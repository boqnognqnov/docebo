<h3><?= Yii::t('app7020', 'Experts assigned to the channel'); ?></h3>
<div class="clear"></div>
<?php
$this->widget('DoceboCGridView', array(
	'id' => 'course-management-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix'),
	'dataProvider' => $statsExperts["channelExperts"],
	'cssFile' => false,
	'enableSorting' => false,
	'enablePagination' => true,
	'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'columns' => array(
		array(
			'name' => 'username',
			'header' => Yii::t('standard', 'Username'),
		),
		array(
			'name' => 'fullname',
			'header' => Yii::t('standard', 'Expert Full Name'),
		),
		array(
			'name' => 'email',
			'header' => Yii::t('standard', 'Email'),
		)
	)
));
?>
<ul class="table-top5">
	<li class="table-row .row-header row-fluid">
		<span class="cell cell-header span7"><h2><?php echo Yii::t('app7020', "Top 5 More active experts (coach)"); ?></h2> <span class="text subtitle"><?php echo Yii::t('app7020', "Top 5 More active experts in the channel (coach)"); ?></span></span>
		<span class="cell cell-reply span1"><i class="fa fa-reply" ></i><span class="icon-description"><?php echo Yii::t('app7020', "Answered"); ?></span></span>
		<span class="cell cell-best-answer span2"><i class="fa fa-check-circle"></i><span class="icon-description"><?php echo Yii::t('app7020', "Best answers"); ?></span></span>
		<span class="cell cell-like span1"><i class="fa fa-thumbs-o-up"></i><span class="icon-description"><?php echo Yii::t('app7020', "Likes"); ?></span></span>
		<span class="cell cell-dislike span1"><i class="fa fa-thumbs-o-down"></i><span class="icon-description"><?php echo Yii::t('app7020', "Dislikes"); ?></span></span>
	</li>
	<?php $i = 1;
	foreach ($statsExperts["topCoachExperts"]["topCoachExperts"] as $expert) {
		?>
		<li class="table-row row-fluid">
			<span class="cell cell-index span1"><?= $i ?></span>
			<span class="cell cell-name span6">
				<span class="avatar">				
					<?php
					$this->widget('common.widgets.App7020Avatar', array('userId' => $expert["idst"]))
					?>
				</span>				<?php
				if (trim($expert["expert_name"])!="") {
					echo $expert["expert_name"];
				} else {
					echo trim($expert["username"],'/');
				}
				?></span>
			<span class="cell cell-dislike span1"><?= $expert["count_answers"]; ?></span>
			<span class="cell cell-best-answer span2"><?= $expert["count_bestAnswers"]; ?></span>
			<span class="cell cell-like span1"><?= $expert["count_likes"]; ?></span>
			<span class="cell cell-dislike span1"><?= $expert["count_dislikes"]; ?></span>
		</li>
		<?php $i++;
	}
	?>
</ul>
<ul class="table-top5">
	<li class="table-row .row-header row-fluid">
		<span class="cell cell-header span8"><h2><?php echo Yii::t('app7020', "Top 5 More active experts (share)"); ?></h2> <span class="text subtitle"><?php echo Yii::t('app7020', "Top 5 More active experts in the channel (share)"); ?></span></span>
		<span class="cell cell-reply span2"><i class="fa fa-check" ></i><span class="icon-description"><?php echo Yii::t('app7020', "Approved assets"); ?></span></span>
		<span class="cell cell-reply span2"><i class="fa fa-clock-o"></i><span class="icon-description"><?php echo Yii::t('app7020', "Avg. review time"); ?></span></span>
	</li>
<?php $i = 1;
foreach ($statsExperts["topCoachExperts"]["topShareExperts"] as $expert) { ?>
		<li class="table-row row-fluid">
			<span class="cell cell-index span1"><?= $i ?></span>
			<span class="cell cell-name span7">
				<span class="avatar">				
				<?php
				$this->widget('common.widgets.App7020Avatar', array('userId' => $expert["idst"]))
				?>
				</span>
				<?php
				if (trim($expert["expert_name"])!="") {
					echo $expert["expert_name"];
				} else {
					echo trim($expert["username"],'/');
				}
				?>
			</span>
			<span class="cell cell-dislike span2"><?= $expert["published"]; ?></span>
			<span class="cell cell-best-answer span2"><?= $expert["hours"]; ?>h <?= $expert["minutes"]; ?>m</span>
		</li>
	<?php $i++;
}
?>
</ul>