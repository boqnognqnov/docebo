<?php
$usermanContainerId = "analytics_manageInvitations";
$usersGridId = "userman-users-management-grid";
$searchFormId = "userman-search-form";
$selectedUsersFieldName = "userman_selected_users";
$expertsCount = App7020Experts::sqlDataProvider()->totalItemCount;
?>
<div id="analytics_manageInvitations">
	<div class="row-fluid">
		<div class="invitePeopleSection">
			<div class="main-actions clearfix">
				<!--Action buttons-->
				<ul class="clearfix" data-bootstro-id="customActionButtons">
					<li>
						<div>
							<?php
							$title = '<span class="fa fa-user-plus"></span>'
									. '<span class="boxTitle">'
									. '<span class="innerBoxTitle">'
									. Yii::t('app7020', 'Invite people to watch')
									. '</span>'
									. '</span>';
							?>
							<a href="#" id="showInviteToWatch">
								<?php echo $title; ?>
							</a>
						</div>
					</li>
				</ul>
				<div class="app7020-hint-userInvitationGridBlankState app7020-hint <?php echo ($sendDataToView['dataProvider']->totalItemCount) ? '' : 'show'; ?>">
					<h3><span><?php echo Yii::t('app7020', 'Ops... it looks like not so many'); ?></span></h3>
					<h3><span><?php echo Yii::t('app7020', 'people are watching this asset!'); ?></span></h3>
					<h3><?php echo Yii::t('app7020', 'Invite people to watch it now!'); ?></h3>
				</div>
			</div>
		</div>
	</div>

	<!-- Analytic chart counters section -->
	<div class="row-fluid">
		<?php $this->widget('common.widgets.app7020AnalyticStatistics', array('params' => App7020Invitations::getStatisticsData($sendDataToView['idContent']), 'htmlOptions' => array('data-idObject' => $sendDataToView['idContent']))); ?>
	</div>
	<!--Invitations CGridView-->
	<div class="row-fluid">
		<div class="inivitationsGridView clearfix">
			<p class="headingline"><?php echo Yii::t('app7020', 'Send invitations'); ?></p>
			<?php
			$form = $this->beginWidget('CActiveForm', array(
				'id' => $searchFormId,
				'htmlOptions' => array(
					'class' => 'form-inline',
				),
			));
			?>
			<div class="main-section" <?php if (!$sendDataToView['dataProvider']->totalItemCount) { ?>style="display: none;" <?php } ?>>
				<div class="filters-wrapper clearfix">
					<div class="checkbox-input-wrapper">
						<?php echo CHtml::checkBox('onlyNonWatched', false, array('id' => 'notWatchedCheckbox')); ?>
						<?php echo CHtml::label(Yii::t('app7020', 'Show only people that haven\'t watched the asset yet'), 'notWatchedCheckbox', array()); ?>
					</div>
					<div class="search-input-wrapper">
						<?php
						$params = array();
						$params['max_number'] = 10;
						$params['autocomplete'] = 1;
						$params['id'] = $sendDataToView['idContent'];
						$autoCompleteUrl = Docebo::createApp7020Url($sendDataToView['searchAction'], $params);
						$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
							'id' => 'userman-search-input',
							'name' => 'search_input',
							'value' => '',
							'options' => array(
								'minLength' => 1,
							),
							'source' => $autoCompleteUrl,
							'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
						));
						?>
						<button type="button" class="close clear-search">&times;</button>
						<span 	class="perform-search search-icon"></span>
					</div>
				</div>
			</div>
			<?php
			echo CHtml::hiddenField($selectedUsersFieldName, '');
			$this->endWidget();
			?>
			<div class="userman-users-grid-wrapper">
				<?php
				$this->widget('DoceboCGridView', array(
					'selectableRows' => 0,
					'ajaxType' => 'POST',
					'id' => $usersGridId,
					'dataProvider' => $sendDataToView['dataProvider'],
					// Elements which will trigger grid update
					'updateSelector' => "{page}, {sort}, .search-input-wrapper .perform-search, #userman-advanced-search .btn-search",
					'columns' => $sendDataToView['selectedColumns'],
					// non-quartz-items: because of a Global JS event listener in script.js (see there please)
					// which breaks normal Yii event chain
					'itemsCssClass' => 'items non-quartz-items',
					'template' => '{items}{summary}{pager}',
					'summaryText' => Yii::t('standard', '_TOTAL'),
					'emptyText' => Yii::t('app7020', 'You haven\'t invited anyone yet to watch the asset.').' '.Chtml::link(Yii::t('app7020', 'Invite people now!'), 'javascript:;', array('class' => 'gridViewOpenInvitePeople')),
					'pager' => array(
						'class' => 'DoceboCLinkPager',
						'maxButtonCount' => 8,
					),
					'beforeAjaxUpdate' => 'function(id,options) {UserMan.beforeUsersGridUpdate(id,options);}',
					'afterAjaxUpdate' => 'function(id, data) {
						var emptyStateHint = $(\'#manageInvitations .invitePeopleSection .app7020-hint-userInvitationGridBlankState\');
						if($(\'#\'+id+\' .items .empty\').length > 0){emptyStateHint.addClass(\'show\');}else{emptyStateHint.removeClass(\'show\');}
						UserMan.setGridColumnsVisibility();
						UserMan.afterUsersGridUpdate(id,data);
						$(\'a[rel="tooltip"]\').tooltip();
						$(\'.popover-action\').popover({
							placement: \'bottom\',
							html: true
						});
						$(document).controls();
					}',
					'selectionChanged' => 'function(id) {
						UserMan.gridSelectionChanged(id);
					}'
				));
				?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="action_buttons_combolist">
			<?php echo CHtml::link(Yii::t('app7020', 'Re-send all invitations'), '', array('class' => 'app7020-buttons-blue')); ?>
			<?php echo CHtml::link(Yii::t('app7020', 'Cancel all invitations'), '', array('class' => 'app7020-buttons-red')); ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	var userManOptions = {
		containerId: '<?= $usermanContainerId ?>',
		usersGridId: '<?= $usersGridId ?>',
		searchFormId: '<?= $searchFormId ?>',
		userManActionUrl: '<?= Docebo::createApp7020Url($searchAction) ?>'
	};

	// See themes/spt/js/user_management.js
	var UserMan = new AdminUserManagement(userManOptions);

</script>