<div id="confirmInvitationDialog" data-assetId="<?php echo $assetId ?>" data-userId="<?php echo $userId ?>" data-mass="<?php echo $mass ?>" data-method="<?php echo $method ?>">
	<?php if($method == 'resend'): ?>
	<p class=""><?php echo Yii::t('app7020', 'Resend invitations to {count} <strong>user(s)</strong> that <strong>haven\'t watched</strong> the asset yet?', array('{count}' => $usersCount)); ?></p>
	<?php endif; ?>
	<?php if($method == 'cancel'): ?>
	<p class=""><?php echo Yii::t('app7020', 'Cancel invitations to {count} <strong>user(s)</strong> that <strong>haven\'t watched</strong> the asset yet?', array('{count}' => $usersCount)); ?></p>
	<?php endif; ?>
		
    <?php echo CHtml::checkBox('agreeCheck', false, array('id' => 'confirm')) ?>
    <?php echo CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'confirm', array('style' => 'display: inline-block')) ?>
</div>
<div class="form-actions">
    <?php
		echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green'));
		echo CHtml::button('Cancel', array('class' => 'close-dialog'));
    ?>
</div>