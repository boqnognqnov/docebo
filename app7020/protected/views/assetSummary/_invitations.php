<?php $timeWords = array('d' => '<span class="small-num">d</span>', 'h' => '<span class="small-num">h</span>', 'm' => '<span class="small-num">m</span>'); ?>
<h3><?= Yii::t('app7020', 'Invitations'); ?></h3>
<div class="left span3">
	<i class="fa fa-user-plus" aria-hidden="true"></i>
	<span id="totalInvitedPeople" class="num"><?= $watchRate["data"]["totalInvitedPeople"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Total invited people'); ?></span>
</div>
<div class="span3">
	<i class="fa fa-pie-chart" aria-hidden="true"></i>
	<span class="num"><?= $watchRate["data"]["globalWatchRate"]; ?> <span class="small-num" >%</span></span>
	<span class="text"><?= Yii::t('app7020', 'Global watch rate'); ?></span>
</div>
<div class="span3">
	<i class="fa fa-clock-o" aria-hidden="true"></i>
	<span class="num"><?= App7020Helpers::secondsToWords($watchRate["data"]['avgReactionTime'], $timeWords, true, true, true, false); ?></span>
	<span class="text"><?= Yii::t('app7020', 'Avg. reaction time'); ?></span>
</div>
<?php if ($watchRate["data"]["totalInvitedPeople"]) { ?>
	<div class="span3 watch-rate">
		<canvas id="DoughnutInvitation" width="90" height="90"></canvas>
		<div class="small-info-block green"><?= $watchRate["data"]["watched"]; ?><br>Watched</div>
		<div class="small-info-block gray"><?= $watchRate["data"]["notWatched"]; ?><br>Unwatched</div>
		<i class="fa fa-user-plus" aria-hidden="true"></i>
	</div>
<?php } ?>
<div class="clear"></div>
<h3><?= Yii::t('app7020', 'Sent Invitations'); ?></h3>
<?php
$this->widget('DoceboCGridView', array(
	'id' => 'course-management-grid',
	'htmlOptions' => array('class' => 'grid-view clearfix'),
	'dataProvider' => $invitations,
	'cssFile' => false,
	'enableSorting' => false,
	'enablePagination' => false,
	'template' => '<div class=scroll-content>{items}</div>{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'DoceboCLinkPager',
		'maxButtonCount' => 8,
	),
	'columns' => array(
		array(
			'name' => 'username',
			'header' => "USERNAME",
		),
		array(
			'name' => 'fullName',
			'header' => "FULLNAME",
		),
		array(
			'name' => 'created',
			'header' => "SENT",
		),
		array(
			'name' => 'watched',
			'header' => "WATCHED",
		)
	),
));
?>
	