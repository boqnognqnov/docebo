<h3><?= Yii::t('app7020', 'Social'); ?></h3>
<div class="timeframe-selector">
	<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
	<?php echo CHtml::dropDownList('socialTimeframe', '30', $timeframes);?>
</div>
<div class="clear" ></div>
<div class="left span4 data-box">
	<span class="num"><?= $statsSocial["totalInvitations"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Invitations sent by users'); ?></span>
</div>
<div class="clear"></div>
<h3><?= Yii::t('app7020', 'Overal Invitations'); ?></h3>
<canvas id="myChartSocial" width="820" height="200"></canvas>