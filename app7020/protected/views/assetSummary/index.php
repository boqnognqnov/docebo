<?php
    $assetViewUrl = Docebo::createApp7020AssetsViewUrl($assetData["assetId"]);
?>
<div data-asset-id="<?= $assetData["assetId"]; ?>" id="App7020AssetSummaryReport">
	<div class="generate-report">
		<div class="report-form">
			<input id="asset-autocomplete" placeholder="Type asset name here" type="text">
			<a class="new-user-summary" id="generateAssetSummaryReport" href="<?php echo Docebo::createApp7020Url('assetSummary/index'); ?>	"><?= Yii::t('app7020', 'Generate'); ?></a>
		</div>
	</div>
	<div class="assetsummary-container">
		<div class="asset-info">
			<h1><a href="<?= $assetViewUrl ?>"><?= $assetData["title"]; ?></a></h1>
			<div class="avatar">
				<?php
				    $this->widget('common.widgets.App7020Avatar', array('userId' => $assetData["userId"]))
				?>
			</div>
			<p><?= Yii::t('app7020', 'By') . " <a>" . $contributor . "</a> " . App7020Helpers::timeAgo(App7020Assets::model()->findByPk($assetData["assetId"])->created) . " " . Yii::t('app7020', 'ago'); ?><p>
		</div>
		<div class=""><a href="<?= $assetViewUrl ?>"><img class="asset-thumb" src="<?= $assetData["thumb"]; ?>"></img></a></div>
		<i class="content-toggle fa fa-chevron-down" aria-hidden="true"></i>
		<div style="" class="description">
			<div class="description-content">
				<div class="small-title"><?= Yii::t('app7020', 'Description'); ?></div>
				<?= $assetData["description"]; ?>
				<div class="small-title"><?= Yii::t('app7020', 'Tags'); ?></div>
				<ul class="tag-list">
					<?php
                        foreach ($tags as $key => $value) {
                            if ($value) {
                                echo "<li>" . $value . "</li>";
                            }
                        }
					?>
				</ul>
				<div class="small-title"><?= Yii::t('app7020', 'Channels'); ?></div>
				<ul class="channels-list">
					<?php
					foreach ($channels as $channel) {
						if ($channel["channel_name"] != '') {
							echo "<li><a href='" . $channel["channel_url"] . "'>" . $channel["channel_name"] . "</a></li>";
						}
					}
					?>
				</ul>
			</div>
		</div>
	</div>

	<div id="sidebar" class="docebo-col-md-3 advanced-sidebar">
		<ul>
			<li class="active"><a data-toggle="tab" href="#overview"><?= Yii::t('app7020', 'Overview'); ?></a></li>
			<li><a data-toggle="tab" href="#social"><?= Yii::t('app7020', 'Social'); ?></a></li>
			<li><a data-toggle="tab" href="#expert"><?= Yii::t('app7020', 'Ask the expert'); ?></a></li>
			<li><a data-toggle="tab" href="#invitations"><?= Yii::t('app7020', 'Invitations'); ?></a></li>
		</ul>
	</div>
	<div class="tab-content  docebo-col-md-9">
		<div id="overview" class="row-fluid tab-pane active">
			<?php
			$this->renderPartial('_overview', array(
				'statsOverview' => $statsOverview, 'timeframes' => $timeframes));
			?>
		</div>
		<div id="social" class="row-fluid tab-pane fade">
			<?php
			$this->renderPartial('_social', array(
				'statsSocial' => $statsSocial, 'timeframes' => $timeframes));
			?>
		</div>
		<div id="expert" class="row-fluid tab-pane fade">
			<?php
			$this->renderPartial('_askTheExpert', array(
				'statsExpert' => $statsExpert, 'timeframes' => $timeframes));
			?>
		</div>
		<div id="invitations" class="row-fluid tab-pane fade">
<?php
$this->renderPartial('_invitations', array(
	'watchRate' => $watchrate, 'timeframes' => $timeframes, 'invitations' => $invitations));
?>
		</div>
	</div>
</div>	
<script type="text/javascript">

	$(".content-toggle").click(function () {
		$(".description").toggle();
		$(this).toggleClass("fa-chevron-up");
		$(this).toggleClass("fa-chevron-down");
	});
</script>