<h3><?= Yii::t('app7020', 'Overview'); ?></h3>
<div class="timeframe-selector">
	<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
	<?php echo CHtml::dropDownList('overviewTimeframe', '30', $timeframes);?>
</div>
<div class="clear"></div>
<div class="left span4 data-box">
	<span id="totalViews" class="num "><?= $statsOverview["totalViews"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Total views'); ?></span>
</div>
<div class="span4 data-box">
	<span class="num rateNum"><?= $statsOverview["assetRating"]; ?></span>
	<div class="rating-directive">
		<?php
		$this->widget('common.widgets.ContentRating', array('contentId' => 536, 'readOnly' => true));
		?>
	</div>
	<div class="clear"></div>
	<span class="text"><?= Yii::t('app7020', 'Asset rating'); ?></span>
</div>
<div class="clear"></div>
<h3><?= Yii::t('app7020', 'Total View'); ?></h3>
<canvas id="myChartOverview" width="820" height="200"></canvas>