<h3><?= Yii::t('app7020', 'Ask the Expert'); ?></h3>
<div class="timeframe-selector">
	<label for="timeframe-selector"><?= Yii::t('app7020', 'Timeframe'); ?></label>
	<?php echo CHtml::dropDownList('askTheExpert', '30', $timeframes);?>
</div> 
<div class="clear"></div>
<div class="left span4 data-box">
	<span id="totalQuestions" class="num"><?= $statsExpert["totalQuestions"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Questions'); ?></span>
</div>
<div class="span4 data-box">
	<span id="totalAnswers" class="num"><?= $statsExpert["totalAnswers"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Answers'); ?></span>
</div>
<div class="span4 data-box">
	<span id="bestAnswers" class="num"><?= $statsExpert["bestAnswers"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Best Answers'); ?></span>
</div>
<div class="left span4 data-box">
	<span id="answerLikes" class="num"><?= $statsExpert["likes"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Answers likes'); ?></span>
</div>
<div class="span4 data-box">
	<span id="answerDislikes" class="num"><?= $statsExpert["dislikes"]; ?></span>
	<span class="text"><?= Yii::t('app7020', 'Answers dislikes'); ?></span>
</div>
<div class="clear"></div>
<div class="legend">
	<div class="legend-question">
		<span class="square-green"></span><label><?= Yii::t('app7020', 'Questions'); ?></label>
	</div>
	<div class="legend-answer">
		<span class="square-blue"></span><label><?= Yii::t('app7020', 'Answers'); ?></label>
	</div>
</div>
<h3><?= Yii::t('app7020', 'Questions and answers'); ?></h3>
<canvas id="expertChart" width="820" height="200"></canvas>