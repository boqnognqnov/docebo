<div class="peer-wrapper">
	<div class="peer-reviews-wrapper">
		<div class="header">
			<span class="header-title blue bold"><?php echo Yii::t('app7020', 'Related experts:'); ?></span>
			<div class="peers">
				<ul>
					<li ng-repeat="expert in peer_reviews.experts">
						<a href="{{expert.expert_channel_url}}" ng-if="$index<2" ng-bind-html="expert.thumbnail_url">
							<!--
							<img ng-if="expert.thumbnail_url" class="thumb" ng-src="{{expert.thumbnail_url}}" width="42px" height="42px" alt="{{expert.expert_names}}" />
							-->
						</a>
						<span ng-if="$index>=2" class="more-experts">3<i class="plus-up">+</i><span>
					</li>
				</ul>
			</div>
		</div>
		<div class="content">
			<div class="all-chats">
				 <!-- {{details.published_datetime | date: 'fullDate' }} -->
				<div ng-repeat="(key, value) in peerData.items ">
					<div class="chat-date">{{ key | date: 'fullDate' }}</div>
					<ul class="wrap-by-date">
						<li ng-repeat="(k,item) in value" class="peer {{item.type}}" data-date="{{item.date}}">
							<div ng-if="item.avatar" ng-bind-html="item.avatar" class="peer-thumb"></div>
							<div class="review-text">
								<div class="heading-info">
									<span class="peer-name bold">{{item.pr_owner}}</span>
									<span class="review-time">{{ item.update_date }}{{ item.hour }}</span>
								</div>
								<div class="review-content">
									{{item.title}}
								</div>
								<div class="peer-dots-menu"  ng-if="item.actions.length > 0 && item.status  == '0'" >
									<span class="dots item-stable" ng-click="toggleGroup(item)" ng-class="{active: isGroupShown(item)}">...</span>
									<div class="peer-review-actions" ng-show="isGroupShown(item)">
										<ul>
											<li ng-repeat="action in item.actions">
												<span class="{{action.action_uid}}" data-date="{{key}}" data-index ="{{k}}" data-review="{{item.id}}" data-type="{{action.action_type}}" data-id="{{action.action_uid}}">{{action.label}}</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</li>
					</ul>
					
				</div>	
			</div>
		</div>
		<div class="footer">
			<form>
				<input id="peerMessage" autocomplete="off" ng-model="title" name="type-question" type="text" placeholder="<?php echo Yii::t('app7020', 'Type here your question..'); ?>">
				<button ng-disabled="isDisabled" ng-click="addNew()" class="submit-review green"><i class="fa fa-paper-plane"></i></button>
			</form>
		</div>
	</div>
</div>
