<div class="docebo-row" ng-show="dataIsLoaded">
	<div class="docebo-col-md-8 docebo-col-sm-12 docebo-col-xs-12 box-sizing">
		<asset-player external="true"></asset-player>
	</div>
	<div class="docebo-col-md-4 docebo-col-sm-12 docebo-col-xs-12 box-sizing">
		<tooltip-sidebar></tooltip-sidebar>
	</div>
	<div class="docebo-col-xs-12 box-sizing">
		<tooltip-timeline></tooltip-timeline>
	</div>
</div>