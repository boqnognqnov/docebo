<div class="docebo-row">
	<div ng-if="!error && dataIsLoaded" class="docebo-col-md-8 docebo-col-sm-12 docebo-col-xs-12 box-sizing">
		<asset-player external="true"></asset-player>
		<asset-edit-details></asset-edit-details>
	</div>
	<div ng-if="!error && dataIsLoaded" class="docebo-col-md-4 docebo-col-sm-12 docebo-col-xs-12 box-sizing">
		<peer-review></peer-review>
	</div>

	<div ng-if="!error &&  dataIsLoaded" class="docebo-col-md-12 docebo-col-sm-12 docebo-col-xs-12 box-sizing edit-form-wrapper">
		<asset-edit></asset-edit>
		<div id="edit-notice"></div>
	</div>
	<div ng-if="error">{{error}}</div>
</div>