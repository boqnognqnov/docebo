<div ng-if="!error && dataIsLoaded" class="docebo-row" ng-show="dataIsLoaded">
	<div class="docebo-col-sm-12 docebo-col-xs-12 box-sizing" ng-class="similar.length > 0 ? 'docebo-col-md-8' : 'docebo-col-md-12'">
		<asset-player id="App7020Asset" data-asset-id="{{assetId}}" external="true"></asset-player>
		<asset-details></asset-details>
	</div>
	<div class="docebo-col-md-4 docebo-col-sm-12 docebo-col-xs-12 box-sizing" ng-show="similar.length > 0">
		<similar-assets></similar-assets>
	</div>
</div>

<div ng-if="!error && dataIsLoaded" id="questionsApp" ng-cloak >
	<?php
	$single = Yii::app()->request->getParam('mode', false);
	if ($single != 'single') {
		?>
		<div class="master-content" ng-controller="AssetPlayAskTheExpert" style="padding: 0 0 35px 0;" >
			<div class="inner-content-wrapper" ng-init="init()">
				<div class="items-wrapper" question-items items="items" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
			</div>
		</div>
		<?php
	} else {
		?>
		<div class="inner-content-wrapper" ng-init="init(false, false)" id="questionPage" ng-cloak ng-show="question.length !== 0">
			<?php $this->renderPartial('../askTheExpert/templates/singleQuestionTemplate', array(), false); ?>
		</div>
		<?php
	}
	?>
	<div class="alertWrapper"></div>
	<div class="go-to-top"></div>
</div>
<div ng-if="error">{{error}}</div>