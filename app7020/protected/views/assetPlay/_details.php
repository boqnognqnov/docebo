<div class="asset-details">
	<div class="meta-details">
		<div class="title-details">
			<h1><?php echo Yii::t('app7020', 'A 380 evacuation procedure - introduction'); ?></h1>
			<div class="author-avatar">
				<img class="avatar-thumb" src="http://placehold.it/42x42" alt="author thumbnail" />
			</div>
		</div>
		<div class="asset-metas">
			<span><?php echo Yii::t('app7020', 'by'); ?></span>
			<a href="/app7020/index.php?r=channels/index/#/pchannel/18066" class="ng-binding">paul.brown</a>
			<span>345 <?php echo Yii::t('app7020', 'views'); ?></span>
			<span>2 <?php echo Yii::t('app7020', 'days ago'); ?></span>
		</div>
	</div>

	<!-- applicable on view mode -->
	<div class="content-toggle" data-mode="view" ng-hide="collapsed">
		<div class="asset-description container-line">
			<h3><?php echo Yii::t('app7020', 'Description'); ?></h3>
			Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum Lorem lipsum
		</div>
		<div class="asset-tags container-line">
			<h3><?php echo Yii::t('app7020', 'Tags'); ?></h3>
			<ul>
				<li ng-repeat="tag in [1, 2, 3, 4]">
					<?php echo Yii::t('app7020', 'Tag name1'); ?>
				</li>
			</ul>
		</div>
		<div class="asset-channels container-line">
			<h3><?php echo Yii::t('app7020', 'Channels'); ?></h3>
			<ul>
				<li ng-repeat="channel in [1, 2, 3, 4]">
					<a href=""><?php echo Yii::t('app7020', 'Channel name1'); ?></a>
				</li>
			</ul>
		</div>
	</div>
	<div class="angle-toggle text-center" ng-click="collapsed=!collapsed">
		<i ng-class="(collapsed) ? 'fa fa-angle-down' : 'fa fa-angle-up'"></i>
	</div>

	<!-- Aassets actions -->
	<div class="asset-details-footer">
		<!-- Asset rating -->
		<div class="rating-directive">
			<span class="blue bold"><?php echo Yii::t('app7020', 'Rate this asset!'); ?></span>
			
			<!-- rating.rating1 should be defined in Controller as this.rating1 = 5; where '5' is the current rating value -->
			<asset-rating ng-model="rating.rating1" max="5"></asset-rating>
			
			<div class="rated-check" data-rated="1">
				<!-- if asset not rated yet -->
				<span class="gray not-rated"><?php echo Yii::t('app7020', 'Not yet rated'); ?></span>
				<!-- if asset rated  -->
				<span class="rated-value">3.5</span>
			</div>
		</div>
		<!-- Asset actions -->
		<asset-actions></asset-actions>
	</div>
</div>