<div data-content="<?= $content_id; ?>" data-target="#formArea-<?= $content_id; ?>" data-input="<?= $video['id'] ?>" data-stage="<?php echo!empty($video['conversion_status']) ? $video['conversion_status'] : (string) 0; ?>" id="file-id-<?= $video['id'] ?>" class="plupload-file-container <?= App7020Assets::getProccessClass($video['conversion_status']); ?>">
    <div class="row-fluid app7020-row-progress">
        <div class="span12">
            <div class="app7020-table-content">
                <div class="app7020-table-row ">
					<div class="app7020-camera-left-col app7020-camera"></div>
					<div class="app7020-content-center-col">
						<!--Uploading counter label-->
						<div class="app7020-contribite-uploading-text-percent">
							<?= Yii::t('app7020', 'Uploading'); ?>  <b>"<?= $video['originalFilename'] ?>"</b>
							<span  class="app7020-contribite-uploading-percent">0%</span>
							<a class="app7020-stop-uploading"  data-file-id="<?= $video['id'] ?>">
								<u>
									<?= Yii::t('app7020', 'Cancel upload'); ?>
								</u>
							</a>
						</div>
						<!--Success local storage-->
						<div class="app7020-video-uploaded " data-file-id="<?= $video['id'] ?>" data-original-file-name="<?= $video['filename'] ?>" data-renamed-file-name="<?= $video['originalFilename'] ?>">
							<b> 
								<?= Yii::t('app7020', 'Contribution'); ?> "<?= $video['originalFilename'] ?>" <?= Yii::t('app7020', 'uploaded'); ?>!
							</b> 
							<?= Yii::t('app7020', 'Add'); ?>
							<b>
								<?= Yii::t('app7020', 'topics'); ?>
							</b>
							<?= Yii::t('app7020', 'and'); ?>
							<b>
								<?= Yii::t('app7020', 'details'); ?>
							</b>
							<?= Yii::t('app7020', 'to'); ?>
							<a href="#app7020-file-list-container" class="app7020-publish-video">
								<?= Yii::t('app7020', 'publish your contribution'); ?>
							</a>
						</div>
						<!--Error message dom-->
						<div class="app7020-video-error">
							<strong>
								<?= Yii::t('app7020', 'Error'); ?>: <?= Yii::t('app7020', 'Something went wrong, contribution cannot be uploaded.'); ?>
							</strong> 
							<?= Yii::t('app7020', 'Error message and status'); ?>
						</div>
						<!--Waiting to send file to s3-->
						<div class="app7020-video-uploaded app7020-lmsupload-success">
							<b><?= Yii::t('app7020', 'Contribution'); ?> "<?= $video['originalFilename'] ?>" <?= Yii::t('app7020', 'uploaded on LMS'); ?>!</b> 
						</div>
						<div class="app7020-video-s3-waiting-send">
							<i><?= Yii::t('app7020', 'Uploading on Cloud'); ?> <?php App7020Assets::isVideo($video['filename']) ? 'Converter' : ''; ?>...</i>
						</div>
						<!--Waiting to send file to s3-->
						<div class="app7020-video-s3-waiting-conversion">
							<i><?= Yii::t('app7020', 'Waiting...'); ?></i>
						</div>
						<!--Waiting to send file to s3-->
						<div class="app7020-video-s3-complete">
							<i><?= Yii::t('app7020', 'Complete'); ?></i>
						</div>
						<!--Progress bar--> 
						<div class="docebo-progress progress" id="app7020-progress-<?= $video['id'] ?>">
							<div id="uploader-progress-bar-<?= $video['id'] ?>" class="bar" style="width: 1%"></div>
						</div>

						<!--review stage--> 
						<div class="app7020-inreview-container">
							<div class="app7020-checkbox-icon">
								<i class="fa fa-check"></i> 
							</div>
							<div class="app7020-inreview-info">
								<strong><?= $video['originalFilename'] ?></strong>
								<?= Yii::t('app7020', 'has been submitted for the Expert\'s peer review. You\'ll be notified as soon as it approved.'); ?>
								<a href="" class="app7020-inreview-view">
									<?= Yii::t('app7020', 'View'); ?>
								</a>
							</div>
						</div>
					</div>
					<div class="app7020-control-right-col">
						<?php
						if (!App7020Assets::hasS3Error($video['conversion_status'])) {
							?>
							<a href="#app7020-file-list-container" class="btn edit app7020-button  <?= !App7020Assets::hasEditAccess($video['conversion_status']) ? 'disabled' : ''; ?> ">
								<?= Yii::t('app7020', 'SHOW MORE'); ?>
							</a>
							<a href="<?= Docebo::createApp7020AssetsViewUrl($content_id) ?>" id="app7020-inreview-file-<?php echo $video['filename']; ?>" class="btn info app7020-button  <?= !App7020Assets::hasEditAccess($video['conversion_status']) ? 'disabled' : ''; ?> ">
								<?= Yii::t('app7020', 'View'); ?>
							</a>
							<?php
						}
						?>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

