<?php
/* @var $content App7020Content */
?>
<section class="row-fluid form-area-row <?= isset($isHide) && $isHide == true ? 'hide' : ''; ?>"  id="formArea-<?= $content->id; ?>">
	<?php
	$form = $this->beginWidget('CActiveForm', $form_args);
	?>
	<div class="row-fluid">
		<div class="span12 contribute_file_header">
			<div class="info">
				<!-- IF is ShareLink show link ELSE show asset filename -->
				<?php if ($content->contentType == App7020Assets::CONTENT_TYPE_LINKS): ?>
					<div class="file">
						<strong>Shared link:</strong>
						<span class="name">
							<?php echo $content->originalFilename; ?>
						</span>
					</div>
				<?php else: ?>
					<div class="file">
						<strong>File:</strong>
						<span class="name"><?php echo $content->originalFilename ?></span>
					</div>
				<?php endif; ?>
				<span class="required"><span>*</span><?php echo Yii::t('app7020', 'Mandatory fields'); ?></span>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span8 formArea">
			<div class="row-fluid app7020-form-container">

				<div class="span3 app7020-thumbnails">
					<?php echo $content->getPreviewThumbnailImage(TRUE, 'small'); ?>
					<?php
					if ($content->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
						?>
						<div class="row-fluid">
							<h6 class="title app7020-thumbnail-heading">
								<?= Yii::t('app7020', 'Choose thumbnail'); ?>
							</h6>
						</div>	
						<?php
						echo $form->radioButtonList($content->getActiveThumbnail(), 'id', $labels_args, $radio_list_args);
					}
					?>
				</div>
				<div class="span9 app7020-section-form">
					<div class="row-fluid">
						<div class="span12">
							<h6 class="title required">
								<?= Yii::t('app7020', 'Details'); ?>
							</h6>
							<p>
								<?= Yii::t('app7020', 'Define video\'s Title and Description'); ?>
							</p>
						</div>
					</div>
					<div class="row-fluid">
						<div class="control-group">
							<div class="controls">
								<?php
								echo $form->hiddenField($content, 'contentType', array('id' => 'App7020Content_contentType-' . $content->id,));
								echo $form->textField($content, 'title', array('placeholder' => Yii::t('app7020', 'Title'), 'id' => 'App7020Content_title-' . $content->id,));
								?>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<?php
								echo $form->textArea($content, 'description', array('placeholder' => Yii::t('app7020', 'Description'), 'id' => 'App7020Content_description-' . $content->id,));
								?>
								<!--<textarea rows="3" placeholder="Description"></textarea>-->
							</div>
						</div>
					</div>
				</div>

				<div class="span9 app7020-section-tags">
					<div class="row-fluid">
						<div class="span12">
							<h6 class="title"><?= Yii::t('app7020', 'Tags'); ?></h6>
							<p><?= Yii::t('app7020', 'Assign tags to your video to make it easier to find it'); ?></p>
						</div>
					</div>
					<div class="row-fluid">
						<div class="control-group">
							<div class="controls">
								<?php
								$tags = $content->getAllTagsAsString();
								$tagsPost = Yii::app()->request->getParam('hidden-undefined', false);
								if (!empty($tagsPost)) {
									$tags .= ',' . $tagsPost;
								}
								?>
								<input data-included="<?= $tags ?>" class="tags-input tag-input-<?= $content->id; ?>" type="text"  placeholder="Add tags here(separated by comma)">
								<div class="app7020-tags-container" id="app7020-tags-container-<?= $content->id; ?>"></div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="span4">
			<div id="rightSideBar">
				<?php
				$isDisabledTree = false;
				if ($content->defineContentAdminAccess() && $content->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED) {
					$isDisabledTree = true;
				}
				if (Yii::app()->user->isGodAdmin) {
					$isDisabledTree = false;
				}
				?>
				<?php
				$getPreselectedTopics = $content->getPreselectedTopics() ? $content->getPreselectedTopics() : $content->tempTopics;
				//$this->renderPartial('../includes/_treeWidget', array('preSelected' => $getPreselectedTopics, 'isDisabledTree' => $isDisabledTree, 'id' => $content->id));
				?>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<footer class="text-right">
				<?php
				echo CHtml::submitButton('PUBLISH NOW', array('class' => 'saveButton btn save app7020-button '));
				if (isset($isHide) && $isHide == true) {
					echo CHtml::button('DELETE', array('class' => 'btn delete app7020-button ', 'data-content' => $content->id));
				}
				?>
			</footer>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</section>
