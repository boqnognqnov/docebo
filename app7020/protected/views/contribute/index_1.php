<?php
$this->breadcrumbs = array(
    Yii::t('app7020', 'SeventyTwenty'),
    Yii::t('app7020', 'Contribute'),
);
?>



<div class="row-fluid" id="main-container">
    <div class="span12">


        <div id="top-elements-container" class="row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div id="app7020-contribute-top-hint" class="span7 app7020-upload-tool contribite-head text-center">
                        <h1><?php echo Yii::t('app7020', 'Share your Knowledge!'); ?></h1>
                        <h2><?php echo Yii::t('app7020', 'Upload your videos from different devices'); ?></h2>
                    </div>
                </div>
                <div class="row-fluid" id="app7020-pl-uploader">
                    <div class="span12 app7020-tumb-container" id="app7020-contribute-tumb-container">

                        <div class="span4 app7020-contribute-tumb text-center app7020-contribute-app-video-capture-tumb">
                            <div class="p-sprite app7020-capture-video"></div>
                            <p class="app7020-tumb-text">
                                <?= Yii::t('app7020', 'Video screen<br>capture'); ?>
                            </p>
                            <div class="row-fluid">
                                <div class="span3"></div>
                                <a class="btn-docebo black big span6" id="app-7020-video-sreen" href="javascript:;"  data-bootstro-id="bootstroApp7020Contribute">
                                    <?php echo Yii::t('app7020', 'Record'); ?>
                                </a>
                                <div class="span3"></div>
                            </div>

                        </div> 
                        <div class="span4 app7020-contribute-tumb text-center" id="app7020-pl-uploader-dropzone">
                            <div id="app7020-pl-uploader">
                                <i class="fa fa-cloud-upload app7020-cloud-icon blue-text"></i>
                                <p class="blue-text app7020-tumb-text">
                                    <?= Yii::t('app7020', 'Drop here<br>your videos or'); ?>
                                </p>
                                <div class="row-fluid">
                                    <div class="span3"></div>
                                    <div class="upload-button">
                                        <a class="btn-docebo blue big span6" id="app7020-browse" href="javascript:;">
                                            <?php echo Yii::t('app7020', 'Browse'); ?>
                                        </a>
                                    </div>
                                    <div class="span3"></div>
                                </div>

                            </div>
                        </div> 
                        <div class="span4 app7020-contribute-app-tumb">
                            <div class="span2"></div>
                            <div class="span8 app7020-contribute-app-text">
                                <?= Yii::t('app7020', 'Get SeventyTwenty<br>Mobile App'); ?>
                            </div>
                            <div class="span2"></div>
                        </div>

                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row-fluid" id="app7020-contibute-form-container" style="display: none">
                        <div class="row-fluid">
                            <div class="span12" ><h4><?= Yii::t('app7020', 'Uploading videos...'); ?></h4></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12 app7020-contibute-form-container"> 
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'action' => '',
                                    'method' => 'post',
                                    'htmlOptions' => array(
                                        'id' => 'app7020-upload-contribute-form',
                                        'class' => 'form-horizontal',
                                    ),
                                ));

                                echo CHtml::hiddenField('original-file-name', '', array('id' => 'original-file-name'));
                                echo CHtml::hiddenField('file-name', '', array('id' => 'file-name'));
                                echo CHtml::hiddenField('file-id', '', array('id' => 'file-id'));
                                echo CHtml::hiddenField('tumbnail-file-name', '', array('id' => 'tumbnail-file-name'));
                                ?>
                                
                                
                                <div class="row-fluid">    
                                    <div id="app7020-contribite-form-hint" class="span4 app7020-upload-tool contribite-form text-center" style="position: absolute">
                                        <h2><?php echo Yii::t('app7020', 'Add details here'); ?></h2>
                                    </div>
                                </div> 
                                
                                
                                
                                
                                <div class="row-fluid">
                                    <div class="span3" id="tumbnails-area">
                                        <div class="row-fluid">
                                            <figure id="tumb-large-img" class="span12">
                                                <img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9730&w=150&h=90" class="">
                                            </figure >
                                        </div>
                                        
                                        <div class="row-fluid">
                                            <?php echo Yii::t('app7020', 'Choose thumbnail'); ?>
                                        </div>
                                        
                                        <ul class="thumbnails">
                                            <div class="row-fluid">
                                                <li class="span6">
                                                    <a href="javascript:;" class="thumbnail" data-scr="https://placeimg.com/150/90/arch" data-tumb-file-name="tumb_1.jpg">
                                                      <img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=74%C3%9748&w=74&h=48">
                                                    </a>
                                                </li>
                                                <li class="span6">
                                                    <a href="javascript:;" class="thumbnail" data-scr="https://placeimg.com/150/90/animals" data-tumb-file-name="tumb_2.jpg">
                                                      <img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=74%C3%9748&w=74&h=48">
                                                    </a>
                                                </li>
                                            </div>
                                            <div class="row-fluid">
                                                <li class="span6">
                                                    <a href="javascript:;" class="thumbnail" data-scr="https://placeimg.com/150/90/nature" data-tumb-file-name="tumb_3.jpg">
                                                      <img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=74%C3%9748&w=74&h=48">
                                                    </a>
                                                </li>
                                                <li class="span6">
                                                    <a href="javascript:;" class="thumbnail" data-scr="https://placeimg.com/150/90/tech" data-tumb-file-name="tumb_4.jpg">
                                                      <img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=74%C3%9748&w=74&h=48">
                                                    </a>
                                                </li>
                                            </div>
                                        </ul>
                                         
                                    </div>
                                    
                                    <div class="span10" >
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="row-fluid">
                                                    <div class="row-fluid">
                                                        <div class="span12 video-ready-text">
                                                            <?= Yii::t('app7020', 'Video is ready to be published'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span12"><div class="video-ready-bar"></div></div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span12 text-right mandatory-text">
                                                            <span class="text-error">*</span><?= Yii::t('app7020', 'Mandatory fields'); ?></div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="span9" >
                                                            <div>
                                                                <span class="text-error">*</span><?= Yii::t('app7020', 'Details'); ?>
                                                            </div>
                                                            <div class="muted mandatory-text">
                                                                <?= Yii::t('app7020', "Define video's title and description"); ?>
                                                            </div>
                                                        </div>
                                                        <div class="span3" >
                                                            <div><span class="text-error">*</span><?= Yii::t('app7020', 'Topics'); ?></div>
                                                            <div class="muted mandatory-text">
                                                                <?= Yii::t('app7020', 'Categorize your video by Topic'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">&nbsp;</div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span9">
                                                <?php echo CHtml::textField('file-title', '', array('id' => 'file-title', 'class' => 'input-block-level', 'placeholder' => Yii::t('standard', '_TITLE'))); ?>
                                                <div class="row">&nbsp;</div>
                                                <?php echo CHtml::textArea('file-description', '', array('id' => 'file-description', 'class' => 'input-block-level', 'placeholder' => Yii::t('standard', '_DESCRIPTION'))); ?>
                                                <div class="row">&nbsp;</div>
                                                <div class="row">&nbsp;</div>
                                                <div><?= Yii::t('app7020', 'Tags'); ?></div>
                                                <div class="muted mandatory-text">
                                                    <?= Yii::t('app7020', "Assign tags to your video to make it easier to find it"); ?>
                                                </div>
                                                <div style="display: inline-block;" id="app7020-fcbk">
                                                    <?php
                                                    echo CHtml::dropDownList('choose_tag', '', array(), array(
                                                        'id' => 'choose_tag',
                                                        'empty' => '---',
                                                        'multiple' => 'multiple',
                                                        'class' => 'fbkcompleate app7020-fbk'
                                                    ));
                                                    ?>
                                                </div>
                                                <ul id="tags-container" class="holder">
                                                </ul>    

                                                <div class="row">&nbsp;</div>
                                                <div class="row">&nbsp;</div>
                                            </div>

                                            <div class="span3" >Topics tree area</div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12" id="app7020-contribute-buttons">
                                                <?= CHtml::button(Yii::t('app7020', 'Delete'), array('class' => 'btn btn-docebo red big pull-right')); ?>
                                                <?= CHtml::button(Yii::t('admin', 'Save'), array('class' => 'btn btn-docebo green big pull-right app7020-buttons-margin', 'id' => 'app7020-save-content')); ?>

                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <?php $this->endWidget(); ?>
                            </div>
                                    </div>
                        <div class="row">&nbsp;</div>
                        
                        
                        
                    </div>
                    <div class="row-fluid">
                        <div class="upload-progress span12" id="plupload-progress-container">
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


</div>




<?php
$uploadUrl = Docebo::createLmsUrl('site/AxUploadFile');
$csrfToken = Yii::app()->request->csrfToken;
$flashUrl = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";
$fcbkAutocompleteUrl = Docebo::createApp7020Url('contribute/contributeTagAutocomplete');
?>

<script>
    var $uploadUrl = <?= json_encode($uploadUrl) ?>;
    var $csrfToken = <?= json_encode($csrfToken) ?>;
    var $flashUrl = <?= json_encode($flashUrl) ?>;
    var $fcbkAutocompleteUrl = <?= json_encode($fcbkAutocompleteUrl) ?>;
</script>

