<div id="deleteExpertsFormContainer" data-content-id="<?php echo $contentId; ?>">
    <p class="topicInfo"><?php echo Yii::t('app7020', 'Are you sure you want to delete this Asset?'); ?></p>
    <?= CHtml::checkBox('agreeCheck', false, array('id' => 'confirm')) ?>
    <?= CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'agreeCheck', array('style' => 'display: inline-block')) ?>
</div>
<div class="form-actions">
    <?php
    echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green', 'id' => 'confirmDeleteAsset'));
    echo CHtml::button('Cancel', array('class' => 'close-dialog'));
    ?>
</div>