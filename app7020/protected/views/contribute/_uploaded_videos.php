<?php

if (!empty($listVideo) && is_array($listVideo)) {
	foreach ($listVideo as $listVideoKey => $listVideoData) {
		$upload_content_params = array(
			'content_id' => $listVideoData->id,
			'video' => array(
				'id' => substr($listVideoData->filename, 0, -4),
				'filename' => $listVideoData->filename,
				'originalFilename' => $listVideoData->originalFilename,
				'conversion_status' => $listVideoData->conversion_status,
 			)
		);
		$this->renderPartial('_video_status_container', $upload_content_params);
	}
}