<?php
$this->breadcrumbs = array(
	Yii::t('app7020', 'Contribute')
);
?>
<div id="contributeMasterContainer" class="app7020DefaultStyles">
	<!--Got it Information-->
	<?php if (!App7020Helpers::actionGetGotItByAttributes('contribute_mobile_app_gotit')): ?>
		<div class="row-fluid">
			<div id="app7020-contribute-gotId">
				<i class="fa fa-times app7020-button-hideThis vertical-center"></i>
				<div class="wrapper">
					<div class="doceboIcon"></div>
					<div class="text">
						<p><?php echo Yii::t('app7020', 'Mobile	App coming soon!'); ?></p>
						<span><?php echo Yii::t('app7020', 'Create and Share Knowledge from your mobile device'); ?></span>
					</div>
					<div class="appLinks"></div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<!--Two Tabs for Contrubute-->
	<div class="row-fluid" id="app7020-pl-uploader">
		<div class="span12 app7020-hint-container">
			<header class="app7020-hint app7020-general-heading">
				<h3>
					<?= Yii::t('app7020', 'Share your Knowledge!'); ?>
				</h3>
				<br />
				<h4>
					<?= Yii::t('app7020', 'Upload your contribution from various devices'); ?>
				</h4>
			</header>
		</div>
		<div class="clear clearfix"></div>
		<div id="contributeContainerTabs">
			<input type="hidden" id="idQuestion" name="idQuestion" value="<?php echo $idQuestion; ?>"/>
			<div class="row-fluid">
				<div class="span6 browse" id="app7020-pl-uploader-dropzone">
					<i class="fa fa-cloud-upload"></i>
					<p class="text">
						<?= Yii::t('app7020', 'Drag and drop <br> your file here or'); ?>
					</p>
					<div class="row-fluid" style="text-align: center;">
						<?php
						if ($showBrowseButton):
							?>
							<a id="app7020-browse" href="javascript:;">
								<?php echo Yii::t('app7020', 'Browse'); ?>
							</a>
							<?php
						endif;
						?>
					</div>
				</div>
				<div class="span6 shareLink">
					<div id="shareLinkActiveForm">
						<div class="wrapper">
							<div class="linkInput">
								<?php echo CHtml::label(Yii::t('app7020', 'Create a knowledge asset <strong>by sharing a link</strong>'), 'asd'); ?>
								<?php echo CHtml::textField('sharedLink', '', array('placeholder' => Yii::t('app7020', 'Paste here the URL you want to share...'), 'class' => 'shareLinkField')); ?>
							</div>
							<div class="buttonsPlace">
								<?php echo CHtml::link(Yii::t('app7020', 'Share!'), 'javascript:;', array('class' => 'submitShareButton')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--END-->

	<!--		<div class="row-fluid">
				<ul class="app7020BootstrapTabsExtended">
					<li class="active">
						<a href="#contribute_toFinalize"><?php echo Yii::t('app7020', 'To Finalize'); ?></a>
					</li>
					<li>
						<a href="#contribute_submited"><?php echo Yii::t('app7020', 'Submitted for peer review / published'); ?></a>
					</li>
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="contribute_toFinalize">
						This is tab content -> 'To Finalize'
					</div>
					<div class="tab-pane" id="contribute_submited">
						This is tab content -> 'Submitted for peer review / published'
					</div>
				</div>
				<br />
				<br />
			</div>-->

	<div class="row-fluid">
		<div class="span12 contributeContainer hide ">
			<?= $allForms; ?>
		</div>
		<div class="row-fluid" id="app7020-file-list-container">
			<div class="span12">
				<?php echo $list_videos; ?>
			</div>
		</div>
	</div>

</div>