<div class="app7020FancyTreeFilterContainer" data-for="assingTopicsTree">
	<?php if (Yii::app()->user->hasFlash('error')) : ?>
        <div class="row-fluid">
            <div class="span12">
                   <div class='alert alert-error alert-compact text-left'>
                   <?= Yii::app()->user->getFlash('error'); ?>
               </div>
           </div>
       </div>
    <?php endif; ?>
	<div class="wrapper">
		<div class="selectActions">
			<p class="info"><?php echo YII::t('app7020', 'You have selected') ?> <span><strong>0</strong> <?php echo Yii::t('app7020', 'items'); ?></span></p>
			<p>
				<?php echo CHtml::link(Yii::t('app7020', 'Select all'), 'javascript:;', array('class' => 'select_all')); ?> |
				<?php echo CHtml::link(Yii::t('app7020', 'Unselect all'), 'javascript:;', array('class' => 'unselect_all')); ?>
			</p>
		</div>
		<div class="selectLegent">
			<i class="icon-yes"></i>
			<span><?php echo YII::t('app7020', 'Yes') ?></span>
			<i class="icon-no"></i>
			<span><?php echo YII::t('app7020', 'No') ?></span>
			<i class="icon-descandants"></i>
			<span><?php echo YII::t('app7020', 'Descandants') ?></span>
		</div>
		<div class="searchField">
			<div class="searchWrapper">
				<?php echo CHtml::textField('fancyTreeSearchInput', '', array('id' => 'fancyTreeSearchInput', 'placeholder' => 'Search')); ?>
				<i class="clear"></i>
				<i class="search"></i>
			</div>
		</div>
	</div>
</div>

<?php

echo CHtml::beginForm($url, 'post', array(
	'class' => 'ajax'
));
$this->widget('common.extensions.fancytree.FancyTree', array(
	'id' => 'assingTopicsTree',
	'treeData' => App7020TopicTree::buildFancytreeDataArray(false, true, false, false, true),
	'selectMode' => FancyTree::SELECT_MODE_DOCEBO2,
	'preSelected' => $preselected,
	'formFieldNames' => array(
	),
	// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
	'fancyOptions' => array(
		'checkbox' => true,
		'icons' => false,
		'filter' => array(
			'autoApply' => true,
			'counter' => true,
			'fuzzy' => true,
			'hideExpandedCounter' => true,
			'highlight' => true,
			'mode' => "hide"
		),
	),
	'htmlOptions' => array(
		'class' => '',
	),
	'filterInputFieldName' => 'fancytree-filter-input',
	'filterClearElemId' => 'fancytree-filter-clear-button',
	// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents		
	'eventHandlers' => array(
		'click' => 'function(event, data){
                        }',
		'blur' => 'function(event, data){
                        }',
		'select' => 'function(event, data) {
			$(".app7020FancyTreeFilterContainer[data-for=\'"+data.tree.$div.attr(\'id\')+"\']").find(\'.selectActions p.info strong\').text(data.tree.countSelected());
			var selectedNodes = [];
			$.each(data.tree.getSelectedNodes(true), function (index, value) {
				selectedNodes.push(value.key);
			});
                        }',
		'beforePreselection' => 'function(event,data) {
                        }',
		'afterPreselection' => 'function(event,data) {
			$(".app7020FancyTreeFilterContainer[data-for=\'"+data.tree.$div.attr(\'id\')+"\']").find(\'.selectActions p.info strong\').text(data.tree.countSelected());
                        }',
	),
));
?>

<div class="form-actions">
	<?php
	echo CHtml::hiddenField("experts", $expertId);
	if(isset($showPrevious)){
		echo CHtml::button(Yii::t('app7020', 'Previous'), array('class' => 'prev-dialog green'));
	}
	echo CHtml::submitButton(Yii::t('app7020', 'Confirm'), array('class' => 'save-dialog green'));
	echo CHtml::button(Yii::t('app7020', 'Cancel'), array('class' => 'close-dialog black'));
	?>
</div>

<?php echo CHtml::endForm(); ?>