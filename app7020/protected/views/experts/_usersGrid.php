<div class="userman-users-grid-wrapper">
	<?php
		$this->widget ('DoceboCGridView', array (
			'selectableRows' => 3,
			'ajaxType' => 'POST',
			'id' => $usersGridId,
			'dataProvider' => $dataProvider,
			
			// Elements which will trigger grid update
			'updateSelector' => "{page}, {sort}, .search-input-wrapper .perform-search, #userman-advanced-search .btn-search",
			
			'columns' => $selectedColumns,
			
			// non-quartz-items: because of a Global JS event listener in script.js (see there please)
			// which breaks normal Yii event chain
			'itemsCssClass' => 'items non-quartz-items',
			
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t ( 'standard', '_TOTAL' ),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			
			'beforeAjaxUpdate' => 'function(id,options) {UserMan.beforeUsersGridUpdate(id,options);}',
			
			'afterAjaxUpdate' => 'function(id, data) {
				UserMan.setGridColumnsVisibility();
				UserMan.afterUsersGridUpdate(id,data);
					$(\'a[rel="tooltip"]\').tooltip();
						$(\'.popover-action\').popover({
					placement: \'bottom\',
					html: true
				});
				$(document).controls();
			}',
			'selectionChanged' => 'function(id) {
				UserMan.gridSelectionChanged(id);
			}' 
		));
	?>
</div>

