<div class="main-section app7020DefaultStyles" id="app7020_Coach_Experts_Topics">
	<h1 class="title-bold back-button title">
		<a href="<?= Docebo::createApp7020Url('experts/index'); ?>"><?= Yii::t('standard', '_BACK'); ?></a>
		<span><?php echo Yii::t('app7020', 'Assign Topics'); ?></span>
	</h1>
	<div class="main-actions clearfix">
		<!--Action buttons-->
		<ul class="clearfix" data-bootstro-id="customActionButtons">
			<li>
				<div>
					<a href="javascript:void(0);" id="selectTopics">
						<span class="fa fa-book"></span>
						<span class="boxTitle">
							<span class="innerBoxTitle"><?= Yii::t('app7020', 'Assign Topics') ?></span>
						</span>
					</a>
				</div>
			</li>
		</ul>
		<!--End-->
	</div>
	<div class="bottom-section clearfix">
		<?php $this->renderPartial('_topicsList', array('listParams' => $listParams)); ?>
	</div>
</div>
