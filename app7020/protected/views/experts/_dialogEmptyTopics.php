<div class="app7020-dialogEmptyTopics">
	<div class="main-content">
		<p class="green"><?php echo Yii::t('app7020', 'Ops... you cant select any Expert to assign'); ?></p>
		<p class="green"><?php echo Yii::t('app7020', 'because you havent\'t created any topic yet'); ?></p>
		<p class="grey"><?php echo Yii::t('app7020', 'Create one or more topics first!'); ?></p>
	</div>
	<div class="action">
		<?php echo CHtml::link(Yii::t('app7020', 'Create Topics'), Docebo::createAppUrl('app7020:topics/index')) ?>
	</div>
</div>