<?php

$url = Docebo::createApp7020Url('axExpertsChannels/deleteExpert');
echo CHtml::beginForm($url, 'post', array(
    'class' => 'ajax',
    'id' => 'deleteExpertFormId'
));
?>

<div id="deleteExpertsFormContainer">
    <p class="topicInfo">Delete experts</p>
    <?= CHtml::checkBox('agreeCheck', false, array('id' => 'confirm')) ?>
    <?= CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'agreeCheck', array('style' => 'display: inline-block')) ?>
</div>
<div class="form-actions">
    <?php
    echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green'));
    echo CHtml::button('Cancel', array('class' => 'close-dialog'));
    ?>
</div>
<?php echo CHtml::hiddenField('idUser', (is_array($idUser) ? implode(',', $idUser) : $idUser)); ?>
<?php

echo CHtml::endForm();
?>