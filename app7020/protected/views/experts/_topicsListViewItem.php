<?php
if(count($data['names']) > 1){
	$data['names'][count($data['names'])-1] = "<strong>".$data['names'][count($data['names'])-1]."</strong>";
}
?>

<div class="singleListItem">
    <span class="content"><?php echo implode(" > ", $data['names']); ?></span>
    <span class="customControls">
		<i class="fa fa-times"></i>
    </span>
</div>