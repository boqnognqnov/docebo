<div class="app7020ContributeBanner">
	<i class="fa fa-times app7020-button-hideThis"></i>
	<div class="hintSection">
		<div class="app7020-hint app7020ContributeBannerHint">
			<h4>
				<?php echo Yii::t('app7020', 'Hit'); ?>
				<?php echo CHtml::link(Yii::t('app7020', 'Contribute,'), Docebo::createAppUrl('app7020:assets/index')); ?>
			</h4>
			<h4><?php echo Yii::t('app7020', 'share the Knowledge!'); ?></h4>
			<h3><?php echo Yii::t('app7020', '<span>Experts will review your assets and</span>'); ?></h3>
			<h3><?php echo Yii::t('app7020', '<span>make them trusted.</span> Be a contributor'); ?></h3>
			<h3><?php echo Yii::t('app7020', 'to the success of your organization!'); ?></h3>
		</div>
	</div>
	<div class="placeholder"></div>
</div>