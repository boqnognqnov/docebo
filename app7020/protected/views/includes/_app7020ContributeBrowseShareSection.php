<div id="contributeContainerTabs">
	<div class="row-fluid">
		<div class="span6 browse" id="app7020-pl-uploader-dropzone">
			<i class="fa fa-cloud-upload"></i>
			<p class="text">
				<?= Yii::t('app7020', 'Drag and drop <br> your file here or'); ?>
			</p>
			<div class="row-fluid" style="text-align: center;">
				<a href="javascript:;">
					<?php echo Yii::t('app7020', 'Browse'); ?>
				</a>
			</div>
		</div>
		<div class="span6 shareLink">
			<div id="shareLinkActiveForm">
				<div class="wrapper">
					<div class="linkInput">
						<?php echo CHtml::label(Yii::t('app7020', 'Create a knowledge asset <strong>by sharing link!</strong>'), 'asd'); ?>
						<?php echo CHtml::textField('sharedLink', '', array('class' => 'shareLinkField')); ?>
					</div>
					<div class="buttonsPlace">
						<?php echo CHtml::link(Yii::t('app7020', 'Share!'), 'javascript:;', array('class' => 'submitShareButton')); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>