<div class="app7020ContributeBrowseSection">
	<div class="inner-section-wrapper">
		<div class="item-section">
			<i class="fa fa-cloud-upload"></i>
			<p><?php echo Yii::t('app7020', 'Drop here'); ?></p>
			<p><?php echo Yii::t('app7020', 'your assets or'); ?></p>
			<?php echo CHtml::link(Yii::t('app7020', 'Browse'), Docebo::createAppUrl('app7020:assets/index')); ?>
		</div>
	</div>
</div>