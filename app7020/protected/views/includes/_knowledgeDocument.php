<div data-custom="contentFileItem">
	<a href="<?= Docebo::createApp7020Url('knowledgeLibrary/forceDownload', array('file' => $data->id)); ?>" target="_blank">
		<div class="iconType">
			<i class="app7020Type-<?= $data->getContentPrefix() ?>"></i>
		</div>
		<div class="content">
			<p class="head"><?= Yii::t('app7020', 'Download') ?>:</p>
			<p class="filename"><?= $data->originalFilename ?></p>
		</div>
		<div class="iconDownload">
			<i class="fa fa-download"></i>
		</div>
	</a>
</div>