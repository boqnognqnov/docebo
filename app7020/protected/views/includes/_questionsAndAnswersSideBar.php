<?php
// ****** GURUS CIRCLES ****** //
$gurusArray = array_fill(0, 6, array(
	'name' => 'Admin',
	'linkSource' => 'javascript:;',
	'status' => 'online',
	'imgSource' => 'https://placeholdit.imgix.net/~text?txtsize=14&txt=40%C3%9740&w=40&h=40')
);
$listParams = array(
	'arrayData' => $gurusArray,
	'maxVisibleItems' => 4,
	'tooltipPlacement' => 'left',
	'containerClass' => 'topVideosGuruList'
);
?>
<div class="app7020-container-sidebar" data-loid="<?php echo $loId; ?>">
	<div class="hint">
		<div class="app7020-hint app7020-QA-sidebar-hint">
			<h4>
				<?= Yii::t('app7020', 'The Experts <span>are here to</span>'); ?>
			</h4>
			<h4>
				<?= Yii::t('app7020', 'answer your questions!'); ?>
			</h4>
		</div>
	</div>
	<div class="circles">
		<?php $this->widget('common.widgets.GuruCirclesList', $listParams); ?>
	</div>
	<div class="form">
		<?= CHtml::link(Yii::t('app7020', 'Ask!'), 'javascript:;', array('class' => 'submitButton')); ?>
		<?= CHtml::textArea('addNewQuestion', ''); ?>
	</div>

	<div class="relatedTo">
		<div class="text">
			<p class="count"><span><span id="app7020-questions-count"><?= $questionCount ?></span> questions</span> and <span><span id="app7020-answers-count"><?= $answerCount ?></span> answers</span> related to:</p>
			<p class="title"><?php echo $loTitle; ?></p>
		</div>
		<?php $this->widget('common.widgets.app7020SingleSearchEngine', array('comboListViewId' => 'questionsComboListView')); ?>
	</div>
</div>
<?php $this->widget('common.widgets.ComboListView', $listViewParams); ?>