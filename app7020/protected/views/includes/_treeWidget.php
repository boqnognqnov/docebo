<div data-custom="channelWidgetContainer" <?php echo $isVisible || ShareApp7020Settings::isGlobalAllowPrivateAssets() == false ? 'style="display: block;"' : ''; ?>>
    <div class="widgetHeader">
        <p class="title">
			<?= Yii::t('app7020', 'Channels'); ?>
			<span class="required">*</span>
		</p>
        <p class="sub-title">
			<?php
			if ($isDisabledTree) {
				echo Yii::t('app7020', 'Channels are locked. To edit <u>report the problem to your administrator</u>');
			} else if (mb_strlen($subTitle) > 0) {
				echo $subTitle;
			} else {
				echo Yii::t('app7020', 'Add your asset in one or more channels');
			}
			?>
        </p>
    </div>
    <div class="widgetBody">
        <div class="search">
			<?php
			echo CHtml::textfield(
					Yii::t('standard', '_TITLE'), '', array('class' => 'channelWidgetSearch', 'placeholder' => Yii::t('app7020','Search channel...'), 'autocomplete' => 'off')
			);
			?>
            <i class="fa fa-search"></i>
        </div>
        <div class="tree">
			<div class="channel-wrapper">
				<div id="channnelsCheckList">
					<?php
					foreach ($checkboxes as $check_key => $check) {
						if (!$check['disabled']) {
							?>
							<div class="checkbox-item-row"> 

								<div class="checkbox-item"> 
									<?php
									$params = array('value' => $check_key, 'id' => 'channel_' . $check_key);
									if ($this->isDisabledTree) {
										$params['DISABLED'] = 'DISABLED';
									}
									if ($this->isReadOnlyTree) {
										$params['readonly'] = 'readonly';
									}
									if (!empty($check['is_pr'])) {
										$params['class'] = 'is_pr';
									}

									echo CHtml::checkBox('channnelsCheckList[]', $check['checked'], $params);
									if (!empty($check['is_pr'])) {
										echo CHtml::label($check['label'] . '<p class="sub-title">' . Yii::t('app7020', 'Requires Experts\' Peer Review') . '</p>', 'channel_' . $check_key);
									} else {
										echo CHtml::label($check['label'], 'channel_' . $check_key);
									}
									?> 
								</div> 
							</div> 
							<?php
						}
					}
					?>
				</div>

			</div>

        </div>
    </div>
</div>
<script>
	$('.checkbox-item.disabled').tooltip( );
</script>
