<h3 class="app7020SearchHeadline clearfix">
	<span class="searchLabel result"><?php echo Yii::t('app7020', 'Results'); ?></span>
	<span class="searchLabel noResult"><?php echo Yii::t('app7020', 'No results found'); ?></span>
	<span class="searchCount">
		<span class="countValue">0</span>
		<span class="countLabel"><?php echo Yii::t('app7020', 'Results'); ?></span>
	</span>
</h3>