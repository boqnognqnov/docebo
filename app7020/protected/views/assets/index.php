<?php
$criteria = array(0, 1, 3, 6, 7, 10, 15, 18, 20);
if ((bool) $hasRequest) {
	$criteria = array(3, 6, 10, 15, 18, 20);
}
?>

<div id="contributeMasterContainer" class="app7020DefaultStyles" data-ng-controller="asset" ng-init='init(<?php echo CHtml::encode(app7020Helpers::getPlOptions()) ?>, <?php echo (int) $hasRequest; ?><?php echo ($slackShare) ? ', "'.$slackShare.'"' : ''; ?>);
								assetsInit(<?php echo CHtml::encode(json_encode(App7020Assets::getUploadedFiles(array('proccessing_criteria' => $criteria, 'created' => 'week', 'idQuestion'=> $hasRequest)), 128)); ?>);' >

	<!--Got it Information-->
	<?php if (!App7020Helpers::actionGetGotItByAttributes('contribute_mobile_app_gotit')): ?>
		<div class="row-fluid">
			<div id="app7020-assets-gotId">
				<i class="fa fa-times app7020-button-hideThis vertical-center"></i>
				<div class="wrapper">
					<!--<div class="doceboIcon"></div>-->
					<div class="text">
						<p><?php echo Yii::t('app7020', 'Get the new Mobile LMS App!'); ?></p>
						<span><?php echo Yii::t('app7020', 'Create and Share Knowledge from your mobile device'); ?></span>
					</div>
					<div class="appLinks">
						<div class="downloadfromappstore"   onClick="window.open('https://itunes.apple.com/us/app/mobile-lms/id1121991330?ls=1&mt=8')" ></div>
						<div class="downloadfromgoogleplay" onClick="window.open('https://play.google.com/store/apps/details?id=com.docebo.mobilelms')"></div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

    <!--Two Tabs for Contrubute-->
    <div class="row-fluid" id="app7020-pl-uploader" >
        <div >
            <header>

            </header>
        </div>
        <div class="clear clearfix"></div>
        <div id="assetsContainerBoxes" ng-show="showUploadForm" ng-cloak>
            <input type="hidden" id="idQuestion" name="idQuestion" value="<?php echo $idQuestion; ?>"/>
            <div class="row-fluid">
                <div class="<?= (!PluginManager::isPluginActive('GoogleDriveApp')) ? "span6" : "span3"; ?> browse" id="app7020-pl-uploader-dropzone">
                    <i class="fa fa-cloud-upload"></i>
                    <p class="text">
						<?= Yii::t('app7020', 'Drag and drop <br> your file here or'); ?>
                    </p>
                    <div class="row-fluid" style="text-align: center;">
						<?php
						if ($showBrowseButton):
							?>
							<a id="app7020-browse_" href="javascript:;">
								<?php echo Yii::t('app7020', 'Browse'); ?>
							</a>
							<?php
						endif;
						?>
                    </div>
                </div>
				<?php if (PluginManager::isPluginActive('GoogleDriveApp')) { ?>
                <div class="span3 browse gdrive" id="app7020-pl-uploader-dropzone">
                    <img class="gd-logo" src="<?php echo Yii::app()->theme->baseUrl . '/img/logo_drive_48px.png'; ?>">
					<p class="gd-title">
						<?= Yii::t('app7020', 'Google Drive'); ?>
                    </p>
					<p class="text-gray">
						<?= Yii::t('app7020', 'Docs, Sheets, Slides, Drawings'); ?>
                    </p>
					<div class="row-fluid">
						<a href="javascript:;" lk-google-picker on-picked="onPicked(docs)" on-loaded="onLoaded()" on-cancel="onCancel()">
							<?php echo Yii::t('app7020', "Browse"); ?>
						</a>
					</div>
                </div>
				<?php } ?>
                <div class="span6 shareLink">
                    <div id="shareLinkActiveForm">
                        <div class="wrapper">
                            <div class="linkInput">
								<?php echo CHtml::label(Yii::t('app7020', 'Create a knowledge asset <strong>by sharing a link!</strong>'), 'asd'); ?>
								<?php echo CHtml::textField('sharedLink', '', array('placeholder' => Yii::t('app7020', 'Paste here the URL you want to share...'), 'class' => 'shareLinkField', 'data-ng-model' => 'shareLink', 'ng-keypress'=>'addLinkKeypress($event)')); ?>
                            </div>
							<div class="contributesharedescription">
								<?php echo Yii::t('app7020', '<p>You can share <span>any kind of URL and Video</span> like:</p>'); ?>
								<i class="youtubecontribute fa fa-youtube"></i>
									<i class="vimeocontribute fa fa-vimeo"></i>
									<img class="wistiacontribute" src="<?php echo Yii::app()->theme->baseUrl . '/img/wistia.png'; ?>">
							</div>
                            <div ng-if="showShareButton" class="buttonsPlace"> 
								<?php echo CHtml::link(Yii::t('app7020', 'Share!'), 'javascript:;', array('class' => 'submitShareButton', 'ng-click' => 'addLink()')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Assets list management -->
    <div class="row-fluid">
		<?php $this->widget('common.widgets.app7020NavigationMenu', array('links' => $navigationMenuLinks, 'oldStyle' => false, 'autoActive' => false)); ?>
    </div>
</div>
