<?php
$this->widget('common.widgets.DoceboAlert', array(
		'id' => 'gotItChannelFeatures',
		'type' => 'warning',
		'isGotIt' => false,
		'smb'=>$smb,
		'view'=>'doceboAlert/_splash',
		'ajaxURL'=> Docebo::createApp7020Url('assets/SplashSetGotIt'),
		'message' => 'You have selected a channel that requires Expertsís Peer '
			. 'Review before having your asset published. Your asset will be published '
			. 'in all the channels as soon as Expert have reviewed it.',
	)
);