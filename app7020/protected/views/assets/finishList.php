<div class="assetUploadedStatusesHeaderContainer clearfix" >
    <div class="pull-left">
        <span><?php echo Yii::t('app7020', 'This week contributions'); ?></span>
    </div>
    <div class="pull-right" ng-controller="clearList" ng-if="(assets|filter:filterAfterProccessing).length > 0" ng-cloak>
        <u ng-click="clearAll(1)"><?php echo Yii::t('app7020', 'Clear this list'); ?></u>
    </div>
    <div class="pull-right" ng-if="(assets|filter:filterAfterProccessing).length > 0" ng-cloak>
    	<?php $url = Docebo::createApp7020Url('channels/index', array('#' => '/pchannel/' . Yii::app()->user->id));	?>
        <?php echo CHtml::link(Yii::t('app7020', 'View all my contributions'), $url); ?>
    </div>
</div>


<div class="assetStatusesContainerEmptyItem finish"  ng-if="(assets | filter: filterAfterProccessing).length == 0" ng-cloak>  
    <p>
        <?php echo Yii::t('app7020', 'You don\'t have any active upload or knowledge asset to finalize and publish.') ?>
    </p>
</div>
<a 	class="assetFinishStatusesContainer" 
	data-ng-repeat="asset in assets | filter: filterAfterProccessing | orderBy: '-created'" 
	ng-cloak 
	href="<?= Docebo::createApp7020Url("", array("#" => '/assets/peerReview/{{asset.id}}')) ?>"
	data-asset-id="{{asset.id}}" 
	data-conversion-status="{{asset.conversion_status}}" 
	data-type="{{asset.contentType}}"> 
    <div class="typeIcon">
        <div class="thumbWrapper">
            <img ng-src="{{asset.thumb}}" />
        </div>

    </div>
    <div class="contentWrapper">
        <!-- CONVERSION_STATUS_FINISHED -->
        <div data-conversion-status="10">
            <!--ago ng angularMoment-->
            <p class="time" >{{asset.created}}</p>
            <p>
                <?php echo Yii::t('app7020', 'Asset'); ?>
                "<u>{{asset.originalFilename}}</u>"
                <?php echo Yii::t('app7020', '<strong>has been submitted for the Expert\'s peer review.</strong> You\'ll be notified as soon as it is approved.') ?>
            </p>
        </div>
        <!-- CONVERSION_STATUS_INREVIEW -->
        <div data-conversion-status="15">
            <!--ago ng angularMoment-->
            <p class="time" >{{asset.created}}</p>
            <p>
                <?php echo Yii::t('app7020', 'Asset'); ?>
                "<u>{{asset.originalFilename}}</u>"
                <?php echo Yii::t('app7020', '<strong>has been submitted for the Expert\'s peer review.</strong> You\'ll be notified as soon as it is approved.') ?>
            </p>
        </div>
        <!-- CONVERSION_STATUS_UNPUBLISH -->
        <div data-conversion-status="18">
            <!--ago ng angularMoment-->
            <p class="time" >{{asset.created}}</p>
            <p>
                <?php echo Yii::t('app7020', 'Asset'); ?>
                "<u>{{asset.originalFilename}}</u>"
                <?php echo Yii::t('app7020', '<strong>has been submitted for the Expert\'s peer review.</strong> You\'ll be notified as soon as it is approved.') ?>
            </p>
        </div>
    </div>
    <div class="controllIcon">
        <i class="fa fa-angle-right"></i>
    </div>
</a>