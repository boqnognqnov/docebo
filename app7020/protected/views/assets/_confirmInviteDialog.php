<div id="confirmInviteToWatchContainer" data-idContent="<?php echo $asset->id;?>" data-redirectFlag="<?php echo $redirect;?>">
	<div class="confirmInviteIcon">
		<i class="fa fa-check-circle-o"></i>

	</div>
	<div class="confirmInviteText">
		
		<?php echo Yii::t('app7020', "Your asset "); ?>
		<span class="blue">
			<?php echo $asset->title ?>
		</span>
		<?php echo Yii::t('app7020', " has been published!"); ?>
	</div>


	<div class="form-actions">
		<?php
		//    echo CHtml::hiddenField('deleteNodeId', '');
		echo CHtml::button(Yii::t('app7020', 'Invite to watch'), array('class' => 'save-dialog green'));
		echo CHtml::button(Yii::t('app7020', 'No, thanks'), array('class' => 'close-dialog'));
		?>
	</div>
</div>