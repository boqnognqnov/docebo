<div class="assetUploadedStatusesHeaderContainer clearfix" >
    <div class="pull-left">
        <span><?php echo Yii::t('app7020', 'This week contributions'); ?></span>
    </div>
    <div class="pull-right" ng-controller="clearList" ng-if="(assets|filter:filterPublishProccessing).length > 0" ng-cloak>
        <u ng-click="clearAll(2)"><?php echo Yii::t('app7020', 'Clear this list'); ?></u>
    </div>
    <div class="pull-right" ng-if="(assets|filter:filterPublishProccessing).length > 0" ng-cloak>
    	<?php
        $urlChannel = Docebo::createApp7020Url('channels/index', array("#" => "/pchannel/" . Yii::app()->user->id));
        if(Yii::app()->legacyWrapper->hydraFrontendEnabled()){
            $urlChannel = '/share/personalChannel/'. Yii::app()->user->id;
        }

    	   echo CHtml::link(Yii::t('app7020', 'View all my contributions'), $urlChannel);
    	?>
    </div>
</div>


<div class="assetStatusesContainerEmptyItem finish"  ng-if="(assets | filter: filterPublishProccessing).length == 0" ng-cloak>  
    <p>
        <?php echo Yii::t('app7020', 'You don\'t have any active upload or knowledge asset to finalize and publish.') ?>
    </p>
</div>



<a  class="assetPublishStatusesContainer" 
	data-ng-repeat="asset in assets | filter: filterPublishProccessing | orderBy: '-created'" 
	ng-cloak 
    <?php
       $url = Docebo::createApp7020AssetsViewUrl("{{asset.id}}");
       if(Yii::app()->legacyWrapper->hydraFrontendEnabled()){
           $url = '/share/asset/view/'. "{{asset.id}}";
       }
    ?>
    href="<?= $url ?>"
    target="_parent"
	data-asset-id="{{asset.id}}" 
	data-conversion-status="{{asset.conversion_status}}" 
	data-type="{{asset.contentType}}">
    <div class="typeIcon">
        <div class="thumbWrapper">
            <img ng-src="{{asset.thumb}}" />
        </div>

    </div>
    <div class="contentWrapper">
        <!-- CONVERSION_STATUS_APPROVED -->
        <div data-conversion-status="20">
            <!--ago ng angularMoment-->
            <p class="time" >{{asset.created}}</p>
            <p>
                <?php echo Yii::t('app7020', 'Asset'); ?>
                "<u>{{asset.originalFilename}}</u>"
                <?php echo Yii::t('app7020', '<strong>has been published.</strong>') ?>
            </p>
        </div>
    </div>
    <div class="controllIcon">
        <div class="viewButton"><?php echo Yii::t('app7020', 'View'); ?></div>
    </div>
</a>