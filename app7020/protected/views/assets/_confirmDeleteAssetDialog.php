<div id="confirmDeleteAssetDialog" data-content-id="<?php echo $asset->id; ?>" ng-controller="app7020DeleteAsset" ng-init="init(<?php echo $asset->id; ?>);">
	<?php if(!$showName): ?>
		<p class="topicInfo"><?php echo Yii::t('app7020', 'Are you sure you want to delete this Asset?'); ?></p>
	<?php endif; ?>
	<?php if($showName): ?>
		<p class="topicInfo"><?php echo Yii::t('app7020', 'Delete'); ?>: <?php echo $asset->originalFilename; ?></p>
	<?php endif; ?>
		
    <?php echo CHtml::checkBox('agreeCheck', false, array('id' => 'confirm')) ?>
    <?php echo CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'confirm', array('style' => 'display: inline-block')) ?>
</div>
<div class="form-actions">
    <?php
		echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green', 'id' => 'confirmDeleteAsset'));
		echo CHtml::button('Cancel', array('class' => 'close-dialog'));
    ?>
</div>