<div class="app7020AssetModal" data-asset-id="<?php echo $asset->id; ?>" data-conversion-status="<?php echo $asset->conversion_status; ?>" data-type="<?php echo $asset->contentType; ?>">
	<?php $form = $this->beginWidget('CActiveForm', array()); ?>
	<?php
	$this->widget('common.widgets.DoceboAlert', array(
		'id' => 'gotItChannelFeatures',
		'type' => 'warning',
		'isGotIt' => true,
		'message' => 'You have selected a channel that requires Experts’s Peer '
		. 'Review before having your asset published. Your asset will be published '
		. 'in all the channels as soon as Expert have reviewed it.',
			)
	);
	if (!empty($asset->request)) {
		$this->widget('common.widgets.DoceboAlert', array(
			'id' => 'warningThatAssetIsRequestedBySomeone',
			'type' => 'warning',
			'isGotIt' => false,
			'message' => 'You cannot change the visibility of this asset. '
			. 'An expert has uploaded this material to satisfy an user request. '
			. 'No further changes can be made to this asset.'
				)
		);
	}
 
	?>
	<div id="error-container">

	</div>
	<!--HEADER INFORMATION-->
	<?php if (!$editItemMode): ?>
		<div class="row-fluid">

			<div class="span12 contribute_file_header">
				<div class="info">
					<!-- IF is ShareLink show link ELSE show asset filename -->
					<?php if ($asset->contentType == App7020Assets::CONTENT_TYPE_LINKS): ?>
						<div class="file">
							<strong><?php echo Yii::t('app7020', 'Shared link'); ?>:</strong>
							<span class="name">
								<?php echo $asset->originalFilename; ?>
							</span>
						</div>
					<?php else: ?>
						<div class="file">
							<strong><?php echo Yii::t('app7020', 'File'); ?>:</strong>
							<span class="name"><?php echo $asset->originalFilename ?></span>
						</div>
					<?php endif; ?>
					<span class="required"><span>*</span><?php echo Yii::t('app7020', 'Mandatory fields'); ?></span>
				</div>
			</div>
		</div>
	<?php endif; ?>


	<!--CENTRAL CONTENT-->
	<div class="row-fluid">
		<div class="<?php echo empty($asset->request) ? 'span8' : 'span12'; ?>">
			<div class="leftSide">
				<?php if ($editItemMode) : ?>
					<p class="text-label">
						<span class="title"><?php echo Yii::t('app7020', 'Details'); ?><strong>*</strong></span>
						<span class="subTitle">
							<?php
							echo Yii::t(
									'app7020', 'Define {type} title and description', array('{type}' => $asset->contentType == App7020Assets::CONTENT_TYPE_VIDEO ? "video's" : "asset's")
							);
							?>
						</span>
					</p>
				<?php endif; ?>
				<!--THUMBNAILS + DETAILS-->
				<div class="row-fluid">


					<div class="span12">
						<div class="details">
							<?php if (!$editItemMode) : ?>
								<p class="text-label">
									<span class="title"><?php echo Yii::t('app7020', 'Details'); ?><strong>*</strong></span>
									<span class="subTitle"><?php echo Yii::t('app7020', 'Asset thumbnail, title and description'); ?></span>
								</p>
							<?php endif; ?>

							<?php echo $form->textField($asset, 'title', array('placeholder' => Yii::t('app7020', 'Add title...'))); ?>
							<?php echo $form->textArea($asset, 'description', array('placeholder' => Yii::t('app7020', 'Add description...'))); ?>
								<?php
								if (!empty($asset->request)) {
									 echo $form->hiddenField($asset, 'is_private', array('value' => '0'));  
								}
								?>
						</div>
					</div>

					<?php if ($editItemMode && $asset->contentType != App7020Assets::CONTENT_TYPE_VIDEO) : ?>
					<?php else : ?>
						<div class="images-container">
							<div class="thumbnails">
								<div class="chooseThumb"><?php echo Yii::t('app7020', 'Select auto-generated thumbnail'); ?></div>
								<?php
								if ($asset->contentType != App7020Assets::CONTENT_TYPE_VIDEO) {
									echo $asset->getPreviewThumbnailImage(TRUE, 'small');
								}
								else {
									echo CHtml::radioButtonList('thumbs', App7020VideoProceeder::getActiveThumb($asset->id), App7020VideoProceeder::getAssetThumbs($asset->id), array('separator' => false));
								}
								?>
							</div>
						</div>
					<?php endif; ?>
				</div>

				<!--TAGS-->
				<div class="row-fluid">
					<div class="span12">
						<div class="tags">
							<p class="text-label">
								<span class="title">
									<?php echo Yii::t('app7020', 'Tags'); ?>
								</span>
								<span class="subTitle">
									<?php echo Yii::t('app7020', 'Assign different tags to improve asset search'); ?>
								</span>
							</p>
							<?php
							$tags = $asset->getAllTagsAsString();
							$tagsPost = Yii::app()->request->getParam('hidden-undefined', false);
							if (!empty($tagsPost)) {
								$tags .= ',' . $tagsPost;
							}
							?>
							<docebo-tags name="tags" data-id="tags" data-asset-id="<?php echo $asset->id; ?>" data-tags="<?php echo $tags; ?>"></docebo-tags>
						</div>
					</div>
				</div>

				<!--ALLOW DOWNLOAD-->
				<?php if ($asset->contentType != App7020Assets::CONTENT_TYPE_LINKS 
						&& $asset->contentType != App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS 
						&& $asset->contentType != App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS
						&& $asset->contentType != App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES
						&& !$editItemMode): ?>
					<div class="row-fluid">
						<div class="span12">
							<div class="allowDownload">
								<p class="text-label">
									<span class="title">
										<?php echo Yii::t('app7020', 'Asset download'); ?>
									</span>
									<span class="subTitle">
										<?php echo Yii::t('app7020', 'You will be also able to set up the asset download in the asset custom settings'); ?>
									</span>
								</p>
								<?php echo CHtml::checkBox('assetDownload', $checkDownload); ?>
								<label for="assetDownload">
									<?php echo Yii::t('app7020', 'Allow users to download asset file'); ?> (<?php echo $asset->originalFilename; ?>)
								</label>
							</div>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>

		<?php if (empty($asset->request)) { ?>

			<div class="span4">
				<div class="rightSide">
					<?php
					$isDisabledTree = false;
					if ($asset->defineContentAdminAccess() && $asset->conversion_status >= App7020Assets::CONVERSION_STATUS_FINISHED) {
						$isDisabledTree = true;
					}
					if (Yii::app()->user->isGodAdmin) {
						$isDisabledTree = false;
					}

					if (ShareApp7020Settings::isGlobalAllowPrivateAssets()):
						?>

						<div data-custom="channel-public-private" >
							<div class="widgetHeader">
								<p class="title">
									<?= Yii::t('app7020', 'Asset visibility '); ?><span class="required">*</span>
								</p>
								<p class="sub-title">
									<?php
									if ($isDisabledTree) {
										echo Yii::t('app7020', 'Visibility is locked. To edit <u>report the problem to your administrator</u>');
									} else {
										echo Yii::t('app7020', 'Define visibility for this asset');
									}
									?>
								</p>
							</div>
							<div class="widget-container">
								<?php
								echo $form->radioButtonList($asset, 'is_private', array(
									1 => '<div class="app7020-radio-container">'
									. '		<div class="app7020-visibility-row">'
									. '			<i class="fa fa-lock"></i> '
									. Yii::t('app7020', 'Private')
									. '		</div>'
									. '		<div class="app7020-visibility-row">'
									. '			<p>' . Yii::t('app7020', 'This asset will be visible only to me and to people I decide to invite') . '</p>'
									. '		</div>'
									. '</div>',
									0 => '<div class="app7020-radio-container">'
									. '		<div class="app7020-visibility-row">'
									. '			<i class="fa fa-globe"></i> '
									. Yii::t('app7020', 'Public')
									. '		</div>'
									. '		<div class="app7020-visibility-row">'
									. '			<p>' . Yii::t('app7020', 'Select a channel where you\'d like to publish your asset') . '</p>'
									. '		</div>'
									. '</div>')
										, array(
									'disabled' => $isDisabledTree,
									'template' => '<div class="checkbox-item-row"><div class="checkbox-item">{input}{label}</div></div>',
									"separator" => ''
										)
								);
								?>
							</div>
						</div>

						<?php
					endif;
					?>

					<?php
					$this->widget('common.widgets.ChannelCheckboxList', array('asset' => $asset, 'editmode' => $editItemMode, 'isDisabledTree' => $isDisabledTree));
					?>
				</div>
			</div>
			<?php
		}
		?>
	</div>

	<!--FOOTER BUTTONS-->
	<div class="row-fluid">
		<div class="footerButtons clearfix">
			<div class="pull-left">
				<?php echo CHtml::link(Yii::t('app7020', 'Cancel upload'), 'javascript:;', array('class' => 'red cancelUpload')); ?>
			</div>
			<div class="pull-right">
				<?php if ($editItemMode) : ?>
					<?php echo CHtml::link(Yii::t('app7020', 'SAVE'), 'javascript:;', array('class' => 'green save', 'ng-click' => 'submitAsset()')); ?>
				<?php else : ?>
					<?php
					echo CHtml::link(Yii::t('app7020', 'Publish Now'), 'javascript:;', array('class' => 'green publishNow', 'style' => 'display: block;'));
					echo CHtml::link(Yii::t('app7020', 'Submit for peer review'), 'javascript:;', array('class' => 'green SubmitForReview', 'style' => 'display: none;'));
					?>
				<?php endif; ?>

			</div>
		</div>
	</div>

	<?php $this->endWidget(); ?>
</div>