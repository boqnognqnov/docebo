<div class="assetStatusesContainerEmptyItem active" ng-if="(assets | filter: filterOnProccessing).length == 0" ng-cloak>
    <p>
		<?php echo Yii::t('app7020', 'You don\'t have any active upload or knowledge asset to finalize and publish.') ?>
		<?php echo CHtml::link(Yii::t('app7020', 'View all my contributions'), Docebo::createApp7020Url('channels/index', array("#" => "/pchannel/" . Yii::app()->user->id))); ?>
    </p>
</div>

<div class="assetActiveStatusesContainer" data-ng-repeat="asset in assets| filter: filterOnProccessing | orderBy: '-created'" data-asset-id="{{asset.id}}" data-conversion-status="{{asset.conversion_status}}" data-type="{{asset.contentType}}" ng-cloak>
    <div class="typeIcon" ng-switch="asset.contentType">
        <i ng-switch-default class="fa fa-file-o"></i>
        <i ng-switch-when="15" class="fa fa-link"></i>
        <i ng-switch-when="1"  class="fa fa-video-camera"></i>
    </div>

    <div class="contentWrapper">

        <!-- CONVERSION_STATUS_PENDING -->
        <div data-conversion-status="0">
			<p>
				<?php echo Yii::t('app7020', 'Uploading <strong class="green">{{asset.percent}}%</strong>'); ?>
			</p>
			<div class="progress"><div ng-style="{'width':  asset.percent +'%' }  "></div></div>
        </div>

        <!-- CONVERSION_STATUS_FILESTORAGE -->
        <div data-conversion-status="1">
			<div ng-if="asset.contentType != '15' && asset.contentType!='16'">
				<?php echo Yii::t('app7020', 'Uploading <strong class="green">{{asset.percent}}%</strong>'); ?>
				<a href="javascript:;" data-cancel-upload><?php echo Yii::t('app7020', 'Cancel upload'); ?></a>
				<!--                (<u>{{asset.originalFilename}}</u>)-->
				</p>
				<div class="progress"><div ng-style=" {'width':  asset.percent +'%' }"></div></div>
			</div>
			<div ng-if="asset.contentType == '15' && asset.contentType!='16'">
				<p>
					<b><?php echo Yii::t('app7020', 'Converting...'); ?></b> <?php echo Yii::t('app7020', 'this operation may take few minutes.'); ?>
					(<u>{{asset.originalFilename}}</u>)
				</p>
				<div class="progress green"><div style="width: 0%;"></div></div>
			</div>
			<div ng-if="asset.contentType == '16'">
				<p ng-switch="asset.contentType">
					<strong class="green"><?php echo Yii::t('app7020', 'Upload completed!'); ?> </strong>
					<?php echo Yii::t('app7020', 'Add details to'); ?>
					<a href="javascript:;" class="open-asset-modal" data-asset-modal>
						<?php echo Yii::t('app7020', 'finalize publishing'); ?></a>
					({{asset.originalFilename}})
				</p>
				<div class="progress green">
					<div style="width: 100%;"></div>

        </div>
			</div>
        </div>

        <!-- CONVERSION_STATUS_S3 -->
        <div data-conversion-status="3">
            <p>
				<b><?php echo Yii::t('app7020', 'Converting...'); ?></b> <?php echo Yii::t('app7020', 'this operation may take few minutes.'); ?>
				(<u>{{asset.originalFilename}}</u>)
			</p>
            <div class="progress green"><div style="width: 0%;"></div></div>

        </div>

        <!-- CONVERSION_STATUS_SNS_SUCCESS -->
        <div data-conversion-status="6">
            <p ng-switch="asset.contentType">
                <strong class="green" ng-switch-default><?php echo Yii::t('app7020', 'Upload completed!'); ?> </strong>
                <strong class="green" ng-switch-when="15"><?php echo Yii::t('app7020', 'Link added!'); ?></strong>

				<?php echo Yii::t('app7020', 'Add details to'); ?>
                <a href="javascript:;" class="open-asset-modal" data-asset-modal>
					<?php echo Yii::t('app7020', 'finalize publishing'); ?></a>
				({{asset.originalFilename}})
            </p>
            <div class="progress green">
				<div style="width: 100%;"></div>
					
			</div>
        </div>

        <!-- CONVERSION_STATUS_SNS_WARNING -->
        <div data-conversion-status="7">
            <p>
				<?php echo Yii::t('app7020', '<strong class="red">Upload failed!</strong>'); ?>
				(<u>{{asset.originalFilename}}</u>)
            </p>
            <div class="progress red"><div style="width: 100%;"></div></div>
        </div>

        <!-- CONVERSION_STATUS_SNS_ERROR -->
        <div data-conversion-status="8">
            <p>
				<?php echo Yii::t('app7020', '<strong class="red">Upload failed!</strong>'); ?>
				(<u>{{asset.originalFilename}}</u>)
            </p>
            <div class="progress red"><div style="width: 100%;"></div></div>
        </div>
        <div data-conversion-status="9">
            <p>
				<?php echo Yii::t('app7020', '<strong class="red">TEST ERROR LENGTH</strong>'); ?>
				(<u>{{asset.originalFilename}}</u>)
            </p>
            <div class="progress red"><div style="width: 100%;"></div></div>
    </div>
    </div>
    <div class="controllIcon" ng-if="asset.conversion_status!=8" >
        <div class="controllIcon">
            <?php
            $this->widget('common.widgets.DropdownControls', array(
                'module' => 'assetsDropdown',
                'arrayData' => array(
                    array('data' => 'asset-modal', 'text' => 'Finalize & Publish'),
                )
            ));
            ?>
        </div>
	</div>
</div>