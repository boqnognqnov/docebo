<div id="modalContainer" class="row-fluid" data-idContent="<?php echo $contentId;?>" data-redirectFlag="<?php echo $redirect;?>">
    <!--
    <div class="span6 playerView" data-custom="treeWidgetContainer">
        <div class="widgetHeader visible">
            <p class="title">Invite people by topic</p>
            <p class="sub-title">The invitation will be send to all the people that can view the topic(s) </p>
        </div>

        <?php /*
        $this->renderPartial('app7020.protected.views.includes._treeWidget', array('preSelected' => "", 'isDisabledTree' => false));
        */ ?>
    </div>
    -->
    <div class="span6 app7020_invited" data-custom="treeWidgetContainer">
        <div class="widgetHeader visible">
            <p class="title">Invite specific people</p>
            <p class="sub-title">Start typing the name of the people you want to invite</p>
        </div>
        <div class="outer">
            <input type="text" class="form-control typeahead" placeholder="<?php echo Yii::t('app7020','Type here') ?> . . ." />
        </div>
        <div class="app7020_invited_header">
            <div>
                <?php echo Yii::t('app7020', 'You have invited'); ?> <span id="app7020-invited-count"> 0 </span> <span><?php echo Yii::t('app7020', 'people'); ?></span>
            </div>
            <div>

                <?php echo CHtml::link('Remove all', 'javascript:;', array('class' => '', 'id' => 'app7020-remove-all-invited')); ?>
            </div>
        </div>
        <div class="app7020_users" id="app7020_invited_users">
            <!--
            <div class="user-entry">
                <div class="media">
                    <a class="pull-left" href="javascript:">

                            <div class="app7020_signature" data-custom="circle-avatar">
                                <img class="media-object" alt="64x64" src="<?php echo $data['avatar'];?>">
                            </div>

                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $data['name'];?></h4>
                        <?php echo $data['username'];?>

                    </div>
                </div>
                <div class="app7020_delete_invited">
                    <i class="fa fa-times"></i>
                </div>
            </div>
            -->
        </div>
    </div>

</div>
<div class="footer-counter">
    <i class="fa fa-paper-plane-o"></i> <strong> <?php echo Yii::t('app7020','Your invitation will effectively reach') ?>: <span> <span id="reachedPeople">0</span> <?php echo Yii::t('app7020','people') ?></span></strong>
</div>
<footer id="app7020-controlls">
    <?php
    echo CHtml::link('Invite', 'javascript:;', array('class' => 'save-dialog green'));
    echo CHtml::link('Close', 'javascript:;', array('class' => 'close-dialog black'));
    ?>
</footer>