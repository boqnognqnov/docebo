<?php
$url = Docebo::createApp7020Url('inviteToWatch/axInviteUsers');
echo CHtml::beginForm($url, 'post', array(
	'class' => 'form_step1',
	'id' => 'inviteToWatchForm',
	'name' => 'inviteToWatchForm',
));
?>
<div id="invite_to_watch"  ng-controller="inviteController" ng-cloak>
	<div class="row-fluid" ng-init="setCurrentAsset(<?php echo $idContent; ?>)" ng-hide="showForm()" >
		<div class="title-header clearfix" >
			<span class="title pull-left" ng-hide="invitedUsers > 0" >
				<?= Yii::t('app7020', 'People'); ?>
			</span >
			<span class="counter pull-left" ng-hide="invitedUsers == 0" >
				<?= Yii::t('app7020', 'You are inviting'); ?> <strong>
					&nbsp;{{invitedUsers}} {{(invitedUsers == 1) ? "<?= Yii::t("app7020","person") ?>" : "<?= Yii::t("app7020","people") ?>"}}&nbsp;&nbsp;
				</strong>
			</span >
			<span ng-click="unhandleAll(<?php echo $idContent; ?>)" class="clear-all pull-right"   ng-hide="invitedUsers == 0">
				<?= Yii::t('app7020', 'Clear all'); ?>
			</span >
		</div >
		<div class="selection-wraper" >
			<ul id="selection" >
				<li class="person" ng-repeat="(key,item) in prepare" >
					{{item.name}}
					<i ng-click="unhandleSelection(key, <?php echo $idContent; ?>)" class="fa fa-times"></i >
				</li >
				<li class="input-holder" >
					<input id="selector" type="text" ng-model="model" placeholder="{{prompt}}" ng-change="change(<?php echo $idContent; ?>);" ng-model-options="{ debounce: 1000 }" ng-keydown="keypress($event,<?php echo $idContent; ?>);" />
				</li >
			</ul >
		</div >
		<div class="title-header" >

			<span class="title" ng-hide="!model.length" >
				<?= Yii::t("webapp", "Search result for"); ?> <strong><u> {{model}} </u></strong>
			</span >


			<span class="title" ng-hide="model.length && errors.length" >
				<?= Yii::t('app7020', 'Suggestions') ?>
			</span >

			<span class="title" ng-if="errors.length" >
				<p ng-repeat="error in errors">
					{{error}}
				</p>
			</span >

		</div >
		<div ng-if="req == 0" class="loading text-center">
			<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/ajax-loader10.gif" alt="">
		</div>
		<div ng-if="req == 1" class="invited_items" >
			<div  ng-if="!errors.length" class="item" ng-repeat="item in suggestions| orderObjectBy: 'typeId'"   style="cursor:pointer" ng-class="{active:isCurrent($index)}" ng-mouseenter="setCurrent($index)" >
				<div data-disabled="{{item.invited}}" >
					<div class="user-entry" >
						<!-- channel layout -->
						<div ng-if="item.typeId == 2" class="media" >
							<div class="pull-left" >
								<div class="app7020_signature  channel-icon" data-custom="circle-avatar" style="background:{{item.avatar['background']}}" >
									<i class="{{item.avatar['icon']}}" style="color:{{item.avatar['color']}}"></i >
								</div >
							</div >
							<div class="media-body pull-left" >
								<h4 class="media-heading">{{item.name}}
									<strong>{{item.type}}  - {{item.users}} <?= Yii::t("app7020","people") ?> </strong >
								</h4 >
							</div >
							<div class="add-to-list pull-right" >
								<i ng-if="item.invited == 0" ng-click="handleSelection(item, <?php echo $idContent; ?>)" class="fa fa-plus-circle add-to-watch"></i >
								<i ng-if="item.invited == 1" class="fa fa-check-circle"></i >
								<span ng-if="item.invited == 2" class="status">
									<?= Yii::t('app7020', 'Invited on') ?> <strong>  {{item.date}}</strong>
								</span >
							</div >
						</div >

						<!-- User layout -->
						<div ng-if = "item.typeId == 1" class="media" >
							<div class="pull-left" >
								<div ng-if="item.avatar != ''" class="app7020_signature" data-custom="circle-avatar" >
									<img class="media-object" alt="64x64" ng-src="{{item.avatar}}" >
								</div >
 								
								<div class="circle-avatar" ng-if="item.avatar == '' && item.firstname != '' && item.lastname != ''">
									{{ item.firstname | limitTo:1 | uppercase }}{{ item.lastname | limitTo:1 | uppercase }}
								</div>
								<div class="circle-avatar" ng-if="item.avatar == '' && item.firstname != '' && item.lastname == ''">
									{{ item.firstname | limitTo:1 | uppercase }}
								</div>
								<div class="circle-avatar" ng-if="item.avatar == '' && item.firstname == '' && item.lastname != ''">
									{{ item.lastname | limitTo:1 | uppercase }}
								</div>
								<div class="circle-avatar" ng-if="item.avatar == '' && item.firstname == '' && item.lastname == ''">
									{{ item.username | limitTo:1 | uppercase }}
								</div>
							</div >
							<div class="media-body pull-left" >
								<h4 class="media-heading">{{item.name}}</h4 >
								{{item.username}}
							</div >
							<div class="add-to-list pull-right" >
								<i ng-if="item.invited == 0" ng-click="handleSelection(item,<?php echo $idContent; ?>)" class="fa fa-plus-circle add-to-watch"></i >
								<i ng-if="item.invited == 1" class="fa fa-check-circle"></i >
								<span ng-if="item.invited == 2" class="status">
									<?= Yii::t('app7020', 'Invited on') ?> <strong>  {{item.date}}</strong>
								</span >

							</div >
						</div >
					</div >
				</div >
			</div >

		</div>
	</div>



	<div id="modalContainer" class="row-fluid"  data-idContent="<?php echo $idContent; ?>" >
		<div class="  playerView loading" ng-hide="showLoading()" >
			<div class="app7020_first_row text-center">
				<i class="fa fa-paper-plane-o"></i>
			</div>
			<div class="app7020_second_row text-center">
				<?php echo Yii::t('app7020', 'Sending invitation') . '...'; ?>
			</div >
			<div class="app7020_fourth_row text-center">
				<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/ajax-loader1.gif" alt="">
			</div>
			<div class="app7020_third_row text-center">
				&nbsp;
			</div>
		</div>

		<div class="  playerView complete" ng-hide="showComplete()" >
			<div class="app7020_first_row text-center">
				<i class="fa fa-check-circle-o"></i>
			</div>
			<div class="app7020_second_row text-center">
				<?php echo Yii::t('app7020', 'Invitation sent to'); ?>
			</div >
			<div class="app7020_third_row text-center">
				{{invitedPeople}}<?php echo " " . Yii::t('app7020', 'people'); ?>
			</div>
		</div>

	</div>
	<?php
	echo CHtml::endForm();
	?>
	<footer id="app7020-controlls" ng-hide="showForm()" >
		<? if (App7020Assets::hasAdminRights($idContent)): ?>
		<a class="manage-invitations pull-left" href="<?php echo Docebo::createApp7020Url('analytics/index'); ?>&id=<?php echo $idContent; ?>"><?php echo Yii::t('app7020', 'Manage sent invitations'); ?></a>
		<? endif; ?>
		<?php
		echo CHtml::button(Yii::t('coaching','Invite'), array('class' => 'save-dialog btn', 'ng-click' => 'inviteUsers(' . $idContent . ',' . Yii::app()->user->idst . ')'));
		echo CHtml::link(Yii::t('app7020','Close'), 'javascript:;', array('class' => 'close-dialog btn', 'ng-click' => 'close()'));
		?>
	</footer>

</div>