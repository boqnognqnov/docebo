<div id="modalContainer" class="row-fluid" data-idContent="<?php echo $contentId;?>" data-redirect="<?php echo $redirect;?>">
	<div class="span12 app7020_invited small" data-custom="treeWidgetContainer">
		<div class="widgetHeader">
			<p class="title">Invite specific people</p>
			<p class="sub-title">Start typing the name of the people you want to invite</p>
		</div>
		<div class="outer">
			<input type="text" class="form-control typeahead" placeholder="<?php echo Yii::t('app7020','Type here') ?> . . ." />
		</div>
		<div class="app7020_invited_header">
			<div>
				<?php echo Yii::t('app7020', 'You have invited'); ?> <span id="app7020-invited-count"> 0 </span> <span><?php echo Yii::t('app7020', 'people'); ?></span>
			</div>
			<div>

				<?php echo CHtml::link('Remove all', 'javascript:;', array('class' => '', 'id' => 'app7020-remove-all-invited')); ?>
			</div>
		</div>
		<div class="app7020_users" id="app7020_invited_users">

		</div>
		<!--
		<div id="pop-hint">
			<div class="app7020-hint app7020-knowledgeLibrary-askGuruTab" id="gurusHint">
				<h4>
					<?= Yii::t('app7020', 'The Experts <span>are here</span>'); ?>
				</h4>
				<br />
				<h4>
					<?= Yii::t('app7020', '<span>to</span> answer your questions!'); ?>
				</h4>

			</div>
		</div>
		-->
	</div>

</div>
<footer id="app7020-controlls">
	<?php
	echo CHtml::link('Invite', 'javascript:;', array('class' => 'save-dialog green'));
	echo CHtml::link('Close', 'javascript:;', array('class' => 'close-dialog black'));
	?>
</footer>