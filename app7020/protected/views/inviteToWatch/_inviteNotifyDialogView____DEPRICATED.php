<?php ?>
<div id="modalContainer" class="row-fluid"  data-idContent="<?php echo $contentId;?>" data-redirectFlag="<?php echo $redirect;?>">
	<div class="span12 playerView loading">
		<div class="app7020_first_row text-center">
			<i class="fa fa-paper-plane-o"></i>
		</div>
		<div class="app7020_second_row text-center">
			<?php echo Yii::t('app7020', 'Sending invitation').'...'; ?>
		</div >
		<div class="app7020_fourth_row text-center">
			<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/ajax-loader1.gif" alt="">
		</div>
		<div class="app7020_third_row text-center">
			&nbsp;
		</div>
	</div>

    <div class="span12 playerView complete">
		<div class="app7020_first_row text-center">
		<i class="fa fa-check-circle-o"></i>
		</div>
		<div class="app7020_second_row text-center">
        <?php echo Yii::t('app7020', 'Invitation sent to'); ?>
		</div >
		<div class="app7020_third_row text-center">
		<?php echo $invitedPeople." ".Yii::t('app7020', 'people'); ?>
		</div>
		
    </div>

</div>
