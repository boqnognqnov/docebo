<div class="title-header" >
	<span class="title" ng-hide="(items | filter:{invited:3}).length" >
		People
	</span >
	<span class="counter" ng-hide="!(items | filter:{invited:3}).length" >
		You are inviting <strong> {{(items| filter:{invited:3,type:2}).length}} {{(items| filter:{invited:3,type:2}).length == 1 ? "'person  " : "' persons  "}} </strong >
	</span >
	<span ng-click="unhandleAll(items)" class="clear-all pull-right" ng-hide="!(items | filter:{invited:3}).length" >
		Clear all 
	</span >
</div >
<div class="selection-wraper" >
	<ul id="selection" >
		<li class="person" ng-repeat="item in items| filter:{invited:3,chanTrack:0}" >
			{{ (item.value).length > 1 ? item.value : item.username}}
			<i ng-click="unhandleSelection(item, items)" class="fa fa-times"></i >
		</li >
		<li class="input-holder" >
			<input id="selector" type="text" ng-model="model" placeholder={{prompt}} ng-keydown="selected = false"/ >
		</li >
	</ul >
</div >
<div class="title-header" >
	<span class="title" ng-hide="model.length" >
		Suggestions
	</span >
	<span class="title" ng-hide="!model.length" >
		Search result for  {{model}}
	</span >
</div >
<input type="hidden" name="items" value={{items}}/ >
<div class="items" >
	<div class="item" ng-repeat="item in items|  filter:model  track by $index" ng-if=" $index < 5 && item.type == 2 && item.invited != 2"  style="cursor:pointer" ng-class="{active:isCurrent($index)}" ng-mouseenter="setCurrent($index)" >
		<div data-disabled={{item.invited}} >
			<div class="user-entry" >
				<div class="media" >
					<div class="pull-left" >
						<div class="app7020_signature" data-custom="circle-avatar" >
							<img class="media-object" alt="64x64" ng-src="{{item.avatar}}" >
						</div >
					</div >
					<div class="media-body pull-left" >
						<h4 class="media-heading">{{item.value}}</h4 >
						{{item.username}}
					</div >
					<div class="add-to-list pull-right" >
						<i ng-if="item.invited == 0" ng-click="handleSelection(item, items)" class="fa fa-plus-circle add-to-watch"></i >
						<i ng-if="item.invited == 3" class="fa fa-check-circle"></i >
						<span ng-if="item.invited == 1" class="status">Invited <strong> on  {{item.date}}</strong></span >
					</div >
				</div >
			</div >
		</div >
	</div >
	<div class="item" ng-repeat="item in items" ng-if="item.type == 1 && item.invited == 0 && (item.username).length >0"  style="cursor:pointer" ng-class="{active:isCurrent($index)}" ng-mouseenter="setCurrent($index)" >
		<div data-disabled={{item.invited}} >
			<div class="user-entry" >
				<div class="media" >
					<div class="pull-left" >
						<div class="app7020_signature  channel-icon" data-custom="circle-avatar" style="background:{{item.ibgr}} >
							 <i class="fa {{item.avatar}}" style="color:{{item.icolor}}"></i >
						</div >
					</div >
					<div class="media-body pull-left" >
						<h4 class="media-heading">{{item.value}}
							<strong>Channel  - {{item.username}} people </strong >
						</h4 >
					</div >
					<div class="add-to-list pull-right" >
						<i ng-if="item.invited == 0" ng-click="handleSelection(item, items)" class="fa fa-plus-circle add-to-watch"></i >
						<i ng-if="item.invited == 3" class="fa fa-check-circle"></i >
						<span ng-if="item.invited == 1" class="status">Invited <strong>on  {{item.date}}</strong></span >
					</div >
				</div >
			</div >
		</div >
	</div >
</div>