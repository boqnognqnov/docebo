<?php
$url = Docebo::createApp7020Url('axChannels/axEditChannel');
$idForEdit = (Yii::app()->request->getParam('id', false) ? '&id=' . Yii::app()->request->getParam('id', '') : '');
$nextStep = ($data['step'] == 'clone_step2')? 'clone_finish' : 'step_visibility';
$prevStep = ($data['step'] == 'clone_step2')? 'clone' : 'initial_create';
$submitText = ($data['step'] == 'clone_step2')? Yii::t('app7020','Clone') : Yii::t('app7020','Next');
echo CHtml::beginForm($url . $idForEdit, 'post', array(
	'class' => 'ajax',
	'id' => 'editChannelForm'
));
?>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'Edit channel'); ?></div>
<script src="<?php echo App7020Helpers::getAssetsUrl() . '/js/libs/farbtastic/jquery.tools.min.js' ?>"></script>
<div class="channelLookAndFell step2" data-id="<?php echo $session['id']; ?>">
	<h2><?php echo Yii::t('app7020', 'Channel look & feel'); ?></h2>

	<div id="color-picker-section" class="row-fluid clearfix">
		<div class="label-icon text-left">
			<div class="label-icon-title"><?php echo Yii::t('label', 'Icon') ?>:</div>
			<?php $this->renderPartial('app7020.protected.views.channelsManagement.modals._iconPicker',array('session'=>$session)); ?>
		</div>

		<div class="label-color clearfix text-center">
			<div class="label-color-title"><?php echo Yii::t('app7020', 'Icon color') ?>:</div>
			<div class="colors">
				<?php $this->renderPartial('app7020.protected.views.channelsManagement.modals._colorView', array('type' => 'color','session'=>$session)); ?>
			</div>
		</div>

		<div class="label-color clearfix text-right">
			<div class="label-color-title"><?php echo Yii::t('app7020', 'Icon background color') ?>:</div>
			<div class="colors">
				<?php $this->renderPartial('app7020.protected.views.channelsManagement.modals._colorView', array('type' => 'background-color','session'=>$session)); ?>
			</div>
		</div>
	</div>

	<div class="preview-title"><?php echo Yii::t('app7020', 'Preview'); ?></div>
	<div class="preview-content">
		<div class="row-fluid">
			<div class="span2 preview-tile">
				<i class="fa <?php echo (isset($session['icon'])) ? $session['icon'] : 'fa-500px' ?>  fa-inverse"></i>
			</div>
			<div class="span10" style="margin-left: 0px;">
				<div class="preview-content-title">
					<?php echo (!empty($session['langs'][$currentLanguage]['title']) ? $session['langs'][$currentLanguage]['title'] : $session['langs'][$defaultBrowseLangCode]['title']); ?>
				</div>
				<div class="preview-content-description">
					<?php echo ((!empty($session['langs'][$currentLanguage]['description'])) ? $session['langs'][$currentLanguage]['description'] : $session['langs'][$defaultBrowseLangCode]['description']); ?>
				</div>
			</div>
		</div>
	</div>

	<!--TAGS-->
	<div class="sort-section">
		<div class="row-fluid">
			<div class="span12">
				<div class="tags">
					<p class="text-label">
						<span class="title"><?php echo Yii::t('app7020', 'Sort channel items by'); ?></span>
						<span class="subTitle"><?php echo Yii::t('app7020', 'Assign different tags to improve asset search'); ?></span>
					</p>
					<div id="selected-sorting-fields">
						<?php
						$sort = $session['sort'];
						$sortPost = Yii::app()->request->getParam('hidden-undefined', false);
						if (!empty($sortPost)) {
							$sort .= ',' . $sortPost;
						}
						$fieldsToRestore = array(
							'name_asc'=>Yii::t('app7020','Item name (Ascending)'),
							'name_desc'=>Yii::t('app7020','Item name (Descending)'),
							'date_asc'=>Yii::t('app7020','Creation Date (Ascending)'),
							'date_desc'=>Yii::t('app7020','Creation Date (Descending)')
						);
						?>
						<select name="hidden-sort" multiple="multiple" placeholder = "<?php echo Yii::t('app7020', 'Add sorting here(separated by comma'); ?>">
							<? if(count($fieldsToRestore)): ?>
								<? foreach($fieldsToRestore as $key=>$value):
									if($key == $sort):
									?>
									<option selected="selected" class="selected" value="<?=$key?>"><?=$value?></option>
									<? endif; ?>
								<? endforeach; ?>
							<? endif; ?>
						</select>
					</div>
					<!--
					<div class="tag-widget-container">
						<div class="tagsContainer"></div>
						<?php //echo CHtml::textField('sort', '', array('data-included' => $sort, 'placeholder' => Yii::t('app7020', 'Add sorting here(separated by comma)'))) ?>
  					</div>
					-->
				</div>
			</div>
		</div>
	</div>
	<?php if ($data['step'] == 'clone_step2') { ?>
		<div class="clone-info">
			<div class="row-fluid">
				<h2>
					<?php echo Yii::t('app7020','All channels properties will also be cloned'); ?>
					<span class="subTitle"><?php echo Yii::t("app7020","You'll be able to edit this options later"); ?></span>
				</h2>
				<ul>
					<li><strong><?php echo Yii::t('app7020', 'Visibility'); ?>:</strong><?php echo App7020ChannelVisibility::getVisibilityText($session['id']); ?></li>
					<li><strong><?php echo Yii::t('app7020', 'Upload Permissions'); ?>:</strong><?php echo App7020Channels::translatePermission($session['permissions']); ?></li>
					<li><strong><?php echo Yii::t('app7020', 'Experts'); ?>:</strong><?php echo App7020ChannelExperts::getExpertCountsByChannelId($session['id']); ?></li>
				</ul>
			</div>
		</div>
	<?php } ?>

</div>





<!-- Define buttons to be moved in the footer by D2 -->
<div class="form-actions">
	<?php
	echo CHtml::hiddenField('nextStep', $nextStep);
	echo CHtml::hiddenField('prevStep', $prevStep);
	echo CHtml::button(Yii::t('standard', '_PREV'), array(
		'class' => 'prev-button',
		'name' => 'prev_button'
	));
	echo CHtml::submitButton($submitText, array('class' => 'save-dialog'));
	echo CHtml::button(Yii::t('standard','_CANCEL'), array('class' => 'close-dialog'));
	?>
</div>

<?php
echo CHtml::endForm();
?>


<?php
$fcbkAutocompleteUrl = Docebo::createApp7020Url('axChannels/sortAutoComplete');
?>

<script>
	var $fcbkAutocompleteUrl = <?= json_encode($fcbkAutocompleteUrl) ?>;

	$(function(){

		var selectElem = $('select[name="hidden-sort"]');

		selectElem.fcbkcomplete({
			json_url: $fcbkAutocompleteUrl,
			addontab: false, // <==buggy
			width: '98.5%',
			cache: false,
			complete_text: '',
			maxshownitems: 8,
			maxitems:1,
			input_min_size: -1,
			input_name: 'sort-input',
			filter_selected: true,
			onselect : function(){
				//console.log(arguments);
			}
		});

	});
</script>

