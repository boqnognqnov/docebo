<?php $icon = (isset($session['icon'])) ? $session['icon'] : ''  ?>

<button id="channelIconPicker" class="btn btn-default" data-search="true" data-search-text="<?=Yii::t('standard', '_SEARCH')?>..."
		data-placement="right" data-iconset="fontawesome" data-icon="<?php echo $icon ;?>" name="channelIcon"></button>