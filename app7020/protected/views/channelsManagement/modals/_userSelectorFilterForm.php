<style>
	label {
		display: inline-block;
		/*font-weight: bold;*/
	}

	.grey-wrapper{
		border-bottom: 1px solid #E4E6E5;
		border-top: 1px solid #E4E6E5;
		background: none repeat scroll 0 0 #F1F3F2;
		padding:1px 0;
		margin-bottom:12px
	}

	.wb{
		border-bottom: 1px solid #fff;
		border-top: 1px solid #fff;
		padding:13px 14px;
	}

	.grey-wrapper .selection * {
		vertical-align: top;
	}

	.selection {
		width: 33%;
		display: inline-block;
		vertical-align: top;
	}

	.comment {
		font-size: 0.9em;
		color: grey;
	}

	.tab-content {
		margin-left: 210px;
	}

	.gridItemsContainer {
		clear: none !important;
	}

	ul#selector-tabs {
		height: 500px;
	}
	ul#selector-tabs a{
		border: 0;
	}
	ul#selector-tabs a:hover{
		background-color: #666666 !important;
	}
	div.tab-content{
		height: 500px;
		overflow-y: auto;
		overflow-x: hidden;
	}
	.tab-content .filter-wrapper {
		border-bottom: 1px solid #E4E6E5;
		border-top: 1px solid #E4E6E5;
		padding: 7px 0px 7px 10px;
		background: none repeat scroll 0 0 #F1F3F2;
		position: relative;
	}
	.labelBlock{
		display: inline-block;
		width: 88%;
	}

	.icon-select-node-yes,
	.icon-select-node-no,
	.icon-select-node-descendants {
		height: 19px !important;
		width: 19px !important;
		display: inline-block !important;
		vertical-align: middle !important;
		margin: 0 5px 0 8px !important;

	}
	.tab-content a{
		color: #0465AC;
		text-decoration: underline;
		font-weight: 600;
	}
	.icon-select-node-yes {
		background: url(../themes/spt/images/icons_elements.png) -166px -200px no-repeat !important;;
	}

	.icon-select-node-no {
		background: url(../themes/spt/images/icons_elements.png) -143px -200px no-repeat !important;;
	}

	.icon-select-node-descendants {
		background: url(../themes/spt/images/icons_elements.png) -189px -200px no-repeat !important;;
	}
	.i-sprite.is-search{
		margin-left: -25px;
	}
	ul.ui-fancytree.fancytree-container{
		width: 100%;
	}
	.filter-wrapper .span5.legend{
		margin-left: -5px;
		padding-top: 8px;
	}
	.filter-wrapper .span3>input{
		position: absolute;
		right: 7px;
		top:11px;
		width: 120px;
		padding-right: 20px;
	}
	.filter-wrapper .span3{
		margin-left: 0;
	}
	.filter-wrapper .span3 #fancytree-filter-clear-button{
		margin-top: 10px;
		margin-right: -15px;
	}
	.span6 #users-grid-search-clear-button,.span6  #groups-grid-search-clear-button{
		position: absolute;
		right: 30px;
		top: 14px;

	}
	.filter-wrapper .span6:last-child{
		text-align: right;
		padding-right: 5px;
	}

	@media (min-width: 980px) {
		.customLoading{
			width: 900px;
			margin-left: -450px;
		}
	}

	@media (min-width: 769px) and (max-width: 979px){
		.customLoading{
			width: 720px;
			margin-left: -360px;
		}
		.labelBlock{
			width: 85%;
		}
		.selection{
			width: 32%;
		}
		.filter-wrapper .span5.legend{
			padding-left: 6px;
		}
	}
	@media (min-width: 1200px) {
		.filter-wrapper .span3 #fancytree-filter-clear-button{
			margin-right: -20px;
		}
	}
</style>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'Edit Channel Visibility'); ?></div>
<p>
	<?php echo Yii::t('standard', 'Define {strong}who can view{/strong} this channel',array('{strong}'=>'<strong>','{/strong}'=>'</strong>')) ?>
</p>
<?php
$error = Yii::app()->request->getParam('error', false);
$visibilityFlag = App7020ChannelVisibility::isExistVisibility($_SESSION['app7020ChannelData']['id']);
if($error) { ?>
	<div style="color:red;"><?php echo Yii::t('standard', 'Please choose at least one group or branch');?></div>
<?php } ?>

<div class="grey-wrapper">
	<div class="wb">
		<div class="selection">
			<?= CHtml::radioButton('visibility_type', $visibilityFlag == 1 || $_SESSION['app7020ChannelData']['id'] == 0, array(
				'class' => 'course-assign-mode',
				'value' => App7020ChannelVisibility::TYPE_PUBLIC,
				'id' => App7020ChannelVisibility::TYPE_PUBLIC,
			)) ?>
			<div class="labelBlock">
				<?= CHtml::label(Yii::t('app7020', 'All groups and branches'), App7020ChannelVisibility::TYPE_PUBLIC) ?>
				<br/><span class="comment"></span>
			</div>
		</div>
		<div class="selection">
			<?= CHtml::radioButton('visibility_type', ($visibilityFlag == 0 && $_SESSION['app7020ChannelData']['id'] != 0) || $error, array(
				'class' => 'course-assign-mode',
				'value' => App7020ChannelVisibility::TYPE_SELECTION,
				'id' => App7020ChannelVisibility::TYPE_SELECTION,
			)) ?>
			<div class="labelBlock">
				<?= CHtml::label(Yii::t('manmenu', 'Select groups and branches'), App7020ChannelVisibility::TYPE_SELECTION) ?>
				<br/><span class="comment"></span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script>

	$('.grey-wrapper input').styler();

	//$('#public-styler').trigger('click');

	function changeState(){
		if($(this).prop('id') == '<?=App7020ChannelVisibility::TYPE_SELECTION?>'){
			$('.tabbable.main-selector-area').show();
		}else{
			$('.tabbable.main-selector-area').hide();
		}
	}

	$(function () {
		$('.selection>input[type="radio"]').on('change',changeState);
		$('.selection>input[type="radio"]:checked').trigger('change');
	});


</script>