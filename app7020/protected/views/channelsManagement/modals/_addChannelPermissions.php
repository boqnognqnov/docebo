<?php
$url = Docebo::createApp7020Url('axChannels/axEditChannel');
$idForEdit = (Yii::app()->request->getParam('id', false) ? '&id='.Yii::app()->request->getParam('id', '') : '');
$active=(isset($session['permissions'])) ? $session['permissions'] : 1;
$list = array(
	'1' => Yii::t('app7020','Everyone').'<span class="subData">'.Yii::t('app7020','Everyone can upload and publish assets to this channels.').'</span>',
	'2' => Yii::t('app7020','Everyone, with Experts peer review').'<span class="subData">'.Yii::t('app7020','Everyone can upload assets to this channels. Experts peer review is needed before the assets are published.').'</span>',
	'3' => Yii::t('app7020','Experts only').'<span class="subData">'.Yii::t('app7020','Experts only can upload and publish assets to this channels.').'</span>',
);
echo CHtml::beginForm($url.$idForEdit, 'post', array(
	'class' => 'ajax',
	'id' => 'editChannelForm'
));
?>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'Edit Channel Permissions'); ?></div>
	<div class="row-fluid">
		<?php echo CHtml::radioButtonList('permissions', $active, $list, array('separator' => '')); ?>
	</div>


	<!-- Define buttons to be moved in the footer by D2 -->
	<div class="form-actions">
		<?php
		echo CHtml::hiddenField('nextStep', 'step_finish');
		echo CHtml::hiddenField('prevStep', 'step_visibility');
		echo CHtml::button(Yii::t('standard', '_PREV'), array(
			'class' => 'prev-button',
			'name' => 'prev_button',
		));
		echo CHtml::submitButton(Yii::t('standard','_NEXT'), array('class' => 'save-dialog'));
		echo CHtml::button(Yii::t('standard','_CANCEL'), array('class' => 'close-dialog'));
		?>
	</div>

<?php
// Must be after div.form-actions!
echo CHtml::endForm();
?>