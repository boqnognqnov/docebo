<?php
$url = Docebo::createApp7020Url('axChannels/axEditChannel');
$idForEdit = (Yii::app()->request->getParam('id', false) ? '&id='.Yii::app()->request->getParam('id', '') : '');
$nextStep = ($data['step'] == 'clone')?'clone_step2':'step2';
echo CHtml::beginForm($url.$idForEdit, 'post', array(
	'class' => 'ajax form_step1',
	'id' => 'editChannelForm'
));
?>
<?php
if(!$session["id"]) { ?>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'New Channel'); ?></div>
<?php }  else { ?>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'Edit channel'); ?></div>
<?php } ?>
<div class="language_content step1" data-id="<?php echo $session['id']; ?>">
	<div class="lang-wrapper clearfix row-fluid">
		<div class="channelLangBox span6">
			<p><?= Yii::t('app7020', 'Assign name and description for each language'); ?></p>
			<?php
			echo CHtml::listBox(
				'languages', $defaultBrowseLangCode, array('0' => Yii::t('adminrules', '_SELECT_LANG_TO_ASSIGN')) + $activeLanguages, array('size' => 1, 'id' => 'channelLangSelect', 'class' => 'languageSelector')
			);
			?>
		</div>
		<div class="channelLangInfo span6">
			<p class="allLangs"><?= Yii::t('app7020', 'All languages'); ?>:<strong><?= count($activeLanguages); ?></strong></p>
			<p class="assignedLang"><?= Yii::t('app7020', 'Personalized based on langauage/s'); ?>:<strong>0</strong></p>
			<p class="requiredLang"><?= Yii::t('app7020', "Default language channel name is required"); ?></p>
			<p class="maxLetters"><?= Yii::t('app7020', 'Text is too long (maximum is 255 characters)'); ?></p>
		</div>
	</div>
	<div class="row-fluid">
		<div class="channelTitleBox span12">
			<p><?php echo  Yii::t('app7020', 'Channel name'); ?></p>
			<!--<p><?php echo  Yii::t('app7020', 'Channel name'); ?> (<span class="current-lang-selected"><?php echo Lang::getNameByBrowserCode($defaultBrowseLangCode); ?></span>)</p>-->
			<?php echo CHtml::textfield('channelTitle', '', array('disabled' => 'disabled') ); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="channelDescription span12">
			<p><?php echo  Yii::t('app7020', 'Channel description'); ?></p>
			<!--<p><?php echo  Yii::t('app7020', 'Channel description'); ?> (<span class="current-lang-selected"><?php echo Lang::getNameByBrowserCode($defaultBrowseLangCode); ?></span>)</p>-->
			<?php echo CHtml::textArea( 'channelDescription', '', array() ); ?>
		</div>
	</div>
</div>

<!-- Define buttons to be moved in the footer by D2 -->
<div class="form-actions">
	<?php
	$inst = ($data['step'] == 'clone') ? ' (2)' : '';
	foreach($activeLanguages as $key => $value){
		$lang = $key;
		$dbTitle = '';
		$dbDescription = '';
		if(isset($session['langs'])){
			foreach ($session['langs'] as $key => $value){
				if($key == $lang){
					$dbTitle = $value['title'];
					$dbDescription = $value['description'];
				}
			}
			echo CHtml::hiddenField('langArr['.$lang.'][title]', $dbTitle);
			echo CHtml::hiddenField('langArr['.$lang.'][description]', $dbDescription);
		} else {
			if(isset($session['trans'])){
				foreach ($session['trans'] as $v){
					if($v['lang'] == $lang){
						$dbTitle = $v['name'].$inst;
						$dbDescription = $v['description'];
					}
				}
			}
			echo CHtml::hiddenField('langArr['.$key.'][title]', $dbTitle);
			echo CHtml::hiddenField('langArr['.$key.'][description]', $dbDescription);
		}


	}
	echo CHtml::hiddenField('currentTarget', $data['target']);
	echo CHtml::hiddenField('checkParentLevel', $data['thisLevel']);
	echo CHtml::hiddenField('nextStep', $nextStep);
	echo CHtml::submitButton(Yii::t('standard','_NEXT'), array('class' => 'save-dialog', 'data-default-lang' => $defaultBrowseLangCode));
	echo CHtml::button(Yii::t('standard','_CANCEL'), array('class' => 'close-dialog'));
	?>
</div>

<?php
// Must be after div.form-actions!
echo CHtml::endForm();
?>