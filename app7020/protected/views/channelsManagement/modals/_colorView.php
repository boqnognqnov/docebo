<?php
	$defaultColor = ($type == 'color' ? '#ffffff' : '#333333');
?>
<div class="color-item">
	<?php
		$color = ($type=='color') ? '#fff' : '#333';
		$activeColor = (isset($session['bgrcolor']))? $session['bgrcolor'] : $color;
		if($type=='color'){
			$activeColor = (isset($session['color']))? $session['color'] : $color;
		}

	?>
	<!--Type variable is (string) "color" | "background-color" -->
	<div class="wrap" data-type="<?php echo $type; ?>">
		
		<?php echo CHtml::textfield($type, $activeColor, array('class' => 'color')); ?>
		
		<div class="tooltip">
			<div class="colorpicker"></div>
		</div>
		
		<div class="color-preview-wrap">
			<div class="color-preview" style="background-color: <?php echo $activeColor; ?>;"></div>
		</div>
	</div>
</div>