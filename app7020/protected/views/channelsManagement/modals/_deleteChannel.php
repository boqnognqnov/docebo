<?php
$url = Docebo::createApp7020Url('axChannels/axEditChannel');
$idForEdit = (Yii::app()->request->getParam('id', false) ? '&id='.Yii::app()->request->getParam('id', '') : '');
echo CHtml::beginForm($url.$idForEdit, 'post', array(
	'class' => 'ajax delete',
	'id' => 'editChannelForm'
));

$allAssets = App7020ChannelAssets::model()->findAllByAttributes(array('idChannel'=>$data['id']));
$title = (isset($session['trans'][$currentLanguage]['name'])) ? $session['trans'][$currentLanguage]['name'] : $session['trans'][$defaultBrowseLangCode]['name'];
if (!$title) $title = Yii::t('app7020', 'Channel') . " # " . $data["id"];
$countOthers = (count($data['assets'])>0) ? $data['assets'][0]['allAssetsInChannel']-$data['assets'][0]['lonelyAssets'] : count($allAssets);
foreach($data["assets"] as $ass) {
	$countUnique += $ass["lonelyAssets"];
}
if(!$countUnique) {
	$countUnique = 0;
}
	$countOthers = $data['assets'][0]['allAssetsInChannel'] - $countUnique;
?>
<script>
    jQuery('body').on('focus', 'input.channelWidgetSearch', function () {
        jQuery(function ($) {
            $('#channnelsCheckList').lookingfor({
                input: $('input.channelWidgetSearch'),
                items: 'div'
            });
        });
    });
</script>
<div class="deleteChannel">
	<?php if($countUnique>0)  {?>
	<div class="noticeWrapper">
		<div class="noticeMessage">
			<p><?php echo Yii::t('app7020', 'In order to delete this channel {b}you must move{/b} all those assets that are assigned to this channel only to a {b}new exhisting channel{/b}', array('{b}' => '<strong>', '{/b}' => '</strong>')); ?></p>
		</div>
	</div>
	<?php }?>

	<div class="designChannelWrapper">
		<p class="head"><strong><?php echo $title; ?></strong> <?php echo Yii::t('app7020', 'Channel'); ?></p>
		<div class="designChannelInner">
			<div class="left">
				<p><?php echo $countUnique; ?></p>
				<span><?php echo Yii::t('app7020', 'Assets assigned to this channel only') ;?></span>
			</div>
			<div class="right">
				<p><?php echo $countOthers; ?></p>
				<span><?php echo Yii::t('app7020', 'Assets assigned also to other channels') ;?></span>
			</div>
		</div>
	</div>

	<?php if($countUnique>0)  {?>
	<div class="channelListtingWrapper">
		<p class="head"><?php echo Yii::t('app7020', '{b}Delete{/b} this channel and {b}move {count} assets{/b} to:', array('{b}' => '<strong>', '{/b}' => '</strong>', '{count}' => $countUnique)); ?></p>
		<?php $this->widget('common.widgets.ChannelCheckboxList',array('excludeChannels'=>$data['id'])); ?>
	</div>
	<?php } ?>
	<div class="footerSection">
		<?php
		echo CHtml::checkBox('confirmDelete', false);
		echo CHtml::label(Yii::t('app7020', 'Yes I want to proceed!'), 'confirmDelete', array());
		?>
	</div>
	<div class="form-actions">
		<?php
			echo CHtml::hiddenField('nextStep', 'delete_finish');
			echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'save-dialog app7020-button green'));
			echo CHtml::button(Yii::t('standard','_CANCEL'), array('class' => 'close-dialog'));
		?>
	</div>
</div>
<?php echo CHtml::endForm(); ?>