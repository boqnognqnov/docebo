<?php
$url = Docebo::createApp7020Url('axChannels/axEditChannel');
$idForEdit = (Yii::app()->request->getParam('id', false) ? '&id=' . Yii::app()->request->getParam('id', '') : '');
echo CHtml::beginForm($url . $idForEdit, 'POST', array(
	'id' => 'topic-wizard-step-finish-form',
	'name' => 'topic-wizard-step-finish-form',
	'class' => 'ajax  form-horizontal',
));

echo CHtml::hiddenField('nextStep', 'step_save');
$prev = (!empty($data['channel']['predefined_uid'])) ? 'step_visibility' : 'step_permissions';
echo CHtml::hiddenField('prevStep', $prev);
?>
<div id="step-title" style="display:none"><?= Yii::t('app7020', 'Edit Channel'); ?></div>
<div class="wrapper clearfix step3 row-fluid">

	<div class="span3">
		<div class="left success" style="margin-top: 40px;">
			<div class="copy-success-icon"></div>
			<?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
		</div>
	</div>

	<div class="span9">
		<p class="intro"><?php echo Yii::t('app7020', "What's next?"); ?></p>
		<div class="row-fluid row-with-border">
			<p><strong><?php echo Yii::t('app7020', 'Add a new channel'); ?></strong></p>
			<div class="btnWrapper">
				<?php
				echo CHtml::submitButton(Yii::t('app7020', 'Add a new channel'), array(
					'name' => 'save_new',
					'class' => 'btn btn-docebo green big',
				));
				?>
			</div>
		</div>
		<?php
		if (PluginManager::isPluginActive('Share7020App') && empty($data['channel']['predefined_uid'])) {
			?>
			<div class="row-fluid row-with-border">
				<p><?php echo Yii::t('app7020', '<strong>Assign experts</strong> to this channel'); ?></p>
				<div class="btnWrapper">
					<?php
					echo CHtml::submitButton(Yii::t('app7020', 'Assign Experts'), array(
						'name' => 'save_assign',
						'class' => 'btn btn-docebo green big',
					));
					?>
				</div>
			</div>
			<?php
		}
		if (PluginManager::isPluginActive('Share7020App') && empty($data['channel']['predefined_uid'])) {
			?>
			<div class="row-fluid row-with-border">
				<p><?php echo Yii::t('app7020', '<strong>Clone</strong> this channel and its properties'); ?></p>
				<div class="btnWrapper">
					<?php
					echo CHtml::submitButton(Yii::t('app7020', 'Clone'), array(
						'name' => 'save_clone',
						'class' => 'btn btn-docebo green big',
					));
					?>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>

<div class="form-actions">
	<?php
	echo CHtml::button(Yii::t('standard', '_PREV'), array(
		'class' => 'prev-button step-finish-prev-button',
		'name' => 'prev_button',
	));
	echo CHtml::submitButton(Yii::t('app7020', 'Finish'), array(
		'class' => 'btn btn-docebo green big',
		'name' => 'finish_step'
	));
	?>
</div>