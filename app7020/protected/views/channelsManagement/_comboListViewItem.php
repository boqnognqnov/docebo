<div class="singleListItem">
	<span class="content"><?php echo $data['firstname'] .' '. $data['lastname'] . ' ('.trim($data['userid'],'/').')'; ?></span>
    <span class="customControls">
        <?php
		$title = '<i class="fa fa-times"></i>';
		echo CHtml::link($title, Docebo::createApp7020Url('axChannels/unAssignExperts', array(
			'idChannel' => $data['idChannel'],
			'idExperts' => $data['idExpert'],
		)), array(
				'class' => 'open-dialog',
				'data-dialog-id' => 'app7020-unassign-experts-dialog',
				'data-dialog-class' => 'app7020-unassign-experts-dialog',
				'data-dialog-title' => Yii::t('app7020', 'Unassign'),
			)
		);
		?>
    </span>
</div>