<div class="main-actions clearfix">
	<!--<h3 class="title-bold"><?php echo Yii::t('app7020', 'Assign Experts'); ?></h3>-->
	<h3 class="title-bold back-button">
		<a href="<?= Docebo::createApp7020Url('channelsManagement/index'); ?>"><?= Yii::t('standard', '_BACK'); ?></a>
		<?php echo Yii::t('app7020', 'Assign Experts'); ?>
	</h3>
	<!--Action buttons-->
	<ul class="clearfix" data-bootstro-id="customActionButtons">
		<li>
			<div>
				<?php
				$idChannel = Yii::app()->request->getParam('id', false);
				$title = '<span class="fa fa-user-plus"></span>'
					. '<span class="boxTitle">'
					. '<span class="innerBoxTitle">'
					. Yii::t('app7020', 'Assign Experts')
					. '</span>'
					. '</span>';

				echo CHtml::link($title, Docebo::createApp7020Url('axChannels/axAssignExperts', array(
					'idChannel' => $idChannel,
				)), array(
						'class' => 'open-dialog',
						'data-dialog-id' => 'app7020-assign-experts-dialog',
						'data-dialog-class' => 'users-selector app7020-assign-experts-dialog',
						'data-dialog-title' => Yii::t('app7020', 'Assign Experts'),
						'alt' => Yii::t('app7020', 'Create and assign Experts to Channels'),
					)
				);
				?>
			</div>
		</li>
	</ul>
	<!--End-->

	<!--Tooltip box-->
	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
	<!--End-->
</div>
