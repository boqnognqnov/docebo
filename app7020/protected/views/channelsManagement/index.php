<div id="app7020ChannelsContainer" class="app7020DefaultStyles">
    <div class="main-actions clearfix">
        <h3 class="title-bold"><?php echo Yii::t('app7020', 'Channels'); ?></h3>
        <!--Action buttons-->
        <ul class="clearfix" data-bootstro-id="customActionButtons">
            <li>
                <div>
                    <a href="#" id="openNewChannelDialog" data-dialog-title="<?php echo Yii::t('app7020', 'New Channel'); ?>" data-modal="openModal">
                        <i class="fa fa-television"></i>
                        <span class="boxTitle">
                            <span class="innerBoxTitle">
                                <?php echo Yii::t('app7020', 'New Channel'); ?>
                            </span>
                        </span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
    <div class="row-fluid">
        <div class="channelsGridViewWrapper">
            <?php $this->widget('common.widgets.ComboGridView', $channelsGridViewParams); ?>
        </div>
    </div>
</div>