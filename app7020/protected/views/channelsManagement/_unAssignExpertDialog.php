<?php
$url = Docebo::createApp7020Url('axChannels/unAssignExperts');
$single = (count($idExperts) > 1) ? false : true;
$c = App7020Channels::model()->findByPk($idChannel);
$translation = $c->translation();

echo CHtml::beginForm($url, 'post', array(
	'class' => 'ajax',
	'id' => 'unAssignExpertFromTopic'
));
if ($single) {
	$idExpertsParams = (is_array($idExperts)) ? $idExperts[0] : $idExperts;
	$channelExpert = App7020Experts::model()->findByPk($idExpertsParams);
	$name = $channelExpert->expert;
}else{
	$idExpertsParams = implode(",", $idExperts);
}
?>
	<div id="deleteTopicFormContainer">
		<p class="topicInfo">
			<?php if ($single): ?>
				<?php echo Yii::t('app7020', 'Un Assign'); ?>: <strong><?= $name->firstname . ' ' . $name->lastname; ?></strong><br />
				<?php echo Yii::t('app7020', 'From Channel'); ?>: <strong><?= $translation['name']; ?></strong>
			<?php else: ?>
				<?php echo Yii::t('app7020', 'Selected Experts to unassign'); ?>: <strong><?= count($idExperts); ?></strong><br />
			<?php endif; ?>
		</p>
		<div class="footerSection">
			<?php echo CHtml::checkBox('agreeCheck', false) ?>
			<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'agreeCheck', array('style' => 'display: inline-block')) ?>
		</div>
	</div>
	<div class="form-actions">
		<?php
		echo CHtml::hiddenField('idExperts', $idExpertsParams);
		echo CHtml::hiddenField('idChannel', $idChannel);
		echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn save-dialog app7020-button green'));
		echo CHtml::button(Yii::t('standard','_CANCEL'), array('class' => 'btn close-dialog'));
		?>
	</div>
<?= CHtml::endForm(); ?>