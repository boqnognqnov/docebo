<div class="answerBoxModel" ng-repeat="item in items | filter: itemsFilter" ng-class="{best: item.is_best_answer}">
	<div ng-bind-html="item.avatar" class="avatar-wrapper"><img ng-src="{{item.avatar}}" /></div>
	<div class="content-wrapper">
		<dropdown actions="item.actions"></dropdown>
		<div class="author-wrapper">
			<span class="name">{{item.author}}</span>
			<span class="role" ng-if="item.is_expert">(<?php echo Yii::t('app7020', 'Expert'); ?>)</span>
			<span class="suffix"><?php echo Yii::t('app7020', 'answered'); ?>:</span>
		</div>
		<div class="answer-wrapper">
			<span class="prefix" ng-if="item.is_best_answer">
				<?php echo Yii::t('app7020', 'Best answer'); ?>:
			</span>
			<span ng-bind-html='to_trusted(item.text)' class="answer" build-tinymce="{{item.id_answer}}"></span>
		</div>
		<div class="footer clearfix">
			<div class="mark-as-best-wrapper" ng-if="params.logged_user.id_user == params.owner_id && params.logged_user.id_user != item.author_id">
				<span class="text-label" ng-click="markAsBest()" ng-if="!item.is_best_answer"><?php echo Yii::t('app7020', 'Mark as best answer'); ?></span>
				<span class="text-label" ng-click="markAsBest()" ng-if="item.is_best_answer"><?php echo Yii::t('app7020', 'Unmark as best answer'); ?></span>
			</div>
			<div class="like-dislike-wrapper" like-dislike likes="{{item.likes}}" dislikes="{{item.dislikes}}" data-id="{{item.id_answer}}" read-only="item.readOnly"></div>
			<div class="time-wrapper">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'updated'); ?></span>
					<span class="count">{{item.update_date}}</span>
				</p>
			</div>
		</div>
	</div>
</div>