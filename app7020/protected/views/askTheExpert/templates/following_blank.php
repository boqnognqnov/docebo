<div id="myAnswerBlank">
	<div class="grayLines">
		<div class="grayLine bottom">
			<div class="customDropdown-widget peerReviewDropdown">
				<div class="wrapper">
					<div class="awesomeIcon-dots">
						<ul class="listArray">
						<li>
							<div class=" link link-node-0" ng-click="">Follow</div>
						</li>
						<li class="fakeLi">
							<div class="grayLineLi"><a>&nbsp;</a></div>
						</li>
					</div>
				</div>
			</div>
		</div>
		<div class="grayLine"></div>
		<div class="grayLine"></div>
		<div class="grayLine"></div>
	</div>
	<h1><?= Yii::t('app7020', 'Hey, you are not following any question'); ?></h1>
	<div class="subtitle"><?= Yii::t('app7020', 'Don`t miss an answer anymore, start following your favourite Questions & Answers'); ?></div>
</div>