<div class="current-question-wrapper">
	<div ng-bind-html="question.avatar" class="avatar-wrapper"></div>
	<div class="content-wrapper">
		<h3 class="title">
			<span class="text" ng-bind-html="to_trusted(question.question)"></span>
		</h3>
		<div class="tags-wrapper" ng-if="question.tags.length"><?php echo Yii::t('app7020', 'Tags') ?>:
			<span ng-repeat="tag in question.tags">{{tag}}</span>
		</div>
		<div class="related-wrapper" ng-if="showRelated">
			<a ng-if="question.other_fields.asset" href="{{question.other_fields.asset.asset_page_url}}">
				<div class="image-wrapper">
					<img ng-src="{{question.other_fields.asset.asset_preview}}" alt="<?php echo Yii::t('app7020', 'Image Not found'); ?>"/>
				</div>
				<div class="text-wrapper">
					<span class="prefix"><?php echo Yii::t('app7020', 'This question is related to'); ?>:</span>
					<span class="link">{{question.other_fields.asset.asset_title}}</span>
				</div>
			</a>
			<a ng-if="!question.other_fields.asset && question.other_fields.channel" href="{{question.other_fields.channel.channel_url}}">
				<div class="image-wrapper" ng-style="{background: question.other_fields.channel.channel_color}">
					<i class="fa {{question.other_fields.channel.channel_icon}}" ng-style="{color: question.other_fields.channel.channel_icon_color}"></i>
				</div>
				<div class="text-wrapper">
					<span class="prefix"><?php echo Yii::t('app7020', 'This question is associated to channel'); ?>:</span>
					<span class="link">{{question.other_fields.channel.channel_title}}</span>
				</div>
			</a>
		</div>
		<div class="footer clearfix">
			<div class="follow-wrapper">
				<div class="follow" ng-if="!question.is_following">
					<span class="text-button" ng-click="following()"><?php echo Yii::t('app7020', 'Follow this question') ?></span>
				</div>
				<div class="unfollow" ng-if="question.is_following">
					<i class="fa fa-check"></i>
					<span class="text-label"><?php echo Yii::t('app7020', 'Following') ?></span>
					<span class="text-button" ng-click="following()"><?php echo Yii::t('app7020', 'Unfollow') ?></span>
				</div>
			</div>
			<div class="time-wrapper">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'updated'); ?></span>
					<span class="count">{{question.update_date}}</span>
				</p>
			</div>
			<div class="views-wrapper">
				<p class="text-label">
					<span class="count">{{question.views}}</span>
					<span class="suffix"><?php echo Yii::t('app7020', 'views'); ?></span>
				</p>
			</div>
			<div class="author-wrapper">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'by'); ?></span>
					<a href="{{question.owner_channel_link}}">
						<span class="name">{{question.owner}}</span>
					</a>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="question-answers-wrapper">
	<div class="headline clearfix">
		<div class="answers-count" ng-if="head.answers_count != 0">
			<span class="count">{{head.answers_count}}</span>
			<span class="suffix"><?php echo Yii::t('app7020', 'Answers'); ?></span>
		</div>
		<div class="no-answers" ng-if="head.answers_count == 0">
			<span class="text-label"><?php echo Yii::t('app7020', 'No answers yet'); ?></span>
		</div>
		<div class="answer-now-button">
			<span class="text-button" ng-click="gotoAnswerNow()" ng-if="head.answers_count != 0"><?php echo Yii::t('app7020', 'Answer this question!'); ?></span>
		</div>
	</div>
	<div class="items-wrapper" answer-items items="question.answers" params="params"></div>
	<div ng-show="question.can_answer" class="new-answer-wrapper">
		<div class="newAnswerBoxModel">
			<div ng-bind-html="head.logged_user.avatar" class="avatar-wrapper"></div>
			<div class="content-wrapper">
				<textarea class="add-new-answer"></textarea>
			</div>
		</div>
	</div>
</div>