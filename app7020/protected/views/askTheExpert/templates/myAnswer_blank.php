<div id="myAnswerBlank">
	<i class="comments-icon fa icon-comments-alt"></i>
	<h1><?= Yii::t('app7020', 'Hey, you haven`t answered any questions yet'); ?></h1>
	<div class='subtitle'><?= Yii::t('app7020', 'Start answering to some questions and spread the knowledge'); ?></div>
</div>