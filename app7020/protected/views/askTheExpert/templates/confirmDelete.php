<div class="inner-wrapper">
    <div class="alerterror"></div>
	<p class="text-label"><?php echo Yii::t('app7020', 'Are you sure you want to delete it?'); ?></p>
	<?php
		echo CHtml::checkBox('agreeCheck', false);
		echo CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'agreeCheck', array('class' => 'inline-block'));
	?>
</div>
<div class="form-actions">
	<?php
		echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green'));
		echo CHtml::button('Cancel', array('class' => 'close-dialog'));
	?>
</div>