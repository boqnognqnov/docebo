<div  class="questionBoxModel"  ng-repeat="item in items| filter: itemsFilter" ng-click="location('/question/' + item.question_id, $event)">

	<div ng-bind-html="item.avatar" class="avatar-wrapper"> </div>


	<div class="content-wrapper">
		<span class="following"  ng-if="item.is_following"><i class="fa fa-eye" aria-hidden="true"></i></span>
		<dropdown actions="item.actions"></dropdown>
		<div class="title">
			<span class="prefix"  ng-if="questionAllow.title_prefix"><?php echo Yii::t('app7020', 'Your last answer to'); ?>:</span>
			<span class="text" build-tinymce="{{item.question_id}}"  ng-bind-html="to_trusted(item.question)"></span>
			
		</div>
		<div class="bestAnswer" ng-if="item.have_best_answer" data-answer-id="{{item.answer.answer_id}}">
			<span class="fa-wrapper">
				<i class="fa fa-check-circle"></i>
			</span>
			<div class="answer">
				<span class="prefix"><?php echo Yii::t('app7020', 'Best answer'); ?> {{item.answer.answerauthor}}:</span>
				<span class="bestAnswerName text"  ng-bind-html="to_trusted(item.answer.text)" ></span>
			</div>
		</div>
		<div class="lastAnswer" ng-if="item.answer.answer_id && !item.have_best_answer" data-answer-id="{{item.answer.answer_id}}">
			<span class="prefix"><?php echo Yii::t('app7020', 'Last answer by'); ?> <strong>{{item.answer.answerauthor}}:</strong></span>
			<span ng-bind-html="to_trusted(item.answer.text)" class="answer"></span>
		</div>
		<div class="footer">
			<div class="related">
				<span ng-if="questionAllow.footer.indexOf('related') > -1 && item.other_fields.asset" ng-click="redirect(item.other_fields.asset.asset_page_url, $event)">
					<div class="image-wrapper">
						<img ng-src="{{item.other_fields.asset.asset_preview}}" alt="<?php echo Yii::t('app7020', 'Image Not found'); ?>"/>
					</div>
					<div class="text-wrapper">
						<span class="prefix"><?php echo Yii::t('app7020', 'This question is related to'); ?>:</span>
						<span class="link">{{item.other_fields.asset.asset_title}}</span>
					</div>
				</span>
				<span ng-if="questionAllow.footer.indexOf('related') > -1 && !item.other_fields.asset && item.other_fields.channel" ng-click="redirect(item.other_fields.channel.channel_url, $event)">
					<div class="image-wrapper" ng-style="{background: item.other_fields.channel.channel_color}">
						<i class="fa {{item.other_fields.channel.channel_icon}}" ng-style="{color: item.other_fields.channel.channel_icon_color}"></i>
					</div>
					<div class="text-wrapper">
						<span class="prefix"><?php echo Yii::t('app7020', 'This question is associated to channel'); ?>:</span>
						<span class="link">{{item.other_fields.channel.channel_title}}</span>
					</div>
				</span>
			</div>
			<div class="author-wrapper" ng-if="questionAllow.footer.indexOf('author') > -1">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'by'); ?></span>
					<span ng-click="redirect(item.owner_channel_link, $event)">
						<span class="name">{{item.owner}}</span>
					</span>
				</p>
			</div>
			<div class="views-wrapper" ng-if="questionAllow.footer.indexOf('views') > -1">
				<p class="text-label">
					<span class="count">{{item.views}}</span>
					<span class="suffix"><?php echo Yii::t('app7020', 'views'); ?></span>
				</p>
			</div>
			<div class="time-wrapper" ng-if="questionAllow.footer.indexOf('time') > -1">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'updated'); ?></span>
					<span class="count">{{item.update_date}}</span>
				</p>
			</div>
			<div class="answers-wrapper" ng-if="questionAllow.footer.indexOf('answers') > -1">
				<p class="text-label">
					<span class="no-answers" ng-show="item.total_number_answers == 0"><?php echo Yii::t('app7020', 'no answers yet'); ?></span>
					<span class="have-answers" ng-show="item.total_number_answers > 0">
						<span class="count">{{item.total_number_answers}}</span>
						<span class="suffix"><?php echo Yii::t('app7020', 'answers'); ?></span>
					</span>
				</p>
			</div>
			<div ng-if="questionAllow.footer.indexOf('likes') > -1" class="like-dislike-wrapper" like-dislike likes="{{item.answer.likes}}" dislikes="{{item.answer.dislikes}}" data-id="{{item.answer.answer_id}}" read-only="item.answer.readOnly"></div>
		</div>
	</div>
</div>