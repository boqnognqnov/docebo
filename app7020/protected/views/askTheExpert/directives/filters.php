<div class="directive-filters" style="display: none;" id="filtersBox">
	<div class="filters" >
		<div class="row show-grid">
			<div ng-repeat="($indexgroup, group) in filters" class="{{group.spantype}}">
				<div class="filterSection">{{group.name}}</div>
				<div ng-repeat="($indexitem, groupoption) in group.options">
					<input data-label="{{groupoption.label}}" value="{{groupoption.value}}" id="{{group.typename}}_{{groupoption.id}}" type="{{groupoption.type}}" name="{{group.typename}}" ng-checked="groupoption.selected">
					<label for="{{group.typename}}_{{groupoption.id}}">
						{{groupoption.label}}
						<span ng-if="groupoption.subtitle" class="subData">{{groupoption.subtitle}}</span>
					</label>

				</div>
			</div>
		</div>

		<div class="row-fluid">
			<div></div>
			<div class="formButton">
				<?php echo CHtml::button(Yii::t('app7020', 'Apply'), array("class" => "apply-filters")); ?>
				<?php echo CHtml::link(Yii::t('app7020', 'Close'), 'javascript:;', array('class' => 'close-dialog black')); ?>
			</div>
		</div>
	</div>

	<!-- Applied filters static -->
	<div ng-if="selectedfilters && selectedfilters.length > 0" id="appliedFilters" class="row-fluid">
		<span class="fControl clearFilters"><?php echo Yii::t("standard", "Clear filters"); ?></span>
		<div ng-repeat="($indexgroup, selectedgroup) in selectedfilters" class="fList">
			<span class="fItem" data-value="{{selectedgroup.name}}"><h5>{{selectedgroup.groupname}}</h5><span>{{selectedgroup.optionname}}</span><i data-value="{{selectedgroup.name}}" class="fa fa-times remove-filter"></i></span>
		</div>
	</div>
	<!-- End applied filters static -->
</div>