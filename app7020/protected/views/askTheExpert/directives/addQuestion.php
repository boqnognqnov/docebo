<?php
 $custom_Select = App7020Channels::customProviderSelect(array('num_experts' => true, 'num_visibility_structures' => false, 'friendly_permissions' => false));
$custom_channel_visibility_select = array(
	"idUser" => Yii::app()->user->idst,
	"ignoreVisallChannels" => false,
	"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
	'appendEnabledEffect' => true,
	'ignoreSystemChannels' => true,
	'customSelect' => $custom_Select,
    'sort' => array(
        'channelOrdering' => array(
            CSort::SORT_ASC => 'channelOrdering',
            CSort::SORT_DESC => 'channelOrdering DESC',
            'default' => CSort::SORT_ASC
        )
    ),
	'return' => 'data'
);
$channels = App7020Channels::extractUserChannels($custom_channel_visibility_select, false);
?>
<form name="addQuestionForm" ng-controller="NavigationMenuController">
	<div class="inner-add-question-content">
		<i class="fa fa-times dialog-close"></i>
		<h2 class="title"><?php echo Yii::t('app7020', 'Ask the Experts!'); ?></h2>
		<h3 class="sub-title"><?php echo Yii::t('app7020', 'and contribute to create Trusted Knowledge'); ?></h3>
		<div class="content-wrapper">
			<dl>
				<dd class="question">
					<div class="wrapper">
						<p class="headline">
							<span class="text"><?php echo Yii::t('app7020', 'What do you want to ask?'); ?></span>
							<span class="required">(<?php echo Yii::t('app7020', 'mandatory'); ?>)</span>
						</p>
						<textarea class="tinymce-add-question" name="title" ui-tinymce="{trusted: true}" ng-model="addQuestionData.title"></textarea>
					</div>
				</dd>
				<dd class="requireAssignChannel">
					<div class="wrapper">
						<label class="forRequest">
							<?php echo CHtml::checkBox('is_request', false); ?>
							<span class="text"><?php echo Yii::t('app7020', 'I would like to receive a knowledge asset (such as a video, a document, a slides presentation) as support material to my question.'); ?></span>
						</label>
					</div>
				</dd>
				<dd class="assignChannel">
					<div class="wrapper">
						<p class="headline">
							<span class="text"><?php echo Yii::t('app7020', 'Assign a channel to your question'); ?></span>
							<span class="required <?php
							if (PluginManager::isPluginActive('MultidomainApp')) {
								echo 'allways';
							}
							?>">(<?php echo Yii::t('app7020', 'mandatory'); ?>)</span>
						</p>
						<p class="sub-headline">
							<span class="text"><?php echo Yii::t('app7020', 'This will notify any expert assigned to that channel, and you\'ll get a faster answer!'); ?></span>
						</p>
						<select id="channelsSelect" ddslick name="id_channel">
							<?php
							if (!PluginManager::isPluginActive('MultidomainApp')) {?>
								<option value="0"><?= Yii::t('app7020', 'Do not assign to any channel') ?></option>
							<?php } ?>

							<?php
							foreach ($channels as $value) :
								if ((int)$value['channelExperts']):
									?>
									<option data-description='<span id="expertnumberingreen" class="green">  <?php echo (int)$value['channelExperts'];  echo (int)$value['channelExperts'] == 1 ? ' '. Yii::t("standard", "assigned expert"): ' '.Yii::t("standard", "assigned experts"); ?>  </span>' value=" <?php echo $value['idChannel']; ?>"> <?php echo $value['channelName']; ?>  -  </option>
								<?php else: ?>
									<option data-description='<span class="grey"> <?php echo $value['channelExperts'] . " " . Yii::t("standard", "assigned experts"); ?> </span>' value=" <?php echo $value['idChannel']; ?>"><?php echo $value['channelName']; ?> - </option>
								<?php
								endif;
							endforeach;
							?>
						</select>
					</div>
				</dd>
				<dd class="questionTags">
					<div class="wrapper">
						<p class="headline">
							<span class="text"><?php echo Yii::t('app7020', 'Add one or more tag to your question'); ?></span>
						</p>
						<p class="sub-headline">
							<span class="text"><?php echo Yii::t('app7020', 'This will improve the categorization of your question'); ?></span>
						</p>
						<p class="suggested">

						<docebo-tags name="tags" data-id="tags" ng-model="addQuestionData.tags"></docebo-tags>
						</p>
					</div>
				</dd>
			</dl>
		</div>
	</div>
	<div class="footer-actions">
<?php echo CHtml::button(Yii::t('app7020', 'Ask the experts!'), array('class' => 'green save', 'ng-click' => 'questionSubmit(addQuestionForm, $event)', 'ng-disabled' => 'questionSubmitDisabled'));
?>
	</div>
</form>