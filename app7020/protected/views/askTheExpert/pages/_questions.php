<div class="other-information-wrapper" ng-if="items.length !== 0">
	<div class="questions-wrapper">
		<span class="count">{{head.questions_count}}</span>
		<span class="suffix"><?php echo Yii::t('app7020', 'Questions'); ?></span>
	</div>
	<div class="settings-button-wrapper">
		<i class="fa fa-sliders"></i>
	</div>
</div>
<div class="filters-wrapper">
	<filters filters="filters" selectedfilters="selectedfilters"></filters>
</div>

<div class="items-wrapper" question-items items="items" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>