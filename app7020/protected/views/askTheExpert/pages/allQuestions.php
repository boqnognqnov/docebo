<div class="inner-content-wrapper" ng-init="init()">
	<div class="addQuestionAsBlankState" id="add-question-dialog" ng-if="head.length != 0 && items.length == 0">
		<?php $this->renderPartial('directives/addQuestion', array(), false); ?>
	</div>
	<?php $this->renderPartial('pages/_questions', array(), false); ?>
</div>