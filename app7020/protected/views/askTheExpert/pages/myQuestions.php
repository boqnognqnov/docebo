<div class="inner-content-wrapper" ng-init="init()">
	<div class="addQuestionAsBlankState" id="add-question-dialog" ng-if="head.length != 0 && items.length == 0">
		<?php $this->renderPartial('directives/addQuestion', array(), false); ?>
	</div>
	<div class="other-information-wrapper" ng-show="items.length !== 0">
		<div class="switchers" ng-init="openClose.have_best_answer = 'false'">
			<input type="radio" name="openClose" id="radio-open" value="false" ng-model="openClose.have_best_answer" />
			<label for="radio-open">
				<?php echo Yii::t('app7020', 'Open'); ?>
				<span class="count">{{  foundOpenQuestions() }}</span>
			</label>
			<input type="radio" name="openClose" id="radio-best" value="true" ng-model="openClose.have_best_answer" />
			<label for="radio-best">
				<?php echo Yii::t('app7020', 'Best answer given'); ?>
			</label>
		</div>
	</div>
	<div class="items-wrapper" question-items items="items" items-filter="openClose" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
</div>