<div class="inner-content-wrapper following-container" ng-init="init()">
	<div ng-include src="'index.php?r=askTheExpert/view&view_name=templates.following_blank'" ng-if="head.length != 0 && items.length == 0"></div>
	<div class="items-wrapper" question-items items="items" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
</div>