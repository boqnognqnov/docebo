<div class="row-fluid assets-ranks-row">
	<!-- Asset Title, etc -->
	<div class="span4 asset-info">
		<div>
			<span>
				<?php echo $assetsTypes[$data['content_type']]; ?> |
			</span>
			<?php if($data['content_type'] == App7020Assets::CONTENT_TYPE_VIDEO) : ?>
				<span>
					<?php echo gmdate("H:i:s", $data['duration']); ?> |
				</span>
			<?php endif; ?>
			<span>
				<?php echo App7020Helpers::timeAgo($data['datePublished']) ?> <?php echo Yii::t('app7020','ago');?>
			</span>
		</div>
		<div>
			<image src='<?php echo App7020Assets::getAssetThumbnailUri($data['id']); ?>'>
			<a href="<?php echo Docebo::createApp7020AssetsViewUrl($data['id']); ?>">
				<?php echo substr($data['title'], 0, 50); ?>
			</a>
		</div>
	</div>
	<!-- Views -->
	<div class="span1">
		<?php echo $data['count_views']; ?>
	</div>
	<!-- Rating -->
	<div class="span1">
		<?php echo $data['sum_ratings']; ?>
		<i class="fa fa-star" aria-hidden="true"></i>
	</div>
	<!-- Invitation -->
	<div class="span2">
		<?php echo $data['count_invitations']; ?>
	</div>
	<!-- Watch rate -->
	<div class="span2">
		<?php echo $data['watch_rate']; ?>
	</div>
	<!-- Reaction time -->
	<div class="span2">
		<?php
			if($data['reaction_time_check'] < 0 || $data['reaction_time_check'] == '' || substr($data['reaction_time'], 0, 1) == '-'){
				echo '00h 00m 00s';
			} else {
				echo $data['reaction_time'];
			}
		?>
	</div>
</div>