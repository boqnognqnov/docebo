
<?php

//$customTabsForm



$customTabsForm = array(
    'menuHeading' => 'My menu',
    'menu' => array(
        array('link-id' => 'general-settings',
            'name' => 'General Settings',
            'icon-id' => 'fa-cog'),
        array('link-id' => 'general-settings2',
            'name' => 'General Settings2',
            'icon-id' => 'fa-cog'),
        array('link-id' => 'general-settings3',
            'name' => 'General Settings3',
            'icon-id' => 'fa-cog'),
    ),
    'content' => array(
        array(
            'contentHeading' => 'Test1',
            'viewName' => '_test1',
            'link-id' => 'general-settings',
            'sendDataToView' => array(),
        ),
        array(
            'contentHeading' => 'Test2',
            'viewName' => '_test2',
            'link-id' => 'general-settings2',
            'sendDataToView' => array(),
        ),
        array(
            'contentHeading' => 'Test3',
            'viewName' => '_test3',
            'link-id' => 'general-settings3',
            'sendDataToView' => array(),
        ),
    )
);
$this->widget('common.widgets.tabsForm', $customTabsForm);
?>

