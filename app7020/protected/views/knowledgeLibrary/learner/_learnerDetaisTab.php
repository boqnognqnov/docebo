<?php
/* @var $content App7020Content */
?>
<div class="row-fluid">
	<div class="app7020-contrbution-description">
		<p>
			<?= $content->description; ?>			
		</p>
	</div>
	<div class="app7020-contrbution-topics">
		<ul>
			<li class="app7020-learner-title-icon">
				<i class="fa fa-tags"></i>
			</li>
			<?php foreach ($content->tagLinks as $tag): ?>
				<li class="tm-tag"><?= $tag->tag->tagText; ?></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div class="app7020-contrbution-tags">
		<ul>
			<li class="app7020-learner-title-icon">
				<i class="fa fa-book"></i>
			</li>
		</ul>
	</div>

</div>