<div id="accordionContainer">
    <div class="askSomething">
        <?= CHtml::link(Yii::t('app7020', 'Ask the Experts') . "!", 'javascript:;', array('class' => 'app7020Buttons green-BtnStyle askBtn')) ?>
    </div>
    <h2>
        <?= Yii::t('app7020', 'SIMILAR ASSETS') ?>
    </h2>
    <div class="accordion-group similarVideoPanel">
        <div id="similarVideos" class="accordion-body">
            <?php foreach ($arraySimilr as $item): ?>
                <a href="<?= Docebo::createApp7020Url('knowledgeLibrary/item', array('id' => $item['id'])) ?>">
                    <div class="item">
                        <div class="thumbnail-view"> 
                            <?= App7020Assets::getPublcPreviewThumbnail($item) ?>
                        </div>
                        <div class="content">
                            <div class="title"><?php
                                if(strlen($item['title'])> 24) {
                                    echo strip_tags(substr($item['title'], 0, 24) . "...");
                                } else {
                                    echo strip_tags($item['title']);
                                }
                                
                                ?></div>
                            <div class="rating">
                                <?php
                                $this->widget('common.widgets.ContentRating', array('contentId' => $item['id'], 'readOnly' => true));
                                ?>
                                <span class="countRating"><?= number_format($item['contentRating'], 1) ?></span>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>