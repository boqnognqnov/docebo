<!--Show App7020 Filters Widget-->
<?php
$links[] = array(
	'title' => Yii::t('app7020', 'Contribute!'),
	'attr' => array('class' => 'contribute green-BtnStyle'),
	'href' => Docebo::createAppUrl('app7020:assets/index')
);
?>

<div class="<?php echo $browser; ?>  main-section row-fluid app7020DefaultStyles" data-container="root" id="knowledgeLibrary_index" >
	<!--Head title
	<h3 class="app7020-headline"><?= Yii::t('app7020', 'Knowledge Library') ?></h3>
	-->
	<?php $this->renderPartial('app7020.protected.views.includes._app7020SearchHeadline'); ?>
	<?php
	if ($returnBlankPage) {
		$this->widget('common.widgets.app7020Filters', array(
			'searchEngine' => false,
			'topics' => false,
			'toggleGrid' => false,
			'links' => $links,
		));
		$this->renderPartial('app7020.protected.views.knowledgeLibrary.listViewBlankTemplates.contributionPageBlank');
	} else {
		$this->widget('common.widgets.app7020Filters', $app7020Filters);
		$this->widget('common.widgets.app7020NavigationMenu', array('links' => $navigationMenuLinks, 'oldStyle' => false, 'select' => (!empty($_GET['tab']) && $_GET['tab'] <= count($navigationMenuLinks)) ? $_GET['tab'] : false));
	}
	?>
</div>












