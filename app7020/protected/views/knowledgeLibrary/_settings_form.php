<?php
/* @var $model App7020SettingsPeerReview */
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app7020-general-settings-form',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'class' => "ajax form-horizontal",
	),
		));
?>
<div class="border-wraper">
	<div class="row-fluid  app7020-container grey">
		<div class="span3">
			<h5><?php echo Yii::t('app7020', 'Custom settings'); ?></h5>
		</div>
		<div class="span9 logical-group">
			<div class="control-group">
				<div class="controls">
					<?php
					echo $form->checkBox($model, 'enableCustomSettings');
					echo $form->labelEx($model, 'enableCustomSettings', array('class' => 'control-label'));
					?> 
				</div>
			</div>
		</div>
	</div>
</div>
<div class="dissabled-container  <?= $model->enableCustomSettings ? '' : 'dissabled' ?>"> 
	<div class="row-fluid additional">
		<div class="span3">
			<h5><?php echo Yii::t('app7020', 'Sharing'); ?></h5>
		</div>
		<div class="span9 logical-group">
			<div class="control-group">
				<div class="controls">
					<?php
					echo $form->checkBox($model, 'inviteToWatch');
					echo $form->labelEx($model, 'inviteToWatch', array('class' => 'control-label'));
					?> 
				</div>
			</div>

		</div>
	</div>

	<div class="border-wraper additional">
		<div class="row-fluid  app7020-container grey">
			<div class="span3">
				<h5><?php echo Yii::t('app7020', 'Ask the Experts'); ?></h5>

			</div>
			<div class="span9 logical-group">
				<div class="control-group">
					<div class="controls">
						<?php
						echo $form->checkBox($model, 'allowContributorAnswer');
						echo $form->labelEx($model, 'allowContributorAnswer', array('class' => 'control-label'));
						?> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span3">

	</div>
	<div class="span9">
		<div class="control-group">
			<div class="controls">
				<?php
				$htmlOptions = array('class' => 'btn save app7020-button');
				$ajaxOptions = array('dataType' => 'json',
					'type' => 'post',
					'success' => 'function(data) {
                             app7020.peerReviewSettings.ajax(data);   
                    }',
					'beforeSend' => 'function(){                        
                          app7020.peerReviewSettings.addLoader();
                      }'
				);
				echo CHtml::ajaxSubmitButton(Yii::t("app7020", "Save changes"), Docebo::createApp7020Url('knowledgeLibrary/axValidateSettings'), $ajaxOptions, $htmlOptions);
				?>

			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
