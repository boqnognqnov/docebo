<h1 class="contentTitle"><?= ($data->title) ? $data->title : Yii::t('app7020', 'Empty'); ?></h1>
<div class="contentInformation">
	<div class="contentInformationTop">
		<div class="avatar">
			<div data-custom="circle-avatar">
				<img alt="64x64" src="<?= $data->contributor->getAvatarImage(true); ?>">
			</div>
		</div>
		<div class="info">
			<div class="infoBox">
				<p class="user"><?= Yii::t('app7020', 'Contributor') ?>: <span><?= $data->contributor->getUserNameFormatted() ?></span></p>
				<p><?= Yii::t('app7020', 'Published') ?>: <span><?= App7020Helpers::timeAgo($data->created); ?> ago</span></p>
				<p><?= Yii::t('app7020', 'Viewed') ?>: <span><?= count($data->viewes) ?> times</span></p>
			</div>
			<div class="ratingBox">
				<p><?= Yii::t('app7020', 'Rate this asset!') ?></p>
				<?php
				$this->widget('common.widgets.ContentRating', array(
					'contentId' => $data->id,
					'readOnly' => $data->isRatedByMe() || $data->isContributor ? true : false
						)
				);
				?>
				<span id="rating_score_<?= $data->id ?>">
					<?= App7020ContentRating::calculateContentRating($data->id) ?>
				</span>

			</div>
		</div>
		<div class="icons">
			<?php
			if (
					(App7020CustomContentSettings::getContentCustomSetting(App7020Assets::SETTING_ALLOW_DOWNLOAD, $data->id) == ShareApp7020Settings::ON ||
					ShareApp7020Settings::isGlobalDownloadAccess()) &&
					$data->contentType != App7020Assets::CONTENT_TYPE_LINKS &&
					$data->contentType != App7020Assets::CONTENT_TYPE_VIDEO &&
					$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_OTHER &&
					$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_ARCHIVE &&
					$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC
			) :
				?>
				<a class="fa fa-download" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', 'Download asset') ?>" href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/forceDownload', array('file' => $data->id)); ?>" target="_blank">
				</a>
			<?php endif; ?>

			<!--
			<i class="fa fa-exclamation-triangle"></i>
			<i class="fa fa-clock-o"></i>
			-->

			<?php
			if (App7020Helpers::hasContentInviteWatch($data->id) && $data->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED):
				?>
				<!--
				<i class="fa fa-share-square"></i>
				-->
				<i class="fa fa-user-plus" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', 'Invite people to watch') ?>"  ng-controller="app7020AssetController" ng-click="openInviteToWatchModal(<?php echo $data->id; ?>)">
				</i>
				<?php
			endif;
			?>
			<!--
			<i class="fa fa-share-alt"></i>
			-->
		</div>
	</div>
	<div id="details-container" class="accordion-group" data-show="false">

		<div id="details" class="accordion-body collapse">
			<!-- Details -->
			<div class="row-fluid">
				<div class="app7020-contrbution-description">
					<ul>
						<li><?= Yii::t('app7020', 'Description'); ?> :</li>
						<li><?= $data->description; ?></li>
					</ul>
				</div>
				<div class="app7020-contrbution-channels">
                    <ul>
                        <li><?= Yii::t('app7020', 'Channels'); ?> :</li>
						<?php
						foreach ($data->asset_channels as $value) {
							$channelObject = App7020Channels::model()->findByPk($value->idChannel);
							if ($channelObject->id) {
								$translation = $channelObject->translation();
								?>
								<span class='channelName'>
									<?php echo $translation['name']; ?>
								</span>
								&nbsp;&nbsp;&nbsp;
								<?php
							}
						}
						?>
					</ul>
				</div>
				<div class="app7020-contrbution-tags">
					<ul>
						<!--
						<li class="app7020-learner-title-icon">
							<i class="fa fa-book"></i>
						</li>
						-->
						<li><?= Yii::t('app7020', 'Tags'); ?> :</li>
						<?php
						foreach ($data->tagLinks as $tag) {
							?>
							<li class="tm-tag"><?= $tag->tag->tagText; ?></li>
							<?php
						}
						?>
					</ul>
				</div>

			</div>
		</div>
		<a class="accordion-toggle show" data-toggle="collapse" href="#details" data-parent="#details-container">
			<?= Yii::t('app7020', 'Show Details') ?>
		</a>
		<a class="accordion-toggle hide" data-toggle="collapse" href="#details" data-parent="#details-container">
			<?= Yii::t('app7020', 'Hide Details') ?>
		</a>
	</div>

</div>
