<section class="row-fluid" id="app7020Tooltips">
    <div class="headline" for="app7020Tooltips">
        <span><?= Yii::t('app7020', 'Tooltips') ?></span>
    </div>
	
	<!--  Container for info and button for switch to "Add tooltips view"   -->
	<div class="app7020Info">
		<div class="contentContainer">
			<div class="app7020SwitchToAddNewTooltip">
				<div><i class="fa fa-plus-circle"></i></div>
				<?=
				Yii::t(
						'app7020',
						'{span}Click here to add new tooltip{/span} at playbar position',
						array('{span}' => '<span>','{/span}' => '</span>')
				)
				?>
			</div>
			<div><?= Yii::t('app7020', 'or') ?></div>
			<div>
				<?=
				Yii::t(
						'app7020',
						'{span}Select a tooltip{/span} from the video timeline to {span}edit position{/span} and {span}resize it.{/span}',
						array('{span}' => '<span>', '{/span}' => '</span>')
				)
				?>
			</div>
		</div>
		<div class="app7020Arrow"><span></span></div>
	</div>
	
		<!--  Container for "Add tooltips view"   -->
		<div class="app7020AddNewTooltip hide">
			<div class="contentContainer">
				<p><?= Yii::t('app7020', 'Tooltip Text') ?></p>
				<form id="form-save-tooltip" class="form-horizontal">
					<div class="control-group">
						<div class="controls">
						 <!-- <?= CHtml::textArea('tooltipText') ?> -->
						 <div editor textareaid="tooltipEditor" toolbar="bold italic underline" ng-model="current.content"></div>
						</div>
					</div>

					<p><?= Yii::t('app7020', 'Tooltip style (dark/light)') ?></p>
					<div class="control-group">
						<div class="controls">
							<div class="row-fluid tooltipStyle">
							<?php
								echo CHtml::radioButtonList(
											'tooltipStyle', '0', 
											array(
												'0' => '', // Dark
												'1' => '', // Light
											), 
											array(
												'template' => '<div class="span6"><label class="radio inline">{input}{label}</label></div>',
												'container' => '',
												'separator' => '',
												'ng-model' => 'current.tooltipStyle',
												'labelOptions' => array()     
											)
										);
								?>
							</div>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<?php
								echo CHtml::button(Yii::t("app7020", "Delete"), array('class' => 'btn delete app7020-button', 'id' => 'app7020-delete-tooltip'));
							?>
						</div>
						<div class="controls">
						<?php 
							echo CHtml::button(Yii::t("app7020", "Save"), array('class' => 'btn save app7020-button', 'id' => 'app7020-save-tooltip'));
						?>
						</div>
					</div>


					
				</form>
			</div>
			
		</div>



</section>