<section class="row-fluid ng-cloak" id="app7020PeerReview"  ng-controller="PeerReviewChat">
    <div class="headline" for="app7020PeerReview">
        <span><?= Yii::t('app7020', 'Peer Review') ?></span>
    </div>
    <header>
        <div class="span4">
            <div class="app7020peerReviewHeaderContent">
                <h2>
                    Experts
                </h2>
            </div>

        </div>
        <div class="span8">
            <div class="app7020peerReviewHeaderContent">
				<?php $this->widget('common.widgets.GuruCirclesList', array('arrayData' => $arrayGurus, 'maxVisibleItems' => 2)); ?>
            </div>
        </div>

    </header>
    <div id="app7020PeerMessages" class="app7020PeerMessages" >
<!--		<div class="app7020-loading"></div>-->
        <ul class="mCustomScrollbar">
			<li></li>
            <li ng-repeat="(messageIteration, message) in messages | orderBy: 'message.id' " class="app7020PeerItem {{message.kind}}" data-review-id="{{message.id}}">

                <section class="app7020PeerItemMessage" ng-init="">
                    <header class="app7020PeerItemPublishDate">
						<span class="edited" ng-if="message.created != message.lastUpdated"><i class="fa fa-pencil"></i><i>edited</i></span>
                        <time datetime="{{message.created != message.lastUpdated ? message.lastUpdated : message.created}}">
							{{ message.created != message.lastUpdated ? message.lastUpdated : message.created | NormalDate }}
						</time>
                        <h2>  {{message.fullname}}</h2>
                    </header>
						<p data-tooltip="{{message.idTooltip}}" ng-if="message.idTooltip"><?php echo Yii::t('app7020', 'Tooltip has been created at');?>:
							<a class='tooltipAt' ng-click="jumpTo(message.idTooltip);">{{message.tooltipAt}}</a>
						</p>
                    <p>			
                        {{message.message}}
                    </p>
					<div class="controlls-peer-review" >
						<?php $this->widget('common.widgets.DropdownControls', $controlParams); ?>
					</div>
                </section>
                <figure class="app7020PeerItemAvatar avatar" data-custom="circle-avatar">
                    <img alt="64x64" ng-src="{{message.avatar}}">
                </figure>
				<div class="clearfix clear"  ng-init="updateMessagesProccess.update(message.id)"></div>
            </li>
        </ul>
    </div>
    <div class="app7020AddNewReview">
        <form name="app7020AddNewAnswerForm" novalidate="novalidate" >
            <div class="app7020TextareaWraper">
				<?php
				echo CHtml::textArea('app7020AddNew-' . time(), '', array(
					'required' => 'required',
					'data-autoresize' => "",
					'placeholder' => Yii::t('app7020', 'Write here...'),
					'ng-model' => 'newMessage.message',
					'ng-required' => 'true')
				)
				?>
				<?= CHtml::link(Yii::t('app7020', 'Send'), 'javascript:;', array('class' => 'sendButton', 'ng-click' => 'addNew()', 'ng-show' => 'isShowInsert()')); ?>
				<?= CHtml::link(Yii::t('app7020', 'Edit'), 'javascript:;', array('class' => 'sendButton', 'ng-click' => 'updateAction()', 'ng-show' => 'isShowUpdate()')); ?>
            </div>
        </form>
    </div>
</section>

