<div class="readToPublish">
	<div class="text unpublishing">
		<span>
			<span class="green-text">
				<?= Yii::t('app7020', 'Published by'); ?>
			</span>
			<?= Yii::t('app7020', 'Expert'); ?>
			<b><?php echo $publisher; ?></b>
			on 
			<time><?php echo $date; ?></time>
			<?= Yii::t('app7020', 'at'); ?>
			<time><?php echo $time; ?></time>
		</span>

	</div>
	<div class="link">
		<?= CHtml::link(Yii::t('app7020', 'Unpublish'), 'javascript:;', array('class' => 'app7020Buttons publish unpublish')) ?>
	</div>
</div>
