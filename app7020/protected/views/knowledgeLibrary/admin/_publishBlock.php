<div class="readToPublish">
	<div class="text">
		<span><?= Yii::t('app7020', 'This asset is ready to be published'); ?></span>

	</div>
	<div class="link">
		<?= CHtml::link(Yii::t('app7020', 'Publish'), 'javascript:;', array('class' => 'app7020Buttons publish publishing')) ?>
	</div>
</div>
