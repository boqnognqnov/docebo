<!-- VIEW mode **** START *** -->	
<div id="App7020KLinfoPanel" class="readToPublish <?= $isEditMode ? 'hide' : '' ?>">
	<div class="text info">
		<?php if ($publisher) : ?>
			<div>
				<span class="green-text">
					<?= Yii::t('app7020', 'Published by:'); ?>
				</span>
				<?= Yii::t('app7020', 'Expert'); ?>
				<b><?php echo $publisher['publisher']; ?></b>
				on 
				<time><?php echo $publisher['date']; ?></time>
				<?= Yii::t('app7020', 'at'); ?>
				<time><?php echo $publisher['time']; ?></time>
			</div>
		<?php endif; ?>	
		<?php if ($editBy) : ?>
			<div>
				<span class="blue-text">
					<?= Yii::t('app7020', 'Last edit by:'); ?>
				</span>
				<?= Yii::t('app7020', 'Expert'); ?>
				<b><?php echo $editBy['editBy']; ?></b>
				on 
				<time><?php echo $editBy['date']; ?></time>
				<?= Yii::t('app7020', 'at'); ?>
				<time><?php echo $editBy['time']; ?></time>
			</div>
		<?php endif; ?>
		<?php if (!$publisher && !$editBy) : ?>
			<div><?= Yii::t('app7020', 'This content is not published or edited yet') ?></div>
		<?php endif; ?>

	</div>
	<div class="tools">			
		<?php if (App7020Assets::canUserDeleteContent($idContent)) : ?>
		<i class="fa fa-trash-o" rel="popover" data-content="<?= Yii::t('app7020', 'Delete') ?>" data-content-id="<?= $idContent ?>" data-method="delete"  ng-click="delete(<?= $idContent ?>)"></i>
		<?php endif; ?>
		<?php echo CHtml::link('', Docebo::createAppUrl('app7020:knowledgeLibrary/editItem', array('id' => $idContent)), array('class' => 'fa fa-pencil-square-o pageTooltips viewMode', 'rel' => 'popover', 'data-content' => Yii::t('app7020', 'Edit mode'))); ?>

		<?php
		if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED) {
			echo CHtml::link('', Docebo::createAppUrl('app7020:analytics/index', array('id' => $idContent)), array('class' => 'fa fa-bar-chart', 'rel' => 'popover', 'data-content' => Yii::t('app7020', 'Analytics')));
		}
		?>
	</div>
</div>
<!-- VIEW mode **** END *** -->	


<!-- EDIT mode **** START *** -->	
<div id="App7020KLeditPanel" class="readToPublish <?= $isEditMode ? '' : 'hide' ?>">
	<?php if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED && !$tooltipsPage) : ?>
		<div class="hint-exit">
			<?= Yii::t('app7020', 'Exit {span}edit mode{/span}', array('{span}' => '<span>', '{/span}' => '</span>')) ?>
		</div>
	<?php endif; ?>
	<?php if (!$hidePublishUnpublishSwitch): ?>
		<div id="switchPublishUnpublish">
			<span class='unpublished'><?= strtoupper(Yii::t('app7020', 'unpublish')) ?></span>
			<input id="switch" type='checkbox' value='' <?= $isChecked ?> name='switch' />
			<label for="switch"></label>
			<span class='published'><?= strtoupper(Yii::t('app7020', 'publish')) ?></span>
		</div>
	<?php endif; ?>
	<div class="text info">
		<?php if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED) : ?>
			<?php if ($publisher) : ?>
				<div class="publishedBy">
					<span class="green-text">
						<?= Yii::t('app7020', 'Published by:'); ?>
					</span>
					<?= Yii::t('app7020', 'Expert'); ?>
					<b><?php echo $publisher['publisher']; ?></b>
					on 
					<time><?php echo $publisher['date']; ?></time>
					<?= Yii::t('app7020', 'at'); ?>
					<time><?php echo $publisher['time']; ?></time>
				</div>
			<?php endif; ?>
			<?php if ($editBy) : ?>
				<div class="editedBy">
					<span class="blue-text">
						<?= Yii::t('app7020', 'Last edit by:'); ?>
					</span>
					<?= Yii::t('app7020', 'Expert'); ?>
					<b><?php echo $editBy['editBy']; ?></b>
					on 
					<time><?php echo $editBy['date']; ?></time>
					<?= Yii::t('app7020', 'at'); ?>
					<time><?php echo $editBy['time']; ?></time>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<div class="readyToPublish <?php
		if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED) {
			echo 'hidden';
		}
		?>">
			<span class="green-text ">
<?php echo Yii::t('app7020', 'This asset is ready to be published'); ?>
			</span>
		</div>
	</div>
	<div class="tools">
		<?php
		if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED && !$tooltipsPage) {
			echo CHtml::link(
					'', Docebo::createAppUrl('app7020:knowledgeLibrary/item', array('id' => $idContent)), array('class' => 'fa fa-times', 'rel' => 'popover', 'data-content' => Yii::t('app7020', 'Exit mode'))
			);
		}
		?>
		<?php if (App7020Assets::canUserDeleteContent($idContent)) : ?>
			<i class="fa fa-trash-o" rel="popover" data-content="<?= Yii::t('app7020', 'Delete') ?>" data-content-id="<?= $idContent ?>" data-method="delete"  ng-init="init(<?php echo $idContent ?>)"></i>
		<?php endif; ?>	
		<?php
		if ($contentStatus == App7020Assets::CONVERSION_STATUS_APPROVED) {
			echo CHtml::link('', Docebo::createAppUrl('app7020:analytics/index', array('id' => $idContent)), array('class' => 'fa fa-bar-chart', 'rel' => 'popover', 'data-content' => Yii::t('app7020', 'Analytics')));
		}
		?>
	</div>
</div>
<!-- EDIT mode **** END *** -->	

