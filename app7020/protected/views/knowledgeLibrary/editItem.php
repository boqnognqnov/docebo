<?php if ($data->getUserRole() == App7020Assets::USER_ROLE_EXPERT || $data->getUserRole() == App7020Assets::USER_ROLE_CONTRIBUTOR) : ?>

	<?php
	$urlOfAvatar = CoreUser::model()->findByPk(Yii::App()->user->idst)->getAvatarImage(true);
	/* @var $data App7020Content */
	?>
	<div id="knowledgeLibrarySingle" class="<?php echo $ie; ?> editSingle" ng-controller="app7020AssetController">
		<?php
		$unblublishMsg = Yii::t('app7020', 'Asset has been UNPUBLISHED from the Knowledge Library. You can find you asset in ');
		if ($data->hasReviewsAccess()) {
			$link = CHtml::link(
							Yii::t('app7020', 'My Contribution > In review'), Docebo::createAppUrl('app7020:knowledgeLibrary/index', array('tab' => 2)));
		} else {
			$link = CHtml::link(
							Yii::t('app7020', 'My Expert Tasks > Assets to approve/review'), Docebo::createAppUrl('app7020:guruDashboard/index'));
		}
		$this->widget('common.widgets.app7020AlertBox', array(
			'id' => 'publishUnpublish',
			'textSuccess' => $unblublishMsg . $link,
			'textError' => Yii::t('app7020', 'File cannot be unpublished!'),
		));
		?>
		<?php
		if ($data->defineContentAdminAccess() || Yii::app()->user->isGodAdmin) {
			?>
			<div class="main-actions clearfix">
				<!--Action buttons-->
				<ul class="clearfix" data-bootstro-id="customActionButtons">
					<?php if (Yii::app()->user->isGodAdmin) : ?>
						<li>
							<div>
								<a href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/settings', array('id' => $data->id)); ?>" id="customSettings">
									<span class="fa fa-gears"></span>
									<span class="boxTitle">
										<span class="innerBoxTitle"><?= Yii::t('app7020', 'Custom Settings') ?></span>
									</span>
								</a>
							</div>
						</li>
					<?php endif; ?>
					<li>
						<div>
							<a href="#" id="togglePeerReview" class="<?= $data->getUserRole() == App7020Assets::USER_ROLE_CONTRIBUTOR ? 'selected' : '' ?>">
								<span class="fa fa-pencil"></span>
								<span class="boxTitle">
									<span class="innerBoxTitle"><?= Yii::t('app7020', 'Peer Review') ?></span>
								</span>
							</a>
						</div>
					</li>
					<?php if($data->contentType== App7020Assets::CONTENT_TYPE_VIDEO) { ?>
					<li>
						<div>
							<a href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/tooltips', array('id' => $data->id)); ?>" id="customSettings">
								<span class="fa fa-comment"></span>
								<span class="boxTitle">
									<span class="innerBoxTitle"><?= Yii::t('app7020', 'Tooltips') ?></span>
								</span>
							</a>
						</div>
					</li>
					<?php } ?>
				</ul>
				<!--End-->
			</div>
			<?php
		}
		?>
		<section class="main-content" <?php echo isset($ngController) ? 'ng-controller="' . $ngController . '"' : ''; ?> <?php echo isset($ngInit) ? 'ng-init="init(' . $ngInit . ', ' . str_replace('"', "'", json_encode($timelineTooltips)) . ')"' : ''; ?>>
			<?php
			echo $this->renderPartial('admin/_contentInfoBlock', $publishAndEdit);
			?>


			<section class="row-fluid">
				<!--Content Place-->
				<section class="span8 leftSide">
					<div class="contentSection">
						<?php
						$this->widget('common.widgets.LmsContentReader', array('content' => $data, 'cuepoints' => !empty($cuepoints) ? $cuepoints : array()));
						?>
					</div>


					<h1 class="contentTitle"><?= ($data->title) ? $data->title : Yii::t('app7020', 'Empty'); ?></h1>
					<div class="contentInformation">
						<div class="contentInformationTop">
							<div class="avatar">
								<div data-custom="circle-avatar">
									<img alt="64x64" src="<?= $data->contributor->getAvatarImage(true); ?>">
								</div>
							</div>
							<div class="info">
								<div class="infoBox">
									<p class="user"><?= Yii::t('app7020', 'Contributor') ?>: <span><?= $data->contributor->getUserNameFormatted() ?></span></p>
									<p><?= Yii::t('app7020', 'Published') ?>: <span><?= App7020Helpers::timeAgo($data->created); ?> ago</span></p>
									<p><?= Yii::t('app7020', 'Viewed') ?>: <span><?= count($data->viewes) ?> times</span></p>
								</div>
								<div class="ratingBox">
									<p><?= Yii::t('app7020', 'Rate this asset') ?></p>
									<?php
									$this->widget('common.widgets.ContentRating', array('contentId' => $data->id, 'readOnly' => $disableRate));
									?>
									<span id="rating_score_<?= $data->id ?>">
										<?= App7020ContentRating::calculateContentRating($data->id) ?>
									</span>

								</div>
							</div>
							<div class="icons">
								<?php
								if (
										(App7020CustomContentSettings::getContentCustomSetting(App7020Assets::SETTING_ALLOW_DOWNLOAD, $data->id) == ShareApp7020Settings::ON ||
										ShareApp7020Settings::isGlobalDownloadAccess()) &&
										$data->contentType != App7020Assets::CONTENT_TYPE_LINKS &&
										$data->contentType != App7020Assets::CONTENT_TYPE_VIDEO &&
										$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_OTHER &&
										$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_ARCHIVE &&
										$data->contentType != App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC
								) :
									?>
									<a class="fa fa-download" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', 'Download asset') ?>" href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/forceDownload', array('file' => $data->id)); ?>" target="_blank">
									</a>
								<?php endif; ?>
			<!--<i class="fa fa-exclamation-triangle"></i>-->
							</div>
						</div>

						<a class="accordion-toggle show-details show hide" data-toggle="collapse" href="#details" data-parent="#details-container">
							<?= Yii::t('app7020', 'Show Details') ?>
						</a>


					</div>






				</section>
				<!--Online Chat-->
				<aside class="span4 rightSide <?= !$data->defineContentAdminAccess() ? 'app7020-learner-right' : ''; ?>">
					<?= $rightSidebar; ?>
				</aside>

				<?php if (isset($bottomFluid)) : ?>
					<div id="app7020BottomFluid">
						<?php echo $bottomFluid; ?>
					</div>
				<?php endif; ?>
			</section>
			<?php
			$this->widget('common.widgets.app7020AlertBox', array('id' => 'editForm'));
			?>
			<div id="details-container" class="accordion-group editMode" data-show="true">

				<div id="details" class="accordion-body in collapse">   
					<!-- Details -->
					<div class="row-fluid">
						<?php
						$this->renderPartial('app7020.protected.views.assets._assetModal', array('asset' => $data, 'editItemMode' => true));
						?>
					</div>
					<a class="accordion-toggle hide-details" data-toggle="collapse" href="#details" data-parent="#details-container">
						<?= Yii::t('app7020', 'Hide Details') ?>
					</a>
				</div>

			</div>



		</section>
	</div>

<?php else : ?>
	<div class="alert alert-error in alert-block fade"><?= Yii::t('app7020', 'Authorization error') ?></div>
<?php endif; ?>
