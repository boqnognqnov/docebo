<div class="app7020-hint app7020-assets_pageBlank_hint text-center">
    <h4><?php echo Yii::t('app7020', 'Hey, it looks empty here...'); ?></h4>
    <h3><?php echo '<span>'.Yii::t('app7020', 'Engage your users and make them create and upload').'</span>'; ?></h3>
    <h3><?php echo '<span>'.Yii::t('app7020', 'their knowledge assets').'</span>'; ?></h3>
</div>
<div class="app7020-stack text-right">
    <?php echo CHtml::image($this->getAssetsUrl() . '/images/tumbleweed.png');  ?>
</div>
