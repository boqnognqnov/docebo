<?php
$urlOfAvatar = CoreUser::model()->findByPk(Yii::App()->user->idst)->getAvatarImage(true);
/* @var $data App7020Content */
?>
<div id="knowledgeLibrarySingle" class="app7020DefaultStyles" data-asset-id="<?php echo $data->id; ?>">
	<?php
	$unblublishMsg = Yii::t('app7020', 'Asset has been UNPUBLISHED from the Knowledge Library. You can find you asset in ');
	if ($data->hasReviewsAccess()) {
		$link = CHtml::link(
						Yii::t('app7020', 'My Contribution > In review'), Docebo::createAppUrl('app7020:knowledgeLibrary/index', array('tab' => 2)));
	} else {
		$link = CHtml::link(
						Yii::t('app7020', 'My Expert Tasks > Assets to approve/review'), Docebo::createAppUrl('app7020:guruDashboard/index'));
	}
	?>
	<section class="main-content"
	<?php echo isset($ngController) ? 'ng-controller="' . $ngController . '"' : ''; ?>
	<?php echo isset($ngInit) ? 'ng-init="init(' . $ngInit . ', ' . str_replace('"', "'", json_encode($timelineTooltips)) . ')"' : ''; ?> >
			 <?php
				 if ($data->isContributor || $data->hasReviewsAccess() || Yii::app()->user->isGodAdmin) {
					 echo $this->renderPartial('admin/_contentInfoBlock', $publishAndEdit);
				 }
				 ?>

		<section class="row-fluid">
			<!--Content Place-->
			<section class="span8 leftSide">
				<div class="contentSection">
					<?php
					$this->widget('common.widgets.LmsContentReader', array('content' => $data, 'cuepoints' => !empty($cuepoints) ? $cuepoints : array()));
					?>
				</div>

			</section>
			<aside class="span4 rightSide <?= !$data->defineContentAdminAccess() ? 'app7020-learner-right' : ''; ?>">
				<?= $rightSidebar; ?>
			</aside>
			<?php
			if (isset($bottomFluid)) {
				?>
				<div id="app7020BottomFluid">
					<?php echo $bottomFluid; ?>
				</div>
				<?php
			}
			?>
		</section>
		<div class="row-fluid">
			<?php echo $bottomMeta; ?>
		</div>

		<div class="metaInformation">
			<h2><?= Yii::t('app7020', 'Ask the Experts') ?>!  </h2>
		</div>

	</section>
	<div id="questionsApp" ng-cloak >
		<div class="master-content" >
			<div ng-view=""></div>
		</div>
		<div class="go-to-top"></div>
	</div>
</div>