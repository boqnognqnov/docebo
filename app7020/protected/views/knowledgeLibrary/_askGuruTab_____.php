<div class="row-fluid singlePage" id="askGuru" data-container="root">
    <div id="gurusRow">
        <div class="app7020-hint app7020-knowledgeLibrary-askGuruTab" id="gurusHint">
    		<h4>
    			<?= Yii::t('app7020', 'The Experts <span>are here</span>'); ?>
    		</h4>
    		<br />
    		<h4>
    			<?= Yii::t('app7020', '<span>to</span> answer your questions!'); ?>
    		</h4>
    		
    	</div>
    	<?php $this->widget('common.widgets.GuruCirclesList', array('arrayData' => $arrayGurus, 'maxVisibleItems' => 2)); ?>
    	<?php 
    	//echo '<div class="rightSide">';
    	
    	//echo CHtml::textfield('search', '', array('class' => 'search', 'placeholder' => 'Search'));
    	//echo CHtml::link('', 'javascript:;', array('class' => 'searchBtn padding-0 fa-search'));
    	//echo '</div>';
    	?>
    </div>
    
    <div class="row-fluid singlePage" id="askGuruAddQuestion" data-container="root">
        <section class="row-fluid">
            <section class="span12" id="main-content-add-question">
                <div class="media online">
                    <div class="media-body">
                        <section id="answers">
                            <footer id="addNewAnswer">
                                <div class="signature" data-custom="circle-avatar">
                                    <img class="media-object" alt="64x64" src="<?= CoreUser::getAvatarByUserId(Yii::App()->user->idst); ?>">
                                </div>
    							<?= CHtml::link(Yii::t('app7020', 'Ask!'), 'javascript:;', array('class'=>'sendAnswerButton', 'onclick' => 'app7020.knowledgeSingle.addContentQuestion('.$contentId.')')); ?>
                                <?= CHtml::textArea('addnewAnsweraArea', '', array('placeholder' => Yii::t('app7020', 'Ask something ...'), 'rows' => '1')); ?>
                            </footer>
                        </section>
                    </div>
                </div>
            </section>
        </section>
    </div>


    <section class="row-fluid">
        <section class="span12 all-quetions" id="main-content">
			<div class="backdrop"></div>
 			<?php $this->widget('common.widgets.ComboListView', $listViewParams); ?>
        </section>
    </section>
</div>