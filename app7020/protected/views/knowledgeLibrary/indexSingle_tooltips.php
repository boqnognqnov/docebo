<?php
$urlOfAvatar = CoreUser::model()->findByPk(Yii::App()->user->idst)->getAvatarImage(true);
/* @var $data App7020Content */
?>
<div class="<?php echo $ie; ?>" id="knowledgeLibrarySingle" ng-controller="app7020AssetController">
	<!--Notify user msg widget-->
	<?php
		$unblublishMsg = Yii::t('app7020', 'Asset has been UNPUBLISHED from the Knowledge Library. You can find you asset in ');
		if ($data->hasReviewsAccess()) {
			$link = CHtml::link(
							Yii::t('app7020', 'My Contribution > In review'), Docebo::createAppUrl('app7020:knowledgeLibrary/index', array('tab' => 2)));
		} else {
			$link = CHtml::link(
							Yii::t('app7020', 'My Expert Tasks > Assets to approve/review'), Docebo::createAppUrl('app7020:guruDashboard/index'));
		}
		$this->widget('common.widgets.app7020AlertBox', array(
			'id' => 'publishUnpublish',
			'textSuccess' => $unblublishMsg . $link,
			'textError' => Yii::t('app7020', 'File cannot be unpublished!'),
		)); 
	?>
	
	<div class="app7020AssetModal" data-asset-id="<?php echo $data->id; ?>" ></div>
	
	<?php
	if ($data->defineContentAdminAccess() || Yii::app()->user->isGodAdmin) {
		?>
		<div class="main-actions clearfix">
			<!--Action buttons-->
			<ul class="clearfix" data-bootstro-id="customActionButtons">
				<?php
				if (Yii::app()->user->isGodAdmin) {
					?>

					<li>
						<div>
							<a href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/settings', array('id' => $data->id)); ?>" id="customSettings">
								<span class="fa fa-gears"></span>
								<span class="boxTitle">
									<span class="innerBoxTitle"><?= Yii::t('app7020', 'Custom Settings') ?></span>
								</span>
							</a>
						</div>
					</li>
					<?php
				}
				?>
				<li>
					<div>
						<a href="<?php echo isset($peerReviewLink) ? Docebo::createApp7020Url('knowledgeLibrary/editItem', array('id' => $data->id)) : '#' ?>" <?php echo !isset($peerReviewLink) ? 'id="togglePeerReview"' : ''; ?>>
							<span class="fa fa-pencil"></span>
							<span class="boxTitle">
								<span class="innerBoxTitle"><?= Yii::t('app7020', 'Peer Review') ?></span>
							</span>
						</a>
					</div>
				</li>
				<?php if(App7020Assets::isVideo($data['filename'])) : ?>
				
				<li class="<?= $isActive ? 'active' : '' ?>">
					<div>
						<a href="<?= !$isActive ? Docebo::createApp7020Url('knowledgeLibrary/tooltips', array("id" => $data->id)) : '#' ?>" id="tooltips">
							<span class="fa fa-comment"></span>
							<span class="boxTitle">
								<span class="innerBoxTitle"><?= Yii::t('app7020', 'Tooltips') ?></span>
							</span>
						</a>
					</div>
				</li>
				<?php endif; ?>
			</ul>
			<!--End-->
		</div>
		<?php
	}
	?>
	<section class="main-content" <?php echo isset($ngController) ? 'ng-controller="'.$ngController.'"': '';?> <?php echo isset($ngInit) ? 'ng-init="init('.$ngInit.', app7020Tooltips)"': '';?>>
		<?php if (Yii::app()->user->isGodAdmin || $data->hasReviewsAccess()) { ?>
			<!-- Publish/Unpublish block -->
			<div id="publishContainer">
				<?php echo $this->renderPartial('admin/_contentInfoBlock', $publishAndEdit); ?>
			</div>
		<?php } ?>
		<section class="row-fluid">
			<!--Content Place-->
			<section class="span8 leftSide">
				<div class="contentSection">
					<?php
					$this->widget('common.widgets.LmsContentReader', array('content' => $data, 'cuepoints' => !empty($cuepoints)?$cuepoints:array()));
					?>
				</div>
				<?php echo $bottomMeta; ?>
			</section>
			<!--Online Chat-->
			<aside class="span4 rightSide <?= !$data->defineContentAdminAccess()  ? 'app7020-learner-right' : ''; ?>">
				<?= $rightSidebar; ?>
			</aside>

			<?php
			if (isset($bottomFluid)) {
				?>
				
				<div id="app7020BottomFluid">
					<div class="headline"><span>Timeline</span></div>
					<?php echo $bottomFluid; ?>
				</div>
				<?php
			}
			?>
		</section>
		<!-- Details Form / Comments List / Ask the Guru {TABS} -->
		<div class="metaInformation">
			<div id="app7020-contribute-err-container"></div>
			<ul class="nav nav-tabs" id="metaInformation">
				<li class="active details">
					<a href="#details" aria-controls="details" role="tab" data-toggle="tab">
						Details
					</a>
				</li>
				<?php echo $ask_guru['tab']; ?>
			</ul>
			<div class="tab-content">
				<div class="tab-pane contributeContainer active" id="details" role="tabpanel">
					<?= $detailsForm; ?>
				</div>
				<?php echo $ask_guru['tab_layout']; ?>
			</div>
		</div> 
	</section>
</div>