<h3 class="title-bold back-button">
	<a href="<?php echo Docebo::createApp7020Url('knowledgeLibrary/item', array('id' => $id)); ?>">Back</a>
	<span><?= Yii::t('app7020', 'Custom Settings') ?></span>
</h3>
<div  id="app7020-peer-review-settings">
	<?php echo $form; ?>
</div>

