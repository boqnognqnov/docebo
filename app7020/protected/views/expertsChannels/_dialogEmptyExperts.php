<div class="app7020-dialogEmptyExperts">
	<div class="main-content">
		<p class="green"><?php echo Yii::t('app7020', 'Ops... to assign channel, you must'); ?></p>
		<p class="green"><?php echo Yii::t('app7020', 'select an Expert first.'); ?></p>
		<p class="grey"><?php echo Yii::t('app7020', 'Select one or more Expert first!'); ?></p>
	</div>
	<div class="action">
		<?php echo CHtml::link(Yii::t('app7020', 'Select Experts'), 'javascript:;') ?>
	</div>
</div>