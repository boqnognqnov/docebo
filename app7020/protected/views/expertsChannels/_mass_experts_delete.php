<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_DELETE_USERS?>,
			'idst': '<?= $idst ?>'
		}, function(data){
			removeModal();
			var dialogId = 'delete-users-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?php echo Yii::t('standard', '_DEL_SELECTED')?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>