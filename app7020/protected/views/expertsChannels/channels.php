<div class="main-section app7020DefaultStyles assignChannelContainer" id="app7020_Coach_Experts_Channels">
	<h1 class="title-bold back-button title">
		<a href="<?= Docebo::createApp7020Url('expertsChannels/index'); ?>"><?= Yii::t('standard', '_BACK'); ?></a>
		<span><?php echo Yii::t('app7020', 'Assign Channels'); ?></span>
	</h1>
	<div class="main-actions clearfix">
		<!--Action buttons-->
		<ul class="clearfix" data-bootstro-id="customActionButtons">
			<li>
				<div>
					<a href="javascript:void(0);" id="selectChannels">
						<span class="fa fa-plus-circle"></span>
						<span class="boxTitle">
							<span class="innerBoxTitle"><?= Yii::t('app7020', 'Assign Channels') ?></span>
						</span>
					</a>
				</div>
			</li>
		</ul>
		<!--End-->
	</div>
	<div class="bottom-section clearfix">
		<?php $this->renderPartial('_topicsList', array('listParams' => $listParams)); ?>
	</div>
</div>
