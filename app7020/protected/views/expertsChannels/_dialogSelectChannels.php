<div class="app7020FancyTreeFilterContainer" data-for="assingTopicsTree">
	
</div>

<?php
$this->widget('common.widgets.ComboGridView', $channelsGridViewParams);
echo CHtml::beginForm($url, 'post', array(
	'class' => 'ajax',
	'id' => 'dialog-channels-selection-form'
));

?>

<div class="form-actions">
	<?php
	
	echo CHtml::hiddenField("experts", $expertId);
	echo CHtml::hiddenField("channelsSelected");
	if(isset($showPrevious)){
		echo CHtml::button(Yii::t('app7020', 'Previous'), array('class' => 'prev-dialog green'));
	}
	echo CHtml::submitButton(Yii::t('app7020', 'Confirm'), array('class' => 'save-dialog green'));
	echo CHtml::button(Yii::t('app7020', 'Cancel'), array('class' => 'close-dialog black'));
	?>
</div>
<?php echo CHtml::endForm(); ?>
