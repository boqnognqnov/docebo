<div class="singleListItem">
	<span class="content"><?php echo $data['username']; ?></span>
    <span class="content"><?php echo $data['expertNames']; ?></span>
	<span class="content"><a href="javascript: void(0);"><span class="fa fa-users"></span><?php echo $data['topicsCount']; ?></a></span>
    <span class="customControls">
        <?php
            $title = '<i class="fa fa-times"></i>';
            echo CHtml::link($title, Docebo::createApp7020Url('topics/unAssignExperts', array(
                            'idTopic' => $data['expertId']
                        )), array(
                            'class' => 'open-dialog',
                            'data-dialog-id' => 'app7020-unassign-experts-dialog',
                            'data-dialog-class' => 'app7020-unassign-experts-dialog',
                            'data-dialog-title' => Yii::t('app7020', 'Unassign'),
                        )
                );
        ?>
    </span>
</div>