<div class="app7020-dialogEmptyChannels">
	<div class="main-content">
		<p class="green"><?php echo Yii::t('app7020', 'Ops... you can\'t select any Expert to assign'); ?></p>
		<p class="green"><?php echo Yii::t('app7020', 'because you haven\'t created any channels yet'); ?></p>
		<p class="grey"><?php echo Yii::t('app7020', 'Create one or more channels first!'); ?></p>
	</div>
	<div class="action">
		<?php echo CHtml::link(Yii::t('app7020', 'Create Channels'), Docebo::createAppUrl('app7020:channelsManagement/index')) ?>
	</div>
</div>