<?php

$url = Docebo::createApp7020Url('axExpertsChannels/AxDeleteChannels');
echo CHtml::beginForm($url, 'post', array(
    'class' => 'ajax',
    'id' => 'deleteExpertChannelsFormId'
));
?>

<div id="deleteExpertsFormContainer">
    <p class="topicInfo"> <?php echo Yii::t('app7020', 'Delete'). ' '. count($items) . ' ' . Yii::t('app7020', 'channels'); ?></p>
    <?= CHtml::checkBox('agreeCheck', false, array('id' => 'confirm')) ?>
    <?= CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'confirm', array('style' => 'display: inline-block')) ?>
</div>
<div class="form-actions">
    <?php
    echo CHtml::submitButton('Confirm', array('class' => 'save-dialog green'));
    echo CHtml::button('Cancel', array('class' => 'close-dialog'));
    ?>
</div>
<?php echo CHtml::hiddenField('forDelete', (count($items) > 1 ? implode(',', $items) : $items[0])); ?>
<?php
echo CHtml::endForm();
?>