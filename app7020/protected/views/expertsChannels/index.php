<?php
	
	$usermanContainerId			= "app7020_Coach_Experts_Channels";
	$usersGridId 				= "userman-users-management-grid";
	$searchFormId				= "userman-search-form";
	$selectedUsersFieldName 	= "userman_selected_users";
	$expertsCount = App7020Experts::sqlDataProvider()->totalItemCount;
?>


<div class="main-section app7020DefaultStyles" id="app7020_Coach_Experts_Channels">
	
	<p class="app7020-headline">
		<?php echo Yii::t('app7020', 'Create and assign Experts to Channels'); ?>
	</p>
	
	<div class="main-actions clearfix">
		<!--Action buttons-->
		<ul class="clearfix" data-bootstro-id="customActionButtons">
			<li>
				<div>
					<a href="javascript:void(0);" id="selectExperts">
						<span class="fa fa-user-plus"></span>
						<span class="boxTitle">
							<span class="innerBoxTitle"><?= Yii::t('app7020', 'Select experts') ?></span>
						</span>
					</a>
				</div>
			</li>

			<li>
				<div>
					<a href="javascript:void(0);" id="multipleAssign">
						<span class="fa fa-tv"></span>
						<span class="boxTitle">
							<span class="innerBoxTitle"><?= Yii::t('app7020', 'Multiple assign experts to channels') ?></span>
						</span>
					</a>
				</div>
			</li>

		</ul>
		<!--End-->
	</div>
	
	
	<div class="emptyTopicsAndExpertsHintSection" <?php if($expertsCount){?>style="display: none;" <?php }?>>
		<div class="app7020-hint app7020-experts-dialog-action-step1">
			<h4><?php echo Yii::t('app7020', 'Step 1'); ?></h4>
			<h3><?php echo Yii::t('app7020', '<span>Select users and make</span>'); ?></h3>
			<h3><?php echo Yii::t('app7020', '<span>them</span> Experts!'); ?></h3>
		</div>
		<div class="app7020-hint app7020-experts-dialog-action-step2">
			<h4><?php echo Yii::t('app7020', 'Step 2'); ?></h4>
			<h3><?php echo Yii::t('app7020', '<span>Select</span> Experts <span>and assign them</span>'); ?></h3>
			<h3><?php echo Yii::t('app7020', '<span>to one or more channels</span>'); ?></h3>
		</div>
	</div>
	
	<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => $searchFormId,
			'htmlOptions' => array(
        		'class' => 'form-inline',
    		),
		));
	?>

	<div class="main-section" <?php if(!$expertsCount){?>style="display: none;" <?php }?>>

		<div class="filters-wrapper">
			

			<div class="search-input-wrapper">
				<?php
					$params = array();
					$params['max_number'] = 10;
					$params['autocomplete'] = 1;
					$autoCompleteUrl = Docebo::createApp7020Url($userManAction, $params);
					$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
						'id' 				=> 'userman-search-input',
						'name' 				=> 'search_input',
						'value' 			=> '',
						'options' => array(
							'minLength' => 1,
						),
						'source' => $autoCompleteUrl,
						'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
					));
				?>
				<button type="button" class="close clear-search">&times;</button>
				<span 	class="perform-search search-icon"></span>
			</div>


												
		</div>
	</div>


	<?php
		echo CHtml::hiddenField($selectedUsersFieldName, '');
		$this->endWidget();
	?>
	
	<div class="selections clearfix" <?php if(!$expertsCount){?>style="display: none;" <?php }?>>

		<div class="left-selections clearfix">
			<p><?= Yii::t('standard', 'You have selected') ?> <strong><span id="userman-users-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>.</p>
			<a href="javascript:void(0);" class="userman-select-all"> <?= Yii::t('standard', '_SELECT_ALL') ?> </a>
			<a href="javascript:void(0);" class="userman-deselect-all"> <?= Yii::t('standard', '_UNSELECT_ALL') ?> </a>
		</div>


		<div class="right-selections clearfix">
			<label for="userman-users-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
			<select name="massive_action" id="userman-users-massive-action">
				<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
				<option value="deleteExperts"><?php echo Yii::t('standard', '_DEL'); ?></option>
			</select>
		</div>
	</div>
	
	
	<div class="bottom-section clearfix">
		<?php
		
			$this->renderPartial('_usersGrid', array(
				'usersGridId' 		=> $usersGridId,
				'selectedColumns'	=> $selectedColumns,
				'dataProvider' => App7020Experts::sqlDataProvider(),
			));
		?>
	</div>
</div>

<script type="text/javascript">
	var userManOptions = {
			containerId					: '<?= $usermanContainerId ?>',
			usersGridId					: '<?= $usersGridId ?>',
			searchFormId				: '<?= $searchFormId ?>',
			userManActionUrl			: '<?= Docebo::createApp7020Url($userManAction) ?>'
	};

	// See themes/spt/js/user_management.js
	var UserMan = new AdminUserManagement(userManOptions);

</script>