<div class="progressBarContentStyle">
	<p class="head clearfix">
		<span class="text"><?php echo Yii::t('app7020', 'Adding'); ?> <?php echo $expertsCount; ?> <?php echo Yii::t('app7020', 'Experts'); ?></span>
		<span class="percents">100%</span>
	</p>
	<div class="progress">
		<div class="bar bar-success" style="width: 100%;"></div>
    </div>
</div>
<div class="form-actions text-right">
	<?php echo CHtml::button(Yii::t('app7020', 'Close'), array('class' => 'close-dialog')); ?>
</div>