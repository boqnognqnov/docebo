<div id="myActivity" ng-controller="shareActivity">
	<?php if(count($channels)>0) {?>
		<h1><?= Yii::t('app7020', 'Channels where I`m an expert'); ?> :</h1>
		<div class="myChannelsList">
			<?php
			foreach ($channels as $channel) { ?>
				<span><?php echo $channel["translation"]["name"]; ?></span>
			<?php } ?>
		</div>
	<?php } ?>
	<div class="angular-tabs row-fluid">
		<div class="tabs span3">
			<h2><?= Yii::t('app7020', 'Main sections'); ?></h2>
			<ul class="ang-tabs span2">
				<li ng-class="{active:isSet(1)}"><a ng-click="setTab(1)" href="javascript:void(0)"><?= Yii::t('app7020', 'Sharing Activity'); ?></a></li>
				<li ng-class="{active:isSet(2)}"><a ng-click="setTab(2)" href="javascript:void(0)"><?= Yii::t('app7020', 'My most voted assets'); ?></a></li>
				<li ng-class="{active:isSet(3)}"><a ng-click="setTab(3)" href="javascript:void(0)"><?= Yii::t('app7020', 'My most watched assets'); ?></a></li>
				<li ng-class="{active:isSet(4)}"><a ng-click="setTab(4)" href="javascript:void(0)"><?= Yii::t('app7020', 'My most shared assets'); ?></a></li>
			</ul>
		</div>
		<div class="tabs-content span9">
			<h2 ng-show="isSet(1)"><?= Yii::t('app7020', 'Sharing Activity'); ?></h2>
			<h2 ng-show="isSet(2)"><?= Yii::t('app7020', 'My most voted assets'); ?></h2>
			<h2 ng-show="isSet(3)"><?= Yii::t('app7020', 'My most watched assets'); ?></h2>
			<h2 ng-show="isSet(4)"><?= Yii::t('app7020', 'My most shared assets'); ?></h2>
			<div ng-show="isSet(1)" class="filter">
				<label for="filter"><?php echo Yii::t('app7020','Timeframe'); ?></label>
				<select name="filter" id="filter" ng-model="timeframe" ng-options="tf.value for tf in timeframes track by tf.timeframe" ng-init="timeframe = timeframes[1]" ng-change = "newSelection()">
				</select>
			</div>
			<div ng-show="isSet(1)" id="coaching-tab" class="tab-content ng-cloak">
				<h3><?php echo Yii::t('app7020','Overview'); ?></h3>
				<ul class="data-boxes row-fluid">
					<li class="span4">
						<div class="span10">
							<div class="rating-stars">
								<i ng-class="{'fa-star':share_data.averageRating>=0.76,'fa-star-o':share_data.averageRating<=0.25,'fa-star-half-o':share_data.averageRating>=0.26 && share_data.averageRating<=0.75}" class="fa <?php echo $class1; ?> "></i>
								<i ng-class="{'fa-star':share_data.averageRating>=1.76,'fa-star-o':share_data.averageRating<=1.25,'fa-star-half-o':share_data.averageRating>=1.26 && share_data.averageRating<=1.75}" class="fa <?php echo $class2; ?> "></i>
								<i ng-class="{'fa-star':share_data.averageRating>=2.76,'fa-star-o':share_data.averageRating<=2.25,'fa-star-half-o':share_data.averageRating>=2.26 && share_data.averageRating<=2.75}" class="fa <?php echo $class3; ?> "></i>
								<i ng-class="{'fa-star':share_data.averageRating>=3.76,'fa-star-o':share_data.averageRating<=3.25,'fa-star-half-o':share_data.averageRating>=3.26 && share_data.averageRating<=3.75}" class="fa <?php echo $class4; ?> "></i>
								<i ng-class="{'fa-star':share_data.averageRating>=4.76,'fa-star-o':share_data.averageRating<=4.25,'fa-star-half-o':share_data.averageRating>=4.26 && share_data.averageRating<=4.75}" class="fa <?php echo $class5; ?> "></i>
							</div>
							<span class="text"><?php echo Yii::t('app7020',"My shared assets avg. rating"); ?></span>
						</div>
						<div class="span2 text-right">
							<span class="num">{{share_data.averageRating | number:2}}</span>
						</div>


					</li>
					<li class="span4">
						<span class="num">{{share_data.sharedContents}}</span>
						<span class="text"><?php echo Yii::t('app7020',"Shared contents"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{share_data.averageWatchRate}} %</span>
						<span class="text"><?php echo Yii::t('app7020',"Avg. watch rate"); ?></span>
					</li>
				</ul>
				<div ng-if="shareb.data.labels.length >0 || shared.data.length>0" class="charts share-charts clearfix">
					<div class="row-fluid">
						<div ng-if="shared.data.length>0" class="span6 dchart">
							<h2><?php echo Yii::t('app7020',"Channel sharing breaktrough"); ?></h2>
							<chart id="shared" value="shared" type="Doughnut" width="300" height="170"></chart>
							<div id="shared-tooltip" class="chartjs-tooltip"></div>
						</div>
					</div>
					<div class="row-fluid">
						<div ng-if="shareb.data.labels.length >0" class="span12">
							<h2><?php echo Yii::t('app7020',"My shared views and assets"); ?></h2>
							<ul class="bar-chart-legend">
								<li class="legend-assets"><i class="fa fa-stop"></i> assets</li>
								<li class="legend-views"><i class="fa fa-stop"></i> views</li>
							</ul>
							<div class="bar-chart">
								<chart id="shareb" value="shareb" type="Line" width="825" height="170"></chart>
								<div id="line-tooltip" class="chartjs-tooltip"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div ng-show="isSet(2)" id="peer-tab" class="tab-content ng-cloak">
				<?php $this->widget('common.widgets.app7020MyMostAssetsListView', $mostVotedList); ?>
			</div>
			<div ng-show="isSet(3)" id="peer-tab" class="tab-content ng-cloak">
				<?php $this->widget('common.widgets.app7020MyMostAssetsListView', $mostWatchedList); ?>
			</div>
			<div ng-show="isSet(4)" id="peer-tab" class="tab-content ng-cloak">
				<?php $this->widget('common.widgets.app7020MyMostAssetsListView', $mostSharedList); ?>
			</div>
		</div>
	</div>
</div>