<div id="myActivity" ng-controller="coachActivity">
	<?php if(count($channels)>0) {?>
		<h1><?= Yii::t('app7020', 'Channels where I`m an expert'); ?> :</h1>
		<div class="myChannelsList">
			<?php
			foreach ($channels as $channel) { ?>
				<span><?php echo $channel["translation"]["name"]; ?></span>
			<?php } ?>
		</div>
	<?php } ?>
	<div class="angular-tabs row-fluid">
		<div class="tabs span3">
			<h2><?= Yii::t('app7020', 'Main sections'); ?></h2>
			<ul class="ang-tabs span2">
				<li ng-class="{active:isSet(1)}"><a ng-click="setTab(1)" href="javascript:void(0)"><?= Yii::t('app7020', 'Coaching Activity'); ?></a></li>
				<li ng-class="{active:isSet(2)}"><a ng-click="setTab(2)" href="javascript:void(0)"><?= Yii::t('app7020', 'Peer Review Activity'); ?></a></li>
			</ul>
		</div>
		<div class="tabs-content span9">
			<h2 ng-show="isSet(1)"><?= Yii::t('app7020', 'Coaching Activity'); ?></h2>
			<h2 ng-show="isSet(2)"><?= Yii::t('app7020', 'Peer review Activity'); ?></h2>
			<div class="filter">
				<label for="filter"><?php echo Yii::t('app7020','Timeframe'); ?></label>
				<select name="filter" id="filter" ng-model="timeframe" ng-options="tf.value for tf in timeframes track by tf.timeframe" ng-init="timeframe = timeframes[1]" ng-change = "newSelection()">
				</select>
			</div>
			<div ng-show="isSet(1)" id="coaching-tab" class="tab-content ng-cloak">
				<h3><?php echo Yii::t('app7020','Overview'); ?></h3>
				<ul class="data-boxes row-fluid">
					<li class="span4">
						<span class="num">{{coach_data.answeredQuestions}}</span>
						<span class="text"><?php echo Yii::t("app7020","Questions i've answered"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.avgAnswerTime.hours}} <strong>h</strong> {{coach_data.avgAnswerTime.minutes}} <strong>m</strong></span>
						<span class="text"><?php echo Yii::t("app7020","Avg. answer time"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.requestsIsatisfy}}</span>
						<span class="text"><?php echo Yii::t("app7020","Requests i've satisfied"); ?></span>
					</li>
				</ul>
				<h3><?php echo Yii::t('app7020','Quality'); ?></h3>
				<ul class="data-boxes row-fluid">
					<li class="span4">
						<span class="num">{{coach_data.likes}}</span>
						<span class="text"><?php echo Yii::t("app7020","My answer's likes"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.dislikes}}</span>
						<span class="text"><?php echo Yii::t("app7020","My answer's dislikes"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.bestAnswers}}</span>
						<span class="text"><?php echo Yii::t('app7020','My answers marked as "best answer"'); ?></span>
					</li>
				</ul>
				<div ng-if="coachb.data.labels.length > 0 || coachd.data.length>0" class="charts">
					<div class="row-fluid">
						<div ng-if="coachd.data.length>0" class="span6 dchart">
							<h2><?php echo Yii::t('app7020',"My activity per channel"); ?></h2>
							<chart id="coachd" value="coachd" type="Doughnut" width="300" height="170"></chart>
							<div id="coachd-tooltip" class="chartjs-tooltip"></div>
						</div>
						<div class="span6 dchart">
							<h2><?php echo Yii::t('app7020',"My answer's likes and dislikes"); ?></h2>
							<chart id="coachl" value="coachl" type="Doughnut" width="300" height="170"></chart>
							<div id="coachl-tooltip" class="chartjs-tooltip"></div>
						</div>
					</div>
					<div class="row-fluid">
						<div ng-if="coachb.data.labels.length >0" class="span12">
							<h2><?php echo Yii::t('app7020',"Questions i've answered"); ?></h2>
							<div class="bar-chart">
								<chart id="coachb" value="coachb" type="Line" width="825" height="170"></chart>
								<div id="line-tooltip" class="chartjs-tooltip"></div>
							</div>
						</div>
					</div>
				</div>
				<ul class="table-top5">
					<li class="coach-activity .row-header row-fluid">
						<span class="cell cell-index span1"></span>
						<span class="cell cell-name span5"><h2><?php echo Yii::t('app7020',"Top 5 experts by answers quality"); ?></h2> <i class="fa fa-question-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', 'Social score based on the quality of the given answers')?>"></i></span>
						<span class="cell cell-best-answer span2"><i class="fa fa-check-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', 'Best answers')?>"></i></span>
						<span class="cell cell-like span2"><i class="fa fa-thumbs-o-up"></i></span>
						<span class="cell cell-dislike span2"><i class="fa fa-thumbs-o-down"></i></span>
					</li>
					<li ng-repeat="(key_aq, aq) in coach_data.expertsByAnswersQuality | limitTo: 5" ng-show="aq != null" class="coach-activity coach-activity row-fluid {{(coach_data.idst == aq.idst) ? 'current' : ''}}">
						<span class="cell cell-index span1">{{key_aq+1}}</span>
						<span class="cell cell-name span5"><span class="avatar" ng-bind-html="aq.avatar"></span> {{(aq.expert_name).length>1 ? aq.expert_name : aq.username}}</span>
						<span class="cell cell-best-answer span2">{{aq.count_bestAnswers}}</span>
						<span class="cell cell-like span2">{{aq.count_likes}}</span>
						<span class="cell cell-dislike span2">{{aq.count_dislikes}}</span>
					</li>
				</ul>
				<ul class="table-top5">
					<li class="coach-activity .row-header row-fluid">
						<span class="cell cell-index span1"></span>
						<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020',"Top 5 experts by first to answer rate"); ?></h2> <i class="fa fa-question-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', "How many questions i've answered first")?>"></i></span>
						<span class="cell cell-question-count span2"></span>
					</li>
					<li ng-repeat="(key_fa, fa) in coach_data.expertsByFirstToAnswer | limitTo: 5" ng-show="fa != null" class="coach-activity row-fluid {{(coach_data.idst == fa.idst) ? 'current' : ''}}">
						<span class="cell cell-index span1">{{key_fa+1}}</span>
						<span class="cell cell-name span9"><span class="avatar" ng-bind-html="fa.avatar"></span> {{(fa.expert_name).length>1 ? fa.expert_name : fa.username }}</span>
						<span class="cell cell-question-count span2">{{fa.count_answers}}</span>
					</li>
				</ul>
				<ul class="table-top5">
					<li class="coach-activity .row-header row-fluid">
						<span class="cell cell-index span1"></span>
						<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020',"Top 5 experts by participation rate"); ?></h2> <i class="fa fa-question-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="<?= Yii::t('app7020', "How many questions i've answered (in total)")?>"></i></span>
						<span class="cell cell-question-count span2"></span>
					</li>
					<li ng-repeat="(key_pr, pr) in coach_data.expertsBypartecipationRate | limitTo: 5" ng-show="pr != null" class="coach-activity row-fluid {{(coach_data.idst == pr.idst) ? 'current' : ''}}">
						<span class="cell cell-index span1">{{key_pr+1}}</span>
						<span class="cell cell-name span9"><span class="avatar" ng-bind-html="pr.avatar"></span> {{(pr.expert_name).length>1 ? pr.expert_name : pr.username}}</span>
						<span class="cell cell-question-count span2">{{pr.count_answers}}</span>
					</li>
				</ul>
			</div>
			<div ng-show="isSet(2)" id="peer-tab" class="tab-content ng-cloak">
				<h3><?php echo Yii::t('app7020','Overview'); ?></h3>
				<ul class="data-boxes row-fluid">
					<li class="span4">
						<span class="num">{{coach_data.reviewed_assets}}</span>
						<span class="text"><?php echo Yii::t('app7020',"Reviewed assets"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.peer_reviews}}</span>
						<span class="text"><?php echo Yii::t('app7020',"Written peer reviews"); ?></span>
					</li>
					<li class="span4">
						<span class="num">{{coach_data.assetsIpublished}}</span>
						<span class="text"><?php echo Yii::t('app7020',"Assets i've published"); ?></span>
					</li>
				</ul>
				<div ng-if="peerb.data.labels.length >0 || peerd.data.length>0" class="charts">
					<div class="row-fluid">
						<div ng-if="peerd.data.length>0" class="span6 dchart">
							<h2><?php echo Yii::t('app7020',"My activity per channel"); ?></h2>
							<chart id="peerd" value="peerd" type="Doughnut" width="300" height="170"></chart>
							<div id="peerd-tooltip" class="chartjs-tooltip"></div>
						</div>
					</div>
					<div class="row-fluid">
						<div ng-if="peerb.data.labels.length >0" class="span12">
							<h2><?php echo Yii::t('app7020',"Reviewed assets"); ?></h2>
							<div class="bar-chart">
								<chart id="peerb" value="peerb" type="Line" width="825" height="170"></chart>
								<div id="line-tooltip2" class="chartjs-tooltip"></div>
							</div>
						</div>
					</div>
				</div>
				<ul class="table-top5">
					<li class="coach-activity .row-header row-fluid">
						<span class="cell cell-index span1"></span>
						<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020',"Top 5 experts by participation rate"); ?></h2> <i class="fa fa-question-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="How many questions i've reviewed (in total)"></i></span>
						<span class="cell cell-question-count span2"></span>
					</li>
					<li ng-repeat="(key_epr, epr) in coach_data.topExpertsByPartecipationRate | limitTo: 5" ng-show="epr != null" class="coach-activity row-fluid {{(coach_data.idst == epr.idst) ? 'current' : ''}}">
						<span class="cell cell-index span1">{{key_epr+1}}</span>
						<span class="cell cell-name span9"><span class="avatar" ng-bind-html="epr.avatar"></span> {{(epr.expert_name).length>1 ? epr.expert_name : epr.username}}</span>
						<span class="cell cell-question-count span2">{{epr.count_reviews}}</span>
					</li>
				</ul>
				<ul class="table-top5">
					<li class="coach-activity .row-header row-fluid">
						<span class="cell cell-index span1"></span>
						<span class="cell cell-name span9"><h2><?php echo Yii::t('app7020',"Top 5 experts by first to review"); ?></h2> <i class="fa fa-question-circle" data-placement="top" rel="tooltip" data-toggle="tooltip" data-original-title="How many questions i've reviewed first"></i></span>
						<span class="cell cell-question-count span2"></span>
					</li>
					<li ng-repeat="(key_efr, efr) in coach_data.topExpertsByFirstToReview | limitTo: 5" ng-show="efr != null" class="coach-activity row-fluid {{(coach_data.idst == efr.idst) ? 'current' : ''}}">
						<span class="cell cell-index span1">{{key_efr+1}}</span>
						<span class="cell cell-name span9"><span class="avatar" ng-bind-html="efr.avatar"></span> {{(efr.expert_name).length>1 ? efr.expert_name : efr.username }}</span>
						<span class="cell cell-question-count span2">{{efr.count_reviews}}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>