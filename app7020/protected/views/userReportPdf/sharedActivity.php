<div id="userReportSharingActivityPdf">
	<h2><?php echo Yii::t('app7020', 'Sharing Activity'); ?></h2>
	<h3><?php echo Yii::t('app7020', 'Timeframe') . ': ' . $renderData['timeframeStr']; ?></h3>

	<!--Info Boxes -->
	<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
	<div class="row-fluid">
		<div class="data-box span4 left">
			<span class="num"><?php echo $renderData['averageRating']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Shared assets avg. rating'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['sharedContents']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Shared contents'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['totalViews']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Total views'); ?></span>
		</div>
		<div class="left data-box span4">
			<span class="num"><?php echo $renderData['averageWatchRate']; ?>%</span><br>
			<span><?php echo Yii::t('app7020', 'Avg. watch rate'); ?></span>
		</div>
	</div>
	<!-- Chart Activity per channel -->
	<h4><?php echo Yii::t('app7020', 'Activity per channel'); ?></h4>
	<div class="chartContainer">
		<?php foreach ($renderData['chartMyActivityPerChannel'] as $myActivityPerChannel): ?>
			<div class="chartRow">
				<div>
					<?php echo $myActivityPerChannel['label'] . ': ' . round($myActivityPerChannel['value']) . '%'; ?>
				</div>
				<div style="width: <?php echo round($myActivityPerChannel['value']); ?>%; height: 12px; background-color: <?php echo $myActivityPerChannel['color']; ?>;"></div>
			</div>
		<?php endforeach; ?>
	</div>

	<!-- Shared assets (contributions) -->
	<h4><?php echo Yii::t('app7020', "My Shared assets (contributions)"); ?></h4>
	<img src="<?php echo $renderData['chartSharedAssets']; ?>">

	<!-- Assets views -->
	<h4><?php echo Yii::t('app7020', "Shared assets (contributions)"); ?></h4>
	<img src="<?php echo $renderData['chartAssetsViews']; ?>">

</div>
