<?php
$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
$avatarImg = $storage->absoluteBaseUrl . '/avatar/';
?>

<div id="userReportQuestionsAndAnswersPdf">
	<h2><?php echo Yii::t('app7020', 'Questions & Answers'); ?></h2>
	<h3><?php echo Yii::t('app7020', 'Timeframe') . ': ' . $renderData['timeframeStr']; ?></h3>

	<!--Info Boxes -->
	<h4><?php echo Yii::t('app7020', 'Overview'); ?></h4>
	<div class="row-fluid">
		<div class="data-box span4 left">
			<span class="num"><?php echo $renderData['infoBoxes']['answeredQuestions']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Answered questions'); ?></span>
		</div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['infoBoxes']['questionsMade']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Question made'); ?></span>
		</div>
	</div>
	<div class="row-fluid">
		<h4><?php echo Yii::t('app7020', 'Quality'); ?></h4>
		<div class="clear"></div>
		<div class="data-box span4">
			<span class="num"><?php echo $renderData['infoBoxes']['likes']; ?></span><br>
			<span><?php echo Yii::t('app7020', "Answers' likes"); ?></span>
		</div>
		<div class="left data-box span4">
			<span class="num"><?php echo $renderData['infoBoxes']['dislikes']; ?></span><br>
			<span><?php echo Yii::t('app7020', "Answers' dislikes"); ?></span>
		</div>
		<div class="left data-box span4">
			<span class="num"><?php echo $renderData['infoBoxes']['bestAnswers']; ?></span><br>
			<span><?php echo Yii::t('app7020', 'Answers marked as "Best Answer"'); ?></span>
		</div>
	</div>


	<!-- Chart Activity per channel -->
	<h4><?php echo Yii::t('app7020', 'Activity per channel'); ?></h4>
	<div class="chartContainer">
		<?php foreach ($renderData['chartMyActivityPerChannel'] as $myActivityPerChannel): ?>
			<div class="chartRow">
				<div>
					<?php echo $myActivityPerChannel['label'] . ': ' . round($myActivityPerChannel['value']) . '%'; ?>
				</div>
				<div style="width: <?php echo round($myActivityPerChannel['value']); ?>%; height: 12px; background-color: <?php echo $myActivityPerChannel['color']; ?>;"></div>
			</div>
		<?php endforeach; ?>
	</div>

	<!-- Chart 'Questions vs Answers' -->
	<h4 class="border"><?php echo Yii::t('app7020', "Questions vs Answers"); ?></h4>

	<table style="width:100%" class="legend">
		<tr>
			<td class="green"></td>
			<td class="text"><?= Yii::t('app7020', 'questions') ?></td>
			<td class="blue"></td>
			<td class="text"><?= Yii::t('app7020', 'answers') ?></td>
		</tr>
	</table>

	<img src="<?php echo $renderData['chartQuestionsAnswers']; ?>">



	<h4><?php echo Yii::t('app7020', "Rank by answers quality"); ?>
	</h4>	
	<div class="subTitle"><?php echo Yii::t('app7020', "Social score based on the quality of the given answers"); ?></div>
	<!-- Rank by answers quality -->
	<table style="width:100%" class="tableRanckings">
		<tr class="no-border">
			<td colspan="3"></td>
			<td class="text-center" >
				<h4>
					<?php echo Yii::t('app7020', "Best answers"); ?>
				</h4>	
			</td>
			<td class="text-center"> 
				<h4>
					<?php echo Yii::t('app7020', "Likes"); ?>
				</h4>
			</td>
			<td class="text-center">
				<h4>
					<?php echo Yii::t('app7020', "Dislikes"); ?>
				</h4>
			</td>
		</tr>
		<?php
		$counterExpertsByAnswersQuality = 0;
		?>
		<?php foreach ($renderData['rankExpertsByAnswersQuality'] as $expertsByAnswersQuality): ?>
			<?php
			$counterExpertsByAnswersQuality++;
			$currentUserClass = $expertsByAnswersQuality['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr>
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByAnswersQuality; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php if ($expertsByAnswersQuality['avatar_db']): ?>
						<img style="border-radius:50px;" class="expertAvatarImg" src="<?php echo $avatarImg . $expertsByAnswersQuality['avatar_db']; ?>">
					<?php else: ?>
						<div class="expertAvatarNoImg">
							<?php
							if ($expertsByAnswersQuality['firstname'] && $expertsByAnswersQuality['lastname']) {
								echo mb_substr($expertsByAnswersQuality['firstname'], 0, 1) . mb_substr($expertsByAnswersQuality['lastname'], 0, 1);
							}
							?>

						</div>
					<?php endif; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByAnswersQuality['expert_name']) {
						echo $expertsByAnswersQuality['expert_name'];
					} else {
						echo $expertsByAnswersQuality['username'];
					}
					?>
				</td>
				<td class="text-center <?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_bestAnswers']; ?>
				</td>
				<td class="text-center <?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_likes']; ?>
				</td>
				<td class="text-center <?php echo $currentUserClass; ?>">
					<?php echo $expertsByAnswersQuality['count_dislikes']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<h4>
		<?php echo Yii::t('app7020', "Rank by first to answer rate"); ?>
	</h4>	
	<div class="subTitle">
		<?php echo Yii::t('app7020', "How many questions expert answered first"); ?>
	</div>
	<!-- Rank by first to answer rate -->
	<table style="width:100%" class="tableRanckings">
		<?php
		$counterExpertsByFirstToAnswer = 0;
		?>
		<?php foreach ($renderData['rankExpertsByFirstToAnswer'] as $expertsByFirstToAnswer): ?>
			<?php
			$counterExpertsByFirstToAnswer++;
			$currentUserClass = $expertsByFirstToAnswer['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class=" <?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByFirstToAnswer; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php if ($expertsByFirstToAnswer['avatar_db']): ?>
						<img class="expertAvatarImg" src="<?php echo $avatarImg . $expertsByFirstToAnswer['avatar_db']; ?>">
					<?php else: ?>
						<div class="expertAvatarNoImg">
							<?php
							if ($expertsByFirstToAnswer['firstname'] && $expertsByFirstToAnswer['lastname']) {
								echo mb_substr($expertsByFirstToAnswer['firstname'], 0, 1) . mb_substr($expertsByFirstToAnswer['lastname'], 0, 1);
							}
							?>

						</div>
					<?php endif; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByFirstToAnswer['expert_name']) {
						echo $expertsByFirstToAnswer['expert_name'];
					} else {
						echo $expertsByFirstToAnswer['username'];
					}
					?>
				</td>
				<td  class="text-center <?php echo $currentUserClass; ?>">
					<?php echo $expertsByFirstToAnswer['count_answers']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

	<h4><?php echo Yii::t('app7020', "Rank by participation rate"); ?></h4>	
	<div class="subTitle"><?php echo Yii::t('app7020', "How many questions expert answered (in total)"); ?></div>
	<!-- Rank by participation rate -->
	<table style="width:100%" class="tableRanckings">
		<?php
		$counterExpertsByPartecipationRate = 0;
		?>
		<?php foreach ($renderData['rankExpertsByPartecipationRate'] as $expertsByPartecipationRate): ?>
			<?php
			$counterExpertsByPartecipationRate++;
			$currentUserClass = $expertsByPartecipationRate['idst'] == $renderData['userModel']->idst ? 'currentUser' : '';
			?>
			<tr class="currentUserRow">
				<td class="<?php echo $currentUserClass; ?>">
					<?php echo $counterExpertsByPartecipationRate; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php if ($expertsByPartecipationRate['avatar_db']): ?>
						<img class="expertAvatarImg" src="<?php echo $avatarImg . $expertsByPartecipationRate['avatar_db']; ?>">
					<?php else: ?>
						<div class="expertAvatarNoImg">
							<?php
							if ($expertsByPartecipationRate['firstname'] && $expertsByPartecipationRate['lastname']) {
								echo mb_substr($expertsByPartecipationRate['firstname'], 0, 1) . mb_substr($expertsByPartecipationRate['lastname'], 0, 1);
							}
							?>
						</div>
					<?php endif; ?>
				</td>
				<td class="<?php echo $currentUserClass; ?>">
					<?php
					if ($expertsByPartecipationRate['expert_name']) {
						echo $expertsByPartecipationRate['expert_name'];
					} else {
						echo $expertsByPartecipationRate['username'];
					}
					?>
				</td>
				<td class="text-center <?php echo $currentUserClass; ?>">
					<?php echo $expertsByPartecipationRate['count_answers']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</table>

</div>




