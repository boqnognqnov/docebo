<div id="guruDashboard" class="main-section app7020DefaultStyles">
	<div id="navigation" class="row-fluid">
		<div class="span3">
			<div class="pull-left">
				<div class="myActivities">
					<a href="javascript:;">
						<div class="signature" data-custom="circle-avatar">
							<img class="media-object" alt="64x64" src="<?= CoreUser::getAvatarByUserId(Yii::app()->user->idst); ?>">
						</div>
						<div class="labels">
							<p class="name"><?= CoreUser::model()->findByPk(Yii::app()->user->idst)->getFullName() ?></p>
							<p class="role"><?= Yii::t("app7020", "Expert"); ?></p>
							<!--<p class="link">My expert activity</p>-->
						</div>
					</a>
				</div>
			</div>
		</div>
		<div class="span9">
			<div class="pull-right">
				<?php $this->widget('common.widgets.GuruActivity', array('isTab' => true, 'autoSelect' => $_GET['tab'])); ?>
			</div>
		</div>
	</div>
	<div id="main-content" class="row-fluid">
		<div class="tab-content">

			<!--TAB SATISFY-->
			<div role="tabpanel" class="tab-pane <?= ($_GET['tab'] == 'activityRequestsSatisfy') ? 'active' : '' ?><?php echo (!$_GET['tab']) ? 'active' : '' ?>" id="activityRequestsSatisfy" data-reference="requests">
				<?php
				$app7020Filters_satisfy = array('toggleGrid' => false, 'topics' => false, 'topicsNew' => true, 'comboListViewId' => 'newOnlySatisfyComboListView,inChargeComboListView,ignoredSatisfyComboListView');
				?>
				<?php // $this->widget('common.widgets.app7020Filters', $app7020Filters_satisfy); ?>
				<?php $this->widget('common.widgets.app7020NavigationMenu', array('links' => $navigationMenuLinks_satisfy, 'oldStyle' => false)); ?>

			</div>

			<!--TAB ASSETS-->
			<div role="tabpanel" class="tab-pane <?= ($_GET['tab'] == 'activityAssetsReview') ? 'active' : '' ?>" id="activityAssetsReview" data-reference="assets">
				<?php
				$app7020Filters_assets = array('toggleGrid' => false, 'topics' => false, 'topicsNew' => true, 'comboListViewId' => 'newOnlyAssetsComboListView,reviewAssetsComboListView,ignoredAssetsComboListView');
				?>
				<?php // $this->widget('common.widgets.app7020Filters', $app7020Filters_assets); ?>
				<?php $this->widget('common.widgets.app7020NavigationMenu', array('links' => $navigationMenuLinks_assets, 'select'=>2, 'oldStyle' => false)); ?>

			</div>

			<!--TAB QUESTIONS-->
			<div role="tabpanel" class="tab-pane <?= ($_GET['tab'] == 'activityQuestions') ? 'active' : '' ?>" id="activityQuestions" data-reference="questions">
				<?php
				$app7020Filters_questions = array('toggleGrid' => false, 'topics' => false, 'topicsNew' => true, 'comboListViewId' => 'newOnlyQuestionsComboListView,reviewQuestionsComboListView,ignoredQuestionsComboListView');
				?>
				<?php // $this->widget('common.widgets.app7020Filters', $app7020Filters_questions); ?>
				<?php $this->widget('common.widgets.app7020NavigationMenu', array('links' => $navigationMenuLinks_questions, 'oldStyle' => false)); ?>
			</div>

		</div>
	</div>
</div>