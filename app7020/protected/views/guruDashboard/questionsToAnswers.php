<div class="main-wrapper" ng-init="init(false)" ng-show="loaded" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite">

	<div class="heading" ng-show="filter == 'openQuestions' && data.questions.open.items.length">
		<h1><?php echo Yii::t('app7020', 'Open questions'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the questions assigned to you, where you and/or other experts already provided an answer. '
				. '<strong>Open questions will be automaticallly removed from here as soon as a best answer is choosen</strong>'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'newOnly' && data.questions.new.items.length">
		<h1><?php echo Yii::t('app7020', 'New Only'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the NEW questions assigned to you, where you haven\'t provided an answer yet.'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'silenced' && data.questions.ignored.items.length">
		<h1><?php echo Yii::t('app7020', 'Silenced'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are your silenced questions. You won\'t be notified for any change that happens. You can re-enable notifications or, if no action is taken, they will be automatically removed from here as soon as a best answer is choosen. Alternatively, you can manua'); ?></p>
	</div>

	<div class="info-wrapper clearfix" ng-show="
		filter == 'openQuestions' && data.questions.open.items.length ||
		filter == 'newOnly' && data.questions.new.items.length ||
		filter == 'silenced' && data.questions.ignored.items.length">
		<div class="counter pull-left">
			<strong ng-if="filter == 'openQuestions'">{{data.questions.open.questions_count}}</strong>
			<strong ng-if="filter == 'newOnly'">{{data.questions.new.questions_count}}</strong>
			<strong ng-if="filter == 'silenced'">{{data.questions.ignored.questions_count}}</strong>
		<?php echo Yii::t('app7020', 'items'); ?></div>
		<div class="clear pull-right" ng-show="filter == 'silenced'" ng-click="clearSilenced('question');"><?php echo Yii::t('app7020', 'Clean silenced list'); ?></div>
	</div>
	<div class="blank-state" ng-show="
		filter == 'openQuestions' && !data.questions.open.questions_count ||
		filter == 'newOnly' && !data.questions.new.questions_count ||
		filter == 'silenced' && !data.questions.ignored.questions_count">
		<i class="fa fa-comment-o"></i>
		<h2><?php echo Yii::t('app7020', 'Hey, you don\'t have any questions to answer'); ?></h2>
	</div>

	<div class="items-wrapper" questions-items items="data.questions.open.items" ng-if="filter == 'openQuestions'"></div>
	<div class="items-wrapper" questions-items items="data.questions.new.items" ng-if="filter == 'newOnly'"></div>
	<div class="items-wrapper" questions-items items="data.questions.ignored.items" ng-if="filter == 'silenced'"></div>
</div>