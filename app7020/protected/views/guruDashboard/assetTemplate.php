<div class="assetBoxModel" ng-repeat="item in items| filter: itemsFilter" ng-click="redirect(item.link_to_asset, $event)" ng-class="{'silence-show-before': item.ignore_state == 1, 'silence-show-after': item.ignore_state == 2, 'ignored-style': item.ignored}">
	<silence-directive>
		<i class="fa fa-times"></i>
		<div class="before" ng-if="!item.ignored">
			<h2><?php echo Yii::t('app7020', 'Silence this asset?'); ?></h2>
			<p><?php echo Yii::t('app7020', 'You won\'t be notified for any change that happens. You can re-enable notifications or, '
					. 'you can manually clean them.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Silence'); ?></button>
		</div>
		<div class="after" ng-if="!item.ignored">
			<h2><?php echo Yii::t('app7020', 'The asset has been moved to'); ?>
				<i ng-click="redirect([':filter?', 'silenced'], $event, true)"><?php echo Yii::t('app7020', 'Silenced'); ?></i>
			</h2>
			<p><?php echo Yii::t('app7020', 'You won\'t be notified anymore for any change that happens.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Undo'); ?></button>
		</div>
		<div class="before" ng-if="item.ignored">
			<h2><?php echo Yii::t('app7020', 'Re-enable notifications for this asset?'); ?></h2>
			<p><?php echo Yii::t('app7020', 'The asset will be moved to the In charge list.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Confirm'); ?></button>
		</div>
		<div class="after" ng-if="item.ignored">
			<h2><?php echo Yii::t('app7020', 'The asset has been moved to'); ?>
				<i ng-click="redirect([':filter?', 'inCharge'], $event, true)"><?php echo Yii::t('app7020', 'In charge'); ?></i>
			</h2>
			<p><?php echo Yii::t('app7020', 'You will be notified for any change that happens.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Undo'); ?></button>
		</div>
	</silence-directive>
	<div class="thumb-wrapper">
		<img ng-src="{{item.thumbnail}}" alt="<?php echo Yii::t('app7020', 'Image'); ?>"/>
		<div class="time-wrapper" ng-if="item.asset_type == 'video'">
			<span class="duration">{{item.duration}}</span>
		</div>
		<div class="type-wrapper" ng-if="item.asset_type">
			<span class="type">{{item.asset_type}}</span>
		</div>
	</div>
	<div class="content-wrapper">
		<action-icons actions="item.actions"></action-icons>
		<h3 class="title">
			<span class="text" build-tinymce="{{item.asset_id}}" ng-bind-html="item.title | HtmlToPlainText"></span>
		</h3>
		<div class="footer clearfix">
			<div class="pull-left">
				<div class="new-wrapper" ng-if="assetAllow.footer.indexOf('new') > -1 && item.is_new">
					<span class="new"><?php echo Yii::t('app7020', 'New'); ?></span>
				</div>
				<div class="peerReviews-wrapper" ng-if="assetAllow.footer.indexOf('peerReviews') > -1">
					<span class="count">{{item.total_peer_reviews}}</span>
					<span class="suffix"><?php echo Yii::t('app7020', 'peer reviews'); ?></span>
				</div>
			</div>
			<div class="pull-right">
				<div class="author-wrapper" ng-if="assetAllow.footer.indexOf('author') > -1">
					<p class="text-label">
						<span class="prefix"><?php echo Yii::t('app7020', 'by'); ?></span>
						<span ng-click="redirect(item.owner_channel_link, $event)">
							<span class="name">{{item.owner}}</span>
						</span>
					</p>
				</div>
				<div class="time-wrapper" ng-if="assetAllow.footer.indexOf('time') > -1">
					<p class="text-label">
						<span class="prefix"><?php echo Yii::t('app7020', 'updated'); ?></span>
						<span class="count">{{item.update_date}}</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>