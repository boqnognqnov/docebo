<div class="questionBoxModel" ng-repeat="item in items| filter: itemsFilter" ng-class="{'silence-show-before': item.ignore_state == 1, 'silence-show-after': item.ignore_state == 2, 'ignored-style': item.ignored, 'assign-in-progress': item.assign_state > 0}">
	<silence-directive>
		<i class="fa fa-times"></i>
		<div class="before" ng-if="!item.ignored">
			<h2><?php echo Yii::t('app7020', 'Silence this question?'); ?></h2>
			<p><?php
				echo Yii::t('app7020', 'You won\'t be notified for any change that happens. You can re-enable notifications or, '
						. 'if no action is taken, they will be automatically removed from the silenced list as soon as a best answer is choosen, '
						. 'or you can manually clean them.');
				?></p>
			<button><?php echo Yii::t('app7020', 'Silence'); ?></button>
		</div>
		<div class="after" ng-if="!item.ignored">
			<h2><?php echo Yii::t('app7020', 'The question has been moved to'); ?>
				<i ng-click="redirect([':filter?', 'silenced'], $event, true)"><?php echo Yii::t('app7020', 'Silenced'); ?></i>
			</h2>
			<p><?php echo Yii::t('app7020', 'You won\'t be notified anymore for any change that happens.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Undo'); ?></button>
		</div>
		<div class="before" ng-if="item.ignored">
			<h2><?php echo Yii::t('app7020', 'Re-enable notifications for this question?'); ?></h2>
			<p><?php
				echo Yii::t('app7020', 'The question will be moved to the open questions list. '
						. 'Open questions will be automatically removed from the list as soon as a best answer is choosen');
				?></p>
			<button><?php echo Yii::t('app7020', 'Confirm'); ?></button>
		</div>
		<div class="after" ng-if="item.ignored">
			<h2><?php echo Yii::t('app7020', 'The question has been moved to'); ?>
				<i ng-click="redirect([':filter?', 'silenced'], $event, true)"><?php echo Yii::t('app7020', 'Open questions'); ?></i>
			</h2>
			<p><?php echo Yii::t('app7020', 'You will be notified for any change that happens.'); ?></p>
			<button><?php echo Yii::t('app7020', 'Undo'); ?></button>
		</div>
	</silence-directive>
	<div ng-bind-html="item.avatar" class="avatar-wrapper"></div>
	<div class="content-wrapper">
		<action-icons actions="item.actions"></action-icons>
		<h3 class="title" ng-click="redirect(item.link_to_question, $event)">
			<span class="prefix" ng-if="questionAllow.title_prefix"><?php echo Yii::t('app7020', 'Your last answer to'); ?>:</span>
			<span class="text" build-tinymce="{{item.question_id}}" ng-bind-html="to_trusted(item.question)"></span>
		</h3>
		<div class="quick_answer" ng-show="item.show_quick_answer === true" data-question-id="{{item.question_id}}">
			<div ng-bind-html="item.current_user_avatar" class="avatar-wrapper"></div>
			<div class="dismiss" ng-click="item.show_quick_answer = false; item.quick_answer = ''"><?php echo Yii::t('app7020', 'Dismiss'); ?></div>
			<div class="content-wrapper">
				<textarea class="quick_answer_textarea" ui-tinymce="tinymceOptions" ng-model="item.quick_answer"></textarea>
			</div>
		</div>
		<div class="bestAnswer" ng-if="item.have_best_answer" ng-hide="item.show_quick_answer === true" data-answer-id="{{item.answer.answer_id}}">
			<span class="fa-wrapper">
				<i class="fa fa-check-circle"></i>
			</span>
			<p class="answer">
				<span class="prefix"><?php echo Yii::t('app7020', 'Best answer'); ?>{{item.answer.author}}:</span>
				<span ng-bind-html='to_trusted(item.answer.text)' class="text"></span>
			</p>
		</div>
		<div class="lastAnswer" ng-if="item.answer.answer_id && !item.have_best_answer" ng-hide="item.show_quick_answer === true" data-answer-id="{{item.answer.answer_id}}">
			<span class="prefix"><?php echo Yii::t('app7020', 'Last answer by'); ?> {{item.answer.author}}:</span>
			<span ng-bind-html='to_trusted(item.answer.text)' class="answer"></span>
		</div>
		<div class="footer" ng-hide="item.show_quick_answer === true">
			<div class="related">
				<span ng-if="questionAllow.footer.indexOf('related') > -1 && item.other_fields.asset" ng-click="redirect(item.other_fields.asset.asset_page_url, $event)">
					<div class="image-wrapper">
						<img ng-src="{{item.other_fields.asset.asset_preview}}" alt="<?php echo Yii::t('app7020', 'Image Not found'); ?>"/>
					</div>
					<div class="text-wrapper">
						<span class="prefix"><?php echo Yii::t('app7020', 'This question is related to'); ?>:</span>
						<span class="link">{{item.other_fields.asset.asset_title}}</span>
					</div>
				</span>
				<span ng-if="questionAllow.footer.indexOf('related') > -1 && !item.other_fields.asset && item.other_fields.channel" ng-click="redirect(item.other_fields.channel.channel_url, $event)">
					<div class="image-wrapper" ng-style="{background: item.other_fields.channel.channel_color}">
						<i class="fa {{item.other_fields.channel.channel_icon}}" ng-style="{color: item.other_fields.channel.channel_icon_color}"></i>
					</div>
					<div class="text-wrapper">
						<span class="prefix"><?php echo Yii::t('app7020', 'This question is associated to channel'); ?>:</span>
						<span class="link">{{item.other_fields.channel.channel_title}}</span>
					</div>
				</span>
			</div>
			<div class="author-wrapper" ng-if="questionAllow.footer.indexOf('author') > -1">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'by'); ?></span>
					<span ng-click="redirect(item.owner_channel_link, $event)">
						<span class="name">{{item.owner}}</span>
					</span>
				</p>
			</div>
			<div class="views-wrapper" ng-if="questionAllow.footer.indexOf('views') > -1">
				<p class="text-label">
					<span class="count">{{item.views}}</span>
					<span class="suffix"><?php echo Yii::t('app7020', 'views'); ?></span>
				</p>
			</div>
			<div class="time-wrapper" ng-if="questionAllow.footer.indexOf('time') > -1">
				<p class="text-label">
					<span class="prefix"><?php echo Yii::t('app7020', 'updated'); ?></span>
					<span class="count">{{item.update_date}}</span>
				</p>
			</div>
			<div class="answers-wrapper" ng-if="questionAllow.footer.indexOf('answers') > -1">
				<p class="text-label" ng-click="redirect(item.link_to_question, $event)">
					<span class="no-answers" ng-show="item.total_number_answers == 0"><?php echo Yii::t('app7020', 'no answers yet'); ?></span>
					<span class="have-answers" ng-show="item.total_number_answers > 0">
						<span class="count">{{item.total_number_answers}}</span>
						<span class="suffix"><?php echo Yii::t('app7020', 'answers'); ?></span>
					</span>
				</p>
			</div>
			<div ng-if="questionAllow.footer.indexOf('likes') > -1" class="like-dislike-wrapper" like-dislike likes="{{item.answer.likes}}" dislikes="{{item.answer.dislikes}}" data-id="{{item.answer.answer_id}}"></div>
		</div>
	</div>
	<assign-directive ng-if="questionAllow.footer.indexOf('assign') > -1">
		<div class="default-state clearfix" ng-show="!item.tooked && item.assign_state == 0">
			<i class="fa fa-bullhorn"></i>
			<p class="text"><?php echo Yii::t('app7020', 'There is an <strong>asset creation request</strong> to support this question'); ?></p>
			<button ng-click="item.assign_state = 1"><?php echo Yii::t('app7020', 'Assign to me'); ?></button>
		</div>
		<div class="before-state" ng-show="!item.tooked && item.assign_state == 1">
			<div ng-bind-html="item.avatar" class="avatar-wrapper"></div>
			<div class="content-wrapper clearfix">
				<div class="table">
					<div class="table-cell">
						<h3 class="title">
							<span class="text" build-tinymce="{{item.question_id}}" ng-bind-html="to_trusted(item.question) "></span>
						</h3>
						<p class="description"><strong>{{item.owner}}</strong> <?php echo Yii::t('app7020', 'requested an asset to support the above question. Other experts will be notified that '
							. '<strong>you are going to create and upload the asset.</strong>'); ?>
						</p>
						<label>
							<input type="checkbox" name="agreeAssign" value="lock" ng-model="agreeAssign" ng-init="agreeAssign = true"/>
							<span class="text"><?php echo Yii::t('app7020', 'I understand that <strong>I cannot dismiss a request I\'ve assigned to me.</strong>'); ?></span>
						</label>
					</div>
					<div class="table-cell">
						<div class="buttons-wrapper">
							<button class="assign" ng-class="{'disabled': !agreeAssign}"><?php echo Yii::t('app7020', 'Assign to me'); ?></button>
							<button ng-click="item.assign_state = 0"><?php echo Yii::t('app7020', 'Cancel'); ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="after-state" ng-show="!item.tooked && item.assign_state == 2">
			<h3 class="title"><?php echo Yii::t('app7020', 'Question and request have been moved to'); ?> <i ng-click="redirect([':filter?', 'inCharge'], $event, true)"><?php echo Yii::t('app7020', 'In charge'); ?></i></h3>
			<p class="description"><?php echo Yii::t('app7020', 'Once you\'re ready, you can upload the requested asset'); ?></p>
		</div>
		<div class="default-state clearfix" ng-show="item.tooked && item.assign_state == 0">
			<div ng-bind-html="item.assigner_avatar" class="avatar-wrapper"></div>
			<p class="text"><?php echo Yii::t('app7020', '<strong>You</strong> took in charge this request'); ?> <strong>{{item.tooked_time}}</strong></p>
			 
			<a ng-if="item.can_upload && item.conversion_status < 3 " href="<?php echo Docebo::createApp7020Url('assets/index'); ?>&idQuestion={{item.question_id}}">
				<?php echo Yii::t('app7020', 'Upload asset'); ?>
			</a>
			<a ng-if="item.can_upload && item.conversion_status < 10 &&  item.conversion_status >= 3  " href="<?php echo Docebo::createApp7020Url('assets/index'); ?>&idQuestion={{item.question_id}}">
				<?php echo Yii::t('app7020', 'Waiting for publish'); ?>
			</a>
<!--			<div ng-if="!item.can_upload" class="pull-right"><?php echo Yii::t('app7020', 'Waiting for publish'); ?></div>-->
		</div>
	</assign-directive>
	<div class="clearfix"></div>	
</div>