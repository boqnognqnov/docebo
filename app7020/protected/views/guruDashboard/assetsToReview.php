<div class="main-wrapper" ng-init="init(false)" ng-show="loaded">

	<div class="heading" ng-show="filter == 'inReview' && data.assets.inreview.items.length">
		<h1><?php echo Yii::t('app7020', 'In Review'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the asset that you and the other experts are reviewing/approving'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'newOnly' && data.assets.new.items.length">
		<h1><?php echo Yii::t('app7020', 'New Only'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the new assets to review and approve'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'silenced' && data.assets.ignored.items.length">
		<h1><?php echo Yii::t('app7020', 'Silenced'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are your silenced assets. You won\'t be notified for any change that happens. You can re-enable notifications or, if no action is taken, <strong>they will be automatically removed from here as soon as a best answer is choosen. Alternatively, you can '); ?></p>
	</div>

	<div class="info-wrapper clearfix" ng-show="
		filter == 'inReview' && data.assets.inreview.items.length ||
		filter == 'newOnly' && data.assets.new.items.length ||
		filter == 'silenced' && data.assets.ignored.items.length">
		<div class="counter pull-left">
			<strong ng-if="filter == 'inReview'">{{data.assets.inreview.asset_count}}</strong>
			<strong ng-if="filter == 'newOnly'">{{data.assets.new.asset_count}}</strong>
			<strong ng-if="filter == 'silenced'">{{data.assets.ignored.asset_count}}</strong>
		<?php echo Yii::t('app7020', 'items'); ?></div>
		<div class="clear pull-right" ng-show="filter == 'silenced'" ng-click="clearSilenced('asset');"><?php echo Yii::t('app7020', 'Clean silenced list'); ?></div>
	</div>

	<div class="blank-state" ng-show="
		filter == 'inReview' && !data.assets.inreview.asset_count ||
		filter == 'newOnly' && !data.assets.new.asset_count ||
		filter == 'silenced' && !data.assets.ignored.asset_count">
		<i class="fa fa-pencil"></i>
		<h2><?php echo Yii::t('app7020', 'Hey, you don\'t have any asset to review'); ?></h2>
		<h3><?php echo Yii::t('app7020', 'Engage your people and make them create and upload their knowledge assets!'); ?></h3>
	</div>

	<div class="items-wrapper" assets-items items="data.assets.inreview.items" ng-if="filter == 'inReview'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
	<div class="items-wrapper" assets-items items="data.assets.new.items" ng-if="filter == 'newOnly'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
	<div class="items-wrapper" assets-items items="data.assets.ignored.items" ng-if="filter == 'silenced'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
</div>