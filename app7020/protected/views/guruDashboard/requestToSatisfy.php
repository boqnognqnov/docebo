<div class="main-wrapper" ng-init="init(false)" ng-show="loaded">

	<div class="heading" ng-show="filter == 'inCharge' && data.requests.incharge.items.length">
		<h1><?php echo Yii::t('app7020', 'In charge'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the requests you took in charge. Don\'t forget to upload the requested asset and spread the knowledge!'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'newRequests' && data.requests.new.items.length">
		<h1><?php echo Yii::t('app7020', 'New requests'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are the new requests that you or other experts can take in charge.'); ?></p>
	</div>
	<div class="heading" ng-show="filter == 'silenced' && data.requests.ignored.items.length">
		<h1><?php echo Yii::t('app7020', 'Silenced'); ?></h1>
		<p><?php echo Yii::t('app7020', 'These are your silenced requests. You won\'t be notified for any change that happens. You can re-enable the notifications or, if no action is taken, <b>they will be automaticcaly removed from here as soon as a best answer is choosen. Alternatively you can '); ?></p>
	</div>

	<div class="info-wrapper clearfix" ng-show="
		filter == 'inCharge' && data.requests.incharge.questions_count ||
		filter == 'newRequests' && data.requests.new.questions_count ||
		filter == 'silenced' && data.requests.ignored.questions_count">
		<div class="counter pull-left">
			<strong ng-if="filter == 'inCharge'">{{data.requests.incharge.questions_count}}</strong>
			<strong ng-if="filter == 'newRequests'">{{data.requests.new.questions_count}}</strong>
			<strong ng-if="filter == 'silenced'">{{data.requests.ignored.questions_count}}</strong>
		<?php echo Yii::t('app7020', 'items'); ?></div>
		<div class="clear pull-right" ng-show="filter == 'silenced'" ng-click="clearSilenced('request');">
			<?php echo Yii::t('app7020', 'Clean silenced list'); ?>
		</div>
	</div>

	<div class="blank-state" ng-show="
		filter == 'inCharge' && !data.requests.incharge.questions_count ||
		filter == 'newRequests' && !data.requests.new.questions_count ||
		filter == 'silenced' && !data.requests.ignored.questions_count">
		<i class="fa fa-bullhorn"></i>
		<h2><?php echo Yii::t('app7020', 'You don\'t have any requests to satisfy'); ?></h2>
	</div>

	<div class="items-wrapper" questions-items items="data.requests.incharge.items" ng-if="filter == 'inCharge'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
	<div class="items-wrapper" questions-items items="data.requests.new.items" ng-if="filter == 'newRequests'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
	<div class="items-wrapper" questions-items items="data.requests.ignored.items" ng-if="filter == 'silenced'" infinite-scroll="runInfinite()" infinite-scroll-disabled="busyInfinite"></div>
</div>