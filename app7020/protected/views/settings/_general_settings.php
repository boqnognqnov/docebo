<section id="app7020-general-settings-form-container">
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'app7020-general-settings-form',
		'enableAjaxValidation' => false,
		'htmlOptions' => array(
			'class' => "ajax form-horizontal",
		),
	));
	?>


    <div class="row-fluid  app7020-container grey">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Video Contributions'); ?></h5>
            <div class="details">
				<?php echo Yii::t("app7020", "Define lenght limits for the videos that contributors want to upload") ?>
            </div>
        </div>

        <div class="span8 logical-group">
			<?php
			echo $form->radioButtonList($sendDataToView['model'], 'video_contribute', array(
				'allow_any_length' => '<span></span>' . Yii::t('app7020', 'Allow upload of videos of any length'),
				'video_duration' => '<span></span>' . Yii::t('app7020', 'Set Duration limits (not shorten/longer than)')
					), $sendDataToView['radio_list_args']);
			?>

            <span class="additional_container <?= $sendDataToView['showAdditionalContainer']['video_contribute'] ?>">
                <div class="control-group">
                    <div class="controls related-items">
						<?php
						echo $form->checkBox($sendDataToView['model'], 'video_not_shorter', array('data-act' => 'related', 'uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
						echo $form->labelEx($sendDataToView['model'], 'video_not_shorter', array('class' => 'control-label'));
						echo $form->textField($sendDataToView['model'], 'video_not_shorter_time', array('placeholder' => '00:00:00', 'id' => 'video_not_shorter_time'));
						echo Yii::t('app7020', 'hh:mm:ss');
						?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls related-items">
						<?php
						echo $form->checkBox($sendDataToView['model'], 'video_not_longer', array('data-act' => 'related', 'uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
						echo $form->labelEx($sendDataToView['model'], 'video_not_longer', array('class' => 'control-label'));
						echo $form->textField($sendDataToView['model'], 'video_not_longer_time', array('placeholder' => '00:00:00', 'id' => 'video_not_longer_time'));
						echo Yii::t('app7020', 'hh:mm:ss');
						?>
                    </div>
                </div>
            </span>       
        </div>

    </div>


    <div class="row-fluid ">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Other Contributions'); ?></h5>
            <div class="details">
				<?php echo Yii::t("app7020", "Define formats of knowledge assets that can be uploaded") ?>
            </div>
        </div>

        <div class="span8 logical-group">
            <div class="control-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'documents_contribution', array('uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'documents_contribution', array('class' => 'control-label'));
					?> 
                </div>
            </div>
            <div class="details padd"><?php echo Yii::t('app7020', 'doc,xls,pdf,ppt,odt,ods,odp,docx,xlsx,pptx') ?></div>
            <div class="control-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'other_contribution', array('data-act' => 'additional', 'uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'other_contribution', array('class' => 'control-label'));
					?> 
                </div>
            </div>
            <span class="additional_container <?= $sendDataToView['showAdditionalContainer']['other_contribution'] ?>">
                <div class="control-group">
                    <div class="controls">
						<?php
						echo $form->textField($sendDataToView['model'], 'file_extentions', array('id' => 'file_extentions'));
						?> 
                    </div>
                </div>
                <div class="details padd"><?php echo Yii::t('app7020', 'Please specify files extensions comma separated (e.g. jpg, txt, rtf)') ?></div>
            </span>

        </div>

    </div>

    <div class="row-fluid app7020-container grey">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Sharing'); ?></h5>
        </div>

        <div class="span8">

            <div class="control-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'invite_watch', array('uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'invite_watch', array('class' => 'control-label'));
					?> 
                </div>
            </div>
            <div class="details padd"><?php echo Yii::t('app7020', 'Allow user to invite other people to watch an asset') ?></div>
<?php
/*
  Commented because not yet implemented
            <div class="control-group">

                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'social_sharing', array('uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'social_sharing', array('class' => 'control-label'));
					?> 
                </div>
            </div>

            <div class="details padd"><?php echo Yii::t('app7020', 'Allow user to share an asset on social networks') ?></div>
 */
?>
        </div>


    </div>
<?php

/*
    Commented because not yet implemented
    <div class="row-fluid">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Comments'); ?></h5>
        </div>

        <div class="span8">

            <div class="control-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'allow_comments', array('uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'allow_comments', array('class' => 'control-label'));
					?> 
                </div>
            </div>



        </div>
    </div>
 */
?>
    <div class="row-fluid">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Ask the Expert'); ?></h5>
        </div>

        <div class="span8">
            <div class="control-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'enable_answer_questions', array('uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' => App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'enable_answer_questions', array('class' => 'control-label'));
					?> 
                </div>
            </div>
            <div class="details padd"><?php echo Yii::t("app7020", "Allow contributor to participate by answering to learners' question along with the assigned Expert") ?></div>

            <div class="control-group logical-group">
                <div class="controls">
					<?php
					echo $form->checkBox($sendDataToView['model'], 'limit_questions', array('data-act' => 'additional', 'uncheckValue' => App7020SettingsGeneralForm::OFF, 'value' =>App7020SettingsGeneralForm::ON));
					echo $form->labelEx($sendDataToView['model'], 'limit_questions', array('class' => 'control-label'));
					?> 
                </div> 
                <span class="additional_container <?= $sendDataToView['showAdditionalContainer']['limit_characters'] ?>"> 
					<?php
					echo $form->textField($sendDataToView['model'], 'limit_characters', array('id' => 'limit_characters'));
					echo Yii::t('app7020', 'characters');
					?>
                </span>
            </div>


        </div>



    </div>

    <div class="row-fluid">
        <div class="span4">

        </div>
        <div class="span8">
            <div class="control-group">
                <div class="controls">
					<?php
					$htmlOptions = array('class' => 'btn save app7020-button');
					$ajaxOptions = array('dataType' => 'json',
						'type' => 'post',
						'success' => 'function(data) {
                            app7020.generalSettings.ajax(data);   
                    }',
						'beforeSend' => 'function(){                        
                           app7020.generalSettings.addLoaderIcon();
                      }'
					);
					echo CHtml::ajaxSubmitButton(Yii::t("app7020", "Save"), Docebo::createApp7020Url('settings/processForm'), $ajaxOptions, $htmlOptions);
					?>
                </div>
            </div>
        </div>
    </div>

	<?php $this->endWidget(); ?>
</section>
