#!/bin/sh

# Helper shell script to 
# 1. Update code from git (pull) repo, completely reseting local changes
# 2. Run Yii DB migration for the specified domain
# 3. Cache clean up for the specified domain and release wide
#
# Usage:
#   Make this file executable or just use it like this:
#   sh update.sh <mylms.domain.com> 


usage="\nUSAGE: sh update.sh my.domain.com username password\n"
if [ "x$1" = "x" -o  "x$2" = "x" -o  "x$3" = "x" ]
then
   echo "$usage"
   exit
fi

# git update first
echo "\n\n==> Updating code from git..."
git reset --hard && git pull

# run Yii DB migrations
echo "\n==> Executing pending Yii DB Migrations..."
cd lms/protected
sh yiic-domain-db-migration.sh $1
cd ../..


# Do come cache cleanup
# Yii caches
echo "\n==> Clean various caches..."
rm -Rf lms/assets/*
rm -Rf themes/spt/assets/*

# Some more Yii temp files and junk files created by some libraries
rm -R files/tmp/*.bin
rm -rf lms/protected/runtime/HTML
rm -rf lms/protected/runtime/URI

# This one will prevent "I don't see the new translation" in JS
rm -Rf common/extensions/JsTrans/assets/dictionary-*

# Clean domain specific garbage/cache
domain_code=`echo $1 | sed -e 's/\W/_/g'`
rm -f files/${domain_code}/tmp/*.bin
rm -f files/${domain_code}/tmp/htmlpurifier/*.bin
rm -f files/${domain_code}/tmp/orgchart/*.bin


# Setting permissions of various folders
# Set permissions
chmod -R 777 admin/assets 
chown -R www-data.www-data admin/assets
chmod -R 777 admin/protected/runtime 
chown -R www-data.www-data admin/protected/runtime
chmod -R 777 lms/assets 
chown -R www-data.www-data lms/assets
chmod -R 777 lms/protected/runtime 
chown -R www-data.www-data lms/protected/runtime
chmod -R 777 common/extensions/JsTrans/assets 
chown -R www-data.www-data common/extensions/JsTrans/assets
chmod -R 777 authoring/assets 
chown -R www-data.www-data authoring/assets
chmod -R 777 authoring/protected/runtime 
chown -R www-data.www-data authoring/protected/runtime
chmod -R 777 media 
chown -R www-data.www-data media
chmod -R 777 themes/spt/assets
chown -R www-data.www-data themes/spt/assets
