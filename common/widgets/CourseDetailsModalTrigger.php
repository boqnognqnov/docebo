<?php

/**
 * If the current url contains a 'course_code' parameter,
 * we'll open the course details modal for that course
 */
class CourseDetailsModalTrigger extends CWidget {

	public function run() {
		if (null!==($_courseCode=Yii::app()->request->getParam('course_code'))) {
			$courseModel = LearningCourse::model()->findByAttributes(array('code'=>$_courseCode));
			if (!$courseModel) return;

			// if the user is subscribed to the course, redirect to the player
			if ( ! Yii::app()->user->getIsGuest()) {
				$isUserSubscribed = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => Yii::app()->user->id,
					'idCourse' => $courseModel->idCourse
				));
				if ($isUserSubscribed) {
					Yii::app()->controller->redirect(Docebo::createAppUrl('lms:player', array('course_id'=>$courseModel->idCourse)));

					Yii::app()->end();
				}
			}

			$url = Docebo::createAppUrl('lms:course/axDetails', array('id'=>$courseModel->idCourse));
			$script = CHtml::script("
				$('<div/>').dialog2({
					id: 'dialog-course-detail-$courseModel->idCourse',
					closeOnEscape: false,
					modalClass: 'course-details-modal',
					content: '$url'
				});
				$(document).delegate('.course-details-modal', 'dialog2.content-update', function() {
            		var e = $(this);
            		var modal = e.find('.modal-body');

            		var autoclose = e.find('a.auto-close');

            		if (autoclose.length > 0) {
                		e.find('.modal-body').dialog2('close');

						var href = autoclose.attr('href');
						if (href) {
							window.location.href = href;
						}
		            }
				});
			");
			echo $script;
		}
	}

} 