<?php

/**
 * Description of TabsForm - this widget is to create funcioanlity like bootstrap tabs -
 *  http://getbootstrap.com/2.3.2/components.html#navs (see Tabs on the left)
 * It is used for example - http://some_loms/app7020/index.php?r=settings/index
 *
 * @author Velislav
 */
class TabsForm extends CWidget {

    public $arrayData = array(
    );
    public $menuHeading = '';
	public $activeTab = '';
	public $menu = array(
            // Example data
            //        array(
            //            'link-id' => 'general-settings',
            //            'name' => 'General Settings',
            //            'icon-id' => 'fa-cog',
            //            ),
            //        array('link-id' => 'general-settings2',
            //            'name' => 'General Settings2',
            //            'icon-id' => 'fa-cog'),
    );
    public $content = array(
            // Example data
//        'menuHeading' => Yii::t('app7020', 'Share'), Over the menu heading
//        'activeTab' => 'general-settings', Active tab - it shoul be =  'link-id' that we wamt to be active. If is not set, first tab will be "active"
//        array(
//            'contentHeading' => 'General Settings',
//            'viewName' => '_general_settings', //It is renderPartial name
//            'link-id' => 'general-settings',
//            'sendDataToView' => array(),
//        ),
//        array(
//            'contentHeading' => 'General Settings2',
//            'viewName' => '_general_settings2', //It is renderPartial name
//            'link-id' => 'general-settings2',
//            'sendDataToView' => array(),
//        )
    );
    public $containerClass = 'customTabsForm';

    public function run() {
		


		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
	
        $this->render('common.widgets.views.tabsForm.index');
    }

    
    // It is needed because you can't call renderPartial using $this in widget - stupped but works now :)
    public function renderPartial($view, $data = null, $return = false, $processOutput = false) {
        $this->controller->renderPartial($view, $data, $return, $processOutput);
    }

}
