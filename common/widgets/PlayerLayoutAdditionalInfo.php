<?php
class PlayerLayoutAdditionalInfo extends CWidget {

	public $loModel;
	public $idCourse;
	public $refreshUrl = false;
	public $deleteUrl = false;
	public $getThumbnailsUrl = false;

	public function init(){
		parent::init();

		$course_id = Yii::app()->request->getParam('course_id', false);

		if($course_id){
			$url = 'player/training/getThumbnails';
		}else{
			$url = 'centralrepo/centralLo/getThumbnails';
		}

		$this->getThumbnailsUrl = Docebo::createLmsUrl($url);

		if(!$this->loModel)
			$this->loModel = new LearningOrganization();
	}

	public function run(){
		$this->render('playerLayoutAdditionalInfo/_additional_information', array(
			'loModel'=> $this->loModel,
			'courseModel' => LearningCourse::model()->findByPk($this->idCourse),
		));
	}
} 