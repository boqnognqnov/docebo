<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class SiteImageArea extends CWidget {

	private $homeImageFileName = '';
	private $homeImageUrl= '';
	private $homeImageBaseUrl = '';

	public function init() {
		parent::init();
		$homeLoginImg = trim(Settings::get('home_login_img'));
		$asset = CoreAsset::model()->findByPk($homeLoginImg);

		if ($asset) {
			/*
			$homeLoginImgPosition = Settings::get('home_login_img_position', 'fill');
			switch ($homeLoginImgPosition) {
				case 'fill':
					$variant = (Settings::get('catalog_external', 'off') == 'on'
						? CoreAsset::VARIANT_STRETCHED_LARGE
						: CoreAsset::VARIANT_STRETCHED);
					break;
				case 'tile':
					$variant = CoreAsset::VARIANT_ORIGINAL;
					break;
				default:
					$variant = CoreAsset::VARIANT_ORIGINAL;
					break;
			}
			*/
			$variant = CoreAsset::VARIANT_ORIGINAL; // NOTE: the background size in resolved via CSS
			$this->homeImageUrl = $asset->getUrl($variant);
		}
		else {
			// Maybe this field consists FILE NAME, instead of asset ID? Get the Home Login Image file name
			$this->homeImageFileName = $homeLoginImg;

			// Check if file exists in known folders and use it
			$filePathInTheme = Yii::app()->theme->basePath . "/images/login/" . $this->homeImageFileName;
			$filePathInFiles = Docebo::filesPathAbs() . "/" . $this->homeImageFileName;

			// Try to find the image file in  1) /files/<domain>  2) /themes/spt/images/login
			// If not found, use the LMS default from Yii config
			if (is_file($filePathInFiles)) {
				$this->homeImageBaseUrl 	= Docebo::filesBaseUrl();
			}
			else if (is_file($filePathInTheme)) {
				$this->homeImageBaseUrl = Yii::app()->theme->baseUrl . '/images/login';
			} else {
				$this->homeImageBaseUrl 	= Yii::app()->theme->baseUrl . '/images/login';
				$this->homeImageFileName 	= Yii::app()->params['defaultHomeImageFileName'];
			}
			
			$this->homeImageUrl = $this->homeImageBaseUrl . "/" . $this->homeImageFileName; 
			
		}
	}

	public function run()
	{

		$homeImage = array(
			'src' => $this->homeImageUrl,
			'aspect' => Settings::get('home_login_img_position', 'fill')
		);

		// Raise event to let plugins override the default login image
		Yii::app()->event->raise('GetLoginImage', new DEvent($this, array(
			'login_image' => &$homeImage
		)));

		$this->render('site_image_area', array(
			'homeImageUrl' => $homeImage['src'],
			'homeImageAspect' => $homeImage['aspect']
		));
	}

}
