<?php
/**
 * Widget to load and display the apps grid.
 *
 * Renders a view where an AJAX call is made repeatedly (optionally) to load apps
 *
 * @author Andrei Amariutei, Dzhuneyt Ahmed
 *
 */
class AppsGrid extends CWidget {

    /* Options */
	public $rowSize = 5;
    public $apps = array();
	public $featuredApps = array();
	public $coverTile;
	public $backCoverTile;
	public $isECSInstallation;

	public $items;
	public $action; // myapps|category

    public function run()
	{
		if(count($this->featuredApps) > 3){ // Only display 3 featured apps for now @TODO check this
			$this->featuredApps = array_slice($this->featuredApps, 0, 3);
		}

        $this->render('apps_grid_loader', array(
			'isECSInstallation' => $this->isECSInstallation
		));
    }
}