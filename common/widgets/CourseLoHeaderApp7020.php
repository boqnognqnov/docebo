<?php

/**
 * Description of CourseLoHeaderApp7020
 *
 * @author Kristian
 */
class CourseLoHeaderApp7020 extends CWidget {

	public $arrayData = array();
	public $containerClass = '';
	public $loID;
	private $_isEnabled;
	
	public function init(){
		parent::init();
		$this->_isEnabled = Settings::get('app7020_ask_button_lo', CoachApp7020Settings::OFF);
	}
	
	public function run() {
		if(!is_null($this->loID) && $this->_isEnabled == CoachApp7020Settings::ON){
			if(LearningOrganization::isAskGuruEnabled($this->loID)){
				$this->render('common.widgets.views.courseLoHeaderApp7020.index');
			}
		}
	}

}
