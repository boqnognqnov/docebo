<?php

/**
 * Description of app7020QuestionListView
 *
 * @author Kristian
 */
class app7020QuestionListView extends CWidget {
	
	public $listParams = array();
	public $gridColumns = 1;
	public $noItemViewContainer = true;
	public $disableMassSelection = true;
	public $disableMassActions = true;
	public $disableFiltersWrapper = true;
	public $disableCheckBox = true;
	
	public $blankTemplates = array();
	protected $_blankTemplates = array(
		'page' => 'common.widgets.views.app7020QuestionListView.defaultPageBlank',
		'search' => 'common.widgets.views.app7020QuestionListView.defaultSearchBlank'
	);
	
	public $extensions = '';
	protected $_extensions = array(
		'available' => 'ago,views,takeInCharge,tookInCharge,gotIt,ignore,unIgnore,takeInChargeDisabled,relatedAsset,lastAnswer,chat,newAnswers,openClosedSwitch',
		'default' => 'ago,views'
	);
	/* Predefined callbacks for this widget */
	protected $_beforeUpdate = "app7020.app7020QuestionListView.listViewBeforeUpdate(this, id, options);";
	protected $_afterUpdate = "app7020.app7020QuestionListView.listViewAfterUpdate(this, id, data);";
	
	public function run() {
		$this->render('common.widgets.views.app7020QuestionListView.index');
	}
}
