<?php

/**
 * Description of GuruCurclesList
 *
 * @author Kristian
 */
class GuruCirclesList extends CWidget {

	public $arrayData = array();
	public $maxVisibleItems = 5;
	public $containerClass = 'customDropdown';
	public $tooltipPlacement = 'top';

	public function run() {
		$this->render('common.widgets.views.guruCirclesList.index');
	}

}
