<?php

Yii::import('zii.widgets.CListView');

class Forum extends CListView
{
    public $postView;
    public $pagerCssClass = 'doceboPager';
    /**
     * Renders the data item list.
     */
    public function renderItems()
    {
        echo CHtml::openTag($this->itemsTagName,array('class'=>$this->itemsCssClass))."\n";
        $data=$this->dataProvider->getData();
        if(($n=count($data))>0)
        {
            $owner=$this->getOwner();
            $viewFile=$owner->getViewFile($this->itemView);
            $j=0;
            foreach($data as $i=>$item)
            {
                $data=$this->viewData;
                $data['index']=$i;
                $data['data']=$item;
                $data['widget']=$this;
                $owner->renderFile($viewFile,$data);
                if($j++ < $n-1)
                    echo $this->separator;
            }
        }
        else
            $this->renderEmptyText();
        echo CHtml::closeTag($this->itemsTagName);
    }

    public function renderPager()
    {
        if(!$this->enablePagination)
            return;

        $pager=array();
        $class='DoceboCLinkPager';
        if(is_string($this->pager))
            $class=$this->pager;
//		else if(is_array($this->pager))
//		{
//			$pager=$this->pager;
//			if(isset($pager['class']))
//			{
//				$class=$pager['class'];
//				unset($pager['class']);
//			}
//		}
        $pager['pages']=$this->dataProvider->getPagination();

        //if($pager['pages']->getPageCount()>1)
        //{
        echo '<div class="'.$this->pagerCssClass.'">';
        $this->widget($class,$pager);
        echo '</div>';
        //}
        //else
        //	$this->widget($class,$pager);
    }
}