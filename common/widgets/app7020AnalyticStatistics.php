<?php

/**
 * Description of app7020AnalyticStatistics
 *
 * @author Kristian
 */
class app7020AnalyticStatistics extends CWidget {

	public $params = array();
	protected $_params = array(
		'mode' => 'manageInvitations'
	);
	public $htmlOptions = array();

	public function init() {
		$cs = Yii::app()->getClientScript();
		$themeUrl = App7020Helpers::getAssetsUrl();
		$cs->registerScriptFile($themeUrl . '/js/app7020AnalyticStatistics/app7020AnalyticStatisticsPlugin.js');
	}

	public function run() {
		$this->render('common.widgets.views.app7020AnalyticStatistics.index');
	}

}
