<?php
/**
 * Description of app7020RadioSwitches
 *
 * @author Kristian
 */
class app7020RadioSwitches extends CWidget {
	public $switches = array();
	/**
	 * Use value of radio button who need select
	 * By for default selected is first
	 * @var text
	 */
	public $select = null;
	
	/**
	 * Select which Combo List Views to update
	 * @var str (comma separated without spaces) example "myInvitationsListId,myContributionListId"
	 */
	public $comboListViewId = '';


	public function run() {
		$this->render('common.widgets.views.app7020RadioSwitches.index');
	}
	
}
