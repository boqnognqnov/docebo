<?php

/**
 * Class DoceboHelpful
 *
 * Renders a like/dislike html control
 */
class DoceboHelpful extends CWidget {

    /**
     * @var string
     */
    public $id;
    /**
     * @var int
     */
    public $likes;
    /**
     * @var int
     */
    public $dislikes;
    /**
     * like|dislike
     * @var string
     */
    public $vote;
    /**
     * @var string
     */
    public $ratingUrl;
    /**
     * @var array
     */
    public $htmlOptions;


    public function __construct() {
        parent::__construct();

        $cssFile = Yii::app()->theme->baseUrl . '/css/docebo-helpful.css';
        $this->registerCssFile($cssFile);
    }

    /**
     * Executes the widget.
     * This method registers all needed client scripts and renders
     * the text field.
     */
    public function run()
    {
        if(isset($this->htmlOptions['id']))
            $id=$this->htmlOptions['id'];
        else
            $id = $this->id;

        $this->registerClientScript($id);
        $this->renderHtml($id);
    }

    /**
     * Renders the element.
     * @param string $id the ID of the container
     */
    protected function renderHtml($id)
    {
        $isReadOnly = in_array($this->vote, array('readonly')); // array('like','dislike','readonly')
        $readOnly = $isReadOnly ? ' readonly' : '';

        $html = CHtml::label(Yii::t('myblog', 'Helpful?'),'');

        $likeUrl = $isReadOnly ? '#' : $this->ratingUrl . '&value=1';
        $likeIcon = CHtml::tag('span', array('class'=>'icon'), '');
        $myVote = ($this->vote==='like') ? ' my-vote' : '';
        $countLikes = CHtml::tag('span', array('class'=>'count'), $this->likes);
        $html .= CHtml::tag('a', array('class'=>'thumbs-up'.$myVote, 'href'=>$likeUrl), $countLikes . $likeIcon);

        $dislikeUrl = $isReadOnly ? '#' : $this->ratingUrl . '&value=-1';
        $dislikeIcon = CHtml::tag('span', array('class'=>'icon'), '');
        $myVote = ($this->vote==='dislike') ? ' my-vote' : '';
        $countDislikes = CHtml::tag('span', array('class'=>'count'), $this->dislikes);
        $html .= CHtml::tag('a', array('class'=>'thumbs-down'.$myVote, 'href'=>$dislikeUrl), $countDislikes . $dislikeIcon);

        echo CHtml::tag('div', array(
            'id'=>$id,
            'class'=>'docebo-helpful'.$readOnly,
            'data-likes'=>$this->likes,
            'data-dislikes'=>$this->dislikes
        ), $html);
    }

    /**
     * Registers the necessary javascript and css scripts.
     * @param string $id the ID of the container
     */
    public function registerClientScript($id)
    {
        /* @var $cs CClientScript */
        $cs=Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.helpful.js');
    }

    /**
     * Registers the needed CSS file.
     * @param string $url the CSS URL. If null, a default CSS URL will be used.
     */
    public static function registerCssFile($url=null)
    {
        $cs=Yii::app()->getClientScript();
        if($url!==null)
            $cs->registerCssFile($url);
    }
} 