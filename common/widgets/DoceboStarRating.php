<?php

class DoceboStarRating extends CStarRating {

    /**
     * The type of rating (see @types below)
     * @var string
     */
    public $type;

    /**
     * Possible types of rating representation.
     * Each must be styled in the css file
     * @var array
     */
    protected $types = array('smiley', 'star', 'big-star', 'circle', 'heart');

	public $id_course = false;
	public $id_user = false;

    public $minRating = 1;
    public $maxRating = 5;

    /**
     * @var array
     */
    public $urlParams;

    /**
     * @var string
     */
    public $ratingUrl;

    public function __construct() {
        parent::__construct();
        $this->cssFile = Yii::app()->theme->baseUrl . '/css/docebo-star-rating.css';
    }

    public function run()
    {
        /*
         * Add a class name to the rating html element to style it
         * accordingly with it's type
         */
        if (empty($this->type) || !in_array($this->type, $this->types))
            $this->type = 'star';

        $cssClass = $this->type . '-type-rating rating-container';
        $this->htmlOptions['class'] = trim($this->htmlOptions['class'] .' '.$cssClass);
        $this->htmlOptions['data-readonly'] = $this->readOnly;
        $this->htmlOptions['data-value'] = $this->value;
        $this->htmlOptions['data-animate'] = in_array($this->type, array('smiley')) ? 1 : 0;
        if (!empty($this->urlParams)) {
            foreach ($this->urlParams as $k => $v) {
                $this->htmlOptions['data-'.strtolower($k)] = CHtml::encode($v);
            }
        }

        $this->callback = $this->getJsCallback($this->urlParams, $this->id_course);

        parent::run();
    }

    public function getJsCallback($urlParams, $id) {
        $params = (!empty($urlParams)) ? $urlParams : array();
        $dataObj = new stdClass();
        foreach ($params as $k => $v) {
            $dataObj->$k = $v;
        }
        $sParams = CJSON::encode($dataObj);
        $js = "function(value, link) {
            var data = $sParams;
            data.value = value;
            $.post('$this->ratingUrl', data, function(result){
                  var data = JSON.parse(result);
                  $('span[data-idcourse=\"$id\"]').html(data.stars);
                  $('span[data-idcourse=\"$id\"]').closest('div').find('.rating-value').html(data.rating);
            });
        }";
        return $js;
    }

    /**
     * Registers the necessary javascript and css scripts.
     * @param string $id the ID of the container
     */
    public function registerClientScript($id)
    {
        /* @var $cs CClientScript */
        $cs=Yii::app()->getClientScript();
        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.rating.js');

        $jsOptions=$this->getClientOptions();
        $jsOptions=empty($jsOptions) ? '' : CJavaScript::encode($jsOptions);
        $js="jQuery('#{$id} > input').rating({$jsOptions});";
        $cs->registerScript('Yii.CStarRating#'.$id,$js);

        if($this->cssFile!==false)
            self::registerCssFile($this->cssFile);
    }

	/**
	 * Returns the array of JS options for the jquery rating plugin
	 */
	public function getOptions() {
		return $this->getClientOptions(); // this is protected and part of the Yii standard code
	}
}