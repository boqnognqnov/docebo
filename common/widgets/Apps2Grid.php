<?php
/**
 * Widget to load and display the apps grid.
 *
 * Renders a view where an AJAX call is made repeatedly (optionally) to load apps
 *
 * @author Andrei Amariutei, Dzhuneyt Ahmed
 *
 */
class Apps2Grid extends CWidget {

    /* Options */
	public $rowSize = 5;
    public $apps = array();
	public $featuredApps = array();
	public $coverTile;
	public $backCoverTile;
	public $isECSInstallation;

	public $items;
	public $action; // myapps|category

    public function run()
	{
		if(count($this->featuredApps) > 3){ // Only display 3 featured apps for now @TODO check this
			$this->featuredApps = array_slice($this->featuredApps, 0, 3);
		}

        $this->render('apps2_grid_loader', array(
			'isECSInstallation' => $this->isECSInstallation
		));
    }
    
    
    protected function renderFeaturedApp($app) {
        
        // Allowed action is calculated in Apps2Controller as the next possible action user can take regarding the App
        $allowedAction = $app['allowed_action'];
        
        $discoverMore = Yii::t('apps', 'Discover More');
        
        switch ($allowedAction) {
        
            case Apps2Widget::ACTION_DEACTIVATE:
                break;
        
            case Apps2Widget::ACTION_CONTACT_US:
                $this->renderFeaturedTemplate($app, $discoverMore, $allowedAction, Yii::t('standard', 'Contact us'), 'blue');
                break;
        
            case Apps2Widget::ACTION_TRY_FOR_FREE:
                $this->renderFeaturedTemplate($app, $discoverMore, $allowedAction, Yii::t('apps', 'Start free trial'), 'orange');
                break;
        
            case Apps2Widget::ACTION_ACTIVATE:
                $this->renderFeaturedTemplate($app, $discoverMore, $allowedAction, Yii::t('apps', 'Install app'), 'green');
                break;
        
            case Apps2Widget::ACTION_UPGRADE:
                $this->renderFeaturedTemplate($app, $discoverMore, $allowedAction, Yii::t('apps', 'Upgrade now'), 'green');
                break;
        
            case Apps2Widget::ACTION_DISCOVER_MORE:
                $this->renderFeaturedTemplate($app, $discoverMore, $allowedAction, Yii::t('apps', 'Discover More'), 'black');
                break;
        
            default:
                break;
                
        }
        
    }
    
    protected function renderFeaturedTemplate($app, $message, $action, $actionTitle, $btnColor) {
        
        $image = $app['image_url'];
        $imageTag = "";
        if ($image) {
            $imageTag = "<div class=\"premium-img\" style=\"background-image: url('$image');\"></div>";
        }
        
        $url = Yii::app()->createUrl('apps2/axAppDetails', array(
            'code'      => $app['internal_codename'],
            'action'    => $action,
        ));
        
        $html = <<< HTT
            $imageTag
			<h2>$app[title]</h2>
			<p class="descr">
                $app[short_description]		 
		    </p>
            <div class="more">
                <a  href="$url"
                    class="open-dialog btn-buy-app $btnColor"
                    data-dialog-class="metro wide app-details" 
                    rel="dialog-app-detail"
                    title="$actionTitle">$message</a>
            </div>
HTT;
        
        echo $html;
        
    }
    
}