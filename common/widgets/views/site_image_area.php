<div id="lms-home-image" class="<?= $homeImageAspect ?>-image" style="background-image: url('<?= $homeImageUrl ?>');overflow:hidden;position:relative;">

	<!--[if lt IE 9]>
	<img class="ie7bgimage" src="<?= $homeImageUrl ?>" style=""/>
	<![endif]-->
</div>

<script type="text/javascript">	
	$(document).ready(function(){
		$(window).on('resize', function(){
			ie7bgimagefix();
		});
		$(window).trigger('resize');
	});
	
	var ie7bgimagefix = function(){
		// IE7 fix for background image stretch

		var child = $('.ie7bgimage');
		var parent = child.parent();
		
		child.show();

		// Show the overlaying image element, because we can't manipulate
		// images set as element backgrounds. Also, simulate 
		// "background-size:cover" by top and left positioning/margins.
		moveVert = ((parent.height() - child.height()) / 2);
		moveHoriz = ((parent.width()- child.width()) / 2);
		$('.ie7bgimage').css('top', moveVert).css('left', moveHoriz);
	}
</script>