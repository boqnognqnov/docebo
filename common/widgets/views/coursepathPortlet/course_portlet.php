<?php
/* @var $courseName string */
/* @var $this CoursepathPortlet */

$coursePlayerUrl = Yii::app()->controller->createUrl("coursepath/details", array("id_path" => $this->coursepathModel->id_path));

$params = array();
$params['id_path'] = $this->coursepathModel->id_path;
if ($this->resetToken) {
	$params['reset'] = $this->resetToken;
}


?>

<li class="<?= trim($this->class) ?>"  data-details-url="<?= Yii::app()->createUrl('coursepath/axHoverDetails', $params) ?>">

	<div class="tile-corner-image"></div>

	<a href="<?=$this->canPlayCoursepath ? $this->coursePathPlayUrl : $this->coursepathDetailUrl?>" title="<?=$this->coursepathModel->path_name?>">
		<?php echo CHtml::image($this->coursepathModel->getLogo(), $this->coursepathModel->path_name, array("class" => "course-logo")); ?>
	</a>

	<div class="tile-content">
		<h6 class="course-title">
			<a href="<?=$this->canPlayCoursepath ? $this->coursePathPlayUrl : $this->coursepathDetailUrl?>"><?php echo $courseName; ?></a>
		</h6>

		<div class="course-hover-details empty"></div>
	</div>
</li>