<?php
$topicObject = new App7020TopicTree();
$translations = $topicObject->translation(false, $this->arrayData);
if (!empty($this->arrayData)) {
	?>
	<div class="customTopics-widget">
		<ul class="listArray">
			<?php
			$cntr = 0;
			foreach ($translations as $index => $value) {
				if ($cntr < $this->maxVisibleItems) {
					?>
					<li>
					   <?= $value;?>
					</li>
					<?php
					array_shift($translations);
					$cntr++;
				}
			}
			if (!empty($translations)) {
				?>
				<li class="additional">
					<span class="count">+<?= count($translations); ?></span>
					<ul>
						<?php
						foreach ($translations as $index => $value) {
							?>
							<li>
        					   <?= $value;?>
        					</li>
							<?php
						}
						?>
					</ul>
				</li>
				<?php
			}
			?>
		</ul>
	</div>
<?php } ?>