<a class="open-dialog" id="ui-orientation-btn" closeOnEscape="false" data-dialog-class="orientation-popup"
   closeOnOverlayClick="false" showCloseHandle="false" rel="ui-orientation" style="display: none;"></a>
<div id="ui-orientation" style="display: none;">
	<h1>Alert</h1>

	<div class="rotation-preview"></div>
	<div class="rotation-info">
		<?=Yii::t('standard', 'You must <strong class="green">rotate your device</strong> in order to use the <strong>mobile version of docebo</strong>');?>
	</div>
</div>

<? $detect = new Mobile_Detect(); ?>

<script type="text/javascript">
	$(function () {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			window.addEventListener('orientationchange', doOnOrientationChange);
			doOnOrientationChange();
			$("#ui-orientation").dialog2({
				showCloseHandle:     false,
				removeOnClose:       false,
				autoOpen:            false,
				closeOnEscape:       false,
				closeOnOverlayClick: false
			});
			/*$(".open-dialog").click(function (event) {
				event.preventDefault();
				$("#ui-orientation").dialog2("open");
			});*/
		}
	});
	function doOnOrientationChange() {
		if ($("#ui-orientation").hasClass('opened'))
			$("#ui-orientation").dialog2("close");
		setTimeout(function () {
			switch (window.orientation) {
				case -90:
				case 90:
					// landscape
					break;
				case 0:
				case 180:

					// Just in case, do another check since some Android tablets report
					// wrong orientation
					if($(window).height() > $(window).width()){
						// portrait

						// Close all other modals and ask the user to rotate his tablet orientation
						var allOpenModals = $('.modal .modal-body.opened');

						if(allOpenModals){
							allOpenModals.each(function () {
								// Since a $(global-selector).dialog2("close") doesn't
								// work, we apply the close command to each dialog individually
								$(this).dialog2("close");
							});
						}

						<? if($detect->isAndroidOS()): ?>
							<? if($detect->version('Android') && version_compare($detect->version('Android'), '4.3.0', '>')) : ?>
								$("#ui-orientation-btn").trigger("click");
							<? endif; ?>
						<? else: ?>
							$("#ui-orientation-btn").trigger("click");
						<? endif; ?>
					}

					break;
			}
		}, 1500);
	}

	$('#ui-orientation').on('click', function()
		{
			$("#ui-orientation").dialog2("close");
		}
	);
</script>