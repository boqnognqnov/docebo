<?php
/* @var $tooltip App7020Tooltips */
?>
<div id="docebo-flowplayer-tooltip-<?php echo $idHash; ?>" class="docebo-flowplayer-hidden-tooltip app7020tooltip media <?php echo isset($style) ? $style : ''; ?>" >
	<span class="pull-left"   data-custom="circle-avatar">
		<img class="media-object" src="<?= $tooltip->expert->getAvatarImage(true);?>">
	</span>
	<div class="media-body">
		<p>
			<?php echo strip_tags($tooltip->text, "<strong><em><span><br>"); ?>
		</p>
		<div class="clear clearfix"></div>
	</div>
</div>
