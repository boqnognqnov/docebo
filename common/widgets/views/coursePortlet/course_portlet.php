<?php
/* @var $this CoursePortlet */
/* @var $courseName string */

$params = array();
$params['course_id'] = $this->courseModel->idCourse;
if ($this->resetToken) {
	$params['reset'] = $this->resetToken;
}
$coursePlayerUrl = Yii::app()->controller->createUrl("/player", $params);

$currentLanguage = Yii::app()->session['current_lang'];


$imageHtml = CHtml::image($this->courseModel->getCourseLogoUrl(), $this->courseModel->name, array("class" => "course-logo"));


$params = array();
$params['idCourse'] = $this->courseModel->idCourse;
if ($this->resetToken) {
	$params['reset'] = $this->resetToken;
}
?>
<li class="<?= trim($this->class) ?>"  data-details-url="<?= Yii::app()->createUrl('course/axHoverDetails', $params) ?>">

	<div class="tile-corner-image<?php if (!empty($currentLanguage)) { echo ' '.$currentLanguage; } ?>"></div>

	<?php if ($this->canPlayCourse) : ?>
	    <a href="<?= $coursePlayerUrl?>"><?= $imageHtml ?></a>
	<?php elseif ($this->type != "locked") : ?>
		<a href="<?=$this->courseDetailUrl?>" title="<?=$this->courseModel->name?>" closeOnEscape='false' data-dialog-class="course-details-modal" class="<?=($this->showCourseDetailsModal) ? "open-dialog" : ""?>" rel="dialog-course-detail1-<?= $this->courseModel->idCourse ?>">
		<?= $imageHtml ?></a>
    <?php else : ?>
        <a><?= $imageHtml ?></a>
	<?php endif;?>

	<div class="tile-content">
		<h6 class="course-title">
		    <?php if ($this->canPlayCourse) : ?>
			    <!-- <a href="../doceboLms/index.php?modname=course&op=aula&idCourse=<?= $this->courseModel->idCourse ?>"><?php echo $courseName; ?></a> -->
			    <a href="<?= $coursePlayerUrl ?>"><?php echo $courseName; ?></a>
			<?php elseif ($this->type != "locked") : ?>
			    <a href="<?= $this->courseDetailUrl ?>" title="<?=$this->courseModel->name?>" closeOnEscape="false" data-dialog-class="course-details-modal" class="<?= $this->showCourseDetailsModal ? 'open-dialog' : '' ?>" rel="dialog-course-detail2-<?= $this->courseModel->idCourse ?>"><?php echo $courseName; ?></a>
            <?php else  : ?>
                <?php echo $courseName; ?>
			<?php endif;?>
		</h6>

		<div class="course-hover-details empty"></div>
	</div>
</li>