<?php
$dataModel = App7020Assets::model()->findByPk($data['id']);

if (in_array('asterisk', $extensions)) {
	$newQuestions = App7020Question::sqlDataProvider(false, array('contentId' => $data['id'], 'onlyNew' => 1))->totalItemCount;
}
if (in_array('count', $extensions)) {
	$count = count(App7020ContentReview::getPeerReviewesByContent($data['id']));
}
// ****** INVITERS CIRCLES ****** //
$invitersParams = array(
	'arrayData' => $data['inviters'],
	'maxVisibleItems' => 1
);

$contributorNames = $dataModel->contributor->firstname . " " . $dataModel->contributor->lastname;
if (!trim($contributorNames)) {
	$contributorNames = ltrim($dataModel->contributor->userid, "/");
}
?>
<!--GOT IT INFORMATION LABEL-->
<?php if (in_array('gotIt', $extensions) && $index == 0 && !App7020Helpers::actionGetGotItByAttributes(CoreSettingUser::GURUDASHBOARD_ASSETS_GOTIT)): ?>
	<div class="app7020GotItInformation">
		<span>
			<?php
			echo Yii::t('app7020', 'These are your ignored assets. You won\'t be notified for any change that happens.You can re-enable notifications or, if no action is taken,they will be automatically removed from here as soon as <strong>the assets are published.');
			?>
		</span>
		<?php echo CHtml::link(Yii::t('app7020', 'Got it'), 'javascript:;', array('class' => 'gotIt')) ?>
	</div>
<?php endif; ?>
<div class="itemGrid <?php echo $spanSizeType; ?>" data-idObject="<?php echo $data['id'] ?>">

	<?php if (!$titleAsLink): ?>

		<?php
		if ($linkToEditMode && $dataModel->conversion_status < App7020Assets::CONVERSION_STATUS_APPROVED) {
			$link = 'peerReview';
		} else {
			$link = 'view';
		}
		?>
		<a href="<?= Docebo::createApp7020Url("", array("#" => "/assets/$link/".$data['id'])) ?>">
		<?php endif; ?>
		<div class="item clearfix">
			<figure class="thumb">
				<?php App7020Assets::getPublcPreviewThumbnail($data, 'small', true); ?>

				<?php if (!empty($data['duration']) && $data['contentType'] == App7020Assets::CONTENT_TYPE_VIDEO): ?>
					<div class="time"><?php echo sprintf('%2d:%02d:%02d', ($data['duration'] / 3600), ($data['duration'] / 60 % 60), $data['duration'] % 60); ?></div>
				<?php endif; ?>

				<?php if (in_array('count', $extensions) && $count > 0): ?>
					<div class="count"><?php echo $count; ?></div>
				<?php endif; ?>

				<?php if (in_array('asterisk', $extensions) && $newQuestions): ?>
					<div class="asterisk"></div>
				<?php endif; ?>
<!--<div class="unpublished"><?php echo Yii::t('app7020', 'unpublished'); ?></div>
<div class="unpublished-drop"></div>-->

<!--<div class="refused"><?php echo Yii::t('app7020', 'Refused'); ?></div>
	<div class="refused-drop"></div>-->

				<?php if (in_array('new', $extensions) && !App7020ContentHistory::isContentViewedByUser($data['id'])): ?>
					<div class="new"><?php echo Yii::t('app7020', 'New'); ?></div>
				<?php endif; ?>

				<?php if (in_array('watched', $extensions) && App7020ContentHistory::isContentViewedByUser($data['id'])): ?>
					<div class="watched"><?php echo Yii::t('app7020', 'Watched'); ?></div>
					<div class="watched-drop"></div>
				<?php endif; ?>

				<?php if (in_array('inviters', $extensions)): ?>
					<div class="gurus">
						<?php $this->widget('common.widgets.GuruCirclesList', $invitersParams); ?>
					</div>
				<?php endif; ?>
			</figure>

			<section class="content">
				<?php if ($titleAsLink): ?>
					<?php if (!$linkToEditMode) { ?>
						<a href="<?= Docebo::createApp7020AssetsViewUrl($data['id']) ?>">
						<?php } else { ?>
							<a href="<?= Docebo::createApp7020Url("", array("#" => "/assets/peerReview/".$data['id'])) ?>">
						<?php } ?>
						<?php endif; ?>
						<h4><?php echo ($data['title']) ? $data['title'] : 'Empty' ?></h4>
						<?php if ($titleAsLink): ?>
						</a>
					<?php endif; ?>


					<!--RATING-->
					<?php if (in_array('rating', $extensions)): ?>
						<?php $this->widget('common.widgets.ContentRating', array('contentId' => $data['id'], 'readOnly' => true)); ?>
						<span id="rating_score_<?php echo $data['id'] ?>">
							<?php echo App7020ContentRating::calculateContentRating($data['id']) ?>
						</span>
					<?php endif; ?>

					<!--TOP/RIGHT EDIT ICON-->
					<?php if (in_array('edit', $extensions)): ?>
						<i class="fa fa-pencil-square-o"></i>
					<?php endif; ?>

					<!--TOP/RIGHT BELL ICON FOR IGNORE REQUEST-->
					<?php if (in_array('ignore', $extensions)): ?>
						<i class="fa fa-bell-slash-o addToIgnore"></i>
					<?php endif; ?>



					<!--TOP/RIGHT BELL ICON FOR UNIGNORE REQUEST-->
					<?php if (in_array('unIgnore', $extensions)): ?>
						<i class="fa fa-bell-o removeFromIgnore"></i>
					<?php endif; ?>
			</section>

			<footer class="footer">
				<p class="by">
					by <span><?php echo $contributorNames; ?></span>
				</p>
				<p class="info">
					<span class="views"><?php echo App7020ContentHistory::getContentViews($data['id']) ?> <?php echo Yii::t('app7020', 'views') ?></span>
					<span class="ago"><?php echo App7020Helpers::timeAgo($data['created']); ?> <?php echo Yii::t('app7020', 'ago') ?></span>
				</p>

				<?php
				if ($dropDownParams['containerClass'] == 'knowledgeLibraryControls') {
					if (!(App7020Helpers::hasContentInviteWatch($dataModel->peer_review_settings->idContent) && $dataModel->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED)) {
						foreach ($dropDownParams['arrayData'] as $key => $value) {
							if ($value['method'] == 'inviteToWatch') {
								unset($dropDownParams['arrayData'][$key]);
							}
						}
					}
					if (!App7020Assets::canUserDeleteContent($data['id'])) {
						foreach ($dropDownParams['arrayData'] as $key => $value) {
							if ($value['method'] == 'delete') {
								unset($dropDownParams['arrayData'][$key]);
							}
						}
					}
				}
				if (!empty($dropDownParams)) {
					$this->widget('common.widgets.DropdownControls', $dropDownParams);
				}
				?>
			</footer>
		</div>
		<?php if (!$titleAsLink): ?>
		</a>
	<?php endif; ?>

</div>
<div class="clearfix"></div>