<?php
// ****** GURUS CIRCLES ****** //
$listParams = array(
	'arrayData' => array_fill(0, 5, array(
		'name' => 'Admin',
		'linkSource' => 'javascript:;',
		'status' => 'online',
		'imgSource' => 'https://placeholdit.imgix.net/~text?txtsize=14&txt=40%C3%9740&w=40&h=40')
	),
	'maxVisibleItems' => 5,
	'containerClass' => 'topVideosGuruList'
);
?>
<div class="defaultSearchBlankTemplate">
	<div class="app7020-hint app7020-dafault_searchBlank_hint_1">
		<h3><?php echo Yii::t('app7020', '<span>We haven\'t found anything for</span>'); ?></h3>
		<h3><?php echo Yii::t('app7020', '<span>your search criteria</span>'); ?></h3>
	</div>
	<div class="app7020-stack text-right">
		<?php echo CHtml::image(App7020Helpers::getAssetsUrl() . '/images/tumbleweed.png'); ?>
	</div>
	<div class="app7020-hint app7020-dafault_searchBlank_hint_2">
		<h4><?php echo Yii::t('app7020', 'Why don\'t you ask the Expert?'); ?></h4>
		<h3><?php echo Yii::t('app7020', 'Experts can satisfy your request and create Trusted'); ?></h3>
		<h3><?php echo Yii::t('app7020', 'Knowledge for you!'); ?></h3>
	</div>
	<div class="gurusSection text-center">
		<?php $this->widget('common.widgets.GuruCirclesList', $listParams); ?>
	</div>
	<div class="expertLinkSection text-center">
		<?php echo CHtml::link(Yii::t('app7020', 'Ask the expert!'), Docebo::createAppUrl('app7020:askGuru/index'), array('class' => 'askGuruBtn')); ?>
	</div>
</div>