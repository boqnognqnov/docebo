<div class="app7020SingleSearchEngine app7020FilterGlobalClass" data-for="<?php echo $this->comboListViewId; ?>">
	<?php echo CHtml::textField('searchField', '', array('class' => 'app7020SingleSearchEngine-field')); ?>
	<i class="fa fa-times app7020SingleSearchEngine-closeButton"></i>
	<i class="fa fa-search app7020SingleSearchEngine-button"></i>
</div>