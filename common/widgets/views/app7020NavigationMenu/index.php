<?php
if (!empty($this->links) && $this->oldStyle === true):
	?>
	<div id="app7020NavigationMenu">
		<ul class="list">
			<?php
			foreach ($this->links as $link):
				$url = explode(':', $link['href']);
				?>
				<li class="<?php echo (Yii::app()->request->getParam('r', false) == $url[1]) ? 'active' : ''; ?>" data-action="<?php echo $url[1]; ?>">
					<a href="<?php echo (!empty($link['href']) ? Docebo::createAppUrl($link['href']) : 'javascript:;'); ?>">
						<?php echo ($link['title']) ? $link['title'] : Yii::t('app7020', 'No Title'); ?>
						<?php if ($link['counter']): ?>
							<span class="badge"><?php echo $link['counter'] ?></span>
						<?php endif; ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

<?php
if (!empty($this->links) && $this->oldStyle === false):
	foreach ($this->links as $key => $link) {
		$rand[$key] = rand(100000, 999999);
		$autoActive = array();
		if($this->autoActive === true && ($key == 0 && $this->select === false || $this->select !== false && $key + 1 == $this->select)){
			$autoActive = array('class' => 'active');
		}
		$tabTitleAttr[$key] = array();
		foreach(array_merge_recursive($this->_tabTitleAttr, (is_array($link['tabTitleAttr']) ? $link['tabTitleAttr'] : array()), $autoActive) as $k => $v){
			$tabTitleAttr[$key][$k] = (is_array($v)) ? implode(' ', $v) : $v;
		}
		$tabContentAttr[$key] = array();
		foreach(array_merge_recursive($this->_tabContentAttr, (is_array($link['tabContentAttr']) ? $link['tabContentAttr'] : array()), $autoActive) as $k => $v){
			$tabContentAttr[$key][$k] = (is_array($v)) ? implode(' ', $v) : $v;
		}
	}
	?>
	<div id="app7020TabsPlus">
		<ul class="list app7020BootstrapTabsExtended">
			<?php foreach ($this->links as $key => $link): ?>
				<li <?php echo CHtml::renderAttributes($tabTitleAttr[$key]); ?> >
					<a href="#app7020TabsPlus_<?php echo $rand[$key]; ?>" data-toggle="tab">
						<?php echo ($link['tabTitle']) ? $link['tabTitle'] : Yii::t('app7020', 'No Title'); ?>
						<span class="badge <?php echo ($link['tabTitleCounter']) ? 'show' : 'hide' ?>"><?php echo ($link['tabTitleCounter']) ? $link['tabTitleCounter'] : 0 ?></span>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<div class="tab-content">
			<?php foreach ($this->links as $key => $link): ?>
				<div id="app7020TabsPlus_<?php echo $rand[$key]; ?>" <?php echo CHtml::renderAttributes($tabContentAttr[$key]); ?> >
					<?php echo ($link['tabContent']) ? $link['tabContent'] : Yii::t('app7020', 'No Content'); ?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>