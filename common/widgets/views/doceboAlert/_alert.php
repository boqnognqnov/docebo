
<div class="alert docebo-alert <?php echo ($isGotIt) ? ' docebo-got-it-container' : ''; ?> <?php echo $type; ?>" id="<?php echo $id; ?>" data-ajax-url="<?php echo $ajaxURL; ?>">
	<div class="table-row">
		<?php
		if ($type == 'warning') {
			?>
			<div class="table-cell docebo-alert-icon">
				<i class="fa fa-exclamation-circle"></i>
			</div>
			<?php
		}
		?>

		<div class="table-cell">
			<p>
				<?php
				echo $message;
				?>
			</p>
		</div>

		<div class="table-cell docebo-alert-close <?php echo ($isGotIt) ? ' docebo-got-it' : ''; ?>">
			<a   class="close"  >
				<?php if ($isGotIt) : ?>
					GOT IT!
				<?php else :
					?>
					&times;
				<?php
				endif;
				?>
			</a>
		</div>
	</div>
</div>
