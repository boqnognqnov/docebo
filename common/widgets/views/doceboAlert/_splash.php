<div id="app7020-splash-screen" class="splash-wrapper">
	<div class="splash-screen">
		<div class="top text-center">
			<h2><?php echo Yii::t('app7020','Coach & Share'); ?></h2>
			<p><?php echo Yii::t('app7020','Supercharge social and experiential learning in your organization'); ?>.</p>
		</div>
		<div class="notice">
			<div class="icon">
				<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
			</div>
			<div class="text">
				<p><?php echo Yii::t('app7020',"Docebo Coach and Share are available to Docebo Enterprise customers (500+ users). However, you have <strong>free access during your 14-day trial period</strong>."); ?></p>
				<p><?php echo Yii::t('app7020',"Ready to upgrade? We'll be happy to go over your options with you."); ?>
					<?php if(Docebo::isSMB()) {?>
					<span class="contact blue" href=""><?php echo Yii::t('app7020','CONTACT OUR TEAM'); ?></span>
					<?php } else { ?>
					<span class="schedule red" href=""><?php echo Yii::t('app7020','SCHEDULE A DEMO NOW'); ?></span>
					<?php } ?>
				</p>

			</div>
		</div>
		<div class="description">
			<div class="coach text-center">
				<h2><?php echo Yii::t('app7020','Coach'); ?></h2>
				<p><?php echo Yii::t('app7020','Let your knowledge seekers actively and directly engage with the right subject matter experts to ask questions and get answers, right at the point of need. Docebo Coach is a place where learners and experts join forces to create best practices and curate '); ?></p>
			</div>
			<div class="share text-center">
				<h2><?php echo Yii::t('app7020','Share'); ?></h2>
				<p><?php echo Yii::t('app7020','Encourage and empower your employees with user generated content from daily on-the-job activities. With Docebo Share, content is uploaded, then categorized and peer-reviewed, curated and validated, while building a culture that recognizes and rewards top '); ?></p>
			</div>
		</div>
		<div class="button text-center docebo-got-it">
			<span class="gotit green" data-close-modal="true" data-ajax-url="<?php echo $ajaxURL; ?>"><?php echo Yii::t('app7020','Ok,Got it') ?></span>
		</div>
		<img src="<?php echo Yii::app()->theme->baseUrl ?>/images/app7020/splash-bottom.jpg" alt="">
	</div>
</div>