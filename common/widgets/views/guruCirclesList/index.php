<?php
// TODO
if (!empty($this->arrayData)) {
	?>
	<div class="customGuruCircles-widget <?= $this->containerClass ?>">
		<ul class="listArray">
			<?php
			foreach ($this->arrayData as $index => $value) {
				if ($index < $this->maxVisibleItems) {
					?>
					<li>
						<div data-custom="circle-avatar" title="<?= ($value['name']) ? $value['name'] : ''; ?>" data-status="<?= ($value['status']) ? $value['status'] : '' ?>">
							<img src="<?= ($value['imgSource']) ? $value['imgSource'] : ''; ?>">
						</div>
					</li>
					<?php
					array_shift($this->arrayData);
				}
			}
			if (!empty($this->arrayData)) {
				?>
				<li class="additional">
					<div class="count" data-toggle="tooltip">+<?= count($this->arrayData); ?></div>
				</li>
				<?php
			}
			?>
		</ul>
		<div class="tooltipHtml">
			<?php
			foreach ($this->arrayData as $index => $value) {
				?>
				<span class="name"><?= ($value['name']) ? $value['name'] : 'Empty'; ?></span>
				<?php
			}
			?>
		</div>
	</div>
<?php } ?>
<script type="text/javascript">
	$(function(){
		app7020.app7020Tooltips.init({
			placement: '<?= $this->tooltipPlacement ?>'
		});
	});
</script>