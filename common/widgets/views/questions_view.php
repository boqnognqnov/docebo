<div class="row-fluid askGuruWidget" id="askGuru"  class="askGuru" data-title="<?= Yii::t('app7020', 'Ask the Expert!'); ?>">
    <section class="span9 all-quetions mainContentWidget" data-container="root">
        <nav id="filterActions">
        <?php 
        if($viewAskGuruButton)
          echo CHtml::link(Yii::t("app7020", "ASK THE EXPERT"), 'javascript:;', array('class' => 'addNew green widget'));
//        echo CHtml::link(Yii::t('app7020', 'Topics'), 'javascript:;', array('class' => 'topics'));
//        echo CHtml::link(Yii::t('app7020', 'Filters'), 'javascript:;', array('class' => 'filter'));
        if($viewSearchButton){
            ?>
            <div class="searchGroup">
            <?php 
           echo CHtml::link('<i class="fa fa-search fa-lg"></i>', 'javascript:;', array('class' => 'searchBtn'));
           echo CHtml::textfield('search', '', array('class' => 'search', 'placeholder' => 'Search'));
        ?>
           </div>
        <?php 
        }
        ?>
        <!--HIDDEN HTML-->
		<?php //$this->renderPartial('../includes/_nav_filters', array('filtersArray' => $filtersArray)); ?>
        </nav>
    	<div id="appliedFilters">
    		<span class="fControl"><?= Yii::t('app7020', 'Clear filters') ?></span>
    		<div class="fList"></div>
    	</div>
        <?php 
        $this->widget('common.widgets.ComboListView', $listViewParams); 
        ?>
        
    </section>

<?php 

if($viewShowAllLink)
            echo CHtml::link(Yii::t("app7020", "View full Questions & Answers"), Docebo::createApp7020Url('askGuru/index'), array('class'=>'view-all'));
?>
</div>