<?php
	$cbName 	= 'combo-list-item-checkbox-' . $data[$this->keyField];
	$cbValue 	= $data[$this->keyField];
?>

<div class="row-fluid">
	<div class="combo-list-item span12" data-key="<?= $data[$this->keyField] ?>">
		<div class="row-fluid">
			<?php
				$p1 = '3%';
				$p2 = '93%';
				if ($this->disableCheckBox) {
					$p1 = '0%';
					$p2 = '96%';
				} 
			?>
			<div class="span1" style="width: <?= $p1 ?>;">
				<?php
					if (!$this->disableCheckBox) {
						echo Chtml::checkBox($cbName, false, array('value' => $cbValue));
					}
				?>
			</div>
			<div class="span11" style="width: <?= $p2 ?>;">
				<div class="row-fluid">
					<div class="span12">
					<?php
						$params = array_merge(array('index' => $index, 'data' => $data, 'widget' => $widget), $viewData);
						$this->controller->renderPartial($this->listItemView, $params);
					?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
