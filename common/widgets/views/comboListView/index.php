<?php
$cs = Yii::app()->getClientScript();
$themeUrl = Yii::app()->theme->baseUrl;
$cs->registerScriptFile($themeUrl . '/js/combolistview.js');
?>


<div class="combo-list-view-container" id="combo-list-view-container-<?= $this->listId ?>">
	
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => "combo-list-form",
		'htmlOptions' => array(
			'class' => 'form-inline',
		),
	));

// Add any hidden fields requested by widget caller
	foreach ($this->hiddenFields as $name => $value) {
		echo CHtml::hiddenField($name, $value);
	}
	?>
	<?php if (!$this->disableFiltersWrapper) : ?>
		<div class="filters-wrapper">
			<table class="filters table-border">
				<tr>
					<td class="group">
						<table width="100%">
							<tr>
								<td>
									<div class="search-input-wrapper pull-right">
										<?php
										$params = array();
										$params['max_number'] = 10;
										$params['autocomplete'] = 1;
										foreach ($this->hiddenFields as $name => $value) {
											$params[$name] = $value;
										}
										$autoCompleteUrl = $this->controller->createUrl($this->autocompleteRoute, $params);
										$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
											'id' => 'combo-list-search-input',
											'name' => 'search_input',
											'value' => '',
											'options' => array(
												'minLength' => $this->autocompleteRoute ? 1 : 9999, // if no route, effectively never try to autocomplete
											),
											'source' => $autoCompleteUrl,
											'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
										));
										?>
										<button type="button" class="close clear-search">&times;</button>
										<span 	class="perform-search search-icon"></span>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	<?php endif; ?>

	<?php if (!$this->disableMassSelection || !$this->disableMassActions) : ?>
		<div class="selections clearfix">
		<?php endif; ?>


		<?php if (!$this->disableMassSelection) : ?>
			<div class="left-selections clearfix">
				<p><?= Yii::t('standard', 'You have selected') ?> <strong><span id="combo-list-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>.</p>
				<br>
				<a href="javascript:void(0);" class="combo-list-select-all"> <?= Yii::t('standard', '_SELECT_ALL') ?> </a>
				<a href="javascript:void(0);" class="combo-list-deselect-all"> <?= Yii::t('standard', '_UNSELECT_ALL') ?> </a>
				<a href="javascript:void(0);" class="combo-list-select-page-all"> <?= Yii::t('standard', '_SELECT_PAGE') ?> </a>
				<a href="javascript:void(0);" class="combo-list-deselect-page-all"> <?= Yii::t('standard', '_DESELECT_PAGE') ?> </a>
			</div>
		<?php endif; ?>

		<?php if (!$this->disableMassActions) : ?>
			<div class="right-selections clearfix">
				<label class="select" for="<?= $this->listId ?>_massive_action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
				<select name="combo_list_massive_action" id="<?= $this->listId ?>_massive_action" class="combo-list-selection-massive-action">
					<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
					<?php if (is_array($this->massiveActions)) foreach ($this->massiveActions as $value => $label) : ?>
							<option value='<?= $value ?>'><?= $label ?></option>
						<?php endforeach; ?>
				</select>
			</div>
		<?php endif; ?>


		<?php if (!$this->disableMassSelection || !$this->disableMassActions) : ?>
		</div>
	<?php endif; ?>
	
	
		<?php if ($this->title) : ?>
			<div id="comboListViewTitle"><?php echo $this->title ?></div>
		<?php endif; ?>
		
	<?php
	if ($this->noYiiListViewJS) {
		Yii::app()->clientScript->scriptMap['jquery.yiilistview.js'] = false;
		Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
	}

	if (!$this->noItemViewContainer) {
		$itemView = $this->columns == 1 ? 'common.widgets.views.comboListView.itemView1Col' : 'common.widgets.views.comboListView.itemView';
	} else {
		$itemView = 'common.widgets.views.comboListView.noItemViewContainer';
	}

	$afterUpdateFunction = 'function(id, data) {
				$(\'#\'+id).comboListView(\'updateCurrentPageSelection\');';

	if ($this->restyleAfterUpdate) {
		$afterUpdateFunction .= '$(\'#\'+id).find(\':checkbox\').styler({});';
	}

	$afterUpdateFunction .= '$(document).controls();
				$(\'a[rel="tooltip"]\').tooltip();
				' . $this->afterAjaxUpdate . '
			}';

	$this->widget('zii.widgets.CListView', array(
		'viewData' => array('viewData' => $this->viewData),
		'id' => $this->listId,
		'ajaxType' => 'POST',
		'ajaxUrl' => Yii::app()->request->getUrl(),
		'htmlOptions' => array('class' => 'list-view clearfix'),
		'dataProvider' => $this->dataProvider,
		'itemView' => $itemView,
		'itemsCssClass' => 'items clearfix ' . $this->itemsCssClass,
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 8,
		),
		'template' => $this->template,
		'beforeAjaxUpdate' => 'function(id, options) {
				options.data = $(\'#\'+id).comboListView(\'getFilterData\');
				' . $this->beforeAjaxUpdate . '
			}',
		'afterAjaxUpdate' => $afterUpdateFunction,
		'infiniteScroll' => $this->infinite
	));

	$this->endWidget();
	?>


</div>


<script type="text/javascript">
//<![CDATA[
	$(function () {
		$('#' +<?= json_encode($this->listId) ?>).comboListView();
	});
//]]>
</script>
