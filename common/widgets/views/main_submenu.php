<?php
/* @var $submenuItems array */
?>

<div class="container">
	<ul class="main-subnav">
	<?php foreach($submenuItems as $item) : ?>
		<li><a <?= ($item['active'] ? 'class="active"' : '') ?> href="<?= $item['url'] ?>"><span class="icon <?= $item['sprite_gmenu_icon'] ?>"></span> <?= $item['text'] ?></a></li>
	<?php endforeach; ?>
	</ul>
</div>
