<?php
	$user = App7020Helpers::getUserAvatar($this->userId);
?>

<div class="widget-app7020Avatar-container">
	<span class="<?= $this->size; ?> avatar-container">
		<?php if($user['avatar']): ?>
			<?php 
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($user['avatar']);
			?>
			<img style='max-width:100px;border-radius:50%' src="<?php echo $avatarImg; ?>">
		<?php else: ?>
			<?php 
				if ($user['firstname'] && $user['lastname']) {
					$avatarAbrr = mb_substr($user['firstname'], 0, 1) . mb_substr($user['lastname'], 0, 1);
				} else {
					$avatarAbrr = mb_substr($user['username'], 0, 1);
				}
			?>
			<span style='max-width:100px;border-radius:50%' class="circle">
				<?php echo $avatarAbrr; ?>
			</span>
		<?php endif; ?> 
	</span>
</div>	