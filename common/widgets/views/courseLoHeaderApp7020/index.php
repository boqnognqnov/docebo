<?php
// ****** GURUS CIRCLES ****** //
$gurusArray = array_fill(0, 6, array(
	'name' => 'Admin',
	'linkSource' => 'javascript:;',
	'status' => 'online',
	'imgSource' => 'https://placeholdit.imgix.net/~text?txtsize=14&txt=40%C3%9740&w=40&h=40')
);
$listParams = array(
	'arrayData' => $gurusArray,
	'maxVisibleItems' => 2,
	'tooltipPlacement' => 'top',
	'containerClass' => 'topVideosGuruList'
);
?>
<div class="courseLoHeaderApp7020 <?php echo $this->containerClass ?>">
	<div class="hint">
		<div class="app7020-hint app7020-courseLoHeaderHint">
			<h4><?php echo Yii::t('app7020', 'Experts are here to'); ?></h4>
			<h4><?php echo Yii::t('app7020', 'answer your questions!'); ?></h4>
		</div>
	</div>
	<div class="circles">
		<?php $this->widget('common.widgets.GuruCirclesList', $listParams); ?>
	</div>
	<div class="sidebarButton">
		<?php echo CHtml::link(Yii::t('app7020', 'Ask the Expert!'), 'javascript:;', array('class' => 'triggerOpenSidebar')); ?>
	</div>
	<div class="clearfix"></div>
</div>