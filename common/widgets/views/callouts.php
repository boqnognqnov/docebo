<?php
/* @var $messages array */
/* @var $axReadUrl string */
?>

<?php foreach ($messages as $msgId => $message) : ?>
	<?php
		$dataTarget = '';
		switch ($message['button_assigned']) {
			case 'buy_now':
				$dataTarget = 'sidebar-tile-buynow';
				break;
			case 'apps':
				$dataTarget = 'sidebar-tile-apps';
				break;
			case 'marketplace':
				$dataTarget = 'sidebar-tile-marketplace';
				break;
			case 'helpdesk':
				$dataTarget = 'sidebar-tile-helpdesk';
				break;
			default: break;
		}
	?>
<div class="callouts-wrapper btn-<?= $message['button_assigned'] ?> hide" data-target="<?= $dataTarget ?>">

	<div class="message" data-id="<?= $msgId ?>">
		<a class="close" href="#">&times;</a>
		<?php
			if ($message['button_assigned'] == 'generic') {
				echo '<span class="i-sprite is-solid-exclam large pull-left"></span>';
			}
		?>
		<div class="content">
		<?php
			$lang = Yii::app()->getLanguage();
			if ($lang!='en' && !empty($message['translation'][$lang])) {
				echo $message['translation'][$lang];
			} else {
				echo $message['message'];
			}
		?>
		</div>
	</div>

</div>
<?php endforeach; ?>

<script type="text/javascript">
	$(document).ready(function() {
        var settings = {readUrl:'<?= $axReadUrl ?>'};
        Docebo.CalloutMessages.ready(settings);
	});
</script>