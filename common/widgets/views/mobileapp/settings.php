<div ng-app="ngSettingsApp" id="ngAppSettings">
<div class="advanced-main mobile-settings <?php echo $isMultidomain === true ? 'style-multidomain' : ''; ?>">
    <?= CHtml::beginForm($action,'post', array('class' => 'ajax-form', 'id' => 'mobileAppSaveSettings')); ?>
    <div class="section" ng-controller="settingsController">
		<?php if($isMultidomain == true){ ?>
		<h1><?php echo Yii::t('standard','Mobile App Settings'); ?></h1>
		<div class="row even">
			<div class="row">
				<div class="row-fluid">
					<div class="setting-name span2 custom-settings-offsets">
						<?= Yii::t('multidomain', 'Custom settings') ?>
					</div>
					<div class="setting-value span9">
						<?php echo CHtml::checkBox('isEnabled', $isEnabled, array()); ?>
						<?php echo  Yii::t('multidomain','Enable custom settings for this client'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
        <div class="row odd blockable">
                <div class="row">
                    <div class="row-fluid">
                        <div class="span2">
                            <div class="setting-name">
                                <?=Yii::t('mobile', 'Mobile APP Style')?>
                            </div>
							<p class="description">
								<?=Yii::t('mobile', 'Define here the logo and the colors of your mobile app')?>
							</p>
                        </div>
                        <div class="span5 mobile-color-settings span-custom-width">
                            <div class="row">
                                <span><?=Yii::t('mobile', 'Login page Background')?></span>
                                <div class="color-selector-mobile colors">
                                    <div class="color-item">
                                        <div class="wrap">
                                            <?=CHtml::textField('login_page_color',
                                                '',
                                                array(
                                                    'autocomplete' => 'off',
                                                    'class' => 'color-tooltip',
													'ng-model' => 'loginPageColor'
                                                )) ?>
                                            <div id="farbtastic-tooltip-signin-bg-mobile-login" class="tooltip farbtastic-tooltip" style=" opacity: 1; display: none;">
                                                <div class="colorpicker"></div>
                                            </div>
                                            <div class="color-preview-wrap">
                                                <div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php echo $loginColor; ?>;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="description"><?= Yii::t('branding', 'Insert HEX code or choose a color by clicking on the icon'); ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <span> <?=Yii::t('mobile', 'Main App Color')?> </span>
                                <div class="color-selector-mobile colors">
                                    <div class="color-item">
                                        <div class="wrap">
                                            <?=CHtml::textField('primary_color',
                                                '',
                                                array(
                                                    'autocomplete' => 'off',
                                                    'class' => 'color-tooltip',
													'ng-model' => 'primaryColor'
                                                )) ?>
                                            <div id="farbtastic-tooltip-signin-bg-mobile-background" class="tooltip farbtastic-tooltip" style=" opacity: 1; display: none;">
                                                <div class="colorpicker"></div>
                                            </div>
                                            <div class="color-preview-wrap">
                                                <div class="color-preview" style="display: block; width: 21px; height: 21px; background-color: <?php echo $primaryColor; ?>;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="description"><?= Yii::t('branding', 'Insert HEX code or choose a color by clicking on the icon'); ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <span class="app-logo">
                                    <?=Yii::t('mobile', 'App Logo')?>
                                </span>
								<div id="container">
									<div style="float: left">
										<div id="logo-preview-wrapper">
											<img src="<?php echo !empty($logo) ? $logo :  Yii::app()->theme->getLogoUrl(); ?>" id="logo-preview" height="58" />
										</div>
										<a id="pickfiles" class="text-center" class="pickfiles confirm-btn btn-docebo green big" href="javascript:void(0);"><?php echo Yii::t('organization', 'Upload File'); ?></a>
										<input type="hidden" id="uploadedFile" value="" name="uploadedFile" />
										<input type="hidden" id="currentFile" value="<?php echo $originalLogo; ?>" name="currentFile" />
										<br />
										<span>
											<p class="description">
												<?=Yii::t('mobile', 'To avoid logo looking blurry on retina display devices, we recommend to upload an image that is at least twice the size of the current logo display size. Maximum allowed dimensions are 200x100')?>
											</p>
										</span>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="span5 smaller-span custom-width">
                            <div class="row mobile-preview-page">
								<div class="style-arrow arrow-first">
									<span class="mobile-preview-style"><?=Yii::t('mobile', 'App Logo')?></span>
									<div class="bootstro-arrow arrow-s"></div>
								</div>
								<div class="style-arrow arrow-second">
									<span class="mobile-preview-style"><?=Yii::t('mobile', 'Login page background')?></span>
									<div class="bootstro-arrow arrow-s"></div>
								</div>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <span><?=Yii::t('mobile', 'Screen preview page')?></span>
                                        <div class="login-preview" id="login-preview">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="preview-logo"><img src="<?php echo !empty($logo) ? $logo : Yii::app()->theme->getLogoUrl(); ?>" id="logo_preview_img" /></div>
                                                </div>
                                                <div class="row">
                                                    <div class="preview-username">
                                                        <i class="icon-user-mobile" style="margin-left: 5px; margin-top: 2px;"></i>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="preview-password" >
                                                       <i class="icon-lock-mobile" style="margin-left: 5px; margin-top: 2px;"></i>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="preview-submit">
                                                        <input class="btn btn-docebo green big" value="LOGIN" type="button"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="main-app-container">
                                                <div class="colored-header" id="colored-header">
                                                    <div class="style-arrow arrow-third">
														<span class="mobile-preview-style"><?=Yii::t('mobile', 'Main App color')?></span>
														<div class="bootstro-arrow arrow-s"></div>
													</div>
													<i id="hamburger-menu" class="icon-menu to-left"></i>
                                                    <i id="search" class="icon-mobile-search to-right"></i>
                                                </div>
                                                <div class="inner-preview"><!-- Holds the background image--></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="values">
                        <div class="control-group">

                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row-fluid right-buttons">
        <?= CHtml::submitButton('Save Changes', array(
            'class' => 'btn btn-docebo green big',
            'name' => 'submit',
			'id' => 'saveButton'
        )); ?>
		<?php if($isMultidomain === true){ ?>
			<a href="<?php echo Docebo::createAdminUrl('MultidomainApp'); ?>" class="btn-docebo black big margin-left"><?=Yii::t('standard', '_CANCEL')?></a>
		<?php } ?>
    </div>
    <?= CHtml::endForm(); ?>
</div>
</div>