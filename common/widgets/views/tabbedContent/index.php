<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 25.11.2015 г.
 * Time: 13:43
 *
 * @var $content array
 * @var $containerClass string
 */
?>
<div class="tabbed-content <?=$containerClass?>">
    <ul class="tabbed-content-nav">
        <?php foreach($content as $id => $item): ?>
        <li <?= $item['active'] == true ? 'class="active"' : '' ?> ><a href="#tab-<?=$id ?>" data-toggle="tab"><?= (($tabsUppercased) ? strtoupper($item['header']) : $item['header']); ?></a></li>
        <?php endforeach; ?>
    </ul>
    <div class="tabbed-content-tab-content">
        <?php foreach($content as $id => $item): ?>
        <div class="tabbed-content-tab-pane <?= $item['active'] == true ? 'active' : '' ?>" id="tab-<?= $id ?>">
            <?= $item['content'] ?>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('.tabbed-content.<?=$containerClass?> .tabbed-content-nav').boostrapSmoothActiveLine({background: '<?= $tabSliderColor ?>'});
    })
</script>