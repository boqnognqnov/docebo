<?php /* @var $this Apps2Grid */ ?>

<h3 class="content-title"><?=Yii::t('apps', 'Extend and Integrate your Docebo!')?></h3>
<p></p>
<div class="clearfix"></div>
<p></p>

<? if ($this->featuredApps) { ?>
	<h3 class="promoted-apps-header"><?=Yii::t('apps', 'Top Docebo Extensions')?></h3>
    <div class="row-fluid promoted-apps">
    	<? foreach($this->featuredApps as $app): ?>
    		<div class="span4">
    			<div class="promoted-app">
    				<?php $this->renderFeaturedApp($app); ?>
    			</div>
    			<div class="clearfix"></div>
    		</div>
    	<? endforeach; ?>
    	<div class="clearfix"></div>
    </div>
<? }  ?>

<p></p>
<h3 class="cover-title"><?=$this->coverTile?></h3>

<?php if(($this->action=='myapps' && empty($this->apps))) { ?>
	<div class="myapps-empty">
		<i><?=Yii::t('apps', "You haven't activated any apps yet")?></i>
	</div>
	
<?php } else {  ?>
	<?php
        /* @var $this AppsGrid */
        foreach ($this->apps as $item) {
            if (is_string($item)) {
                echo $item;
            }
            else {
                $this->widget('common.widgets.Apps2Widget', array(
                    'app' => $item,
                    'isECSInstallation' => $isECSInstallation
                ));
            }
        }
    ?>
<? } ?>

