<?php
	$params = array_merge($this->params, $this->_params);
?>
<div class="app7020AnalyticStatistics" id="<?php echo $params['id']; ?>" <?php echo CHtml::renderAttributes($this->htmlOptions)?>>
	<?php
	switch ($params['mode']) {
		case 'manageInvitations':
			$this->controller->renderPartial('common.widgets.views.app7020AnalyticStatistics.manageInvitations', array('params' => $params));
			break;

		default:
			echo Yii::t('app7020', 'Mode are not selected');
			break;
	}
	?>
</div>