<?php
$timeWords = array('d' => '<span>d</span>', 'h' => '<span>h</span>', 'm' => '<span>m</span>');
?>
<div class="row-fluid <?php echo 'analytic_' . $params['mode']; ?>">
	<div class="totalInvitedPeople">
		<i class="fa fa-user-plus"></i>
		<p class="count"><?php echo ($params['data']['totalInvitedPeople']) ? $params['data']['totalInvitedPeople'] : 0 ?></p>
		<p class="text"><?php echo Yii::t('app7020', 'Total Invited People'); ?></p>
	</div>
	<div class="globalWatchRate">
		<i class="fa fa-pie-chart"></i>
		<p class="count"><?php echo ($params['data']['globalWatchRate']) ? $params['data']['globalWatchRate'] : 0 ?><span>%</span></p>
		<p class="text"><?php echo Yii::t('app7020', 'Global watch rate'); ?></p>
	</div>
	<div class="avgReactionTime">
		<i class="fa fa-clock-o"></i>
		<p class="count"><?php echo App7020Helpers::secondsToWords($params['data']['avgReactionTime'], $timeWords, true, true, true, false)  ?></p>
		<p class="text"><?php echo Yii::t('app7020', 'Avg. reaction time'); ?></p>
	</div>
	<div class="globalInvitationsBreakdown">
		<div class="text">
			<div class="wrapper">
				<p><?php echo Yii::t('app7020', 'Global'); ?></p>
				<p><?php echo Yii::t('app7020', 'Invitations'); ?></p>
				<p><?php echo Yii::t('app7020', 'breakdown'); ?></p>
			</div>
			<i class="fa fa-user-plus"></i>
		</div>
		<div class="jsCounterWrapper">
			<canvas id="jsCounter" width="97" height="97" data-watched="<?php echo ($params['data']['watched']) ? $params['data']['watched'] : 0 ?>" data-notWatched="<?php echo ($params['data']['notWatched']) ? $params['data']['notWatched'] : 0 ?>"></canvas>
			<div id="jsCounterTooltip">
				<p class="count"></p>
				<span class="watchedLabel"><?php echo Yii::t('app7020', 'Watched'); ?></span>
				<span class="notWatchedLabel"><?php echo Yii::t('app7020', 'Not yet watched'); ?></span>
			</div>
		</div>
	</div>
</div>