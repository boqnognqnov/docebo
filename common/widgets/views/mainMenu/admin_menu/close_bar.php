<div class="inner-content">
	<div class="rounded-box grey">
		<i class="fa fa-group"></i>
	</div>
	<div class="title">
	<?php if($this->showUsersInfo): ?>
		<h1><?= Yii::t('order','Active users') ?>:&nbsp;<span class="active"><?= $this->activeUsers ?> / </span><span class="bought"><?= $this->boughtUsers ?></span></h1>
		<a class="open-dialog" href="<?= Docebo::createLmsUrl('site/axHelpDesk') ?>" data-dialog-class="helpdesk-modal users-info-dialogs" closeOnOverlayClick="false" closeOnEscape="false" rel="helpdesk-modal">
			<span><?= Yii::t('standard','Contact us') ?></span><br>
		</a>

		<!-- BILLING STUFF -->
		<?php if ($this->enableBilling) : ?>
			<?php if (Billing::isWireTransfer()) : ?>
				<a href="javascript:;" onclick="contactUs()">
					<span class='buy-now'> <?= Yii::t('userlimit','Get more users') ?></span>
				</a>
			<?php else : ?>
				<a href="<?= $this->buyNowUrl ?>">
					<span class='buy-now'> <?= Yii::t('userlimit','Get more users') ?></span>
				</a>
			<?php endif; ?>
		<?php endif; ?>
		<!-- BILLING STUFF -->

	<?php else: ?>
		<h1><?= Yii::t('menu', 'Manage your LMS')?></h1>
	<?php endif; ?>
	</div>

	<div class="buttons">
		<?php foreach($this->adminButtons as $button): ?>
		<a href="<?= $button['url'] ?>" data-toggle="tooltip" data-placement="top" title="<?= $button['label'] ?>" data-original-title="<?= $button['label'] ?>" rel="tooltip" class="<?=$button['class']?>">
			<div class="rounded-box">
					<span class="fa-stack">
						<?php if(is_array($button['fa-icon'])): ?>
							<?php foreach($button['fa-icon'] as $icon): ?>
								<i class="fa <?=$icon['icon']." ".$icon['stacking']?>"></i>
							<?php endforeach; ?>
						<?php endif; ?>
					</span>
			</div>
		</a>
		<?php endforeach; ?>
	</div>
</div>
<i id="close-button" class="fa fa-close"></i>

<script type="text/javascript">
	//<![CDATA[
	function contactUs()
	{
		if ($('#expiration-modal').length)
			$('#expiration-modal').dialog2('close');

		$('#contactDialog').trigger('click');
	}

	// Fix for iPad, etc mobile devices not opening the link, because of tooltips
	$(document).on('touchstart', 'ul#menu > li > div#admin > div#close-bar > div.inner-content > div.buttons > a', function(e) {
		e.preventDefault();
		window.location.href = $(this).attr('href');
	});

	//]]>
</script>