<div class="message">
	<?=Yii::t('apps', '<span>Expand now</span> your LMS with new <span>APPs,<br/> integrations</span> and <span>additional features!</span>')?>
</div>
<div class="icons">
	<i class="fa fa-link"></i>
	<i class="fa fa-comments-o"></i>
	<i class="fa fa-group"></i>
	<i class="fa fa-bell"></i>
	<i class="fa fa-share-alt"></i>
</div>
<div class="discover">
	<a class="btn-docebo green big" href="<?=Docebo::createAbsoluteLmsUrl('app/index')?>"><?=Yii::t('apps', 'Discover APPs & Features')?></a>
</div>