<?php /* @var $menu_admin Array */ ?>
<?php /* @var $menu_apps Array */ ?>

<div id="close-bar"><?php $this->render('mainMenu/admin_menu/close_bar', array()); ?></div>
<div class="second-menu-spacer second-menu-spacer-platform-info" id="admin-list">
	<div class="row-fluid">
		<?php $isAdditionalMenuAppearing = false;
		if (SandboxCreatorHelper::shouldShowSandboxUI()) { $isAdditionalMenuAppearing = true; ?>
			<div class="span12">
				<?php if (SandboxCreatorHelper::isCreationAllowedByERP() && (!SandboxCreatorHelper::isCreated() && !SandboxCreatorHelper::isInProgress())) {
					// Show "Create sandbox" button ?>
					<div id="sandbox-creator" class="sandbox-creator new-menu <?= SandboxCreatorHelper::getSandboxActivationStatus() ?>">
						<p class="pull-right create-button-wrapper">
							<a data-dialog-class="create-sandbox-dialog"
							   href="<?= Docebo::createAdminUrl('sandbox/create') ?>"
							   class="open-dialog btn-docebo green full text-center"><?= Yii::t('sandbox', 'Activate sandbox') ?></a>
						</p>

						<div class="create-description">
							<?= Yii::t('sandbox', 'Want to test drive all the new features 3 weeks before they get released? Activate now your free sandbox.') ?>
						</div>


						<div class="clearfix"></div>
					</div>
					<script>
						$(function () {
							$(document).on('dialog2.closed', '.create-sandbox-dialog', function () {
								if ($(this).find('.reload-after-close').length > 0) {
									window.location.reload();
								}
							});
						});
					</script>
				<?php } elseif (SandboxCreatorHelper::isInProgress()) {
				// Show "Sandbox activating" button ?>
					<div id="sandbox-creator" class="sandbox-creator new-menu <?= SandboxCreatorHelper::getSandboxActivationStatus() ?>">
						<div class="pull-right in-progress">
							<a href="#"
							   class="btn-docebo green full text-center disabled"><?= Yii::t('sandbox', 'Activating your sandbox...') ?></a>
						</div>
						<div class="create-description">
							<?= Yii::t('sandbox', 'Want to test drive all the new features 3 weeks before they get released? Activate now your free sandbox.') ?>
						</div>
						<div class="clearfix"></div>
					</div>
				<?php } elseif (SandboxCreatorHelper::isCreated()) {
				// Show "Go to sandbox" button ?>
					<div id="sandbox-creator" class="sandbox-creator new-menu <?= SandboxCreatorHelper::getSandboxActivationStatus() ?>">
						<div class="pull-right create-button-wrapper">
							<a target="_blank" href="<?= SandboxCreatorHelper::getSandboxUrl() ?>"
							   class="btn-docebo green full text-center"><?= Yii::t('sandbox', 'Go to sandbox') ?></a>
						</div>
						<div class="create-description">
							<?= Yii::t('sandbox', 'Your sandbox is ready. Enjoy it!') ?>
						</div>
					</div>
				<?php } ?>
			</div>
		<?php } ?>

		<div class="menu-section span3" id="admin-menu" style="<?=$isAdditionalMenuAppearing === true ? 'margin-left: 0':''?>">
			<div class="header row-fluid">
				<div class="title span7"><?=Yii::t('menu', 'Admin')?></div>
				<div class="subtitle span5 version-number"><?=Yii::t('standard', '_VERSION') . ' ' . Settings::get('core_version', '6.4')?></div>
			</div>
			<div class="content">
			<?php foreach($menu_admin as $key => $elem): ?>
				<?php if(isset($elem['items'])): ?>
				<div class="slot">
					<div class="icon">
						<div class="rounded-box blue">
							<?php if(is_array($elem['fa-icon'])): ?>
							<span class="fa-stack">
							<?php foreach($elem['fa-icon'] as $icon): ?>
								<i class="fa <?=$icon['icon']." ".$icon['stacking']?>"></i>
							<?php endforeach; ?>
							</span>
							<?php else: ?>
							<i class="fa <?=$elem['fa-icon']?>"></i>
							<?php endif; ?>
						</div>
					</div>
					<div class="body">
						<h1><?=$elem['label']?></h1>
						<ul>
						<?php foreach ($elem['items'] as $item): ?>
							<?php if ($this->godadmin || Yii::app()->user->checkAccess($item['permission'])): ?>
							<li class="item"><a href="<?=$item['href']?>"><?=$item['label']?></a><?=$item['newLabel']?></li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
			</div>
		</div>
		<?php if ($this->godadmin || $this->admin): ?>
		<div class="menu-section span9" id="apps-menu">
			<div class="header row-fluid">
				<div class="title span7"><?=Yii::t('apps', 'Paid apps')?></div>
				<div class="subtitle span5">
					<?php if (Yii::app()->user->getIsGodadmin()) { ?>
					<a class="discover" href="<?=Docebo::createAbsoluteLmsUrl('app/index');?>"><?=Yii::t('apps', 'Discover more APPs & Features')?></a>
					<?php } ?>
				</div>
			</div>
			<?php if (empty($menu_apps) && ($this->enable_user_billing || isset(Yii::app()->request->cookies["reseller_cookie"]))) : ?>
			<div class="content row-fluid">
				<div class="slot span12 no-apps">
				<?php $this->render('mainMenu/admin_menu/noapps'); ?>
				</div>
			</div>
			<?php else: ?>
				<?php $i = 0; $apps_count = count($menu_apps); ?>
				<?php foreach($menu_apps as $key => $app): ?>
					<?php if($this->godadmin || (isset($app['main_element']['permission']) && $this->checkMenuPermissions($app['main_element']['permission']))): ?>
						<?php if($i % 3 == 0): ?>
						<div class="content row-fluid">
						<?php endif; ?>
						<div class="slot span4" id="<?=$app['id']?>">
							<div class="icon">
								<div class="rounded-box dark">
									<?php if($app['main_element']['app_icon']): ?>
										<?php if(is_array($app['main_element']['app_icon'])): ?>
											<span class="fa-stack">
											<?php foreach($app['main_element']['app_icon'] as $icon): ?>
												<i class="fa <?=$icon['icon']." ".$icon['stacking']?>"></i>
											<?php endforeach; ?>
											</span>
										<?php else: ?>
											<i class="fa <?=$app['main_element']['app_icon']?>"></i>
										<?php endif; ?>
									<?php elseif($app['main_element']['app_initials']): ?>
										<i class="app-initials"><?=$app['main_element']['app_initials']?></i>
									<?php endif; ?>
								</div>
							</div>
							<div class="body">
								<h1><?=str_replace("&nbsp", "", $app['main_element']['label'])?></h1>
								<?php if($app['main_element']['link'] && ($app['main_element']['link'] !== 'javascript:void(0);')):?>
									<div class="item"><a href="<?=$app['main_element']['link']?>"><?=Yii::t('standard', '_MANAGE')?></a></div>
								<?php endif; ?>
								<?php if (!empty($app['items'])): ?>
								<?php foreach ($app['items'] as $label => $href): ?>
									<div class="item"><a href="<?=$href?>"><?=$label?></a></div>
								<?php endforeach; ?>
								<?php endif; ?>
								<?php if($app['main_element']['settings'] && $this->godadmin):?>
									<div class="item"><a href="<?=$app['main_element']['settings']?>"><?=Yii::t('standard', 'Settings')?></a></div>
								<?php endif; ?>
							</div>
						</div>
						<?php if ((($i+1) % 3 == 0) || ($i == ($apps_count - 1))): ?>
						</div>
						<?php endif; ?>
						<?php $i++; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>
</div>