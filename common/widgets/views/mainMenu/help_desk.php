<div class="second-menu-spacer">
	<?php echo CHtml::beginForm($this->helpUrl, 'post', array(
		'id' => 'help-desk-request',
		'enctype' => 'multipart/form-data',
	)); ?>
		<div class="control-group request">
			<label for="help-desk-text"><i class="icon-help iconrequest"></i> <?php echo Yii::t('userlimit', 'Contact Docebo team'); ?></label>
			<div class="controls form-element">
				<textarea id="help-desk-text" name="help-desk-text" class="" rows="6"></textarea>
			</div>
		</div>
		<div class="upload">
			<label for="upload"></label>
			<div class="form-element">
				<input type="file" id="help-desk-upload" name="help-desk-upload" />
			</div>
		</div>
		<div class="control-group target">
			<p><?php echo Yii::t('userlimit', 'Choose the kind of support that you want'); ?></p>
			<div class="controls form-element form-radio-element">
				<input type="radio" id="help-desk-hd" name="help-desk-target" checked="checked" /> <label class="control-label" for="help-desk-hd"><?php echo Yii::t('userlimit', 'Help Desk'); ?></label>
			</div>
			<div class="controls form-element form-radio-element">
				<input type="radio" id="help-desk-sales" name="help-desk-target" /> <label class="control-label" for="help-desk-sales"><?php echo Yii::t('userlimit', 'Sales team'); ?></label>
			</div>
		</div>
		<div class="text-right">
			<button type="submit" class="btn-docebo green big"><?php echo Yii::t('standard', '_SEND'); ?></button>
			<button type="reset" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></button>
		</div>
	<?php echo CHtml::endForm(); ?>

    <div class="menu-helpdesk-search">
        <h2><i class="icon-help manual"></i> <?php echo Yii::t('userlimit', 'Docebo Knowledge Base'); ?></h2>

        <div class="row-fluid">
            <div class="span6">

                <ul class="">
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Getting started') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Users') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Course management') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Report') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Settings') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Apps & Integrations') ?></a></li>
                    <li><span class="i-sprite is-arrow-right"></span><a href="#"><?= Yii::t('userlimit', 'Payments & Billing Policies') ?></a></li>
                </ul>
            </div>
            <div class="span6">
                <?= CHtml::image(Yii::app()->theme->baseUrl . '/images/userlimit/menu_help_desk.png') ?>
                <br>
                <a class="btn-docebo black big" href="#"><?= Yii::t('player', 'Go to the marketplace') ?></a>
            </div>
        </div>
    </div>
</div>