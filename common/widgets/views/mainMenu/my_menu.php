<?php

// Get avatar info here
$urlOfAvatar = CoreUser::model()->findByPk(Yii::App()->user->idst)->getAvatarImage(true);
$profileAvatar = CoreUser::model()->findByPk(Yii::App()->user->idst)->avatar;
$noProfileAvatar = (strlen($profileAvatar) == 0) ? true : false;

?>
<div id="new-my-menu-top-bg">
</div>
<div id="new-my-menu-top-blur">
</div>
<div id="new-my-menu-top-profile">
	<div class="profile-info-container">
		<a class="open-dialog profile-avatar change-profile-avatar" data-dialog-class="modal-change-avatar"
		   href="<?= Docebo::createAppUrl('lms:user/changeAvatar') ?>"
		   title="<?php echo Yii::t('menu_over', '_CHANGE_AVATAR'); ?>">
			<i class="fa fa-camera fa-lg"></i>
			<?php
			if (!$noProfileAvatar) {
				echo Yii::app()->user->getAvatar('pull-left');
			} else { ?>
			<div id="default-new-profile-icon">
				<i class="fa fa-user fa-2x"></i>
			</div>
			<?php } ?>
		</a>
		<div class="profile-details-container">
			<span class="mainMenuUsername"><?php echo Yii::app()->user->getDisplayName(); ?></span>
			<br />
			<?php if (Settings::get('profile_only_pwd', 'on') == 'on') : ?>
				<a href="<?= Docebo::createAppUrl('lms:user/editProfile') ?>" class="open-dialog view-profile-link" data-dialog-class="modal-edit-profile" title="<?php
				echo Yii::t('profile', '_CHANGEPASSWORD'); ?>"><?php echo Yii::t('profile', '_CHANGEPASSWORD'); ?></a>
			<?php else: ?>
				<a href="<?= Docebo::createAppUrl('lms:user/editProfile') ?>" class="open-dialog view-profile-link" data-dialog-class="modal-edit-profile" title="<?php
				echo Yii::t('standard', '_VIEW_PROFILE'); ?>"><?php echo Yii::t('standard', '_VIEW_PROFILE'); ?></a>
			<?php endif; ?>

			<a href="<?= Yii::app()->createAbsoluteUrl('site/logout') ?>" title="<?php echo Yii::t('standard', '_LOGOUT'); ?>" class="profile-sign-out">
				<?php echo Yii::t('standard', '_LOGOUT'); ?>
			</a>

			<?php
			// Raise event to let plugins add extra content
			Yii::app()->event->raise('MainMenuAfterNewMenuProfileRender', new DEvent($this, array()));
			?>
		</div>
	</div>

	<?php
	// Is Coach plugin active
	if($coachPluginActive) {
		// Should we display Ask Guru button in the Profile
		if($askExpertActive) {
		?>
		<a id="new-menu-profile-ask-guru"
		   href="<?= Docebo::createApp7020Url('askTheExpert/index#/allQuestions/addQuestion') ?>" class="coachapp"
		   data-toggle="tooltip" data-placement="top" title=""
		   data-original-title="<?php echo Yii::t('app7020', 'Ask Expert'); ?>" rel="tooltip">
				<span class="fa-stack fa-1x ask-expert-7020">
					 <i class="fa fa-comment-o fa-stack-2x"></i>
					 <i class="fa fa-user fa-stack-1x"></i>
				</span>
		</a>
		<?php }

		//Should we display Share button in the Profile
		if(PluginManager::isPluginActive('Share7020App')){ ?>
		<a id="new-menu-profile-share" href="<?= Docebo::createApp7020Url('assets/index') ?>"
		   data-toggle="tooltip" data-placement="top" title=""
		   data-original-title="<?= Yii::t('app7020', 'Contribute!') ?>" rel="tooltip">
			<i class="fa fa-cloud-upload fa-lg"></i> <!-- ?= Yii::t('app7020', 'Contribute!')? -->
		</a>
		<?php } ?>

	<?php
	}
	?>
</div>
<br />
<?php
// Do two column menu here and show Coach and Share menu only if the Coach plugin is active !!!
// Otherwise it will be regular one column menu
if($coachPluginActive) {
?>
<br />
<div class="row-fluid">
	<div class="span6">
<?php
}
	//Show regular menu
?>

	<div class="second-menu-spacer">
			<div class="menu-section">
				<div class="content">
					<div class="slot">
						<div class="icon">
							<div class="rounded-box blue">
								<i class="fa fa-graduation-cap"></i>
							</div>
						</div>
						<div class="body">
							<h1><?= Yii::t('standard', 'Learn') ?></h1>
							<ul>
							<?php foreach ($menu_home as $key => $elem): ?>
								<?php

								if($elem['target'] == 'iframe') {
									$elem['href'] = Docebo::createLmsUrl('site/embedIframe&iframeId='.$elem['id']);
									$elem['target'] = '_self';
								}

								?>
								<li class="item"><a href="<?=$elem['href']?>"<?=($elem['target'] != 'self' && !is_null($elem['target']) ? ' target="'.$elem['target'].'"' : '')?>><?=$elem['label']?></a></li>
							<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
	</div>

<?php
// Show Coach and Share menu only if the Coach plugin is active !!!
if($coachPluginActive) {
	?>
		</div>
		<div class="span6">
			<div class="second-menu-spacer">
				<div class="menu-section">
					<div class="content">
						<div class="slot">
							<div class="icon">
								<div class="rounded-box blue">
									<span class="fa-stack">
										<i class="fa fa-comment-o fa-stack-2x"></i>
										<?php if(PluginManager::isPluginActive('Share7020App')): ?>
										<i class="fa fa-share-alt fa-stack-1x"></i>
										<?php endif; ?>
									</span>
								</div>
							</div>
							<div class="body">
								<h1><?= PluginManager::isPluginActive('Share7020App') ? Yii::t('app7020', 'Coach & Share') : Yii::t('app7020', 'Coach') ?></h1>
								<ul>
									<?php if (PluginManager::isPluginActive('Share7020App')): ?>
									<li class="item">
										<a href="<?= Docebo::createLmsUrl('channels/index', array('#' => '/myChannel')) ?>" title="<?= Yii::t('app7020', 'My Channel') ?>"><?= Yii::t('app7020', 'My Channel') ?></a>
									</li>
									<?php endif; ?>
									<li class="item">
										<a href="<?= Docebo::createApp7020Url('askTheExpert/index', array("#" => "/")) ?>" title="<?= Yii::t('app7020', 'Questions & Answers') ?>"><?= Yii::t('app7020', 'Questions & Answers') ?></a>
									</li>

									<?php if(App7020Experts::isUserExpert(Yii::App()->user->idst) ||
											 CoreUser::isUserGodadmin(Yii::App()->user->idst)) { ?>
									    <li class="item">
										    <a href="<?= Docebo::createApp7020Url('guruDashboard/index', array('#' => '/')) ?>" title="<?= Yii::t('app7020', 'My Expert Tasks') ?>"><?= Yii::t('app7020', 'My Expert Tasks') ?></a>
									    </li>
                                    <?php } ?>
									<?php if(App7020Experts::isUserExpert(Yii::App()->user->idst) || CoreUser::isUserGodadmin(Yii::App()->user->idst)) { ?>
										<li class="item">
											<a href="<?= Docebo::createApp7020Url('activity/myCoachActivity') ?>" title="<?= Yii::t('app7020', 'My Coaching Activity'); ?>"><?= Yii::t('app7020', 'My Coaching Activity'); ?></a>
										</li>
									<?php } ?>

								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<?php
}
?>

<script type="text/javascript">
//<![CDATA[
	// Fix for iPad, etc mobile devices not opening the link, because of tooltips
	$(document).on('touchstart', 'ul#menu > li > div.second-menu > div#new-my-menu-top-profile > a',
		function(e) {
			e.preventDefault();
			window.location.href = $(this).attr('href');
	});

//]]>
</script>
