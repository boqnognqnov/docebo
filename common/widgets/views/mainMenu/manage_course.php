<?php /* @var $this mainMenu */ 

$pUserRights = Yii::app()->user->checkPURights(Yii::app()->request->getParam('course_id', 0));

$courseEditRights = Yii::app()->user->checkAccess("/lms/admin/course/mod");
$courseCreateRights = Yii::app()->user->checkAccess("/lms/admin/course/add");

$disabledActions = array(); Yii::app()->event->raise('BeforeCourseTreeRowActionsRender', new DEvent($this, array('course' => Yii::app()->player->course, 'disabledActions' => &$disabledActions))); 
$canTogglePublishStatus = ($this->godadmin || ($this->admin_has_course && $courseEditRights)) && !in_array('advanced-settings', $disabledActions);
$canManageElearningTrainingMaterials =  $this->godadmin || $pUserRights->isInstructor || ($this->admin_has_course && Yii::app()->user->checkAccess("/lms/admin/course/mod")); 
$canManageElearningEnrollments = $this->godadmin || $pUserRights->isInstructor || ($this->admin_has_course && Yii::app()->user->checkAccess('/lms/admin/course/mod'));

	$canManageSessionTrainingMaterials = false;
	if(in_array(Yii::app()->player->course->course_type, array(LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR)))
		$canManageSessionTrainingMaterials = (Yii::app()->user->checkAccess('/lms/admin/course/mod') && (!Yii::app()->user->getIsPu() || Yii::app()->user->checkAccess("/lms/admin/classroomsessions/view"))) || $this->usercourse_level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR;

$canSeeCourseReport = $this->godadmin || $pUserRights->isInstructor || $this->admin_has_course;
$canSeeSessionsReport = !Yii::app()->user->getIsPu() || Yii::app()->user->checkAccess("/lms/admin/course/view") || $this->usercourse_level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR;
$canSetAdvancedSettings = ($this->godadmin || $pUserRights->all || ($this->admin_has_course && Yii::app()->user->checkAccess('/lms/admin/course/mod'))) && !in_array('advanced-settings', $disabledActions);
$canEditCourse = ($this->godadmin || ($this->admin_has_course && $courseEditRights)) && !in_array('edit', $disabledActions);
?>
<?php $canSeeSessionsReport = !Yii::app()->user->getIsPu() || Yii::app()->user->checkAccess("/lms/admin/course/view") || $this->usercourse_level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR; ?>
<?php
$level = LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id);
$courseType = Yii::app()->db->createCommand()
	->select('course_type')
	->from(LearningCourse::model()->tableName())
	->where('idCourse =:id',array(':id'=>$this->course_id))->queryScalar();
$canSetAdvancedSettings = ((($level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR && $courseType == LearningCourse::TYPE_CLASSROOM && PluginManager::isPluginActive('GeApp'))
		|| $this->godadmin
		|| ($this->admin_has_course && Yii::app()->user->checkAccess('/lms/admin/course/mod')))
	&& !in_array('advanced-settings', $disabledActions));  ?>

<div class="second-menu-spacer">
	<h2><?= Yii::app()->player->course->name ?></h2>

	<p class="course-publish-status clearfix">
        <?php if(Yii::app()->player->course->course_type == LearningCourse::TYPE_ELEARNING):?>
		    <?=Yii::t('standard', '_STATUS')?> :
            <?php if (Yii::app()->player->course->status == LearningCourse::$COURSE_STATUS_EFFECTIVE) : ?>
                <span id="player-course-status" class="published"><?php echo Yii::t('standard', 'Published'); ?></span>
				<?php if ($canTogglePublishStatus): ?>
					<a class="manage-link" href="<?= $this->courseSettingsUrl ?>"><?=Yii::t('standard', '_MANAGE')?></a>
				<?php endif; ?>
            <?php else: ?>
                <span id="player-course-status" class="not-published"><?php echo Yii::t('standard', 'Unpublished'); ?></span>
				<?php if ($canTogglePublishStatus): ?>
					<a class="manage-link" href="<?= $this->courseSettingsUrl ?>"><?=Yii::t('standard', '_MANAGE')?></a>
				<?php endif; ?>
            <?php endif;?>
        <?php endif; ?>
	</p>

	<ul class="with-paragraph">
    <?php if(Yii::app()->player->course->course_type == LearningCourse::TYPE_ELEARNING):?>

	    <?php if($canManageElearningTrainingMaterials): ?>
		<li><a href="<?= $this->courseTrainingResourcesUrl ?>">
			<div class="row-fluid">
				<div class="span2 font-awesome"><i class="fa fa-list"></i></div>
				<div class="span10">
					<strong><?php echo Yii::t('standard', 'Training materials'); ?></strong>
					<p><?php echo Yii::t('course', 'Upload and organize your training contents'); ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a></li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', 'Training materials'),
				'url' => '/legacy'.$this->courseTrainingResourcesUrl,
				'icons' => ['view-list-alt']
			);?>
	    <?php endif; ?>

		<?php if ($canManageElearningEnrollments):  ?>
		<li><a href="<?= $this->courseEnrollmentsUrl ?>">
			<div class="row-fluid">
				<div class="span2 font-awesome"><i class="fa fa-group"></i></div>
				<div class="span10">
					<strong><?php echo Yii::t('standard', 'Enrollments'); ?></strong>
					<p><?php echo Yii::t('course', 'Create and enroll users into the course'); ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a></li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', 'Enrollments'),
				'url' => '/legacy'.$this->courseEnrollmentsUrl,
				'icons' => ['accounts']
			);?>
		<?php endif; ?>

		<?php if ($canSeeCourseReport):  ?>
		<li><a href="<?= $this->courseReportsUrl ?>">
			<div class="row-fluid">
				<div class="span2 font-awesome"><i class="fa fa-bar-chart"></i></div>
				<div class="span10">
					<strong><?php echo Yii::t('standard', '_REPORTS'); ?></strong>
					<p><?php echo Yii::t('course', 'View course statistics and detailed user reports'); ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a></li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', '_REPORTS'),
				'url' => '/legacy'.$this->courseReportsUrl,
				'icons' => ['chart']
			);?>
		<?php endif; ?>

		<?php elseif (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM && PluginManager::isPluginActive('ClassroomApp')): ?>
			<li>
				<a href="<?= $this->sessionEnrollmentsUrl ?>">
					<div class="row-fluid">
						<div class="span2"><i class="manage-ico sessions-enrollment"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('classroom', 'Sessions and Enrollments'); ?></strong>
							<p><?php echo Yii::t('classroom', 'Manage classroom sessions and enroll users to a classroom session'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('classroom', 'Sessions and Enrollments'),
				'url' => '/legacy'.$this->sessionEnrollmentsUrl,
				'icons' => ['accounts']
			);?>
			<?php if($canManageSessionTrainingMaterials): ?>
			<li>
				<a href="<?= Docebo::createLmsUrl('ClassroomApp/session/manageLearningObjects', array('course_id' => Yii::app()->player->course->getPrimaryKey())) ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-list"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('standard', 'Training materials'); ?></strong>
							<p><?php echo Yii::t('organization', 'ADD TRAINING RESOURCES'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', 'Training materials'),
				'url' => '/legacy'.Docebo::createLmsUrl('ClassroomApp/session/manageLearningObjects', array('course_id' => Yii::app()->player->course->getPrimaryKey())),
				'icons' => ['view-list-alt']
			);?>
			<?php endif; ?>

		    <?php if($canSeeSessionsReport): ?>
			<li>
				<a href="<?= $this->reportSummaryUrl ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-pie-chart"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('stats', 'Session statistics'); ?></strong>
							<p><?php echo Yii::t('course', 'View session statistics for enrolled users'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('stats', 'Session statistics'),
				'url' => '/legacy'.$this->reportSummaryUrl,
				'icons' => ['chart-donut']
			);?>
		    <?php endif; ?>
		    <?php if($canSeeCourseReport): ?>
			<li>
				<a href="<?= $this->courseReportsUrl ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-bar-chart"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('standard', '_REPORTS'); ?></strong>
							<p><?php echo Yii::t('course', 'View course statistics and detailed user reports'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
				<?php $actionButtons[] = array(
					'label' => Yii::t('standard', '_REPORTS'),
					'url' => '/legacy'.$this->courseReportsUrl,
					'icons' => ['chart']
				);?>
		    <?php endif; ?>

	<?php elseif (Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR): ?>
		<li>
			<a href="<?=$this->sessionEnrollmentsUrl?>">
				<div class="row-fluid">
					<div class="span2"><i class="webinar-icon-big" style="margin-left: 11px"></i></div>
					<div class="span10">
						<strong><?php echo Yii::t('classroom', 'Sessions and Enrollments'); ?></strong>
						<p><?php echo Yii::t('classroom', 'Manage webinar sessions and enroll users to a webinar session'); ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</a>
		</li>

		<?php $actionButtons[] = array(
			'label' => Yii::t('classroom', 'Sessions and Enrollments'),
			'url' => '/legacy'.$this->sessionEnrollmentsUrl,
			'icons' => ['accounts']
		);?>

		<?php if ($canManageSessionTrainingMaterials): ?>
			<li>
				<a href="<?= Docebo::createLmsUrl('webinar/session/manageLearningObjects', array('course_id' => Yii::app()->player->course->getPrimaryKey())) ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-list"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('standard', 'Training materials'); ?></strong>
							<p><?php echo Yii::t('organization', 'ADD TRAINING RESOURCES'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', 'Training materials'),
				'url' => '/legacy'.Docebo::createLmsUrl('webinar/session/manageLearningObjects', array('course_id' => Yii::app()->player->course->getPrimaryKey())),
				'icons' => ['view-list-alt']
			);?>
		<?php endif; ?>

		<?php if($canSeeSessionsReport): ?>
			<li>
				<a href="<?= $this->reportSummaryUrl ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-pie-chart"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('stats', 'Session statistics'); ?></strong>
							<p><?php echo Yii::t('course', 'View session statistics for enrolled users'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('stats', 'Session statistics'),
				'url' => '/legacy'.$this->reportSummaryUrl,
				'icons' => ['chart-donut']
			);?>
		<?php endif; ?>
		<?php if($canSeeCourseReport): ?>
			<li>
				<a href="<?= $this->courseReportsUrl ?>">
					<div class="row-fluid">
						<div class="span2 font-awesome"><i class="fa fa-bar-chart"></i></div>
						<div class="span10">
							<strong><?php echo Yii::t('standard', '_REPORTS'); ?></strong>
							<p><?php echo Yii::t('course', 'View course statistics and detailed user reports'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('standard', '_REPORTS'),
				'url' => '/legacy'.$this->courseReportsUrl,
				'icons' => ['chart']
			);?>
		<?php endif; ?>
	<?php endif; ?>

	<?php if($canSetAdvancedSettings): ?>
		<li><a href="<?= $this->courseSettingsUrl ?>">
			<div class="row-fluid">
				<div class="span2 font-awesome"><i class="fa fa-gear"></i></div>
				<div class="span10">
					<strong><?php echo Yii::t('standard', 'Settings'); ?></strong>
					<p><?php echo Yii::t('course', 'View and tune the course behaviours in the way that fits you most'); ?></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a></li>
		<?php $actionButtons[] = array(
			'label' => Yii::t('standard', 'Settings'),
			'url' => '/legacy'.$this->courseSettingsUrl,
			'icons' => ['settings']
		);?>
	<?php endif; ?>
		<?php if($canEditCourse):?>
			<li>
				<a href="<?= Docebo::createAbsoluteUrl('admin:courseManagement/index',array('id'=>Yii::app()->player->course->getPrimaryKey(),'open_modal'=>'edit')) ?>">
                    <div class="row-fluid">
                        <div class="span2 font-awesome"><i class="fa fa-pencil-square-o"></i></div>
                        <div class="span10">
                            <strong><?php echo Yii::t('course', 'Course properties'); ?></strong>
                            <p><?php echo Yii::t('course', 'Edit course <strong>type, code, title, description</strong> and <strong>description</strong>'); ?></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </a>
			</li>
            <?php
                $actionButtons[] = array(
                    'label' => Yii::t('course', 'Course properties'),
                    'url' => '/legacy'.Docebo::createAdminUrl('courseManagement/index', array('id' => Yii::app()->player->course->getPrimaryKey(), 'open_modal' => 'edit')),
                    'icons' => ['edit']
                );
            ?>
		<?php endif; /* End Manage Seats for god admins */ ?>

		<?php if(Yii::app()->player->course->selling && Yii::app()->user->getIsGodadmin()): /* Start Manage Seats for god admins */ ?>
			<li>
				<a href="<?= Docebo::createAdminUrl('courseManagement/manageSeats',
					array('id' => Yii::app()->player->course->getPrimaryKey(), 'ref_src' => 'admin')
				); ?>" class='open-dialog course-manage-seats' data-dialog-title="<?php echo Yii::t('course', 'Manage Seats'); ?>"
				data-dialog-class="manage-pu-seats-dialog" data-dialog-id="manage-pu-seats-dialog">
					<div class="row-fluid">
						<div class="span2"><i class="manage-ico manage-purchase-seats-ico"></i>  <!-- span class="maxAvailable"></span --></div>
						<div class="span10">
							<strong><?php echo Yii::t('course', 'Manage Seats'); ?></strong>
							<p><?php echo Yii::t('course', 'Add/Remove free extra seats to power users, that are set as seat managers'); ?></p>
						</div>
						<div class="clearfix"></div>
					</div>
				</a></li>
			<?php $actionButtons[] = array(
				'label' => Yii::t('course', 'Manage Seats'),
				'url' => '/legacy'.Docebo::createAdminUrl('courseManagement/manageSeats', array('id' => Yii::app()->player->course->getPrimaryKey(), 'ref_src' => 'admin')),
				'icons' => ['seat']
			);?>
		<?php endif; /* End Manage Seats for god admins */ ?>
	</ul>
</div>
<!-- /manage -->

<?php
// Push action buttons to Hydra Frontend
if(!empty($actionButtons)) {
	// Attach the "Learner View" button in the beginning
	$actionButtons[] = array(
		'label' => Yii::t('standard', 'Learner View'),
		'url' => Yii::app()->legacyWrapper->getHydraCourseUrl(Yii::app()->player->course),
		'icons' => ['play-circle-outline']
	);
	echo Yii::app()->legacyWrapper->setHydraActionButtons($actionButtons);
}
?>
