<?php
/**
 * @var $this MainMenu
 * @var $menu_home array
 * @var $coachPluginActive boolean
 * @var $askExpertActive boolean
 */
?>
<div class="menu<?php echo ($animate ? ' animate': '');?> new-menu skip-menu-hover second-gen-menu">
	<ul class="menu-square clearfix" id="menu">
		<?php if ($this->all) { ?>
			<!-- home -->
			<li>
				<div class="tile default" data-bootstro-id="bootstroMainMenu"  data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('adminrules', '_ADMIN_MENU');?>">
					<a href="#home">
						<i class="fa fa-bars fa-lg"></i><span></span>
					</a>
				</div>
				<div class="second-menu <?php echo Yii::app()->getLanguage(); ?>" id="home" style="<?= ($coachPluginActive) ? 'width: 580px;' : 'width: auto; min-width: 510px;'; ?>">
					<?php $this->render('mainMenu/my_menu', array(
							'menu_home'         => $menu_home,
							'coachPluginActive' => $coachPluginActive,
							'askExpertActive' => $askExpertActive,

							)); ?>
				</div>
			</li>
			<!-- /home -->
		<?php } ?>


		<?php
		// Let custom plugins add their own custom course types
		$customCourseDescriptors = array();
		$isCourseWithSessions = (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM || Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR);
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
		if(isset($customCourseDescriptors[Yii::app()->player->course->course_type]))
			$isCourseWithSessions = $isCourseWithSessions || $customCourseDescriptors[Yii::app()->player->course->course_type]['session_based'];

		if ($this->is_in_player && !$isCourseWithSessions) : ?>
			<?php
			$showCounter = true;
			if (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess('/lms/admin/course/view')) {
				$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => Yii::app()->user->id,
					'idCourse' => Yii::app()->player->course->idCourse
				));
				if (empty($enrollment)) {
					$showCounter = false; //otherwise in this specific case an "Access denied" error will be raised
				}
			}
			if ($showCounter):
				?>
				<li>
					<div class="tile blue" id="course-play-tile" data-bootstro-id="bootstroCourseStructure" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('standard', 'Course Chapters');?> <strong id='menu-play-lo-stats-completed-tooltip'>0</strong> / <span id='menu-play-lo-stats-total-tooltip'>0</span>">
						<?php if ($this->is_in_training_res) { /* todo fixme after analysis */ ?>
						<a href="#" id="player-lonav-button">
						<?php } else { ?>
						<a href="<?php echo $this->viewCourseUrl ?>">
						<?php } ?>

							<i class="fa fa-play-circle-o fa-lg"></i>
							<br />
							<div class="course-lo-counter" style="">
								<span class="player" id="lo-stats" data-href="<?= $this->courseLoStatsHtmlUrl ?>">
									<strong id="menu-play-lo-stats-completed">0</strong>&#47;<span id="menu-play-lo-stats-total">0</span>
								</span>
							</div>

							<span class="menu2-hidden-title" style="/* top: 2px; */">
								<?= Yii::t('standard', 'Course Chapters'); ?>&nbsp;
								&ndash;
								<? /* span class="player" id="lo-stats" data-href="<?= $this->courseLoStatsHtmlUrl ?>">
									<strong id="menu-play-lo-stats-completed">0</strong> / <span id="menu-play-lo-stats-total">0</span>
								</span  */ ?>
							</span>
						</a>
					</div>

					<?php if ($this->is_in_training_res) {  /* todo fixme after analysis */  ?>
						<div id="player-lonav" class="second-menu">
							<div class="second-menu-spacer">
								<div class="player-lonav-content">
									<h2><?= Yii::app()->player->course->name?></h2>

									<div class="player-lonav-tree"></div>
								</div>
							</div>
						</div>
					<?php } ?>

				</li>
			<?php endif; ?>
		<?php endif;
		if ($this->is_in_player && ($this->userCanAdminPlayerCourse || ($this->admin && $this->admin_has_course))) : ?>
			<li><div class="tile blue" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('standard', 'Manage Course');?>">
					<a href="#" style="margin-top: 15px;">
						<i class="fa fa-sliders fa-lg"></i>
					</a></div>
				<div class="second-menu" id="manage">
					<?php
					// Let custom plugins add their own custom course types
					$customCourseDescriptors = array();
					Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
					if(isset($customCourseDescriptors[Yii::app()->player->course->course_type]))
						echo $customCourseDescriptors[Yii::app()->player->course->course_type]['manage_course_menu'];
					else
						$this->render('mainMenu/manage_course', array());
					?>
				</div>
			</li>
		<?php endif;



		// Admin Menu item
		if ($this->godadmin || $this->admin) { ?>
			<li>
				<div class="tile default sidebar-tile-admin" data-bootstro-id="bootstroAdmin" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('menu', 'Admin');?>">
					<a href="#admin">
						<i class="fa fa-cogs fa-lg admin"></i><span></span>
					</a>
				</div>
				<div class="second-menu second-menu-platform-info" id="admin">
					<?php $this->render('mainMenu/admin_menu', array('menu_admin' => $menu_admin, 'menu_apps' => $menu_apps)); ?>
				</div>
			</li>
		<?php }


		// Search Menu item
		if(! Yii::app()->es->disabled ) { ?>
			<li>
				<div id="sidebar-tile-elastic-search-tile" class="tile default sidebar-tile-elastic-search" data-bootstro-id="bootstroElasticSearch" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('standard', '_SEARCH');?>">
					<a href="#elasticSearch">
						<i class="fa fa-search fa-lg"></i>
					</a>
				</div>
				<div class="second-menu second-menu-platform-info" id="globalsearch" style="width: auto; min-width: 455px; /* overflow-y: scroll; */">
					<div id="global-search-wrapper">

						<div class="row-fluid" id="search-input-outer-wrapper">
							<div class="span12">
								<div class="search-input-wrapper span12">
									<?= CHtml::textField('global_search_input', '', array(
													'style'         => '',
													'class'         => "search-query ui-autocomplete-input",
													'placeholder'   => Yii::t('standard', 'Search here'),
													'id'            => "global-search-input",
													'autocomplete'  => "off",
											)
									); ?>
									<button id="global-search-clear" type="button" class="close clear-search">&times;</button>
									<span id="global-search-start" class="perform-search search-icon"></span>
								</div>
							</div>
						</div>

						<div class="row-fluid" id="global-search-suggestions-wrapper">
							<div class="span12">
								<div id="global-search-suggestions-container">
									<ul id="global-search-suggestions">
									</ul>
								</div>
								<div id="recent-global-searches">
									<div class="recent"><?= Yii::t('standard', 'Recent Searches'); ?></div>
									<ul id="recent-global-searches-container">
									</ul>
								</div>
							</div>
						</div>

						<div class="row-fluid"  id="global-search-results-container">
							<div class="span12">
								<?php $this->widget('common.widgets.TabbedContent', array('content' => $globalSearchTabs,
												'containerClass'    => 'tabbed-global-search-results',
												'tabsUppercased'    => false,
										)
								); ?>
							</div>
						</div>

					</div>
				</div>
			</li>
		<?php }

		if ($this->godadmin && $this->is_saas && BrandingWhiteLabelForm::isMenuVisible() && ($this->enable_user_billing || isset(Yii::app()->request->cookies['reseller_cookie'])) && !$this->demo_platform) : ?>
			<li class="buynow-tile-li">
				<div class="tile default sidebar-tile-buynow tile-green" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('userlimit', 'Get more users');?>">

					<?php if ($this->subscription['is_trial']) : ?>
					<a href="<?php echo $this->buyNowUrl; ?>">
						<?php else : ?>
						<a class="ajax" href="<?php echo $this->buyMoreUrl; ?>"  data-target="get-more-user">
							<?php endif;?>
								<i class="menu-vertical green"></i>
								<span class="fa-stack fa-1x get-more-users-stack-icons">
									<i class="fa fa-user-plus  fa-stack-2x fa-flip-horizontal"></i>
									<i class="fa fa-usd fa-stack-1x" style="margin-left: 12px; margin-top: -6px;"></i>
								</span>
								<?php /* TODO: fix this if needed !!! */ ?>
						</a>
				</div>

				<?php if (!$this->subscription['is_trial']) : ?>
					<div class="second-menu" id="get-more-user">
						<div class="second-menu-spacer">
							<!-- Load with ajax -->
							<p><i class="menu-loading"></i> <?php echo Yii::t('standard', '_LOADING');?></p>
						</div>
					</div>
				<?php endif;?>
			</li>
		<?php endif;
		if ($this->godadmin && BrandingWhiteLabelForm::isMenuVisible()) : ?>
			<li class="apps-tile-li">
				<div class="tile default sidebar-tile-apps" data-bootstro-id="bootstroApps" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('apps', 'Paid apps');?>">
					<a href="<?= $this->appsUrl ?>">
						<i class="menu-vertical orange"></i><i class="fa fa-th-large fa-lg"></i><span></span>
					</a>
				</div>
			</li>
		<?php endif; ?>
		<?php
		// Trigger event to add custom items
		Yii::app()->event->raise('RenderLmsMenuItem', new DEvent($this, array()));
		?>
		<?php if($this->demo_platform):?>
			<li>
				<div class="tile default" id="demo-platform"></div>
			</li>
		<?php endif; ?>

		<?php
		// Why i moved this, you may wonder? There was some wierd bug on IE10 if this list item is not the last one in the UL. -> https://docebo.atlassian.net/browse/LB-4863
		// Display change language menu item
		if ($this->all && count($menu_languages) > 1) : ?>
			<li class="lang-tile-li">
				<div class="tile default" data-placement="<?= ($isRTL) ? 'left' : 'right'; ?>" data-original-title="<?php echo Yii::t('register', '_CHANGELANG');?>">
					<a href="#language">
						<i class="fa fa-globe fa-lg"></i><span></span>
					</a></div>
				<div class="second-menu" id="language">
					<div class="second-menu-spacer">
						<?php if (Yii::app()->user->isGodAdmin) : ?>
							<p><a href="<?php echo Docebo::createAdminUrl('langManagement/index'); ?>" class="btn-docebo grey full"><?php echo Yii::t('menu', '_LANG'); ?></a></p>
						<?php endif; ?>
						<ul><?php
							foreach ($menu_languages as $item) {
								if ($item['current']) {
									echo '<li><a class="selected" href="' . $item['href'] . '">' . $item['label'] . '</a></li>';
								} else {
									echo '<li><a href="' . $item['href'] . '">' . $item['label'] . '</a></li>';
								}
							} ?>
						</ul>
					</div>
				</div>
			</li>
		<?php endif; ?>

	</ul>
	</div>

<?php $script = "
	//Make MainMenu instance globally available
	var theMainMenu = null;
	$(function(){
		theMainMenu = new MainMenu();

		$('#get-more-user').on('click', '#reactivate-lms', function(){
			var t = $(this);
			t.addClass('disabled');
			$.ajax({
				url: '".Docebo::createLmsUrl('billing/default/reactivateLms')."',
				success: function(data, textStatus, jqXHR){
					t.removeClass('disabled');
					window.location.reload();
				}
			});
		});

		var page_height = $(window).height();
		var info_height = $('#platform-info').height();
		var new_height = page_height - info_height - parseInt($('#admin-list').css('padding-top')) - parseInt($('#admin-list').css('padding-bottom')) - parseInt($('#platform-info').css('padding-top')) - parseInt($('#platform-info').css('padding-bottom'));
		$('#admin-list').height(new_height);

		// Attach Globalsearch plugin to the globalsearch menu/element
        $('.second-menu#globalsearch').globalsearch({
		      minScore: $this->globalSearchMinScore
        });

	});

try{
    var tweet = document.getElementById('mainMenuUsername');
	if(typeof(tweet) != 'undefined' && !tweet && tweet != null)
	{
		tweet.className = 'mainMenuUsernameHiding';

		var slide_timer,
			slide = function () {
				var max = $('#mainMenuUsername span').width();
				tweet.scrollLeft += 1;
				if (tweet.scrollLeft < max) {
					slide_timer = setTimeout(slide, 10);
				}
			};

		tweet.onmouseover = tweet.onmouseout = function (e) {
			e = e || window.event;
			e = e.type === 'mouseover';
			clearTimeout(slide_timer);
			tweet.className = e ? '' : 'mainMenuUsernameHiding';
			if (e) {
				slide();
			} else {
				tweet.scrollLeft = 0;
			}
		};
	}
}catch(e){
	console.log(e);
}

";

if(! Yii::app()->es->disabled ) {
	$script .= "

	var globalSearchType = 'all'; // agent type
	var globalSearchUiType = 'results'; // UI type - results or suggestions
	var globalSearchPageSize = 20; // found items per page
	var globalSearchFrom = 0; // start search from
	var globalSearchString = ''; // search string
	var globalSearchSelectedTab = '#tab-all-global-search'; // current search tab
	var globalSearchOnScroll = true; // should search on scroll be enabled

	";

	if (isset($globalSearchTabs['ellipsis-global-search']) && !empty($globalSearchTabs['ellipsis-global-search']['ellipsisContent'])) {
		$script .= " var globalEllipsisTabContent = '" . addslashes($globalSearchTabs['ellipsis-global-search']['ellipsisContent']) . "';  ";
	}
}
echo CHtml::script($script);
?>

<script type="text/javascript">
	// On menu click
	$(document).on('click', '.menu.second-gen-menu', function() {
		$('.menu.second-gen-menu ul li div.tile').each(function (index, elem) {
			if ($(elem).hasClass('gray-bg')) {
				$(elem).removeClass('gray-bg');
			}
		});

		if ($('.menu.second-gen-menu ul li div.tile.active').length > 0) {
			// make the tiles gray
			$('.menu.second-gen-menu ul li div.tile:not(.active)').addClass('gray-bg');
		}
	});

	// Hide second menu on showing user profile !!!
	$(document).on('click', '.menu.second-gen-menu a[data-dialog-class="modal-edit-profile"]', function() {
		$('.menu.second-gen-menu .second-menu.open').each(function(index) {
			$(this).removeClass('open').addClass('close');
		});
	});

	// on exiting the menu
	$(document).on('mouseleave', '.menu.second-gen-menu', function() {
		$('.menu.second-gen-menu ul li div.tile').each(function (index, elem) {
			if ($(elem).hasClass('gray-bg')) {
				$(elem).removeClass('gray-bg');
			}
		});
	});

	(function($) {
		<?php if (!$this->subscription['is_trial']) { ?>
		$('.sidebar-tile-apps, .sidebar-tile-helpdesk').tooltip();
		<?php } else { ?>
		$('.sidebar-tile-apps, .sidebar-tile-buynow, .sidebar-tile-helpdesk').tooltip();
		<?php } ?>
	})(jQuery);
</script>
<?php if($isRTL) { ?>

<style type="text/css">
	body {
		overflow: auto!important;
	}

	.header-logo-line .pull-right {
		float: left;
	}

	.menu-square #globalsearch #global-search-input {
		padding-left: 60px;
		padding-right: 7px;
	}

	.menu-square #globalsearch .close.clear-search {
		top: 10px;
		left: 36px;
		right: auto;
	}

	.menu-square #globalsearch .perform-search.search-icon {
		top: 12px;
		left: 10px;
		right: auto;
	}

	#admin.second-menu {
		padding-left: 50px!important;
		padding-right: 0px!important;
	}

	.menu.menu2.second-gen-menu,
	.menu.second-gen-menu {
		right: 0;
		left: initial;
	}

	.menu.new-menu .second-menu {
		left: auto!important;
		right: 50px!important;
	}

	.tooltip.left .tooltip-arrow {
		border-left: 5px solid #000000!important;
		border-right: 0!important;
		top: 50%!important;
	}

	.menu.menu2 ul.menu-square .tile a, .menu ul.menu-square .tile a {
		text-align: right;
	}

	#admin.second-menu .menu-section .content .slot .body {
		margin-right: 60px;
		margin-left: 0px;
	}

	#admin.second-menu #close-bar {
		text-align: left;
	}

	#admin.second-menu .menu-section .header .subtitle {
		text-align: left;
	}
	.menu .second-menu.open { 
		z-index: 9999;
	}
	.menu .profile-info-container {
		top: 20px;
		right: 15px;
	}

	.menu #new-my-menu-top-profile .change-profile-avatar {
		float: right;
	}

	.menu .profile-details-container {
		padding-top: 5px;
		margin-right: 70px;
	}

	.menu #new-my-menu-top-profile #new-menu-profile-ask-guru {
		left:  15px;
		right: auto;
		bottom: auto;
	}

	.menu #new-my-menu-top-profile #new-menu-profile-share {
		left: 95px;
		right: auto;
		bottom: auto;
	}

	.menu #new-my-menu-top-profile span.ask-expert-7020 i.fa-comment-o {
		top: 10px;
		right: 14px;
	}

	.menu #new-my-menu-top-profile span.ask-expert-7020 i.fa-user {
		top: 14px;
		right: 18px;
	}

	.menu #new-my-menu-top-profile i.fa-cloud-upload {
		font-size: 28px!important;
		top: 20px!important;
		right: 15px!important;
	}

	#admin.second-menu .menu-section .content .slot .body, #home.second-menu .menu-section .content .slot .body {
		padding-top: 3px;
		margin-right: 60px;
	}

	#maincontent .callouts-wrapper .message:before {
		right: -30px!important;
	}

	#maincontent .callouts-wrapper .message:after {
		right: -28px!important;
	}

	#maincontent .callouts-wrapper .message {
		box-shadow: none;
	}

	#maincontent .callouts-wrapper {
		left: auto!important;
		right: 65px!important;
		box-shadow: initial;
		box-shadow: -3px 3px 6px 0 #888!important;
	}

</style>
<?php } ?>