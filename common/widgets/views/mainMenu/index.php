<?php
/**
 * @var $this MainMenu
 * @var $menu_home array
 * @var $coachPluginActive boolean
 * @var $askExpertActive boolean
 */
?><div class="menu<?php echo ($animate ? ' animate' : ''); ?> <?= $coachPluginActive ? "new-menu" : "" ?>">
	<ul class="menu-square" id="menu">

		<?php
		if ($coachPluginActive && $askExpertActive):
			?>
			<li>
				<div class="tile default sidebar-tile-ask-expert tile-green" data-bootstro-id="bootstroAskExpert">
					<a href="<?= Docebo::createApp7020Url('askGuru/index', array('showQuestionPanel' => 1)) ?>" class="coachapp" title="<?php echo Yii::t('app7020', 'Ask Expert'); ?>">
						<span class="fa-stack fa-1x ask-expert-7020">
							<i class="fa fa-comment-o fa-stack-2x"></i>
							<i class="fa fa-user fa-stack-1x"></i>
						</span>
					</a>
				</div>
			</li>

			<?php
		endif;
		?>

		<?php if (!Yii::app()->es->disabled) { ?>
			<li>
				<div class="tile default sidebar-tile-elastic-search" data-bootstro-id="bootstroElasticSearch">
					<a href="#elasticSearch" title="<?php echo Yii::t('standard', '_SEARCH'); ?>">
						<i class="fa fa-search fa-2x"></i>
					</a>
				</div>
				<div class="second-menu second-menu-platform-info" id="globalsearch" style="width: auto; min-width: 455px; /* overflow-y: scroll; */ overflow-y: hidden;">
					<div id="global-search-wrapper">

						<div class="row-fluid">
							<div class="span12">
								<div class="search-input-wrapper span12">
									<?=
									CHtml::textField('global_search_input', '', array(
										'style' => '',
										'class' => "search-query ui-autocomplete-input",
										'placeholder' => "Search",
										'id' => "global-search-input",
										'autocomplete' => "off",
										)
									);
									?>
									<button id="global-search-clear" type="button" class="close clear-search">&times;</button>
									<span id="global-search-start" class="perform-search search-icon"></span>
								</div>

							</div>
						</div>

						<div id="global-search-suggestions-wrapper" style="display: none">
							<div id="global-search-suggestions-container">
								<ul id="global-search-suggestions">
								</ul>
							</div>
							<div id="recent-global-searches">
								<div class="recent"><?= Yii::t('standard', 'Recent Search'); ?></div>
								<ul id="recent-global-searches-container">
								</ul>
							</div>
						</div>

						<div id="global-search-results-container" style="display: none">
							<?php
							$this->widget('common.widgets.TabbedContent', array('content' => $globalSearchTabs,
								'containerClass' => 'tabbed-global-search-results',
								'tabsUppercased' => false,
								)
							);
							?>

						</div>
					</div>
				</div>
			</li>
<?php } ?>

				<?php if ($this->all) : ?>
			<li><div class="tile default" data-bootstro-id="bootstroMainMenu"><a href="#home" title="<?php echo Yii::t('adminrules', '_ADMIN_MENU'); ?>">
						<i class="<?= $coachPluginActive ? "fa fa-graduation-cap fa-2x" : "menu-ico main" ?>"></i><span><?php echo $coachPluginActive ? "" : Yii::t('adminrules', '_ADMIN_MENU'); ?></span></a></div>
				<div class="second-menu" id="home">
	<?php if ($coachPluginActive): ?>
						<h4><?= Yii::t('standard', 'Learn') ?></h4>
						<hr>
							<?php endif; ?>
					<div class="second-menu-spacer">
						<ul><?php
							foreach ($menu_home as $key => $elem) {
								$checkClasses = stripos($elem['icon'], 'fa ');
								echo '<li><a href="' . $elem['href'] . '">'
								. '<i class="' . ($checkClasses === false ? 'home-ico ' : '') . $elem['icon'] . '"></i> <span>' . $elem['label'] . '</span>'
								. '</a></li>';
							}
							?></ul>
						<br/>
						<h1 class="user-profile">
								<?php //if (Settings::get('profile_only_pwd', 'on') == 'on') :  ?>
								<?php if (false) : ?>
								<?php echo Yii::app()->user->getAvatar(); ?>
	<?php else: ?>
								<a class="open-dialog" data-dialog-class="modal-change-avatar" href="<?= Docebo::createAppUrl('lms:user/changeAvatar') ?>" title="<?php echo Yii::t('menu_over', '_CHANGE_AVATAR'); ?>"><?php echo Yii::app()->user->getAvatar('pull-left'); ?></a>
	<?php endif; ?>
							<div id="mainMenuUsername">
								<span><?php echo Yii::app()->user->getDisplayName(); ?></span>
							</div>

							<a href="<?= Yii::app()->createAbsoluteUrl('site/logout') ?>" title="<?php echo Yii::t('standard', '_LOGOUT'); ?>" class="user-profile-sign-out">
							<?php echo Yii::t('standard', '_LOGOUT'); ?>
							</a>
						</h1>
						<p class="user-profile-cmd">
							   <?php if (Settings::get('profile_only_pwd', 'on') == 'on') : ?>
								<a href="<?= Docebo::createAppUrl('lms:user/editProfile') ?>" class="btn-docebo grey full open-dialog" data-dialog-class="modal-edit-profile" title="<?php echo Yii::t('profile', '_CHANGEPASSWORD'); ?>"><?php echo Yii::t('profile', '_CHANGEPASSWORD'); ?></a>
							   <?php else: ?>
								<a href="<?= Docebo::createAppUrl('lms:user/editProfile') ?>" class="btn-docebo grey full open-dialog" data-dialog-class="modal-edit-profile" title="<?php echo Yii::t('standard', '_VIEW_PROFILE'); ?>"><?php echo Yii::t('standard', '_VIEW_PROFILE'); ?></a>
	<?php endif; ?>
						</p>

						<?php
						// Raise event to let plugins add extra content
						Yii::app()->event->raise('MainMenuAfterMenuRender', new DEvent($this, array()));
						?>

					</div>
				</div>
				<!-- /home -->
			</li>
		<?php
		endif;
		// Let custom plugins add their own custom course types
		$customCourseDescriptors = array();
		$isCourseWithSessions = (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM || Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR);
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
		if (isset($customCourseDescriptors[Yii::app()->player->course->course_type]))
			$isCourseWithSessions = $isCourseWithSessions || $customCourseDescriptors[Yii::app()->player->course->course_type]['session_based'];

		if ($this->is_in_player && !$isCourseWithSessions) :
			?>
			<?php
			$showCounter = true;
			if (Yii::app()->user->getIsPu() && !Yii::app()->user->checkAccess('/lms/admin/course/view')) {
				$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idUser' => Yii::app()->user->id,
					'idCourse' => Yii::app()->player->course->idCourse
				));
				if (empty($enrollment)) {
					$showCounter = false; //otherwise in this specific case an "Access denied" error will be raised
				}
			}
			if ($showCounter):
			?>
		<li>
			<div class="tile blue" data-bootstro-id="bootstroCourseStructure">
                <?php if ($this->is_in_training_res) { ?>
                	<a href="#" id="player-lonav-button">
                <?php } else { ?>
                	<a href="<?php echo $this->viewCourseUrl ?>">
                <?php } ?>

               	<i class="menu-ico play"></i>
				<?php if(!$coachPluginActive): ?>
               	<span class="player" id="lo-stats" data-href="<?= $this->courseLoStatsHtmlUrl ?>">
					<strong id="menu-play-lo-stats-completed">0</strong> / <span id="menu-play-lo-stats-total">0</span>
				</span>
				<?php endif; ?>
                </a>
            </div>

		<?php if ($this->is_in_training_res) : ?>
						<div id="player-lonav" class="second-menu">
							<div class="second-menu-spacer">
								<div class="player-lonav-content">
									<h2><?= Yii::app()->player->course->name ?></h2>

									<div class="player-lonav-tree"></div>
								</div>
							</div>
						</div>
				<?php endif; ?>

		        </li>
	<?php endif; ?>
<?php endif;
if ($this->is_in_player && ($this->userCanAdminPlayerCourse || ($this->admin && $this->admin_has_course))) :
	?>
			<!-- <li><div class="tile blue" data-bootstro-id="bootstroManageCourse"> -->
			<li><div class="tile blue">
					<a href="#" title="<?php echo Yii::t('standard', 'Manage Course'); ?>"><i class="menu-ico manage"></i><span><?php echo $coachPluginActive ? "" : Yii::t('standard', 'Manage Course'); ?></span></a></div>
				<div class="second-menu" id="manage">
					<?php
					// Let custom plugins add their own custom course types
					$customCourseDescriptors = array();
					Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
					if (isset($customCourseDescriptors[Yii::app()->player->course->course_type]))
						echo $customCourseDescriptors[Yii::app()->player->course->course_type]['manage_course_menu'];
					else
						$this->render('mainMenu/manage_course', array());
					?>
				</div>
			</li>
<?php
endif;
if ($coachPluginActive):
	?>
			<li>
				<div class="tile default sidebar-tile-coach" data-bootstro-id="bootstroCoach">
					<a href="#coach" title="<?php echo Yii::t('app7020', 'Coach'); ?>">
						<i class="fa fa-comment-o fa-2x"></i>
					</a>
				</div>
				<div class="second-menu second-menu-platform-info" id="coach">
					<h4 class="coachheader"><?= Yii::t('app7020', 'Coach') ?></h4>
					<hr>
					<ul>
						<li>
							<a href="<?= Docebo::createApp7020Url('askGuru/index', array('showQuestionPanel' => 1)) ?>" title="<?= Yii::t('app7020', 'Ask The Expert!') ?>"><i class="fa fa-comment-o fa-lg"></i> <?= Yii::t('app7020', 'Ask The Expert!') ?></a>
						</li>
						<li>
							<a href="<?= Docebo::createApp7020Url('askGuru/index') ?>" title="<?= Yii::t('app7020', 'Questions & Answers') ?>"><i class="fa fa-comments-o fa-lg"></i><?= Yii::t('app7020', 'Questions & Answers') ?></a>
						</li>
						<?php if (App7020Experts::isUserExpert(Yii::App()->user->idst) ||
							CoreUser::isUserGodadmin(Yii::App()->user->idst)) {
							?>
							<li>
								<a href="<?= Docebo::createApp7020Url('guruDashboard/index') ?>" title="<?= Yii::t('app7020', 'My Expert Tasks') ?>"><i class="fa fa-wrench fa-lg"></i><?= Yii::t('app7020', 'My Expert Tasks') ?></a>
							</li>
	<?php } ?>
					</ul>
				</div>

			</li>
	<?php if (PluginManager::isPluginActive('Share7020App')): ?>
				<li>
					<div class="tile default sidebar-tile-coach" data-bootstro-id="bootstroShare">
						<a href="#share" title="<?php echo Yii::t('app7020', 'Share'); ?>">
							<i class="fa fa-share-alt fa-2x"></i>
						</a>
					</div>
					<div class="second-menu second-menu-platform-info" id="share">
						<h4 class="shareheader"><?= Yii::t('app7020', 'Share') ?></h4>
						<hr>
						<ul>
							<li>
								<a href="<?= Docebo::createApp7020Url('assets/index') ?>" title="<?= Yii::t('app7020', 'Contribute!') ?>"><i class="fa fa-cloud-upload fa-lg"></i> <?= Yii::t('app7020', 'Contribute!') ?></a>
							</li>
							<li>
								<a href="<?= Docebo::createApp7020Url('knowledgeLibrary/index') ?>" title="<?= Yii::t('app7020', 'Knowledge library') ?>"><i class="fa fa-th-list fa-lg"></i><?= Yii::t('app7020', 'Knowledge library') ?></a>
							</li>
						</ul>

					</div>
				</li>
		<?php
	endif;
endif;
if ($this->godadmin || $this->admin) :
	?>
			<li><div class="tile default sidebar-tile-admin" data-bootstro-id="bootstroAdmin">
					<a href="#admin" title="<?php echo Yii::t('menu', 'Admin'); ?>"><i class="<?= $coachPluginActive ? "fa fa-cogs fa-2x" : "menu-ico" ?> admin"></i><span><?php echo $coachPluginActive ? "" : Yii::t('admin_directory', '_DIRECTORY_GROUPTYPE_PRIVATE_ALT'); ?></span></a></div>
				<div class="second-menu second-menu-platform-info" id="admin">

					<? if(SandboxCreatorHelper::shouldShowSandboxUI()): ?>

						<? if(SandboxCreatorHelper::isCreationAllowedByERP() && (!SandboxCreatorHelper::isCreated() && !SandboxCreatorHelper::isInProgress())) :
							// Show "Create sandbox" button ?>
							<div id="sandbox-creator" class="sandbox-creator old-menu <?=SandboxCreatorHelper::getSandboxActivationStatus()?>">
								<div class="create-description">
									<?=Yii::t('sandbox', 'Want to test drive all the new features 3 weeks before they get released? Activate now your free sandbox.')?>
								</div>
								<p>
									<a data-dialog-class="create-sandbox-dialog" href="<?=Docebo::createAdminUrl('sandbox/create')?>" class="open-dialog btn-docebo green full text-center"><?=Yii::t('sandbox', 'Activate sandbox')?></a>
								</p>
							</div>
						<script>
							$(function(){
								$(document).on('dialog2.closed', '.create-sandbox-dialog', function(){
									if($(this).find('.reload-after-close').length > 0){
										window.location.reload();
									}
								});
							});
						</script>
						<? elseif(SandboxCreatorHelper::isInProgress()) :
							// Show "Sandbox activating" button ?>
							<div id="sandbox-creator" class="sandbox-creator old-menu <?=SandboxCreatorHelper::getSandboxActivationStatus()?>">
								<div>
									<a href="#" class="btn-docebo green full text-center disabled"><?=Yii::t('sandbox', 'Activating your sandbox...')?></a>
								</div>
							</div>
						<? elseif(SandboxCreatorHelper::isCreated()) :
							// Show "Go to sandbox" button ?>
							<div id="sandbox-creator" class="sandbox-creator old-menu <?=SandboxCreatorHelper::getSandboxActivationStatus()?>">
								<div>
									<a target="_blank" href="<?=SandboxCreatorHelper::getSandboxUrl()?>" class="btn-docebo green full text-center"><?=Yii::t('sandbox', 'Go to sandbox')?></a>
								</div>
							</div>
						<? endif; ?>
				<? endif; ?>

					<div class="second-menu-spacer second-menu-spacer-platform-info" id="admin-list">
						<?php
						$i = 0;

						foreach ($menu_admin as $key => $elem) {
							if (isset($elem['items'])) {
								echo '<h2>';
								if ($elem['fa-icon'] && !$elem['icon']) {
									if (is_array($elem['fa-icon'])) {
										echo '<span class="fa-stack">';
										foreach ($elem['fa-icon'] as $icon)
											echo '<i class="fa ' . $icon['icon'] . ' ' . $icon['stacking'] . '"></i>';
										echo '</span>';
									} else {
										echo '<i class="fa ' . $elem['fa-icon'] . '"></i>';
									}
								} else
									echo '<i class="admin-ico ' . $elem['icon'] . '"></i>';

								echo ' <span>' . $elem['label'] . '</span>';

								// Add Version info next to the FIRST Admin menu item
								echo ($i == 0 ? '<div class="pull-right version-number">' . Yii::t('standard', '_VERSION') . ' ' . Settings::get('core_version', '6.4') . '</div>' : '')
								. '</h2>'
								. '<ul>';
								foreach ($elem['items'] as $item) {

									if ($this->godadmin || Yii::app()->user->checkAccess($item['permission'])) {
										echo '<li><a href="' . $item['href'] . '">' . $item['label'] . '</a></li>';
									}
								}
								echo '</ul>';
							}
							$i++;
						}
						?>
							<?php if ($this->godadmin || $this->admin): ?>
							<!-- Apps -->
							<h2><i class="admin-ico apps"></i> <?php echo Yii::t('apps', 'Paid apps'); ?></h2>


							<?php if (empty($menu_apps) && ($this->enable_user_billing || isset(Yii::app()->request->cookies["reseller_cookie"]))) : ?>
								<div id="popup_bootstroMenuNoApps" class="bootstro popover fade right in">
									<div class="popover-content"></div>
								<?php $this->widget('OverlayHints', array('hints' => 'bootstroMenuNoApps')); ?>
								</div>

							<?php else: ?>

								<?php
								$event = new DEvent($this, array('menu_apps' => $menu_apps));
								Yii::app()->event->raise("AddViaappOption", $event);
								if (!$event->shouldPerformAsDefault()) {
									$menu_apps = $event->return_value;
								}
								foreach ($menu_apps as $key => $app) {
									if ($this->godadmin || (isset($app['main_element']['permission']) && $this->checkMenuPermissions($app['main_element']['permission']))) {

										echo '<div class="admin-apps" id="' . $app['id'] . '">'
										. '<h3><div class="row-fluid">
											<div class="span' . ($app['main_element']['settings'] ? '10' : '12') . '">

												<a href="' . $app['main_element']['link'] . '"><div class="pull-left">
													<i class="' . $app['main_element']['icon'] . '"></i>
												</div>' . $app['main_element']['label'] . '</a>
											</div>';
										if ($app['main_element']['settings'] && $this->godadmin) {
											echo '<div class="span2 text-center">
												<a class="settings p-hover" href="' . $app['main_element']['settings'] . '"><i class="i-sprite is-gearpair"></i></a>
											</div>';
										}
										echo '</div></h3>';

										if (!empty($app['items'])) {
											echo '<ul>';
											foreach ($app['items'] as $label => $href) {

												echo '<li><a href="' . $href . '">' . $label . '</a></li>';
											}
											echo '</ul>';
										}
										echo '</div>';
									}
								}
								?>

				<?php endif; ?>

					<?php endif; ?>
					</div>
				</div>
			</li>
						<?php endif;
						if ($this->godadmin && $this->is_saas && BrandingWhiteLabelForm::isMenuVisible() && ($this->enable_user_billing || isset(Yii::app()->request->cookies['reseller_cookie'])) && !$this->demo_platform) :
							?>
			<li><div class="tile default sidebar-tile-buynow">

	<?php if ($this->subscription['is_trial']) : ?>
						<a href="<?php echo $this->buyNowUrl; ?>">
	<?php else : ?>
							<a class="ajax" href="<?php echo $this->buyMoreUrl; ?>"  title="<?php echo Yii::t('userlimit', 'Get more users'); ?>" data-target="get-more-user">
							<?php endif; ?>

							<?php if ($coachPluginActive): ?>
								<i class="menu-vertical green"></i>
								<span class="fa-stack fa-1x">
									<i class="fa fa-user-plus  fa-stack-2x fa-flip-horizontal"></i>
									<i class="fa fa-usd fa-stack-1x" style="margin-left: 12px; margin-top: -6px;"></i>
								</span>
								<?php else:
								?>
								<i class="menu-vertical green"></i><i class="menu-ico get_more_user"></i>


								<?php
								if ($this->subscription['is_trial']) {
									echo '<span>' . Yii::t('userlimit', 'Buy now') . '</span>';
								} else {
									echo ''
									// Now that we have the counter in the upper right part, we are hiding this one
									/*
									  .'<span class="active-users-counter">'
									  .'<strong>' . $this->subscription['active_users'] . '</strong> / ' . $this->subscription['max_users']
									  . '</span><br/>' */
									. '<span class="active-users">' . Yii::t('userlimit', 'Get more users') . '</span>';
								}
								?>

	<?php endif;
	?>

						</a>
				</div>

			<?php if (!$this->subscription['is_trial']) : ?>
					<div class="second-menu" id="get-more-user">
						<div class="second-menu-spacer">
							<!-- Load with ajax -->
							<p><i class="menu-loading"></i> <?php echo Yii::t('standard', '_LOADING'); ?></p>
						</div>
					</div>
	<?php endif; ?>
			</li>
<?php endif;
if ($this->godadmin && BrandingWhiteLabelForm::isMenuVisible()) :
	?>
			<li><div class="tile default sidebar-tile-apps" data-bootstro-id="bootstroApps">
					<a href="<?= $this->appsUrl ?>" title="<?php echo Yii::t('apps', 'Paid apps'); ?>">
						<i class="menu-vertical orange"></i><i class="<?= $coachPluginActive ? "fa fa-th-large fa-2x" : "menu-ico apps" ?>"></i><span><?php echo $coachPluginActive ? "" : Yii::t('apps', 'Paid apps'); ?></span></a></div></li>
		<?php endif;
		if ($this->godadmin || $this->isHelpdeskButtonVisible()) :
			?>
			<li><div class="tile default sidebar-tile-helpdesk">
					<a class="open-dialog" href="<?= Docebo::createLmsUrl('site/axHelpDesk') ?>" title="<?php echo Yii::t('userlimit', 'Help Desk'); ?>" data-dialog-class="helpdesk-modal" rel="helpdesk-modal">
						<i class="menu-vertical blu"></i><i class="<?= $coachPluginActive ? "fa fa-comments fa-2x" : "menu-ico helpdesk" ?>"></i><span><?php echo $coachPluginActive ? "" : Yii::t('userlimit', 'Help Desk'); ?></span></a></div>
				<!--
				<div class="second-menu" id="help-desk">
						<?php //$this->render('mainMenu/help_desk', array()); ?>
				</div>
				-->
			</li>
						<?php endif;
						if ($this->all && count($menu_languages) > 1) :
							?>
			<li><div class="tile default">
					<a href="#language" title="<?php echo Yii::t('register', '_CHANGELANG'); ?>">
						<i class="<?= $coachPluginActive ? "fa fa-globe fa-2x" : "menu-ico language" ?>"></i><span><?php echo $coachPluginActive ? "" : Yii::t('register', '_CHANGELANG'); ?></span></a></div>
				<div class="second-menu" id="language">
					<div class="second-menu-spacer">
	<?php if (Yii::app()->user->isGodAdmin) : ?>
							<p><a href="<?php echo Docebo::createAdminUrl('langManagement/index'); ?>" class="btn-docebo grey full"><?php echo Yii::t('menu', '_LANG'); ?></a></p>
			<?php endif; ?>
						<ul><?php
			foreach ($menu_languages as $item) {
				if ($item['current']) {
					echo '<li><a class="selected" href="' . $item['href'] . '">' . $item['label'] . '</a></li>';
				} else {
					echo '<li><a href="' . $item['href'] . '">' . $item['label'] . '</a></li>';
				}
			}
			?>
						</ul>
					</div>
				</div>
			</li>
<?php endif; ?>
<?php
// Trigger event to add custom items
Yii::app()->event->raise('RenderLmsMenuItem', new DEvent($this, array()));
?>
<?php if ($this->demo_platform): ?>
			<li>
				<div class="tile default" id="demo-platform"></div>
			</li>
<?php endif; ?>
	</ul>
</div>

<?php
$script = "
	//Make MainMenu instance globally available
	var theMainMenu = null;
	$(function(){
		theMainMenu = new MainMenu();

		$('#get-more-user').on('click', '#reactivate-lms', function(){
			var t = $(this);
			t.addClass('disabled');
			$.ajax({
				url: '" . Docebo::createLmsUrl('billing/default/reactivateLms') . "',
				success: function(data, textStatus, jqXHR){
					t.removeClass('disabled');
					window.location.reload();
				}
			});
		});

		var page_height = $(window).height();
		var info_height = $('#platform-info').height();
		var new_height = page_height - info_height - parseInt($('#admin-list').css('padding-top')) - parseInt($('#admin-list').css('padding-bottom')) - parseInt($('#platform-info').css('padding-top')) - parseInt($('#platform-info').css('padding-bottom'));
		$('#admin-list').height(new_height);

		// Attach Globalsearch plugin to the globalsearch menu/element
        $('.second-menu#globalsearch').globalsearch();

	});

try{
    var tweet = document.getElementById('mainMenuUsername');
	if(typeof(tweet) != 'undefined' && !tweet && tweet != null)
	{
		tweet.className = 'mainMenuUsernameHiding';

		var slide_timer,
			slide = function () {
				var max = $('#mainMenuUsername span').width();
				tweet.scrollLeft += 1;
				if (tweet.scrollLeft < max) {
					slide_timer = setTimeout(slide, 10);
				}
			};

		tweet.onmouseover = tweet.onmouseout = function (e) {
			e = e || window.event;
			e = e.type === 'mouseover';
			clearTimeout(slide_timer);
			tweet.className = e ? '' : 'mainMenuUsernameHiding';
			if (e) {
				slide();
			} else {
				tweet.scrollLeft = 0;
			}
		};
	}
}catch(e){
	console.log(e);
}

";

if (!Yii::app()->es->disabled) {
	$script .= "

	var globalSearchType = 'all'; // agent type
	var globalSearchUiType = 'results'; // UI type - results or suggestions
	var globalSearchPageSize = 20; // found items per page
	var globalSearchFrom = 0; // start search from
	var globalSearchString = ''; // search string
	var globalSearchSelectedTab = '#tab-all-global-search'; // current search tab
	var globalSearchOnScroll = true; // should search on scroll be enabled

	";

	if (isset($globalSearchTabs['ellipsis-global-search']) && !empty($globalSearchTabs['ellipsis-global-search']['ellipsisContent'])) {
		$script .= " var globalEllipsisTabContent = '" . addslashes($globalSearchTabs['ellipsis-global-search']['ellipsisContent']) . "';  ";
	}
}
echo CHtml::script($script);
?>