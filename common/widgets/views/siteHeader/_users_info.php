<? Yii::app()->getClientScript()->registerCss(time(),'
 .tooltip-inner{
        font-size: 13px;
        line-height: 18px;
        display: inline-block;
        text-align: left;
    }
    .tooltip{
        z-index: 1029;
    }
    .tooltip-inner a{
        padding: 5px 10px;
        color: #fff;
        text-shadow: 0 1px 0 #000 !important;
        margin-top: 10px;
        display: table;
    }
'); ?>
<?php

if (BrandingWhiteLabelForm::isMenuVisible()) :

    $expiration = Settings::get('expiring_notification');

    if(Settings::get('expiring_notification_show') == 'no')
        $expiring_notification = false;
    else
        $expiring_notification = true;

	$enable_billing = ($this->enable_user_billing || isset(Yii::app()->request->cookies['reseller_cookie'])) && !$this->demo_platform;

	// Hide the "Buy now" or "Get more users" buttons if the LMS subscription is
	// temporarily terminated (could be reactivated at any moment though)
	if($this->lmsOrderInfo && $this->lmsOrderInfo['payment_status'] == 'user_cancelled_sub')
		$enable_billing = false;

	//$class = $this->inGracePeriod ? 'in-grace' : ($expiration ? "in-expiration" : "");
	$class = $this->inGracePeriod && $enable_billing ? 'in-grace' : ($expiration && $enable_billing ? "in-expiration" : "");
	//$iconClass = ($expiration || $this->inGracePeriod) ? "p-sprite solid-exclam-mini-white" : "";
	$iconClass = (($expiration || $this->inGracePeriod) && $enable_billing) ? "p-sprite solid-exclam-mini-white" : "";

	$helpDeskUrl = Docebo::createLmsUrl('site/axHelpDesk');

    $expire_date = Settings::get('expiration_date');
    if(DateTime::createFromFormat('Y-m-d H:i:s', $expire_date) || DateTime::createFromFormat('Y-m-d', $expire_date)){
        $days = Yii::app()->localtime->countDayDiff(date('Y-m-d'), $expire_date);
    } else{
        $days = 0;
    }
    ?>
	<span class="users-info">

		<span class="users-info-text">
			<?= Yii::t('order','Active users') ?><br>
			<span class="active"><?= $this->activeUsers ?>/</span><span class="bought"><?= $this->boughtUsers ?></span>
		</span>

		<!-- BILLING STUFF -->
        <?php if($this->inGracePeriod && $enable_billing && !isset($_SESSION['credit_card_expiration_remind_on_next_login']) && $expiring_notification && !$this->demo_platform) : ?>

                <?php switch ($this->inGracePeriod) {
                    case '1' : {
						$title = '<span>'.Yii::t('saas', '_GRACE_PERIOD_MODE', array(
								'{url}' => Docebo::createLmsUrl('billing/default/plan'),
								'{class}' => 'btn-docebo green',
								'{style}' => 'margin-left:auto;margin-right:auto;'
							)).'</span>';
						break;
                    }
                    case '2' : {
                        $title = '<span>'.Yii::t('expiration', 'grace_period_wire_transfer_popup').'</span>'.
                        CHtml::link(Yii::t('standard', 'contact us'), '', array('class' => 'btn-docebo blue', 'onclick' => 'contactUs(); return false;'));
                        break;
                    }
                    default: $title = '';
                } ?>
                <a class="t"
                   style="display: inline-block"
                   data-toggle="tooltip"
                   rel="tooltip"
                   data-placement="bottom"
                   data-delay='{"show":"0", "hide":"30000"}'
                   title='<?=$title;?>'
                   data-html="true">

        <?php elseif($expiration && $enable_billing && !isset($_SESSION['credit_card_expiration_remind_on_next_login']) && $expiring_notification):
                switch($expiration) {
                    case 'credit_card' : {
                        $dialogUrl = Docebo::createLmsUrl('site/planExpirationCretditCard');
                        break;
                    }
                    case 'wire_transfer' : {
                        $dialogUrl = Docebo::createLmsUrl('site/planExpirationWireTransfer');
                        break;
                    }
                    default : $dialogUrl = "";
                } ?>
                <a href="<?=  Docebo::createLmsUrl('billing/default/axCancel');?>"
                    class="open-dialog"
                    data-dialog-class="cancel-subscription  users-info-dialogs"
                    closeOnOverlayClick="false"
                    closeOnEscape="false"
                    id="cancelSubscription"
                    style="display: none;"
                    rel="cancel-subscription">
                </a>
                <a href="<?=  Docebo::createLmsUrl('site/renewSubscription');?>"
                    class="open-dialog"
                    data-dialog-class="renew-subscription  users-info-dialogs"
                    closeOnOverlayClick="false"
                    closeOnEscape="false"
                    id="renewSubscription"
                    style="display: none;"
                    rel="renew-subscription">
                </a>
                <a href="<?=$dialogUrl?>"
                    class="open-dialog"
                    data-dialog-class="expiration-modal users-info-dialogs"
                    closeOnOverlayClick="false"
                    closeOnEscape="false"
                    id="triggerDialog"
                    style="display: none;"
                    rel="expiration-modal">
                </a>
                <a class="t"
                   style="display: inline-block;text-transform:uppercase;"
                   data-toggle="tooltip"
                   rel="tooltip"
                   data-placement="bottom"
                   data-delay='{"show":"0", "hide":"30000"}'
                   title="<?=htmlspecialchars('<span>'
                   .Yii::t('expiration', 'Your annual {users} users subscription plan expires in {days} days', array('{users}' => Settings::get('max_users'), '{days}' => $days))
                   .'</span><a class="btn-docebo green" href="'.$dialogUrl.'" onclick="triggerDialog(); return false;">'
                   .Yii::t('expiration', 'take an action').'...</a>', ENT_QUOTES, 'UTF-8');?>"
                    data-html="true">
				</a>
        <?php endif; ?>
        <!-- BILLING STUFF -->


        <span class="users-info-icon <?=$class; ?>">
        	<span class="<?=$iconClass?>"></span>
            <span class="p-sprite people-mini-white"></span>
        </span>


        <!-- BILLING STUFF -->
        <?php if ((Billing::isWireTransfer() || $expiration || $this->inGracePeriod) && $enable_billing):?>
            <a href="<?=  Docebo::createLmsUrl('site/planExpirationContact');?>"
                class="open-dialog"
                data-dialog-class="expiration-contact-modal  users-info-dialogs"
                closeOnOverlayClick="false"
                closeOnEscape="false"
                id="contactDialog"
                style="display: none;"
                rel="expiration-contact-modal">
            </a>
        <?php endif;?>
        <!-- BILLING STUFF -->




		<span class="users-info-links two-lines">
			<a 	class="open-dialog" href="<?= $helpDeskUrl ?>"
				data-dialog-class="helpdesk-modal users-info-dialogs"
				closeOnOverlayClick="false"
				closeOnEscape="false"
				rel="helpdesk-modal">
				<span class="i-sprite is-env-closed"></span><span> <?= Yii::t('standard','Contact us') ?></span><br>
			</a>

			<!-- BILLING STUFF -->
			<?php if ($enable_billing) : ?>
				<?php if (Billing::isWireTransfer()) : ?>
					<a href="javascript:;" onclick="contactUs()">
						<span class="i-sprite is-man-plus green"></span><span class='buy-now'> <?= Yii::t('userlimit','Get more users') ?></span>
					</a>
				<?php else : ?>
					<a href="<?= $this->buyNowUrl ?>">
						<span class="i-sprite is-man-plus green"></span><span class='buy-now'> <?= Yii::t('userlimit','Get more users') ?></span>
					</a>
				<?php endif; ?>
			<?php else: ?>
				<span>&nbsp;</span>
			<?php endif; ?>
			<!-- BILLING STUFF -->



		</span>
	</span>

    <script type="text/javascript">
		//<![CDATA[
        $(window).load(function() {
            if('<?=Yii::app()->request->getParam('login', 0);?>' == '1') {
                $('.t').tooltip('show');
            }
        });
        function triggerDialog() {
            $('.t').tooltip('hide');
            $("#triggerDialog").trigger('click');
        }
        function contactUs() {
            $('.t').tooltip('hide');
            $("#contactDialog").trigger('click');
        }
        $(document).on("dialog2.content-update", ".modal.users-info-dialogs", function () {
            if ($(this).find("a.auto-close").length > 0) {
                $(this).find(".modal-body").dialog2("close");
            }
       });
        $(document).on("dialog2.content-update", ".modal.modal-edit-prerequisites", function () {
            if ($(this).find("a.auto-close").length > 0) {
                $(this).find(".modal-body").dialog2("close");
            }
       });

		function contactUs()
		{
			if ($('#expiration-modal').length)
			{
				$('#expiration-modal').dialog2('close');
			}
			$('#contactDialog').trigger('click');
		}
	    //]]>
    </script>

<?php endif; ?>