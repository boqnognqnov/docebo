<?php
/** @var $this SiteHeader */
?>
<div class="header-logo-line">
	<div class="container">
		<div class="row-fluid">
			<div class="span4">
				<a class="header-logo" href="<?= Yii::app()->getBaseUrl() ?>" data-bootstro-id="bootstroLogo">
					<?php echo CHtml::image($logoImageUrl, Yii::t('login', '_HOMEPAGE')); ?>
				</a>
			</div>
			<?php
			if(!empty($customMessageInHeader))
				echo '<div class="allowOlUlNumbering" style="padding-top: 15px;padding-left: 0">'. $customMessageInHeader.'</div>'; ?>
		</div>
	</div>
</div>
