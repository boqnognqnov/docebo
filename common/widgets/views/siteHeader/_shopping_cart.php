<div class="shopping-cart">
	<span class="cart-green"></span>
	<span><?= Yii::t('catalogue','_SHOPPING_CART') ?><br><strong><?= $this->cartItemsCount ?> <?= Yii::t('standard', 'items') ?> </strong> <a href="<?= $this->cartUrl ?>"><?= Yii::t('standard','_VIEW') ?></a></span>
</div>
