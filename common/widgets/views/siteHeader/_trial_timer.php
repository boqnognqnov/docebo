<?php
	$contactSalesUrl = Docebo::createLmsUrl('site/axContactSalesTrialExpired');
	$twoLinesClass = "";
	if ($this->trialRemainingDays > 0) {
		$twoLinesClass = "two-lines";
	}
	
	$helpDeskUrl = Docebo::createLmsUrl('site/axHelpDesk');
	
?>
	<span class="trial-timer">
		<span class="timer-text <?= $twoLinesClass ?>">
			<?php if ($this->trialRemainingDays > 0) : ?>
				<?= Yii::t('userlimit','Your trial will expire in', array(
					'[days]' => '<br><span class="days">' . $this->trialRemainingDays . '</span> '
				)) ?>
			<?php elseif ($this->trialRemainingDays  == 0) : ?>
				<?= Yii::t('userlimit', 'Your trial will expire today') ?>
			<?php else : ?>
				<?= Yii::t('userlimit', 'Your trial has now expired') ?>
			<?php  endif; ?>
		</span>
		<span class="timer-icon">
			<span class="icon-trial-timer-white"></span>
		</span>
		
		<span class="timer-links two-lines">
			<a 	class="open-dialog" href="<?= $helpDeskUrl ?>" 
				data-dialog-class="helpdesk-modal"
				closeOnOverlayClick="false" 
				closeOnEscape="false"  
				rel="helpdesk-modal">
				<span class="open-dialog i-sprite is-env-closed"></span><span> <?= Yii::t('standard','Contact us') ?></span><br>
			</a>
			<?php if (BrandingWhiteLabelForm::isMenuVisible() && ($this->enable_user_billing || isset(Yii::app()->request->cookies['reseller_cookie']))) : ?>
			<a href="<?= $this->buyNowUrl ?>">
				<span class="i-sprite is-man-plus green"></span><span class='buy-now'> <?= Yii::t('userlimit','Buy now') ?></span>
			</a>
			<?php endif; ?>
		</span>
	</span>

