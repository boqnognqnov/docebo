<?php
/** @var $this SiteHeader */
?>
<?php $this->widget('common.widgets.CalloutsWidget'); ?>

<div class="header-logo-line">
	<div class="row-fluid">
		<div class="span4">
			<a class="header-logo" href="<?php echo Docebo::getUserHomePageUrl(); ?>">
				<?php echo CHtml::image($logoImageUrl, Yii::t('login', '_HOMEPAGE')); ?>
			</a>
		</div>

		<div class="span8 text-right user-menu">

			<div class="pull-right logout-holder">
				<?php $this->widget('common.widgets.CommunicationCenter', array()); ?>
				<? Yii::app()->event->raise('onBeforeLogoutLink', new DEvent($this, array())); ?>
				<?php if ($this->show_logout) : ?>
					<span class="logout text-right">
					<a href="<?= Docebo::createLmsUrl('site/logout') ?>" title="<?php echo Yii::t('standard', '_LOGOUT'); ?>">
						<i class="logout-black"></i> <?php echo Yii::t('standard', '_LOGOUT'); ?>
					</a>
				</span>
				<?php endif; ?>
			</div>

			<?php if($this->demo_platform):?>
				<span class="text-left" style="margin-right: 10px">
					<?php echo CHtml::image(Yii::app()->theme->getBaseUrl().'/images/menu/partners_demo_top.jpg', '', array('style' => 'width: 289px; height: 52px')); ?>
				</span>
			<?php else : ?>
				<?php
				if ( $this->cartItemsCount > 0) {
					$this->render('common.widgets.views.siteHeader._shopping_cart');
				}
				else if ($this->showTrialTimer && Yii::app()->controller->id != 'setup') {
					// DEBUG:
					// $this->trialRemainingDays = 30;
					$this->render('common.widgets.views.siteHeader._trial_timer');
				}
				else if ($this->showUsersInfo && Yii::app()->controller->id != 'setup') {
					$this->render('common.widgets.views.siteHeader._users_info');
				}
				?>
			<?php endif?>

		</div>

		<div class="clearfix"></div>
		<?php
		if(!empty($customMessageInHeader))
			echo '<div class="allowOlUlNumbering" style="padding-top: 15px;padding-left: 0">'. $customMessageInHeader.'</div>'; ?>
	</div>
</div>