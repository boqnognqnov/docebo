<?php
/** @var $this SiteHeader */
?>
<div class="header-logo-line">
	<div class="container">
		<div class="row-fluid">
			<div class="span4">
				<a class="header-logo" href="<?= Yii::app()->getBaseUrl() ?>">
					<?php echo CHtml::image($logoImageUrl, Yii::t('login', '_HOMEPAGE')); ?>
				</a>
			</div>
			<div class="span8">
				<?php if (Settings::get('register_type', 'admin') != 'admin') : ?>
					<?php if (Yii::app()->event->raise('BeforeSelfRegistrationLinkRender', new DEvent($this, array()))) : ?>
				<div class="header-newuser pull-right">
					<h4><?php echo Yii::t('register', 'New user?'); ?></h4>
					<p><?php echo Yii::t('user', 'If you don\'t have an account, please'); ?>
						<a class="open-dialog" data-dialog-class="register" id="register-modal-btn" rel="dialog-register" href="<?=Yii::app()->createUrl('user/axRegister')?>"><?=Yii::t('user','Register here');?></a></p>
				</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>

		</div>
		<?php
		if(!empty($customMessageInHeader))
			echo '<div class="allowOlUlNumbering" style="padding-top: 15px;padding-left: 0">'. $customMessageInHeader.'</div>'; ?>
	</div>
	<div class="clearfix"></div>
</div>
<div id="header" class="anonymous">
	<div class="container">
		<div class="row-fluid">
			<div class="span11">
				<div class="header-login">
					<?php echo CHtml::beginForm('?r=site/login', 'post', array(
						'id' => 'login_confirm',
						'class' => 'header-login-form',
					)) ?>

						<?php if (Yii::app()->event->raise('BeforeLoginFormRender', new DEvent($this, array()))) : ?>

						<h4><?php echo Yii::t('login', '_LOGIN'); ?></h4>
						<div class="pull-left">
							<?php echo CHtml::hiddenField("client_timezone_offset");?>
							<?php echo CHtml::textField('login_userid', (isset(Yii::app()->session['login_userid']) ? Yii::app()->session['login_userid'] : ''), array(
								'placeholder' => Yii::t('standard', '_USERNAME'),
							));?>

							<?php echo CHtml::passwordField('login_pwd', '', array(
								'placeholder' => Yii::t('standard', '_PASSWORD'),
							));?>
							<?php echo CHtml::submitButton(Yii::t('login', '_LOGIN'), array(
								'class' => 'btn btn-docebo green big',
							));?>

							<? Yii::app()->event->raise('ExternalCatalogAfterLoginForm', new DEvent($this, array())); ?>
						</div>

						<?php Social::displaySocialLoginIcons(); ?>
						<?php if(PluginManager::isPluginActive('GoogleappsApp')): ?>
							<div class="pull-left">
									<a title="Google Apps" class="header-lostpass" href="<?=Docebo::createLmsUrl('GoogleappsApp/GoogleappsApp/gappsLogin')?>">
										<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/googleapps-24.png") ?>
									</a>&nbsp;&nbsp;&nbsp;
							</div>
						<?php endif; ?>
						<?php if(PluginManager::isPluginActive('GooglessoApp') && Docebo::isCurrentLmsDomain()): ?>
							<div class="pull-left">
									<a title="Google Login" class="header-lostpass" href="<?=Docebo::createLmsUrl('GooglessoApp/GooglessoApp/login')?>">
										<?/*=Yii::t('login','_LOGIN_WITH') */?><!-- &nbsp;	--><?= CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/google-24.png") ?>
									</a>&nbsp;&nbsp;&nbsp;
							</div>
						<?php endif; ?>

						<?php
							//let plugins insert their own custom login buttons in the login page header
							$_plugins = PluginManager::getActivePlugins();
							$_excluded = array('GoogleappsApp', 'GooglessoApp');
							if (!empty($_plugins) && is_array($_plugins)) {
								foreach ($_plugins as $_plugin) {
									if ($_plugin instanceof CorePlugin && !in_array($_plugin->plugin_name, $_excluded)) {
										$_module = Yii::app()->getModule($_plugin->plugin_name);
										if (method_exists($_module, 'customLoginButton')) {
											$_customLoginButton = $_module->customLoginButton();
											if (!empty($_customLoginButton)) {
												echo '<div class="pull-left headerLoginCustomApp" id="headerLogin'.$_plugin->plugin_name.'">';
												echo $_customLoginButton;
												echo '</div>';
											}
										}
									}
								}
							}
						?>

						<div class="pull-left">
							<a class="header-lostpass open-dialog" rel="dialog-lostdata" data-dialog-class="lostdata" href="<?=Yii::app()->createUrl('user/axLostdata');?>"><?php echo Yii::t('login', '_LOG_LOSTPWD'); ?></a>
						</div>

						<?php endif; ?>

					<?= CHtml::endForm(); ?>
				</div>
			</div>
			<div class="span1 text-right">
				<div class="header-language inline-block">
					<span class="btn-group">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<?= CHtml::image(Yii::app()->theme->baseUrl . "/images/menu/icon-world.png") ?>
						<?= CHtml::image(Yii::app()->theme->baseUrl . "/images/caret_white.png") ?>
						</a>
						<ul class="dropdown-menu text-left pull-right">
							<?php echo $this->getLanguagesLiHtml(); ?>
						</ul>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<!--[if IE]>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#login_userid").blur();

			// Default placeholders
			var uname = "<?=Yii::t('standard', '_USERNAME')?>";
			var pwd = "<?=Yii::t('standard', '_PASSWORD')?>";
			$("#login_userid").val(uname).focus(function(){
				if($(this).val()==uname){
					$(this).val("");
				}
			}).blur(function(){
				if($(this).val()==""){
					$(this).val(uname);
				}
			});

			$("#login_pwd").val(pwd).focus(function(){
				if($(this).val()==pwd){
					$(this).val("");
				}
			}).blur(function(){
				if($(this).val()==""){
					$(this).val(pwd);
				}
			});
		});
	</script>
<![endif]-->

<!--[if !IE]-->
    <script type="text/javascript">
	   $(document).ready(function(){
		  // Non-IE browsers
		  $("#login_userid").focus();
	   });
    </script>
<!--[endif]-->

<script type="text/javascript">
	$(document).ready(function(){
		var d = new Date();
		$("#client_timezone_offset").val(-d.getTimezoneOffset()*60);
		$("a[href*='site/sso']").each(function()
		{
			var $this = $(this);
			var _href = $this.attr("href");
			$this.attr("href", _href + '&offset='+(-d.getTimezoneOffset()*60));
		});
	});

	$(function(){
		if(<?= json_encode($triggerRegistration) ?>){
			$(document).controls();
			$("#register-modal-btn").click();
		}
	});
</script>