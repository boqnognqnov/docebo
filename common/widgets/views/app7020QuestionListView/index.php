<?php
$spanSizeType = 'span' . (12 / $this->gridColumns);
$mergedExtensions = array();
$current = explode(',', $this->extensions);
$available = explode(',', $this->_extensions['available']);
$checkedExtensions = array_intersect($available, $current);
$mergedExtensions = array_merge($checkedExtensions, explode(',', $this->_extensions['default']));

$defaultViewData = array(
	'spanSizeType' => $spanSizeType,
	'extensions' => $mergedExtensions
);
$viewData = ($this->listParams['viewData']) ? array_merge($this->listParams['viewData'], $defaultViewData) : $defaultViewData;

$this->listParams['viewData'] = $viewData;
$this->listParams['listItemView'] = 'common.widgets.views.app7020QuestionListView.itemView';
$this->listParams['noItemViewContainer'] = $this->noItemViewContainer;
$this->listParams['disableMassSelection'] = $this->disableMassSelection;
$this->listParams['disableMassActions'] = $this->disableMassActions;
$this->listParams['disableFiltersWrapper'] = $this->disableFiltersWrapper;
$this->listParams['disableCheckBox'] = $this->disableCheckBox;
$this->listParams['beforeAjaxUpdate'] = $this->listParams['beforeAjaxUpdate'] . $this->_beforeUpdate;
$this->listParams['afterAjaxUpdate'] = $this->listParams['afterAjaxUpdate'] . $this->_afterUpdate; 
//$this->listParams['noYiiListViewJS'] = true;

if (in_array("openClosedSwitch", $mergedExtensions) && $this->listParams['dataProvider']->totalItemCount) {
	$radioSwitchesOpenClosed = array(
		array(1, Yii::t('app7020', 'Open')),
		array(0, Yii::t('app7020', 'Closed')),
	);
	$this->widget('common.widgets.app7020RadioSwitches', array(
		'switches' => $radioSwitchesOpenClosed,
		'comboListViewId' => $this->listParams['listId']
			)
	);
}
?>

<div class="app7020QuestionListView" data-app7020-gridColumns="<?php echo $this->gridColumns; ?>" data-prevent-pageBlank="<?php echo ($this->blankTemplates === false) ? 'true' : 'false'; ?>">
	<?php $this->widget('common.widgets.ComboListView', $this->listParams); ?>
	<?php if($this->blankTemplates['page'] !== false): ?>
	<div class="app7020QuestionListView_pageBlank <?php echo ($this->blankTemplates !== false && $this->listParams['dataProvider']->totalItemCount == 0) ? 'show' : 'hide' ?>">
		<?php $this->controller->renderPartial(($this->blankTemplates['page']) ? $this->blankTemplates['page'] : $this->_blankTemplates['page']); ?>
	</div>
	<?php endif; ?>
	<?php if($this->blankTemplates['search'] !== false): ?>
	<div class="app7020QuestionListView_searchBlank hide">
		<?php $this->controller->renderPartial(($this->blankTemplates['search']) ? $this->blankTemplates['search'] : $this->_blankTemplates['search']); ?>
	</div>
	<?php endif; ?>
</div>