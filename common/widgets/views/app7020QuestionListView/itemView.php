<?php
$isLinkedVideo = false;
$idContent = false;
$contentTitle = "";
if ($data['idContent']) {
	$content = App7020Content::model()->findByPk($data['idContent']);
	if ($content->contentType == App7020Content::CONTENT_TYPE_VIDEO) {
		$isLinkedVideo = true;
	}
	$idContent = $content->id;
	$contentTitle = $content->title;
} else {
	$request = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $data['id']));
	if ($request->id) {
		$content = App7020Content::model()->findByPk($request->idContent);
		if ($content->conversion_status == App7020Content::CONVERSION_STATUS_APPROVED) {
			if ($content->contentType == App7020Content::CONTENT_TYPE_VIDEO) {
				$isLinkedVideo = true;
			}
			$idContent = $content->id;
			$contentTitle = $content->title;
		}
	}
}

$linkUrl = Docebo::createApp7020Url("askTheExpert/index", array("#" => "/question/".$data['id']));
$lastAnswerStriped = strip_tags($data['aContent']);
$isFollow = App7020QuestionFollow::model()->findByAttributes(array('idQuestion' => $data['id'], 'idUser' => Yii::app()->user->idst));
$newAnswersCount = count(App7020QuestionHistory::getNewAnswers($data['id']));
if(Yii::app()->user->isGodAdmin){
	$controlParams = array(
		'arrayData' => array(
			array('url' => 'javascript:;', 'text' => ($isFollow) ? 'Unfollow' : 'Follow', 'method' => 'follow'),
			array('url' => 'javascript:;', 'text' => 'Delete', 'method' => 'question_delete'),
		));
} else {
	$controlParams = array(
		'arrayData' => array(
			array('url' => 'javascript:;', 'text' => ($isFollow) ? 'Unfollow' : 'Follow', 'method' => 'follow'),
		));
}
?>
<div class="itemGrid <?php echo $spanSizeType; ?>" data-idObject="<?php echo $data['id'] ?>" data-quickViewSize="<?php if ($isLinkedVideo) echo 'largeBox'; ?>" data-follow="<?php echo ($isFollow) ? 'true' : 'false'; ?>" data-newAnswers="<?php echo ($newAnswersCount > 0) ? 'true' : 'false'; ?>">

	<!--GOT IT INFORMATION LABEL-->
	<?php if (in_array('gotIt', $extensions) && $index == 0 && !App7020Helpers::actionGetGotItByAttributes($listGotItType)): ?>
		<div class="app7020GotItInformation">
			<span>
				<?php
				echo Yii::t('app7020', 'These are your ignored questions with requests. You won\'t be notified for any change that happens. You can re-enable notifications or, if no action is taken, they will be automatically removed from here as soon as the request is satisfied');
				?>
			</span>
			<?php echo CHtml::link(Yii::t('app7020', 'Got it'), 'javascript:;', array('class' => 'gotIt')) ?>
		</div>
	<?php endif; ?>

	<div class="media <?= ($data['bestAnswer'] == 2) ? 'approved' : ''; ?>">
		<!--AVATAR ON CREATOR-->
		<div class="signature" data-custom="circle-avatar">
			<img class="media-object" alt="64x64" src="<?= CoreUser::getAvatarByUserId($data['idUser']); ?>">
		</div>
		<div class="media-body">
			<!--DROPDOWN CONTROLLS-->
			<?php
			if (in_array('ago', $extensions)) {
				$this->widget('common.widgets.DropdownControls', $controlParams);
			}
			?>

			<!--OBJECT TITLE-->
			<div class="media-heading-link">
				<h4 class="media-heading"><span><?= Yii::app()->user->getRelativeUsername($data['userid']); ?></span></h4>
			</div>
			<div class="media-information">

				<!--TIME AGO WHEN IS CREATED-->
				<?php if (in_array('ago', $extensions)): ?>
					<span class="ago"><?= App7020Helpers::timeAgo($data['created']); ?> ago</span>
				<?php endif; ?>

				<!--RELATED LEARNING OBJECT-->
				<?php if ($data["idLearningObject"]):
				$object_name = LearningOrganization::model()->findByPk($data['idLearningObject']);
				?>
					<span class="relatedAsset"><a href="/lms/index.php?r=player&course_id=<?= $object_name->idCourse; ?>&lo_key=<?= $object_name->idOrg; ?>"><?= $object_name->title; ?></a></span>
				<?php endif; ?>

				<!--COUNT OF VIEWS-->
				<?php if (in_array('views', $extensions)): ?>
					<span class="views"><?= $data['viewesCount']; ?> views</span>
				<?php endif; ?>

				<!--RELATED ASSET-->
				<?php if (in_array('relatedAsset', $extensions) && $idContent && $content->conversion_status == App7020Content::CONVERSION_STATUS_APPROVED): ?>
					<span class="relatedAsset">
						<a href="<?= Docebo::createApp7020AssetsViewUrl($idContent) ?>"><?php echo $contentTitle; ?></a>
					</span>
				<?php endif; ?>

			</div>

			<!--TOP/RIGHT CHAT ICON-->
			<?php if (in_array('chat', $extensions)): ?>
				<i class="fa fa-comment-o"></i>
			<?php endif; ?>

			<!--TOP/RIGHT BELL ICON FOR IGNORE REQUEST-->
			<?php if (in_array('ignore', $extensions)): ?>
				<i class="fa fa-bell-slash-o addToIgnore"></i>
			<?php endif; ?>

			<!--TOP/RIGHT BELL ICON FOR UNIGNORE REQUEST-->
			<?php if (in_array('unIgnore', $extensions)): ?>
				<i class="fa fa-bell-o removeFromIgnore"></i>
			<?php endif; ?>

		</div>

		<div class="media-content">

			<!--QUESTION TITLE-->
			<div class="questionTitle">
				<?php echo strip_tags($data['title']); ?>
			</div>

			<!--LAST/BEST ANSWER ONLY BEST ANSWER-->
			<?php if (in_array('lastAnswer', $extensions) && $data['aContent'] && $data['bestAnswer'] == 2): ?>
				<div class="text ellipsis">
					<div class="croped-text">
						<strong><?php echo Yii::t('app7020', 'Best answer:'); ?></strong>
						<?php echo CHtml::link(App7020Helpers::searchHTMLforImg($data['aContent']), $linkUrl); ?>
						<?php echo mb_substr($lastAnswerStriped, 0, 160); ?> <?php echo mb_strlen($lastAnswerStriped) >= 160 ? ' ...' : '' ?>
					</div>
				</div>
			<?php endif; ?>

			<!--ANSWERS LABEL ANSWERCOUNT/NOANSWER-->
			<?php
			if ($data['answersCount'] > 0) {
				$title = Yii::t('app7020', 'View') . " " . $data['answersCount'] . " " . Yii::t('app7020', 'Answers');
				echo CHtml::link($title, $linkUrl, array('class' => 'answerCountButton'));
			} else {

				if ($data['idUser'] == Yii::app()->user->idst) {
					?>
					<span class="no-answers-yet"><i><?= Yii::t('app7020', 'No answers yet') ?></i></span>
					<?php
				} else if (isset($content) && App7020Question::canAnswerQuestion($data['id']) === false) {
					?>
					<span class="no-answers-yet"><i><?= Yii::t('app7020', 'No answers yet') ?></i></span>
					<?php
				}

				else {
					?>
					<div class="answerNow">
						<div class="new"><?php echo Yii::t('app7020', 'New'); ?></div>
						<?php echo CHtml::link(Yii::t('app7020', 'answer now!'), $linkUrl, array('class' => 'answerNowButton')); ?>
					</div>
					<?php
				}
			}
			?>
			<!--IF HAVE NEW ANSWERS FOR YOU-->
			<?php if (in_array('newAnswers', $extensions) && $data['answersCount'] > 0 && $newAnswersCount > 0): ?>
				<i class="fa fa-asterisk">
					<div class="app7020-hint app7020QuestionListView_newAnswersHint">
						<h3><?php echo Yii::t('app7020', 'Hey {username}! This shows new answers', array('{username}' => Yii::app()->user->getRelativeUsername($data['userid']))); ?></h3>
						<h3><?php echo Yii::t('app7020', 'for this questions! Check them out!'); ?></h3>
					</div>
				</i>
			<?php endif; ?>
		</div>

		<div class="media-additional">

			<!--TAKE IN CHARGE LABEL-->
			<?php if (in_array('takeInCharge', $extensions)): ?>
				<div class="takeInChargeLabel">
					<i class="fa fa-bullhorn"></i>
					<span>
						<span class="name"><?php echo Yii::app()->user->getRelativeUsername($data['userid']); ?></span>
						<?php echo Yii::t('app7020', 'has requested the creation of an <strong>asset</strong> to support this question'); ?>
					</span>
					<?php echo CHtml::link(Yii::t('app7020', 'Take in charge'), 'javascript:;', array()) ?>
				</div>
			<?php endif; ?>

			<!--TOOK IN CHARGE LABEL-->
			<?php
			if (in_array('tookInCharge', $extensions)):
				$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idQuestion' => $data['id']));
				$showContribute = false;
				$requestOwner = '';
				if ($requestModel->id) {
					if ($requestModel->idExpert) {
						if (Yii::app()->user->idst == $requestModel->idExpert) {
							$requestOwner = Yii::t('app7020', 'You');
							if (!$requestModel->idContent) {
								$showContribute = true;
							}
						} else {
							$requestOwner = $data['expertFirstname'] . " " . $data['expertLastname'];
						}
					}
				}
				?>
				<div class="tookInChargeLabel">
					<span>
						<span class="name"><?php echo $data['firstname'] . " " . $data['lastname']; ?></span>
						<?php echo Yii::t('app7020', 'has requested the creation of an <strong>asset</strong> to support this question'); ?>
					</span>
					<div class="guruInfo">
						<div class="signature" data-custom="circle-avatar">
							<img class="media-object" alt="64x64" src="<?= CoreUser::getAvatarByUserId($data['idUser']); ?>">
						</div>
						<div class="text">
							<?php echo '<strong>' . $requestOwner . Yii::t('app7020', ' took in charge</strong> this request'); ?>
							<span class="ago"><?php echo App7020Helpers::timeAgo($requestModel->tooked) ?> ago</span>
						</div>
						<?php if ($showContribute): ?>
							<i class="fa fa-cloud-upload"></i>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

			<!--TAKE IN CHARGE DISABLED LABEL-->
			<?php if (in_array('takeInChargeDisabled', $extensions)): ?>
				<div class="takeInChargeLabel disabled">
					<i class="fa fa-bullhorn"></i>
					<span>
						<span class="name"><?php echo Yii::app()->user->getRelativeUsername($data['userid']); ?></span>
						<?php echo Yii::t('app7020', 'has requested the creation of an <strong>asset</strong> to support this question'); ?>
					</span>
					<?php echo CHtml::link(Yii::t('app7020', 'Take in charge'), 'javascript:;', array()) ?>
				</div>
			<?php endif; ?>

		</div>
	</div>
</div>
