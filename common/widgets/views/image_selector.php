<?php
	/* @var $this ImageSelector */
	
	if ($this->showImgPreview) {
		$src = CoreAsset::url($this->assetId, $this->imgVariant);
		echo CHtml::image($src, '', $this->imgHtmlOptions);
	}
	
	echo CHtml::hiddenField($this->inputHtmlOptions['name'], $this->assetId, $this->inputHtmlOptions);
	
	$url = Docebo::createLmsUrl('imageSelector/dialog', array(
		'modal_id'			=> $this->dialogId,	
      	'element_id'		=> $this->inputHtmlOptions['id'],	
      	'type' 				=> $this->imageType,
      	'return_variant'	=> $this->imgVariant,
      	'preselected_id'	=> $this->assetId,
        'showStandardImages'   => $this->showStandardImages
	));
	
?>

<br class="clearfix"/>
<br />

<a 	
	class				="open-dialog <?= $this->buttonClass ?>"
	data-dialog-id		="<?= $this->dialogId ?>"
    data-dialog-class	="<?= $this->dialogClass ?>"
    href				="<?= $url ?>"
>
<?= $this->buttonText ?>
</a>

<script type="text/javascript">
	$(document).controls();
</script>
