<?php
/* @var $this Apps2Widget */
?>
<div class="single-app-holder <?=$this->isAvailable ? 'available' : 'not-available';?>">
	<div class="pull-right"><?php $this->renderApp();?></div>
	<h4 class="app-title"><?=$this->app['title']?></h4>
	<p class="description"><?= $this->app['short_description'] ?></p>
	<?php if (!$this->app['is_available'] && !$this->app['is_active']) { ?>
		<div class="available-plan-warning">
			<i class="fa fa-exclamation-triangle"></i>&nbsp; 
    		<?php echo Yii::t("apps", "Available starting from {plan} plan", array(
    	       '{plan}' => '<span class="plan-name">' . $this->app['available_in_plan'] . '</span>'
    	   )); ?> 
		</div>
	<?php } ?>
	
</div>