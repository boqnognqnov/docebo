<?php $url = Yii::app()->legacyWrapper->getHydraCourseUrl(Yii::app()->player->course); ?>
<a id="link-viewer" href="javascript:void(0)">Switch to learner view to see the widget</a>

<script>
	$('#link-viewer').on('click',function(){
		<?php echo Yii::app()->legacyWrapper->setHydraRoute($url,false); ?>
	});
</script>