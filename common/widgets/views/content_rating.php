<?php
$this->widget('common.extensions.dzRaty.DzRaty', array(
    'name' => 'content_rating_'.$this->contentId .  '_' .rand(0, 10000000) ,
    'value' =>  $this->avgRating ? $this->avgRating : App7020ContentRating::calculateContentRating($this->contentId),
    'options' => array(
        'half' => TRUE
    ),
    'htmlOptions' => array(
        'class' => 'new-half-class'
    ),
    'data' => array(1, 2, 3, 4, 5),
    'options' => array(
        'readOnly' => $this->readOnly,
        'click' => "js:function(score, evt){ app7020.rate.init(score, ".$this->contentId.", $(this));}",
    ),
));