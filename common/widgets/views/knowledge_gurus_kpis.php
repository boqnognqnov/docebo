<div class="main-section knowGuruKpiWidget" data-change-layout="false"   data-container="root">
<?php 
$this->widget('common.extensions.jqplot.JqplotGraphWidget', array(
	'pluginScriptFile'=>array(
		'jqplot.dateAxisRenderer.js',
		'jqplot.barRenderer.js',
		'jqplot.categoryAxisRenderer.js',
		'jqplot.highlighter.js',
       )
    )
);

?>
<div class="knowledge_guru_kpi_title">
<?php 
    echo ucwords($dashletArray['title']);
?>
</div>
<div class="knowledge_guru_kpi_chart_container">
    <div class="knowledge_guru_kpi_chart" id="knowledge_guru_kpi_chart"></div>
</div>

<div class="knowledge_guru_kpi_stats">

    <?php if (!empty($widgetOptions['reviewed_assets'])){?>
    <div class="stat_row">    
        <div class="big_number">
            <?= $dashletArray['reviewedAssets']['assetsCount']; ?>
        </div>
        <div class="stat_text">
            <?=ucfirst($dashletArray['subText1']); ?> 
            <span class="reviewed"><?= strtolower(Yii::t('app7020', 'Reviewed Assets')); ?></span>
        </div>
        <div class="stat_percentage">
        <?php if($widgetOptions['timeframe'] != 'from_beginning'){?>
            <span class="<?php echo ($dashletArray['reviewedAssets']['percentage'] < 0) ? "negative" : "positive"; ?>">
                <?php 
                    echo ($dashletArray['reviewedAssets']['percentage'] < 0) ? "-" : ($dashletArray['reviewedAssets']['percentage'] == 0) ? "" : "+";
                    echo $dashletArray['reviewedAssets']['percentage'];
                ?>%
            </span>
        <?php }?>
        </div>
    </div>
    
    <?php }?>

    
    
    <?php if (!empty($widgetOptions['answered_questions'])){?>
    <div class="stat_row">    
        <div class="big_number">
            <?= $dashletArray['answeredQuestions']['questionsCount'][0]; ?>
        </div>
        <div class="stat_text">
            <?=ucfirst($dashletArray['subText1']); ?> 
            <span class="answered"><?= strtolower(Yii::t('app7020', 'Answered Questions')); ?></span>
        </div>
        <div class="stat_percentage">
        <?php if($widgetOptions['timeframe'] != 'from_beginning'){?>
            <span class="<?php echo ($dashletArray['answeredQuestions']['percentage'] < 0) ? "negative" : "positive"; ?>">
                <?php 
                    echo ($dashletArray['answeredQuestions']['percentage'] < 0) ? "-" : ($dashletArray['answeredQuestions']['percentage'] == 0) ? "" : "+";
                    echo $dashletArray['answeredQuestions']['percentage'];
                ?>%
            </span>
        <?php }?>
        </div>
    </div>
    
    
    <?php }?>
	
	
	
	 <?php if (!empty($widgetOptions['satisfied_knowledge_requests'])){?>
    <div class="stat_row">    
        <div class="big_number">
            <?= $dashletArray['satisfied_knowledge_requests']["satisfied"]; ?>
        </div>
        <div class="stat_text">
            <?=ucfirst($dashletArray['subText1']); ?> 
            <span class="satisfied"><?= strtolower(Yii::t('app7020', 'Satisfied requests')); ?></span>
        </div>
        <div class="stat_percentage">
            <span class="<?php echo ($dashletArray['answeredQuestions']['percentage'] < 0) ? "negative" : "positive"; ?>">
              <?php echo $dashletArray['satisfied_knowledge_requests']["percentage"];?>
            </span>
        </div>
    </div>
    
    
    <?php }?>
    
    
    <?php if (!empty($widgetOptions['what_learners_think'])){?>
    <div class="stat_row whiteStatRow">    
        
        <div class="stat_text likeDislikeText">
            <?= ucfirst(Yii::t('app7020', 'What do learners think about my answers ')).$dashletArray['subText2']."?"; ?>
        </div>
        <div class="like-dislike likeDislikeStat" data-custom="likeDislike">
        
			<span class="like"><?= intval($dashletArray['likes'][1]) ?></span>
			<span class="dislike"><?= intval($dashletArray['likes'][2]) ?></span>
		</div>   
        
    </div>
    
    
    <?php }?>
    
    <?php if (!empty($widgetOptions['answers_marked_as_best'])){?>
    <div class="stat_row whiteStatRow">    
        <div class="big_number">
            <?= $dashletArray['bestAnswers']['count']; ?>
        </div>
        <div class="stat_text">
            <?=ucfirst($dashletArray['subText1']); ?> 
            <?= strtolower(Yii::t('app7020', 'answers mark <b>as</b> ')); ?> <b>"<?= strtolower(Yii::t('app7020', 'Best Answer')); ?>"</b>
        </div>
        <div class="stat_percentage">
        <?php if($widgetOptions['timeframe'] != 'from_beginning'){?>
            <span class="<?php echo ($dashletArray['bestAnswers']['percentage'] < 0) ? "negative" : "positive"; ?>">
                <?php 
                    echo ($dashletArray['bestAnswers']['percentage'] < 0) ? "-" : ($dashletArray['bestAnswers']['percentage'] == 0) ? "" : "+";
                    echo $dashletArray['bestAnswers']['percentage'];
                ?>%
            </span>
        <?php }?>
        </div>
    </div>
    
    
    <?php }?>
</div>
<?php 
if($widgetOptions['timeframe'] != 'from_beginning'){?>
    <script type="text/javascript">
	    $(function(){
	    	var line1=<?php echo empty($dashletArray['reviewedAssets']['chartArray']) ? '[]' : $dashletArray['reviewedAssets']['chartArray']; ?>;
	        var line2=<?php echo empty($dashletArray['answeredQuestions']['chartArray']) ? '[]' : $dashletArray['answeredQuestions']['chartArray']; ?>;
	        var line3=<?php echo empty($dashletArray['satisfied_knowledge_requests']['chartArray']) ? '[]' : $dashletArray['satisfied_knowledge_requests']['chartArray']; ?>;
	        var chartData = [];
	        var seriesColors = [];
	        if(line1.length > 0) {
	            chartData.push(line1);
	            seriesColors.push('#5FBD5D');
	        }
	        if(line2.length > 0) {
	            chartData.push(line2);
	            seriesColors.push('#0465AC');
	        }
	        if(line3.length > 0) {
	            chartData.push(line3);
	            seriesColors.push('#E84C3D');
	        }
			if(chartData.length > 0){
				$.jqplot.config.enablePlugins = true;
				var plot1 = $.jqplot('knowledge_guru_kpi_chart', chartData, {

					axes:{
						xaxis:{
							renderer:$.jqplot.DateAxisRenderer,
							tickInterval: '1 day',
							tickOptions:{
								formatString:'<?= $dashletArray['formatDate'] ?>',
								fontSize: 12,
								gridLineColor: '#000000', 
							} 
						},
						yaxis: {
						   tickOptions: {formatString: '%d', showGridline: false, show: false},
						   min: 0
						}, 
						rendererOptions: {
							drawBaseline: false
						}
					},
					seriesColors: seriesColors,
					grid: {
						background: '#f5f5f5',
						shadow: false,
						borderWidth: 0,
					},
					series:[{
								lineWidth:4, 
								markerOptions:{
									style:'circle', 
									size: 1, 
									shadow: false
								}
						   },
						   {
							   lineWidth:4, 
							   markerOptions:{
								   style:'circle', 
								   size: 1, 
								   shadow: false
							   }
						  }
					],
					highlighter: {
						sizeAdjust: 12,
						followMouse: true,
						tooltipAxes: 'y',
						tooltipLocation: 'n',
						tooltipContentEditor: function (str, seriesIndex, pointIndex, plot) {
							var html = "<div class='arrow'></div><span class='color"+seriesIndex+"'>" + str + "</span>";
							return html;
						}
				   },
				   gridPadding:{
					   right:20,
					   left:20, 
					   top: 15, 
					   bottom: 15
				   }

				});


			// make it scalable with the browser window
			$(window).resize(function (event, ui) {
				plot1.replot( { resetAxes: true } );
			});
		}
    });
    
    </script>

<?php }?>
</div>