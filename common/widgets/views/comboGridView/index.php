<?php
/* @var $form  CActiveForm */
/* @var $this ComboGridView */

$cs = Yii::app()->getClientScript();
$themeUrl = Yii::app()->theme->baseUrl;
$cs->registerScriptFile($themeUrl . '/js/combogridview.js');

$selectedItemsFieldName = "combo_grid_selected_items_fieldname";
$formId = "combo-grid-form";
?>
<style>
    .search-input-wrapper.pull-right label{
        display: inline-block;
        vertical-align: text-top;
        font-size: 1.1em;
    }
    .radioDiv{
        display: inline-block;
        margin: 0 10px;
    }
    .left-selections.pull-left {
        float: left !important;
    }
    .combo-grid-view-container td.group table div.left-selections > p {
        margin-bottom: 6px;
    }
    .grid-view.infinite-scroll-ext .infiniteScroll{
        text-align: center;
        padding-bottom: 10px;
        padding-top: 10px;
        border: 1px solid #e6e6e6;
        border-top: 0;
        display: none;
    }
    .grid-view.infinite-scroll-ext .infiniteScroll .fa-spinner{
        margin-right: 5px;
    }
    .grid-view.infinite-scroll-ext .summary{
        display: none;
    }
    .grid-view.infinite-scroll-ext .pager{
        display: none;
    }

</style>

<div class="combo-grid-view-container" id="combo-grid-view-container-<?= $this->gridId ?>">

    <?php
    if (!$this->doNotCreateForm) {
        $form = $this->beginWidget('CActiveForm', array(
            'id' => $formId,
            'htmlOptions' => array(
                'class' => 'form-inline',
            ),
        ));
    }

    // Add any hidden fields requested by widget caller
    foreach ($this->hiddenFields as $name => $value) {
        echo CHtml::hiddenField($name, $value);
    }
    ?>

    <?php if (!$this->disableSearch) : ?>
        <div class="filters-wrapper">
            <table class="filters table-border">
                <tr>
                    <? foreach($this->customFilterGroups as $customFilterGroup): ?>
                    <td class="group <?= (isset($customFilterGroup['groupClass']) ? $customFilterGroup['groupClass'] : null) ?>">
                        <table>
                            <tr>
                                <td>
                                    <?= $customFilterGroup['groupHtml'] ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <? endforeach; ?>
                    <td class="group">
                        <table width="100%">
                            <tr>
                                <td>
                                    <?php if (!$this->disableMassSelection && $this->massSelectorsInFilterForm): ?>
                                        <div class="left-selections pull-left clearfix">
                                            <p><?= Yii::t('standard', 'You have selected') ?> <strong><span id="combo-grid-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>.</p>
                                            <a href="javascript:void(0);" class="combo-grid-select-all"> <?= Yii::t('standard', '_SELECT_ALL') ?> </a>
                                            <a href="javascript:void(0);" class="combo-grid-deselect-all"> <?= Yii::t('standard', '_UNSELECT_ALL') ?> </a>
                                        </div>
                                    <?php endif; ?>
                                    <div class="search-input-wrapper pull-right">
                                        <?php
                                        $params = array();
                                        $params['max_number'] = 10;
                                        $params['autocomplete'] = 1;
                                        foreach ($this->hiddenFields as $name => $value) {
                                            $params[$name] = $value;
                                        }
                                        $autoCompleteUrl = $this->controller->createUrl($this->autocompleteRoute, $params);
                                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                                            'id' => 'combo-grid-search-input-' . $this->gridId,
                                            'name' => 'search_input',
                                            'value' => '',
                                            'options' => array(
                                                'minLength' => $this->autocompleteRoute ? 1 : 9999, // if no route, effectively never try to autocomplete
                                            ),
                                            'source' => $autoCompleteUrl,
                                            'htmlOptions' => array('class' => 'search-query', 'placeholder' => Yii::t('standard', '_SEARCH')),
                                        ));
                                        ?>
                                        <button type="button" class="close clear-search">&times;</button>
                                        <span 	class="perform-search search-icon"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?php if ($this->flagForCustomFields === true) { ?>
                        <td class="group">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div class="search-input-wrapper pull-right">
                                            <div class="radioDiv">
                                                <input type="radio" name="type" value="all"
                                                       id="typeAll" <?php if ($this->typeOfModel === false) echo 'checked'; ?>/>
                                                <label for="typeAll"><?= Yii::t('standard', '_ALL') ?></label>
                                            </div>
                                            <div class="radioDiv">
                                                <input type="radio" name="type" value="classroom"
                                                       id="typeClassroom" <?php if ($this->typeOfModel == 'classroom') echo 'checked'; ?>/>
                                                <label for="typeClassroom"><?= Yii::t('standard', '_CLASSROOM') ?></label>
                                            </div>
                                            <div class="radioDiv">
                                                <input type="radio" name="type" value="webinar"
                                                       id="typeWebinar" <?php if ($this->typeOfModel == 'webinar') echo 'checked'; ?>/>
                                                <label for="typeWebinar"><?= Yii::t('webinar', 'Webinar') ?></label>
                                            </div>
                                            <div class="radioDiv">
                                                <input type="radio" name="type" value="plan"
                                                       id="typePlan" <?php if ($this->typeOfModel == Yii::t('report_filters', '_FIELDS_COURSEPATHS')) echo 'checked'; ?>/>
                                                <label for="typePlan"><?= Yii::t('report_filters', '_FIELDS_COURSEPATHS') ?></label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    <?php } ?>
                </tr>
            </table>
        </div>
    <?php endif; ?>

    <?php if (!empty($this->gridTitleLabel)) : ?>
        <h2><?= $this->gridTitleLabel ?></h2>
    <?php endif; ?>

    <?php if (!$this->disableMassSelection || !$this->disableMassActions) : ?>
        <div class="selections clearfix">
        <?php endif; ?>

        <?php if (!$this->disableMassSelection && !$this->massSelectorsInFilterForm) : ?>
            <div class="left-selections clearfix">
                <p><?= Yii::t('standard', 'You have selected') ?> <strong><span id="combo-grid-selected-count">0</span> <?= Yii::t('standard', 'items') ?></strong>.</p>
                <a href="javascript:void(0);" class="combo-grid-select-all"> <?= Yii::t('standard', '_SELECT_ALL') ?> </a>
                <a href="javascript:void(0);" class="combo-grid-deselect-all"> <?= Yii::t('standard', '_UNSELECT_ALL') ?> </a>
            </div>
        <?php endif; ?>

        <?php if (!$this->disableMassActions) : ?>
            <div class="right-selections clearfix">
                <select name="combo_grid_massive_action" class="combo-grid-selection-massive-action">
                    <option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
                    <?php if (is_array($this->massiveActions)) foreach ($this->massiveActions as $value => $label) : ?>
                            <option value='<?= $value ?>'><?= $label ?></option>
                        <?php endforeach; ?>
                </select>
            </div>
        <?php endif; ?>			

        <?php if (!$this->disableMassSelection || !$this->disableMassActions) : ?>
        </div>
    <?php endif; ?>


    <div class="items-grid-wrapper">
        <?php
        if ($this->noYiiGridViewJS) {
            Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
            Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
        }

        $template = '{pager}{items}{summary}{pager}';
        if ($this->gridTemplate !== false && is_string($this->gridTemplate)) {
            $template = $this->gridTemplate;
        }

        $gridParams = array(
            'selectableRows' => 3,
            'ajaxType' => 'POST',
            'id' => $this->gridId,
            'dataProvider' => $this->dataProvider,
            // Elements which will trigger grid update
            'updateSelector' => "{page}, {sort}",
            'columns' => $this->columns,
            // non-quartz-items: because of a Global JS event listener in script.js (see there please)
            // which breaks normal Yii event chain
            'itemsCssClass' => 'items non-quartz-items',
            'template' => $template,
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
            'rowCssClassExpression' => array($this, 'getRowCssClass'), // used for .sortable() serialization !!!!
            'pager' => array(
                'class' => 'DoceboCLinkPager',
                'maxButtonCount' => 8,
            ),
            'beforeAjaxUpdate' => 'function(id,options) {
					options.data = $(\'#\'+id).comboGridView(\'getFilterData\');
				}',
            'afterAjaxUpdate' => 'function(id, data) {
					$(\'#\'+id).comboGridView(\'updateCurrentPageSelection\');
					$(\'#\'+id).comboGridView(\'afterAjaxUpdate\');
					$(\'a[rel="tooltip"]\').tooltip();
					$(document).controls();
				}',
            'selectionChanged' => 'function(id) {
					$(\'#\'+id).comboGridView(\'gridSelectionChanged\', id);
					$("input,select", $(\'#\'+id)).trigger("refresh");
				}',
            'cssFile' => Yii::app()->theme->baseUrl . '/css/DoceboCGridView.css',
            'infiniteScroll' => $this->infinite
        );
				if (!empty($this->gridAjaxUrl)) { $gridParams['ajaxUrl'] = $this->gridAjaxUrl; }
				$this->widget('zii.widgets.grid.CGridView', $gridParams);

        if (!$this->doNotCreateForm) {
            $this->endWidget();
        }
        ?>
    </div>

</div>

<div class="clearfix"></div>


<script type="text/javascript">
//<![CDATA[
    $(function () {
        $('#' +<?= json_encode($this->gridId) ?>).comboGridView({
            sortableRows: <?= json_encode($this->sortableRows) ?>,
            sortableUpdateUrl: <?= json_encode($this->sortableUpdateUrl) ?>,
            dragHandlerSelector: <?= json_encode($this->dragHandlerSelector) ?>,
            massSelectorUrl: <?= json_encode($this->massSelectorUrl) ?>
        });
    });

<?php if (!empty($this->preselectedItems) && is_array($this->preselectedItems)) { ?>
        $('#' +<?= json_encode($this->gridId) ?>).comboGridView("setSelection", <?= json_encode($this->preselectedItems) ?>);
<?php } ?>
//]]>
</script>
