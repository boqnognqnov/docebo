
<div class="grey-info">

	<div class="pull-right sample-status">
		<div class="p-sprite arrow-circle-black"></div>
	</div>


		<div class="pull-left thumb-holder" data-bootstro-id="bootstroPlayerListViewThumb">
			<div id="bootstroPlayerListViewThumb">
				<div class="arrow"></div>
				<?=Yii::t('standard', 'Thumbnail')?>
			</div>
			<div class="thumb"></div>
		</div>

		<div class="pull-left sample-lo-info">
			<p class="title"><?=Yii::t('report', 'L.O. Title')?></p>

			<p><?= Yii::t('standard', '_SHORTDESC') ?></p>

			<div id="bootstroListViewDescr">
				<div class="arrow"></div>
				<?= Yii::t('standard', '_SHORTDESC') ?>
			</div>
		</div>

		<div class="clearfix"></div>

</div>

<br/><br/><br/>

<div class="row-fluid">

	<div class="span3">
		<p style="float: right; padding-right: 15px; text-align: right">
			<?= Yii::t('standard', '_SHORTDESC') ?>
			<br/>
			<small>Max 200 char.</small>
		</p>
		<div class="clearfix"></div>
	</div>
	<div class="span9">
	<?=CHtml::textArea('short_description', ($loModel ? $loModel->short_description : null), array(
		'rows'=>2,
		'maxlength'=>200,
		'style'=>'width: 96%;'
	))?>
	</div>
</div>

<br/><br/><br/><br/>

<div class="loSlider" data-refresh-url="<?= $this->refreshUrl ? $this->refreshUrl : Docebo::createLmsUrl('player/training/getSlidersContent')?>" data-idcourse="<?= $courseModel ?  $courseModel->idCourse: null?>" data-lo-id="<?=$loModel instanceof LearningOrganization? $loModel->idOrg : null ?>">
	<?php
	// default crop size
	$cropSize = array(
		'width' => 300,
		'height' => 300,
	);
	Yii::app()->event->raise('OverrideCourseImageCropSize', new DEvent($this, array(
		'cropSize' => &$cropSize
	)));

	if($loModel instanceof LearningOrganization){
		list($count, $defaultImages, $userImages, $selected) = LearningOrganization::prepareThumbnails($loModel->idOrg, 10);
	} else {
		list($count, $defaultImages, $userImages, $selected) = LearningRepositoryObject::prepareThumbnails($loModel->id_object, 10);
	}


	$uniqueId = substr(md5(uniqid(rand(), true)), 0, 10);

	$this->render('playerLayoutAdditionalInfo/_sliders', array(
		'model' => $loModel,
		'defaultImages' => $defaultImages,
		'userImages' => $userImages,
		'count' => $count,
		'selectedTab' => $selected,
		'uniqueId' => $uniqueId,
		'cropSize' => $cropSize,
		'idCourse'=> $courseModel ? $courseModel->idCourse : null,
		'deleteUrl' => $this->deleteUrl
	), false); ?>
</div>

<script type="text/javascript">
	$(function(){
		$('.loSlider input').styler();
	})
</script>