<?php
/**
 * @var $deleteUrl string
 */
?>
<div class="courseEnroll-tabs">
	<ul class="nav nav-tabs">
		<li class="<?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
			<a data-toggle="tab" href="#loThumbShared">
				<?php echo Yii::t('course', 'Choose a thumbnail for your course'); ?> (<?php echo $count['default']; ?>)
			</a>
		</li>
		<li class="<?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
			<a data-toggle="tab" href="#loThumbPrivate">
				<?php echo Yii::t('course', 'Upload Your Own Thumbnail'); ?> (<span id="user-images-count" style="margin: 0;"><?php echo $count['user']; ?></span>)
			</a>
		</li>
	</ul>

	<div class="select-user-form-wrapper">
		<div class="upload-form-wrapper" style="display: <?php echo $selectedTab == 'user' ? 'block' : 'none';?>">
			<div class="upload-form">
				<div class="fileUploadContent">
					<label for="upload-btn"><?php echo Yii::t('branding', '_UPLOAD_BACKGROUND_BTN'); ?></label>
					<?php echo CHtml::fileField('LearningOrganization[attachments]', '', array(
						'class' => 'ajaxCropNoParentForm custom-image',
						'data-class' => 'LearningOrganization',
						'data-width' => $cropSize['width'],
						'data-height' => $cropSize['height'],
						'data-image-type' => CoreAsset::TYPE_COURSELOGO,
						'data-callback' => 'updateSliderContent',
						'data-url' => Docebo::createAdminUrl('crop', array(
							'imageType' => CoreAsset::TYPE_LO_IMAGE,
							'class'=>'LearningOrganization',
							'width'=>$cropSize['width'],
							'height'=>$cropSize['height'],
							'cropCallback' => 'updateSliderCarousel', // see $.fn.updateSliderCarousel
						)),
					)); ?>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div id="loThumbShared" class="courseEnroll-page-users tab-pane <?php echo $selectedTab == 'default' ? 'active' : ''; ?>">
				<div id="defaultSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($defaultImages)) { ?>
							<?php foreach ($defaultImages as $key => $defaultThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($defaultThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="thumb" value="' . $key . '" '. ($model && $key == $model->resource ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($model && $key == $model->resource ? 'checked' : '') .'">' . $html . '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#defaultSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
			<div id="loThumbPrivate" class="courseEnroll-page-groups tab-pane <?php echo $selectedTab == 'user' ? 'active' : ''; ?>">
				<div id="userSlider_<?php echo $uniqueId; ?>" class="carousel slide thumbnails-carousel">
					<!-- Carousel items -->
					<div class="carousel-inner">
						<?php if (!empty($userImages)) { ?>
							<?php foreach ($userImages as $key => $userThumbnail) { ?>
								<div class="item<?php echo ($key == 0) ? ' active' : ''; ?>">
									<?php foreach ($userThumbnail as $key => $imageUrl) {
										$html = CHtml::image($imageUrl);
										$html .= '<input type="radio" name="thumb" value="' . $key . '" '. ($model && $key == $model->resource ? 'checked="checked"' : '') .' />';
										echo '<div class="sub-item ' . ($model && $key == $model->resource ? 'checked' : '') .'">' . $html;
										echo '<span class="deleteicon"><span class="i-sprite is-remove red"></span></span>';
										echo '</div>';
									}
									?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>

					<input style="position: absolute;" class="hidden" value="0" id="currentPage"/>
					<input style="position: absolute;" class="hidden" value="<?php echo $count['user'];?>" id="totalPages"/>
					<input style="position: absolute;" class="hidden" value="<?php echo $_REQUEST['course_id'] ? $_REQUEST['course_id'] : false;?>" id="courseId"/>
					<input style="position: absolute;" class="hidden" value="<?php echo $_REQUEST['id_object'] ? $_REQUEST['id_object'] : false;?>" id="objectId"/>
					<input style="position: absolute;" class="hidden" name="selectedImage" value="<?php echo $model->resource ? $model->resource : false;?>" id="selectedImageId"/>
					<input style="position: absolute;" class="hidden" value="<?=$getThumbnailsUrl ? $getThumbnailsUrl : Docebo::createLmsUrl('player/training/getThumbnails')?>" id="thumbnailRequestUrl"/>
					<!-- Carousel nav -->
					<a class="carousel-control left" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="prev">&lsaquo;</a>
					<a class="carousel-control right" href="#userSlider_<?php echo $uniqueId; ?>" data-slide="next">&rsaquo;</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(function(){
		$('.nav.nav-tabs li a').on('click', function (e) {
			if($(this).attr('href')==='#loThumbPrivate')
				$('.upload-form-wrapper').show();
			else
				$('.upload-form-wrapper').hide();
		});

		$('.loSlider .sub-item > img').on('click', function(){
			var isChecked = $(this).parent().hasClass('checked');
			if(isChecked){
				$(this).closest('.sub-item').removeClass('checked');
				$(this).closest('.sub-item').find('input[type=radio]').attr("checked", false);;
			}else{
				// Remove "selected" state from all items in shared and private carousels
				$('.loSlider .sub-item').removeClass('checked');

				// And then check the current image as active
				$(this).closest('.sub-item').addClass('checked').find('input[type=radio]').click();
			}

		});
	});

	function getThumnbnails(forward){
		var container = $('.select-user-form-wrapper');
		var count = $.trim($('#user-images-count').html());
		var id_object = container.find('#objectId').val();
		var nextPage = 0;
		var currentPageVal =  container.find('#currentPage').val();
		var courseId = container.find("#courseId").val();
		if(!currentPageVal){
			currentPageVal = 0;
		}
		var selectedImageId = '';
		if(container.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val()){
			selectedImageId = container.find('.sub-item.checked').find('.jq-radio.checked').parent().find('input').val();
			container.find('#selectedImageId').attr('value', selectedImageId);
		}else{
			selectedImageId = container.find('#selectedImageId').val();
		}
		if(forward){
			nextPage =  parseInt(currentPageVal) +1;
		}else{
			nextPage = parseInt(currentPageVal)-1;
		}
		container.find('.custom-thumbnails-carousel  .carousel-inner').html('<div class="ajaxloader" style="display: block;"></div>');
		$.ajax({
			url: '<?=$getThumbnailsUrl ? $getThumbnailsUrl : Docebo::createLmsUrl('player/training/getThumbnails')?>',
			type: "POST",
			data: {
				pageId: nextPage,
				totalCount: count,
				course_id: courseId,
				selectedImageId: selectedImageId,
				id_object: id_object
			},
			success: function(result){
				var courseId = container.find('.custom-thumbnails-carousel  .carousel-inner').html(result.html);
				if(result.currentPage){
					container.find('#currentPage').attr('value', result.currentPage);
				}else{
					container.find('#currentPage').attr('value',0);
				}
				$('.select-user-form-wrapper .item > div').live('click', function () {
					$('.select-user-form-wrapper .item > div').removeClass('checked');
					// $('.thumbnailSlider .sub-item').removeClass('checked');

					$(this).addClass('checked');

					// $(this).find('input[type="radio"]').trigger('click');
					$(this).find('input[type="radio"]').prop('checked', true);
					$(this).parent().find('.jq-radio').removeClass('checked');
					$(this).find('.jq-radio').addClass('checked');
				});
			},
			error: function() {
			}
		});
	}

	function updateControls(nav) {
		if (nav == 'show' ) {
			$('[id*="userSlider"] .carousel-control.right').show();
			$('[id*="userSlider"] .carousel-control.left').show();
		}
		else if (nav == 'hide' ) {
			$('[id*="userSlider"] .carousel-control.right').hide();
			$('[id*="userSlider"] .carousel-control.left').hide();
		}
	}
	var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
	updateControls(navStatus);



	/**
	 * DELETE User image
	 */

	$(document).on('click', '#loThumbPrivate .deleteicon', function(e) {

		e.preventDefault();

		var imageId = $(this).closest('.sub-item').find('input[type="radio"]').val();
		var form = $(this).closest('form');

		var options = {
			url         : '<?=$deleteUrl ? $deleteUrl : Docebo::createLmsUrl('player/training/deleteImage')?>',
			type        : 'post',
			dataType    : 'json',
			data: {
				delete_image	: true,
				image_id		: imageId,
				'course_id'     : <?=$idCourse? $idCourse : 0?>
			},
			success     : function (res) {
				if (res && (res.success == true)) {
					$('[id*="userSlider"]').find('input[type="radio"][value="'+ imageId +'"]').closest('.sub-item').remove();
					$('#user-images-count').text($('[id*="userSlider"] .sub-item').length);
					var navStatus = $('[id*="userSlider"] .sub-item').length > 0 ? 'show' : 'hide';
					updateControls(navStatus);
				}
			}
		};

		$.ajax(options);
		return false;

	});

</script>
