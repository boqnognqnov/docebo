<? /* @var $this Sidebarred */ ?>
<div class="sidebarred">
	<div id="<?= (!empty($sidebarId) ? $sidebarId : 'sidebar') ?>" class="sidebar">
		<?php if (!empty($sidebarTitle)): ?>
		<h3><?=$sidebarTitle?></h3>
		<?php endif; ?>

		<? if (is_array($sidebarItems)): ?>
		<ul>
			<? foreach($sidebarItems as $sidebarItem): ?>
				<li>
					<?=CHtml::link(($sidebarItem['title'] ? '<span></span>'.$sidebarItem['title'] : null), '#'. $sidebarItem['data-tab'], array_merge($sidebarItem, array('data-loaded' => 0, 'class'=>$sidebarItem['data-tab'])))?>
				</li>
			<? endforeach; ?>
		</ul>
		<? endif; ?>
	</div>
	<div class="main">
		<div class="tab-content">
			<span class="ajaxloader"><?= Yii::t("standard", "_LOADING") ?>...</span>
		</div>
	</div>
</div>