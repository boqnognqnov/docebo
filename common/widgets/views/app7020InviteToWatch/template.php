<h2><?php echo Yii::t('app7020','People') ?> :</h2>
<div class="outer">
	<input type="text" ng-model="model" placeholder="{{prompt}}" ng-keydown="selected=false"/>
</div>
<h2><?php echo Yii::t('app7020','Suggestions') ?> :</h2>
<ul class="items" ng-hide="!model.length || selected">
	<li class="item span6" ng-repeat="item in items | filter:model  track by $index" ng-click="handleSelection(item[title])" style="cursor:pointer" ng-class="{active:isCurrent($index)}" ng-mouseenter="setCurrent($index)">
		<div class="user-entry">
			<div class="media">
				<div class="pull-left" href="javascript:">

					<div class="app7020_signature" data-custom="circle-avatar">
						<img class="media-object" alt="64x64" ng-src="{{item.avatar}}">
					</div>

				</div>
				<div class="media-body pull-left">
					<h4 class="media-heading">{{item.name}}</h4>
					{{item.username}}

				</div>
				<div class="add-to-list">
					<i class="fa fa-plus-circle add-to-watch"></i>
				</div>
			</div>
		</div>
	</li>
</ul>