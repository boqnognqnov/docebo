<?php
$url = Docebo::createApp7020Url('knowledgeLibrary/axInviteUsers');
echo CHtml::beginForm($url, 'post', array(
	'class' => 'ajax form_step1',
	'id' => 'editChannelForm'
));
?>
<div ng-app="InviteToWatch">
	<div id="invite_to_watch" class="row-fluid" ng-controller="cTypeAhead">
		<typeahead items="items" prompt="<?php echo Yii::t('app7020','Start typing a name here') ?> ..." value="name" username="abbreviation" model="name" on-select="onItemSelected()" />
	</div>
</div>
<?php if($this->modal == true){?>
	<footer id="app7020-controlls" ng-controller="cTypeAhead">
		<?php
		echo CHtml::submitButton('Invite', array('class' => 'save-dialog btn'));
		echo CHtml::link('Close', 'javascript:;', array('class' => 'close-dialog btn'));
		?>
	</footer>
<?php }
echo CHtml::endForm();
?>

