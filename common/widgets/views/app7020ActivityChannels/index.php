<?php
foreach ($this->data as $key => $row) {
	$value[$key] = $row['value'];
}
array_multisort($value, SORT_DESC, $this->data);
$limited = array();
if (count($this->data) > $visible) {
	$limited = array_slice($this->data, $this->visible);
	foreach ($limited as $channel) {
		$summary += $channel['value'];
	}
}
?>
<div class="app7020ActivityChannels">
	<div class="grid-wrapper">
		<table>
			<tbody>
				<?php foreach ($this->data as $k => $channel): ?>
					<tr class="<?php echo ($k > $this->visible - 1) ? 'hide' : '' ?>">
						<td class="title">
							<?php echo $channel['label']; ?>
						</td>
						<td class="line-pre-wrapper">
							<div class="line-wrapper" style="width: <?php echo $channel['value']; ?>%; background: <?php echo $channel['color']; ?>;">
								<span class="line-text"><?php echo $channel['value']; ?>%</span>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
				<?php if (count($limited)): ?>
					<tr class="toggle">
						<td class="title"><span class="text-label">Other channels</span><i></i></td>
						<td class="line-pre-wrapper">
							<div class="line-wrapper" style="width: <?php echo $summary; ?>%; background: #999999;">
								<span class="line-text"><?php echo $summary; ?>%</span>
							</div>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<div class="separators">
			<div><span>0%</span></div>
			<div><span>25%</span></div>
			<div><span>50%</span></div>
			<div><span>75%</span></div>
			<div><span>100%</span></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	(function ($) {
		$('.app7020ActivityChannels').each(function () {
			if(typeof $(this).data('events') === 'undefined')
			$(this).on('click', '.toggle .title', function (e) {
				if ($(e.delegateTarget).hasClass('in')) {
					$(e.delegateTarget).removeClass('in');
				} else {
					$(e.delegateTarget).addClass('in');
				}
			});
		});
	})(jQuery);
</script>