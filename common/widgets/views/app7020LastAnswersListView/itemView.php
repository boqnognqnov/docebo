<?php
$questionContentStriped = strip_tags($data['questionTitle']);
$questionContentStriped = mb_strlen($questionContentStriped) >= 200 ? mb_substr($questionContentStriped, 0, 200) . '...' : $questionContentStriped;
$linkUrl = Docebo::createApp7020Url('askGuru/item', array('id' => $data['questionId']));
?>
<div class="itemGrid <?php echo $spanSizeType; ?>" data-idObject="<?php echo $data['answerId'] ?>" data-quickViewSize="<?php if($isLinkedVideo) echo 'largeBox';?>">
    <div class="media  <?= ($data['bestAnswer'] == 1) ? '' : 'approved'; ?>  my-answers">
        <div class="media-body">
			<div class="media-information">
                <div class="ago"><?= App7020Helpers::timeAgo($data['answerCreated']); ?> ago</div>
            </div>
            <h4 class="media-heading"><?= Yii::t('app7020', 'Your last Answer to ') . CHtml::link(trim($questionContentStriped), $linkUrl, array('class' => 'answers')) ?></h4>
            <div class="media-content">
                <div class="text"><strong><?php echo Yii::t('app7020', 'Best answer:'); ?></strong> <?php echo strip_tags($data['answerContent']); ?></div>
            </div>
            <footer class="info clearfix">
                <div class="q-a-info">
                    <?= CHtml::link(Yii::t('app7020', 'View question & {count} answers', array('{count}' => $data['answersCount'])), $linkUrl, array('class' => 'answers')) ?>
                </div>
                <div class="like-dislike">
                    <span class="like"><?= $data['likes']; ?></span>
                    <span class="dislike"><?= $data['dislikes']; ?></span>
                </div>
            </footer>
        </div>
    </div>
</div>