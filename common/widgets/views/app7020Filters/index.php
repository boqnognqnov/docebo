<nav id="filterActions" class="app7020FilterGlobalClass" data-for="<?php echo $this->comboListViewId; ?>" data-action="<?php echo ($this->action) ? $this->action : 'false' ?>">
	<ul class="linksWrapper">
		<?php
				//echo "<pre>";
			//print_r($this->links);
		//echo "</pre>";
		?>
		<?php foreach ($this->links as $link):?>		
		<?php if($this->have_permission || $link["title"]=="My Contribute") { ?>
			<li><?php echo CHtml::link((!empty($link['title']) ? $link['title'] : Yii::t('app7020', 'No Title')), (!empty($link['href']) ? $link['href'] : 'javascript:;'), (!empty($link['attr']) ? $link['attr'] : array())); ?></li>
		<?php } ?>
		<?php endforeach; ?>
		<?php if ($this->topics): ?>
			<!--
			<li><?php echo CHtml::link(Yii::t('app7020', 'Topics'), 'javascript:;', array('class' => 'topics')); ?></li>
			-->
		<?php endif; ?>
		<?php if ($this->topicsNew): ?>
			<!--
			<li><?php echo CHtml::link(Yii::t('app7020', 'Topics'), 'javascript:;', array('class' => 'filter_topics')); ?></li>
			-->
		<?php endif; ?>

		<?php if ($this->filters): ?>
			<li><?php echo CHtml::link(Yii::t('app7020', 'Filters'), 'javascript:;', array('class' => 'filter')); ?></li>
		<?php endif; ?>
		<?php if ($this->filtersNew): ?>
			<li><?php echo CHtml::link(Yii::t('app7020', 'Filters'), 'javascript:;', array('class' => 'filter_filters')); ?></li>
		<?php endif; ?>

		<li class="fluid"></li>

		<?php if ($this->toggleGrid): ?>
			<li><?php echo CHtml::link('<i class="fa fa-list"></i><i class="fa fa-th"></i>', 'javascript:;', array('class' => 'toggleGrid', 'data-grid-switch' => $this->toggleGrid)); ?></li>
		<?php endif; ?>

		<?php if ($this->searchEngine): ?>
			<li style="width:100%;" class="resize text-right in"><?php echo CHtml::textfield('search', $this->searchVal, array('class' => 'search', 'placeholder' => 'Search','style'=>'display:table-cell')); ?><i class="fa fa-times"></i></li>
			<li><?php echo CHtml::link('<i class="fa fa-search"></i>', 'javascript:;', array('class' => 'searchBtn')); ?></li>
		<?php endif; ?>
	</ul>

	<!-- HIDDEN HTML -->
	<?php if (!empty($this->filters)): ?>
		<div class="filtersPopoverDOM" style="display: none;">
			<div class="row-fluid">
				<div class="table-row">
					<?php foreach ($this->filters as $key => $value): ?>
						<div class="<?php echo $value['name']; ?>">
							<p><?php echo $value['title']; ?></p>
							<?php
							$id = 0;
							foreach ($value['data'] as $key => $dataValue):
								$subData = ($value['subData'][$key]) ? '<span class="subData">' . $value['subData'][$key] . '</span>' : '';
								echo CHtml::radioButton($value['name'], (0 == $key) ? true : false, array('data-id' => $value['name'] . '_' . $id, 'value' => $key, 'id' => ''));
								echo CHtml::label($dataValue . $subData, '', array('data-for' => $value['name'] . '_' . $id++));
								?>
								<br />
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="table-row widget-fancy" data-custom="treeWidgetContainer">
				</div>
			</div>
			<div class="row-fluid">
				<div></div>
				<div class="formButton">
					<?php echo CHtml::button(Yii::t('app7020', 'Apply')); ?>
					<?php echo CHtml::link(Yii::t('app7020', 'Close'), 'javascript:;', array('class' => 'close-dialog black'));?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</nav>
<?php if (!empty($this->filters)): ?>
	<div id="appliedFilters">
		<span class="fControl"><?= Yii::t('app7020', 'Clear filters') ?></span>
		<div class="fList"></div>
	</div>
<?php endif; ?>