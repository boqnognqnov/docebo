	<div <?= $this->id ? 'id="'. $this->id. '"' : '' ?> class="app7020AlertBox <?= $this->hide ? 'hide ' : '' ?>  <?= $this->success ? 'success' : 'error' ?>">
		<i id="app7020FadeOut" class="fa fa-times"></i>
		<div class="app7020AlertBoxContainer">
			<div class="content success"><?= $this->textSuccess ?></div>
			<div class="content error"><?= $this->textError ?></div>
			<?= CHtml::hiddenField('successText', $this->textSuccess) ?>
			<?= CHtml::hiddenField('errorText', $this->textError) ?>
		</div>
	</div>