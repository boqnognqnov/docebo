<?php
/* @var $this GridSearchForm */
/* @var $type string */
/* @var $currentCatalog string */
/* @var $catalogs array */
/* @var $labels array */
/* @var $countCoursesWithoutLabel int */

$backToMyCoursesUrl = Yii::app()->createUrl('site/index', array('opt' => 'mycourses'));
$backToCatalogUrl = Yii::app()->createUrl('site/index', array('opt' => 'catalog'));
$backToMixedUrl = Yii::app()->createUrl('site/index', array('opt' => 'mixed'));

?>
<div id="grid-search-form">
	<?php echo CHtml::beginForm('?r=site/courses', 'get', array(
		'id' => 'course-search',
		'class' => 'ui-search',
	)); ?>
		<div class="row-fluid">
			
			<?php // catalog ---------------------------------
			if ($type == CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG && !empty($catalogs)) : ?>
			<div class="span3">
				<div id="gsf-filter-catalog" class="btn-group gsf-dropdown-filter gsf-dropdown-noborder">
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<span id="gsf-catalog-name"><?php echo $currentCatalog; ?></span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li id="gsf-catalog-all" data-type="">
							<a class="item-text" href="index.php?r=site/index&opt=catalog">
								<span><?php echo Yii::t('standard', 'All catalogs'); ?></span>
							</a>
						</li>
						<?php foreach($catalogs as $catalog) : ?>
							<li style="" id="gsf-catalog-<?php echo $catalog['idCatalogue'] ?>"
								data-type="<?= $catalog['idCatalogue'] ?>">
								<a class="item-text"
								   href="index.php?r=site/index&flt_catalog=<?php echo $catalog['idCatalogue'] ?>&opt=catalog">
									<span><?php echo $catalog['name'] ?></span><br/>
									<span
										class="course-number"><?php echo $catalog['course_count'] . ' ' . Yii::t('standard', '_COURSES'); ?></span>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
			<?php endif; ?>

			<div class="<?php echo ($type == CoursesGrid::$GRIDTYPE_EXTERNAL ? 'span9' : 'span6'); ?> mobile-span2of3 types-picker" data-bootstro-id="bootstroMycoursesFilter">
			<?php
			// my course ----------------------------------
			if ($type == CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES) : ?>
				<div class="row-fluid">
					<div class="span4 mobile-span1of3" <?=Settings::get('mycourses_visibility_activecourses', 'on')=='off' ? 'style="display:none;"' : null; ?> >
						<label class="radio">
						<input type="radio" id="mycourse-type-active" name="mycourse-type"
							<?=($defaultMyCoursesView==CoursesGrid::$FILTER_TYPE_MYCOURSES) ? 'checked' : ''?>
							   value="<?= CoursesGrid::$FILTER_TYPE_MYCOURSES ?>" checked>
							<?php echo Yii::t('dashboard', '_ACTIVE_COURSE'); ?>
						</label>
					</div>

					<div class="span4 mobile-span1of3" <?=Settings::get('mycourses_visibility_completedcourses', 'on')=='off' ? 'style="display:none;"' : null; ?> >
						<label class="radio">
						<input type="radio" id="mycourse-type-completed" name="mycourse-type"
							<?=($defaultMyCoursesView==CoursesGrid::$FILTER_TYPE_COMPLETED) ? 'checked' : ''?>
							   value="<?= CoursesGrid::$FILTER_TYPE_COMPLETED ?>">
							<?php echo Yii::t('standard', 'Completed courses'); ?>
						</label>
					</div>

					<div class="span4 mobile-span1of3" <?=Settings::get('mycourses_visibility_allcourses', 'on')=='off' ? 'style="display:none;"' : null; ?> >
						<label class="radio">
						<input type="radio" id="mycourse-type-all" name="mycourse-type"
							<?=($defaultMyCoursesView==CoursesGrid::$FILTER_TYPE_VIEWALL) ? 'checked' : ''?>
							   value="<?= CoursesGrid::$FILTER_TYPE_VIEWALL ?>">
							<?php echo Yii::t('standard', '_ALL_COURSES'); ?>
						</label>
					</div>
				</div>
				<div class="mobile-clearfix"></div>
			<?php endif; ?>

			<?php
			// catalog ------------------------------------
			if ($type == CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG || $type == CoursesGrid::$GRIDTYPE_EXTERNAL) : ?>
				<div class="row-fluid">
					<div class="span3 mobile-span1of3">
						<label class="radio">
						<input type="radio" id="mycourse-type-active" name="mycourse-type"
							   value="<?= CoursesGrid::$FILTER_TYPE_FULLLIST ?>" checked>
							<?php echo Yii::t('standard', '_ALL_COURSES'); ?>
						</label>
					</div>
					<div class="span2 mobile-span1of3">
						<label class="radio">
						<input type="radio" id="mycourse-type-completed" name="mycourse-type"
							   value="<?= CoursesGrid::$FILTER_TYPE_PAID ?>">
							<?php echo Yii::t('getmoreuser', 'Buy'); ?>
						</label>
					</div>
					<div class="span2 mobile-span1of3">
						<label class="radio">
						<input type="radio" id="mycourse-type-all" name="mycourse-type"
							   value="<?= CoursesGrid::$FILTER_TYPE_FREE ?>">
							<?php echo Yii::t('course', 'Free courses'); ?>
						</label>
					</div>
				</div>
				<div class="mobile-clearfix"></div>
			<?php endif; ?>
			</div>
			<!-- /filter -->

			<?php
			// labels -----------------------------------------
			if (!empty($labels)) : ?>
				<div class="span3">
					<div id="gsf-filter-label" class="btn-group gsf-dropdown-filter">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<span id="gsf-label-name"><?php echo Yii::t('standard', 'All labels'); ?></span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li id="gsf-label-all" data-type="">
								<a class="item-text" href="#">
									<span><?php echo Yii::t('standard', 'All labels'); ?></span>
								</a>
							</li>
							<li id="gsf-label-0" data-type="0">
								<a class="item-text" href="#" style="border-color:#FF8205;">
									<span><?php echo Yii::t('label', 'No label'); ?></span><br/>
									<span
										class="course-number"><?php echo $countCoursesWithoutLabel . ' ' . Yii::t('standard', '_COURSES'); ?></span>
								</a>
							</li>
							<?php foreach($labels as $label) : ?>
								<li style="" id="gsf-label-<?php echo $label['id_common_label'] ?>"
									data-type="<?= $label['id_common_label'] ?>">
									<a class="item-text" style="border-color: <?= $label['color'] ?>" href="#">
										<span><?php echo $label['title'] ?></span><br/>
										<span
											class="course-number"><?php echo $label['courseuser_count'] . ' ' . Yii::t('standard', '_COURSES'); ?></span>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			<?php else: ?>
				<div class="span3">&nbsp;</div>
			<?php endif; ?>

			<!-- old catalog dropdown selector position -->
			<div class="span3 mobile-span1of3">
				<div id="grid-filter-search" class="pull-right">
					<div class="ui-search">
						<div class="input-append">
							<input type="text" class="search-txt" id="gsf-search-text" name="gsf-search-text"
								   placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" value="<?php echo Yii::app()->request->getParam('flt_query', ''); ?>" />
						<button id="gsf-btn-search" type="submit" class="search-btn"><i class="icon-search"></i>
						</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php echo CHtml::endForm(); ?>
</div>
<script type="text/javascript">
	$(function() {
		// label
		<?php $flt_label = Yii::app()->request->getParam('flt_label', '0');
		if ($flt_label) : ?>
		var label_name = $('#gsf-label-<?php echo $flt_label; ?> span').first().html();
		$('#gsf-label-name').html(label_name);
		<?php endif; ?>
		$('#gsf-filter-label ul li a').on('click', function(e) {
			// replace the main label with the one currently selected
			var label_name = $(this).find("span").first().html();
			$('#gsf-label-name').html(label_name);
		});
		//catalog
		$('#gsf-filter-catalog ul li a').on('click', function(e) {
			var catalog_name = $(this).find("span").first().html();
			$('#gsf-catalog-name').html(catalog_name);
		});
	});
</script>