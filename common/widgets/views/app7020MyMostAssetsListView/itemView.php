<?php
	$itemIndex = $widget->dataProvider->getPagination()->getPageSize()*$widget->dataProvider->getPagination()->getCurrentPage()+ ++$index;
?>
<div class="itemGrid <?php echo $spanSizeType; ?>" data-idObject="<?php echo $data['id_asset'] ?>">
	<a href="<?php echo $data['url']; ?>">
		<div class="counterIndex"><?php echo $itemIndex; ?></div>
		<div class="thumb-wrapper">
			<img src="<?php echo $data['thumbnail']; ?>" />

			<!--Show only if asset type is 1 (video)-->
			<?php if ($data['type'] == App7020Assets::CONTENT_TYPE_VIDEO): ?>
				<span class="time"><?php echo $data['duration']; ?></span>
			<?php endif; ?>

			<!--Asset type as name-->
			<span class="type"><?php echo $data['typename']; ?></span>
		</div>
		<div class="content-wrapper">
			<p class="title"><?php echo $data['title']; ?></p>
			<div class="meta-info-wrapper clearfix">
				<?php
				if (!empty($data['sections'])):
					foreach ($data['sections'] as $k => $section):
						if(++$k%2 > 0 && $k != 1){
							echo '<div class="clearfix"></div>';
						}
						?>
						<div class="section">
							<div class="counter">
								<span><?php echo (isset($section['counter']) && $section['rating'] === true) ? sprintf("%6.1f", $section['counter']) : $section['counter']; ?></span>
								<?php
								if($section['rating'] === true){
									for($i = 1; $i <= 5; $i++){
										if($i <= $section['counter']){
											echo '<i class="fa fa-star"></i>';
										}else{
											if($section['counter']-($i-1) > 0.25 && $section['counter']-($i-1) < 0.75){
												echo '<i class="fa fa-star-half-o"></i>';
											}else if($section['counter']-($i-1) >= 0.75){
												echo '<i class="fa fa-star"></i>';
											}else{
												echo '<i class="fa fa-star-o"></i>';
											}
										}
									}
								}
								?>
							</div>
							<div class="title"><?php echo ($section['title']) ? $section['title'] : ''; ?></div>
						</div>
						<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</a>
</div>