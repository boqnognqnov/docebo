<div class="customGuruActivity-widget <?= $this->containerClass ?>" data-is-tab="<?= ($this->isTab) ? 'true' : 'false' ?>">
	<div class="wrapper">
		<ul class="clearfix" <?= ($this->isTab) ? 'role="tablist"' : '' ?>>
			<li class="<?= ($this->autoSelect == 'requestsSatisfy') ? 'active' : '' ?><?= (!$this->autoSelect) ? 'active' : '' ?>">
				<a href="<?= ($this->isTab) ? '#activityRequestsSatisfy' : Docebo::createAppUrl('app7020:guruDashboard/index', array('tab' => 'activityRequestsSatisfy')); ?>" data-color="orange" <?= ($this->isTab) ? 'aria-controls="activityRequestsSatisfy" role="tab" data-toggle="tab"' : '' ?>>
					<div class="icon">
						<i class="fa fa-bullhorn"></i>
					</div>
					
					<div class="labels">
						<p class="count"><?= $orangeTab ?></p>
						<p class="title"><?= Yii::t('app7020', '<span>Requests</span>') ?></p>
						<p class="subTitle"><?= Yii::t('app7020', 'To satisfy') ?></p>
					</div>
				</a>
			</li>
			<li class="<?= ($this->autoSelect == 'activityAssetsReview') ? 'active' : '' ?>">
				<a href="<?= ($this->isTab) ? '#activityAssetsReview' : Docebo::createAppUrl('app7020:guruDashboard/index', array('tab' => 'activityAssetsReview')); ?>" data-color="green" <?= ($this->isTab) ? 'aria-controls="activityAssetsReview" role="tab" data-toggle="tab"' : '' ?>>
					<div class="icon">
						<i class="fa fa-pencil"></i>
					</div>
					
					<div class="labels">
						<p class="count"><?= $greenTab ?></p>
						<p class="title"><?= Yii::t('app7020', '<span>Assets</span> To') ?></p>
						<p class="subTitle"><?= Yii::t('app7020', 'review/approve') ?></p>
					</div>
				</a>
			</li>
			<li class="<?= ($this->autoSelect == 'activityQuestions') ? 'active' : '' ?>">
				<a href="<?= ($this->isTab) ? '#activityQuestions' : Docebo::createAppUrl('app7020:guruDashboard/index', array('tab' => 'activityQuestions')); ?>" data-color="blue" <?= ($this->isTab) ? 'aria-controls="activityQuestions" role="tab" data-toggle="tab"' : '' ?>>
					<div class="icon">
						<i class="fa fa-comments-o"></i>
					</div>
					
					<div class="labels">
						<p class="count"><?= $blueTab ?></p>
						<p class="title"><?= Yii::t('app7020', '<span>Questions</span>') ?></p>
						<p class="subTitle"><?= Yii::t('app7020', 'To answer') ?></p>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>