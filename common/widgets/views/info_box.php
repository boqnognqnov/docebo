<?php /* @var $this InfoBox */ ?>
<div class="info-box <?= $this->class ?>">
	<table>
		<tr>
			<td style="vertical-align: middle;"><i class="<?= $this->iconClass ?>"></i></td>
			<td>&nbsp; </td>
			<td>
				<?= $this->text  ?>
			</td>
		</tr>
	</table>
</div>
