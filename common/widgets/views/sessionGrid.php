<?php
/* @var $this SessionGrid */
/* @var $id string */
/* @var $showExpiredSessions bool */
/* @var $puFilter bool */

	$idCourse = $this->course->getPrimaryKey();

?>

<div id="widget-session-grid-<?= $id ?>">

    <?php

    if (!empty($this->title))
        echo CHtml::tag('h3', array('class'=>'title-bold'), $this->title);
    ?>

    <form class="ajax-grid-form session-grid-search-form">
        <div class="filters-wrapper">
            <table class="filters">
                <tbody>
                <tr>
                    <td class="table-padding">
                        <table class="table-border">
                            <tr>
                                <td class="group">
                                    <?php echo ucfirst(Yii::t('statistic', '_FOR_month')); ?>
                                </td>
                                <td class="group">
                                    <?php
                                    $_monthsList = array(0 => Yii::t('standard', '_ALL'));
                                    $_currentMonth = date("m");
                                    for ($i=1; $i<=12; $i++)
                                        $_monthsList[$i] = Yii::t('standard', '_MONTH_'.str_pad(''.$i, 2, "0", STR_PAD_LEFT));
                                    echo CHtml::dropDownList('selectMonth', 0, $_monthsList, array('class' => 'searchfilter-selectMonth'));
                                    ?>
                                </td>
                                <td class="group">
                                    <?php echo ucfirst(Yii::t('standard', '_YEAR')); ?>
                                </td>
                                <td class="group">
                                    <?php
                                    $_yearsList = array(0 => Yii::t('standard', '_ALL'));
                                    $_currentYear = date("Y");
                                    $_tmpList = $this->course->getSessionsYears();
                                    rsort($_tmpList);
                                    foreach ($_tmpList as $_year)
                                        $_yearsList[$_year] = $_year;
                                    echo CHtml::dropDownList('selectYear', 0, $_yearsList, array('class' => 'searchfilter-selectYear'));
                                    ?>
                                </td>
                                <td class="group" style="width: 60%;">
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="input-wrapper">
                                                    <input  data-url="<?= $this->autocompleteUrl ?>"
                                                            data-type="core"
                                                            class="typeahead"
                                                            data-course_id="<?= $idCourse ?>"
                                                            id="advanced-search-session"
                                                            autocomplete="off"
                                                            type="text"
                                                            name="searchText"
                                                            placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"
                                                            data-autocomplete="true" />
                                                    <span class="search-icon"></span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>


    <!--sessions grid-->
    <div class="grid-wrapper">
        <?php

        $_columnList = array();

        $_columnList[] = array(
            'name' => 'id_session',
            'type' => 'raw',
            'value' => 'CHtml::radioButton("'.$idCourse.'-id_session", false, array("value"=>$data->id_session))',
            'header' => '',
            'headerHtmlOptions' => array(
                'class'=>'asdxx'
            )
        );

        $_columnList[] = array(
            'name' => 'name',
            'type' => 'raw',
            'value' => '$data["name"];',
            'header' => Yii::t('standard', '_NAME'),
            'htmlOptions' => array(
                'class' => 'text-colored'
            )
        );

        $_columnList[] = array(
            'name' => 'date_begin',
            'type' => 'raw',
            'value' => '$data->date_begin',
            'header' => Yii::t('standard', '_START')
        );

        $_columnList[] = array(
            'name' => 'date_end',
            'type' => 'raw',
            'value' => '$data->date_end',
            'header' => Yii::t('standard', '_END')
        );

        $_columnList[] = array(
            'type' => 'raw',
            'value' => '$data->renderLocations();',
            'header' => Yii::t('classroom', 'Location')
        );

        $dp = LtCourseSession::model()->dataProvider($idCourse, $showExpiredSessions, $puFilter);
        if (false === $this->enablePagination) {
        	$dp->pagination = false;
        }
        
        
        $this->widget('DoceboCGridView', array(
        	'ajaxUrl' => $this->ajaxUrl,	
            'id' => 'sessions-management-grid_'.$idCourse,
            'htmlOptions' => array('class' => 'grid-view'),
            'dataProvider' => $dp,
            'columns' => $_columnList,
            'template' => '{items}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'enablePagination' => $this->enablePagination,
        	'pager' => array(
        		'class' => 'DoceboCLinkPager',
        		'maxButtonCount' => 8,
        	),
            'ajaxUpdate' => 'sessions-management-grid-all-items',
            'beforeAjaxUpdate' => 'function(id, options) {
        		options.ajaxType = \'POST\';
            }',
            'afterAjaxUpdate' => 'function(id, data){
                $(\'.modal :radio\').styler({});
                $(\'a[rel="tooltip"]\').tooltip();
            }'
        )); ?>
    </div>
</div>


<script type="text/javascript">
    (function(){

        Docebo.log('Loading sessionGrid for idCourse <?= $idCourse ?>');

        var oSessionGrid = {
            $widget: null,

            update: function() {
            	Docebo.log('Update Grid for course <?= $idCourse ?>');
                $.fn.yiiGridView.update('sessions-management-grid_<?= $idCourse ?>', {
                    data: oSessionGrid.$widget.find(".session-grid-search-form").serialize()
                });
            },

            init: function() {

                <?php if (!empty($this->autocompleteUrl)) : ?>
                applyTypeahead(oSessionGrid.$widget.find('.typeahead'));
                <?php endif; ?>

                oSessionGrid.$widget.find('.searchfilter-selectMonth').bind('change', oSessionGrid.update);
                oSessionGrid.$widget.find('.searchfilter-selectYear').bind('change', oSessionGrid.update);
                oSessionGrid.$widget.find('.session-grid-search-form').bind('submit', oSessionGrid.update);

                oSessionGrid.$widget.find(':radio').styler({});

            },
            ready: function() {
                oSessionGrid.$widget = $('#widget-session-grid-<?= $id ?>');
                Docebo.log('Grid Ready. ID=<?= $id ?>');
                oSessionGrid.init();
            }
        };

        $(document).ready(oSessionGrid.ready);

    })();
</script>