<?php /* @var $this DatePicker */ ?>
<div class="date-picker-widget-container form-inline<?= $this->class ? ' ' . $this->class : '' ?>" style="display: inline-block;">
	<label class="control-label">
		<?= (!$this->labelOnRight) 							? $this->label 	: '' ?>
		<?= ($this->stackLabel && !$this->labelOnRight) 	? '<BR/>' 		: '' ?>
		<div id='<?= $this->id ?>' class='datepickers input-append date' data-date-format='<?= $this->dateFormat ?>'>
			<div>
				<?php echo CHtml::textField($this->fieldName, $this->value, $this->htmlOptions); ?>
				<span class="add-on"><i class="<?= $this->iconClass ?>"></i></span>
			</div>
		</div>
		<?= ($this->labelOnRight) ? $this->label : '' ?>
	</label>
</div>	
<script type="text/javascript">
	$(function(){
		$('#<?= $this->htmlOptions["id"] ?>').bdatepicker(<?= json_encode($this->datePickerOptions) ?>);
	});
</script>