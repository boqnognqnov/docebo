<?
/* @var $this AppsGrid */
?>
<h3 class="content-title"><?=Yii::t('apps', 'Extend and Integrate your Docebo!')?></h3>

<p></p>

<h3 class="promoted-apps-header"><?=Yii::t('apps', 'Top Docebo Extensions')?></h3>

<? if($this->featuredApps): ?>
<div class="row-fluid promoted-apps">
	<? foreach($this->featuredApps as $featuredApp): ?>
		<?
		$userLang = Yii::app()->getLanguage();
		if(isset($featuredApp['lang'][$userLang])){
			$langData = $featuredApp['lang'][$userLang];
		}elseif(isset($featuredApp['lang']['en'])){
			$langData = $featuredApp['lang']['en'];
		}else{
			$langData = false;
		}

		$appTitle = $langData['title'] ? $langData['title'] : null;
		$appDescription = $langData['short_description'] ? $langData['short_description'] : null;

		$hasSettings = false;
		if ($featuredApp['is_installed']){
			$plugin_name = ucfirst($featuredApp['internal_codename']).'App';
			// Import App class, check for 'has settings'
			Yii::import("plugin.$plugin_name.$plugin_name");
			$hasSettings = $plugin_name::$HAS_SETTINGS;
		}
		?>
		<div class="span4">
			<div class="promoted-app">
				<img class="badge-img" alt="" src="<?=Yii::app()->theme->baseUrl?>/images/top-app.png">

				<div class="premium-img" style="background-image: url('<?=$featuredApp['image']?>');"></div>

				<h2><?=$appTitle?></h2>

				<p class="descr">
					<?=$appDescription?>
				</p>

				<div class="more">
					<? $featuredAppMoreLink = Yii::t('standard', 'Discover more...'); ?>
					<? if($featuredApp['is_enterprise']): ?>
						<a href="<?=Yii::app()->createUrl('app/index', array('#' => 'fullpage-ecs'));?>"><?=$featuredAppMoreLink?></a>
					<? else: ?>
						<?php if ($featuredApp['is_installed']) : ?>
							<?php if ($featuredApp['is_pending_deactivation'] == 1): ?>
								<a class="open-dialog btn-enable-app" href="<?= Yii::app()->createUrl('app/axReactivateApp', array('id' => $featuredApp['app_id'])) ?>" data-dialog-class="metro app-details" title="<?= Yii::t('standard', '_REACTIVATE') ?>"><?=$featuredAppMoreLink?></a>
							<?php elseif ($featuredApp['is_deactivated'] == 1): ?>
								<a class="open-dialog btn-enable-app" href="<?= Yii::app()->createUrl('app/axReactivateOneTimeApp', array('id' => $featuredApp['app_id'])) ?>" data-dialog-class="metro app-details" title="<?= Yii::t('standard', '_REACTIVATE') ?>"><?=$featuredAppMoreLink?></a>
							<?php elseif ($hasSettings && $plugin_name::settingsUrl()): ?>
								<a class="configure" href="<?= $plugin_name::settingsUrl() ?>" class="btn-configure-app" title="<?= Yii::t('apps', 'Configure') ?>"><?=$featuredAppMoreLink?></a>
							<?php endif; ?>
						<?php elseif ($featuredApp['market_type'] == 'free'): ?>
							<a class="open-dialog btn-configure-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $featuredApp['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Install app') ?>"><?=$featuredAppMoreLink?></a>
						<?php elseif (Docebo::isTrialPeriod()): ?>
							<a class="open-dialog btn-start-trial" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $featuredApp['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Start free trial') ?>"><?=$featuredAppMoreLink?></a>
						<?php else: ?>
							<a class="open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $featuredApp['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('userlimit', 'Buy now') ?>"><?=$featuredAppMoreLink?></a>
						<?php endif; ?>
					<? endif; ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	<? endforeach; ?>

	<div class="clearfix"></div>
</div>
<? /*if($this->featuredApps)*/ endif;  ?>

<div class="clearfix"></div>

<p></p>

<h3 class="cover-title"><?=$this->coverTile?></h3>


<p></p>

<? if(($this->action=='myapps' && !count($this->apps))): ?>
	<div class="myapps-empty">
		<i><?=Yii::t('apps', "You haven't activated any apps yet")?></i>
	</div>
<? endif; ?>

<?php
/* @var $this AppsGrid */
if($this->apps){
	foreach ($this->apps as $item) {
		if (is_string($item)) {
			echo $item;
		}
		else {
			$this->widget('common.widgets.AppWidget', array(
				'app' => $item,
				'isECSInstallation' => $isECSInstallation
			));
		}
	}
}
?>