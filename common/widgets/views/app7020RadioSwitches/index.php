<?php if(!empty($this->switches)): ?>
<div class="app7020RadioSwitches">
	<?php
	foreach ($this->switches as $switch) {
		$list[$switch[0]] = ($switch[1]) ? $switch[1] : Yii::t('app7020' ,'No Title');
	}
	echo CHtml::radioButtonList(
			'switch-' . str_replace(array('.', '_'), '', microtime(true)),
			($this->select !== null ? $this->select : key($list)),
			$list,
			array('separator' => '', 'data-comboListId' => $this->comboListViewId));
	?>
</div>
<?php endif; ?>