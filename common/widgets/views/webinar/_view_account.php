<?php
/* @var $data WebinarToolAccount */
/* @var $widget WebinarAccountsEditor */
/* @var $editAccountUrl string */
/* @var $deleteAccountUrl string  */
/* @var $enableMultiAccount boolean */
?>
<div class="row" data-page-id="<?php echo $data->id_account; ?>" style="margin-left: 0px;">
	<div class="title"><?= $data->name ?></div>
	<div class="actions">
		<ul>
			<li class="edit">
				<?php echo CHtml::link('<span class="i-sprite is-edit"></span>', $editAccountUrl.'&id_account='.$data->id_account, array(
					'class' => 'edit-action open-dialog',
					'data-dialog-class' => 'modal-edit-webinar-account',
					'removeOnClose' => 'true',
					'closeOnOverlayClick' => 'true',
					'closeOnEscape' => 'true'
				));
				?>
			</li>
			<?php if($enableMultiAccount): ?>
			<li class="delete">
				<?php echo CHtml::link("", "", array(
						'class' => 'ajaxModal delete-action',
						'data-toggle' => 'modal',
						'data-modal-class' => 'delete-node',
						'data-modal-title' => Yii::t('standard', '_DEL'),
						'data-buttons' => json_encode(array(
							array('type' => 'submit', 'title' => Yii::t('standard', '_CONFIRM')),
							array('type' => 'cancel', 'title' => Yii::t('standard', '_CANCEL')),
						)),
						'data-url' => $deleteAccountUrl.'&id_account='.$data->id_account,
						'data-after-loading-content' => 'hideConfirmButton();',
						'data-after-submit' => 'updateWebinarAccountsContent',
					)); ?>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</div>