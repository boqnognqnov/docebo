<?php
/* @var $this WebinarAccountsEditor */
?>

<div class="items-sortable-wrapper values">
	<h6><?php echo Yii::t('webinar', 'Account Name'); ?></h6>
	<?php
	if($this->enableMultiAccount || !$this->isSetAccount)
		echo CHtml::link(Yii::t('webinar', 'New Account'),
			$this->editAccountUrl,
			array(
				'class' => 'branding-external-page open-dialog',
				'data-dialog-class' => 'modal-edit-webinar-account',
				'removeOnClose' => 'true',
				'closeOnOverlayClick' => 'true',
				'closeOnEscape' => 'true',
			)
		);

	$this->widget('zii.widgets.CListView', array(
		'id' => 'webinar-accounts-management-list',
		'htmlOptions' => array('class' => 'list-view clearfix'),
		'dataProvider' => $this->dataProvider,
		'viewData' => array(
			'editAccountUrl' => $this->editAccountUrl,
			'deleteAccountUrl' => $this->deleteAccountUrl,
			'enableMultiAccount' => $this->enableMultiAccount
		),
		'itemView' => 'common.widgets.views.webinar._view_account',
		'template' => '{items}',
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'ajaxUpdate' => true,
		'afterAjaxUpdate' => 'function(id, data) {
			$("#webinar-accounts-management-list").controls();
		}'
	)); ?>
</div>

<!--  If have to be set only one account it hides button for New Account after account is set  -->
<?php if($this->isSingleAccount): ?>
    <script type="text/javascript">
        $(document).on("dialog2.closed", ".modal.modal-edit-webinar-account", function () {
             $('a.branding-external-page').hide();
        });
    </script>
<?php endif; ?>

<script type="text/javascript">
	$(function () {
		$.fn.updateWebinarAccountsContent = function(data) {
			if (data.html) {
				$('.modal.in .modal-body').html(data.html);
			} else {
				$('.modal.in').modal('hide');
				$.fn.yiiListView.update('webinar-accounts-management-list');
			}
		}

		<?php $id_account = Yii::app()->request->getParam('id_account'); ?>
		<?php if($id_account): ?>
		$('<div/>').dialog2({
			id: 'dialog-edit-webinar-account',
			modalClass: 'modal-edit-webinar-account',
			removeOnClose: true,
			closeOnOverlayClick: true,
			closeOnEscape: true,
			content: '<?=$this->editAccountUrl?>&id_account=<?=$id_account?>'
		});
		<?php endif; ?>
	});
</script>