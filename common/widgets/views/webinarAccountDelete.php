<?php /* @var $this WebinarAccountsEditor */?>
<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'account_delete_form',
	)); ?>

	<p><?php echo Yii::t('webinar', 'Account') . ': "' . $this->accountModel->name . '"'; ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkbox('confirm', false, array('id' => 'WebinarAccount_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($this->accountModel->name)), 'WebinarAccount_confirm_'.$checkboxIdKey); ?>
		<?php echo CHtml::error($this->accountModel, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>