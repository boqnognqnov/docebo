<?php
// TODO
?>
<div class="customDropdown-widget <?= $this->containerClass ?>"  ng-hide="hideControllers[$index]">
    <div class="wrapper">
        <div class="awesomeIcon-dots">
            <ul class="listArray">
				<?php
				if ($this->isAngular) {
					$attr = array();
					foreach ($this->arrayData as $index => $value) {
						$attr[].= $index . '="' . $value['action'] . '"';
						?>
						<li>
							<div class="link link-node-<?= $index ?>" ng-<?= $value['directive']; ?>="<?= $value['action']; ?>" >
								<?= $value['text'] ?>
							</div>
						</li>
						<?php
					}
				} else {
					echo '<li><a class="disable"><i>' . Yii::t('app7020', 'List is empty') . '</i></a></li>';
				}
				?>
            </ul>
        </div>
    </div>
</div>