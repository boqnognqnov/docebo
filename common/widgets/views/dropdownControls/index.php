<?php
// TODO
?>
<div class="customDropdown-widget <?= $this->containerClass ?>">
    <div class="wrapper">
        <div class="awesomeIcon-dots">
            <ul class="listArray">
				<?php
				if (!empty($this->arrayData)) {
					foreach ($this->arrayData as $index => $value) {
						?>
						<li>
							<div class="link link-node-<?= $index ?>" data-url="<?php echo ($value['url']) ? $value['url'] : 'javascript:;'; ?>" data-method="<?php echo $value['method']; ?>" <?php if(isset($value['dialog-title'])) { echo 'data-dialog-title="'.$value['dialog-title'].'"';} ?>>
								<?= $value['text'] ?>
							</div>
						</li>
						<?php
					}
				}  else {
					echo '<li><a class="disable"><i>' . Yii::t('app7020', 'The list is empty') . '</i></a></li>';
				}
				?>
            </ul>
        </div>
    </div>
</div>