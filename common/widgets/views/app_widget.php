<?php
/* @var $this AppWidget */
/* @var $app Array */

$hasSettings = false;
if ($this->isInstalled){
	// Import App class, check for 'has settings'
	Yii::import("plugin.$plugin_name.$plugin_name");
	$hasSettings = $plugin_name::$HAS_SETTINGS;
}
$appTitle = $this->lang['title'];
?>

<div class="single-app-holder <?=$this->isFreeApp ? 'free' : 'paid';?>">

	<div class="pull-right">
		<!-- Actions buttons area -->
		<? if(!$this->isInstalled): ?>
			<?php if($this->isFreeApp): ?>
				<div class="pull-left">
					<div class="free"><?=Yii::t('app', 'Free')?> !</div>
				</div>
				<a class="btn-docebo big blue open-dialog btn-configure-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Install app') ?>"><?= Yii::t('apps', 'Install app') ?></a>
			<?php else: ?>
				<?php if(!$this->isContactUsOnly): ?>
					<?php if($this->isInTrialPeriod): ?>
						<div class="pull-left">
							<div class="trial">
								<?=Yii::t('apps', '{days}-day', array('{days}'=>Docebo::getTrialRemainingDays()))?>
								<br/>
								<?=Yii::t('apps', 'FREE TRIAL')?>
							</div>
						</div>
						<a class="btn-docebo big orange open-dialog btn-start-trial" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Start free trial') ?>"><?= Yii::t('apps', 'Start free trial') ?></a>
					<?php else: ?>
						<?php if($this->isFreeOnESC): ?>
							<?php if($this->isECSInstallation): ?>
								<div class="pull-left">
									<div class="free"><?=Yii::t('app', 'Free')?> !</div>
								</div>
								<a class="btn-docebo big blue open-dialog btn-configure-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Install app') ?>"><?= Yii::t('apps', 'Install app') ?></a>
							<?php else: ?>
								<?php if($this->enableUserBilling === 'on'): ?>
									<div class="pull-left">
										<div class="paid">
											<? if($app['pricing'] && is_array($app['pricing'])): ?>
												<b><?=($app['pricing']['currency']=='eur') ? '&euro;' : '&dollar;';?><?=round($app['pricing']['price'], 2)?></b><br/>
												<?=Yii::t('order', '+ VAT')?>/<?=($app['pricing']['payment_cycle']=='yearly')? Yii::t('standard', '_YEAR') : Yii::t('classroom', 'Monthly');?>
											<? endif; ?>
										</div>
									</div>
									<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
									   title="<?= Yii::t('userlimit', 'Buy now') ?>"><?= Yii::t('userlimit', 'Buy now') ?></a>
								<?php else: ?>
									<?php if($this->isReseller): ?>
										<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
										   title="<?= Yii::t('standard', 'Contact us') ?>"><?= Yii::t('standard', 'Contact us') ?></a>
									<?php else: ?>
										<a class="btn-docebo big black open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
										   title="<?= Yii::t('apps', 'Discover More') ?>"><?= Yii::t('apps', 'Discover More') ?></a>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>
						<?php else: ?>
							<?php if($this->enableUserBilling === 'on'): ?>
								<div class="pull-left">
									<div class="paid">
										<? if($app['pricing'] && is_array($app['pricing'])): ?>
											<b><?=($app['pricing']['currency']=='eur') ? '&euro;' : '&dollar;';?><?=round($app['pricing']['price'], 2)?></b><br/>
											<?=Yii::t('order', '+ VAT')?>/<?=($app['pricing']['payment_cycle']=='yearly')? Yii::t('standard', '_YEAR') : Yii::t('classroom', 'Monthly');?>
										<? endif; ?>
									</div>
								</div>
								<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
								   title="<?= Yii::t('userlimit', 'Buy now') ?>"><?= Yii::t('userlimit', 'Buy now') ?></a>
							<?php else: ?>
								<?php if($this->isReseller): ?>
									<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
									   title="<?= Yii::t('standard', 'Contact us') ?>"><?= Yii::t('standard', 'Contact us') ?></a>
								<?php else: ?>
									<a class="btn-docebo big black open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
									   title="<?= Yii::t('apps', 'Discover More') ?>"><?= Yii::t('apps', 'Discover More') ?></a>
								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>
				<?php else: ?>
					<?php if($this->enableUserBilling === 'on'): ?>
						<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
						   title="<?= Yii::t('standard', 'Contact us') ?>"><?= Yii::t('standard', 'Contact us') ?></a>
					<?php else: ?>
						<?php if($this->isReseller): ?>
							<a class="btn-docebo big green open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
							   title="<?= Yii::t('standard', 'Contact us') ?>"><?= Yii::t('standard', 'Contact us') ?></a>
						<?php else: ?>
							<a class="btn-docebo big black open-dialog btn-buy-app" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail"
							   title="<?= Yii::t('apps', 'Discover More') ?>"><?= Yii::t('apps', 'Discover More') ?></a>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		<? endif; ?>

		<?php if ($this->isInstalled) : ?>

			<? if($this->isEnterprise):
				// If enterprise app, it has two states (ERP backup_subscription.pending_enterprise):
				// - pending state (a few days until we do some manual changes; pending_enterprise=1)
				// - completely activated (pending_enterprise=0)
				?>

				<? if(!isset($this->app['pending_enterprise']) || $this->app['pending_enterprise'] == 1 ) : ?>
				<a class="btn-docebo big green open-dialog" href="<?= Yii::app()->createUrl('app/axAppDetails', array('id' => $app['app_id'])) ?>" data-dialog-class="metro wide app-details" rel="dialog-app-detail" title="<?= Yii::t('apps', 'Pending Activation') ?>"><?= Yii::t('apps', 'Pending Activation') ?></a>
			<? else: // Enterprise app is COMPLETELY ACTIVE, don't show any action buttons here ?>

			<? endif; ?>

			<? else: // All other apps (installed, not ECS) ?>
				<? if(!$app['is_pending_deactivation']): ?>
					<div class="pull-left installed-app-action">
						<?php if (empty($app['is_pending_deactivation']) && empty($app['is_deactivated'])) : ?>
							<?php if ($hasSettings) : ?>
								<?php if ($plugin_name::settingsUrl()) : ?>
									<a class="configure" href="<?= $plugin_name::settingsUrl() ?>" class="btn-configure-app" title="<?= Yii::t('apps', 'Configure') ?>"><div class="p-sprite gear"></div></a>
								<?php else : ?>
									<a class="configure" href="<?= Yii::app()->createUrl($plugin_name.'/'.$plugin_name.'/settings') ?>" class="btn-configure-app" title="<?= Yii::t('apps', 'Configure') ?>"><div class="p-sprite gear"></div></a>
								<?php endif; ?>
							<?php else:?>

							<?php endif; ?>

						<?php endif; ?>

						<? if(!$app['is_pending_deactivation'] && $hasSettings): ?>
							&nbsp;
						<? endif; ?>

						<?php if ($app['is_deactivated'] == 1) : ?>
							<a class="btn-docebo big green open-dialog btn-enable-app" href="<?= Yii::app()->createUrl('app/axReactivateOneTimeApp', array('id' => $app['app_id'])) ?>" data-dialog-class="metro app-details" title="<?= Yii::t('standard', '_REACTIVATE') ?>"><?= Yii::t('standard', '_REACTIVATE') ?></a>
						<?php elseif(!empty($app['is_locked_by_dependency']) && ($app['is_locked_by_dependency'] == 1)): ?>
							<a class="remove open-dialog btn-remove-dependency" href="<?= Yii::app()->createUrl('app/axRemoveDependency', array('id' => $app['app_id'])) ?>" data-dialog-class="metro app-details" title="<?= Yii::t('standard', '_DEL') ?>"><div class="p-sprite cross-red"></div></a>
						<?php else: ?>
							<a class="remove open-dialog btn-remove-app" data-dialog-class="metro app-details" href="<?= Yii::app()->createUrl('app/axRemoveApp', array('id' => $app['app_id'])) ?>" title="<?= Yii::t('standard', '_DEL') ?>"><div class="p-sprite cross-red"></div></a>
						<?php endif; ?>
					</div>
					<div class="btn-docebo big black "><?=Yii::t('standard', '_ACTIVE')?></div>
				<? else: ?>
					<a class="btn-docebo big green open-dialog btn-enable-app" href="<?= Yii::app()->createUrl('app/axReactivateApp', array('id' => $app['app_id'])) ?>" data-dialog-class="metro app-details" title="<?= Yii::t('standard', '_REACTIVATE') ?>"><?= Yii::t('standard', '_REACTIVATE') ?></a>

					<?php if ($app['market_type'] == 'subscription') : ?>
						<div class="expire-date">
							<?= Yii::t("standard", "_EXPIRATION_DATE") . ' ' . date('j M Y', strtotime($app['subscription_expire_date'])) ?>
						</div>
					<?php endif; ?>
				<? endif; ?>
			<? endif; ?>
		<?php endif; ?>
	</div>

	<h4>
		<?=$appTitle?>
	</h4>
	<p class="description"><?= $this->lang['short_description'] ?></p>

</div>