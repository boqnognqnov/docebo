	<?php if( !PluginManager::isPluginActive('WhitelabelApp') ) : ?>
    <?php $currentLanguage = Yii::app()->getLanguage(); ?>
    <?php ($currentLanguage == 'it' ? $it = 'it/' : $it = ''); ?>
    <?php ($currentLanguage != 'it' && $currentLanguage != 'en' ? $int = '-international' : $int = ''); ?>

    <p class="privacy-policy" style="display: inline; margin-right: 22px;">
        <a href="https://www.docebo.com/<?=$it?>docebo-privacy-policy<?=$int?>/" target="_blank">
            <?=Yii::t('standard', 'Privacy Policy')?>
        </a>
    </p>
<?php endif; ?>

<p class="powered-by" style="display: inline">
	<?php if ($customText !== null) : ?>
		<? if (Settings::get('whitelabel_footer') == CoreLangWhiteLabelSetting::TYPE_FOOTER_URL) : ?>
			<?php
			// show customized url, only if the user is logged in!
			if (!Yii::app()->user->getIsGuest())
				Yii::app()->event->raise('BeforeFooterRender', new DEvent($this, array()));
			else { ?>
				<span><?='<div class="allowOlUlNumbering">'.$customText.'</div>'; ?></span>
<!--				<a href="--><?php //echo (strpos($customSite, 'http://') === false ? 'http://' : '')
//						. $customSite; ?><!--" onclick="window.open(this.href);return false;">--><?php //echo $customText; ?><!--</a>-->
			<?php } ?>
		<? else: ?>
			<span><?='<div class="allowOlUlNumbering">'.$customText.'</div>'; ?></span>
		<? endif; ?>
	<?php else : ?>
		<a href="http://www.docebo.com/" onclick="window.open(this.href);return false;">&copy; Powered by Docebo <?//= $this->year ?></a>
	<?php endif; ?>
</p>