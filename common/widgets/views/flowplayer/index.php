<?php
// Container ID is set in the widget's INIT method
$containerId = $this->containerId;
?>

<div class="f-player-container" style="overflow-y: hidden;" data-has-cue="<?php echo!empty($this->cuepoints) ? 'true' : 'false'; ?>" >
	<style>
		.box.black{
			position: relative;
		}

		#myOverlay {
			width: 250px;
			padding: 15px;
			background: #333 url(/media/img/global/gradient/h150.png) repeat-x;
			color: #fff;
			border: 2px solid #fff;
			outline: 1px solid #000;
			-moz-outline-radius: 4px;
			position: absolute;
			bottom: 15%;
			left:5%;
			margin-top:-80px;
			z-index: 90000;
			display: none;
			opacity: 0;
		}

		.tooltipShow {			
			display: block!important;
			opacity: 1!important;
			transition: opacity 3.5s;
		}
		#myOverlay h3 {
			margin:0px;
			color:#ff7;
		}
	</style>
	<div id="<?= $containerId ?>" <?php echo isset($this->content)? "data-content-id=".$this->content->id : "" ?>>
		<?php if(isset($this->content)){ ?>
			<div class="docebo-player-tooltips-container">
				<div style="position: relative; height: 100%;width: 100%;display: table-cell;vertical-align: bottom;">
					<?php
					$this->widget('common.widgets.Tooltips', array('content' => $this->content));
					?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		var containerId = '<?= $containerId ?>';
		var container = document.getElementById(containerId);
		var bookmarkPointer = <?= json_encode($bookmark) ?>;

		flowplayer.conf = {
			swf: '<?= Flowplayer::getFlowplayerJSUrl() . "flowplayer.swf" ?>',
			swfHls: '<?= Flowplayer::getFlowplayerJSUrl() . "flowplayerhls.swf" ?>'
		};
		// Attach flowplayer to selected container
		var fpObject = flowplayer(container, <?= $player_Json ?>);

		// Do some work on Flowplayer READY state
		fpObject.on("ready", function (event) {
			fpObject.on("cuepoint", function(){
				//console.log("CSKA");
			});
			// Bookmark available?
			if (bookmarkPointer !== false && parseInt(fpObject.video.duration) != parseInt(bookmarkPointer)) {
				fpObject.seek(bookmarkPointer, function () {
					//set the progress bar
					var percent = ((fpObject.video.time) / (fpObject.video.duration)) * 100;
					$('.fp-progress', $('#' + containerId)).css('width', percent + '%');
				});
			}

			// Not "seekable"? (not permitting user to drag the video head forth and back)
			if (Boolean(<?= !$seekable ?>)) {

				var root = $('#' + containerId).closest('.f-player-container');

				// Change the pointer to Default
				$('.flowplayer .fp-buffer, .flowplayer .fp-progress', root).css({
					'cursor': 'default'
				});

				// Prevent dragging the video head 
				fpObject.on("beforeseek", function (e) {
					if (typeof inFlowPlayerOnReady === "undefined" || !inFlowPlayerOnReady)
						e.preventDefault();
				});


				// As usual, IE is the weirdo one: hide the navigation bar in the player, so far we didin't find other solution
				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE ");
				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
					$(".fp-timeline").hide();
				}

				// Prevent seek attempts having any visible effect
				$(".fp-buffer, .fp-progress", root).on("mousedown touchstart", function (e) {
					e.stopPropagation();
				});

				// Do not show touch button on touch devices
				$(root).removeClass("is-touch");
			}

		});

	});
</script>