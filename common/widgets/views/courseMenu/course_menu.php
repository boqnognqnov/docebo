<div class="subnav">
	<ul class="nav nav-pills">
		<?php
				// var_dump($menuItems);
				foreach($menuItems as $itemTest) {
					echo '<li class="dropdown">';
					echo '<a class="dropdown-toggle" data-toggle="dropdown" href="#">' . $itemTest['main']['name'] . '<b class="caret"></b></a>';
					echo '<ul class="dropdown-menu">';
					foreach($itemTest['submenu'] as $sub) {
						echo '<li class=""><a href="'.$sub['link'].'">'.$sub['name'].'</a></li>';
					}
					echo '</ul>';
					echo '</li>';

				}
		?>
	</ul>
</div>
