
<div class="tabsForm-widget <?= $this->containerClass ?>">


    <section id="app7020-tab-widget-container">


        <div class="tabbable tabs-left row-fluid">

            <div class="span2">
				<?php if ($this->menuHeading) : ?>
					<h1><?= $this->menuHeading ?></h1>
				<?php endif; ?>

                <ul class="nav nav-tabs">

					<?php if (!empty($this->menu)) : ?>
						<?php
						$counter = 0;
						?>
						<?php foreach ($this->menu as $menu): ?>
							<?php
							if (isset($this->activeTab) && !empty($this->activeTab)) {
								$classActive = ($this->activeTab == $menu['link-id'] ? 'active' : '');
							} else {
								$counter++;
								$classActive = ($counter == 1 ? 'active' : '');
							}
							?>
							<li class="<?= $classActive ?>"><a href="#<?= $menu['link-id'] ?>" data-toggle="tab"><i class="fa <?= $menu['icon-id'] ?>"></i><?= $menu['name'] ?></a></li>
						<?php endforeach; ?>
					<?php else : ?>
						<li class="<?= $classActive ?>"><a href="#no-data-1" data-toggle="tab"><i class="fa fa-lock"></i>No data 1</a></li>
						<li class="<?= $classActive ?>"><a href="#no-data-2" data-toggle="tab"><i class="fa fa-cog"></i>No data 2</a></li>
					<?php endif; ?>
                </ul>
            </div>

            <div class="tab-content span10">



				<?php if (!empty($this->content)) : ?>
					<?php $counter = 0; ?>
					<?php foreach ($this->content as $content): ?>
						<?php
						if (isset($this->activeTab) && !empty($this->activeTab)) {
							$classActive = ($this->activeTab == $content['link-id'] ? 'active' : '');
						} else {
							$counter++;
							$classActive = ($counter == 1 ? 'active' : '');
						}
						?>

						<div class="tab-pane <?= $classActive ?>" id="<?= $content['link-id'] ?>">
						<?php if (isset($content['contentHeading'])) : ?>
								<h1><?= $content['contentHeading'] ?></h1>
							<?php endif; ?>
							<?php $this->renderPartial($content['viewName'], array('sendDataToView' => $content['sendDataToView'])); ?>
						</div>


	<?php endforeach; ?>
<?php endif; ?>


            </div>

        </div>

    </section>




</div>
