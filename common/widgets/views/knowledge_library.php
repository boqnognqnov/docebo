<div class="main-section knowledgeLibraryWidget" data-change-layout="false" id="knowledgeLibrary"  data-container="root">
	<!--Show App7020 Filters Widget-->
	<?php
	$app7020Filters = array();
	if ($widgetParams['showContribute'] && $have_permission) {
		$app7020Filters['links'][] = array(
			'title' => Yii::t('app7020', 'Contribute!'),
			'attr' => array('class' => 'contribute green-BtnStyle'),
			'href' => Docebo::createAppUrl('app7020:assets/index')
		);
	}
	
	if ($widgetParams['showMyContribute']) {
		$app7020Filters['links'][] = array(
			'title' => Yii::t('app7020', 'My Contribute'),
			'attr' => array('class' => 'contribute blue-BtnStyle'),
			'href' => Docebo::createAppUrl('app7020:knowledgeLibrary/index',array("tab"=>"2"))
		);
	}
	if (!$widgetParams['showTopics']){
		$app7020Filters['topics'] = false;
	}
	if ($widgetParams['showFilters']){
		$app7020Filters['filters'] = $filtersArray;
	}
	if (!$widgetParams['showSearch']){
		$app7020Filters['searchEngine'] = false;
	}
	
	
	if (!$widgetParams['showTumbnailsList']){
		$app7020Filters['toggleGrid'] = false;
	} 
	
	if ($widgetParams['displayMode'] == 'list'){
		$gridColumns = 1;
		if($app7020Filters['toggleGrid'] !== false){
			$app7020Filters['toggleGrid'] = 3;
		}
	} else {
		$gridColumns = 3;
		if($app7020Filters['toggleGrid'] !== false){
			$app7020Filters['toggleGrid'] = 1;
		}
	}
	$app7020Filters["have_permission"] = $have_permission;
	$app7020Filters = array_merge($app7020Filters, array('comboListViewId' => 'recentlyAssetsListView'));
	$this->widget('common.widgets.app7020Filters', $app7020Filters);
	?>

	<!--Show App7020 Content List View Widget-->
	<div class="box-content">
		<?php
		
		$this->widget('common.widgets.app7020ContentListView', array('listParams' => $listParams, 'gridColumns' => $gridColumns, 'extensions' => 'rating'));
		?>
	</div>	

	<!--View full Knowledga Library link-->
	<?php
	if ($widgetParams['showViewFullKnowledgeLibraryLink'] == 1)
		echo CHtml::link(Yii::t("app7020", "View full Knowledga Library"), Docebo::createApp7020Url('knowledgeLibrary/index'), array('class' => 'view-all'));
	?>

</div>