<?php
/**
 * Created by PhpStorm.
 * User: oooo
 * Date: 11/27/2015
 * Time: 3:42 PM
 */
if (App7020Assets::isConvertableDocument($this->arrayData->filename)) {
	$currPage = App7020DocumentTracking::getLastViewedPage($this->arrayData->id, Yii::App()->user->idst);
	$images = App7020DocumentImages::getDocumentImages($this->arrayData->id);
	if ($images) {
		$i = 0;
		$url = App7020DocumentImages::getDocumentImagesUrl($this->arrayData->filename);
		//$docImages = array();
		echo '<div id="owl-wrap">';
		echo '<div class="owl-title">' . $this->arrayData->originalFilename . '</div>';
		echo '<div id="owl-carousel" class="owl-carousel" data-page="' . $currPage[Page] . '" data-objectid="' . $this->arrayData->id . '">';
		foreach ($images as $value) {

			echo '<div class="item"><img src="' . $url . $value['imageName'] . '" alt=""></div>';
		}
		echo '</div>';
		?>
		<div class="custom-owl-nav">
			<a href="javascript:void(0)" class="owl-prev-custom fa fa-arrow-left"></a>
			<ul id="info" class="info">
				<li class="currp"><?= $currPage[Page] + 1; ?></li>
				<li class="allp">/ <strong><?php echo count($images); ?></strong> <?= Yii::t('standard', '_PAGES'); ?></li>
			</ul>
			<div class="expand fa fa-expand"></div>
			<div class="compress fa fa-compress"></div>
			<a href="javascript:void(0)" class="owl-next-custom fa fa-arrow-right"></a>
		</div>
		</div>
		<?php
	}
} elseif ($this->arrayData->contentType == App7020Assets::CONTENT_TYPE_LINKS) {

	if ($youtubeID = App7020WebsiteProceeder::isYoutubeLink($this->arrayData->originalFilename)) {
		?>
		<div class="iframe-wrapper">
			<iframe width="640" height="360" src="https://www.youtube.com/embed/<?php echo $youtubeID ?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<?php
	} elseif ($vimeoId = App7020WebsiteProceeder::isVimeoLink($this->arrayData->originalFilename)) {
		?>
		<div class="iframe-wrapper">
			<iframe src="https://player.vimeo.com/video/<?php echo $vimeoId ?>?title=0&byline=0&portrait=0&badge=0" 
					width="640" height="360" 
					frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>

	<?php } elseif ($wistiaVideo = App7020WebsiteProceeder::isWistiaLink($this->arrayData->originalFilename)) { ?>
		<div class="iframe-wrapper">
			<?php if (!$wistiaVideo['error']) { ?>
				<iframe src="//fast.wistia.net/embed/iframe/<?php echo $wistiaVideo['hashedId']; ?>" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="600" height="366"></iframe>
				<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
			<?php } else { ?>
				<img src="<?php echo $wistiaVideo['errorImg']; ?>">
			<?php } ?>	
		</div>
	<?php } else {
		?>

		<div class="share-link-wrapper">
			<div class="wrapper">
				<a href="<?php echo $this->arrayData->originalFilename ?>" target="_blank" class="content-wrapper">
					<div class="text-wrapper clearfix">
						<div class="thumb">
							<?php App7020Assets::getPublcPreviewThumbnail(App7020Helpers::converObjToArray($this->arrayData), 'original', true); ?>
						</div>
						<div class="textcountainer">
							<p class="sharelinkstittle"><?php echo strip_tags($this->arrayData->title); ?></p>
							<p class="sharelinksdescription" ><?php echo strip_tags($this->arrayData->description); ?></p>
							<p class="sharelinksfilename"><?php echo $this->arrayData->originalFilename; ?></p>
						</div>
					</div>
				</a>
			</div>
		</div>
		<?php
	}
} elseif ($this->arrayData->contentType == App7020Assets::CONTENT_TYPE_IMAGE) {
	$url = App7020DocumentImages::getDocumentImagesUrl($this->arrayData->filename);
	echo '<div id="owl-wrap">';
	echo '<div class="owl-title">' . $this->arrayData->originalFilename . '</div>';
	echo '<div id="owl-carousel" class="owl-carousel">';
	echo '<div class="item">';
	App7020Assets::getPublcPreviewThumbnail(App7020Helpers::converObjToArray($this->arrayData), 'original', true);
	echo '</div>';
	echo '</div>';
	?>
	<div class="custom-owl-nav">
		<ul id="info" class="info">

		</ul>
		<div class="expand fa fa-expand"></div>
		<div class="compress fa fa-compress"></div>
	</div>
	</div>
	<?php
} else {
	?>
	<div data-custom="contentFileItem">

		<div class="contentFileItemRow">
			<div class="contentFileItemVertical">

				<div class="iconType">
					<i class="<?php echo App7020Assets::$contentTypes[$this->arrayData->contentType]['icon']; ?>"></i>
				</div>

				<div class="content">
					<p class="filename"><?= $this->arrayData->originalFilename ?></p>
				</div>

				<div class="iconDownload">
					<i class="fa fa-download"></i>
				</div>
				<div class="downloadText">
					<span>
						<a href="<?= Docebo::createApp7020Url("", array("#" => "/assets/forceDownload/".$this->arrayData->id)) ?>" target="_blank">
							<?php echo Yii::t('app7020', 'Download') ?>
						</a>
					</span>
				</div>

			</div>
		</div>



	</div>
	<?php
}
