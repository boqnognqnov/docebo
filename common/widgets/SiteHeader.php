<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class SiteHeader extends CWidget {

	public $headerType = false;
	public $forceLoggedInLayout = false;

	public $show_logout = true;
	public $buyNowUrl = '';
	public $cartItemsCount = 0;
	public $cartUrl = '';

	public $isSaas 				= false;
	public $isGodadmin 			= false;
	public $isTrial 			= true;
	public $isTrialExpired 		= false;
	public $trialRemainingDays 	= 0;
	public $showTrialTimer		= false;
	public $showUsersInfo		= false;
	public $inGracePeriod 		= false;
	public $activeUsers			= 0;
	public $boughtUsers			= 0;
	public $enable_user_billing = true;
	public $demo_platform 		= false;
	public $lmsOrderInfo        = null;

	private $companyLogoFileName = '';
	private $companyLogoBaseUrl = '';

	private $languages = array();


	/**
	 * Generate HTML for Languages dropdown [<li></li>...]
	 * @return string
	 *
	 */
	public function getLanguagesLiHtml() {
		$result = "";
		$baseUrl = Yii::app()->getBaseUrl(true);
		//check base url, some conditions need to be satisfied in order to generate working urls in the dropdown
		$check = (substr($baseUrl, -4, 4) == '.php');
		if (!$check) {
			$index = count($baseUrl) - 1;
			if ($baseUrl{$index} != '/') { $baseUrl .= '/'; }
		}
		//create list of change language urls
		foreach($this->languages as $language) {
			//prepare complete url
			$href = $baseUrl . '?lang=' . $language['browsercode']; // using browser codes
			if ($language['browsercode'] == Yii::app()->getLanguage()) {
				$text = "<strong>${language['description']}</strong>";
			} else {
				$text = $language['description'];
			}
			$result .= "<li><a href='$href'>" . $text . "</a></li>\n";
		}
		return $result;
	}


	/**
	 * Renders the settings menu in the top menu bar
	 * @return string
	 * @deprecated
	 */
	public function renderSettingsMenu() {}


	/**
	 * Renders the user menu area in the main menu bar
	 * @return string
	 * @deprecated
	 */
	public function renderUserMenu() {}


	/**
	 * @deprecated
	 */
	public function renderCartNavLink() {}


	/**
	 * INIT
	 * @see CWidget::init()
	 */
	public function init() {

		parent::init();

		$this->languages 			= Lang::getLanguages(true);
		$this->show_logout  		= Yii::app()->params['show_logout'];
		$this->buyNowUrl 			= Docebo::createLmsUrl("billing/default/plan");
		if(PluginManager::isPluginActive('EcommerceApp'))
			$this->cartItemsCount = (int) count(Yii::app()->shoppingCart->getPositions());
		$this->cartUrl 				= Docebo::createLmsUrl('cart/index');
		$this->isSaas 				= Docebo::isSaas();
		$this->isGodadmin 			= Yii::app()->user->isGodadmin;
		$this->isTrial 				= Docebo::isTrialPeriod();
		$this->isTrialExpired 		= Docebo::isTrialExpired();
		$this->trialRemainingDays 	= Docebo::getTrialRemainingDays();
		$this->showTrialTimer 		= $this->isGodadmin && !$this->isTrialExpired && $this->isSaas && $this->isTrial;
		$this->showUsersInfo 		= $this->isGodadmin && $this->isSaas && !$this->isTrial;
		$this->inGracePeriod		= Settings::get('grace_period', 0);
		$this->activeUsers			= Docebo::getActiveUsers();
		$this->boughtUsers			= Settings::get('max_users', 0);
		$this->enable_user_billing  = Settings::get('enable_user_billing', 'on') == 'on';
		$this->demo_platform		= Settings::get('demo_platform', 0) == 1;
		// suspended temporarly, it was loading to much the docebo.com server
		$this->lmsOrderInfo         = false; //CJSON::decode(ErpApiClient::apiErpGetSaasOrderInfo(array('installation_id'=>Docebo::getErpInstallationId())));
	}

	/**
	 * RUN
	 * @see CWidget::run()
	 */
	public function run() {

		Yii::app()->event->raise('BeforeSiteHeaderRender', new DEvent($this, array()));

		if (!empty($this->headerType)) {
			$view_name = 'header_' . $this->headerType;
		}
		else if (Yii::app()->user->getIsGuest()) {
			$view_name = 'header_anonymous';
		} else {
			$view_name = 'header_user';
		}
		$logoImageUrl = Yii::app()->theme->getLogoUrl();
		// Company Logo URL

		Yii::app()->event->raise('BeforeHeaderLine', new DEvent($this, array()));
		$triggerRegistration = false;

		if(	isset(Yii::app()->session['registerRedirect']) && Yii::app()->session['registerRedirect']  &&  ($view_name == 'header_anonymous')  &&  (Settings::get('register_type', 'admin') != 'admin')){
			unset(Yii::app()->session['registerRedirect']);
			$triggerRegistration = true;
		}

		$customMessageInHeader = CoreLangWhiteLabelSetting::getCustomHeader();

		Yii::app()->event->raise('GetMultidomainHeaderMessage', new DEvent($this, array('customMessageInHeader' => &$customMessageInHeader)));

		$this->render('common.widgets.views.siteHeader.' . $view_name, array(
			'logoImageUrl' => $logoImageUrl,
			'triggerRegistration' => $triggerRegistration,
			'customMessageInHeader' => $customMessageInHeader,
		));
		// not needed anymore
		// $this->widget('common.widgets.BugReportButton');
	}

}