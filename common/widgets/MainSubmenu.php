<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class MainSubmenu extends CWidget {

	/**
	 * This function returns the list of items to show in the main sub-menu.
	 * @return array
	 */
	private function getMainSubmenuItems() {

		$icons = array(
			'_MY_CERTIFICATE' => 'certificates',
			'_MYCOMPETENCES' => 'competencies',
			'_COURSEPATH' =>'curricula',
			'_PUBLIC_FORUM' =>'community',
			'_COURSE_AUTOREGISTRATION' => 'activate-course',
			'_MESSAGES' => 'messages',
			'_CATALOGUE' => 'catalogue',
			'_MYCOURSES' => 'mycourses'
		);


		$criteria = new CdbCriteria();
		$criteria->addInCondition('module_info',array('all','user'));
		// We remove _MYCOURSES and _CATALOGUE
		$criteria->condition='t.idModule!=47 && t.idModule!=1 && t.idMain=0';
		$criteria->order = 'module_info, sequence';

		$menuItems = LearningMenucourseUnder::model()->with('learningModule')->findAll($criteria);

		$menu = array();

		// adding the 'my courses' and 'catalogue' submenu items
		$urlParamOpt = Yii::app()->request->getParam('opt', '');
		$isMycoursesActive = ('mycourses' == $urlParamOpt || ('' == $urlParamOpt
			&& Yii::app()->controller->id == 'site' && Yii::app()->controller->getAction()->id == 'index'));
		$menu[] = array(
			'active' => $isMycoursesActive,
            'url' => Yii::app()->createUrl('site/index', array()),
			//'url' => Yii::app()->createUrl('site/index', array('opt' => 'mycourses')),
			'text' => Yii::t('menu_over', '_MYCOURSES'),
			'sprite_gmenu_icon' => $icons['_MYCOURSES']
		);
		// check if caatalog is enabled
		$isCatalogEnabled = false;
		if(PluginManager::isPluginActive('CoursecatalogApp')) {
			$isCatalogEnabled = Settings::get('catalog_type', 'mycourses') != "mycourses";
		}
		if ($isCatalogEnabled)
		{
			$isCatalogActive = ('catalog' == $urlParamOpt);
			$menu[] = array(
				'active' => $isCatalogActive,
				'url' => Yii::app()->createUrl('site/index', array('opt' => 'catalog')),
				'text' =>  Yii::t('standard', 'Catalog'),
				'sprite_gmenu_icon' => $icons['_CATALOGUE']
			);
		}

		$count = 0;
		foreach($menuItems as $item) {
			$link = '../doceboLms/index.php?' . ($item->learningModule->mvc_path ? 'r=' . $item->learningModule->mvc_path : 'modname=' . $item->learningModule->module_name . '&amp;op=' . $item->learningModule->default_op) . '&amp;sop=unregistercourse';
			$moduleName =  $item->learningModule->module_name;

			if($item->learningModule->default_name == '_COURSEPATH') {
				$defaultName = Yii::t('standard',$item->learningModule->default_name);
			} else {
				$defaultName = Yii::t('menu_over',$item->learningModule->default_name);
			}


			$middleArea = LearningMiddlearea::model()->findByAttributes(array('obj_index' => 'mo_' . $item->idModule));


			if($middleArea) {
				if($middleArea->currentCanAccessObj() && Yii::app()->user->checkAccess('/lms/course/public/'.$moduleName.'/'.$item->learningModule->token_associated)) {
					$menu[] = array(
						'active' => false,
						'id' => $count,
						'url' => $link,
						'text' => $defaultName,
						'sprite_gmenu_icon' => $icons[''.$item->learningModule->default_name.'']  // to fix with the proper icon
					);
				}
			}

			$count++;
		}

		// Special case for messages
		$middleArea = LearningMiddlearea::model()->findByAttributes(array('obj_index' => 'mo_message'));


		if($middleArea) {
			if ($middleArea->currentCanAccessObj('mo_message') && !Yii::app()->user->isGuest) {
				$link = '../doceboLms/index.php?modname=message&amp;op=message&amp;sop=unregistercourse';
				$defaultName = Yii::t('standard','_MESSAGES');

				$menu[] = array(
					'default' => false,
					'id' => $count,
					'url' => $link,
					'text' => $defaultName,
					'sprite_gmenu_icon' => $icons['_MESSAGES']  // to fix with the proper icon
				);
			}
		}

		// in the case that the submenu only contains "my courses", we should not display it at all
		if (count($menu) == 1) {
			$menu = array();
		}

		return $menu;
	}

	public function init() {
		parent::init();

	}

	public function run() {

		$submenuItems = $this->getMainSubmenuItems();

		$this->render('main_submenu', array(
			'submenuItems' => $submenuItems
		));
	}

}
