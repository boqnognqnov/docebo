<?php

/**
 * Description of Tooltips
 *
 * @author Stanimir
 */
class Tooltips extends CWidget {

	/**
	 * Store current object for tooltips
	 * @var App7020Content 
	 */
	public $content;

	const WIDGET_FOLDER_CONTAINER = 'common.widgets.views.app7020Tooltips.';

	function run() {
		$allTooltips = $this->content->tooltips;
 		foreach (array_reverse($allTooltips) as $tooltips) {
			$this->controller->renderPartial(self::WIDGET_FOLDER_CONTAINER . 'tooltipItem', array('style' => $tooltips->themeClass, 'idHash' => md5($tooltips->id), 'tooltip' => $tooltips));
		}
	}

}
