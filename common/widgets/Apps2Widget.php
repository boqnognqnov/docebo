<?php

/*
 * This widget is used to render the rows for each app along with its actions (e.g. "Install" button, price, etc)
 *
 * @author Andrei Amariutei, Dzhuneyt Ahmed
 */
class Apps2Widget extends CWidget {
    
    
    const ACTION_CONTACT_US     = "contact_us";
    const ACTION_ACTIVATE       = "activate";
    const ACTION_TRY_FOR_FREE   = "try_for_free";
    const ACTION_UPGRADE        = "upgrade";
    const ACTION_DISCOVER_MORE  = "discover_more";
    const ACTION_DEACTIVATE     = "deactivate";
    
    

    /**
     * App data array
     * @var array
     */
	public $app;
	/*
	 * tells if the app is installed or not
	 * used to render the correct buttons on hover
	 */
	public $isInstalled;
	/*
	 * tells if the user is in the trial period
	 * used to render the correct buttons on hover
	 */
	public $isInTrialPeriod;
	/*
	 * If App is available for current plan
	 */
	public $isAvailable;

	/*
	 * Which Plan this App is available in
	 */
	public $availableInPlan;

	/**
	 * Tells if the app us Contact us only to be installed
	 */
	public $isContactUsOnly;

	/**
	 * Tells if the installation is ECS, by checking the installed apps.
	 */
	public $isECSInstallation;

	/**
	 * Tells what is the setting for enable_user_billing in core_setting
	 */
	public $enableUserBilling;

	/**
	 * Tells if the accout is reseller - comes from the cookie.
	 */
	public $isReseller;

	public $maxCourseTitleChars = 150;
	
	public $hasSettings=false;
	
	public $pluginName;

	/**
	 * 
	 * {@inheritDoc}
	 * @see CWidget::init()
	 */
    public function init() {
        
        parent::init();

		$this->isInstalled = (bool) $this->app['is_active'];

		$this->isInTrialPeriod = Docebo::isTrialPeriod();

		$this->isAvailable = (bool) $this->app['is_available'];

	    $this->availableInPlan = $this->app['available_in_plan'];

		$this->isContactUsOnly = (bool) $this->app['use_contact_us'];

		$this->enableUserBilling = Settings::get('enable_user_billing', 'on');

		$this->isReseller = isset(Yii::app()->request->cookies['reseller_cookie']);

		$this->pluginName = ucfirst($this->app['internal_codename']).'App';
		
		$pluginName = $this->pluginName;
		
		if ($this->isInstalled){
		    Yii::import("plugin.$pluginName.$pluginName");
		    $this->hasSettings = $pluginName::$HAS_SETTINGS;
		}
		

    }

    public function run() {
        $this->render('apps2_widget');
    }

    protected function renderApp() {
        
        switch ($this->app['allowed_action']) {
            
            case self::ACTION_DEACTIVATE:
                $this->renderDeActivate();
                break;
            
            case self::ACTION_CONTACT_US:
                $this->renderContactUs();
                break;
            
            case self::ACTION_TRY_FOR_FREE:
                $this->renderTry();
                break;
            
            case self::ACTION_ACTIVATE:
                $this->renderActivate();
                break;
            
            case self::ACTION_UPGRADE:
                $this->renderUpgrade();
                break;
                
            case self::ACTION_DISCOVER_MORE:
                $this->renderDiscoverMore();
                break;
            
            default:
                break;
        }
        
    }

    protected function renderTemplate($title, $btnColor, $desiredAction) {
    
        $url = Yii::app()->createUrl('apps2/axAppDetails', array(
            'code'      => $this->app['internal_codename'],
            'action'    => $desiredAction, // desired, because, server-side, it will be checked if it is possible
        ));
        
        $class = "btn-docebo big $btnColor btn-buy-app";
        
        if ($desiredAction == self::ACTION_CONTACT_US) {
            $url = "javascript:;";
            $class = $class . " contact-us-links";
        }
        else {
            $class = $class . " open-dialog";
        }
        $id = strtolower($this->app['internal_codename'].'_btn');
        $html = <<< HS5
            <a
                id="$id"
                class="$class"
                href="$url"
                data-dialog-class="metro wide app-details"
                rel="dialog-app-detail"
                title="$title">$title
            </a>
HS5;

        echo $html;
    
    }
    
    
    /**
     * 
     * @param string $return
     * @return string
     */
    protected function renderContactUs() {
        $this->renderTemplate(Yii::t('standard', 'Contact us'), 'blue', self::ACTION_CONTACT_US);
    }
    
    
    /**
     *
     * @param string $return
     * @return string
     */
    protected function renderDiscoverMore() {
        $this->renderTemplate(Yii::t('apps', 'Discover More'), 'black', self::ACTION_DISCOVER_MORE);
    }
    

    /**
     *
     * @param string $return
     * @return string
     */
    protected function renderTry() {
        $this->renderTemplate(Yii::t('apps', 'Start free trial'), 'orange', self::ACTION_TRY_FOR_FREE);
    }

    /**
     *
     * @param string $return
     * @return string
     */
    protected function renderUpgrade() {
        $this->renderTemplate(Yii::t('apps', 'Upgrade now'), 'green', self::ACTION_UPGRADE);
    }

    
    /**
     *
     * @param string $return
     * @return string
     */
    protected function renderActivate() {
        $this->renderTemplate(Yii::t('apps', 'Install app'), 'green', self::ACTION_ACTIVATE);
    }
    
    /**
     *
     * @param string $return
     * @return string
     */
    protected function renderDeActivate() {
        
        $url = Yii::app()->createUrl('apps2/axRemoveApp', array('code' => $this->app['internal_codename']));
        $title = Yii::t('standard', '_DEL');
        $silentButtonMessage = Yii::t('standard', '_ACTIVE');
        $configureTitle = Yii::t('apps', 'Configure');
        
        if ($this->hasSettings) {
            $pluginName = $this->pluginName;
            $configureUrl = $pluginName::settingsUrl();
            if (!$configureUrl) {
                $configureUrl = Yii::app()->createUrl($pluginName.'/'.$pluginName.'/settings');
            }
        }
        
        $html = <<< HS5
            <a
                class="remove open-dialog btn-remove-app"
                href="$url"
                data-dialog-class="metro app-details"
                rel="dialog-app-detail"
                title="$title"><div class="p-sprite cross-red"></div>
            </a>&nbsp;&nbsp;<div class="btn-docebo big black ">$silentButtonMessage</div>
HS5;
        
        if ($this->hasSettings) {
            $html = "
                <a 
                    class=\"configure btn-configure-app\" 
                    href=\"$configureUrl\" 
                    title=\"$configureTitle\"><div class=\"p-sprite gear\"></div>
                </a>
                &nbsp;
                "
                . $html;
        }
        
        echo $html;
        
    }
    
    
    
}
?>
