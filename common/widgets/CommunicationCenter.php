<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 4.8.2016 г.
 * Time: 15:26
 */
class CommunicationCenter extends CWidget{

    public $isMenuVisible = true;
    public $isToDocebo = true;
    public $requestLabel = true;
    public $helpdeskRecipients = array();
    public $showHelpAndManuals = true;

    public function init(){
        parent::init();
        // Register assets
        $ds = DIRECTORY_SEPARATOR;
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'communicationCenter' . $ds . 'assets'.$ds.'hydraBackendService.js'));
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'communicationCenter' . $ds . 'assets'.$ds.'notificationBox.js'));
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'communicationCenter' . $ds . 'assets'.$ds.'communicationTemplates.js'));
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'communicationCenter' . $ds . 'assets'.$ds.'communicationBox.js'));
        Yii::app()->getClientScript()->RegisterCssFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'communicationCenter' . $ds . 'assets'.$ds.'panelStyles.css'));

        $this->isMenuVisible = Yii::app()->user->getIsGodadmin() || $this->isHelpdeskButtonVisible();
        $this->isToDocebo = BrandingWhiteLabelForm::isHelpDeskToDocebo();
        Yii::app()->event->raise('BeforeHelpdeskRender', new DEvent($this, array(
            'requestLabel' => &$this->requestLabel,
            'helpdeskRecipients' => &$this->helpdeskRecipients,
            'showHelpAndManuals' => &$this->showHelpAndManuals
        )));
    }

    /**
     * Checks whether the helpdesk button must be visible even for non goadmins
     * @return bool
     */
    public function isHelpdeskButtonVisible() {
        $result = false;
        $event = new DEvent($this, array());
        Yii::app()->event->raise('ShouldDisplayHelpdeskButton', $event);
        if(!$event->shouldPerformAsDefault())
            $result = $event->return_value['show'];

        return $result;
    }

    public function run(){
        if(Yii::app()->controller->id != 'setup') {
            $this->render('common.widgets.communicationCenter.views.index');
        }
    }
}