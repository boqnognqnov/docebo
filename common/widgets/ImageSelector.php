<?php
/**
 * Shows a Dialog2 and allow user to select an Asset Image.
 * Internally, creates <input type=hidden> and <img> elements.
 * Allows uploading new asset images as well as deleting ones.
 *
 */
class ImageSelector extends CWidget {

	/**
	 * Asset Image type to use
	 * @var string
	 */
	public $imageType		= CoreAsset::TYPE_GENERAL_IMAGE;
	
	/**
	 * ID of a pre-selected asset image, if any 
	 * @var integer 
	 */
	public $assetId			= null; // set some current Asset ID (pre selected)
	
	/**
	 * Image Variant to use in image preview
	 * @var integer
	 */
	public $imgVariant 		= CoreAsset::VARIANT_SMALL;
	
	/**
	 * Show/No Show of the image preview
	 * @var boolean
	 */
	public $showImgPreview	= true;  // set to false to disable <img> preview
	
	/**
	 * The button text & class(es)
	 * @var string
	 */
	public $buttonText		= 'Select Image';
	public $buttonClass		= 'btn btn-docebo green big';
	
	/**
	 * Dialog ID and class
	 * @var string
	 */
	public $dialogId		= 'image-selector-modal';
	public $dialogClass		= 'image-selector-modal';

	/**
	 * HTML Options for <input> & <img>
	 * @var array
	 */
	public $inputHtmlOptions	= array(
		'name'	=> 'asset_image_selector_field',	
		'id'	=> 'asset-image-selector-input-id',	
		'class'	=> 'asset-image-selector-input-class',
	);
	public $imgHtmlOptions		= array(
		'id'	=> 'asset-image-selector-img-id',
		'class'	=> 'asset-image-selector-img-class',
	);
    
    public $showStandardImages = true;
	

	/**
	 * @see CWidget::init()
	 */
	public function init() {
		parent::init();
		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/image_selector.js');
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/image_selector.css');
		$this->imgHtmlOptions['id'] = 'image-' . $this->inputHtmlOptions['id'];
	}

	/**
	 * @see CWidget::run()
	 */
	public function run() {
		$this->render('image_selector', array());
	}
	
	
}