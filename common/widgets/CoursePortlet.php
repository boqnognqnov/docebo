<?php

class CoursePortlet extends CWidget {

	/**
	 * Course Model
	 * @var LearningCourse
	 */
	public $courseModel = null;

	public $class = '';
	// cart - overbooked - free
	public $type = '';
	public $courseDetailUrl 	= '#';
	public $resetToken 	= false;
	public $courseLogoUrl 		= '';
	public $maxCourseTitleChars = 150;

	public $typeIcon = '';
	public $gridType;
	public $idUser;

	// If user can play the course (subscribed and canEnter)
	public $canPlayCourse = false;
	public $showCourseDetailsModal = true;


	public function init() {
		parent::init();

		// Set icon type based on Grid Type and user subscribtion(s)
		// Assigning is done in ascending priority order (last one most important)
		if ( $this->gridType == CoursesGrid::$GRIDTYPE_EXTERNAL ) {

			// For sale
			if ($this->courseModel->selling==1) {
				$this->type="cart";
			}
			// Overbooked
			else if ($this->courseModel->isOverbooked())
			{
				$this->type="overbooked";
			}
			// Free and Can Subscribe: NO ICON
			else if ( $this->courseModel->selling==0 && $this->courseModel->can_subscribe==1 ){
				$this->type = null;
			}

		}
		else {

			$criteria = new CDbCriteria();
			$criteria->compare('idUser', $this->idUser);
			$criteria->compare('idCourse', $this->courseModel->idCourse);
			$courseUsersModel = LearningCourseuser::model()->find($criteria);

			// Is user subscribed to this course ?
			if ($courseUsersModel) {

				$model = $courseUsersModel;

				switch ($model->status)
				{
					case 0: // course not started
						$this->type = 'new';
						break;
					case 1: // course started
						break;
					case 2: // course completed
						$this->type = 'completed';
						break;
				}

				$canEnterCourse = $model->canEnterCourse();
				if ( ! $canEnterCourse['can'] ) {
					$this->type = 'locked';
				}
				else {
				    $this->canPlayCourse = true;
				}
			}
			else {
				if ($this->courseModel->selling==1) {
					$this->type="cart";
				}
				// Overbooked
				else if ($this->courseModel->isOverbooked())
				{
				    $this->type="overbooked";
				}
			}
		}

		$this->showCourseDetailsModal = (Settings::get('show_course_details_dedicated_page') !== 'on');

		$this->courseDetailUrl = ($this->showCourseDetailsModal)
			? Yii::app()->createUrl('course/axDetails',array('id' => $this->courseModel->idCourse))
			: Yii::app()->createUrl('course/details',array('id' => $this->courseModel->idCourse));

		Yii::app()->event->raise('BeforeCourseHoverDetailsContentRender', new DEvent($this, array(
			'courseModel' => $this->courseModel,
			'coursePlayerUrl' => &$this->courseDetailUrl,
		)));

		$this->class .= ' tile course '. $this->type . ($this->courseModel->isCurrentUserSubscribedTo ? ' play' : '');
	}

	public function run() {

		// optimizing course title so that it's length in the course grid box is max 4 lines
		$courseName = $this->courseModel->name;
		if (strlen($courseName) > $this->maxCourseTitleChars)
		{
			$courseName = mb_substr($courseName, 0, $this->maxCourseTitleChars, 'UTF-8');
			$lastSpace = mb_strrpos($courseName, ' ', 0, 'UTF-8');
			$courseName = mb_substr($courseName, 0, $lastSpace-1, 'UTF-8') . '&hellip;';
// 			$courseName = substr($courseName, 0, $this->maxCourseTitleChars);
// 			$lastSpace = strrpos($courseName, ' ');
// 			$courseName = substr($courseName, 0, $lastSpace-1) . '&hellip;';
		}

		$this->render('coursePortlet/course_portlet', array(
			'courseName' => $courseName
		));
	}
}
