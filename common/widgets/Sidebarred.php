<?php
/**
 * Reusable sidebar/main content like widget, like the one we use in
 * Advanced Settings.
 *
 * Author: Dzhuneyt Ahmed
 * Date: 22 Jan 2014
 */

class Sidebarred extends CWidget {

	public $sidebarId = null;
	public $sidebarTitle = null;
	public $sidebarItems = array();

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();
	}

	/**
	 * Runs the widget
	 */
	public function run(){

		$hashChangeScript = Yii::app()->theme->baseUrl.'/js/jquery.hashchange.min.js';
		Yii::app()->clientScript->registerScriptFile($hashChangeScript);
		$this->render('common.widgets.views.sidebarred.index', array(
			'sidebarId' => $this->sidebarId,
			'sidebarTitle' => $this->sidebarTitle,
			'sidebarItems' => $this->sidebarItems,
		));
		parent::run();
	}
} 