<?php

/**
 * Description of app7020ContentListView
 *
 * @author Kristian
 */
class app7020MyMostAssetsListView extends CWidget {

	public $listParams = array();
	public $gridColumns = 1;
	public $noItemViewContainer = true;
	public $disableMassSelection = true;
	public $disableMassActions = true;
	public $disableFiltersWrapper = true;
	public $disableCheckBox = true;
	public $titleAsLink = false;
	public $linkToEditMode = false;
	public $blankTemplates = array();
	protected $_blankTemplates = array(
		'page' => 'common.widgets.views.app7020MyMostAssetsListView.defaultPageBlank',
		'search' => 'common.widgets.views.app7020MyMostAssetsListView.defaultSearchBlank'
	);
	public $extensions = '';
	protected $_extensions = array(
		'available' => '',
		'default' => ''
	);

	/* Predefined callbacks for this widget */
//	protected $_beforeUpdate = "app7020.app7020ContentListView.listViewBeforeUpdate(this, id, options);";
//	protected $_afterUpdate = "app7020.app7020ContentListView.listViewAfterUpdate(this, id, data);";
	protected $_beforeUpdate = "";
	protected $_afterUpdate = "";

	public function run() {
		$this->render('common.widgets.views.app7020MyMostAssetsListView.index');
	}

}
