<?php

/**
 * DOCEBO, e-learning SAAS
*
* @link http://www.docebo.com/
* @copyright Copyright &copy; 2004-2012 Docebo
* 
* 
* Example code:
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38815633-1']);
  _gaq.push(['_setDomainName', 'docebotest.info']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

* 
*/

class GoogleAnalytics extends CWidget {

	/**
	 * RUN
	 * @see CWidget::run()
	 */
	public function run() {
		if (!PluginManager::isPluginActive('GoogleAnalyticsApp')) {
			return false;
		}
		
		$code = Settings::get('google_stat_code', '');
		if( Settings::get('google_stat_in_lms', '0') == '1' &&  $code != '') {
			echo $code;
		}
	}

}
