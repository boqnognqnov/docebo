<?php

/**
 * Description of app7020NavigationMenu
 *
 * @author Kristian
 */
class app7020NavigationMenu extends CWidget {

	public $links = array();
	public $oldStyle = true;
	public $select = false;
	public $autoActive = true;
	
	protected $_tabTitleAttr = array(
		// Add default
	);
	protected $_tabContentAttr = array(
		'class' => 'tab-pane'
	);
	
	public function run() {
		$this->render('common.widgets.views.app7020NavigationMenu.index');
	}

}
