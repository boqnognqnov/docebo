<?php
class TestQuestionPreview extends CWidget
{
	public $questionModel;
	protected $answers;

	public function init() {
		$this->answers = LearningTestquestanswer::model()->findAll(array(
			'condition' => 'idQuest = :questId',
			'params' => array(':questId' => $this->questionModel->idQuest),
			'order' => 'idAnswer ASC',
		));
	}


	public function run() {
		$this->renderQuestion();
		switch ($this->questionModel->type_quest) {
			case LearningTestquest::QUESTION_TYPE_CHOICE:
			case LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE:
				$this->formatChoice();
				break;
			case LearningTestquest::QUESTION_TYPE_ASSOCIATE:
				$this->formatAssociate();
				break;
			case LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT:
			case LearningTestquest::QUESTION_TYPE_UPLOAD:
				//no system answers, so skip, but left the 'case' if needed :)
				break;
			case LearningTestquest::QUESTION_TYPE_INLINE_CHOICE:
				$this->formatChoice();
				break;
			case LearningTestquest::QUESTION_TYPE_FITB:
				$this->formatFITB();
				break;
			case LearningTestquest::QUESTION_TYPE_TEXT_ENTRY:
				$this->formatTextEntry();
				break;
		}
	}

	private function renderQuestion()
	{
		echo '<h5>'.$this->questionModel->getTitleQuestTruncated().'</h5><br>';
	}

	private function formatChoice() {
		foreach($this->answers as $i => $answer) {
			if ($answer->is_correct)
				echo '<i class="fa fa-check-circle-o" style="clear: left; float: left; color: #65c063; font-size: 20px;"></i>';
			else
				echo '<i style="float: left; clear: left; width: 17px; height: 20px;"></i>';
			echo '<span class="questionbank-preview" style="float: left; margin-left: 4px;">'.($i+1).'. '.$answer->answer.'</span><br>';
		}
	}

	private function formatTextEntry() {
		echo $this->answers[0]->answer;
	}

	private function formatFITB(){

		$data = array();
		foreach($this->answers as $i => $answer) {
			$data[] = array(
				'shortcode' => '[asnw ' . $answer->sequence . ']',
				'correct' => $answer->answer,
				'comment' => $answer->comment
			);
		}
		Yii::app()->clientScript->registerCss('fitb_preview', '
			table#fitb_test_preview {
				width: 100%;
			}
			div#preview_fitb {
				padding: 0px 0px 10px 0px;
			}
		');
		$table = array();
		$table[] = "<div class='grid-view' id='preview_fitb'><table id='fitb_test_preview' class='items'><thead><tr>";
		$row = array();
		$row[] = '<th>' . Yii::t('test', 'shortcode') . '</th>';
		$row[] = '<th>' . Yii::t('test', 'Correct answers') . '</th>';
		$row[] = '<th>' . Yii::t('standard', '_COMMENTS') . '</th>';
		$table[] = implode('', $row);
		$table[] = "</tr></thead><tbody>";
		foreach($data as $key => $answer){
			$row = array();
			$row[] = '<tr class="' . ($key%2 === 0 ? 'odd' : 'even') .'">';
			$row[] = '<td>' . (isset($answer['shortcode']) ? $answer['shortcode'] : '') . '</td>';
			$row[] = '<td>' . (isset($answer['correct']) ? str_replace(',', ', ',$answer['correct']) : '') . '</td>';
			$row[] = '<td>' . (isset($answer['comment']) ? $answer['comment'] : '') . '</td>';
			$row[] = '</tr>';
			$table[] = implode('', $row);
		}
		$table[] = "</tbody></table></div>";
		echo implode('', $table);
	}

	private function formatAssociate() {
		echo '<div class="row-fluid">';
		echo '<div class="span6 no-margin">'.Yii::t('test', '_TEST_QUEST_ELEMENTS_A').'</div>';
		echo '<div class="span6 no-margin">'.Yii::t('test', '_TEST_QUEST_ELEMENTS_B').'</div>';
		$associates = LearningTestquestanswerAssociate::model()->findAll(array(
			'condition' => 'idQuest = :idQuest',
			'params' => array(':idQuest' => $this->questionModel->idQuest),
			'order' => 'idAnswer ASC'
		));
		foreach ($this->answers as $i => $answer)
		{
			echo '<div class="span6 no-margin">'.$answer->answer.'</div>';
			echo '<div class="span6 no-margin">'.(isset($associates[$i]) ? $associates[$i]->answer : "").'</div>';
		}
		echo '</div>';
	}

}