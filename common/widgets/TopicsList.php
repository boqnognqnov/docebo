<?php

/**
 * Description of GuruCurclesList
 *
 * @author Kristian
 */
class TopicsList extends CWidget {

	public $arrayData = array();
	public $maxVisibleItems = 5;
	public $containerClass = 'customDropdown';

	public function run() {
		$this->render('common.widgets.views.topicsList.index');
	}

}
