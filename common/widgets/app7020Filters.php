<?php
/**
 * Description of app7020Filters
 *
 * @author Kristian
 */
class app7020Filters extends CWidget {
	/**
	 * link simple Array one item example
	 * array(
	 *    title => Yii:t('app7020', 'title of the link'),
	 *    href  => '', // Default is "javascript:;"
	 *    attr => array() // Same as all attributes in LMS
	 * ),
	 * ....,
	 * ...,
	 * ..,
	 * .,
	 */
	public $links = array();
	public $topics = true;
	public $topicsNew = false;
	public $filters = false;
	public $filtersNew = false;
	public $toggleGrid = 1;
	public $searchEngine = true;
	public $comboListViewId = '';
	public $action = false;
	public $searchVal = '';
	public $have_permission = '';
	
	public function run() {
		$this->render('common.widgets.views.app7020Filters.index');
	}
}
