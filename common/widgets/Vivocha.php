<?php

/**
 * DOCEBO, e-learning SAAS
*
* @link http://www.docebo.com/
* @copyright Copyright &copy; 2004-2012 Docebo
* 
* Example code: 
* <script src="//www.vivocha.com/a/plamendp/api/vivocha.js"></script>
* 
*/

class Vivocha extends CWidget {

	/**
	 * RUN
	 * @see CWidget::run()
	 */
	public function run() {
		if (!PluginManager::isPluginActive('VivochaApp')) {
			return false;
		}
		
		if (Yii::app()->user->isGodadmin || Yii::app()->user->isAdmin ) {
			return false;
		}
		$code = Settings::get('vivocha_code', '');
		if( Settings::get('vivocha_in_lms', '0') == '1' &&  $code != '') {
			echo $code;
		}
	}

}