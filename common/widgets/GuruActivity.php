<?php

/**
 * Description of GuruActivity
 *
 * @author Kristian
 */
class GuruActivity extends CWidget {

	public $containerClass = 'customGuruActivity';
	public $isTab = false;
	public $autoSelect = false;

	public function run() {
		$orangeTab = count(App7020QuestionRequest::getExpertsRequests(Yii::app()->user->idst, 1)->rawData) + count(App7020QuestionRequest::getExpertsRequests(Yii::app()->user->idst, 2)->rawData);
		$greenTab = count(App7020Assets::getExpertsReviewedAssets(Yii::app()->user->idst, array(
																						App7020Assets::CONVERSION_STATUS_FINISHED, 
																						App7020Assets::CONVERSION_STATUS_INREVIEW,
																						App7020Assets::CONVERSION_STATUS_UNPUBLISH,
																						App7020Assets::CONVERSION_STATUS_APPROVED,)
				)->rawData);
		$blueTab = App7020Question::getExpertsQuestions(Yii::app()->user->idst, 2)->totalItemCount;
		$this->render('common.widgets.views.guruActivity.index', array('orangeTab' => $orangeTab, 'greenTab' => $greenTab, 'blueTab' => $blueTab));
	}

}
