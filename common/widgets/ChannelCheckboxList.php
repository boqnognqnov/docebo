<?php

/**
 * Component used in 
 * - Contribute metadata form
 * - Assign question to channel
 * 
 *
 * @author Станимир
 */
class ChannelCheckboxList extends CWidget {

	/**
	 *
	 * @var App7020Assets 
	 */
	public $asset;
	public $editmode;
	public $isDisabledTree;
	public $isReadOnlyTree = false;
	public $excludeChannels = array();

	function __construct($owner = null) {
		parent::__construct($owner);
	}

	function init() {
		parent::init();
	}

	public function run() {

		//get all channels
		// $allChannles = App7020Channels::getChannelsDataProvider(false, false, false, array(), $this->excludeChannels);

		$a = array(
			"idUser" => Yii::app()->user->idst,
  			"ignoreVisallChannels" => false, // Admins will see everything in search anyway 
			"ignoreGodAdminEffect" => false, // Admins will see everything in search anyway 
 			'ignoreChannels' => $this->excludeChannels,
			'appendEnabledEffect' => true,
			'ignoreSystemChannels' => true,
            'pagination' => false,
			'customSelect' => array(
				'(CASE WHEN ct.name IS NULL THEN ct_fb.name ELSE ct.name END)' => 'channelName',
				'#alias#.id' => 'idChannel',
				'#alias#.permission_type' => 'channelPermissions',
				'#alias#.enabled' => 'channelEnabled',
				
			)
		);

		$dp = DataProvider::factory("UserChannels", $a)->provider();
 
		//all channels who logged user is an expert;
		$userExpertsChannels = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert();

        //trash arrays for earsly work
		$allCheckBoxes = array();
		$prefilledTmp = array();

		//if exist asset, put their channels into array
		//$idQuestion = Yii::app()->request->getParam('idQuestion', false);
		 
		if ($this->asset instanceof App7020Assets) {
			$prefilledTmp = $this->asset->getCheckedChannels();
		}

		//get all ids for translation and label options for existing of PR state
		$label_options = array();
		$expertRequiredChannel = array();
		$disabledChannel = array();
		foreach ($dp->data as $chan) {

			$allCheckBoxes[] = $chan['idChannel'];
			$label_options[$chan['idChannel']] = (int) $chan['channelPermissions'] == App7020Channels::PERMISSION_TYPE_EVERYONE_PR ? true : false;
			if ((int) $chan['channelPermissions'] == App7020Channels::PERMISSION_TYPE_EXPERTS_ONLY) {
                if (in_array($chan['idChannel'], $userExpertsChannels)) {
					$expertRequiredChannel[$chan['idChannel']] = false;
				} else {
					$expertRequiredChannel[$chan['idChannel']] = true;
                    $disabledChannel[$chan['idChannel']] = false;
				}
			} else {
				$expertRequiredChannel[$chan['idChannel']] = false;
			}

			//check disabled channels
			$disabledChannel[$chan['idChannel']] = true;
            if ((bool)$chan['channelEnabled'] == false) {
				$disabledChannel[$chan['idChannel']] = false;
			}
		}

		//trash array for translated channel names 
        $tmpArrAllTranslatedChannel = array();

		//proccess all channels 
		if (!empty($allCheckBoxes) && is_array($allCheckBoxes)) {
			foreach (App7020Channels::model()->translation(false, $allCheckBoxes) as $key => $chanTranslated) {
//				$isPR = isset();

                $tmpArrAllTranslatedChannel[$key]['label'] = $chanTranslated['name'];
                $tmpArrAllTranslatedChannel[$key]['disabled'] = false;

				if ($label_options[$key]) {
                    $tmpArrAllTranslatedChannel[$key]['is_pr'] = true; // '<div class="app-7020-is-pr "><i class="fa fa-thumbs-o-up"></i> ' . $chanTranslated['name'] . '</div>';
				}

                if (($expertRequiredChannel[$key] || !$disabledChannel[$key]) && !Yii::app()->user->getIsGodadmin()) {
                    $tmpArrAllTranslatedChannel[$key]['disabled'] = true;
				}


				if (in_array($key, $prefilledTmp)) {
                    $tmpArrAllTranslatedChannel[$key]['checked'] = true;
					//$prefilled[] = $key;
				} else {
                    $tmpArrAllTranslatedChannel[$key]['prefiled'] = false;
				}
			}
		}
//		if ($this->editmode && Yii::app()->user) {
//			$this->isDisabledTree = true;
//		}
		$this->render('app7020.protected.views.includes._treeWidget', array(
            'checkboxes' => $tmpArrAllTranslatedChannel,
			'isVisible' => $this->asset->is_private == App7020Assets::PRIVATE_STATUS_INIT ? true : false,
			'isDisabledTree' => $this->isDisabledTree,
			'isReadOnlyTree' => $this->isReadOnlyTree,
			'prefilled' => $prefilledTmp,
				), false, false);
	}

}
