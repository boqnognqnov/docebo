<?php

/**
 * @author vbelev
 * @desription  - app7020AlertBox can be used as alternative of app7020.notify. 
 * The deference is that it MUST be called first in DOM as widget, this way options can be provided in PHP and/or JS
 */
class app7020AlertBox extends CWidget {

	public $success = true;
	public $textSuccess = '';
	public $textError = '';
	public $id = '';
	public $hide = true;
	
	
	
	
	
	public function run() {
		$this->render('common.widgets.views.app7020AlertBox.index');
	}

	
	
	
	
}
