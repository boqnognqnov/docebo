<?php

/* @property CDataProvider $dataProvider  */

class ComboGridView extends CWidget {

    public $autocompleteRoute = '';
    public $massiveActions = false;
    public $gridId = 'combo-grid';
    public $dataProvider = false;
    public $columns = array();
    public $hiddenFields = array();
    public $disableMassSelection = false;
    public $disableMassActions = false;
    public $disableSearch = false;
    public $typeOfModel = false;
    public $flagForCustomFields = false;
    public $gridTitleLabel = false;
    public $gridTemplate = false;
    public $preselectedItems = false;
	public $gridAjaxUrl = false;

    /**
     * Sometimes we need to put the "Select all/De select all" INSIDE the form filter, by design 
     * @var boolean
     */
    public $massSelectorsInFilterForm = false;

    /**
     * Specify the URL to use to get data about select all/deselect all
     * @var 
     */
    public $massSelectorUrl = false;
    public $doNotCreateForm = false;
    // Rows reordering related (sortable)
    public $sortableRows = false;
    public $sortableUpdateUrl = false;
    public $dragHandlerSelector = false;
    public $noYiiGridViewJS = false;
    public $infinite = false;

    /**
     * Allows the customization of the filter by adding custom checkboxes or other
     * input fields. Each element in the array should have the following structure:
     * array(
     *      'groupClass'=>'',
     *      'groupHtml'=>'',
     * )
     * See comboGridView/index.php to see how are they rendered
     */
    public $customFilterGroups = array();

    public function init() {
        
    }

    public function run() {
        $this->render('common.widgets.views.comboGridView.index');
    }

    public function getRowCssClass($row, $data) {
        if ($this->dataProvider instanceof CActiveDataProvider) {
            return "sorted[]_" . $data->getPrimaryKey() . "";
        } elseif ($this->dataProvider instanceof CSqlDataProvider || $this->dataProvider instanceof CArrayDataProvider) {
            $keyField = $this->dataProvider->keyField;
        } else {
            $keyField = 'id';
        }
        if ($data['ui-state-disabled'] == true) {
            return "sorted[]_" . $data[$keyField] . " ui-state-disabled";
        }
        return "sorted[]_" . $data[$keyField] . "";
    }

}
