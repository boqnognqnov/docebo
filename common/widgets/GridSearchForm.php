<?php

/**
 * Display a search, depending on type
 * @author Plamen
 */

class GridSearchForm extends CWidget {

	public $type = null;

	public $moreDropdownLinks;

	public $labels = array();

	public $count_courses_without_label = 0;

	public $catalogs = array();

	public $current_catalog = '';

	public function init() {
		parent::init();

		$this->current_catalog = Yii::t('standard', 'All catalogs');

		$idUser = Yii::app()->user->id;
		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		// there's no use displaying the labels on the catalogue page
		// or when filtering by label
		if ('catalog' !== Yii::app()->request->getParam('opt', '')/* && '' === Yii::app()->request->getParam('flt_label', '')*/) {

			// retrieve all labels for the filter
			$userLabels = LearningLabel::model()->getUserLabels($idUser, $langCode);
			if (!empty($userLabels)) {
				foreach($userLabels as $label) {
					if ($label['courseuser_count'] > 0  && $label['id_common_label'] != 0) {
						$this->labels[] = $label;
					}
					if($label['id_common_label'] == 0) {
						// counter for courses without a label
						$this->count_courses_without_label = $label['courseuser_count'];
					}
				}
			}
		} elseif (Yii::app()->request->getParam('opt', '') == 'catalog') {

			$idsts = Yii::app()->authManager->getAuthAssignmentsIds(Yii::app()->user->id);

			if (!$idsts)
			    return;

			// Also include catalogs directly assigned to the current user
			$idsts[] = $idUser;

			// ---- A quite big workaround. Please don't touch if not needed!!! (questions: Dzhuneyt) ---
			// Since a catalog may have a "orgchart+descendats" assigned to it,
			// walk though the current user's orgcharts upwards to their top most
			// parent and collect OCD IDs along the way and compare these IDs to
			// the assigned "orgchart+descendants" to the catalog
			// If a match is found, the catalog will be displayed to the current user
			// ----
			$ocdArr = array();
			$c2 = new CDbCriteria();
			$c2->addCondition('groupMembers.idstMember=:idstMember');
			$c2->params[':idstMember'] = $idUser;
			$c2->with = array('groupMembers');
			$c2->select = 't.lev, t.iLeft, t.iRight';
			$userGroups = CoreOrgChartTree::model()->findAll($c2);
			foreach($userGroups as $group){
				$tempCriteria = new CDbCriteria();
				$tempCriteria->addCondition('t.iLeft <= :currentLeft');
				$tempCriteria->addCondition('t.iRight >= :currentRight');
				$tempCriteria->params[':currentLeft'] = $group->iLeft;
				$tempCriteria->params[':currentRight'] = $group->iRight;
				$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);

				if($groupParent){
					foreach($groupParent as $parent){
						$ocdArr[] = $parent->idst_ocd;

						if($group->iLeft === $parent->iLeft)
							$ocdArr[] = $parent->idst_oc;
					}

				}
			}
			$idsts = array_merge($idsts, $ocdArr);
			$idsts = array_unique($idsts);

			$catalogues =	Yii::app()->getDb()->createCommand()
							->select('idCatalogue')
							->from(LearningCatalogueMember::model()->tableName())
							->andWhere(array('IN', 'idst_member', $idsts))
							->queryColumn();

			$command = Yii::app()->getDb()->createCommand()
				->select('t.*, COUNT(ce.idEntry) AS course_count')
				->from(LearningCatalogue::model()->tableName().' t')
				->join(LearningCatalogueEntry::model()->tableName().' ce', 't.idCatalogue=ce.idCatalogue')
				->andWhere(array('IN', 't.idCatalogue', $catalogues))
				->group('t.idCatalogue');

			// We remove the courses having show_rules = Only users subscribed to the course
			// and the user is not subscribed.
			$subscribedCourseIds = LearningCourse::getCourseIdsByUser($idUser);
			$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCourseIds);

			// Join only valid course or learning plan entries
			// Catalogs that have no valid entry are excluded
			$command->leftJoin(LearningCourse::model()->tableName().' course', 'ce.idEntry=course.idCourse AND ce.type_of_entry="course" '.(!empty($coursesToNotDisplayList) ? 'AND course.idCourse NOT IN ('.implode(',',$coursesToNotDisplayList).')' : ''))
				->leftJoin(LearningCoursepath::model()->tableName().' cp', 'ce.idEntry=cp.id_path AND ce.type_of_entry="coursepath"')
				// Get either Courses OR Learning Plans (mixed)
				->andWhere('course.idCourse IS NOT NULL OR cp.id_path IS NOT NULL');

			// Get only EFFECTIVE courses
			$command->andWhere('course.status=:status_effective OR course.idCourse IS NULL', array(':status_effective'=>LearningCourse::$COURSE_STATUS_EFFECTIVE));

			// Order by Catalog name
			$command->order('t.name ASC');

			if (!PluginManager::isPluginActive('ClassroomApp')) {
				// Exclude Courses with type "Classroom" if the Classroom app is disabled
				// (but remember the JOINed Learning Plans, the course.idCourse IS NULL is needed for them)
				$command->andWhere('course.course_type <> :classroom OR course.idCourse IS NULL', array(':classroom'=>LearningCourse::TYPE_CLASSROOM));
			}

			$userCatalogs = $command->queryAll();

			if (!empty($userCatalogs)) {
				foreach($userCatalogs as $catalog) {
					$this->catalogs[] = $catalog;
					// check if this is the current catalog
					if ($catalog['idCatalogue'] == Yii::app()->request->getParam('flt_catalog', '')) {
						$this->current_catalog = $catalog['name'];
					}
				}
			}
		} // endif
	}

	/**
	 * @deprecated
	 */
	public function initOld() {
		parent::init();

		$this->current_catalog = Yii::t('standard', 'All catalogs');

		$idUser = Yii::app()->user->id;
		$langCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		// there's no use displaying the labels on the catalogue page
		// or when filtering by label
		if ('catalog' !== Yii::app()->request->getParam('opt', '')/* && '' === Yii::app()->request->getParam('flt_label', '')*/) {

			// retrieve all labels for the filter
			$userLabels = LearningLabel::model()->getUserLabels($idUser, $langCode);
			if (!empty($userLabels)) {
				foreach($userLabels as $label) {
					if ($label['courseuser_count'] > 0  && $label['id_common_label'] != 0) {
						$this->labels[] = $label;
					}
					if($label['id_common_label'] == 0) {
						// counter for courses without a label
						$this->count_courses_without_label = $label['courseuser_count'];
					}
				}
			}
		} elseif (Yii::app()->request->getParam('opt', '') == 'catalog') {

			// catalog
			$idsts = Yii::app()->authManager->getAuthAssignmentsIds(Yii::app()->user->id);

			if (!$idsts)
				return;

			// Also include catalogs directly assigned to the current user
			$idsts[] = $idUser;

			$criteria = new CDbCriteria();
			$criteria->with = array(
				"catalog_members" => array(
					'alias' => 'cm',
				),
				'catalog_entries' => array(
					'alias' => 'ce',
					'group' => 't.idCatalogue',
					'with' => array(
						'course'=>array(
							'joinType' => 'JOIN'
						)
					),
				),
			);
			$criteria->select = "t.*, count(DISTINCT course.idCourse) as course_count";

			// ---- A quite big workaround. Please don't touch if not needed!!! (questions: Dzhuneyt) ---
			// Since a catalog may have a "orgchart+descendats" assigned to it,
			// walk though the current user's orgcharts upwards to their top most
			// parent and collect OCD IDs along the way and compare these IDs to
			// the assigned "orgchart+descendants" to the catalog
			// If a match is found, the catalog will be displayed to the current user
			// ----
			$ocdArr = array();
			$c2 = new CDbCriteria();
			$c2->addCondition('groupMembers.idstMember=:idstMember');
			$c2->params[':idstMember'] = $idUser;
			$c2->with = array('groupMembers');
			$c2->select = 't.lev, t.iLeft, t.iRight';
			$userGroups = CoreOrgChartTree::model()->findAll($c2);
			foreach($userGroups as $group){
				$tempCriteria = new CDbCriteria();
				$tempCriteria->addCondition('t.iLeft <= :currentLeft');
				$tempCriteria->addCondition('t.iRight >= :currentRight');
				$tempCriteria->params[':currentLeft'] = $group->iLeft;
				$tempCriteria->params[':currentRight'] = $group->iRight;
				$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);

				if($groupParent){
					foreach($groupParent as $parent){
						$ocdArr[] = $parent->idst_ocd;

						if($group->iLeft === $parent->iLeft)
							$ocdArr[] = $parent->idst_oc;
					}

				}
			}
			$ocdArr = array_unique($ocdArr);
			$idsts = array_merge($idsts, $ocdArr);
			$criteria->addInCondition('cm.idst_member', $idsts);

			// $criteria->addCondition("ce.type_of_entry='course'");

			// If Classroom is NOT active, count only courses (exclude classrooms)
			// And always JOIN Course table to get really exisitng courses
			if (!PluginManager::isPluginActive('ClassroomApp')) {
				$criteria->addCondition('course.course_type <> :classroom');
				$criteria->params[':classroom'] = LearningCourse::TYPE_CLASSROOM;
			}

			// We remove the courses having show_rules = Only users subscribed to the course
			// and the user is not subscribed.
			$subscribedCourseIds = LearningCourse::getCourseIdsByUser($idUser);
			$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCourseIds);
			if($coursesToNotDisplayList)
				$criteria->addNotInCondition('course.idCourse', $coursesToNotDisplayList);

			// Only consider/count/retrieve courses that are in published status
			$criteria->compare('course.status', LearningCourse::$COURSE_STATUS_EFFECTIVE);
			$criteria->order = "t.name ASC";
			$userCatalogs = LearningCatalogue::model()->findAll($criteria);

			if (!empty($userCatalogs)) {
				foreach($userCatalogs as $catalog) {
					$this->catalogs[] = $catalog;
					// check if this is the current catalog
					if ($catalog['idCatalogue'] == Yii::app()->request->getParam('flt_catalog', '')) {
						$this->current_catalog = $catalog['name'];
					}
				}
			}
		} // endif
	}


	public function run() {

		$params = array(
			'type' => $this->type,
			'currentCatalog' => $this->current_catalog,
			'catalogs' => $this->catalogs,
			'labels' => $this->labels,
			'countCoursesWithoutLabel' => $this->count_courses_without_label,

			'defaultMyCoursesView'=>Settings::get('default_mycourses_view', CoursesGrid::$FILTER_TYPE_MYCOURSES)
		);

		if (Yii::app()->event->raise('BeforeGridSearchFormRender', new DEvent($this, $params))) {
			$this->render('grid_search_form', $params);
		}
	}

}
