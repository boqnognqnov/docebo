<?php

class ComboListView extends CWidget {

	public $viewData = array();
	public $autocompleteRoute = '';
	public $massiveActions = false;
	public $listId = 'combo-list';
	public $dataProvider = false;
	public $listItemView = false;
	public $keyField = "id";
	public $hiddenFields = array();
	public $disableMassSelection = false;
	public $disableMassActions = false;
	public $disableCheckBox = false;
	public $disableFiltersWrapper = false;
    public $noYiiListViewJS     =   false;
	public $columns = 2;
	public $template = '{items}{pager}{summary}';
	// IF true : $listItemView will rendering without container
	public $noItemViewContainer = false;
	// See \common\widgets\views\comboListView\index.php {LINE 123}
	// Need concat quotes with {\}
	public $beforeAjaxUpdate = '';
	// See \common\widgets\views\comboListView\index.php {LINE 130}
	// Need concat quotes with {\}
	public $afterAjaxUpdate = '';
	public $itemsCssClass = '';
	public $restyleAfterUpdate = true;
	public $minMode = false;
	public $pager = false;
	public $infinite = false;
	public $title = '';

	public function init() {
		
		parent::init();
		
		// Resolve data provider key field
		
		// SQL DP ?
		if (isset($this->dataProvider->keyField)) {
			$this->keyField = $this->dataProvider->keyField;
		}
		// Or AR DP's keyAttribute set ?
		else if (isset($this->dataProvider->keyAttribute)) {
			$this->keyField = $this->dataProvider->keyAttribute;
		}
		// Hmm... maybe AR DP with NO key attribute set? Then get the primary key name of the AR model
		else if (get_class($this->dataProvider) === 'CActiveDataProvider' ) {
			$this->keyField = $this->dataProvider->model->tableSchema->primaryKey;
		}
		
		
	}

	public function run() {
		$this->render('common.widgets.views.comboListView.index');
	}


}