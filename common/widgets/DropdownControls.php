<?php

/**
 * Description of DropdownControls
 *
 * @author Kristian
 */
class DropdownControls extends CWidget {

	public $arrayData = array();
	public $containerClass = 'customDropdown';
	public $module = 'index';
	public $isAngular = true;

	public function run() {
		$this->render('common.widgets.views.dropdownControls.' . $this->module);
	}

}
