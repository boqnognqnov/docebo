<?php

/**
 * Class CoursePortlet
 *
 * @property LearningCoursepath $coursepathModel
 */
class CoursepathPortlet extends CWidget {

	public $coursepathModel = null;

	public $class = '';
	// cart - free
	public $type = '';
	public $coursepathDetailUrl = '#';
	public $resetToken 	= false;
	public $coursePathPlayUrl = '#';
	public $courseLogoUrl = '';
	public $maxCourseTitleChars = 150;

	public $typeIcon = '';
	public $gridType;
	public $idUser;

	public $price = null;

	// If user can play the LP (LP is free OR is paid and the user has purchased all courses)
	public $canPlayCoursepath = false;


	public function init() {
		parent::init();

		if ( $this->gridType == CoursesGrid::$GRIDTYPE_EXTERNAL ) {
			// External catalog grid
		}
		else {
			// Internal grid
		}

		switch($this->coursepathModel->is_selling){
			case 1:
				$this->type = 'cart';
				break;
			case 0:
				// Free learning plan (no icon)
				$this->type = null;
				break;
		}

		if($this->coursepathModel->is_selling == 1){
			$this->price = $this->coursepathModel->price;
		}

		if($this->idUser){
			// Check if the current user can "Play" this LP
			// that is, the user is enrolled in it

			$lpsOfUser = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser($this->idUser); // method uses caching
			if(in_array($this->coursepathModel->id_path, $lpsOfUser)){
				$this->canPlayCoursepath = true;
			}
		}

		$this->class .= ' tile course '. $this->type . ($this->coursepathModel->id_path ? '' : '');
		
		$params = array(
			'sop'=>'unregistercourse',
		);
		if ($this->resetToken) {
			$params['reset'] = $this->resetToken;
		}
		$this->coursePathPlayUrl = Docebo::createLmsUrl('curricula/show', $params);
	}

	public function run() {
		// optimizing course title so that it's length in the course grid box is max 4 lines
		$coursepathName = $this->coursepathModel->path_name;
		if (strlen($coursepathName) > $this->maxCourseTitleChars)
		{
			$coursepathName = mb_substr($coursepathName, 0, $this->maxCourseTitleChars, 'UTF-8');
			$lastSpace = mb_strrpos($coursepathName, ' ', 0, 'UTF-8');
			$coursepathName = mb_substr($coursepathName, 0, $lastSpace-1, 'UTF-8') . '&hellip;';
// 			$courseName = substr($courseName, 0, $this->maxCourseTitleChars);
// 			$lastSpace = strrpos($courseName, ' ');
// 			$courseName = substr($courseName, 0, $lastSpace-1) . '&hellip;';
		}

		$this->render('coursepathPortlet/course_portlet', array(
			'courseName' => $coursepathName
		));
	}
}
