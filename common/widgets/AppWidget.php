<?php

/*
 * This widget is used to render the rows for each app along with its actions (e.g. "Install" button, price, etc)
 *
 * @author Andrei Amariutei, Dzhuneyt Ahmed
 */
class AppWidget extends CWidget {

	/*
	 * the $app is an associative array that we have received through an api call to the erp module
	 *
	 * example data:
		{
			"app_id": "8",
			"market_type": "subscription",
			"free_on_premium": "0",
			"internal_codename": "sdf",
			"referral_link": "",
			"published_status": "1",
			"lang": {
				"en": {
					"app_lang_id": "10",
					"app_id": "8",
					"language": "en",
					"title": "Test app",
					"short_description": "",
					"extended_description": ""
				}
			}
		}
	 */
	public $app;
	/*
	 * tells if the app is installed or not
	 * used to render the correct buttons on hover
	 */
	public $isInstalled;
	/*
	 * tells if the user is in the trial period
	 * used to render the correct buttons on hover
	 */
	public $isInTrialPeriod;
	/*
	 * tells if the app is free for install
	 * used to render the correct buttons on hover
	 */
	public $isFreeApp;

	/*
	 * tells if the app is the Cloud Enterprise Solution app
	 */
	public $isEnterprise;

	/*
	 * tells if the current app is free because it's "Free on premium"
	 * and the Enterprise Cloud Solution app is enabled.
	 * This is to be used in conjunction with $this->isFreeApp
	 */
	public $isFreeOnESC;

	/**
	 * Tells if the app us Contact us only to be installed
	 */
	public $isContactUsOnly;

	/**
	 * Tells if the installation is ECS, by checking the installed apps.
	 */
	public $isECSInstallation;

	/**
	 * Tells what is the setting for enable_user_billing in core_setting
	 */
	public $enableUserBilling;

	/**
	 * Tells if the accout is reseller - comes from the cookie.
	 */
	public $isReseller;

	/**
	 * Contains the language dependant features of an app (title, description)
	 * The api response CURRENTLY (13/12/2012) contains the translation for all languages,
	 * so in the init method I'll initialize this var with the correct language data
	 *
	 * @var array $lang
	 */
	public $lang;

	public $maxCourseTitleChars = 150;

	public $showDiscoverMoreButton = false;

    public function init() {
        parent::init();

		$this->isInstalled = (1 == intval($this->app['is_installed']));

		$this->isInTrialPeriod = Docebo::isTrialPeriod();

		$this->isFreeApp = ($this->app['market_type'] == 'free');

	    $this->isEnterprise = $this->app['is_enterprise'];

	    $this->isFreeOnESC = $this->app['free_on_premium'];

		$this->isContactUsOnly = $this->app['use_contact_us'];

		$langData = $this->app['lang'];
		$userLang = Yii::app()->getLanguage();
		$this->lang = (isset($langData[$userLang])) ? $langData[$userLang] : $langData['en'];

		$this->enableUserBilling = Settings::get('enable_user_billing', 'on');

		$this->isReseller = isset(Yii::app()->request->cookies['reseller_cookie']);

		if($this->enableUserBilling != 'on' && !$this->isReseller){
			$this->showDiscoverMoreButton = true;
		}

    }

    public function run() {
        $this->render('app_widget', array(
	        'app' => $this->app,
	        'plugin_name' => ucfirst($this->app['internal_codename']).'App',
        ));
    }

}
?>
