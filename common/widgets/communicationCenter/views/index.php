<?php
/**
 * @var $this CommunicationCenter
 */

if(!Yii::app()->legacyWrapper->hydraFrontendEnabled()):
?>
<?php if($this->isMenuVisible): ?>
    <div class="communication-center-icon">
        <div id="communication-icon">
            <i class="fa fa-comments-o fa-lg" aria-hidden="true"></i>
        </div>
    </div>
<?php endif; ?>
<div class="communication-center-icon">
    <div id="notifications-icon">
        <i class="fa fa-bell fa-lg"></i>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var options = {
            access_token: <?= json_encode(Yii::app()->user->getAccessToken()) ?>,
            baseUrl: <?= json_encode(isset(Yii::app()->params['hydra_backend']) ? Yii::app()->params['hydra_backend'] : Yii::app()->request->getHostInfo()) ?>
        };

        var notifications = new NotificationBox(options);

        <?php if($this->isMenuVisible): ?>
            options.is_to_docebo = <?= json_encode($this->isToDocebo) ?>;
            options.request_label = <?= json_encode($this->requestLabel) ?>;
            options.help_desk_recipients = <?= json_encode($this->helpdeskRecipients) ?>;
            options.show_help_and_manuals = <?=json_encode($this->showHelpAndManuals) ?>;
            var communications = new CommunicationBox(options);
        <?php endif; ?>
    })
</script>
<?php endif; ?>