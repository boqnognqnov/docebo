/**
 * Created by Georgi on 3.8.2016 г..
 */
var _executeCallback =  function(){
    var callback = arguments[0];
    if(callback && typeof callback === "function"){
        var args = Array.prototype.slice.call(arguments);
        args.shift();       
        callback.apply(null, args);
    }
};

var HydraBackendResponse = function(response){
    if(response){
        this.init(response);
    }
};

HydraBackendResponse.prototype = {
    items: [],
    totalCount: 0,
    page: 0,
    totalPages: 0,
    hasMoreData: false,
    pageSize: 0,
    data: {},


    init: function(response){
        if(response && response.data){
            this.data = response.data;
            this.items = this.data.items;
            if(this.data.count){
                this.totalCount = this.data.count;
            } else{
                this.totalCount = this.data.length;
            }
            this.page = this.data.current_page;
            this.totalPages = this.data.total_page_count;
            this.hasMoreData = this.data.has_more_data;
        }
    },

    _iterate: function(callback){
        if(this.totalCount && this.totalCount > 0){
            var items = [];
            if(this.items && Object.keys(this.items).length > 0){
                items = this.items;
            } else if(Object.keys(this.data).length > 0){
                items = this.data;
            }
            for(var i = 0; i < this.totalCount; i++){
                _executeCallback(callback, i, items[i]);
            }
        }
    },

    iterate: function(callback){
        this._iterate(callback);
    },

    iterateAsynch: function(callback){
        var $this = this;
        setTimeout(function(){
            $this._iterate(callback);
        }, 0);
    }
};

var HydraBackendService = function(options){
    this._accessToken = '';
    this._baseUrl = '';
    this._error = '';

    this.STATE_DONE = 'done';
    this.STATE_ERROR = 'error';

    this.response = {};
    this.state = '';

    this.init(options);
};

HydraBackendService.prototype = {

    init: function(options){
        if(options.access_token && typeof options.access_token !== "undefined"){
            this._accessToken = options.access_token;
        }

        if(options.baseUrl && typeof options.baseUrl !== "undefined"){
            this._baseUrl = options.baseUrl;
        }
    },

    call: function(api, params, method, processData, contentType){
        var $this = this;
        this.state = '';
        if(!params){
            params = {};
        }

        if(!method){
        	method = 'GET';
        }

        if(api.charAt(0) != '/'){
        	api = '/' + api;
        }

        if(typeof processData === "undefined"){
            processData = true;
        }

        var ajaxContentType = false;

        if(typeof contentType !== "undefined"){
            ajaxContentType = contentType;
        }

        var ajaxParams = {
            type: method,
            url: $this._baseUrl + api,
            data: params,
            dataType: 'json',
            processData: processData,
            contentType: ajaxContentType,
            headers: {
                Authorization: 'Bearer ' + this._accessToken
            }
        };

        $.ajax(ajaxParams).done(function(res){
            $this.state = $this.STATE_DONE;
            if(res && res.data){
                $this.response = res;
            }
        }).fail(function(req, statusText, errorThrown){
            $this.state = $this.STATE_ERROR;
            $this._error = errorThrown;
        });

        return this;
    },

    _waitStateChange: function(callback){
        var $this = this;
        var interval = setInterval(function(){
            if($this.state !== ''){
                callback();
                clearInterval(interval);
            }
        }, 100)
    },

    success: function(callback){
        var $this = this;
        this._waitStateChange(function(){
            if($this.state === $this.STATE_DONE){
                _executeCallback(callback, $this, new HydraBackendResponse($this.response));
            }
        });

        return this;
    },

    error: function(callback){
        var $this = this;
        this._waitStateChange(function(){
            if($this.state === $this.STATE_ERROR){
                _executeCallback(callback, $this, $this._error);
            }
        });

        return this;
    },

    then: function(callback){
        _executeCallback(callback, this);
        return this;
    }
};