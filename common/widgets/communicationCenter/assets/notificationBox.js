/**
 * Created by Georgi on 4.8.2016 г..
 */

var NotificationBox = function (options) {
    if (options) {
        this.init(options);
    }
};

NotificationBox.prototype = {

    _options: {
        element: 'notifications-icon'
    },
    element: {},
    _rightPanel: {},
    _notificationRightPanelClass: 'notifications-panel',
    counterTemplate: '<div class="counter">{count}</div>',
    _notifications_count: 0,
    _notifications: {},
    _rightPanelOptions: {},
    _readedMessages: [],

    init: function (options) {
        this._options = $.extend({}, this._options, options);
        this.apiCaller = new HydraBackendService(this._options);
        this.element = $('#' + this._options.element);

        var $this = this;
        this.apiCaller.call('/notifications/v1/notification/notifications', {}, 'GET').success(function (request, response) {
            $this._notifications = response.data.items;
            if (response && response.totalCount > 0) {
                $this._notifications_count = response.totalCount;
                $this.element.append($this.counterTemplate.replace('{count}', response.totalCount));
            }

            $this.element.append("<div class='" + $this._notificationRightPanelClass + "'></div>");

            var content = $('<div>');
            $this._rightPanelOptions = {
                trigger: $this.element,
                title: Yii.t('standard', 'Notifications'),
                closeOnOverlayClick: true,
                content: content,
                show: function () {
                    $this.viewALL();
                }
            };
            response.iterate(function (index, item) {
                if (item.is_read) {
                    $this._readedMessages.push(item.id);
                }
            });

            $('.' + $this._notificationRightPanelClass).rightPanel($this._rightPanelOptions);
        }).error(function (data) {
            console.log(data);
        });
    },

    /**
     * Call API to mark the notifications as read.
     *
     * @param id integer ID of notifications
     */
    read: function (id) {
        if (id) {
            if ($.inArray(id, this._readedMessages) === -1) {
                var $this = this;
                this.apiCaller.call('/web/notifications/v1/notification/' + id + '/read', {}, 'POST').success(function (request, response) {
                    $this._readedMessages.push(id);
                });
            }
        }
    },

    /**
     * Called before the rightPanel is show the content.
     *
     * The purpose of this is to show the full list of notifications.
     *
     * You can access the notifications via `this._notifications`
     */
    viewALL: function () {
        var content = $('<div>');
        $('div.fa-chevron-left.arrowBack').hide();
        var contentDiv = $('.' + this._notificationRightPanelClass + ' .contentRightPanel');
        if ($.isEmptyObject(this._notifications)) {
            // no notifications at all
            var icon = $('<div>');
            icon.addClass('fa').addClass('fa-bell-o').addClass('fa-5x').addClass('emptyContentIcon');
            content.html($('<h3 style="color:grey">').text(Yii.t("notification", "You don't have any notification yet")));
            icon.prependTo(content);

        } else {
            var that = this;
            var ul = $('<ul class="listNotification inbox-wrapper">');
            var mainContentWidth = $('div.notifications-panel.rightPanelFlag').width();
            $.each(this._notifications, function (i, e) {
                var li = $('<li data-id="' + i + '">');
                var htmlContent = $('<div>');
                htmlContent.addClass('messageHolder');
                //var title = $('<h4>').text(e.subject);
                htmlContent.html(e.short_message);
                //htmlContent.prepend(title);
                htmlContent.css({'width': mainContentWidth - 130 + 'px', 'font-weight': '600'});
                var dateDiv = $('<div>');
                dateDiv.addClass('dateHolder');
                dateDiv.text(e.date);
                dateDiv.prependTo(htmlContent);
                li.html(htmlContent);
                var image = $('<div class="pull-left imageDiv">');
                if (e.type == 'notificationapp') {
                    image.html($('<div class="fa fa-bell" style="padding: 10px;color: white;font-size: 1.5em">'));
                }
                if (e.type == 'newsletter') {
                    image.html($('<div class="fa fa-envelope" style="padding: 10px;color: white;font-size: 1.5em">'));
                }
                var imageHolder = $('<div class="imageHolder">');
                imageHolder.append(image);
                imageHolder.prependTo(li);
                if ($.inArray(e.id, that._readedMessages) !== -1) {
                    // the notification is read
                    var icon = $('<div class="circle" style="vertical-align: middle;margin-top: 25px;background-color: grey">');
                } else {
                    // not read by now
                    var icon = $('<div class="circle" style="vertical-align: middle;margin-top: 25px">');
                }

                icon.prependTo(li);
                li.appendTo(ul);
            });
            content.append(ul);

            // add "view all" link
            var viewAllLink = $('<a href="/lms/index.php?r=inbox/inbox/all">');
            viewAllLink.text(Yii.t('notification', 'View full notifications page'));
            content.append(viewAllLink);
            contentDiv.empty();
            var mainTitle = $('.notifications-panel.rightPanelFlag h2');
            mainTitle.css('padding-left', '25px');
            mainTitle.text(Yii.t('standard', 'Notifications'));
            $(document).off('click', '.listNotification li').on('click', '.listNotification li', function () {
                var id = $(this).data('id');
                that.view(id);
            });
        }
        content.appendTo(contentDiv);
    },

    /**
     * Called when user wants to read the notifications.
     *
     * The purpose of this function is to render the content for SINGLE notifications
     *
     * @param id integer ID of the notifications
     */
    view: function (id) {
        // get chosen notification onfo
        var notification = this._notifications[id];
        var that = this;
        var contentDiv = $('.' + this._notificationRightPanelClass + ' .contentRightPanel');

        // change the title of the panel
        var containerTitle = $('.' + this._notificationRightPanelClass + ' h2');
        containerTitle.text(Yii.t('event_manager', 'New notification'));
        containerTitle.css('padding-left', '50px');

        // clear the panel content
        contentDiv.empty();

        // build new elements - back arrow, image icon, date, title and content of the notification
        var arrowBack = $('div.fa-chevron-left.arrowBack');
        if (arrowBack.length == 0)
            arrowBack = $('<div class="fa fa-chevron-left arrowBack">');
        arrowBack.show();
        arrowBack.prependTo(containerTitle.parent());
        var notificationContent = $('<div class="singleContent">');

        var image = $('<div class="imageDiv">');
        if (notification.type == 'notificationapp') {
            image.html($('<div class="fa fa-bell" style="padding: 10px;color: white;font-size: 1.5em">'));
        }
        if (notification.type == 'newsletter') {
            image.html($('<div class="fa fa-envelope" style="padding: 10px;color: white;font-size: 1.5em">'));
        }
        var imageHolder = $('<div class="imageHolder" style="position: absolute;top:53px;right: 5px">');
        imageHolder.append(image);
        imageHolder.prependTo(notificationContent);

        // date of the notification
        var dateDiv = $('<div class="dateDivInner">');
        dateDiv.text(notification.date);
        dateDiv.appendTo(notificationContent);

        // title of the notification
        var title = $('<h4 class="innerTitle">');
        title.text(notification.subject);
        title.appendTo(notificationContent);

        // content of the notification
        var innerContent = $('<div style="overflow: auto">');
        innerContent.html(notification.full_message).appendTo(notificationContent);
        notificationContent.appendTo(contentDiv);

        // event, binded to the "arrow"
        $(document).off('click', '.notifications-panel .arrowBack').on('click', '.notifications-panel .arrowBack', function () {
            that.viewALL();
        });
        that.read(notification.id);
    }
};