var tps = {
    avatarTemplate: [
        '<div class="avatar-clip {{$color}}" data-id="{{$id}}">',
            '<div class="avatar-image">',
                '<img src="{{$src}}" alt="avatar">',
            '</div>',
        '</div>'
    ],

    avatarSeverityTemplate: [
        '<div class="avatar-clip icon {{$color}}" data-id="{{$id}}">',
            '<div class="avatar-image">',
                '<i class="fa fa-{{$icon}} avatar-icon" aria-hidden="true"></i>',
            '</div>',
        '</div>'
    ],

    avatarInitialsTemplate: [
        '<div class="avatar-clip initials {{$color}}" data-id="{{$id}}">',
            '<div class="avatar-image">',
                '<b class="avatar-initial">{{$initial}}</b>',
            '</div>',
        '</div>'
    ],

    communicationRecipients: [
        '<div class="span{{$gridNumber}}">',
            '{{$avatar}}',
            '<button class="btn btn-docebo blue big {{$id}}" name="{{$name}}">{{$label}}</button>',
            '{{$info}}',
        '</div>'
    ],

    mainTemplate: [
        '<div class="communication-center main">',
            '<div class="info-box bg-grey-superlight">',
                '<p class="text-gray">' + Yii.t('standard', 'I want to contact:') + '</p>',
            '</div>',
            '<div class="buttons">',
                '<div class="row-fluid">',
                    '{{$buttons}}',
                '</div>',
            '</div>',
            '<div class="messages">',
                '{{$messages}}',
            '</div>',
        '</div>'
    ],

    messageTail: [
        '<div class="row-fluid message-tail" data-id="{{$id}}">',
            '<div class="span1 switch">',
                '<span class="switcher" {{$tooltip}}>',
                    '<i class="fa fa-circle {{$switcherIcon}}" aria-hidden="true"></i>',
                '</span>',
            '</div>',
            '<div class="span2 avatar">',
                '{{$avatar}}',
            '</div>',
            '<div class="span10 info">',
                '<div class="row-fluid">',
                    '<div class="span8">',
                        '<h4 class="message-tail-title {{$color}}">{{$title}}</h4>',
                    '</div>',
                    '<div class="span4 time">',
                        '<span class="message-tail-time text-gray">{{$time}}</span>',
                    '</div>',
                '</div>',
                '<div class="row-fluid">',
                    '<div class="span12">',
                        '<p class="message-tail-description">{{$description}}</p>',
                    '</div>',
                '</div>',
            '</div>',
        '</div>'
    ],

    balloonTemplate: [
        '<div class="container balloon">',
            '<span class="arrow {{$direction}}"></span>',
            '<div class="balloon-title">',
                '<h6 class="blue">{{$title}}</h6>',
            '</div>',
            '<div class="balloon-text">',
                '<p><i>{{$text}}</i></p>',
            '</div>',
        '</div>'
    ],

    callToActionTemplate: [
        '<a style="background-image: none; background-color:  {{$color}}" class="btn btn-docebo big call-to-action" href="{{$url}}">{{$title}}</a>'
    ],

    singleMessageTemplate: [
        '<div class="container single-message-container">',
            '<div class="avatar">',
                '{{$avatar}}',
            '</div>',
            '<div class="row-fluid">',
                '<div class="span12 time">',
                    '<span class="single-message-time text-gray">{{$time}}</span>',
                '</div>',
            '</div>',
            '<div class="row-fluid">',
                '<div class="span12">',
                    '<p class="single-message-description">{{$description}}</p>',
                '</div>',
            '</div>',
            '<div class="row-fluid">',
                '<div class="span12">',
                    '{{$callToAction}}',
                '</div>',
            '</div>',
        '</div>'
    ],

    messagesTemplate: [
        '<div class="info-box bg-gray-superlight">',
            '<p class="text-gray">' + Yii.t('standard', 'My messages history') + ':</p>',
        '</div>',
        '<div class="messages-container">',
            '{{$messages}}',
        '</div>'
    ],

    inlineTopInfo: [
        '<div class="information-balloon">',
            '<div class="row-fluid">',
                '<div class="span2">',
                    '<div class="avatar">{{$avatar}}</div>',
                '</div>',
                '<div class="span10">',
                    '<div class="information-balloon">{{$balloon}}</div>',
                '</div>',
            '</div>',
        '</div>',
    ],

    inlineTemplate: [
        '<div class="communication-center inline">',
            '{{$topBar}}',
            '<form id="communication-form" class="communication-form" enctype="multipart/form-data">',
                '<div class="control-group">',
                    '<label for="subject" class="blue">',
                        '' + Yii.t('standard', '_SUBJECT') + '',
                    '</label>',
                    '<div class="control">',
                        '<input type="text" name="subject" id="subject" placeholder="Write your subject">',
                    '</div>',
                '</div>',
                '<div class="control-group">',
                    '<div class="control">',
                        '<textarea class="empty" name="message" placeholder="' + Yii.t('standard', 'Write your request here...') + '"></textarea>',
                    '</div>',
                '</div>',
                '<div class="control-group file">',
                    '<label for="uploadBtn" class="muted">' + Yii.t('standard', 'Attach a screenshot/file to better describe your request/issue') + '</label>',
                    '<div class="controls">',
                        '<div class="file-area">',
                            '<label for="uploadBtn">Click to choose a file</label>',
                            '<i class="fa fa-cloud-upload" aria-hidden="true"></i>',
                            '<input type="file" name="uploadBtn" id="uploadBtn">',
                        '</div>',
                    '</div>',
                '</div>',
                '<div class="control-group buttons">',
                    '<button class="btn btn-docebo green big" type="submit">' + Yii.t('standard', 'Send Now') + '</button>',
                    '<button class="btn btn-docebo black big" type="button" id="cancelBtn">' + Yii.t('standard', '_CANCEL') + '</button>',
                '</div>',
            '</form>',
        '</div>'
    ],

    iconBoxTemplate: [
        '<a target="_blank" class="help-box" href="{{$url}}"><i class="zmdi zmdi-{{$icon}} zmdi-hc-3x" aria-hidden="true"></i><span class="blue">{{$text}}</span></a>'
    ],

    fabIcon: [
        '<div class="fab-icon" data-message-id="{{$id_message}}">',
            '<i class="fa fa-times close-button" aria-hidden="true"></i>',
            '<div class="balloon-box">',
                '{{$balloon}}',
            '</div>',
            '<div class="avatar">',
                '{{$avatar}}',
            '</div>',
        '</div>'
    ],
};