var CommunicationBox = function(options){
    if(options){
      this.init(options);
    }
};

CommunicationBox.prototype = {
    /**
     * Initial state of the communications panel
     */
    STATE_NONE: 'none',

    /**
     * State when Sales button is clicked
     */
    STATE_SALES: 'sales',

    /**
     * State when Help button is clicked
     */
    STATE_HELP: 'help',

    /**
     * State when showing message content
     */
    STATE_SINGLE_MESSAGE: 'single-message',

    /**
     * Response handler
     */
    _response: {},

    _options: {
        element: 'communication-icon'
    },
    element: {},
    _communicationRightPanelClass: 'communication-panel',
    _rightPanelOptions: {},
    _rightPanel: {},

    /**
     * Current State holder
     */
    _state: this.STATE_NONE,

    /**
     * Current message ID holder
     */
    _currentMessageId: 0,

    /**
     * Recent messages holder ( messages that comes form summary API)
     */
    _recentMessages: [],

    /**
     * If the infinite scroll should load more messages
     */
    _hasMoreMessages: false,

    /**
     * Current messages page holder
     */
    _currentMessagesPage: 1,

    _assigned_csm: {},
    _learning_support_team: [],
    _sales_team: [],

    _sales_button_text: Yii.t('standard', 'Presales team'),

    _snakbar: '<div id="snackbar"></div>',

    _helpdeskLinks: [
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/',
            title: Yii.t('userlimit', 'Help & Support Home'),
            icon: 'home'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/getting-started-with-docebo/',
            title: Yii.t('userlimit','Getting started'),
            icon: 'plane'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/lms-users-management/',
            title: Yii.t('standard', 'Users'),
            icon: 'users'

        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/course-management/',
            title: Yii.t('userlimit','Course Management'),
            icon: 'book'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/manage-report/',
            title: Yii.t('userlimit','Reporting Management'),
            icon: 'bar-chart'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/lms-settings/',
            title: Yii.t('userlimit','Settings'),
            icon: 'cog'
        },
        {
            link: 'http://www.docebo.com/lms-elearning-modules-integrations/',
            title: Yii.t('userlimit','Apps'),
            icon: 'cubes'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/integrations-lms/',
            title: Yii.t('userlimit','Integrations'),
            icon: 'link'
        },
        {
            link: 'http://www.docebo.com/elearning-knowledge-base-support/payment-billing-policies/',
            title: Yii.t('userlimit','Payments and Billing Policies'),
            icon: 'money'
        }
    ],

    /**
     * Build HTML content for initial state of communication panel
     *
     * @returns {*}
     */
    getMainContent: function(){
        //Get the avatars HTML
        var avatars = this.buildAvatars();

        //Get messages HTML
        var messages = this.buildMessages();

        var help_desk_recipients = this._options.help_desk_recipients;
        var recipients = '';

        if(this._options.is_to_docebo){
            recipients += this.parseTemplate(tps.communicationRecipients, {
                gridNumber: 6,
                avatar: '<div class="avatar" id="help-team-avatars">' + avatars.help + '</div>',
                label: Yii.t('standard', 'Learning & Support Team'),
                info: '<p class="text-gray muted">' + Yii.t('standard', 'For reporting bugs or technical requests') + '</p>',
                id: 'helpDeskButton'
            });
            recipients += this.parseTemplate(tps.communicationRecipients, {
                gridNumber: 6,
                avatar: '<div class="avatar" id="sales-team-avatars">' + avatars.sales + '</div>',
                label: this._sales_button_text,
                info: '<p class="text-gray muted">' + Yii.t('standard', 'For commercial inquiries and product demo') + '</p>',
                id: 'salesButton'
            });
        } else{
            if(Array.isArray(recipients)){
                if(help_desk_recipients && help_desk_recipients.length > 0) {
                    var len = help_desk_recipients.length;
                    for(var i in help_desk_recipients){
                        if(help_desk_recipients[i].length > 0){
                            recipients += this.parseTemplate(tps.communicationRecipients, {
                                id: 'help-desk-custom',
                                name: help_desk_recipients[i],
                                label: help_desk_recipients[i],
                                avatar: '',
                                info: '',
                                gridNumber: 12 / len
                            });
                        }
                    }
                }
            } else if(typeof help_desk_recipients === 'object'){
                var objectKeys = Object.keys(help_desk_recipients);
                for(var i in objectKeys){
                    if(help_desk_recipients.hasOwnProperty(objectKeys[i])){
                        recipients += this.parseTemplate(tps.communicationRecipients, {
                            id: 'help-desk-custom',
                            name: objectKeys[i],
                            label: help_desk_recipients[objectKeys[i]],
                            avatar: '',
                            info: '',
                            gridNumber: 12 / objectKeys.length
                        });
                    }
                }
            }
        }

        return this.parseTemplate(tps.mainTemplate, {
            buttons: recipients,
            messages: messages
        });
    },

    /**
     * Get approximately how messages we can display in the remaining space
     *
     * @NOTE We know that the elements before the messages are 296px height
     * @NOTE We know that a single message is 81px height
     *
     * @returns {number}
     */
    calculateNumberOfMessages: function(){
        //Get document height, because the right panel is absolute and his height is 100%
        var height = $(document).height();
        //Get the remaining space
        var messagesContainerHeight = height - 296;

        //Return approximately number of messages that the remaining space can handle
        //@NOTE It will be good to have 1 more, because of the infinite scroll
        return Math.ceil(messagesContainerHeight / 81);
    },

    /**
     * Call once when the component is initialized
     *
     * @param options
     */
    init: function(options){
        var $this = this;
        this._options = $.extend({}, this._options, options);
        this.apiCaller = new HydraBackendService(this._options);
        this.element = $('#' + this._options.element);

        //Get the recent messages
        $this.apiCaller.call('/notifications/v1/communications/summary', {
            //Set the page size
            page_size: this.calculateNumberOfMessages()
        }).success(function(service){
            //Extract data from the response
            $this.extractData(service.response);

            if($('.' + $this._communicationRightPanelClass).length == 0){
                $this.element.append("<div class='" + $this._communicationRightPanelClass + "'></div>");
            }

            $this._rightPanelOptions = {
                trigger: $this.element,
                title: Yii.t('standard', 'Communication center'),
                closeOnOverlayClick: true,
                content: $this.getMainContent(),
                onBack: function(){
                    $this.onBack(this);
                },
                onNext: function(){
                    $this.onNext(this);
                }
            };

            // Initialize Right Panel
            $('.' + $this._communicationRightPanelClass).rightPanel($this._rightPanelOptions);

            //Show FAB button
            $this.showFab();

            //Add global listeners
            $this.addListeners();

            //Initialize infinite scroll
            $this.infiniteScrollListener();

        });
    },

    /**
     * Hide the FAB icon base on message ID
     *
     * @param id_message
     */
    hideFab: function(id_message){
        if(id_message){
            var fabIcon = $(document).find('.fab-icon[data-message-id=' + id_message + ']');

            if(fabIcon.length == 1){
                fabIcon.remove();
            }
        }
    },

    /**
     * Show special FAB button with the last UNREAD message
     *
     * The messages is shown only one per loading page
     */
    showFab: function(){
        var $this = this;
        var messagesLen = this._recentMessages.length;

        /**
         * Loop thought the messages and find the first unread meassage
         */
        for(var i = 0; i < messagesLen; i++){
            if(!this._recentMessages[i].is_read || this._recentMessages[i].is_read == 0){
                var avatarHTML = '';
                //If the message has a avatar image
                if(this._recentMessages[i].avatar_url){
                    avatarHTML += this.parseTemplate(tps.avatarTemplate, {
                        color: 'blue',
                        src: this._recentMessages[i].avatar_url,
                        id: this._recentMessages[i].id_message
                    });
                } else{
                    if(this._recentMessages[i].severity){
                        var icon = 'b';
                        var color = 'blue';
                        var isInitials = true;
                        /**
                         * Determinate what is the severity of the message and build the Avatar template
                         */
                        switch (this._recentMessages[i].severity){
                            case 'warning':
                                icon = 'exclamation-triangle';
                                color = 'orange';
                                isInitials = false;
                                break;
                            case 'error':
                                icon = 'exclamation';
                                color = 'red';
                                isInitials = false;
                                break;
                        }

                        if(isInitials){
                            /**
                             * Avatar template with initials
                             */
                            avatarHTML = this.parseTemplate(tps.avatarInitialsTemplate, {
                                color: color,
                                initial: icon,
                                id: this._recentMessages[i].id_message
                            });
                        } else{
                            /**
                             * Avatar template with severity icon
                             */
                            avatarHTML = this.parseTemplate(tps.avatarSeverityTemplate, {
                                color: color,
                                icon: icon,
                                id: this._recentMessages[i].id_message
                            });
                        }
                    }
                }

                //Get FAB icon HTML
                var fabIcon = this.parseTemplate(tps.fabIcon, {
                    balloon: this.parseTemplate(tps.balloonTemplate, {
                        title: this._recentMessages[i].title,
                        text: this._recentMessages[i].short_message,
                        direction: 'bottom'
                    }),
                    avatar: avatarHTML,
                    id_message: this._recentMessages[i].id_message
                });

                //Append the FAB icon HTML to the BODY element
                $('body').append(fabIcon);

                /**
                 * Listener for balloon and the avatar
                 */
                $(document).on('click', '.fab-icon .balloon-box, .fab-icon .avatar', function(e) {
                    var id = $(this).parent().find('.avatar-clip').attr('data-id');
                    $this._currentMessageId = id;
                    $('.' + $this._communicationRightPanelClass).rightPanel('show');
                    $('.' + $this._communicationRightPanelClass).rightPanel('next');
                    $(this).parent().hide();
                });

                /**
                 * Listener for the close button
                 */
                $(document).on('click', '.fab-icon .close-button', function(){
                    $(this).hide();
                    $(this).parent().find('.balloon-box').hide();
                });
                break;
            }
        }
    },

    /**
     * Build Avatar HTML for message
     *
     * It finds if the message has a avatar image or is severity or initials
     *
     * If returnColor is true, it will return a object contains:
     * color: the color of the avatar and
     * avatarHtml: the HTML of the avatar
     *
     * @param messageId
     * @param returnColor
     * @param isSmall
     * @returns {*}
     */
    buildMessageAvatar: function(messageId, returnColor){
        var avatarHtml = '';
        var color = 'blue';
        var message = this.getMessageData(messageId);

        if(message .avatar_url){
            avatarHtml = this.parseTemplate(tps.avatarTemplate, {
                src: message .avatar_url
            });
        } else{
            var icon = '';
            var isInitials = false;
            switch (message .severity){
                case 'info':
                default:
                    icon = 'd';
                    isInitials = true;
                    break;
                case 'warning':
                    icon = 'exclamation-triangle';
                    color = 'orange';
                    break;
                case 'error':
                    icon = 'exclamation';
                    color = 'red';
                    break;
            }
            if(!isInitials){
                avatarHtml = this.parseTemplate(tps.avatarSeverityTemplate, {
                    icon: icon,
                    color: color
                });
            } else{
                avatarHtml = this.parseTemplate(tps.avatarInitialsTemplate, {
                    color: color,
                    initial: icon
                })
            }
        }

        if(returnColor && typeof returnColor !== "undefined"){
            return {
                color: color,
                avatarHtml: avatarHtml
            };
        }

        return avatarHtml;
    },

    /**
     * Build HTML for "recent messages"
     *
     * Function the the messages from _recentMessages.
     * If customMessages provided, they will be used.
     * If returnContainer is true, the HTML result will also contains the container of the messages
     *
     * @param customMessages
     * @param returnContainer
     * @returns {string}
     */
    buildMessages: function(customMessages, returnContainer){
        var result = '';
        var messages = '';
        var messagesToBuild = this._recentMessages;

        //If customMessages provided - replace them
        if(typeof customMessages !== "undefined"){
            messagesToBuild = customMessages;
        }

        if(messagesToBuild){
            var msg_length = messagesToBuild.length;

            for(var i = 0; i < msg_length; i++){
                //Build Avatar template
                var avatar = this.buildMessageAvatar(messagesToBuild[i].id_message, true, false);

                //Build Message Tail template
                messages += this.parseTemplate(tps.messageTail, {
                    id: messagesToBuild[i].id_message,
                    switcherIcon: messagesToBuild[i].is_read == '1' ? 'read' : 'green',
                    avatar: avatar.avatarHtml,
                    title: messagesToBuild[i].title,
                    time: messagesToBuild[i].publish_date,
                    description: messagesToBuild[i].short_message,
                    color: avatar.color,
                    tooltip: messagesToBuild[i].is_read != '1' ? 'data-toggle="tooltip" rel="tooltip" title="Mark as read" data-placement="top"' : ''
                });
            }

            if(typeof returnContainer === "undefined" || returnContainer !== false){
                //Return the messages in container
                result += this.parseTemplate(tps.messagesTemplate, {
                    messages: messages
                });
            } else{
                result = messages;
            }
        }

        return result;
    },

    /**
     * Create two objects that are representing the HTML of the avatars of the learning support team and sales team
     *
     * @returns {{sales: string, help: string}}
     */
    buildAvatars: function(){
        /**
         * Get Learning Support Team avatars HTML
         */
        var learning_support_team_avatars = '';
        if(this._learning_support_team.length > 0){
            var lsp_length = this._learning_support_team.length;
            for(var i = 0; i < lsp_length; i++){
                learning_support_team_avatars += this.parseTemplate(tps.avatarTemplate, {
                    src: this._learning_support_team[i],
                    color: 'blue'
                });
            }
        }

        /**
         * Get Sales Team avatars HTML
         */
        var sales_team_avatars = '';

        //Check if the LMS has a Personal assigned CSM
        if(!jQuery.isEmptyObject(this._assigned_csm)){

            /**
             * Set the sales team button to be the name of the CSM
             */
            if(this._assigned_csm.name){
                this._sales_button_text = Yii.t('standard', '{name}, my csm', {name: this._assigned_csm.name});
            }

            if(this._assigned_csm.avatar){
                sales_team_avatars += this.parseTemplate(tps.avatarTemplate, {
                    src: this._assigned_csm.avatar,
                    color: 'blue'
                });
            }
        } else{
            /**
             * We don't have assigned CSM and we show the sales team avatars.
             *
             * @NOTE The priority is assigned CSM
             */
            if(this._sales_team.length > 0){
                var st_length = this._sales_team.length;
                for(var i = 0; i < st_length; i++){
                    sales_team_avatars += this.parseTemplate(tps.avatarTemplate, {
                        src: this._sales_team[i],
                        color: 'blue'
                    });
                }
            }
        }

        return {
            sales: sales_team_avatars,
            help: learning_support_team_avatars
        };
    },

    /**
     * Extract data from the response
     *
     * @param response
     */
    extractData: function(response){
        if(response.data && response.data.count > 0){
            this._hasMoreMessages = response.data.has_more_data;
            this._currentMessagesPage = response.data.current_page;
            if(response.data.items){
                this._recentMessages = response.data.items;
            }
        }

        //Extract extra_data
        if(response.extra_data){
            var extraData = response.extra_data;

            if(extraData.assigned_csm){
                this._assigned_csm = extraData.assigned_csm;
            }

            if(extraData.learning_support_team){
                this._learning_support_team = extraData.learning_support_team;
            }

            if(extraData.sales_team){
                this._sales_team = extraData.sales_team;
            }
        }
    },

    /**
     * Get message data for specific message with provided ID (id_message)
     * If returnIndex is true, it will be returned the internal index of the message
     *
     * @param id
     * @param returnIndex
     * @returns {*}
     */
    getMessageData: function(id, returnIndex){
        var result = {};
        var _index = 0;
        var $this = this;

        jQuery.each(this._recentMessages, function(index, value){
            if(value.id_message && value.id_message == id){
                result = $this._recentMessages[index];
                _index = index;
                return false;
            }
        });

        if(returnIndex){
            return _index;
        }

        return result;
    },

    /**
     * Handler when the Right panel trigger "next" event
     *
     * @param panel
     */
    onNext: function(panel){
        var title = '';
        var html = '';
        //If some massage is selected
        if(this._currentMessageId > 0){
            //Get message data
            var message = this.getMessageData(this._currentMessageId);
            if(message){
                title = message.title;

                var callToAction = '';
                if(message.call_to_action){
                    //Build Call to action template
                    callToAction += this.parseTemplate(tps.callToActionTemplate, {
                        color: message.call_to_action.bg_color,
                        url: message.call_to_action.url,
                        title: message.call_to_action.title
                    });
                }

                //Build Message template
                html = this.parseTemplate(tps.singleMessageTemplate, {
                    avatar: this.buildMessageAvatar(this._currentMessageId, false, true),
                    callToAction: callToAction,
                    description: message.full_message,
                    time: message.publish_date
                });
            }

            //Mark the message as readed
            this.markAsRead(this._currentMessageId);

            //Reset the current message
            this._currentMessageId = 0;
        } else{
            /**
             * Get here if some of the buttons (help or sales) is clicked
             */

            var botTitle = Yii.t('standard', 'Help Desk');

            var botText = '';
            if(this._state == this.STATE_HELP){
                title = Yii.t('standard', 'Help Desk');
                botTitle = Yii.t('standard', 'Help Dest team');
                botText = Yii.t('standard', 'Direct your request to the Learning and support Team for reporting bugs or technical requests');
            } else if(this._state == this.STATE_SALES){
                botTitle = Yii.t('standard', 'Pre-Sales team');
                botText = "Direct your request to the Sales team for commercial inquiries and product demos.<br><br> <b>Please note that</b> contacting the Sales Team for any technical request/bug reporting, will increase the answer time. <b>The fastest and right way is </b><a href='#'>contacting the Learning and Support Team.</a>";
            } else{
                title = this._state;
                if(typeof this._options.request_label !== 'undefined' && this._options.request_label){
                    botText = this._options.request_label;
                }
            }

            var topBar = this.parseTemplate(tps.inlineTopInfo, {
                avatar: this.parseTemplate(tps.avatarInitialsTemplate, {
                    color: 'blue',
                    initial: 'd'
                }),
                balloon: this.parseTemplate(tps.balloonTemplate, {
                    title: botTitle,
                    text: botText
                })
            });

            //Build inline template with the avatar of the doceboBot
            html = this.parseTemplate(tps.inlineTemplate, {
                topBar: topBar,
            }) + this._snakbar;

            if(this._state == this.STATE_SALES){
                title = Yii.t('standard', 'Contact the Pre-Sales Team');
            }

            if(this._options.show_help_and_manuals){
                //Append the HelpDesk Links
                html += this.buildHelpDeskLinks();
            }
        }

        //Set the title to the Right Panel
        panel.setTitle(title);

        //Set the content of the Rightr Panel
        panel.setContent(html);
    },

    /**
     * Handler when the Right Panel is triggering "back" event
     *
     * @param panel
     */
    onBack: function(panel){
        //Set the initial content to the Right Pane;
        panel.setContent(this.getMainContent());

        //Initialize the tooltips of the unreaded messages
        $('.message-tail .switcher').tooltip();

        /**
         * Initialize the infinite scroll again, because the content has changed
         */
        this.infiniteScrollListener();
    },

    /**
     * Parse template to HTML
     * To the function can be passed arguments, that are representing {{$*}} in the template (binding)
     *
     * @param template
     * @returns {string}
     */
    parseTemplate: function(template){
        var templateLen = template.length;
        var result = '';
        if(templateLen > 0){
            for(var i = 0; i < templateLen; i++){
                result += template[i];
            }
        }

        //Make a interpolation
        jQuery.each(arguments, function(index, value) {
            if(typeof arguments[index] === 'object'){
                for(var key in value){
                    if(value.hasOwnProperty(key)){
                        result = result.replace('{{$' + key + '}}', value[key]);
                    }
                }
            }
        });

        return result;
    },

    /**
     * Mark specific message as read
     * Call the read API
     * Also set the class of the "switcher" to read - that means that the message is readed
     *
     * @param id
     */
    markAsRead: function(id){
        var $this = this;
        var messageIndex = $this.getMessageData(id, true);
        if(!this._recentMessages[messageIndex].is_read || this._recentMessages[messageIndex].is_read == 0) {
            useAjaxPrefilter = false;
            this.apiCaller.call('/notifications/v1/communications/messages/' + id + '/read', {}, 'PUT').success(function (service) {
                if (service.response) {
                    $this._recentMessages[messageIndex].is_read = true;
                    var message = $('.' + $this._communicationRightPanelClass).find('.message-tail[data-id=' + id + ']');
                    message.find('.switcher i.green').removeClass('green').addClass('read');
                    $this.hideFab(id);
                }
                useAjaxPrefilter = true;
            });
        }
    },

    /**
     * Add listeners to the Communication Center Panel
     */
    addListeners: function(){
        var $this = this;

        /**
         * Listen for this event and open the Center at requested state
         */
        $(document).on('communication-center.open', function(event, state){
            $('.' + $this._communicationRightPanelClass).rightPanel('show');
        	if (typeof state !== "undefined" && ($this._state != state)) {
        		$this._state = state;
            	$('.' + $this._communicationRightPanelClass).rightPanel('next');
        	}
        });

        
        
        //Listener of Help Button
        $(document).on('click', 'button.helpDeskButton', function(){
        	console.log(101);
            $this._state = $this.STATE_HELP;
            $('.' + $this._communicationRightPanelClass).rightPanel('next');
        });

        //Listener for Sales Button
        $(document).on('click', 'button.salesButton', function(){
        	console.log(102);
            $this._state = $this.STATE_SALES;
            $('.' + $this._communicationRightPanelClass).rightPanel('next');
        });

        $(document).on('click', 'button.help-desk-custom', function(){
        	console.log(103);
            $this._state = $(this).attr('name');
            $('.' + $this._communicationRightPanelClass).rightPanel('next');
        });

        //Intitialze the unreaded messages tooltip
        $('.message-tail .switcher').tooltip();

        //Listener when a recent message is clicked
        $(document).on('click', '.message-tail', function(e){
            var id = $(this).attr('data-id');
            var target = $(e.target);

            //Mark the message as read
            $this.markAsRead(id);

            //If the target is the "mark as read" button, do not continue
            if(target.hasClass('fa-circle') && target.hasClass('green')){
                return true;
            }
            $this._currentMessageId = id;
            //Set the current state to be message
            $this._state = $this.STATE_SINGLE_MESSAGE;

            //Trigger "next" to the Right Panel
            $('.' + $this._communicationRightPanelClass).rightPanel('next');
        });

        //Listener when the upload button select a file
        $(document).on('change', 'form#communication-form #uploadBtn', function() {
        	console.log(104);
            if(this.files && this.files.length == 1){
                //Get the file
                var file = this.files[0];

                //Set the label text to be the name of the file
                $(this).parent().find('label').text(file.name);
            }
        });
        
        //Listener the back button in the form will be clicked, and triggers rightPanel's back()
        $(document).on('click', 'form#communication-form #cancelBtn', function() {
            $('.' + $this._communicationRightPanelClass).rightPanel('back');
        });

        //Listener when the form is submitet - both help and sales
        $(document).on('submit', 'form#communication-form', function(){
            var message = $(this).find('textarea[name=message]').val();
            var subject = $(this).find('input[name=subject]').val();
            var fileUploader = document.getElementById('uploadBtn');
            var formData = new FormData();
            var messageText = '';

            /**
             * Set the GLOBAL variable to false, because the ajaxPreFilter should not interact when sending files
             * thought ajax
             *
             * @type {boolean}
             */
            useAjaxPrefilter = false;

            if(message.length <= 0){
                /**
                 * Show error message if no message text is provided
                 */
                $this.openSnackbar(Yii.t('standard', 'Please, enter a message!'), 'error');
                return false;
            }

            if(subject.length > 0){
                /**
                 * If subject is provided - append it to the message body
                 * @type {string}
                 */
                messageText += 'Subject: ' + subject;
            }

            messageText += ' Message: ' + message;

            //Append the message
            formData.append('message', messageText);
            //Append the current state
            formData.append('type', $this._state);

            if(fileUploader.files.length > 0){
                /**
                 * Append the file if selected
                 */
                var file = fileUploader.files[0];
                formData.append('file', file);
            }

            //Call the API
            $this.apiCaller.call('/manage/v1/site/helpDesk', formData, 'POST', false).success(function(service){
                if(service.response && service.response.data){
                    //Show the snack bar if response is OK
                    $this.openSnackbar(service.response.data.message, service.response.data.success ? 'success' : 'error');
                }
            });

            return false;
        });
    },

    /**
     * Function add listeners for infinite scrolling of the recent messages
     */
    infiniteScrollListener: function(){
        var inScroll = false;
        var $this = this;

        $('.messages-container').on('scroll', function(){

            //Get the last message from tha tail
            var lastMessage = $(this).find('.message-tail:last-child');
            var viewTop = $(window).scrollTop();
            var viewBottom = viewTop + $(window).height();
            var elemTop = lastMessage.offset().top;
            var elemBottom = elemTop + lastMessage.height();

            var messagesContainer = $(this);


            //Determinate if the last element is visible
            if((elemBottom <= viewBottom) && (elemTop >= viewTop)){
                //The page count is the current messages that we have
                var messagesCount = $this._recentMessages.length;
                //If not in API call already
                if(!inScroll){
                    if($this._hasMoreMessages) {
                        //Call the API to get the next messages
                        $this.apiCaller.call('/notifications/v1/communications/messages', {
                            page_size: messagesCount,
                            page: $this._currentMessagesPage + 1
                        }).success(function (service) {
                            if (service.response && service.response.data && service.response.data.count > 0) {
                                $this._hasMoreMessages = service.response.data.has_more_data;
                                if (service.response.data.items) {
                                    var items = service.response.data.items;
                                    $this._recentMessages = $this._recentMessages.concat(items);

                                    //Build the new messages HTML
                                    var newMessagesHTML = $this.buildMessages(items, false);
                                    messagesContainer.append(newMessagesHTML);
                                }
                            }

                            //Release the infinite scroll
                            inScroll = false;
                        }).then(function(){
                            //Lock the scroll
                            inScroll = true;
                        });
                    }
                }
            }
        });
    },

    /**
     * Opens a snackBar message
     *
     * @param text
     * @param status
     */
    openSnackbar: function(text, status){
        var icon = 'fa-times';
        var $this = this;

        if(status == 'success'){
            icon = 'fa-check';
        }

        $('.' + $this._communicationRightPanelClass).find('#snackbar').html('<i class="fa ' + icon + ' fa-2x close-snackbar" aria-hidden="true"></i><p>' + text + '</p>');
        $('.' + $this._communicationRightPanelClass).find('#snackbar').find('i.close-snackbar').click(function () {
            $(this).parent().removeClass('show');
        });
        $('.' + $this._communicationRightPanelClass).find('#snackbar').addClass('show');
        $('.' + $this._communicationRightPanelClass).find('#snackbar').addClass(status);
        setTimeout(function(){
            $('.' + $this._communicationRightPanelClass).find('#snackbar').removeClass('show');
        }, 5000);
    },

    /**
     * Build HTML for Help Desk links
     * @returns {string}
     */
    buildHelpDeskLinks: function(){
        var linksLen = this._helpdeskLinks.length;
        var result = '<div class="clearfix"></div><div class="manuals"><h5>' + Yii.t('userlimit', 'Help & Manuals') + '</h5><div class="container">';

        for(var i = 0; i < linksLen; i++){
            result += this.parseTemplate(tps.iconBoxTemplate, {
                url: this._helpdeskLinks[i].link,
                icon: this._helpdeskLinks[i].icon,
                text: this._helpdeskLinks[i].title
            });
        }

        return result;
    }
};