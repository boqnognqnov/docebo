<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

/**
 * Class mainMenu
 * This is the main menu of the platform, is composed by various pieces that are selected and showed based
 * on the level and characteristic of te user .
 * The elements area
 * - Home (All)
 * - Course chapter (if inside a course)
 * - Manage courses (if inside the course and teacher or god-admin)
 * - Admin (for admin adn goadmin)
 * - upgrade plan (only for godadmin)
 * - course marketpalce (only for godadmin)
 * - apps (only for godadmin)
 * - helpdesk (only for goadmin)
 * - language (All)
 * - logout (All, but there is a config option to hide it)
 */
class MainMenu extends CWidget {

	protected $all = TRUE;
	protected $godadmin = FALSE;
	protected $admin = FALSE; // If true, current user is a Power User
	protected $admin_has_course = FALSE; //used only for power users, check PU current course assignment
	protected $is_saas = FALSE;
	protected $is_in_player = FALSE;
	protected $course_id = FALSE;
	protected $is_in_training_res = FALSE;
	protected $enable_user_billing = true;
	protected $demo_platform = false;
	protected $globalSearchMinScore = 0.5;  // ElasticSearch minimal relevance score

	// List of URL/GET params to preserve (if present) in the URL
	// when the language is switched
	protected $whitelistedParams = array('id_object', 'id_test', 'id_poll', 'id'/*Multidomain id*/, 'user_id', 'question_type', 'id_question' );

	// Menu Navigation URLs
	protected $buyMoreUrl = "";
	protected $buyNowUrl = "";
	protected $appsUrl = "";
	protected $gotoMarketplaceUrl = "";
	protected $helpUrl = "";

	// Player interanl pages
	protected $usercourse_level 			= 0;
	protected $courseLoStatsHtmlUrl 		= "";
	protected $userCanAdminPlayerCourse		= FALSE;
	protected $courseTrainingResourcesUrl 	= "";
	protected $viewCourseUrl				= "";
	protected $courseEnrollmentsUrl 	  	= "";
	protected $courseReportsUrl 			= "";
	protected $courseSettingsUrl 			= "";
	protected $sessionEnrollmentsUrl 	  	= "";
	protected $reportSummaryUrl      	  	= "";
	protected $showUsersInfo				= FALSE;
	protected $activeUsers					= 0;
	protected $boughtUsers					= 0;
	protected $enableBilling				= FALSE;

	protected $subscription 				= array();
	protected $manuals 						= array();
	protected $adminButtons						= array();

	protected $spinnerLoading               = '';

	/**
	 * Builds the buttons array for the admin menu
	 */
	private function populateButtonsArray() {

		// New user button
		if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add')) {
			$this->adminButtons[] = array(
				'fa-icon' => array(
					array('icon' => 'fa-user', 'stacking' => 'fa-stack-2x new-user'),
					array('icon' => 'fa-plus-circle', 'stacking' => 'fa-stack-1x new-user')
				),
				'url' => Docebo::createAdminUrl('userManagement/index', array('open' => 'new-user')),
				'label' => Yii::t('standard', '_NEW_USER')
			);
		}

		// New course
	 	if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/course/add')) {
			$this->adminButtons[] = array(
				'fa-icon' => array(
					array('icon' => 'fa-book', 'stacking' => 'fa-stack-2x new-course'),
					array('icon' => 'fa-plus-circle', 'stacking' => 'fa-stack-1x new-course')
				),
				'url' => Docebo::createAdminUrl('courseManagement/index', array('open' => 'new-course')),
				'label' => Yii::t('standard', '_NEW_COURSE')
			);
		}

		// Enroll users to courses
		if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/create')) {
			$this->adminButtons[] = array(
				'fa-icon' => array(
					array('icon' => 'fa-user', 'stacking' => 'fa-stack-2x enroll'),
					array('icon' => 'fa-check', 'stacking' => 'fa-stack-1x enroll')
				),
				'url' => Docebo::createAdminUrl('courseManagement/index', array('open' => 'enroll-multiple')),
				'label' => Yii::t('standard', 'Enroll users')
			);
		}

		// Import from CSV
		if(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add')) {
			$this->adminButtons[] = array(
				'fa-icon' => array (
					array('icon' => 'fa-inbox', 'stacking' => 'fa-stack-2x import-csv'),
					array('icon' => 'fa-caret-down', 'stacking' => 'fa-stack-1x import-csv')
				),
				'url' => Docebo::createAdminUrl('userManagement/importUser'),
				'label' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_USERS')
			);
		}

		// Google import
		$showGoogleImport = (PluginManager::isPluginActive('GoogleappsApp') || PluginManager::isPluginActive('googleapps') || PluginManager::isPluginActive('gapps'));
		if(isset($showGoogleImport) && $showGoogleImport && (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/view'))) {
			$this->adminButtons[] = array(
				'fa-icon' => array(
					array('icon' => 'fa-google', 'stacking' => 'fa-stack-2x import-google')
				),
				'url' => Docebo::createAdminUrl('userManagement/syncGappsUsers'),
				'label' => Yii::t('user_management', 'Import users from Google Apps')
			);
		}
	}

	/**
	 * Initialize the widget with the data needed to draw the main menu
	 */
 	public function init() {
		parent::init();

		$this->spinnerLoading       = '<i class="fa fa-spinner fa-pulse"></i>   ' . Yii::t('standard', '_LOADING') . '...';


		$this->all 					= TRUE;
		$this->course_id 			= Yii::app()->request->getParam('course_id', FALSE);
		$this->godadmin 			= Yii::app()->user->isGodadmin;
		$this->admin 					= Yii::app()->user->isAdmin;
		$this->is_saas 				= Docebo::isSaas();
		$this->is_in_player			= Yii::app()->player->course != NULL;
		$this->is_in_training_res 	= (Yii::app()->controller->id == 'training') && $this->is_in_player;
		$this->enable_user_billing  = Settings::get('enable_user_billing', 'on') == 'on';
		$this->demo_platform		= Settings::get('demo_platform', 0) == 1;

		$this->showUsersInfo 		= $this->godadmin && $this->is_saas && !Docebo::isTrialPeriod() && BrandingWhiteLabelForm::isMenuVisible();
		$this->activeUsers			= Docebo::getActiveUsers();
		$this->boughtUsers			= Settings::get('max_users', 0);
		$this->enableBilling		= ((Settings::get('enable_user_billing', 'on') == 'on') || isset(Yii::app()->request->cookies['reseller_cookie'])) && !$this->demo_platform;

		$this->populateButtonsArray();

		// Special cases

		// This action uses 'id' as course ID URL parameter
		if (strtolower(Yii::app()->controller->route) == 'coursemanagement/enrollment') {
			$this->course_id = Yii::app()->request->getParam('id', FALSE);
		}

		//check power user current course assignment
		if ($this->admin) {
			$hasCourse = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $this->course_id
			));
			$this->admin_has_course = !empty($hasCourse);
		}

	    if($this->godadmin){
		    Yii::import('common.components.sandboxCreator.*');
		    Yii::app()->clientscript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias('common.components.sandboxCreator.assets')).'/sandboxCreator.css');
	    }

		// Player related
		if ($this->is_in_player) {
			$this->usercourse_level 				= LearningCourseuser::userLevel(Yii::app()->user->id, Yii::app()->player->course->idCourse);
			$this->courseLoStatsHtmlUrl				= Docebo::createLmsUrl("/player/training/AxGetLoCountersHtml", array('course_id' => Yii::app()->player->course->idCourse));
			$this->courseTrainingResourcesUrl 		= Docebo::createLmsUrl("/player/training", 		array('course_id' => Yii::app()->player->course->idCourse, 'ref_src' => 'admin'));
			$this->viewCourseUrl 					= Docebo::createLmsUrl("/player/training", 		array('course_id' => Yii::app()->player->course->idCourse, 'mode' => 'view_course'));
			$this->courseEnrollmentsUrl 			= Docebo::createAdminUrl("courseManagement/enrollment", 	array('id' => Yii::app()->player->course->idCourse, 'course_id' => Yii::app()->player->course->idCourse));
			$this->courseReportsUrl 				= Docebo::createLmsUrl("/player/report", 		array('course_id' => Yii::app()->player->course->idCourse));
			$this->courseSettingsUrl 				= Docebo::createLmsUrl("/player/coursesettings",	array('course_id' => Yii::app()->player->course->idCourse));
			$this->userCanAdminPlayerCourse			= ($this->usercourse_level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR) || $this->godadmin || $this->admin_has_course;

			if(PluginManager::isPluginActive('ClassroomApp') && (Yii::app()->player->course->course_type == LearningCourse::TYPE_CLASSROOM))
			{
				$this->sessionEnrollmentsUrl = Docebo::createAppUrl("lms:player/training/session", array('course_id' => Yii::app()->player->course->idCourse));
				$this->reportSummaryUrl = Docebo::createAppUrl("admin:reportManagement/classroomCourseReport", array('course_id' => Yii::app()->player->course->idCourse));
			}
			else if (Yii::app()->player->course->course_type == LearningCourse::TYPE_WEBINAR)
			{
				$this->sessionEnrollmentsUrl = Docebo::createAppUrl("lms:player/training/webinarSession", array('course_id' => Yii::app()->player->course->idCourse));
				$this->reportSummaryUrl = Docebo::createAppUrl("admin:reportManagement/webinarCourseReport", array('course_id' => Yii::app()->player->course->idCourse));
			}
		}

		// platform management links
		$this->buyMoreUrl			= Docebo::createLmsUrl('billing/default/axBuyMenuContent'); // '../doceboLms/index.php?r=adm/userlimit/buymore';
		$this->buyNowUrl			= Docebo::createLmsUrl('billing/default/plan');
		$this->appsUrl 				= '' . Docebo::createLmsUrl('app/index');
		$this->gotoMarketplaceUrl 	= '' . Docebo::createLmsUrl('mainmenu/marketplace');// '../doceboLms/index.php?r=lms/marketplace/gotoMarketplace';
		$this->helpUrl 				= '' . Docebo::createLmsUrl('mainmenu/docebohelp');

		// subscription info
		$this->subscription = array(
			'is_trial' 		=> Docebo::isTrialPeriod(),
			'active_users' 	=> Docebo::getActiveUsers(),
			'max_users' 	=> Docebo::getMaxUsers(),
		);

		// manuals
		if (Yii::app()->getLanguage() == 'it') {
			$this->manuals = array(
				Yii::t('userlimit', 'Admin manual (eng)') => 'http://www.docebo.com/wp-content/uploads/2012/09/Docebo_AdminManual_ITA.pdf',
			);
		} else {
			$this->manuals = array(
				Yii::t('userlimit', 'Admin manual (eng)') => 'http://www.docebo.com/wp-content/uploads/2012/09/Docebo_AdminManual_ENG.pdf',
			);
		}

	}

    /**
     * Checks whether the helpdesk button must be visible even for non goadmins
     * @return bool
     */
    protected function isHelpdeskButtonVisible() {
        $result = false;
        $event = new DEvent($this, array());
        Yii::app()->event->raise('ShouldDisplayHelpdeskButton', $event);
        if(!$event->shouldPerformAsDefault())
            $result = $event->return_value['show'];

        return $result;
    }


	/**
	 * Return the list of menu voices to be placed inside the home menu
	 * @return array
	 */
	protected function getHomeItems() {
	    
	    $icons = array(
	        '_MYCOURSES' => 'my-courses',
	        '_CATALOGUE' => 'catalog',
	        '_MY_CERTIFICATE' => 'certificate',
	        '_COURSEPATH' =>'curricula',
	        '_PUBLIC_FORUM' =>'community',
	        '_MYCOMPETENCES' => 'competencies',
	        '_COURSE_AUTOREGISTRATION' => 'activate',
	        '_MESSAGES' => 'messages',
	        'My Calendar' => 'my-calendar',
	        'mydashboard'	=> 'menu-icon-mydashboard',
	    );

		$elements = array();
		$menuItem = CoreMenuItem::getMainItemByTitle('Menu');
		if($menuItem !== null)
			$elements = $menuItem->getChildMenuItemsFromDB();

		// check if plugins have custom user menu voices to add
		$custom_menu_items = PluginManager::getActiveCustomPlugins();
		$plugin_items = PluginManager::getActivePlugins(true);
		$plugins = array_merge($custom_menu_items, $plugin_items);
		if (!empty($plugins)) {
			foreach ($plugins as $plugin) {
				$module = Yii::app()->getModule($plugin->plugin_name);
				if (method_exists($module, 'userMenu')) {
					$voices = $module->userMenu();
					if (!empty($voices)) {
						foreach ($voices as $voice) {
							$elements[] = $voice;
						}
					}
				}
			}
		}
		$plugins = null;
		unset($plugins);

		return $elements;
	}


	/**
	 * Return the list of items that are part of the admin system
	 * @return array
	 */
	protected function getAdminItems() {

		$itemsElearning = array(
			array('label' => Yii::t('menu', '_DASHBOARD'), 'href' => Docebo::createAdminUrl('dashboard/index'), 'permission' => '/framework/admin/dashboard/view'),
			array('label' => Yii::t('standard', '_USERS'), 'href' => Docebo::createAdminUrl('userManagement/index'), 'permission' => '/framework/admin/usermanagement/view' ),
			array('label' => Yii::t('standard', '_GROUPS'), 'href' => Docebo::createAdminUrl('groupManagement/index'), 'permission' => '/framework/admin/groupmanagement/view'  ),
			array('label' => Yii::t('standard', '_COURSES'), 'href' => Docebo::createAdminUrl('courseManagement/index'), 'permission' => '/lms/admin/course/view' )
		);


		if(Settings::get('enable_legacy_menu', 'off') == 'off')
			$channelsItem = array('label' => Yii::t('app7020', 'Channels'), 'href' => Docebo::createApp7020Url('channelsManagement/index'), 'permission' => '/framework/admin/setting/view');
		else
			$channeslItem = array('label' => Yii::t('app7020', 'Channels').' <span class="mainMenuNewLabel">NEW</span>', 'href' => Docebo::createApp7020Url('channelsManagement/index'), 'permission' => '/framework/admin/setting/view');

		$itemsElearning[] = $channelsItem;


		$itemsElearning = array_merge($itemsElearning, array(
			array('label' => Yii::t('standard', 'Central Repository'), 'href' => Docebo::createLmsUrl('//centralrepo/centralRepo/index'), 'permission' => '/framework/admin/setting/view'),
        ));

		$itemsElearning = array_merge($itemsElearning, array(
			array('label' => Yii::t('standard', 'Content Marketplace'), 'href' => Docebo::createAdminUrl('marketplace/index'), 'newLabel' => '<span class="mainMenuNewLabel">NEW</span>'), //NOTE: by design only godadmins are allowed to morketplace
		));

		$itemsElearning = array_merge($itemsElearning, array(
		    array('label' => Yii::t('standard', '_REPORTS'), 'href' => Docebo::createAdminUrl('reportManagement/index'), 'permission' => '/lms/admin/report/view'),
			array('label' => Yii::t('standard', '_NEWSLETTER'), 'href' => Docebo::createAdminUrl('newsletter/index'), 'permission' => '/framework/admin/newsletter/view')
		));

		$elearningItems = array();
		foreach ($itemsElearning as $item) {
			if ($this->godadmin || (isset($item['permission']) && Yii::app()->user->checkAccess($item['permission']))) {
				$elearningItems[] = $item;
			}
		}

		$menu = array();

		if(!empty($elearningItems)) {
			$menu[] = array(
				'icon' 	=> 'elearning',
				'fa-icon' => 'fa-laptop',
				'label' => Yii::t('standard','_ELEARNING'),
				'items' => $elearningItems
			);
		}

		$itemsSettings = array();

        /**
         * Check if the Admin is Whitelable user
         */
		if(!PluginManager::isPluginActive('WhiteLabelApp')){
		    $itemsSettings[] = array('label' => Yii::t('menu', 'Manage Themes'), 'href' => Docebo::createAdminUrl('themes/index'), 'permission' => '/lms/admin/middlearea/view', 'newLabel' => '<span class="mainMenuNewLabel">NEW</span>');
        } else{
            if(BrandingWhiteLabelForm::isMenuVisible()){
                $itemsSettings[] = array('label' => Yii::t('menu', 'Manage Themes'), 'href' => Docebo::createAdminUrl('themes/index'), 'permission' => '/lms/admin/middlearea/view', 'newLabel' => '<span class="mainMenuNewLabel">NEW</span>');
            }
        }

		/**
		 * Admin > Settings
		 * Check if user meets permissions required
		 */
		$itemsSettings = array_merge($itemsSettings, array(
			array('label' => Yii::t('menu', 'Branding, Menu & Contents'), 'href' => Docebo::createAdminUrl('branding/index'), 'permission' => '/lms/admin/middlearea/view'),
			array('label' => Yii::t('menu', 'Dashboard layout'), 'href' => Docebo::createLmsUrl('//mydashboard/dash/admin'), 'permission' => '/framework/admin/setting/view'),
			array('label' => Yii::t('certificate', '_STRUCTURE_CERTIFICATE'), 'href' => Docebo::createAdminUrl('certificateManagement/index'), 'permission' => '/lms/admin/certificate/view'),
			array('label' => Yii::t('menu', '_LANG'), 'href' => Docebo::createAdminUrl('langManagement/index'), 'permission' => '/framework/admin/lang/view'),
			array('label' => Yii::t('test', '_TEST_MODALITY'), 'href' => Docebo::createAdminUrl('questionBank/index'), 'permission' => '/lms/admin/course/mod'),
			array('label' => Yii::t('menu', '_CONFIGURATION'), 'href' => Docebo::createAdminUrl('advancedSettings/index#register'), 'permission' => '/framework/admin/setting/view')
		));

		/**
		 * Check is MobileApp plugin activated and insert link in the Settings menu
		 */
		if ( PluginManager::isPluginActive('WebApp') ) {
			$itemsSettings[] = array(
				'href'          => Docebo::createAdminUrl('WebApp/MobileApp/index'),
				'label'         => Yii::t('mobile', 'Mobile App Settings'),
				'permission'    => '/framework/admin/setting/view');

		}

		$menuSettings = array(
			'icon' => 'settings',
			'fa-icon' => 'fa-gears',
			'label' => Yii::t('standard', 'Settings'),
			'items' => array(),
		);

		foreach ($itemsSettings as $item) {
			if ($this->godadmin || Yii::app()->user->checkAccess($item['permission'])) {
				$menuSettings['items'][] = $item;
			}
		}

		if($this->is7020AppActived() && Yii::app()->user->isGodAdmin) {
			$menu_7020 = array(
				'fa-icon' => array(
					array('icon' => 'fa-comment-o', 'stacking' => 'fa-stack-2x'),
					array('icon' => 'fa-share-alt', 'stacking' => 'fa-stack-1x')
				),
				'label' => PluginManager::isPluginActive('Share7020App') ? Yii::t('app7020', 'Coach & Share') : Yii::t('app7020', 'Coach'),
				'items' => array(
					//array('label' => Yii::t('app7020', 'Coach Settings'), 'href' => Docebo::createAdminUrl('Share7020App/Share7020App/coachSettings'), 'permission' => false),
					array('label' => Yii::t('app7020', 'Experts & Channels'), 'href' => Docebo::createApp7020Url('expertsChannels/index'), 'permission' => false),
				),
			);

			if(PluginManager::isPluginActive('Share7020App')) {
				$menu_7020['items'][] = array(
					'href' => Docebo::createAdminUrl('Share7020App/Share7020App/settings'),
					'label' => Yii::t('app7020', 'Coach & Share Settings'),
					'permission' 	=> false
				);
			}

			$menu[] = $menu_7020;

		}

		if (!empty($menuSettings['items'])) {
			$menu[] = $menuSettings;
		}



		return $menu;
	}

	/**
	 * Return the list of items that are part of the apps menu, this is formed into the MainMenu component and
	 * is populate by the apps themself on the initialization session
	 * @return array
	 */
	protected function getAppsItems() {
		$apps = Yii::app()->mainmenu->getAppsItems();
		return $apps;
	}


	/**
	 * Return the list of languages
	 *
	 * @param bool $allowedUrlParams
	 *
	 * @return array
	 */
	protected function getLanguageItems($allowedUrlParams = FALSE) {

		$elements = array();
		$models = CoreLangLanguage::model()->findAll('lang_active = :lang_active', array(
			':lang_active' => '1'
		));
		$domain = CoreMultidomain::resolveClient();
		if($domain) {
			// we do this, because to get the currently active languages, otherwise we count on session values
			// in this way, if meanwhile the admin made changes to active languages, we will get it here immediately
			$domain = CoreMultidomain::model()->findByPk($domain->id);
			// we are at multidomain level, so get the languages for this domain, if custom settings are enabled
			if ($domain->use_custom_settings == 1) {
				$disabledLanguages = explode(',', $domain->disabled_languages);
				if(!empty($disabledLanguages)) {
					$models = CoreLangLanguage::model()->findAllByAttributes(array(), array('condition' => 'lang_code NOT IN ("' . implode('","', $disabledLanguages) . '")'));
				}
			}
		}
		$urlParams = array();

		$paramsToPreserve = $allowedUrlParams ? $allowedUrlParams : $this->whitelistedParams;
		foreach($paramsToPreserve as $urlParamName){
			$value = Yii::app()->request->getParam($urlParamName, FALSE);
			if($value){
				$urlParams[$urlParamName] = $value;
			}
		}

		// Add course_id as URL parameter if we are in Player zone
		if ($this->is_in_player) {
			$urlParams["course_id"] = $this->course_id;
			$urlParams["id"] = $this->course_id;

			//manage classrooms case
			if (PluginManager::isPluginActive('ClassroomApp')) {
				$idSession = Yii::app()->request->getParam('id_session', FALSE);
				if (!empty($idSession)) { $urlParams["id_session"] = $idSession; }
			}
		}

		foreach($models as $m) {
			$urlParams["lang"] = $m->lang_browsercode;
			$elements[] = array(
				'current' 	=> ($m->lang_browsercode == Yii::app()->getLanguage()),
				'href' 		=> Yii::app()->controller->createUrl('', $urlParams),
				'label' 	=> $m->lang_description,
			);
		}
		return $elements;
	}


	/**
	 * Main plugin function, will be called when the plugin start
	 */
	public function run() {

	    // Register globalsearch CSS/JS (ElasticSearch module assets)
	    $esModuleAssetsUrl = Yii::app()->getModule('es')->getAssetsUrl();
	    Yii::app()->getClientScript()->registerScriptFile( $esModuleAssetsUrl . "/js/globalsearch.js", CClientScript::POS_END);
	    Yii::app()->getClientScript()->registerCssFile(    $esModuleAssetsUrl . "/css/globalsearch.css");

		// Prepare Global Search Tabs, according to the LMS settings !!!
		$globalSearchTabs = array(
			'all-global-search' => array(
				'header'    => strtoupper(Yii::t('standard', 'All')),
				'content'   => $this->spinnerLoading,
				'active'    => true
			),
			'courses-global-search' => array(
				'header'    => strtoupper(Yii::t('standard', '_COURSES')),
				'content'   => $this->spinnerLoading,
			),
		);

		if (PluginManager::isPluginActive('Share7020App')) {
			$globalSearchTabs['qa-global-search'] = array(
				'header'    => strtoupper(Yii::t('app7020', 'Q & A')),
				'content'   => $this->spinnerLoading,
			);
			$globalSearchTabs['ka-global-search'] = array(
				'header'    => strtoupper(Yii::t('app7020', 'Knowledge assets')),
				'content'   => $this->spinnerLoading,
			);
		}

		switch (count($globalSearchTabs)) {
			case 2:
				// Add Learning Plans to the tabs
				if (PluginManager::isPluginActive('CurriculaApp')) {
					$globalSearchTabs['coursepath-global-search'] = array(
						'header'    => strtoupper(Yii::t('standard', '_COURSEPATH')),
						'content'   => $this->spinnerLoading,
					);
				}

				// Add Learning Objects to the tabs
				$globalSearchTabs['courselo-global-search'] = array(
					'header'    => strtoupper(Yii::t('standard', '_LEARNING_OBJECTS')),
					'content'   => $this->spinnerLoading,
				);

				// we don't need a ellipsis menu, so we don't add nothing
				break;

			case 3:
				// Add Learning Plans to the tabs
				if (PluginManager::isPluginActive('CurriculaApp')) {
					$globalSearchTabs['coursepath-global-search'] = array(
						'header'    => strtoupper(Yii::t('standard', '_COURSEPATH')),
						'content'   => $this->spinnerLoading,
					);
				}

				// Add Ellipsis to the tabs !!!
				$globalSearchTabs['ellipsis-global-search'] = array(
					'header'    => '<i class="fa fa-ellipsis-h fa-lg"></i>',
					'content'   => '',
					'ellipsisContent' => '<a href="#tabLearningObject">'.strtoupper(Yii::t('standard', '_LEARNING_OBJECTS')).'</a>'
				);
				break;

			case 4:
				// Add only Ellipsis to the tabs !!!
				if (PluginManager::isPluginActive('CurriculaApp')) {
					$globalSearchTabs['ellipsis-global-search'] = array(
						'header'    => '<i class="fa fa-ellipsis-h fa-lg"></i>',
						'content'   => '',
						'ellipsisContent' => '<a href="#tabLearningPlan">' . strtoupper(Yii::t('standard', '_COURSEPATH'))
							. '</a><a href="#tabLearningObject">'
							. strtoupper(Yii::t('standard', '_LEARNING_OBJECTS')) . '</a>'
					);
				}
				else {
					$globalSearchTabs['ellipsis-global-search'] = array(
							'header'    => '<i class="fa fa-ellipsis-h fa-lg"></i>',
							'content'   => '',
							'ellipsisContent' => "" 
							. '</a><a href="#tabLearningObject">'
							. strtoupper(Yii::t('standard', '_LEARNING_OBJECTS')) . '</a>'
					);
				}
				break;
		}

		$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
		if ($enable_legacy_menu == 'on') {
			$template = 'mainMenu/index';
		} else {
			$template = 'mainMenu/index_new';
		}

		$isRTL = Lang::isRTL(Yii::app()->getLanguage());

		$this->render($template, array(
			'animate' 			=> isset($_GET['login']),
			'menu_home' 		=> $this->getHomeItems(),
			'menu_admin'		=> $this->getAdminItems(),
			'menu_apps'			=> $this->getAppsItems(),
			'menu_languages' 	=> $this->getLanguageItems(),
			'coachPluginActive'	=> $this->is7020AppActived(),
			'askExpertActive'	=> $this->isAskAxpertActivated(),
//			'menu_seventy_twenty' 	=> $this->getApp7020Items(),
			'globalSearchTabs' 	=> $globalSearchTabs,
			'isRTL'             => $isRTL,
		));
	}

	private function is7020AppActived(){
		return PluginManager::isPluginActive('Share7020App');
	}


	private function isAskAxpertActivated(){

		return $askExpert = true;
	}



	/**
	 * Check menu permissions for current user
	 * @param mixed $permissions can be a single permission (string) or a list of permissions (array of strings)
	 * @return boolean
	 */
	public function checkMenuPermissions($permissions) {
		//is there any permission to check ?
		if (empty($permissions)) { return true; }
		//single permission specification
		if (is_string($permissions)) {
			$permissions = array($permissions); //do the check in the next block
		}
		//multiple permission specification (all permissions must be satisfied, AND condition)
		if (is_array($permissions)) {
			$output = true;
			foreach ($permissions as $permission) {
				$subOutput = false;
				$subPermissions = explode('|', $permission); //a permission string can contain multiple sub-permissions, at least one must be satisfied (OR condition)
				foreach ($subPermissions as $subPermission) {
					$subOutput = ($subOutput || Yii::app()->user->checkAccess(trim($subPermission)));
					if ($subOutput) break; //no more checks required in this loop
				}
				$output = ($output && $subOutput);
				if (!$output) break; //no more checks required in this loop
			}
			return $output;
		}
		//last choice: invalid specified permission
		return false;
	}

}