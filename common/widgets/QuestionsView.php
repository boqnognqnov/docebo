<?php

class QuestionsView extends CWidget
{

    public $widgetOptions;

    public function init()
    {
        $cs = Yii::app()->getClientScript();
        $am = Yii::app()->assetManager;
        $ft = new FancyTree();
        Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_END);
        Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_END);
        Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
        App7020Helpers::registerApp7020Options();
        $cs->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
        
        $cs->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
    }

    public function run()
    {
        Yii::import('app7020.protected.components.*');
        Yii::import('app7020.protected.controllers.*');
        
        $listParamsQuestions = array(
            
            // 'autocompleteRoute' => 'searchAutocomplete',
            'listId' => "questionsComboListView",
            'dataProvider' => App7020Question::sqlDataProvider(false, array(
                'pageSize' => $this->widgetOptions["number_items"],
                'pagination' => $this->widgetOptions["paginate_widget"]
            )),
            'listItemView' => 'app7020.protected.views.askGuru._comboListViewQuestionItem',
            'columns' => 1,
            'disableMassSelection' => true,
            'disableMassActions' => true,
            'disableCheckBox' => true,
			'afterAjaxUpdate' => "app7020.knowledgeLibrary.listViewAfterUpdate();",
			'beforeAjaxUpdate' => "
                // Set this GET parameter to ask the dashlet to return HTML CONTENT only during ajax update
                options.data = options.data + \"&dashletHtmlOnly=1\";
            "
        );
        
        // ****** FILTER PARAMS ****** //
        $filtersArray = array(
            array(
                'title' => Yii::t('app7020', 'Sort by'),
                'name' => 'sort',
                'data' => array(
                    'Date',
                    'Answers number',
                    'Views'
                )
            ),
            array(
                'title' => Yii::t('app7020', 'Show'),
                'name' => 'show',
                'data' => array(
                    'All questions',
                    'Open questions',
                    'Closed questions',
                    'New questions'
                )
            ),
            array(
                'title' => Yii::t('app7020', 'Question type'),
                'name' => 'type',
                'data' => array(
                    'All types'
                )
            )
        )
        ;
        // ****** ------------- ****** //
        
        $this->render('questions_view', array(
            'listViewParams' => $listParamsQuestions,
            'viewAskGuruButton' => $this->widgetOptions["show_ask_guru_button"],
            'viewSearchButton' => $this->widgetOptions["show_search_bar"],
            'viewShowAllLink' => $this->widgetOptions["show_quetions_and_answers_link"],
            'filtersArray' => $filtersArray
        ));
    }
}