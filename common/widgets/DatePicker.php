<?php
/**
 * 
 * Bootstrap Date picker widget (bdatepicker)
 * 
 * An attempt to make this more re-usable, including date format and language (using date picker helper)
 *
 */
class DatePicker extends CWidget {

	/**
	 * Widget parameters
	 * @var mixed
	 */
	public $dateFormat;
	public $class = '';
	public $fieldName;
	public $value;
	public $htmlOptions = array();
	public $datePickerOptions = array();
	public $labelOnRight;
	public $label;
	public $stackLabel;
	public $pickerStartDate; 
	public $pickerEndDate;
    public $iconClass;
	protected $defaultDatePickerOptions = array('autoclose'	=> true);
	protected $defaultHtmlOptions = array();
	public $scriptPosition = false;

	/**
	 * @see CWidget::init()
	 */
	public function init() {
		
		parent::init();
		
		DatePickerHelper::registerAssets(false, $this->scriptPosition);
		
		$this->id = 'date-picker-widget-'  . $this->id;
		
		if (!$this->dateFormat) {
			$this->dateFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
		}
		
		if (!$this->fieldName) {
			$this->fieldName = $this->id . '-input';
		}

		// Default field ID
		$this->defaultHtmlOptions['id'] = $this->id . '-input';
		
		// Merge html options
		$this->htmlOptions = CMap::mergeArray($this->defaultHtmlOptions, $this->htmlOptions);

		if ($this->pickerStartDate) {
			$this->defaultDatePickerOptions['startDate'] = Yii::app()->localtime->dateStringToJQueryDatepickerFormat($this->pickerStartDate);
		}
		
		if ($this->pickerEndDate) {
			$this->defaultDatePickerOptions['endDate'] = Yii::app()->localtime->dateStringToJQueryDatepickerFormat($this->pickerEndDate);
		}
        if(!$this->iconClass){
            $this->iconClass = 'fa fa-calendar';
        }
		
		
		// Merger default datepicker options and incoming ones
		$this->datePickerOptions = CMap::mergeArray($this->defaultDatePickerOptions, $this->datePickerOptions);
		
		
	}

	/**
	 * @see CWidget::run()
	 */
	public function run() {
		$this->render('datepicker/index');
	}
	
}