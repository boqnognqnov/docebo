<?php
/**
 * This widget will output <script></script> tag, connecting to our Cloudfront cookie setter (baker).
 * It will set required cookies in the client's browser to allow signed cookies access to Cloudfront served resources.
 * Required cookies are prepared by the LMS and sent as URL parameters. 
 * 
 * The accessed script is located under Cloudfront Domain, like (configurable):
 *  
 * d2dfsdfykshjdf.cloudfront.net/cookiebaker
 *  
 * and must do one simple thing: Set-Cookie, according to Amazon Signed Cookies documentation
 *  
 * http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-cookies.html
 *
 */
class CloudfrontCookieBaker extends CWidget {
    public $onlyUrl = false;
    public function run() {
        if (isset(Yii::app()->params["cdn"]["distributions"]) && is_array(Yii::app()->params["cdn"]["distributions"])) {
            $distros = Yii::app()->params["cdn"]["distributions"];
            // We output MANY <script> tags, for all distributions set to "cloudfront_signed_cookies"=true
            foreach ($distros as $key => $distro) {
                if (isset($distro["params"]["cloudfront_signed_cookies"]) && $distro["params"]["cloudfront_signed_cookies"]) {
                    $cfObject = Cloudfront::getInstance($key);
                    if ($cfObject && ($url = $cfObject->getCfCookieBakerUrl())) {
                        echo (!$this->onlyUrl ? CHtml::scriptFile($url) : $url);
                    }
                }
            }
        }
    }
    
    
}