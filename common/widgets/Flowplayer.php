<?php
/**
 * 
 *
 */
class Flowplayer extends CWidget{

    const SKIN_FUNCTIONAL = 'functional';
    const SKIN_MINIMALIST = 'minimalist';
    const SKIN_PLAYFUL = 'playful';

    public $skin = self::SKIN_FUNCTIONAL;

    public $videoId;
    public $title;
    public $mediaUrlMp4 = '';// URL of the mp4 media to play
    public $mediaUrlHlsPlaylist = '';// URL of the HLS media to play
    public $autoplay = false;
    public $bookmark = false;
    public $seekable = true;
    public $subtitlesUrls = false; //Example: array('english'=>array('url'=>CoreAsset::url(320), 'fallback'=>true),'bulgarian'=>array('url'=>CoreAsset::url(321),'fallback'=>false))

    public $playerOptions;
    public $clipOptions;
    public $containerId;
    
    // Do NOT play in HLS mode?? 
    public $disableHls = false;
    
    // Keeps published assets URL
    protected $_assetsUrl = null;

    public  $_isRenderable =  false;

    /**
     * @see CWidget::init()
     */
    public function init() {
        self::registerFlowplayerScriptsAndCss($this->skin);
        parent::init();
        $this->containerId = 'player-'.$this->id;
    }

    /**
     * @see CWidget::run()
     */
    public function run() {
        $defaultPlayerOptions = array(
            'keyboard'  => false,
            'tooltip'   => false,
            'fullscreen'=> true,
            'embed'     => false,
            'brand'     => false,
            'wmode'     => "opaque",     //for the controls to not show outside the container. This allows the HTML to hide the flash content
            'autoplay'  => $this->autoplay,
            'title'     => $this->title,
        );

		$sources = array();

		// If HLS is not disabled, and HLS source is specified....
		if ($this->mediaUrlHlsPlaylist && ($this->mediaUrlHlsPlaylist != '') && !$this->disableHls)
			$sources[] = array('type' => "application/x-mpegurl", 'src' => $this->mediaUrlHlsPlaylist);
		$sources[] = array('type' => "video/mp4", 'src' => $this->mediaUrlMp4);
		$clipConf = array(
			'flashls' => array(
				'seekmode' => 'ACCURATE'
			),
			'sources' => $sources,

		);
		$currentLangLong = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$hasCurrentLang = ($this->subtitlesUrls[$currentLangLong]) ? true : false;
		if ($this->subtitlesUrls && is_array($this->subtitlesUrls) && count($this->subtitlesUrls)) {
			$clipConf['subtitles'] = array();
			foreach ($this->subtitlesUrls as $subLang => $sub) {
				$subsOptions = array(
					'kind' => 'subtitles',
					'srclng' => Lang::getBrowserCodeByCode($subLang),
					'label' => ucfirst($subLang),
					'src' => $sub['url']
				);
				if (($hasCurrentLang && $subLang == $currentLangLong) || (!$hasCurrentLang && $sub['fallback'] == true))
					$subsOptions['default'] = true;

				$clipConf['subtitles'][] = $subsOptions;
			}
		}

		if ($this->clipOptions && is_array($this->clipOptions))
			$clipConf = CMap::mergeArray($clipConf, $this->clipOptions);


		$clipOption = array(
			'clip' => $clipConf,

		);

		$json = CMap::mergeArray($defaultPlayerOptions, $clipOption);
		if ($this->playerOptions && is_array($this->playerOptions))
			$json = CMap::mergeArray($json, $this->playerOptions);

		// Merge the domain key (for unlimited domains license)
		$json['key'] = $this->getLicenceKeyForDomain();
    
 		if(!$this->_isRenderable) {
		    $this->render('flowplayer/index', array(
		        'bookmark' => $this->bookmark,
		        'seekable' => $this->seekable,
		        'player_Json' => json_encode($json, JSON_PRETTY_PRINT),
		    ));
		}
		else {
		    var_dump($json)    ;
		    return $json;
		}
	  
	}

	/**
	 * Generates the licence key for the current domain
	 *
	 * @return string
	 */
	private function getLicenceKeyForDomain() {
		$domain = $this->stripSubdomain();
		$domain = array_reverse(str_split($domain));
		$sum1 = 0;
		$sum2 = 0;

		foreach ($domain as $i) {
			$sum1 += ord($i) * 37313435421;
			$sum2 += ord($i) * 546760049;
		}

		// 32bit systems cannot handle large int
		// filter float in IEEE double precision
		$sum1 = str_replace('.', '', $sum1);
		$sum2 = str_replace('.', '', $sum2);

		return substr('$' . $sum1, 0, 8) . substr($sum2, 0, 8);
	}

	function stripSubdomain() {
		$tld = explode(',', 'ab.ca,ac.ac,ac.at,ac.be,ac.cn,ac.il,ac.in,ac.jp,ac.kr,ac.sg,ac.th,ac.uk,ad.jp,adm.br,adv.br,ah.cn,am.br,arq.br,art.br,arts.ro,asn.au,asso.fr,asso.mc,bc.ca,bio.br,biz.pl,biz.tr,bj.cn,br.com,cn.com,cng.br,cnt.br,co.ac,co.at,co.de,co.gl,co.hk,co.id,co.il,co.in,co.jp,co.kr,co.mg,co.ms,co.nz,co.th,co.uk,co.ve,co.vi,co.za,com.ag,com.ai,com.ar,com.au,com.br,com.cn,com.co,com.cy,com.de,com.do,com.ec,com.es,com.fj,com.fr,com.gl,com.gt,com.hk,com.hr,com.hu,com.kg,com.ki,com.lc,com.mg,com.mm,com.ms,com.mt,com.mu,com.mx,com.my,com.na,com.nf,com.ng,com.ni,com.pa,com.ph,com.pl,com.pt,com.qa,com.ro,com.ru,com.sb,com.sc,com.sg,com.sv,com.tr,com.tw,com.ua,com.uy,com.ve,com.vn,cp.tz,cq.cn,de.com,de.org,ecn.br,ed.jp,edu.au,edu.cn,edu.hk,edu.mm,edu.my,edu.pl,edu.pt,edu.qa,edu.sg,edu.tr,edu.tw,eng.br,ernet.in,esp.br,etc.br,eti.br,eu.com,eu.int,eu.lv,firm.in,firm.ro,fm.br,fot.br,fst.br,g12.br,gb.com,gb.net,gd.cn,gen.in,go.jp,go.kr,go.th,gov.au,gov.az,gov.br,gov.cn,gov.il,gov.in,gov.mm,gov.my,gov.qa,gov.sg,gov.tr,gov.tw,gov.uk,gr.jp,gs.cn,gv.ac,gv.at,gx.cn,gz.cn,he.cn,hi.cn,hk.cn,hl.cn,hu.com,id.au,idv.tw,in.ua,ind.br,ind.in,inf.br,info.pl,info.ro,info.tr,info.ve,iwi.nz,jl.cn,jor.br,js.cn,jus.br,k12.il,k12.tr,kr.com,lel.br,lg.jp,ln.cn,ltd.uk,maori.nz,mb.ca,me.uk,med.br,mi.th,mil.br,mil.uk,mo.cn,mod.uk,muni.il,nb.ca,ne.jp,ne.kr,net.ag,net.ai,net.au,net.br,net.cn,net.do,net.gl,net.hk,net.il,net.in,net.kg,net.ki,net.lc,net.mg,net.mm,net.mu,net.ni,net.nz,net.pl,net.ru,net.sb,net.sc,net.sg,net.th,net.tr,net.tw,net.uk,net.ve,nf.ca,nhs.uk,nm.cn,nm.kr,no.com,nom.br,nom.ni,nom.ro,ns.ca,nt.ca,nt.ro,ntr.br,nx.cn,odo.br,off.ai,on.ca,or.ac,or.at,or.jp,or.kr,or.th,org.ag,org.ai,org.au,org.br,org.cn,org.do,org.es,org.gl,org.hk,org.in,org.kg,org.ki,org.lc,org.mg,org.mm,org.ms,org.nf,org.ni,org.nz,org.pl,org.ro,org.ru,org.sb,org.sc,org.sg,org.tr,org.tw,org.uk,org.ve,pe.ca,plc.uk,police.uk,ppg.br,presse.fr,pro.br,psc.br,psi.br,qc.ca,qc.com,qh.cn,rec.br,rec.ro,res.in,sa.com,sc.cn,sch.uk,se.com,se.net,sh.cn,sk.ca,slg.br,sn.cn,store.ro,tj.cn,tm.fr,tm.mc,tm.ro,tmp.br,tur.br,tv.br,tv.tr,tw.cn,uk.com,uk.net,us.com,uy.com,vet.br,waw.pl,web.ve,www.ro,xj.cn,xz.cn,yk.ca,yn.cn,zj.cn,zlg.br');
		$host = strtolower($_SERVER['SERVER_NAME']);
		$bits = explode('.', $host);
		$len = count($bits);

		// 'localhost', 'myintranet' ... or ip number
		if ($len < 2 || preg_match('/^\d+$/', $bits[$len - 1]))
			return $host;

		$secondary = join('.', array_slice($bits, -2));
		if ($len >= 3 && in_array($secondary, $tld))
			return join('.', array_slice($bits, -3));

		return $secondary;
	}

	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl() {
		if (isset($this->_assetsUrl)) {
			return $this->_assetsUrl;
		} else {
			$assetsPath = Yii::getPathOfAlias('common.widgets.views.flowplayer.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			return $this->_assetsUrl = $assetsUrl;
		}
	}

	/**
	 * Registers the Flowplayer's styles and JavaScript
	 *
	 * @param $skin - Flowplayer::SKIN_FUNCTIONAL(1)(default),Flowplayer::SKIN_MINIMALIST(2),Flowplayer::SKIN_PLAYFUL(3)
	 *
	 * @return void
	 */
	public static function registerFlowplayerScriptsAndCss($skinForPlayer = self::SKIN_FUNCTIONAL) {

		// !!! We request this CSS to be rendered here, but please see the note in common/components/Controller.php (search for "Flowplayer)
		// Basically for IE this (here) is not working)
		self::registerCssFile($skinForPlayer);

		Yii::app()->getClientScript()->registerScriptFile(self::getFlowplayerJSUrl() . (YII_DEBUG ? 'flowplayer.js' : 'flowplayer.min.js'), CClientScript::POS_HEAD);
	}

	/**
	 * Register CSS file. We need this separate static method so we can register CSS .. separately
	 *
	 * @param string $skinForPlayer
	 */
	public static function registerCssFile($skinForPlayer = self::SKIN_FUNCTIONAL) {
		$skinName = 'functional';
		switch ($skinForPlayer) {
			case self::SKIN_MINIMALIST:
				$skinName = 'minimalist';
				break;
			case self::SKIN_PLAYFUL:
				$skinName = 'playful';
				break;
		}
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/flowplayer_skins/' . $skinName . '.css');
	}

	/**
	 *
	 */
	public static function getFlowplayerJSUrl() {
		return Yii::app()->theme->baseUrl . '/js/flowplayer/';
	}

}

if (!function_exists('str_split')) {

	// php < 5
	function str_split($text, $split = 1) {
		$array = array();
		for ($i = 0; $i < strlen($text); $i += $split) {
			$array[] = substr($text, $i, $split);
		}
		return $array;
	}

}