<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */
class BugReportButton extends CWidget {

    protected $isGodadmin;

    /**
     * INIT
     * @see CWidget::init()
     */
    public function init() {

        parent::init();

        $this->isGodadmin = Yii::app()->user->isGodadmin;
    }

    /**
     * RUN
     * @see CWidget::run()
     */
    public function run() {
        if (!$this->isGodadmin)
            return;

        $this->render('common.widgets.views.bugReport.button', array(
        ));
    }

}