<?php

/**
 * Class SessionGrid
 *
 * Renders a grid of sessions for a specified course
 * It allows filtering by month/year and also searching
 */
class SessionGrid extends CWidget
{

	/**
	 * @var LearningCourse
	 */
	public $course;
	/**
	 * @var int
	 */
	public $courseId;
	/**
	 * @var string
	 */
	public $title;
	/**
	 * @var string
	 */
	public $autocompleteUrl;

	/**
	 *
	 * @var boolean
	 */
	public $enablePagination = false;

	/**
	 * @var bool
	 */
	public $showExpiredSessions = true;

	/**
	 * @var bool
	 */
	public $puFilter = false;
	/**
	 * CGridView ajaxUrl property, where the AJAX requests should be sent to
	 * @var string
	 */
	public $ajaxUrl = false;

	public function init()
	{
		if (!$this->course) {
			$course = LearningCourse::model()->findByPk($this->courseId);

			if ($course) {
				$this->course = $course;
				$this->courseId = $course->getPrimaryKey();
			}
		}
	}

	public function run()
	{
		if (!$this->course)
			echo CHtml::tag('h4', array('style' => 'color:red;'), 'Please specify a course.');
		else
			$this->render('sessionGrid', array(
				//'id' => $this->course->idCourse . time()
				'id' => $this->course->idCourse,
				'showExpiredSessions' => $this->showExpiredSessions,
				'puFilter' => $this->puFilter
			));
	}

} 