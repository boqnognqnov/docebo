<?php

/*
 * This widget is used to render callout messages
 *
 * @author Andrei Amariutei
 */
class CalloutsWidget extends CWidget {

    public function init() {
        parent::init();
    }

    public function run() {
		$queue = CalloutMessages::getQueue();
        /*if (empty(Yii::app()->session['callout_messages'])) {
            $queue = array(
                1 => array(
                    'button_assigned' => 'generic',
                    'message' => '<h2>Big news!</h2>A still more glorious dawn awaits totam rem aperiam descended from astronomers dream of the minds eye. Across the centuries! Astonishment sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.'
                ),
                2 => array(
                    'button_assigned' => 'buy_now',
                    'message' => '<h2>Get more users here!</h2>Speaking of settling, hows Ann? Buster, what are you doing with mothers rape-horn? ♪♪ It aint easy being white. It aint easy being brown. ♪♪'
                ),
                3 => array(
                    'button_assigned' => 'marketplace',
                    'message' => '<h2>Awesome Courses Marketplace</h2>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.'
                ),
                4 => array(
                    'button_assigned' => 'apps',
                    'message' => '<h2>Extend your Docebo with APPS!</h2>Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.'
                ),
                5 => array(
                    'button_assigned' => 'helpdesk',
                    'message' => '<h2>We\'re here to help you 24/7!</h2>Completely synergize resource sucking relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art customer service.'
                )
            );
            Yii::app()->session['callout_messages'] = $queue;
        }*/

        $isCustomizationProcess = (Yii::app()->controller->id == 'setup');
        $haveCourses = ( 0<LearningCourse::model()->count() );

		if ($haveCourses && !empty($queue) && BrandingWhiteLabelForm::isMenuVisible() && !$isCustomizationProcess) {
        	$this->render('callouts', array(
				'messages' => $queue,
				'axReadUrl' => Docebo::createLmsUrl('callouts/axRead')
			));
		}
    }
}