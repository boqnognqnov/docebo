<?php

class KnowledgeGurusKPIs extends CWidget
{

    public $widgetOptions;

    public function init()
    {
        $clientScript = Yii::app()->getClientScript();
        $clientScript->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
    }

    public function run()
    {
        $dateObject = new DateTime();
        $dateToCurrent = $dateObject->format("Y-m-d"); // todays date
        
        $dashletArray = array();
        
        switch ($this->widgetOptions["timeframe"]) {
            case "weekly":
                $dateFromCurrent = $dateObject->modify("-1 week +1 day")->format("Y-m-d"); // a week before
                $dateToPrevious = $dateObject->modify("-1 day")->format("Y-m-d"); // the end of the previous week
                $dateFromPrevious = $dateObject->modify("-1 week +1 day")->format("Y-m-d"); // start of the previous week
                $dashletArray['title'] = Yii::t('app7020', "my weekly activity");
                $dashletArray['subText1'] = Yii::t('app7020', "this week's");
                $dashletArray['subText2'] = Yii::t('app7020', "this week");
                $dashletArray['formatDate'] = "%a"; // short abbreveature the the day of the week
                break;
            
            case "monthly":
                $dateFromCurrent = $dateObject->modify("-1 month +1 day")->format("Y-m-d"); // a month before
                $dateToPrevious = $dateObject->modify("-1 day")->format("Y-m-d"); // the end of the previous month
                $dateFromPrevious = $dateObject->modify("-1 month +1 day")->format("Y-m-d"); // start of the previous month
                $dashletArray['title'] = Yii::t('app7020', "my monthly activity");
                $dashletArray['subText1'] = Yii::t('app7020', "this month's");
                $dashletArray['subText2'] = Yii::t('app7020', "this month");
                $dashletArray['formatDate'] = "%d";
                break;
            
            default:
                $dateToCurrent = null;
                $dateFromCurrent = null;
                $dateFromPrevious = null;
                $dateToPrevious = null;
                $dashletArray['title'] = Yii::t('app7020', "my activity");
                $dashletArray['subText1'] = Yii::t('app7020', "");
                $dashletArray['subText2'] = Yii::t('app7020', "");
                break;
        }
        
        if (! empty($this->widgetOptions["answered_questions"])) {
            // get a daily counts of the curent month
            $currentAnsweredQuestionsByDays = App7020Question::getAnsweredQuestions(1, $dateFromCurrent, $dateToCurrent, Yii::app()->user->idst);
            // get the count of the current month
            $currentAnsweredQuestionsCount = App7020Question::getAnsweredQuestions(2, $dateFromCurrent, $dateToCurrent, Yii::app()->user->idst);
            // get the count of the previous month
            $previousAnsweredQuestionsCount = App7020Question::getAnsweredQuestions(2, $dateFromPrevious, $dateToPrevious, Yii::app()->user->idst);
            $dateObject = new DateTime();
            $tmpDate = $dateToCurrent;
            
            // if some days are missing from the daily counts array - fill the date with a 0 count
            while ($tmpDate != $dateFromCurrent) {
                
                if (! array_key_exists($tmpDate, $currentAnsweredQuestionsByDays)) {
                    $currentAnsweredQuestionsByDays[$tmpDate] = 0;
                }
                $tmpDate = $dateObject->modify("-1 day")->format("Y-m-d");
            }
            if (! array_key_exists($dateFromCurrent, $currentAnsweredQuestionsByDays)) {
                $currentAnsweredQuestionsByDays[$dateFromCurrent] = 0;
            }
            
            ksort($currentAnsweredQuestionsByDays);
            
            // prepare the right js data array
            $jsChartArray = "[";
            foreach ($currentAnsweredQuestionsByDays as $key => $value) {
                $jsChartArray .= "['" . date("d-M-Y", strtotime($key)) . "'," . $value . "],";
            }
            $jsChartArray = rtrim($jsChartArray, ",");
            $jsChartArray .= "]";
            
            if ($this->widgetOptions["timeframe"] != 'from_beginning') {
                $dashletArray['answeredQuestions']['chartArray'] = $jsChartArray;
                // claculate the precentages
                if ($currentAnsweredQuestionsCount[0]['questionsCount'] == 0 && $previousAnsweredQuestionsCount[0]['questionsCount'] == 0) {
                    $dashletArray['answeredQuestions']['percentage'] = 0;
                } elseif ($currentAnsweredQuestionsCount[0]['questionsCount'] == 0) {
                    $dashletArray['answeredQuestions']['percentage'] = - 100;
                } elseif ($previousAnsweredQuestionsCount[0]['questionsCount'] == 0) {
                    $dashletArray['answeredQuestions']['percentage'] = 100;
                } else {
                    $dashletArray['answeredQuestions']['percentage'] = intval(($currentAnsweredQuestionsCount[0]['questionsCount'] - $previousAnsweredQuestionsCount[0]['questionsCount']) * 100 / $previousAnsweredQuestionsCount[0]['questionsCount']);
                }
            }
            
            $dashletArray['answeredQuestions']['questionsCount'] = $currentAnsweredQuestionsCount[0]['questionsCount'];
        }
        
        
        if (! empty($this->widgetOptions["reviewed_assets"])) {
            
            $currentReviewedAssets = App7020Assets::getReviewedAssetsCountByDays($dateFromCurrent, $dateToCurrent, Yii::app()->user->idst);
            $currentReviewedAssetsCount = array_sum($currentReviewedAssets);
            $previousReviewedAssetsCount = array_sum(App7020Assets::getReviewedAssetsCountByDays($dateFromPrevious, $dateToPrevious, Yii::app()->user->idst));
            
            $dateObject = new DateTime();
            $tmpDate = $dateToCurrent;
            
            // if some days are missing from the daily counts array - fill the date with a 0 count
            while ($tmpDate != $dateFromCurrent) {
            
                if (! array_key_exists($tmpDate, $currentReviewedAssets)) {
                    $currentReviewedAssets[$tmpDate] = 0;
                }
                $tmpDate = $dateObject->modify("-1 day")->format("Y-m-d");
            }
            if (! array_key_exists($dateFromCurrent, $currentReviewedAssets)) {
                $currentReviewedAssets[$dateFromCurrent] = 0;
            }
            
            ksort($currentReviewedAssets);

            // prepare the right js data array
            $jsChartArray = "[";
            foreach ($currentReviewedAssets as $key => $value) {
                $jsChartArray .= "['" . date("d-M-Y", strtotime($key)) . "'," . $value . "],";
            }
            $jsChartArray = rtrim($jsChartArray, ",");
            $jsChartArray .= "]";
            
            if ($this->widgetOptions["timeframe"] != 'from_beginning') {
                $dashletArray['reviewedAssets']['chartArray'] = $jsChartArray;
                // claculate the precentages
                if ($currentReviewedAssetsCount == 0 && $previousReviewedAssetsCount == 0) {
                    $dashletArray['reviewedAssets']['percentage'] = 0;
                } elseif ($currentReviewedAssetsCount == 0) {
                    $dashletArray['reviewedAssets']['percentage'] = - 100;
                } elseif ($previousReviewedAssetsCount == 0) {
                    $dashletArray['reviewedAssets']['percentage'] = 100;
                } else {
                    $dashletArray['reviewedAssets']['percentage'] = intval(($currentReviewedAssetsCount - $previousReviewedAssetsCount) * 100 / $previousReviewedAssetsCount);
                }
            }
            
            $dashletArray['reviewedAssets']['assetsCount'] = $currentReviewedAssetsCount;
        }
        
        
        if ($this->widgetOptions['what_learners_think']) {
            $dashletArray['likes'] = App7020AnswerLike::getUsersLikesUnlikesByPeriod($dateFromCurrent, $dateToCurrent, Yii::app()->user->idst);
        }
        
        if ($this->widgetOptions['answers_marked_as_best']) {
            $currentBestAnswers = App7020Answer::getUsersBestAnswersByPeriod($dateFromCurrent, $dateToCurrent, Yii::app()->user->idst);
            
            $previousBestAnswers = App7020Answer::getUsersBestAnswersByPeriod($dateFromPrevious, $dateToPrevious, Yii::app()->user->idst);
            
            if ($currentBestAnswers[0]['aCount'] == 0 && $previousBestAnswers[0]['aCount'] == 0) {
                $dashletArray['bestAnswers']['percentage'] = 0;
            } elseif ($currentBestAnswers[0]['aCount'] == 0) {
                $dashletArray['bestAnswers']['percentage'] = - 100;
            } elseif ($previousBestAnswers[0]['aCount'] == 0) {
                $dashletArray['bestAnswers']['percentage'] = 100;
            } else {
                $dashletArray['bestAnswers']['percentage'] = intval(($currentBestAnswers[0]['aCount'] - $previousBestAnswers[0]['aCount']) * 100 / $previousBestAnswers[0]['aCount']);
            }
            $dashletArray['bestAnswers']['count'] = $currentBestAnswers[0]['aCount'];
        }
        if (! empty($this->widgetOptions["satisfied_knowledge_requests"])) {
			
			$Criteria = new CDbCriteria();
			$results = Yii::app()->db->createCommand()
				->select('u.id, p.idQuestion, p.idContent , u.created')
				->from('app7020_question u')
				->rightJoin('app7020_questions_requests p', 'u.id=p.idQuestion')
				->leftJoin('app7020_content c', 'c.id=p.idContent')
				->where('u.created>=:created', array(':created'=>date("Y-m-d", strtotime($dateFromCurrent))))
				->where('u.created<=:created', array(':created'=>date("Y-m-d", strtotime($dateToCurrent))))
				->queryAll();
			foreach($results as $key=>$result) {
				if($result["idContent"]!="") {
					$satisfied[] = $result;
				} else {
					$unsatisfied[] = $result;
				}
			}
			foreach($results as $res) {
				if($res["idContent"]!="") {
					$date = date('Y-m-d',strtotime($res["created"]));
					$dates[] = $date; 
				}
			}
			if($dates) {
				$dates = array_count_values($dates);
				$jsChartArray = "[";
				foreach($dates as $key=>$value) {
						$jsChartArray .= "['" . $key . "',".$value."],";
				}

				$jsChartArray = rtrim($jsChartArray, ",");
				$jsChartArray .= "]";
				$dashletArray['satisfied_knowledge_requests']['chartArray'] = $jsChartArray;
			}
			$count_results = count($results);
			$count_satisfied = count($satisfied);
			$percentage_satisfied = ($count_satisfied/$count_results)*100;
			$percentage_satisfied = substr($percentage_satisfied,0,3)."%";
			$count_unsatisfied = count($unsatisfied);
			$dashletArray['satisfied_knowledge_requests']["satisfied"] = $count_satisfied;
			$dashletArray['satisfied_knowledge_requests']["percentage"] = $percentage_satisfied;
			$dashletArray['satisfied_knowledge_requests']["all"] = $count_results;
		}
        $this->render('knowledge_gurus_kpis', array(
            'widgetOptions' => $this->widgetOptions,
            'dashletArray' => $dashletArray
        ));
    }
}