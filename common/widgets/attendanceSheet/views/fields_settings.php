<style>
	#fields_settings {
		padding: 20px;
	}

	#fields_settings .span4 > h4 {
		text-transform: capitalize;
		margin-bottom: 3%;
	}

	#fields_settings .span4 > span {
		color: grey;
	}

	.modal-header > h3 {
		text-transform: capitalize;
	}

	#fields_settings > .span8, #settings > .span4 {
		padding: 2%;
	}

	.column {
		padding: 3% 0;
		border-bottom: 1px solid #e4e6e5;
	}

	.cell {
		display: table-cell;
		vertical-align: middle;
	}

	.span3.cell {
		text-align: center;
		padding-top: 1%;
	}
	.span9.cell{
		padding-top: 1%;
	}
	label {
		display: inline-block;
		margin-bottom: 10px;
	}

	.p-sprite.move.setting-handle {
		display: none;
	}

	.span9.cell:hover .p-sprite.move.setting-handle {
		display: block;
		cursor: pointer;
	}

	.hiddenArrow {
		display: none !important;
	}
</style>
<h1><?= Yii::t('classroom', 'Custom attendance sheet') ?></h1>
<?php $order = explode(',', $order); ?>
<div id="fields_settings" class="settings-tab hidden-tab player-settings-form">
	<div class="coursesettings-grey-box">

		<div class="row-fluid">
			<div class="span12">
				<?= CHtml::beginForm('', 'POST', array('id' => 'settingsForm', 'class' => 'ajax')); ?>
				<div>
					<input type="checkbox" value="1" name="enabledCustomSettings" id="enabledCustomSettings">
					<label
						for="enabledCustomSettings"> <?= Yii::t('classroom', 'Enable custom attendance sheet settings for this course') ?></label>
				</div>
				<div class="column row-fluid">
					<div class="span3 cell" style="text-transform: uppercase;text-align: center">
						<strong><?= Yii::t('automation', 'Column'); ?></strong>
					</div>
					<div class="span9 cell" style="text-transform: uppercase;">
						<strong><?= Yii::t('dashboard', 'Display'); ?></strong>
					</div>
				</div>
				<div id="active">
					<?php
					foreach ($order as $key => $item) {
						$number = array();
						preg_match('/[1-9]/', $item, $number);
						$nameOfVar = 'dropdown' . $number[0];
						?>
						<div class="column row-fluid">
							<div class="span3 cell" style="text-align: center">
								<?= ++$key ?>
							</div>
							<div class="span9 cell">
								<?= $$nameOfVar; ?>
								<span class="p-sprite move setting-handle pull-right"></span>
							</div>
						</div>
					<?php } ?>
				</div>
				<input type="hidden" name="order" value="<?= implode(',', $order) ?>" id="ordering">

				<div class="form-actions" style="margin-top: 10px">
					<input name="submitSettings" type="submit" value="<?= Yii::t('standard', '_SAVE') ?>"
						   class="btn-docebo green big"/>
					<button id="cancelAction" class="btn-docebo black big"><?= Yii::t('standard', '_CANCEL') ?></button>
				</div>
				<div class="clearfix"></div>
				<?= CHtml::endForm(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).on('change', '#first', function () {
		var choise = $(this).val();
		if (choise != 0) {
			$("#second option[value!='" + choise + "']").show();
			$("#second option[value='" + choise + "']").hide();
		} else {
			$("#second option[value!='" + choise + "']").show();
		}
	});
	$(document).on('change', '#second', function () {
		var choise = $(this).val();
		if (choise != 0) {
			$("#first option[value!='" + choise + "']").show();
			$("#first option[value='" + choise + "']").hide();
		} else {
			$("#first option[value!='" + choise + "']").show();
		}
	});
	$(function () {
		var fullListOptionElements = $('#active .span9.cell select').first().find('option');

		$('.modal input,.modal select').styler();
		<?php
		if($customFlag){?>
		$('#enabledCustomSettings').trigger('click');
		<?php }
		?>
		var checked = $('#enabledCustomSettings').is(':checked')
		toggleSelect(checked);

		$('#active').sortable({
			items: '.column.row-fluid',
			handle: '.setting-handle',
			update: function (e, ui) {
				reorderColumns();
			}
		});

		$('#cancelAction').click(function (e) {
			e.preventDefault();
			$('.modal:visible').find('.modal-body').dialog2('close');
			var link = $('<a class="open-dialog" data-dialog-class="attendance-sheet-print-dialog" style="display: block" href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/axAttendanceSheet', array('course_id' => Yii::app()->request->getParam('course_id'),'id_session' => Yii::app()->request->getParam('id_session'))) ?>">1</a>');
			$('body').append(link);
			$(document).controls();
			link.trigger('click');
			link.remove();
		});

		$(document).off('change', '#active .span9.cell select').on('change', '#active .span9.cell select', function () {
			// count all elements with no values
			var br = 0;
			var selects = $('#active .span9.cell select');
			var modalBody = $(this).closest('.modal-body');

			$.each(selects, function (index, element) {
				element = $(element);
				if (element.val() == 0) br++;
			});
			populateDropdowns(fullListOptionElements);
			if (br == selects.length) {
				var html = '<button type="button" class="close" data-dismiss="alert">×</button><?=Yii::t('standard', 'You must select at least one item. Drag & drop to reorder items')?>'
				var errorDiv = $('<div class="alert alert-error">');
				errorDiv.html(html);
				$('.modal-body .alert.alert-error').remove();
				modalBody.prepend(errorDiv);
				$('.modal-footer a:first-of-type').hide();
			} else {
				$('.modal-footer a:first-of-type').show();
			}
		});

		populateDropdowns(fullListOptionElements);
	});

	$('#enabledCustomSettings').change(function () {
		var checked = $(this).is(':checked');
		toggleSelect(checked);
	});

	function populateDropdowns(fullListOptionElements) {
		// get all selected items
		var selects = $('#active .span9.cell select');
		var selectedValues = [];

		// get preselected values
		$.each(selects, function (index, element) {

			if ($.browser.msie) {
				if (element.value != 0)
					selectedValues.push(element.value);
			} else {
				if ($(element).val() != 0)
					selectedValues.push($(element).val());
			}
		});

		// go for each dropdowns
		$.each(selects, function (index, element) {
			element = $(element);
			var valueOfTheSelect = element.val();
			if ($.browser.msie) valueOfTheSelect = element[0].value;
			element.empty();

			for (var i = 0; i < fullListOptionElements.length; i++) {
				element.append($(fullListOptionElements[i]).clone());
			}

			var optionElements = element.find('option');

			for (var i = 0; i < selectedValues.length; i++) {
				// remove already selected item from the dropdown
				element.find('option[value="' + selectedValues[i] + '"]').remove();
			}

			if ($.browser.msie) {
				if (selectedValues.indexOf(valueOfTheSelect) != -1 || valueOfTheSelect == 0) {
					for (var j = 0; j < optionElements.length; j++) {
						if (optionElements[j].value == valueOfTheSelect) {
							var option = $(optionElements[j]);
							option.attr('selected', true);
							element.append(option[0]);
							break;
						}
					}
				}
			} else {
				// normal browser is used :)
				if (selectedValues.indexOf(valueOfTheSelect) != -1 || valueOfTheSelect == 0) {
					for (var j = 0; j < optionElements.length; j++) {
						if (optionElements[j].value == valueOfTheSelect) {
							var option = $(optionElements[j]);
							option.attr('selected', true);
							element.append(option[0]);
							break;
						}
					}
				}
			}
		});
	}

	function toggleSelect(checked) {
		if (!checked) {
			$('#active select').prop('disabled', true);
			$('#active').fadeTo(0, 0.5);
			$('.modal #fields_settings .p-sprite.move.setting-handle').addClass('hiddenArrow');
		} else {
			$('#active select').prop('disabled', false);
			$('#active').fadeTo(0, 1);
			$('.modal #fields_settings .p-sprite.move.setting-handle').removeClass('hiddenArrow');
		}
	}

	function reorderColumns() {
		var numbers = $('#active .span3.cell');
		$.each(numbers, function (index, element) {
			$(element).text(index + 1);
		});
		var fields = $('#active .span9.cell select');
		var order = [];
		$.each(fields, function (index, element) {
			element = $(element);
			order.push(element.attr('name'));
		});
		order = order.join(',');
		$('#ordering').val(order);
	}
</script>