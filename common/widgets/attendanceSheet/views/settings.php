<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 19-Aug-15
 * Time: 11:41 AM
 * @var $form CActiveForm
 * @var $settings AttendanceSheetMainField
 */
$this->breadcrumbs = array(
		Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
		Yii::t('standard', '_CLASSROOM'),
		Yii::t('classroom', 'Settings'),
); ?>
<style>
	#settings {
		background-color: #f1f3f2;
	}

	#settings .span4 > h4, #classroom_allocation .span4 > h4 {
		text-transform: capitalize;
		margin-bottom: 3%;
	}

	#settings .span4 > span {
		color: grey;
	}

	#settings > .span8, #settings > .span4,#classroom_allocation > .span8, #classroom_allocation > .span4 {
		padding: 2%;
	}

	.span8 {
		font-size: 1.1em;
	}

	.column {
		padding: 3% 0;
		border-bottom: 1px solid #e4e6e5;
	}

	.cell {
		display: table-cell;
		vertical-align: middle;
	}
	.span3.cell{
		text-align: center;
		padding-top: 1%;
	}
	.span9.cell{
		padding-top: 1%;
	}
	.p-sprite.move.setting-handle{
		display: none;
	}
	.span9.cell:hover .p-sprite.move.setting-handle{
		display: block;
		cursor: pointer;
	}
</style>
<div>
	<a href="http://www.docebo.com/knowledge-base/how-to-activate-and-manage-the-new-ilt-classroom-app/" target="_blank"
	   class="app-link-read-manual">Read Manual</a>

	<div class="clearfix"></div>
</div>
<div id="content">
	<h3><?= Yii::t('classroom', 'Settings') ?></h3>
	<hr>
	<?php
	DoceboUI::printFlashMessages();
	$order = explode(',', $order);
	?>

	<?= CHtml::beginForm('', 'POST', array('id' => 'settingsForm')); ?>
	<div id="classroom_allocation" class="row-fluid">

		<div class="setting-name span4">
			<h4><?= Yii::t('classroom', 'Classroom allocation') ?></h4>
		</div>
		<div class="setting-value span8">
			<label for="prevent_session_overlap" class="checkbox">
				<input type="checkbox" value="1" <?php if($prevent_session_overlap == 1) {echo 'checked="checked"';} ?> name="prevent_session_overlap" id="prevent_session_overlap">
				<?php echo Yii::t('classroom', 'Prevent overlapping of classroom sessions'); ?>
			</label>
		</div>

	</div>
	<div id="settings" class="row-fluid">
		<div class="span4">
			<h4><?= Yii::t('classroom', 'Custom attendance sheet') ?></h4>
				<span>
					<?= Yii::t('classroom', 'Customize your custom attendance sheet for your classroom session') ?>
				</span>
		</div>
		<div class="span8">
			<?= Yii::t('classroom', 'You can display and order maximum 5 columns in your custom attendance sheet, using your custom user fields') ?>
			<div class="column row-fluid">
				<div class="span3 cell">
					<strong><?= Yii::t('automation', 'Column'); ?></strong>
				</div>
				<div class="span9 cell" id="firstChoise">
					<strong><?= Yii::t('dashboard', 'Display'); ?></strong>
				</div>
			</div>
			<div id="sortable-target">
				<?php
				foreach($order as $key=>$item){
					$number = array();
					preg_match('/[1-9]/',$item, $number);
					$nameOfVar = 'dropdown'.$number[0];
					?>
					<div class="column row-fluid">
						<div class="span3 cell">
							<?=++$key;?>
						</div>
						<div class="span9 cell">
							<?= $$nameOfVar ?>
							<span class="p-sprite move setting-handle pull-right"></span>
						</div>
					</div>
				<?php }
				?>
			</div>
		</div>
	</div>
	<input type="hidden" name="order" value="<?=implode(',', $order)?>" id="ordering">
	<div class="pull-right" style="margin-top: 10px">
		<input name="submitSettings" type="submit" value="<?= Yii::t('standard', '_SAVE') ?>" class="btn-docebo green big"/>
		<a href="<?= Yii::app()->createUrl('') ?>"
		   class="btn-docebo black big"><?= Yii::t('standard', '_CANCEL') ?>
		</a>
	</div>
	<div class="clearfix"></div>
	<?= CHtml::endForm(); ?>
</div>
<script>
	$(function(){
		var fullListOptionElements = $('#sortable-target .span9.cell select').first().find('option');

		$("#sortable-target").sortable({
			items: '.column.row-fluid',
			handle: '.setting-handle',
			update: function(e, ui){
				reorderColumns();
			}
		});

		$('#prevent_session_overlap').styler();

		$(document).off('change', '#sortable-target .span9.cell select').on('change', '#sortable-target .span9.cell select', function(){
			// count all elements with no values
			var br = 0;
			var selects = $('#sortable-target .span9.cell select');
			$.each(selects, function (index, element) {
				element = $(element);
				if(element.val() == 0) br++;
			});
			populateDropdowns(fullListOptionElements);
			if (br == selects.length){
				var html = '<?=Yii::t('standard', 'You must select at least one item. Drag & drop to reorder items')?>'
				Docebo.Feedback.show('error', html);
				$('input[name="submitSettings"]').prop('disabled', true);
				$('input[name="submitSettings"]').addClass('disabled');
			}else{
				$('input[name="submitSettings"]').prop('disabled', false);
				$('input[name="submitSettings"]').removeClass('disabled');
			}
		});

		populateDropdowns(fullListOptionElements);
	});

	function reorderColumns(){
		var numbers = $('#sortable-target .span3.cell');

		$.each(numbers, function (index, element) {
			$(element).text(index + 1);
		});
		var fields = $('#sortable-target .span9.cell select');
		var order = [];
		$.each(fields, function(index,element){
			element = $(element);
			order.push(element.attr('name'));
		});
		order = order.join(',');
		$('#ordering').val(order);
	}

	function populateDropdowns(fullListOptionElements) {
		// get all selected items
		var selects = $('#sortable-target .span9.cell select');
		var selectedValues = [];

		// get preselected values
		$.each(selects, function (index, element) {

			if ($.browser.msie) {
				if (element.value != 0)
					selectedValues.push(element.value);
			} else {
				if ($(element).val() != 0)
					selectedValues.push($(element).val());
			}
		});

		// go for each dropdowns
		$.each(selects, function (index, element) {
			element = $(element);
			var valueOfTheSelect = element.val();
			if ($.browser.msie) valueOfTheSelect = element[0].value;
			element.empty();

			for (var i = 0; i < fullListOptionElements.length; i++) {
				element.append($(fullListOptionElements[i]).clone());
			}

			var optionElements = element.find('option');

			for (var i = 0; i < selectedValues.length; i++) {
				// remove already selected item from the dropdown
				element.find('option[value="' + selectedValues[i] + '"]').remove();
			}

			if ($.browser.msie) {
				if (selectedValues.indexOf(valueOfTheSelect) != -1 || valueOfTheSelect == 0) {
					for (var j = 0; j < optionElements.length; j++) {
						if (optionElements[j].value == valueOfTheSelect) {
							var option = $(optionElements[j]);
							option.attr('selected', true);
							element.append(option[0]);
							break;
						}
					}
				}
			} else {
				// normal browser is used :)
				if (selectedValues.indexOf(valueOfTheSelect) != -1 || valueOfTheSelect == 0) {
					for (var j = 0; j < optionElements.length; j++) {
						if (optionElements[j].value == valueOfTheSelect) {
							var option = $(optionElements[j]);
							option.attr('selected', true);
							element.append(option[0]);
							break;
						}
					}
				}
			}
		});
	}
</script>
