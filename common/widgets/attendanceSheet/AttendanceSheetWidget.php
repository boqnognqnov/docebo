<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 22-Feb-16
 * Time: 1:56 PM
 */
class AttendanceSheetWidget extends CWidget
{

	public $msg;
	public $breadcrumbs;
	public $renderPartial;
	public $settings;
	public $order;
	public $prevent_session_overlap;

	public function run()
	{
		$dropdown1 = $this->fillDropDown(1);
		$dropdown2 = $this->fillDropDown(2);
		$dropdown3 = $this->fillDropDown(3);
		$dropdown4 = $this->fillDropDown(4);
		$dropdown5 = $this->fillDropDown(5);
		$order = $this->settings->order;
		$prevent_session_overlap = $this->settings->prevent_session_overlap;

		if ($this->renderPartial == true) {
			((isset($_POST['enabledCustomSettings']) && $_POST['enabledCustomSettings'] == 1) || $this->settings->id != 1)? $customFlag = true : $customFlag = false;
			Yii::app()->getController()->renderPartial('common.widgets.attendanceSheet.views.fields_settings',
				array('dropdown1' => $dropdown1, 'dropdown2' => $dropdown2, 'dropdown3' => $dropdown3, 'dropdown4' => $dropdown4, 'dropdown5' => $dropdown5, 'msg' => $this->msg, 'order' => $order, 'prevent_session_overlap' => $prevent_session_overlap,  'customFlag' => $customFlag), false, true);
		} else {
			Yii::app()->getController()->render('common.widgets.attendanceSheet.views.settings',
					array('settings' => $this->settings, 'dropdown1' => $dropdown1, 'dropdown2' => $dropdown2, 'dropdown3' => $dropdown3, 'dropdown4' => $dropdown4, 'dropdown5' => $dropdown5, 'order' => $order, 'prevent_session_overlap' => $prevent_session_overlap, 'msg' => $this->msg), false, true);
		}
	}

	private function fillDropDown($number)
	{
		$listOfFieldsTitles = $this->getListOfFields();
		switch($number) {
			case 1:
				$field = 'fieldName1';
				break;
			case 2:
				$field = 'fieldName2';
				break;
			case 3:
				$field = 'fieldName3';
				break;
			case 4:
				$field = 'fieldName4';
				break;
			case 5:
				$field = 'fieldName5';
				break;
		}
		$idCourse = intval(Yii::app()->request->getParam('course_id', 0));
		$idSession = intval(Yii::app()->request->getParam('id_session'));
		$selected = Yii::app()->db->createCommand()
				->select($field)
				->from(AttendanceSheetMainField::model()->tableName())
				->where('idCourse=:id AND idSession=:session', array(':id' => $idCourse, ':session' => $idSession))->queryScalar();

		if ($selected && $selected != 'fullname') {
			$sql = "SELECT f.translation
			FROM `core_field` f
			WHERE f.`id_common`=(SELECT DISTINCT(cf.`id_common`) FROM `core_field` cf WHERE cf.`translation`=:sourseLang)
			AND f.`lang_code`=:targetLang";

			$targetLang = Yii::app()->db->createCommand()
					->select('lang_code')
					->from(CoreLangLanguage::model()->tableName())
					->where('lang_browsercode=:lang', array(':lang' => Yii::app()->getLanguage()))->queryScalar();
			$selectedEnglish = Yii::app()->db->createCommand($sql)
					->queryScalar(array(':sourseLang' => $selected, ':targetLang' => 'english'));
			if ($selectedEnglish) $selected = $selectedEnglish;
		} elseif ($selected != 'fullname') {
			$selected = Yii::app()->db->createCommand()
					->select($field)
					->from(AttendanceSheetMainField::model()->tableName())
					->where('id=:id', array(':id'=>$this->settings->id))->queryScalar();
		}

		$html = CHtml::dropDownList('fieldName'.$number, '', $listOfFieldsTitles,
				array('options' => array($selected => array('selected' => true))));
		return $html;
	}

	private function getListOfFields()
	{

		$langCode = Yii::app()->getLanguage();
		$language = Yii::app()->db->createCommand()
			->select('lang_code')
			->from(CoreLangLanguage::model()->tableName())
			->where('lang_browsercode=:lang', array(':lang' => $langCode))->queryScalar();
        $criteria = new CDbCriteria;
        $criteria->condition = 'type != :type';
        $criteria->params = ['type' => CoreUserField::TYPE_UPLOAD];
//		$listOfFields = CoreUserField::model()->language($language, true)->findAll($criteria);
		$listOfFields = CoreUserField::model()->language('bulgarian', true)->findAll($criteria);
		$listOfFieldsTitles = array();
		foreach ($listOfFields as $field) {
            /** @var CoreUserField $field */
			$listOfFieldsTitles[$field->getFieldId()] = $field->getTranslation();
		}

		$userFields = array(
			0 => Yii::t('classroom', 'Do not display this column'),
			'signature1' => Yii::t('standard', '_SIGNATURE') . ' (' . Yii::t('classroom', 'first half') . ')',
			'signature2' => Yii::t('standard', '_SIGNATURE').' (' . Yii::t('classroom', 'second half') . ')',
			'username' => Yii::t('standard', '_USERNAME'),
			'email' => Yii::t('standard', '_EMAIL'),
			'language' => Yii::t('standard', '_LANGUAGE'),
			'fullname' => Yii::t('classroom', 'Name and Surname'));

        // @todo The code below is not used, but it was not removed for some reason, so just commenting it out for now,
        // if not needed should be removed permanently. If the labels should be displayed in a current language, fetching
        // the labels could be done using CoreUserField::getTranslations(), as the second parameter = true would return
        // fallback labels where there is no proper label value set for the primary language of choice
//		$listOfFieldsTitlesDefaultlanguage = Yii::app()->db->createCommand()
//				->select('id_common, translation')
//				->from(CoreField::model()->tableName())
//				->where('lang_code=:default AND type_field != :type',
//						array(
//								':default' => Settings::get('default_language'),
//								':type' => CoreField::TYPE_FIELD_UPLOAD,
//						))->queryAll();
//		$resultListDefaultLanguage = array();
//		foreach($listOfFieldsTitlesDefaultlanguage as $item){
//			$resultListDefaultLanguage[$item['id_common']] = $item['translation'];
//		}
//		$finalListOfFieldsTitles = array_merge($resultListDefaultLanguage, $listOfFieldsTitles);

		return $userFields + $listOfFieldsTitles;
	}
}