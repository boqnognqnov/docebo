<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 25.11.2015 г.
 * Time: 13:43
 */
class TabbedContent extends CWidget{

    /**
     * @var $content
     *
     * Example
     *
     * array(
            array(
               'header' => 'title1',
               'content' => $this->renderPartial('_path_to_view, $array_of_data, true),
               'active' => true
            ),
            array(
               'header' => 'title2',
               'content' => $this->renderPartial('_path_to_view, $array_of_data, true),
            )
      )
     */
    public $content;

    /**
     * @var $containerClass
     *
     * Example
     * 'containerClass' => 'tabbed-my-container-name'
     */
    public $containerClass;

    /**
     * @var $cssFile
     *
     * Example
     *
     * 'cssFile' => 'path_to_folder/file.css'
     *
     */
    public $cssFile;

    /**
     * @var $basSliderColor
     *
     * Example 'tabsSliderColor' => 'red' || '#f24f4f' || '#000'
     */
    public $tabsSliderColor;

    /**
     * @var $tabsUppercased
     *
     * Example 'tabsUppercased' => true
     */
    public $tabsUppercased = true;

    function init(){
        //Register the css file only if file exists
        if(!empty($this->cssFile) && file_exists($this->cssFile)){
            $clientScript = Yii::app()->getClientScript();
            $clientScript->registerCssFile($this->cssFile);
        }

        //Generate container class if is empty
        if(!$this->containerClass || empty($this->containerClass)){
            $this->containerClass = 'tabbed-content-' . Docebo::randomHash();
        }

        //Prevent for multiple active tabs
        if(!empty($this->content)){
            $foundActive = false;
            foreach($this->content as &$item){
                if(!empty($item['active']) && $item['active'] === true){
                    if($foundActive === false){
                        $foundActive = true;
                    } else{
                        $item['active'] = false;
                    }
                }
            }
        }

        if(!$this->tabsSliderColor){
            $this->tabsSliderColor = '#0465AC';
        }
    }

    function run(){
        //Show the tabs only if we have some data in the content!!!
        if(is_array($this->content) && !empty($this->content)){
            $this->render('common.widgets.views.tabbedContent.index', array(
                'content' => $this->content,
                'containerClass' => $this->containerClass,
                'tabSliderColor' => $this->tabsSliderColor,
                'tabsUppercased' => $this->tabsUppercased
            ));
        }
    }
}