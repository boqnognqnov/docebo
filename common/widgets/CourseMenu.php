<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class CourseMenu extends CWidget {
    
    public $userModel = null;
    public $courseModel = null;
    public $courseUserModel = null;
    public $authManager = null;

	public function init() {
		parent::init();
	}


	private function getCourseMenuItems() {

		$menu = array();
		
		// This menu makes sense only for valid user, course and subscription
		if ($this->userModel === null || $this->courseModel === null || $this->courseUserModel === null) {
		    return $menu;
		}

		$criteria = new CdbCriteria();
		$criteria->condition='idCourse=' . (int) $this->courseModel->idCourse;
		$criteria->order = 'sequence';

		$menuItems = LearningMenucourseMain::model()->findAll($criteria);

		// Get menu items
		foreach($menuItems as $menuItem) {
			$menu[$menuItem->idMain] = array(
				'submenu' => array(),
				'main' => array(
					'name' => Yii::t('menu_course', $menuItem->name),
					'link' => 'index.php?id_module_sel=0&amp;id_main_sel=' . $menuItem->idMain
				)
			);
		}

		// Get submenu items
		$criteriaUnder = new CdbCriteria();
		$criteriaUnder->condition = 't.idCourse=' . (int) $this->courseModel->idCourse;
		$criteriaUnder->order = 'sequence';

		$subMenuItems = LearningMenucourseUnder::model()->with('learningModule')->findAll($criteriaUnder);


		foreach($subMenuItems as $subItem) {

			$subItemName = ($subItem->my_name != '' ? $subItem->my_name : Yii::t('menu_course', $subItem->learningModule->default_name));
			$accessItemName = '/lms/course/private/' . $this->courseModel->idCourse . '/' . $subItem->learningModule->module_name . '/' . $subItem->learningModule->token_associated;
			$accessGranted = $this->authManager->checkAccess($accessItemName, $this->userModel->idst) || $this->authManager === null;
			 
			if ($accessGranted) {
			    // Special case, we are moving this to Yii
			    if ($subItem->learningModule->module_name == 'conference') {
			        $menu[$subItem->idMain]['submenu'][$subItem->idModule] = array(
			                'name' => $subItemName,
			                'link' => '/lms/index.php?r=videoconference&amp;course_id=' . (int) $this->courseModel->idCourse,
			        );
			    }
			    else {
			        $menu[$subItem->idMain]['submenu'][$subItem->idModule] = array(
			                'name' => $subItemName,
			                'link' => ($subItem->learningModule->mvc_path != ''
			                        ? '/doceboLms/index.php?r=' . $subItem->learningModule->mvc_path . '&amp;id_module_sel=' . $subItem->idModule . '&amp;id_main_sel=' . $subItem->idMain
			                        : '/doceboLms/index.php?modname=' . $subItem->learningModule->module_name . '&amp;op=' . $subItem->learningModule->default_op . '&amp;id_module_sel=' . $subItem->idModule . '&amp;id_main_sel=' . $subItem->idMain
			                )
			        );
			    }
			}
		}
		
		// Removing the main items without subitems
		foreach($menu as $key => $value) {
      if(count($value["submenu"])==0) {
          unset($menu[$key]);
      }
    }

		return $menu;

	}


	public function run() {
		$this->render('common.widgets.views.courseMenu.course_menu', array(
			'menuItems' => $this->getCourseMenuItems()
		));
	}

}
