<?php
class InfoBox extends CWidget {

	const TYPE_INFO 	= 'info';
	const TYPE_SUCCESS 	= 'success';
	
	public $type = self::TYPE_INFO; 
	public $text;
	public $iconClass;
	public $class;
	
	public function init() {
		
		parent::init();
		
		if (!$this->iconClass) {
			switch ($this->type) {
				case self::TYPE_INFO:
					$this->iconClass = "i-sprite is-solid-exclam large";
					
					break;
						
				case self::TYPE_SUCCESS:
					$this->iconClass = "i-sprite is-check large white";
					break;
						
				default:
					$this->iconClass = "";
					break;
						
			}
		}
		
		if (!$this->class) {
			$this->class = 'info-box-' . $this->type;
		}
		
		
		
	}
	
	
	private function resolveIcon() {
		
		
	} 

	/**
	 * RUN
	 * @see CWidget::run()
	 */
	public function run() {
		$this->render('common.widgets.views.info_box', array());
	}

}