<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DoceboAlert
 *
 * @author Stanimir
 */
class DoceboAlert extends CWidget {

	public $id = false;
	public $type = 'warning';
	public $isGotIt = false;
	public $smb = false;
	public $message;
	public $view = 'doceboAlert/_alert';
	public $ajaxURL = null;

	public function init() {
		if (!$this->ajaxURL) {
			$this->ajaxURL = Docebo::createLmsUrl('site/AxSetGotIt');
 		}
	}

	public function run() {
		if (!$this->id) {
			throw new Exeption('Missing "Alert" ID for widget [' . __CLASS__ . ']');
		}
		$render = true;
		if ($this->isGotIt && CoreSettingUser::model()->findByAttributes(array('path_name' => $this->id, 'id_user' => Yii::app()->user->idst))) {
			$render = false;
		}

		if ($render) {
			$this->render($this->view, array(
				'id' => $this->id,
				'type' => $this->type,
				'message' => $this->message,
 				'ajaxURL' => $this->ajaxURL,
				'isGotIt' => $this->isGotIt,
			));
		}
	}

}
