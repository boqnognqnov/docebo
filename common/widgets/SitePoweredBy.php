<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

/**
 * This widget print out the powered by functionalities, it's here because in the near future buying some apps
 * the powered by will be customizable by the end user.
 */
class SitePoweredBy extends CWidget {
	
	/**
	 * Keep current year
	 * @var string
	 */
	public $year = null;

    /**
     * Method called in order to run the widget
     * @see CWidget::run()
     */
    public function run() {

    	$this->year = date('Y');

        $this->render('common.widgets.views.sitePoweredBy.index', array(
			'customText' => BrandingWhiteLabelForm::getFooterText(),
			'customSite' => BrandingWhiteLabelForm::getSiteReplacement(),
        ));
    }




}