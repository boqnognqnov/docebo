<?php

/**
 * Description of app7020ContentListView
 *
 * @author Kristian
 */
class app7020ContentListView extends CWidget {

	public $listParams = array();
	public $gridColumns = 3;
	public $noItemViewContainer = true;
	public $disableMassSelection = true;
	public $disableMassActions = true;
	public $disableFiltersWrapper = true;
	public $disableCheckBox = true;
	public $titleAsLink = false;
	public $linkToEditMode = false;
	public $blankTemplates = array();
	protected $_blankTemplates = array(
		'page' => 'common.widgets.views.app7020ContentListView.defaultPageBlank',
		'search' => 'common.widgets.views.app7020ContentListView.defaultSearchBlank'
	);
	public $extensions = '';
	protected $_extensions = array(
		'available' => 'time,count,asterisk,new,watched,gurus,inviters,topics,rating,ignore,unIgnore,gotIt,edit',
		'default' => 'time'
	);

	/* Predefined callbacks for this widget */
	protected $_beforeUpdate = "app7020.app7020ContentListView.listViewBeforeUpdate(this, id, options);";
	protected $_afterUpdate = "app7020.app7020ContentListView.listViewAfterUpdate(this, id, data);";

	public function run() {
		$this->render('common.widgets.views.app7020ContentListView.index');
	}

}
