<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MobileAppSettings
 *
 * @author ignatov
 */
class MobileAppSettings extends CWidget{
	//put your code here
	
	public $action = null;
	public $loginColor;
	public $primaryColor;
	public $logo;
	public $isMultidomain = false;
    public $isEnabled = false;
	
	public function init(){
		parent::init();
		if(is_null($this->action)){
			throw new CException("You must set action url for the form");
		}
	}
	
	
	public function run(){
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl."/js/angularJs/angular.js", CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/../plugins/WebApp/assets/js/ngAppSettings.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScript('mobileapp_settings', "window.mobileAppSettings = {primaryColor: '".$this->primaryColor."', loginColor: '".$this->loginColor."', logo: '".$this->logo."'}", CClientScript::POS_BEGIN);

		if($this->logo) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$logo = $storageManager->fileUrl($this->logo);
		} else
			$logo = Yii::app()->theme->getLogoUrl(true);

		return $this->render('mobileapp/settings', array('primaryColor' => $this->primaryColor, 'loginColor' => $this->loginColor, 'action' => $this->action, 'logo' => $logo, 'isMultidomain' => $this->isMultidomain, 'originalLogo' => $this->logo, 'isEnabled' => $this->isEnabled));
	}
}
