<?php

/**
 * Class WebinarAccountsEditor
 *
 * Renders a grid of webinar accounts for videoconference tools that allow it
 */
class WebinarAccountsEditor extends CWidget {

	/**
	 * The new/edit account URL
	 * @var string
	 */
	public $editAccountUrl;
    
    /*
     * Is already set account for this webinar tool
     * @var boolean
     */
    public $isSetAccount;
    
    /*
     * Does account support single or multi account
     * @var int
     */
    public $isSingleAccount;



    /**
	 * The delete account URL
	 * @var string
	 */
	public $deleteAccountUrl;

	/**
	 * @var CActiveDataProvider
	 */
	public $dataProvider;

	/**
	 * Whether the widget should render the delete account dialog or not
	 * @var boolean
	 */
	public $showDeleteView;

	/**
	 * The current account model
	 * @var WebinarToolAccount
	 */
	public $accountModel;

	/**
	 * If multiaccount should be enabled
	 * @var boolean
	 */
	public $enableMultiAccount;

	/**
	 * If not set, the modal width will default to 350px
	 * @var int
	 */
	public $modalWidth;

	/**
	 * Initializer
	 */
	public function init() {
		if(!$this->deleteAccountUrl)
			$this->deleteAccountUrl = 'webinarManagement/deleteAccount';

		if(empty($this->modalWidth)){
			$width = 350;
			$marginLeft = -150;
		} else {
			$width = $this->modalWidth;
			$marginLeft = (round(($width - 50) / 2) * -1);
		}

		$cs = Yii::app()->getClientScript(); /* @var $cs DoceboClientScript */
		$cs->registerCss('webinars', '
			.modal.modal-edit-webinar-account {
				width: '.$width.'px;
				margin-left: '.$marginLeft.'px;
			}
		');
    }

	/**
	 * Renders the widget content
	 */
	public function run() {
		if($this->showDeleteView)
			$this->render('webinarAccountDelete', array());
		else
			$this->render('webinarAccountsGrid', array());
    }

} 