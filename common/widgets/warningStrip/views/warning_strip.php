<? /* @var $this WarningStrip */ ?>
<div id="<?= $this->id ?>"
	class="warning-strip <?= $this->type ?> <?= $this->id ?>" <?= ( ! $this->message ? 'style="display:none"' : NULL ) ?>>
	<div class="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
	<div class="warning-text">
		<?= $this->message ?>
	</div>

</div>