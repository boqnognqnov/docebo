<?php

/**
 * A simple class for displaying warning/success/info/error messages
 * (varying backgrounds with a big exclamation point to the left
 *
 * Usage:
 * $this->widget('common.widgets.warningStrip.WarningStrip', array(
 *          'message'=>'Some error text',
 *          'type'=>'success|info|warning|error',
 * ));
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class WarningStrip extends CWidget{

	public $id = 'warning-strip';
	public $type = 'warning';
	public $message = null;

	public function init() {
		parent::init();

		// Validate type
		if(!in_array($this->type, array(
			'success', 'info', 'warning', 'error'
		))){
			$this->type = 'warning';
		}

		$ds = DIRECTORY_SEPARATOR;

		// Register assets
		Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(dirname(__FILE__).$ds.'assets'). '/warningstrip.css');
	}

	public function run() {
		parent::run();

		$this->render('warning_strip');
	}
}