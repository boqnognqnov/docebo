<?php

class ContentRating extends CWidget {

    public $contentId;
    public $readOnly = false;
	public $avgRating = false;

	public function init()
    {
        $clientScript = Yii::app()->getClientScript();
        $clientScript->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
    }

    public function run()
    {
        $this->render('content_rating', array('contentId' => $this->contentId, 'readOnly' => $this->readOnly));
    }

   
}