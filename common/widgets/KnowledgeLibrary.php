<?php

class KnowledgeLibrary extends CWidget {

	public $widgetOptions;

	public function init() {
		$cs = Yii::app()->getClientScript();
		$am = Yii::app()->assetManager;
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
		App7020Helpers::registerApp7020Options();
		$cs->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
	}

	public function run() {
		Yii::import('app7020.protected.components.*');
		Yii::import('app7020.protected.controllers.*');

		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');

		// ****** COMBO LIST PARAMS ****** //
		$sessionMainFilterKL = Yii::app()->session['knowledgeLibraryIndexMainFilter'];
		$sessionTopicsTree = Yii::app()->session['knowledgeLibraryIndexTopicsTree'];
		$provider = App7020Assets::sqlDataProvider(
						false, array(
					//'pageSize' => $this->widgetOptions["displayNumberItems"],
					'pageSize' => 12,
					'pagination' => $this->widgetOptions["paginateWidget"],
					'tags' => $this->widgetOptions["showAssetsTags"],
						), array(
					'sessionMainFilterKL' => $sessionMainFilterKL,
					'sessionTopicsTree' => $sessionTopicsTree
		));
		$listParams = array(
			'listId' => "recentlyAssetsListView",
			'dataProvider' => $provider,
//			'viewData' => array('dropDownParams' => $dropDownParams),
			'afterAjaxUpdate' => "app7020.knowledgeLibrary.listViewAfterUpdate();",
			'beforeAjaxUpdate' => "
                // Set this GET parameter to ask the dashlet to return HTML CONTENT only during ajax update
                options.data = options.data + \"&dashletHtmlOnly=1\";
            "
		);
		
		// ****** FILTER PARAMS ****** //
		$filtersArray = array(
			array(
				'title' => Yii::t('app7020', 'Assets type'),
				'name' => 'assets',
				'data' => App7020Assets::getAllTypeArray()
			),
			array(
				'title' => Yii::t('app7020', 'Sort by'),
				'name' => 'sort',
				'data' => array(
					'Date',
					'Videos duration'
				)
			),
			array(
				'title' => Yii::t('app7020', 'Guru\'s rating'),
				'name' => 'rating',
				'data' => array(
					'All'
				)
			)
			,
			array(
				'title' => Yii::t('app7020', 'Uploaded'),
				'name' => 'uploaded',
				'data' => array(
					'At any time',
					'This week',
					'This month',
					'In the pasts 6 months',
					'This year'
				)
			)
		);
		$this->render('knowledge_library', array(
			'listParams' => $listParams,
			'widgetParams' => $this->widgetOptions,
			'filtersArray' => $filtersArray,
			'have_permission'=> true
		));
	}

}
