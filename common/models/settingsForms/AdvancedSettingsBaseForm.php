<?php

class AdvancedSettingsBaseForm extends CFormModel {

    private static $allowCreate = array(
        'login_layout', 'hide_signin_form', 'favicon',
	    'minimal_login_type',
	    'minimal_login_background',
	    'minimal_login_video_fallback_img',
		'custom_css', 'scorm_debug',
        'timezone_default', 'timezone_allow_user_override', 'date_format', 'date_format_custom', 'date_format_custom_lang',
		'disable_registration_email_confirmation',
    	'whitelabel_mobile',
    	'whitelabel_mobile_replace_splashscreen',
    	'whitelabel_mobile_apple_store_link',
    	'whitelabel_mobile_google_play_store_link',
    	'whitelabel_mobile_splashscreen_file',
		'use_node_fields_visibility',
	    'whitelabel_menu',
	    'whitelabel_menu_userid',
	    'whitelabel_helpdesk',
	    'whitelabel_helpdesk_email',
	    'whitelabel_footer',
	    'whitelabel_footer_text',
	    'whitelabel_footer_ext_url',
	    'whitelabel_footer_ext_height',
	    'whitelabel_header',
	    'whitelabel_header_ext_url',
	    'whitelabel_header_ext_height',
	    'whitelabel_disable_naming',
	    'whitelabel_naming_site_enable',
	    'whitelabel_naming',
	    'whitelabel_naming_text',
	    'whitelabel_naming_site',

	    'default_mycourses_view',
	    'mycourses_visibility_activecourses',
	    'mycourses_visibility_completedcourses',
	    'mycourses_visibility_allcourses',
    	'show_course_after_enrollment',

		'course_sharing_permission',
		'course_user_share_permission',
		'course_share_facebook',
		'course_share_twitter',
		'course_share_linkedin',
		'course_share_google',
		'course_rating_permission',
	    'course_resume_autoplay',
        'soft_deadline',

		//Redirect user on logout
		'user_logout_redirect',
		'user_logout_redirect_url',
        'show_first_name_first',

        'return_path',

        /*Passwords */
        'pass_alfanumeric',
        'pass_special_chars',
        'pass_change_first_login',
        'pass_max_time_valid',
        'pass_min_char',
        'pass_not_username',
        'user_pwd_history_length',
        'pass_dictionary_check',

		'allowed_domains',

		'simplesaml_logging',
        
        'extended_tincan_launch_params'
    );


    public function beforeSave() {

    }

    public function save() {
        $this->beforeSave();

        foreach ($this->attributes as $param => $value) {
            if (in_array($param, self::$allowCreate)) {
				switch ($param) {
					case 'disable_registration_email_confirmation':
						Settings::save($param, $value, 'enum', 3, 'register', 3, 15, 0, 0);
						break;
					case 'allowed_domains':{
						Settings::save($param, json_encode($value), 'string', 255, 'register', 3, 15, 0, 0);
					}break;
					default:
						Settings::save($param, $value);
						break;
				}
            }
            else {
                Settings::update($param, $value);
            }
        }
    }

}