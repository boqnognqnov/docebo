<?php

class AdvancedSettingsPasswordForm extends AdvancedSettingsBaseForm {

	private $type = 'password';

	protected $_params;

	public $pass_alfanumeric;
	public $pass_special_chars;
	public $pass_change_first_login;
	public $pass_max_time_valid;
	public $pass_min_char;
	public $pass_not_username;
	public $user_pwd_history_length;
    public $pass_dictionary_check;

	public function rules() {
		return array(
			array('pass_alfanumeric, pass_special_chars, pass_change_first_login, pass_max_time_valid, pass_min_char, pass_not_username, user_pwd_history_length, pass_dictionary_check', 'safe')
		);
	}

	public function init() {
		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting t');
//		    ->where('t.pack = :type AND t.hide_in_modify = 0', array(':type' => $this->type));
        // We disable this check because we are not using type and hide_in_modify in core_setting table, :)

		$result = $command->queryAll();

		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {
				$this->_params[$row['param_name']] = $row;
				$this->{$row['param_name']} = $row['param_value'];
			}
		}
	}

	public function attributeLabels() {
		return array(
			'pass_alfanumeric'          => Yii::t('configuration', '_PASS_ALFANUMERIC'),
			// 'pass_special_chars'     => '_PASS_SPECIAL_CHARS', // not yet impletemented
			'pass_change_first_login'   => Yii::t('configuration',  '_PASS_CHANGE_FIRST_LOGIN'),
			'pass_max_time_valid'       => Yii::t('configuration', '_PASS_MAX_TIME_VALID'),
			'pass_min_char'             => Yii::t('configuration', '_PASS_MIN_CHAR'),
			'pass_not_username'         => Yii::t('configuration',  '_PASS_NOT_USERNAME'),
			'user_pwd_history_length'   => Yii::t('configuration', '_USER_PWD_HISTORY_LENGTH'),
            'pass_dictionary_check'     => Yii::t('configuration', 'Password dictionary check'),
		);
	}

}