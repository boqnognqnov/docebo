<?php

class AdvancedSettingsRegisterForm extends AdvancedSettingsBaseForm {

	private $type = 'register';

	protected $_params;

	public $hour_request_limit;
	public $lastfirst_mandatory;
	public $mail_sender;
	public $mandatory_code;
	public $privacy_policy;
	public $register_type;
	public $registration_code_type;
	public $disable_registration_email_confirmation;
	public $allowed_domains;

	public function rules() {
		return array(
			array('hour_request_limit, lastfirst_mandatory, mail_sender, mandatory_code, privacy_policy, register_type, registration_code_type, disable_registration_email_confirmation', 'safe'),
			array('hour_request_limit' , 'numerical', 'integerOnly' => true),
			array('allowed_domains', 'safe'),
			array('allowed_domains', 'domainValidation'),
			array('mail_sender' , 'email'),
			array('mail_sender' , 'validEmail')
		);
	}

	public function init() {
		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting t')
		    ->where('t.pack = :type AND t.hide_in_modify = 0', array(':type' => $this->type));

		$result = $command->queryAll();

		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {
				if($row['param_name'] == 'allowed_domains'){
					$this->_params[$row['param_name']] = $row;
					$this->{$row['param_name']} = json_decode($row['param_value']);
				}else{
					$this->_params[$row['param_name']] = $row;
					$this->{$row['param_name']} = $row['param_value'];
				}
			}
		}
	}

	public function attributeLabels() {
		return array(
			'hour_request_limit' => Yii::t('configuration', '_HOUR_REQUEST_LIMIT'),
			'mail_sender' => Yii::t('configuration',  '_MAIL_SENDER'),
			'mandatory_code' => Yii::t('configuration', '_MANDATORY_CODE'),
			'register_type' => Yii::t('configuration', '_REGISTER_TYPE'),
			'registration_code_type' => Yii::t('configuration',  '_REGISTRATION_CODE_TYPE'),
			'privacy_policy' => Yii::t('configuration', '_PRIVACY_POLICY'),
			'lastfirst_mandatory' => Yii::t('configuration', '_LASTFIRST_MANDATORY'),
			'disable_registration_email_confirmation' => Yii::t('configuration', 'Do not send confirmation email at self-registration'),
			'allowed_domains' => Yii::t('configuration', 'Restrict domains')
		);
	}

	public function validEmail($attribute, $params) {

		if($this->mail_sender &&  (strpos($this->mail_sender, 'docebosaas') !== false))
		{
			$this->addError('mail_sender',Yii::t('notification', 'Please provide valid sender email'));
			return false;
		}
		return true;
	}

	public function domainValidation($attribute, $params){
		foreach($this->allowed_domains as $key =>$domain){
			$this->allowed_domains[$key] = '@' . trim($domain, '@');
			$valid = preg_match('/^@[\w\-]+\.[\w]+/', $this->allowed_domains[$key], $matches);
			if(!$valid){
				$this->addError('allowed_domains', Yii::t('notification', 'Please provide only valid domain names'));
				return false;
			}
		}
		return true;
	}

}