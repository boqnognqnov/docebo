<?php

class AdvancedSettingsSocialRatingForm extends AdvancedSettingsBaseForm {

	const COURSE_SHARE_DISABLED = 0;
	const COURSE_SHARE_IF_ENROLLED = 1;
	const COURSE_SHARE_IF_COMPLETED = 2;

	const COURSE_RATING_DISABLED = 0;
	const COURSE_RATING_ENABLED = 1;
	const COURSE_RATING_IF_ENROLLED = 2;
	const COURSE_RATING_IF_COMPLETED = 3;

	const SETTING_ON = 'on';
	const SETTING_OFF = 'off';

	protected $_params;

	public $course_sharing_permission;
	public $course_user_share_permission;
	public $course_share_facebook;
	public $course_share_twitter;
	public $course_share_linkedin;
	public $course_share_google;

	public $course_rating_permission;

	public function rules() {
		return array(
			array('course_sharing_permission, course_user_share_permission, course_share_facebook, course_share_twitter, course_share_linkedin, course_share_google, course_rating_permission', 'safe')
		);
	}


	public function init() {
		$fields = array(
			'course_sharing_permission',
			'course_user_share_permission',
			'course_share_facebook',
			'course_share_twitter',
			'course_share_linkedin',
			'course_share_google',
			'course_rating_permission'
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {
				$this->_params[$row['param_name']] = $row;
				$this->{$row['param_name']} = $row['param_value'];
			}
		}

		//default values for share and rate
		if ($this->course_sharing_permission === null) $this->course_sharing_permission = self::COURSE_SHARE_DISABLED;
		if ($this->course_rating_permission === null) $this->course_rating_permission = self::COURSE_RATING_DISABLED;
	}

	public function attributeLabels() {
		return array(
			'course_user_share_permission' => Yii::t('social_rating', 'If the course is completed, user can also share his score'),
			'course_share_facebook' => Yii::t('app', 'facebook'),
			'course_share_twitter' => Yii::t('app', 'twitter'),
			'course_share_linkedin' => Yii::t('app', 'linkedin'),
			'course_share_google' => Yii::t('standard', '_GOOGLE'),
		);
	}

}