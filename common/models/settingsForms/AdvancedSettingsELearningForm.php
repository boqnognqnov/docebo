<?php

class AdvancedSettingsELearningForm extends AdvancedSettingsBaseForm {

	protected $_params;

	public $no_answer_in_poll;
	public $no_answer_in_test;
	public $tracking;
	public $test_force_multichoice;
	public $certificate_real_size;
	public $use_dashlets;
	public $show_course_after_enrollment;
	public $course_resume_autoplay;
    public $soft_deadline;
    public $tincan_urls;

	public function rules() {
		return array(
			array('no_answer_in_poll, no_answer_in_test, tracking, test_force_multichoice, certificate_real_size, show_course_after_enrollment, use_dashlets, course_resume_autoplay, soft_deadline, tincan_urls', 'safe')
		);
	}

	public function init() {
		$fields = array(
			'no_answer_in_poll',
			'no_answer_in_test',
			'test_force_multichoice',
			'certificate_real_size',
			'show_course_after_enrollment',
			'use_dashlets',
			'course_resume_autoplay',
            'soft_deadline',
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting t')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {			
				$this->_params[$row['param_name']] = $row;
				$this->{$row['param_name']} = $row['param_value'];
			}
		}
	}

	public function attributeLabels() {
		return array(
			'no_answer_in_poll' => Yii::t('configuration', '_NO_ANSWER_IN_POLL'),
			'no_answer_in_test' => Yii::t('configuration', '_NO_ANSWER_IN_TEST'),
			'tracking' => Yii::t('configuration', '_TRACKING'),
			'test_force_multichoice' => Yii::t('configuration', '_TEST_FORCE_MULTICHOICE'),
			'certificate_real_size' => Yii::t('certificate', '_CERTIFICATE_REAL_SIZE'),
			'use_dashlets'  => Yii::t('dashboard', '_DASHBOARD'),
			'course_resume_autoplay'=>Yii::t('course', 'Directly play first item (or resume the last played object) when entering a course'),
			'show_course_after_enrollment' => Yii::t('configuration', 'Show Course After Enrollment'),
            'soft_deadline'=>Yii::t('course', 'Allow users to enter an E-Learning course even if the enrollment validity or course end date is expired'),
		    'tincan_urls' => Yii::t('admin', 'XAPI external content URLs'),
		);
	}

	public function canShowUseDashlets() {
		/*$migrationBeforeDeadline = Yii::app()->db->createCommand()
			->select('apply_time')
			->from("tbl_migration")
			->where('apply_time<=:time',array(':time'=>strtotime('2015-05-09 00:00:00')))
			->andWhere("version = 'm150111_160531_core_version_update_to_66'")
			->queryRow();

		return !$migrationBeforeDeadline;*/
		return false;
	}

	/**
	 * Get current list of URLs
	 * 
	 * @return string[]
	 */
	public function getTinCanUrls() {
	    return CoreSettingTincanUrl::model()->getAllUrls();
	}

}