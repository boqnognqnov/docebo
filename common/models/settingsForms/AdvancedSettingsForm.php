<?php

class AdvancedSettingsForm extends AdvancedSettingsBaseForm {

    protected $_params;

    public $ua_mobile;
    public $session_ip_control;
    public $nl_sendpercycle;
    public $nl_sendpause;
    public $stop_concurrent_user;
    public $ttlSession;
    public $sender_event;
    public $send_cc_for_system_emails;
    public $register_deleted_user;
    public $elements_per_page;
    public $scorm_debug;
    public $timezone_default;
    public $timezone_allow_user_override;
    public $date_format;
    public $date_format_custom;
    public $date_format_custom_lang;
    public $return_path;
    public $simplesaml_logging;
    public $extended_tincan_launch_params=false;

    public function rules() {
        return array(
            array('ua_mobile, session_ip_control, nl_sendpercycle, nl_sendpause, stop_concurrent_user, ttlSession, sender_event, send_cc_for_system_emails, register_deleted_user, elements_per_page, scorm_debug, timezone_default, timezone_allow_user_override, date_format, date_format_custom, date_format_custom_lang, return_path, simplesaml_logging, extended_tincan_launch_params', 'safe'),
            array('return_path', 'email'),
            array('sender_event', 'email')
        );
    }

    public function init() {
        $fields = array(
            'ua_mobile',
            'session_ip_control',
            'nl_sendpercycle',
            'nl_sendpause',
            'stop_concurrent_user',
            'ttlSession',
            'sender_event',
            'send_cc_for_system_emails',
            'register_deleted_user',
            'elements_per_page',
            'scorm_debug',
            'timezone_default',
            'timezone_allow_user_override',
            'date_format',
            'date_format_custom',
            'date_format_custom_lang',
            'return_path',
            'simplesaml_logging',
            'extended_tincan_launch_params',
        );

        $command = Yii::app()->db->createCommand()
            ->select('*')
            ->from('core_setting')
            ->where(array('in', 'param_name', $fields));

        $result = $command->queryAll();

        foreach ($result as $row) {
            if (property_exists($this, $row['param_name'])) {
            	$this->_params[$row['param_name']] = $row;
            	$this->{$row['param_name']} = $row['param_value'];
            }
        }

        if (empty($this->date_format_custom_lang)) {
            $this->date_format_custom_lang = Yii::app()->getLanguage();
        }
    }

    public function attributeLabels() {
        return array(
            'ua_mobile'                 => Yii::t('configuration', '_UA_MOBILE'),
            'session_ip_control'        => Yii::t('configuration', '_SESSION_IP_CONTROL'),
            'nl_sendpercycle'           => Yii::t('configuration', '_NL_SENDPERCYCLE'),
            'nl_sendpause'              => Yii::t('configuration', '_NL_SENDPAUSE'),
            'stop_concurrent_user'      => Yii::t('configuration', '_STOP_CONCURRENT_USER'),
            'ttlSession'                => Yii::t('configuration', '_TTLSESSION'),
            'sender_event'              => Yii::t('configuration', '_SENDER_EVENT'),
            'send_cc_for_system_emails' => Yii::t('configuration', '_SEND_CC_FOR_SYSTEM_EMAILS'),
            'register_deleted_user'     => Yii::t('configuration', '_REGISTER_DELETED_USER'),
            'elements_per_page'         => Yii::t('configuration', '_ELEMENTS_PER_PAGE'),
            'scorm_debug'               => Yii::t('configuration', 'Enable debug for Scorm API. Enabling this will print all the Scorm API calls in the browser console.'),
            'timezone_default'          => Yii::t('configuration', 'Time zone'),
            'timezone_allow_user_override' => Yii::t('configuration', 'Let the user choose his own timezone'),
            'date_format'               => Yii::t('standard', '_DATE_FORMAT'),
            'return_path'               => Yii::t('configuration', 'Return path'),
            'simplesaml_logging'		=> Yii::t('configuration', '_DO_DEBUG'),
            'extended_tincan_launch_params' => Yii::t('configuration', 'Add extended list of parameters to XAPI/TinCan launch URL'),
        );
    }
}