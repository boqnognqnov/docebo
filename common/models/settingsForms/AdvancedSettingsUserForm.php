<?php

class AdvancedSettingsUserForm extends AdvancedSettingsBaseForm {

	private $type = 'register';

	protected $_params;

	public $profile_only_pwd;
	public $request_mandatory_fields_compilation;
	public $use_node_fields_visibility;
	public $max_log_attempt;
	public $save_log_attempt;
	public $user_logout_redirect;
	public $user_logout_redirect_url;
    public $show_first_name_first;
	public $enable_email_verification;

	public function rules() {
		return array(
			array('user_logout_redirect_url, user_logout_redirect, profile_only_pwd, request_mandatory_fields_compilation, use_node_fields_visibility, max_log_attempt, save_log_attempt, show_first_name_first, enable_email_verification', 'safe')
		);
	}


	public function init() {
		$fields = array(
			'profile_only_pwd',
			'request_mandatory_fields_compilation',
			'use_node_fields_visibility',
			'max_log_attempt',
			'save_log_attempt',
			'user_logout_redirect',
			'user_logout_redirect_url',
            'show_first_name_first',
			'enable_email_verification'
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {
				$this->_params[$row['param_name']] = $row;
				$this->{$row['param_name']} = $row['param_value'];
			}
		}
	}

	public function attributeLabels() {
		return array(
			'profile_only_pwd' => Yii::t('configuration', '_PROFILE_ONLY_PWD'),
			'request_mandatory_fields_compilation' => Yii::t('configuration', '_REQUEST_MANDATORY_FIELDS_COMPILATION'),
			'use_node_fields_visibility' => Yii::t('configuration', 'Apply additional fields visibility based on Branch nodes'),
			'max_log_attempt' => Yii::t('configuration', '_MAX_LOG_ATTEMPT'),
			'save_log_attempt' => Yii::t('configuration', '_SAVE_LOG_ATTEMPT'),
			'user_logout_redirect' => Yii::t('standard', '_ACTIVATE'),
			'user_logout_redirect_url' => Yii::t('standard', '_URL'),
            'show_first_name_first' => Yii::t('standard', 'Show fullname as "Firstname Lastname"'),
			'enable_email_verification' => Yii::t('configuration', 'Send verification email at first login'),
		);
	}

}