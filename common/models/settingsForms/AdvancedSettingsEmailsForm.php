<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 24.12.2015
 */
class AdvancedSettingsEmailsForm extends AdvancedSettingsBaseForm {

	public $billingEmails;
	public $productEmails;

	public function rules () {
		return array(
			array( 'billingEmails, productEmails', 'safe' ),
			array( 'billingEmails, productEmails', 'required' ),
		);
	}

	public function save () {
		$this->beforeSave();

		// Actually save the emails to the ERP
		$billingEmails = array();
		$productEmails = array();
		if( $this->billingEmails ) {
			if( is_string( $this->billingEmails ) ) {
				$billingEmails = explode( ',', $this->billingEmails );
			} elseif( is_array( $this->billingEmails ) ) {
				$billingEmails = $this->billingEmails;
			}
		}
		if( $this->productEmails ) {
			if( is_string( $this->productEmails ) ) {
				$productEmails = explode( ',', $this->productEmails );
			} elseif( is_array( $this->productEmails ) ) {
				$productEmails = $this->productEmails;
			}
		}

		if( ! empty( $billingEmails ) || ! empty( $productEmails ) ) {

			$erpApiClient = new ErpApiClient2();
			$save         = $erpApiClient->saveInstallationEmails( $billingEmails, $productEmails );
			if( ! $save ) {
				Yii::log( 'ERP communication failed', CLogger::LEVEL_ERROR );
				Yii::app()->user->setFlash( 'error', 'ERP communication failed' );

				return FALSE;
			}

		}

		parent::save();
	}

}
