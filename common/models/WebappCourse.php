<?php

class WebappCourse extends LearningCourse
{
	/**
	 * This method serve learning object for MOBILE
	 *
	 * @param $idCourse
	 * @return mixed
	 */
	public static function getCourseInfo($idCourse){
		$response = array(); // final response
		$courseMetadata = self::getCourseMetadata($idCourse);
		$courseMetadata['description'] = strip_tags($courseMetadata['description']);

		$response['course_metadata'] = $courseMetadata;
		$response['course_user_status'] = $courseMetadata['status'];

		return $response;
	}
	/**
	 * Query the DB to get info about the Course and User completion status for this course
	 * @param $id - of the Course
	 */
	public static function getCourseMetadata($id){
		$command = Yii::app()->db->createCommand()
			->select('lc.idCourse, name, description, lang_code, lcu.status, mediumTime')
			->from(LearningCourse::model()->tableName() . ' lc')
			->leftJoin(LearningCourseuser::model()->tableName().' lcu', 'lcu.idUser = :idUser AND lcu.idCourse = lc.idCourse', array(':idUser'  => Yii::app()->user->id))
			->where('lc.idCourse = :idCourse', array(':idCourse' => $id));

		return $command->queryRow();
	}
}