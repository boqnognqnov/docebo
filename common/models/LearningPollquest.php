<?php

/**
 * This is the model class for table "learning_pollquest".
 *
 * The followings are the available columns in table 'learning_pollquest':
 * @property integer $id_quest
 * @property integer $id_poll
 * @property integer $id_category
 * @property string $type_quest
 * @property string $title_quest
 * @property integer $sequence
 * @property integer $page
 * @property string $settings
 */
class LearningPollquest extends CActiveRecord
{
	const POLL_TYPE_CHOICE = 'choice';
	const POLL_TYPE_CHOICE_MULTIPLE = 'choice_multiple';
	const POLL_TYPE_TEACHER_EVAL = 'doc_valutation';
	const POLL_TYPE_COURSE_EVAL = 'course_valutation';
	const POLL_TYPE_TEXT = 'extended_text';
	const POLL_TYPE_LIKERT_SCALE = 'likert_scale';
	const POLL_TYPE_TITLE = 'title';
	const POLL_TYPE_BREAK_PAGE = 'break_page';
	const POLL_TYPE_EVALUATION = 'evaluation';
	const POLL_TYPE_INLINE_CHOICE = 'inline_choice';

	public static function getPollTypesList() {
		return array(
			self::POLL_TYPE_BREAK_PAGE,
			self::POLL_TYPE_CHOICE,
			self::POLL_TYPE_CHOICE_MULTIPLE,
			self::POLL_TYPE_INLINE_CHOICE,
			//self::POLL_TYPE_COURSE_EVAL,
			self::POLL_TYPE_TEXT,
			//self::POLL_TYPE_TEACHER_EVAL,
			self::POLL_TYPE_LIKERT_SCALE,
			self::POLL_TYPE_TITLE,
			self::POLL_TYPE_EVALUATION,
		);
	}

	public static function getPollTypesTranslationsList() {
		$list = array();
		$list[self::POLL_TYPE_CHOICE] = Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_CHOICE));
		$list[self::POLL_TYPE_CHOICE_MULTIPLE] = Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_CHOICE_MULTIPLE));
		$list[self::POLL_TYPE_INLINE_CHOICE] = Yii::t('test', '_QUEST_'.strtoupper(self::POLL_TYPE_INLINE_CHOICE));
		//self::POLL_TYPE_TEACHER_EVAL => Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_TEACHER_EVAL)),
		//self::POLL_TYPE_COURSE_EVAL => Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_COURSE_EVAL)),
		$list[self::POLL_TYPE_TEXT] = Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_TEXT));

		// this feature is currently manually enabled (see the next line)
		// Settings::save('poll_type_evaluation_enabled', 'on');
		$courseEvaluationEnabled = ('on' === Settings::get('poll_type_evaluation_enabled'));
		if ($courseEvaluationEnabled) {
			$list[self::POLL_TYPE_EVALUATION] = Yii::t('classroom', 'Evaluation');
		}

		$list[self::POLL_TYPE_LIKERT_SCALE] = Yii::t('poll', 'Likert Scale');

		$list[self::POLL_TYPE_TITLE] = Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_TITLE));
		$list[self::POLL_TYPE_BREAK_PAGE] = Yii::t('poll', '_QUEST_'.strtoupper(self::POLL_TYPE_BREAK_PAGE));

		return $list;
	}


	public static function getPollTypesAcronymsList() {
		return array(
			self::POLL_TYPE_CHOICE => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_CHOICE)),
			self::POLL_TYPE_CHOICE_MULTIPLE => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_CHOICE_MULTIPLE)),
			self::POLL_TYPE_INLINE_CHOICE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_INLINE_CHOICE)),
			//self::POLL_TYPE_TEACHER_EVAL => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_TEACHER_EVAL)),
			//self::POLL_TYPE_COURSE_EVAL => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_COURSE_EVAL)),
			self::POLL_TYPE_TEXT => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_TEXT)),
			self::POLL_TYPE_LIKERT_SCALE => Yii::t('poll', 'Define likert scale'),
			self::POLL_TYPE_TITLE => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_TITLE)),
			self::POLL_TYPE_BREAK_PAGE => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_BREAK_PAGE)),
			self::POLL_TYPE_EVALUATION => Yii::t('poll', '_QUEST_ACRN_'.strtoupper(self::POLL_TYPE_EVALUATION)),
		);
	}


	public static function isValidType($type) {
		return in_array($type, self::getPollTypesList());
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningPollquest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_pollquest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_quest', 'required'),
			array('id_poll, id_category, sequence, page', 'numerical', 'integerOnly'=>true),
			array('type_quest', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_quest, id_poll, id_category, type_quest, title_quest, sequence, page, settings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_quest' => 'Id Quest',
			'id_poll' => 'Id Poll',
			'id_category' => 'Id Category',
			'type_quest' => 'Type Quest',
			'title_quest' => 'Title Quest',
			'sequence' => 'Sequence',
			'page' => 'Page',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_quest',$this->id_quest);
		$criteria->compare('id_poll',$this->id_poll);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('type_quest',$this->type_quest,true);
		$criteria->compare('title_quest',$this->title_quest,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('page',$this->page);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function afterFind() {
		parent::afterFind();

		$this->settings = $this->unserializeSettings();
	}

	/**
	 * @return bool
	 */
	public function beforeSave() {
		if (parent::beforeSave()) {

			$this->settings = $this->serializeSettings();

			return true;
		}
		return false;
	}

	public function unserializeSettings() {
		return (!empty($this->settings)) ? unserialize(base64_decode($this->settings)) : array();
	}

	public function serializeSettings() {
		return base64_encode(serialize($this->settings));
	}

	/**
	 * Normalize sequence numbers for all questions of a specified poll
	 * @param int $idPoll
	 */
	public static function fixPollSequence($idPoll) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC, title_quest ASC';
		$questions = self::model()->findAllByAttributes(array('id_poll' => $idPoll), $criteria);
		if (!empty($questions)) {
			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
			try {
				$index = 0;
				foreach ($questions as $question) {
					$index++;
					if ($question->sequence != $index) {
						$question->sequence = $index;
						$rs = $question->save();
						if (!$rs) {
							throw new CException('Error while fixing sequence');
						}
					}
				}
				if (isset($transaction)) { $transaction->commit(); }
			} catch (CException $e) {
				if (!isset($transaction)) { $transaction->rollback(); }
				return false;
			}
		}
		return true;
	}


	/**
	 * Normalize page numbers for all questions of a specified poll
	 * @param int $idPoll
	 */
	public static function fixPollPages($idPoll) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC, title_quest ASC';
		$questions = self::model()->findAllByAttributes(array('id_poll' => $idPoll), $criteria);
		if (!empty($questions)) {
			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
			try {
				$page = 1;
				foreach ($questions as $question) {
					if ($question->page != $page) {
						$question->page = $page;
						if (!$question->save()) {
							throw new CException('Error while fixing questions page');
						}
					}
					if ($question->type_quest == self::POLL_TYPE_BREAK_PAGE) {
						$page++;
					}
				}
				if (isset($transaction)) { $transaction->commit(); }
			} catch (CException $e) {
				if (!isset($transaction)) { $transaction->rollback(); }
				return false;
			}
		}
		return true;
	}



	/**
	 * Find the highest sequence number in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getPollMaxSequence($idPoll) {
		$result = Yii::app()->db->createCommand()
			->select("MAX(sequence) AS max_sequence")
			->from(self::model()->tableName())
			->where("id_poll = :id_poll", array(':id_poll' => $idPoll))
			->queryRow();
		$output = (int)$result['max_sequence'];
		return $output;
	}



	/**
	 * Find the sequence number for a new question in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getPollNextSequence($idPoll) {
		return self::getPollMaxSequence($idPoll) + 1;
	}



	/**
	 * Find the page number for a new question in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getPollPageNumber($idPoll) {
		$db = Yii::app()->db;
		$table = self::model()->tableName();

		$result = $db->createCommand()
			->select("MAX(sequence) AS max_sequence, MAX(page) AS page")
			->from($table)
			->where("id_poll = :id_poll", array(':id_poll' => $idPoll))
			->queryRow();
		$sequence = $result['max_sequence'];
		$page = $result['page'];
		if (!$page) { return 1; } //no questions yet, return first page number

		$result = $db->createCommand()
			->select("type_quest")
			->from($table)
			->where("id_poll = :id_poll", array(':id_poll' => $idPoll))
			->andWhere("sequence = :sequence", array(':sequence' => $sequence))
			->queryRow();
		$questionType = $result['type_quest'];
		if ($questionType == self::POLL_TYPE_BREAK_PAGE) {
			return ($page + 1);
		}

		return $page;
	}

}