<?php

/**
 * This is the model class for table "core_lang_translation".
 *
 * The followings are the available columns in table 'core_lang_translation':
 * @property integer $id_text
 * @property string $lang_code
 * @property string $translation_text
 * @property string $translation_status
 * @property string $save_date
 * @property string $pid
 *
 * The followings are the available model relations:
 * @property CoreLangText $text
 */
class CoreLangTranslation extends CActiveRecord
{
    const STATUS_NEW = 'new';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreLangTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_lang_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_text', 'numerical', 'integerOnly'=>true),
			array('lang_code, translation_status', 'length', 'max'=>50),
			array('pid', 'length', 'max'=>20),
			array('translation_text', 'required', 'on' => 'update'),
			array('translation_text, save_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_text, lang_code, translation_text, translation_status, save_date, pid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'text' => array(self::HAS_ONE, 'CoreLangText', 'id_text'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('save_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array the scope definition.
	 */
	public function scopes() {
		return array(
			'login' => array(
				'condition' => 'text.text_module = "login"',
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_text' => 'Id Text',
			'lang_code' => 'Lang Code',
			'translation_text' => 'Translation Text',
			'translation_status' => 'Translation Status',
			'save_date' => 'Save Date',
			'pid' => 'Pid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_text',$this->id_text);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation_text',$this->translation_text,true);
		$criteria->compare('translation_status',$this->translation_status,true);
		$criteria->compare('save_date',Yii::app()->localtime->fromLocalDateTime($this->save_date),true);
		$criteria->compare('pid',$this->pid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	protected static function convertSmartQuotes($string) {

		//prepare output variable
		$output = $string;

		//do some substitutions
		$search = array(                 // www.fileformat.info/info/unicode/<NUM>/ <NUM> = 2018
			"\xC2\xAB",     // « (U+00AB) in UTF-8
			"\xC2\xBB",     // » (U+00BB) in UTF-8
			"\xE2\x80\x98", // ‘ (U+2018) in UTF-8
			"\xE2\x80\x99", // ’ (U+2019) in UTF-8
			"\xE2\x80\x9A", // ‚ (U+201A) in UTF-8
			"\xE2\x80\x9B", // ‛ (U+201B) in UTF-8
			"\xE2\x80\x9C", // “ (U+201C) in UTF-8
			"\xE2\x80\x9D", // ” (U+201D) in UTF-8
			"\xE2\x80\x9E", // „ (U+201E) in UTF-8
			"\xE2\x80\x9F", // ‟ (U+201F) in UTF-8
			"\xE2\x80\xB9", // ‹ (U+2039) in UTF-8
			"\xE2\x80\xBA", // › (U+203A) in UTF-8
			"\xE2\x80\x93", // – (U+2013) in UTF-8
			"\xE2\x80\x94", // — (U+2014) in UTF-8
			"\xE2\x80\xA6"  // … (U+2026) in UTF-8
		);

		$replace = array(
			"<<",
			">>",
			"'",
			"'",
			"'",
			"'",
			'"',
			'"',
			'"',
			'"',
			"<",
			">",
			"-",
			"-",
			"..."
		);

		$output = str_replace($search, $replace, $output);

		return $output;
	}


	/**
	 * Do some formatting and validation for translation texts to be inserted in DB
	 * @param $text the text string to be prepared to be inserted into DB
	 */
	public static function prepareTranslationText($text) {

		//make sure that there aren't weird quoting characters in the passed text that may break tags or be uncorrectly displayed
		//(usually copied and pasted from softwares like MS Word or similar)
		$text = self::convertSmartQuotes($text);

		//do some html purification
		$text = Yii::app()->htmlpurifier->purify($text, 'standard');

		//sometimes link tags in <a> may be unintentionally url-encoded by text editors
		$text = str_replace(
			array('href="'.urlencode('[link]').'"', 'href=\''.urlencode('[link]').'\''),
			'href="[link]"',
			$text
		);

		$matches = array();
		preg_match('/h{1}r{1}e{1}f{1}={1}"{1}%{1}7{1}B{1}([a-z-_A-Z]*)%{1}7{1}D{1}"/', $text, $matches);
		if(!empty($matches))
			foreach($matches as $match)
				$text = str_replace($match, urldecode($match), $text);

		return $text;
	}


	public function beforeSave() {
		//$this->translation_status = self::STATUS_NEW;
		$this->save_date = Yii::app()->localtime->getUTCNow();
		$this->translation_text = self::prepareTranslationText($this->translation_text);
		return parent::beforeSave();
	}

    /**
     * {@inheritdoc}
     */
    public function afterSave() {
        $this->clearCache();

        parent::afterSave();
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete() {
        $this->clearCache();

        parent::afterDelete();
    }

    /**
     * Clear cache of translations
     *
     * This method clear JsTrans caches in all instance of Yii
     *
     * @author Valentin Hristov
     */
    public function clearCache() {
    	// Temporary disable this function from real functioning until we find better way to clear JsTrans cache (17 Dec 2013, plamen)
    	return;

        // get path to JsTrans
        $reflector = new ReflectionClass("JsTrans");
        $fn = $reflector->getFileName();

        // get path of assets of this extension (/extensions/JsTrans/assets)
        $assets = realpath( dirname($fn) . '/assets' );
        $admin_assets = realpath(Yii::getPathOfAlias('admin').'/assets');
        $lms_assets = realpath(Yii::getPathOfAlias('lms').'/assets');

        $dictionaries = CFileHelper::findFiles($assets, array(
            'exclude' => array('JsTrans.js', 'JsTrans.min.js'),
        ));
        $dictionaries = array_merge($dictionaries, CFileHelper::findFiles($admin_assets));
        $dictionaries = array_merge($dictionaries, CFileHelper::findFiles($lms_assets));

        // remove dictionary files
        foreach ($dictionaries as $dictionary) {
            $pattern = '/^dictionary\-([a-zA-Z0-9]+)\.js$/';

            if (preg_match($pattern, basename($dictionary), $matches) && is_writable($dictionary)) {
                unlink ($dictionary);
            }
        }
    }

}