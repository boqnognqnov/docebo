<?php

/**
 * Description of App7020AssetSummary
 *
 * @author vbelev
 */
class App7020AssetSummary extends CModel {
	
	/**
	 * Timeframe for query to get "Coaching Activity". The value is number of days ago from now
	 * @var array 
	 */
	const TIMEFRAME_DAILY = 1;
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;
	
	public function attributeNames() {
		return;
	}


	/**
	 * Data feeder for .../app7020/index.php?r=assetsummary/index - tab "Overview"
	 * @param int $idContent
	 * @param int $timeInterval
	 * @return array
	 */
	public static function viewsStat($idContent, $timeInterval=7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020ContentHistory::model()->tableName());
		$commandBase->andWhere("idContent = :idContent");
		$commandBase->andWhere("viewed BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(viewed, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(viewed, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(viewed, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(viewed)');
		}
		$commandBase->order('viewed');
		$tempResults = $commandBase->queryAll(true, array(':idContent'=>$idContent, ':timeInterval'=>$timeInterval ));
		
				
		return array(
			'totalViews' => App7020Helpers::arrSumByKey($tempResults, 'count'),
			'assetRating' => App7020ContentRating::calculateContentRating($idContent, 1),
			'chartData' => App7020Helpers::getChartData($tempResults, $timeInterval),
			);
	}
	
	/**
	 * Data feeder for .../app7020/index.php?r=assetsummary/index - tab "Social"
	 * @param int $idContent
	 * @param int $timeInterval
	 * @return array
	 */
	public static function socialStat($idContent, $timeInterval=7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Invitations::model()->tableName());
		$commandBase->andWhere("idContent = :idContent");
		$commandBase->andWhere("created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(created)');
		}
		$commandBase->order('created');
		$tempResults = $commandBase->queryAll(true, array(':idContent'=>$idContent, ':timeInterval'=>$timeInterval ));
		
		return array(
			'totalInvitations' => App7020Helpers::arrSumByKey($tempResults, 'count'),
			'chartData' => App7020Helpers::getChartData($tempResults, $timeInterval),
			);
		
	}
	
	/**
	 * Data feeder for .../app7020/index.php?r=assetsummary/index - tab "Ask the Expert"
	 * @param int $idContent
	 * @param int $timeInterval
	 * @return array
	 */
	public static function askExpertStat($idContent, $timeInterval=7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}
		
		// Questions
		$questionCommand = Yii::app()->db->createCommand();
		$questionCommand->from(App7020Assets::model()->tableName() . ' c');
		$questionCommand->join(App7020Question::model()->tableName() . ' q', 'c.id = q.idContent');
		$questionCommand->andWhere("idContent = :idContent");
		$questionCommand->andWhere("q.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$questionCommand->select("DATE_FORMAT(q.created, '%Y-%m') AS period, COUNT(*) AS count");
			$questionCommand->group("DATE_FORMAT(q.created, '%m')");
		} else {
			$questionCommand->select("DATE_FORMAT(q.created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$questionCommand->group('DATE(q.created)');
		}
		$questionCommand->order('q.created');
		$questionResults = $questionCommand->queryAll(true, array(':idContent'=>$idContent, ':timeInterval'=>$timeInterval ));
		
		//Answers
		$answerCommand = Yii::app()->db->createCommand();
		$answerCommand->from(App7020Assets::model()->tableName() . ' c');
		$answerCommand->join(App7020Question::model()->tableName() . ' q', 'c.id = q.idContent');
		$answerCommand->join(App7020Answer::model()->tableName() . ' a', 'a.idQuestion = q.id');
		$answerCommand->leftJoin(App7020AnswerLike::model()->tableName() . ' al', 'al.idAnswer = a.id');
		$answerCommand->andWhere("idContent = :idContent");
		$answerCommand->andWhere("a.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$answerCommand->select("
				DATE_FORMAT(a.created, '%Y-%m') AS period,
				COUNT(*) AS count,
				SUM(CASE WHEN bestAnswer = :bestAnswer THEN 1 ELSE 0 END) AS countBestAnswers,
				SUM(CASE WHEN al.type = :like THEN 1 ELSE 0 END) AS  countLikes,
				SUM(CASE WHEN al.type = :dislike THEN 1 ELSE 0 END) AS countDislikes
			");
			$answerCommand->group("DATE_FORMAT(a.created, '%m')");
		} else {
			$answerCommand->select("
				DATE_FORMAT(a.created, '%Y-%m-%d') AS period,
				COUNT(*) AS count,
				SUM(CASE WHEN bestAnswer = :bestAnswer THEN 1 ELSE 0 END) AS countBestAnswers,
				SUM(CASE WHEN al.type = :like THEN 1 ELSE 0 END) AS  countLikes,
				SUM(CASE WHEN al.type = :dislike THEN 1 ELSE 0 END) AS countDislikes
			");
			$answerCommand->group('DATE(a.created)');
		}
		$answerCommand->order('a.created');
		$answerResults = $answerCommand->queryAll(true, array(
			':idContent' => $idContent,
			':timeInterval' => $timeInterval,
			':bestAnswer' => App7020Answer::BEST_ANSWER,
			':like' => App7020AnswerLike::TYPE_LIKE,
			':dislike' => App7020AnswerLike::TYPE_DISLIKE,
				));
		
		
		return array(
			'totalQuestions' => App7020Helpers::arrSumByKey($questionResults, 'count'),
			'questionsChartData' => App7020Helpers::getChartData($questionResults, $timeInterval),
			'totalAnswers' => App7020Helpers::arrSumByKey($answerResults, 'count'),
			'bestAnswers' => App7020Helpers::arrSumByKey($answerResults, 'countBestAnswers'),
			'likes' => App7020Helpers::arrSumByKey($answerResults, 'countLikes'),
			'dislikes' => App7020Helpers::arrSumByKey($answerResults, 'countDislikes'),
			'answersChartData' => App7020Helpers::getChartData($answerResults, $timeInterval),
		);
	}
	

}
