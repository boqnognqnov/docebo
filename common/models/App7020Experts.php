<?php

/**
 * This is the model class for table "app7020_experts".
 *
 * The followings are the available columns in table 'app7020_experts':
 * @property integer $id
 * @property string $idUser
 * @property string $idAdmin
 * @property string $created
 *
 * The followings are the available model relations:
 * @property CoreUser $expert
 * @property CoreUser $admin
 */
class App7020Experts extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_experts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idAdmin', 'required'),
			array('idUser, idAdmin', 'numerical', 'integerOnly' => true),
		);
	}
	
	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'expert' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
		);
	}

	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			
		);
	}

	public static function model($className = __CLASS__) {
        return parent::model($className);
    }
	

	public function beforeSave() {
		
		if(!$this->id){
			//$this->created = Yii::app()->localtime->getUTCNow();
		}
		
		if(!$this->idAdmin){
			$this->idAdmin = Yii::app()->user->idst;
		}
		
		return true;
	}
	
	public function beforeDelete() {
		
		App7020ChannelExperts::model()->deleteAllByAttributes(array('idExpert' => $this->idUser));
		return true;
	}
	
	/**
	 * Get Coach7020 experts
	 * @param type $customConfig
	 */
	public static function sqlDataProvider($customConfig = array()){
		
		if($customConfig['search_input']){
			$searchInput = $customConfig['search_input'];
		} else {
			$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		}
		
		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;
		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("e.idUser AS expertId, CONCAT(cu.firstname, ' ', cu.lastname) AS expertNames, e.id as idExpert"
			. ", cu.email AS email, "
			. "TRIM(LEADING '/' FROM cu.userid) as username, "
			. "(SELECT COUNT(id) FROM ".  App7020ChannelExperts::model()->tableName()." WHERE idExpert = e.id) AS channelsCount");
		$dbCommand->from(App7020Experts::model()->tableName()." e");
		$dbCommand->join(CoreUser::model()->tableName()." cu", "e.idUser = cu.idst");
		
		if ($searchInput) {
			$dbCommand->andWhere("CONCAT(cu.firstname, ' ', cu.lastname, ' ', cu.userid) LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}
		
		if ($setPagination) {
			$pageSize = 8; // Default
			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}

		$config = array(
			'totalItemCount' => count($dbCommand->queryAll()),
			'pagination' => $pagination,
			'keyField' => 'expertId'
		);
		$dataProvider = new CSqlDataProvider($dbCommand, $config);
		return $dataProvider;
		
	}
	
	public static function renderTopicsLink($expertId, $topics){
		$classOrange = '';
		if(!$topics){
			$classOrange .= ' orange';
		}
		$content = Chtml::link('<span class="count">'.$topics.'</span><span class="fa fa-book"></span>', Docebo::createApp7020Url('experts/topics', array('expertId' => $expertId)), array(
            'id' => 'topics-expert-'.$expertId,
            'class' => 'app7020_expert_topics'.$classOrange,
            'data-expert-id' => $expertId,
        ));
		return $content;
		
	}
	
	
	public static function renderChannelsLink($expertId, $channels){
		$classOrange = '';
		if(!$channels){
			$classOrange .= ' orange';
		}
		$content = Chtml::link('<span class="count'.$classOrange.'">'.$channels.'</span><span class="fa fa-tv"></span>', Docebo::createApp7020Url('expertsChannels/channels', array('expertId' => $expertId)), array(
            'id' => 'topics-expert-'.$expertId,
            'class' => 'app7020_expert_topics',
            'data-expert-id' => $expertId,
        ));
		return $content;
		
	}
	
	public static function renderDeleteLink($expertId){
		
		$content = Chtml::link(' <span class="delete"></span>', 'javascript:void(0);', array(
            'id' => 'delete-expert-'.$expertId,
            'class' => 'app7020_expert_channles_delete',
            'data-expert-id' => $expertId,
        ));
		return $content;
		
	}
	
	public static function getAllUsersExceptExperts(){
		$expertsProvider = self::sqlDataProvider();
		$experts = $expertsProvider->data;
		$expertsIds = array();
		foreach ($experts as $value) {
			$expertsIds[] = $value['expertId'];
		}
		
		if($customConfig['search_input']){
			$searchInput = $customConfig['search_input'];
		} else {
			$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		}
		
		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;
		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("idst, firstname, lastname, email, "
			. "TRIM(LEADING '/' FROM userid) as username");
		$dbCommand->from(CoreUser::model()->tableName());
		if($expertsIds){
			$dbCommand->where("idst NOT IN (".  implode(",", $expertsIds).")");
		}
		
		
		if ($searchInput) {
			$dbCommand->andWhere("CONCAT(firstname, ' ', lastname) LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}
		
		if ($setPagination) {
			$pageSize = 2; // Default
			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}

		$config = array(
			'totalItemCount' => count($dbCommand->queryAll()),
			'pagination' => $pagination,
			'keyField' => 'idst'
		);
		$dataProvider = new CSqlDataProvider($dbCommand, $config);
		return $dataProvider;
	}
	
	public static function getExpertsIds(){
		$expertsModels = self::model()->findAll();
		$returnArray = array();
		foreach ($expertsModels as $value) {
			$returnArray[] = $value->idUser;
		}
		return $returnArray;
	}

	public static function isUserExpert($id){
		$eu = self::model()->findByAttributes(array('idUser' => $id));
		if(!empty($eu->id)){
			return true;
		} else{
			return false;
		}
	}
		
}
