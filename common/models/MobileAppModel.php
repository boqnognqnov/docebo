<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MobileAppModel{
	const PLUGIN_APP_NAME = 'WebApp';
	const MAX_WIDTH = 200;
	const MAX_HEIGHT = 100;
	/**
	 * 
	 * @param String $logo The name of the file in tmp folder
	 * @param String $oldLogo The name of the old file stored in server
	 */
	public static function moveLogoToS3($logo = '', $oldLogo = ''){
		// move image to it's final storage location => /files/doceboCore/photo/_filename_
		$filename = $logo;
		$uploadTmpFile = Docebo::getUploadTmpPath() . DS . $filename;
		if (file_exists($uploadTmpFile) && is_file($uploadTmpFile)) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$storageManager->storeAs($uploadTmpFile, $filename);
			FileHelper::removeFile($uploadTmpFile);
			// First, remove old file
			if (!empty($oldLogo)) {
				$storageManager->remove($oldLogo);
			}
		}
	}
	
	public static function validateImage($fileName){
		$fileHelper = new CFileHelper();
		$message = false;
		$extension = strtolower($fileHelper->getExtension($fileName ));
		$uploadTmpFile = Docebo::getUploadTmpPath() . DS . $fileName;
		if(file_exists($uploadTmpFile)){
			$size = getimagesize($uploadTmpFile);
			$width = $size[0];
			$height = $size[1];
		}
		if(!isset($size)){
			$message = Yii::t('mobile', 'Invalid file');
		}
		if ($message == false && !in_array($extension, FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_IMAGE))){
			$message = Yii::t('mobile', 'Invalid file extension');
		}
		if($message == false && $width > self::MAX_WIDTH){
			$message = Yii::t('mobile', 'Your logo width must be less than or equal to '.self::MAX_WIDTH);
		}
		if($message == false && $height > self::MAX_HEIGHT){
			$message = Yii::t('mobile', 'Your logo height must be less than or equal to '.self::MAX_HEIGHT);
		}
		if($message != false){
			FileHelper::removeFile($uploadTmpFile);
			return $message;
		}
		return true;
	}

	/**
	 * Generate an MD5 hash string from the contents of a directory.
	 *
	 * @param $directory - points to the web app folder
	 * @return boolean|string
	 */
	public static function validateDirectory($directory){

		if ( !is_dir($directory) ){
			return false;
		}

		$files  = array();
		$dir    = dir($directory);
		$pattern= "/\b(node_modules|sass-cache)\b/i"; // escaping these directories

		while (false !== ($file = $dir->read()))
		{
			if ($file != '.' && $file != '..' && !preg_match($pattern, $file) )
			{
				if ( is_dir($directory . '/' . $file) ){
					$files[] = self::validateDirectory($directory . '/' . $file);
					Log::_($directory . '/' . $file);
					Log::_($file);

				} else {
					$files[] = md5_file($directory . '/' . $file);
					Log::_("FILES ELSE:::::::ElSE");
					Log::_($directory . '/' . $file);
				}
			}
		}
		// Close the directory
		$dir->close();
		$response = md5(implode('', $files));


		return $response;
	}

}