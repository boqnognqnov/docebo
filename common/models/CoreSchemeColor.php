<?php

/**
 * This is the model class for table "core_scheme_color".
 *
 * The followings are the available columns in table 'core_scheme_color':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $color
 */
class CoreSchemeColor extends CActiveRecord
{
    const CHART_BACKGROUND_COLOR = '#E4E6E5';
    const CHART_SINGLE_GREEN_COLOR = '#53AB54';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreSchemeColor the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_scheme_color';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, description, color', 'required'),
            array('title', 'length', 'max'=>100),
            array('color', 'length', 'max'=>7),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, description, color', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'color' => 'Color',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('color',$this->color,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }
}