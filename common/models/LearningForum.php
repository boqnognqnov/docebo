<?php

/**
 * This is the model class for table "learning_forum".
 *
 * The followings are the available columns in table 'learning_forum':
 * @property integer $idForum
 * @property integer $idCourse
 * @property string $title
 * @property string $description
 * @property integer $num_thread
 * @property integer $num_post
 * @property integer $last_post
 * @property integer $locked
 * @property integer $sequence
 * @property string $emoticons
 *
 * The followings are the available model relations:
 * @property LearningForumthread[] $threads
 * */
class LearningForum extends CActiveRecord
{

	/**
	 * Counted number of replies across all forum threads
	 * @var number
	 */
	public static $num_replies_counted = 0;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningForum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_forum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description, title', 'required'),
			array('idCourse, num_thread, num_post, last_post, locked, sequence', 'numerical', 'integerOnly'=>true),
			array('title, emoticons', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idForum, idCourse, title, description, num_thread, num_post, last_post, locked, sequence, emoticons', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'threads' => array(self::HAS_MANY, 'LearningForumthread', 'idForum'),
			'messages' => array(self::HAS_MANY, 'LearningForummessage', array('idThread' => 'idThread'), 'through' => 'LearningForumthread'),
			'numThreads' => array(self::STAT, 'LearningForumthread', 'idForum'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', array('idCourse'=>'idCourse')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idForum' => 'Id Forum',
			'idCourse' => 'Id Course',
			'title' => 'Title',
			'description' => 'Description',
			'num_thread' => 'Num Thread',
			'num_post' => 'Num Post',
			'last_post' => 'Last Post',
			'locked' => 'Locked',
			'sequence' => 'Sequence',
			'emoticons' => 'Emoticons',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idForum',$this->idForum);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('num_thread',$this->num_thread);
		$criteria->compare('num_post',$this->num_post);
		$criteria->compare('last_post',$this->last_post);
		$criteria->compare('locked',$this->locked);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('emoticons',$this->emoticons,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	/**
	 * @return CActiveDataProvider
	 */
	public function getForumListDataProvider() {
		$criteria = new CDbCriteria();

		$dataProvider = new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));

		return $dataProvider;
	}


	/**
	 * Check if user is subscribed to the course that is tied to a given forum.
	 *
	 * @param number $user_id
	 * @param number $forum_id
	 */
	public static function userCanAccessForum($user_id, $forum_id) {
		$forumModel = self::model()->findByPk($forum_id);
		$isSubscribed = LearningCourseuser::isSubscribed($user_id, $forumModel->idCourse);
		return Yii::app()->user->isGodAdmin || $isSubscribed;
	}


	/**
	 * Check if user can admin the course that is tied to a given forum, hence, he/she can admin the forum as well
	 *
	 * @param number $user_id
	 * @param number $forum_id
	 */
	public static function userCanAdminForum($user_id, $forum_id) {
		$forumModel = self::model()->findByPk($forum_id);
		$userCanAdminCourse = LearningCourseuser::userLevel($user_id, $forumModel->idCourse) > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
		return  Yii::app()->user->isGodAdmin || $userCanAdminCourse;
	}


	/**
	 * Get list of course related forums
	 * @param unknown $course_id
	 * @return CActiveDataProvider
	 */
	public function getCourseForumsDataProvider($course_id) {

		$criteria = $this->getForumsCriteriaByCourse($course_id);
		$dataProvider = new CActiveDataProvider($this, array(
				'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Settings::get('elements_per_page', 10)
                )
		));

		return $dataProvider;
	}

	/**
	 * @param $course_id
	 * @return CDbCriteria
	 */
	public function getForumsCriteriaByCourse($course_id)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('idCourse=:course_id');
		$criteria->params[':course_id'] = $course_id;

		return $criteria;
	}

	/**
	 * Get available forums for that course
	 * @param $course_id
	 * @return array
	 */
	public function getForumsByCourse($course_id)
	{
		$criteria = $this->getForumsCriteriaByCourse($course_id);
		return self::model()->findAll($criteria);
	}

	/**
	 * Get ALL LMS forums
	 *
	 * @return CActiveDataProvider
	 */
	public function getAllForumsDataProvider()
	{
		$dataProvider = new CActiveDataProvider($this, array(
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
		return $dataProvider;
	}



	public function beforeSave() {

		$this->description = Yii::app()->htmlpurifier->purify($this->description);

		return parent::beforeSave();
	}

	public function getNumOfReplies()
	{
		$sql = 'SELECT (COUNT(DISTINCT idMessage) - (SELECT COUNT(idThread) FROM '.LearningForumthread::model()->tableName().' WHERE idForum = '.$this->idForum.' AND erased = 0))
				FROM '.LearningForummessage::model()->tableName().' as msg
				INNER JOIN '.LearningForumthread::model()->tableName().' AS thread
				ON msg.`idThread` = thread.`idThread`
				AND thread.idForum = '.$this->idForum.'
				AND thread.erased = 0';
		$res = Yii::app()->db->createCommand($sql)->queryScalar();
		return $res ? $res : 0;
	}

	/**
	 * Create empty init forum from comments blocks
	 * @param $course_id
	 * @return LearningForum
	 */
	public static function createCommentsForum($course_id)
	{
		$model = new LearningForum();
		$model->idCourse = $course_id;
		$model->title = Yii::t('standard', '_COMMENTS');
		$model->description = Yii::t('standard', '_COMMENTS');
		$model->save(false);
		return $model;
	}

}