<?php

/**
 * This is the model class for table "app7020_content".
 *
 * The followings are the available columns in table 'app7020_content':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $filename
 * @property string $originalFilename
 * @property integer $idThumbnail
 * @property integer $contentType
 * @property integer $conversion_status
 * @property integer $userId
 * @property integer $duration
 * @property integer $viewCounter
 * @property string $created
 * @property boolean $shownInList

 *
 * The followings are the available model relations:
 * @property CoreUser $contributor
 * @property CoreUser[] $gurus
 * @property App7020ContentThumbs[] $thumbs
 * @property App7020TagLink[] $tagLinks
 * @property App7020TopicContent[] $topicContentLinks
 * @property CoreAsset $thumbnailAsset
 * @property App7020Question[] $questions
 * @property App7020SettingsPeerReview[] $peer_review_settings
 * @property App7020ContentPublished[] $published
 *
 * tempTopics is temp container for Topics
 * @property string $tempTopics
 *
 *  @property App7020Tooltips[] $tooltips
 *  @property App7020ContentLinks $contentLinks
 *
 * 
 */
class App7020Content extends CActiveRecord {

	/**
	 * Content type Vidoe Object
	 * @var int
	 */
	const CONTENT_TYPE_VIDEO = 1;
	const CONTENT_TYPE_DOC = 2;
	const CONTENT_TYPE_EXCEL = 3;
	const CONTENT_TYPE_PPT = 4;
	const CONTENT_TYPE_PDF = 5;
	const CONTENT_TYPE_TEXT = 6;
	const CONTENT_TYPE_IMAGE = 7;
	const CONTENT_TYPE_QUESTION = 8;
	const CONTENT_TYPE_ANSWER = 9;
	const CONTENT_TYPE_OTHER = 10;
	const CONTENT_TYPE_DEFAULT_OTHER = 11;
	const CONTENT_TYPE_LINKS = 15;

	static public $contentTypes = array(
		App7020Content::CONTENT_TYPE_VIDEO => array(
			'outputPrefix' => 'video',
			'allowed' => array('avi', 'mpg', 'mp4', 'mpeg', '3gp', 'mpe', 'm2v', 'wmv', 'mov', 'webm')
		),
		App7020Content::CONTENT_TYPE_DOC => array(
			'outputPrefix' => 'doc',
			'allowed' => array('doc', 'docx', 'rtf', 'odt'),
			'thumbnail' => 'doc.jpg'
		),
		App7020Content::CONTENT_TYPE_EXCEL => array(
			'outputPrefix' => 'excel',
			'allowed' => array('xls', 'xlsx', 'ods'),
			'thumbnail' => 'xls.jpg'
		),
		App7020Content::CONTENT_TYPE_PPT => array(
			'outputPrefix' => 'ppt',
			'allowed' => array('ppt', 'odp', 'pptx'),
			'thumbnail' => 'ppt.jpg'
		),
		App7020Content::CONTENT_TYPE_PDF => array(
			'outputPrefix' => 'pdf',
			'allowed' => array('pdf'),
			'thumbnail' => 'pdf.jpg'
		),
		App7020Content::CONTENT_TYPE_TEXT => array(
			'outputPrefix' => 'text',
			'allowed' => array('txt'),
			'thumbnail' => 'txt.jpg'
		),
		App7020Content::CONTENT_TYPE_IMAGE => array(
			'outputPrefix' => 'images',
			'allowed' => array('jpg', 'gif', 'png', 'bmp', 'jpeg'),
		),
		App7020Content::CONTENT_TYPE_OTHER => array(
			'outputPrefix' => 'other',
			'thumbnail' => 'otherfile.jpg'
		),
		App7020Content::CONTENT_TYPE_DEFAULT_OTHER => array(
			'outputPrefix' => 'other',
			'allowed' => array('zip', 'mp3'),
			'thumbnail' => 'otherfile.jpg'
		),
	);
	static public $convertibleTypes = array(
		'doc',
		'docx',
		'odt',
		'pages',
		'pdf',
		'ppt',
		'key',
		'pptx',
		'csv',
		'numbers',
		'ods',
		'xls',
		'xlsx',
		'rtf'
	);

	const DOCUMENT_IMAGES_S3_FOLDER = 'images';

	/**
	 * Conversion status
	 * uploading to local storage
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_PENDING = 0;

	/**
	 * is stored to filesystem on LMS
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_FILESTORAGE = 1;

	/**
	 * saved metadata without thumbnails and finished conversion
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_PREPARE_SAVE = 2;

	/**
	 * moved to AMAZON cloud storage
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_S3 = 3;

	/**
	 * moved to AMAZON cloud storage from moblie application - not ready for convertion, no metadata uploaded 
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_FROM_MOBILE = 4;

	/**
	 * if conversion is finished
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_SUCCESS = 6;

	/**
	 * if SNS about conversion have any warnings
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_WARNING = 7;

	/**
	 * If SNS about conversion return error
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_ERROR = 8;

	/**
	 * If we have SUCCESS conversion and stored thumbnails
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_FINISHED = 10;

	/**
	 * If we have content in review status
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_INREVIEW = 15;

	/**
	 * If we have content in UNPUBLISH status
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_UNPUBLISH = 18;

	/**
	 * If we have content approved for show in knowledge 
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_APPROVED = 20;

	/**
	 * Various scenarios
	 *
	 * @var string
	 */
	const SCENARIO_METADATA = 'crud_metadata';

	/**
	 * Scenario for procedures on 
	 * S3 when all is uploaded and edited
	 */
	const SCENARIO_FINISH = 'finish';

	/**
	 * Scenario until edit behavior is in REVIEW
	 */
	const SCENARIO_INREVIEW = 'crud_inreview';

	/**
	 * Scenario when changes only conversion status to "IN REVIEW" or to "APPROVED"
	 */
	const SCENARIO_CHANGE_ONLY_STATUS = 'change_only_status';

	/**
	 * Scenario for work with content type - Links
	 */
	const SCENARIO_STORE_LINKS = 'links_store';

	/**
	 * Scenario when content type is Video
	 */
	const SCENARIO_UPLOAD_VIDEO = 'upload_video';

	/**
	 * Constant for determine that selected cue point should run script for viewing the asset
	 */
	const CUEPOINT_VIDEO_VIEW = 'app7020ViewContent';

	/**
	 * On which percentage of the clip should be marked as viewed
	 */
	const VIDEO_VIEW_PERCENTAGE = 0.75;

	/**
	 * Internally used
	 *
	 * @var mixed
	 */
	public $topicsToAssign = array();

	/**
	 * Store all posted tags as array
	 * @var array 
	 */
	protected $tagsToAssign;

	/**
	 * Store all new incoming thumbnails models
	 * non-related object
	 * @var array|App7020ContentThumbs
	 */
	protected $incomingThumbs = array();

	/**
	 * The active thumbnail id
	 * @var int 
	 */
	protected $activeThumbnail;

	/**
	 * Store search field information on ComboListView
	 * @var string
	 */
	public static $search_input = null;

	/**
	 * The logged user has access to edit, after contet was invike by any methodd
	 * @var boolean 
	 */
	public $isContentAdmin = false;

	/**
	 * The logged user has access to review/edit, after contet was invike by any method
	 * @var boolean 
	 */
	public $isContentGuru = false;

	/**
	 * If current user is contributor on this assets
	 */
	public $isContributor = false;

	/**
	 * temp container for Topics
	 * @var str
	 */
	public $tempTopics = '';

	/**
	 * Determine User Role
	 * @var int
	 */
	public $userRole = self::USER_ROLE_LEARNER;

	const USER_ROLE_LEARNER = 0;
	const USER_ROLE_CONTRIBUTOR = 1;
	// God Admin is considered as Axpert
	const USER_ROLE_EXPERT = 2;

	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_content';
	}

	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('topicsToAssign', 'requireTopicsToAssign', 'on' => self::SCENARIO_METADATA),
			array('title, description', 'required', 'on' => array('update', self::SCENARIO_METADATA, self::SCENARIO_INREVIEW, self::SCENARIO_FINISH)),
			array('contentType, idSource, idThumbnail, contentType, viewCounter, userId, duration, shownInList, sns', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 255),
			array('filename, originalFilename', 'length', 'max' => 512),
			array('filename, originalFilename, created', 'required'),
			// @todo Please remove those attributes that should not be searched.
			array('id, title, description, filename, originalFilename, contentType, idSource, conversion_status, userId, duration, viewCounter, created, sns', 'safe', 'on' => 'search'),
			array('duration', 'isValidDuration', 'on' => self::SCENARIO_UPLOAD_VIDEO),
		);
	}

	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contributor' => array(self::BELONGS_TO, 'CoreUser', 'userId'),
			'peer_review_settings' => array(self::HAS_ONE, 'App7020SettingsPeerReview', 'idContent'),
			'published' => array(self::HAS_ONE, 'App7020ContentPublished', 'idContent'),
			'edited' => array(self::HAS_MANY, 'App7020ContentPublished', 'idContent'),
			'tagLinks' => array(self::HAS_MANY, 'App7020TagLink', 'idContent'),
			'thumbs' => array(self::HAS_MANY, 'App7020ContentThumbs', 'idContent'),
			'topicContentLinks' => array(self::HAS_MANY, 'App7020TopicContent', 'idContent'),
			// Through relation (1 content -> many tag links -> many tags
			// Array of models of TAGS related to THIS content
			'tags' => array(self::HAS_MANY, 'App7020Tag', array('idTag' => 'id'), 'through' => 'tagLinks'),
			// Through relation (1 content -> many topic links -> many topics
			// Array of models of TAGS related to THIS content
			'topics' => array(self::HAS_MANY, 'App7020TopicTree', array('idTopic' => 'id'), 'through' => 'topicContentLinks'),
			'thumbnailAsset' => array(self::BELONGS_TO, 'CoreAsset', 'idThumbnail'),
			'questions' => array(self::HAS_MANY, 'App7020Question', 'idContent'),
			'viewes' => array(self::HAS_MANY, 'App7020ContentHistory', 'idContent', 'group' => 'idUser'),
			'tooltips' => array(self::HAS_MANY, 'App7020Tooltips', 'idContent'),
			'contentLinks' => array(self::HAS_ONE, 'App7020ContentLinks', 'idContent'),
		);
	}

	public function scopes() {
		return array(
			'editBy' => array(
				'condition' => 'actionType = ' . App7020ContentPublished::ACTION_TYPE_EDIT,
				'order' => 'edited.id DESC',
				'limit' => 1,
				'together' => true
			),
		);
	}

	/**
	 * Invoke flags after content was build
	 */
	function afterFind() {
		parent::afterFind();
		//$this->isContentAdmin = $this->defineContentAdminAccess();
		$this->isContentGuru = $this->hasReviewsAccess();
		$this->isContributor = ($this->userId == Yii::app()->user->id);
		if (empty($this->peer_review_settings)) {
			$this->peer_review_settings = new App7020SettingsPeerReview();
		}

		// Determine User Role
		if (Yii::app()->user->isGodAdmin || $this->hasReviewsAccess()) {
			$this->userRole = self::USER_ROLE_EXPERT;
		} elseif ($this->isContributor) {
			$this->userRole = self::USER_ROLE_CONTRIBUTOR;
		} else {
			$this->userRole = self::USER_ROLE_LEARNER;
		}
	}

	/**
	 * Check if asset is rated by current user
	 * @return boolean
	 */
	public function isRatedByMe() {
		$isRated = false;
		$modelContentRating = App7020ContentRating::model()->find(array(
			'condition' => 'idContent=:idContent AND idUser=:idUser',
			'params' => array(':idContent' => $this->id, ':idUser' => Yii::app()->user->id)
		));
		if ($modelContentRating) {
			$isRated = true;
		}
		return $isRated;
	}

	/**
	 * Check the stored topics. This implement required validation for topic atttr
	 * @param string $param
	 * @param string $attr
	 */
	public function requireTopicsToAssign($attr, $param) {
		if (empty($this->topicsToAssign)) {
			$this->addError($attr, Yii::t('app7020', '"Topics" are required value for save!'));
		}
	}

	/*
	 * Validate video duration by core_settings
	 * @param string $param
	 * @param string $attr
	 */

	public function isValidDuration($attr, $params) {
		$tempFolder = Docebo::getUploadTmpPath();
		$tempFilePath = realpath($tempFolder . DIRECTORY_SEPARATOR . $this->filename);
		$duration = App7020Helpers::getVideoDuration($tempFilePath);
		$isValidDuration = ShareApp7020Settings::isValidDuration($duration);
		if (!is_bool($isValidDuration)) {
			switch ($isValidDuration) {
				case ShareApp7020Settings::VIDEO_DURATION_SMALL:
					$durationLimit = Settings::get(ShareApp7020Settings::VIDEO_NOT_SHORTER_TIME);
					$this->addError($attr, Yii::t('app7020', 'Video duration can not be shorter than ') . $durationLimit . ' (hh:mm:ss)');
					break;
				case ShareApp7020Settings::VIDEO_DURATION_BIGGER:
					$durationLimit = Settings::get(ShareApp7020Settings::VIDEO_NOT_LONGER_TIME);
					$this->addError($attr, Yii::t('app7020', 'Video duration can not be longer than ') . $durationLimit . ' (hh:mm:ss)');
					break;
			}
		}
	}

	/**
	 *
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idThumbnail' => 'Thumbnail asset ID',
			'title' => 'Title',
			'description' => 'Description',
			'filename' => 'Filename',
			'originalFilename' => 'Original Filename',
			'contentType' => 'Content Type',
			'idSource' => 'This is relation field to all content sources. 1 - Web App, 2-Android App, 3 iOs Devices',
			'conversion_status' => 'Conversion Status',
			'duration' => 'Duration(in seconds)',
			'viewCounter' => 'Views counter',
			'created' => 'Created',
			'sns' => 'Has SNS Proccessing'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 *         based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('filename', $this->filename, true);
		$criteria->compare('originalFilename', $this->originalFilename, true);
		$criteria->compare('idThumbnail', $this->idThumbnail);
		$criteria->compare('contentType', $this->contentType);
		$criteria->compare('idSource', $this->idSource);
		$criteria->compare('conversion_status', $this->conversion_status);
		$criteria->compare('userId', $this->userId);
		$criteria->compare('duration', $this->duration);
		$criteria->compare('viewCounter', $this->viewCounter);
		$criteria->compare('created', $this->created, true);
		// $criteria->compare('sns', $this->sns, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *            active record class name.
	 * @return App7020Content the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * List all content items having assigned Tag ID (and only if it still exists, hence the second join)
	 *
	 * @param string $idTag            
	 *
	 * @return array
	 */
	public static function getItemsHavingTagId($idTag) {
		$command = Yii::app()->db->createCommand();
		$command->select('DISTINCT(c.id) as cid')
				->from('app7020_content c')
				->join('app7020_tag_link tl', 'c.id=tl.idContent')
				->join('app7020_tag tag', 'tag.id=tl.idTag')
				->where('tag.id=:idTag');

		$params[':idTag'] = $idTag;
		$rows = $command->queryAll(true, $params);

		$result = array();
		foreach ($rows as $row) {
			$result[] = $row['cid'];
		}

		return $result;
	}

	/**
	 * List all content items having assigned a Tag which consists the string tagText
	 *
	 * @param string $idTag            
	 *
	 * @return array
	 */
	public static function getItemsHavingTagText($tagText) {
		$command = Yii::app()->db->createCommand();
		$command->select('DISTINCT(c.id) as cid')
				->from('app7020_content c')
				->join('app7020_tag_link tl', 'c.id=tl.idContent')
				->join('app7020_tag tag', 'tag.id=tl.idTag')
				->where('tag.tagText LIKE :tagText');

		$params[':tagText'] = '%' . $tagText . '%';
		$rows = $command->queryAll(true, $params);

		$result = array();
		foreach ($rows as $row) {
			$result[] = $row['cid'];
		}

		return $result;
	}

	/**
	 * 
	 * @return boolean
	 */
	function secureSave() {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}
		try {
			if (!$this->save()) {
				$transaction->rollback();
				return false;
			} else {
				$transaction->commit();
				return true;
			}
		} catch (Exception $ex) {
			$transaction->rollback();
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 *
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {
		try {
			if (App7020Content::SCENARIO_METADATA == $this->getScenario() || App7020Content::SCENARIO_FINISH == $this->getScenario()) {
				$this->afterSaveTopics();
				$this->afterSaveTags();
			} else if (App7020Content::SCENARIO_INREVIEW == $this->getScenario()) {
				$this->afterSaveTags();
				if (Yii::app()->user->isGodAdmin) {
					$this->afterSaveTopics();
				}
			} elseif ($this->getScenario() == App7020Content::SCENARIO_CHANGE_ONLY_STATUS) {
				//$pub = $this->published;
				$pub = App7020ContentPublished::model()->findByAttributes(array('idContent' => $this->id, 'actionType' => App7020ContentPublished::ACTION_TYPE_PUBLISH));
				if ($this->conversion_status == self::CONVERSION_STATUS_APPROVED) {
					$pub = new App7020ContentPublished;
					$pub->idUser = Yii::app()->user->id;
					$pub->idContent = $this->id;
					$pub->save(false);
					$this->getRelated('published', true);
				} else if ($this->conversion_status == self::CONVERSION_STATUS_UNPUBLISH && !empty($pub->id)) {
					return $pub->delete();
				}
				return true;
			}
			$this->afterSaveThumbs();
			$this->afterSaveActiveThumb();
//            $wsEvent =  new App7020ContentWSClient();
//            $wsEvent->sendChangedStatus($this->id);
			return true;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);		    
			return false;
		}
		parent::afterSave();
	}

	/**
	 * This methos has fired after content save and read all stored thumbs clases 
	 * if thumb is exist will update it, in other case will create new thumb
	 * @return boolean
	 */
	function afterSaveThumbs() {
		//if we have save on existing thumbs

		if (!empty($this->incomingThumbs) && is_array($this->incomingThumbs)) {
			foreach ($this->incomingThumbs as $thumb) {
				if ($thumb instanceof App7020ContentThumbs) {
					$thumb->idContent = $this->id;
					$thumb->save();
					if ($thumb->is_active == 1) {
						$this->activeThumbnail = $thumb->id;
					}
				} else {
					Yii::log('UNEXEPTED THUMB OBJECT', CLogger::LEVEL_ERROR);
					return false;
				}
			}
		}
	}

	/**
	 * 
	 * @return boolean
	 */
	function afterSaveActiveThumb($step = 0) {
		if (!empty($this->activeThumbnail)) {
			try {
				App7020ContentThumbs::model()->updateAll(array('is_active' => 0), 'idContent = :idContent', array('idContent' => $this->id));
				$realThumbModel = App7020ContentThumbs::model()->findByPk($this->activeThumbnail);
				$realThumbModel->is_active = 1;
				//generate path for save the file from S3
				$localPath = $this->getLocalAssetPath($realThumbModel->getFilename());
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
				//get relative path to image on s3 server
				$relativePathToImage = $realThumbModel->getS3RelativePath();
				//Download status for thumbnail
				$downloadedImage = $storage->downloadAs($relativePathToImage, $localPath);
				$coreAssetSaveStatus = null;
				
				Yii::log('Try to download App7020 Image ' . $relativePathToImage . PHP_EOL, CLogger::LEVEL_INFO);
				Yii::log('App7020 Image ' . $downloadedImage, CLogger::LEVEL_INFO);
				
				if ($downloadedImage && $step <= 100) {
				    Yii::log('Success download App7020 Image ' . $relativePathToImage . PHP_EOL, CLogger::LEVEL_INFO);
					$coreAsset = new CoreAsset();
					$coreAsset->type = CoreAsset::TYPE_APP7020;
					$coreAsset->sourceFile = $localPath;
					if (!$coreAsset->save()) {
						throw new Exception('Cannot save asset');
					} else {
						FileHelper::removeFile($localPath);
						//delete old image from server
						if ($coreAsset->id != $realThumbModel->content->idThumbnail && (int) $realThumbModel->content->idThumbnail > 0) {
							CoreAsset::model()->findByPk($realThumbModel->content->idThumbnail)->delete();
						}
						$realThumbModel->content->isNewRecord = false;
						$realThumbModel->content->setAttribute('idThumbnail', $coreAsset->id);
						if (App7020Content::SCENARIO_FINISH == $this->getScenario() && $realThumbModel->content->conversion_status <= App7020Content::CONVERSION_STATUS_FINISHED) {
							$realThumbModel->content->setAttribute('conversion_status', App7020Content::CONVERSION_STATUS_FINISHED);
						} else if ($realThumbModel->content->conversion_status < App7020Content::CONVERSION_STATUS_FINISHED) {
							$realThumbModel->content->setAttribute('conversion_status', App7020Content::CONVERSION_STATUS_SNS_SUCCESS);
						}
						$realThumbModel->content->save(false);
						$realThumbModel->save();
					}
				} else {
				    Yii::log('Failed  download App7020 Image ' . $relativePathToImage . PHP_EOL, CLogger::LEVEL_ERROR);
					return false;
				}
			} catch (Exception $ex) {
				Yii::log($ex, CLogger::LEVEL_ERROR);
				return false;
			}
		}
	}

	/**
	 * Save topics in App7020TopcisContent table
	 * Auto remove unused nodes
	 * @return boolean
	 */
	function afterSaveTopics() {
		try {
			//check all existing content topics 
			$topicExistingContainer = array();
			foreach ($this->topicContentLinks as $existingTopicsObjects) {
				$topicExistingContainer[$existingTopicsObjects->id] = $existingTopicsObjects->idTopic;
			}
			$array_delete = array_diff($topicExistingContainer, $this->topicsToAssign);
			$array_insert = array_diff($this->topicsToAssign, $topicExistingContainer);
			//delete of unused  nodes 
			$deleteCriteria = new CDbCriteria();
			$deleteCriteria->addInCondition('id', array_keys($array_delete));
			App7020TopicContent::model()->deleteAll($deleteCriteria);
			foreach ($array_insert as $insertObject) {
				$topicContent = new App7020TopicContent;
				$topicContent->setAttributes(array('idTopic' => $insertObject, 'idContent' => $this->id));
				$status = $topicContent->save();
				if (!$status) {
					return false;
				}
			}
			return true;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Save all tags after native save on Content
	 * @return boolean
	 */
	function afterSaveTags() {
		try {
			$allWords = explode(',', $this->tagsToAssign);
			$criteria = new CDbCriteria();
			$criteria->addInCondition('tagText', $allWords);
			$tags = App7020Tag::model()->findAll($criteria);

			$allTags = array();
			foreach ($tags as $tag) {
				$allTags[$tag->id] = $tag->tagText;
			}
			$criteriaDest = new CDbCriteria();
			$criteriaDest->select = 't.*';
			$criteriaDest->join = 'INNER JOIN ' . App7020TagLink::model()->tableName() . '  ON  ' . App7020TagLink::model()->tableName() . '.idTag = t.id ';
			$criteriaDest->join .= 'INNER JOIN ' . App7020Content::model()->tableName() . '  ON ' . App7020Content::model()->tableName() . '.id = ' . App7020TagLink::model()->tableName() . '.idContent ';
			$criteriaDest->condition = 'idContent = :idContent';
			$criteriaDest->params = array('idContent' => $this->id);
			$tags = App7020Tag::model();
			$all_dist_tags = array();
			foreach ($tags->findAll($criteriaDest) as $distTag) {
				$all_dist_tags[$distTag->id] = $distTag->tagText;
			}

			//compare for delete and insert
			$array_insert = array_diff($allTags, $all_dist_tags);
			$array_delete = array_diff($all_dist_tags, $allTags);

			$deleteCriteria = new CDbCriteria();
			$deleteCriteria->condition = 'idContent = :idContent';
			$deleteCriteria->params = array('idContent' => $this->id);
			$deleteCriteria->addInCondition('idTag', array_keys($array_delete));
			App7020TagLink::model()->deleteAll($deleteCriteria);
			foreach ($array_insert as $insert_key => $insert_value) {
				$insertTagModel = new App7020TagLink;
				$insertTagModel->idContent = $this->id;
				$insertTagModel->idTag = $insert_key;
				if (!$insertTagModel->save()) {
					return false;
				}
			}
			return true;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Return array for fancytree with selected topics
	 * @param App7020Content $content
	 * @return array
	 */
	public function getPreselectedTopics() {
		$arrayTrash = array();
		//preselected topics
		foreach ($this->topicContentLinks as $key => $topicObject) {
			$arrayTrash[] = (int) $topicObject->idTopic;
		}
		$arrayTrash = implode(",", $arrayTrash);
		return $arrayTrash;
	}

	/**
	 * Check if file extension is in convertable types
	 * @param string $file_name
	 * @return $extension or false
	 */
	public static function isConvertible($file_name) {

		$extension = App7020Helpers::getExtentionByFilename($file_name);

		if (in_array($extension, self::$convertibleTypes)) {
			return $extension;
		} else {
			return false;
		}
	}

	/**
	 * Append class to progress bar in current status
	 *
	 * @param iny $status_code
	 * @author Simeonov
	 * @return string
	 */
	public static function getProccessClass($status_code = '') {
		$appended_class = '';
		if ($status_code == self::CONVERSION_STATUS_PENDING) {
			$appended_class = 'app7020-local-proccessing';
		} elseif ($status_code == self::CONVERSION_STATUS_FILESTORAGE) {
			$appended_class = 'app7020-s3-proccessing';
		} elseif ($status_code == self::CONVERSION_STATUS_PREPARE_SAVE) {
			$appended_class = 'app7020-s3-proccessing app7020-local-prepare_db';
		} elseif ($status_code == self::CONVERSION_STATUS_S3) {
			$appended_class = 'app7020-s3-uploaded';
		} elseif ($status_code == App7020Content::CONVERSION_STATUS_SNS_SUCCESS) {
			$appended_class = 'app7020-s3-converted app7020-s3-success';
		} elseif ($status_code >= App7020Content::CONVERSION_STATUS_FINISHED && $status_code < App7020Content::CONVERSION_STATUS_APPROVED) {
			$appended_class = 'app7020-s3-inreview';
		} elseif (self::hasS3Error($status_code)) {
			$appended_class = 'app7020-s3-wrong';
		}
		return $appended_class;
	}

	/**
	 * Set new thumbs in content model 
	 * @param App7020ContentThumbs $thumbsModel
	 * @return \App7020Content
	 */
	function setThumb(App7020ContentThumbs $thumbsModel) {
		$this->incomingThumbs[] = $thumbsModel;
		return $this;
	}

	/**
	 * Create Initial File row in db for begining on upload
	 *
	 * @param array $params
	 *            App7020Content|false
	 */
	public static function createInitialFileRecord($params) {
		try {
			$content = new self();
			$content->originalFilename = $params['originalFilename'];
			$content->contentType = $params['contentType'];
			$content->conversion_status = self::CONVERSION_STATUS_PENDING;
			$content->userId = $params['userId'];
			$status = $content->save(false);
			if ($status) {
				return $content;
			} else {
				return false;
			}
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * get ALL AMAZON proccessed files
	 * @return static|boolean
	 */
	public static function getAllConverted($array) {
		$criteria = new CDbCriteria();
		$criteria->condition = 'userId=:userId';
		$criteria->params = array('userId' => Yii::app()->user->id);
		$criteria->addInCondition("id", $array);
		try {
			return App7020Content::model()->findAll($criteria);
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Get Single row record from DB if id and user id of under is same with request
	 * @param type $id
	 * @return App7020Content
	 */
	public static function getSingleRecordById($id) {
		try {
			$expr = (!App7020Content::model()->hasReviewsAccess() && !Yii::app()->user->getIsGodAdmin() && !Yii::app()->user->getIsPU());
			$criteria = new CDbCriteria();
			if ($expr) {
				$criteria->condition = 'userId = :userId AND id = :id';
				$criteria->params = array('userId' => Yii::app()->user->id, 'id' => (int) $id);
			} else {
				$criteria->condition = 'id = :id';
				$criteria->params = array('id' => (int) $id);
			}
			return App7020Content::model()->find($criteria);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	/**
	 * Get all Upload files from DB by Criteria
	 *
	 * @author Simeonov
	 * @return App7020Content
	 */
	public static function getUploadedFiles($customConfig = false) {
		try {
			$criteria = new CDbCriteria();
			$proccessing_criteria = ($customConfig['proccessing_criteria']) ? $customConfig['proccessing_criteria'] : Yii::app()->params['s3Storages']['amazon']['proccessing_criteria'];
			if ($customConfig['created'] == 'week') {
				$criteria->condition = 'userId = :userId AND created > (NOW() - INTERVAL 7 DAY) AND shownInList = 0';
			} else {
				$criteria->condition = 'userId = :userId';
			}
			$criteria->params = array('userId' => Yii::app()->user->id);
			$criteria->addInCondition('conversion_status', $proccessing_criteria);
			return App7020Content::model()->findAll($criteria);
		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	public static function deleteCorruptedRows() {
		$deleteCriteria = new CDbCriteria();
		$deleteCriteria->condition = 'userId=:userId';
		$deleteCriteria->params = array('userId' => Yii::app()->user->id);
		$deleteCriteria->addInCondition('conversion_status', self::getErrorCodes());
		return App7020Content::model()->deleteAll($deleteCriteria);
	}

	/**
	 * Check any code for status.
	 *
	 * If status is Error or warning
	 * will return true
	 *
	 * @param int $status_code            
	 * @author Simeonov
	 * @return boolean
	 */
	public static function hasS3Error($status_code) {
		$array_errors = array(
			self::CONVERSION_STATUS_SNS_ERROR,
			self::CONVERSION_STATUS_SNS_WARNING
		);
		if (in_array((int) $status_code, $array_errors)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check any code for status.
	 *
	 * If status until upload to local server or we have any error will return false
	 *
	 * @param int $status_code            
	 * @author Simeonov
	 * @return boolean
	 */
	public static function hasEditAccess($status_code) {
		$array_errors = array(
			self::CONVERSION_STATUS_SNS_ERROR,
			self::CONVERSION_STATUS_SNS_WARNING,
			self::CONVERSION_STATUS_PENDING
		);
		if (in_array((int) $status_code, $array_errors)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Make SQL Query and Configurate ComboListView
	 * @param string $method Example: onlyMy
	 * @param int $perPage
	 * @return \CSqlDataProvider
	 */
	public static function sqlDataProvider($method = false, $customConfig = array(), $forceParams = array()) {
		$myContributionsFilter = Yii::app()->request->getParam('radioSwitch', false);
//		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		$searchInput = $forceParams['search_input'] ? $forceParams['search_input'] : trim(Yii::app()->request->getParam('search_input', false));


		$tmp = Yii::app()->request->getParam('mainFilter', false);
		$mainFilter = !empty($tmp) ? Yii::app()->request->getParam('mainFilter', false) : $forceParams['sessionMainFilterKL'];
		$tmp = Yii::app()->request->getParam('topicFilter', false);
		
		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;

		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		$contentTags = (!empty($customConfig['tags'])) ? $customConfig['tags'] : null;

		if ($searchInput) {
			App7020Content::$search_input = $searchInput ? $searchInput : null;
		}
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select('c.*, u.userId, c.userId AS idUser, '
				. '(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM ' . App7020ContentRating::model()->tableName() . ' WHERE idContent=c.id) AS contentRating,'
				. '(SELECT COUNT(id) FROM ' . App7020ContentHistory::model()->tableName() . ' WHERE idContent=c.id) AS contentViews ');
		$commandBase->from(App7020Assets::model()->tableName() . ' c');
		$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'c.userId = u.idst');
		$commandBase->leftJoin(App7020TopicContent::model()->tableName() . ' tC', 'c.id = tC.idContent');
		
		$commandBase->group('c.id');

		if ($method != 'onlyMy') {
			$commandBase->andWhere('c.conversion_status = ' . App7020Content::CONVERSION_STATUS_APPROVED);
		} else {
			if (!$myContributionsFilter) {
				if ($customConfig['shortList']) {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
				} else {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Content::CONVERSION_STATUS_FINISHED);
					$commandBase->andWhere('c.conversion_status < ' . App7020Content::CONVERSION_STATUS_UNPUBLISH);
				}
			} else {
				if ($myContributionsFilter == App7020Assets::CONVERSION_STATUS_APPROVED) {
					$commandBase->andWhere('c.conversion_status = ' . App7020Assets::CONVERSION_STATUS_APPROVED);
				} elseif ($myContributionsFilter == App7020Assets::CONVERSION_STATUS_UNPUBLISH) {
					$commandBase->andWhere('c.conversion_status = ' . App7020Content::CONVERSION_STATUS_UNPUBLISH);
				} else {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Content::CONVERSION_STATUS_FINISHED);
					$commandBase->andWhere('c.conversion_status < ' . App7020Content::CONVERSION_STATUS_UNPUBLISH);
				}
			}

			//$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
		}

		// TAGS FILTER
		if ($contentTags) {
			$commandBase->leftJoin(App7020TagLink::model()->tableName() . ' tL', 'c.id = tL.idContent');
			$commandBase->andWhere('tL.idTag IN (' . implode(",", $contentTags) . ')');
		}

		// ASSETS FILTER
		if ($mainFilter['assets'] > 0) {
			switch ($mainFilter['assets']) {
				case 1:
					$commandBase->andWhere('c.contentType = 1');
					break;
				case 2:
					$commandBase->andWhere('c.contentType >= 2 AND c.contentType <= 6');
					break;
				case 3:
					$commandBase->andWhere('c.contentType = 15');
					break;
				case 4:
					$commandBase->andWhere('c.contentType >= 7 AND c.contentType < 15');
					break;
			}
			
		}

		// GET ONLY MY CONTENTS
		if ($method == 'onlyMy') {
			if ($method == 'onlyMy') {
				$commandBase->andWhere('c.userId = "' . Yii::app()->user->idst . '"');
			}
		}

		if (App7020Content::$search_input != null) {
			$commandBase->andWhere("CONCAT(COALESCE(c.title,''), ' ', COALESCE(c.description,'')) LIKE :search_input", array(':search_input' => '%' . App7020Content::$search_input . '%'));
		}

		// ORDERS
		switch ($mainFilter['sort']) {
			case 1:
				$commandBase->order('contentRating DESC, created DESC');
				break;
			case 2:
				$commandBase->order('contentViews DESC, created DESC');
				break;
			case 3:
				$commandBase->order('c.duration DESC, created DESC');
				break;
			default :
				$commandBase->order('created DESC');
				break;
		}
		
		// ratings
		if($mainFilter['rating']){
			$commandBase->having('contentRating > '.$mainFilter['rating']);
		}
		
		
		switch ($mainFilter['uploaded']) {
			case 1: // THIS WEEK
				$commandBase->andWhere('YEARWEEK(created, 1) = YEARWEEK(NOW(), 1)');
				break;
			case 2: // THIS MONTH
				$commandBase->andWhere('EXTRACT(MONTH From created) = EXTRACT(MONTH From NOW())');
				break;
			case 3: // PAST 6 MONTHs
				$commandBase->andWhere('created > DATE_SUB(NOW(),INTERVAL 6 MONTH)');
				break;
			case 4: // THIS YEAR
				$commandBase->andWhere('EXTRACT(YEAR From created) = EXTRACT(YEAR From NOW())');
				break;
		}
		$content = $commandBase->queryAll();



		if ($setPagination) {
			$pageSize = 12; // Default
			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}
		

		$config = array(
			'totalItemCount' => count($content),
			'pagination' => $pagination,
			'keyField' => 'id',
		);
		$dataProvider = new CArrayDataProvider($content, $config);

		return $dataProvider;
	}

	/**
	 * Return type if have more than one content with this type
	 * @return Array
	 */
	public static function getAllTypeArray() {
		$result[0] = Yii::t('app7020', 'All');
		$all = self:: getUploadTypesForUploader();
		foreach ($all as $type_key => $type) {
			$result[$type_key] = $type['title'];
		}
		return $result;
	}

	/**
	 * Return all tags as string separated by come 
	 * @return string|boolean
	 */
	function getAllTagsAsString() {
		try {
			$output = array();
			foreach ($this->tagLinks as $tags) {
				$output[] = $tags->tag->tagText;
			}
			return implode(', ', $output);
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return '';
		}
	}

	/**
	 * Return model with active thumbnail
	 * @return \App7020ContentThumbs
	 */
	function getActiveThumbnail() {
		foreach ($this->thumbs as $thumb) {
			if ($thumb->is_active == 1) {
				return $thumb;
			}
		}
		return new App7020ContentThumbs;
	}

	/**
	 * Get all error codes
	 * @return array
	 */
	public static function getErrorCodes() {
		return array(
			self::CONVERSION_STATUS_PENDING,
			self::CONVERSION_STATUS_SNS_WARNING,
			self::CONVERSION_STATUS_SNS_ERROR,
		);
	}

	/**
	 * Get all codes attached for decuments
	 * @return array
	 */
	public static function getDefaultDocumentCodes() {
		return array(
			self::CONTENT_TYPE_DOC,
			self::CONTENT_TYPE_EXCEL,
			self::CONTENT_TYPE_PPT,
			self::CONTENT_TYPE_PDF,
			self::CONTENT_TYPE_TEXT,
			self::CONTENT_TYPE_DEFAULT_OTHER
		);
	}

	/**
	 * Check if file is video content
	 * @param string $filename
	 * @return boolean
	 */
	public static function isVideo($filename) {
		try {
			return self::getContentCodeByExt(App7020Helpers::getExtentionByFilename($filename)) == self::CONTENT_TYPE_VIDEO ? true : false;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Get status on uploaded file. Video Content or Document Content
	 * @param string $filename Uploaded file 
	 * @return boolean
	 */
	public static function isDocument($filename = false) {
		try {

			return self::isDocumentExt(App7020Helpers::getExtentionByFilename($filename));
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check if any extention is document
	 * @param string $ext
	 * @return boolean
	 */
	public static function isDocumentExt($ext) {
		try {
			$contentCode = self::getContentCodeByExt($ext);
			if (in_array($contentCode, self::getDefaultDocumentCodes())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Return Image Asset url to document preview thumbnail placholder
	 * @param int $keyOnType
	 * @return string
	 * 
	 * @DEPRICATED
	 */
	static private function _generateImageUrlByType($keyOnType) {
		$assetUrl = Yii::app()->theme->baseUrl;
		if ($keyOnType && isset(self::$contentTypes[(int) $keyOnType]['thumbnail'])) {
			return $assetUrl . '/images/contribute/' . self::$contentTypes[(int) $keyOnType]['thumbnail'];
		} else {
			return $assetUrl . '/images/contribute/' . self::$contentTypes[self::CONTENT_TYPE_OTHER]['thumbnail'];
		}
	}

	/**
	 * GEt all allowed extentions by ContentTypeID
	 * @param integer $typeId
	 * @return boolean|array
	 */
	static public function getAllowedExtByType($typeId) {
		if (isset(self::$contentTypes[(int) $typeId])) {
			return self::$contentTypes[(int) $typeId]['allowed'] ? self::$contentTypes[(int) $typeId]['allowed'] : false;
		} else {
			return false;
		}
	}

	/**
	 * Set new allowed ext 
	 * @param integer $typeId
	 * @param string $newExt
	 * @return \App7020Content
	 */
	static public function appendExtToType($typeId, $newExt) {
		if (isset(self::$contentTypes[(int) $typeId])) {
			self::$contentTypes[(int) $typeId]['allowed'][] = $newExt;
		}
		return new static();
	}

	/**
	 * Get thumbnail by ext. definition
	 * @param string $needleExt
	 * @return boolean|string
	 * @DEPRICATED
	 */
	static public function getThumbnailURLByExt($needleExt) {
		try {
			return self::_generateImageUrlByType(self::getContentCodeByExt($needleExt));
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Get Content code by Extention 
	 * @param type $needleExt
	 * @return boolean|integer
	 */
	public static function getContentCodeByExt($needleExt) {
		foreach (self::$contentTypes as $singleTypeKey => $singleType) {
			if (isset($singleType['allowed'])) {
				$keyOnTypeInternal = array_search($needleExt, $singleType['allowed']);
				if ($keyOnTypeInternal !== false) {
					return $singleTypeKey;
				}
			}
		}
		return false;
	}

	/**
	 * Return or print conternt tumbnail image by extention of file
	 * @param string $needleExt
	 * @param boolean $isEcho
	 * @param string $alt
	 * @param array $htmlOptions
	 * @return mixed
	 * 
	 * @DEPRICATED 
	 */
	static public function getThumbnailImageByExtention($needleExt, $isEcho = false, $alt = '', $htmlOptions = array()) {
		$image = CHtml::image(self::getThumbnailURLByExt($needleExt), $alt, $htmlOptions);
		if (!$isEcho) {
			return $image;
		} else {
			echo $image;
		}
	}

	/**
	 * Return or 
	 * @param string $needleExt
	 * @param boolean $isEcho
	 * @param string $alt
	 * @param array $htmlOptions
	 * @return mixed
	 */
	static public function getThumbnailImageByType($contentTypeId, $isEcho = false, $alt = '', $htmlOptions = array()) {
		$image = CHtml::image(self::_generateImageUrlByType($contentTypeId), $alt, $htmlOptions);
		if (!$isEcho) {
			return $image;
		} else {
			echo $image;
		}
	}

	/**
	 * Get All allowed types for upload 
	 * @return array
	 */
	static public function getAllowedForUpload() {
		$allTypesForUplaod = array();
		foreach (self::$contentTypes as $type) {
			if (isset($type['allowed'])) {
				$allTypesForUplaod = array_merge($allTypesForUplaod, $type['allowed']);
			}
		}
		return $allTypesForUplaod;
	}

	/**
	 * Return correct format JSON for 
	 * implement it in PLUploader 
	 * @return string (JSON)
	 */
	static public function getUploadTypesForUploader($whitKeys = true) {
		$tempArray = array();
		$array_tmp = Settings::get('file_extensions');

		if ((bool) Settings::get('other_contribution') && !empty($array_tmp)) {
			$tempArray += self::_parseFileExtention($array_tmp, $whitKeys);
		}
		foreach (self::$contentTypes as $key => $type) {
			if (isset($type['allowed'])) {
				if (
						((bool) Settings::get('documents_contribution') && in_array($key, self::getDefaultDocumentCodes())) ||
						$key == self::CONTENT_TYPE_VIDEO
				) {
					if ($whitKeys) {
						$tempArray[$key] = array(
							'title' => ucfirst($type['outputPrefix']),
							'extensions' => implode(',', $type['allowed'])
						);
					} else {
						$tempArray[] = array(
							'title' => ucfirst($type['outputPrefix']),
							'extensions' => implode(',', $type['allowed'])
						);
					}
				}
			}
		}

		return $tempArray;
	}

	/**
	 * Parse file extention
	 * @param array $array_ext
	 * @return boolean|array
	 */
	private static function _parseFileExtention($array_ext, $whitKeys = true) {
		$tempArray = array();
		try {

			if ($whitKeys) {
				$tempArray[self::CONTENT_TYPE_OTHER] = array(
					'title' => 'Other',
					'extensions' => App7020Helpers::clearComaStringInArray($array_ext)
				);
			} else {
				$tempArray[] = array(
					'title' => 'Other',
					'extensions' => App7020Helpers::clearComaStringInArray($array_ext)
				);
			}
			return $tempArray;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 *
	 * @param App7020Content $content
	 * @param boolean $isEcho
	 * @return string
	 */
	public function getPreviewThumbnailImage($isEcho = false, $imageVariant = 'small', $alt = '', $htmlOptions = array()) {
		if (App7020Content::isDocument($this->filename)) {
			return App7020Content::getThumbnailImageByExtention(App7020Helpers::getExtentionByFilename($this->filename), $isEcho);
		} else {
			$coreAsset = CoreAsset::model()->findByPk($this->idThumbnail);
			$src = '';
			if (!empty($coreAsset) && $coreAsset instanceof CoreAsset) {
				$src = $coreAsset->getUrl($imageVariant);
			}
			if (empty($src)) {
				$coreAsset = new CoreAsset();
				$coreAsset->type = CoreAsset::TYPE_APP7020;
				$height = $coreAsset->getImageVariantHeight($imageVariant);
				$width = $coreAsset->getImageVariantWidtht($imageVariant);
				$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
			}
			$htmlOptions['class'] = 'ThumbPreview';
			$image = CHtml::image($src, $alt, $htmlOptions);
			if ($isEcho) {
				echo $image;
			} else {
				return $image;
			}
		}
	}

	/**
	 * Return anchor for original asset of thumbnail
	 * @param App7020Content $content
	 * @param boolean $isEcho
	 */
	public function getPreviewThumbnailImageSource($isEcho = false, $imageVariant = 'small') {
		$coreAsset = CoreAsset::model()->findByPk($this->idThumbnail);
		$src = '';
		if (!empty($coreAsset) && $coreAsset instanceof CoreAsset) {
			$src = $coreAsset->getUrl($imageVariant);
		}
		if (empty($src)) {
			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$height = $coreAsset->getImageVariantHeight($imageVariant);
			$width = $coreAsset->getImageVariantWidtht($imageVariant);
			$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
		}
		if ($isEcho) {
			echo $src;
		} else {
			return $src;
		}
	}

	/**
	 * Returns Tumbnail
	 * @param array $data
	 * @param boolean $isEcho
	 * @param str $alt
	 * @param array $htmlOptions
	 * @return type
	 * 
	 * @depricated  it is mooved to app7020assets
	 * 
	 */
	public static function getPublcPreviewThumbnail($data = array(), $imageVariant = 'small', $isEcho = false, $alt = '', $htmlOptions = array()) {
		try {

			$content = new App7020Content();
			$content->setAttributes($data, false);
			$content->getRelated("thumbs", true);
			return $content->getPreviewThumbnailImage($isEcho, $imageVariant, $alt, $htmlOptions);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$width = $coreAsset->getImageVariantWidtht($imageVariant);
			$height = $coreAsset->getImageVariantHeight($imageVariant);
			$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
			$htmlOptions['class'] = 'ThumbPreview';
			$image = CHtml::image($src, $alt, $htmlOptions);
			if ($isEcho) {
				echo $image;
			} else {
				return $image;
			}
		}
	}

	/**
	 * Return public thumb URL for Video Content by their counter
	 * @param int $countImg ex. 000001
	 * @param App7020Content $content
	 * @return string
	 */
	public function getImageInputUrl($countImg) {
		$url = CFileStorage::getS3DomainStorage(CFileStorage::COLLECTION_7020)
				->fileUrl(
				CS3StorageSt::getConventionFolder($this->filename) . '/' .
				App7020Helpers::getAmazonInputKey($this->filename) .
				'/thumb/' .
				App7020Helpers::getAmazonInputKey($this->filename) .
				'-' . $countImg);
		return $url . '.png';
	}

	/**
	 * Get Content prefix
	 * @return string
	 */
	public function getContentPrefix() {
		return isset(self::$contentTypes[$this->contentType]['outputPrefix']) ? self::$contentTypes[$this->contentType]['outputPrefix'] : self::$contentTypes[App7020Content::CONTENT_TYPE_OTHER]['outputPrefix'];
	}

	/**
	 * Get Last uploaded assets
	 * @param int $limit
	 * @return array
	 */
	public static function getLastUploadedAssets($limit = 5) {
		$data = App7020Content::model()->findAllByAttributes(array('conversion_status' => 10), array('order' => 'created DESC', 'limit' => (int) $limit));
		return $data;
	}

	/**
	 * Get Local path for store the file in local LMS for module app7020
	 * @param string $filename
	 * @return boolean|string
	 */
	public function getLocalAssetPath($filename = false) {
		$folder = Docebo::getUploadTmpPath() . '/' . CoreAsset::TYPE_APP7020 . '/' . $this->getContentPrefix();
		try {
			if (!is_dir($folder)) {
				mkdir($folder, 0755, TRUE);
			}
			return $folder . '/' . $filename;
		} catch (CException $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Calculates the reviewed assets for the given period
	 *
	 * @param date optional $dateFrom start of the period
	 * @param date optional $dateTo end of the period
	 * @param int optional $userId ID of the user. If it's present, the returned assets must belongs to any of topics where the selected user is expert
	 *
	 * return array|integer An array with dates as the keys and count of the reviewed assets
	 */
	public static function getReviewedAssetsCountByDays($dateFrom = null, $dateTo = null, $userId = null) {


		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("id, DATE(created) AS contentDate");

		$commandBase->from = App7020Content::model()->tableName();

		$commandBase->where(" conversion_status >= " . App7020Content::CONVERSION_STATUS_FINISHED);

		if (!empty($dateFrom)) {
			$commandBase->andWhere(" DATE(created) >= :dateFrom", array(':dateFrom' => date("Y-m-d", strtotime($dateFrom))));
		}

		if (!empty($dateTo)) {
			$commandBase->andWhere(" DATE(created) <= :dateTo", array(':dateTo' => date("Y-m-d", strtotime($dateTo))));
		}

		$returnArray = array();
		if ($userId) {
			$expertTopics = App7020TopicExpert::getTopicsByUser($userId);
		}

		$resultArray = $commandBase->queryAll(true);
		foreach ($resultArray as $value) {
			if (!isset($returnArray[$value['contentDate']])) {
				$returnArray[$value['contentDate']] = 0;
			}
			if ($userId) {

				$contentTopics = App7020TopicContent::getTopicsByContent($value['id']);
				if (count(array_intersect($expertTopics, $contentTopics)) > 0) {
					$returnArray[$value['contentDate']] ++;
				}
			} else {
				$returnArray[$value['contentDate']] ++;
			}
		}
		return $returnArray;
	}

	/**
	 * Returns an array with reviewed assets related with selected expert
	 *
	 * @param int required $userId ID of the user. The returned assets must belongs to any of topics where the selected user is expert
	 * @param array optional $assetStatus the statusses of the asset
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getExpertsReviewedAssets($userId, $assetStatus = array(App7020Content::CONVERSION_STATUS_FINISHED), $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()) {

		if (empty($userId)) {
			return false;
		}


		$searchInput = trim(Yii::app()->request->getParam('search_input', false));

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("c.*");
		$commandBase->from(App7020Content::model()->tableName() . " c");
		$commandBase->leftJoin(App7020IgnoreList::model()->tableName() . " il", $userId . " = il.idUser AND il.idObject=c.id AND il.objectType='" . App7020IgnoreList::OBJECT_TYPE_ASSET . "'");

		$commandBase->where(" ISNULL(il.id)");

		if ($searchInput) {
			$commandBase->andWhere("c.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		$commandBase->andWhere(array('IN', 'conversion_status', implode(',', $assetStatus)));
		
		$returnArray = $commandBase->queryAll(true);

		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);

		if (!empty($providerConfig['comboListViewId'])) {
			unset($providerConfig['comboListViewId']);
		}

		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}

	/**
	 * Check user id is guru for this content
	 * @param int $userId
	 * @return boolean
	 */
	function hasReviewsAccess($userId = false) {
		if ($userId == false) {
			$userId = Yii::app()->user->id;
		}
		foreach (self::getContentExperts($this->id) as $guru) {
			if ($guru->idst == $userId) {
				return true;
			} else {
				continue;
			}
		}
		return false;
	}

	/**
	 * Check logged user for admin permissions for opened content (IS NOT LEARNER)
	 * @return boolean
	 */
	public function defineContentAdminAccess() {
		try {
			if ($this->contributor->idst == Yii::app()->user->id || Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPU()) {
				return true;
			}
			return $this->hasReviewsAccess();
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Make a record in content history table
	 * @param integer $idContent
	 * @return boolean
	 */
	public static function viewContent($idContent) {
		$contentHistoryObject = new App7020ContentHistory();
		$contentHistoryObject->idContent = $idContent;
		$contentHistoryObject->idUser = Yii::app()->user->idst;
		return $contentHistoryObject->save();
	}

	/**
	 * Gets similar assets ordered by  number of view, last update date, rank
	 * @param integer $idContent
	 * @param number $count
	 * @return array
	 */
	public static function getSimilarContents($idContent, $count = 5) {

		// Get all topics for this content ID
		$allTopicsOfContentArr = App7020TopicContent::getTopicsByContent($idContent);
		$allTopicsOfContent = implode(',', $allTopicsOfContentArr);

		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("c.*, 
	                       (SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent=c.id) AS contentRating, 
	                       (SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent=c.id) AS contentViews,
	                       (SELECT COUNT(id) FROM " . App7020TopicContent::model()->tableName() . " WHERE idContent=c.id AND idTopic IN(" . $allTopicsOfContent . ")) AS numberTopicMatch"
		);
		$dbCommand->from(App7020Content::model()->tableName() . " c");
		$dbCommand->where(" c.id <> :idContent AND c.conversion_status = " . App7020Content::CONVERSION_STATUS_APPROVED, array(":idContent" => $idContent));
		$dbCommand->order("numberTopicMatch DESC, contentViews DESC, DATE(created) DESC, contentRating DESC");
		$dbCommand->limit($count);
		return $dbCommand->queryAll();
	}

	/**
	 * Gets all experts of selected asset
	 * @param integer $idContent
	 * @return array of user's models
	 */
	public static function getContentExperts($idContent) {
		$contentTopics = App7020ChannelAssets::getChannelsByContent($idContent); //App7020TopicContent::getTopicsByContent($idContent);
		$allExperts = App7020ChannelExperts::getAllExperts();//App7020TopicExpert::getAllExperts();
		$returnArray = array();
		foreach ($allExperts as $expert) {
			$expertTopics = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($expert['idUser']); //App7020TopicExpert::getTopicsByUser($expert['idUser']);
			if (count(array_intersect($expertTopics, $contentTopics)) > 0) {
				$returnArray[] = CoreUser::model()->findByPk($expert['idUser']);
			}
		}
		return $returnArray;
	}

	/**
	 * Check if current user is expert for content
	 * @param int $idContent
	 * @return bool
	 */
	public static function isExpert($idContent) {
		$experts = self::getContentExperts($idContent);
		$expertId = array();
		foreach ($experts as $expert) {
			$expertId[] = $expert->idst;
		}
		$isExpert = in_array(Yii::app()->user->getId(), $expertId);
		return $isExpert;
	}

	/**
	 * Return boolean answer for access of current user for answer of questions by asset
	 * @param App7020Content $content
	 * @return boolean
	 */
	static function getAccessForAnswerByInstance(App7020Content $content) {
		if ($content->isContributor) {
			if ($content->peer_review_settings->enableCustomSettings && $content->peer_review_settings->allowContributorAnswer) {
				return true;
			} else {
				return false;
			}
		} else if (Yii::app()->user->isGodAdmin) {
			return true;
		} else if ($content->hasReviewsAccess()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return boolean answer for access of current user for answer of questions by asset
	 * @param integer $content
	 * @return boolean
	 */
	static function getAccessForAnswerById($contentId) {
		try {
			$content = App7020Content::model()->findByPk($contentId);
			if (empty($content)) {
				return false;
			}
			if ($content->isContributor) {
				if ($content->peer_review_settings->enableCustomSettings && $content->peer_review_settings->allowContributorAnswer) {
					return true;
				} else {
					return false;
				}
			} else if (Yii::app()->user->isGodAdmin) {
				return true;
			} else if ($content->hasReviewsAccess()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

}
