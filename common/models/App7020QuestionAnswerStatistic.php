<?php

/**
 * Description of App7020QuestionAnswerStatistic
 *
 * @author vbelev
 */
class App7020QuestionAnswerStatistic extends CModel {

	/**
	 * Timeframe for query to get "Coaching Activity". The value is number of days ago from now
	 * @var array 
	 */
	const TIMEFRAME_DAILY = 1;
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;

	public function attributeNames() {
		return;
	}

	/**
	 * Get some statistics for Answers:
	 * 	1. Questions I have answered
	 * 	2. My answers likes
	 * 	3. My answers dislikes
	 * 	4. My answers mareked as "Best Answer"
	 * @param int $timeInterval - interval of days ago from now
	 * @return array - ['answeredQuestions' => 5, 'likes' => 45, 'dislikes' => 12, 'bestAnswers' => 23]
	 */
	public static function answersBaseStats($timeInterval, $currentUser) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("
			idQuestion,
			SUM(CASE WHEN al.type = " . App7020AnswerLike::TYPE_LIKE . " THEN 1 ELSE 0 END) count_likes,
			SUM(CASE WHEN al.type = " . App7020AnswerLike::TYPE_DISLIKE . " THEN 1 ELSE 0 END) count_dislikes,
			SUM(CASE WHEN a.bestAnswer = " . App7020Answer::BEST_ANSWER . " THEN 1 ELSE 0 END) count_bestAnswers
		");
			$commandBase->from(App7020Answer::model()->tableName() . ' a');
			$commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' al', 'a.id = al.idAnswer');
			$commandBase->andWhere("a.idUser = :idUser", array(':idUser' => $currentUser ) );
			$commandBase->andWhere('a.created BETWEEN (NOW() - INTERVAL ' . $timeInterval . ' DAY) AND NOW()');
			$commandBase->group('a.idQuestion');
  			$resultArray = $commandBase->queryAll();

			$finaleResult = array(
				'idst' => $currentUser,
				'answeredQuestions' => count($resultArray),
				'likes' => App7020Helpers::arrSumByKey($resultArray, 'count_likes'),
				'dislikes' => App7020Helpers::arrSumByKey($resultArray, 'count_dislikes'),
				'bestAnswers' => App7020Helpers::arrSumByKey($resultArray, 'count_bestAnswers'),
			);
			return $finaleResult;
		} else {
			return array();
		}
	}

	/**
	 * Statistic for Requests that current user has satisfied 
	 * @param int $timeInterval
	 * @return boolean/int
	 */
	public static function requestsIsatisfy($timeInterval, $currentUser) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("count(*) as my_satis_requests");
			$commandBase->from(App7020QuestionRequest::model()->tableName());
			$commandBase->join(App7020Content::model()->tableName(), App7020Content::model()->tableName() . '.id = ' . App7020QuestionRequest::model()->tableName() . '.idContent');
			$commandBase->andWhere(App7020QuestionRequest::model()->tableName() . ".idExpert = " . $currentUser);
			$commandBase->andWhere(App7020Content::model()->tableName() . ".conversion_status = " . App7020Assets::CONVERSION_STATUS_APPROVED);
			$commandBase->andWhere('created BETWEEN (NOW() - INTERVAL ' . $timeInterval . ' DAY) AND NOW()');
			return $commandBase->queryScalar();
		} else {
			return false;
		}
	}

	/**
	 * Avarage answer time - it is a time for answer of all quetions for current user:
	 * I get the creation time of Quetion and subtract the time of FIRST Answer then calculate the Avarage
	 * @param type $timeInterval
	 * @return array - ['hours'=> 3 , 'minutes' => 38]
	 */
	public static function avgAnswerTime($timeInterval, $currentUser) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("FLOOR(AVG(UNIX_TIMESTAMP(t2.created_answer) - UNIX_TIMESTAMP(t1.created))/60) as avg_answer_time_minutes");
			$commandBase->from(App7020Question::model()->tableName() . ' t1');
			$commandBase->join("
				(
					SELECT idQuestion,
					MIN(created) as created_answer 
					FROM " . App7020Answer::model()->tableName() . " 
					GROUP BY idQuestion
				) as t2", "t2.idQuestion = t1.id
			");
			$commandBase->andWhere("t1.idUser = " . $currentUser);
			$commandBase->andWhere('t1.created BETWEEN (NOW() - INTERVAL ' . $timeInterval . ' DAY) AND NOW()');
			$minutes = $commandBase->queryScalar();
			return array(
				'hours' => floor($minutes / 60),
				'minutes' => ($minutes % 60)
			);
		} else {
			return array();
		}
	}

	/**
	 * Get all "Coaching Activity" statistics
	 * @param str $timeInterval - 'day', 'week', 'month', 'year'
	 * @return array/JSON
	 */
	public static function getAllCoachActivity($timeInterval, $currentUser, $json = true) {
		$chartData = self::chartMyActivityPerChannel($timeInterval, $currentUser);
		$result = array_merge(
				self::answersBaseStats($timeInterval, $currentUser), array('requestsIsatisfy' => self::requestsIsatisfy($timeInterval, $currentUser)), array('avgAnswerTime' => self::avgAnswerTime($timeInterval, $currentUser)), array('chartAnsweredQuestions' => self::chartAnsweredQuestions($timeInterval, $currentUser)), array('expertsByAnswersQuality' => self::expertsByAnswersQuality($currentUser)), array('expertsByFirstToAnswer' => self::topExpertsByAnswer(true, $currentUser)), array('expertsBypartecipationRate' => self::topExpertsByAnswer(false, $currentUser)), array('chartMyActivityPerChannel' => self::chartMyActivityPerChannel($timeInterval, $currentUser)), array('chartMyActivityPerChannelHTML' => Yii::app()->getController()->widget('common.widgets.app7020ActivityChannels', array('data' => $chartData), true))
		);
		if ($json) {
			json_encode($result);
		}
		return $result;
	}

	/**
	 * Data for chart "Question I have answered" in page "Coaching Activity"
	 * @param int $timeInterval
	 * @return array
	 */
	public static function chartAnsweredQuestions($timeInterval, $currentUser) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			// If query period is for 1 year ago to now
			if ($timeInterval == 365) {
				$commandBase->select("DATE_FORMAT(created, '%Y-%m') AS period, COUNT(DISTINCT idQuestion) AS count");
				$groupBy = "DATE_FORMAT(created, '%m')";
			} else {
				$commandBase->select("DATE_FORMAT(created, '%c-%d') AS period, COUNT(DISTINCT idQuestion) AS count");
				$groupBy = "DATE(created)";
			}
			$commandBase->from(App7020Answer::model()->tableName());
			$commandBase->andWhere(App7020Answer::model()->tableName() . ".idUser = " . $currentUser);
			$commandBase->andWhere('created BETWEEN (NOW() - INTERVAL ' . $timeInterval . ' DAY) AND NOW()');
			$commandBase->group($groupBy);
			$commandBase->order('created');
			$resultArray = array(
				'labels' => array(),
				'points' => array()
			);
			if ($timeInterval == 365) {
				$months = array();
				for ($i = 0; $i <= 12; $i++) {
					$months[] = date('Y-m', strtotime(date('Y-m') . " -$i months"));
				}
				$months = array_reverse($months);
				$count = count($commandBase->queryAll(true));
				foreach ($months as $month) {
					if (count($commandBase->queryAll(true)) > 0) {
						foreach ($commandBase->queryAll(true) as $answer) {
							if ($answer['period'] == $month) {
								$resultArray['labels'][] = $answer['period'];
								$resultArray['points'][] = $answer['count'];
								$count--;
							} else {
								if (!in_array($month, $resultArray['labels']) && !in_array($answer['period'], $resultArray['labels']) && $count > 0) {
									$resultArray['labels'][] = $month;
									$resultArray['points'][] = 0;
								} else if (!in_array($month, $resultArray['labels']) && $count == 0) {
									$resultArray['labels'][] = $month;
									$resultArray['points'][] = 0;
								}
							}
						}
					} else {
						$resultArray['labels'][] = $month;
						$resultArray['points'][] = 0;
					}
				}
			} else {
				$days = array();
				for ($i = 0; $i <= $timeInterval; $i++) {
					$days[] = date('n-d', strtotime("-$i day"));
				}
				$days = array_reverse($days);
				$count = count($commandBase->queryAll(true));
				foreach ($days as $day) {
					if (count($commandBase->queryAll(true)) > 0) {
						foreach ($commandBase->queryAll(true) as $answer) {
							if ($answer['period'] == "$day") {
								array_push($resultArray['labels'], $answer['period']);
								array_push($resultArray['points'], $answer['count']);
								$count--;
							} else {
								if (!in_array($day, $resultArray['labels']) && !in_array($answer['period'], $resultArray['labels']) && $count > 0) {
									array_push($resultArray['labels'], $day);
									array_push($resultArray['points'], 0);
								} else if (!in_array($day, $resultArray['labels']) && $count == 0) {
									array_push($resultArray['labels'], $day);
									array_push($resultArray['points'], 0);
								}
							}
						}
					} else {
						array_push($resultArray['labels'], $day);
						array_push($resultArray['points'], 0);
					}
				}
			}
			return $resultArray;
		} else {
			return array();
		}
	}

	/**
	 * Data for Rating list 1 - "Top 5 experts by answers quality" in page "Coaching Activity"
	 * @return array
	 */
	public static function expertsByAnswersQuality($currentUser) {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		//$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
					idst, 
					firstname,
					lastname,
					CONCAT(firstname, ' ', lastname) as expert_name,
					TRIM(LEADING '/' FROM userid) AS username,
					avatar,
					SUM(CASE WHEN app7020_answer_like.type = " . App7020AnswerLike::TYPE_LIKE . " THEN 1 ELSE 0 END) count_likes,
					SUM(CASE WHEN app7020_answer_like.type = " . App7020AnswerLike::TYPE_DISLIKE . " THEN 1 ELSE 0 END) count_dislikes,
					SUM(CASE WHEN app7020_answer.bestAnswer = " . App7020Answer::BEST_ANSWER . " THEN 1 ELSE 0 END) count_bestAnswers
			");
		$commandBase->from(CoreUser::model()->tableName() . ' core_user');
		$commandBase->join(App7020Answer::model()->tableName() . ' app7020_answer', 'app7020_answer.idUser = core_user.idst');
		$commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' app7020_answer_like', 'app7020_answer_like.idAnswer = app7020_answer.id');
		$commandBase->group('idst');
		$commandBase->order('count_bestAnswers DESC, count_likes DESC');

		$resultArray = array();
		foreach ($commandBase->queryAll(true) as $result) {
			$avatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$avatardb = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$resultArray[$result['idst']] = array(
				'idst' => $result['idst'],
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'expert_name' => trim($result['expert_name']),
				'username' => $result['username'],
				'avatar' => $avatar,
				'avatar_db' => $avatardb,
				'count_likes' => $result['count_likes'],
				'count_dislikes' => $result['count_dislikes'],
				'count_bestAnswers' => $result['count_bestAnswers']
			);
		}
		$keyCurrent = array_search($currentUser, array_keys($resultArray));
		$resultArray = array_values($resultArray);
		return self::orderArrayForRanking($keyCurrent, $resultArray);
	}

	/**
	 * Data for:
	 * 1. Rating list 2 - "Top 5 experts by first to answer rate"
	 * 2. Rating list 3 - "Top 5 experts by participation rate" (how many questions I've answered in TOTAL)
	 * @param bool $byFirst = provide "false" to get "Top 5 experts by participation rate"
	 * @return array
	 */
	public static function topExpertsByAnswer($byFirst = true, $currentUser) {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
//		$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
				idst, 
				firstname,
				lastname,
				CONCAT(firstname, ' ', lastname) as expert_name,
				TRIM(LEADING '/' FROM userid) AS username,
				avatar,
				count(*) as count_answers
			");
		$commandBase->from(CoreUser::model()->tableName() . ' cu');
		if ($byFirst) {
			// Top 5 experts by first to answer rate
			$commandBase->join(
					"(
						SELECT
						   idUser,
						   idQuestion,
						   created as created_answer,
						   MIN(id) as id,
						   created
						   FROM " . App7020Answer::model()->tableName() . " 
						   GROUP BY idQuestion
					) as ans", "ans.idUser = cu.idst"
			);
		} else {
			// Top 5 experts by participation rate (how many questions I've answered in TOTAL)
			$commandBase->join(App7020Answer::model()->tableName() . ' ans', 'ans.idUser = cu.idst');
		}
		$commandBase->group('idst');
		$commandBase->order('count_answers DESC');

		$resultArray = array();
		foreach ($commandBase->queryAll(true) as $result) {
			$avatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$avatardb = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$resultArray[$result['idst']] = array(
				'idst' => $result['idst'],
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'expert_name' => trim($result['expert_name']),
				'username' => $result['username'],
				'avatar' => $avatar,
				'avatar_db' => $avatardb,
				'count_answers' => $result['count_answers']
			);
		}

		$keyCurrent = array_search($currentUser, array_keys($resultArray));
		$resultArray = array_values($resultArray);
		return self::orderArrayForRanking($keyCurrent, $resultArray);
	}

	public static function orderArrayForRanking($key, $array) {
		if ($key == 0) {
			$users[$key] = $array[$key];
			$users[$key + 1] = $array[$key + 1];
			$users[$key + 2] = $array[$key + 2];
			return $users;
		} else {
			$users[$key - 1] = $array[$key - 1];
			$users[$key] = $array[$key];
			$users[$key + 1] = $array[$key + 1];
			return $users;
		}
	}

	/**
	 * Data for Chart 2 - "My Activity per channel" - it should be sum of all my questions to given channel and calculate percenatge
	 * @param type $timeInterval
	 * @return array
	 */
	public static function chartMyActivityPerChannel($timeInterval, $currentUser) {
		if (!$timeInterval)
			$timeInterval = 30;
		if (is_int($timeInterval)) {
			// Default (platform) language ('en')
			$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
			// Resolve requested language
			$language = Yii::app()->getLanguage();

			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select('
				trans.name AS channel_trans_name,
				transDef.name AS channel_transDef_name,
				count(DISTINCT a.idQuestion) AS count_questions,
				ch.id AS channel_id,
				ch.icon_bgr AS icon_bgr,
				ch.icon_color AS icon_color,
				q.id AS question_id,
				q.idUser AS question_idUser,
				q.title AS question_title
				');
			$commandBase->from(App7020Answer::model()->tableName() . ' a');
			$commandBase->join(App7020Question::model()->tableName() . ' q', 'q.id = a.idQuestion');
			$commandBase->leftJoin(App7020ChannelQuestions::model()->tableName() . ' cq', 'cq.idQuestion = q.id');
			$commandBase->leftJoin(App7020Content::model()->tableName() . ' c', 'c.id = q.idContent');
			$commandBase->leftJoin(App7020ChannelAssets::model()->tableName() . ' ca', 'ca.idAsset = c.id AND ca.asset_type=1');
			$commandBase->join(App7020Channels::model()->tableName() . ' ch', 'ch.id = ca.idChannel OR ch.id = cq.idChannel');
			$commandBase->leftJoin(App7020ChannelTranslation::model()->tableName() . ' trans', 'trans.idChannel = ch.id AND trans.lang = :language');
			$commandBase->leftJoin(App7020ChannelTranslation::model()->tableName() . ' transDef', 'transDef.idChannel = ch.id AND transDef.lang = :languageDef');
			$commandBase->andWhere('a.idUser = :currenUser');
			$commandBase->andWhere('a.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()');
			$commandBase->group('ch.id');
			$commandBase->order('count_questions DESC');

			$params = array(
				':language' => $language,
				':languageDef' => $defaultLanguage,
				':currenUser' => $currentUser,
				':timeInterval' => $timeInterval
			);

			$results = $commandBase->queryAll(true, $params);
			// Get Count of all Questions to calculate percenrage of every channel
			$sumAllQuestions = App7020Helpers::arrSumByKey($results, 'count_questions');

			$resultArray = array();
			if (count($results) > 0) {
				foreach ($results as $result) {
					if (!$result['color'])
						$result['color'] = '#030303';
					if (!$result['icon_bgr'])
						$result['icon_bgr'] = '#030303';
					if (!$result['text_color'])
						$result['text_color'] = '#ffffff';
					$resultArray[] = array(
						'label' => mb_strlen($result['channel_trans_name']) ? $result['channel_trans_name'] : $result['channel_transDef_name'],
						'value' => round(($result['count_questions'] / $sumAllQuestions) * 100),
						'color' => $result['icon_bgr'],
						'highlight' => $result['icon_bgr'],
						'text_color' => $result['icon_color']
					);
				}
			} else {
				$resultArray[] = array(
					'label' => Yii::t('app7020', 'No data available'),
					'value' => 0.1,
					'color' => '#030303',
					'highlight' => '#030303',
					'text_color' => '#ffffff'
				);
			}

			return $resultArray;
		} else {
			return array();
		}
	}

}
