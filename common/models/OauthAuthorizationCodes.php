<?php

/**
 * This is the model class for table "oauth_authorization_codes".
 *
 * The followings are the available columns in table 'oauth_authorization_codes':
 * @property string $authorization_code
 * @property string $client_id
 * @property string $user_id
 * @property string $redirect_uri
 * @property string $expires
 * @property string $scope
 */
class OauthAuthorizationCodes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oauth_authorization_codes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('authorization_code, client_id, expires', 'required'),
			array('authorization_code', 'length', 'max'=>40),
			array('client_id', 'length', 'max'=>80),
			array('user_id', 'length', 'max'=>255),
			array('redirect_uri, scope', 'length', 'max'=>2000),
			array('authorization_code, client_id, user_id, redirect_uri, expires, scope', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'authorization_code' => 'Authorization Code',
			'client_id' => 'Client',
			'user_id' => 'User',
			'redirect_uri' => 'Redirect Uri',
			'expires' => 'Expires',
			'scope' => 'Scope',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('authorization_code',$this->authorization_code,true);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('redirect_uri',$this->redirect_uri,true);
		$criteria->compare('expires',$this->expires,true);
		$criteria->compare('scope',$this->scope,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OauthAuthorizationCodes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
