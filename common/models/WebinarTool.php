<?php
/**
 * Generic superclass for videoconference tools integrated with
 * the Docebo LMS
 */
abstract class WebinarTool {

	public static $webinarTools = [
		'AdobeConnectApp',
		'BigbluebuttonApp',
		'DoceboWebconferenceApp',
		'GotomeetingApp',
		'OnsyncApp',
		'TeleskillApp',
		'VivochaApp',
		'WebexApp',
		'SkymeetingApp',
		'ViaApp'
	];

	/**
	 * @var string The name of the tool (e.g. Custom, GotoMeeting, Webex Event..)
	 */
	protected $name;

	/**
	 * @var string The identifier of the tool (e.g. 'custom', 'webex_meeting', 'gotomeeting',..)
	 */
	protected $id;

	/**
	 * @var WebinarToolAccount[] Array of accounts for this tool
	 */
	protected $accounts;

	/**
	 * @var bool Whether the joining url must be directly generated into course page or not
	 */
	protected $directJoin = true;

	/**
	 * @var array Array of singleton instances for the different tool handlers
	 */
	private static $_instances = array();

	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call or automatically identify a viewing URL?
	 * IMPORTANT: DO NOT SET BOTH TO TRUE FOR A SINGLE WEBINAR TOOL. $apiFindRecording is used as a 'Plan B' if API grab isn't possible
	 */
	protected $apiGrabSupported = false;
	protected $apiFindRecording = false;


	const PLAY_RECORDING_TYPE_DIALOG = 'dialog';
	const PLAY_RECORDING_TYPE_EXTERNAL = 'external';

	/**
	 * @var bool specify some policies about how to play webinar recording videos.
	 * Possible values can be constants PLAY_RECORDING_TYPE_* .
	 * If value is set to FALSE or an empty value, then the default options will be used (playing in dialog player).
	 */
	protected $playRecordingType = false;


	/**
	 * @var bool tell the system if this webinar tool is able to handle timezones with DST when creating/updating sessions.
	 * If tool cannot handle it, then the system will provide dateimes already adjusted.
	 * To be overloaded in subclasses if a different value is needed.
	 */
	protected $_canHandleDST = false;


	/**
	 * Protected constructor to implement the singleton pattern
	 */
	protected function __construct() {}

	/**
	 * Initializes the array of webinar tools
	 */
	private static function _initToolsArray() {
		// Add the dummy custom webinar tool
		self::$_instances['custom'] = new CustomWebinarTool();

		// Raise an event to collect all other webinar tools
		$event = new DEvent(new stdClass(), array());
		Yii::app()->event->raise('WebinarCollectTools', $event);
		$collectedTools = $event->return_value['tools'];
		if(is_array($collectedTools)) {
			foreach($collectedTools as $toolId => $toolClass)
				self::$_instances[$toolId] = new $toolClass();
		}
	}


	/**
	 * Tell if the tools handle DST timezones by itself or if it needs help from the system.
	 * NOTE: most tools just receive dates in UTC and do not handle DST shiftings by themselves.
	 * @return bool
	 */
	public function canHandleDST() {
		return $this->_canHandleDST;
	}


	/**
	 * Tell if the conference tool is directly joinable in the course or a intermediate LMS link is necessary
	 * @return bool
	 */
	public function isDirectJoin() {
		return $this->directJoin;
	}

	/**
	 * Returns whether or not grabbing a recording through an API call is possible for the current webinar tool
	 * @return bool
	 */
	public function allowApiGrabRecording(){
		return $this->apiGrabSupported;
	}

	/**
	 * Returns whether or not it is possible to fetch a link to a recording for the current webinar tool
	 * @return bool
	 */
	public function allowApiFindRecording(){
		return $this->apiFindRecording;
	}

	/**
	 * Return webinar recording videos playing policy.
	 * @return bool/string
	 */
	public function getPlayRecordingType() {
		return $this->playRecordingType;
	}

	/**
	 * It checks if is already set account for this webinar tool
	 * @param $toolName string The tool name
	 * @return boolean
	 */
	public static function isSetAccount($toolName)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'tool=:tool';
		$criteria->params = array(':tool' => $toolName);
		if (WebinarToolAccount::model()->exists($criteria)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $id string The identifier of the tool to instantiate
	 * @throws CHttpException
	 * @return WebinarTool
	 */
	public static function getById($id) {
		if(!$id)
			throw new CHttpException(500, "Missing webinar tool name");

		// Check if we should collect all the available webinar tools
		if(empty(self::$_instances))
			self::_initToolsArray();

		// Check if we have the requested event available
		if(isset(self::$_instances[$id]) && is_subclass_of(self::$_instances[$id],'WebinarTool'))
			return self::$_instances[$id];
		else
			throw new CHttpException(500, "Invalid webinar tool requested");
	}

	/**
	 * Returns the array of all available tools
	 */
	public static function getAllTools() {
		// Check if we should collect all the available webinar tools
		if(empty(self::$_instances))
			self::_initToolsArray();

		return self::$_instances;
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport() {
		return false;
	}

	/**
	 * Returns the array of accounts configured for the current tool indexed by id
	 * @return WebinarToolAccount[]
	 */
	public function getAccounts() {
		if(!$this->accounts) {
			$records = WebinarToolAccount::model()->findAllByAttributes(array('tool' => $this->id));
			$this->accounts = CHtml::listData($records, 'id_account', function($row){
				return $row;
			});
		}

		return $this->accounts;
	}

	/**
	 * Returns the account identified by the current id
	 * @param $id
	 *
	 * @return WebinarToolAccount
	 */
	public final function getAccountById($id, $skip_error = false) {
		$accounts = $this->getAccounts();
		if(isset($accounts[$id]))
			return $accounts[$id];
		elseif(!$skip_error)
			throw new CHttpException(500, "Invalid webinar account requested");
		else
		{
			Yii::log('Invalid webinar account requested with id: '.$id, CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Returns true if the current tools supports automatic download of webinar recordings
	 * @return bool
	 */
	public function hasRecordingDownloadSupport() {
		return false;
	}

	/**
	 * Downloads the recording file to be processed
	 *
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return string The filename of the downloaded recording
	 */
	public function getRecording($idAccount, $api_params = array()) {
		if($this->hasRecordingDownloadSupport()) {
			// TODO: Implement it
		} else
			throw new CHttpException(500, "Recording download not available for the curren tool");
	}

	/**
	 * Renders the HTML for the settings of the current tool (if any)
	 *
	 * @param $api_params
	 *
	 * @return The html to embed in the webinar form
	 */
	public function getCustomSettingsHTML($api_params) {
		return '';
	}

	/**
	 * Returns an array of "localized" room info to be displayed somewhere (e.g. in the New webinar session notification)
	 * @param integer $idAccount
	 * @param array $api_params
	 *
	 * @return array
	 */
	public function getAttendeeInfoForRoom($idAccount, $api_params = array()) {
		return array();
	}

	/**
	 * Creates a new room using the current tool api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 *
	 * @return string
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 *
	 * @return string
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * @param int $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return file downloaded to tmp upload folder
	 */
	public function getApiRecordings($idAccount, $api_params){
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * @param int $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return string viewing URL for embedding
	 */
	public function retrieveRecordingLink($idAccount, $api_params){
		throw new CHttpException(500, 'Not implemented');
	}

	/**
	 * Convert a string replacing whitespaces to "_"  (slugify)
	 *
	 * @param string $str
	 * @return string
	 */
	protected function slugify($str) {
		return preg_replace("/[^\w\.]/", "_", trim(strtolower($str)));
	}

	/**
	 * Generate random string of an arbitrary length
	 *
	 * @param int $length
	 *
	 * @return string
	 */
	protected function randomString($length = 12) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	/**
	 * Returns the username to display in the webinar
	 * @param $userModel
	 */
	protected function getUserDisplayName($userModel) {
		// Resolve Meeting User display name
		$userDisplayName = trim($userModel->firstname . " " . $userModel->lastname);
		$userDisplayName = !empty($userDisplayName) ? $userDisplayName : "User " . $userModel->idst;
		return $userDisplayName;
	}

	/**
	 * Get tool ID
	 * @return string
	 */
	public function getId()	{
		return $this->id;
	}

	/**
	 * Get tool Name
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Check if the user is valid for current tool
	 * @param $user user IDST
	 * @return array validation result
	 */
	public function validateUser($user) {
		return array('valid' => true, 'message' => '');
	}
}