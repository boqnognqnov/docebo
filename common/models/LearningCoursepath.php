<?php

/**
 * This is the model class for table "learning_coursepath".
 *
 * The followings are the available columns in table 'learning_coursepath':
 * @property integer $id_path
 * @property string $path_code
 * @property string $path_name
 * @property string $path_descr
 * @property integer $subscribe_method
 * @property integer $img
 * @property boolean $is_selling
 * @property integer $purchase_type
 * @property float $price
 * @property boolean $visible_in_catalog
 * @property integer $days_valid
 * @property integer $days_valid_type
 * @property integer $catchup_enabled
 * @property integer $catchup_limit
 * @property integer $deep_link
 *
 * The followings are the available model relations:
 * @property LearningCoursepathCourses[] $learningCoursepathCourses
 * @property LearningCourse[] $learningCourse
 * @property CoreUser[] $coreUsers
 * @property LearningCertificateCoursepath $certificate
 * @property CertificationItem $certification
 */
class LearningCoursepath extends CActiveRecord
{

	public $catchup_limit_tmp; //used to store checkbox value from the form

	// Possible column values for "purchase_type" column
	const PURCHASE_TYPE_FULL_PACK_ONLY = 1;
	const PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE = 2;

	const DAYS_VALID_TYPE_FIRST_COURSE_ACCESS = 0;
	const DAYS_VALID_TYPE_BEING_ENROLLED = 1;



	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCoursepath the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_coursepath';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('path_name, path_descr', 'required'),
			array('subscribe_method, img, is_selling, purchase_type, visible_in_catalog, deep_link', 'numerical', 'integerOnly' => true),
			array('price, days_valid, days_valid_type', 'numerical'),
			array('path_code, path_name, credits', 'length', 'max' => 255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('catchup_enabled, catchup_limit, catchup_limit_tmp, deep_link', 'safe'),
			array('id_path, path_code, path_name, path_descr, subscribe_method, create_date, deep_link', 'safe', 'on' => 'search'),
		);

		// Let plugin add their own validation rules
		Yii::app()->event->raise('LearningPlanModelValidationRules', new DEvent($this, array('rules' => &$rules)));

		return $rules;
	}


	public function behaviors()
	{
		$behaviors = array_merge(parent::behaviors(), array(
			'modelSelect' => array('class' => 'ModelSelectBehavior'),
		));
		return $behaviors;
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningCoursepathCourses' => array(self::HAS_MANY, 'LearningCoursepathCourses', 'id_path'),
			'coreUsers' => array(self::MANY_MANY, 'CoreUser', 'learning_coursepath_user(id_path, idUser)'),
			'certificate' => array(self::HAS_ONE, 'LearningCertificateCoursepath', array('id_path' => 'id_path')),
			'certification' => array(self::HAS_ONE, 'CertificationItem', 'id_item', 'on' => 'certification.item_type=:itemTypeLP', 'params' => array(':itemTypeLP' => CertificationItem::TYPE_LEARNING_PLAN), 'together' => true),
			'learningCourse' => array(self::MANY_MANY, 'LearningCourse', 'learning_coursepath_courses(id_path, id_item)'),
			'coursesCount' => array(self::STAT, 'LearningCourse', 'learning_coursepath_courses(id_path, id_item)'),
			'powerUserManagers' => array(self::MANY_MANY, 'CoreUser', 'core_user_pu_coursepath(path_id, puser_id)'),
			'coreUserPuCoursepath' => array(self::HAS_MANY, 'CoreUserPuCoursepath', 'path_id'),
			'learningCatalogueEntry' => array(self::HAS_MANY, 'LearningCatalogueEntry', 'idEntry', 'condition' => 'type_of_entry = "coursepath"'),
		);
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_path' => 'Id',
            'path_code' => Yii::t('standard', '_CODE'),
            'path_name' => Yii::t('standard', '_NAME'),
            'path_descr' => Yii::t('standard', '_DESCRIPTION'),
            'subscribe_method' => Yii::t('course', '_SUBSCRIBE_METHOD'),
            'create_date' => Yii::t('report', '_CREATION_DATE'),
            'credits' => Yii::t('standard', '_CREDITS'),
            'days_valid' => Yii::t('course', '_DAY_OF_VALIDITY'),
            'catchup_enabled' => Yii::t('coursepath', 'Enable catch-up courses for this learning plan'),
            'deep_link' => Yii::t('coursepath', 'Enable deep link for this learning plan'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_path', $this->id_path);
        $criteria->compare('path_code', $this->path_code, true);
        $criteria->compare('path_name', $this->path_name, true);
        $criteria->compare('path_descr', $this->path_descr, true);
        //$criteria->compare('deep_link', $this->deep_link, true);

        //$criteria->compare('subscribe_method',$this->subscribe_method);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }

    public function dataProvider($excludedPaths = array(), $showAll = true)
    {
        $criteria = new CDbCriteria;

        if (!empty($excludedPaths)) {
            $criteria->addNotInCondition('id_path', $excludedPaths);
        }

        $learning_coursepath = Yii::app()->request->getParam('LearningCoursepath');

        if (isset($learning_coursepath['path_name']) && $learning_coursepath['path_name'] !== '') {
            $criteria->addCondition('(t.path_name like :filter OR t.path_code like :filter)');
            $criteria->params[':filter'] = '%' . $learning_coursepath['path_name'] . '%';
        }

        switch ($this->scenario) {
            case 'curricula_management': {
                if (empty($this->powerUserManager)) break; //apply the filter only if we are using a power user
            }
            case 'power_user_management':
            case 'power_user_assigned':
                $criteria->with['powerUserManagers'] = array(
                    'joinType' => 'INNER JOIN',
                    'condition' => 'powerUserManagers.idst=:puser_id',
                    'params' => array(':puser_id' => $this->powerUserManager->idst),
                );
                $criteria->together = true;
                $criteria->group = 't.id_path';
                break;
            case 'power_user_assign':
                $criteria->with['coreUserPuCoursepath'] = array(
                    'joinType' => 'LEFT JOIN',
                    'on' => 'coreUserPuCoursepath.puser_id = :puser_id',
                    'params' => array(':puser_id' => $this->powerUserManager->idst),
                );
                $criteria->together = true;
                if ($showAll) {
                    $criteria->addCondition('coreUserPuCoursepath.path_id IS NULL');
                }
                break;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            ),
        ));
    }

    public function beforeValidate(){
        $validate = true;
        $event = new DEvent($this, array('course' => $this, 'validate' => &$validate));
        Yii::app()->event->raise('beforeValidateCoursepath', $event);
        if($event->return_value)
            return $validate;

        return parent::beforeValidate();
    }


    public function beforeSave()
    {
        if (empty($this->create_date)) {
            $this->create_date = Yii::app()->localtime->getUTCNow();
        }

        //clear catchup_limit in case of disabled feature or not set limit from the UI
        if (!$this->catchup_enabled || !$this->catchup_limit_tmp)
            $this->catchup_limit = 0;

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if ($this->certificate->isNewRecord) {
            $this->certificate->id_path = $this->id_path;
            $this->certificate->available_for_status = 2;
        }
        $this->certificate->save();

        if ($this->isNewRecord) {
            //it is a new LP created
            Yii::app()->event->raise(EventManager::EVENT_ADDED_LEARNING_COURSEPATH, new DEvent($this, array(
                'id_path'   => $this->id_path,
                'model'     => $this
            )));
        } else {
            //it is an update of a LP
            Yii::app()->event->raise(EventManager::EVENT_UPDATED_LEARNING_COURSEPATH, new DEvent($this, array(
                'id_path'   => $this->id_path,
                'model'     => $this
            )));
        }

        Yii::app()->event->raise(EventManager::EVENT_LEARNING_PLAN_AFTER_SAVE, new DEvent($this, array(
            'model' => $this
        )));

        return parent::afterSave();
    }

    public function beforeDelete()
    {
        Yii::app()->event->raise('BeforeCoursepathDeleted', new DEvent($this));
        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        $customReports = LearningReportFilter::model()->findAll();
        foreach ($customReports as $report) {
            $report->cleanUpCoursepaths($this->id_path);
        }
        //it is a LP deletion
        Yii::app()->event->raise(EventManager::EVENT_DELETED_LEARNING_COURSEPATH, new DEvent($this, array(
            'id_path'   => $this->id_path,
            'model'     => $this
        )));

		CoreNotificationAssoc::model()->deleteAllByAttributes(array('idItem' => $this->id_path, 'type' => CoreNotificationAssoc::NOTIF_ASSOC_PLAN));
        $core_enroll_rule_item = CoreEnrollRuleItem::model();
        if (!$core_enroll_rule_item->deleteAllByAttributes(array('item_type' => 'curricula', 'item_id' => $this->id_path)))
            return false;

        return true;
    }


    /**
     * Used in power user management for locations assignment
     * @var \CoreUser
     */
    public $powerUserManager = NULL;


    public function renderCourses()
    {
        $icon = count($this->learningCoursepathCourses) > 0 ? '<span class="assignCourses"></span>' : '<span class="assignCourses yellow">';

        return CHtml::link(count($this->learningCoursepathCourses) . $icon, Yii::app()->createUrl('CurriculaApp/curriculaManagement/courses', array('id' => $this->id_path)),
            array(
                'title' => Yii::t('standard', '_ASSIGN_COURSES'),
                'rel' => 'tooltip',
                'data-html' => true
            ));
    }

    public function renderUsers()
    {

        $cmd = Yii::app()->db->createCommand()
            ->select("COUNT(*) AS num_users")
            ->from(LearningCoursepathUser::model()->tableName() . " cpu");
        if (Yii::app()->user->isAdmin) {
            $cmd->join(
                CoreUserPU::model()->tableName() . " pu",
                "pu.user_id = cpu.idUser AND pu.puser_id = :puser_id",
                array(':puser_id' => Yii::app()->user->id)
            );
        }
        $cmd->where("cpu.id_path = :id_path", array(':id_path' => $this->getPrimaryKey()));
        $countUsers = $cmd->queryScalar();

        $icon = $countUsers > 0 ? '<span class="assignUsers"></span>' : '<span class="assignUsers yellow">';

        return CHtml::link($countUsers . $icon, Yii::app()->createUrl('CurriculaApp/curriculaManagement/users', array('id' => $this->id_path)),
            array(
                'title' => Yii::t('standard', 'Enroll users'),
                'rel' => 'tooltip',
                'data-html' => true
            ));
    }

    public function renderCatchUpCourses()
    {
        $iconCatchUp = count($this->learningCourseCourseCatchUp) > 0 ? '<span class="assignCatchUpCourses"></span>' : '<span class="assignCatchUpCourses yellow"></span>';
    }

    public function updateCourseSequence()
    {
        $criteria = new CDbCriteria();
        $criteria->compare('id_path', $this->id_path);
        $criteria->order = 'sequence';

        $courses = LearningCoursepathCourses::model()->findAll($criteria);
        if (!empty($courses)) {
            foreach ($courses as $i => $course) {
                $course->sequence = $i;
                $course->save();
            }
        }
    }

    public function removePrerequisites($ids)
    {
        if (!empty($this->learningCoursepathCourses)) {
            foreach ($this->learningCoursepathCourses as $course) {
                $prerequisites = $course->prerequisitesToArray();
                if (!empty($prerequisites)) {
                    foreach ($prerequisites as $i => $prerequisiteId) {
                        if (in_array($prerequisiteId, $ids)) {
                            unset($prerequisites[$i]);
                        }
                    }
                    $course->prerequisites = implode(',', $prerequisites);
                    $course->save();
                }
            }
        }
    }
    //Get (if completed) the completion date for a path (plan) and user
    public static function getCompletionDate($idUser, $idPath){
        $commandBase = Yii::app()->db->createCommand()
            ->from('learning_coursepath_courses planCourse')
            ->join('learning_courseuser courseUser', 'planCourse.id_item=courseUser.idCourse');
        $commandBase->andWhere("planCourse.id_path = ".$idPath." AND courseUser.idUser = ".$idUser." AND courseUser.date_complete IS NOT NULL");
        $commandBase->select('MAX(date_complete)');
        $result = $commandBase->query();
        $result->next();
        $result = $result->current();
        return $result['MAX(date_complete)'];
    }

    public function getCoursepathPercentage($idUser = false,$idPath = false)
    {
        $res = array();
        $total = 0;

        $criteria = new CDbCriteria();
        $criteria->with['learningCoursepathCourses'] = array(
            'joinType' => 'INNER JOIN',
        );
        $criteria->condition = 't.status = :status AND t.idUser = :idUser AND learningCoursepathCourses.id_path = :id_path';
        $criteria->params[':status'] = LearningCourseuser::$COURSE_USER_END;
        $criteria->params[':id_path'] = ($idPath)? $idPath : $this->id_path;
        $criteria->params[':idUser'] = ($idUser) ? $idUser : Yii::app()->user->id;
        $completed = LearningCourseuser::model()->count($criteria);


        if (!$total) {
            $criteria = new CDbCriteria();
            $criteria->condition = 't.id_path = :id_path';
            $criteria->params[':id_path'] = ($idPath)? $idPath : $this->id_path;
            $total = LearningCoursepathCourses::model()->count($criteria);
        }
        $res['percentage'] = ($completed == 0 ? 0 : round(($completed / $total) * 100, 0));
        $res['total'] = $total;
        $res['done'] = $completed;

        return $res;
    }


    /**
     * Returns the completion date of the learning plan by the user.
     * Internally this method checks for the last completed course in
     * the learning plan and gets that date/time.
     *
     * @param $learningPlanId
     * @param $userId
     *
     * @return bool|string
     */
    static public function getCoursepathCompletionDate($learningPlanId, $userId)
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('learningCoursepathCourses');
        $criteria->compare('learningCoursepathCourses.id_path', $learningPlanId);

        $criteria->compare('t.idUser', $userId);
        $criteria->compare('t.status', LearningCourseuser::$COURSE_USER_END);
        $criteria->order = 't.date_complete DESC';
        $lastCompletedCourse = LearningCourseuser::model()->find($criteria);

        return $lastCompletedCourse ? $lastCompletedCourse->date_complete : false;
    }


    /**
     * @return LearningCoursepathCourses[]
     */
    public function getCoursepathCourseDetails()
    {
        $criteria = new CDbCriteria();
        $criteria->with['learningCourse'] = array();
        $criteria->condition = 't.id_path = :id_path';
        $criteria->params[':id_path'] = $this->id_path;
        $criteria->group = 'learningCourse.idCourse';
        $criteria->order = 't.id_path, t.sequence';
        /*
                $criteria->with['learningCourseuser'] = array(
                    'select' => 'idCourse, status',
                );
                $criteria->with['learningCoursepathCourses'] = array(
                    'select' => 'prerequisites',
                );
                $criteria->condition = 'learningCoursepathCourses.id_path = :id_path AND learningCourseuser.idUser = :idUser';
                $criteria->params[':id_path'] = $this->id_path;
                $criteria->params[':idUser'] = Yii::app()->user->id;
                $criteria->group = 'learningCoursepathCourses.id_path, t.idCourse';
                $criteria->order = 'learningCoursepathCourses.id_path, learningCoursepathCourses.sequence';

                //$criteria->params[':idUser'] = Yii::app()->user->id; AND coreUsers_coreUsers.idUser = :idUser
        */

        $res = LearningCoursepathCourses::model()->findAll($criteria);
        return $res;
    }


    /**
     * Return list of user IDs assigned to this curricula
     *
     * @return array
     */
    public function getUsersIdList()
    {
        $models = $this->coreUsers;
        $result = array();
        if (empty($models))
            return $result;

        foreach ($models as $model) {
            $result[] = $model->idst;
        }
        return $result;
    }


    /**
     * Return list of users, enrolled to this LP
     */
    public function getEnrolledUsers($ignoreWaiting=true) {
        $command = Yii::app()->db->createCommand();
	    $command->select('idUser');
	    $command->from("learning_coursepath_user lpu");
	    $command->andWhere('id_path=:idPath');

	    if ($ignoreWaiting) {
	       $command->andWhere('waiting=0');
	    }

	    return $command->queryColumn(array(':idPath' => $this->id_path));
    }

	/**
	 * Get the user enrolled in at least one lp
	 * @param $users An array with the listo of the user to check
	 * @param $idCourse the numeric ID of a course
	 * @return array list of enrollments (array is empty if no LPs are found)
	 */
	public static function getEnrollments($users, $idCourse)
	{
		$query = "SELECT DISTINCT(cpu.idUser)"
			." FROM ".LearningCoursepathCourses::model()->tableName()." cpc "
			." JOIN ".LearningCoursepathUser::model()->tableName()." cpu "
			." ON (cpc.id_path = cpu.id_path AND cpc.id_item = :id_course)"
			." WHERE cpu.idUser IN (" . implode(",", $users) . ")";;

		return Yii::app()->db->createCommand($query)->queryColumn(array(':id_course' => $idCourse));
	}

    /**
     * Return list of course IDs assigned to this curricula
     *
     * @return array
     */
    public function getCoursesIdList()
    {
        $models = $this->learningCourse;
        $result = array();
        if (empty($models))
            return $result;

        foreach ($models as $model) {
            $result[] = $model->idCourse;
        }
        //get catchup courses
        $catchUps = LearningCourseCatchUp::model()->findAllByAttributes(array(
            'id_path' => $this->id_path
        ));
        foreach ($catchUps as $catchup)
            $result[] = $catchup->catchUpCourseModel->idCourse;

        return $result;
    }


    /**
     * Bulk Subscribe all users of THIS curricula to all courses of it
     *
     */
    public function subscribeUsersToCourses()
    {
        $users = $this->getUsersIdList();
        $courses = $this->getCoursesIdList();
        LearningCourseuser::subscribeUsersToCourses($users, $courses);
    }


    /**
     * Bulk UN-Subscribe all users of THIS curricula from all courses of it
     *
     */
    public function unSubscribeUsersFromCourses()
    {
        $users = $this->getUsersIdList();
        $courses = $this->getCoursesIdList();
        LearningCourseuser::unSubscribeUsersFromCourses($users, $courses);
    }

    /**
     * Subscribe SINGLE User to ALL curricula's courses
     *
     */
    public function subscribeSingleUserToCourses($idUser, $fields = array())
    {
        $users = array($idUser);
        $courses = $this->getCoursesIdList();

        // Just to make it clear what this parameter means:
        // TRUE will signal the subscription process down the path
        // that it is coming from learning plan MASS course enrollment
        $learningPlanContext = true;
        LearningCourseuser::subscribeUsersToCourses($users, $courses, false, $learningPlanContext, $fields);
    }

    /**
     * Subscribe SINGLE Course to ALL curricula's users
     *
     */
    public function subscribeUsersToSingleCourse($idCourse)
    {
        $users = $this->getUsersIdList();
        $courses = array($idCourse);
        LearningCourseuser::subscribeUsersToCourses($users, $courses);
    }

    /**
     * Count courses assigned to this learning plan
     * @return number
     */
    public function getCoursesNumber()
    {
        $count = Yii::app()->db->createCommand()
            ->select("COUNT(*)")
            ->from(LearningCoursepathCourses::model()->tableName() . " cpc")
            ->where("cpc.id_path = :id_path", array(':id_path' => $this->getPrimaryKey()))
            ->queryScalar();
        return (int)$count;
    }

    /**
     * An internal method used to retrieve shared and private (user specific)
     * images to be used in a carousel widget-like structure
     *
     * @param     $idPath
     *
     * @param int $gallerySize - How many thumbnails to display per carousel slide
     *
     * @return array with exactly 4 elements:
     * 1: Total images count
     * 2. Default images in the platform (also called "shared")
     * 3. Private images (uploaded by user)
     * 4. The current selected image. Possible values: default|user
     */
    static public function prepareThumbnails($idPath = null, $gallerySize = 10)
    {

        if ($idPath)
            $model = self::model()->findByPk($idPath);
        else
            $model = new LearningCoursepath();

        // Get list of URLs of SHARED and USER player backgrounds
        $assets = new CoreAsset();
        $defaultImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
        $userImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);

        $count = array(
            'default' => count($defaultImages),
            'user' => count($userImages),
        );

        $reorderArray = function (&$array, $key, $value) {
            unset($array[$key]);
            $array = array($key => $value) + $array;
        };

        foreach ($defaultImages as $key => $value) {
            if ($key == $model->img) {
                $selected = 'default';

                $reorderArray($defaultImages, $key, $value);
                break;
            }
        }

        if (empty($selected)) {
            foreach ($userImages as $key => $value) {
                if ($key == $model->img) {
                    $selected = 'user';
                    $reorderArray($userImages, $key, $value);
                    break;
                }
            }
        }

        $selected = empty($selected) ? 'default' : $selected;

        $defaultImages = array_chunk($defaultImages, $gallerySize, true);
        $userImages = array_chunk($userImages, $gallerySize, true);

        return array($count, $defaultImages, $userImages, $selected);
    }

    /**
     * Return the price of this LP based on the following condition:
     * 1. If LP full pack price is set, use it
     * 2. Else calculate the sum price of all courses in the LP
     *
     * @return int
     */
    public function getCoursepathPrice()
    {
        $sum = 0;

        if ($this->price) {
            $event = new DEvent($this, array('coursepath' => $this));
            Yii::app()->event->raise('OnCatalogGetCoursepathPrice', $event);

            if (isset($event->return_value['price'])) {
                $sum = $event->return_value['price'];
            } else {
                $sum = $this->price;
            }
        } else {
            foreach ($this->learningCourse as $course) {
                $sum = $sum + floatval($course->getCoursePrice());
            }
        }

        return $sum;
    }

    public function getLogo($variant = CoreAsset::VARIANT_ORIGINAL, $dashboard = false)
    {
        $arCheck = array('http:', 'https:');
        $arReplace = array('', '');

        $asset = CoreAsset::model()->findByPk($this->img);
        $url = null;
        if ($asset) {
            $url = $asset->getUrl($variant);
        } else {
            if (!$dashboard)
                $url = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
            else
                $url = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo230x230.png';
        }

        if ((stripos($url, 'http:') === 0) || (stripos($url, 'https:') === 0)) {
            $url = str_ireplace($arCheck, $arReplace, $url);
        }

        return $url;
    }

    /**
     * Render learning plan logo: helper function for grids
     *
     * @param bool|string $size
     * @return string
     */
    public function renderLogo($variant = CoreAsset::VARIANT_ORIGINAL)
    {
        $style = "";
        if ($variant == CoreAsset::VARIANT_MICRO)
            $style = "width: 30px;";

        $logoUrl = $this->getLogo($variant);
        $content = CHtml::image($logoUrl, 'Logo', array('style' => $style));
        return $content;
    }


    /**
     * Reset tracking for a given learning plan and user.
     * Effectively resetting data for courses of the learning plan for the passed user.
     *
     * @param integer $idPath
     * @param integer $idUser
     */
    public static function reset($idPath, $idUser)
    {
        $model = self::model()->findByPk($idPath);
        $coursesIds = $model->getCoursesIdList();
        foreach ($coursesIds as $idCourse) {
            LearningCourseuser::reset($idCourse, $idUser);
        }
    }


    /**
     * return course path name in format "course_name - code" or "course_name" (if the code is empty).
     * used in courseManagement/courseAutocomplete
     * @return string
     */
    public function getCoursepathNameFormatted()
    {
        if (!empty($this->path_code))
            return $this->path_name . ' - ' . $this->path_code;
        else
            return $this->path_name;
    }


    /**
     * Return some useful statistics about Learning Plan + Courses inside + User
     *    - totalCourses
     *  - nonStartedCourses
     *  - completedCourses
     *
     * @param integer $idUser
     * @return array
     */
    public function getUserCoursesStats($idUser)
    {
        $command = Yii::app()->getDb()->createCommand()
            ->select('lp.id_path, lp.path_name, lp.img, COUNT(cpc.id_item) AS totalCourses, SUM(cu.status=0) AS nonStartedCourses, SUM(cu.status=2) AS completedCourses')
            ->from($this->tableName() . ' lp')
            ->leftJoin(LearningCoursepathCourses::model()->tableName() . ' cpc', 'lp.id_path=cpc.id_path')
            ->leftJoin(LearningCourseuser::model()->tableName() . ' cu', 'cpc.id_item=cu.idCourse AND cu.idUser=:idUser', array(':idUser' => $idUser))
            ->where('lp.id_path=:id_path', array(':id_path' => $this->id_path))
            ->group('lp.id_path');
        $lp = $command->queryAll(true);
        if (is_array($lp) && !empty($lp)) {
            return $lp[0];
        }
        return array();
    }


    /**
     * Resolve Learning plan access/policy etc. for My Dashboard: Catalog and My Learning plans
     *
     * Return an array of data for the presentation layer in the learning plan tile
     *
     * @param integer $idUser
     *
     * @return array
     */
    public function resolvePlanAccess($idUser = false)
    {

        if (!$idUser)
            $idUser = Yii::app()->user->id;

        $canPlay = false;
        $canBuy = false;
        $canEnroll = false;
        $tags = array();
        $linkTag = false;


        $playUrl = Docebo::createLmsUrl("curricula/show", array("id_path" => $this->id_path));
        $enrollUrl = Docebo::createLmsUrl('coursepath/details', array("id_path" => $this->id_path));
        $buyUrl = Docebo::createLmsUrl('coursepath/details', array("id_path" => $this->id_path));

        $playLinkTag = "<a href='$playUrl'>";
        $enrollLinkTag = "<a href='$enrollUrl'>";
        $buyLinkTag = "<a href='$buyUrl'>";

        $lpsOfUser = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser($idUser); // method uses caching

        // If user is subscribed to the Plan...
        if (in_array($this->id_path, $lpsOfUser)) {

            $stats = $this->getUserCoursesStats($idUser);

            if ($stats['totalCourses'] > 0 && $stats['totalCourses'] == $stats['completedCourses']) {
                $isCompleted = true;
                $action = array(
                    'label' => Yii::t('standard', '_COMPLETED'),
                    'icon' => "fa fa-check-circle-o",
                    'class' => "completed",
                );
                $linkTag = $playLinkTag;
            } else {
                $canPlay = true;
                $action = array(
                    'label' => Yii::t('standard', '_PLAY'),
                    'icon' => "fa fa-play-circle-o",
                    'class' => "play",
                );
                $linkTag = $playLinkTag;
            }

            $isNew = $stats['totalCourses'] > 0 && $stats['totalCourses'] == $stats['nonStartedCourses'];
            if ($isNew) {
                $tags[] = array(
                    'label' => strtoupper(Yii::t('standard', '_NEW')),
                    'class' => 'tag-new',
                );
            }

        } // Else, if the Plan is PAID
        else if ($this->is_selling) {
            $canBuy = true;
            $action = array(
                'label' => Yii::t('order', 'Buy'),
                'icon' => "fa fa-shopping-cart",
                'class' => "enroll buy",
            );
            $linkTag = $buyLinkTag;
        } else {
            $canEnroll = true;
            $action = array(
                'label' => Yii::t('standard', 'Enroll'),
                'icon' => "fa fa-plus",
                'class' => "enroll",
            );
            $linkTag = $enrollLinkTag;
        }

        $result = array(
            'tags' => $tags,
            'action' => $action,
            'linkTag' => $linkTag,
            'canBuy' => $canBuy,
            'canPlay' => $canPlay,
            'canEnroll' => $canEnroll,
        );

        return $result;

    }


    /**
     * Checks if the learning plan has some courses the passed user is not yet enrolled in
     * @param $userModel CoreUser
     */
    public function hasCoursesToEnroll($userModel)
    {
        $coursesIds = $this->getCoursesIdList();
        foreach ($coursesIds as $idCourse) {
            if (!LearningCourseuser::isSubscribed($userModel->idst, $idCourse))
                return true;
        }

        return false;
    }


    public function getCatchupCoursesForUser($idPath, $idUser)
    {
        $commandBase = Yii::app()->db->createCommand();

        $commandBase->select("t1.id_path, t1.path_name, t1.catchup_limit, t3.idCourse, t3.name, t6.idst, t6.firstname, t6.lastname, t4.id_catch_up, t7.date_first_access, (SELECT name FROM learning_course AS lc WHERE lc.idCourse = t4.id_catch_up) 'catchup_course'")

                    ->from("learning_coursepath t1")
                    ->join("learning_coursepath_courses t2", "t2.id_path = t1.id_path")
                    ->join("learning_course t3", "t3.idCourse = t2.id_item")
                    ->join("learning_course_catch_up t4", "t4.id_path = t1.id_path AND t4.id_course = t3.idCourse")
                    ->join("learning_coursepath_user t5", "t5.id_path=t1.id_path")
                    ->join("core_user t6", "t5.idUser = t6.idst")
                    ->leftJoin("learning_courseuser t7", "t7.idUser = t6.idst AND t7.idCourse = t4.id_course")

                    ->where("t1.id_path = :id_path", array(":id_path" => (int)$idPath))
                    ->andWhere("t6.idst = :idst", array(":idst" => (int)$idUser))
                    ->andWhere("t7.date_first_access is not null");
        $commandData = clone $commandBase;
        $config = array(
            'keyField' => 'id_path',
        );
        $dataProvider = new CSqlDataProvider($commandData, $config);

        return $dataProvider;
    }


    public function countPlayedCatchupCourses($idUser) {
        $count = Yii::app()->db->createCommand()
            ->select("COUNT(*)")
            ->from(LearningCourseuser::model()->tableName() . " t1")
            ->join("learning_course_catch_up t2","t2.id_catch_up = t1.idCourse AND t2.id_path = :idPath", array(':idPath' => $this->id_path))
            ->where("t1.idUser = :idUser", array(":idUser" => (int)$idUser))
            ->andWhere("t1.status = :status", array(':status' => LearningCourseuser::$COURSE_USER_END))
            ->queryScalar();
        return (int)$count;

    }


    /**
     * getUserCoursesInLP() - get Users Courses, that are assigned to a Learning Plan
     * @param $idUser - id of the user
     * @param $idPlan - id of the learning plan
     * @return array of courses ids, that are assigned to the learning plan passed and are sssigned to user too
     */
    public static function getUserCoursesInLP($idUser, $idPlan)
    {
        $selectedCourses = array(); // return empty array if CurriculaApp is not active

        if(PluginManager::isPluginActive('CurriculaApp')) {
            //get the courses, that user is assigned to and belongs to the current Learning Plan
            $selectCourses = Yii::app()->db->createCommand(
                'SELECT idCourse FROM ' . LearningCourseuser::model()->tableName() . ' cu ' .
                'INNER JOIN ' . LearningCoursepathCourses::model()->tableName() . ' pl ' .
                'ON pl.id_item = cu.idCourse ' .
                'WHERE  cu.idUser = ' . (int) $idUser . ' AND pl.id_path = ' . (int) $idPlan
            )->queryAll();

            //prepare output selected courses ids array
            foreach ($selectCourses as $idCourse) {
                $selectedCourses[] = (int) $idCourse['idCourse'];
            }
        }

        return $selectedCourses;
    }


    /**
     * getCoursesOutsideLP() - get all courses ids, that are not assigned to a Learning Plan
     * @param $idPlan - id of the learning plan
     * @return array of courses ids, that are not assigned to the learning plan passed
     */
    public static function getCoursesOutsideLP($idPlan) {
        //prepare excluded courses
        $excludeCourses = array();

        //get the courses, that are not assigned to the current Learning Plan
        $selectExcludeCourses = Yii::app()->db->createCommand(
            'SELECT ec.idCourse FROM ' . LearningCourse::model()->tableName() . ' ec ' .
            'WHERE ec.idCourse NOT IN ( ' .
            'SELECT idCourse FROM ' . LearningCourse::model()->tableName() . ' c ' .
            'INNER JOIN ' . LearningCoursepathCourses::model()->tableName() . ' pl ' .
            'ON pl.id_item = c.idCourse ' .
            'WHERE  pl.id_path = ' . (int) $idPlan . ')'
        )->queryAll();

        //prepare excluded courses array
        foreach ($selectExcludeCourses as $idCourse) {
            $excludeCourses[] = (int) $idCourse['idCourse'];
        }

        return $excludeCourses;
    }


    /**
     * getCoursesOfLP() - get all courses ids, that are assigned to a Learning Plan
     * @param $idPlan - id of the learning plan
     * @return array of courses ids, that are assigned to the learning plan passed
     */
    public static function getCoursesOfLP($idPlan) {
        //prepare lp courses
        $includeCourses = array();

        //get the courses, that are assigned to the current Learning Plan
        $selectIncludeCourses = Yii::app()->db->createCommand(
            'SELECT id_item AS idCourse FROM ' . LearningCoursepathCourses::model()->tableName() .
            ' WHERE  id_path = ' . (int) $idPlan
        )->queryAll();

        //prepare included courses array
        foreach ($selectIncludeCourses as $idCourse) {
            $includeCourses[] = (int) $idCourse['idCourse'];
        }

        return $includeCourses;
    }


	/**
	 * Check if an enrollment is part of a LP
	 * @param $idUser the numeric ID of a user
	 * @param $idCourse the numeric ID of a course
	 * @param $lpInfo wheter returning additional info about learning plan
	 * @return array list of LPs enrollments (array is empty if no LPs are found)
	 */
	public static function checkEnrollment($idUser, $idCourse, $lpInfo = false) {

		//compose query and sql command
		$query = "SELECT ".($lpInfo ? " cp.* " : " cpu.id_path ")
			." FROM ".LearningCoursepathCourses::model()->tableName()." cpc "
			." JOIN ".LearningCoursepathUser::model()->tableName()." cpu "
			." ON (cpc.id_path = cpu.id_path AND cpu.idUser = :id_user AND cpc.id_item = :id_course) "
			.($lpInfo ? " JOIN ".LearningCoursepath::model()->tableName()." cp ON (cp.id_path = cpu.id_path) " : "");

		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand($query);
		$reader = $cmd->query(array(
			':id_user' => $idUser,
			':id_course' => $idCourse
		));

		$result = array();
		while ($record = $reader->read()) {
			if ($lpInfo) {
				$result[$record['id_path']] = $record;
			} else {
				$result[$record['id_path']] = true; //use keys to avoid multiple LPs references
}
		}

		//remove unnecessary data/indexes
		if (!$lpInfo) { $result = array_keys($result); }

		//if some LPs have been found, than this user/course combination is part of some learning plan
		return $result;
	}


	/**
	 * Check if users are enrolled to some LPs
	 * @param $users a list of users IDs (an array of integers)
	 * @param $idCourse the numeric ID of a course
	 * @return array for every user enrolled in LPs, which LPs have been found
	 */
	public static function checkEnrollmentsMultipleUsers($users, $idCourse) {

		if (empty($users) || !is_array($users)) {
			//no users to check have been provided
			return array();
		}

		//NOTE: users list may be really big, in this case subdivide the array in smaller pieces, just to be sure not to hang the SQL engine
		$chunks = array_chunk($users, 1000);
		foreach ($chunks as $chunk) {

			//compose query and sql command
			$query = "SELECT cpu.id_path, cpu.idUser "
				. " FROM " . LearningCoursepathCourses::model()->tableName() . " cpc "
				. " JOIN " . LearningCoursepathUser::model()->tableName() . " cpu "
				. " ON cpc.id_path = cpu.id_path AND cpc.id_item = :id_course "
				. " AND cpu.idUser IN (" . implode(",", $chunk) . ")";

			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand($query);
			$reader = $cmd->query(array(
				':id_course' => $idCourse
			));

			$result = array();
			while ($record = $reader->read()) {

				$idUser = $record['idUser'];
				$idPath = $record['id_path'];

				if (!isset($result[$idUser])) {
					$result[$idUser] = array();
				}
				if (!in_array($idPath, $result[$idUser])) {
					$result[$idUser][] = $idPath;
				}
			}
		}

		//for every provided user: if some LPs have been found, than this user/course combination is part of some learning plan
		return $result;
	}



	/**
	 * Return list of Learning plan IDs of plans marked for selling
	 */
	public static function getItemsForSale()
	{
		$result = Yii::app()->db->createCommand()
			->select('id_path')
			->from(self::model()->tableName())
			->where('is_selling > 0')
			->queryColumn();

		return $result;
	}


		/**
		 * Returns the deeplink for this learningplan
		 * @param integer $idUser
		 */
	public function getDeeplink($idUser = null)
	{
		$genById = (!$idUser) ? Yii::app()->user->id : (int)$idUser;
		$key = Yii::app()->params['deeplink_secret_key'];
		$hash = hash('sha1', $this->id_path . "," . Yii::app()->user->id . "," . $key);

		return Docebo::createAbsoluteLmsUrl('coursepath/deeplink', array(
			'id_path' => $this->id_path,
			'generated_by' => $genById,
			'hash' => $hash
		));
	}


	/**
	 * Returns array with LP courses ids
	 * @param array
	 */
	public function getLPCoursesIds() {
		$courses = array();
		foreach($this->learningCoursepathCourses as $lpCourseModel){
			$courses[] = $lpCourseModel->id_item;
		}
		return $courses;
	}


	//Reset date begin and end validity for LP enrolled or started course user
	public function updateLPUser($courses, $userId, $reset = false){
		$lpCoursesFirstAccess = Yii::app()->getDb()->createCommand()
			->select('MIN(c.date_first_access)')
			->from(LearningCourseuser::model()->tableName().' c')
			->where('c.idUser=:idUser AND date_first_access IS NOT NULL', array(
				':idUser' => $userId
			))
			->andWhere(array('in', 'idCourse', $courses))
			->queryScalar();

		$condition = ($reset) ? '' : "date_end_validity = :date";
		$params =  ($reset) ? array() : array(':date' => '0000-00-00 00:00:00');

		$lpUserModel = LearningCoursepathUser::model()->findByPk(array(
			'id_path' 	=> $this->id_path,
			'idUser' 	=> $userId
		), $condition, $params);

		if ($lpUserModel) {
			$lpUserModel->date_begin_validity = ($lpCoursesFirstAccess) ? Yii::app()->localtime->toLocalDateTime($lpCoursesFirstAccess) : '';
			$lpUserModel->date_end_validity   = ($lpCoursesFirstAccess) ? Yii::app()->localtime->addInterval($lpUserModel->date_begin_validity, 'P' . intval($this->days_valid) . 'D'): '';
			$lpUserModel->save();
		}
	}


	/**
	 * Check if any course in the specified learning path contain any C.S.P. learning objects
	 *
	 * @param $idCourse ID of the course to be checked
	 */
	public static function hasCspObjects($idPath) {
		$query = "SELECT COUNT(*) AS num_csp "
			. " FROM " . LearningOrganization::model()->tableName() . " o "
			. " JOIN " . LearningCoursepathCourses::model()->tableName() . " cpc ON (o.idCourse = cpc.id_item)"
			. " WHERE o.from_csp IS NOT NULL AND o.objectType <> '' AND cpc.id_path = :id_path";
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand($query);
		$result = $cmd->queryScalar([':id_path' => $idPath]);
		return (!empty($result) && $result > 0);
	}

	public static function coursepathDetailsIsVisibleForUser($id_path)
	{
		if(LearningCoursepathUser::model()->findByAttributes(array('id_path' => $id_path, 'idUser' => Yii::app()->user->getIdst())))
			return true;

		$showAllCoursesIfNoCatalogueIsAssigned = ('on' == Settings::get('on_catalogue_empty', 'on'));
		$idUser = Yii::app()->user->getIdst();

		// Load the list of catalogs this use can see
		$userCatalogs = DashletCatalog::getUserAvailableCatalogs(false);
		$userAllCatalogs = DashletCatalog::getUserAvailableCatalogs(true, true);

		if (empty($userCatalogs) || (is_array($userCatalogs) && count($userCatalogs) == 1 && isset($userCatalogs['all'])))
		{
			if (!$showAllCoursesIfNoCatalogueIsAssigned || (count($userAllCatalogs) > 1))
				return false;
			else
				return true;
		}
		else
		{
			$command = Yii::app()->db->createCommand();
			$command->select('
					t.idEntry AS id
				')
				->from(LearningCatalogueEntry::model()->tableName() . ' t');

			$coursePathOnCondition = 'coursepath.visible_in_catalog=1';
			$command->andWhere(array('IN', 't.idCatalogue', array_keys($userCatalogs)));
			$command->join(LearningCatalogue::model()->tableName() . ' cat', 'cat.idCatalogue = t.idCatalogue')
				->join(LearningCoursepath::model()->tableName() . ' coursepath',
					't.type_of_entry="coursepath" AND t.idEntry=coursepath.id_path AND ' . $coursePathOnCondition);

			$records = $command->queryColumn();

			if (array_search($id_path, $records) === false) {
				return false;
			}
		}

		return true;
	}


    /**
     * @param $courseId
     */
    public static function removeCourseFromPrerequisites($courseId){
        $rows = Yii::app()->db->createCommand()
            ->select("id_path, id_item, prerequisites")
            ->from(LearningCoursepathCourses::model()->tableName())
            ->where("prerequisites LIKE '%".$courseId."%'")
            ->queryAll();
        foreach($rows as $row){
            $prereq = explode(',',$row['prerequisites']);
            unset($prereq[array_search ( $courseId , $prereq)]);
            if (empty($prereq))
                $newValue = '';
            else
                $newValue = (implode(',',$prereq));
            Yii::app()->db->createCommand()->update(LearningCoursepathCourses::model()->tableName(), array('prerequisites' => $newValue),'id_path = '.$row['id_path'].' AND id_item = '.$row['id_item']);
        }

    }
}
