<?php

/**
 * This is the model class for table "webinar_session_date_attendance".
 *
 * The followings are the available columns in table 'webinar_session_date_attendance':
 * @property integer $id_session
 * @property string $day
 * @property integer $id_user
 * @property integer $watched_live
 * @property integer $watched_recording
 */
class WebinarSessionDateAttendance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_session_date_attendance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_session, day, id_user', 'required'),
			array('id_session, id_user, watched_live, watched_recording', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, day, id_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'day' => 'Day',
			'id_user' => 'Id User',
			'watched_live' => 'Watched live',
			'watched_recording' => 'Watched recording'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('watched_live',$this->watched_live);
		$criteria->compare('watched_recording',$this->watched_recording);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarSessionDateAttendance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
