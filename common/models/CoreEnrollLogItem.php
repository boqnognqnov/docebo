<?php

/**
 * This is the model class for table "core_enroll_log_item".
 *
 * The followings are the available columns in table 'core_enroll_log_item':
 * @property integer $log_item_id
 * @property integer $enroll_log_id
 * @property integer $user_id
 * @property integer $target_id
 * @property integer $rollback
 *
 * The followings are the available model relations:
 * @property CoreEnrollLog $enrollLog
 */
class CoreEnrollLogItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreEnrollLogItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_enroll_log_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enroll_log_id, user_id, target_id', 'required'),
			array('enroll_log_id, user_id, target_id, rollback', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('log_item_id, enroll_log_id, user_id, target_id, rollback', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'enrollLog' => array(self::BELONGS_TO, 'CoreEnrollLog', 'enroll_log_id'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'user_id'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'target_id'),
			'curricula' => array(self::BELONGS_TO, 'LearningCoursepath', 'target_id'),
			'deleted_user' => array(self::BELONGS_TO, 'CoreDeletedUser', array('user_id' => 'idst'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_item_id' => 'Log Item',
			'enroll_log_id' => 'Enroll Log',
			'user_id' => 'User',
			'target_id' => 'Target',
			'rollback' => 'Rollback',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('log_item_id',$this->log_item_id);
		$criteria->compare('enroll_log_id',$this->enroll_log_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('target_id',$this->target_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function rollback()
	{
		switch ($this->enrollLog->rule->rule_type)
		{
			case CoreEnrollRule::RULE_BRANCH_CURRICULA:
			case CoreEnrollRule::RULE_GROUP_CURRICULA:
				LearningCoursepathUser::unsubscribeUser($this->user_id, $this->target_id);
				break;
			case CoreEnrollRule::RULE_GROUP_COURSE:
			case CoreEnrollRule::RULE_BRANCH_COURSE:
				$model = new LearningCourseuser();
				$model->unsubscribeUser($this->user_id, $this->target_id);
				break;
		}
		$this->rollback = 1;
		$this->save(false);
	}
}