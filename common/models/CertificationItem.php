<?php

/**
 * This is the model class for table "certification_item".
 *
 * The followings are the available columns in table 'certification_item':
 * @property integer $id
 * @property integer $id_cert
 * @property integer $id_item
 * @property string $item_type
 *
 * The followings are the available model relations:
 * @property Certification $certification
 * @property CertificationUser[] $certifiedUsers
 */
class CertificationItem extends CActiveRecord
{
	
	const TYPE_COURSE 			= 'course';
	const TYPE_LEARNING_PLAN 	= 'plan';
	const TYPE_TRANSCRIPT 		= 'transcript';
	
	public $search_input = null;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cert, id_item', 'required'),
			array('id_cert, id_item', 'numerical', 'integerOnly'=>true),
			array('item_type', 'length', 'max'=>13),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_cert, id_item, item_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certification' => array(self::BELONGS_TO, 'Certification', 'id_cert'),
			'certifiedUsers' => array(self::HAS_MANY, 'CertificationUser', 'id_cert_item'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_cert' => 'Id Cert',
			'id_item' => 'Id Item',
			'item_type' => 'Item Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_cert',$this->id_cert);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('item_type',$this->item_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CertificationItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProvider() {
	
		// SQL Parameters
		$params = array();
	
		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from('certification_item t');
		
		$commandBase->leftJoin("learning_course course", "t.id_item=course.idCourse AND t.item_type=:type_course");
		$commandBase->leftJoin("learning_coursepath plan", "t.id_item=plan.id_path AND t.item_type=:type_plan");
		$params[':type_course'] = self::TYPE_COURSE;
		$params[':type_plan'] 	= self::TYPE_LEARNING_PLAN;
		
		
		if (PluginManager::isPluginActive('TranscriptsApp')) {
			$commandBase->leftJoin("transcripts_record ts", "t.id_item=ts.id_record AND t.item_type=:type_transcript");
		}
		else {
			$commandBase->andWhere("t.item_type <> :type_transcript");
		}
		$params[':type_transcript'] = self::TYPE_TRANSCRIPT;
		
		
		// Also search by user related text
		if ($this->search_input != null) {
			if (PluginManager::isPluginActive('TranscriptsApp')) {
				$cond = "CONCAT(COALESCE(plan.path_name,''), ' ', COALESCE(course.name,''), ' ', COALESCE(ts.course_name,'')) LIKE :search";
			}
			else {
				$cond = "CONCAT(COALESCE(plan.path_name,''), ' ', COALESCE(course.name,'')) LIKE :search";
			}
			$commandBase->andWhere($cond);
			$params[':search'] = '%'.$this->search_input.'%';
		}
		
		// Filter by Certification ID
		if ($this->id_cert) {
			$commandBase->andWhere("t.id_cert=:id_cert");
			$params[':id_cert'] = $this->id_cert;
		}
		
		// Filter by ITEM id
		if ($this->id) {
			$commandBase->andWhere("t.id=:id");
			$params[':id'] = $this->id;
		}
		
	
		// DATA
		$commandData = clone $commandBase;
	
	
		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id_item)');
		$numRecords = $commandCounter->queryScalar($params);
	
	
		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'name_plan' => array(
				'asc' 	=> 'plan.path_name',
				'desc'	=> 'plan.path_name DESC',
			),
			'name_course' => array(
				'asc' 	=> 'course.name',
				'desc'	=> 'course.name DESC',
			),
			'item_type' => array(
				'asc' 	=> 't.item_type ASC, course.name ASC, plan.path_name ASC',
				'desc'	=> 't.item_type DESC, course.name DESC, plan.path_name DESC',
			),
				
		);
		$sort->defaultOrder = array(
			'item_type'	=>	CSort::SORT_ASC,
		);
	
		// DATA-II
		$commandData->select("*, course.course_type");
	
		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> array('pageSize' => $pageSize),
			'keyField'			=> 'id',
			'sort' 				=> $sort,
			'params'			=> $params,
		);
	
		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);
	
		return $dataProvider;
	
	}
	
	
	/**
	 * 
	 * @param string $type
	 * @return array
	 */
	public static function globalAssignedItems($type) {

		// SQL Parameters
		$params = array();
		
		$command = Yii::app()->db->createCommand();
		$command->from('certification_item t');
		$command->where('t.item_type=:item_type');
		$params[':item_type'] = $type;
		
		$command->select('id_item');
		
		$list = array();
		
		$reader = $command->query($params);
		foreach ($reader as $row) {
			$list[] = (int) $row['id_item'];
		}
		
		return $list;
		
	}


	/**
	 *
	 * @param $type
	 * @param $idItem
	 *
	 * @return integer
	 */
	public static function itemCertification($type, $idItem) {
		
		// SQL Parameters
		$params = array();
		
		$command = Yii::app()->getDb()->createCommand();
		$command->from(CertificationItem::model()->tableName());
		$command->where('item_type=:item_type');
		$command->andWhere('id_item=:id_item');
		
		$params[':item_type'] 	= $type;
		$params[':id_item'] 	= (int) $idItem;
		
		$command->select('id_cert');
		
		$idCert = $command->queryScalar($params);
		
		return $idCert;
		
	}
	
	
	
	
	
	
	/**
	 * 
	 * @param string $type
	 * @param integer $idItem
	 * @param integer $idCert
	 */
	public static function assignItemToCertification($type, $idItem, $idCert) {
		
		$model = self::model()->findByAttributes(array('item_type' => $type, 'id_item' => $idItem));
		
		if ($model) {
			if (!$idCert) {
				$model->delete();
				return;
			}
			$model->id_cert = $idCert;
			$model->save();
		}elseif ($idCert) {
			$model = new self();
			$model->id_cert = $idCert;
			$model->id_item = $idItem;
			$model->item_type = $type;
			$model->save();
		}
	}

	/**
	 * Get the name of the certification assigned to this item ID and item type (e.g. course, learning plan, transcript).
	 * Uses static cache internally to reduce DB load
	 *
	 * @param $itemId
	 * @param $itemType
	 *
	 * @return string The name of the certification associated with this item
	 */
	static public function getCertificationNameByItemAndType($itemId, $itemType){

		/*
		 * array(
		 *      'transcript'=>array(
		 *          'itemId1'=>'certificationName'
		 *          'itemId2'=>'certificationName'
		 *      ),
		 *      'course'=>array(
		 *          'itemId1'=>'certificationName'
		 *          'itemId2'=>'certificationName'
		 *      ),
		 *      'plan'=>array(
		 *          'itemId1'=>'certificationName'
		 *          'itemId2'=>'certificationName'
		 *      )
		 * )
		 */
		static $namesCache = array();

		if(empty($namesCache)){
			// Only query DB once
			$query = Yii::app()->getDb()->createCommand()
				->select('t.id_item, t.item_type, c.title')
				->from(CertificationItem::model()->tableName().' t')
				->join(Certification::model()->tableName().' c', 't.id_cert=c.id_cert')
				->queryAll();
			foreach($query as $tmp){
				if(!isset($namesCache[$tmp['item_type']])){
					$namesCache[$tmp['item_type']] = array();
				}
				$namesCache[$tmp['item_type']][$tmp['id_item']] = $tmp['title'];
			}
		}

		if(isset($namesCache[$itemType][$itemId]))
			return $namesCache[$itemType][$itemId];

		return null;
	}

}
