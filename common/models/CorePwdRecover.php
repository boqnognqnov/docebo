<?php

/**
 * This is the model class for table "core_pwd_recover".
 *
 * The followings are the available columns in table 'core_pwd_recover':
 * @property integer $idst_user
 * @property string $random_code
 * @property string $request_date
 *
 * The followings are the available model relations:
 * @property CoreUser $idstUser
 */
class CorePwdRecover extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CorePwdRecover the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_pwd_recover';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst_user', 'numerical', 'integerOnly'=>true),
			array('random_code', 'length', 'max'=>255),
			array('request_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst_user, random_code, request_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idstUser' => array(self::BELONGS_TO, 'CoreUser', 'idst_user'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('request_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst_user' => 'Idst User',
			'random_code' => 'Random Code',
			'request_date' => 'Request Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst_user',$this->idst_user);
		$criteria->compare('random_code',$this->random_code,true);
		$criteria->compare('request_date',Yii::app()->localtime->fromLocalDateTime($this->request_date),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}