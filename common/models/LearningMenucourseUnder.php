<?php

/**
 * This is the model class for table "learning_menucourse_under".
 *
 * The followings are the available columns in table 'learning_menucourse_under':
 * @property integer $idCourse
 * @property integer $idModule
 * @property integer $idMain
 * @property integer $sequence
 * @property string $my_name
 *
 * The followings are the available model relations:
 * @property LearningModule $learningModule
 */
class LearningMenucourseUnder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningMenucourseUnder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_menucourse_under';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCourse, idModule, idMain, sequence', 'numerical', 'integerOnly'=>true),
			array('my_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCourse, idModule, idMain, sequence, my_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningModule' => array(self::BELONGS_TO, 'LearningModule', 'idModule'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCourse' => 'Id Course',
			'idModule' => 'Id Module',
			'idMain' => 'Id Main',
			'sequence' => 'Sequence',
			'my_name' => 'My Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idModule',$this->idModule);
		$criteria->compare('idMain',$this->idMain);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('my_name',$this->my_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}