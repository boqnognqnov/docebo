<?php
/**
 * Used to log jobs execution.
 *
 * The followings are the available columns in table 'core_job_log':
 * @property integer $id
 * @property integer $id_job
 * @property string $started
 * @property string $finished
 * @property string $status
 * @property string $message
 * @property string $json_job
 *
 * The followings are the available model relations:
 * @property CoreJob $job
 */
class CoreJobLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_job_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_job, started, json_job', 'required'),
			array('id_job', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>32),
			array('finished, message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_job, started, finished, status, message, json_job', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'job' => array(self::BELONGS_TO, 'CoreJob', 'id_job'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Log ID',
			'id_job' => 'Job ID',
			'started' => 'Started',
			'finished' => 'Finished',
			'status' => 'Status',
			'message' => 'Message',
			'json_job' => 'Original job attributes',
		);
	}
	
	
	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
				'LocalTimeConversionBehavior' => array(
						'class' => 'common.components.LocalTimeConversionBehavior',
						'timestampAttributes' => array('started medium', 'finished medium'),
				)
		);
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_job',$this->id_job);
		$criteria->compare('started',$this->started,true);
		$criteria->compare('finished',$this->finished,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('json_job',$this->json_job,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreJobLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
