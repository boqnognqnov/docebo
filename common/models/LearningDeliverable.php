<?php

/**
 * This is the model class for table "learning_deliverable".
 *
 * The followings are the available columns in table 'learning_deliverable':
 * @property integer $id_deliverable
 * @property string $title
 * @property string $description
 * @property integer $allow_multiupload
 * @property integer $learner_policy
 *
 * The followings are the available model relations:
 * @property LearningDeliverableObject[] $deliverableObjects
 */
class LearningDeliverable extends CActiveRecord
{
	/**
	 * Navigation rules - represent behavior
	 */
	const NAVRULE_SUBMITTED = 1; // the learner can progress when the assignment is submitted
	const NAVRULE_PASSED 	= 0; // the learner can progress when the assignment has been evaluated with a passing score

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_deliverable';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, description', 'required'),
			array('title, description, allow_multiupload', 'safe'),
			array('allow_multiupload, learner_policy', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_deliverable, title, description, allow_multiupload, learner_policy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deliverableObjects' => array(self::HAS_MANY, 'LearningDeliverableObject', 'id_deliverable'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_deliverable' => 'Id Deliverable',
			'title' => 'Title',
            'description' => 'Description',
			'allow_multiupload' => 'Allow Multiupload',
			'learner_policy' => 'Learner Policy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_deliverable',$this->id_deliverable);
		$criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
		$criteria->compare('allow_multiupload',$this->allow_multiupload);
		$criteria->compare('learner_policy',$this->learner_policy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningDeliverable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getUserDeliverableObjects($userId) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('id_deliverable = :id_deliverable');
        $criteria->addCondition('id_user = :id_user');
        $criteria->params = array(
            ':id_deliverable' => $this->id_deliverable,
            ':id_user' => $userId
        );
        $criteria->order = 'created DESC';
        $deliverableObjects = LearningDeliverableObject::model()->findAll($criteria);
        return $deliverableObjects;
    }

    public function behaviors() {
        return array(
            'learningObjects' => array(
                'class' => 'LearningObjectsBehavior',
                'objectType' => LearningOrganization::OBJECT_TYPE_DELIVERABLE,
                'idPropertyName' => 'id_deliverable',
                'titlePropertyName' => 'title'
            )
        );
    }

    public function beforeDelete() {
        $return = parent::beforeDelete();

        if ($return) {
            // delete all deliverable objects
            $deliverableObjects = $this->deliverableObjects;
            if (!empty($deliverableObjects)) {
                foreach ($deliverableObjects as $deliverableObject) {
                    if ( ! $deliverableObject->delete() )
                        return false;
                }
            }
        }

        return $return;
    }

    public function getObjectScores($id_user = null) {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('evaluation_status', array(
            LearningDeliverableObject::STATUS_ACCEPTED,
            LearningDeliverableObject::STATUS_REJECTED,
        ));
        $criteria->addCondition('id_deliverable = :id_deliverable');
        if($id_user) {
            $criteria->addCondition('id_user = :id_user');
            $criteria->params[':id_user'] = $id_user;
        }
        $criteria->params[':id_deliverable'] = $this->id_deliverable;
        $criteria->order = 'evaluation_date DESC';
        $deliverableObject = LearningDeliverableObject::model()->find($criteria);

        return array('raw' => $deliverableObject->evaluation_score, 'max' => 100);
    }

	/**
	 * This function creates a copy of the Assignment LO
	 *
	 * @throws CException
	 * @return LearningDeliverable the new Assignment LO model
	 */
	public function copy() {

		// Copy the Deliverable LO in DB
		$copiedPackage = clone $this;
		$copiedPackage->id_deliverable = null;
		$copiedPackage->setIsNewRecord(true);
		if (!$copiedPackage->save()) {
			$errorMessage = 'Error while copying Assignment LO: '.print_r($copiedPackage->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}

		return $copiedPackage;

	}
}
