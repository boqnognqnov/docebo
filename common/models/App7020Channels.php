<?php

/**
 * This is the model class for table "app7020_channels".
 *
 * The followings are the available columns in table 'app7020_channels':
 * @property integer $id
 * @property integer $type
 * @property string $icon_fa
 * @property string $icon_color
 * @property string $icon_bgr
 * @property string $predefined_uid
 * @property integer $enabled
 * @property integer $ordering
 * @property string $sort_items
 * @property integer $permission_type
 *
 * The followings are the available model relations:
 * @property App7020ChannelAssets[] $assets
 * @property App7020ChannelExperts[] $experts
 * @property App7020ChannelTranslation[] $channel_translations
 * @property App7020ChannelVisibility[] $channel_visibilities
 */
class App7020Channels extends CActiveRecord {

    const PERMISSION_TYPE_EVERYONE = 1;
    const PERMISSION_TYPE_EVERYONE_PR = 2;
    const PERMISSION_TYPE_EXPERTS_ONLY = 3;

    public static $permissions = array(
        self::PERMISSION_TYPE_EVERYONE => array(
            'text' => 'Everyone',
        ),
        self::PERMISSION_TYPE_EVERYONE_PR => array(
            'text' => 'Everyone, with experts peer review',
        ),
        self::PERMISSION_TYPE_EXPERTS_ONLY => array(
            'text' => 'Experts only',
        )
    );

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_channels';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, ordering', 'required'),
            array('type, enabled, ordering', 'numerical', 'integerOnly' => true),
            array('predefined_uid', 'length', 'max' => 150),
            array('sort_items', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, predefined_uid, enabled, ordering, sort_items', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'assets' => array(self::HAS_MANY, 'App7020ChannelAssets', 'idChannel'),
            'experts' => array(self::HAS_MANY, 'App7020ChannelExperts', 'idChannel'),
            'channel_translations' => array(self::HAS_MANY, 'App7020ChannelTranslation', 'idChannel'),
            'channel_visibilities' => array(self::HAS_MANY, 'App7020ChannelVisibility', 'idChannel'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'icon_fa' => 'Icon type from FontAwesome',
            'icon_color' => 'Icon color',
            'icon_bgr' => 'Icon container background',
            'predefined_uid' => 'use this for migrations, etc, Invitations, My Courses, My Active Courses, Continue Watch, Watch Later, Recent, ',
            'enabled' => 'Enabled',
            'ordering' => 'Order',
            'sort_items' => 'here will be stored data for ordering of assets into channel ',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type);
        $criteria->compare('icon_fa', $this->icon_fa);
        $criteria->compare('icon_color', $this->icon_color);
        $criteria->compare('icon_bgr', $this->icon_bgr);
        $criteria->compare('predefined_uid', $this->predefined_uid, true);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('ordering', $this->ordering);
        $criteria->compare('sort_items', $this->sort_items, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020Channels the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /*     * ********************************** */
    /* Combo Grid View Columns Renderers */
    /*     * ********************************** */

    public static function channelColumnName($data, $isIcon = true, $withDescription = 0) {
        $icon = 'fa-user-plus';
        $iconColor = '#fff';
        $iconBackground = '#0465AC';
        $iconHtml = '';
        if ($data['channelIcon'])
            $icon = $data['channelIcon'];
        if ($data['channelIconColor'])
            $iconColor = $data['channelIconColor'];
        if ($data['channelIconBackground'])
            $iconBackground = $data['channelIconBackground'];
        if ($isIcon === true) {
            $iconHtml = '<i class="fa ' . $icon . '" style="color: ' . $iconColor . '; background: ' . $iconBackground . ';"></i>';
        }


        if ($withDescription) {
            $data['channelName'] .= "<span class='channelDescription'>" . $data['channelDescription'] . "</span>";
        }

        return $iconHtml . $data['channelName'];
    }

    public static function channelColumnVisibility($data) {
        return '<span>' . $data["visibility"] . '</span>';
    }

    public static function channelColumnUploadPermission($data) {
        $disabled = (!empty($data['channelPredefinedUid'])) ? 'predefined' : 'not_predefined';

        return '<span class="' . $disabled . '">' . $data["upload_permissions"] . '</span>';
    }

    public static function channelColumnExperts($data) {
        $link = Docebo::createApp7020Url('channelsManagement/experts', array('id' => $data["idChannel"]));
        $return = '<a href="' . $link . '"><span class="' . ($data["channelExperts"] ? '' : 'empty') . '"><span class="count">' . $data["channelExperts"] . '</span><i class="fa fa-users"></i></span></a>';
        if ($data['channelType'] == 1)
            $return = '';
        return $return;
    }

    public static function channelColumnShow($data) {
        return '<i class="fa fa-check-circle-o ' . ($data['channelEnabled'] == 0 ? 'disabled' : '') . '" data-channel-id="' . $data["idChannel"] . '"></i>';
    }

    public static function channelColumnDnd($data) {
        $return = '<i class="fa fa-arrows"></i>';
        /* Disable predefined channel can to dran n drop */
        //if ($data['channelType'] == 1)
        //	$return = '';
        return $return;
    }

    public static function channelColumnDropdownMenu($data) {
        $controlParams = array(
            'arrayData' => array(
                array('url' => 'javascript:;', 'text' => Yii::t('standard', 'Edit'), 'method' => 'edit', 'dialog-title' => Yii::t('app7020', 'Edit Channel')),
            )
        );
        if ($data['channelType'] != 1) {
            $controlParams['arrayData'] = array_merge($controlParams['arrayData'], array(
                array('url' => 'javascript:;', 'text' => Yii::t('app7020', 'Clone'), 'method' => 'clone', 'dialog-title' => Yii::t('app7020', 'Clone Channel')),
                array('url' => 'javascript:;', 'text' => Yii::t('standard', 'Delete'), 'method' => 'delete', 'dialog-title' => Yii::t('app7020', 'Delete Channel'))
            ));
        }
        return Yii::app()->controller->widget("common.widgets.DropdownControls", $controlParams, true);
    }

    /**
     * Get channel translations by id
     * @param int $id
     */
    public static function getChannelTranslationsById($id = false) {

        $command = Yii::app()->db->createCommand();
        $command
            //->from(App7020Channels::model()->tableName() . ' channel')
            ->from(App7020ChannelTranslation::model()->tableName())
            //->leftJoin(App7020ChannelTranslation::model()->tableName() . ' transDef', '(channel.id=transDef.idChannel)')
            ->andWhere('idChannel = :idChannel', array(':idChannel' => $id));

        $data = $command->queryAll(true);
        return $data;
    }

    /**
     * Return translated title text of THIS channel. By default returns the text in current language.
     * If second parameter is passed (array of IDs), returns an array of  <id> => <translation> for that list of channels
     *
     * If the translation in desired language (explicitely specified or current one) is NOT available,
     * translation in DEFAULT PLATFORM language is returned. That's why the double join and CASE in the SQL.
     *
     * Usage when channels list is passed:
     * 		$array = App7020Channels::model()->translation(false, array(1,2,3,4));
     *
     * @param string $language  (en, de, it, ...)
     * @param string $channelsList Optional list of topic IDs
     *
     * @return string|array
     */
    public function translation($language = false, $channelsList = false) {

        // Default (platform) language ('en')
        $defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

        $language = self::resolveLanguage($language);

        $command = Yii::app()->db->createCommand();
        $command
            ->from(App7020Channels::model()->tableName() . ' channel')
            ->leftJoin(App7020ChannelTranslation::model()->tableName() . ' trans', '(channel.id=trans.idChannel) 		AND (trans.lang=:language)')
            ->leftJoin(App7020ChannelTranslation::model()->tableName() . ' transDef', '(channel.id=transDef.idChannel) 	AND (transDef.lang=:languageDef)')
            ->order('channel.ordering', 'asc');

        $params = array(
            ':language' => $language,
            ':languageDef' => $defaultLanguage,
        );

        // We do different SELECT and WHERE, depending on input parameter topicsList
        if (is_array($channelsList) && !empty($channelsList)) {
            $command->select('channel.ordering as channelOrder, channel.id as idChannel, (CASE WHEN trans.name IS NULL THEN transDef.name ELSE trans.name END) AS translatedName, '
                . '(CASE WHEN trans.description IS NULL THEN transDef.description ELSE trans.description END) AS translatedDescription');
            $command->andWhere(array("IN", "channel.id", $channelsList));
        } else {
            $params[':idChannel'] = $this->id;
            $command->select('(CASE WHEN trans.name IS NULL THEN transDef.name ELSE trans.name END) AS translatedName, '
                . '(CASE WHEN trans.description IS NULL THEN transDef.description ELSE trans.description END) AS translatedDescription');
            $command->andWhere('channel.id = :idChannel');
        }

        // Also, we return different result, depending on input parameter channelsList
        // No list ?   an array is returned
        // List of IDs?  Array of arrays with keys equal to channel IDs
        if (is_array($channelsList) && !empty($channelsList)) {
            $rows = $command->queryAll(true, $params);
            $result = array();
            foreach ($rows as $row) {
                $result[$row['idChannel']]['name'] = $row['translatedName'];
                $result[$row['idChannel']]['description'] = $row['translatedDescription'];
            }
            // Fix NULL translations to "Channel #<id>"
            array_walk($result, function(&$item, $index) {
                $item['name'] = $item['name'] ? $item['name'] : "channel #" . (int) $index;
            });

            return $result;
        } else {
            if ($row = $command->queryRow(true, $params)) {
                $result['name'] = $row['translatedName'];
                $result['description'] = $row['translatedDescription'];
                return $result;
            } else {
                return array('name' => "channel #" . (int) $this->id, 'description' => '');
            }
        }
    }

    /**
     * Return resolved language, depending on parameter, default platform language and current language
     *
     * @param string $language
     */
    public static function resolveLanguage($language = false) {
        if ($language === false)
            $language = Yii::app()->getLanguage();
        else if ($language == App7020ChannelTranslation::DEFAULT_PLATFORM_LANGUAGE) {
            $language = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
        }

        return $language;
    }

    /**
     * Use App7020Helpers::truncateAbstractHTMLContents()
     * for using and html tags closing
     * @deprecated
     */
    static function truncate($str, $length) {
        if (strlen($str) > $length) {
            return substr($str, 0, $length) . '...';
        } else {
            return $str;
        }
    }

    /**
     * Custom select for channel data provider incl. visibility 
     * @param array $args
     * @param bool $args['num_experts'] request to how many experts are assigned to channels
     * @param bool $args['ask_num_visibility_structures'] request to check how many structures are asseigned to channel, branches and groups 
     * @param bool $args['friendly_permissions'] request to extract friendly result about channel permissions 
     * @return string
     * @throws Exception Required criteria for custom select for channels are missing
     */
    static function customProviderSelect($args = array('num_experts' => false, 'num_visibility_structures' => false, 'friendly_permissions' => false)) {
        if (!isset($args['num_experts']) && !isset($args['ask_num_visibility_structures']) && !isset($args['friendly_permissions'])) {
            throw new Exception('Required criteria for custom select for channels are missing');
        }
        $arr = array(
            '(CASE WHEN ct.name IS NULL THEN ct_fb.name ELSE ct.name END)' => 'channelName',
            '(CASE WHEN ct.description IS NULL THEN ct_fb.description ELSE ct.description END)' => 'channelDescription',
            '#alias#.id' => 'idChannel',
            '#alias#.permission_type' => 'channelPermissions',
            '#alias#.enabled' => 'channelEnabled',
            '#alias#.predefined_uid' => 'channelPredefinedUid',
            '#alias#.type' => 'channelType',
            '#alias#.icon_fa' => 'channelIcon',
            '#alias#.icon_color' => 'channelIconColor',
            '#alias#.icon_bgr' => 'channelIconBackground',
            '#alias#.ordering' => 'channelOrdering',
        );
        if ($args['num_experts']) {
            $arr['(SELECT COUNT(id) FROM ' . App7020ChannelExperts::model()->tableName() . ' WHERE idChannel = #alias#.id)'] = 'channelExperts';
        }
        if ($args['num_visibility_structures']) {
//            $arr['@groups := '
//                . '('
//                . 'SELECT COUNT(id) FROM ' . App7020ChannelVisibility::model()->tableName() . ''
//                . ' WHERE '
//                . ' idChannel = #alias#.id '
//                . 'AND type = 1'
//                . ')'
//                ] = 'count_groups';
//            $arr['@branches := '
//                . '('
//                . 'SELECT COUNT(id) FROM ' . App7020ChannelVisibility::model()->tableName()
//                . ' INNER JOIN ' . CoreOrgChartTree::model()->tableName() . ' tree ON  idObject = tree.idOrg'
//                . ' INNER JOIN ' . CoreOrgChartTree::model()->tableName() . ' tree2 ON  tree2.iLeft >= tree.iLeft AND tree2.iRight <= tree.iRight'
//                . ' WHERE '
//                . 'idChannel = #alias#.id '
//                . 'AND type = 2'
//                . ')'] = 'count_branches';
            $str = '
			CONCAT_WS(
					", ",
					IF (
						@branches > 0,
						CONCAT_WS(" ", @branches, IF(@branches = 1,"' . Yii::t('app7020', 'Branch') . '", "' . Yii::t('app7020', 'Branches') . '")),NULL
					),
					IF (
						@groups > 0,
						CONCAT_WS(" ", @groups,  IF(@groups = 1,"' . Yii::t('app7020', 'Group') . '", "' . Yii::t('app7020', 'Groups') . '")),NULL
					),
					IF(@groups = 0 AND  @branches = 0 , "All groups and branches", null)
			)
		';

            $arr['@groups := '
                . '('
                . 'SELECT COUNT(id) FROM ' . App7020ChannelVisibility::model()->tableName() . ''
                . ' WHERE '
                . ' idChannel = #alias#.id '
                . 'AND type = 1'
                . ')'
                ] = 'count_groups';
            $arr['@branches_general := '
                . '('
                . 'SELECT COUNT(id) FROM ' . App7020ChannelVisibility::model()->tableName()
                . ' INNER JOIN ' . CoreOrgChartTree::model()->tableName() . ' tree ON  idObject = tree.idOrg'
                . ' INNER JOIN ' . CoreOrgChartTree::model()->tableName() . ' tree2 ON  tree2.iLeft >= tree.iLeft AND tree2.iRight <= tree.iRight'
                . ' WHERE '
                . 'idChannel = #alias#.id '
                . 'AND type = 2 '
                . 'AND selectState = 2'
                . ')'
                ] = 'branches_general';
            $arr['@branches_nested := '
                . '('
                . 'SELECT COUNT(id) FROM ' . App7020ChannelVisibility::model()->tableName()
                . ' INNER JOIN ' . CoreOrgChartTree::model()->tableName() . ' tree ON  idObject = tree.idOrg'
                 . ' WHERE '
                . 'idChannel = #alias#.id '
                . 'AND type = 2 '
                . 'AND selectState = 1 '
                . ')'
                ] = 'branches_nested';
            $arr['@branches := '
                . '('
                . '@branches_general + @branches_nested'
                . ')'
                ] = 'branches';
            
            $str = '	
			CONCAT_WS(
					", ",
					IF (
						@branches > 0,
						CONCAT_WS(" ", @branches, IF(@branches = 1,"' . Yii::t('app7020', 'Branch') . '", "' . Yii::t('app7020', 'Branches') . '")),NULL
					),
					IF (
						@groups > 0,
						CONCAT_WS(" ", @groups,  IF(@groups = 1,"' . Yii::t('app7020', 'Group') . '", "' . Yii::t('app7020', 'Groups') . '")),NULL
					),
					IF(@groups = 0 AND  @branches = 0 , "All groups and branches", null)
			)
		';
            $arr[$str] = 'visibility';
        }
        if ($args['friendly_permissions']) {
            $str_permissions = '(
			CASE c.permission_type 
				WHEN ' . self::PERMISSION_TYPE_EVERYONE . ' THEN "' . self::translatePermission(self::PERMISSION_TYPE_EVERYONE) . '"
				WHEN ' . self::PERMISSION_TYPE_EVERYONE_PR . ' THEN "' . self::translatePermission(self::PERMISSION_TYPE_EVERYONE_PR) . '"
				WHEN ' . self::PERMISSION_TYPE_EXPERTS_ONLY . ' THEN "' . self::translatePermission(self::PERMISSION_TYPE_EXPERTS_ONLY) . '"
			END   
			) ';

            $arr[$str_permissions] = 'upload_permissions';
        }
        return $arr;
    }

    /**
     *
     * @staticvar array $cache
     * @param type $onlyEnabledChannels
     * @param type $idUser
     * @param type $excludePredefined
     * @return array
     * @deprecated since version weekly69-sprint-14
     */
	public static function getUsersVisibleChannels($onlyEnabledChannels = false, $idUser = null, $excludePredefined = false) {

		static $cache = array();
		$cacheKey = md5(json_encode(func_get_args()));
		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}

		$condition = "LEFT JOIN core_user cu ON (cgm.idstMember=cu.idst OR cgm1.idstMember=cu.idst OR cgm22.idstMember=cu.idst) OR (cv.idChannel IS NULL)
						WHERE cu.idst = :idUser";

		if (!$idUser) {
			$idUser = Yii::app()->user->idst;
		}
		$params = array();

		$all_user_idst = Yii::app()->authManager->getAuthAssignmentsIds($idUser);
		$conditionGroups = " AND cgm.idst IN (" . implode(',', $all_user_idst) . ")";
		if(Yii::app()->user->getIsGodadmin()){
			$conditionGroups ='';
			if($onlyEnabledChannels && $excludePredefined){
				$condition = "WHERE c.enabled = 1  AND c.type = 2";
			}elseif($onlyEnabledChannels && !$excludePredefined){
				$condition = "WHERE c.enabled = 1  ";
			}elseif(!$onlyEnabledChannels && $excludePredefined){
				$condition = "WHERE  c.type = 2";
			}else{
				$condition = '';
			}
		}else{
			$params = array(
				':idUser' => $idUser
			);
			if($excludePredefined){
				$condition .= "  AND c.type = 2";
			}elseif($onlyEnabledChannels ){
				$condition .= " AND c.enabled = 1";
			}
		}


		$sql = "SELECT
			c.id as idChannel,
			cv.idObject AS idObject,
			cv.type as userSelectionType,
			cgm.idst AS idGroupDirect,
			cgm.idstMember AS idUserDirectGroup,
			tree1.idOrg as idOrgDirect,
			cgm1.idstMember as idUserBranchDirect,
			tree21.idOrg AS idOrgBandD,
			tree22.idOrg,
			cgm22.idstMember AS idUserBranchAndDesc,
			" . (int) $idUser . " as idFinalUser

			FROM app7020_channels c

			LEFT JOIN app7020_channel_visibility cv ON cv.idChannel = c.id
			LEFT JOIN core_group_members cgm ON (cv.type=1 AND cv.idObject=cgm.idst " . $conditionGroups . ")

			LEFT JOIN core_org_chart_tree tree1 ON (cv.type=2 AND cv.idObject=tree1.idOrg AND cv.selectState = 1)
			LEFT JOIN core_group_members cgm1 ON (cgm1.idst=tree1.idst_oc)

			LEFT JOIN core_org_chart_tree tree21 ON (cv.type=2 AND cv.idObject=tree21.idOrg AND cv.selectState = 2)
			LEFT JOIN core_org_chart_tree tree22 ON ((tree22.iLeft >= tree21.iLeft) AND (tree22.iRight <= tree21.iRight ))

			LEFT JOIN core_group_members cgm22 ON (cgm22.idst=tree22.idst_oc)
			".$condition;



		$sql .= " GROUP BY c.id";

		$command = Yii::app()->db->createCommand($sql);


		$data = $command->queryAll(true, $params);

		$cache[$cacheKey] = $data;

		return $data;
	}

    /**
     *
     * @param type $permissionType
     * @return type
     */
    static public function translatePermission($permissionType) {
        return isset(self::$permissions[$permissionType]['text']) ?
            Yii::t('app7020', self::$permissions[$permissionType]['text']) : '';
    }

    /**
     *
     * @return type
     */
    static public function getOrdering() {
        $sqlCommand = Yii::app()->db->createCommand();
        $sqlCommand->select("MAX(ordering)");
        $sqlCommand->from(self::model()->tableName());
        $dataArray = $sqlCommand->queryRow();
        $result = $dataArray['MAX(ordering)'] + 1;
        return $result;
    }

    /**
     * Assign user or users to THIS topic
     * @param integer|array $idUsers
     */
    public function assignExperts($idUsers) {

        if (!is_array($idUsers)) {
            $idUsers = array((int) $idUsers);
        }

        $counter = 1;
        $setArray = array();
        foreach ($idUsers as $idUser) {
            $param1 = ":idChannel" . $counter;
            $param2 = ":idExpert" . $counter;
            $setArray[] = "($param1, $param2)";
            $params[$param1] = $this->id;


            //check if the selected experts is present in Experts table - if not - add the user as expert
            $expertModel = App7020Experts::model()->findByAttributes(array('idUser' => $idUser));
            if (!$expertModel->id) {
                $expertModel = new App7020Experts();
                $expertModel->idUser = $idUser;
                $expertModel->idAdmin = Yii::app()->user->idst;
                $expertModel->save();
            }
            $params[$param2] = $expertModel->id;
            $counter++;
        }

        $result = 0;

        if (!empty($setArray)) {
            $sql = "INSERT IGNORE INTO app7020_channel_experts (idChannel, idExpert) VALUES " . implode(',', $setArray);
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->execute($params);
        }

        return $result;
    }

    /**
     * Return all sorting as string separated by come
     * @return string|boolean
     */
    function getAllSortAsString() {
        try {
            $output = array();
            foreach ($this->sort_items as $sort) {
                $output[] = $sort->sort->sortText;
            }
            return implode(', ', $output);
        } catch (Exception $ex) {
            Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
            return '';
        }
    }

    /**
     * Get users (experts) currently assigned to THIS topic
     * @return array  List of IDs
     */
    public function getAssignedExperts() {
        $command = Yii::app()->db->createCommand();
        $command
            ->select('e.idUser as eid')
            ->from(App7020ChannelExperts::model()->tableName() . ' ce')
            ->join(App7020Experts::model()->tableName() . ' e', 'ce.idExpert = e.id')
            ->where('ce.idChannel = :idChannel', array(':idChannel' => $this->id));
        return $command->queryColumn();
    }

    /**
     * Get users (experts) currently assigned to THIS topic
     * @return integer
     */
    public static function countExperts($idChannel) {
        $command = Yii::app()->db->createCommand();
        $command
            ->select('COUNT(*)')
            ->from(App7020ChannelExperts::model()->tableName() . ' ce')
            ->where('ce.idChannel = :idChannel', array(':idChannel' => $idChannel));

        return $command->queryScalar();
    }

    /**
     * Check if some channel requres the PR from an expert
     * @param array $channels_key_array stack channel id-s
     * @return boolean
     */
    public static function checkChannelsForPeerReviewReqirement($channels_key_array) {
        $command = Yii::app()->db->createCommand();
        $command
            ->select('COUNT(c.id) as counter')
            ->from(self::model()->tableName() . ' c')
            ->where('c.id  IN (' . implode(',', $channels_key_array) . ')')
            ->andWhere('c.permission_type = :permission_type', array(':permission_type' => App7020Channels::PERMISSION_TYPE_EVERYONE_PR));
        return (bool) $command->queryScalar();
    }

    /**
     * Returns a flat array of user IDs which can view the given channel
     * @param type $idChannel
     * @return type
     * @deprecated since version sprint-14
     */
    public static function getChannelUsersIds($idChannel, $notInvited = false, $idAsset = false) {

        $sql = "SELECT DISTINCT cu.idst
				FROM app7020_channels c
				LEFT JOIN app7020_channel_visibility cv ON cv.idChannel = c.id
				LEFT JOIN core_group_members cgm ON (cv.type=1 AND cv.idObject=cgm.idst)
				LEFT JOIN core_org_chart_tree tree1 ON (cv.type=2 AND cv.idObject=tree1.idOrg)
				LEFT JOIN core_group_members cgm1 ON (cgm1.idst=tree1.idst_oc)
				LEFT JOIN core_org_chart_tree tree21 ON (cv.type=3 AND cv.idObject=tree21.idOrg)
				LEFT JOIN core_org_chart_tree tree22 ON ( (tree22.iLeft >= tree21.iLeft) AND (tree22.iRight <= tree21.iRight ) )
				LEFT JOIN core_group_members cgm22 ON (cgm22.idst=tree22.idst_oc)
				LEFT JOIN core_user cu ON (cgm.idstMember=cu.idst OR cgm1.idstMember=cu.idst OR cgm22.idstMember=cu.idst) OR (cv.idChannel IS NULL)
				WHERE c.id = :idChannel and cu.idst>0 ";
        if ($notInvited == true && $idAsset) {
			$sql .="AND cu.idst NOT IN
				(
				SELECT idInvited
				from app7020_invitations
				Where idContent = " . $idAsset . ")";
		}
		$sql .= " GROUP BY cu.idst ";

		$command = Yii::app()->db->createCommand($sql);
		$params = array(
			':idChannel' => $idChannel
		);

		$data = $command->queryColumn($params);

		return $data;
	}

	/**
	 * Return list of Channel IDs of channels visible to all
	 * (defined as "missing visibility setting in channel visibility table)
	 *
	 * @param mixed $idChannel Integer or array of integer to filer
	 */
	public static function getVisibleToAllChannels($idChannel = null) {

		if ($idChannel) {
			if (!is_array($idChannel)) {
				$idChannel = array((int) $idChannel);
			}
		}
		$command = Yii::app()->db->createCommand();
		$command->select("c.id");
		$command->from("app7020_channels c");
		$command->leftJoin("app7020_channel_visibility cv", "cv.idChannel=c.id");
		$command->andWhere("cv.idChannel IS NULL");
		if ($idChannel) {
			$command->andWhere(array("IN", "c.id", $idChannel));
		}
		return $command->queryColumn();
	}

	public function afterSave() {
		parent::afterSave();
		/*
		  Yii::app()->event->raise(EventManager::EVENT_APP7020_CHANNEL_UPDATED, new DEvent($this, array(
		  'model' => $this,
		  )));
		 */
	}

	/**
	 * Check for visibility of user to channel instance
	 * @return boolean
	 * @version
	 * @author Stanimir Simeonov
	 */
	function isVisible() {
		return App7020ChannelVisibility::isExistVisibility($this->id);
	}

	/**
	 * return all user ids of current channel instance
	 * @param boolean $notInvited
	 * @param intege|boolean $idAsset
	 * @return array|boolean
	 * @author Stanimir Simeonov
	 */
	function channelUserIds($notInvited = false, $idAsset = false) {
		return self::extractUserIds($this->id, $notInvited, $idAsset);
	}

	/**
	 * Extract all the user ids who are presents in channel
	 * @param integer $idChannel
	 * @param boolean $notInvited
	 * @param integer|boolean $idAsset
	 * @return boolean|array
	 * @author Stanimir Simeonov
	 */
	static function extractUserIds($idChannel, $notInvited = false, $idAsset = false) {
		try {
			$ar = array(
				"idChannel" => $idChannel,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => true,
				"ignoreInvited" => $notInvited,
				"appendEnabledEffect" => true,
				"idsOnly" => true,
				"ignoreSuspendedUsers" => true,
				"pagination" => false
			);
			$dp = DataProvider::factory("ChannelUsers", $ar)->provider();
			return array_column($dp->data, 'id');
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			return false;
		}
	}

	/**
	 * Return ID of channels that user can see, we have criteria to see by assetID
	 * 
	 * @param array $args
	 * @param int $args['idUser']
	 * @param boolean $args['ignoreVisallChannels'] Ignote "All branches and Groups" visibility channels from common criteria 
	 * @param boolean $args['appendEnabledEffect'] If set this criteria to true, we extract only the enabled channels
	 * @param boolean $args['ignoreGodAdminEffect'] if this is set to true, SA are using same visibility criteria as normal user 
	 * @param boolean $args['ignoreSystemChannels'] If this criteria is set to true, PREDEFINED channels are skipped
	 * @param boolean $args['idsOnly'] If this criteria is set to true, we retrieve only the id
	 * @param boolean $args['ignoreChannels'] Flat array with channel ids that u want to skip it 
	 * @param int|boolean $args['idAsset'] Append Asset criteria 
	 * @param string $args['return'] Which kind of data you want to recieve: data, provicer, count
	 * @param array $args['customSelect'] Append custom select criteria to common provider
	 * @param array $args['sort'] Array with sorting criteria
	 * @param array $args['sort']['field']  append which kind of sorting u have to append to described field
	 * @param boolean $pagination
	 * @return boolean
	 * @throws Exception
	 */
	static function extractUserChannels($args = array(), $pagination = true /* ,$idUser, $byAssetId = false, $ignoreSystemChannels = true, $ignoreGodAdmin = true */) {
		$args_template = array(
			"ignoreVisallChannels" => false,
			"appendEnabledEffect" => true,
			"ignoreGodAdminEffect" => false,
			"ignoreSystemChannels" => false,
			'idsOnly' => false,
			'idAsset' => false,
			'return' => 'data', // have to be data, provider, count
			'customSelect' => false
		);

		if(trim(Yii::app()->request->getParam('search_input', false))){
			$args_template['search'] = trim(Yii::app()->request->getParam('search_input', false));
		}


		//finish criterias
		$args_process = array_merge($args_template, $args);
		if (!isset($args_process['idUser'])) {
			throw new Exception('User Id for channel visibility is required param!');
		}
		if (!$pagination) {
			$args_process['pagination'] = false;
		}
		try {
			$dp = DataProvider::factory("UserChannels", $args_process)->provider();
			if (!empty($args_process['sort'])) {
                $dp->setSort(array('attributes' => $args_process['sort']));
                $default = array();
                foreach ($args_process['sort'] as $sorting_field => $sorting) {
                    $default[$sorting_field] = $args_process['sort'][$sorting_field]['default'];
                }
                $dp->sort->defaultOrder = $default;
            }
            if ($args_process['idsOnly']) {
                return (array_unique(array_column($dp->data, 'id')));
            } else if ($args_process['return'] == 'data') {
                return $dp->data;
            } else if ($args_process['return'] == 'count') {
                return $dp->totalItemCount;
            } else if ($args_process['return'] == 'provider') {
                return $dp;
            }
        } catch (Exception $ex) {
            Log::_($ex->getMessage());
            return false;
        }
    }

}
