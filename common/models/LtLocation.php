<?php

/**
 * This is the model class for table "lt_location".
 *
 * The followings are the available columns in table 'lt_location':
 * @property integer $id_location
 * @property string $name
 * @property string $address
 * @property integer $id_country
 * @property string $telephone
 * @property string $email
 * @property string $reaching_info
 * @property string $accomodations
 * @property string $other_info
 *
 * The followings are the available model relations:
 * @property LtCourseSessionDate[] $learningCourseSessionDates
 * @property CoreCountry $country
 * @property LtLocationPhoto[] $photos
 */
class LtLocation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lt_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, id_country', 'required'),
			array('id_country', 'numerical', 'integerOnly'=>true),
			array('name, address, email', 'length', 'max'=>255),
			array('telephone', 'length', 'max'=>45),
			array('reaching_info, accomodations, other_info', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_location, name, address, id_country, telephone, email, reaching_info, accomodations, other_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningCourseSessionDates' => array(self::HAS_MANY, 'LtCourseSessionDate', 'id_location'),
			'country' => array(self::BELONGS_TO, 'CoreCountry', 'id_country'),
			'photos' => array(self::HAS_MANY, 'LtLocationPhoto', 'id_location'),
			'classrooms' => array(self::HAS_MANY, 'LtClassroom', 'id_location'),
			'powerUserManagers' => array(self::MANY_MANY, 'CoreUser', 'core_user_pu_location(location_id, puser_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_location' => Yii::t('classroom', 'Location id'),
			'name' => Yii::t('classroom', 'Location name'),
			'address' => Yii::t('billing', '_ADDRESS'),
			'id_country' => Yii::t('standard', '_COUNTRY'),
			'telephone' => Yii::t('classroom', 'Location telephone number'),
			'email' => Yii::t('classroom', 'Location email'),
			'reaching_info' => Yii::t('classroom', 'How to reach the location'),
			'accomodations' => Yii::t('classroom', 'Suggested accomodations'),
			'other_info' => Yii::t('classroom', 'Other info'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_location',$this->id_location);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('id_country',$this->id_country);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('reaching_info',$this->reaching_info,true);
		$criteria->compare('accomodations',$this->accomodations,true);
		$criteria->compare('other_info',$this->other_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LtLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function behaviors() {
		return array(
			'modelSelect' => array(
				'class' => 'ModelSelectBehavior'
			),
		);
	}



	public function beforeDelete() {
		$returnValue = parent::beforeDelete();
		if (true === $returnValue) {
			// delete location photos
			$photos = $this->photos;
			if (is_array($photos) && count($photos) > 0) {
				foreach ($photos as $photo) {
					$photo->delete();
				}
			}
		}
		return $returnValue;
	}

	public function afterDelete()
	{
		$returnValue = parent::beforeDelete();
		if($returnValue)
			CoreUserPuLocation::model()->deleteAllByAttributes(array('location_id' => $this->id_location));

		return $returnValue;
	}


	/**
	 * Used in power user management for locations assignment
	 * @var \CoreUser
	 */
	public $powerUserManager = NULL;



	public function dataProvider() {
		$config = array();
		$criteria = new CDbCriteria();

		if (!empty($this->name)) {
			$criteria->addCondition('t.name LIKE :search');
			$criteria->params[':search'] = "%$this->name%";
		}

		$criteria->with = array('country');

		switch($this->scenario) {
			case 'power_user_management':
			case 'power_user_assigned':
				$criteria->with['powerUserManagers'] = array(
					'joinType' => 'INNER JOIN',
					'condition' => 'powerUserManagers.idst=:puser_id',
					'params' => array(':puser_id' => $this->powerUserManager->idst),
				);
				$criteria->together = true;
				$criteria->group = 't.id_location';
			break;
			case 'power_user_assign':
				$criteria->with['powerUserManagers'] = array(
					'joinType' => 'LEFT JOIN',
				);
				$puLocations = array();
				$tmpCriteria = new CDbCriteria();
				$tmpCriteria->addCondition("puser_id = :puser_id");
				$tmpCriteria->params[':puser_id'] = $this->powerUserManager->idst;
				$tmpList = CoreUserPuLocation::model()->findAll($tmpCriteria);
				if (!empty($tmpList)) { foreach ($tmpList as $tmpItem) { $puLocations[] = $tmpItem->location_id; } }
				if (!empty($puLocations)) {
					$criteria->with['powerUserManagers']['condition'] = 't.id_location NOT IN ('
						. implode(',', $puLocations)
						. ') OR powerUserManagers.idst IS NULL';
				}
				$criteria->together = true;
				$criteria->group = 't.id_location';
			break;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = array('name' => false);
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}




	/**
	 * Returns a formatted address
	 * Eg: street Name - Country name | Country name
	 * @return string
	 */
	public function renderAddress() {
		$address = array();
		if (!empty($this->address)) {
			$address[] = trim($this->address);
		}
		if (!empty($this->country)) {
			$address[] = $this->country->name_country;
		}
		return implode(' - ', $address);
	}


	/**
	 * Returns true if optional information is available for the location
	 * @return bool
	 */
	public function hasExtraInfo() {
		return (!empty($this->reaching_info) || !empty($this->accomodations) || !empty($this->other_info) || !empty($this->photos));
	}


	protected $_cache_country;


	public function getCountry() {
		if ($this->_cache_country !== NULL) {
			//first try to read cache; if it's filled, then check validity and return cached value
			if ($this->_cache_country->id_country== $this->id_country) { return $this->_cache_country; }
			//if cache value is not valid, nullify it and retrieve data from DB
			$this->_cache_country = NULL;
		}
		//retrieve location info from DB
		$this->_cache_country = ucwords(strtolower( CoreCountry::model()->findByPk($this->id_country) ));
		return $this->_cache_country;
	}

	/**
	 * Returns true if a location is used in at least one session date in the future
	 * @param $locationId
	 * @return bool
	 */
	public static function isUsedInFutureSessionDate($locationId) {
		$activeSessionDates = Yii::app()->db->createCommand()
			->select('id_session, CONVERT_TZ(CONCAT_WS(" ", day, time_end), timezone, "UTC") AS utcEndTimestamp')
			->from(LtCourseSessionDate::model()->tableName())
			->where('id_location = :id_location', array(':id_location' => $locationId))
			->having('utcEndTimestamp > :utcNow', array(':utcNow' => Yii::app()->localtime->getUTCNow()))
			->queryAll();

		if(count($activeSessionDates) > 0)
			return true;
		else
			return false;
	}

}
