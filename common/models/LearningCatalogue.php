<?php

/**
 * This is the model class for table "learning_catalogue".
 *
 * The followings are the available columns in table 'learning_catalogue':
 * @property integer $idCatalogue
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property LearningCatalogueEntry[] $catalog_entries
 * @property LearningCatalogueMember[] $catalog_members
 * @property LearningCourse $course
 * @property LearningCoursepath $coursepath
 */
class LearningCatalogue extends CActiveRecord {

	/**
	 * Used when reading catalogues and the number of courses in them
	 * @var int
	 */
	public $course_count;
	public $confirm;
	public $search_input;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningCatalogue the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_catalogue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'type', 'type' => 'string', 'allowEmpty' => TRUE),
			array('name', 'length', 'max'=>255),
			array('name', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCatalogue, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catalog_entries' => array(self::HAS_MANY, 'LearningCatalogueEntry', 'idCatalogue'),
			'catalog_members' => array(self::HAS_MANY, 'LearningCatalogueMember', 'idCatalogue')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idCatalogue' => 'Id Catalogue',
			'name' => Yii::t('standard', '_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCatalogue',$this->idCatalogue);
		$criteria->compare('name', $this->name, TRUE);
		$criteria->compare('description', $this->description, TRUE);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function dataProvider($powerUserId = null, $search = null, $exceptCatalogsIds = null) {
		$criteria=new CDbCriteria;

		if($powerUserId != null) {
			$criteria->join = 'JOIN '.CoreAdminCourse::model()->tableName().' cac ON idCatalogue = cac.id_entry';
			$criteria->addCondition('cac.idst_user = '.$powerUserId.' AND cac.type_of_entry = "'.CoreAdminCourse::TYPE_CATALOG.'"');
		}

		if($search != null) {
			$criteria->addCondition('name LIKE :name OR description LIKE :name');
			$criteria->params[':name'] = '%'.$search.'%';
		}

		if($exceptCatalogsIds != null){
			$criteria->addNotInCondition('idCatalogue', $exceptCatalogsIds);
		}
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.idCatalogue desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function sqlDataProvider( $filterObject = false, $pageSize = false ) {

		// Start building SQL parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand()
		->from('learning_catalogue c');

		if ( $filterObject->search_input ) {
			$commandBase->andWhere('c.name LIKE :name');
			$params[':name'] = "%". $filterObject->search_input ."%";
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName]['asc'] 	= 'c.'.$attributeName.' asc';
			$sortAttributes[$attributeName]['desc'] = 'c.'.$attributeName.' desc';
		}

		// DATA
		$commandData = clone $commandBase;

		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(c.idCatalogue) AS S');
		$numRecords = $commandCounter->queryScalar($params);

		$commandData->select('*');

		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = $sortAttributes;
		$sort->defaultOrder = array(
				'idCatalogue'		=>	CSort::SORT_DESC,
		);

		// END
		$pagination = array();
		if ($pageSize) {
			$pagination = array('pageSize' => $pageSize);
		}

		$config = array(
			'totalItemCount'	=> $numRecords,
			'pagination' 		=> $pagination,
			'sort' 				=> $sort,
			'params'			=> $params,
			'keyField'			=> 'idCatalogue',
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData->gettext(), $config);

		return $dataProvider;
	}


	public function renderCatalogUsers($customIcon = false, $onlyNumber = false) {
		/*
		$criteria = new CDbCriteria();
		$criteria->compare('idCatalogue', $this->idCatalogue);
		$countAssignUsers = count(LearningCatalogueMember::model()->findAll($criteria));
		*/
		$countAssignUsers = 0;
		//this query will filter idst selecting only existing ones and avoiding "ghost" assignments
		$reader = Yii::app()->db->createCommand("(SELECT COUNT(cm.idst_member) AS num, 'user' AS member_type
			FROM learning_catalogue_member cm
			JOIN core_user u ON (cm.idst_member = u.idst AND cm.idCatalogue = :id_catalog)
			".(Yii::app()->user->getIsPu() ? "JOIN ".CoreUserPU::model()->tableName()." pu ON (u.idst = pu.user_id AND puser_id = ".Yii::app()->user->id.")" : "").")
			UNION
			(SELECT COUNT(cm.idst_member) AS num, 'group' AS member_type
			FROM learning_catalogue_member cm
			JOIN core_group g on (cm.idst_member = g.idst AND cm.idCatalogue = :id_catalog))")->query(array(':id_catalog' => $this->idCatalogue));
		if ($reader) {
			while ($row = $reader->read()) {
				$countAssignUsers += (int)$row['num'];
			}
		}
		if($onlyNumber === true){
			return $countAssignUsers;
		}
		if($customIcon === true){
			$icon = '<span class="i-sprite is-couple"></span>';
			return $countAssignUsers . $icon;
		}else{
			$icon = ($countAssignUsers > 0) ? '<span class="assignUsers"></span>' : '<span class="assignUsers yellow"></span>';
		}

		$content = CHtml::link($countAssignUsers . $icon, Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/assignUsers', array('id' => $this->idCatalogue)));

		return $content;
	}


	public function renderCatalogCourses($customIcon = false, $onlyNumber = false) {
		$totalCount = 0;

		//count courses first
		$command = Yii::app()->db->createCommand();
		$command->select("COUNT(*)");
		$command->from(LearningCatalogueEntry::model()->tableName()." ce");
		$command->where("ce.idCatalogue = :idCatalogue", array(":idCatalogue" => $this->idCatalogue));

		// If Classroom is NOT active, count only courses (exclude classrooms)
		// And always JOIN Course table to get really exisitng courses
		if (!PluginManager::isPluginActive('ClassroomApp')) {
			$command->join(LearningCourse::model()->tableName()." c", "(ce.idEntry=c.idCourse) AND ce.type_of_entry = 'course' AND (c.course_type <> :classroom)", array(':classroom' => LearningCourse::TYPE_CLASSROOM));
		} else {
			$command->join(LearningCourse::model()->tableName()." c", "(ce.idEntry=c.idCourse) AND ce.type_of_entry = 'course'");
		}

		if(Yii::app()->user->getIsPu())
			$command->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = "  . (int)Yii::app()->user->id .   " AND puc.course_id = c.idCourse");

		$totalCount += $command->queryScalar();

		//then learning plans as well
		$command = Yii::app()->db->createCommand();
		$command->select("COUNT(*)");
		$command->from(LearningCatalogueEntry::model()->tableName()." ce");
		$command->where("ce.idCatalogue = :idCatalogue", array(":idCatalogue" => $this->idCatalogue));
		$command->join(LearningCoursepath::model()->tableName()." c", "(ce.idEntry=c.id_path) AND ce.type_of_entry = 'coursepath'");

		if(Yii::app()->user->getIsPu())
			$command->join(CoreUserPuCoursepath::model()->tableName()." puc", "puc.puser_id = "  . (int) Yii::app()->user->id .   " AND puc.path_id = c.id_path");

		$totalCount += $command->queryScalar();
		if($onlyNumber === true){
			return $totalCount;
		}
		if($customIcon === true){
			$icon = '<span class="i-sprite is-forum"></span>';
			return $totalCount . $icon;
		}else{
			$icon = ($totalCount > 0) ? '<span class="assignCourses"></span>' : '<span class="assignCourses yellow"></span>';
		}

		$content = CHtml::link($totalCount . $icon, Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/assignCourses', array('id' => $this->idCatalogue)));

		return $content;
	}

	/**
	 * Returns a list of course Ids from a given Catalog Id (like "1,5,78,76...")
	 *
	 * @param int $catalogId
	 *
	 * @param bool $imploded
	 *
	 * @return string
	 */
	public static function getCatalogCoursesList($catalogId, $imploded = TRUE) {
	    $criteria = new CDbCriteria();
	    $courseIds = array();
	    $coursesList = "";

	    // Make sure the parameter is an array
	    if (!is_array($catalogId)) {
	    	$catalogId = array($catalogId);
	    }

		if (is_array($catalogId)) {
			$criteria->addInCondition('idCatalogue', $catalogId);
		}

		// $criteria->compare('type_of_entry', 'course');

	    $items = LearningCatalogueEntry::model()->findAll($criteria);

	    foreach ($items as $item) {
	        $courseIds[] = $item->idEntry;
	    }
		if (!$imploded) {
			return $courseIds;
		}
	    $coursesList = implode(',', $courseIds);

	    return $coursesList;

	}


	/**
	 * Get list of PLANS in catalogue or catalogues
	 *
	 * @param array $catalogs
	 */
	public static function getCatalogPlansList($catalogs) {

	    if (!array($catalogs))
	        $catalogs = array($catalogs);

	    $plans = Yii::app()->db->createCommand()
	       ->select('idEntry')
	       ->from(LearningCatalogueEntry::model()->tableName())
	       ->andWhere("type_of_entry='coursepath'")
	       ->andWhere(array("IN", "idCatalogue", $catalogs))
	       ->queryColumn();

	    return array_unique($plans);

	}

	/**
	 * Get list of COURSES in catalogue or catalogues
	 *
	 * @param array $catalogs
	 */
	public static function getCatalogCoursesListV2($catalogs, $types=false) {

	    if (!array($catalogs))
	        $catalogs = array($catalogs);

	    $command = Yii::app()->db->createCommand();

	    $command->select('idEntry')
	       ->from(LearningCatalogueEntry::model()->tableName() . ' ce')
	       ->andWhere("type_of_entry='course'")
	       ->andWhere(array("IN", "idCatalogue", $catalogs));

	    if (is_array($types) && !empty($types)) {
	        $command->join('learning_course course', 'course.idCourse=ce.idEntry');
	        $command->andWhere(array('IN', 'course_type', $types));
	    }

	    $courses = array_unique($command->queryColumn());

	    return $courses;

	}


    /**
     * Returns a list of catalog IDs a user is assigned to
	 *
     * @param $userId
     */
    public static function getUserCatalogIds($idUser) {

        static $cache = array();
        $cacheKey = md5($idUser);

        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }


        $catalogIds = array();

        // Get the ID of groups the user belongs to
        $authManager = Yii::app()->authManager;
        $idsts = $authManager->getAuthAssignmentsIds($idUser);

        if ($idsts) {
            // Also include catalogs directly assigned to the current user
            $idsts[] = $idUser;

            $criteria = new CDbCriteria();
            $criteria->alias = 'c';
            $criteria->select = "c.idCatalogue";
            $criteria->with = array(
                "catalog_members" => array(
                    'alias' => 'cm',
                )
            );

            // Since a catalog may have a "orgchart+descendats" assigned to it,
            // walk though the current user's groups upwards to their parent
            // groups and get those groups' OCD IDs. Then if one of those IDs
            // match an ID assigned to a catalog, we should include this catalog,
            // since the user is a member of a descendant group of a
            // "group+descendants" assigned to the catalog.
            $ocdArr = array();
            $c2 = new CDbCriteria();
            $c2->addCondition('groupMembers.idstMember=:idstMember');
            $c2->params[':idstMember'] = $idUser;
            $c2->with = array('groupMembers');
            $c2->select = 't.lev, t.iLeft, t.iRight';
            $userGroups = CoreOrgChartTree::model()->findAll($c2);
            foreach($userGroups as $group){
                $tempCriteria = new CDbCriteria();
                $tempCriteria->addCondition('t.iLeft <= :currentLeft');
                $tempCriteria->addCondition('t.iRight >= :currentRight');
                $tempCriteria->params[':currentLeft'] = $group->iLeft;
                $tempCriteria->params[':currentRight'] = $group->iRight;
                $groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);
                if($groupParent) {
                    foreach($groupParent as $parent){
                        $ocdArr[] = $parent->idst_ocd;
                        if($group->iLeft === $parent->iLeft)
                            $ocdArr[] = $parent->idst_oc;
                    }
                }
            }
            $ocdArr = array_unique($ocdArr);
            $idsts = array_merge($idsts, $ocdArr);
            $criteria->addCondition("cm.idst_member in (" . implode(",", $idsts) . ")");
            $userCatalogs = LearningCatalogue::model()->findAll($criteria);
            foreach($userCatalogs as $catalog)
                $catalogIds[] = $catalog->idCatalogue;
        }

        $cache[$cacheKey] = $catalogIds;

        return $catalogIds;
    }

	/**
	 * Search database and return set of courses (one page) for so called 'catalog' grid
	 *
	 * @param null $idUser May be null for the external catalog
	 * @param int $offset
	 * @param int $limit
	 * @param array $flt list of filter parameters
	 *
	 * @return array
	 */
	public static function getCatalogCoursesPage($idUser = NULL, $offset = 0, $limit = 0/*, $flt_type = '', $flt_query = '', $fltCatalog = ''*/, $flt, $loadJplayer = true) {

		if (Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		$flt_type = (is_array($flt) && isset($flt['type']) ? $flt['type'] : '');
		$flt_query = (is_array($flt) && isset($flt['query']) ? $flt['query'] : '');
		$fltCatalog = (is_array($flt) && isset($flt['catalog']) ? $flt['catalog'] : '');
		$fltCategory = (is_array($flt) && isset($flt['category']) ? $flt['category'] : '');

		$criteria = new CDbCriteria;
		$criteria->alias = 'course';

		// Getting only courses that have status EFFECTIVE or AVAILABLE
		$criteria->addInCondition('status', array(LearningCourse::$COURSE_STATUS_EFFECTIVE, LearningCourse::$COURSE_STATUS_AVAILABLE));

		if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
			$criteria->compare('selling', 1);
		} else {
			if ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
				$criteria->compare('selling', 0);
			}
		}

		// Search text, search by name, other text fields can be searched
		if (!empty($flt_query)) {
			$criteria->addCondition("name LIKE '%".addslashes(str_replace(' ', '%', ${flt_query}))."%' OR description LIKE '%".addslashes(str_replace(' ', '%', ${flt_query}))."%'");
		}

		//check user catalogs visibility
		$associatedCatalogs    = (empty($idUser) ? FALSE : array());
		$associatedCourses     = (empty($idUser) ? FALSE : array());
		$associatedCoursepaths = (empty($idUser) ? FALSE : array());
		if (!empty($idUser)) {
			//find user's associated catalogs
			$associatedCatalogs = LearningCatalogue::getUserCatalogs($idUser);
		}

		if (is_array($associatedCatalogs)) {
			if (empty($associatedCatalogs)) {
				//no assigned catalogs for the user
				if (Settings::get('on_catalogue_empty') === 'on') {
					$associatedCourses = FALSE;
				} else {
					return array(
						'total' => 0,
						'courses' => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			} else {
				//retrieve associated courses
				$associatedCourses = Yii::app()->getDb()->createCommand()
					->select('idEntry')
					->from(LearningCatalogueEntry::model()->tableName())
					->where('type_of_entry="course"')
					->andWhere(array('IN', 'idCatalogue', $associatedCatalogs))
					->queryColumn();

				if (empty($associatedCourses)) {
					return array(
						'total' => 0,
						'courses' => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}

				$associatedCoursepaths = Yii::app()->getDb()->createCommand()
					->select('idEntry')
					->from(LearningCatalogueEntry::model()->tableName())
					->where('type_of_entry="coursepath"')
					->andWhere(array('IN', 'idCatalogue', $associatedCatalogs))
					->queryColumn();
			}
		}

		// filter on course category
		if (Settings::get('catalog_use_categories_tree') === 'on' && !empty($fltCategory)) {
			$categories = array((int)$fltCategory);
			//uncategorized courses are considered as contained into root
			$_roots = LearningCourseCategoryTree::model()->roots()->findAll();
			if (empty($_roots)) {
				throw new CException('Invalid categories tree');
			}
			$rootCategory = $_roots[0];
			if ($fltCategory == $rootCategory->idCategory) {
				$categories[] = 0;
			}
			//find subcategories
			$_node = LearningCourseCategoryTree::model()->findByPk($fltCategory);
			$_descendants = $_node->descendants()->findAll();
			if (!empty($_descendants)) {
				foreach ($_descendants as $_descendant) {
					$categories[] = (int)$_descendant->idCategory;
				}
			}
			//add condition to criteria
			if (!empty($fltCategory) && (int)$fltCategory > 0) {
				$criteria->addInCondition("course.idCategory", $categories);
			}
		}

		$subscribedCoursesList = array();
		if ($idUser) {
			$subscribedCoursesList = CoreUser::model()->getSubscribedCoursesList($idUser, TRUE);
		} else {
			// Display mode options - Everyone, and show on home page
			$criteria->compare('show_rules', LearningCourse::DISPLAY_EVERYONE_HOMEPAGE);
		}


		// Filter by Catalog(s) ?
		if ($fltCatalog != '') {
			$catalogCoursesList = self::model()->getCatalogCoursesList($fltCatalog, FALSE);
			if (is_array($associatedCourses)) {
				$catalogCoursesList = array_intersect($catalogCoursesList, $associatedCourses);
				if (empty($catalogCoursesList)) {
					return array(
						'total' => 0,
						'courses' => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			}

			if (!empty($catalogCoursesList)) {
				//$criteria->addCondition("course.idCourse in ($catalogCoursesList)");
				$criteria->addInCondition("course.idCourse", $catalogCoursesList);
			} else {
				$criteria->addCondition("false");
			}
		} else {
			if (is_array($associatedCourses)) {
				if (!empty($associatedCourses)) {
					$criteria->addInCondition("course.idCourse", $associatedCourses);
				} else {
					$criteria->addCondition("false");
				}
			}
		}

		// Get only FREE courses if e-commerce is disabled
		$eCommerceEnabled = Settings::get("module_ecommerce", "off") == "on";
		$shopifyEnabled = PluginManager::isPluginActive('ShopifyApp');
		if (!$eCommerceEnabled && !$shopifyEnabled) {
			$criteria->addCondition("selling = 0");
		}

		// select only "elearning" type, ignore "classroom" etc.
		//$criteria->addCondition("course_type='elearning'");
		// Set Offset/Limit
		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		// We remove the courses having show_rules = Only users subscribed to the course
		// and the user is not subscribed.
		$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCoursesList);

        $excludedCourses = array();
        Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent(self, array(
            'excludedCourses' => &$excludedCourses
        )));
        if (!empty($excludedCourses)) {
            $coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $excludedCourses);
        }

		$criteria->addNotInCondition('idCourse', $coursesToNotDisplayList);

		$orderBy = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = $orderBy;

		// Get models ('page' or portion)
		$courses = LearningCourse::model()->findAll($criteria);


		// mark the courses that the current user is subscribed to
		if (!empty($courses) && !empty($subscribedCoursesList)) {
			foreach ($courses as $key => $course) {
				/* @var $course LearningCourse */
				if (in_array($course->idCourse, $subscribedCoursesList)) {
					$course->isCurrentUserSubscribedTo = TRUE;
				}
			}
		}


		// Count all filtered courses first
		$coursesCount = LearningCourse::model()->count($criteria);

		// A bit more work: Check if there are courses for sale and for free AT ALL!!
		$hasForSale = LearningCourse::model()->exists('selling=1');
		$hasForFree = LearningCourse::model()->exists('selling=0');

		$result = array(
			'total'        => $coursesCount,
			'courses'      => $courses,
			'has_for_sale' => $hasForSale,
			'has_for_free' => $hasForFree,
		);


		return $result;
	}

	public static function getCatalogCoursesPageNew($idUser = NULL, $offset = 0, $limit = 0/*, $flt_type = '', $flt_query = '', $fltCatalog = ''*/, $flt, $order = null) {

		if (Yii::getPathOfAlias('jplayer') === false)
			Yii::setPathOfAlias('jplayer', Yii::app()->basePath.'/../../common/extensions/jplayer');

		$assetsPath = Yii::getPathOfAlias('jplayer.assets');
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
		Yii::app()->getClientScript()->registerCssFile($assetsUrl . "/skin/dialog2_videoplayer/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($assetsUrl . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		$flt_type    = (is_array($flt) && isset($flt['type']) ? $flt['type'] : '');
		$flt_query   = (is_array($flt) && isset($flt['query']) ? $flt['query'] : '');
		$fltCatalog  = (is_array($flt) && isset($flt['catalog']) ? $flt['catalog'] : '');
		$fltCategory = (is_array($flt) && isset($flt['category']) ? $flt['category'] : '');

		$criteria        = new CDbCriteria;

		// Getting only courses that have status EFFECTIVE or AVAILABLE
		//$criteria->addInCondition('course.status', array(LearningCourse::$COURSE_STATUS_EFFECTIVE, LearningCourse::$COURSE_STATUS_AVAILABLE));

		$courseOnCondition = 'course.status IN (:effective, :available)';
		$coursePathOnCondition = 'coursepath.visible_in_catalog=1';
		$criteria->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
		$criteria->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;

		// Get only FREE courses if e-commerce is disabled
		$eCommerceEnabled = Settings::get("module_ecommerce", "off") == "on";
		$shopifyEnabled = PluginManager::isPluginActive('ShopifyApp');
		if(!$eCommerceEnabled && !$shopifyEnabled){
			$flt_type = CoursesGrid::$FILTER_TYPE_FREE;
		}

		if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
			$courseOnCondition .= ' AND course.selling=1 ';
			$coursePathOnCondition .= ' AND coursepath.is_selling=1 ';
		} elseif ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
			$courseOnCondition .= ' AND course.selling=0 ';
			$coursePathOnCondition .= ' AND coursepath.is_selling=0 ';
		}

		// Search text, search by name, other text fields can be searched
		if (!empty($flt_query)) {
			$courseOnCondition .= ' AND (course.name LIKE :search OR course.description LIKE :search OR course.code LIKE :search) ';
			$coursePathOnCondition .= ' AND (coursepath.path_name LIKE :search OR coursepath.path_descr LIKE :search OR coursepath.path_code LIKE :search) ';
			$criteria->params[':search'] = '%'.str_replace(' ', '%', $flt_query).'%';
		}

		//check user catalogs visibility
		$associatedCatalogs    = (empty($idUser) ? FALSE : array());
		$associatedCourses     = (empty($idUser) ? FALSE : array());

		if(!empty($fltCategory) && Settings::get('catalog_use_categories_tree') === 'on'){
			// Filtering by category

			$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);

			// add condition to criteria
			if (!empty($fltCategory) && (int) $fltCategory > 0) {
				$courseOnCondition .= ' AND course.idCategory IN ('.implode(',', $categories).') ';
			}
		}

		// Filter by Catalog id ? (multicatalog)
		if ($fltCatalog != '') {
			$criteria->compare('t.idCatalogue', $fltCatalog);
		}

		$subscribedCoursesList = array();
		if ($idUser) {
			$subscribedCoursesList = CoreUser::model()->getSubscribedCoursesList($idUser, TRUE);
		} else {
			// Display mode options - Everyone, and show on home page
			$courseOnCondition .= ' AND course.show_rules = :show_on_homepage ';
			$criteria->params[':show_on_homepage'] = LearningCourse::DISPLAY_EVERYONE_HOMEPAGE;
		}

		// We remove the courses having show_rules = Only users subscribed to the course
		// and the user is not subscribed.
		$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCoursesList);
		$coursesExcludedByPlugins = array();
		Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent(self, array(
			'excludedCourses' => &$coursesExcludedByPlugins
		)));
		if (!empty($coursesExcludedByPlugins)) {
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $coursesExcludedByPlugins);
		}

		if(!empty($coursesToNotDisplayList)){
			// Add the exlude courses condition to the main criteria
			$courseOnCondition .= ' AND course.idCourse NOT IN ('.implode(',', $coursesToNotDisplayList).') ';
		}

		if ($idUser) {
			// find user's associated catalogs
			// and if they have at least some Course or Learning Plan
			$associatedCatalogs = LearningCatalogue::getUserCatalogs($idUser);

			if(!empty($associatedCatalogs)){
				$c2 = new CDbCriteria();
				$c2->compare('idCatalogue', $associatedCatalogs);
				$userAssignedCatalogsHaveEntries = LearningCatalogueEntry::model()->exists($c2);
				if(!$userAssignedCatalogsHaveEntries){
					return array(
						'total'        => 0,
						'courses'      => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			}

			if(empty($associatedCatalogs)){
				if(Settings::get('on_catalogue_empty') == 'on'){
					// The setting "If no catalogs assigned to user
					// show all courses" is enabled

					// Get all courses and learning plans (using offset and limit)

					$allCoursesCriteria = new CDbCriteria();
					$allCoursesCriteria->addNotInCondition('idCourse', $coursesToNotDisplayList);
					// Display mode options - Everyone, and show on home page
					$allCoursesCriteria->compare('show_rules', LearningCourse::DISPLAY_EVERYONE_HOMEPAGE);

					if(!empty($fltCategory) && Settings::get('catalog_use_categories_tree') === 'on'){
						// Filtering by category
						$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);
						if (!empty($fltCategory) && (int) $fltCategory > 0) {
							$allCoursesCriteria->compare('idCategory', $categories);
						}
					}
					if (!empty($flt_query)) {
						$allCoursesCriteria->addCondition('name LIKE :search OR description LIKE :search OR code LIKE :search');
						$allCoursesCriteria->params[":search"] = '%'.str_replace(' ', '%', $flt_query).'%';
					}

					$allCoursesCriteria->addInCondition('status', array(LearningCourse::$COURSE_STATUS_EFFECTIVE, LearningCourse::$COURSE_STATUS_AVAILABLE));

					if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
						$allCoursesCriteria->compare('selling', "1");
					} elseif ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
						$allCoursesCriteria->compare('selling', "0");
					}
					// Select only some columns to reduce overhead
					$allCoursesCriteria->select = 'selling, can_subscribe, name, description, status, idCourse, allow_overbooking, max_num_subscribe, img_course';
					$allCourses = LearningCourse::model()->findAll($allCoursesCriteria);

					$allLpsCriteria = new CDbCriteria();
					if (!empty($flt_query)) {
						$allLpsCriteria->addCondition('path_name LIKE :search OR path_descr LIKE :search OR path_code LIKE :search');
						$allCoursesCriteria->params[":search"] = '%'.str_replace(' ', '%', $flt_query).'%';
						$allLpsCriteria->params[":search"] = '%'.str_replace(' ', '%', $flt_query).'%';
					}
					// For now we are just displaying only LPs that are selling
                    if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
                        $allLpsCriteria->compare('is_selling', "1");
                    } elseif ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
                        $allLpsCriteria->compare('is_selling', "0");
                    }
					$allLpsCriteria->compare('visible_in_catalog', "1");
					$allLpsCriteria->select = 'id_path, path_name, path_descr, is_selling, img, price';

					$allLearningPlans = LearningCoursepath::model()->findAll($allLpsCriteria);

					$allGridItems = array_merge($allCourses, $allLearningPlans);

					$entries = array_slice($allGridItems, $offset, $limit);

					return array(
						'total'        => count($allGridItems),
						'courses'      => $entries,
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}else{
					return array(
						'total'        => 0,
						'courses'      => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			}else{
				// User has some catalogs assigned
				$userLearningPlanIds = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser(Yii::app()->user->getIdst());

				if(!empty($userLearningPlanIds)){
					// Hide free LPs if the user is not enrolled in them
					$coursePathOnCondition .= ' AND (coursepath.id_path IN ('.implode(',', $userLearningPlanIds).') OR coursepath.is_selling=1)';
				}
			}
		}else{
			// Anonymous user/external catalog
			// Get all courses and learning plans (using offset and limit)

			$allCoursesCriteria = new CDbCriteria();
			$allCoursesCriteria->addNotInCondition('idCourse', $coursesToNotDisplayList);
			// Display mode options - Everyone, and show on home page
			$allCoursesCriteria->compare('show_rules', LearningCourse::DISPLAY_EVERYONE_HOMEPAGE);

			if(!empty($fltCategory) && Settings::get('catalog_use_categories_tree') === 'on'){
				// Filtering by category
				$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);
				if (!empty($fltCategory) && (int) $fltCategory > 0) {
					$allCoursesCriteria->compare('idCategory', $categories);
				}
			}
			if (!empty($flt_query)) {
				$allCoursesCriteria->addCondition('name LIKE :search OR description LIKE :search OR code LIKE :search');
				$allCoursesCriteria->params[":search"] = '%'.str_replace(' ', '%', $flt_query).'%';
			}

			$allCoursesCriteria->addInCondition('status', array(LearningCourse::$COURSE_STATUS_EFFECTIVE, LearningCourse::$COURSE_STATUS_AVAILABLE));

			if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
				$allCoursesCriteria->compare('selling', "1");
			} elseif ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
				$allCoursesCriteria->compare('selling', "0");
			}
			// Select only some columns to reduce overhead
			$allCoursesCriteria->select = 'selling, can_subscribe, name, description, status, idCourse, allow_overbooking, max_num_subscribe, img_course';
			//if the course is external
			if ($order)
				$allCoursesCriteria->order = "name ASC";


			// Filter by catalog(s)
			if ($fltCatalog) {
				if (!is_array($fltCatalog)) {
					$fltCatalog = array($fltCatalog);
				}
				if (!empty($fltCatalog)) {
					$allCoursesCriteria->with['learningCatalogueEntry'] = array(
						'alias'		=> 't100',
						'condition'	=> 't100.idCatalogue IN (' . implode(',', $fltCatalog) . ')',
						'joinType' 	=> 'INNER JOIN',
					);
				}
			}


			// Get courses
			$allCourses = LearningCourse::model()->findAll($allCoursesCriteria);


			if(empty($fltCategory)) {
				$allLpsCriteria = new CDbCriteria();
				$allLpsCriteria->addCondition("visible_in_catalog = 1");
				if (!empty($flt_query)) {
					$allLpsCriteria->addCondition('path_name LIKE :search OR path_descr LIKE :search OR path_code LIKE :search');
					$allLpsCriteria->params[":search"] = '%' . str_replace(' ', '%', $flt_query) . '%';
				}
				// We are displaying FREE and PAID Learning Plans
                if ($flt_type == CoursesGrid::$FILTER_TYPE_PAID) {
                    $allLpsCriteria->compare('is_selling', "1");
                } elseif ($flt_type == CoursesGrid::$FILTER_TYPE_FREE) {
                    $allLpsCriteria->compare('is_selling', "0");
                }
				$allLpsCriteria->select = 'id_path, path_name, path_descr, is_selling, img, price';
				if ($order) { $allLpsCriteria->order = "path_name ASC"; }


				// Filter by catalog(s)
				if ($fltCatalog) {
					if (!is_array($fltCatalog)) {
						$fltCatalog = array($fltCatalog);
					}
					if (!empty($fltCatalog)) {
						$allLpsCriteria->with['learningCatalogueEntry'] = array(
							'alias'		=> 't200',
							'condition'	=> 't200.idCatalogue IN (' . implode(',', $fltCatalog) . ')',
							'joinType' 	=> 'INNER JOIN',
						);
					}
				}

				// Get LPs
				$allLearningPlans = LearningCoursepath::model()->findAll($allLpsCriteria);

			} else {
				$allLearningPlans = array();
			}

			$allGridItems = array_merge($allCourses, $allLearningPlans);

			$entries = array_slice($allGridItems, $offset, $limit);
			return array(
				'total'        => count($allGridItems),
				'courses'      => $entries,
				'has_for_sale' => FALSE,
				'has_for_free' => FALSE,
			);
		}

		// Set Offset/Limit
		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		$orderBy         = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = rtrim($orderBy, ', ').', coursepath.path_code';

		$criteria->with = array(
			'course'=>array(
				'on'=>$courseOnCondition,
				'joinType'=>'LEFT JOIN'
			),
			'coursepath'=>array(
				'on'=>$coursePathOnCondition,
				'joinType'=>'LEFT JOIN'
			),
		);
		$criteria->addCondition('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL');
		$criteria->group = 'course.idCourse, coursepath.id_path';

		$entries = LearningCatalogueEntry::model()->findAll($criteria);

		$totalEntries = LearningCatalogueEntry::model()->count($criteria);

		return array(
			'total'        => $totalEntries,
			'courses'      => $entries,
			'has_for_sale' => FALSE,
			'has_for_free' => FALSE,
		);

		if (is_array($associatedCatalogs)) {
			if (empty($associatedCatalogs)) {
				//no assigned catalogs for the user
				if (Settings::get('on_catalogue_empty') === 'on') {
					$associatedCourses = FALSE;
				} else {
					return array(
						'total'        => 0,
						'courses'      => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			} else {
				//retrieve associated courses
				$associatedCourses = Yii::app()->getDb()->createCommand()
					->select('idEntry')
					->from(LearningCatalogueEntry::model()->tableName())
					->where('type_of_entry="course"')
					->andWhere(array('IN', 'idCatalogue', $associatedCatalogs))
					->queryColumn();

				//retrieve associated Learning Plans
				$associatedCoursepaths = Yii::app()->getDb()->createCommand()
					->select('idEntry')
					->from(LearningCatalogueEntry::model()->tableName())
					->where('type_of_entry="coursepath"')
					->andWhere(array('IN', 'idCatalogue', $associatedCatalogs))
					->queryColumn();

				if (empty($associatedCourses) && empty($associatedCoursepaths)) {
					return array(
						'total'        => 0,
						'courses'      => array(),
						'has_for_sale' => FALSE,
						'has_for_free' => FALSE,
					);
				}
			}
		}

		// filter on course category
		if (Settings::get('catalog_use_categories_tree') === 'on' && !empty($fltCategory)) {
			$categories = LearningCourseCategoryTree::model()->getCategoryAndDescendants($fltCategory);

			// add condition to criteria
			if (!empty($fltCategory) && (int) $fltCategory > 0) {
				$courseOnCondition .= ' AND course.idCategory IN ('.implode(',', $categories).') ';
			}
		}

		$subscribedCoursesList = array();
		if ($idUser) {
			$subscribedCoursesList = CoreUser::model()->getSubscribedCoursesList($idUser, TRUE);
		} else {
			// Display mode options - Everyone, and show on home page
			$courseOnCondition .= ' AND course.show_rules = :show_on_homepage ';
			$criteria->params[':show_on_homepage'] = LearningCourse::DISPLAY_EVERYONE_HOMEPAGE;
		}

		// Filter by Catalog id ? (multicatalog)
		if ($fltCatalog != '') {
			$criteria->compare('t.idCatalogue', $fltCatalog);
		}

		// Set Offset/Limit
		if ($offset > 0) {
			$criteria->offset = $offset;
		}
		if ($limit > 0) {
			$criteria->limit = $limit;
		}

		// We remove the courses having show_rules = Only users subscribed to the course
		// and the user is not subscribed.
		$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay($subscribedCoursesList);

		$coursesExcludedByPlugins = array();
		Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent(self, array(
			'excludedCourses' => &$coursesExcludedByPlugins
		)));
		if (!empty($coursesExcludedByPlugins)) {
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $coursesExcludedByPlugins);
		}

		//$criteria->addNotInCondition('course.idCourse', $coursesToNotDisplayList);

		$orderBy         = LearningCourse::getOrderBy('course', 'status,name');
		$criteria->order = rtrim($orderBy, ', ').', coursepath.path_code';

		$criteria->with = array(
			'course'=>array(
				'on'=>''
			),
			'coursepath'=>array(),
		);
		$criteria->addCondition('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL');
		$criteria->select = '*';

		// Get models ('page' or portion)
		$catalogEntries = LearningCatalogueEntry::model()->findAll($criteria);

		// mark the courses that the current user is subscribed to
		if (!empty($catalogEntries) && !empty($subscribedCoursesList)) {
			foreach ($catalogEntries as $key => $courseOrCoursepath) {
				/* @var $courseOrCoursepath LearningCatalogueEntry */
				if($courseOrCoursepath->type_of_entry == 'course'){
					if (in_array($courseOrCoursepath->course->idCourse, $subscribedCoursesList)) {
						$courseOrCoursepath->course->isCurrentUserSubscribedTo = TRUE;
					}
				}
			}
		}


		// Count all filtered courses first
		$catalogItemsCount = LearningCatalogueEntry::model()->count($criteria);

		// A bit more work: Check if there are courses for sale and for free AT ALL!!
		$hasForSale = LearningCourse::model()->exists('selling=1');
		$hasForFree = LearningCourse::model()->exists('selling=0');

		$result = array(
			'total'        => $catalogItemsCount,
			'courses'      => $catalogEntries,
			'has_for_sale' => $hasForSale,
			'has_for_free' => $hasForFree,
		);

		return $result;
	}


    public function afterSave() {
        if ( ($this->scenario == 'insert') || ($this->isNewRecord) ) {
            Yii::app()->event->raise('AddedCatalogue', new DEvent($this, array(
            	'model'	=> $this,
            )));
        } elseif ($this->scenario  == 'update') {
            Yii::app()->event->raise('EditedCatalogue', new DEvent($this, array(
                'model' => $this
            )));
        }
        parent::afterSave();
    }

    public function afterDelete() {
        Yii::app()->event->raise('DeletedCatalogue', new DEvent($this, array(
            'model' => $this
        )));
        parent::afterDelete();
    }


    /**
     * Check if External catalog filtration is enabled
     */
    public static function externalCatalogFiltrationEnabled() {
	    $settingCatalogExternal = Settings::get('catalog_external', 'off');
	    $settingSelectedCatalogs = Settings::get('catalog_external_selected_catalogs', '');
    	return (($settingCatalogExternal === 'on') && !empty($settingSelectedCatalogs));
    }


    /**
     * Return list of Externally/ (for anoonymous users) visible catalogs
     *
     * @param string $fltCatalog Additional filter
     */
    public static function getVisiblePublicCatalogs($fltCatalog = '') {
    	// NOTE: the next two settings are overwritten by MultiDomain plugin or another plugin
    	$settingCatalogExternal = Settings::get('catalog_external', 'off');
    	$settingSelectedCatalogs = Settings::get('catalog_external_selected_catalogs', '');

    	// use global catalog selection
    	if ($settingCatalogExternal === 'on') {
    		if (!empty($settingSelectedCatalogs)) {
    			if (!empty($fltCatalog))
    				$fltCatalog = array_intersect(explode(',', $settingSelectedCatalogs), explode(',', $fltCatalog));
    			else
    				$fltCatalog = explode(',', $settingSelectedCatalogs);
    		}
    	}

    	return $fltCatalog; // catalogs visible to current user in the external catalog
    }


    /**
     * Check if an Entry (by ID) is part of a catalog or catalogs
     *
     * @param integer $idEntry
     * @param array|integer $catalogs
     * @param string $entryType
     *
     * @return boolean
     */
    public static function isEntryInCatalogs($idEntry, $catalogs, $entryType) {

        if (!is_array($catalogs)) {
            $catalogs = array((int) $catalogs);
        }

        if (empty($catalogs)) {
            return false;
        }

        $exists = LearningCatalogueEntry::model()->exists("(type_of_entry=:entryType) AND (idEntry=:idEntry) AND (idCatalogue IN (" . implode(',', $catalogs). "))", array(
            ':entryType'    => $entryType,
            ':idEntry'      => $idEntry,
        ));

        return $exists;

    }


    /**
     * Return list of catalogs a given entry (or entries) of a particular type is assigned to,
     * e.g. a course can be assigned to N catalogs -> return list of those catalogs
     *
     * @param array|integer $idEntries
     * @param string $entryType
     * @return array
     */
    public static function getEntryCatalogs($idEntries, $entryType=false) {

        if (!is_array($idEntries)) {
            $idEntries = array((int) $idEntries);
        }

        $params = array();
        $command = Yii::app()->getDb()->createCommand();
        $command->select('idCatalogue');
        $command->from("learning_catalogue_entry lce");

        if ($entryType !== false && is_string($entryType)) {
            $params[":entryType"] = $entryType;
            $command->andWhere("type_of_entry=:entryType");
        }

        $command->andWhere(array("IN", "idEntry", $idEntries));

        return $command->queryColumn($params);
    }


    /**
     * Return list of catalogs a given course (or courses) is assigned to
     * @param integer|array $idCourses
     */
    public static function getCourseCatalogs($idCourses) {
        return self::getEntryCatalogs($idCourses, LearningCatalogueEntry::ENTRY_COURSE);
    }

    /**
     * Return list of catalogs a given learning plan (or plans) is assigned to
     *
     * @param integer|array $idCourses
     */
    public static function getPlanCatalogs($idPlans) {
        return self::getEntryCatalogs($idPlans, LearningCatalogueEntry::ENTRY_COURSEPATH);
    }

    /**
     * Return various user-groups-orgchart-catalogs information, depending on $params.
     * It is one big SQL, universal by design. You can extract a lot of information from the result.
     * Please dump the SQL and execute it in your SQL client to figure out more filtering options
     *
     * <strong>Please use with caution. If it is called without any filtering parameters it may return huge amount of data.</strong>
     *
     * @param array $params<br>
     *          <strong>"idUsers"</strong>           => array|integer user ID or user IDs to filter; FALSE means NO filtering, empty array will return empty result<br>
     *          <strong>"idCatalogs"</strong>        => array|integer catalog ID or catalog IDs to filter; FALSE means NO filtering, empty array will return empty result<br>
     *          <strong>"catalogsOnly"</strong>   	=> boolean Return list of catalogs only (true/false)<br>
     *          <strong>"skipUserless"</strong>		=> boolean Skip entries where NO effective user assigned to catalog(s) is available<br>
     *          <strong>"usersOnly"</strong>   		=> boolean Return list of users only (implies skipUserless)<br>
     *          <strong>"idOrgNodes"</strong>   	=> array|integer Org Chart Nodes to filter by - org nodes assignes as Node Only (no desc)
     *          <strong>"idOrgBranches"</strong>   	=> array|integer Org Chart Branches to filter by - org nodes assignes as Node & Desc
     *          <strong>"idGroups"</strong>   	    => array|integer Groups to filter by
     *
     * @param boolean $returnReader Instead of returning the actual result, return CDbDataReader
     *
     * @see CDbDataReader
     *
     * @return array
     */
    public static function getUsersCatalogsAssignmentsInfo($params=false, $returnReader=false) {

        $idUsers    	= isset($params['idUsers']) ? $params['idUsers'] : false;
        $idCatalogs 	= isset($params['idCatalogs']) ? $params['idCatalogs'] : false;
        $skipUserless 	= isset($params['skipUserless']) ? $params['skipUserless'] : false;
        $usersOnly 		= isset($params['usersOnly']) ? $params['usersOnly'] : false;
        $idOrgNodes 	= isset($params['idOrgNodes']) ? $params['idOrgNodes'] : false;
        $idOrgBranches 	= isset($params['idOrgBranches']) ? $params['idOrgBranches'] : false;
        $idGroups 	    = isset($params['idGroups']) ? $params['idGroups'] : false;


        // Should we return List of catalogs only (see below how select is changing)
        $catalogsOnly = isset($params['catalogsOnly']) ? $params['catalogsOnly'] : false;

        if ($idUsers !== false) {
            if (empty($idUsers)) {
                return array();
            }
            if(!is_array($idUsers)) {
                $idUsers = array((int)$idUsers);
            }
        }

        if ($idCatalogs !== false) {
            if (empty($idCatalogs)) {
                return array();
            }
            if(!is_array($idCatalogs)) {
                $idCatalogs = array((int)$idCatalogs);
            }
        }

        if ($idOrgNodes !== false) {
            if (empty($idOrgNodes)) {
                return array();
            }
            if(!is_array($idOrgNodes)) {
                $idOrgNodes = array((int)$idOrgNodes);
            }
        }

        if ($idOrgBranches !== false) {
            if (empty($idOrgBranches)) {
                return array();
            }
            if(!is_array($idOrgBranches)) {
                $idOrgBranches = array((int)$idOrgBranches);
            }
        }

        if ($idGroups !== false) {
            if (empty($idGroups)) {
                return array();
            }
            if(!is_array($idGroups)) {
                $idGroups = array((int)$idGroups);
            }
        }


        $queryParams = array();
        $command = Yii::app()->getDb()->createCommand();

        $command->from('learning_catalogue_member lcm');
        $command->leftJoin("core_group cg", "(cg.idst=lcm.idst_member) AND (cg.hidden='false')");
        $command->leftJoin("core_org_chart_tree tree1", "(tree1.idst_oc=lcm.idst_member)");
        $command->leftJoin("core_group cg_oc", "(tree1.idst_oc=cg_oc.idst)");
        $command->leftJoin("core_org_chart_tree tree21", "(tree21.idst_ocd=lcm.idst_member)");
        $command->leftJoin("core_org_chart_tree tree22", "((tree22.iLeft >= tree21.iLeft) AND (tree22.iRight <= tree21.iRight))");
        $command->leftJoin("core_group cg_ocd", "(tree22.idst_ocd=cg_ocd.idst)");
        $command->leftJoin("core_user u", "u.idst=lcm.idst_member");
        $command->leftJoin("core_group_members cgm_direct", "cg.idst=cgm_direct.idst");
        $command->leftJoin("core_group_members cgm_oc", "cg_oc.idst=cgm_oc.idst");
        $command->leftJoin("core_group_members cgm_ocd", "cg_ocd.idst=cgm_ocd.idst");

        // If we request users only or asked to ommit records without users (only groups/org charts), then exclude those records with null users
        if ($skipUserless || $usersOnly) {
        	$command->andWhere("(u.idst IS NOT NULL) OR (cgm_direct.idstMember IS NOT NULL) OR (cgm_oc.idstMember IS NOT NULL) OR (cgm_ocd.idstMember IS NOT NULL)");
        }

        // Filter by users?
        if ($idUsers !== false && !empty($idUsers)) {
            $list = implode(",", $idUsers);
            $command->andWhere("
	           u.idst IN (:userList) OR
	           cgm_direct.idstMember IN (:userList) OR
	           cgm_oc.idstMember IN (:userList) OR
	           cgm_ocd.idstMember IN (:userList)
            ");
            $queryParams = array(":userList"    => $list);
        }

        // Filter by catalogs?
        if ($idCatalogs !== false && !empty($idCatalogs)) {
            $command->andWhere(array("IN", "lcm.idCatalogue", $idCatalogs));
        }

        // Filter by Org Nodes (assigned as node only)?
        if ($idOrgNodes !== false && !empty($idOrgNodes)) {
            $command->andWhere(array("IN", "tree1.idOrg", $idOrgNodes));
        }

        // Filter by Org Branches (assigned Node & Desc)?
        if ($idOrgBranches !== false && !empty($idOrgBranches)) {
            $command->andWhere(array("IN", "tree21.idOrg", $idOrgBranches));
        }

        // Filter by Groups
        if ($idGroups !== false && !empty($idGroups)) {
            $command->andWhere(array("IN", "cg.idst", $idGroups));
        }

        // Define SELECT variants
        // Variant EXTENDED
        $selectArrayExtended = array(
            "(CASE
                WHEN u.idst IS NOT NULL THEN u.idst
                WHEN cgm_direct.idstMember IS NOT NULL THEN cgm_direct.idstMember
                WHEN cgm_oc.idstMember IS NOT NULL THEN cgm_oc.idstMember
                WHEN cgm_ocd.idstMember IS NOT NULL THEN cgm_ocd.idstMember
             END) as idFinalUser",
            "lcm.idCatalogue as idCatalogue",
            "u.idst AS idUser",
            "cgm_direct.idstMember AS idUserDirectGroup",
            "cg.idst AS idDirectGroup",
            "cgm_oc.idstMember AS idUserNode",
            "cgm_oc.idst AS idGroupNode",
            "tree1.idOrg as idOrgNode",
            "cgm_ocd.idstMember AS idUserDesc",
            "tree21.idOrg AS idOrgBranch",
            "cgm_ocd.idst AS idGroupDesc",
            "tree22.idOrg as idOrgDesc"
        );


        // Variant for CATALOGS only
        $selectArrayCatalogsListOnly = array(
            "lcm.idCatalogue as idCatalogue",
        );

        // Variant for USERS ONLY
        $selectArrayUsersListOnly = array(
            "(CASE
                WHEN u.idst IS NOT NULL THEN u.idst
                WHEN cgm_direct.idstMember IS NOT NULL THEN cgm_direct.idstMember
                WHEN cgm_oc.idstMember IS NOT NULL THEN cgm_oc.idstMember
                WHEN cgm_ocd.idstMember IS NOT NULL THEN cgm_ocd.idstMember
             END) as idFinalUser",
        );


        // Default select is extended one (full info)
        $selectArray = $selectArrayExtended;

        // Get list of catalogs only?
        if ($catalogsOnly) {
            $selectArray = $selectArrayCatalogsListOnly;
            $command->distinct = true;
        }
        // Or maybe users only
        else if ($usersOnly) {
        	$selectArray = $selectArrayUsersListOnly;
        	$command->distinct = true;
        }

        // SELECT
        $command->select($selectArray);

        // Dump SQL
        // var_dump($command->text);

        // Different options may change the way we retrieve data
        if ($catalogsOnly || $usersOnly) {
            $result = $command->queryColumn($queryParams);
        }
        else {
            if ($returnReader) {
                $result = $command->query($queryParams);
            }
            else {
                $result = $command->queryAll(true, $queryParams);
            }
        }

        return $result;

    }

    /**
     * Get list of catalogs user is assign to, through all possible ways: directly, through groups, org nodes or branches
     * @param array|integer $idUsers
     */
    public static function getUserCatalogs($idUsers) {
		$m = self::model();

		$params = array(
			'idUsers'	=> $idUsers,
			'catalogsOnly'	=> true,
		);
		$result = self::getUsersCatalogsAssignmentsInfo($params);
		return $result;
    }

    /**
     * Return list of users assigned to a catalog or catalogs, either directly or through groups, org nodes or org branches
     * @param array|integer $idCatalogs
     */
    public static function getCatalogUsers($idCatalogs) {
        $params = array(
            'idCatalogs'	=> $idCatalogs,
            'usersOnly'	    => true,
        );
        $result = self::getUsersCatalogsAssignmentsInfo($params);
        return $result;
    }



    /**
     * Return FULL users-catalog assignments. USE WITH CAUTION!!
     */
    public static function getFullAssignmentsInfo() {
    	return self::getUsersCatalogsAssignmentsInfo();
    }

}
