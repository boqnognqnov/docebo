<?php

/**
 * This is the model class for table "app7020_content_thumbs".
 *
 * The followings are the available columns in table 'app7020_content_thumbs':
 * @property integer $id
 * @property integer $idContent
 * @property string $file
 * @property integer $is_active
 * 
 * @property App7020Content $content

 */
class App7020ContentThumbs extends CActiveRecord {

	const ACTIVE_THUMBNAIL_SCENARIO = 'make_active';

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_content_thumbs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idContent, is_active', 'numerical', 'integerOnly' => true),
			array('file', 'length', 'max' => 255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idContent, file, is_active', 'safe', 'on' => 'search'),
			array('id', 'validateActiveThumbnail', 'on' => self::ACTIVE_THUMBNAIL_SCENARIO),
		);
	}

	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
		);
	}

	/**
	 * Custom validation on thumbnails 
	 * @param string $attr
	 * @param array $param
	 */
	public function validateActiveThumbnail($attr, $param) {
		if (empty($this->id)) {
			$this->addError($attr, Yii::t('app7020', 'The "thumbnail" is required!'));
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id Content',
			'file' => 'Filename',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('file', $this->file, true);
		$criteria->compare('is_active', $this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ContentThumbs the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Build name of thumbnail
	 * @return string
	 */
	public function getFilename() {
		$inputKey = $this->getInternalFolder();
		return $inputKey . '-' . $this->file . '.png';
	}

	/**
	 * Return folder with files for this content 
	 * @return string
	 */
	function getInternalFolder() {
		$inputKey = '';
		if ($this->content->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
			$inputKey = App7020Helpers::getAmazonInputKey($this->content->filename);
		}
		return $inputKey;
	}

	function getS3RelativePath() {
		try {
			$relativeSubFolder = CS3StorageSt::getConventionFolder($this->content->filename);
			return $relativeSubFolder . '/' . $this->getInternalFolder() . '/thumb/' . $this->getFilename();
		} catch (CException $ex) {
			return false;
		}
	}

//	public function afterSave() {
//
//		// If this thumbail is active (default one for the content), generate a CoreAsset and use its ID in content model using this thumb
//		if ($this->is_active == 1) {
//			if ($contentObject = App7020Assets::model()->findByPk($this->idContent)) {
//				if ($coreAsset = App7020VideoProceeder::generateThumbCoreAsset($contentObject, $this)) {
//					$contentObject->idThumbnail = $coreAsset->id;
//					if ($contentObject->conversion_status < App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
//						$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_SNS_SUCCESS;
//					}
//					$c_result = $contentObject->save(false);
//				}
//			}
//		}
//		parent::afterSave();
//	}

}
