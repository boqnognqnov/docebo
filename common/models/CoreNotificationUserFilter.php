<?php

/**
 * This is the model class for table "core_notification_user_filter".
 * 
 * Holds list of user_filter filtering information for a given notification
 *
 * The followings are the available columns in table 'core_notification_user_filter':
 * @property integer $id
 * @property integer $idItem  (Group or Branch ID)
 * @property integer $type	Type of the item (1: group, 2: branch, 3: user)
 * @property integer $selectionState  For branches only: 1: This branch only, 2: This branch and all children (@see CoreOrgChartTree const) 
 * 
 * @property CoreNotification $notification
 */
class CoreNotificationUserFilter extends CActiveRecord
{
	
	/**
	 * Possible values for item type 
	 * @var integer
	 */
	const NOTIF_FILTER_GROUP 	= 1;
	const NOTIF_FILTER_BRANCH 	= 2;
	const NOTIF_FILTER_USER 	= 3;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_notification_user_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idItem, type', 'required'),
			array('id, idItem, type, selectionState', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idItem, type, selectionState', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notification' 	=> array(self::BELONGS_TO, 'CoreNotification', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Notification ID',
			'idItem' => 'Item ID',
			'type' => 'Item type',
			'selectionState' => 'Selections state for branches'	
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idItem',$this->idItem);
		$criteria->compare('type',$this->type);
		$criteria->compare('selectionState',$this->selectionState);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreNotificationUserFilter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
