<?php
/**
 * Created by PhpStorm.
 * User: kyuchukovv
 * Date: 11-Aug-16
 * Time: 4:12 PM
 *
 * Reference: common/models/WebappGlobalSearch.php
 */

class WebappGlobalSearch {
	/**
	 * Define constants
	 * @minScore - for elastic search
	 * @maxItems - Maximum returned items for suggestion auto-complete
	 * @pageSize - Number of returned result elements
	 */
	const minScore = 0.5;  // Minimal score to target matches
	const maxItems = 10;    // Maximum Items per chunk get from ES
	const pageSize = 10;
	const maxSuggestionItems = 8;

	const VIEW_SUGGESTIONS = 'view_suggestions';
	const VIEW_RESULT = 'view_result';
	const LIMIT_TITLE = 40;

	/* Agent Types */
	const ALL    = 'all';
	const COURSE = 'EsAgentCourse';
	const ASSET  = 'EsAgentKnowledgeAsset';
	const LO     = 'EsAgentLo';
	const PLAN   = 'EsAgentPlan';

	/** Course Types */
	const ELEARNING = 'Elearning';
	const CLASSROOM = 'Classroom';
	const WEBINAR   = 'Webinar';

	/* Learning Object types*/
	const SCORM     = 'scormorg';
	const VIDEO     = 'video';
	const HTML      = 'html';
	const AICC      = 'aicc';
	const FILE      = 'file';

	/* Supported types for search result */
	static $supportedTypes = array(WebappGlobalSearch::ALL, WebappGlobalSearch::COURSE, WebappGlobalSearch::ASSET, WebappGlobalSearch::LO, WebappGlobalSearch::PLAN);

	public $viewType = self::VIEW_SUGGESTIONS;

	public static function getSuggestions($params){

		//Suggestiosn params
		return self::_elasticSearch('', $params);

	}

	public static function getResults($params){

	}

	private static function _elasticSearch($viewType, ElasticSearchParams $params){
		// LOGIC HERE
	}

}

/**
 * Class SuggestionsParams
 */
class SuggestionsParams implements ElasticSearchParams{
	/**
	 *
	 */
	public static function getParams()
	{
		// TODO: Implement getParams() method.
	}

	/**
	 * Get the title out of the result array
	 *
	 * @param $hit -> single array of result .
	 * @return bool
	 */
	public static function getHitTitle($hit){
		if ( is_array($hit)){
			switch($hit['_type']){
				case WebappGlobalSearch::COURSE :
					return $hit['_source']['name']; break;
				case WebappGlobalSearch::LO :
				case WebappGlobalSearch::ASSET :
					return $hit['_source']['title']; break;
				case WebappGlobalSearch::PLAN :
					return $hit['_source']['path_name']; break;
			}
		} else
			return false;
	}

	/**
	 * @param $type
	 */
	public static function isTypeSupported($type){
		$supportedTypes = WebappGlobalSearch::$supportedTypes;
		if ( in_array($type, $supportedTypes))
			return true;
		else
			return false;
	}

	/**
	 * @param $type
	 */
	public static function getTypeOptions($type){

	}

	/**
	 * @param $typeId
	 */
	public static function getImageUrl($id){

	}

	/**
	 * @param $id
	 */
	public static function getCourseType($id){

	}

	/**
	 * @param $type
	 */
	public static function getLoIcon($type){

	}

	/**
	 * @param $id
	 */
	public static function getLoOptions($id){
		if ( isset($id)){
			$response = array();
			$LearningObject = LearningOrganization::model()->findByPk($id);

			if ( $LearningObject){
				try {
					$idCourse = (int)$LearningObject->idCourse; // Get ID of the Course for the LO
					$LearningCourse = LearningCourse::model()->findByPk($idCourse); // Get the model
					if ($LearningCourse){
						$response['courseId']   = $idCourse;
					}

					return $response;
				} catch (Exception $e){
					return array('success'  => false, 'error'    => 'Could not get Model');
				}
			}
		}
		return array('success'  => false, 'error'    => 'Invalid Learning Object ID: '. $id);
	}

	/**
	 * @param $id
	 */
	public static function getAssetDuration($id){

	}

	/**
	 * @param $search
	 */
	public static function secondsToWords($search){

	}

	/**
	 * @param $id
	 */
	public function canEnter($id){
		$idUser = Yii::app()->user->idst;

		if ( !$id || !$idUser ) { return false; }

		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idCourse' => $id,
			'idUser' => $idUser
		));
		if ($enrollment)
			$canEnter = $enrollment->canEnterCourse();
		else
			$canEnter = array('can' => false);

		return $canEnter;
	}

	/**
	 * @param $id
	 */
	public function getLPImage($id){

	}

	/**
	 * @param $idLP
	 * @param $idUser
	 */
	public static function isEnrolledToLP($idLP, $idUser=false){
		if ( $idUser === false) $idUser = Yii::app()->user->idst;
		if ( $idUser && $idLP ){
			$LpUserModel = LearningCoursepathUser::model()->findByAttributes(array(
				'id_path'   => $idLP,
				'idUser'    => $idUser
			));
			if ( $LpUserModel )
				return true;
		}
		return false;
	}

	/**
	 * @param $word - The string which need to be searched
	 * @param $type
	 * @param $from
	 * @param $pageSize
	 * @param $minScore
	 * @return array response holds the suggestion words which user can see
	 *
	 * SUGGESTIONS
	 */
	public function getResult($word, $type, $from, $pageSize, $minScore){
		$response    = array();     // Final Response
		$tempHits    = array();     // Array with Suggestions
		/* Get Logged in user */
		$currentUser = Yii::app()->user->idst;

		$es          = Yii::app()->es;
		$suggestions = $es->search($word, $type, $from, $pageSize, $minScore);
		/* Go through all the hits from ES and fill the names in $tempHits[] */
		foreach ( $suggestions['hits'] as $hit ){
			$id         = $hit['_id'];
			$ResType    = $hit['_type'];

			if ( self::isTypeSupported($ResType) && isset($id)){
				switch ($ResType){
					case WebappGlobalSearch::COURSE :
						$canEnter = $this->canEnter($id);
						/* If you can enter the course then add it */
						if($canEnter['can']){
							$hitName = self::getHitTitle($hit);
							$tempHits[$hitName] = $hitName;
						}
						break;
					case WebappGlobalSearch::LO :
						$options  = self::getLoOptions($id);
						$canEnter = $this->canEnter($options['courseId']);
						/* If you can enter the course then add it */
						if($canEnter['can']){
							$hitName = self::getHitTitle($hit);
							$tempHits[$hitName] = $hitName;
						}
						break;
					case WebappGlobalSearch::ASSET :
						$hitName            = self::getHitTitle($hit);
						$tempHits[$hitName] = $hitName;
						break;
					case WebappGlobalSearch::PLAN :
						if($this->isEnrolledToLP($id)){
							$hitName            = self::getHitTitle($hit);
							$tempHits[$hitName] = $hitName;
						}
						break;
				}
			}
		}

		$response['search']         = $word;
		$response['recentSearches'] = CoreUser::getRecentSearch($currentUser);
		$response['suggestions']    = $tempHits;
		$response['success']        = true;

		return $response;
	}
}

/**
 * Class ResultParams
 */
class ResultParams implements ElasticSearchParams
{
	/**
	 * @return array data structure for result items
	 */
	public static function getParams()
	{
		// TODO: Implement getParams() method.
		$searchItem = array(
			'type'          => '',
			'title'         => '',
			'description'   => '',
			'id'            => '',
			'other_fields'  => array(
				'image_url'     => '',
				'fa_icon'       => '',
				'duration'      => '',
				'course_name'   => '',
				'course_type'   => '',
				'lo_type'       => ''
			)
		);

		return $searchItem;
	}

	/**
	 * @param $hit
	 */
	public static function getHitTitle($hit){

	}


	/**
	 * Return true if the type is supported by the models
	 * Possible values: "all","EsAgentCourse", "EsAgentKnowledgeAsset","EsAgentLo","EsAgentPlan"
	 *
	 * @param $type
	 * @return bool
	 */
	public static function isTypeSupported($type){
		$supportedTypes = WebappGlobalSearch::$supportedTypes;
		if ( in_array($type, $supportedTypes))
			return true;
		else
			return false;
	}

	/**
	 * @param $id
	 * @return mixed URL of the image( img_course )
	 */
	public static function getImageUrl($id){
		if ( isset($id)){
			$courseModel = LearningCourse::model()->findByPk($id);
			if ( $courseModel){
				$courseLogo = $courseModel->getCourseLogoUrl();
			}else{ // If could not get Model - return nologo.png
				$courseLogo = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
			}

		}else{ // If there is no ID - return nologo.png
			$courseLogo = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
		}

		return $courseLogo;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function getCourseType($id){
		if ( isset($id)){
			$courseType = Yii::app()->db->createCommand()
				->select('course_type')
				->from(LearningCourse::model()->tableName() . ' AS lc')
				->where('lc.idCourse=:idCourse', array(':idCourse'=>$id))
				->queryRow();

			return $courseType['course_type'];
		}
		return array('error' => 'Invalid course ID: '. $id);
	}

	/**
	 * @param $type -> of the learning object
	 * @return bool|string ->  Hold a class name of the icon
	 */
	public static function getLoIcon($type){
		if ( isset($type)){
			$icon = ''; // Hold Learning Object Icon
			switch ($type){
				case LearningOrganization::OBJECT_TYPE_SCORMORG :
				case LearningOrganization::OBJECT_TYPE_AICC :
				case LearningOrganization::OBJECT_TYPE_TINCAN :
					case LearningOrganization::OBJECT_TYPE_AUTHORING :
					$icon = 'fa-file-archive-o'; break;
				case LearningOrganization::OBJECT_TYPE_VIDEO :
					$icon = 'fa-film'; break;
				case LearningOrganization::OBJECT_TYPE_FILE :
					$icon = 'fa-download'; break;
				case LearningOrganization::OBJECT_TYPE_CENTRALREPO :
					$icon = 'fa-hdd-o'; break;
				case LearningOrganization::OBJECT_TYPE_DELIVERABLE :
					$icon = 'fa-files-o'; break;
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE :
					$icon = 'fa-file-text-o'; break;
				case LearningOrganization::OBJECT_TYPE_POLL :
					$icon = 'fa-list-ul'; break;
				case LearningOrganization::OBJECT_TYPE_TEST :
					$icon = 'fa-check-square-o'; break;
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT :
					$icon = 'fa-edge'; break;
				case LearningOrganization::OBJECT_TYPE_LECTORA :
					$icon = 'fa-book'; break;
				default: // Fallback for unknown types
					$icon = 'fa-question-circle'; break;
			}
			return $icon;
		}
		return false;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function getLoOptions($id){
		if ( isset($id)){
			$response = array();
			$LearningObject = LearningOrganization::model()->findByPk($id);
			$objectType     = $LearningObject->objectType;
			if ( $LearningObject){
				try {
					$idCourse = (int)$LearningObject->idCourse; // Get ID of the Course for the LO
					$LearningCourse = LearningCourse::model()->findByPk($idCourse); // Get the model
					$courseName = $LearningCourse->getCourseNameFormatted(); // Get the Course Name
					$imageUrl = CoreAsset::url($LearningObject->resource); // Get the Image
					$faIcon = self::getLoIcon($objectType); // If no image, get the icon for the LO type
					$response['courseId']   = $idCourse;
					$response['courseName'] = $courseName;
					$response['courseType'] = self::getCourseType($idCourse);
					$response['loType']     = $objectType;
					$response['image_url']  = $imageUrl;
					$response['fa_icon']    = $faIcon;

					return $response;
				} catch (Exception $e){
					return array('success'  => false, 'error'    => 'Could not get Model');
				}
			}
		}
		return array('success'  => false, 'error'    => 'Invalid Learning Object ID: '. $id);
	}

	/**
	 * Passing ID of Knowledge Asset and return the duration of the asset if it is a video
	 *
	 * @param $id
	 * @return array|string
	 */
	public static function getAssetDuration($id){
		if ( isset($id)){
			$app7020Content = Yii::app()->db->createCommand()
				->select('duration')
				->from(App7020Content::model()->tableName() . ' AS c')
				->where('c.id=:id', array(':id'=>$id))
				->andWhere('c.contentType = 1')
				->queryRow();
			$duration = self::secondsToWords($app7020Content['duration']);
//			$duration = $app7020Content['duration'];
			return $duration;
		}
		return array('error' => 'Invalid App7020 Content ID: '. $id);
	}

	/**
	 * Convert seconds to days, hours, minutes, seconds
	 * 
	 * @param $seconds
	 * @param array $words
	 * @param bool $d
	 * @param bool $h
	 * @param bool $m
	 * @param bool $s
	 * @return string
	 */
	public static function secondsToWords($seconds, $words = array('d' => 'd', 'h' => 'h', 'm' => 'm', 's' => 's'), $d = true, $h = true, $m = true, $s = true) {
		$ret = "";
		$days = intval(intval($seconds) / (3600 * 24));
		if ($d && $days) {
			$ret .= "$days$words[d]";
		}
		$hours = (intval($seconds) / 3600) % 24;
		if ($h && $hours) {
			$ret .= "$hours$words[h]";
		}
		$minutes = (intval($seconds) / 60) % 60;
		if ($m && $minutes) {
			$ret .= "$minutes$words[m]";
		}
		$seconds = intval($seconds) % 60;
		if ($s && $seconds) {
			$ret .= "$seconds$words[s]";
		}
		return $ret;
	}

	/**
	 * @param $id
	 */
	public function canEnter($id){
		$idUser = Yii::app()->user->idst;

		if ( !$id || !$idUser ) { 
			return array('can' => false);
		}

		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idCourse' => $id,
			'idUser' => $idUser
		));
		if ($enrollment)
			$canEnter = $enrollment->canEnterCourse();
		else
			$canEnter = array('can' => false);

		return $canEnter;
	}

	/**
	 * @param bool $LPid - Learning Plan ID
	 * @return string - If there is no image return empty string
	 */
	public function getLPImage($LPid=false){
		$imageUrl='';
		if ( $LPid !== false ){
			$LearningCoursepath = LearningCoursepath::model()->findByPk($LPid);
			if ( $LearningCoursepath){
				$imageId = $LearningCoursepath->img;  // Get the ID of the image
				$imageUrl = CoreAsset::url($imageId); // Get the real Image URL

				return $imageUrl;
			}
		}
		return $imageUrl;
	}

	/**
	 * @param $idLP - Id of the Learning Plan
	 * @param $idUser - Id of the user
	 *
	 * @return bool - Return true if the User is Enrolled to the Learning Plan
	 */
	public static function isEnrolledToLP($idLP, $idUser=false){
		if ( $idUser === false) $idUser = Yii::app()->user->idst;
		if ( $idUser && $idLP ){
			$LpUserModel = LearningCoursepathUser::model()->findByAttributes(array(
				'id_path'   => $idLP,
				'idUser'    => $idUser
			));
			if ( $LpUserModel )
				return true;
		}
		return false;
	}
	
	public static function getAssetImage($id){
		return App7020Assets::getAssetThumbnailUri($id);
	}

	/**
	 * @param $word
	 * @param $type
	 * @param $from
	 * @param $pageSize
	 * @param $minScore
	 * @return mixed
	 */
	public function getResult($word, $type, $from=0, $pageSize, $minScore=0.5)
	{
		$es = Yii::app()->es;
		$searchResults  = array(); // Preparing the final response in here

		$results = $es->search($word, $type, $from, $pageSize, $minScore); // Hold the search results from calling the ES
		if ( is_array($results)){
			foreach ($results['hits'] as $hit){
				$id         = $hit['_id'];
				$ResType    = $hit['_type'];
				if ( self::isTypeSupported($ResType) && isset($id)){
					switch ($ResType){
						case WebappGlobalSearch::COURSE :
							$canEnter = $this->canEnter($id);
							/* If you can enter the course then add it */
							if($canEnter['can']){
								$searchResults[]  = array(
									'type'          => $hit['_type'],
									'title'         => $hit['_source']['name'],
									'description'   => $hit['_source']['description'],
									'id'            => $hit['_id'],
									'other_fields'  => array(
										'image_url'     => self::getImageUrl($id),
										'course_type'   => self::getCourseType($id),
										'visibility' => $canEnter
									)
								);
							}
						break;
						case WebappGlobalSearch::LO :
							$options = self::getLoOptions($id);
							$canEnter = $this->canEnter($options['courseId']);
							/* If you can enter the course then add it */
							if($canEnter['can']){
								$searchResults[] = array(
									'type'          => $hit['_type'],
									'title'         => $hit['_source']['title'],
									'description'   => $hit['_source']['description'],
									'id'            => $hit['_id'],
									'other_fields'  => array(
										'lo_type'       => $options['loType'],
										'image_url'     => $options['image_url'],
										'fa_icon'       => $options['fa_icon'],
										'course_id'     => $options['courseId'],
										'course_name'   => $options['courseName'],
										'course_type'   => $options['courseType'],
										'visibility' => $canEnter
									)
								);
							}
						break;
						case WebappGlobalSearch::ASSET :
							$searchResults[] = array(
								'type'          => $hit['_type'],
								'title'         => $hit['_source']['title'],
								'description'   => $hit['_source']['description'],
								'id'            => $hit['_id'],
								'other_fields'  => array(
									'image_url'     => self::getAssetImage($id),
									'duration'      => self::getAssetDuration($id),
									'visibility' => array('can' => true)
								)
							);
						break;
						case WebappGlobalSearch::PLAN :
							if(self::isEnrolledToLP($hit['_id'])){
								$searchResults[]    = array(
									'type'          => $hit['_type'],
									'title'         => $hit['_source']['path_name'],
									'description'   => $hit['_source']['path_descr'],
									'id'            => $hit['_id'],
									'path_code'       => $hit['_source']['path_code'],
									'other_fields'  => array(
										'image_url'     => $this->getLPImage($hit['_id']),
										'visibility' => array('can' => $this->isEnrolledToLP($hit['_id']))
									)

								);
							}
						break;
					}
				}

			}
		}
		return $searchResults;
	}
}

interface ElasticSearchParams{
	public static function getParams();
	public static function isTypeSupported($type);
	public static function getHitTitle($hit);
	public static function getImageUrl($id);
	public static function getCourseType($id);
	public function getResult($word, $type, $from, $pageSize, $minScore);
	public static function getLoIcon($type);
	public static function secondsToWords($seconds);
	public static function getAssetDuration($id);
	public function canEnter($id);
	public function getLPImage($id);
	public static function isEnrolledToLP($idLP, $idUser);
}