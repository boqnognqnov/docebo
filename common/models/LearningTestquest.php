<?php

/**
 * This is the model class for table "learning_testquest".
 *
 * The followings are the available columns in table 'learning_testquest':
 * @property integer $idQuest
 * @property integer $idCategory
 * @property string $type_quest
 * @property string $title_quest
 * @property integer $difficult
 * @property integer $time_assigned
 * @property integer $sequence
 * @property integer $page
 * @property integer $shuffle
 * @property integer $is_bank
 * @property string $settings
 * @property integer $c
 */
class LearningTestquest extends CActiveRecord
{
	//NOTE: these properties here are due the refactoring with mid relational table (learning_test_quest_rel),
	// so they are TEMP properties, for easier usage
	public $page;
	public $sequence;
	public $idTest;
	public $settings;
	//END NOTE

	public $search_input;
	public $unvisible;
	public $visible;

	const QUESTION_TYPE_ASSOCIATE = 'associate';
	const QUESTION_TYPE_BREAK_PAGE = 'break_page';
	const QUESTION_TYPE_CHOICE = 'choice';
	const QUESTION_TYPE_CHOICE_MULTIPLE = 'choice_multiple';
	const QUESTION_TYPE_EXTENDED_TEXT = 'extended_text';
	const QUESTION_TYPE_FITB = 'fill_in_the_blank';
	//const QUESTION_TYPE_HOT_TEXT = 'hot_text';
	const QUESTION_TYPE_INLINE_CHOICE = 'inline_choice';
	//const QUESTION_TYPE_NUMERICAL = 'numerical'; //is this really needed/used ?
	const QUESTION_TYPE_TEXT_ENTRY = 'text_entry';
	const QUESTION_TYPE_TITLE = 'title';
	const QUESTION_TYPE_UPLOAD = 'upload';


	const DIFFICULTY_VERY_EASY = 1;
	const DIFFICULTY_EASY = 2;
	const DIFFICULTY_MEDIUM = 3;
	const DIFFICULTY_HARD = 4;
	const DIFFICULTY_VERY_HARD = 5;

	const QUEST_TITLE_SHORT_LENGTH = 50;

	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 2;

	public static function getQuestionTypesList() {
		return array(
			LearningTestquest::QUESTION_TYPE_ASSOCIATE,
			LearningTestquest::QUESTION_TYPE_BREAK_PAGE,
			LearningTestquest::QUESTION_TYPE_CHOICE,
			LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE,
			LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT,
			LearningTestquest::QUESTION_TYPE_FITB,
			//LearningTestquest::QUESTION_TYPE_HOT_TEXT,
			LearningTestquest::QUESTION_TYPE_INLINE_CHOICE,
			LearningTestquest::QUESTION_TYPE_TEXT_ENTRY,
			LearningTestquest::QUESTION_TYPE_TITLE,
			LearningTestquest::QUESTION_TYPE_UPLOAD,
		);
	}


	public static function getQuestionTypesTranslationsList($courseModel = false) {
		$isMobile = ($courseModel instanceof LearningCourse) ? $courseModel->isMobile() : false;
		$list = array();
		$list[LearningTestquest::QUESTION_TYPE_CHOICE] 				= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_CHOICE)));
		$list[LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE] 	= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE)));

		// Mobile courses allow only restricted type of questions
		if (!$isMobile) {
			$list[LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT] 	= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT)));
			$list[LearningTestquest::QUESTION_TYPE_FITB] 			= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_FITB)));
			$list[LearningTestquest::QUESTION_TYPE_INLINE_CHOICE]	= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_INLINE_CHOICE)));
			$list[LearningTestquest::QUESTION_TYPE_TEXT_ENTRY]		= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_TEXT_ENTRY)));
			$list[LearningTestquest::QUESTION_TYPE_ASSOCIATE] 		= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_ASSOCIATE)));
			$list[LearningTestquest::QUESTION_TYPE_UPLOAD] 			= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_UPLOAD)));
			$list[LearningTestquest::QUESTION_TYPE_TITLE] 			= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_TITLE)));
			$list[LearningTestquest::QUESTION_TYPE_BREAK_PAGE]		= Docebo::newLineToBr(Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_BREAK_PAGE)));
		}

		return $list;
	}





	public static function getQuestionTypesAcronymsList() {
		return array(
			LearningTestquest::QUESTION_TYPE_ASSOCIATE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_ASSOCIATE)),
			LearningTestquest::QUESTION_TYPE_BREAK_PAGE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_BREAK_PAGE)),
			LearningTestquest::QUESTION_TYPE_CHOICE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_CHOICE)),
			LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_CHOICE_MULTIPLE)),
			LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT)),
			LearningTestquest::QUESTION_TYPE_FITB => Yii::t('test', '_QUEST_'.strtoupper(LearningTestquest::QUESTION_TYPE_FITB)),
			//LearningTestquest::QUESTION_TYPE_HOT_TEXT => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_HOT_TEXT)),
			LearningTestquest::QUESTION_TYPE_INLINE_CHOICE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_INLINE_CHOICE)),
			LearningTestquest::QUESTION_TYPE_TEXT_ENTRY => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_TEXT_ENTRY)),
			LearningTestquest::QUESTION_TYPE_TITLE => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_TITLE)),
			LearningTestquest::QUESTION_TYPE_UPLOAD => Yii::t('test', '_QUEST_ACRN_'.strtoupper(LearningTestquest::QUESTION_TYPE_UPLOAD)),
		);
	}

	public function getTypeName()
	{
		$names = self::getQuestionTypesTranslationsList();
		if (isset($names[$this->type_quest]))
			return $names[$this->type_quest];
		else
			return null;
	}


	public static function isValidType($type) {
		return in_array($type, self::getQuestionTypesList());
	}


	public static function getDifficultyList() {
		return array(
			LearningTestquest::DIFFICULTY_VERY_EASY => Yii::t('standard', '_DIFFICULT_VERYEASY'),
			LearningTestquest::DIFFICULTY_EASY => Yii::t('standard', '_DIFFICULT_EASY'),
			LearningTestquest::DIFFICULTY_MEDIUM => Yii::t('standard', '_DIFFICULT_MEDIUM'),
			LearningTestquest::DIFFICULTY_HARD => Yii::t('standard', '_DIFFICULT_DIFFICULT'),
			LearningTestquest::DIFFICULTY_VERY_HARD => Yii::t('standard', '_DIFFICULT_VERYDIFFICULT'),
		);
	}


	public static function isValidDifficulty($value) {
		return array_key_exists($value, self::getDifficultyList());
	}


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTestquest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testquest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title_quest', 'required'),
			array('idCategory, difficult, time_assigned, sequence, page, shuffle, is_bank', 'numerical', 'integerOnly'=>true),
			array('type_quest', 'length', 'max'=>255),
			array('search_input, visible, unvisible, settings', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idQuest, idCategory, type_quest, title_quest, difficult, time_assigned, sequence, page, shuffle, is_bank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'correctanswer' => array(self::HAS_ONE, 'LearningTestquestanswer', '', 'on' => 'correctanswer.idQuest = t.idQuest AND correctanswer.score_correct = '.LearningTestquestanswer::SCORE_CORRECT),
			'answers' => array(self::HAS_MANY, 'LearningTestquestanswer', 'idQuest'),
			'answerAssociates' => array(self::HAS_MANY, 'LearningTestquestanswerAssociate', 'idQuest'),
			'trackAnswers' => array(self::HAS_MANY, 'LearningTesttrackAnswer', 'idQuest'),
			'category' => array(self::BELONGS_TO, 'LearningQuestCategory', 'idCategory'),
			'testAssociation' => array(self::HAS_MANY, 'LearningTestQuestRel', array('id_question' => 'idQuest'))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idQuest' => 'Id Quest',
			'idCategory' => 'Id Category',
			'type_quest' => 'Type Quest',
			'title_quest' => 'Title Quest',
			'difficult' => 'Difficult',
			'time_assigned' => 'Time Assigned',
			'sequence' => 'Sequence',
			'page' => 'Page',
			'is_bank' => 'Question bank',
			'shuffle' => 'Shuffle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if ($this->search_input)
		{
			$criteria->addCondition('t.title_quest LIKE :search_input');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}

		if ($this->idCategory) //otherwise it will compare to 0
			$criteria->compare('idCategory',$this->idCategory);

		if ($this->visible && $this->unvisible) {
			$criteria->addInCondition('t.visible', array(0, 1));
		} else {
			if ($this->visible) {
				$criteria->addCondition('t.visible = 0');
			} elseif ($this->unvisible) {
				$criteria->addCondition('t.visible = 1');
			}
		}

		$criteria->compare('is_bank', 1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function init () {
		parent::init();

		if(!$this->settings){
			$this->settings = array();
		}
	}

	protected function afterFind () {
		parent::afterFind();
		$this->settings = (array) CJSON::decode($this->settings, 1);
	}

	/**
	 * Add/edit the relational model with the TMP properties declared at the top (sequence, idTest, page)
	 * as we now have many-many relation with some settings in the relational table
	 *
	 * Simply the properties are already used and it's easier to handle it from here instead of refactoring everything else
	 */
	public function afterSave()
	{
		if ($this->isNewRecord)
		{
			if ($this->idTest) //create relation between question and test
			{
				$model = new LearningTestQuestRel();
				$model->id_test = $this->idTest;
				$model->id_question = $this->idQuest;
				$model->page = $this->page;
				$model->sequence = $this->sequence;
				$model->save(false);
			}
		}
		else
		{
			if ($this->idTest) //update relation based data between question and test
			{
				$model = LearningTestQuestRel::model()->findByAttributes(array(
					'id_test' => $this->idTest,
					'id_question' => $this->idQuest,
				));
				if ($model) {
					if ($this->page)
						$model->page = $this->page;
					if ($this->sequence)
						$model->sequence = $this->sequence;

					if ($this->page || $this->sequence)
						$model->save(false);
				}
			}
		}

		parent::afterSave();
	}

	protected function beforeSave () {
		$this->settings = CJSON::encode($this->settings);
		return parent::beforeSave();
	}

	public function onBeforeSave($event) {
		if (!self::isValidType($this->type_quest)) { throw new CException('Invalid question type: '.$this->type_quest); }
		$this->sequence = (self::getTestMaxSequence($this->idTest) + 1);
		parent::onBeforeSave($event);
	}

/*
	//no more needed, this has been reached through DB foreign keys
	public function onBeforeDelete($event) {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		try {
			$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $this->getPrimaryKey()));
			if (!empty($answers)) {
				foreach ($answers as $answer) {
					$rs = $answer->delete();
					if ($rs === FALSE) {
						throw new CException('Error while deleting test question answers');
					}
				}
			}
			if ($transaction) { $transaction->commit(); }
		} catch (CException $e) {
			if ($transaction) { $transaction->rollback(); }
			throw ($e);
		}
		parent::onBeforeDelete($event);
	}
*/


	/**
	 * Normalize sequence numbers for all questions of a specified test
	 * @param int $idTest
	 */
	public static function fixTestSequence($idTest) {
		$criteria = new CDbCriteria();
		$criteria->order = 't.sequence ASC, question.title_quest ASC';
		$criteria->with = array('question');
		$questions = LearningTestQuestRel::model()->findAllByAttributes(array('id_test' => $idTest), $criteria);
		if (!empty($questions)) {
			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
			try {
				$sequence = 1;
				foreach ($questions as $question) {
					if ($question->sequence != $sequence) {
						$question->sequence = $sequence;
						$rs = $question->save();
						if (!$rs) {
							throw new CException('Error while fixing sequence');
						}
					}
					$sequence++;
				}
				if (isset($transaction)) { $transaction->commit(); }
			} catch (CException $e) {
				if (!isset($transaction)) { $transaction->rollback(); }
				return false;
			}
		}
		return true;
	}


	/**
	 * Normalize page numbers for all questions of a specified test
	 * @param int $idTest
	 */
	public static function fixTestPages($idTest) {
		$criteria = new CDbCriteria();
		$criteria->order = 't.sequence ASC, question.title_quest ASC';
		$criteria->with = array('question');
		$questions = LearningTestQuestRel::model()->findAllByAttributes(array('id_test' => $idTest), $criteria);
		if (!empty($questions)) {
			if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
			try {
				$page = 1;
				foreach ($questions as $question) {
					if ($question->page != $page) {
						$question->page = $page;
						if (!$question->save()) {
							throw new CException('Error while fixing questions page');
						}
					}
					if ($question->question->type_quest == self::QUESTION_TYPE_BREAK_PAGE) {
						$page++;
					}
				}
				if (isset($transaction)) { $transaction->commit(); }
			} catch (CException $e) {
				if (!isset($transaction)) { $transaction->rollback(); }
				return false;
			}
		}
		return true;
	}


	/**
	 * Find the highest sequence number in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getTestMaxSequence($idTest) {
		$result = Yii::app()->db->createCommand()
			->select("MAX(sequence) AS max_sequence")
			->from(LearningTestQuestRel::model()->tableName())
			->where("id_test = :id_test", array(':id_test' => $idTest))
			->queryRow();
		$output = (int)$result['max_sequence'];
		return $output;
	}



	/**
	 * Find the sequence number for a new question in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getTestNextSequence($idTest) {
		return self::getTestMaxSequence($idTest) + 1;
	}



	/**
	 * Find the page number for a new question in a specified test
	 * @param type $idTest
	 * @return integer
	 */
	public static function getTestPageNumber($idTest) {
		$db = Yii::app()->db;
		$table = LearningTestQuestRel::model()->tableName();

		$result = $db->createCommand()
			->select("MAX(sequence) AS max_sequence, MAX(page) AS page")
			->from($table)
			->where("id_test = :id_test", array(':id_test' => $idTest))
			->queryRow();
		$sequence = $result['max_sequence'];
		$page = $result['page'];
		if (!$page) { return 1; } //no questions yet, return first page number

		$table = LearningTestquest::model()->tableName();
		$result = $db->createCommand()
			->select("type_quest")
			->from($table)
			->join(LearningTestQuestRel::model()->tableName().' rel', 'rel.id_question = '.$table.'.idQuest')
			->where("rel.id_test = :id_test", array(':id_test' => $idTest))
			->andWhere("rel.sequence = :sequence", array(':sequence' => $sequence))
			->queryRow();
		$questionType = $result['type_quest'];
		if ($questionType == self::QUESTION_TYPE_BREAK_PAGE) {
			return ($page + 1);
		}

		return $page;
	}



	/**
	 * Find the question max possible score obtainable by users
	 * @return double
	 */
	public function getScore() {
		$output = false;
		$pk = $this->getPrimaryKey();
		if ($pk) {
			switch ($this->type_quest) {
				case LearningTestquest::QUESTION_TYPE_BREAK_PAGE:
				case LearningTestquest::QUESTION_TYPE_TITLE:
				case LearningTestquest::QUESTION_TYPE_UPLOAD: break; //no possible score for these question types
				default: {
					$output = 0.0;
					$answers = LearningTestquestanswer::model()->findAllByAttributes(array('idQuest' => $pk));
					if (!empty($answers)) {
						foreach ($answers as $answer) {
							$output += $answer->score_correct;
						}
					}
				} break;
			}
		}
		return $output;
	}

	/**
	 * Remove html characters from the title + new lines and truncate it if longer than QUEST_TITLE_SHORT_LENGTH
	 * @return string
	 */
	public function getTitleQuestTruncated()
	{
		$title = str_replace("\r\n", " ", $this->title_quest);
		$title = trim($title);
		$title = strip_tags($title);
		$title = CHtml::decode($title); //decode any special symbols such as &amp; &lt; &gt; etc AFTER the tags are removed

		return Docebo::ellipsis($title, self::QUEST_TITLE_SHORT_LENGTH, '...');
	}

	public static function getTitleQuestTruncated2($string)
	{
		$title = str_replace("\r\n", " ", $string);
		$title = trim($title);
		$title = strip_tags($title);
		if (function_exists(mb_strlen))
			$len = mb_strlen($title);
		else
			$len = strlen($title);

		if ($len > self::QUEST_TITLE_SHORT_LENGTH)
		{
			if (function_exists(mb_substr))
				$title = mb_substr($title, 0, self::QUEST_TITLE_SHORT_LENGTH);
			else
				$title = substr($title, 0, self::QUEST_TITLE_SHORT_LENGTH);

			$title .= '...';
		}
		return $title;
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {

		// Cleanup uploaded files, for questions of type "upload"
		if ($this->type_quest == self::QUESTION_TYPE_UPLOAD) {
			$trackAnswers = $this->trackAnswers;
			foreach ($trackAnswers as $answer) {
				// If there is some text in "more_info" field, that must be a file; delete it
				if ($answer->more_info) {
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TEST);
					$storage->remove($answer->more_info);
				}
			}
		}

		return parent::beforeDelete();

	}

	public function categoryName()
	{
		return $this->category ? $this->category->name : '';
	}

	/**
	 * Get available BANK questions. Filter already assigned to the given test (if any)
	 * @param null $id_test
	 * @return CActiveDataProvider
	 */
	public function getAvailableBankQuestions($id_test = null)
	{
		$criteria=new CDbCriteria;
		$criteria->condition = 'is_bank = 1 AND visible = :status_active';
		$criteria->params = array(':status_active' => LearningTestquest::STATUS_ACTIVE);

		if ($this->search_input)
		{
			$criteria->addCondition('t.title_quest LIKE :search_input');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}

		if ($this->idCategory){ //otherwise it will compare to 0
			$criteria->compare('idCategory',$this->idCategory);
		}

		//exclude already assigned questions to the given test (if any)
		if ($id_test !== null)
		{
			$criteria->join = 'LEFT JOIN '.LearningTestQuestRel::model()->tableName().' qr ON qr.id_question = t.idQuest AND qr.id_test = :id_test';
			$criteria->addCondition('qr.id_test IS NULL');
			$criteria->params[':id_test'] = $id_test;
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	public function getTestCount()
	{
		return LearningTestQuestRel::getTestCountByQuestion($this->idQuest);
	}

	public function renderActionsColumn()
	{
		$res = '<a href="'.Docebo::createLmsUrl('test/question/update', array('id_question' => $this->idQuest)).'" class="updateBankQuestion"><i class="fa fa-edit" style="font-size: 18px"></i></a>&nbsp;';

		if ($this->visible)
			$res .= '<a href="'.Docebo::createAdminUrl('questionBank/axToggleVisibility', array('idQuest' => $this->idQuest)).'" onclick="toggleVisibility(this); return false;"><i class="fa fa-eye" style="font-size: 18px""></i></a>';
		else
			$res .= '<a href="'.Docebo::createAdminUrl('questionBank/axToggleVisibility', array('idQuest' => $this->idQuest)).'" onclick="toggleVisibility(this); return false;"><i class="fa fa-eye-slash" style="font-size: 18px"></i></a>';

		if ($this->getTestCount() == 0)
		{
			$res .= '<a href="'.Docebo::createAdminUrl('questionBank/axDelete', array('idQuest' => $this->idQuest)).'" class="open-dialog" rel="delete-question"><i class="fa fa-remove" style="font-size: 22px; color: #d70b0a;"></i></a>';
		}

		return $res;
	}

	public function formatImportQuestionTitle()
	{
		$res = '<span id="toggleTitle_'.$this->idQuest.'">'.$this->getTitleQuestTruncated().'</span>';
		$res .= '<div id="toggleQuest_'.$this->idQuest.'" style="display: none;">';
		$res .= Yii::app()->controller->widget('root.common.widgets.TestQuestionPreview', array(
			'questionModel' => $this
		), true);
		$res .= '</div>';
		return $res;
	}

}