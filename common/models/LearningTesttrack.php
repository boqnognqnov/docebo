<?php

/**
 * This is the model class for table "learning_testtrack".
 *
 * The followings are the available columns in table 'learning_testtrack':
 * @property integer $idTrack
 * @property integer $idUser
 * @property integer $idReference
 * @property integer $idTest
 * @property string $date_attempt
 * @property string $date_attempt_mod
 * @property string $date_end_attempt
 * @property integer $last_page_seen
 * @property integer $last_page_saved
 * @property integer $number_of_save
 * @property integer $number_of_attempt
 * @property double $score
 * @property double $bonus_score
 * @property string $score_status
 * @property string $comment
 * @property string $attempts_for_suspension
 * @property string $suspended_until
 *
 * @property LearningTest $test
 *
 * @method setObjectType($type)
 * @method setStatus($status)
 */
class LearningTesttrack extends CActiveRecord
{

	const STATUS_NOT_COMPLETE = 'not_complete';
	const STATUS_DOING = 'doing';
	const STATUS_VALID = 'valid';
	const STATUS_PASSED = 'passed';
	const STATUS_NOT_PASSED = 'not_passed';
	const STATUS_FAILED = 'failed';
	const STATUS_COMPLETED = 'completed';
	const STATUS_CHECKED = 'checked';
	const STATUS_NOT_CHECKED = 'not_checked';

	// Populated with old attribute values for the model in afterFind()
	private $oldAttributes = array();


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('comment', 'required'),
			array('idUser, idReference, idTest, last_page_seen, last_page_saved, number_of_save, number_of_attempt', 'numerical', 'integerOnly'=>true),
			array('score, bonus_score', 'numerical'),
			array('score_status', 'length', 'max'=>12),
			array('attempts_for_suspension', 'length', 'max'=>10),
			array('date_attempt, date_attempt_mod, date_end_attempt, suspended_until', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idUser, idReference, idTest, date_attempt, date_attempt_mod, date_end_attempt, last_page_seen, last_page_saved, number_of_save, number_of_attempt, score, bonus_score, score_status, comment, attempts_for_suspension, suspended_until', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'times' => array(self::HAS_MANY, 'LearningTesttrackTimes', 'idTrack'),

			'test' => array(self::HAS_ONE, 'LearningTest', array('idTest'=>'idTest')),

			//should be called with "with()" directly with the current model load, otherwise the filter will not work.
			'quests' => array(self::HAS_MANY, 'LearningTestquest', '',
							'join' => 'INNER JOIN learning_test_quest_rel qr on qr.id_test = t.idTest AND quests.idQuest = qr.id_question',
							'on' => '
        					quests.type_quest != :type_break_page
        					AND quests.type_quest != :type_title',
							'params' => array(':type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE, ':type_title' => LearningTestquest::QUESTION_TYPE_TITLE),
							'order'=>'qr.sequence ASC'
			),
			'questsWithTitles' => array(self::HAS_MANY, 'LearningTestquest', '',
							'join' => 'INNER JOIN learning_test_quest_rel qr on qr.id_test = t.idTest AND questsWithTitles.idQuest = qr.id_question',
							'on' => 'questsWithTitles.type_quest != :type_break_page',
							'params' => array(':type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE),
							'order'=>'qr.sequence ASC'
						),
			'answers' => array(self::HAS_MANY, 'LearningTesttrackAnswer', array('idTrack'=>'idTrack')),
		);
	}

	protected function afterFind() {
		parent::afterFind();

		$this->oldAttributes = $this->attributes;
	}

	protected function afterSave() {

		// Those tracking statuses cause the score to be copied to
		// learning_courseuser.score_given, if the tracked object is the
		// "Final object" inside the course
		if(in_array($this->score_status, array(self::STATUS_CHECKED, self::STATUS_COMPLETED, self::STATUS_PASSED, self::STATUS_VALID))){
			if(empty($this->oldAttributes) || ($this->score != $this->oldAttributes['score'] || $this->bonus_score != $this->oldAttributes['bonus_score'])){
				// The score of the user inside the test was set or changed (e.g. manually set by admin or user has retaken the test)
				$this->_updateGlobalCourseScoreIfNeeded();
			}
		}

		parent::afterSave();
	}


	public function byUser($userId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'learningTesttracks.idUser=:scopeUserId',
			'params'=>array(':scopeUserId'=>$userId),
		));
		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idUser' => 'Id User',
			'idReference' => 'Id Reference',
			'idTest' => 'Id Test',
			'date_attempt' => Yii::t('standard', 'Date Attempt'),
			'date_attempt_mod' => 'Date Attempt Mod',
			'date_end_attempt' => 'Date End Attempt',
			'last_page_seen' => 'Last Page Seen',
			'last_page_saved' => 'Last Page Saved',
			'number_of_save' => 'Number Of Save',
			'number_of_attempt' => 'Number Of Attempt',
			'score' => 'Score',
			'bonus_score' => 'Bonus Score',
			'score_status' => 'Score Status',
			'comment' => 'Comment',
			'attempts_for_suspension' => 'Attempts For Suspension',
			'suspended_until' => 'Suspended Until',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idTest',$this->idTest);
		$criteria->compare('date_attempt',Yii::app()->localtime->fromLocalDateTime($this->date_attempt),true);
		$criteria->compare('date_attempt_mod',Yii::app()->localtime->fromLocalDateTime($this->date_attempt_mod),true);
		$criteria->compare('date_end_attempt',Yii::app()->localtime->fromLocalDateTime($this->date_end_attempt),true);
		$criteria->compare('last_page_seen',$this->last_page_seen);
		$criteria->compare('last_page_saved',$this->last_page_saved);
		$criteria->compare('number_of_save',$this->number_of_save);
		$criteria->compare('number_of_attempt',$this->number_of_attempt);
		$criteria->compare('score',$this->score);
		$criteria->compare('bonus_score',$this->bonus_score);
		$criteria->compare('score_status',$this->score_status,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('attempts_for_suspension',$this->attempts_for_suspension,true);
		$criteria->compare('suspended_until',Yii::app()->localtime->fromLocalDateTime($this->suspended_until),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'commonTrack' => array(
				'class' => 'CommontrackBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idResourcePropertyName' => 'idTest',
				'idUserPropertyName' => 'idUser',
				'idOrgPropertyName' => 'idReference',
				'statusPropertyName' => 'score_status',
				'statusConversion' => array( //note: some of this statuses may be no more used, kept for backward compatibility
					self::STATUS_NOT_COMPLETE => LearningCommontrack::STATUS_ATTEMPTED,
					self::STATUS_DOING => LearningCommontrack::STATUS_ATTEMPTED,
					self::STATUS_VALID => LearningCommontrack::STATUS_COMPLETED,
					self::STATUS_PASSED => LearningCommontrack::STATUS_COMPLETED,
					self::STATUS_NOT_PASSED => LearningCommontrack::STATUS_FAILED,
					self::STATUS_FAILED => LearningCommontrack::STATUS_FAILED,
					//self::STATUS_COMPLETED => LearningCommontrack::STATUS_COMPLETED,
					//self::STATUS_CHECKED => LearningCommontrack::STATUS_ATTEMPTED,
					self::STATUS_NOT_CHECKED => LearningCommontrack::STATUS_ATTEMPTED
				)
			),
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			    'timestampAttributes' => array('date_attempt medium', 'date_attempt_mod medium', 'date_end_attempt medium', 'suspended_until medium'),
			    'dateAttributes' => array()
            )
		);
	}


	/**
	 * Get current score for the current test
	 * @return int
	 */
	public function getCurrentScore()
	{
//		$criteria = new CDbCriteria();
//		$criteria->select = array('SUM(score) as score');
//		$criteria->condition = 'idTrack = :idTrack AND score_status = :score_valid';
//		$criteria->params = array(':idTrack' => $this->idTrack, ':score_valid' => LearningTesttrackTimes::SCORE_STATUS_VALID);
//		$model = LearningTesttrackTimes::model()->find($criteria);
//		return $model ? $model->score : 0;
		return ($this->score + $this->bonus_score);
	}

	/**
	 * Get total score for the current test
	 * @return int
	 */
	public function getTotalScore()
	{
		$res = Yii::app()->db->createCommand()
			->select('SUM(score_correct)')
			->from(LearningTestquest::model()->tableName()." t")
			->join(LearningTestQuestRel::model()->tableName()." qr", "qr.id_question = t.idQuest")
			->join(LearningTestquestanswer::model()->tableName()." a", "a.idQuest = t.idQuest")
			->where('qr.id_test=:id_test AND a.is_correct >= :correct', array(':id_test'=>$this->idTest, ':correct' => LearningTestquestanswer::SCORE_CORRECT))
			->queryScalar();
		return $res;
	}

	/**
	 * Generate scores object - current/total
	 * @return stdClass
	 */
	public function getScores()
	{
		$scores = new stdClass();
		$scores->current = $this->getCurrentScore();
		$scores->total = $this->getTotalScore();
		return $scores;
	}

	/**
	 * @return string
	 */
	public function getScoresPair()
	{
		$scores = $this->getScores();
		$idTest = $this->idTest;
		$point_type = LearningTest::model()->findByPk($idTest)->point_type;
		if ($scores->total > 0) {
			if ($point_type == "1") {
				return $scores->current . "%";
			} else {
				return $scores->current . "/" . $scores->total;
			}
		} else {
			return $scores->current;
		}
	}

	public function userTimeInTheTest()
	{
		$timeAccumulated = 0;

		$pageList = Yii::app()->db->createCommand()
			->select('display_to, display_from, accumulated, idTrack')
			->from(LearningTesttrackPage::model()->tableName())
			->where('idTrack=:id', array(':id'=>$this->getPrimaryKey()))
			->queryAll(true);

			foreach ($pageList as $page)
			{
				if (!empty($page['display_to']))
				{
					$timeFrom = strtotime($page['display_from']);
					$timeTo = strtotime($page['display_to']);
					$timeAccumulated += abs($timeTo - $timeFrom);
				}
				$timeAccumulated += (int)$page['accumulated'];
			}

		return $timeAccumulated;
	}



	public function userTimeInThePage($page) {

		$timeAccumulated = 0;

		$tp = LearningTesttrackPage::model()->findByAttributes(array('idTrack' => $this->getPrimaryKey(), 'page' => $page));
		if (!$tp) { return $timeAccumulated; }

		$timeFrom = strtotime(Yii::app()->localtime->fromLocalDateTime($tp->display_from));
		$timeTo = strtotime(Yii::app()->localtime->fromLocalDateTime($tp->display_to));

		if (!is_null($tp->display_to)) { $timeAccumulated += abs($timeTo - $timeFrom); }
		$timeAccumulated += $tp->accumulated;

		return $timeAccumulated;
	}



	public function storePage($pageToSave, $canOverwrite) {

		$test = LearningTest::model()->findByPk($this->idTest);
		$questions = $test->getQuestionsForPage($pageToSave);
		if (!empty($questions)) {
			foreach ($questions as $question) {

				if ($questionObject = CQuestionComponent::getQuestionManager($question->type_quest)) {
					$storing = $questionObject->storeUserAnswer($this->getPrimaryKey(), $question->getPrimaryKey(), false, $canOverwrite);
				}
			}
		}
	}



	public function closeTrackPageSession($page) {

		$pageInfo = LearningTesttrackPage::model()->findByPk(array('idTrack' => $this->getPrimaryKey(), 'page' => $page));
		if (!empty($pageInfo)) {

			if (empty($pageInfo->display_to)) {
				$pageInfo->display_to = Yii::app()->localtime->toLocalDateTime();
				if (!$pageInfo->save()) {
					//throw ...
				}
			}
			else
			{
				$this->updateTrackForPage($page);
				$pageInfo->display_to = Yii::app()->localtime->toLocalDateTime();
				if (!$pageInfo->save()) {
					//throw ...
				}
			}
		}
	}



	public function updateTrackForPage($page) {

		//TODO: may it be better to modify DB in order to avoid the use of idTrack?

		$time = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
		$pageInfo = LearningTesttrackPage::model()->findByPk(array('idTrack' => $this->getPrimaryKey(), 'page' => $page));

		if (empty($pageInfo)) {

			$trackPage = new LearningTesttrackPage();
			$trackPage->idTrack = $this->getPrimaryKey();
			$trackPage->page = $page;
			$trackPage->display_from = Yii::app()->localtime->toLocalDateTime();
			$trackPage->display_to = NULL;
			$trackPage->accumulated = 0;
			if (!$trackPage->save()) {
				return false;
			}

		} else {

			//$timeFrom = strtotime(Yii::app()->localtime->fromLocalDateTime($pageInfo->display_from));
			//NOTE: strtotime() doesn't handle well datetimes in ISO 8601 format (e.g. like 2014-11-04T10:26:48+0000)
			$utcDisplayFrom = Yii::app()->localtime->fromLocalDateTime($pageInfo->display_from);
			$tmp = new DateTime($utcDisplayFrom);
			$timeFrom = strtotime($tmp->format('Y-m-d H:i:s'));

			if ($pageInfo->display_to == NULL) {
				if($pageInfo->accumulated == 0)
					$pageInfo->accumulated = $time - $timeFrom;
			} else {
				//$timeTo = strtotime(Yii::app()->localtime->fromLocalDateTime($pageInfo->display_to));
				//NOTE: see above
				$utcDisplayTo = Yii::app()->localtime->fromLocalDateTime($pageInfo->display_to);
				$tmp = new DateTime($utcDisplayTo);
				$timeTo = strtotime($tmp->format('Y-m-d H:i:s'));
				$pageInfo->accumulated = $pageInfo->accumulated + abs($timeTo - $timeFrom);
				$pageInfo->display_from = Yii::app()->localtime->toLocalDateTime();
				$pageInfo->display_to = NULL;
			}

			if (!$pageInfo->save()) { //throw new CException($pageInfo->getErrors());
				return false;
			}
		}

		return true;
	}



	public function reset() {

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$questions = LearningTestquest::model()->findAll(array(
				'join' => 'INNER JOIN '.LearningTestQuestRel::model()->tableName().' qr ON qr.id_question = t.idQuest',
				'condition' => 'qr.id_test = :idTest',
				'params' => array(':idTest' => $this->idTest)
			));
			if (!empty($questions)) {
				foreach ($questions as $question) {
					$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
					if ($questionManager && method_exists($questionManager, 'deleteUserAnswer')) {
						$questionManager->deleteUserAnswer($this->getPrimaryKey(), $question->getPrimaryKey());
					}
				}
			}

			$rs = LearningTesttrackPage::model()->deleteAllByAttributes(array('idTrack' => $this->getPrimaryKey()));
			if ($rs === false) { throw new CException('Error while resetting track info'); }
			$rs = LearningTesttrackQuest::model()->deleteAllByAttributes(array('idTrack' => $this->getPrimaryKey()));
			if ($rs === false) { throw new CException('Error while resetting track info'); }

			$newTrackInfo = array(
				'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'date_end_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'date_attempt_mod' => NULL,
				'last_page_seen' => 0,
				'last_page_saved' => 0,
				'score' => 0,
				'bonus_score' => 0,
				'score_status' => self::STATUS_NOT_COMPLETE
			);
			$this->setAttributes($newTrackInfo);
			if (!$this->save()) {
				throw new CException('Error while resetting track info');
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}



	public function getAnsweredQuestions() {
		$q = Yii::app()->db->createCommand()
			->select('ttq.*, tq.type_quest')
			->from(LearningTesttrackQuest::model()->tableName().' ttq')
			->join(LearningTestquest::model()->tableName().' tq', 'tq.idQuest = ttq.idQuest')
			->where('idTrack = :id_track', array(':id_track' => $this->getPrimaryKey()))
			->andWhere('tq.type_quest <> :type_1', array(':type_1' => LearningTestquest::QUESTION_TYPE_TITLE))
			->andWhere('tq.type_quest <> :type_2', array(':type_2' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE));

		if($this->test && $this->test->shuffle_answer == LearningTest::SHUFFLE_ANSWER_RANDOM){
			//$list = $q->order('RAND()')->queryAll();
			//NOTE: this function is used for reporting issue. We need to retrieve the original display order, so we are going
			// to order data by page and display sequence. Searching a random ("RAND()") order here does not make much sense.
			$list = $q->order('ttq.page ASC, ttq.quest_sequence')->queryAll();
		}else{
			$q->order('ttq.page, ttq.quest_sequence ASC');
			$list = $q->queryAll();
		}
		return $list;
	}

	public function getMaxManualScore() {
		$questions = $this->getAnsweredQuestions();
		$manualScore = 0;
		foreach ($questions as $question) {
			$questionManager = CQuestionComponent::getQuestionManager($question['type_quest']);
			$maxScore = $questionManager->getMaxScore($question['idQuest']);

			if ($questionManager->getScoreType() == CQuestionComponent::SCORE_TYPE_MANUAL) {
				$manualScore = round($manualScore + $maxScore, 2);
			}
		}
		if($this->test->point_type == LearningTest::POINT_TYPE_PERCENT) {
			$totalMaxScore = $this->test->getMaxScore($this->idUser, true);
			if($totalMaxScore > 0)
				$manualScore = round(100 * $manualScore / $totalMaxScore, 1, PHP_ROUND_HALF_EVEN);
		}

		return $manualScore;
	}

	/**
	 *  Retrieve exisitng tracking record OR create new one if it does not exist and if requested
	 *
	 * @param number $idTest
	 * @param number $idUser
	 * @param number $idCourse
	 * @param boolean $create
	 * @return boolean | LearningTesttrack
	 */
	public static function getTrack($idTest, $idUser, $idCourse, $create = true) {
		$track = LearningTesttrack::model()->findByAttributes(array('idTest' => $idTest, 'idUser' => $idUser));

		// If track not found, create new one
		if (!$track && $create) {
			$loModel = LearningOrganization::model()->findByAttributes(array(
				'idResource' 	=> $idTest,
				'objectType' 	=> LearningOrganization::OBJECT_TYPE_TEST,
				'idCourse' 		=> $idCourse,
			));
			$track = new LearningTesttrack();
			$track->idTest = $idTest;
			$track->idUser = $idUser;
			$track->idReference = $loModel->idOrg;
			$track->date_attempt = Yii::app()->localtime->toLocalDateTime();
			$track->date_end_attempt = Yii::app()->localtime->toLocalDateTime();
			$track->last_page_seen = 0;
			$track->number_of_save = 0;
			$track->number_of_attempt = 0;
			$track->score_status = LearningTesttrack::STATUS_DOING;
			$track->save();
		}
		else if (!$track) {
			$track = false;
		}
		return $track;
	}


	/**
	 * Sometimes tests may be left uncomplete with partial question and answer tracking, but tracking for all question is needed.
	 * This function will fill all untracked question and answers forcing empty answers.
	 */
	public function fillUnansweredQuestions() {

		if($this->test->order_type != LearningTest::ORDER_TYPE_RANDOM_SUBGROUP && $this->test->order_type != LearningTest::ORDER_TYPE_RANDOM_CATEGORY) {
			$questions = $this->test->getQuestions(false); //get all test questions, excluding page breaks and titles
			if (!empty($questions)) {

				//retrieve already tracked questions, these won't be touched
				$listTrackedQuestions = array();
				$models = LearningTesttrackQuest::model()->findAllByAttributes(array('idTrack' => $this->getPrimaryKey()));
				foreach ($models as $model) {
					$listTrackedQuestions[] = $model->idQuest;
				}

				//retrieve all answered questions ids, these will be used to exclude them in the answers filling process
				$listIdQuestionAnswered = array();
				$answered = $this->getAnsweredQuestions();
				if (!empty($answered)) {
					foreach ($answered as $item) {
						$listIdQuestionAnswered[] = $item->idQuest;
					}
				}

				//then, for each test question, check if it is unanswered
				foreach ($questions as $question) {

					//build question tracking
					if (!in_array($question->getPrimaryKey(), $listTrackedQuestions)) { //if question has not been tracked yet, then force the tracking
						$trackQuestion = new LearningTesttrackQuest();
						$trackQuestion->idTrack = $this->getPrimaryKey();
						$trackQuestion->idQuest = $question->getPrimaryKey();
						$trackQuestion->page = $question->page; //this may be incorrect depending on test options, but it shouldn't be important here
						$trackQuestion->save();
					}

					//build answers tracking
					if (!in_array($question->getPrimaryKey(), $listIdQuestionAnswered)) { //if this answer do not exists in answered question, then fill it with empty value
						if ($questionObject = CQuestionComponent::getQuestionManager($question->type_quest)) {
							$questionObject->storeUserAnswer($this->getPrimaryKey(), $question->getPrimaryKey(), array(), false);
						}
					}

				}

			}
		} elseif($this->test->order_type == LearningTest::ORDER_TYPE_RANDOM_SUBGROUP) {
			//retrieve count of already tracked questions
			$answeredCount = LearningTesttrackQuest::model()->countByAttributes(array('idTrack' => $this->getPrimaryKey()));
			$questionRandomNumber = $this->test->question_random_number;

			//retrieve not shown questions
			$leftQuestions = $this->getNotShownQuestions();

			//track questions
			for($i = 0; $i < $questionRandomNumber - $answeredCount && count($leftQuestions); $i++) {
				//calculate the max offset
				$maxOffset = count($leftQuestions) - 1;

				//choose random question
				$questionIndex = rand(0, $maxOffset);
				$question = $leftQuestions[$questionIndex];

				//track the question
				$testTrackQuestion = new LearningTesttrackQuest();
				$testTrackQuestion->idTrack = $this->getPrimaryKey();
				$testTrackQuestion->idQuest = $question['idQuest'];
				$testTrackQuestion->page = $question['page'];
				$testTrackQuestion->save(false);

				//remove the question from the list with the left questions
				unset($leftQuestions[$questionIndex]);
				$leftQuestions = array_values($leftQuestions);
			}
		} elseif($this->test->order_type == LearningTest::ORDER_TYPE_RANDOM_CATEGORY) {
			//retrieve categories info
			$categoriesInfo = CJSON::decode($this->test->order_info);
			//retrieve count of already tracked questions for each category
			$results = Yii::app()->db->createCommand()
				->select('COUNT(ttq.idQuest) AS countQuestions, tq.idCategory')
				->from(LearningTesttrackQuest::model()->tableName().' ttq')
				->join(LearningTestquest::model()->tableName().' tq', 'tq.idQuest=ttq.idQuest')
				->where('ttq.idTrack = :idTrack', array(':idTrack' => $this->getPrimaryKey()))
				->group('tq.idCategory')
				->queryAll();
			$trackedQuestions = array();
			foreach($results as $result) {
				$trackedQuestions[$result['idCategory']] = $result['countQuestions'];
			}

			foreach($categoriesInfo as $category) {
				if($category['selected']) {
					//retrieve not shown questions
					$leftQuestions = $this->getNotShownQuestions($category['id_category']);

					//track questions
					for($i = 0; $i < $category['selected'] - $trackedQuestions[$category['id_category']] && count($leftQuestions); $i++) {
						//calculate the max offset
						$maxOffset = count($leftQuestions) - 1;
						//choose random question
						$questionIndex = rand(0, $maxOffset);
						$question = $leftQuestions[$questionIndex];

						//track the question
						$testTrackQuestion = new LearningTesttrackQuest();
						$testTrackQuestion->idTrack = $this->getPrimaryKey();
						$testTrackQuestion->idQuest = $question['idQuest'];
						$testTrackQuestion->page = $question['page'];
						$testTrackQuestion->save(false);

						//remove the question from the list with the left questions
						unset($leftQuestions[$questionIndex]);
						$leftQuestions = array_values($leftQuestions);
					}
				}
			}
		}
	}

	/**
	 * get all not shown questions
	 * @param bool/int $questionCategory
	 * @return mixed
	 */
	public function getNotShownQuestions($questionCategory = false) {
		$q = Yii::app()->db->createCommand()
			->select('tq.idQuest, tqr.page')
			->from(LearningTestquest::model()->tableName().' tq')
			->join(LearningTestQuestRel::model()->tableName().' tqr',
				'tq.idQuest=tqr.id_question AND tqr.id_test = :idTest', array(
					':idTest' => $this->test->idTest
				))
			->leftJoin(
				LearningTesttrackQuest::model()->tableName().' ttq',
				'ttq.idTrack = :idTrack AND tq.idQuest = ttq.idQuest',
				array(':idTrack' => $this->getPrimaryKey())
			)
			->where('ttq.idQuest IS NULL')
			->andWhere('tq.type_quest <> :type_1', array(':type_1' => LearningTestquest::QUESTION_TYPE_TITLE))
			->andWhere('tq.type_quest <> :type_2', array(':type_2' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE));

		if($questionCategory) {
			$q->andWhere('tq.idCategory = :idCategory', array(':idCategory' => $questionCategory));
		}

		return $q->queryAll();
	}

	/**
	 * @param $idTest
	 * @return mixed
	 */
	public static function getUserCountByTest($idTest)
	{
		return Yii::app()->db->createCommand()
			->select('count(idUser)')
			->from(LearningTesttrack::model()->tableName())
			->where('idTest = :idTest', array(':idTest' => $idTest))
			->group('idTest')
			->queryScalar();
	}

	/**
	 * A method called by afterSave(). Checks if the currently tracked
	 * Test LO is the final object inside its course and if yes
	 * updates the global (course level) learning_courseuser.score_given
	 * with the score points that the user received for this Test track
	 */
	protected function _updateGlobalCourseScoreIfNeeded(){
		$idCourse = Yii::app()->getDb()->createCommand()
		               ->select('idCourse')
		               ->from(LearningOrganization::model()->tableName())
		               ->where('idOrg=:idReference AND objectType=:test', array(':idReference'=>$this->idReference, ':test'=>LearningOrganization::OBJECT_TYPE_TEST))
		               ->queryScalar();

		if($idCourse){
			// Trigger update of 'score_given'
			LearningCourseuser::recalculateLastScoreByUserAndCourse($idCourse, $this->idUser);

			// also update initial_score_given
			LearningCourseuser::recalculateInitialScoreGiven($this->idUser, $idCourse);
		}
	}
}
