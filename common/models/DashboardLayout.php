<?php

/**
 * This is the model class for table "dashboard_layout".
 *
 * The followings are the available columns in table 'dashboard_layout':
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property integer $default
 * @property integer $ufilter_option
 * @property integer $puprofiles_filter_option
 * @property integer $is_fallback
 * @property integer $readonly
 *
 * The followings are the available model relations:
 * @property DashboardAssign[] $assigns
 * @property DashboardRow[] $rows
 */
class DashboardLayout extends CActiveRecord
{
	
	const LAYOUT_OPTION_ALL 		= 1;
	const LAYOUT_OPTION_SELECTED 	= 2;
	
	/**
	 * Idenitfiers for Virtual, non-real, in-memory layouts
	 * 
	 * @var integer
	 */
	// "single cell layout" (view all my courses uses this approach)
	const VIRTUAL_TYPE_SINGLE_CELL	= -1;
	
	public $search_input;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_layout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('position, default, ufilter_option, puprofiles_filter_option, is_fallback', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, position, default, ufilter_option, puprofiles_filter_option, is_fallback', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'assigns' => array(self::HAS_MANY, 'DashboardAssign', 'id_layout'),
			'rows' => array(self::HAS_MANY, 'DashboardRow', 'id_layout', 'order' => 'rows.position ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'position' => 'Position',
			'default' => 'Default',
			'ufilter_option' => 'Ufilter Option',			
			'puprofiles_filter_option' => 'Power User Profiles Filter Option',
			'is_fallback' => 'Is this layout the ultimate fallback if any other layout doesn not match',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('default',$this->default);
		$criteria->compare('ufilter_option',$this->ufilter_option);		
		$criteria->compare('puprofiles_filter_option',$this->puprofiles_filter_option);
		$criteria->compare('is_fallback',$this->is_fallback);
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardLayout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProvider() {
	
		// SQL Parameters
		$params = array();
	
		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from('dashboard_layout t');
	
		// Also search by user related text
		if ($this->search_input != null) {
			$commandBase->andWhere('CONCAT(t.name) LIKE :search');
			$params[':search'] = '%'.$this->search_input.'%';
		}
		
		// DATA
		$commandData = clone $commandBase;
	
	
		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id)');
		$numRecords = $commandCounter->queryScalar($params);
	
	
		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'position'	=> array(
				'asc' 	=> 't.position ASC, t.id ASC',
				'desc'	=> 't.position DESC, t.id DESC',
			),	
			'name' => array(
				'asc' 	=> 't.name',
				'desc'	=> 't.name DESC',
			),
		);
		$sort->defaultOrder = array(
			'position'		=>	CSort::SORT_ASC,
		);
	
		// DATA-II
		$commandData->select("*");
	
	
		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> array('pageSize' => $pageSize),
			'keyField'			=> 'id',
			'sort' 				=> $sort,
			'params'			=> $params,
		);
	
		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);
	
		return $dataProvider;
	
	}
	
	
	
	/**
	 * Count dashlets associated to this LAYOUT
	 *
	 * @param integer $id
	 *
	 * @return integer
	 */
	public function countLayoutDashlets($id=false) {
		
		$availHandlers = array();
		foreach (Yii::app()->mydashboard->getAvailHandlers() as $key => $value)
			$availHandlers[] = "'" . $value . "'";
		
		if (!$id)
			$id = $this->id;
		$sql = "
			SELECT count(*)
				FROM dashboard_layout l 
				JOIN dashboard_row r ON (r.id_layout=l.id)
				JOIN dashboard_cell c ON (c.row=r.id)
				JOIN dashboard_dashlet dl ON (dl.cell=c.id)
				WHERE (l.id=:id) AND (dl.handler IN (" . implode(",", $availHandlers) . "))
		";

		$command = Yii::app()->db->createCommand($sql);
		$number = $command->queryScalar(array(':id' => $id));
		return $number;
	}
	

	
	/**
	 * Check if THIS laout has any assigned users, groups or branches
	 * 
	 * @return boolean
	 */
	public function hasUserFilter() {
		// Quote them (SQL IN()!!! later)
		$relatedTypes = DashboardAssign::$userFilterTypes;
		foreach ($relatedTypes as $k => $v)
			$relatedTypes[$k] = "'" . $v . "'";
		
		$sql = "
			SELECT dl.id
			FROM dashboard_layout dl
			JOIN dashboard_assign da ON ((da.id_layout=dl.id) AND (da.item_type IN (". implode(',',$relatedTypes)  .")))
			WHERE dl.id=:id
			LIMIT 1
		";
		$params[":id"] = $this->id;
		
		$result = Yii::app()->db->createCommand($sql)->queryScalar($params);
		
		return $result;
	}
	
	/**
	 * Return Array of list of users, groups and branches, assigned to THIS layout
	 * 
	 * @return array
	 */
	public function getUserFilterInfo() {
		
		// Quote them (SQL IN()!!! later)
		$relatedTypes = DashboardAssign::$userFilterTypes;
		foreach ($relatedTypes as $k => $v)
			$relatedTypes[$k] = "'" . $v . "'";
		
		$users = array();
		$groups = array();
		$branches = array();
		
		$sql = "
			SELECT * 
			FROM dashboard_layout dl
			JOIN dashboard_assign da ON ((da.id_layout=dl.id) AND (da.item_type IN (". implode(',',$relatedTypes)  .")))
			WHERE dl.id=:id
		";
		$params[":id"] = $this->id;
		
		
		$reader = Yii::app()->db->createCommand($sql)->query($params);
		foreach ($reader as $row) {
			switch ($row['item_type']) {
				case DashboardAssign::ITEM_TYPE_USER:
					$users[] = (int) $row['id_item'];
					break;
				case DashboardAssign::ITEM_TYPE_GROUP:
					$groups[] = (int) $row['id_item'];
					break;
				case DashboardAssign::ITEM_TYPE_BRANCH:
				case DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC:
					$element = array(
						"key" => (int) $row['id_item'],
						"selectState" => $row['item_type'] == DashboardAssign::ITEM_TYPE_BRANCH ? "1" : "2",
					);
					$branches[] = $element;
					break;
			}
		}
		
		return array(
			'users' 		=> $users,
			'groups'		=> $groups,
			'branches'		=> $branches,		
		);
	}
	
	
	/**
	 * Return list of power user profiles assigned to THIS layout
	 * 
	 * @return array
	 */
	public function getAssignedPuProfiles() {
		$result = array();
		$sql = "
			SELECT *
			FROM dashboard_layout dl
			JOIN dashboard_assign da ON ((da.id_layout=dl.id) AND (da.item_type=:pu_profile))
			WHERE dl.id=:id
		";
		$params[":id"] = $this->id;
		$params[":pu_profile"] = DashboardAssign::ITEM_TYPE_PUPROFILE;
		$reader = Yii::app()->db->createCommand($sql)->query($params);
		foreach ($reader as $row) {
			$result[] = $row['id_item'];
		}
		return $result;
	}
	
	

	/**
	 * 
	 * @return boolean|string
	 */
	public function getLevelFilter() {
		$assignment = DashboardAssign::model()->findByAttributes(array('id_layout' => $this->id, 'item_type' => DashboardAssign::ITEM_TYPE_USERLEVEL));
		if ($assignment) {
			return $assignment->id_item;
		}
		return false;
	}
	
	
	/**
	 * 
	 * @param array $data
	 */
	public function assignUsers($data) {

		// First, remove old assignments
		DashboardAssign::model()->deleteAllByAttributes(array(
			'id_layout' => $this->id,
			'item_type'	=> DashboardAssign::ITEM_TYPE_USER,
		));
		
		foreach ($data as $item) {
			$model = new DashboardAssign();
			$model->id_layout = $this->id;
			$model->item_type = DashboardAssign::ITEM_TYPE_USER;
			$model->id_item = $item;
			$model->save();
		}
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function assignGroups($data) {
		
		// First, remove old assignments
		DashboardAssign::model()->deleteAllByAttributes(array(
			'id_layout' => $this->id,
			'item_type'	=> DashboardAssign::ITEM_TYPE_GROUP,
		));
		
		foreach ($data as $item) {
			$model = new DashboardAssign();
			$model->id_layout = $this->id;
			$model->item_type = DashboardAssign::ITEM_TYPE_GROUP;
			$model->id_item = $item;
			$model->save();
		}
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public function assignBranches($data) {
		
		// First, remove old assignments
		DashboardAssign::model()->deleteAllByAttributes(array(
			'id_layout' => $this->id,
			'item_type'	=> array(DashboardAssign::ITEM_TYPE_BRANCH, DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC),
		));
		
		
		foreach ($data as $item) {
			$model = new DashboardAssign();
			$model->id_layout = $this->id;
			$model->item_type = $item['selectState'] == 1 ? DashboardAssign::ITEM_TYPE_BRANCH : DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC;
			$model->id_item = $item['key'];
			$model->save();
		}
	}
	
	
	
	/**
	 *
	 * @param array $data
	 */
	public function assignPuProfiles($data) {
	
		// First, remove old assignments
		DashboardAssign::model()->deleteAllByAttributes(array(
			'id_layout' => $this->id,
			'item_type'	=> array(DashboardAssign::ITEM_TYPE_PUPROFILE),
		));
	
		foreach ($data as $item) {
			$model = new DashboardAssign();
			$model->id_layout = $this->id;
			$model->item_type = DashboardAssign::ITEM_TYPE_PUPROFILE;
			$model->id_item = $item;
			$model->save();
		}
		
	}
	
	
	
	
	/**
	 * Make a copy of THIS layout, including all the content (rows, cells, dashlets, assignments...)
	 * 
	 * @param string $name The "name" of the cloned layout, optional, by default = "Copy of <original-name>"

	 * @return DashboardLayout
	 */
	public function cloneMe($name = false) {
		
		if (!$name) 
			$name = "Copy of " . $this->name;
		
		if (Yii::app()->db->getCurrentTransaction() === NULL) $transaction = Yii::app()->db->beginTransaction();
		
		try {
			// Clone the dashboard_layout record
			$clone = new self();
			$clone->name = $name;
			$clone->position = $this->position;
			$clone->default = 0;
			$clone->ufilter_option = $this->ufilter_option; 
			$clone->puprofiles_filter_option = $this->puprofiles_filter_option;
			$clone->save();
			
			$cloneId = $clone->id;
			$originalId = (int) $this->id;
			
			foreach ($this->rows as $row) {
				// Clone the dashboard_row record
				$newRow = new DashboardRow();
				
				// Attribute by attribute, to avoid thousands of log message about "Unable to set attribute..." 
				$newRow->id_layout 	= $cloneId; // !!
				$newRow->position 	= $row->position;
				$newRow->save();
				foreach ($row->cells as $cell) {
					// Clone the dashboard_cell record
					$newCell = new DashboardCell();
					$newCell->row 		= $newRow->id; // !!
					$newCell->position 	= $cell->position;
					$newCell->width 	= $cell->width;
					$newCell->save();
					foreach ($cell->dashlets as $dashlet) {

						$modelTranslations = DashboardDashletTranslation::model()->findAllByAttributes(array(
							'id_dashlet' => $dashlet->id
						));

						// Clone the dashboard_dashlet record
						$newDashlet = new DashboardDashlet();
						$newDashlet->handler 	= $dashlet->handler;
						$newDashlet->cell 		= $newCell->id; // !!
						$newDashlet->position 	= $dashlet->position;
						$newDashlet->height 	= $dashlet->height;
						$newDashlet->header 	= $dashlet->header;
						$newDashlet->params 	= $dashlet->params;
						$newDashlet->save();

						foreach($modelTranslations as $translation){
							$newTranslations = new DashboardDashletTranslation();
							$newTranslationsAttrs = $translation->attributes;
							$newTranslationsAttrs['id_dashlet'] = $newDashlet->id;
							$newTranslations->attributes = $newTranslationsAttrs;
							$newTranslations->save();
						}

					}
				}
			}
	
			// Copy Layout assignments, if any, from dashboard_assign tables
			if (DashboardAssign::model()->countByAttributes(array('id_layout' => $this->id)) > 0) {
				$tempTableName = "dashboard_assign_" . Docebo::randomHash();
				$sql = "
					CREATE TEMPORARY TABLE $tempTableName SELECT * FROM dashboard_assign WHERE id_layout = $originalId;
				
					INSERT INTO dashboard_assign (id_layout, item_type, id_item)
					SELECT :clone_id,item_type,id_item
					FROM $tempTableName
					WHERE id_layout=:original_id;
					
					DROP TEMPORARY TABLE IF EXISTS $tempTableName;
				";
					
				$params = array(
					':original_id' 	=> $originalId,
					':clone_id'		=> $cloneId,
				);
		
				if (!Yii::app()->db->createCommand($sql)->execute($params)) {
					throw new Exception("Failed to execute layout assignments cloning");
				}
			}

			if (isset($transaction)) $transaction->commit();
				
			return $clone;
			
		
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			if (isset($transaction)) $transaction->rollback();
			return false;
		}
		
		
	}

	
	/**
	 * 
	 * @return boolean
	 */
	public function isForPowerUsers() {
		$tmp = DashboardAssign::model()->countByAttributes(array(
			'id_layout' => $this->id, 
			'item_type' => DashboardAssign::ITEM_TYPE_USERLEVEL,
			'id_item'	=> Yii::app()->user->level_admin,	
		));
		return $tmp > 0;
	}
	
	
	
	/**
	 * 
	 * @return void|DashboardLayout
	 */
	public static function createFallbackLayout() {
		
		// Check if we already have one
		$fallbackLayout = self::model()->findByAttributes(array('is_fallback' => 1));
		if ($fallbackLayout)
			return false;
		
		$fallbackLayout = new self();
		$fallbackLayout->name = "Standard layout";
		$fallbackLayout->position = 9999;
		$fallbackLayout->default = 0;
		$fallbackLayout->ufilter_option = 1;
		$fallbackLayout->puprofiles_filter_option = 1;
		$fallbackLayout->is_fallback = 1;
		$fallbackLayout->save();
 		
		$row = new DashboardRow();
		$row->id_layout = $fallbackLayout->id;
		$row->position = 0;
		$row->save();
		
		$cell = new DashboardCell();
		$cell->row = $row->id;
		$cell->position = 0;
		$cell->width = 12;
		$cell->save();
		
		$dashlet = new DashboardDashlet();
		$dashlet->handler = "mydashboard.widgets.DashletMyCourses";
		$dashlet->cell = $cell->id;
		$dashlet->position = 0;
		$dashlet->height = 0;
		$dashlet->header = "";
		$dashlet->params = json_encode(array());
		$dashlet->save();
		
		
		return $fallbackLayout;
		
	}
	

	

	/**
	 * Return a Virtual layout of given type
	 * 
	 * @param integer $type
	 * @param array|DashboardLayout $dashlet dashlet(s) this layout to consist of
	 * 
	 * @return NULL|DashboardLayout
	 */
	public static function getVirtualLayout($type, $dashlets) {
		$layout = null;
		switch ($type) {
			case self::VIRTUAL_TYPE_SINGLE_CELL:
				if ($dashlets) {
					$layout = self::getSingleCellLayout($dashlets);
				}
				break;
		}
		return $layout;
	}
	
	
	
	/**
	 * Create a virtual, in memory layout model having single row & single cell inside it (and all dashlets inside that cell)72.
	 * Used to display a given dashlet(s) in FULL width, stand alone in the My Dashboard screen 
	 * 
	 * @param array $dashlets Array of DashboardDashlet or single DashboardDashlet
	 * 
	 * @return DashboardLayout
	 */
	public static function getSingleCellLayout($dashlets) {
	
		$cell		= new DashboardCell();
		$row		= new DashboardRow();
		$layout		= new DashboardLayout();
	
		$layout->id = DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL;
	
		$row->id = DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL;
		$row->layout = $layout;
		$row->id_layout = $layout->id;
	
		$cell->id = DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL;
		$cell->row = $row->id;
		$cell->dashboardRow = $row;
		$cell->width = 12; // request full width for cell and dashlets (propagated by JavaScript)
	
		if (!is_array($dashlets)) {
			$dashlets = array($dashlets);
		}
		
		foreach ($dashlets as $dashlet) {
			$dashlet->dashboardCell = $cell;
			$dashlet->cell = $cell->id;
		}
		
		$cell->dashlets = $dashlets;
		$row->cells = array($cell);
		$layout->rows = array($row);
	
		return $layout;
	
	}
	

	/**
	 * Get exsiting dashlets in this Layout
	 * 
	 * @param boolean $idsOnly If TRUE, return list of IDs instead of list of models
	 * 
	 * @return mixed
	 */
	public function getDashlets($idsOnly=false) {
	
		$availHandlers = array();
		foreach (Yii::app()->mydashboard->getAvailHandlers() as $key => $value)
			$availHandlers[] = "'" . $value . "'";
	
		$sql = "
			SELECT dl.id
				FROM dashboard_layout l
				JOIN dashboard_row r ON (r.id_layout=l.id)
				JOIN dashboard_cell c ON (c.row=r.id)
				JOIN dashboard_dashlet dl ON (dl.cell=c.id)
				WHERE (l.id=:id) AND (dl.handler IN (" . implode(",", $availHandlers) . "))
		";
	
		$command = Yii::app()->db->createCommand($sql);
		
		$ids = $command->queryAll(true, array(':id' => $this->id));
		
		if ($idsOnly)
			return $ids;
		
		$result = array();
		foreach ($ids as $id) {
			$result[] = DashboardDashlet::model()->findByPk($id);
		}
		
		return $result;
		
		
	}
	

	/**
	 * Get list of disallowed dashlets for THIS layout (because they already exists in THIS layout && they have disallowMultiInstance in their descriptor)
	 * 
	 * @return array
	 */
	public function getDisallowedDashlets() {
		$existingDashlets = $this->getDashlets();
		$disallowedDashlets = array();
		foreach ($existingDashlets as $dashlet) {
			$descriptor = $dashlet->getDescriptor();
			if ($descriptor && $descriptor->disallowMultiInstance) {
				$disallowedDashlets[] = $descriptor->name;
			}
		}
		return $disallowedDashlets;
	}
	
	
	/**
	 * Decide what page size to use for virtual layouts.
	 * Return FALSE if you want NO pagination.
	 * 
	 * @param array $params
	 * 
	 * @return integer
	 */
	public static function getVirtualLayoutPageSize($params = array()) {
	    return Settings::get('elements_per_page', false);
	}
	
	
	
}
