<?php

/**
 * This is the model class for table "learning_course_catch_up".
 *
 * The followings are the available columns in table 'learning_course_catch_up':
 * @property integer $id_path
 * @property integer $id_course
 * @property integer $id_catch_up
 *
 * The followings are the available model relations:
 * @property LearningCourse $idCatchUp
 * @property LearningCourse $idCourse
 * @property LearningCoursepath $idPath
 */
class LearningCourseCatchUp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_catch_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_catch_up', 'required', 'message'=>Yii::t('coursepath', 'Please select one course')),
			array('id_path, id_course, id_catch_up', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_path, id_course, id_catch_up', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catchUpCourseModel' => array(self::BELONGS_TO, 'LearningCourse', 'id_catch_up'),
			'courseModel' => array(self::BELONGS_TO, 'LearningCourse', 'id_course'),
			'pathModel' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_path' => 'Id Path',
			'id_course' => 'Id Course',
			'id_catch_up' => 'Id Catch Up',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_path',$this->id_path);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_catch_up',$this->id_catch_up);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseCatchUp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get assigned catchup course (if any) for the given LP an course
	 * @param $idPath
	 * @param $idCourse
	 * @return array|mixed|null|static
	 */
	public static function loadAssignedCatchup($idPath, $idCourse)
	{
		return self::model()->findByAttributes(array(
			'id_path' => $idPath,
			'id_course' => $idCourse
		));
	}
}