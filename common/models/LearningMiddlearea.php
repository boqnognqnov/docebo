<?php

/**
 * This is the model class for table "learning_middlearea".
 *
 * The followings are the available columns in table 'learning_middlearea':
 * @property string $obj_index
 * @property integer $disabled
 * @property string $idst_list
 */
class LearningMiddlearea extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningMiddlearea the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_middlearea';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst_list', 'required'),
			array('disabled', 'numerical', 'integerOnly'=>true),
			array('obj_index', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('obj_index, disabled, idst_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'obj_index' => 'Obj Index',
			'disabled' => 'Disabled',
			'idst_list' => 'Idst List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('obj_index',$this->obj_index,true);
		$criteria->compare('disabled',$this->disabled);
		$criteria->compare('idst_list',$this->idst_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	public function currentCanAccessObj (){

		if($this->disabled==1) return false;

		//if(Yii::app()->user->isGodAdmin) return true;

		$list = unserialize($this->idst_list);

		$user_assigned = Yii::app()->authManager->getAuthAssignmentsIds(Yii::app()->user->id);

		if(empty($list)) {
			return true;
		} else {
			if (!is_array($user_assigned)) { $user_assigned = array(); }
			if (!is_array($list)) { $list = array(); }
			$intersect = array_intersect($user_assigned, $list);
		}

		return !empty($intersect);

	}
    
    /**
     * Get list of disabled objects
     * 
     * @return array
     */
    public static function getDisabledList() {

		$disabled = array();

        $criteria = new CDbCriteria();
        $criteria->select = 't.obj_index';
        $criteria->condition = 't.disabled = :disabled';
        $criteria->params[':disabled'] = '1';
        $rows = self::model()->findAll($criteria);
        
        foreach ($rows as $row)
            $disabled[$row->obj_index] = $row->obj_index;

		return $disabled;
    }


}