<?php

class BrandingCustomizeCssForm extends AdvancedSettingsBaseForm {

	protected $_params;
	protected $_cssFiles = array();

	public $custom_css;

	public function getCssFiles() {
		return $this->_cssFiles;
	}

	public function rules() {
		return array(
			array('custom_css', 'safe')
		);
	}

	public function beforeSave() {
		if ($this->custom_css == "") {
			Yii::app()->theme->deleteCustomCss();
		}
	}

	public function attributeLabels() {
		return array(
			'custom_css' => Yii::t('templatemanager', 'Insert here yours modify css classes'),
		);
	}

	public function init() {
		$fields = array(
			'custom_css',
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			$this->_params[$row['param_name']] = $row;
			$this->{$row['param_name']} = $row['param_value'];
		}

		$this->_cssFiles = array(
			'base' => array(
				'name' => 'common.css',
				'filepath' => _base_ .'/themes/'. Yii::app()->theme->name . '/css/common.css',
			),
			'admin' => array(
				'name' => 'admin.css',
				'filepath' => _base_ .'/themes/'. Yii::app()->theme->name . '/css/admin.css',
			),
		);

		$customCssFilepath = $this->resolveCustomCss();
		if ($customCssFilepath) {
			$this->_cssFiles['custom'] = array(
				'name' => 'custom.css',
				'filepath' => $customCssFilepath,
			);
		}
	}

	private function resolveCustomCss() {
		$filepath = Yii::app()->theme->checkCustomCss();

		if (!$filepath && $this->custom_css != "") {
			$filepath = Yii::app()->theme->createCustomCss($this->custom_css);
		}

		return $filepath;
	}

	public function sendCss($name) {
		if (!array_key_exists($name, $this->_cssFiles) && $name != 'custom') {
			return false;
		}

		if ($name == 'custom') {
			Yii::app()->request->sendFile('custom_'. Yii::app()->theme->name .'.css', $this->custom_css);
		} else {
			Yii::app()->request->sendFile($this->_cssFiles[$name]['name'], file_get_contents($this->_cssFiles[$name]['filepath']));
		}
	}
}