<?php

class BrandingCoursePlayerForm extends CFormModel {

	protected $_params;
	protected $_logo;

	public $player_bg;
    public $player_bg_aspect;
    public $html_page_css;

	public $player_layout;

	/*public function getLogo() {
		return $this->_logo;
	}

	public function setLogo($value) {
		$this->_logo = $value;
		return true;
	}*/

    public function renderCoursePlayerBg($variant = CoreAsset::VARIANT_ORIGINAL) {

        $src = '';
        if (!empty($this->player_bg)) {
        	$src = CoreAsset::url($this->player_bg, $variant);
        }

        if (empty($src)) {
            $src = Yii::app()->theme->getBaseUrl() . '/images/course_player/' . Yii::app()->params['defaultCoursePlayerBgFileName'];
        }

        return CHtml::image($src);

    }

	public function onAfterSave() { }

	public function rules() {
		return array(
			array('player_layout, player_bg, player_bg_aspect, html_page_css', 'safe')
		);
	}

	public function init() {

        //  the settings do not exist

		$this->player_layout = Settings::get('player_layout');
		if(!$this->player_layout){
			Settings::save('player_layout', LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON);
			$this->player_layout = LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON;
		}

        $this->player_bg = Settings::get('player_bg');
        if (!$this->player_bg) {
            Settings::save('player_bg', '');
            $this->player_bg = '';
        }
        $this->player_bg_aspect = Settings::get('player_bg_aspect');
        if (!$this->player_bg_aspect) {
            Settings::save('player_bg_aspect', 'fill');
            $this->player_bg_aspect = 'fill';
        }
        $this->html_page_css = Settings::get('html_page_css');
        if (!$this->html_page_css) {
            Settings::save('html_page_css', '');
            $this->html_page_css = '';
        }
	}

	public function attributeLabels() {
		return array(
			'player_layout'=>Yii::t('standard', 'Training Material View'),
			'player_bg' => Yii::t('certificate', '_BACK_IMAGE'),
			'html_page_css' => Yii::t('branding', 'HTML page CSS')
		);
	}

	public function beforeSave() {
	}

    public function save() {
        // try to upload a new file
        //$this->beforeSave();

        if (!empty($this->player_bg_aspect) && in_array($this->player_bg_aspect, array('fill', 'tile'))) {
            Settings::save('player_bg_aspect', $this->player_bg_aspect);
        }
        if (!empty($this->player_bg)) {
            Settings::save('player_bg', $this->player_bg);
        }
        if (!empty($this->html_page_css) || $this->html_page_css == '') {
            Settings::save('html_page_css', Yii::app()->htmlpurifier->purify($this->html_page_css));
        }


	    $player_layout_types = array(LearningCourse::$PLAYER_LAYOUT_PLAY_BUTTON, LearningCourse::$PLAYER_LAYOUT_NAVIGATOR, LearningCourse::$PLAYER_LAYOUT_LISTVIEW); // Default is "play_button" (blue play button)

	    if(!empty($this->player_layout) && in_array($this->player_layout, $player_layout_types)){
		    Settings::save('player_layout', $this->player_layout);
	    }
    }

}