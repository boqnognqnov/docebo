<?php

class BrandingWhiteLabelForm extends AdvancedSettingsBaseForm
{
	
	const MOBILE_DOCEBO_APP 			= 'download_docebo_app';
	const MOBILE_REDIRECT_DESKTOP 		= 'redirect_desktop';
	const MOBILE_OWN_APP 				= 'download_own_app';
	
	const MOBILE_DEFAULT				= self::MOBILE_DOCEBO_APP;

	const POWEREDBY_CUSTOM_TEXT				= 'custom_text';
	const POWEREDBY_CUSTOM_URL				= 'custom_url';
	const POWEREDBY_CUSTOM_URL_HEADER		= 'custom_url_header';

	protected $_params;

	public $whitelabel_header;
	public $whitelabel_header_ext_url;
	public $whitelabel_header_ext_height;
	public $whitelabel_footer;
	public $whitelabel_footer_text;
	public $whitelabel_footer_ext_height;
	public $whitelabel_footer_ext_url;
	public $whitelabel_menu;
	public $whitelabel_menu_userid;
	public $whitelabel_helpdesk;
	public $whitelabel_helpdesk_email;
	public $whitelabel_naming;
	public $whitelabel_naming_text;
	public $whitelabel_naming_site_enable;
	public $whitelabel_naming_site;
	public $whitelabel_mobile;
	public $whitelabel_mobile_apple_store_link;
	public $whitelabel_mobile_google_play_store_link;
	public $whitelabel_mobile_replace_splashscreen;	
	public $whitelabel_mobile_splashscreen_file;
	public $whitelabel_disable_naming;
	public $language;
	public $languageForUrl;
	public $languageForUrlHeader;
	public $whitelabel_footer_text_per_language;

	public function rules()
	{
		return array(
			array('whitelabel_header_ext_height', 'numerical', 'integerOnly'=>true, 'min' => 50),
			array('whitelabel_footer_ext_height', 'numerical', 'integerOnly'=>true, 'min' => 30),
			array('whitelabel_helpdesk_email', 'email', 'allowEmpty'=>true),
			array('whitelabel_header_ext_url, whitelabel_footer_ext_url', 'url', 'allowEmpty'=>true),
			array('whitelabel_footer, whitelabel_menu, whitelabel_menu_userid, whitelabel_helpdesk, whitelabel_naming,
				   whitelabel_naming_text, whitelabel_naming_site, 
//				   whitelabel_footer_text,
				   whitelabel_mobile, whitelabel_mobile_apple_store_link, whitelabel_mobile_google_play_store_link,
				   whitelabel_mobile_replace_splashscreen, whitelabel_mobile_splashscreen_file, whitelabel_header,
				   whitelabel_naming_site_enable, whitelabel_disable_naming', 'safe'),

		);
	}

	public function init()
	{
		$fields = array(
			'whitelabel_header',
			'whitelabel_header_ext_url',
			'whitelabel_header_ext_height',
			'whitelabel_footer',
//			'whitelabel_footer_text',
			'whitelabel_footer_ext_height',
//			'whitelabel_footer_ext_url',
			'whitelabel_menu',
			'whitelabel_helpdesk',
			'whitelabel_helpdesk_email',
			'whitelabel_naming',
			'whitelabel_naming_text',
			'whitelabel_naming_site',
			'whitelabel_naming_site_enable',
			'whitelabel_mobile',
			'whitelabel_mobile_apple_store_link',
			'whitelabel_mobile_google_play_store_link',
			'whitelabel_mobile_replace_splashscreen',
			'whitelabel_mobile_splashscreen_file',	
			'whitelabel_disable_naming',
		);

		$command = Yii::app()->db->createCommand()
			->select('*')
			->from('core_setting')
			->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			$this->_params[$row['param_name']] = $row;
			$this->{$row['param_name']} = $row['param_value'];
		}
		if (!empty($_POST['valuesForText']) || !empty($_POST['valuesForUrl'])) {
			$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
			$customTexts = $_POST['valuesForText'];
			$customUrls = $_POST['valuesForUrl'];
			if($_POST['BrandingWhiteLabelForm']['whitelabel_footer'] == self::POWEREDBY_CUSTOM_TEXT) {
				$customTexts[$defaultLanguage] = trim($customTexts[$defaultLanguage]);
				if(empty($customTexts[$defaultLanguage])){
					$this->addError('whitelabel_footer', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
					return;
				}
				foreach ($customTexts as $lang => $text) {
						$langSettingModel = CoreLangWhiteLabelSetting::model()->findByAttributes(array(
								'language' => $lang,
								'type' => self::POWEREDBY_CUSTOM_TEXT,
						));

						if (!$langSettingModel) {
							$langSettingModel = new CoreLangWhiteLabelSetting();
							$langSettingModel->language = $lang;
							$langSettingModel->type = self::POWEREDBY_CUSTOM_TEXT;
						}

						$langSettingModel->value = $text;
						$langSettingModel->save();
				}
			}
			if($_POST['BrandingWhiteLabelForm']['whitelabel_footer'] == self::POWEREDBY_CUSTOM_URL) {
				$customUrls[$defaultLanguage] = trim($customUrls[$defaultLanguage]);
				if(empty($customUrls[$defaultLanguage]) || $customUrls[$defaultLanguage] == 'http://'){
					$this->addError('whitelabel_footer', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
					return;
				}
				$wrongUrls = array();
				$urlValidator = new CUrlValidator();
				foreach ($customUrls as $lang => $url) {
					$langSettingModel = CoreLangWhiteLabelSetting::model()->findByAttributes(array(
							'language' => $lang,
							'type' => self::POWEREDBY_CUSTOM_URL,
					));

					if (!$langSettingModel) {
						$langSettingModel = new CoreLangWhiteLabelSetting();
						$langSettingModel->language = $lang;
						$langSettingModel->type = self::POWEREDBY_CUSTOM_URL;
					}
					$isUrlValid = false;
					if (empty($url) || $url == 'http://') {
						$url = 'http://';
						$isUrlValid = true;
					} else {
						$isUrlValid = $urlValidator->validateValue($url);
					}

					if ($isUrlValid) {
						$langSettingModel->value = $url;
						$langSettingModel->save();
					} else {
						$wrongUrls[] = $lang;
					}
				}
				if(!empty($wrongUrls))
					$this->addError('whitelabel_footer', Yii::t('standard', 'Please, enter a valid URL') . ' - ' . implode(', ', $wrongUrls));
			}
		}

		if(!empty($_POST['valuesForUrlHeader']) && is_array($_POST['valuesForUrlHeader']) && $_POST['BrandingWhiteLabelForm']['whitelabel_header'] == 'external_header') {
			$customUrlsHeader = $_POST['valuesForUrlHeader'];
			if(empty($customUrlsHeader[$defaultLanguage]) || $customUrlsHeader[$defaultLanguage] == 'http://'){
				$this->addError('whitelabel_header', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));
				return;
			}
			$wrongUrls = array();
			$urlValidator = new CUrlValidator();
			foreach ($customUrlsHeader as $lang => $item) {
				$customUrlHeaderModel = CoreLangWhiteLabelSetting::model()->findByAttributes(array(
						'language' => $lang,
						'type' => self::POWEREDBY_CUSTOM_URL_HEADER
				));

				if(!$customUrlHeaderModel) {
					$customUrlHeaderModel = new CoreLangWhiteLabelSetting();
					$customUrlHeaderModel->language = $lang;
					$customUrlHeaderModel->type = self::POWEREDBY_CUSTOM_URL_HEADER;
				}
				if(empty($item)) $item = 'http://';
				if ($item == 'http://') {
					$isUrlValid = true;
				} else {
					$isUrlValid = $urlValidator->validateValue($item);
				}

				if ($isUrlValid) {
					$customUrlHeaderModel->value = $item;
					$customUrlHeaderModel->save();
				} else {
					$wrongUrls[] = $lang;
				}
			}
			if(!empty($wrongUrls))
				$this->addError('whitelabel_header', Yii::t('standard', 'Please, enter a valid URL') . ' - ' . implode(', ', $wrongUrls));
		}
		$this->whitelabel_footer_text_per_language = Yii::app()->db->createCommand()
				->select()->from(CoreLangWhiteLabelSetting::model()->tableName())->queryAll();

		// If option is NOT set use default one
		if (empty($this->whitelabel_mobile)) {
			$this->whitelabel_mobile = self::MOBILE_DEFAULT;
		}
	}

	public function attributeLabels()
	{
		return array(
			'whitelabel_header' => Yii::t('templatemanager', 'Header'),
			'whitelabel_header_ext_url'	=> Yii::t('templatemanager', 'Header') . ' ' . Yii::t('standard', '_URL'),
			'whitelabel_footer_ext_url'	=> Yii::t('templatemanager', 'Foooter') . ' ' . Yii::t('standard', '_URL'),
			'whitelabel_header_ext_height'	=> Yii::t('templatemanager', 'Header') .' '. Yii::t('templatemanager', 'height'),
			'whitelabel_footer_ext_height'	=> Yii::t('templatemanager', 'Foooter') .' '. Yii::t('templatemanager', 'height'),
			'whitelabel_footer' => Yii::t('templatemanager', 'Foooter'),
			'whitelabel_menu' => Yii::t('adminrules', '_ADMIN_MENU'),
			'whitelabel_helpdesk' => Yii::t('standard', 'Contact us'),
			'whitelabel_naming' => Yii::t('templatemanager', 'Naming'),
			'whitelabel_naming_text' => Yii::t('configuration', '_URL'),
		);
	}

	public function beforeSave()
	{
		$this->whitelabel_menu_userid = Yii::app()->user->id;
		
		// A numeric FILE means that NO new files sent, but the Asset ID has been passed back; Do not use it of course
		if (!is_numeric($this->whitelabel_mobile_splashscreen_file)) {
			$asset = new CoreAsset();
			$asset->sourceFile = Docebo::getUploadTmpPath() . "/" . $this->whitelabel_mobile_splashscreen_file;
			$asset->scope = CoreAsset::SCOPE_PRIVATE;
			$asset->type  = CoreAsset::TYPE_GENERAL_IMAGE;

			// Defaults to nothing
			$this->whitelabel_mobile_splashscreen_file = '';
			if ($asset->save()) {
				$this->whitelabel_mobile_splashscreen_file = $asset->id;
			}
		}
		
		parent::beforeSave();
	}

	public function save(){
		$this->whitelabel_footer_text = '';
		$this->whitelabel_footer_ext_url = '';
		parent::save();
	}

	public static function isPluginActive()
	{
		static $active = null;
		if ($active !== null)
			return $active;

		$active = PluginManager::isPluginActive('WhiteLabelApp');
		return $active;
	}

	/**
	 * Return custom text (or empty string to hide the text) in case of active plugin or return NULL for default Docebo Powered widget.
	 * @return null|string
	 */
	public static function getFooterText()
	{
        if (self::isPluginActive()) {
            $whiteLabelFooter               = Settings::get('whitelabel_footer', null, false, false);
            $whiteLabelFooterText           = Settings::get('whitelabel_footer_text', null, false, false);
            $whiteLabelFooterExtHeight      = Settings::get('whitelabel_footer_ext_height', null, false, false);
            $whiteLabelFooterExtUrl         = Settings::get('whitelabel_footer_ext_url',  null, false, false);

			// get main domain settings
			$mainDomainCurrentLang = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('language=:lang', array(':lang' => $_SESSION['current_lang']))->queryAll();
			$mainDomainDefaultLang = Yii::app()->db->createCommand()
					->select()->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('language=:lang', array(':lang' => CoreLangLanguage::getDefaultLanguage()))->queryAll();
			foreach($mainDomainCurrentLang as $row){
				switch($row['type']){
					case CoreLangWhiteLabelSetting::TYPE_FOOTER_URL:
						$whiteLabelFooterExtUrl = $row['value'];
						break;
					case CoreLangWhiteLabelSetting::TYPE_FOOTER_TEXT:
						$whiteLabelFooterText = $row['value'];
						break;
				}
			}

			if($whiteLabelFooterExtUrl == 'http://' || empty($whiteLabelFooterExtUrl)) {
				// get default language value
				foreach($mainDomainDefaultLang as $def){
					switch($def['type']){
						case CoreLangWhiteLabelSetting::TYPE_FOOTER_URL:
							$whiteLabelFooterExtUrl = $def['value'];
							break;
					}
				}
			}

			if(empty($whiteLabelFooterText)){
				// get default language value
				foreach($mainDomainDefaultLang as $def){
					switch($def['type']){
						case CoreLangWhiteLabelSetting::TYPE_FOOTER_TEXT:
							$whiteLabelFooterText = $def['value'];
							break;
					}
				}
			}
			if (Settings::get('whitelabel_naming_site_enable') == 1) {
				$siteReplacement = Settings::get('whitelabel_naming_site');
				if (strpos($siteReplacement, 'http://') !== false) $siteReplacement = substr($siteReplacement, 7);
				$whiteLabelFooterText = str_replace('www.docebo.com', $siteReplacement, $whiteLabelFooterText);
			}

			// allow settings to be overridden
            Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
                'whitelabel_footer' => &$whiteLabelFooter,
                'whitelabel_footer_text' => &$whiteLabelFooterText,
                'whitelabel_footer_ext_height' => &$whiteLabelFooterExtHeight,
                'whitelabel_footer_ext_url' => &$whiteLabelFooterExtUrl
            )));

            if ($whiteLabelFooter != 'show_docebo')
            {
                if ($whiteLabelFooter == 'hide_docebo')
                    return '&nbsp;';
                else {
					return $whiteLabelFooterText;
				}
            }
        }
        return null;
	}

	/**
	 * Check if the menus should be hidden
	 * @return bool
	 */
	public static function isMenuVisible()
	{
		if (self::isPluginActive() && Settings::get('whitelabel_menu', null) == 1 && Settings::get('whitelabel_menu_userid', null) != Yii::app()->user->id && !isset(Yii::app()->request->cookies['reseller_cookie']))
			return false;
		else
			return true;
	}

	/**
	 * Check if the current help desk is performed y Docebo or the white-labeler
	 * @return bool
	 */
	public static function isHelpDeskToDocebo()
	{
        $event = new DEvent(self, array());
        Yii::app()->event->raise('IsDoceboHelpdeskRequest', $event);

        // Get info from MAIN WhiteLabel settings
        $helpDesk 		= Settings::get('whitelabel_helpdesk', null, false, false);
        $helpDeskEmail 	= Settings::get('whitelabel_helpdesk_email', null, false, false);
        
        // Raise an event to collect more info about this
        Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
        	'whitelabel_helpdesk'			=> &$helpDesk,	
        	'whitelabel_helpdesk_email'		=> &$helpDeskEmail,
        )));
        
		if ((self::isPluginActive()
			&& $helpDesk != 'to_docebo'
			&& $helpDeskEmail != ''
			&& !(Settings::get('whitelabel_menu', 0) == 0
				|| (Settings::get('whitelabel_menu', 0) == 1
					&& Yii::app()->user->getId() == Settings::get('whitelabel_menu_userid', null))
			))
            || !$event->shouldPerformAsDefault()
		)	{
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * Get the custom email in case it's available
	 * @return null|string
	 */
	public static function getHelpDeskEmail()
	{
		$event = new DEvent(self, array());
		Yii::app()->event->raise('GetCustomHelpdeskRecipient', $event);

		if($event->shouldPerformAsDefault() || !isset($event->return_value['recipient_email']) || !$event->return_value['recipient_email'])
		{
			$whitelabelHelpdesk = Settings::get('whitelabel_helpdesk', null, false, false);
			$whitelabelHelpdeskEmail = Settings::get('whitelabel_helpdesk_email', null, false, false);
			Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
				'whitelabel_helpdesk' => &$whitelabelHelpdesk,
				'whitelabel_helpdesk_email' => &$whitelabelHelpdeskEmail,
			)));
			if(self::isPluginActive()
				&& $whitelabelHelpdesk == 'to_email'
				&& $whitelabelHelpdeskEmail !== null) {
				return $whitelabelHelpdeskEmail;
			} else {
				return false;
			}
		}
		else {
			return $event->return_value['recipient_email'];
		}
	}

	public static function getSiteReplacement() {
        $label = 'www.docebo.com';
		$whiteLabelNamingSiteEnabled = Settings::get('whitelabel_naming_site_enable', '', false, false);

        if (self::isPluginActive() && $whiteLabelNamingSiteEnabled) {
			$whiteLabelNamingSiteReplacement = '';
			if($whiteLabelNamingSiteEnabled == 1){
				$whiteLabelNamingSiteReplacement = Settings::get('whitelabel_naming_site', '' , false, false);
			}

			if(Settings::get('whitelabel_footer') == CoreLangWhiteLabelSetting::TYPE_FOOTER_URL) {
				$whiteLabelNamingSite = Yii::app()->db->createCommand()
						->select('value')->from(CoreLangWhiteLabelSetting::model()->tableName())
						->where('type=:type AND language=:lang', array(
								':type' => CoreLangWhiteLabelSetting::TYPE_FOOTER_URL,
								':lang' => $_SESSION['current_lang']
						))->queryScalar();
				if (empty($whiteLabelNamingSite) || $whiteLabelNamingSite == 'http://') {
					$whiteLabelNamingSite = Yii::app()->db->createCommand()
							->select('value')->from(CoreLangWhiteLabelSetting::model()->tableName())
							->where('type=:type AND language=:lang', array(
									':type' => CoreLangWhiteLabelSetting::TYPE_FOOTER_URL,
									':lang' => CoreLangLanguage::getDefaultLanguage()
							))->queryScalar();
				}
			}else{
				$whiteLabelNamingSite = $whiteLabelNamingSiteReplacement;
			}

            // allow settings to be overidden
            Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
                'whitelabel_naming_site' => &$whiteLabelNamingSite,
                'whitelabel_naming_site_enable' => &$whiteLabelNamingSiteEnabled,
                'whiteLabelNamingSiteReplacement' => &$whiteLabelNamingSiteReplacement
            )));
			$whiteLabelNamingSite = str_replace('www.docebo.com', $whiteLabelNamingSiteReplacement, $whiteLabelNamingSite);
            if ($whiteLabelNamingSite != '')
			    $label = $whiteLabelNamingSite;
        }

		return $label;
	}

	/**
	 * Return custom app naming/label OR the default one - Docebo
	 * @return string
	 */
	public static function getLabelReplacement()
	{
		static $label = 'Docebo';

        if (self::isPluginActive())
        {
            $whiteLabelNaming       = Settings::get('whitelabel_naming',  null, false, false);
            $whiteLabelNamingText   = Settings::get('whitelabel_naming_text',   '', false, false);

            // allow settings to be overidden
            Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
                'whitelabel_naming' => &$whiteLabelNaming,
                'whitelabel_naming_text' => &$whiteLabelNamingText
            )));

            if ($whiteLabelNaming == 1)
                $label = $whiteLabelNamingText;
            else
                $label = 'Docebo';
        }

        if ($label !== null)
            return $label;
	}
}