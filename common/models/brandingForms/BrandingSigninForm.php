<?php

class BrandingSigninForm extends AdvancedSettingsBaseForm {

	protected $_params;
	protected $_languages;
	protected $_translations;

	public $login_layout;
	public $hide_signin_form;
	public $home_login_img;
	public $home_login_img_position;

	public $minimal_login_type;
	public $minimal_login_background;
	public $minimal_login_video_fallback_img;

	public function rules() {
		return array(
			array('login_layout,hide_signin_form, home_login_img, home_login_img_position, minimal_login_type, minimal_login_background, minimal_login_video_fallback_img', 'safe')
		);
	}

	public function init() {
		$fields = array(
			'login_layout',
			'hide_signin_form',
			'home_login_img',
			'home_login_img_position',
			'minimal_login_type',
			'minimal_login_background',
			'minimal_login_video_fallback_img'
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			$this->_params[$row['param_name']] = $row;
			$this->{$row['param_name']} = $row['param_value'];
		}

		if(!$this->minimal_login_type) {
			$this->minimal_login_type = 'color';
		}

		$minimal_login_background = $this->minimal_login_background;
		if(empty($this->minimal_login_background)) {
			$this->minimal_login_background = '#FFFFFF';
		}

		if(!$this->minimal_login_video_fallback_img) {
			$this->minimal_login_video_fallback_img = '';
		}

		$this->getHomepageTextTranslations();
	}

	public function attributeLabels() {
		return array(
			'login_layout' => Yii::t('templatemanager', 'Login page layout'),
			'hide_signin_form' => Yii::t('templatemanager', 'Login form'),
			'home_login_img' => Yii::t('login', '_HOMEPAGE'),
			'home_login_img_position' => Yii::t('branding', 'Sign in layout description'),
			'minimal_login_type' => Yii::t('login', '_HOMEPAGE'),
			'minimal_login_background' => '',
			'minimal_login_video_fallback_img' => Yii::t('branding', 'Fallback image')
		);
	}

	public function beforeSave() {
		if(!$this->minimal_login_type) {
			$this->minimal_login_type = 'color';
		}

		$minimal_login_background = $this->minimal_login_background;
		if(empty($this->minimal_login_background)) {
			$this->minimal_login_background = '#FFFFFF';
		}

		if(!$this->minimal_login_video_fallback_img) {
			$this->minimal_login_video_fallback_img = '';
		}

		if (!empty($_POST['CoreLangTranslation'])) {
			foreach ($_POST['CoreLangTranslation'] as $langCode => $translations) {
				foreach ($translations as $idText => $translation) {
					$t = CoreLangTranslation::model()->findByAttributes(array('lang_code' => $langCode, 'id_text' => $idText));

					if (empty($t)) {
						$t = new CoreLangTranslation;
						$t->id_text = $idText;
						$t->lang_code = $langCode;
						$t->translation_status = 'new';
					}

					$t->translation_text = $translation['translation_text'];
					$t->save_date = Yii::app()->localtime->toLocalDateTime();
					$t->save(false); //false to allow empty text, i.e. to clear the values
				}
			}
			$this->clearLangCache();
		}
	}

	protected function clearLangCache() {

		//analyze input
		$arrIdText = array();
		if (!empty($_POST['CoreLangTranslation'])) {
			foreach ($_POST['CoreLangTranslation'] as $langCode => $translations) {
				foreach ($translations as $idText => $translation) {
					if (!in_array((int)$idText, $arrIdText)) {
						$arrIdText[] = (int)$idText;
					}
				}
			}
		}

		if (empty($arrIdText)) { return true; } //no cache to upgrade

		//retrieve all input's text modules
		$categories = array();
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id_text', $arrIdText);
		$list = CoreLangText::model()->findAll($criteria);
		foreach ($list as $item) {
			if (!in_array($item->text_module, $categories)) {
				$categories[] = $item->text_module;
			}
		}

		if (empty($categories)) { return true; } //no cache to upgrade

		//clean text modules in cache
		foreach ($categories as $category) {
			Yii::app()->messages->resetCache($category);
		}
		
		return true;
	}

	public function getHomepageTextTranslations() {

		// make sure that these keys exist: _LOGIN_MAIN_TITLE and _LOGIN_MAIN_CAPTION
		$textKeyTitle = CoreLangText::model()->findByAttributes(array('text_key'=>'_LOGIN_MAIN_TITLE'));
		if (!$textKeyTitle) {
			$textKeyTitle = new CoreLangText();
			$textKeyTitle->text_key = '_LOGIN_MAIN_TITLE';
			$textKeyTitle->text_module = 'login';
			$textKeyTitle->context = 'login';
			$textKeyTitle->save();
		}
		$textKeyCaption = CoreLangText::model()->findByAttributes(array('text_key'=>'_LOGIN_MAIN_CAPTION'));
		if (!$textKeyCaption) {
			$textKeyCaption = new CoreLangText();
			$textKeyCaption->text_key = '_LOGIN_MAIN_CAPTION';
			$textKeyCaption->text_module = 'login';
			$textKeyCaption->context = 'login';
			$textKeyCaption->save();
		}

		// Sign in section
		$languages = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$languages[$lang_code] = $languages[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";

		$textKeys = array($textKeyTitle, $textKeyCaption);
		$textsIds = CHtml::listData($textKeys, 'id_text', '');
		$textsByKey = array();
		foreach ($textKeys as $text) {
			$textsByKey[$text->id_text] = $text;
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('id_text', array_keys($textsIds));
		$foundTranslations = CoreLangTranslation::model()->findAll($criteria);
		$translationsByIdText = array();

		// here we group the found translations into an array['id_text']['lang_code'] = $translation
		if (!empty($foundTranslations)) {
			foreach ($foundTranslations as $translation) {
				if (!isset($translationsByIdText[$translation->id_text]))
					$translationsByIdText[$translation->id_text] = array();
				$translationsByIdText[$translation->id_text][$translation->lang_code] = $translation;
			}
		}

		// There MIGHT be NO translations in DB at all!
		$arrTranslations = array();

		// fill in all languages translations
		foreach ($languages as $langCode => $langName) {
			foreach ($textKeys as $text) {

				// did we find the translation ?

				if (!empty($translationsByIdText[$text->id_text][$langCode])) {

					$arrTranslations[$langCode][$text->text_key] = $translationsByIdText[$text->id_text][$langCode];
				}

				else {

					$newLang = new CoreLangTranslation();
					$newLang->id_text = $text->id_text;

					$arrTranslations[$langCode][$text->text_key] = $newLang;
				}
			}

			// add * for each language in the languages dropdown to indicate that a translation exists
			$translation = $arrTranslations[$langCode]['_LOGIN_MAIN_TITLE'];
			$caption = $arrTranslations[$langCode]['_LOGIN_MAIN_CAPTION'];
			if (!empty($translation->translation_text) || !empty($caption->translation_text)) {
				$languages[$langCode] = "$langName *";
			}
		}

		$this->_languages = $languages;
		$this->_translations = $arrTranslations;
	}

	public function getLanguages() {
		return $this->_languages;
	}

	public function getTranslations() {
		return $this->_translations;
	}

	/** Create asset and save its details by data given
	 * @param $original_filename
	 * @param $path
	 * @param $form
	 * @param $saveTo
	 * @param string $assetType
	 * @param array $allowedExtensions
	 * @param bool|false $isImage
	 * @throws CDbException
	 */
	public static function createAsset($original_filename, $path, $form, $saveTo, $assetType = CoreAsset::TYPE_GENERAL_ASSET, $allowedExtensions = array(), $isImage = false) {
		if(!empty($original_filename) && !empty($path)) {
			// 1. Create new CoreAsset
			$asset = new CoreAsset();

			// 2. Set the temporary sub file details
			$asset->type = $assetType;

			// 3. Run some extension checks on the file
			$fileHelper = new CFileHelper();
			$extension = strtolower($fileHelper->getExtension($original_filename));

			// 4.1 Set the actual temp filepath
			$asset->sourceFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $path . '.' . $extension;
			$asset->original_filename = $original_filename;

			// 4.2 Rescale if it is an image
			if ($isImage) {
				self::rescaleImage($asset->sourceFile);
			}

			// 5. Save the asset
			if ($asset->save()) {
				// Fix the original filename !!!
				$asset->original_filename = $original_filename;
				$asset->save(false);

				// 6. Do we need to cleanup previously set asset here?
				$saveToValue = $form->$saveTo;
				if (!empty($saveToValue) && (strpos($saveToValue, '#') === false)) {
					// There is an already saved asset, so we need to find and remove it
					$oldAsset = CoreAsset::model()->findByPk($saveToValue);
					if ($oldAsset) { // if the old asset still exist, remove it
						$oldAsset->delete();
					}
				}

				// 7. Update the form db record via the corresponding model
				$form->$saveTo = $asset->id;

				// 8. Save the form model
				$form->save(false);
			}

			// 9. Clean the temporary file
			FileHelper::removeFile(Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $path . '.' . $extension);
		}
	}


	/**
	 * rescales an image file to given max dimensions
	 * @param $filepath
	 * @param int $maxWidth
	 * @param int $maxHeight
	 * @return bool
	 */
	public static function rescaleImage($filepath, $maxWidth = 1920, $maxHeight = 1080) {
		$imageInfo = getimagesize($filepath);

		$currentWidth   = $imageInfo[0];
		$currentHeight  = $imageInfo[1];

		// Get original image size ratio
		$ratio_orig = $currentWidth / $currentHeight;

		// Calculate the dimentions according the aspect ration and ensure it is not over the max size !!!
		if ($maxWidth / $maxHeight > $ratio_orig) {
			$newWidth = round($maxHeight * $ratio_orig);
			$maxWidth = ($maxWidth < $newWidth) ? $maxWidth : $newWidth;
		} else {
			$newHeight = round($maxWidth / $ratio_orig);
			$maxHeight = ($maxHeight < $newHeight) ? $maxHeight : $newHeight;
		}

		$convertedFilesPath = Docebo::getUploadTmpPath() ;// . DIRECTORY_SEPARATOR . 'converted';

		if ($currentHeight > $maxHeight || $currentWidth > $maxWidth) {
			/* @var $imageMagick ImageMagickConverter */
			$imageMagick = Yii::app()->imageMagick;
			$imageMagick->setSourcePath($filepath);
			$imageMagick->setDestinationPath($convertedFilesPath . DIRECTORY_SEPARATOR . basename($filepath).'_converted');
			$imageMagick->setWidth($maxWidth);
			$imageMagick->setHeight($maxHeight);
			$imageMagick->setCropAfterResize(true);
			$imageMagick->setQuality(100);
			if ($imageMagick->resize()) {
				FileHelper::removeFile($filepath);
				rename($convertedFilesPath . DIRECTORY_SEPARATOR . basename($filepath).'_converted', $filepath);
				return true;
			} else {
				return false;
			}
		}

		return true;
	}
}