<?php

class BrandingLogoForm extends AdvancedSettingsBaseForm {

	protected $_params;
	protected $_logo;

	public $page_title;
	public $company_logo;
	public $header_message_active;
	public $language;
    public $favicon;

	public function getLogo() {
		return $this->_logo;
	}

	public function setLogo($value) {
		$this->_logo = $value;
		return true;
	}

	public function onAfterSave() { }

	public function rules() {
		return array(
			array('page_title, company_logo, header_message_active, favicon', 'safe')
		);
	}

	public function init() {
		$fields = array(
			'page_title',
			'company_logo',
			'header_message_active',
            'favicon'
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			$this->_params[$row['param_name']] = $row;
			$this->{$row['param_name']} = $row['param_value'];
		}
	}


	public function attributeLabels() {
		return array(
			'page_title' => Yii::t('configuration', '_PAGE_TITLE'),
			'company_logo' => Yii::t('course', '_SPONSOR_LOGO'),
			'header_message_active' => Yii::t('templatemanager', 'Header').' <span style="text-transform:lowercase">'.Yii::t('htmlframechat', '_MSGTXT').'</span>'
		);
	}


	public function beforeValidate()
	{
		$validator = new FileTypeValidator(FileTypeValidator::TYPE_IMAGE, true);
		if (!$validator->validateFile('BrandingLogoForm[logo]'))
			$this->addError('company_logo', $validator->errorMsg);

        if (!$validator->validateFile('BrandingLogoForm[favicon]'))
            $this->addError('favicon', $validator->errorMsg);
		return parent::beforeValidate();
	}

	public function beforeSave() {
		$logo = CUploadedFile::getInstanceByName('BrandingLogoForm[logo]');
		if (!empty($logo)) {
			$asset = new CoreAsset();
			$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
			$asset->sourceFile = $logo; // YES! Assets model will detect that this is an Uploaded file and will act accordingly
			if ($asset->save()) {
				$this->company_logo = $asset->id;
			}
		}
		else
			$this->company_logo = (isset($this->_params['company_logo']['param_value']) ? $this->_params['company_logo']['param_value'] : '');

        $favicon = CUploadedFile::getInstanceByName('BrandingLogoForm[favicon]');
        if (!empty($favicon)) {
        	$asset = new CoreAsset();
        	$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
        	$asset->sourceFile = $favicon; // YES! Assets model will detect that this is an Uploaded file and will act accordingly
        	if ($asset->save()) {
        		$this->favicon = $asset->id;
        	}
        }
		else
			$this->favicon = (isset($this->_params['favicon']['param_value']) ? $this->_params['favicon']['param_value'] : '');
	}

}