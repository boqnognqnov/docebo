<?php

class BrandingLayoutColorsForm extends AdvancedSettingsBaseForm {

	protected $_params;

	public $login_layout;
	public $home_login_img;
	public $home_login_img_position;

	public function rules() {
		return array(
			array('login_layout, home_login_img, home_login_img_position', 'safe')
		);
	}

	public function init() {
		$fields = array(
			'login_layout',
			'home_login_img',
			'home_login_img_position',
		);

		$command = Yii::app()->db->createCommand()
		    ->select('*')
		    ->from('core_setting')
		    ->where(array('in', 'param_name', $fields));

		$result = $command->queryAll();

		foreach ($result as $row) {
			$this->_params[$row['param_name']] = $row;
			$this->{$row['param_name']} = $row['param_value'];
		}
	}

	public function attributeLabels() {
		return array(
			'login_layout' => Yii::t('templatemanager', 'Login page layout'),
			'home_login_img' => Yii::t('login', '_HOMEPAGE'),
			'home_login_img_position' => Yii::t('branding', 'Sign in layout description'),
		);
	}

	public function beforeSave() {

	}

}