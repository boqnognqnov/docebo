<?php

/**
 * This is the model class for table "learning_repository_object_version".
 *
 * The followings are the available columns in table 'learning_repository_object_version':
 * @property integer $id_resource
 * @property string $object_type
 * @property integer $version
 * @property integer $id_org
 * @property integer $id_object
 * @property string $version_name
 * @property string $version_description
 * @property integer $author
 * @property integer $visible
 * @property string $create_date
 * @property string $update_date
 *
 * @property LearningRepositoryObject $repositoryLO
 */
class LearningRepositoryObjectVersion extends CActiveRecord
{
	public $confirm;
	
	
	private $paramsToPush = array(
	    'enable_oauth',
	    'oauth_client',
	    'use_xapi_content_url',
	);
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_repository_object_version';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_resource, object_type, create_date', 'required'),
			array('id_resource, version, id_org, id_object, visible', 'numerical', 'integerOnly'=>true),
			array('version_name, object_type, author', 'length', 'max'=>255),
			array('version_description, update_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_resource, object_type, version, id_org, id_object, version_name, version_description, author, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'repositoryLO'=>array(self::BELONGS_TO, 'LearningRepositoryObject', 'id_object'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_resource' => 'Id Recourse',
			'object_type' => 'Object Type',
			'version' => 'Version',
			'id_org' => 'Id Org',
			'id_object' => 'Id Object',
			'version_name' => 'Version Name',
			'version_description' => 'Version Description',
			'author' => 'Author',
			'visible' => 'Visible',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_resource',$this->id_resource);
		$criteria->compare('object_type',$this->object_type);
		$criteria->compare('version',$this->version);
		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('id_object',$this->id_object);
		$criteria->compare('version_name',$this->version_name,true);
		$criteria->compare('version_description',$this->version_description,true);
		$criteria->compare('author',$this->author);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getAuthorName(){
		$userSettings = is_string($this->author) ? CJSON::decode($this->author) : $this->author;
		// If the username is provided then let's return it directly
		if(isset($userSettings['username']) && $userSettings['username'])
			return $userSettings['username'];
		// if there is no username but we have a user id let's extract the user name for the DB
		if(isset($userSettings['idUser']) && $userSettings['idUser'])
			return CoreUser::model()->findByPk($userSettings['idUser'])->getFullName();
		// If no data is provided then go OUT!!
		return null;
	}

	public function getObjectVersionsFormatted(){
		$versionsQuery = Yii::app()->db->createCommand()
			->select(array('version', 'version_name', 'create_date', 'update_date'))
			->from($this->tableName())
			->where('id_object = :object')
			->queryAll(true, array(':object' => $this->id_object));
		$versions = array();
		if(!empty($versionsQuery)){
			foreach($versionsQuery as $row){
                $row_dateTime = $row['update_date'] ? $row['update_date'] : $row['create_date'];
                $row_localDateTime = Yii::app()->localtime->toLocalDateTime($row_dateTime);
				$versions[$row['version']] = $row['version_name'] . ' - ' . ($row_localDateTime);
			}
		}

		return $versions;
	}

	/**
	 * Get certain version of LO object, if no version passed return the last one
	 *
	 * @param integer $idObject
	 * @param integer|null $version
	 *
	 * @return LearningRepositoryObjectVersion
	 */
	public static function getInstanceVersion($idObject, $version = null){
		$criteria = new CDbCriteria();
		$criteria->condition = 'id_object = :id';
		$criteria->params[":id"] = $idObject;
		if($version !== null){
			$criteria->addCondition('version = :ver');
			$criteria->params[":ver"] = $version;
		}
		$criteria->order = '`version` DESC';
		$criteria->limit= 1;
		return LearningRepositoryObjectVersion::model()->find($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningRepositoryObjectVersion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function changeVisibleStatusToPushedLO(){
		$criteria = new CDbCriteria();
		$criteria->addCondition("(idResource=:idVideo) AND (objectType='". LearningOrganization::OBJECT_TYPE_VIDEO . "')");
		$criteria->params[':idVideo'] = $this->id_resource;

		$res = LearningOrganization::model()->updateAll(array(
			'visible' => $this->visible
		), $criteria);

	}

	/**
	 * Make SQL Query and Configurate ComboListView
	 * @return \CSqlDataProvider
	 */
	public function sqlDataProvider($customConfig = array()) {
		$loRepositoryId = Yii::app()->request->getParam('id', '');

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(self::model()->tableName() . ' vr');
		$commandBase->andWhere('vr.id_object = :idObject', array(':idObject' => $loRepositoryId));

		$commandData = clone $commandBase;
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(*)');
		$numRecords = $commandCounter->queryScalar();

		$commandData->order = "vr.id_resource DESC";

		$pageSize = Settings::get('elements_per_page', 10);

		if (!empty($customConfig['elements_per_page'])) {
			$pageSize = $customConfig['elements_per_page'];
		}

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id_resource',
		);

		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}

	public function afterDelete(){
		$lro = LearningRepositoryObject::model()->findByPk($this->id_object);
		$learningOrganizations = LearningOrganization::model()->findAllByAttributes(array('idResource'=>$this->id_resource, 'objectType'=>$this->object_type));

		if (count($lro->versions) == 0)
			$lro->delete();

		foreach ($learningOrganizations as $lo) {
			$lo->delete();
		}

		parent::afterDelete();
	}

	/**
	 * Returns a list of idOrgs of all LO placeholders for the current shared object version.
	 *
	 * @param integer | null $idUser If provided, considers only the courses where this user is enrolled in
	 * @param array $excludeList If provided exclude one or more objects from the search
	 *
	 * @return array The array of idOrgs
	 */
	public function getCoursePlaceholdersForThisObject($idUser = null, $excludeList = array()) {
		$cmd = Yii::app()->db->createCommand()
			->select("idOrg")
			->from(LearningOrganization::model()->tableName()." lo")
			->where("id_object = :idObject", array(':idObject' => $this->id_object))
			->andWhere("idResource = :idResource", array(':idResource' => $this->id_resource));

		if($idUser)
			$cmd->join(LearningCourseuser::model()->tableName().' lcu', 'lcu.idCourse=lo.idCourse AND lcu.idUser=:idUser', array(':idUser' => $idUser));

		if(!empty($excludeList))
			$cmd->andWhere("idOrg NOT IN (".implode(",", $excludeList).")");

		return $cmd->queryColumn();
	}

	/**
	 * Implements the "pushing" logic of a Central LO object version inside a course
	 * @param $idCourse The course to push to
	 *
	 * @return integer The idOrg of the object placeholder
	 */
	public function pushToCourse($idCourse, $parentId = false) {

		// 1. Check that we don't have already the same object pushed in this course
		$idExstingLo = Yii::app()->db->createCommand()
			->select("idOrg")
			->from(LearningOrganization::model()->tableName())
			->where("id_object = :idObject", array('idObject' => $this->id_object))
			->andWhere("idCourse = :idCourse", array('idCourse' => $idCourse))
			->queryScalar();
		if($idExstingLo) {
			Yii::log("Trying to push the same central LO object (ID = ".$this->id_object.") two times in the same course (ID = ".$idCourse.")", CLogger::LEVEL_INFO);
			return 0;
		}

		// 2. Load root organization node
		$parent = LearningObjectsManager::getRootNode($idCourse);

		if($parentId){
			$loParent = LearningOrganization::model()->findByPk($parentId);
			if($loParent && $loParent->isFolder()){
				$parent = $loParent;
			}
		}

		// 3. insert new object in database
		$organization = new LearningOrganization();
		// Get and set visible status from DB (for videos only)
		if($this->object_type == "video") {
			$organization->visible = $this->visible;
		} else {
			$organization->visible = 1;
		}

		$shortDescription = $this->repositoryLO->short_description;
		if (strlen($shortDescription) > 200) {
			$shortDescription = substr($shortDescription, 0, 197) . '...';
		}

		$organization->title = $this->repositoryLO->title;
		$organization->objectType = $this->object_type;
		$organization->idResource = $this->id_resource;
		$organization->idAuthor = CJSON::decode($this->author)['idUser'];
		$organization->dateInsert = Yii::app()->localtime->toLocalDateTime();
		$organization->idCourse = $idCourse;
		$organization->resource = $this->repositoryLO->resource;
		$organization->short_description = $shortDescription;
		$organization->keep_updated = false; // We keep automatic update disabled as it's too dangerous
		$organization->id_object = $this->id_object;
		
		//Do not fill the params_json, so we can overwrite it late when edit a pushed Central LO object
		$organization->params_json_inherited = $this->repositoryLO->params_json;
		// However, we introduced more LO params, saved params_json, which must be pushed (TinCan related)
		$this->setSpecialParams($organization);
		
		$organization->from_csp = $this->repositoryLO->from_csp;
		$organization->saveViewMode = false;
		$organization->appendTo($parent);
		if($organization->idOrg)
			CommonTracker::createSlaveTracksForPushedObject($this, $organization, $idCourse);
		else
			throw new CHttpException("Pushing of LO ".$this->id_object." to course ".$idCourse." failed due to the following error: ".CHtml::errorSummary($organization).")");

		return $organization->idOrg;
	}

	/**
	 * Implements the "pushing new version" logic of a Central LO object inside a course
	 * @param $idCourse The course to push to
	 *
	 * @return integer The idOrg of the object placeholder
	 */
	public function updateVersionToCourse($idCourse) {
		$loObject  = LearningOrganization::model()->findByAttributes(array("id_object" => $this->id_object, "idCourse" => $idCourse));
		$versionStatus = $this->findByAttributes(array("id_object" => $this->id_object, "id_resource" => $this->id_resource, "object_type" => "video"))->visible;
		if($loObject) {
		    
		    $this->setSpecialParams($loObject);
		    
			//Get the OLD id Resource
			$oldIdResource = $loObject->idResource;
			$loObject->idResource = $this->id_resource;
			if($versionStatus && $this->object_type == "video") {
				$loObject->visible = $versionStatus;
			}
			// If the update was Successful, NOW we must remove the History FOR All users in that Course
			if ( $loObject->saveNode() ){
				CommonTracker::updateTracksAfterVersionSwitch($oldIdResource, $loObject);
			}
			return $loObject->idOrg;
		}
	}

	public static function numberOfCoursesVersionIsUsedIn($idResource, $objType){
		return Yii::app()->db->createCommand()
			->select("COUNT(1)")
			->from(LearningOrganization::model()->tableName())
			->where(array("AND", "idResource = :resource", "objectType = :type"))
			->queryScalar(array(":resource" => $idResource, ":type" => $objType));
	}

	// Get status of current version (for videos only)
	public static function getVersionStatus($idResource, $idObject) {
		return $versionStatus = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_resource' => $idResource, 'id_object' => $idObject, 'object_type' => "video"))->visible;
	}

	/**
	 * Set SOME parameter (@see paramsToPush) from Repo params_json to passed LearningOrganization object
	 * 
	 * @param LearningOrganization $loObject 
	 * 
	 * @return LearningOrganization
	 */
	private function setSpecialParams($loObject) {
	    $repoParams    = json_decode($this->repositoryLO->params_json, true);
	    foreach ($this->paramsToPush as $p) {
	        if (isset($repoParams[$p])) {
	            $loObject->$p = $repoParams[$p];
	        }
	    }
	    return $loObject;
	}
	
}
