<?php

/**
 * This is the model class for table "core_newsletter".
 * The followings are the available columns in table 'core_newsletter':
 * @property integer $id
 * @property integer $id_send
 * @property string $sub
 * @property string $msg
 * @property string $fromemail
 * @property string $language
 * @property integer $tot
 * @property string $send_type
 * @property string $stime
 * @property string $file
 *
 * @property CoreNewsletterSendto[] $recipients
 */
class CoreNewsletter extends CActiveRecord {

	const SEND_TYPE_EMAIL = 'email';
	const SEND_TYPE_INBOX = 'inbox_only';
	const SEND_TYPE_INBOX_AND_EMAIL = 'inbox_and_email';
	const SEND_TYPE_SMS = 'sms';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreNewsletter the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_newsletter';
	}

	public function init() {
		$this->fromemail = Yii::app()->user->email;
		$this->language = '-any-';
		$this->send_type = 'email';
		$this->stime = Yii::app()->localtime->toLocalDateTime();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('msg, fromemail, sub', 'required'),
			array('fromemail', 'email'),
			array('fromemail', 'validEmail'),
			array('id_send, tot', 'numerical', 'integerOnly' => true),
			array('tot', 'numerical', 'min' => 1, 'tooSmall' => Yii::t('newsletter', 'Please specify at least one recipient')),
			array('sub, fromemail, language', 'length', 'max' => 255),
			array('send_type', 'length', 'max' => 50),
			array('stime', 'safe'),
			array('fromemail, sub, msg, file', 'safe', 'on' => 'create'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_send, sub, msg, fromemail, language, tot, send_type, stime, file', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'recipients' => array(self::HAS_MANY, 'CoreNewsletterSendto', 'id_send', 'joinType' => 'INNER JOIN', 'together' => true),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('stime medium'),
			 'dateAttributes' => array()
            )
        );
    }


	public function validEmail($attribute, $params) {

		if($this->fromemail &&  (strpos($this->fromemail, 'docebosaas') !== false))
		{
			$this->addError('fromemail', Yii::t('notification', 'Please provide valid sender email'));
			return false;
		}
		return true;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'Id',
			'id_send' => 'Id send',
			'sub' => Yii::t('standard', '_TITLE'),
			'msg' => Yii::t('standard', '_TEXTOF'),
			'fromemail' => Yii::t('standard', '_FROM'),
			'language' => Yii::t('standard', '_LANGUAGE'),
			'tot' => Yii::t('standard', '_RECIPIENTS'),
			'send_type' => Yii::t('standard', '_TYPE'),
			'stime' => Yii::t('report', '_TOTAL_SESSION'),
			'file' => Yii::t('standard', '_FILE'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('id_send', $this->id_send);
		$criteria->compare('sub', $this->sub, true);
		$criteria->compare('msg', $this->msg, true);
		$criteria->compare('fromemail', $this->fromemail, true);
		$criteria->compare('language', $this->language, true);
		$criteria->compare('tot', $this->tot);
		$criteria->compare('send_type', $this->send_type, true);
		$criteria->compare('stime', Yii::app()->localtime->fromLocalDateTime($this->stime), true);
		$criteria->compare('file', $this->file, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function renderFile() {
		$html = '';

		if (!empty($this->file)) {
			foreach ($this->file as $i => $item) {
				$html .= '<div id="'.$i.'" class="file">'.
					'<span class="file-icon"></span>'.
					'<span class="filename">'.$item['name'].'</span>'.
					'<span class="filesize">'.$item['size'].'</span>'.
					'<div class="progress"><div></div></div>'.
					'<input type="hidden" name="CoreNewsletter[file]['.$i.'][name]" value="'.$item['name'].'"/>'.
					'<input type="hidden" class="uploaded-file-size" name="CoreNewsletter[file]['.$i.'][size]" value="'.$item['size'].'"/>'.
					'<span class="close">&times;</span></div>';
			}
		}

		return $html;
	}


	public function beforeSave() {
		$files = array();
		if ($this->file == '') {
			$this->file = array();
		}
		if (is_array($this->file)) {
			foreach ($this->file as $file) {
				$files[] = $file['name'];
			}
			$this->file = CJSON::encode($files);
		}
		return parent::beforeSave();
	}
	/*public function afterSave() {
		if ($this->id_send !== $this->id) {
			$this->id_send = $this->id;
			$this->save();
		}
	}*/
}
