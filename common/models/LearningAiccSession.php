<?php

/**
 * This is the model class for table "learning_aicc_session".
 *
 * The followings are the available columns in table 'learning_aicc_session':
 * @property integer $id
 * @property integer $idUser
 * @property integer $idItem
 * @property string $hapcSession
 * @property string $timecreated
 * @property string $timemodified
 * @property string $mode
 * @property string $status
 * @property string $lesson_status
 * @property string $session_time
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property LearningAiccItem $item
 */
class LearningAiccSession extends CActiveRecord
{
	
	/**
	 * Session timeout
	 * 
	 * @var integer
	 */
	const TIMEOUT = 3000;  // Seconds
	
	
	/**
	 * Session status
	 * 
	 * @var string
	 */	
	const STATUS_NOTINIT 		= 'notinitialized';
	const STATUS_RUNNING 		= 'running';
	const STATUS_TERMINATED 	= 'terminated';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_aicc_session';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idItem, hapcSession, timecreated', 'required'),
			array('idUser, idItem', 'numerical', 'integerOnly'=>true),
			array('hapcSession, mode, status, lesson_status, session_time', 'length', 'max'=>128),
			array('timemodified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, idItem, hapcSession, timecreated, timemodified, mode, status, lesson_status, session_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' 	=> array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'item' 	=> array(self::BELONGS_TO, 'LearningAiccItem', 'idItem'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUser' => 'Id User',
			'idItem' => 'Id Item',
			'hapcSession' => 'Hapc Session',
			'timecreated' => 'Timecreated',
			'timemodified' => 'Timemodified',
			'mode' => 'Mode',
			'status' => 'Status',
			'lesson_status' => 'Lesson Status',
			'session_time' => 'Session Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idItem',$this->idItem);
		$criteria->compare('hapcSession',$this->hapcSession,true);
		$criteria->compare('timecreated',$this->timecreated,true);
		$criteria->compare('timemodified',$this->timemodified,true);
		$criteria->compare('mode',$this->mode,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('lesson_status',$this->lesson_status,true);
		$criteria->compare('session_time',$this->session_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningAiccSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 * Get (create) a new session for given AICC Item (AU) and current USER
	 * @param unknown $id
	 * @return string
	 */
	public static function getSession($idItem, $mode=LearningAiccItemTrack::LESSON_MODE_NORMAL, $idOrg = null) {
		
		$model = new self();
		
		$model->idUser 			= Yii::app()->user->id;
		$model->idItem 			= $idItem;
		$model->hapcSession 	= Docebo::randomHash();
		$model->timecreated 	= Yii::app()->localtime->getUTCNow();
		$model->timemodified 	= $model->timecreated;
		$model->status 			= self::STATUS_NOTINIT;
		$model->mode 			= $mode;

		// This will also internally pre-create item tracking records for the user for ALL package items (if they do not exists)
		$itemTrack 			= LearningAiccItemTrack::getTrack($model->idUser, $idItem, $idOrg);
		$packageTrack 		= LearningAiccPackageTrack::getTrack($model->idUser, $itemTrack->item->id_package);
		if ($itemTrack) {
			$model->lesson_status = $track->lesson_status;
		}

		
		if (!$model->save()) {
			Yii::log('Could not create AICC HAPC session. Should NOT happen, ever!', CLogger::LEVEL_ERROR);
			Yii::log(var_export($model->errors, true), CLogger::LEVEL_ERROR);
			return false;
		} 
		return $model->hapcSession;
	}

	

	/**
	 * Check if a given session id is still valid one (timeout maybe?).
	 * Upon success, return the session model itself
	 * 
	 * @param string $hapcSession
	 * @return boolean|LearningAiccSession
	 */
	public static function checkSession($hapcSession) {
		
		/* @var $model LearningAiccSession  */
		
		$nowUtc = Yii::app()->localtime->getUTCNow('Y:m:d H:i:s') . " UTC";  // Make sure we have UTC at the end!
		$nowSeconds = strtotime($nowUtc);
		$model = self::model()->find("hapcSession=:session", array(
			':session'		=> $hapcSession,
		));
		
		if (!$model) {
			return false;
		}
		return $model;
		
	}
	
	
}
