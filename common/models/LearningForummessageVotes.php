<?php

/**
 * This is the model class for table "learning_forummessage_votes".
 *
 * The followings are the available columns in table 'learning_forummessage_votes':
 * @property integer $id_message
 * @property integer $id_user
 * @property string $type
 * @property integer $value
 *
 * The followings are the available model relations:
 * @property LearningForummessage $learningForummessage
 * @property CoreUser $coreUser
 */
class LearningForummessageVotes extends CActiveRecord
{

	//voting types
	const TYPE_TONE = 'tone';
	const TYPE_HELPFUL = 'helpful';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_forummessage_votes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_message, id_user, type', 'required'),
			array('id_message, id_user, value', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_message, id_user, type, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningForummessage' => array(self::BELONGS_TO, 'LearningForummessage', 'id_message'),
			'coreUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_message' => 'Id Message',
			'id_user' => 'Id User',
			'type' => 'Type',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_message',$this->id_message);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningForummessageVotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * (non-PHPdoc)
     * @see CActiveRecord::afterSave()
     */
    public function afterSave() {
        parent::afterSave();
        if ($this->isNewRecord) {
            Yii::app()->event->raise(($this->type == 'tone') ? 'NewToneFeedback' : 'NewHelpfulFeedback', new DEvent($this, array(
                'evaluator_user' => $this->coreUser,
                'evaluated_user' => $this->learningForummessage->msgUser,
                'value' => $this->value,
                'target_type' => 'forum_reply',
                'target_element' => $this->learningForummessage
            )));
        }
    }
}
