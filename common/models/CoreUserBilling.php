<?php

/**
 * This is the model class for table "core_user_billing".
 *
 * The followings are the available columns in table 'core_user_billing':
 * @property string $id
 * @property integer $idst
 * @property string $userid
 * @property string $bill_address1
 * @property string $bill_address2
 * @property string $bill_city
 * @property string $bill_state
 * @property string $bill_zip
 * @property string $bill_country_code
 * @property string $bill_company_name
 * @property string $bill_vat_number
 * @property string $bill_stored
 * @property string $bill_updated
 * @property integer $bill_is_current
 * @property string $sha1_sum
 *
 * The followings are the available model relations:
 * @property EcommerceTransaction[] $transactions
 * @property CoreUser $user
 */
class CoreUserBilling extends CActiveRecord
{

    private $_sha1FieldNames = array(
            'bill_company_name',
            'bill_vat_number',
            'bill_address1',
            'bill_address2',
            'bill_city',
            'bill_state',
            'bill_zip',
            'bill_country_code',
            'idst',
    );



	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUserBilling the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user_billing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('idst, userid, bill_address1, bill_address2, bill_city, bill_state, bill_zip, bill_country_code, bill_stored, sha1_sum', 'required'),
		    //array('idst, bill_address1, bill_address2, bill_city, bill_zip, bill_country_code, bill_stored, sha1_sum', 'required'),

			array('idst, bill_is_current', 'numerical', 'integerOnly'=>true),
			array('userid, bill_address1, bill_address2, sha1_sum', 'length', 'max'=>255),
			array('bill_city, bill_company_name', 'length', 'max'=>128),
			array('bill_state, bill_vat_number', 'length', 'max'=>64),
			array('bill_zip', 'length', 'max'=>32),
			array('bill_country_code', 'length', 'max'=>16),
			array('bill_updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idst, userid, bill_address1, bill_address2, bill_city, bill_state, bill_zip, bill_country_code, bill_company_name, bill_vat_number, bill_stored, bill_updated, bill_is_current, sha1_sum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactions' => array(self::HAS_MANY, 'EcommerceTransaction', 'billing_info_id'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idst'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('bill_stored medium', 'bill_updated medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idst' => 'Idst',
			'userid' => 'Userid',
			'bill_address1' => 'Bill Address1',
			'bill_address2' => 'Bill Address2',
			'bill_city' => 'Bill City',
			'bill_state' => 'Bill State',
			'bill_zip' => 'Bill Zip',
			'bill_country_code' => 'Bill Country Code',
			'bill_company_name' => 'Bill Company Name',
			'bill_vat_number' => 'Bill Vat Number',
			'bill_stored' => 'Bill Stored',
			'bill_updated' => 'Bill Updated',
			'bill_is_current' => 'Bill Is Current',
			'sha1_sum' => 'Sha1 Sum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idst',$this->idst);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('bill_address1',$this->bill_address1,true);
		$criteria->compare('bill_address2',$this->bill_address2,true);
		$criteria->compare('bill_city',$this->bill_city,true);
		$criteria->compare('bill_state',$this->bill_state,true);
		$criteria->compare('bill_zip',$this->bill_zip,true);
		$criteria->compare('bill_country_code',$this->bill_country_code,true);
		$criteria->compare('bill_company_name',$this->bill_company_name,true);
		$criteria->compare('bill_vat_number',$this->bill_vat_number,true);
		$criteria->compare('bill_stored',Yii::app()->localtime->fromLocalDateTime($this->bill_stored),true);
		$criteria->compare('bill_updated',Yii::app()->localtime->fromLocalDateTime($this->bill_updated),true);
		$criteria->compare('bill_is_current',$this->bill_is_current);
		$criteria->compare('sha1_sum',$this->sha1_sum,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	/**
	 * Return the LAST (by id) billing info model for a give user
	 *
	 * @param int $idUser
	 * @return CoreUserBilling
	 */
	public function getUserLastBillingInfo($idUser) {

	    if (!$idUser) {
	        return null;
	    }

	    $criteria = new CDbCriteria();
	    $criteria->order = "id DESC";
	    $criteria->compare('idst', $idUser);

	    // Get the first model
	    $model = $this->find($criteria);

        return $model;
	}


	/**
	 * Calculate SHA1 of selected attributes. Should be saved in database table
	 * and later used to find out if all data are the same.
	 */
	public function sha1() {
	    $serial_str = "";
	    foreach ($this->_sha1FieldNames as $fieldName) {
	        $serial_str .= addslashes($this->$fieldName);
	    }

	    $sha1_sum = sha1($serial_str);
	    return $sha1_sum;
	}




}