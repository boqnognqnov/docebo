<?php

/**
 * This is the model class for table "learning_conversion".
 *
 * The followings are the available columns in table 'learning_conversion':
 * @property integer $track_id
 * @property integer $authoring_id
 * @property integer $user_id
 * @property string $completion_status
 * @property string $suspend_data
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property LearningAuthoring $authoring
 */
class LearningConversion extends CActiveRecord
{
	const STATUS_INCOMPLETE = 'incomplete';
	const STATUS_COMPLETED = 'completed';

	public $idCourse = null;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningConversion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_conversion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('authoring_id, user_id', 'required'),
			array('authoring_id, user_id', 'numerical', 'integerOnly'=>true),
			array('completion_status', 'length', 'max'=>20),
			array('suspend_data', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('track_id, authoring_id, user_id, completion_status, suspend_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'user_id'),
			'authoring' => array(self::BELONGS_TO, 'LearningAuthoring', 'authoring_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'track_id' => 'Track',
			'authoring_id' => 'Authoring',
			'user_id' => 'User',
			'completion_status' => 'Completion Status',
			'suspend_data' => 'Suspend Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('track_id',$this->track_id);
		$criteria->compare('authoring_id',$this->authoring_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('completion_status',$this->completion_status,true);
		$criteria->compare('suspend_data',$this->suspend_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	
	public function behaviors() {
		return array(
			'trackBehavior' => array(
				'class' => 'qsExt.local.tracking.AuthoringTrackBehavior',
				'idCoursePropertyName' => 'idCourse',
			),
		);
	}

	public function init()
	{
		if ($this->isNewRecord)
		{
			$this->completion_status = self::STATUS_INCOMPLETE;
		}
	}
}