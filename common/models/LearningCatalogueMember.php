<?php

/**
 * This is the model class for table "learning_catalogue_member".
 *
 * The followings are the available columns in table 'learning_catalogue_member':
 * @property integer $idCatalogue
 * @property integer $idst_member
 *
 * The followings are the available model relations:
 * @property LearningCatalogue $catalog
 */
class LearningCatalogueMember extends CActiveRecord {

	public $search_input;

	public $confirm;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatalogue, idst_member', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCatalogue, idst_member, search_input', 'safe', 'on' => 'search'),

		);
	}


	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}


	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catalog' => array(self::BELONGS_TO, 'LearningCatalogue', 'idCatalogue'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idst_member'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idCatalogue' => 'Id Catalogue',
			'idst_member' => 'Idst Member',
		);
	}


	public function dataProvider() {
		$criteria = new CDbCriteria;

		$criteria->compare('idCatalogue', $this->idCatalogue);
		$criteria->compare('idst_member', $this->idst_member);
		//$criteria->compare('t.idst', $this->idst_member, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	/**
	 * @param $id_catalogue
	 * @return CArrayDataProvider
	 */
	public function assignedDataProvider($id_catalogue) {

		$items = $this->getCatalogMembers($id_catalogue, $this->getMemberTypes(), true);
		return new CArrayDataProvider($items, array(
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	/**
	 * Retrieves members of a catalog, with type(s) specified.
	 *
	 * Note: this method does NOT return list of USERS!  It returns entities assigned to the
	 * Catalog and it could be list of Users, Groups, Chart oc_* groups or ocd_* groups!
	 *
	 * @param numeric $id_catalogue
	 * @param string|array $types requested members types
	 * @return array the list of retrieved members
	 */
	public function getCatalogMembers($id_catalogue, $types, $pu_filter = false) {

		$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$output = array();
		foreach ($types as $type) {
			switch($type) {
				case 'user':
				{
					if($this->search_input){
						$condition = 'at.idCatalogue = :id_catalogue AND (CONCAT(u.firstname, " ", u.lastname) like :search OR u.email LIKE :search OR u.userid LIKE :search)';
						$conditionArray = array(':id_catalogue' => $id_catalogue, ':search' => "%$this->search_input%");
					}else{
						$condition = 'at.idCatalogue = :id_catalogue';
						$conditionArray = array(':id_catalogue' => $id_catalogue);
					}

					$command = Yii::app()->db->createCommand()
						->select("at.idst_member, u.userid AS name")
						->from(self::model()->tableName() . " at")
						->join(CoreUser::model()->tableName() . " u", "at.idst_member = u.idst")
						->where($condition, $conditionArray)
						->order("u.userid ASC");

					if(Yii::app()->user->getIsPu() && $pu_filter)
						$command->join(CoreUserPU::model()->tableName() . ' pu', 'u.idst=pu.user_id AND puser_id='.(int)Yii::app()->user->id);

					$list = $command->queryAll();

					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst_member'], // User idst
								'id' => (int)$item['idst_member'], // User idst  (yes, the same; for the sake of normalized output)
								'name' => $item['name'],
								'type' => 'user',
								'id_catalog' => $id_catalogue //it may look unnecessary, but it's required in some view rendering
							);
						}
						$output = array_merge($output, $typedList);
					}
				}
					break;
				case 'group':
				{
					$command = Yii::app()->db->createCommand()
						->select("at.idst_member, g.idst as idstGroup, g.groupid AS name")
						->from(self::model()->tableName() . " at")
						->join(CoreGroup::model()->tableName() . " g", "at.idst_member = g.idst AND g.hidden = :hidden", array(':hidden' => 'false'))
						->where("at.idCatalogue = :id_catalogue", array(':id_catalogue' => $id_catalogue));

					if(Yii::app()->user->getIsPu() && $pu_filter)
						$command->join(CoreUserPU::model()->tableName() . ' pu', 'g.idst=pu.user_id AND puser_id='.(int)Yii::app()->user->id);

					$list = $command->queryAll();

					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst_member'], // Group idst
								'id' => (int)$item['idstGroup'], // Group idst (yes, the same; for the sake of normalized output)
								'name' => $item['name'],
								'type' => 'group'
							);
						}
						$output = array_merge($output, $typedList);
					}
				}
					break;
				case 'orgchart':
				{
					$command = Yii::app()->db->createCommand()
						->select("at.idst_member, oct.idOrg as idOrg, oc.translation AS name")
						->from(self::model()->tableName() . " at")
						->join(CoreOrgChartTree::model()->tableName() . " oct", "at.idst_member = oct.idst_oc")
						->join(CoreOrgChart::model()->tableName() . " oc", "oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code", array(':lang_code' => $language))
						->where("at.idCatalogue = :id_catalogue", array(':id_catalogue' => $id_catalogue));

					if(Yii::app()->user->getIsPu() && $pu_filter)
						$command->join(CoreUserPU::model()->tableName() . ' pu', 'at.idst_member=pu.user_id AND puser_id='.(int)Yii::app()->user->id);

					$list = $command->queryAll();

					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst_member'], // Org Chart oc_ Group
								'id' => (int)$item['idOrg'], // Org Chart ID
								'name' => $item['name'],
								'type' => 'orgchart'
							);
						}
						$output = array_merge($output, $typedList);
					}
				}
					break;
				case 'orgchart_desc':
				{
					$command = Yii::app()->db->createCommand()
						->select("at.idst_member, oct.idOrg as idOrg, oc.translation AS name")
						->from(self::model()->tableName() . " at")
						->join(CoreOrgChartTree::model()->tableName() . " oct", "at.idst_member = oct.idst_ocd")
						->join(CoreOrgChart::model()->tableName() . " oc", "oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code", array(':lang_code' => $language))
						->where("at.idCatalogue = :id_catalogue", array(':id_catalogue' => $id_catalogue));


					if(Yii::app()->user->getIsPu() && $pu_filter)
						$command->join(CoreUserPU::model()->tableName() . ' pu', 'at.idst_member=pu.user_id AND puser_id='.(int)Yii::app()->user->id);

					$list = $command->queryAll();

					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst_member'], // Org Chart ocd_ Group
								'id' => (int)$item['idOrg'], // Org Chart ID
								'name' => $item['name'],
								'type' => 'orgchart_desc'
							);
						}
						$output = array_merge($output, $typedList);
					}
				}
					break;
			}
		}

		return $output;
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_catalogue_member';
	}


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCatalogueMember the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}


	/**
	 * Just the types packed in an array
	 * @return type
	 */
	public function getMemberTypes() {
		return array(
			'user',
			'group',
			'orgchart',
			'orgchart_desc'
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idCatalogue', $this->idCatalogue);
		$criteria->compare('idst_member', $this->idst_member);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	
	public function afterSave() {
	    // Just created new record? 
	    if ($this->isNewRecord) {
	       Yii::app()->event->raise(EventManager::EVENT_MEMBER_ASSIGNED_TO_CATALOG, new DEvent($this, array(
	           'model' => $this,
	       )));
	    }
	    parent::afterSave();
	}
	
	public function afterDelete() {
	    Yii::app()->event->raise(EventManager::EVENT_MEMBER_UNASSIGNED_FROM_CATALOG, new DEvent($this, array(
	        'model' => $this,
	    )));
	    parent::afterDelete();
	}

	
	/**
	 * Return list of all Users (IDs) of THIS Catalog member (which could be a single User,  a Group , a Branch)
	 * @return array Array of integers
	 */
	public function getMemberUsers() {
	    $id = $this->idst_member;
	    $users = array();
	
	    // Is it a user?
	    if (CoreUser::model()->exists('idst=:idUser', array(':idUser' => $id))) {
	        $users[] = $id;
	    }
	    // Or a Group?
	    else if (CoreGroup::isGroup($id)) {
	        $users = CoreGroup::model()->findByPk($id)->getUsers();
	    }
	    // Or an Orgchart group (oc)
	    else if (CoreGroup::isOrgchartNode($id)) {
	        $node = CoreOrgChartTree::model()->findByAttributes(array('idst_oc' => $id));
	        if ($node) {
	            $users = $node->getNodeUsers();
	        }
	    }
	    // Or an Orgchart group with descendants (ocd)
	    else if (CoreGroup::isOrgchartFolder($id)) {
	        $node = CoreOrgChartTree::model()->findByAttributes(array('idst_ocd' => $id));
	        if ($node) {
	            $users = $node->getBranchUsers();
	        }
	    }
	
	    return $users;
	
	}
	

}