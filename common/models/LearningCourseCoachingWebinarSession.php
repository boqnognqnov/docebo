<?php

/**
 * This is the model class for table "learning_course_coaching_webinar_session".
 *
 * The followings are the available columns in table 'learning_course_coaching_webinar_session':
 * @property integer $id_session
 * @property integer $id_coaching_session
 * @property integer $id_tool_account
 * @property string $name
 * @property string $date_begin
 * @property string $date_end
 * @property string $tool
 * @property string $custom_url
 * @property string $tool_params
 *
 * The followings are the available model relations:
 * @property LearningCourseCoachingSession $idCoachingSession
 * @property WebinarToolAccount $idToolAccount
 * @property LearningCourseCoachingWebinarSessionUser[] $learningCourseCoachingWebinarSessionUsers
 */
class LearningCourseCoachingWebinarSession extends CActiveRecord
{
    //temp values from the form before converting it to "date_begin" and "date_end"
    public $date;
    public $start;
    public $duration;

    public $dateEnd;
    public $end;
    public $tool;
    public $max_enroll;
    const TOOL_CUSTOM = 'custom';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_coaching_webinar_session';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('max_enroll, date, start, duration, tool, name', 'required', 'on' => 'create, update'),
			array('id_coaching_session', 'required'),
			array('id_coaching_session, id_tool_account,max_enroll', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>45),
            array('tool', 'length', 'max'=>255),
            array('custom_url', 'length', 'max'=>1024),
            array('tool', 'validateTool', 'on' => 'create, update'),
            array('tool', 'validateRoom', 'on' => 'create, update'),
            array('date', 'validateDate', 'on' => 'create, update'),
			array('date_end, tool_params, date, start, duration,', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, id_coaching_session, id_tool_account, name, date_begin, date_end, tool, custom_url, tool_params', 'safe', 'on'=>'search'),
		);
	}
    /**
     * Custom validator for Tool input fields
     */
    public function validateTool()
    {
        if ($this->tool != LearningCourseCoachingWebinarSession::TOOL_CUSTOM)
        {
            if (!$this->id_tool_account)
                $this->addError('id_tool_account', Yii::t('webinar', 'Please select webinar account'));
        }
        else if ($this->tool == LearningCourseCoachingWebinarSession::TOOL_CUSTOM)
        {
            if (!$this->custom_url)
                $this->addError('custom_url', Yii::t('webinar', 'Please add custom webinar URl'));
        }
    }

    /**
     * Check if the session input data is suitable for creating/updating a room
     */
    public function validateRoom()
    {
        $utcNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
        if ($this->isNewRecord)
        {
            if ($this->tool) { //there is separate validation for this, but before it's selection there is no need to validate room anyway
                if (!ConferenceManager::canCreateNewRoom($this->course_id, $this->tool, $utcNow)) {
                    $this->addError('tool', Yii::t('conference', 'Max number of active rooms has been reached.'));
                }
            }
        }
    }

    /**
     * Check if the date selected is not in the past
     */
    public function validateDate()
    {
        if ($this->date && $this->start)
        {
            $timezone = Yii::app()->localtime->getTimeZone();
            $nowDate = new DateTime();
            $formDate = Yii::app()->localtime-> fromLocalDate($this->date).' '.$this->start;
            $formDateTime = new DateTime($formDate, new DateTimeZone($timezone)); //set the input date in the user's specific timezone

            //then compare the two DT objects
            if ($nowDate > $formDateTime)
                $this->addError('date', Yii::t('standard', 'Invalid starting date or time.'));
        }
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCoachingSession' => array(self::BELONGS_TO, 'LearningCourseCoachingSession', 'id_coaching_session'),
			'idToolAccount' => array(self::BELONGS_TO, 'WebinarToolAccount', 'id_tool_account'),
			'learningCourseCoachingWebinarSessionUsers' => array(self::HAS_MANY, 'LearningCourseCoachingWebinarSessionUser', 'id_session'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                'timestampAttributes' => array(),
                'dateAttributes' => array('date_begin', 'date_end')
            )
        );
    }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'date' => Yii::t('standard', '_DATE'),
            'start' => Yii::t('standard', '_START'),
            'duration' => Yii::t('statistic', '_TIME_IN'),
			'id_session' => 'Id Session',
			'id_coaching_session' => 'Id Coaching Session',
			'id_tool_account' => Yii::t('webinar', 'Tool account'),
			'name' => Yii::t('standard', '_NAME'),
			'date_begin' => 'Date Begin',
			'date_end' => 'Date End',
			'tool' => Yii::t('webinar', 'Tool'),
            'custom_url' => Yii::t('webinar', 'Webinar URL'),
			'tool_params' => 'Tool Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('id_coaching_session',$this->id_coaching_session);
		$criteria->compare('id_tool_account',$this->id_tool_account);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_begin',$this->date_begin,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('tool',$this->tool,true);
		$criteria->compare('tool_params',$this->tool_params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseCoachingWebinarSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave()
    {
        //Save custom URL to tool_params as JSON object
        if ($this->custom_url && $this->tool == LearningCourseCoachingWebinarSession::TOOL_CUSTOM) {
            $params = array('custom_url' => $this->custom_url);
            $this->tool_params = CJSON::encode($params);
        }

        // Try calling the API
        $tool = WebinarTool::getById($this->tool);
        $startDateUtc = Yii::app()->localtime->fromLocalDateTime($this->date_begin);
        $endDateUtc = Yii::app()->localtime->fromLocalDateTime($this->date_end);
        if ($this->isNewRecord) {
            
            if($this->tool !== LearningCourseCoachingWebinarSession::TOOL_CUSTOM) {
                $this->tool_params = CJSON::encode($tool->createRoom($this->name, '', $this->id_tool_account, $startDateUtc, $endDateUtc, $this->max_enroll, $this->tool_params));
            }
        } else
            $this->tool_params = CJSON::encode($tool->updateRoom($this->name, '', $this->id_tool_account,
                is_array($this->tool_params) ? $this->tool_params : CJSON::decode($this->tool_params), $startDateUtc, $endDateUtc, $this->max_enroll));

        return parent::beforeSave();
    }
    public function afterSave(){
        if($this->getScenario()=='insert') {
            $params = array(
                'webinarModel'  => $this,
                'idCourse'      => $this->idCoachingSession->idCourse,
                'courseModel'   => LearningCourse::model()->findByPk($this->idCoachingSession->idCourse),
                'idUser'        => Yii::app()->user->id,
                'idCoach'       => $this->idCoachingSession->idCoach,
                'idSession'     => $this->idCoachingSession->idSession
            );

            Yii::app()->event->raise('NewCoachingWebinarSessionCreated', new DEvent($this, $params));
        }
        return parent::afterSave();
    }

    /**
     * Returns webinar URL string
     * Start Room URL for admins
     * Join Room for other users
     */
    public function getWebinarUrl($idUser)
    {
        $url = '';

        try {
            $tool = WebinarTool::getById($this->tool);
            $userModel = CoreUser::model()->findByPk($idUser);
            $coachingSession = LearningCourseCoachingSession::model()->findByPk($this->id_coaching_session);
            if ($this->allowAdminOperations())
                $url = $tool->getStartRoomUrl($this->name, $coachingSession->max_users, $this->id_tool_account, CJSON::decode($this->tool_params), $userModel, $this);
            else
                $url = $tool->getJoinRoomUrl($this->name, $coachingSession->max_users, $this->id_tool_account, CJSON::decode($this->tool_params), $userModel, $this);

        } catch (CException $e) {

        }

        return $url;
    }

    /**
     * Is current user allowed to manage sessions
     * @return bool
     */
    public function allowAdminOperations()
    {
        $coachingSession = LearningCourseCoachingSession::model()->findByPk($this->id_coaching_session);
        return ($coachingSession->idCoach == Yii::app()->user->id);
    }

    /**
     * Return number of assigned to webinar session users
     */
    public function countWebinarSessionUsers() {
        return LearningCourseCoachingWebinarSessionUser::model()->countByAttributes(array('id_session' => $this->id_session));
    }
    /**
     * convert input values to the DB representation
     * simply calculate date_end with the start date/hour and duration
     */
    public function configureDates()
    {
        $this->date_begin = Yii::app()->localtime-> fromLocalDate($this->date).' '.$this->start;
        $time = new DateTime($this->date_begin);
        $time->add(new DateInterval('PT' . $this->duration . 'M'));
        $this->date_end = $time->format('Y-m-d H:i');

        $this->date_begin = Yii::app()->localtime->toLocalDateTime($this->date_begin, 'short', 'short', null, Yii::app()->localtime->getTimeZone());
        $this->date_end   = Yii::app()->localtime->toLocalDateTime($this->date_end, 'short', 'short', null, Yii::app()->localtime->getTimeZone());

    }

    public function assignUsers($idUsers){
        if (!is_array($idUsers)) {
            $idUsers = array($idUsers);
        }

        $counter = 1;
        $setArray = array();
        foreach ($idUsers as $idUser) {
            $idSessionParam = ":idSession" 	. $counter;
            $idUserParam 	= ":idUser" 	. $counter;
            $setArray[] 	= "($idSessionParam, $idUserParam,'".LearningCourseCoachingWebinarSessionUser::STATUS_INVITED."')";
            $param[$idSessionParam] = $this->id_session;
            $param[$idUserParam] 	= $idUser;
            $counter++;
        }

        $result = 0;
        if (!empty($setArray)) {
            $sql = "INSERT IGNORE INTO learning_course_coaching_webinar_session_user (id_session, id_user,status) VALUES " . implode(',', $setArray);
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->execute($param);
        }

        return $result;
    }
    public function unassignUsers($idUsers){
        if (!is_array($idUsers)) {
            $idUsers = array($idUsers);
        }

        $criteria = new CDbCriteria;
        $criteria->addCondition('id_session='. $this->id_session);
        $criteria->addInCondition('id_user', $idUsers );
        $result = LearningCourseCoachingWebinarSessionUser::model()->deleteAll($criteria);

        return $result;
    }

    /**
     * Compile dates tmp fields for the form representation
     */
    public function afterFind()
    {
        parent::afterFind();
//        if ($this->time_spent_to_complete > 0)
//            $this->eval_spent_time_on_webinar = 1;

        if ($this->date_begin != '0000-00-00 00:00:00') {

            $date_begin = new DateTime(Yii::app()->localtime->fromLocalDatetime($this->date_begin), Yii::app()->localtime->getServerDateTimeZone());
            $date_begin->setTimezone(Yii::app()->localtime->getLocalDateTimeZone());

            $this->date = Yii::app()->localtime->stripTimeFromLocalDateTime($this->date_begin);//$date_begin->format('Y-m-d H:i');//
            $this->start =  $date_begin->format('H:i');//str_replace ($this->date,'',$this->date_begin);//
        }

        if ($this->date_end != '0000-00-00 00:00:00') {
            $date_end = new DateTime(Yii::app()->localtime->fromLocalDatetime($this->date_end), Yii::app()->localtime->getServerDateTimeZone());
            $date_end->setTimezone(Yii::app()->localtime->getLocalDateTimeZone());
            $since_start = $date_begin->diff($date_end);
            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;
            $this->dateEnd = $date_end->format('Y-m-d');
            $this->end = $date_end->format('H:i');
            $this->duration = $minutes;
        }

        if ($this->tool == LearningCourseCoachingWebinarSession::TOOL_CUSTOM)
        {
            $params = CJSON::decode($this->tool_params);
            if (isset($params['custom_url']))
                $this->custom_url = $params['custom_url'];
        }

    }

    public function getListOfAssignedUsers(){
        $command = Yii::app()->db->createCommand()
            ->select("id_user")
            ->from(LearningCourseCoachingWebinarSessionUser::model()->tableName()." wsu");

        $command->where("wsu.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
        $qResults = $command->queryAll();
        $rs = array();
        foreach($qResults as $record){
            $rs[] = $record['id_user'];
        }
        return $rs;
    }

    /**
     * This method is invoked after deleting a record.
     */
    protected function afterDelete()
    {
        // Try calling the API
        $tool = WebinarTool::getById($this->tool);
        $tool->deleteRoom($this->id_tool_account, CJSON::decode($this->tool_params));

        return parent::afterDelete();
    }
}

