<?php

/**
 * This is the model class for table "learning_label_course".
 *
 * The followings are the available columns in table 'learning_label_course':
 * @property integer $id_common_label
 * @property integer $id_course
 */
class LearningLabelCourse extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningLabelCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_label_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_common_label, id_course', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_common_label, id_course', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_common_label' => 'Id Common Label',
			'id_course' => 'Id Course',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_common_label',$this->id_common_label);
		$criteria->compare('id_course',$this->id_course);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Even that we have many-many relational table, the request was to save ONLY ONE label per course...
	 * @param $label
	 * @param $courseId
	 */
	public static function saveLabelsToCourse($label, $courseId)
	{
		// Remove all labels to the given course
		self::model()->deleteAllByAttributes(array(
			'id_course' => $courseId
		));

		//save all given labels to that course
		if ($label)
		{
			$model = new LearningLabelCourse();
			$model->id_common_label = $label;
			$model->id_course = $courseId;
			$model->save(false);
		}
	}

	/**
	 * Creates the assignment of courses to channel through the label assignment
	 * @param $channelId
	 */
	public function createChannelAssignment($channelId) {
		$assignment = new App7020ChannelAssets();
		$assignment->asset_type = 3; // course type
		$assignment->idAsset = $this->id_course;
		$assignment->idChannel = $channelId;
		$assignment->save();
	}
}