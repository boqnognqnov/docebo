<?php

/**
 * This is the model class for table "learning_label".
 *
 * The followings are the available columns in table 'learning_label':
 * @property integer id_filter
 * @property integer report_type_id
 * @property integer author
 * @property datetime creation_date
 * @property string filter_name
 * @property string filter_data
 * @property integer is_public
 * @property integer views
 * @property integer is_standard
 * @property integer id_job
 *
 * Available model AR relations
 * @property LearningReportType $type
 * @property CoreJob  scheduledJob
 *
 */
class LearningReportFilter extends CActiveRecord {

	const EXPORT_TYPE_CSV = 'csv';
	const EXPORT_TYPE_XLS = 'xls';
	const EXPORT_TYPE_HTML = 'html';
	const EXPORT_TYPE_PDF = 'pdf';
	const ALL_COURSES = 1;
	const SELECT_COURSES = 0;

	/**
	 * An array of raw filter data used to build the filter_data table field. Usualy, this is the $_REQUEST.
	 * If it is false, it will not be used upon save.
	 * @var array
	 */
	public $rawFilterData = false;

	/**
	 * Define an array of STANDARD report filters of any type you want.
	 * They are used in afterFind() and replace the one coming from DB.
	 *
	 * Note again: this is only if the report "is standard" !!!
	 * Note again: Standard filter_data in DATABASE is overwritten!
	 *
	 * @var unknown
	 */
	protected static $standardReportFilters = array(
		LearningReportType::USERS_COURSES => array(
			'fields' => array(
				'user' => array(
					'userid' => '1',
					'firstname' => '1',
					'lastname' => '1',
					'email' => '1',
					'register_date' => '1',
					'valid' => '1',
					'suspend_date' => '1',
				),
				'course' => array(
					'name' => '1',
					'coursesCategory.translation' => '1',
					'code' => '1',
					'status' => '1',
					'credits' => '1',
					'date_begin' => '1',
					'date_end' => '1',
					'expired' => '1'
				),
				'enrollment' => array(
					'level' => '1',
					'date_inscr' => '1',
					'date_first_access' => '1',
					'date_complete' => '1',
					'status' => '1',
					'number_of_sessions' => '1',
					'total_time_in_course' => '1',
					'date_begin_validity' => '1',
					'date_expire_validity' => '1',
					'score_given' => '1',
				),
			),
		),
		LearningReportType::USERS_DELAY => array(
			'fields' => array(
				'user' => array(
					'userid' => '1',
					'firstname' => '1',
					'lastname' => '1',
					'email' => '1',
					'register_date' => '1',
					'valid' => '1',
				),
				'course' => array(
					'name' => '1',
					'coursesCategory.translation' => '1',
					'code' => '1',
					'status' => '1',
					'credits' => '1',
				),
			),
			'filters' => array(
				'courses_expiring_in' => '10',
			),
		),
		LearningReportType::USERS_LEARNING => array(
			'fields' => array(
				'user' => array(
					'userid' => '1',
					'firstname' => '1',
					'lastname' => '1',
					'email' => '1',
					'register_date' => '1',
					'valid' => '1',
					'suspend_date' => '1',
				),
				'course' => array(
					'name' => '1',
					'coursesCategory.translation' => '1',
					'code' => '1',
					'status' => '1',
					'credits' => '1',
				),
				'learning_object' => array(
					'objectType' => '1',
					'firstAttempt' => '1',
					'dateAttempt' => '1',
					'status' => '1',
					'score' => '1',
					'version' => '1'
				),
			),
		),
		LearningReportType::COURSES_USERS => array(
			'fields' => array(
				'course' => array(
					'name' => '1',
					'coursesCategory.translation' => '1',
					'code' => '1',
					'status' => '1',
					'credits' => '1',
				),
				'stat' => array(
					'total_subscribed_users' => '1',
					'number_of_users_who_has_not_started_yet' => '1',
					'number_of_users_in_progress' => '1',
					'number_of_users_completed' => '1',
					'total_time_spent_by_users_in_the_course' => '1',
					'show_percents' => '1',
					'rate_average' => '1'
				),
			),
		),
		LearningReportType::GROUPS_COURSES => array(
			'fields' => array(
				'course' => array(
					'name' => '1',
					'coursesCategory.translation' => '1',
					'code' => '1',
					'status' => '1',
					'credits' => '1',
				),
				'stat' => array(
					'total_subscribed_users' => '1',
					'number_of_users_who_has_not_started_yet' => '1',
					'number_of_users_in_progress' => '1',
					'number_of_users_completed' => '1',
					'total_time_spent_by_users_in_the_course' => '1',
					'show_percents' => '1',
				),
				'group' => array(
					'group_name' => '1',
					'type' => '1',
					'total_users_in_group' => '1',
					'course_name' => '1',
				),
			),
		),
		LearningReportType::USERS_COURSEPATH => array(
			'fields' => array(
				'user' => array(
					'userid' => '1',
					'firstname' => '1',
					'lastname' => '1',
					'email' => '1',
					'register_date' => '1',
					'valid' => '1',
					'suspend_date' => '1',
				),
				'coursepaths' => array(
					'plan_code' => '1',
					'plan_credits' => '1',
					'plan_name' => '1',
				),
				'plansUsers' => array(
					'plan_subDate' => '1',
					'plan_compDate' => '1',
					'plan_compStatus' => '1',
					'plan_compPercent' => '1',
				),
			),
		),
	);
	public $confirm;
	// An array of "fields" that are actually UI "options", like  show/hide something.
	// So they are NOT data fields and must be skipped.
	public static $optionFields = array(
		'stat.show_percents',
		'user.show_suspended',
	);

	//An array that maps the report types and the selector used primary key  in the dataprovider query
	public static $reportsGroupsFields = array(
		LearningReportType::USERS_COURSES               =>  self::F_USER_ID,
		LearningReportType::USERS_DELAY		            =>  self::F_USER_ID,
	    LearningReportType::ECOMMERCE_TRANSACTIONS      =>  self::F_USER_ID,
		LearningReportType::USERS_COURSEPATH		    =>  self::F_USER_ID,
		LearningReportType::USERS_SESSION		        =>  self::F_USER_ID,
		LearningReportType::USERS_CERTIFICATION		    =>  self::F_USER_ID,
		LearningReportType::USERS_EXTERNAL_TRAINING	    =>  self::F_USER_ID,
		LearningReportType::USERS_BADGES		        =>  self::F_USER_ID,
		LearningReportType::USERS_CONTESTS		        =>  self::F_USER_ID,
		LearningReportType::APP7020_USER_CONTRIBUTIONS  =>  self::F_USER_CONTRIBUTION_UID,
	);

	// List of filter fields, prefixed by their "bundle" name
	// COURSE
	const F_COURSE_ID = "course.idCourse";
	const F_COURSE_NAME = "course.name";
	const F_COURSE_TYPE = "course.course_type";
	const F_COURSE_CATEGORY = "course.coursesCategory.translation";
	const F_COURSE_CODE = "course.code";
	const F_COURSE_STATUS = "course.status";
	const F_COURSE_CREDITS = "course.credits";
	const F_COURSE_DATE_BEGIN = "course.date_begin";
	const F_COURSE_DATE_END = "course.date_end";
	const F_COURSE_EXPIRED = "course.expired";
	// GROUP
	const F_GROUP_IDST = "group.idst";
	const F_GROUP_GROUPID = "group.groupid";
	const F_GROUP_NAME = "group.group_name";
	const F_GROUP_DESCRIPTION = "course.group_description";
	const F_GROUP_TYPE = "group.type";
	const F_GROUP_TOTAL_USERS = "group.total_users_in_group";
	const F_GROUP_COURSE_NAME = "group.course_name";
	// STAT
	const F_STAT_TOTAL_SUBSCRIBED_USERS = "stat.total_subscribed_users";
	const F_STAT_NUMBER_USERS_NOT_STARTED_COURSE = "stat.number_of_users_who_has_not_started_yet";
	const F_STAT_NUMBER_USERS_NOT_STARTED_COURSE_PERCENTAGE = "stat.number_of_users_who_has_not_started_yet_percentage";
	const F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE = "stat.number_of_users_in_progress";
	const F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE_PERCENTAGE = "stat.number_of_users_in_progress_percentage";
	const F_STAT_NUMBER_USERS_COMPLETED_COURSE = "stat.number_of_users_completed";
	const F_STAT_NUMBER_USERS_COMPLETED_COURSE_PERCENTAGE = "stat.number_of_users_completed_percentage";
	const F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE = "stat.total_time_spent_by_users_in_the_course";
	const F_STAT_TYPE_OF_COURSE = "course.course_type";
	const F_STAT_INTERNAL_COURSE_ID = "course.course_internal_id";
	const F_STAT_AVERAGE_EVALUATION = "stat.rate_average";
	// USER
	const F_USER_ID = 'user.idst';
	const F_USER_IDST = 'user.idUser';
	const F_USER_USERID = 'user.userid';
	const F_USER_FIRSTNAME = 'user.firstname';
	const F_USER_LASTNAME = 'user.lastname';
	const F_USER_EMAIL = 'user.email';
	const F_USER_REGISTER_DATE = 'user.register_date';
	const F_USER_VALID = 'user.valid';
	const F_USER_SUSPEND_DATE = 'user.suspend_date';
//	const F_USER_SUSPENDED													= 'user.suspended';
	const F_USER_SUSPENDED = 'user.valid';
	const F_USER_GROUPS_LIST = 'user.groups_list';
	const F_USER_EXPIRATION = 'user.expiration';
	const F_USER_EMAIL_STATUS = 'user.email_status';
	// ENROLLMENT
	const F_ENROLL_LEVEL = 'enrollment.level';
	const F_ENROLL_DATE_ENROLLMENT = 'enrollment.date_inscr';
	const F_ENROLL_DATE_FIRST_ACCESS = 'enrollment.date_first_access';
	const F_ENROLL_DATE_LAST_ACCESS = 'enrollment.date_last_access';
	const F_ENROLL_DATE_COMPLETE = 'enrollment.date_complete';
	const F_ENROLL_STATUS = 'enrollment.status';
	const F_ENROLL_USER_NUMBER_SESSIONS = 'enrollment.number_of_sessions';
	const F_ENROLL_USER_TIME_IN_COURSE = 'enrollment.total_time_in_course';
	const F_ENROLL_DATE_BEGIN_VALIDITY = 'enrollment.date_begin_validity';
	const F_ENROLL_DATE_EXPIRE_VALIDITY = 'enrollment.date_expire_validity';
	const F_ENROLL_USER_SCORE_GIVEN = 'enrollment.score_given';
	const F_ENROLL_USER_INITIAL_SCORE_GIVEN = 'enrollment.initial_score_given';
	const F_ENROLL_SUBSCRIPTION_CODE = 'enrollment.subscription_code';
	const F_ENROLL_SUBSCRIPTION_SET = 'enrollment.subscription_code_set';
	// LO
	const F_LO_TITLE = 'learning_object.title';
	const F_LO_TYPE = 'learning_object.objectType';
	const F_LO_TRACK_DATE_FIRST_ATTEMPT = 'learning_object.firstAttempt';
	const F_LO_TRACK_DATE_LAST_ATTEMPT = 'learning_object.dateAttempt';
	const F_LO_TRACK_USER_STATUS = 'learning_object.status';
	const F_LO_TRACK_USER_SCORE = 'learning_object.score';
	const F_LO_MILESTONE = 'learning_object.milestone';
	const F_LO_VERSION = 'learning_object.version';
	// Users Delay
	const F_UDELAY_DATE_TO_COMPLETE = 'udelay.dateToComplete';
	const F_UDELAY_DAYS_LEFT_TO_COMPLETE = 'udelay.daysLeftToComplete';
	// Ecommerce
	const F_ECOMMERCE_ID_TRANSACTION = 'ecommerce.id_trans';
	const F_ECOMMERCE_ITEM_QUANTITY = 'ecommerce.quantity';
	const F_ECOMMERCE_ITEM_SINGLE_PRICE = 'ecommerce.single_price';
	const F_ECOMMERCE_TOTAL_PRICE = 'ecommerce.total_price';
	const F_ECOMMERCE_DISCOUNT = 'ecommerce.discount';
	const F_ECOMMERCE_PAYMENT_CURRENCY = 'ecommerce.payment_currency';
	const F_ECOMMERCE_DATE_CREATED = 'ecommerce.date_creation';
	const F_ECOMMERCE_DATE_PAID = 'ecommerce.date_activated';
	const F_ECOMMERCE_PAYMENT_TYPE = 'ecommerce.payment_type';
	const F_ECOMMERCE_PAYMENT_TXN_ID = 'ecommerce.payment_txn_id';
	const F_ECOMMERCE_PAYMENT_STATUS = 'ecommerce.paid';
	const F_ECOMMERCE_ITEM_CODE = 'ecommerce_item.item_code';
	const F_ECOMMERCE_ITEM_NAME = 'ecommerce_item.item_name';
	const F_ECOMMERCE_ITEM_SESSION_NAME = 'ecommerce_item.session_name';
	const F_ECOMMERCE_ITEM_SESSION_START_DATE = 'ecommerce_item.session_start_date';
	const F_ECOMMERCE_ITEM_SESSION_END_DATE = 'ecommerce_item.session_end_date';
	const F_ECOMMERCE_ITEM_TYPE = 'ecommerce_item.item_type';
	const F_ECOMMERCE_BILLING_ADDRESS_1 = 'ecommerce.bill_address1';
	const F_ECOMMERCE_BILLING_ADDRESS_2 = 'ecommerce.bill_address2';
	const F_ECOMMERCE_BILLING_BILL_CITY = 'ecommerce.bill_city';
	const F_ECOMMERCE_BILLING_BILL_STATE = 'ecommerce.bill_state';
	const F_ECOMMERCE_BILLING_BILL_ZIP = 'ecommerce.bill_zip';
	const F_ECOMMERCE_BILLING_BILL_COMPANY_NAME = 'ecommerce.bill_company_name';
	const F_ECOMMERCE_BILLING_VAT_NUMBER = 'ecommerce.bill_vat_number';
	const F_ECOMMERCE_COUPON_CODE = 'ecommerce.coupon_code';
	const F_ECOMMERCE_COUPON_DISCOUNT = 'ecommerce.coupon_discount';
	const F_ECOMMERCE_COUPON_DISCOUNT_TYPE = 'ecommerce.coupon_discount_type';
	const F_ECOMMERCE_COUPON_DESCRIPTION = 'ecommerce.coupon_description';
	const F_ECOMMERCE_LOCATION = 'ecommerce_item.location';
	// Audit trail
	const F_AUDIT_TRAIL_PU_USER_ID = "audit_trail.idUser";
	const F_AUDIT_TRAIL_ACTION = "audit_trail.action";
	const F_AUDIT_TRAIL_TARGET_COURSE = "audit_trail.target_course";
	const F_AUDIT_TRAIL_TARGET_USER = "audit_trail.target_user";
	const F_AUDIT_TRAIL_JSON = "audit_trail.json_data";
	const F_AUDIT_TRAIL_IP = "audit_trail.ip";
	const F_AUDIT_TRAIL_TIMESTAMP = "audit_trail.timestamp";
	const F_AUDIT_TRAIL_APPROVED_BY = "user.approved_by";
	//Users Notifications
	const F_NOTIFICATION_TIMESTAMP = "notification.notif_timestamp";
	const F_NOTIFICATION_TYPE = 'notification.notif_type';
	const F_NOTIFICATION_JSON = 'notification.notif_json_data';
	const F_NOTIFICATIONS_DELIVERED_READ = 'delivered.notif_is_read';
	//Users - Learning Plan (Coursepath)
	const F_COURSEPATH_ID = "coursepaths.plan_id";
	const F_COURSEPATH_CODE = "coursepaths.plan_code";
	const F_COURSEPATH_NAME = "coursepaths.plan_name";
	const F_COURSEPATH_CREDITS = "coursepaths.plan_credits";
	const F_COURSEPATH_SUBSCRIPTION_DATE = "plansUsers.plan_subDate";
	const F_COURSEPATH_COMPLETION_DATE = "plansUsers.plan_compDate";
	const F_COURSEPATH_COMPLETION_STATUS = "plansUsers.plan_compStatus";
	const F_COURSEPATH_COMPLETION_PERCENT = "plansUsers.plan_compPercent";
	//Session info (lt_course_session)
	const F_COURSEUSER_SESSION_NAME = 'session.name';
	const F_COURSEUSER_SESSION_SCORE_BASE = 'session.score_base';
	const F_COURSEUSER_SESSION_DATE_BEGIN = 'session.date_begin';
	const F_COURSEUSER_SESSION_DATE_END = 'session.date_end';
	const F_COURSEUSER_SESSION_TOTAL_HOURS = 'session.total_hours';
	const F_COURSEUSER_SESSION_EVALUATION_SCORE = 'enrollment.learningCourseuserSessions.evaluation_score';
	const F_COURSEUSER_SESSION_EVALUATION_STATUS = 'enrollment.learningCourseuserSessions.evaluation_status';
	const F_COURSEUSER_SESSION_EVALUATION_TEXT = 'enrollment.learningCourseuserSessions.evaluation_text';
	const F_COURSEUSER_SESSION_ATTENDANCE_HOURS = 'enrollment.learningCourseuserSessions.attendance_hours';
	const F_CERTIFICATION_ID = 'certification.id_cert';
	const F_CERTIFICATION_NAME = 'certification.title';
	const F_CERTIFICATION_DESCRIPTION = 'certification.description';
	const F_CERTIFICATION_DURATION = 'certification.duration';
	const F_CERTIFICATION_EXPIRATION = 'certification.expiration';
	const F_CERTIFICATION_ISSUED = 'stat.issued';
	const F_CERTIFICATION_EXPIRED = 'stat.expired';
	const F_CERTIFICATION_ACTIVE = 'stat.active';
	const F_CERTIFICATION_ISSUED_ON = 'enrollment.issued_on';
	const F_CERTIFICATION_RENEW_IN = 'enrollment.to_renew_in';
	const F_CERTIFICATION_ACTIVITY_NAME = 'enrollment.activity_name';
	const F_CERTIFICATION_ACTIVITY_TYPE = 'enrollment.activity_type';
	const F_CERTIFICATION_ACTIVITY = 'enrollment.completed_activity';
	const F_EXTERNAL_TRAININGS_COURSE_NAME = 'external_trainings.course_name';
	const F_EXTERNAL_TRAININGS_COURSE_TYPE = 'external_trainings.course_type';
	const F_EXTERNAL_TRAININGS_SCORE = 'external_trainings.score';
	const F_EXTERNAL_TRAININGS_TO_DATE = 'external_trainings.to_date';
	const F_EXTERNAL_TRAININGS_CREDITS = 'external_trainings.credits';
	const F_EXTERNAL_TRAININGS_TRAINING_INSTITUTE = 'external_trainings.training_institute';
	const F_EXTERNAL_TRAININGS_CERTIFICATE = 'external_trainings.certificate';
	const F_EXTERNAL_TRAININGS_STATUS = 'external_trainings.status';
//    const F_EXTERNAL_TRAININGS_GRANTED_CERTIFICATION = 'external_trainings.granted_certification';
	//BADGES
	const F_BADGES_ICON = 'badges.icon';
	const F_BADGES_NAME = 'badges.name';
	const F_BADGES_DESC = 'badges.description';
	const F_BADGES_SCORE = 'badges.score';
	const F_BADGES_ISSUED_ON = 'assignment.issued_on';
	const F_BADGES_ASSIGNMENT_MODE = 'assignment.event_key';
	const F_CONTEST_NAME = 'contest.contest_name';
	const F_CONTEST_DESCRIPTION = 'contest.contest_description';
	const F_CONTEST_PERIOD = 'contest.contest_period';
	const F_CONTEST_GOAL = 'contest.contest_goal';
	const F_RANKING_POSITION = 'ranking.rank_position';
	const F_RANKING_REWARDS = 'ranking.rank_rewards';
	const F_RANKING_GOAL_STATS = 'ranking.rank_goal_stats';
	//App7020 assets statistics fields
	const F_ASSET_TITLE = 'asset.title';
	const F_ASSET_PUBLISHED_BY = 'asset.published_by';
	const F_ASSET_PUBLISHED_ON = 'asset.published_on';
	const F_ASSET_CHANNELS = 'asset.channels';
	const F_ASSET_TOTAL_VIEWS = 'stat.total_views';
	const F_ASSET_RATING = 'stat.asset_rating';
	const F_ASSET_QUESTIONS = 'stat.questions';
	const F_ASSET_ANSWERS = 'stat.answers';
	const F_ASSET_BEST_ANSWERS = 'stat.best_answers';
	const F_ASSET_LIKES = 'stat.answers_likes';
	const F_ASSET_DISLIKES = 'stat.answers_dislikes';
	const F_ASSET_INVITATIONS = 'stat.total_invited_people';
	const F_ASSET_WATCH_RATE = 'stat.global_watch_rate';
	const F_ASSET_REACTION_TIME = 'stat.average_reaction_time';
	const F_ASSET_WATCHED = 'stat.watched';
	const F_ASSET_NOT_WATCHED = 'stat.not_watched';
	const F_EXPERTS_USERNAME = 'experts.username';
	const F_EXPERTS_EXPERT_FULL_NAME = 'experts.expert_full_name';
	const F_EXPERTS_EMAIL = 'experts.email';
	const F_EXPERTS_ASSIGNED_CHANNELS = 'experts.assigned_channels';
	const F_EXPERTS_QUESTION_ANSWERED = 'caf.questions_answered';
	const F_EXPERTS_AVERAGE_ANSWER_TIME = 'caf.average_answer_time';
	const F_EXPERTS_REQUESTS_SATISFIED = 'caf.requests_satisfied';
	const F_EXPERTS_ANSWER_LIKES = 'caf.answers_likes';
	const F_EXPERTS_ANSWER_DISLIKES = 'caf.answers_dislikes';
	const F_EXPERTS_BEST_ANSWER = 'caf.best_answer';
	const F_EXPERTS_RANK_BY_ANSWERS_QUALITY = 'caf.rank_by_answers_quality';
	const F_EXPERTS_RANK_BY_FIRST_TO_ANSWER_RATE = 'caf.rank_by_first_to_answer_rate';
	const F_EXPERTS_RANK_BY_PARTECIPATION_RATE = 'caf.rank_by_partecipation_rate';
	const F_EXPERTS_REVIEW_ASSETS = 'pr.review_assets';
	const F_EXPERTS_WRITEN_PEER_REVIEWS = 'pr.writen_peer_reviews';
	const F_EXPERTS_ASSET_PUBLISHED = 'pr.asset_published';
	const F_EXPERTS_AVERAGE_REVIEW_TIME = 'pr.average_review_time';
	const F_EXPERTS_RANK_BY_PARTECIPATION_RATE_PR = 'pr.rank_by_partecipation_rate';
	const F_EXPERTS_RANK_BY_FIRST_TO_REVIEW = 'pr.rank_by_first_to_review';
	//App7020 channels statistics fields
	const F_CHANNELS_NAME = 'channel.name';
	const F_CHANNELS_DESCRIPTION = 'channel.description';
	const F_CHANNELS_PUBLISHED_ASSETS = 'channel.published_assets';
	const F_CHANNELS_SUBMITTED_ASSETS = 'stat.submitted_assets';
	const F_CHANNELS_UNPUBLISHED_ASSETS = 'stat.unpublished_assets';
	const F_CHANNELS_ASSETS_RATING = 'stat.average_assets_rating';
	const F_CHANNELS_TOTAL_CONTRIBUTORS = 'stat.total_contributors';
	const F_CHANNELS_BEST_ANSWERS = 'stat.best_answers';
	const F_CHANNELS_QUESTIONS = 'stat.questions';
	const F_CHANNELS_ANSWERS = 'stat.answers';
	const F_CHANNELS_EXPERTS = 'stat.channel_experts';
	const F_CHANNELS_ASKERS = 'stat.askers_involved';
	const F_CHANNELS_PERCENT_PR = 'stat.percent_peer_reviews';
	const F_CHANNELS_PERCENT_SHARED = 'stat.percent_shared_assets';
	const F_CHANNELS_PERCENT_QA = 'stat.percent_qa';


	//App7020 user contributions custom report
	const F_USER_CONTRIBUTION_UID = 'us.idst';
	const F_USER_CONTRIBUTION_FIRSTNAME = 'us.firstname';
	const F_USER_CONTRIBUTION_LASTNAME = 'us.lastname';
	const F_USER_CONTRIBUTION_EMAIL = 'us.email';
	const F_USER_CONTRIBUTION_CREATION_DATE = 'us.creation_date';
	const F_USER_CONTRIBUTION_ACTIVE_DATE = 'us.active_date';
	const F_USER_CONTRIBUTION_SUSPEND_DATE = 'us.suspend_date';
	const F_USER_CONTRIBUTION_EXPIRY = 'us.expiry';
	const F_USER_CONTRIBUTION_EMAIL_VALIDATION_STATUS = 'us.email_validation_status';
	const F_USER_CONTRIBUTION_BRANCHES = 'us.branches';
	const F_USER_CONTRIBUTION_ROLE = 'us.role';

	const F_USER_CONTRIBUTION_UPLOADED_ASSETS = 'as.number_of_uploaded_assets';
	const F_USER_CONTRIBUTION_INVOLVED_CHANNELS = 'as.involved_channels';
	const F_USER_CONTRIBUTION_PUBLISHED_STATUS = 'as.published_status';
	const F_USER_CONTRIBUTION_UNUBLISHED_STATUS = 'as.unpublished_status';
	const F_USER_CONTRIBUTION_PRIVATE_ASSETS = 'as.private_assets';







	/**
	 * use this property to get only allowed to be sorted elements from all reports,
	 * this returns the real tables and columns, or their aliases from the sql provider method for each report
	 * IF you change values or add/delete some of the constants above, also change tables in sql provider methods OR their aliases, please update this array!!!
	 */
	public static $remappedConstants = array(
		self::F_COURSE_CATEGORY => 'coursecat.translation',
		self::F_COURSE_CODE => self::F_COURSE_CODE,
		self::F_COURSE_CREDITS => self::F_COURSE_CREDITS,
		self::F_COURSE_DATE_BEGIN => self::F_COURSE_DATE_BEGIN,
		self::F_COURSE_DATE_END => self::F_COURSE_DATE_END,
		self::F_COURSE_ID => self::F_COURSE_ID,
		self::F_COURSE_NAME => self::F_COURSE_NAME,
		self::F_COURSE_STATUS => self::F_COURSE_STATUS,
		self::F_COURSE_TYPE => self::F_COURSE_TYPE,
		self::F_GROUP_IDST => 't.idst',
		self::F_GROUP_GROUPID => 't.groupid',
		self::F_GROUP_TYPE => 't.type',
		self::F_USER_USERID => self::F_USER_USERID,
		self::F_USER_FIRSTNAME => self::F_USER_FIRSTNAME,
		self::F_USER_LASTNAME => self::F_USER_LASTNAME,
		self::F_USER_EMAIL => self::F_USER_EMAIL,
		self::F_USER_REGISTER_DATE => self::F_USER_REGISTER_DATE,
		self::F_USER_VALID => self::F_USER_VALID,
		self::F_USER_SUSPEND_DATE => self::F_USER_SUSPEND_DATE,
		self::F_USER_SUSPENDED => self::F_USER_SUSPENDED,
		self::F_USER_EXPIRATION => self::F_USER_EXPIRATION,
		self::F_ENROLL_LEVEL => self::F_ENROLL_LEVEL,
		self::F_ENROLL_DATE_ENROLLMENT => self::F_ENROLL_DATE_ENROLLMENT,
		self::F_ENROLL_DATE_FIRST_ACCESS => self::F_ENROLL_DATE_FIRST_ACCESS,
		self::F_ENROLL_DATE_LAST_ACCESS => self::F_ENROLL_DATE_LAST_ACCESS,
		self::F_ENROLL_DATE_COMPLETE => self::F_ENROLL_DATE_COMPLETE,
		self::F_ENROLL_STATUS => self::F_ENROLL_STATUS,
		self::F_ENROLL_DATE_BEGIN_VALIDITY => self::F_ENROLL_DATE_BEGIN_VALIDITY,
		self::F_ENROLL_DATE_EXPIRE_VALIDITY => self::F_ENROLL_DATE_EXPIRE_VALIDITY,
		self::F_ENROLL_USER_SCORE_GIVEN => self::F_ENROLL_USER_SCORE_GIVEN,
		self::F_ENROLL_USER_INITIAL_SCORE_GIVEN => self::F_ENROLL_USER_INITIAL_SCORE_GIVEN,
		self::F_LO_TITLE => 'learning_organization.title',
		self::F_LO_TYPE => 'learning_organization.objectType',
		self::F_LO_TRACK_DATE_FIRST_ATTEMPT => 'commontrack.firstAttempt',
		self::F_LO_TRACK_DATE_LAST_ATTEMPT => 'commontrack.dateAttempt',
		self::F_LO_TRACK_USER_STATUS => 'commontrack.status',
		self::F_LO_TRACK_USER_SCORE => 'commontrack.score',
		self::F_LO_MILESTONE => 'learning_organization.milestone',
		self::F_ECOMMERCE_ID_TRANSACTION => self::F_ECOMMERCE_ID_TRANSACTION,
		self::F_ECOMMERCE_ITEM_SINGLE_PRICE => 'ecommerce_item.price',
		self::F_ECOMMERCE_DISCOUNT => self::F_ECOMMERCE_DISCOUNT,
		self::F_ECOMMERCE_PAYMENT_CURRENCY => self::F_ECOMMERCE_PAYMENT_CURRENCY,
		self::F_ECOMMERCE_DATE_CREATED => self::F_ECOMMERCE_DATE_CREATED,
		self::F_ECOMMERCE_DATE_PAID => self::F_ECOMMERCE_DATE_PAID,
		self::F_ECOMMERCE_PAYMENT_STATUS => self::F_ECOMMERCE_PAYMENT_STATUS,
		self::F_ECOMMERCE_ITEM_TYPE => self::F_ECOMMERCE_ITEM_TYPE,
		self::F_ECOMMERCE_ITEM_CODE => 'ecommerce_item.code',
		self::F_ECOMMERCE_ITEM_NAME => 'ecommerce_item.name',
		self::F_AUDIT_TRAIL_PU_USER_ID => self::F_AUDIT_TRAIL_PU_USER_ID,
		self::F_AUDIT_TRAIL_ACTION => self::F_AUDIT_TRAIL_ACTION,
		self::F_AUDIT_TRAIL_TARGET_COURSE => self::F_AUDIT_TRAIL_TARGET_COURSE,
		self::F_AUDIT_TRAIL_TARGET_USER => self::F_AUDIT_TRAIL_TARGET_USER,
		self::F_AUDIT_TRAIL_JSON => self::F_AUDIT_TRAIL_JSON,
		self::F_AUDIT_TRAIL_IP => self::F_AUDIT_TRAIL_IP,
		self::F_AUDIT_TRAIL_TIMESTAMP => self::F_AUDIT_TRAIL_TIMESTAMP,
		self::F_NOTIFICATION_TIMESTAMP => 'inbox_app_notification.timestamp',
		self::F_NOTIFICATION_TYPE => 'inbox_app_notification.type',
		self::F_NOTIFICATION_JSON => 'inbox_app_notification.json_data',
		self::F_NOTIFICATIONS_DELIVERED_READ => 'inbox_app_notification_delivered.is_read',
		self::F_COURSEPATH_ID => "plan.id_path",
		self::F_COURSEPATH_CODE => "plan.path_code",
		self::F_COURSEPATH_NAME => "plan.path_name",
		self::F_COURSEPATH_CREDITS => "plan.credits",
		self::F_COURSEPATH_SUBSCRIPTION_DATE => "planUser.date_assign",
		self::F_COURSEPATH_COMPLETION_STATUS => "planUser.course_completed",
		self::F_COURSEUSER_SESSION_NAME => 'cs.name',
		self::F_COURSEUSER_SESSION_SCORE_BASE => 'cs.score_base',
		self::F_COURSEUSER_SESSION_DATE_BEGIN => 'cs.date_begin',
		self::F_COURSEUSER_SESSION_DATE_END => 'cs.date_end',
		self::F_COURSEUSER_SESSION_TOTAL_HOURS => 'cs.total_hours',
		self::F_COURSEUSER_SESSION_EVALUATION_STATUS => 'cus.status',
		self::F_COURSEUSER_SESSION_EVALUATION_TEXT => 'cus.evaluation_text',
		self::F_RANKING_POSITION => 'ranking.rank',
		self::F_RANKING_GOAL_STATS => 'ranking.ranking_result',
		self::F_CONTEST_NAME => 'gct.name',
		self::F_CONTEST_DESCRIPTION => 'gct.description',
		self::F_CONTEST_PERIOD => 'contest.from_date',
		self::F_CERTIFICATION_NAME => self::F_CERTIFICATION_NAME,
		self::F_BADGES_ICON => 'gb.icon',
		self::F_BADGES_NAME => 'gbt.name',
		self::F_BADGES_DESC => 'gbt.description',
		self::F_BADGES_SCORE => 'gb.score',
		self::F_BADGES_ISSUED_ON => 'assignment.issued_on',
		self::F_BADGES_ASSIGNMENT_MODE => 'assignment.event_key',
		self::F_ASSET_TITLE => 'asset.title',
		self::F_ASSET_PUBLISHED_BY => 'asset.published_by',
		self::F_ASSET_PUBLISHED_ON => 'asset.published_on',
		self::F_ASSET_TOTAL_VIEWS => 'stat.total_views',
		self::F_ASSET_RATING => 'stat.asset_rating',
		self::F_ASSET_QUESTIONS => 'stat.questions',
		self::F_ASSET_ANSWERS => 'stat.answers',
		self::F_ASSET_BEST_ANSWERS => 'stat.best_answers',
		self::F_ASSET_LIKES => 'stat.answers_likes',
		self::F_ASSET_DISLIKES => 'stat.answers_dislikes',
		self::F_ASSET_INVITATIONS => self::F_ASSET_INVITATIONS,
		self::F_ASSET_WATCHED => 'stat.watched',
		self::F_ASSET_NOT_WATCHED => 'stat.not_watched',
		self::F_EXPERTS_USERNAME => 'experts.username',
		self::F_EXPERTS_EXPERT_FULL_NAME => 'experts.expert_full_name',
		self::F_EXPERTS_EMAIL => 'experts.email',
		self::F_EXPERTS_ASSIGNED_CHANNELS => 'experts.assigned_channels',
		self::F_EXPERTS_QUESTION_ANSWERED => 'caf.questions_answered',
		self::F_EXPERTS_AVERAGE_ANSWER_TIME => 'caf.average_answer_time',
		self::F_EXPERTS_REQUESTS_SATISFIED => 'caf.requests_satisfied',
		self::F_EXPERTS_ANSWER_LIKES => 'caf.answers_likes',
		self::F_EXPERTS_ANSWER_DISLIKES => 'caf.answers_dislikes',
		self::F_EXPERTS_BEST_ANSWER => 'caf.best_answer',
		self::F_EXPERTS_RANK_BY_ANSWERS_QUALITY => 'caf.rank_by_answers_quality',
		self::F_EXPERTS_RANK_BY_FIRST_TO_ANSWER_RATE => 'caf.rank_by_first_to_answer_rate',
		self::F_EXPERTS_RANK_BY_PARTECIPATION_RATE => 'caf.rank_by_partecipation_rate',
		self::F_EXPERTS_REVIEW_ASSETS => 'pr.review_assets',
		self::F_EXPERTS_WRITEN_PEER_REVIEWS => 'pr.writen_peer_reviews',
		self::F_EXPERTS_ASSET_PUBLISHED => 'pr.asset_published',
		self::F_EXPERTS_AVERAGE_REVIEW_TIME => 'pr.average_review_time',
		self::F_EXPERTS_RANK_BY_PARTECIPATION_RATE_PR => 'pr.rank_by_partecipation_rate',
		self::F_EXPERTS_RANK_BY_FIRST_TO_REVIEW => 'pr.rank_by_first_to_review',
		self::F_CHANNELS_NAME => 'channel.name',
		self::F_CHANNELS_DESCRIPTION => 'channel.description',
		self::F_CHANNELS_PUBLISHED_ASSETS => 'channel.published_assets',
		self::F_CHANNELS_SUBMITTED_ASSETS => 'stat.submitted_assets',
		self::F_CHANNELS_UNPUBLISHED_ASSETS => 'stat.unpublished_assets',
		self::F_CHANNELS_ASSETS_RATING => 'stat.average_assets_rating',
		self::F_CHANNELS_TOTAL_CONTRIBUTORS => 'stat.total_contributors',
		self::F_CHANNELS_BEST_ANSWERS => 'stat.best_answers',
		self::F_CHANNELS_QUESTIONS => 'stat.questions',
		self::F_CHANNELS_ANSWERS => 'stat.answers',
		self::F_CHANNELS_EXPERTS => 'stat.experts',
		self::F_CHANNELS_ASKERS => 'stat.askers',
		self::F_CHANNELS_PERCENT_PR => 'stat.percent_pr',
		self::F_CHANNELS_PERCENT_SHARED => 'stat.percent_shared_assets',
		self::F_CHANNELS_PERCENT_QA => 'stat.percent_qa',
		self::F_USER_CONTRIBUTION_UID => 'us.idst',
		self::F_USER_CONTRIBUTION_FIRSTNAME => 'us.firstname',
		self::F_USER_CONTRIBUTION_LASTNAME => 'us.lastname',
		self::F_USER_CONTRIBUTION_EMAIL => 'us.email',
		self::F_USER_CONTRIBUTION_CREATION_DATE => 'us.creation_date',
		self::F_USER_CONTRIBUTION_ACTIVE_DATE => 'us.active_date',
		self::F_USER_CONTRIBUTION_SUSPEND_DATE => 'us.suspend_date',
		self::F_USER_CONTRIBUTION_EXPIRY => 'us.expiry',
		self::F_USER_CONTRIBUTION_EMAIL_VALIDATION_STATUS => 'us.email_validation_status',
		self::F_USER_CONTRIBUTION_BRANCHES => 'us.branches',
		self::F_USER_CONTRIBUTION_ROLE => 'us.role',
		self::F_USER_CONTRIBUTION_UPLOADED_ASSETS => 'as.number_of_uploaded_assets',
		self::F_USER_CONTRIBUTION_INVOLVED_CHANNELS => 'as.involved_channels',
		self::F_USER_CONTRIBUTION_PUBLISHED_STATUS => 'as.published_status',
		self::F_USER_CONTRIBUTION_UNUBLISHED_STATUS => 'as.unpublished_status',
		self::F_USER_CONTRIBUTION_PRIVATE_ASSETS => 'as.private_assets',
	);

	/**
	 * Columns order in different report types
	 * @var Array
	 */
	public static $columnsOrder = array(
		LearningReportType::GROUPS_COURSES => array(
			self::F_GROUP_NAME,
			self::F_GROUP_GROUPID,
			self::F_GROUP_TOTAL_USERS,
			self::F_COURSE_CATEGORY,
			self::F_COURSE_NAME,
			self::F_COURSE_CODE,
			self::F_COURSE_STATUS,
			self::F_COURSE_EXPIRED,
			self::F_COURSE_CREDITS,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_STAT_TOTAL_SUBSCRIBED_USERS,
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE,
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE_PERCENTAGE,
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE,
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE_PERCENTAGE,
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE,
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE_PERCENTAGE,
			self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE,
			self::F_STAT_TYPE_OF_COURSE,
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_GROUP_DESCRIPTION,

		),
		LearningReportType::USERS_CERTIFICATION => array(
			self::F_USER_IDST,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_ENROLL_LEVEL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_CERTIFICATION_NAME,
			self::F_CERTIFICATION_DESCRIPTION,
			self::F_CERTIFICATION_EXPIRATION,
			self::F_CERTIFICATION_ISSUED_ON,
			self::F_CERTIFICATION_RENEW_IN,
			self::F_CERTIFICATION_ACTIVITY_NAME,
			self::F_CERTIFICATION_ACTIVITY_TYPE
		),
		LearningReportType::USERS_BADGES => array(
			self::F_USER_IDST,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_REGISTER_DATE,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_BADGES_ASSIGNMENT_MODE,
			self::F_BADGES_ICON,
			self::F_BADGES_NAME,
			self::F_BADGES_DESC,
			self::F_BADGES_SCORE,
			self::F_BADGES_ISSUED_ON,
		),
		LearningReportType::USERS_COURSES => array(
			self::F_USER_IDST,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_ENROLL_LEVEL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_COURSE_CODE,
			self::F_COURSE_NAME,
			self::F_COURSE_CATEGORY,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_COURSE_EXPIRED,
			self::F_COURSE_TYPE,
			self::F_ENROLL_DATE_BEGIN_VALIDITY,
			self::F_ENROLL_DATE_EXPIRE_VALIDITY,
			//self::F_COURSE_STATUS,
			//self::F_GROUP_NAME,
			self::F_USER_REGISTER_DATE,
			self::F_ENROLL_DATE_ENROLLMENT,
			self::F_ENROLL_DATE_FIRST_ACCESS,
			self::F_ENROLL_DATE_COMPLETE,
			self::F_ENROLL_DATE_LAST_ACCESS,
			self::F_ENROLL_STATUS,
			self::F_ENROLL_USER_SCORE_GIVEN,
			self::F_ENROLL_USER_INITIAL_SCORE_GIVEN,
			self::F_ENROLL_USER_NUMBER_SESSIONS,
			self::F_ENROLL_USER_TIME_IN_COURSE,
			self::F_COURSE_CREDITS,
			self::F_ENROLL_SUBSCRIPTION_CODE,
			self::F_ENROLL_SUBSCRIPTION_SET
		),
		LearningReportType::COURSES_USERS => array(
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_COURSE_NAME,
			self::F_COURSE_CATEGORY,
			self::F_COURSE_CODE,
			self::F_COURSE_STATUS,
			self::F_COURSE_EXPIRED,
			self::F_COURSE_CREDITS,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_COURSE_TYPE,
			self::F_STAT_TOTAL_SUBSCRIBED_USERS,
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE,
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE_PERCENTAGE,
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE,
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE_PERCENTAGE,
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE,
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE_PERCENTAGE,
			self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE,
			self::F_STAT_AVERAGE_EVALUATION
		),
		LearningReportType::USERS_LEARNING => array(
			self::F_USER_GROUPS_LIST,
			self::F_USER_IDST,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_USER_REGISTER_DATE,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_COURSE_NAME,
			self::F_COURSE_CATEGORY,
			self::F_COURSE_CODE,
			self::F_COURSE_STATUS,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_COURSE_TYPE,
			self::F_LO_TITLE,
			self::F_LO_TYPE,
			self::F_LO_TRACK_USER_STATUS,
			self::F_LO_TRACK_DATE_FIRST_ATTEMPT,
			self::F_LO_TRACK_DATE_LAST_ATTEMPT,
			self::F_LO_MILESTONE,
			self::F_LO_TRACK_USER_SCORE,
			self::F_LO_VERSION,
			self::F_ENROLL_DATE_ENROLLMENT,
			self::F_ENROLL_DATE_COMPLETE,
		),
		LearningReportType::USERS_DELAY => array(
			self::F_USER_ID,
			self::F_USER_GROUPS_LIST,
			self::F_USER_IDST,
			self::F_USER_USERID,
			self::F_USER_LASTNAME,
			self::F_USER_FIRSTNAME,
			self::F_USER_EMAIL,
			self::F_USER_REGISTER_DATE,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			//self::F_USER_SUSPENDED,
			//self::F_USER_SUSPEND_DATE,
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_COURSE_NAME,
			self::F_COURSE_CATEGORY,
			self::F_COURSE_CODE,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_COURSE_TYPE,
			//self::F_COURSE_STATUS,
			//self::F_COURSE_CREDITS,
			self::F_ENROLL_LEVEL,
			self::F_ENROLL_STATUS,
			self::F_ENROLL_DATE_ENROLLMENT,
			self::F_ENROLL_DATE_FIRST_ACCESS,
			self::F_ENROLL_DATE_LAST_ACCESS,
			//self::F_ENROLL_DATE_COMPLETE,
			self::F_ENROLL_USER_NUMBER_SESSIONS,
			self::F_ENROLL_USER_TIME_IN_COURSE,
			self::F_UDELAY_DATE_TO_COMPLETE,
			self::F_UDELAY_DAYS_LEFT_TO_COMPLETE,
		),
		LearningReportType::ECOMMERCE_TRANSACTIONS => array(
			self::F_USER_ID,
			self::F_ECOMMERCE_ID_TRANSACTION,
			self::F_ECOMMERCE_ITEM_QUANTITY,
			self::F_ECOMMERCE_ITEM_SINGLE_PRICE,
			self::F_ECOMMERCE_TOTAL_PRICE,
			self::F_ECOMMERCE_DISCOUNT,
			self::F_ECOMMERCE_PAYMENT_CURRENCY,
			self::F_ECOMMERCE_PAYMENT_STATUS,
			self::F_ECOMMERCE_DATE_CREATED,
			self::F_ECOMMERCE_DATE_PAID,
			self::F_USER_IDST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_REGISTER_DATE,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_USER_GROUPS_LIST,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_ECOMMERCE_ITEM_TYPE,
			self::F_ECOMMERCE_ITEM_CODE,
			self::F_ECOMMERCE_ITEM_NAME,
			self::F_ECOMMERCE_ITEM_SESSION_NAME,
			self::F_ECOMMERCE_ITEM_SESSION_START_DATE,
			self::F_ECOMMERCE_ITEM_SESSION_END_DATE,
			self::F_ECOMMERCE_PAYMENT_TYPE,
			self::F_ECOMMERCE_PAYMENT_TXN_ID,
			self::F_ECOMMERCE_BILLING_BILL_COMPANY_NAME,
			self::F_ECOMMERCE_BILLING_VAT_NUMBER,
			self::F_ECOMMERCE_BILLING_ADDRESS_1,
			self::F_ECOMMERCE_BILLING_ADDRESS_2,
			self::F_ECOMMERCE_BILLING_BILL_CITY,
			self::F_ECOMMERCE_BILLING_BILL_STATE,
			self::F_ECOMMERCE_BILLING_BILL_ZIP,
			self::F_ECOMMERCE_COUPON_CODE,
			self::F_ECOMMERCE_COUPON_DESCRIPTION,
			self::F_ECOMMERCE_LOCATION,
		),
		LearningReportType::USERS_NOTIFICATIONS => array(
			self::F_NOTIFICATION_TYPE,
			self::F_NOTIFICATION_TIMESTAMP,
			self::F_NOTIFICATION_JSON,
			self::F_NOTIFICATIONS_DELIVERED_READ,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_GROUPS_LIST,
		),
		LearningReportType::USERS_COURSEPATH => array(
			self::F_USER_ID,
			self::F_USER_GROUPS_LIST,
			self::F_USER_IDST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_REGISTER_DATE,
			self::F_USER_EMAIL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_COURSEPATH_CODE,
			self::F_COURSEPATH_NAME,
			self::F_COURSEPATH_CREDITS,
			self::F_COURSEPATH_SUBSCRIPTION_DATE,
			self::F_COURSEPATH_COMPLETION_DATE,
			self::F_COURSEPATH_COMPLETION_STATUS,
			self::F_COURSEPATH_COMPLETION_PERCENT,
		),
		LearningReportType::USERS_SESSION => array(
			self::F_USER_ID,
			self::F_USER_GROUPS_LIST,
			self::F_USER_IDST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_ENROLL_LEVEL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_STAT_INTERNAL_COURSE_ID,
			self::F_COURSE_CODE,
			self::F_COURSE_NAME,
			self::F_COURSE_CATEGORY,
			//self::F_COURSE_STATUS,
			self::F_USER_REGISTER_DATE,
			self::F_COURSE_DATE_BEGIN,
			self::F_COURSE_DATE_END,
			self::F_COURSE_TYPE,
			self::F_ENROLL_DATE_ENROLLMENT,
			self::F_ENROLL_STATUS,
			self::F_ENROLL_USER_NUMBER_SESSIONS,
			self::F_ENROLL_USER_TIME_IN_COURSE,
			self::F_COURSE_CREDITS,
			//Sessions reports
			self::F_COURSEUSER_SESSION_NAME,
			self::F_COURSEUSER_SESSION_SCORE_BASE,
			self::F_COURSEUSER_SESSION_DATE_BEGIN,
			self::F_COURSEUSER_SESSION_DATE_END,
			self::F_COURSEUSER_SESSION_TOTAL_HOURS,
			self::F_COURSEUSER_SESSION_EVALUATION_SCORE,
			self::F_COURSEUSER_SESSION_EVALUATION_STATUS,
			self::F_COURSEUSER_SESSION_EVALUATION_TEXT,
			self::F_COURSEUSER_SESSION_ATTENDANCE_HOURS,
		),
		LearningReportType::USERS_EXTERNAL_TRAINING => array(
			self::F_USER_IDST,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_ENROLL_LEVEL,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_USER_REGISTER_DATE,
			self::F_EXTERNAL_TRAININGS_COURSE_NAME,
			self::F_EXTERNAL_TRAININGS_COURSE_TYPE,
			self::F_EXTERNAL_TRAININGS_SCORE,
			self::F_EXTERNAL_TRAININGS_TO_DATE,
			self::F_EXTERNAL_TRAININGS_CREDITS,
			self::F_EXTERNAL_TRAININGS_TRAINING_INSTITUTE,
			self::F_EXTERNAL_TRAININGS_CERTIFICATE,
			self::F_EXTERNAL_TRAININGS_STATUS
		),
		LearningReportType::USERS_CONTESTS => array(
			self::F_USER_ID,
			self::F_USER_GROUPS_LIST,
			self::F_USER_USERID,
			self::F_USER_EXPIRATION,
			self::F_USER_EMAIL_STATUS,
			self::F_USER_FIRSTNAME,
			self::F_USER_LASTNAME,
			self::F_USER_EMAIL,
			self::F_USER_EMAIL,
			self::F_USER_REGISTER_DATE,
			self::F_USER_SUSPENDED,
			self::F_USER_SUSPEND_DATE,
			self::F_CONTEST_PERIOD,
			self::F_CONTEST_GOAL,
			self::F_CONTEST_NAME,
			self::F_CONTEST_DESCRIPTION,
			self::F_RANKING_GOAL_STATS,
			self::F_RANKING_POSITION,
			self::F_RANKING_REWARDS
		)
	);

	/**
	 * Return list of export types

	 * @return array
	 */
	public static function exportTypes() {
		$exportTypes = array(
			LearningReportFilter::EXPORT_TYPE_XLS => Yii::t('standard', '_EXPORT_XLS'),
			LearningReportFilter::EXPORT_TYPE_CSV => Yii::t('standard', '_EXPORT_CSV'),
			LearningReportFilter::EXPORT_TYPE_HTML => Yii::t('standard', '_EXPORT_HTML'),
		);

		return $exportTypes;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningLabel the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function init() {
		$this->author = "";
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_report_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array(
			array('report_type_id', 'required'),
			array('report_type_id', 'numerical', 'integerOnly' => true),
			array('id_job', 'numerical', 'integerOnly' => true),
			array('report_type_id', 'compare', 'compareValue' => 0, 'operator' => '>', 'message' => 'Report type is required'),
			array('filter_name', 'required'),
			array('filter_name', 'length', 'max' => 255),
			array('author', 'numerical', 'integerOnly' => true),
			// array('description', 'required'),
			// array('id_common_label', 'numerical', 'integerOnly'=>true),
			// array('lang_code, title, color', 'length', 'max'=>255),
			// array('sequence', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			// array('id_common_label, lang_code, title, description, color, sequence', 'safe', 'on'=>'search'),
		);
	}

	// public function checkReportTypeId

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type' => array(self::BELONGS_TO, 'LearningReportType', 'report_type_id'),
			'access' => array(self::BELONGS_TO, 'LearningReportAccess', 'id_filter'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'author'),
			'scheduledJob' => array(self::BELONGS_TO, 'CoreJob', 'id_job'),
			// 'course' => array(self::HAS, 'CoreUser', 'author'),
			// 'labelCourses' => array(self::HAS_MANY, 'LearningLabelCourse', 'id_common_label'),
			// 'learningCourses' => array(self::HAS_MANY, 'LearningCourse', array("id_course" => "idCourse"),'through' => 'labelCourses'),
			// 'learningCourses' => array(self::MANY_MANY, 'LearningCourse', 'learning_label_course(id_common_label, id_course)'),
		);
	}

	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('creation_date medium'),
				'dateAttributes' => array()
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		$attributeLabels = array(
			'id_filter' => 'Id filter', // do not translate
			'id_report' => 'Id report', // do not translate
			'author' => Yii::t('standard', '_AUTHOR'),
			'creation_date' => Yii::t('report', '_CREATION_DATE'),
			'filter_name' => Yii::t('standard', '_NAME'),
			'filter_data' => 'Data', // do not translate
			'is_public' => Yii::t('report', '_TAB_REP_PUBLIC'),
			'views' => Yii::t('standard', '_PLAY_CHANCE'),
			'is_standard' => 'Standard', // do not translate
			'id_job' => 'Job ID',
		);
		$result = array_merge(self::fieldNames(), $attributeLabels); // To get the translations for all the fields in reports

		return $result;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		// $criteria->compare('id_common_label',$this->id_common_label);
		// $criteria->compare('lang_code',$this->lang_code,true);
		// $criteria->compare('title',$this->title,true);
		// $criteria->compare('description',$this->description,true);
		// $criteria->compare('color',$this->color,true);
		// $criteria->compare('sequence',$this->sequence,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	public function dataProvider() {
		$criteria = new CDbCriteria;

		$criteria->with = array(
			'type' => array(
				'joinType' => 'JOIN'
			),
			'user'
		);

		//$criteria->compare('is_standard', 1);
		$criteria->compare('filter_name', $this->filter_name, true);
		$criteria->compare('author', $this->author, true);

		if (!PluginManager::isPluginActive('EcommerceApp')) {
			$criteria->addCondition('report_type_id != '. LearningReportType::ECOMMERCE_TRANSACTIONS);
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		if (Yii::app()->user->getIsAdmin()) {
			// allowed reports to see with "selection" access filter
			$allReportsIds = Yii::app()->db->createCommand()
							->select('id_filter')
							->from(LearningReportFilter::model()->tableName())->queryColumn();
			$targetIds = array();
			foreach ($allReportsIds as $reportId) {
				if (LearningReportAccess::checkPermissionToEdit(Yii::app()->user->id, $reportId) === true)
					$targetIds[] = $reportId;
			}

			$criteria->join = 'JOIN ' . LearningReportAccess::model()->tableName() . ' lrv ON t.id_filter = lrv.id_report';
			$criteria->addCondition('t.author = :author OR lrv.visibility_type = :type OR(lrv.visibility_type = :selection AND t.`id_filter` IN(' . "'" . implode("', '", $targetIds) . "'" . '))');
			$criteria->params[':type'] = LearningReportAccess::TYPE_PUBLIC;
//            $criteria->condition .= ' AND (t.is_public = :is_public OR t.author = :author)';
//            $criteria->params[':is_public'] = 1;
			$criteria->params[':author'] = Yii::app()->user->id;
			$criteria->params[':selection'] = LearningReportAccess::TYPE_SELECTION;

		}

		$config = array();
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'is_standard DESC,creation_date DESC';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderStandard($filterInactivePlugins = false) {
		$criteria = new CDbCriteria;

		$criteria->compare('is_standard', true);

		if ($filterInactivePlugins) {
			if (!PluginManager::isPluginActive('CurriculaApp')) {
				$criteria->addCondition('report_type_id != ' . LearningReportType::USERS_COURSEPATH, 'AND');
			}
			if (!PluginManager::isPluginActive('GamificationApp')) {
				$criteria->addCondition('report_type_id != ' . LearningReportType::USERS_BADGES, 'AND');
			}
			if (!PluginManager::isPluginActive('Share7020App')) {
				$criteria->addCondition('report_type_id != ' . LearningReportType::APP7020_ASSETS, 'AND');
				$criteria->addCondition('report_type_id != ' . LearningReportType::APP7020_EXPERTS, 'AND');
				$criteria->addCondition('report_type_id != ' . LearningReportType::APP7020_CHANNELS, 'AND');
			}
			if (!PluginManager::isPluginActive('EcommerceApp')) {
				$criteria->addCondition('report_type_id != '. LearningReportType::ECOMMERCE_TRANSACTIONS);
			}
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config = array();
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['pagination'] = false;

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function renderExportActions() {
		$popoverContent = Yii::app()->getController()->renderPartial('_reportExportActions', array(
			'id' => $this->id_filter,
		), true);
		$content = Chtml::link('', 'javascript:void(0);', array(
			'id' => 'popover-' . uniqid(),
			'class' => 'popover-action popover-trigger report-export',
			'data-toggle' => 'popover',
			'data-content' => $popoverContent,
			"rel" => "tooltip",
			"title" => Yii::t("standard", "_EXPORT"),
		));

		return $content;
	}

	public function renderStandardReportActions() {
		$standardReportActions = Yii::app()->getController()->renderPartial('_standardReportActions2', array('model' => $this), true);
		$content = Chtml::link('export', 'javascript:void(0);', array(
			'id' => 'popover-' . get_class($this) . '-' . $this->id_filter,
			'class' => 'popover-action popover-trigger report-export',
			'data-toggle' => 'popover',
			'data-content' => $standardReportActions,
			"rel" => "tooltip",
			"title" => Yii::t("standard", "_EXPORT"),
		));
		return $content;
	}

	public static function getSessionAttributes() {
		if (isset(Yii::app()->session['reportAttributes'])) {
			return Yii::app()->session['reportAttributes'];
		} else {
			return array();
		}
	}

	public static function setSessionAttributes(array $data) {
		if (!isset(Yii::app()->session['reportAttributes'])) {
			Yii::app()->session['reportAttributes'] = array();
		}

		// Yii::app()->session['reportAttributes'] += $data;
		$attributes = Yii::app()->session['reportAttributes'];
		if (!empty($data['fields'])) {
			$attributes['fields'] = array();
		}
		if (!empty($data['courses'])) {
			$attributes['courses'] = array();
			$attributes['gridData']['select-course-grid-checkboxes'] = array();
			$attributes['gridData']['select-course-grid-selected-items'] = array();
		}
		if (!empty($data['users'])) {
			$attributes['users'] = array();
			$attributes['gridData']['select-user-grid-checkboxes'] = array();
			$attributes['gridData']['select-group-grid-checkboxes'] = array();
			$attributes['gridData']['select-orgchart'] = array();
			$attributes['gridData']['select-user-grid-selected-items'] = array();
			$attributes['gridData']['select-group-grid-selected-items'] = array();
		}
		Yii::app()->session['reportAttributes'] = $attributes;


		// Merging session & new incoming data; this is a good idea, to allow "next/previous" steps navigation and keep what has been selected so far.
		// However, the above =array() expressions actually neglects this approach (?!)
		// I keep it, because I don't want to interfere with the original author's (Quartz) idea [plamen]
		$forSession = CMap::mergeArray($attributes, $data);

		// Wee need to get UNIQUE values for some filter data to avoid multiple values like
		// [filters][lo_type] = array('file', 'file', 'file', 'file')
		if (isset($forSession['filters']['lo_type']) && is_array($forSession['filters']['lo_type'])) {
			$forSession['filters']['lo_type'] = array_unique($forSession['filters']['lo_type']);
		}
		if (isset($forSession['filters']['milestone']) && is_array($forSession['filters']['milestone'])) {
			$forSession['filters']['milestone'] = array_unique($forSession['filters']['milestone']);
		}

		// Save data to session
		Yii::app()->session['reportAttributes'] = $forSession;

		return true;
	}

	public static function clearSessionAttributes() {
		unset(Yii::app()->session['reportAttributes']);
		unset(Yii::app()->session['selectedItems']);
	}

	/**
	 * Build SQL Data Data Provider for Groups-Courses custom report.
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderGroupsCourses($pagination = false, $customFilter = false) {

		// Resolve groups from filter data
		$groups = $this->getGroupsFromFilter();

		// Get selected users from filter
		// NOTE: for this report, USERS doesn't count!!!
		// $users  = $this->getUsersFromFilter();
		// Join Org Chart tables to get OrgChart names etc., if ANY (left join)
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		// This is base SQL builder
		$commandBase = Yii::app()->db->createCommand()
			->from('core_group t')
			->join('core_group_members group_member', 't.idst=group_member.idst')
			->join('core_user user', 'group_member.idstMember = user.idst')
			->join('learning_courseuser enrollment', 'user.idst = enrollment.idUser')
			->join('learning_course course', 'enrollment.idCourse = course.idCourse');


		// This is needed to get Org Chart names
		$commandBase
			->leftJoin('core_org_chart_tree chart_tree', '(t.idst = chart_tree.idst_oc)')
			->leftJoin('core_org_chart chart', '( (chart_tree.idOrg = chart.id_dir) AND (chart.lang_code=\'' . $languageCode . '\'))');


		// Filter by groups, resolved from saved filter, if any (these are explicitely selected groups/orgcharts)
		if (count($groups) > 0) {
			$groupsList = implode(',', $groups);
			$commandBase->where('t.idst IN (' . $groupsList . ')');
		}

		// Power User filtering
		// Tricky part. The idea is to filter OUT all groups that are NOT assigned to PU (either as Orgchart or as normal group)
		if (Yii::app()->user->getIsAdmin()) {

			// Filter out users NOT assigned to the current PU (checking the **resulting** table which should uptodate to work!!!
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");

			// FIlter out courses NOT assigned to PU
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");

			// Now, get ALL groups assigned to PU (as Org Chart nodes or as regular groups)
			$puOrgChartNodes = CoreAdminTree::getPowerUserOrgChartsOnly(Yii::app()->user->id, false);
			$puGroups = array();

			// Groups, resulting from assigned Org Charts
			if (isset($puOrgChartNodes['nodeObjects']) && count($puOrgChartNodes['nodeObjects']) > 0) {
				foreach ($puOrgChartNodes['nodeObjects'] as $node) {
					$puGroups[] = (int) $node['idst_oc'];
				}
			}

			// Get Normal groups IDs
			$puNormalGroupsInfo = CoreAdminTree::getPowerUserGroups(Yii::app()->user->id);
			foreach ($puNormalGroupsInfo as $groupIdst => $info) {
				$puGroups[] = $groupIdst;
			}

			// If there is NO groups assigned, we zero the result!
			if (count($puGroups) > 0) {
				$commandBase->andWhere('t.idst IN (' . implode(',', $puGroups) . ')');
			} else {
				$commandBase->andWhere('1=0');
			}
		}



		// Filter out groups that are 'hidden' and are NOT Org chart based group
		$commandBase->andWhere("(t.hidden='false') OR (chart_tree.idOrg IS NOT NULL)");

		// Other filters
		// .....
		// Filter by selected courses, if any
		$courses = $this->getCoursesFromFilter();
		if (count($courses) > 0) {
			$coursesList = implode(',', $courses);
			$commandBase->andWhere("course.idCourse IN ($coursesList)");
		}


		// Grouping
		$commandBase->group(array('t.idst', 'course.idCourse'));


		// Now, the Tricky part: we need to COUNT GROUPED records, so we use do
		// SELECT COUNT(*) FROM (<stripped data subquery>)
		// Save the query builder we made so far to a Data Quiery builder (used later)
		$commandData = clone $commandBase;

		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('t.idst');

		//var_dump($commandBase->getText());die;
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar();

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select("

			CONCAT(t.idst, '_', enrollment.idCourse) 	as 'id',
			t.idst 										as idGroup,
			t.idst 										as idGroupG,
			t.description 								as groupDescription,
			enrollment.idCourse 						as idCourse,
			enrollment.idCourse 						as idCourseG,
			chart.translation							as orgNodeName,
			t.idst										as `" . self::F_GROUP_IDST . "`,
			t.idst										as `" . self::F_GROUP_GROUPID . "`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.course_type							as `" . self::F_COURSE_TYPE . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.status			 					as `" . self::F_COURSE_STATUS . "`,
			course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,

            course.course_type							as `".self::F_STAT_TYPE_OF_COURSE."`,
            course.idCourse								as `".self::F_STAT_INTERNAL_COURSE_ID."`,

            t.description								as `".self::F_GROUP_DESCRIPTION."`,



			IF(course.`course_type` = '" . LearningCourse::ELEARNING . "',
			    IF(course.date_end != '0000-00-00' AND course.date_end < CURDATE(), '_YES','_NO'),
			    IF(course.`course_type` = '" . LearningCourse::CLASSROOM . "',
			        IF(MAX(`ltcsd`.day) < CURDATE(), '_YES', '_NO'),
					IF(DATE(MAX(`webc`.date_end)) < CURDATE(), '_YES', '_NO')))as `" . self::F_COURSE_EXPIRED . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,

			count(distinct enrollment.idCourse,enrollment.idUser)
				 										as `" . self::F_STAT_TOTAL_SUBSCRIBED_USERS . "`,

			coreMemCount.cnt
														as `" . self::F_GROUP_TOTAL_USERS . "`,

			TRIM(LEADING '/' FROM t.groupid)			as clean_groupid,

			(CASE WHEN chart.translation IS NOT NULL THEN CONCAT('(OC) ', chart.translation) ELSE CONCAT('(G) ', TRIM(LEADING '/' FROM t.groupid)) END)
														as `" . self::F_GROUP_NAME . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = 0 THEN enrollment.idUser ELSE NULL END) as `" . self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = 1 THEN enrollment.idUser ELSE NULL END) as `" . self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = 2 THEN enrollment.idUser ELSE NULL END) 	as `" . self::F_STAT_NUMBER_USERS_COMPLETED_COURSE . "`,



			SUM(`time_spent`.`total_time`) AS `" . self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE . "`

		");

		//Join all session and webinar dates se we can calculate if course has expired or not
		$commandData->leftJoin('lt_course_session ltcs', '(ltcs.course_id = course.idCourse)');
		$commandData->leftJoin('lt_course_session_date ltcsd', '(ltcsd.id_session = ltcs.id_session)');
		$commandData->leftJoin('webinar_session webc', '(webc.course_id = course.idCourse)');

		// Join Course category table to get categor names
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');
		$commandData->join("(SELECT COUNT(idstMember) AS cnt, idst FROM " . CoreGroupMembers::model()->tableName() . " GROUP BY idst) AS coreMemCount", " coreMemCount.idst = t.idst");

		// Join tracking sessions table in order to calculated total time spent in the course by users
		$commandData->leftJoin("(SELECT `idUser`, `idCourse`, SUM(UNIX_TIMESTAMP(`lastTime`) - UNIX_TIMESTAMP(`enterTime`)) as `total_time` "
			. " FROM `" . LearningTracksession::model()->tableName() . "` "
			. " GROUP BY `idUser`, `idCourse`) `time_spent` ", "(`time_spent`.`idUser` = `user`.`idst` AND `time_spent`.`idCourse` = `course`.`idCourse`)");
// I think here is a part of the problem this "where" should be "andWhere" LB-2580
		$filterOptions = $this->getFilteringOptionsFromFilter();
		// Ordering xxx
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', clean_groupid ASC, course.name';
		} else {
			$commandData->order = "clean_groupid ASC, course.name";
		}

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		);


		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users - Learning Plan (Coursepath) custom report.
	 *
	 * @param bool $pagination
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersCoursepath($pagination = false, $customFilter = false) {
		// Get users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		// Get selected plans
		$plans = $this->getPlansFromFilter();
		
		$params = array();

		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		// Build a subquery in order to sort users correctly by status
        $subquery = Yii::app()->db->createCommand()
            ->from('learning_courseuser course')
            ->select('MAX(status) AS status, idUser')
            ->leftJoin('learning_coursepath_courses lcc', 'lcc.id_item=course.idCourse');
		
		if(!empty($users)) {
			$subquery->where('course.idUser IN (' . implode(',', $users) . ')');
		}
		$subquery->group('course.idUser');
		$commandBase = Yii::app()->db->createCommand()
			->from('learning_coursepath_user planUser')
			->join('learning_coursepath plan', 'plan.id_path=planUser.id_path')
			->join('core_user user', 'user.idst=planUser.idUser')
            ->join('(' . $subquery->getText() . ') course', 'planUser.idUser=course.idUser');

		// PU filters
		if (Yii::app()->user->getIsPu()) {
			// Filter out users NOT assigned to the current PU (checking the **resulting** table which should uptodate to work!!!
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");

			// FIlter out coursepath NOT assigned to PU
			$commandBase->join(CoreUserPuCoursepath::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.path_id = plan.id_path");
		}

		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');

		if ((count($users) > 0) && (count($groups) > 0)) {
			$commandBase->where('user.idst IN (' . implode(',', $users) . ') OR group_member.idst IN (' . implode(',', $groups) . ')');
		} elseif (count($users) > 0) {
			$commandBase->where('user.idst IN (' . implode(',', $users) . ')');
		} elseif (count($groups) > 0) {
			$commandBase->where('group_member.idst IN (' . implode(',', $groups) . ')');
		}


		// Filter by selected courses, if any
		if (count($plans) > 0) {
			$plansList = implode(',', $plans);
			$commandBase->andWhere("plan.id_path IN ($plansList)");
		}

		// MORE FILTERS
		$filterOptions = $this->getFilteringOptionsFromFilter();

		$progressFilter = ( ($filterOptions['subscription_status'] == 'not_started') || ($filterOptions['subscription_status'] == 'in_progress') || ($filterOptions['subscription_status'] == 'completed')) ? true : false;

		if ($progressFilter) {
			$commandStatusCheck = Yii::app()->db->createCommand()
				->from('learning_coursepath_user planUser')
				->select("*");
			$commandStatusCheck->order = "idUser ASC";
			$result = $commandStatusCheck->query();
			$finalWhere = '(';
			for ($i = 0; $i < $result->count(); $i++) {
				$result->next();
				$row = $result->current();
				$res = LearningCoursepath::model()->getCoursepathPercentage($row['idUser'], $row['id_path']);
				if (($filterOptions['subscription_status'] == 'completed' && $res['percentage'] >= 100) || ($filterOptions['subscription_status'] == 'not_started' && $res['percentage'] == 0 ) || ($filterOptions['subscription_status'] == 'in_progress' && $res['percentage'] > 0 && $res['percentage'] < 100)){
					$finalWhere .= "(user.idst,plan.id_path)=(" . $row['idUser'] . "," . $row['id_path'] . ") OR ";
				}
			}

			$finalWhere = substr($finalWhere, 0, strlen($finalWhere) - 4);
			$finalWhere .= ')';
			$commandBase->andWhere($finalWhere);
		}

		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('user.idst', 'plan.id_path'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');

		
		/*		 * *********** COUNTER ****************** */
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		

		/*		 * *********** DATA ****************** */

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select(
		    ($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") .
			"
            group_concat(group_member.idst)			as `" . self::F_USER_GROUPS_LIST . "`,
			CONCAT(user.idst, '_', plan.id_path) 	as `id`,

			user.idst									as `user.idUser`,
			user.idst									as `" . self::F_USER_ID . "`,
			user.userid									as `" . self::F_USER_USERID . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END) as `" . self::F_USER_SUSPEND_DATE . "`,
			plan.path_code                              as `" . self::F_COURSEPATH_CODE . "`,
			plan.path_name                              as `" . self::F_COURSEPATH_NAME . "`,
			plan.credits                                as `" . self::F_COURSEPATH_CREDITS . "`,
			planUser.date_assign                        as `" . self::F_COURSEPATH_SUBSCRIPTION_DATE . "`,
			plan.id_path                                as `" . self::F_COURSEPATH_COMPLETION_DATE . "`,
			plan.id_path                                as `" . self::F_COURSEPATH_ID . "`,
			user.idst                                   as `" . self::F_COURSEPATH_COMPLETION_STATUS . "`,
			1                                           as `" . self::F_COURSEPATH_COMPLETION_PERCENT . "`,
			course.status                               as `course_completion.status`
		");

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', plan.path_code ASC' . ', course.status' . ' ' . $filterOptions['order']['type'];
		} else {
			$commandData->order = "plan.path_code ASC";
		}

		// Log::_($commandData->getText());die;
		
		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params' => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users-Courses custom report.
	 *
	 * @param bool $pagination
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersCourses($pagination = false, $customFilter = false) {

		// Get users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		// Get selected courses
		$courses = $this->getCoursesFromFilter();

		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		
		$params = array();

		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser enrollment')
			->join('core_user user', 'user.idst=enrollment.idUser')
			->join('learning_course course', 'enrollment.idCourse=course.idCourse');

		// No need for LEFT join, every user is a member of at least one group or orgchart!
		$commandBase->join('core_group_members group_member', 'user.idst=group_member.idstMember');

		Yii::app()->event->raise('AddAdditionalJoinForCustomTableData', new DEvent($this, array('commandBase' => &$commandBase)));

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");
		}

		if (count($groups) > 0 && count($users) > 0) {
			$tmpGroupTableName = 'tmp_selected_groups';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpGroupTableName;
				CREATE TEMPORARY TABLE $tmpGroupTableName (idst int(11) NOT NULL, PRIMARY KEY(idst), INDEX(idst));
				INSERT INTO $tmpGroupTableName VALUES (" . implode('),(', $groups) . ")
			")->execute();
			$tmpUsersTableName = 'tmp_selected_users';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpUsersTableName;
				CREATE TEMPORARY TABLE $tmpUsersTableName (idst int(11) NOT NULL, PRIMARY KEY(idst), INDEX(idst));
				INSERT INTO $tmpUsersTableName VALUES (" . implode('),(', $users) . ")
			")->execute();

			$commandBase->leftJoin($tmpGroupTableName . ' FORCE INDEX FOR JOIN (idst)', "group_member.idst = $tmpGroupTableName.idst");
			$commandBase->leftJoin($tmpUsersTableName . ' FORCE INDEX FOR JOIN (idst)', "user.idst = $tmpUsersTableName.idst");

			$commandBase->andWhere("$tmpGroupTableName.idst IS NOT NULL OR $tmpUsersTableName.idst IS NOT NULL");
		} elseif (count($groups) > 0) {// Filter by groups
			$tmpGroupTableName = 'tmp_selected_groups';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpGroupTableName;
				CREATE TEMPORARY TABLE $tmpGroupTableName (idst int(11) NOT NULL, PRIMARY KEY(idst), INDEX(idst));
				INSERT INTO $tmpGroupTableName VALUES (" . implode('),(', $groups) . ")
			")->execute();
			$commandBase->join($tmpGroupTableName . ' FORCE INDEX FOR JOIN (idst)', "group_member.idst = $tmpGroupTableName.idst");
		} elseif (count($users) > 0) {// OR .. users
			$tmpUsersTableName = 'tmp_selected_users';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpUsersTableName;
				CREATE TEMPORARY TABLE $tmpUsersTableName (idst int(11) NOT NULL, PRIMARY KEY(idst), INDEX(idst));
				INSERT INTO $tmpUsersTableName VALUES (" . implode('),(', $users) . ")
			")->execute();
			$commandBase->join($tmpUsersTableName . ' FORCE INDEX FOR JOIN (idst)', "user.idst = $tmpUsersTableName.idst");
		}


		// Filter by selected courses, if any
		if (count($courses) > 0) {
			$tmpCoursesTableName = 'tmp_selected_courses';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpCoursesTableName;
				CREATE TEMPORARY TABLE $tmpCoursesTableName (idCourse int(11) NOT NULL, PRIMARY KEY(idCourse), INDEX(idCourse));
				INSERT INTO $tmpCoursesTableName VALUES (" . implode('),(', $courses) . ")
			")->execute();
			$commandBase->join($tmpCoursesTableName . ' FORCE INDEX FOR JOIN (idCourse)', "course.idCourse = $tmpCoursesTableName.idCourse");
		}

		// MORE FILTERS
		$filterOptions = $this->getFilteringOptionsFromFilter();


		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if (!empty($filterOptions['start_date']) || !empty($filterOptions['end_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));
			$sql_completion = $this->getSqlFromDateFiltersForEnrollments('end_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));

			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
			if (!empty($sql_completion))
				$commandBase->$condition($sql_completion);
		}

		if ($filterOptions['subscription_status'] == 'not_started') {
			$commandBase->andWhere("enrollment.status=" . (int) LearningCourseuser::$COURSE_USER_SUBSCRIBED);
		} else if ($filterOptions['subscription_status'] == 'in_progress') {
			$commandBase->andWhere("enrollment.status=" . (int) LearningCourseuser::$COURSE_USER_BEGIN);
		} else if ($filterOptions['subscription_status'] == 'completed') {
			$commandBase->andWhere("enrollment.status=" . (int) LearningCourseuser::$COURSE_USER_END);
		}

		// Show users that subscribed more than X Days ago
		if ($filterOptions['days_from_course']) {
			$commandBase->andWhere("enrollment.date_inscr < DATE_SUB(NOW(), INTERVAL " . (int) $filterOptions['days_from_course'] . " DAY)");
		}

		// Show courses expiring in next X Days
		if ($filterOptions['courses_expiring_in']) {
			$commandBase->andWhere(" (course.date_end != '0000-00-00') AND (course.date_end IS NOT NULL) AND course.date_end < DATE_ADD(NOW(), INTERVAL " . (int) $filterOptions['courses_expiring_in'] . " DAY)");
		}

		// Show courses that expire BEFORE certain date
		if ($filterOptions['courses_expiring_before']) {
			$commandBase->andWhere(" (course.date_end < '" . $filterOptions['courses_expiring_before'] . "') OR (course.date_end IS NULL)");
		}

		$subscriptionCodesSelect = '';
		if(PluginManager::isPluginActive('SubscriptionCodesApp')){
			$commandBase->leftJoin('subscriptioncodes_course scc', 'scc.course_id = course.idCourse');
			$commandBase->leftJoin('subscriptioncodes_code sc', 'sc.id_set = scc.id_set AND sc.used_by = user.idst');
			$commandBase->leftJoin('subscriptioncodes_set scs', 'sc.id_set = scs.id_set');

			$subscriptionCodesSelect =  " GROUP_CONCAT(DISTINCT(sc.code))				as `" . self::F_ENROLL_SUBSCRIPTION_CODE . "`,
                                            GROUP_CONCAT(DISTINCT(scs.name))			as `" . self::F_ENROLL_SUBSCRIPTION_SET . "`," ;
		}

		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);

		// Grouping
		$commandBase->group(array('user.idst', 'course.idCourse'));

		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;

		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');


		/*		 * *********** COUNTER ****************** */
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------

		/*		 * *********** DATA ****************** */
		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "").
			"
			group_concat(group_member.idst) 			as `" . self::F_USER_GROUPS_LIST . "`,

			CONCAT(user.idst, '_', course.idCourse) 	as `id`,
			user.idst                                   as idst,
			user.idst                                   as `user.idst`,
			user.idst									as `user.idUser`,
			user.userid									as `" . self::F_USER_USERID . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,
			course.idCourse			 					as `" . self::F_STAT_INTERNAL_COURSE_ID . "`,
			course.course_type			 				as `" . self::F_COURSE_TYPE . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.status			 					as `" . self::F_COURSE_STATUS . "`,
            course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,
            IF(course.`course_type` = '" . LearningCourse::ELEARNING . "',
			    IF(course.date_end != '0000-00-00' AND course.date_end < CURDATE(), '_YES','_NO'),
			    IF(course.`course_type` = '" . LearningCourse::CLASSROOM . "',
			        IF((SELECT DATE(MAX(date_end)) FROM lt_course_session WHERE course_id = course.idCourse) < CURDATE(), '_YES', '_NO'),
					IF((SELECT DATE(MAX(date_end)) FROM webinar_session WHERE course_id = course.idCourse) < CURDATE(), '_YES', '_NO')))
														as `" . self::F_COURSE_EXPIRED . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,
			enrollment.idUser							as `" . self::F_ENROLL_LEVEL . "`,
			enrollment.status							as `" . self::F_ENROLL_STATUS . "`,
			enrollment.score_given						as `" . self::F_ENROLL_USER_SCORE_GIVEN . "`,
			enrollment.initial_score_given				as `" . self::F_ENROLL_USER_INITIAL_SCORE_GIVEN . "`,
			enrollment.date_inscr						as `" . self::F_ENROLL_DATE_ENROLLMENT . "`,
			enrollment.date_first_access				as `" . self::F_ENROLL_DATE_FIRST_ACCESS . "`,
			enrollment.date_last_access					as `" . self::F_ENROLL_DATE_LAST_ACCESS . "`,
			enrollment.date_complete					as `" . self::F_ENROLL_DATE_COMPLETE . "`,
            enrollment.date_begin_validity				as `" . self::F_ENROLL_DATE_BEGIN_VALIDITY . "`,
            enrollment.date_expire_validity				as `" . self::F_ENROLL_DATE_EXPIRE_VALIDITY . "`,
            
        
           ".$subscriptionCodesSelect."
			(SELECT COUNT(*) FROM learning_tracksession WHERE idCourse=course.idCourse AND idUser=user.idst)
														as `" . self::F_ENROLL_USER_NUMBER_SESSIONS . "`
				
		");


		Yii::app()->event->raise('AddAdditionalSelectRules', new DEvent($this, array('commandBase' => &$commandBase)));


		// -- ADDITIONAL joins NOT affecting filering and counting
		// Get Course category information
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', user.firstname DESC, user.lastname, course.name';
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, course.name";
		}

		
		// Log::_($commandData->getText()); die;
		
		$commandData->params = $params;
		
		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params' => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	public function sqlDataProviderUsersContests($pagination = false, $customFilter = false) {
		$users = $this->getUsersFromFilter();
		$contests = $this->getContestsFromFilter();
		$groups = $this->getGroupsFromFilter();
		
		$params = array();

		$defaultTimezone = Settings::get('timezone_default');

		$filterOptions = $this->getFilteringOptionsFromFilter();
		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(GamificationContestChart::model()->tableName() . ' ranking');
		$commandBase->join(CoreUser::model()->tableName() . ' user', 'ranking.id_user=user.idst');
		$commandBase->leftJoin(GamificationContest::model()->tableName() . ' contest', 'ranking.id_contest=contest.id AND CONVERT_TZ(contest.to_date, IFNULL(contest.timezone, "' . $defaultTimezone . '"), "+00:00") < NOW()');
		$commandBase->join(GamificationContestTranslation::model()->tableName() . ' gct', 'contest.id = gct.id_contest AND gct.lang_code="' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"');
		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
		}
		if (count($contests) > 0) {
			$commandBase->andWhere('contest.id IN (' . implode(',', $contests) . ')');
		}

		if (count($groups) > 0) {
			$commandBase->andWhere('group_member.idst IN (' . implode(',', $groups) . ')');
		}
		// OR .. users
		if (count($users) > 0) {
			if (count($groups) > 0) {
				$commandBase->orWhere('user.idst IN (' . implode(',', $users) . ')');
			} else {
				$commandBase->andWhere('user.idst IN (' . implode(',', $users) . ')');
			}
		}

		if ($filterOptions['date_from'] || $filterOptions['date_to']) {
			$dateFromFilter = $filterOptions['date_from'] ? "contest.to_date >= '" . $filterOptions['date_from'] . " 23:59:59'" : false;
			$dateToFilter = $filterOptions['date_to'] ? "contest.to_date <= '" . $filterOptions['date_to'] . " 23:59:59'" : false;

			if ($dateFromFilter || $dateToFilter) {
				$commandBase->andWhere(($dateFromFilter !== false ? $dateFromFilter : '') . ($dateFromFilter && $dateToFilter ? ' AND ' : '') . ($dateToFilter !== false ? $dateToFilter : ''));
			}
		}



		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('user.idst', 'contest.id'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');


		/*		 * *********** COUNTER ****************** */

		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------


		/*		 * *********** DATA ****************** */
		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "").
			"
			group_concat(group_member.idst) 			            as `" . self::F_USER_GROUPS_LIST . "`,
			user.idst                                   			as idst,
			user.idst                                   			as `" . self::F_USER_ID . "`,
			user.idst												as `user.idUser`,
			user.userid												as `" . self::F_USER_USERID . "`,
			user.expiration											as `" . self::F_USER_EXPIRATION . "`,
			user.email_status										as `" . self::F_USER_EMAIL_STATUS . "`,
			user.firstname											as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname											as `" . self::F_USER_LASTNAME . "`,
			user.email												as `" . self::F_USER_EMAIL . "`,
			user.register_date										as `" . self::F_USER_REGISTER_DATE . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)				as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			contest.id,
			CONCAT(contest.from_date, ',', contest.to_date)         as `" . self::F_CONTEST_PERIOD . "`,
			contest.goal                                            as `" . self::F_CONTEST_GOAL . "`,
			gct.name 									            as `" . self::F_CONTEST_NAME . "`,
			gct.description 								        as `" . self::F_CONTEST_DESCRIPTION . "`,
			ranking.ranking_result								    as `" . self::F_RANKING_GOAL_STATS . "`,
			ranking.rank                                            as `" . self::F_RANKING_POSITION . "`,
			ranking.reward_data                                     as `" . self::F_RANKING_REWARDS . "`"
		);

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', user.firstname DESC, user.lastname, gct.name';
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, gct.name";
		}

		//Log::_($commandData->getText());//die;

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params' => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	public function sqlDataProviderUsersBadges($pagination = false, $customFilter = false) {
		$users = $this->getUsersFromFilter();
		$badges = $this->getBadgesFromFilter();
		$groups = $this->getGroupsFromFilter();
		$params = array();

		$filterOptions = $this->getFilteringOptionsFromFilter();
		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(GamificationAssignedBadges::model()->tableName() . ' assignment');
		$commandBase->join(CoreUser::model()->tableName() . ' user', 'assignment.id_user=user.idst');
		$commandBase->leftJoin(GamificationBadge::model()->tableName() . ' gb', 'gb.id_badge=assignment.id_badge');
		$commandBase->join(GamificationBadgeTranslation::model()->tableName() . ' gbt', 'gb.id_badge=gbt.id_badge AND ((gbt.lang_code = "' . Settings::get('default_language') . '" AND gbt.`id_badge` NOT IN (
            SELECT gbt.`id_badge` FROM gamification_badge_translation AS gbt WHERE gbt.`lang_code` ="' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"))
            OR (gbt.`lang_code` = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"))');
		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');
		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
		}
		if (count($badges) > 0) {
			$commandBase->andWhere('gb.id_badge IN (' . implode(',', $badges) . ')');
		}

		$groupsCondition = '';
		$logicalOperator = '';
		$usersCondition = '';
		if (count($groups) > 0) {
			$groupsCondition = 'group_member.idst IN (' . implode(',', $groups) . ')';
		}
		// OR .. users
		if (count($users) > 0) {
			$usersCondition = 'user.idst IN (' . implode(',', $users) . ')';
			if (count($groups) > 0) {
				$logicalOperator = ' OR ';
			}
		}
		if ($groupsCondition != '' || $usersCondition != '') {
			$commandBase->andWhere('(' . $groupsCondition . $logicalOperator . $usersCondition . ')');
		}

		if ($filterOptions['date_from'] || $filterOptions['date_to']) {
			$dateFromFilter = $filterOptions['date_from'] ? "assignment.issued_on >= '" . $filterOptions['date_from'] . " 00:00:00'" : false;
			$dateToFilter = $filterOptions['date_to'] ? "assignment.issued_on <= '" . $filterOptions['date_to'] . " 23:59:59'" : false;

			if ($dateFromFilter || $dateToFilter) {
				$commandBase->andWhere(($dateFromFilter !== false ? $dateFromFilter : '') . ($dateFromFilter && $dateToFilter ? ' AND ' : '') . ($dateToFilter !== false ? $dateToFilter : ''));
			}
		}

		
		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('assignment.id_user', 'assignment.id_badge', 'assignment.issued_on'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');


		/*		 * *********** COUNTER ****************** */

		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------




		/*		 * *********** DATA ****************** */

		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "").
			"
			group_concat(group_member.idst) 			            as `" . self::F_USER_GROUPS_LIST . "`,
			user.idst                                   				as idst,
			user.idst                                   				as `user.idst`,
			user.idst													as `user.idUser`,
			user.userid												as `" . self::F_USER_USERID . "`,
			user.expiration											as `" . self::F_USER_EXPIRATION . "`,
			user.email_status										as `" . self::F_USER_EMAIL_STATUS . "`,
			user.firstname											as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname											as `" . self::F_USER_LASTNAME . "`,
			user.email												as `" . self::F_USER_EMAIL . "`,
			user.register_date										as `" . self::F_USER_REGISTER_DATE . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)				as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			gb.id_badge,
			gbt.name 									            as `" . self::F_BADGES_NAME . "`,
			gbt.description 								        as `" . self::F_BADGES_DESC . "`,
			gb.icon									                as `" . self::F_BADGES_ICON . "`,
			gb.score											    as `" . self::F_BADGES_SCORE . "`,
			assignment.issued_on                                           as `" . self::F_BADGES_ISSUED_ON . "`,
			assignment.event_key											as `" . self::F_BADGES_ASSIGNMENT_MODE . "`,
			assignment.event_params"
		);

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', user.firstname DESC, user.lastname, gbt.name';
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, gbt.name";
		}

		//Log::_($commandData->getText());die;

		// Data provider config
		$config = array(
				'totalItemCount' => $numRecords,
				'pagination' => $pagination,
		        'params'      => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;


	}

	public function sqlDataProviderUsersCertifications($pagination = false, $customFilter = false) {
		$users = $this->getUsersFromFilter();
		$certifications = $this->getCertificationFromFilter();
		$groups = $this->getGroupsFromFilter();
		$params = array();

		$filterOptions = $this->getFilteringOptionsFromFilter();

		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(CertificationUser::model()->tableName() . ' ceu');
		$commandBase->join(CertificationItem::model()->tableName() . ' ci', 'ci.id = ceu.id_cert_item');
		$commandBase->join(Certification::model()->tableName() . ' certification', 'certification.id_cert = ci.id_cert');
		$commandBase->join(CoreUser::model()->tableName() . ' user', 'ceu.id_user = user.idst' . ($filterOptions['show_suspended_users'] == 1 ? '' : ' AND user.valid = ' . CoreUser::STATUS_VALID));

		$commandBase->leftJoin(LearningCourse::model()->tableName() . ' lc', 'ci.id_item = lc.idCourse');
		$commandBase->leftJoin(LearningCoursepath::model()->tableName() . ' lcp', 'ci.id_item = lcp.id_path');

		if (PluginManager::isPluginActive("TranscriptsApp"))
			$commandBase->leftJoin(TranscriptsRecord::model()->tableName() . ' tr', 'ci.id_item = tr.id_record');

		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
		}

		if (count($certifications) > 0) {
			$commandBase->andWhere('ci.id_cert IN (' . implode(',', $certifications) . ')');
		}

		$commandBase->andWhere('certification.deleted = 0 ');
		if (count($groups) > 0) {
			$commandBase->andWhere('group_member.idst IN (' . implode(',', $groups) . ')');
		}
		// OR .. users
		if (count($users) > 0) {
			if (count($groups) > 0) {
				$commandBase->orWhere('user.idst IN (' . implode(',', $users) . ')');
			} else {
				$commandBase->andWhere('user.idst IN (' . implode(',', $users) . ')');
			}
		}

		$dateNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if ($filterOptions['certification_status'] == 'active') {
			$commandBase->andWhere('ceu.on_datetime <= "' . $dateNow . '" AND ceu.expire_at > "' . $dateNow . '"');
		} else if ($filterOptions['certification_status'] == 'expired') {
			$commandBase->andWhere('ceu.expire_at < "' . $dateNow . '"');
		}

		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if (!empty($filterOptions['start_date']) || !empty($filterOptions['end_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('ceu.on_datetime', 'ceu.expire_at'));
			$sql_completion = $this->getSqlFromDateFiltersForEnrollments('end_date', $filterOptions, array('ceu.on_datetime', 'ceu.expire_at'));

			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
			if (!empty($sql_completion))
				$commandBase->$condition($sql_completion);
		}


		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('ceu.id'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');
		

		/*		 * *********** COUNTER ****************** */

		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------



		/*		 * *********** DATA ****************** */

		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "").
			"
			group_concat(group_member.idst) 			as `" . self::F_USER_GROUPS_LIST . "`,
			user.idst                                   				as idst,
			user.idst                                   				as `user.idst`,
			user.idst													as `user.idUser`,
			user.userid												as `" . self::F_USER_USERID . "`,
			user.expiration											as `" . self::F_USER_EXPIRATION . "`,
			user.email_status										as `" . self::F_USER_EMAIL_STATUS . "`,
			user.firstname											as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname											as `" . self::F_USER_LASTNAME . "`,
			user.email												as `" . self::F_USER_EMAIL . "`,
			user.register_date										as `" . self::F_USER_REGISTER_DATE . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)				as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			certification.title 									as `" . self::F_CERTIFICATION_NAME . "`,
			certification.description 								as `" . self::F_CERTIFICATION_DESCRIPTION . "`,
			certification.duration									as `" . self::F_CERTIFICATION_EXPIRATION . "`,
			certification.duration_unit,
			ci.item_type											as `" . self::F_CERTIFICATION_ACTIVITY_TYPE . "`,
			CASE WHEN ci.item_type = 'course' THEN lc.name
			WHEN ci.item_type = 'plan' THEN lcp.path_name"
			. (PluginManager::isPluginActive("TranscriptsApp") ? (" WHEN ci.item_type = 'transcript' THEN tr.course_name END as `" . self::F_CERTIFICATION_ACTIVITY_NAME . "`,") : " END,") .
			"ceu.on_datetime                                         as `" . self::F_CERTIFICATION_ISSUED_ON . "`,
			ceu.expire_at                                           as `" . self::F_CERTIFICATION_RENEW_IN . "`

		");

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'];
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, certification.title";
		}

		// Data provider config
		$config = array(
				'totalItemCount' => $numRecords,
				'pagination' => $pagination,
		        'params'      => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;

	}

	public function sqlDataProviderCertificationsUsers($pagination = false, $customFilter = false) {
		$users = $this->getUsersFromFilter();
		$certifications = $this->getCertificationFromFilter();
		$groups = $this->getGroupsFromFilter();

		$filterOptions = $this->getFilteringOptionsFromFilter();

		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(CertificationUser::model()->tableName() . ' ceu');
		$commandBase->join(CertificationItem::model()->tableName() . ' ci', 'ci.id = ceu.id_cert_item');
		$commandBase->join(Certification::model()->tableName() . ' certification', 'certification.id_cert = ci.id_cert');
		$commandBase->join(CoreUser::model()->tableName() . ' cu', 'ceu.id_user = cu.idst' . ($filterOptions['show_suspended_users'] == 1 ? '' : ' AND cu.valid = ' . CoreUser::STATUS_VALID));

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND cu.idst = pu.user_id");
		}

		if (count($certifications) > 0) {
			$commandBase->andWhere('ci.id_cert IN (' . implode(',', $certifications) . ')');
		}
		$commandBase->andWhere('certification.deleted = 0 ');

		// Filter by groups
		if (count($groups) > 0) {
			$cgmTable = '(
				SELECT idstMember
				FROM core_group_members
				WHERE idst IN (' . implode(',', $groups) . ')
				GROUP BY idstMember
			) gm';
			$commandBase->join($cgmTable, 'gm.idstMember = cu.idst');
		}
		// OR .. users
		if (count($users) > 0) {
			if (count($groups) > 0) {
				$commandBase->orWhere('cu.idst IN (' . implode(',', $users) . ')');
			} else {
				$commandBase->andWhere('cu.idst IN (' . implode(',', $users) . ')');
			}
		}

		$dateNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		if ($filterOptions['certification_status'] == 'active') {
			$commandBase->andWhere('ceu.on_datetime <= "' . $dateNow . '" AND ceu.expire_at > "' . $dateNow . '"');
		} else if ($filterOptions['certification_status'] == 'expired') {
			$commandBase->andWhere('ceu.expire_at < "' . $dateNow . '"');
		}

		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if (!empty($filterOptions['start_date']) || !empty($filterOptions['end_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('ceu.on_datetime', 'ceu.expire_at'));
			$sql_completion = $this->getSqlFromDateFiltersForEnrollments('end_date', $filterOptions, array('ceu.on_datetime', 'ceu.expire_at'));

			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
			if (!empty($sql_completion))
				$commandBase->$condition($sql_completion);
		}

		$commandBase->group(array('certification.id_cert'));
		/*		 * *********** COUNTER ****************** */
		// Create a brand new counter query and use the base SQL as FROM subquery.
		// Non filtering SQL should be LEFT for later addition to rel DATA query (optimization)
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(distinct(`certification`.`id_cert`))');
		$commandCounter->group = '';

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar();
		$commandBase->select("
			certification.id_cert 													as `id`,
			certification.id_cert 													as `" . self::F_CERTIFICATION_ID . "`,
			certification.title 													as `" . self::F_CERTIFICATION_NAME . "`,
			certification.description 												as `" . self::F_CERTIFICATION_DESCRIPTION . "`,
			certification.duration													as `" . self::F_CERTIFICATION_EXPIRATION . "`,
			certification.duration_unit,
			count(certification.id_cert)											as `" . self::F_CERTIFICATION_ISSUED . "`,
			SUM(CASE WHEN certification.duration != 0 AND ceu.expire_at <= '" . $dateNow . "' THEN 1 ELSE 0 END)	as `" . self::F_CERTIFICATION_EXPIRED . "`,
			SUM(CASE WHEN ceu.on_datetime < '" . $dateNow . "' AND ceu.expire_at > '" . $dateNow . "'  THEN 1 ELSE 0 END)	as `" . self::F_CERTIFICATION_ACTIVE . "`
		");

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandBase->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'];
		} else {
			$commandBase->order = "certification.id_cert";
		}


		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination
		);

		$sql = $commandBase->getText();
		$dataProvider = new CSqlDataProvider($sql, $config);

		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users-Session custom report.
	 *
	 * @param bool $pagination
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersSessions($pagination = false, $customFilter = false) {

		// Get users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		// Get selected courses
		$courses = $this->getCoursesFromFilter();
		
		$params = array();

		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser enrollment')
			->join('core_user user', 'user.idst=enrollment.idUser')
			->join('learning_course course', 'enrollment.idCourse=course.idCourse');

		// No need for LEFT join, every user is a member of at least one group or orgchart!
		$commandBase->join('core_group_members group_member', 'user.idst=group_member.idstMember');

		//Session
		$commandBase->join('lt_courseuser_session cus', 'cus.id_user = user.idst');
		$commandBase->join('lt_course_session cs', 'cs.id_session = cus.id_session AND cs.`course_id` = course.`idCourse`');

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");
		}

		// Filter for ILT Courses only
		$commandBase->where("course.course_type = '" . LearningCourse::CLASSROOM . "' ");

		// Filter by groups
		if (count($groups) > 0) {
			$commandBase->andWhere('group_member.idst IN (' . implode(',', $groups) . ')');
		}
		// OR .. users
		if (count($users) > 0) {
			if (count($groups) > 0) {
				$commandBase->orWhere('user.idst IN (' . implode(',', $users) . ')');
			} else {
				$commandBase->andWhere('user.idst IN (' . implode(',', $users) . ')');
			}
		}

		// Filter by selected courses, if any
		if (count($courses) > 0) {
			$coursesList = implode(',', $courses);
			$commandBase->andWhere("course.idCourse IN ($coursesList)");
		}

		// MORE FILTERS
		$filterOptions = $this->getFilteringOptionsFromFilter();

		if (!empty($filterOptions['start_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));
			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
		}


		if ($filterOptions['subscription_status'] == 'not_started') {
			$commandBase->andWhere("enrollment.status=" . (int) LearningCourseuser::$COURSE_USER_SUBSCRIBED);
		} else if ($filterOptions['subscription_status'] == 'in_progress') {
			$commandBase->andWhere("enrollment.status=" . (int) LearningCourseuser::$COURSE_USER_BEGIN);
		}

		// Show users that subscribed more than X Days ago
		if ($filterOptions['days_from_course']) {
			$commandBase->andWhere("enrollment.date_inscr < DATE_SUB(NOW(), INTERVAL " . (int) $filterOptions['days_from_course'] . " DAY)");
		}

		// Show courses expiring in next X Days
		if ($filterOptions['courses_expiring_in']) {
			$commandBase->andWhere(" (course.date_end != '0000-00-00') AND (course.date_end IS NOT NULL) AND course.date_end < DATE_ADD(NOW(), INTERVAL " . (int) $filterOptions['courses_expiring_in'] . " DAY)");
		}

		// Show courses that expire BEFORE certain date
		if ($filterOptions['courses_expiring_before']) {
			$commandBase->andWhere(" (course.date_end < '" . $filterOptions['courses_expiring_before'] . "') OR (course.date_end IS NULL)");
		}

		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('user.idst', 'cs.id_session'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');
		
		
		/*		 * *********** COUNTER ****************** */
		// Create a brand new counter query and use the base SQL as FROM subquery.
		// Non filtering SQL should be LEFT for later addition to rel DATA query (optimization)
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		

		/*		 * *********** DATA ****************** */

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select(
		    ($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") .
			"
			group_concat(group_member.idst) 			as `" . self::F_USER_GROUPS_LIST . "`,

			CONCAT(user.idst, '_', course.idCourse) 	as `id`,

			user.idst									as `user.idUser`,
			user.idst									as `" . self::F_USER_ID . "`,
			user.userid									as `" . self::F_USER_USERID . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.course_type							as `" . self::F_COURSE_TYPE . "`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			course.idCourse								as `" . self::F_STAT_INTERNAL_COURSE_ID . "`,
			course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			user.suspend_date							as `" . self::F_USER_SUSPEND_DATE . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,
			enrollment.level							as `" . self::F_ENROLL_LEVEL . "`,
			enrollment.status							as `" . self::F_ENROLL_STATUS . "`,
			enrollment.date_inscr						as `" . self::F_ENROLL_DATE_ENROLLMENT . "`,
			cs.id_session											as `session.id_session`,
			cs.name													as `" . self::F_COURSEUSER_SESSION_NAME . "`,
			cs.score_base											as `" . self::F_COURSEUSER_SESSION_SCORE_BASE . "`,
			cs.date_begin						 					as `" . self::F_COURSEUSER_SESSION_DATE_BEGIN . "`,
			cs.date_end							 					as `" . self::F_COURSEUSER_SESSION_DATE_END . "`,
			cs.total_hours						 					as `" . self::F_COURSEUSER_SESSION_TOTAL_HOURS . "`,
			CONCAT(cus.evaluation_score	, '/', cs.score_base)		as `" . self::F_COURSEUSER_SESSION_EVALUATION_SCORE . "`,
			cus.evaluation_status 									as `" . self::F_COURSEUSER_SESSION_EVALUATION_STATUS . "`,
			cus.evaluation_text					 					as `" . self::F_COURSEUSER_SESSION_EVALUATION_TEXT . "`,
			cus.attendance_hours					 				as `cus.attendance_hours`,
			CONCAT(cus.attendance_hours, 'h/', cs.total_hours, 'h') as `" . self::F_COURSEUSER_SESSION_ATTENDANCE_HOURS . "`
		");


		// Get Course category information
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', user.firstname DESC, user.lastname, course.name';
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, course.name";
		}

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params'     => $params,
		);


		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Courses-Users custom report.
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderCoursesUsers($pagination = false, $customFilter = false) {

		// Resolve groups from filter data
		$groups = $this->getGroupsFromFilter();

		// Get selected users from filter
		$users = $this->getUsersFromFilter();

		// Get selected courses
		$courses = $this->getCoursesFromFilter();

		$params = array();
		
		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser enrollment')
			->join('learning_course course', 'enrollment.idCourse=course.idCourse')
			->join('core_user user', 'enrollment.idUser=user.idst');


		// Power User filtering
		$count_pu_join = "";
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");
		}

		//var_dump($commandBase->getText());die;

		$count_condition = '';

		if (count($groups) > 0) {
			$rootOrgNode = CoreOrgChartTree::getOrgRootNode();
			if (in_array($rootOrgNode->idst_oc, $groups) || in_array($rootOrgNode->idst_ocd, $groups)) {
				$groups = array();
				$users = array();
			}
		}


		if (count($groups) > 0 && count($users) > 0) {
			$group_users = Yii::app()->db->createCommand()
				->select('idstMember')
				->from(CoreGroupMembers::model()->tableName())
				->where(array('in', 'idst', $groups))
				->queryColumn();

			$group_users = array_unique($group_users);
			$full_users = array_unique(array_merge($group_users, $users));

			$commandBase->where('enrollment.idUser IN (' . implode(',', $full_users) . ')');

			Yii::app()->session['consider_other_than_students'] = 1;
		} elseif (count($groups) > 0) {
			$group_users = Yii::app()->db->createCommand()
				->select('idstMember')
				->from(CoreGroupMembers::model()->tableName())
				->where(array('in', 'idst', $groups))
				->queryColumn();

			$group_users = array_unique($group_users);
			$commandBase->where(array('in', 'enrollment.idUser', $group_users));

		} elseif (count($users)) {
			$commandBase->where('enrollment.idUser IN (' . implode(',', $users) . ')');

			Yii::app()->session['consider_other_than_students'] = 1;
		}


		// Filter by selected courses, if any
		if (count($courses) > 0) {
			$coursesList = implode(',', $courses);
			$commandBase->andWhere("course.idCourse IN ($coursesList)");
		}

		// MORE FILTERS
		$filterOptions = $this->getFilteringOptionsFromFilter();

		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if (!empty($filterOptions['start_date']) || !empty($filterOptions['end_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));
			$sql_completion = $this->getSqlFromDateFiltersForEnrollments('end_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));

			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
			if (!empty($sql_completion))
				$commandBase->$condition($sql_completion);
		}

		// Show all enrollment levels, not only student, if this options is set
		if (!(int) $filterOptions['consider_other_than_students'] == 1) {
			$commandBase->andWhere("enrollment.level =" . LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
			Yii::app()->session['consider_other_than_students'] = $filterOptions['consider_other_than_students'];
		}

		// Show also suspended users, if this option is set
		if (!(int) $filterOptions['consider_suspended_users'] == 1) {
			$commandBase->andWhere("(user.valid=1)");
		}
		
		
		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('course.idCourse'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('course.idCourse');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		
		/*		 * *********** DATA ****************** */

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select("
			course.idCourse 							as `id`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			course.idCourse								as `" . self::F_STAT_INTERNAL_COURSE_ID . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.course_type			 				as `" . self::F_COURSE_TYPE . "`,
			course.status			 					as `" . self::F_COURSE_STATUS . "`,
			course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,
            course.social_rating_settings,
			IF(course.`course_type` = '" . LearningCourse::ELEARNING . "',
			    IF(course.date_end != '0000-00-00' AND course.date_end < CURDATE(), '_YES','_NO'),
			    IF(course.`course_type` = '" . LearningCourse::CLASSROOM . "',
			        IF(MAX(`ltcsd`.day) < CURDATE(), '_YES', '_NO'),
					IF(DATE(MAX(`webc`.date_end)) < CURDATE(), '_YES', '_NO')))as `" . self::F_COURSE_EXPIRED . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,

			COUNT(DISTINCT enrollment.idUser) AS `" . self::F_STAT_TOTAL_SUBSCRIBED_USERS . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = '" . LearningCourseuser::$COURSE_USER_SUBSCRIBED . "' THEN enrollment.idUser ELSE NULL END) AS `" . self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = '" . LearningCourseuser::$COURSE_USER_BEGIN . "' THEN enrollment.idUser ELSE NULL END) AS `" . self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE . "`,
			COUNT(DISTINCT CASE WHEN enrollment.status = '" . LearningCourseuser::$COURSE_USER_END . "' THEN enrollment.idUser ELSE NULL END) AS `" . self::F_STAT_NUMBER_USERS_COMPLETED_COURSE . "`,

			SUM(`time_spent`.`total_time`) AS `" . self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE . "`,
			lcr.rate_average							as `" . self::F_STAT_AVERAGE_EVALUATION . "`
		");

		//Join all session and webinar dates se we can calculate if course has expired or not
		$commandData->leftJoin('lt_course_session ltcs', '(ltcs.course_id = course.idCourse)');
		$commandData->leftJoin('lt_course_session_date ltcsd', '(ltcsd.id_session = ltcs.id_session)');
		$commandData->leftJoin('webinar_session webc', '(webc.course_id = course.idCourse)');

		// Get Course category information
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');

		$commandData->leftJoin("(SELECT `idUser`, `idCourse`, SUM(UNIX_TIMESTAMP(`lastTime`) - UNIX_TIMESTAMP(`enterTime`)) as `total_time` "
			. " FROM `" . LearningTracksession::model()->tableName() . "` "
			. " GROUP BY `idUser`, `idCourse`) `time_spent` ", "(`time_spent`.`idUser` = `enrollment`.`idUser` AND `time_spent`.`idCourse` = `course`.`idCourse`)");

		$commandData->leftJoin('learning_course_rating lcr', '(lcr.idCourse = course.idCourse)');
		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', course.name ASC';
		} else {
			$commandData->order = "course.name ASC";
		}

		//var_dump($commandData->getText());die;
		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params'     => $params,
		);

		// Create data provider and return to caller
		$text = $commandData->getText();
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users-Courses custom report.
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersLo($pagination = false, $customFilter = false) {

		// Get selected users from filter
		$users = $this->getUsersFromFilter();

		// Resolve groups from filter data
		$groups = $this->getGroupsFromFilter();

		// Get selected courses
		$courses = $this->getCoursesFromFilter();

		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$params = array();

		/**
		 * @var $commandBase CDbCommand
		 */
		$commandBase = Yii::app()->db->createCommand()->from('core_user user');
		$commandBase->join("learning_courseuser enrollment", "user.idst=enrollment.idUser");
		$commandBase->join('learning_course course', 'enrollment.idCourse=course.idCourse');
		$commandBase->join('core_group_members group_member', 'enrollment.idUser=group_member.idstMember');


		// Join LO and Common tracking tables
		$commandBase->join('learning_organization learning_object', '(course.idCourse=learning_object.idCourse) AND (learning_object.lev != 1)');

		// Power User filtering
		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");
		}

		if (count($users) > 0 && count($groups) > 0) {
			$commandBase->where('enrollment.idUser IN (' . implode(',', $users) . ') OR EXISTS (SELECT cgm.idst FROM core_group_members cgm WHERE (cgm.idstMember=enrollment.idUser) AND (cgm.idst IN (' . implode(',', $groups) . ')))');
		} elseif (count($users) > 0) {
			$commandBase->where('enrollment.idUser IN (' . implode(',', $users) . ')');
		} elseif (count($groups) > 0) {
			$commandBase->where('EXISTS (SELECT cgm.idst FROM core_group_members cgm WHERE (cgm.idstMember=enrollment.idUser) AND (cgm.idst IN (' . implode(',', $groups) . ')))');
		}


		// Filter by selected courses, if any
		if (count($courses) > 0) {
			$coursesList = implode(',', $courses);
			$commandBase->andWhere("course.idCourse IN ($coursesList)");
		}


		// MORE FILTERS
		$filterOptions = $this->getFilteringOptionsFromFilter();

		$condition = 'andWhere';
		if ($filterOptions['condition_status'] == 'or') {
			$condition = 'orWhere';
		}

		if (!empty($filterOptions['start_date']) || !empty($filterOptions['end_date'])) {
			$sql_enrollment = $this->getSqlFromDateFiltersForEnrollments('start_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));
			$sql_completion = $this->getSqlFromDateFiltersForEnrollments('end_date', $filterOptions, array('enrollment.date_inscr', 'enrollment.date_complete'));

			if (!empty($sql_enrollment))
				$commandBase->andWhere($sql_enrollment);
			if (!empty($sql_completion))
				$commandBase->$condition($sql_completion);
		}

		//var_dump($filterOptions);die;
		// Filter by Learning object type
		if ($filterOptions['lo_type'] && is_array($filterOptions['lo_type']) && (count($filterOptions['lo_type']) > 0)) {
			$arr = array();
			foreach ($filterOptions['lo_type'] as $index => $loType) {
				$arr[] = "'" . $loType . "'";
			}
			$loTypesList = implode(',', $arr);
			$commandBase->andWhere("learning_object.objectType IN ($loTypesList)");
		}

		// Filter by Milestone
		if ($filterOptions['milestone'] && is_array($filterOptions['milestone']) && (count($filterOptions['milestone']) > 0)) {
			$arr = array();
			foreach ($filterOptions['milestone'] as $index => $milestone) {
				//$milestone = ($milestone == 'none') ? "-"  : $milestone;
				if ($milestone == 'none') {
					$arr[] = "'-'";
					$arr[] = "''";
				} else {
					$arr[] = "'" . $milestone . "'";
				}
			}
			$milestonesList = implode(',', $arr);
			$commandBase->andWhere("learning_object.milestone IN ($milestonesList)");
		}


		
		
		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('user.idst', 'course.idCourse', 'learning_object.idOrg'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		
		/*		 * *********** DATA ****************** */


		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select(
		    ($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "").
			"
			group_concat(group_member.idst)			as `" . self::F_USER_GROUPS_LIST . "`,
			CONCAT(user.idst, '_', course.idCourse, '_', learning_object.idOrg)  as `id`,

			user.idst									as `user.idst`,
			user.idst									as `user.idUser`,
			user.userid									as `" . self::F_USER_USERID . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.course_type							as `" . self::F_COURSE_TYPE . "`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			learning_object.title						as `" . self::F_LO_TITLE . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,

			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,
			course.idCourse		 						as `" . self::F_STAT_INTERNAL_COURSE_ID . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.status			 					as `" . self::F_COURSE_STATUS . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,
			course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,

			learning_object.objectType					as `" . self::F_LO_TYPE . "`,
			(
				CASE
					WHEN course.initial_object = learning_object.idOrg AND course.final_object = learning_object.idOrg AND course.initial_score_mode = '".LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT."' AND course.final_score_mode = '".LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT."' THEN 'start - end'
					WHEN course.initial_object = learning_object.idOrg AND course.initial_score_mode = '".LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT."' THEN 'start'
					WHEN course.final_object = learning_object.idOrg AND course.final_score_mode = '".LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT."' THEN 'end'
					ELSE '-'
				END
			) as `" . self::F_LO_MILESTONE . "`,

			commontrack.firstAttempt					as `" . self::F_LO_TRACK_DATE_FIRST_ATTEMPT . "`,
			commontrack.dateAttempt						as `" . self::F_LO_TRACK_DATE_LAST_ATTEMPT . "`,
			commontrack.status							as `" . self::F_LO_TRACK_USER_STATUS . "`,
			commontrack.score							as `" . self::F_LO_TRACK_USER_SCORE . "`,
			commontrack.score_max						as `score_max`,
			learning_test.point_type					as `test_point_type`,
			enrollment.date_inscr						as `" . self::F_ENROLL_DATE_ENROLLMENT . "`,
			enrollment.date_complete					as `" . self::F_ENROLL_DATE_COMPLETE . "`,
			learning_object_version.version_name		as `" . self::F_LO_VERSION . "`

		");


		// Get Course category information
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');

		//$commandData->join('learning_commontrack commontrack', 'learning_object.idOrg = commontrack.idReference AND (commontrack.idUser=user.idst)');
		$commandData->leftJoin('learning_commontrack commontrack', 'learning_object.idOrg = commontrack.idReference AND (commontrack.idUser=user.idst)');

		//Left Join of the version
		$commandData->leftJoin('learning_repository_object_version learning_object_version', '(CASE WHEN commontrack.idResource is null THEN learning_object.idResource ELSE commontrack.idResource END) = learning_object_version.id_resource AND learning_object.objectType = learning_object_version.object_type');

		$commandData->leftJoin('learning_test learning_test', 'learning_test.idTest = learning_object.idResource AND learning_object.objectType = :objectTypeTest');
		$params['objectTypeTest'] = LearningOrganization::OBJECT_TYPE_TEST;

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', user.firstname DESC, user.lastname, course.name, learning_object.iLeft';
		} else {
			$commandData->order = "user.firstname DESC, user.lastname, course.name, learning_object.iLeft";
		}


		//var_dump($commandData->getText()); die;
		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params
		);

		// Create data provider and return to caller
		$sql = $commandData->getText();
		$dataProvider = new CSqlDataProvider($sql, $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users-Delay custom report.
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersDelay($pagination = false, $customFilter = false) {

		// Get selected users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		// Get selected courses
		$courses = $this->getCoursesFromFilter();

		// Some checks to prevent LAAAARGE results
		if (!count($users) && !count($courses)) {
			//Yii::app()->user->setFlash('error', Yii::t('standard', 'You must select some filtering criteria: users and/or courses'));
			//return false;
		}

		$params = array();
		
		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser enrollment')
			->join('core_user user', 'enrollment.idUser=user.idst')
			->join('learning_course course', 'enrollment.idCourse=course.idCourse');


		// No need for LEFT join, every user is a member of at least one group or orgchart!
		// $commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');
		$commandBase->join('core_group_members group_member', 'user.idst=group_member.idstMember');

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName() . " puc", "puc.puser_id = " . (int) Yii::app()->user->id . " AND puc.course_id = course.idCourse");
		}

		if (count($groups) > 0 && count($users) > 0) {
			$commandBase->where('group_member.idst IN (' . implode(',', $groups) . ') OR user.idst IN (' . implode(',', $users) . ')');
		} elseif (count($groups) > 0) {
			$commandBase->where('group_member.idst IN (' . implode(',', $groups) . ')');
		} elseif (count($users) > 0) {
			$commandBase->where('user.idst IN (' . implode(',', $users) . ')');
		}



		// Filter by selected courses, if any
		if (count($courses) > 0) {
			$coursesList = implode(',', $courses);
			$commandBase->andWhere("course.idCourse IN ($coursesList)");
		}

		$filterOptions = $this->getFilteringOptionsFromFilter();
		$coursesExpiringIn = isset($filterOptions['courses_expiring_in']) ? $filterOptions['courses_expiring_in'] : 10;
		if(!empty($filterOptions['courses_expiring_in'])) {
			//calculate for cases when 'Also update all enrollments expiration dates for users already enrolled in this course' is not checked
			$commandBase->andWhere("DATEDIFF(DATE_ADD(IF(course.valid_time_type = 0, enrollment.date_first_access, enrollment.date_inscr), INTERVAL course.valid_time DAY), NOW()) = " . (int) $coursesExpiringIn);
		}

		$commandBase->andWhere("
			(
				(enrollment.status = " . LearningCourseuser::$COURSE_USER_SUBSCRIBED . ")
				OR
				(enrollment.status = " . LearningCourseuser::$COURSE_USER_BEGIN . ")
			)
			AND
			(course.valid_time > 0 OR enrollment.date_expire_validity IS NOT NULL)
		");


		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('course.idCourse', 'user.idst'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('course.idCourse');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		
		/*		 * *********** DATA ****************** */

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") .
			"
			group_concat(group_member.idst)			as `" . self::F_USER_GROUPS_LIST . "`,
			CONCAT(user.idst, '_', course.idCourse) 	as `id`,
			user.idst									as `user.idUser`,
			user.idst									as `" . self::F_USER_ID . "`,
			user.userid									as `" . self::F_USER_USERID . "`,
			course.name									as `" . self::F_COURSE_NAME . "`,
			course.course_type							as `" . self::F_COURSE_TYPE . "`,
			course.idCourse								as `" . self::F_STAT_INTERNAL_COURSE_ID . "`,
			course.idCourse								as `" . self::F_COURSE_ID . "`,
			course.date_begin		 					as `" . self::F_COURSE_DATE_BEGIN . "`,
            course.date_end 		 					as `" . self::F_COURSE_DATE_END . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,
			coursecat.translation 						as `" . self::F_COURSE_CATEGORY . "`,
			course.code			 						as `" . self::F_COURSE_CODE . "`,
			course.status			 					as `" . self::F_COURSE_STATUS . "`,
			course.credits			 					as `" . self::F_COURSE_CREDITS . "`,
			enrollment.level							as `" . self::F_ENROLL_LEVEL . "`,
			enrollment.status							as `" . self::F_ENROLL_STATUS . "`,
			enrollment.date_inscr						as `" . self::F_ENROLL_DATE_ENROLLMENT . "`,
			enrollment.date_first_access				as `" . self::F_ENROLL_DATE_FIRST_ACCESS . "`,
			enrollment.date_last_access					as `" . self::F_ENROLL_DATE_LAST_ACCESS . "`,
			enrollment.date_complete					as `" . self::F_ENROLL_DATE_COMPLETE . "`,
			(SELECT COUNT(*) FROM learning_tracksession WHERE idCourse=course.idCourse AND idUser=user.idst)
														as `" . self::F_ENROLL_USER_NUMBER_SESSIONS . "`,

			-- the day/time when course MUST be/have been completed
				enrollment.date_expire_validity
														as `" . self::F_UDELAY_DATE_TO_COMPLETE . "`,

			-- days to complete
				DATEDIFF(enrollment.date_expire_validity, NOW())
														as `" . self::F_UDELAY_DAYS_LEFT_TO_COMPLETE . "`


		");

		// Get Course category information
		$commandData->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode . '\')');


		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', course.name ASC';
		} else {
			$commandData->order = "course.name ASC";
		}

		//var_dump($commandData->getText()); die;
		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params'  => $params,
		);

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Users-Notifications custom report.
	 *
	 * @param bool $pagination
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderUsersNotificationsData($pagination = false, $customFilter = false) {
		$filter = CJSON::decode($this->filter_data);

		// Get selected users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$params = array();

		$commandBase = Yii::app()->getDb()->createCommand()
			->from(InboxAppNotificationDelivered::model()->tableName() . ' delivered')
			->join(InboxAppNotification::model()->tableName() . ' notification', 'delivered.notification_id=notification.id')
			->join(CoreUser::model()->tableName() . ' user', 'delivered.user_id=user.idst');

		// Filter by groups
		if (count($groups) > 0) {
			$commandBase->andWhere(array('IN', 'group_member.idst', $groups));
		}
		$commandBase->join('core_group_members group_member', 'user.idst=group_member.idstMember');

		// OR .. users
		if (count($users) > 0) {
			$commandBase->andWhere(array('IN', 'delivered.user_id', $users));
		}


		$filterOptions = $this->getFilteringOptionsFromFilter();

		$dateFrom = isset($filter['filters']['date_from']) ? $filter['filters']['date_from'] : false;
		$dateTo = isset($filter['filters']['date_to']) ? $filter['filters']['date_to'] : false;

		if ($dateFrom) {
			$commandBase->andWhere('notification.timestamp >= :dateFrom');
			$params[':dateFrom'] = $dateFrom;
		}
		if ($dateTo) {
			$commandBase->andWhere('notification.timestamp <= :dateTo');
			$params[':dateTo'] = $dateTo;
		}

		

		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('delivered.id', 'user.idst'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('delivered.id, user.idst ');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		/*		 * *********** DATA ****************** */
		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandBase->select(
		    ($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") . 
			"
			group_concat( group_member.idst)			as `" . self::F_USER_GROUPS_LIST . "`,
			CONCAT_WS('_', delivered.id, delivered.user_id) AS id,
			user.idst									as `user.idUser`,
			user.userid									as `" . self::F_USER_USERID . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,

			notification.timestamp as `" . self::F_NOTIFICATION_TIMESTAMP . "`,
			notification.type as `" . self::F_NOTIFICATION_TYPE . "`,
            delivered.is_read as`" . self::F_NOTIFICATIONS_DELIVERED_READ . "`,
			notification.json_data as `" . self::F_NOTIFICATION_JSON . "`

		");


		// Ordering
		$commandBase->order = "notification.timestamp DESC";
		$commandBase->group(array('delivered.id', 'user.idst'));

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}

	/**
	 * Build SQL Data Data Provider for Ecommerce - Transactions custom report.
	 *
	 * @param bool $pagination
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderEcommerceData($pagination = false, $customFilter = false) {

		$filter = CJSON::decode($this->filter_data);

		$includeDeleted = isset($filter['filters']['show_deleted_too']) && $filter['filters']['show_deleted_too'];

		// Get selected users from filter
		$users = $this->getUsersFromFilter();

		// Get groups from filter (directly selected and from Orgcharts)
		$groups = $this->getGroupsFromFilter();

		$courses = $this->getCoursesFromFilter();
		$plans = $this->getPlansFromFilter();

		if (Yii::app()->user->getIsPu()) {
			$puPlans = CoreUserPuCoursepath::model()->getList(Yii::app()->user->id);
			$plans = array_intersect($plans, $puPlans);
			$puCourses = CoreUserPuCourse::model()->getList(Yii::app()->user->id);
			$courses = array_intersect($courses, $puCourses);
		}

		if (empty($courses) && empty($plans)) {
			// If no courses are selected during report creation wizard
			// this means show all courses
			$command = Yii::app()->db->createCommand();
			$command->select = 'idCourse';
			$command->from = 'learning_course';
			$courses = $command->queryColumn();

			// AND learning plans
			$command = Yii::app()->db->createCommand();
			$command->select = 'id_path';
			$command->from = 'learning_coursepath';
			$plans = $command->queryColumn();
		} elseif (empty($courses) and ! empty($plans)) {
			//only some learning plans have been selected
			$courses = array(0); //this is required to make the final query working
		}



		// Current LMS language
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$params = array();
		$commandBase = Yii::app()->getDb()->createCommand()
			->from(EcommerceTransaction::model()->tableName() . ' ecommerce')
			->join(EcommerceTransactionInfo::model()->tableName() . ' ecommerce_item', 'ecommerce.id_trans=ecommerce_item.id_trans');

		if ($includeDeleted) {
			$commandBase->leftJoin(CoreUser::model()->tableName() . ' user', 'ecommerce.id_user=user.idst');
		} else {
			$commandBase->join(CoreUser::model()->tableName() . ' user', 'ecommerce.id_user=user.idst');
		}

		$commandBase->join(CoreUserBilling::model()->tableName() . ' userBilling', 'ecommerce.billing_info_id = userBilling.id');

		// Filter by courses
		if (!empty($courses)) {
			// Get only transactions related to those courses OR transactions
			// that are not course related at all (e.g. Learning Plan related)
			if ($includeDeleted) {
				// Include also "invalid" course/LP ids, that is:
				// those that are deleted from the system
				$commandBase->andWhere('(
											course.idCourse IS NULL
											AND
											coursepath.id_path IS NULL)
										OR (
											ecommerce_item.item_type NOT IN(:type_course, :type_courseseats)
											OR
											# ecommerce_item.item_type IS of the type course/courseseats
											ecommerce_item.id_course IN (' . implode(',', $courses) . ')
										)');
			} else {
				$commandBase->andWhere('ecommerce_item.item_type NOT IN(:type_course, :type_courseseats) OR ecommerce_item.id_course IN (' . implode(',', $courses) . ')');
			}
			$params[':type_course'] = EcommerceTransactionInfo::TYPE_COURSE;
			$params[':type_courseseats'] = EcommerceTransactionInfo::TYPE_COURSESEATS;
		}

		if (empty($plans)) {
			// Exclude LP related transaction items, since not a single
			// LP was selected during report creation wizard

			$commandBase->andWhere(array('NOT IN', 'ecommerce_item.item_type', array(
				EcommerceTransactionInfo::TYPE_COURSEPATH,
			)));
		} else {
			// If some Learning Plans are selected during report creation
			// filter LP related ecommerce transaction items to those selected
			$commandBase->andWhere('ecommerce_item.item_type <> :type_learningplan OR ecommerce_item.id_path IN (' . implode(',', $plans) . ')');
			$params[':type_learningplan'] = EcommerceTransactionInfo::TYPE_COURSEPATH;
		}

		// No need for LEFT join, every user is a member of at least one group or orgchart!
		// $commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');
		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');

		// Join each ecommerce transaction item with its course OR learning plan
		// (depending on what is being purchased)
		// The third type of transaction item is "Power user purchasing seats in course"
		// and its information is derived from the "item_data_json" column
		$commandBase->leftJoin(LearningCourse::model()->tableName() . ' course', 'ecommerce_item.item_type IN (:type_course, :type_seats) AND ecommerce_item.id_course=course.idCourse');
		$commandBase->leftJoin(LearningCoursepath::model()->tableName() . ' coursepath', 'ecommerce_item.item_type=:type_learningplan AND ecommerce_item.id_path=coursepath.id_path');

		if (!$includeDeleted) {
			// Constrain only to existing/valid courses or learning plans
			$commandBase->andWhere('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL');
		}


		$params[':type_course'] = EcommerceTransactionInfo::TYPE_COURSE;
		$params[':type_learningplan'] = EcommerceTransactionInfo::TYPE_COURSEPATH;
		$params[':type_seats'] = EcommerceTransactionInfo::TYPE_COURSESEATS;

		$commandBase->leftJoin(LtCourseSession::model()->tableName() . ' classroom_session', 'ecommerce_item.id_date=classroom_session.id_session AND course.idCourse=classroom_session.course_id AND course.course_type=:classroom');
		$commandBase->leftJoin(LtCourseSessionDate::model()->tableName().' csd','classroom_session.id_session=csd.id_session');
		$commandBase->leftJoin(LtLocation::model()->tableName().' loc','csd.id_location=loc.id_location');
		$commandBase->leftJoin(CoreCountry::model()->tableName().' country','loc.id_country=country.id_country');
		$commandBase->leftJoin(WebinarSession::model()->tableName() . ' webinar_session', 'ecommerce_item.id_date=webinar_session.id_session AND course.idCourse=webinar_session.course_id AND course.course_type=:webinar');
		$params[':classroom'] = LearningCourse::TYPE_CLASSROOM;
		$params[':webinar'] = LearningCourse::TYPE_WEBINAR;


		if (!$includeDeleted) {
			// Constrain only to valid sessions
			$commandBase->andWhere('
									COALESCE(course.course_type, "") <> :classroom
									OR (
											COALESCE(course.course_type, "")=:classroom
											AND
											( # only valid/exisitng sessions
												classroom_session.id_session IS NOT NULL
												OR webinar_session.id_session IS NOT NULL
												OR ecommerce_item.id_date = 0
												OR ecommerce_item.id_edition = 0
											)
									)');
		}

		if (Yii::app()->user->getIsPu()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = :puser_id AND ecommerce.id_user = pu.user_id");
			$params[':puser_id'] = Yii::app()->user->id;
		}

		if (count($groups) > 0 && count($users) > 0) {
			$commandBase->andWhere("group_member.idst IN (" . implode(',', $groups) . ") OR ecommerce.id_user IN (" . implode(',', $users) . ")");
		} elseif (count($groups) > 0) {
			$commandBase->andWhere(array('IN', 'group_member.idst', $groups));
		} elseif (count($users) > 0) {
			$commandBase->andWhere(array('IN', 'ecommerce.id_user', $users));
		}

		$filterOptions = $this->getFilteringOptionsFromFilter();

		$dateFrom = isset($filter['filters']['date_from']) ? $filter['filters']['date_from'] : false;
		$dateTo = isset($filter['filters']['date_to']) ? $filter['filters']['date_to'] : false;

		if ($dateFrom) {
			$commandBase->andWhere('ecommerce.date_activated >= :dateFrom');
			$params[':dateFrom'] = $dateFrom;
		}
		if ($dateTo) {
			$commandBase->andWhere('ecommerce.date_activated <= :dateTo');
			$params[':dateTo'] = $dateTo;
		}

		
		
		
		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('ecommerce_item.id_trans', 'ecommerce_item.id_course', 'ecommerce_item.id_path', 'ecommerce_item.id_date', 'user.idst'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('ecommerce.id_trans');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		
		/*		 * *********** DATA ****************** */

		// get coupon info also
		$commandBase->leftJoin(EcommerceCoupon::model()->tableName().' coupon',' ecommerce.id_coupon=coupon.id_coupon');

		// Good, Now lets finish the real Data SQL (continuing the BASE one, which we used to clone from)
		$commandBase->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") . 
			"
			CONCAT_WS('_', ecommerce.id_trans, ecommerce.id_user, ecommerce_item.id_course, ecommerce_item.id_path, ecommerce_item.id_date) AS id,
			user.idst									as `user.idUser`,
			user.idst									as `" . self::F_USER_ID . "`,
			user.userid									as `" . self::F_USER_USERID . "`,
			user.firstname								as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname								as `" . self::F_USER_LASTNAME . "`,
			user.email									as `" . self::F_USER_EMAIL . "`,
			user.register_date							as `" . self::F_USER_REGISTER_DATE . "`,
			user.expiration								as `" . self::F_USER_EXPIRATION . "`,
			user.email_status							as `" . self::F_USER_EMAIL_STATUS . "`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `" . self::F_USER_SUSPENDED . "`,
			(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	as `" . self::F_USER_SUSPEND_DATE . "`,

			group_concat(group_member.idst) 			as `" . self::F_USER_GROUPS_LIST . "`,

			ecommerce_item.id_course as `idCourse`,
			ecommerce_item.id_path as `idPath`,
			ecommerce.paid as `" . self::F_ECOMMERCE_PAYMENT_STATUS . "`,
			ecommerce.date_creation as `" . self::F_ECOMMERCE_DATE_CREATED . "`,
			ecommerce.date_activated as `" . self::F_ECOMMERCE_DATE_PAID . "`,
			ecommerce.id_trans as `" . self::F_ECOMMERCE_ID_TRANSACTION . "`,
			ecommerce.discount as `" . self::F_ECOMMERCE_DISCOUNT . "`,
			ecommerce.payment_currency as `" . self::F_ECOMMERCE_PAYMENT_CURRENCY . "`,
			ecommerce.payment_type as `" . self::F_ECOMMERCE_PAYMENT_TYPE . "`,
			coupon.code as `" . self::F_ECOMMERCE_COUPON_CODE . "`,
			coupon.discount as `" . self::F_ECOMMERCE_COUPON_DISCOUNT ."`,
			coupon.discount_type as `" . self::F_ECOMMERCE_COUPON_DISCOUNT_TYPE ."`,
			coupon.description as `" . self::F_ECOMMERCE_COUPON_DESCRIPTION . "`,
			CONCAT_WS(', ',country.name_country,loc.name) as `" . self::F_ECOMMERCE_LOCATION . "`,
			ecommerce.payment_txn_id as `" . self::F_ECOMMERCE_PAYMENT_TXN_ID . "`,
			1 as `" . self::F_ECOMMERCE_ITEM_QUANTITY . "`, # if item type is 'PU seats', derive the quantity from the JSON data
			ecommerce_item.price as `" . self::F_ECOMMERCE_ITEM_SINGLE_PRICE . "`,
			ecommerce_item.code as `" . self::F_ECOMMERCE_ITEM_CODE . "`,
			ecommerce_item.name as `" . self::F_ECOMMERCE_ITEM_NAME . "`,
			userBilling.bill_address1 as `ecommerce.bill_address1`,
			userBilling.bill_address2 as `ecommerce.bill_address2`,
			userBilling.bill_city as `ecommerce.bill_city`,
			userBilling.bill_state as `ecommerce.bill_state`,
			userBilling.bill_zip as `ecommerce.bill_zip`,
			userBilling.bill_company_name as `ecommerce.bill_company_name`,
			userBilling.bill_vat_number as `ecommerce.bill_vat_number`,
			COALESCE(classroom_session.name, webinar_session.name) as `" . self::F_ECOMMERCE_ITEM_SESSION_NAME . "`,
			COALESCE(classroom_session.date_begin, webinar_session.date_begin) as `" . self::F_ECOMMERCE_ITEM_SESSION_START_DATE . "`,
			COALESCE(classroom_session.date_end, webinar_session.date_end) as `" . self::F_ECOMMERCE_ITEM_SESSION_END_DATE . "`,

			ecommerce_item.item_type as `" . self::F_ECOMMERCE_ITEM_TYPE . "`,
			ecommerce_item.item_data_json,

			# Internally used joined values
			course.name as `course.name`,
			course.course_type as `" . self::F_COURSE_TYPE . "`,
			coursepath.path_name as `coursepath.name`

		");

		// Ordering
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandBase->order = self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type'] . ', ecommerce.date_activated ASC';
		} else {
			$commandBase->order = "ecommerce.date_activated ASC";
		}

		$event = new DEvent($this, array('command' => &$commandBase));
		Yii::app()->event->raise('onBeforeGetEcommercerReportData', $event);

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}

	public function sqlDataProviderUsersExternalTrainings($pagination = false, $customFilter = false) {
		$params = array();
		$users = $this->getUsersFromFilter();
		$groups = $this->getGroupsFromFilter();
		$externalTrainings = $this->getExternalTrainingsFromFilter();

		/**
		 * @var $commandBase CDbCommand
		 */
		$filterOptions = $this->getFilteringOptionsFromFilter();

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(TranscriptsRecord::model()->tableName() . ' external_trainings');
		$commandBase->join(CoreUser::model()->tableName() . ' user', 'user.idst = external_trainings.id_user'/* . ($filterOptions['show_suspended_users'] == 1 ? '' : ' AND user.valid = ' . CoreUser::STATUS_VALID)*/); // Not checkbox for this option in wizard and not necessary
		$commandBase->leftJoin('core_group_members group_member', 'user.idst=group_member.idstMember');
		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$commandBase->join(CoreUserPU::model()->tableName() . " pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND user.idst = pu.user_id");
		}

		if (count($externalTrainings) > 0) {
			$commandBase->andWhere('external_trainings.id_record IN (' . implode(',', $externalTrainings) . ')');
		}

		if (count($groups) > 0) {
			$commandBase->andWhere('group_member.idst IN (' . implode(',', $groups) . ')');
		}
		// OR .. users
		if (count($users) > 0) {
			if (count($groups) > 0) {
				$commandBase->orWhere('user.idst IN (' . implode(',', $users) . ')');
			} else {
				$commandBase->andWhere('user.idst IN (' . implode(',', $users) . ')');
			}
		}

//    $dateNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		$condition = 'andWhere';

		if (is_array($filterOptions)) {
			if ($filterOptions['subscription_status'] == 'all') {
				$commandBase->$condition("external_trainings.status = 'approved' OR external_trainings.status = 'waiting_approval' OR external_trainings.status = 'rejected' ");
			}

			if ($filterOptions['subscription_status'] == 'approved') {
				$commandBase->$condition("external_trainings.status = 'approved'");
			}

			if ($filterOptions['subscription_status'] == 'waiting') {
				$commandBase->$condition("external_trainings.status = 'waiting_approval'");
			}

			// Show only trainings between FROM 01.09(end_date) TO 31.09(end_date)
			if ($filterOptions['date_from'])
				$commandBase->$condition("external_trainings.to_date >= '" . $filterOptions['date_from'] . " 00:00:00'");

			if ($filterOptions['date_to'])
				$commandBase->$condition("external_trainings.to_date <= '" . $filterOptions['date_to'] . " 23:59:59'");
		}


		

		/*		 * *********** USER ADDITIONAL FIELDS ****************** */
		// Left Join Core User Field Values table, if needed
		// Also, add "where" clauses to filter by additional fields
		list($userAdditionalFieldsSelect, $userAdditionalFieldsSqlParams) = $this->joinAndFilterUserAdditionalFields($commandBase, $filterOptions);
		$params = array_merge($params, $userAdditionalFieldsSqlParams);
		
		// Grouping
		$commandBase->group(array('external_trainings.id_record'));
		
		// CommandData with NO SELECT yet
		$commandData = clone $commandBase;
		
		// Avoid using "SELECT *" for counting records;  just select something, some field
		$commandBase->select('user.idst');
		
		
		/*		 * *********** COUNTER ****************** */
		
		// Create a brand new counter query and use the base SQL as FROM subquery
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		
		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($params);
		// ------------------
		
		

		/*		 * *********** DATA ****************** */
		
		$commandData->select(
			($userAdditionalFieldsSelect ? $userAdditionalFieldsSelect . "," : "") . 
			"
   			group_concat(group_member.idst) 			                            as `" . self::F_USER_GROUPS_LIST . "`,
			user.idst                                   				as idst,
			user.idst                                   				as `user.idst`,
			user.idst													as `user.idUser`,
			user.userid												    as `" . self::F_USER_USERID . "`,
			user.expiration											    as `" . self::F_USER_EXPIRATION . "`,
			user.email_status											as `" . self::F_USER_EMAIL_STATUS . "`,
			user.firstname											    as `" . self::F_USER_FIRSTNAME . "`,
			user.lastname												as `" . self::F_USER_LASTNAME . "`,
			user.email												    as `" . self::F_USER_EMAIL . "`,
			user.register_date										    as `" . self::F_USER_REGISTER_DATE . "`,

		(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	                    as `" . self::F_USER_SUSPENDED . "`,
		(CASE WHEN user.valid>0 THEN '' ELSE user.suspend_date END)	    as `" . self::F_USER_SUSPEND_DATE . "`,

		external_trainings.id_record	                                as `external_trainings.id_record`,
		external_trainings.course_name	                                as `" . self::F_EXTERNAL_TRAININGS_COURSE_NAME . "`,
		external_trainings.course_type	                                as `" . self::F_EXTERNAL_TRAININGS_COURSE_TYPE . "`,
		external_trainings.to_date	                                    as `" . self::F_EXTERNAL_TRAININGS_TO_DATE . "`,
		external_trainings.score		                                as `" . self::F_EXTERNAL_TRAININGS_SCORE . "`,
		external_trainings.credits		                                as `" . self::F_EXTERNAL_TRAININGS_CREDITS . "`,
		external_trainings.training_institute                           as `" . self::F_EXTERNAL_TRAININGS_TRAINING_INSTITUTE . "`,
		external_trainings.original_filename	                        as `" . self::F_EXTERNAL_TRAININGS_CERTIFICATE . "`,
		external_trainings.status		                                as `" . self::F_EXTERNAL_TRAININGS_STATUS . "`

    ");
		// order
		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order(self::$remappedConstants[$filterOptions['order']['orderBy']] . ' ' . $filterOptions['order']['type']);
		} else {
			$commandData->order("user.idst ASC");
		}

		//Log::_($commandData->getText());die;

		// Data provider config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
		    'params' => $params,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData->getText(), $config);
		return $dataProvider;


	}

	/**
	 * Prepare columns for Groups-Courses custpom report
	 * @return array
	 */
	public function getGridColumnsGroupsCourses() {

		$filter = CJSON::decode($this->filter_data);

		$columns = array();


		$columns = array_merge($columns, $this->getFieldsBundleColumns('course'));

		$columns = array_merge($columns, $this->getSingleColumn(self::F_GROUP_IDST));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('group'));

		// Add STAT fields; specify the "percentage base field" used for percentage calculations
		$showPercent = isset($filter['fields']['stat']['show_percents']) && ( (int) $filter['fields']['stat']['show_percents'] == 1);
		$statBaseField = $showPercent ? self::F_STAT_TOTAL_SUBSCRIBED_USERS : false;
		$columns = array_merge($columns, $this->getFieldsBundleColumns('stat', false, $statBaseField));

		$columns = $this->reorderColumns(LearningReportType::GROUPS_COURSES, $columns);
		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();
		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}
		return $columns;
	}

	/**
	 * Prepare columns for Users - Learning Plan (Coursepath) custom report
	 * @return array
	 */
	public function getGridColumnsUsersCoursepath() {
		$filter = CJSON::decode($this->filter_data);
		$columns = array();
		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('coursepaths'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('plansUsers'));

		$columns = $this->reorderColumns(LearningReportType::USERS_COURSEPATH, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		return $columns;
	}

	/**
	 * Prepare columns for Users-Courses custpom report
	 * @return array
	 */
	public function getGridColumnsUsersCourses($convertDateToUserFormat = true) {

		$filter = CJSON::decode($this->filter_data);


		$columns = array();


		$columns = array_merge($columns, $this->getFieldsBundleColumns('user', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('course', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('enrollment', false, false, $convertDateToUserFormat));




		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
			unset($columns['user.idst']);
		}



		$columns = $this->reorderColumns(LearningReportType::USERS_COURSES, $columns);


		$additionalFields = $this->getAdditionalFieldsFromFilter();

		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();

		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}

		$additionalEnrollFields = $this->getAdditionalEnrollmentFieldsFromFilter();

		foreach ($additionalEnrollFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalEnrollmentField($fieldId));
		}

//		Log::_($columns);

		//var_dump($columns);
		return $columns;
	}

	/**
	 * Prepare columns for Users-Sessions custpom report
	 * @return array
	 */
	public function getGridColumnsUsersSessions($convertDateToUserFormat = true) {

		$columns = array();
		$columns = array_merge($columns, $this->getFieldsBundleColumns('user', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('course', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('session', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('enrollment', false, false, $convertDateToUserFormat));
		$event = new DEvent($this, array());
		Yii::app()->event->raise('AdditionalColumns', $event);
		if (!$event->shouldPerformAsDefault()) {
			$columns = array_merge($columns, $this->getFieldsBundleColumns('codes', false, false, $convertDateToUserFormat));

			// 25 is the custom report type id
			$columns = $this->reorderColumns(25, $columns);
		} else {

			// old main logic
			$columns = $this->reorderColumns(LearningReportType::USERS_SESSION, $columns);
		}

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}
		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();
		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}
		Yii::app()->event->raise('ChangeColumnsValues', new DEvent($this, array('columns' => &$columns)));
		return $columns;
	}

	public function getGridColumnsUsersContests() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('contest'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('ranking'));

		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
		}
		$columns = $this->reorderColumns(LearningReportType::USERS_CONTESTS, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		return $columns;
	}

	public function getGridColumnsUsersBadges() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('badges'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('assignment'));

		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
			unset($columns['user.idst']);
		}
		$columns = $this->reorderColumns(LearningReportType::USERS_BADGES, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		return $columns;
	}

	public function getGridColumsUsersCertifications() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('certification'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('enrollment'));

		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
			unset($columns['user.idst']);
		}
		$columns = $this->reorderColumns(LearningReportType::USERS_CERTIFICATION, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}
		//var_dump($columns);

		return $columns;
	}

	public function getGridCoulumnsCertificationsUsers() {
		$columns = array();
		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('certification'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('stat'));

		$columns = $this->reorderColumns(LearningReportType::CERTIFICATION_USERS, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		return $columns;
	}

	/**
	 * Prepare columns for Courses-Users custpom report
	 * @return array
	 */
	public function getGridColumnsCoursesUsers($convertDateToUserFormat = true) {

		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('course', false, false, $convertDateToUserFormat));

		// Add STAT fields; specify the "percentage base field" used for percentage calculations
		$showPercent = isset($filter['fields']['stat']['show_percents']) && ( (int) $filter['fields']['stat']['show_percents'] == 1);
		$statBaseField = $showPercent ? self::F_STAT_TOTAL_SUBSCRIBED_USERS : false;
		$columns = array_merge($columns, $this->getFieldsBundleColumns('stat', false, $statBaseField, $convertDateToUserFormat));

		$columns = $this->reorderColumns(LearningReportType::COURSES_USERS, $columns);
		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();
		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}
		return $columns;
	}

	/**
	 * Prepare columns for Users-Courses custpom report
	 * @return array
	 */
	public function getGridColumnsUsersLo($convertDateToUserFormat = true) {

		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('course', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('enrollment', false, false, $convertDateToUserFormat));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('learning_object', false, false, $convertDateToUserFormat));

		$columns = array_merge($columns, $this->getSingleColumn(self::F_USER_SUSPENDED));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_LO_TITLE));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_LO_MILESTONE));
		//$columns = array_merge($columns, $this->getSingleColumn(self::F_LO_TRACK_USER_SCORE)); //It must depends on Custom selection filter
		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
			unset($columns['user.idst']);
		}
		$columns = $this->reorderColumns(LearningReportType::USERS_LEARNING, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		//var_dump($columns);die;
		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();
		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}
		return $columns;
	}

	/**
	 * Prepare columns for Users-Delay custpom report
	 * @return array
	 */
	public function getGridColumnsUsersDelay() {

		$filter = CJSON::decode($this->filter_data);

		//var_export($filter);die;

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('course'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('enrollment'));

		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_LEVEL));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_STATUS));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_DATE_FIRST_ACCESS));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_DATE_LAST_ACCESS));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_DATE_COMPLETE));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_USER_NUMBER_SESSIONS));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_ENROLL_USER_TIME_IN_COURSE));

		$columns = array_merge($columns, $this->getSingleColumn(self::F_UDELAY_DATE_TO_COMPLETE));
		$columns = array_merge($columns, $this->getSingleColumn(self::F_UDELAY_DAYS_LEFT_TO_COMPLETE));


		$columns = $this->reorderColumns(LearningReportType::USERS_DELAY, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		$additionalCourseFields = $this->getAdditionalCourseFieldsFromFilter();
		foreach ($additionalCourseFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalCourseField($fieldId));
		}
		return $columns;
	}

	/**
	 * Prepare columns for Users-Notifications custom report
	 * @return array
	 */
	public function getGridColumnsUsersNotifications() {
		$filter = CJSON::decode($this->filter_data);

		//var_export($filter);die;

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('notification'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('delivered'));

		$columns = $this->reorderColumns(LearningReportType::USERS_NOTIFICATIONS, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		return $columns;
	}

	public function getGridColumnsEcommerce() {
		$filter = CJSON::decode($this->filter_data);

		//var_export($filter);die;

		$columns = array();
		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('ecommerce'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('ecommerce_item'));

		$columns = $this->reorderColumns(LearningReportType::ECOMMERCE_TRANSACTIONS, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}

		//var_dump($columns);exit;


		return $columns;
	}

	/**
	 * @return array
	 */
	public function getGridColumsUsersExternalTrainings() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('external_trainings'));

		if (isset($columns['user.idst'])) {
			$columns['user.idUser'] = $columns['user.idst'];
			unset($columns['user.idst']);
		}
		$columns = $this->reorderColumns(LearningReportType::USERS_EXTERNAL_TRAINING, $columns);

		$additionalFields = $this->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForAdditionalField($fieldId));
		}
		// Get External Training AdditionalFields
		$externaltrainingAdditionalFields = $this->getExternalTrainingAdditionalFieldsFromFilter();
		foreach ($externaltrainingAdditionalFields as $fieldId) {
			$columns = array_merge($columns, $this->getSingleColumnForExternalTrainingAdditionalField($fieldId));
		}

		return $columns;
	}

	/**
	 * Returns an array of ONE or TWO elements (columns), depending on $statBaseField and $skipPercentage.
	 *
	 * This is to return the Column data PLUS a percentage value (just another column), if requested (calculated using the statsBaseField).
	 * Also, apply value transformation if needed based on the column field.
	 *
	 * @param string $field
	 * @param boolean|string $statBaseField
	 * @param boolean $skipPercentage
	 * @return array
	 */
	public function getSingleColumn($field, $statBaseField = false, $skipPercentage = false, $convertDateToUserFormat = true) {

		// Certain columns require special "calculations", handled case by case

		/**
		 * A small utility function for ecommerce transaction reports,
		 * used to retrieve the buyer's information from the transaction
		 * itself, in case the user was deleted from the system
		 *
		 * @param $dataRow - the current ecommerce_transaction_info row
		 * @param $field - the field name to retrieve for the user, e.g.
		 * userid, firstname, lastname, email (properties of CoreUser model)
		 *
		 * @return string
		 */
		$_helperGetCachedUserValue = function($dataRow, $field) {
			$liveUserDeleted = array_key_exists(LearningReportFilter::F_USER_IDST, $dataRow) && !$dataRow[LearningReportFilter::F_USER_IDST];

			if (isset($dataRow[$field]) && !$liveUserDeleted) {
				// If the live user exists (not deleted), use that
				// user's value, even if null
				// (e.g. null is possible for firstname, lastname, etc)
				return $dataRow[$field];
			} else {
				if (!isset($dataRow['item_data_json'])) {
					return;
				}

				// Else try to get cached value from the transaction row
				$jsonData = CJSON::decode($dataRow['item_data_json']);
				$buyerUserCached = isset($jsonData['position_snapshot']['buyer_user']) ? (array) $jsonData['position_snapshot']['buyer_user'] : array();

				$fieldCleaned = str_replace('user.', '', $field);

				if ($buyerUserCached[$fieldCleaned]) {
					return $buyerUserCached[$fieldCleaned];
				}
			}

			// Neither snapshot data of the transaction item, nor the real
			// model was found. The item is deleted from the system so return "(deleted)" string
			return "(" . Yii::t('standard', '_USER_STATUS_CANCELLED') . ")";
		};

		$columnParams = array();

		switch ($field) {

			case self::F_COURSE_NAME:
				// Escape showing the link when exporting
				if (isset($_REQUEST['exportType']) || isset(Yii::app()->session['schedulerExport'])) {
					$valueString = '$data["' . $field . '"]';
				} else {
					$valueString = function ($data) use ($field) {
						// Raise an event and see if someone can build the value string for us, for this field
						$event = new DEvent($this, array('data' => $data, 'name' => $data[$field]));
						Yii::app()->event->raise('onBeforeRenderCourseName', $event);
						if ($event->return_value)
							return $event->return_value;

						switch ($data[LearningReportFilter::F_COURSE_TYPE]) {
							case LearningCourse::ELEARNING: {
								return CHtml::link($data[$field], Docebo::createLmsUrl("player/report", array("course_id" => $data[LearningReportFilter::F_COURSE_ID])), array("target" => "_blank", "class" => "courseLink"));
							}
								break;
							case LearningCourse::CLASSROOM: {
								return CHtml::link($data[$field], Docebo::createAdminUrl("reportManagement/classroomCourseReport", array("course_id" => $data[LearningReportFilter::F_COURSE_ID])), array("target" => "_blank", "class" => "courseLink"));
							}
								break;
							case LearningCourse::TYPE_WEBINAR: {
								return CHtml::link($data[$field], Docebo::createAdminUrl("reportManagement/webinarCourseReport", array("course_id" => $data[LearningReportFilter::F_COURSE_ID])), array("target" => "_blank", "class" => "courseLink"));
							}
								break;
						}
						// If the course type is not among the ordinary types or it's not handled by the custom plugin, returning the course name without url
						return $data[$field];
					};
					$typeRaw = true;
				}
				break;

			case self::F_COURSE_STATUS:
				$valueString = 'LearningCourse::getStatusLabelTrsnaslated($data["' . $field . '"])';
				break;

			case self::F_ENROLL_STATUS:
				$valueString = 'Yii::t("standard", LearningCourseuser::getStatusLangLabel($data["' . $field . '"]))';
				break;

			case self::F_ENROLL_LEVEL:
				$valueString = 'Yii::t("levels", "_LEVEL_" . $data["' . $field . '"])';
				break;

			case self::F_ENROLL_USER_TIME_IN_COURSE:
				$valueString = 'LearningTracksession::model()->getUserTotalTime($data["user.idUser"], $data["course.idCourse"], \'G\h i\m s\s\')';
				break;

			case self::F_ENROLL_USER_SCORE_GIVEN:
				$valueString = function($data) {
					if ($data[LearningReportFilter::F_ENROLL_USER_SCORE_GIVEN]) {
						return $data[LearningReportFilter::F_ENROLL_USER_SCORE_GIVEN];
					} else {
						return LearningCourseuser::getLastScoreByUserAndCourse($data[LearningReportFilter::F_COURSE_ID], $data['user.idUser']);
					}
				};
				break;

			case self::F_ENROLL_USER_INITIAL_SCORE_GIVEN:
				$valueString = function($data) {
					if ($data[LearningReportFilter::F_ENROLL_USER_INITIAL_SCORE_GIVEN]) {
						return $data[LearningReportFilter::F_ENROLL_USER_INITIAL_SCORE_GIVEN];
					} else {
						return LearningCourseuser::getLastInitialScoreByUserAndCourse($data[LearningReportFilter::F_COURSE_ID], $data['user.idUser']);
					}
				};
				break;

			case self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE:
				$valueString = function($data) use ($field) {
					$seconds = (int) $data[$field];
					return Yii::app()->localtime->hoursAndMinutes($seconds);
				};
				break;

			case self::F_USER_IDST:
			case self::F_USER_ID:
				$valueString = function($data) use ($field, $_helperGetCachedUserValue) {
					return $_helperGetCachedUserValue($data, $field);
				};
				break;
			case self::F_USER_FIRSTNAME:
				$valueString = function($data) use ($field, $_helperGetCachedUserValue) {
					return $_helperGetCachedUserValue($data, $field);
				};
				break;
			case self::F_USER_LASTNAME:
				$valueString = function($data) use ($field, $_helperGetCachedUserValue) {
					return $_helperGetCachedUserValue($data, $field);
				};
				break;
			case self::F_USER_EMAIL:
				$valueString = function($data) use ($field, $_helperGetCachedUserValue) {
					return $_helperGetCachedUserValue($data, $field);
				};
				break;

			case self::F_USER_USERID:
				$valueString = function($data) use ($field) {
					if ($data[$field]) {
						// If grid column has value, use that (live) value
						return ltrim($data[$field], "/");
					} else {
						// Else try to get cached value from the transaction row
						$jsonData = CJSON::decode($data['item_data_json']);
						$buyerUserCached = isset($jsonData['position_snapshot']['buyer_user']) ? (array) $jsonData['position_snapshot']['buyer_user'] : array();

						$fieldCleaned = str_replace('user.', '', $field);

						if ($buyerUserCached[$fieldCleaned]) {
							return ltrim($buyerUserCached[$fieldCleaned], "/");
						}
					}

					// Neither snapshot data of the transaction item, nor the real
					// model was found. The item is deleted from the system so return "(deleted)" string
					return "(" . Yii::t('standard', '_USER_STATUS_CANCELLED') . ")";
				};
				break;

			case self::F_USER_SUSPENDED:
				$valueString = '$data["' . $field . '"] ? Yii::t("standard", "_YES") : Yii::t("standard", "_NO")';
				break;

			case self::F_LO_TRACK_USER_STATUS:
				$valueString = 'LearningCommontrack::model()->getStatusLabel($data["' . $field . '"])';
				break;

			// If the field is user.groups_list, lets transform it in a list of Org Chart PATHS
			case self::F_USER_GROUPS_LIST:
				if(isset(self::$reportsGroupsFields[$this->report_type_id]) && self::$reportsGroupsFields[$this->report_type_id]){
					$valueString = 'CoreGroupMembers::getOcReportGroupsPaths($data["' . $field . '"] , $data["'.self::$reportsGroupsFields[$this->report_type_id].'"])';
				}else{
					$valueString = 'CoreOrgChartTree::getOcGroupsPaths($data["' . $field . '"], " > ", true, ",\n")';
				}

				$columnParams = array(
					'type' => 'ntext',
					'htmlOptions' => array('style' => 'white-space:nowrap;')
				);
				break;
			case self::F_USER_CONTRIBUTION_BRANCHES:
				$valueString = 'CoreOrgChartTree::getOcGroupsPaths($data["' . self::F_USER_CONTRIBUTION_BRANCHES . '"], " > ", true, ",\n")';
				$columnParams = array(
					'type' => 'ntext',
					'htmlOptions' => array('style' => 'white-space:nowrap;')
				);
				break;

			case self::F_LO_TYPE:
				$valueString = 'LearningCommontrack::model()->getObjectTypeTranslation($data["' . $field . '"])';
				break;
			case self::F_COURSEUSER_SESSION_TOTAL_HOURS:
				$valueString = 'LtCourseSession::model()->convertHoursToMinutes($data["'.$field.'"])';
				break;
			case self::F_LO_TRACK_USER_SCORE:
				$valueString = function($data) use ($field) {
					if ($data[LearningReportFilter::F_LO_TYPE] == LearningOrganization::OBJECT_TYPE_TEST) {
						if ($data['test_point_type'] == LearningTest::POINT_TYPE_PERCENT && $data['score_max'] == 100)
							return $data[$field] . " %";
					}
					return $data[$field];
				};
				break;
				break;

			// Format datetime columns
			case self::F_ENROLL_DATE_ENROLLMENT:
			case self::F_USER_REGISTER_DATE:
			case self::F_USER_SUSPEND_DATE:
			case self::F_ENROLL_DATE_FIRST_ACCESS:
			case self::F_ENROLL_DATE_LAST_ACCESS:
			case self::F_ENROLL_DATE_COMPLETE:
			case self::F_LO_TRACK_DATE_FIRST_ATTEMPT:
			case self::F_LO_TRACK_DATE_LAST_ATTEMPT:
			case self::F_UDELAY_DATE_TO_COMPLETE:
			case self::F_ECOMMERCE_DATE_CREATED:
			case self::F_ECOMMERCE_DATE_PAID:
			case self::F_BADGES_ISSUED_ON:
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00" || $data["' . $field . '"] == null) ? "" : Yii::app()->localtime->toLocalDateTime($data["' . $field . '"],"short","short",null,null,false)';
				} else {
					$valueString = 'Yii::app()->localtime->toDateTimeInServerFormatWithLocalTz($data["' . $field . '"],"short", "short")';
				}
				break;
			case self::F_CERTIFICATION_ISSUED_ON:
			case self::F_CERTIFICATION_RENEW_IN:
				$valueString = '(strpos($data["' . $field . '"], "0000-00-00") !== false || $data["' . $field . '"] == null) ? "" : Yii::app()->localtime->toLocalDate($data["' . $field . '"])';
				break;
			case self::F_COURSE_DATE_BEGIN:
			case self::F_COURSE_DATE_END:
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00" || !$data["'.$field.'"]) ? "" : Yii::app()->localtime->toLocalDate($data["' . $field . '"],"short")';
				} else {
					$valueString = '($data["' . $field . '"] == "0000-00-00") ? "" : Yii::app()->dateFormatter->format(Yii::app()->locale->getDateFormat("short"), $data["' . $field . '"])';
				}
				break;
			case self::F_COURSE_EXPIRED:
				$valueString = 'Yii::t("standard", $data["' . $field . '"])';
				break;
			case self::F_ENROLL_DATE_BEGIN_VALIDITY:
			case self::F_ENROLL_DATE_EXPIRE_VALIDITY:
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00 00:00:00") ? "" : Yii::app()->localtime->toLocalDateTime($data["' . $field . '"],"short","short",null,null,false)';
				} else {
					$valueString = 'Yii::app()->localtime->toDateTimeInServerFormatWithLocalTz($data["' . $field . '"],"short", "short")';
				}

				break;

			case self::F_ECOMMERCE_TOTAL_PRICE:
				$fieldNameSinglePrice = self::F_ECOMMERCE_ITEM_SINGLE_PRICE;
				$fieldNameQuantity = self::F_ECOMMERCE_ITEM_QUANTITY;
				$fieldNameType = self::F_ECOMMERCE_ITEM_TYPE;
				$fieldNameDiscount = self::F_ECOMMERCE_DISCOUNT;
				$fieldNameCouponDiscount = self::F_ECOMMERCE_COUPON_DISCOUNT;
				$fieldNamePaymentCurrency = self::F_ECOMMERCE_PAYMENT_CURRENCY;
				$fieldNameCouponDiscountType = self::F_ECOMMERCE_COUPON_DISCOUNT_TYPE;
				$fieldNameTransactionId = self::F_ECOMMERCE_ID_TRANSACTION;

				// Use an anonymous function as column value parser here
				// because if the transaction item to display on this row is
				// of the type "PU bought seats in course), the quantity of
				// purchased seats is stored in a JSON data field (not in a directly
				// retrievable column from $data)
				$valueString = function($data) use ($field, $fieldNameCouponDiscount, $fieldNameQuantity, $fieldNameSinglePrice, $fieldNameType, $fieldNameDiscount, $fieldNamePaymentCurrency, $fieldNameCouponDiscountType, $fieldNameTransactionId) {
					$quantity = $data[$fieldNameQuantity];

					$singlePrice = $data[$fieldNameSinglePrice];
					switch ($data[$fieldNameType]) {
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							$transactionItemJson = CJSON::decode($data['item_data_json']);
							if (isset($transactionItemJson['seats']) && $transactionItemJson['seats'] > 0) {
								$quantity = (int) $transactionItemJson['seats'];
								if($quantity > 0) $singlePrice = $singlePrice/$quantity;
							}
							break;
					}

					$totalPrice = $quantity * $singlePrice;
//					if ($data[$fieldNameDiscount])
//						$totalPrice -= $data[$fieldNameDiscount];

					if($data[$fieldNameCouponDiscount]) {
						$couponDiscount = $data[$fieldNameCouponDiscount];
						if ($couponDiscount > 0) {
							if($data[$fieldNameCouponDiscountType] == 'percent'){
								$discount = ($totalPrice / 100) * $couponDiscount;
								$totalPrice = $totalPrice - $discount;

							} else {
								$discount = $data[$fieldNameDiscount];
								$totalPrice = $totalPrice - $discount;
							}
							if($data[$field] == $data[$fieldNameDiscount]){
								$data[$field] = $discount;
							}
						}
					}

					return $totalPrice .  ' ' . $data[$fieldNamePaymentCurrency];
				};
				break;

			case self::F_ECOMMERCE_DISCOUNT:
				$fieldNamePaymentCurrency = self::F_ECOMMERCE_PAYMENT_CURRENCY;
				$fieldNameDiscount = self::F_ECOMMERCE_DISCOUNT;
				$fieldNameType = self::F_ECOMMERCE_ITEM_TYPE;
				$fieldNameQuantity = self::F_ECOMMERCE_ITEM_QUANTITY;
				$fieldNameSinglePrice = self::F_ECOMMERCE_ITEM_SINGLE_PRICE;
				$fieldNameCouponDiscount = self::F_ECOMMERCE_COUPON_DISCOUNT;
				$fieldNameCouponDiscountType = self::F_ECOMMERCE_COUPON_DISCOUNT_TYPE;

				$valueString = function($data) use ($field, $fieldNamePaymentCurrency, $fieldNameDiscount, $fieldNameQuantity, $fieldNameCouponDiscount, $fieldNameType, $fieldNameSinglePrice, $fieldNameCouponDiscountType) {

					$quantity = $data[$fieldNameQuantity];
					$singlePrice = $data[$fieldNameSinglePrice];

					switch ($data[$fieldNameType]) {
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							$transactionItemJson = CJSON::decode($data['item_data_json']);
							if (isset($transactionItemJson['seats']) && $transactionItemJson['seats'] > 0) {
								$quantity = (int) $transactionItemJson['seats'];
								if($quantity > 0) $singlePrice = $singlePrice/$quantity;
							}
							break;
					}

					$fieldNamePaymentCurrency = self::F_ECOMMERCE_PAYMENT_CURRENCY;

					$totalPrice = $quantity * $singlePrice;

					if($data[$fieldNameCouponDiscount]) {
						$couponDiscount = $data[$fieldNameCouponDiscount];
						if ($couponDiscount > 0) {
							if($data[$fieldNameCouponDiscountType] == 'percent'){
								$discount = ($totalPrice / 100) * $couponDiscount;
							} else {
								$discount = $data[$fieldNameDiscount];
							}
							if($data[$field] == $data[$fieldNameDiscount]){
								$data[$field] = $discount;
							}
						}
					}

					return $data[$field] . ' ' . $data[$fieldNamePaymentCurrency];
				};
				break;


			case self::F_ECOMMERCE_ITEM_SINGLE_PRICE:
				$fieldNamePaymentCurrency = self::F_ECOMMERCE_PAYMENT_CURRENCY;
				$valueString = function($data) use ($field, $fieldNamePaymentCurrency) {
					$singlePrice = $data[$field];
					if($data[self::F_ECOMMERCE_ITEM_TYPE] == EcommerceTransactionInfo::TYPE_COURSESEATS) {
						$transactionItemJson = CJSON::decode($data['item_data_json']);
						if (isset($transactionItemJson['seats']) && $transactionItemJson['seats'] > 0) {
							$quantity = (int) $transactionItemJson['seats'];
							if($quantity > 0) $singlePrice = $singlePrice/$quantity;
						}
					}
					return $singlePrice . ' ' . $data[$fieldNamePaymentCurrency];
				};
				break;

			case self::F_ECOMMERCE_PAYMENT_STATUS:
				$valueString = function($data) use ($field) {
					return $data[$field] ? Yii::t('billing', '_PAID') : Yii::t('billing', '_NOT_PAID');
				};
				break;

			case self::F_ECOMMERCE_ITEM_QUANTITY:
				// By default it's always 1 except if the transaction item
				// is a "Power user bought seats in course". In this case,
				// the quantity is the bought seats and is retrieved from
				// the raw JSON data of the transaction item

				$fieldNameQuantity = self::F_ECOMMERCE_ITEM_QUANTITY;
				$fieldNameType = self::F_ECOMMERCE_ITEM_TYPE;

				$valueString = function($data) use ($fieldNameType, $fieldNameQuantity) {
					$quantity = $data[$fieldNameQuantity];

					switch ($data[$fieldNameType]) {
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							$transactionItemJson = CJSON::decode($data['item_data_json']);
							if (isset($transactionItemJson['seats']) && $transactionItemJson['seats'] > 0) {
								$quantity = (int) $transactionItemJson['seats'];
							}
							break;
					}

					return $quantity;
				};
				break;

			case self::F_ECOMMERCE_ITEM_NAME:
				// Get the item name from the transaction snapshot. If fails,
				// try to get the actual live item's name that was purchased
				// (e.g. the course or LP name). If that fails, fallback to
				// the transaction standard item name
				$valueString = function($data) use ($field) {
					$jsonData = CJSON::decode($data['item_data_json']);
					$positionModelSnapshot = isset($jsonData['position_snapshot']) ? (array) $jsonData['position_snapshot'] : array();


					switch ($data[LearningReportFilter::F_ECOMMERCE_ITEM_TYPE]) {
						case EcommerceTransactionInfo::TYPE_COURSE:
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							//Get CourseID
							$courseId = isset($data['idCourse']) ? $data['idCourse'] : null;
							if (isset($positionModelSnapshot['name'])) {
								if (!isset($_REQUEST['exportType']) && !isset(Yii::app()->session['schedulerExport'])) {
									// Raise an event and see if someone can build the value string for us, for this field
									$event = new DEvent($this, array('data' => $data, 'name' => $positionModelSnapshot['name']));
									Yii::app()->event->raise('onBeforeRenderCourseName', $event);
									if ($event->shouldPerformAsDefault() && $event->return_value)
										return $event->return_value;

									switch ($data[LearningReportFilter::F_COURSE_TYPE]) {
										case LearningCourse::ELEARNING: {
											return CHtml::link($positionModelSnapshot['name'], Docebo::createLmsUrl("player/report", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
										case LearningCourse::CLASSROOM: {
											return CHtml::link($positionModelSnapshot['name'], Docebo::createAdminUrl("reportManagement/classroomCourseReport", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
										case LearningCourse::TYPE_WEBINAR: {
											return CHtml::link($positionModelSnapshot['name'], Docebo::createAdminUrl("reportManagement/webinarCourseReport", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
									}
								} else {
									return $positionModelSnapshot['name'];
								}
							} elseif (isset($data['course.name'])) {
								if (!isset($_REQUEST['exportType']) && !isset(Yii::app()->session['schedulerExport'])) {
									// Raise an event and see if someone can build the value string for us, for this field
									$event = new DEvent($this, array('data' => $data, 'name' => $data[LearningReportFilter::F_COURSE_NAME]));
									Yii::app()->event->raise('onBeforeRenderCourseName', $event);
									if ($event->shouldPerformAsDefault() && $event->return_value)
										return $event->return_value;

									switch ($data[LearningReportFilter::F_COURSE_TYPE]) {
										case LearningCourse::ELEARNING: {
											return CHtml::link($data[LearningReportFilter::F_COURSE_NAME], Docebo::createLmsUrl("player/report", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
										case LearningCourse::CLASSROOM: {
											return CHtml::link($data[LearningReportFilter::F_COURSE_NAME], Docebo::createAdminUrl("reportManagement/classroomCourseReport", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
										case LearningCourse::TYPE_WEBINAR: {
											return CHtml::link($data[LearningReportFilter::F_COURSE_NAME], Docebo::createAdminUrl("reportManagement/webinarCourseReport", array("course_id" => $courseId)), array("target" => "_blank", "class" => "courseLink"));
										}break;
									}
								} else {
									return $data['course.name'];
								}
							}
							break;
						case EcommerceTransactionInfo::TYPE_COURSEPATH:
							$pathId = isset($data['idPath']) ? $data['idPath'] : null;
							if (isset($positionModelSnapshot['path_name'])) {
								if (!isset($_REQUEST['exportType']) && !isset(Yii::app()->session['schedulerExport']))
									return CHtml::link($positionModelSnapshot['path_name'], Docebo::createAdminUrl("CurriculaApp/curriculaManagement/courses", array("id" => $pathId)), array("target" => "_blank", "class" => "courseLink"));
								else
									return $positionModelSnapshot['path_name'];
							}elseif (isset($data['coursepath.name'])) {
								if (!isset($_REQUEST['exportType']) && !isset(Yii::app()->session['schedulerExport']))
									return CHtml::link($data['coursepath.name'], Docebo::createAdminUrl("CurriculaApp/curriculaManagement/courses", array("id" => $pathId)), array("target" => "_blank", "class" => "courseLink"));
								else
									return $data['coursepath.name'];
							}
							break;
						default:
							return $data[$field];
					}

					// Neither snapshot data of the transaction item, nor the real
					// model was found. The item is deleted from the system so return "(deleted)" string
					return "(" . Yii::t('standard', '_USER_STATUS_CANCELLED') . ")";
				};
				$typeRaw = true;
				break;

			case self::F_ECOMMERCE_ITEM_TYPE:
				$valueString = function($data) use ($field) {
					switch ($data[$field]) {
						case EcommerceTransactionInfo::TYPE_COURSE:
							return Yii::t('standard', '_COURSE');
							break;
						case EcommerceTransactionInfo::TYPE_COURSEPATH:
							return Yii::t('standard', '_COURSEPATH');
							break;
						case EcommerceTransactionInfo::TYPE_COURSESEATS:
							return Yii::t('standard', 'Seats');
						default:
							return 'Unknown transaction item type';
					}
				};
				break;

			case self::F_NOTIFICATION_TYPE:
				$valueString = '($data["' . $field . '"] == "newsletter") ? Yii::t("standard", "_MESSAGES") : Yii::t("notification", "Notifications")';
				break;
			case self::F_NOTIFICATION_TIMESTAMP:
				$valueString = 'Yii::app()->localtime->toLocalDateTime($data["' . $field . '"])';
				break;
			case self::F_NOTIFICATIONS_DELIVERED_READ:
				$valueString = '($data["' . $field . '"] == 1) ? Yii::t("standard", "_YES") : Yii::t("standard", "_NO")';
				break;
			case self::F_NOTIFICATION_JSON:
				$valueString = function($data) {
					$dataArr = CJSON::decode($data[LearningReportFilter::F_NOTIFICATION_JSON]);
					$result = "";
					foreach ($dataArr as $key => $value) {
						$value = strip_tags($value);
						$value = preg_replace("/&#?[a-z0-9]+;/i", " ", $value);
						$line = "[" . strtoupper($key) . "]: " . $value . " <br />";
						$result = $result . $line;
					}

					$result = trim(preg_replace('/\s\s+|\r|\n/', ' ', $result));
					return $result;
				};
				$isNotificationJSON = true;
				break;
			case self::F_COURSEPATH_NAME:
				if (isset($_REQUEST['exportType']) || isset(Yii::app()->session['schedulerExport'])) {
					$valueString = '$data["' . $field . '"]';
				} else {
					if(Yii::app()->user->getIsAdmin() && !Yii::app()->user->checkAccess('/lms/admin/coursepath/view')){
						$valueString = '$data["' . $field . '"]';
					}else{
						$valueString = 'CHtml::link($data["' . $field . '"], Docebo::createAdminUrl("CurriculaApp/curriculaManagement/courses" , array("id" => $data["' . self::F_COURSEPATH_ID . '"])), array("target" => "_blank", "class" => "courseLink"))';
					}
					$typeRaw = true;
				}
				break;
			case self::F_COURSEPATH_SUBSCRIPTION_DATE:
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00") ? "" : Yii::app()->localtime->toLocalDateTime($data["' . $field . '"],"short","short")';
				} else {
					$valueString = 'Yii::app()->localtime->toDateTimeInServerFormatWithLocalTz($data["' . $field . '"],"short", "short")';
				}
				break;
			case self::F_COURSEPATH_COMPLETION_DATE:
				$valueString = function($data) use ($field) {
					$res = LearningCoursepath::model()->getCoursepathPercentage($data[LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS], $data[$field]);
					if ($res['percentage'] == 100) {
						$completionDate = LearningCoursepath::getCompletionDate($data[LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS], $data[$field]);
						return Yii::app()->localtime->toLocalDateTime($completionDate);
					} else {
						return Yii::t('standard', '_NONE');
					}
				};
				break;
			case self::F_COURSEPATH_COMPLETION_PERCENT:
				$valueString = function($data) use ($field) {
					$res = LearningCoursepath::model()->getCoursepathPercentage($data[LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS], $data[LearningReportFilter::F_COURSEPATH_COMPLETION_DATE]);
					return $res['percentage'] . '%';
				};
				break;
			case self::F_COURSEPATH_COMPLETION_STATUS:
				$valueString = function($data) use ($field) {
					$res = LearningCoursepath::model()->getCoursepathPercentage($data[LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS], $data[LearningReportFilter::F_COURSEPATH_COMPLETION_DATE]);
					switch ($res['percentage']) {
						case 0:
							$atLeastOneCourseInProgress = Yii::app()->db->createCommand()
								->select('COUNT(*)')
								->from(LearningCourseuser::model()->tableName().' cu')
								->join(LearningCoursepathCourses::model()->tableName().' cpc', 'cpc.id_item = cu.idCourse AND cpc.id_path = :id_path AND cu.idUser=:idUser',
									array(
										'id_path' => $data[LearningReportFilter::F_COURSEPATH_COMPLETION_DATE],
										'idUser' => $data[LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS]
									))
								->where('cu.status = :status', array(':status' => LearningCourseuser::$COURSE_USER_BEGIN))
								->queryScalar();
							if($atLeastOneCourseInProgress)
								return Yii::t('report', 'STATUS_IN_PROGRESS');
							return Yii::t('standard', '_NOT_STARTED');
						case 100:
							return Yii::t('course', '_CST_CONCLUDED');
						default:
							return Yii::t('report', 'STATUS_IN_PROGRESS');
					}
				};
				break;
			//Session reports
			case self::F_COURSEUSER_SESSION_EVALUATION_STATUS:
				$valueString = '$data["' . $field . '"] == LtCourseuserSession::EVALUATION_STATUS_PASSED ? Yii::t("standard", "passed") : ($data["' . $field . '"] == LtCourseuserSession::EVALUATION_STATUS_FAILED ? Yii::t("standard", "failed") : Yii::t("standard", "_USER_STATUS_BEGIN"))';
				break;
			case self::F_COURSEUSER_SESSION_EVALUATION_TEXT:
				$valueString = '$data["' . $field . '"]';
				$isNotificationJSON = true;
				break;
			case self::F_COURSEUSER_SESSION_DATE_BEGIN :
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00") ? "" : Yii::app()->localtime->toLocalDateTime($data["' . $field . '"])';
				} else {
					$valueString = 'Yii::app()->localtime->toDateTimeInServerFormatWithLocalTz($data["' . $field . '"],"short", "short")';
				}

				break;
			case self::F_COURSEUSER_SESSION_DATE_END :
				if ($convertDateToUserFormat) {
					$valueString = '($data["' . $field . '"] == "0000-00-00") ? "" : Yii::app()->localtime->toLocalDateTime($data["' . $field . '"])';
				} else {
					$valueString = 'Yii::app()->localtime->toDateTimeInServerFormatWithLocalTz($data["' . $field . '"],"short", "short")';
				}
				break;
			case self::F_EXTERNAL_TRAININGS_TO_DATE :
				$valueString = 'Yii::app()->localtime->toLocalDate($data["' . $field . '"])';
				break;
			case self::F_USER_EXPIRATION :
				$valueString = function($data) use ($field) {
					// prepare user date format
					$dateFormat = Yii::app()->localtime->getPHPLocalDateFormat();
					$objDateTime = DateTime::createFromFormat('Y-m-d', $data[$field]);
					return (!empty($objDateTime) ? $objDateTime->format($dateFormat) : '');
				};
				break;
			case self::F_USER_EMAIL_STATUS :
				$valueString = function($data) use ($field) {
					return ($data[$field] == 1) ? Yii::t('standard', 'Verified') : Yii::t('standard', 'Unverified');
				};
				break;
			case self::F_CERTIFICATION_DESCRIPTION:
			case self::F_CONTEST_DESCRIPTION:
				$valueString = '$data["' . $field . '"]';
				$isNotificationJSON = true;
				break;
			case self::F_CERTIFICATION_EXPIRATION:
				$valueString = 'LearningReportFilter::renderCertificationExpirationColumn($data, true)';
				break;
			case self::F_CERTIFICATION_ACTIVITY_TYPE:
				$valueString = function($data) use ($field) {
					if ($data[$field] == "course")
						return Yii::t("standard", "_COURSE");
					else if ($data[$field] == "plan")
						return Yii::t("standard", "_COURSEPATH");
					else
						return Yii::t("transcripts", "External activities");
				};
				break;
			case self::F_BADGES_ICON:
				$valueString = '"<img style=\"height:30px;\" src=\'".CoreAsset::model()->findByPk($data["' . $field . '"])->getUrl()."\'/>"';
				$isNotificationJSON = true;
				break;
			case self::F_BADGES_DESC:
				$valueString = '$data["' . $field . '"]';
				$isNotificationJSON = true;
				break;
			case self::F_BADGES_ASSIGNMENT_MODE:
				$valueString = function($data) use ($field) {
					$search = array();
					$replace = array();
					$eventParams = CJSON::decode($data['event_params']);
                    if (count($eventParams)>0) {
						if(count($eventParams[0]) >0){
							foreach ($eventParams[0] as $key => $value) {
								$search[] = $key;
								$replace[] = '<strong>' . $value . '</strong>';
							}
						} else {
							foreach ($eventParams as $key => $value) {
								$search[] = $key;
								$replace[] = '<strong>' . $value . '</strong>';
							}
						}

						return str_replace($search, $replace, $data[$field]);
					}
					return $data[$field];
				};
				$isNotificationJSON = true;
				break;
			case self::F_CONTEST_PERIOD:
				$valueString = function($data) use($field) {
					$periods = explode(',', $data[$field]);
					$useUserTimezone = Settings::get('timezone_allow_user_override', 'off');

					$timezone = '';
					$defaultTimezone = Settings::get('timezone_default', 'UTC');

					if ($useUserTimezone == 'on') {
						$userTimezone = CoreSettingUser::model()->findByAttributes(array(
							'path_name' => 'timezone',
							'id_user' => Yii::app()->user->id
						));

						if ($userTimezone) {
							$timezone = $userTimezone->value;
						}
					} else {
						$timezone = $defaultTimezone;
					}

					if (empty($data['timezone'])) {
						$data['timezone'] = $defaultTimezone;
					}
					if ($data['timezone'] !== $timezone) {
						$timezonesArray = Yii::app()->localtime->getTimezonesArray();
						preg_match('#\(.*?\)#', $timezonesArray[$data['timezone']], $currentTimezoneText);
					}
					return Yii::app()->localtime->toLocalDate($periods[0]) . ' / ' . Yii::app()->localtime->toLocalDate($periods[1]) . ' ' . $currentTimezoneText[0];
				};
				break;
			case self::F_CONTEST_GOAL:
				$valueString = function($data) use($field) {
					$goals = GamificationContest::getGoals();

					return $goals[$data[$field]];
				};
				break;

			case self::F_RANKING_REWARDS:
				$valueString = function($data) use($field) {
					$reward = CJSON::decode($data[$field]);
					if ($reward['type'] == GamificationContestReward::REWARD_TYPE_BADGE) {
						return '<img width="40" height="40" src=" ' . CoreAsset::model()->findByPk($reward['value']['badge_icon'])->getUrl() . ' " />';
					} else {
						return '';
					}
				};
				$isNotificationJSON = true;
				break;

			case self::F_ASSET_CHANNELS:
				$valueString = 'App7020Assets::getChannelsNames($data["idAsset"])';
				break;

			case self::F_ASSET_WATCH_RATE:
				$valueString = 'App7020Assets::globalWatchRate($data["idAsset"])';
				break;

			case self::F_ASSET_REACTION_TIME:
				$valueString = 'gmdate("H:i:s", App7020Assets::getAverageReactionTime($data["idAsset"]))';
				break;

			case self::F_CHANNELS_DESCRIPTION:
				$valueString = 'strip_tags($data["channel.description"])';
				break;

			case self::F_CHANNELS_PERCENT_PR:
				$valueString = '($data["'.self::F_CHANNELS_PERCENT_PR.'"]*100). "%"';
				break;

			case self::F_CHANNELS_PERCENT_QA:
				$valueString = '($data["'.self::F_CHANNELS_PERCENT_QA.'"]*100). "%"';
				break;

			case self::F_CHANNELS_PERCENT_SHARED:
				$valueString = '($data["'.self::F_CHANNELS_PERCENT_SHARED.'"]*100). "%"';
				break;
            case self::F_AUDIT_TRAIL_APPROVED_BY:
                $valueString = function($data) {
                    $auditTrailActionType = $data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_ACTION];
                    $result = '';
                    switch($auditTrailActionType) {
                        case CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG:
                            $json = CJSON::decode($data[self::F_AUDIT_TRAIL_JSON]);

                            $fName = $json['approved_by']['firstname'];
                            $lName = $json['approved_by']['lastname'];

                            if($fName && $lName){
                                $show_first_name_first = Settings::get('show_first_name_first', 'off');
                                if($show_first_name_first == 'on'){
                                    $result = $fName. ' '. $lName;
                                } else{
                                    $result = $lName . ' ' . $fName;
                                }
                            }elseif($lName){
                                $result = $lName;
                            }elseif($fName){
                                $result = $fName;
                            }else{
                                $result = Yii::app()->user->getRelativeUsername($json['approved_by']['userid']);
                            }
                            break;
                    }
                    return $result;
                };
                break;
			case self::F_STAT_AVERAGE_EVALUATION:
				$valueString = function($data) use($field) {
					// global rating setting
					$ratingSetting = Settings::get('course_rating_permission', AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED);
					// course rating setting
					$courseRatingSetting = json_decode($data['social_rating_settings'], true);

					if(isset($courseRatingSetting['course_rating_permission'])) {
						$ratingSetting = $courseRatingSetting['course_rating_permission'];
					}

					if($ratingSetting == AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED)
						return '';

					return round($data[$field]);
				};
				break;

			default:
				$valueString = false;
				// Raise an event and see if someone can build the value string for us, for this field
				Yii::app()->event->raise('CustomReportGetSingleColumn', new DEvent($this, array(
					'field' => $field,
					'valueString' => &$valueString
				)));

				if ($valueString === false)
					$valueString = '$data["' . $field . '"]';

				break;
		}

		// Standard VALUE column
		$columns[$field] = array_merge($columnParams, array(
			'header' => self::fieldName($field),
			'value' => $valueString,
		));

		if (isset($isNotificationJSON) && $isNotificationJSON) {
			$columns[$field]['type'] = 'html';
		}

		// Set type raw so we can render a link inside
		if (isset($typeRaw) && $typeRaw) {
			$columns[$field]['type'] = 'raw';
		}

		// Percentage Value Column: IF $statBaseField is provided, add a "percent" column after the current one,
		if ($statBaseField && ($statBaseField != $field) && !$skipPercentage) {
			$columns[$field . "_percentage"] = array(
				'value' => 'number_format($data["' . $field . '"]/$data["' . $statBaseField . '"]*100,2)',
				'header' => '%',
			);
		}

		return $columns;
	}

	/**
	 * Get column definition for a USER single additional field
	 *
	 * @param number $fieldId (1,2,3,...)
	 * @return array
	 */
	public function getSingleColumnForAdditionalField($fieldId) {
		$columns = array();
		
		// Get field name in current language
		$coreUserFieldData = CoreUserField::getFieldsTranslations((int)$fieldId);

		if (!isset($coreUserFieldData[0]['translation'])) {
			$header = "field: $fieldId";
		} else {
			$header = $coreUserFieldData[0]['translation'];
		}

		// SQL field name
		$fieldValueName = "field_" . $fieldId . "_value";

		$columnValue='';

		if (isset($coreUserFieldData[0]['type'])) {

		    
		    // NOTE: All values are returned by a anonymous function
		    // All required raw data are already loaded by SQL and can be fiound in $data array
		    // e.g. $data["field_234_value"], $data["idst"], etc.
		    // Basically $data represents one record
			switch ($coreUserFieldData[0]['type']) {
			    
				case CoreUserField::TYPE_YESNO:
				    $columnValue = function($data) use ($fieldValueName) {
				        if (!$data[$fieldValueName]) return "";
				        return FieldYesno::getLabelFromValue($data[$fieldValueName]);
				    };
					break;
					
				case CoreUserField::TYPE_COUNTRY:
				    $columnValue = function($data) use ($fieldValueName) {
				        if (!$data[$fieldValueName]) return "";
				        $field = new FieldCountry();
				        return $field->renderFieldValue($data[$fieldValueName]);
				    };
					break;

				case CoreUserField::TYPE_UPLOAD:
					$columnValue = function($data) use ($fieldValueName) {
					    return $data[$fieldValueName];
					};
					break;

				case CoreUserField::TYPE_DROPDOWN:
				    $columnValue = function($data) use ($fieldValueName, $fieldId) {
				        // Don't worry, it is script-cached, SQL executed once per page only
				        $options = CoreUserFieldDropdownTranslations::getAllOptions(false);
				        if (empty($options)) return "";
				        return $options[$data[$fieldValueName]];
				    };
					break;

				// Other field types, just return the raw value of the record field
				default:
				    $columnValue = function($data) use ($fieldValueName) {
				        return $data[$fieldValueName];
				    };
					break;
			}
		}


		$columns[$fieldValueName] = array(
			'header' => CHtml::encode($header),
			'value' => $columnValue,
		);

		return $columns;
	}

	/**
	 * Get column definition for a single COURSE additional field
	 *
	 * @param number $fieldId  (learning_course_field->id_field)
	 * @return array
	 */
	public function getSingleColumnForAdditionalCourseField($fieldId) {
		$columns = array();

		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		// Get field name in current language
		$translation = LearningCourseField::model()->getTranslation((int) $fieldId);
		if (!$translation) {
			$header = "field: $fieldId";
		} else {
			$header = $translation;
		}
		$fieldValueName = "course_field_" . $fieldId . "_value";

		$columns[$fieldValueName] = array(
			'header' => $header,
		    'value' => function($data) use ($fieldId) {
		        $idCourse = $data['course.idCourse'];
		        if (!$idCourse) return "";
                $val = LearningCourse::model()->getAdditionalFieldValue($fieldId, $idCourse);  
                $val = strip_tags($val);
		        return $val;
		    }
		);
		return $columns;
	}


	/**
	 * Get column definition for a single COURSE additional field
	 *
	 * @param number $fieldId  (learning_course_field->id_field)
	 * @return array
	 */
	public function getSingleColumnForAdditionalEnrollmentField($fieldId) {
		$columns = array();

		$sql='SELECT JSON_UNQUOTE(t.translation->\'$[0]."'.Yii::app()->getLanguage().'"\') as field_name FROM learning_enrollment_fields t WHERE t.id='.$fieldId;
        $connection=Yii::app()->db;
        $customFieldRaw=$connection->createCommand($sql)->queryRow();

		$customFieldName='';
		if($customFieldRaw){
			$customFieldName=$customFieldRaw['field_name'];
		}

		$fieldValueName = "field_" . $fieldId . "_value";
	
		$columns[$fieldValueName] = array(
			'header' => $customFieldName,
		    'value' => function($data) use ($fieldId) {
                $idCourse = $data['course.idCourse'];
                if (!$idCourse) return "";
                $val = LearningCourseuser::getAdditionalFieldValueByFieldId($data["course.idCourse"],$data["enrollment.level"], $fieldId);
                $val = strip_tags($val);
                return $val;
		    },
		);
		return $columns;
	}



	/**
	 * Get column definition for a single COURSE additional field
	 *
	 * @param number $fieldId  (learning_course_field->id_field)
	 * @return array
	 */
	public function getSingleColumnForExternalTrainingAdditionalField($fieldId) {
		$columns = array();
		// Get field name in current language
		$translation = TranscriptsField::model()->getTranslation((int) $fieldId);
		if (!$translation) {
			$header = "field: $fieldId";
		} else {
			$header = $translation;
		}
		$fieldValueName = "field_" . $fieldId . "_value";
		$fieldType = Yii::app()->db->createCommand()
			->select('type')
			->from(TranscriptsField::model()->tableName())
			->where('id_field = ' . ((int) $fieldId))
			->queryScalar();

		$columns[$fieldValueName] = array(
			'header' => $header,
			'value' => '($data["external_trainings.id_record"])?(($val = TranscriptsRecord::model()->getAdditionalFieldValue(' . $fieldId . ',$data["external_trainings.id_record"]))? $val : "" ):""', //$columnValue,
		);

		if ($fieldType == TranscriptsField::TYPE_FIELD_TEXTAREA) {
			if (isset($_REQUEST['exportType'])) {
				$columns[$fieldValueName]['value'] = '($data["external_trainings.id_record"])?(($val = TranscriptsRecord::model()->getAdditionalFieldValue(' . $fieldId . ',$data["external_trainings.id_record"]))? strip_tags($val) : "" ):""';
			}
			$columns[$fieldValueName]['type'] = 'raw';
		}

		return $columns;
	}

	/**
	 * Return an array of fileds, defining their order in the report, per report type
	 * Also rasing an event to ask the world for any additional definitions
	 *
	 */
	public static function columnsOrder() {

		$result = self::$columnsOrder;

		Yii::app()->event->raise('OnReportColumnsOrder', new DEvent(self, array(
			'columnsOrder' => &$result
		)));

		return $result;
	}

	/**
	 * Reorder columns.
	 *
	 * When the full list of columns is ready, pass here to put them in the required order to show in grids and exports.
	 * The required order is set in a class static variable $columnsOrder
	 *
	 * @param number $reportType
	 * @param array $inputColumns
	 * @return array
	 */
	public function reorderColumns($reportType, $inputColumns) {
		$columns = array();
		$columnsOrder = self::columnsOrder();

		if (isset($columnsOrder[$reportType]) && !empty($columnsOrder[$reportType])) {
			foreach ($columnsOrder[$reportType] as $columnKey) {
				if (array_key_exists($columnKey, $inputColumns)) {
					$columns[$columnKey] = $inputColumns[$columnKey];
				}
			}
		} else {
			$columns = $inputColumns;
		}
		return $columns;
	}

	/**
	 * Return selected GROUPS only (not counting Org Chart based ones!)
	 *
	 */
	public function getGroupsOnlyFromFilter() {

		$filter = CJSON::decode($this->filter_data);

		// Old and new filter format differ. Watch out
		if ($this->isOldFormat()) {
			$groups = isset($filter['gridData']['select-group-grid-selected-items']) && (is_string($filter['gridData']['select-group-grid-selected-items'])) ?
				explode(',', $filter['gridData']['select-group-grid-selected-items']) :
				array();
		} else {
			$groups = isset($filter['groups']) ? $filter['groups'] : array();
		}

		return array_unique($groups);
	}

	/**
	 * Return selected Org Chart NODES ONLY  (idOrg's)
	 *
	 * @return array(<orgChartId> => <selectStatte/Depth>)
	 */
	public function getOrgchartsOnlyFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		// New approach
		$orgChartsNew = isset($filter['orgchartnodes']) ? $filter['orgchartnodes'] : array();

		// Old approach (Quartz): backward compat.
		$orgChartsOld = isset($filter['gridData']['select-orgchart']) ? $filter['gridData']['select-orgchart'] : array();
		$orgcharts = array();
		if (count($orgChartsOld) > 0) {
			foreach ($orgChartsOld as $key => $selectState) {
				$orgcharts[] = array('key' => $key, 'selectState' => $selectState);
			}
		}

		$orgcharts = array_merge($orgcharts, $orgChartsNew);

		return $orgcharts;
	}

	/**
	 * Given the filter data, resolve list of groups based on Groups and Org Chart selections
	 *
	 * @return array
	 */
	public function getGroupsFromFilter() {

		// Explicitely selected groups
		$groupsOnly = $this->getGroupsOnlyFromFilter();

		// Add groups of the selected Org Chart Nodes, respecting selected "depth" (select state)
		$filter = CJSON::decode($this->filter_data);
		$orgCharts = $this->getOrgchartsOnlyFromFilter();
		$orgChartGroups = array();
		foreach ($orgCharts as $orgChartSelection) {
			$orgChartNodeModel = CoreOrgChartTree::model()->findByPk($orgChartSelection['key']);
			if ($orgChartNodeModel) {
				if ($orgChartSelection['selectState'] == 1) {
					$orgChartGroups[] = $orgChartNodeModel->idst_oc;
				} else if ($orgChartSelection['selectState'] == 2) {
					$orgChartGroups = array_merge($orgChartGroups, $orgChartNodeModel->getBranchGroups(true));
				}
			}
		}

		// Merge into main groups list
		$groups = array_merge($groupsOnly, $orgChartGroups);

		return $groups;
	}

	/**
	 * Return selected coursepaths (Learning Plans) from filter, if any
	 *
	 * @return array
	 */
	public function getCoursepathsFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		$result = array();
		if (isset($filter['plans']) && is_array($filter['plans'])) {
			foreach ($filter['plans'] as $idCoursepath) {
				$result[] = (int) $idCoursepath;
			}
		}
		return $result;
	}

	/**
	 * Return selected courses from filter, if any
	 *
	 * @return array
	 */
	public function getCoursesFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		$result = array();
		if (isset($filter['courses']) && is_array($filter['courses'])) {
			foreach ($filter['courses'] as $idCourse) {
				$result[] = (int) $idCourse;
			}
		}
		return $result;
	}

	/**
	 * Return selected Learning Plans from filter, if any
	 *
	 * @return array
	 */
	public function getPlansFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		$result = array();
		if (isset($filter['plans']) && is_array($filter['plans'])) {
			foreach ($filter['plans'] as $idCourse) {
				$result[] = (int) $idCourse;
			}
		}
		return $result;
	}

	/**
	 * Get ALL users derived from the filter, union between 1.Directly selected, 2. From selected groups and 3. From charts
	 * @return multitype:
	 */
	public function getCombinedUsersFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$users = array();

		// Directly selected users
		$users = $this->getUsersFromFilter();

		// Add Users derived from groups (including groups derived from branches)
		$groups = $this->getGroupsFromFilter();
		if (count($groups) > 0) {
			$command = Yii::app()->db->createCommand();
			$command
				->select('idstMember')
				->from('core_group_members')
				->where('idst IN (' . implode(',', $groups) . ')');
			$reader = $command->query();
			while ($row = $reader->read()) {
				$users[] = (int) $row['idstMember'];
			}
		}


		$users = array_unique($users);
		return $users;
	}

	/**
	 * Return selected USERS from filter, if any
	 *
	 * @return array
	 */
	public function getUsersFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		// Old and new filter format differ. Watch out
		if ($this->isOldFormat()) {
			$users = isset($filter['gridData']['select-user-grid-selected-items']) && (is_string($filter['gridData']['select-user-grid-selected-items'])) ?
				explode(',', $filter['gridData']['select-user-grid-selected-items']) :
				array();
		} else {
			$users = isset($filter['users']) ?
				$filter['users'] :
				array();
		}
		return array_filter($users);
	}

	/**
	 * Return selected ASSETS from filter, if any
	 *
	 * @return array
	 */
	public function getAssetsFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		return isset($filter['assets']) ? $filter['assets'] : array();
	}

	/**
	 * Return selected CHANNELS from filter, if any
	 *
	 * @return array
	 */
	public function getChannelsFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		return isset($filter['channels']) ? $filter['channels'] : array();
	}

	/**
	 * Return selected CHANNELS from filter, if any
	 *
	 * @return array
	 */
	public function getBadgesFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$badges = array();

		if ($this->isOldFormat()) {
			return '';
		} else {
			$badges = isset($filter['badges']) ? $filter['badges'] : array();
		}

		return $badges;
	}

	public function getCertificationFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$certifications = array();

		if ($this->isOldFormat()) {
			return '';
		} else {
			$certifications = isset($filter['certifications']) ? $filter['certifications'] : array();
		}

		return $certifications;
	}

	public function getContestsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$certifications = array();

		if ($this->isOldFormat()) {
			return '';
		} else {
			$certifications = isset($filter['contests']) ? $filter['contests'] : array();
		}

		return $certifications;
	}

	public function getExternalTrainingsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$externalTrainings = array();

		if ($this->isOldFormat()) {
			return '';
		} else {
			$externalTrainings = isset($filter['external_trainings']) ? $filter['external_trainings'] : array();
		}

		return $externalTrainings;
	}

	/**
	 * Return selected User ADDITIONAL FIELDS from filter, to be shown in the report
	 *
	 * @return array
	 */
	public function getAdditionalFieldsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$result = array();
		$userFields = isset($filter['fields']['user']) ? $filter['fields']['user'] : array();

		foreach ($userFields as $field => $flag) {
			if (is_int($field) && $flag == 1) {
				$result[] = (int) $field;
			}
		}
		Yii::app()->event->raise('ModifyAdditionalFieldsForReport', new DEvent($this, array('result' => &$result)));
		return $result;
	}

	/**
	 * Return selected Course ADDITIONAL FIELDS from filter, to be shown in the report
	 *
	 * @return array
	 */
	public function getAdditionalCourseFieldsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$result = array();
		$courseFields = isset($filter['fields']['course']) ? $filter['fields']['course'] : array();

		foreach ($courseFields as $field => $flag) {
			if (is_int($field) && $flag == 1) {
				$result[] = (int) $field;
			}
		}

		return $result;
	}


	/**
	 * Return selected Course ADDITIONAL FIELDS from filter, to be shown in the report
	 *
	 * @return array
	 */
	public function getAdditionalEnrollmentFieldsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$result = array();
		$enrollmentFields = isset($filter['fields']['enrollment']) ? $filter['fields']['enrollment'] : array();

		foreach ($enrollmentFields as $field => $flag) {
			if (is_int($field) && $flag == 1) {
				$result[] = (int) $field;
			}
		}

		return $result;
	}


	/**
	 * Return selected Course ADDITIONAL FIELDS from filter, to be shown in the report
	 *
	 * @return array
	 */
	public function getExternalTrainingAdditionalFieldsFromFilter() {
		$filter = CJSON::decode($this->filter_data);

		$result = array();
		$courseFields = isset($filter['fields']['external_trainings']) ? $filter['fields']['external_trainings'] : array();
		// Get the table in order to check if the column exists
		$table = Yii::app()->db->schema->getTable(TranscriptsFieldValue::model()->tableName(), true);
		foreach ($courseFields as $field => $flag) {
			// Check if the column exists in first place
			$columnExists = isset($table->columns['field_' . $field]);
			if (is_int($field) && $flag == 1 && $columnExists) {
				$result[] = (int) $field;
			}
		}

		return $result;
	}

	/**
	 * Report may have some "filtering" options, like "show suspended only", "show in progress users only" etc.
	 * This method returns them in an array.
	 *
	 * @return array
	 */
	public function getFilteringOptionsFromFilter($checkFilters = false) {
		$filter = CJSON::decode($this->filter_data);

		$result = array();

		if ($checkFilters) {
			// Check for "date/time" filters and set to ISO format
			if (isset($filter['filters']['date_from']) && $filter['filters']['date_from']) {
				$filter['filters']['date_from'] = Yii::app()->localtime->toLocalDate($filter['filters']['date_from']);
			};
			if (isset($filter['filters']['date_to']) && $filter['filters']['date_to']) {
				$filter['filters']['date_to'] = Yii::app()->localtime->toLocalDate($filter['filters']['date_to']);
			};
			if (isset($filter['filters']['courses_expiring_before']) && $filter['filters']['courses_expiring_before']) {
				$filter['filters']['courses_expiring_before'] = Yii::app()->localtime->toLocalDate($filter['filters']['courses_expiring_before']);
			};
			if (isset($filter['filters']['expiration_date_from']) && $filter['filters']['expiration_date_from']) {
				$filter['filters']['expiration_date_from'] = Yii::app()->localtime->toLocalDate($filter['filters']['expiration_date_from']);
			};
			if (isset($filter['filters']['expiration_date_to']) && $filter['filters']['expiration_date_to']) {
				$filter['filters']['expiration_date_to'] = Yii::app()->localtime->toLocalDate($filter['filters']['expiration_date_to']);
			}
		}

		if (isset($filter['order']) && is_array($filter['order'])) {
			$filter['filters']['order'] = $filter['order'];
		}


		$result = isset($filter['filters']) ? $filter['filters'] : array();

		return $result;
	}

	/**
	 * Return an array of  (bundle.fieldname => flag). Filter by "fields bundle" name, if provided
	 *
	 * 		Example:
	 *  	array(
	 *  		course.name => 1,
	 *  		stat.total_subscribed_users => 1,
	 *  		stat->show_percents => 0,
	 *  	)
	 *
	 *  These fields are known in advance and MUST be used in SQL "AS <field-name>"
	 *  	Example:
	 *    	SELECT name as `course.name` FROM learning_course
	 *
	 *  	Note  "`" around the "course.name", because "." (the dot) is not allowed by SQL as part of field name
	 *
	 * @param string $bundleName
	 * @return array
	 */
	public function getSelectionFieldsFromFilter($bundleName = false) {
		$filter = CJSON::decode($this->filter_data);
		$result = array();

		if (!isset($filter['fields'])) {
			return $result;
		}

		foreach ($filter['fields'] as $bundleKey => $fields) {
			foreach ($fields as $field => $flag) {
				if (($bundleName === false) || ($bundleName == $bundleKey )) {
					$result[$bundleKey . "." . $field] = (int) $flag;
				}
			}
		}

		return $result;
	}

	/**
	 * Extracts data from saved filter and returns list of fields for the given "fields" bundle (group, user, stat, ...).
	 * Column is added only if that field "show" flag is set to 1  (the checkbox in the last screen of Report creation procedure)
	 *
	 * Example:  getFieldsBundleColumns('course'):
	 * 	Result is:
	 * 		array (
	 * 			array('header' => "Course name", 		'value' => '$data["course.name"]),
	 * 			array('header' => "Course category", 	'value' => '$data["course.coursesCategory.translation"]),
	 * 			....
	 * 		)
	 *
	 * Additional checks are done to normalize/fix some information and/or to add percentage columns.
	 * This is sort of complex method, but provides a flexible way to show/hide columns in grids and exports.
	 * Be patient and read it carefully!
	 *
	 * @param string $bundleName  'group', 'user', 'stat', ...
	 * @param boolean|array $excludeFields  False: No percentages will be calculated
	 * @param string $statBaseField Name of field used to calculate percentages
	 * @return array
	 */
	public function getFieldsBundleColumns($bundleName, $excludeFields = false, $statBaseField = false, $convertDateToUserFormat = true) {

		$filter = CJSON::decode($this->filter_data);
		$columns = array();


		if ($excludeFields === false || !is_array($excludeFields)) {
			$excludeFields = array();
		}

		$fields = $this->getSelectionFieldsFromFilter($bundleName);



		foreach ($fields as $field => $flag) {

			if (((int) $flag > 0) && (!in_array($field, $excludeFields))) {
				$skipPercentage = false;

				/*				 * ******************************************************* */
				// AD-HOC BUG FIXES and normalization
				// We have some bugs in Report Filter data saved by report creation process. Fix them on the fly:
				// 1. fileds['group'['course_name'] is irrelevant, why it is in "group" bundle?? => SKIP
				if ($field == self::F_GROUP_COURSE_NAME) {
					continue;
				}

				// 2. Skip "options" fields
				if (in_array($field, self::$optionFields)) {
					continue;
				}

				// 3. stat.total_times spent in course is NOT subject to percentage calculations
				if ($field == self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE) {
					$skipPercentage = true;
				}
				// END OF AD-HOX FIXES
				/*				 * ******************************************************* */

				if ($field == self::F_CERTIFICATION_ACTIVITY) {
					$columns = array_merge($columns, $this->getSingleColumn(self::F_CERTIFICATION_ACTIVITY_NAME, $statBaseField, $skipPercentage));
					$columns = array_merge($columns, $this->getSingleColumn(self::F_CERTIFICATION_ACTIVITY_TYPE, $statBaseField, $skipPercentage));
					continue;
				}

				// Add next column, calling common method
				$columns = array_merge($columns, $this->getSingleColumn($field, $statBaseField, $skipPercentage, $convertDateToUserFormat));
			}
		}

		return $columns;
	}

	/**
	 * Array of translated filed/column names used in custom report grids and exports
	 *
	 * @return array
	 */
	public static function fieldNames() {

		$result = array(
			// COURSE
			self::F_COURSE_ID => Yii::t('standard', 'Course ID'),
			self::F_COURSE_NAME => Yii::t('standard', '_COURSE_NAME'),
			self::F_COURSE_CATEGORY => Yii::t('standard', '_CATEGORY'),
			self::F_COURSE_CODE => Yii::t('standard', '_COURSE_CODE'),
			self::F_COURSE_STATUS => Yii::t('standard', '_STATUS'),
			self::F_COURSE_CREDITS => Yii::t('standard', '_CREDITS'),
			self::F_COURSE_DATE_BEGIN => Yii::t('standard', '_DATE_BEGIN'),
			self::F_COURSE_DATE_END => Yii::t('standard', '_DATE_END'),
			self::F_COURSE_EXPIRED => Yii::t('notification', 'Course has expired'),

			self::F_STAT_TYPE_OF_COURSE => Yii::t('standard', 'Type of course'),
			self::F_STAT_INTERNAL_COURSE_ID => Yii::t('standard', 'Internal course id'),
			self::F_GROUP_DESCRIPTION => Yii::t('standard', 'Group Description'),

			// GROUP
			self::F_GROUP_NAME => Yii::t('standard', 'Org/Group name'),
			self::F_GROUP_GROUPID => Yii::t('standard', 'Internal group id'),
			self::F_GROUP_IDST => Yii::t('standard', 'Group ID'),
			self::F_GROUP_TYPE => Yii::t('standard', '_TYPE'),
			self::F_GROUP_TOTAL_USERS => Yii::t('standard', 'Members'),
			self::F_GROUP_COURSE_NAME => Yii::t('standard', '_COURSE_NAME'),
			// STAT
			self::F_STAT_TOTAL_SUBSCRIBED_USERS => Yii::t('course', '_COURSE_USERISCR'),
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE => Yii::t('standard', '_NOT_STARTED'),
			self::F_STAT_NUMBER_USERS_NOT_STARTED_COURSE_PERCENTAGE => '%',
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE => Yii::t('standard', '_USER_STATUS_BEGIN'),
			self::F_STAT_NUMBER_USERS_IN_PROGRESS_COURSE_PERCENTAGE => '%',
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE => Yii::t('standard', '_USER_STATUS_END'),
			self::F_STAT_NUMBER_USERS_COMPLETED_COURSE_PERCENTAGE => '%',
			self::F_STAT_TOTAL_TIME_ALL_USERS_IN_COURSE => Yii::t('standard', '_TOTAL_TIME'),
			self::F_STAT_AVERAGE_EVALUATION => Yii::t('social_rating', 'Rating'),
			// USER
			'user.idst'												=> Yii::t('standard', 'User unique ID'),
			self::F_USER_IDST										=> Yii::t('standard', 'User unique ID'),
			self::F_USER_USERID										=> Yii::t('admin_directory', '_DIRECTORY_FILTER_userid'),
			self::F_USER_FIRSTNAME									=> Yii::t('standard', '_FIRSTNAME'),
			self::F_USER_LASTNAME									=> Yii::t('standard', '_LASTNAME'),
			self::F_USER_EMAIL										=> Yii::t('standard', '_EMAIL'),
			self::F_USER_REGISTER_DATE								=> Yii::t('report', '_CREATION_DATE'),
			self::F_USER_VALID										=> Yii::t('standard', '_ACTIVE'),//NOTE: this seems not to be actually used and is the same (inverted) of F_USER_SUSPENDED
			self::F_USER_SUSPEND_DATE								=> Yii::t('standard', 'Suspend Date'),
			self::F_USER_SUSPENDED									=> Yii::t('standard', 'Deactivated'),//Yii::t('standard', '_SUSPENDED'), //NOTE: this is actually used and is the same of "F_USER_VALID" (inverted)
			self::F_USER_GROUPS_LIST	                            => Yii::t('standard', 'branches'),
			self::F_USER_EXPIRATION		                            => Yii::t('standard', 'Expiration'),
			self::F_USER_EMAIL_STATUS	                            => Yii::t('standard', 'Email validation status'),
			self::F_AUDIT_TRAIL_APPROVED_BY	                        => Yii::t('audit_trail', 'Approved By'),
			// self::F_USER_GROUPS_LIST	--> Created and updated in getGridColumns() /- AuditTrailLogCustomReportHelper -/

			// ENROLLMENT
			self::F_ENROLL_LEVEL => Yii::t('standard', '_LEVEL'),
			self::F_ENROLL_DATE_ENROLLMENT => Yii::t('report', '_DATE_INSCR'),
			self::F_ENROLL_DATE_FIRST_ACCESS => Yii::t('standard', '_DATE_FIRST_ACCESS'),
			self::F_ENROLL_DATE_LAST_ACCESS => Yii::t('standard', '_DATE_LAST_ACCESS'),
			self::F_ENROLL_DATE_COMPLETE => Yii::t('report', '_DATE_COURSE_COMPLETED'),
			self::F_ENROLL_STATUS => Yii::t('standard', '_STATUS'),
			self::F_ENROLL_USER_NUMBER_SESSIONS => Yii::t('report', '_TH_USER_NUMBER_SESSION'),
			self::F_ENROLL_USER_TIME_IN_COURSE => Yii::t('profile', '_ACCESS_TIME'),
			self::F_ENROLL_DATE_BEGIN_VALIDITY => Yii::t('standard', 'Enrollment start of validity'),
			self::F_ENROLL_DATE_EXPIRE_VALIDITY => YII::t('standard', 'Enrollment end of validity'),
			self::F_ENROLL_USER_SCORE_GIVEN => Yii::t('standard', '_FINAL_SCORE'),
			self::F_ENROLL_USER_INITIAL_SCORE_GIVEN => Yii::t('report', '_COURSES_FILTER_SCORE_INIT'),
			self::F_ENROLL_SUBSCRIPTION_CODE => Yii::t('standard', 'Subscription code'),
			self::F_ENROLL_SUBSCRIPTION_SET => Yii::t('standard', 'Subscription code set'),
			//LO
			self::F_LO_TYPE => Yii::t('report', '_LO_COL_TYPE'),
			self::F_LO_TITLE => Yii::t('report', 'L.O. Title'),
			self::F_LO_TRACK_DATE_FIRST_ATTEMPT => Yii::t('report', '_LO_COL_FIRSTATT'),
			self::F_LO_TRACK_DATE_LAST_ATTEMPT => Yii::t('standard', 'Date Attempt'),
			self::F_LO_TRACK_USER_STATUS => Yii::t('standard', '_STATUS'),
			self::F_LO_TRACK_USER_SCORE => Yii::t('standard', '_SCORE'),
			self::F_LO_MILESTONE => Yii::t('organization', '_ORGMILESTONE'),
			self::F_LO_VERSION => Yii::t('standard', 'Version'),
			// USERS Delay
			self::F_UDELAY_DATE_TO_COMPLETE => Yii::t('report', '_DATE_COURSE_COMPLETED'),
			self::F_UDELAY_DAYS_LEFT_TO_COMPLETE => Yii::t('standard', 'Days Left'),
			// Ecommerce
			self::F_ECOMMERCE_ID_TRANSACTION => Yii::t('report', 'Transaction ID'),
			self::F_ECOMMERCE_ITEM_CODE => Yii::t('report', 'Course/LP code'),
			self::F_ECOMMERCE_ITEM_NAME => Yii::t('report', 'Course/LP name'),
			self::F_ECOMMERCE_ITEM_QUANTITY => Yii::t('report', 'Quantity'),
			self::F_ECOMMERCE_ITEM_SESSION_NAME => Yii::t('report', 'ILT/Webinar session name'),
			self::F_ECOMMERCE_ITEM_SESSION_START_DATE => Yii::t('report', 'Start date'),
			self::F_ECOMMERCE_ITEM_SESSION_END_DATE => Yii::t('report', 'End date'),
			self::F_ECOMMERCE_ITEM_SINGLE_PRICE => Yii::t('report', 'Price'),
			self::F_ECOMMERCE_DISCOUNT => Yii::t('coupon', 'Discount'),
			self::F_ECOMMERCE_ITEM_TYPE => Yii::t('report', 'Type'),
			self::F_ECOMMERCE_TOTAL_PRICE => Yii::t('report', 'Total price'),
			self::F_ECOMMERCE_PAYMENT_STATUS => Yii::t('report', 'Payment status'),
			self::F_ECOMMERCE_DATE_CREATED => Yii::t('report', '_CREATION_DATE'),
			self::F_ECOMMERCE_DATE_PAID => Yii::t('reports', 'Date paid'),
			self::F_ECOMMERCE_PAYMENT_TYPE => Yii::t('cart', 'Payment method'),
			self::F_ECOMMERCE_PAYMENT_TXN_ID => Yii::t('standard', 'Payment Txn Id'),
			self::F_ECOMMERCE_BILLING_ADDRESS_1 => Yii::t('standard', 'Address 1'),
			self::F_ECOMMERCE_BILLING_ADDRESS_2 => Yii::t('standard', 'Address 2'),
			self::F_ECOMMERCE_BILLING_VAT_NUMBER => Yii::t('order', 'Vat number'),
			self::F_ECOMMERCE_BILLING_BILL_COMPANY_NAME => Yii::t('billing', '_COMPANY_NAME'),
			self::F_ECOMMERCE_BILLING_BILL_CITY => Yii::t('classroom', '_CITY'),
			self::F_ECOMMERCE_BILLING_BILL_STATE => Yii::t('classroom', '_STATE'),
			self::F_ECOMMERCE_BILLING_BILL_ZIP => Yii::t('classroom', '_ZIP_CODE'),
			self::F_ECOMMERCE_COUPON_CODE => Yii::t('coupon', 'Coupon code'),
			self::F_ECOMMERCE_COUPON_DESCRIPTION => Yii::t('coupon', 'Coupon description'),
			self::F_ECOMMERCE_LOCATION => Yii::t('classroom', 'ILT location'),
			//Learning Plan
			self::F_COURSEPATH_CODE => Yii::t('certificate', 'Learning Plan Code'),
			self::F_COURSEPATH_NAME => Yii::t('standard', '_COURSEPATH'),
			self::F_COURSEPATH_CREDITS => Yii::t('standard', '_CREDITS'),
			self::F_COURSEPATH_SUBSCRIPTION_DATE => Yii::t('report', '_DATE_INSCR'),
			self::F_COURSEPATH_COMPLETION_DATE => Yii::t('report', '_DATE_COURSE_COMPLETED'),
			self::F_COURSEPATH_COMPLETION_STATUS => Yii::t('standard', '_STATUS'),
			self::F_COURSEPATH_COMPLETION_PERCENT => Yii::t('standard', '_PERCENTAGE'),
			//SESSION REPORTS
			self::F_COURSEUSER_SESSION_NAME => Yii::t('classroom', 'Session name'),
			self::F_COURSEUSER_SESSION_SCORE_BASE => Yii::t('classroom', 'Evaluation score base'),
			self::F_COURSEUSER_SESSION_DATE_BEGIN => Yii::t('certificate', 'Session date begin'),
			self::F_COURSEUSER_SESSION_DATE_END => Yii::t('certificate', 'Session date end'),
			self::F_COURSEUSER_SESSION_TOTAL_HOURS => Yii::t('certificate', 'Session total hours'),
			self::F_COURSEUSER_SESSION_EVALUATION_SCORE => Yii::t('classroom', 'Learner\'s evaluation'),
			self::F_COURSEUSER_SESSION_EVALUATION_STATUS => Yii::t('classroom', 'Evaluation Status'),
			self::F_COURSEUSER_SESSION_EVALUATION_TEXT => Yii::t('classroom', 'Learning performance evaluation'),
			self::F_COURSEUSER_SESSION_ATTENDANCE_HOURS => Yii::t('standard', '_ATTENDANCE'),
			//Certification Reports
			self::F_CERTIFICATION_NAME => Yii::t('standard', '_TITLE'),
			self::F_CERTIFICATION_DESCRIPTION => Yii::t('standard', '_DESCRIPTION'),
			self::F_CERTIFICATION_EXPIRATION => Yii::t('standard', 'Expiration'),
			self::F_CERTIFICATION_ISSUED => Yii::t('certification', 'Issued'),
			self::F_CERTIFICATION_EXPIRED => Yii::t('certification', 'Expired'),
			self::F_CERTIFICATION_ACTIVE => Yii::t('certification', 'Active'),
			self::F_CERTIFICATION_RENEW_IN => Yii::t('standard', 'To renew in'),
			self::F_CERTIFICATION_ISSUED_ON => Yii::t('gamification', 'Issued on'),
			self::F_CERTIFICATION_ACTIVITY_NAME => Yii::t('certification', 'Activity name'),
			self::F_CERTIFICATION_ACTIVITY_TYPE => Yii::t('certification', 'Activity type'),
			// EXTERNAL TRAININGS REPORTS
			self::F_EXTERNAL_TRAININGS_CERTIFICATE => Yii::t('player', 'Certificate'),
			self::F_EXTERNAL_TRAININGS_COURSE_NAME => Yii::t('standard', '_COURSE_NAME'),
			self::F_EXTERNAL_TRAININGS_COURSE_TYPE => Yii::t('catalogue', '_COURSE_TYPE'),
			self::F_EXTERNAL_TRAININGS_SCORE => Yii::t('standard', '_SCORE'),
			self::F_EXTERNAL_TRAININGS_TO_DATE => Yii::t('standard', '_DATE'),
			self::F_EXTERNAL_TRAININGS_CREDITS => Yii::t('standard', '_CREDITS'),
			self::F_EXTERNAL_TRAININGS_STATUS => Yii::t('standard', 'Approval Status'),
			self::F_EXTERNAL_TRAININGS_TRAINING_INSTITUTE => Yii::t('transcripts', 'Training institute'),
			//Learning Plan
			self::F_BADGES_DESC => Yii::t('standard', '_DESCRIPTION'),
			self::F_BADGES_ICON => Yii::t('label', 'Icon'),
			self::F_BADGES_NAME => Yii::t('automation', 'Name'),
			self::F_BADGES_SCORE => Yii::t('standard', '_SCORE'),
			self::F_BADGES_ISSUED_ON => Yii::t('gamification', 'Issued on'),
			self::F_BADGES_ASSIGNMENT_MODE => Yii::t('gamification', 'Assignment mode'),
			//Contests
			self::F_CONTEST_NAME => Yii::t('standard', '_NAME'),
			self::F_CONTEST_DESCRIPTION => Yii::t('standard', '_DESCRIPTION'),
			self::F_CONTEST_GOAL => Yii::t('gamification', 'Goal'),
			self::F_CONTEST_PERIOD => Yii::t('gamification', 'Period'),
			self::F_RANKING_GOAL_STATS => Yii::t('gamification', 'Goal Stats'),
			self::F_RANKING_REWARDS => Yii::t('gamification', 'Rewards'),
			self::F_RANKING_POSITION => Yii::t('gamification', 'Position'),
			//assets
			self::F_ASSET_TITLE => Yii::t('standard', 'Title'),
			self::F_ASSET_PUBLISHED_BY => Yii::t('standard', 'Published by'),
			self::F_ASSET_PUBLISHED_ON => Yii::t('standard', 'Published on'),
			self::F_ASSET_CHANNELS => Yii::t('standard', 'Channels'),
			self::F_ASSET_TOTAL_VIEWS => Yii::t('standard', 'Total views'),
			self::F_ASSET_RATING => Yii::t('standard', 'Asset rating'),
			self::F_ASSET_QUESTIONS => Yii::t('standard', 'Questions'),
			self::F_ASSET_ANSWERS => Yii::t('standard', 'Answers'),
			self::F_ASSET_BEST_ANSWERS => Yii::t('standard', 'Best answers'),
			self::F_ASSET_LIKES => Yii::t('standard', 'Answers likes'),
			self::F_ASSET_DISLIKES => Yii::t('standard', 'Answers dislikes'),
			self::F_ASSET_WATCH_RATE => Yii::t('standard', 'Global watch rate'),
			self::F_ASSET_REACTION_TIME => Yii::t('standard', 'Average reaction time'),
			self::F_ASSET_WATCHED => Yii::t('standard', 'Watched'),
			self::F_ASSET_NOT_WATCHED => Yii::t('standard', 'Not watched'),
			self::F_ASSET_INVITATIONS => Yii::t('standard', 'Total invited people'),
			//experts custom report
			self::F_EXPERTS_USERNAME => Yii::t('standard', 'Username'),
			self::F_EXPERTS_EXPERT_FULL_NAME => Yii::t('standard', 'Fullname'),
			self::F_EXPERTS_EMAIL => Yii::t('standard', 'Email'),
			self::F_EXPERTS_ASSIGNED_CHANNELS => Yii::t('standard', 'Assigned channels'),
			self::F_EXPERTS_QUESTION_ANSWERED => Yii::t('standard', 'Question answered'),
			self::F_EXPERTS_AVERAGE_ANSWER_TIME => Yii::t('standard', 'Average answer time'),
			self::F_EXPERTS_REQUESTS_SATISFIED => Yii::t('standard', 'Requests satisfied'),
			self::F_EXPERTS_ANSWER_LIKES => Yii::t('standard', 'Answer likes'),
			self::F_EXPERTS_ANSWER_DISLIKES => Yii::t('standard', 'Answer Dislikes'),
			self::F_EXPERTS_BEST_ANSWER => Yii::t('standard', 'Best Answer'),
			self::F_EXPERTS_RANK_BY_ANSWERS_QUALITY => Yii::t('standard', 'Rank by answer quality'),
			self::F_EXPERTS_RANK_BY_FIRST_TO_ANSWER_RATE => Yii::t('standard', 'Rank by first to answer rate'),
			self::F_EXPERTS_RANK_BY_PARTECIPATION_RATE => Yii::t('standard', 'Rank by partecipation rate'),
			self::F_EXPERTS_REVIEW_ASSETS => Yii::t('standard', 'Review assets'),
			self::F_EXPERTS_WRITEN_PEER_REVIEWS => Yii::t('standard', 'Writen peer reviews'),
			self::F_EXPERTS_ASSET_PUBLISHED => Yii::t('standard', 'Asset published'),
			self::F_EXPERTS_AVERAGE_REVIEW_TIME => Yii::t('standard', 'Average review time'),
			self::F_EXPERTS_RANK_BY_PARTECIPATION_RATE_PR => Yii::t('standard', 'Rank by partecipation rate'),
			self::F_EXPERTS_RANK_BY_FIRST_TO_REVIEW => Yii::t('standard', 'First to Review'),

			//channels custom report
			self::F_CHANNELS_NAME                                    => Yii::t('standard', 'Channel name'),
			self::F_CHANNELS_DESCRIPTION                             => Yii::t('standard', 'Channel description'),
			self::F_CHANNELS_PUBLISHED_ASSETS                        => Yii::t('standard', 'Published assets'),
			self::F_CHANNELS_SUBMITTED_ASSETS                        => Yii::t('standard', 'Submitted assets'),
			self::F_CHANNELS_UNPUBLISHED_ASSETS                      => Yii::t('standard', 'Unpublished assets'),
			self::F_CHANNELS_ASSETS_RATING                           => Yii::t('standard', 'Average assets rating'),
			self::F_CHANNELS_TOTAL_CONTRIBUTORS                      => Yii::t('standard', 'Total contributors'),
			self::F_CHANNELS_BEST_ANSWERS                            => Yii::t('standard', 'Best answers'),
			self::F_CHANNELS_QUESTIONS                               => Yii::t('standard', 'Questions'),
			self::F_CHANNELS_ANSWERS                                 => Yii::t('standard', 'Answers'),
			self::F_CHANNELS_EXPERTS                                 => Yii::t('standard', 'Channel experts'),
			self::F_CHANNELS_ASKERS                                  => Yii::t('standard', 'Askers involved'),
			self::F_CHANNELS_PERCENT_PR                              => Yii::t('standard', '% of peer reviews compared to total channels'),
			self::F_CHANNELS_PERCENT_SHARED                          => Yii::t('standard', '% of shared assets compared to total channels'),
			self::F_CHANNELS_PERCENT_QA                              => Yii::t('standard','% of q&a compared to total channels'),


			self::F_USER_CONTRIBUTION_UID =>                          Yii::t('standard', 'User unique ID'),
			self::F_USER_CONTRIBUTION_FIRSTNAME =>                    Yii::t('standard', 'First Name'),
			self::F_USER_CONTRIBUTION_LASTNAME =>                     Yii::t('standard', 'Last Name'),
			self::F_USER_CONTRIBUTION_EMAIL =>                        Yii::t('standard', 'Email'),
			self::F_USER_CONTRIBUTION_CREATION_DATE =>                Yii::t('standard', 'Creation Date'),
			self::F_USER_CONTRIBUTION_ACTIVE_DATE =>                  Yii::t('standard', 'Active'),
			self::F_USER_CONTRIBUTION_SUSPEND_DATE =>                 Yii::t('standard', 'Suspend Date'),
			self::F_USER_CONTRIBUTION_EXPIRY =>                       Yii::t('standard', 'Expiry'),
			self::F_USER_CONTRIBUTION_EMAIL_VALIDATION_STATUS =>      Yii::t('standard', 'Email Validation Status'),
			self::F_USER_CONTRIBUTION_BRANCHES =>                     Yii::t('standard', 'Branches'),
			self::F_USER_CONTRIBUTION_ROLE =>                         Yii::t('standard', 'Role'),
			self::F_USER_CONTRIBUTION_UPLOADED_ASSETS =>              Yii::t('standard', 'Uploaded Assets'),
			self::F_USER_CONTRIBUTION_INVOLVED_CHANNELS =>            Yii::t('standard', 'Involved Channels'),
			self::F_USER_CONTRIBUTION_PUBLISHED_STATUS =>             Yii::t('standard', 'Published Status'),
			self::F_USER_CONTRIBUTION_UNUBLISHED_STATUS =>            Yii::t('standard', 'Unpublished Status'),
			self::F_USER_CONTRIBUTION_PRIVATE_ASSETS =>               Yii::t('standard', 'Private Assets')


		);

		Yii::app()->event->raise('OnReportFilterFieldNames', new DEvent(self, array(
			'arrayOfNames' => &$result
		)));

		return $result;
	}

	/**
	 * Get a single report filter field name, translated
	 *
	 * @param string $field
	 * @return string
	 */
	public static function fieldName($field) {
		$names = self::fieldNames();
		return isset($names[$field]) ? $names[$field] : $field;
	}

	/**
	 * Add LEFT JOINS for all additional fields from the filter
	 * @param array $additionalFieldIds
	 * @param CDbCommand $dbCommand
	 * @return CDbCommand
	 */
	public function joinAdditionalFieldsJoins(array $additionalFieldIds = array(), CDbCommand $dbCommand)
    {
		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		if(isset($additionalFieldIds['users'])){
            if(sizeof($additionalFieldIds['users'])>0){
                $fldId = '{fieldId}';
				$templateTable = "(
				    SELECT 
                        `cfue`.`id_user` AS `cfueIdUser`,
                        `cfue`.`field_{$fldId}` AS `cfueUserEntry`,
                        (
                            IF(
                                `dropdownTranslation`.`translation` IS NOT NULL,
                                `dropdownTranslation`.`translation`,
                                `cfue`.`field_{$fldId}`
                            )
                        ) AS `cfValue`
                    FROM
                        `core_user_field_value` AS `cfue` 
                        LEFT JOIN (
                                (SELECT 
                                    `cuf`.`id_field`,
                                    `cufd`.`id_option` AS `id_option`,
                                    (
                                        CASE
                                            WHEN (
                                                `cuf`.`type` = '" . CoreUserField::TYPE_DROPDOWN . "'
                                            ) 
                                            THEN `cufdt`.`translation` 
                                            ELSE NULL 
                                        END
                                    ) AS `translation` 
                                FROM
                                    `core_user_field` `cuf`
                                    LEFT JOIN `core_user_field_dropdown` `cufd`
                                        ON `cufd`.`id_field` = `cuf`.`id_field` 
                                    LEFT JOIN `core_user_field_dropdown_translations` `cufdt`
                                        ON `cufdt`.`id_option` = `cufd`.`id_option` 
                                WHERE `cuf`.`id_field` = {$fldId} 
                                    AND `cufdt`.`lang_code` = '{$languageCode}') AS `dropdownTranslation`
                            ) 
                            ON `cfue`.`field_{$fldId}` = `dropdownTranslation`.`id_option`
				) as f{$fldId}";

				$templateCondition = "f{$fldId}.cfueIdUser = user.idst";

				foreach ($additionalFieldIds['users'] as $fieldId) {
					$table = str_replace($fldId, $fieldId, $templateTable);
					$condition = str_replace($fldId, $fieldId, $templateCondition);
					$dbCommand->leftJoin($table, $condition);
				}
			}
		}

		return $dbCommand;
	}

	/**
	 * Add LEFT JOINS for all additional fields from the filter
	 * @param unknown $additionalFieldIds
	 * @param CDbCommand $dbCommand
	 * @return CDbCommand
	 */
	public function joinAdditionalFieldsJoinsOld($additionalFieldIds = array(), CDbCommand $dbCommand) {

		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$templateBase = "
		(
			SELECT
				cfue.id_user as cfueIdUser,
				cf.idField as cfueIdField,
				cfue.user_entry as cfueUserEntry,
				cf.type_field as cfueType,

				(CASE
					WHEN cf.type_field='dropdown' THEN son.translation
					WHEN cf.type_field='upload' THEN cfue.user_entry_extra
					ELSE
						cfue.user_entry END
				) as cfValue

				FROM core_field_userentry cfue
				join core_field cf on ( (cfue.id_common = cf.id_common) AND (cf.lang_code='" . $languageCode . "') AND (cf.id_common=__FIELDID__))
				left join core_field_son son on ( (son.lang_code='" . $languageCode . "') and (son.id_common_son=cfue.id_common_son)  )

		) f__FIELDID__ ";

		foreach ($additionalFieldIds as $fieldId) {
			$template = $templateBase;
			$subselect = str_replace('__FIELDID__', $fieldId, $template);
			$dbCommand->leftJoin($subselect, str_replace('__FIELDID__', $fieldId, 'f__FIELDID__.cfueIdUser=user.idst'));
		}

		return $dbCommand;
	}

	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterFind()
	 */
	public function afterFind() {
		parent::afterFind();

		// If the report is STANDRAD, override the incoming filter date with hardcoded one
		if ($this->is_standard && (array_key_exists($this->report_type_id, self::$standardReportFilters))) {
			$this->filter_data = CJSON::encode(self::$standardReportFilters[$this->report_type_id]);
		}

		$filterData = CJSON::decode($this->filter_data);
		$filters = $filterData['filters'];

		if (isset($filters['subscription_status']) && empty($filters['subscription_status'])) {
			if ($filters['subscribed_status'] == 1) {
				$filters['subscription_status'] = 'not_started';
			} else if ($filters['in_progress_status'] == 1) {
				$filters['subscription_status'] = 'in_progress';
			} else {
				$filters['subscription_status'] = 'all';
			}

			$filterData['filters'] = $filters;
			$this->filter_data = CJSON::encode($filterData);
		}
	}

	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
		// If rawFilterData is set, use it to build filter_data field
		// NOTE: rawFilterData is set ONLY by the new Custom Report Wizard
		if (isset($this->rawFilterData['Wizard']['selector-courses']['courses-filter']) && $this->rawFilterData['Wizard']['selector-courses']['courses-filter'] == 1) {
			unset($this->rawFilterData['Wizard']['selector-courses']['selected_courses']);
		}
		if (isset($this->rawFilterData['Wizard']['selector-badges']['badges-filter']) && $this->rawFilterData['Wizard']['selector-badges']['badges-filter'] == 1) {
			unset($this->rawFilterData['Wizard']['selector-badges']['selected_badges']);
		}
		if ($this->rawFilterData != false) {
			$this->filter_data = CJSON::encode($this->buildFilterDataField($this->rawFilterData));
		}
		return parent::beforeSave();
	}

	/**
	 * Build filter_data filed out of data received from Custom Report Wizard.
	 * Thus, this is closely related and tied to wizard name, selector field names and other stuff set in reportManagementController wizard action
	 *
	 * @param array $data
	 * @return string
	 */
	public function buildFilterDataField($data) {
		$result = array();
		$usersData = $data['Wizard']['selector-users'];
		$coursesData = $data['Wizard']['selector-courses'];
		$certificationData = $data['Wizard']['selector-certification'];
		$badgesData = $data['Wizard']['selector-badges'];
		$contestsData = $data['Wizard']['selector-contests'];
		$assetsData = $data['Wizard']['selector-assets'];
		if ($data['Wizard']['report-wizard-step-1-form']['idReportType'] == LearningReportType::APP7020_ASSETS) {
			$channelsData = $data['Wizard']['selector-assets'];
		} else {
			$channelsData = $data['Wizard']['selector-channels'];
		}

		$expertsData = $data['Wizard']['selector-experts'];

		$fieldsFormData = $data['Wizard']['report-wizard-step-4-form']['ReportFieldsForm'];
		if (isset($data['Wizard']['report-wizard-order-form']))
			$orderFieldsData = $data['Wizard']['report-wizard-order-form']['ReportFieldsForm']['filters'];

		$coursesList = UsersSelector::getCoursesListOnly($coursesData);
		$learningPlansList = UsersSelector::getPlansListOnly($coursesData);
		$usersList = UsersSelector::getUsersListOnly($usersData);
		$groupsList = UsersSelector::getGroupsListOnly($usersData);
		$orgchartsList = UsersSelector::getBranchesListOnly($usersData);
		$certificationsList = (isset($certificationData['certifications-filter']) && $certificationData ['certifications-filter'] == 1 ? array() : UsersSelector::getCertificationsListOnly($certificationData));
		$badgesList = UsersSelector::getBadgesListOnly($badgesData);
		$contestsList = (isset($contestsData['contests-filter']) && $contestsData['contests-filter'] == 1 ? array() : UsersSelector::getContestsListOnly($contestsData));
		$assetsList = UsersSelector::getAssetsListOnly($assetsData);
		$channelsList = UsersSelector::getChannelsListOnly($channelsData);
		$expertsList = UsersSelector::getExpertsListOnly($expertsData);

		// Check for "date/time" filters and set to ISO format
		if (isset($fieldsFormData['filters']['date_from'])) {
			$fieldsFormData['filters']['date_from'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['date_from']);
		};
		if (isset($fieldsFormData['filters']['date_to'])) {
			$fieldsFormData['filters']['date_to'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['date_to']);
		};
		if (isset($fieldsFormData['filters']['courses_expiring_before'])) {
			$fieldsFormData['filters']['courses_expiring_before'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['courses_expiring_before']);
		};
		if (isset($fieldsFormData['filters']['expiration_date_from'])) {
			$fieldsFormData['filters']['expiration_date_from'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['expiration_date_from']);
		};
		if (isset($fieldsFormData['filters']['expiration_date_to'])) {
			$fieldsFormData['filters']['expiration_date_to'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['expiration_date_to']);
		};

		if(!empty($fieldsFormData['filters']['start_date']['data']['from'])){
			$fieldsFormData['filters']['start_date']['data']['from'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['start_date']['data']['from']);
		}
		if(!empty($fieldsFormData['filters']['start_date']['data']['to'])){
			$fieldsFormData['filters']['start_date']['data']['to'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['start_date']['data']['to']);
		}
		if(!empty($fieldsFormData['filters']['end_date']['data']['from'])){
			$fieldsFormData['filters']['end_date']['data']['from'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['end_date']['data']['from']);
		}
		if(!empty($fieldsFormData['filters']['end_date']['data']['to'])){
			$fieldsFormData['filters']['end_date']['data']['to'] = Yii::app()->localtime->fromLocalDate($fieldsFormData['filters']['end_date']['data']['to']);
		}

		// Special handling for array of checkboxes for LO type filtering
		if (isset($fieldsFormData['filters']['lo_type'])) {
			$tmp = array();
			foreach ($fieldsFormData['filters']['lo_type'] as $loType => $value) {
				$tmp[] = $loType;
			}
			$fieldsFormData['filters']['lo_type'] = $tmp;
		}

		$result['filters'] = $fieldsFormData['filters'];


		$result['fields'] = $fieldsFormData['fields'];

		$result['users'] = $usersList;
		$result['plans'] = $learningPlansList;
		$result['groups'] = $groupsList;
		$result['courses'] = $coursesList;
		$result['orgchartnodes'] = $orgchartsList;
		$result['certifications'] = $certificationsList;
		$result['badges'] = $badgesList;
		$result['contests'] = $contestsList;
		$result['assets'] = $assetsList;
		$result['channels'] = $channelsList;
		$result['experts'] = $expertsList;

		// gridData
		/*
		  foreach ($orgchartsList as $nodeInfo) {
		  $result['gridData']['select-orgchart'][$nodeInfo["key"]] = $nodeInfo["selectState"];
		  }

		  $result['gridData']['select-group-grid-selected-items'] 	= implode(',', $groupsList);
		  $result['gridData']['select-group-grid-checkboxes'] 		= $groupsList;

		  $result['gridData']['select-course-grid-selected-items'] 	= implode(",", $coursesList);
		  $result['gridData']['select-course-grid-checkboxes'] 		= $coursesList;

		  $result['gridData']['select-user-grid-selected-items'] 		= implode(",", $usersList);
		  $result['gridData']['select-user-grid-checkboxes'] 			= $usersList;
		 */


		// For some reason model attributes (table fields) are also saved in filter_data
		$result['reportId'] = $this->id_filter;
		$result['author'] = $this->author;
		$result['id_filter'] = $this->id_filter;
		$result['report_type_id'] = $this->report_type_id;
		$result['creation_date'] = $this->creation_date;
		$result['filter_name'] = $this->filter_name;
		$result['is_public'] = $this->is_public;
		$result['is_standard'] = $this->is_standard;
		$result['id_job'] = $this->id_job;
		if (isset($orderFieldsData)) {
			$result['order'] = array(
				'orderBy' => $orderFieldsData['orderBy'],
				'type' => $orderFieldsData['typeOfOrder'],
			);
		}
		return $result;
	}

	/**
	 * Detect if filter format is still in Old, Quartz format
	 */
	public function isOldFormat() {
		$filter = CJSON::decode($this->filter_data);
		return isset($filter['gridData']);
	}

	/**
	 * onBeforeDelete Event handler
	 *
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {

		// Clean up scheduled job, if any
		$job = $this->scheduledJob;
		if ($job) {
			// Remove the job from Sidekiq Scheduler and the job itself
			Yii::app()->scheduler->deleteJob($job);
			$job->delete();
		}

		return parent::beforeDelete();
	}

	/**
	 * Based on the passed Report filter ID, performs report specific SQL Dataprovider composition as well as Grid Columns to show
	 *
	 * @param number $id Report filter id
	 * @param bool   $pagination
	 *
	 * @throws CException
	 * @return array of data with the following keys: dataProvider, gridColumns, reportTitle
	 */
	public static function getReportDataProviderInfo($id, $pagination = false, $customFilter = false, $convertDateToUserFormat = true) {

		// Get Report Filter data
		$reportFilterModel = LearningReportFilter::model()->findByPk($id);
		if (!$reportFilterModel) {
			throw new CException('Invalid report');
		}
		/* @var $reportFilterModel LearningReportFilter */



		// Get type and dispatch data provider retrieval.
		// We are going to use SQL Dataproviders for all report types.
		$type = $reportFilterModel->report_type_id;
		$reportTitle = $reportFilterModel->filter_name . "_" . date('Y_m_d_H_i');

		$dataProvider = null;
		$gridColumns = null;

		switch ($type) {

			case LearningReportType::GROUPS_COURSES:
				$dataProvider = $reportFilterModel->sqlDataProviderGroupsCourses($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsGroupsCourses();
				break;

			case LearningReportType::COURSES_USERS:
				$dataProvider = $reportFilterModel->sqlDataProviderCoursesUsers($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsCoursesUsers($convertDateToUserFormat);
				break;

			case LearningReportType::USERS_COURSES:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersCourses($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersCourses($convertDateToUserFormat);
				break;

			case LearningReportType::USERS_LEARNING:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersLo($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersLo($convertDateToUserFormat);
				break;

			case LearningReportType::USERS_DELAY:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersDelay($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersDelay();
				break;

			case LearningReportType::ECOMMERCE_TRANSACTIONS:
				$gridColumns = $reportFilterModel->getGridColumnsEcommerce();
				$dataProvider = $reportFilterModel->sqlDataProviderEcommerceData($pagination, $customFilter);
				break;

			case LearningReportType::USERS_COURSEPATH:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersCoursepath($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersCoursepath();
				break;

			case LearningReportType::USERS_SESSION:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersSessions($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersSessions($convertDateToUserFormat);
				break;
			case LearningReportType::CERTIFICATION_USERS:
				$dataProvider = $reportFilterModel->sqlDataProviderCertificationsUsers($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridCoulumnsCertificationsUsers();
				break;
			case LearningReportType::USERS_CERTIFICATION:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersCertifications($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumsUsersCertifications();
				break;
			case LearningReportType::USERS_EXTERNAL_TRAINING:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersExternalTrainings($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumsUsersExternalTrainings();
				break;
			case LearningReportType::USERS_BADGES :
				$dataProvider = $reportFilterModel->sqlDataProviderUsersBadges($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersBadges();
				break;
			case LearningReportType::USERS_CONTESTS:
				$dataProvider = $reportFilterModel->sqlDataProviderUsersContests($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsUsersContests();
				break;

			case LearningReportType::APP7020_ASSETS:
				$dataProvider = $reportFilterModel->sqlDataProviderAssets($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsAssets();
				break;
			case LearningReportType::APP7020_EXPERTS:
				$dataProvider = $reportFilterModel->sqlDataProviderExperts($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsExperts();
				break;

			case LearningReportType::APP7020_CHANNELS:
				$dataProvider = $reportFilterModel->sqlDataProviderChannels($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsChannels();
				break;
			case LearningReportType::APP7020_USER_CONTRIBUTIONS:
				$dataProvider = $reportFilterModel->sqlDataProviderContribution($pagination, $customFilter);
				$gridColumns = $reportFilterModel->getGridColumnsContribution();
				break;

			default:
				Yii::app()->event->raise('OnGetReportDataProviderInfo', new DEvent(self, array(
					'reportType' => $type,
					'reportFilterModel' => &$reportFilterModel,
					'pagination' => $pagination,
					'customFilter' => $customFilter,
					'gridColumns' => &$gridColumns,
					'dataProvider' => &$dataProvider
				)));
				break;
		}

		return array(
			'dataProvider' => $dataProvider,
			'gridColumns' => $gridColumns,
			'reportTitle' => $reportTitle,
		);
	}

	/**
	 * Add JOINs and WHERE's for additional user fields, used to filter the report data
	 *
	 * @param CDbCommand $command  Will be modified
	 * @param array $filterOptions
	 * @return CDbCommand
	 */
	protected function joinAndFilterAdditionalFilteringFields($command, $filterOptions) {
		$toJoin = array();
		if (isset($filterOptions['custom_fields']) && is_array($filterOptions['custom_fields'])) {
			foreach ($filterOptions['custom_fields'] as $idCommon => $filterValue) {
				if ($filterValue) {
					$alias = "f" . $idCommon;

					// Here, Field type can be retrieved and different condition could be applied,
					// e.g. for TEXTFIELD using LIKE %%
					$command->andWhere("({$alias}.cfueUserEntry='$filterValue')");

					$toJoin[] = $idCommon;
				}
			}
		}

		if (!empty($toJoin)) {
			$command = $this->joinAdditionalFieldsJoins(['users' => $toJoin], $command);
		}

		return $command;
	}
	

	/**
	 * 
	 * @param CDbCommand $command
	 * @param unknown $filterOptions
	 * @return string
	 */
	protected function joinAndFilterUserAdditionalFields(CDbCommand &$command, $filterOptions, $alias="fv") {
	    
	    $select = array();
	    
	    // Fields to include in the result (SELECT)
	    $fieldsForSelect = $this->getAdditionalFieldsFromFilter();
	    
	    // Fields to filter by (e.g. field1=100)
	    $fieldsForFilter = $this->getAdditionalFilteringFieldsFromFilter();
	    
	    $allFields = array_unique(array_merge($fieldsForFilter,$fieldsForSelect));
	    
	    $select = "";
	    if (!empty($allFields)) {
	       array_walk($allFields, function(&$item) use ($alias) {
	           $item = "{$alias}.field_{$item} AS field_{$item}_value" ;
	       });
	       
	       $select = implode(",", $allFields);
	       
	       $command->leftJoin("core_user_field_value {$alias}", "{$alias}.id_user=user.idst");
	       
	       if (!empty($fieldsForFilter)) {
	           $conditions = array();
	           $params = array();
	           foreach ($fieldsForFilter as $fieldId) {
	               $value = $filterOptions['filters']['custom_fields'][$fieldId];
	               $conditions[] = "field_$fieldId = :value_$fieldId";
                   $params[":value_$fieldId"] = $value;	               
	           }
	           $finalCondition = implode(" AND ", $conditions);
	           $command->andWhere($finalCondition);
	       }
	    }
	    
	    return array($select, $params);
	    
	}
	

	/**
	 * JOIN selected Additional User fields (to be shown in the report) and build a SELECT for them
	 *
	 * @param CDbCommand $command  Will be modified!!!!!!!!!
	 * @param array $excludeFields Fileds (id_common) to exclude from JOINs
	 *
	 * @return string
	 */
	public function joinAndBuildSelectForSelectedAdditionalFields($command, $excludeFields = array()) {

		$select = "";

		// Get additional fields, if selected to be shown in the report.
		// They are represented by INTEGER ID of the id_common from core_field table.
		// Then build all extra LEFT JOINS, joining additional fields tables (actually, a view) repeatedly for all requested fields.
		$additionalFields = $this->getAdditionalFieldsFromFilter();

		// Exclude from JOINING MAYBE ALREADY joined fileds earlier
		$additionalFieldsToJoin['users'] = array_diff($additionalFields, $excludeFields);

		$additionalFieldsToJoin['enrollment'] = $this->getAdditionalEnrollmentFieldsFromFilter();


		// Add JOINs for the resulting additional fields
		$command = $this->joinAdditionalFieldsJoins($additionalFieldsToJoin, $command);

		// Add SELECTs for additional fields (selected to be showin in report! NOT filtering by!)
		$select = "";
		foreach ($additionalFields as $fieldId) {
			$select .= "f" . $fieldId . ".cfValue as field_" . $fieldId . "_value,";  // f100.cfValue as field_100_value,
		}

		$command->select($select);

		return $select;
	}

	/**
	 * Return User ADDITIONAL FIELDS from filter, which have some value to filter by
	 *
	 * @return array
	 */
	public function getAdditionalFilteringFieldsFromFilter() {
		$filter = CJSON::decode($this->filter_data);


		$result = array();
		$filterFields = isset($filter['filters']['custom_fields']) ? $filter['filters']['custom_fields'] : array();
		foreach ($filterFields as $field => $value) {
			if (is_int($field) && $value) {
				$result[] = (int) $field;
			}
		}


		return $result;
	}

	/**
	 *
	 */
	public function afterSave() {
		if ($this->isNewRecord && $this->scenario == 'insert') {
			//added new custom report
			Yii::app()->event->raise('CustomReportAdded', new DEvent($this, array(
				'id_filter' => $this->id_filter,
				'model' => $this
			)));
		} elseif ($this->scenario == 'update') {
			//updated existing custom report
			Yii::app()->event->raise('CustomReportUpdated', new DEvent($this, array(
				'id_filter' => $this->id_filter,
				'model' => $this
			)));
		}

		parent::afterSave();
	}

	/**
	 *
	 */
	public function afterDelete() {
		//deleted custom report
		Yii::app()->event->raise('CustomReportDeleted', new DEvent($this, array(
			'id_filter' => $this->id_filter,
			'model' => $this
		)));

		parent::afterDelete();
	}

	/**
	 * Clean an array of users from the list of selected user in THIS report
	 *
	 * @param integer|array $users
	 */
	public function cleanUpUsers($usersToClean) {

		if (!is_array($usersToClean)) {
			$usersToClean = array((int) $usersToClean);
		}

		// Current list of users
		$oldUsers = $this->getUsersFromFilter();

		// If any ?
		if (!empty($oldUsers)) {
			$tmpData = CJSON::decode($this->filter_data);
			// We've got "users" array in filter data? (just double check)
			if (isset($tmpData['users'])) {
				// Substract the users to clean from current users list
				// JSON encode and save data
				$cleanedUsers = array_diff($oldUsers, $usersToClean);
				$tmpData['users'] = $cleanedUsers;
				$this->filter_data = CJSON::encode($tmpData);
				$this->save();
			}
		}
	}

	/**
	 * Clean an array of learning plans from the list of selected user in THIS report
	 *
	 * @param integer|array $plansToClean
	 */
	public function cleanUpCoursepaths($plansToClean) {
		if (!is_array($plansToClean)) {
			$plansToClean = array((int) $plansToClean);
		}
		// Current list of users
		$oldPlans = $this->getCoursepathsFromFilter();
		// If any ?
		if (!empty($oldPlans)) {
			$tmpData = CJSON::decode($this->filter_data);
			// We've got "plans" array in filter data? (just double check)
			if (isset($tmpData['plans'])) {
				// Substract the plans to clean from current plans list
				// JSON encode and save data
				$cleanedPlans = array_diff($oldPlans, $plansToClean);
				$tmpData['plans'] = $cleanedPlans;
				$this->filter_data = CJSON::encode($tmpData);
				$this->save();
			}
		}
	}

	public function hasCourses() {
		$courses = $this->getCoursesFromFilter();

		return !empty($courses);
	}

	public function hasCertifications() {
		$certifications = $this->getCertificationFromFilter();

		return !empty($certifications);
	}

	public function hasContests() {
		$contests = $this->getContestsFromFilter();

		return !empty($contests);
	}

	public function hasAssets() {
		$assets = $this->getAssetsFromFilter();

		return !empty($assets);
	}

	public function hasChannels() {
		$channels = $this->getChannelsFromFilter();

		return !empty($channels);
	}

	public function hasExperts() {
		$exp = $this->getExpertsFromFilter();

		return !empty($exp);
	}

	public function hasBadges() {
		$badges = $this->getBadgesFromFilter();

		return !empty($badges);
	}

	public function hasPlans() {
		$plans = $this->getPlansFromFilter();

		return !empty($plans);
	}

	public static function renderCertificationExpirationColumn($data, $fromReport = false) {
		$duration = $fromReport ? $data[LearningReportFilter::F_CERTIFICATION_EXPIRATION] : $data['duration'];

		if ($duration > 0) {
			$unitLabels = Certification::expirationDurationUnits();
			$html = $duration . ' ' . $unitLabels[$data['duration_unit']];
		} else {
			$html = Yii::t('standard', '_NEVER');
		}
		return $html;
	}

	public function renderNewAccessStatus($isOwner) {
		$visibilityModel = LearningReportAccess::model()->findByAttributes(
			array(
				'id_report' => $this->id_filter
			)
		);
		if (!$visibilityModel) {
			$visibilityModel = new LearningReportAccess();
			$visibilityModel->visibility_type = LearningReportAccess::TYPE_PRIVATE;
			$visibilityModel->id_report = $this->id_filter;
			$visibilityModel->save();
		}

		echo $visibilityModel->renderNewAccessFilter($isOwner);
	}

	public static function columnValuePostProcessing($field, &$value, $data) {
		switch ($field) {
			case self::F_BADGES_ICON:
			case self::F_RANKING_REWARDS:
				$value = '';
				break;
			case self::F_BADGES_ASSIGNMENT_MODE:
				$value = strip_tags($value);
				break;
			case self::F_BADGES_DESC:
			case self::F_CONTEST_DESCRIPTION:
			case self::F_CERTIFICATION_DESCRIPTION:
				$value = html_entity_decode(strip_tags($data[$field]));
				break;
		}
	}

	private function getSqlFromDateFiltersForEnrollments($dateType, $filterOptions, $fields) {
		$sql = '';
		$field = $dateType == 'start_date' ? $fields[0] : $fields[1];
		$localtime = Yii::app()->localtime;
		switch ($filterOptions[$dateType]['type']) {
			case 'ndago': // n days ago
				$filterOptions[$dateType]['data']['days_count'] = (int) $filterOptions[$dateType]['data']['days_count'];
				if ($filterOptions[$dateType]['data']['days_count'] == 0) {
					return $sql;
				}
				$days = "P{$filterOptions[$dateType]['data']['days_count']}D";
				$nDaysAgoType = $filterOptions[$dateType]['data']['combobox'];
				$permittedTypes = array('LIKE', '<', '<=', '>', '>=', '=');
				$now = $localtime->getUTCNow();

				$date = $localtime->subInterval($now, $days);

				$dateTime = explode(' ', $date);
				switch ($nDaysAgoType) {
					case '<':
						$sql = "{$field} > '{$dateTime[0]} 23:59:59'";
						break;
					case '>=':
						$sql = "{$field} LIKE '%{$dateTime[0]}%' OR {$field} < '{$dateTime[0]} 23:59:59'";
						break;
					case '>':
						$sql = "{$field} < '{$dateTime[0]} 00:00:00'";
						break;
					case '<=':
						$sql = "{$field} LIKE '%{$dateTime[0]}%' OR {$field} > '{$dateTime[0]} 00:00:00'";
						break;
					case '=':
						$sql = "{$field} LIKE '%{$dateTime[0]}%'";
						break;
				}
				break;

			case 'range': // from date -> to date
				$dateFrom = !empty($filterOptions[$dateType]['data']['from']) ? explode(' ', $filterOptions[$dateType]['data']['from']) : false;
				if ($dateFrom) {
					$dateFromFormatted = $localtime->fromLocalDate($filterOptions[$dateType]['data']['from'], 'short');
					$dateFromFull = "{$dateFromFormatted} 00:00:00";
				}

				$dateTo = !empty($filterOptions[$dateType]['data']['to']) ? explode(' ', $filterOptions[$dateType]['data']['to']) : false;
				if ($dateTo) {
					$dateToFormatted = $localtime->fromLocalDate($filterOptions[$dateType]['data']['to'], 'short');
					$dateToFull = "{$dateToFormatted} 23:59:59";
				}

				if ($dateFrom && $dateTo) {
					$sql = "{$field} BETWEEN '{$dateFromFull}' AND '{$dateToFull}'";
				} elseif ($dateFrom || $dateTo) {
					$dateFromFilter = $dateFrom ? "{$field} >= '{$dateFromFull}'" : false;
					$dateToFilter = $dateTo ? "{$field} <= '{$dateToFull}'" : false;

					$sql = ($dateFromFilter !== false ? $dateFromFilter : '') . ($dateFromFilter && $dateToFilter ? ' AND ' : '') . ($dateToFilter !== false ? $dateToFilter : '');
				}
				break;
		}
		return $sql;
	}

	public function sqlDataProviderAssets($pagination = false, $customFilter = false) {
		$assets = $this->getAssetsFromFilter();
		$channels = $this->getChannelsFromFilter();

		$defaultTimezone = Settings::get('timezone_default');

		$filterOptions = $this->getFilteringOptionsFromFilter();


		$params = array();

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' a');
		$commandBase->leftJoin(App7020ChannelAssets::model()->tableName() . ' ca', 'ca.idAsset = a.id');


		if ($assets) {
			$commandBase->orWhere('a.id IN (' . implode(',', $assets) . ')');
		}

		if ($channels) {
			$commandBase->orWhere('ca.idChannel IN (' . implode(',', $channels) . ')');
		}

		if ($filterOptions['start_date']) {
			$sqlFilter = $this->buildFilter(array('field_name_start' => "`".self::F_ASSET_PUBLISHED_ON."`"));
			if ($sqlFilter)
				$commandBase->having($sqlFilter);
		}

		$commandBase->andWhere("a.conversion_status = 20");
		$commandBase->andWhere("a.is_private = 0");
		$commandBase->group('a.id');

		$commandBase->select("
			a.id as idAsset,
			a.title													as `" . self::F_ASSET_TITLE . "`,
			(SELECT id FROM app7020_content_published WHERE idContent = idAsset AND actionType = 1 ORDER BY datePublished DESC LIMIT 1) as publishedId,
			(SELECT datePublished FROM app7020_content_published WHERE id=publishedId) as `" . self::F_ASSET_PUBLISHED_ON . "`,
			(SELECT idUser FROM app7020_content_published WHERE id=publishedId) as idPublisher,
			(SELECT IF(firstname ='' AND lastname = '', TRIM(LEADING '/' FROM userid), CONCAT(firstname, ' ' , lastname)) FROM core_user WHERE idst=idPublisher) as `" . self::F_ASSET_PUBLISHED_BY . "`,
			(SELECT COUNT(DISTINCT idUser) FROM app7020_content_history WHERE idContent=idAsset) as `" . self::F_ASSET_TOTAL_VIEWS . "`,
			(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent= idAsset) as `" . self::F_ASSET_RATING . "`,
			(SELECT COUNT(id) FROM app7020_question WHERE idContent=idAsset) as `" . self::F_ASSET_QUESTIONS . "`,
			(SELECT COUNT(app7020_answer.id) FROM app7020_answer INNER JOIN app7020_question ON
				app7020_answer.idQuestion = app7020_question.id WHERE app7020_question.idContent=idAsset) as `" . self::F_ASSET_ANSWERS . "`,
			(SELECT COUNT(app7020_answer.id) FROM app7020_answer INNER JOIN app7020_question ON
				app7020_answer.idQuestion = app7020_question.id WHERE app7020_question.idContent=idAsset AND app7020_answer.bestAnswer = 2) as `" . self::F_ASSET_BEST_ANSWERS . "`,
			(SELECT COUNT(app7020_answer_like.id) FROM app7020_answer_like INNER JOIN app7020_answer ON app7020_answer.id = app7020_answer_like.idAnswer
				INNER JOIN app7020_question ON app7020_answer.idQuestion = app7020_question.id
				WHERE app7020_question.idContent=idAsset AND app7020_answer_like.type=1) as `" . self::F_ASSET_LIKES . "`,
			(SELECT COUNT(app7020_answer_like.id) FROM app7020_answer_like INNER JOIN app7020_answer ON app7020_answer.id = app7020_answer_like.idAnswer
				INNER JOIN app7020_question ON app7020_answer.idQuestion = app7020_question.id
				WHERE app7020_question.idContent=idAsset AND app7020_answer_like.type=2) as `" . self::F_ASSET_DISLIKES . "`,
			(SELECT COUNT(DISTINCT idInvited) FROM app7020_invitations WHERE idContent=idAsset)	as `" . self::F_ASSET_INVITATIONS . "`,
			(SELECT IF(COUNT(id) > 0, 'Yes', 'No') FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent= idAsset) as `" . self::F_ASSET_WATCHED . "`,
			(SELECT IF(COUNT(id) > 0, 'No', 'Yes') FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent= idAsset) as `" . self::F_ASSET_NOT_WATCHED . "`"
		);


		$commandData = clone $commandBase;
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");


		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = '(`' . self::$remappedConstants[$filterOptions['order']['orderBy']] . '`) ' . $filterOptions['order']['type'];
		} else {
			$commandData->order = "a.title";
		}
		$numRecords = $commandCounter->queryScalar();

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);

		//Log::_($commandData->text, 80, true); die;

		$dataProvider = new CSqlDataProvider($commandData, $config);

		return $dataProvider;
	}

	/**
	 * Get Experts from filter
	 * @return array
	 */
	public function getExpertsFromFilter() {
		$filter = CJSON::decode($this->filter_data);
		$result = array();
		if (isset($filter['experts']) && is_array($filter['experts'])) {
			foreach ($filter['experts'] as $idExpert) {
				$result[] = (int) $idExpert;
			}
		}
		return $result;
	}

	public function sqlDataProviderExperts($pagination = false, $customFilter = false) {
		$user_language = App7020Channels::resolveLanguage();
		$default_language = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));


		$select = "
				core_user.userId, REPLACE(core_user.userId, '/', '') AS  `experts.username`,
				CONCAT(core_user.firstname, ' ', core_user.lastname) AS `experts.expert_full_name`,
				core_user.email AS `experts.email`,
				(

						SELECT group_concat(IFNULL(ct2.`name`, ct.`name`))  as channelName
						FROM app7020_channels as  c
						LEFT JOIN app7020_channel_translation as ct
						ON c.id=ct.idChannel AND ct.lang='{$user_language}'
						LEFT JOIN app7020_channel_translation as ct2
						ON c.id=ct2.idChannel AND ct2.lang='{$default_language}'

						WHERE c.id IN
										(
											SELECT ac.id  FROM
											app7020_channel_experts as ace
											INNER JOIN app7020_channels AS ac ON ac.id = ace.idChannel
											INNER JOIN app7020_experts as ae ON ace.idExpert  = ae.id
											WHERE ae.idUser = core_user.idst
											GROUP BY ac.id
										)
						AND IFNULL(ct2.`name`, ct.`name`) IS NOT NULL
						and {$this->buildFilter(array('field_name_start' => 'c.created'))}


				) as  `experts.assigned_channels`,
				(
					SELECT COUNT(*) as counter
					FROM app7020_question as aq
					INNER JOIN app7020_answer as aa ON aa.`idQuestion` = aq.id
					WHERE aa.`idUser` = core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'aa.created'))}
				) AS  `caf.questions_answered`,
				(
					SELECT CONCAT(
						FLOOR(HOUR(Sec_to_time(Avg (Timestampdiff (second, aq.created, aa.created)))) / 24), ' DAYS ',
						   HOUR(Sec_to_time(Avg (Timestampdiff (second, aq.created, aa.created)))) - (FLOOR(HOUR(Sec_to_time(Avg (Timestampdiff (second, aq.created, aa.created)))) / 24)*24),	' hr ',
						   MINUTE(Sec_to_time(Avg (Timestampdiff (second, aq.created, aa.created)))),	' min ',
						   SECOND(Sec_to_time(Avg (Timestampdiff (second, aq.created, aa.created)))),	' sec '
						)
					FROM app7020_question as aq
					INNER JOIN app7020_answer as aa ON aa.`idQuestion` = aq.id
					WHERE aa.`idUser` = core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'aq.created'))}
					AND {$this->buildFilter(array('field_name_start' => 'aa.created'))}
				) AS  `caf.average_answer_time`,

				(
					SELECT COUNT(*)
					FROM app7020_answer_like AS aal
					WHERE aal.`idUser` = core_user.idst
					AND aal.`type` = 1
					AND {$this->buildFilter(array('field_name_start' => 'aal.created'))}
				) AS  `caf.answers_likes`,

				(
					SELECT COUNT(*)
					FROM app7020_answer_like AS aal1
					WHERE aal1.`idUser` = core_user.idst
					AND aal1.`type` = 2
					AND {$this->buildFilter(array('field_name_start' => 'aal1.created'))}

				) AS `caf.answers_dislikes`,

				(
					SELECT COUNT(*)
					FROM app7020_answer as aa
					WHERE aa.`idUser` = core_user.idst
					AND aa.bestAnswer = 2
					AND {$this->buildFilter(array('field_name_start' => 'aa.created'))}
				) AS `caf.best_answer`,

				(
					SELECT rank FROM
						(
							SELECT
							@rn:=@rn+1 AS rank,
							t.*
							FROM
							(
								SELECT
								aa.idUser,
									(
										SELECT COUNT(*) FROM app7020_answer_like as sub_aal
										WHERE sub_aal.`type` = 1
										AND sub_aal.idUser = aa.idUser
										AND {$this->buildFilter(array('field_name_start' => 'sub_aal.created'))}
									) as LIKES,

									(
										SELECT COUNT(*) FROM app7020_answer_like as sub_aal
										WHERE sub_aal.`type` = 2
										AND sub_aal.idUser = aa.idUser
										AND {$this->buildFilter(array('field_name_start' => 'sub_aal.created'))}
									)
									as DISLIKES,

									(
										SELECT COUNT(*) FROM app7020_answer as sub_aa
										WHERE sub_aa.bestAnswer = 2
										AND sub_aa.idUser = aa.idUser
										AND {$this->buildFilter(array('field_name_start' => 'sub_aa.created'))}
									)
									as BA
									FROM app7020_answer as aa
									GROUP BY aa.idUser
									ORDER BY  LIKES DESC, BA DESC, DISLIKES ASC
							) as t,  (SELECT @rn:=0) t2
						 ) as answer_quality_rating
						 WHERE answer_quality_rating.idUser = core_user.idst
				) AS  `caf.rank_by_answers_quality`,

				(
					SELECT rank FROM (
						SELECT  @rn:=@rn+1 AS rank, idUser  FROM (
							SELECT COUNT(idUser) as c, idUser FROM
							(
								SELECT * FROM app7020_answer AS aa
								WHERE
								aa.created =
								(
									SELECT MIN(created)
									from app7020_answer as aaa
									WHERE aaa.`idQuestion` = aa.`idQuestion`
									AND {$this->buildFilter(array('field_name_start' => 'aaa.created'))}
								)
								GROUP BY `idQuestion`
							) as virtual_table
							GROUP BY idUser
							ORDER BY c DESC
						) as virtual_table_increment  ,  (SELECT @rn:=0) t2
					) as first_answer_rate
					 WHERE  idUser = core_user.idst
				) AS  `caf.rank_by_first_to_answer_rate`,

				(
					SELECT COUNT(*) FROM app7020_content_reviews as acr
					INNER JOIN app7020_content as acc ON acc.id = acr.`idContent`
					WHERE acc.`userId` = core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'acr.created'))}
				) AS  `pr.review_assets`,

				(
					SELECT COUNT(*) FROM app7020_questions_requests as aqr
					WHERE aqr.`idExpert`= core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'aqr.tooked'))}
				) AS  `caf.requests_satisfied`,

				(
					SELECT COUNT(*) FROM app7020_content_reviews as acr
					WHERE acr.`idUser` =  core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'acr.created'))}
				) AS  `pr.writen_peer_reviews`,

				(
					SELECT COUNT(*) FROM app7020_content as ac
					INNER JOIN app7020_content_published  as acp ON acp.idContent = ac.id AND acp.actionType = 1
					WHERE ac.`userId` = core_user.idst AND ac.conversion_status = 20
					AND {$this->buildFilter(array('field_name_start' => 'acp.datePublished'))}
				) AS  `pr.asset_published`,

				(
					SELECT CONCAT(
						FLOOR(HOUR(SEC_TO_TIME(Avg (Timestampdiff (second, acr.created, ac.created)))) / 24), ' DAYS ',
						   HOUR(SEC_TO_TIME(Avg (Timestampdiff (second, acr.created, ac.created)))) - (FLOOR(HOUR(Sec_to_time(Avg (Timestampdiff (second, acr.created, ac.created)))) / 24)*24),	' hr ',
						   MINUTE(SEC_TO_TIME(Avg (Timestampdiff (second, acr.created, ac.created)))),	' min ',
						   SECOND(SEC_TO_TIME(Avg (Timestampdiff (second, acr.created, ac.created)))),	' sec '
						)
					FROM app7020_content_reviews as acr
					INNER JOIN app7020_content as ac ON ac.`id` = acr.`idContent`
					WHERE ac.`userId` = core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'acr.created'))}
					AND {$this->buildFilter(array('field_name_start' => 'ac.created'))}
				) AS  `pr.average_review_time`,

				(
					SELECT rank FROM (
						SELECT  @rn:=@rn+1 AS rank, idUser, created FROM (
							SELECT COUNT(*) as c, idUser, created
							FROM `app7020_content_reviews` AS acr
							GROUP BY idUser
							ORDER BY c DESC
						) as virtual_table_increment  ,  (SELECT @rn:=0) t2
					) as d
					WHERE d.idUser =  core_user.idst
					AND {$this->buildFilter(array('field_name_start' => 'd.created'))}
				) AS `pr.rank_by_partecipation_rate`
  			";
		$users = $this->getExpertsFromFilter();
		$filterOptions = $this->getFilteringOptionsFromFilter();
		$params = array();
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select($select);
		$commandBase->from(App7020Experts::model()->tableName() . ' AS experts');
		$commandBase->leftJoin(CoreUser::model()->tableName(), 'core_user.idst = experts.idUser');
		if(!empty($users)) {
			$commandBase->where('core_user.idst IN ('.implode(',', $users).')');
		}
		$commandData = clone $commandBase;
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");

		$numRecords = $commandCounter->queryScalar();


		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = '(`' . self::$remappedConstants[$filterOptions['order']['orderBy']] . '`) ' . $filterOptions['order']['type'];
		}
		$commandBase->group('core_user.idst');

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);
		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}


	public function sqlDataProviderContribution($pagination = false, $customFilter = false) {
		$user_language = App7020Channels::resolveLanguage();
		$default_language = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		$zone = Yii::app()->localtime->getLocalDateTimeZone();
		$groups = $this->getGroupsFromFilter();
		$users = $this->getUsersFromFilter();

		$subSQL = '';
		if(!empty($groups)) {
			$subSQL = ' cu.idst IN ( '
				. '		SELECT u.idst idUser FROM core_group_members as gm '
				. '		INNER JOIN core_user as u ON gm.idstMember=u.idst '
				. '	WHERE gm.idst IN ('.implode(',', $groups).') '
				. ' )';
		}
		if(!empty($users)) {
			if(empty($subSQL)) {
				$subSQL .= ' cu.idst IN ('.  implode(',', $users).') ';
			}
			else {
				$subSQL .= ' OR cu.idst IN ('.  implode(',', $users).') ';
			}
		}


  		$select = "
 					cu.idst AS `" . self::F_USER_CONTRIBUTION_UID . "`,
					IF(cu.firstname IS NULL OR cu.firstname = '', ' --- ', cu.firstname) AS `" . self::F_USER_CONTRIBUTION_FIRSTNAME . "`,
					IF(cu.lastname IS NULL OR cu.lastname = '', ' --- ', cu.lastname)  AS `" . self::F_USER_CONTRIBUTION_LASTNAME . "`,
					IF(cu.email IS NULL OR cu.email = '', ' --- ', cu.email) AS   `" . self::F_USER_CONTRIBUTION_EMAIL . "`,
					IF(cu.register_date IS NULL, ' --- ', cu.register_date)  AS   `" . self::F_USER_CONTRIBUTION_CREATION_DATE . "`,
					(

					IF ( cu.valid, IF ( cu.expiration IS NOT NULL AND cu.expiration > '0000-00-00', DATE( CONVERT_TZ(NOW(), 'Europe/Sofia', 'UTC') ) < DATE(cu.expiration),'Yes'), 'No') ) AS  `".	self::F_USER_CONTRIBUTION_ACTIVE_DATE ."`,
					IF(cu.suspend_date IS NULL, 'N/A', cu.suspend_date) AS  `".self::F_USER_CONTRIBUTION_SUSPEND_DATE ."`,
					IF(cu.expiration IS NULL OR cu.expiration = '0000-00-00', '"."Does\'t expirte"."', cu.expiration) AS  `".self::F_USER_CONTRIBUTION_EXPIRY ."`,
					IF (cu.email_status, 'YES', 'NO') AS   `".self::F_USER_CONTRIBUTION_EMAIL_VALIDATION_STATUS ."`,

					(
						SELECT group_concat(group_member.idst)
						FROM core_group_members as group_member
						INNER JOIN core_group AS cg ON cg.idst = group_member.idst
						WHERE cu.idst = group_member.idstMember
						AND cg.groupid NOT IN (
								'/framework/level/godadmin',
								'/framework/level/admin',
								'/framework/level/user'
							)
					)as `" . self::F_USER_CONTRIBUTION_BRANCHES . "`,
 					(
						SELECT
							(
								CASE cg.groupid
								WHEN '/framework/level/godadmin' THEN
									'Super Admin'
								WHEN '/framework/level/admin' THEN
									'Power User'
								ELSE
									'User'
								END
							) AS kind_user
						FROM core_group_members AS cgm
						INNER JOIN core_group AS cg ON cg.idst = cgm.idst
						WHERE cgm.idstMember = cu.idst
						AND cg.groupid IN (
							'/framework/level/godadmin',
							'/framework/level/admin',
							'/framework/level/user'
						)
					) AS   `".self::F_USER_CONTRIBUTION_ROLE ."`,

					(
						SELECT COUNT(*) as a
						FROM app7020_content AS ac
						WHERE ac.userid = cu.idst
						AND ac.conversion_status >= 3
						AND {$this->buildFilter(array('field_name_start' => 'ac.created'))}


					) AS  `".self::F_USER_CONTRIBUTION_UPLOADED_ASSETS ."`,

					(
						SELECT
						GROUP_CONCAT( IFNULL(ct2.`name`,ct.`name`) ) AS channelName
						FROM app7020_channels AS c
						LEFT JOIN app7020_channel_translation AS ct ON c.id = ct.idChannel
						AND ct.lang = '{$user_language}'
						LEFT JOIN app7020_channel_translation AS ct2 ON c.id = ct2.idChannel
						AND ct2.lang = '{$default_language}'
						WHERE 	c.id IN (
								SELECT 	ac.id
								FROM app7020_channel_experts AS ace
								INNER JOIN app7020_channels AS ac ON ac.id = ace.idChannel
								INNER JOIN app7020_experts AS ae ON ace.idExpert = ae.id
								WHERE ae.idUser = cu.idst
								GROUP BY ac.id
							)

					) AS   `".self::F_USER_CONTRIBUTION_INVOLVED_CHANNELS."`,

					(
						SELECT 	COUNT(*)
						FROM app7020_content AS ac
						WHERE ac.userid = cu.idst
						AND ac.conversion_status = 20
						AND {$this->buildFilter(array('field_name_start' => 'ac.created'))}

					) AS `".self::F_USER_CONTRIBUTION_PUBLISHED_STATUS ."`,

					(
						SELECT COUNT(*)
						FROM app7020_content AS ac
						WHERE ac.userid = cu.idst
						AND ac.conversion_status = 18
						AND {$this->buildFilter(array('field_name_start' => 'ac.created'))}
 					) AS `".self::F_USER_CONTRIBUTION_UNUBLISHED_STATUS."`,

					(
						SELECT COUNT(*) FROM app7020_content AS ac
						WHERE ac.userid = cu.idst AND ac.is_private > 0
						AND {$this->buildFilter(array('field_name_start' => 'ac.created'))}

					) AS `".self::F_USER_CONTRIBUTION_PRIVATE_ASSETS."`
   			";

		$filterOptions = $this->getFilteringOptionsFromFilter();

		$params = array();
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select($select);
		$commandBase->from(CoreUser::model()->tableName() . ' AS cu');
		$commandBase->where($subSQL );
		$commandData = clone $commandBase;
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		$numRecords = $commandCounter->queryScalar();


		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = '(`' . self::$remappedConstants[$filterOptions['order']['orderBy']] . '`) ' . $filterOptions['order']['type'];
		}



		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);
		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}

	public function getGridColumnsAssets() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('asset'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('stat'));

		$columns = $this->reorderColumns(LearningReportType::APP7020_ASSETS, $columns);

		return $columns;
	}

	public function getGridColumnsContribution() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('us'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('as'));

		$columns = $this->reorderColumns(LearningReportType::APP7020_USER_CONTRIBUTIONS, $columns);

		return $columns;
	}

	/**
	 * @example $param['field_name_start'] should contains the filed who contain the date for compare. e.g `created`, `ac.date_created`
	 * @example $param['field_name_end'] = should contains the field who contain the end date for compare.  e.g `created`, `ac.date_created`
	 * @example $param['operator'] = The math operator for compare
	 * @example $param['filter_type'] = The type for filtering; That option can contains range, ndago and any;
	 * @example $param['ago_interval']; The interval does contain the substract value for date range period;
	 * @example $param['from']; The interval does contain the substract value for date range period;
	 * @example $param['to']; The interval does contain the substract value for date range period;
	 *
	 * @author Stanimir Simeonov
	 * @todo to be extend logic for calculations related to end date
	 */
	function buildFilter($params = array()) {

		$param = array();
		$param['where_statement'] = false;
		$allowed = array('field_name_start', 'field_name_end', 'operator', 'filter_type', 'ago_interval', 'where_statement', 'from', 'to');
		try {
			//get data referred to first key
			$filter = CJSON::decode($this->filter_data)['filters'][key(CJSON::decode($this->filter_data)['filters'])];

			//prepare the arguments
			foreach ($params as $param_key => $param_value) {
				if (in_array($param_key, $allowed)) {
					$param[$param_key] = $param_value;
				}
			}

			if (empty($param['operator'])) {
				$param['operator'] = $filter['data']['combobox'];
			}
			if (empty($param['filter_type'])) {
				$param['filter_type'] = $filter['type'];
			}
			if (empty($param['ago_interval'])) {
				$param['ago_interval'] = $filter['data']['days_count'];
			}
			if (empty($param['from'])) {
				$param['from'] = $filter['data']['from'];
			}
			if (empty($param['to'])) {
				$param['to'] = $filter['data']['to'];
			}

			if (!isset($params['field_name_start'])) {
				throw new Exception(Yii::t('standard', 'The common array does\'n contains required "date_field_start"'));
			}

			$sql = '';
			if ((boolean) $param['where_statement']) {
				$sql = 'WHERE ';
			}



			switch ($param['filter_type']) {
				case 'ndago':
					$this->_buildNdago($param,  $sql);
					return $sql;
				case 'range':
					$this->_buildRange($param,  $sql);
					return $sql;
				default:
					return $sql . '1';
			}
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 *
	 *
	WHERE date(aa.created) BETWEEN CURDATE() - INTERVAL 3 DAY and CURDATE(); 3 <=
	WHERE date(aa.created) BETWEEN CURDATE() - INTERVAL (3-1) DAY and CURDATE(); 3 <

	WHERE date(aa.created) BETWEEN DATE('1970-01-01')  and CURDATE() - INTERVAL 3 DAY;  3 >=
	WHERE date(aa.created) BETWEEN DATE('1970-01-01') AND CURDATE() - INTERVAL 3+1 DAY ;  > =
	 *
	 * @author Stanimir Simeonov / Scavaline
	 * @param type $param
	 * @param type $sql
	 */
	private function _buildNdago($param = array(), &$sql) {
		//CONCAT BETWEEN FOR COMMON USAGE
		if ($param['operator'] == '=') {
			$sql .= "DATE({$param['field_name_start']}) = ";
		} else {
			$sql .= "DATE({$param['field_name_start']}) BETWEEN ";

			$localTime =  Yii::app()->localtime->toLocalDateTime( null,  false,  false, 'Y-m-d H:i:s');
			$zone = Yii::app()->localtime->getLocalDateTimeZone();

  		//Accept value for current date; That calucalte it
		$current_date = "DATE(CONVERT_TZ('{$localTime}', '".$zone->getName()."', 'UTC'))";

		(int) $period = $param['ago_interval'];
			if ($param['filter_type'] == 'ndago') {
				if ($param['operator'] == '<=') {
					$sql .= "DATE('1970-01-01') AND {$current_date} - INTERVAL {$period} DAY";
				} else if ($param['operator'] == '<') {
					$period --;
					$sql .= "{$current_date} - INTERVAL {$period} DAY and {$current_date}";
				} else if ($param['operator'] == '>') {
					$period++;
					$sql .= "DATE('1970-01-01') AND {$current_date} - INTERVAL {$period} DAY";
				} else if ($param['operator'] == '>=') {
					$sql .= "DATE('1970-01-01') AND {$current_date} - INTERVAL {$period} DAY";
				} else if ($param['operator'] == '='){
					$sql .= "DATE({$current_date} - INTERVAL {$period} DAY)";
				}
			}
		}
	}

	/**
	 * Build sql fragments for range between 2 dates
	 * @param string $params
	 * @param string $sql
	 */
	private function _buildRange($params = array(), &$sql) {
		$defaultTimezone = Settings::get('timezone_default');
		$localtime = Yii::app()->localtime;
		$from_range_date = $localtime->fromLocalDate($params['from'], 'short');
		$to_range_date = $localtime->fromLocalDate($params['to'], 'short');

		$sql .= "DATE({$params['field_name_start']}) BETWEEN ";
		(int) $period = $params['ago_interval'];
		if ($params['filter_type'] == 'range') {
			$sql .= "DATE('{$from_range_date}') AND DATE('{$to_range_date}')";
		}
	}

	private function getSqlFromDateFiltersForAssets($filterOptions, $filed = "a.created") {
		$sql = '';
		$localtime = Yii::app()->localtime;
		switch ($filterOptions[$dateType]['type']) {
			case 'ndago': // n days ago
				$filterOptions[$dateType]['data']['days_count'] = (int) $filterOptions[$dateType]['data']['days_count'];
				if ($filterOptions[$dateType]['data']['days_count'] == 0) {
					return $sql;
				}
				$days = "P{$filterOptions[$dateType]['data']['days_count']}D";
				$nDaysAgoType = $filterOptions[$dateType]['data']['combobox'];
				$permittedTypes = array('LIKE', '<', '<=', '>', '>=', '=');
				$now = $localtime->getUTCNow();

				$date = $localtime->subInterval($now, $days);

				$dateTime = explode(' ', $date);
				switch ($nDaysAgoType) {
					case '<':
						$sql = "{$field} > '{$dateTime[0]} 23:59:59'";
						break;
					case '>=':
						$sql = "DATE({$field}) LIKE '%{$dateTime[0]}%' OR {$field} < '{$dateTime[0]} 23:59:59'";
						break;
					case '>':
						$sql = "{$field} < '{$dateTime[0]} 00:00:00'";
						break;
					case '<=':
						$sql = "DATE({$field}) LIKE '%{$dateTime[0]}%' OR {$field} > '{$dateTime[0]} 00:00:00'";
						break;
					case '=':
						$sql = "DATE({$field}) LIKE '%{$dateTime[0]}%'";
						break;
				}
				break;

			case 'range': // from date -> to date
				$dateFrom = !empty($filterOptions[$dateType]['data']['from']) ? explode(' ', $filterOptions[$dateType]['data']['from']) : false;
				if ($dateFrom) {
					$dateFromFormatted = $localtime->fromLocalDate($filterOptions[$dateType]['data']['from'], 'short');
					$dateFromFull = "{$dateFromFormatted} 00:00:00";
				}

				$dateTo = !empty($filterOptions[$dateType]['data']['to']) ? explode(' ', $filterOptions[$dateType]['data']['to']) : false;
				if ($dateTo) {
					$dateToFormatted = $localtime->fromLocalDate($filterOptions[$dateType]['data']['to'], 'short');
					$dateToFull = "{$dateToFormatted} 23:59:59";
				}

				if ($dateFrom && $dateTo) {
					$sql = "{$field} BETWEEN '{$dateFromFull}' AND '{$dateToFull}'";
				} elseif ($dateFrom || $dateTo) {
					$dateFromFilter = $dateFrom ? "{$field} >= '{$dateFromFull}'" : false;
					$dateToFilter = $dateTo ? "{$field} <= '{$dateToFull}'" : false;

					$sql = ($dateFromFilter !== false ? $dateFromFilter : '') . ($dateFromFilter && $dateToFilter ? ' AND ' : '') . ($dateToFilter !== false ? $dateToFilter : '');
				}
				break;
		}
		return $sql;
	}

	/**
	 *
	 * @return type
	 */
	public function getGridColumnsExperts() {
		$filter = CJSON::decode($this->filter_data);
		$columns = array();
		$columns = array_merge($columns, $this->getFieldsBundleColumns('experts'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('caf'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('pr'));
		$columns = $this->reorderColumns(LearningReportType::APP7020_EXPERTS, $columns);

		return $columns;
	}

	public function sqlDataProviderChannels($pagination = false, $customFilter = false) {
		$channels = $this->getChannelsFromFilter();

		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

		$language = App7020Channels::resolveLanguage();

		$filterOptions = $this->getFilteringOptionsFromFilter();


		$params = array();

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Channels::model()->tableName() . ' c');
		$commandBase->leftJoin(App7020ChannelTranslation::model()->tableName() . ' ct', 'ct.idChannel = c.id AND ct.lang=\''.$language.'\'');
		$commandBase->leftJoin(App7020ChannelTranslation::model()->tableName() . ' ct2', 'ct2.idChannel = c.id AND ct2.lang=\''.$defaultLanguage.'\'');

		if ($filterOptions['start_date']) {
			$sqlFilter = $this->buildFilter(array('field_name_start' => 'c.created'));
			if ($sqlFilter)
				$commandBase->where($sqlFilter);
		}

		if ($channels) {
			$commandBase->andWhere('c.id IN (' . implode(',', $channels) . ')');
		}

		$commandBase->andWhere("c.enabled = 1");
		$commandBase->andWhere("c.type = 2");

			$visibleChannels = App7020Channels::getUsersVisibleChannels(true);

			if (!$visibleChannels) {
				$dataProvider = new CArrayDataProvider(array(), array('totalItemCount' => 0, 'pagination' => false));
				return $dataProvider;
			}
			$filteredChannels = array();
			foreach ($visibleChannels as $value) {
				$filteredChannels[] = $value['idChannel'];
			}
			$commandBase->andWhere(array("IN", "c.id", $filteredChannels));
		$commandBase->group('c.id');

		$commandBase->select("
			c.id as channelId,
			IF(ct.name IS NULL, ct2.name, ct.name)													AS `" . self::F_CHANNELS_NAME . "`,

			IF(ct.description IS NULL, ct2.description, ct.description)								AS `" . self::F_CHANNELS_DESCRIPTION . "`,

			(SELECT COUNT(a.id) FROM app7020_content a INNER JOIN app7020_channel_assets ca ON
				a.id = ca.idAsset WHERE a.conversion_status = 20 AND ca.idChannel = channelId)		AS `" . self::F_CHANNELS_PUBLISHED_ASSETS . "`,

			(SELECT COUNT(a.id) FROM app7020_content a INNER JOIN app7020_channel_assets ca ON
				a.id = ca.idAsset WHERE a.conversion_status >= 10 AND ca.idChannel = channelId)		AS `" . self::F_CHANNELS_SUBMITTED_ASSETS . "`,

			(SELECT COUNT(a.id) FROM app7020_content a INNER JOIN app7020_channel_assets ca ON
				a.id = ca.idAsset WHERE a.conversion_status >= 10
				AND a.conversion_status < 20 AND ca.idChannel = channelId)							AS `" . self::F_CHANNELS_UNPUBLISHED_ASSETS . "`,

			IF((SELECT COUNT(cr.id) FROM app7020_content_rating cr INNER JOIN
				app7020_channel_assets ca ON cr.idContent = ca.idAsset WHERE
				ca.idChannel = channelId) > 0,(SELECT SUM(cr.rating) FROM app7020_content_rating cr INNER JOIN
				app7020_channel_assets ca ON cr.idContent = ca.idAsset WHERE
				ca.idChannel = channelId) / (SELECT COUNT(cr.id) FROM app7020_content_rating cr INNER JOIN
				app7020_channel_assets ca ON cr.idContent = ca.idAsset WHERE
				ca.idChannel = channelId), 0)														AS `" . self::F_CHANNELS_ASSETS_RATING . "`,

			(SELECT COUNT(DISTINCT a.userId) FROM app7020_content a INNER JOIN
				app7020_channel_assets ca ON a.id = ca.idAsset WHERE ca.idChannel = channelId)		AS `" . self::F_CHANNELS_TOTAL_CONTRIBUTORS . "`,

			(SELECT COUNT(q.id) FROM app7020_question q INNER JOIN app7020_channel_assets ca
				ON q.idContent = ca.idAsset WHERE ca.idChannel = channelId)							AS `" . self::F_CHANNELS_QUESTIONS . "`,

			(SELECT COUNT(an.id) FROM app7020_answer an INNER JOIN app7020_question q  ON
				an.idQuestion = q.id INNER JOIN app7020_channel_assets ca
				ON q.idContent = ca.idAsset WHERE ca.idChannel = channelId)							AS `" . self::F_CHANNELS_ANSWERS . "`,

			(SELECT COUNT(an.id) FROM app7020_answer an INNER JOIN app7020_question q  ON
				an.idQuestion = q.id INNER JOIN app7020_channel_assets ca
				ON q.idContent = ca.idAsset WHERE ca.idChannel = channelId AND an.bestAnswer=2)		AS `" . self::F_CHANNELS_BEST_ANSWERS . "`,

			(SELECT COUNT(DISTINCT q.idUser) FROM app7020_question q INNER JOIN app7020_channel_assets ca
				ON q.idContent = ca.idAsset WHERE ca.idChannel = channelId)							AS `" . self::F_CHANNELS_ASKERS . "`,

			(SELECT COUNT(DISTINCT idExpert) FROM app7020_channel_experts
				WHERE idChannel = channelId)															AS `" . self::F_CHANNELS_EXPERTS . "`,

			IF((SELECT COUNT(pr.id) FROM app7020_content_reviews pr INNER JOIN
				app7020_channel_assets ca ON pr.idContent = ca.idAsset) > 0,
				(SELECT COUNT(pr.id) FROM app7020_content_reviews pr INNER JOIN
				app7020_channel_assets ca ON pr.idContent = ca.idAsset WHERE
				ca.idChannel = channelId)/(SELECT COUNT(pr.id) FROM app7020_content_reviews pr INNER JOIN
				app7020_channel_assets ca ON pr.idContent = ca.idAsset), 0)													AS `" . self::F_CHANNELS_PERCENT_PR . "`,

			IF((SELECT COUNT(q.id) FROM app7020_question q INNER JOIN
				app7020_channel_assets ca ON q.idContent = ca.idAsset) > 0, (SELECT COUNT(q.id) FROM app7020_question q INNER JOIN
				app7020_channel_assets ca ON q.idContent = ca.idAsset WHERE
				ca.idChannel = channelId)/(SELECT COUNT(q.id) FROM app7020_question q INNER JOIN
				app7020_channel_assets ca ON q.idContent = ca.idAsset), 0)													AS `" . self::F_CHANNELS_PERCENT_QA . "`,

			IF((SELECT COUNT(i.id) FROM app7020_invitations i INNER JOIN
				app7020_channel_assets ca ON i.idContent = ca.idAsset) > 0, (SELECT COUNT(i.id) FROM app7020_invitations i INNER JOIN
				app7020_channel_assets ca ON i.idContent = ca.idAsset WHERE
				ca.idChannel = channelId)/(SELECT COUNT(i.id) FROM app7020_invitations i INNER JOIN
				app7020_channel_assets ca ON i.idContent = ca.idAsset), 0)													AS `" . self::F_CHANNELS_PERCENT_SHARED . "`"
		);

		$commandData = clone $commandBase;
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");


		if (isset($filterOptions['order']) && is_array($filterOptions['order']) && array_key_exists($filterOptions['order']['orderBy'], self::$remappedConstants)) {
			$commandData->order = '(`' . self::$remappedConstants[$filterOptions['order']['orderBy']] . '`) ' . $filterOptions['order']['type'];
		} else {
			$commandData->order = "c.ordering";
		}
		$numRecords = $commandCounter->queryScalar();

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $params,
		);

		//Log::_($commandData->text, 80, true); die;

		$dataProvider = new CSqlDataProvider($commandData, $config);

		return $dataProvider;
	}

	public function getGridColumnsChannels() {
		$filter = CJSON::decode($this->filter_data);

		$columns = array();

		$columns = array_merge($columns, $this->getFieldsBundleColumns('channel'));
		$columns = array_merge($columns, $this->getFieldsBundleColumns('stat'));

		$columns = $this->reorderColumns(LearningReportType::APP7020_ASSETS, $columns);

		return $columns;
	}
}
