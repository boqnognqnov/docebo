<?php

/**
 * This is the model class for table "core_scheme_color_selection".
 *
 * The followings are the available columns in table 'core_scheme_color_selection':
 * @property integer $id
 * @property integer $scheme_id
 * @property integer $color_id
 * @property string $color
 */
class CoreSchemeColorSelection extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreSchemeColorSelection the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_scheme_color_selection';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('scheme_id, color_id, color', 'required'),
            array('id, scheme_id, color_id', 'numerical', 'integerOnly'=>true),
            array('color', 'length', 'max'=>7),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, scheme_id, color_id, color', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'schemeColor' => array(self::BELONGS_TO, 'CoreSchemeColor', 'color_id'),
            'scheme' => array(self::BELONGS_TO, 'CoreScheme', 'scheme_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'scheme_id' => 'Scheme',
            'color_id' => 'Color',
            'color' => 'Color',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('scheme_id',$this->scheme_id);
        $criteria->compare('color_id',$this->color_id);
        $criteria->compare('color',$this->color,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }
}