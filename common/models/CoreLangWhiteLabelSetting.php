<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 06-Apr-16
 * Time: 11:42 AM
 *
 * @property string $language
 * @property string $type
 * @property string $value
 */

class CoreLangWhiteLabelSetting extends CActiveRecord
{

	const TYPE_HEADER_MESSAGE = 'header_message';
	const TYPE_FOOTER_TEXT = 'custom_text';
	const TYPE_FOOTER_URL = 'custom_url';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreLangWhiteLabelSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_lang_white_label_settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language, type', 'length', 'max' => 30),
			array('language, type', 'required'),
			array('value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('language, type, value', 'safe', 'on'=>'search'),
		);
	}


	public static function getCustomHeader(){
		$customMessageInHeader = '';
		$isCustomMsgActive = Settings::get('header_message_active', 0);
		if ($isCustomMsgActive == 1) {
			$currentLanguage = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
			$defaultLanguage = CoreLangLanguage::getDefaultLanguage();
			$customMessageInHeader = Yii::app()->db->createCommand()
					->select('value')
					->from(CoreLangWhiteLabelSetting::model()->tableName())
					->where('type=:type AND language=:lang', array(
							':type'=>CoreLangWhiteLabelSetting::TYPE_HEADER_MESSAGE,
							':lang'=>$currentLanguage
					))->queryScalar();
			if(empty($customMessageInHeader))
				$customMessageInHeader = Yii::app()->db->createCommand()
						->select('value')
						->from(CoreLangWhiteLabelSetting::model()->tableName())
						->where('type=:type AND language=:lang', array(
								':type' => CoreLangWhiteLabelSetting::TYPE_HEADER_MESSAGE,
								':lang' => $defaultLanguage
						))->queryScalar();
		}

		Yii::app()->event->raise('GetMultidomainHeaderMessage', new DEvent(new self, array('customMessageInHeader'=>&$customMessageInHeader)));
		return $customMessageInHeader;
	}
}