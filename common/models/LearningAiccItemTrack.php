<?php

/**
 * This is the model class for table "learning_aicc_item_track".
 *
 * The followings are the available columns in table 'learning_aicc_item_track':
 * @property integer $id
 * @property integer $idUser
 * @property integer $idItem
 * @property integer $idOrg
 * @property string $timemodified
 * @property string $lesson_status
 * @property string $lesson_location
 * @property string $lesson_mode
 * @property string $total_time
 * @property double $score_min
 * @property double $score_max
 * @property double $score_raw
 * @property string $suspend_data
 * @property string $launch_data
 * @property string $comments
 * @property string $comments_from_lms
 * @property string $first_access
 * @property string $last_access
 * @property string $cmi_json
 * @property integer $nChildCompleted
 * @property integer $nDescendantCompleted
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property LearningAiccItem $item
 */
class LearningAiccItemTrack extends CActiveRecord
{


	const LESSON_STATUS_PASSED 			= 'passed';
	const LESSON_STATUS_FAILED 			= 'failed';
	const LESSON_STATUS_COMPLETED 		= 'completed';
	const LESSON_STATUS_INCOMPLETE 		= 'incomplete';
	const LESSON_STATUS_NOT_ATTEMPTED 	= 'not attempted';
	const LESSON_STATUS_BROWSED 		= 'browsed';
	const LESSON_STATUS_ATTEMPTED 		= 'attempted';

	const LESSON_MODE_BROWSE			= 'browse';
	const LESSON_MODE_NORMAL			= 'normal';
	const LESSON_MODE_REVIEW			= 'review';

	const ENTRY_AB_INITIO				= 'ab-initio';
	const ENTRY_RESUME					= 'resume';


	static $FREEFORM_DATAMODEL_ELEMENTS = array(
		'comments',
		'core_lesson',
	);


	static $LESSON_STATUSES_MAP = array(
		'passed' 		=> 'passed',
		'completed' 	=> 'completed',
		'complete' 		=> 'completed',
		'failed' 		=> 'failed',
		'incomplete' 	=> 'incomplete',
		'browsed' 		=> 'browsed',
		'not attempted' => 'not attempted',
		'p' 			=> 'passed',
		'c' 			=> 'completed',
		'f' 			=> 'failed',
		'i' 			=> 'incomplete',
		'b' 			=> 'browsed',
		'n' 			=> 'not attempted'
	);

	static $LESSON_EXITES_MAP = array(
    	'logout' 		=> 'logout',
        'time-out' 		=> 'time-out',
        'suspend' 		=> 'suspend',
        'l' 			=> 'logout',
        't' 			=> 'time-out',
        's' 			=> 'suspend',
	);


	public $cmi;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_aicc_item_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idItem', 'required'),
			array('idUser, idItem, idOrg, nChildCompleted, nDescendantCompleted', 'numerical', 'integerOnly'=>true),
			array('score_min, score_max, score_raw', 'numerical'),
			array('lesson_status', 'length', 'max'=>128),
			array('lesson_location', 'length', 'max'=>255),
			array('lesson_mode', 'length', 'max'=>32),
			array('total_time', 'length', 'max'=>16),
			array('timemodified, suspend_data, launch_data, comments, comments_from_lms, first_access, last_access', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, idItem, idOrg, timemodified, lesson_status, lesson_location, lesson_mode, total_time, score_min, score_max, score_raw, suspend_data, launch_data, comments, comments_from_lms, first_access, last_access', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			// User involved in this tracking record
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),

			// And the AICC item tracked
			'item' => array(self::BELONGS_TO, 'LearningAiccItem', 'idItem'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUser' => 'User ID',
			'idItem' => 'Item ID',
			'idOrg' => 'Organization ID',
			'timemodified' => 'Modification time',
			'lesson_status' => 'Lesson Status',
			'lesson_location' => 'Lesson Location',
			'lesson_mode' => 'Lesson Mode',
			'total_time' => 'Total Time',
			'score_min' => 'Score Min',
			'score_max' => 'Score Max',
			'score_raw' => 'Score Raw',
			'suspend_data' => 'Suspend Data',
			'launch_data' => 'Launch Data',
			'comments' => 'Comments',
			'comments_from_lms' => 'Comments From Lms',
			'first_access' => 'First Access',
			'last_access' => 'Last Access',
			'cmi_json' => 'Cmi Json',
			'nChildCompleted' => 'N Child Completed',
			'nDescendantCompleted' => 'N Descendant Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idItem',$this->idItem);
		$criteria->compare('timemodified',$this->timemodified,true);
		$criteria->compare('lesson_status',$this->lesson_status,true);
		$criteria->compare('lesson_location',$this->lesson_location,true);
		$criteria->compare('lesson_mode',$this->lesson_mode,true);
		$criteria->compare('total_time',$this->total_time,true);
		$criteria->compare('score_min',$this->score_min);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('score_raw',$this->score_raw);
		$criteria->compare('suspend_data',$this->suspend_data,true);
		$criteria->compare('launch_data',$this->launch_data,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('comments_from_lms',$this->comments_from_lms,true);
		$criteria->compare('first_access',$this->first_access,true);
		$criteria->compare('last_access',$this->last_access,true);
		$criteria->compare('cmi_json',$this->cmi_json,true);
		$criteria->compare('nChildCompleted',$this->nChildCompleted);
		$criteria->compare('nDescendantCompleted',$this->nDescendantCompleted);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningAiccItemTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * GET (or create, if not found) tracking model for the given User/Item
	 *
	 * @param integer $idUser
	 * @param integer $idItem
	 * @param integer $idOrg
	 *
	 * @return LearningAiccItemTrack
	 */
	public static function getTrack($idUser, $idItem, $idOrg = null) {
		$attributes = array(
			'idUser'	=> (int) $idUser,
			'idItem'	=> (int) $idItem,
		);
		$model = self::model()->findByAttributes($attributes);
		if (!$model) {
			$item = LearningAiccItem::model()->findByPk($idItem);
			$model 					= new self();
			$model->lesson_status	= self::ENTRY_AB_INITIO;
			$model->idItem 			= $idItem;
			$model->idUser 			= $idUser;
			$model->idOrg 			= $idOrg;
			$model->cmi_json 		= json_encode(array());
			$model->launch_data		= $item->datafromlms;
			$model->save();
		}

		// JSON decode any saved CMI data during previos tracking activity
		$model->cmi = @json_decode($model->cmi, true);

		if (!is_array($model->cmi)) {
			$model->cmi= false;
		}

		return $model;
	}


	/**
	 * Parse incoming AICC data, if it is in INI format.
	 *
	 * Free form sections are returned as  "<name>> => "text", while others are returned as 2-D array, ex:
	 *
	 * X[<section_name>] = <free form text>
	 * X[<section_name>][<key>] = <value>
	 *
	 * There is a predefined list of possible "freeform" sections (according to CMI001(v4) specification)
	 *
	 */
	public static function parseAiccIniData($data, $freeFormTextSections=false) {

		if ($freeFormTextSections === false || !is_array($freeFormTextSections)) {
			$freeFormTextSections = self::$FREEFORM_DATAMODEL_ELEMENTS;
		}

		// Find section names
		$result = preg_match_all('/^\s*\[(\w+)\].*/Usm', $data, $matches);

		$sectionsData = array();

		// If some found...
		if ($result) {

			// Split the whole INI string into an array of section data items, split pattern is [....]
			$sectionValues = preg_split('/^\s*\[\w+\]/Usm', $data);
			$i = 1;

			// Now build the final array
			foreach ($matches[1] as $sectionName) {

				$tmpKey = strtolower($sectionName);
				if (in_array($tmpKey, $freeFormTextSections))
					$sectionsData[$tmpKey] = rawurldecode($sectionValues[$i]);
				else
					$sectionsData[$tmpKey] = $sectionValues[$i];

				$i++;
			}

			// Now enumerate all sections and see if we can parse their internal structures, i.e.  <key>=<value>
			foreach ($sectionsData as $sectionName => $sectionData ) {

				// If the section is NOT of type FREE FORM... do it...
				if (!in_array(strtolower($sectionName), $freeFormTextSections)) {
					$tmpSubArray = parse_ini_string($sectionData);
					if (is_array($tmpSubArray)) {
						$sectionSubArray = array();
						foreach ($tmpSubArray as $subKey => $subValue) {
							$sectionSubArray[strtolower($subKey)] = rawurldecode($subValue);
						}
						$sectionsData[$sectionName] = $sectionSubArray;
					}
				}
			}

		}

		// Lower the case of lesson status (some AUs send "Compleated", not "compleated")
		if (isset($sectionsData['lesson_status'])) {
			$sectionsData['lesson_status'] = strtolower($sectionsData['lesson_status']);
		}

		return $sectionsData;
	}


	/**
	 * Sum $a & $b, where they are session times in the format of "hhhh:mm:ss.dd"
	 * @param unknown $a
	 * @param unknown $b
	 * @return string
	 */
	public static function addSessionTime($a, $b) {

		if (!$a)
			$a = '0000:00:00.00';

		if (!$b)
			$b = '0000:00:00.00';

		$aes = explode(':', $a);
		$bes = explode(':', $b);
		$aseconds = explode('.', $aes[2]);
		$bseconds = explode('.', $bes[2]);
		$change = 0;

		$acents = 0;  // Cents.
		if (count($aseconds) > 1) {
			$acents = $aseconds[1];
		}
		$bcents = 0;
		if (count($bseconds) > 1) {
			$bcents = $bseconds[1];
		}
		$cents = $acents + $bcents;
		$change = floor($cents / 100);
		$cents = $cents - ($change * 100);
		if (floor($cents) < 10) {
			$cents = '0'. $cents;
		}

		$secs = $aseconds[0] + $bseconds[0] + $change;  // Seconds.
		$change = floor($secs / 60);
		$secs = $secs - ($change * 60);
		if (floor($secs) < 10) {
			$secs = '0'. $secs;
		}

		$mins = $aes[1] + $bes[1] + $change;   // Minutes.
		$change = floor($mins / 60);
		$mins = $mins - ($change * 60);
		if ($mins < 10) {
			$mins = '0' .  $mins;
		}

		$hours = $aes[0] + $bes[0] + $change;  // Hours.
		if ($hours < 10) {
			$hours = '0' . $hours;
		}

		if ($cents != '0') {
			return $hours . ":" . $mins . ":" . $secs . '.' . $cents;
		} else {
			return $hours . ":" . $mins . ":" . $secs;
		}


	}



	public function propagateCompletion() {

		/* @var $track LearningAiccItemTrack  */
		/* @var $item LearningAiccItem */

		// ANCESTORS
		$ancestors = $this->item->getAncestors();
		$ancestors = array_reverse($ancestors);
		foreach ($ancestors as $item) {
			$track = $item->getTrackForUser($this->idUser);
			if ($track->lesson_status != self::LESSON_STATUS_COMPLETED) {
				$track->nDescendantCompleted = $track->nDescendantCompleted + 1;
				if ($track->nDescendantCompleted >= $item->nDescendant) {
					$track->lesson_status = self::LESSON_STATUS_COMPLETED;
				}
				else {
					$track->lesson_status = self::LESSON_STATUS_ATTEMPTED;
				}
				$track->save();
			}
		}

		// MASTER ITEM (package track level)
		// Use Common Tracking status constants!!
		$package = $this->item->package;
		$masterItem = $package->masterItem;
		$packageTrack = $package->getTrackForUser($this->idUser);

		if ($packageTrack->status != LearningCommontrack::STATUS_COMPLETED) {
			$packageTrack->nDescendantCompleted = $packageTrack->nDescendantCompleted + 1;
			if ($packageTrack->nDescendantCompleted >= $masterItem->nDescendant) {
				$packageTrack->status = LearningCommontrack::STATUS_COMPLETED;
			}
			else {
				$packageTrack->status = LearningCommontrack::STATUS_ATTEMPTED;
			}
			$packageTrack->save();
		}

		// COMMON TRACKER, set its status to the status of the Package as a whole
		$idOrg = $this->getCommonTrackIdOrg($package);
		CommonTracker::track($idOrg, $this->idUser, $packageTrack->status);
	}

	/**
	 * Retrieves the idOrg of the LO (local vs central LO)
	 * @param $package LearningAiccPackage
	 */
	private function getCommonTrackIdOrg($package) {
		$idOrg = $package->idReference;

		// If idReference is NULL, this means the AICC object is from Central LO
		if(!$idOrg) {
			// Try to load idReference from this track
			if($this->idOrg) {
				$idOrg = $this->idOrg;

				// Are we playing an object from Central LO with shared tracking ON?
				$object = LearningOrganization::model()->findByPk($idOrg)->getMasterForCentralLo($this->idUser);
				$idOrg = $object->idOrg;
			}
		}

		return $idOrg;
	}

	/**
	 * Propagate given status (attempted or failed) up to master AICC item (package) tracking and to Common tracing 
	 * 
	 * @param string $status  LearningCommontrack based status constants!
	 * 
	 * @param string $overrideCompleted
	 */
	public function propagateNonComplete($status, $overrideCompleted=false) {
		
		$package = $this->item->package;
		$masterItem = $package->masterItem;
		$packageTrack = $package->getTrackForUser($this->idUser);
		
		// Master Item (package level track)
		if ($overrideCompleted || ($packageTrack->status != self::LESSON_STATUS_COMPLETED)) {
			$packageTrack->status = $status;
			$packageTrack->save();
		}
		
		// Common tracking: set LO to Attempted if NOT yet completed
		$commonTrack = LearningCommontrack::model()->findByAttributes(array(
				'idReference' 	=> $package->idReference,
				'idUser' 		=> $this->idUser,
				'objectType' 	=> LearningOrganization::OBJECT_TYPE_AICC,
		));
		if (!$commonTrack || ($overrideCompleted || ($commonTrack->status != LearningCommontrack::STATUS_COMPLETED && $commonTrack->status != LearningCommontrack::STATUS_PASSED))) {
			$idOrg = $this->getCommonTrackIdOrg($package);
			CommonTracker::track($idOrg, $this->idUser, $status);
		}
		
	}
	
	
	/**
	 * 
	 */
	public function propagateIncomplete($overrideCompleted=false) {
		$this->propagateNonComplete(LearningCommontrack::STATUS_ATTEMPTED, $overrideCompleted);
	}
	
	
	/**
	 * 
	 */
	public function propagateFailed($overrideCompleted = false) {
		$this->propagateNonComplete(LearningCommontrack::STATUS_FAILED, $overrideCompleted);
	}

	public function getTotalTimeInSeconds() {
		if (!empty($this->total_time)) {
			if(strpos($this->total_time, "P") === false) {
				list($hours, $minutes, $seconds) = explode(':', $this->total_time);
				if (strlen($seconds)>2) list($seconds, $hundredths) = explode('.', $seconds);
				$time = $hours*3600 + $minutes*60 + $seconds;
				return $time;
			} else {
				$matches = array();
				preg_match ('/^P((\d*)Y)?((\d*)M)?((\d*)D)?(T((\d*)H)?((\d*)M)?((\d*)(\.(\d{1,2}))?S)?)?$/', $this->total_time, $matches);
				$seconds = (isset($matches[13]))? (int)$matches[13] : 0;
				$minutes = (isset($matches[11]))? (int)$matches[11] : 0;
				$hours = (isset($matches[9]))? (int)$matches[9] : 0;
			}
			$time = $hours*3600 + $minutes*60 + $seconds;
			return $time;
		}
		return 0;
	}
	
	
	
}
