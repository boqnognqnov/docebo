<?php

/**
 * This is the model class for table "app7020_content".
 *
 * The followings are the available columns in table 'app7020_content':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $filename
 * @property string $originalFilename
 * @property integer $idThumbnail
 * @property integer $contentType
 * @property integer $idSource
 * @property integer $conversion_status
 * @property integer $userId
 * @property integer $duration
 * @property integer $viewCounter
 * @property string $created
 * @property integer $shownInList
 * @property boolean|integer $is_private
 *
 * The followings are the available model relations:
 * @property CoreAsset $idThumbnail
 * @property CoreUser $user
 * @property App7020ContentHistory[] $histories
 * @property App7020ContentPublished[] $published
 * @property App7020ContentRating[] $ratings
 * @property App7020ContentReviews[] $reviews
 * @property App7020ContentThumbs[] $thumbs
 * @property App7020DocumentConversions[] $conversions
 * @property App7020DocumentTracking[] $tracking
 * @property App7020Invitations[] $invitations
 * @property App7020Question[] $questions
 * @property App7020QuestionsRequests[] $requests
 * @property App7020SettingsPeerReview[] $reviews
 * @property App7020TagLink[] $tags
 * @property App7020Tooltips[] $topics
 * @property App7020TopicContent[] $topicContent
 * @property App7020ChannelsAsstes[] $asset_channels
 */
class App7020Assets extends CActiveRecord {

	/**
	 * Conversion status
	 * uploading to local storage
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_PENDING = 0;

	/**
	 * is stored to filesystem on LMS
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_INPROGRESS = 1;

	/**
	 * moved to AMAZON cloud storage
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_S3 = 3;

	/**
	 * moved to AMAZON cloud storage from moblie application - not ready for convertion, no metadata uploaded 
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_FROM_MOBILE = 4;

	/**
	 * if conversion is finished
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_SUCCESS_NO_THUMBS = 5;

	/**
	 * if conversion is finished
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_SUCCESS = 6;

	/**
	 * if SNS about conversion have any warnings
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_WARNING = 7;

	/**
	 * If SNS about conversion return error
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_SNS_ERROR = 8;

	/**
	 * If video duration doesn't match the restrictions
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_VIDEO_LENGTH_ERROR = 9;

	/**
	 * If we have SUCCESS conversion and stored thumbnails
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_FINISHED = 10;

	/**
	 * If we have content in review status
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_INREVIEW = 15;

	/**
	 * If we have content in UNPUBLISH status
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_UNPUBLISH = 18;

	/**
	 * If we have content approved for show in knowledge 
	 *
	 * @var integer
	 */
	const CONVERSION_STATUS_APPROVED = 20;

	/**
	 * Content type 
	 * @var int
	 */
	const CONTENT_TYPE_VIDEO = 1;
	const CONTENT_TYPE_DOC = 2;
	const CONTENT_TYPE_EXCEL = 3;
	const CONTENT_TYPE_PPT = 4;
	const CONTENT_TYPE_PDF = 5;
	const CONTENT_TYPE_TEXT = 6;
	const CONTENT_TYPE_IMAGE = 7;
	const CONTENT_TYPE_QUESTION = 8;
	const CONTENT_TYPE_ANSWER = 9;
	const CONTENT_TYPE_OTHER = 10;
	const CONTENT_TYPE_DEFAULT_OTHER = 11;
	const CONTENT_TYPE_DEFAULT_MUSIC = 12;
	const CONTENT_TYPE_DEFAULT_ARCHIVE = 13;
	const CONTENT_TYPE_LINKS = 15;
	/* GOOGLE DRIVE  */
	const CONTENT_TYPE_GOOGLE_DRIVE_DOCS = 16;
	const CONTENT_TYPE_GOOGLE_DRIVE_SHEETS = 17;
	const CONTENT_TYPE_GOOGLE_DRIVE_SLIDES = 18;
	/* CHECK METHOD googleDriveTypes() */
    const CONTENT_TYPE_PLAYLIST = 19;
	/**
	 * Asset type
	 * @var int
	 */
	const ASSET_TYPE_KNOWLEDGE_ASSET = 1;
	const ASSET_TYPE_PLAYLIST = 2;
	const ASSET_TYPE_COURSE = 3;
	const ASSET_TYPE_LEARNINGPLAN = 4;

	/**
	 * On which percentage of the clip should be marked as viewed
	 */
	const VIDEO_VIEW_PERCENTAGE = 0.75;
//	/**
//	 * Constant for determine that selected cue point should run script for viewing the asset
//	 */
	const CUEPOINT_VIDEO_VIEW = 'app7020ViewContent';

//
	/**
	 * Determine User Role
	 * @var int
	 */
	public $userRole = self::USER_ROLE_LEARNER;

	const USER_ROLE_LEARNER = 0;
	const USER_ROLE_CONTRIBUTOR = 1;
	const USER_ROLE_EXPERT = 2;

	/**
	 * Store search field information on ComboListView
	 * @var string
	 */
	public static $search_input = null;
	static public $contentTypes = array(
		App7020Assets::CONTENT_TYPE_VIDEO => array(
			'outputPrefix' => 'video',
			'allowed' => array('avi', 'mpg', 'mp4', 'mpeg', '3gp', 'mpe', 'm2v', 'wmv', 'mov', 'webm', 'mpeg4', 'mkv', 'mpg2', '3gp2')
		),
		App7020Assets::CONTENT_TYPE_DOC => array(
			'outputPrefix' => 'doc',
			'allowed' => array('doc', 'docx', 'rtf', 'odt'),
			'thumbnail' => 'word.gif',
			'icon' => 'fa fa-file-word-o asset-icon-word'
		),
		App7020Assets::CONTENT_TYPE_EXCEL => array(
			'outputPrefix' => 'excel',
			'allowed' => array('xls', 'xlsx', 'ods', 'csv'),
			'thumbnail' => 'excel.gif',
			'icon' => 'fa fa-file-excel-o asset-icon-word'
		),
		App7020Assets::CONTENT_TYPE_PPT => array(
			'outputPrefix' => 'ppt',
			'allowed' => array('ppt', 'odp', 'pptx', 'ppsx'),
			'thumbnail' => 'ppt.gif',
			'icon' => 'fa fa-file-powerpoint-o asset-icon-pp'
		),
		App7020Assets::CONTENT_TYPE_PDF => array(
			'outputPrefix' => 'pdf',
			'allowed' => array('pdf'),
			//'thumbnail' => 'pdf.png',
			'thumbnail' => 'pdf.gif',
			'icon' => 'fa fa-file-pdf-o asset-icon-pdf'
		),
		App7020Assets::CONTENT_TYPE_TEXT => array(
			'outputPrefix' => 'text',
			'allowed' => array('txt'),
			'thumbnail' => 'word.gif',
			'icon' => 'fa fa-file-text-o asset-icon-text'
		),
		App7020Assets::CONTENT_TYPE_IMAGE => array(
			'outputPrefix' => 'images',
			'allowed' => array('jpg', 'gif', 'png', 'bmp', 'jpeg'),
			'icon' => 'fa fa-file-image-o asset-icon-image'
		),
		App7020Assets::CONTENT_TYPE_OTHER => array(
			'outputPrefix' => 'other',
			'thumbnail' => 'file.gif',
			'icon' => 'fa fa-file-o asset-icon-archive'
		),
		App7020Assets::CONTENT_TYPE_DEFAULT_OTHER => array(
			'outputPrefix' => 'other',
			'allowed' => array(),
			'thumbnail' => 'file.gif',
			'icon' => 'fa fa-file-archive-o asset-icon-archive'
		),
		App7020Assets::CONTENT_TYPE_DEFAULT_ARCHIVE => array(
			'outputPrefix' => 'other',
			'allowed' => array('zip', 'rar', 'tar', '7z', 'ice', 'bz2', 'gz', 's7z', 'cab'),
			'thumbnail' => 'zip.gif',
			'icon' => 'fa fa-file-archive-o asset-icon-archive'
		),
		App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC => array(
			'outputPrefix' => 'audio',
			'allowed' => array('mp3', 'wma', 'flac', 'ogg', 'wav'),
			'thumbnail' => 'audio.gif',
			'icon' => 'fa fa-file-audio-o asset-icon-sound'
		),
		App7020Assets::CONTENT_TYPE_LINKS => array(
			'outputPrefix' => 'link',
			'icon' => 'fa fa-file-image-o asset-icon-image'
		),
		App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS => array(
			'outputPrefix' => 'Google Docs',
			'thumbnail' => 'gdoc.gif'
		),
		App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES => array(
			'outputPrefix' => 'Google Slides',
			'thumbnail' => 'gslides.gif'
		),
		App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS => array(
			'outputPrefix' => 'Google Sheets',
			'thumbnail' => 'gsheet.gif'
		),
	);
	static public $convertibleTypes = array(
		'doc',
		'docx',
		'odt',
		'pages',
		'pdf',
		'ppt',
		'key',
		'pptx',
		'numbers',
		'xls',
		'xlsx',
		'rtf'
	);

	const SETTING_ALLOW_DOWNLOAD = 'allow_assets_dowload';

	/**
	 * Get all codes attached for documents
	 * @return array
	 */
	public static function getDefaultDocumentCodes() {
		return array(
			self::CONTENT_TYPE_DOC,
			self::CONTENT_TYPE_EXCEL,
			self::CONTENT_TYPE_PPT,
			self::CONTENT_TYPE_PDF,
			self::CONTENT_TYPE_OTHER,
			self::CONTENT_TYPE_GOOGLE_DRIVE_DOCS,
			self::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS,
			self::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES,
		);
	}

	/**
	 * Get all codes in asset features with assigned default placeholders
	 *
	 * @return array
	 */
	public static function getDefaultKindCodes() {
		return array(
			self::CONTENT_TYPE_DOC,
			self::CONTENT_TYPE_EXCEL,
			self::CONTENT_TYPE_PPT,
			self::CONTENT_TYPE_PDF,
			self::CONTENT_TYPE_TEXT,
			self::CONTENT_TYPE_OTHER,
			self::CONTENT_TYPE_DEFAULT_ARCHIVE,
			self::CONTENT_TYPE_DEFAULT_MUSIC,
			self::CONTENT_TYPE_GOOGLE_DRIVE_DOCS,
			self::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS,
			self::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES
		);
	}

	/**
	 * S3 folder to store images of converted documents
	 */
	const DOCUMENT_IMAGES_S3_FOLDER = 'images';

	/**
	 * Various scenarios
	 *
	 * @var string
	 */
	const SCENARIO_SAVE_METADATA = 'save_metadata';
	const SCENARIO_SAVE_METADATA_EDIT_MODE = 'save_metadata_edit_mode';

	/**
	 * The logged user has access to edit, after contet was invike by any methodd
	 * @var boolean 
	 */
	public $isContentAdmin = false;

	/**
	 * The logged user has access to review/edit, after contet was invike by any method
	 * @var boolean 
	 */
	public $isContentGuru = false;

	/**
	 * If current user is contributor on this assets
	 */
	public $isContributor = false;

	/**
	 * When some asset is ready to be published, we need from array of ID channels, assigned to the channel
	 * This case is only in case of save 
	 * @var array 
	 */
	public $relatedChannels = array();

	/**
	 * Store all posted tags separated by comma
	 * @var string 
	 */
	public $tagsToAssign = '';

	/**
	 * The active thumbnail id
	 * @var int 
	 */
	protected $activeThumbnail;

	/**
	 * Temporary value with all ids of questions before delete of the assset.
	 * If he need from delete and question, (case in requests) will be stored there
	 * @var array 
	 */
	private $tmpQuestionsIds = array();

	/**
	 * Before delete asset, store temporary model for early work 
	 * We use this when delete asset, check for existing model
	 * @var App7020QuestionRequest 
	 */
	private $tmpRequestModel = null;

	/**
	 * Initial status when is made the asset(PUBLIC MODE)
	 */
	const PRIVATE_STATUS_INIT = 0;

	/**
	 * To be private an asset should be set to 1 
	 */
	const PRIVATE_STATUS_PRIVATE = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_content';
	}

	public function __construct($scenario = 'insert') {

		parent::__construct($scenario);
	}

	function init() {
		parent::init();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
		return array(
			array('filename, originalFilename, created', 'required'),
			//modified by Stanimir 
			array(
				'title, description',
				'required',
				'message' => Yii::t('app7020', 'The value for "{attribute}" is required')
			),
			array(
				'activeThumbnail',
				'valideActiveThumbnailValue',
			),
			array(
				'Channel',
				'validateChannelRequirement',
			),
			array(
				'is_private',
				'required',
				'message' => Yii::t('app7020', 'The value for "Asset visibility" is required')
			),
			//check for valide thumbnail and there will be the proccess for cration new thumbnail, if something went wrong, then will fire error in general error stack;
			array('idThumbnail, contentType, idSource, conversion_status, userId, duration, viewCounter, shownInList', 'numerical', 'integerOnly' => true),
			array('filename, originalFilename', 'length', 'max' => 512),
			// The following rule is used by search().
// @todo Please remove those attributes that should not be searched.
			array('id, title, description, filename, originalFilename, idThumbnail, contentType, idSource, conversion_status, userId, duration, viewCounter, created, shownInList', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.


		return array(
			'thumbnail' => array(self::BELONGS_TO, 'CoreAsset', 'idThumbnail'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'userId'),
			'histories' => array(self::HAS_MANY, 'App7020ContentHistory', 'idContent'),
			'published' => array(self::HAS_ONE, 'App7020ContentPublished', 'idContent', 'condition' => 'actionType = 1'),
			'ratings' => array(self::HAS_MANY, 'App7020ContentRating', 'idContent'),
			'reviews' => array(self::HAS_MANY, 'App7020ContentReviews', 'idContent'),
			'thumbs' => array(self::HAS_MANY, 'App7020ContentThumbs', 'idContent'),
			'conversions' => array(self::HAS_MANY, 'App7020DocumentConversions', 'idContent'),
			'tracking' => array(self::HAS_MANY, 'App7020DocumentTracking', 'idContent'),
			'invitations' => array(self::HAS_MANY, 'App7020Invitations', 'idContent'),
			'questions' => array(self::HAS_MANY, 'App7020Question', 'idContent'),
			//request relation  to question requests 
			'request' => array(self::HAS_ONE, 'App7020QuestionRequest', 'idContent'),
			'question_requests_channels' => array(self::HAS_MANY, 'App7020ChannelQuestions', array('idQuestion' => 'idQuestion'), 'through' => 'request'),
			'question_channels' => array(self::HAS_MANY, 'App7020Channels', array('idChannel' => 'id'), 'through' => 'question_requests_channels'),
			'tags' => array(self::HAS_MANY, 'App7020TagLink', 'idContent'),
			'tooltips' => array(self::HAS_MANY, 'App7020Tooltips', 'idContent'),
			//is used
			'peer_review_settings' => array(self::HAS_ONE, 'App7020SettingsPeerReview', 'idContent'),
			'contributor' => array(self::BELONGS_TO, 'CoreUser', 'userId'),
			'viewes' => array(self::HAS_MANY, 'App7020ContentHistory', 'idContent', 'group' => 'idUser'),
			//is used 
			//is used
			'tagLinks' => array(self::HAS_MANY, 'App7020TagLink', 'idContent'),
			//is used
			'asset_channels' => array(self::HAS_MANY, 'App7020ChannelAssets', 'idAsset', 'condition' => 'asset_type = 1'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020Assets the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Update all rows in defined period for PR tab 
	 * Update column is shownInList
	 * Updated Value is True (is shown already)
	 * 
	 * @param type $period
	 * @return type
	 */
	public static function clearPeerReviewAssetsList($period = 'WEEK') {
		$criteria = Yii::app()->db->createCommand();
		$condition = 'created >= DATE_SUB(NOW(), INTERVAL 1 ' . strtoupper($period) . ') AND  userId  =:userId AND conversion_status = :conversion_status';
		$params = array(
			'userId' => Yii::app()->user->id,
			'conversion_status' => App7020Assets::CONVERSION_STATUS_FINISHED
		);
		$numUpdates = $criteria->update(self::model()->tableName(), array('shownInList' => 1), $condition, $params);
		return (bool) $numUpdates;
	}

	/**
	 * Update all rows in defined period for published tab
	 * Update column is shownInList
	 * Updated Value is True (is shown already)
	 * 
	 * @param type $period
	 * @return type
	 */
	public static function clearPublishedAssetsList($period = 'WEEK') {
		$criteria = Yii::app()->db->createCommand();
		$condition = 'created >= DATE_SUB(NOW(), INTERVAL 1 ' . strtoupper($period) . ') AND  userId  =:userId AND conversion_status = :conversion_status';
		$params = array(
			'userId' => Yii::app()->user->id,
			'conversion_status' => App7020Assets::CONVERSION_STATUS_APPROVED
		);
		$numUpdates = $criteria->update(self::model()->tableName(), array('shownInList' => 1), $condition, $params);
		return (bool) $numUpdates;
	}

	/**
	 * Custom validation rule for check and replacement of acttive thumbnail if all in the ENV works properly
	 * @param string $attribute
	 * @param array $params
	 * @return boolean
	 */
	function valideActiveThumbnailValue($attribute, $params) {
		if ($this->contentType != self::CONTENT_TYPE_VIDEO) {
			return true;
		}

		if (empty($this->$attribute)) {
			$this->addError('Thumbnail', Yii::t('app7020', 'The Thumbnail is required for this operation. Please correct this issue!'));
			return false;
		}

		return true;
	}

	/**
	 * Channel validation in different cases 
	 * @param string $attribute
	 * @param array $params
	 * @return boolean
	 */
	function validateChannelRequirement($attribute, $params) {
		$isAllowPrivate = ShareApp7020Settings::isGlobalAllowPrivateAssets();
		if ($this->conversion_status < 10 && $this->is_private == App7020Assets::PRIVATE_STATUS_INIT && $isAllowPrivate) {
			if ($this->isSuperAdminAccess() && $isAllowPrivate) {
				if (empty($this->relatedChannels)) {
					$this->addError($attribute, Yii::t('app7020', 'The Channel is required for an asset!'));
					return false;
				}
			}
			return true;
		}
		if (($this->conversion_status >= 10) && $this->is_private == App7020Assets::PRIVATE_STATUS_INIT && $isAllowPrivate) {
			if ($this->isSuperAdminAccess() && $isAllowPrivate) {
				if (empty($this->relatedChannels)) {
					$this->addError($attribute, Yii::t('app7020', 'The Channel is required for an asset!'));
					return false;
				}
				return true;
			} else {
				return true;
			}
		}
	}

	public static function validateCustomDuration(App7020Assets $assetModel) {
		if (Settings::get('video_contribute') == 1) {
			if ($assetModel->conversion_status == self::CONVERSION_STATUS_S3 && $assetModel->contentType == self::CONTENT_TYPE_VIDEO) {
				$setting_duration_not_longer = Settings::get('video_not_longer_time');
				$setting_duration_not_longer_to_seconds = App7020Helpers::durToSeconds($setting_duration_not_longer);
				$setting_duration_not_shorter = Settings::get('video_not_shorter_time') ? Settings::get('video_not_shorter_time') : 0;
				$setting_duration_not_shorter_to_seconds = App7020Helpers::durToSeconds($setting_duration_not_shorter);
				if (!$setting_duration_not_longer) {
					if ($setting_duration_not_shorter_to_seconds <= $assetModel->duration) {
						return true;
					}
				} else {
					if ($setting_duration_not_longer_to_seconds >= $assetModel->duration && $setting_duration_not_shorter_to_seconds <= $assetModel->duration) {
						return true;
					}
				}
			}
		} else {
			return true;
		}
	}

	/**
	 * Start proccess after success uploaded file
	 * @return boolean
	 */
	function startProccessingAfterFileUploaded() {

		if ($this->contentType == self::CONTENT_TYPE_IMAGE) {
			if ($this->idThumbnail = App7020ImageProceeder::generateCoreAssets($this->id)) {
				$this->conversion_status = self::CONVERSION_STATUS_SNS_SUCCESS;
				return true;
			}
		} elseif (self::isConvertableDocument($this->filename)) {
			return App7020DocumentProceeder::startDocumentConversion($this->id);
		} elseif ($this->contentType == self::CONTENT_TYPE_VIDEO && (int) $this->sns == 0) {
			return App7020VideoProceeder::startVideoConversion($this->id);
		} elseif ($this->contentType == self::CONTENT_TYPE_VIDEO && (int) $this->sns == 1) {
			return true;
		} else {
			$this->conversion_status = self::CONVERSION_STATUS_SNS_SUCCESS;
			return true;
		}
	}

	function beforeSave() {

		if (!$this->isNewRecord) {
			Yii::app()->event->raise(EventManager::EVENT_APP7020_BEFORE_ASSET_SAVE, new DEvent($this, array('model' => $this)));
		}

		if ($this->conversion_status > self::CONVERSION_STATUS_SNS_SUCCESS) {
			if ($this->contentType == self::CONTENT_TYPE_VIDEO && ($this->activeThumbnail != App7020VideoProceeder::getActiveThumb($this->id)) && $this->activeThumbnail) {
				if ($this->activeThumbnail) {
					$criteria = new CDbCriteria;
					$criteria->addCondition('idContent=:idContent');
					$criteria->params = array(':idContent' => $this->id);
					App7020ContentThumbs::model()->updateAll(array('is_active' => '0'), $criteria);
					$thumbObject = App7020ContentThumbs::model()->findByPk($this->activeThumbnail);
					$thumbObject->is_active = 1;
					$thumbObject->save(false);
					$generateImage = App7020VideoProceeder::generateThumbCoreAsset($this, $thumbObject);
					$this->idThumbnail = $generateImage->id;
				}
			}
		}

		return parent::beforeSave();
	}

	/**
	 * 
	 */
	public function afterSave() {
		if ($this->conversion_status == self::CONVERSION_STATUS_SNS_SUCCESS) {
			$assets = App7020Assets::model()->findAllByAttributes(
					array('userId' => Yii::app()->user->idst), array(
				'condition' => 'conversion_status>=:status',
				'params' => array(':status' => self::CONVERSION_STATUS_SNS_SUCCESS)
					)
			);
			Yii::app()->event->raise('UserReachedAGoal', new DEvent($this, array('goal' => 5, 'asset_count' => count($assets), 'user' => Yii::app()->user->idst)));
		}

		$this->saveTags();
		//make record in history table for updating the asset
		if ($this->conversion_status >= self::CONVERSION_STATUS_FINISHED) {
			App7020ContentPublished::saveEditAction($this->id, $this->conversion_status);
		}

		if ($this->conversion_status == self::CONVERSION_STATUS_APPROVED) {

			$publishedObject = App7020ContentPublished::model()->findByAttributes(array('idContent' => $this->id, 'actionType' => App7020ContentPublished::ACTION_TYPE_PUBLISH));

			if (!$publishedObject->id) {
				$publishObject = new App7020ContentPublished;
				$publishObject->idUser = Yii::app()->user->id;
				$publishObject->idContent = $this->id;
				$publishObject->save(false);
				$this->getRelated('published', true);
			}


			$tempInvitations = App7020TemporaryInvitations::model()->findByAttributes(array('idContent' => $this->id));
			if ($tempInvitations) {
				$invited = unserialize($tempInvitations->invited);
				foreach ($invited as $value) {
					$invitationObject = new App7020Invitations();
					$invitationObject->idInvited = $value;
					$invitationObject->idInviter = $this->userId;
					$invitationObject->idContent = $this->id;
					$invitationObject->save(false);
				}

				Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invited,
					'contentId' => $this->id)));

				App7020TemporaryInvitations::model()->deleteAllByAttributes(array('idContent' => $this->id));
			}


			//if we have request for this asset and the auotInvite option is ON - Invite the owner of the request/question
			$requestModel = App7020QuestionRequest::model()->findByAttributes(array('idContent' => $this->id, 'autoInvite' => 1));
			if ($requestModel->id) {
				$questionModel = App7020Question::model()->findByPk($requestModel->idQuestion);
				$invitedUsers = array($questionModel->idUser);
				foreach ($invitedUsers as $value) {
					$invitationObject = new App7020Invitations();
					$invitationObject->idInvited = $value;
					$invitationObject->idContent = $this->id;
					$invitationObject->save(false);
				}
				Yii::app()->event->raise('App7020Invitation', new DEvent($this, array('users' => $invitedUsers,
					'contentId' => $this->id)));
			}
		}

		if ($this->conversion_status >= self::CONVERSION_STATUS_S3) {

			if (!empty($this->relatedChannels)) {

				$currentChannelsForAnAsset = App7020ChannelAssets::getChannelKeysForAsset($this->id);
				$currentPostedChannelsForAnAsset = $this->relatedChannels;

				$delete_array = array_diff($currentChannelsForAnAsset, $currentPostedChannelsForAnAsset);
				$insert_array = array_diff($currentPostedChannelsForAnAsset, $currentChannelsForAnAsset);
				$insert_proccess = App7020ChannelAssets::massAppendChannelToAnAsset($insert_array, $this->id);
				$delete_proccess = App7020ChannelAssets::massRemoveChannelsForAsset($delete_array, $this->id);

				// Return value makes no sense in afterSave event
				//return ($insert_proccess && $delete_proccess);
			}
		}


		// This is the formal moment of completing the Asset Save
		Yii::app()->event->raise(EventManager::EVENT_APP7020_ASSET_UPDATED, new DEvent($this, array(
			'model' => $this,
		)));
	}

	/**
	 * 
	 * @param App7020Assets $content
	 * @param integer $step
	 * @return boolean
	 */
	public static function valideFileIsUploadedSuccessed(App7020Assets $content, $step = 0) {
		$fileStorageObject = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
		$fileStorageObject->setKey($content->filename);
		$outputPath = ($content->contentType == App7020Assets::CONTENT_TYPE_VIDEO) ? $fileStorageObject->getKey() : $fileStorageObject->getOutputKey();
		Yii::log(" [APP7020] - CHECK FOR  SUCCESSED UPLOADED FILE RESULT  " . $fileStorageObject->fileExists($outputPath), CLogger::LEVEL_INFO);

		if ($fileStorageObject->fileExists($outputPath) && $step < 60) {
			Yii::log(" [APP7020] - CHECK FOR  SUCCESSED UPLOADED FILE  successed-  ==", CLogger::LEVEL_INFO);
			return true;
		} else if ($step > 60) {
			Yii::log(" [APP7020] - CHECK FOR  SUCCESSED UPLOADED FILE  CANNOT BE FOUND-  ", CLogger::LEVEL_INFO);
			return false;
		} else {
			sleep(1);
			Yii::log(" [APP7020] - CHECK FOR  SUCCESSED UPLOADED FILE  retry " . $step . " FILE: " . $outputPath . "-  =", CLogger::LEVEL_INFO);
			return self::valideFileIsUploadedSuccessed($content, ++$step);
		}
	}

	/**
	 * Save all tags after native save on Content
	 * @return boolean
	 */
	function saveTags() {
		if ($this->tagsToAssign) {
			try {
				$allWords = explode(',', $this->tagsToAssign);
				$criteria = new CDbCriteria();
				$criteria->addInCondition('tagText', $allWords);
				$tags = App7020Tag::model()->findAll($criteria);

				$allTags = array();
				foreach ($tags as $tag) {
					$allTags[$tag->id] = $tag->tagText;
				}
				$criteriaDest = new CDbCriteria();
				$criteriaDest->select = 't.*';
				$criteriaDest->join = 'INNER JOIN ' . App7020TagLink::model()->tableName() . '  ON  ' . App7020TagLink::model()->tableName() . '.idTag = t.id ';
				$criteriaDest->join .= 'INNER JOIN ' . self::model()->tableName() . '  ON ' . self::model()->tableName() . '.id = ' . App7020TagLink::model()->tableName() . '.idContent ';
				$criteriaDest->condition = 'idContent = :idContent';
				$criteriaDest->params = array('idContent' => $this->id);
				$tags = App7020Tag::model();
				$all_dist_tags = array();
				foreach ($tags->findAll($criteriaDest) as $distTag) {
					$all_dist_tags[$distTag->id] = $distTag->tagText;
				}

				//compare for delete and insert
				$array_insert = array_diff($allTags, $all_dist_tags);
				$array_delete = array_diff($all_dist_tags, $allTags);

				$deleteCriteria = new CDbCriteria();
				$deleteCriteria->condition = 'idContent = :idContent';
				$deleteCriteria->params = array('idContent' => $this->id);
				$deleteCriteria->addInCondition('idTag', array_keys($array_delete));
				App7020TagLink::model()->deleteAll($deleteCriteria);
				foreach ($array_insert as $insert_key => $insert_value) {
					$insertTagModel = new App7020TagLink;
					$insertTagModel->idContent = $this->id;
					$insertTagModel->idTag = $insert_key;
					if (!$insertTagModel->save()) {
						return false;
					}
				}
				return true;
			} catch (Exception $ex) {
				Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
				return false;
			}
		}
	}

	/**
	 * Get status on uploaded file. Video Content or Document Content
	 * @param string $filename Uploaded file
	 * @return boolean
	 */
	public static function isDocument($filename = false) {
		try {
			return self::isDocumentExt(App7020Helpers::getExtentionByFilename($filename));
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check if any extention is document
	 * @param string $ext
	 * @return boolean
	 */
	public static function isDocumentExt($ext) {
		try {
			$contentCode = self::getContentCodeByExt($ext);
			if (in_array($contentCode, self::getDefaultDocumentCodes())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check if any extention is from default kind of files with assigned placeholder
	 * @param string $ext
	 * @return boolean
	 */
	public static function isDefaultKindExt($ext) {
		try {
			$contentCode = self::getContentCodeByExt($ext);
			if (in_array($contentCode, self::getDefaultKindCodes())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check if file extension is in convertable types
	 * @param string $file_name
	 * @return $extension or false
	 */
	public static function isConvertableDocument($filename) {

		$extension = App7020Helpers::getExtentionByFilename($filename);

		if (in_array($extension, self::$convertibleTypes)) {
			return $extension;
		} else {
			return false;
		}
	}

	public static function isFromDefaultKindOfFiles($filename) {
		try {

			return self::isDefaultKindExt(App7020Helpers::getExtentionByFilename($filename));
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Get Content code by Extention 
	 * @param type $needleExt
	 * @return boolean|integer
	 */
	public static function getContentCodeByExt($needleExt) {
		foreach (self::$contentTypes as $singleTypeKey => $singleType) {
			if (isset($singleType['allowed'])) {
				$keyOnTypeInternal = array_search($needleExt, $singleType['allowed']);
				if ($keyOnTypeInternal !== false) {
					return $singleTypeKey;
				}
			}
		}
		return false;
	}

	/**
	 *
	 * @param App7020Content $content
	 * @param boolean $isEcho
	 * @return string
	 */
	public function getPreviewThumbnailImage($isEcho = false, $imageVariant = 'small', $alt = '', $htmlOptions = array()) {

		if (self::isDocument($this->filename)) {
			return self::getThumbnailImageByExtention(App7020Helpers::getExtentionByFilename($this->filename), $isEcho);
		} else if (self::isFromDefaultKindOfFiles($this->filename)) {
			return self::getThumbnailImageByExtention(App7020Helpers::getExtentionByFilename($this->filename), $isEcho);
		} else if (self::isGoogleDriveTypes($this->contentType)) {
			return self::getThumbnailImageByContentType($this->contentType);
		} else {
			$coreAsset = CoreAsset::model()->findByPk($this->idThumbnail);
			$src = '';
			if (!empty($coreAsset) && $coreAsset instanceof CoreAsset) {
				$src = $coreAsset->getUrl($imageVariant);
			}
			if (empty($src)) {
				$coreAsset = new CoreAsset();
				$coreAsset->type = CoreAsset::TYPE_APP7020;
				$height = $coreAsset->getImageVariantHeight($imageVariant);
				$width = $coreAsset->getImageVariantWidtht($imageVariant);
				$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
			}
			$htmlOptions['class'] = 'ThumbPreview';
			$image = CHtml::image($src, $alt, $htmlOptions);
			if ($isEcho) {
				echo $image;
			} else {
				return $image;
			}
		}
	}

	/**
	 *
	 * @param array $data object attributes such as database fields
	 * @param string $imageVariant type of thumbnile. Use variants from config. e.g small|large|carousel ..... etc
	 * @param bool $isEcho Do you want print the image or not, only append to var
	 * @param string $alt Alt atribute to image
	 * @param array $htmlOptions  Set other attributes to array
	 * @return type
	 */
	public static function getThumbnail($data = array(), $imageVariant = 'small', $isEcho = false, $alt = '', $htmlOptions = array()) {
		try {

			$content = new App7020Assets();
			$content->setAttributes($data, false);
			$content->getRelated("thumbs", true);
			return $content->getPreviewThumbnailImage($isEcho, $imageVariant, $alt, $htmlOptions);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$width = $coreAsset->getImageVariantWidtht($imageVariant);
			$height = $coreAsset->getImageVariantHeight($imageVariant);
			$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
			$htmlOptions['class'] = 'ThumbPreview';
			$image = CHtml::image($src, $alt, $htmlOptions);
			if ($isEcho) {
				echo $image;
			} else {
				return $image;
			}
		}
	}

	/**
	 * Return or print conternt tumbnail image by extention of file
	 * @param string $needleExt
	 * @param boolean $isEcho
	 * @param string $alt
	 * @param array $htmlOptions
	 * @return mixed
	 *
	 *
	 */
	static public function getThumbnailImageByExtention($needleExt, $isEcho = false, $alt = '', $htmlOptions = array()) {
		$image = CHtml::image(self::getThumbnailURLByExt($needleExt), $alt, $htmlOptions);
		if (!$isEcho) {
			return $image;
		} else {
			echo $image;
		}
	}

	/**
	 * 
	 * @param type $needleExt
	 * @param type $isEcho
	 * @param type $alt
	 * @param type $htmlOptions
	 * @return type
	 */
	static public function getThumbnailImageByContentType($contentType, $isEcho = false, $alt = '', $htmlOptions = array()) {

		$image = CHtml::image(self::_generateImageUrlByType($contentType), $alt, $htmlOptions);
		if (!$isEcho) {
			return $image;
		} else {
			echo $image;
		}
	}

	/**
	 * Get thumbnail by ext. definition
	 * @param string $needleExt
	 * @return boolean|string
	 *
	 */
	static public function getThumbnailURLByExt($needleExt) {
		try {
			return self::_generateImageUrlByType(self::getContentCodeByExt($needleExt));
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Return Image Asset url to document preview thumbnail placholder
	 * 
	 * @param int $keyOnType
	 * @return string
	 *
	 *
	 */
	static private function _generateImageUrlByType($keyOnType) {
		$assetUrl = Yii::app()->theme->baseUrl;
		if ($keyOnType && isset(self::$contentTypes[(int) $keyOnType]['thumbnail'])) {
			return $assetUrl . '/images/contribute/' . self::$contentTypes[(int) $keyOnType]['thumbnail'];
		} else {
			return $assetUrl . '/images/contribute/' . self::$contentTypes[self::CONTENT_TYPE_OTHER]['thumbnail'];
		}
	}

	/**
	 * Return all tags as string separated by come 
	 * @return string|boolean
	 */
	function getAllTagsAsString() {
		try {
			$output = array();
			foreach ($this->tags as $tags) {
				$output[] = $tags->tag->tagText;
			}
			return implode(', ', $output);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return '';
		}
	}

	/**
	 * Invoke flags after content was build
	 */
	function afterFind() {
		parent::afterFind();
		$this->isContributor = ($this->userId == Yii::app()->user->id);
		if (empty($this->peer_review_settings)) {
			$this->peer_review_settings = new App7020SettingsPeerReview();
		}
		foreach ($this->getAttributes() as $attr_key => $attr_val) {
			$this->setAttribute($attr_key, htmlspecialchars_decode($attr_val, ENT_QUOTES  ));
		}
	}

	/**
	 * Check logged user for admin permissions for opened content (IS NOT LEARNER)
	 * @return boolean
	 */
	public function defineContentAdminAccess() {
		try {
			if ($this->contributor->idst == Yii::app()->user->id || Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPU()) {
				return true;
			}
			return $this->hasReviewsAccess();
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check logged user for admin permissions for opened content (IS NOT LEARNER)
	 * @return boolean
	 */
	public function isSuperAdminAccess() {
		try {
			if (Yii::app()->user->isGodAdmin) {
				return true;
			}
			return false;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Check user id is guru for this content
	 * @param int $userId
	 * @return boolean
	 */
	function hasReviewsAccess($userId = false) {
		if ($userId == false) {
			$userId = Yii::app()->user->id;
		}
		return in_array($userId, App7020ChannelExperts::getAllChannelExperts($this->id));
	}

	/**
	 * Get All assets who need any progress to be completed
	 * @param array $customConfig
	 * @return array
	 */
	public static function getUploadedFiles($customConfig = false) {

		try {
			$criteria = new CDbCriteria();
			$proccessing_criteria = ($customConfig['proccessing_criteria']) ? $customConfig['proccessing_criteria'] : Yii::app()->params['s3Storages']['amazon']['proccessing_criteria'];
			if ($customConfig['created'] == 'week') {
				$criteria->condition = 'userId = :userId AND ((created > (NOW() - INTERVAL 7 DAY) AND shownInList = 0 AND conversion_status >= 10) OR (conversion_status < 10))';
			} else {
				$criteria->condition = 'userId = :userId';
			}
			$criteria->params = array('userId' => Yii::app()->user->id);
			$criteria->addInCondition('conversion_status', $proccessing_criteria);

			if (empty($customConfig['idQuestion'])) {
				$assetModels = self::model()->findAll($criteria);
			} else {
				$assetModels = self::model()->with(
								array(
									'request' => array(
										'select' => false,
										'joinType' => 'INNER JOIN',
										'condition' => 'idQuestion=' . (int) $customConfig['idQuestion'],
									)
								)
						)->findAll();
			}
			$assetTempArray = array();
			$i = 0;
			foreach ($assetModels as $model) {
				$assetTempArray[$i] = $model->getAttributes();
				$assetTempArray[$i]['title'] = urlencode($assetTempArray[$i]['title']);
				$assetTempArray[$i]['description'] = urlencode($assetTempArray[$i]['description']);
				$idThumb = $model->idThumbnail;
				$thumb = CoreAsset::model()->findByPk($idThumb);
				$assetTempArray[$i]["thumb"] = App7020Assets::getAssetThumbnailUri($model->id);
				$i++;
			}
			return $assetTempArray;
		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	/**
	 * Return correct format JSON for 
	 * implement it in PLUploader 
	 * @return string (JSON)
	 */
	static public function getAllowesTypesForUploader($whitKeys = true) {
		$tempArray = array();
		$array_tmp = FileTypeValidator::getWhiteListArray(FileTypeValidator::TYPE_APP7020_CONTENT);

		foreach (self::$contentTypes as $key => $type) {
			if (isset($type['allowed'])) {

				foreach ($type['allowed'] as $allowedExt) {
					$keyForDelete = array_search($allowedExt, $array_tmp);
					if ($keyForDelete !== false) {
						unset($array_tmp[$keyForDelete]);
					}
				}
				if ($whitKeys) {
					$tempArray[$key] = array(
						'title' => ucfirst($type['outputPrefix']),
						'extensions' => implode(',', $type['allowed'])
					);
				} else {
					$tempArray[] = array(
						'title' => ucfirst($type['outputPrefix']),
						'extensions' => implode(',', $type['allowed'])
					);
				}
			}
		}

		if (!empty($array_tmp)) {
			$tempArray[] = array(
				'title' => ucfirst('other'),
				'extensions' => implode(',', $array_tmp)
			);
		}
		return $tempArray;
	}

	/**
	 * Parse file extention
	 * @param array $array_ext
	 * @return boolean|array
	 */
	private static function _parseFileExtention($array_ext, $whitKeys = true) {
		$tempArray = array();
		try {

			if ($whitKeys) {
				$tempArray[self::CONTENT_TYPE_OTHER] = array(
					'title' => 'Other',
					'extensions' => App7020Helpers::clearComaStringInArray($array_ext)
				);
			} else {
				$tempArray[] = array(
					'title' => 'Other',
					'extensions' => App7020Helpers::clearComaStringInArray($array_ext)
				);
			}
			return $tempArray;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * 
	 * @param type $idContent
	 * @return boolean
	 */
	public static function hasAdminRights($idContent) {
		if (Yii::app()->user->isGodAdmin) {
			return true;
		} elseif (self::isExpert($idContent)) {
			return true;
		} else {
			$assetModel = self::model()->findByPk($idContent);
			if (!empty($assetModel->id) && $assetModel->userId == Yii::app()->user->idst) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Check if current user is expert for content
	 * @param int $idContent
	 * @return bool
	 */
	public static function isExpert($idContent) {
		$assetExperts = App7020Assets::getAssetExperts($idContent);
		$assetExperts = App7020Assets::model()->getUsersDataFromArray($assetExperts);
		return in_array(Yii::app()->user->getId(), $assetExperts);
	}

	/**
	 * 
	 * @param type $idContent
	 */
	public function getContentExperts($idContent) {
		return App7020Assets::getAssetExperts($idContent);
	}

	/**
	 * Check if THIS asset is visible to a given user or to current user, if user is not provided
	 * 
	 * @param integer $idUser
	 */
	public function isVisibleToUser($idUser = null) {
		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}
//		// GodAdmins are ... god admins.. they see them all
//		if (CoreUser::isUserGodadmin($idUser)) {
//			return true;
//		}
        $invited = App7020Invitations::model()->findAllByAttributes([],
            $condition = 'idInvited = :invited AND idContent = :idContent',
            $params = [':invited' => $idUser, ':idContent' => $this->id]);
		//if user is contributor 
        if ((int)$this->userId == (int)$idUser || !empty($invited)) {
			return true;
		}
		$custom_Select = App7020Channels::customProviderSelect();
		$custom_channel_visibility_select = array(
			"idUser" => $idUser,
			"idAsset" => $this->id,
			"ignoreVisallChannels" => false,
			"ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
			'appendEnabledEffect' => true,
			'ignoreSystemChannels' => true,
			'customSelect' => $custom_Select,
            'ignoreExpertChannels' => false,
			'return' => 'count'
		);
		$dp = App7020Channels::extractUserChannels($custom_channel_visibility_select);

		return !empty((int) $dp);
	}

	/**
	 * Return a Mapping between Asset(s) and its (their) Expert(s), or vice versa (inversed map)
	 * 
	 * @param string $idContent Filter by asset(s)
	 * @param string $idUser Filter user ID(s)
	 * @param string $inverse Instead of asset => array(experts), return  expert => array(assets)
	 * 
	 * @return array
	 */
	public static function assetToExpertMap($idAssets = false, $idUsers = false, $inverse = false) {

// Apply some caching for repeated calls in the same request/run
		static $cache = false;
		$cacheKey = md5(json_encode(func_get_args()));
		if (is_array($cache) && isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}

		$params = array();
		$command = Yii::app()->db->createCommand();

		if ($idAssets) {
			if (!is_array($idAssets))
				$idAssets = array((int) $idAssets);
			$command->andWhere(array("IN", "a.id", $idAssets));
		}
		if ($idUsers) {
			if (!is_array($idUsers))
				$idUsers = array((int) $idUsers);
			$command->andWhere(array("IN", "te.idExpert", $idUsers));
		}

		if ($inverse) {
			$command->group('te.idExpert');
			$command->select("GROUP_CONCAT(a.id) as assets, te.idExpert as idExpert");
		} else {
			$command->group('a.id');
			$command->select("a.id as idAsset, GROUP_CONCAT(te.idExpert) as experts");
		}

		$command->from("app7020_content a");
		$command->join("app7020_topic_content tc", "tc.idContent=a.id");
		$command->join("app7020_topic_expert te", "te.idTopic=tc.idTopic");

		$rows = $command->queryAll(true, $params);

		$result = array();
		foreach ($rows as $row) {
			if ($inverse) {
				$result[$row['idExpert']] = $row['assets'];
			} else {
				$result[$row['idAsset']] = $row['experts'];
			}
		}

		$cache[$cacheKey] = $result;

		return $result;
	}

	/**
	 * Check if user can delete content
	 * @param type $idContent
	 * @return boolean
	 */
	public static function canUserDeleteContent($idContent) {
		if (Yii::app()->user->isGodAdmin) {
			return true;
		}

		$contentObject = self::model()->findByPk($idContent);
		if ($contentObject->id && $contentObject->isContributor) {
			return true;
		}
        if(in_array(Yii::app()->user->idst, App7020Assets::getAssetExperts($contentObject->id))){
            return true;
        }

		return false;
	}

	/**
	 * Few actions befroe delete an asset
	 * @return boolean
	 */
	function beforeDelete() {

		try {
			foreach ($this->questions as $question) {
				$this->tmpQuestionsIds[] = $question->id;
			}
			$this->tmpRequestModel = $this->request;
			return parent::beforeDelete();
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Delete all question related to this asset, previosly stored in a temporary array
	 */
	private function deleteRelatedQuestions() {
		// This temporary array is filled up in beforeDelete() event!
		// Because on asset deletion questions table -> idContent is set to NULL (by FK rule (??))
		if (is_array($this->tmpQuestionsIds) && !empty($this->tmpQuestionsIds)) {
			$questionsIds = array_values($this->tmpQuestionsIds);
			foreach ($questionsIds as $id) {
				// Use Model->delete in order to go through afterDelete() events for Question
				App7020Question::model()->findByPk($id)->delete();
			}
		}
	}

	/**
	 * We must delete or reset request for question on asset delete
	 */
	function afterDelete() {
		try {
			// Delete all related records in App7020ContentHistory
			App7020ContentHistory::model()->deleteAllByAttributes(array('idContent' => $this->id));
			// Delete all related records in App7020Channels
			App7020ChannelAssets::model()->deleteAllByAttributes(array('idAsset' => $this->id));
			// @TODO use THIS event only, if possible, for Asset deletion
			Yii::app()->event->raise(EventManager::EVENT_APP7020_ASSET_DELETED, new DEvent($this, array(
				'model' => $this,
				'user' => Yii::app()->user->idst,
			)));


			//Send notifications
			if ($this->conversion_status >= App7020Assets::CONVERSION_STATUS_SNS_SUCCESS) {
				if (Yii::app()->user->isGodAdmin) {
					Yii::app()->event->raise('App7020DeleteAssetByAdmin', new DEvent($this, array(
						'users' => Yii::app()->user->idst,
						'content' => $this,
					)));
				} else {
					Yii::app()->event->raise('App7020DeleteAsset', new DEvent($this, array(
						'content' => $this,
					)));
				}
			}
			if ($this->conversion_status == self::CONVERSION_STATUS_APPROVED) {
				if (!empty($this->tmpRequestModel)) {
					$this->tmpRequestModel->delete();
				}
			} else if (!empty($this->tmpRequestModel)) {
				$this->tmpRequestModel->idContent = NULL;
				$this->tmpRequestModel->save();
			}

			// Unconditionally remove all questions related to this Asset
			$this->deleteRelatedQuestions();
			parent::afterDelete();
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	/**
	 * Return assets data for REST API, 'onlyNew' or 'ignored' depends on $params['onlyNew'] and $params['ignored'] (https://docebo.atlassian.net/browse/PR7020-1994)
	 * @param int $userId
	 * @param array $params - $params['onlyNew'] and $params['ignored']
	 * @param array $assetStatus
	 * @return array
	 */
	public static function assetToReviewApprove($userId, $params = null, $assetStatus = array(App7020Assets::CONVERSION_STATUS_FINISHED, App7020Assets::CONVERSION_STATUS_INREVIEW)) {
		$expertsChannels = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($userId);

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select(
				"c.id AS asset_id,
					c.idThumbnail AS id_thumb,
					c.contentType AS asset_type,
					c.duration AS duration, 
					c.title AS title, 
					c.userId AS owner_id,
					cp.datePublished as update_date, 
					IF(c.created >= NOW() - INTERVAL 1 DAY, true, false) AS is_new,
					count(cr.id) AS total_peer_reviews"
		);
		$commandBase->from(App7020Assets::model()->tableName() . " c");
		$commandBase->leftJoin(App7020IgnoreList::model()->tableName() . " il", ":idUser = il.idUser AND il.idObject=c.id AND il.objectType=:objectType");
		$commandBase->leftJoin(App7020ContentPublished::model()->tableName() . ' cp', 'cp.idContent = c.id');

		$actionTypes = array(App7020ContentPublished::ACTION_TYPE_EDIT, App7020ContentPublished::ACTION_TYPE_PUBLISH, App7020ContentPublished::ACTION_TYPE_FINISHED);

		$commandBase->join(
				"(
						SELECT
							MAX(datePublished) AS max_val,
							idContent AS idc
						FROM " . App7020ContentPublished::model()->tableName() . "
							WHERE actionType  IN (" . implode(',', $actionTypes) . ")
						GROUP BY idContent
					) as cp2", "cp.idContent = cp2.idc AND cp.datePublished = cp2.max_val"
		);


		$commandBase->leftJoin(App7020ChannelAssets::model()->tableName() . " ca", "c.id = ca.idAsset");
		$commandBase->leftJoin(App7020ContentReview::model()->tableName() . ' cr', 'cr.idContent = c.id');
		if ($params['ignored']) {
			$commandBase->andWhere("il.id IS NOT NULL");
		} else {
			$commandBase->andWhere("il.id IS NULL");
		}

		//if (!$params['ignored'] && !$params['onlyNew']) {
		//$commandBase->andWhere('cr.id IS NOT NULL');
		//}

		if (!Yii::app()->user->isGodAdmin) {
			if (!empty($expertsChannels))
				$commandBase->andWhere('ca.idChannel IN (' . implode(',', $expertsChannels) . ')');
			else
				$commandBase->andWhere('ca.idChannel IN (-1)');
		}
		
		$commandBase->leftJoin(App7020Channels::model()->tableName() . " ch", "ch.id = ca.idChannel");
		$commandBase->andWhere('ch.enabled = 1');

		$commandBase->andWhere('c.conversion_status IN (' . implode(',', $assetStatus) . ')');

		if ($params['onlyNew']) {
			$commandBase->andWhere("c.created >= NOW() - INTERVAL 1 DAY");
		}

		$commandBase->group('c.id');

		if (!isset($params['all'])) {
			$commandBase->limit($params['count'], $params['from']);
		}

		$returnArray = $commandBase->queryAll(true, array(
			//':actionType' => App7020ContentPublished::ACTION_TYPE_EDIT,
			':idUser' => $userId,
			':objectType' => App7020IgnoreList::OBJECT_TYPE_ASSET,
		));

//		Log::_($returnArray);
//		$returnArray = $commandBase->text;
		return $returnArray;
	}

	/**
	 * Returns an array with reviewed assets related with selected expert
	 *
	 * @param int required $userId ID of the user. The returned assets must belongs to any of topics where the selected user is expert
	 * @param array optional $assetStatus the statusses of the asset
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getExpertsReviewedAssets($userId, $assetStatus = array(App7020Assets::CONVERSION_STATUS_FINISHED), $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()) {

		if (empty($userId)) {
			return false;
		}


		$searchInput = trim(Yii::app()->request->getParam('search_input', false));

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("c.*");
		$commandBase->from(App7020Assets::model()->tableName() . " c");
		$commandBase->leftJoin(App7020IgnoreList::model()->tableName() . " il", $userId . " = il.idUser AND il.idObject=c.id AND il.objectType='" . App7020IgnoreList::OBJECT_TYPE_ASSET . "'");

		$commandBase->where(" ISNULL(il.id)");

		if ($searchInput) {
			$commandBase->andWhere("c.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}


		$commandBase->andWhere(array('IN', 'conversion_status', implode(',', $assetStatus)));

		$returnArray = $commandBase->queryAll(true);

		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);

		if (!empty($providerConfig['comboListViewId'])) {
			unset($providerConfig['comboListViewId']);
		}

		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}

	/**
	 * @todo Remove toppic assigment / MUST REWORK WITH CHANNEL
	 * Make SQL Query and Configurate ComboListView
	 * @param string $method Example: onlyMy
	 * @param int $perPage
	 * @return \CArrayDataProvider
	 */
	public static function sqlDataProvider($method = false, $customConfig = array(), $forceParams = array()) {
		$myContributionsFilter = Yii::app()->request->getParam('radioSwitch', false);
//		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		$searchInput = $forceParams['search_input'] ? $forceParams['search_input'] : trim(Yii::app()->request->getParam('search_input', false));


		$tmp = Yii::app()->request->getParam('mainFilter', false);
		$mainFilter = !empty($tmp) ? Yii::app()->request->getParam('mainFilter', false) : $forceParams['sessionMainFilterKL'];




		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;

		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		$contentTags = (!empty($customConfig['tags'])) ? $customConfig['tags'] : null;

		if ($searchInput) {
			self::$search_input = $searchInput ? $searchInput : null;
		}
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select('c.*, u.userId, c.userId AS idUser');
		$commandBase->from(App7020Assets::model()->tableName() . ' c');
		$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'c.userId = u.idst');



		$commandBase->group('c.id');

		if ($method != 'onlyMy') {
			$commandBase->andWhere('c.conversion_status = ' . App7020Assets::CONVERSION_STATUS_APPROVED);
		} else {
			if (!$myContributionsFilter) {
				if ($customConfig['shortList']) {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
				} else {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
					$commandBase->andWhere('c.conversion_status < ' . App7020Assets::CONVERSION_STATUS_UNPUBLISH);
				}
			} else {
				if ($myContributionsFilter == App7020Assets::CONVERSION_STATUS_APPROVED) {
					$commandBase->andWhere('c.conversion_status = ' . App7020Assets::CONVERSION_STATUS_APPROVED);
				} elseif ($myContributionsFilter == App7020Assets::CONVERSION_STATUS_UNPUBLISH) {
					$commandBase->andWhere('c.conversion_status = ' . App7020Assets::CONVERSION_STATUS_UNPUBLISH);
				} else {
					$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
					$commandBase->andWhere('c.conversion_status < ' . App7020Assets::CONVERSION_STATUS_UNPUBLISH);
				}
			}

//$commandBase->andWhere('c.conversion_status >= ' . App7020Assets::CONVERSION_STATUS_FINISHED);
		}

// TAGS FILTER
		if ($contentTags) {
			$commandBase->leftJoin(App7020TagLink::model()->tableName() . ' tL', 'c.id = tL.idContent');
			$commandBase->andWhere('tL.idTag IN (' . implode(",", $contentTags) . ')');
		}

// ASSETS FILTER
		if ($mainFilter['assets'] > 0) {
			$commandBase->andWhere('c.contentType = "' . (int) $mainFilter['assets'] . '"');
		}

// GET ONLY MY CONTENTS
		if ($method == 'onlyMy') {
			if ($method == 'onlyMy') {
				$commandBase->andWhere('c.userId = "' . Yii::app()->user->idst . '"');
			}
		}

		if (self::$search_input != null) {
			$commandBase->andWhere("CONCAT(COALESCE(c.title,''), ' ', COALESCE(c.description,'')) LIKE :search_input", array(':search_input' => '%' . self::$search_input . '%'));
		}

// ORDERS
		$commandBase->order('created DESC');

		switch ($mainFilter['sort']) {
			case 1:
				$commandBase->order('duration DESC');
				break;
		}
		switch ($mainFilter['uploaded']) {
			case 1: // THIS WEEK
				$commandBase->andWhere('YEARWEEK(created, 1) = YEARWEEK(NOW(), 1)');
				break;
			case 2: // THIS MONTH
				$commandBase->andWhere('EXTRACT(MONTH From created) = EXTRACT(MONTH From NOW())');
				break;
			case 3: // PAST 6 MONTHs
				$commandBase->andWhere('created > DATE_SUB(NOW(),INTERVAL 6 MONTH)');
				break;
			case 4: // THIS YEAR
				$commandBase->andWhere('EXTRACT(YEAR From created) = EXTRACT(YEAR From NOW())');
				break;
		}
		$content = $commandBase->queryAll();



		if ($setPagination) {
			$pageSize = 6; // Default
			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}
		$sort = new CSort();
		$sort->attributes = array(
			'duration' => CSort::SORT_DESC,
		);
		$sort->defaultOrder = array(
			'created' => CSort::SORT_DESC,
		);

		$config = array(
			'totalItemCount' => count($content),
			'pagination' => $pagination,
			'keyField' => 'id',
			'sort' => $sort,
		);
		$dataProvider = new CArrayDataProvider($content, $config);

		return $dataProvider;
	}

	/**
	 * Make a record in content history table
	 * @param integer $idContent
	 * @return boolean
	 */
	public static function viewContent($idContent) {
		$contentHistoryObject = new App7020ContentHistory();
		$contentHistoryObject->idContent = $idContent;
		$contentHistoryObject->idUser = Yii::app()->user->idst;
		return $contentHistoryObject->save();
	}

	/**
	 * Gets similar assets ordered by  number of view, last update date, rank
	 * @param integer $idContent
	 * @param number $count
	 * @return array
	 * @depricated
	 */
	public static function getSimilarContents($idContent, $count = 5, $from = 0, $hasFailback = false, $all = false) {

		$model = App7020Assets::model()->findByPk($idContent);
		$userId = Yii::app()->user->idst;
		//GET ASSET WITH SAME TAG
		if (!empty($model->tags)) {
			$tags = App7020TagLink::getAssetsByTag($model->tags);
		} else {
			$tags = array(-1);
		}
		//implode the result for the SQL
		$tags = implode(',', $tags);
		//get visible channels for this User
		$conf_extr_channels = array(
			'idUser' => Yii::app()->user->idst,
			'idsOnly' => true
		);
		$userChannels = array_merge(array(-1), App7020Channels::extractUserChannels($conf_extr_channels, false));
		$finalChannel = array(-1);
		//If asset is private
		if (!$model->isPrivate()) {
			$assetChannels = App7020ChannelAssets::getChannelsByContent($idContent);
			//get same channel visibility on asset and user
			$finalChannel = array_merge($finalChannel, array_intersect($assetChannels, $userChannels));
			$userChannelsVis = '';
			$userPublicVis = 'AND acs.idChannel IN ( ' . implode(',', $userChannels) . ')';
		} else {
			$finalChannel = array(-1);
			$userPublicVis = '';
			//for private assets add visibility
			$userChannels = implode(',', $userChannels);
			$userChannelsVis = "AND ( acs.idChannel IN ({$userChannels}))";
		}

		//implode the result for the SQL
		$finalChannel = implode(',', $finalChannel);
		//SET THE LIMITS
		$limit = '';
		if ($all == false) {
			$limit = " LIMIT  {$from}, {$count}";
		}
		//OVERWRITE THE LIMIT IF WE WORKS WITH PRIVATE MODEL
		if ($model->isPrivate()) {
			$limit = ' LIMIT 0, 5';
		}
		$nonPrivateAsset = '';
		if (!$model->isPrivate()) {
			$nonPrivateAsset .=
					<<<SQL
                                (
								SELECT
								(
									SELECT COUNT(DISTINCT idUser)
									FROM app7020_content_history
									WHERE idContent= ac.id
								)  as contentViews,
								(
									SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0)
									FROM app7020_content_rating
									WHERE idContent= ac.id
								) as contentRating,
								1 as ordering,
								ac.*
								FROM app7020_content as ac
								INNER JOIN app7020_channel_assets as acs ON acs.idAsset = ac.id
								INNER JOIN app7020_channels as acc ON acc.id = acs.idChannel
								WHERE ( acs.idChannel IN ({$finalChannel}))
								AND ac.conversion_status = 20
								AND ac.is_private = 0
								AND ac.contentType != 19
								AND acs.asset_type = 1
								AND acc.enabled = 1

 						)

						UNION ALL
						(
								SELECT
									(

										SELECT COUNT(DISTINCT idUser)
										FROM app7020_content_history
										WHERE idContent= ac.id
									)  as contentViews,

									(
										SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0)
										FROM app7020_content_rating
										WHERE idContent= ac.id
									) as contentRating,
									1 as ordering,
									ac.*
									FROM app7020_content as ac
									INNER JOIN app7020_channel_assets acs ON acs.idAsset = ac.id
									INNER JOIN app7020_channels acc ON acc.id = acs.idChannel
									WHERE ac.id IN ({$tags})
									{$userPublicVis}
									AND ac.conversion_status = 20
									AND ac.is_private = 0
									AND ac.contentType != 19
									AND acc.enabled = 1
							)

SQL;
		} else {
			$hasFailback = true;
		}

		$failbackSQL = '';
		if ($hasFailback) {
			$failbackSQL .= <<<failbackSQL
					( SELECT
							(
								SELECT COUNT(DISTINCT idUser)
								FROM app7020_content_history
								WHERE idContent= ac.id
							)  as contentViews,
							(
								SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0)
								FROM app7020_content_rating
								WHERE idContent= ac.id
							) as contentRating,
							2 as ordering,
							ac.*

							FROM app7020_content as ac

							INNER JOIN app7020_channel_assets acs ON acs.idAsset = ac.id
							INNER JOIN app7020_channels acc ON acc.id = acs.idChannel
							WHERE ac.userId = {$model->userId}
							{$userChannelsVis}
							AND ac.conversion_status = 20
							AND ac.is_private = 0
							AND ac.contentType != 19
							AND acc.enabled = 1
					)
failbackSQL;
		}
		$hasBetweenUnion = '';
		if (!empty($failbackSQL) && !empty($nonPrivateAsset)) {
			$hasBetweenUnion = ' UNION ALL ';
		}


		$sql = <<<SQL
							SELECT * FROM (
							{$nonPrivateAsset}
							{$hasBetweenUnion}
							{$failbackSQL}
  						) as temporary_union_table

					WHERE temporary_union_table.id != {$idContent}
					GROUP BY temporary_union_table.id
					ORDER BY ordering ASC, contentRating DESC, contentViews DESC, temporary_union_table.id  ASC
					{$limit}
SQL;
		$dbCommand = Yii::app()->db->createCommand($sql);
		return $dbCommand->queryAll();
	}

	/**
	 * Determine user role
	 */
	public function getUserRole() {
		// Determine User Role
		if (Yii::app()->user->isGodAdmin || $this->hasReviewsAccess()) {
			$this->userRole = self::USER_ROLE_EXPERT;
		} elseif ($this->isContributor) {
			$this->userRole = self::USER_ROLE_CONTRIBUTOR;
		} else {
			$this->userRole = self::USER_ROLE_LEARNER;
		}

		return $this->userRole;
	}

	/**
	 * Return type if have more than one content with this type
	 * @return Array
	 */
	public static function getAllTypeArray() {
		$result[0] = Yii::t('app7020', 'All');
		$all = self:: getUploadTypesForUploader();
		foreach ($all as $type_key => $type) {
			$result[$type_key] = $type['title'];
		}
		return $result;
	}

	/**
	 * Return boolean answer for access of current user for answer of questions by asset
	 * @param integer $content
	 * @return boolean
	 */
	public static function getAccessForAnswerById($contentId) {
		try {
			$content = App7020Assets::model()->findByPk($contentId);
			if (empty($content)) {
				return false;
			}
			if ($content->isContributor) {
				if ($content->peer_review_settings->enableCustomSettings && $content->peer_review_settings->allowContributorAnswer) {
					return true;
				} else {
					return false;
				}
			} else if (Yii::app()->user->isGodAdmin) {
				return true;
			} else if ($content->hasReviewsAccess()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Returns Tumbnail
	 * @param array $data
	 * @param boolean $isEcho
	 * @param str $alt
	 * @param array $htmlOptions
	 * @return type
	 */
	public static function getPublcPreviewThumbnail($data = array(), $imageVariant = 'small', $isEcho = false, $alt = '', $htmlOptions = array()) {
		try {

			$content = new App7020Assets();
			$content->setAttributes($data, false);
			$content->getRelated("thumbs", true);
			return $content->getPreviewThumbnailImage($isEcho, $imageVariant, $alt, $htmlOptions);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			$coreAsset = new CoreAsset();
			$coreAsset->type = CoreAsset::TYPE_APP7020;
			$width = $coreAsset->getImageVariantWidtht($imageVariant);
			$height = $coreAsset->getImageVariantHeight($imageVariant);
			$src = '//placehold.it/' . $width . 'x' . $height . '&text=Preview';
			$htmlOptions['class'] = 'ThumbPreview';
			$image = CHtml::image($src, $alt, $htmlOptions);
			if ($isEcho) {
				echo $image;
			} else {
				return $image;
			}
		}
	}

	/**
	 * Check if file is video content
	 * @param string $filename
	 * @return boolean
	 */
	public static function isVideo($filename) {
		try {
			return self::getContentCodeByExt(App7020Helpers::getExtentionByFilename($filename)) == self::CONTENT_TYPE_VIDEO ? true : false;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Calculates the reviewed assets for the given period
	 *
	 * @param date optional $dateFrom start of the period
	 * @param date optional $dateTo end of the period
	 * @param int optional $userId ID of the user. If it's present, the returned assets must belongs to any of topics where the selected user is expert
	 *
	 * return array|integer An array with dates as the keys and count of the reviewed assets
	 */
	public static function getReviewedAssetsCountByDays($dateFrom = null, $dateTo = null, $userId = null) {


		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("id, DATE(created) AS contentDate");

		$commandBase->from = self::model()->tableName();

		$commandBase->where(" conversion_status >= " . App7020Assets::CONVERSION_STATUS_FINISHED);

		if (!empty($dateFrom)) {
			$commandBase->andWhere(" DATE(created) >= :dateFrom", array(':dateFrom' => date("Y-m-d", strtotime($dateFrom))));
		}

		if (!empty($dateTo)) {
			$commandBase->andWhere(" DATE(created) <= :dateTo", array(':dateTo' => date("Y-m-d", strtotime($dateTo))));
		}

		$returnArray = array();
		if ($userId) {
			$expertTopics = App7020TopicExpert::getTopicsByUser($userId);
		}

		$resultArray = $commandBase->queryAll(true);
		foreach ($resultArray as $value) {
			if (!isset($returnArray[$value['contentDate']])) {
				$returnArray[$value['contentDate']] = 0;
			}
			if ($userId) {

				$contentTopics = App7020TopicContent::getTopicsByContent($value['id']);
				if (count(array_intersect($expertTopics, $contentTopics)) > 0) {
					$returnArray[$value['contentDate']] ++;
				}
			} else {
				$returnArray[$value['contentDate']] ++;
			}
		}
		return $returnArray;
	}

	/**
	 * Append class to progress bar in current status
	 *
	 * @param iny $status_code
	 * @author Simeonov
	 * @return string
	 */
	public static function getProccessClass($status_code = '') {
		$appended_class = '';
		if ($status_code == self::CONVERSION_STATUS_PENDING) {
			$appended_class = 'app7020-local-proccessing';
		} elseif ($status_code == self::CONVERSION_STATUS_INPROGRESS) {
			$appended_class = 'app7020-s3-proccessing';
		} elseif ($status_code == self::CONVERSION_STATUS_S3) {
			$appended_class = 'app7020-s3-uploaded';
		} elseif ($status_code == self::CONVERSION_STATUS_SNS_SUCCESS) {
			$appended_class = 'app7020-s3-converted app7020-s3-success';
		} elseif ($status_code >= self::CONVERSION_STATUS_FINISHED && $status_code < App7020Assets::CONVERSION_STATUS_APPROVED) {
			$appended_class = 'app7020-s3-inreview';
		} elseif (self::hasS3Error($status_code)) {
			$appended_class = 'app7020-s3-wrong';
		}
		return $appended_class;
	}

	/**
	 * Check any code for status.
	 *
	 * If status is Error or warning
	 * will return true
	 *
	 * @param int $status_code            
	 * @author Simeonov
	 * @return boolean
	 */
	public static function hasS3Error($status_code) {
		$array_errors = array(
			self::CONVERSION_STATUS_SNS_ERROR,
			self::CONVERSION_STATUS_SNS_WARNING
		);
		if (in_array((int) $status_code, $array_errors)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check any code for status.
	 *
	 * If status until upload to local server or we have any error will return false
	 *
	 * @param int $status_code            
	 * @author Simeonov
	 * @return boolean
	 */
	public static function hasEditAccess($status_code) {
		$array_errors = array(
			self::CONVERSION_STATUS_SNS_ERROR,
			self::CONVERSION_STATUS_SNS_WARNING,
			self::CONVERSION_STATUS_PENDING
		);
		if (in_array((int) $status_code, $array_errors)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Check if asset is rated by current user
	 * @return boolean
	 */
	public function isRatedByMe() {
		$isRated = false;
		$modelContentRating = App7020ContentRating::model()->find(array(
			'condition' => 'idContent=:idContent AND idUser=:idUser',
			'params' => array(':idContent' => $this->id, ':idUser' => Yii::app()->user->id)
		));
		if ($modelContentRating) {
			$isRated = true;
		}
		return $isRated;
	}

	/**
	 * Return public thumb URL for Video Content by their counter
	 * @param int $countImg ex. 000001
	 * @param App7020Content $content
	 * @return string
	 */
	public function getImageInputUrl($countImg) {
		$url = CFileStorage::getS3DomainStorage(CFileStorage::COLLECTION_7020)
				->fileUrl(
				CS3StorageSt::getConventionFolder($this->filename) . '/' .
				App7020Helpers::getAmazonInputKey($this->filename) .
				'/thumb/' .
				App7020Helpers::getAmazonInputKey($this->filename) .
				'-' . $countImg);
		return $url . '.png';
	}

	/**
	 * Return model with active thumbnail
	 * @return \App7020ContentThumbs
	 */
	function getActiveThumbnail() {
		foreach ($this->thumbs as $thumb) {
			if ($thumb->is_active == 1) {
				return $thumb;
			}
		}
		return new App7020ContentThumbs;
	}

	/**
	 * Return all assigned channels to asset in flat array
	 * @return array
	 */
	public function getCheckedChannels() {

		$checked = array();

		if (!empty($this->asset_channels)) {
			foreach ($this->asset_channels as $channelAsset) {
				$checked[] = $channelAsset->idChannel;
			}
		}

		if ($this->conversion_status < 10) {
			$request = $this->request;
			$channels = App7020ChannelQuestions::model()->findAllByAttributes(array('idQuestion' => $request->idQuestion));
			foreach ($channels as $channel) {
				$checked[] = $channel->idChannel;
			}
		}

		return $checked;
	}

	/**
	 * Make SQL Query to get user's contributions
	 * @param string $method Example: onlyMy
	 * @param int $perPage
	 * @return \CArrayDataProvider
	 */
    public static function getUsersContributions($idUser, $forceParams = array('assetStatus' => array(10, 15, 18, 20)))
    {

        if (!isset($forceParams['withPlaylist'])) {
            $forceParams['withPlaylist'] = false;
        }
        if (!isset($forceParams['ignoreBlankPlaylists'])) {
            $forceParams['ignoreBlankPlaylists'] = false;
        }

		$userViewing = Yii::app()->user->idst;

        $conf_extr_channels = array(
            'idUser' => $userViewing,
            'idsOnly' => true
        );
        $userChannels = App7020Channels::extractUserChannels($conf_extr_channels, false);

        $select =
            'ccc.*,
            ccc.userId AS idUser,
            (
            SELECT COUNT(id) FROM ' . App7020ContentHistory::model()->tableName() . ' WHERE idUser = ' . $idUser . ' AND idContent = ccc.id ) as watched,
            (SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM ' . App7020ContentRating::model()->tableName()
            . ' WHERE idContent = ccc.id ) AS contentRating
            ';
        if($userViewing != $idUser){
            $commandBase1 = Yii::app()->db->createCommand();
            $commandBase1->select($select);
            $commandBase1->from(App7020Assets::model()->tableName() . ' ccc');
            $commandBase1->leftJoin(App7020ChannelAssets::model()->tableName() . ' ca', '(ccc.id = ca.idAsset)');
            $commandBase1->where('(ccc.userId = ' . $idUser . ')');
            $commandBase1->andWhere('ca.idChannel IN(' . implode(',', $userChannels) . ')');
            $commandBase1->andWhere('ccc.contentType != ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
            $commandBase1->andWhere('ccc.is_private != 1');
            if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                $commandBase1->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
            }

            $commandBase2 = Yii::app()->db->createCommand();
            $commandBase2->select($select);
            $commandBase2->from(App7020Assets::model()->tableName() . ' ccc');
            $commandBase2->leftJoin(App7020Invitations::model()->tableName() . ' inv', 'inv.idContent = ccc.id');
            $commandBase2->where('(ccc.userId = ' . $idUser . ')');
            $commandBase2->andWhere('ccc.contentType != ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
            $commandBase2->andWhere('ccc.is_private = 1');
            $commandBase2->andWhere('inv.idInvited = ' . $userViewing);
            if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                $commandBase2->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
            }
            $commandBase1->union($commandBase2->getText());
            if ($forceParams['withPlaylist']) {
                $commandBase3 = Yii::app()->db->createCommand();
                $commandBase3->select($select);
                $commandBase3->from(App7020Assets::model()->tableName() . ' ccc');
                $commandBase3->leftJoin('app7020_content_playlists cp',' cp.idPlaylist = ccc.id ');
                $commandBase3->where('(ccc.userId = ' . $idUser . ')');
                $commandBase3->andWhere('ccc.contentType = ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
                $commandBase3->andWhere('(ccc.is_private != 1');
                if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                    $commandBase3->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
                }
                if ($forceParams['ignoreBlankPlaylists']) {
                    $commandBase3->andWhere('cp.idContent IS NOT NULL ');
                }

                $commandBase1->union($commandBase3->getText().')');
            }


            $commandBase1->group('id');

            if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                $commandBase1->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
            }

            $commandBase1->order('created DESC');

            if (isset($forceParams['from']) && !empty($forceParams['count'])) {
                $commandBase1->limit($forceParams['count'], $forceParams['from']);
            }
        }else{
            $commandBase1 = Yii::app()->db->createCommand();
            $commandBase1->select($select);
            $commandBase1->from(App7020Assets::model()->tableName() . ' ccc');
            $commandBase1->where('(ccc.userId = ' . $idUser . ')');
            $commandBase1->andWhere('ccc.contentType != ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
            if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                $commandBase1->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
            }

            if ($forceParams['withPlaylist']) {
                $commandBase3 = Yii::app()->db->createCommand();
                $commandBase3->select($select);
                $commandBase3->from(App7020Assets::model()->tableName() . ' ccc');
                $commandBase3->leftJoin('app7020_content_playlists cp',' cp.idPlaylist = ccc.id ');
                $commandBase3->where('(ccc.userId = ' . $idUser . ')');
                $commandBase3->andWhere('ccc.contentType = ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
                if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                    $commandBase3->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
                }
                if ($forceParams['ignoreBlankPlaylists']) {
                    $commandBase3->andWhere('cp.idContent IS NOT NULL ');
                }

                $commandBase1->union($commandBase3->getText());
            }


            $commandBase1->group('id');

            if (is_array($forceParams['assetStatus']) && $forceParams['assetStatus']) {
                $commandBase1->andWhere(array("IN", "conversion_status", $forceParams['assetStatus']));
            }

            $commandBase1->order('created DESC');

            if (isset($forceParams['from']) && !empty($forceParams['count'])) {
                $commandBase1->limit($forceParams['count'], $forceParams['from']);
            }
        }
        $result = $commandBase1->queryAll();

		$config = array(
			'totalItemCount' => count($result),
			'pagination' => false,
			'keyField' => 'id',
		);
		$dataProvider = new CArrayDataProvider($result, $config);

		return $dataProvider;
	}

	/**
	 * On base channel upload permissions retuer the conversion status after upload an publish the asset
	 * @param array $channel_list stack array with channel ids for proccessing
	 * @return integer
	 */
	public static function getFinalConversionStatus($channel_list = array()) {
		//all channels who logged user is an expert;
		$userExpertsChannels = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert();

		//find for some channel is expert neededm and user is an axpert 
		$result = array_intersect($channel_list, $userExpertsChannels);
		if (!empty($result)) {
			return App7020Assets::CONVERSION_STATUS_APPROVED;
		} elseif (App7020Channels::checkChannelsForPeerReviewReqirement($channel_list)) {
			return App7020Assets::CONVERSION_STATUS_FINISHED;
		} else {
			return App7020Assets::CONVERSION_STATUS_APPROVED;
		}
	}

	/**
	 * is asset private
	 * @return boolean
	 */
	public function isPrivate() {
		return (bool) $this->is_private;
	}

	/**
	 * Get Asset Thumbnail Uri
	 */
	public static function getAssetThumbnailUri($idAsset) {
		if ($assetObject = App7020Assets::model()->findByPk($idAsset)) {
			$imgUri = $assetObject->getPreviewThumbnailImage(false, 'small', '', array());
			preg_match_all('/src="([^"]+)/', $imgUri, $imgageAttr);

			if (count($imgageAttr) > 0)
				return $imgageAttr[1][0];

			return null;
		}
		return null;
	}

	/**
	 * Returns a flat array of IDs of users, which can view the given asset
	 * 
	 * @param integer $idAsset
	 * @param bool $includeLoggedAdmin - when it's true, the function check if the logged user is admin, and if yes, includes the admin's ID into result array
	 * @return Array
	 */
	public static function getUsersWhichCanViewAnAsset($idAsset, $includeLoggedAdmin = true, $excludeNotInvited = true) {
		$assetModel = self::model()->findByPk($idAsset);

		//if fake data is provided
		if (!$assetModel->id)
			return array();

		if ($assetModel->is_private) {
			if ($excludeNotInvited) {
				//get IDs of invited users of private assets
				$sql = "SELECT DISTINCT(idInvited) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = " . $idAsset;
			} else {
				//get all user's IDs - this is used for invitation of private assets
				$sql = "SELECT idst FROM core_user";
			}

			$command = Yii::app()->db->createCommand($sql);
			$arrayIds = $command->queryColumn();
		} else {
			$channels = $assetModel->getCheckedChannels();

			if (!$channels) {
				//if for some reason there are not assigned channels (the admin has deleted the assigned channels), 
				//prepare an empty array and after that insert the contributor's and admin's IDs (if the looged user is admin)
				$arrayIds = array();
			} else {
				//implement channels visibility
				$sql = "SELECT
					cu.idst as idFinalUser

					FROM app7020_channels c

					LEFT JOIN app7020_channel_visibility cv ON cv.idChannel = c.id
					LEFT JOIN core_group_members cgm ON (cv.type=1 AND cv.idObject=cgm.idst)

					LEFT JOIN core_org_chart_tree tree1 ON (cv.type=2 AND cv.idObject=tree1.idOrg)
					LEFT JOIN core_group_members cgm1 ON (cgm1.idst=tree1.idst_oc)

					LEFT JOIN core_org_chart_tree tree21 ON (cv.type=3 AND cv.idObject=tree21.idOrg)
					LEFT JOIN core_org_chart_tree tree22 ON ( (tree22.iLeft >= tree21.iLeft) AND (tree22.iRight <= tree21.iRight ) )

					LEFT JOIN core_group_members cgm22 ON (cgm22.idst=tree22.idst_oc)

					LEFT JOIN core_user cu ON (cgm.idstMember=cu.idst OR cgm1.idstMember=cu.idst OR cgm22.idstMember=cu.idst) OR (cv.idChannel IS NULL)

                    LEFT JOIN app7020_experts e ON e.idUser = cu.idst
					LEFT JOIN app7020_channel_experts ce ON e.id = ce.idExpert

					WHERE cu.idst IS NOT NULL AND c.id IN (" . implode(",", $channels) . ") OR ce.idChannel IN(" . implode(",", $channels) . ")
					GROUP BY idFinalUser";

				$command = Yii::app()->db->createCommand($sql);
                $arrayIds = $command->queryColumn();
			}
		}

		//After implementation of private/public logic,
		//give rights to the admins and the contributor to view the asset if theirs IDs are missing in 
		//the composed array

		if ($includeLoggedAdmin) {
			//if the logged user is admin, and his/her ID is not present in the constructed array - push the ID
			if (Yii::app()->user->isGodAdmin && !in_array(Yii::app()->user->id, $arrayIds)) {
                array_push($arrayIds, Yii::app()->user->idst);
			}
		}

		//If the ID of contributor is missing in the constructed array - push the ID
		//This case could happen, when the admins change the channels visibility after publishing the asset
		if (!in_array($assetModel->userId, $arrayIds)) {
			array_push($arrayIds, $assetModel->userId);
		}

		return $arrayIds;
	}

	/**
	 * Get all Experts for given Asset
	 * @param int $idAsset
	 * @return CoreUser[]
	 */
	public static function getAssetExperts($idAsset) {
//		$criteria = new CDbCriteria();
//		$criteria->select = 'DISTINCT  t.*';
//		$criteria->join = 'INNER JOIN app7020_experts as ae ON t.idst = ae.idUser '
//				. 'INNER JOIN app7020_channel_experts as ace ON ace.idExpert  = ae.id '
//				. 'INNER JOIN app7020_channels as achan ON achan.id = ace.idChannel '
//				. 'INNER JOIN app7020_channel_assets as aca ON aca.idChannel = achan.id  '
//				. 'INNER JOIN app7020_content as ac ON ac.id = aca.idAsset';
//		$criteria->condition = 'ac.id = :acid';
//		$criteria->params = array('acid' => $idAsset);
//		$users = CoreUser::model()->findAll($criteria);
//		return $users;
//        SELECT e.idUser FROM app7020_channel_experts ce
//left join app7020_experts e ON ce.idExpert = e.id
//LEFT JOIN app7020_channel_assets ca on ce.idChannel = ca.idChannel
//WHERE ca.idChannel IN (14,8) AND ca.asset_type = 1 AND ca.idAsset = 343
        $custom_channel_visibility_select = array(
            "idUser" => Yii::app()->user->idst,
            "ignoreVisallChannels" => false,
            "ignoreGodAdminEffect" => !Yii::app()->user->getIsGodadmin(),
            'appendEnabledEffect' => true,
            'ignoreSystemChannels' => true,
            'idAsset' => $idAsset,
            'return' => 'data'
        );
        $channels = App7020Channels::extractUserChannels($custom_channel_visibility_select);
        foreach($channels as $channel){
            $visChannels[]=$channel['id'];
        }
        $command = YII::app()->db->createCommand();
        $command->select('idst as idExpert');
        $command->from(CoreUser::model()->tableName().' cu');

        $command->join(App7020Experts::model()->tableName(). ' e', 'e.idUser = cu.idst');
        $command->leftJoin(App7020ChannelExperts::model()->tableName(). ' ce', 'ce.idExpert = e.id');
        $command->leftJoin(App7020ChannelAssets::model()->tableName(). ' ca', 'ca.idChannel = ce.idChannel');
        $command->andWhere('ca.asset_type = 1');
        $command->andWhere('ca.idAsset = '. $idAsset);
        $command->andWhere(['in','ca.idChannel',  $visChannels]);
        $result = $command->queryAll();
        foreach($result as $res){
            $res2[]=$res['idExpert'];
        }
        return $res2;

    }

	/**
	 * Get all content views for given asset
	 * @param int $idContent
	 * @return int
	 */
	public static function getAllContentViews($idContent) {
		$idContent = App7020Assets::model()->findByPk($idContent)->id;
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("COUNT(id) as P ");
		$dbCommand->from(App7020ContentHistory::model()->tableName());
		$dbCommand->where(" idContent= " . $idContent . "");
		$a = $dbCommand->queryRow();
		return $a["P"];
	}

	public static function getChannelsNames($idAsset, $asString = true) {
		$assetObject = self::model()->findByPk($idAsset);
		$channels = array();
		if ($assetObject->id) {
			$assetChannels = $assetObject->getCheckedChannels();
			foreach ($assetChannels as $value) {
				$channelTranslation = App7020Channels::model()->findByPk($value)->translation();
				if ($channelTranslation)
					$channels[] = $channelTranslation['name'];
			}
		}
		return $asString ? implode(",", $channels) : $channels;
	}

	/**
	 * Calculates global watch rate of the asset
	 * @param type $idAsset
	 */
	public static function globalWatchRate($idAsset) {

		$command = Yii::app()->db->createCommand();
		$command->select("c.id, "
				. "(SELECT COUNT(id) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = c.id) as invitations, "
				. "(SELECT COUNT(DISTINCT idUser) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent = c.id) AS views");
		$command->from(App7020Assets::model()->tableName() . ' c');

		$command->where('c.id = :idAsset', array(':idAsset' => $idAsset));

		$result = $command->queryRow();

		return ($result['invitations']) ? round(($result['views'] * 100 / $result['invitations']), 2) . ' %' : '0 %';
	}

	/**
	 * Get average reaction time
	 * @param type $idContent
	 */
	public static function getAverageReactionTime($idContent) {

		$sql = "SELECT TIMESTAMPDIFF(SECOND, ai.created, ch.viewed) as reactionTime "
				. "FROM app7020_invitations ai LEFT JOIN app7020_content_history ch ON ai.idInvited=ch.idUser AND ch.idContent=" . $idContent . " AND "
				. "ch.viewed = (SELECT MIN(viewed) FROM app7020_content_history WHERE idUser=ai.idInvited AND idContent=" . $idContent . ") "
				. "WHERE ai.idContent=" . $idContent;
		$sql .= " GROUP BY ai.idInvited";
		$dbCommand = Yii::app()->db->createCommand($sql);
		$result = $dbCommand->queryColumn();

		return $result ? round(array_sum($result) / count($result), 2) : 0;
	}

	/**
	 * Get count of views for all assets of given user
	 * @param int $userId
	 * @param int $timeframe
	 * @return int
	 */
	public static function getAssetsViewsCountByUser($userId, $timeframe) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select('COUNT(DISTINCT ch.idUser)');
		$commandBase->from(App7020Assets::model()->tableName() . ' c');
		$commandBase->join(App7020ContentHistory::model()->tableName() . ' ch', 'c.id = ch.idContent');
		$commandBase->andWhere("c.userId = :idUser");
		$commandBase->andWhere("viewed BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");

		return $commandBase->queryScalar(array(':idUser' => $userId, ':timeInterval' => $timeframe));
	}

	/**
	 * Data provider for page '/lms/index.php?r=myActivities/userReport&from=reportManagement&tab=assets-ranks'
	 * @param int $userId
	 * @param str $searchInput
	 * @param str $orderASC
	 * @return \CArrayDataProvider
	 */
	public static function getAssetsRanksDataProvider($userId, $searchInput, $orderASC, $orderType) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
			c.id,
			c.title,
			c.contentType AS content_type,
			c.duration,
			(SELECT COUNT(DISTINCT idUser) FROM app7020_content_history AS ch WHERE ch.idContent = c.id) AS count_views,
			@count_views := (SELECT COUNT(DISTINCT idUser) FROM app7020_invitations AS inv  LEFT JOIN app7020_content_history ch ON (ch.idContent=inv.idContent AND inv.idInvited=idUser) WHERE ch.idContent = c.id) AS count_views_invited,
			(SELECT COALESCE(ROUND(SUM(cr.rating)/COUNT(*), 1),0) FROM app7020_content_rating AS cr WHERE cr.idContent = c.id) AS sum_ratings,
			@count_invitations := (SELECT COUNT(DISTINCT idInvited) FROM app7020_invitations AS i WHERE i.idContent = c.id) AS count_invitations,
			CONCAT(COALESCE(ROUND((@count_views/@count_invitations)*100,1), 0), '%') AS watch_rate,
			CONCAT(COALESCE(ROUND((@count_views/@count_invitations)*100,1), 0)) AS watch_rate_clear,
			cp.datePublished,
			ch2.viewed_time,
			TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, cp.datePublished, ch2.viewed_time)),'%H') as reaction_time_check,
			IF(TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, cp.datePublished, ch2.viewed_time)),'%H') >= 24,
			(SELECT CONCAT(
			   FLOOR(HOUR(TIMEDIFF(cp.datePublished, ch2.viewed_time)) / 24), 'd  ',
			  MOD(HOUR(TIMEDIFF(cp.datePublished, ch2.viewed_time)), 24), 'h ',
			  MINUTE(TIMEDIFF(cp.datePublished, ch2.viewed_time)), 'm')
			)
			,
			TIME_FORMAT(SEC_TO_TIME(TIMESTAMPDIFF(SECOND, cp.datePublished, ch2.viewed_time)),'%Hh %im %Ss')
			) as reaction_time
		");
		$commandBase->from(App7020Assets::model()->tableName() . ' c');
		$commandBase->join(App7020ContentPublished::model()->tableName() . ' cp', "c.id = cp.idContent AND cp.actionType = :actionType");
		$commandBase->leftJoin("
			(
				SELECT
					idContent,
					MIN(viewed) as viewed_time 
				FROM " . App7020ContentHistory::model()->tableName() . " 
				GROUP BY idContent
			) as ch2", "c.id = ch2.idContent"
		);
		$commandBase->andWhere("c.userId = :userId");

		if ($searchInput) {
			$commandBase->andWhere("c.title LIKE :title", array(':title' => '%' . $searchInput . '%'));
		}

		$commandBase->andWhere("c.conversion_status = :conversionStatus");
		if ($orderASC) {
			if ($orderType == 'rating') {
				$commandBase->order("sum_ratings ASC");
			} elseif ($orderType == 'invitation') {
				$commandBase->order("count_invitations ASC");
			} elseif ($orderType == 'wr') {
				$commandBase->order("watch_rate_clear ASC");
			} else {
				$commandBase->order("count_views ASC");
			}
		} else {
			if ($orderType == 'rating') {
				$commandBase->order("sum_ratings DESC");
			} elseif ($orderType == 'invitation') {
				$commandBase->order("count_invitations DESC");
			} elseif ($orderType == 'wr') {
				$commandBase->order("watch_rate_clear DESC");
			} else {
				$commandBase->order("count_views DESC");
			}
		}
		$returnArray = $commandBase->queryAll(true, array(
			':userId' => $userId,
			':conversionStatus' => self::CONVERSION_STATUS_APPROVED,
			':actionType' => App7020ContentPublished::ACTION_TYPE_PUBLISH,
		));

		$pageSize = Settings::get('elements_per_page', 10);
		$config = array(
			'totalItemCount' => count($returnArray),
			'pagination' => array('pageSize' => $pageSize)
		);
		$dataProvider = new CArrayDataProvider($returnArray, $config);
		return $dataProvider;
	}

	/**
	 * Get count of views for all assets of given user
	 * @param int $userId
	 * @param int $timeframe
	 * @return int
	 */
	public static function canRate($idAsset) {
		$asset = App7020Assets::model()->findByPk($idAsset);
		$alreadyVoted = App7020ContentRating::model()->findByAttributes(array('idContent' => $idAsset, 'idUser' => Yii::app()->user->id));
		if (Yii::app()->user->id != $asset->userId && !$alreadyVoted) {
			return true;
		}
		return false;
	}

	/**
	 * Check logged user has access to unpublish the asset
	 * @return boolean
	 */
	public function publishingRights() {
		//get experts
		$assetExperts = App7020Assets::getAssetExperts($this->id);
		$hasBeenPublished = App7020ContentPublished::model()->findByAttributes(array('idContent' => $this->id, 'actionType' => 1));
		//case only when we have private an asset and I'm contributor
		if ($this->isPrivate() && $this->isContributor) {
			return true;
		}
		//we exclude all criterias
		if (Yii::app()->user->isGodAdmin) {
			return true;
		} else if (in_array(Yii::app()->user->idst, $assetExperts)) {
			return true;
		} else if ($this->isContributor) {
			// if contributor is expert for an asset
			if (in_array(Yii::app()->user->idst, $assetExperts)) {
				return true;
			} else if (!empty($hasBeenPublished)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * return flat array with all the user's id
	 * @param array $arrayUsers
	 * @return array
	 */
	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {
			$users[] = $userModel->idst;
		}
		return $users;
	}

	/**
	 * Return array with google drive documents
	 * @depricate Don't use it 
	 */
	public static function googleDriveTypes() {
		$types = array(
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS,
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS,
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES,
		);
		return $types;
	}

	/**
	 * return if some type of files is google
	 */
	public static function isGoogleDriveTypes($conetentType) {

		return in_array($conetentType, array(
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_DOCS,
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SHEETS,
			App7020Assets::CONTENT_TYPE_GOOGLE_DRIVE_SLIDES,
		));
	}

	/*	 * ************************************************************************
	 * ** NEW GETTERS OF DATA AND REPLACE WITH THESE OLDER DEPRICATED METHODS **
	 * ************************************************************************* */

	static function getAllVisibleAssets($idUser = false, $perPage = 12, $current_page = 1, $asActiveRecords = false) {

		$dp = DataProvider::factory("UserAssets", array(
					"idUser" => $idUser === false ? Yii::app()->user->idst : $idUser,
					"ignoreVisallChannels" => false, // "visible to all" items are maerked as such in ElasticSearch and will be visible to all, noo need to add them here
					"ignoreGodAdminEffect" => true, // Admins will see everything in search anyway 
					"ignorePrivate" => false, // we have to ignore all visible private asset in 
					"ignorePlaylists" => false, // Admins will see everything in search anyway 
					"ignoreBlankPlaylists" => true, // Admins will see everything in search anyway 
					"conversionStatus" => App7020Assets::CONVERSION_STATUS_APPROVED, // Approved only
					"customSelect" => [
						'id' => 'id',
						'title' => 'name',
						'created' => 'created',
						'conversion_status' => 'status'
					]
				))->provider();

		return $dp;
	}

	/**
	 * Check the user has visibility to an asset;
	 * @param int $idAsset
	 * @param int $idUser
	 * @return boolean
	 */
	static function isVisibleAsset($idAsset, $idUser = false) {
		$dp = DataProvider::factory("UserChannels", array(
					"idUser" => $idUser === false ? Yii::app()->user->idst : $idUser,
					"idAsset" => $idAsset === false ? $this->id : $idAsset,
					"ignoreVisallChannels" => false, // "visible to all" items are maerked as such in ElasticSearch and will be visible to all, noo need to add them here
					"ignoreGodAdminEffect" => true, // Admins will see everything in search anyway 
					"ignorePrivate" => false, // we have to ignore all visible private asset in 
					"ignorePlaylists" => false, // Admins will see everything in search anyway 
					"ignoreBlankPlaylists" => false, // Admins will see everything in search anyway 
					//"conversionStatus" => App7020Assets::CONVERSION_STATUS_APPROVED, // Approved only
					"customSelect" => [
						'id' => 'id',
						'title' => 'name',
						'created' => 'created',
						'conversion_status' => 'status'
					]
				))->provider();

		$iterator = new CDataProviderIterator($dp);
		return (bool) ((int) $iterator->getTotalItemCount());
	}

}
