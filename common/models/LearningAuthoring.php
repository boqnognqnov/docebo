<?php

/**
 * This is the model class for table "learning_authoring".
 *
 * The followings are the available columns in table 'learning_authoring':
 * @property integer $authoring_id
 * @property string $title
 * @property string $last_update
 * @property integer $file_version
 * @property string $file_extension
 * @property integer $slide_count
 * @property integer $status_id
 * @property boolean $slide_number
 * @property boolean $playbar
 * @property boolean $fullscreen
 * @property string $background_color
 * @property string $text_color
 * @property string $hover_color
 * @property string $copy_of
 * @property string $pipeline_id
 *
 * @property LearningOrganization $organization
 */
class LearningAuthoring extends CActiveRecord
{
	const DEFAULT_TITLE = 'Authoring Title';

	public $resource_type = null; // shared|private|null
	public $parentFolder = false; //to be set when creatin

	public $organizationSync = true;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningAuthoring the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_authoring';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required', 'on' => 'edit'),
			array('slide_number, playbar, fullscreen, background_color, text_color, hover_color', 'required', 'on' => 'publish'),
//			array('file', 'file', 'types' => 'ppt,pptx,pdf,odp', 'allowEmpty' => false, 'on' => 'insert', 'wrongType' => 'Only files with these extensions are allowed: ppt, pptx, pdf, odp.'),
			array('file_version, slide_count, status_id, slider_offset', 'numerical', 'integerOnly'=>true),
			array('title, file_extension, pipeline_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('authoring_id, title, last_update, file_version, file_extension, slide_count, status_id, slide_number, playbar, fullscreen, background_color, text_color, hover_color, copy_of, pipeline_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'status' => array(self::BELONGS_TO, 'LearningAuthoringStatus', 'status_id'),
			'organization' => array(self::HAS_ONE, 'LearningOrganization', 'idResource', 'condition'=>'organization.objectType=:authoring', 'params'=>array(':authoring'=>LearningOrganization::OBJECT_TYPE_AUTHORING))
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'authoring_id' => Yii::t('menu_over', '_AUTHORING'),
			'title' => Yii::t('standard', '_TITLE'),
			'last_update' => Yii::t('learningAuthoring', 'LAST_UPDATE'),
			'file_version' => Yii::t('standard', '_VERSION'),
			'file_extension' => Yii::t('learningAuthoring', 'FILE_EXTENSION'),
			'slide_count' => Yii::t('learningAuthoring', 'SLIDE_COUNT'),
			'slide_number' => Yii::t('learningAuthoring', 'SLIDE_NUMBER'),
			'playbar' => Yii::t('learningAuthoring', 'PLAYBAR'),
			'fullscreen' => Yii::t('learningAuthoring', 'FULLSCREEN'),
			'background_color' => Yii::t('templatemanager', 'Background color'),
			'text_color' => Yii::t('templatemanager', 'Generic text color'),
			'hover_color' => Yii::t('authoring', 'HOVER_COLOR'),
			'slider_offset' => Yii::t('authoring', 'Slider Offset'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('authoring_id',$this->authoring_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('last_update',Yii::app()->localtime->fromLocalDateTime($this->last_update),true);
		$criteria->compare('file_version',$this->file_version);
		$criteria->compare('file_extension',$this->file_extension,true);
		$criteria->compare('slide_count',$this->slide_count);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('slide_number',$this->slide_number);
		$criteria->compare('playbar',$this->playbar);
		$criteria->compare('fullscreen',$this->fullscreen);
		$criteria->compare('background_color',$this->background_color);
		$criteria->compare('text_color',$this->text_color);
		$criteria->compare('hover_color',$this->hover_color);
		$criteria->compare('pipeline_id',$this->pipeline_id);
		$criteria->with = 'status';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function behaviors() {
		return array(
//			'fileBehavior' => array(
//				'class' => 'qsExt.local.files.PresentationBehavior',
//				'filePropertyName' => 'file',
//				'fileStorageBucketName' => Yii::app()->params['Authoring']['bucketName'],
//				//'subDirTemplate' => '{__file__}' . DIRECTORY_SEPARATOR . '{^pk}' . DIRECTORY_SEPARATOR . '{pk}',
//				'subDirTemplate' =>  Yii::app()->params['Authoring']['subDirTemplate'],//'{^__domain__}'.'/'.'{^^__domain__}'.'/'.'{__domain__}'.'/'.'{pk}',
//				'imageResolutions' => Yii::app()->params['Authoring']['imageResolutions'],/*array(
//					'full' => '1280x960',
//					'mini' => '200x150'
//				),*/
//			),
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('last_update medium'),
			 'dateAttributes' => array()
            )
		);
	}

	public function beforeSave()
	{
		$this->last_update = Yii::app()->localtime->getUTCNow();
		return parent::beforeSave();
	}

	public function afterSave() {

		self::flushAuthoringCache();

		//create/update LearningOrganization

		// Is this a private or shared resource. If this session variable
		// is blank, then it's a resource for a specific course.
		if(isset($_SESSION['resource_type'])){
			$this->resource_type = $_SESSION['resource_type'];
		}

		if ($this->organizationSync) {

			Yii::log('Saving course authoring object', CLogger::LEVEL_INFO, __CLASS__ . __METHOD__ .'->line '. __LINE__);
			$loModel = LearningOrganization::model()->findByAttributes(array('idResource' => $this->authoring_id, 'objectType' => 'authoring'));

			if ($loModel) {
				if($this->resource_type == 'private' || $this->resource_type == 'shared'){
					$loModel->path = '/root/'.$this->title;
				}
				$loModel->title = $this->title;
				$loModel->saveNode(false); // Don't run validation since some properties may be still missing
			} else {

				$idParent = (int)$this->parentFolder;
				if (empty($idParent)) {
					$idCourse = Yii::app()->learningOrganization->getIdCourse();
					$idParent = LearningOrganization::model()->getRoot($idCourse)->getPrimaryKey();
				}

				Yii::app()->learningOrganization->addLearningObject('authoring', $this->authoring_id, $this->title, $idParent);
			}

		}

		parent::afterSave();
	}

	public function init()
	{
		if ($this->isNewRecord)
		{
			$this->title = self::DEFAULT_TITLE;
			$this->status_id = LearningAuthoringStatus::PENDING;
			$this->background_color = '#333333';
			$this->text_color = '#333333';
			$this->hover_color = '#52A1DD';
			$this->slide_number = true;
			$this->playbar = true;
			$this->fullscreen = true;
		}
	}

	/**
	 * Confirm authoring
	 *
	 * @return boolean Model saved
	 */
	public function confirm()
	{
		$this->status_id = LearningAuthoringStatus::CONFIRMED;
		return $this->save();
	}

	/**
	 * Publish authoring
	 *
	 * @return boolean Model saved
	 */
	public function publish()
	{
		$this->status_id = LearningAuthoringStatus::PUBLISHED;
		return $this->save();
	}

	/**
	 * Set status pending
	 *
	 * @return boolean Model saved
	 */
	public function applyStatusPending()
	{
		$this->status_id = LearningAuthoringStatus::PENDING;
		return $this->save();
	}

	/**
	 * Creates learning conversion record for current authoring and user with idst = $userId
	 *
	 * @param integer $userId User idst
	 * @return boolean Learning conversion created or exists
	 */
	public function track($userId, $idCourse)
	{
		$learningConversion = LearningConversion::model()->findByAttributes(array(
			'authoring_id' => $this->authoring_id,
			'user_id' => $userId,
		));

		if ($learningConversion == null)
		{
			$learningConversion = new LearningConversion();
			$learningConversion->user_id = $userId;
			$learningConversion->idCourse = $idCourse;
			$learningConversion->authoring_id = $this->authoring_id;
			return $learningConversion->save();
		}

		return true;
	}

	/**
	 * Sets status 'completed' for current authoring and aser with idst = $userId
	 *
	 * @param integer $userId User idst
	 * @return boolean Completion status is set successfully
	 */
	public function complete($userId)
	{
		$learningConversion = LearningConversion::model()->findByAttributes(array(
			'authoring_id' => $this->authoring_id,
			'user_id' => $userId,
		));

		if ($learningConversion != null)
		{
			$learningConversion->completion_status = LearningConversion::STATUS_COMPLETED;
			return $learningConversion->save();
		}

		return false;
	}

	/**
	 * Sets suspend data for current authoring and user with idst = $userId
	 *
	 * @param string $suspendData suspend data
	 * @param integer $userId User idst
	 * @return boolean LearningConversion is updated successfully
	 */
	public function createBookmark($suspendData, $userId, $idCourse)
	{
		$learningConversion = LearningConversion::model()->findByAttributes(array(
			'authoring_id' => $this->authoring_id,
			'user_id' => $userId,
		));

		if ($learningConversion != null)
		{
			$learningConversion->suspend_data = CJSON::encode($suspendData);
			if ($suspendData['slideNumber'] == $this->slide_count)
				$learningConversion->completion_status = LearningConversion::STATUS_COMPLETED;

			$learningConversion->idCourse = $idCourse;
			return $learningConversion->save();
		}

		return false;
	}

	/**
	 * Returns suspend data for user with idst = $userId
	 *
	 * @param integer $userId User idst
	 * @return string
	 */
	public function getSuspendData($userId = null)
	{
		if (!$userId)
		{
			$userId = Yii::app()->user->id;
		}

		$learningConversion = LearningConversion::model()->findByAttributes(array(
			'authoring_id' => $this->authoring_id,
			'user_id' => $userId,
		));

		if ($learningConversion)
		{
			return $learningConversion->suspend_data;
		}

		return null;
	}

	/**
	 * Deletes LearningAuthoring by ID
	 *
	 * @param integer $authoringId Authoring ID
	 * @return boolean Success
	 */
	public function deleteAuthoring($authoringId)
	{
		$model = $this->model()->findByPk($authoringId);
		if ($model)
		{
			return $model->delete();
		}

		return false;
	}


	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @param bool $organization
	 * @throws CException
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {

		$copy = new LearningAuthoring();
		$copy->title = $this->title;
		//$copy->last_update = date("Y-m-d H:i:s"); //beforeSave() event will take care of this
		$copy->file_version = $this->file_version;
		$copy->file_extension = $this->file_extension;
		$copy->slide_count = $this->slide_count;
		$copy->slider_offset = $this->slider_offset;
		$copy->status_id = $this->status_id;
		$copy->slide_number = $this->slide_number;
		$copy->playbar = $this->playbar;
		$copy->fullscreen = $this->fullscreen;
		$copy->background_color = $this->background_color;
		$copy->text_color = $this->text_color;
		$copy->hover_color = $this->hover_color;

		// NOTE: the "copy_of" field is actually used to generate image urls when playing slides. Only the first 'source'
		//  authoring LO contains a valid ID to generate URL, so a copy of a copy must refer to it instead to a copied LO.
		$copy->copy_of = (!empty($this->copy_of) ? $this->copy_of : $this->authoring_id);

		if (!$organization) {
			$this->organizationSync = false; //this is important, otherwise it will break DB
			$copy->organizationSync = false;
		}
		if ($copy->save(false)) {
			if (!$organization) { $this->organizationSync = true; }
			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.print_r($copy->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}


	/**
	 * Flushes (delete) all Authoring cache
	 *
	 */
	public static function flushAuthoringCache() {
		if (isset(Yii::app()->cache_authoring)) {
			Yii::app()->cache_authoring->flush();
		}
	}


	public function getJpgUrls($size = "full"){
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AUTHORING);
		if($this->slider_offset >= 1){
			$iteration = $this->slider_offset;
		}else{
			$iteration = 1;
		}
		$urls = array();
		for($i=$iteration;$i<$this->slide_count + $iteration;$i++) {
			$urls[] = $storage->fileUrl($this->authoring_id . '_full_slide' . $i . '_1.jpg',(string)$this->authoring_id);

		}
		return $urls;
	}

}