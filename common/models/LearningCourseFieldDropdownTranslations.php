<?php

/**
 * This is the model class for table "learning_course_field_dropdown_translations".
 *
 * The followings are the available columns in table 'learning_course_field_dropdown_translations':
 * @property integer $id_option
 * @property string $lang_code
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property LearningCourseFieldDropdown $idOption
 */
class LearningCourseFieldDropdownTranslations extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_field_dropdown_translations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('lang_code, translation', 'length', 'max'=>255),
			array('id_option, lang_code, translation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idOption' => array(self::BELONGS_TO, 'LearningCourseFieldDropdown', 'id_option'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_option' => 'Id Option',
			'lang_code' => 'Lang Code',
			'translation' => 'Translation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id_option',$this->id_option);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation',$this->translation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseFieldDropdownTranslations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
