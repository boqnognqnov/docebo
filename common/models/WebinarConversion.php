<?php

/**
 * This is the model class for table "webinar_conversion".
 *
 * The followings are the available columns in table 'webinar_conversion':
 * @property integer $id
 * @property integer $session_id
 * @property string $date
 * @property string $conversion_date
 * @property string $status
 * @property string $filename
 * @property string $job_id
 * @property string $error
 */
class WebinarConversion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_conversion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('session_id, date, status, filename, job_id', 'required'),
			array('session_id', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>20),
			array('filename', 'length', 'max'=>255),
			array('job_id', 'length', 'max'=>64),
			array('conversion_date, error', 'safe'),
			// The following rule is used by search().
			array('id, session_id, date, conversion_date, status, filename, job_id, error', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'session_id' => 'Session',
			'date' => 'Date',
			'conversion_date' => 'Conversion Date',
			'status' => 'Status',
			'filename' => 'Filename',
			'job_id' => 'Job',
			'error' => 'Error',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('session_id',$this->session_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('conversion_date',$this->conversion_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('job_id',$this->job_id,true);
		$criteria->compare('error',$this->error,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarConversion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
