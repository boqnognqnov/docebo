<?php

/**
 * This is the model class for table "ecommerce_transaction_info".
 *
 * The followings are the available columns in table 'ecommerce_transaction_info':
 * @property integer $item_id
 * @property integer $id_trans
 * @property integer $id_course
 * @property integer $id_date
 * @property integer $id_edition
 * @property string $code
 * @property string $name
 * @property string $price
 * @property integer $activated
 * @property string $item_type
 * @property integer $id_path
 * @property string $item_data_json
 *
 * The followings are the available model relations:
 * @property LearningCourse $course
 * @property LearningCoursepath $coursepath
 * @property EcommerceTransaction $transaction
 */
class EcommerceTransactionInfo extends CActiveRecord
{
    /* Virtual properties */
    public $total;  // Used to calculate total amount for a given transaction Id, sum(price)

	// Possible values for the "item_type" column
	const TYPE_COURSE = "course";
	const TYPE_COURSEPATH = "coursepath";
	const TYPE_COURSESEATS = "courseseats";

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ecommerce_transaction_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_trans, id_course, id_date, id_edition, id_path, activated', 'numerical', 'integerOnly'=>true),
			array('code, name, price, item_type', 'length', 'max'=>255),
			array('item_data_json', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('item_id, id_trans, id_course, id_date, id_edition, code, name, price, activated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'id_course'),
			'coursepath' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
			'transaction' => array(self::BELONGS_TO, 'EcommerceTransaction', 'id_trans'),
			'courseseats' => array(self::BELONGS_TO, 'LearningCourse', 'id_course'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'item_id' => 'Item',
			'id_trans' => 'Id Trans',
			'id_course' => 'Id Course',
			'id_date' => 'Id Date',
			'id_edition' => 'Id Edition',
			'code' => 'Code',
			'name' => 'Name',
			'price' => 'Price',
			'activated' => 'Activated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('id_trans',$this->id_trans);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_date',$this->id_date);
		$criteria->compare('id_edition',$this->id_edition);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('activated',$this->activated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EcommerceTransactionInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function delete() {
		//prevent record deletion, since transactions cannot be deleted anymore
		throw new CDbException('The ecommerce_transaction_info record #'.$this->item_id.' cannot be deleted, only soft deletion is allowed.');
	}

	public function afterDelete()
	{
		Log::_('DELETION: Transaction item with id '.$this->item_id.' cancelled by user: '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
	}

    /**
     * Delivers all transaction items for a given Transaction Id
     *
     * @return CActiveDataProvider
     */
    public function searchByTransactionId()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('t.id_trans', $this->id_trans);

        $criteria->with = array('transaction', 'course', 'coursepath');
        $criteria->together = true;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }

    /**
     * Returns the sum of prices for all items of a given transaction Id
     *
     * @param int $id_trans
     * @return float
     */
    public static function getTransactionTotal($id_trans) {
        $criteria = new CDbCriteria();
        //$criteria->compare('id_trans', $id_trans);
        $criteria->select = "sum(price) as total";
        $criteria->group = "id_trans";
        $criteria->having = "id_trans='$id_trans'";

        $model = self::model()->find($criteria);
        if ($model) {
            return $model->total;
        }
        else {
            return 0;
        }

    }
}
