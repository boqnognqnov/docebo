<?php

/**
 * This is the model class for table "app7020_custom_content_settings".
 *
 * The followings are the available columns in table 'app7020_custom_content_settings':
 * @property integer $id
 * @property integer $idContent
 * @property string $settingName
 * @property string	$settingValue
 * 
 */
class App7020CustomContentSettings extends CActiveRecord {
    
    const APP7020_DEFAULT_REVIEWS_COUNT = 10;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_custom_content_settings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

       return array(
            array('idContent, settingName', 'required'),
            array('idContent', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
       return array(
            'id' => 'ID',
            'idContent' => 'Content',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
       
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020TopicContent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

	public static function getContentCustomSetting($settingName, $idContent){
		$settingObject = self::model()->findByAttributes(array('settingName'=>$settingName, 'idContent' => $idContent));
		if($settingObject->id){
			return $settingObject->settingValue;
		} 
		
		if(!Settings::exists($settingName)){
			return false;
		}
		
		return Settings::get($settingName);
		
	}
}

