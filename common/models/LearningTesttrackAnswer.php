<?php

/**
 * This is the model class for table "learning_testtrack_answer".
 *
 * The followings are the available columns in table 'learning_testtrack_answer':
 * @property integer $idTrack
 * @property integer $idQuest
 * @property integer $idAnswer
 * @property double $score_assigned
 * @property string $more_info
 * @property integer $manual_assigned
 * @property integer $user_answer
 *
 * @property LearningTesttrack $testtrack
 */
class LearningTesttrackAnswer extends CActiveRecord
{

	const SCENARIO_EVALUATION = 'evaluation';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrackAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('more_info', 'required'),
			array('idTrack, idQuest, idAnswer, manual_assigned, user_answer', 'numerical', 'integerOnly'=>true),
			array('score_assigned', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idQuest, idAnswer, score_assigned, more_info, manual_assigned, user_answer', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questanswer' => array(self::BELONGS_TO, 'LearningTestquestanswer', 'idAnswer'),
			'testtrack' => array(self::HAS_ONE, 'LearningTesttrack', array('idTrack'=>'idTrack')),
			'quest' => array(self::BELONGS_TO, 'LearningTestquest', 'idQuest'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idQuest' => 'Id Quest',
			'idAnswer' => 'Id Answer',
			'score_assigned' => 'Score Assigned',
			'more_info' => 'More Info',
			'manual_assigned' => 'Manual Assigned',
			'user_answer' => 'User Answer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idQuest',$this->idQuest);
		$criteria->compare('idAnswer',$this->idAnswer);
		$criteria->compare('score_assigned',$this->score_assigned);
		$criteria->compare('more_info',$this->more_info,true);
		$criteria->compare('manual_assigned',$this->manual_assigned);
		$criteria->compare('user_answer',$this->user_answer);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public static function getScoreByTrackAndQuestion($trackId, $questId)
	{
		$score = 0;
		$answers = self::model()->findAllByAttributes(array(
			'idTrack' => $trackId,
			'idQuest' => $questId,
		));

		foreach ($answers as $answer)
			$score += $answer->score_assigned;

		return $score;
	}

	public function afterSave(){
		if($this->getScenario() == self::SCENARIO_EVALUATION) {
			$loModel = (isset($this->testtrack) && $this->testtrack) ? LearningOrganization::model()->findByPk($this->testtrack->idReference) : LearningOrganization::model()->findByPk(LearningTesttrack::model()->findByPk($this->idTrack)->idReference);
			$modOrigin = LearningOrganization::getModOrigin($loModel->idOrg);
			$event = new DEvent($this, array(
					'user' => CoreUser::model()->findByPk($this->testtrack->idUser),
					'course' => ($modOrigin && $modOrigin != $loModel->idOrg)? LearningCourse::model()->findByPk(LearningOrganization::model()->findByPk($modOrigin)->idCourse) : LearningCourse::model()->findByPk($loModel->idCourse),
					'learningobject' => $loModel,
					'answertrack' => $this,
			));
			Yii::app()->event->raise('InstructorEvaluatedQuestion', $event);
		}
	}
}