<?php

/**
 * This is the model class for table "core_plugin".
 *
 * The followings are the available columns in table 'core_plugin':
 * @property string $plugin_id
 * @property string $plugin_name
 * @property string $plugin_title
 * @property integer $is_active
 * @property array $settings
 */
class CorePlugin extends CActiveRecord
{
	public function init () {
		parent::init();

		if(!$this->settings){
			$this->settings = array();
		}
	}

	protected function afterFind () {
		parent::afterFind();
		$this->settings = (array) CJSON::decode($this->settings, 1);
	}

	protected function beforeSave () {
		$this->settings = CJSON::encode($this->settings);
		return parent::beforeSave();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CorePlugin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_plugin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('plugin_name', 'required'),
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('plugin_name, plugin_title', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('plugin_id, plugin_name, plugin_title, is_active, settings', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'plugin_id' => 'Plugin',
			'plugin_name' => 'Plugin Name',
			'plugin_title' => 'Plugin Title',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('plugin_id',$this->plugin_id,true);
		$criteria->compare('plugin_name',$this->plugin_name,true);
		$criteria->compare('plugin_title',$this->plugin_title,true);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function afterSave(){
		$this->removeOldCache();

		// Revert the effect of beforeSave() so we still have a useful
		// $model->settings after $model->save() is called
		$this->settings = (array) CJSON::decode($this->settings, 1);

		parent::afterSave();
	}
	public function afterDelete(){
		$this->removeOldCache();
		parent::afterDelete();
	}

	/**
	 * Remove old (pre-Yii code) cache if exists
	 */
	private function removeOldCache(){
		if(is_file(Docebo::filesPathAbs().DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'dcache_chacing_plugin_files.txt')){
			unlink(Docebo::filesPathAbs().DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'dcache_chacing_plugin_files.txt');
		}
	}
}