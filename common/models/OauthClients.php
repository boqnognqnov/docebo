<?php

/**
 * This is the model class for table "oauth_clients".
 *
 * The followings are the available columns in table 'oauth_clients':
 * @property string $client_id
 * @property string $client_secret
 * @property string $redirect_uri
 * @property string $grant_types
 * @property string $scope
 * @property string $user_id
 * @property string $app_name
 * @property string $app_description
 * @property string $app_icon
 * @property integer $enabled
 * @property integer $hidden
 */
class OauthClients extends CActiveRecord
{
    //Used by swagger in the api/docs
    const SWAGGER_CLIENT_ID = 'swagger2_api';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oauth_clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, client_secret, redirect_uri, app_name, app_description', 'required'),
			array('enabled, hidden', 'numerical', 'integerOnly'=>true),
			array('client_id, client_secret, grant_types, user_id', 'length', 'max'=>80),
			array('redirect_uri', 'length', 'max'=>2000),
			array('redirect_uri', 'validateRedirectUri'),
			array('scope', 'length', 'max'=>100),
			array('app_name, app_icon', 'length', 'max'=>255),
			array('app_description', 'safe'),
			array('client_id, client_secret, redirect_uri, grant_types, scope, user_id, app_name, app_description, app_icon, enabled, hidden', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'client_id' => Yii::t('app', 'Client id'),
			'client_secret' => Yii::t('app', 'Client Secret'),
			'redirect_uri' => Yii::t('app', 'Redirect Uri'),
			'grant_types' => Yii::t('app', 'Grant Types'),
			'scope' => Yii::t('app', 'Scope'),
			'user_id' => Yii::t('standard', '_USER'),
			'app_name' => Yii::t('app', 'App Name'),
			'app_description' => Yii::t('app', 'App Description'),
			'app_icon' => Yii::t('app', 'App Icon'),
			'enabled' => Yii::t('configuration', 'Enabled'),
			'hidden' => 'Hidden',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('client_secret',$this->client_secret,true);
		$criteria->compare('redirect_uri',$this->redirect_uri,true);
		$criteria->compare('grant_types',$this->grant_types,true);
		$criteria->compare('scope',$this->scope,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('app_name',$this->app_name,true);
		$criteria->compare('app_description',$this->app_description,true);
		$criteria->compare('app_icon',$this->app_icon,true);
		$criteria->compare('enabled',$this->enabled);
		$criteria->compare('hidden',$this->hidden);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OauthClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get available sso clients
	 *
	 * Using a standard class object: filterObject which passes various filtering options, like:
	 * search_input string to search among the
	 *
	 * Returns a rich array of data about sso clients
	 *
	 * @param boolean|integer $idCourse
	 * @param boolean|stdClass $filterObject
	 * @param boolean|integer  $pageSize
	 *
	 * @return CSqlDataProvider
	 */
	public static function sqlDataProvider(stdClass $filterObject = null, $pageSize = false) {
		$idCourse = false;

		// Start building SQL parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand()
			->from(OauthClients::model()->tableName() . ' oa');

		// Additional filtering
		if (get_class($filterObject) == 'stdClass') {
			// Filter by a string (like coach name, course name, etc)
			if ($filterObject->search_input) {
				$params[':search'] 	= '%' . $filterObject->search_input . '%';
				$cond = "CONCAT(COALESCE(oa.client_id,''), ' ', COALESCE(oa.app_name,''), ' ', COALESCE(oa.app_description,'')) LIKE :search";
				$commandBase->andWhere($cond);
			}
		}

		$commandBase->andWhere('(hidden IS NULL OR hidden = 0)');

		// DATA
		$commandData = clone $commandBase;

		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(oa.client_id) AS C');
		$numRecords = $commandCounter->queryScalar($params);

		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'app_name' => array(
				'asc' 	=> 'oa.app_name',
				'desc'	=> 'oa.app_name DESC',
			),
			'app_desacription' => array(
				'asc' 	=> 'oa.app_desacription',
				'desc'	=> 'oa.app_desacription DESC',
			)
		);
		$sort->defaultOrder = array(
			'app_name'		=>	CSort::SORT_ASC,
		);

		// END
		$pagination = array();
		if ($pageSize) {
			$pagination = array('pageSize' => $pageSize);
		}

		$config = array(
			'totalItemCount'	=> $numRecords,
			'pagination' 		=> $pagination,
			//'keyField'			=> 'key_client_id',
			//'sort' 				=> $sort,
			'params'			=> $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData->gettext(), $config);

		return $dataProvider;
	}

	public function beforeValidate() {
		// Double-check it is not array passed as grant types list; if so convert it to string
		if (is_array($this->grant_types)) {
			$this->grant_types = implode(',', $this->grant_types);
		}

		// Set default value for the grant types if nothing set
		$this->grant_types = ($this->grant_types == '' ? 'authorization_code' : $this->grant_types);
		return parent::beforeValidate();
	}

	public function validateRedirectUri($attribute,$params) {
		//
		if (!preg_match('/^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i', $this->redirect_uri)) {
			$this->addError('redirect_uri', Yii::t('app', 'Please enter valid Redirect URI'));
		}
	}
}
