<?php

/**
 * This is the model class for table "learning_category".
 *
 * The followings are the available columns in table 'learning_category':
 * @property integer $idCategory
 * @property integer $idParent
 * @property integer $lev
 * @property string $path
 * @property string $description
 * @property integer $iLeft
 * @property integer $iRight
 *
 * The followings are the available model relations:
 * @property LearningCourse[] $learningCourses
 */
class LearningCourseCategory_ extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourseCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path, description', 'required'),
			array('idParent, lev, iLeft, iRight', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCategory, idParent, lev, path, description, iLeft, iRight', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courses' => array(self::HAS_MANY, 'LearningCourse', 'idCategory'),
		);
	}

	public function behaviors() {
		return array(
			'nestedSetBehavior' => array(
				'class' => 'common.extensions.NestedSetBehavior.NestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
				//'hasManyRoots' => true,
				//'rootAttribute' => 'is_root',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCategory' => 'Id Category',
			'idParent' => 'Id Parent',
			'lev' => 'Lev',
			'path' => 'Path',
			'description' => 'Description',
			'iLeft' => 'I Left',
			'iRight' => 'I Right',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCategory',$this->idCategory);
		$criteria->compare('idParent',$this->idParent);
		$criteria->compare('lev',$this->lev);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('iLeft',$this->iLeft);
		$criteria->compare('iRight',$this->iRight);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	
	
	/**
	 * Category name, the last child of the 'path':  Cat1/Cat2/Cat3 => Cat3
	 * 
	 * @return string
	 */
	public function getName() {
	    $path = $this->path;
	    
	    $result = '';
	    if ( !empty($path) ) {
	        $elements = explode('/', $path);
	        $result = $elements[count($elements)-1];
	    }
	    
	    return $result;
	     
	}
	
	
}