<?php

/**
 * This is the model class for table "conference_bigbluebutton_meeting".
 *
 * The followings are the available columns in table 'conference_bigbluebutton_meeting':
 * @property integer $id
 * @property integer $id_room
 * @property string $name
 * @property string $meeting_id
 * @property string $attendee_pw
 * @property string $moderator_pw
 * @property string $logout_url
 * @property integer $duration
 */
class ConferenceBigbluebuttonMeeting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceBigbluebuttonMeeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_bigbluebutton_meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_room, name, meeting_id, attendee_pw, moderator_pw', 'required'),
			array('id_room, duration', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('meeting_id', 'length', 'max'=>32),
			array('attendee_pw, moderator_pw', 'length', 'max'=>64),
			array('logout_url', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_room, name, meeting_id, attendee_pw, moderator_pw, logout_url, duration', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'conferenceRoom' => array(self::BELONGS_TO, 'ConferenceRoom', 'id_room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_room' => 'Id Room',
			'name' => 'Name',
			'meeting_id' => 'Meeting Id',
			'attendee_pw' => 'Attendee Password',
			'moderator_pw' => 'Moderator Password',
			'logout_url' => 'Logout Url',
			'duration' => 'Duration',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_room',$this->id_room);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('meeting_id',$this->meeting_id,true);
		$criteria->compare('attendee_pw',$this->attendee_pw,true);
		$criteria->compare('moderator_pw',$this->moderator_pw,true);
		$criteria->compare('logout_url',$this->logout_url,true);
		$criteria->compare('duration',$this->duration);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
}