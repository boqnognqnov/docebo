<?php

/**
 * This is the model class for table "webinar_session_user".
 *
 * The followings are the available columns in table 'webinar_session_user':
 * @property integer $id_session
 * @property integer $id_user
 * @property string $evaluation_text
 * @property string $evaluation_file
 * @property float $evaluation_score
 * @property string $evaluation_date
 * @property integer $evaluation_status
 * @property integer $evaluator_id
 * @property integer $attendance_hours
 * @property integer $waiting
 * @property integer $status
 * @property string $date_subscribed
 * @property string $date_completed
 * @property integer $attended
 *
 * The followings are the available model relations:
 * @property CoreUser $userModel
 * @property WebinarSession $sessionModel
 * @property LearningCourseuser $learningCourseuser
 */
class WebinarSessionUser extends CActiveRecord
{
	public static $SESSION_USER_WAITING_LIST = -2;
	public static $SESSION_USER_CONFIRMED = -1;
	public static $SESSION_USER_SUBSCRIBED = 0;
	public static $SESSION_USER_BEGIN = 1;
	public static $SESSION_USER_END = 2;
	public static $SESSION_USER_SUSPEND = 3;

	const EVALUATION_STATUS_FAILED = -1;
	const EVALUATION_STATUS_PASSED = 1;

	protected $_synchronize = true;
	protected $_cache_courseuser = NULL;
	protected $_level = NULL;
	protected $_sessionModel = NULL;

	protected $oldAttributes; // Stores old attributes on afterFind() so we can compare against them during save
	public $confirm;

	/**
	 * Enable or disable auto-synchronization with learning_courseuser table after saving a new record.
	 * This feature is used to obtaing better performance when saving multiple users to the same session
	 *
	 * @var boolean
	 */
	public function isSynchronized() { return $this->_synchronize; }
	public function enableSynchronization() { $this->_synchronize = true; }
	public function disableSynchronization() { $this->_synchronize = false; }

	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('date_subscribed medium', 'date_completed medium'),
				'dateAttributes' => array('evaluation_date')
			)
		);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_session_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_session, id_user, date_subscribed', 'required'),
			array('id_session, id_user, evaluation_score, evaluation_status, evaluator_id, attendance_hours, waiting, status, attended', 'numerical'),
			array('evaluation_file', 'length', 'max'=>255),
			array('evaluation_text, evaluation_date, date_completed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, id_user, evaluation_text, evaluation_file, evaluation_score, evaluation_date, evaluation_status, evaluator_id, attendance_hours, waiting, status, date_subscribed, date_completed, attended', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userModel' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'sessionModel' => array(self::BELONGS_TO, 'WebinarSession', 'id_session'),
			'learningCourseuser' => array(self::BELONGS_TO, 'LearningCourseuser', array('id_user' => 'idUser'), 'on' => 'learningCourseuser.idCourse = sessionModel.course_id'),
		);
	}

	public function afterFind()
	{
		// Store unchanged/original attributes so we can compare against them during AR save
		$this->oldAttributes = $this->attributes;

		return parent::afterFind();
	}

	public function beforeSave() {
		$sessionModel = WebinarSession::model()->findByPk($this->id_session);
		// if the user is a Power User make some validations for the actions that he can do with enrollments statuses.
		if (Yii::app()->user->getIsPu() && $this->scenario != 'insert' &&  ($sessionModel && LearningCourse::isCourseSelling($sessionModel->course_id))) {
			$statuses_translations = self::getStatusesArray();
			$power_user_set_status_error = false;

			// Conditions that has to be met for the Power User to change the status of the enrolled user
			$conditions = array(
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_END && $this->status != $this->oldAttributes['status']),
					'available_statuses' => array($this->oldAttributes['status'])
				),
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_SUBSCRIBED),
					'available_statuses' => array(self::$SESSION_USER_BEGIN, self::$SESSION_USER_END, self::$SESSION_USER_SUSPEND, self::$SESSION_USER_WAITING_LIST, self::$SESSION_USER_CONFIRMED, $this->oldAttributes['status'])
				),
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_BEGIN),
					'available_statuses' => array(self::$SESSION_USER_END/*, self::$SESSION_USER_SUSPEND*/, $this->oldAttributes['status'])
				)
			);

			// Executing the conditions
			foreach ($conditions as $condition) {
				if ($condition['cond']) {
					if (!in_array($this->status, $condition['available_statuses'])) {
						$power_user_set_status_error = true;
						break;
					}
				}
			}

			// if the conditions are not met then there is an error 'power_user_set_status_error' and return a message to the Power User:
			if ($power_user_set_status_error) {
				throw new CException(Yii::t('course_management', 'Power Users cannot change enrollment status from {status_1} to {status_2}',
					array(
						'{status_1}' => $statuses_translations[$this->oldAttributes['status']],
						'{status_2}' => $statuses_translations[$this->status]
					)
				));
			}
		}

		//manual evaluation sets the date completed as well
		if($this->status == LearningCourseuser::$COURSE_USER_END) {
			if(!$this->date_completed) {
				$this->date_completed = Yii::app()->localtime->getUTCNow();
			}
		}
		//update session status when session is being marked as "passed"
		else if ($this->evaluation_status > 0) {
			$this->status = LearningCourseuser::$COURSE_USER_END;

			if(!$this->date_completed) {
				$this->date_completed = Yii::app()->localtime->getUTCNow();
			}
		}
		else {
			$this->date_completed = '0000-00-00 00:00:00';
		}

		return parent::beforeSave();
	}

	/**
	 * Get courseuser model based on userId and sessionId with some joins...
	 * @return LearningCourseuser
	 */
	public function getCourseuser()
	{
		if (!$this->_cache_courseuser) {
			$this->_cache_courseuser = LearningCourseuser::model()->find(array(
				'join' => 'INNER JOIN ' . WebinarSession::model()->tableName() . ' ws ON ws.course_id = t.idCourse',
				'condition' => 'ws.id_session = :id_session AND t.idUser = :idUser',
				'params' => array(':id_session' => $this->id_session, ':idUser' => $this->id_user),
			));
		}
		return $this->_cache_courseuser;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'id_user' => 'Id User',
			'evaluation_text' => 'Evaluation Text',
			'evaluation_file' => 'Evaluation File',
			'evaluation_score' => 'Evaluation Score',
			'evaluation_date' => 'Evaluation Date',
			'evaluation_status' => 'Evaluation Status',
			'evaluator_id' => 'Evaluator',
			'attendance_hours' => 'Attendance Hours',
			'waiting' => 'Waiting',
			'status' => 'Status',
			'date_subscribed' => 'Date Subscribed',
			'date_completed' => 'Date Completed',
			'attended' => 'Attended',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('evaluation_text',$this->evaluation_text,true);
		$criteria->compare('evaluation_file',$this->evaluation_file,true);
		$criteria->compare('evaluation_score',$this->evaluation_score);
		$criteria->compare('evaluation_date',$this->evaluation_date,true);
		$criteria->compare('evaluation_status',$this->evaluation_status);
		$criteria->compare('evaluator_id',$this->evaluator_id);
		$criteria->compare('attendance_hours',$this->attendance_hours);
		$criteria->compare('waiting',$this->waiting);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_subscribed',$this->date_subscribed,true);
		$criteria->compare('date_completed',$this->date_completed,true);
		$criteria->compare('attended',$this->attended);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarSessionUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterSave() {

		if ($this->isSynchronized()) {

			//after creating the enrollment session record, create the course one
			if ($this->isNewRecord) {
				//$session = $this->sessionModel;
				$session = WebinarSession::model()->findByPk($this->id_session);
				$courseEnroll = new LearningCourseuser();
				$level = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
				$enrolled = $courseEnroll->subscribeUser($this->id_user, Yii::app()->user->id, $session->course_id, 0, $level);
				if (!$enrolled) {
					switch ($courseEnroll->errorMessage) {
						case LearningCourseuser::$ALREADY_SUBSCRIBED: break; //this is ok for us, go on
						default: {
						throw new CException('Error while enrollind user to course (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
						} break;
					}
				}
			}

			if($this->status == LearningCourseuser::$COURSE_USER_END)
			{
				$session = WebinarSession::model()->findByPk($this->id_session);
				$model = LearningCourseuser::model()->findByAttributes(array('idUser' => $this->id_user, 'idCourse' => $session->course_id));
				if($model->status != LearningCourseuser::$COURSE_USER_END)
				{
					$model->status = LearningCourseuser::$COURSE_USER_END;
					$model->date_complete = Yii::app()->localtime->toLocalDateTime();
					$model->save();
		}
			}
		}

		if($this->getIsNewRecord()) {
			$this->sessionModel->course->updateMaxSubscriptionQuota();
		}

		$courseEnroll = $this->getCourseuser();
		if($courseEnroll->waiting == 1 && $this->status != self::$SESSION_USER_WAITING_LIST) {
			$courseEnroll->waiting = 0;
			$courseEnroll->save();
		}

		parent::afterSave();
	}

	/**
	 * The method is called after the course purchase has been confirmed.
	 * It will enroll a user to a classroom type course session with the waiting status.
	 */
	public function activateSession($idUser, $idSession) {

		// find the session
		$cus = WebinarSessionUser::model()->findByAttributes(array(
			'id_session' => $idSession,
			'id_user' => $idUser
		));
		if (!$cus)
			return false;

		if($cus->status == self::$SESSION_USER_WAITING_LIST)
			$cus->status = self::$SESSION_USER_SUBSCRIBED;

		$result = $cus->save();

		if ($result) {
		    Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED, new DEvent($this, array(
		        'enrollment' => $cus,
		    )));
		}


		return $result;
	}

	/**
	 * Returns the array of session user statuses
	 * @return array
	 */
	public static function getStatusesArray() {
		return array(
			self::$SESSION_USER_WAITING_LIST => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_WAITING_LIST)),
			self::$SESSION_USER_CONFIRMED => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_CONFIRMED)),
			self::$SESSION_USER_SUBSCRIBED => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_SUBSCRIBED)),
			self::$SESSION_USER_BEGIN => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_BEGIN)),
			self::$SESSION_USER_END => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_END)),
			self::$SESSION_USER_SUSPEND => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_SUSPEND))
		);
	}

	/**
	 * Return the specific level language constant string
	 * @param $status
	 * @return string
	 */
	public static function getStatusLangLabel($status) {
		$languageVariable = '';
		switch ($status) {
			case self::$SESSION_USER_WAITING_LIST:
				$languageVariable = '_WAITING_USERS';
				break;
			case self::$SESSION_USER_CONFIRMED:
				$languageVariable = '_USER_STATUS_CONFIRMED';
				break;
			case self::$SESSION_USER_SUBSCRIBED:
				$languageVariable = '_USER_STATUS_SUBS';
				break;
			case self::$SESSION_USER_BEGIN:
				$languageVariable = '_USER_STATUS_BEGIN';
				break;
			case self::$SESSION_USER_END:
				$languageVariable = '_USER_STATUS_END';
				break;
			case self::$SESSION_USER_SUSPEND:
				$languageVariable = '_USER_STATUS_SUSPEND';
				break;
		}
		return $languageVariable;
	}

	public static function getStatusColorLabel($sessionId, $userId)
	{
		$model = self::model()->findByAttributes(array(
			'id_user' => $userId,
			'id_session' => $sessionId,
		));
		if ($model)
		{
			switch ($model->status)
			{
				case self::$SESSION_USER_END:
					$label = 'label-light-green';
					break;
				case self::$SESSION_USER_SUBSCRIBED:
					$label = 'label-black';
					break;
				default:
					$label = '';
			}
		}
		else
			$label = '';

		return '<span class="webinar-label '.$label.'">'.strtoupper(Yii::t("subscribe", self::getStatusLangLabel(($model ? $model->status : self::$SESSION_USER_BEGIN)))).'</span>';
	}

	/**
	 * Returns true if user is subscribed to a session
	 * @param $userId
	 * @param $sessionId
	 * @return bool
	 */
	public static function isSubscribed ($userId, $sessionId) {
		$count = self::model()->countByAttributes(array("id_user" => $userId, "id_session" => $sessionId));
		return $count > 0 ? TRUE : FALSE;
	}

	/**
	 * Enrolls a user to a course session
	 * @param $userId
	 * @param $sessionId
	 * @param $status
	 * @return bool|WebinarSessionUser
	 */
	public static function enrollUser($userId, $sessionId, $status = false, $is_cart_presubscription = false) {
		// Check if user is already subscribed
		if (WebinarSessionUser::isSubscribed($userId, $sessionId)) {
			Yii::log("User ($userId) is already subscribed to this session ($sessionId)", 'warning', __CLASS__);
			return false;
		}

		$model = new WebinarSessionUser();
		$model->id_user = $userId;
		$model->id_session = $sessionId;
		$model->date_subscribed = Yii::app()->localtime->toLocalDateTime();
		$model->status = ($status===false) ? self::$SESSION_USER_SUBSCRIBED : $status;

		if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
			$model->status = self::$SESSION_USER_WAITING_LIST;

        $saveResult = $model->save(false);

        if ($saveResult && !$is_cart_presubscription) {
        	$event = new DEvent(new stdClass(), array(
        		'enrollment'	=> $model,
				'session_id' => $model->id_session,
				'user' => $userId
        	));
			if ($model->status == self::$SESSION_USER_WAITING_LIST){
				Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_WAITING_FOR_APPROVAL, $event);
			}else {
        		Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION, $event);
        	}
        }

		return $saveResult ? $model : false;
	}

	/**
	* Unsubscribes user from session
	* @param $userId
	* @param $sessionId
	* @return int
	*/
	public static function unsubscribeUser($userId, $sessionId) {
		return self::model()->deleteByPk(array(
				'id_user' => $userId,
				'id_session' => $sessionId
		));
	}

	public function getLevel() {
		if ($this->isNewRecord) {
			return $this->_level;
		}
		$cu = $this->courseuser;
		return (!empty($cu) ? $cu->level : false);
	}

	public function setLevel($newLevel) {

		$levels = LearningCourseuser::model()->getLevelsArray(array(), array(LearningCourseuser::$USER_SUBSCR_LEVEL_COACH));
		if (!isset($levels[$newLevel])) { throw new CException('Invalid specified level: '.$newLevel); }

		if ($this->isNewRecord) {

			$this->_level = $newLevel;

		} else {

			//record exists in DB: update learning_courseuser level column
			$session = $this->sessionModel;
			if (!$this->courseuser) {
				throw new CException('Error: unable to retrieve course enrollment info (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
			}

			$this->courseuser->level = $newLevel;
			if (!$this->courseuser->save()) {
				throw new CException('Error while saving new user level in  the course (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
			}
		}

		return true;
	}


	/**
	 * To be used in webinar/session/Select
	 * @param $userId
	 * @param $courseId
	 * @return CActiveDataProvider
	 */
	public function sessionSelectDataProvider($userId, $courseId) {
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'sessionModel' => array(
				'alias' => 'cs',
				'condition' => 'cs.course_id = :course_id'
			)
		);

		$criteria->addInCondition('id_user', array($userId));

		$criteria->params[':course_id'] = $courseId;

		$config = array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'cs.date_begin ASC'
			)
		);

		return new CActiveDataProvider($this, $config);
	}

	/**
	 * Returns an array containing the number and the percentage of the students who passed the course
	 *
	 * @param $courseId int
	 * @return array
	 */
	public function getPassStats($courseId)
	{

		$sessionIds = array();
		if(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') && (!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign'))) {
			$sessions = WebinarSession::model()->findAllByAttributes(array('course_id' => $courseId, 'created_by' => Yii::app()->user->id));
		} else {
			$sessions = WebinarSession::model()->findAllByAttributes(array('course_id' => $courseId));
		}
		foreach($sessions as $session)
			$sessionIds[] = $session->id_session;

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id_session', $sessionIds);
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---
                  $pUserRights = Yii::app()->user->checkPURights($courseId);
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $courseId, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($courseId);
			if(!empty($instructorUsers))
				$criteria->addInCondition('t.id_user', $instructorUsers);
		}
		//---
		$enrolledAllSessions = WebinarSessionUser::model()->with('sessionModel', 'learningCourseuser')->findAll($criteria);
		$enrolled = CHtml::listData($enrolledAllSessions, 'id_user', 'id_user');
		$totalEnrolled = count($enrolled);

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id_session', $sessionIds);
		$criteria->addCondition('t.evaluation_status = :evaluation_status');
		$criteria->params[':evaluation_status'] = self::EVALUATION_STATUS_PASSED;
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//---
		$passedUsers = self::model()->with('sessionModel', 'learningCourseuser')->findAll($criteria);
		$passed = CHtml::listData($passedUsers, 'id_user', 'id_user');
		$countPassed = count($passed);

		if (0 == $totalEnrolled)
			$percentPassed = 0;
		else
			$percentPassed = number_format(doubleval($countPassed / $totalEnrolled) * 100, 1);

		return array(
			'total' => $totalEnrolled,
			'count' => $countPassed,
			'percent' => $percentPassed
		);
	}

	public function dataProviderUserStatistics($idSession) {
		$criteria = new CDbCriteria;
		$sessionModel = WebinarSession::model()->findByPk($idSession);
                  $pUserRights = Yii::app()->user->checkPURights($sessionModel->course_id);

		$criteria->with = array(
			'sessionModel',
			'userModel',
			'learningCourseuser'
		);

		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

		if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$managedUserIds = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $managedUserIds);
		}

		$criteria->addCondition('t.id_session = :id_session');
		$criteria->params[':id_session'] = $idSession;

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		//$config['sort']['defaultOrder'] = 'id asc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


		return new CActiveDataProvider($this, $config);
	}

	/**
	 * Used to display the session statistics for all users enrolled in all sessions of a classroom course
	 *
	 * @return CActiveDataProvider
	 */
	public function dataProviderCourseStatistics($idCourse)
	{

		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
			'sessionModel',
			'userModel',
			'learningCourseuser'
		);

		$criteria->addCondition('learningCourseuser.level = ' . LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

		$criteria->addCondition('sessionModel.course_id = :course_id');
		$criteria->params[':course_id'] = $idCourse;
                  $pUserRights = Yii::app()->user->checkPURights($idCourse);
		if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('userModel.idst', $puManagedUsers);
		}

		if(Yii::app()->user->getIsPu()) {
			if(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add')) {
				if(!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign')) {
					$criteria->addCOndition('sessionModel.created_by = :createdBy');
					$criteria->params[':createdBy'] = Yii::app()->user->id;
				}
			}
		}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($idCourse);
			if(!empty($instructorUsers))
				$criteria->addInCondition('userModel.idst', $instructorUsers);
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 'sessionModel.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'userModel.userid asc, sessionModel.name asc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function renderEvaluationStatus() {
		switch ($this->evaluation_status) {
			case self::EVALUATION_STATUS_PASSED:
				return Yii::t('standard', 'passed');
			case self::EVALUATION_STATUS_FAILED:
				return Yii::t('standard', 'failed');
			default:
				return Yii::t('standard', '_USER_STATUS_BEGIN');
		}
	}

	public function renderSessionScore()
	{
        $output = '';
        $hasScore = false;
        $score = 0;
        $maxScore = 0;

		if ($this->sessionModel->evaluation_type == WebinarSession::EVAL_TYPE_ONLINE_TEST) {
            //get the score of the last completed test
            $testIds = Yii::app()->getDb()->createCommand()
                ->select('idOrg')
                ->from(LearningOrganization::model()->tableName())
                ->where('idCourse=:idCourse', array(':idCourse' => $this->sessionModel->course->idCourse))
                ->andWhere('objectType = :objectType', array('objectType' => LearningOrganization::OBJECT_TYPE_TEST))
                ->queryColumn();

            $criteria = new CDbCriteria();
            $criteria->addCondition('(idUser=:idUser)');
            $criteria->params = array(':idUser' => $this->id_user);
            $criteria->addInCondition('idReference', $testIds);
            $criteria->order = 'date_attempt DESC';
            $criteria->with = 'test';
            $tracking = LearningTesttrack::model()->find($criteria);
            if ($tracking) {
                $hasScore = true;
                $testModel = $tracking->test;
                $score = ($tracking->score + $tracking->bonus_score);

                if ($testModel) {
                    $maxScore = $testModel->getMaxScore($this->id_user, false, false);
                }
            }

            $output .= $hasScore ? number_format($score, 2) . '/' . $maxScore : '-';
		} else {
			//show session standard evaluation score
			$output .= intval($this->evaluation_score) . "/" . intval($this->sessionModel->score_base);
		}
		return $output;
	}

	public function dataProviderClassroomCoursesActivity($idUser) {
		$criteria = new CDbCriteria;

		$criteria->with = array(
			'sessionModel' => array(
				'joinType' => 'INNER JOIN',
				'with' => array(
					'course' => array(
						'alias' => 'course'
					)
				)
			)
		);

		$curentUserId = Yii::app()->user->getId();
		if (Yii::app()->user->getIsPu() && (!isset($idUser) || $curentUserId != $idUser)) {
			$puManagedCourses = CoreUserPuCourse::model()->getList(Yii::app()->user->getId());
			$criteria->addInCondition('course.idCourse', $puManagedCourses);
		}

		$criteria->compare('t.id_user', $idUser);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'date_subscribed desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


		return new CActiveDataProvider($this, $config);
	}

	public function renderCourseLink($name)
	{
		$learningCourseuser = LearningCourseuser::model()->findByPk(array(
			'idCourse' => (int)$this->sessionModel->course->idCourse,
			'idUser' => (int)$this->id_user,
			'edition_id' => 0));

		if(!$learningCourseuser)
			return $name;

		$link = '<a href="../lms/index.php?r=player&course_id='.$this->sessionModel->course->idCourse.'">'.$name.'</a>';
		$canEnterCourse = $learningCourseuser->canEnterCourse();

		return ($canEnterCourse['can'] ? $link : $name);
	}



	/**
	 * Set an users evaluation, with all related info (score, text, etc.)
	 *
	 * @param int $status if the user has passed the class session or not. default value = 0 = in progress
	 * @param float $score score obtained by the user
	 * @param int $evaluator IDST of the instructor evaluating the user
	 * @param string $text evaluation notes by instructor
	 * @param string $file additional file by instructor (just the file name, assume it has already been placed in storage folder)
	 * @return boolean operation successful or not
	 * @throws CException
	 */
	public function setEvaluation($status, $score, $evaluator, $text = '', $file = '') {

		//retrieve related classroom session AR
		$sessionModel = $this->sessionModel;

		//check score value validity
		if ($score < 0 || ($score > $sessionModel->score_base && !($sessionModel->evaluation_type == WebinarSession::EVAL_TYPE_ONLINE_TEST))) { throw new CException('Invalid score value'); }

		//set values
		$status = intval($status);
		if (!in_array($status, array(
			self::EVALUATION_STATUS_PASSED,
			self::EVALUATION_STATUS_FAILED
		))) {
			$status = 0;
		}
		$this->evaluation_status = $status;
		$this->evaluation_score = $score;
		$this->evaluator_id = $evaluator;
		$this->evaluation_date = Yii::app()->localtime->toLocalDateTime();

		//validate text
		if ($text) {
			$trimmedText = Yii::app()->htmlpurifier->purify(trim($text));
			if ($trimmedText != "") { $this->evaluation_text = $text; }
		}

		//validate file
		$removeOldEvaluationFile = false;
		$oldEvaluationFile = $this->evaluation_file;
		if (empty($file)) {
			//if we had an old evaluation file, remove it
			if (!empty($this->evaluation_file)) {
				$removeOldEvaluationFile = true;
			}
			//no evaluation file
			$this->evaluation_file = NULL;
		} else {
			//check if we are setting a new evaluation file
			if (!empty($this->evaluation_file) && $this->evaluation_file != $file) {
				//an old evaluation file which is different from new one is present: remove it
				$removeOldEvaluationFile = true;
			}
			$this->evaluation_file = $file;
		}

		//physical DB operation
		$rs = $this->save();

		//do physical file removing, if needed (only if DB operation was successful)
		if ($rs && $removeOldEvaluationFile && !empty($oldEvaluationFile)) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
			//$rs = $storageManager->remove($oldEvaluationFile);
			$rs = $storageManager->remove($this->getOriginalFileName($oldEvaluationFile));
		}

		// Trigger event to let plugins execute their custom evaluation options
		$event = new DEvent($this, array (
			'courseuserSessionModel' => &$this,
			'idUser' => $this->id_user,
			'idSession' => $this->id_session
		));
		Yii::app()->event->raise('OnAfterWebinarUserSetEvaluation', $event);
		// end event

		return $rs;
	}


	/**
	 * Clear user's evaluation info
	 *
	 * @return boolean
	 */
	public function clearEvaluation() {

		$this->evaluation_score = NULL;
		$this->evaluation_date = NULL;
		$this->evaluation_text = NULL;
		$this->evaluator_id = NULL;
		$this->evaluation_status = NULL;

		$fileName = $this->evaluation_file; //postpone file deletion, first ensure  that the DB deletion is ok
		$this->evaluation_file = NULL;

		$rs = $this->save();
		if ($rs) {
			//database information has been cleared, now delete physical evaluation file, if present
			if (!empty($fileName)) {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
				$rs = $storageManager->remove($fileName);
			}

			// Trigger event to let plugins execute their custom evaluation options
			$event = new DEvent($this, array ('idUser' => $this->id_user, 'idSession' => $this->id_session));
			Yii::app()->event->raise('OnAfterClassroomUserClearEvaluation', $event);
			// end event
		}

		return $rs;
	}

	/**
	 * Given an evaluation record, remove unique prefix from evaluation file name
	 * and return the original file name. Since evaluation files are optional, the
	 * field "evaluation_file" may be empty: in this case just return FALSE.
	 *
	 * @return string|boolean the original file name (or false, if no file has been uploaded)
	 */
	public function getOriginalFileName() {

		//no file has been uploaded for this evaluation
		if (empty($this->evaluation_file)) return false;

		//remove unique prefix from file name and return the original file name
		$arr = explode(".", $this->evaluation_file);
		array_shift($arr);
		return implode(".", $arr);
	}


	//file management utilities

	/**
	 * The storage directory for evaluation files uploaded by evaluators
	 *
	 * @return string the files path
	 */
	public static function getFilesPath() {
		return 'doceboLms'.DIRECTORY_SEPARATOR.'webinar_evaluation';
	}


	/**
	 * Given a file name, render it unique in order to store in evaluation files
	 * directory, since uploaded files may have the same name.
	 *
	 * @param string $originalFileName the original file name to be rendered unique
	 * @return string the new file name
	 */
	public function createUniqueFileName($originalFileName) {
		//create an unique ID based on current time and record info
		$uniquePrefix = $this->id_session.'_'.$this->id_user.'_'.time().'_'.mt_rand(0, 99999999);

		//just put the unique ID as a prefix in the file name
		return $uniquePrefix.'.'.$originalFileName;
	}

	/**
	 * Returns true or false if there is an evaluation file
	 * @return bool
	 */
	public function hasEvaluationFile() {
		return !empty($this->evaluation_file);
	}

	public function afterDelete() {

	if($this->getScenario() != 'selfUnEnroll') {
		//unenroll the user from the course if he is not subscribed to any session
		$courseId = $this->sessionModel->course_id;
		$userId = $this->id_user;

		$subscribedToSession = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from('webinar_session_user cus')
			->join('webinar_session cs', 'cs.id_session = cus.id_session AND cs.course_id = :idCourse', array(':idCourse' => $courseId))
			->where('cus.id_user = :idUser', array(':idUser' => $userId))
			->queryScalar();

		if(!$subscribedToSession)
		{
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $userId, 'idCourse' => $courseId));
			if($courseUserModel && ($courseUserModel->waiting !=1)) {
				$courseUserModel->unsubscribeUser( $userId, $courseId );

				if(CoreUserPU::isPUAndSeatManager() && $courseUserModel->course->selling){
					CoreUserPU::modifyAvailableSeats($courseId, 1);
				}
			}

		}
	}
		parent::afterDelete();
	}




	/**
	 * Check if an user has played any learning object present in the course
	 * @param int $idUser IDST of the user to be checked (false/empty or negative value to use current logged user)
	 * @return bool
	 */
	public function userHasAlreadyPlayedSomeCourseLOs($idUser = false) {
		//check input, user can be passed or it can fallback to current logged user
		if (empty($idUser) || $idUser <= 0) {
			$idUser = Yii::app()->user->id;
		}

		//prepare output value
		$output = false;

		//first check if our course has any learning objects
		$countLOs = LearningOrganization::model()->count("idCourse = :id_course AND objectType <> ''", array(':id_course' => $this->course_id));
		if ($countLOs > 0) {
			//the course has some LOs, check if the user has started/completed any of them
			$t1 = LearningCommontrack::model()->tableName();
			$t2 = LearningOrganization::model()->tableName();
			$query = "SELECT COUNT(*) FROM $t1 t1 JOIN $t2 t2 "
				." ON (t1.idReference = t2.idOrg AND t2.idCourse = :id_course AND t2.objectType <> '' AND t1.status <> :ct_status AND t1.idUser = :id_user)";
			$params = array(
				':id_user' => $idUser,
				':id_course' => $this->course_id,
				':ct_status' => LearningCommontrack::STATUS_AB_INITIO
			);
			$countPlayedLOs = Yii::app()->db->createCommand($query)->queryScalar($params);
			if ($countPlayedLOs > 0) { $output = true; }
		}

		return $output;
	}

	public function getWaitingUsers()
	{
		$session = WebinarSession::model()->findByPk($this->id_session);
		$enrolledUsers = $session->countEnrollments(false, true);

		$freeToEnroll = $session->max_enroll - $enrolledUsers;
		$students = false;

		if ($freeToEnroll > 0) {
			$criteria = new CDbCriteria();

			$criteria->addCondition('t.id_session = :id_session');
			$criteria->params[':id_session'] = $this->id_session;

			$criteria->addCondition('learningCourseuser.level = :level');
			$criteria->params[':level'] = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;

			$criteria->addCondition('t.status = :status');
			$criteria->params[':status'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

			$criteria->order = 'date_subscribed ASC';
			$criteria->limit = $freeToEnroll;

			$students = WebinarSessionUser::model()->with(array(
				'sessionModel',
				'learningCourseuser'
			))->findAll($criteria);
		}
		return $students;
	}
}
