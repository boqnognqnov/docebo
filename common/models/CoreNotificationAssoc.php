<?php

/**
 * This is the model class for table "core_notification_assoc".
 * Holds information about courses/plans association with a notification
 *
 * The followings are the available columns in table 'core_notification_assoc':
 * @property integer $id Notification ID
 * @property integer $idItem Course or Plan ID
 * @property integer $type Item type: 1=Course, 2=Learning Plan
 * 
 * @property CoreNotification $notification
 */
class CoreNotificationAssoc extends CActiveRecord
{
	
	/**
	 * Possible values for item type
	 * @var integer
	 */
	const NOTIF_ASSOC_COURSE 	= 1;
	const NOTIF_ASSOC_PLAN 		= 2;  
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_notification_assoc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idItem, type', 'required'),
			array('id, idItem, type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idItem, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notification' 	=> array(self::BELONGS_TO, 'CoreNotification', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idItem' => 'Item ID',
			'type' => 'Item type'	
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idItem',$this->idItem);
		$criteria->compare('type',$this->type);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreNotificationAssoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
