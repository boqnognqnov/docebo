<?php

/**
 * This is the model class for table "app7020_tooltips".
 *
 * The followings are the available columns in table 'app7020_tooltips':
 * @property integer $id
 * @property integer $idContent id of assigned Content
 * @property integer $idExpert Id of Expert user
 * @property string $dateCreated When is created the tooltip
 * @property string $dateUpdated When is updated for last time the tooltip
 * @property integer $tooltipRow Display tooltip into timeline/flowplayer row
 * @property integer $tooltipStyle Choose light or dark style. Light is 1, 2 is drak
 * @property string $durationFrom Begin Cue seccond for show the anotation message
 * @property string $durationTo End Cue seccond for hide the anotation message
 * @property string text Content of tooltips
 *
 * @property App7020ContentReviews $app7020ContentReviews if tooltip is created by PeerReview Item
 * @property App7020Content $ontent The content assigned for tooltip
 * @property CoreUser $expert The expert created tooltip
 */
class App7020Tooltips extends CActiveRecord {

	static $_themes = array(
		1 => 'app7020-tooltip-light',
		0 => 'app7020-tooltip-dark',
	);
	/**
	 * Store How many rows have in timeline
	 * Each one row is element from to array 
	 * @var array 
	 */
	static $_allowed_rows = array(1, 2, 3);

	/**
	 * Add current class 
	 * @var string 
	 */
	public $themeClass = false;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_tooltips';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array(
			array('idContent, idExpert,  durationFrom, durationTo', 'required'),
			array('idContent, idExpert,tooltipRow, durationFrom, durationTo', 'numerical', 'integerOnly' => true),
			array('duration', 'valideTooltipDuration', 'on' => 'insert'),
			array('duration', 'valideTooltipDuration', 'on' => 'update'),
			array('durationFrom, durationTo', 'length', 'max' => 11),
			array('id, idContent, idExpert, dateCreated, dateUpdated, tooltipRow, tooltipStyle, durationFrom, durationTo', 'safe', 'on' => 'search'),
			array('text', 'validateTooltipText', 'on' => 'insert, update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
		return array(
			'app7020ContentReviews' => array(self::HAS_ONE, 'App7020ContentReviews', 'idTooltip'),
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
			'expert' => array(self::BELONGS_TO, 'CoreUser', 'idExpert'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id of app7020_content table. This field assign tooltop to any content',
			'idExpert' => 'relation to core_users. This filed is responsible for author of tooltip',
			'dateCreated' => 'Date Created',
			'dateUpdated' => 'Date Updated',
			'tooltipRow' => 'Tooltip Row',
			'tooltipStyle' => 'Tooltip Style',
			'durationFrom' => 'When Flow player must CUE start anotation',
			'durationTo' => 'When Flow player must CUE End anotation',
			'text' => 'Content of tooltip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idExpert', $this->idExpert);
		$criteria->compare('dateCreated', $this->dateCreated, true);
		$criteria->compare('dateUpdated', $this->dateUpdated, true);
		$criteria->compare('tooltipRow', $this->tooltipRow);
		$criteria->compare('tooltipStyle', $this->tooltipStyle);
		$criteria->compare('durationFrom', $this->durationFrom, true);
		$criteria->compare('durationTo', $this->durationTo, true);
		$criteria->compare('text', $this->text, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020Tooltips the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	function afterFind() {
		parent::afterFind();
		//assign class after find 
		if (isset(self::$_themes[$this->tooltipStyle])) {
			$this->themeClass = self::$_themes[$this->tooltipStyle];
		}
	}

	function beforeSave() {
		if (!$this->isNewRecord) {
			$this->dateUpdated = date('Y-m-d H:i:s', time());
		}
		return parent::beforeSave();
	}

//	function getBlankPo
	static function getBlankPosition($from, $to, $contentId) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select = '* FROM (			
			SELECT DISTINCT at1.`tooltipRow`, rows.c 
				FROM  (
					SELECT 1 as tooltipRow
					UNION
					SELECT 2 as tooltipRow
					UNION
					SELECT 3 as tooltipRow
				)
				as at1 
				LEFT JOIN 
					( 
						SELECT count(*) as c, atTemp.tooltipRow 

						FROM app7020_tooltips as atTemp 
						WHERE 
                        atTemp.idContent = :idContent
                        and :durationFrom <= atTemp.`durationTo` 
						AND :durationTo >= atTemp.`durationFrom` 
						group BY atTemp.tooltipRow 
						HAVING c IS NOT NULL
					)
				rows 
				ON rows.`tooltipRow` = at1.tooltipRow
			) as tmpTable  WHERE c is NULL';
		$dbCommand->bindParam('durationFrom', $from);
		$dbCommand->bindParam('durationTo', $to);
		$dbCommand->bindParam('idContent', $contentId);

		$result = $dbCommand->query();
		foreach ($result as $row) {
			$rowTooltip = (int) $row['tooltipRow'];
			if (in_array($rowTooltip, self::$_allowed_rows) && empty($row['countTooltips'])) {
				return $rowTooltip;
			}
		}
		return NULL;
	}

	/**
	 * Return the number of blank positions at this moment for this range for current tooltip
	 * @return Int
	 */
	function getNumFreePositions() {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select = ' 
 				count(*) as freePos 
				FROM(
				SELECT DISTINCT at1.`tooltipRow`, rows.c 
				FROM  (
					SELECT 1 as tooltipRow
					UNION
					SELECT 2 as tooltipRow
					UNION
					SELECT 3 as tooltipRow
				)
				as at1 
				LEFT JOIN 
					( 
						SELECT count(*) as c, atTemp.tooltipRow 
						FROM app7020_tooltips as atTemp 
						WHERE 
                        atTemp.idContent = :idContent
                        and :durationFrom <= atTemp.`durationTo` 
						AND :durationTo >= atTemp.`durationFrom` 
						group BY atTemp.tooltipRow 
						HAVING c IS NOT NULL
					)
				rows 
				ON rows.`tooltipRow` = at1.tooltipRow
			) AS tmp WHERE tmp.c IS NULL';

		$dbCommand->bindParam('durationFrom', $this->durationFrom);
		$dbCommand->bindParam('durationTo', $this->durationTo);
		$dbCommand->bindParam('idContent', $this->idContent);
		return (int) $dbCommand->queryScalar();
	}
	
	/**
	 * Gets number of tooltips within duration
	 * @param Int $from
	 * @param Int $to
	 * @param Int $idContent
	 * @return boolean
	 */
	public static function getPositionsInRange($from, $to, $idContent, $excludeID){
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select = 'COUNT(`id`) as num FROM `app7020_tooltips` WHERE `durationFrom` <= :to AND `durationTo` >= :from AND `idContent` = :idContent AND `id` != :excludeID Group by tooltipRow';
		$dbCommand->bindParam('to', $to);
		$dbCommand->bindParam('from', $from);
		$dbCommand->bindParam('idContent', $idContent);
		$dbCommand->bindParam('excludeID', $excludeID);
		return $dbCommand->queryAll();
	}

	/**
	 * Has Access to write tooltip into db per new duration
	 * @param mixed $attribute
	 * @param array $params
	 * @return boolean
	 */
	public function valideTooltipDuration($attribute, $params) {
		$id = 0;
		if(!empty($this->id)){
			$id = $this->id;
		}
		$count = self::getPositionsInRange($this->durationFrom, $this->durationTo, $this->idContent, $id);
		if(count($count) >= 3){
			$this->addError('Duration', 'Only 3 tooltips can overlap at the same time');
			return false;
		}else{
			return true;
		}
		if ((bool) $this->getNumFreePositions()) {
			$this->tooltipRow = self::getBlankPosition($this->durationFrom, $this->durationTo, $this->idContent);
			return true;
		} else {
			$this->addError('Duration', 'Invalid range for this asset');
			return false;
		}
	}

	static function generateQuepoints($idContent, $incAssetView = false) {
		$tooltips = App7020Tooltips::model()->findAll('idContent = :idContent', array('idContent' => (int) $idContent));
		$cuepoints = array();
		foreach ($tooltips as $tooltip) {
			$avatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $tooltip->idExpert), true);
			$cuepoints[] = array(
				'time' => (int)$tooltip->durationFrom,
				'type' => 'setTooltip',
				'action' => 'show',
				'hash' => md5($tooltip->id),
				'tooltipId' => $tooltip->id,
				'avatar' => $avatar,
				'text' => strip_tags($tooltip->text, '<p><strong><em><u><br>'),
				'style' => self::$_themes[$tooltip->tooltipStyle]
			);
			$cuepoints[] = array(
				'time' => (int)$tooltip->durationTo,
				'type' => 'setTooltip',
				'action' => 'hide',
				'tooltipId' => $tooltip->id,
				'hash' => md5($tooltip->id),
				'avatar' => $avatar,
				'text' => strip_tags($tooltip->text, '<p><strong><em><u><br>'),
				'style' => self::$_themes[$tooltip->tooltipStyle]
			);
		}
		if($incAssetView){
			$assetModel = App7020Assets::model()->findByPk((int) $idContent);
			$cuepoints[] = array(
 				'time' => $assetModel->duration * App7020Assets::VIDEO_VIEW_PERCENTAGE,
 				'type' => App7020Assets::CUEPOINT_VIDEO_VIEW
 			);
		}
		return $cuepoints;
	}
	
	/**
	 * Strip all tags and check text length
	 * @param type $attribute
	 * @param type $params
	 */
	public function validateTooltipText($attrs, $params){
		$text = preg_replace("/&nbsp;/", " ", strip_tags($this->text));
		$text = preg_replace("/\s+/", " ", $text);
		
		if(mb_strlen(trim($text)) > 100 || mb_strlen(trim($text)) < 5){
			$this->addError('textLimit', Yii::t('app7020', 'Text limit is (5 ~ 100) chars'));
			return false;
		}
		$output_text = strip_tags($this->text, '<p><strong><em><u><br>');
		$output_text = preg_replace("/&nbsp;/", " ", $output_text);
		$output_text = preg_replace("/\s+/", " ", $output_text);
		$this->text = trim($output_text);
		return true;
	}
	
	public function afterSave() {
		
		// Save in table "app7020_content_published" content as "Edited"
		App7020ContentPublished::saveEditAction($this->idContent);
		
		//Raise notification
		if ($this->isNewRecord) {
			Yii::app()->event->raise('App7020NewTooltip', new DEvent($this, array('tooltipId' => $this->id)));
		}

		//Create or update peer review system messages

		$sysMsg = App7020ContentReview::model()->findByAttributes(array('idContent'=>$this->idContent,'idTooltip'=>$this->id,'systemMessage'=>1));

		if(!$sysMsg){
			$newSysMsg = new App7020ContentReview();
			$newSysMsg->idContent = $this->idContent;
			$newSysMsg->idUser = $this->idExpert;
			$newSysMsg->message = Yii::t('app7020', 'Tooltip has been created');
			$newSysMsg->systemMessage = 1;
			$newSysMsg->idTooltip = $this->id;
			$newSysMsg->save(false);
		} else {
			$sysMsg->idUser = $this->idExpert;
			$sysMsg->message = Yii::t('app7020', 'Tooltip has been created');
			$sysMsg->lastUpdated = Yii::app()->localtime->getUTCNow();
			$sysMsg->save(false);
		}
		return true;
	}

	public function beforeDelete() {

		$sysMsg = App7020ContentReview::model()->findByAttributes(array('idContent'=>$this->idContent,'idTooltip'=>$this->id,'systemMessage'=>1));
		if($sysMsg){
			App7020ContentReview::model()->deleteByPk($sysMsg->id);
		}

		return true;

	}
}
