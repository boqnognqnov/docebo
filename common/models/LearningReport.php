<?php

/**
 * This is the model class for table "learning_report".
 *
 * The followings are the available columns in table 'learning_report':
 * @property integer $id_report
 * @property string $report_name
 * @property string $class_name
 * @property string $file_name
 * @property string $use_user_selection
 * @property integer $enabled
 */
class LearningReport extends CActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LearningReport the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'learning_report';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('enabled', 'numerical', 'integerOnly' => true),
            array('report_name, class_name, file_name', 'length', 'max' => 255),
            array('use_user_selection', 'length', 'max' => 5),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id_report, report_name, class_name, file_name, use_user_selection, enabled', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id_report' => 'Id Report',
            'report_name' => 'Report Name',
            'class_name' => 'Class Name',
            'file_name' => 'File Name',
            'use_user_selection' => 'Use User Selection',
            'enabled' => 'Enabled',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_report', $this->id_report);
        $criteria->compare('report_name', $this->report_name, true);
        $criteria->compare('class_name', $this->class_name, true);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('use_user_selection', $this->use_user_selection, true);
        $criteria->compare('enabled', $this->enabled);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }
}