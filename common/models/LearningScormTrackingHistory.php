<?php

/**
 * This is the model class for table "learning_scorm_tracking_history".
 *
 * The followings are the available columns in table 'learning_scorm_tracking_history':
 * @property integer $idscorm_tracking
 * @property string $date_action
 * @property double $score_raw
 * @property double $score_max
 * @property string $session_time
 * @property string $lesson_status
 */
class LearningScormTrackingHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormTrackingHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_tracking_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idscorm_tracking', 'numerical', 'integerOnly'=>true),
			array('score_raw, score_max', 'numerical'),
			array('session_time', 'length', 'max'=>15),
			array('lesson_status', 'length', 'max'=>24),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_tracking, date_action, score_raw, score_max, session_time, lesson_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('date_action medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_tracking' => 'Idscorm Tracking',
			'date_action' => 'Date Action',
			'score_raw' => 'Score Raw',
			'score_max' => 'Score Max',
			'session_time' => 'Session Time',
			'lesson_status' => 'Lesson Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_tracking',$this->idscorm_tracking);
		$criteria->compare('date_action',Yii::app()->localtime->fromLocalDateTime($this->date_action),true);
		$criteria->compare('score_raw',$this->score_raw);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('session_time',$this->session_time,true);
		$criteria->compare('lesson_status',$this->lesson_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}