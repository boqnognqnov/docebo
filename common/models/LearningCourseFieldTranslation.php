<?php

/**
 * This is the model class for table "learning_course_field_translation".
 *
 * The followings are the available columns in table 'learning_course_field_translation':
 * @property integer $id_field
 * @property string $lang_code
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property LearningCourseField $idField
 */
class LearningCourseFieldTranslation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_field_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_field', 'required'),
			array('id_field', 'numerical', 'integerOnly'=>true),
			array('lang_code, translation', 'length', 'max'=>255),
			array('id_field, lang_code, translation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idField' => array(self::BELONGS_TO, 'LearningCourseField', 'id_field'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_field' => 'Id Field',
			'lang_code' => 'Lang Code',
			'translation' => 'Translation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation',$this->translation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseFieldTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getFieldTranslations( $id = null ) {
        if ($id) {
            $fieldModelsTranslation = self::model()->findAllByAttributes(array(
                'id_field'  => $id,
            ));
            $fieldTranslations = array();
            foreach ($fieldModelsTranslation as $model) {
                if ($model->translation) {
                    $fieldTranslations[$model->lang_code] = $model->translation;
                }
            }
            return $fieldTranslations;
        } else {
            return null;
        }
    }
}
