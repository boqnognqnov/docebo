<?php

/**
 * Description of App7020ChannelSummary
 *
 */
class App7020ChannelSummary extends CModel {

	/**
	 * @var array 
	 */
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;

	public function attributeNames() {
		return;
	}

	/**
	 * Data feeder for .../app7020/index.php?r=assetsummary/index - tab "Overview"
	 * @param int $idChannel
	 * @param int $timeInterval
	 * @return array
	 */
	/*
	 * Return total count of published assets per channel for given period
	 */
	public static function publishedAssets($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'asset.id = ca.idAsset');
		$commandBase->andWhere("asset.conversion_status = '20'");
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("asset.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('asset.created');
		$tempResults = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		$counter = 0;
		foreach ($tempResults as $asset) {
			$singleRating = App7020ContentRating::calculateContentRating($asset["idAsset"]);
			if ($singleRating) {
				$counter++;
			}
			$totalRating +=$singleRating;
		}
		if ($totalRating) {
			$averageRating = $totalRating / $counter;
		} else {
			$averageRating = 0;
		}
		return array(
			'count' => count($tempResults),
			'averageRating' => round($averageRating,2)
		);
	}

	/*
	 * Return total count of submitted assets per channel for given period
	 */

	public static function submittedAssets($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'asset.id = ca.idAsset');
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("asset.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('asset.created');
		$tempResults = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		$counter = 0;
		foreach ($tempResults as $asset) {
			$singleRating = App7020ContentRating::calculateContentRating($asset["idAsset"]);
			if ($singleRating) {
				$counter++;
			}
			$totalRating +=$singleRating;
		}
		if ($totalRating) {
			$averageRating = $totalRating / $counter;
		} else {
			$averageRating = 0;
		}
		return count($tempResults);
	}

	/*
	 * Return total contributors in channel for given period
	 */

	public static function totalContributors($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'asset.id = ca.idAsset');
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("asset.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('asset.created');
		$commandBase->group('asset.userId');
		$tempResults = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return count($tempResults);
	}

	/*
	 * Return total askers in channel for given period
	 */

	public static function totalAskers($idChannel, $timeInterval = 7) {
		$askersToChannel = array();
		$askersToAsset = array();
		$askers = array();
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}
		// Questions related directly to the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'question.id = cq.idQuestion');
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$commandBase->group('question.idUser');
		$askersToChannel = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		// Questions related to assets of the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'question.idContent = ca.idAsset');
		$commandBase->andWhere("question.idContent != ''");
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$commandBase->group('question.idUser');
		$askersToAsset = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		// Merge the arrays with questions to asset and questions to channel
		$totalAskers = array_merge($askersToAsset, $askersToChannel);
		foreach ($totalAskers as $asker) {
			$askers[] = $asker["idUser"];
		}
		// Return only unique idUser
		$askers = array_unique($askers);
		return count($askers);
	}

	/*
	 * Get contributions in the given channel for selected period
	 */

	public static function channelContributions($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'asset.id = ca.idAsset');
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(created)');
		}
		$commandBase->order('created');
		$tempResults = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return array(
			'chartData' => App7020Helpers::getChartData($tempResults, $timeInterval),
		);
	}

	public static function channelAssetViews($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ContentHistory::model()->tableName() . ' ch', 'asset.id = ch.idContent');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'asset.id = ca.idAsset');
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("ch.viewed BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(ch.viewed, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(ch.viewed, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(ch.viewed, '%Y-%m-%d') AS period, COUNT(*) AS count");
			//$commandBase->group('DATE(ch.viewed)');
		}
		$commandBase->order('ch.viewed');
		$tempResults = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return array(
			'chartData' => App7020Helpers::getChartData($tempResults, $timeInterval),
		);
	}

	public static function sharedAssetsVsTotalChannels($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("SUM(CASE WHEN ca.idChannel = :idChannel THEN 1 ELSE 0 END) currentChannelCount, COUNT(asset.id) allChannelCount");
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'ca.idAsset = asset.id');
		$commandBase->andWhere("asset.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$assets = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		if ($assets[0]["allChannelCount"]) {
			$channelPerc = ($assets[0]["currentChannelCount"] / $assets[0]["allChannelCount"]) * 100;
		} else {
			$channelPerc = 0;
		}
		return array(
			'currentChannelCount' => $assets[0]["currentChannelCount"],
			'otherChannelCount' => $assets[0]["allChannelCount"],
			'channelPerc' => round($channelPerc, 2)
		);
	}

	public static function submittedVsPublished($idChannel, $timeInterval = 7) {
		$publishedAssets["count"] = self::publishedAssets($idChannel, $timeInterval)["count"];
		$submittedAssets["count"] = self::submittedAssets($idChannel, $timeInterval);
		$unsubmittedAssets["count"] = $submittedAssets["count"] - $publishedAssets["count"] ;
		$publishedAssets["label"] = Yii::t('app7020', 'Published assets');
		$submittedAssets["label"] = Yii::t('app7020', 'Submitted assets');
		$unsubmittedAssets["label"] =  Yii::t('app7020', 'Unpublished assets');
		if($submittedAssets["count"]) {
			
			$submittedAssets["percentage"] = 100;
			$publishedAssets["percentage"] = ($publishedAssets["count"] / $submittedAssets["count"]) * 100;
		} else {
			$submittedAssets["percentage"] = 0;
			$publishedAssets["percentage"] = 0;
		}
		$unsubmittedAssets["percentage"] = $submittedAssets["percentage"] - $publishedAssets["percentage"] ;
		return array(
			'publishedAssets' => $publishedAssets,
			'submittedAssets' => $submittedAssets,
			'unsubmittedAssets' => $unsubmittedAssets,
		);
	}

	// All data for "Overview" tab
	public static function channelSummaryOverviewData($idChannel, $timeInterval = 7) {
		$data["publishedAssets"] = self::publishedAssets($idChannel, $timeInterval);
		$data["totalAskers"] = self::totalContributors($idChannel, $timeInterval);
		$data["totalContributors"] = self::totalAskers($idChannel, $timeInterval);
		$data["channelContributions"] = self::channelContributions($idChannel, $timeInterval);
		$data["channelAssetViews"] = self::channelAssetViews($idChannel, $timeInterval);
		$data["sharedAssetsVsTotalChannels"] = self::sharedAssetsVsTotalChannels($idChannel, $timeInterval);
		$data["submittedVsPublished"] = self::submittedVsPublished($idChannel, $timeInterval);
		$data["lastAssets"]= App7020ChannelAssets::getChannelAssets($idChannel, 0, 3);
		return $data;
	}

	public static function channelQuestionsAnswers($idChannel, $timeInterval = 7) {
		if (in_array($timeInterval, array(self::TIMEFRAME_WEEKLY, self::TIMEFRAME_MONTHLY, self::TIMEFRAME_YEARLY), true)) {
			$timeInterval = $timeInterval;
		} else {
			$timeInterval = self::TIMEFRAME_WEEKLY;
		}
		// Questions Related to channels Data
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Question::model()->tableName() . ' q');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'q.id = cq.idQuestion');
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("q.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(q.created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(q.created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(q.created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(q.created)');
		}
		$commandBase->order('q.created');
		$questionsToChannelData = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		$questionsToChannelData = App7020Helpers::getChartData($questionsToChannelData, $timeInterval);
		// Questions related to assets of the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'question.idContent = ca.idAsset');
		$commandBase->andWhere("question.idContent != ''");
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(question.created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(question.created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(question.created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(question.created)');
		}
		$commandBase->order('question.created');
		$questionsToAssetData = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		$questionsToAssetData = App7020Helpers::getChartData($questionsToAssetData, $timeInterval);
		// Answer Data
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Answer::model()->tableName() . ' answer');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'answer.idQuestion = cq.idQuestion');
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("answer.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$commandBase->select("DATE_FORMAT(answer.created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(answer.created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(answer.created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(answer.created)');
		}
		$commandBase->order('answer.created');
		$answerData = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		foreach ($questionsToAssetData["points"] as $key => $value) {
			$sumQuestions[$key] = $questionsToChannelData["points"][$key] + $value;
		}
		$questionsToChannelData["points"] = $sumQuestions;
		return array(
			'questionsData' => $questionsToChannelData,
			'answersData' => App7020Helpers::getChartData($answerData, $timeInterval),
		);
	}

	public static function getChannelQuestions($idChannel, $timeInterval = 7) {
		// Questions related directly to the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'question.id = cq.idQuestion');
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$questionChannel = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		// Questions related to assets of the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'question.idContent = ca.idAsset');
		$commandBase->andWhere("question.idContent != ''");
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$questionAssetChannel = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return array(
			'questionChannel' => count($questionChannel),
			'questionAssetChannel' => count($questionAssetChannel),
			'totalQuestions' => count($questionChannel) + count($questionAssetChannel)
		);
	}

	public static function getOtherChannelQuestions($idChannel, $timeInterval = 7) {
		// Questions related directly to the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'question.id = cq.idQuestion');
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$questionChannel = $commandBase->queryAll(true, array(':timeInterval' => $timeInterval));
		// Questions related to assets of the given channel
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("question.idUser");
		$commandBase->from(App7020Question::model()->tableName() . ' question');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'question.idContent = ca.idAsset');
		$commandBase->andWhere("question.idContent != ''");
		$commandBase->andWhere("question.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('question.created');
		$questionAssetChannel = $commandBase->queryAll(true, array(':timeInterval' => $timeInterval));
		return array(
			'questionChannel' => count($questionChannel),
			'questionAssetChannel' => count($questionAssetChannel),
			'totalQuestions' => count($questionChannel) + count($questionAssetChannel)
		);
	}

	public static function getChannelAnswers($idChannel, $timeInterval = 7) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Answer::model()->tableName() . ' answer');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'answer.idQuestion = cq.idQuestion');
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("answer.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('answer.created');
		$answerData = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return array(
			'totalAnswers' => count($answerData)
		);
	}

	public static function getChannelBestAnswers($idChannel, $timeInterval = 7) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Answer::model()->tableName() . ' answer');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'answer.idQuestion = cq.idQuestion');
		$commandBase->andWhere("answer.bestAnswer = '2'");
		$commandBase->andWhere("cq.idChannel = :idChannel");
		$commandBase->andWhere("answer.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('answer.created');
		$bestAnswers = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		return array(
			'bestAnswers' => count($bestAnswers)
		);
	}

	public static function getChannelReviews($idChannel, $timeInterval = 7) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020ContentReview::model()->tableName() . ' review');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'review.idContent = ca.idAsset');
		$commandBase->andWhere("ca.idChannel = :idChannel");
		$commandBase->andWhere("review.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('review.created');
		$singleChannelReviews = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020ContentReview::model()->tableName() . ' review');
		$commandBase->join(App7020ChannelAssets::model()->tableName() . ' ca', 'review.idContent = ca.idAsset');
		$commandBase->andWhere("ca.idChannel != :idChannel");
		$commandBase->andWhere("review.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('review.created');
		$allReviews = $commandBase->queryAll(true, array(':idChannel' => $idChannel, ':timeInterval' => $timeInterval));
		if ($allReviews) {
			$reviewPercentage = (count($singleChannelReviews) / count($allReviews)) * 100;
			$reviewPercentage = number_format((float) $reviewPercentage, 2, '.', '');
		} else {
			$reviewPercentage = 0;
		}
		return array(
			'singleChannelReviews' => count($singleChannelReviews),
			'allReviews' => count($allReviews),
			'reviewPercentage' => $reviewPercentage
		);
	}

	public static function getOtherChannelsAnswers($idChannel, $timeInterval = 7) {
		// Other Channels Answers
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Answer::model()->tableName() . ' answer');
		$commandBase->join(App7020ChannelQuestions::model()->tableName() . ' cq', 'answer.idQuestion = cq.idQuestion');
		$commandBase->andWhere("answer.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		$commandBase->order('answer.created');
		$answerData = $commandBase->queryAll(true, array(':timeInterval' => $timeInterval));
		return array(
			'totalAnswers' => count($answerData)
		);
	}

	public static function getQaComparedToTotalChannels($idChannel, $timeInterval = 7) {
		$questions = self::getChannelQuestions($idChannel, $timeInterval);
		$questions = $questions["totalQuestions"];
		$answers = self::getChannelAnswers($idChannel, $timeInterval);
		$answers = $answers["totalAnswers"];
		$currentChannelQa = $questions + $answers;
		$otherChannelsQuestions = self::getOtherChannelQuestions($idChannel, $timeInterval);
		$otherChannelsQuestions = $otherChannelsQuestions["totalQuestions"];
		$otherChannelsAnswers = self::getOtherChannelsAnswers($idChannel, $timeInterval);
		$otherChannelsAnswers = $otherChannelsAnswers["totalAnswers"];
		$otherChannelsQa = $otherChannelsQuestions + $otherChannelsAnswers;
		if ($otherChannelsQa) {
			$qaPercentage = ($currentChannelQa / $otherChannelsQa) * 100;
		} else {
			$qaPercentage = "0";
		}
		return round($qaPercentage, 2);
	}

	// All data for Questions & Answers tab
	public static function channelSummaryQaData($idChannel, $timeInterval = 7) {
		$data["questionAnswersChart"] = self::channelQuestionsAnswers($idChannel, $timeInterval);
		$data["questions"] = self::getChannelQuestions($idChannel, $timeInterval);
		$data["answers"] = self::getChannelAnswers($idChannel, $timeInterval);
		$data["bestAnswers"] = self::getChannelBestAnswers($idChannel, $timeInterval);
		$data["reviews"] = self::getChannelReviews($idChannel, $timeInterval);
		$data["qaPercentage"] = self::getQaComparedToTotalChannels($idChannel, $timeInterval);
		return $data;
	}

	public static function getTopCoachExperts($idChannel) {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
					idst, 
					firstname,
					lastname,
					CONCAT(firstname, ' ', lastname) as expert_name,
					TRIM(LEADING '/' FROM userid) AS username,
					avatar,
					SUM(CASE WHEN app7020_answer_like.type = " . App7020AnswerLike::TYPE_LIKE . " THEN 1 ELSE 0 END) count_likes,
					SUM(CASE WHEN app7020_answer_like.type = " . App7020AnswerLike::TYPE_DISLIKE . " THEN 1 ELSE 0 END) count_dislikes,
					SUM(CASE WHEN app7020_answer.bestAnswer = " . App7020Answer::BEST_ANSWER . " THEN 1 ELSE 0 END) count_bestAnswers,
					COUNT(app7020_answer.id) as count_answers
			");
		$commandBase->from(CoreUser::model()->tableName() . ' core_user');
		$commandBase->join(App7020Answer::model()->tableName() . ' app7020_answer', 'app7020_answer.idUser = core_user.idst');
		$commandBase->leftJoin(App7020AnswerLike::model()->tableName() . ' app7020_answer_like', 'app7020_answer_like.idAnswer = app7020_answer.id');
		$commandBase->group('idst');
		$commandBase->order('count_bestAnswers DESC, count_likes DESC');
		$commandBase->limit(5);

		$resultArray = array();
		foreach ($commandBase->queryAll(true) as $result) {
			$resultArray[] = array(
				'idst' => $result['idst'],
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'expert_name' => trim($result['expert_name']),
				'username' => $result['username'],
				'avatar' => $result['avatar'] ? $storage->absoluteBaseUrl . '/avatar/' . $result['avatar'] : $standartAvatar,
				'avatar_db' => $result['avatar'],
				'count_likes' => $result['count_likes'],
				'count_dislikes' => $result['count_dislikes'],
				'count_bestAnswers' => $result['count_bestAnswers'],
				'count_answers' => $result['count_answers']
			);
		}

		
		return $resultArray;
	}

	public static function getTopShareExperts($idChannel) {

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("asset.*, ROUND(AVG(UNIX_TIMESTAMP(cp.datePublished) - UNIX_TIMESTAMP(asset.created))) as avgTime , SUM(CASE WHEN cp.actionType = '1' THEN 1 ELSE 0 END) published, SUM(CASE WHEN cp.actionType = '2' THEN 1 ELSE 0 END) reviewed");
		$commandBase->from(App7020Assets::model()->tableName() . ' asset');
		$commandBase->join(App7020ContentPublished::model()->tableName() . ' cp', 'asset.id = cp.idContent');
		$commandBase->group('cp.idUser');
		$commandBase->order('avgTime ASC');
		$commandBase->limit(5);
		$results = $commandBase->queryAll(true);
		$resultArray = array();
		foreach ($results as $result) {
			$userModel = CoreUser::model()->findByPk($result['userId']);
			$resultArray[] = array(
				'idst' => $result['userId'],
				'firstname' => $userModel->firstname,
				'lastname' => $userModel->lastname,
				'expert_name' => $userModel->firstname . " " . $userModel->lastname,
				'username' => $userModel->userid,
				'avgTime' => $result['avgTime'],
				'hours' => floor($result['avgTime'] / 3600),
				'minutes' => floor(($result['avgTime'] / 60) % 60),
				'published' => $result['published'],
				'reviewed' => $result['reviewed']
			);
		}
		return $resultArray;
	}

	// All data for Experts
	public static function ExpertsData($idChannel) {

		$data["topCoachExperts"] = self::getTopCoachExperts($idChannel);
		$data["topShareExperts"] = self::getTopShareExperts($idChannel);
		return $data;
	}

}
