<?php



/**
 * This is the model class for table "core_user_field_dropdown".
 *
 * @property integer $id_field
 * @property integer $id_option
 * @property integer $sequence
 *
 */
class CoreUserFieldDropdown extends \CActiveRecord {


	/**
	 * @inheritdoc
	 */
	public function tableName() {
		return 'core_user_field_dropdown';
	}
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['id_field, sequence', 'required'],
			['id_field, sequence', 'numerical', 'integerOnly' => true],
		];
	}

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'translations' => array(self::HAS_MANY, 'CoreUserFieldDropdownTranslations', 'id_option'),
        );
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [];
	}

	public static function renderDropDownOptionValue($idCommon, $idUser, $htmlEncode = true)
	{
		$fieldUserEntry = CoreUserFieldValue::model()->findByAttributes(array(
			'id_user' => $idUser
		));

		$fieldName = 'field_' . $idCommon;
		$userEntry = (int) $fieldUserEntry->{$fieldName};

		$longLangCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaultLangCode = Settings::get('default_language', 'english');
        $cmdResult = CoreUserFieldDropdownTranslations::getLabelForLanguage($longLangCode, $defaultLangCode, $idCommon, $userEntry)->queryRow();

		if ($htmlEncode) {
            return CHtml::encode($cmdResult['translation']);
        } else {
            return $cmdResult['translation'];
        }
	}

    /**
     * Retrieving the option value relevant to a label
     * Used in cases as importing users from a CSV file
     *
     * @param string $label
     * @param string|integer $fieldId
     * @param string $language
     * @return string|integer|false
     */
	public static function getOptionValueByLabel($label, $fieldId, $language = null)
    {
        $trTbl = CoreUserFieldDropdownTranslations::model()->tableName();
        $thisTbl = static::model()->tableName();
        $fieldTbl = CoreUserField::model()->tableName();

        /** @var CDbCommand $cmd */
        $cmd = Yii::app()->db->createCommand();
        $cmd->select($thisTbl . '.id_option');
        $cmd->from($thisTbl);
        $cmd->join($trTbl, "{$trTbl}.id_option = {$thisTbl}.id_option");
        $cmd->join($fieldTbl, "{$fieldTbl}.id_field = {$thisTbl}.id_field");

        $condition = "{$fieldTbl}.id_field = :fieldId AND {$trTbl}.translation = :label";
        $conditionParams = ['fieldId' => $fieldId, 'label' => $label];

        if ($language !== null) {
            $condition .= " AND {$trTbl}.lang_code = :lang";
            $conditionParams['lang'] = $language;
        }

        $cmd->where($condition, $conditionParams);

        return $cmd->queryScalar();
    }

}
