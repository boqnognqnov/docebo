<?php

/**
 * This is the model class for table "conference_room".
 *
 * The followings are the available columns in table 'conference_room':
 * @property string $id
 * @property string $idCal
 * @property integer $idCourse
 * @property integer $idSt
 * @property integer $accountId
 * @property string $name
 * @property string $description
 * @property string $room_type
 * @property string $starttime
 * @property string $endtime
 * @property integer $meetinghours
 * @property integer $meetingminutes
 * @property integer $maxparticipants
 * @property integer $bookable
 *
 * The followings are the available model relations:
 * @property user $user
 * @property course $course
 */
class ConferenceRoom extends CActiveRecord
{
    const STATUS_ANYTIME = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_HISTORY = 2;
    const STATUS_ONAIR = 4;
    const STATUS_SCHEDULED = 5;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceRoom the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_room';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCourse, idSt, accountId, meetinghours, meetingminutes, maxparticipants, bookable', 'numerical', 'integerOnly'=>true),
			array('idCal, starttime, endtime', 'length', 'max'=>20),
			array('name, room_type', 'length', 'max'=>255),
            array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idCal, idCourse, idSt, accountId, name, description, room_type, starttime, endtime, meetinghours, meetingminutes, maxparticipants, bookable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idSt'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'idCourse'),
		    'adobeConnectMeetingData' => array(self::HAS_ONE, 'ConferenceAdobeconnectMeeting', 'id_room'),
            'bigBlueButtonMeetingData' => array(self::HAS_ONE, 'ConferenceBigbluebuttonMeeting', 'id_room'),
            'gotoMeetingMeetingData' => array(self::HAS_ONE, 'ConferenceGotomeetingMeeting', 'id_room'),
            'onSyncMeetingData' => array(self::HAS_ONE, 'ConferenceOnsyncMeeting', 'id_room'),
            'webexMeetingData' => array(self::HAS_ONE, 'ConferenceWebexMeeting', 'id_room'),
            'teleskillMeetingData' => array(self::HAS_ONE, 'ConferenceTeleskillMeeting', 'id_room'),
			'skyMeetingMeetingData' => array(self::HAS_ONE, 'ConferenceSkyMeetingMeeting', 'id_room'),
			'viaappMeetingData' => array(self::HAS_ONE, 'ConferenceViaappMeeting', 'id_room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCal' => 'Calendar Id',
			'idCourse' => 'Course Id',
			'idSt' => 'User Id',
			'accountId' => 'Account Id',
			'name' => 'Name',
            'description' => 'Description',
			'room_type' => 'Room Type',
			'starttime' => 'Start time',
			'endtime' => 'End time',
			'meetinghours' => 'Hours',
		    'meetingminutes' => 'Minutes',
			'maxparticipants' => 'Max participants',
			'bookable' => 'Bookable',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idCal',$this->idCal,true);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idSt',$this->idSt);
		$criteria->compare('accountId',$this->accountId);
		$criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
		$criteria->compare('room_type',$this->room_type,true);
		$criteria->compare('starttime',$this->starttime,true);
		$criteria->compare('endtime',$this->endtime,true);
		$criteria->compare('meetinghours',$this->meetinghours);
		$criteria->compare('meetingminutes',$this->meetinghours);
		$criteria->compare('maxparticipants',$this->maxparticipants);
		$criteria->compare('bookable',$this->bookable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    /**
     * @param $idCourse
     * @param int $timeConstrain
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
	public function getRoomsListDataProvider($idCourse, $timeConstrain = self::STATUS_ANYTIME)
	{
		$availableConfSystems = ConferenceManager::getAvailableSystems();

		// Get Server time converted to UTC
		$utcNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$timeMark = strtotime($utcNow);

		$criteria=new CDbCriteria;
		$criteria->compare('idCourse',$idCourse);

		// Get only *available* conf. systems
		$criteria->addInCondition('room_type', array_keys($availableConfSystems));

		// Search for PAST rooms?  (no more joins; it is ended)
		if ($timeConstrain == self::STATUS_HISTORY) {
			$criteria->addCondition(" (endtime < $timeMark) OR (is_history=1) ");
		}
		// All rooms not yet finished (or maybe not yet started)
		else if ($timeConstrain == self::STATUS_ACTIVE) {
			$criteria->addCondition(" (endtime >= $timeMark) AND (is_history=0) ");
		}
		// Active rooms running NOW?
		else if ($timeConstrain == self::STATUS_ONAIR) {
			$criteria->addCondition(" (starttime <= $timeMark) AND (endtime >= $timeMark) AND (is_history=0) ");
		}
		// Active rooms not yet started?
		else if ($timeConstrain == self::STATUS_SCHEDULED) {
			$criteria->addCondition(" (starttime > $timeMark) AND (is_history=0) ");
		}

		$dateFilter = Yii::app()->request->getParam('videoconf_history_date_filter');
		if ($dateFilter) {
			$starttimeTimestamp = strtotime($dateFilter);
			$endtimeTimestamp = strtotime('+1 day', $starttimeTimestamp);

			$criteria->addCondition(" (starttime >= $starttimeTimestamp) ");
			$criteria->addCondition(" (endtime < $endtimeTimestamp) ");
		}

		$provider = new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
                'pagination' => array(
                    'pageSize' => Settings::get('elements_per_page', 10)
                )
		));

		return $provider;
	}

    /**
     * Return a link (text && URL) to launch a conference.
     * This is called from GridView colimns'  'value'
     *
     * @param bool $moderator
     * @return string
     */
	public function getLauncherLink($moderator = false, $joinRoute = "videoconference/joinMeeting") {

        $result = Yii::t('standard', '_PLANNED');;

        $now = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

        if ( ($now >= $this->starttime) && ($now <= $this->endtime) || $moderator || (true)) {
            $text = Yii::t('standard', '_ENTER');
            if ($moderator) {
                $text .= " (" . Yii::t('standard', 'as moderator') . ")";
            }
            $url = Yii::app()->createUrl($joinRoute, array("room_id" => $this->id));
            $result = "<a target='_new' href='$url'>$text</a>";
        }

	    return $result;
	}
}