<?php

/**
 * This is the model class for table "salesforce_user".
 *
 * The followings are the available columns in table 'salesforce_user':
 * @property integer $id
 * @property integer $id_user_lms
 * @property integer $id_user_sf
 * @property string $sf_type
 * @property integer $sf_lms_id
 * @property string $account_id
 *
 * The followings are the available model relations:
 * @property CoreUser $idUserLms
 */
class SalesforceUser extends CActiveRecord
{
	
	/**
	 * Salesforce user types
	 * @var string
	 */
	const USER_TYPE_USER            = 'user';
	const USER_TYPE_CONTACT         = 'contact';
	const USER_TYPE_LEAD            = 'lead';
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user_lms, id_user_sf, sf_type', 'required'),
			array('id_user_lms, sf_lms_id', 'numerical', 'integerOnly'=>true),
			array('sf_type', 'length', 'max'=>16),
			array('account_id', 'length', 'max'=>18),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user_lms, id_user_sf, sf_type, sf_lms_id, account_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUserLms' => array(self::BELONGS_TO, 'CoreUser', 'id_user_lms'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user_lms' => 'Id User Lms',
			'id_user_sf' => 'Id User Sf',
			'sf_type' => 'Sf Type',
			'sf_lms_id' => 'Sf Lms',
			'account_id' => 'Sf Account ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user_lms',$this->id_user_lms);
		$criteria->compare('id_user_sf',$this->id_user_sf);
		$criteria->compare('sf_type',$this->sf_type,true);
		$criteria->compare('sf_lms_id',$this->sf_lms_id);
		$criteria->compare('account_id',$this->account_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * 
	 * @param unknown $idAccount
	 * @param unknown $idUser
	 * @return mixed
	 */
	public static function getUserType($idAccount, $idUser) {
	    $command = Yii::app()->db->createCommand();
	    $command->select("sf_type")
	       ->from(self::model()->tableName())
	       ->where('sf_lms_id=:idAccount AND id_user_lms=:idUser')
	       ->limit(1);
	    
	    $params = array(
	        'idAccount'    => $idAccount,
	        'idUser'       => $idUser,
	    );
	    
	    $type = $command->queryScalar($params);
	    
	    return $type;
	    
	}
	
}
