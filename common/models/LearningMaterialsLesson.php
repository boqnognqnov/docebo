<?php

/**
 * This is the model class for table "learning_materials_lesson".
 *
 * The followings are the available columns in table 'learning_materials_lesson':
 * @property integer $idLesson
 * @property integer $author
 * @property string $title
 * @property string $description
 * @property string $path
 * @property string $original_filename
 */
class LearningMaterialsLesson extends CActiveRecord
{
	const EXTENSIONS_WHITELIST = 'zip,doc,xls,ppt,jpg,gif,png,txt,docx,pptx,ppsx,xlsx,pdf,csv,bmp';


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningMaterialsLesson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_materials_lesson';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('description', 'required'),
			array('original_filename', 'required'),
			array('author', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('path, original_filename', 'length', 'max'=>255),
			array('title, description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLesson, author, title, description, path, original_filename', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLesson' => 'Id Lesson',
			'author' => 'Author',
			'title' => 'Title',
			'description' => 'Description',
			'path' => 'Path',
			'original_filename' => 'Original Filename',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLesson',$this->idLesson);
		$criteria->compare('author',$this->author);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('original_filename',$this->original_filename,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	/**
	 * Called when "delete()" functiont is called from an AR instance of this model; used for file deleting
	 */
	public function beforeDelete() {
		// we will delete the file only if it's referenced by one object
		$occurencies = LearningMaterialsLesson::model()->count('path = :path', array(
			':path' => $this->path
		));
		if ($occurencies <= 1) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
			$rs = $storage->remove($this->path);
			if (!$rs) {
				Yii::log('Unable to delete FILE learning object from storage: ' . $this->path, CLogger::LEVEL_ERROR);
			}
		}
		return parent::beforeDelete();
	}

	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @param bool $organization
	 * @throws CException
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {
		$copy = new LearningMaterialsLesson();
		$copy->author = Yii::app()->user->id;
		$copy->title = $this->title;
		$copy->description = $this->description;
		$copy->path = $this->path; //TO DO: a new file (copy of the original) should be created and assigned in "path" field
		$copy->original_filename = (empty($this->original_filename) || $this->original_filename === '' ? $this->path : $this->original_filename); //TO DO: a new file (copy of the original) should be created and assigned in "path" field

		if ($copy->save()) {

			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.print_r($copy->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}

}