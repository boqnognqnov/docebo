<?php

/**
 * This is the model class for table "core_role".
 *
 * The followings are the available columns in table 'core_role':
 * @property integer $idst
 * @property string $roleid
 * @property string $description
 *
 * The followings are the available model relations:
 * @property CoreRoleMembers[] $coreRoleMembers
 */
class CoreRole extends CActiveRecord {
	public static function getRolesInfo() {
		$actions = array(
			'view' => Yii::t('standard', '_VIEW'),
			'add' => Yii::t('standard', '_CREATE'),
			'mod' => Yii::t('standard', '_MOD'),
			'del' => Yii::t('standard', '_DEL'),
			'approve' => Yii::t('admin_directory', '_DIRECTORY_GROUPWAIT_ACCORDECLINE'),
			'moderate' => Yii::t('dashboard', '_WAITING_SUBSCRIPTION'),
			'assign' => Yii::t('standard', 'Enroll users'),
		);



		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('CustomRoleActionInfo', new DEvent(self, array('actions' => &$actions)));

		return $actions;
	}

	//THIS DATA IS ONLY FOR NEW LOGIC IN HYDRA PROJECT  !!!
	public static $rbac_permissions = array(
		'enrollment' => array(
			'title' => 'Enrollment',
			'items' => array(
				'view' => array('title' => "View"),
				'create' => array('title' => "Create"),
				'update' => array('title' => "Edit"),
				'delete' => array('title' => "Delete"),
			)
		),
		'subschema' => array(
			'title' => 'Subscription bundle',
			'items' => array(
				'create' => array('title' => "Create"),
				'edit' => array('title' => "Edit"),
				'view' => array('title' => "View"),
				'delete' => array('title' => "Delete"),
			)

		),
		'subplan' => array(
			'title' => 'Subscription plan',
			'items' => array(
				'create' => array('title' => "Create"),
				'edit' => array('title' => "Edit"),
				'view' => array('title' => "View"),
				'delete' => array('title' => "Delete"),
			)

		),
		'subrecord' => array(
			'title' => 'Subscription record',
			'items' => array(
				'create' => array('title' => "Create"),
				'edit' => array('title' => "Edit"),
				'view' => array('title' => "View"),
				'delete' => array('title' => "Delete"),
				'manage_seat' => array('title' => "Manage seats"),
			)

		)
	);


	public static function getPuPermissionDescription() {
		$permissionDescription = array(
					'/framework/admin/usermanagement/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view all the assigned users')),
					'/framework/admin/usermanagement/add' => array('label'=> Yii::t('standard', '_CREATE'), 'description'=>  Yii::t('adminrules', 'Can create users')),
					'/framework/admin/usermanagement/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=>  Yii::t('adminrules', 'Can edit all assigned users')),
					'/framework/admin/usermanagement/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'Can delete all assigned users')),
					'/framework/admin/usermanagement/approve_waiting_user' => array('label'=>Yii::t('admin_directory', '_DIRECTORY_GROUPWAIT_ACCORDECLINE'), 'description'=> Yii::t('adminrules', 'Can manage users in waiting list, but only if user is in an assigned branch')),
					'/framework/admin/groupmanagement/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view all the assigned groups')),
					'/framework/admin/groupmanagement/add' => array('label'=> Yii::t('standard', '_CREATE'), 'description'=> Yii::t('adminrules', 'Can create groups')),
					'/framework/admin/groupmanagement/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit all assigned groups')),
					'/framework/admin/groupmanagement/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'Can delete all assigned groups')),
					'/lms/admin/course/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view all the assigned courses')),
					'/lms/admin/course/add' => array('label'=> Yii::t('standard', '_CREATE'), 'description'=> Yii::t('adminrules', 'Can create courses')),
					'/lms/admin/course/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit all assigned courses')),
					'/lms/admin/course/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'Can delete all assigned courses')),
					'/lms/admin/course/moderate' => array('label'=> Yii::t('dashboard', '_WAITING_SUBSCRIPTION'), 'description'=> Yii::t('adminrules', 'Can manage assigned users in waiting list')),
					'/lms/admin/course/evaluation' => array('label'=> Yii::t('classroom', 'Evaluation'), 'description'=> Yii::t('adminrules', 'Can evaluate their assigned users in their assigned courses')),
					'/lms/admin/report/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view assigned reports')),
					'/lms/admin/report/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit assigned reports')),
					'/framework/admin/newsletter/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=>Yii::t('adminrules', 'Can send newsletter')),
					'/lms/admin/certificate/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=>Yii::t('adminrules', 'Can view certificates for assigned courses and users')),
					'/lms/admin/certificate/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit certificates for assigned courses and users')),
			        '/framework/admin/dashboard/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view ADMIN dashboard, includes assigned users/courses only')),
					'/framework/admin/transcripts/view' => array('label' => Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view external training for assigned users')),
					'/framework/admin/transcripts/mod' => array('label' => Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit external training for assigned users')),
			        '/lms/admin/classroomsessions/view' => array('label' => Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view all sessions')),
					'/lms/admin/classroomsessions/add' => array('label' => Yii::t('standard', '_CREATE'), 'description'=> Yii::t('adminrules', 'Can add new sessions <br> Can edit their own created sessions <br> Can enroll assigned users to their own created sessions')),
					'/lms/admin/classroomsessions/mod' => array('label' => Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit all sessions')),
			        '/lms/admin/classroomsessions/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'With Add: Can delete their sessions <br> With Edit: Can delete all sessions')),
					'/lms/admin/classroomsessions/assign' => array('label'=> Yii::t('standard', 'Enroll users'), 'description'=> Yii::t('adminrules', 'Can enroll in all sessions')),
					'/lms/admin/webinarsessions/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view all sessions')),
					'/lms/admin/webinarsessions/add' => array('label'=> Yii::t('standard', '_CREATE'), 'description'=> Yii::t('adminrules', 'Can add new sessions <br> Can edit their own created sessions <br> Can enroll assigned users to their own created sessions')),
			        '/lms/admin/webinarsessions/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit all sessions')),
			        '/lms/admin/webinarsessions/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'With Add: Can delete their sessions <br> With Edit: Can delete all sessions')),
			        '/lms/admin/webinarsessions/assign' => array('label'=> Yii::t('standard', 'Enroll users'), 'description'=> Yii::t('adminrules', 'Can enroll in all sessions')),
			        '/framework/admin/catalogManagement/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view assigned catalogs')),
			        '/framework/admin/catalogManagement/add' => array('label'=> Yii::t('standard', '_CREATE'), 'description'=> Yii::t('adminrules', 'Can add new catalogs')),
					'/framework/admin/catalogManagement/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit assigned catalogs')),
					'/framework/admin/catalogManagement/del' => array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('adminrules', 'Can delete assigned catalogs')),
					'/lms/admin/coursepath/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view assigned learning plans, and manage their users')),
					'/lms/admin/coursepath/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit assigned learning plans')),
					'/lms/admin/location/view' => array('label'=> Yii::t('standard', '_VIEW'), 'description'=> Yii::t('adminrules', 'Can view assigned locations')),
					'/lms/admin/location/mod' => array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('adminrules', 'Can edit assigned locations')),
		);

		if(PluginManager::isPluginActive('GucciApp')) {
			$permissionDescription['/lms/admin/retailportal/add'] = array('label'=> Yii::t('standard', '_ADD'), 'description'=> Yii::t('gucci.main', 'Can add new categories'));
			$permissionDescription['/lms/admin/retailportal/mod'] = array('label'=> Yii::t('standard', '_MOD'), 'description'=> Yii::t('gucci.main', 'Can edit assigned categories'));
			$permissionDescription['/lms/admin/retailportal/del'] = array('label'=> Yii::t('standard', '_DEL'), 'description'=> Yii::t('gucci.main', 'Can delete assigned categories'));
		}

		return $permissionDescription;
	}


	public static function initRoles($selectedRoles = array()) {
		// $selected = CoreRoleMembers::model()->findAllByAttributes(array('idstMember' => $id));
		// $selected = !empty($selected) ? CHtml::listData($selected, 'idst', 'idst') : array();

		$defaults = array(
			'users' => array(
				'approve' => '/framework/admin/usermanagement/approve_waiting_user',
			),
			'newsletter' => array(
				'view' => '/framework/admin/newsletter/view',
			),
		);

		$roleIds = self::getRoleIds();

		$actions = array('view', 'add', 'mod', 'del', 'approve', 'moderate', 'assign', 'evaluation');

		// Event raised for YnY app to hook on to
		Yii::app()->event->raise('CustomRoleAction', new DEvent(self, array('actions' => &$actions)));

		foreach ($roleIds as $key => $roleInfo) {
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('roleid', $roleInfo['roleId']);
			$roles = CoreRole::model()->findAll($criteria);

			$roles = CHtml::listData($roles, 'idst', 'roleid');
			$keys = array_flip($roles);

			foreach ($actions as $action) {
				$str = $roleInfo['roleId'] . $action;

				if (in_array($str, $roles)) {
					$out[$key][$action]['idst'] = $keys[$str];
					$out[$key][$action]['role'] = $str;
				} elseif (!empty($defaults[$key][$action])) {
					$out[$key][$action] = array(
						'idst' => $keys[$defaults[$key][$action]],
						'role' => $defaults[$key][$action],
					);
				} else {
					$out[$key][$action] = array();
				}

				if (!empty($out[$key][$action]['idst'])) {
					$out[$key][$action]['selected'] = array_key_exists($out[$key][$action]['idst'], $selectedRoles) ? 1 : 0;
				}
			}
		}

		return $out;
	}


	public static function getRoleIds() {
		$roles = array(
			'users' => array(
				'title' => Yii::t('standard', '_USERS'),
				'roleId' => '/framework/admin/usermanagement/',
			),
			'groups' => array(
				'title' => Yii::t('standard', '_GROUPS'),
				'roleId' => '/framework/admin/groupmanagement/',
			),
			'courses' => array(
				'title' => Yii::t('standard', '_COURSES'),
				'roleId' => '/lms/admin/course/',
			),
			'reports' => array(
				'title' => Yii::t('standard', '_REPORTS'),
				'roleId' => '/lms/admin/report/',
			),
			'newsletter' => array(
				'title' => Yii::t('standard', '_NEWSLETTER'),
				'roleId' => '/framework/admin/newsletter/',
			),
			'certificates' => array(
				'title' => Yii::t('standard', '_CERTIFICATES'),
				'roleId' => '/lms/admin/certificate/',
			),
			'dashboard' => array(
				'title' => Yii::t('menu', '_DASHBOARD'),
				'roleId' => '/framework/admin/dashboard/',
			),
			'transcripts' => array(
				'title' => Yii::t('transcripts', 'External activities'),
				'roleId' => '/framework/admin/transcripts/',
			),
			'sessions'=>array(
				'title' => Yii::t('classroom', 'Classroom sessions'),
				'roleId' => '/lms/admin/classroomsessions/'
			),
			'webinars'=>array(
				'title' => Yii::t('webinar', 'Webinar sessions'),
				'roleId' => '/lms/admin/webinarsessions/'
			),
			'catalogs'=>array(
				'title' => Yii::t('standard', 'Courses catalogs'),
				'roleId' => '/framework/admin/catalogManagement/'
			),
		);			

		$plugins_roles = PluginManager::moduleCallback('getRoleIds');
		foreach ($plugins_roles as $plugin_name => $plugin_roles) {

			$roles = array_merge($roles, $plugin_roles);
		}
		
		$event = new DEvent($this);
		Yii::app()->event->raise('OnPowerUserPermissionsRender', $event);
		
		if(!empty($event->return_value['roles'])){
			$roles = array_merge($roles, $event->return_value['roles']);
		}
		
		return $roles;
	}


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreRole the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_role';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst', 'numerical', 'integerOnly' => true),
			array('roleid, description', 'length', 'max' => 255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, roleid, description', 'safe', 'on' => 'search'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreRoleMembers' => array(self::HAS_MANY, 'CoreRoleMembers', 'idst'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idst' => 'Idst',
			'roleid' => 'Roleid',
			'description' => 'Description',
		);
	}


	public function beforeSave() {
		if (!parent::beforeSave()) {
			return false;
		}
		// if idst is null we have to generate it through core_st
		if ($this->isNewRecord) {

			$st = new CoreSt;
			if (!$st->save()) {
				return false;
			}
			$this->idst = $st->idst;
		}
		return true;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idst', $this->idst);
		$criteria->compare('roleid', $this->roleid, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}
}