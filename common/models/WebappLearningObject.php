<?php

class WebappLearningObject extends LearningOrganization
{
	/**
	 * This method serve learning object for MOBILE
	 *
	 * @param $idCourse
	 * @return mixed
	 */
	public function getLearningMaterials($idCourse, $idUser){
		$response = array(); // final response

		$params = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'downloadOnScormFoldersOnly' => false, 'aiccChapters' => true);
		$objectsTree = LearningObjectsManager::getCourseLearningObjectsTree($idCourse, $params);
		$materials   = LearningObjectsManager::flattenLearningObjectsTree($objectsTree, array('getFolders' => true));
		$response = $this->updateMaterials($materials, $idUser); // Update the status of materials modify response

		return $response;
	}

	/**
	 * @param $materials
	 * @return array
	 */
	protected function updateMaterials(&$materials, $idUser){
		$count = 0;
		$response = [];
		try {
			foreach ($materials as &$material){
				$type = $material['type'];
				$IdMaterial = $material['key'];

				self::updateStatus($material);
				self::cleanMaterials($material);
				if($type == LearningOrganization::OBJECT_TYPE_VIDEO){
					$learningVideo = LearningVideo::model()->findByPk($material['id_resource']);
					$material['video_type'] = $learningVideo->type;
					if($material['video_type'] == LearningVideo::VIDEO_TYPE_UPLOAD){
						if(count($learningVideo->subtitles) > 0){
							$subs = $learningVideo->subtitles[0];
							$material['subtitles'] = array(
								'lang_code' => $subs['lang_code']
							);
							$material['subtitles']['url'] = CoreAsset::url($subs['asset_id']);
						}
						$material['video_thumb']= $this->getVideoThumb($learningVideo->id_video);
					}
				}
				if($type == LearningOrganization::OBJECT_TYPE_SCORMORG || $type == "sco"){
					$scorm_nav = new ScormNav();
					//var_dump($material);exit;
					$scorm_item = LearningScormItems::model()->findByPk($material['idScormItem']);
					$data = $scorm_nav->getScoLaunchInfo($material['id_organization'], $scorm_item, $idUser, 'webapp');
					$material['scorm_version'] = $data['scorm_version'];
					$scorm_resource = LearningScormResources::model()->findByPk($material['id_resource']);
					$material['entry_point'] = $scorm_resource->href;
					$material['adlcp_prerequisites'] = $scorm_item->adlcp_prerequisites;
					$material['scorm_items'] = self::_getScormItems($material, $idUser, $scorm_item, $data['scorm_version']);
				}
				$response['lo_metadata'][$IdMaterial]       = $material;
				$response['lo_status'][$IdMaterial]         = self::getObjectStatus($IdMaterial);
				$response['lo_user_track'][$IdMaterial]     = self::getUserTrack($material);
				$response['lo_launch_params'][$IdMaterial]  = $this->getLaunchParams($IdMaterial, $idUser);
				if ($material['isFolder'] != true)
				$count++;
			}
			$response['count'] = $count;
		} catch (Exception $e){
			return array('result' => false, 'errors' => $e->getMessage());
		}

		return $response;
	}
	
	private static function _getScormItems($material, $idUser, $scorm_item, $sco_version){
		if ($sco_version == '1.2') {
			$scormapi = new ScormApi12($material['id_resource']);
		} else {
			$scormapi = new ScormApi2004($material['id_resource']);
		}
		$items = $scormapi->getUserScormData($idUser, $scorm_item);
		$result = array();
		$scorm_nav = new ScormNav();
		foreach($items as $k => $item){
			$scorm_resource = LearningScormResources::model()->findByPk($item['idscorm_resource']);
			$sc_item = LearningScormItems::model()->findByPk($item['idscorm_item']);
			$result[$k]['item'] = $item;
			$result[$k]['entry_point'] = $scorm_resource->href;
			$result[$k]['track'] = $item->scormItemsTrack[0]->status;
			try{
				$data = $scorm_nav->getScoLaunchInfo($material['id_organization'], $sc_item, $idUser, 'webapp');
			}catch(Exception $e){
				Log::_($e);
				Log::_($sc_item);
			}
			$result[$k]['scoVersion'] = $data['scorm_version'];
		}
		return $result;
	}

	/**
	 * Clean the training material array of unused params
	 *
	 * @param $material
	 */
	protected static function cleanMaterials(&$material){
		$removeKeys = array(
			'level',
			'path',
			'isEvaluationAllowed',
			'sort',
			'path',
			'params',
			'authoring_copy_of',
			'height',
			'width',
			'versionName',
			'showVersioning',
			'id_object',
			'is_video_transcoding',
			'is_video_transcoding_error',
			'version_create_date',
			'identifierRef',
			'identifier', 
			'timeLimitAction',
			'title_shortened',
			'headerWidget',
			'keep_updated',
			'short_description',
			'resource'
		);
		$lowerKeys = array(
			'isFolder' => 'is_folder',
			'idResource' => 'id_resource',
			'idOrganization' => 'id_organization'
		);
		if($material['type'] != LearningOrganization::OBJECT_TYPE_TEST){
			$removeKeys[] = 'self_prerequisite';
		}
		foreach($lowerKeys as $k => $v){
			$material = self::_change_key($material, $k, $v);
		}

		foreach ($removeKeys as $remove){
			unset($material[$remove]);
		}
	}

	/**
	 * @param $materials
	 * @param $response
	 */
	protected static function updateStatus(&$item){
			switch ($item['status']) {
				case LearningCommontrack::STATUS_AB_INITIO:
					$item['status_class'] = "icon-play-circle gray";
					break;
				case LearningCommontrack::STATUS_ATTEMPTED:
					$item['status_class'] = "icon-play-circle yellow";
					break;
				case LearningCommontrack::STATUS_COMPLETED:
					$item['status_class'] = "icon-completed green";
					break;
				case LearningCommontrack::STATUS_FAILED:
					$item['status_class'] = "icon-play-circle red";
					break;
				case LearningCommontrack::STATUS_PASSED:
					$item['status_class'] = "icon-completed green";
					break;
				default:
					$item['status_class'] = "icon-play-circle gray";
			}
	}
	/**
	 * @param $id - id of Learning Object ( learning_organization.idOrg )
	 */
	public static function getObjectStatus($id){
		$command = Yii::app()->db->createCommand()
			->select('status')
			->from(LearningCommontrack::model()->tableName() . ' lct')
			->where('lct.idReference = :id AND lct.idUser = :idUser', array(':id' => $id, ':idUser' => Yii::app()->user->id));

		$result = $command->queryRow();
		return $result['status'];
	}

	/**
	 * @param $material array with info for each learning objects contained in a course
	 * @return array
	 */
	private static function getUserTrack($material){
		$id        = $material['id_organization'];
		$type      = $material['type'];
		$idScorm   = $material['idScormItem'];
		$response  = array();                   // Hold final response
		$res       = '';                        // Tracking data result
		$trackData = self::getTrackData($id);   // Info from CommonTrack table for each LO
		$idTrack   = $trackData['idTrack'];     // idTrack from CommonTrack table

		if ( $type){
			$prvtCommand = Yii::app()->db->createCommand()->select('*');
			switch ( $type){ // According to LO type query the DB to get tracking information
				case LearningOrganization::OBJECT_TYPE_VIDEO :
					$prvtCommand->from(LearningVideoTrack::model()->tableName() . ' lvt')
						->where('lvt.idReference = :idTrack', array(':idTrack' => $id));
					$res = $prvtCommand->queryRow();
					break;
				case LearningOrganization::OBJECT_TYPE_TEST :
					$prvtCommand->from(LearningTesttrack::model()->tableName() . ' ltt')
						->where('ltt.idTrack = :idTrack', array(':idTrack' => $idTrack));
					$res = $prvtCommand->queryRow();
					break;
				case 'sco' :
				$prvtCommand->from(LearningScormTracking::model()->tableName(). ' lct')
					->where('lct.idscorm_item = :idScorm AND lct.idUser = :idUser', array(':idScorm' => $idScorm, ':idUser' => Yii::app()->user->id));
					$res = $prvtCommand->queryRow();
					break;
				case LearningOrganization::OBJECT_TYPE_AUTHORING:{
					$learningConversion = LearningConversion::model()->findByPk($idTrack);
					if ( $learningConversion){
						$res = CJSON::decode($learningConversion->suspend_data);
						if ( $res == '') $res = array('slideNumber' => 1);
					}
				}break;
			}
		}

		$response['timestamp'] = $trackData['dateAttempt'];
		$response['data']      = ($res ? $res : '');

		return $response;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	protected static function getTrackData($id){
		$command = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from(LearningCommontrack::model()->tableName() . ' lct');
		$command->where('lct.idReference = :id AND lct.idUser = :idUser', array(':id' => $id, ':idUser' => Yii::app()->user->id));
		$result = $command->queryRow();

		if ( $result) return $result;

		else return false;
	}

	/**
	 * @param $id
	 * @param $idUser
	 * @return array
	 */
	protected function getLaunchParams($id, $idUser){

		$loModel = LearningOrganization::model()->findByPk($id);
		if (!$loModel) {
			return array("success" => false, "err" => self::PLAYER_ERR_INVALID_LO);
		}
		$auth = new AuthToken($idUser);
		if ($auth) {
			// Get auth token, generate if not found
			$auth_code = $auth->get(true);
		}

		if(!$auth || empty($auth_code)) {
			return array("success" => false, "err" => self::PLAYER_ERR_AUTH_TOKEN_NOT_FOUND);
		}
		// Base parameters
		$urlParams = array(
			"id_object" 	=> $id,
			"course_id" 	=> $loModel->idCourse,
			"id_user" 		=> $idUser,
			"auth_code" 	=> $auth_code
		);
		return $urlParams;
	}
	
	public static function getVideoObject($video){
		switch($video['type']){
			case LearningVideo::VIDEO_TYPE_UPLOAD:
				$fileName = array_values(CJSON::decode($video['video_formats']))[0];
				$storage  = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
				$result   = $storage->fileUrl($fileName);
				break;
			case LearningVideo::VIDEO_TYPE_VIMEO:
			case LearningVideo::VIDEO_TYPE_WISTIA:
			case LearningVideo::VIDEO_TYPE_YOUTUBE:
				$videoData = json_decode($video['video_formats']);
				$result = urldecode($videoData->url);
				break;
		}
		return $result;
	}

	/**
	 * @param $id - idOrg of the Learning Material
	 * @param $idUser - Current User ( passed by the API )
	 * @param $idScorm -
	 * @return array|bool|mixed|string
	 */
	public static function getLaunchUrl($id, $idUser, $idScorm, $authCode=false){
		if (!$idUser) $idUser = Yii::app()->user->idst;
		/* Get Learning Material (only what we need) */
		$material = self::getMaterial($id);
		$objectType = $material['objectType'];
		$idResource = $material['idResource'];

		try {
			switch ($objectType){
				case LearningOrganization::OBJECT_TYPE_VIDEO :
					$video    = self::getVideoRecord($idResource);
					$result = self::getVideoObject($video);
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					$result = Player::getLoAuthPlayUrl($id, $idUser, $idScorm, "webapp", $authCode)['url'];
					break;
				case LearningOrganization::OBJECT_TYPE_AUTHORING:
					$result = self::getAuthoringSlides($idResource);
					$result['isArray'] = true;
					break;
				default:
					$result = Player::getLoAuthPlayUrl($id, $idUser, false, "webapp")['url'];
					break;
			}
		} catch (CException $e){
			$result = array('success' => false, 'err' => $e->getMessage());
		}

		return $result;
	}

	/**
	 * @param $idOrg
	 * @param $idUser
	 * @param $idScorm
	 */
	public function setTrackMaterial($id, $idUser, &$params=array()){
		// Validate params
		$status = $params['status'];
		if ( $id < 1 || $idUser < 1 || !$status) return array('success' => false, Player::PLAYER_ERR_BAD_RESOURCE);
		// Get learning object needed info
		$Material = LearningOrganization::model()->findByPk((int)$id);
		if ( !$Material ) return array('success' => false, 'error' => Player::PLAYER_ERR_INVALID_LO);
		$track = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$idUser, 'idReference' => (int)$id));
		if ( !$track){     // If the object is not tracked yet - do it
			$idTrack        = false;
			// Loop through all the LO types
			$idTrackResult  = $this->getIdTrack($idUser, $id, $Material);
			$idTrack        = ( $idTrackResult['success'] ? $idTrackResult['idTrack'] : false);
			if ( !$idTrack ){
				// Set new track record
				$track = $this->newTrackRecord($id, $idUser, $status, $Material->idResource, $Material->objectType);
			}
		} else {
			if ( $track->status !== LearningCommontrack::STATUS_COMPLETED){
				$track->status = $status;
			}
			if ( $status === LearningCommontrack::STATUS_COMPLETED)
				$track->last_complete = Yii::app()->localtime->toLocalDateTime();
		}
		if ( !$track ) throw new CException('Error while creating Commontrack model');
		// Update the track record
		$params['status'] = $status;
		$updateRecord = true;
		if(isset($params['timestamp'])){
			$currentDate = strtotime($track->dateAttempt);
			$inputDate = strtotime($params['timestamp']);
			if($inputDate < $currentDate){
				$updateRecord = false;
			}
		}
		if($updateRecord){
			$this->updateTrackRecord($id, $idUser, $track, $Material, $params);
		}
	}

	/** Create a new tracking record in the common DB
	 *
	 * @param $idReference
	 * @param $idUser
	 * @param $status
	 * @param $idTrack
	 * @param $objectType
	 */
	private function newTrackRecord($idReference, $idUser, $status, $idTrack, $objectType){
		$Commontrack = new LearningCommontrack();
		$Commontrack->idUser          = (int)$idUser;
		$Commontrack->idReference     = (int)$idReference;
		$Commontrack->idTrack         = (int)$idTrack;
		$Commontrack->objectType      = $objectType;
		$Commontrack->firstAttempt    = Yii::app()->localtime->toLocalDateTime();
		$Commontrack->dateAttempt     = Yii::app()->localtime->toLocalDateTime();
		$Commontrack->status          = $status;

		if ($status == LearningCommontrack::STATUS_COMPLETED) {
			$Commontrack->first_complete = Yii::app()->localtime->toLocalDateTime();
			$Commontrack->last_complete = Yii::app()->localtime->toLocalDateTime();
			$firstObjectCompletion = true;
		}
		if ( !$Commontrack) return array('success' => false, 'error' => 'Could not create Commontrack model');
		return $Commontrack;

	}

	/**
	 * Update the track record status
	 * if completed set the completion date
	 * @param $track - LearningCommontrack model object
	 * @param $status - status of the record
	 * @param $Material - LearningOrganization model object
	 */
	private function updateTrackRecord($id, $idUser, &$track, $Material, &$params ){
		// Update the version of the course
		$track->idResource = $Material->idResource;

		$specificTrack = false;
		switch ($track->objectType){
			case LearningOrganization::OBJECT_TYPE_VIDEO :{
				$criteria = new CDbCriteria();
				$criteria->addCondition('(idReference = :id) AND (idUser = :idUser)');
				$criteria->params = array(':id' => $id, ':idUser' => $idUser);

				$videoTrack = LearningVideoTrack::model()->findByAttributes(array('idReference' => $id, 'idUser' => $idUser));
				if ( $videoTrack){
					$videoTrack->bookmark = $params['bookmark'];
					$videoTrack->save();
				}
			}break;
			case LearningOrganization::OBJECT_TYPE_SCORMORG : {
				$criteria = new CDbCriteria();
				$criteria->addCondition('(idReference = :id) AND (idUser = :idUser) AND (score_raw > 0)');
				$criteria->params = array(':id' => $id, ':idUser' => $idUser);
				$criteria->order = "idscorm_tracking DESC";
				$scoredItem = LearningScormTracking::model()->find($criteria);
				if ( $scoredItem){
					$track->score = (int)$scoredItem->score_raw;
					$track->score_max = ( $scoredItem->score_max ? (int)$scoredItem->score_max : 100);
				}
			}break;
			case LearningOrganization::OBJECT_TYPE_TEST : {
				$specificTrack = LearningTesttrack::model()->findByAttributes(array(
					'idUser' => $idUser, 'idReference' => $id)
				);
				if (!$specificTrack) throw new CException('Error while tracking test score');
				$track->score = $specificTrack->getCurrentScore();
				$test = LearningTest::model()->findByPk($specificTrack->idTest);
				if (!$test) throw new CException('Error while tracking test score');
				$track->score_max = round($test->getMaxScore());
			}break;
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE :{
				$deliverableObject = LearningDeliverableObject::model()->findByPk($params['deliverableObject']);
				$track->score = intval($deliverableObject->evaluation_score);
				$track->score_max = 100;
			}break;
			case LearningOrganization::OBJECT_TYPE_AUTHORING:{
				$this->setAuthoringSlides($track->idTrack, $params, $idUser);
			}break;
		}
		$track->save();
		if (!$track->save()) {
			throw new CException("Error while saving commontrack for (idUser=".(int)$idUser.",idReference=".(int)$Material->idOrg."); error(s) = ".print_r($track->getErrors()));
		}

		return $track;
	}

	/**     The goal is to return the idTrack of each different object type, because
	 *      every one is in different respectable table
	 *
	 * @param $idUser       - Current User
	 * @param $idReference  - idOrg of the Learning Object
	 * @param $organization - LearningOrganization model
	 * @return array|bool   - true -> ['success' => true, 'idTrack' => $idTrack]
	 *                      - false-> ['success' => false, 'error' => 'description']
	 */
	private function getIdTrack($idUser, $idReference, $organization){
		$idTrack = false;
		switch ($organization->objectType){
	case LearningOrganization::OBJECT_TYPE_AICC: {
			$package       = $organization->aiccPackage;
			$specificTrack = LearningAiccPackageTrack::model()->findByAttributes(array(
				'id_package' => $package->id,'idUser' => $idUser)
			);
			if ($specificTrack) $idTrack = $specificTrack->id;
		} break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG: {
			$specificTrack = LearningScormItemsTrack::model()->findByAttributes(array(
				'idscorm_item' => NULL, 'idUser' => $idUser, 'idReference' => $idReference)
			);
			if ($specificTrack) $idTrack = $specificTrack->idscorm_item_track;
		} break;
				case LearningOrganization::OBJECT_TYPE_TEST: {
			$specificTrack = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
			if ($specificTrack) $idTrack = $specificTrack->idTrack;
		} break;
				case LearningOrganization::OBJECT_TYPE_POLL: {
			$specificTrack = LearningPolltrack::model()->findByAttributes(array('id_user' => $idUser, 'id_reference' => $idReference));
			if ($specificTrack) $idTrack = $specificTrack->id_track;
		} break;

				case LearningOrganization::OBJECT_TYPE_VIDEO: {
			$specificTrack = LearningVideoTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
			if ($specificTrack) $idTrack = $specificTrack->idTrack;
		} break;
				case LearningOrganization::OBJECT_TYPE_TINCAN:
			    case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				    {
					    $specificTrack = LearningTcTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
					    if ($specificTrack) $idTrack = $specificTrack->idTrack;
				    } break;
				case LearningOrganization::OBJECT_TYPE_DELIVERABLE:{
			$tackingDeliverable = true;
			$specificTrack = LearningMaterialsTrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $idReference));
			if ($specificTrack) { $idTrack = $specificTrack->idTrack; }
		}break;
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
				case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE:
				case LearningOrganization::OBJECT_TYPE_FILE: {
			$specificTrack = LearningMaterialsTrack::model()->findByAttributes(array(
				'idUser' => $idUser, 'idReference' => $idReference)
			);
			$idTrack = ( $specificTrack ? $specificTrack->idTrack : false);
		} break;
				case LearningOrganization::OBJECT_TYPE_AUTHORING: {
			$specificTrack = LearningConversion::model()->findByAttributes(array(
				'user_id' => $idUser, 'authoring_id' => $organization->idResource)
			);
			if ($specificTrack) $idTrack = $specificTrack->track_id;
		} break;
				default: {
					return array('success' => false, 'error' => 'Could not get idTrack');
				} break;
		}
		return array('success' => true, 'idTrack' => $idTrack);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	private static function getMaterial($id){
		$command = Yii::app()->db->createCommand()
			->select('objectType, idResource')
			->from('learning_organization lo')
			->where('lo.idOrg=:id', array(':id' => $id));

		return $command->queryRow();
	}

	/**
	 * @param $id - Need to pass idResource from learningOrganization
	 * in the table (learning_video) $id = id_video
	 */
	private static function getVideoRecord($id){
		$command = Yii::app()->db->createCommand()
			->select('*')
			->from(LearningVideo::model()->tableName(). ' lv')
			->where('lv.id_video=:id', array(':id' => $id));

		return $command->queryRow();
	}

	/**
	 * By givven authoring_id return array of urls pointing to amazon
	 * @param $id - authoring_id from LearningAuthoring table
	 * @return array of urls
	 * @throws CException
	 */
	private static function getAuthoringSlides($id){
		if ( !$id ) throw new CException('Does not exist Authoring with id: ' . $id);
		$learningAuthoring = LearningAuthoring::model()->findByPk($id);
		if ( !$learningAuthoring) throw new CException('Can not create LearningAuthoring model with id: ' . $id);
		$urls = $learningAuthoring->getJpgUrls(); // Get the Urls Array

		$response = ($urls && count($urls) > 0 ? array('urls' => $urls) : array('error' => 'Can not fetch url array from authoring with id: ' . $id));

		return $response;
	}

	/**
	 * We provide $params and $idUser because when the PDF have only one side
	 * it is immediate completed and there is no track session created
	 *
	 * @param $idTrack
	 * @param $params - metadata
	 * @return array
	 * @throws CException
	 */
	private function setAuthoringSlides($idTrack, $params, $idUser){
		$authoringTrackModel = LearningConversion::model()->findByPk($idTrack);
		if ( !$authoringTrackModel) { // Not existing track record - create one !
			$authoringTrackModel = new LearningConversion();
			$authoringTrackModel->authoring_id = $params['id_resource'];
			$authoringTrackModel->user_id = $idUser;
			$authoringTrackModel->completion_status = $params['status'];
			$authoringTrackModel->suspend_data = CJSON::encode(array('slideNumber' => 1));
			$authoringTrackModel->save();
		}
		if ( !$authoringTrackModel)
			throw new CException('Cannot create LearningConversion model with idTrack: ' . $idTrack);
		if ( $params['currentSlide'] != 0){
			$newSlide = array('slideNumber' => (int)$params['currentSlide']);
			$authoringTrackModel->suspend_data = CJSON::encode($newSlide);
			$authoringTrackModel->save();
		}
	}

	/**
	 * @param $idResource
	 * @return bool|mixed|string
	 */
	private function getVideoThumb($idResource){
		$video    = self::getVideoRecord($idResource);
		$fileName = array_values(CJSON::decode($video['video_formats']))[0];
		$folderName = pathinfo($fileName);
		$thumbName= $folderName['filename'] . '_00001' . '.png';
		$storage  = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
		$thumbUrl = $storage->fileUrl($thumbName, $folderName['filename'].'/thumbs');

		return $thumbUrl;
	}
	
	
	private static function _change_key( $array, $old_key, $new_key ) {

		if( ! array_key_exists( $old_key, $array ) )
			return $array;

		$keys = array_keys( $array );
		$keys[ array_search( $old_key, $keys ) ] = $new_key;

		return array_combine( $keys, $array );
	}
	
	public static function syncScormTracking(){
		return self::_scormFinish();
	}
	
	private static function _scormFinish(){
		$id_user      = Yii::app()->request->getParam("id_user", 0);
		$id_reference = Yii::app()->request->getParam("id_reference", 0);
		$id_item      = Yii::app()->request->getParam("id_item", 0);
		$launch_type  = Yii::app()->request->getParam("launch_type", false);
		$timestamp    = Yii::app()->request->getParam("timestamp", false);
		$cmi = json_decode(Yii::app()->request->getParam("cmi", "{}"));

		$object = LearningOrganization::model()->findByPk($id_reference);
		$timestampComparator = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$id_user, 'idReference' => (int)$id_reference));
		try {
			$clientTime = strtotime($timestamp);
			$databaseTime = strtotime($timestampComparator->dateAttempt);
			if($clientTime > $databaseTime){
				if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG) {
					throw new CException('Invalid Scorm Learning Object: ' . $id_reference);
				}
				Yii::app()->session['idCourse'] = $object->idCourse;

				// Adjust the object reference (in case this is a central repo LO)
				$object = $object->getMasterForCentralLo($id_user);
				$id_reference = $object->idOrg;

				// retrive info about this chapter play
				$scorm_organization = LearningScormOrganizations::model()->findByPk($object->idResource);
				if (!$scorm_organization) {
					throw new CException('Invalid Scorm organization: ' . $object->idResource);
				}
				$scorm_package = LearningScormPackage::model()->findByPk($scorm_organization->idscorm_package);
				if (!$scorm_package) {
					throw new CException('Invalid Scorm package: ' . $scorm_organization->idscorm_package);
				}
				$scorm_item = LearningScormItems::model()->findByPk($id_item);
				if (!$scorm_item) {
					throw new CException('Invalid Scorm item: ' . $id_item);
				}

				// TODO: starting data for scorm 1.2 or 2004
				if ($scorm_package->scormVersion == '1.2') {
					$scormapi = new ScormApi12($id_reference);
				} else {
					$scormapi = new ScormApi2004($id_reference);
				}

				// let's save the data, if somethign goes wrong this will launch an exception
				$cmi = $scormapi->Finish($id_user, $scorm_item, $cmi);
			}
			//it will return success even there is no update since its a succesfull operation
			$data = array(
				'success' => true
			);

		} catch (CException $e) {
			$data = array(
				'success' => false,
				'message' => $e->getMessage(),
			);
		}

		echo CJSON::encode($data);
		Yii::app()->end();
	}
}