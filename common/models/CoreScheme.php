<?php

/**
 * This is the model class for table "core_scheme".
 *
 * The followings are the available columns in table 'core_scheme':
 * @property integer $id
 * @property string $title
 * @property integer $is_custom
 * @property integer $enabled
 */
class CoreScheme extends CActiveRecord
{
    const COLOR_DARK = 1; // Menu & Headers
    const COLOR_MIDDLE = 10; // Charts 2
    const COLOR_LIGHT = 4; // Hover item
    const COLOR_TITLE = 6; // Action buttons

	public $confirm; // filter

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreScheme the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function init() {
        $this->is_custom = 1;
        $this->enabled = 0;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_scheme';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, is_custom, enabled', 'required'),
            array('is_custom, enabled', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, is_custom, enabled', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'colorsSelection' => array(self::HAS_MANY, 'CoreSchemeColorSelection', 'scheme_id'),
            'colors' => array(self::MANY_MANY, 'CoreSchemeColor', 'core_scheme_color_selection(scheme_id, color_id)'),
        );
    }

    /**
     * @return array the scope definition.
     */
    public function scopes() {
        return array(
            'enabled' => array(
                'condition' => 't.enabled = 1',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => Yii::t('standard', '_TITLE'),
            'is_custom' => 'Is Custom',
            'enabled' => 'Enabled',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function dataProvider()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('is_custom',$this->is_custom);
        $criteria->compare('enabled',$this->enabled);

        $config['criteria'] = $criteria;
        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

        return new CActiveDataProvider($this, $config);
    }

    /**
     * 
     * @return array
     */
    public function getColorPalette() {
        $result = array(
            'dark' => '#DDD',
            'middle' => '#DDD',
            'light' => '#DDD',
            'title' => '#DDD',
        );
        
        foreach ($this->colorsSelection as $color) {
            switch ($color->color_id) {
                case self::COLOR_DARK:
                    $result['dark'] = $color->color;
                    break;
                case self::COLOR_MIDDLE:
                    $result['middle'] = $color->color;
                    break;
                case self::COLOR_LIGHT:
                    $result['light'] = $color->color;
                    break;
                case self::COLOR_TITLE:
                    $result['title'] = $color->color;
                    break;
            }
        }
        return $result;
    }

	/**
     * Get list of custom color schemes
     * 
	 * @return array
	 */
	public static function getColorSchemeList()
	{
        $criteria = new CDbCriteria();
        $criteria->condition = 't.is_custom = :is_custom';
        $criteria->with['colorsSelection'] = array(
            'joinType' => 'INNER JOIN',
        );
        $criteria->params[':is_custom'] = true;
        $criteria->order = 't.title ASC';
        
        $result = self::model()->findAll($criteria);
        return $result;
	}

	/**
	 * Get activated color scheme
     * 
	 * @return CoreScheme|false
	 */
	public static function getActiveColorScheme()
	{
		$defaultColorScheme = Settings::get('default_color_scheme', 1);

		if (!empty($_SESSION['preview_color_scheme']) && $_SESSION['preview_color_scheme'] > 0) {
			$selectedColorScheme = intval($_SESSION['preview_color_scheme']);
		} else {
			$selectedColorScheme = Settings::get('selected_color_scheme', $defaultColorScheme);
		}

		$activeColorTemplate = self::model()->findByPk($selectedColorScheme);
		return $activeColorTemplate;
	}

    /**
     * Load colors in CSS file
     * 
     * @return string
     */
    public static function generateCss($scheme = false) {
        $css = file_get_contents(Yii::app()->theme->basePath . '/css/color.css');
        if ($css !== FALSE) {
            $criteria = new CDbCriteria();
            $criteria->with['scheme'] = array(
                'joinType' => 'INNER JOIN',
            );

            if ($scheme === false && isset(Yii::app()->session['selected_color_scheme'])) {
                $scheme = Yii::app()->session['selected_color_scheme'];
            }

            if ($scheme !== false) {
                $criteria->condition = 't.color IS NOT NULL AND scheme.id = :id';
                $criteria->params[':id'] = $scheme;
            } else {
                $criteria->condition = 't.color IS NOT NULL AND scheme.enabled = :enabled AND scheme.is_custom';
                $criteria->params[':enabled'] = true;
            }

            $criteria->order = 't.color_id ASC';

            $templateColors = CoreSchemeColorSelection::model()->findAll($criteria);

            if ($templateColors) {
                $colors = self::loadColors($templateColors, true);
            } else {$criteria = new CDbCriteria();
                /*
                 * Get defailt values
                $criteria->order = 't.id ASC';

                $templateColors = CoreSchemeColor::model()->findAll($criteria);
                $colors = self::loadColors($templateColors);
                */
                
                return '';
            }


            $css = str_replace(array_keys($colors), array_values($colors), $css);
        }
        return $css;
    }

	/**
	 * Load colors in CSS file
	 *
	 * @return string
	 */
	public static function generateMobileCss($scheme = false) {
		$css = file_get_contents(Yii::app()->theme->basePath . '/css/color-mobile.css');
		if ($css !== FALSE) {
			$criteria = new CDbCriteria();
			$criteria->with['scheme'] = array(
				'joinType' => 'INNER JOIN',
			);

			if ($scheme === false && isset(Yii::app()->session['selected_color_scheme'])) {
				$scheme = Yii::app()->session['selected_color_scheme'];
			}

			if ($scheme !== false) {
				$criteria->condition = 't.color IS NOT NULL AND scheme.id = :id';
				$criteria->params[':id'] = $scheme;
			} else {
				$criteria->condition = 't.color IS NOT NULL AND scheme.enabled = :enabled AND scheme.is_custom';
				$criteria->params[':enabled'] = true;
			}

			$criteria->order = 't.color_id ASC';

			$templateColors = CoreSchemeColorSelection::model()->findAll($criteria);

			if ($templateColors) {
				$colors = self::loadColors($templateColors, true);
			} else {$criteria = new CDbCriteria();
				/*
				 * Get defailt values
				$criteria->order = 't.id ASC';

				$templateColors = CoreSchemeColor::model()->findAll($criteria);
				$colors = self::loadColors($templateColors);
				*/

				return '';
			}


			$css = str_replace(array_keys($colors), array_values($colors), $css);
		}
		return $css;
	}

    /**
     * Load colors in array
     * 
     * @param array $templateColors
     * @param boolean $isCustom
     * @return array
     */
    public static function loadColors($templateColors, $isCustom = false) {
        $colors = array();

        foreach ($templateColors as $color) {
            if ($isCustom)
                $id = $color->color_id;
            else
                $id = $color->id;

            switch ($id) {
                case 1:
                    $colors['@menuHeaders'] = $color->color;
                    break;
                case 2:
                    $colors['@titleText'] = $color->color;
                    break;
                case 3:
                    $colors['@text'] = $color->color;
                    break;
                case 4:
                    $colors['@hoverItem'] = $color->color;
                    break;
                case 5:
                    $colors['@selectedItems'] = $color->color;
                    break;
                case 6:
                    $colors['@actionButtons'] = $color->color;
                    break;
                case 7:
                    $colors['@secondaryButtons'] = $color->color;
                    break;
                case 8:
                    $colors['@otherButtons'] = $color->color;
                    break;
                case 9:
                    $colors['@charts1'] = $color->color;
                    break;
                case 10:
                    $colors['@charts2'] = $color->color;
                    break;
                case 11:
                    $colors['@charts3'] = $color->color;
                    break;
				case 12:
					$colors['@menuBackground'] = $color->color;
					break;
            }
        }

        return $colors;
    }
}