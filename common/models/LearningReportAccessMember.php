<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 08-Dec-15
 * Time: 4:32 PM
 * @property integer id_report
 * @property string member_type
 * @property integer member_id
 * @property string select_state
 */
class LearningReportAccessMember extends CActiveRecord
{
	const TYPE_USER = 'user';
	const TYPE_GROUP = 'group';
	const TYPE_BRANCH = 'branch';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningReport the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_report_access_members';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_report, member_id', 'numerical', 'integerOnly' => true),
			array('member_type, select_state', 'length', 'max' => 100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_report, member_id, member_type, select_state', 'safe', 'on' => 'search'),
		);
	}

	public static function getMembersByMemberType($idReport, $type){
		$command = Yii::app()->db->createCommand()
				->from(LearningReportAccessMember::model()->tableName())
				->where('id_report = :id AND member_type = :type', array(':id' => $idReport, ':type' => $type));
		if ($type == self::TYPE_BRANCH) {
			$command->select('member_id as key, select_state as selectState')->andWhere('select_state IS NOT NULL');
			$members = $command->queryAll();
		} else {
			$command->select('member_id');
			$members = $command->queryColumn();
		}

		return $members;
	}
}