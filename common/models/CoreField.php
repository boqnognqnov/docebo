<?php

/**
 * This is the model class for table "core_field".
 *
 * The followings are the available columns in table 'core_field':
 * @property integer $idField
 * @property integer $id_common
 * @property string $type_field
 * @property string $lang_code
 * @property string $translation
 * @property integer $sequence
 * @property string $show_on_platform
 * @property integer $use_multilang
 * @property integer $mandatory
 * @property integer $invisible_to_user
 */
class CoreField extends CActiveRecord {

	public $userEntry = '';
	public $confirm;
	public $maxSequence;
	public $default_translation;

	const TYPE_FIELD_DATE = 'date';
	const TYPE_FIELD_DROPDOWN = 'dropdown';
	const TYPE_FIELD_COUNTRY = 'country';
	const TYPE_FIELD_UPLOAD = 'upload';
	const TYPE_FIELD_YESNO = 'yesno';
	const TYPE_FIELD_TEXTFIELD = 'textfield';
	const TYPE_FIELD_FREETEXT = 'freetext';

	const MANDATORY_SYMBOL_HTML = '<span class="mandatory"> *</span>';


	// @TODO add other types here for future reference

	private function populateField() {
		$className = 'Field' . ucfirst($this->getFieldType());
		return $className::model()->findByAttributes(array('id_field' => $this->getFieldId()));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreField the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function init() {
		parent::init();

		$this->show_on_platform = 'framework,';
		$this->use_multilang = 0;

		return true;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_common, sequence, use_multilang', 'numerical', 'integerOnly'=>true),
			array('type_field, lang_code, translation, show_on_platform', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idField, id_common, type_field, lang_code, translation, sequence, show_on_platform, use_multilang', 'safe', 'on'=>'search'),
			array('userEntry', 'validateFieldEntry', 'on' => 'additionalValidate', 'skipOnError' => true),
		);
	}

	/**
	 * @return array the scope definition.
	 */
	public function scopes() {
		return array(
			'filters' => array(
				'condition' => 't.type_field <> "upload"',
			),
		);
	}

/*	public function defaultScope() {
		return array(
			'order' => 't.sequence ASC',
		);
	}*/

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'core_field_type'=>array(self::HAS_ONE, 'CoreFieldType', '', 'on'=>' core_field_type.type_field=t.type_field '),
			'core_field_userentry'=>array(self::HAS_MANY, 'CoreFieldUserentry', '', 'on'=>' core_field_userentry.id_common=t.id_common '),
			'translations' => array(self::HAS_MANY, 'CoreField', '', 'on' => 'translations.id_common = '. $this->getTableAlias() .'.idField'),
			'enTranslation' => array(self::HAS_MANY, 'CoreField', '', 'on' => "enTranslation.id_common = ". $this->getTableAlias() .".id_common and enTranslation.lang_code = '".Settings::get('default_language', 'english')."'"),
			'fieldSons' => array(self::HAS_MANY, 'CoreFieldSon', '', 'on' => $this->getTableAlias() . '.idField = fieldSons.idField'),
			'core_group_fields'=>array(self::HAS_MANY, 'CoreGroupFields', '', 'on'=>' core_group_fields.id_field=id_common ')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idField' => 'Id Field',
			'id_common' => 'Id common',
			'type_field' => Yii::t('standard', '_TYPE'),
			'lang_code' => Yii::t('standard', '_LANGUAGE'),
			'translation' => Yii::t('admin_lang', '_LANG_TRANSLATION'),
			'mandatory' => Yii::t('standard', '_MANDATORY'),
			'invisible_to_user' => Yii::t('organization_chart', '_ORG_CHART_FIELD_WRITE'),
			'sequence' => Yii::t('manmenu', '_ORDER'),
			'show_on_platform' => 'Show only in platform',
			'use_multilang' => 'Use multilanguage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idField',$this->idField);
		$criteria->compare('id_common',$this->id_common);
		$criteria->compare('type_field',$this->type_field,true);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation',$this->translation,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('show_on_platform',$this->show_on_platform,true);
		$criteria->compare('use_multilang',$this->use_multilang);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}

	public function dataProvider() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$this->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());// english

		$criteria->compare('idField',$this->idField);
		$criteria->compare('id_common',$this->id_common);
		$criteria->compare('type_field',$this->type_field,true);
		$criteria->compare('lang_code',$this->lang_code);
		$criteria->compare('translation',$this->translation,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('show_on_platform',$this->show_on_platform,true);
		$criteria->compare('use_multilang',$this->use_multilang);

		$criteria->order = 't.sequence ASC';
		$criteria->group = 'id_common';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
		));
	}

    /**
     * Returns the type of the field
     * Introduced as a common method of fetching field type for the current model and CoreUserField
     *
     * @return string
     */
	public function getFieldType()
    {
        return $this->type_field;
    }

    /**
     * Returns the ID of the field
     * Introduced as a common method of fetching field id for the current model and CoreUserField
     *
     * @return string|integer
     */
    public function getFieldId()
    {
        return $this->id_common;
    }

	// to be used in additionalFields/index view
	public function getAdditionalFieldsDataProvider()
	{
		$rawData = $this->additionalFieldsSearch()->with('enTranslation')->findAll();

		return new CArrayDataProvider($rawData, array(
			'keyField' => 'idField',
			'pagination' => false,
		));
	}


	public function additionalFieldsSearch($forceLanguage=false) {

		static $cache = array();

		$lang = ($forceLanguage === false ? Yii::app()->getLanguage() : $forceLanguage);

		if(!isset($cache[$lang])){
			$cache[$lang] = CoreLangLanguage::model()->findByAttributes(array(
				'lang_browsercode' => $lang,
			));
		}

		$language = $cache[$lang];
		$params = array(':language' => $language->lang_code);
		$filter = Yii::app()->request->getParam('filter', '');

		if($filter !== '')
			$params[':filter'] = '%'.$filter.'%';

		$this->getDbCriteria()->mergeWith(array(
			'condition' => $this->getTableAlias() . '.lang_code = :language'.($filter !== '' ? ' AND enTranslation.translation like :filter' : ''),
			'params' => $params,
			'order' => $this->getTableAlias() . '.sequence ASC'
		));

		return $this;
	}


	public function getOrgChartVisibilityDataProvider()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("lang_code = :language");
		if(!Yii::app()->user->getIsGodadmin())
			$criteria->addCondition("invisible_to_user = 0");
		$criteria->params[':language'] = Yii::app()->session['current_lang'];
		$criteria->order = "sequence ASC";
		//$fields = CoreField::model()->findAll($criteria);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false
		));
	}


	public static function getFieldIdCommonFromTranslation($translation)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('translation LIKE :translation');
		$criteria->params = array(':translation' => $translation);

		$row = CoreField::model()->find($criteria);
		if($row !== false)
			return $row->id_common;
		return false;
	}

	public function getConditionsOptions() {
		return array(
			'contains' => 'Contains',
			'equal' => 'Equal to',
			'not-equal' => 'Not equal to',
		);
	}

	public function renderFilterField() { }

	public function getCategoryName() {
		return Yii::t('field', '_' . strtoupper($this->type_field));
	}

	public function language($forceLanguage=false) {

		static $cache = array();

		$lang = ($forceLanguage === false ? Yii::app()->getLanguage() : $forceLanguage);

		if(!isset($cache[$lang])){
			$cache[$lang] = CoreLangLanguage::model()->findByAttributes(array(
				'lang_browsercode' => $lang,
			));
		}
		$language = $cache[$lang];

		$params = array(':language' => $language->lang_code, ':default_language' => CoreLangLanguage::getDefaultLanguage());

		$filter = Yii::app()->request->getParam('filter', '');

		if($filter !== '')
			$params[':filter'] = '%'.$filter.'%';

		$a = $this->getTableAlias();
		$this->getDbCriteria()->mergeWith(array(
			'select' => "$a.*, dl.default_translation",
			'join' => "left join (select translation as default_translation, id_common as d_id_common from {$this->tableName()} where lang_code = :default_language) dl ON $a.id_common = dl.d_id_common",
			'condition' => "$a.lang_code = :language".($filter !== "" ? " AND $a.translation like :filter" : ""),
			'params' => $params,
            'order' => "$a.sequence ASC"
		));

		return $this;
	}

	public function renderField($forAjax = false) {
		$field = $this->populateField();

		$field->addErrors($this->errors);
		$field->userEntry = $this->userEntry;

		$html = "";

		if ($field->hasErrors()) {
			$html .= '<div class="clearfix" style="text-align: right;">';
			$html .= CHtml::error($field, 'userEntry');
			$html .= '</div>';
		}
		$html .= $field->render($forAjax);

		return $html;
	}

	public function renderFilter() {
		$field = $this->populateField();
		return $field->renderFilterField();
	}

	public function renderValue($userEntry, $userEntryModel=null) {
		$className = 'Field' . ucfirst($this->type_field);
		$field = $className::model()->findByAttributes(array('idField' => $this->idField));
		return $field->renderFieldValue($userEntry, $userEntryModel);
	}

	public function renderFieldValue($userEntry, $userEntryModel=null) {
		Yii::app()->event->raise('RenderAdditionalFieldValue', new DEvent($this, array(
			'userEntry' => &$userEntry,
			'userEntryModel' => $userEntryModel,
		)));
		return CHtml::encode($userEntry);
	}

	/**
	 * @param $idCommon
	 * @param $idUser
	 * @param bool $htmlEncode
	 * @return string
	 */
	public static function renderFieldValueByUserId($idCommon, $idUser, $htmlEncode = true) {
		$fieldUserEntry = CoreFieldUserentry::model()->findByAttributes(array(
			'id_common' => $idCommon,
			'id_user' => $idUser
		));
		$userEntry = $fieldUserEntry->user_entry;
		Yii::app()->event->raise('RenderAdditionalFieldValue', new DEvent(self, array(
			'userEntry' => &$userEntry,
			'userEntryModel' => $fieldUserEntry
		)));
		if($htmlEncode)
			return CHtml::encode($userEntry);
		else
			return $userEntry;
	}


	/**
	 * @param $idCommon int|array list of fields id_commons
	 * @param $users int|array list of users idsts
	 * @param bool $htmlEncode
	 * @return string
	 */
	public static function renderFieldValueByUserIdMultiple($idCommons, $idUsers, $htmlEncode = true) {
		//check input data
		if (is_numeric($idCommons)) { $idCommons = array((int)$idCommons); }
		if (is_numeric($idUsers)) { $idUsers = array((int)$idUsers); }
		//prepare output variable
		$output = array();
		//retrieve all field user entries models in a single operation
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id_common', $idCommons);
		$criteria->addInCondition('id_user', $idUsers);
		$fieldUserEntries = CoreFieldUserentry::model()->findAll($criteria);
		//render field values
		foreach ($fieldUserEntries as $fieldUserEntry) {
			/* @var $fieldUserEntry CorefieldUserentry */
			$userEntry = $fieldUserEntry->user_entry;
			Yii::app()->event->raise('RenderAdditionalFieldValue', new DEvent(self, array(
				'userEntry' => &$userEntry,
				'userEntryModel' => $fieldUserEntry
			)));
			$idCommon = $fieldUserEntry->id_common;
			$idUser = $fieldUserEntry->id_user;
			if($htmlEncode)
				$output[$idCommon][$idUser] = CHtml::encode($userEntry);
			else
				$output[$idCommon][$idUser] = $userEntry;
		}
		return $output;
	}




	public function validateFieldEntry() {
		if ($this->mandatory) {
			if (empty($this->userEntry) && strlen($this->userEntry) <= 0) {
				$this->addError('userEntry', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
			}
		}
	}

	/**
	 * Get any unfilled and required mandatory "additional fields"
	 * for the provided user (or the current logged in one if not provided)
	 *
	 * @param int $userId (optional) a user for which to get unfilled fields
	 * or the current user if not provided
	 *
	 * @return array - array of models of unfilled fields for the user
	 */
	public function getUnfilledMandatoryFields($userId = null){

		if(!$userId) $userId = Yii::app()->user->getId();

		$anonymous = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
		$id_anonymous = $anonymous->getPrimaryKey();

		if(!$userId || $id_anonymous == $userId)
			return array();

		$visibleFiedsIds = CoreUser::model()->getVisibleFields();

		$c = new CDbCriteria();
		$c->addInCondition('t.id_common', $visibleFiedsIds);
		$c->addCondition('t.lang_code = :language');
		$c->addCondition('t.invisible_to_user = 0');
		$c->addCondition('t.mandatory = 1'); //only explicitly mandatory fields are required to be filled
		$c->with = array('core_field_userentry');
		$c->addCondition("core_field_userentry.id_user = :id_user OR core_field_userentry.id_user IS NULL ) AND (core_field_userentry.user_entry = '' OR core_field_userentry.user_entry IS NULL");
        $c->order = 't.sequence';
		$c->params[':id_user'] = $userId;
		$c->params[':language'] = Yii::app()->session['current_lang'];

		$fields = self::model()->findAll($c);

		return $fields;
	}

	/**
	 * Return Field type by its id_common
	 *
	 * @param number $idCommon
	 * @return string
	 */
	public static function getFieldTypeByComminId($idCommon) {

		static $types = array();

		if (isset($types[$idCommon])) {
			return $types[$idCommon];
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition('id_common=:id_common');
		$criteria->params = array(':id_common' => $idCommon);

		$model = CoreField::model()->find($criteria);
		if($model) {
			$types[$idCommon] = $model->type_field;
			return $types[$idCommon];
		}

		return false;

	}

    /**
     * Return a collection of additional field model instances
     * Uses the data of `CoreField` and the related models
     *
     * @param CoreUser $user
     * @param boolean $withUserEntryValues = false - forcing the return results to contain user values
     * @return array
     */
	public static function getForUser(CoreUser $user, $withUserEntryValues = false)
    {
        $fields = [];

        $criteria = new CDbCriteria();
        if($user->getScenario() == 'selfEditProfile'){
            // If the user is self-editing his profile
            // don't get fields marked as "Invisible to user".
            // Those are only visible for admins editing
            // the user's profile.
            $criteria->addCondition('invisible_to_user=0');
        }

        $fields = CoreField::model()->language()->findAll($criteria);

        // Load Additional fields data if this is an existing model OR we are creating user from Temporary User
        if ($withUserEntryValues === true) {
            foreach ($fields as $i => $field) {
                $fieldUserEntry = CoreFieldUserentry::model()->findByAttributes(array(
                    'id_user' => $user->idst,
                    'id_common' => $field->id_common,
                ));
                if ($fieldUserEntry) {
                    $fields[$i]->userEntry = $fieldUserEntry->user_entry;
                }
            }
        }

        return $fields;
    }

    /**
     * Returns the additional field values for a collection of users the specified by id
     *
     * @param array $userIds
     * @param boolean $returnAsArray = false
     * @return array
     */
    public static function getValuesForUsers(array $userIds, $returnAsArray = false)
    {
        $values = CoreFieldUserentry::model()->findAllByAttributes(array(
            'id_user' => $userIds,
        ));

        $grouped = [];

        foreach ($values as $entry) {
            $userId = $entry->id_user;

            if (isset($grouped[$userId]) === false) {
                $grouped[$userId] = [];
            }

            if ($returnAsArray === true) {
                $entry = $entry->getAttributes();
            }

            array_push($grouped[$userId], $entry);
        }

        return $grouped;
    }

    /**
     * Returns an array of IDs of the visible additional fields for the user, filtered by branch
     *
     * @param $user - the user to load the CoreField ids for
     * @param array $nodes - ids of the branches to filter by
     * @return array
     * @todo Moved from CoreUser! Needs refactoring for performance improvement!
     */
    public static function getIdsForUserByBranch($user, array $nodes = [])
    {
        $criteria = new CDbCriteria();

        // If not editing own profile and nodes visibility usage setting is off - return the loaded data
        if (($user->scenario != 'selfEditProfile') && (Settings::get('use_node_fields_visibility', 'off') == 'off')) {
            $fields = CoreField::model()->language()->findAll($criteria);

            return CHtml::listData($fields, 'id_common', 'id_common');
        }

        $allowedFields = array();
        $rootNode = CoreOrgChartTree::model()->getOrgRootNode();

        if(empty($nodes)) {
            if($user->isNewRecord && isset(Yii::app()->session['currentNodeId']) && Yii::app()->session['currentNodeId']){
                // Selected branch from org chart tree
                $nodes[] = Yii::app()->session['currentNodeId'];
            }
        }

        if(Yii::app()->user->getIsPu()) {
            $puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();
            // PU with at least one assigned branch
            if(count($puBranchIdsArray) > 0) {
                // show only additional fields, that are visible for the branches, assigned to PU
                $nodes = array_intersect($nodes, $puBranchIdsArray);
                if(empty($nodes))
                    $nodes = $puBranchIdsArray;
            }
        }

        if (count($nodes) !== 0) {
            foreach($nodes as $nodeId){
                $node = CoreOrgChartTree::model()->findByPk($nodeId);
                if($node && !$node->isRoot()){
                    $cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
                        ." FROM ".CoreOrgChartTree::model()->tableName()." oct "
                        ." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
                        ." WHERE oct.iLeft < :iLeft AND oct.iRight > :iRight AND oct.idOrg <> :root");
                    $reader = $cmd->query(array(
                        ':iLeft' => $node->iLeft,
                        ':iRight' => $node->iRight,
                        ':root' => $rootNode->getPrimaryKey()
                    ));
                    while ($row = $reader->read()) {
                        $allowedFields[] = $row['id_field'];
                    }

                    $cmd = Yii::app()->db->createCommand("SELECT * FROM ".CoreGroupFields::model()->tableName()." WHERE idst = :idst_oc OR idst = :idst_ocd");
                    $reader = $cmd->query(array(
                        ':idst_oc' => $node->idst_oc,
                        ':idst_ocd' => $node->idst_ocd
                    ));
                    while ($row = $reader->read()) {
                        $allowedFields[] = $row['id_field'];
                    }
                }
            }
        }

        if(Yii::app()->user->getIsPu() || (!empty($allowedFields) && is_array($allowedFields))){
            $criteria->addInCondition('id_common', $allowedFields);
        }

        $fields = CoreField::model()->language()->findAll($criteria);

        return CHtml::listData($fields, 'id_common', 'id_common');
    }

	public function getNextSequence()
	{
		$criteria=new CDbCriteria;
		$criteria->select = 'max(sequence) as maxSequence';
		$res = $this->model()->find($criteria);
		if(is_null($res->maxSequence))
			$max_seq = 0;
		else
			$max_seq = $res->maxSequence + 1;
		return $max_seq;
	}

	public function getTranslation() {
		if($this->translation == '')
		{
			$tmp = CoreField::model()->findByAttributes(array('id_common' => $this->id_common, 'lang_code' => Settings::get('default_language', 'english')));
			return $tmp->translation;
		}

		return $this->translation;
	}

	public function getAllAdditionalFields($idst_ocd=false)
	{

		if ($idst_ocd){
			$fields=CoreGroupFields::model()->findAll(array(
				'select'=>'id_field',
				'condition'=>"idst = $idst_ocd",
			));
		}
		else {

			$fields=CoreField::model()->findAll(array(
				'select'=>'id_common',
				'group'=>'id_common',
				'distinct'=>true,
				'order'=>'sequence ASC'
			));
		}

		return $fields;
	}

	/**
	 * insert records in core_field for language $language if not exist
	 * @param $language
	 * @deprecated
	 */
	public static function insertFieldsForLanguage($language) {
		$coreFieldTable = self::model()->tableName();
		$language = addslashes($language);

		$sql = "INSERT INTO $coreFieldTable
			SELECT NULL, cf.id_common, cf.type_field, '$language', '', cf.sequence, cf.show_on_platform, cf.use_multilang, cf.mandatory, cf.invisible_to_user
			FROM
				(SELECT * FROM $coreFieldTable GROUP BY `id_common`) cf
			LEFT JOIN $coreFieldTable cf1
			ON cf.id_common = cf1.id_common AND cf1.lang_code = '$language'
			WHERE cf1.`id_common` IS NULL";

		Yii::app()->db->createCommand($sql)->execute();
	}

    /**
     * Gets every additional fields option, based on the language.
     *
     * @param int $id - id_common of the additional field
     * @return array - array of options with their translations
     */
    public static function getDropdownElemnts($id){
        $options = array();
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $data = array();

        if(!empty($id)){
            $criteria = new CDbCriteria(array(
                'condition' => 'idField = :id AND lang_code = :lang',
                'params' => array(
                    ':id' => $id,
                    ':lang' => $lang
                )
            ));
            $options = CoreFieldSon::model()->findAll($criteria);

            foreach ($options as $id => $option ){
                $data[$option->id_common_son] = $option->translation;
            }

            return $data;
        }
    }

	public static function getTranslationById($id){
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

	    static $translations = array();

	    if(!isset($translations[$id])){
		    $translations[$id] = Yii::app()->getDb()->createCommand()
			    ->select('translation')
			    ->from(self::model()->tableName())
			    ->where('id_common=:idCommon', array(':idCommon'=>$id))
			    ->andWhere('lang_code=:current OR lang_code=:default', array(
				    ':current'=>$lang,
				    ':default'=>Settings::get('default_language', 'english'),
			    ))->queryScalar();
	    }

	    return $translations[$id];
    }

	static public function getIdFieldByIdCommon($idCommon){
		static $cache = null;

		if($cache===null){
			$cache = array();

			$query = Yii::app()->getDb()->createCommand()
				->select('id_common, idField')
				->from(CoreField::model()->tableName())
				->where('lang_code=:currentLang', array(':currentLang'=>Lang::getCodeByBrowserCode(Yii::app()->getLanguage())))
				->queryAll();
			foreach($query as $row){
				$cache[$row['id_common']] = $row['idField'];
			}
		}

		if(isset($cache[$idCommon])){
			return $cache[$idCommon];
		}

		return null;

	}

}

