<?php

/**
 * This is the model class for table "learning_organization".
 *
 * The followings are the available columns in table 'learning_organization':
 * @property integer $idOrg
 * @property integer $idParent
 * @property integer $lev
 * @property string $iLeft
 * @property string $iRight
 * @property string $title
 * @property string $objectType
 * @property integer $idResource
 * @property integer $keep_updated
 * @property integer $id_object
 * @property integer $idAuthor
 * @property string $language
 * @property string $dateInsert
 * @property string $date_updated
 * @property integer $idCourse
 * @property string $prerequisites
 * @property integer $isTerminator
 * @property integer $visible
 * @property string $milestone
 * @property string $width
 * @property string $height
 * @property string $publish_from
 * @property string $publish_to
 * @property string $location
 * @property string $params_json
 * @property string $params_json_inherited
 * @property string $self_prerequisite
 * @property integer $resource
 * @property string $short_description
 * @property string $from_csp
 *
 * The followings are the available model relations:
 * @property LearningCommontrack[] $learningCommontracks
 * @property LearningCourse $course
 * @property LearningScormOrganizations $scormOrganization
 * @property LearningAiccPackage $aiccPackage
 * @property LearningElucidat[] $elucidats
 * @property LearningRepositoryObject $repositoryObject
 * @property LearningRepositoryObjectVersion $objectVersion
 * @property LearningTcActivity $tincanMainActivity
 *
 * @package docebo.models
 * @subpackage table
 */
class LearningOrganization extends CActiveRecord {

	const OPENMODE_POPUP 			= 'popup';    			// new window
	const OPENMODE_LIGHTBOX 		= 'lightbox';
	const OPENMODE_INLINE 			= 'inline';
	const OPENMODE_REDIRECT 		= 'redirect';
	const OPENMODE_FULLSCREEN 		= 'fullscreen';

	const OPENMODE_DEFAULT 			= self::OPENMODE_LIGHTBOX;

	/**
	 * Object types subject to "LO max failed attempts" control
	 * @var array
	 */
	public static $OBJECTS_TO_CHECK_MAX_ATTEMPTS = array(
	   LearningOrganization::OBJECT_TYPE_AICC,
	   LearningOrganization::OBJECT_TYPE_DELIVERABLE,
	   LearningOrganization::OBJECT_TYPE_ELUCIDAT,
	   LearningOrganization::OBJECT_TYPE_LECTORA,
	   LearningOrganization::OBJECT_TYPE_POLL,
	   LearningOrganization::OBJECT_TYPE_SCORMORG,
	   LearningOrganization::OBJECT_TYPE_TEST,
	   LearningOrganization::OBJECT_TYPE_TINCAN,
	);
	

	public static $DEFAULT_OPENMODES = array(
		self::OBJECT_TYPE_AUTHORING 	=> self::OPENMODE_REDIRECT,
		self::OBJECT_TYPE_SCORMORG 		=> self::OPENMODE_LIGHTBOX,
		self::OBJECT_TYPE_AICC 			=> self::OPENMODE_LIGHTBOX,
		self::OBJECT_TYPE_TINCAN 		=> self::OPENMODE_LIGHTBOX,
		self::OBJECT_TYPE_ELUCIDAT      => self::OPENMODE_LIGHTBOX,
		self::OBJECT_TYPE_LTI           => self::OPENMODE_LIGHTBOX,
		self::OBJECT_TYPE_GOOGLEDRIVE   => self::OPENMODE_LIGHTBOX,
	);

	public static $OPENMODE_NAMES = array(
		self::OPENMODE_POPUP      => 'New window',
		self::OPENMODE_LIGHTBOX   => 'Lightbox',
		self::OPENMODE_INLINE     => 'Inline',
		self::OPENMODE_REDIRECT   => 'Redirect',
		self::OPENMODE_FULLSCREEN => 'Fullscreen',
	);

	public static $ALLOWED_OPENMODES = array(
		self::OPENMODE_INLINE   => 'Inline',
		self::OPENMODE_LIGHTBOX => 'Lightbox',
		self::OPENMODE_POPUP    => 'New window',
		// self::OPENMODE_REDIRECT => 'Redirect',
	);

	/**
	 * Due to an old issue we are using 1 as autoplay yes and -1 as autoplay none
	 */
	const AUTOPLAY_YES = 1;
	const AUTOPLAY_NO  = -1;

	const MILESTONE_NO      = '-';
	const MILESTONE_START   = 'start';
	const MILESTONE_END     = 'end';
	const MILESTONE_DEFAULT = self::MILESTONE_NO;

	const SELF_PREREQ_INCOMPLETE  = 'incomplete';
	const SELF_PREREQ_INFINITE    = '*';
	const SELF_PREREQ_ONCE        = 'NULL';
	const SELF_PREREQ_DEFAULT     = self::SELF_PREREQ_INFINITE;

	const OBJECT_TYPE_SCORMORG    = 'scormorg';
	const OBJECT_TYPE_TEST        = 'test';
	const OBJECT_TYPE_POLL        = 'poll';
	const OBJECT_TYPE_ITEM        = 'item';
	const OBJECT_TYPE_VIDEO       = 'video';
	const OBJECT_TYPE_HTMLPAGE    = 'htmlpage';
	const OBJECT_TYPE_TINCAN      = 'tincan';
	const OBJECT_TYPE_FILE        = 'file';
	const OBJECT_TYPE_AUTHORING   = 'authoring';
	const OBJECT_TYPE_DELIVERABLE = 'deliverable';
	const OBJECT_TYPE_AICC        = 'aicc';
	const OBJECT_TYPE_ELUCIDAT    = 'elucidat';
	const OBJECT_TYPE_CENTRALREPO = 'centralrepo';
	const OBJECT_TYPE_LECTORA     = 'lectora';
	const OBJECT_TYPE_LTI         = 'lti';
	const OBJECT_TYPE_GOOGLEDRIVE = 'googledrive';

	const ROOT_NODE_PREFIX = 'root_';

	/**
	 * Error constants
	 * @var number
	 */
	const PLAYER_ERR_INVALID_LO				= 201;
	const PLAYER_ERR_INVALID_LO_TYPE		= 202;
	const PLAYER_ERR_BAD_RESOURCE			= 203;
	const PLAYER_ERR_FAILED_LAUNCH_URL		= 204;
	const PLAYER_ERR_AUTH_TOKEN_NOT_FOUND	= 205;


	/**
	 * Array of errors
	 * @var array(<errorn-number-constant> => <error-message>)
	 */
	public static $errorMessages = array(
		self::PLAYER_ERR_INVALID_LO 			=> "Invalid Learning Object requested",
		self::PLAYER_ERR_BAD_RESOURCE	 		=> "Bad or missing resource",
		self::PLAYER_ERR_FAILED_LAUNCH_URL		=> "Error while retrieving the launch URL for the requested resource",
		self::PLAYER_ERR_AUTH_TOKEN_NOT_FOUND	=> "Auth token not found for a user",
		self::PLAYER_ERR_INVALID_LO_TYPE		=> "Unsupported learning object type",

	);

	// Virtual properties
	public $openmode = self::OPENMODE_DEFAULT;
	public $autoplay = self::AUTOPLAY_YES;

	public $desktop_openmode 		= self::OPENMODE_LIGHTBOX;
	public $tablet_openmode 		= self::OPENMODE_FULLSCREEN;
	public $smartphone_openmode		= self::OPENMODE_FULLSCREEN;

	public $saveViewMode = true;
	
	// TinCan specific virtual attributes
	public $enable_oauth      		= false;  // Add xapi_auth_code in the launch URL
	public $oauth_client         	= null;   // OAuth Client ID to use for generating the authorization code above
	public $use_xapi_content_url    = false;  // Instead uploading file, use/play XAPI content hosted on remote machine 
	

	// Different LO types have some specific parameters: mobile, autoplay, width, height, openmode
	// Used in Configure LO parameters interfaces
	public static $HAVE_MOBILE_PARAMETER = array(
		self::OBJECT_TYPE_VIDEO,
		self::OBJECT_TYPE_TEST,
	);

	public static $HAVE_AUTOPLAY_PARAMETER = array(
		self::OBJECT_TYPE_SCORMORG
	);

	public static $HAVE_WIDTH_PARAMETER = array(
		self::OBJECT_TYPE_SCORMORG,
		self::OBJECT_TYPE_TINCAN,
		self::OBJECT_TYPE_VIDEO,
	);

	public static $HAVE_HEIGHT_PARAMETER = array(
		self::OBJECT_TYPE_SCORMORG,
		self::OBJECT_TYPE_TINCAN,
		self::OBJECT_TYPE_VIDEO,
		self::OBJECT_TYPE_HTMLPAGE,
		self::OBJECT_TYPE_LTI,
		self::OBJECT_TYPE_GOOGLEDRIVE,
	);

	public static $HAVE_OPENMODE_PARAMETER = array(
		self::OBJECT_TYPE_TINCAN,
		self::OBJECT_TYPE_AUTHORING,
	);

	public static function objectTypes() {
		return array(
			self::OBJECT_TYPE_SCORMORG => Yii::t('storage', '_LONAME_scormorg'),
			self::OBJECT_TYPE_AICC => 'AICC',
			self::OBJECT_TYPE_TEST => Yii::t('storage', '_LONAME_test'),
			self::OBJECT_TYPE_POLL => Yii::t('storage', '_LONAME_poll'),
			self::OBJECT_TYPE_ITEM => Yii::t('standard', '_FILE'),
			self::OBJECT_TYPE_VIDEO => Yii::t('organization', 'Video'),
			self::OBJECT_TYPE_HTMLPAGE => Yii::t('storage', '_LONAME_htmlpage'),
			self::OBJECT_TYPE_TINCAN => Yii::t('standard', 'Tin Can'),
			self::OBJECT_TYPE_FILE => Yii::t('standard', '_FILE'),
			self::OBJECT_TYPE_AUTHORING => Yii::t('menu_over', '_AUTHORING'),
			self::OBJECT_TYPE_DELIVERABLE => Yii::t('deliverable', '_LONAME_deliverable'),
			self::OBJECT_TYPE_ELUCIDAT => "Elucidat",
			self::OBJECT_TYPE_LTI => 'LTI',
			self::OBJECT_TYPE_GOOGLEDRIVE => Yii::t('googledrive', 'Google Drive'),
		);
	}

	public static function objectTypeValue($type) {
		$types = self::objectTypes();
		return  isset($types[$type]) ? $types[$type] : null;
	}

	public static function objectTypeModel($type) {
		$types = self::objectTypes();
		if (isset($types[$type])) {
			switch ($type) {
				case self::OBJECT_TYPE_SCORMORG: { return 'LearningScormOrganizations'; } break;
				case self::OBJECT_TYPE_TEST: { return 'LearningTest'; } break;
				case self::OBJECT_TYPE_POLL: { return 'LearningPoll'; } break;
				//case self::OBJECT_TYPE_ITEM: { return 'LearningMaterialsLesson'; } break;
				case self::OBJECT_TYPE_VIDEO: { return 'LearningVideo'; } break;
				case self::OBJECT_TYPE_HTMLPAGE: { return 'LearningHtmlpage'; } break;
				case self::OBJECT_TYPE_TINCAN: { return 'LearningTcAp'; } break;
				case self::OBJECT_TYPE_FILE: { return 'LearningMaterialsLesson'; } break;
				case self::OBJECT_TYPE_AUTHORING: { return 'LearningAuthoring'; } break;
				case self::OBJECT_TYPE_DELIVERABLE: { return 'LearningDeliverable'; } break;
				case self::OBJECT_TYPE_ELUCIDAT: { return 'LearningElucidat'; } break;
				case self::OBJECT_TYPE_AICC: {return 'LearningAiccPackage';} break;
				case self::OBJECT_TYPE_LTI: {return 'LearningLti';} break;
				case self::OBJECT_TYPE_GOOGLEDRIVE: {return 'LearningGoogledoc';} break;
			}
		}
		return false;
	}

	public static function objectUplodableTypes() {
		return array(
			self::OBJECT_TYPE_SCORMORG,	self::OBJECT_TYPE_AICC,	self::OBJECT_TYPE_VIDEO, self::OBJECT_TYPE_TINCAN, self::OBJECT_TYPE_FILE
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningOrganization the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_organization';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idParent, lev, idResource, idAuthor, idCourse, isTerminator, visible, resource', 'numerical', 'integerOnly'=>true),
			array('title, prerequisites, location, from_csp', 'length', 'max'=>255),
			array('short_description', 'length', 'max'=>200),
			array('iLeft, iRight', 'length', 'max'=>11),
			array('objectType', 'length', 'max'=>20),
			array('language', 'length', 'max'=>50),
			array('milestone', 'length', 'max'=>5),
			array('width, height', 'length', 'max'=>4),
			array('dateInsert, date_updated, publish_from, publish_to, self_prerequisite, openmode, autoplay, params_json, resource, short_description, from_csp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idOrg, idParent, lev, iLeft, iRight, title, objectType, idResource, idAuthor, language, dateInsert, idCourse, prerequisites, isTerminator, visible, milestone, width, height, publish_from, publish_to, location, from_csp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningCommontracks' => array(self::HAS_MANY, 'LearningCommontrack', 'idReference'),
			'learningCommontrack' => array(self::HAS_ONE, 'LearningCommontrack', 'idReference'), //used in timeline data
			'learningTesttracks' => array(self::HAS_MANY, 'LearningTesttrack', 'idReference'),
			'learningScormTracking' => array(self::HAS_MANY, 'LearningScormTracking', 'idReference'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'idCourse'),
			'repositoryObject'	=> array(self::BELONGS_TO, 'LearningRepositoryObject', 'id_object'),
			'scormOrganization'	=> array(self::HAS_ONE, 'LearningScormOrganizations', array('idscorm_organization' => 'idResource')), // in case it is a Scorm
			'tincanMainActivity' => array(self::HAS_ONE, 'LearningTcActivity', array('id_tc_activity' => 'idResource')), // In case it's a TinCan LO

			// This will return the AICC package (ONE LO => ONE Package) associated to this LO or NULL
			'aiccPackage' => array(self::BELONGS_TO, 'LearningAiccPackage', 'idResource'),
//			'elucidats' => array(self::HAS_MANY, 'LearningElucidat', 'id_org'),
			'objectVersion' => array(self::BELONGS_TO, 'LearningRepositoryObjectVersion', array('objectType' => 'object_type', 'idResource' => 'id_resource'))

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idOrg' => 'Id Org',
			'idParent' => 'Id Parent',
			'lev' => 'Lev',
			'iLeft' => 'I Left',
			'iRight' => 'I Right',
			'title' => Yii::t('standard', '_TITLE'),
			'objectType' => Yii::t('standard', 'Object Type'),
			'idResource' => 'Id Resource',
			'idAuthor' => 'Id Author',
			'language' => 'Language',
			'dateInsert' => 'Date Insert',
			'idCourse' => 'Id Course',
			'prerequisites' => 'Prerequisites',
			'isTerminator' => 'Is Terminator',
			'visible' => 'Visible',
			'milestone' => 'Milestone',
			'width' => 'Width',
			'height' => 'Height',
			'publish_from' => 'Publish From',
			'publish_to' => 'Publish To',
			'location' => 'Location',
			'params_json' => 'Params JSON',
			'self_prerequisites' => 'Self Prerequisites',
			'short_description' => 'Short description',
			'from_csp' => 'From CSP'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idOrg',$this->idOrg);
		$criteria->compare('idParent',$this->idParent);
		$criteria->compare('lev',$this->lev);
		$criteria->compare('iLeft',$this->iLeft,true);
		$criteria->compare('iRight',$this->iRight,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('objectType',$this->objectType,true);
		$criteria->compare('idResource',$this->idResource);
		$criteria->compare('idAuthor',$this->idAuthor);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('dateInsert',Yii::app()->localtime->fromLocalDateTime($this->dateInsert),true);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('prerequisites',$this->prerequisites,true);
		$criteria->compare('isTerminator',$this->isTerminator);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('milestone',$this->milestone,true);
		$criteria->compare('width',$this->width,true);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('publish_from',Yii::app()->localtime->fromLocalDateTime($this->publish_from),true);
		$criteria->compare('publish_to',Yii::app()->localtime->fromLocalDateTime($this->publish_to),true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('short_description', $this->short_description, true);
		$criteria->compare('from_csp', $this->from_csp, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	/**
	 * Parameter based scope to get by course
	 * @param $courseId
	 * @return $this
	 */
	public function byCourse($courseId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 't.objectType != "" AND t.idCourse = :courseId',
			'params' => array(':courseId' => $courseId),
		));
		return $this;
	}

	/**
	 * Parameter based scope to filter completed by user
	 * @param $userId
	 * @return $this
	 */
	public function completedByUser($userId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition' => 'learningCommontracks.idUser = :userId AND learningCommontracks.status = :status',
			'params' => array(':userId' => $userId, ':status' => LearningCommontrack::STATUS_COMPLETED),
			'with' => array('learningCommontracks' => array('joinType' => 'INNER JOIN')),
			'together' => true,
		));
		return $this;
	}

	public function behaviors() {
		return array(
			'nestedSetBehavior'=>array(
				'class' => 'common.components.DoceboNestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
				'subTreeAttribute' => 'idCourse',
				'idParentAttribute' => 'idParent', //not supported yet
			),
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                'timestampAttributes' => array('dateInsert medium', 'date_updated medium', 'publish_from medium', 'publish_to medium'),
                'dateAttributes' => array()
            )
		);
	}

	/**
	 * @param $courseId
	 * @param null $userId
	 * @return CDbCriteria
	 */
	public function getLearningObjectsCriteria($courseId, $userId = null, $withCommontrackOnly = false)
	{
		$criteria = new CDbCriteria();


		if ($userId) {

			if (!$withCommontrackOnly) {

				$criteria->with = array(
					'learningCommontracks'=>array(
						'scopes'=>array('byUser'=>$userId),
						// [plamen] Commented because many table fields are used deep into the code. Can't tell which one, so I returned back.
						// This absolutely blows up the score reporting!!!
						// It is a good idea to lower the memory usage, but exact list of used fields is required. Carefully built.
						//'select' => 'learningCommontracks.status, learningCommontracks.idUser, learningCommontracks.score, learningCommontracks.score_max'
					),
					'learningTesttracks'=>array(
						'scopes'=>array('byUser'=>$userId),
						// See [plamen] comment above
						//'select' => 'learningTesttracks.idTest, learningTesttracks.idUser',
					),
					'learningScormTracking'=>array(
						// See [plamen] comment above
						// 'select'=>'learningScormTracking.score_raw, learningScormTracking.score_max'
					));

			}
			else {
				$criteria->with = array(
						'learningCommontracks'=>array(
								'scopes'=>array('byUser'=>$userId),
						),
				);
			}
		}

		$criteria->order = 't.iLeft ASC';
		$criteria->condition = 't.idCourse = :courseId AND t.objectType != ""';

		$criteria->select = array('t.title', 't.objectType', 't.idResource', 't.idOrg');
		$criteria->params = array(
			':courseId' => $courseId,
		);

		return $criteria;
	}


	/**
	 * Collect statistical information related to a given course (and/or user)
	 *
	 * @param number $idCourse
	 * @param number $idUser
	 * @return array
	 */
	public static function getLearningArray($idCourse, $idUser = null, $getLatest = false, $onlyWithPositiveStatus = false) {
		

		// Count enrolled users
		$enrolledUsersCount = LearningCourseuser::getEnrollments($idCourse, $idUser, null, true);
		$pUserRights = Yii::app()->user->checkPURights($idCourse);
		$instructor = LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
		$instructorUsers = ($instructor)? LearningCourse::getInstructorUsers($idCourse) : array();

		$result = array();

		// We start from Learning Organization table, collecting LO and their statuses from common tracking table, if any
		$command = Yii::app()->db->createCommand();
		$selectColumns = array(
			'lo.idOrg', 
		    'lo.title', 
		    'lo.objectType', 
		    'lo.idResource', 
		    'commontrack.status', 
		    'count(commontrack.status) as statusCount', 
		    'lrov.version_name', 
		    'commontrack.idMasterOrg',
		    'failed_attempts_taken',
		    'failed_attempts_allowed',
		);

		$command->from('learning_organization lo');
		$command->where('lo.idCourse=:idCourse AND lo.objectType != ""');

		if ($idUser) {

			$selectColumns[] = '(SELECT IF(lro.shared_tracking, COUNT(*), 0)
				FROM learning_organization ilo
				LEFT JOIN learning_courseuser ilcu ON ilcu.idCourse = ilo.idCourse
				LEFT JOIN learning_repository_object lro ON lro.id_object = ilo.id_object
				WHERE ilo.id_object = lo.id_object AND ilo.idCourse != :idCourse AND ilcu.idUser = :idUser AND ilo.idResource = lo.idResource
			) as sharedTracking';

			$command->leftJoin('learning_commontrack commontrack', '(commontrack.idReference=lo.idOrg) AND (commontrack.idUser=:idUser)');
			$command->andWhere('(commontrack.idUser=:idUser) OR (commontrack.idUser IS NULL)');
			$command->params[':idUser'] = $idUser;
		}
		else {
			if ($pUserRights->isPu && !$pUserRights->isInstructor) {
				$command->leftJoin('learning_commontrack commontrack', '(commontrack.idReference=lo.idOrg) AND (commontrack.idUser IN ('.implode(',', CoreUserPU::getList()).')) AND commontrack.idUser IN (SELECT idUser FROM learning_courseuser WHERE idCourse = :idCourse)');
			} elseif(!Yii::app()->user->getIsGodadmin() && $instructor && !empty($instructorUsers)) {
				$command->leftJoin('learning_commontrack commontrack', '(commontrack.idReference=lo.idOrg) AND (commontrack.idUser IN ('.implode(',', $instructorUsers).')) AND commontrack.idUser IN (SELECT idUser FROM learning_courseuser WHERE idCourse = :idCourse)');
			} else {
				$command->leftJoin('learning_commontrack commontrack', 'commontrack.idReference=lo.idOrg AND commontrack.idUser IN (SELECT idUser FROM learning_courseuser WHERE idCourse = :idCourse)');
			}
		}

		// Define the Columns that should be selected
		$command->select($selectColumns);

		$command->leftJoin('learning_repository_object_version lrov', 'commontrack.idResource = lrov.id_resource AND lo.objectType = lrov.object_type');


		$command->group('lo.idOrg, commontrack.status');
		$command->order('lo.iLeft ASC');

		$command->params[':idCourse'] = $idCourse;

		// Get List of records, one row per status per learning object
		$objects = $command->queryAll();

		// This array will keep track which SCORMORG/AICC LO (if any) is already resolved (i.e. its children traversed)
		$scormOrgResolved = array();
		$aiccOrgResolved = array();


		// Lets see if we have SCORMs, make a list of all SCORM LOs
		$scormLos = array();
		foreach ($objects as $object) {
			if ($object["objectType"] == self::OBJECT_TYPE_SCORMORG) {
				$scormLos[] = (int) $object["idMasterOrg"] ? $object["idMasterOrg"] : $object["idOrg"];
			}
		}

		// IF we have SCORMs in the list of course objects... collect average scores for all Scorm Items, in advance, use them later
		// This is so that we execute a single SQL query, not one per SCORM LO
		if ($scormLos) {
			$scormItemsAvgScoresDataCommand = Yii::app()->db->createCommand()
				->select('AVG(score_raw) AS avg_raw, AVG(score_max) as avg_max, idscorm_item')
				->from('learning_scorm_tracking')
				->join(LearningCourseuser::model()->tableName().' AS cu', 'cu.idCourse = '.$idCourse.' AND cu.idUser = learning_scorm_tracking.idUser')
				->where('idReference IN(' .implode(',',$scormLos). ')')
				->group('idscorm_item');

			if ($idUser) {
				$scormItemsAvgScoresDataCommand->andWhere('learning_scorm_tracking.idUser=:idUser');
				$scormItemsAvgScoresDataCommand->params[':idUser'] = $idUser;
			}

			// Power User filtering
			if (Yii::app()->user->getIsPu()) {
				$scormItemsAvgScoresDataCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (cu.idUser = pu.user_id)");
			}

			$scormItemsAvgScoresData = $scormItemsAvgScoresDataCommand->queryAll();
			$scormItemsAvgScores = array();
			foreach ($scormItemsAvgScoresData as $scormItemAvgScores) {
				$scormItemsAvgScores[$scormItemAvgScores["idscorm_item"]]["avg_raw"] = $scormItemAvgScores["avg_raw"];
				$scormItemsAvgScores[$scormItemAvgScores["idscorm_item"]]["avg_max"] = $scormItemAvgScores["avg_max"];
			}
		}



		// Also, see if we have AICC LOs, make a list of all AICC LOs
		$aiccItemsAvgScores = array();
		$aiccLos = array();
		foreach ($objects as $object) {
			if ($object["objectType"] == self::OBJECT_TYPE_AICC) {
				$aiccLos[] = (int) $object["idMasterOrg"] ? $object["idMasterOrg"] : $object["idOrg"];
			}
		}
		// IF we have AICC in the list of course objects... collect average scores for all AICC Items, in advance, use them later
		// This is so that we execute a single SQL query, not one per AICC LO
		if ($aiccLos) {
			$aiccItemsAvgScoresDataCommand = Yii::app()->db->createCommand()
				->select('track.idItem AS aiccIdItem, AVG(score_raw) AS avg_raw, AVG(score_max) as avg_max')
				->from('learning_aicc_item_track track')
				->join('learning_aicc_item item', 'item.id=track.idItem')
				->join('learning_aicc_package package', 'package.id=item.id_package')
				->where('(package.idReference IN(' .implode(',',$aiccLos). ')) OR (track.idOrg IN(' .implode(',',$aiccLos). '))')
				->group('track.idItem');

			if ($idUser) {
				$aiccItemsAvgScoresDataCommand->andWhere('track.idUser=:idUser');
				$aiccItemsAvgScoresDataCommand->params[':idUser'] = $idUser;
			}

			// Power User filtering
			if (Yii::app()->user->getIsPu()) {
				$aiccItemsAvgScoresDataCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (track.idUser = pu.user_id)");
			}

			$aiccItemsAvgScoresData = $aiccItemsAvgScoresDataCommand->queryAll();
			$aiccItemsAvgScores = array();
			foreach ($aiccItemsAvgScoresData as $aiccItemAvgScores) {
				$aiccItemsAvgScores[$aiccItemAvgScores["aiccIdItem"]]["avg_raw"] = $aiccItemAvgScores["avg_raw"];
				$aiccItemsAvgScores[$aiccItemAvgScores["aiccIdItem"]]["avg_max"] = $aiccItemAvgScores["avg_max"];
			}
		}




		$id = 0;
		//-- Enumerate records; outermost cycle
		//-- =========================================================================
		foreach ($objects as $object) {

			// If this LO hasn't been met yet, create new element
			if ( !isset($result[$object["idOrg"]]) ) {

				$id++;
				$stats 						= new ReportHelper();
				$stats->id 					= $id;
				$stats->title 				= $object["title"];
				$stats->objectType 			= $object["objectType"];
				$stats->is_folder 			= false;
				$stats->objectId 			= (int) $object["idResource"];  // <<-- this is very misleading !! Change please
				$stats->organizationId 		= (int) $object["idOrg"];
				$stats->totalItems 			= $enrolledUsersCount;  // Total number of enrolled users
				$stats->versionName 		= $object['version_name'] ? $object['version_name'] : "";
				$stats->participateInOtherCourses = isset($object["sharedTracking"]) ? $object['sharedTracking'] : 0;
				$stats->failedAttemptsTaken = $object['failed_attempts_taken'];
				$stats->failedAttemptsAllowed = $object['failed_attempts_allowed'];

				// Type specific Average scores
				switch($object["objectType"]) {

					case self::OBJECT_TYPE_TEST:
						$avgScoresCommand = Yii::app()->db->createCommand()
							->select('AVG(IFNULL(tt.score,0)+IFNULL(tt.bonus_score,0)) AS avg_raw')
							->from(LearningTesttrack::model()->tableName().' AS tt')
							->join(LearningCourseuser::model()->tableName().' AS cu', 'cu.idCourse = :idCourse and cu.idUser = tt.idUser', array(':idCourse' => $idCourse))
							->join(LearningCommontrack::model()->tableName().' AS ct', 'ct.idTrack = tt.idTrack AND ct.objectType=:objectTypeTest AND ct.idReference=:id', array(':objectTypeTest'=>LearningOrganization::OBJECT_TYPE_TEST))
							->where('tt.idReference=:id', array(':id'=> (int) ($object["idMasterOrg"] ? $object["idMasterOrg"] : $object["idOrg"])));

						if ($idUser) {
							$avgScoresCommand->andWhere('tt.idUser=:idUser');
							$avgScoresCommand->params[':idUser'] = $idUser;
						}

						// Power User filtering
						if ($pUserRights->isPu && !$pUserRights->isInstructor) {
							$avgScoresCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (tt.idUser = pu.user_id)");
						}
						if(!Yii::app()->user->getIsGodadmin() && $instructor && !empty($instructorUsers)) {
							$avgScoresCommand->andWhere('tt.idUser IN ('.implode(',', $instructorUsers).')');
						}

						$avgScores = $avgScoresCommand->queryAll();
						$testModel = LearningTest::model()->findByPk((int) $object["idResource"]);
						$stats->scores[] = array('raw' => $avgScores[0]['avg_raw'], 'max' => $testModel->getMaxScore());

						if($idUser){
							$testTrackModel = LearningTesttrack::model()->findByAttributes(array('idTest'=>$object["idResource"], 'idUser'=>$idUser));
							if($testTrackModel){
								$stats->times[] = (int)$testTrackModel->userTimeInTheTest();
							}
						}

						break;

					case self::OBJECT_TYPE_AICC:
						// NOTHING TO DO !!! We'have collected them earlier for ALL AICC LOs and all AICC items
						break;
					case self::OBJECT_TYPE_SCORMORG:
						// NOTHING TO DO !!! We'have collected them earlier for ALL ScormOrgs and all Scorm items
						break;

					case self::OBJECT_TYPE_DELIVERABLE:
						if($getLatest){
							$lastScoresCommand = Yii::app()->db->createCommand()
								->select('evaluation_score AS accepted_score')
								->from('learning_deliverable_object')
								->where('id_deliverable=:id AND evaluation_status = :accepted', array(':id'=> (int) $object["idResource"], ':accepted' => LearningDeliverableObject::STATUS_ACCEPTED))
								->order('evaluation_date desc')
								->limit(1);

							if ($idUser) {
								$lastScoresCommand->andWhere('learning_deliverable_object.id_user=:idUser');
								$lastScoresCommand->params[':idUser'] = $idUser;
							}

							// Power User filtering
							if ($pUserRights->isPu && !$pUserRights->isInstructor) {
								$lastScoresCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (learning_deliverable_object.id_user = pu.user_id)");
							}
							if(!Yii::app()->user->getIsGodadmin() && $instructor && !empty($instructorUsers)) {
								$lastScoresCommand->andWhere('learning_deliverable_object.id_user IN ('.implode(',', $instructorUsers).')');
							}

							$score = $lastScoresCommand->queryAll();
							if ($score) {
								$stats->scores[] = array('raw' => $score[0]['accepted_score'], 'max' => 100);
							}
							else {
								$stats->scores[] = array('raw'=>0, 'max'=>0);
							}

							break;
						}else{

							if($idUser){
								$sql = 'SELECT idObject  FROM learning_deliverable_object
											WHERE id_deliverable= :id AND evaluation_status = :accepted AND id_user = :idUser
											ORDER BY evaluation_date DESC';
							}else{
								$sql = 'SELECT idObject FROM learning_deliverable_object
									WHERE evaluation_date IN(SELECT  MAX(evaluation_date) AS max_date FROM learning_deliverable_object
											WHERE id_deliverable= :id AND evaluation_status = :accepted
											GROUP  BY id_user)';
							}

							$subQuery = Yii::app()->db->createCommand($sql);
							$avgScoresIds = $subQuery->text;
							$avgScoresCommand = Yii::app()->db->createCommand()
								->select(' AVG(evaluation_score)  AS avg_raw ')
								->from('learning_deliverable_object')
								->where("idObject IN ($avgScoresIds)");


							if ($idUser) {
								$avgScoresCommand->andWhere('learning_deliverable_object.id_user=:idUser');
								$avgScoresCommand->bindValues(array(':id'=> (int) $object["idResource"],
									':accepted' => LearningDeliverableObject::STATUS_ACCEPTED,
									':idUser' => $idUser
								));
							}else{
								$avgScoresCommand->bindValues(array(':id'=> (int) $object["idResource"], ':accepted' => LearningDeliverableObject::STATUS_ACCEPTED));
							}

							// Power User filtering
							if ($pUserRights->isPu && !$pUserRights->isInstructor) {
								$avgScoresCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (learning_deliverable_object.id_user = pu.user_id)");
							}
							if(!Yii::app()->user->getIsGodadmin() && $instructor && !empty($instructorUsers)) {
								$avgScoresCommand->andWhere('learning_deliverable_object.id_user IN ('.implode(',', $instructorUsers).')');
							}

							$avgScores = $avgScoresCommand->queryAll();

							if ($avgScores) {
								$stats->scores[] = array('raw' => $avgScores[0]['avg_raw'], 'max' => 100);
							}
							else {
								$stats->scores[] = array('raw'=>0, 'max'=>0);
							}

							break;
						}





					// By default, collect scores from learning_commontrack table
					case self::OBJECT_TYPE_TINCAN:
					case self::OBJECT_TYPE_ELUCIDAT:
					default:
						$avgScoresCommand = Yii::app()->db->createCommand()
							->select('AVG(score) AS avg_raw, AVG(score_max) AS avg_max')
							->from('learning_commontrack')
							->where('idReference=:id', array(':id'=> (int) $object["idOrg"]));

						if ($idUser) {
							$avgScoresCommand->andWhere('learning_commontrack.idUser=:idUser');
							$avgScoresCommand->params[':idUser'] = $idUser;
						}

						// Power User filtering
						if ($pUserRights->isPu && !$pUserRights->isInstructor) {
							$avgScoresCommand->join(CoreUserPU::model()->tableName()." pu", "(pu.puser_id = " . (int) Yii::app()->user->id . ") AND (learning_commontrack.idUser = pu.user_id)");
						}
						if(!Yii::app()->user->getIsGodadmin() && $instructor && !empty($instructorUsers)) {
							$avgScoresCommand->andWhere('learning_commontrack.idUser IN ('.implode(',', $instructorUsers).')');
						}

						$avgScores = $avgScoresCommand->queryAll();
						if ($avgScores) {
							$stats->scores[] = array('raw' => $avgScores[0]['avg_raw'], 'max' => $avgScores[0]['avg_max']);
						}
						else {
							$stats->scores[] = array('raw'=>0, 'max'=>0);
						}


						break;
				}

			}
			// ... otherwise, use the previous one (element/row in the list) and update with new info
			else {
				$stats = $result[$object["idOrg"]];
			}

			$statusCount = intval($object["statusCount"]);
			switch ($object["status"]) {
				case LearningCommontrack::STATUS_AB_INITIO:
					$stats->itemsInitState = $statusCount;
					break;
				case LearningCommontrack::STATUS_ATTEMPTED:
					$stats->itemsAttempted = $statusCount;
					break;
				case LearningCommontrack::STATUS_COMPLETED:
				case LearningTesttrack::STATUS_PASSED:
					$stats->itemsCompleted = $statusCount;
					break;
				case LearningCommontrack::STATUS_FAILED:
					$stats->itemsFailed = $statusCount;
					break;

			}

			// Add LO in the list of objects
			$result[$object["idOrg"]] = $stats;


			// Next are very specific for SCORM AND AICC (building SCORM/AICC LO Tree in the report)

			// SCORM:::: If this LO is of type SCORMORG and not yet traversed down, traverse and add its children (SCO, Items)
			if ($object["objectType"] == self::OBJECT_TYPE_SCORMORG && !isset($scormOrgResolved[$object["idOrg"]])) {

				$scormOrgResolved[$object["idOrg"]] = true;

				$commandSco = Yii::app()->db->createCommand();

				$commandSco->select('scormitems.idscorm_item, scormitems.idscorm_parentitem, scormitems.title, scormitems.idscorm_resource,
									scormitemstrack.status as trackingStatus, count(scormitemstrack.status) AS statusCount');

				$commandSco->from('learning_scorm_items scormitems');
				$commandSco->where('scormitems.idscorm_organization = :idResource');
				$commandSco->group('scormitems.idscorm_item,scormitemstrack.status');

				if ($idUser) {
					$commandSco->leftJoin('learning_scorm_items_track scormitemstrack', '(scormitems.idscorm_item=scormitemstrack.idscorm_item) AND (scormitemstrack.idUser=:idUser)');
					$commandSco->andWhere('(scormitemstrack.idUser=:idUser) OR (scormitemstrack.idUser IS NULL)');
					$commandSco->params[':idUser'] = $idUser;
				}
				else {
					$commandSco->leftJoin('learning_scorm_items_track scormitemstrack', 'scormitems.idscorm_item=scormitemstrack.idscorm_item');
				}

				$commandSco->join(LearningCourseuser::model()->tableName().' AS cu', 'cu.idCourse = :idCourse AND cu.idUser = scormitemstrack.idUser');
				$commandSco->params[':idCourse'] = $idCourse;

				// Power User filtering
				if (Yii::app()->user->getIsAdmin()) {
					$commandSco->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND cu.idUser = pu.user_id");
				}


				$commandSco->params[':idResource'] = $object["idResource"];

				// List of scorm items; one roe per item per status
				$scormItems = $commandSco->queryAll();
				$scormItemsTreeLevels = self::_calculateScormItemsTreeLevels($scormItems);

				foreach ($scormItems as $scormItem) {
					$resultKey = $object["idOrg"] . "_" . $scormItem["idscorm_item"];
					if (!isset($result[$resultKey])) {
						$id++;
						$scormItemStats = new ReportHelper();
						$scormItemStats->id = $id;
						$scormItemStats->title = $scormItem["title"];
						$scormItemStats->is_folder = $scormItem["idscorm_resource"] ? false : true;
						$scormItemStats->objectId = $scormItem["idscorm_item"];
						$scormItemStats->organizationId = $object["idOrg"];
						$scormItemStats->totalItems = $enrolledUsersCount;

						// All Scorm Children Items should inherit hi's parent's value for participateInOtherCourses
						if (isset($result[$object['idOrg']])) {
							$parent = $result[$object['idOrg']];
							$scormItemStats->participateInOtherCourses = $parent->participateInOtherCourses;
						}

						//assign sco item node tree level
						if (isset($scormItemsTreeLevels[$scormItem["idscorm_item"]])) {
							$scormItemStats->scormItemTreeLevel = $scormItemsTreeLevels[$scormItem["idscorm_item"]];
						}

						// Use Scorm Items average scores collected earlier
						$scormItemStats->scores[] = array('raw' => $scormItemsAvgScores[$scormItem["idscorm_item"]]["avg_raw"], 'max' => $scormItemsAvgScores[$scormItem["idscorm_item"]]["avg_max"]);

						if($scormItem["idscorm_item"] && $idUser){
							$itemModel = LearningScormTracking::model()->findByAttributes(array('idscorm_item'=>$scormItem["idscorm_item"], 'idUser'=>$idUser));
							if($itemModel){
								$scormItemStats->times[] = $itemModel->getTotalTimeInSeconds();
					}
						}

					}
					else {
						$scormItemStats = $result[$resultKey];
					}

					$statusCount = intval($scormItem["statusCount"]);
					switch ($scormItem["trackingStatus"]) {
						case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
							$scormItemStats->itemsInitState = $statusCount;
							break;
						case LearningScormItemsTrack::STATUS_INCOMPLETE:
							$scormItemStats->itemsAttempted = $statusCount;
							break;
						case LearningScormItemsTrack::STATUS_FAILED:
							$scormItemStats->itemsFailed = $statusCount;
							break;
						case LearningScormItemsTrack::STATUS_COMPLETED:
						case LearningScormItemsTrack::STATUS_PASSED:
							$scormItemStats->itemsCompleted = $statusCount;
							break;
					}

					// Add to output list
					$result[$resultKey] = $scormItemStats;
				}
 			}


 			// AICC:::: If this LO is of type AICC and not yet traversed down, traverse and add its children (SCO, Items)
 			if ($object["objectType"] == self::OBJECT_TYPE_AICC && !isset($aiccOrgResolved[$object["idOrg"]])) {

 				$aiccOrgResolved[$object["idOrg"]] = true;

 				$command = Yii::app()->db->createCommand();

 				$command->select('item.*, item.id AS itemId, track.lesson_status as trackingStatus, count(track.lesson_status) AS statusCount');

 				$command->from('learning_aicc_item item');

 				// Exclude BLOCKS
 				$command->where("(item.id_package = :idResource) AND (item.parent <> '/' ) AND (item.item_type <> :block)", array(':block' => LearningAiccPackage::ITEM_TYPE_BLOCK));

 				// For now, we also exclude OBJECTIVES from list of AICC items to show in reports
 				$command->andWhere("item.item_type <> :objective", array(':objective' => LearningAiccPackage::ITEM_TYPE_OBJECTIVE));

 				$command->group('item.id, track.lesson_status');

 				if ($idUser) {
 					$command->leftJoin('learning_aicc_item_track track', '(item.id=track.idItem) AND (track.idUser=:idUser)');
 					$command->andWhere('(track.idUser=:idUser) OR (track.idUser IS NULL)');
 					$command->params[':idUser'] = $idUser;
 				}
 				else {
 					$command->leftJoin('learning_aicc_item_track track', 'item.id=track.idItem');
 				}

 				// Power User filtering
 				if (Yii::app()->user->getIsAdmin()) {
 					$command->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND (track.idUser = pu.user_id) OR (track.idUser IS NULL)");
 				}


 				$command->params[':idResource'] = $object["idResource"];

 				// List of scorm items; one roe per item per status
 				$aiccItems = $command->queryAll();


 				// @TODO This method is not yet working properly, so we show the AICC content as a FLAT list of items
 				// $aiccItemsTreeLevels = self::_calculateAiccItemsTreeLevels($aiccItems);
 				$aiccItemsTreeLevels = array();

 				foreach ($aiccItems as $aiccItem) {
 					$resultKey = $object["idOrg"] . "_aicc_" . $aiccItem["itemId"];
 					if (!isset($result[$resultKey])) {
 						$id++;
 						$aiccItemStats = new ReportHelper();
 						$aiccItemStats->isAiccItem = true;
 						$aiccItemStats->id = $id;
 						$aiccItemStats->title = $aiccItem["title"];

 						// ????  is_folder (of an AICC item) should be resolved at PACKAGE EXTRACTION stage!!!
 						$aiccItemStats->is_folder = $aiccItem['item_type'] == LearningAiccPackage::ITEM_TYPE_BLOCK;

 						$aiccItemStats->objectId = $aiccItem["itemId"];
 						$aiccItemStats->organizationId = $object["idOrg"];
 						$aiccItemStats->totalItems = $enrolledUsersCount;

						// All Scorm Children Items should inherit hi's parent's value for participateInOtherCourses
						if (isset($result[$object['idOrg']])) {
							$parent = $result[$object['idOrg']];
							$aiccItemStats->participateInOtherCourses = $parent->participateInOtherCourses;
						}

 						//assign item node tree level
 						if (isset($aiccItemsTreeLevels[$aiccItem["itemId"]])) {
 							// Yes, "scorm" is correct, it is just an attribute to hold object levels, could be SCORM, could be AICC
 							// Just bad naming
 							$aiccItemStats->scormItemTreeLevel = $aiccItemsTreeLevels[$aiccItem["itemId"]];
 						}

 						// Use Scorm Items average scores collected earlier
 						$aiccItemStats->scores[] = array('raw' => $aiccItemsAvgScores[$aiccItem["itemId"]]["avg_raw"], 'max' => $aiccItemsAvgScores[$aiccItem["itemId"]]["avg_max"]);
 					}
 					else {
 						$aiccItemStats = $result[$resultKey];
 					}

 					$statusCount = intval($aiccItem["statusCount"]);
 					switch ($aiccItem["trackingStatus"]) {
 						case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
 							$aiccItemStats->itemsInitState = $statusCount;
 							break;
 						case LearningScormItemsTrack::STATUS_INCOMPLETE:
 							$aiccItemStats->itemsAttempted = $statusCount;
 							break;
 						case LearningScormItemsTrack::STATUS_FAILED:
 							$aiccItemStats->itemsFailed = $statusCount;
 							break;
 						case LearningScormItemsTrack::STATUS_COMPLETED:
 						case LearningScormItemsTrack::STATUS_PASSED:
 							$aiccItemStats->itemsCompleted = $statusCount;
 							break;
 					}

 					// Add to output list
 					$result[$resultKey] = $aiccItemStats;

 				}
 			}




		}
		//-- ===========================



		// Adjust "to begin" users by number of not involved users
		foreach ($result as $index => $tmp) {
			$statusedUsers 			= $tmp->itemsInitState + $tmp->itemsAttempted + $tmp->itemsCompleted + $tmp->itemsFailed + $tmp->itemsPassed;
			$tmp->itemsInitState 	= $tmp->itemsInitState + $enrolledUsersCount - $statusedUsers;
			$result[$index] = $tmp;
		}

		// Reindex the output array, si index starts from Zero etc. It might be important and it WAS like that before.
		$result = array_values($result);

		if( $onlyWithPositiveStatus ){
			$tmpArray = array();
			foreach($result as $key => $value){
                $tmpArray[$key]['object'] = $value;
				if($value->itemsCompleted) {
                    $tmpArray[$key]['resultType'] = 'icon-completed green';
				}
                else if($value->itemsAttempted )
                {
                    $tmpArray[$key]['resultType'] = 'icon-play-circle yellow';
                }
                else if($value->itemsFailed)
                {
                    $tmpArray[$key]['resultType'] = 'failed';
                }
                else if($value->itemsPassed)
                {
                    $tmpArray[$key]['resultType'] = 'icon-completed green';
                } else {
                    $tmpArray[$key]['resultType'] = 'icon-play-circle gray';
                }
			}
			return $tmpArray;
		}

		return $result;

	}


	protected static function _calculateScormItemsTreeLevels($scormItems) {
		if (empty($scormItems)) { return array(); }

		//first collect parent itemsreferences
		$parents = array();
		foreach ($scormItems as $scormItem) {
			$idScormItem = (int)$scormItem['idscorm_item'];
			$parents[$idScormItem] = (int)$scormItem['idscorm_parentitem'];
		}

		//then calculate level for each sco object
		$output = array();
		foreach ($scormItems as $scormItem) {
			$idScormItem = (int)$scormItem['idscorm_item'];
			$output[$idScormItem] = 1; //initialize level number
			if (isset($parents[$idScormItem])) {
				if ($parents[$idScormItem] <= 0) {
					//this is for sure a first level node
					continue;
				} else {
					//we have to calculate the node level iterating through ancestors
					$tmpParent = $idScormItem;
					do {
						$output[$idScormItem]++;
						$tmpParent = $parents[$tmpParent];
					} while(isset($parents[$tmpParent]) && $parents[$tmpParent] > 0);
				}
			}
		}

		return $output;
	}


	/**
	 * //
	 * // @TODO This method is not yet working properly! DO NOT USE IT !!!
	 * //
	 *
	 *
	 * @param unknown $items
	 */
	protected static function _calculateAiccItemsTreeLevels($items) {
		if (empty($items)) { return array(); }

		//first collect parent itemsreferences
		$parents = array();
		foreach ($items as $item) {
			$idAiccItem = (int)$item['itemId'];
			$parents[$idAiccItem] = $item['parent'];
		}

		//then calculate level for each sco object
		$output = array();
		foreach ($items as $item) {
			$idAiccItem = (int)$item['itemId'];
			$output[$idAiccItem] = 1; //initialize level number
			if (isset($parents[$idAiccItem])) {
				if ($parents[$idAiccItem] != '') {
					$tmpParent = $idAiccItem;
					do {
						$output[$idAiccItem]++;
						$tmpParent = $parents[$tmpParent];
					} while(isset($parents[$tmpParent]) && $parents[$tmpParent] > 0);
				}
			}
		}

		return $output;
	}



	/**
	 * Get learning items collected in array of objects
	 * @param $courseId
	 * @param null $userId
	 * @return array
	 */
	public function getLearningArrayOld($courseId, $userId = null)
	{
		$criteria = $this->getLearningObjectsCriteria($courseId, $userId);
		if ($userId !== null) {
			$enrolledUsers[$userId] = true;
		}
		else
			$enrolledUsers = CoreUser::getAllUsersByCourse($courseId);

		// [DZ] We query the scorm trackings via QueryBuilder below
		// which lowers memory usage around 10 fold
		if(isset($criteria->with['learningScormTracking']))
			unset($criteria->with['learningScormTracking']);

		$models = self::model()->findAll($criteria);

		$result = array();
		$id = 1;
		foreach ($models as $model)
		{
			$tmpUsers = $enrolledUsers;
			if ($model->objectType) // skip folders
			{
				$stats = new ReportHelper();
				$stats->id = $id;
				$stats->title = $model->title;
				$stats->objectType = $model->objectType;
				$stats->is_folder = false;
				$stats->objectId = $model->idResource;
				$stats->organizationId = $model->idOrg;

				foreach ($model->learningCommontracks as $commonItem)
				{
					$stats->totalItems++;
					switch ($commonItem->status)
					{
						case LearningCommontrack::STATUS_AB_INITIO:
							$stats->itemsInitState++;
							break;
						case LearningCommontrack::STATUS_ATTEMPTED:
							$stats->itemsAttempted++;
							break;
						case LearningCommontrack::STATUS_COMPLETED:
							$stats->itemsCompleted++;
							break;
						case LearningCommontrack::STATUS_FAILED:
							$stats->itemsFailed++;
							break;
					}
					unset($tmpUsers[$commonItem->idUser]);
				}
				switch($model->objectType)
				{
					case self::OBJECT_TYPE_TEST:
						foreach ($model->learningTesttracks as $testTrack)
						{
							$test = $testTrack->test;
							$stats->scores[] = array('raw' => $testTrack->getCurrentScore(), 'max' => $test->getMaxScore());
							$stats->times[] = (int)$testTrack->userTimeInTheTest();
						}
						break;
					case self::OBJECT_TYPE_TINCAN:
						foreach ($model->learningCommontracks as $commonItem)
							$stats->scores[] = array('raw' => $commonItem->score, 'max' => $commonItem->score_max);
						break;
					case self::OBJECT_TYPE_SCORMORG:
						// Using Query Builder here instead of AR relations
						// for performance
						$scormTrackings = Yii::app()->db->createCommand()
							->select('score_raw, score_max, idReference')
							->from(LearningScormTracking::model()->tableName())
							->where('idReference=:id', array(':id'=>$model->getPrimaryKey()))
							->queryAll(true);
						foreach ($scormTrackings as $scormTracking)
							$stats->scores[] = array('raw' => $scormTracking['score_raw'], 'max' => $scormTracking['score_max']);
						break;
					case self::OBJECT_TYPE_DELIVERABLE:
                        $scoreModel = LearningDeliverable::model()->findByPk($model->idResource);
                        if ($scoreModel) {
                        	$stats->scores[] = $scoreModel->getObjectScores($userId);
                        }
						break;
					default:
						$stats->scores[] = array('raw'=>0, 'max'=>0);
						//$stats->times[] = (int)$commonItem->total_time;
						$stats->times[] = 0; //for now add only test and scorm times to the report
				}

				//add as "not started" all enrolled users that are not tracked
				$usersNotInvolved = count($tmpUsers);
				if ($usersNotInvolved > 0)
				{
					$stats->totalItems += $usersNotInvolved;
					$stats->itemsInitState += $usersNotInvolved;
				}
				$result[] = $stats;
				if ($model->objectType == self::OBJECT_TYPE_SCORMORG)
				{
					$id++;
					$itemModel = new LearningScormItems();
					$items = $itemModel->findAll($itemModel->getItemsCriteria($model->idResource, $userId));
					foreach ($items as $item)
					{
						$tmpUsers = $enrolledUsers;
						$stats = new ReportHelper();
						// $stats->objectType = 'sco'; // This will break more things than it will fix
						$stats->id = $id;
						$stats->title = $item->title;
						$stats->is_folder = $item->idscorm_resource ? false : true;
						$stats->objectId = $item->idscorm_item;
						$stats->organizationId = $model->idOrg;
						foreach ($item->scormItemsTrack as $itemTrack)
						{
							$stats->totalItems++;
							switch ($itemTrack->status)
							{
								case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED:
									$stats->itemsInitState++;
									break;
								case LearningScormItemsTrack::STATUS_INCOMPLETE:
									$stats->itemsAttempted++;
									break;
								case LearningScormItemsTrack::STATUS_FAILED:
									$stats->itemsFailed++;
									break;
								case LearningScormItemsTrack::STATUS_COMPLETED:
									$stats->itemsCompleted++;
									break;
								case LearningScormItemsTrack::STATUS_PASSED:
									$stats->itemsPassed++;
									break;
							}
							$stats->times[] = $itemTrack->scormItemsTracking ? (int)$itemTrack->scormItemsTracking->getTotalTimeInSeconds() : 0;
							$stats->scores[] = array('raw'=>(int)$itemTrack->scormItemsTracking->score_raw, 'max'=>(int)$itemTrack->scormItemsTracking->score_max);
							unset($tmpUsers[$commonItem->idUser]);
						}
						//add as "not started" all enrolled users that are not tracked
						$usersNotInvolved = count($tmpUsers);
						if ($usersNotInvolved > 0)
						{
							$stats->totalItems += $usersNotInvolved;
							$stats->itemsInitState += $usersNotInvolved;
						}
						$result[] = $stats;
					}
				}
				$id++;
			}
		}
		return $result;
	}
	/**
	 * Get user timeline data for a given course
	 * @param $idCourse
	 * @param $idUser
	 * @return array
	 */
	public static function getTimelineData($idCourse, $idUser)
	{
		$criteria = new CDbCriteria();
		$criteria->with = array('learningCommontrack');
		$criteria->together = true;
		$criteria->condition = 'learningCommontrack.idUser = :idUser AND t.objectType != "" AND t.idCourse = :idCourse';
		$criteria->params = array(':idUser' => $idUser, ':idCourse' => $idCourse);
		$criteria->order = 'learningCommontrack.dateAttempt ASC';

		$models = self::model()->findAll($criteria);
		$activityData = array();

		foreach ($models as $model)
		{
			$activity = array(
				'start'=> Yii::app()->localtime->fromLocalDateTime($model->learningCommontrack->firstAttempt, 'short', 'medium', null, true), // The timeline accepts datetime in mysql format
				'content'=>$model->title,
				'isKeyEvent'=>false
			);
			if (($model->learningCommontrack->status == 'completed' || $model->learningCommontrack->status == 'passed') && !empty($model->first_complete)) {
				$endDate = $model->first_complete;
				//if difference is not greater than 6 hours, only display the start date
                $utcEndDate = Yii::app()->localtime->fromLocalDateTime($endDate);
                $utcStartDate = Yii::app()->localtime->fromLocalDateTime($activity['start']);
				$diff = strtotime($utcEndDate) - strtotime($utcStartDate);
				$hours = (float) ($diff / 3600.0);
				if ($hours >= 6) {
					$activity['end'] = Yii::app()->localtime->fromLocalDateTime($endDate, 'short', 'medium', null, true);
				}
			}
			$activityData[] = $activity;
		}

        $criteria = new CDbCriteria();
		$criteria->condition = 't.idUser = :idUser AND t.idCourse = :idCourse';
		$criteria->params = array(':idUser' => $idUser, ':idCourse' => $idCourse);
		$criteria->with = array('user');
		$criteria->together = true;
		$model = LearningCourseuser::model()->find($criteria);

		if ($model)
		{
			array_unshift($activityData, array(
				'start'=> Yii::app()->localtime->fromLocalDateTime($model->date_inscr, 'short', 'medium', null, true),
				'content'=>Yii::t('standard', '_USER_STATUS_SUBS'),
				'isKeyEvent'=>true
			));
			array_push($activityData, array(
				'start'=> Yii::app()->localtime->fromLocalDateTime((!empty($model->date_complete)) ? $model->date_complete : $model->user->lastenter, 'short', 'medium', null, true),
				'content'=>Yii::t('standard', '_DATE_LAST_ACCESS'),
				'isKeyEvent'=>true
			));
		}


		$minDate = $activityData[0]['start'];

        if (strcmp($minDate, $activityData[1]['start'])  > 0)
            $minDate = $activityData[1]['start'];

        $maxDate = $activityData[count($activityData)-1]['start'];

		if (strcmp($minDate, $activityData[count($activityData)-1]['start']) < 0)
            $maxDate = $activityData[count($activityData)-1]['start'];

		$timeline = array(
			'minDate' => $minDate,
			'maxDate' => $maxDate,
			'data' => $activityData
		);

		return $timeline;
	}

	/**
	 * Return True if LO is of type FOLDER
	 *
	 * @return boolean
	 */
	public function isFolder() {
		return $this->objectType == '';
	}



	public function beforeValidate() {
		$this->setDefaultValues();
		return parent::beforeValidate();
	}


	public function beforeSave() {
		// Convert 0000 and empty string dates to NULL

		// BIG NOTE ON 'publish_from', 'publish_to' [Dzhuneyt] !!!!!!!!!!
		// Below we just convert the format of the UTC "start of day" and "end of day" of 'publish_from'
		// and 'publish_to' to local format without going
		// through Yii::app()->localtime->toLocalDateTime(), because that function will
		// offset the UTC time using the local time zone and we don't want that. The field should
		// remain/exit from this function in the model as "[date] 00:00:00" (in local formatting)
		// because the LocalTimeConversionBehavior will do the local time zone to UTC offsetting
		// and converting to ISO format in beforeSave()
        if ($this->publish_from) {
	        $utcPublishFrom = Yii::app()->localtime->fromLocalDate(Yii::app()->localtime->stripTimeFromLocalDateTime($this->publish_from), 'short', 'medium', null, true);
	        $utcPublishFromStartOfDay = $utcPublishFrom . ' 00:00:00';

	        $localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
	        $this->publish_from = date($localFormat, strtotime($utcPublishFromStartOfDay));
        } else
            $this->publish_from = null;

        if ($this->publish_to) {
	        $utcPublishFrom = Yii::app()->localtime->fromLocalDate(Yii::app()->localtime->stripTimeFromLocalDateTime($this->publish_to), 'short', 'medium', null, true);
	        $utcPublishFromStartOfDay = $utcPublishFrom . ' 23:59:59';

	        $localFormat = Yii::app()->localtime->getPHPLocalDateTimeFormat('short', 'medium');
	        $this->publish_to = date($localFormat, strtotime($utcPublishFromStartOfDay));
        } else
            $this->publish_to = null;

		// Encode virtual properties into single JSON data field
		$params = array(
			'openmode' 				=> $this->openmode,
		    'autoplay'				=> $this->autoplay,
			'desktop_openmode'		=> $this->desktop_openmode,
			'tablet_openmode'		=> $this->tablet_openmode,
			'smartphone_openmode'	=> $this->smartphone_openmode,
		    'enable_oauth'    		=> $this->enable_oauth,
		    'oauth_client'       	=> $this->oauth_client,
		    'use_xapi_content_url'  => $this->use_xapi_content_url,
		);
		if($this->saveViewMode){
			$this->params_json = CJSON::encode($params);
		} else{
			$this->params_json = null;
		}

		$this->setDefaultValues();

		// We can have ONLY ONE LO bookmarked as "end" per course
		// Check if THIS LO (that is being saved) is "end"-bookmarked.
		// If Yes, then remove "end" bookmarks (milestone field) from ALL OTHER LOs for the same course
		if ($this->milestone == self::MILESTONE_END) {
			self::model()->updateAll(array("milestone" => "-"), "idCourse=:idCourse AND milestone=:endMilestone", array(
				":idCourse" => $this->idCourse,
				":endMilestone" => self::MILESTONE_END,
			));
		}

		return parent::beforeSave();
	}


	public function afterSave(){

		// See CoreNotification/TransportAgent for events
		// Raise event that a new LO is added to a course
		if($this->getIsNewRecord() && $this->scenario == 'insert'){

			// Don't raise the event for LO folders only, all others raise it
			if(!empty($this->objectType)){
                //start event on successful LO adding
				Yii::app()->event->raise('NewLoAddedCourse', new DEvent($this, array(
					'course' => $this->course, // AR relation
					'learningobject' => $this,
					'user' => Yii::app()->user->loadUserModel(),
				)));
			}
		} elseif ($this->scenario == 'update') {
            //it is update of existing LO
            // Don't raise the event for LO folders only, all others raise it
            if(!empty($this->objectType)){
                //start event on successful LO update
                /* TODO Fixme do not uncomment until you know what you are doing ( @stone )
                Yii::app()->event->raise('EditedCourseLo', new DEvent($this, array(
                    'course' => $this->course, // AR relation
                    'learningobject' => $this,
                    'user' => CoreUser::model()->findByPk(Yii::app()->user->getId()),
                )));

                Yii::log("Raised event EditedCourseLo for course {$this->course->idCourse} and LearningOrganization {$this->idOrg}", 'info', __CLASS__);
                */
            }
        }

        Yii::app()->event->raise(EventManager::EVENT_LO_AFTER_SAVE, new DEvent($this, array(
       		'course' => $this->course, // AR relation
       		'learningobject' => $this,
       		'user' => Yii::app()->user->loadUserModel(),
        )));


		return parent::afterSave();
	}


	public function afterFind() {
		// Decode the JSON-params field and assign values to virtual properties
		if (empty($this->params_json)) {
			$this->params_json = "{}";
		}
		$params = CJSON::decode($this->params_json);
		$this->openmode 			= isset($params['openmode']) ? $params['openmode'] : null;
		$this->autoplay 			= isset($params['autoplay']) && $params['autoplay'] != '0' ? $params['autoplay'] : self::AUTOPLAY_YES;
		$this->desktop_openmode 	= isset($params['desktop_openmode']) ? $params['desktop_openmode'] : null;
		$this->tablet_openmode 		= isset($params['tablet_openmode']) ? $params['tablet_openmode'] : null;
		$this->smartphone_openmode 	= isset($params['smartphone_openmode']) ? $params['smartphone_openmode'] : null;
		$this->enable_oauth  		= isset($params['enable_oauth']) ? $params['enable_oauth'] : false;
		$this->oauth_client    		= isset($params['oauth_client']) ? $params['oauth_client'] : null;
		$this->use_xapi_content_url = isset($params['use_xapi_content_url']) ? $params['use_xapi_content_url'] : null;
		
		$this->setDefaultValues();

		return parent::afterFind();
	}



	/**
	 * Sets some default values if attributes are NULL
	 */
	public function setDefaultValues() {

		if ($this->visible === null)
			$this->visible = 1;

		if ($this->self_prerequisite === null)
			$this->self_prerequisite = self::SELF_PREREQ_DEFAULT;

		if ($this->isTerminator === null)
			$this->isTerminator = 0;   // NO

		if ($this->milestone === null)
			$this->milestone = self::MILESTONE_DEFAULT;

		if ($this->autoplay === null)
			$this->autoplay = self::AUTOPLAY_YES;

		if ($this->openmode === null) {
			if (isset(self::$DEFAULT_OPENMODES[$this->objectType])) {
				$this->openmode = self::$DEFAULT_OPENMODES[$this->objectType];
			}
			else {
				$this->openmode = self::OPENMODE_DEFAULT;
			}
		}

		if ($this->width === null)
			$this->width = '100%';

		if ($this->height === null)
			$this->height = '0';

	}

	public static function getProgressByUserAndCourseNew($courseId, $userId)
	{
		// Some cache
		static $learningOrganization = null;
		if($learningOrganization===null)
			$learningOrganization = new LearningOrganization();

		$criteria = $learningOrganization->getLearningObjectsCriteria($courseId, null);
		$criteria->with = null;

		static $models = null;
		if($models===null)
			$models = self::model()->findAll($criteria);

		$completed = 0;
		$totalItems = 0;

		foreach ($models as $model)
		{

			$totalItems++;

			if ($model->objectType) // skip folders
			{

				foreach ($model->learningCommontracks as $commonItem)
				{
					// We get stats for all users
					// and then filter them here
					if($commonItem->idUser != $userId)
						continue;

					if($commonItem->status == LearningCommontrack::STATUS_COMPLETED){
						$completed++;
					}
				}

				if ($model->objectType == self::OBJECT_TYPE_SCORMORG)
				{
					$itemModel = new LearningScormItems();
					$scormItemsCriteria = $itemModel->getItemsCriteria($model->idResource, $userId);

					$items = $itemModel->findAll($scormItemsCriteria);
					if(!empty($items)) {
						if(count($items)>1){
							$totalItems--; // Decrease by one to avoid counting objects twice (the entire scorm + N SCO chapters)
						}


						foreach ($items as $item)
						{
							$totalItems++;
							foreach ($item->scormItemsTrack as $itemTrack)
							{
								if($itemTrack->status==LearningScormItemsTrack::STATUS_COMPLETED){
									$completed++;
								}
							}
						}
					}

				}
			}
		}

		if($totalItems)
			return round(($completed * 100) / $totalItems);
		else
			return 0;
	}

	public static function getCompletedAndTotalLOsCount($courseId, $userId) {
		$learningOrganization = new LearningOrganization();

		$criteria = $learningOrganization->getLearningObjectsCriteria($courseId, $userId, true);
		$criteria->addCondition('visible = 1');

		$models = self::model()->findAll($criteria);

		$result = array(
			'completed' => 0,
			'totalItems' => 0
		);

		foreach ($models as $model)
		{
			$result['totalItems']++;
			if ($model->objectType) // skip folders
			{
				foreach ($model->learningCommontracks as $commonItem)
				{
					if($commonItem->status == LearningCommontrack::STATUS_COMPLETED){
						$result['completed']++;
					}
				}

			}
		}

		return $result;
	}

	public static function getProgressByUserAndCourse($courseId, $userId)
	{
		$items = self::getCompletedAndTotalLOsCount($courseId, $userId);

		if($items['totalItems'])
			return round(($items['completed'] * 100) / $items['totalItems']);
		else
			return 0;
	}


	/**
	 * return an array with the progress of all users, enrolled to a course
	 * array(
	 *    id_user => progress,
	 *    ...
	 * )
	 *
	 * @param int $courseId
	 * @return array
	 */
	public static function getProgressByCourse($courseId) {
		//count all completed LOs for each user, enrolled in a given course
		$results = Yii::app()->db->createCommand()
			->select('ct.idUser, COUNT(lo.idOrg) as countCompleted')
			->from(LearningOrganization::model()->tableName().' lo')
			->leftJoin(LearningCommontrack::model()->tableName().' ct', 'ct.idReference = lo.idOrg')
			->where("lo.visible = :visible AND lo.objectType != '' AND lo.idCourse = :idCourse AND (ct.status=:statusCompleted OR ct.status=:statusPassed)", array(
				':visible' => 1,
				':idCourse' => $courseId,
				':statusCompleted' => LearningCommontrack::STATUS_COMPLETED,
				':statusPassed' => LearningCommontrack::STATUS_PASSED
			))
			->group('ct.idUser')
			->queryAll();

		$completedLOs = array();
		foreach($results as $result) {
			$completedLOs[$result['idUser']] = $result['countCompleted'];
		}

		//total learning objects
		$totalLos = Yii::app()->db->createCommand()
			->select('COUNT(lo.idOrg)')
			->from(LearningOrganization::model()->tableName().' lo')
			->where("lo.visible = 1 AND lo.objectType != '' AND lo.idCourse = :idCourse", array(
				':idCourse' => $courseId
			))
			->queryScalar();

		//users enrolled to the course
		$enrolledUsers = array();
		$command = Yii::app()->db->createCommand()
			->select('cu.idUser')
			->from(LearningCourseuser::model()->tableName().' cu')
			->where('cu.idCourse = :idCourse', array(':idCourse' => $courseId));

		//power user filtering
		if(Yii::app()->user->getIsPu()){
			$command->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = :userId AND cu.idUser = pu.user_id", array(
				':userId' => Yii::app()->user->id
			));
			$command->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = :userId AND puc.course_id = cu.idCourse", array(
				':userId' => Yii::app()->user->id
			));
		}
		$results = $command->queryAll();

		//calculate the progress
		foreach($results as $result) {
			if($completedLOs[$result['idUser']])
				$progress = round(($completedLOs[$result['idUser']] * 100) / $totalLos);
			else
				$progress = 0;

			$enrolledUsers[$result['idUser']] = $progress;
		}

		return $enrolledUsers;
	}

	/**
	 * Return translated learning object open/view mode
	 *
	 * @param string $mode
	 * @return string
	 */
	public static function openModeName($mode) {
		return Yii::t('player', self::$OPENMODE_NAMES[$mode]);
	}


	/**
	 * Data provider used in classroom courses LOs grid
	 * @return CActiveDataProvider
	 */
	public function dataProviderClassroomLOs($idCourse, $idUser = false) {

		$criteria = new CDbCriteria();
		$criteria->addCondition("t.idCourse = :id_course");
		//Remove those LO from the list, the "" is for removing the folders
		$criteria->addNotInCondition('t.objectType', array(self::OBJECT_TYPE_AICC, self::OBJECT_TYPE_SCORMORG, self::OBJECT_TYPE_TINCAN, ""));
		$criteria->addCondition("(t.publish_from IS NULL OR t.publish_from <= :publish_from)");
		$criteria->addCondition("(t.publish_to IS NULL OR t.publish_to >= :publish_to)");
		//$criteria->order = "iLeft ASC";
		$criteria->addCondition("t.visible = :visible");

		$timeNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$criteria->params[':id_course'] = $idCourse;
		$criteria->params[':publish_from'] = $timeNow;
		$criteria->params[':publish_to'] = $timeNow;
		$criteria->params[':visible'] = 1;

		/*
		//NOTE: this relation seems to cause problems, forget it for now
		$criteria->with = array(
			'learningCommontracks' => array('scopes' => array('byUser' => ($idUser > 0 ? $idUser : Yii::app()->user->id)))
		);
		*/

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.iLeft ASC';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}



	static public function prepareThumbnails($loId = null, $gallerySize = 10) {

		if($loId)
			$model = self::model()->findByPk($loId);
		else
			$model = new LearningOrganization();

		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$defaultImages   = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
		$userImages   	= $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);

		$count = array(
			'default' => count($defaultImages),
			'user' => count($userImages),
		);

		$reorderArray = function(&$array, $key, $value) {
			unset($array[$key]);
			$array = array($key => $value) + $array;
		};

		foreach ($defaultImages as $key => $value) {
			if ($key == $model->resource) {
				$selected = 'default';

				$reorderArray($defaultImages, $key, $value);
				break;
			}
		}

		if (empty($selected)) {
			foreach ($userImages as $key => $value) {
				if ($key == $model->resource) {
					$selected = 'user';
					$reorderArray($userImages, $key, $value);
					break;
				}
			}
		}

		$selected = empty($selected) ? 'default' : $selected;

		$defaultImages = array_chunk($defaultImages, $gallerySize, true);
		$userImages = array_chunk($userImages, $gallerySize, true);

		return array($count, $defaultImages, $userImages, $selected);
	}

	/**
	 * Gets the last score the user got on this LO
	 *
	 * @param      $idOrg
	 * @param      $userId
	 *
	 * @param bool $asAssociative
	 *
	 * @return int|string
	 * @throws CException
	 */
	public static function getLastScoreOfUserInLO($idOrg, $userId, $asAssociative = false){

		$model = self::model()->findByPk($idOrg);
		$model = $model->getMasterForCentralLo($userId);

		if(!$model)
			throw new CException('Organization model not found - LearningOrganization::getScoreofUser()');

		$hasScore = FALSE;
		$score = 0;
		$maxScore = 100;

		$res = 0;

		switch($model->objectType) {

			case LearningOrganization::OBJECT_TYPE_AICC:
				$organization = LearningOrganization::model()->findByPk($model->idOrg);
				$package = $organization->aiccPackage;
				$lastScoredTrack = null;
				// Get last tracked item of the package, having score, if any
				if($package){
					$lastScoredTrack = $package->getLastItemTrack($userId, true, true);
				}

				if ($lastScoredTrack) {
					$hasScore = TRUE;
					$score = $lastScoredTrack->score_raw;
					$maxScore = ($lastScoredTrack->score_max ? $lastScoredTrack->score_max : 100);
					$res = number_format($score,2);
				}
				break;


			case LearningOrganization::OBJECT_TYPE_SCORMORG:
				$tracking = Yii::app()->db->createCommand()
				                          ->select('score_raw, score_max')
				                          ->from(LearningScormTracking::model()->tableName())
				                          ->where('(idReference=:idOrg) AND (idUser=:idUser) AND (score_raw > 0)', array(':idOrg' => $model->idOrg, ':idUser' => $userId))
				                          ->order("idscorm_tracking DESC")
				                          ->queryRow();
				if(!empty($tracking)) {
					$hasScore = TRUE;
					$score = $tracking['score_raw'];
					$maxScore = ($tracking['score_max'] ? $tracking['score_max'] : 100);
					$res = number_format($tracking['score_raw'],2);
				}
				break;

			case LearningOrganization::OBJECT_TYPE_TEST:
				$criteria = new CDbCriteria();
				// Using COALESCE() here because 'score' and 'bonus_score' may have NULL value and with MYSQL the calculation of NULL+[int] equals NULL
				// COALESCE() will cast NULL to zero value
				$criteria->addCondition('(idReference=:idOrg) AND (idUser=:idUser) AND ( (coalesce(score,0)+coalesce(bonus_score,0)) > 0)');
				$criteria->params = array(':idOrg' => $model->idOrg, ':idUser' => $userId);
				$criteria->order = "idTrack DESC";
				$criteria->with = 'test';
				$tracking = LearningTesttrack::model()->find($criteria);
				if ($tracking) {
					$hasScore = TRUE;
					$testModel = $tracking->test;
					$score = ($tracking->score + $tracking->bonus_score);

					if($testModel)
						$maxScore = $testModel->getMaxScore($userId, false, false);
					$res = number_format($score,2);
				}
				break;

			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
			case LearningOrganization::OBJECT_TYPE_LTI:
				$tracking = Yii::app()->db->createCommand()
				                          ->select('score, score_max')
				                          ->from(LearningCommontrack::model()->tableName())
				                          ->where('(idReference=:idOrg) AND (idUser=:idUser) AND ( score > 0)', array(':idOrg' => $model->idOrg, ':idUser' => $userId))
				                          ->order("idTrack DESC")
				                          ->queryRow();
				if ($tracking) {
					$hasScore = TRUE;
					$score = $tracking['score'];
					$maxScore = $tracking['score_max'];
					$res = number_format($score,2);
				}
				break;
			//If the last LO passed is Assignment
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
				$tracking = Yii::app()->db->createCommand()
				                          ->select('evaluation_score')
				                          ->from(LearningDeliverableObject::model()->tableName())
				                          ->where('(id_deliverable=:idResource) AND (id_user=:idUser) AND ( evaluation_score > 0)', array(':idResource' => $model->idResource, ':idUser' => $userId))
				                          ->order("idObject DESC")
				                          ->queryRow();
				if ($tracking) {
					$hasScore = TRUE;
					$score = $tracking['evaluation_score'];
					$maxScore = 100;
					$res = $score;
				}
				break;

			default:
				break;

		}


		if($res=='-') $res = 0; // Some LOs consider this to be an "empty score"

		$res = number_format($res, 2); // X.XX

		return !$asAssociative ? $res : array(
			'hasScore'=>$hasScore,
			'score'=>$res,
			'maxScore'=>$maxScore,
		);

	}

	public function beforeDelete() {

		// Do clean up
		// Warning: LearningObjectsBehavior MIGHT be attached to whatever LO type you try to clean up
		// Make sure behavior events are disabled by calling disableLearningObjectsEvents()

		// Clean up AICC package
		if (!$this->id_object && ($this->objectType == self::OBJECT_TYPE_AICC)) {
			/* @var $aiccPackageModel LearningAiccPackage */
			$aiccPackageModel = LearningAiccPackage::model()->findByPk($this->idResource);
			if ($aiccPackageModel) {
				// Very important: avoid nested calls
				$aiccPackageModel->disableLearningObjectsEvents();
				$aiccPackageModel->delete();
			}
		}

		// If we're deleting a central LO with shared tracking and this LO owns any master track,
		// we must elect a slave track (if any) as the new master and update the resource specific tracking with it
		$this->adjustMasterTracksForCentralLo();

		return true;
	}

	/**
	 * Elects new master tracks for the object about to be deleted
	 */
	private function adjustMasterTracksForCentralLo() {
		if($this->repositoryObject && $this->repositoryObject->shared_tracking) {
			// 1. Determine if this object owns the master track for any user
			$criteria = new CDbCriteria();
			$criteria->addCondition('(idResource = :idResource) AND (objectType = :objectType) AND (idMasterOrg IS NULL) AND (idReference = :idReference)');
			$criteria->params = array(':idResource' => $this->idResource, ':objectType' => $this->objectType, ':idReference' => $this->idOrg);
			$masterTracks = LearningCommontrack::model()->findAll($criteria);
			foreach($masterTracks as $track)
				LearningCommontrack::electNewMasterTrack($track->idReference, $track->idUser, $track->objectType);
		}
	}

    public function afterDelete() {
        //it is a deletion of existing LO
        // Don't raise the event for LO folders only, all others raise it
        if(!empty($this->objectType)){
            //start event on successful LO delete
            Yii::app()->event->raise(EventManager::EVENT_DELETED_COURSE_LO, new DEvent($this, array(
                'course' => $this->course, // AR relation
                'learningobject' => $this,
                'user' => Yii::app()->user->loadUserModel(),
            )));
        }
    }

	/**
	 * Checks if a LO is related to App7020 Ask Gurus feature
	 * @param type $loId
	 * @return boolean
	 */
	public static function isAskGuruEnabled($loId){
		$loTags = App7020TagsLo::model()->findAllByAttributes(array('idLo' => $loId));
		if(!empty($loTags)){
			return true;
		}
		
		return false;
	}

	/**
	 * Get learning object info.
	 * @param number $idOrg
	 * @param number $idUser

	 * @return array
	 */
	public static function getLoInfo($id, $idUser, $scormItemId) {
		if (!$idUser) {
			$idUser = Yii::app()->user->idst;
		}
		/* Check is user allowed to enter the Learning Object*/
		$userLoAccess = LearningObjectsManager::getUserLOsAccessibility($idUser, array($id));
		if (!$userLoAccess[$id]){
			$result["success"] = false;
			$result["err"] = self::PLAYER_ERR_INVALID_LO;
			return $result;
		}

		$command = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from('learning_organization lo');
		$command->where('lo.idOrg=:id');
		$command->params[':id'] = $id;

		// Get learning object
		$loModel = $command->queryRow();

		$result = array("success" => true);
		if (!$loModel) {
			$result["success"] = false;
			$result["err"] = self::PLAYER_ERR_INVALID_LO;
			return $result;
		}

		if ($loModel['objectType'] == self::OBJECT_TYPE_VIDEO) {
			$model = LearningVideo::model()->findByPk($loModel['idResource']);
			$video_files = CJSON::decode($model->video_formats);
			$video_file_names = array_values($video_files);
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
			$result["url"] = $storage->fileUrl($video_file_names[0]);
			$result["description"] = $model->description;
			$result["type"] = $model->type;
			$result["video_formats"] = $model->video_formats;

			// We have low control on external, so we are marking them as completed
			$tracking = VideoComponent::track($idUser, $id, $loModel['idResource'], LearningCommontrack::STATUS_COMPLETED);
			UserLimit::logActiveUserLogin($idUser, true);
			CommonTracker::track($id, $idUser, LearningCommontrack::STATUS_COMPLETED);
			if (!$tracking) return array("success" => false, "err" => 203);
		}else if($loModel['objectType'] == self::OBJECT_TYPE_GOOGLEDRIVE){
				$track = LearningMaterialsTrack::model()->findByAttributes(array('idReference' =>$id, 'idUser' => $idUser));
				if (!$track) {
					//the object has not been tracked yet for this user: create the record now
					$track = new LearningMaterialsTrack();
					$track->idUser = (int)$idUser;
					$track->idReference = $id;
					$track->idResource = $loModel['idResource'];
				}
				$track->setStatus(LearningCommontrack::STATUS_COMPLETED);
				$track->setObjectType(LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE);
				if (!$track->save()) {
					throw new CException("Error while saving track data");
				}
				UserLimit::logActiveUserAccess($idUser);
				$result = Player::getLoAuthPlayUrl($id, $idUser, $scormItemId, "webapp");
		} else {
			$result = Player::getLoAuthPlayUrl($id, $idUser, $scormItemId, "webapp");
			$user_track = LearningCommontrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $id, 'objectType' => $loModel['objectType']));
			if ( !$user_track)
				CommonTracker::track($id, $idUser, LearningCommontrack::STATUS_AB_INITIO);
		}

		$result['completed'] = ($user_track ? $user_track['status'] : 'false');
		$result['loModel'] = $loModel;
		$result['next'] = self::getNextLoInfo($id, $result['loModel']['idCourse']);

		return $result;
	}


	/**
	 * Get next learning object info.
	 * @param number $idCurrentLo
	 * @param number $idCourse

	 * @return array
	 */
	public static function getNextLoInfo($idCurrentLo, $idCourse) {
		$params = array('userInfo' => true, 'scormChapters' => true, 'showHiddenObjects' => false, 'downloadOnScormFoldersOnly' => false, 'aiccChapters' => true);
		$objectsTree = LearningObjectsManager::getCourseLearningObjectsTree($idCourse, $params);
		$objectsFlatList = LearningObjectsManager::flattenLearningObjectsTree($objectsTree);

		foreach ($objectsFlatList as $index => $item) {
			if ($item['idOrganization'] == $idCurrentLo && $objectsFlatList[$index + 1]) {
				return $objectsFlatList[$index + 1];
			}
		}

		return false;
	}

	/**
	 * Get next learning object info.
	 * @param number $idResource
	 * @param string $objectType

	 * @return array
	 */
	public static function getLOsForLORepoVersion($objectType, $idResource) {
		$criteria = new CDbCriteria();
		$criteria->select = "idCourse";
		$criteria->condition = 'objectType = :objectType AND idResource = :idResource ';
		$criteria->params = array(':objectType' => $objectType, ':idResource' => $idResource);
		$criteria->distinct = true;

		return self::model()->findAll($criteria);

	}

	/**
	 * Get next learning object info.
	 * @param number $idResource
	 * @param string $objectType

	 * @return array
	 */
	public static function getLOsForLORepo($idObject) {
		$criteria = new CDbCriteria();
		$criteria->select = "idCourse";
		$criteria->condition = 'id_object = :idObject';
		$criteria->params = array(':idObject' => $idObject);
		$criteria->distinct = true;

		return self::model()->findAll($criteria);
	}

	/**
	 * Returns the learning_organization AR model for the master object, i.e. the first central LO placeholder "similar" to the current one played by the user.
	 * By "similar" we mean with the same version of the current one.
	 *
	 * @param $idUser integer The user that is trying to play this LO object
	 *
	 * @return LearningOrganization
	 */
	public function getMasterForCentralLo($idUser) {
		$master = $this;

		// Are we playing an object from Central LO with shared tracking ON?
		if($this->repositoryObject && $this->repositoryObject->shared_tracking) {
			// Find (if any) the master track .. and replace the LO with the master one (for tracking)
			$commonTrack = LearningCommontrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $this->idOrg));
			if ($commonTrack) {
				if ($commonTrack->idMasterOrg) {
					$master = LearningOrganization::model()->findByPk($commonTrack->idMasterOrg);
					$loModificationsOrigins = Yii::app()->cache->get('loModificationsOrigins');
					if(!$loModificationsOrigins)
						$loModificationsOrigins = array($master->idOrg => $this->idOrg);
					Yii::app()->cache->set('loModificationsOrigins',$loModificationsOrigins);
					Yii::log("** Central LO **: tracking " . $this->objectType . " on master LO (ID=" . $commonTrack->idMasterOrg . ") instead of slave LO (ID=" . $this->idOrg . ")", CLogger::LEVEL_INFO);
				}
			} else {
				// This user has no commn track yet -> find a candidate master anyway
				$masterTrack = LearningCommontrack::model()->findByAttributes(array('idUser' => $idUser, 'idResource' => $this->idResource,
					'objectType' => $this->objectType, 'idMasterOrg' => null));
				if($masterTrack) {
					$master = LearningOrganization::model()->findByPk($masterTrack->idReference);
					$loModificationsOrigins = Yii::app()->cache->get('loModificationsOrigins');
					if(!$loModificationsOrigins)
						$loModificationsOrigins = array($master->idOrg => $this->idOrg);
					Yii::app()->cache->set('loModificationsOrigins',$loModificationsOrigins);
					Yii::log("** Central LO **: tracking " . $this->objectType . " on master LO (ID=" . $masterTrack->idReference . ") instead of slave LO (ID=" . $this->idOrg . ")", CLogger::LEVEL_INFO);
				}
			}
		}

		return $master;
	}

	//Returns the id of the original organization for which the modification was made
	public static function getModOrigin($idOrganization){
		$originId = false;
		$origins = Yii::app()->cache->get('loModificationsOrigins');
		if(isset($origins[$idOrganization]))
			$originId = $origins[$idOrganization];

		return $originId;
	}

	//Removes record for any modifications origin for this organization
	public static function removeModOrigin($idOrganization){
		$loModificationsOrigins = Yii::app()->cache->get('loModificationsOrigins');
		if(!$loModificationsOrigins)
			$loModificationsOrigins = array();
		else{
			unset($loModificationsOrigins[$idOrganization]);
		}
		Yii::app()->cache->set('loModificationsOrigins',$loModificationsOrigins);
	}

	public function getUserLoCourseParticipation($idUser, $idCourse) {
		$courses = 0;

		// The Change will take Effect only if the LO is from Central REPO and have shared tracking
		if($this->repositoryObject && $this->repositoryObject->shared_tracking) {
			$courses = Yii::app()->db->createCommand()
				->select(array("COUNT(*)"))
				->from(LearningOrganization::model()->tableName() . " lo")
				->leftJoin(LearningCourseuser::model()->tableName(). " lcu", "lcu.idCourse = lo.idCourse")
				->where(array( "and",
					"lo.id_object = :object",
					"lo.idCourse != :course",
					"lcu.idUser = :user",
					"lo.idResource = :resource"
				))
				->queryScalar(array(
					":object" => $this->id_object,
					":course" => $idCourse,
					":user" => $idUser,
					":resource" => $this->idResource
					));
		}

		return $courses;
	}

	/**
	 * Returns how many users are in attempted or ab-initio state in the current LO
	 * @return mixed
	 */
	public function countInProgressUsers() {
		return Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from(LearningCommontrack::model()->tableName())
			->where("idReference = :idOrg AND objectType = :objectType", array(':idOrg' => $this->idOrg, ':objectType' => $this->objectType))
			->andWhere("status IN ('".implode("','", array(LearningCommontrack::STATUS_ATTEMPTED, LearningCommontrack::STATUS_AB_INITIO))."')")
			->queryScalar();
	}
	

	/**
	 * Set video LOs having "in progress" status to "error" if they were inserted more than certain amount of time in the past
	 * 
	 */
	public function setOldInProgressVideosToError() {
		
		// We are dealing with videos only, in progressing only
		if ($this->objectType != self::OBJECT_TYPE_VIDEO || (int) $this->visible != -1) {
			return;
		}
		
		// Now
		$now = Yii::app()->localtime->toLocalDateTime();
		
		// Time, a day earlier
		$mark = Yii::app()->localtime->subInterval($now, 'P1D');
		
		// If dateInsert is LESS THAN a day earlier
		$old = Yii::app()->localtime->isLt($this->dateInsert, $mark);

		//We need to check if the file the video was recently updated before returning an error
		if($old && ($this->objectType == self::OBJECT_TYPE_VIDEO) && ((int) $this->visible == -1) && isset($this->date_updated) && $this->date_updated) {
			$old = Yii::app()->localtime->isLt($this->date_updated, $mark);
		}

		if ($old) {
			$this->visible = -2;
			Yii::log("Old video LO found in 'transcoding' status. Assuming 'error' status, changed. ID: " . $this->idOrg, CLogger::LEVEL_WARNING);
			$this->saveNode(false);
		}
		
		
	}

	/**
	 * @return bool
	 */
	public function testHasSpecialQuestions(){
		if($this->objectType == self::OBJECT_TYPE_TEST){
			$allQuestionsRels = LearningTestQuestRel::model()->findAllByAttributes(array(
				'id_test' => $this->idResource,
			));

			$hasSpecial = false;
			foreach($allQuestionsRels as $rel){
				$question 	= LearningTestquest::model()->findByPk($rel->id_question);
				if( ($question->type_quest == LearningTestquest::QUESTION_TYPE_EXTENDED_TEXT) || ($question->type_quest == LearningTestquest::QUESTION_TYPE_UPLOAD) ){
					$hasSpecial = true;
					break;
				}
			}
			return $hasSpecial;
		}
		return false;
	}
	

	/**
	 * 
	 * @return array
	 */
	public static function getTinCanPredefinedUrs() {
	    $models = CoreSettingTincanUrl::model()->findAll();
	    $urls = array();
	    foreach ($models as $m) {
	        $urls[$m->id] = $m->url;
	    }
	    return $urls;
	}

}