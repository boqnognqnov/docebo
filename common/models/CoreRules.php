<?php

/**
 * This is the model class for table "core_rules".
 *
 * The followings are the available columns in table 'core_rules':
 * @property integer $id_rule
 * @property string $title
 * @property string $lang_code
 * @property string $rule_type
 * @property string $creation_date
 * @property integer $rule_active
 * @property string $course_list
 */
class CoreRules extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreRules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_rules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course_list', 'required'),
			array('rule_active', 'numerical', 'integerOnly'=>true),
			array('title, lang_code', 'length', 'max'=>255),
			array('rule_type', 'length', 'max'=>10),
			array('creation_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_rule, title, lang_code, rule_type, creation_date, rule_active, course_list', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'core_rules_entity' => array(self::HAS_MANY, 'CoreRulesEntity', 'id_rule'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array(),
			 'dateAttributes' => array('creation_date')
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_rule' => 'Id Rule',
			'title' => 'Title',
			'lang_code' => 'Lang Code',
			'rule_type' => Yii::t('standard', 'Rule Type'),
			'creation_date' => 'Creation Date',
			'rule_active' => 'Rule Active',
			'course_list' => 'Course List',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_rule',$this->id_rule);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('rule_type',$this->rule_type,true);
		$criteria->compare('creation_date',Yii::app()->localtime->fromLocalDate($this->creation_date),true);
		$criteria->compare('rule_active',$this->rule_active);
		$criteria->compare('course_list',$this->course_list,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	
	public function getBaseRuleAndCourses($langs = false){
		
		// Contains a simple array of all course IDs that the user should be subscribed to
		// (they are assigned to the base enrollment policy)
		$result = array(
			'courses'=>array(),
			'rules'=>array()
		);
			
		$criteria = new CDbCriteria();
		$criteria->with = 'core_rules_entity';
		
		// Apply the base rule condition
		$criteria->addCondition(array(
			"`rule_type`='base'",
			"`rule_active`='1'"
		));
	
		// No need for param bindig here, it's already done by addInCondition()
		$criteria->addInCondition('`core_rules_entity`.`id_entity`', $langs);
		
		$baseRule = CoreRules::model()->findAll($criteria);
		if(isset($baseRule[0]) && count($baseRule[0]->core_rules_entity)){
			$baseRuleEntities = $baseRule[0]->core_rules_entity;
			
			$allCoursesToSubscribe = array();
			$rules = array();
			
			foreach($baseRuleEntities as $singleBaseRuleEntity){
				if($singleBaseRuleEntity->course_list){
					$coursesFromEntity = CJSON::decode($singleBaseRuleEntity->course_list, true);
					$allCoursesToSubscribe = array_merge($allCoursesToSubscribe, $coursesFromEntity);
				}
				
				$rules[] = $singleBaseRuleEntity->id_rule . '_' . $singleBaseRuleEntity->id_entity;
			}
			$allCoursesToSubscribe = array_unique($allCoursesToSubscribe);
			
		}
		
		if(count($allCoursesToSubscribe)){
			$result['courses'] = $allCoursesToSubscribe;
		}
		if(count($rules)){
			$result['rules'] = $rules;
		}
		
		return $result;
	}
	
	public function applyBaseEnrollmentRuleToUser($idstUser){
		
		// Get the entities from the base condition:
		// - applicable to all languages
		// - applicable to the current language
		$langs = array('all');
		if(isset(Yii::app()->session['current_lang']))
			$langs[] = Yii::app()->session['current_lang'];
		
		// $baseRuleEntities is an array with two indexes - 'courses' and 'rules'
		// 'courses' is an array of course IDs from the base rule
		// 'rules' is an array of strings that must be logged
		$baseRule = self::model()->getBaseRuleAndCourses($langs);
			
		// Subscribe the user to the courses in the base rule for the current language
		// and those that apply to all languages.
		if(isset($baseRule['courses'])  && is_array($baseRule['courses'])){
			foreach($baseRule['courses'] as $courseId){
				$model = new LearningCourseuser();
				$model->subscribeUser(
						$idstUser,
						'', // Subscribed by user (ID)
						$courseId, // Course ID
						0, // Waiting (1 or 0)
						LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT // Subscription level (3 by default)
				);
				
			}
		}
		
		// Log that this enrollment policy has been applied
		if(isset($baseRule['rules']) && is_array($baseRule['rules'])){
			foreach($baseRule['rules'] as $rule){
				$logModel = new CoreRulesLog();
				$logModel->log_action = '_NEW_USER';
				$logModel->log_time = Yii::app()->localtime->toLocalDateTime();
				$logModel->applied = addslashes(CJSON::encode($rule));
				$logModel->save();
			}
		}
	}
}