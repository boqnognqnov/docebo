<?php

/**
 * This is the model class for table "learning_certificate".
 *
 * The followings are the available columns in table 'learning_certificate':
 * @property integer $id_certificate
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $base_language
 * @property string $cert_structure
 * @property string $orientation
 * @property string $bgimage
 * @property integer $meta
 * @property integer $user_release
 */
class LearningCertificate extends CActiveRecord
{

	public $confirm;
	
	/**
	 * Used when Editing Certificate template to upload the background image
	 * 
	 * @var CUploadedFile
	 */
	public $background_image = null;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCertificate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_certificate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'), //, cert_structure
			array('name, code','filter','filter'=>'strip_tags'),
			array('cert_structure', 'required', 'on' => 'template'), //, cert_structure
			array('meta, user_release', 'numerical', 'integerOnly'=>true),
			array('code, name, base_language, bgimage', 'length', 'max'=>255),
			array('orientation', 'length', 'max'=>1),
            array('code, name, description', 'safe', 'on' => array('update')),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_certificate, code, name, description, base_language, cert_structure, orientation, bgimage, meta, user_release', 'safe', 'on'=>'search'),

			// NON-DB
			array('background_image', 'safe', 'on' => array('template')),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_certificate' => 'Id Certificate',
			'code' => Yii::t('standard', '_CODE'),
			'name' => Yii::t('standard', '_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'base_language' => 'Base Language',
			'cert_structure' => 'Cert Structure',
			'orientation' => 'Orientation',
			'bgimage' => 'Bgimage',
			'meta' => 'Meta',
			'user_release' => 'User Release',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_certificate',$this->id_certificate);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('base_language',$this->base_language,true);
		$criteria->compare('cert_structure',$this->cert_structure,true);
		$criteria->compare('orientation',$this->orientation,true);
		$criteria->compare('bgimage',$this->bgimage,true);
		$criteria->compare('meta',$this->meta);
		$criteria->compare('user_release',$this->user_release);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	public function dataProvider() {
		$criteria = new CDbCriteria;
//		$criteria->compare('idst', $this->idst);
		// $criteria->compare('t.groupid', $this->groupid, true);
//		$criteria->compare('description', $this->description, true);
		// $criteria->compare('t.hidden', $this->hidden, true);
		// $criteria->compare('t.type', $this->type, true);
//		$criteria->compare('show_on_platform', $this->show_on_platform, true);

		// $criteria->with = array('coreGroupMembers');

		if(Yii::app()->user->getIsPu()){
			$showOnlyCertsIssuedToAssignedUsers = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'view_assigned_user_certificates_only');
			// The current PUs profile has the option "Show only certificate templates
			// issued to assigned users" enabled, so the current PU will see those certificate
			// templates and those that are yet to be assigned to anyone (because e.g. the PU
			// may have just created those templates, if he has enough permission)
			if($showOnlyCertsIssuedToAssignedUsers){
				$certsIssuedToPUsManagedUsers = Yii::app()->getDb()->createCommand()
					->selectDistinct('t.id_certificate')
					->from(LearningCertificateAssign::model()->tableName().' t')
					->join(CoreUserPU::model()->tableName().' puu', 'puu.user_id=t.id_user AND puu.puser_id=:currentPu', array(
						':currentPu'=>Yii::app()->user->id,
					))
					->queryColumn();

				$certsIssuedToCourses = Yii::app()->getDb()->createCommand()
										   ->selectDistinct('id_certificate')
										   ->from('learning_certificate_assign')
										   ->queryColumn();
				$certsIssuedToLPs = Yii::app()->getDb()->createCommand()
									   ->selectDistinct('id_certificate')
									   ->from('learning_certificate_assign_cp')
									   ->queryColumn();
				$certsIssuedToSomeone = array_merge($certsIssuedToCourses, $certsIssuedToLPs);

				if(!empty($certsIssuedToSomeone)){
					$certsIssuedToNobody = Yii::app()->getDb()->createCommand()
											  ->select('id_certificate')
											  ->from('learning_certificate')
											  ->where(array("NOT IN", 'id_certificate', $certsIssuedToSomeone))
											  ->queryColumn();
				}else{
					$certsIssuedToNobody = Yii::app()->getDb()->createCommand()
											  ->select('id_certificate')
											  ->from('learning_certificate')
											  ->queryColumn();
				}


				$criteria->addInCondition('t.id_certificate', array_merge($certsIssuedToNobody, $certsIssuedToPUsManagedUsers));
			}
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 't.idst desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	/**
	 * @param $certificate_id
	 * @param $course_id
	 */
	public static function assignToCourse($certificate_id, $course_id)
	{

		// Lets do some clean up:
		// If incoming Cert ID is null/0/none (imply a certificate detachment)
		// OR is different from currently attached Cert ID (re-attachment, changing certificate for the course)
		// .. then delete all issued certificates related to this course
		$courseCertModel = 	LearningCertificateCourse::model()->findByAttributes(array('id_course' => $course_id));
		if (!$courseCertModel || ($courseCertModel->id_certificate != $certificate_id)) {
			$assignments = LearningCertificateAssign::model()->findAllByAttributes(array('id_course' => $course_id));
			if ($assignments)  foreach ($assignments as $assignment) $assignment->delete();
		}

		//remove all labels to the given course
		LearningCertificateCourse::model()->deleteAllByAttributes(array(
			'id_course' => $course_id
		));

		//save all given labels to that course
		if ($certificate_id)
		{
			$model = new LearningCertificateCourse();
			$model->id_certificate = $certificate_id;
			$model->id_course = $course_id;
			// this is done to release the certificate only on course completion, we are doing a simplification here
			// because previously we allowed this for other situation only but for now we had done a step back in
			// order to simplify the user understanding of the system
			$model->available_for_status = 3;
			$model->save(false);
		}
	}

	public function deleteImage()
	{
		$asset = CoreAsset::model()->findByPk($this->bgimage);
		if ($asset) {
			$asset->delete();
		}
		$this->bgimage = '';
		return $this->save(false);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {

		// DO a Cleanup:  remove attached certificates and issued certificates as well from all possible tables

		// Remove all assignments (issued) to COURSE/USERS related to THIS certificate (internally it will delete all PDFs also)
		$assignments = LearningCertificateAssign::model()->findAllByAttributes(array('id_certificate' => $this->id_certificate));
		if ($assignments)  foreach ($assignments as $assignment) $assignment->delete();

		// Remove all assignments (issued) to Curricula/USERS related to THIS certificate (internally it will delete all PDFs also)
		$assignments = LearningCertificateAssignCp::model()->findAllByAttributes(array('id_certificate' => $this->id_certificate));
		if ($assignments)  foreach ($assignments as $assignment) $assignment->delete();


		// Also, delete all certificate attachments to Courses and Curriculas
		$assignments = LearningCertificateCourse::model()->findAllByAttributes(array('id_certificate' => $this->id_certificate));
		if ($assignments)  foreach ($assignments as $assignment) $assignment->delete();
		
		$assignments = LearningCertificateCoursepath::model()->findAllByAttributes(array('id_certificate' => $this->id_certificate));
		if ($assignments)  foreach ($assignments as $assignment) $assignment->delete();
		
		// Remove background image asset 
		$asset = CoreAsset::model()->findByPk($this->bgimage);
		if ($asset) {
			$asset->delete();
		}
		

	}




	/**
	 * Return URL to certificate background image, if any.
	 *
	 * Background image is saved in the model as an Asset ID (integer).
	 * If it is saved as a string (presumably a FILENAME), we are searching for that filename in themes/<theme>/images path, assuming this is some default image.
	 *
	 * @param string $variant
	 * @param bool $useBasePath
	 * @return string
	 */
	public function getBgImageUrl($variant = CoreAsset::VARIANT_ORIGINAL, $useBasePath = false) {
		
		$url = '';
		
		if (empty($this->bgimage)) {
			return $url;
		}

		// Check Assets descriptor table
		$url = CoreAsset::url( (int) $this->bgimage, $variant, true);
		
		// If not, check for exisiting filename in themes
		if (empty($url)) {
			$filePath = Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $this->bgimage;
			if (is_file($filePath)) {
				if($useBasePath)
					return $filePath;

				$url = Yii::app()->theme->getAbsoluteBaseUrl() . "/images/" . $this->bgimage;
			}
		}
		
		return $url;
		
		
	}



	public function afterFind()
	{
		if(preg_match("/\p{Han}+/u", $this->cert_structure)){
			$this->cert_structure = str_replace("sans-serif", "", $this->cert_structure);
		}

		return parent::afterFind();

	}
	

}