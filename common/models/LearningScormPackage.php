<?php

/**
 * This is the model class for table "learning_scorm_package".
 *
 * The followings are the available columns in table 'learning_scorm_package':
 * @property integer $idscorm_package
 * @property string $idpackage
 * @property integer $idProg
 * @property string $path
 * @property string $location
 * @property string $defaultOrg
 * @property integer $idUser
 * @property string $scormVersion
 * 
 * @property LearningScormOrganizations $scormOrganization
 * @property LearningScormResources[] $scormResources
 * 
 */
class LearningScormPackage extends CActiveRecord
{

	const RESOURCES_DIRECTORY = 'scorm';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormPackage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_package';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProg, idUser', 'numerical', 'integerOnly'=>true),
			array('idpackage, path, location, defaultOrg', 'length', 'max'=>255),
			array('scormVersion', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_package, idpackage, idProg, path, location, defaultOrg, idUser, scormVersion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scormOrganization' => array(self::HAS_ONE, 'LearningScormOrganizations', 'idscorm_package'),
			'scormResources'    => array(self::HAS_MANY, 'LearningScormResources', 'idscorm_package'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_package' => 'Idscorm Package',
			'idpackage' => 'Idpackage',
			'idProg' => 'Id Prog',
			'path' => 'Path',
			'location' => 'Location',
			'defaultOrg' => 'Default Org',
			'idUser' => 'Id User',
			'scormVersion' => 'Scorm Version',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_package',$this->idscorm_package);
		$criteria->compare('idpackage',$this->idpackage,true);
		$criteria->compare('idProg',$this->idProg);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('defaultOrg',$this->defaultOrg,true);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('scormVersion',$this->scormVersion,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	public function behaviors() {
		return array(
			'Scorm' => array(
				'class' => 'ScormBehavior',
			),
		);
	}



	public function onBeforeDelete($event) {

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		try {

			//crate a storage manager instance
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);

			if ($storage->fileExists($this->path)) {
				//check if there are other packages sharing the same files. If not, then proceed with deleting
				$criteria = new CDbCriteria();
				$criteria->addCondition('idscorm_package <> :idscorm_package');
				$criteria->params[':idscorm_package'] = $this->getPrimaryKey();
				$otherPackages = LearningScormPackage::model()->findAllByAttributes(array('path' => $this->path), $criteria);
				
				if (count($otherPackages) <= 0) {
					$rs = $storage->removeFolder($this->path);
					if (!$rs) {
						throw new CException('Error while deleting package files');
					}
				}
			}

			//remove packages resources from DB
			$rs = LearningScormResources::model()->deleteAllByAttributes(array('idscorm_package' => $this->getPrimaryKey()));
			if (!$rs) {
				throw new CException('Error while deleting package resources info');
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {
			
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e; //pass exception to above level
		}
		parent::onBeforeDelete($event);
	}
	
	
	
	
	/**
	 * Unzip a given ZIP file into a given folder in the file storage.
	 * Check for the manifest in the ZIP. Nothing more!
	 * 
	 * @param string $archiveFilePath Full path to the archive file (local)
	 * @param string $destFolderName Relative path to root folder of the SCORMORG package itself (imsmanifest.xml location)<br>
	 * 	 <i>Example (local file system):</i><br>
	 * 		$destFolderName = 'myscormorg'<br>
	 *  	Resulting location: /files/doceboLms/scorm/myscormorg<br>
	 * 
	 * @throws CException
	 */
	public function extractScormPackage($archiveFilePath, $destFolderName) {
	
		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $destFolderName;
		
		// check if a previous upload (path) exists; create folder if not
		if (!file_exists($unpackPath)) {
			if ( ! mkdir($unpackPath, 0777) ) {
				throw new CException("Directory creation failed.");
			}
		}

		$unpackPath = realpath($unpackPath);
		
		// ZIP Management
		$zip = new ZipArchive();
		// Open the zip file
		$zip_open = $zip->open($archiveFilePath);
		if ($zip_open !== true) {
			// unable to open the zip file
			throw new CException(Yii::t('player', 'This file is a not valid Zip Package (Error: {error_code})', array('{error_code}' => $zip_open)) .
					'. ' . Yii::t('player', 'Please check if your SCORM package is correct and if the problem persist report the error message to the help desk team'), $zip_open);
		}
		// Checking if it is a valid scorm package
		if ($zip->locateName("imsmanifest.xml") === false) {
			// ok no imsmanifest, throw this away
			throw new CException(Yii::t('player', 'The required imsmanifest.xml file not found or is not in the zip root')
					. '. ' . Yii::t('player', 'Please check if your SCORM package is correct and if the problem persist report the error message to the help desk team'));
		}
		// Extract scorm package
		if($zip->extractTo($unpackPath) !== true) {
			// decompression failed
			// remove uncompressed dir
			throw new CException(Yii::t('player', 'Zip decompression failed (Error: {error_code})', array('{error_code}' => $zip->getStatusString())) .
					'. ' . Yii::t('player', 'Please check if your SCORM package is correct and if the problem persist report the error message to the help desk team'), $zip->getStatusString());
		}
		$zip->close();
			
		// save the scorm structure in it's final directory
		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
		if ( ! $storageManager->store($unpackPath)) { throw new CException("Error while saving file to final storage."); }

		// Rename zip with hash
		$newZippedScorm = $unpackPath . ".zip";
		@rename($archiveFilePath, $newZippedScorm);

		if(PluginManager::isPluginActive('OfflinePlayerApp')) {
			// In order to improve performace of Offline Player Api, let's
			// keep the zip copy on S3, renamed with the unique hash.
			if (!$storageManager->store($newZippedScorm))
				throw new CException("Error while saving file to final storage.");
		}

		// remove tmp unpack dir and zip file
		FileHelper::removeDirectory($unpackPath);
		FileHelper::removeFile($newZippedScorm);

		return true;
	
	}

	/**
	 * @param $idScorm
	 * Return the "path" to the scorm package
	 */
	public static function getScormPath($idScorm){
		if ( !$idScorm) return false;

		$qry = "SELECT path 
				From learning_organization AS lo
				LEFT JOIN ". self::model()->tableName() ." lsc ON lo.idResource = lsc.idscorm_package
				WHERE  idOrg = " . $idScorm;
			$scormPath = Yii::app()->db->createCommand($qry)->queryColumn();

		return $scormPath[0];
	}

}