<?php

/**
 * This is the model class for table "core_deleted_user".
 *
 * The followings are the available columns in table 'core_deleted_user':
 * @property integer $id_deletion
 * @property integer $idst
 * @property string $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $pass
 * @property string $email
 * @property string $photo
 * @property string $avatar
 * @property string $signature
 * @property integer $level
 * @property string $lastenter
 * @property integer $valid
 * @property string $pwd_expire_at
 * @property string $register_date
 * @property string $deletion_date
 * @property integer $deleted_by
 */
class CoreDeletedUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreDeletedUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_deleted_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst, level, valid, deleted_by', 'numerical', 'integerOnly'=>true),
			array('userid, firstname, lastname, email, photo, avatar', 'length', 'max'=>255),
			array('pass', 'length', 'max'=>64),
			array('lastenter, pwd_expire_at, register_date, deletion_date', 'safe'),
			array('idst, userid, firstname, lastname, pass, email, avatar, signature, level, lastenter, valid, pwd_expire_at, register_date', 'safe', 'on' => 'create'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_deletion, idst, userid, firstname, lastname, pass, email, photo, avatar, signature, level, lastenter, valid, pwd_expire_at, register_date, deletion_date, deleted_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('lastenter medium', 'pwd_expire_at medium', 'register_date medium', 'deletion_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_deletion' => 'Id Deletion',
			'idst' => 'Idst',
			'userid' => 'Userid',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'pass' => 'Pass',
			'email' => 'Email',
			'photo' => 'Photo',
			'avatar' => 'Avatar',
			'signature' => 'Signature',
			'level' => 'Level',
			'lastenter' => 'Lastenter',
			'valid' => 'Valid',
			'pwd_expire_at' => 'Pwd Expire At',
			'register_date' => 'Register Date',
			'deletion_date' => 'Deletion Date',
			'deleted_by' => 'Deleted By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_deletion',$this->id_deletion);
		$criteria->compare('idst',$this->idst);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('signature',$this->signature,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('lastenter',Yii::app()->localtime->fromLocalDateTime($this->lastenter),true);
		$criteria->compare('valid',$this->valid);
		$criteria->compare('pwd_expire_at',Yii::app()->localtime->fromLocalDateTime($this->pwd_expire_at),true);
		$criteria->compare('register_date',Yii::app()->localtime->fromLocalDateTime($this->register_date),true);
		$criteria->compare('deletion_date',Yii::app()->localtime->fromLocalDateTime($this->deletion_date),true);
		$criteria->compare('deleted_by',$this->deleted_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}

	public function getClearUserId()
	{
		if (substr($this->userid, 0, 1) == '/') {
			return substr($this->userid, 1);
		}
		else {
			return $this->userid;
		}
	}
}