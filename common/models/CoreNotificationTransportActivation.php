<?php

/**
 * This is the model class for table "core_notification_transport_activation".
 *
 * The followings are the available columns in table 'core_notification_transport_activation':
 * @property string $notification_id
 * @property integer $transport_id
 * @property integer $active
 * @property string $additional_info
 */
class CoreNotificationTransportActivation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_notification_transport_activation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notification_id, transport_id', 'required'),
			array('transport_id, active', 'numerical', 'integerOnly'=>true),
			array('notification_id', 'length', 'max'=>20),
			array('additional_info', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('notification_id, transport_id, active, additional_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'notification_id' => 'Notification',
			'transport_id' => 'Transport',
			'active' => 'Active',
			'additional_info' => 'Additional Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('notification_id',$this->notification_id,true);
		$criteria->compare('transport_id',$this->transport_id);
		$criteria->compare('active',$this->active);
		$criteria->compare('additional_info',$this->additional_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreNotificationTransportActivation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @param int $notificationId
	 * @param string $transportMethod, textual representation of the required method, eg: "email", "inbox", ...
	 * @return array
	 */
	public static function getAdditionalInfo($notificationId, $transportMethod){
		$additionalInfo = Yii::app()->db->createCommand("
			SELECT additional_info
			FROM ".self::model()->tableName()." AS cnta
			INNER JOIN ".CoreNotificationTransport::model()->tableName()." AS cnt ON cnta.transport_id = cnt.id
			WHERE cnta.notification_id = :notificationId AND cnt.name = :transportMethod
		")->queryRow(true, array(
			':notificationId' => $notificationId,
			':transportMethod' => $transportMethod
		));
		return CJSON::decode($additionalInfo['additional_info']);
	}
}
