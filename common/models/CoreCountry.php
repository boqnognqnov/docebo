<?php

/**
 * This is the model class for table "core_country".
 *
 * The followings are the available columns in table 'core_country':
 * @property integer $id_country
 * @property string $name_country
 * @property string $iso_code_country
 * @property integer $id_zone
 */
class CoreCountry extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreCountry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_zone', 'numerical', 'integerOnly'=>true),
			array('name_country', 'length', 'max'=>64),
			array('iso_code_country', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_country, name_country, iso_code_country, id_zone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_country' => 'Id Country',
			'name_country' => 'Name Country',
			'iso_code_country' => 'Iso Code Country',
			'id_zone' => 'Id Zone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_country',$this->id_country);
		$criteria->compare('name_country',$this->name_country,true);
		$criteria->compare('iso_code_country',$this->iso_code_country,true);
		$criteria->compare('id_zone',$this->id_zone);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
	
	/**
	 * Return country name by ISO code
	 * @param string $iso_code
	 * @return string
	 */
	public static function getNameByIsoCode($iso_code) {
		$model = self::model()->findByAttributes(array('iso_code_country' => $iso_code));
		return ($model ? $model->name_country : "");
	}
    
    public static function getNameById($id){        
		$countriesSource = self::model()->find('id_country = :id', array(
            ':id' => $id
        ));
        
        return $countriesSource['name_country'];
    }
	
	
	/**
	 * Return ID of the country by country name
	 * 
	 * @param string $countryName
	 * @return number
	 */
	public static function getIdCountryByName($countryName) {
		
		static $ids = array();
		
		$key = 'name_' . $countryName;
		if (isset($ids[$key])) {
			return $ids[$key];
		}
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('LOWER(name_country)=:name_country');
		$criteria->params = array(
				':name_country' 	=> mb_strtolower($countryName,  'UTF-8'),
		);
		
		$model = self::model()->find($criteria);
		if($model) {
			$ids[$key] = $model->id_country;
			return $ids[$key];
		}
		
		return false;
		
		
	}
	
	
	/**
	 * Return an array of  <country-name> => <country-name>, i.e. the KEY is equal to VALUE and is set to Country Name, rather than some country ID
	 * 
	 * @return array
	 */
	public static function getCountryCountryList() {
		
		$countries = array();
		$countriesSource = self::model()->findAll(array('order' => 'name_country'));
		foreach ($countriesSource as $c) {
			$countries[$c['name_country']] = $c['name_country'];
		}
		
		return $countries;
		
	}
	

	/**
	 * Return an array of  <country-iso-code> => <country-name>
	 *
	 * @return array
	 */
	public static function getCountryList() {
	
		$countries = array();
		$countriesSource = self::model()->findAll(array('order' => 'name_country'));
		foreach ($countriesSource as $c) {
			$countries[$c['iso_code_country']] = $c['name_country'];
		}
	
		return $countries;
	
	}
    
    public static function getCountryListWithId(){
        $countries = array();
		$countriesSource = self::model()->findAll(array('order' => 'name_country'));
		foreach ($countriesSource as $c) {
			$countries[$c['id_country']] = $c['name_country'];
		}
	
		return $countries;
    }
	
	
}
