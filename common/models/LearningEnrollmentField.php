<?php

/**
 * This is the model class for table "learning_enrollment_fields".
 *
 * The followings are the available columns in table 'learning_enrollment_fields':
 * @property integer $id
 * @property string $type
 * @property integer $sequence
 * @property integer $visible_to_user
 * @property integer $mandatory
 * @property string $settings
 * @property json $translation
 * @property json $courses
 *
 * The followings are the available model relations:
 * @property LearningCourseFieldDropdown[] $learningCourseFieldDropdowns
 * @property LearningCourseFieldTranslation[] $learningCourseFieldTranslations
 *
 * @property string $courseEntry
 * @property string $lang_code
 * @property LearningCourse $course
 */
class LearningEnrollmentField extends CActiveRecord
{
	/**
	 * Array of translations for this field in all available languages
	 */
	private $fieldTranslations = array();

	public $maxSequence;
	public $courseEntry = '';
	public $lang_code;
	public $course = null;

	/* FIELD TYPES */
	const TYPE_FIELD_DATE = 1;
	const TYPE_FIELD_DROPDOWN = 2;
	const TYPE_FIELD_TEXTFIELD = 3;
	const TYPE_FIELD_TEXTAREA = 4;
	const TYPE_FIELD_IFRAME = 5;

	/**
	 * Mapping between field types and value column type
	 * @var array
	 */
	private $_valueColumnType = array(
		self::TYPE_FIELD_DATE => 'DATE',
		self::TYPE_FIELD_DROPDOWN => 'SMALLINT',
		self::TYPE_FIELD_TEXTAREA => 'TEXT',
		self::TYPE_FIELD_TEXTFIELD => 'VARCHAR(255)',
	    self::TYPE_FIELD_IFRAME => 'VARCHAR(2048)'
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_enrollment_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('sequence, visible_to_user', 'numerical', 'integerOnly'=>true)
		);
	}
	
	public function renderFilterField(){
		return '';
	}

	public function getSubclassedInstance() {
		$subclassType = $this->type ? 'CourseField' . ucfirst($this->type) : 'LearningCourseField';
		$instance = new $subclassType('clone');
		$instance->attributes = $this->attributes;
		return $instance;
	}

	public function renderFieldSettings() {
		return '';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'LearningEnrollmentFieldsDropdown' => array(self::HAS_MANY, 'LearningEnrollmentFieldsDropdown', 'id_field'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'          => 'Id Field',
			'type'              => Yii::t('standard', '_TYPE'),
			'sequence'          => Yii::t('manmenu', '_ORDER'),
			'visible_to_user' => Yii::t('standard', 'Show in Course Catalog and Course Description widget'),
			'translation' => Yii::t('standard', 'Translation')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('invisible_to_user',$this->invisible_to_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave () {
//		// Load all language codes
//		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();
//
//		// 1. Translations handling
//		if($this->getIsNewRecord()) {
//
//			// New record -> create all translations
//			$sql = "INSERT INTO ".LearningCourseFieldTranslation::model()->tableName()." (id_field, lang_code, translation) VALUES ";
//			$rows= array();
//			foreach($activeLanguagesList as $key => $language)
//				$rows[] = "(".$this->id_field.",'".$key."', ".Yii::app()->db->quoteValue(isset($this->fieldTranslations[$key]) ? $this->fieldTranslations[$key] : '').")";
//			$sql .= implode(", ", $rows);
//			Yii::app()->db->createCommand($sql)->execute();
//
//		} else if(!empty($this->fieldTranslations)) {
//
//			// Existing record -> update translations only if set in fieldTranslations
//			$sql = '';
//			foreach($this->fieldTranslations as $key => $translation)
//				$sql .= "UPDATE " . LearningCourseFieldTranslation::model()->tableName() . " SET translation = " . Yii::app()->db->quoteValue($translation) .
//					" WHERE id_field = " . $this->id_field . " AND lang_code = '" . $key . "'";
//			if($sql)
//				Yii::app()->db->createCommand($sql)->execute();
//		}
//
//		// 2. Value table handling
//		if($this->getIsNewRecord()) {
//			$event = new DEvent();
//			Yii::app()->event->raise('BeforeCourseAdditionalFieldAfterSave', $event);
//                    
//			if($event->return_value)
//				$this->_valueColumnType = array_merge($this->_valueColumnType, $event->return_value);
//
//			// Add new column to the value table
//			$sql = "ALTER TABLE ".LearningCourseFieldValue::model()->tableName()."
//					CHARACTER SET = utf8 , COLLATE = utf8_general_ci,
//  					ADD COLUMN field_".$this->id_field." ".$this->_valueColumnType[$this->type]." DEFAULT NULL";
//
//			try {
//				// Syntax only available for MYSQL >= 5.6
//				Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
//			} catch(CDbException $e) {
//				// Well, looks like MYSQL version doesn't like this syntax
//				Yii::app()->db->createCommand($sql)->execute();
//			}
//		}

        parent::afterSave();
	}

    public function afterDelete() {
//
//        // After deleting the field, remove the column from learning_course_field_value
//        try {
//            $sql = "ALTER TABLE ".LearningCourseFieldValue::model()->tableName()."
//  					DROP COLUMN field_".$this->id_field;
//
//            try {
//                // Syntax only available for MYSQL >= 5.6
//                Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
//            } catch(CDbException $e) {
//                // Well, looks like MYSQL version doesn't like this syntax
//                Yii::app()->db->createCommand($sql)->execute();
//            }
//		$allFilters = LearningReportFilter::model()->findAll();
//		foreach($allFilters as $filter) {
//			$new_filter = new stdClass();
//			$data = json_decode($filter->filter_data);
//			if($data->fields->course) {
//				foreach($data->fields->course as $key=>$value) {
//					if($key != $this->id_field) {
//					$new_filter->$key = $value;
//						//Log::_($this->id_field);
//					}
//				}
//				$data->fields->course = $new_filter;
//				$filter->filter_data = json_encode($data);
//				if($filter->save()) {
//				}
//			}	
//		}
//            parent::afterDelete();
//        } catch(Exception $e) {
//            Log::_($e->getMessage());
//        }
    }

	/**
	 * Returns the next sequence number
	 * @return int
	 */
	public function getNextSequence()
	{
		$criteria=new CDbCriteria;
		$criteria->select = 'max(sequence) as maxSequence';
		$res = $this->model()->find($criteria);
		if(is_null($res->maxSequence))
			$max_seq = 0;
		else
			$max_seq = $res->maxSequence + 1;

		return $max_seq;
	}

	/**
	 * Sets the fields translations to be saved in afterSave
	 * @param $translations
	 */
//	public function setFieldTranslations($translations) {
//		if(!empty($translations))
//			$this->fieldTranslations = $translations;
//	}

    /**
     * @return CArrayDataProvider
     */
    public function getAdditionalEnrollmentFieldsDataProvider() {
		$defaultLang = Lang::getBrowserCodeByCode(Settings::get('default_language'));
		$lang = Yii::app()->getLanguage();
        $params = Yii::app()->request->getParam('LearningCourseFieldTranslation');
        $translation = $params['translation'];
        $criteria = new CDbCriteria(array('order' => 'sequence asc'));
		$criteria->select = [
			"id",
			"type",
			"sequence",
			"IF( 
				JSON_UNQUOTE(translation->'$.\"" . $lang . "\"') IS NOT NULL,
				JSON_UNQUOTE(translation->'$.\"" . $lang . "\"'),
				JSON_UNQUOTE(translation->'$.\"" . $defaultLang . "\"')
			) as translation"
		];
		
        if ( $translation ) {
            $criteria->addSearchCondition('translation', $translation);
        }

        $rawData = $this->findAll($criteria);
        $defaultElements = 10;
        $coreSetting = CoreSetting::model()->findByAttributes(array('param_name' => 'elements_per_page'));
        $elementsPerPage = $coreSetting->param_value ? $coreSetting->param_value : $defaultElements;
        /*=- If there is not set elements per page in Advance Setting default is 10 -=*/
        return new CArrayDataProvider($rawData, array(
            'keyField'      => 'id',
            'pagination' => array(
                'pageSize' => $elementsPerPage,
            ),
        ));
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return self::getCategoryNameForType($this->type);
    }

	/**
	 * Generates the course field label (localized)
	 * @param $type
	 * @return string
	 */
	public static function getCategoryNameForType($type) {
		$enrollmentTypes = LearningEnrollmentField::getEnrollmentFieldsTypes();
		if ( isset($enrollmentTypes[$type]) ) {
			return $enrollmentTypes[$type];
		} else {
			'Unknown field type';
		}
	}

    /**
     * @return array
     */
    public static function getEnrollmentFieldsTypes()
    {
        $fieldTypes = array(
			self::TYPE_FIELD_DATE => Yii::t('standard', 'Date'),
			self::TYPE_FIELD_DROPDOWN => Yii::t('standard', 'Dropdown field'),
			self::TYPE_FIELD_TEXTAREA => Yii::t('standard', 'Free text field'), 
			self::TYPE_FIELD_TEXTFIELD => Yii::t('standard', 'Text field'),
            self::TYPE_FIELD_IFRAME => Yii::t('standard', 'IFRAME')
		);
        return $fieldTypes;
    }
    /**
     * @return array
     */
    public static function getEnrollmentFieldsArr()
    {
        $fieldTypes = array(
			self::TYPE_FIELD_DATE => 'date',
			self::TYPE_FIELD_DROPDOWN => 'dropdown',
			self::TYPE_FIELD_TEXTAREA => 'textarea', 
			self::TYPE_FIELD_TEXTFIELD => 'textfield',
            self::TYPE_FIELD_IFRAME => "iframe",
		);
        return $fieldTypes;
    }

    /**
     * @param null $id_field
     * @return mixed
     */
    public function getAdditionalFieldsTranslationName($id_field=null)
    {
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        static $translations = array();

        if(!isset($translations[$id_field])) {
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => $lang,
                ))
                ->queryScalar();
        }

        if ( empty($translations[$id_field]) || $translations[$id_field] === '') {
            /* If there is no translation for this $id_field apply the default(english) */
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => Settings::get('default_language', 'english'),
                ))
                ->queryScalar();
			// if there is no translation for the default language also, we get the first translated language
			if(empty($translations[$id_field]) || $translations[$id_field] === ''){
				$translations[$id_field] = Yii::app()->getDb()->createCommand()
					->select('translation')
					->from(LearningCourseFieldTranslation::model()->tableName())
					->where('id_field=:id_field', array(':id_field' => $id_field))
					->andWhere('translation <> ""')
					->queryScalar();
			}
        }

        return $translations[$id_field];
    }


    public function getTranslation($idField=false, $lang_code = false, $returnDefault = true){
        $lang_code = !$lang_code ? Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) : $lang_code;
        $fid = ($idField)? $idField : $this->id;
        $translation = Yii::app()->db->createCommand()
            ->select('translation')
            ->from(LearningCourseFieldTranslation::model()->tableName().' ft')
            ->where('id_field = '.$fid.' AND lang_code = "'.$lang_code.'"')
            ->queryScalar();
        if(!$translation && $returnDefault){
            $translation = Yii::app()->db->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName().' ft')
                ->where('id_field = '.$fid.' AND lang_code = "'.Settings::get('default_language').'"')
                ->queryScalar();

			if(empty($translation) || $translation == ''){
				$translation = Yii::app()->db->createCommand()
					->select('translation')
					->from(LearningCourseFieldTranslation::model()->tableName().' ft')
					->where('id_field = '.$fid.' AND translation <> ""')
					->queryScalar();
			}
        }
        return $translation;
    }

	public function getConditionsOptions() {
		return array(
			'contains' => Yii::t('standard','_CONTAINS'),
			'equal' => Yii::t('standard','_EQUAL'),
			'not-equal' => Yii::t('standard','_NOT_EQUAL'),
		);
	}

	public function getTextareaConditionsOptions() {
		return array(
			'contains' => Yii::t('standard','_CONTAINS'),
			'not-contains' => Yii::t('standard','_NOT_CONTAINS')
		);
	}

	public function validateFieldEntry() {
		if ($this->mandatory) {
			if (empty($this->courseEntry)) {
				$this->addError('courseEntry', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
			}
		}
	}

	public function renderValue($courseEntry, $courseEntryModel=null) {
		$className = 'CourseField' . ucfirst($this->type);
		$field = $className::model()->findByAttributes(array('id' => $this->id));
		return $field->renderFieldValue($courseEntry, $courseEntryModel);
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel=null, $renderedWhere) {
		return '';
	}

	private function populateField() {
		$className = 'CourseField' . ucfirst($this->type);
		return $className::model()->findByAttributes(array('id_field' => $this->id));
	}

	public function renderFilter() {
		$field = $this->populateField();
		return $field->renderFilterField();
	}
	
	public static function hasAnyField(){
		$fieldCount = self::model()->count();
		
		return (($fieldCount > 0) ? true : false);
	}

    public function prepareDropdownData( ) {
		if ( !isset($this->id) ) {
			$data = new LearningEnrollmentFieldDropdown();
		} else {
			$data = LearningEnrollmentFieldDropdown::model()->find('id_field = :id', [':id' => $this->id]);
		}
        return $data;
    }

	/**
	 * Returns a deserialized settings array for this field model
	 * @return array|mixed
	 */
	public function getSettingsArray() {
		return $this->settings ? CJSON::decode($this->settings) : array();
	}

	public function getFieldEntry($idCourse){
		$fieldClass = 'CourseField' . ucfirst($this->type);
		$courseField = $fieldClass::model()->findByPk($this->id);
		$tempValue = Yii::app()->getDb()->createCommand()
				->select('field_'.$this->id)
				->from(LearningCourseFieldValue::model()->tableName().' v')
				->where('v.id_course=:id_course', array(':id_course' => $idCourse))
				->queryScalar();
		$courseField->courseEntry = $tempValue;
		$resultValue = '';
		$resultValue = $fieldClass::renderFieldValue($courseField->courseEntry);
		return $resultValue;
	}
	
	/**
	 * Get an informational array for all fieldsm filtered by course and or type of the field
	 * 
	 * @param integer $idCourse
	 * @param integer[] $types
	 * @return array
	 */
	public static function getFieldsByCourse($idCourse = null, $types=false){
		
		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		// Resolve requested language
		$language = Yii::app()->getLanguage();
		
		$command = Yii::app()->db->createCommand();
		$command->select('id, IF(JSON_UNQUOTE(translation->\'$."'.$language.'"\') IS NULL OR JSON_UNQUOTE(translation->\'$."'.$language.'"\') = "", JSON_UNQUOTE(translation->\'$."'.$defaultLanguage.'"\'), JSON_UNQUOTE(translation->\'$."'.$language.'"\')) as title, type, mandatory');
		$command->from(self::model()->tableName());

		if($idCourse){

			$courseObject = LearningCourse::model()->findByPk($idCourse);
			if(empty($courseObject->idCourse)) return array();

			$command->where("courses IS NULL OR JSON_CONTAINS(courses, '".$courseObject->idCategory."')");
		}
		
		if ($types !== false) {
		    if (!is_array($types)) {
		        $types = array((int) $types);
		    }
		    $command->andWhere(array("IN", "type", $types));
		}
		
		if(!Yii::app()->user->isGodAdmin && !Yii::app()->user->getIsPU()){
			$command->andWhere("visible_to_user = 1");
		}
		$command->order("sequence");
		$result = $command->queryAll();
		
		foreach($result as $key => $value){
			if($value['type'] == self::TYPE_FIELD_DROPDOWN){
				$result[$key]['options'] = LearningEnrollmentFieldDropdown::getOptionsByFieldId($value['id']);
			}
		}
		
		return $result;
	}
	
	public static function validateEnrollmentArray($arrayFields, $idCourse = null) {

		$missingMandatoryFields = array();
		$missingFields = array();
		$invalidFields = array();
		$success = true;

		if($idCourse){
			$course = LearningCourse::model()->findByAttributes(array('idCourse' => $idCourse));
		}
		
		//chack if all mandatory fields are present
		$mandatoryFileds = self::model()->findAllByAttributes(array('mandatory' => 1));
		if (!empty($mandatoryFileds)) {
			foreach ($mandatoryFileds as $value) {
				
				if(!$idCourse || empty($value->courses) || (!empty($course->idCategory) && is_array(json_encode($value->courses)) && in_array($course->idCategory, json_encode($value->courses)))){
					if (!array_key_exists($value->id, $arrayFields) || empty($arrayFields[$value->id])) {
						//some mandatory fields is missing
						$missingMandatoryFields[] = $value->id;
						$success = false;
					}
				}
			}
		}
		
		

		foreach ($arrayFields as $key => $value) {
			$field = self::model()->findByPk($key);
			if (empty($field->id)) {
				//field is not found
				$success = false;
				$missingFields[] = $key;
			} else {
				
				//if course is provided, check if the field is visible in this course
				if($idCourse && $field->courses){
					if(!empty($course->idCategory)){
						if(!in_array($course->idCategory, json_decode($field->courses))){
							$success = false;
							$invalidFields[] = $key;
						}
					}
				}
				
				//if requested field is from dropdown type
				if ($field->type == self::TYPE_FIELD_DROPDOWN) {
					$options = LearningEnrollmentFieldDropdown::model()->findAllByAttributes(array('id_field' => $key));
					$optionsArray = array();
					
					foreach ($options as $valueOption) {
						$optionsArray[] = $valueOption->id;
					}
					if(!in_array($value, $optionsArray)){
						$success = false;
						$invalidFields[] = $key;
					}
				}
			}
		}
		
		return array(
			'success' => $success,
			'invalidFields' => $invalidFields,
			'missingMandatoryFields' => $missingMandatoryFields,
			'missingFields' => $missingFields
		);
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
	    // Ensure we have valid JSON in settings field
	    if (empty($this->settings)) {
	        $this->settings = "{}";
	    }
	    return parent::beforeSave();
	}
	
}

