<?php

/**
 * This is the model class for table "learning_course_coaching_session_user".
 *
 * The followings are the available columns in table 'learning_course_coaching_session_user':
 * @property integer $id
 * @property integer $idSession
 * @property integer $idUser
 *
 * The followings are the available model relations:
 * @property LearningCourseCoachingSession $session
 * @property CoreUser $user
 */
class LearningCourseCoachingSessionUser extends CActiveRecord
{
	// USER COACHING SESSION STATUSES
	const COACHING_SESSION_STATUS_PROCEED = null; // There is no coaching enabled for this course, so user can start the course
	const COACHING_SESSION_STATUS_HAVE_ACTIVE_SESSION = 0; // Coaching is enabled and user already has an active coaching session
	const COACHING_SESSION_STATUS_HAVE_SESSION_NOT_STARTED = 1; // Coaching is enabled and user already has a coaching session, that is not started
	const COACHING_SESSION_STATUS_HAVE_EXPIRED_SESSION = 2; // Coaching is enabled and user already has a coaching session, but it is expired
	const COACHING_SESSION_STATUS_SELECT = 3; // Coaching is enabled and user needs to select a session manually first
	const COACHING_SESSION_STATUS_ASK = 4; // Coaching is enabled and user needs to ask admin for a session
	const COACHING_SESSION_STATUS_NEW = 5; // Coaching is enabled and user just committed his/her choice
	const COACHING_SESSION_STATUS_ASK_BUT_ACCESSED = 6; // Coaching is enabled and user needs to ask admin for a session but has permission to access

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_coaching_session_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idSession', 'required'),
			array('idSession, idUser', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idSession, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'session' => array(self::BELONGS_TO, 'LearningCourseCoachingSession', 'idSession'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'	=> 'ID',
			'idSession' => 'Id Session',
			'idUser' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idSession',$this->idSession);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseCoachingSessionUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * getUserCoachingSessions()
	 */
	public function getUserActiveCoachingSessions($idUser, $idCourse) {
		// Get current time in UTC to use it in the query
		$currentUTCNow = Yii::app()->localtime->fromLocalDateTime(Yii::app()->localtime->toUTC(Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow()).' 00:00:00'));

		// Get user coaching session if exists
		$userCoachingSession = Yii::app()->db->createCommand()
			->select('ucs.*')
			->from('learning_course_coaching_session_user ucsu')
			->join('learning_course_coaching_session ucs', 'ucs.idSession = ucsu.idSession ')
			->where('ucsu.idUser =' . $idUser)
			->andWhere('ucs.idCourse =' . $idCourse)
			->andWhere('ucs.datetime_start <= :currentUTCNow', array(':currentUTCNow' => $currentUTCNow))
			->andWhere('ucs.datetime_end >= :currentUTCNow', array(':currentUTCNow' => $currentUTCNow))
			->queryAll();

		return $userCoachingSession;
	}

	/**
	 * getUserCoachingSessions()
	 */
	public function getUserNotStartedCoachingSessions($idUser, $idCourse) {
		// Get current time in UTC to use it in the query
		$currentUTCNow = Yii::app()->localtime->toUTC(Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->getLocalNow()).' 00:00:00');

		// Get user coaching session if exists
		$userCoachingSession = Yii::app()->db->createCommand()
			->select('ucs.*')
			->from('learning_course_coaching_session_user ucsu')
			->join('learning_course_coaching_session ucs', 'ucs.idSession = ucsu.idSession ')
			->where('ucsu.idUser =' . $idUser)
			->andWhere('ucs.idCourse =' . $idCourse)
			->andWhere('ucs.datetime_start > :currentUTCNow', array(':currentUTCNow' => $currentUTCNow))
			->queryAll();

		return $userCoachingSession;
	}
}
