<?php

/**
 * This is the model class for table "learning_elucidat".
 *
 * The followings are the available columns in table 'learning_elucidat':
 * @property integer $id
 * @property integer $id_resource
 * @property string $last_status
 * @property string $last_status_updated
 * @property string $last_successful_update
 * @property string $p_project_code
 * @property string $p_name
 * @property string $p_created
 * @property string $p_project_key
 * @property string $r_release_code
 * @property string $r_created
 * @property string $r_description
 * @property string $r_version_number
 * @property string $r_release_mode
 * @property string $edit_url
 * @property integer $keep_updated
 *
 */
class LearningElucidat extends CActiveRecord
{

	const STATUS_SUCCESS = 'success';
	const STATUS_FAILURE = 'failure';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_elucidat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_resource, p_project_code, p_name, p_created, p_project_key, r_release_code', 'required'),
			array('id_resource, keep_updated', 'numerical', 'integerOnly'=>true),
			array('last_status, r_release_mode', 'length', 'max'=>32),
			array('p_project_code, r_release_code', 'length', 'max'=>64),
			array('p_name', 'length', 'max'=>255),
			array('p_project_key', 'length', 'max'=>128),
			array('r_description', 'length', 'max'=>2048),
			array('r_version_number', 'length', 'max'=>16),
			array('edit_url', 'length', 'max'=>2000),
			array('last_status_updated, last_successful_update, r_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_resource, last_status, last_status_updated, last_successful_update, p_project_code, p_name, p_created, p_project_key, r_release_code, r_created, r_description, r_version_number, r_release_mode, edit_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_resource' => 'Id Org',
			'last_status' => 'Last Status',
			'last_status_updated' => 'Last Status Updated',
			'last_successful_update' => 'Last Successful Update',
			'p_project_code' => 'Project Code',
			'p_name' => 'Project Name',
			'p_created' => 'Project Created',
			'p_project_key' => 'Project Key',
			'r_release_code' => 'Release Code',
			'r_created' => 'Release Created',
			'r_description' => 'Release Description',
			'r_version_number' => 'Release Version Number',
			'r_release_mode' => 'Release Mode',
			'edit_url' => 'Edit Url',
			'keep_updated' => 'Keep Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_resource',$this->id_resource);
		$criteria->compare('last_status',$this->last_status,true);
		$criteria->compare('last_status_updated',$this->last_status_updated,true);
		$criteria->compare('last_successful_update',$this->last_successful_update,true);
		$criteria->compare('p_project_code',$this->p_project_code,true);
		$criteria->compare('p_name',$this->p_name,true);
		$criteria->compare('p_created',$this->p_created,true);
		$criteria->compare('p_project_key',$this->p_project_key,true);
		$criteria->compare('r_release_code',$this->r_release_code,true);
		$criteria->compare('r_created',$this->r_created,true);
		$criteria->compare('r_description',$this->r_description,true);
		$criteria->compare('r_version_number',$this->r_version_number,true);
		$criteria->compare('r_release_mode',$this->r_release_mode,true);
		$criteria->compare('edit_url',$this->edit_url,true);
		$criteria->compare('keep_updated',$this->keep_updated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningElucidat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Create a temporary TinCan ZIP package in temporary folder. This file can be used to "fake" tincan package installation, if required.
	 * 
	 * @param string $name
	 * @param string $description
	 * 
	 * @return strin Local ZIP file
	 */
	public function createTemporaryZipPackage($name, $description) {
	
	    $template = <<<TPL
<?xml version="1.0" encoding="utf-8" ?>
<tincan xmlns="http://projecttincan.com/tincan.xsd">
    <activities>
        <activity id="%%ACTIVITY_ID%%" type="http://adlnet.gov/expapi/activities/course">
            <name>%%NAME%%</name>
            <description lang="en-US">%%DESCRIPTION%%</description>
            <launch lang="en-us">nolaunch.html</launch>
        </activity>
    </activities>
</tincan>
TPL;
	    
	    $activityId = "https://learning.elucidat.com/course/" . $this->p_project_code . "-" . $this->r_release_code;
	    $template = str_replace('%%ACTIVITY_ID%%', $activityId, $template);
	    $template = str_replace('%%NAME%%', $name, $template);
	    $template = str_replace('%%DESCRIPTION%%', $description, $template);
	     
	    $tmp                   = Docebo::randomHash() . "_elucidat";
	    $tmpDir                = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $tmp;
	    $tmpZipFilePath        = $tmpDir . ".zip";
	    $tmpTincanXmlFilePath  = $tmpDir . DIRECTORY_SEPARATOR . "tincan.xml";
	    
	    FileHelper::createDirectory($tmpDir, 0777, true);
	    file_put_contents($tmpTincanXmlFilePath, $template);
	    $zip = new Zip($tmpZipFilePath);
	    $zip->addFolder($tmpDir);
	    
	    
	    if ($zip->lastError) {
	        return false;
	    }
	    
	    $zip->close();

	    return $tmpZipFilePath;
	    
	}
	
	
}
