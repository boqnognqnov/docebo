<?php

/**
 * This is the model class for table "lt_course_session".
 *
 * The followings are the available columns in table 'lt_course_session':
 * @property integer $id_session
 * @property integer $course_id
 * @property string $name
 * @property integer $max_enroll
 * @property integer $min_enroll
 * @property integer $score_base
 * @property string $other_info
 * @property integer $notify_enrolled
 * @property string $date_begin
 * @property string $date_end
 * @property string $last_subscription_date
 * @property integer $total_hours
 * @property integer $created_by
 * @property integer $evaluation_type
 *
 * The followings are the available model relations:
 * @property LearningCourse $course
 * @property LtCourseSessionDate[] $learningCourseSessionDates
 * @property LtCourseuserSession $learningCourseuserSession
 */
class LtCourseSession extends CActiveRecord
{
	const REPEAT_DAILY = 0;
	const REPEAT_WEEKLY = 1;
	const REPEAT_MONTHLY = 2;
	const REPEAT_YEARLY = 3;

	const EVENT_AFTER_DELETE = 'LtCourseSessionAfterDelete';


	const EVALUATION_TYPE_SCORE = 0;
	const EVALUATION_TYPE_ONLINE = 1;
	const EVALUATION_TYPE_ATTENDANCE = 2;
	const EVALUATION_TYPE_NONE = 3;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lt_course_session';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// Rules for those attributes that will receive user inputs.
		return array(
			array('course_id, max_enroll', 'required'),
			array('course_id, max_enroll, min_enroll, score_base, notify_enrolled, created_by', 'numerical', 'integerOnly' => true),
			array('total_hours', 'type', 'type' => 'float', 'allowEmpty' => false),
			array('name', 'length', 'max'=>45),
			array('other_info, evaluation_type, last_subscription_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, course_id, name, max_enroll, min_enroll, score_base, other_info, notify_enrolled, date_begin, date_end, last_subscription_date, total_hours, evaluation_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
			'learningCourse' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
			'learningCourseSessionDates' => array(self::HAS_MANY, 'LtCourseSessionDate', 'id_session'),
			'learningCourseuserSessions' => array(self::HAS_MANY, 'LtCourseuserSession', 'id_session'),
			'createdBy' => array(self::BELONGS_TO, 'CoreUser', 'created_by')
		);
	}

	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('date_begin medium', 'date_end medium', 'last_subscription_date medium'),
				'dateAttributes' => array()
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'course_id' => 'Course',
			'name' => Yii::t('classroom', 'Session name'),
			'max_enroll' => 'Max Enroll',
			'min_enroll' => 'Min Enroll',
			'score_base' => Yii::t('classroom', 'Evaluation score base'),
			'other_info' => 'Other Info',
			'notify_enrolled' => 'Notify Enrolled',
			'date_begin' => Yii::t('certificate', 'Session date begin'),
			'date_end' => Yii::t('certificate', 'Session date end'),
			'last_subscription_date' => Yii::t('classroom', 'Last subscription date'),
			'total_hours' => Yii::t('certificate', 'Session total hours'),
			'evaluation_type' => 'Evaluation type',
			//For report only
			'level' => Yii::t('standard', '_LEVEL'),
			'date_inscr' => Yii::t('report', '_DATE_INSCR'),
			'status' => Yii::t('standard', '_STATUS'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('max_enroll',$this->max_enroll);
		$criteria->compare('min_enroll',$this->min_enroll);
		$criteria->compare('score_base',$this->score_base);
		$criteria->compare('other_info',$this->other_info,true);
		$criteria->compare('notify_enrolled',$this->notify_enrolled);
		$criteria->compare('date_begin',Yii::app()->localtime->fromLocalDateTime($this->date_begin),true);
		$criteria->compare('date_end',Yii::app()->localtime->fromLocalDateTime($this->date_end),true);
		$criteria->compare('last_subscription_date',Yii::app()->localtime->fromLocalDateTime($this->last_subscription_date),true);
		$criteria->compare('total_hours',$this->total_hours);
		$criteria->compare('evaluation_type',$this->evaluation_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LtCourseSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		if($this->isNewRecord)
		{
			$this->created_by = Yii::app()->user->id;
		}

		return parent::beforeSave();
	}

	/**
	 * Saves a session with its dates in a transaction
	 * @param $dates array The array of session dates to save
	 * @throws CException
	 * @throws Exception
	 */
	public function saveWithDates(array $dates) {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL)
			$transaction = $db->beginTransaction();

		try {
			// Save the session object first
			$rs = $this->save();
			if (!$rs)
				throw new CException("Error while saving session object.");

			// Save each day for the current session
			$this->saveSessionDates($dates);

			// Update calculated columns in the session date
			$this->updateCalculatedSessionColumns();

			// Everything ok -> commit changes
			$transaction->commit();
		} catch (CException $e) {
			$transaction->rollBack();
			throw $e;
		}
	}




	/**
	 * Deletes a session with its related tables (dates, attendance, and so on)
	 */
	public function deleteSession() {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}
		$userIdsInSession = Yii::app()->db->createCommand()
			->select('id_user')
			->from(LtCourseuserSession::model()->tableName())
			->where('id_session = :id', array(':id'=>$this->id_session))->queryColumn();
		$eventData = array(
			'session_data' => array(
				'id_session' => $this->id_session,
				'name' => $this->name,
				'date_begin' => $this->date_begin,
				'date_end' => $this->date_end,
				'other_info' => $this->other_info,
				'icsLocation' => $this->icsLocation(),
				'sessionDates' => $this->compileShortcodeDates(),
				'usersIds' => $userIdsInSession
			),
			'course_id' => $this->course_id,
		);

		try {

			// Delete session dates, attendance info and subscriptions
			// Users subscribed to just this session will become "unassigned" and stay in learning_courseuser waiting to be
			// assigned to another session (or deleted by an admin)
			LtCourseSessionDateAttendance::model()->deleteAllByAttributes(array('id_session' => $this->id_session));
			LtCourseSessionDate::model()->deleteAllByAttributes(array('id_session' => $this->id_session));
			LtCourseuserSession::model()->deleteAllByAttributes(array('id_session' => $this->id_session));

			// Delete any "course file" visibilities, made visible to this session
			LearningCourseFileVisibility::model()->deleteAllByAttributes(array('id_session'=>$this->id_session));

			// Now delete session object
			$this->delete();

			// Commit everything
			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}

		if (Yii::app()->user->isPu || Yii::app()->user->isGodAdmin)
		{
			Yii::app()->event->raise('ILTSessionDeleted', new DEvent($this, $eventData));
		}

		return true;
	}



	/**
	 * Generic method for retrieving all sessions of a classroom course
	 *
	 * @param int $idCourse
	 * @return array the list of session ARs
	 */
	public static function findSessions($idCourse) {
		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :course_id", array(':course_id' => $idCourse));
		return self::model()->findAll($criteria);
	}





	//--- SESSIONS GRID FUNCTIONS ---
	/**
	 * Returns the data provide object for the sessions grid view
	 * @param $idCourse
	 * @param bool $showExpiredSessions
	 * @param bool $ownerFilter
	 * @return CActiveDataProvider
	 */
	public function dataProvider($idCourse, $showExpiredSessions = true, $ownerFilter = false, $showInstructorSessions = false)
	{

        // Read filters from HTTP request
        $rq = Yii::app()->request;
        $input = new stdClass();
        $input->searchText = trim($rq->getParam('searchText', ''));
        $input->searchMonth = $rq->getParam('selectMonth', 0); // default: all months
        $input->searchYear = $rq->getParam('selectYear', 0); // default: all years
		$input->sort = $rq->getParam(get_class($this) . '_sort', false);

        // Prepare selection criteria
        $criteria = new CDbCriteria();
        $criteria->addCondition("course_id = :course_id");
        $criteria->params[':course_id'] = $idCourse;

        // 1. Text filter
        if ($input->searchText != "") {
            $criteria->addCondition("name LIKE :search_text");
			$criteria->params[':search_text'] = '%' . $input->searchText . '%';
        }

        // 2. Time filters
        $filterMonth = ($input->searchMonth >= 1 && $input->searchMonth <= 12);
        $filterYear = ($input->searchYear > 0);
        if ($filterMonth && $filterYear) {
            // Both filters have been set
            $monthDays = cal_days_in_month(CAL_GREGORIAN, $input->searchMonth, $input->searchYear);
            $monthIndex = str_pad($input->searchMonth, 2, '0', STR_PAD_LEFT);
			$startCheck = $input->searchYear . '-' . $monthIndex . '-01 00:00:00';
			$endCheck = $input->searchYear . '-' . $monthIndex . '-' . $monthDays . ' 23:59:59';
            $tmpCriteria = new CDbCriteria();
            $tmpCriteria->addBetweenCondition("date_begin", $startCheck, $endCheck);
            $tmpCriteria->addBetweenCondition("date_end", $startCheck, $endCheck, "OR");
            $criteria->mergeWith($tmpCriteria, "AND");
        } elseif ($filterMonth && !$filterYear) {
            // Only month has been set
            $tmpCriteria = new CDbCriteria();
            $tmpCriteria->addCondition("MONTH(date_begin) = :search_month", "OR");
            $tmpCriteria->addCondition("MONTH(date_end) = :search_month", "OR");
            $tmpCriteria->params[':search_month'] = $input->searchMonth;
            $criteria->mergeWith($tmpCriteria, "AND");
        } elseif (!$filterMonth && $filterYear) {
            // Only year has been set
			$startCheck = $input->searchYear . '-01-01 00:00:00';
			$endCheck = $input->searchYear . '-12-31 23:59:59';
            $tmpCriteria = new CDbCriteria();
            $tmpCriteria->addBetweenCondition("date_begin", $startCheck, $endCheck);
            $tmpCriteria->addBetweenCondition("date_end", $startCheck, $endCheck, "OR");
            $criteria->mergeWith($tmpCriteria, "AND");
        }

        // Check if list should be filtered for instructors
        $pUserRights = Yii::app()->user->checkPURights($idCourse);

		if (!Yii::app()->user->getIsGodadmin() && !$pUserRights->all && ($pUserRights->isInstructor || $pUserRights->isTutor)) {
			$criteria->join .= ' INNER JOIN lt_courseuser_session cus ON cus.id_session = ' . $this->getTableAlias() . '.id_session AND cus.id_user = ' . Yii::app()->user->id . ' ';
        }

		if (!$showExpiredSessions) {
			//Show only sessions with last_subscription_date > now for the power users
			$criteria->addCondition("last_subscription_date = :not_set OR last_subscription_date IS NULL OR last_subscription_date = '' OR last_subscription_date > :last_subscription_date");
			$criteria->params[':not_set'] = '0000-00-00 00:00:00';
			$criteria->params[':last_subscription_date'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		}

		if ($ownerFilter) {
			$instructorCondition = '';
			if($showInstructorSessions && $pUserRights->isInstructor) {
				$criteria->join .= ' LEFT JOIN lt_courseuser_session instructor_sessions ON instructor_sessions.id_session = ' . $this->getTableAlias() . '.id_session AND instructor_sessions.id_user = ' . Yii::app()->user->id . ' ';
				$instructorCondition = ' OR instructor_sessions.id_user IS NOT NULL';
			}
			$criteria->addCondition("created_by = :created_by".$instructorCondition);
			$criteria->params[':created_by'] = Yii::app()->user->id;
		}

		if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsPu()){
			// current user is just a user or instructor, so we filter the sessions to that, in which this user is enrolled in
			$enrolledSessionIds = Yii::app()->db->createCommand()
					->select('id_session')->from(LtCourseuserSession::model()->tableName())
					->where('id_user=:id', array(':id' => Yii::app()->user->id))->queryColumn();
			$criteria->addCondition('t.id_session IN ("' . implode('","', $enrolledSessionIds) . '")');
		}

		$criteria->with['createdBy'] = array(
			'joinType' => 'LEFT JOIN',
		);
		//$criteria->together = true;

        // Pass search criteria to the data provider
        $config = array();

        $config['criteria'] = $criteria;
        $sortAttributes = array(
			'name' => $this->getTableAlias().".name",
			'created_by' => array(
				'asc' => "createdBy.userid ASC, ".$this->getTableAlias().".date_begin DESC",
				'desc' => "createdBy.userid DESC, ".$this->getTableAlias().".date_begin ASC"
			),
			'date_begin' => $this->getTableAlias().".date_begin",
			'date_end' => $this->getTableAlias().".date_end"
        );
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = 'date_begin DESC';
        $config['pagination'] = array(
            'pageSize' => Settings::get('elements_per_page', 10)
        );

        return new CActiveDataProvider(get_class($this), $config);
    }

	/**
	 * Returns the data provider object for session enrollment listings
	 * @return CSqlDataProvider
	 */
	public function dataProviderEnrollment($searchInput = null, $orderByInput = null) {
		$db = Yii::app()->db;

		$command = $db->createCommand()
			->select("COUNT(*)")
			->from(LtCourseuserSession::model()->tableName() . " cus")
			->join(CoreUser::model()->tableName() . " u", "u.idst = cus.id_user")
			->join(LearningCourseuser::model()->tableName() . " cu", "u.idst = cu.idUser AND cu.idCourse = :id_course", array(':id_course' => $this->course_id))
			->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->andWhere("cus.status <> :waiting_status", array(':waiting_status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST))
			->andWhere("cus.status <> :subs_confirm", array(':subs_confirm' => LtCourseuserSession::$SESSION_USER_CONFIRMED));


		//--- power user filter ---
                  $pUserRights = Yii::app()->user->checkPURights($this->course_id);
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
			$command->join(CoreUserPU::model()->tableName() . ' pu', 'u.idst = pu.user_id AND puser_id = :puser_id', array(':puser_id' => Yii::app()->user->id));
		//---

		if ($searchInput)
			$command->andWhere('CONCAT(u.firstname, " ", u.lastname) like :search OR u.email LIKE :search OR u.userid LIKE :search', array(':search' => '%' . $searchInput . '%'));

		$count = $command->queryScalar();

		$command = $db->createCommand()
			->select("u.idst, u.userid, u.firstname, u.lastname, u.email, cus.id_user, cu.level, cus.status, cu.idCourse, cus.id_session")
			->from(LtCourseuserSession::model()->tableName() . " cus")
			->join(CoreUser::model()->tableName() . " u", "u.idst = cus.id_user")
			->join(LearningCourseuser::model()->tableName() . " cu", "u.idst = cu.idUser AND cu.idCourse = :id_course", array(':id_course' => $this->course_id))
			->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->andWhere("cus.status <> :waiting_status", array(':waiting_status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST))
			->andWhere("cus.status <> :subs_confirm", array(':subs_confirm' => LtCourseuserSession::$SESSION_USER_CONFIRMED))
			->order($orderByInput ? $orderByInput : "u.userid ASC, u.firstname ASC, u.lastname ASC");

		//--- power user filter ---
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
			$command->join(CoreUserPU::model()->tableName() . ' pu', 'u.idst = pu.user_id AND puser_id = :puser_id', array(':puser_id' => Yii::app()->user->id));
		//---

		if ($searchInput)
			$command->andWhere('CONCAT(u.firstname, " ", u.lastname) like :search OR u.email LIKE :search OR u.userid LIKE :search', array(':search' => '%' . $searchInput . '%'));

		$dataProvider = new CSqlDataProvider($command, array(
			'totalItemCount' => $count,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
		));

		// $dataProvider->getData() will return a list of arrays.
		return $dataProvider;
	}

	/**
	 * Create data source for instructor's evaluation page grid
	 *
	 * @param int $idSession
	 * @return \CArrayDataProvider
	 * @throws CException
	 */
	public function dataProviderEvaluation($idSession) {

		//read search input filters
		$rq = Yii::app()->request;
		$searchFilter = $rq->getParam('search-filter-status', 'all');
		$searchText = trim($rq->getParam('searchText', ''));

		//prepare internal variables
		$arrData = array();
		$sessionModel = LtCourseSession::model()->findByPk($idSession);
		if (empty($sessionModel)) { throw new CHttpException('Invalid specified session'); }

		//retreive users enrolled to the session. The grid will have an user per row.
		$uinfo = array();
                  $pUserRights = Yii::app()->user->checkPURights($sessionModel->course_id);
		$users = $sessionModel->getEnrolledUsers(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT, true, $pUserRights->isInstructor);
		if (!empty($users)) {

			$uids = array(); //this will be simply a list of users idsts
			$evaluations = array(); //this wil collect evaluation info, organizad by user idst
			$attendance = array(); //this will collect info aboouyt days attendance, organized by user idst

			//prepare a row for each user in $arrData variable
			foreach ($users as $user) {

				//check search options first
				$toAdd = true;
				switch ($searchFilter) {
					case 'waiting': {
						$toAdd = (!is_numeric($user['evaluation_status']));//empty($user['evaluation_score']);
					} break;
					case 'already': {
						$toAdd = (is_numeric($user['evaluation_status']));//(!empty($user['evaluation_score']) || $user['evaluation_score'] === 0);
					} break;
				}

				//collect users to be displayed in the grid
				if ($toAdd) {
					$uids[] = $user['id_user'];
					$evaluations[$user['id_user']] = array(
						'date' => $user['evaluation_date'] ? Yii::app()->localtime->toLocalDate($user['evaluation_date']) : null,
						'score' => $user['evaluation_score'],
						'status' => $user['evaluation_status']
					);
				}
			}

			//retrieve user info
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $uids);
			if ($searchText) {
				$criteria->addCondition("CONCAT(firstname,' ',lastname) LIKE :search_text OR userid LIKE :search_text");
				$criteria->params[':search_text'] = '%'.$searchText.'%';
			}
			$urecords = CoreUser::model()->findAll($criteria);
			if (!empty($urecords)) {
				foreach ($urecords as $urecord) {
					$uinfo[$urecord->getPrimaryKey()] = array(
						'userid' => Yii::app()->user->getRelativeUsername($urecord->userid),
						'firstname' => $urecord->firstname,
						'lastname' => $urecord->lastname
					);
				}
			}
			//(free some memory)
			$uids = NULL;
			$urecords = NULL;
			unset($uids);
			unset($urecords);

			//retrieve attendance date and evaluation info


			//retrieve raw data from attendance table
			$arecords = LtCourseSessionDateAttendance::model()->findAllByAttributes(array('id_session' => $idSession));
			if (!empty($arecords)) {
				foreach ($arecords as $arecord) {
					$attendance[(int)$arecord->id_user][] = $arecord->day;
				}
			}

			//turn raw data into organized data
			foreach ($uinfo as $uid => $user) {

				//preapre grid row data
				$row = array();

				$row['id_user'] = $uid;
				$row['userid'] = $user['userid'];
				$row['firstname'] = $user['firstname'];
				$row['lastname'] = $user['lastname'];
				$row['evaluation_score'] = (isset($evaluations[$uid]) ? $evaluations[$uid]['score'] : false);
				$row['evaluation_date'] = (isset($evaluations[$uid]) ? $evaluations[$uid]['date'] : false);
				$row['evaluation_status'] = (isset($evaluations[$uid]) ? $evaluations[$uid]['status'] : false);

				$ainfo = array();
				if (!empty($attendance)) {
					if (isset($attendance[(int)$uid])) {
						$ainfo = $attendance[(int)$uid];
					}
				}
				$row['attendance'] = $ainfo;

				$arrData[] = $row;
			}

			//(free some memory)
			$uinfo = NULL;
			$evaluations = NULL;
			unset($uinfo);
			unset($evaluations);
		}
		//(free memory)
		$users = NULL;
		unset($users);

		//sort data by userid
		$sorter = function($a, $b) {
			if ($a['userid'] == $b['userid']) { return 0; }
			return ($a['userid'] > $b['userid'] ? 1 : -1);
		};
		usort($arrData, $sorter);

		//prepare provider object with organizad data
		return new CArrayDataProvider($arrData);
	}



	public function dataProviderWaiting($params = array())
	{

		// get assigned users to this session with status 'waiting'
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'learningUser' => array(
				'alias' => 'user'
			),
			'ltCourseSession' => array(
				'condition' => 'ltCourseSession.course_id = :cu_idCourse',
				'params' => array(
					':cu_idCourse' => $this->course_id
				)
			),
			'learningCourseuser' => array(
				'with' => array(
					'learningUserSubscribedBy'
				)
			)
		);
		$criteria->addCondition('t.id_session = :id_session');
		$criteria->addCondition('t.status = :status');
		$criteria->params[':id_session'] = $this->id_session;
		$criteria->params[':status'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%' . $userQuery . '%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%' . $userSearch[0] . '%';
		}

		//check if we have to apply filter for power user
		if (isset($params['applyPowerUserFilter']) && $params['applyPowerUserFilter'] && Yii::app()->user->getIsPu()) {
			$criteria->join = " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND pu.user_id = t.id_user) ";
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}

		$config['criteria'] = $criteria;

		//$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		$dataProvider = new CActiveDataProvider(LtCourseuserSession::model(), $config);

		// manually set the keys array so that the grid selector works
		if ($dataProvider->getTotalItemCount() > 0) {
			$keys = array();
			foreach ($dataProvider->getData() as $i => $data)
				$keys[$i] = $data->id_user;
			$dataProvider->setKeys($keys);
		}

		return $dataProvider;
	}




	public function sessionEnrollmentDataProvider($courseId, $showOverbookedSessions = true, $hideCurrentUserSubscribedSessions = false)
	{

		$criteria = new CDbCriteria();

		$criteria->with = array(
			'learningCourseSessionDates' => array(
				'alias' => 'dates',
				'order' => 'day ASC'
			)
		);

		if ($hideCurrentUserSubscribedSessions) {
			$criteria->join = 'LEFT JOIN ' . LtCourseuserSession::model()->tableName() . ' as users ON users.id_session = t.id_session AND users.id_user = ' . Yii::app()->user->id;
			$criteria->addCondition('users.id_user IS NULL');
		}

		$criteria->addCondition('course_id = :course_id');
		$criteria->params[':course_id'] = $courseId;
		$criteria->addCondition('t.date_end > :date_end');
		$criteria->params[':date_end'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		//Show only sessions with last_subscription_date > now
		$criteria->addCondition("t.last_subscription_date = :not_set OR t.last_subscription_date IS NULL OR t.last_subscription_date = '' OR t.last_subscription_date > :last_subscription_date");
		$criteria->params[':not_set'] = '0000-00-00 00:00:00';
		$criteria->params[':last_subscription_date'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		if (!$showOverbookedSessions) {
			// show only sessions that are not overbooked
			$criteria->addCondition('t.max_enroll > (
				select count(*)
				from '.LtCourseuserSession::model()->tableName().' cus
				join '.LearningCourseuser::model()->tableName().' as cu on cu.idUser = cus.id_user and cu.idCourse = :course_id
				where cus.id_session = t.id_session)');
		}

		$excludedSessions = array();
		Yii::app()->event->raise('OnCourseSessionsQuery', new DEvent($this, array(
			'excludedSessions' => &$excludedSessions
		)));
		if (!empty($excludedSessions)) {
			$criteria->addNotInCondition('t.id_session', $excludedSessions);
		}

		$config = array(
			'criteria' => $criteria,
			'sort' => array(
				'attributes' => 'date_begin',
				'defaultOrder' => 'date_begin ASC'
			)
		);

		return new CActiveDataProvider($this, $config);

	}



	public function renderSessionActions($viewPath = false) {

		if (!$viewPath) {
			$viewPath = '_sessionTableActions';
		}
		$sessionActions = Yii::app()->getController()->renderPartial($viewPath, array(
			'idSession' => $this->id_session,
			'idCourse' => $this->course_id,
			'name' => $this->name,
			'model'=>$this,
		), true);

		if(strpos($sessionActions, '<li') !== FALSE) {
			$content = Chtml::link('', 'javascript:void(0);', array(
				'id' => 'popover-'.uniqid(),
				'class' => 'popover-action popover-trigger',
				'data-toggle' => 'popover',
				'data-content' => $sessionActions,
			));
		} else
			$content = '';

		return $content;
	}


	/**
	 * Renders the 'waiting' column of the sessions grid
	 * @return string
	 */
	public function renderWaiting() {

			$count = $this->countWaiting(true);

		if ($count == 0)
			return '0';

		if (Yii::app()->user->getIsPu()) {
			$isPuWithSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
			$isPuWithSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
			$isSessionOwnedByPu = ($this->created_by == Yii::app()->user->getIdst());
			if ( !($isSessionOwnedByPu && $isPuWithSessionAddPerm) && !(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign'))) {
					// PU doesn't have course waiting management permission OR the session was not created by him and he has not the edit permission for sessions
					// In this case, just display the number of waiting users as string
					return $count;
				}
			}

		return CHtml::link($count, Docebo::createAppUrl('admin:ClassroomApp/session/waiting', array(
			'course_id' => $this->course_id,
			'id_session' => $this->getPrimaryKey()
		)), array('class' => 'waiting-users'));
	}


	public function renderDetailedSessionName($separator = '<br/>') {
		$html = $this->name . $separator
			. ucfirst(Yii::t('standard', '_FROM')) . ' ' . $this->date_begin
			. ' ' . Yii::t('standard', '_TO') . ' ' . $this->date_end;
		return $html;
	}


	/**
	 * Calculates the number of attendance hours for the current session
	 * @throws CException
	 */
	public function getUserAttendance($idUser)
	{

		// get the attended dates
		$attendedDates = LtCourseSessionDateAttendance::model()->with('ltCourseSessionDate')->findAllByAttributes(array(
			'id_session' => $this->id_session,
			'id_user' => $idUser
		));

		if (!$attendedDates) return 0;

		$totalHours = 0;

		foreach ($attendedDates as $attendedDate) {

			$dateBegin = NULL;
			$dateEnd = NULL;

			/* @var $date LtCourseSessionDate */
			$date = $attendedDate->ltCourseSessionDate;

			// Build full datetime start and begin of the date
			$dateBegin = $date->day . ' ' . $date->time_begin;
			$dateEnd = $date->day . ' ' . $date->time_end;

			// Calculate hours in this date
			$time1 = CDateTimeParser::parse($dateBegin, "yyyy-MM-dd hh:mm:ss");
			$time2 = CDateTimeParser::parse($dateEnd, "yyyy-MM-dd hh:mm:ss");

			if ($date->break_begin && $date->break_end) {
				$time3 = CDateTimeParser::parse($date->day . ' ' . $date->break_begin, "yyyy-MM-dd hh:mm:ss");
				$time4 = CDateTimeParser::parse($date->day . ' ' . $date->break_end, "yyyy-MM-dd hh:mm:ss");
				$hours1 = ($time3 - $time1) / 3600;
				$hours2 = ($time2 - $time4) / 3600;
				$totalHours += $hours1 + $hours2;
			} else
				$totalHours += ($time2 - $time1) / 3600;
		}

		return $totalHours;
	}

	public function renderAttendance($idUser)
	{
		$userAttendance = $this->getUserAttendance($idUser);
		$event = new DEvent($this, array('userAttendance' => &$userAttendance, 'userId' => $idUser));
		Yii::app()->event->raise('GetDeductedTimeForSession', $event);
		if(!$event->shouldPerformAsDefault()){
			return $userAttendance . '/' . $this->total_hours . 'h';
		}
		return number_format($userAttendance, 1) . 'h/' . $this->total_hours . 'h';
	}

	public function convertHoursToMinutes($currentHour = null){
		// Converting hours to hh:mm for example 1.50 which is 1 hour and 30 minutes
		$hours = floor($currentHour);
		$minutes = str_pad( round((($currentHour - $hours)*60)), 2, 0, STR_PAD_LEFT);
		$hours = str_pad($hours, 2, 0, STR_PAD_LEFT);
		return sprintf('%s:%s', $hours, $minutes);
	}

	/**
	 * Renders the enroll column of session grid
	 * @param bool $isInstructor
	 * @return string
	 */
	public function renderEnrolled($isInstructor = false, $params = array('disallowPUEnrollCoursesWithLPs' => false, 'tooltip' => false, 'tooltipTitle' => ''))
	{
		// set default values
		$content = '';
		$titleContent = $params['tooltipTitle'];
		$pUserRights = Yii::app()->user->checkPURights($this->course_id);

		//enrollments screen is visible to any power users (with or without enroll permission)
		//actions that admins/PUs can perform on users depend on permission, though
		$isPuWithCourseEnrollPerm = $pUserRights->all && Yii::app()->user->checkAccess('enrollment/view');
		$isPuWithSessionAddPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add");
		$isPuWithSessionAssign = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/assign");
		$isSessionOwnedByPU = ($this->created_by == Yii::app()->user->getIdst());
				$course_associated = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $this->course_id)) ? true : false;

		//check if we are standard users in the LMS and set permissiosn accordingly
		$isUser = (Yii::app()->user->isUser);
		$isUserAndInstructor = false; //initialize value
		if ($isUser) {
			$enrollmentModel = LearningCourseuser::model()->findByAttributes(array('idUser' => Yii::app()->user->id, 'idCourse' => $this->course_id));
			$isUserAndInstructor = $enrollmentModel->level == LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR;
		}

		//the same as above, but for Power User case
		$isPuAndCanEnroll = false;
		if (Yii::app()->user->getIsPu() && $course_associated) {
			if ($isPuWithCourseEnrollPerm) {
				if ($isSessionOwnedByPU) {
					$isPuAndCanEnroll =  ($isPuWithSessionAssign || $isPuWithSessionAddPerm);
				} else {
					$isPuAndCanEnroll = $isPuWithSessionAssign;
				}
			}
		}

		if ($isInstructor) {
			$canEnroll = false;
		} else {
			$canEnroll = (Yii::app()->user->getIsGodadmin() || $isPuAndCanEnroll);
		}

		// Count already enrolled users
		$countEnrollments = $this->countEnrollments(!$pUserRights->isInstructor, true);

		//build content
		if ($countEnrollments > 0 || ($countEnrollments <= 0 && !$canEnroll)) {

			$countUsers = $countEnrollments;
			$content = $countUsers . '<span class="enrolled"></span>';

		} else {

			//get the LPs, to which the course is assigned
			$courseModel = LearningCourse::model()->findByPk($this->course_id);
			$itemsLPs = $courseModel->getCourseLPs();

			//if user is PU and there are LPs to which the course is assigned
			if ($params['disallowPUEnrollCoursesWithLPs'] && Yii::app()->user->isAdmin && (count($itemsLPs) > 0)) {
				$content = '<span class="enroll enrolled_gray">' . Yii::t('standard', 'Enroll') . '</span>';
				$canEnroll = false;
			} else {
				//this will be showed only to god admin and power users with appropriate enroll permission
				$content = '<span class="enroll">' . Yii::t('standard', 'Enroll') . '</span>';
			}

		}

		//build the link encapsulating the content (if a link needed)
		if ($canEnroll  || $params['disallowPUEnrollCoursesWithLPs'] || $isInstructor) {
			if (!Yii::app()->user->getIsPu() || $isPuAndCanEnroll || (Yii::app()->user->getIsPu() && !$isPuAndCanEnroll) || $pUserRights->isInstructor || $isUserAndInstructor) {
				//get the LPs, to which the course is assigned
				if (!isset($courseModel) || !isset($itemsLPs)) { //avoid possible re-calculation of this data (see above)
					$courseModel = LearningCourse::model()->findByPk($this->course_id);
					$itemsLPs = $courseModel->getCourseLPs();
				}
				if (!($params['disallowPUEnrollCoursesWithLPs'] && Yii::app()->user->isAdmin && (count($itemsLPs) > 0))) {
					$content = CHtml::link($content, Docebo::createAppUrl('admin:ClassroomApp/enrollment/index', array(
						'course_id' => $this->course_id,
						'id_session' => $this->getPrimaryKey()
					)));
				} else {
					if (!$params['tooltip']) {
						// don't show tooltip
						$content = CHtml::link($content, 'javascript:;');
					} else {
						//show a tooltip
						$content = CHtml::link($content, '#!', array(
							'title' => $titleContent,
							'rel' => 'tooltip',
							'data-html' => true,
							'class' => 'lps-list',
							'onclick' => 'javascript:;'
						));
					}
				}
			}
		}

		return $content;
	}




	/**
	 * Render locations in session grids (shows the tooltip as well)
	 * @return string
	 */
	public function renderLocations($onlyText = false) {
		$output = '';

		// Get the list of locations
		$locations = $this->getLocations();
		if (!empty($locations)) {
			if (count($locations) == 1)
				$text = $title = $locations[0]['name']; // Just one location
			else {
				// Multiple locations to show!! -> Show a message and the list in the tooltip
				$text = Yii::t('classroom', 'Multiple locations') . ' (' . count($locations) . ')';
				$title = '';
				foreach ($locations as $location)
					$title .= $location['name'] . "<br/>";
			}

			$output = CHtml::link($text, '#', array(
				'title' => $title,
				'rel' => 'tooltip',
				'data-html' => true,
				'class' => 'location-list',
				'onclick' => 'javascript:;'
			));
			if ($onlyText === true) {
				$output = $text;
			}
		}

		return $output;
	}


	public function renderInstructorAction($type) {
		$content = '';
		switch ($type) {
			case 'details': {
				$href = Docebo::createLmsUrl('player/training/session', array(
					'course_id' => $this->course_id,
					'session_id' => $this->getPrimaryKey()
				));
				$content = Chtml::link('', $href, array(
					'id' => 'action-details-'.$this->getPrimaryKey(),
					'class' => 'classroom-sprite details'
				));
			} break;
			case 'evaluation':
			case 'attendance': {
				$href = Docebo::createLmsUrl('ClassroomApp/instructor/evaluation', array(
					'course_id' => $this->course_id,
					'id_session' => $this->getPrimaryKey()
				));
				$content = Chtml::link('', $href, array(
					'id' => 'action-attendance-'.$this->getPrimaryKey(),
					'class' => 'classroom-sprite attendance'
				));
			} break;
			case 'print': {
				if ($this->countDates() > 0) { //printing attendance sheet makes sense only if we have some dates
					$href = Docebo::createLmsUrl('ClassroomApp/instructor/axAttendanceSheet', array(
						'course_id' => $this->course_id,
						'id_session' => $this->getPrimaryKey()
					)); //'javascript:void(0);'
					$content = Chtml::link('', $href, array(
						'id' => 'action-print-'.$this->getPrimaryKey(),
						'class' => 'open-dialog classroom-sprite print',
						'data-dialog-class' => 'attendance-sheet-print-dialog'
					));
				} else {
					$content = '-';
				}
			} break;
		}
		return $content;
	}


	/**
	 * Render the grid actions for instructor in instructor attendances/evaluations management for sessions
	 *
	 * @param int $idUser
	 * @param int $idSession
	 * @param date $day
	 * @param int/boolean $status user has passed the class session (1/0), or not evaluated yet (false)
	 * @param boolean/float $score user score obtained in the session
	 * @param boolean/float $scoreMax session max score
	 * @return string
	 */
	public function renderEvaluationScore($idUser, $idCourse, $idSession, $day, $status = false, $score = false, $scoreMax = false) {

		//check if the user has already been evaluated
		if ($status === false || $status === null) { //beware: $score may be === 0 and in that case it must be treated as a valid score
			return CHtml::link(
				Yii::t('classroom', 'Evaluate'),
				Docebo::createLmsUrl('ClassroomApp/instructor/axEvaluateUser', array('course_id' => $idCourse, 'id_session' => $idSession, 'id_user' => $idUser)),
				array(
					'class' => 'evaluate-user open-dialog',
					'data-id_user' => $idUser,
					'data-day' => $day,
					'data-dialog-class' => 'edit-evaluation-dialog'
				)
			);
		}

		//display user evaluation
		$html = '';
		$html .= '<div id="evaluation-container-'.$idUser.'" class="evaluation-actions-envelope popover-trigger" data-id_user="'.$idUser.'">';

		$score = trim($score);

		if (is_numeric($status)) {
			$status = intval($status);

			$cssClass = '';
			$appendHtml = '';

			switch ($status) {
				case LtCourseuserSession::EVALUATION_STATUS_PASSED:
					$cssClass = 'good';
					$appendHtml = '<span class="classroom-sprite approved-small popover-trigger"></span>';
					break;
				case LtCourseuserSession::EVALUATION_STATUS_FAILED:
					$cssClass = 'bad';
					$appendHtml = '<span class="classroom-sprite unapproved-small popover-trigger"></span>';
					break;
				default: break;
			}
			$html .= '<span class="evaluation-score popover-trigger '.$cssClass.'">'.$score.'</span>/'.$scoreMax;
			$html .= '&nbsp;';
			$html .= $appendHtml;
		}

		$html .= '</div>';

		return $html;
	}


	/**
	 * Retrieves the list of dates for the current session object
	 * @return array|LtCourseSessionDate[]
	 */
	public function getDates() {
		$criteria = new CDbCriteria();
		$criteria->addCondition("id_session = :id_session");
		$criteria->params[':id_session'] = $this->getPrimaryKey();
		$criteria->order = "day ASC";
		$list = LtCourseSessionDate::model()->findAll($criteria);
		return (!empty($list) ? $list : array());
	}


	/**
	 * Retrieves the number of dates for the current session object
	 * @return integer
	 */
	public function countDates() {
		$output = Yii::app()->db->createCommand()
			->select("COUNT(*) AS num_dates")
			->from(LtCourseSessionDate::model()->tableName()." csu")
			->where("csu.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->queryScalar();
		return (int)$output;
	}


	/**
	 * Updates the dates for the current session object
	 * @param array $dates Array of dates
	 * @throws CException
	 * @throws Exception
	 */
	private function saveSessionDates(array $newDates) {
		// Get already present session dates
		$tmpDates = $this->getDates();
		$prevDates = array();
		foreach ($tmpDates as $date)
			$prevDates[$date->day] = $date;

		/* @var $date LtCourseSessionDate */

		// Delete days no more present
		foreach ($prevDates as $date) {
			if (!isset($newDates[$date->day])) {
				if (!$date->delete())
					throw new CException("Error while updating session dates");
			}
		}

		// Loop through the new dates and create/update them
		foreach ($newDates as $day => $newDateObj) {
			$date = $prevDates[$day];
			if (!$date) {
				$date = new LtCourseSessionDate();
				$newDateObj['id_session'] = $this->getPrimaryKey();
			}

			//When break times aren't set, then force them to '00:00' (this is the default way they are stored in database).
			//On some LMSs, empty values may cause errors when saving them that way.
			if (isset($newDateObj['break_begin']) && empty($newDateObj['break_begin'])) { $newDateObj['break_begin'] = '00:00'; }
			if (isset($newDateObj['break_end']) && empty($newDateObj['break_end'])) { $newDateObj['break_end'] = '00:00'; }

			//since break times simply cannot have value "00:00" then if just one of those is set like that, put both of them as "00:00" (meaning no break time has been set)
			if (isset($newDateObj['break_begin']) && $newDateObj['break_begin'] == '0:00') { $newDateObj['break_begin'] = '00:00'; }
			if (isset($newDateObj['break_end']) && $newDateObj['break_end'] == '0:00') { $newDateObj['break_begin'] = '00:00'; }
			if (!isset($newDateObj['break_begin']) || $newDateObj['break_begin'] == '00:00' || !isset($newDateObj['break_end']) || $newDateObj['break_end'] == '00:00') {
				$newDateObj['break_begin'] = '00:00';
				$newDateObj['break_end'] = '00:00';
			}

			$date->attributes = $newDateObj;

			if (!$date->save())
				throw new CException("Error while updating session dates");
		}
	}



	/**
	 * Updates the calculated columns for the current session object (e.g. date_begin, date_end, total_hours)
	 * @throws CException
	 */
	public function updateCalculatedSessionColumns() {
		$this->date_begin = NULL;
		$this->date_end = NULL;
		$this->total_hours = 0;

		// Reload dates from DB (ordered in ascending day order)
		$dates = $this->getDates();
		$dateBegin = NULL;
		$timezoneBegin = NULL;
		$timezoneEnd = NULL;
		$dateEnd = NULL;
		foreach ($dates as $date) {
			/* @var $date LtCourseSessionDate */

			// Build full datetime start and begin of the dat
			$dateBegin = $date->day . ' ' . $date->time_begin;
			$dateEnd = $date->day . ' ' . $date->time_end;
			$timezoneEnd = $date->timezone;

			// Set start date
			if ($this->date_begin === NULL) {
				$this->date_begin = $dateBegin;
                $timezoneBegin = $date->timezone;
			}

			// Calculate hours in this date
			$time1 = CDateTimeParser::parse($dateBegin, "yyyy-MM-dd hh:mm:ss");
			$time2 = CDateTimeParser::parse($dateEnd, "yyyy-MM-dd hh:mm:ss");

			if ($date->break_begin && $date->break_end) {
				$time3 = CDateTimeParser::parse($date->day . ' ' . $date->break_begin, "yyyy-MM-dd hh:mm:ss");
				$time4 = CDateTimeParser::parse($date->day . ' ' . $date->break_end, "yyyy-MM-dd hh:mm:ss");
				$hours1 = ($time3 - $time1) / 3600;
				$hours2 = ($time2 - $time4) / 3600;
				$this->total_hours += $hours1 + $hours2;
			} else
				$this->total_hours += ($time2 - $time1) / 3600;
		}

		$this->total_hours = number_format($this->total_hours, 2);

		// Set end date
		$this->date_end = $dateEnd;

		// convert date_begin/date_end to local date/time (we can be sure they come in ISO format)
		$this->date_begin = Yii::app()->localtime->toLocalDateTime($this->date_begin, 'short', 'short', null, $timezoneBegin);
		$this->date_end = Yii::app()->localtime->toLocalDateTime($this->date_end, 'short', 'short', null, $timezoneEnd);

		// Update calculated columns
		if (!$this->save())
			throw new CException('Error while updating session dates info');
	}



	/**
	 * Counts the users enrolled to this session. Users can be filtered by power user visibility.
	 *
	 * @param boolean $adminFilter IDST of the power user (if set to false, no filter will be applied and all users will be counted)
	 * @return integer the calculated number of users enrolled to this session
	 */
	public function countEnrollments($adminFilter = true, $excludeWaiting = false) {
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS num_enrolled")
			->from(LtCourseuserSession::model()->tableName()." cus"); // TODO: it may be better to join this with learning_courseuser, to avoid potential incoherent data
		if ($adminFilter && Yii::app()->user->getIsAdmin()) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = cus.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}
		$command->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
		if ($excludeWaiting) {
			$command->andWhere("cus.status <> :waiting_status", array(':waiting_status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST));
			$command->andWhere("cus.status <> :subs_confirm", array(':subs_confirm' => LtCourseuserSession::$SESSION_USER_CONFIRMED));
		}
		$rs = $command->queryAll();
		return (int)$rs[0]['num_enrolled'];
	}



	/**
	 * Counts the users enrolled to this session, but their status is 'waiting'
	 *
	 * @param $filterByPowerUser bool if the result has to be filtered by current logged PU or not
	 * @return string
	 */
	public function countWaiting($filterByPowerUser = false)
	{
		$params = array(
			':id_session' => $this->id_session,
			':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST
		);
		$query = "SELECT COUNT(*) AS num_waiting "
			." FROM ".LtCourseuserSession::model()->tableName()." cus ";
		if ($filterByPowerUser && Yii::app()->user->getIsPu()) {
			$query .= " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND cus.id_user = pu.user_id)";
			$params[':puser_id'] = Yii::app()->user->id;
		}
		$query .= " WHERE cus.id_session = :id_session AND cus.status = :status";

		return Yii::app()->db->createCommand($query)->queryScalar($params);
	}



	public function renderEnrollmentActions() {
		return '';
	}



	/**
	 * Returns an array of CoreUser object for users enrolled in this session
	 * @return mixed
	 */
	public function getEnrolledUsersExtended() {
		// Collect all idst for users enrolled to this session
		$command = Yii::app()->db->createCommand()
						->select("id_user")
						->from(LtCourseuserSession::model()->tableName() . " cus")
						->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
		$enrolledUserIds = $command->queryColumn();

		// Get the list of CoreUser objects with the found idsts
		$enrolledUsers = CoreUser::model()->findAllByAttributes(array('idst' => $enrolledUserIds));

		return $enrolledUsers;
	}



	/**
	 * Get a list of enrolled users to this session. Users can be filtered by level.
	 *
	 * @param integer $level if set, only users with the specifed level will be retrieved
	 * @return array a list of records (arrays) retrieved
	 */
	public function getEnrolledUsers($level = false, $excludeWaiting = false, $noAdminCheck = false, $orderBy = false) {
		$command = Yii::app()->db->createCommand()
			->select("cus.*, cs.course_id, cu.level, cu.status")
			->from(LearningCourseuser::model()->tableName()." cu")
			->join(CoreUser::model()->tableName().' u', 'u.idst = cu.idUser')
			->join(LtCourseSession::model()->tableName()." cs", "cs.course_id = cu.idCourse")
			->join(LtCourseuserSession::model()->tableName()." cus", "cu.idCourse = cs.course_id AND cus.id_session = cs.id_session AND cus.id_user = cu.idUser")
			->where("cs.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));

		if ($excludeWaiting) {
			$command->andWhere("cus.status <> :status", array(':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST));
		}

		if ($level != false) {
			$command->andWhere("cu.level = :level", array(':level' => $level));
		}

		if (!$noAdminCheck && Yii::app()->user->getIsAdmin()) {
			$command->join(
				CoreUserPU::model()->tableName() . " pu",
				"pu.user_id = cus.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}

        switch ($orderBy) {
            case 'fullname' :
                $command->order("CONCAT(u.firstname, ' ', u.lastname)");
                break;
            case 'email' :
                //show empty rows last
                $command->order("IF(u.email = '' OR u.email IS NULL,1,0),u.email");
                break;
            case 'language' :
                $command->leftJoin(CoreSettingUser::model()->tableName(). ' csu', 'csu.id_user = u.idst AND csu.path_name = "ui.language"');
                $command->order("IF(csu.value = '' OR csu.value IS NULL,1,0),csu.value");
                break;
            default :
                $command->order("u.userid");
                break;
        }

		// Raise event when building the list with user's attendance
		Yii::app()->event->raise('onBuildAttendanceSheet', new DEvent($this, array('query' => &$command)));

		return $command->queryAll();
	}



	/**
	 * Get a count of enrolled users to this session. USers can be filtered by level.
	 *
	 * @param integer $level if set, only users with the specifed level will be counted
	 * @return integer the number of users enrolled
	 */
	public function countEnrolledUsers($level = false, $excludeWaiting = false/*, $noAdminCheck = false*/) {
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(LearningCourseuser::model()->tableName()." cu")
			->join(LtCourseSession::model()->tableName()." cs", "cs.course_id = cu.idCourse")
			->join(LtCourseuserSession::model()->tableName()." cus", "cu.idCourse = cs.course_id AND cus.id_session = cs.id_session AND cus.id_user = cu.idUser")
			->where("cs.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));

		if ($excludeWaiting) {
			$command->andWhere("cus.status <> :status", array(':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST));
		}

		if ($level != false) {
			$command->andWhere("cu.level = :level", array(':level' => $level));
		}

		//NOTE: until now this function has worked without admin filter, but it has to be made sure if this is right or not.
		//TODO: check if power user filter is needed here
		/*if (!$noAdminCheck && Yii::app()->user->getIsAdmin()) {
			$command->join(
				CoreUserPU::model()->tableName() . " pu",
				"pu.user_id = cus.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}*/

		return $command->queryScalar();
	}



	/**
	 * Removes an user enrollment from this session and performs related operations.
	 *
	 * @param integer $idUser the id of the users to be unenrolled to the session
	 * @return boolean if the operations has been successful or not
	 * @throws CException
	 */
	public function removeEnrolledUser($idUser) {

		$db = Yii::app()->db;

		// retrieve enrollment info
		$enroll = LtCourseuserSession::model()->findByPk(array(
			'id_session' => $this->getPrimaryKey(),
			'id_user' => $idUser
		));

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			//validate retrieved values
			if (!$enroll) { throw new CException('Invalid enrollment specification'); }

			//perform physical unenrollment. The ->delete() function will call related events.
			if (!$enroll->delete()) { throw new CException('Error while deleting user'); }

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return true;
	}



	/**
	 * Retrieve a list of locations in this session.
	 *
	 * @return array list of records (arrays) of locations
	 */
	public function getLocations() {
		return Yii::app()->db->createCommand()
			->select("l.*")
			->from(LtLocation::model()->tableName()." l")
			->join(LtCourseSessionDate::model()->tableName()." csd", "l.id_location = csd.id_location")
			->where("csd.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->group("l.id_location")
			->order("l.name ASC")
			->queryAll();
	}


	/**
	 * Copy this session into a new record.
	 * Dates and hours total will be calculated separately, since they depend on related data.
	 *
	 * @param string $newName the name of the new copied session
	 * @param boolean $duplicateDates if set to true, dates will be copied too
	 * @param integer $daysShift if dates are copied too, specify how many days they will be shifted in the calendar
	 * @return \LtCourseSession the copied session AR
	 * @throws CException
	 */
	public function copy($newName, $duplicateDates = false, $daysShift = 0) {

		$db = Yii::app()->db;
		$output = false;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
			// Clone session object
			$session = new LtCourseSession();
			$session->course_id = $this->course_id;
			$session->name = (!empty($newName) ? $newName : 'Copy of ' . $this->name);
			$session->max_enroll = $this->max_enroll;
			$session->min_enroll = $this->min_enroll;
			$session->score_base = $this->score_base;
			$session->other_info = $this->other_info;
			$session->notify_enrolled = $this->notify_enrolled;
			$session->evaluation_type = $this->evaluation_type;

			if (!$session->save())
				throw new CException('Error while copying session');

			// Make copy even if duplicate dates is not checked
			$dates = $this->getDates();
			foreach($dates as $date) {
				/* @var $date LtCourSessionDate */
				if($duplicateDates) {
					$date->copy($session->getPrimaryKey(), $daysShift);
				} else {
					// Make copy with the same dates
					$date->copy($session->getPrimaryKey(), 0);
				}
			}

			// Update calculated columns
			$session->updateCalculatedSessionColumns();

			if (isset($transaction)) { $transaction->commit(); }

			$output = $session;

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return $output;
	}


    /**
     * Returns an array containing the number and the percentage of the students who passed the session
     *
     * @return array
     */
    public function getPassStats() {

        $pUserRights = Yii::app()->user->checkPURights($this->course_id);
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id_session = :id_session');
        $criteria->params[':id_session'] = $this->id_session;
	   $criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
        //--- power user filter ---
	   $sessionModel = LtCourseSession::model()->findByPk($this->id_session);
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
		{
	        $puManagedUsers = CoreUserPU::getList();
	        $criteria->addInCondition('t.id_user', $puManagedUsers);
        }
        //---
        $totalEnrolled = LtCourseuserSession::model()->with(array('ltCourseSession', 'learningCourseuser'))->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->addCondition('t.id_session = :id_session');
        $criteria->addCondition('t.evaluation_status = :evaluation_status');
        $criteria->params[':id_session'] = $this->id_session;
        $criteria->params[':evaluation_status'] = LtCourseuserSession::EVALUATION_STATUS_PASSED;
	    $criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
        //--- power user filter ---
        if (Yii::app()->user->getIsPu())
	   {
	        $puManagedUsers = CoreUserPU::getList();
	        $criteria->addInCondition('t.id_user', $puManagedUsers);
        }
        //---
        $countPassed = LtCourseuserSession::model()->with('ltCourseSession', 'learningCourseuser')->count($criteria);

        if (0 == $totalEnrolled)
            $percentPassed = 0;
        else
            $percentPassed = number_format(doubleval($countPassed / $totalEnrolled) * 100, 1);

        return array(
            'total' => $totalEnrolled,
            'count' => $countPassed,
            'percent' => $percentPassed
        );
    }

    public function isMaxEnrolledReached()
	{
		$criteria = new CDbCriteria();

        $criteria->addCondition('t.id_session = :id_session');
        $criteria->params[':id_session'] = $this->id_session;

		$criteria->addCondition('t.status <> :status');
		$criteria->params[':status'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

		$students = LtCourseuserSession::model()->with(array('ltCourseSession', 'learningCourseuser'))->count($criteria);

        return $students >= $this->max_enroll;
    }

	/**
	 * Checks if there is only one location of all dates.
	 * If yes - returns the location, otherwise returns null. Because ICS file cannot have more than one location (I think.. :D )
	 * @return null|string|array
	 */
	public function icsLocation()
	{
		$location = null;

		$models = LtCourseSessionDate::model()->findAll(array(
            'condition' => 'id_session = :id_session',
            'group' => 'id_location',
            'params' => array(':id_session' => $this->id_session)
        ));

		if (count($models) == 1)
		{
			$ltLocation = $models[0]->ltLocation;
            $ltClassroom = $models[0]->ltClassroom;
			$location = "{$ltLocation->address}, {$ltLocation->country->name_country}-{$ltClassroom->name}";
		}

		return $location;
	}

	/**
	 * Generate UL with list of each date for the current session
	 * @return string
	 */
	public function compileShortcodeDates($textFormat = false)
	{
		if (!class_exists('NotificationTransportManager')) {
			//make sure to avoid a fatal error if notifications are not active and consequently abova class is not loaded
			$path = Yii::getPathOfAlias('plugin.NotificationApp.components.transports');
			spl_autoload_unregister(array('YiiBase','autoload'));
			require_once($path . DIRECTORY_SEPARATOR . 'NotificationTransportManager.php');
			spl_autoload_register(array('YiiBase','autoload'));
		}
		if (empty($textFormat)) $textFormat = NotificationTransportManager::TEXT_FORMAT_HTML; //TODO: this is just for backward compatibility, but should be removed later

		$sessionDates = '';
		if (count($this->learningCourseSessionDates))
		{
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$sessionDates = '<ul>';
					$courseModel = LearningCourse::model()->findByPk($this->course_id);
					foreach ($this->learningCourseSessionDates as $sessionDate) {
						$currentSessionTimezone = '';
						if(isset($sessionDate->timezone)){
							$currentSessionTimezone = Yii::app()->localtime->getTimezoneOffset($sessionDate->timezone);
						}
						if($courseModel && isset($courseModel->course_type) && ( $courseModel->course_type == 'telephone' ||  $courseModel->course_type == 'videoconference') ){
							$timeBegin = str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($sessionDate->time_begin),'',Yii::app()->localtime->toLocalDateTime($sessionDate->time_begin));
							$timeEnd = str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($sessionDate->time_end),'',Yii::app()->localtime->toLocalDateTime($sessionDate->time_end));
							$sessionDates .= '<li>' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' .$timeBegin. ' - ' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' .$timeEnd. ' ' . $currentSessionTimezone . ';<br> '.Yii::t('classroom', 'Location').':' . $sessionDate->ltLocation->address . ', ' . $sessionDate->ltLocation->country->name_country . '</li>';
						}else{
							$sessionDates .= '<li>' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' . $sessionDate->time_begin . ' - ' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' . $sessionDate->time_end . ' ' . $currentSessionTimezone . ' ;<br>  '.Yii::t('classroom', 'Location').':' . $sessionDate->ltLocation->address . ', ' . $sessionDate->ltLocation->country->name_country . '</li>';
						}
					}
					$sessionDates .= '</ul>';
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default: //fallback to plain text
					$courseModel = LearningCourse::model()->findByPk($this->course_id);
					foreach ($this->learningCourseSessionDates as $sessionDate) {
						$currentSessionTimezone = '';
						if(isset($sessionDate->timezone)){
							$currentSessionTimezone = Yii::app()->localtime->getTimezoneOffset($sessionDate->timezone);
						}
						if($courseModel && isset($courseModel->course_type) && ( $courseModel->course_type == 'telephone' ||  $courseModel->course_type == 'videoconference') ){
							$timeBegin = str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($sessionDate->time_begin),'',Yii::app()->localtime->toLocalDateTime($sessionDate->time_begin));
							$timeEnd = str_replace(Yii::app()->localtime->stripTimeFromLocalDateTime($sessionDate->time_end),'',Yii::app()->localtime->toLocalDateTime($sessionDate->time_end));
							$sessionDates .= "\t" . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' .$timeBegin. ' - ' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' .$timeEnd. ' ' . $currentSessionTimezone . ';'
								. "\n"."\t" .' '.Yii::t('classroom', 'Location').':' . $sessionDate->ltLocation->address . ', ' . $sessionDate->ltLocation->country->name_country . "\n";
						}else{
							$sessionDates .= "\t" . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' . $sessionDate->time_begin . ' - ' . Yii::app()->localtime->toLocalDate($sessionDate->day) . ' ' . $sessionDate->time_end . ' ' . $currentSessionTimezone . ' ;'
								. "\n"."\t" .'  '.Yii::t('classroom', 'Location').':' . $sessionDate->ltLocation->address . ', ' . $sessionDate->ltLocation->country->name_country . "\n";
						}
					}
					break;
			}

		}
		return $sessionDates;
	}

	public function lastSubscriptionDatePassed() {
		if (!empty($this->last_subscription_date) && $this->last_subscription_date != '0000-00-00 00:00:00') {
			$lastSubscriptionDateUtc = Yii::app()->localtime->fromLocalDateTime($this->last_subscription_date);
			$lastSubscriptionDateTimestamp = strtotime($lastSubscriptionDateUtc);
			$nowUtc = Yii::app()->localtime->getUtcNow();
			$nowTimestamp = strtotime($nowUtc);
			if ($lastSubscriptionDateTimestamp < $nowTimestamp)
				return true;
		}

		return false;
	}

	/**
	 * Get count of the available enrollments
	 * @return int
	 */
	public function getAvailableEnrollments()
	{
		$enrolled = $this->countEnrolledUsers(false, true);
		return ($enrolled >= $this->max_enroll) ? 0 : ($this->max_enroll - $enrolled);
	}

	/**
	 * Check if enrollments are available for the given session and users number (used from mass enrollment)
	 * @param $sessionId
	 * @param $usersCount
	 * @return bool
	 */
	public static function checkIfEnrollmentsAreAvailable($sessionId, $usersCount)
	{
		$res = false;
		$model = LtCourseSession::model()->findByPk($sessionId);
		if ($model)
		{
			$freeEnrollments = $model->getAvailableEnrollments();
			if ($usersCount <= $freeEnrollments)
				$res = true;
		}

		return $res;
	}


	/**
	 * Return a list of instructor(s) for this course session
	 * @param bool $models type of returned data: if true a list of AR models will be returned, otherwise only numeric IDs
	 * @return array list of instructors for this record's user (ARs or numeric IDs)
	 */
	public function findInstructors($models = true) {

		//now retrieve IDSTs of the instructors
		$instructorsIdsts = array();
		$cmd = Yii::app()->db->createCommand()
			->select("cus.id_user")
			->from(LtCourseuserSession::model()->tableName()." cus")
			->join(self::model()->tableName()." cs", "cs.id_session = cus.id_session")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
			->where("cs.course_id = :id_course", array(':id_course' => $this->course_id))
			->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			//->andWhere("cus.id_user <> :id_user", array(':id_user' => $this->id_user))
			->andWhere("cus.id_session = :id_session", array(':id_session' => $this->id_session));
		$reader = $cmd->query();
		if ($reader) {
			while ($row = $reader->read()) {
				$instructorsIdsts[] = $row['id_user'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		if ($models) {
			//find user records from instructors idsts
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $instructorsIdsts);
			$criteria->order = "firstname ASC, lastname ASC";
			return CoreUser::model()->findAll($criteria);
		} else {
			return $instructorsIdsts;
		}
	}

	/**
	 * Get last session by a given course
	 * @param $idCourse
	 * @return static
	 */
	public static function getLastSessionByDateBegin($idCourse, $idUser)
	{
		$criteria = new CDbCriteria(array(
			'condition' => 'course_id = :course_id',
			'params' => array(':course_id' => $idCourse),
			'order' => 'date_begin DESC',
		));

		// Add filter with lt_courseuser_session
		$criteria->with = array(
			'learningCourseuserSessions' => array(
				'condition' => 'learningCourseuserSessions.id_user = '.$idUser
			)
		);

		return self::model()->find($criteria);
	}

	/**
	 * Get the break time for the current Classroom Session in seconds (or in munutes)
	 *
	 * @param bool|false $inMinutes Return in minutes
	 * @return float|int The time interval of the breaks for all dates in this session
	 */
	public function getBreakTime($inMinutes = false){
		$criteria = new CDbCriteria();
		$criteria->addCondition("id_session = :id_session");
		$criteria->params[':id_session'] = $this->getPrimaryKey();
		$criteria->order = "day ASC";
		$dates = LtCourseSessionDate::model()->findAll($criteria);

		$time = 0;

		foreach($dates as $date){
			/**
			 * @var $date LtCourseSessionDate
			 */
			$time += Yii::app()->localtime->diff($date->break_begin, $date->break_end);
		}

		if($inMinutes){
			return $time / 60;
		}

		return $time;
	}


	/**
	 * Check if an user has played any learning object present in the course
	 * @param int $idUser IDST of the user to be checked (false/empty or negative value to use current logged user)
	 * @return bool
	 */
	public function userHasAlreadyPlayedSomeCourseLOs($idUser = false) {
		//check input, user can be passed or it can fallback to current logged user
		if (empty($idUser) || $idUser <= 0) {
			$idUser = Yii::app()->user->id;
		}

		//prepare output value
		$output = false;

		//first check if our course has any learning objects
		$countLOs = LearningOrganization::model()->count("idCourse = :id_course AND objectType <> ''", array(':id_course' => $this->course_id));
		if ($countLOs > 0) {
			//the course has some LOs, check if the user has started/completed any of them
			$t1 = LearningCommontrack::model()->tableName();
			$t2 = LearningOrganization::model()->tableName();
			$query = "SELECT COUNT(*) FROM $t1 t1 JOIN $t2 t2 "
				." ON (t1.idReference = t2.idOrg AND t2.idCourse = :id_course AND t2.objectType <> '' AND t1.status <> :ct_status AND t1.idUser = :id_user)";
			$params = array(
				':id_user' => $idUser,
				':id_course' => $this->course_id,
				':ct_status' => LearningCommontrack::STATUS_AB_INITIO
			);
			$countPlayedLOs = Yii::app()->db->createCommand($query)->queryScalar($params);
			if ($countPlayedLOs > 0) { $output = true; }
		}

		return $output;
	}


	public function getIcsDatesData($name, $description, $location = null, $organizerName = null, $organizerEmail = null, $type){
		$icsDatesData = '';
		$dates = $this->getDates();
		if($dates){
			foreach($dates as $date){
                $classroomName = '';
				$dateBegin = $date->day.' '.$date->time_begin;
				$dateEnd = $date->day.' '.$date->time_end;
				$timezone = $date->timezone;
				$dateBegin =  Yii::app()->localtime->toLocalDateTime($dateBegin, 'short', 'short', null, $timezone);
				$dateEnd =  Yii::app()->localtime->toLocalDateTime($dateEnd, 'short', 'short', null, $timezone);
				$dateLocation = $date->dateIcsLocation();
                $classroom = LtClassroom::model()->findByPk($date->id_classroom);
                if($classroom) {
                    $classroomName = "-{$classroom->name}";
                }
				$uid = $this->getPrimaryKey().'_'.$date->day;
				if($type == ICSManager::TYPE_CANCEL){
					$status = "STATUS:CANCELLED\r\n";
				}else{
					$status = "";
				}

				// Some checks for Organizer name and email if they are missing
				$organizer = "ORGANIZER;CN=";
				if($organizerName && $organizerEmail) {
					$organizer .= "{$organizerName}:MAILTO:{$organizerEmail}\r\n";
				} elseif($organizerName && !$organizerEmail) {
					$organizer .= "{$organizerName}\r\n";
				} elseif(!$organizerName && $organizerEmail) {
					$organizer .= "{$organizerEmail}\r\n";
				} else {
					$organizer = "";
				}

				$summary = Yii::t('standard', '_COURSE') . ': ' . $this->course->name . ', ' . Yii::t('classroom', 'Session') . ': ' . $name;

				$icsDatesData .=   "BEGIN:VEVENT\r\n"
					."DTSTART:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($dateBegin), 0, 19)).'Z'."\r\n"
					."DTEND:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($dateEnd), 0, 19)).'Z'."\r\n"
					."FREEBUSY;FBTYPE=BUSY:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($dateBegin), 0, 19)).'Z'."/".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($dateEnd), 0, 19)).'Z'."\r\n"
					."LOCATION:".($dateLocation ? $dateLocation : $location)."{$classroomName}\r\n"
					."TRANSP: OPAQUE\r\n"
					."SEQUENCE:0\r\n"
					."UID:".$uid."\r\n"
					.$status
					."DTSTAMP:".date("Ymd\THis\Z")."\r\n"
					."SUMMARY: ".$summary."\r\n"
					."DESCRIPTION:".$description."\r\n"
					."PRIORITY:1\r\n"
					//."ORGANIZER;CN=\"{$organizerName}\":{$organizerEmail}\r\n"
					.$organizer
					."CLASS:PUBLIC\r\n"
					."X-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\n"
					."X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\n"
					."X-MICROSOFT-CDO-IMPORTANCE:1\r\n"
					."BEGIN:VALARM\r\n"
					."TRIGGER:-PT15M\r\n" // reminder 60 minutes before the event
					."ACTION:DISPLAY\r\n"
					."DESCRIPTION:Reminder\r\n"
					."END:VALARM\r\n"
					."END:VEVENT\r\n";
			}
		}

		return $icsDatesData;
	}
	public function dataProviderUserWaiting($params = array()){

		$criteria = new CDbCriteria();

		$criteria->with = array(
			'ltCourseSession' => array(
				'condition' => 'ltCourseSession.course_id = :courseId',
			),
			'learningUser' => array(
				'alias' => 'user'
			)
		);
		$criteria->together = true;
		$criteria->params['courseId'] = $params['courseId'];
		$criteria->params['waitingStatus'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
		$criteria->addCondition('(t.waiting = 1 OR t.status = :waitingStatus)');

		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%' . $userQuery . '%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%' . $userSearch[0] . '%';
		}

		//check if we have to apply filter for power user
		if (isset($params['applyPowerUserFilter']) && $params['applyPowerUserFilter'] && Yii::app()->user->getIsPu()) {
			$criteria->join = " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND pu.user_id = t.id_user) ";
			$criteria->params[':puser_id'] = Yii::app()->user->id;

			$PU_hideOtherSessions = false;
			if (Yii::app()->user->isPu) {
				$pUserRights = Yii::app()->user->checkPURights($params['courseId']);
				$isPuWithSessionEditPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod");
				if (!$isPuWithSessionEditPerm) {
					$PU_hideOtherSessions = true;
				}

				if($PU_hideOtherSessions){
					$criteria->addCondition("created_by = :created_by");
					$criteria->params['created_by'] =  Yii::app()->user->id;
				}
			}
		}

		$config['criteria'] = $criteria;
		$config['sort']['defaultOrder'] = 'user.userid ASC';

		$dataProvider = new CActiveDataProvider(LtCourseuserSession::model(), $config);

		return $dataProvider;

	}
}

