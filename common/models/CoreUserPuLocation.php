<?php

/**
 * This is the model class for table "core_user_pu_location".
 *
 * The followings are the available columns in table 'core_user_pu_location':
 * @property integer $puser_id
 * @property integer $location_id
 */
class CoreUserPuLocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUserPuLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user_pu_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('puser_id, location_id', 'required'),
			array('puser_id, location_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('puser_id, location_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'puser_id' => 'Puser',
			'location_id' => 'Location',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('puser_id',$this->puser_id);
		$criteria->compare('location_id',$this->location_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	public function getList($idAdmin) {
		$db = Yii::app()->db;
		$output = array();
		$tmp = $db->createCommand()
			->select("location_id")
			->from(self::model()->tableName())
			->where("puser_id = :id_admin", array(':id_admin' => $idAdmin))
			->queryAll();
		//TO DO: make this more efficient by fetching manually the query result. Think about a caching method.
		if (!empty($tmp)) {
			foreach ($tmp as $tmpRecord) { $output[] = $tmpRecord['location_id']; }
		}
		return $output;
	}



	/**
	 * The number of assigned locations for a given power user
	 * @param int $idUser the idst of the power user
	 * @return int
	 */
	public function countUserLocations($idUser) {
		return Yii::app()->db->createCommand()
			->select("COUNT(*) AS count_locations")
			->from(self::model()->tableName())
			->where("puser_id = :puser_id", array(':puser_id' => $idUser))
			->queryScalar();
	}

}