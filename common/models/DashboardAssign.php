<?php

/**
 * This is the model class for table "dashboard_assign".
 *
 * The followings are the available columns in table 'dashboard_assign':
 * @property integer $id
 * @property integer $id_layout
 * @property string $item_type
 * @property string $id_item
 *
 * The followings are the available model relations:
 * @property DashboardLayout $layout
 */
class DashboardAssign extends CActiveRecord
{

	const ITEM_TYPE_BRANCH				= 'branch';
	const ITEM_TYPE_BRANCH_AND_DESC		= 'branch_and_desc';
	const ITEM_TYPE_USERLEVEL			= 'userlevel';
	const ITEM_TYPE_PUPROFILE			= 'puprofile';
	const ITEM_TYPE_USER				= 'user';
	const ITEM_TYPE_GROUP				= 'group';


	const FILTER_ALL				= 'all';

	public static $userFilterTypes = array(
		DashboardAssign::ITEM_TYPE_USER,
		DashboardAssign::ITEM_TYPE_GROUP,
		DashboardAssign::ITEM_TYPE_BRANCH,
		DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC,
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_assign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_layout, item_type, id_item', 'required'),
			array('id_layout', 'numerical', 'integerOnly'=>true),
			array('item_type', 'length', 'max'=>32),
			array('id_item', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_layout, item_type, id_item', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'layout' => array(self::BELONGS_TO, 'DashboardLayout', 'id_layout'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_layout' => 'Id Layout',
			'item_type' => 'Item Type',
			'id_item' => 'Id Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_layout',$this->id_layout);
		$criteria->compare('item_type',$this->item_type,true);
		$criteria->compare('id_item',$this->id_item,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardAssign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Resolve Dashboard layout to use based on User and layout assignments
	 *
	 * @return number
	 */
	public static function resolveUserLayout($idUser=false) {
		$idUser = Yii::app()->user->id;

		// Trigger event to let custom plugins decide the preferred layout
		$event = new DEvent(self, array('idUser' => $idUser));
		Yii::app()->event->raise('OnResolvingDashboardLayout', $event);
		if(!$event->shouldPerformAsDefault() && $event->return_value['idLayout'] > 0)
			return $event->return_value['idLayout'];

		// Get user levels and 'quote' them (they are used in SQL IN() conditions)
		$userLevels = Yii::app()->user->getUserLevels($idUser);
		foreach ($userLevels as $key => $value) {
			$userLevels[$key] = "'" . $value . "'";
		}


		// Build possible layouts out of branches, groups and users selection
		$possibleLayouts1 = array();

		// First, Collect all layouts having user filter option = ALL (1)
		$tmpModels = DashboardLayout::model()->findAllByAttributes(array('ufilter_option' => DashboardLayout::LAYOUT_OPTION_ALL));
		foreach ($tmpModels as $tmpModel) {
			$possibleLayouts1[] = $tmpModel->id;
		}


		// BRANCHES: direct ?
		$sql = "
			SELECT 	da.id_layout AS idLayout
			FROM dashboard_assign da
			JOIN dashboard_layout dl ON (dl.id=da.id_layout)
			JOIN core_org_chart_tree cot on (STRCMP(da.id_item,cot.idOrg)=0)
			JOIN core_group_members cgm on (cgm.idst=cot.idst_oc)
			WHERE (cgm.idstMember=:idUser) AND (da.item_type=:itemType)
			ORDER BY dl.position ASC, dl.id ASC
		";
		$params = array(':idUser' => $idUser, ':itemType' => self::ITEM_TYPE_BRANCH);
		$result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
		if ($result) {
			foreach ($result as $row) {
				$possibleLayouts1[] = $row['idLayout'];
			}
		}

		// BRANCHES Incl. descendants
		$sql = "
			SELECT 	da.id_layout AS idLayout
			FROM dashboard_assign da
			JOIN dashboard_layout dl ON (dl.id=da.id_layout)
			JOIN core_org_chart_tree cot1 on (STRCMP(da.id_item,cot1.idOrg)=0)
			JOIN core_org_chart_tree cot2 on (cot2.iLeft >= cot1.iLeft AND cot2.iRight <= cot1.iRight)
			JOIN core_group_members cgm on (cgm.idst=cot2.idst_oc)
			WHERE (cgm.idstMember=:idUser) AND (da.item_type=:itemType)
			ORDER BY dl.position ASC, dl.id ASC
		";
		$params = array(':idUser' => $idUser, ':itemType' => self::ITEM_TYPE_BRANCH_AND_DESC);
		$result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
		if ($result) {
			foreach ($result as $row) {
				$possibleLayouts1[] = $row['idLayout'];
			}
		}


		// USERS ?
		$sql = "
			SELECT da.id_layout AS idLayout
			FROM dashboard_assign da
			JOIN dashboard_layout dl ON (dl.id=da.id_layout)
			WHERE (da.item_type=:itemType) AND (da.id_item=:idUser)
		";
		$params = array(':idUser' => $idUser, ':itemType' => self::ITEM_TYPE_USER);
		$result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
		if ($result) {
			foreach ($result as $row) {
				$possibleLayouts1[] = $row['idLayout'];
			}
		}


		// GROUPS ?
		$sql = "
			SELECT da.id_layout AS idLayout
			FROM dashboard_assign da
			JOIN dashboard_layout dl ON (dl.id=da.id_layout)
			JOIN core_group_members cgm on (cgm.idst=da.id_item)
			WHERE (da.item_type=:itemType) AND (cgm.idstMember=:idUser)
		";
		$params = array(':idUser' => $idUser, ':itemType' => self::ITEM_TYPE_GROUP);
		$result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
		if ($result) {
			foreach ($result as $row) {
				$possibleLayouts1[] = $row['idLayout'];
			}
		}

		// Build possible layouts out of user level settings of layouts
		$possibleLayouts2 = array();
		// Prevent SQL error if user levels is an empty value (which should never happen!)
		if (is_array($userLevels) && !empty($userLevels)) {
			$sql = "
				SELECT da.id_layout AS idLayout
				FROM dashboard_assign da
				JOIN dashboard_layout dl ON (dl.id=da.id_layout)
				WHERE (da.item_type=:itemType) AND ((da.id_item IN(" . implode(',', $userLevels). ")) OR (LOWER(da.id_item) = :all))
			";
			$params = array(':itemType' => self::ITEM_TYPE_USERLEVEL, ':all' => strtolower(self::FILTER_ALL));
			$result = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
			if ($result) {
				foreach ($result as $row) {
					$possibleLayouts2[] = $row['idLayout'];
				}
			}
		}


		// NOW, Intersect the above collected possible layouts; we need those that are common for (branches, groups, users) AND (levels)
		$possibleLayouts = array_intersect($possibleLayouts1, $possibleLayouts2);



		// NEXT, substract NOT allowed layouts (for any reason)
		$notPossibleLayouts = array();

		// Get 'Fallback/Standard' layout ID
		$fallbackLayout = DashboardLayout::model()->findByAttributes(array('is_fallback' => 1));
		if (!$fallbackLayout) {
			// OOOOPS!!! BAAD!!! We NEED TO HAVE THIS LAYOUT!! Create one!
			$fallbackLayout = DashboardLayout::createFallbackLayout();
		}
		if ($fallbackLayout) {
			$notPossibleLayouts[] = $fallbackLayout->id;
		}


		// Build list of layouts NOT allowed to current [POWER] user, if he/she IS a Power User; based on layouts PU assignments.
		// Basically, we list all layouts NOT having THAT power user's PROFILE assigned, and NOT being "for ALL Pu profiles".
		// It sounds easy achievable through SQL, but is not. Kepp it THIS way please!
		if (Yii::app()->user->getIsPu()) {
			$userPuProfile = Yii::app()->user->loadUserModel()->adminProfile[0];  // This is a a CoreGroup model
			$puAssignments = self::layoutPuProfilesAssignments();  // Array of [<layout>] => array of PU profiles
			foreach ($puAssignments as $idLayout => $assignment) {
				$layoutModel = DashboardLayout::model()->findByPk($idLayout);
				if (!in_array($userPuProfile->idst, $assignment) && $layoutModel->puprofiles_filter_option != DashboardLayout::LAYOUT_OPTION_ALL) {
					$notPossibleLayouts[] = $idLayout;
				}
			}
		}
		$possibleLayouts = array_diff($possibleLayouts, $notPossibleLayouts);


		// NOW, get the FIRST layout having NON-zero dashlets (from possible layouts, of course!)
		$layout = false;
		if (count($possibleLayouts) > 0) {
			$sql 	= "SELECT id FROM dashboard_layout WHERE id IN(". implode(',', $possibleLayouts).") ORDER BY position ASC, id ASC";
			$reader = Yii::app()->db->createCommand($sql)->query();
			foreach ($reader as $lo) {
				$tmpModel = DashboardLayout::model()->findByPk($lo['id']);
				if ($tmpModel->countLayoutDashlets() > 0) {
					$layout = $lo['id'];
					break;
				}
			}
		}

		// Sooo, lets check if we have found a valid layout. If not, we have to decide/find the DEFAULT one.
		if (!$layout) {
			$defaultLayoutModel = DashboardLayout::model()->findByAttributes(array(
				'default' => 1,
			));

			// Lets check if there is one.. we still need to decide about the user home page :-)
			if ($defaultLayoutModel) {
				$layout = $defaultLayoutModel->id;
			}
		}

		// Still NO layout??? Come on, what is this?? .. Get the standard one then (the fallback)
		if (!$layout) {
			// We created the model a bit earlier, above... in the code...
			$layout = $fallbackLayout->id;
		}
		// Oh, we have one... but if model is not available OR number of dashlets is NONE... use again the fallback one
		else  {
			$layoutModel = DashboardLayout::model()->findByPk($layout);
			if (!$layoutModel || ($layoutModel->countLayoutDashlets() <= 0)) {
				$layout = $fallbackLayout->id;
			}
		}

		// Finally... by now we should have an ID of the resolved Layout (as long as we "fallback", this is guaranteed!)
		return $layout;
	}


	/**
	 *
	 * @param string $idLayout
	 * @return multitype:
	 */
	public static function layoutPuProfilesAssignments($idLayout = false) {

		$attributes = array();
		$attributes['item_type'] = self::ITEM_TYPE_PUPROFILE;
		if ($idLayout) {
			$attributes['id_layout'] = $idLayout;
		}

		$models = self::model()->findAllByAttributes($attributes);

		$result = array();
		foreach ($models as $model) {
			$result[$model->id_layout][] = $model->id_item;
		}

		return $result;

	}



}
