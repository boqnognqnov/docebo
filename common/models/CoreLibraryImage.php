<?php

/**
 * This is the model class for table "core_field".
 *
 * The followings are the available columns in table 'core_field':
 * @property integer   $id
 * @property integer   $file_id
 * @property integer   $user_id
 * @property integer   $type
 */
class CoreLibraryImage extends CActiveRecord {

    const TYPE_USER_COURSE = 1;
    const TYPE_DEFAULT_COURSE = 2;
    const TYPE_USER_BACKGROUND = 3;
    const TYPE_DEFAULT_BACKGROUND = 4;
    const TYPE_COURSE_PLAYER_BACKGROUND = 5;
    const TYPE_USER_COURSE_PLAYER_BACKGROUND = 6;
    const TYPE_GAMIFICATION_BADGES = 7;
    const TYPE_GAMIFICATION_BADGES_USER = 8;

    public $uploadedFile;

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return CoreCalendar the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'core_library_image';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//				array(
//					'type_id, ref_type',
//					'required'
//				),
//				array(
//					'ref_id',
//					'numerical',
//					'allowEmpty' => true
//				),
//				array(
//					'filesize',
//					'numerical'
//				),
//				array(
//					'filename, original_filename, extension',
//					'length',
//					'max' => '255'
//				),
//				// array('filesize, type_id, ref_type, ref_id', 'integerOnly' => true),
//				// The following rule is used by search().
//				// Please remove those attributes that should not be searched.
//				array(
//					'id, created, modified, type_id',
//					'safe',
//					'on' => 'search'
//				),
        );
    }


    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
//				'id' => 'ID',
//				'filepath' => 'Class',
//				'filesize' => 'Create Date',
//				'created' => 'Start Date',
//				'modified' => 'End Date',
//				'type_id' => 'Title',
//				'ref_type' => 'Description',
//				'ref_id' => 'Private',
        );
    }

    public function imageType($type) {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->getTableAlias(false, false) . '.type = :imageType AND '. $this->getTableAlias(false, false) .'.file_id > 0',
            'params' => array(':imageType' => $type),
        ));

        return $this;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

//			$criteria->compare('id', $this->id);
//			$criteria->compare('filepath', $this->filepath, true);
//			$criteria->compare('type_id', $this->type_id);
//			$criteria->compare('ref_type', $this->ref_type);
//			$criteria->compare('ref_id', $this->ref_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }


	/**
	 * Return the list of degault images for the selected category, if they are still not processed (aka in the source folder)
	 * this method first process them copying (and resizing in some cases) into assets and media
	 * @param $path
	 * @param $imageType
	 * @param string $regex
	 * @return array
	 */
	public function getDefaultImages($path, $imageType, $regex = '') {
        $images = $this->imageType($imageType)->with('attachment')->findAll();

        $existing = array();
        foreach ($images as $img) {
            $existing[] = $img->attachment->original_filename;
        }

        $newImages = $this->scanDefaultDirectory($path, $existing, $imageType, $regex);

        return array_merge($images, $newImages);
    }


	/**
	 * This method, starting from the source directory read all the default images and save them into a new path
	 * @param $path
	 * @param $existing
	 * @param $imageType
	 * @param string $regex
	 * @return array
	 */
	private function scanDefaultDirectory($path, $existing, $imageType, $regex = '') {
        $files = CFileHelper::findFiles($path, array('fileTypes' => array('jpg', 'gif', 'png', 'jpeg')));
        $outFiles = array();
        $outFileNames = array();

        if (!empty($files) && $regex != '') {
            foreach ($files as $file) {
                if (preg_match($regex, $file)) {
                    $fileName = substr($file, strrpos($file, '/') + 1);
                    $outFileNames[] = $fileName;
                    $outFiles[$fileName] = $file;
                }
            }
        }

        $outFileNames = array_diff($outFileNames, $existing);
        $images = array();

        if (!empty($outFileNames)) {
            foreach ($outFileNames as $fileName) {
                $f = new CoreLibraryImage;
                $f->file_id = $f->saveUploadedFile($outFiles[$fileName]);
                $f->type = $imageType;
                $f->save();

                $images[] = $f;
            }
        }

        return $images;
    }
}