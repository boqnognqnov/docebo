<?php

/**
 * This is the model class for table "app7020_channel_visibility".
 *
 * The followings are the available columns in table 'app7020_channel_visibility':
 * @property integer $id
 * @property integer $idChannel
 * @property integer $idObject
 * @property integer $type
 * @property integer $selectState
 *
 * The followings are the available model relations:
 * @property App7020Channels $channel
 */
class App7020ChannelVisibility extends CActiveRecord {

	const TYPE_PUBLIC = 'public';
	const TYPE_SELECTION = 'selection';

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_channel_visibility';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idChannel, idObject, type, selectState', 'required'),
			array('idChannel, idObject, type, selectState', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idChannel, idObject, type, selectState', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channel' => array(self::BELONGS_TO, 'App7020Channels', 'idChannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idChannel' => 'Id Channel',
			'idObject' => 'Id Object',
			'type' => 'Type',
			'selectState' => 'Selected State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idChannel', $this->idChannel);
		$criteria->compare('idObject', $this->idObject);
		$criteria->compare('type', $this->type);
		$criteria->compare('selectState', $this->selectedState);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ChannelVisibility the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Get by id
	 * @deprecated since sprint-23 Using into clone channels step, this have to be removed asap
	 */
	public static function getVisibilityById($id) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("*");
		$dbCommand->from(App7020ChannelVisibility::model()->tableName());
		$dbCommand->where("idChannel = :idChannel", array(":idChannel" => $id));
		$result = $dbCommand->queryAll();
		$returnArray = array();
		if ($result) {
			$g = 0;
			$b = 0;
			foreach ($result as $v) {
				if ($v['type'] == 1) {
					$g++;
				} else {
					$b++;
				}
			}
			$returnArray = array(
				'groups' => $g,
				'branches' => $b,
			);
		}

		return $returnArray;
	}

	/**
	 * 
	 * @param type $idChannel
	 * @return type
	 * @depricated since sprint-23 Using into clone channels step, this have to be removed asap
	 * 
	 */
	public static function getVisibilityText($idChannel) {
		$channelVisibility = self::getVisibilityById($idChannel);
		if (count($channelVisibility) > 0) {
			if ($channelVisibility['groups'] > 0 && $channelVisibility['branches'] > 0) {
				$channelVisibility = $channelVisibility['groups'] . Yii::t('app7020', 'Groups') . ' ,' . $channelVisibility['branches'] . ' ' . Yii::t('app7020', 'Branches');
			} else {
				$channelVisibility = ($channelVisibility['groups'] > 0) ? $channelVisibility['groups'] . ' ' . Yii::t('app7020', 'Groups') : $channelVisibility['branches'] . ' ' . Yii::t('app7020', 'Branches');
			}
		} else {
			$channelVisibility = Yii::t('app7020', 'All groups and branches');
		}
		return $channelVisibility;
	}

	public static function secureDelete($id) {
		Log::_(print_r('secureDelete', $id, 1));
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("*");
		$dbCommand->from(App7020ChannelVisibility::model()->tableName());
		$dbCommand->where("idChannel = :idChannel", array(":idChannel" => $id));
		$result = $dbCommand->queryAll();
		if ($result) {
			foreach ($result as $d) {
				Log::_(print_r("DELETE {$d['id']}", 1));
				App7020ChannelVisibility::model()->findByPk($d['id'])->delete();
			}
		}
	}

	// Get Selected
	public static function getAxSelectedGroupsAndBranches($id) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("*");
		$dbCommand->from(App7020ChannelVisibility::model()->tableName());
		$dbCommand->where("idChannel = :idChannel", array(":idChannel" => $id));
		$result = $dbCommand->queryAll();
		$returnArray = array();
		$groups = array();
		$branches = array();
		if ($result) {
			foreach ($result as $v) {
				if ($v['type'] == 1) {
					$groups[] = $v['idObject'];
				} else {
					$branches[] = $v['idObject'];
				}
			}
			$returnArray = array(
				'groups' => $groups,
				'branches' => $branches,
			);
		}
		return $returnArray;
	}

	// Get Selected for User Selector
	public static function getAxSelectedGroupsAndBranchesForSelector($id) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("*");
		$dbCommand->from(App7020ChannelVisibility::model()->tableName());
		$dbCommand->where("idChannel = :idChannel", array(":idChannel" => $id));
		$result = $dbCommand->queryAll();
		$returnArray = array();
		$groups = array();
		$branches = array();
		if ($result) {
			foreach ($result as $v) {
				if ($v['type'] == 1) {
					$groups[] = $v['idObject'];
				} else {
					$branches[] = array(
						'key' => $v['idObject'],
						'selectState' => $v['selectState']
					);
				}
			}
			$returnArray = array(
				'groups' => $groups,
				'branches' => $branches,
			);
		}
		return $returnArray;
	}

	/**
	 * Check for existing visibility for an asset
	 * @param integer $channelID
	 * @return boolean
	 * @deprecated  
	 */
	public static function isExistVisibility_($channelID) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("count(cv.id) as counter");
		$dbCommand->from(App7020ChannelVisibility::model()->tableName() . ' as cv');
		$dbCommand->where("idChannel = :idChannel", array(":idChannel" => (int) $channelID));
		return (boolean) $dbCommand->queryScalar();
	}

	/**
	 * Check for existing visibility for an asset
	 * @param integer $channelID
	 * @return boolean
	 * @deprecated  
	 */
	public static function isExistVisibility($channelID) {
		try {
			$ar = array(
				'idUser' => Yii::app()->user->idst,
				"idChannel" => $channelID,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => true,
				"ignoreInvited" => false,
				"appendEnabledEffect" => false,
			);
			$dp = DataProvider::factory("ChannelUsers", $ar)->provider();
			return boolval($dp->totalItemCount);
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			return false;
		}
	}

	public function beforeSave() {
		Yii::app()->event->raise(EventManager::EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_UPDATED, new DEvent($this, array(
			'model' => $this,
		)));
		return parent::beforeSave();
	}

	public function afterSave() {
		Yii::app()->event->raise(EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_UPDATED, new DEvent($this, array(
			'model' => $this,
		)));
	}

	public function beforeDelete() {
		Yii::app()->event->raise(EventManager::EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_DELETED, new DEvent($this, array(
			'model' => $this,
		)));
		return parent::beforeDelete();
	}

	public function afterDelete() {
		Yii::app()->event->raise(EventManager::EVENT_APP7020_CHANNEL_VISIBILITY_DELETED, new DEvent($this, array(
			'model' => $this,
		)));
	}

	/**
	 * *************************************************************************
	 * ********************** AFTER IMPROVEMENTS  ******************************
	 * *************************************************************************
	 */

	/**
	 * Return count of users who presents in channel
	 * 
	 * @param integer $channel_id
	 * @param integer|array $ignoreUsers
	 * @return boolean|integer
	 */
	static function getNumUsersVisibilirtyToChannels($channel_id, $ignoreUsers = array(-1)) {
		try {

			$ar = array(
				"idChannel" => $channel_id,
				"ignoreVisallChannels" => false,
				"ignoreGodAdminEffect" => true,
				"ignoreInvited" => false,
				"appendEnabledEffect" => true,
				"ignoreUsers" => $ignoreUsers,
				"ignoreSuspendedUsers" => true,
			);
			$dp = DataProvider::factory("ChannelUsers", $ar)->provider();
			return intval($dp->totalItemCount);
		} catch (Exception $ex) {
			Log::_($ex->getMessage());
			return false;
		}
	}

}
