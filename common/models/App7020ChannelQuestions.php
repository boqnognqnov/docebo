<?php

/**
 * This is the model class for table "app7020_channel_questions".
 *
 * The followings are the available columns in table 'app7020_channel_questions':
 * @property integer $id
 * @property integer $idChannel
 * @property integer $idQuestion
 *
 * The followings are the available model relations:
 * @property App7020Channels $channel
 */
class App7020ChannelQuestions extends CActiveRecord {

	

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_channel_questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idChannel, idQuestion', 'required'),
			array('idChannel, idQuestion', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idChannel, idQuestion', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channel' => array(self::BELONGS_TO, 'App7020Channels', 'idChannel'),
			'question' => array(self::BELONGS_TO, 'App7020Question', 'idQuestion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idChannel' => 'Id Channel',
			'idQuestion' => 'Id Question'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idChannel', $this->idChannel);
		$criteria->compare('idQuestion', $this->idQuestion);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ChannelAssets the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	

}
