<?php

/**
 * This is the model class for table "ecommerce_coupon".
 *
 * The followings are the available columns in table 'ecommerce_coupon':
 * @property integer $id_coupon
 * @property string $code
 * @property string $description
 * @property string $valid_from
 * @property string $valid_to
 * @property integer $can_have_courses
 * @property integer $usage_count
 * @property string $discount
 * @property string $discount_type
 * @property string $minimum_order
 *
 * The followings are the available model relations:
 * @property LearningCourse[] $assignedCourses
 * @property EcommerceTransaction[] $ecommerceTransactions
 */
class EcommerceCoupon extends CActiveRecord
{
    const VALIDITY_ALWAYS = 'always';
    const VALIDITY_SPECIFIED = 'specified';

    const DISCOUNT_TYPE_PERCENT = 'percent';
    const DISCOUNT_TYPE_AMOUNT = 'amount';

    const USAGE_UNLIMITED_USERS = 'unlimited';
    const USAGE_LIMITED_USERS = 'limited';

    //used for search purposes
    public $search_input;
    public $search_date_from;
    public $search_date_to;
    public $hide_expired;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ecommerce_coupon';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, description', 'required'),
			array('can_have_courses, usage_count', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>45),
			array('description', 'length', 'max'=>255),
			array('discount, minimum_order', 'length', 'max'=>10),
            array('discount_type', 'length', 'max'=>12),
			array('code, description, valid_from, valid_to, usage_count, discount, discount_type, minimum_order', 'safe'),
			// The following rule is used by search().
			array('id_coupon, code, description, valid_from, valid_to, can_have_courses, usage_count, discount, discount_type, minimum_order, search_input, search_date_from, search_date_to, hide_expired', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'assignedCourses' => array(self::MANY_MANY, 'LearningCourse', 'ecommerce_coupon_courses(id_coupon, id_course)'),
			'ecommerceTransactions' => array(self::HAS_MANY, 'EcommerceTransaction', 'id_coupon'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_coupon' => 'Id Coupon',
			'code' => Yii::t('standard', '_CODE'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'valid_from' => Yii::t('coupon', 'Valid from'),
			'valid_to' => Yii::t('coupon', 'Valid to'),
			'can_have_courses' => 'Can Have Courses',
			'usage_count' => 'Usage Count',
			'discount' => Yii::t('coupon', 'Coupon discount'),
            'discount_type' => 'Discount Type',
			'minimum_order' => Yii::t('coupon', 'Minimum order'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior'
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_coupon',$this->id_coupon);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('valid_from',$this->valid_from,true);
		$criteria->compare('valid_to',$this->valid_to,true);
		$criteria->compare('can_have_courses',$this->can_have_courses);
		$criteria->compare('usage_count',$this->usage_count);
		$criteria->compare('discount',$this->discount,true);
        $criteria->compare('discount_type',$this->discount_type,true);
		$criteria->compare('minimum_order',$this->minimum_order,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EcommerceCoupon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave() {
        if (parent::beforeSave()) {

            // check code to be unique
            $model = self::model()->findByAttributes(array('code'=>$this->code));

            $codeAlreadyUsed = ($model && $model->id_coupon != $this->id_coupon);

            if ($codeAlreadyUsed) {
                $this->addError('code', Yii::t('coupon', 'This code is already used'));
                return false;
            }

            return true;
        }
        return false;
    }

    /**
     * Default data provider for the list of coupons
     * @return CActiveDataProvider
     */
    public function dataProvider() {
        $config = array();
        $criteria = new CDbCriteria();
        $criteria->with = array('assignedCourses');

        if (!empty($this->search_input)) {
            $criteria->addCondition('CONCAT(t.code, t.description) like :search');
            $criteria->params[':search'] = '%'.$this->search_input.'%';
        }
        
        if (!empty($this->search_date_from)) {
            $criteria->addCondition('DATE(t.valid_from) >= DATE(:date_from)');
            $mysqlDateFrom = Yii::app()->localtime->fromLocalDate($this->search_date_from);
            $criteria->params[':date_from'] = $mysqlDateFrom;
        }
        
        if (!empty($this->search_date_to)) {
            $criteria->addCondition('DATE(t.valid_to) <= DATE(:date_to)');
            $mysqlDateTo = Yii::app()->localtime->fromLocalDate($this->search_date_to);
            $criteria->params[':date_to'] = $mysqlDateTo;
        }
        if ($this->hide_expired) {
            $criteria->addCondition('t.valid_to IS NULL OR (:date_now <= t.valid_to AND :date_now >= t.valid_from)');
            $criteria->params[':date_now'] = Yii::app()->localtime->getUTCNow();
        }

        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName) {
            $sortAttributes[$attributeName] = 't.' . $attributeName;
        }

        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = array('id_coupon' => false);
        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * @return CActiveDataProvider
     */
    public function assignedCoursesDataProvider() {
        $config = array();
        $assignedCourses = CHtml::listData($this->assignedCourses, 'idCourse', 'idCourse');

        $criteria = new CDbCriteria();
        $criteria->addInCondition('idCourse', $assignedCourses);

        $searchQuery = addcslashes($_REQUEST['LearningCourse']['name'], '%_');
        if (!empty($searchQuery)) {
            $criteria->addCondition('name LIKE :match OR name LIKE :match2');
            $criteria->params[':match'] = "/%$searchQuery%";
            $criteria->params[':match2'] = "%$searchQuery%";
        }

        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = array('name');
        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

        return new CActiveDataProvider('LearningCourse', $config);
    }

    /**
     * Returns the edit icon for a coupon row
     * @return string
     */
    public function renderDiscount() {
        $html = ($this->discount_type == 'percent')
            ? $this->discount . ' %'
            : $this->discount . ' ' . Settings::get('currency_symbol');
        return $html;
    }

    /**
     * Renders the validity column for this coupon
     * @return string
     */
    public function renderValidity() {
		$html = '';

        if(!empty($this->valid_from) && !empty($this->valid_to)) {

            $utcValidFrom 	= Yii::app()->localtime->fromLocalDateTime($this->valid_from);
            $utcValidTo 	= Yii::app()->localtime->fromLocalDateTime($this->valid_to);

			$isExpired = $this->isExpired();

            if($isExpired)
                $html .= '<span class="expired-coupon">';

			if ($this->valid_to === $this->valid_from) {
				$html .= Yii::app()->localtime->toLocalDate($utcValidFrom);
			}
			else {
				$html .= Yii::app()->localtime->toLocalDate($utcValidFrom) . ' - ' .
					Yii::app()->localtime->toLocalDate($utcValidTo);
			}

            if($isExpired)
                $html .= '</span>';
        } else
            $html = Yii::t('coupon', 'No expiration');

        return $html;
    }

	/**
	 * @return bool
	 */
	public function isExpired() {
		if(!empty($this->valid_from) && !empty($this->valid_to)) {
			$utcValidFrom 	= strtotime(Yii::app()->localtime->fromLocalDateTime($this->valid_from));
			$utcValidTo 	= strtotime(Yii::app()->localtime->fromLocalDateTime($this->valid_to));
			$utcNow 		= strtotime(Yii::app()->localtime->getUTCNow());
			$isExpired 		= ($utcNow < $utcValidFrom) || $utcNow > $utcValidTo;
			return $isExpired;
		}

		return false;
	}

    /**
     * Renders the list of assigned courses
     * @return string
     */
    public function renderAssignedCourses() {

        if ($this->can_have_courses) {
            $countAssignedCourses = count($this->assignedCourses);
            $icon = ($countAssignedCourses > 0)
                ? '<span class="assignCourses"></span>'
                : '<span class="assignCourses yellow"></span>';
            $assignCoursesUrl = Yii::app()->controller->createUrl('assignCourses', array('id_coupon'=>$this->id_coupon));
            $html = CHtml::link($countAssignedCourses . $icon, $assignCoursesUrl, array());
        } else {
            $html = Yii::t('standard', '_ALL_COURSES');
        }

        return $html;
    }

    /**
     * @return string
     */
    public function renderUsage() {
        $html = count($this->ecommerceTransactions) . '/' . intval($this->usage_count);
        return $html;
    }

    /**
     * Checks the coupon criteria and applies the discount
     *
     * @param float $cartTotal
     * @param array $cartCourses
     * @return array
     */
    public function applyDiscount($cartTotal, $cartCourses = array(), $cartLearningPaths = array(), $cartSeats = array()) {
        $result = array(
            'valid_courses' => array(),
            'error' => '',
            'discount' => 0,
            'code' => $this->code
        );

        try {
            $amountToBeDiscounted = 0;
            $coursesToBeDiscounted = array();

            // load the models for the courses added to the cart
            $criteria = new CDbCriteria();
            $criteria->addInCondition('idCourse', $cartCourses);
            $courseModels = CourseCartPosition::model()->findAll($criteria);
            $cartCoursesModels = array();
            foreach($courseModels as $course)
                $cartCoursesModels[$course->idCourse] = $course;

	        $learningPlansIds = array();
			if(isset($cartLearningPaths['ids'])){
				foreach($cartLearningPaths['ids'] as $planId){
					$learningPlansIds[$planId] = $planId;
				}
			}

	        $courseSeatsIds = array();
	        if(isset($cartSeats['ids'])){
		        foreach($cartSeats['ids'] as $seatId){
			        $courseSeatsIds[$seatId] = $seatId;
		        }
	        }


	        $criteriaPlans = new CDbCriteria();
	        $criteriaPlans->addInCondition('id_path', $learningPlansIds);
	        $planModels = CoursepathCartPosition::model()->findAll($criteriaPlans);
	        $cartPathsModels = array();
	        foreach($planModels as $plan){
		        $cartPathsModels[$plan->id_path] = $plan;
	        }

	        $criteriaSeats = new CDbCriteria();
	        $criteriaSeats->addInCondition('idCourse', $courseSeatsIds);
	        $seatModels = CourseseatsCartPosition::model()->findAll($criteriaSeats);
	        $cartSeatsModels = array();
	        foreach($seatModels as $seat){
		        $cartSeatsModels[$seat->idCourse] = $seat;
	        }

            // check validity dates
            if ($this->isExpired())
                throw new CException(Yii::t('coupon', 'Expired or invalid coupon'));

            // check if minimum order applies
            $minOrder = floatval($this->minimum_order);
            if ($minOrder > 0 && $cartTotal < $minOrder)
                throw new CException(Yii::t('coupon', 'Minimum order must be {min_order} to apply the discount', array('{min_order}' => number_format($minOrder, 2))));

            // each user can use this coupon once
			$event = new DEvent($this, array(
				'user_id' => Yii::app()->user->id,
				'coupon' => $this
			));

			Yii::app()->event->raise('BeforeCouponUsageCheck', $event);
			if($event->shouldPerformAsDefault()) {
				$previouslyUsed = EcommerceTransaction::model()->findByAttributes(array(
					'id_user' => Yii::app()->user->id,
					'id_coupon' => $this->id_coupon,
					'paid' => 1
				));
				if ($previouslyUsed)
					throw new CException(Yii::t('coupon', 'The coupon has already been used'));
			}

            // check coupon usage
            $usageCount = intval($this->usage_count);
            if($usageCount > 0) {
                $couponTransactions = EcommerceTransaction::model()->findAllByAttributes(array('id_coupon'=>$this->id_coupon));
                if (count($couponTransactions) >= $usageCount)
                    throw new CException(Yii::t('coupon', 'Expired or invalid coupon'));
            }

            // Check if it applies to any of the courses in the cart
            if ($this->can_have_courses) {
                $assignedCourses = $this->getAssignedCoursesIds();
                $validCourses = array();
                if (!empty($assignedCourses)) {
                    foreach ($cartCourses as $learningCourseId) {
                        if (in_array($learningCourseId, $assignedCourses)) {
                            // mark the course as valid for this coupon
                            if(isset($cartCoursesModels[$learningCourseId])) {
                                $learningCourse = $cartCoursesModels[$learningCourseId];
                                $coursesToBeDiscounted[] = $learningCourse;
                                $validCourses[] = $learningCourse->name;
                            }
                        }
                    }
                }

                $result['valid_courses'] = $validCourses;
            }
            else
                $coursesToBeDiscounted = $courseModels;

	        if ($this->can_have_courses) {
	            if(empty($coursesToBeDiscounted)){
		            throw new CException(Yii::t('coupon', 'Expired or invalid coupon'));
	            }
	        }else{
		        if(empty($coursesToBeDiscounted) && empty($cartPathsModels)  && empty($cartSeatsModels)){
			        throw new CException(Yii::t('coupon', 'Expired or invalid coupon'));
		        }

		        foreach($cartPathsModels as $learningPathModel){
			        if(isset($cartLearningPaths['prices'][$learningPathModel->id_path]) && $cartLearningPaths['prices'][$learningPathModel->id_path] ){
				        $amountToBeDiscounted += floatval($cartLearningPaths['prices'][$learningPathModel->id_path]);
			        }
		        }

		        foreach($cartSeatsModels as $courseSeatModel){
			        if(isset($cartSeats['prices'][$courseSeatModel->idCourse]) && $cartSeats['prices'][$courseSeatModel->idCourse]){
				        $amountToBeDiscounted += floatval($cartSeats['prices'][$courseSeatModel->idCourse]);
			        }
		        }
	        }

            // calculate the amount to be discounted
            /* @var $learningCourse CourseCartPosition */
            foreach ($coursesToBeDiscounted as $learningCourse) {
                $amountToBeDiscounted += floatval($learningCourse->getPrice());
            }

            // apply the discount
            if ($this->discount_type == self::DISCOUNT_TYPE_PERCENT) {
                $discountPercent = $this->discount;
                $discount = floatval($amountToBeDiscounted / 100) * $discountPercent;
            }
            else
                $discount = ($this->discount > $amountToBeDiscounted) ? $amountToBeDiscounted : $this->discount;

            $result['discount'] 	= $discount;
            $result['discounted'] 	= $cartTotal - $discount;
        }
        catch (CException $ex) {
            $result['error'] = $ex->getMessage();
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getAssignedCoursesIds() {
        $ids = array();
        if (!empty($this->assignedCourses)) {
            foreach ($this->assignedCourses as $learningCourse) {
                $ids[] = $learningCourse->idCourse;
            }
        }
        return $ids;
    }
}

/*

ALTER TABLE `ecommerce_coupon` CHANGE `valid_from` `valid_from` DATE  NULL;
ALTER TABLE `ecommerce_coupon` CHANGE `valid_to` `valid_to` DATE  NULL;


 */