<?php

/**
 * This is the model class for table "certification_user".
 *
 * The followings are the available columns in table 'certification_user':
 * @property integer $id
 * @property integer $id_user
 * @property string $on_datetime
 * @property string $expire_at
 * @property integer $id_cert_item
 * @property integer $archived
 * @property string $extra_data
 * @property integer $in_renew
 * @property string $reset_token
 * 
 * The followings are the available model relations:
 * @property CertificationItem $certificationItem
 * @property CoreUser $user
 */
class CertificationUser extends CActiveRecord
{
	
	public $includeArchived = false;
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, on_datetime, id_cert_item', 'required'),
			array('id_user, id_cert_item, archived, in_renew', 'numerical', 'integerOnly'=>true),
			array('expire_at, extra_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, on_datetime, expire_at, id_cert_item, archived, extra_data, in_renew', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificationItem' => array(self::BELONGS_TO, 'CertificationItem', 'id_cert_item'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'on_datetime' => 'On Datetime',
			'expire_at' => 'Expire At',
			'id_cert_item' => 'Id Cert Item',
			'archived' => 'Archived',
			'extra_data' => 'Extra Data',
			'in_renew' => 'In renew mode',					
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('on_datetime',$this->on_datetime,true);
		$criteria->compare('expire_at',$this->expire_at,true);
		$criteria->compare('id_cert_item',$this->id_cert_item);
		$criteria->compare('archived',$this->archived);
		$criteria->compare('extra_data',$this->extra_data,true);
		$criteria->compare('in_renew',$this->in_renew);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('on_datetime medium', 'expire_at medium'),
				'dateAttributes' => array()
			)
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CertificationUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function sqlDataProvider($showExpired = false, $searchKeyword = false){
		$params = array();

		$commandData = Yii::app()->getDb()->createCommand()
			->select(
				"*, u.id AS cu_id, c.deleted," .
				"TIMESTAMPDIFF(SECOND, NOW(),u.expire_at) AS timeToExpire"
			)
			->from(CertificationUser::model()->tableName().' u')
			->join(CertificationItem::model()->tableName().' item', 'u.id_cert_item=item.id')
			->join(Certification::model()->tableName().' c', 'item.id_cert=c.id_cert');

		if(!$showExpired){
			$commandData->andWhere("u.expire_at > NOW() OR u.expire_at='0000-00-00 00:00:00'");
		}

		if($searchKeyword){
			$commandData->andWhere('(c.title LIKE :search OR c.description LIKE :search)');
			$params[':search'] = '%'.$searchKeyword.'%';
		}

		if($this->id_user){
			$commandData->andWhere('u.id_user=:idUser');
			$params[':idUser'] = $this->id_user;
		}

		// Ignore "archived"?
		if (!$this->includeArchived) {
			$commandData->andWhere('u.archived=0');
		}
		
		$commandCount = clone $commandData;
		$commandCount->select('COUNT(item.id) as itemCount');
		$numRecords =$commandCount->queryScalar($params);

		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> array('pageSize' => $pageSize),
			'keyField'			=> 'id_cert',
			'params'			=> $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);

		return $dataProvider;
	}
	
	
	
	/**
	 * General usage method to issue a Certification to a User through a particular Item assigned to the certification (course, plan, transcript, ...)
	 * Apply checks about
	 * 	-- allow using the same learning item to obtain the same certification?
	 *  -- is the user certification in a "renew mode" ?
	 * 
	 * @param integer $idCertItem  
	 * @param integer $idUser
	 */
	public static function issueCertification($idCertItem, $idUser, $extraData=array(), $transcriptModel = false) {
	
		$idCertItem = (int) $idCertItem;
		$idUser = (int) $idUser;
		
		$certItemModel = CertificationItem::model()->findByPk($idCertItem);
		$userModel = CoreUser::model()->findByPk($idUser);
		
		if (!$certItemModel || !$userModel) {
			Yii::log("An attempt to issue a certification failed. Either the item or the user is invalid [$idCertItem, $idUser] ", CLogger::LEVEL_ERROR);
			return;
		}
		
		// Get the certificaton model, we will need it later
		$certModel = $certItemModel->certification;
		if (!$certModel) {
			Yii::log("An attempt to issue an invalid certification failed. Item ID# " . $idCertItem, CLogger::LEVEL_ERROR);
			return;
		}

		if($certModel->deleted == Certification::$CERTIFICATION_SOFT_DELETED){
			return;
		}
		
		// Check if user is currently certified by a NON-archived certifications (sort of "active" certification, even if it is expired) 
		$isUserCertified = Certification::isIssuedTo(array($certModel->id_cert), array($idUser));
		
		// Check if the learning item of this certification item is already used to certify THAT user by THAT certification, EVER
		$certsAlreadyUsing 		 = Certification::certificationsUsedLearningItemToCertifyUser($certItemModel->id_item, $certItemModel->item_type, $idUser);
		$learningItemAlreadyUsed = is_array($certsAlreadyUsing) && in_array($certModel->id_cert, $certsAlreadyUsing);
		
		// Check if it is NOT-allowed to obtain the same certification using the same learning item (course, plan, ...)
		// If that is the case, get out and stop the process
		if ($learningItemAlreadyUsed && !$certModel->allow_same_item) {
			Yii::log("Issuing/renewing certification canceled as it is not allowed to be certified by completing the same learning item", CLogger::LEVEL_INFO);
			Yii::log("Learning item type and ID: {$certItemModel->item_type}, {$certItemModel->id_item}", CLogger::LEVEL_INFO);
			return;
		}

		// We need these later
		$utcNow 		= Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		$localNow 		= Yii::app()->localtime->toLocalDateTime($utcNow);
		
		// If user has NO "current/active/non-archived" certification.. create new one
		if (!$isUserCertified) {
			Yii::log("Executing Certification assignment: ({$certModel->title}, {$userModel->userid})", CLogger::LEVEL_INFO);
			Yii::log("User completed certification item: {$certItemModel->item_type} / {$certItemModel->id_item}", CLogger::LEVEL_INFO);

			/**
			 * @var $transcriptModel TranscriptsRecord
			 */

			$toDate = $localNow;

			if($transcriptModel && $transcriptModel->to_date){
				$toDate = $transcriptModel->to_date;
			}

			$certUser = new CertificationUser();
			$certUser->id_user = $userModel->idst;
			$certUser->on_datetime 	= $toDate;
			$certUser->setExpireAt($certModel);
			$certUser->id_cert_item = $certItemModel->id;
			$certUser->extra_data = json_encode($extraData);
			$certUser->save();
			
			Yii::app()->event->raise('CertificationIssued', new DEvent(self, array(
				'certificationUser' 	=> $certUser,
			)));
			
			return;
		}

		// So, user IS certified, lets see what can we do....
		
		// Get current user certification model
		$certUser = self::getCurrentUserCertification($certModel->id_cert, $idUser);
		
		// If this issued certification is NOT in renew mode, get out (unless the incoming item is a Transcript, which override the in_renew)
		// Renew mode is set when User Clicks on RENEW links somewhere, i.e. it is a result of USER action
		if (!$certUser->in_renew && ($certItemModel->item_type !== CertificationItem::TYPE_TRANSCRIPT)) {
			Yii::log("Certification renewal cancelled as it is not in renew mode (#{$certModel->id_cert}, {$certModel->title})", CLogger::LEVEL_INFO);
			Yii::log("User must initiate RENEW process by selecting one of assigned items to the certification being renewed", CLogger::LEVEL_INFO);
			return;
		}

		// Soo, we have a certification in a renew mode, archive it
		Yii::log("Executing Certification Renewal: ({$certModel->title}, {$userModel->userid})", CLogger::LEVEL_INFO);
		$certUser->archived = 1;
		$certUser->in_renew = 0;
		$certUser->save();
		

		// We always end up with creating a NEW user certification!
		// Create new certification for the user, the "current" one
		$certUser = new CertificationUser();
		$certUser->archived = 0;
		$certUser->in_renew = 0;
		$certUser->id_user = $idUser;
		$certUser->on_datetime 	= $localNow;
		$certUser->id_cert_item = $certItemModel->id;
		$certUser->setExpireAt($certModel);
		$certUser->extra_data = json_encode($extraData);
		$certUser->save();
		
		$never = $certUser->expire_at == null;
		Yii::log("Certification will expire at: " . ($never ? 'NEVER' : $certUser->expire_at), CLogger::LEVEL_INFO);
			
		Yii::app()->event->raise('CertificationIssued', new DEvent(self, array(
			'certificationUser' 	=> $certUser,
		)));
		
	
	}

	
	/**
	 * Calculate and set expiration time of THIS model, with a little help of a Certification model
	 */
	public function setExpireAt($certModel=false) {
		
		$start 		= $this->on_datetime;
		if (!$certModel) {
			$certModel = $this->certificationItem->certification;
		}
		$duration 	= (int) $certModel->duration;
		
		if ($duration <= 0) {
			$this->expire_at = null; // never
			return;
		}
		
		$unit 		= $certModel->duration_unit;
		switch ($unit) {
			case Certification::DURATION_UNIT_WEEK:
				$v = "P{$duration}W";
				break;
	
			case Certification::DURATION_UNIT_MONTH:
				$v = "P{$duration}M";
				break;
	
			case Certification::DURATION_UNIT_YEAR:
				$v = "P{$duration}Y";
				break;
	
			case Certification::DURATION_UNIT_DAY:
			default:
				$v = "P{$duration}D";
				break;
		}
		
		$this->expire_at = Yii::app()->localtime->addInterval($start, $v);
	}
	
	
	/**
	 * Get USER certification (issued one) of idCert type, which is current/non-archived
	 * 
	 * @param integer $idCert Certification ID
	 * @param integer $idUser
	 * @param boolean $returnModel
	 * 
	 * @return boolean|CertificationUser
	 */
	public static function getCurrentUserCertification($idCert, $idUser, $returnModel=true) {
		
		// SQL Parameters
		$params = array();
		
		$command = Yii::app()->getDb()->createCommand();
		$command->from('certification_user cu');
		$command->join('certification_item ci', 'cu.id_cert_item=ci.id');
		$command->andWhere('ci.id_cert=:id_cert');
		$command->andWhere('cu.id_user=:id_user');
		$command->andWhere('cu.archived=0');
		
		$params[':id_cert'] 	= (int) $idCert;
		$params[':id_user'] 	= (int) $idUser;
		
		$command->select('cu.id AS cuId');
		
		$idCertUser = $command->queryScalar($params); 

		if ($returnModel) {
			$result = self::model()->findByPk($idCertUser);
		}
		else {
			$result = $idCertUser;
		}
		
		return $result;
		
	}
	

	/**
	 * Check if this user certification is expired
	 * 
	 * @param string $pivotDateTime Datetime to compare to, optionsl, e.g. 2014-01-01 00:00:00
	 */
	public function isExpired($pivotDateTime=false) {
		if (!$pivotDateTime) {
			$pivotDateTime = Yii::app()->localtime->getUTCNow();
		}
		return Yii::app()->localtime->isGt($pivotDateTime, $this->expire_at);
	}
	
	

	/**
	 * Return number of seconds between pivotal time and user certification expire_at time.
	 * Negative value is possible (when pivotal point in time is AFTER the expire time
	 * 
	 * @param string $pivotDateTime, e.g. 2011-11-11 00:01:02
	 */
	public function timeToExpire($pivotDateTime=false) {
		if (!$pivotDateTime) {
			$pivotDateTime = Yii::app()->localtime->getUTCNow();
		}
		return Yii::app()->localtime->diff($pivotDateTime, $this->expire_at);
	}
	
	
	
	/**
	 * Helper static method to check if a given user certification is expired.
	 * Internally using isExpired().
	 * 
	 * @param integer $userCertId
	 * @param string $pivotDateTime
	 * @return boolean
	 */	
	public static function isUserCertificationExpired($userCertId, $pivotDateTime=false) {
		if (!$pivotDateTime) {
			$pivotDateTime = Yii::app()->localtime->getUTCNow();
		}
		$userCertId = (int) $userCertId;
		$model = self::model()->findByPk($userCertId);
		return $model->isExpired($pivotDateTime);
	}



	/**
	 * @param $id
	 * @return bool
	 */
	public static function showRenewButton($id){
		$showButton = false;
		$certUser = CertificationUser::model()->findByPk($id);
		if($certUser){
			$assignedItems = $certUser->certificationItem->certification->getAssignmentInfo();
			$certModel = $certUser->certificationItem->certification;
			$idUser = Yii::app()->user->id;

			foreach ($assignedItems['courses'] as $assignedItemId) {
				// If this item is already used in ANY user's certification in the past?
				$isAlreadyUsed = Certification::certificationsUsedLearningItemToCertifyUser($assignedItemId, CertificationItem::TYPE_COURSE, $idUser);

				// Check if are allowed to re-train using the same item
				// If not, we must exclude the item from the list
				$allowRetrain = $certModel->allow_same_item || $isAlreadyUsed === false;
				if($allowRetrain) {
					$showButton = true;
					break;
				}
			}

			if(!$showButton){
				foreach ($assignedItems['plans'] as $assignedItemId) {
					$isAlreadyUsed = Certification::certificationsUsedLearningItemToCertifyUser($assignedItemId, CertificationItem::TYPE_LEARNING_PLAN, $idUser);
					$allowRetrain = $certModel->allow_same_item || $isAlreadyUsed === false;
					if($allowRetrain) {
						$showButton = true;
						break;
					}
				}
			}
		}

		return $showButton;
	}
	
	
}



