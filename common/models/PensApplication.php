<?php

/**
 * This is the model class for table "pens_application".
 *
 * The followings are the available columns in table 'pens_application':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $app_id
 * @property string $user_id
 * @property string $password
 * @property integer $enable
 * @property string $date_creation
 * @property string $date_update
 * @property integer $not_editable
 * @property integer $related_plugin
 * @property integer $category_id

 */
class PensApplication extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pens_application';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, app_id, user_id, password', 'required'),
			array('enable, not_editable, category_id', 'numerical', 'integerOnly'=>true),
			array('name, password, related_plugin', 'length', 'max'=>255),
			array('app_id, user_id', 'length', 'min'=>8, 'max'=>8),
			array('description, date_creation, date_update', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, description, app_id, user_id, password, enable, date_creation, date_update, not_editable, related_plugin, category_id', 'safe', 'on'=>'search'),

			//custom rules
			array('app_id, user_id', 'idValidity'),
			array('password', 'passwordValidity')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, LearningCourseCategoryTree::model()->tableName(), 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => Yii::t('standard', '_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'app_id' => Yii::t('pens', 'Application ID'),
			'user_id' => Yii::t('pens', 'User ID'),
			'password' => Yii::t('standard', '_PASSWORD'),
			'enable' => Yii::t('pens', 'Enable/disable'),
			'date_creation' => Yii::t('report', '_CREATION_DATE'),
			'date_update' => Yii::t('standard', 'Last update'),
			'not_editable' => Yii::t('pens', 'Not editable'),
			'related_plugin' => 'Related Plugin',
			'category_id' => Yii::t('standard', '_CATEGORY'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('enable',$this->enable);
		$criteria->compare('date_creation',$this->date_creation,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('not_editable',$this->not_editable);
		$criteria->compare('related_plugin',$this->related_plugin,true);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PensApplication the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the list of behaviors associated to the model
	 * @return array
	 */
	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('date_creation, date_update'),
				'dateAttributes' => array()
			)
		);
	}


	/**
	 * @param bool|string $searchInput text to be searched
	 * @return CSqlDataProvider
	 */
	public static function dataProvider($searchInput = false) {
		// SQL Parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(PensApplication::model()->tableName());

		// Also search by user related text
		if (!empty($searchInput)) {
			$commandBase->andWhere("name LIKE :search_input");
			$params[':search_input'] = '%'.strtolower(addcslashes($searchInput, '%_')).'%';
		}

		// DATA
		$commandData = clone $commandBase;

		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('COUNT(*)');
		$numRecords = $commandCounter->queryScalar($params);


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'name' => array(
				'asc'  => 'name ASC',
				'desc' => 'name DESC',
			),
			'description' => array(
				'asc'  => 'description ASC',
				'desc' => 'description DESC',
			),
			'app_id' => array(
				'asc' => 'app_id ASC',
				'desc' >= 'app_id DESC'
			)
		);
		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);

		// DATA-II
		$commandData->select("*");


		// END
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 20)
			),
			'keyField' => 'id',
			'sort' => $sort,
			'params' => $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);

		return $dataProvider;
	}


	/**
	 * Generates an unique (APPLICATION ID , USER ID) value. Both IDs are alphanumeric strings of 8 characters.
	 * @return array
	 */
	public static function generateUniqueKey() {
		//prepare generating characters pool
		$numbers = '0123456789';
		$letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$maxLoops = 10; //security limit to avoid potential infinite looping

		do {
			//initialize output variable
			$output = array(
				'app_id' => '',
				'user_id' => ''
			);

			foreach (array_keys($output) as $key) {
				// we make the first character always a letter in order to avoid potential errors, since some applications
				// may interprete it as a number and trying to convert the ID
				$output[$key] .= $letters{rand(0, strlen($letters) - 1)};

				// then fill all the other 7 characters of the generated ID. Now they can be either numbers or letters.
				for ($i = 0; $i < 7; $i++) {
					//first choose between numbers or letters
					$typeChooser = rand(0, 100);
					if (($typeChooser % 2) > 0) {
						//letters
						$output[$key] .= $letters{rand(0, strlen($letters) - 1)};
					} else {
						//numbers
						$output[$key] .= $numbers{rand(0, strlen($numbers) - 1)};
					}
				}
			}

			//make sure that the generated values are unique in the DB
			$tester = PensApplication::model()->findByAttributes($output);

			$maxLoops--;

		} while (!empty($tester) && $maxLoops > 0);

		if ($maxLoops <= 0 && !empty($tester)) {
			// We were not able to generate an unique key. This is EXTREMELY improbable, but still not impossible ...
			// TODO: should it be appropriate to throw an exception or return a false/null value here?
			throw new CException('An error occurred');
		}

		return $output;
	}


	/**
	 * Generate a random password for the applications
	 * @return string
	 */
	public static function generatePassword() {
		$numChars = 12 + rand(0, 8); //generated password can be 12-20 characters in length
		$pool = array(
			'0123456789',
			'abcdefghijklmnopqrstuvwxyz',
			'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'@!?-_#*+=&%$'
		);
		$output = ''; //initialize output variable

		//make sure that at least one character for each type of pool characters is present at random position
		$selected = array(); //the selected position for each character type
		$positions = array(); //temporary indexes list from which choose randomly the character position in the final password
		for ($i=0; $i<$numChars; $i++) {
			$positions[] = $i;
		}
		for ($i=0; $i<count($pool); $i++) {
			$spliced = array_splice($positions, rand(0, count($positions)-1), 1); //$spliced will always be an array with one cell only
			$selected[$spliced[0]] = $i;
		}
		$positions = NULL; //remove no more useful variable from memory
		unset($positions);

		//finally compose the random generated password
		$iterations = 0; //security check to avoid potential infinite loops (although it is an extremely improbable eventuality)
		for ($i = 0; $i < $numChars; $i++) {
			if (array_key_exists($i, $selected)) {
				$poolIndex = $selected[$i];
			} else {
				$poolIndex = rand(0, count($pool)-1);
			}
			$newChar = $pool[$poolIndex]{rand(0, strlen($pool[$poolIndex]) - 1)};
			//do a check in order to avoid sequences of equal characters. Maximum of 2 sequantial equal characters are allowed.
			$length = strlen($output);
			if ($length >= 2 && $output{$length-1} == $newChar && $output{$length-2} == $newChar) {
				//same characters detected, re-loop this one and generate a new character
				$i--;
			} else {
				//it is ok, add the new character to the output
				$output .= $newChar;
			}
			//loop security check
			$iterations++;
			if ($iterations >= 1000) {
				throw new CException('An error occurred');
			}
		}

		return $output;
	}


	/**
	 * Checks the validity for Application ID and User ID values.
	 * Validity rules are:
	 * - value must be 8 characters in length
	 * - must must contain only valid characters
	 * This is the 'idValidity'  validator as declared in rules().
	 * @param $attribute
	 * @param $params
	 */
	public function idValidity($attribute, $params)
	{
		$value = $this->$attribute;

		//check length (although it should have already been checked by another rule)
		if (strlen($value) != 8) {
			$this->addError($attribute, $this->getAttributeLabel($attribute).' must be 8 characters in length');
		}

		//check characters
		$pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$invalidChars = array();
		for ($i=0; $i<strlen($value); $i++) {
			$toBeChecked = $value{$i};
			if (strpos($pool, $toBeChecked) === false) {
				$invalidChars[] = $toBeChecked;
			}
		}
		if (count($invalidChars) > 0) {
			$this->addError($attribute, $this->getAttributeLabel($attribute).' contains the following not allowed characters: '.implode(', ', $invalidChars));
		}
	}


	/**
	 * Checks if the password is valid.
	 * Validity rules are:
	 * - password must contain only valid characters
	 * - password must be between 12 and 20 characters in length
	 * - no more than 2 sequential equal characters are allowed
	 * This is the 'passwordValidity' validator as declared in rules().
	 * @param $attribute
	 * @param $params
	 */
	public function passwordValidity($attribute, $params)
	{
		$value = $this->$attribute;

		//check length
		$length = strlen($value);
		if ($length < 12 || $length > 20) {
			$this->addError($attribute, 'Password is '.$length.' characters long while it must be between 12 and 20 characters');
		}

		//check characters
		$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!?-_#*+=&%$';
		$invalidChars = array();
		for ($i=0; $i<strlen($value); $i++) {
			$toBeChecked = $value{$i};
			if (strpos($pool, $toBeChecked) === false) {
				$invalidChars[] = $toBeChecked;
			}
		}
		if (count($invalidChars) > 0) {
			$this->addError($attribute, 'Password contains the following not allowed characters: '.implode(', ', $invalidChars));
		}

		//check sequential equal characters
		$noRepetitions = true;
		for ($i=0; $i<(strlen($value)-2) && $noRepetitions; $i++) {
			$currentChar = $value{$i};
			$nextChar = $value{$i+1};
			if ($currentChar == $nextChar) {
				$nextNextChar = $value{$i+2};
				if ($currentChar == $nextNextChar) {
					$noRepetitions = false;
				}
			}
		}
		if (!$noRepetitions) {
			$this->addError($attribute, 'Password contains more than 2 sequential equal characters');
		}
	}

}
