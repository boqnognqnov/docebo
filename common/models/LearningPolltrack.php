<?php

/**
 * This is the model class for table "learning_polltrack".
 *
 * The followings are the available columns in table 'learning_polltrack':
 * @property integer $id_track
 * @property integer $id_user
 * @property integer $id_reference
 * @property integer $id_poll
 * @property string $date_attempt
 * @property string $status
 */
class LearningPolltrack extends CActiveRecord
{

	const STATUS_NOT_COMPLETE = 'not_complete';
	const STATUS_VALID = 'valid';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningPolltrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_polltrack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_reference, id_poll', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>12),
			array('date_attempt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_track, id_user, id_reference, id_poll, date_attempt, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_track' => 'Id Track',
			'id_user' => 'Id User',
			'id_reference' => 'Id Reference',
			'id_poll' => 'Id Poll',
			'date_attempt' => Yii::t('standard', 'Date Attempt'),
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_track',$this->id_track);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_reference',$this->id_reference);
		$criteria->compare('id_poll',$this->id_poll);
		$criteria->compare('date_attempt',Yii::app()->localtime->fromLocalDateTime($this->date_attempt),true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'commonTrack' => array(
				'class' => 'CommontrackBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
				'idResourcePropertyName' => 'id_poll',
				'idUserPropertyName' => 'id_user',
				'idOrgPropertyName' => 'id_reference',
				'statusPropertyName' => 'status',
				'statusConversion' => array(
					self::STATUS_NOT_COMPLETE => LearningCommontrack::STATUS_ATTEMPTED,
					self::STATUS_VALID => LearningCommontrack::STATUS_COMPLETED,
				)
			),
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('date_attempt medium'),
			 'dateAttributes' => array()
            )
		);
	}




	public function reset() {
		
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		
		try {
		
			$questions = LearningPollquest::model()->findByAttributes(array('id_poll' => $this->id_poll));
			if (!empty($questions)) {
				foreach ($questions as $question) {
					$questionManager = PollQuestion::getQuestionManager($question->type_quest);
					if ($questionManager && method_exists($questionManager, 'deleteUserAnswer')) {
						$questionManager->deleteUserAnswer($this->track->getPrimaryKey(), $question->getPrimaryKey());
					}
				}
			}

			$newTrackInfo = array(
				'date_attempt' => Yii::app()->localtime->toLocalDateTime(),
				'score_status' => self::STATUS_NOT_COMPLETE
			);
			$this->setAttributes($newTrackInfo);
			if (!$this->save()) {
				throw new CException('Error while resetting track info');
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {
			
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
	}



	public function storePage($pageToSave) {

		$poll = LearningPoll::model()->findByPk($this->id_poll);
		$questions = $poll->getQuestionsForPage($pageToSave);
		if (!empty($questions)) {
			foreach ($questions as $question) {

				if ($questionObject = PollQuestion::getQuestionManager($question->type_quest)) {
					$storing = $questionObject->storeUserAnswer($this->getPrimaryKey(), $question->getPrimaryKey(), false, true);
				}
			}
		}
	}

	public function getAnsweredQuestions() {
		$list = Yii::app()->db->createCommand()
			->select('*')
			->from(LearningPolltrack::model()->tableName().' pt')
			->join(LearningPollquest::model()->tableName().' pq', 'pq.id_poll = pt.id_poll')
			->where('pt.id_track = :id_track', array(':id_track' => $this->getPrimaryKey()))
			->andWhere('pq.type_quest <> :type_2', array(':type_2' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
			->order('pq.sequence ASC')->queryAll();

		return $list;
	}

	
}