<?php

/**
 * This is the model class for table "learning_lo_types".
 *
 * The followings are the available columns in table 'learning_lo_types':
 * @property string $objectType
 * @property string $className
 * @property string $fileName
 * @property string $classNameTrack
 * @property string $fileNameTrack
 */
class LearningObjectType extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LearningObjectType the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'learning_lo_types';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('objectType, className', 'length', 'max'=>20),
            array('fileName', 'length', 'max'=>50),
            array('classNameTrack, fileNameTrack', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('objectType, className, fileName, classNameTrack, fileNameTrack', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'objectType' => Yii::t('standard', 'Object Type'),
            'className' => 'Class Name',
            'fileName' => 'File Name',
            'classNameTrack' => 'Class Name Track',
            'fileNameTrack' => 'File Name Track',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('objectType',$this->objectType,true);
        $criteria->compare('className',$this->className,true);
        $criteria->compare('fileName',$this->fileName,true);
        $criteria->compare('classNameTrack',$this->classNameTrack,true);
        $criteria->compare('fileNameTrack',$this->fileNameTrack,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }
}