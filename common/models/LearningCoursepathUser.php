<?php

/**
 * This is the model class for table "learning_coursepath_user".
 * The followings are the available columns in table 'learning_coursepath_user':
 * @property integer $id_path
 * @property integer $idUser
 * @property integer $waiting
 * @property integer $course_completed
 * @property string $date_assign
 * @property integer $subscribed_by
 * @property string $date_begin_validity
 * @property string $date_end_validity
 * $property integer $catchup_user_limit
 * $property integer $deeplinked_by
 *
 * The followings are the available model relations:
 * @property LearningCoursepath $path
 * @property CoreUser $user
 * @property LearningCertificateAssignCp $learningCertificateAssignCp
 * @property LearningCertificateCoursepath $learningCertificateCoursepath
 */
class LearningCoursepathUser extends CActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCoursepathUser the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

    public function behaviors() {
        return array(
            'modelSelect' => array(
                'class' => 'ModelSelectBehavior'
            ),
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('date_assign medium, date_begin_validity short, date_end_velidity short'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_coursepath_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_path, idUser, waiting, course_completed, subscribed_by, catchup_user_limit, deeplinked_by', 'numerical', 'integerOnly' => true),
			array('date_assign', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_path, idUser, waiting, course_completed, date_assign, subscribed_by, catchup_user_limit, deeplinked_by', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreUser' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			// required in certificateManagement/_release ! don't remove. try to remove the line above instead
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'path' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path', 'joinType' => 'INNER JOIN'),
			'learningCertificateAssignCp' => array(self::HAS_ONE, 'LearningCertificateAssignCp', array('id_user' => 'idUser', 'id_path' => 'id_path')),
			'learningCertificateCoursepath' => array(self::HAS_ONE, 'LearningCertificateCoursepath', 'id_path'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id_path' => 'Id Path',
			'idUser' => 'Id User',
			'waiting' => 'Waiting',
			'course_completed' => 'Course Completed',
			'date_assign' => 'Date Assign',
			'subscribed_by' => 'Subscribed By',
            'catchup_user_limit' => 'Catchup User Limit',
            'deeplinked_by'     => 'Deeplinked By'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_path', $this->id_path);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('waiting', $this->waiting);
		$criteria->compare('course_completed', $this->course_completed);
		$criteria->compare('date_assign', Yii::app()->localtime->fromLocalDateTime($this->date_assign), true);
		$criteria->compare('subscribed_by', $this->subscribed_by);
		$criteria->compare('catchup_user_limit', $this->catchup_user_limit);
		$criteria->compare('deeplinked_by', $this->deeplinked_by);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	/**
	 * Power user filter visibility on users
	 * @var \CoreUser
	 */
	public $powerUser = NULL;

	public function dataProvider() {
		$criteria = new CDbCriteria;

		$criteria->with = array('coreUser');

		$criteria->compare('id_path', $this->id_path);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('waiting', $this->waiting);
		$criteria->compare('course_completed', $this->course_completed);
		$criteria->compare('date_assign', Yii::app()->localtime->fromLocalDateTime($this->date_assign), true);
		$criteria->compare('subscribed_by', $this->subscribed_by);
		$criteria->compare('catchup_user_limit', $this->catchup_user_limit);
		$criteria->compare('deeplinked_by', $this->deeplinked_by);

		if ($this->coreUser !== null) {
			if ($this->coreUser->search_input) {
				$criteria->addCondition('CONCAT(coreUser.firstname, " ", coreUser.lastname) LIKE :search OR coreUser.email LIKE :search OR coreUser.userid LIKE :search');
				$criteria->params[':search'] = '%' . $this->coreUser->search_input . '%';
			}

			if($this->coreUser->order_by)
				$criteria->order = $this->coreUser->order_by;
		}

		switch ($this->scenario) {
			case 'curricula_management':
				if (!empty($this->powerUser)) {
					/*
					$criteria->with['powerUsers'] = array(
						'joinType' => 'INNER JOIN',
						'condition' => 'powerUsers.idst=:puser_id',
						'params' => array(':puser_id' => $this->powerUser->idst),
					);
					$criteria->together = true;
					$criteria->group = 't.idUser';
					*/
					$criteria->join = "JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.user_id = t.idUser AND pu.puser_id = :puser_id)";
					$criteria->params[':puser_id'] = $this->powerUser->idst;
				}
			break;
		}

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	public function dataProviderSelectAll(){

		$db = Yii::app()->db;
		$command = $db->createCommand()
			->select('idUser, id_path')
			->from($this->tableName().' lcu')
			->where('lcu.id_path =  '.$this->id_path)
			->join(CoreUser::model()->tableName() . " u", "u.idst = lcu.idUser");


		if ($this->coreUser !== null) {
			if ($this->coreUser->search_input) {
				$command->andWhere('CONCAT(u.firstname, " ", u.lastname) LIKE :search OR u.email LIKE :search OR u.userid LIKE :search' ,
					array(':search'=> '%' . $this->coreUser->search_input . '%'));
			}
		}


		switch ($this->scenario) {
			case 'curricula_management':
				if (!empty($this->powerUser) && $this->powerUser->idst) {
					$command->join( CoreUserPU::model()->tableName(), " (".CoreUserPU::model()->tableName().".user_id = lcu.idUser AND ".CoreUserPU::model()->tableName().".puser_id = :puser_id)", array(':puser_id'=>$this->powerUser->idst));
				}
				break;
		}
		$count = $command->queryScalar();


		return new CSqlDataProvider($command, array(
			'totalItemCount'=>$count,
			'pagination'=>false,
		));
	}

	public function dataProviderCertificate() {
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->compare('t.id_path', $this->id_path);
		$criteria->compare('user.userid', $this->user->userid, TRUE);
		// $criteria->compare('learningCertificateAssign.id_certificate', $this->learningCertificateAssign->id_certificate, false, 'OR');
		// $criteria->compare('learningCertificateCourse.id_certificate', $this->learningCertificateCourse->id_certificate, false, 'OR');
		$criteria->compare('t.idUser', '<>/Anonymous');

		if(Yii::app()->user->getIsPu()){
			$currentPuUserUsers = Yii::app()->getDb()->createCommand()
				->select('user_id')
				->from(CoreUserPU::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idUser', $currentPuUserUsers);
		}

		$criteria->with = array(
			'user' => array('joinType' => 'INNER JOIN'),
			'learningCertificateAssignCp' => array(
				'on' => 'learningCertificateAssignCp.id_certificate = :id_certificate',
				'params' => array(':id_certificate' => $this->learningCertificateAssignCp->id_certificate)
			),
			'learningCertificateCoursepath' => array(
				'on' => 'learningCertificateCoursepath.id_certificate = :id_certificate',
				'params' => array(':id_certificate' => $this->learningCertificateCoursepath->id_certificate)
			)
		);
		$criteria->group = 'user.idst';

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}



	public function dataProviderMyActivities($idUser = false) {

		if (empty($idUser)) { $idUser = Yii::app()->user->id; }

		$criteria = new CDbCriteria;

		$criteria->with = array('path');

		if (Yii::app()->user->getIsPu())
		{
			$criteria->with['path.powerUserManagers'] = array(
				'joinType' => 'INNER JOIN',
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => Yii::app()->user->getIdst()),
				'together' => true
			);
		}

		$criteria->compare('id_path', $this->id_path);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('waiting', $this->waiting);
		$criteria->compare('course_completed', $this->course_completed);
		$criteria->compare('date_assign', Yii::app()->localtime->fromLocalDateTime($this->date_assign), true);
		$criteria->compare('subscribed_by', $this->subscribed_by);
		$criteria->compare('catchup_user_limit', $this->catchup_user_limit);
		$criteria->compare('deeplinked_by', $this->deeplinked_by);

		$criteria->addCondition("idUser = :id_user");
		$criteria->params[':id_user'] = $idUser;

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		$sortAttributes['path_code'] = 'path.path_code';
		$sortAttributes['path_name'] = 'path.path_name';
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'path.path_code ASC, path.path_name ASC';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}


	public function getCertificateUserLevel() {
		$available_for_status = $this->learningCertificateCoursepath->available_for_status;

		if (!empty($this->learningCertificateAssignCp)) {
			return 1;
		} elseif (($available_for_status == 1) || ($available_for_status == 2 && $this->waiting) || ($available_for_status == 3 && $this->course_completed)) {
			return 2;
		}
		return 0;
	}

	public function renderCertificateStatus() {
		$level = $this->getCertificateUserLevel();

		if ($level === 1) {
			$content = CHtml::link(
					'<span class="cert-generated"></span>', // Link label
					// Link URL
					Docebo::createAdminUrl('certificateManagement/download', array(
						'id' => $this->learningCertificateAssignCp->id_certificate,
						'user_id' => $this->learningCertificateAssignCp->id_user,
						'path_id' => $this->learningCertificateAssignCp->id_path,
						)
					),
					// HTML Options
					array(
					'data-title' => Yii::t('catalogue', '_CERT_RELESABLE'),
					'title' => Yii::t('catalogue', '_CERT_RELESABLE'),
					'data-toggle' => 'tooltip',
					)
			);
		} elseif ($level === 2) {
			$content = CHtml::link(
					'<span class="cert-cant-generated"></span>', // Link label
					NULL, // URL
					// HTML Options
					array(
					'data-title' => Yii::t('certificate', '_NO_CERT_AVAILABLE'),
					'title' => Yii::t('certificate', '_NO_CERT_AVAILABLE'),
					'data-toggle' => 'tooltip',
					)
			);
		}
		return $content;
	}

	/**
	 * Checks if a certificate must be generated for this learningplan - user; if so, generate one.
	 */
	public function generateCertificate() {

		// See if there is a certificate assigned to this learning plan
		$certificate = LearningCertificateCoursepath::model()->findByAttributes(array('id_path' => $this->id_path));
		if ($certificate) {
			// The learning plan has a Certificate for completion assigned
			$certificateId = $certificate->id_certificate;
			$oldCertificate = LearningCertificateAssignCp::model()->findByAttributes(array(
				'id_certificate' => $certificateId,
				'id_user' => $this->idUser,
				'id_path' => $this->id_path,
			));

			if (!$oldCertificate) {
				// Only generate a certificate if not generated previously
				try {
					$substitution = Yii::app()->certificateTag->getSubstitution($this->idUser, $this->id_path, TRUE);
					Yii::app()->certificate->createForLearningPlan($certificateId, $this->idUser, $this->id_path, $substitution, FALSE);
				} catch (Exception $e) {
					Yii::log('Can not generate certificate for learningplan. LearningCourseuser::afterSave', CLogger::LEVEL_ERROR);
				}
			}
		}
	}

	/**
	 * @param $userId
	 * @param $pathId
	 * @param bool $subscribeToCourses
	 * @param bool $updatePowerUserSelection
	 * @return bool
	 */
	public static function subscribeUser($userId, $pathId, $subscribeToCourses = true, $updatePowerUserSelection = false) {
		$result = false;
		if (self::loadSubscription($userId, $pathId) === null) {
			$model = new LearningCoursepathUser();
			$model->idUser = $userId;
			$model->id_path = $pathId;
			$model->date_assign = Yii::app()->localtime->toLocalDateTime();
			$model->subscribed_by = Yii::app()->user->id;
			$model->save(false);
			$curriculaModel = LearningCoursepath::model()->findByPk($pathId);
            //do we need to skip subscription to LP courses?
            if ($subscribeToCourses) {
                $curriculaModel->subscribeSingleUserToCourses($userId);
            }
			Yii::app()->event->raise('UserEnrolledInLearningPlan', new DEvent(self, array('user_id' => $userId, 'learning_plan' => $curriculaModel, 'updatePowerUserSelection' => $updatePowerUserSelection)));

			$result = true;
		}
		return $result;
	}

	/**
	 * @param $userId
	 * @param $pathId
	 * @return bool
	 */
	public static function unsubscribeUser($userId, $pathId) {
		$result = false;
		$model = self::loadSubscription($userId, $pathId);
		if ($model !== null) {
			$model->delete();
		}
		return $result;
	}

	/**
	 * @param $userId
	 * @param $pathId
	 * @return CActiveRecord | null
	 */
	public static function loadSubscription($userId, $pathId) {
		return self::model()->findByAttributes(array(
				'idUser' => $userId,
				'id_path' => $pathId,
		));
	}

	/**
	 * @param $id_user The idst of the user
	 * @return int The number of coursepath associated to the users
	 */
	public static function getUserCoursepathNumber($id_user) {
		return self::model()->with('path')->countByAttributes(array('idUser' => $id_user));
	}

	public static function getEnrolledLearningPlanIdsByUser($userId){
		static $cache = array();

		if(!isset($cache[$userId])) {
			$cache[$userId] = Yii::app()->getDb()->createCommand()
			   ->select( 'id_path' )
			   ->from( LearningCoursepathUser::model()->tableName() )
			   ->where( 'idUser=:idUser', array( ':idUser' => $userId ) )
			   ->queryColumn();
		}

		return $cache[$userId];
	}

	/**
	 * This method is invoked after saving a record successfully.
	 */
	protected function afterSave()
	{
		if($this->isNewRecord) {
			// If there's a days of validity set for this LP, let's update date_begin_validity and date_end_validity
			if($this->path->days_valid) {
				Yii::app()->getDb()->createCommand()
					->update(self::model()->tableName(), array(
						'date_begin_validity'=>new CDbExpression('date_assign'),
						'date_end_validity' => new CDbExpression('DATE_ADD(date_assign, INTERVAL :days DAY)')
					), 'id_path=:idPath AND idUser=:idUser', array(
						':idPath'=>$this->id_path,
						':idUser'=>$this->idUser,
						':days' => $this->path->days_valid
					));
			}
		}

		return parent::afterSave();
	}

    /**
     * This function return true if a User has completed All Learning Plans assigned to him
     * @param $idUser - the id of the user
     * @param $idPlans array of learning plan's id
     * @return bool true / false
     */
    public static function getCompletedLearningPlansByUser ($idUser, $idPlans = array()) {
        if ( !$idUser || empty( $idUser)) return false;
        if ( empty( $idPlans) || !isset( $idPlans) || ( isset( $idPlans) && !is_array( $idPlans)) ) return false;
        $flag = true;
        /* Query for getting all Learning Plans and the count of ALL courses in the LP and the COMPLETED courses */
        $command = Yii::app()->getDb()->createCommand()
            ->select('lp.id_path, lp.path_name, lp.img, COUNT(cpc.id_item) AS totalCourses, SUM(cu.status=0) AS nonStartedCourses, SUM(cu.status=2) AS completedCourses')
            ->from(LearningCoursepath::model()->tableName() . ' lp')
            ->leftJoin(LearningCoursepathCourses::model()->tableName() . ' cpc', 'lp.id_path=cpc.id_path')
            ->leftJoin(LearningCourseuser::model()->tableName() . ' cu', 'cpc.id_item=cu.idCourse AND cu.idUser=:idUser',
                array(':idUser' => $idUser))
            ->where('lp.id_path IN ('. implode(',', $idPlans). ')')
            ->group('lp.id_path');
        /* End Query */
        $lp = $command->queryAll(true);

        if ( isset($lp) && is_array($lp) ) {
            foreach ( $lp as $plan ) {
                if ( $plan['totalCourses'] == $plan['completedCourses'] ) {
                    $flag = (($flag === false) ? false : true);
                } else {
                    $flag = false;
                }
            }
        } else {
           return false;
        }

        if ( $flag )
            return true;
        else
            return false;

    }
}
