<?php

/**
 * This is the model class for table "learning_forum_timing".
 *
 * The followings are the available columns in table 'learning_forum_timing':
 * @property integer $idUser
 * @property integer $idThread
 * @property string $last_access
 */
class LearningForumTiming extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningForumTiming the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_forum_timing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idThread', 'numerical', 'integerOnly'=>true),
			array('last_access', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUser, idThread, last_access', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('last_access medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUser' => 'Id User',
			'idThread' => 'Id Thread',
			'last_access' => Yii::t('standard', '_DATE_LAST_ACCESS'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idThread',$this->idThread);
		$criteria->compare('last_access',Yii::app()->localtime->fromLocalDateTime($this->last_access),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Track thread access
	 * @param $threadId
	 */
	public static function trackAccess($threadId)
	{
		$model = self::model()->findByAttributes(array(
			'idThread' => $threadId,
			'idUser' => Yii::app()->user->id,
		));
		if ($model)
		{
			$model->last_access = Yii::app()->localtime->toLocalDateTime();
			$model->save(false);
		}
		else
		{
			$model = new LearningForumTiming();
			$model->idUser = Yii::app()->user->id;
			$model->idThread = $threadId;
			$model->last_access = Yii::app()->localtime->toLocalDateTime();
			$model->save(false);
		}
	}
}