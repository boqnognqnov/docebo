<?php

/**
 * This is the model class for table "learning_tracksession".
 *
 * The followings are the available columns in table 'learning_tracksession':
 * @property integer $idEnter
 * @property integer $idCourse
 * @property integer $idUser
 * @property string $session_id
 * @property string $enterTime
 * @property integer $numOp
 * @property string $lastFunction
 * @property string $lastOp
 * @property string $lastTime
 * @property string $ip_address
 * @property integer $active
 */
class LearningTracksession extends CActiveRecord
{


	public $countSessions;
	public $sumTime;
	public $maxLastTime;
	public $month;
	public $count;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTracksession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_tracksession';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCourse, idUser, numOp, active', 'numerical', 'integerOnly'=>true),
			array('session_id', 'length', 'max'=>255),
			array('lastFunction', 'length', 'max'=>50),
			array('lastOp', 'length', 'max'=>5),
			array('ip_address', 'length', 'max'=>40),
			array('enterTime, lastTime', 'safe'),
			// The following rule is used to prevent data update for certain fields
			array('enterTime', 'unsafe', 'on' => 'update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idEnter, idCourse, idUser, session_id, enterTime, numOp, lastFunction, lastOp, lastTime, ip_address, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('enterTime medium', 'lastTime medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEnter' => 'Id Enter',
			'idCourse' => 'Id Course',
			'idUser' => 'Id User',
			'session_id' => 'Session',
			'enterTime' => 'Enter Time',
			'numOp' => 'Num Op',
			'lastFunction' => 'Last Function',
			'lastOp' => 'Last Op',
			'lastTime' => 'Last Time',
			'ip_address' => 'Ip Address',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEnter',$this->idEnter);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('session_id',$this->session_id,true);
		$criteria->compare('enterTime',Yii::app()->localtime->fromLocalDateTime($this->enterTime),true);
		$criteria->compare('numOp',$this->numOp);
		$criteria->compare('lastFunction',$this->lastFunction,true);
		$criteria->compare('lastOp',$this->lastOp,true);
		$criteria->compare('lastTime',Yii::app()->localtime->fromLocalDateTime($this->lastTime),true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Get total time spent by a user in a course
	 * @param $courseId
	 * @param $userId
	 * @return int (seconds)
	 */
	public static function getTotalTime($courseId, $userId)
	{
		$c = Yii::app()->db->createCommand();
		$totalTime = $c->select('SUM(TIME_TO_SEC(TIMEDIFF(lastTime, enterTime))) AS totalTime')
			->from(self::model()->tableName())
			->where('idCourse = :idCourse AND idUser = :idUser', array(':idCourse' => $courseId,':idUser' => $userId))
			->queryScalar();

		return $totalTime ? $totalTime : 0;

	}

	/**
	 * Return total views for course by user
	 * @param $courseId
	 * @param $userId
	 * @return int
	 */
	public static function getTotalViews($courseId, $userId)
	{
		$model = self::getAllByCourseAndUser($courseId, $userId);
		return count($model);
	}

	/**
	 * Get all records for course by user
	 * @param $courseId
	 * @param $userId
	 * @return array
	 */
	public static function getAllByCourseAndUser($courseId, $userId)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'idCourse = :idCourse AND idUser = :idUser';
		$criteria->params = array(':idCourse' => $courseId, ':idUser' => $userId);
		return self::model()->findAll($criteria);
	}

	/**
	 * Statistics of LMS logins for last X days (used in player graphs or others)
	 * @param $courseId
	 * @param null $userId
	 * @param int $days
	 * @return array
	 */
	public static function getLmsLogins($courseId, $days = 7, $userId = null) {
		if (!is_numeric($days) || $days<=0) $days = 7;
		$dateEnd = date("Y-m-d");
		$dateBegin = date("Y-m-d", strtotime("-".($days-1)." days"));
		$activity = array();

		// initializing the activity dates
		$_curDate = strtotime($dateBegin);
		do {
			$activity[date('Y-m-d', $_curDate)] = array();
			$_curDate =strtotime('+1 day', $_curDate);
		} while($_curDate<=strtotime($dateEnd));

		$command = Yii::app()->db->createCommand()
			->select('enterTime, idUser')
			->from(LearningTracksession::model()->tableName())
			->where('idCourse = :courseId AND enterTime > :dateBegin AND enterTime < DATE_ADD(:dateEnd, INTERVAL 1 DAY)', array(
				':dateBegin' => $dateBegin,
				':dateEnd' => $dateEnd,
				':courseId' => $courseId
			));

		if ($userId != null)
		{
			$command->andWhere('idUser = :userId', array(':userId' => $userId));
		}

		$trackSessionData = $command->queryAll();
		foreach ($trackSessionData as $record)
		{
			$date = substr($record['enterTime'], 0, 10);
			if (!isset($activity[$date])) $activity[$date] = array();
			if (!isset($activity[$date][$record['idUser']])) {
				$activity[$date][$record['idUser']] = 0;
			}
			$activity[$date][$record['idUser']]+=1;
		}

		if (!empty($activity)) {
			foreach ($activity as $date => $val) {
				if ($userId > 0) {
					if (empty($val)) {
						$activity[$date] = 0;
					} else {
						$userActivity = array_values($val);
						$activity[$date] = $userActivity[0];
					}
				} else {
					$activity[$date] = count($val);
				}
			}
		}
		return $activity;
	}

	public function getUserTotalTime($userId = null, $courseId = null, $datePattern = 'G\h i\m', $from_report = false, $seconds = false) {
		$criteria = new CDbCriteria();
		$criteria->select = array(
			't.idUser',
			't.idCourse',
			'COUNT(*) as countSessions',
			'SUM(UNIX_TIMESTAMP(t.lastTime) - UNIX_TIMESTAMP(t.enterTime)) as sumTime',
			'MAX(t.lastTime) as maxLastTime',
		);
		$criteria->addCondition('UNIX_TIMESTAMP(t.lastTime) > UNIX_TIMESTAMP(t.enterTime) AND t.lastTime != "0000-00-00 00:00:00" AND t.lastTime IS NOT NULL AND t.enterTime != "0000-00-00 00:00:00" AND t.enterTime IS NOT NULL');
		$criteria->params = array();
		if (!empty($userId)) {
			if(is_array($userId)) {
				$criteria->addInCondition('t.idUser', $userId);
			} else {
				$criteria->addCondition('t.idUser = :userid');
				$criteria->params[':userid'] = $userId;
			}
		}
		//TODO: That part was used only for courses - users report. Must be removed or rethinked. That was just a fast fix for making the report work correctly. Bug #74285692
		if(!empty(Yii::app()->session['consider_other_than_students']) && $from_report === true)
		{
			if(Yii::app()->session['consider_other_than_students'] != 1)
			{
				$criteria->join .= " INNER JOIN ".LearningCourseuser::model()->tableName()." lcu ON lcu.idCourse = t.idCourse AND lcu.idUser = t.idUser";
				$criteria->addCondition('lcu.level = :level');
				$criteria->params[':level'] = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
			}
		}
		if (!empty($courseId)) {
			$criteria->addCondition('t.idCourse = :courseid');
			$criteria->params[':courseid'] = $courseId;
		}

		// Power User filtering
		if (Yii::app()->user->getIsAdmin() && isset($courseId)) {
			$criteria->join .= " INNER JOIN " . CoreUserPU::model()->tableName()." pu ON (pu.puser_id = " . (int) Yii::app()->user->id . " AND (t.idUser = pu.user_id))";
			$criteria->join .= " INNER JOIN " . CoreUserPuCourse::model()->tableName()." puc ON (puc.puser_id = "  . (int) Yii::app()->user->id .   " AND (puc.course_id = t.idCourse))";
		}


		$trackSession = $this->find($criteria);

		$totalTimeSec = (!empty($trackSession->sumTime) ? $trackSession->sumTime : 0);
		
		if ($seconds) return $totalTimeSec; 
		
		$totalTime = Yii::app()->localtime->hoursAndMinutes($totalTimeSec);
		return $totalTime;
	}

	/**
	 * return an array with the total times of all users, enrolled to a course
	 * array(
	 *    id_user => total_time,
	 *    ...
	 * )
	 *
	 * @param int $courseId
	 * @return array
	 */
	public static function getTotalTimesByCourse($courseId) {
		$command = Yii::app()->db->createCommand();
		$command->select = 'cu.idUser, IFNULL(SUM(UNIX_TIMESTAMP(ts.lastTime) - UNIX_TIMESTAMP(ts.enterTime)), 0) AS sumTime';
		$command->from(LearningCourseuser::model()->tableName().' cu');

		// Power User filtering
		if (Yii::app()->user->getIsAdmin()) {
			$command->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = :userId AND cu.idUser = pu.user_id", array(':userId' => Yii::app()->user->id));
			$command->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = :userId AND puc.course_id = cu.idCourse", array(':userId' => Yii::app()->user->id));
		}
		$command->leftJoin(LearningTracksession::model()->tableName().' ts', 'cu.idCourse=ts.idCourse AND cu.idUser = ts.idUser');
		$command->andWhere('cu.idCourse = :courseid', array(':courseid' => $courseId));
		$command->group('cu.idUser');

		$results = $command->queryAll();

		$totalTimes = array();
		foreach($results as $result) {
			$totalTimes[$result['idUser']] = Yii::app()->localtime->hoursAndMinutes($result['sumTime']);
		}

		return $totalTimes;
	}

	/**
	 * Create new course session for User + Course. Deactivates previous sessions.
	 *
	 * @param number $idUser
	 * @param number $idCourse
	 * @return LearningTracksession
	 */
	public static function createSessionCourseTrack($idUser, $idCourse) {
		$courseSessionKey = 'id_course_' . (int) $idCourse;

		self::model()->updateAll(array('active' => 0), 'idCourse='. (int) $idCourse .' AND idUser='.(int) $idUser . ' AND active=1');

		$model = new self();
		$model->idCourse = $idCourse;
		$model->idUser = $idUser;
		$model->enterTime = Yii::app()->localtime->toLocalDateTime();
		$model->lastTime = Yii::app()->localtime->toLocalDateTime();
		$model->numOp = 1;
		$model->ip_address = $_SERVER['REMOTE_ADDR'];
		$model->active = 1;

		if ($model->save()) {
			Yii::app()->session['id_enter_course'] = array($courseSessionKey => $model->idEnter);
		}

		return $model;
	}


	/**
	 * Track User+Course session. Called at certain points of code.
	 *
	 * @param number $idUser
	 * @param number $idCourse
	 * @param string $functionName
	 * @param string $operation
	 * @return LearningTracksession
	 */
	public static function track($idUser, $idCourse, $functionName = '', $operation = '') {

		$courseSessionKey = 'id_course_' . (int) $idCourse;

		if (!isset(Yii::app()->session['id_enter_course'][$courseSessionKey])) {
			if (self::createSessionCourseTrack($idUser, $idCourse)) {
				return null;
			}
		}

		$idEnter = (int) Yii::app()->session['id_enter_course'][$courseSessionKey];
		$model = self::model()->findByPk($idEnter);

		// We've got session ID Enter, but couldn't find it in table ??
		// Unset this session value and create new session-track record!
		if (!$model) {
			unset(Yii::app()->session['id_enter_course'][$courseSessionKey]);
			if (self::createSessionCourseTrack($idUser, $idCourse)) {
				return null;
			}
		}

		// Something fishy here: we've got request session, we've got a model,
		// but this record has nothing to do with current user/course... Get out!
		if ($model->idUser != $idUser || $model->idCourse != $idCourse) {
			return null;
		}


		if( Settings::get('ttlSession') && (Yii::app()->localtime->diff($model->lastTime, Yii::app()->localtime->getLocalNow()) > Settings::get('ttlSession'))){
			if (self::createSessionCourseTrack($idUser, $idCourse)){
				return null;
			}
		}

		$query = "
			update ".$model->tableName()."
			set numOp = numOp + 1,
			lastFunction = :functionName,
			lastOp = :lastOp,
			lastTime = :lastTime,
			ip_address = :ip_address
			where idEnter = :idEnter";

		$params = array(
			':functionName' => addslashes($functionName),
			':lastOp' => addslashes($operation),
			':lastTime' => Yii::app()->localtime->getUTCNow(),
			':ip_address' => $_SERVER['REMOTE_ADDR'],
			':idEnter' => $idEnter
		);

		if(Yii::app()->db->createCommand($query)->execute($params))
			return self::model()->findByPk($idEnter);
		else
			return null;

		/*// Alright... lets track it
		$model->numOp++;
		$model->lastFunction = $functionName;
		$model->lastOp = $operation;
		$model->lastTime = Yii::app()->localtime->toLocalDateTime();
		$model->ip_address = $_SERVER['REMOTE_ADDR'];

		return ($model->save() ? $model : null);*/
	}

	public function getMonthsList() {
		$month = array(
			1 => Yii::t('calendar', '_JAN'),
			2 => Yii::t('calendar', '_FEB'),
			3 => Yii::t('calendar', '_MAR'),
			4 => Yii::t('calendar', '_APR'),
			5 => Yii::t('calendar', '_MAY'),
			6 => Yii::t('calendar', '_JUN'),
			7 => Yii::t('calendar', '_JUL'),
			8 => Yii::t('calendar', '_AUG'),
			9 => Yii::t('calendar', '_SEP'),
			10 => Yii::t('calendar', '_OCT'),
			11 => Yii::t('calendar', '_NOV'),
			12 => Yii::t('calendar', '_DEC'),
		);

		$month_result = array();

		for($i = (int)(date('m', strtotime(Yii::app()->localtime->getLocalNow())) + 1); $i <= 12; $i++)
			$month_result[] = $month[$i];

		for($i = 1; $i <= (int)(date('m', strtotime(Yii::app()->localtime->getLocalNow()))); $i++)
			$month_result[] = $month[$i];

		return $month_result;
	}

	public function getCourseMonthlyActivities($courseId, $checkPowerUser=false, $idPowerUser=false) {

		$criteria = new CDbCriteria();
		$criteria->select = '*, MONTH(enterTime) as month, COUNT(*) as count';


		if ($checkPowerUser) {
			if ( !$idPowerUser && Yii::app()->user->getIsAdmin()) {
				$idPowerUser = Yii::app()->user->id;
			}
		}
		if ((int) $idPowerUser > 0) {
			$criteria->join = "INNER JOIN core_user_pu cup on ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )";
			$criteria->params[':idPowerUser'] = $idPowerUser;
			$criteria->together = true;
		}

		$criteria->addCondition('idCourse = :courseId');
		$criteria->params[':courseId'] = $courseId;

		$date_begin = date('Y-m-d H:i:s', mktime(0,0,0,date('m') - 1, 1, date('Y') - 1));
		$date_end = date('Y-m-d H:i:s', mktime(23,59,59,date('m') + 1, -1, date('Y')));

		$criteria->addCondition('enterTime >= :date_begin');
		$criteria->params[':date_begin'] = $date_begin;
		$criteria->addCondition('enterTime <= :date_end');
		$criteria->params[':date_end'] = $date_end;
		$criteria->group = 'MONTH(enterTime)';
		$criteria->order = 'enterTime ASC';

		$courseActivities = $this->findAll($criteria);
		return $this->getMonthlyActivities($criteria);
	}

	public function getUserMonthlyActivities($userId) {
		$criteria = new CDbCriteria();
		$criteria->select = '*, MONTH(enterTime) as month, COUNT(*) as count';
		$criteria->addCondition('idUser = :userId');
		$criteria->params = array(
			':userId' => $userId,
		);
		$date_begin = date('Y-m-d H:i:s', mktime(0,0,0,date('m') - 1, 1, date('Y') - 1));
		$date_end = date('Y-m-d H:i:s', mktime(23,59,59,date('m') + 1, -1, date('Y')));
		$criteria->addCondition('enterTime >= :date_begin');
		$criteria->params[':date_begin'] = $date_begin;
		$criteria->addCondition('enterTime <= :date_end');
		$criteria->params[':date_end'] = $date_end;
		$criteria->group = 'MONTH(enterTime)';
		$criteria->order = 'enterTime ASC';
		return $this->getMonthlyActivities($criteria);
	}

	private function getMonthlyActivities(CDbCriteria $criteria)
	{
		$monthlyActivities = $this->findAll($criteria);
		$month_result = array();

		for($i = (int)(date('m', strtotime(Yii::app()->localtime->getLocalNow())) + 1); $i <= 12; $i++)
			$month_result[$i] = 0;
		for($i = 1; $i <= (int)(date('m', strtotime(Yii::app()->localtime->getLocalNow())) - 1); $i++)
			$month_result[$i] = 0;

		if (!empty($monthlyActivities))
			foreach ($monthlyActivities as $activity)
				$month_result[(int)($activity->month)] = (float) $activity->count;

		return array_values($month_result);
	}
}