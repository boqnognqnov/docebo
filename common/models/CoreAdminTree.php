<?php

/**
 * This is the model class for table "core_admin_tree".
 *
 * The followings are the available columns in table 'core_admin_tree':
 * @property string $idst
 * @property string $idstAdmin
 */
class CoreAdminTree extends CActiveRecord
{

	const TYPE_USER = 'user';
	const TYPE_GROUP = 'group';
	const TYPE_ORGCHART = 'orgchart';
	const TYPE_ORGCHART_DESC = 'orgchart_desc';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_admin_tree';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst, idstAdmin', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idst, idstAdmin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst' => 'Idst',
			'idstAdmin' => 'Idst Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst',$this->idst,TRUE);
		$criteria->compare('idstAdmin',$this->idstAdmin,TRUE);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreAdminTree the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}




	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}



	public $search_input = NULL;
	public $powerUserManager = NULL;

	/**
	 * Just the types constants packed in an array
	 * @return type
	 */
	public function getMemberTypes() {
		return array(
			self::TYPE_USER,
			self::TYPE_GROUP,
			self::TYPE_ORGCHART,
			self::TYPE_ORGCHART_DESC
		);
	}



	/**
	 * Retrieves all power users idsts
	 * @param numerice $idAdmin the idst of the power user
	 * @return array the list of power user's idsts
	 */
	public function getPowerUserIdsts($idAdmin) {
		$list = self::model()->findAllByAttributes(array('idstAdmin' => $idAdmin));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $record) {
				$output[] = $record['idst'];
			}
		}
		return $output;
	}



	/**
	 * Retrieves members of a power user, with type(s) specified.
	 *
	 * Note: this method does NOT return list of USERS!  It returns entities assigned to the
	 * Power User and it could be list of Users, Groups, Chart oc_* groups or ocd_* groups!
	 *
	 * @param numeric $idAdmin
	 * @param string|array $types requested members types
	 * @return array the list of retrieved members
	 */
	public static function getPowerUserMembers($idAdmin, $types, $searchFilter = false) {
		if (is_string($types)) {
			if (in_array($types, self::getMemberTypes())) {
				$types = array($types);
			} else {
				return array();
			}
		}

		if (!is_array($types)) {
			return array();
		}

		$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$output = array();
		foreach ($types as $type) {
			switch ($type) {
				case self::TYPE_USER: {
					$command = Yii::app()->db->createCommand()
						->select("at.idst, u.userid AS name")
						->from(self::model()->tableName()." at")
						->join(CoreUser::model()->tableName()." u", "at.idst = u.idst")
						->where("at.idstAdmin = :idst_admin", array(':idst_admin' => $idAdmin));
					if (!empty($searchFilter)) { $command->andWhere("u.userid LIKE :search_filter", array(':search_filter' => '%'.$searchFilter.'%')); }
					$list = $command->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst'],  	// User idst
								'id'   => (int)$item['idst'], 	// User idst  (yes, the same; for the sake of normalized output)
								'name' => $item['name'],
								'type' => self::TYPE_USER
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_GROUP: {
					$command = Yii::app()->db->createCommand()
						->select("at.idst, g.idst as idstGroup, g.groupid AS name")
						->from(self::model()->tableName()." at")
						->join(CoreGroup::model()->tableName()." g", "at.idst = g.idst AND g.hidden = :hidden", array(':hidden' => 'false'))
						->where("at.idstAdmin = :idst_admin", array(':idst_admin' => $idAdmin));
					if (!empty($searchFilter)) { $command->andWhere("g.groupid LIKE :search_filter", array(':search_filter' => '%'.$searchFilter.'%')); }
					$list = $command->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst'],  		// Group idst
								'id'   => (int)$item['idstGroup'], 	// Group idst (yes, the same; for the sake of normalized output)
								'name' => $item['name'],
								'type' => self::TYPE_GROUP
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_ORGCHART: {
					$command = Yii::app()->db->createCommand()
						->select("at.idst, oct.idOrg as idOrg, oc.translation AS name")
						->from(self::model()->tableName()." at")
						->join(CoreOrgChartTree::model()->tableName()." oct", "at.idst = oct.idst_oc")
						->join(CoreOrgChart::model()->tableName()." oc", "oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code", array(':lang_code' => $language))
						->where("at.idstAdmin = :idst_admin", array(':idst_admin' => $idAdmin));
					if (!empty($searchFilter)) { $command->andWhere("oc.translation LIKE :search_filter", array(':search_filter' => '%'.$searchFilter.'%')); }
					$list = $command->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst'],  	// Org Chart oc_ Group
								'id'   => (int)$item['idOrg'], 	// Org Chart ID
								'name' => $item['name'],
								'type' => self::TYPE_ORGCHART
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_ORGCHART_DESC: {
					$command = Yii::app()->db->createCommand()
						->select("at.idst, oct.idOrg as idOrg, oc.translation AS name")
						->from(self::model()->tableName()." at")
						->join(CoreOrgChartTree::model()->tableName()." oct", "at.idst = oct.idst_ocd")
						->join(CoreOrgChart::model()->tableName()." oc", "oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code", array(':lang_code' => $language))
						->where("at.idstAdmin = :idst_admin", array(':idst_admin' => $idAdmin));
					if (!empty($searchFilter)) { $command->andWhere("oc.translation LIKE :search_filter", array(':search_filter' => '%'.$searchFilter.'%')); }
					$list = $command->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'idst' => (int)$item['idst'],  	// Org Chart ocd_ Group
								'id'   => (int)$item['idOrg'], 	// Org Chart ID
								'name' => $item['name'],
								'type' => self::TYPE_ORGCHART_DESC
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
			}
		}

		return $output;
	}



	public static function getPowerUserUsers($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_USER));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['idst']] = $item['name'];
			}
		}
		return $output;
	}

	public static function getPowerUserGroups($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_GROUP));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['idst']] = $item['name'];
			}
		}
		return $output;
	}

	public static function getPowerUserOrgchart($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_ORGCHART));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['idst']] = $item['name'];
			}
		}
		return $output;
	}


	public static function getPowerUserOrgchartDesc($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_ORGCHART_DESC));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['idst']] = $item['name'];
			}
		}
		return $output;
	}


	public function dataProvider($idAdmin) {

		$items = self::getPowerUserMembers($idAdmin, $this->getMemberTypes(), $this->search_input);
		$config = array(
			//'sort' => array(
			//	'attributes' => array('type')
			//),
			//'pagination' => array('pageSize' => Settings::get('elements_per_page', 10))
		);
		return new CArrayDataProvider($items, $config);
	}



	/**
	 *
	 * @param number $idAdmin Power user ID
	 * @param array $list  Array of IDs of items to add (users, groups, org chart groups)
	 * @param bool $removeUnselected if entities not present in $list must be removed from power user selection (replacement) or not (strictly addition)
	 * @throws CException
	 * @return boolean
	 */
	public static function updatePowerUserSelection($idAdmin, $list, $removeUnselected = false) {

		$db = Yii::app()->db;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
			// Get existing items assigned to this Power User, to avoid adding item more than once
			$allExisting = array_merge(
				array_keys(self::getPowerUserUsers($idAdmin)),
				array_keys(self::getPowerUserGroups($idAdmin)),
				array_keys(self::getPowerUserOrgchart($idAdmin)),
				array_keys(self::getPowerUserOrgchartDesc($idAdmin))
			);


			if ($removeUnselected) {

				//detect items to be removed, to be added and to be kept in admin list
				$toKeep = array_intersect($allExisting, $list);
				$toRemove = array_diff($allExisting, $toKeep);
				$toAdd = array_diff($list, $toKeep);

				//delete items to be removed
				if (!empty($toRemove)) {
					CoreAdminTree::model()->deleteAllByAttributes(array('idstAdmin' => $idAdmin, 'idst' => $toRemove));
				}
				//add items to be added
				foreach ($toAdd as $itemToAdd) {
					$ar = new CoreAdminTree();
					$ar->idstAdmin = $idAdmin;
					$ar->idst = $itemToAdd;
					$rs = $ar->save(FALSE);
					if (!$rs) {
						throw new CException('Error while updating power user selection');
					}
				}

			} else {

				// Enumerate Incoming list
				if (!empty($list)) {
					foreach ($list as $idst) {
						if (!in_array($idst, $allExisting)) {
							$ar = new CoreAdminTree();
							$ar->idstAdmin = $idAdmin;
							$ar->idst = $idst;
							$rs = $ar->save(FALSE);
							if (!$rs) {
								throw new CException('Error while updating power user selection');
							}
						}
					}
				}

			}

			// Now update CoreUserPu table: it consists only user names
			$rs = CoreUserPU::model()->updatePowerUserSelection($idAdmin);

			if (!$rs) { throw new CException('Error while updating power user selection'); }

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return TRUE;
	}


	public static function checkPowerUserMember($idAdmin, $idMember){
		$criteria = new CDbCriteria();
		$criteria->condition = 'idst = :idst AND idstAdmin = :idstAdmin';
		$criteria->params = array(':idst' => $idMember, ':idstAdmin' => $idAdmin);
		$count = CoreAdminTree::model()->count($criteria);

		return $count;
	}

	public static function addPowerUserMember($idAdmin, $idMember, $updatePU = TRUE) {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		try {
			//if ($idAdmin != $idMember) {
				$ar = new self();
				$ar->idstAdmin = $idAdmin;
				$ar->idst = $idMember;
				if (!$ar->save()) { throw new CException('Error while saving power user member'); }
				if ($updatePU) {
					$rs = CoreUserPU::model()->updatePowerUserSelection($idAdmin);
					if (!$rs) { throw new CException('Error while update power user members'); }
				}
			//}
			if (isset($transaction)) { $transaction->commit(); }
		} catch (CException $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
		return TRUE;
	}



	/**
	 * Return list of Org Chart Node IDs where the given Power User has its own assigned users.
	 *
	 */
	public function getPowerUserOrgChartsHavingUsers($idAdmin, $includeNodeItself = false, $includeDescendants = false) {

		// Get list of power user's  Users (full list of all users assigned to PU, either directly or through OrgCharts, Groups etc)
		$userIds = CoreUserPU::getList($idAdmin);  // true = cache result

		// Get list of Org Chart Nodes (IDs) having any of the above users in it
		$idOrgIds = CoreOrgChartTree::getNodesHavingUsers($userIds);

		$result=array();
		foreach ($idOrgIds as $idOrg) {
			$node = CoreOrgChartTree::model()->findByPk($idOrg);
			$ancestors = $node->ancestors()->findAll();
			foreach ($ancestors as $ancestor) {
				$result[] = $ancestor->getPrimaryKey();
			}

			if ($includeDescendants) {
				$descendants = $node->descendants()->findAll();
				foreach ($descendants as $descendant) { $result[] = $descendant->getPrimaryKey(); }
			}

			if ($includeNodeItself) {
				$result[] = $idOrg;
			}
		}

		// Uniques IDs are required only
		$result = array_unique($result);

		return $result;

	}



	/**
	 * Returns information about all Branches assigned to a given Power User.
	 * The result is an associative array:
	 * 		array(
	 * 			'list' =>  array of Org Chard IDs, including their ancestors (if requested) and descendants (IF node is assigned as "branch and descendants)
	 *  		'leafs' => List of Org Chart IDs which, due to Power User assignment(s) are effectively leaf, even though they have childlren
	 *  	)
	 *
	 * @param integer $idAdmin  Power User ID
	 * @param boolean $includeAncestors  Return also IDs of all ancestors of assigned branches
	 * @return array
	 */
	public static function getPowerUserOrgChartsOnly($idAdmin, $includeAncestors = true) {

		// Get Info about org charts assigned to the Power User (members)
		$orgChartsMembers = self::getPowerUserMembers($idAdmin, array(self::TYPE_ORGCHART, self::TYPE_ORGCHART_DESC));

		// Since the Power User filtering is changing the tree (like parents becoming a leaf, because children are disabled/not assigned)
		// we need to track nodes that are "transformed" into leafs  ("faked" leafs, "forced" leafs)
		$leafs = array();

		// Array of models of assigned nodes
		$nodeModels = array();
		foreach ($orgChartsMembers as $info) {
			$orgChartModel = CoreOrgChartTree::model()->findByPk($info['id']);

			// Add Node itself
			$nodeModels[] = $orgChartModel;

			// Add Ancestors, if requested
			if($includeAncestors) {
				$nodeModels = array_merge($nodeModels, $orgChartModel->ancestors()->findAll());
			}

			// Decsendants: only if the branch is assigned to the user as "branch with descendants"
			if ($info['type'] == self::TYPE_ORGCHART_DESC) {
				$nodeModels = array_merge($nodeModels, $orgChartModel->descendants()->findAll());
			}
			else {
				// This node is selected as branch itself only, no descendants;
				// include it in the list of ***possible** "fake" leafs (casting shadow on descendants)
				$leafs[] = $orgChartModel;
			}

		}

		// We return list of Node IDs and list of "faked" leafs
		$result = array(
			'nodes' => array(),
			'leafs' => array(),
		);

		// Build array of idOrgs of all nodes assigned to PU
		foreach ($nodeModels as $node) {
			$result['nodes'][] = $node->idOrg;
			$result['nodeObjects'][] = $node;
		}

		// Build an array of idOrgs of "fake" leafs. We need them in building the chart tree view
		// @TODO This
		foreach ($leafs as $leaf) {
			// Before we confirm this is a "faked" leaf, we must be sure there is NO another node, below this one, that is also assigned to PU
			// This will "cancel" the "shadow" casted by the above "fake" leaf
			/*
			 * Branch 1  (V)  (assigned as "branch only", possible leaf, but see below, more nodes are assigned)
			 * 		|
			 *      +---- B1-1  (V)  (assigned as "branch and descendants"; children become visible unconditionaly)
			 *      |		|
			 *      |		+----- B1-1-1 (V, because is child of B1-1)
			 *      |		+----- B1-1-2 (V, because is child of B1-1)
			 *      |
			 *      +---- B1-2  (V)  (assigned as "branch only", possible fake leaf, but see B1-2-2)
			 *        		+----- B1-2-1 (Nv, because NOT assigned and no parent assigned as "branch and descendants)
			 *        		+----- B1-2-2 (V) (assigned, no matter how, must be visible, that means B1-2 is NOT a fake leaf)
			 *        				|
			 *        				+----- B1-2-2-1 (Nv)
			 *        				+----- B1-2-2-2 (V)  (assigned as "branch only", possible fake leaf; and it IS, because there no more children assigned)
			 *        				+----- B1-2-2-3 (Nv)
			 *  (V) 	=== Visible
			 *  (Nv) 	=== Not visible
			 *
			 */
			$desc = $leaf->descendants()->findAll();  // all nodes below the wannabe $leaf
			$found = false;
			foreach ($desc as $descNode) {
				// Is this node assigned also? If Yes, set we found one and break the loop
				if (in_array($descNode->idOrg, $result['nodes'])) {
					$found = true;
					break;
				}
			}
			// If NO, then this leaf is... really a "fake" leaf, nothing lies below it
			if (!$found) {
				$result['leafs'][] = $leaf->idOrg;
			}

		}

		// Return associative array
		return $result;

	}


	/**
	 * Return an array of Group IDs, associated to Power User THROUGH "GROUP" or "ORG CHART" (branch OR branch+desc) enitities.
	 *
	 * Note: Groups of type /ocd_  are not taken into account. We use getBranchGroups() method which merely gets all oc_ groups
	 * of a given Org Chart NODE and oc_ groups of all of its descendants (using iLeft/iRigh approach).
	 *
	 * @param number $idAdmin
	 * @param number $withDirectGroups Include also groups directly assigned to PU
	 */
	public static function getPowerUserFullListOfGroups($idAdmin, $withDirectGroups=true) {

		if ($withDirectGroups) {
			$types = array(self::TYPE_ORGCHART, self::TYPE_ORGCHART_DESC, self::TYPE_GROUP);
		}
		else {
			$types = array(self::TYPE_ORGCHART, self::TYPE_ORGCHART_DESC);
		}


		$puMembers = self::getPowerUserMembers($idAdmin, $types);
		$groups = array();
		foreach ($puMembers as $member) {
			if ($member['type'] == self::TYPE_ORGCHART) {
				$groups[] = $member['idst'];
			}
			else if ($member['type'] == self::TYPE_ORGCHART_DESC) {
				$branchGroups = CoreOrgChartTree::model()->findByPk($member['id'])->getBranchGroups();
				$groups = array_merge($groups, $branchGroups);
			}
			else if ($member['type'] == self::TYPE_GROUP) {
				$groups[] = $member['idst'];
			}
		}

		// Make them all INTEGER
		foreach ($groups as $index => $group)
			$groups[$index] = (int) $group;

		return $groups;

	}


	/**
	 * Get list of power users to whom list of users are assigned to. (reverse resolving).
	 * Also, return every PowerUser user's allocations (see returned data structure),
	 * i.e. <power user ID> => <list of user ids>
	 *
	 * @param array|boolean $usersList Set to false to get FULL list of all PU having allocated users
	 *
	 * @return array of <power-user-id> => array of <user-id>
	 */
	public static function getPowerUsersOfUsers($usersList=false) {

		static $cache = array();
		$cacheKey = sha1(json_encode(func_get_args()));

		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}

		// Filtering by EMPTY array definitely means empty result
		if ($usersList !== false && empty($usersList)) {
			$cache[$cacheKey] = array();
			return $cache[$cacheKey];
		}

		$command = Yii::app()->db->createCommand()
			->select("puser_id AS idstAdmin, GROUP_CONCAT(u2.idst) AS users_list")
			->from('core_user_pu upu')
			->join("core_user u1", "upu.puser_id = u1.idst")  // PU exists
			->join("core_user u2", "upu.user_id	 = u2.idst")  // USER exists
			->group('puser_id');

		// If there is some array, filter the result
		if ($usersList !== false) {
			$command->andWhere('upu.user_id IN (' . implode(',', $usersList) . ')');
		}

		$reader = $command->query();

		$result = array();
		foreach ($reader as  $row) {
			$result[$row['idstAdmin']] = isset($row['users_list']) ? explode(',',$row['users_list']) : array();
		}

		$cache[$cacheKey] = $result;
		return $cache[$cacheKey];

	}


	/**
	 * Retrieve power user members to be displayed in grids.
	 * @param $idAdmin the idst of the P.U.
	 * @param $searchFilter a text filter
	 * @return CSqlDataProvider
	 */
	public function dataProviderPowerUserMembers($idAdmin) {

		$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$searchFilter = (!empty($this->search_input) ? $this->search_input : false);

		$table = self::model()->tableName();
		$queries = array( 'data' => array());
		$params = array();
		$types = array_unique(self::model()->getMemberTypes());
		foreach ($types as $type) {
			switch ($type) {
				case self::TYPE_USER:
					$user_table = CoreUser::model()->tableName();
					$query = " FROM $table at "
						." JOIN $user_table u ON (at.idst = u.idst AND at.idstAdmin = :idst_admin) ";
					if (!empty($searchFilter)) { $query .= " WHERE u.userid LIKE '%$searchFilter%' "; }
					$queries['data'][] = "SELECT at.idst, u.idst AS id_specific, REPLACE(u.userid, '/', '') AS name, '$type' AS type ".$query;
					$queries['data_count'][] = "SELECT count(*), '1' ".$query;
					$params[':idst_admin'] = $idAdmin;
					break;
				case self::TYPE_GROUP:
					$group_table = CoreGroup::model()->tableName();
					$query = " FROM $table at "
						." JOIN $group_table g ON (at.idst = g.idst AND g.hidden = 'false' AND at.idstAdmin = :idst_admin) ";
					if (!empty($searchFilter)) { $query .= " WHERE g.groupid LIKE '%$searchFilter%' "; }
					$queries['data'][] = "SELECT at.idst, g.idst AS id_specific, REPLACE(g.groupid, '/', '') AS name, '$type' AS type ".$query;
					$queries['data_count'][] = "SELECT count(*), '2' ".$query;
					$params[':idst_admin'] = $idAdmin;
					break;
				case self::TYPE_ORGCHART:
					$orgchart_table = CoreOrgChartTree::model()->tableName();
					$orgtrans_table = CoreOrgChart::model()->tableName();
                    $group_members = CoreGroupMembers::model()->tableName();
					$query = " FROM $table at "
						." JOIN $orgchart_table oct ON (at.idst = oct.idst_oc AND at.idstAdmin = :idst_admin) "
						." JOIN $orgtrans_table oc ON (oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code) ";
					if (!empty($searchFilter)) { $query .= " WHERE oc.translation LIKE '%$searchFilter%' "; }
					$queries['data'][] = "SELECT at.idst, oct.idOrg as id_specific, oc.translation AS name, '$type' AS type ".$query;
					$queries['data_count'][] = "SELECT count(*), '3' ".$query;
					$params[':idst_admin'] = $idAdmin;
					$params[':lang_code'] = $language;
					break;
				case self::TYPE_ORGCHART_DESC:
					$orgchart_table = CoreOrgChartTree::model()->tableName();
					$orgtrans_table = CoreOrgChart::model()->tableName();
					$query = " FROM $table at "
						." JOIN $orgchart_table oct ON (at.idst = oct.idst_ocd AND at.idstAdmin = :idst_admin) "
						." JOIN $orgtrans_table oc ON (oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code) ";
					if (!empty($searchFilter)) { $query .= " WHERE oc.translation LIKE '%$searchFilter%' "; }
					$queries['data'][] = "SELECT at.idst, oct.idOrg as id_specific, oc.translation AS name, '$type' AS type ".$query;
					$queries['data_count'][] = "SELECT count(*), '4' ".$query;
					$params[':idst_admin'] = $idAdmin;
					$params[':lang_code'] = $language;
					break;
			}
		}

		$unionData = "(".implode(") UNION (", $queries['data']).")";
		$countUnionData = "(".implode(") UNION (", $queries['data_count']).")";

		/* @var $commandData CDbCommand */
		$commandData = Yii::app()->db->createCommand($unionData);
		$commandData->order = "name ASC";
		$commandData->params = $params;

		$commandCountData = Yii::app()->db->createCommand($countUnionData);
		$commandCountData->params = $params;

		$total_element = 0;

		$results = $commandCountData->queryColumn();
		foreach($results as $result_count)
			$total_element += $result_count;

		//data provider config
		$config = array(
			'totalItemCount' => $total_element,//$this->getPowerUserMembersNum($idAdmin),//self::model()->countByAttributes(array('idstAdmin' => $idAdmin )),
			'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
			'sort' => array(
				'attributes' => array(
					'name' => 'name',
					'type' => array(
						'asc' => 'member_type ASC, name ASC',
						'desc' => 'member_type DESC, name DESC',
					)
				),
				'defaultOrder' => 'name ASC'
			),
		);

		//create data provider and return to caller
		return new CSqlDataProvider($commandData, $config);
	}

    public function getPowerUserMembersNum($idAdmin){
        $language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$searchFilter = (!empty($this->search_input) ? $this->search_input : false);

		$table = self::model()->tableName();
		$queries = array('count' => array());
		$params = array();
		$types = array_unique(self::model()->getMemberTypes());
        //hold the members of the childern branches
        $idstOrgDesc = array();
		foreach ($types as $type) {
			switch ($type) {
				case self::TYPE_USER:
					$user_table = CoreUser::model()->tableName();
					$query = " FROM $table at "
						." JOIN $user_table u ON (at.idst = u.idst AND at.idstAdmin = :idst_admin) ";
					if (!empty($searchFilter)) { $query .= " WHERE u.userid LIKE '%$searchFilter%' "; }
					$queries['count'][] = "SELECT u.idst AS id ".$query;
					$params[':idst_admin'] = $idAdmin;
					break;
				case self::TYPE_GROUP:
					$group_table = CoreGroup::model()->tableName();
                    $groupMember_table = CoreGroupMembers::model()->tableName();
					$query = " FROM $table at "
//						." JOIN $group_table g ON (at.idst = g.idst AND g.hidden = 'false' AND at.idstAdmin = :idst_admin) "
                        ." JOIN $groupMember_table gm ON (gm.idst = at.idst) AND at.idstAdmin = '$idAdmin'";
					if (!empty($searchFilter)) { $query .= " WHERE g.groupid LIKE '%$searchFilter%' "; }
					$queries['count'][] = "SELECT gm.idstMember AS id ".$query;
					$params[':idst_admin'] = $idAdmin;
					break;
				case self::TYPE_ORGCHART:
					$orgchart_table = CoreOrgChartTree::model()->tableName();
					$orgtrans_table = CoreOrgChart::model()->tableName();
                    $group_members = CoreGroupMembers::model()->tableName();
					$query = " FROM $table at "
						." JOIN $orgchart_table oct ON (at.idst = oct.idst_oc AND at.idstAdmin = :idst_admin) "
						." JOIN $orgtrans_table oc ON (oc.id_dir = oct.idOrg AND oc.lang_code = :lang_code) "
                        ." JOIN $group_members gm ON (gm.idst = oct.idst_oc)";
					if (!empty($searchFilter)) { $query .= " WHERE oc.translation LIKE '%$searchFilter%' "; }
					$queries['count'][] = "SELECT gm.idstMember AS id ".$query;
					$params[':idst_admin'] = $idAdmin;
					$params[':lang_code'] = $language;
					break;
				case self::TYPE_ORGCHART_DESC:
                    //The case when we want to show users in children branches
					$command = Yii::app()->db->createCommand('SELECT idOrg FROM core_org_chart_tree oct JOIN core_admin_tree at ON (at.idst = oct.idst_ocd AND at.idstAdmin = :idAdmin)');
                    $command->params[':idAdmin'] = $idAdmin;
                    $idOrgs = $command->queryAll();
                    $numOrgs = count($idOrgs);
					$idstOrgDesc = array();
                    for($i = 0; $i < $numOrgs; $i++){
                        $command = Yii::app()->db->createCommand('SELECT iLeft, iRight FROM core_org_chart_tree WHERE idOrg = :org');
                        $command->params[':org'] = $idOrgs[$i]['idOrg'];
                        $data = $command->queryRow();
                        $iLeft = !empty($data['iLeft']) ? $data['iLeft'] : -1;
                        $iRight = !empty($data['iRight']) ? $data['iRight'] : -1;

                        if (($iLeft != -1) && ($iRight != -1)) {
                            // we can do the query if we have
                            $allOrgs = Yii::app()->db->createCommand('SELECT idst_oc FROM core_org_chart_tree WHERE iLeft >= ' . $iLeft . ' AND iRight <= ' . $iRight)->queryAll();

                            $allOrgs = $this->_implodeSQLDataInString($allOrgs, 'idst_oc');

                            $allOrgMembersIds = Yii::app()->db->createCommand()
                                ->select('u.idst')
                                ->from('core_user u')
                                ->join('core_group_members gm', 'gm.idstMember = u.idst ')
                                ->where('gm.idst IN (' . $allOrgs . ')')
                                ->group('u.idst')
                                ->queryColumn();
                            $idstOrgDesc = array_merge($idstOrgDesc, $allOrgMembersIds);
                        }
                    }
					break;
			}
		}

		$unionCount = "(".implode(") UNION (", $queries['count']).")";

		/* @var $commandData CDbCommand */
		$commandCount = Yii::app()->db->createCommand($unionCount);

        //get all idst from users, groups and branches!
		$idst = $commandCount->queryColumn($params);

        //get only the unique users from: users, groups, branches and children branches
        $allMembers = array_merge($idst, $idstOrgDesc);
        $uniqueMembers = array_unique($allMembers);

        return count($uniqueMembers);
    }

    private function _implodeSQLDataInString($array, $field){
        $temptData = array();
        $imploedString = "";
        if (!empty($array) && (count($array) > 0)) {
            foreach ($array as $temp) {
                if (!empty($temp[$field]))
                    $temptData[] = $temp[$field];
            }

            // prepare the org chart ids list for the query
            $imploedString = implode(', ', $temptData);
        }

        return $imploedString;
    }

}
