<?php

/**
 * This is the model class for table "lt_courseuser_session".
 *
 * The followings are the available columns in table 'lt_courseuser_session':
 * @property integer $id_session
 * @property integer $id_user
 * @property string $evaluation_text
 * @property string $evaluation_file
 * @property double $evaluation_score
 * @property string $evaluation_date
 * @property integer $evaluation_status
 * @property integer $evaluator_id
 * @property integer $attendance_hours
 * @property integer $status
 * @property string $date_subscribed
 * @property string $date_completed
 *
 * The followings are the available model relations:
 * @property CoreUser $idUser
 * @property CoreUser $learningUser
 * @property CoreUser $evaluator
 * @property LearningCourseuser $learningCourseuser
 * @property LtCourseSession $ltCourseSession
 */
class LtCourseuserSession extends CActiveRecord
{

	// User/Course subscription statuses; learning_courseuser->status
	public static $SESSION_USER_WAITING_LIST = -2;
	public static $SESSION_USER_CONFIRMED = -1;
	public static $SESSION_USER_SUBSCRIBED = 0;
	public static $SESSION_USER_BEGIN = 1;
	public static $SESSION_USER_END = 2;
	public static $SESSION_USER_SUSPEND = 3;

	protected $oldAttributes; // Stores old attributes on afterFind() so we can compare against them during save

	const EVALUATION_STATUS_FAILED = -1;
	const EVALUATION_STATUS_PASSED = 1;

	public $confirm;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lt_courseuser_session';
	}

	public function validateEvaluationScore($attribute, $params){
		$event = new DEvent($this, array('attribute' => $this->$attribute, 'params' => $params));
		Yii::app()->event->raise('OnEvaluationScoreValidate', $event);
		$label = $this->getAttributeLabel($attribute);
		if (!$event->shouldPerformAsDefault()) {
			// custom plugin behavior
			if($event->return_value == true)
				return;
			$this->addError($attribute, $label.' '.Yii::t('error', 'Invalid input data'));
		} else {
			// standard behavior - we expect only integer numbers
			if ($this->$attribute !== null && (floatval($this->$attribute) != $this->$attribute))
				$this->addError($attribute, $label.' '.Yii::t('error', 'Invalid input data'));
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_session, id_user', 'required'),
			array('id_session, id_user, evaluation_status, evaluator_id, attendance_hours, status', 'numerical', 'integerOnly' => true),
			array('evaluation_file', 'length', 'max' => 255),
			array('evaluation_score', 'validateEvaluationScore'),
			array('evaluation_text, evaluation_date, date_completed', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, id_user, evaluation_text, evaluation_file, evaluation_score, evaluation_date, evaluation_status, evaluator_id, attendance_hours, status, date_subscribed, date_completed', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'evaluator' => array(self::BELONGS_TO, 'CoreUser', 'evaluator_id'),
			'learningUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			//Corrected the learningCourseuser relation but the ltCourseSession relation must be included with it and must not use a different alias other than the default one
			'learningCourseuser' => array(self::BELONGS_TO, 'LearningCourseuser', array('id_user' => 'idUser'), 'on' => 'learningCourseuser.idCourse = ltCourseSession.course_id'),
			'ltCourseSession' => array(self::BELONGS_TO, 'LtCourseSession', 'id_session'),
		);
	}

	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('date_subscribed medium', 'date_completed medium'),
				'dateAttributes' => array('evaluation_date')
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'id_user' => 'Id User',
			'evaluation_text' => Yii::t('classroom', 'Learning performance evaluation'),
			'evaluation_file' => 'Evaluation File',
			'evaluation_score' => Yii::t('classroom', 'Learner\'s evaluation'),
			'evaluation_date' => 'Evaluation Date',
			'evaluation_status' => Yii::t('classroom','Evaluation Status'),
			'evaluator_id' => 'Evaluator',
			'attendance_hours' => Yii::t('standard', '_ATTENDANCE'),
			'status' => 'Status',
			'date_subscribed' => 'Date Subscribed',
			'date_completed' => 'Date Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('evaluation_text',$this->evaluation_text,true);
		$criteria->compare('evaluation_file',$this->evaluation_file,true);
		$criteria->compare('evaluation_score',$this->evaluation_score);
		$criteria->compare('evaluation_date',Yii::app()->localtime->fromLocalDate($this->evaluation_date),true);
		$criteria->compare('evaluation_status',$this->evaluation_status);
		$criteria->compare('evaluator_id',$this->evaluator_id);
		$criteria->compare('attendance_hours',$this->attendance_hours);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_subscribed',Yii::app()->localtime->fromLocalDateTime($this->date_subscribed),true);
		$criteria->compare('date_completed',Yii::app()->localtime->fromLocalDateTime($this->date_completed),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LtCourseuserSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Returns the array of session user statuses
	 * @return array
	 */
	public static function getStatusesArray() {
		return array(
			self::$SESSION_USER_WAITING_LIST => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_WAITING_LIST)),
			self::$SESSION_USER_CONFIRMED => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_CONFIRMED)),
			self::$SESSION_USER_SUBSCRIBED => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_SUBSCRIBED)),
			self::$SESSION_USER_BEGIN => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_BEGIN)),
			self::$SESSION_USER_END => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_END)),
			self::$SESSION_USER_SUSPEND => Yii::t("subscribe", self::getStatusLangLabel(self::$SESSION_USER_SUSPEND))
		);
	}


	/**
	 * Return the specific level language constant string
	 * @param $status
	 * @return string
	 */
	public static function getStatusLangLabel($status) {
		$languageVariable = '';
		switch ($status) {
			case self::$SESSION_USER_WAITING_LIST:
				$languageVariable = '_WAITING_USERS';
				break;
			case self::$SESSION_USER_CONFIRMED:
				$languageVariable = '_USER_STATUS_CONFIRMED';
				break;
			case self::$SESSION_USER_SUBSCRIBED:
				$languageVariable = '_USER_STATUS_SUBS';
				break;
			case self::$SESSION_USER_BEGIN:
				$languageVariable = '_USER_STATUS_BEGIN';
				break;
			case self::$SESSION_USER_END:
				$languageVariable = '_USER_STATUS_END';
				break;
			case self::$SESSION_USER_SUSPEND:
				$languageVariable = '_USER_STATUS_SUSPEND';
				break;
		}
		return $languageVariable;
	}



	/**
	 * Enable or disable auto-synchronization with learning_courseuser table after saving a new record.
	 * This feature is used to obtaing better performance when saving multiple users to the same session
	 *
	 * @var boolean
	 */
	protected $_synchronize = true;

	public function isSynchronized() { return $this->_synchronize; }
	public function enableSynchronization() { $this->_synchronize = true; }
	public function disableSynchronization() { $this->_synchronize = false; }



	public function afterFind()
	{
		// Store unchanged/original attributes so we can compare against them during AR save
		$this->oldAttributes = $this->attributes;

		return parent::afterFind();
	}



	public function beforeSave()
	{
		//manual evaluation sets the date completed as well
		if($this->status == LearningCourseuser::$COURSE_USER_END) {
			if(!$this->date_completed) {
				$this->date_completed = Yii::app()->localtime->getUTCNow();
			}
		}
		//update session status when session is being marked as "passed"
		else if ($this->evaluation_status > 0) {
			$this->status = LearningCourseuser::$COURSE_USER_END;

			if(!$this->date_completed) {
				$this->date_completed = Yii::app()->localtime->getUTCNow();
			}
		}
		else {
			$this->date_completed = '0000-00-00 00:00:00';
		}

		$sessionModel  = LtCourseSession::model()->findByPk($this->id_session);
		// if the user is a Power User make some validations for the actions that he can do with enrollments statuses.
		if (Yii::app()->user->getIsPu() && $this->scenario != 'insert' &&  ($sessionModel && LearningCourse::isCourseSelling($sessionModel->course_id))) {
			$statuses_translations = self::getStatusesArray();
			$power_user_set_status_error = false;

			// Conditions that has to be met for the Power User to change the status of the enrolled user
			$conditions = array(
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_END && $this->status != $this->oldAttributes['status']),
					'available_statuses' => array($this->oldAttributes['status'])
				),
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_SUBSCRIBED),
					'available_statuses' => array(self::$SESSION_USER_BEGIN, self::$SESSION_USER_END, self::$SESSION_USER_SUSPEND, self::$SESSION_USER_WAITING_LIST, self::$SESSION_USER_CONFIRMED, $this->oldAttributes['status'])
				),
				array(
					'cond' => ($this->oldAttributes['status'] == self::$SESSION_USER_BEGIN),
					'available_statuses' => array(self::$SESSION_USER_END/*, self::$SESSION_USER_SUSPEND*/, $this->oldAttributes['status'])
				)
			);

			// Executing the conditions
			foreach ($conditions as $condition) {
				if ($condition['cond']) {
					if (!in_array($this->status, $condition['available_statuses'])) {
						$power_user_set_status_error = true;
						break;
					}
				}
			}

			// if the conditions are not met then there is an error 'power_user_set_status_error' and return a message to the Power User:
			if ($power_user_set_status_error) {
				throw new CException(Yii::t('course_management', 'Power Users cannot change enrollment status from {status_1} to {status_2}',
					array(
						'{status_1}' => $statuses_translations[$this->oldAttributes['status']],
						'{status_2}' => $statuses_translations[$this->status]
					)
				));
			}
		}

		if ($this->getScenario() == 'handlingAfterSaveUpdate') {
			return true;
		}

		// Update Attendance hours by calculating the record in the lt_course_session_date_attendance
		$this->updateUserAttendanceHours();

		return parent::beforeSave();
	}



	public function afterSave() {

		if ($this->getScenario() == 'handlingAfterSaveUpdate') { return true; }

		if ($this->isSynchronized()) {

			//after creating the enrollment session record, create the course one
			if ($this->isNewRecord) {
				$session = $this->getSession();
				$courseEnroll = new LearningCourseuser();
				$level = ($this->_level !== NULL ? $this->_level : LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				$enrolled = $courseEnroll->subscribeUser($this->id_user, Yii::app()->user->id, $session->course_id, 0, $level);
				if (!$enrolled) {
					switch ($courseEnroll->errorMessage) {
						case LearningCourseuser::$ALREADY_SUBSCRIBED: break; //this is ok for us, go on
						default: {
							throw new CException('Error while enrolling user to course (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
						} break;
					}
				}
			}
		}


		// Trigger event to let plugins use their custom aftersave operations
		// Pass by reference, in case any of the attributes changed
		$event = new DEvent($this, array(
			'courseuserSessionModel' => &$this
		));
		Yii::app()->event->raise('OnAfterCourseuserSessionSave', $event);
		$continue = true;
		if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
			//some plugins may not need normal subscribe actions .. let them avoid those
			if (isset($event->return_value['continue']) && !$event->return_value['continue']) {
				$continue = false;
			}
		}
		// end event
		if ($continue) {
			if(($this->status == LearningCourseuser::$COURSE_USER_END) && ($this->getScenario() == 'editUser'))
			{
				$courseEnroll = $this->getCourseuser();
				if($courseEnroll->status != LearningCourseuser::$COURSE_USER_END)
				{
					$courseEnroll->status = LearningCourseuser::$COURSE_USER_END;
					$rs = $courseEnroll->save();
					if (!$rs) { throw new CException('Error while updating enrollment status'); }
				}
			}

			if($this->evaluation_status == self::EVALUATION_STATUS_FAILED){
				// A session evaluation happened now. Mark the user's
				// course level status to "In progress"
				$courseEnroll = $this->getCourseuser();
				$courseEnroll->status = LearningCourseuser::$COURSE_USER_BEGIN;
				$rs = $courseEnroll->save();
				if (!$rs) { throw new CException('Error while updating enrollment status'); }
			}

			//check on session status: if the session has been passed, the general course status has to be updated too
			if ($this->evaluation_status == self::EVALUATION_STATUS_PASSED) {
				//update course enrollment status
				$courseEnroll = $this->getCourseuser();
				$courseEnroll->status = LearningCourseuser::$COURSE_USER_END;
				$rs = $courseEnroll->save();
				if (!$rs) { throw new CException('Error while updating enrollment status'); }
			}

			// Trigger evaluation event
			if($this->evaluation_status == self::EVALUATION_STATUS_PASSED)
				Yii::app()->event->raise('StudentPassedILTSession', new DEvent($this, array('userSessionModel' => $this)));
			else if($this->evaluation_status == self::EVALUATION_STATUS_FAILED)
				Yii::app()->event->raise('StudentFailedILTSession', new DEvent($this, array('userSessionModel' => $this)));

		}

		if($this->getIsNewRecord()) {
			$this->ltCourseSession->course->updateMaxSubscriptionQuota();
		}

		$courseEnroll = $this->getCourseuser();
		if($courseEnroll->waiting == 1 && $this->status != self::$SESSION_USER_WAITING_LIST) {
			$courseEnroll->waiting = 0;
			$courseEnroll->save();
		}

		parent::afterSave();
	}

	public function afterDelete() {

	if($this->getScenario() != 'selfUnEnroll'){

		//unenroll the user from the course if he is not subscribed to any session
		$courseId = $this->ltCourseSession->course_id;
		$userId = $this->id_user;

		$subscribedToSession = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from('lt_courseuser_session cus')
			->join('lt_course_session cs', 'cs.id_session = cus.id_session AND cs.course_id = :idCourse', array(':idCourse' => $courseId))
			->where('cus.id_user = :idUser', array(':idUser' => $userId))
			->queryScalar();

		if(!$subscribedToSession)
		{
			$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $userId, 'idCourse' => $courseId));
			if($courseUserModel && ($courseUserModel->waiting !=1)){
				if($courseUserModel->unsubscribeUser($userId, $courseId)){

					// Restore PU seat (if applicable)
					if(CoreUserPU::isPuAndSeatManager() && $courseUserModel->course->selling){
						if($courseUserModel->status==LearningCourseuser::$COURSE_USER_SUBSCRIBED){
							// Seat not consumed yet. Unenrolling from course restores +1 seat
							CoreUserPU::modifyAvailableSeats($courseId, 1);
						}else{
							// Seat inside course consumed
						}
					}
				}
			}
		}
	}

		parent::afterDelete();
	}

	private function updateUserAttendanceHours()
	{
		$model = LtCourseSession::model()->findByPk($this->id_session);
		$this->attendance_hours = $model->getUserAttendance($this->id_user);
	}

	protected $_sessionModel = NULL;

	/**
	 * Get session AR object (with some cache support, for multiple retrieving)
	 *
	 * @return LtCourseSession
	 * @throws CException
	 */
	public function getSession() {
		//check if the session AR has already been cached
		if (!empty($this->_sessionModel)) { return $this->_sessionModel; }

		//if not, then retrieve it, cache it and return it
		$session = LtCourseSession::model()->findByPk($this->id_session);
		if (!$session) { throw new CException('Invalid session'); }

		$this->_sessionModel = $session;
		return $session;
	}


	protected $_cache_courseuser = NULL;
	protected $_level = NULL;

	public function getCourseuser() {
		if ($this->_cache_courseuser !== NULL) {
			//first try to read cache; if it's filled, then check validity and return cached value
			if ($this->_cache_courseuser->idUser == $this->id_user) { return $this->_cache_courseuser; }
			//if cache value is not valid, nullify it and retrieve data from DB
			$this->_cache_courseuser = NULL;
		}
		//retrieve location info from DB
		$session = $this->getSession();
		$tmp = LearningCourseuser::model()->findByAttributes(array('idUser' => $this->id_user, 'idCourse' => $session->course_id));
		if ($tmp) { $this->_cache_courseuser = $tmp; } //fill internal cache value
		return $this->_cache_courseuser;
	}

	public function getLevel() {
		if ($this->isNewRecord) {
			return $this->_level;
		}
		$cu = $this->courseuser;
		return (!empty($cu) ? $cu->level : false);
	}

	public function setLevel($newLevel) {

		$levels = LearningCourseuser::model()->getLevelsArray(array(), array(LearningCourseuser::$USER_SUBSCR_LEVEL_COACH));
		if (!isset($levels[$newLevel])) { throw new CException('Invalid specified level: '.$newLevel); }

		if ($this->isNewRecord) {

			$this->_level = $newLevel;

		} else {

			//record exists in DB: update learning_courseuser level column
			$session = $this->getSession();
			if (!$this->courseuser) {
				throw new CException('Error: unable to retrieve course enrollment info (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
			}

			$this->courseuser->level = $newLevel;
			if (!$this->courseuser->save()) {
				throw new CException('Error while saving new user level in  the course (idUser: '.$this->id_user.'; idCourse: '.$session->course_id.')');
			}
		}

		return true;
	}



	protected $_cache_evaluator = NULL;

	/**
	 * Retrieve infos about evaluator user
	 *
	 * @return boolean|/CoreUser AR of the evaluator from users table (if evaluator_id is filled, otherwise return false)
	 */
	public function getEvaluator() {

		//no evaluator has been set for this enrolled user
		if (empty($this->evaluator_id)) {
			$this->_cache_evaluator = NULL;
			return false;
		}

		//evaluator has already been cached in the object
		if (!empty($this->_cache_evaluator)) { return $this->_cache_evaluator; }

		//retrieve evaluator info, store and return it
		$evaluator = CoreUser::model()->findByPk($this->evaluator_id);
		$this->_cache_evaluator = $evaluator;
		return $evaluator;
	}



	//file management utilities

	/**
	 * The storage directory for evaluation files uploaded by evaluators
	 *
	 * @return string the files path
	 */
	public static function getFilesPath() {
		return 'doceboLms'.DIRECTORY_SEPARATOR.'classroom_evaluation';
	}


	/**
	 * Given a file name, render it unique in order to store in evaluation files
	 * directory, since uploaded files may have the same name.
	 *
	 * @param string $originalFileName the original file name to be rendered unique
	 * @return string the new file name
	 */
	public function createUniqueFileName($originalFileName) {
		//create an unique ID based on current time and record info
		$uniquePrefix = $this->id_session.'_'.$this->id_user.'_'.time().'_'.mt_rand(0, 99999999);

		//just put the unique ID as a prefix in the file name
		return $uniquePrefix.'.'.$originalFileName;
	}


	/**
	 * Given an evaluation record, remove unique prefix from evaluation file name
	 * and return the original file name. Since evaluation files are optional, the
	 * field "evaluation_file" may be empty: in this case just return FALSE.
	 *
	 * @return string|boolean the original file name (or false, if no file has been uploaded)
	 */
	public function getOriginalFileName() {

		//no file has been uploaded for this evaluation
		if (empty($this->evaluation_file)) return false;

		//remove unique prefix from file name and return the original file name
		$arr = explode(".", $this->evaluation_file);
		array_shift($arr);
		return implode(".", $arr);
	}

    /**
     * Returns true or false if there is an evaluation file
     * @return bool
     */
    public function hasEvaluationFile() {
        return !empty($this->evaluation_file);
    }

	//other functions

    /**
     * Returns true if user is subscribed to a session
     * @param $userId
     * @param $sessionId
     * @return bool
     */
    public static function isSubscribed ($userId, $sessionId) {
        $count = self::model()->countByAttributes(array("id_user" => $userId, "id_session" => $sessionId));
        return $count > 0 ? TRUE : FALSE;
    }

	/**
	 * Enrolls a user to a course session
	 * @param $userId
	 * @param $sessionId
	 * @param $status
	 * @return bool|LtCourseuserSession
	 */
	public static function enrollUser($userId, $sessionId, $status = false, $is_cart_presubscription = false) {
        // Check if user is already subscribed
        if (LtCourseuserSession::isSubscribed($userId, $sessionId)) {
            Yii::log("User ($userId) is already subscribed to this session ($sessionId)", 'warning', __CLASS__);
            return false;
        }

		$model = new LtCourseuserSession();
		$model->id_user = $userId;
		$model->id_session = $sessionId;
        $model->date_subscribed = Yii::app()->localtime->toLocalDateTime();
        $model->status = ($status===false) ? self::$SESSION_USER_SUBSCRIBED : $status;

		if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
			$model->status = self::$SESSION_USER_WAITING_LIST;

		if($model->save() && !$is_cart_presubscription){
			if ($model->status == self::$SESSION_USER_WAITING_LIST){
				Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_WAITING_FOR_APPROVAL,
					new DEvent(new self(), array(
						'session_id' => $model->id_session,
						'user' => $userId,
					)));
			} else {
				Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION,
					new DEvent(new self(), array(
						'session_id' => $model->id_session,
						'user' => $userId,
					)));
			}
			return $model;
		}

        return false;
	}

    /**
     * Unsubscribes user from session
     * @param $userId
     * @param $sessionId
     * @return int
     */
    public static function unsubscribeUser($userId, $sessionId) {
        return self::model()->deleteByPk(array(
            'id_user' => $userId,
            'id_session' => $sessionId
        ));
    }

    /**
     * The method is called after the course purchase has been confirmed.
     * It will enroll a user to a classroom type course session with the waiting status.
     */
    public function activateSession($idUser, $idSession) {

        // find the session
        $cus = LtCourseuserSession::model()->findByAttributes(array(
            'id_session' => $idSession,
            'id_user' => $idUser
        ));
        if (!$cus)
            return false;

		if($cus->status == self::$SESSION_USER_WAITING_LIST)
			$cus->status = self::$SESSION_USER_SUBSCRIBED;

        $result = $cus->save();

        if ($result) {
            Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED, new DEvent($this, array(
                'session_id'    => $idSession,
                'user'          => $idUser,
            )));
        }

        return $result;
    }

	/**
	 * To be used in ClassroomApp/Session/Select
	 * @param $userId
	 * @param $courseId
	 * @return CActiveDataProvider
	 */
	public function sessionSelectDataProvider($userId, $courseId) {
		$criteria = new CDbCriteria();
		$criteria->with = array(
				'ltCourseSession' => array(
						'alias' => 'cs',
						'condition' => 'cs.course_id = :course_id'
				)
		);

		$criteria->addInCondition('id_user', array($userId));

		$criteria->params[':course_id'] = $courseId;

		$config = array(
				'criteria' => $criteria,
				'sort' => array(
						'defaultOrder' => 'cs.date_begin ASC'
				)
		);

		return new CActiveDataProvider($this, $config);
	}



	/**
     * The student just selected a session from a list of session
     * @param $sessionId
     * @param $userId
     * @return bool
     */
    public static function continueSession($sessionId, $userId) {
        /* @var $model LtCourseuserSession */
        $model = self::model()->findByAttributes(array(
            'id_session' => $sessionId,
            'id_user' => $userId
        ));
        if (!$model)
            return false;

        // start if not started
        if ($model->status == self::$SESSION_USER_SUBSCRIBED) {
            $model->status = self::$SESSION_USER_BEGIN;
            $model->date_subscribed = Yii::app()->localtime->toLocalDateTime();
            return $model->save();
        }

        return true;
    }





	/**
	 * Set an users evaluation, with all related info (score, text, etc.)
	 *
	 * @param int $status if the user has passed the class session or not. default value = 0 = in progress
	 * @param float $score score obtained by the user
	 * @param int $evaluator IDST of the instructor evaluating the user
	 * @param string $text evaluation notes by instructor
	 * @param string $file additional file by instructor (just the file name, assume it has already been placed in storage folder)
	 * @return boolean operation successful or not
	 * @throws CException
	 */
	public function setEvaluation($status, $score, $evaluator, $text = '', $file = '') {

		//retrieve related classroom session AR
		$sessionModel = $this->getSession(); /* @var $sessionModel LtCourseSession */

		//check score value validity
		if ($score < 0)
			throw new CException('Invalid score value');

		//set values
		$status = intval($status);
		if (!in_array($status, array(
			self::EVALUATION_STATUS_PASSED,
			self::EVALUATION_STATUS_FAILED
		))) {
			$status = 0;
		}
		$this->evaluation_status = $status;
		$this->evaluation_score = $score;
		$this->evaluator_id = $evaluator;
		$this->evaluation_date = Yii::app()->localtime->toLocalDateTime();
		if($status == self::EVALUATION_STATUS_PASSED){
			foreach($sessionModel->getDates() as $date){
				if(!LtCourseSessionDateAttendance::model()->findByAttributes(array('id_user'=>$this->id_user, 'id_session'=>$this->id_session, 'day'=>$date->day))){
					$attendanceModel = new LtCourseSessionDateAttendance();
					$attendanceModel->id_user = $this->id_user;
					$attendanceModel->id_session =  $this->id_session;
					$attendanceModel->day = $date->day;
					$attendanceModel->save();
				}
			}
		}


		if($this->evaluation_status == LtCourseuserSession::EVALUATION_STATUS_FAILED)
			$this->status = LtCourseuserSession::$SESSION_USER_BEGIN;

		//validate text
		if ($text) {
			$trimmedText = Yii::app()->htmlpurifier->purify(trim($text));
			if ($trimmedText != "") { $this->evaluation_text = $text; }
		}

		//validate file
		$removeOldEvaluationFile = false;
		$oldEvaluationFile = $this->evaluation_file;
		if (empty($file)) {
			//if we had an old evaluation file, remove it
			if (!empty($this->evaluation_file)) {
				$removeOldEvaluationFile = true;
			}
			//no evaluation file
			$this->evaluation_file = NULL;
		} else {
			//check if we are setting a new evaluation file
			if (!empty($this->evaluation_file) && $this->evaluation_file != $file) {
				//an old evaluation file which is different from new one is present: remove it
				$removeOldEvaluationFile = true;
			}
			$this->evaluation_file = $file;
		}

		//physical DB operation
		$rs = $this->save();

		if($rs)
		{
			$courseEnroll = LearningCourseuser::model()->findByAttributes(array('idUser' => $this->id_user, 'idCourse' => $sessionModel->course_id));
            // Updating enrollments score if the score value is not forced (manually set for the enrollment by superadmin / PU)
            if (((boolean) $courseEnroll->forced_score_given) !== true) {
                $courseEnroll->score_given = $this->evaluation_score;
            }
			$rs = $courseEnroll->save();
		}

		//do physical file removing, if needed (only if DB operation was successful)
		if ($rs && $removeOldEvaluationFile && !empty($oldEvaluationFile)) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
			//$rs = $storageManager->remove($oldEvaluationFile);
			$rs = $storageManager->remove($this->getOriginalFileName($oldEvaluationFile));
		}

		if($this->evaluation_status == LtCourseuserSession::EVALUATION_STATUS_FAILED)
		{
				$courseEnroll = $this->getCourseuser();
				$courseEnroll->status = LearningCourseuser::$COURSE_USER_BEGIN;
				$courseEnroll->date_complete = NULL;
				$rs = $courseEnroll->save();
				if (!$rs) { throw new CException('Error while updating enrollment status'); }
		}

		// Trigger event to let plugins execute their custom evaluation options
		$event = new DEvent($this, array (
			'courseuserSessionModel' => &$this,
			'idUser' => $this->id_user,
			'idSession' => $this->id_session
		));
		Yii::app()->event->raise('OnAfterClassroomUserSetEvaluation', $event);
		// end event

		return $rs;
	}


	/**
	 * Clear user's evaluation info
	 *
	 * @return boolean
	 */
	public function clearEvaluation() {

		$this->evaluation_score = NULL;
		$this->evaluation_date = NULL;
		$this->evaluation_text = NULL;
		$this->evaluator_id = NULL;
		$this->evaluation_status = NULL;
		$this->status = 0;

		$fileName = $this->evaluation_file; //postpone file deletion, first ensure  that the DB deletion is ok
		$this->evaluation_file = NULL;

		$rs = $this->save();
		if ($rs) {
			//database information has been cleared, now delete physical evaluation file, if present
			if (!empty($fileName)) {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
				$rs = $storageManager->remove($fileName);
			}

			$event = new DEvent($this, array());
			Yii::app()->event->raise('OnBeforeClearSessionDateAttendance', $event);
			if ($event->shouldPerformAsDefault()) {
				$attendanceModels = LtCourseSessionDateAttendance::model()->findAllByAttributes(array(
					'id_session' => $this->id_session,
					'id_user' => $this->id_user
				));

				foreach($attendanceModels as $attnModel){
					$attnModel->delete();
				}
			}

			$courseEnroll = $this->getCourseuser();
			$courseEnroll->status = LearningCourseuser::$COURSE_USER_BEGIN;
			$courseEnroll->date_complete = NULL;
			$res = $courseEnroll->save();
			if (!$res) { throw new CException('Error while updating enrollment status'); }

			// Trigger event to let plugins execute their custom evaluation options
			$event = new DEvent($this, array ('idUser' => $this->id_user, 'idSession' => $this->id_session));
			Yii::app()->event->raise('OnAfterClassroomUserClearEvaluation', $event);
			// end event
		}

		return $rs;
	}



	/**
	 * Return a list of instructor(s) for this course session
	 *
	 * @return array list of CoreUser ARs of instructors for this record's user
	 */
	public function findInstructors() {
/*
		//list of session from which search for instructor
		$checkList = array($this->id_session);
*/
		//retrieve actual session info
		$session = $this->getSession();
/*
		//check if the user is enrolled to other sessions of the course
		$others = Yii::app()->db->createCommand()
			->select("cus.id_session")
			->from(self::model()->tableName()." cus")
			->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
			->where("cs.course_id = :id_course", array(':id_course' => $session->course_id))
			->andWhere("cus.id_session <> :id_session", array(':id_session' => $this->id_session))
			->queryAll();
		if (!empty($others)) {
			foreach ($others as $other) { $checkList[] = $other['id_session']; }
		}
*/
		//now retrieve IDSTs of the instructors
		$instructorsIdsts = array();
		$idsts = Yii::app()->db->createCommand()
			->select("cus.id_user")
			->from(self::model()->tableName()." cus")
			->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
			->where("cs.course_id = :id_course", array(':id_course' => $session->course_id))
			->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			//->andWhere("cus.id_user <> :id_user", array(':id_user' => $this->id_user))
			->andWhere("cus.id_session = :id_session", array(':id_session' => $this->id_session))
			->queryAll();
		if (!empty($idsts)) {
			foreach ($idsts as $idst) {
				$instructorsIdsts[] = $idst['id_user'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		//find user records from instructors idsts
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst', $instructorsIdsts);
		$criteria->order = "firstname ASC, lastname ASC";
		return CoreUser::model()->findAll($criteria);
	}



	/**
	 * Generic method for instructor list retrieving
	 *
	 * @param int $idSession
	 * @return array
	 * @throws CException
	 */
	public static function findSessionInstructors($idSession) {

		//retrieve session info
		$session = LtCourseSession::model()->findByPk($idSession);
		if (!$session) { throw new CException('Invalid specified session'); }

		//now retrieve IDSTs of the instructors
		$instructorsIdsts = array();
		$idsts = Yii::app()->db->createCommand()
			->select("cus.id_user")
			->from(self::model()->tableName()." cus")
			->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
			->where("cs.course_id = :id_course", array(':id_course' => $session->course_id))
			->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			->andWhere("cus.id_session = :id_session", array(':id_session' => $idSession))
			->queryAll();
		if (!empty($idsts)) {
			foreach ($idsts as $idst) {
				$instructorsIdsts[] = $idst['id_user'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		//find user records from instructors idsts
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst', $instructorsIdsts);
		$criteria->order = "firstname ASC, lastname ASC";
		return CoreUser::model()->findAll($criteria);
	}



	public function dataProviderClassroomCoursesActivity($idUser) {
		$criteria = new CDbCriteria;

		$criteria->with = array(
			'ltCourseSession' => array(
				'alias' => 'session',
				'joinType' => 'INNER JOIN',
				'with' => array(
					'learningCourse' => array(
						'alias' => 'course'
					)
				)
			)
		);

		$curentUserId = Yii::app()->user->getId();
		if (Yii::app()->user->getIsPu() && (!isset($idUser) || $curentUserId != $idUser)) {
			$puManagedCourses = CoreUserPuCourse::model()->getList($curentUserId);
			$criteria->addInCondition('course.idCourse', $puManagedCourses);
		}

		$criteria->compare('t.id_user', $idUser);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'date_subscribed desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


		return new CActiveDataProvider($this, $config);
	}

	public function renderSessionScore()
	{
		$output = '';
        $hasScore = false;
        $score = 0;
        $maxScore = 0;

		if ($this->ltCourseSession->evaluation_type == LtCourseSession::EVALUATION_TYPE_ONLINE) {
            //get the score of the last completed test
            $testIds = Yii::app()->getDb()->createCommand()
                ->select('idOrg')
                ->from(LearningOrganization::model()->tableName())
                ->where('idCourse=:idCourse', array(':idCourse' => $this->ltCourseSession->course->idCourse))
                ->andWhere('objectType = :objectType', array('objectType' => LearningOrganization::OBJECT_TYPE_TEST))
                ->queryColumn();

            $criteria = new CDbCriteria();
            $criteria->addCondition('(idUser=:idUser)');
            $criteria->params = array(':idUser' => $this->id_user);
            $criteria->addInCondition('idReference', $testIds);
            $criteria->order = 'date_attempt DESC';
            $criteria->with = 'test';
            $tracking = LearningTesttrack::model()->find($criteria);
            if ($tracking) {
                $hasScore = true;
                $testModel = $tracking->test;
                $score = ($tracking->score + $tracking->bonus_score);

                if ($testModel) {
                    $maxScore = $testModel->getMaxScore($this->id_user, false, false);
                }
            }

            $output .= $hasScore ? number_format($score, 2) . '/' . $maxScore : '-';
		} else {
			//show session standard evaluation score
			$output .= floatval($this->evaluation_score) . "/" . intval($this->ltCourseSession->score_base);
		}
		return $output;
	}

	public function renderCourseLink($name)
	{
		$learningCourseuser = LearningCourseuser::model()->findByPk(array(
			'idCourse' => (int)$this->session->course->idCourse,
			'idUser' => (int)$this->id_user,
			'edition_id' => 0));

		if(!$learningCourseuser)
			return $name;

		$link = '<a href="../lms/index.php?r=player&course_id='.$this->session->course->idCourse.'">'.$name.'</a>';
		$canEnterCourse = $learningCourseuser->canEnterCourse();

		return ($canEnterCourse['can'] ? $link : $name);
	}

	public function renderEvaluationStatus() {
		switch ($this->evaluation_status) {
			case self::EVALUATION_STATUS_PASSED:
				return Yii::t('standard', 'passed');
			case self::EVALUATION_STATUS_FAILED:
				return Yii::t('standard', 'failed');
			default:
				return Yii::t('standard', '_USER_STATUS_BEGIN');
		}
	}

    public function dataProviderUserStatistics($idSession) {
            $criteria = new CDbCriteria;

            $criteria->with = array(
                    'ltCourseSession',
                    'learningUser',
                    'learningCourseuser'
            );

            $criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

            $sessionModel = LtCourseSession::model()->findByPk($idSession);
            $pUserRights = Yii::app()->user->checkPURights($sessionModel->course_id);
            if ($pUserRights->isPu && !$pUserRights->isInstructor) {
                    $managedUserIds = CoreUserPU::getList();
                    $criteria->addInCondition('t.id_user', $managedUserIds);
            }

            $criteria->addCondition('t.id_session = :id_session');
            $criteria->params[':id_session'] = $idSession;

            $sortAttributes = array();
            foreach ($this->attributeNames() as $attributeName) {
                    $sortAttributes[$attributeName] = 't.'.$attributeName;
            }

            $config['criteria'] = $criteria;
            $config['sort']['attributes'] = $sortAttributes;
            //$config['sort']['defaultOrder'] = 'id asc';
            $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


            return new CActiveDataProvider($this, $config);
    }


	/**
	 * Used to display the session statistics for all users enrolled in all sessions of a classroom course
	 *
	 * @return CActiveDataProvider
	 */
	public function dataProviderCourseStatistics($idCourse)
	{

		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
			'ltCourseSession',
			'learningUser',
			'learningCourseuser'
		);

		$criteria->addCondition('learningCourseuser.level = ' . LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

		$criteria->addCondition('ltCourseSession.course_id = :course_id');
		$criteria->params[':course_id'] = $idCourse;

                    $pUserRights = Yii::app()->user->checkPURights($idCourse);
                    if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('learningUser.idst', $puManagedUsers);
		}

		if(Yii::app()->user->getIsPu()) {
			if(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add')) {
				if(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign')) {
					$criteria->addCondition('ltCourseSession.created_by = :createdBy');
					$criteria->params[':createdBy'] = Yii::app()->user->id;
				}
			}
		}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($idCourse);
			if(!empty($instructorUsers))
				$criteria->addInCondition('learningUser.idst', $instructorUsers);
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 'ltCourseSession.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'learningUser.userid asc, ltCourseSession.name asc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}


	/**
	 * Returns an array containing the number and the percentage of the students who passed the course
	 *
	 * @param $courseId int
	 * @return array
	 */
	public function getPassStats($courseId)
	{
		$sessionIds = array();
		if(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') && (!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign'))) {
			$sessions = LtCourseSession::model()->findAllByAttributes(array('course_id' => $courseId, 'created_by' => Yii::app()->user->id));
		} else {
			$sessions = LtCourseSession::model()->findAllByAttributes(array('course_id' => $courseId));
		}

		foreach($sessions as $session)
			$sessionIds[] = $session->id_session;

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id_session', $sessionIds);
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---

                    $pUserRights = Yii::app()->user->checkPURights($courseId);
                    if ($pUserRights->isPu && !$pUserRights->isInstructor)
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $courseId, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($courseId);
			if(!empty($instructorUsers))
				$criteria->addInCondition('t.id_user', $instructorUsers);
		}
		//---
		$enrolledAllSessions = LtCourseuserSession::model()->with('ltCourseSession', 'learningCourseuser')->findAll($criteria);
		$enrolled = CHtml::listData($enrolledAllSessions, 'id_user', 'id_user');
		$totalEnrolled = count($enrolled);

		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id_session', $sessionIds);
		$criteria->addCondition('t.evaluation_status = :evaluation_status');
		$criteria->params[':evaluation_status'] = LtCourseuserSession::EVALUATION_STATUS_PASSED;
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---
		if (Yii::app()->user->getIsPu())
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//---
		$passedUsers = LtCourseuserSession::model()->with('ltCourseSession', 'learningCourseuser')->findAll($criteria);
		$passed = CHtml::listData($passedUsers, 'id_user', 'id_user');
		$countPassed = count($passed);

		if (0 == $totalEnrolled)
			$percentPassed = 0;
		else
			$percentPassed = number_format(doubleval($countPassed / $totalEnrolled) * 100, 1);

		return array(
			'total' => $totalEnrolled,
			'count' => $countPassed,
			'percent' => $percentPassed
		);
	}



	/**
	 * Retrieve all other user session enrollments for this course
	 *
	 * @return array
	 */
	public function getUserOtherSessionEnrollments() {

		$session = $this->getSession();

		$criteria = new CDbCriteria();

		$criteria->join = "JOIN ".LtCourseSession::model()->tableName()." cs ON (cs.id_session = cus.id_session)";
		$criteria->alias = "cus";

		$criteria->addCondition("cus.id_user = :id_user");
		$criteria->params[':id_user'] = $this->id_user;

		$criteria->addCondition("cus.id_session <> :id_session");
		$criteria->params[':id_session'] = $this->id_session;

		$criteria->addCondition("cs.course_id = :id_course");
		$criteria->params[':id_course'] = $session->course_id;

		return LtCourseuserSession::model()->findAll($criteria);
	}

	/**
	 * Check if a given session failed
	 * @param $idSession
	 * @param $idUser
	 * @return bool
	 */
	public static function isSessionFailedByUser($idSession, $idUser)
	{
		return self::model()->exists(array(
			'condition' => 'id_session = :id_session AND id_user = :id_user AND evaluation_status = :status',
			'params' => array(':id_session' => $idSession, ':id_user' => $idUser, ':status' => self::EVALUATION_STATUS_FAILED),
		));
	}
}
