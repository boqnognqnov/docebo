<?php

/**
 * This is the model class for table "core_notification_translation".
 *
 * The followings are the available columns in table 'core_notification_translation':
 * @property integer $notification_id
 * @property integer $transport_id
 * @property string $language
 * @property string $subject
 * @property string $message
 *
 * The followings are the available model relations:
 * @property CoreNotification $id0
 */
class CoreNotificationTranslation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreNotificationTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_notification_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notification_id, transport_id, language', 'required'),
			array('notification_id', 'length', 'max'=>20),
			array('transport_id', 'length', 'max'=>11),
			array('language', 'length', 'max'=>32),
			array('subject', 'length', 'max'=>512),
			array('message', 'length', 'max'=>10240),
				// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('notification_id, transport_id, language, subject, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notification' => array(self::BELONGS_TO, 'CoreNotification', 'notification_id'),
			'transport' => array(self::BELONGS_TO, 'CoreNotificationTransport', 'transport_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'notification_id' => 'Notification',
			'transport_id' => 'Transport',
			'language' => 'Language short code',
			'subject' => 'Subject',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('notification_id',$this->notification_id,true);
		$criteria->compare('transport_id',$this->transport_id,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}