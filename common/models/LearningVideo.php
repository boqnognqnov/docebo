<?php

/**
 * This is the model class for table "learning_video".
 *
 * The followings are the available columns in table 'learning_video':
 * @property integer $id_video
 * @property string $title
 * @property string $description
 * @property string $video_formats
 * @property integer $seekable
 * @property string $type
 *
 * The followings are the available model relations:
 * @property LearningVideoSubtitles[] $subtitles
 */
class LearningVideo extends CActiveRecord
{

	const VIDEO_TYPE_UPLOAD = 'upload';
	const VIDEO_TYPE_YOUTUBE = 'youtube';
	const VIDEO_TYPE_VIMEO = 'vimeo';
	const VIDEO_TYPE_WISTIA = 'wistia';

	const VIDEO_SEEKABLE_NO = 0;
	const VIDEO_SEEKABLE_YES = 1;
	const VIDEO_SEEKABLE_AFTER_COMPLETE = 2;

	public $url;
	public $videoId;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, video_formats', 'required'),
			array('title', 'length', 'max'=>255),
			array('type', 'length', 'max' => 32),
		    array('seekable', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_video, title, description, video_formats, seekable, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subtitles' => array(self::HAS_MANY, 'LearningVideoSubtitles', 'id_video'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_video'      => 'Id Video',
			'title'         => 'Title',
			'description'   => 'Description',
			'video_formats' => 'Video Formats',
            'seekable'      => 'Seekable',
			'type' 			=> 'Type'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_video',$this->id_video);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('video_formats',$this->video_formats,true);
        $criteria->compare('seekable',$this->seekable);
		$criteria->compare('type', $this->type);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	/**
	 * Called when "delete()" functiont is called from an AR instance of this model; used for file deleting
	 */
	public function beforeDelete() {
		//delete tracking information
		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }
		try {
			$orgs = LearningOrganization::model()->findAllByAttributes(array(
				'idResource' => $this->getPrimaryKey(),
				'objectType' => LearningOrganization::OBJECT_TYPE_VIDEO
			));

			foreach($orgs as $org) {
				$rs = LearningVideoTrack::model()->deleteAllByAttributes(array('idReference' => $org->getPrimaryKey()));
				if ($rs === false)
					throw new CException('Error while deleting track data');

				$rs = CommonTracker::delete($org->getPrimaryKey());
				if ($rs === false)
					throw new CException('Error while deleting common track data');
			}
			if (isset($transaction)) { $transaction->commit(); }
		} catch(CException $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e; //pass the exception to the above level
		}
		//delete video files from storage
		if (!$this->isVideoFileReferencedElsewhere()) {
		    $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
		    $videoFiles = CJSON::decode($this->video_formats);
		    if (!empty($videoFiles)) {
		        foreach ($videoFiles as $type => $file) {
		            if (!empty($file)) {

		                if ($type === 'mp4') {
		                    $rs = $storage->remove($file);
		                }
		                else if ($type === 'hls_id') {
		                    $storage->removeFolder($file);
		                }
		            }
		        }
		    }
		}
		return parent::beforeDelete();
	}



	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {
		$copy = new LearningVideo();
		$copy->title = $this->title;
		$copy->description = $this->description;
		$copy->video_formats = $this->video_formats;
		$copy->seekable = $this->seekable;

		if ($copy->save()) {
			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.print_r($copy->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}

	/**
	 * Check if filename.mp4 or HLS ID of THIS video object is referenced by some other object.
	 * Such scenario is possible when A course is copied together with training materials
	 * (where we do NOT copy video files but onlt LO meta data).
	 */
	public function isVideoFileReferencedElsewhere() {

	    $formats = CJSON::decode($this->video_formats);

	    if (isset($formats['mp4']) && $formats['mp4']) {
	        $referenced = LearningVideo::model()->count('(video_formats LIKE :value) AND (id_video <> :thisId)', array(
	            ':value'   => '%' . $formats['mp4'] . '%',
	            ':thisId'  => $this->id_video,
	        ));
	        if ((int) $referenced > 0) return true;
	    }

	    if (isset($formats['hls_id']) && $formats['hls_id']) {
	        $referenced = LearningVideo::model()->count('(video_formats LIKE :value) AND (id_video <> :thisId)', array(
	            ':value'   => '%' . $formats['hls_id'] . '%',
	            ':thisId'  => $this->id_video,
	        ));
	        if ((int) $referenced > 0) return true;
	    }

	    return false;

	}

	public function afterFind(){
		$formats = CJSON::decode($this->video_formats);

		if(isset($formats['url'])){
			$this->url = $formats['url'];
		}

		if(isset($formats['id'])){
			$this->videoId = $formats['id'];
		}
		return parent::afterFind();
	}

	/**
	 * If the video is of type upload return the hash for Amazon S3
	 *
	 * @return bool
	 */
	public function getVideoHash(){
		$formats = CJSON::decode($this->video_formats);
		$response = ($this->type === self::VIDEO_TYPE_UPLOAD ? reset($formats) : false);

		return $response;
	}


}