<?php

/**
 * This is the model class for table "equivalent_courses".
 *
 * The followings are the available columns in table 'equivalent_courses':
 * @property integer $id
 * @property integer $source_course_id
 * @property integer $target_course_id
 */
class EquivalentCourse extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'equivalent_courses';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EcommerceTransactionInfo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, target_course_id, source_course_id', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, target_course_id, source_course_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @param LearningCourse $sourceCourseModel
     * @param array $equivalentCoursesIds - list of desired courses ids, for which the info should return
     * @return CArrayDataProvider
     */
    public static function dataProvider(LearningCourse $sourceCourseModel){
        $sourceCourseId = $sourceCourseModel->idCourse;
        $equivalentCoursesIds = $sourceCourseModel->getEquivalentCoursesIds();
        $toBeDeletedCourses = $_SESSION['selectedEquivalentCoursesIdsForDeletion'];
        $toBeAddedCourses = $_SESSION['selectedEquivalentCoursesIds'];
        $command = Yii::app()->db->createCommand();
        $sessionDataCommand = clone $command;
        $selectStatement= 'lc.idCourse, lc.code, lc.name, lc.course_type as type, IF(eqc.bidirectional, eqc.bidirectional, 0) as bidirectional';

        // get data from db, decreased with courses, selected to be removed
        $command->select($selectStatement)
            ->from(LearningCourse::model()->tableName() . ' lc')
            ->join(EquivalentCourse::model()->tableName() . ' eqc',
                'eqc.target_course_id = lc.idCourse AND eqc.source_course_id=:source',
                array(':source' => $sourceCourseId))
            ->andWhere(array('IN', 'eqc.target_course_id', $equivalentCoursesIds));
        if(!empty($toBeDeletedCourses))
            $command->andWhere(array('NOT IN', 'lc.idCourse', $toBeDeletedCourses));


        if (!empty($toBeAddedCourses)) {
            $sessionDataCommand->select($selectStatement)
                ->from(LearningCourse::model()->tableName() . ' lc')
                ->leftJoin(EquivalentCourse::model()->tableName() . ' eqc',
                    'eqc.target_course_id = lc.idCourse');
            $sessionDataCommand->where(array('IN', 'lc.idCourse', $toBeAddedCourses));
            $command->union($sessionDataCommand->getText());
        }

        $dataArray = $command->queryAll();
        $config = array(
            'pagination' => array('pageSize' => Settings::get('elements_per_page', 10))
        );
        $dataProvider = new CArrayDataProvider($dataArray, $config);
        return $dataProvider;
    }

    public static function synchronizeEquivalents(){
        // get all relations
        $relations = Yii::app()->db->createCommand()
            ->select()->from(self::model()->tableName())->queryAll();
        $count = count($relations);

        // clear all flags
        Yii::app()->db->createCommand()
            ->update(self::model()->tableName(), array('bidirectional' => 0));
        for ($i = 0; $i < $count; $i++) {
            for ($j = $i + 1; $j < $count; $j++) {
                if($relations[$i]['source_course_id'] == $relations[$j]['target_course_id']
                && $relations[$i]['target_course_id'] == $relations[$j]['source_course_id']){
                    // update those relation to bidirectional = 1
                    Yii::app()->db->createCommand()
                        ->update(self::model()->tableName(), array('bidirectional' => 1),
                            '(source_course_id=:source AND target_course_id=:target) OR (source_course_id=:target AND target_course_id=:source)',
                            array(
                                ':source' => $relations[$i]['source_course_id'],
                                ':target' => $relations[$i]['target_course_id']
                            ));
                }
            }
        }
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
//    public function attributeLabels()
//    {
//        return array(
//            'target_course_id' => 'Id Trans',
//            'source_course_id' => 'Activated',
//        );
//    }


}