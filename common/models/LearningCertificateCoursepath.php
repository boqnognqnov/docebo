<?php

/**
 * This is the model class for table "learning_certificate_coursepath".
 * The followings are the available columns in table 'learning_certificate_coursepath':
 * @property integer $id_certificate
 * @property integer $id_path
 * @property integer $available_for_status
 * @property integer $point_required
 *
 * @property LearningCoursepath $learningCoursepath
 */
class LearningCertificateCoursepath extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCertificateCoursepath the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_certificate_coursepath';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_certificate, id_path, available_for_status, point_required', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_certificate, id_path, available_for_status, point_required', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificate' => array(self::HAS_ONE, 'LearningCertificate', 'id_certificate'),
            'learningCoursepath' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id_certificate' => 'Id Certificate',
			'id_path' => 'Id Path',
			'available_for_status' => 'Available For Status',
			'point_required' => 'Point Required',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_certificate', $this->id_certificate);
		$criteria->compare('id_path', $this->id_path);
		$criteria->compare('available_for_status', $this->available_for_status);
		$criteria->compare('point_required', $this->point_required);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}