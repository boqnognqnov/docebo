<?php

/**
 * This is the model class for table "app7020_settings_peer_review".
 *
 * The followings are the available columns in table 'app7020_settings_peer_review':
 * @property integer $id
 * @property integer $idContent
 * @property integer $enableCustomSettings
 * @property integer $inviteToWatch
 * @property integer $allowContributorAnswer
 * @property string $created
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 */
class App7020SettingsPeerReview extends CActiveRecord {
	/**
	 * Flag for checkbox if have any limit for questions. This will be used for swith on/off the settings
	 * @var integer
	 */

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_settings_peer_review';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idContent', 'required'),
			array('enableCustomSettings, inviteToWatch, allowContributorAnswer', 'in', 'range' => array(0, 1)),
			array('created', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id Content',
			'enableCustomSettings' => 'Enable custom settings for this asset',
			'inviteToWatch' => 'Invite to watch',
			'allowContributorAnswer' => 'Enable contributor to answer questions<div>Allow contributor to partecipate by answering to learners\' question along with the assigned knowledge gurus</div>',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('enableCustomSettings', $this->enableCustomSettings);
		$criteria->compare('inviteToWatch', $this->inviteToWatch);
		$criteria->compare('allowContributorAnswer', $this->allowContributorAnswer);
		$criteria->compare('created', $this->created, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020SettingsPeerReview the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	
	
	/**
	 * After save 
	 * @return type
	 */
	public function afterSave() {
		 

		// If enableCustomSettings is set on false, we remove record, because
		// settings will be gotten from core_settings
		if (!$this->enableCustomSettings) {
			 self::model()->deleteByPk($this->id);
		}
		
		return parent::afterSave();
	}
	
	
	
	
	

	/**
	 * Has Custtom settings for related content
	 * @return boolean
	 */
	function hasEnableCustomSettings() {
		return (bool) $this->enableCustomSettings;
	}

	/**
	 * Has invitation for watch for related content 
	 * @return boolean
	 */
	function hasInviteToWatch() {
		return (bool) $this->inviteToWatch;
	}

	/**
	 * HAs allow contributor answer
	 * @return boolean
	 */
	function hasAllowContributorAnswer() {
		return (bool) $this->allowContributorAnswer;
	}

}
