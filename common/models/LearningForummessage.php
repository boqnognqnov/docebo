<?php

/**
 * This is the model class for table "learning_forummessage".
 *
 * The followings are the available columns in table 'learning_forummessage':
 * @property integer $idMessage
 * @property integer $idThread
 * @property integer $idReply
 * @property integer $idCourse
 * @property string $answer_tree
 * @property string $title
 * @property string $textof
 * @property string $posted
 * @property integer $author
 * @property integer $generator
 * @property string $attach
 * @property integer $locked
 * @property integer $modified_by
 * @property string $modified_by_on
 * @property integer $tone_sum
 * @property integer $tone_count
 * @property string $path
 * @property string $original_filename
 *
 * The followings are the available model relations:
 * @property LearningForumthread $thread
 */
class LearningForummessage extends CActiveRecord
{
	public $search_input; //used for messages search in the list view
    public $order_by;
	public $file_maxsize = 3; // Maximum filesize in MB
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningForummessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_forummessage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, textof', 'required', 'on'=>'create'),
			array('textof', 'required', 'on'=>'reply'),
			array('idThread, idReply, idCourse, author, generator, locked, modified_by, tone_sum, tone_count', 'numerical', 'integerOnly'=>true),
			array('title, attach', 'length', 'max'=>255),
			array('posted, modified_by_on, search_input, order_by, path, original_filename', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idMessage, idThread, idReply, idCourse, answer_tree, title, textof, posted, author, generator, attach, locked, modified_by, modified_by_on, tone_sum, tone_count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'thread' => array(self::BELONGS_TO, 'LearningForumthread', 'idThread'),
			'msgUser' => array(self::BELONGS_TO, 'CoreUser', 'author'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('posted medium', 'modified_by_on medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idMessage' => 'Id Message',
			'idThread' => 'Id Thread',
			'idCourse' => 'Id Course',
			'answer_tree' => 'Answer Tree',
			'title' => Yii::t('standard', '_TITLE'),
			'textof' => Yii::t('standard', '_TEXTOF'),
			'posted' => 'Posted',
			'author' => 'Author',
			'generator' => 'Generator',
			'attach' => 'Attach',
			'locked' => 'Locked',
			'modified_by' => 'Modified By',
			'modified_by_on' => 'Modified By On',
            'tone_sum' => 'Tone Sum',
            'tone_count' => 'Tone Count',
			'path' => 'Path',
			'original_filename' => 'Original Filename',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->condition = 'idReply IS NULL';

		$criteria->compare('t.idMessage',$this->idMessage);
		$criteria->compare('t.idThread',$this->idThread);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.textof',$this->textof,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('original_filename',$this->original_filename,true);

		if ($this->search_input)
		{
			$criteria->addCondition('t.title like :search_input OR t.textof like :search_input');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}

		$criteria->with = array('thread', 'msgUser');
		$criteria->together = true;

        if($this->order_by)
            $criteria->order = 't.posted ' . $this->order_by;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

    public function getSubReplies($options){
        $criteria=new CDbCriteria;
        $criteria->compare('t.idReply',$this->idMessage);
        $criteria->with = array('thread', 'msgUser');
        $criteria->together = true;

        if($this->order_by)
            $criteria->order = 't.posted ' . $this->order_by;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => isset($options['pageSize'])  ? $options['pageSize'] : Settings::get('elements_per_page', 10),
                'currentPage' => intval($options['page']),
                'validateCurrentPage' => false
            )
        ));
    }

	/**
	 * Get all posts for a user and forum (optional)
	 * @param $userId
	 * @param $forumId
	 * @return int
	 */
	public static function getCountOfTotalUserPosts($userId, $forumId = false)
	{
		if (!empty($forumId) && (int)$forumId > 0) {
			$sql = 'SELECT COUNT(DISTINCT idMessage) FROM '.self::model()->tableName().' as msg
					INNER JOIN '.LearningForumthread::model()->tableName().' AS thread
					ON msg.`idThread` = thread.`idThread`
					AND thread.idForum = '.(int)$forumId.'
					WHERE msg.author = '.(int)$userId;
			$res = Yii::app()->db->createCommand($sql)->queryScalar();
		} else {
			$res = Yii::app()->db->createCommand()
				->select("COUNT(*)")
				->from(self::model()->tableName())
				->where("author = :author", array(':author' => $userId))
				->queryScalar();
		}

		return $res ? (int)$res : 0;
	}

    /**
     * Get all sub-replies for certain message
     * @param $message_id
     * @return int
     */
    public static function getCountSubReplies($message_id)
    {
        $res = 0;
        if (!empty($message_id) && (int)$message_id > 0) {
            $sql = 'SELECT COUNT(1) FROM '.self::model()->tableName().' as msg WHERE msg.idReply = :reply';
            $res = Yii::app()->db->createCommand($sql)->queryScalar(array(":reply" => $message_id));
        }
        return $res ? (int)$res : 0;
    }

	public function afterSave() {
		if($this->getIsNewRecord()){
			// Raised when a new reply is posted inside a discussion
			Yii::app()->event->raise('NewReply', new DEvent($this, array(
				'thread' 		=> $this->thread, // AR relation
				'user'			=> $this->msgUser, // AR relation
				'course' 		=> $this->thread->forum->course, // AR relation
			)));

            Yii::app()->event->raise(CoreNotification::NTYPE_NEW_COMMENT, new DEvent($this, array(
                'thread' 		=> $this->thread, // AR relation
                'user'			=> $this->msgUser, // AR relation
                'course' 		=> $this->thread->forum->course, // AR relation
                'message' 	    => $this->textof
            )));
		}

		parent::afterSave();
	}

    public function beforeDelete() {
        // Erase all the sub replies, if any
        $this->model()->deleteAllByAttributes(array('idReply' => $this->idMessage));

        return parent::beforeDelete();
    }

	public function afterDelete() {
		parent::afterDelete();
	}

	public function beforeSave() {
		$this->textof = Yii::app()->htmlpurifier->purify($this->textof, 'allowFrameAndTarget');
		$this->title = Yii::app()->htmlpurifier->purify($this->title, 'allowFrameAndTarget');
		return parent::beforeSave();
	}

	public function formatQuotedText()
	{
		$text = $this->textof;
		$text = preg_replace('/\[QUOTE=([^\]]*);\]/ui', '<fieldset class="forum-quote"><legend class="forum-quote-author">$1</legend>', $text);
		if (stripos($text,'[/quote]') !== false)
			$text = str_ireplace('[/quote]', '</fieldset>', $text);

		return $text;
	}



    /**
     * @return float
     */
    public function getRating() {
        $sum = intval($this->tone_sum);
        $count = intval($this->tone_count);
        $avg = (!$count) ? 0 : doubleval($sum / $count);
        return round($avg);
    }

    /**
     * @param $idUser
     * @param $value
     */
    public function saveRating($idUser, $value) {
		$model = new LearningForummessageVotes();

		$learningForummessage = $this->tableName();
		$learningForummessageVotes = $model->tableName();
		$idMessage = $this->idMessage;

		// check if the user has previously voted
		$row = Yii::app()->db->createCommand("
SELECT *
FROM $learningForummessageVotes
WHERE id_message=$idMessage AND id_user=$idUser AND type='tone'")->queryRow();

		if ($row) {
			// delete the previous rating
			$rowValue = intval($row['value']);
			Yii::app()->db->createCommand("
DELETE FROM $learningForummessageVotes
WHERE id_message=$idMessage AND id_user=$idUser AND type='tone'")->query();
			// update the tone counters for the message
			$newToneSum = $this->tone_sum - $rowValue;
			$this->tone_sum = $newToneSum;
			$newToneCount = $this->tone_count - 1;
			$this->tone_count = $newToneCount;
			Yii::app()->db->createCommand("
UPDATE $learningForummessage
SET tone_sum=$newToneSum, tone_count=$newToneCount
WHERE idMessage=$idMessage")->query();
		}

		// save the new rating
		/*Yii::app()->db->createCommand("
INSERT INTO $learningForummessageVotes
SET id_message=$idMessage, id_user=$idUser, type='tone', value=$value")->query();*/
		/* Using the model to save because we need from its afterSave() method for another events */
		$model->id_message = $idMessage;
		$model->id_user = $idUser;
		$model->type = 'tone';
		$model->value = $value;
		$model->save();

		// update the forum message
		$toneSum = intval($this->tone_sum) + $value;
		$toneCount = intval($this->tone_count) + 1;
		Yii::app()->db->createCommand("
UPDATE $learningForummessage
SET tone_sum=$toneSum, tone_count=$toneCount
WHERE idMessage=$idMessage")->query();
    }

    /**
     * @return array
     */
    public function getHelpful() {
		$learningForummessageVotes = LearningForummessageVotes::model()->tableName();
		$idMessage = $this->idMessage;

		$rowLikes = Yii::app()->db->createCommand("
SELECT COUNT(*) AS count
FROM $learningForummessageVotes
WHERE type='helpful' and id_message='$idMessage' and value=1")->queryRow();

		$rowDislikes = Yii::app()->db->createCommand("
SELECT COUNT(*) AS count
FROM $learningForummessageVotes
WHERE type='helpful' and id_message='$idMessage' and value=-1")->queryRow();

        $helpful = array(
            'likes' => intval($rowLikes['count']),
            'dislikes' => intval($rowDislikes['count'])
        );

        return $helpful;
    }

    /**
     * @param $idUser
     * @return string|mixed
     */
    public function getUserHelpfulVote($idUser) {
        $model = new LearningForummessageVotes('search');
        $model->id_message = $this->idMessage;
        $model->id_user = $idUser;
        $model->value = null;
        $model->type = 'helpful';
        $dataProvider = $model->search();

        if ($dataProvider->totalItemCount > 0) {
            $data = $dataProvider->getData();
            return ($data[0]->value == 1) ? 'like' : 'dislike';
        }

        return null;
    }

    /**
     * @param $idUser
     * @return bool|int
     */
    public function getUserRatingVote($idUser) {
		$idMessage = $this->idMessage;
		$learningForummessageVotes = LearningForummessageVotes::model()->tableName();

		$vote = Yii::app()->db->createCommand("
SELECT value
FROM $learningForummessageVotes
WHERE id_message=$idMessage AND id_user=$idUser AND type='tone'
		")->queryScalar();

		return $vote;
    }


	/**
	 * Calculate the number of helpful votes (either positive and negative) for a specific user
	 * @param $idUser the idst of the user who has been voted
	 * @return array 'yes' the number of positive helful votes, 'not' the number of negative helpful votes
	 */
	public function getUserTotalHelpfulVotes($idUser) {

		//prepare output
		$output = array(
			'yes' => 0,
			'not' => 0
		);

		//create query
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS total")
			->from(self::model()->tableName()." fm")
			->join(LearningForummessageVotes::model()->tableName()." fmv", "fm.idMessage = fmv.id_message AND fm.author = :author")
			->where("fmv.type = :type AND fmv.value = :value");
		/* @var $command CDbCommand */
		$command->bindValue(':author', $idUser);
		$command->bindValue(':type', LearningForummessageVotes::TYPE_HELPFUL);

		//calculate positive helpful votes
		$command->bindValue(':value', 1);
		$output['yes'] = $command->queryScalar();

		//calculate negative helpful votes
		$command->bindValue(':value', -1);
		$output['not'] = $command->queryScalar();

		return $output;
	}


	/**
	 * Calculate average tone vote for all messages of a  specified user
	 * @param $idUser the idst of the user who has been voted
	 * @return bool|float|int the average tone vote for messages of the specified user
	 */
	public function getUserAverageToneVotes($idUser) {
		$totals = Yii::app()->db->createCommand()
			->select("SUM(tone_sum) AS tot_tone_sum, SUM(tone_count) AS tot_tone_count")
			->from(self::model()->tableName())
			->where("author = :author", array(':author' => $idUser))
			->queryRow();
		if (!$totals) return false;
		if ($totals['tot_tone_count'] <= 0) return 0;
		return ($totals['tot_tone_sum'] / $totals['tot_tone_count']);
	}


	/**
	 * Count how many votes of value $vote the specified user has received
	 * @param $idUser the idst of the specified user
	 * @param $vote the target vote to be counted
	 * @return int
	 */
	public function countUserToneVotesByVote($idUser, $vote) {
		$count = Yii::app()->db->createCommand()
			->select("COUNT(*) AS total")
			->from(self::model()->tableName()." fm")
			->join(LearningForummessageVotes::model()->tableName()." fmv", "fm.idMessage = fmv.id_message AND fm.author = :author", array(':author' => $idUser))
			->where("fmv.type = :type AND fmv.value = :value", array(':type' => LearningForummessageVotes::TYPE_TONE, ':value' => $vote))
			->queryScalar();
		return (int)$count;
	}
}