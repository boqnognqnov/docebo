<?php

/**
 * This is the model class for table "app7020_content_history".
 *
 * The followings are the available columns in table 'app7020_content_history':
 * @property integer $id
 * @property integer $idContent
 * @property integer $idUser
 * @property timestamp $dateCreated
 * @property timestamp $dateUpdated
 * @property integer $spentTime
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 * @property CoreUser $user
 */
class App7020SpentTimeHistory extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_spent_time_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('idContent, idUser', 'required'),
			array('idContent, idUser', 'numerical', 'integerOnly' => true)
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id Content',
			'idUser' => 'Id User',
			'dateCreated' => 'Created',
			'dateUpdated' => 'Updated',
			'spentTime' => 'Spent Time'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('spentTime', $this->spentTime);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020TopicContent the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function afterSave(){

		$sql= "SELECT SUM(spentTime) AS sum FROM ".self::model()->tableName()." WHERE idContent=".$this->idContent." ";
		$command = Yii::app()->db->createCommand($sql);
		$new_time = $command->queryColumn();
		$asset = App7020Assets::model()->findByPk($this->idContent);
		Yii::app()->event->raise('AssetViewedForLongestTime', new DEvent($this, array('id_content' => $this->idContent,'new_time'=>$new_time,'user'=>$asset->userId)));
	}
	
	public static function getContinueWatchAssetsByUser($idUser){
		if(empty($idUser)){
            $idUser = Yii::app()->user->idst;
        }
		$command = Yii::app()->db->createCommand();
		$command->select(
						"asset.*, 
						asset.created AS create_date, 
						CONCAT(u.firstname, '.' , u.lastname) as author, 
						(
						SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM  app7020_content_rating WHERE idContent=asset.id
						) AS contentRating,
						(
						SELECT COUNT(id) FROM app7020_content_history WHERE idContent=asset.id
						) AS contentViews");
		$command->from(App7020Assets::model()->tableName() . ' asset');
		$command->leftJoin(CoreUser::model()->tableName() . ' u', 'u.idst=asset.userId');
		$command->leftJoin(App7020ContentHistory::model()->tableName() . ' ch', 'ch.idContent = asset.id');
		$command->leftJoin(App7020SpentTimeHistory::model()->tableName() . ' sth', 'sth.idContent = asset.id');
		$command->where('sth.idUser = '.$idUser);
		$command->andWhere('ISNULL(ch.viewed)');
		$command->andWhere('!ISNULL(sth.spentTime)');
		return $command->queryAll();
	}
}