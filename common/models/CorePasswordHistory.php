<?php

/**
 * This is the model class for table "core_password_history".
 *
 * The followings are the available columns in table 'core_password_history':
 * @property integer $idst_user
 * @property string $pwd_date
 * @property string $passw
 * @property integer $changed_by
 *
 * The followings are the available model relations:
 * @property CoreUser $changedBy
 * @property CoreUser $idstUser
 */
class CorePasswordHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CorePasswordHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_password_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst_user, changed_by', 'numerical', 'integerOnly'=>true),
			array('passw', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst_user, pwd_date, passw, changed_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'changedBy' => array(self::BELONGS_TO, 'CoreUser', 'changed_by'),
			'idstUser' => array(self::BELONGS_TO, 'CoreUser', 'idst_user'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('pwd_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst_user' => 'Idst User',
			'pwd_date' => 'Pwd Date',
			'passw' => 'Passw',
			'changed_by' => 'Changed By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst_user',$this->idst_user);
		$criteria->compare('pwd_date',Yii::app()->localtime->fromLocalDateTime($this->pwd_date),true);
		$criteria->compare('passw',$this->passw,true);
		$criteria->compare('changed_by',$this->changed_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}