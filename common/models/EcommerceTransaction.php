<?php

/**
 * This is the model class for table "ecommerce_transaction".
 *
 * The followings are the available columns in table 'ecommerce_transaction':
 * @property integer $id_trans
 * @property integer $id_user
 * @property string $location
 * @property string $date_creation
 * @property string $date_activated
 * @property integer $paid
 * @property string $note
 * @property string $billing_info_id
 * @property string $payment_type
 * @property string $payment_txn_id
 * @property string $payment_currency
 * @property string $raw_data_json
 * @property string $receipt_id
 * @property string $discount
 * @property integer $id_coupon
 * @property integer $abandoned
 * @property integer $cancelled
 * @property string $done_by
 *
 * The followings are the available model relations:
 * @property EcommerceCoupon $ecommerceCoupon
 * @property CoreUser $user
 * @property CoreUserBilling $billingInfo
 * @property EcommerceTransactionInfo[] $transaction_items
 * @property float totalPrice
 */
class EcommerceTransaction extends CActiveRecord
{
	const USERNAME_ALIAS = 'u.userid';

	const PAYMENT_STATUS_PENDING = 0;
	const PAYMENT_STATUS_SUCCESSFUL = 1;
	const PAYMENT_STATUS_FAILED = 2;

	const DONE_BY_USER = 'user';
	const DONE_BY_ADMIN = 'admin';

    //used for search purposes
    public $search_input;
	public $search_status;
    public $search_date_from;
    public $search_date_to;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ecommerce_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_creation, payment_currency', 'required'),
			array('id_user, paid, id_coupon, abandoned', 'numerical', 'integerOnly'=>true),
			array('location, discount', 'length', 'max'=>10),
			array('receipt_id', 'length', 'max'=>255),
			array('billing_info_id', 'length', 'max'=>20),
			array('payment_type', 'length', 'max'=>32),
			array('payment_txn_id', 'length', 'max'=>512),
			array('payment_currency', 'length', 'max'=>6),
			array('note, date_activated, raw_data_json', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_trans, id_user, location, date_creation, date_activated, paid, note, billing_info_id, payment_type,
			    payment_txn_id, payment_currency, raw_data_json, receipt_id, discount, id_coupon, abandoned, cancelled, done_by, 
			    search_input, search_status, search_date_from, search_date_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ecommerceCoupon' => array(self::BELONGS_TO, 'EcommerceCoupon', 'id_coupon'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'billingInfo' => array(self::BELONGS_TO, 'CoreUserBilling', 'billing_info_id'),
			'transaction_items' => array(self::HAS_MANY, 'EcommerceTransactionInfo', 'id_trans'),
            'totalPrice' => array(self::STAT, 'EcommerceTransactionInfo', 'id_trans', 'select' => 'SUM(t.price)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_trans' => 'Id Trans',
			'id_user' => 'Id User',
			'location' => 'Location',
			'date_creation' => 'Date Creation',
			'date_activated' => 'Date Activated',
			'paid' => 'Paid',
			'note' => 'Note',
			'billing_info_id' => 'Billing Info',
			'payment_type' => 'Payment Type',
			'payment_txn_id' => 'Payment Txn',
			'payment_currency' => 'Payment Currency',
			'raw_data_json' => 'Raw Data Json',
			'receipt_id' => 'Receipt',
			'discount' => 'Discount',
			'id_coupon' => 'Id Coupon',
            'abandoned' => 'Abandoned cart',
			'cancelled' => 'Cancelled transaction',
			'done_by' => 'Done by'
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior'
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_trans',$this->id_trans);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('date_creation',$this->date_creation,true);
		$criteria->compare('date_activated',$this->date_activated,true);
		$criteria->compare('paid',$this->paid);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('billing_info_id',$this->billing_info_id,true);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('payment_txn_id',$this->payment_txn_id,true);
		$criteria->compare('payment_currency',$this->payment_currency,true);
		$criteria->compare('raw_data_json',$this->raw_data_json,true);
		$criteria->compare('receipt_id',$this->receipt_id,true);
		$criteria->compare('discount',$this->discount,true);
		$criteria->compare('id_coupon',$this->id_coupon);
		$criteria->compare('abandoned',$this->abandoned);
		$criteria->compare('cancelled',$this->cancelled);
		$criteria->compare('done_by',$this->done_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EcommerceTransaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function delete() {
		//prevent record deletion, since transactions cannot be deleted anymore
		throw new CDbException('The ecommerce_transaction record #'.$this->id_trans.' cannot be deleted, only soft deletion is allowed.');
	}

	public function afterDelete()
	{
		Log::_('DELETION: Transaction with id '.$this->id_trans.' cancelled by user: '.Yii::app()->user->id, CLogger::LEVEL_WARNING);
	}


    /**
     * Delivers data for Main Transactions managemtn UI grid view
     *
     * @return CActiveDataProvider
     */
    public function transactionsManageDataProvider($paginate = true, $params = array())
    {
		//first half of the query
		$countQuery = Yii::app()->db->createCommand();
		$countQuery->select('count(*) as num_items');
		$countQuery->from($this->tableName() . ' t');
		$countQuery->join(CoreUserBilling::model()->tableName() . ' b', 'b.id = t.billing_info_id');
		$countQuery->join(CoreUser::model()->tableName() . ' u', 'u.idst = t.id_user');

		//second half
		$secondPartCountQuery = Yii::app()->db->createCommand();
		$secondPartCountQuery->select('count(*) as num_items');
		$secondPartCountQuery->from($this->tableName() . ' t');
		$secondPartCountQuery->join(CoreUserBilling::model()->tableName() . ' b', 'b.id = t.billing_info_id');
		$secondPartCountQuery->join(CoreDeletedUser::model()->tableName() . ' u', 'u.idst = t.id_user');

		// ==!!== filters are added to the first part of the query, then they are copied over ==!!==
        if ($this->search_input)
		{
			$countQuery->andWhere("t.id_trans = :search_input");
			$countQuery->orWhere("t.location LIKE :search_input_like");
			$countQuery->orWhere("u.firstname LIKE :search_input_like");
			$countQuery->orWhere("u.lastname LIKE :search_input_like");
			$countQuery->orWhere("u.email LIKE :search_input_like");
			$countQuery->orWhere("u.userid LIKE :search_input_like");
			//can't use 1 parameter for both
			$countQuery->params[':search_input'] = $this->search_input;
			$countQuery->params[':search_input_like'] = '%' . $this->search_input . '%';
		}

		if ($this->search_status)
		{


			switch ($this->search_status) {
				case 'pending':
				case 'successful':
				case 'failed':
					$statuses = self::getStatusesList();
					if (isset($statuses[$this->search_status]) && $statuses[$this->search_status]['paid'] !== false) {
						$countQuery->andWhere("t.paid = :search_status");
						$countQuery->andWhere("t.cancelled = 0");
						$countQuery->params[':search_status'] = $statuses[$this->search_status]['paid'];
					}
					break;
				case 'cancelled':
					$countQuery->andWhere("t.cancelled = 1");
					$countQuery->params[':search_status'] = $this->search_status;
					break;
			}
		}

        if ($this->search_date_from)
        {
            $parsedDateFrom = Yii::app()->localtime->fromLocalDate($this->search_date_from, 'short');
            $utcDateFrom = Yii::app()->localtime->toUTC($parsedDateFrom);
			$countQuery->andWhere('t.date_creation >= :utcDateFrom');
			$countQuery->params[':utcDateFrom'] = $utcDateFrom;
		}

		if ($this->search_date_to)
		{
			$parsedDateTo = Yii::app()->localtime->fromLocalDate($this->search_date_to, 'short');
			$utcDateTo = Yii::app()->localtime->toUTC($parsedDateTo);
			$countQuery->andWhere('t.date_creation <= :utcDateTo');
			$countQuery->params[':utcDateTo'] = $utcDateTo;
        }

//		if (isset($params['username']) && $params['username']){
//			$filters .= " AND u.userid = '/" . $params['username'] . "'";
//		}
//
//		if (isset($params['id_user']) && $params['id_user']){
//			$filters .= " AND u.idst = " . $params['id_user'];
//		}
//
//		if (isset($params['created_from']) && $params['created_from'])
//		{
//			$filters .= " AND t.date_creation >= '" . $params['created_from'] . "'";
//		}
//
//		if (isset($params['created_to']) && $params['created_to'])
//		{
//			$filters .= " AND t.date_creation <= '" . $params['created_to'] . "'";
//		}
		if(!empty($params)){
			$limitFrom = $params['from'];
			$limitCount = $params['count'];
			unset($params['from']);
			unset($params['count']);

			foreach($params as $columnName => $condition){
				if(is_array($condition)){
					// we expect two values - "from" and "to" in date format 'Y-m-d H:i:s'
					$fromParam = ":{$columnName}_from";
					$toParam = ":{$columnName}_to";
					$countQuery->andWhere("$columnName BETWEEN $fromParam AND $toParam");
					$countQuery->params[$fromParam] = $condition['from'];
					$countQuery->params[$toParam] = $condition['to'];
				} else {
					$param = str_replace('.', '_', $columnName);
					$countQuery->andWhere("$columnName = :{$param}");
					if ($columnName == self::USERNAME_ALIAS)
						$condition = '/' . $condition;
					$countQuery->params[$param] = $condition;
				}
			}
		}

		//copy filters (no need to copy params)
		$secondPartCountQuery->where = $countQuery->getWhere();
		//union both halves;
		$countQuery->union($secondPartCountQuery->getText());
		$countList = $countQuery->queryAll();

        $count = 0;
        if (!empty($countList))
            foreach ($countList as $countItem)
                $count += $countItem['num_items'];

		//first half of the dataprovider query
		$query = Yii::app()->db->createCommand();
		$query->select("t.*, b.*, u.userid, u.firstname, u.lastname, u.email, sum(ti.price) as totalPrice");
		$query->from($this->tableName() . ' t');
		$query->leftJoin(EcommerceTransactionInfo::model()->tableName() . ' ti', 'ti.id_trans = t.id_trans');
		$query->join(CoreUserBilling::model()->tableName() . ' b', 'b.id = t.billing_info_id');
		$query->join(CoreUser::model()->tableName() . ' u', 'u.idst = t.id_user');
		$query->group('t.id_trans');

		//second half of the dataprovider query
		$secondPartQuery = Yii::app()->db->createCommand();
		$secondPartQuery->select("t.*, b.*, u.userid, u.firstname, u.lastname, u.email, sum(ti.price) as totalPrice");
		$secondPartQuery->from($this->tableName() . ' t');
		$secondPartQuery->leftJoin(EcommerceTransactionInfo::model()->tableName() . ' ti', 'ti.id_trans = t.id_trans');
		$secondPartQuery->join(CoreUserBilling::model()->tableName() . ' b', 'b.id = t.billing_info_id');
		$secondPartQuery->join(CoreDeletedUser::model()->tableName() . ' u', 'u.idst = t.id_user');
		$secondPartQuery->group('t.id_trans');
		//copy filters ( we can get them from $countQuery, they are the same)
		$secondPartQuery->where = $countQuery->getWhere();
		$query->where = $countQuery->getWhere();

		//union both halves;
		$query->union($secondPartQuery->getText());
		//copy filters and params (we can get them from $countQuery, they are the same)
		$query->where = $countQuery->getWhere();
		$query->params = $countQuery->params;

		//for listTransactions API
		if(!empty($params)){
			$query->limit($limitCount, $limitFrom);
			return $query->queryAll();
		}

        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName)
            $sortAttributes[$attributeName] = 't.' . $attributeName;

        return new CSqlDataProvider($query, array(
            'totalItemCount' => $count,
            'sort' => array(
                'defaultOrder' => 'date_creation desc'
            ),
            'pagination' => ($paginate ? array('pageSize' => Settings::get('elements_per_page', 10)) : false),
        ));
    }

    public function couponsDataProvider($couponId = null) {
        $config = array();

        //$config['keyField'] = 'id_trans';
        $config['sort']['attributes'] = array('date_creation');
        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

        $criteria = new CDbCriteria();
        $with = array(
            'ecommerceCoupon' => array(
                'joinType' => 'INNER JOIN'
            ),
            'user' => array(
                'joinType' => 'INNER JOIN'
            )
        );

        $postCoreUser = Yii::app()->request->getParam('CoreUser');
        if (!empty($postCoreUser['search_input'])) {
            $userQuery = strtolower(addcslashes($postCoreUser['search_input'], '%_'));

            $criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) like :search OR user.email LIKE :search OR user.userid LIKE :search');
            $criteria->params[':search'] = '%'.$userQuery.'%';
        }

        if ($couponId) {
            $criteria->addCondition('t.id_coupon = :id_coupon');
            $criteria->params[':id_coupon'] = $couponId;
        }

        $criteria->with = $with;

        $config['criteria'] = $criteria;

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * Return an array course or learning plan models related to this transaction
     *
     * @return array
     */
    public function getRelatedItems() {
        $purchasedItems = array();
        foreach ($this->transaction_items as $transaction_info ) {
	        if($transaction_info->item_type=='course'){
		        $purchasedItems[] = $transaction_info->course;
		        }elseif($transaction_info->item_type=='coursepath'){
			        $purchasedItems[] = $transaction_info->coursepath;
	    		    }elseif($transaction_info->item_type=='courseseats'){
		        		$purchasedItems[] = $transaction_info->courseseats;
	        			}
        }
        return $purchasedItems;
    }

    public static function exportTypes($addPrompt = false)	{

        $result = array(
            'Excel5' => Yii::t('standard', 'Excel'),
            'CSV' => Yii::t('standard', 'CSV'),
            'HTML' => Yii::t('standard', 'HTML'),
            //'PDF' => Yii::t('standard', 'PDF'), currently there are problems with the export
        );

        if ($addPrompt) {
            $result = array('' => Yii::t('standard','_SELECT')) + $result;
        }


        return $result;
    }

    /**
     * @return string
     */
    public function renderTotalPaid() {
        $totalPaid = $this->totalPrice;
        $discount = floatval($this->discount);

        if ($discount)
            $totalPaid -= $discount;

		if($totalPaid < 0)
			$totalPaid = 0;

        return number_format($totalPaid, 2) . ' ' . $this->payment_currency;
    }

    public static function renderTotalPaidFromData($data = array()) {
        $totalPaid = $data['totalPrice'];
        $discount = floatval($data['discount']);

        if ($discount)
            $totalPaid -= $discount;

		if($totalPaid < 0)
			$totalPaid = 0;

        return number_format($totalPaid, 2) . ' ' . $data['payment_currency'];
    }

    /**
     * @return string
     */
    public function renderTotalAmount() {
        return number_format($this->totalPrice, 2) . ' ' . $this->payment_currency;
    }

    /**
     * @return string
     */
    public function renderDiscount() {
        return number_format($this->discount, 2) . ' ' . $this->payment_currency;
    }

    /**
     * @return string
     */
    public function renderDateCreation() {
		$timeConversion = new LocalTime();
		$fromDateFormat = $timeConversion->getPHPLocalDateFormat();
		return $timeConversion->convertCouponDate($this->date_creation, $fromDateFormat);
    }


    /**
     * Return an specially crafted array of RECEIPT data, using NOTIFICATION shortcodes as element keys
     * Usefull for NotificationApp Transport agent, to easy the placeholder replacemtns
     *
     * @return array
     */
    public function getOrderReceiptInfo() {

    	// Extract various receipt data from transaction
    	$subtotal 	= floatval($this->totalPrice);
    	$discount 	= floatval($this->discount);
    	$totalPaid 	= floatval($this->totalPrice) - $discount;
    	$user 	= $this->billingInfo->user;
    	$bi 	= $this->billingInfo;
    	$items = $this->transaction_items;
    	$orderItems = "";
    	foreach ($items as $item) {
    		$course = $item->course;
    		$orderItems .= $course->name .  "\t\t\t" . $item->price . " " . $this->payment_currency . "\n";
    	}

    	// Build the result
    	$result = array();

    	$result[CoreNotification::SC_ORDER_ID] 				= $this->id_trans;
    	$result[CoreNotification::SC_ORDER_PAYMENT_DATE] 	= $this->date_creation;
    	$result[CoreNotification::SC_ORDER_SUBTOTAL]		= $subtotal;
    	$result[CoreNotification::SC_ORDER_DISCOUNT]		= $discount;
    	$result[CoreNotification::SC_ORDER_TOTAL_PAID]		= $totalPaid;
    	$result[CoreNotification::SC_ORDER_ITEMS] 			= $orderItems;

    	// Billing info
    	if ($bi) {
    		$result[CoreNotification::SC_ORDER_COMPANY]			= $bi->bill_company_name;
    		$result[CoreNotification::SC_ORDER_ADDRESS]			= $bi->bill_address1 . " " . $bi->bill_address2;
    		$result[CoreNotification::SC_ORDER_STATE] 			= $bi->bill_state;
    		$result[CoreNotification::SC_ORDER_ZIP] 			= $bi->bill_zip;
    		$result[CoreNotification::SC_ORDER_CITY] 			= $bi->bill_city;
    		$result[CoreNotification::SC_ORDER_COUNTRY] 		= $bi->bill_country_code ? CoreCountry::getNameByIsoCode($bi->bill_country_code) : "";
    	}

    	// User info
    	if ($user) {
    		$result[CoreNotification::SC_FIRSTNAME] 		= $user->firstname;
    		$result[CoreNotification::SC_LASTNAME] 			= $user->lastname;
    	}

    	return $result;

    }


    /**
     * Return a string (full receipt, multi line) suitable for sending by email or on-screen display, consisting the whole order info
     *
     */
    public function getOrderReceiptContent() {

    	// This is the Yii KEY !!! Copy & Paste, don't change; do NOT change!
		$text =
"---------------------------------------------
Order Id: {id_trans}
Date: {date_creation}
Subtotal: {total} {currency}
Discount: {discount} {currency}
Total Paid: {total_paid} {currency}

Billed to:
{billed_to}

Order Items
---------------------------------------------
{order_items}
---------------------------------------------";

    	// User billing info
    	$b = $this->billingInfo;

    	// And the User
    	$user = $this->billingInfo->user;

    	$total = floatval($this->totalPrice);
    	$discount = floatval($this->discount);
    	$totalPaid = floatval($this->totalPrice) - $discount;

    	// Compose "billed to" text
    	$billedTo =
    		($user ? $user->firstname . " " . $user->lastname . "\n" : "") .
    		($b->bill_company_name ? $b->bill_company_name . "\n" : "") .
    		$b->bill_address1 . "\n" .
    		$b->bill_address2 . "\n" .
    		$b->bill_state . "\n" .
    		$b->bill_zip . " " . $b->bill_city . "\n" .
    		CoreCountry::getNameByIsoCode($b->bill_country_code) . "\n" .
    		"";

    	// Compose "items list" text
    	$orderItems = $this->getOrderItemsText();

    	// Use Yii translation mechanic to replace the data and translate at the same time
    	$text = Yii::t('transaction', $text, array(
    			'{id_trans}' 		=> $this->id_trans,
    			'{date_creation}' 	=> $this->date_creation,
    			'{total}'			=> $total,
    			'{discount}'		=> $discount,
    			'{total_paid}'		=> $totalPaid,
    			'{billed_to}'		=> $billedTo,
    			'{currency}'		=> $this->payment_currency,
    			'{order_items}'		=> $orderItems,
    	));

    	return $text;

    }

	/**
	 * "items list" text
	 * @return string
	 */
	public function getOrderItemsText()
	{
		$items = $this->transaction_items;
		$orderItems = "";
		foreach ($items as $item) {
			$course = $item->course;
			$orderItems .= $course->name .  "\t\t\t" . $item->price . " " . $this->payment_currency . "\n";
		}

		return $orderItems;
	}


	/**
	 * List of transaction statuses for filter dropdown (related values and translated names)
	 * @return array
	 */
	public static function getStatusesList() {
		return array(
			'pending' => array('paid' => self::PAYMENT_STATUS_PENDING, 'title' => Yii::t('gamification', 'Pending')),
			'successful' => array('paid' => self::PAYMENT_STATUS_SUCCESSFUL, 'title' => Yii::t('standard', 'Successful')),
			'failed' => array('paid' => self::PAYMENT_STATUS_FAILED, 'title' => Yii::t('standard', 'failed')),
			'cancelled' => array('paid' => false, 'title' => Yii::t('course', '_CST_CANCELLED')),
		);
	}

}
