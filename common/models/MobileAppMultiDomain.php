<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class MobileAppMultiDomain extends CActiveRecord{
    /**
     * @return string
     */
	public function tableName()
	{
		return 'mobileapp_multidomain';
	}

    /**
     * @param string $className
     * @return static
     */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Check in the CoreMultidomain table for active domain in the mobileApp.
     * If any return the mobileApp
     *
     * If do not have settings for the MultiDomain get the settings from
     * the plugin MobileApp /Single domain/
     *
     * @return array of settings['login_color','primary_color','app_logo']
     */
    public static function getSettings() {
		$result = array();
		if ( PluginManager::isPluginActive('WebApp') ) {
            $mobileApp  = CorePlugin::model()->findByAttributes(array('plugin_name' => 'WebApp'));
            $result    = $mobileApp['settings'];
        }
		if ( PluginManager::isPluginActive('MultidomainApp') ) {
			// Resolve Multidomain client, ignoring the one saved in session, if any (true)
			$multidomain = CoreMultidomain::resolveClient(true);
	 
			$model = self::model()->findByAttributes(array('idMultidomain' => $multidomain->id));
			if($model != NULL) {
				$result['login_page_background'] = !is_null($model->login_color) ? $model->login_color : false;
				$result['main_app_color'] = !is_null($model->primary_color) ? $model->primary_color : false;
				$result['app_logo'] = !is_null($model->logo) ? $model->logo : false;
				$result['isMultidomain'] = true;
			}
		}

		return $result;
    }

    /**
     * INPUT::--  'http://www.my.lms.docebo.com/api'
     * OUTPUT::-- 'www.my.lms.docebo.com'
     *
     * Get the domain name and strip '/api' from it as well: 'http://', 'https://'
     * @return clean domain name
     */
    public static function getDomainName() {
        $currentUrl = Yii::app()->getBaseUrl(true);
        $currentUrl = preg_replace('#^https?://#', '', $currentUrl);
        $currentUrl = str_replace('/api', '', $currentUrl);

        return $currentUrl;
    }
}