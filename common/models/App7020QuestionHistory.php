<?php

/**
 * This is the model class for table "app7020_question_history".
 *
 * The followings are the available columns in table 'app7020_question_history':
 * @property integer $id
 * @property integer $idQuestion
 * @property integer $idUser
 * @property timestamp $viewed
 *
 * The followings are the available model relations:
 * @property App7020Question $question
 * @property CoreUser $user
 */
class App7020QuestionHistory extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_question_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        
        return array(
            array('idQuestion, idUser', 'required'),
            array('idQuestion, idUser', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'question' => array(self::BELONGS_TO, 'App7020Question', 'idQuestion'),
            'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idQuestion' => 'Id Question',
            'idUser' => 'Id User',
            'viewed' => 'Viewed'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idQuestion', $this->idQuestion);
		$criteria->compare('idUser', $this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020TopicContent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Checks if in the history table exists a record for the selected user and question. 
     * If yes - updates the record, otherwise - a new record will be created
     * @return boolean
     */
    public function secureSave(){
        if(!$this->idUser || !$this->idQuestion){
            return false;
        }
        if($existingHistory = $this->findByAttributes(array('idQuestion' => $this->idQuestion, 'idUser' => $this->idUser))){
            $existingHistory->viewed = Yii::app()->localtime->getUTCNow();
            return $existingHistory->update();
        } else {
            return $this->save();
        }
    }
    
    /**
     * Get the list of answer IDs which are created after last user's view of the selected question
     * @param integer $idQuestion
     * @return array
     */
    public static function getNewAnswers($idQuestion){
        if(!$idQuestion){
            return false;
        }
        
        $returnArray = array();
        
        $dbCommand = Yii::app()->db->createCommand();
        
        $dbCommand->select("a.id as idAnswer");
        $dbCommand->from(App7020Answer::model()->tableName()." a");
        $dbCommand->leftJoin(App7020QuestionHistory::model()->tableName(). " qh", " a.idQuestion=qh.idQuestion");
        $dbCommand->where("a.idQuestion = :idQuestion", array(":idQuestion" => $idQuestion));
        $dbCommand->andWhere("(a.created > qh.viewed OR ISNULL(qh.viewed))");
        $dbCommand->andWhere("(qh.idUser = ".Yii::app()->user->idst." OR ISNULL(qh.idUser))");
        $dbCommand->order("a.created DESC");
        return $dbCommand->queryAll();
        
    }
    
    /**
     * Get the list of answer IDs which are created after last user's view of the selected question
     * @param integer $idQuestion
     * @return array
     */
    public static function getQuestionViewes($idQuestion){
        if(!$idQuestion){
            return false;
        }
    $criteria = new CDbCriteria();
		$criteria->condition = 'idQuestion = :idQuestion';
		$criteria->params = array(':idQuestion' => $idQuestion);
		$criteria->group = 'idUser';
		$count = App7020QuestionHistory::model()->count($criteria);
		return $count;
//        $returnArray = array();
//    
//        $dbCommand = Yii::app()->db->createCommand();
//    
//        $dbCommand->select("COUNT(id) AS viewesCount");
//        $dbCommand->from(App7020QuestionHistory::model()->tableName());
//        $dbCommand->where("idQuestion = :idQuestion", array(":idQuestion" => $idQuestion));
//        $result = $dbCommand->queryAll();
//        return $result[0]['viewesCount'];
    
    }
    
 }
