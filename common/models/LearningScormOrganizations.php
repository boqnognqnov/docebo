<?php

/**
 * This is the model class for table "learning_scorm_organizations".
 *
 * The followings are the available columns in table 'learning_scorm_organizations':
 * @property integer $idscorm_organization
 * @property string $org_identifier
 * @property integer $idscorm_package
 * @property string $title
 * @property integer $nChild
 * @property integer $nDescendant
 *
 * @property LearningScormItems[] $scormItems
 * @property LearningScormPackage $scormPackage
 * @property LearningOrganization $learningOrganization
 */
class LearningScormOrganizations extends CActiveRecord
{

	/**
	 * If set to true, when the organization is deleted the track data related to scorm items will be deleted too
	 * @var boolean
	 */
	private $_eraseTrackData = true;


	//get / set methods

	public function getEraseTrackData() {
		return $this->_eraseTrackData;
	}

	public function setEraseTrackData($value) {
		$this->_eraseTrackData = (bool)$value;
	}



	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormOrganizations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_organizations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idscorm_package, nChild, nDescendant', 'numerical', 'integerOnly'=>true),
			array('org_identifier', 'length', 'max'=>255),
			array('title', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_organization, org_identifier, idscorm_package, title, nChild, nDescendant', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scormPackage'			=> array(self::BELONGS_TO, 'LearningScormPackage', 'idscorm_package'),
			'scormItems' 			=> array(self::HAS_MANY, 'LearningScormItems', 'idscorm_organization'),
			'learningOrganization'	=> array(self::HAS_ONE, 'LearningOrganization', array('idResource' => 'idscorm_organization')), 
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_organization' => 'Idscorm Organization',
			'org_identifier' => 'Org Identifier',
			'idscorm_package' => 'Idscorm Package',
			'title' => 'Title',
			'nChild' => 'N Child',
			'nDescendant' => 'N Descendant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_organization',$this->idscorm_organization);
		$criteria->compare('org_identifier',$this->org_identifier,true);
		$criteria->compare('idscorm_package',$this->idscorm_package);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('nChild',$this->nChild);
		$criteria->compare('nDescendant',$this->nDescendant);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'learningObjects' => array(
				'class' => 'LearningObjectsBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG,
				'idPropertyName' => 'idscorm_organization',
				'titlePropertyName' => 'title'
			)
		);
	}



	public function onBeforeDelete($event) {

		if (Yii::app()->db->getCurrentTransaction() === NULL) { $transaction = Yii::app()->db->beginTransaction(); }

		try {

			//remove tracking data, if requested
			if ($this->getEraseTrackData()) {
				// Retrievee all objects linking this scorm package
				$organizations = LearningOrganization::model()->findAllByAttributes(array(
					'idResource' => $this->getPrimaryKey(),
					'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
				));

				// delete organization tracking data
				foreach($organizations as $organization) {
					$rs = LearningScormTracking::model()->deleteAllByAttributes(array('idReference' => $organization->getPrimaryKey()));
					if ($rs === false)
						throw new CException('Error while deleting organization tracking data');
				}

				//delete items tracking data
				$rs = LearningScormItemsTrack::model()->deleteAllByAttributes(array('idscorm_organization' => $this->getPrimaryKey()));
				if ($rs === false) {
					throw new CException('Error while deleting items tracking data');
				}
			}

			//remove items
			$rs = LearningScormItems::model()->deleteAllByAttributes(array('idscorm_organization' => $this->getPrimaryKey()));
			if ($rs === false) {
				throw new CException('Error while deleting scorm items');
			}

			$package = LearningScormPackage::model()->findByPk($this->idscorm_package);
			if (!$package) {
				throw new CException('Invalid package specification');
			}
				
			// Detect if there are other scorm organizations in the SAME package, DIFFERENT than the current one ($this)
			$criteria = new CDbCriteria();
			$criteria->addCondition('idscorm_organization <> :idscorm_organization');
			$criteria->addCondition('idscorm_package=:idscorm_package');
			$criteria->params[':idscorm_organization'] = $this->getPrimaryKey();
			$criteria->params[':idscorm_package'] = $this->idscorm_package;
			$others = LearningScormOrganizations::model()->findAll($criteria);

			if (count($others) <= 0) {
				//there aren't other organizations in this package, so delete resources, files and package info

				//delete the package: this will call in cascade package-related operations such as files and resources deleting
				$rs = $package->delete();
				if (!$rs) {
					throw new CException('Error while deleting scorm package');
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch(CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e; //pass exception to above level
		}

		parent::onBeforeDelete($event);
	}



	public function getItems() {
		$criteria = new CDbCriteria();
		$criteria->order = 'idscorm_item ASC';
		return LearningScormItems::model()->findAllByAttributes(array('idscorm_organization' => $this->getPrimaryKey()), $criteria);
	}


	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {

		// first we need to copy the scorm package
		$original_package = LearningScormPackage::model()->findByPk($this->idscorm_package);

		$destination_package = new LearningScormPackage();
		$destination_package->idpackage 	= $original_package->idpackage;
		$destination_package->idProg 		= $original_package->idProg;
		$destination_package->path 			= $original_package->path;
		$destination_package->location 		= $original_package->location;
		$destination_package->defaultOrg 	= $original_package->defaultOrg;
		$destination_package->idUser 		= $original_package->idUser;
		$destination_package->scormVersion 	= $original_package->scormVersion;
		// disable behaviour, they contain file management
		$destination_package->disableBehavior('Scorm');
		if (!$destination_package->save()) {
			$errorMessage = 'Error while copying LO: '.implode("; ", $destination_package->getErrors());
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}

		// now the scorm orgnization !
		$copy = new LearningScormOrganizations();
		$copy->org_identifier 	= $this->org_identifier;
		$copy->idscorm_package 	= $destination_package->idscorm_package;
		$copy->title 			= $this->title;
		$copy->nChild 			= $this->nChild;
		$copy->nDescendant 		= $this->nDescendant;
		if (!$organization) {
			$copy->disableLearningObjectsEvents(); //this is important, otherwise it will break DB
		}
		if (!$copy->save()) {

			$errorMessage = 'Error while copying LO: '.implode("; ", $destination_package->getErrors());
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
		// duplicate resources
		$original_resources = LearningScormResources::model()->findAll('idscorm_package=:idscorm_package', array(
			':idscorm_package' => $this->idscorm_package
		));
		$map_resources = array();
		foreach ($original_resources as $original_resource) {

			$destination_reource = new LearningScormResources();
			$destination_reource->idsco = $original_resource->idsco;
			$destination_reource->idscorm_package = $destination_package->idscorm_package;
			$destination_reource->scormtype = $original_resource->scormtype;
			$destination_reource->href = $original_resource->href;
			if ($destination_reource->save()) {

				$map_resources[$original_resource->idscorm_resource] = $destination_reource->idscorm_resource;
			}
		}

		//copy scorm items and resources
		$items = $this->getItems();
		if (!empty($items)) {
			$copiedItems = array();
			$mirrorItems = array();

			// Keep  PK => Parent model
			$parents = array();
			$twins = array();

			foreach ($items as $item) {
				$copiedItem = new LearningScormItems();
				$copiedItem->idscorm_organization = $copy->getPrimaryKey();
				$copiedItem->idscorm_parentitem = NULL; //this will be set in a second step
				$copiedItem->adlcp_prerequisites = $item->adlcp_prerequisites;
				$copiedItem->adlcp_maxtimeallowed = $item->adlcp_maxtimeallowed;
				$copiedItem->adlcp_timelimitaction = $item->adlcp_timelimitaction;
				$copiedItem->adlcp_datafromlms = $item->adlcp_datafromlms;
				$copiedItem->adlcp_masteryscore = $item->adlcp_masteryscore;
				$copiedItem->item_identifier = $item->item_identifier;
				$copiedItem->identifierref = $item->identifierref;
				$copiedItem->idscorm_resource = $map_resources[$item->idscorm_resource];
				$copiedItem->isvisible = $item->isvisible;
				$copiedItem->parameters = $item->parameters;
				$copiedItem->title = $item->title;
				$copiedItem->nChild = $item->nChild;
				$copiedItem->nDescendant = $item->nDescendant;
				$copiedItem->adlcp_completionthreshold = $item->adlcp_completionthreshold;

				if (!$copiedItem->save()) {
					$errorMessage = 'Error while copying LO: '.implode("; ", $copiedItem->getErrors());
					Yii::log($errorMessage);
					throw new CException($errorMessage);
				}

				// OldModel PK -> New Model PK
				$twins[$item->getPrimaryKey()] = $copiedItem->getPrimaryKey();

				// New Model PK -> Old Model Parent PK
				$oldModelParentPks[$copiedItem->getPrimaryKey()] = $item->idscorm_parentitem;

				// Keep all copied models; Below their parents will be fixed
				$copiedItems[] = $copiedItem;
			}

			// Enumerate newly copied models and set Parents
			foreach ($copiedItems as $copiedItem) {
				/*
				  OLD Model								New model (copied)
				  =====================                 ====================
				  PK         parent PK                 	PK         parent PK
				  ---------------------                	-------------------
				  [12] -------=10=--------------------> [110]   -      ....
				  13      -    [12]<------------------  [120]     -    110

				  Logic is:
				  For 120:
					1. get parent PK of old model (oldModelParentPks[120]=12)
					2. use it to get the PK of the twin of its parent (twins[12] = 110)

				 */
				$copiedItem->idscorm_parentitem = $twins[$oldModelParentPks[$copiedItem->getPrimaryKey()]];

				if (!$copiedItem->save()) {
					$errorMessage = 'Error while setting item parents: '.implode("; ", $copiedItem->getErrors());
					Yii::log($errorMessage);
					throw new CException($errorMessage);
				}
			}

		}
		if (!$organization) { $copy->enableLearningObjectsEvents(); }
		return $copy;

	}

}