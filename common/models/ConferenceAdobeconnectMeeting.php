<?php

/**
 * This is the model class for table "conference_adobeconnect_meeting".
 *
 * The followings are the available columns in table 'conference_adobeconnect_meeting':
 * @property integer $id
 * @property integer $id_room
 * @property string $folder_id
 * @property string $created_by
 * @property string $server_url
 * @property string $url_path
 * @property string $sco_id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property ConferenceRoom $idRoom
 */
class ConferenceAdobeconnectMeeting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceAdobeconnectMeeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_adobeconnect_meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_room, folder_id, server_url, url_path, sco_id, name', 'required'),
			array('id_room', 'numerical', 'integerOnly'=>true),
			array('folder_id, sco_id', 'length', 'max'=>32),
			array('created_by, url_path', 'length', 'max'=>128),
			array('created_by', 'safe'),
			array('server_url', 'length', 'max'=>512),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_room, folder_id, created_by, server_url, url_path, sco_id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'conferenceRoom' => array(self::BELONGS_TO, 'ConferenceRoom', 'id_room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_room' => 'Id Room',
			'folder_id' => 'Folder',
			'created_by' => 'Created By',
			'server_url' => 'Server Url',
			'url_path' => 'Url Path',
			'sco_id' => 'Sco',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_room',$this->id_room);
		$criteria->compare('folder_id',$this->folder_id,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('server_url',$this->server_url,true);
		$criteria->compare('url_path',$this->url_path,true);
		$criteria->compare('sco_id',$this->sco_id,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
}