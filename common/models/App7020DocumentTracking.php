<?php
/**
 * This is the model class for table "app7020_document_tracking".
 *
 *
 * @property integer $id
 * @property integer $idContent
 * @property integer $idUser
 * @property integer $Page
 * @property timestamp $date

 * @property bool $used

 *
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 * @property CoreUser $user
 */

class App7020DocumentTracking extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_document_tracking';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array(
            array('idContent, idUser', 'required'),
            array('idContent, idUser', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
            'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idContent' => 'Id Content',
            'idUser' => 'Id User',
            'Page' => 'Page',
            'date' => 'Date',

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idContent', $this->idContent);
        $criteria->compare('idUser', $this->idUser);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020DocumentConversions the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Checks if in the tracking table exists a record for the selected user and content.
     * If yes - updates the record, otherwise - a new record will be created
     * @return boolean
     */
    public function secureSave(){
        if(!$this->idUser || !$this->idContent){
            return false;
        }
        if($existingTracking = $this->findByAttributes(array('idContent' => $this->idContent, 'idUser' => $this->idUser))){
            $existingTracking->date = Yii::app()->localtime->getUTCNow();
            $existingTracking->Page = $this->Page;
            return $existingTracking->update();
        } else {
            return $this->save();
        }
    }


    public static function getLastViewedPage($idContent, $idUser){
        if(empty($idUser)){
            $idUser = Yii::app()->user->idst;
        }
        $dbCommand = Yii::app()->db->createCommand();
        $dbCommand->select('Page');
        $dbCommand->from(self::model()->tableName());
        $dbCommand->where("idContent = :idContent AND idUser = :idUser", array(':idContent' => $idContent, ':idUser' => $idUser));
        $result = $dbCommand->queryRow();
        return $result;
    }

	/*
	 * Get all documents which are not yet read by the given user
	 * 
	 * @param int $idUser
	 */
	public static function getProceedingDocs($idUser){
        if(empty($idUser)){
            $idUser = Yii::app()->user->idst;
        }
        $dbCommand = Yii::app()->db->createCommand();
        $dbCommand->select("asset.*, asset.created AS create_date, CONCAT(u.firstname, '.', u.lastname) as author, (tracking.Page+1) as viewedPages, count(di.idContent) as allPages, "
        . "(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent=asset.id) AS contentRating, "
        . "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent=asset.id) AS contentViews ");
        $dbCommand->from(App7020Assets::model()->tableName() . ' asset');
        $dbCommand->rightJoin(self::model()->tableName(). ' tracking', 'tracking.idContent = asset.id');
		$dbCommand->join(App7020DocumentImages::model()->tableName().' di', 'di.idContent = tracking.idContent');
        $dbCommand->join(CoreUser::model()->tableName() . " u", "u.idst=asset.userId");
        $dbCommand->where("tracking.idUser=:idUser", array(':idUser' => $idUser));
        $dbCommand->andWhere("asset.is_private=:private", array(':private' => App7020Assets::PRIVATE_STATUS_INIT)); //only public assets
        $dbCommand->andWhere("asset.conversion_status=:status", array(':status' => App7020Assets::CONVERSION_STATUS_APPROVED));

		$dbCommand->group('tracking.idContent');
		$dbCommand->having("viewedPages < allPages");

        return $dbCommand->queryAll();
    }


}