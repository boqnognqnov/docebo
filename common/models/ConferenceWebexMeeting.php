<?php

/**
 * This is the model class for table "conference_webex_meeting".
 *
 * The followings are the available columns in table 'conference_webex_meeting':
 * @property integer $id
 * @property integer $id_room
 * @property string $type
 * @property string $session_id
 * @property string $host_calendar_url
 * @property string $attendee_calendar_url
 * @property string $guest_token
 */
class ConferenceWebexMeeting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceWebexMeeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_webex_meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_room, session_id', 'required'),
			array('id_room', 'numerical', 'integerOnly'=>true),
			array('session_id, type', 'length', 'max'=>20),
			array('host_calendar_url, attendee_calendar_url', 'length', 'max'=>512),
			array('guest_token', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_room, type, session_id, host_calendar_url, attendee_calendar_url, guest_token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_room' => 'Id Room',
			'session_id' => 'Session',
			'host_calendar_url' => 'Host Calendar Url',
			'attendee_calendar_url' => 'Attendee Calendar Url',
			'guest_token' => 'Guest Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_room',$this->id_room);
		$criteria->compare('session_id',$this->session_id,true);
		$criteria->compare('host_calendar_url',$this->host_calendar_url,true);
		$criteria->compare('attendee_calendar_url',$this->attendee_calendar_url,true);
		$criteria->compare('guest_token',$this->guest_token,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}