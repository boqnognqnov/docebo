<?php
/**
 * 'Assets' are files, accessed from Web, located in the File Storage, COLLECTION_ASSETS.
 * They are just files: images, documents and so on.
 *
 * They can be Shared or Private, residing phisically in a different locations,
 * but from user and developer point of view, it is transparent.
 *
 * Shared means available for ALL LMSs running on this system/server or basically having access to the same storage/collection!!!!
 * Private means available for the currently running LMS only.
 *
 * In theory, one LMS should NOT be able to access another LMS's assets, but only its own Private and Shared ones.
 *
 * Assets are grouped by its TYPE. The type defines the asset role or puspose. It is purely logical separation.
 * Course Logo is a type,  Player background is a type, etc.
 *
 * Assets of the same type go into the same subfolder of the collection.
 *
 * Every individual assets MAY have "variants", e.g. Course Logo images could be "original", "small" and "micro" by size.
 * (As of this writing only *image* types have variants)
 *
 * Variants go into their own subfolder of the TYPE folder. It is coder reposnsible to know variants and use them properly.
 * Asset variants share the same FILE NAME, e.g. (note the same file name):
 * 		courselogo/original/thefile.png
 * 		courselogo/small/thefile.png
 * 		courselogo/micro/thefile.png
 *
 * Assets of generic *image* type (course logos, backgrounds and so on), use possible variants from Yii::app()->params['imageVariants']
 * You can find it in /common/config/parts/params.php). If a variant is found there, subfolder with converted images will be created.
 * This is to automate the process of adding more image variant: just add another set of settings in the params.
 *
 * This model is a descriptor of all assets, holding information about (but not limited to):
 * 		Filename 				e.g.   86876348654376534.png
 * 		Original Filename 		the file/path this assets has been created from
 * 		Type					e.g. Course log, PLayer BG, ...
 * 		Scope					e.g.  Shared, Private
 *
 *
 * The followings are the available columns in table 'core_asset':
 * @property integer $id
 * @property string $filename			The final name of the file saved into the storage (remote), used in URL's
 * @property string $original_filename 	The name of the source file used to create this asset (local)
 * @property string $type				Type of the file: course logo, player background, etc.
 * @property integer $scope 			Shared or Private
 * @property sourceFile				NON-DB attribute: asset source file
 */
class CoreAsset extends CActiveRecord
{

	/**
	 * Possible asset types, by its role or purpose.
	 * Matches the first folder level in assets storage collection.
	 *
	 * @var string
	 */
	// IMAGES
	// NOTE: These *image* types must exist in /common/config/parts/params.php Yii configuration file,
	// which describes their variants/sizes.
	const TYPE_COURSELOGO 				= 'courselogo';
	const TYPE_PLAYER_BACKGROUND 		= 'playerbg';
	const TYPE_LO_IMAGE                 = 'lo_image';
	const TYPE_LOGIN_BACKGROUND 		= 'loginbg';
	const TYPE_GAMIFICATION_BADGES 		= 'badges';
	const TYPE_GAMIFICATION_REWARDS 	= 'rewards';
	const TYPE_GENERAL_IMAGE 			= 'img';
	const TYPE_APP7020                  = 'app7020';
	const TYPE_APP_ICON      			= 'api_icon';

	// GENERAL ASSETS
	const TYPE_GENERAL_ASSET 			= 'doc';

	// Video subtitles
	const TYPE_SUBTITLES				= 'subtitles'; 


	/**
	 * Asset shared status:
	 * SHARED : available for ALL LMSs running on this server or having access to the storage
	 * PRIVATE: only available and used by currently running LMS
	 *
	 * @var number
	 */
	const SCOPE_SHARED			= 1;  	// This item is Shared (used by ALL LMSs
	const SCOPE_PRIVATE			= 2;	// This item is private to currently runnin LMS installation

	/**
	 * Possible variants of the asset (file). Name of the file is the same in all variants, but is located in
	 * different sub-folders (matching variant names).
	 * E.g.:  /assets/courselogo/small, which is translated as /assets/<type>/<variant>
	 *
	 * Note: Not only images could have variants. PDF for example, is also a legitimate candidate.
	 *
	 * @var string
	 */
	const VARIANT_ORIGINAL					= 'original';
	const VARIANT_SMALL							= 'small';
	const VARIANT_MICRO							= 'micro';
	//const VARIANT_STRETCHED					= 'stretched';
	//const VARIANT_STRETCHED_LARGE		= 'stretched_large';

	/**
	 * List of asset types representing images,
	 * @var array of strings
	 */
	static $imageAssetTypes	= array(
		self::TYPE_COURSELOGO,
		self::TYPE_LO_IMAGE,
		self::TYPE_PLAYER_BACKGROUND,
		self::TYPE_LOGIN_BACKGROUND,
		self::TYPE_GAMIFICATION_BADGES,
        self::TYPE_GAMIFICATION_REWARDS,
		self::TYPE_GENERAL_IMAGE,
		self::TYPE_APP_ICON,
		self::TYPE_APP7020,
	);

	/**
	 * Paths to images in /themes/<theme> where system (later shared) images reside.
	 * During initial assets generation, these paths are scanned, files are copied, converted and rename and then upload to storage.
	 *
	 * @var string
	 */
	static $systemImagesPaths = array(
		self::TYPE_COURSELOGO 				=> 'course_tb',			// in /themes/<theme>/images
		self::TYPE_PLAYER_BACKGROUND 		=> 'course_player',		// in /themes/<theme>/images
		self::TYPE_LOGIN_BACKGROUND 		=> 'login',				// in /themes/<theme>/images
		self::TYPE_GAMIFICATION_BADGES		=> 'badges',   	
        self::TYPE_GAMIFICATION_REWARDS     => 'rewards'// in /plugins/GamificationApp/assets
	);

	/**
	 * Array representation of all "variants" of all asset types.
	 * This is used during cleanup and is a hardcoded list. This is the downside.
	 * The good thing is, it speeds up the cleanup, avoiding recursive search in storage.
	 * So, we pay the price.
	 *
	 * @var array
	 */
	static $possibleVariants	= array(
		self::VARIANT_ORIGINAL,
		self::VARIANT_SMALL,
		self::VARIANT_MICRO,
		//self::VARIANT_STRETCHED,
		//self::VARIANT_STRETCHED_LARGE
	);


	static $allowedTypes = array(
		self::TYPE_COURSELOGO,
		self::TYPE_LO_IMAGE,
		self::TYPE_PLAYER_BACKGROUND,
		self::TYPE_LOGIN_BACKGROUND,
		self::TYPE_GENERAL_IMAGE,
		self::TYPE_GAMIFICATION_BADGES,
        self::TYPE_GAMIFICATION_REWARDS,
		self::TYPE_GENERAL_ASSET,
		self::TYPE_APP_ICON,
		self::TYPE_APP7020,
		self::TYPE_SUBTITLES,
	);


	/**
	 * Various scenario names
	 *
	 * @var string
	 */
	const SCENARIO_SAVING_INCOMING_ASSET	= 'savingIncomingAsset';   // turned ON automatically, when sourceFile is set

	/**
	 * Full path to the SOURCE file of this assets OR an instance of CUploadedFile.
	 *
	 * It is used to create the final asset by copying the file (uploaded file) to final storage.
	 * With that said, source file must be considered temporary: this value is not saved, it is just used to access the asset source.
	 *
	 * @var string | CUploadedFile
	 */
	public $sourceFile 	= null;


	/**
	 * Constructor
	 *
	 * @param string $scenario
	 */
	public function __construct($scenario="insert") {
		parent::__construct($scenario);
	}


	/**
	 * List of local files considered to be garbage to remove after whatever operations we did
	 *
	 * @var array
	 */
	private $_garbageFiles = array();

	/**
	 * Run garbage clean up or not?
	 *
	 * @var boolean
	 */
	private $_cleanGarbage = true;
	private $_confParams = array();

	function init() {
		parent::init();
		// Read options from Yii "params" config
		if (isset(Yii::app()->params['imageVariants'])) {
			$this->_confParams = Yii::app()->params['imageVariants'];
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_asset';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, scope', 'required'),

			array('filename, original_filename', 'required', 'except' => array(self::SCENARIO_SAVING_INCOMING_ASSET)),

			array('scope', 'numerical', 'integerOnly' => true),
			array('filename, original_filename', 'length', 'max' => 512),
			array('type', 'length', 'max' => 32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, filename, original_filename, type, scope', 'safe', 'on' => 'search'),

			// Non-database attributes rules
			array('sourceFile', 'validateSourceFile'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'filename' => 'File name',
            'original_filename' => 'Original Filename',
            'type' => 'Type',
            'scope' => 'Scope',
			'sourceFile' => 'Asset source file',
        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('filename',$this->filename,true);
		$criteria->compare('original_filename',$this->original_filename,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('scope',$this->scope);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreAsset the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Add a file to garbage collection, subject to clean up
	 * @param unknown $filePath
	 */
	private function registerGarbageFile($filePath) {
		$this->_garbageFiles[] = $filePath;
	}

	/**
	 * Run a garbage clean up - removing files registered into garbage collection
	 */
	private function removeGarbage() {
		if (!$this->_cleanGarbage)
			return;

		foreach ($this->_garbageFiles as $filePath) {
			FileHelper::removeFile($filePath);
		}
	}



	/**
	 * Generate ONE type of shared images (e.g. "Course Logo", "Player BG",....).
	 * This is because they are located under different folders in /themes
	 *
	 * @param string $type  Image type (see top of the class for constants)
	 * @param string $relativePath The relative path to Themes based images (see $systemImagesPaths). Relative to /themes/<theme>/images !!!
	 *
	 * @throws CException
	 */
	protected function generateSharedImagesFromPath($type, $relativePath) {

		$options = array('fileTypes' => array('jpg', 'gif', 'png', 'jpeg'));

		// Shared images path (the source in /themes/.../images or somewhere else, depending on $type)
		if ($type == self::TYPE_GAMIFICATION_BADGES) {
			$sourcePath = Docebo::getRootBasePath() . DIRECTORY_SEPARATOR
							. 'plugins' . DIRECTORY_SEPARATOR . 'GamificationApp' . DIRECTORY_SEPARATOR
							. 'assets'  . DIRECTORY_SEPARATOR
							. 'images'  . DIRECTORY_SEPARATOR
							. $relativePath;
		}
		else {
			$sourcePath = Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $relativePath;
		}

		$sourcePath = str_replace(array('/','\\'), DIRECTORY_SEPARATOR, $sourcePath);


		// Upload-Tmp folder, local
		$tmpFolder = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $relativePath;
		$tmpFolder = str_replace(array('/','\\'), DIRECTORY_SEPARATOR, $tmpFolder);

		// Then create the folder again and copy all files in-there for further processing
		@mkdir($tmpFolder, 0777, true);
		@chmod($tmpFolder, 0777);
		if (is_dir($sourcePath) || is_dir($tmpFolder) ) {

			// Copy images from /themes/.../images/<type>
			FileHelper::copyDirectory($sourcePath, $tmpFolder, $options);

			// Ok, enumerate all files in the folder and rename them
			// $files = CFileHelper::findFiles($tmpFolder, $options);
			$files = FileHelper::findFiles($tmpFolder, $options);
			foreach ($files as $file) {
				$pathInfo = pathinfo($file);
				$newFileName = md5($relativePath . "\\" . $pathInfo['basename']);
				$newFile = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $newFileName . "." . $pathInfo['extension'];
				@rename($file, $newFile);
			}

			// Get a storage manager for SYSTEM "files" folder
			$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS);

			// Now, copy all files to respective folders into "assets/<type>", into the SYSTEM (!!!) "files"!  NOT Domain specific one!!!!!
			// Clean the folder first
			$storage->removeFolder($type);

			// Get image variants from Yii config
			$imageVariants = $this->getImageVariants($type);

			// Enumerate all variants for this type, converting and copying the whole folder
			foreach ($imageVariants as $variantName => $dummy) {
				$this->convertFolder($tmpFolder, $type, $variantName);
				$storage->storeAs($tmpFolder . DIRECTORY_SEPARATOR . $variantName, $type . DIRECTORY_SEPARATOR . $variantName);
			}

			// And again, to keep things clean, remove the temporary folder
			FileHelper::removeDirectory($tmpFolder);

		}
		else {
			$message = 'Failed shared images pre-generation: Type=' . $type . " , Source Path=" . $sourcePath;
			Yii::log($message, 'error', __METHOD__ . "(" . __LINE__ . ")");
		}


	}


	/**
	 * Run Imagemagick Mogrify in batch mode, converting the whole folder, IN PLACE!
	 *
	 * @param string $path  LOCAL folder to convert
	 * @param string $type  Asset type (see self::TYPE_* constants)
	 * @param string $variant Conversion variant name (small | micro | ...)
	 * @return boolean:
	 */
	protected function convertFolder($path, $type, $variant) {

		// Base command, having really common options
		$command = '"mogrify" -gravity "center" -background "transparent" -path ' . $path  . DIRECTORY_SEPARATOR . $variant;
		@mkdir($path  . DIRECTORY_SEPARATOR . $variant, 0777, true);
		@chmod($path  . DIRECTORY_SEPARATOR . $variant, 0777);

		// Additional options from Yii config, if any
		$command .= ' ' . $this->getImageVariantOptions($type, $variant) . " ";

		$command .= ' ' . $path . DIRECTORY_SEPARATOR . "*";
		$command .= ' 2>&1';

		$output = array();
		exec($command, $output);

		if (!empty($output)) {
			Yii::log(var_export($output,true), CLogger::LEVEL_ERROR);
		}

		return true;

	}


	protected function convertSourceFile($variant) {

		$convertedFilesPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . 'converted';
		@mkdir($convertedFilesPath, 0777, true);
		@chmod($convertedFilesPath, 0777);

		// Base command, having really common options
		$command = '"mogrify" -gravity "center" -background "transparent" -path ' . $convertedFilesPath;

		// Additional options from Yii config, if any
		$command .= ' ' . $this->getImageVariantOptions($this->type, $variant) . " ";

		$command .= ' ' . $this->sourceFile;
		$command .= ' 2>&1';

		$output = array();
		exec($command, $output);

		if (!empty($output)) {
			Yii::log(var_export($output,true), CLogger::LEVEL_ERROR);
		}

		return $convertedFilesPath . DIRECTORY_SEPARATOR . basename($this->sourceFile);


	}

	/**
	 * Smart resize and crop images per predefined dimensions.
	 * @param string $variant
	 * @return boolean
	 */
	public function _cropSourceFile($variant) {
		$convertedFilesPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . 'converted';
		@mkdir($convertedFilesPath, 0777, true);
		@chmod($convertedFilesPath, 0777);

		/* @var $imageMagick ImageMagickConverter */
		$imageMagick = Yii::app()->imageMagick;
		$imageMagick->setSourcePath($this->sourceFile);
		$imageMagick->setDestinationPath($convertedFilesPath . DIRECTORY_SEPARATOR . basename($this->sourceFile));
		$imageMagick->setWidth($this->getImageVariantWidtht($variant));
		$imageMagick->setHeight($this->getImageVariantHeight($variant));
		$imageMagick->setCropAfterResize(true);
		$imageMagick->setQuality(100);
		if ($imageMagick->resize()) {
			return $imageMagick->getDestinationPath();
		} else {
			return false;
		}
	}

	/**
	 * Return configured Image Variants for a given asset type (see /common/config/parts/params.php)
	 *
	 * @param string $type
	 * @return array
	 */
	protected function getImageVariants($type) {
		$result = array();
		if (!isset(Yii::app()->params['imageVariants']) || !is_array(Yii::app()->params['imageVariants']) || !isset(Yii::app()->params['imageVariants'][$type])) {
			return $result;
		}
		return Yii::app()->params['imageVariants'][$type];
	}


	/**
	 * Return a string of options for "mogrify". Reads options from Yii params config
	 *
	 * @param string $type  Asset type (see TYPE_* constants in this model)
	 * @param string $variant  small | micro
	 * @return string
	 */
	protected function getImageVariantOptions($type, $variant) {

		$options = "";

		// Read options from Yii "params" config
		if (isset(Yii::app()->params['imageVariants'])) {
			$imConfig = Yii::app()->params['imageVariants'];
		}
		else {
			return $options;
		}

		// Check if we have options for the requested $type and $variant
		if (isset($imConfig[$type][$variant])) {

			// SIZE
			if (isset($imConfig[$type][$variant]['size'])) {
				$size = $imConfig[$type][$variant]['size'];
				/*if ($type == self::TYPE_LOGIN_BACKGROUND && $variant == self::VARIANT_ORIGINAL && Settings::get('catalog_external', 'off') == 'on') {
					$size = '1156x1156';
				}*/
				$options .= ' -thumbnail "' . $size . '" ';
			}

			// COLORSPACE
			if (isset($imConfig[$type][$variant]['colorspace'])) {
				$options .= ' -colorspace "' . $imConfig[$type][$variant]['colorspace'] . '" ';
			}

			// DEPTH
			if (isset($imConfig[$type][$variant]['depth'])) {
				$options .= ' -depth "' . $imConfig[$type][$variant]['depth'] . '" ';
			}

			// TYPE
			if (isset($imConfig[$type][$variant]['format'])) {
				$options .= ' -format "' . $imConfig[$type][$variant]['format'] . '" ';
			}



		}


		return $options;

	}

	private function _getImageVariantDimensions($variant) {
		try {
			$options = $this->_getVariantOptions($variant);
			if (isset($options['size'])) {
				return explode('x', $options['size']);
			}
			return array();
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	/**
	 * Return predefined height from params.php file
	 * @param string $variant
	 * @return string|int|null
	 */
	function getImageVariantHeight($variant) {
		try {
			$dimension = $this->_getImageVariantDimensions($variant);
			return !empty($dimension) ? $dimension[1] : null;
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return null;
		}
	}

	/**
	 * Return predefined width from params.php conf file
	 * @param string $variant
	 * @return string|int|null
	 */
	function getImageVariantWidtht($variant) {
		try {
			$dimension = $this->_getImageVariantDimensions($variant);
			return !empty($dimension) ? $dimension[0] : null;
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return array();
		}
	}

	/**
	 * Get all options for type
	 * @param string $type
	 * @return array
	 */
	private function _getVariantOptions($variant, $type = false) {
		if (!$type) {
			$type = $this->type;
		}
		if ($this->_isExistingVariant($variant, $type)) {
			return $this->_confParams[$type][$variant];
		} else {
			return array();
		}
	}

	/**
	 * Check for options about image variant defined in params.php conf file
	 * @param string $type
	 * @param string $variant
	 * @return boolean
	 */
	private function _isExistingVariant($variant, $type = false) {
		if ($type == false) {
			$type = $this->type;
		}
		if (isset($this->_confParams[$type][$variant])) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Populate single type of images
	 *
	 * @param CFileStorage $storage
	 * @param string  $type  Image type, e.g. Course logo,  Logi background, etc.
	 */
	protected function populateOneSharedType($storage, $type) {

		// Get list of files in the respective folder
		// Images MUST have at least one variant: 'original'
		$list =  $storage->findFiles($type . '/' . self::VARIANT_ORIGINAL);

		foreach ($list as $filePath) {

			$pathInfo = pathinfo($filePath);
			$filename = $pathInfo['basename'];

			// Searching is canse SENSITIVE!! Be aware,  jpg is NOT equeal to JPG !!!
			$found = self::model()->findByAttributes(array(
					'filename' 	=> $filename,
					'type'		=> $type,
					'scope'		=> self::SCOPE_SHARED,

			));

			// Skip files that we already have in the table, which, among other things,
			// preserves the ID of the file that MIGHT be already used/selected across the LMS settings!!
			// This is important!!!
			if (!$found) {
				$model = new self();
				$model->attributes = array(
						'filename' 				=> $filename,
						'original_filename' 	=> $filename,
						'type' 					=> $type,
						'scope'					=> self::SCOPE_SHARED,
				);
				// Skip validation, we are pretty sure everything is ok
				$model->save(false);
			}

		}


	}


	/**
	 * Return URL to a given asset, identified by its ID
	 *
	 * @param number $id
	 * @param string $variant  (optional) Has meaning for *image* asset types only (see variant constants)
	 *
	 * @return string
	 */
	public static function url($id, $variant=false, $checkFile = false) {
		$url = '';
		$model = self::model()->findByPk($id);
		if ($model) {
			$url = $model->getUrl($variant, $checkFile);
		}
		return $url;
	}


	/**
	 * NOTE: THIS should be called ONCE for the WHOLE LMS RELEASE! Not per LMS!!
	 * And re-run whenever new shared images are added in /themes.
	 *
	 * Generate all shared images
	 *
	 * The final result is:  having all types of shared images (see self::$systemImagesPaths)
	 * copied to <bucket>/files/assets/<type>/<original|small|micro>.
	 *
	 * Because md5() is used to rename files, same names are guaranteed on every run,
	 * as long as SOURCE path and file is the same,
	 * e.g.  images/course_tb/myfile.jpg will result into the same filename in the assets storage.
	 *
	 */
	public function generateSharedImages() {
		foreach (self::$systemImagesPaths as $type => $relativePath) {
			$this->generateSharedImagesFromPath($type, $relativePath);
		}
	}

	/**
	 * Populate Database table with all shared files available at the moment.
	 *
	 * Note: we save only ONE record per file, despite the fact that image may have MANY variants (small, micro, etc.).
	 * That is because, variant images have the SAME name, just put in different subfolders
	 *
	 * !!! This method should be run once, upon LMS installation and after that, when new SHARED images are added.
	 *
	 */
	public function populateImagesFromSharedAssets() {

		$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS);

		$this->populateOneSharedType($storage, self::TYPE_COURSELOGO);
		$this->populateOneSharedType($storage, self::TYPE_LOGIN_BACKGROUND);
		$this->populateOneSharedType($storage, self::TYPE_PLAYER_BACKGROUND);
		$this->populateOneSharedType($storage, self::TYPE_GAMIFICATION_BADGES);

	}




	/**
	 * Return CFileStorage object, depending on Asset scope (shared or private).
	 *
	 * @param string $scope Optional scope parameter, overriding model's one
	 * @return CFileStorage
	 */
	public function getStorage($scope = false) {

		if ($scope === false) {
			$scope = $this->scope;
		}

		if ($scope == self::SCOPE_SHARED) {
			$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS);
		}
		else {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
		}

		return $storage;
	}


	/**
	 * Return list of URLs to all assets, according to parameters. Asset ID is used for array keys.
	 *
	 * @param string $type  Assets type (course logo, player bg, ...)
	 * @param number $scope  Shared, Private
	 * @param number $chunkSize  Optionally chunkthe resulting array
	 * @param string $variant
	 * @return array Result list: array(<asset-id> => <url>)
	 */
	public function urlList($type, $scope, $chunkSize = false, $variant = false, $offset = null, $limit = null) {

		// Normalize variant for assets of type image, i.e. if NO variant is specified, use "original"
		if (in_array($type, self::$imageAssetTypes) && empty($variant)) {
			$variant = self::VARIANT_ORIGINAL;
		}

		// Get the storage
		$storage = $this->getStorage($scope);

		$criteria = new CDbCriteria();
		$criteria->addCondition('type=:type AND scope=:scope');
		$criteria->params = array(
				':type' 	=> $type,
				':scope' 	=> $scope,
		);

		if($offset != null && $limit){
			$criteria->offset = $offset;
			$criteria->limit = $limit;
		}elseif($limit){
			$criteria->limit = $limit;
		}

		$models = $this->findAll($criteria);

		// Return empty result if no models have been found
		if (empty($models)) {
			return array();
		}

		$subFolder = $variant ? "/" . $variant : "";

		$linearList = CHtml::listData($models, 'id', function ($data) use ($storage, $type, $subFolder) {
			return $storage->fileUrl($data->filename, $type . $subFolder);
		});

		// Chunk the array, if requested
		$finalList = $linearList;
		if ($chunkSize !== false) {
			$finalList = array_chunk($linearList, $chunkSize, true);
		}

		return $finalList;

	}



	/**
	 * Return URL to THIS asset. It is just an URL to the asset!
	 *
	 * @param string $variant Relevant only to certain types of assets (e.g. image types)
	 * @param boolean $checkFile Check if file exists before building URL. This is time consuming and expensive operation, use it wisely!
	 * @return string
	 */
	public function getUrl($variant = false, $checkFile = false) {
		// Normalize variant for assets of type image, i.e. if NO variant is specified, use "original"
		if (in_array($this->type, self::$imageAssetTypes) && empty($variant)) {
			$variant = self::VARIANT_ORIGINAL;
		}
		$storage = $this->getStorage();
		$subFolder = $this->type . ($variant ? '/' . $variant : '');

		// Should we check the file first ?
		if ($checkFile && !$storage->fileExists($this->filename, $subFolder)) {
				return '';
		}
		$url = $storage->fileUrl($this->filename, $subFolder);
		return $url;

	}


	/**
	 * Delete file(s) from storage associated with this model. Should be called in afterDelete()
	 */
	public function cleanUp() {

		$storage = $this->getStorage();

		// Try to remove the file from "assets/<asset-type>", if it has no "variants"
		$storage->remove($this->filename, $this->type);

		// We enumerate ALL possible variants, no matter the type of the document, just to make our live easier.
		// The worst scenario is to try to delete file(s) that does not exists which is ok.
		// This is not the best approach! It involves hard coding an union of all possible asset variants,
		// but for now it is ok.
		foreach (array_unique(CMap::mergeArray(self::$possibleVariants, array_keys($this->getImageVariants($this->type)))) as $variant) {
			$subFolder = $this->type . "/" . $variant;
			$storage->remove($this->filename, $subFolder);
		}
		return true;
	}


	/**
	 * @see CModel::beforeValidate()
	 */
	public function beforeValidate() {

		// Stop if parent validation fails
		if (!parent::beforeValidate()) {
			return false;
		}

		// We must be sure model has an asset type
		// Validate TYPE: it must be one of allowed
		$this->type = $this->type ? $this->type : self::TYPE_GENERAL_ASSET;
		
		$typeParts = preg_split('/\//', $this->type);;
		if (!is_array($typeParts) || !isset($typeParts[0])) {
			return false;
		}
		
		if (!in_array($typeParts[0], self::$allowedTypes)) {
			$this->addError('type', Yii::t('standard', 'Unknown asset type') . ' "' . $this->type . '" ');
			return false;
		}

		// By default, assets should be private to LMS
		if (!$this->scope) {
			$this->scope = self::SCOPE_PRIVATE;
		}

		// If sourceFile is set, switch model to this scenario, which is handled
		// differently in terms of validation rules (see $this->rules())
		if ($this->sourceFile) {
			$this->scenario = self::SCENARIO_SAVING_INCOMING_ASSET;

			// Check if incoming file is an instance of CUploadedFile, meaning we are asked to save asset out of a just uploaded file
			// Save the incoming file to our own temporary space where we would be able to manipulate, and let the model use it.
			if ($this->sourceFile instanceof CUploadedFile) {
				$fileName = $this->sourceFile->getName();
				$storage = CFileStorage::getLocalTempDomainStorage(CFileStorage::COLLECTION_ASSETS);
				$storage->storeAs($this->sourceFile->getTempName(), $fileName, 'tmp');
				$this->sourceFile = $storage->path() . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $fileName;

				// Register this file for garbage cleaning later
				$this->registerGarbageFile($this->sourceFile);
			}

		}

		return true;

	}

	/**
	 * Responsible for handling the SOURCE file, including, copy, rename, convert and upload to final storage.
	 *
	 * Note: name of the file is randomized (SHA1 of a random UUID) to guarantee uniqueness across the LMS, at least.
	 *
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {

		if (!parent::beforeSave()) {
			return false;
		}


		// Handle SCENARIO: SCENARIO_SAVING_INCOMING_ASSET
		// Looks like we are going to create/save an asset
		if ( $this->scenario == self::SCENARIO_SAVING_INCOMING_ASSET ) {

			// This scenario REQUIRES sourceFile to be set AND file to exist!
			if (!$this->sourceFile || !is_file($this->sourceFile)) {
				$this->addError('sourceFile', Yii::t('standard', 'Missing or invalid asset source file'));
				return false;
			}

			if (!$this->isNewRecord) {
				$this->cleanUp();
			}

			// Get Path information about the source file
			$pathinfo = pathinfo($this->sourceFile);

			// Set original filename (the full path? Or maybe just the name, for reference)
			if (!preg_match('#^'.self::TYPE_SUBTITLES.'#', $this->type)) { // Skip this for subtitles type
				$this->original_filename = $pathinfo['basename'];
			}

			// Copy the file in our TMP folder and rename it to hashed name, same extension
			// Switch the sourceFile to it, just forgetting about the REAL source file
			$newFilename = Docebo::randomHash() . "." . $pathinfo['extension'];
			$newFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $newFilename;
			@copy($this->sourceFile, $newFilePath);
			$this->sourceFile=$newFilePath;

			$this->filename = $newFilename;

			$storage = $this->getStorage();

			// If it is an image, convert it for all variants or do whatever else is required to do with the file of its type
			switch ($this->type) {
				case self::TYPE_COURSELOGO:
				case self::TYPE_LO_IMAGE:
				case self::TYPE_PLAYER_BACKGROUND:
				case self::TYPE_LOGIN_BACKGROUND:
				case self::TYPE_GAMIFICATION_BADGES:
				case self::TYPE_GENERAL_IMAGE:
                case self::TYPE_GAMIFICATION_REWARDS:
					// Enumerate all variants for this type, converting and saving to storage
					$imageVariants = $this->getImageVariants($this->type);
					foreach ($imageVariants as $variantName => $dummy) {
						$convertedFilePath = $this->convertSourceFile($variantName);
						$subFolder = $this->type . "/" . $variantName;
						$storage->store($convertedFilePath, $subFolder);
						FileHelper::removeFile($convertedFilePath);
					}
					break;

				case self::TYPE_APP7020:
				case self::TYPE_APP_ICON:
					// Enumerate all variants for this type, converting and saving to storage
					$imageVariants = $this->getImageVariants($this->type);
					foreach ($imageVariants as $variantName => $dummy) {
						$sourcePath = $this->_cropSourceFile($variantName);
						$subFolder = $this->type . "/" . $variantName;
						$storage->store($sourcePath, $subFolder);
						FileHelper::removeFile($subFolder);
					}
					break;


				case self::TYPE_GENERAL_ASSET:
				case self::TYPE_SUBTITLES:  // just to remind us that subtitles/123456789 goes to "default"					
				default:
					// !! $this->sourceFile is already renamed (hashed)
					$storage->store($this->sourceFile, $this->type);
					break;

			}

			// Remove files and reset file to null.
			FileHelper::removeFile($this->sourceFile);
			FileHelper::removeFile($newFilePath);
			$this->sourceFile = null;


		}


		return true;

	}


	/**
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {

		// Call parent's method
		parent::afterDelete();


		// Cleanup
		$this->cleanUp();

	}


	/**
	 * Low level asset source file validation using common component FileTypeValidator
	 *
	 * @param string $attribute
	 * @param array $params
	 */
	public function validateSourceFile($attribute, $params) {

		// Use one of standard validator types. If we need MORE, add validators to FileTypeValidator!
		$validatorType = in_array($this->type, self::$imageAssetTypes) ? FileTypeValidator::TYPE_IMAGE : FileTypeValidator::TYPE_ITEM;

		// Validate...
		$validator = new FileTypeValidator($validatorType);
		$valid = $validator->validateLocalFile($this->sourceFile);

		// Set error if not valid
		if (!$valid) {
			Yii::log('Asset file validation failed: ' . $validator->errorMsg, 'error', __METHOD__);
			$this->addError('sourceFile', Yii::t('standard', 'Asset file validation failed: ' . $validator->errorMsg));
		}

	}


	/**
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {
		parent::afterSave();

		// Remove all files registered as garbage
		$this->removeGarbage();
	}


}
