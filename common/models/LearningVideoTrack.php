<?php

/**
 * This is the model class for table "learning_video_track".
 *
 * The followings are the available columns in table 'learning_video_track':
 * @property integer $idTrack
 * @property integer $idReference
 * @property integer $idResource
 * @property integer $idUser
 * @property string $status
 * @property integer $total_time
 * @property integer $accesses
 * @property integer $bookmark
 */
class LearningVideoTrack extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningVideoTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_video_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('idReference, idResource, idUser, status, total_time, accesses', 'required'),
			array('idReference, idResource, idUser, status', 'required'),
			array('idReference, idResource, idUser, total_time, accesses', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>255),
            array('bookmark','type','type'=>'float'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idReference, idResource, idUser, status, total_time, accesses, bookmark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idReference' => 'Id Reference',
			'idResource' => 'Id Resource',
			'idUser' => 'Id User',
			'status' => 'Status',
			'total_time' => 'Total Time',
			'accesses' => 'Accesses',
            'bookmark'    =>  'Time bookmark'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idResource',$this->idResource);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('total_time',$this->total_time);
		$criteria->compare('accesses',$this->accesses);
        $criteria->compare('bookmark',$this->bookmark);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function behaviors() {
		return array(
			'commonTrack' => array(
				'class' => 'CommontrackBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_VIDEO,
				'idResourcePropertyName' => 'idResource',
				'idUserPropertyName' => 'idUser',
				'idOrgPropertyName' => 'idReference',
				'statusPropertyName' => 'status',
			)
		);
	}

    public function updateBookmark($bookmark){
        $this->bookmark = $bookmark;
        if (!$this->save()) {
            throw new CException("Error while saving track info");
        }
    }

}