<?php

/**
 * This is the model class for table "core_multidomain_webpage".
 *
 * The followings are the available columns in table 'core_multidomain_webpage':
 * @property integer $id
 * @property integer $id_multidomain
 * @property integer $id_webpage
 *
 * The followings are the available model relations:
 * @property CoreMultidomain $idMultidomain
 * @property LearningWebpage $idWebpage
 */
class CoreMultidomainWebpage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_multidomain_webpage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_multidomain, id_webpage', 'required'),
			array('id_multidomain, id_webpage', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_multidomain, id_webpage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMultidomain' => array(self::BELONGS_TO, 'CoreMultidomain', 'id_multidomain'),
			'idWebpage' => array(self::BELONGS_TO, 'LearningWebpage', 'id_webpage'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_multidomain' => 'Id Multidomain',
			'id_webpage' => 'Id Webpage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_multidomain',$this->id_multidomain);
		$criteria->compare('id_webpage',$this->id_webpage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreMultidomainWebpage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
