<?php

/**
 * This is the model class for table "learning_htmlpage".
 *
 * The followings are the available columns in table 'learning_htmlpage':
 * @property integer $idPage
 * @property string $title
 * @property string $textof
 * @property integer $author
 */
class LearningHtmlpage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningHtmlpage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_htmlpage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('textof', 'required'),
			array('author', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>150),
			array('title', 'safe'),
			array('textof', 'checkIframeSources'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPage, title, textof, author', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPage' => 'Id Page',
			'title' => 'Title',
			'textof' => 'Textof',
			'author' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPage',$this->idPage);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('textof',$this->textof,true);
		$criteria->compare('author',$this->author);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'learningObjects' => array(
				'class' => 'LearningObjectsBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_HTMLPAGE,
				'idPropertyName' => 'idPage',
				'titlePropertyName' => 'title'
			)
		);
	}


	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @param bool $organization
	 * @throws CException
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {
		$copy = new LearningHtmlpage();
		$copy->title = $this->title;
		$copy->textof = $this->textof;
		$copy->author = Yii::app()->user->id;
		if (!$organization) {
			$copy->disableLearningObjectsEvents(); //this is important, otherwise it will break DB
		}
		if ($copy->save()) {
			if (!$organization) { $copy->enableLearningObjectsEvents(); }
			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.print_r($copy->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}

	/**
	 * checkIframeSources() - checks, whatever LO Html page content has iframe(-s) and checks if their sources are whitelisted
	 * @param $attribute
	 */
	public function checkIframeSources($attribute) {
		//$searchIframes = preg_match_all('%<iframe[A-Z0-9a-z\.\,\!\?\-\_\s\=\"\'\:\/\%]+>[A-Z0-9a-z\.\,\!\?\-\_\s\=\"\'\:\/\%]*</iframe>%im', $this->$attribute, $matches);
		$searchIframes = preg_match_all('%<iframe[A-Z0-9a-z\.\,\!\?\-\_\s\=\"\'\:\/\%]+>%im', $this->$attribute, $matches);

		// If there are iframes in the description proceed
		if(isset($matches) && is_array($matches) && count($matches) > 0 && isset($matches[0]) && count($matches[0]) > 0) {
			$matches = $matches[0];

			// Try to get custom defined iframe sources(urls)
			$customIframeUrls = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
			$addIframeUrlRules = '';
			if($customIframeUrls) {
				$allcustomIframeUrls = CJSON::decode($customIframeUrls->whitelist);
				foreach($allcustomIframeUrls as $customIframeUrl) {
					$customIframeUrl = preg_replace('%^(http://|https://)%', '', $customIframeUrl);
					$addIframeUrlRules .= '|'.$customIframeUrl .'$';

					// after slash
					if(!preg_match('%/$%', $customIframeUrl)) {
						$addIframeUrlRules .= '|'.$customIframeUrl . '/[\w]*';
					} else {
						$addIframeUrlRules .= '|'.$customIframeUrl . '[\w]*';
					}

					// after question mark
					if(!preg_match('%\?$%', $customIframeUrl)) {
						$addIframeUrlRules .= '|'.$customIframeUrl . '\?[\w]*';
					} else {
						$addIframeUrlRules .= '|'.$customIframeUrl . '[\w]*';
					}

				}
			}

			$newIframeRules = Yii::app()->params['embed_whitelist'];
			if($addIframeUrlRules != '') {
				$newIframeRules = substr($newIframeRules, 0, -2);
				$newIframeRules .= $addIframeUrlRules . ')%';
			}

			// Go through all of the iframes found
			foreach($matches as $matchedIframe) {
				// try to get the current iframe source in order to check if it exists in the whitelist !!!
				$matchedIframeSource = preg_replace('%<iframe[A-Z0-9a-z\.\,\!\?\-\_\s\=\"\'\:\/\%]*src=%im', '', $matchedIframe);
				$matchedIframeSource = substr($matchedIframeSource, 1);
				$matchedIframeSource = preg_replace('%\".*%im', '', $matchedIframeSource);
				$matchedIframeSource = preg_replace('%\'.*%im', '', $matchedIframeSource);

				// Check is the iframe whitelisted !!!
				$checkIframeSource = preg_match($newIframeRules.'im', $matchedIframeSource, $mmm);

				// if not whitelisted add error
				if ($checkIframeSource != 1) {
					$this->addError($attribute, Yii::t('course', 'Your iframe source URL is not listed in the whitelist!'));
				}
			}
		}
	}
}