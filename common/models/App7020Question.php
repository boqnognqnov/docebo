<?php

/**
 * This is the model class for table "app7020_question".
 *
 * The followings are the available columns in table 'app7020_question':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $idContent
 * @property integer $idLearningObject
 * @property integer $idUser
 * @property integer $viewCounter
 * @property integer $open
 * @property string $created
 *
 * The followings are the available model relations:
 * @property App7020Answer[] $answers
 * @property CoreUser $user
 * @property App7020QuestionFollow[] $questionFollows
 * @property App7020Content $contributiuon
 * 
 */
class App7020Question extends CActiveRecord {

	/**
	 * Scenario sned notification about new question
	 */
	const SCENARIO_NEW_QUESTION_NOTIFICATION = 'new_question';
	// It is used for validation of "Add new question" and "Edit question"
	const MIN_CHARACTERS = 30;

	/**
	 * Scenario sned notification about new content question
	 */
	const SCENARIO_NEW_CONTENT_QUESTION_NOTIFICATION = 'new_content_question';

	/**
	 * Scenario sned notification about new question
	 */
	const SCENARIO_NO_NOTIFICATION = 'no_notification';

	/**
	 * Scenario sned notification about new question
	 */
	const SCENARIO_OPEN_CLOSE = 'open_close';

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser', 'required'),
			// for "saveQuestion" and "saveQuestionDialog" Scenario for validation is used custom validation - validate_question_length
			array('title', 'required', 'except' => 'saveQuestion, saveQuestionDialog'),
			array('idUser, idContent, viewCounter', 'numerical', 'integerOnly' => true),
			array('content', 'safe'),
			// The following rule is used by search().
			array('id, title, content, idUser, idContent, viewCounter, open, created', 'safe', 'on' => 'search'),
			array('title', 'validate_question_length', 'on' => 'update, saveQuestion, saveQuestionDialog, insert'),
			array('contribution', 'validate_content', 'on' => 'saveQuestion'),
			array('idContent', 'exist', 'on' => 'saveQuestion', 'className' => 'App7020Assets', 'attributeName' => 'id', 'message' => Yii::t('app7020', 'Problem with question data')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'App7020Answer', 'idQuestion'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'questionFollows' => array(self::HAS_MANY, 'App7020QuestionFollow', 'idQuestion'),
			'contribution' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'content' => 'Content',
			'idUser' => 'Id User',
			'idContent' => 'Id Content',
			'viewCounter' => 'View Counter',
			'open' => 'Status (OPEN)',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('viewCounter', $this->viewCounter);
		$criteria->compare('open', $this->open);
		$criteria->compare('created', $this->created, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function beforeSave() {
		$this->title = Yii::app()->htmlpurifier->purify($this->title);
		$this->idUser = (!$this->idUser) ? Yii::app()->user->id : $this->idUser;
		return parent::beforeSave();
	}

	public function afterSave() {
		Yii::app()->event->raise(EventManager::EVENT_APP7020_QUESTION_UPDATED, new DEvent($this, array(
			'model' => $this,
		)));

		return parent::afterSave();
	}

	/**
	 * It validates if id content is provided
	 * @param type $attribute
	 * @param type $params
	 */
	public function validate_content($attribute, $params) {
		if (!$this->idContent) {
			$this->addError('title', Yii::t('app7020', 'Id content is mandatory'));
		}
	}

	/**
	 * Check Question length
	 * @param type $attribute
	 * @param type $params
	 */
	public function validate_question_length($attribute, $params) {
		// Validate max characters of question
		$limit = CoachApp7020Settings::getLimitCharacters();
		if ($limit != false) {
			if ($limit < self::MIN_CHARACTERS) {
				$limit = self::MIN_CHARACTERS;
			}
			if (mb_strlen(App7020Helpers::getPureText($this->title)) > $limit) {
				$this->addError('title', Yii::t('app7020', 'The  text length can not be more than {maxCharacters} characters', array('{maxCharacters}' => $limit)));
			}
		}

		// Validated min characters of Question
		if (mb_strlen(App7020Helpers::getPureText($this->title)) < self::MIN_CHARACTERS) {
			$this->addError('title', Yii::t('app7020', 'The  text length can not be less than {minCharacters} characters', array('{minCharacters}' => self::MIN_CHARACTERS)));
		}
	}

	/**
	 * Check if user is owner of Question. If not can not manage Question
	 * @param type $attribute
	 * @param type $params
	 */
	public function validate_question_owner($attribute, $params) {
		if (Yii::app()->user->getId() != $this->idUser) {
			$this->addError('title', Yii::t('app7020', 'Authorization error'));
		}
	}

	public static function getQuestionById($id = false) {
		$data = array();
		if ($id) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select('q.*, u.userid, u.firstname, u.lastname, u.avatar');
			$commandBase->from(App7020Question::model()->tableName() . ' q');
			$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'q.idUser = u.idst');
			$commandBase->where('id = :id', array(':id' => $id));
			$data = $commandBase->queryRow(true);
		}
		return $data;
	}

	/**
	 * 
	 * @param int $id
	 * @param bool $onlyBestAnswer
	 * @return array  return Best Answer if have or last answer or (bool) False
	 */
	public static function getLastAnswerByQuestionId($id = false, $onlyBestAnswer = false, $onlyLastAnswer = false) {
		$data = array();
		if ($id) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select('a.*, u.userid, (Select count(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 1) as likes, (Select count(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 2) as dislikes');
			$commandBase->from(App7020Answer::model()->tableName() . ' a');
			$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'a.idUser = u.idst');
			$commandBase->where('idQuestion = :id', array(':id' => $id));
			if ($onlyBestAnswer) {
				$commandBase->andWhere('bestAnswer = 2');
			}
			if ($onlyLastAnswer) {
				$commandBase->andWhere('bestAnswer = 1');
			}
			$commandBase->order('bestAnswer DESC, created DESC');
			$data = $commandBase->queryRow();
		}

		return $data;
	}

	/**
	 * Make SQL Query and Configurate ComboListView
	 * @param text $method
	 * @example 'onlyMy' / 'onlyMyFollow' / default: false (All)
	 * @return \CSqlDataProvider
	 */
	public static function sqlDataProvider($method = false, $customConfig = array(), $forceParams = array(), $referer = false) {
		$statusOpen = Yii::app()->request->getParam('radioSwitch', false);
		$filter = Yii::app()->request->getParam('mainFilter', false);
		$mainFilter = !empty($filter) ? Yii::app()->request->getParam('mainFilter', false) : $forceParams['sessionMainFilterAskGuru'][$referer];

		$searchInput = trim(Yii::app()->request->getParam('search_input', false));

		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;
		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		$contentId = (!empty($customConfig['contentId'])) ? $customConfig['contentId'] : false;
		$loId = (!empty($customConfig['loId'])) ? $customConfig['loId'] : false;
		$onlyNew = (!empty($customConfig['onlyNew'])) ? $customConfig['onlyNew'] : false;

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Question::model()->tableName() . ' q');
		$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'q.idUser = u.idst');
		$commandBase->leftJoin(App7020Assets::model()->tableName() . ' a', 'q.idContent = a.id');
		$commandBase->leftJoin(App7020ChannelAssets::model()->tableName() . ' ca', 'ca.idAsset = a.id');

		$commandBase->group('q.id');


		// GET ONLY MY OR MY FOLLOWS CONTENTS
		if (in_array((string) $method, array('onlyMy', 'onlyMyFollow'), true)) {
			if ($method == 'onlyMy') {
				if ($statusOpen !== false) {
					$commandBase->andWhere('q.idUser = ' . Yii::app()->user->idst . ' AND q.open = ' . (int) $statusOpen);
				} else {
					$commandBase->andWhere('q.idUser = ' . Yii::app()->user->idst . ' AND q.open = 1');
				}
			}
			if ($method == 'onlyMyFollow') {
				$commandBase->leftJoin(App7020QuestionFollow::model()->tableName() . ' qF', 'q.id = qF.idQuestion');
				$commandBase->andWhere('qF.idUser = "' . Yii::app()->user->idst . '"');
			}
		}

		if ($contentId) {
			$commandBase->andWhere("q.idContent = :contentId", array(":contentId" => $contentId));
		}

		if ($loId) {
			$commandBase->andWhere("q.idLearningObject = :loId", array(":loId" => $loId));
		}

		if ($onlyNew) {
			$commandBase->andWhere("(SELECT COUNT(id) FROM app7020_answer WHERE idQuestion = q.id) = 0");
		}

		if ($searchInput) {
			$commandBase->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		// ORDERS
		$commandBase->order('created DESC');
		switch ($mainFilter['sort']) {
			case 1:
				$commandBase->order('answersCount DESC');
				break;
			case 2:
				$commandBase->order('viewCounter DESC');
				break;
		}
		// OPEN / CLOSE / NEW
		switch ($mainFilter['show']) {
			case 1:
				$commandBase->andWhere('q.open = 1');
				break;
			case 2:
				$commandBase->andWhere('q.open = 0');
				break;
			case 3:
				$commandBase->andWhere('q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)');
				break;
		}



		$loCourseID = '(SELECT `idCourse` FROM `learning_organization` WHERE `idOrg` = q.idLearningObject)';
		$commandBase->select('q.*, u.userid, u.firstname, u.lastname, u.avatar, 
		                      (Select count(id) From app7020_answer Where idQuestion = q.id) as answersCount, 
		                      (SELECT COUNT(id) FROM ' . App7020QuestionHistory::model()->tableName() . ' WHERE idQuestion = q.id) AS viewesCount, 
		                      (SELECT id FROM ' . App7020Answer::model()->tableName() . ' WHERE idQuestion = q.id ORDER BY bestAnswer DESC, created DESC LIMIT 1) as aid, 
		                      (SELECT content FROM app7020_answer WHERE id=aid) as aContent, 
							  (SELECT created FROM app7020_answer WHERE id=aid) as aDate, 
		                      (SELECT bestAnswer FROM app7020_answer WHERE id=aid) as bestAnswer,
							  (SELECT `title` FROM `app7020_content` WHERE `id` = q.idContent) as contentTitle,
							  (SELECT `title` FROM `learning_organization` WHERE `idOrg` = q.idLearningObject) as loTitle,
							  ' . $loCourseID . ' AS loCourseID');
		if (!Yii::app()->user->getIsGodadmin()) {
			$where = array();
			if (Yii::app()->user->getIsPu()) {
				$where[] = "((SELECT COUNT(`puser_id`) FROM `core_user_pu_course` WHERE `puser_id` = :userID AND `course_id` = " . $loCourseID . ") > 0)";
			}
			$where[] = "((SELECT COUNT(`idUser`) FROM `learning_courseuser` WHERE idCourse = " . $loCourseID . " AND `idUser` = :userID) > 0)";
			$where[] = "(" . $loCourseID . " IS NULL)";
			$whereString = implode(" OR ", $where);
			$commandBase->andWhere("(" . $whereString . ")", array(':userID' => Yii::app()->user->idst));


			//filter questions related to the assets, implementing channels visibility
			//$visibleChannels = App7020Channels::extractUserChannels(Yii::app()->user->idst);
			$conf_extr_channels = array(
				'idUser' => Yii::app()->user->idst,
				'idsOnly' => true
			);
			$visibleChannels = App7020Channels::extractUserChannels($conf_extr_channels, false);
			$commandBase->andWhere(' ca.idChannel IS NULL OR ca.idChannel IN (' . implode(',', $visibleChannels) . ')');
		}
		$questions = $commandBase->queryAll();

		if ($setPagination) {
			$pageSize = 5;
			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}
		$sort = new CSort();
		$config = array(
			'totalItemCount' => count($questions),
			'pagination' => $pagination,
			'keyField' => 'id',
			'sort' => $sort,
		);
		$dataProvider = new CArrayDataProvider($questions, $config);
		return $dataProvider;
	}

	/**
	 * SQL Data provider for 'My answers' Combo List View
	 * @param text $method
	 * @return \CSqlDataProvider
	 */
	public static function myAnswersSqlDataProvider($method = false, $pageSize = 5) {
		$mainFilter = Yii::app()->request->getParam('mainFilter', false);

		$commandData = Yii::app()->db->createCommand();


		$commandData->select("
								a.id as answerId,
								a.idUser as answerUserId,
								a.content as answerContent,
								a.bestAnswer,
								a.created as answerCreated,
								q.id as questionId,
								q.title as questionTitle,
								q.open as questionOpen,
								q.created as questionCreated,
								q.viewCounter as questionViewCounter,
								(SELECT count(*) FROM app7020_answer WHERE idQuestion = q.id) as answersCount,
								(SELECT count(*) FROM app7020_answer_like WHERE idAnswer = a.id AND type = 1) as likes,
								(SELECT count(*) FROM app7020_answer_like WHERE idAnswer = a.id AND type = 2) as dislikes
							");
		$commandData->from('(SELECT * FROM ' . App7020Answer::model()->tableName() . ' ORDER BY created DESC) AS a');
		$commandData->leftJoin('app7020_question q', 'q.id = a.idQuestion');
		$commandData->andWhere('a.idUser =' . Yii::app()->user->idst);
		$commandData->group('a.idQuestion');


		// FILTER: ORDERS
		switch ($mainFilter['sort']) {
			case 1:
				$commandData->order('answersCount DESC');
				break;
			case 2:
				$commandData->order('q.viewCounter DESC');
				break;
			default:
				$commandData->order('q.created DESC');
				break;
		}

		// FILTER: OPEN / CLOSE / NEW
		switch ($mainFilter['show']) {
			case 1:
				$commandData->andWhere('q.open = 1');
				break;
			case 2:
				$commandData->andWhere('q.open = 0');
				break;
			case 3:
				$commandData->andWhere('q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)');
				break;
		}



		// Make Query
		$numRecords = count($commandData->queryAll());



		$sort = new CSort();
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'id',
			'sort' => $sort,
		);
		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020Question the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public static function countMyQuestionsMyAnswersMyFollowing() {
		$criteriaMyQuestions = new CDbCriteria();
		$criteriaMyQuestions->condition = 'idUser = :idUser';
		$criteriaMyQuestions->params = array(':idUser' => Yii::app()->user->getIdst());
		$countMyQuestions = App7020Question::model()->count($criteriaMyQuestions);


		$criteriaMyAnswers = new CDbCriteria();
		$criteriaMyAnswers->condition = 'idUser = :idUser';
		$criteriaMyAnswers->params = array(':idUser' => Yii::app()->user->getIdst());
		$countMyAnswers = App7020Answer::model()->count($criteriaMyAnswers);

		$criteriaMyFollowing = new CDbCriteria();
		$criteriaMyFollowing->condition = 'idUser = :idUser';
		$criteriaMyFollowing->params = array(':idUser' => Yii::app()->user->getIdst());
		$countMyFollowing = App7020QuestionFollow::model()->count($criteriaMyFollowing);

		return array(
			'countMyQuestions' => $countMyQuestions,
			'countMyAnswers' => $countMyAnswers,
			'countMyFollowing' => $countMyFollowing,
		);
	}

	/**
	 * Calculates the answered questions of the given user for the given period
	 *
	 * @param int  requred grouping Group by days (1) or group by questions (2).
	 * @param date optional $dateFrom start of the period
	 * @param date optional $dateTo end of the period
	 * @param int optional $userId ID of the user
	 *
	 * return array|integer An array with dates as the keys and count of the answered questions as values or total count group by questions
	 */
	public static function getAnsweredQuestions($grouping, $dateFrom = null, $dateTo = null, $userId = null) {
		if (intval($grouping) > 2 || intval($grouping) < 1) {
			return false;
		}
		$grouping = intval($grouping);

		$commandBase = Yii::app()->db->createCommand();
		if ($grouping == 1) {
			$commandBase->select("COUNT(DISTINCT idQuestion) AS questionsCount, DATE(created) AS answersDate");
		} else {
			$commandBase->select("COUNT(DISTINCT idQuestion) AS questionsCount");
		}

		$commandBase->from = App7020Answer::model()->tableName();

		if (!empty($dateFrom)) {
			$commandBase->andWhere(" DATE(created) >= :dateFrom", array(':dateFrom' => date("Y-m-d", strtotime($dateFrom))));
		}

		if (!empty($dateTo)) {
			$commandBase->andWhere(" DATE(created) <= :dateTo", array(':dateTo' => date("Y-m-d", strtotime($dateTo))));
		}

		if (!empty($userId)) {
			$commandBase->andWhere(" idUser = :userId", array(':userId' => $userId));
		}

		if ($grouping == 1) {
			$commandBase->group("answersDate");
		}

		if ($grouping == 1) {
			$returnArray = array();
			$resultArray = $commandBase->queryAll(true);
			foreach ($resultArray as $value) {
				$returnArray[$value['answersDate']] = $value['questionsCount'];
			}
			return $returnArray;
		} else {
			return $commandBase->queryAll(true);
		}
	}

	/**
	 * Returns an array with questions related with selected expert
	 *
	 * @param int required $userId ID of the user.
	 * @param array optional $questionStatus the statusses of the question 1-new, 2-opened
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getExpertsQuestions($userId, $questionStatus = 1, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()) {

		if (empty($userId)) {
			return false;
		}

		$searchInput = trim(Yii::app()->request->getParam('search_input', false));


		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select(" q.*, cu.firstname, cu.lastname, cu.userid,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) as answersCount,
		                       (SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) AS viewesCount,
	                           (SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id ORDER BY created DESC LIMIT 1) as lastAnswerId,
	                           (SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id=lastAnswerId) as lastAnswerText,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id AND bestAnswer = 2) as haveBestAnswer,
		                       (SELECT COUNT(a.id) FROM " . App7020Answer::model()->tableName() . " a
	                               LEFT JOIN " . App7020QuestionHistory::model()->tableName() . " qh
	                               ON  a.idQuestion=qh.idQuestion
                                   WHERE a.idQuestion = q.id AND (a.created > qh.viewed OR ISNULL(qh.viewed))
	                               AND (qh.idUser = :userId OR ISNULL(qh.idUser))) as newAnswers");

		$commandBase->params = array(":userId" => $userId);
		$commandBase->from(App7020Question::model()->tableName() . " q");
		$commandBase->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		$commandBase->leftJoin(App7020IgnoreList::model()->tableName() . " il", $userId . " = il.idUser AND il.idObject=q.id AND il.objectType='" . App7020IgnoreList::OBJECT_TYPE_QUESTION . "'");

		$commandBase->where(" ISNULL(il.id)");

		if ($searchInput) {
			$commandBase->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		if ($questionStatus == 1) {
			$commandBase->andWhere(" (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) = 0");
		} else {
			$commandBase->andWhere(" (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) >= 0");
			//$commandBase->orWhere(" (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) = 0");
		}

		//$commandBase->order($orderField);
		$returnArray = array();

		$resultArray = $commandBase->queryAll(true);

		foreach ($resultArray as $key => $value) {
			if ($value['haveBestAnswer'] < 1) {
				$returnArray[] = $value;
			}
		}

		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);

		if (!empty($providerConfig['comboListViewId'])) {
			unset($providerConfig['comboListViewId']);
		}

		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$providerConfig['sort'] = array('attributes' => array('title'));
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			//$dataProvider->sortData();
			return $dataProvider;
		}
	}

	/**
	 * Make a record in question history table
	 * @param integer $idQuestion
	 * @return boolean
	 */
	public static function viewQuestion($idQuestion) {
		$questionHistory = new App7020QuestionHistory();
		$questionHistory->idQuestion = $idQuestion;
		$questionHistory->idUser = Yii::app()->user->idst;
		return $questionHistory->save();
	}

	/**
	 * Get the questions related to a LO
	 * @param type $loId
	 * @return array
	 */
	public static function getQuestionsByLO($loId) {
		$commandData = Yii::app()->db->createCommand();

		$commandData->select('q.*');
		$commandData->from(App7020Question::model()->tableName() . " q");
		$commandData->where("q.idLearningObject = :loId", array(':loId' => $loId));
		return $commandData->queryAll();
	}

	/**
	 * Gets all experts of selected question
	 * @param integer $idQuestion
	 * @return array of user's models
	 */
	public static function getQuestionExperts($idQuestion) {
		$allExperts = App7020Experts::model()->findAll();
		$returnArray = array();
		foreach ($allExperts as $expert) {
			$returnArray[] = CoreUser::model()->findByPk($expert->idUser);
		}
		return $returnArray;
	}

	/**
	 * Check is enable_answer_question is turned on and contributor can answer question
	 * @param integer $idQuestion
	 * @return bool
	 */
	public static function canAnswerQuestion($idQuestion) {
		$result = true;
		$question = self::getQuestionById($idQuestion);
		$idContent = intval($question['idContent']);
		$PeerReviewModel = App7020SettingsPeerReview::model()->find(array(
			'condition' => 'idContent = :idContent',
			'params' => array(':idContent' => $idContent),
		));


		$isAllowContributorAnswer = ShareApp7020Settings::isEnableAnswerQuestionsGlobal();


		$isContributor = false;
		if ($idContent > 0) {
			$content = App7020Assets::model()->findByPk($idContent);
			$isContributor = $content->isContributor;
		}
		if (!$isAllowContributorAnswer) {
			if ($isContributor && $idContent) {
				//check if the question is a request and the content satisfies the question - allow the contributor to answer the questions related to the uploaded asset
				$requestObject = App7020QuestionRequest::model()->findByAttributes(array("idContent" => $idContent, "idQuestion" => $idQuestion, "idExpert" => $content->userId));
				//if not such a request - disable answers by the contributor
				if (!$requestObject->id) {
					$result = false;
				}
			}
		}
		return $result;
	}

	public function afterDelete() {

		App7020QuestionsTags::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		App7020ChannelQuestions::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		Yii::app()->event->raise(EventManager::EVENT_APP7020_QUESTION_DELETED, new DEvent($this, array(
			'model' => $this,
		)));

		parent::afterDelete();
	}

	/**
	 * Return user <--> question visibility information,
	 * explicitly defined through Groups/Branches visibility of Channels and questions assigned to those Channels
	 *
	 * <strong><font color="red">NB 1: this method does NOT return exhausting list of visible questions from a user or users!
	 * Rather, it returns list of questions explicitly defined as visible through channels visibility to groups & branches!
	 * That's why the word "ruled" in the method name!</font></strong>
	 *
	 * <strong><font color="red">NB 2: It is assumed that if a channel is visible to a branch, it is also visible to all of its descendant branches</font></strong>
	 *
	 * @param array|int $idUsers Filter by user ID or IDs. <strong>Can be false/null, but it might be  dangerous in case of MANY users</strong>
	 * @param boolean $perUser  Return per-user list of visible question IDs
	 * @param boolean|array|integer $idQuestions IDs of questions we are interested in. This is to limit the amount of data and optimize
	 * @return array
	 */
	public static function getQuestionToUserRuledVisibility($idUsers = false, $perUser = false, $idQuestions = false) {

		// Make sure it is an array
		if ($idUsers && !is_array($idUsers)) {
			$idUsers = array((int) $idUsers);
		}

		$params = array();
		$command = Yii::app()->db->createCommand();

		// Return detailed information? Per user?
		if ($perUser) {
			$selectArray = array(
				'a.idUser as id_user',
				'group_concat(DISTINCT cq.idQuestion) AS question_list',
			);
			$command->select($selectArray);
			$command->group('a.idUser');
		}
		// Or just one long list of all Question IDs?
		else {
			$command->selectDistinct('cq.idQuestion AS idQuestion');
		}


		// Filter by user? Or just get information about ALL users ? (dangerous)
		$userFilter = "";
		if (is_array($idUsers) && !empty($idUsers)) {
			$command->andWhere(array("IN", "a.idUser", $idUsers));
			$userFilter = " AND (u.idst IN (" . implode(",", $idUsers) . ")) ";
		}

		// Get the root node of OrgChart
		$rootNode = CoreOrgChartTree::getOrgRootNode();
		$params[':idOrgRoot'] = $rootNode->idOrg;
		$subselectSql = "
		(
		SELECT
		u.idst AS idUser,
		g.idst AS idGroup,
		tree2.idOrg AS idOrgParent
		FROM core_user u
		JOIN core_group_members cgm ON cgm.idstMember=u.idst
		LEFT JOIN core_group g ON g.idst=cgm.idst
		LEFT JOIN core_org_chart_tree tree ON ((tree.idst_oc=g.idst) AND (g.groupid LIKE '/oc\_%'))
		LEFT JOIN core_org_chart_tree tree2 ON ((tree2.iLeft <= tree.iLeft) AND (tree2.iRight >= tree.iRight))
		WHERE (tree.idOrg IS NULL AND g.hidden = 'false') OR (tree.idOrg IS NOT NULL AND tree.idOrg <> :idOrgRoot)
		$userFilter
		) a
		";
		$command->from($subselectSql);

		//// ---- The block between these 2 marks was completely missing for some reason??????????
		//// It was TOPIC based, but still the same logic
		$command->leftJoin("app7020_channel_visibility cv1", "(cv1.idObject=a.idGroup AND cv1.type=1) OR (cv1.idObject=a.idOrgParent AND cv1.type=2)");

		if ($idQuestions !== false) {
			if (!is_array($idQuestions)) {
				$idQuestions = array((int) $idQuestions);
			}
			if (!empty($idQuestions)) {
				$command->join("app7020_channel_questions cq", "cq.idChannel=cv1.idChannel AND cq.idQuestion IN (" . implode(",", $idQuestions) . ")");
			} else {
				$command->join("app7020_channel_questions cq", "cq.idChannel=cv1.idChannel");
			}
		} else {
			$command->join("app7020_channel_questions cq", "cq.idChannel=cv1.idChannel");
		}

		$command->andWhere("cv1.idChannel IS NOT NULL");
		//// ----
		// echo $command->text;
		// Again, per user info?
		if ($perUser) {
			$rows = $command->queryAll(true, $params);
			$result = array();
			foreach ($rows as $row) {
				$result[$row['id_user']] = $row['question_list'];
			}
		} else {
			$result = $command->queryColumn($params);
		}

		return $result;
	}

	/**
	 * By definition and by design, if a channel has NO assigned groups or branches, all questions associated to that channel are visible to all users.
	 * Also, if a question is NOT assigned to any channel - again, it is visible to all users.
	 *
	 * @return boolean
	 */
	public function isVisibleToAll() {

		// If question is not assigned to ANY channel, it is visible to all users
		if (!App7020ChannelQuestions::model()->exists("idQuestion=:idQuestion", array(":idQuestion" => $this->id))) {
			return true;
		}

		$params = array(
			":id" => $this->id,
		);
		$command = Yii::app()->db->createCommand();
		$command->select("t.id");
		$command->from("app7020_question t");
		$command->join("app7020_channel_questions cq", "cq.idQuestion=t.id");
		$command->leftJoin("app7020_channel_visibility cv", "cq.idTopic=cv.idTopic");
		$command->andWhere("t.id=:id AND cv.id IS NULL ");

		return true && $command->queryScalar($params);
	}

	/**
	 * Check if THIS Question is visible to a given user or to current user, if user is not provided
	 *
	 * @param integer $idUser
	 */
	public function isVisibleToUser($idUser = null) {

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		// GodAdmins are ... god admins.. they see them all
		if (CoreUser::isUserGodadmin($idUser)) {
			return true;
		}

		// If this asset is explicitely set as visible through topic -> groups/branches
		$info = self::getQuestionToUserRuledVisibility($idUser, false, $this->id);
		if (!empty($info)) {
			return true;
		}

		// If anything else failed, check if this question is visible to ALL
		return $this->isVisibleToAll();
	}

	/**
	 * Get questions data by the giving options
	 * @param type $idUser - if is set, gets only user's questions
	 * @param type $params - mixed array of options (from, count, idContent, idLO, etc.)
	 */
	public static function getQuestionsDataProvider($idUser = null, $params = array()) {

		$command = Yii::app()->db->createCommand();
		$command->select("q.*, ase.id AS idContent, lo.idCourse, a.content AS bestAnswerText, a.id AS bestAnswerId, cq.idChannel, f.id AS followId, "
				. "(SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) as views, "
				. "(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) as answers, "
				. "(SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id ORDER BY id DESC LIMIT 1) as lastAnswerId, "
				. "(SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id = lastAnswerId) as lastAnswerText, "
				. "(SELECT bestAnswer FROM " . App7020Answer::model()->tableName() . " WHERE id = lastAnswerId) as lastAnswerBest");

		$command->from(App7020Question::model()->tableName() . " q");
		$command->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		$command->leftJoin(App7020QuestionFollow::model()->tableName() . " f", "q.id = f.idQuestion AND f.idUser = " . Yii::app()->user->id);
		$command->leftJoin(App7020Answer::model()->tableName() . " a", "q.id = a.idQuestion AND a.bestAnswer = 2");
		$command->leftJoin(App7020Assets::model()->tableName() . " ase", "q.idContent = ase.id");
		$command->leftJoin(LearningOrganization::model()->tableName() . " lo", "q.idLearningObject = lo.idOrg");
		$command->leftJoin(App7020ChannelQuestions::model()->tableName() . " cq", "q.id = cq.idQuestion");
		$command->leftJoin(App7020ChannelAssets::model()->tableName() . " ca", "q.idContent = ca.idAsset");

		//if (!Yii::app()->user->getIsGodadmin()) {
		//filter questions related to the assets, implementing channels visibility
		//$visibleChannels = App7020Channels::extractUserChannels(Yii::app()->user->idst);
		$conf_extr_channels = array(
			'idUser' => Yii::app()->user->idst,
            'idsOnly' => true,
            'appendEnabledEffect' => true,
		);


        $visibleChannels = App7020Channels::extractUserChannels($conf_extr_channels, false);
		$command->andWhere(' ca.idChannel IS NULL OR ca.idChannel IN (' . implode(',', $visibleChannels) . ')');
		$command->andWhere(' cq.idChannel IS NULL OR cq.idChannel IN (' . implode(',', $visibleChannels) . ')');
		//}

		if ($idUser) {
			$command->andWhere("q.idUser = :idUser", array(":idUser" => $idUser));
		}
		if ($params['followed']) {
			$command->andWhere('f.id > 0');
		}

		if ($params['idAsset']) {
			$command->andWhere("q.idContent = :contentId", array(":contentId" => $params['idAsset']));
		}

		if ($params['idLo']) {
			$command->andWhere("q.idLearningObject = :loId", array(":loId" => $params['idLo']));
		}

		if ($params['onlyNew']) {
			$command->andWhere("(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) = 0");
		}

		if ($params['searchText']) {
			$command->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $params['searchText'] . '%'));
		}

		// ORDERS
		$command->order('created DESC');
		switch ($params['sort']) {
			case 1:
				$command->order('answers DESC');
				break;
			case 2:
				$command->order('views DESC');
				break;
		}

		// OPEN / CLOSE / NEW
		switch ($params['show']) {
			case 1:
				$command->andWhere('q.open = 1');
				break;
			case 2:
				$command->andWhere('q.open = 0');
				break;
			case 3:
				$command->andWhere('q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)');
				break;
		}

		// ALL, Only with assets, only with LO
		switch ($params['type']) {
			case 1:
				$command->andWhere('q.idContent > 0');
				break;
			case 2:
				$command->andWhere('q.idLearningObject > 0');
				break;
			case 3:
				$command->andWhere('q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)');
				break;
		}

		if ($params['from'] && $params['count']) {
			$command->limit($params['from'], $params['count']);
		}

		$command->group("q.id");
		return $command->queryAll();
	}

	/**
	 * Multi-purpose SQL data provider, used by REST-API.
	 * It should be one-for-all, adding more and more filtering and so on.
	 *
	 * @param array $params Array of arbitrary parameters, usually comming from REST request
	 * @param string $idQuestionUser Filter by Question Author, return only questions created by this user
	 * @param string $idAnswerUser Filter by Answer Author, return ONLY questions having answers by this user
	 * @param string $idViewerUser Specify the VIWER user id, i.e. the one who RUNS this call (may affect access, visibility, ...)
	 * @param string $applyVisibility Apply Channel <--> Questions visibility
	 *
	 * @return CSqlDataProvider
	 */
	public static function sqlRestDataProvider($params = array(), $idQuestionUser = false, $idAnswerUser = false, $idViewerUser = false, $applyVisibility = true) {

		// Script level caching, because we  might be called several times during one PHP run
		static $cache = array();
		$cacheKey = md5(json_encode(func_get_args()));
		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}

		$dpParams = array();

		// Make sure we have Viwer/Actor
		if ($idViewerUser === false) {
			$idViewerUser = Yii::app()->user->id;
		}
		$invitations = App7020Invitations::getAllInvitationsByInvited($idViewerUser);
		if (!empty($invitations)) {
			foreach ($invitations as $idAsset) {
				$assetInvIds[] = $idAsset['id'];
			}
			$assetInvIds = implode(',', $assetInvIds);
			$invIds = 'OR ase.id IN(' . $assetInvIds . ')';
		} else {
			$invIds = '';
		}
		$dpParams[":idViewerUser"] = (int) $idViewerUser;

		// BASE Command
		$command = Yii::app()->db->createCommand();

		// Prepare base SELECT array
		$selectArray = array(
			"q.*",
			"lo.idCourse",
			"besta.content AS bestAnswerText",
			"besta.id AS bestAnswerId",
			"cq.idChannel",
			"f.id AS followId",
			"(SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) as views",
			"(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) as answers",
			"(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE (idQuestion = q.id) AND (created > DATE_SUB(NOW(),INTERVAL 1 DAY))) as newAnswersCount",
			"(q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)) AS isNew",
			"GROUP_CONCAT(DISTINCT tag.tagText) as tags",
		);

		// FROM && JOINS && SELECT adjustment
		$command->from(App7020Question::model()->tableName() . " q");
		$command->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		$command->leftJoin(App7020QuestionFollow::model()->tableName() . " f", "(q.id=f.idQuestion) AND (f.idUser=:idViewerUser)");
		$command->leftJoin(App7020Assets::model()->tableName() . " ase", "q.idContent = ase.id");
		$command->leftJoin(LearningOrganization::model()->tableName() . " lo", "q.idLearningObject = lo.idOrg");
		$command->leftJoin(App7020ChannelQuestions::model()->tableName() . " cq", "q.id = cq.idQuestion");
		$command->leftJoin(App7020ChannelAssets::model()->tableName() . " ca", "q.idContent = ca.idAsset");
		$command->leftJoin(App7020QuestionsTags::model()->tableName() . " qt", "q.id = qt.idQuestion");
		$command->leftJoin(App7020Tag::model()->tableName() . " tag", "tag.id = qt.idTag");

		// If we are filtering by answers given by a specific user, i.e. get only questions having answer(s) from this very user ($idAnswerUser)
		if ($idAnswerUser !== false) {
			$dpParams[":idAnswerUser"] = (int) $idAnswerUser;
			$command->leftJoin(App7020Answer::model()->tableName() . " besta", "q.id = besta.idQuestion AND besta.bestAnswer = 2 AND besta.idUser=:idAnswerUser");
			$command->join("app7020_answer amy", "q.id = amy.idQuestion AND amy.idUser=:idAnswerUser");
			$selectArray[] = "(SELECT id FROM app7020_answer WHERE idQuestion = q.id AND idUser=:idAnswerUser ORDER BY created DESC LIMIT 1) as lastAnswerId";
			$selectArray[] = "(SELECT content FROM app7020_answer WHERE id = lastAnswerId AND idUser=:idAnswerUser) as lastAnswerText";
			$selectArray[] = "(SELECT bestAnswer FROM app7020_answer WHERE id = lastAnswerId AND idUser=:idAnswerUser) as lastAnswerBest";
		}
		// Otherwise...
		else {
			$command->leftJoin(App7020Answer::model()->tableName() . " besta", "q.id = besta.idQuestion AND besta.bestAnswer = 2");
			$selectArray[] = "(SELECT id FROM app7020_answer WHERE idQuestion = q.id ORDER BY created DESC LIMIT 1) as lastAnswerId";
			$selectArray[] = "(SELECT content FROM app7020_answer WHERE id = lastAnswerId) as lastAnswerText";
			$selectArray[] = "(SELECT bestAnswer FROM app7020_answer WHERE id = lastAnswerId) as lastAnswerBest";
		}

		// Likes and dislikes
		$selectArray[] = "(SELECT COUNT(idAnswer) FROM app7020_answer_like WHERE idAnswer=bestAnswerId AND type=1) as bestAnswerLikes";
		$selectArray[] = "(SELECT COUNT(idAnswer) FROM app7020_answer_like WHERE idAnswer=bestAnswerId AND type=2) as bestAnswerDislikes";
		$selectArray[] = "(SELECT COUNT(idAnswer) FROM app7020_answer_like WHERE idAnswer=lastAnswerId AND type=1) as lastAnswerLikes";
		$selectArray[] = "(SELECT COUNT(idAnswer) FROM app7020_answer_like WHERE idAnswer=lastAnswerId AND type=2) as lastAnswerDislikes";


		// Finally, set the SELECT
		$command->select($selectArray);


		// filter  by question(s)
		if (isset($params["id_question"])) {
			if (is_array($params["id_question"])) {
				$command->andWhere(array("IN", "q.id", $params["id_question"]));
			} else {
				$dpParams[":idQuestion"] = (int) $params["id_question"];
				$command->andWhere("q.id=:idQuestion");
			}
		}

        // Filter questions by their visibility to CURRENT USER (the viеwer ), through channels
			// @TODO Must be checked
			$conf_extr_channels = array(
				'idUser' => Yii::app()->user->idst,
                'idsOnly' => true,
                'appendEnabledEffect' => true,
			);
			$visibleChannels = App7020Channels::extractUserChannels($conf_extr_channels, false);
			$command->andWhere(' ca.idChannel IS NULL OR ca.idChannel IN (' . implode(',', $visibleChannels) . ')');
			$command->andWhere(' cq.idChannel IS NULL OR cq.idChannel IN (' . implode(',', $visibleChannels) . ')');

		if (!$idQuestionUser && !$params["id_question"] && !Yii::app()->user->getIsGodadmin()) { // All questions
			$command->andWhere('ase.is_private = 0 OR ISNULL(ase.is_private) ' . $invIds . ' OR ase.userId = ' . Yii::app()->user->idst);
		}
		// Filter by Question Author?
		if ($idQuestionUser !== false) {
			$dpParams[":idQuestionUser"] = (int) $idQuestionUser;
			$command->andWhere("q.idUser = :idQuestionUser");
		}

		// Only followed questions?
		if ($params['followed']) {
			$command->andWhere('f.id > 0');
		}

		// Only having an associated asset ?
		if ($params['idAsset']) {
			$dpParams[":contentId"] = $params['idAsset'];
			$command->andWhere("q.idContent = :contentId");
		}

		// Only having Learning object associated?
		if ($params['idLo']) {
			$dpParams[":loId"] = $params['loId'];
			$command->andWhere("q.idLearningObject = :loId");
		}

		// Only questions, having NO answers at all?
		if ($params['onlyNew']) {
			$command->andWhere("(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) = 0");
		}

		// Or just do some search by text
		if ($params['searchText']) {
			$dpParams[":search_input"] = '%' . $params['searchText'] . '%';
			$command->andWhere("CONCAT(COALESCE(q.title,''), ' ', COALESCE(q.content,'')) LIKE :search_input");
		}

		// OTHER FILTERING
		// OPEN / CLOSED
		if ($params['open']) {
			$command->andWhere('q.open = 1');
		}
		if ($params['closed']) {
			$command->andWhere('q.open = 0');
		}

		// Created last 24 hours (1 day)
		if ($params['last_day']) {
			$command->andWhere("q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)");
		}

		// ALL, Only with assets, only with LO
		switch ($params['type']) {
			case 1:
				$command->andWhere('q.idContent > 0');
				break;
			case 2:
				$command->andWhere('q.idLearningObject > 0');
				break;
//	        case 3:
//	            $command->andWhere('q.created > DATE_SUB(NOW(),INTERVAL 1 DAY)');
//	            break;
		}

		// BEST ANSWER GIVEN (having !!! where does not work... just saying)
		if ($params['best_answer_given']) {
			$command->having("bestAnswerId > 0");
		}

		if ($params['no_best_answer_given']) {
			$command->having("bestAnswerId = 0");
		}

		// GROUPING
		$command->group("q.id");
		// Save the query builder we made so far to a Data Quiery builder (used later)
		$commandData = clone $command;

		// Create a brand new counter query and use the base SQL as FROM subquery, because it has GROUPING
		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('count(*)')->from("(" . $command->getText() . ") AS baseSQL");

		// Execute Counter query to get total number of records
		$numRecords = $commandCounter->queryScalar($dpParams);


		// ORDER (note AFTER count, no need to order before counting)
		$commandData->order("created DESC");
		switch ($params['sort']) {
			case 1:
				$commandData->order('answers DESC');
				break;
			case 2:
				$commandData->order('views DESC');
				break;
		}

		// Resolve pagination
		$pageSize = isset($params['count']) ? (int) $params['count'] : Settings::get('elements_per_page', 10);
		// If there IS "from", we do NOT use the native Data Provider pagination. Instead, we set offset/limit
		if (isset($params['from'])) {
			$pagination = false;
			$commandData->limit($pageSize, $params['from']);
		}
		// Otherwise, just use the native data provider pagination
		else {
			$pagination = array(
				'pagesize' => $pageSize,
			);
		}

		// DP config
		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'params' => $dpParams,
		);
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, $config);

		$cache[$cacheKey] = $dataProvider;

		return $dataProvider;
	}

	public static function getUsersWhichCanViewAQuestion(App7020Question $questionModel, $name) {
		$users = array();
		if ($questionModel->idContent) {
			$users = App7020Assets::getUsersWhichCanViewAnAsset($questionModel->idContent);
		} else {
			$channelId = App7020ChannelQuestions::model()->findByAttributes(array('idQuestion' => $questionModel->id))->idChannel;
			if ($channelId) {
				$users = App7020Channels::getChannelUsersIds($channelId);
			}
		}

		$command = Yii::app()->db->createCommand();
		$command->select("firstname, lastname , avatar , userid , idst");
		$command->from(CoreUser::model()->tableName());
		$command->where("concat(firstname,' ',lastname) REGEXP :name", array(":name" => '^' . $name));

		if ($users) {
			$command->andWhere(array("IN", "idst", $users));
		}
		$command->limit(5);

		$users = $command->queryAll(true);
		return $users;
	}

	public function beforeDelete() {
		parent::beforeDelete();
		App7020QuestionFollow::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		App7020QuestionHistory::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		App7020QuestionRequest::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		App7020QuestionsTags::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		App7020ChannelQuestions::model()->deleteAllByAttributes(array("idQuestion" => $this->id));
		$answers = App7020Answer::model()->findAllByAttributes(array("idQuestion" => $this->id));
		foreach ($answers as $value) {
			App7020AnswerLike::model()->deleteAllByAttributes(array("idAnswer" => $value->id));
			$value->delete();
		}

		return true;
	}

	/**
	 * Returns an array with questions related to the selected expert
	 *
	 * @param int required $userId ID of the user.
	 * @param array optional $questionStatus the statusses of the question 1-new, 2-opened
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getQuestionsToAnswer($idUser, $params) {

		if (empty($idUser)) {
			$idUser = Yii::app()->user->id;
		}
		$isAdmin = Yii::app()->user->getIsGodadmin();
		if (!isset($params["from"]))
			$params["from"] = 0;

		if (!isset($params["count"]))
			$params["count"] = 5;

		$expertsChannels = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($idUser);
		if (!$expertsChannels && !$isAdmin) {
			return array();
		}
        $custom_Select = App7020Channels::customProviderSelect(array('num_experts' => true, 'num_visibility_structures' => true, 'friendly_permissions' => true));
        $custom_channel_visibility_select = array(
            "idUser" => Yii::app()->user->idst,
            "ignoreVisallChannels" => false,
            "ignoreGodAdminEffect" => false,
            'appendEnabledEffect' => true,
            'ignoreSystemChannels' => true,
            'customSelect' => $custom_Select,
            'idsOnly' => true,
            'sort' => array(
                'channelOrdering' => array(
                    CSort::SORT_ASC => 'channelOrdering',
                    CSort::SORT_DESC => 'channelOrdering DESC',
                    'default' => CSort::SORT_ASC
                )
            ),
            'return' => 'provider',
        );
        $channels = App7020Channels::extractUserChannels($custom_channel_visibility_select);
		$command = Yii::app()->db->createCommand();
		$command->select("q.*, ase.id AS idContent, lo.idCourse, cq.idChannel, "
				. "(SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) as views, "
				. "(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) as answers, "
				. "(SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id ORDER BY id DESC LIMIT 1) as lastAnswerId, "
				. "(SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id = lastAnswerId) as lastAnswerText ");
		$command->from(App7020Question::model()->tableName() . " q");
		$command->leftJoin(App7020Assets::model()->tableName() . " ase", "q.idContent = ase.id");
		$command->leftJoin(LearningOrganization::model()->tableName() . " lo", "q.idLearningObject = lo.idOrg");
		$command->leftJoin(App7020ChannelQuestions::model()->tableName() . " cq", "q.id = cq.idQuestion");
		$command->leftJoin(App7020ChannelAssets::model()->tableName() . " ca", "q.idContent = ca.idAsset");
		$command->leftJoin(App7020IgnoreList::model()->tableName() . " il", "q.id = il.idObject AND il.objectType = '" . App7020IgnoreList::OBJECT_TYPE_QUESTION . "' AND il.idUser = " . $idUser);
        $command->andWhere('ca.idChannel IN (' . implode(',', $channels) . ') OR cq.idChannel IN (' . implode(',', $channels) . ')');
		$command->andWhere('q.open = 1');

		if ($params['onlyNew']) {
			$command->andWhere("q.created > DATE_SUB(NOW(),INTERVAL 100 DAY)");
		}

		if ($params['searchText']) {
			$command->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $params['searchText'] . '%'));
		}

		if ($params['ignored']) {
			$command->andWhere("il.id IS NOT NULL");
		} else {
			$command->andWhere("il.id IS NULL");
		}

		$command->order('created DESC');

		$command->group("q.id");
		if (!$params['all']) {
			$command->limit($params['count'], $params['from']);
		}
		return $command->queryAll();
	}

	/**
	 * Get all Questions for given user, grouped by day/month
	 * @param int $userId
	 * @param int $timeframe
	 * @return array
	 */
	public static function getQuestionsCountByPeriod($userId, $timeframe) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Question::model()->tableName());
		$commandBase->andWhere("idUser = :idUser");
		$commandBase->andWhere("created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeframe == 365) {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(created)');
		}
		$commandBase->order('created');

		return $commandBase->queryAll(true, array(':idUser' => $userId, ':timeInterval' => $timeframe));
	}

}
