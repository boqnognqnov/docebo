<?php

/**
 * This is the model class for table "app7020_content_history".
 *
 * The followings are the available columns in table 'app7020_content_history':
 * @property integer $id
 * @property integer $idContent
 * @property integer $idUser
 * @property timestamp $viewed
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 * @property CoreUser $user
 */
class App7020ContentHistory extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_content_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        
        return array(
            array('idContent, idUser', 'required'),
            array('idContent, idUser', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
            'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idContent' => 'Id Content',
            'idUser' => 'Id User',
            'viewed' => 'Viewed'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idUser', $this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020TopicContent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Checks if in the history table exists a record for the selected user and content. 
     * If yes - updates the record, otherwise - a new record will be created
     * @return boolean
     */
    public function secureSave(){
        if(!$this->idUser || !$this->idContent){
            return false;
        }
        if($existingHistory = $this->findByAttributes(array('idContent' => $this->idContent, 'idUser' => $this->idUser))){
            $existingHistory->viewed = Yii::app()->localtime->getUTCNow();
            return $existingHistory->update();
        } else {
            return $this->save();
			// Raise badge event for Asset Recieved a view
			
        }
    }
    
	
	/**
	 * Finds number of views of content
	 * @param int $idContent
	 * @return int
	 */
	public function afterSave() {
        $assetModel = App7020Assets::model()->findByPk($this->idContent);
		Yii::app()->event->raise('AssetReachedAGoal', new DEvent($this, array('userId'=>  $assetModel->userId,'contentId' => $this->idContent , 'goal'=>1)));
	}
    public static function getContentViews($idContent) {
		$criteria = new CDbCriteria();
		$criteria->condition = 'idContent = :idContent';
		$criteria->params = array(':idContent' => $idContent);
		$criteria->group = 'idUser';
		$count = App7020ContentHistory::model()->count($criteria);
		return $count;
	}
	
	/**
	 * Finds number of views of content
	 * @param int $idContent
	 * @return int
	 */
    public static function isContentViewedByUser($idContent, $idUser = false) {
		if(empty($idUser)){
			$idUser = Yii::app()->user->idst;
		}
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select('id');
		$dbCommand->from(self::model()->tableName());
		$dbCommand->where("idContent = :idContent AND idUser = :idUser", array(':idContent' => $idContent, ':idUser' => $idUser));
		$result = $dbCommand->queryAll();
		return count($result) > 0 ? true : false;
	}

    /**
     * Finds number of views of content
     * @param int $idContent
     * @return array
     */
    public static function getContentViewedByUser($idUser = false) {
        if(empty($idUser)){
            $idUser = Yii::app()->user->idst;
        }

        $sqlCommand = Yii::app()->db->createCommand();
        $sqlCommand->selectDistinct('asset.*, asset.created AS create_date');
        $sqlCommand->from(self::model()->tableName() . " t");
        $sqlCommand->leftJoin(App7020Content::model()->tableName() . " asset", "asset.id=t.idContent");
        $sqlCommand->where("idUser = :idUser", array(':idUser' => $idUser));
        $sqlCommand->andWhere("asset.is_private=:private", array(':private' => App7020Assets::PRIVATE_STATUS_INIT)); //only public assets
        $sqlCommand->andWhere("asset.conversion_status=:status", array(':status' => App7020Assets::CONVERSION_STATUS_APPROVED));
		$sqlCommand->order('create_date DESC');

        return  $sqlCommand->queryAll();
    }

 }
