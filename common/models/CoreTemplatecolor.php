<?php

/**
 * This is the model class for table "core_templatecolor".
 *
 * The followings are the available columns in table 'core_templatecolor':
 * @property string $set_id
 * @property string $set_title
 * @property string $set_colors
 * @property integer $is_custom
 * @property integer $is_default
 */
class CoreTemplatecolor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreTemplatecolor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_templatecolor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('set_title', 'required'),
			array('is_custom, is_default', 'numerical', 'integerOnly'=>true),
			array('set_title', 'length', 'max'=>255),
			array('set_colors', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('set_id, set_title, set_colors, is_custom, is_default', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'set_id' => 'Set',
			'set_title' => 'Set Title',
			'set_colors' => 'Set Colors',
			'is_custom' => 'Is Custom',
			'is_default' => 'Is Default',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('set_id',$this->set_id,true);
		$criteria->compare('set_title',$this->set_title,true);
		$criteria->compare('set_colors',$this->set_colors,true);
		$criteria->compare('is_custom',$this->is_custom);
		$criteria->compare('is_default',$this->is_default);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
     * OLD
     *
	 * @var array
	 */
	private static $_colorTemplates;

	/**
     * OLD
     *
	 * @static
	 * @return array
	 */
	public static function getColorTemplates()
	{
		if (empty(self::$_colorTemplates))
		{
			$colorTemplates = array();

			/* @var $results CoreTemplatecolor[] */
			$results = CoreTemplatecolor::model()->findAll();

			foreach ($results as $result)
			{
				$result->set_colors = CJSON::decode($result->set_colors);
				$colorTemplates[$result->set_id] = $result;
			}
			self::$_colorTemplates = $colorTemplates;
		}
		return self::$_colorTemplates;
	}

	/**
     * OLD
     *
	 * @static
	 * @return CoreTemplatecolor
	 */
	public static function getActiveColorTemplate()
	{
		$defaultColorScheme = Settings::get('default_color_scheme', 1);

		if (!empty($_SESSION['preview_color_scheme']) && $_SESSION['preview_color_scheme'] > 0) {
			$selectedColorScheme = intval($_SESSION['preview_color_scheme']);
		} else {
			$selectedColorScheme = Settings::get('selected_color_scheme', $defaultColorScheme);
		}

		$activeColorTemplate = CoreTemplatecolor::model()->findByPk($selectedColorScheme);
		return $activeColorTemplate;
	}

    /**
     * OLD - Load colors in CSS file
     *
     * @return string
     */
    public static function getColorschemeCss() {
        $css = file_get_contents(Yii::app()->theme->basePath . '/css/color.css');
        if ($css !== FALSE) {
            $criteria = new CDbCriteria();
            $criteria->with['scheme'] = array(
                'joinType' => 'INNER JOIN',
            );

            $criteria->condition = 't.color IS NOT NULL AND scheme.enabled = 1 AND scheme.is_custom';
            $criteria->order = 't.color_id ASC';

            $templateColors = CoreSchemeColorSelection::model()->findAll($criteria);

            if ($templateColors) {
                $colors = self::loadColors($templateColors, true);
            } else {$criteria = new CDbCriteria();
                /*
                 * Get defailt values
                $criteria->order = 't.id ASC';

                $templateColors = CoreSchemeColor::model()->findAll($criteria);
                $colors = self::loadColors($templateColors);
                */

                return '';
            }


            $css = str_replace(array_keys($colors), array_values($colors), $css);
        }
        return $css;
    }

    /**
     * OLD - Load colors in array
     *
     * @param array $templateColors
     * @param boolean $isCustom
     * @return array
     */
    public static function loadColors($templateColors, $isCustom = false) {
        $colors = array();

        foreach ($templateColors as $color) {
			$id = $color->color_id;

            switch ($id) {
                case 1:
                    $colors['@menuHeaders'] = $color->color;
                    break;
                case 2:
                    $colors['@titleText'] = $color->color;
                    break;
                case 3:
                    $colors['@text'] = $color->color;
                    break;
                case 4:
                    $colors['@hoverItem'] = $color->color;
                    break;
                case 5:
                    $colors['@selectedItems'] = $color->color;
                    break;
                case 6:
                    $colors['@actionButtons'] = $color->color;
                    break;
                case 7:
                    $colors['@secondaryButtons'] = $color->color;
                    break;
                case 8:
                    $colors['@otherButtons'] = $color->color;
                    break;
                case 9:
                    $colors['@charts1'] = $color->color;
                    break;
                case 10:
                    $colors['@charts2'] = $color->color;
                    break;
                case 11:
                    $colors['@charts3'] = $color->color;
                    break;
            }
        }

        return $colors;
    }
}