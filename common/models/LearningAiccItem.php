<?php

/**
 * This is the model class for table "learning_aicc_item".
 *
 * The followings are the available columns in table 'learning_aicc_item':
 * @property integer $id
 * @property integer $id_package
 * @property string $system_id
 * @property string $parent
 * @property string $launch
 * @property integer $sorting
 * @property string $item_type
 * @property string $developer_id
 * @property string $title
 * @property string $description
 * @property string $command_line
 * @property string $datafromlms
 * @property double $mastery_score
 * @property double $max_score
 * @property string $max_time_allowed
 * @property string $time_limit_action
 * @property string $web_launch
 * @property string $prerequisite
 * @property string $completion
 * @property string $system_vendor
 * @property string $core_vendor
 * @property string $au_password
 * @property string $type
 * @property integer $nChild
 * @property integer $nDescendant
 *
 * The followings are the available model relations:
 * @property LearningAiccItemTrack[] $itemTracks
 * @property LearningAiccPackage $package
 * @property LearningAiccSession[] $sessions
 */
class LearningAiccItem extends CActiveRecord
{

	const MASTER_PARENT = '/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_aicc_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_package, system_id, parent, title', 'required'),
			array('id_package, sorting, nChild, nDescendant', 'numerical', 'integerOnly'=>true),
			array('mastery_score, max_score', 'numerical'),
			array('system_id, parent, developer_id, title, time_limit_action, prerequisite, completion, au_password, type', 'length', 'max'=>255),
			array('item_type, max_time_allowed', 'length', 'max'=>16),
			array('system_vendor', 'length', 'max'=>32),
			array('launch, description, command_line, web_launch, core_vendor', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_package, system_id, parent, launch, sorting, item_type, developer_id, title, description, command_line, mastery_score, max_score, max_time_allowed, time_limit_action, web_launch, prerequisite, completion, system_vendor, core_vendor, au_password, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			// The AICC package this item is part of
			'package' 		=> array(self::BELONGS_TO, 'LearningAiccPackage', 'id_package'),

			// All Lesson Sessions this item has been involved to, all users
			'sessions' 		=> array(self::HAS_MANY, 'LearningAiccSession', 'idItem'),

			// All tracking records for this item, all users
			'itemTracks' 	=> array(self::HAS_MANY, 'LearningAiccItemTrack', 'idItem'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_package' => 'Id Package',
			'system_id' => 'System',
			'parent' => 'Parent',
			'launch' => 'Launch',
			'sorting' => 'Sorting',
			'item_type' => 'Item Type',
			'developer_id' => 'Developer',
			'title' => 'Title',
			'description' => 'Description',
			'command_line' => 'Command Line',
			'datafromlms' => 'Datafromlms',
			'mastery_score' => 'Mastery Score',
			'max_score' => 'Max Score',
			'max_time_allowed' => 'Max Time Allowed',
			'time_limit_action' => 'Time Limit Action',
			'web_launch' => 'Web Launch Parameters',
			'prerequisite' => 'Prerequisite',
			'completion' => 'Completion',
			'system_vendor' => 'System Vendor',
			'core_vendor' => 'Core Vendor',
			'au_password' => 'Au Password',
			'type' => 'Type',
			'nChild' => 'N Child',
			'nDescendant' => 'N Descendant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_package',$this->id_package);
		$criteria->compare('system_id',$this->system_id,true);
		$criteria->compare('parent',$this->parent,true);
		$criteria->compare('launch',$this->launch,true);
		$criteria->compare('sorting',$this->sorting);
		$criteria->compare('item_type',$this->item_type,true);
		$criteria->compare('developer_id',$this->developer_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('command_line',$this->command_line,true);
		$criteria->compare('datafromlms',$this->datafromlms,true);
		$criteria->compare('mastery_score',$this->mastery_score);
		$criteria->compare('max_score',$this->max_score);
		$criteria->compare('max_time_allowed',$this->max_time_allowed,true);
		$criteria->compare('time_limit_action',$this->time_limit_action,true);
		$criteria->compare('web_launch',$this->web_launch,true);
		$criteria->compare('prerequisite',$this->prerequisite,true);
		$criteria->compare('completion',$this->completion,true);
		$criteria->compare('system_vendor',$this->system_vendor,true);
		$criteria->compare('core_vendor',$this->core_vendor,true);
		$criteria->compare('au_password',$this->au_password,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('nChild',$this->nChild);
		$criteria->compare('nDescendant',$this->nDescendant);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningAiccItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * Resolve the Launcing URL for THIS item
	 * @return string
	 */
	public function launchUrl($session, $idOrg) {

		// NON AU ca not be played. Empty launch... well.. can not too
		if ($this->item_type != LearningAiccPackage::ITEM_TYPE_AU)
			return '';

		if(!$this->launch)
			$this->launch = 'story.html';

		// If it starts with HTTP, assume external one..
		if (preg_match('/^http/', $this->launch)) {
			if(Yii::app()->request->isSecureConnection && !preg_match('/^https/', $this->launch))
				$url = preg_replace('/^http/', 'https', $this->launch);
			elseif(!Yii::app()->request->isSecureConnection && preg_match('/^https/', $this->launch))
				$url = preg_replace('/^https/', 'http', $this->launch);
			else
				$url = $this->launch;
		}
		// Otherwise, build the URL to the resource, assuming launch is a relative path to a file
		else {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
			$url = $storageManager->uri() . "/" . $this->package->path . "/" . $this->launch;
		}


		// Fix for "?"
		$connector = '';
		if (stripos($this->launch, '?') !== false) {
			$connector = '&';
		} else {
			$connector = '?';
		}

		$launchParams = $this->web_launch ? $this->web_launch : '';
	    if ($launchParams) {
	    	if ($launchParams[0] == '?') {
	    		$launchParams = substr($launchParams, 1);
	    	}
        	$launchParams = '&'. $launchParams;
    	}


		// HAPC tracker URL (our URL), must be URL encoded
		$hapcUrl = urlencode($this->hapcUrl($idOrg));

		$url = $url . $connector . 'aicc_sid=' . urlencode($session) . '&aicc_url=' . $hapcUrl . $launchParams;

		return $url;

	}

	/**
	 * Resolve the Launcing URL for THIS item
	 * @return string
	 */
	public function previewUrl($session, $idObject) {

		// NON AU ca not be played. Empty launch... well.. can not too
		if ($this->item_type != LearningAiccPackage::ITEM_TYPE_AU)
			return '';

		if(!$this->launch)
			$this->launch = 'story.html';

		// If it starts with HTTP, assume external one..
		if (preg_match('/^http/', $this->launch)) {
			if(Yii::app()->request->isSecureConnection && !preg_match('/^https/', $this->launch))
				$url = preg_replace('/^http/', 'https', $this->launch);
			elseif(!Yii::app()->request->isSecureConnection && preg_match('/^https/', $this->launch))
				$url = preg_replace('/^https/', 'http', $this->launch);
			else
				$url = $this->launch;
		}
		// Otherwise, build the URL to the resource, assuming launch is a relative path to a file
		else {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
			$url = $storageManager->uri() . "/" . $this->package->path . "/" . $this->launch;
		}


		// Fix for "?"
		$connector = '';
		if (stripos($this->launch, '?') !== false) {
			$connector = '&';
		} else {
			$connector = '?';
		}

		$launchParams = $this->web_launch ? $this->web_launch : '';
		if ($launchParams) {
			if ($launchParams[0] == '?') {
				$launchParams = substr($launchParams, 1);
			}
			$launchParams = '&'. $launchParams;
		}


		// HAPC tracker URL (our URL), must be URL encoded
		$hapcUrl = urlencode($this->hapcUrlPreview(Yii::app()->user->id, $this->id));

		$url = $url . $connector . 'aicc_sid=' . urlencode($session) . '&aicc_url=' . $hapcUrl . $launchParams;

		return $url;

	}


	/**
	 *
	 * @return unknown
	 */
	public function hapcUrl($idOrg) {
		$url = Docebo::createAbsoluteLmsUrl('aicc/hapc', array('id_org' => $idOrg));
		return $url;
	}

	public function hapcUrlPreview($idUser, $idItem){
		return Docebo::createAbsoluteLmsUrl('aicc/hapc', array( 'id_user' => $idUser, 'id_item' => $idItem));
	}


	/**
	 * Get *immediate* children of this item
	 * NOTE! NOT descendants, but only children, i.e. one level below
	 *
	 * @param boolean $returnIds  TRUE=return list of IDs (integer), FALSE=return list of models
	 * @param integer $countAus  AU (playable units) counter among all children, passed by reference
	 *
	 * @return array Array
	 */
	public function getChildren($returnIds = false, &$countAus=false) {

		$childrenArray = array();
		foreach ($this->package->items as $item) {
			if (($item->parent == $this->system_id)) {
				$childrenArray[] = $returnIds ? $item->id : $item;
				if (is_numeric($countAus) && ($item->item_type == LearningAiccPackage::ITEM_TYPE_AU)) {
					$countAus++;
				}
			}
		}

		return $childrenArray;

	}


	/**
	 * Count number of children (NOT descendants!!! Children)
	 *
	 * @param integer $countAus  AU (playable units) counter among counted children, passed by reference
	 *
	 * @return number
	 */
	public function countChildren(&$countAus=false) {
		$array = $this->getChildren(true, $countAus);
		return count($array);
	}


	/**
	 * Get a FLAT list of THIS item's descendants (deep down to the bottom)
	 *
	 * @param boolean $returnIds  TRUE=return list of IDs (integer), FALSE=return list of models
	 * @param string $stripObjectiveLeafs  Strip out Leafs of type OBJECTIVE?
	 * @param integer $countAus  AU (playable units) counter among all descendants, passed by reference
	 *
	 * @return array
	 */
	public function getDescendants($returnIds = false, $stripObjectiveLeafs = false, &$countAus=false) {

		static $objectiveLeafs = array();

		$children 	= $this->getChildren($returnIds, $countAus);
		$nChildren 	= count($children);
		$tmp = array();
		if ($nChildren > 0){
			foreach ($children as $item) {
				if ($returnIds) {
					$item = self::model()->findByPk($item);
				}
				$childItems = $item->getDescendants($returnIds, $stripObjectiveLeafs, $countAus);
				$tmp = array_merge($tmp, $childItems);
			}
		}
		else {
			if ($this->item_type == LearningAiccPackage::ITEM_TYPE_OBJECTIVE) {
				$objectiveLeafs[] = $returnIds ? $this->id : $this;
			}
		}

		$result = array_merge($children, $tmp);

		if ($stripObjectiveLeafs) {
			foreach ($objectiveLeafs as $leaf) {
				foreach ($result as $index => $rItem) {
					if ( ($returnIds && ($leaf == $rItem)) || (!$returnIds && ($leaf->id == $rItem->id)))  {
						unset($result[$index]);
					}
				}
			}
		}


		return $result;
	}


	/**
	 * Count a given item's descendants (deep down to the bottom)
	 *
	 * @param integer $countAus
	 *
	 * @return number
	 */
	public function countDescendants($stripObjectiveLeafs = false, &$countAus=false) {
		return count($this->getDescendants(true, $stripObjectiveLeafs, $countAus));
	}

	/**
	 * Get array of descendnats of this item, in a TREE ARRAY, where array KEYS are NODE IDs.
	 * Optionally, strip out LEAFS being OBJECTIVES type
	 *
	 * @param string $stripObjectiveLeafs
	 * @return array
	 */
	public function getDescendantsTree($stripObjectiveLeafs = false, &$countAus=false) {
		$tree 		= array();
		$children 	= $this->getChildren(false, $countAus);
		$nChildren 	= count($children);
		$tmp = array();
		if ($nChildren > 0){
			foreach ($children as $item) {
				$tmp = $item->getDescendantsTree($stripObjectiveLeafs, $countAus);
				if (is_array($tmp) && count($tmp) > 0) {
					$tree[$this->id][$item->id] = $tmp[$item->id];
				}
				else if (!$stripObjectiveLeafs || ($item->item_type != LearningAiccPackage::ITEM_TYPE_OBJECTIVE)) {
					$tree[$this->id][$item->id] = $tmp;
				}
			}
		}
		return $tree;
	}



	/**
	 *
	 * @return LearningAiccItem
	 */
	public function getParent() {
		$item = self::model()->findByAttributes(array(
			'system_id'		=> $this->parent,
			'id_package'	=> $this->id_package,
		));
		return $item;
	}


	/**
	 * Get list of IDs or models of all ancestors of this item
	 *
	 * @param boolean $includeMaster
	 * @param boolean $returnIds
	 * @param boolean $includeMe
	 * @return array
	 */
	public function getAncestors($includeMaster = false, $returnIds = false, $includeMe = false) {
		$parent = $this->getParent();
		$inversePath = array();
		if ($includeMe) {
			$inversePath[] = $returnIds ? $this->id : $this;
		}
		while ($parent) {
			if (($parent->parent != self::MASTER_PARENT) || ($includeMaster)) {
				$inversePath[] = $returnIds ? $parent->id : $parent;
			}
			$parent = $parent->getParent();
		}
		return array_reverse($inversePath);
	}


	/**
	 *
	 * @param integer $idUser
	 *
	 * @return static
	 */
	public function getTrackForUser($idUser) {
		$model = LearningAiccItemTrack::getTrack($idUser, $this->id);
		return $model;
	}


}
