<?php

/**
 * This is the model class for table "learning_label".
 *
 * The followings are the available columns in table 'learning_label':
 * @property integer $id_common_label
 * @property string $lang_code
 * @property string $title
 * @property string $description
 * @property string $color
 * @property string $sequence
 * @property string $icon
 * @property integer $id_channel
 *
 */
class LearningLabel extends CActiveRecord
{

	public $confirm;
	public $color;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningLabel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_label';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('description', 'required'),
			array('id_common_label', 'numerical', 'integerOnly'=>true),
			array('lang_code, title, color', 'length', 'max'=>255),
			array('sequence', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_common_label, lang_code, title, description, color, sequence, icon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'labelCourses' => array(self::HAS_MANY, 'LearningLabelCourse', 'id_common_label'),
		    'learningCourses' => array(self::HAS_MANY, 'LearningCourse', array("id_course" => "idCourse"),'through' => 'labelCourses'),
			//'learningCourses' => array(self::MANY_MANY, 'LearningCourse', 'learning_label_course(id_common_label, id_course)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_common_label' => 'Id Common Label',
			'lang_code' => 'Lang Code',
			'title' => 'Title',
			'description' => 'Description',
			'color' => 'Color',
			'sequence' => 'Sequence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_common_label',$this->id_common_label);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('color',$this->color,true);
		$criteria->compare('sequence',$this->sequence,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function dataProvider() {
		$criteria = new CDbCriteria;
		$config = array();
		$defaultLang = CoreLangLanguage::getDefaultLanguage();
		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));

		/*
		 * Because we have language codes like:
		 * portuguese and portuguese-br
		 * best way is to filter the lang_code by exact match not by partial match.
		 */
		$criteria->compare('lang_code', $lang->lang_code, false);

		// Get title and description on default language if they are missing on current language
		$criteria->select = 't.id_common_label, t.lang_code, t.description, t.color, t.sequence, t.icon, t.id_channel, ';
		$criteria->select .= 'IF(t.title!="", t.title, (SELECT title from learning_label WHERE lang_code = "'.$defaultLang.'" and id_common_label=t.id_common_label)) as title, ';
		$criteria->select .= 'IF(t.description!="", t.description, (SELECT description from learning_label WHERE lang_code = "'.$defaultLang.'" and id_common_label=t.id_common_label)) as description';

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		// $sortAttributes['userLanguage'] = 'language.lang_description';
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'sequence ASC';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}


	/**
	 * Return the user related labels for a specified language
	 * @param $idUser
	 * @param $langCode
	 * @return array
	 */
	public function getUserLabels($idUser, $langCode) {

		$labels = Yii::app()->db->createCommand()
		    ->select('label.*, count(*) as courseuser_count')
		    ->from('learning_course course')
		    ->leftJoin('learning_courseuser enrol', 'course.idCourse = enrol.idCourse')
			->leftJoin('learning_label_course lc', 'course.idCourse = lc.id_course')
			->leftJoin('learning_label label', 'label.id_common_label = lc.id_common_label AND label.lang_code = :lang_code', array(
				':lang_code'=>$langCode,
			))
		    ->where('enrol.idUser = :idUser', array(':idUser' => $idUser))
			->group('label.id_common_label')
			->order('label.sequence')
		    ->queryAll();

		return $labels;

		/*
		print_r($labels);
		die();

	    $criteria = new CDbCriteria();
	    $criteria->alias = 'label';
	    $criteria->with = array(
	            'learningCourses' => array(
	                    'alias'=>'course',
	                    'select'=>'idCourse',
	                    'joinType'=>'INNER JOIN',
	                    'with' => array(
	                            'learningCourseusers' => array(
	                                    'alias'=>'courseuser',
	                                    'select'=>false,
	                                    'joinType'=>'INNER JOIN'
	                            )
	                    )
	            )
	    );

	    // Only Non-empty titled labels are active
	    $criteria->addCondition("label.title !=''");

	    $criteria->addCondition('label.lang_code = :lang_code');
	    $criteria->addCondition('courseuser.idUser = :idUser');
	    $criteria->params = array(
	            ':lang_code'=>$langCode,
	            ':idUser'=>$idUser
	    );
	    $criteria->order = 'label.sequence ASC';

	    $labels = LearningLabel::model()->findAll($criteria);

	    return $labels;
		*/
    }

	/**
	 * Get available labels for a given language
	 * @param $lang
	 * @return array
	 */
	public static function getLabelsByLang($lang) {
		$criteria = new CDbCriteria();
		$criteria->alias = 'label';

		// Only Non-empty titled labels are active
		$criteria->addCondition("label.title !=''");
		$criteria->addCondition('label.lang_code = :lang_code');

		$criteria->params = array(
			':lang_code'=>$lang,
		);
		$criteria->order = 'label.sequence ASC';

		return LearningLabel::model()->findAll($criteria);
	}

	public function renderMove() {
		$content = '<div class="label-field-move">'.$this->id_common_label.'</div>';
		return $content;
	}

	public function renderColor() {
		$content = '<span class="label-field-color-wrapper"><span class="label-field-color" style="background-color:'.$this->color.'"></span></span>';
		return $content;
	}

	/**
	 * insert labels for language $language if not exist
	 * @param $language
	 */
	public static function insertLabelsForLanguage($language) {
		$additionalColumns = '';
		if(PluginManager::isPluginActive('LabelApp') && self::model()->hasAttribute('id_channel')){
			$additionalColumns = ', ll.id_channel';
		}

		$sql = "
			INSERT INTO ".self::model()->tableName()."
				SELECT ll.id_common_label, '$language', '', '', ll.color, ll.sequence, ll.icon ".$additionalColumns."
				FROM
					(SELECT * FROM learning_label GROUP BY id_common_label) ll
				LEFT JOIN learning_label l
				ON l.id_common_label = ll.id_common_label AND l.lang_code = '$language'
				WHERE l.id_common_label IS NULL
			";

		Yii::app()->db->createCommand($sql)->execute();
	}



	/**
	 * Return the title and description of the label, but checking if they are blank in the current language.
	 * If that's true for one or both of them, than do the fallback on LMS default language.
	 * @return array the title  and description of the label
	 */
	public function getTitleAndDescriptionLanguageSafe() {
		$output = array(
			'title' => $this->title,
			'description' => $this->description
		);
		$defaultLanguage = Settings::get('default_language');
		$currentLanguage = Yii::app()->session['current_lang'];
		if ((empty($this->title) || empty($this->description)) && $currentLanguage != $defaultLanguage) {
			//if current language's title and/or description is blank, then retrieve from database the default language's translations
			$cmd = Yii::app()->db->createCommand("SELECT title, description FROM ".self::model()->tableName()." WHERE id_common_label = :id_common_label AND lang_code = :lang_code");
			$record = $cmd->queryRow(true, array(
				':id_common_label' => $this->id_common_label,
				':lang_code' => $defaultLanguage
			));
			if (!empty($record)) {
				if (empty($this->title)) { $output['title'] = $record['title']; }
				if (empty($this->description)) { $output['description'] = $record['description']; }
			}
		}
		return $output;
	}

	/**
	 * Creates a channel from a label
	 */
	public function createChannelFromLabel() {

		// Create channel using label data for initialization
		$ordering = App7020Channels::getOrdering();
		$channelObject = new App7020Channels();
		$channelObject->type = 2; //2 => admin channel
		$channelObject->enabled = 1;
		$channelObject->ordering = $ordering;
		$channelObject->sort_items = '';
		$channelObject->icon_bgr = $this->color;
		$channelObject->icon_fa = $this->icon;
		$channelObject->icon_color = "#ffffff"; //default
		$channelObject->permission_type = 1;
		$channelObject->save();

		$this->id_channel = $channelObject->id;
		$sql = "UPDATE `".LearningLabel::model()->tableName()."` SET `id_channel` = ".$this->id_channel." WHERE `id_common_label` = ".$this->id_common_label;
		Yii::app()->db->createCommand($sql)->execute();

		// Update channel translations
		$sql = "SELECT * FROM `".LearningLabel::model()->tableName()."` WHERE `id_common_label` = ".$this->id_common_label;
		$labelTranslations = Yii::app()->db->createCommand($sql)->queryAll();
		foreach($labelTranslations as $labelTranslation) {
			$languageCode = Lang::getBrowserCodeByCode($labelTranslation['lang_code']);
			if ($labelTranslation['title'] || $labelTranslation['description']) {
				$channelTranslation = new App7020ChannelTranslation();
				$channelTranslation->idChannel = $this->id_channel;
				$channelTranslation->lang = $languageCode;
				$channelTranslation->name = $labelTranslation['title'];
				$channelTranslation->description = $labelTranslation['description'];
				$channelTranslation->save();
			}
		}

		return $this->id_channel;
	}

}