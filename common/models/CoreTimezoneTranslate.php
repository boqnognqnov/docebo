<?php

/**
 * This is the model class for table "core_timezone_translate".
 *
 ********************************************************************************************************************
 * Some additional notes: not all timezones can be properly 'translated' to a new one for each specific tool.       *
 * When (*) is added to a citrix_description field, there's no near timezone, but another exists with same time.    *
 * When (**) os added, there's no translated timezone available, and is translated to the closest one               *
 * eg: Citrix doesn't provide a zone for Pacific/Marquesas (GMT-09:30), instead this is added to GMT-10:00 (Hawaii) *
 ********************************************************************************************************************
 *
 * The followings are the available columns in table 'core_timezone_translate':
 * @property integer $id
 * @property string $offset
 * @property string $yii_timezone
 * @property string $yii_description
 * @property string $citrix_timezone
 * @property string $citrix_description
 */
class CoreTimezoneTranslate extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_timezone_translate';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('offset, yii_timezone, yii_description, citrix_timezone, citrix_description', 'required'),
            array('yii_timezone, yii_description, citrix_timezone, citrix_description', 'length', 'max'=>80),
            array('offset', 'length', 'max'=>10),
            array('id, offset, yii_timezone, yii_description, citrix_timezone, citrix_description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'offset' => 'Offset',
            'yii_timezone' => 'Yii Timezone',
            'yii_description' => 'Yii Description',
            'citrix_timezone' => 'Citrix Timezone',
            'citrix_description' => 'Citrix Description'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('offset',$this->offset);
        $criteria->compare('yii_timezone',$this->yii_timezone);
        $criteria->compare('yii_description',$this->yii_description);
        $criteria->compare('citrix_timezone',$this->citrix_timezone);
        $criteria->compare('citrix_description',$this->citrix_description);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SalesforceSyncListviews the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * "Translates" a default LMS timezone into a possible timezone expected by a 3rd party tool
     *
     * @param string $yiiTimezone
     * @param string $tool
     * @return string|boolean
     */
    public static function translateTimezone($yiiTimezone, $tool){
        return Yii::app()->db->createCommand()
            ->select($tool."_timezone")
            ->from(self::model()->tableName())
            ->where('yii_timezone = :yiiTimezone', array(':yiiTimezone' => $yiiTimezone))
            ->queryScalar();
    }
}
