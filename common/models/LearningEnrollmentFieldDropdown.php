<?php

/**
 * This is the model class for table "learning_enrollment_field_dropdown".
 *
 * The followings are the available columns in table 'learning_course_field_dropdown':
 * @property integer $id
 * @property integer $id_field
 * @property integer $sequence
 * @property json $translation
 *
 * The followings are the available model relations:
 * @property LearningCourseField $idField
 * @property LearningCourseFieldDropdownTranslations[] $learningCourseFieldDropdownTranslations
 */
class LearningEnrollmentFieldDropdown extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_enrollment_fields_dropdown';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_field', 'required'),
			array('id_field, sequence', 'numerical', 'integerOnly'=>true),
			array('id, id_field, sequence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idField' => array(self::BELONGS_TO, 'LearningEnrollmentField', 'id_field'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_option' => 'Id Option',
			'id_field' => 'Id Field',
			'sequence' => 'Sequence',
		);
	}

	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseFieldDropdown the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getOptionsByFieldId($idField){
		
		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		// Resolve requested language
		$language = Yii::app()->getLanguage();
		
		$command = Yii::app()->db->createCommand();
		$command->select('id, IF(JSON_UNQUOTE(translation->\'$."'.$language.'"\') IS NULL OR JSON_UNQUOTE(translation->\'$."'.$language.'"\') = "", JSON_UNQUOTE(translation->\'$."'.$defaultLanguage.'"\'), JSON_UNQUOTE(translation->\'$."'.$language.'"\')) as title');
		$command->from(self::model()->tableName());
		$command->where("id_field = :idField", array(":idField" => $idField));
		$command->order("sequence");
		$result = $command->queryAll();
		$returnArray = array();
		if($result){
			foreach ($result as $value) {
				$returnArray[$value['id']] = $value['title'];				
			}
		}
		
		return $returnArray;
	}
	
	public static function getDropdownData($idField) {
		$dropdownModel = LearningEnrollmentFieldDropdown::model()->findAll('id_field = :id', [':id' => $idField]);
		$fieldModel = LearningEnrollmentField::model()->findByPk($idField);
		
		if ( isset($fieldModel->translation) ) {
			$fieldObj = json_decode($fieldModel->translation);
		}
		
		$tempArr = array_values( CHtml::listData( $dropdownModel, 'translation', 'translation' ) );
		$dropdownOptionsTemp = array();
		foreach ($tempArr as $translation) {
			$dropdownOptionsTemp[] = json_decode($translation);
		}
		unset($tempArr);
		
		
		$dropdownOptions = array();
		foreach ($dropdownOptionsTemp as $options) {
			foreach ($options as $langCode => $option) {
				if ( !isset($dropdownOptions[$langCode]) ) {
				$dropdownOptions[$langCode] = array();
				} 
				$dropdownOptions[$langCode][] = $option;
			}
		}
		unset($dropdownOptionsTemp);
		
		$langCodes = CoreLangLanguage::getActiveLanguagesByBrowsercode();
		$dropdownArr = array();
		foreach ($langCodes as $langCode => $value) {
			if ( !isset($dropdownArr[$langCode]) ) {
				$dropdownArr[$langCode] = array(
					'label' => $fieldObj->$langCode,
					'options' => array()
				);
			} 
			$dropdownArr[$langCode]['options'] = $dropdownOptions[$langCode];
		}
		
		return $dropdownArr;
	}
	
}
