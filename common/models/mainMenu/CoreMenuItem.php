<?php

/**
 * This is the model class for table "core_menu_item".
 *
 * The followings are the available columns in table 'core_menu_item':
 * @property integer $id
 * @property integer $lev
 * @property integer $iLeft
 * @property integer $iRight
 * @property integer $idParent
 * @property string $icon
 * @property string $url
 * @property integer $is_custom
 * @property integer $visible
 * @property integer $active
 * @property integer $default
 * @property string $options
 * @property string $secretKey
 * @property integer $iframe_height
 * @property string $salt_secret
 * @property string $oauth_client
 */
class CoreMenuItem extends CActiveRecord {

	// Window Target Types
	CONST SAME_WIN = '_self';
	CONST NEW_WIN = '_blank';
	CONST ACCREDIFRAME = 'iframe';
	//Menu item Filter types
	CONST SHOW_TO_ALL = 0;
	CONST SHOW_TO_CUSTOM = 1;

	public static $urlTargetType = array(
		self::SAME_WIN => 'Same window',
		self::NEW_WIN => 'New window',
		self::ACCREDIFRAME => 'Iframe'
	);
	public static $filterType = array(
		self::SHOW_TO_ALL => 'All branches and groups',
		self::SHOW_TO_CUSTOM => '_show_to_custom'
	);
	public static $all_icons = array(
		'my-courses' => 'my-courses',
		'catalog' => 'catalog',
		'certificate' => 'certificate',
		'competencies' => 'competencies',
		'community' => 'community',
		'curricula' => 'curricula',
		'activate' => 'activate',
		'messages' => 'messages',
		'my-activities' => 'my-activities',
		'my-calendar' => 'my-calendar',
		'fa fa-comments-o' => 'fa fa-comments-o',
		'fa fa-cloud-upload' => 'fa fa-cloud-upload',
		'fa fa-th-list' => 'fa fa-th-list',
		'fa fa-wrench' => 'fa fa-wrench',
	);
	//Menu Options
	public $lang_code;
	public $useSecretKey;
	public $secretKey;
	public $targetType;
	public $useAccredIframe;
	public $iframe_height;
	public $salt_secret;
	public $repeatSaltSecret;
	public $oauth_client;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_menu_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idParent, url', 'required'),
			array('useSecretKey ,lev, iLeft, iRight, idParent, is_custom, visible, useAccredIframe, iframe_height', 'numerical', 'integerOnly' => true),
			array('secretKey', 'required', 'on' => 'useSecret'),
			array('salt_secret, repeatSaltSecret', 'required', 'on' => 'useAccred'),
			array('targetType, secretKey, icon, url, salt_secret, repeatSaltSecret, oauth_client', 'length', 'max' => 255),
			array('options, useSecretKey, targetType, secretKey, icon', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lev, iLeft, iRight, idParent, icon, url, is_custom, visible, options', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'translation' => array(self::HAS_MANY, 'CoreMenuTranslation', 'idMenu'),
			'filter' => array(self::HAS_MANY, 'CoreMenuFilter', 'idMenu'),
		);
	}

	public function behaviors() {
		return array(
			'nestedSetBehavior' => array(
				'class' => 'common.components.DoceboNestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
				'idParentAttribute' => 'idParent',
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'lev' => 'Lev',
			'iLeft' => 'I Left',
			'iRight' => 'I Right',
			'idParent' => 'Id Parent',
			'icon' => 'Icon',
			'url' => 'Url',
			'is_custom' => 'Is Custom',
			'visible' => 'Visible',
			'options' => 'Options',
			'iframe_height' => Yii::t('standard', 'Height'),
			'salt_secret' => Yii::t('standard', 'Salt Secret'),
		    'repeatSaltSecret' => Yii::t('standard', 'Repeat secret'), 
			'oauth_client' => Yii::t('standard', 'OAuth Client'),
		    'useAccredIframe' => Yii::t('standard', 'Advanced'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('lev', $this->lev);
		$criteria->compare('iLeft', $this->iLeft);
		$criteria->compare('iRight', $this->iRight);
		$criteria->compare('idParent', $this->idParent);
		$criteria->compare('icon', $this->icon, true);
		$criteria->compare('url', $this->url, true);
		$criteria->compare('is_custom', $this->is_custom);
		$criteria->compare('visible', $this->visible);
		$criteria->compare('options', $this->options, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreMenuItem the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function beforeSave() {
		$this->options = $this->options !== null ? json_encode($this->options) : null;
		return parent::beforeSave();
	}

	public function afterFind() {
		$this->options = $this->options !== null ? json_decode($this->options, true) : array();
		if (isset($this->options['target']))
			$this->targetType = $this->options['target'];
		if (isset($this->options['useSecretKey']))
			$this->useSecretKey = $this->options['useSecretKey'];
		
		if (isset($this->options['secretKey']))
			$this->secretKey = $this->options['secretKey'];

        if (isset($this->options['useAccredIframe']))
		  $this->useAccredIframe = $this->options['useAccredIframe'];
			    	
			
		$this->lang_code = Settings::get('default_language');
		return parent::afterFind();
	}

	public static function getMainItemByTitle($title) {
		return self::model()->with(array('translation' => array('label' => $title)))->find();
	}

	public function getChildMenuItemsFromDB() {
		$elements = Yii::app()->cache->get('cached_menu_' . Yii::app()->user->id);
		if ($elements) {
            $items = $this->getAllChildren();
            foreach ($elements as $key => $elem) {
                foreach ($items as $item) {
                    if ($elem['id'] == $item->id) {
                        $elements[$key]['label'] = $item->getLabel();
                    }
                }
            }
			return $elements;
		} else {
			//Get current child from DB
			$elements = array();
			$items = $this->getActiveAndVisibleChildren();
			$catalogType = Settings::get('catalog_type', 'catalog');
			$catalogIsVisible = $catalogType == 'mycourses'? false : true;
			foreach ($items as $item) {
				if($item->module_name == 'CoursecatalogApp' && !$catalogIsVisible)
					continue;
				if (!empty($item->filter) && !$item->canLoggedUserSeeThisMenu())
					continue;
				$elements[] = array(
					'href' => $item->getItemUrl(),
					'icon' => $item->icon,
					'label' => $item->getLabel(),
					'target' => $item->targetType,
					'id' => $item->id
				);
			}
			Yii::app()->cache->set('cached_menu_' . Yii::app()->user->id, $elements, 6 * 60 * 60);
			return $elements;
		}
	}

	public function getActiveAndVisibleChildren() {
		$criteria = new CDbCriteria();
		$criteria->condition = 'idParent = :parent AND active AND visible';
		$criteria->params[':parent'] = $this->id;
		$criteria->order = 'iLeft';
		return self::model()->with('translation')->findAll($criteria);
	}

    public function getAllChildren() {
        $criteria = new CDbCriteria();
        $criteria->condition = 'idParent = :parent';
        $criteria->params[':parent'] = $this->id;
        $criteria->order = 'iLeft';
        return self::model()->with('translation')->findAll($criteria);
    }

	public function getLabel($forceLanguage = false) {
		static $cache = array();

		$lang = ($forceLanguage === false ? Yii::app()->getLanguage() : $forceLanguage);

		if (!isset($cache[$lang])) {
			$cache[$lang] = CoreLangLanguage::model()->findByAttributes(array(
				'lang_browsercode' => $lang,
			));
		}
		$language = $cache[$lang];

		$label = false;
		if ($this->is_custom <= 0) {
			//collect known standard menu items
			//NOTE: we use the combo "icon"/"url" as key to detect a specific menu item, since numeric IDs can vary in different installations
			$list = array(
				array('icon' => 'menu-icon-mydashboard', 'url' => 'site/index', 'module' => 'menu_over', 'key' => 'My Dashboard'),
				array('icon' => 'my-courses', 'url' => 'site/index', 'module' => 'menu_over', 'key' => '_MYCOURSES'),
				array('icon' => 'catalog', 'url' => 'site/index', 'module' => 'standard', 'key' => 'Catalog'),
				array('icon' => 'my-activities', 'url' => 'myActivities/index', 'module' => 'course', 'key' => 'My Activities'),
				array('icon' => 'curricula', 'url' => 'curricula/show', 'module' => 'standard', 'key' => '_COURSEPATH'),
				array('icon' => 'activate', 'url' => 'course/autoreg', 'module' => 'menu_over', 'key' => '_COURSE_AUTOREGISTRATION'),
				array('icon' => 'my-calendar', 'url' => 'mycalendar/show', 'module' => 'calendar', 'key' => 'My Calendar'),
				array('icon' => 'my-blog', 'url' => 'MyBlogApp/MyBlogApp/index', 'module' => 'myblog', 'key' => 'My Blog')
			);
			$counter = 0;
			while ($label === false && $counter < count($list)) {
				if ($list[$counter]['icon'] == $this->icon && $list[$counter]['url'] == $this->url) {
					$label = Yii::t($list[$counter]['module'], $list[$counter]['key']);
				}
				$counter++;
			}
		}
		if (empty($label)) {
			//check if we have translations in menu translations table
			$criteria = new CDbCriteria();
			$criteria->condition = 'lang_code = :language and idMenu = :idMenu';
			$criteria->params = array(':language' => $language->lang_code, ':idMenu' => $this->id);
			$trans = CoreMenuTranslation::model()->find($criteria);
			$label = (($trans !== null && $trans->label) ? $trans->label : $this->getDefaultLabel());
		}
		return $label;
	}

	private function getDefaultLabel() {
		$model = CoreMenuTranslation::model()->findByAttributes(array('idMenu' => $this->id, 'lang_code' => 'english '));
		return $model->label;
	}

	public function getMenuItemsDataProvider() {
		$parent = self::getMainItemByTitle('Menu');
		$criteria = new CDbCriteria();
		$criteria->with = array('translation');
		$criteria->condition = 'idParent = :parent AND active';
		$criteria->order = 'iLeft';
		$criteria->params = array(
			':parent' => $parent->id,
		);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false
		));
	}

	public function getIcon() {
		if($this->icon != NULL && strlen($this->icon) > 0){
			$icon = $this->icon;
		} else {
			$icon = 'fa-star';
		}
		if($this->is_custom == 1){
			$class = 'fa';
		} else {
			$class= 'zmdi';
		}

		return "<i class='".$class." ". $icon . "'></i>";
	}

	public function getUrlTarget() {
		return $this->targetType ? $this->targetType : '_self';
	}

	public function setMenuAdditionalOptions() {
		$this->options = array(
			'target' => $this->targetType,
			'useSecretKey' => $this->useSecretKey,
			'secretKey' => $this->secretKey,
		    'useAccredIframe' => $this->useAccredIframe,
		);
	}

	public function getItemFilter() {
		$infoText = '';
		if (empty($this->filter)) {
			$infoText = Yii::t('standard', 'All branches and groups');
		} else {
			$parts = array();
			$sql = "SELECT COUNT(IF(obj_type = :item, obj_type, NULL)) AS groups, COUNT(IF(obj_type != :item, obj_type, NULL)) AS branches
					FROM core_menu_filter
					WHERE idMenu = :menu";
			$query = Yii::app()->db->createCommand($sql)->queryRow(true, array(':item' => DashboardAssign::ITEM_TYPE_GROUP, ':menu' => $this->id));
			$parts[] = $query['groups'] . " " . Yii::t('standard', '_GROUPS');
			$parts[] = $query['branches'] . " " . Yii::t('standard', 'branches');
			if (count($parts) > 0) {
				$infoText = implode(', ', $parts);
			}
		}

		return CHtml::link(
						$infoText, Docebo::createLmsUrl(
								'usersSelector/axOpen', array(
							'type' => UsersSelector::TYPE_FILTER_MENU_ITEMS_FOR_USERS,
							'idMenu' => $this->id,
							'tp' => 'admin.protected.views.menuManagement._ufilter_top',
							'noyiixviewjs' => true,
						)), array(
					'class' => 'open-dialog filter-menu-items underline',
					'rel' => 'users-selector',
					'data-dialog-title' => Yii::t('standard', 'Filter'),
					'title' => Yii::t('standard', 'Filter')
						)
		);
	}

	public function getMenuFilterInfo() {
		$relatedTypes = DashboardAssign::$userFilterTypes;
		foreach ($relatedTypes as $k => $v)
			$relatedTypes[$k] = "'" . $v . "'";

		$groups = array();
		$branches = array();
		$sql = "
			SELECT *
			FROM core_menu_item cmi
			JOIN core_menu_filter cmf ON ((cmf.idMenu=cmi.id) AND (cmf.obj_type IN (" . implode(',', $relatedTypes) . ")))
			WHERE cmi.id=:id
		";
		$params[":id"] = $this->id;

		$reader = Yii::app()->db->createCommand($sql)->query($params);
		foreach ($reader as $row) {
			switch ($row['obj_type']) {
				case DashboardAssign::ITEM_TYPE_GROUP:
					$groups[] = (int) $row['idResource'];
					break;
				case DashboardAssign::ITEM_TYPE_BRANCH:
				case DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC:
					$element = array(
						"key" => (int) $row['idResource'],
						"selectState" => $row['obj_type'] == DashboardAssign::ITEM_TYPE_BRANCH ? "1" : "2",
					);
					$branches[] = $element;
					break;
			}
		}

		return array(
			'groups' => $groups,
			'branches' => $branches,
		);
	}

	public function clearFilters() {
		CoreMenuFilter::model()->deleteAllByAttributes(array('idMenu' => $this->id));
	}

	public function assignGroups($data) {
		// First, remove old assignments
		CoreMenuFilter::model()->deleteAllByAttributes(array(
			'idMenu' => $this->id,
			'obj_type' => DashboardAssign::ITEM_TYPE_GROUP,
		));

		foreach ($data as $item) {
			$model = new CoreMenuFilter();
			$model->idMenu = $this->id;
			$model->obj_type = DashboardAssign::ITEM_TYPE_GROUP;
			$model->idResource = $item;
			$model->save();
		}
	}

	public function assignBranches($data) {
		// First, remove old assignments
		CoreMenuFilter::model()->deleteAllByAttributes(array(
			'idMenu' => $this->id,
			'obj_type' => array(DashboardAssign::ITEM_TYPE_BRANCH, DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC),
		));

		foreach ($data as $item) {
			$model = new CoreMenuFilter();
			$model->idMenu = $this->id;
			$model->obj_type = $item['selectState'] == 1 ? DashboardAssign::ITEM_TYPE_BRANCH : DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC;
			$model->idResource = $item['key'];
			$model->save();
		}
	}

	public function canLoggedUserSeeThisMenu() {
		$groups = array();
		$oneBranch = array();
		$branchesAndDesc = array();

		foreach ($this->filter as $filter) {
			if ($filter->obj_type == DashboardAssign::ITEM_TYPE_GROUP) {
				$groups[] = $filter->idResource;
			} else {
				if ($filter->obj_type == DashboardAssign::ITEM_TYPE_BRANCH)
					$oneBranch[] = $filter->idResource;
				if ($filter->obj_type == DashboardAssign::ITEM_TYPE_BRANCH_AND_DESC)
					$branchesAndDesc[] = $filter->idResource;
			}
		}

		if (!empty($groups)) {
			$model = CoreGroupMembers::model()->findByAttributes(array('idstMember' => Yii::app()->user->id, 'idst' => $groups));
			if ($model !== null)
				return true;
		}

		if (!empty($oneBranch) || !empty($branchesAndDesc)) {
			if (!empty($oneBranch)) {
				$criteria = new CDbCriteria();
				$criteria->condition = 'idstMember = :user';
				$criteria->params[':user'] = Yii::app()->user->id;
				$criteria->join = 'JOIN core_group_members cgm ON cgm.`idst` = t.idst_oc';
				$criteria->addInCondition('t.idOrg', $oneBranch);
				$models = CoreOrgChartTree::model()->exists($criteria);
				if ($models)
					return true;
			}
			if (!empty($branchesAndDesc)) {
				$criteria = new CDbCriteria();
				$criteria->condition = 'idstMember = :user';
				$criteria->params[':user'] = Yii::app()->user->id;
				$criteria->addInCondition('t.idOrg', $branchesAndDesc);
				$criteria->join = 'JOIN core_org_chart_tree coct_t ON coct_t.`iLeft` >= t.`iLeft` AND coct_t.`iRight` <= t.`iRight`';
				$criteria->join .= ' JOIN core_group_members cgm ON cgm.`idst` = coct_t.idst_oc OR cgm.`idst` = coct_t.idst_ocd';
				$models = CoreOrgChartTree::model()->exists($criteria);
				if ($models)
					return true;
			}
		}
		return false;
	}

	public function getIsDefault() {
		//If is not from the default menu items we don't show flag
		if ($this->is_custom)
			return "";
		$green = $this->default == 1 ? "green" : "";
		if ($this->default != 1)
			$html = '<a href="javascript:;" class="toggle-default-layout-link" data-id="' . $this->id . '"><span class="p-sprite flag3 ' . $green . '"></span></a>';
		else
			$html = '<a href="javascript: void(0);" rel="tooltip" title="' . Yii::t('standard', 'Default') . '"><span class="p-sprite flag3 ' . $green . '"></span></a>';
		return $html;
	}

	public function getItemUrl() {
		// Only the custom links don't have target type, so check if this is custom url
		$checkAppMode = preg_grep("/^(\w){3,}:(\w){3,}\/(\w){3,}$/", explode("\n", $this->url));
		if ($this->targetType) {
			$http = (stripos($this->url, 'http') === 0 || stripos($this->url, 'mailto:') === 0) ? '' : 'http://';
			if ($this->useSecretKey) {
				$slash = substr($this->url, -1) != '/' ? '/' : '';
				$timestamp = time();
				$username = Yii::app()->user->getRelativeUsername(Yii::app()->user->getUserAttrib('userid'));
				$email = Yii::app()->user->getEmail();
				$urlOptions = array(
					'timestamp' => $timestamp,
					'username' => $username,
					'email' => $email,
					'hash' => base64_encode(hash_hmac('sha1', $timestamp . $username . $email, $this->secretKey, true))
				);
				return $http . $this->url . $slash . 'link?' . http_build_query($urlOptions);
			}
			if (!empty($checkAppMode)) {
				return Docebo::createAppUrl($this->url, json_decode($this->default_opt, true));
			}
			return $http . $this->url;
		}
		if (!empty($checkAppMode)) {
			return Docebo::createAppUrl($this->url, json_decode($this->default_opt, true));
		}
		return Docebo::createLmsUrl($this->url, json_decode($this->default_opt, true));
	}

	public static function getLmsDefaultUrl() {
		$model = self::model()->findByAttributes(array('default' => 1, 'visible' => 1, 'active' => 1));
		if($model->module_name == 'CoursecatalogApp') {
			$catalogType = Settings::get('catalog_type', 'catalog');
			if ($catalogType == 'mycourses') {
				return array('url' => 'site/index', 'prop' => null);
			}
		}
		if ($model !== null) {
			$userIsNotAssignedToAnyLP = !(PluginManager::isPluginActive('CurriculaApp') && LearningCoursepathUser::getUserCoursepathNumber(Yii::app()->user->idst));
			if ($model->url == 'curricula/show' && $userIsNotAssignedToAnyLP)
				return array('url' => 'site/index', 'prop' => array('opt' => 'fullmycourses'));

			return array('url' => $model->url, 'prop' => json_decode($model->default_opt, true));
		} else
			return array('url' => null, 'prop' => null);
	}

	public static function getUrlTargetType()
	{
		$translated = array();
		foreach (self::$urlTargetType as $key => $value)
			$translated[$key] = Yii::t('standard', $value);
		return $translated;
	}

}
