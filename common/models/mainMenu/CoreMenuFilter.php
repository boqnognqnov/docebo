<?php

/**
 * This is the model class for table "core_menu_filter".
 *
 * The followings are the available columns in table 'core_menu_filter':
 * @property integer $id
 * @property integer $idMenu
 * @property string $obj_type
 * @property integer $idResource
 */
class CoreMenuFilter extends CActiveRecord
{
	CONST BRANCH = 10;
	CONST GROUP  = 20;

	public static $obj_type = array(
		self::BRANCH => 'branch',
		self::GROUP => 'group',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_menu_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idMenu, idResource', 'numerical', 'integerOnly'=>true),
			array('obj_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idMenu, obj_type, idResource', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idMenu' => 'Id Menu',
			'obj_type' => 'Obj Type',
			'idResource' => 'Id Resource',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idMenu',$this->idMenu);
		$criteria->compare('obj_type',$this->obj_type,true);
		$criteria->compare('idResource',$this->idResource);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreMenuFilter the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
