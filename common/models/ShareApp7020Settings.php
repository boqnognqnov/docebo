<?php

/**
 * Model for Share 7020 Plugin settings
 */
class ShareApp7020Settings extends CFormModel {

	const ON = 'on';
	const OFF = 'off';
	const ALLOW_ANY_LENGHT = 0;
	const VIDEO_DURATION = 1;
	const LIMIT_CHARACTERS = 'limit_characters';
	const VIDEO_DURATION_SMALL = 0;
	const VIDEO_DURATION_BIGGER = 1;
	const TYPE = 'share7020settings';
	const VIDEO_NOT_SHORTER_TIME = 'video_not_shorter_time';
	const VIDEO_NOT_LONGER_TIME = 'video_not_longer_time';
	const VIDEO_CONTRIBUTE = 'video_contribute';
	const INVITE_WATCH = 'invite_watch';
	const ALLOW_DOWNLOAD = 'allow_assets_dowload';
	const ALLOW_PRIVATE_ASSETS = 'allow_private_assets';
	const ENABLE_ANSWER_QUESTIONS = 'enable_answer_questions';

	public
			$video_contribute,
			$allow_any_length,
			$allow_comments,
			$invite_watch,
			$video_not_longer_time,
			$video_not_shorter_time,
			$documents_contribution,
			$other_contribution,
			$file_extensions,
			$allow_assets_dowload,
			$enable_answer_questions,
			$allow_private_assets = 'on';

	/**
	 * Constructor for the class
	 * @param bool $loadSettings - if true automatically will load settings into model
	 * @throws CException
	 */
	public function __construct($loadSettings = false) {
		if (!is_bool($loadSettings)) {
			throw new CException("Argument loadSettings must be boolean");
		}
		if ($loadSettings !== false) {
			$this->getSettings();
		}
	}

	/**
	 * Form validation rules
	 * @return array;
	 */
	public function rules() {
		$regex_pattern = '/^[\d]{2}:(?(?=[0-5]{1})[0-5]{1}[0-9]{1}):(?(?=[0-5]{1})[0-5]{1}[0-9]{1})$/';
		return array(
			array('file_extensions', 'length', 'max' => 500),
			array('file_extensions', 'filter', 'filter' => 'strip_tags'),
			array('documents_contribution, invite_watch, allow_assets_dowload,enable_answer_questions, ' . self::ALLOW_PRIVATE_ASSETS,
				'in',
				'range' =>
				array(
					self::ON,
					self::OFF
				),
				'message' => Yii::t('app7020', 'The {attribute} is set incorrect')
			),
			array(self::VIDEO_CONTRIBUTE,
				'in',
				'range' =>
				array(
					self::ALLOW_ANY_LENGHT,
					self::VIDEO_DURATION
				),
				'message' => Yii::t('app7020', 'The set {attribute} is incorrect')
			),
			// video_not_shorter_time and video_not_longer_time must be in format 99:59:59 (hh:mm:ss) or empty
			array(
				self::VIDEO_NOT_SHORTER_TIME,
				'match',
				'pattern' => $regex_pattern,
				'message' => Yii::t('app7020', 'The {attribute} format must be hh:mm:ss')
			),
			array(
				self::VIDEO_NOT_LONGER_TIME,
				'match',
				'pattern' => $regex_pattern,
				'message' => Yii::t('app7020', 'The {attribute} format must be hh:mm:ss')
			),
			 
			array(
				self::VIDEO_CONTRIBUTE,
				'validate_video_contribute'
			),
			array(
				self::VIDEO_NOT_SHORTER_TIME . ', ' . self::VIDEO_NOT_LONGER_TIME . ', file_extensions',
				'filter',
				'filter' => 'trim'
			),
		);
	}

	/**
	 * Static method to call instance of the model
	 * @param $loadSettings Bool - if true model settings will be loaded automaticly default false
	 * @return ShareApp7020Settings;
	 */
	public static function model($loadSettings = false) {
		return new self($loadSettings);
	}

	/**
	 * Attributed labels for the form
	 * @return array;
	 */
	public function attributeLabels() {
		return array(
			'allow_any_length' => Yii::t('app7020', 'Allow upload of videos of any length222'),
			'video_duration' => Yii::t('app7020', 'Set duration limits (not shorten/longer than)'),
			'documents_contribution' => '<span></span>' . Yii::t('app7020', 'Allow documents contribution'),
 			'invite_watch' => '<span></span>' . Yii::t('app7020', 'Invite to watch'),
			'allow_comments' => '<span></span>' . Yii::t('app7020', 'Allow comments'),
			'allow_assets_dowload' => '<span></span>' . Yii::t('app7020', 'Allow users to download assets source file'),
			'allow_private_assets' => '<span></span>' . Yii::t('app7020', 'Allow users to upload private assets'),
			'enable_answer_questions' => '<span></span>' . Yii::t('app7020', 'Enable contributor to answer Questions'),
		);
	}

	/**
	 * Validate video length is valid
	 * @return CFormModel error;
	 */
	public function validate_video_contribute($attribute, $params) {
		if ($this->video_contribute && (empty($this->video_not_longer_time) && empty($this->video_not_shorter_time))) {
			$this->addError(self::VIDEO_CONTRIBUTE, Yii::t('app7020', 'Please specify "Not shorter than" or "Not longer than"'));
		}
		if (!empty($this->video_not_longer_time) && !empty($this->video_not_shorter_time)) {
			$video_not_shorter_time = str_replace(':', '', $this->video_not_shorter_time);
			$video_not_longer_time = str_replace(':', '', $this->video_not_longer_time);
			if ($video_not_shorter_time > $video_not_longer_time) {
				$this->addError(self::VIDEO_CONTRIBUTE, Yii::t('app7020', ' "Video not shorter than" can NOT be greater than "Video not longer than"'));
			}
		}
	}

	/**
	 * Validate file extensions format entered is coma seperated
	 * @return CFormModel error;
	 */
	public function validate_file_extensions($attribute, $params) {
		if ($this->other_contribution == self::ON) {
			if (empty($this->file_extensions)) {
				$this->addError('file_extensions', Yii::t('app7020', 'Please specify files extensions comma separated (e.g. jpg, txt, rtf)'));
			}
			if (!preg_match('/^[-\w\s]+(?:,[-\w\s]*)*$/', $this->file_extensions)) {
				$this->addError('file_extensions', Yii::t('app7020', 'Invalid extensions entered, supported only comma separated (e.g. jpg, txt, rtf)'));
			}
		}
	}

	/**
	 * Loading settings from core_settings table
	 * @return void;
	 */
	public function getSettings() {
		$command = Yii::app()->db->createCommand()
				->select('*')
				->from('core_setting t')
				->where('t.value_type = :type', array(':type' => self::TYPE));
		$result = $command->queryAll();
		foreach ($result as $row) {
			if (property_exists($this, $row['param_name'])) {
				$this->{$row['param_name']} = $row['param_value'];
			}
		}
		return $this;
	}

	/**
	 * Save settings to core_settings table
	 * @return bool;
	 */
	public function save() {
		if (is_array($this->attributes)) {
			foreach ($this->attributes as $param => $value) {
				Settings::save($param, $value, self::TYPE);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get max Characters from DB table core_setting
	 * @return bool
	 */
	public static function canInviteGlobal() {
		$canInvite = Settings::get('invite_watch', self::OFF);
		if ($canInvite != self::OFF) {
			return true;
		}
		return false;
	}

	/**
	 * Check duration limits for contribution upload from DB table core_setting
	 * @param $duration Play time string  duration format
	 * @return bool
	 */
	public static function isValidDuration($duration) {
		// Check if there is any video dirattion limits
		// it should go IN this "if" clause if there IS any duration limit
		if (Settings::get(self::VIDEO_CONTRIBUTE)) {
			$minDurationLimit = Settings::get(self::VIDEO_NOT_SHORTER_TIME);
			$maxDurationLimit = Settings::get(self::VIDEO_NOT_LONGER_TIME);
			// Check if Video duration is under duration minimum 
			if ($minDurationLimit && !App7020Helpers::compareDuration($duration, $minDurationLimit)) {
				return self::VIDEO_DURATION_SMALL;
			}
			// Check if Video duration is over duration miximum 
			if ($maxDurationLimit && !App7020Helpers::compareDuration($maxDurationLimit, $duration)) {
				return self::VIDEO_DURATION_BIGGER;
			}
		}

		return true;
	}

	/**
	 * Check if Invite to Watch is set to "ON" in core_settings
	 * @return boolean
	 */
	public static function isInviteWatchGlobal() {
		return Settings::get(self::INVITE_WATCH) == self::ON ? true : false;
	}

	/**
	 * 
	 * @return boolean
	 */
	public static function isGlobalDownloadAccess() {
		return Settings::get(self::ALLOW_DOWNLOAD) == self::ON ? true : false;
	}

	/**
	 * Return valid boolean value of current stage of this setting
	 * @return boolean
	 */
	public static function isGlobalAllowPrivateAssets() {

		$setting = Settings::get(self::ALLOW_PRIVATE_ASSETS);
		if (is_null($setting)) {
			return true;
		}
		return Settings::get(self::ALLOW_PRIVATE_ASSETS) == self::ON ? true : false;
	}

	/**
	 *
	 * @return boolean
	 */
	public static function isEnableAnswerQuestionsGlobal() {
		return Settings::get(self::ENABLE_ANSWER_QUESTIONS) == self::ON ? true : false;
	}

}
