<?php

/**
 * This is the model class for table "dashboard_dashlet".
 *
 * The followings are the available columns in table 'dashboard_dashlet':
 * @property integer $id
 * @property string $handler
 * @property integer $cell
 * @property integer $position
 * @property integer $height
 * @property string $header
 * @property string $params
 *
 * The followings are the available model relations:
 * @property DashboardCell $dashboardCell
 */
class DashboardDashlet extends CActiveRecord
{


	public $search_input;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_dashlet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('handler', 'required'),
			array('cell, position, height', 'numerical', 'integerOnly'=>true),
			array('handler, header', 'length', 'max'=>255),
			array('params', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, handler, cell, position, height, header, params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dashboardCell' => array(self::BELONGS_TO, 'DashboardCell', 'cell'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'handler' => 'Handler',
			'cell' => 'Cell',
			'position' => 'Position',
			'height' => 'Height',
			'header' => 'Header',
			'params' => 'Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('handler',$this->handler,true);
		$criteria->compare('cell',$this->cell);
		$criteria->compare('position',$this->position);
		$criteria->compare('height',$this->height);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('params',$this->params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardDashlet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Normalize dashlets' CSS height
	 *
	 * @param integer $height
	 *
	 * @return integer
	 */
	public static function normalizeCssHeight($height) {
		$height 	= (int) $height 		> 0  ? (int) $height 	: "auto";
		return $height;
	}


	/**
	 * Create a new dashlet for FULL CATALOG, not part of any REAL layout; It will be used in VIRTUAL layout
	 *
	 */
	public static function createFullCatalogDashlet() {
		$descriptor = DashletCatalog::descriptor();
		$model = new self();
		$model->cell = null;
		$model->handler = $descriptor->handler;
		$model->header = '';
		$paramsArray = DashletCatalog::getDefaultParameters();
		unset($paramsArray['display_number_items_fullist']);
		$paramsArray['selected_sorting_fields'] = array('name');
		$model->params = json_encode($paramsArray);
		$model->save();
		return $model;
	}


	/**
	 * Create a new dashlet for FULL MY COURSES, not part of any REAL layout; It will be used in VIRTUAL layout
	 *
	 */
	public static function createFullMyCoursesDashlet() {
		$descriptor = DashletMyCourses::descriptor();
		$model = new self();
		$model->cell = null;
		$model->handler = $descriptor->handler;
		$model->header = '';
		$paramsArray = DashletMyCourses::getDefaultParameters();
		$paramsArray['aggregate_labels'] = true;
		$paramsArray['selected_sorting_fields'] = array('name');
		$model->params = json_encode($paramsArray);
		$model->save();
		return $model;
	}



	/**
	 * Return THIS dashlet instance descriptor
	 *
	 * @return DashletDescriptor
	 */
	public function getDescriptor() {
		// Do some caching
		static $cache = array();
		if (isset($cache[$this->id]))
			return $cache[$this->id];

		// We need the CLASS of this handler, i.e. its handler, which is path.of.alias.to.Class, so we get the last element
		$handler = $this->handler;
		$tmp = explode('.', $this->handler);
		$class = array_pop($tmp);

		// .. then call the static descriptor() method
		$cache[$this->id] = $class::descriptor();
		return $cache[$this->id];

	}


}
