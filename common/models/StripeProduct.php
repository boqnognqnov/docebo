<?php

/**
 * This is the model class for table "stripe_product".
 *
 * The followings are the available columns in table 'stripe_product':
 * @property integer $id
 * @property integer $id_product_lms
 * @property string $product_type
 * @property string $domain
 * @property string $id_product_stripe
 * @property string $id_sku_stripe
 * @property float $price_stripe
 *
 * The followings are the available model relations:
 */
class StripeProduct extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'stripe_product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_product_lms, product_type, id_product_stripe, id_sku_stripe, price_stripe', 'required'),
            array('id_product_lms', 'numerical', 'integerOnly'=>true),
            array('price_stripe', 'numerical', 'integerOnly'=>false),
            array('id_product_stripe, id_sku_stripe', 'length', 'max'=>50),
            array('domain', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, id_product_lms, product_type, id_product_stripe, id_sku_stripe, price_stripe', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_product_lms' => 'LMS Product ID',
            'product_type' => 'Product Type',
            'domain' => 'Domain',
            'id_product_stripe' => 'Stripe Product ID',
            'id_sku_stripe' => 'Stripe SKU ID',
            'price_stripe' => 'Price Stripe',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('id_product_lms',$this->id_product_lms);
        $criteria->compare('product_type',$this->product_type,true);
        $criteria->compare('domain',$this->domain,true);
        $criteria->compare('id_product_stripe',$this->id_product_stripe,true);
        $criteria->compare('id_sku_stripe',$this->id_sku_stripe,true);
        $criteria->compare('price_stripe',$this->price_stripe,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StripeCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
