<?php

/**
 * This is the model class for table "learning_testtrack_quest".
 *
 * The followings are the available columns in table 'learning_testtrack_quest':
 * @property integer $idTrack
 * @property integer $idQuest
 * @property integer $page
 */
class LearningTesttrackQuest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrackQuest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack_quest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTrack, idQuest, page', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idQuest, page', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idQuest' => 'Id Quest',
			'page' => 'Page',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idQuest',$this->idQuest);
		$criteria->compare('page',$this->page);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	
	
	
	/**
	 * Retrieve existing Question tracking record or create new one
	 * 
	 * @param number $idTrack
	 * @param number $idQuest
	 * @param number $page
	 * @return object LearningTesttrackQuest
	 */
	public static function getTrack($idTrack, $idQuest, $page) {
		$trackQuestion = LearningTesttrackQuest::model()->findByPk(array('idTrack' => $idTrack, 'idQuest' => $idQuest));
		if (!$trackQuestion) {
			$trackQuestion = new LearningTesttrackQuest();
			$trackQuestion->idTrack = $idTrack;
			$trackQuestion->idQuest = $idQuest;
			$trackQuestion->page = $page;
			$trackQuestion->save();
		}
		return $trackQuestion;
	}
	
	
	
}