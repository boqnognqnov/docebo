<?php

/**
 * This is the model class for table "learning_userlimit_login_log".
 *
 * The followings are the available columns in table 'learning_userlimit_login_log':
 * @property integer $id
 * @property integer $userIdst
 * @property string $first_period_login
 */
class LearningUserlimitLoginLog extends CActiveRecord
{

	//used for search purposes
	public $search_input;
	public $search_period;


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningUserlimitLoginLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_userlimit_login_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userIdst', 'required'),
			array('userIdst', 'numerical', 'integerOnly'=>true),
			array('first_period_login', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userIdst, first_period_login', 'safe', 'on'=>'search'),

			array('search_period, search_input', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'userIdst'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('first_period_login medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userIdst' => 'User Idst',
			'first_period_login' => 'First Period Login',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userIdst',$this->userIdst);
		$criteria->compare('first_period_login',Yii::app()->localtime->fromLocalDateTime($this->first_period_login),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	/**
	 * Using SQL Dataprovider because we are using UNION
	 *
	 * @param number $pagesize
	 * @return CSqlDataProvider
	 */
	public function reportSqlDataProvider($pagesize = 5) {

		$command = Yii::app()->db->createCommand();

		if ($this->search_input)
			$command->andWhere('CONCAT(u.userid, u.firstname, u.lastname) LIKE "%' . $this->search_input . '%"');

		if ($this->search_period) {
			$items = explode('|', $this->search_period);
			$from = $items[1];
			$to = $items[2];
			$command->andWhere('first_period_login >= "'.$from.'" AND first_period_login <= "'.$to.'"');
		}

		// Ignore "staff.docebo" username
		$command->andWhere("UPPER(u.userid) NOT LIKE UPPER('%staff.docebo%')");

		$command->from($this->tableName() . ' l');

		// ^^^^^ Until now all commands are the same

		// Clone the command and use it for the union (same WHERE, different JOIN)
		$unionCommand = clone $command;

		$command->join(CoreUser::model()->tableName() . " u", 'l.userIdst=u.idst');
		$unionCommand->join(CoreDeletedUser::model()->tableName() . " u", 'l.userIdst=u.idst');

		// Clone commands to use them later for DATA (same WHERE, same JOINs)
		$dataCommand = clone $command;
		$dataUnionCommand = clone $unionCommand;

		// Counters
		$command->select('count(*) c');
		$unionCommand->select('count(distinct(idst))');
		$command->union($unionCommand->getText());
		// Get counters (2 selects, union!)
		$counts = $command->queryAll();
		$totalCount = 0;
		foreach ($counts as $row) {
			$totalCount += $row['c'];
		}

		// Selects for DATA
		$dataCommand->select('u.idst as idst, u.userid as userid, u.firstname as firstname, u.lastname as lastname, l.id as id, l.first_period_login as first_period_login, u.lastenter as lastenter');
		$dataUnionCommand->select('distinct(u.idst) as idst, u.userid as userid, u.firstname as firstname, u.lastname as lastname, l.id as id, l.first_period_login as first_period_login, u.lastenter as lastenter');
		$dataCommand->union($dataUnionCommand->getText());
		$dataCommand->order('first_period_login ASC');
		$dataUnionCommand->order('first_period_login ASC');

		//Build joins to get the user related branches
		$fJoin = 'core_group_members group_member';
		$fJoinCond = 't.idst=group_member.idstMember';
		$sJoin = 'core_org_chart_tree coct';
		$sJoinCond = 'coct.idst_oc = group_member.idst OR coct.idst_ocd = group_member.idst';
		$tJoin = 'core_org_chart coc';
		$tJoinCond = 'coc.id_dir = coct.`idOrg` AND lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"';

		$branchesCommand = Yii::app()->db->createCommand();
		$branchesCommand->select("group_concat(group_member.idst) AS branches, GROUP_CONCAT(DISTINCT coct.idOrg) AS client, t.*");
		$branchesCommand->from('(' . $dataCommand->getText() . ') as t');
		$branchesCommand->leftJoin($fJoin, $fJoinCond);
		$branchesCommand->leftJoin($sJoin, $sJoinCond);
		$branchesCommand->leftJoin($tJoin, $tJoinCond);
		//$branchesCommand->andWhere('coc.translation != ""');
		$branchesCommand->group('t.id');
		$branchesCommand->order('t.first_period_login ASC');
		
		// Paginate ???
		if (!$pagesize) {
			$pagination = false;
		}
		else {
			$pagination = array(
					'pageSize' => $pagesize,
			);
		}
		
		// Get the SQL provider; Pass the DATA command (union-ed)
		$dataProvider = new CSqlDataProvider($branchesCommand->getText(), array(
			'totalItemCount'=>$totalCount,
			'pagination'=> $pagination,
		));

		return $dataProvider;


	}



	/**
	 * NOT USED!!!
	 *
	 * @param number $pagesize
	 * @return CActiveDataProvider
	 */
	public function reportDataProvider($pagesize = 5) {

		$this->reportSqlDataProvider();

		$criteria=new CDbCriteria;

		if ($this->search_input) {
			$criteria->addCondition('
                CONCAT(user.firstname, user.lastname)
                LIKE :search_input
			');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}

		if ($this->search_period) {
			$items = explode('|', $this->search_period);
			$from = $items[1];
			$to = $items[2];

			$criteria->addCondition('first_period_login >= :from AND first_period_login < :to');
			$criteria->params[':from'] = $from;
			$criteria->params[':to'] = $to;
		}

		$criteria->with = array('user');
		$criteria->together = true;

		if (!$pagesize) {
			$pagination = false;
		}
		else {
			$pagination = array(
                'pageSize' => $pagesize,
			);
		}

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
             'pagination'=> $pagination,
            )
		);
	}

	public static function activeUsersForPeriod($from, $to)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 'first_period_login >= :from AND first_period_login <= :to';
		$criteria->params = array(
			':from' => $from,
			':to' => $to,
		);
		return self::model()->count($criteria);
	}
}