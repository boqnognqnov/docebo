<?php

/**
 * This is the model class for table "learning_test".
 *
 * The followings are the available columns in table 'learning_test':
 * @property integer $idTest
 * @property integer $author
 * @property string $title
 * @property string $description
 * @property integer $point_type
 * @property integer $point_assignment
 * @property integer $show_users_avg
 * @property double $point_required
 * @property integer $display_type
 * @property integer $order_type
 * @property integer $shuffle_answer
 * @property integer $question_random_number
 * @property integer $save_keep
 * @property integer $mod_doanswer
 * @property integer $can_travel
 * @property integer $show_only_status
 * @property integer $show_score
 * @property integer $show_score_cat
 * @property integer $show_doanswer
 * @property integer $show_solution
 * @property integer $time_dependent
 * @property integer $time_assigned
 * @property integer $penality_test
 * @property double $penality_time_test
 * @property integer $penality_quest
 * @property double $penality_time_quest
 * @property integer $max_attempt
 * @property integer $hide_info
 * @property string $order_info
 * @property integer $use_suspension
 * @property string $suspension_num_attempts
 * @property string $suspension_num_hours
 * @property integer $suspension_prerequisites
 * @property string $chart_options
 * @property integer $mandatory_answer
 * @property integer $score_max
 * @property string $show_solution_date
 *
 * @package docebo.models
 * @subpackage table
 */
class LearningTest extends CActiveRecord
{

	const DISPLAY_TYPE_GROUPED = 0;
	const DISPLAY_TYPE_SINGLE = 1;

	const ORDER_TYPE_SEQUENCE = 0;
	const ORDER_TYPE_RANDOM = 1;
	const ORDER_TYPE_RANDOM_SUBGROUP = 2;
	const ORDER_TYPE_RANDOM_CATEGORY = 3;

	const SHUFFLE_ANSWER_SEQUENCE = 0;
	const SHUFFLE_ANSWER_RANDOM = 1;

	const POINT_TYPE_NORMAL = 0;
	const POINT_TYPE_PERCENT = 1;

	const POINT_ASSIGNMENT_DO_NOTHING = 0;
	const POINT_ASSIGNMENT_TEST_PM_DIFFICULT = 1;
	const POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL = 2;
	const POINT_ASSIGNMENT_TEST_PM_MANUAL = 3;
	const POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE = 4;

	const TIME_NO = 0;
	const TIME_YES = 1;
	const TIME_YES_QUEST = 2;

	const SHOW_SOLUTION_YES = 0;
	const SHOW_SOLUTION_NO = 1;
	const SHOW_SOLUTION_YES_IF_PASSED = 2;
	const SHOW_SOLUTION_YES_FROM_DATE = 3;

	const SHOW_ANSWERS_YES = 0;
	const SHOW_ANSWERS_NO = 1;
	const SHOW_ANSWERS_YES_IF_PASSED = 2;

	public $orgModel; //used to keep the model for usage from different methods

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('author, point_type, point_assignment, display_type, order_type, shuffle_answer, question_random_number, save_keep, mod_doanswer, can_travel, show_only_status, show_score, show_score_cat, show_doanswer, show_solution, time_dependent, time_assigned, penality_test, penality_quest, max_attempt, hide_info, use_suspension, suspension_prerequisites, mandatory_answer, score_max', 'numerical', 'integerOnly'=>true),
			array('point_required, penality_time_test, penality_time_quest', 'numerical'),
			array('title', 'length', 'max'=>255),
			array('suspension_num_attempts, suspension_num_hours', 'length', 'max'=>10),
			array('show_solution_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTest, author, title, description, point_type, point_assignment, point_required, display_type, order_type, shuffle_answer, question_random_number, save_keep, mod_doanswer, can_travel, show_only_status, show_score, show_score_cat, show_doanswer, show_solution, time_dependent, time_assigned, penality_test, penality_time_test, penality_quest, penality_time_quest, max_attempt, hide_info, order_info, use_suspension, suspension_num_attempts, suspension_num_hours, suspension_prerequisites, chart_options, mandatory_answer, score_max', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'organization' => array(self::BELONGS_TO, 'LearningOrganization', '', 'on'=>'t.idTest=organization.idResource AND organization.objectType=:objectType',
			'params' => array(':objectType'=>LearningOrganization::OBJECT_TYPE_TEST)),
			'feedbacks' => array(self::HAS_MANY, 'LearningTestFeedback', 'id_test'),
			'learning_test_quest_rel' => array(self::HAS_MANY, 'LearningTestQuestRel', 'id_test'),
			'questions' => array(self::MANY_MANY, 'LearningTestquest', 'learning_test_quest_rel(id_question, id_test)',
				'select' => 'questions_questions.page as page, questions_questions.id_test as idTest, questions_questions.sequence as sequence, questions.*',
				'order' => 'questions_questions.sequence ASC'),
			//LearningTestquest::getQuestionTypesList()
			'questionsOnly' => array(self::MANY_MANY, 'LearningTestquest', 'learning_test_quest_rel(id_question, id_test)',
				'select' => 'questionsOnly_questionsOnly.page as page, questionsOnly_questionsOnly.id_test as idTest, questionsOnly_questionsOnly.sequence as sequence, questionsOnly.*',
				'order' => 'questionsOnly_questionsOnly.sequence ASC',
				'condition' => 'questionsOnly.type_quest <> :type_title && questionsOnly.type_quest <> :type_break_page',
				'params' => array(':type_title' => LearningTestquest::QUESTION_TYPE_TITLE, ':type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE)),
			'tracks' => array(self::HAS_MANY, 'LearningTesttrack', 'idTest'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTest' => 'Id Test',
			'author' => 'Author',
			'title' => 'Title',
			'description' => 'Description',
			'point_type' => 'Point Type',
			'point_assignment' => 'Point Assignment',
			'point_required' => 'Point Required',
			'display_type' => 'Display Type',
			'order_type' => 'Order Type',
			'shuffle_answer' => 'Shuffle Answer',
			'question_random_number' => 'Question Random Number',
			'save_keep' => 'Save Keep',
			'mod_doanswer' => 'Mod Doanswer',
			'can_travel' => 'Can Travel',
			'show_only_status' => 'Show Only Status',
			'show_score' => 'Show Score',
			'show_score_cat' => 'Show Score Cat',
			'show_doanswer' => 'Show Doanswer',
			'show_solution' => 'Show Solution',
			'time_dependent' => 'Time Dependent',
			'time_assigned' => 'Time Assigned',
			'penality_test' => 'Penality Test',
			'penality_time_test' => 'Penality Time Test',
			'penality_quest' => 'Penality Quest',
			'penality_time_quest' => 'Penality Time Quest',
			'max_attempt' => 'Max Attempt',
			'hide_info' => 'Hide Info',
			'order_info' => 'Order Info',
			'use_suspension' => 'Use Suspension',
			'suspension_num_attempts' => 'Suspension Num Attempts',
			'suspension_num_hours' => 'Suspension Num Hours',
			'suspension_prerequisites' => 'Suspension Prerequisites',
			'chart_options' => 'Chart Options',
			'mandatory_answer' => 'Mandatory Answer',
			'score_max' => 'Score Max',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTest',$this->idTest);
		$criteria->compare('author',$this->author);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('point_type',$this->point_type);
		$criteria->compare('point_assignment',$this->point_assignment);
		$criteria->compare('point_required',$this->point_required);
		$criteria->compare('display_type',$this->display_type);
		$criteria->compare('order_type',$this->order_type);
		$criteria->compare('shuffle_answer',$this->shuffle_answer);
		$criteria->compare('question_random_number',$this->question_random_number);
		$criteria->compare('save_keep',$this->save_keep);
		$criteria->compare('mod_doanswer',$this->mod_doanswer);
		$criteria->compare('can_travel',$this->can_travel);
		$criteria->compare('show_only_status',$this->show_only_status);
		$criteria->compare('show_score',$this->show_score);
		$criteria->compare('show_score_cat',$this->show_score_cat);
		$criteria->compare('show_doanswer',$this->show_doanswer);
		$criteria->compare('show_solution',$this->show_solution);
		$criteria->compare('time_dependent',$this->time_dependent);
		$criteria->compare('time_assigned',$this->time_assigned);
		$criteria->compare('penality_test',$this->penality_test);
		$criteria->compare('penality_time_test',$this->penality_time_test);
		$criteria->compare('penality_quest',$this->penality_quest);
		$criteria->compare('penality_time_quest',$this->penality_time_quest);
		$criteria->compare('max_attempt',$this->max_attempt);
		$criteria->compare('hide_info',$this->hide_info);
		$criteria->compare('order_info',$this->order_info,true);
		$criteria->compare('use_suspension',$this->use_suspension);
		$criteria->compare('suspension_num_attempts',$this->suspension_num_attempts,true);
		$criteria->compare('suspension_num_hours',$this->suspension_num_hours,true);
		$criteria->compare('suspension_prerequisites',$this->suspension_prerequisites);
		$criteria->compare('chart_options',$this->chart_options,true);
		$criteria->compare('mandatory_answer',$this->mandatory_answer);
		$criteria->compare('score_max',$this->score_max);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'learningObjects' => array(
				'class' => 'LearningObjectsBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idPropertyName' => 'idTest',
				'titlePropertyName' => 'title'
			)
		);
	}


/*
	//no more needed, this has been reached through DB foreign keys
	public function beforeDelete() {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		try {
			$questions = LearningTestquest::model()->findAllByAttributes(array('idTest' => $this->getPrimaryKey()));
			if (is_array($questions)) {
				foreach ($questions as $question) {
					$rs = $question->delete();
					if ($rs === FALSE) {
						throw new CException('Error while deleting test questions');
					}
				}
			}
			if ($transaction) { $transaction->commit(); }
		} catch (CException $e) {
			if ($transaction) { $transaction->rollback(); }
			throw $e;//return false;
		}
		parent::beforeDelete();
	}
*/

	public function getPointAssingmentTypes() {
		return array(
			self::POINT_ASSIGNMENT_DO_NOTHING => Yii::t('preassessment', '_DO_NOTHING'),
			self::POINT_ASSIGNMENT_TEST_PM_DIFFICULT => Yii::t('test', '_TEST_PM_DIFFICULT'),
			self::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL => Yii::t('test', '_TEST_PM_EQUALTOALL', array('{number_questions}' => count($this->getQuestions()))),
			self::POINT_ASSIGNMENT_TEST_PM_MANUAL => Yii::t('test', '_TEST_PM_MANUAL'),
			self::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE => Yii::t('test', 'Same point for each <strong>visible</strong> question (total score / {number_questions})', array('{number_questions}' => $this->getNumberOfQuestionsForStudent())),
		);
	}

	public function getPointTypes() {
		return array(
			self::POINT_TYPE_NORMAL => Yii::t('test', '_TEST_PM1_POINT'),
			self::POINT_TYPE_PERCENT => Yii::t('test', '_TEST_PM1_PERC'),
		);
	}
	public function getDisplayType() {
		return ($this->time_dependent == 2
			? LearningTest::DISPLAY_TYPE_SINGLE
			: $this->display_type);
	}


	public function getTotalPageNumber() {

		if ($this->order_type == self::ORDER_TYPE_RANDOM_CATEGORY) {

			if ($this->getDisplayType() == self::DISPLAY_TYPE_GROUPED) {
				return 1;
			} else {
				$orderInfo = CJSON::decode($this->order_info);
				$totalPages = 0;
				foreach ($orderInfo as $value) {
					$totalPages += (int)$value['selected'];
				}
				return $totalPages;
			}
		}


		if ($this->order_type == self::ORDER_TYPE_RANDOM_SUBGROUP) {

			if ($this->getDisplayType() == self::DISPLAY_TYPE_GROUPED) {
				$totalPages = 1;
			} else {
				$totalPages = (int)$this->question_random_number; //TO DO: what about if "question_random_number == 0 ?
			}
			return $totalPages;
		}


		//at this point the 'order type' can only be "ORDER_TYPE_SEQUENCE" or "ORDER_TYPE_RANDOM", both managed in the same way
		if ($this->getDisplayType() == self::DISPLAY_TYPE_GROUPED) {

			//TO DO: check if pages are correctly stored in learning_testquest table
			$result = Yii::app()->db->createCommand()
				->select("MAX(qr.page) AS num_pages")
				->from(LearningTestquest::model()->tableName()." q")
				->join(LearningTestQuestRel::model()->tableName()." qr", "qr.id_question = q.idQuest")
				->where("qr.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
				->queryRow();
			$totalPages = (int)$result['num_pages'];

		} else {

			$result = Yii::app()->db->createCommand()
				->select("COUNT(*) AS num_pages")
				->from(LearningTestquest::model()->tableName()." q")
				->join(LearningTestQuestRel::model()->tableName()." qr", "qr.id_question = q.idQuest")
				->where("qr.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
				->andWhere("q.type_quest <> :qtype_1", array(':qtype_1' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
				->andWhere("q.type_quest <> :qtype_2", array(':qtype_2' => LearningTestquest::QUESTION_TYPE_TITLE))
				->queryRow();
			$totalPages = (int)$result['num_pages'];

		}
		return $totalPages;
	}



	/**
	 *
	 * @return int the number of questions in the test that the student will need to take
     * (can be less than the total number of questions - for example when the random options is used)
	 */
	public function getNumberOfQuestionsForStudent() {
		switch ($this->order_type) {
			case $this::ORDER_TYPE_RANDOM_SUBGROUP:
				$count = $this->question_random_number;
			break;
			case $this::ORDER_TYPE_RANDOM_CATEGORY:
				$orderInfo = CJSON::decode($this->order_info);
				$count = 0;
				foreach ($orderInfo as $info)
					$count += (int)$info['selected'];
			break;
			default:
				$rs = Yii::app()->db->createCommand()
					->select("COUNT(*) AS num_questions")
					->from(LearningTestquest::model()->tableName().' quest')
					->join(LearningTestQuestRel::model()->tableName().' rel', 'rel.id_question = quest.idQuest')
					->where("rel.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
					->andWhere("type_quest <> :type_title", array(':type_title' => LearningTestquest::QUESTION_TYPE_TITLE))
					->andWhere("type_quest <> :type_break_page", array(':type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
					->group("rel.id_test")
					->queryRow();
				$count = (int)$rs['num_questions'];
			break;
		}
		return $count;
	}



	/**
	 *
	 * @return int the total number of questions in the test
	 */
	public function getNumberOfQuestions() {
		$rs = Yii::app()->db->createCommand()
			->select("COUNT(*) AS num_questions")
			->from(LearningTestquest::model()->tableName().' quest')
			->join(LearningTestQuestRel::model()->tableName().' rel', 'rel.id_question = quest.idQuest')
			->where("rel.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
			->andWhere("type_quest <> :type_title", array(':type_title' => LearningTestquest::QUESTION_TYPE_TITLE))
			->andWhere("type_quest <> :type_break_page", array(':type_break_page' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
			->group("rel.id_test")
			->queryRow();
		$count = (int)$rs['num_questions'];
		return $count;
	}



	/**
	 * Retrieve test questions
	 * @param boolean $all if set to true, titles and breakpages will be retrieved too
	 * @return array the list of test questions
	 */
	public function getQuestions($all = false) {
		if ($all)
			return $this->questions;
		else
			return $this->questionsOnly;
	}



	/**
	 * Retrieve questions contained in a given page, based on test properties
	 * @param int $pageNumber the index of teh page
	 * @param int $idUser id of the user of which track data is retrieved
	 * @return array a list of Active Records (each AR is a question of the page)
	 */
	public function getQuestionsForPage($pageNumber, $idUser = false) {

		if ($this->isNewRecord) { return false; } //the test has not yet been saved in DB

		if (empty($idUser)) { $idUser = Yii::app()->user->id; }

		// cast display to one quest at time if the time is by quest
		if ($this->time_dependent == 2) {
			$displayType = LearningTest::DISPLAY_TYPE_SINGLE;
		} else {
			$displayType = $this->display_type;
		}
		$event = new DEvent($this, array('this' => $this));
		Yii::app()->event->raise('CustomCriteriaToGetIncorrectAnswers', $event);

		$eventForStudyMode = new DEvent($this, array('this' => $this));
		Yii::app()->event->raise('CheckForStudyMode', $eventForStudyMode);

		if(!$event->shouldPerformAsDefault() && $eventForStudyMode->shouldPerformAsDefault()){
			$questionsCriteria = $event->return_value;
		}
		else {
			$questionsCriteria = new CDbCriteria();
			$questionsCriteria->join = "INNER JOIN " . LearningTestQuestRel::model()->tableName() . " qr ON qr.id_question = t.idQuest";
			$questionsCriteria->addCondition('qr.id_test = :id_test');
			$questionsCriteria->params[':id_test'] = $this->getPrimaryKey();
			$questionsCriteria->addInCondition('type_quest', LearningTestquest::getQuestionTypesList());
		}
		//retrieve already displayed questions, if any
		//NOTE: trying to avoid idTrack column
		$t1 = LearningTesttrackQuest::model()->tableName();
		$t2 = LearningTesttrack::model()->tableName();
		$already = Yii::app()->db->createCommand()
			->select("t1.idQuest, t1.quest_sequence")
			->from($t1." t1")
			->join($t2." t2", "t1.idTrack = t2.idTrack")
			->where("t1.page = :page_number", array(':page_number' => (int)$pageNumber))
			->andWhere("t2.idUser = :id_user", array(':id_user' => (int)$idUser))
			->andWhere("t2.idTest = :id_test", array(':id_test' => $this->getPrimaryKey()))
			->order("t1.quest_sequence ASC")
			->queryAll();

		if (!empty($already))
		{
			$alreadyOrder = array();
			$qlist = array();
			foreach ($already as $record) {
				$idQuestion = (int)$record['idQuest'];
				$qlist[] = $idQuestion;
				$alreadyOrder[$idQuestion] = (int)$record['quest_sequence'];
			}

			$questionsCriteria->addInCondition('t.idQuest', $qlist);
			$questionsCriteria->order = 'qr.sequence ASC';

			$questionsList = LearningTestquest::model()->findAll($questionsCriteria);

			//reproduce original displayed question ordering
			usort($questionsList, function($a, $b) use (&$alreadyOrder) {
				$index_a = $alreadyOrder[$a->idQuest];
				$index_b = $alreadyOrder[$b->idQuest];
				if ($index_a == $index_b) return 0;
				return ($index_a < $index_b ? -1 : 1);
			});

			return $questionsList;
		}


		if ($displayType == LearningTest::DISPLAY_TYPE_GROUPED) {

			// Respect page number
			switch($this->order_type) {
				case LearningTest::ORDER_TYPE_SEQUENCE : {

					// sequential
					$questionsCriteria->addCondition('qr.page = :page_number');
					$questionsCriteria->params[':page_number'] = $pageNumber;
					$questionsCriteria->order = 'qr.sequence ASC';

					return LearningTestquest::model()->findAll($questionsCriteria);
				};break;
				case LearningTest::ORDER_TYPE_RANDOM : {

					// shuffle
					$questionsCriteria->addCondition('qr.page = :page_number');
					$questionsCriteria->addCondition('type_quest <> :title_type');
					$questionsCriteria->params[':page_number'] = $pageNumber;
					$questionsCriteria->params[':title_type'] = LearningTestquest::QUESTION_TYPE_TITLE;

					$questionsList = LearningTestquest::model()->findAll($questionsCriteria);
					shuffle($questionsList);
					return $questionsList;

				};break;
				case LearningTest::ORDER_TYPE_RANDOM_SUBGROUP : {

					// Random X quest on a total of N quest
					$questionsCriteria->addCondition('type_quest <> :question_type_1');
					$questionsCriteria->addCondition('type_quest <> :question_type_2');
					$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
					$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

					$questionsList = LearningTestquest::model()->findAll($questionsCriteria);
					shuffle($questionsList);
					return array_slice($questionsList, 0, $this->question_random_number);

				};break;
				case LearningTest::ORDER_TYPE_RANDOM_CATEGORY : {

					// Random X quest on a set of selected categories, each of N(idCategory) quests
					$orderInfo = CJSON::decode($this->order_info);

					$questionsCriteria->addCondition('type_quest <> :question_type_1');
					$questionsCriteria->addCondition('type_quest <> :question_type_2');
					$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
					$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

					//first step: extract all questions from the test and shuffle them
					$questionsList = LearningTestquest::model()->findAll($questionsCriteria);
					shuffle($questionsList);

					//second step: count how many question we need for each category and in total
					$counters = array();
					$total = 0;
					foreach ($orderInfo as $info) {
						$counters[(int)$info['id_category']] = (int)$info['selected'];
						$total += (int)$info['selected'];
					}

					//third step: extract questions from shuffled pool
					$output = array();
					foreach ($questionsList as $question) {
						$idCategory = (int)$question->idCategory;
						if (isset($counters[$idCategory]) && $counters[$idCategory] > 0) {
							$output[] = $question;
							$counters[$idCategory]--;
							$total--;
						}
						if ($total <= 0) { break; } //all needed questions have been collected, exit from foreach cycle
					}

					return $output;

				};break;
			}
		} elseif ($displayType == LearningTest::DISPLAY_TYPE_SINGLE) { //note: this if condition may be unnecessary

			// One question per page

			$questionsCriteria->addCondition('type_quest <> :question_type_1');
			$questionsCriteria->addCondition('type_quest <> :question_type_2');
			$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
			$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

			// Retrive question alredy displayed
			//NOTE: trying to avoid idTrack column
			$t1 = LearningTesttrackQuest::model()->tableName();
			$t2 = LearningTesttrack::model()->tableName();
			$questionsSeen = Yii::app()->db->createCommand()
				->select("t1.idQuest")
				->from($t1." t1")
				->join($t2." t2", "t1.idTrack = t2.idTrack")
				->where("t2.idUser = :id_user", array(':id_user' => (int)$idUser))
				->andWhere("t2.idTest = :id_test", array(':id_test' => $this->getPrimaryKey()))
				->queryAll();

			if (!empty($questionsSeen)) {
				$qlist = array();
				foreach ($questionsSeen as $record) {
					$qlist[] = (int)$record['idQuest'];
				}
				$questionsCriteria->addNotInCondition('idQuest', $qlist);
			}

			switch($this->order_type) {
				case LearningTest::ORDER_TYPE_SEQUENCE : {

					// Sequential
					$questionsCriteria->order = 'qr.sequence ASC';
					$questionsCriteria->offset = 0;
					$questionsCriteria->limit = 1;

					return LearningTestquest::model()->findAll($questionsCriteria);

				};break;
				case LearningTest::ORDER_TYPE_RANDOM :
				case LearningTest::ORDER_TYPE_RANDOM_SUBGROUP : {

					// Shuffle
					$questionsCriteria->addCondition('type_quest <> :title_type');
					$questionsCriteria->params[':title_type'] = LearningTestquest::QUESTION_TYPE_TITLE;

					//count how many questions have not been seen
					/*
					$countCriteria = $questionsCriteria;
					$countCriteria->select = 'COUNT(*) AS num_questions';
					$maxOffset = (int)LearningTestquest::model()->find($countCriteria)->num_questions;
					*/
					//TO DO: try to do this in a better way
					$countCriteria = $questionsCriteria;
					$list = LearningTestquest::model()->findAll($countCriteria);
					$maxOffset = count($list);

					//pick a random unseen question
					$questionsCriteria->offset = rand(0, $maxOffset-1);
					$questionsCriteria->limit = 1;

					return LearningTestquest::model()->findAll($questionsCriteria);

				};break;
				case LearningTest::ORDER_TYPE_RANDOM_CATEGORY : {

					$orderInfo = CJSON::decode($this->order_info);

					$counters = array();
					$total = 0;
					foreach ($orderInfo as $info)
					{
						if($info['selected'] > 0)
							$counters[(int)$info['id_category']] = (int)$info['selected'];
						$total += (int)$info['selected'];
					}

					//retrieve already seen questions, but subdivided by cateogry
					$t1 = LearningTesttrackQuest::model()->tableName();
					$t2 = LearningTestquest::model()->tableName();
					$t3 = LearningTesttrack::model()->tableName();
					$t4 = LearningTestQuestRel::model()->tableName();
					$questSeen = Yii::app()->db->createCommand()
						->select("tq.idCategory, tq.idQuest")
						->from($t1." ttq")
						->join($t4." qr", "qr.id_question = ttq.idQuest AND qr.id_test = :idTest", array(':idTest' => $this->getPrimaryKey()))
						->join($t2." tq", "ttq.idQuest = tq.idQuest")
						->join($t3." tt", "tt.idTrack = ttq.idTrack")
						->where("tt.idUser = :id_user", array(':id_user' => (int)$idUser))
						->andWhere("tt.idTest = :id_test", array(':id_test' => $this->getPrimaryKey()))
						->queryAll();
					//for every retrieved category, find the number of non-seen questions
					$selectedCategories = array();
					$questSeenList = array();

					foreach($questSeen as $quest)
					{
						if(isset($counters[$quest['idCategory']]))
						{
							$counters[$quest['idCategory']]--;
							if($counters[$quest['idCategory']] == 0)
								unset($counters[$quest['idCategory']]);
						}

						$questSeenList[] = $quest['idQuest'];
					}

					foreach($counters as $idCategory => $quest_to_show)
					{
						$selectedCategories[] = array(
							'idCategory' => $idCategory,
							'num_questions' => $quest_to_show
						);
					}

					//choose a random category and extract a random question from that category
					$index = rand(0, count($selectedCategories)-1);
					$questionsCriteria->addCondition("idCategory = :id_category");
					$questionsCriteria->params[':id_category'] = $selectedCategories[$index]['idCategory'];
					if(!empty($questSeenList))
						$questionsCriteria->addNotInCondition("idQuest", $questSeenList);
					$questionsCriteria->order = "RAND()";
					$questionsCriteria->limit = 1;

					return LearningTestquest::model()->findAll($questionsCriteria);

				};break;
			}
		}
	}

	public function getPreviewQuestionsForPage($pageNumber, $order) {

		$order = CJSON::decode($order, true);

		// cast display to one quest at time if the time is by quest
		if ($this->time_dependent == 2) {
			$displayType = LearningTest::DISPLAY_TYPE_SINGLE;
		} else {
			$displayType = $this->display_type;
		}

		$questionsCriteria = new CDbCriteria();
		$questionsCriteria->join = "INNER JOIN " . LearningTestQuestRel::model()->tableName() . " qr ON qr.id_question = t.idQuest";
		$questionsCriteria->addCondition('qr.id_test = :id_test');
		$questionsCriteria->params[':id_test'] = $this->getPrimaryKey();
		$questionsCriteria->addInCondition('type_quest', LearningTestquest::getQuestionTypesList());

		if ($displayType == LearningTest::DISPLAY_TYPE_GROUPED) {

			// Respect page number
			switch($this->order_type) {
				case LearningTest::ORDER_TYPE_SEQUENCE : {

					// sequential
					$questionsCriteria->addCondition('qr.page = :page_number');
					$questionsCriteria->params[':page_number'] = $pageNumber;
					$questionsCriteria->order = 'qr.sequence ASC';

					return LearningTestquest::model()->findAll($questionsCriteria);
				};break;
				case LearningTest::ORDER_TYPE_RANDOM : {

					// shuffle
					$questions =  isset($order[$pageNumber]) ? $order[$pageNumber] : array();
					$questionsCriteria->addCondition('qr.page = :page_number');
					$questionsCriteria->addCondition('type_quest <> :title_type');
					$questionsCriteria->params[':page_number'] = $pageNumber;
					$questionsCriteria->params[':title_type'] = LearningTestquest::QUESTION_TYPE_TITLE;
					if($questions !== array()){
						$questionsCriteria->order = "FIELD(idQuest," . implode(",", $questions) . ")";
					}

					return LearningTestquest::model()->findAll($questionsCriteria);
				};break;
				case LearningTest::ORDER_TYPE_RANDOM_SUBGROUP : {

					// Random X quest on a total of N quest
					$questionsCriteria->addCondition('type_quest <> :question_type_1');
					$questionsCriteria->addCondition('type_quest <> :question_type_2');
					$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
					$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

					$questionsList = LearningTestquest::model()->findAll($questionsCriteria);
					shuffle($questionsList);
					return array_slice($questionsList, 0, $this->question_random_number);

				};break;
				case LearningTest::ORDER_TYPE_RANDOM_CATEGORY : {

					// Random X quest on a set of selected categories, each of N(idCategory) quests
					$orderInfo = CJSON::decode($this->order_info);

					$questionsCriteria->addCondition('type_quest <> :question_type_1');
					$questionsCriteria->addCondition('type_quest <> :question_type_2');
					$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
					$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

					//first step: extract all questions from the test and shuffle them
					$questionsList = LearningTestquest::model()->findAll($questionsCriteria);
					shuffle($questionsList);

					//second step: count how many question we need for each category and in total
					$counters = array();
					$total = 0;
					foreach ($orderInfo as $info) {
						$counters[(int)$info['id_category']] = (int)$info['selected'];
						$total += (int)$info['selected'];
					}

					//third step: extract questions from shuffled pool
					$output = array();
					foreach ($questionsList as $question) {
						$idCategory = (int)$question->idCategory;
						if (isset($counters[$idCategory]) && $counters[$idCategory] > 0) {
							$output[] = $question;
							$counters[$idCategory]--;
							$total--;
						}
						if ($total <= 0) { break; } //all needed questions have been collected, exit from foreach cycle
					}

					return $output;

				};break;
			}
		} elseif ($displayType == LearningTest::DISPLAY_TYPE_SINGLE) { //note: this if condition may be unnecessary

			// One question per page

			$questionsCriteria->addCondition('type_quest <> :question_type_1');
			$questionsCriteria->addCondition('type_quest <> :question_type_2');
			$questionsCriteria->params[':question_type_1'] = LearningTestquest::QUESTION_TYPE_TITLE;
			$questionsCriteria->params[':question_type_2'] = LearningTestquest::QUESTION_TYPE_BREAK_PAGE;

			switch($this->order_type) {
				case LearningTest::ORDER_TYPE_SEQUENCE : {

					// Sequential
					$questionsCriteria->order = 'qr.sequence ASC';
					$questionsCriteria->offset = $pageNumber -1 . "," . $pageNumber;
					$questionsCriteria->limit = 1;

					return LearningTestquest::model()->findAll($questionsCriteria);

				};break;
				case LearningTest::ORDER_TYPE_RANDOM :
				case LearningTest::ORDER_TYPE_RANDOM_SUBGROUP :
				case LearningTest::ORDER_TYPE_RANDOM_CATEGORY : {

					/*
					 * Since we shuffled the questions earlier
					 * no we should just show the question's page
					 */
					$questionsCriteria->addCondition('idQuest = :quest');
					$questionsCriteria->params[':quest'] = $order[$pageNumber];

					return LearningTestquest::model()->findAll($questionsCriteria);

				};break;
			}
		}
	}

	public function getInitQuestionSequenceNumberForPage($pageNumber) {

		if ($this->display_type == self::DISPLAY_TYPE_GROUPED) {

			$db = Yii::app()->db;
			$result = $db->createCommand()
				->select("COUNT(*) AS num")
				->from(LearningTestquest::model()->tableName().' q')
				->join(LearningTestQuestRel::model()->tableName().' qr', 'qr.id_question = q.idQuest')
				->where("qr.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
				->andWhere("qr.page < :page_number", array(':page_number' => $pageNumber))
				->andWhere($db->quoteColumnName('type_quest')." <> :qtype_1", array(':qtype_1' => LearningTestquest::QUESTION_TYPE_TITLE))
				->andWhere($db->quoteColumnName('type_quest')." <> :qtype_2", array(':qtype_2' => LearningTestquest::QUESTION_TYPE_BREAK_PAGE))
				->queryRow();
			return (int)$result['num'];

		} else {

			return $pageNumber;
		}
	}



	/**
	 * Retrieve max possible score for the test, based on his questions score properties
	 * @return int the max possible score for the test
	 */
	public function getMaxScore($forUserId = false, $returnPoints = false, $returnCache = true) {

		static $cache = array();
		$cacheKey = sha1(serialize(func_get_args()));
		if (isset($cache[$cacheKey]) && $returnCache) {
			return $cache[$cacheKey];
		}

		if ($this->point_type == self::POINT_TYPE_PERCENT && !$returnPoints) { return 100; }

		if((($this->order_type == self::ORDER_TYPE_RANDOM_SUBGROUP && $this->question_random_number > 0) ||
			$this->order_type == self::ORDER_TYPE_RANDOM_CATEGORY
			) && $forUserId){
			// If "Random choice of X questions on Y available" selected in Test Options,
			// only take into account the questions that the user actually saw and attempted
			// (the ones that he got from the random drawing)

			$rs = Yii::app()->db->createCommand()
				->select("SUM(a.score_correct) AS max_score")
				->from(LearningTestquest::model()->tableName()." q")
				->join(LearningTestQuestRel::model()->tableName()." qr", "q.idQuest = qr.id_question")
				->join(LearningTestquestanswer::model()->tableName()." a", "a.idQuest = q.idQuest")
				->join(LearningTesttrack::model()->tableName()." ltt", 'ltt.idTest = qr.id_test AND ltt.idUser=:idUser', array(':idUser'=>$forUserId))
				->join(LearningTesttrackQuest::model()->tableName()." lttq", 'ltt.idTrack=lttq.idTrack AND lttq.idQuest=a.idQuest')
				->where("qr.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
				->queryRow();

			$maxScore = (float)$rs['max_score'];
			$cache[$cacheKey] = $maxScore;
			return $maxScore;
		}

		$rs = Yii::app()->db->createCommand()
			->select("SUM(a.score_correct) AS max_score")
			->from(LearningTestquest::model()->tableName()." q")
			->join(LearningTestQuestRel::model()->tableName()." qr", "q.idQuest = qr.id_question")
			->join(LearningTestquestanswer::model()->tableName()." a", "a.idQuest = q.idQuest")
			->where("qr.id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
			->queryRow();

		if($this->point_assignment == self::POINT_ASSIGNMENT_TEST_PM_EQUAL_TO_ALL_VISIBLE) {
			$countQuestions = $this->getNumberOfQuestionsForStudent();
			$allQuestions = count($this->getQuestions());
			if($allQuestions > 0 && $countQuestions > 0) {
				$pointsPerQuestion = $rs['max_score'] / $allQuestions;
				$rs['max_score'] = Yii::app()->format->formatNumber($pointsPerQuestion*$countQuestions);
			}
		}

		$rs['max_score'] = floatval($rs['max_score']);
		$cache[$cacheKey] = $rs['max_score'];
		return round($rs['max_score'], 2); // DOCEBO-415 -- because if you get maximum score it will not give you max
	}



	/**
	 * Retrieve max possible sequence of test questions
	 * @return int the max sequence of test questions
	 */
	public function getMaxSequence() {
		$rs = Yii::app()->db->createCommand()
			->select("MAX(sequence) AS max_sequence")
			->from(LearningTestQuestRel::model()->tableName())
			->where("id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
			->queryRow();
		return (int)$rs['max_sequence']; //result can be NULL, but it will be cast to int
	}



	/**
	 * Retrieve max possible page number for test questions
	 * @return int the highest page of test questions
	 */
	public function getMaxPage() {
		$rs = Yii::app()->db->createCommand()
			->select("MAX(page) AS max_page")
			->from(LearningTestQuestRel::model()->tableName())
			->where("id_test = :id_test", array(':id_test' => $this->getPrimaryKey()))
			->queryRow();
		return (int)$rs['max_page']; //result can be NULL, but it will be cast to int
	}



	/**
	 * This function creates a copy of the record in the same table
	 *
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {
		$copy = new LearningTest();
		$copy->author = Yii::app()->user->id;
		$copy->title = $this->title;
		$copy->description = $this->description;
		$copy->point_type = $this->point_type;
		$copy->point_assignment = $this->point_assignment;
		$copy->point_required = $this->point_required;
		$copy->display_type = $this->display_type;
		$copy->order_type = $this->order_type;
		$copy->shuffle_answer = $this->shuffle_answer;
		$copy->question_random_number = $this->question_random_number;
		$copy->save_keep = $this->save_keep;
		$copy->mod_doanswer = $this->mod_doanswer;
		$copy->can_travel = $this->can_travel;
		$copy->show_only_status = $this->show_only_status;
		$copy->show_score = $this->show_score;
		$copy->show_score_cat = $this->show_score_cat;
		$copy->show_doanswer = $this->show_doanswer;
		$copy->show_solution = $this->show_solution;
		$copy->time_dependent = $this->time_dependent;
		$copy->time_assigned = $this->time_assigned;
		$copy->penality_test = $this->penality_test;
		$copy->penality_time_test = $this->penality_time_test;
		$copy->penality_quest = $this->penality_quest;
		$copy->penality_time_quest = $this->penality_time_quest;
		$copy->max_attempt = $this->max_attempt;
		$copy->hide_info = $this->hide_info;
		$copy->order_info = $this->order_info;
		$copy->use_suspension = $this->use_suspension;
		$copy->suspension_num_attempts = $this->suspension_num_attempts;
		$copy->suspension_num_hours = $this->suspension_num_hours;
		$copy->suspension_prerequisites = $this->suspension_prerequisites;
		$copy->mandatory_answer = $this->mandatory_answer;
		$copy->score_max = $this->score_max;
		if (!$organization) {
			$copy->disableLearningObjectsEvents(); //this is important, otherwise it will break DB
		}
		if ($copy->save()) {
			//copy poll questions and answers
			$questions = $this->getQuestions(true);
			if (!empty($questions)) {
				$questionIdList = array();
				foreach ($questions as $question) { $questionIdList[] = $question->getPrimaryKey(); }
				$criteria = new CDbCriteria();
				$criteria->addInCondition('idQuest', $questionIdList);
				$answers = LearningTestquestanswer::model()->findAll($criteria);
				$associations = LearningTestquestanswerAssociate::model()->findAll($criteria);
				$questionsMirror = array();
				$questionsQuestionBank = array();
				$association_quest = array();
				foreach ($questions as $question) {
					$copiedQuestion = new LearningTestquest();
					$copiedQuestion->idTest = $copy->getPrimaryKey();
					$copiedQuestion->idCategory = $question->idCategory;
					$copiedQuestion->type_quest = $question->type_quest;
					$copiedQuestion->title_quest = $question->title_quest;
					$copiedQuestion->difficult = $question->difficult;
					$copiedQuestion->time_assigned = $question->time_assigned;
					$copiedQuestion->shuffle = $question->shuffle;
					$copiedQuestion->is_bank = $question->is_bank;
					$copiedQuestion->sequence = $question->sequence;
					$copiedQuestion->page = $question->page;
					if(!$copiedQuestion->is_bank) { //only copy if question does not belong to Question bank
						if (!$copiedQuestion->save()) {
							$errorMessage = 'Error while copying LO: ' . implode("; ", $copiedQuestion->getErrors());
							Yii::log($errorMessage);
							throw new CException($errorMessage);
						}
						$questionsMirror[$question->getPrimaryKey()] = $copiedQuestion->getPrimaryKey();
					} else {
						$questionsQuestionBank[$question->getPrimaryKey()] = $question->getPrimaryKey();
					}
				}


				if(!empty($associations))
				{
					$association_update = array();

					foreach($associations as $association)
					{
						if(!isset($questionsQuestionBank[$association->idQuest]))
						{
							$copiedAssociation = new LearningTestquestanswerAssociate();
							if(isset($questionsMirror[$association->idQuest])) {
								$copiedAssociation->idQuest = $questionsMirror[$association->idQuest];
							}
							$copiedAssociation->answer = $association->answer;
							if(!$copiedAssociation->save())
							{
								$errorMessage = 'Error while copying LO: '.implode("; ", $copiedAssociation->getErrors('answer'));
								Yii::log($errorMessage);
								throw new CException($errorMessage);
							}
							$association_update[$association->idAnswer] = $copiedAssociation->idAnswer;
							$association_quest[$association->idQuest] = $association->idQuest;
						}
					}
				}

				$arUniqueQuestions = array();
				if (!empty($answers)) {
					foreach ($answers as $answer) {
						if(isset($questionsMirror[$answer->idQuest])) {
							$copiedAnswer = new LearningTestquestanswer();
							$copiedAnswer->idQuest = $questionsMirror[$answer->idQuest];
							$copiedAnswer->sequence = $answer->sequence;
							$copiedAnswer->answer = $answer->answer;
							$copiedAnswer->is_correct = (!empty($association) && isset($association_quest[$answer->idQuest]) ? (isset($association_update[$answer->is_correct]) ? $association_update[$answer->is_correct] : 0) : $answer->is_correct);
							$copiedAnswer->comment = $answer->comment;
							$copiedAnswer->score_correct = $answer->score_correct;
							$copiedAnswer->score_incorrect = $answer->score_incorrect;
							if (!$copiedAnswer->save()) {
								$errorMessage = 'Error while copying LO: ' . implode("; ", $copiedAnswer->getErrors());
								Yii::log($errorMessage);
								throw new CException($errorMessage);
							}
						} else {
							$oldQuestion = Yii::app()->db->createCommand()
								->select('sequence, page')
								->from('learning_test_quest_rel')
								->where('id_test = :idTest AND id_question = :idQuestion', array(':idTest' => $this->idTest, ':idQuestion' => $questionsQuestionBank[$answer->idQuest]))
								->queryRow();

							if(!isset($arUniqueQuestions[$this->idTest][$questionsQuestionBank[$answer->idQuest]])){
								$copyQuestionBaseAnswer = new LearningTestQuestRel();
								$copyQuestionBaseAnswer->id_test = $copy->getPrimaryKey();
								$copyQuestionBaseAnswer->id_question = $questionsQuestionBank[$answer->idQuest];
								$copyQuestionBaseAnswer->sequence = $oldQuestion['sequence'];
								$copyQuestionBaseAnswer->page = $oldQuestion['page'];
								if (!$copyQuestionBaseAnswer->save()) {
									$errorMessage = 'Error while copying LO: ' . implode("; ", $copyQuestionBaseAnswer->getErrors());
									Yii::log($errorMessage);
									throw new CException($errorMessage);
								}
								$arUniqueQuestions[$this->idTest][$questionsQuestionBank[$answer->idQuest]] = true;
							}
						}
					}
				}
			}

			//copy feedback if any
			$feedback = new LearningTestFeedback();
			$feedbackResults = $feedback->getFeedbackById($this->idTest);
			if(count($feedbackResults)){
				foreach($feedbackResults as $result){
					$feedbackResult = new LearningTestFeedback();
					$feedbackResult->id_test = $copy->getPrimaryKey();
					$feedbackResult->from_score = $result['from_score'];
					$feedbackResult->to_score = $result['to_score'];
					$feedbackResult->feedback_text = $result['feedback_text'];
					if (!$feedbackResult->save()) {
						$errorMessage = 'Error while copying LO: ' . implode("; ", $feedbackResult->getErrors());
						Yii::log($errorMessage);
						throw new CException($errorMessage);
					}
					/*Yii::app()->db->createCommand()
						->insert('learning_test_feedback', array(
							'id_test' => $copy->getPrimaryKey(),
							'from_score' => $result['from_score'],
							'to_score' => $result['to_score'],
							'feedback_text' => $result['feedback_text']
						))
						->execute();*/
				}
			}

			if (!$organization) { $copy->enableLearningObjectsEvents(); }
			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.implode("; ", $copy->getErrors());
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}

	public function getManualScore() {
		$output = 0;
		$questions = $this->getQuestions();
		if (!empty($questions)) {
			foreach ($questions as $question) {
				$questionManager = CQuestionComponent::getQuestionManager($question->type_quest);
				if ($questionManager && $questionManager->getScoreType() == CQuestionComponent::SCORE_TYPE_MANUAL) {
					$output += $question->getScore();
				}
			}
		}
		return $output;
	}



	/**
	 * Return information if answers should be visible to the given user. It depends on TEST option and User's own TEST progress/status
	 *
	 * @param number $idUser
	 * @return boolean
	 */
	public function answersVisible($idUser) {

		static $cache = array();

		if (isset($cache[$this->idTest][$idUser])) {
			return $cache[$this->idTest][$idUser];
		}

		$testTrackModel = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $this->idTest));
		if (!$testTrackModel) {
			$cache[$this->idTest][$idUser] = false;
			return false;
		}

		$status = $testTrackModel->score_status;
		$result = false;

		if ($this->show_solution_date != null) {
			$date = new DateTime($this->show_solution_date);
			$now = new DateTime();
			$dateReached = ($now >= $date) ? true : false;
		}
		else
			$dateReached = false;

		if (
			($this->show_doanswer == self::SHOW_ANSWERS_YES) ||
			($this->show_solution == self::SHOW_SOLUTION_YES) ||
			($this->show_doanswer == self::SHOW_ANSWERS_YES_IF_PASSED && $status == LearningTesttrack::STATUS_PASSED) ||
			($this->show_solution == self::SHOW_SOLUTION_YES_IF_PASSED && $status == LearningTesttrack::STATUS_PASSED) ||
			($this->show_solution == self::SHOW_SOLUTION_YES_FROM_DATE && $dateReached)

		) {
			$result = true;
		}

		$cache[$this->idTest][$idUser] = $result;

		return $cache[$this->idTest][$idUser];

	}




	/**
	 * Return information if CORRECT answers should be visible to the given user. It depends on TEST options and User's own TEST progress/status
	 *
	 * @param number $idUser
	 * @return boolean
	 */
	public function correctAnswersVisible($idUser) {

		static $cache = array();

		if (isset($cache[$this->idTest][$idUser])) {
			return $cache[$this->idTest][$idUser];
		}

		if (!$this->answersVisible($idUser)) {
			$cache[$this->idTest][$idUser] = false;
			return false;
		}

		$testTrackModel = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $this->idTest));
		if (!$testTrackModel) {
			$cache[$this->idTest][$idUser] = false;
			return false;
		}

		$status = $testTrackModel->score_status;
		$result = false;

		if (
				($this->show_solution == self::SHOW_SOLUTION_YES) ||
				(($this->show_solution == self::SHOW_SOLUTION_YES_IF_PASSED) && ($status == LearningTesttrack::STATUS_PASSED))
		) {
			$result = true;
		}

		$cache[$this->idTest][$idUser] = $result;

		return $cache[$this->idTest][$idUser];

	}

	public function answersVisibleOnReview($idUser)
	{
		static $cache = array();

		if (isset($cache[$this->idTest][$idUser]))
			return $cache[$this->idTest][$idUser];

		$testTrackModel = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $this->idTest));

		$status = $testTrackModel->score_status;
		$result = false;

		if($this->show_doanswer == self::SHOW_ANSWERS_YES || ($this->show_doanswer == self::SHOW_ANSWERS_YES_IF_PASSED && $status == LearningTesttrack::STATUS_PASSED))
			$result = true;

		$cache[$this->idTest][$idUser] = $result;

		return $cache[$this->idTest][$idUser];
	}

	public function correctAnswersVisibleOnReview($idUser)
	{
		static $cache = array();

		if (isset($cache[$this->idTest][$idUser]))
			return $cache[$this->idTest][$idUser];

		$testTrackModel = LearningTesttrack::model()->findByAttributes(array('idUser' => $idUser, 'idTest' => $this->idTest));

		$status = $testTrackModel->score_status;
		$result = false;

		if ($this->show_solution_date != null) {
			$date = new DateTime($this->show_solution_date);
			$now = new DateTime();
			$dateReached = ($now >= $date) ? true : false;
		}
		else
			$dateReached = false;

		if(
			$this->show_solution == self::SHOW_SOLUTION_YES ||
			($this->show_solution == self::SHOW_SOLUTION_YES_IF_PASSED && $status == LearningTesttrack::STATUS_PASSED) ||
			($this->show_solution == self::SHOW_SOLUTION_YES_FROM_DATE && $dateReached)
		)
			$result = true;

		$cache[$this->idTest][$idUser] = $result;

		return $cache[$this->idTest][$idUser];
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {

		// CLEAN UP: delete files uploaded as an answer to "file upload" questions of THIS test, if any
		// Get list of "upload" questions, if any
		$ulQuestions = $this->questions(array('condition' => "type_quest='" . LearningTestquest::QUESTION_TYPE_UPLOAD . "'"));

		// Enumerate them and look into tracked answers
		foreach ($ulQuestions as $question) {
			$trackAnswers = $question->trackAnswers;
			foreach ($trackAnswers as $answer) {
				// If there is some text in "more_info" field, that must be a file; delete it
				if ($answer->more_info) {
					$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TEST);
					$storage->remove($answer->more_info);
				}
			}
		}

		$testQuestRel = LearningTestQuestRel::model()->findAllByAttributes(array('id_test' => $this->idTest));
		foreach($testQuestRel as $rel)
			$rel->delete();

		return parent::beforeDelete();

	}


	/**
	 * Collect various TEST LO information (regarding specific USER), used by the TEST "launch" UI
	 * NOTE: Do not use it in loops!!!!
	 *
	 * @param number $idOrg
	 * @param number $idUser
	 * @throws CException
	 * @return array
	 */
	public static function collectTestInfoByLoAndUser($idOrg, $idUser) {

		$result = array(
				'loModel' 				=> null,
				'testModel' 			=> null,
				'trackModel' 			=> null,
				'commonTrackModel'		=> null,
				'testScores' 			=> null,
				'courseUserModel' 		=> null,
				'max_attemp_reached' 	=> false,
				'suspended' 			=> false,
				'suspend_time_left' 	=> 0,
				'need_prerequisites' 	=> false,
				'courseModel'			=> null,
		);


		$loModel 	= LearningOrganization::model()->findByPk($idOrg);
		$idTest 	= $loModel->idResource;
		$testModel 	= LearningTest::model()->findByPk($idTest);
		if (!$testModel) {
			return $result;
		}

		$trackModel = LearningTesttrack::model()->findByAttributes(array(
				'idUser' => $idUser,
				'idTest' => $idTest
		));

		$commonTrackModel = null;
		if($trackModel) {
			$commonTrackModel = LearningCommontrack::model()->findByPk(array(
				'idTrack' => $trackModel->idTrack,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'idReference' => $loModel->idOrg
			));
		}

		if ($testModel->time_dependent && $testModel->time_assigned) {
			$minutesAssigned = (int)($testModel->time_assigned / 60);
			$secondsAssigned = (int)($testModel->time_assigned % 60);
			$secondsAssigned = str_pad(''.$secondsAssigned, 2, '0', STR_PAD_LEFT);
			$timeReadable = Yii::t('test', '_TEST_TIME_ASSIGNED', array(
					'[time_assigned]' => $minutesAssigned.':'.$secondsAssigned,
					'[second_assigned]' => $secondsAssigned,
					'[minute_assigned]' => $minutesAssigned
			));
		}


		$idCourse = $loModel->idCourse;
		$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idCourse' => $idCourse, 'idUser' => $idUser));

		$testScores = TestUtils::getTestsScores($idTest, $idUser);

		$maxAttempt = $testModel->max_attempt;

		$maxAttemptsReached = false;

		if ($maxAttempt > 0 && $trackModel) {
			if ($maxAttempt - $trackModel->number_of_attempt <= 0) {
				$maxAttemptsReached = true;
			}
		}
		$suspended = false;
		$suspend_time_left = '';
		$need_prerequisites = false;
		$prerequisites_obj = array();
		$courseModel = LearningCourse::model()->findByPk($idCourse);

		//check if the test is using suspension and if it is effectively suspended at the current time
		if ($testModel->use_suspension && $testModel->max_attempt > 0 && $testModel->suspension_num_hours >= 0 && !is_null($trackModel) && $trackModel->number_of_attempt >= $testModel->max_attempt && !is_null($trackModel->suspended_until)) {
			$time_left = 0;

			$current_time 		= Yii::app()->localtime->getUTCNow();
			$compilation_time 	= Yii::app()->localtime->fromLocalDateTime($trackModel->date_end_attempt);
			/*
			if ($testModel->suspension_num_hours > 0) {
				$time_left = (strtotime($compilation_time) + $testModel->suspension_num_hours*3600) - strtotime($current_time);
			}
			*/
			if($trackModel->score_status == LearningTesttrack::STATUS_NOT_PASSED && !$trackModel->suspended_until) {
				$suspendDateUTC = date('Y-m-d H:i:s', strtotime($compilation_time) + $testModel->suspension_num_hours*3600);
				$trackModel->suspended_until = Yii::app()->localtime->toLocalDateTime($suspendDateUTC);
			}
			//NOTE: suspended_until date is assumed to be already calculated. Dates from previous suspensions are simply ignored because older than current time
			$suspendedUntil = Yii::app()->localtime->fromLocalDateTime($trackModel->suspended_until);

			if ($suspendedUntil) {
				$dt1 = new DateTime($current_time);
				$dt2 = new DateTime($suspendedUntil);
				$sec1 = $dt1->getTimestamp();
				$sec2 = $dt2->getTimestamp();
				$time_left = ($sec2 - $sec1);
			} else {
				$time_left = 0;
			}

			if($time_left > 0) {
				$suspended = true;
				$seconds_left 	= $time_left % 60;
				$time_left 		-= $seconds_left;
				$hours_left 	= (int)($time_left / 3600);
				$time_left 		-= $hours_left * 3600;
				$minutes_left 	= $time_left / 60;
				$suspend_time_left = ($hours_left > 0 ? $hours_left.' '.Yii::t('standard', '_HOURS').' ' : '').$minutes_left.' '.Yii::t('standard', '_MINUTES');
			}

			switch ($trackModel->score_status) {
				case LearningTesttrack::STATUS_FAILED:
				case LearningTesttrack::STATUS_NOT_PASSED: { $isTestFailed = true; } break;
				default: { $isTestFailed = false; } break;
			}

			if ($isTestFailed && $testModel->suspension_prerequisites == 1 && !empty($trackModel->suspended_until)) {

				switch ($courseModel->prerequisites_policy) {

					case LearningCourse::NAVRULE_PRETEST:
						//Nothing to do in that case but we can put an easteregg here :P
						break;

					case LearningCourse::NAVRULE_FINALTEST:
						//Check just if the object is the final one of the course
						$criteria = new CDbCriteria;
						$criteria->addCondition("objectType <> ''");
						$criteria->addCondition('idCourse = :id_course');
						$criteria->params[':id_course'] = $idCourse;
						$criteria->order = 'iLeft DESC';

						$last_object = LearningOrganization::model()->find($criteria);

						if ($last_object->idOrg == $idOrg && $last_object->idResource == $idTest) {
							//Check that all the previous object was completed after the last test completition date. We must ignore Scorm and TinCan object
							$criteria = new CDbCriteria;
							$criteria->addCondition("objectType <> ''");
							$criteria->addCondition("objectType <> 'tincan'");
							// $criteria->addCondition("objectType <> 'scormorg'");
							$criteria->addCondition("idOrg <> :id_org");
							$criteria->params[':id_org'] = $last_object->idOrg;
							$criteria->addCondition('idCourse = :id_course');
							$criteria->params[':id_course'] = $idCourse;

							$previous_objects = LearningOrganization::model()->findAll($criteria);

							foreach($previous_objects as $object) {
								$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $object->idOrg, 'idUser' => $idUser));
								if(is_null($track)) {
									$suspended = true;
									$need_prerequisites = true;
									break;
								}

								if($track->status != 'completed') {
									$suspended = true;
									$need_prerequisites = true;
									break;
								}

								$last_complete_time = Yii::app()->localtime->fromLocalDateTime($track->last_complete);
								$interval = strtotime($compilation_time) -  strtotime($last_complete_time);

								if($interval >= 0) {
									$suspended = true;
									$need_prerequisites = true;
									break;
								}
							}
						}
						break;

					case LearningCourse::NAVRULE_SEQUENTAL:

						$object = LearningOrganization::model()->findByPk($idOrg);

						$criteria = new CDbCriteria;
						$criteria->addCondition("objectType <> ''");
						$criteria->addCondition("objectType <> 'tincan'");
						// $criteria->addCondition("objectType <> 'scormorg'");
						$criteria->addCondition("idOrg <> :id_org");
						$criteria->params[':id_org'] = $object->idOrg;
						$criteria->addCondition('idCourse = :id_course');
						$criteria->params[':id_course'] = $idCourse;
						$criteria->addCondition('iLeft < :i_left');
						$criteria->params[':i_left'] = $object->iLeft;
						$criteria->order = 'iLeft ASC';

						$previous_object = LearningOrganization::model()->find($criteria);

						if (!is_null($previous_object)) {

							$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $previous_object->idOrg, 'idUser' => Yii::app()->user->id));

							if(is_null($track)) {
								$suspended = true;
								$need_prerequisites = true;
								break;
							}

							if($track->status != 'completed') {
								$suspended = true;
								$need_prerequisites = true;
								break;
							}

							$last_complete_time = Yii::app()->localtime->fromLocalDateTime($track->last_complete);
							$interval = strtotime($compilation_time) - strtotime($last_complete_time);

							if($interval >= 0) {
								$suspended = true;
								$need_prerequisites = true;
								break;
							}
						}
						break;

					case LearningCourse::NAVRULE_FREE:
					default:
						$object = LearningOrganization::model()->findByPk($idOrg);

						$prerequisites = explode(',', $object->prerequisites);

						$criteria = new CDbCriteria;
						$criteria->addCondition("objectType <> ''");
						$criteria->addCondition("objectType <> 'tincan'");
						// $criteria->addCondition("objectType <> 'scormorg'");
						$criteria->addCondition("idOrg <> :id_org");
						$criteria->params[':id_org'] = $object->idOrg;
						$criteria->addCondition('idCourse = :id_course');
						$criteria->params[':id_course'] = $idCourse;
						$criteria->addInCondition('idOrg', $prerequisites);
						$criteria->order = 'iLeft ASC';

						$previous_objects = LearningOrganization::model()->findAll($criteria);

						foreach ($previous_objects as $object) {

							$track = LearningCommontrack::model()->findByAttributes(array('idReference' => $object->idOrg, 'idUser' => $idUser));
							if (is_null($track)) {
								$suspended = true;
								$need_prerequisites = true;
								$prerequisites_obj[$object->idOrg] = $object->title;
							}

							if ($track->status != 'completed') {
								$suspended = true;
								$need_prerequisites = true;
								$prerequisites_obj[$object->idOrg] = $object->title;
							}

							$last_complete_time = Yii::app()->localtime->fromLocalDateTime($track->last_complete);
							$interval = strtotime($compilation_time) -  strtotime($last_complete_time);

							if ($interval >= 0) {
								$suspended = true;
								$need_prerequisites = true;
								$prerequisites_obj[$object->idOrg] = $object->title;
							}
						}
						break;
				}
			}

			if($suspended == false)
			{
				$trackModel->suspended_until = NULL;
				$trackModel->number_of_attempt = 0;
				$maxAttemptsReached = false;
				$trackModel->update();
			}
		}


		$result = array(
			'loModel' 				=> $loModel,
			'testModel' 			=> $testModel,
			'trackModel' 			=> $trackModel,
			'commonTrackModel' 		=> $commonTrackModel,
			'testScores' 			=> $testScores,
			'courseUserModel' 		=> $courseUserModel,
			'max_attemp_reached' 	=> $maxAttemptsReached,
			'suspended' 			=> $suspended,
			'suspend_time_left' 	=> $suspend_time_left,
			'need_prerequisites' 	=> $need_prerequisites,
			'courseModel'			=> $courseModel,
			'timeReadable'			=> $timeReadable,
			'prerequisites_obj'     => $prerequisites_obj,
		);

		return $result;


	}

	/**
	 * Return LearningOrganization model and load it if not available
	 * @return mixed|static
	 */
	public function loadOrgModel()
	{
		if (!$this->orgModel) {
			$this->orgModel = LearningOrganization::model()->findByAttributes(array(
				'idResource' => $this->idTest,
				'objectType' => LearningOrganization::OBJECT_TYPE_TEST,
				'id_object' => null
			));
		}
		return $this->orgModel;
	}

	public function getCourseName()
	{
		$org = $this->loadOrgModel();
		if ($org && $org->course)
			return $org->course->name;
		else
			return '';
	}

	public function getTitleWithEditLink()
	{
		$org = $this->loadOrgModel();
		if ($org && $org->course)
			return CHtml::link($this->title, Docebo::createLmsUrl('test/default/edit', array('id_object' => $org->idOrg, 'course_id' => $org->course->idCourse)), array('style' => 'color: #0a63a7; text-decoration: underline;'));
		else
			return '';
	}

	public function getUsersAverage()
	{
		if ($this->show_users_avg)
		{

		}
		else
			return '';
	}

	/**
	 * Update all tests score_max related to the given question
	 * @param $idQuest
	 */
	public static function updateScoreMaxByQuestion($idQuest)
	{
		//get all tests related to that question
		$tests = Yii::app()->getDb()->createCommand()
			->select('t.idTest')
			->from(LearningTest::model()->tableName().' t')
			->join(LearningTestQuestRel::model()->tableName().' qr', 't.idTest = qr.id_test')
			->where('qr.id_question = :idQuest', array(':idQuest' => $idQuest))
			->queryAll();

		//get all questions total possible score and save it to each test
		foreach($tests as $test)
		{
			$model = LearningTest::model()->findByPk($test['idTest']);
			$scoreMax = round($model->getMaxScore(false, false, false));

			//update test score_max
			LearningTest::model()->updateByPk($test['idTest'], array('score_max' => $scoreMax));
		}
	}

}