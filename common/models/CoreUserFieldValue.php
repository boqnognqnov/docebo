<?php



/**
 * This is the model class for table "core_user_field_value".
 *
 * @property integer $id_user
 *
 */
class CoreUserFieldValue extends \CActiveRecord{

    /**
     * Just set this array to LOCAL files and it will trigger saving them to Core Field Collection storage later in beforeSave()
     * 
     * array(
     *      "field_300" => "/local/path/to/file1.txt"
     *      "field_400" => "/local/path/to/file2.txt"
     * )
     *
     * @var string
     */
    public $sourceFiles = array();
    

	/**
	 * @inheritdoc
	 */
	public  function tableName() {
		return 'core_user_field_value';
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['id_user', 'required'],
            ['id_user', 'numerical', 'integerOnly' => true],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [];
	}

    /**
     * @see CActiveRecord::afterDelete()
     */
    public function afterDelete ()
    {
        $this->deleteFiles();
    }

    /**
     * Deletes the files related to any additional fields of type upload
     *
     * @return void
     */
    private function deleteFiles()
    {
        $userValues = $this->getAttributes();

        unset($userValues['id_user']);
        $values = array_values($userValues);
        $fieldIds = array_keys($userValues);
        $fieldIds = array_map(function($key) {
            // $key = str_replace('field_', '', $key);

            // Alternative to string replace for performance enhancement, such as
            // we know the length and the string itself that needs to be removed
            $key = substr($key, 6);
            return intval($key);
        }, $fieldIds);

        $userValues = array_combine($fieldIds, $values);

        $fields = CoreUserField::model()->findAllByAttributes(array(
            'id_field' => $fieldIds,
        ));

        foreach ($fields as $field) {
            if ($field->getFieldType() !== CoreUserField::TYPE_UPLOAD) {
                continue;
            }

            $filename = $userValues[$field->getFieldId()];

            if(empty($filename) === false) {
                $storage = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_CORE_FIELD );
                $storage->remove($filename);
            }
        }
    }
    

    /**
     * 
     * {@inheritDoc}
     * @see CActiveRecord::beforeSave()
     */
    public function beforeSave () {
        
        if (!parent::beforeSave()) {
            return false;
        }
        
        // If someone have setted sourceFiles array, upload them and update model's field(s)
        if( isset( $this->sourceFiles ) && is_array($this->sourceFiles) ) {
            foreach ($this->sourceFiles as $column => $filePath) {
                $this->saveFileUserEntry($column, $filePath);
            }
        }
        
        // Always
        return true;
    
    }
    

    /**
     * Upload a file and update current model field $column
     * 
     * @param string $column Table column name, e.g. 'field_1'
     * @param string $filePath Absolute path to local file to be stored in File Storage (e.g. Amazon S3)
     */
    protected function saveFileUserEntry ($column, $filePath) {
        if( !is_file( $filePath ) ) {
            return;
        }
        
        $pathInfo     = pathinfo( $filePath );
        $slugFileName = preg_replace('/[\s-]+/', "_", $pathInfo["filename"]);
        $newFileName  = $slugFileName . "_" . Docebo::randomHash(true) . '.' . $pathInfo['extension'];
        $storage      = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_CORE_FIELD );
        if( $storage->storeAs( $filePath, $newFileName ) ) {
            $this->{$column} = $newFileName;
        } else {
            Yii::log( 'Unable to save local file: ' . $filePath . ' to storage: ' . get_class( $storage ) . '. Remote file name to save: ' . $newFileName, CLogger::LEVEL_ERROR );
        }
    }
    
    

}
