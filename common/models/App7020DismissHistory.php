<?php

class App7020DismissHistory extends CActiveRecord {
	public function tableName() {
		return 'app7020_asset_dismiss_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('idContent, idUser,contentType', 'required'),
			array('idContent, idUser,contentType', 'numerical', 'integerOnly' => true)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id Content',
			'contentType' => 'Content Type',
			'idUser' => 'Id User',
			'created' => 'Created'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idUser', $this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020TopicContent the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
}