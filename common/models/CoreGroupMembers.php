<?php

/**
 * This is the model class for table "core_group_members".
 * The followings are the available columns in table 'core_group_members':
 * @property integer $idst
 * @property integer $idstMember
 * @property string $filter
 * The followings are the available model relations:
 * @property CoreGroup $group
 */
class CoreGroupMembers extends CActiveRecord {

	const GROUP_TYPE = 1;
	const ORGCHART_TYPE = 2;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreGroupMembers the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_group_members';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst, idstMember', 'numerical', 'integerOnly' => true),
			array('filter', 'length', 'max' => 50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, idstMember, filter', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'group' => array(self::BELONGS_TO, 'CoreGroup', 'idst'),
			'orgchart' => array(self::HAS_ONE, 'CoreOrgChartTree', 'idst_ocd'),
			'powerUser' => array(self::BELONGS_TO, 'CoreUser', 'idstMember'),
			'users' => array(self::BELONGS_TO, 'CoreUser', 'idstMember'),
		);
	}

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idst' => 'Idst',
			'idstMember' => 'Idst Member',
			'filter' => 'Filter',
		);
	}

	public function getIsGroup() {
		return is_object($this->group);
	}

	public function getIsOrgchart() {
		return is_object($this->orgchart);
	}

	public function getType() {
		if ($this->getIsGroup()) {
			return self::GROUP_TYPE;
		} elseif ($this->getIsOrgchart()) {
			return self::ORGCHART_TYPE;
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idst', $this->idst);
		$criteria->compare('idstMember', $this->idstMember);
		$criteria->compare('filter', $this->filter, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
        ));
	}
	
	public function beforeSave(){
		if($this->idst && $this->idstMember){
			
			// Raise event (see CoreNotification/TransportAgent.php for all events)
			if ($this->group->hidden != 'true') {
				Yii::app()->event->raise(EventManager::EVENT_USER_SUBSCRIBED_GROUP, new DEvent($this, array(
					'user' => $this->users, // AR relation
					'group' => $this->group, // AR relations
				)));
			}
		}
		
		return parent::beforeSave();
	}
	
	public function beforeDelete(){
		if($this->idst && $this->idstMember){
			// Raise event (see CoreNotification/TransportAgent.php for all events)
			if ($this->group->hidden != 'true') {
				Yii::app()->event->raise(EventManager::EVENT_USER_REMOVED_FROM_GROUP, new DEvent($this, array(
					'user' => $this->users, // AR relations 
					'group' => $this->group, // AR relations
				)));
				
			}
		}
		
		return parent::beforeDelete();
	}

	/**PHPDoc
	 * @return CActiveDataProvider
	 * @deprecated
	 */
	public function dataProvider() {
		$criteria = new CDbCriteria;

		$criteria->compare('t.idst', $this->idst);
//		$criteria->compare('t.idstMember', $this->idstMember);
//		$criteria->compare('t.filter', $this->filter, true);

		if (!empty($this->users->userid)) {
			$criteria->with = array('users' => array(
				'condition' => 'users.userid LIKE :userid',
				'params' => array(':userid' => '%'.$this->users->userid.'%'),
			));
		} else {
			$criteria->with = array('users' => array(
				'condition' => 'users.userid IS NOT NULL' //this will avoid the visualization of invalid user references, since we are dealing with a left join
			));
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.idstMember desc';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	// new optimized data provider
	public function sqlDataProvider() {
		$command = Yii::app()->db->createCommand()
				->select()
				->from(CoreGroupMembers::model()->tableName(). ' cgm')
				->where('cgm.idst = :id', array(':id' => $this->idst));
		if (!empty($this->users->userid)) {
			$command->join(CoreUser::model()->tableName() . ' cu',
					'cgm.idstMember=cu.idst AND cu.userid LIKE :userid', array(':userid' => '%' . $this->users->userid . '%'));
		} else {
			$command->join(CoreUser::model()->tableName() . ' cu', 'cgm.idstMember=cu.idst');
		}
		$cloneCommand = clone($command);
		$cloneCommand->select('COUNT(cgm.idstMember)');
		$numRecords = $cloneCommand->queryScalar();
		$config = array(
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
			'totalItemCount' => $numRecords,
		);

		$dataProvider = new CSqlDataProvider($command, $config);

		return $dataProvider;
	}

	/**
	 * search groups containing a security token
	 * @param int $idstMember the security token of the searched member
	 * @param string $filter (Optional). Filter to applay.
	 * @return array array of security token of groups that contains $idstMember
	 */
	public static function getGroupsAllContainer($arrMember, $filter = '') {

		$criteria = new CDbCriteria();
		$criteria->condition = 'filter = :filter';
		$criteria->addInCondition('idstMember', $arrMember);
		$criteria->params = array(
			':filter' => $filter
		);
		$rows = CoreGroupMembers::model()->findAll($criteria);

		$arrGroups = array();
		foreach ($rows as $row) {
			$arrGroups = $row->idst;
		}

		return $arrGroups;
	}

	public function findAdminLevel($idst) {
		$criteria = new CDbCriteria();
		$criteria->with = array('group');
		$criteria->compare('group.groupid', '/framework/level', true);
		$criteria->compare('t.idstMember', $idst);

		$model = self::model()->find($criteria);
		return $model;
	}

	public function processImportCSVUpload($groupId, $users) {
		$result    = array(
			'imported' => 0,
			'skipped' => 0,
			'notFound' => 0,
		);
		$usersList = CHtml::listData($users, 'username', function ($user) {
			return Yii::app()->user->getAbsoluteUsername($user['username']);
		});

		$existUsers     = CoreUser::model()->findAllByAttributes(array('userid' => $usersList));
		$existUsersList = CHtml::listData($existUsers, 'idst', 'userid');

		$notFound           = array_diff($usersList, $existUsersList);
		$result['notFound'] = count($notFound);

		$existGroupMembers     = CoreGroupMembers::model()->findAllByAttributes(array(
			'idst' => $groupId,
			'idstMember' => array_keys($existUsersList)
		));
		$existGroupMembersList = CHtml::listData($existGroupMembers, 'idstMember', 'idstMember');
		$result['skipped']     = count($existGroupMembersList);

		$newGroupMembers = array_diff(array_keys($existUsersList), $existGroupMembersList);
		foreach ($newGroupMembers as $newGroupMemberId) {
			$newGroupMember             = new CoreGroupMembers();
			$newGroupMember->idst       = $groupId;
			$newGroupMember->idstMember = $newGroupMemberId;
			if ($newGroupMember->save()) {
				$result['imported']++;
			} else {
				$result['skipped']++;
			}
		}
		return $result;
	}

	public function getPowerUserProfile($puserId) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.idstMember = :idst';
		$criteria->params = array(
			':idst' => $puserId,
		);
		$criteria->with = array(
			'powerUser.adminProfile' => array(
				'condition' => 'adminProfile.idst = t.idst'
			),
		);
		$profileMemberModel = CoreGroupMembers::model()->find($criteria);
		if (empty($profileMemberModel)) {
			$profileMemberModel = new CoreGroupMembers();
			$profileMemberModel->idstMember = $puserId;
		}
		return $profileMemberModel;
	}

	public function afterSave()
	{
		// Self-registration should not trigger Enrollment rules,
		// because some Enrollment rules try to subscribe the user
		// to courses, but that user immediately after self-registration
		// is not a real user yet. He is in core_user_temp, not in core_user.
		//
		// When the temp user becomes a real user (see CoreUserTemp->confirm()),
		// these subscriptions will be preserved and the enrollment rules will be
		// triggered.
		if ($this->isNewRecord && $this->scenario != 'self-register')
		{
			$pk = (is_array($this->primaryKey))? implode('-',$this->primaryKey) : $this->primaryKey;
			if ($this->group->isUserGroup)
			{
				Yii::app()->event->raise(EventManager::EVENT_NEW_GROUP_USER, new DEvent($this, array()));
			}
			else if ($this->group->isOrgChart)
			{
				Yii::app()->event->raise(EventManager::EVENT_NEW_BRANCH_USER, new DEvent($this, array()));
			}
		}

		//different event used mainly from customer plugins, so raised always
		if ($this->idst)
		{
			Yii::app()->event->raise('UserAssignedToGroup', new DEvent($this, array(
				'profileId' => $this->idst,
				'user' => $this->users,
			)));
		}
		parent::afterSave();
	}

    /**
     * Retrieves the assigned to branch users ids
     * @return array of users ids
     */
    public function getBranchUserIds($orgChartId, $includeDescendants = true) {
        // array for all branches ids
        $allOrgIds = array();

        // array for all member ids in branch(-es)
        $allOrgMembersIds = array();

        // do we need to get the descendants too
        if ($includeDescendants) {
            // check is it passed $orgChartId
            if (!empty($orgChartId)) {
                // get org chart tree left
                $iLeft = Yii::app()->db->createCommand('SELECT iLeft FROM core_org_chart_tree WHERE idOrg = ' . $orgChartId)->queryRow();
                $iLeft = !empty($iLeft['iLeft']) ? $iLeft['iLeft'] : -1;

                // get org chart tree right
                $iRight = Yii::app()->db->createCommand('SELECT iRight FROM core_org_chart_tree WHERE idOrg = ' . $orgChartId)->queryRow();
                $iRight = !empty($iRight['iRight']) ? $iRight['iRight'] : -1;

                // get org chart and its descendant `idst_oc`s
                if (($iLeft != -1) && ($iRight != -1)) {
                    // we can do the query if we have
                    $allOrgIds = Yii::app()->db->createCommand('SELECT idst_oc FROM core_org_chart_tree WHERE iLeft >= ' . $iLeft . ' AND iRight <= ' . $iRight)->queryAll();
                }
            }

            // array to store temporary org chart ids
            $temptAllOrgIds = array();
            if (!empty($allOrgIds) && (count($allOrgIds) > 0)) {
                foreach ($allOrgIds as $tempOrgId) {
                    if (!empty($tempOrgId['idst_oc']))
                        $temptAllOrgIds[] = $tempOrgId['idst_oc'];
                }

                // prepare the org chart ids list for the query
                $allOrgIds = implode(', ', $temptAllOrgIds);
            } else {
                // we don't have any org chart ids for some reason
                $allOrgIds = '';
            }

            // execute query if there are any org chart ids only
            if ($allOrgIds <> '') {
                $allOrgMembersIds = Yii::app()->db->createCommand()
                    ->select('u.idst')
                    ->from('core_user u')
                    ->join('core_group_members gm', 'gm.idstMember = u.idst ')
                    ->where('gm.idst IN (' . $allOrgIds . ')')
                    ->group('u.idst')
                    ->queryAll();

            } else {
                // there are no org charts selected and return empty array
                return $allOrgMembersIds;
            }
        } else {
            // we need to select only the users in the branch, that is set and ignore its descendants

            // 1st we nee to get the correct org chart id of the core_group db table
            $coreGroupId = Yii::app()->db->createCommand('SELECT idst_oc FROM core_org_chart_tree WHERE idOrg = ' . $orgChartId)->queryRow();
            $coreGroupId = !empty($coreGroupId['idst_oc']) ? $coreGroupId['idst_oc'] : -1;

            // if there is a group id
            if ($coreGroupId != -1) {
                $allOrgMembersIds = Yii::app()->db->createCommand()
                    ->select('u.idst')
                    ->from('core_user u')
                    ->join('core_group_members gm', 'gm.idstMember = u.idst ')
                    ->where('gm.idst = ' . $coreGroupId )
                    ->group('u.idst')
                    ->queryAll();

            } else {
                // no branch found and we cannot get its members
            }
        }

        return $allOrgMembersIds;
    }

	public static function inSelectList($list, $searchedId){
		if (isset(Yii::app()->session[$list]) && is_array(Yii::app()->session[$list])) {
			return in_array($searchedId, Yii::app()->session[$list]);
		}
		return false;
	}


	public static function getOcReportGroupsPaths($list, $idUser = false){
		$result = CoreOrgChartTree::getOcGroupsPaths($list, " > ", true, ",\n");

		if(!$result && $idUser){
			$id_ocs = Yii::app()->db->createCommand()
				->select('idst')
				->from(CoreGroupMembers::model()->tableName().' gm')
				->where('gm.idstMember = :idstMember', array(':idstMember' => $idUser))
				->queryColumn();
			if($id_ocs && is_array($id_ocs)){
				$result = CoreOrgChartTree::getOcGroupsPaths(implode($id_ocs, ','), " > ", true, ",\n");
			}
		}

		return $result;
	}

}