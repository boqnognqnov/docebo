<?php

/**
 * This is the model class for table "subscriptioncodes_set".
 *
 * The followings are the available columns in table 'subscriptioncodes_set':
 * @property integer $id_set
 * @property string $name
 * @property string $valid_from
 * @property string $valid_to
 * @property integer $is_active
 *
 * The followings are the available model relations:
 * @property SubscriptioncodesCode[] $subscriptioncodesCodes
 * @property LearningCourse[] $learningCourses
 */
class SubscriptioncodesSet extends CActiveRecord
{

	// Date/Time attributes for Localtime behavior
	public $timestampAttributes = array();
	public $dateAttributes = array('valid_from', 'valid_to');
	
	// Various additional attributes
	public $searchText = false;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subscriptioncodes_set';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('valid_from, valid_to', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_set, name, valid_from, valid_to, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'codes' 		=> array(self::HAS_MANY, 'SubscriptioncodesCode', 'id_set'),
			'countCodes'	=> array(self::STAT,  'SubscriptioncodesCode', 'id_set', 'select' => 'count(id_set)'),
			'countCourses'	=> array(self::STAT,  'SubscriptioncodesCourse', 'id_set', 'select' => 'count(id_set)'),
			'courses' 		=> array(self::MANY_MANY, 'LearningCourse', 'subscriptioncodes_course(id_set, course_id)'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_set' => 'Id Set',
			'name' =>  Yii::t('standard', '_NAME'),
			'valid_from' =>  Yii::t('subscribe', '_DATE_BEGIN_VALIDITY'),
			'valid_to' =>  Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY'),
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_set',$this->id_set);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('valid_from',$this->valid_from,true);
		$criteria->compare('valid_to',$this->valid_to,true);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubscriptioncodesSet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => $this->timestampAttributes,
				'dateAttributes' => $this->dateAttributes
			)
		);
	}
	
	
	/**
	 * Data provider used in SubsCodes SET management UI
	 * @return CActiveDataProvider
	 */
	public function dataProviderManagementGrid() {
		
		$criteria = new CDbCriteria;
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => array(
					'pageSize' => Settings::get('elements_per_page', 10)
				)
		));
		
	}
	

	/**
	 * Return array of IDs of assigned courses 
	 */
	public function assignedCoursesList() {
		$list = array();
		
		$command = Yii::app()->db->createCommand();
		$command
			->select('course_id')
			->from('subscriptioncodes_course')
			->where('id_set=' . (int) $this->id_set);
		
		$rows = $command->queryAll();
		foreach ($rows as $row) {
			$list[] = $row['course_id'];
		}
		
		return $list;
	}

	
	/**
	 * Check if passed $date is "valid data", i.e. how $date relates to Valid FROM/TO
	 * 
	 * @param string $date
	 * @return boolean
	 */
	public function isValidTime($date) {

		// Consider 0000-00-00 as NULL
		if ($this->valid_from == '0000-00-00')
			$this->valid_from = null;
		
		if ($this->valid_to == '0000-00-00')
			$this->valid_to = null;
		
		
		// BOTH dates NULL: TRUE
		if ( is_null($this->valid_from) && is_null($this->valid_to) ) {
			return true;
		}
		
		// Open from LEFT
		if (is_null($this->valid_from) && Yii::app()->localtime->isLte($date, $this->valid_to)) {
			return true;
		} 
		
		// Open from RIGHT
		if (is_null($this->valid_to) && Yii::app()->localtime->isGte($date, $this->valid_from)) {
			return true;
		}
		
		// Specific Period
		if (Yii::app()->localtime->isGte($date, $this->valid_from) && Yii::app()->localtime->isLte($date, $this->valid_to)) {
			return true;
		}
		
		return false;
		
	}
	
	
}
