<?php

/**
 * This is the model class for table "learning_testtrack_times".
 *
 * The followings are the available columns in table 'learning_testtrack_times':
 * @property integer $idTrack
 * @property integer $idReference
 * @property integer $idTest
 * @property string $date_attempt
 * @property integer $number_time
 * @property double $score
 * @property string $score_status
 * @property string $date_begin
 * @property string $date_end
 * @property integer $time
 */
class LearningTesttrackTimes extends CActiveRecord
{
	const SCORE_STATUS_VALID = 'valid';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrackTimes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack_times';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_begin, date_end, time', 'required'),
			array('idTrack, idReference, idTest, number_time, time', 'numerical', 'integerOnly'=>true),
			array('score', 'numerical'),
			array('score_status', 'length', 'max'=>50),
			array('date_attempt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idReference, idTest, date_attempt, number_time, score, score_status, date_begin, date_end, time', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('date_attempt medium', 'date_begin medium', 'date_end medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idReference' => 'Id Reference',
			'idTest' => 'Id Test',
			'date_attempt' => Yii::t('standard', 'Date Attempt'),
			'number_time' => 'Number Time',
			'score' => 'Score',
			'score_status' => 'Score Status',
			'date_begin' => 'Date Begin',
			'date_end' => 'Date End',
			'time' => 'Time',
		);
	}

    /**
     * (non-PHPdoc)
     * @see CActiveRecord::afterSave()
     */
    public function afterSave() {

        // Raise event right after the user finished test attempt
        Yii::app()->event->raise('afterUserMakeTestAttempt', new DEvent($this, array(
            'attempt' => $this, // AR relation
        )));

        return parent::afterSave();

    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idTest',$this->idTest);
		$criteria->compare('date_attempt',Yii::app()->localtime->fromLocalDateTime($this->date_attempt),true);
		$criteria->compare('number_time',$this->number_time);
		$criteria->compare('score',$this->score);
		$criteria->compare('score_status',$this->score_status,true);
		$criteria->compare('date_begin',Yii::app()->localtime->fromLocalDateTime($this->date_begin),true);
		$criteria->compare('date_end',Yii::app()->localtime->fromLocalDateTime($this->date_end),true);
		$criteria->compare('time',$this->time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}