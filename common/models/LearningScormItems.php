<?php

/**
 * This is the model class for table "learning_scorm_items".
 *
 * The followings are the available columns in table 'learning_scorm_items':
 * @property integer $idscorm_item
 * @property integer $idscorm_organization
 * @property integer $idscorm_parentitem
 * @property string $adlcp_prerequisites
 * @property string $adlcp_maxtimeallowed
 * @property string $adlcp_timelimitaction
 * @property string $adlcp_datafromlms
 * @property string $adlcp_masteryscore
 * @property string $item_identifier
 * @property string $identifierref
 * @property integer $idscorm_resource
 * @property string $isvisible
 * @property string $parameters
 * @property string $title
 * @property integer $nChild
 * @property integer $nDescendant
 * @property string $adlcp_completionthreshold
 */
class LearningScormItems extends CActiveRecord
{
	public $itemCount; //used for search purposes

	/**
	 * @var The array of questions and answers inside the current item
	 */
	private $_questionAnswers;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormItems the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idscorm_organization, idscorm_parentitem, idscorm_resource, nChild, nDescendant', 'numerical', 'integerOnly' => true),
			array('adlcp_prerequisites, adlcp_masteryscore', 'length', 'max' => 200),
			array('adlcp_maxtimeallowed, adlcp_timelimitaction', 'length', 'max' => 24),
			array('item_identifier, identifierref', 'length', 'max' => 255),
			array('adlcp_datafromlms', 'length', 'max' => 4096),
			array('parameters, title', 'length', 'max' => 100),
			array('adlcp_completionthreshold', 'length', 'max' => 10),
			array('isvisible', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_item, idscorm_organization, idscorm_parentitem, adlcp_prerequisites, adlcp_maxtimeallowed, adlcp_timelimitaction, adlcp_datafromlms, adlcp_masteryscore, item_identifier, identifierref, idscorm_resource, isvisible, parameters, title, nChild, nDescendant, adlcp_completionthreshold', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scormItemsTrack' => array(self::HAS_MANY, 'LearningScormItemsTrack', 'idscorm_item'),
			'scorm_org' => array(self::BELONGS_TO, 'LearningScormOrganizations', 'idscorm_organization'),
			'scorm_resource' => array(self::BELONGS_TO, 'LearningScormResources', 'idscorm_resource'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_item' => 'Idscorm Item',
			'idscorm_organization' => 'Idscorm Organization',
			'idscorm_parentitem' => 'Idscorm Parentitem',
			'adlcp_prerequisites' => 'Adlcp Prerequisites',
			'adlcp_maxtimeallowed' => 'Adlcp Maxtimeallowed',
			'adlcp_timelimitaction' => 'Adlcp Timelimitaction',
			'adlcp_datafromlms' => 'Adlcp Datafromlms',
			'adlcp_masteryscore' => 'Adlcp Masteryscore',
			'item_identifier' => 'Item Identifier',
			'identifierref' => 'Identifierref',
			'idscorm_resource' => 'Idscorm Resource',
			'isvisible' => 'Isvisible',
			'parameters' => 'Parameters',
			'title' => 'Title',
			'nChild' => 'N Child',
			'nDescendant' => 'N Descendant',
			'adlcp_completionthreshold' => 'Adlcp Completionthreshold',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idscorm_item', $this->idscorm_item);
		$criteria->compare('idscorm_organization', $this->idscorm_organization);
		$criteria->compare('idscorm_parentitem', $this->idscorm_parentitem);
		$criteria->compare('adlcp_prerequisites', $this->adlcp_prerequisites, true);
		$criteria->compare('adlcp_maxtimeallowed', $this->adlcp_maxtimeallowed, true);
		$criteria->compare('adlcp_timelimitaction', $this->adlcp_timelimitaction, true);
		$criteria->compare('adlcp_datafromlms', $this->adlcp_datafromlms, true);
		$criteria->compare('adlcp_masteryscore', $this->adlcp_masteryscore, true);
		$criteria->compare('item_identifier', $this->item_identifier, true);
		$criteria->compare('identifierref', $this->identifierref, true);
		$criteria->compare('idscorm_resource', $this->idscorm_resource);
		$criteria->compare('isvisible', $this->isvisible, true);
		$criteria->compare('parameters', $this->parameters, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('nChild', $this->nChild);
		$criteria->compare('nDescendant', $this->nDescendant);
		$criteria->compare('adlcp_completionthreshold', $this->adlcp_completionthreshold, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * @param $idResource
	 * @param null $userId
	 * @return CDbCriteria
	 */
	public function getItemsCriteria($idResource, $userId = null)
	{
		$criteria = new CDbCriteria();
		if ($userId !== null)
			$criteria->with = array('scormItemsTrack' => array('scopes' => array('byUser' => $userId)), 'scormItemsTrack.scormItemsTracking');
		else
			$criteria->with = array('scormItemsTrack');

		$criteria->select = array('t.*', 'scormItemsTrack.*', 'count(scormItemsTrack.status) AS itemCount');
		$criteria->together = true;
		$criteria->group = 't.idscorm_item, scormItemsTrack.status';
		$criteria->condition = 't.idscorm_organization = :idResource';
		$criteria->params = array(':idResource' => $idResource);

		return $criteria;
	}

	/**
	 * Returns the possible interaction types with their corresponding label
	 */
	public static function getAllowedInteractionTypes() {
		return array(
			"true-false" => Yii::t('interactions', 'True or false'),
			"choice" =>  Yii::t('test', '_QUEST_CHOICE_MULTIPLE'),
			"fill-in" =>  Yii::t('test', '_QUEST_EXTENDED_TEXT'),
			"long-fill-in" =>  Yii::t('test', '_QUEST_EXTENDED_TEXT'),
			"matching" => Yii::t('interactions', 'Matching couples'),
			"performance" => Yii::t('interactions', 'Performance questions'),
			"sequencing" => Yii::t('interactions', 'Sorting elements'),
			"likert" => Yii::t('interactions', 'Single value in a range'),
			"numeric" => Yii::t('interactions', 'Number'),
		);
	}

	/**
	 * Returns the cache key for the current scorm item
	 */
	private function _getCacheKey($other = false) {
		$cacheKeyActors = array(
			Yii::app()->db->connectionString,
			$this->getPrimaryKey(),
			Yii::app()->user->id,
			$other
		);

		$cacheKey = md5(json_encode($cacheKeyActors));
		return $cacheKey;
	}

	/**
	 * Flushes (delete) all cache files
	 */
	public function flushQuestionsAndAnswersCache() {
		// if (isset(Yii::app()->cache_sco_responses))
		// Yii::app()->cache_sco_responses->delete($this->_getCacheKey());
		// Disabling this method, because SCO cache will expire in 2 hours (performance improvement)
		return;
	}


	/**
	 * Decodes short identifiers
	 * @param $string
	 */
	private function _decodeShortIdentifier($string) {
		return str_replace("_", " ", $string);
	}

	/**
	 * Returns an array (cached) of all questions and answers inside the current scorm item.
	 * It will search all tracks generated for this scorm_item and build the array.
	 *
	 * The array is cached using DoceboFileCache
	 */
	public function collectQuestionsAndAnswers($users = false) {
		// Check if we already have the array unserialized and ready to be used
		if(!$this->_questionAnswers || $users !== false) {
			// We have to build the array. Let's start querying the cache
			if (isset(Yii::app()->cache_sco_responses)) {
				$cache_key = $this->_getCacheKey($users);
				$this->_questionAnswers = Yii::app()->cache_sco_responses->get($cache_key);
			}

			if(!$this->_questionAnswers) { // No valid result in cache -> build it from all other sco tracks
				$this->_questionAnswers = array();

				// Find the current scorm version of this item
				$scorm_version = $this->scorm_resource->scormPackage->scormVersion;
				if(!$scorm_version)
					return $this->_questionAnswers;

				// Here we use the same model object to parse CMI data returned by the CDbCommand direct query
				$track = LearningScormTracking::model(); /* @var $track LearningScormTracking */
				$courseId = Yii::app()->db->createCommand()
					->select('idCourse')
					->from(LearningOrganization::model()->tableName())
					->where('idResource = :idResource AND objectType = :objectType', array(
						':idResource' => $this->idscorm_organization,
						':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG
					))
					->queryScalar();

                IF ( !Yii::app()->user->getIsAdmin() ) {
				// Run through all current tracks for the same sco item and collect all available learner responses
					$command = Yii::app()->db->createCommand()
						->select('st.xmldata')
						->from($track->tableName().' st')
						->join(LearningCourseuser::model()->tableName().' cu', 'cu.idUser = st.idUser')
						->where('st.idscorm_item = :idscorm_item AND cu.idCourse = :idCourse', array(
							':idscorm_item' => $this->idscorm_item,
							':idCourse' => $courseId,
						));

					if(is_array($users) && !empty($users))
					{
						$command->andWhere(array('in', 'cu.idUser', $users));
					}

					$reader = $command->query();
                }
                else {
					$command = Yii::app()->db->createCommand()
						->select('st.xmldata')
						->from($track->tableName().' st')
						->join(CoreUserPU::model()->tableName().' cup', 'cup.puser_id = '.Yii::app()->user->getIdst())
						->join(LearningCourseuser::model()->tableName().' cu', 'cu.idUser = st.idUser')
						->where('st.idscorm_item = :idscorm_item AND cup.user_id = st.idUser AND cu.idCourse = :idCourse', array(
							':idscorm_item' => $this->idscorm_item,
							':idCourse' => $courseId,
						));

					if(is_array($users) && !empty($users))
					{
						$command->andWhere(array('in', 'cu.idUser', $users));
					}

					$reader = $command->query();
                }
				while ($row = $reader->read()) {

					// Let the track decode the interactions inside it
					$track->xmldata = $row['xmldata'];
					$user_interactions = $track->getRawLearnerInteractions($scorm_version);

					// Loop through all question answers and only select certain interactions
					foreach($user_interactions as $index => $user_interaction) {
						$question_id = ($user_interaction['id'])? $user_interaction['id'] : $index;
						$question_type = $user_interaction['type'];
						if(in_array($question_type, array('choice', 'true-false', 'matching', 'fill-in', 'sequencing', 'other'))) { // todo: add support for other questions types??
							// Create a new record for the question (if not present)
							if(!isset($this->_questionAnswers[$question_id])) {
								$this->_questionAnswers[$question_id] = array (
									'description' => $user_interaction['description'],
									'type' => $question_type,
									'correct_responses' => $user_interaction['correct_responses'],
									'total_responses' => 0
								);

								if(in_array($question_type, array('choice', 'true-false'))) {
									$this->_questionAnswers[$question_id]['responses'] = array();

									// Add answers from correct responses
									if(!empty($user_interaction['correct_responses'])) {
										foreach($user_interaction['correct_responses'] as $answer) {
											$answer = is_array($answer) ? $answer : array($answer);
											foreach($answer as $part) {
												if(!isset($this->_questionAnswers[$question_id]['responses'][$part])) {
													$this->_questionAnswers[$question_id]['responses'][$part] = array (
														'counter' => 0,
														'description' => $this->_decodeShortIdentifier($part)
													);
												}
											}
										}
									}
								} else if($question_type == 'matching') {
									// For matching questions we build the left and right sets
									$this->_questionAnswers[$question_id]['sets'] = array('left' => array(), 'right' => array());

									// Add correct responses to the array
									if(!empty($user_interaction['correct_responses'])) {
										foreach($user_interaction['correct_responses'] as $key => $correct_response) {
											$left = ($correct_response[0])? $correct_response[0] : $key;
											if(!in_array($left, $this->_questionAnswers[$question_id]['sets']['left']))
												$this->_questionAnswers[$question_id]['sets']['left'][] = $left;
											if(!in_array($correct_response[1], $this->_questionAnswers[$question_id]['sets']['right']))
												$this->_questionAnswers[$question_id]['sets']['right'][] = $correct_response[1];
										}
									}
								}
							}

							// Extract only learner responses
							if($user_interaction['learner_response'])
								$answers = is_array($user_interaction['learner_response']) ? $user_interaction['learner_response'] : array($user_interaction['learner_response']);
							else
								$answers = array();

							if(!empty($answers)) {

								// Increment total questions counter
								$this->_questionAnswers[$question_id]['total_responses']++;

								if(in_array($question_type, array('choice', 'true-false'))) {
									// Add answers from learner responses
									foreach($answers as $answer) {
										if(!isset($this->_questionAnswers[$question_id]['responses'][$answer])) {
											$this->_questionAnswers[$question_id]['responses'][$answer] = array (
												'counter' => 0,
												'description' => $this->_decodeShortIdentifier($answer)
											);
										}
									}

									// Increment answers
									switch($question_type) {
										case 'true-false':
											$this->_questionAnswers[$question_id]['responses'][$answers[0]]['counter']++;
											break;
										case 'choice':
											foreach($answers as $answer)
												$this->_questionAnswers[$question_id]['responses'][$answer]['counter']++;
											break;
									}

								} else if($question_type == 'matching') {
									// Do some further checkings because sometimes 'sets' information is missing: in this case just ignore the question
									// NOTE: multiple "isset()" checks are performed in order to avoid as much as possible the raising of warnings and notices
									if (isset($this->_questionAnswers[$question_id])
										&& isset($this->_questionAnswers[$question_id]['sets'])
										&& isset($this->_questionAnswers[$question_id]['sets']['left'])
										&& isset($this->_questionAnswers[$question_id]['sets']['right']))
									{
										foreach($answers as $key => $response_couple) {
											$left = ($response_couple[0])? $response_couple[0] : $key;
											if(!in_array($left, $this->_questionAnswers[$question_id]['sets']['left']))
												$this->_questionAnswers[$question_id]['sets']['left'][] = $left;
											if(!in_array($response_couple[1], $this->_questionAnswers[$question_id]['sets']['right']))
												$this->_questionAnswers[$question_id]['sets']['right'][] = $response_couple[1];
										}
									}
								} else if($question_type == 'fill-in' || $question_type == 'sequencing' || $question_type == 'other') {
									if(isset($this->_questionAnswers[$question_id]['responses'][$answers[0]]['counter'])) {
										$this->_questionAnswers[$question_id]['responses'][$answers[0]]['counter']++;
										if($user_interaction['result'] === 'correct')
											$this->_questionAnswers[$question_id]['responses'][$answers[0]]['correct'] = true;
									} else
										$this->_questionAnswers[$question_id]['responses'][$answers[0]] = array (
											'counter' => 1,
											'description' => $this->_decodeShortIdentifier($answers[0]),
											'correct' => $user_interaction['result'] === 'correct'
										);
								}
							}
						}
					}
				}

				// Save results in cache
				if (isset(Yii::app()->cache_sco_responses))
					Yii::app()->cache_sco_responses->set($cache_key, $this->_questionAnswers, 2*60*60); // Cache expires in 2 hours
			}
		}

		return $this->_questionAnswers;
	}


	/**
	 * Detect if the current sco chapter is a folder or a playable chapter
	 * @return bool
	 */
	public function isFolder() {
		return ((int)$this->nChild > 0 || (int)$this->nDescendant > 0);
	}

}