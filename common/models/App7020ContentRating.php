<?php

/**
 * This is the model class for table "app7020_question_history".
 *
 * The followings are the available columns in table 'app7020_question_history':
 * @property integer $id
 * @property integer $idContent
 * @property integer $idUser
 * @property integer $rating
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 * @property CoreUser $user
 */
class App7020ContentRating extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_content_rating';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        
        return array(
            array('idContent, idUser, rating', 'required'),
            array('idContent, idUser, rating', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
            'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idContent' => 'Id Content',
            'idUser' => 'Id User',
            'rating' => 'Rating'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idUser', $this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020TopicContent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Checks if in the history table exists a record for the selected user and question. 
     * If yes - updates the record, otherwise - a new record will be created
     * @return boolean
     */
    public function secureSave(){
        if(!$this->idUser || !$this->idContent){
            return false;
        }
        if($existingRate = $this->findByAttributes(array('idContent' => $this->idContent, 'idUser' => $this->idUser))){
            $existingRate->rating = $this->rating;
            return $existingRate->update();
        } else {
            return $this->save();
        }
    }
    
    /**
     * Recalculates content rating
     * @param integer $contentId
     * @param integer $floatDecimals
     * @return float the new content rationg
     */
    
    public static function calculateContentRating($contentId, $floatDecimals = 2){
        $sqlCommand = Yii::app()->db->createCommand();
        $sqlCommand->select("SUM(rating) AS ratingsSum, COUNT(id) AS ratingsCount");
        $sqlCommand->from(self::model()->tableName());
        $sqlCommand->where(" idContent = :contentId", array(":contentId" => $contentId));
        $dataArray = $sqlCommand->queryRow();
        if($dataArray['ratingsCount'] == 0) {
            return 0;
        }
        
        return round($dataArray['ratingsSum']/$dataArray['ratingsCount'], $floatDecimals);
        
    }
    
    
 }
