<?php
/**
 * Basic SSO interface that all sso handlers will have to implement,
 * if they want to intercept sso requests.
 */

interface ISsoHandler {
    /**
     * This method should performs the authentication checks and return (if the user is logged in)
     * the userid that Docebo must set as "logged in" in the LMS. In some cases, it can even not return
     * and perform a redirect to the external authentication portal.
     * @return string Userid of the logged in user
     * @throws Exception in case something is wrong and SSO cannot be performed
     */
    public function login();

    /**
     * This method returns the URL to redirect to, after a successful LMS login
     * @return string URL to redirect to
     */
    public function getLandingURL();
}