<?php

/**
 * This is the model class for table "core_org_chart_tree".
 *
 * The followings are the available columns in table 'core_org_chart_tree':
 * @property integer      $idOrg
 * @property integer      $idParent
 * @property string       $path
 * @property integer      $lev
 * @property integer      $iLeft
 * @property integer      $iRight
 * @property string       $code
 * @property integer      $idst_oc
 * @property integer      $idst_ocd
 * @property string       $associated_policy
 * @property string       $associated_template
 *
 * The followings are the available model relations:
 * @property CoreOrgChart $coreOrgChart
 * @property CoreOrgChart $coreOrgChartTranslated
 * @property CoreOrgChart $coreOrgChartTranslatedDefaultLang
 */
class CoreOrgChartTree extends CActiveRecord {


	const SELECT_THIS 				= 1;
	const SELECT_THIS_AND_DESC 		= 2;

	public $hasChildren = FALSE;
	public $confirm;
	public $min_ileft;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return CoreOrgChartTree the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_org_chart_tree';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//				array(
//					'path',
//					'required'
//				),
			array(
				'idParent, lev, iLeft, iRight, idst_oc, idst_ocd',
				'numerical',
				'integerOnly' => TRUE
			),
			array(
				'code, associated_template',
				'length',
				'max' => 255
			),
// 			array(
// 				'code',
// 				'unique'
// 			),
			array(
				'associated_policy',
				'length',
				'max' => 11
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'idOrg, idParent, path, lev, iLeft, iRight, code, idst_oc, idst_ocd, associated_policy, associated_template',
				'safe',
				'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

		/*
		$lang = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => Yii::app()->user->idst,
				'path_name' => 'ui.language'
		));
		if (!$lang) {
			$lang = CoreUser::getDefaultLangCode();
		}
		else {
			$lang = $lang->value;
		}
		*/



		return array(
			'coreOrgChart' => array(
				self::BELONGS_TO,
				'CoreOrgChart',
				'idOrg'
			),

			// Get the node in the current language
			'coreOrgChartTranslated' => array(
				self::BELONGS_TO,
				'CoreOrgChart',
				'idOrg',
				'condition' => 'coreOrgChartTranslated.lang_code = :lang_code',
				'params' => array(':lang_code' => Lang::getFullLangName(Yii::app()->getLanguage()) )
			),

			// Get the node in the platform's default language
			// This is usually a fallback to the 'coreOrgChartTranslated' relation
			'coreOrgChartTranslatedDefaultLang' => array(
				self::BELONGS_TO,
				'CoreOrgChart',
				'idOrg',
				'condition' => 'coreOrgChartTranslatedDefaultLang.lang_code = :lang_code',
				'params' => array(':lang_code' => Settings::get('default_language', 'english') )
			),
			'groupMembers' => array(self::HAS_MANY, 'CoreGroupMembers', array('idst' => 'idst_ocd')),
		);
	}

	/**
	 * Returns an array of org nodes that the user belongs to
	 * @param $userId the IDST of the user
	 * @param $additionalCriteria CDbCriteria additional search filter, optional (false = no additional criteria)
	 * @return CoreOrgChartTree[]
	 */
	public function getOrgNodesByUserId($userId, $additionalCriteria = false)
	{
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'groupMembers' => array(
				'alias' => 'm',
				'select' => FALSE,
				'joinType' => 'INNER JOIN',
				'condition' => 'm.idstMember = :id_user',
				'params' => array(':id_user' => $userId)
			)
		);
		if (!empty($additionalCriteria) && is_object($additionalCriteria) && get_class($additionalCriteria) == 'CDbCriteria') {
			$criteria->mergeWith($additionalCriteria);
		}

		$results = $this->findAll($criteria);
		return $results;
	}

	/**
	 * @param CoreOrgChartTree $node
	 * @return CActiveRecord[]
	 */
	public function getAncestorsNodes($node)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "iLeft < " . $node->iLeft . " AND iRight > " . $node->iRight;
		$criteria->order = 'iLeft DESC';
		$results = $this->findAll($criteria);
		return $results;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idOrg' => 'Id Org',
			'idParent' => 'Id Parent',
			'path' => 'Path',
			'lev' => 'Lev',
			'iLeft' => 'I Left',
			'iRight' => 'I Right',
			'code' => Yii::t('standard', '_CODE_BRANCH'),
			'idst_oc' => 'Idst Oc',
			'idst_ocd' => 'Idst Ocd',
			'associated_policy' => 'Associated Policy',
			'associated_template' => 'Associated Template',
		);
	}

	public function behaviors() {
		return array(
			'nestedSetBehavior' => array(
				'class' => 'common.extensions.NestedSetBehavior.NestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
				//'hasManyRoots' => true,
				//'rootAttribute' => 'is_root',
			),
		);
	}

	public function scopes() {
		if (Yii::app()->hasComponent('session')) {
			$currentNodeId = Yii::app()->session['currentNodeId'];
		} else {
			$root = self::getOrgRootNode();
			$currentNodeId = $root->getPrimaryKey();
		}
		return array(
			'nodeUsersOnly' => array(
				'condition' => $this->getTableAlias(FALSE, FALSE) . '.idOrg = :idOrg',
				'params' => array(':idOrg' => $currentNodeId),
			),
		);
	}

//		public function nodesListUsersOnly($allChildrenNodesIds) {
//			if (!empty($allChildrenNodesIds)) {
//				$this->getDbCriteria()->mergeWith(array(
//					'condition' => $this->getTableAlias(false, false) . '.idOrg IN (\''.implode('\',\'', $allChildrenNodesIds).'\')',
//				));
//			}
//			return $this;
//		}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idOrg', $this->idOrg);
		$criteria->compare('idParent', $this->idParent);
		$criteria->compare('path', $this->path, TRUE);
		$criteria->compare('lev', $this->lev);
		$criteria->compare('iLeft', $this->iLeft);
		$criteria->compare('iRight', $this->iRight);
		$criteria->compare('code', $this->code, TRUE);
		$criteria->compare('idst_oc', $this->idst_oc);
		$criteria->compare('idst_ocd', $this->idst_ocd);
		$criteria->compare('associated_policy', $this->associated_policy, TRUE);
		$criteria->compare('associated_template', $this->associated_template, TRUE);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	public function beforeSave() {

		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				// creation unique idst
				$st_oc = new CoreSt();
				$st_ocd = new CoreSt();
				if (!$st_oc->save() || !$st_ocd->save()) {
					return FALSE;
				}
				$this->idst_oc = $st_oc->idst;
				$this->idst_ocd = $st_ocd->idst;
			}

			// Clean ORGCHART cache
			$this->flushCache();

			return TRUE;
		}
		return FALSE;
	}


	/**
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {
		// Clean ORGCHART cache
		$this->flushCache();
		if(PluginManager::isPluginActive('GamificationApp')) {
			GamificationContest::cleanUpBranches($this->idOrg);
			GamificationLeaderboard::cleanUpBranches($this->idOrg);
		}
	}


	/**
	 * Flushes (delete) all cache files from ORGCHART specific cache [path]
	 *
	 * See main-live.php for class parameters and specifically the path where Orgchart cache resides.
	 *
	 */
	public function flushCache() {
		if (isset(Yii::app()->cache_orgchart)) {
			Yii::app()->cache_orgchart->flush();
		}
	}


	public function afterSave() {
		parent::afterSave();

		if ($this->isNewRecord) {
			// create records in core_group
			$group_oc = new CoreGroup();
			$group_oc->idst = $this->idst_oc;
			$group_oc->groupid = '/oc_'.$this->idOrg;
			$group_oc->description = '';
            $group_oc->hidden = TRUE;

			$group_ocd = new CoreGroup();
			$group_ocd->idst = $this->idst_ocd;
			$group_ocd->groupid = '/ocd_'.$this->idOrg;
			$group_ocd->description = '';
            $group_ocd->hidden = TRUE;

			if (!$group_oc->save() || !$group_ocd->save()) {
				return FALSE;

			}


			$this->updateEnrollmentRulesBranches();
		}

        Yii::app()->event->raise('afterSaveOrgChartTreeNode', new DEvent($this, array('CoreOrgChartTree' => $this)));
	}


	/*
	 * returns the code of a org branch
	 */
	public static function getFoldersFromCode($code) {
		$tree_folders = array();

		$rows = CoreOrgChartTree::model()->findAllByAttributes(array('code' => $code));

		foreach ($rows as $row) {
			/* @var $row CoreOrgChartTree */
			$tree_folders[$row->idOrg] = $row->idOrg;
		}

		return $tree_folders;
	}

	/**
	 * Returns oc and ocd of a given folder
	 *
	 * @param  int $idOrg
	 *
	 * @return array
	 */
	public static function getFolderGroups($idOrg) {
		$res = array();

		$rows = CoreOrgChartTree::model()->findAllByAttributes(array(
			'idOrg' => $idOrg
		));

		foreach ($rows as $row) {
			/* @var $row CoreOrgChartTree */
			$res[] = $row->idst_oc;
			$res[] = $row->idst_ocd;
		}

		return $res;
	}

	/*********************************************************************************/

	/**
	 * @param $node CoreOrgChartTree
	 */
	public function removeNode($node) {

		// remove related records in core_group and core_group_members
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst', array($node->idst_oc, $node->idst_ocd));
		CoreGroupMembers::model()->deleteAll($criteria);

		CoreGroup::model()->deleteByPk($node->idst_oc);
		CoreGroup::model()->deleteByPk($node->idst_ocd);

		//CoreOrgChartTree::model()->deleteByPk($node->idOrg);
		// We are deleting the $node, NOT $this
		$node->deleteNode();

		CoreOrgChart::model()->deleteAllByAttributes(array('id_dir' => $node->idOrg));

		$core_enroll_rule_item = CoreEnrollRuleItem::model();
		$core_enroll_rule_item->deleteAllByAttributes(array('item_type' => 'branch', 'item_id' => $node->idOrg));

		if(PluginManager::isPluginActive('NotificationApp')) {
			CoreNotificationUserFilter::model()->deleteAllByAttributes(array('idItem' => $node->idOrg, 'type' => CoreNotificationUserFilter::NOTIF_FILTER_BRANCH));
		}
	}


	/*********************************************************************************/

	// temp use in admin/site/actionAjaxTree
	public function getParentNodes() {
		$lang             = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$coreOrgChartTree = $this->with(array(
			'coreOrgChart' => array(
				'on' => 'coreOrgChart.lang_code = :lang',
				'params' => array('lang' => $lang->lang_code)
			)
		))->findAllByAttributes(array('idParent' => 0));
		$childrenData     = array();
		if (!empty($coreOrgChartTree)) {
			foreach ($coreOrgChartTree as $parentNode) {
				$childrenData[] = array(
					'attr' => array(
						'id' => 'node_' . $parentNode->idOrg,
						'rel' => 'folder'
					),
					'data' => $parentNode->coreOrgChart->translation,
					//						'state' => 'open',
					'state' => 'close',
				);
			}
		}
		if (!empty($childrenData)) {
			return $childrenData;
		}
		return FALSE;
	}


	/**
	 * Get all org charts available to the current user. If $adminFilter is true and
	 * the current user is a power user, the available branches are only those assigned, their ancestors and descendants
	 *
	 * @param bool $indents
	 * @param bool $adminFilter - if TRUE will only filter/return branches that are
	 *      available to the current user and the current user is an admin or power user
	 * @param bool $includeCurrentChild - if FALSE (default) will only include the
	 *      parents/descendants of the available branches but not the branches themselves
	 *
	 * @return CActiveRecord[]
	 */
	public static function getFullOrgChartTree($indents = FALSE, $adminFilter = FALSE, $includeCurrentChild = FALSE) {
		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
				'coreOrgChart' => array(
						'on' => 'coreOrgChart.lang_code = :lang',
						'params' => array(
								'lang' => $lang->lang_code,
								//'transl' => '',
						),
						'joinType' => 'INNER JOIN',
				),
				'coreOrgChartTranslatedDefaultLang',  // for language fallback to LMS default language

		);

		$criteria = new CDbCriteria();
		$puLeafs = array();

		// Filter by PowerUser ?
		if ($adminFilter && Yii::app()->user->getIsAdmin()) {
			$orgCharts = CoreAdminTree::model()->getPowerUserOrgChartsOnly(Yii::app()->user->id);
			$puIdOrgIds = $orgCharts['nodes'];
			$puLeafs = $orgCharts['leafs'];

			// Let plugins decide which node to show
			Yii::app()->event->raise('OrgchartFilterNodeForPU', new DEvent(self, array('puser_id' => Yii::app()->user->id, 'puOrgIds' => &$puIdOrgIds)));

			if (!empty($puIdOrgIds)) {
				$criteria->addInCondition('idOrg', $puIdOrgIds);
			}
			else {
				return array(
					'list' => array(),
					'leafs' => array(),
				);
			}
		}

		// Order bu iLeft so they are shown in the tree sequence
		$criteria->order = 'iLeft ASC';

		$list = CoreOrgChartTree::model()->with($with)->findAll($criteria);

		// Fix display value: use translation in current LMS language or fallback to LLMS's default language
		if (is_array($list)) {
			foreach ($list as $i => $node) {
				$currentLangTranslation = $node->coreOrgChart->translation;
				$translation = $currentLangTranslation ? $currentLangTranslation : $node->coreOrgChartTranslatedDefaultLang->translation;
				if ($indents) {
					$translation = str_repeat("&nbsp;&nbsp;", $node->lev - 1) . $translation;
				}
				$node->coreOrgChart->translation = $translation;
				$list[$i] = $node;
			}
		}

		$result = array(
				'list' => $list,
				'leafs' => $puLeafs,
		);

		return $result;

	}



	/**
	 * Get all org charts available to the current user. If $adminFilter is true and
	 * the current user is a power user, the available branches are only those assigned, their ancestors and descendants
	 *
	 * @param bool $indents Add N spacing infront of the node name, according to its "level"
	 * @param bool $adminFilter  If TRUE,  filter nodes by PowerUser assignments
	 * @param string $startingNodeId Start loading the tree starting from this Node ID (idOrg)
	 * @param string $immediateChildrenOnly Stop at next lower level, i.e. load only immediate children
	 * @param string $includeStartingNode Also include starting in tree data (in LazyLoading we do NOT need it, for example)
	 *
	 * @return CActiveRecord[]
	 */
	public static function getFullOrgChartTreeV2(
			$indents = FALSE,
			$adminFilter = FALSE,
			$startingNodeId=false,
			$immediateChildrenOnly=false,
			$includeStartingNode=true) {

		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
			'coreOrgChart' => array(
				'on' => 'coreOrgChart.lang_code = :lang',
				'params' => array(
					'lang' => $lang->lang_code,
				),
				'joinType' => 'INNER JOIN',
			),
			'coreOrgChartTranslatedDefaultLang',  // for language fallback to LMS default language
		);

		$criteria = new CDbCriteria();
		$puLeafs = array();
		$rootNode = self::getOrgRootNode();

		// Filter by PowerUser ?
		if ($adminFilter && Yii::app()->user->getIsAdmin()) {
			$orgCharts = CoreAdminTree::model()->getPowerUserOrgChartsOnly(Yii::app()->user->id);
			$puIdOrgIds = $orgCharts['nodes'];

			// Let plugins decide which node to show
			Yii::app()->event->raise('OrgchartFilterNodeForPU', new DEvent(self, array('puser_id' => Yii::app()->user->id, 'puOrgIds' => &$puIdOrgIds)));

			if (!empty($puIdOrgIds)) {
				$criteria->addInCondition('idOrg', $puIdOrgIds);
			}
			else {
				$criteria->addInCondition('idOrg', array($rootNode->idOrg));
			}
		}

		// There is always a Staring Node; Root, for example
		$startingNode = ($startingNodeId !== false) ? CoreOrgChartTree::model()->findByPk((int) $startingNodeId) : $rootNode;

		// We are still not sure about the starting node, because $startingNodeId may point to a non-existent branch
		// Make sure we really have it or assign the root node as a measure of last resort
		if (!$startingNode) {
			$startingNode = $rootNode;
		}

		// Narow nodes to SrartingNode descendants only
		// But preserve Starting Node also, if required
		if ($includeStartingNode) {
			$criteria->addCondition('(iLeft >= :iLeft) AND (iRight <= :iRight)');
		}
		else {
			$criteria->addCondition('(iLeft > :iLeft) AND (iRight < :iRight)');
		}
		$criteria->params[':iLeft'] 	= $startingNode->iLeft;
		$criteria->params[':iRight'] 	= $startingNode->iRight;


		// Get List of immediate children and put them in IN condition, if requested
		// But preserve Starting Node also, if required
		if ($immediateChildrenOnly) {
			$criteria->addCondition("(lev <= " . (int) ($startingNode->lev+1).")");
			/*$childrenIds = $startingNode->getImmediateChildren();
			if (is_array($childrenIds)) {
				if ($includeStartingNode) {
					$childrenIds[] = $startingNode->idOrg;
				}
				$criteria->addInCondition('idOrg', $childrenIds);
			}*/
		}

		// Order by iLeft so they are shown in the tree sequence
		$criteria->order = 'iLeft ASC';
		$list = CoreOrgChartTree::model()->with($with)->findAll($criteria);

		// Fix display value: use translation in current LMS language or fallback to LMS's default language
		foreach ($list as $i => $node) {
			$currentLangTranslation = $node->coreOrgChart->translation;
			$translation = $currentLangTranslation ? $currentLangTranslation : $node->coreOrgChartTranslatedDefaultLang->translation;
			if ($indents) {
				$translation = str_repeat("&nbsp;&nbsp;", $node->lev - 1).$translation;
			}
			$node->coreOrgChart->translation = $translation;
			$list[$i] = $node;
		}

		$result = array(
				'list' => $list,
				'leafs' => $puLeafs,
		);

		return $result;

	}


	/**
	 * Return a string representing the PATH to THIS node, optionaly excluding the root node (and if THIS is the root node, return empty string)
	 * @param string $separator
	 * @param string $excludeRoot
	 * @return string
	 */
	public function getNodePath($separator='>', $excludeRoot = FALSE) {
		$nodePath = '';

		if ($this->isRoot() && $excludeRoot) {
			return $nodePath;
		}

		foreach ($this->ancestors()->findAll() as $node) {
			if ($node->isRoot() && $excludeRoot) {
				continue;
			}
			$nodePath .= self::getTranslatedName($node->idOrg) . "$separator" ;
		}

		// Add THIS node itself as part of the path
		$nodePath .= self::getTranslatedName($this->idOrg);

		return $nodePath;
	}


	/**
	 * Return a model of the ROOT node.
	 * Root is the one having the smallest iLeft id
	 *
	 * @return CoreOrgChartTree
	 */
	public static function getOrgRootNode() {

		// First, Find the min(iLeft) among all nodes
		$query = "SELECT MIN(iLeft) AS min_ileft FROM ".self::model()->tableName();
		$minLeftValue = Yii::app()->db->createCommand($query)->queryScalar();

		// Get the node model having it
		$rootNode = CoreOrgChartTree::model()->findByAttributes(array(
			'iLeft' => $minLeftValue,
		));
		return $rootNode;
	}

	/**
	 * Returns list of "oc_" groups of all children/descendant AND optionally the node's group itself ($includeMe = true)
	 *
	 * Note: This method does NOT use ocd_ resolving
	 *
	 * @param boolean $includeMe
	 * @return array
	 */
	public function getBranchGroups($includeMe=TRUE) {
		$groups = array();

		// Should we include the group of THIS model?
		if ($includeMe) {
			$groups[] = $this->idst_oc;
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition('iLeft > :myLeft AND iRight < :myRight');
		$criteria->params[':myLeft'] = $this->iLeft;
		$criteria->params[':myRight'] = $this->iRight;

		$children = self::model()->findAll($criteria);
		foreach ($children as $node) {
			$groups[] = $node->idst_oc;
		}

		return $groups;
	}



	/**
	 * Returns list of "oc_" groups of all children/descendant AND optionally the node's group itself ($includeMe = true)
	 *
	 * Note: This method does NOT use ocd_ resolving. It is identical to getBranchGroups but avoid using AR models
	 *
	 * @param boolean $includeMe
	 * @param string $select
	 * @return array
	 */
	public function getBranchGroupsPdo($includeMe=true, $select='idst_oc') {
	    $groups = array();

	    $command = Yii::app()->db->createCommand();
	    $command->select($select);
	    $command->from('core_org_chart_tree tree');

	    if ($includeMe) {
	       $command->where('(iLeft >= :left) and (iRight <= :right)');
	    }
	    else {
	        $command->where('(iLeft > :left) and (iRight < :right)');
	    }

	    $params = array(
	        ':left'    => $this->iLeft,
	        ':right'   => $this->iRight,
	    );
	    $groups = $command->queryColumn($params);

	    return $groups;
	}


	/**
	 * Returns list of all descendant AND optionally the node's group itself ($includeMe = true)
	 *
	 * Note: Using ocd_
	 *
	 * @param boolean $includeMe
	 * @return array
	 */
	public function getBranchGroupsByOcd($includeMe=TRUE) {
		$groups = array();

		$criteria = new CDbCriteria();
		$criteria->addCondition('idst=:idstOcd');
		$criteria->params[':idstOcd'] =  (int) $this->idst_ocd;

		$childrenGroups = CoreGroupMembers::model()->findAll($criteria);
		foreach ($childrenGroups as $group) {
			if ( ($group->idstMember == $this->idst_oc) && !$includeMe)
				continue;
			$groups[] = $group->idstMember;
		}

		return $groups;

	}



	/**
	 * Returns an array of User IDs, members of all groups of the branch (i.e. node and descendants), starting with THIS node.
	 *
	 * @param boolean $includeMe Include/exclude this node's group (oc_). Default = include
	 * @param array|boolean $includeGroups List of group IDs to filter by
	 * @param array|boolean $excludeGroups List of groups to exclude from the process
	 * @param bool $excludeSuspended
	 * @return array
	 */
	public function getBranchUsers($includeMe=TRUE, $includeGroups=false, $excludeGroups=false, $excludeSuspended=false) {

		$result = array();

		// Using oc_ traversing method, not ocd_ helper one
		$groups = $this->getBranchGroups($includeMe);

		// Filter by list of groups, i.e. get users ONLY from groups listed in the $includeGroups array ???
		if (is_array($includeGroups)) {
			$groups = array_intersect($groups, $includeGroups);
		}

		if (!is_array($groups) || (count($groups) <= 0)) {
			return $result;
		}

		$command = Yii::app()->db->createCommand()
			->from('core_group_members group_member')
			->join('core_user user', 'group_member.idstMember=user.idst')
			->where("group_member.idst IN (".implode(',', $groups).")");
		if($excludeSuspended)
			$command->andWhere('user.valid=1');

		// Return a string of comma separated IDs
		$command->select("distinct(user.idst)");
		$members = $command->queryAll(TRUE);
		if ($members)
			foreach ($members as $member)
				$result[] = $member['idst'];

		return $result;
	}


	/**
	 * Return information about USERS in THIS branch, in its descendants or in both (i.e. full branch).
	 * Provides set of parameters for fine tuned filtering.
	 *
	 * By default it returns simple array of User Ids of THIS branch and its descendants. As simple as that.
	 *
	 * This method uses PDO only and does not count on OCD groups in Core Org Chart Tree, but on left/right only!
	 *
	 * The format of the returned array (if it is an array) may vary depending on function parameters! Test and explore!
	 *
	 * @param string $includeMe Include users directly assigned to THIS brnach node?
	 * @param string $includeGroups Include ONLY branches associated to these groups (idst_oc!!)
	 * @param string $excludeGroups Exclude branches associated to these groups (idst_oc!!)
	 * @param string $meOnly Exclude all other brnahces but me (i.e. me only please)
	 * @param string $fullData Return full user set of data from core_user
	 * @param array $userFields If $fullData=true, return custom fields from core user, e.g. array("idst", "email",  "register_date")
	 * @param string $justCount Just return the number of users
	 * @param string $returnGroupsList Also, if appropriate, return comma separated list of groups (idst) and orgcharts (idOrg)
	 * @param string $onlyValidUsers Ignore users having core_user->valid != 1, i.e. non-active/non-valid users
	 *
	 * @return array|integer
	 */
	public function getBranchUsersPdo(
	       $includeMe          = true,
	       $includeGroups      = false,
	       $excludeGroups      = false,
	       $meOnly             = false,
	       $fullData           = false,
	       $userFields         = false,
	       $justCount          = false,
	       $returnGroupsList   = false,
	       $onlyValidUsers     = false
	    ) {

	    // In case we have a custom list of user fields to select
        $userFieldsSelectString = false;
	    if (is_array($userFields) && !empty($userFields)) {
	        // core_user->idst is mandatory and MUST be part of the fields list!
	        if (!in_array("idst", $userFields)) {
	            $userFields[] = "idst";
	        }
	        foreach ($userFields as $index => $value) {
	            $userFields[$index] = "user." . $value;
	        }
	        $userFieldsSelectString = implode(", ", $userFields);
	    }

	    // SQL parameters
	    $params = array(
	        ':left'    => $this->iLeft,
	        ':right'   => $this->iRight,
	    );


	    $command = Yii::app()->db->createCommand();
	    $command->from('core_group_members cgm');
	    $command->join('core_user user', 'user.idst=cgm.idstMember');

	    if ($returnGroupsList) {
	        $command->join('core_org_chart_tree tree2', 'cgm.idst=tree2.idst_oc');
	    }

        // Left/Right comparision variations, depending on function parameters
	    $operatorLeft  = " > ";
	    $operatorRight = " < ";
	    if ($meOnly) {
	        $operatorLeft  = " = ";
	        $operatorRight = " = ";
	    }
	    else if ($includeMe) {
	        $operatorLeft  = " >= ";
	        $operatorRight = " <= ";
	    }
	    else {
	        $operatorLeft  = " > ";
	        $operatorRight = " < ";
	    }
	    $command->join("(select tree.idst_oc from core_org_chart_tree tree where (iLeft $operatorLeft :left) and (iRight $operatorRight :right)) t1", "t1.idst_oc=cgm.idst");


	    // Groups restriction?
	    if (is_array($includeGroups) && !empty($includeGroups)) {
	        $command->andWhere(array("IN", "cgm.idst", $includeGroups));
	    }
	    if (is_array($excludeGroups) && !empty($excludeGroups)) {
	        $command->andWhere(array("NOT IN", "cgm.idst", $excludeGroups));
	    }

        // Valid users only?
	    if ($onlyValidUsers) {
	        $command->andWhere('user.valid=1');
	    }

	    // Just do a count ?
	    if ($justCount) {
	        $command->select('COUNT(DISTINCT user.idst) AS number');
	        return $command->queryScalar($params);
	    }
	    // Or... not ?
	    else {
	        $command->group('cgm.idstMember');

	        // Return more data about users ?
	        if ($fullData) {
	            $selectUserFields = "user.*";
	            if (is_string($userFieldsSelectString)) {
	                $selectUserFields = $userFieldsSelectString;
	            }
	            if ($returnGroupsList) {
	               $command->select("$selectUserFields, GROUP_CONCAT(cgm.idst) AS agreg_groups_list, GROUP_CONCAT(tree2.idOrg) AS agreg_orgcharts");
	            }
	            else {
	                $command->select($selectUserFields);
	            }
	            $rows = $command->queryAll(true, $params);
	            $result = array();
	            foreach ($rows as $userRow) {
	                $result[strval($userRow['idst'])] = $userRow;
	            }
	            return $result;
	        }
	        // Or... ?
	        else {
	            if ($returnGroupsList) {
	               $command->select('user.idst, GROUP_CONCAT(cgm.idst) AS agreg_groups_list, GROUP_CONCAT(tree2.idOrg) AS agreg_orgcharts');
	               $rows = $command->queryAll(true, $params);
	               $result = array();
	               foreach ($rows as $userRow) {
	                   $result[$userRow['idst']] = $userRow;
	               }
	               return $result;

	            }
	            else {
	                $command->select('cgm.idstMember');
	                return $command->queryColumn($params);
	            }
	        }
	    }

	    return array();

	}



	/**
	 * Get list of Users (IDs), members of the "oc_" group of THIS node, i.e. the node itself ONLY, NO descendants accounted
	 *
	 * @param array|boolean $includeGroups List of group IDs to filter by
	 * @param array|boolean $excludeGroups List of groups to exclude from the process
	 * @return array
	 */
	public function getNodeUsersOLD($includeGroups=false, $excludeGroups=false) {
		$userIds = array();

		if (is_array($includeGroups) && (!in_array($this->idst_oc, $includeGroups))) {
			return array();
		}

		$groupModel = CoreGroup::model()->findByPk($this->idst_oc);
		if ($groupModel)
			foreach ($groupModel->users as $user)
				$userIds[] = $user->idst;

		return $userIds;
	}


	/**
	 * Get list of Users (IDs), members of the "oc_" group of THIS node, i.e. the node itself ONLY, NO descendants accounted
	 *
	 * @param array|boolean $includeGroups List of group IDs to filter by
	 * @param array|boolean $excludeGroups List of groups to exclude from the process
	 * @param bool $filterAdmin
	 * @param bool $excludeSuspended
	 * @return array
	 */
	public function getNodeUsers($includeGroups=false, $excludeGroups=false, $filterAdmin=false, $excludeSuspended=false) {

		$output = array();

		// If we are filtering by group ID(s)
		if (is_array($includeGroups) && (!in_array($this->idst_oc, $includeGroups))) {
			return $output;
		}

		$command = Yii::app()->db->createCommand();

		$command
			->select('idstMember')
			->from('core_group_members cgm')
			->where('cgm.idst=:idGroup');


		// Power User filtering
		if ($filterAdmin && Yii::app()->user->getIsPu()) {
			$command->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = " . (int) Yii::app()->user->id . " AND cgm.idstMember = pu.user_id");
		}

		if($excludeSuspended) {
			$command->join(CoreUser::model()->tableName()." u", "u.idst=cgm.idstMember");
			$command->andWhere('u.valid=1');
		}

		$command->params[':idGroup'] = $this->idst_oc;

		$rows = $command->queryAll(true);
		$output = array();
		foreach ($rows as $row) {
			$output[] = $row['idstMember'];
		}

		return $output;
	}



	/**
	 * Returns an array of "paths" to all branch's nodes, starting from $branchRootId or from "root", if $branchRootId is null.
	 *
	 * The array Key can be "idOrg" (primary key of Org Chart Tree table) or "idst_oc" (the group associated to the node, which should be unique [is it ?])
	 * This is to provide two different ways to associate node to its 'path'.
	 *
	 * Example:  array(
	 * 		12380 => 'Org Chart Root',
	 * 		12392 => 'Org Chart Root/Branch 1',
	 * 		12396 => 'Org Chart Root/Branch 1/Branch 1-1',
	 * 		12401 => 'Org Chart Root/Branch 2',
	 * 		12407 => 'Org Chart Root/Branch 2/Branch 2-1',
	 * 		12409 => 'Org Chart Root/Branch 2/Branch 2-2',
	 * 		12411 => 'Org Chart Root/Branch 2/Branch 2-2/Branch 2-3',
	 * )
	 *
	 *
	 * @param string $keyField  Please specify 'idOrg' (or just null) or 'idst_oc'; anthing else will lead to 'idOrg' again
	 * @param number $branchRootId  If specified, the branch will be traversed starting from this idOrg node; otherwise traversing will start from ROOT
	 * @param boolean $onlyChildren  Exclude the branch root from list of paths, i.e. get ONLY children paths
	 * @param string $separator   Default is '>'
	 */
	public static function getBranchNodesPaths($branchRootId = NULL, $keyField = NULL,  $onlyChildren = FALSE, $separator = ">", $excludeRoot = FALSE) {

		// Cache results to avoid overloading
		static $cache = array();
		$cacheKey = implode('_', func_get_args());
		if(isset($cache[$cacheKey])){
			return $cache[$cacheKey];
		}

		if ($branchRootId != NULL) {
			$startNode = self::model()->findByPk($branchRootId);
		}
		else {
			$startNode = self::getOrgRootNode();

		}

		if ($onlyChildren)
			$nodes = $startNode->descendants()->findAll();
		else
			$nodes = array_merge(array($startNode),$startNode->descendants()->findAll());

		$paths = array();
		foreach ($nodes as $node) {
			$path = $node->getNodePath($separator, $excludeRoot);
			if ( ($keyField == 'idOrg') || (!$keyField) || (!in_array($keyField, array('idOrg', 'idst_oc')))) {
				$paths[$node->idOrg] = $path;
			}
			else {
				$paths[$node->idst_oc] = $path;
			}
		}

		$cache[$cacheKey] = $paths;

		return $paths;


	}


	/**
	 * Return list of Node Ids (or list of node models, optional) that given list of users are assigned to (any user from the list)
	 *
	 * @param array $users  Array of user IDs
	 * @param boolean $returnModels Return array of models if TRUE
	 *
	 * @return array
	 */
	public static function getNodesHavingUsers($users, $returnModels = false) {

		$output = array();

		// Check through OC groups (one join). Exclude  /oc_0 and /ocd_0
		$command = Yii::app()->db->createCommand();
		$command
			->from('core_org_chart_tree tree')
			->join('core_group g', 'g.idst=tree.idst_oc')
			->join('core_group_members cgm', 'tree.idst_oc=cgm.idst')
			->where('cgm.idstMember IN (' . implode(',',$users) . ')')
			->andWhere("g.groupid NOT IN ('/oc_0','/ocd_0')");
		$result_oc = $command->queryAll(true);
		foreach ($result_oc as $record) {
			$output[] = $record['idOrg'];
		}

		// Check through OCD groups (double joining core group members). Exclude  /oc_0 and /ocd_0
		$command = Yii::app()->db->createCommand();
		$command
			->select('tree.idOrg as idOrg, cgm1.idst idst_ocd, cgm2.idst user_group, cgm2.idstMember user')
			->from('core_org_chart_tree tree')
			->join('core_group g', 'g.idst=tree.idst_oc')
			->join('core_group_members cgm1', 'tree.idst_ocd=cgm1.idst')
			->join('core_group_members cgm2', 'cgm1.idstMember=cgm2.idst')
			->where('(cgm2.idstMember IN ('.implode(',',$users).'))')
			->andWhere("g.groupid NOT IN ('/oc_0','/ocd_0')");
		$result_ocd = $command->queryAll(true);

		foreach ($result_ocd as $record) {
			$output[] = $record['idOrg'];
		}

		$output = array_unique($output);

		// Maybe caller needs chart node models??
		if ($returnModels) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idOrg', $output);
			$output = self::model()->findAll($criteria);
		}

		return $output;

	}





	/**
	 * Given a list of models, build an array of Fancytree-compatible data, using iLeft / iRight ONLY!
	 *
	 * @param array $nodeList Array of CoreOrgChartTree models. Must be sorted by iLeft!!!
	 * @return array
	 */
	public static function buildFancytreeDataArray($indents = FALSE, $adminFilter = FALSE, $includeCurrentChild = FALSE, $expandRoot=true, $checkBranchIds = FALSE) {

		/**
		 * Recursively called function
		 *
		 * @param array      $sourceData
		 * @param int|number $left
		 * @param string     $right
		 *
		 * @return array
		 */

		$puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();


		function createTree($sourceData, $left = 0, $right = null, $lev = null)  {

			$tree = array();
			foreach ($sourceData as $idOrg => $item) {

				// The list of nodes is sorted by iLeft. We add nodes to tree not only if it is
				// an immediate child, but if it is just a descendant of the calling node.
				// This is to handle the scenario when intermediate nodes are NOT available.
				// This must be heavily tested, please
				$isDescendant = ($item['iLeft'] > $left) && (is_null($right) || $item['iRight'] < $right);
				if($lev && $isDescendant){
					$isDescendant =  ($item['lev'] == ($lev+1));
				}

				if ($isDescendant && !$item['skip']) {

					$left = $item['iRight'];

					$element = array();
					$element['folder'] = false;
					$element['title'] = $item['title'];
					$element['key'] = $item['key'];
					$element['data'] = $item['data'];
					$element['expanded'] = $item['expanded'];
					$element['hideCheckbox'] = (isset($item['unselectable']) && $item['unselectable'] == true) ? true : false;
					if(Yii::app()->user->getIsPu()){
						$element['unselectable'] = $item['selectable'];
					}

					if($item['customCssClasses'])
						$element['extraClasses'] = $item['customCssClasses'];

					// Save some work for the next call, if any
					//unset($sourceData[$idOrg]);

					$children = null;
					// If this node is not a leaf, call for its children
					if ( ($item['iRight'] - $item['iLeft']) > 1 ) {
						//$element['lazy'] = true;
						$children = createTree($sourceData, $item['iLeft'], $item['iRight'], $item['lev']);
						$element['folder'] = true;
					}

					$element['children'] = $children;
					$tree[] = $element;

				}
			}
			return $tree;
		}

		// We do caching; set proper key to ensure uniqueness across different calls and environment.
		// Using specific application level cache:  cache_orgchart, so we can clean it upon org chart changes.
		if (isset(Yii::app()->cache_orgchart)) {
			$cacheKeyActors = array(
				Yii::app()->db->connectionString,
				Yii::app()->getLanguage(),
				Yii::app()->user->getIsGodadmin(),
				Yii::app()->user->getIsAdmin(),
				Yii::app()->user->getIsUser(),
				isset(Yii::app()->user->id) ? Yii::app()->user->id : 0,
			);
			$cacheKeyActors = array_merge($cacheKeyActors, func_get_args());

			$cacheKey = md5(json_encode($cacheKeyActors));
			$tree = Yii::app()->cache_orgchart->get($cacheKey);
			if ($tree)
				return $tree;
		}

		$nodeList = self::getFullOrgChartTree($indents, $adminFilter, $includeCurrentChild);

		// Get the root node
		$rootNode = $expandRoot ? self::getOrgRootNode() : false;

		// Transform orgchart model based array into array of arrays, no models
		$sourceData = array();
		foreach ($nodeList['list'] as $index => $node) {
			//$currentLangTranslation = $node->coreOrgChart->translation;
			$item = array(
					'iLeft'			=> $node->iLeft,
					'iRight'		=> $node->iRight,
					//'title' 		=> $currentLangTranslation ? $currentLangTranslation : $node->coreOrgChartTranslatedDefaultLang->translation,
					'title' 		=> $node->coreOrgChart->translation,
					'key'			=> $node->idOrg,
					'data'			=> $node->attributes,
					'lev'           => $node->lev,
					'expanded'		=> ($rootNode != false) && ($rootNode->idOrg == $node->idOrg),
					'folder'		=> false,
					'children'		=> null,
			);

			if ($adminFilter && Yii::app()->user->getIsPu() && $checkBranchIds) {
				$item['selectable'] = in_array($node->idOrg, $puBranchIdsArray) ? false : true;
			} else {
				$item['selectable'] = false;
			}
			$sourceData[$node->idOrg] = $item;
		}

		// Let plugins process the source data, before creating the tree
		Yii::app()->event->raise('BeforeCreateOrgchartTree', new DEvent(self, array('sourceData' => &$sourceData, 'leafs' => $nodeList['leafs'])));

		$tree  = createTree($sourceData);

		// Let plugins process the tree structure
		Yii::app()->event->raise('AfterCreateOrgchartTree', new DEvent(self, array('tree' => &$tree)));

		// Cache the result
		if (isset(Yii::app()->cache_orgchart)) {
			Yii::app()->cache_orgchart->set($cacheKey, $tree);
		}

		return $tree;

	}


	/**
	 * Build Fancytree-compatible array, implementing Lazy Loading capabilities
	 *
	 * @param string $indents
	 * @param bool $adminFilter  Filter by PowerUser ?
	 * @param bool $expandRoot Expand ROOT node on loading?
	 * @param bool $lazyMode Apply lazy folder loading (children loaded on click on folder)
	 * @param integer $startingNodeId Where to start from in the tree
	 * @param bool $includeStartingNode Show that starting node ?
	 * @return array
	 */
	public static function buildFancytreeDataArrayV2(
			$indents 				= FALSE,
			$adminFilter 			= FALSE,
			$expandRoot				= TRUE,
			$lazyMode				= FALSE,
			$startingNodeId 		= FALSE,
			$includeStartingNode	= TRUE) {

		/**
		 * Recursively called function to build the actual fancytree-array
		 *
		 * @param array      $sourceData Flat List of nodes having left/right info
		 * @param int $left Caller iLeft
		 * @param string     $right Caller iRight
		 * @param            $lazyMode
		 * @param            $rootNode
		 *
		 * @return array
		 */
		function createTree($sourceData, $left = 0, $right = null, $lazyMode, $rootNode)  {

			$tree = array();
			foreach ($sourceData as $idOrg => $item) {

				// The list of nodes is sorted by iLeft. We add nodes to tree not only if it is
				// an immediate child, but if it is just a descendant of the calling node.
				// This is to handle the scenario when intermediate nodes are NOT available.
				// This must be heavily tested, please
				$isDescendant = ($item['iLeft'] > $left) && (is_null($right) || $item['iRight'] < $right);

				if ($isDescendant && !$item['skip']) {

					$left = $item['iRight'];

					$element = array();
					$element['folder'] = false;
					$element['title'] = $item['title'];
					$element['key'] = $item['key'];
					$element['data'] = $item['data'];
					$element['expanded'] = $item['expanded'];
					if($item['customCssClasses'])
						$element['extraClasses'] = $item['customCssClasses'];

					// Save some work for the next call, if any
					//unset($sourceData[$idOrg]);

					$children = null;
					$element['folder'] = false;

					// If this node is not a leaf, call for its children
					if ( ($item['iRight'] - $item['iLeft']) > 1 ) {
						if ($lazyMode && !($item['expanded'])) {
							$element['lazy'] = true;
						}
						else {
							$children = createTree($sourceData, $item['iLeft'], $item['iRight'], $lazyMode, $rootNode);
						}
						$element['folder'] = true;
					}

					$element['children'] = $children;
					$tree[] = $element;

				}
			}
			return $tree;
		}

		// We do caching; set proper key to ensure uniqueness across different calls and environment.
		// Using specific application level cache:  cache_orgchart, so we can clean it upon org chart changes.
		if (isset(Yii::app()->cache_orgchart) && !$lazyMode) {
			$cacheKeyActors = array(
					Yii::app()->db->connectionString,
					Yii::app()->getLanguage(),
					Yii::app()->user->getIsGodadmin(),
					Yii::app()->user->getIsAdmin(),
					Yii::app()->user->getIsUser(),
					isset(Yii::app()->user->id) ? Yii::app()->user->id : 0,
			);
			$cacheKeyActors = array_merge($cacheKeyActors, func_get_args());

			$cacheKey = md5(json_encode($cacheKeyActors));
			$tree = Yii::app()->cache_orgchart->get($cacheKey);
			if ($tree) {
				return $tree;
			}
		}

		// Get root Node, we may need it later
		$rootNode = self::getOrgRootNode();

		// In LazyMode, if we are requested to build sub-tree data, take into account *immediate* children only
		$immediateChildrenOnly = $lazyMode;
		$nodeList = self::getFullOrgChartTreeV2($indents, $adminFilter, $startingNodeId, $immediateChildrenOnly, $includeStartingNode);

		// Transform orgchart model based array into array of arrays, no models
		$sourceData = array();
		foreach ($nodeList['list'] as $index => $node) {
			//$currentLangTranslation = $node->coreOrgChart->translation;
			$item = array(
					'iLeft'			=> $node->iLeft,
					'iRight'		=> $node->iRight,
					'title' 		=> $node->coreOrgChart->translation,
					'key'			=> $node->idOrg,
					'data'			=> $node->attributes,
					'expanded'		=> ($expandRoot != false) && ($rootNode->idOrg == $node->idOrg),
					'folder'		=> false,
					'children'		=> null,
			);
			$sourceData[$node->idOrg] = $item;
		}

		// Let plugins process the source data, before creating the tree
		Yii::app()->event->raise('BeforeCreateOrgchartTree', new DEvent(self, array('sourceData' => &$sourceData, 'leafs' => $nodeList['leafs'])));

		$tree  = createTree($sourceData, 0, null, $lazyMode, $rootNode);

		// Let plugins process the tree structure
		Yii::app()->event->raise('AfterCreateOrgchartTree', new DEvent(self, array('tree' => &$tree)));

		// Cache the result
		if (isset(Yii::app()->cache_orgchart) && !$lazyMode) {
			Yii::app()->cache_orgchart->set($cacheKey, $tree);
		}

		return $tree;


	}




	/**
	 * Return Org Chart PATH to a given node, represented by idst_OC GROUP ID
	 *
	 * @param integer $idGroup  Group ID (idst_oc) of the node
	 * @param string $separator
	 * @param string $excludeRoot
	 * @return string
	 */
	public static function getOcGroupPath($idGroup, $separator = ' > ', $excludeRoot=true) {

		// Do cache
		static $cache = array();
		$cacheKey = sha1(serialize(func_get_args()));
		if(isset($cache[$cacheKey])){
			return $cache[$cacheKey];
		}

		$nodeModel = self::model()->findByAttributes(array('idst_oc' => $idGroup));
		if (!$nodeModel) {
			return "";
		}

		$path = $nodeModel->getNodePath($separator, $excludeRoot);

		$cache[$cacheKey] = $path;
		return $path;

	}

	/**
	 * Retrieve the organization chart node name translation, specifying the language
	 *
	 * @param bool|string $language the language code of the requested translation (false = current|default lms language)
	 * @return bool|string the translation of the orgchart node, false if no translation is retrievable
	 */
	public function getTranslation($language = false) {
		if (empty($language)) {
			$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage()); //default language is current language
			if (empty($language)) { $language = Settings::get('default_language'); } //if no current language is set, fallback to lms default language
		}
		$activeLanguages = CoreLangLanguage::getActiveLanguages();
		if (!isset($activeLanguages[$language])) return false; //make sure the requested language is valid
		$ar = CoreOrgChart::model()->findByPk(array('id_dir' => $this->getPrimaryKey(), 'lang_code' => $language));
		if (!$ar) return false;
		return $ar->translation;
	}

	/**
	 * Set/update the organization chart node name translation, specifying the language
	 *
	 * @param bool|string $language the language code of the requested translation (false = current|default lms language)
	 * @param string $translation the translation string
	 * @return bool|string the translation of the orgchart node, false if no translation is retrievable
	 */
	public function setTranslation($language = false, $translation) {
		if (empty($language)) {
			$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage()); //default language is current language
			if (empty($language)) { $language = Settings::get('default_language'); } //if no current language is set, fallback to lms default language
		}
		$activeLanguages = CoreLangLanguage::getActiveLanguages();
		if (!isset($activeLanguages[$language])) return false; //make sure the requested language is valid
		$ar = CoreOrgChart::model()->findByPk(array('id_dir' => $this->getPrimaryKey(), 'lang_code' => $language));
		if (!$ar) return false;
		$ar->translation = $translation;
		return $ar->save();
	}

	/**
	 * Return Org Chart PATHs to list of nodese, represented by their idst_OC GROUP IDs
	 *
	 * @param string $list  e.g.   "1,4,77,88,99,.."
	 * @param string $separator
	 * @param string $excludeRoot
	 * @return string e.g.    "Node name 1 >  Node name 2,  Node name 3 > Node name 4
	 */
	public static function getOcGroupsPaths($list, $separator = ' > ', $excludeRoot=true, $nodesSeparator = ', ') {

		static $cache = array();
		$cacheKey = sha1(serialize(func_get_args()));
		if(isset($cache[$cacheKey])){
			return $cache[$cacheKey];
}


		$list = explode(',', $list);
		if($list && is_array($list)){
			$list = array_unique($list);
		}


		if (!is_array($list) || count($list) <= 0) {
			return "";
		}

		$result = array();
		foreach ($list as $idGroup) {
			$groupPath = self::getOcGroupPath($idGroup, $separator, $excludeRoot);
			if (!empty($groupPath)) {
				$result[] = $groupPath;
			}
		}

		natcasesort($result);
		$result = implode($nodesSeparator, $result);

		$cache[$cacheKey] = $result;

		return $result;
	}




	/**
	 * Return list of idOrg's of nodes which are immediate children of the given node (by idOrg).
	 * Using iLeft/iRight only. It is still time consuming!
	 *
	 * @return array
	 */
	public function getImmediateChildrenNative($includeMe=true) {
		$sql =
		"SELECT tree.idOrg, (COUNT(parent.idOrg) - (sub_tree.depth + 1)) AS depth
		FROM
			core_org_chart_tree AS tree,
    		core_org_chart_tree AS parent,
			core_org_chart_tree AS sub_parent,
			(
				SELECT tree.idOrg, (COUNT(parent.idOrg) - 1) AS depth
					FROM
						core_org_chart_tree AS tree,
	            		core_org_chart_tree AS parent

			        WHERE
						(tree.iLeft BETWEEN parent.iLeft AND parent.iRight)
						AND
						tree.idOrg = " . (int) $this->idOrg . "
		        	GROUP BY
		        		tree.idOrg
			) as sub_tree

		WHERE
			tree.iLeft BETWEEN parent.iLeft AND parent.iRight
        	AND
			tree.iLeft BETWEEN sub_parent.iLeft AND sub_parent.iRight
        	AND
			sub_parent.idOrg = sub_tree.idOrg

		GROUP BY tree.idOrg
		HAVING depth <= 1
		";

		$children = array();
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		foreach ($rows as $row) {
			if ((int) $row['idOrg'] == $this->idOrg && !$includeMe)
				continue;
			$children[] = (int) $row['idOrg'];
		}
		return $children;
	}




	/**
	 * Return list of idOrg's of nodes which are immediate children of the given node (by idOrg).
	 * It uses "level" table field, so it MUST be populated!
	 *
	 * @return array
	 */
	public function getImmediateChildren() {
		$sql =
		"
		SELECT idOrg
		FROM
			core_org_chart_tree tree
		WHERE
			(tree.lev = " . (int) ($this->lev+1) . ")
			AND
			(tree.iLeft BETWEEN " . $this->iLeft . " AND " . $this->iRight .")
		";
		$children = array();
		$rows = Yii::app()->db->createCommand($sql)->queryAll();
		foreach ($rows as $row) {
			$children[] = $row['idOrg'];
		}
		return $children;
	}

	public function updateEnrollmentRulesBranches() {
		$ancestors = $this->ancestors(1)->findAll();
		foreach($ancestors as $ancestor){
			$enrollmentRuleItems = CoreEnrollRuleItem::model()->findAllByAttributes(array('item_id'=>$ancestor->idOrg, 'item_type'=>CoreEnrollRuleItem::ITEM_TYPE_BRANCH));
			$descendatns = $ancestor->descendants(1)->findAll();
			$descendantsIdsArray = array();
			foreach($descendatns as $descendant){
				$descendantsIdsArray[] = $descendant->idOrg;
			}

			if($enrollmentRuleItems){
				foreach($enrollmentRuleItems as $id => $item){
					$enrollmentRule = CoreEnrollRule::model()->findByPk($item->rule_id);
					if($enrollmentRule){
						$ruleModel = CoreEnrollRuleItem::model()->findByAttributes(array('rule_id'=>$item->rule_id, 'item_type'=> CoreEnrollRuleItem::ITEM_TYPE_BRANCH, 'item_id'=>$this->idOrg ));
						$criteria = new CDbCriteria;
						$criteria->addCondition('t.rule_id = '.$item->rule_id.' AND t.item_type ="'.CoreEnrollRuleItem::ITEM_TYPE_BRANCH.'"');
						$criteria->addInCondition('t.item_id' , $descendantsIdsArray);
						$rulesModels = CoreEnrollRuleItem::model()->findAll($criteria);

						$rulesModelsCount = count($rulesModels);
						$descendantsCount = count($descendatns);

						if(!$ruleModel && (($rulesModelsCount == ($descendantsCount-1)) || $descendantsCount == 1)  ){
							$model = new CoreEnrollRuleItem();
							$model->rule_id = $item->rule_id;
							$model->item_type = CoreEnrollRuleItem::ITEM_TYPE_BRANCH;
							$model->item_id = $this->idOrg;
							$model->save(false);
						}
					}
				}
			}
		}
	}

	/**
	 * Returns an array of idOrgs that the user belongs to (descendants branches NOT accounted, only direct membership!)
	 *
	 * @param $idUser
	 * @return array
	 */
	public static function getOrgChartListByUserId($idUser) {
		if ( ! is_array($idUser) ) {
			$idUser = array( $idUser );
		}
		$output = array();

		$query = "
            SELECT o.idOrg
            FROM core_group_members AS gm
            JOIN core_group AS g ON (gm.idst = g.idst AND g.groupid LIKE '/oc\_%')
            JOIN core_org_chart_tree AS o ON (gm.idst = o.idst_oc)
            WHERE gm.idstMember IN (". implode(',', $idUser) .")";

		$rows = self::model()->findAllBySql($query);


		if ($rows) {
			foreach ($rows as $row) {
				$output[] = $row->idOrg;
			}
		}

		return $output;
	}




	/**
	 * Return a model by its name, taking into account the language (optional)
	 *
	 * @param string $language  Language code, e.g. "english", "italian"
	 */
	public static function getBranchByName($name, $langcode = false) {
		if ($langcode === false) {
			$langcode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		}
		$modelChart = CoreOrgChart::model()->findByAttributes(array('lang_code' => $langcode, 'translation' => $name));
		return $modelChart->coreOrgChartTree;
	}

	static public function getUserOCDArr($idUser){
		$ocdArr = array();
		$c2 = new CDbCriteria();
		$c2->addCondition('groupMembers.idstMember=:idstMember');
		$c2->params[':idstMember'] = $idUser;
		$c2->with = array('groupMembers');
		$c2->select = 't.lev, t.iLeft, t.iRight';
		$userGroups = CoreOrgChartTree::model()->findAll($c2);
		foreach($userGroups as $group){
			$tempCriteria = new CDbCriteria();
			$tempCriteria->addCondition('t.iLeft <= :currentLeft');
			$tempCriteria->addCondition('t.iRight >= :currentRight');
			$tempCriteria->params[':currentLeft'] = $group->iLeft;
			$tempCriteria->params[':currentRight'] = $group->iRight;
			$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);

			if($groupParent){
				foreach($groupParent as $parent){
					$ocdArr[] = $parent->idst_ocd;

					if($group->iLeft === $parent->iLeft)
						$ocdArr[] = $parent->idst_oc;

					}

				}
			}

		return array_unique($ocdArr);
	}


	public static function getPuBranchIds(){

		$res = array();
		$puOrgChartDesc = CoreAdminTree::getPowerUserMembers(Yii::app()->user->id, array( CoreAdminTree::TYPE_ORGCHART_DESC));

		foreach($puOrgChartDesc as $chart){
			if (isset($chart['id']) && is_numeric($chart['id'])) {
				/*
				$orgChartTree = CoreOrgChartTree::model()->findByPk($chart['id']);
				if($orgChartTree){
					foreach($orgChartTree->getImmediateChildrenNative(true) as $idOrg){
						$res[] = $idOrg;
					}
				}
				*/
				//NOTE: ALL descendats subnodes must be selectable by the PU, not just the first level
				if (!isset($command)) {
					$query = "SELECT tree.idOrg "
						." FROM ".self::model()->tableName()." AS tree "
						." JOIN ".self::model()->tableName()." AS parent "
						." ON (tree.iLeft >= parent.iLeft AND tree.iRight <= parent.iRight AND parent.idOrg = :id_org)";
					$command = Yii::app()->db->createCommand($query);
				}
				if ($command) {
					$reader = $command->query(array(':id_org' => $chart['id']));
					if ($reader) {
						while ($row = $reader->read()) {
							$res[] = (int)$row['idOrg'];
						}
					}
				}
			}
		}

		$puOrgChart = CoreAdminTree::getPowerUserMembers(Yii::app()->user->id, array( CoreAdminTree::TYPE_ORGCHART));
		foreach($puOrgChart as $chart ){
			if (isset($chart['id']) && is_numeric($chart['id'])) {
				$res[] = (int)$chart['id'];
			}
		}

		return array_unique($res);
	}


	/**
	 * Check if user is a member of a given branch OR (optionally) a member of ANY descendants of that branch
	 *
	 * @param unknown $idUser
	 * @param unknown $idOrg
	 * @param string $includeDescendants
	 */
	public static function isUserMemberOf($idUser, $idOrg, $includeDescendants=false) {

		// Cache during HTTP request
		static $cache = array();
		$cacheKey = implode('_', func_get_args());
		if(isset($cache[$cacheKey])){
			return $cache[$cacheKey];
		}

		// Get branch model
		$branch   = self::model()->findByPk($idOrg);

		// Make the list of Branches to check if user belongs to, because it might be extended by descendants
		$branchesToCheck = array();
		$branchesToCheck[] = $idOrg;

		// Add descendants, if requested
		if ($includeDescendants && $branch !== null) {
			$descendants = $branch->descendants()->findAll();
			if (!empty($descendants)) {
				foreach ($descendants as $desc) {
					$branchesToCheck[] = $desc->idOrg;
				}
			}
		}

		// Get list of branches user REALLY belongs to
		$userBranches = self::getOrgChartListByUserId($idUser);

		// Find the intersection between above two array: possible branches AND branches user belongs to
		// Meaning, if ANY of the user branches match any of the possible branches, we say YES!
		$result = array_intersect($branchesToCheck, $userBranches);
		$result = !empty($result);

		// Do some caching
		$cache[$cacheKey] = $result;

		return $cache[$cacheKey];
	}



	/**
	 * Give a $idUser and return the names of the branches
	 *
	 * @param any $idUser
	 * Return $groupPaths as 'Branch 1 > Branch 2, Branch 3 > Branch 4
	 */
	public static function getOrgPathByUserId($idUser = null)
	{
		if (!$idUser)
			return '';

		$id_ocs = Yii::app()->db->createCommand()
			->select('idst_oc')
			->from(CoreOrgChartTree::model()->tableName(). ' ct')
			->join(CoreGroupMembers::model()->tableName().' gm', '(gm.idst = ct.idst_ocd)')
			->where('gm.idstMember = :idstMember', array(':idstMember' => $idUser))
			->queryColumn();

		$groupPaths = CoreOrgChartTree::getOcGroupsPaths(implode(',', $id_ocs));

		return $groupPaths;
	}



	/**
	 * @param $idUser
	 * @param $source
	 * @param $target
	 * @throws CDbException
	 */
	public static function moveUserToNode($idUser, $source, $target)
	{

		if (!$idUser || empty($idUser) || !self::isUserMemberOf($idUser, $source) || self::isUserMemberOf($idUser, $target)) {
			return;
		}

		// Get Source branch model
		$sourceBranch = self::model()->findByPk($source);
		if ($sourceBranch) {
			$idst_oc_source = $sourceBranch->idst_oc;
			$idst_ocd_source = $sourceBranch->idst_ocd;
		}

		// Get Target branch model
		$targetBranch = self::model()->findByPk($target);
		if ($targetBranch) {
			$idst_oc_target = $targetBranch->idst_oc;
			$idst_ocd_target = $targetBranch->idst_ocd;
		}

		/***** INSERT OC  ****/
		$command = Yii::app()->db->createCommand();
		$command->insert(CoreGroupMembers::model()->tableName(), array(
			'idst' => $idst_oc_target,
			'idstMember' => $idUser
		));
		/****************/

		/***** INSERT OCD  ****/
		$commandD = Yii::app()->db->createCommand();
		$commandD->insert(CoreGroupMembers::model()->tableName(), array(
			'idst' => $idst_ocd_target,
			'idstMember' => $idUser
		));
		/****************/

		/***** DELETE OC****/
		CoreGroupMembers::model()->deleteAllByAttributes(array(
			'idst' => $idst_oc_source,
			'idstMember' => $idUser
		));

		/***** DELETE OCD****/
		CoreGroupMembers::model()->deleteAllByAttributes(array(
			'idst' => $idst_ocd_source,
			'idstMember' => $idUser
		));
	}



	/**
	 * Get idOrg of the node by the OC or the OCD
	 * @param $idst_oc
	 * @param $idst_ocd Please do not use it!
	 * @return $result (-1 if no params)
	 */
	public static function getIdOrgByOcOrOcd($idst_oc = false, $idst_ocd = false)
	{
	    $result = Yii::app()->db->createCommand()
	       ->select("tree.idOrg")
	       ->from("core_org_chart_tree tree")
	       ->where("(tree.idst_oc=:oc) OR (tree.idst_ocd=:oc)")
	       ->queryScalar(array(':oc' => (int) $idst_oc));

	    if ($result===false) {$result = -1;}
		return $result;
	}



	/**
	 * Get name of the node by the idOrg
	 * @param $idOrg
	 * @param $language
	 * @return string
	 */
	public static function getNodeNameByIdOrg($nodeIdOrg, $language = false)
	{
	    return self::getTranslatedName($nodeIdOrg, $language);
	}



	/**
	 * Return Node name, translated in: requested language OR LMS deafult language OR a fallback language.
	 * Yes, it looks somewhat complex, but it is resolving the translation in 3 attempts (single SQL though!).
	 *
	 * Using Static Cache!
	 *
	 * @param string $language
	 */
	public static function getTranslatedName($idOrg, $language=false, $fallbackLanguage="english") {

	    static $cache=array();
	    $cacheKey = md5(json_encode(func_get_args()));

	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }

	    // If no language is specified, get the current request language
	    if (!$language) {
	        $language = Yii::app()->getLanguage();
	    }

	    // Transform it into code (en => english)
	    $language = Lang::getCodeByBrowserCode($language);

	    // Prepare the LMS default language as well
	    $defLanguage = Settings::get('default_language', 'english');

	    $params = array(
	       ":idOrg"           => (int) $idOrg,
	       ":wantedLang"      => $language,
	       ":lmsDefLang"      => $defLanguage,
	       ":fallbackLang"    => $fallbackLanguage,
	    );

	    $joinCondition = "
	       (tree.idOrg=coc.id_dir and coc.lang_code=:wantedLang) OR
	       (tree.idOrg=coc.id_dir and coc.lang_code=:lmsDefLang) OR
	       (tree.idOrg=coc.id_dir and coc.lang_code=:fallbackLang)
	    ";

	    // In case we can't find any translation
	    $absoluteFallback = 'node-' . $idOrg;

	    $command = Yii::app()->db->createCommand()
	       ->select("lang_code, translation")
	       ->from("core_org_chart_tree tree")
	       ->join("core_org_chart coc", $joinCondition)
	       ->andWhere("tree.idOrg=:idOrg");

	    $rows = $command->queryAll(true, $params);

	    if (!$rows || !is_array($rows)) {
	        return $absoluteFallback;
	    }

	    $tmp = array();
	    foreach ($rows as $translations) {
	        $tmp[$translations['lang_code']] = $translations['translation'];
	    }

	    // Get the translation, in proper priority
	    if (isset($tmp[$language]) && !empty($tmp[$language]))                 $result = $tmp[$language];
	    else if (isset($tmp[$defLanguage]) && !empty($tmp[$defLanguage]))           $result = $tmp[$defLanguage];
	    else if (isset($tmp[$fallbackLanguage]) && !empty($tmp[$fallbackLanguage])) $result = $tmp[$fallbackLanguage];
	    else $result=$absoluteFallback;


	    $cache[$cacheKey] = $result;
	    return $result;

	}



    /**
     * Return an array of <idOrg> => <idParent> map (both integers, except when idParent is NULL)
     * The node having idParent = null is the ROOT one.
     *
     * This is SQL based function made for speed, for possible mass operations or data collection.
     * If you need just the parent of single node, use $this->parent() method (NestedBehavior method, returning a model).
     *
     * Checkout this: http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
     *
     * @param string $idParent Filter by specific idOrg of a parent
     */
    public static function getNodeToParentMap($idParent=false) {

        $sql = "
	    SELECT t1.idOrg,
	        (SELECT t2.idOrg
             FROM core_org_chart_tree t2
             WHERE t2.iLeft < t1.iLeft AND t2.iRight > t1.iRight
             ORDER BY t2.iRight-t1.iRight ASC
             LIMIT 1) AS idParent

        FROM core_org_chart_tree t1

        %%HAVING%%

        ORDER BY t1.iRight - t1.iLeft DESC
        ";

        $replace = "";
        if ($idParent !== false) {
            $replace = " HAVING idParent=" . (int) $idParent . " ";

        }
        $sql = str_replace('%%HAVING%%', $replace, $sql);

        $command = Yii::app()->db->createCommand($sql);

        $result = $command->queryAll(true);

        foreach ($result as $pair) {
            $map [$pair['idOrg']] = $pair['idParent'];
        }

        return $map;

    }


    /**
     * Check if a given user or users is a member of a NODE (or nodes) or a BRANCH (or branches)
     *
     * NODE - single organizational node
     * BRANCH - a NODE and its descendants
     *
     * USING "user_org_assignment" VIEW !! Do not use this method to extract ALL data if organizational structure of the LMS is huge!!!
     *
     * @param array|integer $idUsers
     * @param array|integer $idOrgs
     * @param boolean $orInDescendants
     * @return boolean
     */
    private function isUserInNodeInternal($idUsers, $idOrgs, $orInDescendants=false) {

    	// Make sure input parameters are array
    	if ( !is_array($idUsers) ) { $idUsers = array((int) $idUsers); }
    	if ( empty($idUsers) ) { return false; }

    	if ( !is_array($idOrgs) ) { $idOrgs = array((int) $idOrgs); }
    	if ( empty($idOrgs) ) { return false; }

    	// Use the view
    	$command = Yii::app()->db->createCommand();
    	$command->distinct = true;
    	$command->select("idUser");
    	$command->from("user_org_assignment v");
    	$command->andWhere(array("IN", "v.idUser", $idUsers));
    	$command->andWhere("v.isUserGroup=0");
    	$command->limit(1);

    	$idOrgsList = implode(",", $idOrgs);
    	if ($orInDescendants) {
    		$command->andWhere("(v.idOrgDirectMemberOf IN ($idOrgsList)) OR (v.idOrgDirectMemberOrInChildOf IN ($idOrgsList))");
    	}
    	else {
    		$command->andWhere("v.idOrgDirectMemberOf IN ($idOrgsList)");
    	}

    	$result  = ($command->queryScalar() !== false);

    	return $result;

    }

    /**
     * Check if user (or users) is a direct member of a very particular NODE (or nodes)
     *
     * @param array|integer $idUsers
     * @param array|integer $idOrgs
     * @return bool
     */
    public static function isUserInNode($idUsers, $idOrgs) {
		return self::model()->isUserInNodeInternal($idUsers, $idOrgs, false);
    }

    /**
     * Check if user (or users) is a member of a BRANCH or branches (i.e. a node and all its descendants)
     *
     * @param array|integer $idUsers
     * @param array|integer $idOrgs
     * @return bool
     */
    public static function isUserInBranch($idUsers, $idOrgs) {
    	return self::model()->isUserInNodeInternal($idUsers, $idOrgs, true);
    }




}
