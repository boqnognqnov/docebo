<?php

/**
 * This is the model class for table "conference_onsync_meeting".
 *
 * The followings are the available columns in table 'conference_onsync_meeting':
 * @property integer $id
 * @property integer $id_room
 * @property string $name
 * @property string $session_id
 * @property string $friendly_id
 * @property string $logout_url
 * @property integer $max_participants
 */
class ConferenceOnsyncMeeting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceOnsyncMeeting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_onsync_meeting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_room, name', 'required'),
			array('id_room, max_participants', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('logout_url', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_room, name, logout_url, max_participants', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'conferenceRoom' => array(self::BELONGS_TO, 'ConferenceRoom', 'id_room'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_room' => 'Id Room',
			'name' => 'Name',
			'logout_url' => 'Logout Url',
			'max_participants' => 'Max Participants',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_room',$this->id_room);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('logout_url',$this->logout_url,true);
		$criteria->compare('max_participants',$this->max_participants);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
}