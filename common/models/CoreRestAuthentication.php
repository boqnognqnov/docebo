<?php

/**
 * This is the model class for table "core_rest_authentication".
 *
 * The followings are the available columns in table 'core_rest_authentication':
 * @property integer $id_user
 * @property integer $user_level
 * @property string $token
 * @property string $generation_date
 * @property string $last_enter_date
 * @property string $expiry_date
 */
class CoreRestAuthentication extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_rest_authentication';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, user_level', 'numerical', 'integerOnly'=>true),
			array('token', 'length', 'max'=>255),
			array('generation_date, last_enter_date, expiry_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_user, user_level, token, generation_date, last_enter_date, expiry_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function behaviors()
	{
		return array(
			// NOTE: This model need to work in utc only, don't use the timezones !
			/*'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('generation_date medium', 'last_enter_date medium', 'expiry_date short'),
				'dateAttributes' => array()
			)*/
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'user_level' => 'User Level',
			'token' => 'Token',
			'generation_date' => 'Generation Date',
			'last_enter_date' => 'Last Enter Date',
			'expiry_date' => 'Expiry Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('generation_date',Yii::app()->localtime->fromLocalDateTime($this->generation_date),true);
		$criteria->compare('last_enter_date',Yii::app()->localtime->fromLocalDateTime($this->last_enter_date),true);
		$criteria->compare('expiry_date',Yii::app()->localtime->fromLocalDateTime($this->expiry_date),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreRestAuthentication the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
