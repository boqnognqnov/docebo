<?php

/**
 * This is the model class for table "learning_quest_category".
 *
 * The followings are the available columns in table 'learning_quest_category':
 * @property integer $idCategory
 * @property string $name
 * @property string $textof
 * @property integer $author
 */
class LearningQuestCategory extends CActiveRecord
{
	
	public $search_input = "";
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningQuestCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_quest_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('search_input', 'safe'),
			array('name, textof', 'safe'),
			array('name', 'required'),
			array('author', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCategory, name, textof, author', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'testsCount' => 	array(self::STAT, 'LearningTestquest', 'idCategory'),
			'pollsCount' => 	array(self::STAT, 'LearningPollquest', 'id_category'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCategory' => 'ID',
			'name' => Yii::t('standard', '_NAME'),
			'textof' => Yii::t('standard', '_DESCRIPTION'),
			'author' => Yii::t('standard', '_AUTHOR'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if ($this->search_input) {
			$criteria->addCondition('
					CONCAT(name, textof)
					LIKE :search_input
			');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize' => Settings::get('elements_per_page'),
			),
			'sort' => array (
				'defaultOrder' => 'name asc'
			)
		));
	}
	
	
	/**
	 * Checks if this question category is used in TESTS or POLLS 
	 * 
	 * @return boolean
	 */
	public function isUsed() {
		return ($this->testsCount > 0) || ($this->pollsCount);
	}
	
	public static function getAllCategoriesSorted()
	{
		return self::model()->findAll(array(
			'order' => 'name ASC'
		));
	}
	
}