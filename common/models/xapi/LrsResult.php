<?php

/**
 * This is the model class for table "lrs_result".
 *
 * The followings are the available columns in table 'lrs_result':
 * @property integer $id_result
 * @property string $json_data
 * @property string $hash_data
 * @property string $simple_string
 * @property integer $success
 * @property integer $completion
 * @property string $duration
 * @property string $response_json
 * @property double $score_scaled
 * @property double $score_raw
 * @property double $score_min
 * @property double $score_max
 * @property string $extensions_json
 */
class LrsResult extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data', 'required'),
			array('success, completion', 'numerical', 'integerOnly'=>true),
			array('score_scaled, score_raw, score_min, score_max', 'numerical'),
			array('hash_data, simple_string', 'length', 'max'=>255),
			array('duration', 'length', 'max'=>64),
			array('response_json, extensions_json', 'safe'),
			// The following rule is used by search().
			array('id_result, json_data, hash_data, simple_string, success, completion, duration, response_json, score_scaled, score_raw, score_min, score_max, extensions_json', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_result' => 'Id Result',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'simple_string' => 'Simple String',
			'success' => 'Success',
			'completion' => 'Completion',
			'duration' => 'Duration',
			'response_json' => 'Response Json',
			'score_scaled' => 'Score Scaled',
			'score_raw' => 'Score Raw',
			'score_min' => 'Score Min',
			'score_max' => 'Score Max',
			'extensions_json' => 'Extensions Json',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_result',$this->id_result);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('simple_string',$this->simple_string,true);
		$criteria->compare('success',$this->success);
		$criteria->compare('completion',$this->completion);
		$criteria->compare('duration',$this->duration,true);
		$criteria->compare('response_json',$this->response_json,true);
		$criteria->compare('score_scaled',$this->score_scaled);
		$criteria->compare('score_raw',$this->score_raw);
		$criteria->compare('score_min',$this->score_min);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('extensions_json',$this->extensions_json,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsResult the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public static function storeResult($dataArr) {
	    
	    if (!$dataArr) return null;
	    
	    $dataDump = Xapi::dataDumpForDb($dataArr);
	    
        $model = new self();
	    
        $model->json_data       = $dataDump['json_data'];
        $model->hash_data       = $dataDump['hash_data'];
        $model->success         = isset($dataArr['success']) ? (int) $dataArr['success'] : null;
        $model->completion      = isset($dataArr['completion']) ? (int) $dataArr['completion'] : null;
        $model->duration        = isset($dataArr['duration']) ? $dataArr['duration'] : null;
        $model->response_json   = isset($dataArr['response']) ? $dataArr['response'] : null;
        $model->score_scaled    = isset($dataArr['score']['scaled']) ? $dataArr['score']['scaled'] : null;
        $model->score_raw       = isset($dataArr['score']['raw']) ? $dataArr['score']['raw'] : null;
        $model->score_min       = isset($dataArr['score']['min']) ? $dataArr['score']['min'] : null;
        $model->score_max       = isset($dataArr['score']['max']) ? $dataArr['score']['max'] : null;
        $model->simple_string   = @$dataArr['simple_string'];
        
        
        if (!$model->save()) {
            Xapi::log('Errors while saving Result:', CLogger::LEVEL_ERROR);
            Xapi::log($model->errors, CLogger::LEVEL_ERROR);
            return null;
        }
        
	    return $model;
        
	}
	
}
