<?php

/**
 * This is the model class for table "lrs_statement".
 *
 * The followings are the available columns in table 'lrs_statement':
 * @property integer $id_statement
 * @property string $json_data
 * @property string $hash_data
 * @property string $uuid
 * @property integer $id_actor
 * @property integer $id_authority
 * @property string $verb
 * @property integer $id_object
 * @property string $object_type
 * @property integer $id_result
 * @property integer $id_context
 * @property integer $id_user
 * @property string $timestamp
 * @property string $stored
 * @property integer $score
 * @property string $status
 * @property string $time
 * @property string $authority_consumer_key
 * @property integer $voided
 * @property string $version
 * @property integer $id_verb
 * @property integer $xapi_version_code
 *
 * The followings are the available model relations:
 * @property LrsVerb $idVerb
 * @property LrsStatementAttachment[] $lrsStatementAttachments
 */
class LrsStatement extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_statement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data, uuid, id_authority, verb, id_object, timestamp', 'required'),
			array('id_actor, id_authority, id_object, id_result, id_context, id_user, score, voided, id_verb, xapi_version_code', 'numerical', 'integerOnly'=>true),
			array('hash_data, uuid, status, time', 'length', 'max'=>255),
			array('object_type, authority_consumer_key', 'length', 'max'=>64),
			array('version', 'length', 'max'=>16),
			array('stored', 'safe'),
			// The following rule is used by search().
			array('id_statement, json_data, hash_data, uuid, id_actor, id_authority, verb, id_object, object_type, id_result, id_context, id_user, timestamp, stored, score, status, time, authority_consumer_key, voided, version, id_verb, xapi_version_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idVerb' => array(self::BELONGS_TO, 'LrsVerb', 'id_verb'),
			'lrsStatementAttachments' => array(self::HAS_MANY, 'LrsStatementAttachment', 'id_statement'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_statement' => 'Id Statement',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'uuid' => 'Uuid',
			'id_actor' => 'Id Actor',
			'id_authority' => 'Id Authority',
			'verb' => 'Verb',
			'id_object' => 'Id Object',
			'object_type' => 'Object Type',
			'id_result' => 'Id Result',
			'id_context' => 'Id Context',
			'id_user' => 'Id User',
			'timestamp' => 'Timestamp',
			'stored' => 'Stored',
			'score' => 'Score',
			'status' => 'Status',
			'time' => 'Time',
			'authority_consumer_key' => 'Authority Consumer Key',
			'voided' => 'Voided',
			'version' => 'Version',
			'id_verb' => 'Id Verb',
		    'xapi_version_code' => 'Xapi Version Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_statement',$this->id_statement);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('uuid',$this->uuid,true);
		$criteria->compare('id_actor',$this->id_actor);
		$criteria->compare('id_authority',$this->id_authority);
		$criteria->compare('verb',$this->verb,true);
		$criteria->compare('id_object',$this->id_object);
		$criteria->compare('object_type',$this->object_type,true);
		$criteria->compare('id_result',$this->id_result);
		$criteria->compare('id_context',$this->id_context);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('stored',$this->stored,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('authority_consumer_key',$this->authority_consumer_key,true);
		$criteria->compare('voided',$this->voided);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('id_verb',$this->id_verb);
		$criteria->compare('xapi_version_code',$this->xapi_version_code);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsStatement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public static function storeOneStatement($statement, $isSubStatement=false) {
	    
	    // Determine the object type (and SET it internaly)
	    $statement['object'] = isset($statement['object']) ? self::resolveObjectType($statement['object']) : null;
	     
	    // Generate and assign UUID if not specified yet
	    if ( empty($statement['statementId']) ) {
	        $statement['statementId'] = self::generateStatementUuid();
	    }

	    // Subsystems has some restrictions
	    if ($isSubStatement && ( isset($statement['id']) || iiset($statement['stored'])  || iiset($statement['version']) || iiset($statement['authority']))) {
	    	Xapi::log('Invalid SubStatement: contains forbidden attributes', CLogger::LEVEL_ERROR);
			return null;	    	
	    }
	    
	    
	    // Validate the incoming statement
	    if (!XapiValidator::validateStatement($statement)) {
	        Xapi::log('Failed to validate statement', CLogger::LEVEL_ERROR);
	        Xapi::log($statement, CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_BAD_REQUEST, "Failed to validate the statement");
	    }
	    
	    // Get these shortcuts
	    $verb      = $statement['verb'];
	    $actor     = $statement['actor'];
	    $object    = $statement['object'];
	    
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeStatementWrite($statement)) {
	        Xapi::log('Unauthorized Statement WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::log($statement, CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot write statements.");
	    }
	    
	    // Check if Statement exists (by UUID). Reject modification immediately! Send back CONFLICT response and die.
	    if (self::model()->findByAttributes(array('uuid' => $statement['statementId']))) {
	        Xapi::out(Xapi::HTTP_CONFLICT);
	    }

	    // VERB
	    if ($verb) {
            $verbModel = LrsVerb::storeVerb($verb);
            if (!$verbModel) {
                Xapi::log('Failed to save Verb', CLogger::LEVEL_ERROR);
                Xapi::log($verb, CLogger::LEVEL_ERROR);
                throw new XapiException('Failed to save Verb', Xapi::HTTP_BAD_REQUEST);
            }
            
			// Voiding another statement ???
            if ( ($verb == Xapi::VERB_VOIDED) && isset($object['id']) && ($object['objectType'] === Xapi::OBJ_TYPE_STATEMENT_REF) ) {
            	self::voidStatement($object['id']);
            }
	    }
	    
	    // OBJECT
	    $id_object = null;
	    $objectType = $object['objectType'];
	    if ( $objectType == Xapi::OBJ_TYPE_ACTIVITY ) {
	        $activity_model = LrsActivity::storeActivity($object);
	        $id_object = $activity_model->id_activity;
	    }
	    else if ( in_array($objectType, array(Xapi::OBJ_TYPE_AGENT, Xapi::OBJ_TYPE_GROUP)) ) {
	        $agent_model = LrsAgent::storeAgent($object);
	        $id_object = $agent_model->id_agent;
	    }
	    else if ( $objectType == Xapi::OBJ_TYPE_STATEMENT_REF ) {
	        $statement_object_model = self::model()->findByAttributes(array('uuid' => $object['id']));
	        if ($statement_object_model) {
	            $id_object = $statement_object_model->id_statement;
	        }
	    }
	    else if ( ($objectType == Xapi::OBJ_TYPE_SUB_STATEMENT) && !$isSubStatement ) {
	    	$objectCopy = $object;
	    	$objectCopy['objectType'] = Xapi::OBJ_TYPE_STATEMENT;
	    	$subStatementModel = self::storeOneStatement($objectCopy, true);
	    	if (!$subStatementModel) {
	    		Xapi::log('Failed to save a Statement', CLogger::LEVEL_ERROR);
	    		Xapi::log($objectCopy, CLogger::LEVEL_ERROR);
	    		Xapi::out(Xapi::HTTP_SERVER_ERROR);
	    	}
	    	else {
	    		$id_object = $subStatementModel->id_statement;
	    	}
	    }
	    else {
	    	Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Statement Object type");
	    }
	    	  
	    // ACTOR
	    $id_actor = null;
	    $agent_model = LrsAgent::storeAgent($actor);
        $id_actor = $agent_model->id_agent;
	    
	    // AUTHORITY
        $id_authority = null;
        if ( ! isset($statement['authority']) ) {
            $statement['authority'] = LrsAgent::makeActorFromLmsUserData(XapiAuth::getInstance()->user);
        }
        $agentArr = $statement['authority'];
        $agent_model = LrsAgent::storeAgent($agentArr);
        $id_authority = $agent_model->id_agent;
	    	  
        // CONTEXT
	    $id_context = null;
        if ( isset($statement['context']) ) {
            $contextArr = $statement['context'];
            $context_model = LrsContext::storeContext($contextArr);
            $id_context = $context_model ? $context_model->id_context : null;
        }
        
	    // RESULT
        $id_result = null;
        if ( isset($statement['result']) ) {
             
            // Result object CAN be a simple string, see TCAPI Spec 0.9 p.7)
            if ( !is_array($statement['result']) ) {
                $resultArr = array("simple_string" => $statement['result']);
            }
            else {
                $resultArr = $statement['result'];
            }
             
            $result_model = LrsResult::storeResult($resultArr);
            $id_result = $result_model ? $result_model->id_result : null;
        }
        
	    	  

	    //----
	    
        // Save
        
        // We must NOT save statementId into JSON-data dump
        $statement_copy = $statement;
        
        // Treat this statement as possible OBJECT! Set its attribute objectType ro "Statement";
        $statement_copy['objectType'] = Xapi::OBJ_TYPE_STATEMENT;
        
        unset($statement_copy['statementId']);
        
        $dataDump = Xapi::dataDumpForDb($statement_copy);
        
        // Record new statement
        $time_stamp = time();
        $stored = date('Y-m-d', $time_stamp) . "T" . date('H:i:s',$time_stamp)."Z";
        
        
        $model = new self();
        
        $model->json_data           = $dataDump['json_data'];
        $model->hash_data           = $dataDump['hash_data'];
        $model->uuid                = $statement['statementId'];
        $model->id_actor            = $id_actor;
        $model->id_authority        = $id_authority;
        $model->verb                = $verbModel->verb_id;
        $model->id_verb             = $verbModel->id;
        $model->id_object           = $id_object;
        $model->object_type         = $objectType;
        $model->timestamp           = isset($statement['timestamp']) ? $statement['timestamp'] : $stored;
        $model->stored              = $stored;
        $model->id_context          = $id_context;
        $model->id_result           = $id_result;
        $model->version             = isset($statement['version']) ? $statement['version'] : Xapi::getClientVersion();
              
        
        if (!$model->save()) {
            Xapi::log('Errors while saving a Statement: ', CLogger::LEVEL_ERROR);
            Xapi::log($model->errors, CLogger::LEVEL_ERROR);
            return null;
        }
	    
        // Track activity from this Statement
        //unset($statement['context']);
        //unset($statement['actor']);
        //unset($statement['authority']);
        //Log::_($statement);
        
        XapiTrack::trackStatement($model);
        
        return $model;
        
	}
	
	
	
	/**
	 * 
	 * @param unknown $statementsArr
	 */
	public static function storeManyStatements($statementsArr) {
	   
	    $statement_uuids = array();
	    
	    foreach ( $statementsArr as $statement ) {
	        $model = self::storeOneStatement($statement);
	        if (!$model) {
	            Xapi::log('Failed to save a Statement', CLogger::LEVEL_ERROR);
	            Xapi::log($statement, CLogger::LEVEL_ERROR);
	            Xapi::out(Xapi::HTTP_SERVER_ERROR);
	        }
	        $statement_uuids[] = $model->uuid;
	    }
	    
	    return $statement_uuids;
	     
	}
	
	
	/**
	 * Resolves (and sets) objectType of an Object
	 *
	 * @param array $objectArr
	 * @return array $objectArr
	 */
	private static function resolveObjectType($objectArr) {
	
	    // If type is set and not empty.. ok..
	    if ( isset($objectArr['objectType']) && !empty($objectArr['objectType']) ) {
	        $objectType = $objectArr['objectType'];
	    }
	    // If not explicitely set... is there a "verb" property there ? (a sign of a statement)...
	    else if ( isset($objectArr['verb']) ) {
	        $objectType = Xapi::OBJ_TYPE_STATEMENT;
	    }
	    // Not present, but maybe an Agent? Lets see...
	    else if ( LrsAgent::isAgent($objectArr) ) {
	        $objectType = Xapi::OBJ_TYPE_AGENTOBJ_TYPE;
	    }
	    // Well, by default, Activity
	    else {
	        $objectType = Xapi::OBJ_TYPE_ACTIVITY;
	    }
	
	    $objectArr['objectType'] = $objectType;
	
	    return $objectArr;
	}

	
	/**
	 * Generate random UUID in form of: 8-4-4-4-12
	 */
	public static function generateStatementUuid() {
	    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
	        // 8
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
	
	        // 4
	        mt_rand( 0, 0xffff ),
	
	        // 4
	        mt_rand( 0, 0x0fff ) | 0x4000,
	
	        // 4
	        mt_rand( 0, 0x3fff ) | 0x8000,
	
	        // 12
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	    );
	}
	

	/**
	 * Void a statement, by UUID.
	 * Do NOT void the statement if it is a voiding one itself! (detected by Voided Verb ID)
	 * 
	 * @param string $uuid
	 */
	public static function voidStatement($uuid) {
		
		$model = self::model()->findByAttributes(array(
			'uuid' => $uuid
		));
		
		// We cannot VOID a voiding statements!!
		if ($model->verb != Xapi::VERB_VOIDED) {
			if ($model) {
				$model->voided = 1;
				$model->save(false);
			}
			else {
				Xapi::out(Xapi::HTTP_NOT_FOUND, 'Statement not found while voiding');
			}
		}
		
	}
	
	/**
	 * 
	 * @param array $data
	 */
	public static function getStatements($data) {

	    // If a specific Registration ID is found in the filter,
	    // try to see if this is a previously mapped to another, internally to LMS registration ID, if any.
	    if (isset($data['registration'])) {
	        $data['registration'] = LrsRegistrationMap::toLmsRegistration($data['registration']);
	    }
	    
	    // This will directly send response to client if failed
	    if (!XapiValidator::validateFetchStatementsFilter($data)) {
	        Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid Statements Filter");
	    }
	    
	    // Offset 
	    $offset    =  isset($data['offset']) ? $data['offset'] : 0;

	    $params = array();
	    $command = Yii::app()->db->createCommand();
	    $command->from(self::model()->tableName() . " t_s");
	    $command->leftJoin(LrsAgent::model()->tableName()       		. " t_actor"            , "t_s.id_actor=t_actor.id_agent");
	    $command->leftJoin(LrsAgent::model()->tableName()       		. " t_actor_object"     , "t_s.id_object=t_actor_object.id_agent");
	    $command->leftJoin(LrsAgent::model()->tableName()       		. " t_authority"        , "t_s.id_authority=t_authority.id_agent");
	    $command->leftJoin(LrsContext::model()->tableName()     		. " t_context"          , "t_s.id_context=t_context.id_context");
	    $command->leftJoin(LrsResult::model()->tableName()      		. " t_result"           , "t_s.id_result=t_result.id_result");
	    $command->leftJoin(LrsActivity::model()->tableName()    		. " t_activity_object"  , "t_s.id_object=t_activity_object.id_activity");
	    
	    
	    // Is this an LMS internal report?
	    $internalReport = isset($data['internal']);
	    
	    // statementId : By Statement Id
	    if ( isset($data['statementId']) ) {
	        $command->andWhere('t_s.uuid=:uuid');
	        $params[':uuid'] = $data['statementId'];
	    }
	    
	    // voidedStatementId :  By Statement ID, must be Voided 
	    if ( isset($data['voidedStatementId']) ) {
	    	$command->andWhere('t_s.uuid=:uuid');
	    	$command->andWhere('t_s.voided > 0');
	    	$params[':uuid'] = $data['voidedStatementId'];
	    }
	    else {
	    	// Never return Voided statements if not explicitely requested to return a particular voided statement
	    	$command->andWhere('t_s.voided IS NULL');
	    }
	    
	    // AGENT (Agent or Identified Group As a Statement ACTOR  OR  OBJECT!!), v1.0
	    if ( isset($data['agent']) ) {
	        
	    	$agentArr = $data['agent'];
    		$agent_model = LrsAgent::findAgent($agentArr);
	    		
    		// IF there is an Agent model, good, filter, but if NOT, the whole query should FAIL
    		if ($agent_model) {
    			$id_agent = $agent_model->id_agent;
    			$command->andWhere('(t_actor.id_agent=:id_agent1) OR (t_actor_object.id_agent=:id_agent2 AND (t_s.object_type= :agent  OR t_s.object_type= :group))');
    			$params[':id_agent1'] = $id_agent;
    			$params[':id_agent2'] = $id_agent;
			    $params[':agent'] = Xapi::OBJ_TYPE_AGENT;
			    $params[':group'] = Xapi::OBJ_TYPE_GROUP;
    		}
    		else {
    			$command->andWhere('(FALSE)');  // No model, no answer
    		}
	    }
	     
	    
	    // ACTOR (v09)
	    if ( isset($data['actor']) ) {
	        $agentArr = $data['actor'];
	        $agent_model = LrsAgent::findAgent($agentArr);
	         
	        // IF there is an Agent model, good, filter, but if NOT, the whole query should FAIL
	        if ($agent_model) {
	            $id_agent = $agent_model->id_agent;
	            $command->andWhere('t_actor.id_agent=:id_agent1');
	            $params[':id_agent1'] = $id_agent;
	        }
	        else {
	            $command->andWhere('(FALSE)');  // No model, no answer
	        }
	    }
	     
	    // VERB: by Verb ID
	    if ( !empty($data['verb']) ) {
	    	$command->andWhere('t_s.verb=:verb');
	    	$params[':verb'] = $data['verb'];
	    }
	    
	    
	    // ACTIVITY: by Activity ID (v1.0)
	    if ( isset($data['activity']) ) {
	        
	    	$dataArr = LrsActivity::getSimpleActivityArray($data['activity']);
	    	$activityModel = LrsActivity::findActivity($dataArr);
	    	if ($activityModel) {
	    		$idActivity = $activityModel->id_activity;
	    		
	    		// Also related_activities
	    		if ( $data['related_activities'] ) {
    		        // :: Through the Context
       				$command->leftJoin(LrsContextActivities::model()->tableName()   . " t_context_activities"  , "t_s.id_context=t_context_activities.id_context");
       				$command->andWhere(' (t_s.id_object=:id_activity1) OR (t_context_activities.id_activity=:id_activity2)');
   	   		      	$params[':id_activity1'] = $idActivity;
   		  		    $params[':id_activity2'] = $idActivity;
	    		}
	    		else {
	    			$command->andWhere('t_s.id_object=:id_activity');
	    			$params[':id_activity'] = $idActivity;
	    		}
	    	}
	    	else {
	    		$command->andWhere('(FALSE)');
	    	}
	    	
	    }
	    

	    // OBJECT: Activity OR Actor (v0.9)
	    if ( isset($data['object']) ) {
	         
	        if (empty($data['object']) || empty($data['object']['objectType'])) {
	            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid OBJECT in fetch statements");
	        }
	        
	        $object = $data['object'];
	        
	        if ($object['objectType'] == Xapi::OBJ_TYPE_ACTIVITY) {
	            $activityModel = LrsActivity::findActivity($object);
	            if ($activityModel) {
	                $idActivity = $activityModel->id_activity;
                    if ($data['context']) {
                        $command->leftJoin(LrsContextActivities::model()->tableName()   . " t_context_activities"  , "t_s.id_context=t_context_activities.id_context");
                        $command->andWhere(' (t_s.id_object=:id_activity1) OR (t_context_activities.id_activity=:id_activity2)');
                        $params[':id_activity1'] = $idActivity;
                        $params[':id_activity2'] = $idActivity;
                    }
                    else {
                        $command->andWhere('t_s.id_object=:id_activity');
                        $params[':id_activity'] = $idActivity;
                    }
	            }
	            else {
	                $command->andWhere('(FALSE)');
	            }
	        }
	        else if ($object['objectType'] == Xapi::OBJ_TYPE_AGENT) {
	            $agent_model = LrsAgent::findAgent($object);
	            if ($agent_model) {
	                $id_agent = $agent_model->id_agent;
	                $command->andWhere('t_actor_object.id_agent=:id_agent1');
	                $params[':id_agent1'] = $id_agent;
	            }
	            else {
	                $command->andWhere('(FALSE)');  // No model, no answer
	            }
	        }
	        else {
	            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid OBJECT TYPE in fetch statements");
	        }
	        
	    
	    }
	     
	    
	    // REGISTRATION
	    if ( !empty($data['registration']) ) {
	        
	        if (!XapiValidator::validateRegistration($data['registration'])) {
	            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid activity");
	        }
	        
	    	$command->andWhere('t_context.registration_id=:registration');
	    	$params[':registration'] = $data['registration'];
	    }
	     
	    // SINCE
	    if ( !empty($data['since']) ) {
	        $command->andWhere('t_s.stored > :since');
	        $params[':since'] = $data['since'];
	    }
	    
	    // UNTIL
	    if ( !empty($data['until']) ) {
	        $command->andWhere('t_s.stored <= :until');
	        $params[':until'] = $data['until'];
	    }
	    
	    // limit:
	    $limit = 50;
	    if (isset($data['limit'])) {
	        if ( (int) $data['limit'] <= 0) {
	            Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid limit");
	        }
	        $limit = $data['limit'];
	    }
	    
	    
	    // attachments
	    // @TODO
	    	  
	    // ascending
	    $ascending =  isset($data['ascending']) ? $data['ascending'] : false;  
	    
	    // Order
	    $command->order = $ascending ? "stored ASC" : "stored DESC"; 

	    

	    // AUTHORIZE for "statement/read/mine" scope and/or Statement READ
	    // If user is granted "statement/read/mine" (and even he/she is granted "all" scope!), restrict to user's statements only
	    $auth = XapiAuth::getInstance();
	    if ( $auth->isScopeGranted(XapiAuth::SCOPE_STATEMENT_READ_MINE) ) {
	        $inferred_actor = XapiAuth::getInstance()->user['inferred_actor'];
	        $authority_model = LrsAgent::findAgent($inferred_actor);
	        if ( !$authority_model ) {
	            Xapi::log('Unauthorized Stetament READ operation. User Authority data not found.', CLogger::LEVEL_ERROR);
	            Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot Read OTHER's Statements");
	        }
	        $command->andWhere('id_authority = :id_authority');
	        $params[':id_authority'] = $authority_model->id_agent;
	    }
	    // Else, if None of the following scopes are granted, UNAUTHORIZE!!
	    else {
	        if ( !$auth->isAnyScopeGranted(array(XapiAuth::SCOPE_STATEMENT_READ, XapiAuth::SCOPE_ALL_READ)) ) {
	            Xapi::log('Unauthorized Stetament READ operation.', CLogger::LEVEL_ERROR);
	            Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot Read Statements");
	        }
	    }
	    
	    $command->group("t_s.id_statement");
	    
	    
	    $statementSelectStr        = self::modelFieldsAliasedSelect("LrsStatement",        "t_s");
	    $actorSelectStr            = self::modelFieldsAliasedSelect("LrsAgent",            "t_actor");
	    $actorObjectSelectStr      = self::modelFieldsAliasedSelect("LrsAgent",            "t_actor_object");
	    $authoritySelectStr        = self::modelFieldsAliasedSelect("LrsAgent",            "t_authority");
	    $contextSelectStr          = self::modelFieldsAliasedSelect("LrsContext",          "t_context");
	    $resultSelectStr           = self::modelFieldsAliasedSelect("LrsResult",           "t_result");
	    $activitySelectStr         = self::modelFieldsAliasedSelect("LrsActivity",         "t_activity_object");
	     
        // Select	    
	    $select = "
	        -- Statement
	        $statementSelectStr,
            DATE_FORMAT(t_s.stored,'%Y-%m-%dT%H:%i:%sZ') AS t_s__formatted_stored,
	        
	        -- Actor
	        $actorSelectStr,
	        
	        -- Actor/Agent
	        $actorObjectSelectStr,
	        
	        -- Authority
            $authoritySelectStr,
            
            -- Context
            $contextSelectStr,
            
            -- Result
            $resultSelectStr,
            
            -- Activity
            $activitySelectStr
        ";
	     
	    $command->select($select);
	    

	    $command->limit = $limit;
	    $command->offset = $offset;
	    
	    $statements = $command->queryAll(true, $params); 

	    $format = Xapi::STATEMENT_FORMAT_DEFAULT;
	    if (isset($data["format"])) {
	        if (!XapiValidator::validateFetchStatementsFormat($data["format"])) {
	           Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid statements format requested");        
	        }
	        $format = $data["format"];
	    }
	    
	    
	    // --- MORE URL
	    // We are very optimistically assuming that:
	    // if LIMIT is equal to the number of results, we MOST probably have MORE results to extract.
	    // This is just to optimize the query process, avoiding COUNTING all statements first and then limit them
	    $moreUrl = "";
	    
	    if ((int) $limit == (int) count($statements)) {
	    	$query_params = array();
	    	parse_str($data["queryString"], $query_params);
	    
		    // Unset some stuff
		    unset($query_params["type"]);
	    	unset($query_params["offset"]);
	    	unset($query_params["ascending"]);
	    
	    	$more_query_params = array();
	    	foreach ( $query_params as $key => $value ) {
	    		$more_query_params[] = "$key=" . urlencode($value);
	    	}
	    	$offset = $offset + $limit;
	    	$moreUrl = "statements?offset=" . $offset . "&" . "ascending=" . $data['ascending'] . "&" . implode("&", $more_query_params);
	    }
	    // ---
	    	  
	    $result = array(
	    	"statements"	=> array(),
	    	"more"			=> $moreUrl,	
	    );
	    
	    
	    if (!empty($statements)) {
	    	foreach ($statements as $input) {
	    		$output = self::composeStatementArr($input, $format, $internalReport);
	    		$result["statements"][] = $output;
	    	}
	    }

	    return $result;
	    
	    
	}
	
	
	
	
	/**
	 * Given a Database Statement (tables, fields, etc.), make a valid Statement for report
	 *
	 * @param array input
	 */
	private static function composeStatementArr($input, $format, $internalReport=false) {
	
	    if ($internalReport || (Xapi::getClientVersionCode() === Xapi::VERSION_CODE_0)) {
	        $format = Xapi::STATEMENT_FORMAT_EXACT;
	    }
	    
	    
		switch (strtolower($format)) {

			case Xapi::STATEMENT_FORMAT_IDS:

				$statementArr = array();
					
				// ID
				$statementArr['id'] 		= $input['t_s__uuid'];
				
				// ACTOR
				$model	= LrsAgent::model()->findByPk($input['t_s__id_actor']);
				$statementArr['actor'] = $model->toMinimalArr();
				
				// VERB
				$verbModel = LrsVerb::model()->findByPk($input['t_s__id_verb']);
				$statementArr['verb'] 		= array(
					'id'		=> $verbModel->verb_id,
					'display'	=> CJSON::decode($verbModel->display),
				);
					
					
				// OBJECT
				if ($input['t_s__object_type'] == Xapi::OBJ_TYPE_ACTIVITY) {
					$model = LrsActivity::model()->findByPk($input['t_s__id_object']);
					$statementArr['object'] = $model->toMinimalArr();
				}
				else if ($input['t_s__object_type'] == Xapi::OBJ_TYPE_AGENT || $input['t_s__object_type'] == Xapi::OBJ_TYPE_GROUP) {
					$model = LrsAgent::model()->findByPk($input['t_s__id_object']);
					$statementArr['object'] = $model->toMinimalArr();
				}
				else if ($input['t_s__object_type'] == Xapi::OBJ_TYPE_STATEMENT_REF) {
					$model = self::model()->findByPk($input['t_s__id_object']);
					$statementArr['object'] = array(
						'objectType'	=> 	Xapi::OBJ_TYPE_STATEMENT_REF,
						'id'			=>  $model->uuid,
					);
				}
				else if ($input['t_s__object_type'] == Xapi::OBJ_TYPE_SUB_STATEMENT) {
					$model = self::model()->findByPk($input['t_s__id_object']);
					$statementArr['object'] = CJSON::decode($model->json_data);
					$statementArr['object']['objectType'] = Xapi::OBJ_TYPE_SUB_STATEMENT;
				}
					
					
				// RESULT
				if ( (int) $input['t_s__id_result']) {
					$model = LrsResult::model()->findByPk((int) $input['t_s__id_result']);
					$statementArr['result'] = CJSON::decode($model->json_data);
				}
					
				// Context
				if ( (int) $input['t_s__id_context']) {
					$model = LrsContext::model()->findByPk((int) $input['t_s__id_context']);
					$statementArr['context'] = CJSON::decode($model->json_data);
				}
					
				// TIMESTAMP
				$statementArr['timestamp'] = $input['t_s__timestamp'];
				
				// STORED
				$statementArr['stored'] 	= $input['t_s__formatted_stored'];
					
				// Authority
				if ( (int) $input['t_s__id_authority']) {
					$model = LrsAgent::model()->findByPk((int) $input['t_s__id_authority']);
					$statementArr['authority'] = $model->toMinimalArr();
				}
				
				// Attachments
				if ($input['t_s__attachments']) {
					$statementArr['attachments'] = $input['t_s__attachments'];
				}
				
				break;
				
				
			case Xapi::STATEMENT_FORMAT_CANONICAL:	
			case Xapi::STATEMENT_FORMAT_EXACT:
			default:
			    
			    // First, get stored Statement JSON
				$statementArr = CJSON::decode($input['t_s__json_data']);
				
				// Then add some internally saved data
				if ($input['t_s__object_type'] == Xapi::OBJ_TYPE_ACTIVITY) {
				    $model = LrsActivity::model()->findByPk($input['t_s__id_object']);
				    $statementArr['object'] = $model->toFullArr();
				}
				
				break;
			
		}
		
		
		// Check Version
		if (empty($statementArr['version'])) { 
			if ($input['t_s__version']) {
				$statementArr['version'] = $input['t_s__version'];
			}
			else if ((int) $input['t_s__xapi_version_code'] > 0) {
				$statementArr['version'] = "1.0.0";
			}
		}
				
		
		$statementArr['stored'] 	          = $input['t_s__formatted_stored'];
		$statementArr['xapi_version_code']    = $input['t_s__xapi_version_code'];
		$statementArr["originalJSON"]         = $input["t_s__json_data"];
		$statementArr["id"]                   = $input["t_s__uuid"];
		
		// Internal (LMS) reporting expects the statements to be in current MAX supported version
		if ($internalReport) {
		      $statementArr = self::convertFrom09ToCurrentMax($statementArr);
		}

		
		return $statementArr;
		
		
	}

	
	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
		 
		// Always set the xAPI version code. We collect data for different [client] versions in the same database table
		// and use the xapi_version_code to distinguish DB differences, if any {@see Xapi::getVersionCode())
		$this->xapi_version_code = Xapi::getClientVersionCode();
		 
		return parent::beforeSave();
		
	}
	
	/**
	 * Creates a "select"-ish string like "tablealias.fieldname AS tablealias__fieldname, ..." for all table fields
	 * Usefull for SQL SELECTs to automate the process of creating  <field> AS <fieldAlias>
	 * 
	 * @param string $arClassName Active Record Class name
	 * @param string $tableAlias AR table alias (used as a prefix to all field aliases)
	 * 
	 * @return string
	 */
	private static function modelFieldsAliasedSelect($arClassName, $tableAlias) {
	    $columnAliases = array();
	    $tmp = new $arClassName();
	    $columns = $tmp->metaData->columns;
	    foreach ($columns as $column) {
	        $columnsAliases[$column->name] = $tableAlias . "." . $column->name . " AS ". $tableAlias . "__" . $column->name;
	    }
	    $selectStr = implode(",", $columnsAliases);
	    return $selectStr;
	}
	
	
	
	/**
	 * Convert a statement to currently MAX supported version
	 * 
	 * @param array $dataArr
	 * @return array The modified input array
	 */
	public static function convertFrom09ToCurrentMax($dataArr) {

		if (!empty($dataArr['voided'])) {
			return $dataArr;
		}
		
		// Remove VOIDED property from the satetement
		if (isset($dataArr['voided'])) unset($dataArr['voided']);
		
		// Add version
		$dataArr['version'] = Xapi::VERSION_MAX;
		
		// Fix the VERB (0.9 only)
		if (!empty($dataArr['verb'])) {
			if (in_array($dataArr['verb'], Xapi::$VERBS_09X)) {
				$dataArr['verb'] = array(
				   "id" => "http://adlnet.gov/expapi/verbs/" . $dataArr['verb'],
				   "display"    => array(
				       "en-US"  => $dataArr['verb'], 
				   ), 
				);
			}
		}		
		
		// Check if OBJECT is an activity
		if (!empty($dataArr['object']) && (empty($dataArr['object']['objectType']) ||  $dataArr['object']['objectType'] == Xapi::OBJ_TYPE_ACTIVITY) ) {
			$dataArr['object'] = LrsActivity::convertFrom09ToCurrentMax($dataArr['object']);
		}
		
		// Check Context for activities
		if (!empty($dataArr['context'])) {
			// Unset non-reference for Statement property
			if (isset($dataArr['context']['statement']) && !is_string($dataArr['context']['statement'])) {
				unset($dataArr['context']['statement']);
			}
			$dataArr['context'] = LrsContext::convertFrom09ToCurrentMax($dataArr['context']);
		}
		
		// Check ACTOR attribute
		if (!empty($dataArr['actor'])) {
			$dataArr['actor'] = LrsAgent::convertFrom09ToCurrentMax($dataArr['actor']);
		}
		
		// Check Authority attribute
		if (!empty($dataArr['authority'])) {
			$dataArr['authority'] = LrsAgent::convertFrom09ToCurrentMax($dataArr['authority']);
		}
		else {
			$dataArr['authority'] = array(
				'homePage'	=> Docebo::getRootBaseUrl(true),
				'name'		=> "unknown",	
			);
		}
		
		$dataArr['xapi_version_code'] = Xapi::getVersionCode(Xapi::VERSION_MAX);
		
		
		return $dataArr;
		
		
	}

	

	
}
