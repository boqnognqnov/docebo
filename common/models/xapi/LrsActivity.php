<?php

/**
 * This is the model class for table "lrs_activity".
 *
 * The followings are the available columns in table 'lrs_activity':
 * @property integer $id_activity
 * @property string $json_data
 * @property string $hash_data
 * @property string $activity_id
 * @property string $def_name_json
 * @property string $def_description_json
 * @property string $def_activity_type
 * @property string $def_interaction_type
 * @property string $def_correct_pattern_json
 * @property string $def_interaction_components
 * @property string $def_extensions_json
 * @property string $def_moreInfo
 * @property integer $canonical_version
 * @property integer $xapi_version_code
 *
 * The followings are the available model relations:
 * @property contextActivities[] $lrsContextActivities
 * 
 */
class LrsActivity extends CActiveRecord
{
	
	public static $_possibleInteractionType = array(
			'choice',
			'sequencing',
			'likert',
			'matching',
			'performance',
			'true-false',
			'fill-in',
			'numeric',
			'other'
	);
	
	
	public static $_possibleActivityType = array(
			"course",
			"module",
			"meeting",
			"media",
			"performance",
			"simulation",
			"assessment",
			"interaction",
			"cmi.interaction",
			"question",
			"objective",
			"link",
	);
	
	public static $INTERACTION_TYPE_COMPONENTS_MAP = array(
			'choice'		 	=> 'choices',
			'sequencing' 		=> 'choices',
			'likert'			=> 'scale',
			'matching'			=> 'source,target',   // TWO list of
			'performance'		=> 'steps',
			'true-false'        => 'correctResponsesPattern',
			'fill-in'			=> 'correctResponsesPattern',
			'numeric'			=> 'correctResponsesPattern',
			'other'				=> 'correctResponsesPattern'
	);

	public static $OPTIONAL_COMPONENTS = array(
			"correctResponsesPattern"
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_activity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data, activity_id', 'required'),
			array('canonical_version, xapi_version_code', 'numerical', 'integerOnly'=>true),
			array('hash_data, def_extensions_json', 'length', 'max'=>255),
			array('def_interaction_type', 'length', 'max'=>200),
			array('def_moreInfo, def_activity_type, activity_id', 'length', 'max'=>2000),
			array('def_name_json, def_description_json, def_correct_pattern_json, def_interaction_components', 'safe'),
			// The following rule is used by search().
			array('id_activity, json_data, hash_data, activity_id, def_name_json, def_description_json, def_activity_type, def_interaction_type, def_correct_pattern_json, def_interaction_components, def_extensions_json, def_moreInfo, canonical_version, xapi_version_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'contextActivities' => array(self::HAS_MANY, 'LrsContextActivities', 'id_activity'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_activity' => 'Id Activity',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'activity_id' => 'Activity',
			'def_name_json' => 'Def Name Json',
			'def_description_json' => 'Def Description Json',
			'def_activity_type' => 'Def Activity Type',
			'def_interaction_type' => 'Def Interaction Type',
			'def_correct_pattern_json' => 'Def Correct Pattern Json',
			'def_interaction_components' => 'Def Interaction Components',
			'def_extensions_json' => 'Def Extensions Json',
			'def_moreInfo' => 'Def More Info',
			'canonical_version' => 'Canonical Version',
		    'xapi_version_code' => 'Xapi Version Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_activity',$this->id_activity);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('activity_id',$this->activity_id,true);
		$criteria->compare('def_name_json',$this->def_name_json,true);
		$criteria->compare('def_description_json',$this->def_description_json,true);
		$criteria->compare('def_activity_type',$this->def_activity_type,true);
		$criteria->compare('def_interaction_type',$this->def_interaction_type,true);
		$criteria->compare('def_correct_pattern_json',$this->def_correct_pattern_json,true);
		$criteria->compare('def_interaction_components',$this->def_interaction_components,true);
		$criteria->compare('def_extensions_json',$this->def_extensions_json,true);
		$criteria->compare('def_moreInfo',$this->def_moreInfo,true);
		$criteria->compare('canonical_version',$this->canonical_version);
		$criteria->compare('xapi_version_code',$this->xapi_version_code);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsActivity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Find a model of an activity identified by its DATA (array of data) 
	 * 
	 * @param array $dataArr
	 * 
	 * @return LrsActivity
	 */
	public static function findActivity($dataArr) {
	 
	    $attributes = array(
	        'activity_id'          => $dataArr['id'],
	        'xapi_version_code'    => Xapi::getClientVersionCode(),
	    );
	    $models = self::model()->findAllByAttributes(array('activity_id' => $dataArr['id']));
	    
	    if (count($models) > 1) {
	        throw new XapiException('Non-uqiue activity exists in the database: ' . $dataArr['id']);
	    }
	    else if (empty($models)) {
	        return null;
	    }
	    
	    $model = $models[0];
	    
	    
	    return $model;
	    
	}
	
	
	/**
	 * 
	 * @param unknown $dataArr
	 */
	public static function storeActivity($dataArr) {
	    
	    if (!$dataArr) return null;
	    
	    // Validate Activity data structure
		if (!XapiValidator::validateActivity($dataArr)) {
		    Xapi::out(Xapi::HTTP_BAD_REQUEST, "Invalid activity");
		}
		
		// Save the activity (internally updates or creates)
		return self::saveActivity($dataArr);
	}
	

	/**
	 * Save Activity
	 */
	private static function saveActivity($dataArr) {
	    
	    /* @var $model LrsActivity */
	    
	    $dataDump = Xapi::dataDumpForDb($dataArr);
	    
	    $interaction_type = '';
	    $def_interaction_components = '';
	    
	    // Search if we already have this activity in the DB.
	    $model = self::findActivity($dataArr);
	    if (!$model) {
	        $model = new self();
	    }

	    // Decode stored data
	    $storedData = CJSON::decode($model->json_data);
	    
	    // -----
	    // 1. Get definition from function parameter 
	    $incomingDefinition = isset($dataArr['definition']) ? $dataArr['definition'] : array();
	    
	    // 2. Get previously stored definition (if any)
	    $storedDefinition = isset($storedData['definition']) ? $storedData['definition'] : array();
	    
	    // Merge incoming definition over stored one
	    $defintion = self::mergeDefinition($storedDefinition, $incomingDefinition);

	    // 3. Download definition from URL (if any)
	    // To optimize the process, only download the definition if not yet stored
	    $downloadedDefinition = false;
	    if (empty($defintion) && (Xapi::getClientVersionCode() >=  Xapi::VERSION_CODE_1)) {
	    	$downloadedDefinition = self::getDefinitionFromUrl($dataArr['id']);
	    }
	    
	    // 4. Resolve final activity definition
	    // Merge downloaded definition over stored & incoming ones
	    if (!empty($downloadedDefinition) !== false && is_array($downloadedDefinition)) {
	       $defintion = $this->mergeDefinition($defintion, $downloadedDefinition);
	    }
	    // -----

	    
	    // --
	    // Prepare some field values; they may or may not be defined
	    if ( !empty($defintion) ) {
	        // We might have merged the definition, lets put it back into the incoming array (yes, modifying it!)
	        $dataArr['definition']     = $defintion;
	        $def_name_json             = Xapi::jsonPropertyForDb($defintion, "name");
	        $def_description_json      = Xapi::jsonPropertyForDb($defintion, "description");
	        $def_activity_type         = isset($defintion['type']) ? $defintion['type'] : "" ;
	        $def_correct_pattern_json  = Xapi::jsonPropertyForDb($defintion, "correctResponsesPattern");
	        $def_extensions_json       = Xapi::jsonPropertyForDb($defintion, "extensions");
	    }
	    else {
	        $def_name_json             = "";
	        $def_description_json      = "";
	        $def_activity_type         = "";
	        $def_correct_pattern_json  = "";
	        $def_extensions_json       = "";
	    }
	    // --
	     
	    
	    // ---
	    // 1. Do we have an interaction type set?
	    if (isset($defintion['interactionType'])) {
	        $interaction_type = $defintion['interactionType'];
	         
	        // 2. Do we have a name for components list for this type of interaction
	        if ( isset(self::$INTERACTION_TYPE_COMPONENTS_MAP[$interaction_type]) ) {
	            $components_list_name = self::$INTERACTION_TYPE_COMPONENTS_MAP[$interaction_type];
	             
	            // 3. Do we finally have an array of components for this interaction type
	            if ( isset($defintion[$components_list_name]) ) {
	                // JSON for MySQL
	                $def_interaction_components = Xapi::jsonPropertyForDb($defintion,$components_list_name);
	            }
	        }
	    }
	    // --

	    
	    $dataDump = Xapi::dataDumpForDb($dataArr);
	     
	    $model->json_data                  = $dataDump['json_data'];
	    $model->hash_data                  = $dataDump['hash_data'];
	    $model->activity_id                = $dataArr['id'];
	    $model->def_name_json              = $def_name_json;
	    $model->def_description_json       = $def_description_json;
	    $model->def_activity_type          = $def_activity_type;
	    $model->def_interaction_type       = $interaction_type;
	    $model->def_correct_pattern_json   = $def_correct_pattern_json;
	    $model->def_interaction_components = $def_interaction_components;
	    $model->def_extensions_json        = $def_extensions_json;
	     
	    if (!$model->save()) {
		    Xapi::log("Failed to save Activity", CLogger::LEVEL_ERROR);
		    Xapi::log($model->errors, CLogger::LEVEL_ERROR);
	        return null;
	    }
	    
	    return $model;
	    
	}
	

	/**
	 * 
	 * @param unknown $dataArr
	 * @return boolean
	 */
	public static function validateActivity($dataArr) {
	    
	    $versionCode = Xapi::getClientVersionCode();
	
		if ( isset($dataArr['definition']['interactionType']) ) {
			if(!self::validateInteractionType($dataArr['definition']['interactionType'])) return false;
			if(!self::validateComponentList($dataArr['definition'])) return false;
		}
	
		if ( isset($dataArr['definition']['type']) && !self::validateActivityType($dataArr['definition']['type'])) return false;
	
		if (!isset($dataArr['id'])) return false;
		
		if ($versionCode == Xapi::VERSION_CODE_0) {
		    if (!is_string($dataArr['id'])) return false;
		    // XAPI Spec is not much clear about Spaces in Activity IDs (v0.9)
		    // We have customers playing TinCan object having really bad formed Activity IDs, so, lets give them support
		    // That's why we comment out the following line 
		    //if (preg_match('/\s/', $dataArr['id'])) return false;
		}
		else {
		    if (!is_string($dataArr['id'])) return false;
		    // The same for v1.x (see above)
		    // if (preg_match('/\s/', $dataArr['id'])) return false;
		    if (!Xapi::isUri($dataArr['id'])) return false;
		}
		
		return true;
		
	}
	
	
	/**
	 * 
	 * @param unknown $type
	 */
	private static function validateInteractionType($type) {
		if(in_array($type,self::$_possibleInteractionType)) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * 
	 * @param unknown $definition
	 */
	private static function validateComponentList($definition) {
		$result = false;
		$component_list = explode(',', self::$INTERACTION_TYPE_COMPONENTS_MAP[$definition['interactionType']]);
		$component_list = array_diff($component_list, self::$OPTIONAL_COMPONENTS);
		if(empty($component_list))
			return true;

		foreach($component_list as $component) {
			if(isset($definition[$component])) {
				$result = true;
			}
		}
		return $result;
	}
	
	
	/**
	 * 
	 * @param unknown $type
	 */
	private static function validateActivityType($type) {
	    
	    // For now, just accept ANY Activity type, which is basically an IRI
	    // http://activitystrea.ms/schema/1.0/page
	    return true;
	    
		$type = strtolower($type);
		if ( in_array($type, self::$_possibleActivityType )) {
			return true;
		}
		return false;
	}
	
	
	
	/**
	 * 
	 * @param string $activityId
	 * @return array
	 */
	public static function getSimpleActivityArray($activityId) {
		return array('objectType' => Xapi::OBJ_TYPE_ACTIVITY, 'id' => $activityId);
	}
	
	/**
	 * Download Activity definition from URL. 
	 * Utilize FILE cache to optimize the load.
	 * 
	 * @param string $iri  Activity ID IRI
	 * @return boolean|mixed
	 */
    public static function getDefinitionFromUrl($iri) {

    	$cacheKey = md5($iri);
    	
    	if (isset(Yii::app()->cache_xapi_activity)) {
    		$arr = Yii::app()->cache_xapi_activity->get($cacheKey);
    		if ($arr !== false) {
    			return $arr;
    		}
    	}
    	
        $url = Xapi::iriToUri($iri);
        
        $ch = curl_init();
        
        $options = array(
            CURLOPT_URL                     => $url,
            CURLOPT_VERBOSE                 => 1,
            CURLOPT_SSL_VERIFYPEER          => FALSE,
            CURLOPT_SSL_VERIFYHOST          => FALSE,
            CURLOPT_RETURNTRANSFER          => 1,
            CURLOPT_HTTPHEADER              => array(
                "Accept: application/json,/",
            ),
        	CURLOPT_TIMEOUT					=> 10,  //  seconds
            //CURLOPT_PROXY                   => "127.0.0.1",
            //CURLOPT_PROXYPORT               => "8888",
        );
        
        curl_setopt_array($ch, $options);
        
        // Get remote data
        $result = curl_exec($ch);
       
        // NOT false? So we got some result, lets check
        $arr = array();
        if ($result !== false) {
            // We expect a JSON, lets parse
            $arr = CJSON::decode($result);
            // If the result is NOT an array, we've got some problem.. Reset to array (meaning Nothing for the callers)
            if (!is_array($arr)) {
                Xapi::log("Failed to parse JSON from " . $url . ". The response was: ");
                Xapi::log($result);
                $arr = array();
            }
        }
        
        // Cache the result (even the empty array!)
        if (isset(Yii::app()->cache_xapi_activity)) {
        	Yii::app()->cache_xapi_activity->set($cacheKey, $arr);
        }
        
        return $arr;
        
    }
	
    /**
     * Flush (clear) Activity definitions cache
     */
    public static function flushCache() {
    	if (isset(Yii::app()->cache_xapi_activity)) {
    		Yii::app()->cache_xapi_activity->flush();
    	}
    }
    
    
    /**
     * @see CActiveRecord::beforeSave()
     */
	public function beforeSave() {
	    
	    // Always set the xAPI version code. We collect data for different [client] versions in the same database table
	    // and use the xapi_version_code to distinguish DB differences, if any {@see Xapi::getVersionCode())  
	    $this->xapi_version_code = Xapi::getClientVersionCode();
	    
	    return parent::beforeSave();
	}
	
	
	/**
	 *
	 * @return boolean
	 */
	public function toMinimalArr() {
		$arr = array();
		$arr['objectType'] 	= Xapi::OBJ_TYPE_ACTIVITY;
		$arr['id']			= $this->activity_id;
		return $arr;
	}
	
	
	/**
	 * Return a re-engineered Activity JSON (from internally saved data)
	 * The point is: sometimes ac Activity received as part of a Statement consists of
	 * only "id" and few more data, where "id" is actually an IRI -> URI -> URL which is inspected
	 * during the Activity save process. Loaded data (from that URL) is saved into the database table as
	 * an internal representation of the Activity, while the ORIGINAL JSON is saved in json_data
	 * which MUST be there for later usage.
	 * 
	 * This method uses ALL collected data and builds a full blown Activity Object (well, an array, which is then converted to JSON)
	 * 
	 */
	public function toFullArr() {
	    
	    $arr = array();
	    
	    $arr = CJSON::decode($this->json_data);
	    $arr['objectType'] 	= Xapi::OBJ_TYPE_ACTIVITY;
	    
	    
	    // Full up temporary definition array
	    $tmpDef = array();
	    
        if ($this->def_name_json) {
            $tmpDef["name"] = CJSON::decode($this->def_name_json);
        }
	    
        if ($this->def_description_json) {
            $tmpDef["description"] = CJSON::decode($this->def_description_json);
        }

        if ($this->def_moreInfo) {
            $tmpDef["moreInfo"] = $this->def_moreInfo;
        }
        
        if ($this->def_extensions_json) {
            $tmpDef["extensions"] = CJSON::decode($this->def_extensions_json);
        }
        
        if ($this->def_interaction_type) {
            unset($arr['definition']['interactionType']);
            $tmpDef["interactionType"] = $this->def_interaction_type;
        }
        
        if ($this->def_correct_pattern_json) {
            unset($arr['definition']['correctResponsesPattern']);
            $tmpDef["correctResponsesPattern"] = CJSON::decode($this->def_correct_pattern_json);
        }
        
        if ($this->def_interaction_components) {
            $typeName = self::$INTERACTION_TYPE_COMPONENTS_MAP[$this->def_interaction_type];
            unset($arr['definition'][$typeName]);
            $tmpDef[$typeName] = CJSON::decode($this->def_interaction_components); 
        }
        
        if (!empty($tmpDef)) {
            if (!isset($arr['definition'])) $arr['definition'] = array();
            $arr['definition'] = $this->mergeDefinition($arr['definition'], $tmpDef);
        }
        
	    
	    return $arr;
	     
	}
	

	/**
	 * Convert 0.90 Activity to current MAX supported version
	 *
	 * @param array $dataArr
	 * @return array The modified input array
	 */
	public static function convertFrom09ToCurrentMax($dataArr) {
	
		if (!empty($dataArr['id'])) {
			$id = $dataArr['id'];
			if (!(strpos('http://', $id) === 0) && !(strpos('https://', $id) === 0) ) {
				$dataArr['id'] = "tag:adlnet.gov,2013:expapi:0.9:activities:" . $id;
			}
		}
	
		if (!empty($dataArr['extensions']) && is_array($dataArr['extensions'])) {
			foreach ($dataArr['extensions'] as $key => $value) {
				if (!(strpos('http://', $key) === 0) && !(strpos('https://', $key) === 0) ) {
					$dataArr['extensions']["tag:adlnet.gov,2013:expapi:0.9:extensions:" . $key] = $value;
					unset($dataArr['extensions'][$key]);
				}
			}
		}
	
		if (!empty($dataArr['definition']['type'])) {
			if (!Xapi::isUri($dataArr['definition']['type'])) {
				$dataArr['definition']['type'] = "http://adlnet.gov/expapi/activities/" . $dataArr['definition']['type'];
			}
		}
	
		return $dataArr;
	
	}
	
	
	/**
	 * Merge Old and New Activity definition
	 * 
	 * And NO, It is NOT possible to user array_merge() or CMap::mergeArray()   :-)
	 * 
	 * 
	 * @param array $def1
	 * @param array $def2
	 */
	private static function mergeDefinition($old, $new) {
	    
	    $choices   = isset($new['choices']) ? $new['choices'] : (isset($old['choices']) ? $old['choices'] : false);
	    
	    $sources   = isset($new['source']) ? $new['source'] : (isset($old['source']) ? $old['source'] : false);
	    $targets   = isset($new['target']) ? $new['target'] : (isset($old['target']) ? $old['target'] : false);
	    
	    $steps     = isset($new['steps']) ? $new['steps'] : (isset($old['steps']) ? $old['steps'] : false);
	    $scales    = isset($new['scale']) ? $new['scale'] : (isset($old['scale']) ? $old['scale'] : false);
	    
        $type          = isset($new['type']) ? $new['type'] : (isset($old['type']) ? $old['type'] : false); 
	    $name          = isset($new['name']) ? $new['name'] : (isset($old['name']) ? $old['name'] : false); 
        $description   = isset($new['description']) ? $new['description'] : (isset($old['description']) ? $old['description'] : false);

        $interactionType          = isset($new['interactionType']) ? $new['interactionType'] : (isset($old['interactionType']) ? $old['interactionType'] : false);
        $correctResponsesPattern  = isset($new['correctResponsesPattern']) ? $new['correctResponsesPattern'] : (isset($old['correctResponsesPattern']) ? $old['correctResponsesPattern'] : false);
        
        $definition = array();

        if ($type)                          $definition['type'] = $type;
        if ($name)                          $definition['name'] = $name;
        if ($description)                   $definition['description'] = $description;
        if ($interactionType)               $definition['interactionType'] = $interactionType;
        if ($correctResponsesPattern)       $definition['correctResponsesPattern'] = $correctResponsesPattern;
        if ($choices)                       $definition['choices'] = $choices;
        if ($scales)                        $definition['scale'] = $scales;
        if ($sources)                       $definition['source'] = $sources;
        if ($targets)                       $definition['target'] = $targets;
        if ($steps)                         $definition['steps'] = $steps;
        
        return $definition;
	    
	}
	
	
}
