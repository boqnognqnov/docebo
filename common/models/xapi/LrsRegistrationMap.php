<?php

/**
 * This is the model class for table "lrs_registration_map".
 *
 * The followings are the available columns in table 'lrs_registration_map':
 * @property integer $id
 * @property string $ap_registration
 * @property string $lms_registration
 */
class LrsRegistrationMap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_registration_map';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ap_registration, lms_registration', 'required'),
			array('ap_registration, lms_registration', 'length', 'max'=>128),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ap_registration, lms_registration', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ap_registration' => 'Ap Registration',
			'lms_registration' => 'Lms Registration',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ap_registration',$this->ap_registration,true);
		$criteria->compare('lms_registration',$this->lms_registration,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsRegistrationMap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Map Content (AP) registration ID to internal (LMS) registration ID, if any
	 * 
	 * @param string $registration
	 */
	public static function toLmsRegistration($registration) {
	    $model = self::model()->findByAttributes(array("ap_registration" => $registration));
	    if (!$model) return $registration;
	    return $model->lms_registration;
	}
	
	/**
	 * Map internal (LMS) registration to Content (AP) registration ID, if any
	 * 
	 * @param string $registration
	 */
	public static function toApRegistration($registration) {
	    $model = self::model()->findByAttributes(array("lms_registration" => $registration));
	    if (!$model) return $registration;
	    return $model->ap_registration;
	}
	
	
}
