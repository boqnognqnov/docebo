<?php

/**
 * This is the model class for table "lrs_agent".
 *
 * The followings are the available columns in table 'lrs_agent':
 * @property integer $id_agent
 * @property string $json_data
 * @property string $hash_data
 * @property string $objectType
 * @property string $name_json
 * @property string $mbox_json
 * @property string $mbox_sha1sum_json
 * @property string $openid_json
 * @property string $member
 * @property string $givenName_json
 * @property string $familyName_json
 * @property string $firstName_json
 * @property string $lastName_json
 * @property string $account_json
 * @property integer $canonical_version
 * @property string $account_homePage
 * @property string $account_name
 * @property integer $xapi_version_code
 */
class LrsAgent extends CActiveRecord
{
    
    static $AGENT_TYPES = array(Xapi::OBJ_TYPE_AGENT, Xapi::OBJ_TYPE_GROUP, Xapi::OBJ_TYPE_PERSON);
    
    /**
     * List of Agent/Actor/Persona properties, very specific to the kind
     */
    private static $SPECIFIC_PROPS = array("mbox", "mbox_sha1sum", "openid", "account");
    
    /**
     * Person specific properties for version 0.9
     */
    private static $SPECIFIC_PROPS_V09 = array("givenName", "familyName", "firstName", "lastName");
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_agent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data, objectType', 'required'),
			array('canonical_version, xapi_version_code', 'numerical', 'integerOnly'=>true),
			array('hash_data, account_name', 'length', 'max'=>255),
			array('objectType', 'length', 'max'=>64),
			array('account_homePage', 'length', 'max'=>2000),
			array('name_json, mbox_json, mbox_sha1sum_json, openid_json, member, givenName_json, familyName_json, firstName_json, lastName_json, account_json', 'safe'),
			// The following rule is used by search().
			array('id_agent, json_data, hash_data, objectType, name_json, mbox_json, mbox_sha1sum_json, openid_json, member, givenName_json, familyName_json, firstName_json, lastName_json, account_json, canonical_version, account_homePage, account_name, xapi_version_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_agent' => 'Id Agent',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'objectType' => 'Object Type',
			'name_json' => 'Name Json',
			'mbox_json' => 'Mbox Json',
			'mbox_sha1sum_json' => 'Mbox Sha1sum Json',
			'openid_json' => 'Openid Json',
			'member' => 'Member',
			'givenName_json' => 'Given Name Json',
			'familyName_json' => 'Family Name Json',
			'firstName_json' => 'First Name Json',
			'lastName_json' => 'Last Name Json',
			'account_json' => 'Account Json',
			'canonical_version' => 'Canonical Version',
			'account_homePage' => 'Account Home Page',
			'account_name' => 'Account Name',
		    'xapi_version_code' => 'Xapi Version Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_agent',$this->id_agent);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('objectType',$this->objectType,true);
		$criteria->compare('name_json',$this->name_json,true);
		$criteria->compare('mbox_json',$this->mbox_json,true);
		$criteria->compare('mbox_sha1sum_json',$this->mbox_sha1sum_json,true);
		$criteria->compare('openid_json',$this->openid_json,true);
		$criteria->compare('member',$this->member,true);
		$criteria->compare('givenName_json',$this->givenName_json,true);
		$criteria->compare('familyName_json',$this->familyName_json,true);
		$criteria->compare('firstName_json',$this->firstName_json,true);
		$criteria->compare('lastName_json',$this->lastName_json,true);
		$criteria->compare('account_json',$this->account_json,true);
		$criteria->compare('canonical_version',$this->canonical_version);
		$criteria->compare('account_homePage',$this->account_homePage,true);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('xapi_version_code',$this->xapi_version_code);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsAgent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Find an Agent in the database
	 * 
	 * @param array $agentArr
	 *
	 * @return LrsAgent
	 */
	public static function findAgent($agentArr) {
	    
	    $versionCode = Xapi::getClientVersionCode();
	    
	    $model = null;
	    $agentArr = self::resolveAgentType($agentArr);
	    $objectType = $agentArr['objectType'];
	
	    if (!XapiValidator::validateAgent($agentArr)) return null;
	    
        if ( !$model ) {
            $model = self::findByMbox($agentArr, $versionCode);
        }
        if ( !$model ) {
            $model = self::findByAccount($agentArr, $versionCode);
        }
        if ( !$model ) {
            $model = self::findByOpenid($agentArr, $versionCode);
        }
        if ( !$model ) {
            $model = self::findByMboxSha1sum($agentArr, $versionCode);
        }
	        
	    return $model;
	
	}
	
	
	/**
	 * Store Agent into DB (actor, person, group, etc.)
	 *
	 * @return LrsAgent
	 */
	public static function storeAgent($agentArr) {
	    
	    if (!$agentArr) return null;
	    
	    $agentArr = self::resolveAgentType($agentArr);
	    if (!XapiValidator::validateAgent($agentArr)) {
	        Xapi::log('Invalid agent: ', CLogger::LEVEL_ERROR);
	        Xapi::log($agentArr, CLogger::LEVEL_ERROR);
	        return null;
	    }
        return self::saveAgent($agentArr);
	}
	
	
	/**
	 * Create/Update an agant in DB
	 * 
	 * @param array $agentArr
	 * 
	 * @return LrsAgent
	 */
	private static function saveAgent($agentArr) {

	    $model = self::findAgent($agentArr);
	    if ( !$model ) {
	        $model = new self();
	    }

	    $dataDump = Xapi::dataDumpForDb($agentArr);
	    
	    $model->json_data              = $dataDump['json_data'];
	    $model->hash_data              = $dataDump['hash_data'];
	    $model->objectType             = $agentArr['objectType'];
	    $model->name_json              = Xapi::jsonPropertyForDb($agentArr, 'name');
	    $model->mbox_json              = Xapi::jsonPropertyForDb($agentArr, 'mbox');
	    $model->mbox_sha1sum_json      = Xapi::jsonPropertyForDb($agentArr, 'mbox_sha1sum');
	    $model->openid_json            = Xapi::jsonPropertyForDb($agentArr, 'openid');
	    $model->account_json           = Xapi::jsonPropertyForDb($agentArr, 'account');

	    // @TODO Member is an array of Agents (objects), i.e. an array of arrays
	    // We need to add another model: LrsGroupMember, to save relation between a Group (which is an Agent type) and its members
	    // Save members of this group (if it is a group) in a JSON
	    if (isset($agentArr['member']) && is_array($agentArr['member'])) {
	       $model->member = Xapi::jsonPropertyForDb($agentArr, 'member');
	    }
	    
	    if (!$model->save()) {
	        Xapi::log("Failed to save Agent", CLogger::LEVEL_ERROR);
	        Xapi::log($model->errors, CLogger::LEVEL_ERROR);
	        return null;
	    }
	    
	    return $model;
	    
	}
	
	
	/**
	 * Identify the agent type (Agent, Person or Group)
	 * 
	 * @param array $agentArr Agent information
	 * @return array The same or modified Agent array
	 */
	private static function resolveAgentType($agentArr) {

	    $xapiCode = Xapi::getClientVersionCode();
	    
		// If it is already set, then go back
		if ( isset($agentArr['objectType']) && !empty($agentArr['objectType']) ) {
			return $agentArr;
		}
	
		// Group members listed?
		// SPEC102 4.1.2.2 When the Actor ObjectType is Group
		if ( isset($agentArr['member']) && is_array($agentArr['member']) && (count($agentArr['member']) > 0) ) {
			$agentArr['objectType'] = Xapi::AGENT_TYPE_GROUP;
		}
	
		// A Person is similar to Agents but has multiple identifying properties, i.e. an array of mbox, array of name, etc.
		// SPEC102 7.6 Agent Profile
		else if (is_array($agentArr['name']) || is_array($agentArr['mbox']) || is_array($agentArr['mbox_sha1sum']) || is_array($agentArr['openid']) || is_array($agentArr['account'])) {
		    // Version 0.95+ ?
		    if ($xapiCode >= Xapi::VERSION_CODE_1) {
                $agentArr['objectType'] = Xapi::AGENT_TYPE_PERSON;
		    }
		    // Version 0.9: Agent IFIs are arrays
		    else {
		        $agentArr['objectType'] = Xapi::AGENT_TYPE_AGENT;
		    }
		}
		
		// Agents have single identifying properties (one name string, one mbox string,...)
		// SPEC102 4.1.2 Actor   
		// This should happen only for version 0.95+  (version code 1+)
		else if (is_string($agentArr['name']) || is_string($agentArr['mbox']) || is_string($agentArr['mbox_sha1sum']) || is_string($agentArr['openid']) || is_array($agentArr['account'])) {
			$agentArr['objectType'] = Xapi::AGENT_TYPE_AGENT;
		}
		
		// Version 0.9 define "Person" as having set of specific properties: givenName, familyName and so on
		// Person ? (givenName, etc.)
		foreach ( self::$SPECIFIC_PROPS_V09 as $key ) {
		    $property = isset($agentArr[$key]) ? $agentArr[$key] : null;
		    if ( $property ) {
		        if ( is_array($property) && (count($property) > 0) ) {
		            $agentArr['objectType'] = Xapi::AGENT_TYPE_PERSON;
		            break;
		        }
		    }
		}
		
		
		// If still NOT set, make it... the default..
		if ( !isset($agentArr['objectType']) ) {
			$agentArr['objectType'] = Xapi::AGENT_TYPE_AGENT;
			return $agentArr;
		}
	
		return $agentArr;
	}
	

	/**
	 * 
	 * @param array $agentArr Agent Information
	 */
	private static function findByMbox($agentArr, $versionCode=Xapi::VERSION_CODE_1)  {

	    if (empty($agentArr['mbox'])) {
	        return null;
	    }
	     
	    $crit = new CDbCriteria();
	    
	    if (is_array($agentArr['mbox'])) {
	        foreach ($agentArr['mbox'] as $mbox) {
	            $crit->addCondition("mbox_json LIKE '%" . $mbox ."%'", "OR");
	        }
	    }
	    else {
	        $crit->addCondition("mbox_json LIKE '%" . $agentArr['mbox'] ."%'");
	    }
		
	    if ($versionCode === Xapi::VERSION_CODE_0)
		  $crit->addCondition("xapi_version_code IS NULL");
	    else 
	      $crit->addCondition("xapi_version_code=" . $versionCode);
		
		
		$model = self::model()->find($crit);
		
		return $model;
	}
	
	/**
	 * Find an Agent by Account property 
	 *
	 * @param array $agentArr Agent Information
	 */
	private static function findByAccount($agentArr, $versionCode=Xapi::VERSION_CODE_1)  {

	    if (empty($agentArr['account'])) {
	        return null;
	    }
	    
	    $crit = new CDbCriteria();
	    
	    if ($versionCode === Xapi::VERSION_CODE_0) {
	        foreach ($agentArr['account'] as $account) {
	            $crit->addCondition("account_json LIKE '%\"accountName\":\"". $account['accountName'] . "\"%'", 'OR');
	            $crit->addCondition("REPLACE(account_json,'\\\','') LIKE '%\"accountServiceHomePage\":\"". $account['accountServiceHomePage'] . "\"%'", 'OR');
	        }
	        $crit->addCondition("xapi_version_code IS NULL");
	    }
	    else {
	        $crit->addCondition("account_json LIKE '%\"name\":\"". $agentArr['account']['name'] . "\"%'");
	        $crit->addCondition("REPLACE(account_json,'\\\','') LIKE '%\"homePage\":\"". $agentArr['account']['homePage'] . "\"%'");
	        $crit->addCondition("xapi_version_code=" . $versionCode);
	    }
	    
	    $model = self::model()->find($crit);
	    
	    return $model;
	}
	
	/**
	 * Find an Agent by Open ID URI.
	 * @param array $agentArr Agent Information
	 */
	private  static function findByOpenid($agentArr, $versionCode=Xapi::VERSION_CODE_1)  {

	    if (empty($agentArr['openid'])) {
	        return null;
	    }
	    
	    $crit = new CDbCriteria();
	    
	    if (is_array($agentArr['openid'])) {
	        foreach ($agentArr['openid'] as $openid) {
	            $crit->addCondition(" ( REPLACE(openid_json, '\\\', '') LIKE '%" . $openid . "%' ) ", "OR");
	        }
	    }
	    else {
	        $crit->addCondition(" ( REPLACE(openid_json, '\\\', '') LIKE '%" . $agentArr['openid'] . "%' ) ");
	    }
	    
	    if ($versionCode === Xapi::VERSION_CODE_0)
	        $crit->addCondition("xapi_version_code IS NULL");
	    else
	        $crit->addCondition("xapi_version_code=" . $versionCode);
	     
	    $model = self::model()->find($crit);
	    
	    return $model;
	}
	
	/**
	 * Find an Agent by mbox_sha1_sum.
	 * @param array $agentArr Agent Information
	 */
	private static function findByMboxSha1sum($agentArr, $versionCode=Xapi::VERSION_CODE_1)  {

	    if (empty($agentArr['mbox_sha1sum'])) {
	        return null;
	    }
	     
	    $crit = new CDbCriteria();
	    
	    if (is_array($agentArr['mbox_sha1sum'])) {
	        foreach ($agentArr['mbox_sha1sum'] as $mbox_sha1sum) {
	            $crit->addCondition(" ( mbox_sha1sum_json LIKE '%" . $mbox_sha1sum . "%' ) ", "OR");
	        }
	    }
	    else {
	        $crit->addCondition(" ( mbox_sha1sum_json LIKE '%" . $agentArr['mbox_sha1sum'] . "%' ) ");
	    }
	    
	    
	    if ($versionCode === Xapi::VERSION_CODE_0)
	        $crit->addCondition("xapi_version_code IS NULL");
	    else
	        $crit->addCondition("xapi_version_code=" . $versionCode);
	     
	    $model = self::model()->find($crit);
	    
	    return $model;
	}
	
	
	/**
	 * Detect if the object (dataArr) is most probably an Agent
	 * 
	 * @param array $dataArr
	 * @return boolean
	 */
	public static function isAgent($dataArr) {
	    
	    if (is_array($dataArr)) 
	       return !empty(array_intersect(array_keys($dataArr), self::$SPECIFIC_PROPS, self::$SPECIFIC_PROPS_V09));
	    
	    return false;
	}
	

	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
	    $this->xapi_version_code = Xapi::getClientVersionCode();
	    return parent::beforeSave();
	}

	
	
	/**
	 *
	 * @param array $user_data  User Information extracted from LMS Database
	 */
	public static function makeActorFromLmsUserData($user_data) {
	    
	    $xapiVersionCode = Xapi::getClientVersionCode();
	    
	    $agentArr = array();
	    
	    if ($xapiVersionCode >= Xapi::VERSION_CODE_1) {
	        $agentArr['mbox'] = "mailto:" . $user_data['email'];
	        $agentArr['name'] = $user_data['firstname'] . " " . $user_data['lastname'];
	        $agentArr['account'] = array('homePage' => Docebo::getRootBaseUrl(true), 'name' => $user_data['idst']);
	    }
	    else {
	        $agentArr['mbox'] = array("mailto:" . $user_data['email']);
	        $agentArr['name'] = array($user_data['firstname'] . " " . $user_data['lastname']);
	        $agentArr['account'] = array(array('accountServiceHomePage' => Docebo::getRootBaseUrl(true), 'accountName' => $user_data['idst']));
	    }
	    
	    return $agentArr;
	}
	
	
	/**
	 * Convert an Agent to an Expanded Agent Object (Person) (Spec 1.0.2 page 48)
	 * 
	 * @param unknown $agentArr
	 */
	public static function toExpandedAgent($agentArr) {
		$object = array();
		$object['objectType'] 		= Xapi::OBJ_TYPE_PERSON;
		$object['name'] 			= isset($agentArr['name']) ? array($agentArr['name']) : array();
		$object['mbox'] 			= isset($agentArr['mbox']) ? array($agentArr['mbox']) : array();
		$object['mbox_sha1sum'] 	= isset($agentArr['mbox_sha1sum']) ? array($agentArr['mbox_sha1sum']) : array();
		$object['openid'] 			= isset($agentArr['openid']) ? array($agentArr['openid']) : array();
		$object['account'] 			= isset($agentArr['account']) ? array($agentArr['account']) : array();
		return $object;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function toMinimalArr() {
		
		$agent = array();
		$agent['objectType'] = $this->objectType;
		$noIfi = true;
		
		if ($this->mbox_json) {
			$agent['mbox'] = $this->mbox_json;
			$noIfi = false;
		}
		else if ($this->mbox_sha1sum_json) {
			$agent['mbox_sha1sum_json'] = $this->mbox_sha1sum_json;
			$noIfi = false;
		}
		else if ($this->openid_json) {
			$agent['openid'] = $this->openid_json;
			$noIfi = false;
		}
		else if ($this->account_json) {
			$agent['account'] = $this->account_json;
			$noIfi = false;
		}

		// @TODO  : Include members for Anonymous groups (having no IFI)
		
		return $agent;
		
		
	}

	/**
	 * Convert Context to currently MAX supported version
	 *
	 * @param array $dataArr
	 * @return array The modified input array
	 */
	public static function convertFrom09ToCurrentMax($dataArr) {
	    
		$keepProps = array('objectType');
		$ifiArray = array('mbox', 'mbox_sha1sum', 'openid', 'account');
		// Use the first IFI found from the above array, checked in that very order
		foreach ($ifiArray as $ifi) {
			if (!empty($dataArr[$ifi]) && is_array($dataArr[$ifi])) {
				
				$tmp = $dataArr[$ifi][0];
				if ($ifi == "account") {
					$tmp['homePage'] 	= $tmp['accountServiceHomePage'];
					$tmp['name']		= $tmp['accountName'];
					unset($tmp['accountServiceHomePage']);
					unset($tmp['accountName']);
				}
				$dataArr[$ifi] = $tmp;
				
				$keepProps[] = $ifi;
				break;
			}
			else if (!empty($dataArr[$ifi])) {
			    $keepProps[] = $ifi;
			}
		}
		
		// Use the first "name", if available
		if (!empty($dataArr['name']) && is_array($dataArr['name'])) {
			$dataArr['name'] = $dataArr['name'][0];
			$keepProps[] = "name";
		}
		else if (!empty($dataArr['name'])) {
		    $keepProps[] = "name";
		}
		
		// Keep only the properties collected in the $keepProps array
		// like "mbox" and "name", or  "account" and "name"
		foreach ($dataArr as $k => $v) {
			if (!in_array($k, $keepProps)) {
				unset($dataArr[$k]);
			}
		}
		
		
		return $dataArr;
	}
	
	
}
