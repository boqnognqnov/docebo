<?php

/**
 * This is the model class for table "lrs_statement_attachment".
 *
 * The followings are the available columns in table 'lrs_statement_attachment':
 * @property integer $id
 * @property integer $id_statement
 * @property string $usageType
 * @property string $contentType
 * @property integer $length
 * @property string $sha2
 * @property string $fileUrl
 * @property string $description
 * @property string $display
 * @property string $payloadContent
 * @property string $payloadFilePath
 *
 * The followings are the available model relations:
 * @property LrsStatement $idStatement
 */
class LrsStatementAttachment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_statement_attachment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_statement, usageType, contentType, length, sha2, display, payloadFilePath', 'required'),
			array('id_statement, length', 'numerical', 'integerOnly'=>true),
			array('usageType, fileUrl', 'length', 'max'=>2000),
			array('contentType, sha2', 'length', 'max'=>256),
			array('payloadFilePath', 'length', 'max'=>2048),
			array('description, payloadContent', 'safe'),
			// The following rule is used by search().
			array('id, id_statement, usageType, contentType, length, sha2, fileUrl, description, display, payloadContent, payloadFilePath', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idStatement' => array(self::BELONGS_TO, 'LrsStatement', 'id_statement'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_statement' => 'Id Statement',
			'usageType' => 'Usage Type',
			'contentType' => 'Content Type',
			'length' => 'Length',
			'sha2' => 'Sha2',
			'fileUrl' => 'File Url',
			'description' => 'Description',
			'display' => 'Display',
			'payloadContent' => 'Payload Content',
			'payloadFilePath' => 'Payload File Path',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_statement',$this->id_statement);
		$criteria->compare('usageType',$this->usageType,true);
		$criteria->compare('contentType',$this->contentType,true);
		$criteria->compare('length',$this->length);
		$criteria->compare('sha2',$this->sha2,true);
		$criteria->compare('fileUrl',$this->fileUrl,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('display',$this->display,true);
		$criteria->compare('payloadContent',$this->payloadContent,true);
		$criteria->compare('payloadFilePath',$this->payloadFilePath,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsStatementAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
