<?php

/**
 * This is the model class for table "lrs_context".
 *
 * The followings are the available columns in table 'lrs_context':
 * @property integer $id_context
 * @property string $json_data
 * @property string $hash_data
 * @property integer $id_parent
 * @property integer $id_grouping
 * @property integer $id_other
 * @property string $registration_id
 * @property integer $id_instructor
 * @property integer $id_team
 * @property string $revision
 * @property string $platform
 * @property string $language
 * @property integer $id_statement
 * @property string $extensions_json
 *
 * The followings are the available model relations:
 * @property contextActivities[] $lrsContextActivities
 */
class LrsContext extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_context';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data', 'required'),
			array('id_parent, id_grouping, id_other, id_instructor, id_team, id_statement', 'numerical', 'integerOnly'=>true),
			array('hash_data, registration_id, extensions_json', 'length', 'max'=>255),
			array('revision', 'length', 'max'=>64),
			array('platform', 'length', 'max'=>200),
			array('language', 'length', 'max'=>8),
			// The following rule is used by search().
			array('id_context, json_data, hash_data, id_parent, id_grouping, id_other, registration_id, id_instructor, id_team, revision, platform, language, id_statement, extensions_json', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'contextActivities' => array(self::HAS_MANY, 'LrsContextActivities', 'id_context'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_context' => 'Id Context',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'id_parent' => 'Id Parent',
			'id_grouping' => 'Id Grouping',
			'id_other' => 'Id Other',
			'registration_id' => 'Registration',
			'id_instructor' => 'Id Instructor',
			'id_team' => 'Id Team',
			'revision' => 'Revision',
			'platform' => 'Platform',
			'language' => 'Language',
			'id_statement' => 'Id Statement',
			'extensions_json' => 'Extensions Json',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_context',$this->id_context);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('id_parent',$this->id_parent);
		$criteria->compare('id_grouping',$this->id_grouping);
		$criteria->compare('id_other',$this->id_other);
		$criteria->compare('registration_id',$this->registration_id,true);
		$criteria->compare('id_instructor',$this->id_instructor);
		$criteria->compare('id_team',$this->id_team);
		$criteria->compare('revision',$this->revision,true);
		$criteria->compare('platform',$this->platform,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('id_statement',$this->id_statement);
		$criteria->compare('extensions_json',$this->extensions_json,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsContext the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public static function storeContext($dataArr) {
	    
	    /* @var $model LrsContext */
	    
	    XapiValidator::validateContext($dataArr);
	    
	    $model = self::findContextByHash($dataArr);
	    if (!$model) {
	        $model = new self();
	    }

	    $dataDump = Xapi::dataDumpForDb($dataArr);
	    
	    $tmpModel = LrsAgent::storeAgent(@$dataArr['instructor']);
	    $id_instructor = $tmpModel ? $tmpModel->id_agent : null;
	    
	    $tmpModel = LrsAgent::storeAgent(@$dataArr['team']);
	    $id_team = $tmpModel ? $tmpModel->id_agent : null;
	    
	    // If the context is another statement
	    $id_statement = null;
	    if (isset($dataArr['statement']) && isset($dataArr['statement']['objectType']) && $dataArr['statement']['objectType'] == Xapi::OBJ_TYPE_STATEMENT_REF) {
			$statementId = $dataArr['statement']['id'];
			$statementModel = LrsStatement::model()->findByAttributes(array('uuid' => $statementId)); 		    	
			if ($statementId) {
				$id_statement = $statementId->id_statement;
			}
	    }
	    
	    // Prepare some fields
	    $registration_id = isset($dataArr['registration']) ? $dataArr['registration'] : null;
	    
	    $model->json_data          = $dataDump['json_data'];
	    $model->hash_data          = $dataDump['hash_data'];
	    $model->registration_id    = $registration_id;
	    $model->id_parent          = $id_parent;
	    $model->id_grouping        = $id_grouping;
	    $model->id_other           = $id_other;
	    $model->id_instructor      = $id_instructor;
	    $model->id_team            = $id_team;
	    $model->revision           = @$dataArr['revision'];
	    $model->platform           = @$dataArr['platform'];
	    $model->language           = @$dataArr['language'];
	    $model->id_statement       = $id_statement;
	    $model->extensions_json    = CJSON::encode(@$dataArr['extensions']);
	     
	    if (!$model->save()) {
	        return null;
	    }
	    
	    // Now that we have the CONTEXT model, lets handle context activities, which require Context ID from DB table
	    if (isset($dataArr["contextActivities"])) {
	        $result = LrsContextActivities::storeActivities($model->id_context, $dataArr["contextActivities"]);
	    }
	    
	    return $model;
	     
	}
	
	
	/**
	 * Find a context in Context table, by hash_data
	 * 
	 * @param array $dataArr
	 */
	public static function findContextByHash($dataArr) {
	    $hash_data = self::hash($dataArr);
	    if ( empty($hash_data) )  {
	        return null;
	    }
	    return self::model()->findByAttributes(array('hash_data' => $hash_data));
	}

	
	/**
	 * 
	 * @param unknown $dataArr
	 */
	public static function hash($dataArr) {
	    return sha1(CJSON::encode($dataArr));
	}
	
	
	/**
	 * Convert Context to currently MAX supported version
	 * 
	 * @param array $dataArr
	 * @return array The modified input array
	 */
	public static function convertFrom09ToCurrentMax($dataArr) {
		if (!empty($dataArr['contextActivities'])) {
			if (!empty($dataArr['contextActivities']['parent'])) {
				$dataArr['contextActivities']['parent'] = LrsActivity::convertFrom09ToCurrentMax($dataArr['contextActivities']['parent']);
			}
			if (!empty($dataArr['contextActivities']['grouping'])) {
				$dataArr['contextActivities']['grouping']= LrsActivity::convertFrom09ToCurrentMax($dataArr['contextActivities']['grouping']);
			}
			if (!empty($dataArr['contextActivities']['other'])) {
				$dataArr['contextActivities']['other']= LrsActivity::convertFrom09ToCurrentMax($dataArr['contextActivities']['other']);
			}
		}
		return $dataArr;
	}
	
	
	
}
