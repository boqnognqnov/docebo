<?php

/**
 * This is the model class for table "lrs_state".
 *
 * The followings are the available columns in table 'lrs_state':
 * @property integer $id_state
 * @property string $json_data
 * @property string $hash_data
 * @property string $state_id
 * @property integer $id_agent
 * @property integer $id_activity
 * @property string $state
 * @property string $registration_id
 * @property string $updated
 * @property string $etag
 * @property string $content_type
 */
class LrsState extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_state';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('json_data, hash_data, state_id', 'required'),
			array('id_agent, id_activity', 'numerical', 'integerOnly'=>true),
			array('hash_data, content_type', 'length', 'max'=>255),
			array('state_id, registration_id', 'length', 'max'=>200),
			array('etag', 'length', 'max'=>128),
			array('updated', 'safe'),
			// The following rule is used by search().
			array('id_state, json_data, hash_data, state_id, id_agent, id_activity, state, registration_id, updated, etag, content_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_state' => 'Id State',
			'json_data' => 'Json Data',
			'hash_data' => 'Hash Data',
			'state_id' => 'State',
			'id_agent' => 'Id Agent',
			'id_activity' => 'Id Activity',
			'state' => 'State',
			'registration_id' => 'Registration',
			'updated' => 'Updated',
			'etag' => 'Etag',
			'content_type' => 'Content Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_state',$this->id_state);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('hash_data',$this->hash_data,true);
		$criteria->compare('state_id',$this->state_id,true);
		$criteria->compare('id_agent',$this->id_agent);
		$criteria->compare('id_activity',$this->id_activity);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('registration_id',$this->registration_id,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('etag',$this->etag,true);
		$criteria->compare('content_type',$this->content_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsState the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	

	/**
	 * Return Activity 'state' by state ID
	 */
	public static function findState($agentArr, $activityId, $stateId=null, $registration=null, $since=null) {
	
		$agentModel = LrsAgent::findAgent($agentArr);
		
		if (!$agentModel) return null;
		
		// Simple Activity array
		$activityArr = LrsActivity::getSimpleActivityArray($activityId); 
		$activityModel = LrsActivity::findActivity($activityArr);
		
		if (!$activityModel) return null;
		
		
		$crit = new CDbCriteria();
		$crit->params = array();
		
		$crit->addCondition("(id_agent=:idAgent) AND (id_activity=:idActivity)");
		$crit->params[':idAgent'] = $agentModel->id_agent;
		$crit->params[':idActivity'] = $activityModel->id_activity;
		
		if ($stateId) {
		    $crit->addCondition("state_id=:stateId");
		    $crit->params[':stateId'] = $stateId;
		}
		
		if ($registration) {
			$crit->addCondition("registration_id=:idReg");
			$crit->params[':idReg'] = $registration;
		}
		
		if ($since) {
		    $crit->addCondition("updated > :since");
		    $crit->params[':since'] = $since;
		}
		
		
		if ($stateId) {
		  $result = self::model()->find($crit);
		}
		else {
		  $result = self::model()->findAll($crit);
		}
		
		return $result;
		
	}
	
	
	/**
	 * Return Activity 'state' by state ID
	 */
	public static function getState($stateId, $agentArr, $activityId, $registration=null) {
	    
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeStateOperation(XapiAuth::OPERATION_READ, $agentArr, $activityId, $registration) ) {
	        Xapi::log('Unauthorized State READ operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot READ State.");
	    }
	     
	    
		$model = self::findState($agentArr, $activityId, $stateId, $registration);
		if (!$model) return "";
		return $model->state;
	}
	
	/**
	 * Return Activity 'states', filtered by the context
	 */
	public static function getStates($agentArr, $activityId, $registration, $since=null) {
	    
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeStateOperation(XapiAuth::OPERATION_READ, $agentArr, $activityId, $registration) ) {
	        Xapi::log('Unauthorized State READ operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot READ State.");
	    }
	     
	    
	    $result = array();
	    $models = self::findState($agentArr, $activityId, null, $registration, $since);
	    if (!empty($models)) {
	        foreach ($models as $model) {
	            $result[] = $model->state_id;
	        }
	    }
	    return $result;
	}
	
	
	/**
	 * Delete State or States
	 *
	 */
	public static function deleteState($agentArr, $activityId, $stateId=null, $registration=null) {
	    
	    
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeStateOperation(XapiAuth::OPERATION_WRITE, $agentArr, $activityId, $registration) ) {
	        Xapi::log('Unauthorized State WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot WRITE State.");
	    }
	     
	    
	    $agentModel = LrsAgent::findAgent($agentArr);
	    
	    if (!$agentModel) return null;
	    
	    // Simple Activity array
	    $activityArr = LrsActivity::getSimpleActivityArray($activityId);
	    $activityModel = LrsActivity::findActivity($activityArr);
	    
	    if (!$activityModel) return null;
	    
	    $crit = new CDbCriteria();
	    $crit->params = array();
	    
	    $crit->addCondition("(id_agent=:idAgent) AND (id_activity=:idActivity)");
	    $crit->params[':idAgent'] = $agentModel->id_agent;
	    $crit->params[':idActivity'] = $activityModel->id_activity;
	    
	    if ($stateId) {
	        $crit->addCondition("state_id=:stateId");
	        $crit->params[':stateId'] = $stateId;
	    }
	    
	    if ($registration) {
	        $crit->addCondition("registration_id=:idReg");
	        $crit->params[':idReg'] = $registration;
	    }
	    
	    $result = self::model()->deleteAll($crit);
	    
	    return $result;
	    
	}
	
	/**
	 * Create or update an Activity State
	 *
	 * @return LrsState
	 */
	public static function storeState($data, $agentArr, $activityId, $stateId, $state, $registration=null) {
	
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeStateOperation(XapiAuth::OPERATION_WRITE, $agentArr, $activityId, $registration) ) {
	        Xapi::log('Unauthorized State WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot write State.");
	    }
	    
		if (empty($activityId) || empty($agentArr)) {
			Xapi::out(Xapi::HTTP_BAD_REQUEST, 'Missing activityId or agent data');
		}
	
		// Make a simple Activity object and attempt to store its data (create or update)
		$activityArr = LrsActivity::getSimpleActivityArray($activityId);
		$activityModel = LrsActivity::storeActivity($activityArr);
		if (!$activityModel) {
		    Xapi::out(Xapi::HTTP_SERVER_ERROR, "Failed to store Activity");
		}
		 
		// Try to save/create/update the agent
		$agentModel = LrsAgent::storeAgent($agentArr);
		if (!$agentModel) {
		    Xapi::out(Xapi::HTTP_SERVER_ERROR, "Failed to store Agent");
		}
		
		
		$stateModel = self::saveState($state, $agentModel->id_agent, $activityModel->id_activity, $registration, $stateId, $data);
		
		if (!$stateModel) return false;
		
		return true;
		
	}
	

	private static function saveState($state, $id_agent, $id_activity, $registration, $stateId, $data) {
	    
	    $attributes = array(
	        'id_agent'         => $id_agent,
	        'id_activity'      => $id_activity,
	    );
	    
	    if (!empty($registration)) $attributes['registration_id']  = $registration;
	    if (!empty($stateId))      $attributes['state_id']         = $stateId;
	    
	    $model = self::model()->findByAttributes($attributes);
	    
	    if (!$model) {
	        $model = new self();
	    }
	    
	    $dataDump = Xapi::dataDumpForDb($state);
	     
	    $model->json_data              = $dataDump['json_data'];
	    $model->hash_data              = $dataDump['hash_data'];
	     
        if (!empty($stateId)) $model->state_id = $stateId;	    
        if (!empty($registration)) $model->registration_id = $registration;
	    $model->id_agent = $id_agent;
	    $model->id_activity = $id_activity;
        $model->state = $state;
	    $model->updated = date('Y-m-d H:i:s');
	    
	    if (!$model->save()) {
	        Xapi::log("Failed to save State", CLogger::LEVEL_ERROR);
	        Xapi::log($model->errors, CLogger::LEVEL_ERROR);
	        return null;
	    }
	     
	    return $model;
	     
	    
	}
	
}
