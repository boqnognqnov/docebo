<?php

/**
 * This is the model class for table "lrs_profile".
 *
 * The followings are the available columns in table 'lrs_profile':
 * @property integer $id_profile
 * @property string $profile_id
 * @property integer $id_activity
 * @property integer $id_actor
 * @property string $updated
 * @property string $contents
 * @property string $etag
 * @property string $content_type
 * @property integer $xapi_version_code
 */
class LrsProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profile_id, updated', 'required'),
			array('id_activity, id_actor, xapi_version_code', 'numerical', 'integerOnly'=>true),
			array('profile_id, content_type', 'length', 'max'=>255),
			array('etag', 'length', 'max'=>128),
			array('contents', 'safe'),
			// The following rule is used by search().
			array('id_profile, profile_id, id_activity, id_actor, updated, contents, etag, content_type, xapi_version_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_profile' => 'Id Profile',
			'profile_id' => 'Profile',
			'id_activity' => 'Id Activity',
			'id_actor' => 'Id Actor',
			'updated' => 'Updated',
			'contents' => 'Contents',
			'etag' => 'Etag',
			'content_type' => 'Content Type',
		    'xapi_version_code' => 'Xapi Version Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id_profile',$this->id_profile);
		$criteria->compare('profile_id',$this->profile_id,true);
		$criteria->compare('id_activity',$this->id_activity);
		$criteria->compare('id_actor',$this->id_actor);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('contents',$this->contents,true);
		$criteria->compare('etag',$this->etag,true);
		$criteria->compare('content_type',$this->content_type,true);
		$criteria->compare('xapi_version_code',$this->xapi_version_code);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * 
	 * @param unknown $activity_id
	 * @param unknown $actorArr
	 * @param unknown $profile_id
	 * @param unknown $data
	 * @throws XapiException
	 */
	public static function storeProfile($activity_id, $actorArr, $profile_id, $data) {
		
		
		// AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeProfileOperation(XapiAuth::OPERATION_WRITE, $activity_id, $actorArr) ) {
	        Xapi::log('Unauthorized Profile WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot write Profile.");
	    }
	    
		// Profile contents is FREE FORM !!!
		$contents = !empty($data['json']) ? $data['json'] : $data['document'];
		
		$activity_model = LrsActivity::model()->findByAttributes(array('activity_id' => $activity_id));
		$agent_model 	= LrsAgent::findAgent($actorArr);

		// LRS doesn't know about this Activity or Actor; That is ok. We should CREATE them 
		$model = $activity_model ? $activity_model : ($agent_model ? $agent_model : null);
		
		if (!$model) {
			if ($activity_id) {
				$activityArr = LrsActivity::getSimpleActivityArray($activityId);
				$model = LrsActivity::storeActivity($activityArr);
			}
			else if ($actorArr) {
				$model = LrsAgent::storeAgent($actorArr);
			}
		}
		
		if (!$model) {
			return null;
		}

		// Since this function is 2-in-1....
		$id_activity = $activity_id ? $model->id_activity : null;
		$id_actor = $actorArr ? $model->id_agent : null;
		
		
		$model = self::saveProfile($id_activity, $id_actor, $profile_id, $contents, $data);
		
		if (!$model) {
			throw new XapiException('Failed to save object profile.', Xapi::HTTP_SERVER_ERROR);
		}
		
	}
	


	/**
	 * 
	 * @param unknown $id_activity
	 * @param unknown $id_actor
	 * @param unknown $profile_id
	 * @param unknown $contents
	 * @throws TcapiException
	 * @return static
	 */
	private static function saveProfile($id_activity, $id_actor, $profile_id, $contents, $data) {
	
		$crit = new CDbCriteria();
		$params = array();
		
		if ($id_activity) {
			$crit->addCondition('id_activity=:id_activity');
			$params[':id_activity'] = $id_activity;
		}
		else {
			$crit->addCondition('id_actor=:id_actor');
			$params[':id_actor'] = $id_actor;
		}
		
		$crit->addCondition('profile_id=:profile_id');
		$params[':profile_id'] = $profile_id;
		
		$crit->params = $params;
		
		$profileModel = self::model()->find($crit); 

		if (!$profileModel) {
			$profileModel = new self();
		}
		
		// Respect If-Match/If-None-Match
		if ($data['method'] == Xapi::METHOD_PUT && (Xapi::getClientVersionCode() >= Xapi::VERSION_CODE_1)) {
		  Xapi::checkETag($profileModel->contents, $contents, $data);
		}
		
		$profileModel->profile_id 		= $profile_id;
		$profileModel->id_activity		= $id_activity;
		$profileModel->id_actor			= $id_actor;
		$profileModel->updated			= date('Y-m-d H:i:s');
		$profileModel->contents			= $contents;
		$profileModel->etag				= Xapi::makeETag($contents);
		$profileModel->content_type		= "";

		if (!$profileModel->save()) {
			return null;
		}
		
		return $profileModel;
	
	}
	
	/**
	 * 
	 * @param unknown $activity_id
	 * @param unknown $actorArr
	 * @param string $profile_id
	 * @param string $since
	 */
	public static function getProfiles($activity_id, $actorArr, $profile_id=null, $since=null) {
	    
		// AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeProfileOperation(XapiAuth::OPERATION_READ, $activity_id, $actorArr) ) {
	        Xapi::log('Unauthorized Profile READ operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot Read Profile.");
	    }
	     
	    
		$activityArr = LrsActivity::getSimpleActivityArray($activity_id);
		$activity_model = LrsActivity::findActivity($activityArr);
		$agent_model 	= LrsAgent::findAgent($actorArr);
		
		// LRS doesn't know about this Activity or Actor; That is ok. We should CREATE them
		$model = $activity_model ? $activity_model : ($agent_model ? $agent_model : null);
		
		if (!$model) {
			if ($activity_id) {
				$activityArr = LrsActivity::getSimpleActivityArray($activityId);
				$model = LrsActivity::storeActivity($activityArr);
			}
			else if ($actorArr) {
				$model = LrsAgent::storeAgent($actorArr);
			}
		}
		if (!$model) {
			return null;
		}
		
		// Since this function is 2-in-1....
		$id_activity = $activity_id ? $model->id_activity : null;
		$id_actor = $actorArr ? $model->id_agent : null;
		
		$crit = new CDbCriteria();
		$params = array();
		
		if ($id_activity) {
			$crit->addCondition('id_activity=:id_activity');
			$params[':id_activity'] = $id_activity;
		}
		else {
			$crit->addCondition('id_actor=:id_actor');
			$params[':id_actor'] = $id_actor;
		}

		if ($since) {
			$crit->addCondition('updated > :since');
			$params[':since'] = $since;
		}
		
		if ($profile_id) {
			$crit->addCondition('profile_id=:profile_id');
			$params[':profile_id'] = $profile_id;
		}
		
		// XAPI version code		
		$crit->addCondition('xapi_version_code=:xapi_version_code');
		$params[':xapi_version_code'] = Xapi::getClientVersionCode();
		
		$crit->params = $params;
		
		$crit->order = "updated ASC";
		
		$profileModels = self::model()->findAll($crit);
		
		$result = array('success' => false, 'etag' => '' );
		$output = array();

		if (!empty($profileModels)) {
			// Looking for exact Object and Profile IDs (result = document contents)
			if ( ($activity_id || $actorArr) && $profile_id ) {
				$profileModel = $profileModels[0];
				$output = $profileModel->contents;
				$result['success'] = true;
				$result['data'] = $output;
				$result['etag'] = Xapi::makeETag($profileModel->contents);
			}
			else if ( $activity_id || $actorArr ) {
				foreach ($profileModels as $profileModel) {
					$output[] = $profileModel->profile_id;
				}
				$result['success'] = true;
				$result['data'] = $output;
				$result['etag'] = ""; // ?????? no idea // @TODO decide !
			}
		}
		
		return $result;
	
	}
	

	/**
	 * 
	 * @param unknown $agentArr
	 * @param unknown $profile_id
	 */
	public static function deleteAgentProfile($agentArr, $profileId) {
		
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeProfileOperation(XapiAuth::OPERATION_WRITE, null, $agentArr) ) {
	        Xapi::log('Unauthorized WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot WRITE Profile.");
	    }
	    
		$model 	= LrsAgent::findAgent($agentArr);
		
		if (!$model) {
			Xapi::out(Xapi::HTTP_BAD_REQUEST, "Agent/Actor not found");
		}
		
		$crit = new CDbCriteria();
		$params = array();
		
		$crit->addCondition('id_actor=:id_actor');
		$params[':id_actor'] = $model->id_agent;
		
		$crit->addCondition('profile_id=:profile_id');
		$params[':profile_id'] = $profileId;
		
		self::model()->deleteAll($crit);
		
	}
	
	
	/**
	 *
	 * @param unknown $agentArr
	 * @param unknown $profile_id
	 */
	public static function deleteActivityProfile($activityId, $profileId) {
	    
	    // AUTHORIZE this operation
	    if ( !XapiAuth::getInstance()->authorizeProfileOperation(XapiAuth::OPERATION_WRITE, $activityId) ) {
	        Xapi::log('Unauthorized WRITE operation.', CLogger::LEVEL_ERROR);
	        Xapi::out(Xapi::HTTP_UNAUTHORIZED, "User unauthorized scope: cannot WRITE Profile.");
	    }
	     
	    
	    $simpleActivityArr = LrsActivity::getSimpleActivityArray($activityId);
	    $model = LrsActivity::findActivity($simpleActivityArr);
	    
	    if (!$model) {
	        Xapi::out(Xapi::HTTP_BAD_REQUEST, "Activity not found");
	    }

	    $crit = new CDbCriteria();
	    $params = array();
	    
	    $crit->addCondition('id_activity=:id_activity');
	    $params[':id_activity'] = $model->id_activity;
	    
	    $crit->addCondition('profile_id=:profile_id');
	    $params[':profile_id'] = $profileId;
	    
	    $crit->params = $params;
	    
	    self::model()->deleteAll($crit);
	
	}
	
	
	
	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
	    $this->xapi_version_code = Xapi::getClientVersionCode();
	    return parent::beforeSave();
	}
	
}
