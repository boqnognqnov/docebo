<?php

/**
 * This is the model class for table "lrs_context_activities".
 *
 * The followings are the available columns in table 'lrs_context_activities':
 * @property integer $id
 * @property integer $id_context
 * @property integer $id_activity
 * @property string $context_type
 *
 * The followings are the available model relations:
 * @property LrsContext $idContext
 * @property LrsActivity $idActivity
 */
class LrsContextActivities extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_context_activities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_context, id_activity, context_type', 'required'),
			array('id_context, id_activity', 'numerical', 'integerOnly'=>true),
			array('context_type', 'length', 'max'=>32),
			// The following rule is used by search().
			array('id, id_context, id_activity, context_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idContext' => array(self::BELONGS_TO, 'LrsContext', 'id_context'),
			'idActivity' => array(self::BELONGS_TO, 'LrsActivity', 'id_activity'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_context' => 'Id Context',
			'id_activity' => 'Id Activity',
			'context_type' => 'Context Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_context',$this->id_context);
		$criteria->compare('id_activity',$this->id_activity);
		$criteria->compare('context_type',$this->context_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsContextActivities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	

	/**
	 * 
	 * @param unknown $idContext
	 * @param unknown $dataArr
	 */
	public static function storeActivities($idContext, $dataArr) {
	    
	    if (!$dataArr) return null;

	    foreach (Xapi::$CONTEXT_ACTIVITY_TYPES as $type) {
	        self::storeContextActivityType($idContext, $type, $dataArr);
	    }
	    
	    
	}
	
	
	/**
	 * 
	 * @param unknown $idContext
	 * @param unknown $type
	 * @param unknown $dataArr
	 */
	private static function storeContextActivityType($idContext, $type, $dataArr) {
	    
	    if (isset($dataArr[$type])) {
	         
	        $activities = $dataArr[$type];
	         
	        // Single parent specified? Make it an array of parents
	        if (isset($activities['id'])) {
	            $activities = array($activities);
	        }
	        
	        // We've got an array of parents? Ok, lets do this..
	        if (is_array($activities)) {
	            
	            foreach ($activities as $activity) {
	                
	                // Of course, we need Activity ID
	                if (isset($activity['id'])) {
	                    
	                    $tmpModel = LrsActivity::storeActivity($activity);
	                     
	                    // We've got an Activity model? Ok, finally, lets create the many-many record
	                    if ($tmpModel) {
	                        
	                        // Lets see if such a record exists (for this context, activity, of the same type)
	                        $caModel = self::model()->findByAttributes(array(
	                            'id_context'    => (int) $idContext,
	                            'id_activity'   => $tmpModel->id_activity,
	                            'context_type'  => $type,
	                        ));
	                        if (!$caModel) {
	                            $caModel = new self();
	                        }
	                        $caModel->id_context    = $idContext;
	                        $caModel->id_activity   = $tmpModel->id_activity;
	                        $caModel->context_type  = $type;
	                        if (!$caModel->save()) {
	                            Xapi::log('Failed to save context activity information', CLogger::LEVEL_ERROR);
	                            Xapi::log('Context ID:' . $idContext, CLogger::LEVEL_ERROR);
	                            Xapi::log('Activity ID:' . $tmpModel->id_activity, CLogger::LEVEL_ERROR);
	                            Xapi::log('Context type:' . $type, CLogger::LEVEL_ERROR);
	                        }
	                    }
	                    
	                }
	                
	            }
	            
	        }
	    }
	     
	    
	}
	
	
}
