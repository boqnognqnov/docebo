<?php

/**
 * This is the model class for table "lrs_verb".
 *
 * The followings are the available columns in table 'lrs_verb':
 * @property integer $id
 * @property string $verb_id
 * @property string $display
 *
 * The followings are the available model relations:
 * @property LrsStatement[] $lrsStatements
 */
class LrsVerb extends CActiveRecord
{
	//@TODO Enrich the list of this map
	private static $VERBID_TO_COMMONTRACK_STATUS_MAP = array(
		"http://adlnet.gov/expapi/verbs/attempted"			=> LearningCommontrack::STATUS_ATTEMPTED,
	    "http://adlnet.gov/expapi/verbs/resumed"			=> LearningCommontrack::STATUS_ATTEMPTED,
		"http://adlnet.gov/expapi/verbs/completed"			=> LearningCommontrack::STATUS_COMPLETED,
		"http://adlnet.gov/expapi/verbs/failed"				=> LearningCommontrack::STATUS_FAILED,
		"http://adlnet.gov/expapi/verbs/passed"				=> LearningCommontrack::STATUS_COMPLETED,

		// V 0.9
		"attempted"											=> LearningCommontrack::STATUS_ATTEMPTED,
		"completed"											=> LearningCommontrack::STATUS_COMPLETED,
		"failed"											=> LearningCommontrack::STATUS_FAILED,
		"passed"											=> LearningCommontrack::STATUS_COMPLETED,
	    "resumed"                                           => LearningCommontrack::STATUS_ATTEMPTED,
			
	);
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lrs_verb';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('verb_id', 'required'),
			array('verb_id', 'length', 'max'=>2000),
			array('display', 'safe'),
			// The following rule is used by search().
			array('id, verb_id, display', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lrsStatements' => array(self::HAS_MANY, 'LrsStatement', 'id_verb'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'verb_id' => 'Verb',
			'display' => 'Display',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('verb_id',$this->verb_id,true);
		$criteria->compare('display',$this->display,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LrsVerb the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * 
	 * @param unknown $verb
	 */
	public static function storeVerb($verb) {
	    
	    if (isset($verb['id'])) {
	        $id = $verb['id'];
	    }
	    else {
	        $id = $verb;  // 0.9
	    }
	     
	    $model = self::findVerb($verb);
	    if (!$model) {
	        $model = new self();
	    }
	    
	    $display = isset($verb['display']) ? $verb['display'] : "";
	    
	    $model->verb_id = $id;
	    $model->display = CJSON::encode($display);
	    
	    if (!$model->save()) {
	        Yii::log('Verb model errors:', CLogger::LEVEL_ERROR);
	        Yii::log($model->errors, CLogger::LEVEL_ERROR);
	        return null;
	    }
	    
	    return $model;
	    
	}
	
	/**
	 * 
	 * @param unknown $verb
	 */
	public static function findVerb($verb) {
	    
	    if (isset($verb['id'])) {
	        $id = $verb['id'];
	    }
	    else {
	        $id = $verb;  // 0.9
	    }
	    
	    $crit = new CDbCriteria();
	    $crit->addCondition("verb_id=:id");
	    $crit->params[':id'] = $id; 
	    $model = self::model()->find($crit);
	    
	    return $model;
	    
	}

	
	/**
	 * Convert/translate v1.x XAPI Verb (IRI) to LMS specific common tracking status
	 * 
	 * @param striong $verbId
	 * @return multitype:string |boolean
	 */
	public static function verbIdToCommontrackStatus($verbId, $default = false) {
		
		if (isset(self::$VERBID_TO_COMMONTRACK_STATUS_MAP[$verbId])) return self::$VERBID_TO_COMMONTRACK_STATUS_MAP[$verbId];
		
		if ($default !== false) {
			return $default;
		}
		else {
			return LearningCommontrack::STATUS_ATTEMPTED;			
		}
		
	}

	
	
}
