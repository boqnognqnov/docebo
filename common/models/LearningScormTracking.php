<?php

/**
 * This is the model class for table "learning_scorm_tracking".
 *
 * The followings are the available columns in table 'learning_scorm_tracking':
 * @property integer $idscorm_tracking
 * @property integer $idUser
 * @property integer $idReference
 * @property integer $idscorm_item
 * @property string $user_name
 * @property string $lesson_location
 * @property string $credit
 * @property string $lesson_status
 * @property string $entry
 * @property double $score_raw
 * @property double $score_max
 * @property double $score_min
 * @property string $total_time
 * @property string $lesson_mode
 * @property string $exit
 * @property string $session_time
 * @property string $suspend_data
 * @property string $launch_data
 * @property string $comments
 * @property string $comments_from_lms
 * @property string $xmldata
 * @property string $first_access
 * @property string $last_access
 *
 * @property LearningScormItems $scorm_item
 * @property CoreUser $user
 */
class LearningScormTracking extends CActiveRecord
{
	const STATUS_COMPLETED = 'completed';
	const STATUS_FAILED = 'failed';
	const STATUS_INCOMPLETE = 'incomplete';
	const STATUS_NOT_ATTEMPTED = 'not attempted';
	const STATUS_PASSED = 'passed';

	public $search_input;
	private $xmlArr = array();

	protected $oldAttributes = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormTracking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_tracking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idReference, idscorm_item', 'numerical', 'integerOnly'=>true),
			array('score_raw, score_max, score_min', 'numerical'),
			array('user_name, lesson_location', 'length', 'max'=>255),
			array('credit, lesson_status, entry, lesson_mode, exit', 'length', 'max'=>24),
			array('total_time, session_time', 'length', 'max'=>15),
			array('suspend_data, launch_data, comments, comments_from_lms, xmldata, first_access, last_access, search_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_tracking, idUser, idReference, idscorm_item, user_name, lesson_location, credit, lesson_status, entry, score_raw, score_max, score_min, total_time, lesson_mode, exit, session_time, suspend_data, launch_data, comments, comments_from_lms, xmldata, first_access, last_access', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scorm_item' => array(self::BELONGS_TO, 'LearningScormItems', 'idscorm_item'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('first_access medium', 'last_access medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_tracking' => 'Idscorm Tracking',
			'idUser' => 'Id User',
			'idReference' => 'Id Reference',
			'idscorm_item' => 'Idscorm Item',
			'user_name' => 'User Name',
			'lesson_location' => 'Lesson Location',
			'credit' => 'Credit',
			'lesson_status' => 'Lesson Status',
			'entry' => 'Entry',
			'score_raw' => 'Score Raw',
			'score_max' => 'Score Max',
			'score_min' => 'Score Min',
			'total_time' => 'Total Time',
			'lesson_mode' => 'Lesson Mode',
			'exit' => 'Exit',
			'session_time' => 'Session Time',
			'suspend_data' => 'Suspend Data',
			'launch_data' => 'Launch Data',
			'comments' => 'Comments',
			'comments_from_lms' => 'Comments From Lms',
			'xmldata' => 'Xmldata',
			'first_access' => 'First Access',
			'last_access' => Yii::t('standard', '_DATE_LAST_ACCESS'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_tracking',$this->idscorm_tracking);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idscorm_item',$this->idscorm_item);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('lesson_location',$this->lesson_location,true);
		$criteria->compare('credit',$this->credit,true);
		$criteria->compare('lesson_status',$this->lesson_status,true);
		$criteria->compare('entry',$this->entry,true);
		$criteria->compare('score_raw',$this->score_raw);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('score_min',$this->score_min);
		$criteria->compare('total_time',$this->total_time,true);
		$criteria->compare('lesson_mode',$this->lesson_mode,true);
		$criteria->compare('exit',$this->exit,true);
		$criteria->compare('session_time',$this->session_time,true);
		$criteria->compare('suspend_data',$this->suspend_data,true);
		$criteria->compare('launch_data',$this->launch_data,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('comments_from_lms',$this->comments_from_lms,true);
		$criteria->compare('xmldata',$this->xmldata,true);
		$criteria->compare('first_access',Yii::app()->localtime->fromLocalDateTime($this->first_access),true);
		$criteria->compare('last_access',Yii::app()->localtime->fromLocalDateTime($this->last_access),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	protected function afterFind() {
		parent::afterFind();

		$this->oldAttributes = $this->attributes;
	}


	public function afterSave() {

		if ($this->isNewRecord) {
			// This will try to create item tracking records if main one does not exists
			self::setupScormItemsTrack($this->idUser, $this->scorm_item->idscorm_organization, $this->idReference);
		}

		// A change has been made to a scorm item track -> invalidate the questions and answers cache
		$scormItem = $this->scorm_item;
		if ($scormItem) {
			$scormItem->flushQuestionsAndAnswersCache();
		}

		if($this->lesson_status==self::STATUS_COMPLETED){
			if($this->getIsNewRecord() || $this->oldAttributes['score_raw'] != $this->score_raw){
				// Score created or updated
				$idCourse = Yii::app()->getDb()->createCommand()
					->select('idCourse')
					->from('learning_organization')
					->where('idOrg=:idOrg', array(':idOrg'=>$this->idReference))
					->queryScalar();

				// Trigger update of 'score_given'
				LearningCourseuser::recalculateLastScoreByUserAndCourse($idCourse, $this->idUser);

				// also update initial_score_given
				LearningCourseuser::recalculateInitialScoreGiven($this->idUser, $idCourse);
			}
		}

		return parent::afterSave();
	}

	/**
	 * Awesome method to convert unknown time format to seconds
	 * @return int
	 */
	public function getTotalTimeInSeconds() {
		if (!empty($this->total_time)) {
			if(strpos($this->total_time, "P") === false) {
				list($hours, $minutes, $seconds) = explode(':', $this->total_time);
				if (strlen($seconds)>2) list($seconds, $hundredths) = explode('.', $seconds);
				$time = $hours*3600 + $minutes*60 + $seconds;
				return $time;
			} else {
				$matches = array();
				preg_match ('/^P((\d*)Y)?((\d*)M)?((\d*)D)?(T((\d*)H)?((\d*)M)?((\d*)(\.(\d{1,2}))?S)?)?$/', $this->total_time, $matches);
				$seconds = (isset($matches[13]))? (int)$matches[13] : 0;
				$minutes = (isset($matches[11]))? (int)$matches[11] : 0;
				$hours = (isset($matches[9]))? (int)$matches[9] : 0;
			}
			$time = $hours*3600 + $minutes*60 + $seconds;
			return $time;
		}
		return 0;
	}

	public function getArrayFromXml()
	{
		if ($this->xmldata)
		{
			$xml = new SimpleXMLElement($this->xmldata);
			$this->buildArrFromXml($xml->cmi, '');
		}
		return $this->xmlArr;
	}

	/**
	 * Recursive build of an array from the xmldata property
	 * @param $node SimpleXMLElement object to start from
	 * @param $path with dots (cmi.core)
	 */
	private function buildArrFromXml($node, $path)
	{
		if (count($node->children()) > 0) {
			foreach ($node->children() as $child) {
				$index = (isset($node['index']) && (string)$node['index'] != 'no') ? '.'.(string)$node['index'] : '';
				$tmpPath = ($path ? $path.".".(string)$node->getName() : (string)$node->getName()).$index;
				$this->buildArrFromXml($child, $tmpPath);
			}
		} else {
			$nextElement = count($this->xmlArr);
			$this->xmlArr[$nextElement]['cmi'] = $path.".".(string)$node->getName();
			$this->xmlArr[$nextElement]['title'] = (string)$node;
 		}

	}

	public function searchXmlArray() {
		$this->getArrayFromXml();
		if ($this->search_input)
		{
			foreach ($this->xmlArr as $key => $cmi)
			{
				if (strpos($cmi['title'], $this->search_input) === false && strpos($cmi['cmi'], $this->search_input) === false)
				{
					unset($this->xmlArr[$key]);
				}
			}
		}
		return new CArrayDataProvider($this->xmlArr, array(
			'pagination' => array('pageSize' => count($this->xmlArr)), //hide pinator
		));
	}


	public function searchJsonArray() {

		$this->xmlArr = array();
		$cmi = CJSON::decode($this->xmldata, true);
		foreach($cmi as $key => $value) {
			if (is_array($value)) {
				$value = $value[0];
			}
			if ($this->search_input) {

				if (strpos($key, $this->search_input) !== false || strpos($value, $this->search_input) !== false) {
					$this->xmlArr[] = array(
						'cmi' => $key,
						'title' => $value,
					);
				}
			} else {

				$this->xmlArr[] = array(
					'cmi' => $key,
					'title' => $value,
				);
			}

		}

		return new CArrayDataProvider($this->xmlArr, array(
			'pagination' => array('pageSize' => count($this->xmlArr)), //hide pinator
		));
	}

	public function getTrackingArray() {


		if ($this->xmldata) {

			if ($this->xmldata{0} == '<') {
				return $this->searchXmlArray();
			} else {
				return $this->searchJsonArray();
			}
		} else {

			return new CArrayDataProvider(array(), array(
				'pagination' => array('pageSize' => 0), //hide pinator
			));
		}
	}

	/**
	 * Returns the decoded "choice" response from the CMI encoded format
	 * @param $response_string
	 * @param $scormVersion
	 */
	private function _decodeInteractionChoiceResponse($response_string, $scormVersion) {
		$multiple_response_delimiter = ",";
		if($scormVersion == "1.3")
			$multiple_response_delimiter = "[,]";

		return explode($multiple_response_delimiter, $response_string);
	}

	/**
	 * Returns the decoded "matching" response from the CMI encoded format
	 * @param $response_string
	 * @param $scormVersion
	 */
	private function _decodeInteractionMatchingResponse($response_string, $scormVersion) {
		$values = array();

		$multiple_response_delimiter = ",";
		$couple_delimiter = ".";
		if($scormVersion == "1.3") {
			$multiple_response_delimiter = "[,]";
			$couple_delimiter = "[.]";
		}

		$responses = explode($multiple_response_delimiter, $response_string);
		foreach($responses as $response)
			$values[] = explode($couple_delimiter, $response);

		return $values;
	}

	/**
	 * Returns the raw (unprocessed) array of user interactions from this scorm item
	 * @param $scorm_version
	 */
	public function getRawLearnerInteractions($scorm_version) {
		$interactions = array();

		if ($this->xmldata) {

			// Step 1: Read xmldata in both JSON (new) and XML (old) format
			// $cmi is an array of cmi records, indexed by the cmi key
			$cmi = array();
			if ($this->xmldata{0} == '<') {
				$xml = new SimpleXMLElement($this->xmldata);
				$this->xmlArr = array();
				$this->buildArrFromXml($xml->cmi, '');
				foreach ($this->xmlArr as $index => $record)
					$cmi[$record['cmi']] = $record['title'];
			} else
				$cmi = CJSON::decode($this->xmldata, true);

			// Step 2: extract all interactions and build an array of all the interactions
			foreach ($cmi as $key => $value) {
				if (preg_match("/^cmi.interactions.(\d+).(.+)/i", $key, $matches)) {
					$interactionIndex = $matches[1];
					$interactionParam = $matches[2];

					// Map scorm 1.2 params to scorm 2004 names
					switch ($interactionParam) {
						case 'time':
							$interactionParam = 'timestamp';
							break;
						case 'student_response':
							$interactionParam = 'learner_response';
							break;
					}

					if (preg_match("/^correct_responses.(\d+).pattern/i", $interactionParam, $paramMatches)) {
						$responseId = $paramMatches[1];
						$interactions[$interactionIndex]['correct_responses'][$responseId] = $value;
					} else if (preg_match("/^objectives.(\d+).id/i", $interactionParam, $paramMatches)) {
						// Do not include objects at this moment in time
						// $objectiveId = $paramMatches[1];
						// $interactions[$interactionIndex]['objectives'][$objectiveId] = $value;
					} else
						$interactions[$interactionIndex][$interactionParam] = $value;
				}
			}

			// Step 3: Post-process all interactions and normalize some data
			foreach($interactions as $index => &$user_interaction) {
				$type = $user_interaction['type'];
				switch($type) {
					case 'true-false': // True - False question
						$response = strtolower($user_interaction['correct_responses'][0]);
						switch($response){
							case 'true':
							case '1':
								$user_interaction['correct_responses'][0] =  't';
								break;
							case 'false':
							case '0':
								$user_interaction['correct_responses'][0] =  'f';
								break;
						}
						$learnerResponse = strtolower($user_interaction['learner_response']);
						switch($learnerResponse){
							case 'true':
							case '1':
								$user_interaction['learner_response'] =  't';
								break;
							case 'false':
							case '0':
								$user_interaction['learner_response'] =  'f';
								break;
						}
						break;
					case 'choice':
					case 'likert': // Similar to choice (todo verify)
					case 'sequencing':
					case 'performance':
						// Decode learner response for choice type
						if($user_interaction['learner_response'])
							$user_interaction['learner_response'] = $this->_decodeInteractionChoiceResponse($user_interaction['learner_response'], $scorm_version);

						// Decode correct responses for choice type
						if(!empty($user_interaction['correct_responses'])) {
							$result = array();
							foreach($user_interaction['correct_responses'] as $correct_response) {
								$correct_response = $this->_decodeInteractionChoiceResponse($correct_response, $scorm_version);
								$result = array_merge($result, $correct_response);
							}
							$user_interaction['correct_responses'] = $result;
						}
						break;
					case 'fill-in':
					case 'long-fill-in':
					case 'numeric':
						// No processing here -> just learner_response and result are used
						// todo: Later we may think about processing scorm 2004 correct_responses pattern and show all possible values
						break;
					case 'matching':
						// Decode learner response for matching type
						if($user_interaction['learner_response'])
							$user_interaction['learner_response'] = $this->_decodeInteractionMatchingResponse($user_interaction['learner_response'], $scorm_version);

						// Decode correct responses for matching type
						if(!empty($user_interaction['correct_responses'])) {
							$result = array();
							foreach($user_interaction['correct_responses'] as $correct_response) {
								$correct_response = $this->_decodeInteractionMatchingResponse($correct_response, $scorm_version);
								$result = array_merge($result, $correct_response);
							}
							$user_interaction['correct_responses'] = $result;
						}
						break;
				}
			}
		}

		return $interactions;
	}

	/**
	 * Returns the array of interactions for this track
	 * @return array
	 * @throws CHttpException
	 */
	public function getLearnerInteractions($question_answers_filter = false) {
		// Find the current scorm version of this item
		$scorm_version = $this->scorm_item->scorm_resource->scormPackage->scormVersion;
		if(!$scorm_version)
			return array();

		// Read the interactions
		$interactions = $this->getRawLearnerInteractions($scorm_version);

		// Fill them with the responses
		$responses = $this->scorm_item->collectQuestionsAndAnswers($question_answers_filter);
		foreach($interactions as $key => &$user_interaction) {
			$question_id = ($user_interaction['id'])? $user_interaction['id'] : $key;
			$question_type = $user_interaction['type'];
			if(isset($responses[$question_id])) {
				if(in_array($question_type, array('true-false', 'choice')))
					$user_interaction['responses'] = $responses[$question_id]['responses'];
				else if($question_type == 'matching')
					$user_interaction['sets'] = $responses[$question_id]['sets'];
			}
		}

		return $interactions;
	}

	public static function formatUserName($firstName, $lastName) {
		return $lastName.'.'.$firstName;
	}

	public static function formatTotalTime($timeSeconds, $centiSeconds = 0) {
		$hours = floor($timeSeconds / 3600);
		$minutes = floor($timeSeconds /60);
		$seconds = $timeSeconds % 60;
		if ($centiSeconds < 0) { $centiSeconds = 0; }
		if ($centiSeconds > 99) { $centiSeconds = 99; }
		return str_pad($hours, 4, '0', STR_PAD_LEFT)
			.':'.str_pad($minutes, 2, '0', STR_PAD_LEFT)
			.':'.str_pad($seconds, 2, '0', STR_PAD_LEFT)
			.'.'.str_pad($centiSeconds, 2, '0', STR_PAD_LEFT);
	}



	public static function getStatusList() {
		return array(
			self::STATUS_COMPLETED,
			self::STATUS_FAILED,
			self::STATUS_INCOMPLETE,
			self::STATUS_NOT_ATTEMPTED,
			self::STATUS_PASSED
		);
	}

	public static function isValidStatus($status) {
		return in_array($status, self::getStatusList());
	}

	public function updateStatus($newStatus) {

		if (!self::isValidStatus($newStatus)) {
			throw new CException('Invalid SCORM status: '.$newStatus);
		}

		$previousStatus = $this->lesson_status;
		if ($previousStatus == $newStatus) { return true; } //nothing to change

		if ($this->isNewRecord) {
			$this->lesson_status = $newStatus;
			return true;
		}

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			//if ($previousStatus == self::STATUS_COMPLETED || $previousStatus == self::STATUS_PASSED || $previousStatus == self::STATUS_FAILED) {
			switch ($newStatus) {
				case self::STATUS_NOT_ATTEMPTED: {
					//score need to be reset
					$this->score_raw = NULL;
					//reset scorm items statuses
					$rs = LearningScormItemsTrack::model()->updateAll(
						array('status' => LearningScormItemsTrack::STATUS_NOT_ATTEMPTED),
						"idReference = :id_reference AND idUser = :id_user",
						array(':id_reference' => $this->idReference, ':id_user' => $this->idUser)
					);
				} //do not break, continue with "inocmplete" operations, which are common for both statuses
				case self::STATUS_FAILED: {
					if ($previousStatus == self::STATUS_COMPLETED || $previousStatus == self::STATUS_PASSED) {
						$rs = LearningScormItemsTrack::model()->updateAll(
							array('status' => LearningScormItemsTrack::STATUS_FAILED),
							"idReference = :id_reference AND idUser = :id_user AND status = :status",
							array(':id_reference' => $this->idReference, ':id_user' => $this->idUser, ':status' => LearningScormItemsTrack::STATUS_PASSED)
						);
					}
				} break;
				case self::STATUS_INCOMPLETE: {
					//update scorm chapters status too
					$trackItems = LearningScormItemsTrack::model()->findAllByAttributes(array(
						'idReference' => $this->idReference,
						'idUser' => $this->idUser
					));
					if (!empty($trackItems)) {
						foreach ($trackItems as $trackItem) {
							switch ($trackItem->status) {
								case LearningScormItemsTrack::STATUS_FAILED:
								case LearningScormItemsTrack::STATUS_PASSED:
								case LearningScormItemsTrack::STATUS_COMPLETED: { $trackItem->status = LearningScormItemsTrack::STATUS_INCOMPLETE; } break;
							}
							if (!$trackItem->save()) {
								throw new CException('Error while updating scorm items status');
							}
						}
					}
				} break;
				case self::STATUS_COMPLETED:
				case self::STATUS_PASSED: {
					$rs = LearningScormItemsTrack::model()->updateAll(
						array('status' => LearningScormItemsTrack::STATUS_COMPLETED),
						"idReference = :id_reference AND idUser = :id_user",
						array(':id_reference' => $this->idReference, ':id_user' => $this->idUser)
					);
				} break;
			}

			$this->lesson_status = $newStatus;
			if (!$this->save()) { throw new CException('Error while updating SCORM status'); }

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return true;
	}



	/**
	 * Create required tracking records for all SCORM items for a given user, SCORM organization and LO
	 *
	 * @param int $id_user
	 * @param int $idscorm_organization
	 * @$idReference  Learning organization id
	 *
	 * @return bool
	 * @throws CException
	 */
	public static function setupScormItemsTrack($id_user, $idscorm_organization, $idReference) {

		// Check if it already exists (judging by the presense of the 'main' tracing record)
		$found = LearningScormItemsTrack::model()->findByAttributes(array('idUser' => $id_user,'idReference' => $idReference,'idscorm_organization'=>$idscorm_organization));
		if ($found) return true;

		// Create main row for the tracking based on the scorm organization
		$scorm_org = LearningScormOrganizations::model()->findByPk($idscorm_organization);
		if (!$scorm_org) {
			throw new CException('Unknow scorm organization : ' . $idscorm_organization);
		}

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
			// insert the row into scorm tracking
			$org_track = new LearningScormItemsTrack();
			$org_track->idscorm_organization 	= $scorm_org->idscorm_organization;
			$org_track->idscorm_item 			= new CDbExpression("NULL");
			$org_track->idReference 			= $idReference;
			$org_track->idUser 					= $id_user;
			$org_track->idscorm_tracking 		= new CDbExpression("NULL");
			$org_track->status 					= LearningCommontrack::STATUS_AB_INITIO;
			$org_track->nChild 					= $scorm_org->nChild;
			$org_track->nChildCompleted 		= 0;
			$org_track->nDescendant 			= $scorm_org->nDescendant;
			$org_track->nDescendantCompleted 	= 0;

			if (!$org_track->save()) {
				throw new CException('Unable to create main tracking data for : ' . $idscorm_organization);
			}

			// ok now we can "copy" the scorm_items into scorm_items_track
			$criteria = new CDbCriteria();
			$criteria->condition = 'idscorm_organization=:idscorm_organization';
			$criteria->params = array(
					':idscorm_organization' => $idscorm_organization
			);
			$criteria->order = 'idscorm_item ASC';
			$items =  LearningScormItems::model()->findAll($criteria);

			$sql = "INSERT INTO " . $org_track->tableName() . "
			(idscorm_organization,idscorm_item,idReference,idUser,status,nChild,nChildCompleted,nDescendant,nDescendantCompleted)
			VALUES
			(:idscorm_organization,:idscorm_item,:idReference,:idUser,:status,:nChild,:nChildCompleted,:nDescendant,:nDescendantCompleted)";
			$command = Yii::app()->db->createCommand($sql);

			foreach ($items as $scorm_item) {

				// replace the placeholder ":xxx" with the actual value
				$command->bindValue(":idscorm_organization", $idscorm_organization, PDO::PARAM_INT);
				$command->bindValue(":idscorm_item", $scorm_item->idscorm_item, PDO::PARAM_INT);
				$command->bindValue(":idReference", $idReference, PDO::PARAM_INT);
				$command->bindValue(":idUser", $id_user, PDO::PARAM_INT);
				$command->bindValue(":status", 'not attempted', PDO::PARAM_STR);
				$command->bindValue(":nChild", $scorm_item->nChild, PDO::PARAM_INT);
				$command->bindValue(":nChildCompleted", 0, PDO::PARAM_INT);
				$command->bindValue(":nDescendant", $scorm_item->nDescendant, PDO::PARAM_INT);
				$command->bindValue(":nDescendantCompleted", 0, PDO::PARAM_INT);
				if (!$command->execute()) {
					throw new CException('Unable to create scos tracking data for');
				}
			}

			// Create a record in common tracking table
			CommonTracker::track($idReference, $id_user, LearningCommontrack::STATUS_AB_INITIO);


			// all is setted up, let's commit this transaction
			if (isset($transaction)) { $transaction->commit(); }
		}
		catch(Exception $e)  {
			if (isset($transaction)) { $transaction->rollback(); }
			// relaunch the exception to the caller
			throw new CException('Unable to create scos tracking data for : ' . $idscorm_organization);
		}
		return true;
	}

	public function renderScore($fallback = '-') {
		$html = $fallback;

		if (is_numeric($this->score_raw)) {
            // prepare score raw value
            $score_raw = floatval($this->score_raw);
            $score_raw_whole = floor($score_raw); // whole part
            $score_raw_fraction = $score_raw - $score_raw_whole; // fraction part

            // prepare score max value
            $score_max = floatval($this->score_max);
            $score_max_whole = floor($score_max); // the whole part
            $score_max_fraction = $score_max - $score_max_whole; // fraction part

            // if the score items are fraction, then use round it to two decimal places
			$html = (($score_raw_fraction <> 0) ? number_format($score_raw, 2) : $score_raw) . '/' . (
				($score_max > 0) ? (($score_max_fraction <> 0) ? number_format($score_max, 2) : $score_max) : 100
			);
		}

		return $html;
	}


	public function renderLastAccess($fallback = '-') {
		$html = $fallback;

		if ($this->last_access) {
			$html = $this->last_access;
		}

		return $html;
	}


}