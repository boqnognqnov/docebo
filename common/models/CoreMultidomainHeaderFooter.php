<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 08-Apr-16
 * Time: 2:32 PM
 * @property string $language
 * @property string $type
 * @property string $value
 * @property integer $id_multidomain
 */
class CoreMultidomainHeaderFooter extends CActiveRecord
{

	const TYPE_HEADER_MESSAGE = 'header_message';
	const TYPE_FOOTER_CUSTOM_MESSAGE = 'custom_text';
	const TYPE_FOOTER_CUSTOM_URL = 'custom_url';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreLangWhiteLabelSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_multidomain_header_footer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language, type', 'length', 'max' => 100),
			array('language, type, id_multidomain', 'required'),
			array('id_multidomain', 'numerical', 'integerOnly' => true),
			array('value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('language, type, value, id_multidomain, id', 'safe', 'on'=>'search'),
		);
	}
}