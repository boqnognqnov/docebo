<?php

/**
 * This is the model class for table "app7020_question_follow".
 *
 * The followings are the available columns in table 'app7020_question_follow':
 * @property integer $id
 * @property integer $idQuestion
 * @property integer $idExpert
 * @property integer $idContent
 * @property integer $autoPublish
 * @property integer $autoInvite
 * @property timstamp $tooked
 *
 * The followings are the available model relations:
 * @property App7020QuestionRequest $question
 */
class App7020QuestionRequest extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_questions_requests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idQuestion', 'required'),
			array('idQuestion', 'numerical', 'integerOnly' => true),
//			array('idQuestion, idExpert, idContent, autoPublish, autoInvite', 'numerical', 'setOnEmpty' => true, 'value' => null),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idQuestion', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'question' => array(self::BELONGS_TO, 'App7020Question', 'idQuestion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idQuestion' => 'Id Question',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020QuestionFollow the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Returns an array with questions related with selected expert
	 *
	 * @param int required $userId ID of the user. The returned assets must belongs to any of topics where the selected user is expert
	 * @param array optional $questionStatus the statusses of the question 1-new, 2-opened
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getExpertsRequests($userId, $requestStatus = 1, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()) {

		if (empty($userId)) {
			return false;
		}

		$searchInput = trim(Yii::app()->request->getParam('search_input', false));

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select(" q.*, cu.firstname, cu.lastname, cu.userid, eu.firstname AS expertFirstname, eu.lastname AS expertLastname,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) as answersCount,
		                       (SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) AS viewesCount,
	                           (SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id ORDER BY created DESC LIMIT 1) as lastAnswerId,
	                           (SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id=lastAnswerId) as lastAnswerText,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id AND bestAnswer = 2) as haveBestAnswer, 
		                       (SELECT COUNT(a.id) FROM " . App7020Answer::model()->tableName() . " a 
	                               LEFT JOIN " . App7020QuestionHistory::model()->tableName() . " qh 
	                               ON  a.idQuestion=qh.idQuestion
                                   WHERE a.idQuestion = q.id AND (a.created > qh.viewed OR ISNULL(qh.viewed)) 
	                               AND (qh.idUser = :userId OR ISNULL(qh.idUser))) as newAnswers");

		$commandBase->params = array(":userId" => $userId);
		$commandBase->from(App7020QuestionRequest::model()->tableName() . " qr");
		$commandBase->join(App7020Question::model()->tableName() . " q", "qr.idQuestion = q.id");
		$commandBase->leftJoin(App7020IgnoreList::model()->tableName() . " il", $userId . " = il.idUser AND il.idObject=q.id AND il.objectType='" . App7020IgnoreList::OBJECT_TYPE_QUESTION . "'");
		$commandBase->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		$commandBase->leftJoin(CoreUser::model()->tableName() . " eu", "qr.idExpert = eu.idst");
		$commandBase->leftJoin(App7020Assets::model()->tableName() . " c", "qr.idContent = c.id");

		$commandBase->where(" (c.conversion_status < " . App7020Assets::CONVERSION_STATUS_APPROVED . " OR ISNULL(c.conversion_status))");
		$commandBase->andWhere(" ISNULL(il.id)");

		if ($searchInput) {
			$commandBase->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		if ($requestStatus == 1) {
			$commandBase->andWhere(" ISNULL(qr.idExpert)");
		} else {
			$commandBase->andWhere(" qr.idExpert > 0");
		}


		$returnArray = $commandBase->queryAll(true);

		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);

		if (!empty($providerConfig['comboListViewId'])) {
			unset($providerConfig['comboListViewId']);
		}

		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}

	/**
	 * Returns an array with questionsrequests related to the selected expert
	 *
	 * @param int required $userId ID of the user. 
	 * @param array optional $questionStatus the statusses of the question 1-new, 2-opened
	 * @param string optional $orderField
	 * @param bool optional $asArrayDataProvider - if is set to true - an CArrayDataProvider object will be returned, otherwise an array will be returned
	 * @param array optional $providerConfig The configuration of the CArrayDataProvider object
	 *
	 * return array|ArrayDataProvider
	 */
	public static function getRequestsToSatisfy($idUser, $params) {

		if (empty($idUser)) {
			$idUser = Yii::app()->user->id;
		}

		if (!isset($params["from"]))
			$params["from"] = 0;

		if (!isset($params["count"]))
			$params["count"] = 5;

		$expertsChannels = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($idUser);

		if (!$expertsChannels && !Yii::app()->user->isGodAdmin) {
			return array();
		}
	

		$command = Yii::app()->db->createCommand();
		$command->select("q.*, qr.*, cq.idChannel, a.conversion_status, "
			. "(SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) as views, "
			. "(SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id) as answers, "
			. "(SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id ORDER BY id DESC LIMIT 1) as lastAnswerId, "
			. "(SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id = lastAnswerId) as lastAnswerText ");

		$command->from(App7020QuestionRequest::model()->tableName() . " qr");
		$command->join(App7020Question::model()->tableName() . " q", "qr.idQuestion = q.id");
		$command->leftJoin(App7020ChannelQuestions::model()->tableName() . " cq", "q.id = cq.idQuestion");
		$command->leftJoin(App7020Assets::model()->tableName() . " a", "a.id = qr.idContent");
		$command->leftJoin(App7020IgnoreList::model()->tableName() . " il", "q.id = il.idObject AND il.objectType = '" . App7020IgnoreList::OBJECT_TYPE_QUESTION . "' AND il.idUser = " . $idUser);
        $command->leftJoin(App7020Channels::model()->tableName() . ' c', 'c.id = cq.idChannel');

	
		if (!Yii::app()->user->isGodAdmin) {
			$command->where(array('in', 'cq.idChannel', $expertsChannels));
 		}
 		
 		$command->andWhere('q.open = 1');
		$command->andWhere('a.conversion_status IS NULL OR a.conversion_status < 20');
        $command->andWhere('c.enabled = 1');

		if ($params['onlyNew']) {
			$command->andWhere(" ISNULL(qr.idExpert)");
		}

		if ($params['taken']) {
			$command->andWhere(" qr.idExpert = :idUser", array(':idUser' => $idUser));
			//$command->andWhere("qr.idExpert IS NOT NULL");
		}

		if ($params['ignored']) {
			$command->andWhere("il.id IS NOT NULL");
		} else {
			$command->andWhere("il.id IS NULL");
		}

		$command->order('created DESC');

		$command->group("q.id");
		if (!$params['all']) {
			$command->limit($params['count'], $params['from']);
		}
		return $command->queryAll();
	}

}
