<?php

/**
 * This is the model class for table "core_setting_user".
 * The followings are the available columns in table 'core_setting_user':
 * @property string $path_name
 * @property integer $id_user
 * @property string $value
 * The followings are the available model relations:
 * @property CoreUser $idUser
 */
class CoreSettingUser extends CActiveRecord {
	
	const GURUDASHBOARD_SATISFY_GOTIT = 'guru_dashboard_satisfy_gotit';
	const GURUDASHBOARD_ASSETS_GOTIT = 'guru_dashboard_assets_gotit';
	const GURUDASHBOARD_QUESTIONS_GOTIT = 'guru_dashboard_questions_gotit';
//	const CONTRIBUTE_MOBILE_APP_GOTIT = 'contribute_mobile_app_gotit';
//	const KNOWLEDGELIBRARY_CONTRIBUTE_BANNER_GOTIT = 'knowledgelibrary_contribute_banner_gotit';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreSettingUser the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_setting_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('value', 'required'),
			array('id_user', 'numerical', 'integerOnly' => true),
			array('path_name', 'length', 'max' => 255),
			array('value', 'safe', 'on' => 'create, update, selfEditProfile'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('path_name, id_user, value', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'path_name' => 'Path Name',
			'id_user' => 'Id User',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('path_name', $this->path_name, true);
		$criteria->compare('id_user', $this->id_user);
		$criteria->compare('value', $this->value, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	static public function getSettingByUser($userId, $settingName){
		return Yii::app()->getDb()->createCommand()
				  ->select( 'value' )
				  ->from( self::model()->tableName() )
				  ->where( 'id_user=:idUser', array( ':idUser' => $userId ) )
				  ->andWhere( 'path_name=:name', array( ':name' => $settingName ) )
				  ->queryScalar();
	}
}