<?php

/**
 * This is the model class for table "dashboard_dashlet_translation".
 *
 * The followings are the available columns in table 'dashboard_dashlet_translation':
 * @property integer $id
 * @property integer $id_dashlet
 * @property string $lang_code
 * @property string $header
 *
 * The followings are the available model relations:
 * @property DashboardDashlet $idDashlet
 */
class DashboardDashletTranslation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_dashlet_translation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_dashlet, lang_code', 'required'),
			array('id_dashlet', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('header', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_dashlet, lang_code, header', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDashlet' => array(self::BELONGS_TO, 'DashboardDashlet', 'id_dashlet'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_dashlet' => 'Id Dashlet',
			'lang_code' => 'Lang Code',
			'header' => 'Header',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_dashlet',$this->id_dashlet);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('header',$this->header,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardDashletTranslation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
