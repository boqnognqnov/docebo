<?php

/**
 * This is the model class for table "stripe_customer".
 *
 * The followings are the available columns in table 'stripe_customer':
 * @property integer $id
 * @property integer $id_user_lms
 * @property string $id_customer_stripe
 *
 * The followings are the available model relations:
 * @property CoreUser $id0
 */
class StripeCustomer extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'stripe_customer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_user_lms, id_customer_stripe', 'required'),
            array('id_user_lms', 'numerical', 'integerOnly'=>true),
            array('id_customer_stripe', 'length', 'max'=>50),
            // The following rule is used by search().
            array('id, id_user_lms, id_customer_stripe', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'id0' => array(self::BELONGS_TO, 'CoreUser', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user_lms' => 'Id User Lms',
            'id_customer_stripe' => 'Id Customer Stripe',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('id_user_lms',$this->id_user_lms);
        $criteria->compare('id_customer_stripe',$this->id_customer_stripe,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StripeCustomer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
