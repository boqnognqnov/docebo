<?php

/**
 * This is the model class for table "learning_category".
 *
 * The followings are the available columns in table 'learning_category':
 * @property integer        $idCategory
 * @property integer        $lev
 * @property integer        $iLeft
 * @property integer        $iRight
 * @property integer 		$soft_deadline
 *
 * The followings are the available model relations:
 * @property LearningCourse[] $learningCourses
 */
class LearningCourseCategoryTree extends CActiveRecord {

	public $confirm;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningCourseCategoryTree the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_category_tree';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'idCategory, lev, iLeft, iRight, soft_deadline',
				'numerical',
				'integerOnly' => true
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'idCategory, lev, iLeft, iRight, soft_deadline',
				'safe',
				'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courses' => array(
				self::HAS_MANY,
				'LearningCourse',
				'idCategory'
			),
			'coursesCategory' => array(
				self::BELONGS_TO,
				'LearningCourseCategory',
				'idCategory'
			),
			'coursesCategoryTranslated' => array(
				self::BELONGS_TO,
				'LearningCourseCategory',
				'idCategory',
				'condition' => 'coursesCategoryTranslated.lang_code = :lang_code',
				'params' => array(':lang_code' => Lang::getFullLangName(Yii::app()->getLanguage()))
			),
			/* 'coursesCategoryDefault' => array(
			  self::BELONGS_TO,
			  'LearningCourseCategory',
			  'idCategory',
			  'condition' => 'coursesCategoryTranslated.lang_code = :lang_code',
			  'params' => array(':lang_code' => Docebo::sett('defaultLanguage') )
			  ), */
		);
	}

	public function behaviors() {
		return array(
			'nestedSetBehavior' => array(
				'class' => 'common.extensions.NestedSetBehavior.NestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
			//'hasManyRoots' => true,
			//'rootAttribute' => 'is_root',
			),
		);
	}

	public function scopes() {
		$currentNodeId = Yii::app()->session['currentCourseNodeId'];
		$currentObjectsNodeId = Yii::app()->session['currentCentralRepoNodeId'];
		return array(
			'nodeCoursesOnly' => array(
				'condition' => $this->getTableAlias(false, false) . '.idCategory = :idCategory',
				'params' => array(':idCategory' => $currentNodeId),
			),
			'nodeObjectsOnly' => array(
				'condition' => $this->getTableAlias(false, false) . '.idCategory = :idCategory',
				'params' => array(':idCategory' => $currentObjectsNodeId),
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idCategory' => 'Id Category',
			'lev' => 'Lev',
			'iLeft' => 'I Left',
			'iRight' => 'I Right',
		);
	}

	public function afterSave() {
		// See CoreNotification/TransportAgent for events
		// Raise event when a new folder is created (not a valid LO type)
		// NOTE: This is raised even if just a folder is created.
		// All checking must be done in the event callback/handler.
		Yii::app()->event->raise('NewCategory', new DEvent($this, array(
			'category' => $this->coursesCategory, // AR relation
		)));


		return parent::afterSave();
	}

	public function removeNode($id) {
		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$transaction = Yii::app()->db->beginTransaction();
		}
		try {
			$categoryModelAttributes = LearningCourseCategoryTree::model()->findByPk($id)->coursesCategoryTranslated->attributes;

			// First change all the Keys
			$rootNodeId = LearningCourseCategoryTree::getRootNodeId();
			if($rootNodeId){
				$rs = LearningCourse::model()->updateAll(array('idCategory' => $rootNodeId), 'idCategory = '.$id);
				$rsLO = LearningRepositoryObject::model()->updateAll(array('id_category' => $rootNodeId), 'id_category = '.$id);
			}else{
				$rs = LearningCourse::model()->updateAll(array('idCategory' => 0), 'idCategory = '.$id);
				$rsLO = LearningRepositoryObject::model()->updateAll(array('id_category' => $rootNodeId), 'id_category = '.$id);
			}

			if ($rs === false || $rsLO === false) {
				throw new CException('Error while deleting course category');
			}

			// Then delete the selected NODE
			$rs = LearningCourseCategory::model()->deleteAllByAttributes(array('idCategory' => $id));
			if ($rs === false) {
				throw new CException('Error while deleting course category');
			}
			$rs = LearningCourseCategoryTree::model()->findByPk($id)->deleteNode();

			if ($rs === false) {
				throw new CException('Error while deleting course category');
			}

			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch (Exception $e) {
			Yii::log($e->getMessage(),CLogger::LEVEL_ERROR);
			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}

        Yii::app()->event->raise('RemoveCategory', new DEvent($this, array(
            'category' => $categoryModelAttributes
        )));

		return true;
	}


	/**
	 * Retrieve the id of the root node for the course categories
	 *
	 * @return mixed Id of the root node or false
	 */
	public static function getRootNodeId()
	{
		$roots = LearningCourseCategoryTree::model()->roots()->findAll();
		$node = (!empty($roots) ? $roots[0] : false);

		if($node)
			return $node->idCategory;
		return false;
	}

	/**
	 * Retrieve a tree-like structure for categories fancytree
	 *
	 * @param int $idUser the user for catalogs retrieval
	 * @param string $language translation language of the tree folders
	 * @return array tree-like structure of nodes for fancytree
	 */
	public static function getCatalogTreeData($language = false, $includeRoot=true) {
	    
		$showRoot = $includeRoot;

		//initialize output variable
		$output = array();

		//detect language, if not specified
		if (!$language) { $language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage()); }

		//retrieve nodes data from DB
		$data = Yii::app()->db->createCommand()
			->select("ct.*, c.translation")
			->from(LearningCourseCategoryTree::model()->tableName()." ct")
			->leftJoin(
				LearningCourseCategory::model()->tableName()." c",
				"c.idCategory = ct.idCategory AND c.lang_code = :lang_code",
				array(':lang_code' => $language)
			)
			->order("c.translation")
			->queryAll();

		//default english node titles translations, in case of untranslated node names
		$defaultLangCode = Settings::get('default_language', '');
		$defaultTitles = array();
		$criteria = new CDbCriteria();
		$criteria->addCondition("lang_code = :lang_code");
		$criteria->params[':lang_code'] = $defaultLangCode;
		$_transList = LearningCourseCategory::model()->findAll($criteria);
		if (!empty($_transList)) {
			foreach ($_transList as $_transItem) {
				$defaultTitles[(int)$_transItem->idCategory] = trim($_transItem->translation);
			}
		}

		//create tree structure from DB data
		if (!empty($data)) {
			//tree retrieval function
			$retrieveChildren = function($node = false) use(&$retrieveChildren, &$data, &$defaultTitles, $showRoot) {
				$output = array();
				$firstLevel = ($showRoot ? 1 : 2);
				$checkLevel = ($node ? ($node['lev'] + 1) : $firstLevel); //children level
				if ($checkLevel < $firstLevel) { $checkLevel = $firstLevel; }
				for ($i=0; $i<count($data); $i++) {
					//check if the node is a child of the function argument's node
					$isChild = false;
					if ($data[$i]['lev'] == $checkLevel) {
						if ($node) {
							$isChild = ($data[$i]['iLeft'] > $node['iLeft'] && $data[$i]['iRight'] < $node['iRight']);
						} else {
							$isChild = true;
						}
					}
					//if it is, then create node structure
					if ($isChild) {
						$isRoot = ($data[$i]['lev'] <= 1);
						$key = (int)$data[$i]['idCategory'];
						if ($isRoot) {
							$_nodeTitle = Yii::t('standard', '_ALL_CATEGORIES');
						} else {
							$_nodeTitle = trim($data[$i]['translation']);
							if (empty($_nodeTitle)) {
								if (isset($defaultTitles[$key])) {
									$_nodeTitle = $defaultTitles[$key];
								}
							}
						}
						$output[] = array(
							'folder' => true,
							'key' => $key,
							'title' => $_nodeTitle,
							'data' => array(
								'is_root' => (bool)$isRoot
							),
							'expanded' => ($isRoot ? true : false), //only root node is eventually pre-expanded
							'children' => $retrieveChildren($data[$i])
						);
					}
				}
				return $output;
			};

			$output = $retrieveChildren();
		}

		return $output;
	}

	/**
	 * Gets the given category and its descendants IDs
	 *
	 * @param $idCategory
	 *
	 * @return array of IDs (also includes the Root category ID: "0")
	 * @throws CException
	 */
	public function getCategoryAndDescendants($idCategory){
		// Add the requested category ID to the array
		$categories = array((int) $idCategory);

		// uncategorized courses are considered as contained into root

		// Get the root categories
		$_roots = LearningCourseCategoryTree::model()->roots()->findAll();
		if (empty($_roots)) {
			throw new CException('Invalid categories tree');
		}

		// Check if the requested category is the root
		$rootCategory = $_roots[0];
		if ($idCategory == $rootCategory->idCategory) {
			$categories[] = 0;
		}
		//find subcategories
		$_node        = LearningCourseCategoryTree::model()->findByPk($idCategory);

		if(!$_node){
			return array();
		}

		$_descendants = $_node->descendants()->findAll();

		if (!empty($_descendants)) {
			foreach ($_descendants as $_descendant) {
				$categories[] = (int) $_descendant->idCategory;
			}
		}

		return $categories;
	}
    
    /*
     * Function is arranging the courses categories tree, which is allready sorted, alphabetical.
     * 
     * @param $tree The tree, which is needed to be arranged and sorted alphabetical
     * @param $isFromCategoryModel Bolean - if we get the tree with leftjoin with LearningCourseCategory model
     * 
     * @return array with arranged alphabetical courses categories tree
     */    
    public static function arrangeTreeAlphabetical($tree){                                   
        
        $buildTree = function($categoryTree, $iLeft = 0, $iRight = null, $lev = 0) use (&$buildTree){                        
            $arrangedTree = array();                        
                        
            foreach($categoryTree as $idOrg => $item){                
                
                $isDecendence = ($item['iLeft'] > $iLeft) && (is_null($iRight) || ($item['iRight'] < $iRight))  && (($lev + 1) == $item['lev']);

                if($isDecendence){
                    $arrangedTree[] = $item;                    

                    if($item['iRight'] - $item['iLeft'] > 1){
                        $arrangedTree = array_merge_recursive($arrangedTree, $buildTree($categoryTree, $item['iLeft'], $item['iRight'], $item['lev']));                        
                        
                    }
                }                 
            }                                    
            return $arrangedTree;
        };
        
        return $buildTree($tree);
    }

	/**
	 * Returns the value of the soft_deadline flag inherited
	 * @return array|null
	 */
	public function getInheritedSoftDeadline($excludeCurrentNode = false) {
		$sql = "select * from learning_category_tree
		where iLeft <".(!$excludeCurrentNode ? "=" : "")." :iLeft and iRight >".(!$excludeCurrentNode ? "=" : ""). ":iRight and soft_deadline IS NOT NULL
		order by iLeft DESC";

		return Yii::app()->db->createCommand($sql)->queryRow(true, array(
			':iLeft' => $this->iLeft,
			':iRight' => $this->iRight
		));
	}

	/**
	 * Returns true if this category (or its ancestors) has the soft_deadline flag on
	 *
	 * @return boolean
	 */
	public function hasSoftDeadline($excludeCurrentNode = false) {
		$row = $this->getInheritedSoftDeadline($excludeCurrentNode);
		return !is_null($row);
	}
}
