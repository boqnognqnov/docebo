<?php

/**
 * This is the model class for table "app7020_temp_invitations".
 *
 * The followings are the available columns in table 'app7020_temp_invitations':
 * @property integer $id
 * @property integer $idContent
 * @property text $invited
 *
 */
class App7020TemporaryInvitations extends CActiveRecord {


	public function tableName() {
        return 'app7020_temp_invitations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idContent, invited', 'required'),
        );
    }

	public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}
