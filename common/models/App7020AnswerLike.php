<?php

/**
 * This is the model class for table "app7020_answer_like".
 *
 * The followings are the available columns in table 'app7020_answer_like':
 * @property integer $id
 * @property integer $idAnswer
 * @property integer $idUser
 * @property integer $type
 *
 * The followings are the available model relations:
 * @property App7020Answer $answer
 * @property CoreUser $user
 */
class App7020AnswerLike extends CActiveRecord
{
	
	/**
	 * Value of column "type" in case of "Like"
	 */
	const TYPE_LIKE = 1;
	
	/**
	 * Value of column "type" in case of "Dislike"
	 */
	const TYPE_DISLIKE = 2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app7020_answer_like';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAnswer, idUser, type', 'required'),
			array('idAnswer, idUser, type', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idAnswer, idUser, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answer' => array(self::BELONGS_TO, 'App7020Answer', 'idAnswer'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idAnswer' => 'Id Answer',
			'idUser' => 'Id User',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idAnswer',$this->idAnswer);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020AnswerLike the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
     * Calculates the likes and dislikes of the given user's answers for the given period
     * 
     * @param date optional $dateFrom start of the period
     * @param date optional $dateTo end of the period
     * @param int optional $userId ID of the user
     * 
     * return array|integer An array with type (1-likes; 2-dislikes) as the keys and count of the likes and dislikes
     */
	public static function getUsersLikesUnlikesByPeriod($dateFrom = null, $dateTo = null, $userId = null){
	    
	    $commandBase = Yii::app()->db->createCommand();
        $commandBase->select("COUNT(answerslikes.id) AS aCount, answerslikes.type as aType");
	    
	    $commandBase->from(App7020AnswerLike::model()->tableName(). " answerslikes");
	    $commandBase->join(App7020Answer::model()->tableName()." answers", "answerslikes.idAnswer=answers.id");
	    
	    if(!empty($dateFrom)){
	        $commandBase->andWhere(" DATE(answers.created) >= :dateFrom", array(':dateFrom' => date("Y-m-d", strtotime($dateFrom))));
	    }
	    
	    if(!empty($dateTo)){
	        $commandBase->andWhere(" DATE(answers.created) <= :dateTo", array(':dateTo' => date("Y-m-d", strtotime($dateTo))));
	    }
	    
	    if(!empty($userId)){
	        $commandBase->andWhere(" answers.idUser = :userId", array(':userId' => $userId));
	    }
	    
	    $commandBase->group("answerslikes.type");
	    
	    $returnArray = array();
	    $resultArray = $commandBase->queryAll(true);
	    foreach ($resultArray as $value){
	        $returnArray[$value['aType']] = $value['aCount'];
	    }
	    return $returnArray;
	}


	public function afterSave(){
		$answerModel = App7020Answer::model()->findByPk($this->idAnswer);
		if($this->type == 1){
			Yii::app()->event->raise('UserReachedAGoal', new DEvent($this, array('goal' => 1,'user'=>$answerModel->idUser)));
		} else {
			Yii::app()->event->raise('UserReachedAGoal', new DEvent($this, array('goal' => 2,'user'=>$answerModel->idUser)));
		}
	}
}
