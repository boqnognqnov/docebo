<?php

/**
 * This is the model class for table "core_field".
 *
 * The followings are the available columns in table 'core_field':
 * @property string $id
 * @property string $filename
 * @property string $original_filename
 * @property string $extension
 * @property integer $filesize
 * @property integer $type_id
 * @property integer $ref_type
 * @property integer $ref_id
 * @property datetime $created
 * @property datetime $modified
 */
class CoreFile extends CActiveRecord
{

	const FILE_IMAGE = 1;
	const FILE_DOCUMENT = 2;

	public $uploadedFile;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreCalendar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type_id, ref_type', 'required'),
			array('ref_id', 'numerical', 'allowEmpty' => true),
			array('filesize', 'numerical'),
			array('filename, original_filename, extension', 'length', 'max' => '255'),
			// array('filesize, type_id, ref_type, ref_id', 'integerOnly' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, created, modified, type_id', 'safe', 'on'=>'search'),
		);
	}

	public function behaviors() {
		return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                 'timestampAttributes' => array('created medium', 'modified medium'),
                 'dateAttributes' => array()
            )
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'imageLibrary' => array(self::HAS_ONE, 'CoreLibraryImage', array('file_id' => 'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'filepath' => 'Class',
			'filesize' => 'Create Date',
			'created' => 'Start Date',
			'modified' => 'End Date',
			'type_id' => 'Title',
			'ref_type' => 'Description',
			'ref_id' => 'Private',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('filepath', $this->filepath, true);
		$criteria->compare('created', Yii::app()->localtime->fromLocalDateTime($this->created));
		$criteria->compare('modified', Yii::app()->localtime->fromLocalDateTime($this->modified));
		$criteria->compare('type_id', $this->type_id);
		$criteria->compare('ref_type', $this->ref_type);
		$criteria->compare('ref_id', $this->ref_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}