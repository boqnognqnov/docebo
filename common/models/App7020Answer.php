<?php

/**
 * This is the model class for table "app7020_answer".
 *
 * The followings are the available columns in table 'app7020_answer':
 * @property integer $id
 * @property integer $idQuestion
 * @property integer $idUser
 * @property integer $content
 * @property integer $bestAnswer
 * @property string $created
 *
 * The followings are the available model relations:
 * @property App7020Question $question
 * @property CoreUser $user
 * @property App7020AnswerLike[] $app7020AnswerLikes
 */
class App7020Answer extends CActiveRecord {
	
	/**
	 * Scenario sned notification about new answer
	 */
	const SCENARIO_NEW_ANSWER = 'new_answer';

	
	/**
	 * Scenario sned notification about new question
	 */
	const SCENARIO_EDIT_ANSWER = 'edit_answer';
	
	/**
	 * Value of column "bestAnswer" if IS "Best Answer"
	 */
	const BEST_ANSWER = 2;

	/**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_answer';
    }
	
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idQuestion, idUser, content, bestAnswer', 'required'),
            array('idQuestion, idUser, bestAnswer', 'numerical', 'integerOnly' => true),
            array('content', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idQuestion, idUser, content, bestAnswer, created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'question' => array(self::BELONGS_TO, 'App7020Question', 'idQuestion'),
            'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
            'app7020AnswerLikes' => array(self::HAS_MANY, 'App7020AnswerLike', 'idAnswer'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idQuestion' => 'Id Question',
            'idUser' => 'Id User',
            'content' => 'Content',
            'bestAnswer' => 'Best Answer',
            'created' => 'Created',
        );
    }
    
    public function beforeSave() {
        $this->content = Yii::app()->htmlpurifier->purify($this->content);
		if(empty($this->id)){
			$this->setScenario(self::SCENARIO_NEW_ANSWER);
		}
        
		return parent::beforeSave();
	} 
    
	public function afterSave() {
		if($this->scenario == self::SCENARIO_NEW_ANSWER){
			Yii::app()->event->raise('App7020NewAnswer', new DEvent($this, array('questionId' => $this->idQuestion, 'ownerAnswer' => CoreUser::model()->findByPk(Yii::app()->user->idst))));
		}

		
		Yii::app()->event->raise(EventManager::EVENT_APP7020_ANSWER_UPDATED, new DEvent($this, array(
			'model'	=> $this,
		)));

		parent::afterSave();
	}
    

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idQuestion', $this->idQuestion);
        $criteria->compare('idUser', $this->idUser);
        $criteria->compare('content', $this->content);
        $criteria->compare('bestAnswer', $this->bestAnswer);
        $criteria->compare('created', $this->created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Make SQL Query and Configurate ComboListView
     * @return \CSqlDataProvider
     */
    public static function sqlDataProvider($customConfig = array()) {
        $id = (int) Yii::app()->request->getParam('id', false);
		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;
        $qid = (int)$customConfig['questionId'];
        if(!empty($qid)) $id = $customConfig['questionId'];
		
		
        $checkForBestAnswer = Yii::app()->db->createCommand("Select Count(id) From ".App7020Answer::model()->tableName()." Where bestAnswer = 2 and idQuestion = :idQuestion");
        $countOfbestAnswerInQuestion = $checkForBestAnswer->queryScalar(array(':idQuestion' => $id));
        $commandBase = Yii::app()->db->createCommand();
        $commandBase->from(App7020Answer::model()->tableName() . ' a');
        $commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'a.idUser = u.idst');
        $commandBase->where('a.idQuestion = :idQuestion AND a.bestAnswer NOT IN (2)', array(':idQuestion' => $id));

        $commandData = clone $commandBase;
        $commandData->select('a.*, u.userid, u.firstname, u.lastname, u.avatar, (Select count(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 1) as likes, (Select count(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 2) as dislikes');
        $commandCounter = clone $commandBase;
        $commandCounter->select('count(a.id)');
        $numRecords = $commandCounter->queryScalar();
		if(!empty($customConfig['order'])){
			$commandData->order = $customConfig['order'];
		} else {
			$commandData->order = "created DESC";
		}
        
        $sort = new CSort();
        $sort->attributes = array(
                //
        );
        $sort->defaultOrder = array(
            'id' => CSort::SORT_DESC,
        );
		if ($setPagination) {
			$pageSize = 5;

			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}

        $size = (int)$customConfig['pageSize'];
		if(!empty($size)) $pageSize = $customConfig['pageSize'];
        $config = array(
            'totalItemCount' => $numRecords,
            'pagination' => $pagination,
            'keyField' => 'id',
            'sort' => $sort
        );
        $dataProvider = new CSqlDataProvider($commandData, $config);
        return $dataProvider;
    }
    
//    public static function getBestAnswerByQuestionId(){
//        $data = array();
//        $commandBase = Yii::app()->db->createCommand();
//        $commandBase->select('*');
//        $commandBase->from(App7020Answer::model()->tableName());
//        $commandBase->where('');
//        return $data;
//    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020Answer the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    
    /**
     * Calculates the count of the besta answers of the given user's answers for the given period
     *
     * @param date optional $dateFrom start of the period
     * @param date optional $dateTo end of the period
     * @param int optional $userId ID of the user
     *
     * return array|integer 
     */
    public static function getUsersBestAnswersByPeriod($dateFrom = null, $dateTo = null, $userId = null){
         
        $commandBase = Yii::app()->db->createCommand();
        $commandBase->select("COUNT(id) AS aCount");
        $commandBase->from(App7020Answer::model()->tableName());
        $commandBase->andWhere("bestAnswer = 2");
         
        if(!empty($dateFrom)){
            $commandBase->andWhere(" DATE(created) >= :dateFrom", array(':dateFrom' => date("Y-m-d", strtotime($dateFrom))));
        }
         
        if(!empty($dateTo)){
            $commandBase->andWhere(" DATE(created) <= :dateTo", array(':dateTo' => date("Y-m-d", strtotime($dateTo))));
        }
         
        if(!empty($userId)){
            $commandBase->andWhere(" idUser = :userId", array(':userId' => $userId));
        }
         
        return $commandBase->queryAll(true);
    }
	
	public static function myLastAnswersCount() {
		$commandData =  Yii::app()->db->createCommand();
		$commandData->select("a.id");
		$commandData->from(App7020Answer::model()->tableName() . ' a');
		$commandData->leftJoin(App7020Question::model()->tableName() . ' q', 'q.id = a.idQuestion');
		$commandData->andWhere('a.idUser =' . Yii::app()->user->idst);
		$commandData->group('a.idQuestion');
		$count = count($commandData->queryAll());  
		return $count;

	}
	
	/**
	 * Get the answers related to a LO
	 * @param type $loId
	 * @return array
	 */
	public static function getAnswersByLO($loId){
		$commandData =  Yii::app()->db->createCommand();
		
		$commandData->select('a.*');
		$commandData->from(App7020Answer::model()->tableName()." a");
		$commandData->join(App7020Question::model()->tableName()." q", 'q.id = a.idQuestion');
		$commandData->where("q.idLearningObject = :loId", array(':loId' => $loId));
		return $commandData->queryAll();
	}

	public function afterDelete() {
	
		Yii::app()->event->raise(EventManager::EVENT_APP7020_ANSWER_DELETED, new DEvent($this, array(
			'model'	=> $this,
		)));
	
		parent::afterDelete();
		
	}
	

	/**
	 * Multi-purpose SQL data provider, used by REST-API. 
	 * It should be one-for-all, adding more and more filtering and so on.
	 * 
	 * @param array $params Array of arbitrary parameters, usually comming from REST request
	 * @param integer|array $idQuestion Question ID or array of question IDs
	 * 
	 * @return CSqlDataProvider
	 */
	public static function sqlRestDataProvider($params, $idQuestion=false) {
	    
	    // Script level caching, because we  might be called several times during one PHP run
	    static $cache = array();
	    $cacheKey = md5(json_encode(func_get_args()));
	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }
	     
	    // Data provider parameters
	    $dpParams = array();
	    
	    // BASE
	    $commandBase = Yii::app()->db->createCommand();
	    
	    $commandBase->from("app7020_answer a");
	    $commandBase->join("app7020_question q", "q.id=a.idQuestion");
	    $commandBase->join("core_user u", "u.idst=a.idUser");
	    
	    // FILTER
	    if ($idQuestion !== false) {
	        if (is_array($idQuestion)) {
	            $commandBase->andWhere(array("IN", "q.id", $idQuestion));
	        }
	        else {
	           $dpParams[":idQuestion"] = (int) $idQuestion;
	           $commandBase->andWhere("q.id = :idQuestion");
	        }
	    }
	
	    // CLONE
	    $commandData       = clone $commandBase;
	    $commandCounter    = clone $commandBase;
	    
	    // COUNT
	    $commandCounter->select('count(a.id)');
	    $numRecords = $commandCounter->queryScalar($dpParams);
	    
	    // SELECT
        $selectArray = array(
            "a.*", 
            "u.userid", 
	        "u.firstname", 
	        "u.lastname", 
            "u.avatar", 
            "(SELECT COUNT(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 1) as likes", 
            "(SELECT COUNT(idAnswer) From app7020_answer_like WHERE idAnswer = a.id AND type = 2) as dislikes",
	    );
	    $commandData->select($selectArray);
	    
	    // ORDER
        $commandData->order = "a.created DESC";
	
	    // Resolve pagination    
    	$pageSize = isset($params['count']) ? (int) $params['count'] : Settings::get('elements_per_page', 10);
    	// If there IS "from", we do NOT use the native Data Provider pagination. Instead, we set offset/limit
		if (isset($params['from'])) {
		    $pagination = false;
		    $commandData->limit($pageSize, $params['from']);
		}
		// Otherwise, just use the native data provider pagination
		else {
		    $pagination = array(
		        'pagesize' => $pageSize, 
		    );
		}
	    
	    $config = array(
	        'totalItemCount'   => $numRecords,
	        'pagination'       => $pagination,
	        'params'           => $dpParams,
	    );
	    
	    $dataProvider = new CSqlDataProvider($commandData, $config);
	    
	    $cache[$cacheKey] = $dataProvider;
	    
	    return $dataProvider;
	}
	
	
	/**
	 * Get all Answers for given user, grouped by day/month
	 * @param int $userId
	 * @param int $timeframe
	 * @return array
	 */
	public static function getAnswersCountByPeriod($userId, $timeframe) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020Answer::model()->tableName());
		$commandBase->andWhere("idUser = :idUser");
		$commandBase->andWhere("created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()");
		if ($timeframe == 365) {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m') AS period, COUNT(*) AS count");
			$commandBase->group("DATE_FORMAT(created, '%m')");
		} else {
			$commandBase->select("DATE_FORMAT(created, '%Y-%m-%d') AS period, COUNT(*) AS count");
			$commandBase->group('DATE(created)');
		}
		$commandBase->order('created');
		
		return $commandBase->queryAll(true, array(':idUser'=>$userId, ':timeInterval'=>$timeframe ));
	}
	
		static public function getUserByAnswerId($answerId) {
		$command = Yii::app()->db->createCommand();
		$command->select("idUser ");
		$command->from("app7020_answer");
		$command->where("id = " . $answerId);

		return $command->queryRow();
	}
}
