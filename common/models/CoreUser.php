<?php

/**
 * This is the model class for table "core_user".
 * The followings are the available columns in table 'core_user':
 * @property integer $idst
 * @property string $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $pass
 * @property string $email
 * @property string $avatar
 * @property string $signature
 * @property integer $level
 * @property string $lastenter
 * @property integer $valid
 * @property string $pwd_expire_at
 * @property integer $force_change
 * @property string $register_date
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $linkedin_id
 * @property string $google_id
 * @property integer $privacy_policy
 * @property string $suspend_date
 * @property string $saas_expire_date
 * @property string $saas_status
 * @property string $saas_helpdesk_user
 * @property string $containChildren
 * @property string $expiration
 * @property string $email_code
 * @property string $requested_on
 * @property integer $email_status
 * @property string $recent_search
 * @property integer $password_changed
 *
 * The followings are the available model relations:
 * @property EcommerceTransaction[] transactions
 * @property CoreUserBilling[] billingInfos
 * @property CoreUserOrigin[] $coreUserOrigins
 * @property LearningCommontrack[] $learningCommontracks
 * @property LearningCourseCoachingSession[] $coachingSessionsAsCoach
 * @property LearningCourseCoachingSessionUser[] $coachingSessionAssignmentsAsStudent
 * @property LearningCourseuser[] $learningCourseusers
 * @property LearningCourseDateUser[] $learningCourseDateUsers
 * @property CoreSettingUser[] $coreUserSettings
 * @property ConferenceRoom[] $conferenceRooms
 * @property CoreNotification[] $notifications
 * @property CoreJob[] $jobs
 * @property App7020TopicExpert[] $app7020TopicExpertLinks
 * @property App7020Answer[] $app7020Answers
 * @property App7020AnswerLike[] $app7020AnswerLikes
 * @property App7020Question[] $app7020Questions
 * @property App7020QuestionFollow[] $app7020QuestionFollows
 *
 *
 */
class CoreUser extends AuditActiveRecord {

	// Used to cache the list of all Org Chart Nodes between loops, if any
	protected static $allOrgChartNodes = array();

    // Used to cache the god admins
    protected static $allGodAdmins = array();

    protected static $_fieldsCache = false;

	const USER_STATUS_ACTIVE = 1;
	const USER_STATUS_SUSPENDED = 2;

	const STATUS_VALID = 1;
	const STATUS_NOTVALID = 0;

	public $new_password;
	public $new_password_repeat;

	// for sorting
	public $userLevel;
	public $userLanguage;

	public $chartGroups = array();
	public $_additionalFields = null;
    private $_additionalFieldsChanged = false;

    protected $orgChartGroupsList;

	public $search_input, $level, $status, $sessionTime, $coachingSessionFilter, $deadline; //for search purposes only
	public $order_by;

	public $containChildren; // filter if true contain users from children nodes
	public $confirm; // filter if true contain users from children nodes

	// Used when needed (like in Notifications)
	public $passwordPlainText = false;

	public $powerUserManager;

	public $timestampAttributes = array('lastenter medium', 'pwd_expire_at medium', 'register_date medium', 'suspend_date medium');
	public $dateAttributes = array('saas_expire_date', 'expiration');

	// Set this MODEL's attribute to true to skip Additional fields validation
	// This is to allow PER MODEL additional fields validation process
	public $skipAdditionalFieldsValidation = false;

	//For forcing direct group autoassign run
	public $runGroupAutoassign = false;

	/**
	 * This is used to check register_date field, which should only be assigned when creating user and then never change
	 * @var null
	 */
	protected $keepRegisterDate = null;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUser the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

    /**
     * Retrieving user entries for additional fields for a collection of users, specified by id
     * @example $nested == false - DEFAULT
     * result: [
     *    [
     *        {userId} => [
     *            {fieldId} => <scalar> {value},
     *            ...
     *        ],
     *        ...
     *    ],
     *    ...
     * ]
     *
     * @example $nested == true
     * result: [
     *    {user_id} => [
     *        {field_id} => <scalar> {value},
     *        {field_id} => <scalar> {value},
     *        ...
     *    ],
     *    ...
     * ]
     *
     * @param array $userIds
     * @param boolean $nested = false - return the values nested - see @example above
     * @param boolean $valueOnly = false - force return user the value as scalar value
     * @return array
     */
    public static function getAdditionalFieldEntryValuesArray(array $userIds, $nested = false, $valueOnly = false)
    {
        $fieldValues = static::getAdditionalFieldValuesForUsers($userIds, true);

        // Return empty array if there are no results
        if (empty($fieldValues) === true) {
            return [];
        }

        // Indexing the result is not needed
        if ($nested === false) {
            return $fieldValues;
        }

        return static::convertAdditionalFieldValueInstancesToArray($fieldValues);
    }

    /**
     * Converts a passed collection of additional field user entries to an array representation of the data
     *
     * @param array $fieldValues - the instances of additional fields user entries
     * @return array
     */
    public static function convertAdditionalFieldValueInstancesToArray(array $fieldValues)
    {
        // First level indexing
        $grouped = [];

        // Nesting additional field user entry details
        foreach ($fieldValues as $userId => $entry) {
            if (is_array($entry) === false) {
                $entry = $entry->getAttributes();
            }

            if (isset($entry['id_user']) === false) {
                $grouped[$userId] = $entry;
                continue;
            }

            unset($entry['id_user']);

            $vals = array_values($entry);
            $keys = array_keys($entry);
            $keys = array_map(function($key) {
                // $key = str_replace('field_', '', $key);

                // Alternative to string replace for performance enhancement, such as
                // we know the length and the string itself that needs to be removed
                $key = substr($key, 6);
                return intval($key);
            }, $keys);

            $grouped[$userId] = array_combine($keys, $vals);
        }

        return $grouped;
    }

    /**
     * Retrieving additional field user entry values for the specified by id users
     *
     * @param array $userIds - the ids of the users
     * @param bool $returnAsArray - force return the result as an array
     * @return array
     */
    public static function getAdditionalFieldValuesForUsers(array $userIds, $returnAsArray = false)
    {
        return CoreUserField::getValuesForUsers($userIds, $returnAsArray);
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_user';
	}

	public function behaviors()
	{
		return array(
			'modelSelect' => array(
				'class' => 'ModelSelectBehavior'
			),
			'UserBehavior' => array(
				'class' => 'UserBehavior'
			),
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => $this->timestampAttributes,
				'dateAttributes' => $this->dateAttributes
			)
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('userid', 'uniqueUserid', 'on' => 'create, createApi, update, updateApi, import, selfEditProfile'),
			array('email', 'unique', 'except' => 'import, create, createApi, update, updateApi, changePassword, selfEditProfile'),
			array('email', 'validateIfChanged', 'on' => 'selfEditProfile'),
			array('userid', 'required', 'except' => 'massiveUpdate'),
			array('idst, level, valid, force_change, privacy_policy, containChildren, email_status, password_changed', 'numerical', 'integerOnly' => true),
			array(
				'userid, firstname, lastname, email, avatar, facebook_id, twitter_id, linkedin_id, google_id',
				'length',
				'max' => 255
			),
			array('pass', 'length', 'max' => 64),
			array('saas_status', 'length', 'max' => 30),
			array('saas_helpdesk_user', 'length', 'max' => 120),
			array('lastenter, pwd_expire_at, register_date, suspend_date, saas_expire_date, containChildren, expiration', 'safe'),
			// igor/ new rules
			array('email', 'email'),
			Yii::app()->user->getIsUser() //It's not required if the user is NORMAL user, because of the French laws!
				? array('email', 'required',  'except' => 'selfEditProfile, changePassword')
				: array('email', 'required',  'on' => 'selfEditProfile'),
			array('new_password', 'required', 'on' => 'create, changePassword'),
			array('new_password_repeat', 'required', 'on' => 'create, changePassword'),
			array('new_password', 'compare', 'compareAttribute' => 'new_password_repeat', 'on' => array('create', 'createApi', 'update', 'updateApi', 'massiveUpdate', 'changePassword', 'selfEditProfile')),
			array('new_password', 'unsafe', 'except' => array('create', 'createApi', 'update', 'changePassword', 'selfEditProfile')),
			array('new_password_repeat, chartGroups', 'safe', 'on' => array('create', 'createApi', 'update', 'updateApi', 'changePassword', 'selfEditProfile')),
            array('new_password', 'passwordValidatePassword'),
//          array('new_password', 'passwordDefaultValidation'),
//			array('new_password', 'passwordValidateAlphaNumeric'),
//			array('new_password', 'passwordDictionaryCheck'),
//			array('new_password', 'passwordMinCharCheck'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('coachingSessionFilter, search_input, order_by, level, status, deadline, idst, userid, firstname, lastname, pass, email, avatar, signature, level, lastenter, valid, containChildren, pwd_expire_at, force_change, register_date, expiration, facebook_id, twitter_id, linkedin_id, google_id, privacy_policy, suspend_date, saas_expire_date, saas_status, saas_helpdesk_user, requested_on, email_code, email_status',
				'safe',
				'on' => array('search', 'massiveUpdate')
			),
		);

		// call event for check if the 'skip mandatory fields' option is enabled
		$eventForSkipMandatoryFields = new DEvent();
		Yii::app()->event->raise('EventForSkipMandatoryFields', $eventForSkipMandatoryFields);
		if (!$eventForSkipMandatoryFields->shouldPerformAsDefault() || $this->skipAdditionalFieldsValidation) {
			// the option is on, so skip validation of additional fields
			$rules[] = array('additionalFields', 'safe');
		} else {
			$rules[] = array('additionalFields', 'validateAdditionalFields', 'on' => 'create, update, import, selfEditProfile');
		}
		$event = new DEvent($this, array('user' => $this));
		Yii::app()->event->raise('BeforeCoreUserValidationRules', $event);
		if(!$event->shouldPerformAsDefault() && is_array($event->return_value['rules']))
			$rules = $event->return_value['rules'];

		return $rules;
	}

	/**
	 * Validation rule that is to be used in AR models to remove unneeded JS code
	 *
	 * @param string $attribute the name of the attribute to be validated
	 * @param array $params options specified in the validation rule
	 *
	 * @return boolean Continue the validation process?
	 */
	public function purifyModelAttribute($attribute, $params)
	{
		$this->setAttribute($attribute, Yii::app()->htmlpurifier->purify($this->getAttribute($attribute)));
		return true;
	}


	/**
	 * If the user changes his email and it's used by another user -> validate
	 * While currently administrators can create users with equal emails
	 */
	public function validateIfChanged($attribute)
	{
		//Check if the users leave the email field empty
		if (!$this->email && empty($this->email)) {
			return;
		}
		if ($this->hasAttributeChanged($attribute) && CoreUser::isEmailUsed($this->$attribute, $this->email)) {
			$this->addError($attribute, Yii::t('user', 'Email {attribute} has already been taken', array('{attribute}' => $this->$attribute)));
		}
	}




	/**
	 * Check if the email is already in use
	 * @param $email
	 * @param $userId
	 * @return bool
	 */
	public static function isEmailUsed($email, $userId)
	{
		$model = CoreUser::model()->find(array(
			'condition' => 'email = :email AND idst != :idst',
			'params' => array(':email' => $email, ':idst' => $userId)
		));
		return $model ? true : false;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

		$relationships = array(
			'transactions' => array(self::HAS_MANY, 'EcommerceTransaction', 'id_user'),
			'billingInfos' => array(self::HAS_MANY, 'CoreUserBilling', 'idst'),
			'coreUserOrigins' => array(self::HAS_MANY, 'CoreUserOrigin', 'user_id'),
			'learningCommontracks' => array(self::HAS_MANY, 'LearningCommontrack', 'idUser'),
			'learningCourseusers' => array(self::HAS_MANY, 'LearningCourseuser', 'idUser'),
			'learningCourses' => array(self::HAS_MANY, 'LearningCourse', array('idCourse', 'idCourse'), 'through' => 'learningCourseusers', 'joinType' => 'INNER JOIN'),
			//'learningCourseDateUsers' => array(self::HAS_MANY, 'LearningCourseDateUser', 'id_user'),
			'learningScormItemsTrack' => array(self::HAS_ONE, 'LearningScormItemsTrack', 'idUser'), //used in scorm report search in Player
			'learningScormTracking' => array(self::HAS_ONE, 'LearningScormTracking', 'idUser'), //used in scorm report search in Player
			'coreUserSettings' => array(self::HAS_MANY, 'CoreSettingUser', 'id_user'),
			'userLanguageSetting' => array(self::HAS_ONE, 'CoreSettingUser', 'id_user', 'on' => "userLanguageSetting.path_name = 'ui.language'"),

			'memberGroups' => array(self::HAS_MANY, 'CoreGroupMembers', 'idstMember'),
			//'adminLevel' =>array(self::HAS_ONE, 'CoreGroup', array('idst' => 'idst'), 'through' => 'memberGroups', 'condition' => 'adminLevel.groupid LIKE "%framework/level%" OR adminLevel.groupid="/oc_0" OR adminLevel.groupid="/ocd_0"'),
			'adminLevel' =>array(self::HAS_ONE, 'CoreGroup', array('idst' => 'idst'), 'through' => 'memberGroups', 'condition' => 'adminLevel.groupid LIKE "%framework/level%"'),


			// 'profileMemberGroups' => array(self::HAS_MANY, 'CoreGroupMembers', 'idstMember'),
			// 'adminProfile' => array(self::BELONGS_TO, 'CoreGroup', array('idst' => 'idst')/*, 'through' => 'profileMemberGroups'*/, 'on' => 'adminProfile.groupid LIKE "%/framework/adminrules%"'),
			// 'adminProfile' => array(self::HAS_MANY, 'CoreGroup', array('idst' => 'idst')/*, 'through' => 'profileMemberGroups'*/, 'on' => 'adminProfile.groupid LIKE "%/framework/adminrules%"'),
			'adminProfile' => array(self::MANY_MANY, 'CoreGroup', 'core_group_members(idstMember, idst)', 'on' => 'adminProfile.groupid LIKE "%/framework/adminrules%"'),

			'orgChartGroups' => array(self::HAS_MANY, 'CoreOrgChartTree', array('idst' => 'idst_ocd'), 'through' => 'memberGroups'),
			'language' => array(self::HAS_ONE, 'CoreLangLanguage', array('value' => 'lang_code'), 'through' => 'userLanguageSetting', 'joinType' => 'LEFT JOIN'),
			'conferenceRooms' => array(self::HAS_MANY, 'ConferenceRoom', 'idSt'),
			'learningCourse' => array(self::HAS_ONE, 'LearningCourseuser', 'idUser'), //used for enrollments in Player
			'learningTracksession' => array(self::HAS_MANY, 'LearningTracksession', 'idUser'), //used for reports in Player
			'learningTesttrack' => array(self::HAS_ONE, 'LearningTesttrack', 'idUser'), //used for reports in Player
			'learningCommontrack' => array(self::HAS_ONE, 'LearningCommontrack', 'idUser'), //used for reports in Player
			'groups' => array(self::MANY_MANY, 'CoreGroup', 'core_group_members(idstMember, idst)'),
			'onlyAssignedUsers' => array(self::HAS_MANY, 'CoreUserPU', 'user_id'), // used for Power User autocomplete
			'allAssignedUsers' => array(self::HAS_MANY, 'CoreUserPU', 'puser_id'), //add power user all child users
			'manageCourses' => array(self::MANY_MANY, 'LearningCourse', 'core_user_pu_course(puser_id, course_id)'),
			'manageCoursepaths' => array(self::MANY_MANY, 'LearningCoursepath', 'core_user_pu_coursepath(puser_id, path_id)'),
			'learningCatalogueMember' => array(self::HAS_MANY, 'LearningCatalogueMember', 'idst_member'),

			'ltCourseSession' => array(self::HAS_ONE, 'LtCourseuserSession', 'id_user'), //used for enrollments in session management and Player
			'ltCourseuserSession' => array(self::HAS_ONE, 'LtCourseuserSession', 'id_user'), //used for enrollments in session management and Player
			'learningCourseusersSession' => array(self::HAS_MANY, 'LtCourseuserSession', 'id_user'), //used for enrollments in session management and Player

			// Jobs which author this user is
			'jobs' => array(self::HAS_MANY, 'CoreJob', 'id_author'),

			// Notifications which author this user is
			'notifications' => array(self::HAS_MANY, 'CoreNotification', 'id_author'),

			// List of coaching session models where THIS user is a coach
			'coachingSessionsAsCoach'   => array(self::HAS_MANY, 'LearningCourseCoachingSession', 'idCoach'),
			// List of coaching user/session models THIS user is assigned to
			'coachingSessionAssignmentsAsStudent' => array(self::HAS_MANY, 'LearningCourseCoachingSessionUser', 'idUser'),

			// List of expert-topic links (middle table)
			'app7020TopicExpertLinks' => array(self::HAS_MANY, 'App7020TopicExpert', 'idExpert'),
			// Through relation (1 expert (user) -> many topic links -> many topics
			// Array of models of CONTENTS related to THIS user (expert)
			//'app7020Topics'	=> array(self::HAS_MANY, 'App7020TopicTree', array('idTopic'=>'id'), 'through'=>'app7020TopicExpertLinks'),

			'app7020Answers' => array(self::HAS_MANY, 'App7020Answer', 'idUser'),
			'app7020AnswerLikes' => array(self::HAS_MANY, 'App7020AnswerLike', 'idUser'),
			'app7020Questions' => array(self::HAS_MANY, 'App7020Question', 'idUser'),
			'app7020QuestionFollows' => array(self::HAS_MANY, 'App7020QuestionFollow', 'idUser'),
		);

        return $relationships;
	}

    /**
     * Returns the correct identifier for additional field of type 'upload', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getUploadFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_UPLOAD;
    }

    /**
     * Returns the correct identifier for additional field of type 'date', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getDateFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_DATE;
    }

    /**
     * Returns the correct identifier for additional field of type 'country', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getCountryFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_COUNTRY;
    }

    /**
     * Returns the correct identifier for additional field of type 'dropdown', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getDropdownFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_DROPDOWN;
    }

    /**
     * Returns the correct identifier for additional field of type 'textfield', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getTextfieldFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_TEXT;
    }

    /**
     * Returns the correct identifier for additional field of type 'yesno', depending on the
     * mode used for additional fields relationship
     *
     * @return string
     * @deprecated Only CoreUserField and the related models should be used from now on
     */
    public static function getYesnoFieldTypeIdentifier()
    {
        return CoreUserField::TYPE_YESNO;
    }

    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idUser' => Yii::t('standard', 'User unique ID'), // 'Idst',
			'idst' => 'Idst', // 'Idst',
			'userid' => Yii::t('standard', '_USERNAME'), // 'Username',
			'firstname' => Yii::t('standard', '_FIRSTNAME'), // 'First name',
			'lastname' => Yii::t('standard', '_LASTNAME'), // 'Last name',
			'pass' => Yii::t('standard', '_PASSWORD'), // 'Password',
			'email' => Yii::t('standard', '_EMAIL'), // 'Email',
			'avatar' => Yii::t('classroom', '_PHOTO'), // 'Avatar',
			'signature' => Yii::t('standard', '_SIGNATURE'), // 'Signature',
			'level' => Yii::t('standard', '_LEVEL'), // 'Level',
			'lastenter' => Yii::t('standard', '_DATE_LAST_ACCESS'), // 'Last access',
			'valid' => Yii::t('standard', '_ACTIVE'), // 'Valid',
			'pwd_expire_at' => Yii::t('standard', '_EXPIRATION_DATE'), // 'Pwd Expire At',
			'force_change' => Yii::t('configuration', '_PASS_CHANGE_FIRST_LOGIN'), // 'Force Change',
			'register_date' => Yii::t('report', '_CREATION_DATE'), // 'Registered On',
			'facebook_id' => Yii::t('standard', '_FACEBOOK'), // 'Facebook',
			'twitter_id' => Yii::t('standard', '_TWITTER'), // 'Twitter',
			'linkedin_id' => Yii::t('standard', '_LINKEDIN'), // 'Linkedin',
			'google_id' => Yii::t('standard', '_GOOGLE'), // 'Google',
			'privacy_policy' => Yii::t('login', '_REG_PRIVACY_POLICY'), // 'Privacy Policy',
			'suspend_date' => Yii::t('standard', 'Suspend Date'), // 'Suspend Date',
			'saas_expire_date' => 'Saas Expire Date', // 'Saas Expire Date', not used anymore
			'saas_status' => Yii::t('standard', '_STATUS'), // 'Saas Status',
			'saas_helpdesk_user' => 'Saas Helpdesk User', // 'Saas Helpdesk User', not used anymore
			'new_password' => Yii::t('register', '_NEW_PASSWORD'), // 'Password',
			'new_password_repeat' => Yii::t('register', '_RETYPE_PASSWORD'), // 'Retype the password',
			'language.value' => Yii::t('standard', '_LANGUAGE'), // 'Language',
			'timezone.value' => Yii::t('configuration', 'Time zone'), // 'Time zone',
			'date_format.value' => '', // 'Date format',
			'date_format_locale.value' => Yii::t('standard', '_DATE_FORMAT'),
			'expiration' => Yii::t('standard', 'Expiration'),
			'email_status' => Yii::t('standard', 'Email validation status'),
            'recent_search' => Yii::t('standard', 'Recent Search'),
		);
	}



	/**
	 * Returns admin permissions
	 * @return array
	 */
	public static function getAdminPermissions()
	{
		$permissions = array(
			'add' => Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/add'),
			'mod' => Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/mod'),
			'del' => Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/del'),
			'view' => Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('/framework/admin/usermanagement/view')
		);
		return $permissions;
	}

    /**
     * Flushing cached additional fields data
     *
     * @return $this
     */
	public function clearAdditionalFieldsCache()
    {
        $this->_additionalFields = null;
        static::$_fieldsCache = false;

        return $this;
    }

    /**
     * Returns an array of model instances of the additional field objects.
     *
     * @param boolean $forceReturnWithUserEntryValues = false - forcing the return results to contain user values
     * @return array
     */
    public function getAdditionalFields($forceReturnWithUserEntryValues = false)
    {
        if ($this->_additionalFields !== null) {
            return $this->_additionalFields;
        }

        // Deciding if user values for the additional fields should be loaded
        $shouldReturnUserValues = (!$this->isNewRecord || $this->scenario == 'fromTempUser') || $forceReturnWithUserEntryValues;

        $this->_additionalFields = CoreUserField::getForUser($this, $shouldReturnUserValues);

        return $this->_additionalFields;
    }

    /**
     * Returns a collection of additional fields
     *
     * @param string $language = null - the language to get the fields for, if not the automatically loaded one
     * @return array
     */
    public static function getAdditionalFieldsByLanguage($language = null)
    {
        $language = $language ?: false;
        $fields = CoreUserField::model()->language($language)->findAll();

        return $fields;
    }

    /**
     * Returns an array of IDs of the additional fields, filtered by branch.
     * Also applying some basic filtering based THIS User Model, BUT NOT real visibility per-se!
     * For true visibility filtering, use CoreUserField::getForUser().
     * 
     * Mostly used for administrative operations where fields must filtered by branch.
     *
     * @param array $nodes - ids of the branches to filter by
     * @return array
     */
	public function getVisibleAdditionalFieldsByBranch($nodes = array())
    {
        return CoreUserField::getIdsForUserByBranch($this, $nodes);
	}

	/**
	 * Used during saving user profile to set additional fields from POST request
     * Updates the values of the user additional fields caches in a model's property
     * NOT SAVING any changes in the database tables!
	 *
	 * @param array $additionalFields
     * @return void
	 */
	public function setAdditionalFieldValues($additionalFields, $uploadedFiles = array())
    {
        // Check if there are additional fields related to the user - if not - stop further method code execution
        $currentFields = $this->getAdditionalFields();
        if (count($currentFields) === 0) {
            return;
        }

        // Update additional field values with the passed details
        foreach ($this->getAdditionalFields() as $i => $field) {
            $fieldId = $field->getFieldId();

            switch ($field->getFieldType()) {
                // In case of UPLOAD field, if there is NO uploaded file, we set the field value to NULL (detected later during save)
                case CoreUserField::TYPE_UPLOAD:
                    if (isset($uploadedFiles[$fieldId])) {
						$this->_additionalFields[$i]->userEntry = $uploadedFiles[$fieldId];
					} else {
					    $this->_additionalFields[$i]->userEntry = null;
					}
                    break;

                // Standard handling: just use the incoming [value]
                default:
                    if (isset($additionalFields[$fieldId]['value'])) { // NOTE: field value MUST BE physically present in the input !!!
                        //If the user modified his additional fields, enable flag for applying Group auto assign
                        if ($this->isAdditionalFieldModified($this->_additionalFields[$i], $additionalFields[$fieldId])) {
                            $this->_additionalFieldsChanged = true;
                        }
						$this->_additionalFields[$i]->userEntry = $additionalFields[$fieldId]['value'];
					}
                    break;
            }
        }
	}

	public function removeUploadsOnRequested()
	{
		foreach ($_POST as $postKey => $postValue) {
			if (strpos($postKey, 'toRemoveUpload') === 0) {
				if ($postValue === 'true') {
					$randomFieldKey = str_replace('toRemoveUpload', '', $postKey);
					$arr = explode('___', $randomFieldKey);
					$fieldId = $arr[1];

					/*
					$result =
						Yii::app()->db->createCommand()->
							delete(
								'core_field_userentry',
								'id_common = :id_common and id_user = :id_user',
								array(
									':id_common' => $fieldId,
									':id_user' => $this->idst
								)
							);
					*/
					// NOTE: deleting the row will make the mandatory fields checking not working anymore ... so just fill an empty value instead
                    // Clearing out user entries for additional fields
                    $this->unsetAdditionalFieldValue($fieldId);
				}
			}
		}
	}

    /**
     * Updates the related tables' column values for user entry effectively leaving it empty
     * SAVES the update values immediately, not at saving the user instance
     *
     * @param string|integer $fieldId
     * @return CoreUser
     */
	public function unsetAdditionalFieldValue($fieldId)
    {
        $params = [':id_user' => $this->idst];
        $query  = 'UPDATE `' . CoreUserFieldValue::model()->tableName() . '`' . ' SET `field_'  . $fieldId .  '` = NULL WHERE `id_user` = :id_user';

        Yii::app()->db->createCommand($query)->execute($params);

        return $this;
    }

	/**
	 * Function is checking if the user change his additional fields.
	 * It's not working for field type: file!!!
	 *
	 * @param Array<CoreUserField> $myField Additional fields of the user
	 * @param CoreUserField $newField The new additional field to be changed
	 * @return boolean Is user entry of the additional field is changed.
	 */
	private function isAdditionalFieldModified($myField, $newField)
	{
		$type_field = $myField->getFieldType();
		$modified = false;

		switch ($type_field) {
			case CoreUserField::TYPE_DATE:
				$mySqlDate = date('Y-m-d', strtotime($myField->userEntry));
				$newSqlDate = date('Y-m-d', strtotime($newField['value']));
				if ($mySqlDate != $newSqlDate) {
					$modified = true;
				}
				break;
			default:
				if ($myField->userEntry !== $newField['value']) {
					$modified = true;
				}
				break;
		}

		return $modified;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idst', $this->idst);
		$criteria->compare('userid', $this->userid, true);
		$criteria->compare('firstname', $this->firstname, true);
		$criteria->compare('lastname', $this->lastname, true);
		$criteria->compare('pass', $this->pass, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('avatar', $this->avatar, true);
		$criteria->compare('signature', $this->signature, true);
		$criteria->compare('level', $this->level);
		$criteria->compare('lastenter', Yii::app()->localtime->fromLocalDateTime($this->lastenter), true);
		$criteria->compare('valid', $this->valid);
		$criteria->compare('pwd_expire_at', Yii::app()->localtime->fromLocalDateTime($this->pwd_expire_at), true);
		$criteria->compare('force_change', $this->force_change);
		$criteria->compare('register_date', Yii::app()->localtime->fromLocalDateTime($this->register_date), true);
		$criteria->compare('facebook_id', $this->facebook_id, true);
		$criteria->compare('twitter_id', $this->twitter_id, true);
		$criteria->compare('linkedin_id', $this->linkedin_id, true);
		$criteria->compare('google_id', $this->google_id, true);
		$criteria->compare('privacy_policy', $this->privacy_policy);
		$criteria->compare('suspend_date', Yii::app()->localtime->fromLocalDateTime($this->suspend_date), true);
		$criteria->compare('saas_expire_date', Yii::app()->localtime->fromLocalDate($this->saas_expire_date), true);
		$criteria->compare('saas_status', $this->saas_status, true);
		$criteria->compare('saas_helpdesk_user', $this->saas_helpdesk_user, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function searchCriteria($courseId, $withTracksession = null, $userId = null) {
		$criteria = new CDbCriteria;
		$criteria->compare('learningCourse.level', $this->level);
		$criteria->compare('learningCourse.status', $this->status);

		if($userId)
			$criteria->compare('t.idst', $userId);

                  $pUserRights = Yii::app()->user->checkPURights($courseId);

		if($pUserRights->isPu && !$pUserRights->isInstructor){
			// If the current user is a Power User limit the resulting stats
			// to include only users assigned to him
            // if the Power User has a role (Instructor) will not get in here.

			// Get IDs array of users assigned to current Power User
			$puAssignedUsers = CoreUserPU::getList();

			$criteria->addInCondition('t.idst', $puAssignedUsers);
		}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $courseId, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($courseId);
			if(!empty($instructorUsers))
				$criteria->addInCondition('t.idst', $instructorUsers);
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		$criteria->select = array('t.*');

		if ($withTracksession) {
			$criteria->with['learningTracksession'] = array('on' => 'learningTracksession.idCourse = '.$courseId);
			$criteria->select[] = 'SUM(UNIX_TIMESTAMP(learningTracksession.lastTime) - UNIX_TIMESTAMP(learningTracksession.enterTime)) AS sessionTime';
			$criteria->group = 't.idst';
			$criteria->together = true;
		}

		//get all users for only "this" course
		$criteria->with['learningCourse'] = array(
			'condition' => 'learningCourse.idCourse = '.$courseId
		);

		return $criteria;
	}

	/**
	 * @param $courseId
	 * @param null $withTracksession
	 * @return CActiveDataProvider
	 */
	public function searchUsersByCourse($courseId, $withTracksession = null) {
		$criteria = $this->searchCriteria($courseId, $withTracksession);

        if ($this->powerUserManager !== null) {
            $criteria->with['onlyAssignedUsers'] = array(
				'joinType' => 'INNER JOIN',
				'on' => 'onlyAssignedUsers.puser_id = :puser_id',
				'params' => array(
					':puser_id' => $this->powerUserManager,
				),
            );
        }

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * @param $courseId
	 * @param $scormId
	 * @return CDbCriteria
	 */
	public function searchScormUsersCriteria($courseId, $scormId)
	{
		$criteria = $this->searchCriteria($courseId, true);
		$scormCriteria = new CDbCriteria();
		$scormCriteria->with = array(
				'learningScormItemsTrack' => array('on' => 'learningScormItemsTrack.idscorm_item = '.$scormId),
				'learningScormTracking' => array('on' => 'learningScormTracking.idscorm_item = '.$scormId)
			);
		$criteria->mergeWith($scormCriteria);
		return $criteria;
	}

	/**
	 * @param $courseId
	 * @param $organizationId
	 * @return CDbCriteria
	 */
	public function searchTestUsersCriteria($courseId, $organizationId)
	{
		$criteria = $this->searchCriteria($courseId, false);

		$testCriteria = new CDbCriteria();
		$organizationModel = LearningOrganization::model()->findByPk($organizationId);
		if($organizationModel && $organizationModel->repositoryObject && $organizationModel->repositoryObject->shared_tracking) {
			// Collect all course placeholder for the passed object
			$organizationIds = $organizationModel->objectVersion->getCoursePlaceholdersForThisObject($this->idst);
			$testCriteria->with = array('learningTesttrack' => array('on' => 'learningTesttrack.idReference IN ('.implode(",", $organizationIds).')', 'joinType'=>'INNER JOIN'));
		} else
			$testCriteria->with = array('learningTesttrack' => array('on' => 'learningTesttrack.idReference = '.$organizationId, 'joinType'=>'INNER JOIN'));

		$criteria->mergeWith($testCriteria);
		return $criteria;
	}

	/**
	 * @param $courseId
	 * @param $organizationId
	 * @return CDbCriteria
	 */
	public function searchCommonUsersCriteria($courseId, $organizationId)
	{
		$criteria = $this->searchCriteria($courseId, false);
		$commonCriteria = new CDbCriteria();
		$commonCriteria->with = array(
			'learningCommontrack' => array(
				'on' => 'learningCommontrack.idReference = '.$organizationId, 'joinType'=>'INNER JOIN'
			),
			'learningCommontrack.organization.objectVersion'
		);
		$criteria->mergeWith($commonCriteria);
		return $criteria;
	}

	/**
	 * @param $courseId
	 * @param $organizationId
	 * @return CActiveDataProvider
	 */
	public function searchUsersByCommonTrack($courseId, $organizationId)
	{
		$criteria = $this->searchCommonUsersCriteria($courseId, $organizationId);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * @param $courseId
	 * @param $scormId
	 * @return CActiveDataProvider
	 */
	public function searchUsersByCourseAndScorm($courseId, $scormId)
	{
		$criteria = $this->searchScormUsersCriteria($courseId, $scormId);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * @param $courseId
	 * @param $organizationId
	 * @return CActiveDataProvider
	 */
	public function searchUsersByCourseAndTest($courseId, $organizationId)
	{
		$criteria = $this->searchTestUsersCriteria($courseId, $organizationId);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
				'pageSize' => 10
//				'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

    /**
     * @param $courseId
     * @param $organizationId
     * @return CDbCriteria
     */
    public function searchUsersByDeliverableCriteria($courseId, $organizationId)
    {
        $criteria = $this->searchCriteria($courseId, false);
        $commonCriteria = new CDbCriteria();
        $commonCriteria->with = array(
            'learningCommontrack' => array(
                'on' => 'learningCommontrack.idReference = '.$organizationId,
                'joinType'=>'INNER JOIN',
                'condition' => 'learningCommontrack.objectType = :objectType',
                'params' => array(
                    ':objectType' => LearningOrganization::OBJECT_TYPE_DELIVERABLE
                )
            )
        );
        $criteria->mergeWith($commonCriteria);
        return $criteria;
    }

	/**
	 * @param $courseId
     * @param $organizationModel
	 * @return CActiveDataProvider
	 */
	public function searchUsersByDeliverableObject($courseId, $organizationModel)
	{
        $criteria = $this->searchUsersByDeliverableCriteria($courseId, $organizationModel->idOrg);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * @param $courseId
	 * @return array
	 */
	public static function getAllUsersByCourse($courseId)
	{
		static $cache = array();

		if(isset($cache[$courseId])){
			return $cache[$courseId];
		}

		$result = array();
		$criteria = self::model()->searchCriteria($courseId);
		$models = self::model()->findAll($criteria);
		foreach ($models as $model)
		{
			$result[$model->idst] = $model->clearUserId;
		}

		$cache[$courseId] = $result;

		return $result;
	}


    /**
     * Return an array of coaches for a given Course Id, start date to be available after and end date to be available before.
     * The last parameter is for excluding the current session from the check if the coach is available for those dates
     * @param int $idCourse
     * @param string $search_input
     * @param string $start_date
     * @param string $end_date
     * @param int $currentSession
     * @return array
     */
    public function sqlCoachesForCourses($idCourse,$search_input=false,$start_date=false,$end_date=false,$currentSession=false) {
        $commandBase = Yii::app()->db->createCommand()
            ->from('learning_courseuser courseUser')
            ->join('core_user user', 'user.idst=courseUser.idUser')
            ->andWhere('courseUser.idCourse = :idCourse')
            ->andWhere('courseUser.level = :userLevel')
            ->select('idst,userid,firstname,lastname,email');
        $params=array(
            ':idCourse'	    => (int) $idCourse,
            ':userLevel'	=> LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
        );
        if ($search_input){
            $commandBase->andWhere('user.userid LIKE :search');
            $params[':search'] = '%'.$search_input.'%';
        }

        if ($start_date || $end_date) {
            $unavailableIds = LearningCourse::getUnavailableCoachesByDates($idCourse, (($start_date) ? $start_date : false), (($end_date) ? $end_date : false), $currentSession);
            if(is_array($unavailableIds) && !empty($unavailableIds)) {
                $commandBase->andWhere('user.idst NOT IN (' . implode(',',$unavailableIds) . ')');
            }
        }

        //For sorting the table by any of the given column
        $sort = new CSort();
        $sort->attributes = array(
            'name' => array(
                'asc' 	=> 'user.userid',
                'desc'	=> 'user.userid DESC',
            ),
           'firstname' => array(
                'asc' 	=> 'user.firstname',
                'desc'	=> 'user.firstname DESC',
            ),
            'lastname' => array(
                'asc' 	=> 'user.lastname',
                'desc'	=> 'user.lastname DESC',
            ),
            'email' => array(
                'asc' 	=> 'user.email',
                'desc'	=> 'user.email DESC',
            ),
        );
        $sort->defaultOrder = array(
            'name'		=>	CSort::SORT_ASC,
        );
        //To show "Total:[number]"
        $commandCounter = clone $commandBase;
        $commandCounter->select('user.idst');
        $numRecords = count($commandCounter->query($params));


        $config = array(
            'totalItemCount' => $numRecords,
            'sort' => $sort,
            'params' => $params
        );

        $dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
        return $dataProvider;
    }

    /**
	 * Return an array of course Ids  (1,2,3,...) for a given User Id
	 * @param int $idUser
	 * @param boolean $ignoreClassrooms  (count 'classrooms' as NOT subscribed)
	 * @return array
	 */
	public function getSubscribedCoursesList($idUser, $ignoreClassrooms = false) {
		$command = Yii::app()->db->createCommand();
		$command->select('lc.idCourse')->from(LearningCourse::model()->tableName() . ' lc')
				->join(LearningCourseuser::model()->tableName() . ' lcu', 'lc.idCourse=lcu.idCourse AND lcu.idUser = :user', array(':user' => $idUser));
		if ($ignoreClassrooms) {
			$command->andWhere('lc.course_type !=:type', array(':type' => LearningCourse::TYPE_CLASSROOM));
		}
		$subscribedCourseIds = $command->queryColumn();
		return $subscribedCourseIds;
	}

	/**
	 * Get user's full name concatenated
	 * @return string
	 */
	public function getFullName() {
		$userFullName = null;
		if($this->firstname && $this->lastname){
            $show_first_name_first = Settings::get('show_first_name_first', 'off');
            if($show_first_name_first == 'on'){
                $userFullName = $this->firstname. ' '. $this->lastname;
            } else{
                $userFullName = $this->lastname . ' ' . $this->firstname;
            }
		}elseif($this->firstname){
			$userFullName = $this->firstname;
		}else{
			$userFullName = Yii::app()->user->getRelativeUsername($this->userid);
		}
		return $userFullName;
	}

	/**
	 * @param bool|false $firstname
	 * @param bool|false $lastname
	 * @param bool|false $username
	 * @return bool|string
	 */
	public static function getFullnameByNames($firstname = false, $lastname = false, $username = false){
		$userFullName = '';
		if($firstname && $lastname && $firstname != '' && $lastname != ''){
			$show_first_name_first = Settings::get('show_first_name_first', 'off');
			if($show_first_name_first == 'on'){
				$userFullName = $firstname. ' '. $lastname;
			} else{
				$userFullName = $lastname . ' ' . $firstname;
			}
		}elseif(($firstname && $firstname != '') || ($lastname && $lastname != '')){
			if($firstname)
				$userFullName = $firstname;
			else
				$userFullName = $lastname;

		}elseif($username && $username != ''){
			$userFullName = $username;
		}
		return $userFullName;
	}

    /**
     * Get user's full name concatenated (with SQL)
     * @param $id
     * @return string
     */
    public static function getFullNameById($uid) {
        $userFullName = null;
        $reader = Yii::app()->db->createCommand()
            ->select("*")
            ->from(CoreUser::model()->tableName())
            ->where("idst = :uid")
            ->query(array(':uid'=>(int)$uid));
        $result = $reader->read();
        $fName = $result['firstname'];
        $lName = $result['lastname'];
        $userId = $result['userid'];

        if($fName && $lName){
            $show_first_name_first = Settings::get('show_first_name_first', 'off');
            if($show_first_name_first == 'on'){
                $userFullName = $fName. ' '. $lName;
            } else{
                $userFullName = $lName . ' ' . $fName;
            }
        }elseif($lName){
            $userFullName = $lName;
        }elseif($fName){
            $userFullName = $fName;
        }else{
            $userFullName = Yii::app()->user->getRelativeUsername($userId);
        }
        return $userFullName;
    }
    public static function getFirstNameById($uid) {
        $userFullName = null;
        $reader = Yii::app()->db->createCommand()
            ->select("*")
            ->from(CoreUser::model()->tableName())
            ->where("idst = :uid")
            ->query(array(':uid'=>(int)$uid));
        $result = $reader->read();
        $fName = $result['firstname'];
        return $fName;
    }


	public function getClearUserId() {
		if (substr($this->userid, 0, 1) == '/') {
			return substr($this->userid, 1);
		}
		else {
			return $this->userid;
		}
	}

	public function afterFind(){
		parent::afterFind();

		if($this->expiration=='0000-00-00'){
			$this->expiration = null;
		}
	}

	public function beforeSave()
	{

		if(!$this->isNewRecord)
		{

			$register_date_conv = Yii::app()->localtime->fromLocalDateTime($this->register_date, 'medium', LocalTime::DEFAULT_TIME_WIDTH);
			$diff = Yii::app()->localtime->diff($register_date_conv, Yii::app()->localtime->getUTCNow());

			if(($diff >= 0 && $diff <= 60) || ($diff <= 0 && $diff >= -60))
			{
				$db_check = CoreUser::model()->findByPk($this->idst);
				$this->register_date = $db_check->register_date;
			}
		}

		if (parent::beforeSave())
		{
			$attributes = $this->getAttributes();
			foreach($attributes as $key => $attr){
				if($this->$key && $attr && is_string($attr)){
					$this->$key = strip_tags($attr);
				}
			}

			//Trim user data
			$this->userid = trim($this->userid);
			$this->firstname = trim($this->firstname);
			$this->lastname = trim($this->lastname);

			if ($this->isNewRecord) {
				if (!$this->idst) //in case of createFromTempUser() it is already set
				{
					// creation unique idst
					$st = new CoreSt();
					if (!$st->save()) {
						return false;
					}
					$this->idst = $st->idst;
				}
				$this->register_date = Yii::app()->localtime->getUTCNow();
			}
			if ($this->new_password != ''){
				$this->applyNewPassword();
			}
			$this->userid = Yii::app()->user->getAbsoluteUsername($this->userid);

			// saving of additional fields
            $this->saveAdditionalFieldsOnSave();

			if ($this->isNewRecord) {

				/*
				 * This is very special case and intercepts the User creation process::
				 * 		If current user is a Power User and he is not allowed to directly create AND activate users
				 * 		we just create TEMP user which must be Approved by authorized users.
				 * 		Additionally though, we save Brnach assigmnent as it is legitimate choice.
				 *
				 */
				if (Yii::app()->user->getIsAdmin() && ( in_array($this->scenario, array('create', 'import')))) {
					if (!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_user_insert')) {
						$tempUser = new CoreUserTemp('create');
						$tempUser->attributes = $this->attributes;
						$tempUser->userid = Yii::app()->user->getRelativeUsername($this->userid); // NOTE: temp users do not use the "/" in usernames !!!!
						$tempUser->request_on = Yii::app()->localtime->toLocalDateTime();
						$tempUser->create_by_admin = Yii::app()->user->id;
						$randomCode = md5(Yii::app()->user->id . mt_rand().mt_rand().mt_rand());
						$tempUser->random_code =$randomCode;

						// Validate and SAVE TEMP user saving. If it is successful, change the model scenario on the fly.
						// The caller should know what to do about it.
						if ($tempUser->save()) {
							// Save branch assignent of this user. Even though we create a TEMP user,
							// after his activation, he must be part of the intended branch.
							// Normally this is done in CoreUser::afterSave() method, but we
							// cancelling the save process (returning false) and it will not be called.
							$this->saveChartGroups();
							$this->scenario = 'powerUserCreateTempUser';
						}
						return false; // because power user is NOT allowed to actually create the user!!!!!!!
					}
				}
			}else{

				// Is an update (not a new record)

				if($this->hasAttributeChanged('valid')){
					// User has been suspended or unsuspended
					if($this->valid){
						Yii::app()->event->raise(CoreNotification::NTYPE_USER_SUSPENDED, new DEvent($this, array('user'=>$this)));
					}else{
						Yii::app()->event->raise(CoreNotification::NTYPE_USER_UNSUSPENDED, new DEvent($this, array('user'=>$this)));
					}
				}
			}


			return true;
		}
		return false;
	}

    /**
     * Saving additional fields details (user input values) when saving the CoreUser instance
     *
     * @return CoreUser
     */
	private function saveAdditionalFieldsOnSave()
    {
        // Return if there are no additional fields associated to the current user instance
        $additionalFields = $this->getAdditionalFields();
        if (count($additionalFields) === 0) {
            return $this;
        }

        $this->saveAdditionalCoreUserFieldsDataOnSave();

        return $this;
    }

    /**
     * Saving additional fields details, using the CoreUserField and related models fields data source
     *
     * @return CoreUser
     */
    private function saveAdditionalCoreUserFieldsDataOnSave()
    {
        // Load or create user additional field values entry using CoreUserFieldValue model
        $valuesEntry = CoreUserFieldValue::model()->findByAttributes(array('id_user' => $this->idst));

        if ($valuesEntry == null) {
            $valuesEntry = new CoreUserFieldValue;
            $valuesEntry->id_user = $this->idst;
        }

        // For each additional field value set - update the entry
        $fields = $this->getAdditionalFields();
        foreach ($fields as $field) {
            if ($field->userEntry === null) {
                continue;
            }
            
            $columnName = 'field_' . $field->getFieldId();
            
            // If field is UPLOAD, do NOTHING if field value is NULL. Otherwise just set "sourceFiles" array for later handling in CoreUserFieldValue::beforeSave()
            if ($field->type == CoreUserField::TYPE_UPLOAD && $field->userEntry !== null) {
                $valuesEntry->sourceFiles[$columnName] = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $field->userEntry;
            }
            // Otherwise, just set the field value
            else {
                $valuesEntry->{$columnName} = $field->userEntry;
            }
        }

        $valuesEntry->save();


        return $this;
    }

	public function afterSave() {

		// Save group membership resulting from Org Chart assignments if not in a list of scenarios
		//  selfEditProfile : when user edits his/her profile
		//  changePassword: when user changes his password (self edit profile, forgotten password, force password change on next login)
		//  massiveUpdate : when users are imported
		//  updateLastEnter: every 5 minutes when the user is staying active in the platform
		//  fromTempUser: when a new self-registration (core_user_temp) is being copied to core_user (becomes a regular user)
		//if ( !in_array($this->scenario, array('selfEditProfile', 'massiveUpdate', 'changePassword', 'updateLastEnter', 'fromTempUser', 'activate', 'deactivate')) ) {
			// $this->saveChartGroups();
		//}

		if (!$this->isNewRecord)
		{
			$event = new DEvent($this, array('changedAttributes' => $this->getChangedAttributes(), 'user' => $this));
			Yii::app()->event->raise('UpdateCoreUserAttributes', $event);
		}

		//get idst of the current user, if none is set, then fallback to Anonymous user (this is the case in the API scenario)
		$idUser = Yii::app()->user->id;
		if (!$idUser) {
			//if no current user is found then fallback to anonymous user
			$anonymous = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
			$idUser = $anonymous->getPrimaryKey();
		}

		// saving to 'core_password_history'
		if ($this->new_password != '') {
			$passwordHistory = new CorePasswordHistory();
			$passwordHistory->idst_user = $this->idst;
			$passwordHistory->pwd_date = Yii::app()->localtime->toLocalDateTime();
			$passwordHistory->passw = $this->pass;
			$passwordHistory->changed_by = $idUser;
			$passwordHistory->save();
		}

		//new users from import by a power user
		if (in_array($this->scenario, array('create', 'import'))) {
			if (Yii::app()->user->id && Yii::app()->user->getIsAdmin()) {

				if (!CoreAdminTree::model()->findByAttributes(array('idst' => $this->idst, 'idstAdmin' => Yii::app()->user->id))) {
					$association = new CoreAdminTree();
					$association->idstAdmin = Yii::app()->user->id;
					$association->idst = $this->idst;
					$association->save();
				}

				if (!CoreUserPU::model()->findByAttributes(array('user_id' => $this->idst, 'puser_id' => Yii::app()->user->id))) {
					$associationPU = new CoreUserPU();
					$associationPU->puser_id = Yii::app()->user->id;
					$associationPU->user_id = $this->idst;
					$associationPU->save();
				}

			}
		}

		//If the users changed his additional fields
		if($this->_additionalFieldsChanged == true && !$this->runGroupAutoassign) {
			$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
			if($hasSelfPopulatingGroups > 0) {
				$scheduler = Yii::app()->scheduler;
				$params = array(
					'user_idst' => $this->idst,
					'offset' => 0
				);
				if ($this->getScenario() != 'import') { //do not do this when importing users, that will already be handled in user management controller in a single step
					$scheduler->createImmediateJob(GroupAutoAssignTask::JOB_NAME, GroupAutoAssignTask::id(), $params);
				}
			}
		}

		if($this->runGroupAutoassign)
		{
			$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
			if($hasSelfPopulatingGroups > 0) {
				$params = array(
					'user_idst' => $this->idst,
					'offset' => 0
				);

				GroupAutoAssignTask::singleUserRun($this);
			}
		}

		parent::afterSave();
	}



	public function beforeDelete()
	{
		WebinarSessionUser::model()->deleteAllByAttributes(array('id_user' => $this->idst));
		LtCourseuserSession::model()->deleteAllByAttributes(array('id_user' => $this->idst));
		return parent::beforeDelete();
	}

	public function afterDelete() {

        /* Raise EVENT for Successfully DELETED USER for stats collection */
        Yii::app()->event->raise('DeletedUser', new DEvent($this, array('userModel'=>$this)));

		// delete Avatar
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$storage->remove($this->avatar);

		// delete relations with chart
		$models = CoreGroupMembers::model()->deleteAllByAttributes(array('idstMember' => $this->idst));

		//delete commontracks
		$models = LearningCommontrack::model()->deleteAllByAttributes(array('idUser' => $this->idst));

		//delete Video tracking info
		$models = LearningVideoTrack::model()->deleteAllByAttributes(array('idUser' => $this->idst));


		// delete deliverables
		$deliverables = LearningDeliverableObject::model()->findAllByAttributes(array('id_user' => $this->idst));
		if (!empty($deliverables)) {
			// need to call delete() for each model, so that the after/before delete methods run
			foreach ($deliverables as $deliverable) {
				$deliverable->delete();
			}
		}

		//delete course users
		$models = LearningCourseuser::model()->deleteAllByAttributes(array('idUser' => $this->idst));

		// More Clean Up
		$this->cleanUpPowerUserData();

		// Clean user entries
		// Use AR so that model to call its own afterDelete() which will clean Uploaded files, if any
		$userFieldEntries = CoreUserFieldValue::model()->findAllByAttributes((array('id_user' => $this->idst)));
		foreach ($userFieldEntries as $fieldEntry) {
			$fieldEntry->delete();
		}

		//cleaning up catalogs assignments
		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			LearningCatalogueMember::model()->deleteAllByAttributes(array('idst_member' => $this->idst));
		}

		$output = true;
		$toCheck = strtolower(Yii::app()->user->getRelativeUsername($this->userid));
		if ($toCheck != 'staff.docebo') {
			$deletedUser = new CoreDeletedUser('create');
			$deletedUser->attributes = $this->attributes;
			$deletedUser->deleted_by = Yii::app()->user->id;
			$deletedUser->deletion_date = Yii::app()->localtime->toLocalDateTime();
			if ($deletedUser->save()) {
				$output = true;
			} else {
				$output = false;
			}
		}

		// Clean up report filters: remove the user being deleted
		// from all reports "users" selection (except ecommerce ones)
		$criteria = new CDbCriteria();
		$criteria->addCondition('report_type_id <> :ecommerce');
		$criteria->addCondition('filter_data like :filter_data');
		$criteria->params[':ecommerce'] = LearningReportType::ECOMMERCE_TRANSACTIONS;
		$criteria->params[':filter_data'] = '%"'.$this->idst.'"%';
		$customReports = LearningReportFilter::model()->findAll($criteria);
		foreach ($customReports as $report) {
			$report->cleanUpUsers($this->idst);
		}

		return $output;
	}

	public function cleanUpPowerUserData() {
		CoreAdminTree::model()->deleteAll('idst = :idst OR idstAdmin = :idstAdmin', array(':idst' => $this->idst, ":idstAdmin" => $this->idst));
		//clear all power users with this idst
		CoreUserPU::model()->deleteAll('puser_id = :puser_id OR user_id = :user_id', array(':puser_id' => $this->idst, ':user_id' => $this->idst));
		//clear all assigned courses
		CoreUserPuCourse::model()->deleteAllByAttributes(array('puser_id' => $this->idst));
		//clear all assigned learning plans
		CoreUserPuCoursepath::model()->deleteAllByAttributes(array('puser_id' => $this->idst));
		//clear all courses, that this user can admin
		CoreAdminCourse::model()->deleteAllByAttributes(array('idst_user' => $this->idst));
		//clear all locations
		CoreUserPuLocation::model()->deleteAllByAttributes(array('puser_id' => $this->idst));
		//remove power user profile
		$profileMemberModel = CoreGroupMembers::model()->getPowerUserProfile($this->idst);
		if(!$profileMemberModel->isNewRecord)
			$profileMemberModel->delete();
	}

	/**
	 * Activate User
	 */
	public function activate() {
		$this->valid = self::STATUS_VALID;
		$this->suspend_date = NULL;
		return $this->save(false);
	}

	/**
	 * Deactivate User
	 */
	public function deactivate() {
		$this->valid = self::STATUS_NOTVALID;
		$this->suspend_date = Yii::app()->localtime->getLocalNow();
		return $this->save(false);
	}



	public function dataProvider($isUserManagement = false, $onlyUsers = false) {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
				// 'adminLevel',
				'language',
				// 'fields',
		);


		if ($onlyUsers) {
			$criteria->with['adminLevel'] = array(
				'condition' => 'adminLevel.groupid = :groupid',
				'params' => array(
					':groupid' => '/framework/level/user',
				),
			);
		} else {
			$criteria->with[] = 'adminLevel';
		}

		//$criteria->together = true;

		if (!Yii::app()->request->isAjaxRequest) {
			unset(Yii::app()->session['advancedSearchUids']);
		}

		if ((!empty($_REQUEST['advancedSearch']['fields']) || !empty($_REQUEST['advancedSearch']['additional'])) && $_REQUEST['advancedSearch']['visible'] == 1) {
			$params = $_REQUEST['advancedSearch'];

			if (!empty($params['fields'])) {
				foreach ($params['fields'] as $key => $data) {
					if($key == 'userLanguage') {
						$criteria->addCondition('language.lang_code = :' . $key . '_keywords');
						$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
						continue;
					}

					if($key == 'userLevel') {
						$criteria->select =
							CoreUser::model()->getTableAlias().'.*, '.
							CoreOrgChartTree::model()->getTableAlias().'.*, '.
							CoreLangLanguage::model()->getTableAlias().'.*, '.
							'(SELECT t1.idst FROM  `core_group_members` t1 WHERE  t1.idstMember = t.idst AND t1.idst = :' . $key . '_keywords LIMIT 1) AS userLevel';
						$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
						$criteria->having = 'userLevel IS NOT NULL';
						continue;
					}

					$data['keywords'] = trim($data['keywords']);

					if ($key == 'userid') {
						$this->userid = '';

						if ($data['condition'] != 'contains') {
							$data['keywords'] = Yii::app()->user->getAbsoluteUsername($data['keywords']);
						}
					}

					if($key == 'expiration'){
						if (!Yii::app()->localtime->forceLongYears()) {
							//trying to prevent short year format to be used in input fields
							switch (LocalTime::DEFAULT_DATE_WIDTH) {
								case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
								default: $dateWidth = LocalTime::MEDIUM; break;
							}
						} else {
							//long years are forced, all date widths are safe
							$dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
						}

						// prepare user date format
						$dateFormat = Yii::app()->localtime->getPHPLocalDateFormat();

						// create date object based on the user date format
						$objDateTime = DateTime::createFromFormat($dateFormat, $data['keywords']);

						// prepare date for storing in the DB tables
						$dbDate = (!empty($objDateTime) ? $objDateTime->format('Y-m-d') : '');
						$criteria->addCondition('expiration ' . $data['condition'] . '"' . $dbDate . '"');
					}

					if($key == 'email_status'){
						$criteria->addCondition('email_status = '.$data['keywords']);
					}

					if ($data['condition'] == 'contains') {
						$criteria->addSearchCondition($key, "{$data['keywords']}", true, ($params['condition'] == 'and' ? 'AND' : 'OR'));
					} elseif ($data['condition'] == 'equal') {
						$criteria->addCondition($key . ' = :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
						$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
					} elseif ($data['condition'] == 'not-equal') {
						$criteria->addCondition($key . ' <> :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
						$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
					}
				}
			}

			if (!empty($params['additional'])) {
				$this->processAdditionalFilters($params['additional']);
			}
		}

		if (isset(Yii::app()->session['advancedSearchUids']) && $isUserManagement && !empty($params['additional'])) {
			$uids = unserialize(Yii::app()->session['advancedSearchUids']);
			$criteria->addInCondition('t.idst', $uids);
		}

		//this will filter table users with power user visibility.
		//TO DO: this is just a patch. It works, but should be implemented in a better way
		//(power user idst should be passed to model through parameter; the join should be implemented with "with" criteria clausole)
		if ($isUserManagement || $this->scenario == 'search') {
			if (Yii::app()->user->getIsPu()) {
				$criteria->join .= ' JOIN '.CoreUserPU::model()->tableName().' pu ON (t.idst = pu.user_id AND puser_id = :puser_id) ';
				$criteria->params[':puser_id'] = Yii::app()->user->id;
			}
		}

		$criteria->compare('t.idst', $this->idst);
		$criteria->compare('t.userid', $this->userid, true);
		$criteria->compare('t.userid', '<>/Anonymous');
		$criteria->compare('t.firstname', $this->firstname, true);
		$criteria->compare('t.lastname', $this->lastname, true);
		$criteria->compare('t.level', $this->level);
		//$criteria->compare('t.lastenter', Yii::app()->localtime->fromLocalDateTime($this->lastenter), true);
		//$criteria->compare('t.register_date', Yii::app()->localtime->fromLocalDateTime($this->register_date), true);


		if($this->valid === "0" && $isUserManagement){
			$criteria->addCondition('(DATE(t.expiration) < DATE(NOW()) AND t.expiration IS NOT NULL AND  t.expiration<>"" AND expiration<>"0000-00-00" AND valid = 1)  OR valid = 0', 'AND');
		}elseif($this->valid === "1" && $isUserManagement){
			$criteria->addCondition('(DATE(t.expiration) >= DATE(NOW()) OR (t.expiration IS NULL OR t.expiration="" OR t.expiration="0000-00-00" )) AND valid = 1', 'AND');
		}else{
			$criteria->compare('t.valid', $this->valid);
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) LIKE :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		if (isset(Yii::app()->session['currentNodeId']) && $isUserManagement) {
			$currentNodeId = Yii::app()->session['currentNodeId'];
			if (!empty($currentNodeId)/* && $currentNodeId > 1*/) { ///?!?!?
				$allChildrenNodesIds = $this->getCurrentAndChildrenNodesList();
				if ($this->containChildren && !empty($allChildrenNodesIds)) {
					$criteria->with= array(
						'orgChartGroups' => array(
							'joinType' => 'INNER JOIN',
							'condition' => 'orgChartGroups.idOrg IN (\'' . implode('\',\'', $allChildrenNodesIds) . '\')',
						),
					);
				} else {
					$criteria->with= array(
						'orgChartGroups' => array(
							'joinType' => 'INNER JOIN',
							'scopes' => array('nodeUsersOnly'),
						)
					);
				}
				$criteria->with[] = 'language';
				$criteria->together = true;

				/* @var $db CDbConnection */
				$db = Yii::app()->db;
				$levelsIds = array_keys(Yii::app()->user->getUserLevelIds());
				$_alias = $db->quoteTableName('adminLevel');
				$criteria->join .= " JOIN ".$db->quoteTableName(CoreGroupMembers::model()->tableName())." ".$_alias." "
					." ON (".$_alias.".".$db->quoteColumnName('idstMember')." = ".$db->quoteTableName('t').".".$db->quoteColumnName('idst')." "
					." AND ".$_alias.".".$db->quoteColumnName('idst')." IN (".implode(",", $levelsIds)."))";
			}
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$sortAttributes['userLevel'] = array(
			'asc'  => 'adminLevel.idst DESC, t.userid ASC',
			'desc' => 'adminLevel.idst ASC, t.userid DESC'
		);
		$sortAttributes['userLanguage'] = 'language.lang_description';
		$sortAttributes['fullname'] = array(
				'asc'=>'t.firstname, t.lastname',
				'desc'=>'t.firstname DESC, t.lastname DESC',
		);

		$criteria->group = 't.idst';

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'register_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));
		$tmpDataProvider = new CActiveDataProvider(get_class($this), $config);
		return $tmpDataProvider;
	}




	public function dataProviderPowerUser($idCourse = false, $excludePuWithNoSeats = false) {
		$config = array();
		$params = array();
		$filterEmpty = '';
		$filter = Yii::app()->request->getParam('filter', null);
		if($filter === null)
			$filter = !empty($_SESSION['filterSearch']) ? $_SESSION['filterSearch'] : '';
		$_SESSION['filterSearch'] = $filter;
		if (!empty($_POST)) {
			if (isset($_POST['filterEmpty'])) {
				$filterEmpty = $_POST['filterEmpty'];
				$_SESSION['filterEmpty'] = $filterEmpty;
			} else {
				unset($_SESSION['filterEmpty']);
			}
		} else {
			if (isset($_SESSION['filterEmpty'])) {
				$filterEmpty = $_SESSION['filterEmpty'];
			} else {
				if (isset($_GET['filterEmpty'])) {
					$filterEmpty = $_GET['filterEmpty'];
				}
				if (!is_array($filterEmpty)) $filterEmpty = explode(',', $filterEmpty);
			}
		}

		$command = Yii::app()->db->createCommand()
			->select('u.idst, u.`userid`, u.firstname, u.lastname, CONCAT(u.firstname," ", u.lastname) fullname,
			`profiles`.groupid, puAssignedUsers.cnt AS puUsersCnt, courses.coursesCnt AS cntCourses, catalogs.catalogsCnt AS cntCatalogs, locations.locationsCnt')
			->from(CoreUser::model()->tableName() . ' u')
			->join(CoreGroupMembers::model()->tableName() . ' cg', 'cg.idstMember=u.idst')
			->join(CoreGroup::model()->tableName() . ' g', 'cg.idst=g.idst AND g.groupid="/framework/level/admin"')
			->leftJoin('(SELECT puUsers.puser_id, COUNT(puUsers.user_id) AS cnt FROM core_user_pu puUsers JOIN core_user u1 ON puUsers.user_id=u1.idst GROUP BY puser_id) puAssignedUsers', 'u.idst=puAssignedUsers.puser_id')
			->leftJoin('(SELECT COUNT(core_user_pu_course.`course_id`) AS coursesCnt, puser_id FROM `core_user_pu_course` GROUP BY core_user_pu_course.puser_id) courses', 'u.idst=courses.puser_id')
			->leftJoin('(SELECT COUNT(core_admin_course.`id_entry`) AS catalogsCnt,idst_user, type_of_entry FROM `core_admin_course` WHERE core_admin_course.type_of_entry=:type2 GROUP BY core_admin_course.`idst_user`) catalogs', 'u.idst=catalogs.idst_user', array(':type2' => CoreAdminCourse::TYPE_CATALOG))
			->leftJoin('(SELECT core_user_pu_location.`puser_id`,COUNT(core_user_pu_location.`location_id`) AS locationsCnt FROM `core_user_pu_location` GROUP BY core_user_pu_location.`puser_id`) locations', 'u.idst=locations.puser_id')
			->leftJoin('(SELECT g.groupid, cgm.idstMember from core_group g JOIN core_group_members cgm ON g.idst=cgm.idst AND g.groupid LIKE "%/framework/adminrules%")' . ' profiles',
				'`profiles`.idstMember=u.idst');

		if ($filter !== '') {
			$command->andWhere('u.userid like :filter OR u.firstname like :filter OR u.lastname like :filter', array(':filter' => '%' . $filter . '%'));
		}

		if (is_array($filterEmpty)) {
			foreach ($filterEmpty as $f) {
				switch ($f) {
					case 'userProfile':
						$command->andWhere('profiles.groupid IS NULL');
						break;
					case 'users':
						// that's why, because the PU always have at least one assigned user - himself
						$command->andWhere('puAssignedUsers.cnt <= 1 OR puAssignedUsers.cnt IS NULL');
						break;
					case 'courses':
						$command->andWhere('courses.coursesCnt = 0 OR courses.coursesCnt IS NULL');
						break;
					case 'catalogs':
						$command->andWhere('catalogs.catalogsCnt = 0 OR catalogs.catalogsCnt IS NULL');
						break;
					case 'locations':
						$command->andWhere('locations.locationsCnt = 0 OR locations.locationsCnt IS NULL');
						break;
					default:
						if (!empty($f))
							$command->andWhere('u.userid like :filter OR u.firstname like :filter OR u.lastname like :filter', array(':filter' => '%' . $f . '%'));
						break;
				}
			}
		}

		if($idCourse) {
			$command->join(CoreUserPuCourse::model()->tableName().' puc', 'u.idst = puc.puser_id AND course_id = :course_id', array(':course_id' => (int) $idCourse));

			if($excludePuWithNoSeats) {
				$command->andWhere('total_seats != 0');

				// Get course PUs, that are seat managers too
				$courseSeatManagers = CoreUserPU::model()->getCoursePUAndSeatManagers($idCourse);

				// If there are PU, that are also seat managers filter by them
				if(count($courseSeatManagers) > 0) {
					$command->andWhere('puc.puser_id IN ('. implode(',', $courseSeatManagers).')');
				} else {
					// Make sure, there won't be displayed any PU as there are no seat managers
					$command->andWhere('puc.puser_id = 0 ');
				}
			}
		}
		$command->group('u.idst');
		$n = clone $command;
		$count = count($n->queryAll());
		$sortAttributes = array();
		$sortAttributes['fullname'] = 'fullname';
		$sortAttributes['userid'] = 'userid';
		$sortAttributes['groupid'] = 'groupid';

		$courseId = $idCourse ? $idCourse : '';

		if (is_array($filterEmpty))
			$filterEmpty = implode(',', $filterEmpty);

		$config = array(
			'totalItemCount' => $count,
			'keyField' => 'idst',
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10),
				'params' => array('filterEmpty' => $filterEmpty, 'sort' => Yii::app()->request->getParam('sort', ''), 'id' => $courseId),
			),
			'sort' => array(
				'attributes' => $sortAttributes,
				'defaultOrder' => 'userid',
				'params' => array('filterEmpty' => $filterEmpty)
			),
		);

		return new CSqlDataProvider($command, $config);
	}

	/**
	 * Counts entities assigned to THIS power user (if it is not a power user, returns 0) and returns a rendred HTML
	 *
	 * @return string
	 */
	public function renderPowerUserAssignedEntities() {
		$count = Yii::app()->getDb()->createCommand()
			->select('COUNT(puu.user_id)')
			->from('core_user_pu puu')
			->join('core_user u', 'u.idst=puu.user_id')
			->where('puu.puser_id=:idUser', array(':idUser'=>$this->idst))
			->queryScalar();
		//echo $count;exit;
		//$count = CoreAdminTree::model()->getPowerUserMembersNum($this->idst);//CoreAdminTree::model()->countByAttributes(array('idstAdmin' => $this->idst ));
		$icon = ($count > 0) ? '<span class="assignUsers"></span>' : '<span class="assignUsers yellow"></span>';
		$content = CHtml::link($count . $icon, Docebo::createAdminUrl('PowerUserApp/powerUserManagement/assignUsers', array('id' => $this->idst)));
		return $content;
	}


	public function renderPowerUserUsers() {
		// pr($this->attributes,0);
		$countAssignUsers = count($this->allAssignedUsers);
		$icon = ($countAssignUsers > 0) ? '<span class="assignUsers"></span>' : '<span class="assignUsers yellow"></span>';
		$content = CHtml::link($countAssignUsers . $icon, Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignUsers', array('id' => $this->idst)));
		return $content;
	}

	public function renderPowerUserCourses() {
		$countAssignCourses = CoreUserPuCourse::model()->with('courses')->count('puser_id = :puser_id', array(':puser_id' => $this->idst));
		if (PluginManager::isPluginActive('CurriculaApp')) {
			$countAssignCourses += count($this->manageCoursepaths);
		}
		$icon = ($countAssignCourses > 0) ? '<span class="assignCourses"></span>' : '<span class="assignCourses yellow"></span>';
		$content = CHtml::link($countAssignCourses . $icon, Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignedCourses', array('id' => $this->idst)));
		return $content;
	}

	public function renderPowerUserCatalogs() {
		$countAssignCatalogs = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from(CoreAdminCourse::model()->tableName())
			->where('idst_user = :userId AND type_of_entry=:type', array(':userId'=>$this->idst, ':type'=>CoreAdminCourse::TYPE_CATALOG))->queryScalar();
		$icon = ($countAssignCatalogs > 0) ? '<span class="assignCatalogs"></span>' : '<span class="assignCatalogs orange"></span>';
		$content = CHtml::link($countAssignCatalogs . $icon, Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignedCatalogs', array('id' => $this->idst)));
		return $content;
	}

	public function renderPowerUserLocations() {
		$countLocations = CoreUserPuLocation::model()->countUserLocations($this->idst);
		$icon = ($countLocations > 0) ? '<span class="assignLocations"></span>' : '<span class="assignLocations yellow"></span>';
		$content = CHtml::link($countLocations . $icon, Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignedLocations', array('id' => $this->idst)));
		return $content;
	}

	public function renderPowerUserProfile() {
		static $profiles = null;

		if($profiles===null) {
			$criteria         = new CDbCriteria;
			$criteria->scopes = array( 'group' => array( 'groupKey' => '/framework/adminrules' ) );
			$profiles         = CoreGroup::model()->findAll( $criteria );
		}
		$profilesList = CHtml::listData($profiles, 'idst', function($data){
			return $data->relativeProfileId();
		});
		if (!empty($this->adminProfile[0])) {
			$content = CHtml::link($this->adminProfile[0]->relativeProfileId(), 'javascript:void(0);', array('class' => 'powerUserProfileLink'));
		} else {
			$content = CHtml::link('<span class="assignProfile yellow">'. Yii::t('power_user_management', '_ASSIGN_A_PROFILE') .'</span>', 'javascript:void(0);', array('class' => 'powerUserProfileLink'));
		}

		$content .= CHtml::dropDownList('CoreGroup[idst]', (!empty($this->adminProfile[0]->idst)?$this->adminProfile[0]->idst:''), $profilesList, array('empty' => 'Select Profile', 'class' => 'powerUserProfileSelect', 'data-puser-id' => $this->idst));

		return $content;
	}

	public function dataProviderEnroll($courseId = null) {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
			'adminLevel',
			'language',
		);

		if (!empty($_REQUEST['advancedSearch']['fields']) && $_REQUEST['advancedSearch']['visible'] == 1) {
			$params = $_REQUEST['advancedSearch'];
			foreach ($params['fields'] as $key => $data) {
				if ($key == 'userid') {
					$this->userid = '';

					if ($data['condition'] != 'contains') {
						$data['keywords'] = Yii::app()->user->getAbsoluteUsername($data['keywords']);
					}
				}

				if ($data['condition'] == 'contains') {
					$criteria->addSearchCondition($key, "{$data['keywords']}", true, ($params['condition'] == 'and' ? 'AND' : 'OR'));
				} elseif ($data['condition'] == 'equal') {
					$criteria->addCondition($key . ' = :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				} elseif ($data['condition'] == 'not-equal') {
					$criteria->addCondition($key . ' <> :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				}
			}
		}

		if (($courseId !== null) && is_numeric($courseId)) {
			$models = LearningCourseuser::model()->findAllByAttributes(array(), 't.idCourse = :courseId', array(':courseId' => $courseId));

			if (!empty($models)) {
				$criteria->mergeWith(array(
					'with' => array(
						'learningCourseusers' => array(
							'innerType' => 'LEFT JOIN',
							'condition' => 'learningCourseusers.idUser NOT IN ('.implode(',', CHtml::listData($models, 'idUser', 'idUser')).') OR learningCourseusers.idUser IS NULL',
						),
					),
					'group' => 't.idst',
					'together' => true,
				));
			}
		}

		if (Yii::app()->user->getIsAdmin()) { // Power user
			$criteria->join = ' JOIN '.CoreUserPU::model()->tableName().' pu ON (t.idst = pu.user_id AND puser_id = :puser_id) ';
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		$criteria->compare('t.idst', $this->idst);
		$criteria->compare('t.userid', $this->userid, true);
		$criteria->compare('t.userid', '<>/Anonymous');
		$criteria->compare('t.firstname', $this->firstname, true);
		$criteria->compare('t.lastname', $this->lastname, true);
		$criteria->compare('t.level', $this->level);
		$criteria->compare('t.lastenter', Yii::app()->localtime->fromLocalDateTime($this->lastenter), true);
		$criteria->compare('t.valid', $this->valid);
		$criteria->compare('t.register_date', Yii::app()->localtime->fromLocalDateTime($this->register_date), true);

        $sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$sortAttributes['userLevel'] = 'adminLevel.groupid';
		$sortAttributes['userLanguage'] = 'language.lang_description';
		$sortAttributes['fullname'] = array(
			'asc'=>'t.firstname, t.lastname',
			'desc'=>'t.firstname DESC, t.lastname DESC',
		);

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'register_date desc';
		$config['pagination'] = array(
            'pageSize' => Settings::get('elements_per_page', 10)
        );
		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderEnrollment() {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
			'learningCourse'
		);

		$criteria->compare('t.userid', '<>/Anonymous');
		$criteria->compare('learningCourse.idCourse', $this->learningCourse->idCourse);
		$criteria->compare('learningCourse.level', $this->learningCourse->level);
		$criteria->compare('learningCourse.status', $this->learningCourse->status);
		$criteria->compare('learningCourse.waiting', $this->learningCourse->waiting);

		// This one generates SQL exception
        //$criteria->compare('coreUserSettings.path_name', 'ui.language');
		if (isset(Yii::app()->session['enrollNodeId'])) {
			$enrollNodeId = Yii::app()->session['enrollNodeId'];
			if (!empty($enrollNodeId) && $enrollNodeId > 1) {
				$nodes = array($enrollNodeId);
				if ($this->containChildren) {
					$nodes = $this->getCurrentAndChildrenNodesList($enrollNodeId);
					if (empty($nodes)) {
						$nodes = array($enrollNodeId);
					}
				}
				$criteria->with['orgChartGroups'] = array(
					'joinType' => 'INNER JOIN',
				);
				$criteria->addCondition('orgChartGroups.idOrg IN ('.implode(',', $nodes).')');
				$criteria->together = true;
			}
		}

        //Filter by Groups
        if (isset(Yii::app()->session['enrollGroupId'])) {
            $enrollGroupId  =   Yii::app()->session['enrollGroupId'];
            if (!empty($enrollGroupId) && $enrollGroupId > 1) {
                $groups = array($enrollGroupId);
                $criteria->with['groups'] = array(
                    'joinType' => 'INNER JOIN',
                );
                $criteria->addCondition('groups.idst IN ('.implode(',', $groups).')');
                $criteria->together = true;
            }
        }

		//Filter by Deadline
		if ($this->learningCourse->deadline) {
			if ($this->learningCourse->deadline == LearningCourseuser::$DEADLINE_SELECT_WITHOUT) {
				$criteria->addCondition('learningCourse.date_expire_validity IS NULL OR learningCourse.date_expire_validity = "0000-00-00 00:00:00"');
			} elseif ($this->learningCourse->deadline == LearningCourseuser::$DEADLINE_DATE_RANGE) {
				if($this->learningCourse->fromDate && $this->learningCourse->toDate) {

					$fromDate = Yii::app()->localtime->fromLocalDate($this->learningCourse->fromDate);
					$toDate = Yii::app()->localtime->fromLocalDate($this->learningCourse->toDate);

					$criteria->addCondition('(DATE(learningCourse.date_expire_validity) >= :fromDate AND DATE(learningCourse.date_expire_validity) <= :toDate) AND (learningCourse.date_expire_validity IS NOT NULL OR learningCourse.date_expire_validity <> "0000-00-00 00:00:00")');
					$criteria->params[':fromDate'] = $fromDate;
					$criteria->params[':toDate'] = $toDate;
				}
			}
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		//--- power user filter ---
                  $pUserRights = Yii::app()->user->checkPURights($this->learningCourse->idCourse);

		if ($this->scenario == 'search' && $pUserRights->isPu && !$pUserRights->isInstructor) {
			$criteria->join = ' JOIN '.CoreUserPU::model()->tableName().' pu ON (t.idst = pu.user_id AND puser_id = :puser_id) ';
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}
		//---


		//--- coaching session filtering
		if ($this->coachingSessionFilter) {
			if ((int) $this->coachingSessionFilter > 0) {
				$criteria->join .= ' JOIN learning_course_coaching_session_user su ON (su.idUser=t.idst) ';
				$criteria->join .= ' JOIN learning_course_coaching_session s ON (su.idSession=s.idSession) AND (s.idCourse='.$this->learningCourse->idCourse.') AND (s.idSession='.$this->coachingSessionFilter.') ';
			}
			else if ($this->coachingSessionFilter == LearningCourse::COACHING_FILTER_UNASSIGNED) {
				// Just exclude users having a record in learning_course_coaching_session_user
				// for sessions of the course in question, using SUB SELECT
				$criteria->addCondition('
					t.idst NOT IN (
						SELECT su.idUser
						FROM learning_course_coaching_session_user su
						JOIN learning_course_coaching_session s ON (su.idSession=s.idSession)
						WHERE s.idCourse=' . $this->learningCourse->idCourse . '
					)');

			}

		}
		//---

		$config['criteria'] = $criteria;
		// If the user were selected certain order use it, otherwise use the default one
		$config['sort']['defaultOrder'] = $this->order_by ? $this->order_by : 't.firstname, t.lastname, t.userid';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderEnrollmentSelector() {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->select = 't.idst';

		$criteria->with = array(
			'learningCourse' => array(
				'select' => ''
			)
		);

		$criteria->compare('t.userid', '<>/Anonymous');
		$criteria->compare('learningCourse.idCourse', $this->learningCourse->idCourse);
		$criteria->compare('learningCourse.level', $this->learningCourse->level);
		$criteria->compare('learningCourse.status', $this->learningCourse->status);
		$criteria->compare('learningCourse.waiting', $this->learningCourse->waiting);

		if (isset(Yii::app()->session['enrollNodeId'])) {
			$enrollNodeId = Yii::app()->session['enrollNodeId'];
			if (!empty($enrollNodeId) && $enrollNodeId > 1) {
				$nodes = array($enrollNodeId);
				if ($this->containChildren) {
					$nodes = $this->getCurrentAndChildrenNodesList($enrollNodeId);
					if (empty($nodes)) {
						$nodes = array($enrollNodeId);
					}
				}
				$criteria->with['orgChartGroups'] = array(
					'joinType' => 'INNER JOIN',
					'select' => ''
				);
				$criteria->addCondition('orgChartGroups.idOrg IN ('.implode(',', $nodes).')');
				$criteria->together = true;
			}
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		//--- power user filter ---
		if ($this->scenario == 'search' && Yii::app()->user->isAdmin) {
			$criteria->join = ' JOIN '.CoreUserPU::model()->tableName().' pu ON (t.idst = pu.user_id AND puser_id = :puser_id) ';
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}
		//---

		$config['criteria'] = $criteria;
		$config['sort']['defaultOrder'] = 't.firstname, t.lastname, t.userid';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}




	/**
	 * Data provider used to build list of ALL ITEMS (used in Select All/Deselect All)
	 *
	 * NOTE: $this->learningCourse used here is misleading (thanks Quartz).
	 * It is actually a LearningCourseUser class !!! Don't be fooled!!!
	 *
	 * @return CSqlDataProvider
	 */
	public function dataSqlProviderEnrollmentSelector() {

		// SQL Parameters
		$params = array();

		// JOIN also COURSEs table to filter out enrollments to non existent courses; Just in case
		$command = Yii::app()->db->createCommand()
			->from('core_user t')
			->join('learning_courseuser enrollment'	, 't.idst=enrollment.idUser')
			->join('learning_course 	course'		, 'enrollment.idCourse=course.idCourse');


		// Exclude Anonymous users
		$command->where("t.userid <> '/Anonymous'")->andWhere('enrollment.idCourse=:idCourse');
		$params[':idCourse'] = $this->learningCourse->idCourse;


		// If any of the following attributes is set, use it/them to filter
		if ($this->learningCourse->level != null) {
			$command->andWhere('enrollment.level=:enrollmentLevel');
			$params[':enrollmentLevel'] = $this->learningCourse->level;
		}

		if ($this->learningCourse->status != null) {
			$command->andWhere('enrollment.status=:enrollmentStatus');
			$params[':enrollmentStatus'] = $this->learningCourse->status;
		}

		if ($this->learningCourse->waiting != null) {
			$command->andWhere('enrollment.waiting=:enrollmentWaiting');
			$params[':enrollmentWaiting'] = $this->learningCourse->waiting;
		}

		// Also search by user related text
		if ($this->search_input != null) {
			$command->andWhere('CONCAT(t.firstname, " ", t.lastname, " ", t.email, " ", t.userid) LIKE :search');
			$params[':search'] = '%'.$this->search_input.'%';
		}

		// Power user filter
		if ($this->scenario == 'search' && Yii::app()->user->getIsPu()) {
			$command->join(CoreUserPU::model()->tableName() . ' pu', 't.idst=pu.user_id AND puser_id=:puser_id');
			$params[':puser_id'] = Yii::app()->user->id;
		}

		// Filter by Branch(es)
		if (isset(Yii::app()->session['enrollNodeId'])) {
			$enrollNodeId = Yii::app()->session['enrollNodeId'];
			if (!empty($enrollNodeId) && $enrollNodeId > 1) {
				$nodes = array($enrollNodeId);
				if ($this->containChildren) {
					$nodes = $this->getCurrentAndChildrenNodesList($enrollNodeId);
					if (empty($nodes)) {
						$nodes = array($enrollNodeId);
					}
				}
				$command->join('core_group_members cgm', 't.idst=cgm.idstMember');
				$command->join('core_org_chart_tree tree', 'tree.idst_ocd=cgm.idst');
				$command->andWhere('tree.idOrg IN (' .implode(',', $nodes) . ')');
			}
		}


		//Filter by Groups
		if (isset(Yii::app()->session['enrollGroupId'])) {
			$enrollGroupId  =   Yii::app()->session['enrollGroupId'];
			if (!empty($enrollGroupId) && $enrollGroupId > 1) {
				$groups = array($enrollGroupId);
				if(is_array($groups) && !empty($groups)){
					$command->join('core_group_members groups', 't.idst = groups.idstMember');
					$command->andWhere('groups.idst IN ('.implode(',', $groups).')');
				}
			}
		}

		// COUNTER first
		$commandCounter = clone $command;
		$commandCounter->select('count(t.idst)');
		$commandCounter->params = $params;
		$numRecords = $commandCounter->queryScalar();


		// Base command select: we need idst only
		$command->select('t.idst as idst');


		// Data provider config
		$config['sort']['defaultOrder'] = 't.firstname, t.lastname, t.userid';
		$config['pagination'] 			= array('pageSize' => Settings::get('elements_per_page', 10));
		$config['keyField'] 			= 'idst';
		$config['params'] 				= $params;
		$config['totalItemCount'] 		= $numRecords;

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($command->getText(), $config);
		return $dataProvider;


	}






	public function dataProviderEnrollSession($idCourse = null, $idSession = null) {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with = array(
			'adminLevel',
			'language',
		);

		if (!empty($_REQUEST['advancedSearch']['fields']) && $_REQUEST['advancedSearch']['visible'] == 1) {
			$params = $_REQUEST['advancedSearch'];
			foreach ($params['fields'] as $key => $data) {
				if ($key == 'userid') {
					$this->userid = '';

					if ($data['condition'] != 'contains') {
						$data['keywords'] = Yii::app()->user->getAbsoluteUsername($data['keywords']);
					}
				}

				if ($data['condition'] == 'contains') {
					$criteria->addSearchCondition($key, "{$data['keywords']}", true, ($params['condition'] == 'and' ? 'AND' : 'OR'));
				} elseif ($data['condition'] == 'equal') {
					$criteria->addCondition($key . ' = :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				} elseif ($data['condition'] == 'not-equal') {
					$criteria->addCondition($key . ' <> :' . $key . '_keywords', ($params['condition'] == 'and' ? 'AND' : 'OR'));
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				}
			}
		}

		if (($idSession !== null) && is_numeric($idSession)) {
			// TODO: $idCourse should be used and tables learning_courseuser and learning_course_session should be joined too
			$models = LtCourseuserSession::model()->findAllByAttributes(array('id_session' => $idSession));

			if (!empty($models)) {
				$criteria->mergeWith(array(
					'with' => array(
						'learningCourseusersSession' => array(
							'innerType' => 'LEFT JOIN',
							'condition' => 'learningCourseusersSession.id_user NOT IN ('.implode(',', CHtml::listData($models, 'id_user', 'id_user')).') OR learningCourseusersSession.id_user IS NULL',
						),
					),
					'group' => 't.idst',
					'together' => true,
				));
			}
		}

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

        //--- power user filter ---
        if (Yii::app()->user->isAdmin) {
            $criteria->join = ' JOIN '.CoreUserPU::model()->tableName().' pu ON (t.idst = pu.user_id AND puser_id = :puser_id) ';
            $criteria->params[':puser_id'] = Yii::app()->user->id;
        }
        //---

		$criteria->compare('t.idst', $this->idst);
		$criteria->compare('t.userid', $this->userid, true);
		$criteria->compare('t.userid', '<>/Anonymous');
		$criteria->compare('t.firstname', $this->firstname, true);
		$criteria->compare('t.lastname', $this->lastname, true);
		$criteria->compare('t.level', $this->level);
		$criteria->compare('t.lastenter', Yii::app()->localtime->fromLocalDateTime($this->lastenter), true);
		$criteria->compare('t.valid', $this->valid);
		$criteria->compare('t.register_date', Yii::app()->localtime->fromLocalDateTime($this->register_date), true);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$sortAttributes['userLevel'] = 'adminLevel.groupid';
		$sortAttributes['userLanguage'] = 'language.lang_description';
		$sortAttributes['fullname'] = array(
			'asc'=>'t.firstname, t.lastname',
			'desc'=>'t.firstname DESC, t.lastname DESC',
		);

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'register_date desc';
		$config['pagination'] = array(
            'pageSize' => Settings::get('elements_per_page', 10)
        );
		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderNode($id = null)
	{
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->select = array('t.*');
		$chart = CoreOrgChartTree::model()->findByPk($id);
		//adding the assigned users to a session in order to preselect them in the grid
		if ($chart)
		{
			$relatedIds = array();
			if ($chart->idst_oc)
				$relatedIds[] = $chart->idst_oc;
			if ($chart->idst_ocd)
				$relatedIds[] = $chart->idst_ocd;

			$groupMemberCriteria = new CDbCriteria();
			$groupMemberCriteria->addInCondition('idst', $relatedIds);
			$groupMembers = CoreGroupMembers::model()->findAll($groupMemberCriteria);
			Yii::app()->session['selectedItems'] = CHtml::listData($groupMembers, 'idstMember', 'idstMember');
		}
		else
		{
			Yii::app()->session['selectedItems'] = array();
		}
		$criteria->group = 't.idst';

		$criteria->addCondition('t.userid <> :anonymous');
		$criteria->params[':anonymous'] = '/Anonymous';

		if ($this->search_input) {
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) like :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$sortAttributes['userLanguage'] = 'language.lang_description';
		$sortAttributes['fullname'] = array(
			'asc'=>'t.firstname, t.lastname',
			'desc'=>'t.firstname DESC, t.lastname DESC',
		);

		$config['criteria'] = $criteria;
		//$config['sort']['attributes'] = $sortAttributes;
		//$config['sort']['defaultOrder'] = 'register_date desc';
		$config['pagination'] = array('pageSize' => 10);

		return new CActiveDataProvider(get_class($this), $config);
	}

	/**
	 * Get the provided node and its children from CoreOrgChartTree
	 *
	 * @param integer $id
	 *
	 * @return array of node IDs
	 */
	public function getCurrentAndChildrenNodesList($id = null) {
		if ($id === null) {
			$currentNodeId = Yii::app()->session['currentNodeId'];
		} else {
			$currentNodeId = $id;
		}
		$allChildrenNodesIds = array();
		if (!empty($currentNodeId)) {
			$currentNode = CoreOrgChartTree::model()->find('idOrg=:idOrg', array(':idOrg'=>$currentNodeId));
			if ($currentNode)
			{
				$allChildrenNodes    = $currentNode->descendants()->findAll();
				$allChildrenNodesIds = CHtml::listData($allChildrenNodes, 'idOrg', 'idOrg');
				$allChildrenNodesIds = array_merge(array($currentNode->idOrg => $currentNode->idOrg), $allChildrenNodesIds);
			}
		}
//		else {
//			$criteria            = new CDbCriteria();
//			$criteria->condition = 'iLeft = 1';
//			$currentNode = CoreOrgChartTree::model()->find($criteria);
//			return array();
//		}

		return $allChildrenNodesIds;
	}

	public function filterColumns() {
		return array(
			'core' =>
				array(
					'userid' => array(
						'value' => 'Docebo::ellipsis(Yii::app()->user->getRelativeUsername($data->userid), 25, "...")',
						'title' => $this->getAttributeLabel('userid'),
						'class' => 'locked',
						'cssClass' => 'td-username',
					),
					'firstname' => array(
						'title' => $this->getAttributeLabel('firstname'),
						'class' => 'selected',
						'cssClass' => 'td-firstname',
					),
					'lastname' => array(
						'title' =>  $this->getAttributeLabel('lastname'),
						'class' => 'selected',
						'cssClass' => 'td-lastname',
					),
					'email' => array(
						'title' => $this->getAttributeLabel('email'),
						'class' => 'selected',
						'cssClass' => 'td-email',
					),
					'lastenter' => array(
						'title' => Yii::t('standard', '_DATE_LAST_ACCESS'),
						'class' => 'selected',
						'cssClass' => 'td-lastenter',
					),
					'register_date' => array(
						'title' => $this->getAttributeLabel('register_date'),
						'class' => 'selected',
						'cssClass' => 'td-register_date',
					),
					'userLanguage' => array(
						'value' => '$data->language->lang_description',
						'title' => $this->getAttributeLabel('language.value'),
						'class' => '',
						'cssClass' => 'td-userLanguage',
					),
					'userLevel' => array(
						'value' => 'Yii::t(\'admin_directory\', \'_DIRECTORY_\'.$data->adminLevel->groupid)',
						'title' => $this->getAttributeLabel('level'),
						'class' => '',
						'cssClass' => 'td-userLevel',
					),
					'expiration' => array(
						'value' => function($data){
							if($data->isUserExpired()){
								return '<span class="text-error">'.$data->expiration.'</span>';
							}else {
								return $data->expiration;
							}
						},
						'title' => $this->getAttributeLabel('expiration'),
						'class' => '',
						'cssClass' => 'td-expiration',
					),
					'email_status' => array(
						'value' => function($data){
							echo ($data->email_status == 1) ? Yii::t('standard', 'Verified') : Yii::t('standard', 'Unverified');
						},
						'title' => $this->getAttributeLabel('email_status'),
						'class' => '',
						'cssClass' => 'td-email_status',
					),
				),
			'additional' => $this->getAdditionalFieldFilterColumns()
		);
	}

    /**
     * Retrieving additional field details convenient for using as filter column details
     *
     * @return array
     */
    public function getAdditionalFieldFilterColumns()
    {
        $fieldsModel = new CoreUserField;

        // Loading the details for joined field translations table
        $joinDetails   = $fieldsModel->getJoinTranslationDetails(false);
        $trTableName   = $joinDetails['joinTableName'];
        $trTableAlias  = $joinDetails['joinTableAlias'];
        $fldTableName  = $joinDetails['fieldsTableName'];
        $fldTableAlias = $joinDetails['fieldsTableAlias'];
        $trKey  = '`' . $trTableAlias . '`.`' . $joinDetails['joinedFieldName'] . '`';
        $fldKey = '`' . $fldTableAlias . '`.`' . $joinDetails['ownFieldName'] . '`';

        // Loading the names of the tables and their aliases for fetching translated field details
        $trTableAlias   = 'cuft';
        $selectFields   = [
            // Additional user field table fields
            $fldTableAlias . '.' . $joinDetails['ownFieldName'],
            $fldTableAlias . '.type',
            $fldTableAlias . '.sequence',
            $fldTableAlias . '.invisible_to_user',
            $fldTableAlias . '.settings',
            $fldTableAlias . '.mandatory',

            // Translation fields
            $trTableAlias . '.lang_code',
            $trTableAlias . '.translation',
        ];

        // Using the criteria created by the model after language
        // filter was applied to fetch the needed details
        $queryCriteria = (new CoreUserField)->language()->getDbCriteria();

        // Filtering by visible field ids
        $visibleFields = $this->getVisibleAdditionalFieldsByBranch(array());
        $visibleFieldIDs = array_keys($visibleFields);
        if (empty($visibleFieldIDs) === false) {
            $criteria = $queryCriteria->condition . ' AND ' . $fldTableAlias . '.id_field IN(' . implode(', ', $visibleFieldIDs) . ')';
        }

        // Creating a command for fetching the needed details
        $command = Yii::app()->db->createCommand();
        $command->setSelect($selectFields);
        $command->from($fldTableName . ' as ' . $fldTableAlias);
        $command->where($criteria, $queryCriteria->params);
        $command->join($trTableName . ' AS ' . $trTableAlias, $trKey . ' = ' . $fldKey);
        $command->bindValues($queryCriteria->params);

        $fieldsDetails = $command->queryAll();
        $columnDetails = [];

        foreach ($fieldsDetails as $fieldDetails) {
            $fieldId = $fieldDetails['id_field'];
            $identifier = 'additional' . $fieldDetails['id_field'];
            $columnDetails[$identifier] = [
                'title' => $fieldDetails['translation'],
                'class' => '',
                'cssClass' => 'td-' . $identifier,
                'value' => '$data->getTestField(' . $fieldId . ',' . $fieldId . ');',
            ];
        }

        return $columnDetails;
    }

	public function getTestField($idField, $idCommon)
    {
        // Check if cache variable has already been initialized. If not, do the initialization.
		if (self::$_fieldsCache === false) {
			$c = new CDbCriteria();
            $c->index = 'id_field';
			self::$_fieldsCache = CoreUserField::model()->findAll($c);
		}

		// Retrieving the user values for the additional fields
        $values = static::getAdditionalFieldValuesForUsers([$this->idst], false);
        $valuesArray = static::convertAdditionalFieldValueInstancesToArray($values, true, true);

        // The retult is indexed by user id - getting rid of it here
        $values = $values[$this->idst];
        $valuesArray = $valuesArray[$this->idst];

        // Render the field value
        if (empty($values) === true) {
            return '';
        }

        $fieldUserEntry = null;
        if (is_array($values) === true) {
            $indexedValues = [];
            foreach ($values as $userEntry) {
                $indexedValues[$userEntry->id_common] = $userEntry;

                if ($userEntry->id_common == $idCommon) {
                    $fieldUserEntry = $userEntry;
                }
            }
            $values = $indexedValues;
            unset($indexedValues);
        } else {
            $fieldUserEntry = $values;
        }

        $fieldUserEntryValue = isset($valuesArray[$idCommon]) ? $valuesArray[$idCommon] : null;

        if ($fieldUserEntryValue === null || $fieldUserEntry === null) {
            return '';
        }

        $coreField = self::$_fieldsCache[$idCommon];
        $renderedValue = $coreField->renderValue($fieldUserEntryValue, $fieldUserEntry);

        switch ($coreField->getFieldType()) {
            case CoreUserField::TYPE_TEXT:
            case CoreUserField::TYPE_TEXTAREA:
                $renderedValue = CHtml::decode($renderedValue); //textual field types need some more handling to be displayed
                break;
        }

        return $renderedValue;
	}

	protected function applyNewPassword() {
		if ($this->getScenario() == 'fromTempUser') {
			return $this;
		}

		if (!empty($this->new_password)) {
			$this->pass = $this->encryptPassword($this->new_password);
		}
		return $this;
	}

	public function encryptPassword($password) {
		return Yii::app()->security->hashPassword($password);
	}

	public function createFromTempUser(CoreUserTemp $tempUser) {
		if(!$tempUser)
			return FALSE;

		$this->attributes = $tempUser->attributes;
		$this->register_date = Yii::app()->localtime->toLocalDateTime();
		if(Settings::get('disable_registration_email_confirmation') === 'on'){
			$this->email_status = 0;
		}else {
			$this->email_status = $tempUser->confirmed;
		}
		$save = $this->save(false);

		$registerType = Settings::get('register_type', '', true);

		if($save){
			// Subscribe him as a regular user
			try {
				$auth = Yii::app()->authManager;
				$auth->assign(Yii::app()->user->level_user, $this->idst, null, null, $registerType);
				$auth->assign('/oc_0', $this->idst, null, null, $registerType);
				$auth->assign('/ocd_0', $this->idst, null, null, $registerType);

				// synchronize with Hydra rbac
				HydraRBAC::manageUserToRole(array( "user" => $this->idst, "group" =>  Yii::app()->user->getUserLevelIds(true)[Yii::app()->user->level_user]));

			} catch (Exception $e) {
				// already assigned, just ignore
			}

			// Assign the new user to any orgchart node assigned to the temp user
			$groups = CoreGroupMembers::model()->findAllByAttributes(array('idstMember' => $tempUser->idst));
			foreach($groups as $group) {
				$groupMembership = CoreGroupMembers::model()->findByAttributes(array('idst' => $group->idst, 'idstMember' => $this->idst));
				if(!$groupMembership) {
					$groupMembership = new CoreGroupMembers('self-register');
					$groupMembership->idst = $group->idst;
					$groupMembership->idstMember = $this->idst;
					$groupMembership->save();
				}
			}

			// Save the user's preferred language during registration as his main lang
			//$usrLang = $tempUser->language ? trim(Lang::getCodeByBrowserCode($tempUser->language)) : null;
			$usrLang = $tempUser->language ? trim($tempUser->language) : null;

			if($usrLang){
				$usrPref = new CoreSettingUser();
				$usrPref->id_user = $this->idst;
				$usrPref->value = $usrLang;
				$usrPref->path_name = 'ui.language';
				$usrPref->save();
				if($usrPref->hasErrors()){
					Yii::log("Error saving user's lang preference. Errors: ".CVarDumper::dumpAsString($usrPref->getErrors(), 10, true));
				}
			}
		}

		return $save;
	}



	/**
	 * Save/Update user's group membership based on selected branches (org chart nodes)
	 * Call this after calling $model->save() whenever you set new $model->chartGroups
	 * to the model
	 */
	public function saveChartGroups() {

		// $this->chartGroups is coming from a Create user FORM (_userForm.php) and actually does NOT represent GROUPS, but ORG NODES!!!!!!
		// So, lets rename it and stop confusing ourselves
		if($this->chartGroups === false)
			$selectedNodes = array();
		else
			$selectedNodes = $this->chartGroups;  // again, NODES!

		// Add "physical root" node as if it is selected; every user is at least 'member' of that root node! NOT Group! Node!
		// That is, we "fake" this because controller MAY miss it.
		//$rootNode = CoreOrgChartTree::model()->findByAttributes(array('idParent' => 0, 'lev' => 1));
		$_roots = CoreOrgChartTree::model()->roots()->findAll(); //NOTE: we should always rely on nested set behavior standard functions !!!
		if (empty($_roots)) {
			//TODO: is it appropriate throwing an exception here?
		}
		$rootNode = $_roots[0];
		$selectedNodes[] = $rootNode->idOrg;

		$selectedNodes = array_unique($selectedNodes);

		// Get ALL possible chart nodes and make a list of all possible groups that can be assigned/deassigned
		// It is assumed that groups related to ORG chart is "normal" groups, hidden.
		// Use cached result, if available!
		if (empty(self::$allOrgChartNodes)) {
			$command = Yii::app()->getDb()->createCommand()
				->select('t.idOrg idOrg, t.idst_oc idst_oc, t.idst_ocd idst_ocd')
				->from(CoreOrgChartTree::model()->tableName().' t');
			self::$allOrgChartNodes = $command->queryAll();
		}

		$membershipGroupsToDelete = array();
		$membershipGroupsToAdd = array();
		foreach (self::$allOrgChartNodes as $node) {
			// If node is not selected, its groups must be deleted from user's membership
			if (!in_array($node['idOrg'], $selectedNodes) && $this->chartGroups !== false) {
				$membershipGroupsToDelete[] = $node['idst_oc'];
				$membershipGroupsToDelete[] = $node['idst_ocd'];
			}
			// Otherwise.. its group must assigned to user
			elseif($this->chartGroups !== false) {
				$membershipGroupsToAdd[] = $node['idst_oc'];
				$membershipGroupsToAdd[] = $node['idst_ocd'];
			}
		}


		// Delete memberships as per the list above
		if (count($membershipGroupsToDelete) > 0) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $membershipGroupsToDelete);
			$criteria->addCondition('idstMember=:idUser');
			$criteria->params[':idUser'] = $this->idst;
			CoreGroupMembers::model()->deleteAll($criteria);
		}

		// .. Or assign ones
		if (count($membershipGroupsToAdd)) {
			foreach ($membershipGroupsToAdd as $group) {
				$found = CoreGroupMembers::model()->findByAttributes(array('idst' => $group, 'idstMember' => $this->idst));
				if (!$found) {
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $group;
					$groupMember->idstMember = $this->idst;
					$groupMember->save();
				}
			}
		}

		return true;

	}




	/**
	 * OLD METHOD
	 * @return boolean
	 */
	protected function saveChartGroupsOld() {
        $criteria = new CDbCriteria();
        $criteria->order = 't.idOrg ASC';
		$chartGroups = CoreOrgChartTree::model()->findAll($criteria);

		if (count($this->chartGroups) <= 0) {
			//$this->chartGroups[] = 0;
		}

		foreach ($chartGroups as $chartGroup) {
			if ($chartGroup->idst_oc != 0) {
				$models = CoreGroupMembers::model()->findAllByAttributes(array(
					'idst' => array($chartGroup->idst_oc, $chartGroup->idst_ocd),
					'idstMember' => $this->idst,
				));
				if (!empty($models) && !in_array($chartGroup->idOrg, $this->chartGroups)) {
					foreach ($models as $model) {
						$model->delete();
					}
				}
				elseif (empty($models) && !in_array($chartGroup->idOrg, $this->chartGroups)) {
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chartGroup->idst_oc;
					$groupMember->idstMember = $this->idst;
					$groupMember->save();
					$groupMember = new CoreGroupMembers();
					$groupMember->idst = $chartGroup->idst_ocd;
					$groupMember->idstMember = $this->idst;
					$groupMember->save();
				}
			}
		}
		return true;
	}

	public function getOrgChartGroupsList() {
		if (!isset($this->orgChartGroupsList)) {
			$list = array();
			foreach ($this->orgChartGroups as $group) {
				$list[] = $group->idOrg;
			}
			$this->orgChartGroupsList = $list;
		}
		return $this->orgChartGroupsList;
	}

	public function setOrgChartGroupsList($list = array()) {
		if (is_array($list)) {
			$this->orgChartGroupsList = $list;
		}
		return true;
	}

	public function uniqueUserid($attribute, $params) {
		$userid = trim($this->userid);
		$criteria = new CDbCriteria();
		$criteria->addCondition('LOWER(userid) = :userid');
		$criteria->params['userid'] = strtolower($this->getAbsoluteUsername($userid));
		$user = self::model()->find($criteria);
		if (!$user) {
			$criteria = new CDbCriteria();
			$criteria->addCondition('LOWER(userid) = :userid');
			$criteria->params['userid'] = strtolower($this->getAbsoluteUsername($userid));
			$user = CoreUserTemp::model()->find($criteria);
		}
		if ($user && ($user->idst != $this->idst)) {
			$this->addError($attribute, 'Username "'.$userid.'" has already been taken.');
		}
	}

	public function validateAdditionalFields() {
		if(Yii::app()->user->getIsUser() || (Yii::app()->user->getIsPU() && !CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_power_users_to_skip_mandatory_fields'))) {
			$additionalFields = $this->getAdditionalFields();
			if(Settings::get('use_node_fields_visibility', 'off') == 'on') {
                if($this->getScenario() === 'selfEditProfile')
                {
                    //exclude non-visible fields
                    $visibleFields = $this->getVisibleFields();
                } elseif(Yii::app()->user->getIsPU() && count(CoreOrgChartTree::getPuBranchIds())) { // PU with at least one assigned branch
                    $visibleFields = $this->getVisibleAdditionalFieldsByBranch($this->chartGroups);
                }
                if (!empty($visibleFields))
                {
                    $filteredFields = array();
                    foreach($additionalFields as $key => $additionalField)
                    {
                        if(!in_array($additionalField->getFieldId(), $visibleFields))
                            unset($additionalFields[$key]);
                    }
				} else
					$additionalFields = array();
			}

			foreach ($additionalFields as $i => $field) {
				$className = 'Field' . ucfirst($field->getFieldType());
				$tempField = new $className();
				$tempField->setScenario('additionalValidate');
				$tempField->setAttributes($field->attributes, false);
				$tempField->userEntry = $field->userEntry;
                if (!$tempField->validate()) {
                    $this->_additionalFields[$i]->addErrors($tempField->errors);
					$this->addError($field->getFieldId(), $tempField->getError('userEntry'));
				}
			}
		}
	}

    /**
     * @param $fields
     * @deprecated Not used anywhere, so probably shouldn't from now on with the new tables structure
     */
	public function attachAdditionalFields($fields) {
		foreach ($fields as $fieldId => $data) {
            $entry = CoreUserFieldValue::model()->findByAttributes([
                'id_user' => $this->idst,
            ]);
            $fieldColumn = 'field_' . $fieldId;
            $entry->{$fieldColumn} = $data['value'];
            $entry->save();
		}
	}

	/**
	 * @see getAvatarImage()
	 * Similar to the method above, but allows a random user ID to be passed
	 * and doesn't work with Yii models (performance)
	 *
	 * @param $userId
	 *
	 * @return
	 * @throws \CException
	 */
	static public function getAvatarSrcByUser($userId){
		static $cache = array();

		if(!isset($cache[$userId])){
			// Fetch from database and cache during this request

			$criteria = new CDbCriteria();
			$criteria->select = 'avatar'; // Only select the 'avatar' db field for performance
			$criteria->addCondition('idst=:idUser');
			$criteria->params[':idUser'] = $userId;
			$user = CoreUser::model()->find($criteria);

			if ($user && $user->avatar) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$src = $storage->fileUrl($user->avatar);
			} else {
				$src = Yii::app()->theme->baseUrl .  '/images/standard/user.png';
			}
			$cache[$userId] = $src;
		}

		return $cache[$userId];
	}
	/**
	 * Returns the url of the user's avatar (if param is true), or an html image tag
	 * @param bool $onlySrc
	 * @return string
	 */
	public function getAvatarImage($onlySrc = false, $className = '', $style = '') {
		if ($this->avatar) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
			$src = $storage->fileUrl($this->avatar);
			$alt = $this->getFullName();
		} else {
			$src = Yii::app()->theme->baseUrl .  '/images/standard/user.png';
			$alt = Yii::t('profile', '_NOAVATAR');
		}

        // Raise event to let plugins provide a different layout/link for the avatar
        if(!$onlySrc) {
            $event = new DEvent($this, array('user' => $this, 'src' => $src, 'class' => $className, 'alt' => $alt, 'style' => $style));
            Yii::app()->event->raise("BeforeRenderUserAvatar", $event);
            if(!$event->shouldPerformAsDefault() && isset($event->return_value['html']))
                return $event->return_value['html'];
        }

		return $onlySrc ? $src : CHtml::image($src, $alt, array('class' => $className, 'style' => $style));
	}


	public static function getAnonymousAvatarImage($onlySrc = false) {
		$src = Yii::app()->theme->baseUrl .  '/images/standard/user.png';
		$alt = Yii::t('profile', '_NOAVATAR');
		return $onlySrc ? $src : CHtml::image($src, $alt);
	}

	public function getChangeStatusLink() {
		if ($this->valid == CoreUser::STATUS_VALID) {
			$class = 'suspend-action';
			$linkTitle = Yii::t('standard', '_DEACTIVATE');
			$modalTitle = Yii::t('standard', '_DEACTIVATE');
			$status = CoreUser::STATUS_NOTVALID;
		} else {
			$class = 'activate-action';
			$linkTitle = Yii::t('standard', '_ACTIVATE');
			$modalTitle = Yii::t('standard', '_ACTIVATE');
			$status = CoreUser::STATUS_VALID;
		}


		$content = Yii::app()->controller->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => $class,
				'linkTitle' => $linkTitle,
				'modalTitle' => $modalTitle,
				'url' => 'userManagement/changeStatus&status='.$status.'&idst=' . $this->idst,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL'),
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updateUserContent',
				'linkOptions' => array (
					'rel' => 'tooltip',
					'title' => $linkTitle
				)
			),
		), true);
		return $content;
	}

	public function processAdditionalFilters(array $filters = [])
    {
        $coreUserFieldValue = new CoreUserFieldValue;
        $valuesTableName  = $coreUserFieldValue->tableName();
        $valuesTableAlias = $coreUserFieldValue->getTableAlias();
        $command = Yii::app()->db->createCommand()
            ->from($valuesTableName . ' ' . $valuesTableAlias)
        ;

        if (empty($filters) === true) {
            $result = $command->queryAll();
            $uids = CHtml::listData($result, 'id_user', 'id_user');
            Yii::app()->session['advancedSearchUids'] = serialize($uids);
            return;
        }

        // Collecting filter details
        $where = [
            'keys' => [],
            'params' => [],
            'conditions' => [],
        ];

        foreach ($filters as $fieldId => $data) {
            if (empty($data) || (empty($data['value']) && $data['value'] !== '0')) { //user may explicitly have sent "0" as search value
                continue;
            }

            $fieldColumn = $valuesTableAlias . '.field_' . $fieldId;
            $where['keys'][":key_$fieldId"] = $fieldId;
            $where['params'][":value_$fieldId"] = trim($data['value']);

            if (!empty($data['condition'])) {
                if ($data['condition'] == 'contains') {
                    $where['conditions'][] = "($fieldColumn LIKE :value_$fieldId)";
                    $where['params'][":value_$fieldId"] = '%' . $data['value'] . '%';
                } elseif ($data['condition'] == 'not-equal') {
                    $where['conditions'][] = "($fieldColumn <> :value_$fieldId)";
                } elseif ($data['condition'] == 'equal') {
                    $where['conditions'][] = "($fieldColumn = :value_$fieldId)";
                } else {
                    // old conditions before LB-2
                    // ... this one should be refactored for `core_user_field_value` support
                    //$where['conditions'][] = "($a.id_common = :key_$fieldId AND $a.user_entry ".$data['condition']." :value_$fieldId)";

                    // search input field id
                    $searchFieldId = $where['keys'][":key_$fieldId"];

                    // get field type
                    $searchFieldType = CoreUserField::getFieldTypeById($searchFieldId);

                    // if the search input is date
                    if ($searchFieldType == CoreUserField::TYPE_DATE) {
                        // prepare date string
                        //$dateString = Yii::app()->localtime->toLocalDate($where['params'][":value_$fieldId"], LocalTime::MEDIUM);
                        $dateString = $where['params'][":value_$fieldId"]; //input is already passed in local format !!!

                        //set date width
                        if (!Yii::app()->localtime->forceLongYears()) {
                            //trying to prevent short year format to be used in input fields
                            switch (LocalTime::DEFAULT_DATE_WIDTH) {
                                case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
                                default: $dateWidth = LocalTime::MEDIUM; break;
                            }
                        } else {
                            //long years are forced, all date widths are safe
                            $dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
                        }

                        // prepare user date format
                        $dateFormat = Yii::app()->localtime->getPHPLocalDateFormat($dateWidth);

                        // create date object based on the user date format
                        $objDateTime = DateTime::createFromFormat($dateFormat, $dateString);

                        // prepare date for storing in the 'core_field_userentry' DB table
                        $dbDate = (!empty($objDateTime) ? $objDateTime->format('Y-m-d') : '');

                        // rewrite the date value in the $where array
                        $where['params'][":value_$fieldId"] = $dbDate;
                    }

                    // NOTE: old code was comparing two varchars and was returning wrong data
                    // ... this one should be refactored for `core_user_field_value` support
                    //$where['conditions'][] = "($a.id_common = :key_$fieldId AND $a.user_entry ".$data['condition']." :value_$fieldId)";
                    $where['conditions'][] = "(TIMESTAMP({$fieldColumn}) " . $data['condition'] . " TIMESTAMP(:value_{$fieldId}))";
                }
            } else {
                $where['conditions'][] = "($fieldColumn = :value_$fieldId)";
            }
        }

        $command->where(implode(" AND ", $where['conditions']), $where['params']);

        $result = $command->queryAll();
        $uids = CHtml::listData($result, 'id_user', 'id_user');

        Yii::app()->session['advancedSearchUids'] = serialize($uids);
	}

	/**
	 * Reset the user's language to be the platform default
	 * essentially removing his language preference
	 *
	 * @param integer $userId the user ID whose language to reset or leave blank if calling from CoreUser model
	 *
	 * @throws \CException if neither a CoreUser model is found or $userId is not provided
	 */
	public function resetUserLanguage($userId = null){
		if(!$userId){
			$userId = $this->idst;
		}

		if(!$userId) throw new CException('Can not reset user language without user ID or user model');

		$userLangPreference = CoreSettingUser::model()->findByAttributes(array(
			'id_user'=>$userId,
			'path_name'=>'ui.language'
		));
		if(!$userLangPreference) {
			$userLangPreference = new CoreSettingUser( 'create' );
			$userLangPreference->unsetAttributes();
		}
		$userLangPreference->path_name = 'ui.language';
		$userLangPreference->value = CoreUser::getDefaultLangCode();
		$userLangPreference->id_user = $userId;
		$userLangPreference->save();
	}

	/**
	 * Count registered (active) users
	 *
	 * @param boolean $excludeAnonymous
	 * @return number
	 */
	public static function getUsersCount($excludeAnonymous = true)  {
		$criteria = new CDbCriteria();

		if ($excludeAnonymous) {
			$criteria->addCondition("UPPER(userid) <> '/ANONYMOUS' AND UPPER(userid) <> 'ANONYMOUS'");
		}

		return self::model()->count($criteria);
	}

	/**
	 * Get authors (as CoreUser models) of all custom reports marked as public
	 * (Example scenario where this comes handy is when a Power User
	 * is viewing the reports area - he shouldn't be able to view
	 * any other authors)
	 */
	public static function getPublicReportAuthors(){
		$customPublicReports = LearningReportFilter::model()->with(array('user', 'access'))->findAll('is_standard=0 AND access.visibility_type = "'.LearningReportAccess::TYPE_PUBLIC.'"' );
		$allReportsIds = Yii::app()->db->createCommand()
			->select('id_filter')
			->where('is_standard = 0')
			->from(LearningReportFilter::model()->tableName())->queryColumn();
		$targetIds = array();
		foreach ($allReportsIds as $reportId) {
			if (LearningReportAccess::checkPermissionToEdit(Yii::app()->user->id, $reportId) === true)
				$targetIds[] = $reportId;
		}

		$customPublicReports = LearningReportFilter::model()->with(array('user', 'access'))
			->findAll('is_standard=0 AND (access.visibility_type = "'.LearningReportAccess::TYPE_PUBLIC.'" OR (access.visibility_type = "'.LearningReportAccess::TYPE_SELECTION.'" AND t.`id_filter` IN('."'" . implode("', '", $targetIds)."'" .'))  OR t.author = '.Yii::app()->user->id.')' );

		$users = array();
		foreach($customPublicReports as $report){
			if($report->user){
				$users[] = $report->user;
			}
		}

		return $users;
	}

	public static function getUsersListByRole($roleId) {
		$users = Yii::app()->db->createCommand()
			->select('u.idst, SUBSTR(u.userid,2) AS userid')
			->from(CoreUser::model()->tableName().' u')
			->join(CoreGroupMembers::model()->tableName().' cgm', 'u.idst = cgm.idstMember')
			->join(CoreGroup::model()->tableName().' g', 'g.idst = cgm.idst')
			->join(CoreRoleMembers::model()->tableName().' crm', 'g.idst = crm.idstMember')
			->join(CoreRole::model()->tableName().' r', 'r.idst = crm.idst')
			->where('r.roleid = :roleId', array(':roleId' => $roleId))
			->queryAll();

		return CHtml::listData($users, 'idst', 'userid');
	}

//	public static function isUserGodadmin($userId){
//
//		// Some caching for performance
//		// (we use this method/check in some mass actionns in User management)
//		static $godadmins = NULL;
//
//		if($godadmins == NULL){
//			$c = new CDbCriteria();
//			$c->select = 't.idst';
//			$c->addCondition("groups.groupid='/framework/level/godadmin'");
//			$c->addCondition('t.idst=:idst');
//			$c->params[':idst'] = $userId;
//			$godadmins = CoreUser::model()->with('groups')->findAll($c);
//		}
//		if(is_array($godadmins)){
//			foreach($godadmins as $godadmin){
//				if($godadmin->idst==$userId){
//					return TRUE;
//				}
//			}
//		}
//		return FALSE;
//	}
	public static function isUserGodadmin($userId){

		static $cache=array();
		$cacheKey = md5($userId);

		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}

		$c = new CDbCriteria();
		$c->select = 't.idst';
		$c->addCondition("groups.groupid='/framework/level/godadmin'");
		$c->addCondition('t.idst=:idst');
		$c->params[':idst'] = $userId;
		$userModels = CoreUser::model()->with('groups')->findAll($c);

		if (count($userModels) > 0) {
			$cache[$cacheKey] = true;
		}
		else {
			$cache[$cacheKey] = false;
		}
		return $cache[$cacheKey];
	}

	/**
	 * Check if arbitrary user is a power user
	 * @param integer $idUser
	 */
	public static function isPowerUser($idUser){
		static $cache=array();

		if (isset($cache[$idUser])) return $cache[$idUser];

		$c = new CDbCriteria();
		$c->select = 't.idst';
		$c->addCondition("groups.groupid='" .Yii::app()->user->level_admin . "'");   //   "/framework/level/admin"
		$c->addCondition('t.idst=:idst');
		$c->params[':idst'] = $idUser;

		$model = CoreUser::model()->with('groups')->find($c);
		if ($model) $cache[$idUser] = true;
		else $cache[$idUser] = false;

		return $cache[$idUser];

	}



    /**
     * Select default language
     *
     * @return string
     */
    public static function getDefaultLangCode() {
        if ( !(Yii::app() instanceof CConsoleApplication) && isset(Yii::app()->session['current_lang']) && Yii::app()->session['current_lang']) {
            return Yii::app()->session['current_lang'];
        }
        else if (Yii::app()->getLanguage() && ($lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage()))) {
        	return $lang;
        }
        else {
            return 'english';
        }
    }


    /**
     * Checks the user group membership and if a given admin_rules.*  path is enabled, like 'admin_rules.direct_course_subscribe'
     *
     * Note:
     * 		This matter is a bit ambiguous by database design.
     * 		For example, user might be part of two different groups, where the first group's admin rule is disabled ('off') while
     * 		the second group has the same admin rule enabled. This method searches if ANY group has it enabled, thus ignoring 'disabled' ocasions.
     *
     * @param number $idUser
     * @param string $rule
     * @return boolean
     */
	public static function adminRuleEnabled($idUser, $rule)
	{

    	// Cache results to save resources in loops
    	static $cache = array();

    	$key = $idUser .'-' . $rule;

    	if (isset($cache[$key])) {
    		return $cache[$key];
    	}

    	$command = Yii::app()->db->createCommand();
    	$command->select('csg.value');
    	$command->from('core_group_members cgm');
    	$command->join('core_group cg', 'cg.idst=cgm.idst');
    	$command->join('core_setting_group csg', 'csg.idst=cgm.idst');
    	$command->where('csg.path_name=:path');
    	$command->andWhere("csg.value='on'");
    	$command->andWhere("cgm.idstMember=:idstMember");

	    $enabled = $command->queryScalar(array(
    			':idstMember' => $idUser,
    			':path' => 'admin_rules.' . $rule,
	    ));

		$cache[$key] = ($enabled == 'on');
		return $cache[$key];
    	}



    /**
     * Checks if user is a Power User, then checks if the passed array consist any value that is NOT equal to Org Root AND is greter than 0.
     *
     * Note this is a sort of micro-helper method for Create/Update User methods to avoid duplication.
     * The passed paeameter is an array of selected org chart nodes, if any, in the user form dialog.
     *
     * The idea is: Power User MUST select AT LEAST ONE org chart/branch when createing or updating his own user.
     * Org Chart Root is not a legitimate choice!
     *
     *
     * @return boolean
     */
    public static function validatePowerAdminChartSelection($orgCharts, $allowRoot = false) {
    	$result = true;
    	if (Yii::app()->user->getIsAdmin()) {
    		$puMembers = CoreAdminTree::getPowerUserMembers(Yii::app()->user->id, array(CoreAdminTree::TYPE_ORGCHART, CoreAdminTree::TYPE_ORGCHART_DESC));
    		if (count($puMembers) > 0 ) {
    			$result = false;
    			$rootIdOrg = CoreOrgChartTree::getOrgRootNode()->idOrg;
				// only the root branch is assigned
				if($allowRoot && count($puMembers) == 1 && $puMembers[0]['id'] == $rootIdOrg && $puMembers[0]['type'] == CoreAdminTree::TYPE_ORGCHART)
					return true;
    			foreach ($orgCharts as $idOrg) {
    				if (  ($rootIdOrg != $idOrg) && ($idOrg > 0)) {
    					return true;
    				}
    			}
    		}
    	}
    	return $result;
    }

    /**
     * @return string
     */
    public function renderUserActions() {

        $permissions = self::getAdminPermissions();

        $userActions = Yii::app()->getController()->renderPartial('_userActions', array(
            'permissions' => $permissions,
            'user' => $this
        ), true);

        $content = Chtml::link('', 'javascript:void(0);', array(
            'id' => 'popover-'.uniqid(),
            'class' => 'popover-action popover-trigger',
            'data-toggle' => 'popover',
            'data-content' => $userActions,
        ));

		//Check if we can show the user actions
		$event = new DEvent($this, array('user' => $this));
		Yii::app()->event->raise(EventManager::EVENT_ON_RENDER_USERMAN_USER_ACTIONS, $event);

		if($event->return_value['hideActions'] === true){
			return "";
		}


        return $content;
    }

    /**
     * @return string
     */
    public function renderUserActive() {

        $permissions = self::getAdminPermissions();
		$content = '';

		// If User's expiration date is in the past, don't display the activate/deactivate icon
		if($this->isUserExpired()){
			return;
		}

		if ($permissions['mod'])
		{
			if(Yii::app()->user->getIsGodadmin() || !CoreUser::model()->isUserGodadmin($this->idst))
			{
				$isUserValid = ($this->valid == CoreUser::STATUS_VALID);
                $activateClass = $isUserValid ? 'node-suspend' : 'node-activate';
                $activateModalClass = $isUserValid ? 'suspend-action' : 'activate-action';
                $activateLabel = $isUserValid ? Yii::t('standard', '_DEACTIVATE') : Yii::t('standard', '_ACTIVATE');
                $activateStatus = $isUserValid ? CoreUser::STATUS_NOTVALID : CoreUser::STATUS_VALID;

                $activateOptions = array(
                    'class' => $activateClass . ' ajaxModal',
                    'data-toggle' => 'modal',
                    'data-modal-class' => $activateModalClass,
                    'data-modal-title' => $activateLabel,
                    'data-url' => implode('&', array(
                        'userManagement/changeStatus',
                        'status='.$activateStatus,
                        'idst='.$this->idst
                    )),
                    'data-buttons' => '[
                        {"type": "submit", "title": "'.Yii::t("standard", "_CONFIRM").'"},
                        {"type": "cancel", "title": "'.Yii::t("standard", "_CANCEL").'"}
                    ]',
                    'data-after-loading-content' => 'hideConfirmButton();',
                    'data-after-submit' => 'updateUserContent',
                    'modal-request-type' => 'GET',
                    'modal-request-data' => CJSON::encode(array()),
                    'rel' => 'tooltip',
                );

				$position = $isUserValid ? 467 : 441;
				$span = CHtml::tag('span', array(
					'style' => 'width:20px;height:20px;overflow:hidden;display:inline-block;background-image:url('.Yii::app()->theme->getAbsoluteBaseUrl().'/images/popover-icons.png);background-position:0px -'.$position.'px;'
				), '');

                $content .= CHtml::tag('a', $activateOptions, $span);
			}
		}
		else
		{
			$isUserValid = ($this->valid == CoreUser::STATUS_VALID);
            $activateClass = $isUserValid ? 'node-suspend' : 'node-activate';
			$activateLabel = $isUserValid ? Yii::t('standard', '_DEACTIVATE') : Yii::t('standard', '_ACTIVATE');
			$activateOptions = array(
				'class' => $activateClass,
				'title' => $activateLabel
			);

			$position = $isUserValid ? 467 : 441;
			$span = CHtml::tag('span', array(
				'style' => 'width:20px;height:20px;overflow:hidden;display:inline-block;background-image:url('.Yii::app()->theme->getAbsoluteBaseUrl().'/images/popover-icons.png);background-position:0px -'.$position.'px;'
			));

			$content .= $span;
		}

        return $content;
    }

    /**
     * Transform username from "user" to "/user"
     * @param $username
     * @return string
     */
    public static function getAbsoluteUsername($username) {
    	$res = $username;
    	if (substr($username, 0, 1) != '/') {
    		$res = '/'.$username;
    	}
    	return $res;
    }


	/**
	 * retrieve all user's visible custom fields
	 * @param bool $onlyMandatory filter only mandatory fields
	 * @return array
	 */
	public function getVisibleFields($onlyMandatory = false) {
	    
	    // If user is real, use its ID, otherwise, use the VIEWER ID
	    $targetUser = $this->idst ? $this->idst : Yii::app()->user->id;
	    
	    // If ID is not yet set (e.g. when creating a user), GodAdmins can see all fields
	    // @TODO maybe also to check power users?
	    if (!$this->idst) {
	        if (Yii::app()->user->getIsGodadmin()) {
	            $forceViewAll = true;
	        }
	    }
	    
		//prepare output variable
		$output = array();

        //pre-set query condition for mandatory fields, if requested
        $mandatoryCond = ($onlyMandatory ? " AND mandatory = 1" : "");

        // If the user visibility by organization chart fields is not active, then just return all user visible fields
        // Or, if for some reason "all visible" is forced
		if ((Settings::get('use_node_fields_visibility', 'off') != 'on') || $forceViewAll) {
			$fields = CoreUserField::model()->findAll('invisible_to_user = 0' . $mandatoryCond);
			foreach ($fields as $field) { $output[] = (int) $field->getFieldId(); }
			return array_unique($output);
		}

		$language = Settings::get('default_language'); //NOTE: no matter here what language we use ... jsut pick one to filter core_field table
		$root = CoreOrgChartTree::model()->getOrgRootNode(); //organization chart root node AR

		//check if the user is assigned to root node only: in this specific case no filter has to be applied on fields
		$cmd = Yii::app()->db->createCommand("SELECT DISTINCT oct.idOrg "
			." FROM ".CoreGroupMembers::model()->tableName()." gm  "
			." JOIN ".CoreOrgChartTree::model()->tableName()." oct "
			." ON (gm.idstMember = :id_user AND (gm.idst = oct.idst_oc OR gm.idst = oct.idst_ocd))");
		
		$reader = $cmd->query(array(
			':id_user' => $targetUser
		));
		
		//initialize checker variables
		$onlyRoot = true;
		$counter = 0;
		if ($reader) {
			while (($row = $reader->read()) && $onlyRoot) { //loop until we find a non-root node (it won't take more than 2 cycles)
				$counter++;
				if ($row['idOrg'] != $root->getPrimaryKey()) {
					$onlyRoot = false;
					break; //now we know what we need: just exit from loop
				}
			}
		}

		if ($onlyRoot && $counter == 1) {
			//the user is assigned to root only: pick all available visible fields
			$fields = CoreUserField::model()->findAll("invisible_to_user = 0" . $mandatoryCond);
			foreach ($fields as $field) { $output[] = (int)$field->getFieldId(); }
			return array_unique($output);
		}

        //pre-set query condition for mandatory fields, if requested
        $mandatoryCond = ($onlyMandatory ? " AND f.mandatory = 1" : "");

		//single query to exctract all user nodes fields from org. branches.
		//NOTE 1: due to recent changes org. branch do not inherit fields from ancestor anymore (15-05-2015) [see also RegisterHelper::retrieveNodeFields() method].
		//NOTE 2: we assume that in CoreField table every field has translation in every available language (if not, the query below may fail to retrieve some data)
		// Despite the above note: implementation actually was using inheritance, propagating branch-field assignments to all children branches (?!?!)
		// So, effectively, inheritance is still in force (you can see it in 6.9 LIVE)
		$cmd = Yii::app()->db->createCommand("SELECT f.id_field FROM ".CoreGroupFields::model()->tableName()." gf"
			." JOIN ".CoreUserFieldTranslations::model()->tableName()." ft ON (ft.id_field = gf.id_field AND ft.lang_code = :language) "
			." JOIN ".CoreUserField::model()->tableName()." f ON (f.id_field = ft.id_field AND f.invisible_to_user = 0".$mandatoryCond.") "
			." JOIN ".CoreGroupMembers::model()->tableName()." gm ON (gm.idstMember = :id_user AND gf.idst = gm.idst) "
			." JOIN ".CoreOrgChartTree::model()->tableName()." oct ON (gm.idst = oct.idst_oc OR gm.idst = oct.idst_ocd)");

		$reader = $cmd->query(array(
			':id_user'  => $targetUser,
			':language' => $language
		));

		$output = array();
		if ($reader) 
            while ($row = $reader->read()) 
                $output[] = $row['id_field'];
			
		
		// Inherited fields of all user branches
		$idOrgs = CoreOrgChartTree::getOrgChartListByUserId($targetUser);
		$branches = CoreOrgChartTree::model()->findAllByAttributes(array("idOrg" => $idOrgs));
        $inherited = array();
        if (is_array($branches) && !empty($branches)) {
            $rootId = CoreOrgChartTree::getOrgRootNode()->idOrg;
            foreach ($branches as $branch) {
                $cmd = Yii::app()->db->createCommand();
                $cmd
                    ->select("cgf.id_field")
                    ->from("core_org_chart_tree tree")
                    ->join("core_group_fields cgf", "cgf.idst=tree.idst_oc OR cgf.idst=tree.idst_ocd")
                    ->where("tree.iLeft < :iLeft AND tree.iRight > :iRight AND tree.idOrg <> :root", array(
                        ':iLeft'  => $branch->iLeft,
                        ':iRight' => $branch->iRight,
                        ':root'   => $rootId
                    ));
                    
                if ($onlyMandatory) {
                    $cmd->join("core_user_field cuf", "cgf.id_field=cuf.id_field AND cuf.mandatory <> 0");
                }
                
                $tmp = $cmd->queryColumn();
                $inherited = array_merge($inherited, $tmp);
                
            }
            $output = array_merge($output, $inherited);
        }

		
		return array_unique($output);
	}

	public static function getUsersByIdSt($idsts)
	{
		if(!is_array($idsts) || empty($idsts))
			return array();

		$users = array();

		//Cast all element to in, just for security reasons
		foreach($idsts as $key => $value)
			$idsts[$key] = (int)$value;

		//Retrive the users
		$command = Yii::app()->db->createCommand();
		$command->select('idst');
		$command->from(CoreUser::model()->tableName());
		$command->where('idst IN ('.implode(',', $idsts).')');

		$users = array_merge($users, $command->queryColumn());

		return $users;
	}


    /**
     * Does all password validations
     * @return boolean
     */
    public function passwordValidatePassword($attrName){
        $options = false;

        if(isset($this->userid) && $this->userid){
            $username = $this->getClearUserId();
        }
        else{
            $username = false;
        }
            $options = array();
        if ($this->scenario == 'import' || $this->scenario == 'createApi' || $this->scenario == 'updateApi'){
            $options['dictionary'] = false;
        }

        if($this->scenario == 'createApi' || $this->scenario == 'updateApi'){
            $options['repeating'] = false;
            $options['sequentialNum'] = false;
            $options['sequentialLet'] = false;
            $options['sequentialKey'] = false;
        }

        if (Yii::app()->user->getIsGodadmin()) { // skip if user is Godadmin
			$options['minChars'] = true;
			$options['alphaNum'] = false;
			$options['repeating'] = false;
			$options['sequentialNum'] = false;
			$options['sequentialLet'] = false;
			$options['sequentialKey'] = false;
			$options['dictionary'] = false;
        }

        if ($this->$attrName == '') {
            return true; // Empty value -> do not care
        }
        $result = Yii::app()->security->checkPasswordStrength($this->$attrName, false, $username, $options);//, $password_retype, $username);
        if(!$result['success'] && $result['message']!='')
        {
            $this->addError('new_password', $result['message']);
            return false;
        }
    }

    /**
     * Does all default password validations
     * @return boolean
     * @deprecated
     */
    public function passwordDefaultValidation($new_password){
        if (preg_match('/([\w\W])\1{2,}/', $this->new_password)) {
            $this->addError('new_password', Yii::t('register','Password cannot contain 3 or more repeating characters!'));
            return false;
        }
        // Check for 3 or more sequential numbers
        if (preg_match('/(012|123|234|345|456|567|678|789|987|876|765|654|543|432|321|210)+/', strtolower($this->new_password))) {
            $this->addError('new_password', Yii::t('register','Password cannot contain 3 or more sequential numbers!'));
            return false;
        }
        // Check for 4 or more alphabetically sequential characters
        if (preg_match('/((abcd|bcde|cdef|defg|efgh|fghi|ghij|hijk|ijkl|jklm|klmn|lmno|mnop|nopq|opqr|pqrs|qrst|rstu|stuv|tuvw|uvwx|vwxy|wxyz)|(zyxw|yxwv|xwvu|wvut|vuts|utsr|tsrq|srqp|rqpo|qpon|ponm|onml|nmlk|mlkj|lkji|kjih|jihg|ihgf|hgfe|gfed|fedc|edcb|dcba))+/', strtolower($this->new_password))) {
            $this->addError('new_password', Yii::t('register','Password cannot contain 4 or more alphabetically sequential characters!'));
            return false;
        }
        // Check for 4 or more US-keyboard-sequential characters
        if (preg_match('/((qwer|wert|erty|rtyu|tyui|yuio|uiop|asdf|sdfg|dfgh|fghj|ghjk|hjkl|zxcv|xcvb|cvbn|vbnm)|(mnbv|nbvc|bvcx|vcxz|lkjh|kjhg|jhgf|hgfd|gfds|fdsa|poiu|oiuy|iuyt|uytr|ytre|trew|rewq))+/', strtolower($this->new_password))) {
            $this->addError('new_password', Yii::t('register','Password cannot contain 4 or more US-keyboard-sequential characters!'));
            return false;
        }
    }

	/**
     * check is password alphanumeric if it is set in Settings
     * @return boolean
     */
	public function passwordValidateAlphaNumeric($new_password)
	{
        // NOTE: as per requirements for Super(God) admin(-s) should neglect require alphanumeric password
        if (Yii::app()->user->getIsGodadmin()) {
            return true;
        }

		// if empty password submitted do not need to validate it as password will be left intact
		if ($this->$new_password == '') {
			return true;
		}

		// Check should the password be in the system settings
		if (Settings::get('pass_alfanumeric') == 'on') {
			// check for alphanumeric password
			if (!(preg_match('/[a-z]/i', $this->$new_password) && preg_match('/[0-9]/', $this->$new_password))) {
				$this->addError('new_password', Yii::t("register", "_ERR_PASSWORD_MUSTBE_ALPHA"));
				return false;
			}
		}

        return true;
    }



	public function getUserNameFormatted()
	{
        return str_replace('/', '', $this->userid);
    }


	/**
	 * getIdstByUserId() get the user id(idst) by username(userid) passed
	 * @param $userId
	 * @return int idst of the user(id of a user)
	 */
	public static function getIdstByUserId($userId)
	{
		if (!$userId) return false;
		if (strpos($userId, '/') === false) {
			$userId = '/' . $userId;
		}

		$command = Yii::app()->db->createCommand();
		$command->select('idst');
		$command->from(CoreUser::model()->tableName());
		$command->where('userid = :userId', array(':userId' => $userId));

		$userIdst = $command->queryColumn();

		if (is_array($userIdst) && count($userIdst)) {
			$userIdst = current($userIdst);
		}
		return $userIdst;
	}


	/**
	 * getUserIdByIdst() get the userid(username) by the idst(id of an user)
	 * @param $idst
	 * @param $removeSlash - whatever to remove slash from userid
	 * @return string $userid
	 */
	public static function getUserIdByIdst( $idst = false, $removeSlash = true)
	{
		if ( !$idst ) return '';

		$command = Yii::app()->db->createCommand();
		$command->select('userid');
		$command->from(CoreUser::model()->tableName());
		$command->where('idst = :idst', array(':idst' => $idst));

		$userId = $command->queryColumn();

		if ( is_array($userId) && count($userId) ) {
			$userId = current( $userId );
		}

		// Remove the slash from userid(username)
		if ($removeSlash) {
			$userId = str_replace('/', '', $userId);
		}

		return $userId;
	}

    /**
     * @param $new_password
     * @return bool
     */
	public function passwordDictionaryCheck($attributeName)
	{

        if ( $this->scenario == 'import' )
			return true;

        if (Yii::app()->user->getIsGodadmin()) { // skip if user is Godadmin
			return true;
        }

        if ($this->$attributeName == '' ) {
			return true; // Empty value -> do not care
        }

        if ( Settings::get('pass_dictionary_check') == 'on') {
            $base = Docebo::getRootBasePath();

            $arrayOfWords = file($base. DIRECTORY_SEPARATOR . 'lms' . DIRECTORY_SEPARATOR . 'protected' . DIRECTORY_SEPARATOR . 'words.txt');
            $arrayOfWords = array_map('trim', $arrayOfWords);
            $flippedArray = array_flip($arrayOfWords);

            if ( isset($flippedArray[$this->$attributeName]) ) {
				$this->addError('new_password', Yii::t('register', 'Password cannot contain common dictionary words and most common passwords'));
				return false;
            }
        }
		return true;
    }


	/**
	 * @param $attributeName
	 * @return bool
	 */
	public function passwordMinCharCheck($attributeName) {

		// skip if user is Godadmin
		if (Yii::app()->user->getIsGodadmin()) { return true; }

		// if empty password submitted do not need to validate it as password will be left intact
		if ($this->new_password == '') { return true; }

		// check for password length
		if (strlen($this->new_password) < Settings::get('pass_min_char', 0)) {
			$this->addError('new_password', Yii::t('register','_PASSWORD_TOO_SHORT'));
			return false;
		}

		return true;
	}



	public static function getAvatarByUserId($pk, $onlySrc = true){
		$m = CoreUser::model()->findByPk($pk);
		if($m){
			return $m->getAvatarImage($onlySrc);
		}
		return CoreUser::getAnonymousAvatarImage($onlySrc);
	}

	public static function getExpirationDate($idUser){
		return Yii::app()->getDb()->createCommand()->select('expiration')
			->from(CoreUser::model()->tableName())
			->where('idst=:idUser', array(':idUser'=>$idUser))
			->queryScalar();
	}

	public static function getExpirationComparesToJson(){
		$levels = array();
		$levels[] = array(
			'value' => '>',
			'text' => '>'
		);
		$levels[] = array(
			'value' => '<',
			'text' => '<'
		);
		$levels[] = array(
			'value' => '=',
			'text' => '='
		);
		$levels[] = array(
			'value' => '>=',
			'text' => '>='
		);
		$levels[] = array(
			'value' => '<=',
			'text' => '<='
		);
		$levels[] = array(
			'value' => '<>',
			'text' => '!='
		);

		return json_encode($levels);
	}

	public function getVerificationLink($withDate = false)
	{
		$emailLink = CHtml::link(Yii::t('user', 'Confirm your registration'), Docebo::createAbsoluteAdminUrl('userManagement/verifyEmail',
				array('userId' => $this->idst, 'hash' => $this->email_code)));
		if (!$withDate)
			return $emailLink;
		else
			return array($emailLink, $this->requested_on);
	}

	public function setVerificationLink($toBeSaved = true){
		$hash = md5($this->userid . $this->idst . mt_rand() . mt_rand() . mt_rand());
		$time = strtotime(Yii::app()->localtime->getUTCNow());
		$date = date('Y-m-d H:i:s', $time);
		$this->email_code = $hash;
		$this->requested_on = $date;
		if ($toBeSaved) {
			if ($this->save())
				return true;
			return false;
		} else {
			Yii::app()->db->createCommand()
					->update(CoreUser::model()->tableName(), array(
							'email_code' => $hash,
							'requested_on' => $date
					), 'idst = :id', array(':id' => $this->idst));
			return true;
		}
	}

	public static function getEmailStatusComparesToJson(){
		$levels = array();
		$levels[] = array(
				'value' => '0',
				'text' => Yii::t('standard', 'Unverified')
		);
		$levels[] = array(
				'value' => '1',
				'text' => Yii::t('standard', 'Verified')
		);

		return json_encode($levels);
	}

	/**
	 * Check the current user's expiration date and return true if expired
	 * @return bool
	 */
	public function isUserExpired () {
		if($this->expiration && $this->expiration!='0000-00-00'){
			$localFormat = Yii::app()->localtime->getPHPLocalDateFormat();

			$expirationDateObject = DateTime::createFromFormat($localFormat, $this->expiration, Yii::app()->localtime->getLocalDateTimeZone());
			$expirationDateObject->setTime(23, 59, 59);

			$currentDateObject = new DateTime();

			if($expirationDateObject->getTimestamp() < $currentDateObject->getTimestamp()){
				// User is expired
				return true;
			}
		}

		return false;
	}

	public function getCurrentUserLanguage(){
		$language = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreSettingUser::model()->tableName())
				->where('path_name = "ui.language" AND id_user = :userId', array(':userId' => $this->idst))
				->queryScalar();
		if (!$language) $language = self::getDefaultLangCode();
		return $language;
	}

	public static function getUserLanguage($idst){
		$language = Yii::app()->db->createCommand()
			->select('value')
			->from(CoreSettingUser::model()->tableName())
			->where('path_name = "ui.language" AND id_user = :userId', array(':userId' => $idst))
			->queryScalar();
		if (!$language) $language = self::getDefaultLangCode();
		return $language;
	}

    /**
     * @param $idst The id of the USER or current user if is passed invalid UserId
     * @return array of recent searches
     *
     */
    public static function getRecentSearch($idUser) {
        if ( !$idUser || empty($idUser) || $idUser == false)
            $idUser = Yii::app()->user->id;

        $recentSearch = self::model()->findByPk($idUser)->recent_search;
        if ( empty($recentSearch ))
            $result = array();
        else
        $result = CJSON::decode($recentSearch);

        return $result; // array //
    }

    /**
     * @param $idUser - If invalid ID is passed , get currentUser
     * @param $search - single string of searched word to be added
     *                  in the JSON in coreUser->recent_search
     *
     *                  If $search word exist it goes first and
     *                  the word that accured in the array is
     *                  pop out from the stack.
     */
    public static function updateRecentSearch($idUser, $search) {
        $recSearches = CoreUser::getRecentSearch($idUser);

        if ( is_array($recSearches) ) {
            try {
                    if( !empty($recSearches) && ($key = array_search($search, $recSearches) ) !== false ) {
                        unset($recSearches[$key]);            // Remove the repeated word
                        array_unshift($recSearches, $search); // And  put it FIRST
                    } else
                        array_unshift($recSearches, $search); // Just put it FIRST
                /*- Do not let the recent searches be greater than 'max_recent_search_keep' -*/

				$maxRecentNumber = Settings::get('max_recent_search_keep', 5);

                if ( count($recSearches) > $maxRecentNumber ) {
                    $recSearches = array_slice($recSearches, 0, $maxRecentNumber);
                }

                $toInsert   = CJSON::encode($recSearches);
                $sql        = "UPDATE core_user SET recent_search = :search WHERE core_user.idst = :idst";
                $parameters = array(
                    ":search"   => $toInsert,
                    ":idst"     => $idUser
                );
                $insertQuery = Yii::app()->db->createCommand($sql)->execute($parameters);
               if ( !$insertQuery )
                    return false;
            } catch (CException $e) {
                return false;
            }
        }
    }



    /**
     * Return list of users and their branch(es) and/or group(s) assignment information.
     * Note please, requesting assignments for ALL users at once might be risky if user base is big!
     *
     * @param array|integer $idUsers Single user or array of users (IDs)
     * @param string $useGroupConcat If set to tru, return ONE array element per user; other wise, one element per user <-> assignment
     *
     * @return array
     */
    public static function getUsersGroupAndBranchAssignments($idUsers=false, $useGroupConcat=true) {

        // Apply some caching for repeated calls in the same request/run
        static $cache = false;
        $cacheKey = md5(json_encode(func_get_args()));
        if (is_array($cache) && isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        // Make sure it is an array
        if (($idUsers !== false) && !is_array($idUsers)) {
            $idUsers = array((int) $idUsers);
        }

        $params = array();
        $command = Yii::app()->db->createCommand();

        $selectArray = array(
            "u.idst as id_user"
        );

        // Outout depends on this parameter
        // If it is TRUE, we get an array of elements, one per user
        if ($useGroupConcat) {
            $selectArray = array_merge($selectArray, array(
                "group_concat(DISTINCT (CASE WHEN tree2.idOrg IS NULL THEN g.idst ELSE NULL END) ) as groups",
                "group_concat(DISTINCT tree.idOrg) as direct_branch_memberships",
                "group_concat(DISTINCT tree2.idOrg) as all_parent_branches",
            ));
            $command->group("id_user");
        }
        // Otherwise, we get many elements, one per user <--> assignment for better analyzis
        else {
            $selectArray = array_merge($selectArray, array(
                "(CASE WHEN g.hidden='false' THEN 1 ELSE 0 END) as is_user_group",
                "(CASE WHEN g.hidden='false' THEN g.idst ELSE null END) as user_group",
                "tree2.idOrg as parent",
                "tree.idOrg as direct_branch_membership",
            ));
        }
        $command->select($selectArray);


        $rootNode = CoreOrgChartTree::getOrgRootNode();
        $params[':idOrgRoot'] = $rootNode->idOrg;

        $command->from("core_user u");
        $command->join("core_group_members cgm", "cgm.idstMember=u.idst");
        $command->leftJoin("core_group g", "g.idst=cgm.idst");
        $command->leftJoin("core_org_chart_tree tree", "tree.idst_oc=g.idst AND g.groupid LIKE '/oc\_%'");
        $command->leftJoin("core_org_chart_tree tree2", "tree2.iLeft < tree.iLeft AND tree2.iRight > tree.iRight");

        $command->andWhere("((tree.idOrg IS NULL AND g.hidden = 'false') OR (tree.idOrg IS NOT NULL) AND (tree.idOrg <> :idOrgRoot) )");

        if (is_array($idUsers) && !empty($idUsers)) {
            $command->andWhere(array("IN","u.idst", $idUsers));
        }

        // Get all rows
        $rows = $command->queryAll(true, $params);

        if ($useGroupConcat) {
            $result = array();
            foreach ($rows as $userRow) {
                $tmpId = "" . $userRow['id_user'] . "";
                unset($userRow['id_user']);
                $result[$tmpId] = $userRow;
            }
        }
        else {
            $result = $rows;
        }

        $cache[$cacheKey] = $result;

        return $result;

    }



	public function checkBranchError($currentNode=null){
		$branchError = false;
		if($this->chartGroups){
			foreach ($this->chartGroups as $node) {
				$orgChart = CoreOrgChartTree::model()->findByPk($node);
				if(!$orgChart){
					$branchError = true;
					break;
				}
			}
		}

		if($currentNode){
			$orgChart = CoreOrgChartTree::model()->findByPk($currentNode);
			if(is_array($this->chartGroups) && in_array($currentNode ,$this->chartGroups) && !$orgChart){
				$branchError = true;
			}

			if(is_array($this->chartGroups) && in_array($currentNode ,$this->chartGroups) && ($orgChart && isset(Yii::app()->session['currentNodePath']) && Yii::app()->session['currentNodePath'])){
				if(($orgChart->getNodePath()) != (Yii::app()->session['currentNodePath'])){
					$branchError = true;
				}
			}
		}


		return $branchError;
	}

    public static function getUnfilledDefaultFields($userId = null) {

        if(!$userId) $userId = Yii::app()->user->getId();

		$anonymous = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
		$id_anonymous = $anonymous->getPrimaryKey();

		if(!$userId || $id_anonymous == $userId)
			return array();

        $user = self::model()->findByPk($userId);
        $attributes = array();
		if(Settings::get('lastfirst_mandatory', 'off') == 'on') {
			$attributes = array('firstname', 'lastname');
		}
		$attributes[] = 'email';
        $text = null;
        $unfilledFields = array();

        foreach ($attributes as $attribute) {
            if($user->$attribute == '' || $user->$attribute == null) {
                $unfilledFields[] = $attribute;
            }
        }

        return $unfilledFields;
    }

    public function lostPassword($usernameOrEmail) {

        $userModel = CoreUser::model()->find(
            'userid = :userid OR email = :email ORDER BY idst DESC',
            array(':userid' => '/' . $usernameOrEmail, ':email' => $usernameOrEmail)
        );

        if($userModel && !empty($usernameOrEmail) && !empty($userModel->email) )
        {
            $code = md5(mt_rand() . mt_rand());
            // Checking if the code generated already exists in core_pwd_recover
            $corePwdRecoverModel = CorePwdRecover::model()->findByPk($userModel->idst);

            if ($corePwdRecoverModel === NULL) {
                $corePwdRecoverModel = new CorePwdRecover();
                $corePwdRecoverModel->idst_user = $userModel->idst;
                $corePwdRecoverModel->random_code = $code;
                $corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
                $corePwdRecoverModel->save();

            } else {
                $corePwdRecoverModel->random_code = $code;
                $corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
                $corePwdRecoverModel->update();
            }

            // Get the link, but prevent Url manager to strip "release", if any
            $oldStripRelease = Yii::app()->urlManager->getStripRelease();
            Yii::app()->urlManager->setStripRelease(false);

            $link = Docebo::createAbsoluteUrl('lms:user/recoverPwd', array('code' => $code));

            Yii::app()->urlManager->setStripRelease($oldStripRelease);


            $fromAdminEmail = array(Settings::get('mail_sender', 'noreply@docebo.com'));
            $toEmail = array($userModel->email);
            $subject = Yii::t('register', '_LOST_PWD_TITLE');

            $body = Yii::t('register', '_LOST_PWD_MAILTEXT', array(
                '[link]' => $link,
                '[username]' => $userModel->getClearUserId()
            ));

            $mm = new MailManager();
            $mm->setContentType('text/html');

            try {
                $mm->mail($fromAdminEmail, $toEmail, $subject, $body);
                return true;
            } catch (Swift_RfcComplianceException $e) {
                $error = Yii::t('standard', '_OPERATION_FAILURE');
                Yii::log($error, CLogger::LEVEL_ERROR);
            }
            return false;
        }
    }

    /**
     * @param $idst
     * @return array|string
     */
    public static function getUserMobileInfo($idst) {
        if ( !$idst ) return '';
        $userInfo = array();
        $baseUrl = Yii::app()->getBaseUrl(true);
        $baseUrl = str_replace('/api', '', $baseUrl);

        $command = Yii::app()->db->createCommand();
        $command->select('idst, firstname, lastname, email');
        $command->from(CoreUser::model()->tableName());
        $command->where('idst = :idst', array(':idst' => $idst));

        $response = $command->queryAll();
        $userInfo = $response[0];

		/*AVATAR*/
        $avatar = CoreUser::model()->findByPk($idst)->getAvatarImage(true);

		if ( strlen($avatar) < 1 ){
			$avatar = Yii::app()->theme->baseUrl .  '/images/standard/user.png';
		}

        $userInfo['avatar'] = $avatar;
		$skipWalkthrough = CoreSettingUser::model()->findByAttributes(array('path_name' => 'skip_walkthrough', 'id_user' => $idst));
		$userInfo['skip_walkthrough'] = (is_null($skipWalkthrough) ? 0 : 1);

        return $userInfo;
    }



	/**
	 * Returns formatted user's names by the new convention (paul.brown)
	 * @param integer|CoreUser $idUser
	 */
	public static function getForamattedNames($idUser, $separator = ".", $lowerCase = true){
		$userNames = "";
		if (is_object($idUser) && $idUser instanceof CoreUser) {
			$userModel = $idUser;
		} else {
			$userModel = self::model()->findByPk($idUser);
		}
		if (!empty($userModel) && $userModel->idst) {
			if ($userModel->firstname && $userModel->lastname) {
				$userNames = $userModel->firstname.$separator.$userModel->lastname;
			} elseif ($userModel->firstname) {
				$userNames = $userModel->firstname;
			} elseif ($userModel->lastname) {
				$userNames = $userModel->lastname;
			} else {
				$userNames = ltrim($userModel->userid, "/");
			}
		}

		if ($lowerCase) { return mb_strtolower ($userNames,"UTF-8"); }
		return $userNames;
	}



	public static function getUsers($name){
		//REGEXP '^b'
		//$param =array(":name"=>$name.'%');
		$param =array(":name"=>'^'.$name);
		$sql = "SELECT firstname, lastname , avatar , userid , idst FROM `core_user` WHERE concat(firstname,' ',lastname) REGEXP :name LIMIT 5";
		$users = Yii::app()->db->createCommand($sql)->queryAll(true,$param);
		return $users;
	}



	/** Returns array of the classrooms' ids for which the user has chosen a session
	 * @param $userId
	 * @return array
	 */
	public static function getClassroomsIfUserIsEnrolledInAnySession($userId){
		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse');
		$command->from(LearningCourse::model()->tableName().' c');
		$command->join(LtCourseSession::model()->tableName().' cs', 'cs.course_id = c.idCourse');
		$command->join(LtCourseuserSession::model()->tableName().' cus','cus.id_session = cs.id_session AND cus.id_user = '.$userId);
		$command->andWhere('c.course_type = "'.LearningCourse::TYPE_CLASSROOM.'"');
		$command->group('idCourse');

		$rows= $command->queryAll();
		$result = array();
		foreach($rows as $row){
			$result[] = $row['idCourse'];
		}

		return $result;
	}

	/** Returns array of the webinars' ids for which the user has chosen a session
	 * @param $userId
	 * @return array
	 */
	public static function getWebinarsIfUserIsEnrolledInAnySession($userId){
		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse');
		$command->from(LearningCourse::model()->tableName().' c');
		$command->join(WebinarSession::model()->tableName().' ws', 'ws.course_id = c.idCourse');
		$command->join(WebinarSessionUser::model()->tableName().' wsu','wsu.id_session = ws.id_session AND wsu.id_user = '.$userId);
		$command->andWhere('c.course_type = "'.LearningCourse::TYPE_WEBINAR.'"');
		$command->group('idCourse');

		$rows= $command->queryAll();
		$result = array();
		foreach($rows as $row){
			$result[] = $row['idCourse'];
		}

		return $result;
	}

    /**
     * Get array of all GodAdmins of only the first one, with email address!!
     *
     * @param bool $onlyFirst Get only first godadmin
     * @param bool $onlyIdst Get array of idst ONLY
     * @return array|CoreUser array of idst | CoreUser
     */
	public static function getGodAdmins($onlyFirst = false, $onlyIdst = false){
        $result = array();

        if(empty(self::$allGodAdmins)){
            $admins = array();
            $criteria = new CDbCriteria();
            $criteria->addCondition(array(
                "groups.groupid = '/framework/level/godadmin'",
                "t.valid = " . self::USER_STATUS_ACTIVE
            ));
            $godAdmins = self::model()->with('groups')->findAll($criteria);

            if(!empty($godAdmins)){
                foreach ($godAdmins as $admin){
                    if(!empty($admin->email)){
                        $admins[] = $admin;
                    }
                }
            }

            self::$allGodAdmins = $admins;
        }

        foreach (self::$allGodAdmins as $admin){
            /**
             * @var $admin CoreUser
             */
            $result[] = $onlyIdst ? $admin->idst : $admin;
        }

        return $onlyFirst
            ? !empty($result)
                ? $result[0]
                : $result
            : $result;
    }
}
