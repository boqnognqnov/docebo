<?php

/**
 * This is the model class for table "core_admin_course".
 *
 * The followings are the available columns in table 'core_admin_course':
 * @property integer $idst_user
 * @property string $type_of_entry
 * @property string $id_entry
 */
class CoreAdminCourse extends CActiveRecord
{

	const TYPE_COURSE = 'course';
	const TYPE_COURSEPATH = 'coursepath';
	const TYPE_CATALOG = 'catalogue';
	const TYPE_CATEGORY = 'category';
	const TYPE_CATEGORY_DESC = 'category_desc';
	const TYPE_COURSE_ASSIGN_MODE = 'course_assign_mode';
	
	
	const COURSE_SELECTION_MODE_STANDARD = 'select_courses';
	const COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS = 'all_courses_from_visible_catalogs';
	const COURSE_SELECTION_MODE_ALL = '*';
    const COURSE_SELECTION_MODEL_ALL_WITH_PATHS = '**';
	const COURSE_SELECTION_MODEL_ALL_PATHS = 'all_coursepaths';
	//All courses / learning plans in visible catalogs
	const COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS = 'all_courses_coursepaths_in_catalogs';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_admin_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst_user', 'numerical', 'integerOnly'=>true),
			array('type_of_entry', 'length', 'max'=>50),
			array('id_entry', 'length', 'max'=>255),
			// The following rule is used by search().
			array('idst_user, type_of_entry, id_entry', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst_user' => 'Idst User',
			'type_of_entry' => 'Type Of Entry',
			'id_entry' => 'Id Entry',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst_user',$this->idst_user);
		$criteria->compare('type_of_entry',$this->type_of_entry,true);
		$criteria->compare('id_entry',$this->id_entry);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreAdminCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public $search_input = NULL;
	public $powerUserManager = NULL;

	/**
	 * Just the types constants packed in an array
	 * @return array
	 */
	public function getMemberTypes() {
		return array(
			self::TYPE_COURSE,
			self::TYPE_COURSEPATH,
			self::TYPE_CATALOG,
			self::TYPE_CATEGORY,
			self::TYPE_CATEGORY_DESC
		);
	}



	/**
	 * Retrieves members of a power users, with type specified
	 * @param numeric $idAdmin
	 * @param string|array $types requested members types
	 * @return array the list of retrieved members
	 */
	public static function getPowerUserMembers($idAdmin, $types) {
		if (is_string($types)) {
			if (in_array($types, self::getMemberTypes())) {
				$types = array($types);
			} else {
				return array();
			}
		}

		if (!is_array($types)) {
			return array();
		}

		$language = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$output = array();
		foreach ($types as $type) {
			switch ($type) {
				case self::TYPE_COURSE: {
					$list = Yii::app()->db->createCommand()
						->select("ac.id_entry, c.name ")
						->from(self::model()->tableName()." ac")
						->join(LearningCourse::model()->tableName()." c", "ac.id_entry = c.idCourse")
						->where("ac.idst_user = :idst_admin", array(':idst_admin' => $idAdmin))
						->andWhere("ac.type_of_entry = :type_of_entry", array(':type_of_entry' => self::TYPE_COURSE))
						->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'id' => (int)$item['id_entry'],
								'name' => $item['name'],
								'type' => self::TYPE_COURSE
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_COURSEPATH: {
					$list = Yii::app()->db->createCommand()
						->select("ac.id_entry, c.path_name AS name")
						->from(self::model()->tableName()." ac")
						->join(LearningCoursepath::model()->tableName()." c", "ac.id_entry = c.id_path")
						->where("ac.idst_user = :idst_admin", array(':idst_admin' => $idAdmin))
						->andWhere("ac.type_of_entry = :type_of_entry", array(':type_of_entry' => self::TYPE_COURSEPATH))
						->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'id' => (int)$item['id_entry'],
								'name' => $item['name'],
								'type' => self::TYPE_COURSEPATH
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_CATALOG: {
					$list = Yii::app()->db->createCommand()
						->select("ac.id_entry, c.name")
						->from(self::model()->tableName()." ac")
						->join(LearningCatalogue::model()->tableName()." c", "ac.id_entry = c.idCatalogue")
						->where("ac.idst_user = :idst_admin", array(':idst_admin' => $idAdmin))
						->andWhere("ac.type_of_entry = :type_of_entry", array(':type_of_entry' => self::TYPE_CATALOG))
						->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'id' => (int)$item['id_entry'],
								'name' => $item['name'],
								'type' => self::TYPE_CATALOG
							);
						}
						$output = array_merge($output, $typedList);
					}
				} break;
				case self::TYPE_CATEGORY:
					$list = Yii::app()->db->createCommand()
					                      ->select("ac.id_entry, c.translation")
					                      ->from(self::model()->tableName()." ac")
					                      ->join(LearningCourseCategory::model()->tableName()." c", "ac.id_entry = c.idCategory AND c.lang_code=:lang", array(':lang'=>$language))
					                      ->where("ac.idst_user = :idst_admin", array(':idst_admin' => $idAdmin))
					                      ->andWhere("ac.type_of_entry = :type_of_entry", array(':type_of_entry' => self::TYPE_CATEGORY))
					                      ->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'id' => (int)$item['id_entry'],
								'name' => $item['name'],
								'type' => self::TYPE_CATEGORY
							);
						}
						$output = array_merge($output, $typedList);
					}
					break;
				case self::TYPE_CATEGORY_DESC:
					$list = Yii::app()->db->createCommand()
					                      ->select("ac.id_entry, c.translation")
					                      ->from(self::model()->tableName()." ac")
					                      ->join(LearningCourseCategory::model()->tableName()." c", "ac.id_entry = c.idCategory AND c.lang_code=:lang", array(':lang'=>$language))
					                      ->where("ac.idst_user = :idst_admin", array(':idst_admin' => $idAdmin))
					                      ->andWhere("ac.type_of_entry = :type_of_entry", array(':type_of_entry' => self::TYPE_CATEGORY_DESC))
					                      ->queryAll();
					if (is_array($list) && !empty($list)) {
						$typedList = array();
						foreach ($list as $item) {
							$typedList[] = array(
								'id' => (int)$item['id_entry'],
								'name' => $item['name'],
								'type' => self::TYPE_CATEGORY_DESC
							);
						}
						$output = array_merge($output, $typedList);
					}
					break;
			}
		}

		return $output;
	}





	public static function getPowerUserCourses($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_COURSE));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['id']] = $item['name'];
			}
		}
		return $output;
	}

	/**
	 * Return List of items (entries) IDs which are members of catalogs a given Power User can control.
	 * If requested, return array of  Item ID -> Catalog ID array instead (extended info)
	 *
	 * @param integer $idAdmin  Supposedly Power User ID
	 * @param string $entryType Type of the entry (course, catalogue, coursepath, ... ; see constants)
	 * @param boolean $extendedInfo Request a little bit more info, rather than just list of IDs
	 * @return array
	 */
	public static function getPowerUserEntriesOfPuCatalogs($idAdmin, $entryType, $extendedInfo=false) {
	    $params = array(
	        ':idUser'               => $idAdmin,
	        ':entryTypeCatalogue'   => LearningCatalogueEntry::ENTRY_CATALOGUE,
	        ':entryType'            => $entryType,
	    );
	    $command = Yii::app()->db->createCommand();

	    if ($extendedInfo) {
	        $command->select("lce.idEntry as id, cac.id_entry as catalogId");
	    }
	    else {
	       $command->select("lce.idEntry as id");
	    }

	    $command->from("core_admin_course cac");
	    $command->join("learning_catalogue_entry lce", "lce.idCatalogue=cac.id_entry AND lce.type_of_entry=:entryType");
	    $command->where("cac.idst_user=:idUser AND cac.type_of_entry=:entryTypeCatalogue");

	    if ($extendedInfo) {
	       $rows = $command->queryAll(true, $params);
	       foreach ($rows as $row) {
	           $items[$row['id']] = $row['catalogId'];
	       }
	    }
	    else {
	        $items = $command->queryColumn($params);
	    }

	    if ($items && is_array($items )) {
	        return $items;
	    }
	    return array();
	}



	public static function getPowerUserCoursepaths($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_COURSEPATH));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['id']] = $item['name'];
			}
		}
		return $output;
	}

	public static function getPowerUserCatalogs($idAdmin) {
		$list = self::getPowerUserMembers($idAdmin, array(self::TYPE_CATALOG));
		$output = array();
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$output[$item['id']] = $item['name'];
			}
		}
		return $output;
	}

	/**
	 * @param      $idAdmin
	 * @param bool $catPlusDescendant - if True, get only those assigned categories
	 * that are selected as "Category + Descendants" manner. Otherwise, just get
	 * only selected categories (no descendants)
	 *
	 * @return array
	 */
	public static function getPowerUserCategories($idAdmin, $catPlusDescendant = false) {

		if($catPlusDescendant)
			$types = array(self::TYPE_CATEGORY_DESC);
		else
			$types = array(self::TYPE_CATEGORY);

		$list   = self::getPowerUserMembers( $idAdmin, $types);
		$output = array();
		if ( is_array( $list ) && ! empty( $list ) ) {
			foreach ( $list as $item ) {
				$output[ $item['id'] ] = $item['name'];
			}
		}

		return $output;
	}



	public static function updatePowerUserSelection($idAdmin, $list) {

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$existant = array(
				self::TYPE_COURSE => array_keys(self::getPowerUserCourses($idAdmin)),
				self::TYPE_COURSEPATH => array_keys(self::getPowerUserCoursepaths($idAdmin)),
				self::TYPE_CATALOG => array_keys(self::getPowerCatalogs($idAdmin))
			);

			$toCreate = array();
			$toDelete = array();

			if (isset($list[self::TYPE_COURSE])) {
				$toCreate[self::TYPE_COURSE] = array_diff($list[self::TYPE_COURSE], $existant[self::TYPE_COURSE]);
				$toDelete[self::TYPE_COURSE] = array_diff($existant[self::TYPE_COURSE], $list[self::TYPE_COURSE]);
			}
			if (isset($list[self::TYPE_COURSEPATH])) {
				$toCreate[self::TYPE_COURSEPATH] = array_diff($list[self::TYPE_COURSEPATH], $existant[self::TYPE_COURSEPATH]);
				$toDelete[self::TYPE_COURSEPATH] = array_diff($existant[self::TYPE_COURSEPATH], $list[self::TYPE_COURSEPATH]);
			}
			if (isset($list[self::TYPE_CATALOG])) {
				$toCreate[self::TYPE_CATALOG] = array_diff($list[self::TYPE_CATALOG], $existant[self::TYPE_CATALOG]);
				$toDelete[self::TYPE_CATALOG] = array_diff($existant[self::TYPE_CATALOG], $list[self::TYPE_CATALOG]);
			}

			if (!empty($toDelete)) {
				foreach ($toDelete as $type => $typedList) {
					switch ($type) {
						case self::TYPE_COURSE:
						case self::TYPE_COURSEPATH:
						case self::TYPE_CATALOG: {
							$criteria = new CDbCriteria();
							$criteria->addCondition('type_of_entry', $type);
							$criteria->addInCondition('id_entry', $typedList);
							$arList = self::model()->findAll($criteria);
							if (!empty($arList)) {
								foreach ($arList as $ar) {
									$rs = $ar->delete();
									if ($rs === FALSE) { throw new CException('Error while updating power user selection'); }
								}
							}
						} break;
					}
				}
			}

			if (!empty($toCreate)) {
				foreach ($toCreate as $type => $typedList) {
					switch ($type) {
						case self::TYPE_COURSE:
						case self::TYPE_COURSEPATH:
						case self::TYPE_CATALOG: {
							foreach ($typedList as $idEntry) {
								$ar = new CoreAdminCourse();
								$ar->idst_user = $idAdmin;
								$ar->id_entry = $idEntry;
								$ar->type_of_entry = $type;
								$rs = $ar->save();
								if (!$rs) { throw new CException('Error while updating power user selection'); }
							}
						} break;
					}
				}
			}

			//NOTE: this may not be necessary
			$rs = CoreUserPuCourse::model()->updatePowerUserSelection($idAdmin);
			if (!$rs) { throw new CException('Error while updating power user selection'); }

			if (isset($transaction)) { $transaction->commit(); }
		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return true;
	}



	public static function addPowerUserMember($idAdmin, $idMember, $updatePU = true) {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		try {
			if ($idAdmin != $idMember) {
				$ar = new self();
				$ar->idstAdmin = $idAdmin;
				$ar->idst = $idMember;
				if (!$ar->save()) { throw new CException('Error while saving power user member'); }
				if ($updatePU) {
					$rs = CoreUserPU::model()->updatePowerUserSelection($idAdmin);
					if (!$rs) { throw new CException('Error while update power user members'); }
				}
			}
			if (isset($transaction)) { $transaction->commit(); }
		} catch (CException $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
		return true;
	}



	/**
	 * Get list of power users to whom list of courses are assigned to. (reverse resolving)
	 * NOTE: also through Learning Plans and Catalogs
	 *
	 * @param array|boolean $coursesList Set to false to return data about ANY course
	 *
	 * @return array Array of <power-user-id> => array of <user-id>
	 */
	public static function getPowerUsersOfCourses($coursesList=false, $groupsList = array()) {

		static $cache = array();
		$cacheKey = sha1(json_encode(func_get_args()));

		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}


		// Filtering by EMPTY array definitely means empty result
		if ($coursesList !== false && empty($coursesList)) {
			$cache[$cacheKey] = array();
			return $cache[$cacheKey];
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select('cupc.puser_id as idstAdmin, GROUP_CONCAT(course.idCourse) AS courses_list');
		$commandBase->from('core_user_pu_course cupc');
		$commandBase->join('learning_course course', 'course.idCourse=cupc.course_id');

		if ($coursesList !== false) {
			$commandBase->where('course.idCourse IN (' . implode(',', $coursesList) . ')');
		}

		if(!empty($groupsList)) {
			$commandBase->join(CoreGroupMembers::model()->tableName().' cgm', 'cgm.idstMember = cupc.puser_id');
			$commandBase->andWhere(array('IN','cgm.idst', $groupsList));
		}

		$commandBase->group('idstAdmin');

		$result = array();

		// FROM COURSES relations
		$params = array();
		$reader = $commandBase->query($params);
		foreach ($reader as $row)
		{
			$toAdd = isset($row['courses_list']) ? explode(',',$row['courses_list']) : array();
			$result[$row['idstAdmin']] = $toAdd;
		}

		$cache[$cacheKey] = $result;
		return $cache[$cacheKey];
	}


	/**
	 * A helper array to get the preselected "items" to be passed to the
	 * FancyTree widget so they are displayed as selected in UI
	 *
	 * @param $idUser
	 * @return array
	 */
	public static function getFancyTreePreSelectedArrayCategories($idUser){
		$query = Yii::app()->getDb()->createCommand()
			->select('*')
			->from(self::model()->tableName())
			->where('idst_user=:idUser', array(':idUser'=>$idUser))
			->andWhere(array('IN', 'type_of_entry', array(self::TYPE_CATEGORY, self::TYPE_CATEGORY_DESC)))
			->queryAll();

		$res = array();

		foreach($query as $row){
			$newEntry = array();

			$newEntry['key'] = $row['id_entry'];

			switch($row['type_of_entry']){
				case self::TYPE_CATEGORY_DESC:
					// If it's a Category + Descendants, make the 'selectState'=2
					$newEntry['selectState'] = 2;
					break;
				default:
					// Otherwise just select the current item in the tree (not the descendants)
					$newEntry['selectState'] = 1;
					break;
			}

			$res[] = $newEntry;
		}

		return $res;
	}

	static public function getCourseAssignModeForPU($idPowerUser){
		$model = CoreAdminCourse::model()->findByAttributes(array(
			'idst_user'=>$idPowerUser,
			'type_of_entry'=>self::TYPE_COURSE_ASSIGN_MODE,
		));
		if($model && in_array($model->id_entry, array(
				self::COURSE_SELECTION_MODE_STANDARD,
				self::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS,
				self::COURSE_SELECTION_MODE_ALL,
                self::COURSE_SELECTION_MODEL_ALL_WITH_PATHS,
				self::COURSE_SELECTION_MODEL_ALL_PATHS,
				self::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS,
			))){
			return $model->id_entry;
		}

		return self::COURSE_SELECTION_MODE_STANDARD;
	}
	static public function setCourseAssignModeForPU($idPowerUser, $newCourseAssignMode){
		if(!in_array($newCourseAssignMode, array(
			CoreAdminCourse::COURSE_SELECTION_MODE_ALL,
			CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS,
			CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD,
            CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS,
			CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS,
			CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS,
		))){
			throw new CException( 'Invalid course selection mode' );
		}

		// Delete old Course Assign Mode for this PU
		CoreAdminCourse::model()->deleteAllByAttributes( array(
			'idst_user'     => $idPowerUser,
			'type_of_entry' => CoreAdminCourse::TYPE_COURSE_ASSIGN_MODE
		) );

        CoreUserPuCoursepath::model()->deleteAllByAttributes( array( 'puser_id' => $idPowerUser ) );

		$model                = new CoreAdminCourse();
		$model->idst_user     = $idPowerUser;
		$model->type_of_entry = CoreAdminCourse::TYPE_COURSE_ASSIGN_MODE;
		$model->id_entry = $newCourseAssignMode;
		if( ! $model->save() ) {
			Yii::log( print_r( $model->getErrors(), 1 ), CLogger::LEVEL_ERROR );
			throw new CException( 'Can not save course assignment mode for power user' );
		}

		return true;
	}

	public static function canPUManageCatalogs(){

		$result = array(
			'add' => false,
			'view' => false,
			'mod' => false,
			'del' => false,
		);

		if (Yii::app()->user->checkAccess('/framework/admin/catalogManagement/add')) {
			$result['add'] = true;
		}
		if (Yii::app()->user->checkAccess('/framework/admin/catalogManagement/view')) {
			$result['view'] = true;
		}
		if (Yii::app()->user->checkAccess('/framework/admin/catalogManagement/mod')) {
			$result['mod'] = true;
		}
		if (Yii::app()->user->checkAccess('/framework/admin/catalogManagement/del')) {
			$result['del'] = true;
		}

		return $result;
	}

	public static function canPUManageCurrentCatalog($idCatalogue, $action = 'view'){
		$actions = self::canPUManageCatalogs();

		// check if that catalog is assigned to the PU
		$catalogs = self::getPowerUserCatalogs(Yii::app()->user->id);
		if (in_array($idCatalogue, $catalogs, true) && $actions[$action] == true)
			return true;
		return false;
	}
}
