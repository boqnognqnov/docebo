<?php

/**
 * This is the model class for table "audit_trail_log".
 *
 * The followings are the available columns in table 'audit_trail_log':
 * @property integer $id
 * @property integer $idUser
 * @property string $action
 * @property integer $target_course
 * @property string $id_target
 * @property integer $target_user
 * @property string $json_data
 * @property string $ip
 * @property string $timestamp
 */
class AuditTrailLog extends CActiveRecord
{
	// These correspond to the raised Yii events and values in the "action" column of the "audit_trail_log" table
	// Only "added" actions/events are here. The actual list of logged events is much bigger
	// (see getAvailableCategoriesAndEvents method).
	const ACTION_SEAT_CONSUMED 							= 'onPuSeatConsume';
	const ACTION_SEAT_RESTORED 							= 'onPuSeatRestore';
	const ACTION_SEATS_BOUGHT 							= 'onPuBuySeats';
	const ACTION_ADMIN_ADDED_EXTRA_SEATS 				= 'AdminAddedExtraSeatsToPu';
	const ACTION_ADMIN_REMOVED_EXTRA_SEATS 				= 'AdminRemovedExtraSeatsToPu';
	const ACTION_ADMIN_REMOVE_CATEGORY 					= 'RemoveCategory';
	const ACTION_ADMIN_UPDATE_CATEGORY 					= 'UpdateCategory';
	const ACTION_ADMIN_UPDATE_CATALOGUE 				= 'EditedCatalogue';
	const ACTION_ADMIN_ADD_CATALOGUE 					= 'AddedCatalogue';
	const ACTION_ADMIN_DELETED_CATALOGUE 				= 'DeletedCatalogue';
	const ACTION_ADMIN_RESET_USER_LO_STATUS 			= 'ResetUserLoStatus';
	const ACTION_ADMIN_EDITED_COURSE_LO 				= 'EditedCourseLo';
	const ACTION_ADMIN_DELETED_COURSE_LO 				= 'DeletedCourseLo';
	const ACTION_ADMIN_CHANGED_USER_COURSE_STATUS	    = 'UserEnrollmentStatusChanged';
	const ACTION_ADDED_DOWNLOAD 						= 'CourseFileAdded';
	const ACTION_COURSE_FILE_DOWNLOAD 					= 'CourseFileDownloaded';
	const ACTION_LEARNING_COURSEPATH_ADDED 				= 'AddedLearningCoursepath';
	const ACTION_LEARNING_COURSEPATH_UPDATED 			= 'UpdatedLearningCoursepath';
	const ACTION_LEARNING_COURSEPATH_DELETED 			= 'DeletedLearningCoursepath';
    const ACTION_COURSE_ADDED_TO_LEARNING_COURSEPATH    = 'CourseAddedToCoursepath';
    const ACTION_COURSE_REMOVED_FROM_LEARNING_COURSEPATH= 'CourseRemovedFromCoursepath';
	const ACTION_CUSTOM_REPORT_PUBLISHED_UNPUBLISHED 	= 'ToggledPublicCustomReport';
	const ACTION_CUSTOM_REPORT_ADDED 					= 'CustomReportAdded';
	const ACTION_CUSTOM_REPORT_DELETED 					= 'CustomReportDeleted';

	/**
	 * Audit Trail event categories
	 * @var string
	 */
	const CATEGORY_PU 					= "power_user";
	const CATEGORY_COURSE 				= "course";
	const CATEGORY_LEARNING_PLANS 		= "learning_plan";
	const CATEGORY_CUSTOM_REPORT 		= "custom_report";
	const CATEGORY_USER_MANAGEMENT 		= "user_management";
    const CATEGORY_ENROLLMENT           = "enrollment";

    const SUB_EVENTS          			= "Subscription plan, bundle, record and seat association";




	/**
	 * List of translated action labels defined in this model
	 *
	 */
	public static function actionLabels() {

		$result = array(
				self::ACTION_SEAT_CONSUMED                                 => Yii::t('audit_trail', 'Allocated seat(s)'),
				self::ACTION_SEAT_RESTORED                                 => Yii::t('audit_trail', 'Reclaimed seat(s)'),
				self::ACTION_SEATS_BOUGHT 							       => Yii::t('audit_trail', 'Purchased seat(s)'),
				self::ACTION_ADMIN_ADDED_EXTRA_SEATS				       => Yii::t('audit_trail', 'Superadmin granted free extra seat(s) to PU'),
				self::ACTION_ADMIN_REMOVED_EXTRA_SEATS				       => Yii::t('audit_trail', 'Superadmin removed free extra seat(s) from PU'),
				self::ACTION_ADMIN_REMOVE_CATEGORY 					       => Yii::t('audit_trail', 'Admin removed category'),
				self::ACTION_ADMIN_UPDATE_CATEGORY 					       => Yii::t('audit_trail', 'Admin changed category'),
				self::ACTION_ADMIN_UPDATE_CATALOGUE 				       => Yii::t('audit_trail', 'Admin updated course catalog'),
				self::ACTION_ADMIN_ADD_CATALOGUE 					       => Yii::t('audit_trail', 'Admin created course catalog'),
				self::ACTION_ADMIN_DELETED_CATALOGUE 				       => Yii::t('audit_trail', 'Admin deleted course catalog'),
			    self::ACTION_ADMIN_RESET_USER_LO_STATUS				       => Yii::t('audit_trail', 'Admin has reset Learning Object for a User'),
				self::ACTION_ADMIN_EDITED_COURSE_LO 				       => Yii::t('audit_trail', 'Updated training material in a course'),
				self::ACTION_ADMIN_DELETED_COURSE_LO 				       => Yii::t('audit_trail', 'Deleted training material from course'),
				self::ACTION_ADDED_DOWNLOAD 						       => Yii::t('audit_trail', 'Uploaded file in File Download widget'),
				self::ACTION_COURSE_FILE_DOWNLOAD 					       => Yii::t('audit_trail', 'Downloaded file from File Download widget'),
				self::ACTION_LEARNING_COURSEPATH_ADDED 				       => Yii::t('audit_trail', 'Admin created a Learning Plan'),
				self::ACTION_LEARNING_COURSEPATH_UPDATED 			       => Yii::t('audit_trail', 'Admin updated a Learning Plan'),
				self::ACTION_LEARNING_COURSEPATH_DELETED 			       => Yii::t('audit_trail', 'Admin deleted a Learning plan'),
                self::ACTION_COURSE_ADDED_TO_LEARNING_COURSEPATH           => Yii::t('audit_trail', 'Admin added a course to a Learning Plan'),
                self::ACTION_COURSE_REMOVED_FROM_LEARNING_COURSEPATH       => Yii::t('audit_trail', 'Admin removed a course from a Learning Plan'),
				self::ACTION_CUSTOM_REPORT_PUBLISHED_UNPUBLISHED 	       => Yii::t('audit_trail', 'Admin published/unpublished Report'),
				self::ACTION_CUSTOM_REPORT_ADDED 					       => Yii::t('audit_trail', 'Admin created Report'),
				self::ACTION_CUSTOM_REPORT_DELETED 					       => Yii::t('audit_trail', 'Admin removed Report'),
				self::ACTION_ADMIN_CHANGED_USER_COURSE_STATUS		       => Yii::t('audit_trail', 'User enrollment status has been changed'),
				CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH    	       => Yii::t('event_manager','User assigned to a branch'),
				CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH 	       => Yii::t('event_manager','User removed from a branch'),
				CoreNotification::NTYPE_USER_SUSPENDED                     => Yii::t('audit_trail', 'User has been suspended'),
				CoreNotification::NTYPE_USER_UNSUSPENDED                   => Yii::t('audit_trail', 'User has been unsuspended'),
				CoreNotification::NTYPE_ILT_SESSION_DELETED                => Yii::t('notification', 'ILT session deleted'),
				CoreNotification::NTYPE_WEBINAR_SESSION_DELETED            => Yii::t('notification', 'Webinar session deleted'),
                EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION       => Yii::t('audit_trail', 'User enrolled in webinar session'),
                EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION   => Yii::t('audit_trail', 'User unenrolled from webinar session'),
                CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => Yii::t('audit_trail', 'User waiting to be approved in platform subscription'),
                CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG           => Yii::t('notification', 'User has been created (confirmed registration)'),

			    CoreNotification::NTYPE_SUB_PLAN_CREATED		  		   => Yii::t('notification', 'Create subscription plan'),
			    CoreNotification::NTYPE_SUB_PLAN_EDITED		               => Yii::t('notification', 'Edit subscription plan'),
			    CoreNotification::NTYPE_SUB_PLAN_DELETED		           => Yii::t('notification', 'Delete subscription plan'),

				CoreNotification::NTYPE_SUB_BUNDLE_CREATED		  		   => Yii::t('notification', 'Create subscription bundle'),
				CoreNotification::NTYPE_SUB_BUNDLE_EDITED	               => Yii::t('notification', 'Edit subscription bundle'),
				CoreNotification::NTYPE_SUB_BUNDLE_DELETED		           => Yii::t('notification', 'Delete subscription bundle'),
				CoreNotification::NTYPE_SUB_BUNDLE_ITEM_DELETED		       => Yii::t('notification', 'Delete subscription bundle\'s item'),
				CoreNotification::NTYPE_SUB_BUNDLE_ADD_ITEM		       	   => Yii::t('notification', 'Add subscription bundle\'s item'),

				CoreNotification::NTYPE_SUB_BUNDLE_ADD_VISIBILITY		   => Yii::t('notification', 'Add subscription bundle\'s visibility'),
				CoreNotification::NTYPE_SUB_BUNDLE_DELETE_VISIBILITY	   => Yii::t('notification', 'Delete subscription bundle\'s visibility'),

				CoreNotification::NTYPE_SUB_SEAT_ASSIGNED		  		   => Yii::t('notification', 'Assign seat'),
				CoreNotification::NTYPE_SUB_SEAT_EDITED	               	   => Yii::t('notification', 'Edit assigned seat'),
				CoreNotification::NTYPE_SUB_SEAT_UNASSIGNED		           => Yii::t('notification', 'Unassign seat'),

				CoreNotification::NTYPE_SUB_RECORD_CREATED		  		   => Yii::t('notification', 'Create subscription record'),
				CoreNotification::NTYPE_SUB_RECORD_EDITED              	   => Yii::t('notification', 'Edit subscription record'),
				CoreNotification::NTYPE_SUB_RECORD_DELETED	               => Yii::t('notification', 'Delete subscription record'),

				CoreNotification::NTYPE_SUB_RECORD_ADD_ITEM	               => Yii::t('notification', 'Add subscription record item'),
				CoreNotification::NTYPE_SUB_RECORD_DELETE_ITEM	           => Yii::t('notification', 'Delete subscription record item'),

		);

		return $result;
	}

	/**
	 * Return tranlsated action label.
	 * Searches in NEW actions and also in CoreNotification list of notification
	 *
	 * @param string $action
	 * @return string
	 */
	public static function actionLabel($action) {

		// First, Our own Action type/names
		$actions = self::actionLabels();

		// If not found, try getting event label from Norification mechanic
		if (!isset($actions[$action])) {
			$name = CoreNotification::typeName($action);
			// If also not found, just use the raw action name
			if (!$name) {
				$name = $action;
			}
		}
		else {
			$name = $actions[$action];
		}

		return $name;

	}


	/**
	 * Return list of categories, translated
	 * @return array
	 */
	public static function categories() {
		$result = array(
			self::CATEGORY_PU 				=> Yii::t('standard', 'Power users'),
			self::CATEGORY_COURSE			=> Yii::t('standard', '_COURSES'),
			self::CATEGORY_LEARNING_PLANS	=> Yii::t('standard', '_COURSEPATH'),
			self::CATEGORY_CUSTOM_REPORT	=> Yii::t('audit_trail', 'Custom Reports'),
			self::CATEGORY_USER_MANAGEMENT	=> Yii::t('audit_trail', 'User Management'),
            self::CATEGORY_ENROLLMENT       => Yii::t('standard', 'Enrollments'),
		);
		return $result;
	}


	/**
	 * Return translated name of a category
	 *
	 * @param string $categoryId
	 * @return string
	 */
	public static function categoryName($categoryId) {
		$cats = self::categories();
		return isset($cats[$categoryId]) ? $cats[$categoryId] : $categoryId;
	}



	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'audit_trail_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, action, ip', 'required'),
			array('idUser, target_course, target_user', 'numerical', 'integerOnly'=>true),
			array('action, id_target, ip', 'length', 'max'=>255),
			array('json_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, action, target_course, id_target, target_user, json_data, ip, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUser' => 'Id User',
			'action' => 'Action',
			'target_course' => 'Target Course',
            'id_target' => 'Id Target',
			'target_user' => 'Target User',
			'json_data' => 'Json Data',
			'ip' => 'Ip',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('target_course',$this->target_course);
        $criteria->compare('id_target',$this->id_target,true);
		$criteria->compare('target_user',$this->target_user);
		$criteria->compare('json_data',$this->json_data,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AuditTrailLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {

		// Make sure we have JSON data, at least an empty object
		if (!$this->json_data) {
			$this->json_data = '{}';
		}

		return parent::beforeSave();

	}


	/**
	 * Return a list of all available Audit Trail categories and the events attached/observed.
	 *
	 * Ex: 	array(<category-internal-id-name> => array(
	 * 			"name" => <translated category name>,
	 * 			"events" => array(
	 * 				array(
	 * 					"event_name"		=> <event-internal-id-name>,
	 * 					"event_label"		=> <translated event label>,
	 * 					"event_description"	=> <translated event description>
	 * 				)
	 * 			)
	 * 		)
	 *
	 * @return array
	 *
	 *
	 */
	static public function getAvailableCategoriesAndEvents($getLabels = true){

		$categories = array(

			self::CATEGORY_PU => array(
				'name'=> self::categoryName(self::CATEGORY_PU),
				'events'=>array(
					array(
							'event_name'=>self::ACTION_SEAT_CONSUMED,
							'event_description'=>Yii::t('audit_trail', 'E.g. PU allocated N seats to enroll U1, U2, etc. in course C on mm/dd/yyy HH:mm'),
					),
					array(
							'event_name'=>self::ACTION_SEAT_RESTORED,
							'event_description'=>Yii::t('audit_trail', 'E.g. PU reclaimed N seats by unenrolling users U1, U2, etc. from course C on mm/dd/yyy HH:mm'),
					),
					array(
							'event_name'=>self::ACTION_SEATS_BOUGHT,
							'event_description'=>Yii::t('audit_trail', 'E.g. PU purchased N seats for course C on mm/dd/yyy HH:mm'),
					),
					array(
						'event_name'=>self::ACTION_ADMIN_ADDED_EXTRA_SEATS,
						'event_description'=>Yii::t('audit_trail', 'E.g. Superadmin granted N extra seats to power user A for course C on mm/dd/yyy HH:mm'),
					),
					array(
						'event_name'=>self::ACTION_ADMIN_REMOVED_EXTRA_SEATS,
						'event_description'=>Yii::t('audit_trail', 'E.g. Superadmin removed N extra seats from power user A for course C on mm/dd/yyy HH:mm'),
					),
				),
			),

			self::CATEGORY_COURSE => array(
				'name'=> self::categoryName(self::CATEGORY_COURSE),
				'events'=>array(
					//start base Course events
					array(
							'event_name' 	=> CoreNotification::NTYPE_COURSE_DELETED,
							'event_description'=>Yii::t('audit_trail', 'Course deleted')
					),
					array(
							'event_name' 	=> CoreNotification::NTYPE_ILT_SESSION_DELETED,
							'event_description'=>Yii::t('notification', 'ILT session deleted')
					),
					array(
							'event_name' 	=> CoreNotification::NTYPE_WEBINAR_SESSION_DELETED,
							'event_description'=>Yii::t('notification', 'Webinar session deleted')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_NEW_COURSE,
							'event_description'=>Yii::t('audit_trail', 'Course created')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_COURSE_PROP_CHANGED,
							'event_description'=>Yii::t('audit_trail', 'Admin updated course')
					),

					//start Courses Category events
					array(
							'event_name'	=> CoreNotification::NTYPE_NEW_CATEGORY,
							'event_description'=>Yii::t('audit_trail', 'Admin created new category')
					),
					array(
							'event_name'	=> self::ACTION_ADMIN_REMOVE_CATEGORY,
							'event_description'=>Yii::t('audit_trail', 'Admin removed category')
					),
					array(
							'event_name'	=> self::ACTION_ADMIN_UPDATE_CATEGORY,
							'event_description'=>Yii::t('audit_trail', 'Admin changed category')
					),

					//start Courses Catalogue events
					array(
							'event_name'	=> self::ACTION_ADMIN_UPDATE_CATALOGUE,
							'event_description'=>Yii::t('audit_trail', 'Admin updated course catalog')
					),

					array(
							'event_name'	=> self::ACTION_ADMIN_ADD_CATALOGUE,
							'event_description'=>Yii::t('audit_trail', 'Admin created course catalog')
					),

					array(
							'event_name'	=> self::ACTION_ADMIN_DELETED_CATALOGUE,
							'event_description'=>Yii::t('audit_trail', 'Admin deleted course catalog')
					),
					//Start Courses LO operation events
					array(
							'event_name'	=> CoreNotification::NTYPE_NEW_LO_ADDED_TO_COURSE,
							'event_description'=>Yii::t('audit_trail', 'Added training material in a course')
					),
                    /* TOD0: fixme adjust self::ACTION_ADMIN_EDITED_COURSE_LO not to be raised on New LO creation when setting iLeft/iRight !!!
					array(
							'event_name'	=> self::ACTION_ADMIN_EDITED_COURSE_LO,
							'event_description'=>Yii::t('audit_trail', 'Updated training material in a course')
					),
                    */
					array(
							'event_name'	=> self::ACTION_ADMIN_DELETED_COURSE_LO,
							'event_description'=>Yii::t('audit_trail', 'Deleted training material from course')
					),
					//start File download area events
					array(
							'event_name'	=> self::ACTION_ADDED_DOWNLOAD,
							'event_description'=>Yii::t('audit_trail', 'Uploaded file in File Download widget')
					),
					array(
							'event_name'	=> self::ACTION_COURSE_FILE_DOWNLOAD,
							'event_description'=>Yii::t('audit_trail', 'Downloaded file from File Download widget')
					),
					array(
						'event_name' => self::ACTION_ADMIN_RESET_USER_LO_STATUS,
						'event_description'=>Yii::t('audit_trail', 'An Admin has reset the course Learning Object for a User'),
					)
				),
			),


			self::CATEGORY_LEARNING_PLANS => array(
				'name'=> self::categoryName(self::CATEGORY_LEARNING_PLANS),
				'events'=>array(
					//start base Learning Coursepath events
					array(
							'event_name' 	=> self::ACTION_LEARNING_COURSEPATH_DELETED,
							'event_description'=>Yii::t('audit_trail', 'Admin deleted a Learning plan')
					),
					array(
							'event_name'	=> self::ACTION_LEARNING_COURSEPATH_ADDED,
							'event_description'=>Yii::t('audit_trail', 'Admin created a Learning Plan')
					),
					array(
							'event_name'	=> self::ACTION_LEARNING_COURSEPATH_UPDATED,
							'event_description'=>Yii::t('audit_trail', 'Admin updated a Learning Plan')
					),
                    array(
                        'event_name'	=> self::ACTION_COURSE_ADDED_TO_LEARNING_COURSEPATH,
                        'event_description'=>Yii::t('audit_trail', 'Admin added a course to a Learning Plan')
                    ),
                    array(
                        'event_name'	=> self::ACTION_COURSE_REMOVED_FROM_LEARNING_COURSEPATH,
                        'event_description'=>Yii::t('audit_trail', 'Admin removed a course from a Learning Plan')
                    ),
				)
			),


			self::CATEGORY_CUSTOM_REPORT => array(
				'name'=> self::categoryName(self::CATEGORY_CUSTOM_REPORT),
				'events'=>array(
					//start base Custom Reports events
					array(
							'event_name'	=> self::ACTION_CUSTOM_REPORT_ADDED,
							'event_description'=>Yii::t('audit_trail', 'Admin created Report')
					),
					array(
							'event_name' 	=> self::ACTION_CUSTOM_REPORT_DELETED,
							'event_description'=>Yii::t('audit_trail', 'Admin removed Report')
					),
					array(
							'event_name'	=> self::ACTION_CUSTOM_REPORT_PUBLISHED_UNPUBLISHED,
							'event_description'=>Yii::t('audit_trail', 'Admin published/unpublished Report')
					)
				)
			),


			self::CATEGORY_USER_MANAGEMENT => array(
				'name'=> self::categoryName(self::CATEGORY_USER_MANAGEMENT),
				'events'=>array(
					//start base User Management events
					array(
							'event_name'	=> CoreNotification::NTYPE_NEW_USER_CREATED,
							'event_description'=>Yii::t('audit_trail', 'Admin created user')
					),
					array(
							'event_name' 	=> CoreNotification::NTYPE_USER_DELETED,
							'event_description'=>Yii::t('audit_trail', 'Admin deleted user')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_MODIFIED,
							'event_description'=>Yii::t('audit_trail', 'Admin updated user')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG,
							'event_description'=>Yii::t('audit_trail', 'User self-registers into the LMS')
					),
					//user assignment to groups events
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_SUBSCRIBED_GROUP,
							'event_description'=>Yii::t('audit_trail', 'Admin assigned user to a Group')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_REMOVED_FROM_GROUP,
							'event_description'=>Yii::t('audit_trail', 'Admin removed user from a Group')
					),
					//user assignment to branch
					array(
						'event_name'	=> CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH,
						'event_description'=>Yii::t('audit_trail', 'Admin assigned a user to a branch')
					),
					array(
						'event_name'	=> CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH,
						'event_description'=>Yii::t('audit_trail', 'Admin removed a user from a branch')
					),
					//user enrollment to courses events
					array(
						'event_name'=>CoreNotification::NTYPE_USER_SUSPENDED,
						'event_description'=>Yii::t('audit_trail', 'User has been suspended')
					),
					array(
						'event_name'	=> CoreNotification::NTYPE_USER_UNSUSPENDED,
						'event_description'=>Yii::t('audit_trail', 'User has been unsuspended')
					),
					array(
						'event_name'	=> CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION,
						'event_description'=>Yii::t('audit_trail', 'User confirmed email and waiting for approval. Users will not be filtered based on the previous screens')
					),
				)
			),
            self::CATEGORY_ENROLLMENT => array(
				'name'=> self::categoryName(self::CATEGORY_ENROLLMENT),
				'events'=>array(
                    // moved
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_SUBSCRIBED_COURSE,
							'event_description'=>Yii::t('audit_trail', 'User enrolled to a course')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_UNSUBSCRIBED,
							'event_description'=>Yii::t('audit_trail', 'User unenrolled from a course')
					),
					array(
							'event_name'	=> CoreNotification::NTYPE_USER_COURSELEVEL_CHANGED,
							'event_description'=>Yii::t('audit_trail', 'User level changed in a course')
					),
                    // new
					array(
                        'event_name'	=> CoreNotification::NTYPE_ILT_SESSION_USER_ENROLLED,
                        'event_description'=>Yii::t('audit_trail', 'Users enrolled to ILT session')
					),
					array(
						'event_name'	=> AuditTrailLog::ACTION_ADMIN_CHANGED_USER_COURSE_STATUS,
						'event_description'=>Yii::t('audit_trail', 'User enrollment status has been changed')
					),
					array(
                        'event_name'	=> CoreNotification::NTYPE_ILT_SESSION_USER_UNENROLLED,
                        'event_description'=>Yii::t('audit_trail', 'Users unenrolled from ILT session')
					),
					array(
                        'event_name'	=> EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION,
                        'event_description'=>Yii::t('audit_trail', 'User enrolled to webinar session')
					),
					array(
                        'event_name'	=> EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION,
                        'event_description'=>Yii::t('audit_trail', 'User unenrolled from webinar session')
					),
                )
            ),
			self::SUB_EVENTS => array(
				'name' => self::categoryName(self::SUB_EVENTS),
				'events' => array(
					// moved
					array(
						'event_name' => CoreNotification::NTYPE_SUB_PLAN_CREATED,
						'event_description' => Yii::t('audit_trail', 'Create subscription plan')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_PLAN_EDITED,
						'event_description' => Yii::t('audit_trail', 'Edit subscription plan')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_PLAN_DELETED,
						'event_description' => Yii::t('audit_trail', 'Delete subscription plan')
					),

					array(
						'event_name' => CoreNotification::NTYPE_SUB_RECORD_CREATED,
						'event_description' => Yii::t('audit_trail', 'Create subscription record')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_RECORD_EDITED,
						'event_description' => Yii::t('audit_trail', 'Edit subscription record')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_RECORD_DELETED,
						'event_description' => Yii::t('audit_trail', 'Delete subscription record')
					),

					array(
						'event_name' => CoreNotification::NTYPE_SUB_RECORD_ADD_ITEM,
						'event_description' => Yii::t('audit_trail', 'Add subscription record item')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_RECORD_DELETE_ITEM,
						'event_description' => Yii::t('audit_trail', 'Delete subscription record item')
					),

					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_CREATED,
						'event_description' => Yii::t('audit_trail', 'Create subscription bundle')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_EDITED,
						'event_description' => Yii::t('audit_trail', 'Edit subscription bundle')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_DELETED,
						'event_description' => Yii::t('audit_trail', 'Delete subscription bundle')
					),

					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_ADD_ITEM,
						'event_description' => Yii::t('audit_trail', 'Add subscription bundle\'s item' )
					),

					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_ITEM_DELETED,
						'event_description' => Yii::t('audit_trail', 'Delete subscription bundle\'s item' )
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_ADD_VISIBILITY,
						'event_description' => Yii::t('audit_trail', 'Add subscription bundle\'s visibility' )
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_BUNDLE_DELETE_VISIBILITY,
						'event_description' => Yii::t('audit_trail', 'Delete subscription bundle\'s visibility' )
					),



					array(
						'event_name' => CoreNotification::NTYPE_SUB_SEAT_ASSIGNED,
						'event_description' => Yii::t('audit_trail', 'Create subscription seat association')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_SEAT_EDITED,
						'event_description' => Yii::t('audit_trail', 'Edit subscription seat association')
					),
					array(
						'event_name' => CoreNotification::NTYPE_SUB_SEAT_UNASSIGNED,
						'event_description' => Yii::t('audit_trail', 'Delete subscription seat association')
					),

				)
			),

		);

		if($getLabels){
			// SET translated Evanet LABELS (adding a new array element to all event arrays
			foreach ($categories as $categoryName => $category)
				foreach ($category['events'] as $indexEvent => $event)
					$categories[$categoryName]['events'][$indexEvent]['event_label'] = self::actionLabel($event['event_name']);
		}




		return $categories;
	}



	/**
	 * Return a flat array of event-name => event-label pairs
	 *
	 * @return array
	 */
	static public function getAvailableEventsSimpleArray(){

		$categoriesAndEvents = AuditTrailLog::getAvailableCategoriesAndEvents();

		$res = array();
		foreach($categoriesAndEvents as $category){
			foreach ($category['events'] as $event){
				$res[$event['event_name']] = $event['event_label'];
			}
		}
		return $res;
	}


}
