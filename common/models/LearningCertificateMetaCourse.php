<?php

/**
 * This is the model class for table "learning_certificate_meta_course".
 *
 * The followings are the available columns in table 'learning_certificate_meta_course':
 * @property integer $id
 * @property integer $idMetaCertificate
 * @property integer $idUser
 * @property integer $idCourse
 * @property integer $idCourseEdition
 */
class LearningCertificateMetaCourse extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCertificateMetaCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_certificate_meta_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idMetaCertificate, idUser, idCourse, idCourseEdition', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idMetaCertificate, idUser, idCourse, idCourseEdition', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idMetaCertificate' => 'Id Meta Certificate',
			'idUser' => 'Id User',
			'idCourse' => 'Id Course',
			'idCourseEdition' => 'Id Course Edition',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idMetaCertificate',$this->idMetaCertificate);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idCourseEdition',$this->idCourseEdition);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}