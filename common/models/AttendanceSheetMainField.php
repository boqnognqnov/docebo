<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 22-Feb-16
 * Time: 2:32 PM
 * This is the model class for table "attendance_sheet_fields_main".
 * @property integer $id
 * @property integer $idCourse
 * @property integer $idSession
 * @property string $courseType
 * @property string $fieldName1
 * @property string $fieldName2
 * @property string $fieldName3
 * @property string $fieldName4
 * @property string $fieldName5
 * @property string $order
 * @property integer $prevent_session_overlap
 */
class AttendanceSheetMainField extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'attendance_sheet_fields_main';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idCourse, idSession', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCourse, idSession, courseType, fieldName1, fieldName2, fieldName3, fieldName4, fieldName5, order', 'safe', 'on' => 'search'),
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idCourse', $this->idCourse);
		$criteria->compare('courseType', $this->courseType);
		$criteria->compare('fieldName1', $this->fieldName1);
		$criteria->compare('fieldName2', $this->fieldName2);
		$criteria->compare('fieldName3', $this->fieldName3);
		$criteria->compare('fieldName4', $this->fieldName4);
		$criteria->compare('fieldName5', $this->fieldName5);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function attributeLabels()
	{
		return array(
			'idCourse' => Yii::t('standard', 'Course ID'),
			'courseType' => Yii::t('course', '_COURSE_TYPE'),
			'fieldName1' => 1,
			'fieldName2' => 2,
			'fieldName3' => 3,
			'fieldName4' => 4,
			'fieldName5' => 5,
		);
	}
}