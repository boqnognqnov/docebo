<?php

/**
 * This is the model class for table "learning_test_feedback".
 *
 * The followings are the available columns in table 'learning_test_feedback':
 * @property integer $id_feedback
 * @property integer $id_test
 * @property double $from_score
 * @property double $to_score
 * @property string $feedback_text
 *
 * The followings are the available model relations:
 * @property LearningTest $idTest
 */
class LearningTestFeedback extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTestFeedback the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_test_feedback';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_test, from_score, to_score, feedback_text', 'required'),
			array('id_test', 'numerical', 'integerOnly'=>true),
			array('from_score, to_score', 'numerical'),

			array('id_test, from_score, to_score, feedback_text', 'safe'),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_feedback, id_test, from_score, to_score, feedback_text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idTest' => array(self::BELONGS_TO, 'LearningTest', 'id_test'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_feedback' => 'Id Feedback',
			'id_test' => 'Id Test',
			'from_score' => 'From Score',
			'to_score' => 'To Score',
			'feedback_text' => 'Feedback Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_feedback',$this->id_feedback);
		$criteria->compare('id_test',$this->id_test);
		$criteria->compare('from_score',$this->from_score);
		$criteria->compare('to_score',$this->to_score);
		$criteria->compare('feedback_text',$this->feedback_text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Do some checks before saving data
	 * @param CEvent $event
	 * @throws CException
	 */
	public function onBeforeSave($event) {
		if ($this->from_score >= $this->to_score) {
			throw new CException('Invalid score range');
		}
		parent::onBeforeSave($event);
	}

	public function getDataProvider($idTest) {
		$criteria = new CDbCriteria();
		$criteria->addCondition('id_test = :id_test');
		$criteria->params = array(':id_test' => $idTest);
		$criteria->order = "from_score ASC";

		$provider = new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => false
		));

		return $provider;
	}


	public static function getFeedbackText($idTest, $score) {
		$criteria = new CDbCriteria();
		$criteria->addCondition("from_score <= :score");
		$criteria->addCondition("to_score >= :score");
		$criteria->params[':score'] = (double)$score;
		$info = LearningTestFeedback::model()->findByAttributes(array('id_test' => $idTest), $criteria);
		return ($info ? $info->feedback_text : false);
	}

	public function getFeedbackById($idTest){
		$results = Yii::app()->db->createCommand()
			->select('from_score, to_score, feedback_text')
			->from('learning_test_feedback')
			->where('id_test = :idTest', array(':idTest' => $idTest))
			->order('from_score ASC')
			->queryAll();
		return $results;
	}
}