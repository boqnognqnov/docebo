<?php

/**
 * This is the model class for table "core_user_pu_coursepath".
 *
 * The followings are the available columns in table 'core_user_pu_coursepath':
 * @property integer $puser_id
 * @property integer $path_id
 */
class CoreUserPuCoursepath extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUserPuCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user_pu_coursepath';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('puser_id, path_id', 'required'),
			array('puser_id, path_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('puser_id, path_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'puser_id' => 'Puser',
			'path_id' => 'Coursepath',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('puser_id',$this->puser_id);
		$criteria->compare('path_id',$this->path_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	public function getList($idAdmin) {
		static $cache = array();
		if(!isset($cache[$idAdmin])){
			$cache[$idAdmin] = Yii::app()->db->createCommand()
										 ->select("path_id")
										 ->from(self::model()->tableName())
										 ->where("puser_id = :id_admin", array(':id_admin' => $idAdmin))
										 ->queryColumn();
		}
		return $cache[$idAdmin];
	}


	/**
	 * The number of assigned coursepaths for a given power user
	 * @param int $idUser the idst of the power user
	 * @return int
	 */
	public function countUserLocations($idUser) {
		return Yii::app()->db->createCommand()
			->select("COUNT(*) AS count_coursepaths")
			->from(self::model()->tableName())
			->where("puser_id = :puser_id", array(':puser_id' => $idUser))
			->queryScalar();
	}

}