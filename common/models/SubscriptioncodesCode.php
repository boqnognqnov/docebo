<?php

/**
 * This is the model class for table "subscriptioncodes_code".
 *
 * The followings are the available columns in table 'subscriptioncodes_code':
 * @property string $code
 * @property integer $id_set
 * @property string $used_on
 * @property integer $used_by
 *
 * The followings are the available model relations:
 * @property CoreUser $usedBy
 * @property SubscriptioncodesSet $idSet
 */
class SubscriptioncodesCode extends CActiveRecord
{
	
	const STATUS_USED 		= 'used';
	const STATUS_UNUSED 	= 'unused';
	const STATUS_ALL 		= 'all';
	
	// Date/Time attributes for Localtime behavior
	public $timestampAttributes = array('used_on');
	public $dateAttributes = array();
	
	// Various additional attributes
	public $searchText = false;  // for filtering
	public $codeStatusFilter = false; // for filtering
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subscriptioncodes_code';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, id_set', 'required'),
			array('id_set, used_by', 'numerical', 'integerOnly'=>true),
			array('code', 'length', 'max'=>50),
			array('used_on', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('code, id_set, used_on, used_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'used_by'),
			'subscriptionSet' => array(self::BELONGS_TO, 'SubscriptioncodesSet', 'id_set'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'code' => 'Code',
			'id_set' => 'Id Set',
			'used_on' => 'Used On',
			'used_by' => 'Used By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('code',$this->code,true);
		$criteria->compare('id_set',$this->id_set);
		$criteria->compare('used_on',$this->used_on,true);
		$criteria->compare('used_by',$this->used_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubscriptioncodesCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => $this->timestampAttributes,
				'dateAttributes' => $this->dateAttributes
			)
		);
	}
	
	
	/**
	 * Data Provider for Add Codes grid
	 *  
	 * @return CActiveDataProvider
	 */
	public function dataProviderAddCodesGrid() {
		
		$criteria = new CDbCriteria;
		$criteria->compare('id_set',$this->id_set);
		
		// LEFT JOIN core_user. LEFT, because user could be deleted
		$criteria->with= array(
			'user' => array(
				'joinType' => 'LEFT JOIN'
			),
		);

		if ($this->searchText) {
			// !! COALESCE(), becayse user.useris MIGHT be NULL (LEFT JOIN)
			$criteria->addCondition("CONCAT(t.code, COALESCE(user.userid,'')) LIKE :search");
			$criteria->params[':search'] = '%' . $this->searchText . '%';
		}
		
		if ($this->codeStatusFilter) {
			switch ($this->codeStatusFilter) {
				case self::STATUS_USED:
					$criteria->addCondition('t.used_by IS NOT NULL');
					break;
				case self::STATUS_UNUSED:
					$criteria->addCondition('t.used_by IS NULL');
					break;
				case self::STATUS_ALL:
				default:
					break;
			}
		}

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => array('pageSize' => Settings::get('elements_per_page', 10))
		));
		
		
	}
	
	
	/**
	 *
	 * @return CActiveDataProvider
	 */
	public function dataProviderLogsGrid() {
		
		$commandBase = Yii::app()->db->createCommand();
		
		$commandBase->from(self::model()->tableName() . " t");
		$commandBase->join('subscriptioncodes_set ts', 't.id_set=ts.id_set');
		$commandBase->leftJoin('core_user user', 't.used_by=user.idst');
		$commandBase->where('t.used_by IS NOT NULL');
		
		$params = array();
		
		if ($this->searchText) {
			$commandBase->andWhere("CONCAT(t.code,user.userid,ts.name,t.used_on) LIKE :search");
			$params[':search'] = '%' . $this->searchText . '%';
		}
		
		$commandBase->order('t.used_on DESC');
		$commandBase->params = $params;
		
		$commandCounter	= clone $commandBase;
		$commandData 	= clone $commandBase;

		$commandCounter->select('count(t.code) as C');
		
		$row = $commandCounter->queryRow(true);
		$numRecords = $commandCounter->queryScalar();

		$commandData->select('t.code as codeName, ts.name as setName, t.used_on as usedOnDate, user.userid as userId, t.used_on');

		// Data provider config
		$pagination = array('pageSize' => Settings::get('elements_per_page', 10));
		$config = array(
			'totalItemCount'=>$numRecords,
			'pagination' => $pagination,
		);
		
		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, $config);
		
		return $dataProvider;
	
	
	}
	
	
	
	
	/**
	 * Creates as many new records as much the codeArray size is provided the code does not exist already.
	 * Code must be unique among ALL codes from ALL sets!
	 * 
	 * @param number $idSet
	 * @param array $codesArray
	 */
	public static function addCodesToSet($idSet, $codesArray) {
		if (empty($codesArray) || !is_array($codesArray) || (int) $idSet <= 0) return;
		
		$codesArray = array_unique($codesArray);
		
		foreach ($codesArray as $code) {
			$code = trim($code);
			if (!empty($code)) {
				if (!self::model()->exists('code=:code', array(':code' => $code))) {
					$model = new self();
					$model->code = $code;
					$model->id_set = $idSet;
					$model->save(false);
				}
			}
		}
	} 
	
	
}
