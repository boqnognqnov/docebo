<?php

/**
 * This is the model class for table "learning_course".
 *
 * The followings are the available columns in table 'learning_course':
 * @property integer $idCourse
 * @property integer $mpidCourse
 * @property integer $idCategory
 * @property string $code
 * @property string $name
 * @property string $description
 * @property string $lang_code
 * @property integer $status
 * @property integer $level_show_user
 * @property integer $subscribe_method
 * @property string $linkSponsor
 * @property string $imgSponsor
 * @property string $img_course
 * @property string $img_demothumb
 * @property string $img_material
 * @property string $img_othermaterial
 * @property string $player_bg
 * @property string $player_bg_aspect
 * @property string $course_demo
 * @property string $mediumTime
 * @property integer $permCloseLO
 * @property integer $userStatusOp
 * @property string $difficult
 * @property integer $show_progress
 * @property integer $show_time
 * @property integer $show_who_online
 * @property integer $show_extra_info
 * @property integer $show_rules
 * @property string $date_begin
 * @property string $date_end
 * @property string $hour_begin
 * @property string $hour_end
 * @property integer $valid_time
 * @property integer $valid_time_type
 * @property integer $abs_max_subscriptions
 * @property integer $max_num_subscribe
 * @property integer $min_num_subscribe
 * @property double $max_sms_budget
 * @property integer $selling
 * @property string $prize
 * @property string $course_type
 * @property string $policy_point
 * @property integer $point_to_all
 * @property integer $course_edition
 * @property string $classrooms
 * @property string $certificates
 * @property string $create_date
 * @property string $security_code
 * @property string $imported_from_connection
 * @property string $course_quota
 * @property string $used_space
 * @property double $course_vote
 * @property integer $allow_overbooking
 * @property integer $allow_automatically_enroll
 * @property integer $can_subscribe
 * @property integer $enable_self_unenrollment
 * @property integer $enable_session_self_unenrollment
 * @property integer $enable_unenrollment_on_course_completion
 * @property string $sub_start_date
 * @property string $sub_end_date
 * @property string $advance
 * @property string $autoregistration_code
 * @property integer $direct_play
 * @property integer $use_logo_in_courselist
 * @property integer $show_result
 * @property integer $credits
 * @property integer $auto_unsubscribe
 * @property string $unsubscribe_date_limit
 * @property integer $prerequisites_policy
 * @property string $player_layout
 * @property string $social_rating_settings
 * @property string $social_sharing_settings
 * @property integer $resume_autoplay
 * @property integer $enable_coaching
 * @property integer $learner_assignment
 * @property string $final_score_mode
 * @property integer $final_object
 * @property string $initial_score_mode
 * @property integer $initial_object
 * @property integer $coaching_option_allow_start_course
 * @property integer $coaching_option_allow_request_session_change
 * @property integer $coaching_option_access_without_assign_coach
 * @property integer $soft_deadline
 * @property integer $deep_link
 * @property integer $id_publisher
 * @property string $publish_date
 * @property integer $has_overview
 * @property integer $lo_max_attempts
 * @property integer $show_toc
 *
 *
 * The followings are the available model relations:
 * @property EcommerceCoupon[] $ecommerceCoupons
 * @property EcommerceTransactionInfo[] $transaction_items
 * @property LearningCourseCoachingSession[] $coachingSessions
 * @property LearningCourseuser[] $learningCourseusers
 * @property LearningLabel[] $learningLabels
 * @property LearningOrganization[] $learningOrganizations
 * @property ConferenceRoom[] $conferenceRooms
 * @property LtCourseSession[] $learningCourseSessions
 * @property WebinarSession[] $learningWebinarSessions
 * @property LearningForum[] $learningForums
 * @property LearningCourseRating $learningCourseRating
 * @property CoreNotification[] $notifications
 * @property LearningCoursepathCourses[] $learningPlans
 * @property CoreUser $publisher
 *
 */
class LearningCourse extends CActiveRecord {




	// Prerequisites policy (navigation rules)
	const NAVRULE_FREE = 1; // Free to run any LO, with no restrictions
	const NAVRULE_SEQUENTAL = 2; // Run next LOs only if all previous LOs are completed
	const NAVRULE_FINALTEST = 3; // Final LO in the list can be played ONLY if all previous LOs have been completed
	const NAVRULE_PRETEST = 4; // First LO must be completed before all other LOs; no other LO can be run if first one is not completed.

	const TYPE_CLASSROOM 	= 'classroom';
	const TYPE_ELEARNING 	= 'elearning';
	const TYPE_MOBILE		= 'mobile';
	const TYPE_WEBINAR		= 'webinar';

	const DIFFICULTY_VERY_EASY = 'veryeasy';
	const DIFFICULTY_EASY = 'easy';
	const DIFFICULTY_MEDIUM = 'medium';
	const DIFFICULTY_DIFFICULT = 'difficult';
	const DIFFICULTY_VERY_DIFFICULT = 'verydifficult';

	const COURSE_QUOTA_INHERIT = -1;

	public static $DEMO_MEDIA_CATEGORY_VIDEO = 'video';
	public static $DEMO_MEDIA_CATEGORY_SWF = 'swf';
	public static $DEMO_MEDIA_CATEGORY_UNKNOWN = 'unknown';

	public static $final_score_mode_cache = array();

	// CST_PREPARATION
	public static $COURSE_STATUS_PREPARATION = 0;
	// CST_AVAILABLE
	public static $COURSE_STATUS_AVAILABLE = 1;
	// CST_EFFECTIVE
	public static $COURSE_STATUS_EFFECTIVE = 2;
	// CST_CONCLUDED
	public static $COURSE_STATUS_CONCLUDED = 3;
	// CST_CANCELLED
	public static $COURSE_STATUS_CANCELLED = 4;

	/* Enrollment policy */

	const SUBSMETHOD_ADMIN = 0;
	const SUBSMETHOD_MODERATED = 1;
	const SUBSMETHOD_FREE = 2;

	/* Course subscription Policy */
	const COURSE_SUBSCRIPTION_CLOSED = 0;
	const COURSE_SUBSCRIPTION_OPEN = 1;
	const COURSE_SUBSCRIPTION_BETWEEN_DATES = 2;

	/** DISPLAY MODE OPTIONS */
	// Show course -  Everyone, and show on home page
	const DISPLAY_EVERYONE_HOMEPAGE = 0;
	// Show course - Only for logged in users
	const DISPLAY_ONLY_LOGGED_USER = 1;
	// Show course - Pnly users subscribed to the course
	const DISPLAY_ONLY_SUBSCRIBED_USERS = 2;

	const COURSE_TYPE_WAITING = 1;
	const COURSE_TYPE_CLASSROOM = 2;
	const COURSE_TYPE_ECOMMERCE = 3;
	const COURSE_TYPE_MARKETPLACE = 4;

	const RESUME_AUTOPLAY_UNSET = -1;
	const RESUME_AUTOPLAY_OFF = 0;
	const RESUME_AUTOPLAY_ON = 1;

	const SOFT_DEADLINE_OFF = 0;
	const SOFT_DEADLINE_ON = 1;

	const DEEP_LINK_OFF = 0;
	const DEEP_LINK_ON = 1;

	const FINAL_SCORE_TYPE_STANDARD = ''; // old logic ("Final bookmark" or the largest iLeft LO)
	const FINAL_SCORE_TYPE_KEY_OBJECT = 'single_LO_as_final';
	const INITIAL_SCORE_TYPE_KEY_OBJECT = 'single_LO_as_initial';
	const FINAL_SCORE_TYPE_SUM_ALL_COMPLETED = 'all_LOs_sum';
	const FINAL_SCORE_TYPE_AVG_ALL_COMPLETED = 'avg_LOs_sum';

	// Code was refactored to use TYPE_XXXXXX constants, but keep these two for backward compatibility
	const ELEARNING = 'elearning';
	const CLASSROOM = 'classroom';


	const COACHING_FILTER_ALL_USERS 	= 'all';
	const COACHING_FILTER_UNASSIGNED 	= 'unassigned';

	const VALID_TIME_TYPE_FIRST_COURSE_ACCESS = 0;
	const VALID_TIME_TYPE_BEING_ENROLLED = 1;

	public static $PLAYER_LAYOUT_PLAY_BUTTON = 'play_button';
	public static $PLAYER_LAYOUT_NAVIGATOR = 'navigator';
	public static $PLAYER_LAYOUT_LISTVIEW = 'listview';

	public $isCurrentUserSubscribedTo;

	public $cascade_on_editions; //used in course settings form as a flag
	public $labels; //to handle labels relation values

	public $assigned_certificate; //to handle assigned_certificate relation values

	public $inherit_quota;
	public $userLevels = array(); //keeping user levels from the settings form before mask them
	public $userStatuses = array(); //keeping user statuses from the settings form before mask them
	public $random_autoregistration_code;

	public $copyTraining;
	public $copyEnrollments;
	public $copyBlockWidgets;
	public $confirm;

	public $powerUserManager;

	public $certification;

	public $search_input; //used to collect and use general search inputs

	public $expired; // used in Custom Reports to determinate by the date_end and the certain date if a course is expired or not

	/**
	 * Used in Course create/edit forms
	 */
	public $selectedImage;
	public $attachments;

	/**
	 * Array containing the mediumTime field expanded into hours/minutes/seconds
	 * @var array
	 */
	public $durationHours;
	public $durationMinutes;
	public $durationSeconds;

	// Social and Raging properties
	public $social_sharing_custom;
	public $social_rating_custom;
	public $course_sharing_permission;
	public $course_user_share_permission;
	public $course_share_facebook;
	public $course_share_twitter;
	public $course_share_linkedin;
	public $course_share_google;
	public $course_rating_permission;

	protected $oldAttributes = array();

	// Coaching properties
	const COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_MAXIMIZED = 1;
	const COACHING_LEARNER_ASSIGNMENT_AUTOMATICALLY_BALANCED = 2;
	const COACHING_LEARNER_ASSIGNMENT_LEARNER_SELECTED = 3;
	const COACHING_LEARNER_ASSIGNMENT_ADMIN_SELECTED = 4;

    public static function subscriptionPolicyList() {
		return array(
			self::COURSE_SUBSCRIPTION_CLOSED => Yii::t('course', '_SUBSCRIPTION_CLOSED'),
			self::COURSE_SUBSCRIPTION_OPEN => Yii::t('course', '_SUBSCRIPTION_OPEN'),
			self::COURSE_SUBSCRIPTION_BETWEEN_DATES => Yii::t('course', '_SUBSCRIPTION_IN_PERIOD'),
		);
	}


	public static function subscribeMethodList() {
		return array(
			self::SUBSMETHOD_ADMIN => Yii::t('course', '_COURSE_S_GODADMIN'),
			self::SUBSMETHOD_MODERATED => Yii::t('course', '_COURSE_S_MODERATE'),
			self::SUBSMETHOD_FREE => Yii::t('standard', '_COURSE_S_FREE'),
		);
	}


	public static function difficultyList()
	{
		return array(
			self::DIFFICULTY_VERY_EASY => Yii::t('standard', '_DIFFICULT_VERYEASY'),
			self::DIFFICULTY_EASY => Yii::t('standard', '_DIFFICULT_EASY'),
			self::DIFFICULTY_MEDIUM => Yii::t('standard', '_DIFFICULT_MEDIUM'),
			self::DIFFICULTY_DIFFICULT => Yii::t('standard', '_DIFFICULT_DIFFICULT'),
			self::DIFFICULTY_VERY_DIFFICULT => Yii::t('standard', '_DIFFICULT_VERYDIFFICULT'),
		);
	}


	public static function statusesList()
	{
		return array(
			self::$COURSE_STATUS_PREPARATION => Yii::t('course', '_CST_PREPARATION'),
			self::$COURSE_STATUS_AVAILABLE => Yii::t('course', '_CST_AVAILABLE'),
			self::$COURSE_STATUS_EFFECTIVE => Yii::t('course', '_CST_CONFIRMED'),
			self::$COURSE_STATUS_CONCLUDED => Yii::t('course', '_CST_CONCLUDED'),
			self::$COURSE_STATUS_CANCELLED => Yii::t('course', '_CST_CANCELLED'),
		);
	}


	public static function typesList()
	{
		$type = array(
			self::TYPE_CLASSROOM => Yii::t('standard', '_CLASSROOM'),
			self::TYPE_ELEARNING => Yii::t('standard', '_ELEARNING'),
			// smartphone is not used anymore
			self::TYPE_MOBILE => Yii::t('standard', 'Smartphone'),
			self::TYPE_WEBINAR => Yii::t('webinar', 'Webinar'),
		);

		$customCourseDescriptors = array();
		Yii::app()->event->raise('CollectCustomCourseTypes',
			new DEvent(self, array('types' => &$customCourseDescriptors)));
		foreach ($customCourseDescriptors as $type_id => $descriptor) {
			$type[$type_id] = $descriptor['translation'];
		}

		return $type;
	}


	public $containChildren; // filter if true contain users from children nodes
	public $filterType; // filter by type

	public static function displayRulesList($key = null)
	{
		return array(
			self::DISPLAY_EVERYONE_HOMEPAGE => Yii::t('course', '_SC_EVERYWHERE'),
			self::DISPLAY_ONLY_LOGGED_USER => Yii::t('course', '_SC_ONLY_IN'),
			self::DISPLAY_ONLY_SUBSCRIBED_USERS => Yii::t('course', '_SC_ONLYINSC_USER'),
		);
	}

	public static function getFinalScoreModesList()
	{
		return array(
			self::FINAL_SCORE_TYPE_STANDARD => Yii::t('course', 'Do not enable score calculation for this course'),
			self::FINAL_SCORE_TYPE_KEY_OBJECT => Yii::t('course', 'The score generated by a learning object'),
			self::FINAL_SCORE_TYPE_SUM_ALL_COMPLETED => Yii::t('course',
				'The sum of all scores taken by the user in the course'),
			self::FINAL_SCORE_TYPE_AVG_ALL_COMPLETED => Yii::t('course',
				'The average of all scores taken by the user in the course'),
		);
	}

	public static function getInitialScoreModesList()
	{
		return array(
			self::FINAL_SCORE_TYPE_STANDARD => Yii::t('course',
				'Do not enable initial score calculation for this course'),
			self::INITIAL_SCORE_TYPE_KEY_OBJECT => Yii::t('course', 'The score generated by a learning object'),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourse the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			// set some mandatory field only in case of a scenario different from course setting
			array('name, description', 'required', 'except' => 'course_setting,combocopy'),
			array('name, description', 'purifyModelAttribute'),
			array(
				'mpidCourse, idCategory, status, level_show_user, subscribe_method, permCloseLO, userStatusOp, show_progress, show_time, show_who_online, show_extra_info, show_rules, valid_time, valid_time_type, abs_max_subscriptions, max_num_subscribe, min_num_subscribe, selling, point_to_all, course_edition, allow_overbooking, allow_automatically_enroll, can_subscribe, enable_self_unenrollment, enable_session_self_unenrollment, enable_unenrollment_on_course_completion, direct_play, use_logo_in_courselist, show_result, auto_unsubscribe, prerequisites_policy, containChildren, filterType, resume_autoplay, enable_coaching, learner_assignment, coaching_option_allow_start_course, coaching_option_allow_request_session_change, final_object, soft_deadline, initial_object, deep_link',
				'numerical',
				'integerOnly' => true
			),
			array('credits', 'type', 'type' => 'float'),
			array('max_sms_budget, course_vote, has_overview, show_toc', 'numerical'),
		    array('lo_max_attempts', 'numerical', 'integerOnly'=>true, 'min'=>0),
			array('code', 'length', 'max' => 50),
			array(
				'name, linkSponsor, imgSponsor, img_course, img_demothumb, img_material, img_othermaterial, player_bg, player_bg_aspect, prize, course_type, policy_point, classrooms, certificates, security_code, imported_from_connection, course_quota, used_space, advance, autoregistration_code, final_score_mode, initial_score_mode',
				'length',
				'max' => 255
			),
			array(
				'final_score_mode',
				'in',
				'range' => array_keys(self::getFinalScoreModesList()),
				'allowEmpty' => true
			),
			array(
				'initial_score_mode',
				'in',
				'range' => array_keys(self::getInitialScoreModesList()),
				'allowEmpty' => true
			),
			array('lang_code', 'length', 'max' => 100),
			array('mediumTime', 'length', 'max' => 10),
			array('difficult', 'length', 'max' => 13),
			array('hour_begin, hour_end', 'length', 'max' => 5),
			array(
				'random_autoregistration_code, inherit_quota, date_begin, date_end, create_date, sub_start_date, sub_end_date, unsubscribe_date_limit, cascade_on_editions, description, labels, userLevels, userStatuses, assigned_certificate, player_bg_aspect, durationHours, durationMinutes, durationSeconds, resume_autoplay, soft_deadline, deep_link, lo_max_attempts',
				'safe'
			),
			array('player_bg', 'unsafe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'idCourse, mpidCourse, idCategory, code, name, description, lang_code, status, level_show_user, subscribe_method, linkSponsor, imgSponsor, img_course, img_demothumb, img_material, img_othermaterial, course_demo, mediumTime, permCloseLO, userStatusOp, difficult, show_progress, show_time, show_who_online, show_extra_info, show_rules, date_begin, date_end, hour_begin, hour_end, valid_time, abs_max_subscriptions, max_num_subscribe, min_num_subscribe, max_sms_budget, selling, prize, course_type, policy_point, point_to_all, course_edition, classrooms, certificates, create_date, security_code, imported_from_connection, course_quota, used_space, course_vote, allow_overbooking, allow_automatically_enroll, can_subscribe, enable_self_unenrollment, enable_session_self_unenrollment, enable_unenrollment_on_course_completion, sub_start_date, sub_end_date, advance, autoregistration_code, direct_play, use_logo_in_courselist, show_result, credits, auto_unsubscribe, unsubscribe_date_limit, prerequisites_policy, enable_coaching, learner_assignment, coaching_option_allow_start_course, coaching_option_allow_request_session_change',
				'safe',
				'on' => array('search', 'update')
			),
			array(
				'attachments, selectedImage, player_layout, search_input',
				'safe',
			),
			array(
				'certification',
				'safe',
			),
			// files
			//array('img_course', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true,),
			array(
				'course_demo',
				'file',
				'types' => 'mp4,zip,doc,xls,ppt,jpg,jpeg,gif,png,txt,docx,pptx,ppsx,xlsx,pdf,csv,bmp',
				'allowEmpty' => true,
				'except' => 'update'
			),
			// Make these safe when trying to copy LO/Enrollments/Blocks in a combo
			array('copyTraining, copyEnrollments, copyBlockWidgets', 'safe', 'on' => array('combocopy')),
			array(
				'social_sharing_settings, social_rating_settings, social_sharing_custom, social_rating_custom, course_sharing_permission, course_user_share_permission, course_share_facebook, course_share_twitter, course_share_linkedin, course_share_google, course_rating_permission',
				'safe',
			),

		);

		$event = new DEvent($this, array('course' => $this));
		Yii::app()->event->raise('BeforeLearningCourseValidationRules', $event);
		if (!$event->shouldPerformAsDefault() && is_array($event->return_value['rules'])) {
			$rules = $event->return_value['rules'];
		}

		return $rules;
	}

	/**
	 * Validation rule that is to be used in AR models to remove unneeded JS code
	 *
	 * @param string $attribute the name of the attribute to be validated
	 * @param array $params options specified in the validation rule
	 *
	 * @return boolean Continue the validation process?
	 */
	public function purifyModelAttribute($attribute, $params)
	{
		$this->setAttribute($attribute,
			html_entity_decode(Yii::app()->htmlpurifier->purify($this->getAttribute($attribute))));
		return true;
	}

	public function scopes()
	{
		return array(
			'maintenance' => array(
				'condition' => 't.status = :status',
				'params' => array(':status' => self::$COURSE_STATUS_PREPARATION)
			),
			'active' => array(
				'condition' => 't.status = :status_available OR t.status = :status_effective',
				'params' => array(
					':status_available' => self::$COURSE_STATUS_AVAILABLE,
					':status_effective' => self::$COURSE_STATUS_EFFECTIVE
				)
			),
			'completed' => array(
				'condition' => 't.status = :status_concluded OR t.status = :status_cancelled',
				'params' => array(
					':status_concluded' => self::$COURSE_STATUS_CONCLUDED,
					':status_cancelled' => self::$COURSE_STATUS_CANCELLED
				)
			),
		);
	}

	public function afterDelete()
	{
		parent::AfterDelete();

		Yii::app()->event->raise('AfterCourseDeleted', new DEvent($this));
		Yii::app()->event->raise('NewCourseDeleted', new DEvent());

		LearningCoursepath::removeCourseFromPrerequisites($this->idCourse);

		// Flush category tree used in Catalog Dashlet
		LearningCourseCategory::flushTreeCache();

		CoreNotificationAssoc::model()->deleteAllByAttributes(array(
			'idItem' => $this->idCourse,
			'type' => CoreNotificationAssoc::NOTIF_ASSOC_COURSE
		));
		$core_enroll_rule_item = CoreEnrollRuleItem::model();
		if (!$core_enroll_rule_item->deleteAllByAttributes(array(
			'item_type' => 'course',
			'item_id' => $this->idCourse
		))
		) {
			return false;
		}
		return true;
	}

	public function afterFind()
	{
		parent::afterFind();

		$this->explodeMediumTime();

		//Restore sharing settings from JSON to the LearningCourse object
		if ($this->social_sharing_settings) {
			$this->setAttributes(CJSON::decode($this->social_sharing_settings));
			$this->social_sharing_custom = 1;
		} else //or get the values from core_settings
		{
			$this->course_sharing_permission = Settings::get('course_sharing_permission',
				AdvancedSettingsSocialRatingForm::COURSE_SHARE_DISABLED);
			$this->course_user_share_permission = Settings::get('course_user_share_permission',
				AdvancedSettingsSocialRatingForm::SETTING_OFF);
			$this->course_share_facebook = Settings::get('course_share_facebook',
				AdvancedSettingsSocialRatingForm::SETTING_OFF);
			$this->course_share_twitter = Settings::get('course_share_twitter',
				AdvancedSettingsSocialRatingForm::SETTING_OFF);
			$this->course_share_linkedin = Settings::get('course_share_linkedin',
				AdvancedSettingsSocialRatingForm::SETTING_OFF);
			$this->course_share_google = Settings::get('course_share_google',
				AdvancedSettingsSocialRatingForm::SETTING_OFF);
		}

		//Restore rating settings from JSON to the LearningCourse object
		if ($this->social_rating_settings) {
			$this->setAttributes(CJSON::decode($this->social_rating_settings));
			$this->social_rating_custom = 1;
		} else //or get the values from core_settings
		{
			$this->course_rating_permission = Settings::get('course_rating_permission',
				AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED);
		}

		$this->oldAttributes = $this->attributes;
	}

	public function setCourseDefaults()
	{
		// default values, they will be applied on validate
		$this->course_type = 'elearning';
		$this->status = self::$COURSE_STATUS_EFFECTIVE;
		$this->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$this->difficult = 'medium';
		$this->img_course = '';
		$this->subscribe_method = '2';
		$this->can_subscribe = '1';
		$this->enable_self_unenrollment = '0';
		$this->enable_session_self_unenrollment = '0';
		$this->enable_unenrollment_on_course_completion = '1';
		$this->show_progress = '1';
		$this->show_time = '1';
		$this->show_who_online = '1';
		$this->userStatusOp = (1 << 3);
		$this->hour_begin = '-1';
		$this->hour_end = '-1';
		$this->linkSponsor = 'http://';
		$this->use_logo_in_courselist = '1';
		$this->prerequisites_policy = '1';
		$this->create_date = Yii::app()->localtime->toLocalDateTime();
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ecommerceCoupons' => array(
				self::MANY_MANY,
				'EcommerceCoupon',
				'ecommerce_coupon_courses(id_course, id_coupon)'
			),
			'transaction_items' => array(self::HAS_MANY, 'EcommerceTransactionInfo', 'id_course'),
			'learningCourseusers' => array(self::HAS_MANY, 'LearningCourseuser', 'idCourse'),
			'coachingSessions' => array(self::HAS_MANY, 'LearningCourseCoachingSession', 'idCourse'),
			'enrolledCourseusers' => array(
				self::HAS_MANY,
				'LearningCourseuser',
				'idCourse',
				'on' => 'enrolledCourseusers.level = ' . LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
			),
			'enrolledCourseadmins' => array(
				self::HAS_MANY,
				'LearningCourseuser',
				'idCourse',
				'on' => 'enrolledCourseadmins.level <> ' . LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
			),
			'learningLabels' => array(
				self::MANY_MANY,
				'LearningLabel',
				'learning_label_course(id_course, id_common_label)'
			),
			'learningOrganizations' => array(self::HAS_MANY, 'LearningOrganization', 'idCourse'),
			'learningOrganization' => array(self::HAS_ONE, 'LearningOrganization', 'idCourse'), // ONE!!

			'learningOrgResources' => array(
				self::HAS_MANY,
				'LearningOrganization',
				'idCourse',
				'condition' => 'learningOrgResources.objectType != ""'
			), //excluding folders
			//'learningCourseDates' => array(self::HAS_MANY, 'LearningCourseDate', 'id_course'),
			'conferenceRooms' => array(self::HAS_MANY, 'ConferenceRoom', 'idCourse'),
			'playerCourseblocks' => array(self::HAS_MANY, 'PlayerBaseblock', 'course_id', 'order' => 'position ASC'),
			'coursesCategory' => array(
				self::BELONGS_TO,
				'LearningCourseCategory',
				'idCategory'
			),
			'coursesCategoryTranslated' => array(
				self::BELONGS_TO,
				'LearningCourseCategory',
				'idCategory',
				'condition' => 'coursesCategoryTranslated.lang_code=:lang',
				'params' => array(':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())),
			),
			'coursesCategoryTree' => array(
				self::BELONGS_TO,
				'LearningCourseCategoryTree',
				'idCategory'
			),
			'powerUserManagers' => array(self::MANY_MANY, 'CoreUser', 'core_user_pu_course(course_id, puser_id)'),
			'coreUserPuCourse' => array(self::HAS_MANY, 'CoreUserPuCourse', 'course_id'),
			'certificatesAssigned' => array(
				self::MANY_MANY,
				'LearningCertificate',
				'learning_certificate_course(id_course, id_certificate)'
			),
			// Single certificate
			'certificate' => array(self::HAS_ONE, 'LearningCertificateCourse', 'id_course'),
			'learningCourseuser' => array(self::HAS_ONE, 'LearningCourseuser', 'idCourse'),
			'learningCoursepathCourses' => array(self::HAS_ONE, 'LearningCoursepathCourses', 'id_item'),
			'learningPlans' => array(self::HAS_MANY, 'LearningCoursepathCourses', 'id_item'),
			'curriculas' => array(
				self::MANY_MANY,
				'LearningCoursepath',
				'learning_coursepath_courses(id_item, id_path)'
			),
			'learningCatalogueEntry' => array(
				self::HAS_MANY,
				'LearningCatalogueEntry',
				'idEntry',
				'condition' => 'type_of_entry = "course"'
			),
			'learningCourseSessions' => array(self::HAS_MANY, 'LtCourseSession', 'course_id'),
			'learningWebinarSessions' => array(self::HAS_MANY, 'WebinarSession', 'course_id'),
			'learningForums' => array(self::HAS_MANY, 'LearningForum', 'idCourse'),
			'learningTrackSession' => array(self::HAS_MANY, 'LearningTracksession', 'idCourse'), // used in new apis
			'learningCourseRating' => array(self::HAS_ONE, 'LearningCourseRating', 'idCourse'),
			'publisher' => array(self::BELONGS_TO, 'CoreUser', 'id_publisher'),
		);
	}


	public function behaviors()
	{
		return array(
			'modelSelect' => array(
				'class' => 'ModelSelectBehavior'
			),
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array(
					'create_date medium',
					'sub_start_date medium keep_utc',
					'sub_end_date medium keep_utc',
					'unsubscribe_date_limit medium keep_utc',
					'publish_date medium'
				),
				'dateAttributes' => array('date_begin short', 'date_end short')
			)
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCourse' => 'Id Course',
			'cascade_on_editions' => Yii::t('course', '_CASCADE_MOD_ON_EDITION'),
			'mpidCourse' => 'Mpid Course',
			'idCategory' => Yii::t('course', '_CATEGORY_SELECTED'),
			'labels' => Yii::t('standard', '_LABELS'),
			'code' => Yii::t('standard', '_COURSE_CODE'),
			'name' => Yii::t('standard', '_COURSE_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'lang_code' => Yii::t('course', '_COURSE_LANG_METHOD'),
			'status' => Yii::t('standard', '_STATUS'),
			'level_show_user' => 'Level Show User', // not used anymore
			'subscribe_method' => 'Subscribe Method',
			'linkSponsor' => 'Link Sponsor', // not used anymore
			'imgSponsor' => 'Img Sponsor', // not used anymore
			'img_course' => Yii::t('course', '_COURSE_LOGO'),
			'img_demothumb' => 'Img Demothumb',
			'img_material' => 'Img Material', // not used anymore?
			'img_othermaterial' => 'Img Othermaterial', // not used anymore?
			'course_demo' => Yii::t('course', '_COURSE_DEMO'),
			'mediumTime' => Yii::t('course', '_MEDIUM_TIME'),
			'permCloseLO' => 'Perm Close Lo', // not used anymore
			'userStatusOp' => 'User Status Op', // not used anymore
			'difficult' => Yii::t('standard', '_DIFFICULTY'),
			'show_progress' => Yii::t('course', '_SHOW_PROGRESS'), // not used anymore
			'show_time' => Yii::t('course', '_SHOW_TIME'), // not used anymore
			'show_who_online' => 'Show Who Online', // not used anymore
			'show_extra_info' => 'Show Extra Info', // not used anymore
			'show_rules' => 'Show Rules', // not used anymore
			'date_begin' => Yii::t('standard', '_DATE_BEGIN'),
			'date_end' => Yii::t('standard', '_DATE_END'),
			'expired' => Yii::t('notification', 'Course has expired'),
			'hour_begin' => 'Hour Begin', // not used anymore
			'hour_end' => 'Hour End', // not used anymore
			'valid_time' => Yii::t('course', '_DAY_OF_VALIDITY'),
			'abs_max_subscriptions' => 'Abs Max Subscriptions', //marketplace
			'max_num_subscribe' => Yii::t('course', '_MAX_NUM_SUBSCRIBE'),
			'min_num_subscribe' => 'Minimum subscriptions quota (0 = unlimited)', // not used anymore
			'max_sms_budget' => 'Max Sms Budget', // not used anymore
			'selling' => Yii::t('course', '_COURSE_SELL'),
			'prize' => Yii::t('transaction', '_PRICE'),
			'course_type' => Yii::t('course', '_COURSE_TYPE'),
			'policy_point' => 'Policy Point', // not used anymore
			'point_to_all' => 'Point To All', // not used anymore
			'course_edition' => 'Course Edition', // not used anymore
			'classrooms' => 'Classrooms', // not used anymore
			'certificates' => Yii::t('menu', '_CERTIFICATE'),
			'create_date' => Yii::t('report', '_CREATION_DATE'),
			'security_code' => 'Security Code', // not used anymore?
			'imported_from_connection' => 'Imported From Connection', // not used anymore
			'course_quota' => Yii::t('course', '_COURSE_QUOTA'), // not used anymore
			'used_space' => 'Used Space', // not used anymore
			'course_vote' => 'Course Vote', // not used anymore
			'allow_overbooking' => Yii::t('course', '_ALLOW_OVERBOOKING'),
			'allow_automatically_enroll' => Yii::t('course', '_ALLOW_OVERBOOKING'),
			'can_subscribe' => 'Can Subscribe',
			'enable_self_unenrollment' => Yii::t('course', '_SELF_UNENROLLMENT'),
			'enable_session_self_unenrollment' => Yii::t('course', '_SELF_SESSION_UNENROLLMENT'),
			'sub_start_date' => Yii::t('standard', '_FROM'),
			'sub_end_date' => Yii::t('standard', '_TO'),
			'advance' => 'Advance', // not used anymore
			'autoregistration_code' => Yii::t('course', '_COURSE_AUTOREGISTRATION_CODE'),
			'direct_play' => 'Direct Play', // not used anymore
			'use_logo_in_courselist' => 'Use Logo In Courselist', // not used anymore
			'show_result' => Yii::t('standard', '_SHOW_RESULTS'), // not used anymore
			'credits' => Yii::t('standard', '_CREDITS'),
			'auto_unsubscribe' => Yii::t('course', '_USER_CAN_UNSUBSCRIBE'), // not used anymore
			'unsubscribe_date_limit' => 'Unsubscribe Date Limit', // not used anymore
			'prerequisites_policy' => Yii::t("standard", "Navigation rules"),
			'inherit_quota' => Yii::t('course', '_INHERIT_QUOTA'), // not used anymore
			'player_layout' => Yii::t('standard', 'Training Material View'),
			'enable_coaching' => Yii::t('course', 'Enable Coaching'),
			'learner_assignment' => Yii::t('course', 'Learner Assignment'),
			'final_score_mode' => Yii::t('course', 'Course final score calculation (Bookmark)'),
			'coaching_option_allow_start_course' => 'Coaching Option Allow Start Course',
			'coaching_option_allow_request_session_change' => 'Coaching Option Allow Request Session Change',
			'initial_score_mode' => Yii::t('course', 'Course initial score calculation'),
			'channels' => Yii::t('app7020', 'Channels'),
		    'lo_max_attempts' => Yii::t('standard', 'Maximum attempts'),
		);
	}

	/**
	 * Parameterized name scope to filter by the users assigned to a power user
	 * @param $powerUserId
	 * @return $this
	 */
	public function powerUserAssigned($powerUserId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'join' => 'INNER JOIN core_user_pu_course cupc ON ( (t.idCourse=cupc.course_id) AND (cupc.puser_id=:idPowerUser) )',
			'params' => array(':idPowerUser' => $powerUserId)
		));
		return $this;
	}

	/**
	 * @param bool $isCourseMangement
	 * @return CActiveDataProvider
	 */
	public function dataProvider($isCourseMangement = false, $excludedCourses = array(), $unlimited = false)
	{
		$criteria = new CDbCriteria;
		$config = array();
		//$criteria->compare('t.name', $this->name, true);
		if (!empty($this->name)) {
			$criteria->addCondition("t.name LIKE :filter_name OR t.code LIKE :filter_name");
			$criteria->params[':filter_name'] = '%' . $this->name . '%';
		}
		if (!empty($this->course_type)) {
			$criteria->addCondition("t.course_type = :typeOfCourse");
			$criteria->params[':typeOfCourse'] = $this->course_type;
		}
		$criteria->compare('t.idCourse', $this->idCourse);
		//		$criteria->compare('t.userid', $this->userid, true);
		//		$criteria->compare('t.userid', '<>/Anonymous');
		//		$criteria->compare('t.firstname', $this->firstname, true);
		//		$criteria->compare('t.lastname', $this->lastname, true);
		//		$criteria->compare('t.level', $this->level);
		//		$criteria->compare('t.valid', $this->valid);

		$allowedTypes = array(self::TYPE_ELEARNING, self::TYPE_MOBILE, self::TYPE_WEBINAR);
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$allowedTypes[] = self::TYPE_CLASSROOM;
		}

		// Let custom plugins add their own custom course types
		$_customTypesList = array();
		Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$_customTypesList)));
		if (!empty($_customTypesList)) {
			$allowedTypes = array_merge($allowedTypes, array_keys($_customTypesList));
		}

		$criteria->compare('t.course_type', $allowedTypes);

		if (isset(Yii::app()->session['currentCourseNodeId']) && $isCourseMangement) {
			$currentNodeId = Yii::app()->session['currentCourseNodeId'];
			if (!empty($currentNodeId) && $currentNodeId != LearningCourseCategoryTree::getRootNodeId()) {
				if ($this->containChildren) {
					$allChildrenNodesIds = $this->getCurrentAndChildrenNodesList();
					if (!empty($allChildrenNodesIds)) {
						$criteria->with['coursesCategoryTree'] = array(
							'joinType' => 'INNER JOIN',
							'condition' => 'coursesCategoryTree.idCategory IN (\'' . implode('\',\'',
									$allChildrenNodesIds) . '\')',
						);
					}
				} else {
					$criteria->with['coursesCategoryTree'] = array(
						'joinType' => 'INNER JOIN',
						'scopes' => array('nodeCoursesOnly'),
					);
				}
				$criteria->together = true;
			}
		}

		if (!empty($excludedCourses)) {
			$criteria->addNotInCondition('t.idCourse', $excludedCourses);
		}

		if ($this->powerUserManager !== null) {
			switch ($this->scenario) {
				case 'power_user_assigned':
					$criteria->with['powerUserManagers'] = array(
						'joinType' => 'INNER JOIN',
						'condition' => 'powerUserManagers.idst=:puser_id',
						'params' => array(':puser_id' => $this->powerUserManager->idst),
					);
					$criteria->together = true;
					$criteria->group = 't.idCourse';
					break;
				case 'power_user_assign':
					$criteria->with['coreUserPuCourse'] = array(
						'joinType' => 'LEFT JOIN',
						'on' => 'coreUserPuCourse.puser_id = :puser_id',
						'params' => array(':puser_id' => $this->powerUserManager->idst),
					);
					$criteria->together = true;
					//$criteria->addCondition('coreUserPuCourse.course_id IS NULL');
					break;
			}
			if ($isCourseMangement) {
				//NOTE: for some reasons the "->with[]" sometimes do not work correctly. We had to change in into a direct join in order to make courses gridview working properly
				/*
				$criteria->with['coreUserPuCourse'] = array(
					'joinType' => 'JOIN',
					'on' => 'coreUserPuCourse.puser_id = :puser_id',
					'params' => array(':puser_id' => $this->powerUserManager->idst)
				);
				*/
				/* @var $db CDbConnection */
				$db = Yii::app()->db;
				$_alias = $db->quoteTableName('coreUserPuCourse');
				$criteria->join .= " JOIN " . $db->quoteTableName(CoreUserPuCourse::model()->tableName()) . " " . $_alias . " "
					. " ON (" . $db->quoteTableName('t') . "." . $db->quoteColumnName('idCourse') . " = " . $_alias . "." . $db->quoteColumnName('course_id') . " "
					. " AND " . $_alias . "." . $db->quoteColumnName('puser_id') . " = :puser_id_filter)";
				$criteria->params[':puser_id_filter'] = $this->powerUserManager->idst;
				
			}
		}
		if ($isCourseMangement) {
			$criteria->group = 't.idCourse';
		}
		// Filters
		$criteria = $this->addFiltersConditions($criteria, $isCourseMangement);
		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
				$sortAttributes['userLevel'] = 'adminLevel.groupid';
				$sortAttributes['enrollment'] = 'enrolled';
		$config['criteria'] = $criteria;

		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = $unlimited
			? false // NOTE: it MUST be exactly boolean 'false' value, not NULL or other empty values!
			: array(
				'pageSize' => Settings::get('elements_per_page', 10)
			);

		return new CActiveDataProvider(get_class($this), $config);
	}


	public function dataProviderPUManagement()
	{

		if (!$this->powerUserManager) {
			//this dataprovider is only for power users management, so go back to index
			Yii::app()->controller->redirect(Docebo::createAppUrl('admin:PowerUserApp/powerUserManagement'));
		}

		$puserId = $this->powerUserManager->idst;

		$params = array(
			':puserId' => $puserId,
		);

		// TODO: do it with CDbCommand class functions, avoid direct SQL

		// Count courses
		$coursesCount = Yii::app()->getDb()->createCommand()
			->select('COUNT(c.idCourse)')
			->from(LearningCourse::model()->tableName() . ' c')
			->join(CoreAdminCourse::model()->tableName() . ' pu',
				'c.idCourse = pu.id_entry AND pu.type_of_entry=:courses AND pu.idst_user = :idUser',
				array(':idUser' => $puserId, ':courses' => CoreAdminCourse::TYPE_COURSE));

		if ($this->name) {
			$coursesCount->where('c.name LIKE :name', array(
				':name' => "%" . $this->name . "%"
			));
		}
		$count = $coursesCount->queryScalar();

		// Count also Learning Plans
		if (PluginManager::isPluginActive('CurriculaApp')) {
			$pathCount = Yii::app()->getDb()->createCommand()
				->select('COUNT(c.id_path)')
				->from(LearningCoursepath::model()->tableName() . ' c')
				->join(CoreUserPuCoursepath::model()->tableName() . ' pu',
					'c.id_path = pu.path_id AND pu.puser_id = :idUser', array(':idUser' => $puserId));
			if ($this->name) {
				$pathCount->where('c.path_name LIKE :name', array(
					':name' => "%" . $this->name . "%"
				));
			}
			$count += $pathCount->queryScalar();
		}

		// And count assigned Course Categories
		$countCategories = Yii::app()->getDb()->createCommand()
			->select('COUNT(pu.id_entry)')
			->from(LearningCourseCategoryTree::model()->tableName() . ' c')
			->join(CoreAdminCourse::model()->tableName() . ' pu',
				'c.idCategory=pu.id_entry
			   AND (pu.type_of_entry=:category OR pu.type_of_entry=:categoryDesc)
			   AND pu.idst_user=:idUser');

		if ($this->name) {
			$countCategories->join(LearningCourseCategory::model()->tableName() . ' lc',
				'c.idCategory = lc.idCategory AND lc.lang_code = :lang', array(
					':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
				))
				->where('lc.translation LIKE :name', array(
					':name' => "%" . $this->name . "%"
				));
		}
		$count += $countCategories->queryScalar(array(
			':idUser' => $puserId,
			':category' => CoreAdminCourse::TYPE_CATEGORY,
			':categoryDesc' => CoreAdminCourse::TYPE_CATEGORY_DESC
		));

		$nameSearch = '';
		if ($this->name) {
			$nameSearch = 'WHERE c.name like :nameSearch';
			$params[':nameSearch'] = '%' . $this->name . '%';
		}
		$query = "(SELECT CONCAT_WS('_', c.idCourse, '" . CoreAdminCourse::TYPE_COURSE . "') AS compositePK, c.idCourse AS id_item, c.code, c.name, c.create_date, c.course_type, '" . CoreAdminCourse::TYPE_COURSE . "' AS item_type "
			. " FROM learning_course c JOIN core_admin_course pu ON (c.idCourse = pu.id_entry AND pu.type_of_entry = :type_course AND pu.idst_user = :puserId) $nameSearch) ";
		$params[':type_course'] = CoreAdminCourse::TYPE_COURSE;

		if (PluginManager::isPluginActive('CurriculaApp')) {
			$pathNameSearch = '';
			if ($this->name) {
				$pathNameSearch = 'WHERE c.path_name like :pathNameSearch';
				$params[':pathNameSearch'] = '%' . $this->name . '%';
			}
			$query .= " UNION "
				. "(SELECT CONCAT_WS('_', c.id_path, '" . CoreAdminCourse::TYPE_COURSEPATH . "') AS compositePK, c.id_path AS id_item, c.path_code AS code, c.path_name AS name, c.create_date, '' AS course_type, '" . CoreAdminCourse::TYPE_COURSEPATH . "' AS item_type "
				. " FROM learning_coursepath c JOIN core_user_pu_coursepath pu ON (c.id_path = pu.path_id AND pu.puser_id = :puserId) $pathNameSearch)";
		}

		// Select the assigned Course Categories too
		$translationNameSearch = '';
		if ($this->name) {
			$translationNameSearch = ' AND c2.translation LIKE :translationNameSearch';
			$params[':translationNameSearch'] = '%' . $this->name . '%';
		}

		$query .= "
		UNION
		(
			SELECT CONCAT_WS('_', cat_assigned.id_entry, cat_assigned.type_of_entry) AS compositePK, cat_assigned.id_entry AS id_item, '' AS code, c2.translation AS name, '' AS create_date, '' AS course_type, cat_assigned.type_of_entry AS item_type
			FROM core_admin_course cat_assigned JOIN learning_category c2 ON
			(
				cat_assigned.id_entry=c2.idCategory AND
				cat_assigned.idst_user=:puserId AND
				c2.lang_code=:lang AND
				cat_assigned.type_of_entry IN (:cat, :catDesc)
				$translationNameSearch
			)
		)
		";
		$params[':lang'] = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$params[':cat'] = CoreAdminCourse::TYPE_CATEGORY;
		$params[':catDesc'] = CoreAdminCourse::TYPE_CATEGORY_DESC;

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		return new CSqlDataProvider($query, array(
			'totalItemCount' => $count,
			'sort' => array(
				'attributes' => $sortAttributes,
				'defaultOrder' => 'create_date DESC'
			),
			'keyField' => 'compositePK',
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
			'params' => $params,
		));
	}

	/**
	 * @return CActiveDataProvider
	 */
	public function dataProviderCatalogue($excludedCourses = array())
	{
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->compare('t.name', $this->name, true);
		$criteria->compare('t.idCourse', $this->idCourse);
		//		$criteria->compare('t.userid', $this->userid, true);
		//		$criteria->compare('t.userid', '<>/Anonymous');

		if ($this->powerUserManager !== null) {
			//TO DO: make a better implementation with criteria's "with" clausole. Power user idst should be passed through parameters
			$criteria->join = ' JOIN core_user_pu_course pu ON (t.idCourse = pu.course_id AND puser_id = :puser_id) ';
			if (is_int($this->powerUserManager)) {
				$criteria->params[':puser_id'] = $this->powerUserManager;
			} elseif (is_a($this->powerUserManager, 'CoreUser')) {
				$criteria->params[':puser_id'] = $this->powerUserManager->idst;
			} else {
				throw new CException('powerUserManager property of LearningCourse is of invalid type');
			}
		}

		//check if we are allowed to retrieve classroom courses
		if (!PluginManager::isPluginActive('ClassroomApp')) {
			$criteria->addCondition("t.course_type <> :course_type");
			$criteria->params[':course_type'] = LearningCourse::TYPE_CLASSROOM;
		}

		if (!empty($excludedCourses)) {
			$criteria->addNotInCondition('idCourse', $excludedCourses);
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;

		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}


	/**
	 * @param $id
	 * @return CActiveDataProvider
	 */
	public function dataProviderCurricula($id)
	{
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->compare('t.name', $this->name, true);

		//check if we are allowed to retrieve classroom courses
		if (!PluginManager::isPluginActive('ClassroomApp')) {
			$criteria->addCondition("t.course_type <> :course_type");
			$criteria->params[':course_type'] = LearningCourse::TYPE_CLASSROOM;
		}

		if ($this->powerUserManager !== null) {
			//TO DO: make a better implementation with criteria's "with" clausole. Power user idst should be passed through parameters
			$criteria->join = ' JOIN core_user_pu_course pu ON (t.idCourse = pu.course_id AND puser_id = :puser_id) ';
			$criteria->params[':puser_id'] = $this->powerUserManager->idst;
		}

		$criteria->join .= " LEFT JOIN " . LearningCoursepathCourses::model()->tableName() . " cc ON t.idCourse = cc.id_item AND cc.id_path = :id_path";
		$criteria->params[':id_path'] = $id;
		$criteria->addCondition("cc.id_item IS NULL");

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}


	/**
	 * @return array
	 */
	public function getCurrentAndChildrenNodesList()
	{
		$currentNodeId = Yii::app()->session['currentCourseNodeId'];
		$allChildrenNodesIds = array();
		if (!empty($currentNodeId) && $currentNodeId != LearningCourseCategoryTree::getRootNodeId()) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'idCategory = ' . $currentNodeId;
			$currentNode = LearningCourseCategoryTree::model()->find($criteria);
			if ($currentNode) {
				$allChildrenNodes = $currentNode->descendants()->findAll();
				$allChildrenNodesIds = CHtml::listData($allChildrenNodes, 'idCategory', 'idCategory');
				$allChildrenNodesIds = array_merge(array($currentNode->idCategory => $currentNode->idCategory),
					$allChildrenNodesIds);
			}
		}
		return $allChildrenNodesIds;
	}


	/**
	 * @return string
	 */
	public function renderCourseActions()
	{

		if ($this->course_type == LearningCourse::TYPE_CLASSROOM || $this->course_type == LearningCourse::TYPE_WEBINAR) {
			if (Yii::app()->user->getIsPu()) {
				// A power user has this course assigned, but
				// has no mod/del permission so don't render the course
				// actions button
				if (Yii::app()->user->checkAccess('/lms/admin/course/view') && (!Yii::app()->user->checkAccess('/lms/admin/course/mod') && !Yii::app()->user->checkAccess('/lms/admin/course/del'))) {
					return;
				}
			}
		}

		$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
		$with = array(
			'courses' => array(
				'on' => 'courses.lang_code = :lang',
				'params' => array('lang' => $lang->lang_code)
			)
		);
		// $currentNode = LearningCourseCategoryTree::model()->with($with)->findByPk($this->idCategory);
		// if (!empty($currentNode)) {
		// 	$parentNodes = array_merge($currentNode->ancestors()->with($with)->findAll(), array($currentNode));
		// } else {
		// 	$parentNodes = array(LearningCourseCategoryTree::model()->roots()->with($with)->find());
		// }

		$countEnrolledStudents = LearningCourseuser::getCountEnrolledByLevel($this->idCourse,
			LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		$countEnrolledAdmins = LearningCourseuser::getCountEnrolledAdmins($this->idCourse);

		$disabledActions = array();
		Yii::app()->event->raise('BeforeCourseTreeRowActionsRender', new DEvent($this, array(
			'course' => $this,
			'disabledActions' => &$disabledActions
		)));

		$courseActions = Yii::app()->getController()->renderPartial('_courseActions', array(
			'id' => $this->idCourse,
			'name' => $this->name,
			'courseType' => $this->course_type,
			//'parentNodes' => $parentNodes
			//'countUsers' => (!empty($this->enrolledCourseusers) ? count($this->enrolledCourseusers) : 0),
			//'countAdmins' => (!empty($this->enrolledCourseadmins) ? count($this->enrolledCourseadmins) : 0),
			'countUsers' => $countEnrolledStudents,
			'countAdmins' => $countEnrolledAdmins,
			'isSelling' => (PluginManager::isPluginActive('EcommerceApp') || PluginManager::isPluginActive('ShopifyApp')) && $this->selling,
			'sellingPrice' => (!empty($this->prize)
				? Yii::t('transaction', '_PRICE') . '<br>' . $this->prize . ' ' . Settings::get('currency_symbol',
					'&euro;')
				: Yii::t('course_management', '_NOT_SELLED')),
			'isMarketplaceCourse' => $this->mpidCourse ? true : false,
			'disabledActions' => $disabledActions
		), true);
		$content = Chtml::link('', 'javascript:void(0);', array(
			'id' => 'popover-' . uniqid(),
			'class' => 'popover-action popover-trigger',
			'data-toggle' => 'popover',
			'data-content' => $courseActions,
		));
		return $content;
	}


	/**
	 * Renders AVAILABLE seats column in courses grid
	 * @return string
	 */
	public function renderMaxAvailable()
	{
		$content = "";
		switch ($this->course_type) {
			case self::TYPE_CLASSROOM: {
				$content .= '<span id="tmp-classroom-' . $this->idCourse . '"></span>';
			}
				break;
			case self::TYPE_WEBINAR: {
				$content .= '<span id="tmp-webinar-' . $this->idCourse . '"></span>';
			}
			default: {
				$customCourseDescriptors = array();
				Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent($this, array('types' => &$customCourseDescriptors)));
				if(isset($customCourseDescriptors[$this->course_type]))
					$content .= '<span id="tmp-'.$this->course_type.'-' . $this->idCourse . '"></span>';
				else {
					//--- CSP LOs check ---
					if (LearningCourse::model()->hasCspObjects($this->idCourse)) {
						return '<span class="maxAvailable"></span>';
					}
					//---
					$availableSeats = $this->getAllSeats()-$this->getTakenSeats();
					$subscriptionsLabel = ($availableSeats > 0) ? $availableSeats : '-';
					$content .= (($this->mpidCourse > 0)
						? '<div data-toggle="tooltip" class="tooltip-enabled" title="' . Yii::t('standard', 'Available seats') . '">' . $subscriptionsLabel . '<span class="maxAvailable"></span></div>'
						: '');
				}
			}
				break;
		}
		return $content;
	}


	/**
	 * @return string
	 */
	public function renderType()
	{
		switch ($this->course_type) {
			case self::TYPE_ELEARNING:
			case self::TYPE_CLASSROOM:
			case self::TYPE_WEBINAR:
			case self::TYPE_MOBILE: {
				$class = $this->course_type;
				$title = $this->getCourseTypeTranslation($this->course_type);
			}
				break;

			default: {
				$class = "";
				$title = "";
				$customCourseDescriptors = array();
				Yii::app()->event->raise('CollectCustomCourseTypes',
					new DEvent($this, array('types' => &$customCourseDescriptors)));
				if (isset($customCourseDescriptors[$this->course_type])) {
					$class = $this->course_type;
					$title = $customCourseDescriptors[$this->course_type]['translation'];
				}
			}
				break;
		}
		$content = '<span class="' . $class . '" title="' . $title . '"></span>';
		return $content;
	}


	/**
	 * @return string
	 */
	public function renderEnrolled(
		$params = array('disallowPUEnrollCoursesWithLPs' => false, 'tooltip' => false, 'tooltipTitle' => '')
	) {
		// set default values
		$content = '';
		$titleContent = $params['tooltipTitle'];

		switch ($this->course_type) {
			case self::TYPE_WEBINAR:
			case self::TYPE_CLASSROOM: {

			}
				break;
			default: {
				//enrollments screen is visible to any power users (with or without enroll permission)
				$canEnroll = (Yii::app()->user->isGodAdmin || (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('enrollment/create')));
				$content = '';


				//$countEnrollments = count($this->learningCourseusers);
				$countEnrollments = LearningCourseuser::getCountEnrolledByLevel($this->idCourse, null, false);

				if ($countEnrollments > 0 || ($countEnrollments <= 0 && !$canEnroll)) {
					$content = $countEnrollments . '<span class="enrolled"></span>';
				} else {
					//this will be showed only to god admin and power users with enroll permission
					//set course by id
					$courseModel = LearningCourse::model()->findByPk($this->idCourse);

					//get the LPs, to which the course is assigned
					$itemsLPs = $courseModel->getCourseLPs();

					if ($params['disallowPUEnrollCoursesWithLPs'] && Yii::app()->user->isAdmin && (count($itemsLPs) > 0)) {
						//if set 'disallowPUEnrollCoursesWithLPs' and user is PU and there are LPs to which the course is assigned
						$content = '<span class="enroll enrolled_gray">' . Yii::t('standard', 'Enroll') . '</span>';
						$canEnroll = false;
					} else {
						$content = '<span class="enroll">' . Yii::t('standard', 'Enroll') . '</span>';
					}
				}
				if ($canEnroll || (!$canEnroll && $countEnrollments > 0)) {
					$content = CHtml::link($content, Yii::app()->urlManager->createUrl('courseManagement/enrollment',
						array('id' => $this->idCourse)));
				} else {
					if (!$params['tooltip']) {
						// don't show tooltip
						$content = CHtml::link($content, 'javascript:;');
					} else {
						//show a tooltip
						$content = CHtml::link($content, '#!', array(
							'title' => $titleContent,
							'rel' => 'tooltip',
							'data-html' => true,
							'class' => 'lps-list',
							'onclick' => 'javascript:;'
						));
					}
				}
			}
				break;
		}
		return $content;
	}


	/**
	 * @return string
	 */
	public function renderWaiting()
	{
		$canModWaitingList = true;

		// If user is PU check waiting list permission
		if (Yii::app()->user->getIsPu()) {
			if (Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('/lms/admin/course/moderate')) {
				$canModWaitingList = true;
			} else {
				$canModWaitingList = false;
			}
		}
		$content = '';
		switch ($this->course_type) {
			case self::TYPE_WEBINAR:
			case self::TYPE_CLASSROOM: {

			}
				break;
			default: {
				$count = $this->countElearningWaitingUsers();

				// Show link button only if waiting list permission is checked and count > 0
				if ($canModWaitingList) {
					if ($count == 0) {
						$content = 0;
					} else {
						$content = CHtml::link($count, Yii::app()->createUrl('courseManagement/waitingEnrollment',
							array('id' => $this->idCourse)));
					}
				} else {
					$content = $count;
				}
			}
				break;
		}
		return $content;
	}

	/**
	 * @return string
	 */
	public function countElearningWaitingUsers()
	{
		return LearningCourseuser::model()->with(array('user' => array('joinType' => 'INNER JOIN')),
			'course')->countByAttributes(array(
			'idCourse' => $this->idCourse,
			'waiting' => 1,
		));
	}

	/**
	 * @param CDbCriteria $criteria
	 * @param bool $isCoursesManagement
	 *
	 * @return CDbCriteria the same criteria to allow method chaining
	 */
	public function addFiltersConditions($criteria, $isCoursesManagement = false)
	{
		if (!empty($this->filterType)) {
			foreach ($this->filterType as $type) {
				switch ($type) {
					case LearningCourse::COURSE_TYPE_WAITING :
						if ($isCoursesManagement) {
							$courses = LearningCourse::getCoursesWithAllKindOfWaitingUsers(true);
							$coursesArray = array();
							foreach ($courses as $course) {
								$coursesArray[] = $course['idCourse'];
							}
							$criteria->addInCondition('t.idCourse', $coursesArray);
						} else {
							// Constrain courses list to only those that
							// have waiting users in them
							$criteria->with['learningCourseuser'] = array(
								'joinType' => 'JOIN',
								'on' => 'learningCourseuser.waiting=1',
							);
							$criteria->with['learningCourseuser.user'] = array(
								'joinType' => 'INNER JOIN',
							);
						}
						break;
					case LearningCourse::COURSE_TYPE_CLASSROOM :
						// Constrain courses list to only classroom type ones
						$criteria->compare('t.course_type', 'classroom');
						break;
					case LearningCourse::COURSE_TYPE_ECOMMERCE :
						// Constrain courses list to only those that are being sold
						// and have a price set
						$criteria->compare('t.selling', '1');
						$criteria->compare('t.prize', '>0');
						break;
					case LearningCourse::COURSE_TYPE_MARKETPLACE :
						$criteria->compare('t.mpidCourse', '<>0');
						break;
					case 'enrolled_only' :
					$coursesWithEnrollments = Yii::app()->getDb()->createCommand()
						->select('lcu.idCourse')
						->from(LearningCourseUser::model()->tableName().' lcu')
						->group('idCourse');
						if (Yii::app()->user->getIsAdmin()) {
							$coursesWithEnrollments->join(CoreUserPU::model()->tableName().' pu', 'lcu.idUser=pu.user_id');
							$coursesWithEnrollments->andWhere('puser_id = '.Yii::app()->user->id);
						}
						$criteria->addInCondition('t.idCourse', $coursesWithEnrollments->queryColumn());
						break;
					default : {
						$event = new DEvent($this, array('filter_id' => $type, 'criteria' => $criteria));
						Yii::app()->event->raise('CourseManagementFilterApplyCriteria', $event);
					}
				}
			}
		}
		return $criteria;
	}


	/**
	 * Returns URL to the course logo (local, assets based, marketplace,...)
	 *
	 * @param string $variant
	 * @return string
	 */
	public function getCourseLogoUrl($variant = CoreAsset::VARIANT_ORIGINAL, $dashboard = false)
	{

		// Get id or filename from course field
		$imageCourse = trim($this->img_course);

		$arCheck = array('http:', 'https:');
		$arReplace = array('', '');

		// Check Assets for logo, if any
		$url = CoreAsset::url($imageCourse, $variant);
		if (!empty($url) && (!$this->mpidCourse || ($this->mpidCourse && stripos($imageCourse,
						'courselogo') === false))
		) {
			if ((stripos($url, 'http:') === 0) || (stripos($url, 'https:') === 0)) {
				$url = str_ireplace($arCheck, $arReplace, $url);
			}
			return $url;
		}

		// No assets, ok. Set to Default: URL to No-Logo image in Yii themes/standard/images/course
		$url = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
		if (!$dashboard) {
			$url = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo.png';
		} else {
			$url = Docebo::getRootBaseUrl(true) . '/themes/spt/images/course/course_nologo230x230.png';
		}


		// If img_course is empty, BUT Demo Thumb is specified...  use it
		if (empty($imageCourse) && !empty($this->img_demothumb)) {
			$url = $this->img_demothumb;
			if ((stripos($url, 'http:') === 0) || (stripos($url, 'https:') === 0)) {
				$url = str_ireplace($arCheck, $arReplace, $url);
			}
			return $url;
		}

		// Looks like img_course is empty or we didn't find logo so far.
		// Now we have to check if LOCAL file exists named after img_course.
		// because LMS USER MAY edit Marketplace Course Logo, in which case the
		// Logo is saved locally, making MP based logo orphaned (the Cloudfront one)
		$filePath = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . 'doceboLms' . DIRECTORY_SEPARATOR . Settings::get('pathcourse') . $imageCourse;

		// Definitely use LOCAL logo if exists
		if (is_file($filePath)) {
			$url = Docebo::filesBaseUrl() . '/doceboLms/' . rtrim(Settings::get('pathcourse'),
					'/') . '/' . $imageCourse;

			// Fix local course logo issue on the fly
			/*
			if ($asset = $this->fixLocalCourseLogo()) {
				$url = $asset->getUrl($variant);
			}
			*/


		} else {
			if (!empty($imageCourse) && $this->mpidCourse && ($this->mpidCourse != 0)) {
				$url = Yii::app()->params['mp_cloudfront'] . '/catalogo_docebo/thumbnails/courses/' . $imageCourse;
			}
		}

		// Fix path for https, removing http in order to have the url start with //
		if ((stripos($url, 'http:') === 0) || (stripos($url, 'https:') === 0)) {
			$url = str_ireplace($arCheck, $arReplace, $url);
		}

		return $url;
	}


	/**
	 * Returns the number of users subscribed to THIS course. Filter by power user, if requested.
	 *
	 * @param bool|string $checkPowerUser Should we filter by power user?
	 * @param bool|false|number $idPowerUser ID of the Power User to filter by, if previous parameter is true. FALSE=check current user
	 * @param bool $includeWaiting
	 * @return number
	 */
	public function getTotalSubscribedUsers($checkPowerUser = false, $idPowerUser = false, $includeWaiting = true)
	{

		if ($checkPowerUser) {
			if (!$idPowerUser && Yii::app()->user->getIsAdmin()) {
				$idPowerUser = Yii::app()->user->id;
			}
		}

		$criteria = new CDbCriteria();

		if ((int)$idPowerUser > 0) {
			$criteria->join = "INNER JOIN core_user_pu cup on ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )";
			$criteria->params[':idPowerUser'] = $idPowerUser;
			$criteria->together = true;
		}

		if (!$includeWaiting) {
			switch ($this->course_type) {
				case self::TYPE_ELEARNING :
					$criteria->addCondition('t.waiting = 0');
					break;
				case self::TYPE_CLASSROOM :
					$criteria->join .= " JOIN lt_course_session s ON s.course_id = t.idCourse";
					$criteria->join .= " LEFT JOIN lt_courseuser_session cus ON s.id_session = cus.id_session AND t.idUser = cus.id_user";
					$criteria->addCondition('cus.id_user IS NOT NULL');
					break;
				case self::TYPE_WEBINAR :
					$criteria->join .= " JOIN webinar_session s ON s.course_id = t.idCourse";
					$criteria->join .= " LEFT JOIN webinar_session_user wus ON s.id_session = wus.id_session AND t.idUser = wus.id_user";
					$criteria->addCondition('wus.id_user IS NOT NULL');
					break;
			}
		}

		$criteria->addCondition('idCourse=:idCourse');
		$criteria->params[':idCourse'] = $this->idCourse;

		$count = LearningCourseuser::model()->count($criteria);
		return $count;
	}

	/**
	 * Returns the number of users subscribed as students to THIS course. Filter by power user, if requested.
	 *
	 * @param bool|string $checkPowerUser Should we filter by power user?
	 * @param bool|false|number $idPowerUser ID of the Power User to filter by, if previous parameter is true. FALSE=check current user
	 * @return number
	 */
	public function getTotalSubscribedStudents($checkPowerUser = false, $idPowerUser = false)
	{

		if ($checkPowerUser) {
			if (!$idPowerUser && Yii::app()->user->getIsAdmin()) {
				$idPowerUser = Yii::app()->user->id;
			}
		}

		$criteria = new CDbCriteria();

		if ((int)$idPowerUser > 0) {
			$criteria->join = "INNER JOIN core_user_pu cup on ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )";
			$criteria->params[':idPowerUser'] = $idPowerUser;
			$criteria->together = true;
		}

		$criteria->addCondition('idCourse=:idCourse');
		$criteria->addCondition('level=:level');
		$criteria->params[':idCourse'] = $this->idCourse;
		$criteria->params[':level'] = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
		$count = LearningCourseuser::model()->count($criteria);
		return $count;
	}

	public function updateMaxSubscriptionQuota()
	{
		if ($this->max_num_subscribe > 0) { // max subscription quota
			$subscribedUsers = $this->getTotalSubscribedUsers(false, false, false);
			if ($subscribedUsers > $this->max_num_subscribe) {
				Yii::app()->db->createCommand()->update(
					self::model()->tableName(),
					array('max_num_subscribe' => $subscribedUsers),
					'idCourse = :idCourse',
					array(':idCourse' => $this->idCourse)
				);
			}
		}
	}

	/**
	 * Automatically enroll users from waiting list on selfunenroll and on administrator increases the Max subscriptions quota.
	 */
	public function getFreePositions($sessionModel = null, $usersToUnenroll = 0)
	{
		if ($this->selling == 1 || $this->allow_automatically_enroll == 0) {
			return 0;
		}
		if ($this->allow_automatically_enroll) {
			switch ($this->course_type) {
				case self::TYPE_ELEARNING :
					$waitingUsers = $this->countElearningWaitingUsers();
					$subscribedUsers = $this->getTotalSubscribedUsers(false, false, false);
					$freePositions = $this->max_num_subscribe - ($subscribedUsers - $usersToUnenroll);
					if ($freePositions > 0 && ($waitingUsers > 0 && $this->allow_automatically_enroll)) {
						return $freePositions;
					}
					break;
				case self::TYPE_CLASSROOM:
				case self::TYPE_WEBINAR:
					if ($sessionModel) {
						$waitingUsers = $sessionModel->countWaiting(false);
						$freePositions = $sessionModel->max_enroll - ($sessionModel->countEnrollments(false,
									true) - $usersToUnenroll);
						if ($freePositions > 0 && ($waitingUsers > 0 && $this->allow_automatically_enroll)) {
							return $freePositions;
						}
					}
					break;
			}
		}
		return 0;
	}

	/**
	 * Automatically enroll users from waiting list on selfunenroll and on administrator increases the Max subscriptions quota.
	 */
	public function automaticallyEnroll($courseType, $sessionInstance = null, $unenrolledUsers)
	{

		$freePositions = $this->getFreePositions($sessionInstance);
		$usersToEnroll = ($unenrolledUsers > $freePositions) ? $freePositions : $unenrolledUsers;

		if ($freePositions && $freePositions > 0) {
			switch ($courseType) {
				case self::TYPE_ELEARNING :
					Yii::app()->db->createCommand("UPDATE learning_courseuser
					SET waiting =0, status =:subscribed
					WHERE idCourse=:idCourse AND waiting = 1
					ORDER BY date_inscr ASC
					LIMIT :usersToSubsribe")
						->bindValues(array(
							':idCourse' => $this->idCourse,
							':usersToSubsribe' => $usersToEnroll,
							':subscribed' => LearningCourseuser::$COURSE_USER_SUBSCRIBED
						))
						->query();
					break;
				case self::TYPE_CLASSROOM:
					$listOfWaitingToEnroll = Yii::app()->db->createCommand()
						->select('lt_courseuser_session.id_user')
						->from('lt_courseuser_session')
						->join('lt_course_session',
							'lt_course_session.id_session = lt_courseuser_session.id_session')
						->where('lt_courseuser_session.status = :waitingStatus',
							['waitingStatus' => LtCourseuserSession::$SESSION_USER_WAITING_LIST])
						->andWhere('lt_course_session.id_session = :idSession',
							['idSession' => $sessionInstance->id_session])
						->order('date_subscribed ASC')
						->limit($usersToEnroll)
						->queryAll();

					foreach ($listOfWaitingToEnroll as $oneUserRecord) {
						$record = LtCourseuserSession::model()->findByAttributes(array(
							'id_user' => $oneUserRecord['id_user'],
							'id_session' => $sessionInstance->id_session
						));
						$record->status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;
						if ($record->hasAttribute('waiting')) {
							$record->waiting = 0;
						} // NOTE: "waiting" column is deprecated for "webinar_session_user" table. Use "status" instead.
						$status = $record->save(false);

						if ($status) {
							Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED,
								new DEvent($this, array(
									'session_id' => $sessionInstance->id_session,
									'user' => $oneUserRecord['id_user'],
								)));
						}

						// also change the status of the corresponding _courseuser row
						$cuModel = LearningCourseuser::model()->findByAttributes(array(
							'idUser' => $oneUserRecord['id_user'],
							'idCourse' => $this->idCourse
						));
						if ($cuModel) {
							$cuModel->status = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
							$cuModel->waiting = 0;  // In case the subscription is moderated
							$cuModel->save();


							// Raised when the user's subscription for a moderated course is approved by admin
							Yii::app()->event->raise('UserApproval', new DEvent($this, array(
								'course' => $cuModel->course, // AR relation
								'user' => $cuModel->user, // AR relation
							)));
						}
					}

					break;
				case self::TYPE_WEBINAR:
					$listOfWaitingToEnroll = Yii::app()->db->createCommand()
						->select('webinar_session_user.id_user')
						->from('webinar_session_user')
						->join('webinar_session',
							'webinar_session.id_session = webinar_session_user.id_session')
						->where('webinar_session_user.status = :waitingStatus',
							['waitingStatus' => WebinarSessionUser::$SESSION_USER_WAITING_LIST])
						->andWhere('webinar_session.id_session = :idSession',
							['idSession' => $sessionInstance->id_session])
						->order('date_subscribed ASC')
						->limit($usersToEnroll)
						->queryAll();

					foreach ($listOfWaitingToEnroll as $oneUserRecord) {
						/* @var $model WebinarSessionUser */
						$model = WebinarSessionUser::model()->findByAttributes(array(
							'id_user' => $oneUserRecord['id_user'],
							'id_session' => $sessionInstance->id_session
						));

						if ($model) {
							$model->status = WebinarSessionUser::$SESSION_USER_SUBSCRIBED;
							if ($model->hasAttribute('waiting')) {
								$model->waiting = 0;
							} // NOTE: "waiting" column is deprecated for "webinar_session_user" table. Use "status" instead.
							$status = $model->save(false);

							if ($status) {
								$event = new DEvent(new stdClass(), array(
									'enrollment' => $model,
									'session_id' => $sessionInstance->id_session,
									'user' => $oneUserRecord['id_user']
								));
								Yii::app()->event->raise(EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED,
									$event);
							}
							// also change the status of the corresponding _courseuser row
							$cuModel = LearningCourseuser::model()->findByAttributes(array(
								'idUser' => $oneUserRecord['id_user'],
								'idCourse' => $this->idCourse
							));

							if ($cuModel) {
								$cuModel->status = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
								$cuModel->waiting = 0;  // In case the subscription is moderated
								$cuModel->save();
							}
						}
					}
					break;
			}
		}
	}

	/**
	 * Count users completed THIS course within first N days since their enrollment. Filter by Power User, if requested.
	 *
	 * @param number $days
	 * @param bool|string $checkPowerUser Should we filter by power user?
	 * @param bool|false|number $idPowerUser ID of the Power User to filter by, if previous parameter is true. FALSE=check current user
	 * @return number
	 */
	public function countUsersCompletedWithinNDays($days, $checkPowerUser = false, $idPowerUser = false)
	{

		// Should we filter by power user and which power user (current one or arbitrary one)?
		if ($checkPowerUser) {
			if (!$idPowerUser && Yii::app()->user->getIsAdmin()) {
				$idPowerUser = Yii::app()->user->id;
			}
		}

		$criteria = new CDbCriteria();

		// Join power user assignment table to filter users and get only PU's ones
		if ((int)$idPowerUser > 0) {
			$criteria->join = "INNER JOIN core_user_pu cup on ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )";
			$criteria->params[':idPowerUser'] = $idPowerUser;
			$criteria->together = true;
		}

		$criteria->addCondition('idCourse=:idCourse');
		$criteria->addCondition('status=:status');
		$criteria->addCondition('date_complete <= DATE_ADD(date_inscr, INTERVAL :days DAY)');

		$criteria->params[':idCourse'] = $this->idCourse;
		$criteria->params[':status'] = LearningCourseuser::$COURSE_USER_END;
		$criteria->params[':days'] = (int)$days;

		$count = LearningCourseuser::model()->count($criteria);
		return $count;
	}


	/**
	 * Returns if the course is overbooked (more subscription then the available seat and “allow overbooking” is enabled)
	 * @return boolean - true if course is overbooked
	 */
	public function isOverbooked()
	{
		// Checking if allow_overbookin options is enabled
		if ($this->allow_overbooking == 1 && ($this->can_subscribe == 1 || $this->can_subscribe == 2)) {
			// Is setted a restriction about the number of user that can be subscribed to the course?
			if ($this->max_num_subscribe > 0) {
				$subscribedUsers = $this->getTotalSubscribedUsers(false, false, false);
				if ($subscribedUsers >= $this->max_num_subscribe) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	/**
	 * @return array
	 */
	public function getCoursePolicy()
	{
		$label = Yii::t('standard', '_SUBSCRIBE');
		$canSubscribe = true;
		$canSelfSubscribe = true;
		$showButton = false;
		$onlyAdmin = false;

		// Enrollement Policy
		switch ($this->subscribe_method) {
			case self::SUBSMETHOD_ADMIN:
				$label = Yii::t('catalogue', '_COURSE_S_GODADMIN');
				$onlyAdmin = true;
				$canSelfSubscribe = false;
				break;
			case self::SUBSMETHOD_MODERATED:
				$label = Yii::t('user_managment', '_REGISTER_TYPE_MODERATE');
				$showButton = true;
				break;
			case self::SUBSMETHOD_FREE:
				$label = Yii::t('standard', '_SUBSCRIBE');
				$showButton = true;
				break;
		}

		/*
		 *  We check the subscribtion policy
		 *  only if the enrollement method is not admin.
		 */
		if (!$onlyAdmin) {
			switch ($this->can_subscribe) {
				case self::COURSE_SUBSCRIPTION_CLOSED:
					$label = Yii::t('course', '_SUBSCRIPTION_CLOSED');
					$showButton = false;
					$canSubscribe = false;
					break;

				case self::COURSE_SUBSCRIPTION_BETWEEN_DATES:
					if (!$this->checkSubscribtionDates()) {
						$label = Yii::t('course', '_SUBSCRIPTION_CLOSED');
						$showButton = false;
						$canSubscribe = false;
					}
					break;

				case self::COURSE_SUBSCRIPTION_OPEN:
					$showButton = true;
					break;

			}

			// Checking Special Options
			// if max_subscribtion is setted and overbooking disabled
			if ($this->max_num_subscribe > 0 && $this->allow_overbooking == 0) {
				$numEnrolled = $this->getTotalSubscribedUsers(false, false, false);
				if ($numEnrolled >= $this->max_num_subscribe) {
					$label = Yii::t('course', '_MAX_NUM_SUBSCRIBE');
					$showButton = false;
					$canSubscribe = false;
				}

			}
		}

		if ($this->status == LearningCourse::$COURSE_STATUS_PREPARATION) {
			$label = Yii::t('course', '_SUBSCRIPTION_CLOSED');
			$showButton = false;
			$canSubscribe = false;
		}

		return array(
			'label' => $label,
			'showButton' => $showButton,
			'canSubscribe' => $canSubscribe,  // can user subscribe at all?
			'canSelfSubscribe' => $canSelfSubscribe, // .. and can he/she also initiate a subscription process?
		);
	}


	/**
	 * Resolve course access/policy etc. for My Dashboard: Catalog and My Courses dashlets
	 *
	 * Return an array of data for the presentation layer in the course tile
	 *
	 * @param integer $idUser
	 *
	 * @return array
	 */
	public function resolveCourseAccess($idUser = false)
	{

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		$canPlay = false;
		$canBuy = false;
		$canEnroll = false;

		// Dedicated page ???
		if (Settings::get('show_course_details_dedicated_page') === 'on') {
			$openDialogCss = "";
			$courseDetailsUrl = Docebo::createLmsUrl('course/details', array('id' => $this->idCourse));
		} else {
			$openDialogCss = "open-dialog";
			$courseDetailsUrl = Docebo::createLmsUrl('course/axDetails', array('id' => $this->idCourse));
		}

		$playUrl = Docebo::createLmsUrl("/player", array('course_id' => $this->idCourse));

		$tags = array();

		$playLinkTag = "<a href='$playUrl'>";
		$buyLinkTag = "<a href='$courseDetailsUrl' data-dialog-class='course-details-modal' rel='dialog-course-detail-$this->idCourse' class='$openDialogCss'>";
		$enrollLinkTag = "<a href='$courseDetailsUrl' data-dialog-class='course-details-modal' rel='dialog-course-detail-$this->idCourse' class='$openDialogCss'>";

		$linkTag = false;

		// Get the enrollment of the user, if any
		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $idUser,
			'idCourse' => $this->idCourse
		));

		// If user is enrolled...
		if ($enrollment) {
			if ($enrollment->status == LearningCourseuser::$COURSE_USER_END) {
				$isCompleted = true;
				$action = array(
					'label' => Yii::t('standard', '_COMPLETED'),
					'icon' => "fa fa-check-circle-o",
					'class' => "completed",
				);
			} else {
				if ($enrollment->status == LearningCourseuser::$COURSE_USER_SUBSCRIBED) {
					$isNew = true;
					$tags[] = array(
						'label' => strtoupper(Yii::t('standard', '_NEW')),
						'class' => 'tag-new',
					);
				}
			}

			// Check if user can enter the course
			$canEnterCourse = $enrollment->canEnterCourse();

			if ($canEnterCourse['can']) {
				$canPlay = true;
				if (!$isCompleted) {
					$action = array(
						'label' => Yii::t('standard', '_PLAY'),
						'icon' => "fa fa-play-circle-o",
						'class' => "play",
					);
				}
				$linkTag = $playLinkTag;
			} // If, not, state the reason as an action label, or tag
			else {

				switch ($canEnterCourse['reason']) {
					case 'prerequisites':
						$actionClass = "locked";
						$actionIconClass = "fa fa-lock";
						$actionLabel = Yii::t('storage', '_LOCKED');
						break;
					case 'waiting':
						$actionClass = "waiting";
						$actionIconClass = "fa fa-clock-o";
						$actionLabel = Yii::t('standard', '_WAITING');
						break;
					case 'user_status':
					case 'course_status':
					case 'subscription_expired':
					case 'subscription_not_started':
					case 'course_date':
					default:
						$actionClass = "locked";
						$actionIconClass = "fa fa-lock";
						$actionLabel = Yii::t('storage', '_LOCKED');
						break;
				}

				$action = array(
					'label' => $actionLabel,
					'icon' => $actionIconClass,
					'class' => $actionClass,
				);

			}
		} // Else, if the course is PAID
		else {
			if ($this->selling == 1) {
				$canBuy = true;
				$action = array(
					'label' => Yii::t('order', 'Buy'),
					'icon' => "fa fa-shopping-cart",
					'class' => "enroll buy",
				);
				$linkTag = $buyLinkTag;
			} // Otherwise, looks like the course is FREE
			else {
				$canEnroll = true;
				$action = array(
					'label' => Yii::t('standard', 'Enroll'),
					'icon' => "fa fa-plus",
					'class' => "enroll",
				);
				$linkTag = $enrollLinkTag;

			}
		}


		// On top of everything above (related to NON-subscribed courses), apply course enrollment policy
		if (!$enrollment) {
			switch ($this->subscribe_method) {

				// Admin only: reset everything, this is high priortiy setting
				case self::SUBSMETHOD_ADMIN:
					$onlyAdmin = true;
					$canEnroll = false;
					$canBuy = false;
					$linkTag = false;
					$action = array(
						'label' => Yii::t('storage', '_LOCKED'),
						'icon' => "fa fa-lock",
						'class' => "locked",
					);
					$tags = array(
						array(
							'label' => Yii::t('dashboard', 'Admin only'),
							'class' => 'tag-adminonly',
							'tooltip' => Yii::t('dashboard', 'Only administrator can enroll users to this course'),
						)
					);
					break;

				// Moderated enrollments: just add a tag
				case self::SUBSMETHOD_MODERATED:
					$tags = array(
						array(
							'label' => Yii::t('dashboard', 'Moderated'),
							'class' => 'tag-moderated',
							'tooltip' => Yii::t('dashboard',
								'You can apply to enroll, but it requires administrator approval'),
						)
					);
					break;

			}

			// If it is NOT admin only, check these policies as well
			if (!$onlyAdmin) {
				switch ($this->can_subscribe) {
					case self::COURSE_SUBSCRIPTION_CLOSED:
						$canEnroll = false;
						$canBuy = false;
						$linkTag = false;

						$action = array(
							'label' => Yii::t('storage', '_LOCKED'),
							'icon' => "fa fa-lock",
							'class' => "locked",
						);
						$tags = array(
							array(
								'label' => Yii::t('standard', 'Closed'),
								'class' => 'tag-closed',
								'tooltip' => Yii::t('course', '_SUBSCRIPTION_CLOSED')
							)
						);
						break;

					case self::COURSE_SUBSCRIPTION_BETWEEN_DATES:
						if (!$this->checkSubscribtionDates()) {
							$canEnroll = false;
							$canBuy = false;
							$linkTag = false;
							$action = array(
								'label' => Yii::t('storage', '_LOCKED'),
								'icon' => "fa fa-lock",
								'class' => "locked",
							);
							$tags = array(
								array(
									'label' => Yii::t('standard', 'Closed'),
									'class' => 'tag-closed',
									'tooltip' => Yii::t('course', '_SUBSCRIPTION_CLOSED')
								)
							);
						}
						break;
				}
			}
		}

		if ($this->max_num_subscribe > 0 && !$enrollment && $this->course_type == LearningCourse::TYPE_ELEARNING) {
			$numEnrolled = $this->getTotalSubscribedUsers(false, false, false);
			if ($numEnrolled >= $this->max_num_subscribe) {
				if ($this->allow_overbooking) {
					if ($canBuy || $canEnroll) {
						$action['class'] = 'waiting';

						$tags = array(
							array(
								'label' => Yii::t('course', 'OVERBOOK'),
								'class' => 'tag-closed',
								'tooltip' => Yii::t('course', 'OVERBOOK')
							)
						);
					}
				} else {
					$label = Yii::t('course', '_MAX_NUM_SUBSCRIBE');
					$canBuy = false;
					$canEnroll = false;
					$action = array(
						'label' => Yii::t('storage', '_LOCKED'),
						'icon' => "fa fa-lock",
						'class' => "locked",
					);
					$tags = array(
						array(
							'label' => Yii::t('course', '_MAX_NUM_SUBSCRIBE'),
							'class' => 'tag-closed',
							'tooltip' => Yii::t('course', '_MAX_NUM_SUBSCRIBE')
						)
					);
				}
			}
		}

		$result = array(
			'tags' => $tags,
			'action' => $action,
			'linkTag' => $linkTag,
			'canBuy' => $canBuy,
			'canPlay' => $canPlay,
			'canEnroll' => $canEnroll,
			'enrollment' => $enrollment, // LearningCourseUser model
		);


		Yii::app()->event->raise('BeforeCourseHoverDetailsRender',
			new DEvent($this, array('course_id' => $this->idCourse, 'course_tile' => &$result)));

		return $result;

	}


	/**
	 * @return bool
	 */
	public function mpSeatsUnlimited()
	{
		return ($this->abs_max_subscriptions == 0);
	}


	/**
	 * Checks if the course is between these two dates (sub_start_date and sub_end_date) to understand
	 * if The course is expired or is in the future.
	 * @return bool (true if it's between two dates, otherwise false).
	 */
	public function checkSubscribtionDates()
	{
		$now = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
		$subStartDate = strtotime(Yii::app()->localtime->fromLocalDateTime($this->sub_start_date));
		$subEndDate = strtotime(Yii::app()->localtime->fromLocalDateTime($this->sub_end_date));

		if (
			($now >= $subStartDate && $now <= $subEndDate) ||
			($now >= $subStartDate && $subEndDate == 0) ||
			($now <= $subEndDate && $subStartDate = 0) ||
			($subStartDate == 0 && $subEndDate == 0) ||
			($subStartDate == null && $subEndDate == null)
		) {
			return true;
		}

		return false;
	}


	/**
	 * Determine the course demo media category by the filename extension
	 */
	public function getDemoMediaCategory()
	{

		$demoFileName = trim($this->course_demo);

		if (empty($demoFileName)) {
			return false;
		}

		$mimeType = CFileHelper::getMimeTypeByExtension($demoFileName);

		if (!$mimeType) {
			// yii/utils/mimeTypes.php is not meant to be a full list of
			// extension=>mimetype pairs. There are some extensions
			// (e.g. mp4) that are missing there so we have to check for them
			// manually and add their mimetype until the core Yii team decides
			// to implement them out of the box

			if (($ext = pathinfo($demoFileName, PATHINFO_EXTENSION)) !== '') {
				$ext = strtolower($ext);

				switch ($ext) {
					case 'mp4':
						$mimeType = 'video/mp4';
						break;
				}
			}
		}

		$tmp = preg_split("/\//", $mimeType);
		if ($tmp[0] === 'video') {
			$result = LearningCourse::$DEMO_MEDIA_CATEGORY_VIDEO;
		} else {
			if ($tmp[1] === 'x-shockwave-flash') {
				$result = LearningCourse::$DEMO_MEDIA_CATEGORY_SWF;
			} else {
				$result = LearningCourse::$DEMO_MEDIA_CATEGORY_UNKNOWN;
			}
		}

		return $result;
	}


	/**
	 * Returns an array containing the number of completed/total objects for this course
	 * @param null $idUser
	 * @param bool $include_hidden
	 * @return array
	 */
	public function getObjectsProgress($idUser = null, $include_hidden = true)
	{
		$progress = array(
			'total' => 0
		);
		$c = new CDbCriteria();
		$c->addCondition('idCourse=:idCourse');
		$c->params[':idCourse'] = $this->idCourse;
		if (!$include_hidden) {
			// Only include "visible" LOs
			$c->addCondition('visible = 1');
		}

		// Don't include folders in the object list
		$c->addCondition("objectType <> ''");

		$progress['total'] = LearningOrganization::model()->count($c);
		if ($idUser) {
			$c = new CDbCriteria();
			$c->addCondition('t.idUser=:idUser');
			$c->params[':idUser'] = (int)$idUser;
			$c->addInCondition('t.status', array('completed', 'passed'));
			if (!$include_hidden) {
				// Only include "visible" LOs
				$c->addCondition('org.visible = 1');
			}
			$progress['completed'] = (int)LearningCommontrack::model()->with(array(
				'organization' => array(
					'alias' => 'org',
					'select' => false,
					'joinType' => 'INNER JOIN',
					'condition' => 'org.idCourse=' . $this->idCourse
				)
			))->count($c);
		}
		return $progress;
	}


	/**
	 * Returns a string representing the fields to sort courses by
	 * @static
	 * @param string $prefix Optional, the alias of the table
	 * @param string $default Mandatory, default fields to sort by (comma separated)
	 * @return string Example output: course.name,course.code,course.status
	 */
	public static function getOrderBy($prefix = '', $default)
	{
		$orderBy = array();

		$courseOrderFields = Settings::get('tablist_mycourses', '');
		if ($courseOrderFields != '') {
			$courseOrderFields = explode(',', $courseOrderFields);
			foreach ($courseOrderFields as $field) {
				if (trim($field)) {
					$orderBy[] = (($prefix != '') ? $prefix . '.' : '') . $field;
				}
			}
		} else {
			$defaultOrderFields = explode(',', $default);
			foreach ($defaultOrderFields as $field) {
				if (trim($field)) {
					$orderBy[] = (($prefix != '') ? $prefix . '.' : '') . $field;
				}
			}
		}

		return implode(',', $orderBy);
	}


	/**
	 * This function creates a list of courses having show_rules = Only users subscribed to the course
	 * but the user is not subscribed to those courses.
	 * @param $subscribedCoursesList - an array with the list of the courses in which the user is subscribed
	 * @return array a list of idCourse that should not be displayed
	 */
	public function getCoursesToNotDisplay($subscribedCoursesList = array())
	{

		return Yii::app()->getDb()->createCommand()
			->select('idCourse')
			->from(self::tableName())
			->where('show_rules=:onlySubscribed', array(':onlySubscribed' => self::DISPLAY_ONLY_SUBSCRIBED_USERS))
			->andWhere(array('NOT IN', 'idCourse', $subscribedCoursesList))
			->queryColumn();
	}

	/**
	 * Get IDs of courses to which a given user is subscribed (any level)
	 *
	 * @param $userId
	 */
	static public function getCourseIdsByUser($userId)
	{
		return Yii::app()->db->createCommand()
			->selectDistinct('c.idCourse')
			->from(LearningCourseuser::model()->tableName() . ' cu')
			->join(LearningCourse::model()->tableName() . ' c', 'c.idCourse=cu.idCourse')
			->where('cu.idUser=:idUser', array(':idUser' => $userId))
			->queryColumn();
	}


	/**
	 * Return if course exists
	 * @param int $idCourse
	 * @return boolean
	 */
	public static function isExistent($idCourse)
	{
		$model = self::model()->findByPk($idCourse);
		return $model ? true : false;
	}


	/**
	 * All seats for this course
	 * @return int
	 */
	public function getAllSeats()
	{
		return $this->abs_max_subscriptions;
	}


	/**
	 * Number of already taken seats
	 * @return int
	 */
	public function getTakenSeats()
	{
		return LearningLogMarketplaceCourseLogin::takenSeatsByCourse($this->idCourse);
	}


	/**
	 * If course is a Marpetplace course, return available seats; Otherwise, return -1 (no info)
	 */
	public function getAvailableSeats()
	{
		if (!($this->mpidCourse > 0)) {
			return -1;
		}
		$availableSeats = $this->getAllSeats() - $this->getTakenSeats();
		return $availableSeats;
	}


	/**
	 * Get enrollment statistics about THIS course
	 *
	 * @return stdClass
	 */
	public function getCourseStats()
	{
		$stats = new stdClass(); //will contain all statistic properties

		$stats->to_begin = 0;
		$stats->in_progress = 0;
		$stats->completed = 0;
		$stats->completed_percentage = 0.00;
		$stats->completed_over_month = 0;
		$stats->completed_over_month_percentage = 0.00;
		$stats->resources = count($this->learningOrgResources);

		$utcCreateDate = Yii::app()->localtime->fromLocalDateTime($this->create_date);
		$utcNow = Yii::app()->localtime->UTCNow;
		$stats->days_since_lunch = Yii::app()->localtime->countDayDiff($utcNow, $utcCreateDate);


		// Enrollment STATUS (like number of enrolled users, number of completed, etc.)
		// ------------------------------------------------------------------------------
		$command = Yii::app()->db->createCommand();
		$command
			->select('enrollment.status as status_code, count(enrollment.status) as status_count')
			->from('learning_courseuser enrollment')
			->join('core_user user', 'user.idst=enrollment.idUser')// only exisitng users
			->where('enrollment.idCourse=:idCourse')
			->group('enrollment.status');

		// Filter OUT users NOT assigned to current user, if it is a Power User
		$pUserRights = Yii::app()->user->checkPURights($this->idCourse);
		if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$command->join(CoreUserPU::model()->tableName() . " pu",
				"pu.puser_id = " . (int)Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$command->join(CoreUserPuCourse::model()->tableName() . " puc",
				"puc.puser_id = " . (int)Yii::app()->user->id . " AND puc.course_id = :idCourse");
		}

		//PU instructor OR user instructor
		if (!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $this->idCourse,
				LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)
		) {
			$instructorUsers = LearningCourse::getInstructorUsers($this->idCourse);
			if (!empty($instructorUsers)) {
				$command->andWhere('enrollment.idUser IN (' . implode(',', $instructorUsers) . ')');
			}
		}

		$params = array();
		$params[':idCourse'] = $this->idCourse;
		$command->params = $params;
		$rows = $command->queryAll();

		// Build stats
		$enrollmentCount = 0;
		if (!empty($rows)) {
			foreach ($rows as $row) {
				$statusCount = $row['status_count'];
				switch ($row['status_code']) {
					case self::$COURSE_STATUS_PREPARATION:
						$stats->to_begin = $statusCount;
						break;
					case self::$COURSE_STATUS_AVAILABLE:
						$stats->in_progress = $statusCount;
						break;
					case self::$COURSE_STATUS_EFFECTIVE:
						$stats->completed = $statusCount;
						break;
					default:
						// We do NOT count unknown statuses!!
						// $statusCount = 0;
						break;
				}
				$enrollmentCount += $statusCount;
			}
		}

		$stats->enrolled = $enrollmentCount;
		if ($stats->enrolled) {
			$stats->completed_percentage = (float)($stats->completed * 100) / $stats->enrolled;
		}


		// Completed in MORE than 30 days: get number of users completed the course in MORE than XX days
		// -----------------------------------------------------------------------------------------------
		$days = 30;
		$command = Yii::app()->db->createCommand();
		$command
			->select('count(*) AS number')
			->from('learning_courseuser enrollment')
			->where('enrollment.idCourse=:idCourse')
			->andWhere('enrollment.date_complete IS NOT NULL')
			->andWhere('DATEDIFF(enrollment.date_complete, enrollment.date_inscr) > :numberOfDays');

		// Filter OUT users NOT assigned to current user, if it is a Power User
		if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$command->join(CoreUserPU::model()->tableName() . " pu",
				"pu.puser_id = " . (int)Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$command->join(CoreUserPuCourse::model()->tableName() . " puc",
				"puc.puser_id = " . (int)Yii::app()->user->id . " AND puc.course_id = :idCourse");
		}
		$params = array();
		$params[':idCourse'] = $this->idCourse;
		$params[':numberOfDays'] = $days;
		$command->params = $params;
		$rows = $command->queryAll();
		$stats->completed_over_month = $rows['number'];


		// -- Return result

		return $stats;

	}


	/**
	 * Generate an unique auto registration code
	 * @return string
	 */
	public function generateAutoRegCode()
	{
		do {
			$code = strtoupper(substr(sha1(md5(time())), 0, 10));
			$exists = $this->findByAttributes(array(
				'autoregistration_code' => $code
			));
		} while ($exists != null);
		return $code;
	}


	/**
	 * Get upload path to course
	 * @return string
	 */
	public function getUploadPath()
	{
		$path = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . 'doceboLms' . DIRECTORY_SEPARATOR . Settings::get('pathcourse');
		return $path;
	}


	/**
	 *
	 */
	public function removeCourseFile($file)
	{
		if (file_exists($file)) {
			unlink($file);
		}
	}


	/**
	 * Manage couse logo file
	 */
	public function manageLogo()
	{
		//load old (current) logo model
		$oldModel = $this->findByPk($this->primaryKey);
		$path = $this->getUploadPath();

		$this->img_course = CUploadedFile::getInstance($this, 'img_course');
		if ($this->img_course) {
			//remove the old file if exists
			if ($oldModel->img_course) {
				$oldModel->removeCourseFile($path . $oldModel->img_course);
			}
			//save new file
			$name = sha1(rand(0, 1000) . time()) . "." . $this->img_course->extensionName;
			$this->img_course->saveAs($path . $name);
			$this->img_course = $name; //adding the custom name after saveAs() method
		} else {
			$this->img_course = $oldModel->img_course;
		}
	}

	/**
	 * Manage course demo file
	 */
	public function manageCourseDemo()
	{

		// Get the course model that is still "current"
		$oldModel = $this->findByPk($this->primaryKey);

		$this->course_demo = CUploadedFile::getInstance($this, 'course_demo');

		if ($this->course_demo) {

			// Get Storage manager
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);

			// Remove the old file if exists
			if ($oldModel->course_demo) {
				$storage->remove($oldModel->course_demo);
			}

			// Phisically save uploaded file into storage
			$newFilename = Docebo::randomHash() . "." . $this->course_demo->extensionName;
			$storage->storeAs($this->course_demo->tempName, $newFilename);
			$this->course_demo = $newFilename;

		} else {
			$this->course_demo = $oldModel->course_demo;
		}
	}


	public function renderPlayerBg($variant = CoreAsset::VARIANT_ORIGINAL, $skipPluginOverriding = false)
	{
		$imageInfo = $this->getPlayerBgImage($variant, $skipPluginOverriding);
		return CHtml::image($imageInfo['src']);
	}


	/**
	 * @return array
	 */
	public function getPlayerBgImage($variant = CoreAsset::VARIANT_ORIGINAL, $skipPluginOverriding = false)
	{
		// need to return the image url and display mode
		$src = null;
		$playerBgAspect = null;

		if (!empty($this->player_bg)) {
			$src = CoreAsset::url($this->player_bg, $variant);
			$playerBgAspect = $this->player_bg_aspect;
		}

		// admin default
		if (empty($src)) {

			$customPlayerBg = array('src' => null, 'aspect' => null);

			if (!$skipPluginOverriding) {
				// Raise event to let plugins override the default login image
				Yii::app()->event->raise('GetPlayerBgImage',
					new DEvent($this, array('player_bg_image' => &$customPlayerBg)));
			}

			if (!$customPlayerBg['src'] || !$customPlayerBg['aspect']) {
				// get default bg
				$defaultBg = Settings::get('player_bg', false);

				if (!empty($defaultBg)) {
					$src = CoreAsset::url($defaultBg, $variant);
					$playerBgAspect = Settings::get('player_bg_aspect', 'fill');
				}

			} else {
				$src = $customPlayerBg['src'];
				$playerBgAspect = $customPlayerBg['aspect'];
			}
		}

		// system default
		if (empty($src)) {
			$src = Yii::app()->theme->getBaseUrl() . '/images/course_player/' . Yii::app()->params['defaultCoursePlayerBgFileName'];
			$playerBgAspect = empty($this->player_bg_aspect) ? 'fill' : $this->player_bg_aspect;
		}

		// Fix, if any
		$playerBgAspect = (empty($playerBgAspect) || !in_array($playerBgAspect,
				array('fill', 'tile'))) ? 'fill' : $playerBgAspect;

		$playerBgImage = array(
			'src' => $src,
			'aspect' => $playerBgAspect
		);

		return $playerBgImage;
	}


	public function saveCourse($raiseException = false)
	{
		// begin the transaction
		if (Yii::app()->db->getCurrentTransaction() === null) {
			$transaction = Yii::app()->db->beginTransaction();
		}
		try {
			if ($this->save()) {

				if (Yii::app()->user->isAdmin /* && $this->scenario == 'update'*/) {
					//if a power user is creating a new course, the course must be assigned to himself
					$adminCourse = new CoreAdminCourse();
					$adminCourse->idst_user = Yii::App()->user->id;
					$adminCourse->type_of_entry = CoreAdminCourse::TYPE_COURSE;
					$adminCourse->id_entry = $this->getPrimaryKey();
					if (!$adminCourse->save()) {
						throw new CException('Error while updating power user course data');
					}

					$existingPUassignment = CoreUserPuCourse::model()->findByPk(array(
						'puser_id' => Yii::app()->user->id,
						'course_id' => $this->getPrimaryKey()
					)); //some events may pre-set this entry, so we have to check for it in order to avoid SQL errors
					if (empty($existingPUassignment)) {
						$puCourse = new CoreUserPuCourse();
						$puCourse->puser_id = Yii::app()->user->id;
						$puCourse->course_id = $this->getPrimaryKey();
						if (!$puCourse->save()) {
							throw new CException('Error while updating power user course data');
						}
						//TO DO: check for any possible cached id list to update for current user.
					}
				}

				if (PluginManager::isPluginActive('PowerUserApp')) {
					Yii::app()->scheduler->createImmediateJob('update_power_user_selection', PowerUserJobHandler::id(),
						array('courses' => array((int)$this->getPrimaryKey())));
				}
			} else {
				throw new CException('Error while creating course');
			}

			Yii::app()->event->raise(EventManager::EVENT_NEW_COURSE_ADDED, new DEvent($this, array(
				'course' => $this
			)));

			// save all the operations
			if ($transaction) {
				$transaction->commit();
			}

			return true;
		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			if ($transaction) {
				$transaction->rollback();
			}
			if ($raiseException) {
				throw $e;
			}
		}
		return false;
	}


	/**
	 * Process an array of users (uploaded from CSV or otherwise built) and enroll them to THIS course.
	 *
	 * Users are loaded from CSV with NO HEADER!!!
	 *
	 * @param array $users Array of users:  array( <index> => array('0'=>'user_name', '1' => <level>)) !!!!!!!!!!!!!!!!!!!!!
	 * @param array $params Key value array to pass params as needed
	 * @return array Information array
	 */
	public function processEnrollmentCSVUpload($users, $params = array())
	{

		$result = array(
			'enrolled' => 0,
			'skipped' => 0,
			'notFound' => 0,
		);

		foreach ($users as $user) {

			// Skip emtpy lines
			if (!isset($user[0])) {
				continue;
			}

			$criteria = new CDbCriteria();
			$criteria->addCondition('userid = :userid');

			// Note:
			// Index 0 is the username from CSV
			// Index 1 is the course subscription level of the user; if not set, it will be set to 3 (Learner/Student)
			$criteria->params[':userid'] = Yii::app()->user->getAbsoluteUsername(trim(trim($user[0]), '"'));

			// If current user is a power user, join core_user_pu table to filter OUT unassigned users
			if (Yii::app()->user->getIsPu()) {
				$criteria->with['onlyAssignedUsers'] = array(
					'condition' => 'onlyAssignedUsers.puser_id=' . (int)Yii::app()->user->id,
				);
			}

			// We need ONE model only
			$userProfile = CoreUser::model()->find($criteria);

			if (empty($userProfile)) {
				$result['notFound']++;
			} else {
				$learningCourseUser = new LearningCourseuser();
				$level = isset($user[1]) ? (int)trim($user[1]) : LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
				// there are cases when we want to restrict the user level when enrolling
				// eg: power users have this option
				if (!empty($params['force_level'])) {
					$level = $params['force_level'];
				}

				// Is there any learning_courseuser's deadlines to be set
				$date_begin_validity = !empty($params['date_begin_validity']) ? $params['date_begin_validity'] : false;
				$date_expire_validity = !empty($params['date_expire_validity']) ? $params['date_expire_validity'] : false;
				$enrollment_fields_values = !empty($params['enrollment_fields_values']) ? $params['enrollment_fields_values'] : false;


				$subscribeResult = $learningCourseUser->subscribeUser($userProfile->idst, Yii::app()->user->id,
					$this->idCourse, 0, $level, false, $date_begin_validity, $date_expire_validity);
				if ($subscribeResult) {
					$result['enrolled']++;

					if (LearningCourse::isCourseSelling($this->idCourse) && CoreUserPU::isPUAndSeatManager()) {
						Yii::app()->event->raise('onPuSeatConsume', new DEvent($this, array(
							'idUser' => $userProfile->idst,
							'idCourse' => $this->idCourse,
							'idPowerUser' => Yii::app()->user->id,
						)));
					}
				} else {
					if ($learningCourseUser->errorMessage == LearningCourseuser::$ALREADY_SUBSCRIBED) {
						$result['skipped']++;
					}
				}
			}
		}

		return $result;
	}


	function getCourseTotalDays()
	{
		$utcCreateDate = Yii::app()->localtime->fromLocalDateTime($this->create_date);
		$utcNow = Yii::app()->localtime->UTCNow;
		$diff = Yii::app()->localtime->countDayDiff($utcNow, $utcCreateDate);
		return $diff;
	}


	/**
	 * Return array of course models having given auto registration code.
	 *
	 * array(
	 *        'all'                => @array  <i>all courses with code</i>,
	 *        'subscribed'        => @array <i>set if there are subscribed courses with the code</i>
	 *        'notsubscribed'    => @array <i>set if there are NOT subscribed courses with the code</i>
	 * )
	 *
	 * @param string $code
	 * @param bool|number $idUser
	 *
	 * @return array
	 */
	public static function getCoursesHavingAutoregCode($code, $idUser = false)
	{

		$criteria = new CDbCriteria();
		$criteria->addCondition("upper(autoregistration_code)=upper(:code) AND :code != ''");
		$criteria->params[':code'] = $code;

		$result = false;

		$courses = self::model()->findAll($criteria);

		if (is_array($courses) && (count($courses) > 0)) {
			$result = array();

			// Return an array of all found courses in any case
			$result['all'] = $courses;

			// If idUser is NOT false, we add more information: array of subscribed and not subscribed courses
			if (($idUser !== false) && ((int)$idUser > 0)) {
				$subscribed = array();
				$notsubscribed = array();
				foreach ($courses as $course) {
					if (LearningCourseuser::isSubscribed($idUser, $course->idCourse)) {
						$subscribed[] = $course;
					} else {
						$notsubscribed[] = $course;
					}
				}

				// Any already subscribed course?
				if (count($subscribed) > 0) {
					$result['subscribed'] = $subscribed;
				}

				// Any NON-subscribed course ?
				if (count($notsubscribed) > 0) {
					$result['notsubscribed'] = $notsubscribed;
				}

			}

		}

		return $result;

	}


	public function getStatusLabel()
	{
		$statuses = self::statusesList();
		return isset($statuses[$this->status]) ? $statuses[$this->status] : null;
	}


	public static function getStatusLabelTrsnaslated($status)
	{
		$statuses = self::statusesList();
		return isset($statuses[$status]) ? $statuses[$status] : $status;
	}


	/**
	 * Checks if given user can subscribe to this course.
	 * This should a bit complex check related to overbooking, user permisisons, e-commerce, editions, dates and so on.
	 *
	 * @param number $idUSer
	 * @return bool
	 */
	public function canUserSubscribe($idUSer)
	{

		return true;

	}


	/**
	 * Batch copy of all LO resources of THIS course to another one, identified by ID.
	 * The target course must already exists.
	 *
	 * @param number $course_id id of the target course
	 * @param bool|number $folder_id idOrg of a learning_organization record of type "folder" (objectType=""): if set, LOs will be copied there, otherwise in course root node
	 * @throws CException
	 * @throws CException
	 * @throws Exception
	 * @return boolean
	 */
	public function copyLearningObjectsTo($course_id, $folder_id = false)
	{

		$source = $this;
		$target = self::model()->findByPk($course_id);

		// Target course MUST already exist
		if (!$target) {
			throw new CException('Target course does not exist');
		}

		Yii::log("Copy LO from $this->idCourse to $target->idCourse", CLogger::LEVEL_INFO);

		// Ok, copy all LOs from $source to $target. Note, $target MAY have LOs already, so: ADD $source LOs

		$rootSource = LearningOrganization::model()->getRoot($this->getPrimaryKey());
		$rootTarget = LearningOrganization::model()->getRoot($course_id);

		//direct SQL copy of LOs will be performed.
		//NOTE: Future upgrades will use a better method, being this process very "dirty" but easier to implement.
		//TO DO: do a better implementation, using models and not direct sql
		$criteria = new CDbCriteria();
		$criteria->addCondition("idParent <> 0"); //exclude root node
		$criteria->order = "iLeft ASC"; //tree ordering
		$toBeCopied = LearningOrganization::model()->findAllByAttributes(array('idCourse' => $this->getPrimaryKey()),
			$criteria);
		if ($toBeCopied) {
			$db = Yii::app()->db;
			if ($db->getCurrentTransaction() === null) {
				$transaction = $db->beginTransaction();
			}
			try {
				$maxRight = 1;
				$mirror = array();
				$listOfNewLoIds = array();
				$copyParentList = array();
				foreach ($toBeCopied as $record) {
					//now copy LO specific data, this depends on LO type and LO specific models
					$newLOInfo = false;
					$loModel = LearningOrganization::model()->objectTypeModel($record->objectType);
					$version = LearningRepositoryObjectVersion::model()->findByAttributes(array(
						'id_resource' => $record->idResource,
						'id_object' => $record->id_object,
						'object_type' => $record->objectType
					));

					if ($loModel !== false && !$version) {
						switch ($record->objectType) {
							case LearningOrganization::OBJECT_TYPE_TINCAN:
								// For TinCans we store the main activity's ID (reference
								// to LearningTcActivity) in learning_organization.idResource,
								// but we need the package model (LearningTcAp) instead
								$c = new CDbCriteria();
								$c->compare('learningTcActivities.id_tc_activity', $record->idResource);
								$loInfo = $loModel::model()->with('learningTcActivities')->find($c);
								break;
							default:
								$loInfo = $loModel::model()->findByPk($record->idResource);
						}

						if (method_exists($loInfo, 'copy')) {
							$newLOInfo = $loInfo->copy(false);
						}
					}

					//copy organization info
					$values = array();
					$fields = array();
					foreach ($record->getAttributes() as $key => $value) {
						if ($key != 'idOrg') {
							switch ($key) {
								case 'idResource': {
									$newValue = $version ? $version->id_resource : (!empty($newLOInfo) ? $newLOInfo->getPrimaryKey() : '0');
								}
									break;
								case 'idCourse': {
									$newValue = $course_id;
								}
									break;
								case 'idAuthor': {
									$newValue = empty(Yii::app()->user->id) ? 0 : Yii::app()->user->id;
								}
									break;
								case 'dateInsert': {
									$newValue = Yii::app()->localtime->getUTCNow();
								}
									break;
								case 'prerequisites': {
									$newValue = '';
								}
									break;
								case 'idParent': {
									if (isset($copyParentList[$value]) && $copyParentList[$value]) {
										$loParent = LearningOrganization::model()->findByPk($copyParentList[$value]);
										if ($loParent && $loParent->isFolder()) {
											$newValue = $loParent->idOrg;
										}
									} else {
										$rootNode = LearningObjectsManager::getRootNode($course_id);
										if ($rootNode && $rootNode->idOrg) {
											$newValue = $rootNode->idOrg;
										} else {
											$newValue = $value;
										}
									}
								}
									break;
								case 'objectType': {
									$newValue = ($version && $version->object_type) ? $version->object_type : $value;
								}
									break;
								case 'title': {
									$newValue = ($version && $version->repositoryLO->title) ? $version->repositoryLO->title : $value;
								}
									break;
								case 'resource': {
									$newValue = ($version && $version->repositoryLO->resource) ? $version->repositoryLO->resource : $value;
								}
									break;
								case 'short_description': {
									$newValue = ($version && $version->repositoryLO->short_description) ? $version->repositoryLO->short_description : $value;
								}
									break;
								case 'id_object': {
									$newValue = ($version && $version->id_object) ? $version->id_object : $value;
								}
									break;
								case 'params_json_inherited': {
									$newValue = $version ? ($version->repositoryLO->params_json ? $version->repositoryLO->params_json : "") : $value;
								}
									break;
								case 'self_prerequisite':
								default: {
									$newValue = $value;
								}
							}
							$values[':' . $key] = $newValue;
							$fields[] = $key;
						}
					}
					$insertQuery = "INSERT INTO `learning_organization` (`" . implode("`,`",
							$fields) . "`) VALUES (" . implode(",", array_keys($values)) . ")";
					$rs = $db->createCommand($insertQuery)->bindValues($values)->execute();
					if ($rs === false) {
						throw new CException('Error while copying learning objects');
					}

					$copyID = $db->lastInsertID;
					if (!empty($newLOInfo) && ($newLOInfo instanceof LearningAiccPackage) && ($record->objectType == LearningOrganization::OBJECT_TYPE_AICC) && !$version) {
						$newLOInfo->idReference = $copyID;
						$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
						$exist = $storage->fileExists($newLOInfo->path);
						if (!$exist) {
							$newLOInfo->scenario = 'copy';
						}
						$newLOInfo->save(false);
					}

					$listOfNewLoIds[] = $copyID;
					$copyParentList[$record->idOrg] = $copyID;

					//other operations
					$mirror[$record->getPrimaryKey()] = $copyID;
					if ($record->iRight > $maxRight) {
						$maxRight = $record->iRight;
					}
				}
				$maxRight++;
				$updateQuery = "UPDATE `learning_organization` SET `iRight` = :max_right WHERE `idOrg` = " . $rootTarget->getPrimaryKey();
				$rs = $db->createCommand($updateQuery)->bindParam(':max_right', $maxRight)->execute();
				if ($rs === false) {
					throw new CException('Error while copying learning objects (target root updating)');
				}
				//post operation: update prerequisites field for copied nodes
				foreach ($toBeCopied as $record) {
					if ($record->prerequisites != '') {
						$list = explode(',', $record->prerequisites);
						if (!empty($list)) {
							$newPrerequisites = array();
							foreach ($list as $prerequisite) {
								if (!empty($prerequisite) && isset($mirror[$prerequisite])) {
									$newPrerequisites[] = $mirror[$prerequisite];
								}
							}
							if (!empty($newPrerequisites)) {
								$param = implode(',', $newPrerequisites);
								$updateQuery = "UPDATE `learning_organization` SET `prerequisites` = :prerequisites WHERE `idOrg` = " . $mirror[$record->getPrimaryKey()];
								$rs = $db->createCommand($updateQuery)->bindParam(':prerequisites', $param)->execute();
								if ($rs === false) {
									throw new CException('Error while copying learning objects (updating prerequisite)');
								}
							}
						}
					}
				}
				if (isset($transaction)) {
					$transaction->commit();
				}

				// Now that we copied all LOs, trigger their afterSave(), in "insert" scenario
				// We do this AFTER COMMIT!!!!!!   ($transaction->commit())
				foreach ($listOfNewLoIds as $idOrg) {
					$tmpModel = LearningOrganization::model()->findByPk($idOrg);
					$tmpModel->scenario = 'insert';
					$tmpModel->afterSave();

					if ($tmpModel->idOrg) {
						$loVersion = LearningRepositoryObjectVersion::model()->findByAttributes(array(
							'id_resource' => $tmpModel->idResource,
							'id_object' => $tmpModel->id_object,
							'object_type' => $tmpModel->objectType
						));
						if ($loVersion) {
							CommonTracker::createSlaveTracksForPushedObject($loVersion, $tmpModel, $tmpModel->idCourse);
						}
					}
				}


			} catch (CException $e) {
				if (isset($transaction)) {
					$transaction->rollback();
				}
				throw ($e);
			}
		}

		return true;

	}


	/**
	 * Batch copy of all enrollments of THIS course to another one, identified by ID.
	 * The target course must already exists.
	 *
	 * @param number $course_id
	 * @param array $params
	 * @throws CException
	 * @return boolean
	 */
	public function copyEnrollmentsTo($course_id, $params = array())
	{

		$source = $this;
		$target = self::model()->findByPk($course_id);

		// Target course MUST already exist
		if (!$target) {
			throw new CException('Target course does not exist');
		}

		Yii::log("Copy Enrollments from $this->idCourse to $target->idCourse", CLogger::LEVEL_INFO);

		// Ok, copy
		// See CourseManagementController::actionCopyEnrollments() for hints. It is working well

		$currentCourseId = $this->getPrimaryKey();
		$sourceCourseUsers = LearningCourseuser::model()->findAllByAttributes(array('idCourse' => $currentCourseId));
		if (!empty($sourceCourseUsers)) {
			foreach ($sourceCourseUsers as $courseUser) {
				$learningCourseUser = new LearningCourseuser();
				$level = (!empty($params['force_level'])) ? $params['force_level'] : $courseUser->level;
				$rs = $learningCourseUser->subscribeUser($courseUser->idUser, Yii::app()->user->id, $course_id,
					$courseUser->waiting, $level);
				if ($rs === false) {
					switch ($learningCourseUser->getErrorMessage()) {
						case LearningCourseuser::$ALREADY_SUBSCRIBED: {
							//do nothing here, this can be accepted
						}
							break;
						default: {
							throw new CException('Error while copying enrollments');
						}
							break;
					}
				}
			}
		}

		return true;
	}


	/**
	 * Batch copy of all blocks assigned to THIS course to another one, identified by ID.
	 * The target course must already exists.
	 *
	 * @param number $course_id
	 * @throws CException
	 * @return boolean
	 */
	public function copyBlockWidgetsTo($course_id)
	{
		$target = self::model()->findByPk($course_id);

		// Target course MUST already exist
		if (!$target) {
			throw new CException('Target course does not exist');
		}

		$widgets = PlayerBaseblock::model()->findAllByAttributes(array(
			'course_id' => $this->idCourse
		));

		foreach ($widgets as $widget) {
			$block = new PlayerBaseblock();
			$block->course_id = $course_id;
			$block->content_type = $widget->content_type;
			$block->title = $widget->title;
			$block->columns_span = $widget->columns_span;
			$block->position = $widget->position;
			$block->params = '[]';
			$block->save(false);
			if ($widget->content_type == PlayerBaseblock::TYPE_COMMENTS) {
				$forum = new LearningForum();
				$forum->idCourse = $course_id;
				$forum->title = Yii::t('standard', '_COMMENTS');
				$forum->description = Yii::t('standard', '_COMMENTS');
				$forum->save(false);
			}
		}

		Yii::log("Copy Blocks from $this->idCourse to $target->idCourse", CLogger::LEVEL_INFO);
		// Ok, copy

		return true;
	}


	/**
	 * Render course logo: helper function for grids
	 *
	 * @param bool|string $size
	 * @return string
	 */
	public function renderCourseLogo($variant = CoreAsset::VARIANT_ORIGINAL)
	{
		$content = '';

		$style = "";
		if ($variant == CoreAsset::VARIANT_MICRO) {
			$style = "width: 30px;";
		}

		$logoUrl = $this->getCourseLogoUrl($variant);

		$content = CHtml::image($logoUrl, 'Logo', array('style' => $style));
		return $content;
	}


	/**
	 * If it's a classroom course, retrieve all sessions ARs (false otherwise)
	 *
	 * @return array
	 */
	public function getSessions($showExpiredSessions = true)
	{

		if ($this->course_type == self::TYPE_ELEARNING) {
			return false;
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :id_course");
		$criteria->params[':id_course'] = $this->getPrimaryKey();
		$criteria->order = "date_begin ASC";
		$criteria->index = "id_session"; // use id_session as array keys instead of autoincrement int

		if ($this->course_type == self::TYPE_WEBINAR) {
			if (!$showExpiredSessions) {
				$criteria->join = ' LEFT JOIN ' . WebinarSessionDate::model()->tableName() . ' wsd ON wsd.id_session = t.id_session';
				$criteria->addCondition("DATE_ADD(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE)  >= :dateNow");
				$criteria->params[':dateNow'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
			}

			$pUserRights = Yii::app()->user->checkPURights($this->getPrimaryKey());
			if (!Yii::app()->user->getIsGodadmin() && !$pUserRights->all && $pUserRights->isInstructor) {
				$criteria->with = array(
					'webinarSessionUsers' => array(
						'alias' => 'wsu'
					)
				);
				$criteria->addCondition('wsu.id_user = :idUser');
				$criteria->params[':idUser'] = Yii::app()->user->id;
			}
			return WebinarSession::model()->findAll($criteria);
		}

		if (!$showExpiredSessions) {
			//Show only sessions with last_subscription_date > now for the power users
			$criteria->addCondition("last_subscription_date = :not_set OR last_subscription_date IS NULL OR last_subscription_date = '' OR last_subscription_date >= :last_subscription_date");
			$criteria->addCondition("date_end >=:last_subscription_date");
			$criteria->params[':not_set'] = '0000-00-00 00:00:00';
			$criteria->params[':last_subscription_date'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		}

		$pUserRights = Yii::app()->user->checkPURights($this->getPrimaryKey());
		if (!Yii::app()->user->getIsGodadmin() && !$pUserRights->all && $pUserRights->isInstructor) {
			$criteria->with = array(
				'learningCourseuserSessions' => array(
					'alias' => 'cus'
				)
			);
			$criteria->addCondition('cus.id_user = :idUser');
			$criteria->params[':idUser'] = Yii::app()->user->id;
		}


		if(Yii::app()->user->getIsPu()) {
			if(Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add')) {
				if(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign')){
					$criteria->addCondition('created_by = :createdBy');
					$criteria->params[':createdBy'] = Yii::app()->user->id;
				}
			}
		}

		return LtCourseSession::model()->findAll($criteria);
	}

	/**
	 * If it's a webinar course, retrieve all sessions ARs (false otherwise)
	 *
	 * @return array
	 */
	public function getWebinarSessions()
	{

		if (!in_array($this->course_type, array(self::TYPE_WEBINAR, self::TYPE_ELEARNING))) {
			return false;
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :id_course");
		$criteria->params[':id_course'] = $this->getPrimaryKey();
		$criteria->order = "date_begin ASC";
		$criteria->index = "id_session"; // use id_session as array keys instead of autoincrement int

		$pUserRights = Yii::app()->user->checkPURights($this->getPrimaryKey());
		if (!Yii::app()->user->getIsGodadmin() && !$pUserRights->all && $pUserRights->isInstructor) {
			$criteria->with = array(
				'webinarSessionUsers' => array(
					'alias' => 'wsu'
				)
			);
			$criteria->addCondition('wsu.id_user = :idUser');
			$criteria->params[':idUser'] = Yii::app()->user->id;
		}

		if(Yii::app()->user->getIsPu()) {
			if(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add')) {
				if(!Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') && !Yii::app()->user->checkAccess('/lms/admin/webinarsessions/assign')) {
					$criteria->addCOndition('created_by = :createdBy');
					$criteria->params[':createdBy'] = Yii::app()->user->id;
				}
			}
		}

		return WebinarSession::model()->findAll($criteria);
	}


	/**
	 * If it's a classroom course, retrieve all sessions years (false otherwise)
	 *
	 * @return array
	 */
	public function getSessionsYears()
	{

		if ($this->course_type != self::TYPE_CLASSROOM && $this->course_type != self::TYPE_WEBINAR) {
			return false;
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :id_course");
		$criteria->params[':id_course'] = $this->getPrimaryKey();

		$output = array();
		if ($this->course_type == self::TYPE_CLASSROOM) {
			$list = LtCourseSession::model()->findAll($criteria);
		} else {
			$list = WebinarSession::model()->findAll($criteria);
		}
		if (!empty($list)) {
			foreach ($list as $record) {
				$utcDateBegin = Yii::app()->localtime->fromLocalDateTime($record->date_begin);
				$year = substr($utcDateBegin, 0, 4);
				if (!in_array($year, $output)) {
					$output[] = $year;
				}
				$utcDateEnd = Yii::app()->localtime->fromLocalDateTime($record->date_end);
				$year = substr($utcDateEnd, 0, 4);
				if (!in_array($year, $output)) {
					$output[] = $year;
				}
			}
		}

		$output = array_unique($output);
		sort($output);
		return $output;
	}

	public function getWebinarSessionYears()
	{
		if ($this->course_type != self::TYPE_CLASSROOM && $this->course_type != self::TYPE_WEBINAR) {
			return false;
		}

		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :id_course");
		$criteria->params[':id_course'] = $this->getPrimaryKey();
		$output = array();
		$list = WebinarSession::model()->findAll($criteria);
		if (!empty($list)) {
			foreach ($list as $record) {
				$dateCriteria = new CDbCriteria();
				$dateCriteria->addCondition("id_session = :session_id");
				$dateCriteria->params[':session_id'] = $record->id_session;
				$datesList = WebinarSessionDate::model()->findAll($dateCriteria);

				if (!empty($datesList)) {
					foreach ($datesList as $date) {
						$lowestDate = Yii::app()->getDb()->createCommand()
							->select("CONVERT_TZ(concat(day,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC")
							->from(WebinarSessionDate::model()->tableName())
							->andWhere('id_session=:idsession', array(':idsession' => $record->id_session))
							->order("beginTimestampUTC ASC")
							->queryScalar();

						$year = substr($lowestDate, 0, 4);
						if (!in_array($year, $output) && $year) {
							$output[] = $year;
						}

						$biggestEndDate = Yii::app()->getDb()->createCommand()
							->select("DATE_ADD(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE) AS endtimestampUTC")
							->from(WebinarSessionDate::model()->tableName())
							->andWhere('id_session=:idsession', array(':idsession' => $record->id_session))
							->order("endtimestampUTC DESC")
							->queryScalar();
						$year = substr($biggestEndDate, 0, 4);
						if (!in_array($year, $output)) {
							$output[] = $year;
						}
					}
				}
			}
		}

		$output = array_unique($output);
		sort($output);
		return $output;
	}


	public function unassignedUsersDataProvider($params = array())
	{
		// then get unassigned users
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'learningUser' => array(
				'alias' => 'user',
				'joinType' => 'INNER JOIN'
			)
		);
		$criteria->addCondition('idCourse = :course_id');
		$criteria->addCondition('idUser NOT IN (
			SELECT DISTINCT(id_user)
			FROM lt_courseuser_session
			JOIN lt_course_session ON lt_courseuser_session.`id_session` = lt_course_session.`id_session`
			WHERE lt_course_session.`course_id` = :course_id
		)');
		$criteria->params[':course_id'] = $this->idCourse;

		if (Yii::app()->user->getIsAdmin()) {
			$criteria->join = ' INNER JOIN core_user_pu pu ON (pu.puser_id = :puser_id AND pu.user_id = t.idUser) ';
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}

		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%' . $userQuery . '%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%' . $userSearch[0] . '%';
		}

		$config['criteria'] = $criteria;

		//$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
//		$config['pagination'] = array(
//			'pageSize' => Settings::get('elements_per_page', 10)
//		);

		$dataProvider = new CActiveDataProvider(LearningCourseuser::model(), $config);

		return $dataProvider;
	}

	public function unassignedWebinarUsersDataProvider($params = array())
	{
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'learningUser' => array(
				'alias' => 'user',
				'joinType' => 'INNER JOIN'
			)
		);
		$criteria->addCondition('idCourse = :course_id');
		$criteria->addCondition('idUser NOT IN (
			SELECT DISTINCT(id_user)
			FROM webinar_session_user
			JOIN webinar_session ON webinar_session_user.`id_session` = webinar_session.`id_session`
			WHERE webinar_session.`course_id` = :course_id
		)');
		$criteria->params[':course_id'] = $this->idCourse;

		if (Yii::app()->user->getIsAdmin()) {
			$criteria->join = ' INNER JOIN core_user_pu pu ON (pu.puser_id = :puser_id AND pu.user_id = t.idUser) ';
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}

		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%' . $userQuery . '%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%' . $userSearch[0] . '%';
		}

		$config['criteria'] = $criteria;

		//$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		$dataProvider = new CActiveDataProvider(LearningCourseuser::model(), $config);

		return $dataProvider;
	}

	/**
	 * @return int
	 */
	public function getUnassignedUsersCount()
	{
		$dataProvider = $this->unassignedUsersDataProvider();
		return $dataProvider->getTotalItemCount();
	}

	/**
	 * @return int
	 */
	public function getUnassignedWebinarUsersCount()
	{
		$dataProvider = $this->unassignedWebinarUsersDataProvider();
		return $dataProvider->getTotalItemCount();
	}


	/**
	 * @return bool
	 */
	public function isClassroomCourse()
	{
		return ($this->course_type == self::TYPE_CLASSROOM);
	}

	/**
	 * @return bool
	 */
	public function isWebinarCourse()
	{
		return ($this->course_type == self::TYPE_WEBINAR);
	}


	/**
	 * Based on course_type attribute, return its translation in real world
	 *
	 * @return string
	 */
	public function getCourseTypeTranslation($type = false)
	{
		$type = ($type == false ? $this->course_type : $type);
		$list = self::typesList();
		if (!isset($list[$type])) {
			return "unknown type";
		}
		return $list[$type];
	}


	/**
	 * Helper function to return Root LO title, used internally only
	 *
	 * @param number $idCourse
	 * @return string
	 */
	protected function getRootTitle($idCourse)
	{
		return LearningOrganization::ROOT_NODE_PREFIX . (int)$idCourse;
	}

	public function beforeValidate()
	{

		$validate = true;
		$event = new DEvent($this, array('course' => $this, 'validate' => &$validate));
		Yii::app()->event->raise('beforeValidateCourse', $event);
		if ($event->return_value) {
			return $validate;
		}

		return parent::beforeValidate();
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->create_date = Yii::app()->localtime->getUTCNow();
		}

		$rootNode = LearningCourseCategoryTree::getRootNodeId();
		$categoryTree = LearningCourseCategoryTree::model()->findByPk($this->idCategory);
		if (((!$this->idCategory) || (!$categoryTree)) && $rootNode) {
			$this->idCategory = $rootNode;
		}

		// Prevent issues with not saving unchgecked checkboxes
		$this->coaching_option_allow_start_course = Yii::app()->request->getParam('coaching_option_allow_start_course',
			0);
		$this->coaching_option_allow_request_session_change = Yii::app()->request->getParam('coaching_option_allow_request_session_change',
			0);
		$this->coaching_option_access_without_assign_coach = Yii::app()->request->getParam('coaching_option_access_without_assign_coach',
			0);

		$this->implodeMediumTime();
		return parent::beforeSave();
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave()
	{

		parent::afterSave();

		// Flush category tree used in Catalog Dashlet
		LearningCourseCategory::flushTreeCache();

		$idCourse = $this->idCourse;

		if ($this->isNewRecord) {

			Yii::app()->event->raise('NewCourseCreated', new DEvent());

			if (Yii::app()->db->getCurrentTransaction() === null) {
				$transaction = Yii::app()->db->beginTransaction();
			}

			try {
				// Create root node and save it
				$courseRoot = new LearningOrganization();
				$courseRoot->title = $this->getRootTitle($idCourse);
				$courseRoot->idCourse = (int)$idCourse;
				$rs = $courseRoot->saveNode(); // it will call internally "makeRoot" method, assigning 'idCourse' as subtree identifier

				if (!$rs) {
					throw new CException('Error in course organization root node creation');
				}

				if (isset($transaction)) {
					$transaction->commit();
				}

				Yii::app()->event->raise(EventManager::EVENT_NEW_COURSE, new DEvent($this, array(
					'course' => $this,
				)));


			} catch (CException $e) {
				if (isset($transaction)) {
					$transaction->rollback();
				}
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				throw $e;
			}
		} else {
			// Course properties changed (course modified)
			// Course properties changed (course modified)
			Yii::app()->event->raise(EventManager::EVENT_COURSE_PROP_CHANGED, new DEvent($this, array(
				'course' => $this,
				'user' => Yii::app()->user->loadUserModel(),
			)));

			if ($this->oldAttributes['final_score_mode'] != $this->final_score_mode || $this->oldAttributes['final_object'] != $this->final_object) {
				// Reset all cache 'score_given' values for this course
				// since the score calculation formula has changed
				// On next request for the user's score
				// (via LearningCourseuser::getLastScoreForUserAndCourse())
				// the score_given value will be calculated dynamically
				Yii::app()->getDb()->createCommand()
					->update(LearningCourseuser::model()->tableName(), array('score_given' => null),
						'idCourse=:idCourse', array(':idCourse' => $this->idCourse));
				Yii::log('Resetting cached score_given values for course ' . $idCourse, CLogger::LEVEL_INFO);
			}

			if ($this->oldAttributes['initial_score_mode'] != $this->initial_score_mode || $this->oldAttributes['initial_object'] != $this->initial_object) {
				// Reset all cache 'initial_score_given' values for this course
				// since the score calculation formula has changed
				// On next request for the user's score
				// (via LearningCourseuser::getLastInitialScoreByUserAndCourse())
				// the score_given value will be calculated dynamically
				Yii::app()->getDb()->createCommand()
					->update(LearningCourseuser::model()->tableName(), array('initial_score_given' => null),
						'idCourse=:idCourse', array(':idCourse' => $this->idCourse));
				Yii::log('Resetting cached initial_score_given values for course ' . $idCourse, CLogger::LEVEL_INFO);
			}
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete()
	{
		if (!parent::beforeDelete()) {
			return false;
		}

		Yii::app()->event->raise('BeforeCourseDeleted', new DEvent($this));

		if (Yii::app()->db->getCurrentTransaction() === null) {
			$transaction = Yii::app()->db->beginTransaction();
		}

		$idCourse = $this->idCourse;

		try {

			// First check if we are dealing with a classroom course, if so delete course sessions
			if ($this->course_type == LearningCourse::TYPE_CLASSROOM) {
				$sessions = $this->getSessions();
				if (!empty($sessions)) {
					foreach ($sessions as $session) {
						$rs = $session->deleteSession();
						if (!$rs) {
							throw new CException('Error while deleting course sessions.');
						}
					}
				}
			}

			// First check if we are dealing with a classroom course, if so delete course sessions
			if (in_array($this->course_type, array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_WEBINAR))) {
				$sessions = $this->getWebinarSessions();
				if (!empty($sessions)) {
					foreach ($sessions as $session) {
						try {
							$rs = $session->deleteSession();
							if (!$rs) {
								$msg = "Error while deleting course sessions:\n".implode("\n", $session->getErrors());
								Yii::log($msg, CLogger::LEVEL_ERROR);
								throw new CException($msg);
							}
						} catch (Exception $e) {
							Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
							throw new CException('Error while deleting course sessions.');
						}
					}
				}
			}

			// Retrieve root node for this course
			$courseRoot = LearningObjectsManager::getRootNode($idCourse);
			if (!$courseRoot) {
				throw new CException('Invalid root node');
			}

			// Just delete it with L.O. manager class (this will clear L.O. related data and files too)
			$rs = LearningObjectsManager::deleteLearningObject($courseRoot->getPrimaryKey(),
				array('preventRootDeleting' => false));
			if (!$rs) {
				throw new CException('Error in course learning objects deleting');
			}

			if (isset($transaction)) {
				$transaction->commit();
			}

			Yii::app()->event->raise(EventManager::EVENT_COURSE_DELETED, new DEvent($this, array(
				'course' => $this,
			)));


		} catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}


		return true;


	}


	/**
	 * @return bool
	 */
	public function isMobile()
	{
		return ($this->course_type == self::TYPE_MOBILE);
	}


	/**
	 * A method, temporary in its virtue, to fix OLD course logos, specified as FILE NAMES in learning_course->img_course table field.
	 * Create a private asset on the fly and return asset object.
	 *
	 * @return Ambigous <boolean, CoreAsset>
	 */
	protected function fixLocalCourseLogo()
	{

		$imageCourse = trim($this->img_course);
		$filePath = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . 'doceboLms' . DIRECTORY_SEPARATOR . Settings::get('pathcourse') . $imageCourse;
		if (!is_file($filePath)) {
			return false;
		}

		$asset = new CoreAsset();
		$asset->type = CoreAsset::TYPE_COURSELOGO;
		$asset->scope = CoreAsset::SCOPE_PRIVATE;
		$asset->sourceFile = $filePath;
		if ($asset->save()) {
			$this->img_course = $asset->id;
			$this->update();
		} else {
			Yii::log('Fix OLD course logo failed: ' . print_r($asset->errors), 'error');
			$asset = false;
		}

		return $asset;

	}

	public function getRoundedCourseTimeByHoursMinutes()
	{
		if ($this->mediumTime > 3540) {
			// more than 59 minutes
			return (floor($this->mediumTime / 3600)) . ' h ' . round(($this->mediumTime % 3600) / 60) . ' m';
		} else {
			// less than 59 minutes
			return ceil($this->mediumTime / 60) . ' m';
		}
	}


	public function formatAverageCourseTime()
	{
		$res = '';

		if ($this->durationHours > 0) {
			$res .= $this->durationHours . ':';
		}

		if ($this->durationMinutes > 0) {
			$res .= sprintf('%02d', $this->durationMinutes) . ':';
		} else {
			$res .= '00:';
		}

		if ($this->durationSeconds > 0) {
			$res .= sprintf('%02d', $this->durationSeconds);
		} else {
			$res .= '00';
		}

		return $res;
	}

	/**
	 * @return float
	 */
	public function getRating()
	{
		if (!$this->learningCourseRating) {
			return 0;
		}

		$rating = $this->learningCourseRating;
		$avg = $rating->rate_average;
		return round($avg);
	}

	//internal fields for caching purpose
	protected $_terminator = null;
	protected $_endObject = null;

	/**
	 * Retrieve course LO with terminator property if present, false otherwise
	 * @return \LearningObject|bool|null the AR record of the LO with terminator property, or false if no object has been set
	 */
	public function getTerminatorLearningObject()
	{
		if ($this->isNewRecord) {
			return false;
		}
		if ($this->_terminator) {
			return $this->_terminator;
		}
		$objects = LearningOrganization::model()->findAllByAttributes(array(
			'idCourse' => $this->getPrimaryKey(),
			'isTerminator' => 1,
		));
		$output = false;
		if (!empty($objects)) {
			$output = $objects[0];
			$this->_terminator = $output;
		}
		return $output;
	}

	/**
	 * Retrieve course LO with milestone seto to 'end', if present (false otherwise)
	 * @return \LearningObject|bool|null the AR record of the LO with terminator property, or false if no object has been set
	 */
	public function getEndMilestoneLearningObject()
	{
		if ($this->isNewRecord) {
			return false;
		}
		if ($this->_endObject) {
			return $this->_endObject;
		}
		$objects = LearningOrganization::model()->findAllByAttributes(array(
			'idCourse' => $this->getPrimaryKey(),
			'milestone' => LearningOrganization::MILESTONE_END
		));
		$output = false;
		if (!empty($objects)) {
			$output = $objects[0];
			$this->_endObject = $output;
		}
		return $output;
	}

	/**
	 * @return int
	 */
	public function getRatingsCount()
	{
		if (!$this->learningCourseRating) {
			return 0;
		}

		$rating = $this->learningCourseRating;

		return $rating->rate_count;
	}

	/**
	 * @param $idUser
	 * @param $value
	 * @return bool
	 */
	public function saveRating($idUser, $value)
	{
		$ipUser = Yii::app()->request->userHostAddress;
		$anon_user = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
		if ($idUser) {
			$vote = LearningCourseRatingVote::model()->findByPk(array(
				'idUser' => $idUser,
				'idCourse' => $this->idCourse
			));
			$old_rating = $vote->rating;
			if (!$vote) {
				$vote = new LearningCourseRatingVote();
			}
			$vote->idCourse = $this->idCourse;
			$vote->idUser = $idUser;
			$vote->rating = $value;
			$vote->timestamp = date('Y-m-d H:i:s');
			$vote->save();
		}
		$total_rating = LearningCourseRating::model()->findByPk($this->idCourse);
		if (empty($total_rating)) {
			$total_rating = new LearningCourseRating();
		}
		if ($old_rating == "") {
			$total_rating->rate_sum = $total_rating->rate_sum + $value;
			$total_rating->rate_count = $total_rating->rate_count + 1;
		} else {
			$total_rating->rate_sum = $total_rating->rate_sum - $old_rating + $value;
		}

		$total_rating->idCourse = $this->idCourse;
		$total_rating->rate_average = ceil($total_rating->rate_sum / $total_rating->rate_count);
		$total_rating->save();
	}

	/**
	 * Returns the {$count} most rated courses
	 * @param int $count
	 * @return LearningCourse[]
	 */
	public static function getTopRated($count = 5)
	{
		$results = array();
		$showAllCoursesIfNoCatalogueIsAssigned = ('on' == Settings::get('on_catalogue_empty', 'on'));
		$all_courses = false;
		$courses_idst = array(0);

		$catalogues = LearningCatalogue::model()->getUserCatalogIds(Yii::app()->user->id);
		if (!empty($catalogues)) {
			$all_courses = false;
			$command = Yii::app()->db->createCommand();
			$command->select('c.idCourse');
			$command->from(LearningCourse::model()->tableName() . ' AS c');
			$command->join(LearningCatalogueEntry::model()->tableName() . ' AS ce', 'c.idCourse = ce.idEntry');
			$command->where(array('in', 'ce.idCatalogue', $catalogues));
			$command->andWhere(array(
				'in',
				'c.status',
				array(LearningCourse::$COURSE_STATUS_EFFECTIVE, LearningCourse::$COURSE_STATUS_AVAILABLE)
			));
			$command->andWhere('ce.type_of_entry ="' . LearningCatalogueEntry::ENTRY_COURSE . '"');
			if (Settings::get("module_ecommerce", "off") != "on") {
				$command->andWhere('c.selling = 0');
			}

			$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay();
			$excludedCourses = array();
			Yii::app()->event->raise('OnCatalogQueryCourses', new DEvent(self, array(
				'excludedCourses' => &$excludedCourses
			)));
			if (!empty($excludedCourses)) {
				$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $excludedCourses);
			}
			$command->andWhere(array('not in', 'c.idCourse', $coursesToNotDisplayList));

			$courses_idst = $command->queryColumn();

			if (empty($courses_idst)) {
				$courses_idst = array(0);
			}
		} elseif ($showAllCoursesIfNoCatalogueIsAssigned) {
			$all_courses = true;
		}

		$criteria = new CDbCriteria();
		$criteria->with = array(
			'learningCourseRating' => array(
				'alias' => 'rating',
				'joinType' => 'INNER JOIN'
			)
		);

		if (!$all_courses) {
			$criteria->addInCondition('t.idCourse', $courses_idst);
		}

		$criteria->order = 'rating.rate_average DESC';
		$criteria->limit = $count;
		//We don't want courses that have custom rating settings disabled
		$condition = "(t.social_rating_settings NOT LIKE '%" . AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED . "%')";

		// First get the system rating settings
		$systemSettings = Settings::get('course_rating_permission',
			AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED);
		/*
		 * If the system rating settings is enabled we allow to get courses that don't have
		 * custom settings at all
		 */
		if ($systemSettings != AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED) {
			$condition .= " OR (t.social_rating_settings IS NULL)";
		}
		$criteria->addCondition($condition);

		return LearningCourse::model()->findAll($criteria);
	}


	public function getUserSessions($userId)
	{
		$userSessions = array();
		switch ($this->course_type) {
			case self::TYPE_CLASSROOM :
				$criteria = new CDbCriteria();
				$criteria->with = array('ltCourseSession');
				$criteria->together = true;
				$criteria->addCondition(LtCourseuserSession::model()->getTableAlias() . '.id_user = :idUser');
				$criteria->addCondition('ltCourseSession.course_id = :idCourse');
				$criteria->params = array(
					':idUser' => $userId,
					':idCourse' => $this->idCourse
				);
				$userSessions = LtCourseuserSession::model()->findAll($criteria);
				break;
			case self::TYPE_WEBINAR :
				$criteria = new CDbCriteria();
				$criteria->with = array('sessionModel');
				$criteria->together = true;
				$criteria->addCondition(WebinarSessionUser::model()->getTableAlias() . '.id_user = :idUser');
				$criteria->addCondition('sessionModel.course_id = :idCourse');
				$criteria->params = array(
					':idUser' => $userId,
					':idCourse' => $this->idCourse
				);
				$userSessions = WebinarSessionUser::model()->findAll($criteria);
				break;
		}

		return $userSessions;
	}

	/**
	 * Removes all user sessions for user
	 * @param $userId
	 */
	public function removeAllUserSessionsByUserId($userId)
	{
		$userSessions = $this->getUserSessions($userId);
		if (isset($userSessions)) {
			foreach ($userSessions as $session) {
				$session->delete();
				if ($this->course_type == self::TYPE_CLASSROOM) {
					Yii::app()->event->raise('ILTSessionUserUnenrolled', new DEvent($this, array(
						'session' => $session->id_session,
						'user' => $userId,
					)));
				}
			}
		}
	}

	/**
	 * Expand the course time into the hours/minutes/seconds components
	 */
	public function explodeMediumTime()
	{
		$duration = array(
			'h' => 0,
			'm' => 0,
			's' => 0
		);

		if (intval($this->mediumTime) > 0) {
			$mediumTime = intval($this->mediumTime);
			$duration['h'] = intval($this->mediumTime / 3600);

			$mediumTime -= $duration['h'] * 3600;
			$duration['m'] = intval($mediumTime / 60);

			$mediumTime -= $duration['m'] * 60;
			$duration['s'] = $mediumTime;
		}

		$this->durationHours = $duration['h'];
		$this->durationMinutes = $duration['m'];
		$this->durationSeconds = $duration['s'];
	}

	public function implodeMediumTime()
	{
		$duration = intval($this->durationHours) * 3600
			+ intval($this->durationMinutes) * 60
			+ intval($this->durationSeconds);
		$this->mediumTime = $duration;
	}

	/**
	 * Returns the course price after triggering an event
	 * @return string
	 */
	public function getCoursePrice()
	{
		$event = new DEvent($this, array('course' => $this));
		Yii::app()->event->raise('OnCatalogGetCoursePrice', $event);

		if ($event->shouldPerformAsDefault() || !isset($event->return_value['price'])) {
			return ($this->prize && $this->selling) ? $this->prize : 0;
		} else {
			return $event->return_value['price'];
		}
	}

	/**
	 * @return LearningCertificate
	 */
	public function getCertificateAssigned()
	{
		if ($this->certificatesAssigned) {
			if (is_array($this->certificatesAssigned)) {
				return $this->certificatesAssigned[0];
			} else {
				return $this->certificatesAssigned;
			}
		}
	}


	/**
	 * Try to find the Player Layout used for this course.
	 * If the course has custom one set, use that, otherwise
	 * use the platform default one
	 * @param null $idCourse
	 *
	 * @return mixed
	 */
	public static function getPlayerLayout($idCourse = null)
	{

		static $layoutsArr = array();

		$playerLayoutDefault = Settings::get('player_layout', self::$PLAYER_LAYOUT_PLAY_BUTTON);

		Yii::app()->event->raise('ResolvingCoursePlayerLayout', new DEvent(new LearningCourse(), array(
			'player_layout' => &$playerLayoutDefault,
		)));


		if (!isset($layoutsArr[$idCourse])) {
			$tmp = LearningCourse::model()->findByPk($idCourse);
			if ($tmp && $tmp->player_layout) {
				$layoutsArr[$idCourse] = $tmp->player_layout;
			} else {
				$layoutsArr[$idCourse] = $playerLayoutDefault;
			}
		}

		return $layoutsArr[$idCourse];
	}

	public static function getResumeAutoplayEnabled($idCourse = null)
	{
		static $cache = null;

		if ($cache === null || !isset($cache[$idCourse])) {
			$query = Yii::app()->getDb()->createCommand()
				->select('resume_autoplay, idCourse')
				->from(self::model()->tableName());
			if ($idCourse) {
				$query->where('idCourse = :idCourse', array(':idCourse' => $idCourse));
			}

			$results = $query->queryAll();

			$cache = array();
			foreach ($results as $row) {
				$cache[$row['idCourse']] = $row['resume_autoplay'];
			}
		}

		if (isset($cache[$idCourse]) && $cache[$idCourse] != self::RESUME_AUTOPLAY_UNSET) {
			if ($cache[$idCourse] == self::RESUME_AUTOPLAY_ON) {
				return true;
			}
		} else {
			$globalSetting = Settings::get('course_resume_autoplay');
			if ($globalSetting == 'on') {
				return true;
			}
		}
		return false;
	}

	/** Check if deadlines are soft for this course or globally
	 * @return bool
	 */
	public function getSoftDeadlineEnabled()
	{
		if (!is_null($this->soft_deadline)) {
			return $this->soft_deadline == self::SOFT_DEADLINE_ON;
		} else {
			// Check if course has category and the category has a non null soft_deadline flag (climbing up the entire hierarchy)
			if (($this->idCategory > 0) && ($category = LearningCourseCategoryTree::model()->findByPk($this->idCategory))) {
				$categoryLevelDeadline = $category->getInheritedSoftDeadline();
				if ($categoryLevelDeadline) {
					return $categoryLevelDeadline['soft_deadline'];
				}
			}

			// Last attempt -> global soft_deadline flag
			$globalSetting = Settings::get('soft_deadline', 'off');
			return $globalSetting == 'on';
		}
	}

	/**
	 * An internal method used to retrieve shared and private (user specific)
	 * images to be used in a carousel widget-like structure
	 *
	 * @param     $idCourse
	 *
	 * @param int $gallerySize - How many thumbnails to display per carousel slide
	 *
	 * @return array with exactly 4 elements:
	 * 1: Total images count
	 * 2. Default images in the platform (also called "shared")
	 * 3. Private images (uploaded by user)
	 * 4. The current selected image. Possible values: default|user
	 */
	static public function prepareThumbnails($idCourse = null, $gallerySize = 10)
	{

		if ($idCourse) {
			$model = self::model()->findByPk($idCourse);
		} else {
			$model = new LearningCourse();
		}

		// Get list of URLs of SHARED and USER player backgrounds
		$assets = new CoreAsset();
		$defaultImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_SHARED, false,
			CoreAsset::VARIANT_SMALL);
		$userImages = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_PRIVATE, false,
			CoreAsset::VARIANT_SMALL);

		$count = array(
			'default' => count($defaultImages),
			'user' => count($userImages),
		);

		$reorderArray = function (&$array, $key, $value) {
			unset($array[$key]);
			$array = array($key => $value) + $array;
		};

		foreach ($defaultImages as $key => $value) {
			if ($key == $model->img_course) {
				$selected = 'default';

				$reorderArray($defaultImages, $key, $value);
				break;
			}
		}

		if (empty($selected)) {
			foreach ($userImages as $key => $value) {
				if ($key == $model->img_course) {
					$selected = 'user';
					$reorderArray($userImages, $key, $value);
					break;
				}
			}
		}

		$selected = empty($selected) ? 'default' : $selected;

		$defaultImages = array_chunk($defaultImages, $gallerySize, true);
		$userImages = array_chunk($userImages, $gallerySize, true);

		return array($count, $defaultImages, $userImages, $selected);
	}

	/**
	 * return course name in format "course_name - code" or "course_name" (if the code is empty).
	 * used in courseManagement/courseAutocomplete
	 * @return string
	 */
	public function getCourseNameFormatted()
	{
		if (!empty($this->code)) {
			return $this->name . ' - ' . $this->code;
		} else {
			return $this->name;
		}
	}

	/**
	 * Return if the course can be shared by the user
	 * @param null $userId
	 * @return bool
	 */
	public function canShare($userId = null)
	{
		$res = false;
		if ($this->course_sharing_permission == AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_ENROLLED) {
			if (LearningCourseUser::isSubscribed($userId, $this->idCourse)) {
				$res = true;
			}
		} else {
			if ($this->course_sharing_permission == AdvancedSettingsSocialRatingForm::COURSE_SHARE_IF_COMPLETED) {
				$model = LearningCourseUser::model()->findByAttributes(array(
					"idUser" => $userId,
					"idCourse" => $this->idCourse
				));
				if ($model && $model->status == LearningCourseUser::$COURSE_USER_END) {
					$res = true;
				}
			}
		}
		return $res;
	}

	// Changed so all users disregarding their subscription status can view course ratings,
	// unless disabled option is selected
	public function canViewRating()
	{
		$can_view_rating = false;

		switch ($this->course_rating_permission) {
			case AdvancedSettingsSocialRatingForm::COURSE_RATING_ENABLED:
				$can_view_rating = true;
				break;
			case AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_ENROLLED:
//				if(LearningCourseUser::isSubscribed(Yii::app()->user->id, $this->idCourse))
//                $can_view_rating = true;
				$can_view_rating = true;
				break;
			case AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_COMPLETED:
//				$model = LearningCourseUser::model()->findByAttributes(array("idUser" => Yii::app()->user->id, "idCourse" => $this->idCourse));
//				if($model && $model->status == LearningCourseUser::$COURSE_USER_END)
				$can_view_rating = true;
				break;
			case AdvancedSettingsSocialRatingForm::COURSE_RATING_DISABLED:
			default:
				$can_view_rating = false;
				break;
		}

		return $can_view_rating;
	}

	/**
	 * Return if the course can be rated by the user
	 * @param null $userId
	 * @return bool
	 */
	public function canRate($userId = null)
	{
		// Prevent rating spam/frau
		// 1. Using course specific cookie
		// 2. Last IP rated this course in less than <N> seconds
		if ($this->course_rating_permission == AdvancedSettingsSocialRatingForm::COURSE_RATING_ENABLED) {
			return true;
		} else {
			if ($this->course_rating_permission == AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_ENROLLED) {
				if (LearningCourseUser::isSubscribed($userId, $this->idCourse)) {
					return true;
				}
			} else {
				if ($this->course_rating_permission == AdvancedSettingsSocialRatingForm::COURSE_RATING_IF_COMPLETED) {
					$model = LearningCourseUser::model()->findByAttributes(array(
						"idUser" => $userId,
						"idCourse" => $this->idCourse
					));
					if ($model && $model->status == LearningCourseUser::$COURSE_USER_END) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public function renderCssClassDel()
	{
		if (PluginManager::isPluginActive("ClassroomApp") && $this->course_type == LearningCourse::TYPE_CLASSROOM) {
			return "classroom-substitution-del";
		} else {
			if ($this->course_type == LearningCourse::TYPE_WEBINAR) {
				return "webinar-substitution-del";
			} else {
				$customCourseDescriptors = array();
				Yii::app()->event->raise('CollectCustomCourseTypes',
					new DEvent($this, array('types' => &$customCourseDescriptors)));
				if (isset($customCourseDescriptors[$this->course_type])) {
					return $this->course_type . "-substitution-del";
				} else {
					return "center-aligned";
				}
			}
		}
	}

	public function renderCssClassMain()
	{
		if (PluginManager::isPluginActive("ClassroomApp") && $this->course_type == LearningCourse::TYPE_CLASSROOM) {
			return "classroom-substitution-main center-aligned";
		} else {
			if ($this->course_type == LearningCourse::TYPE_WEBINAR) {
				return "webinar-substitution-main center-aligned";
			} else {
				$customCourseDescriptors = array();
				Yii::app()->event->raise('CollectCustomCourseTypes',
					new DEvent($this, array('types' => &$customCourseDescriptors)));
				if (isset($customCourseDescriptors[$this->course_type])) {
					return $this->course_type . "-substitution-main center-aligned";
				} else {
					return "max-available-column";
				}
			}
		}
	}

	/**
	 * render extra class for LP column for classrooms and webinars
	 *
	 * @return classname to be added in the grid column for classrooms/webinars
	 */
	public function renderCssClassLP()
	{
		if ((PluginManager::isPluginActive("ClassroomApp") && $this->course_type == LearningCourse::TYPE_CLASSROOM)
			|| ($this->course_type == LearningCourse::TYPE_WEBINAR)
		) {
			return "lp-substitution-main";
		} else {
			return "";
		}
	}


	/**
	 * A tiny utilitiy to return a idCourse=>name based array
	 * of all courses (uses internal cache, so it's safe to be
	 * called inside a loop)
	 *
	 * @return mixed
	 */
	static public function getAllNames()
	{
		static $cache = array();

		if (!empty($cache)) {
			return $cache;
		}

		$data = Yii::app()->getDb()->createCommand()
			->select('idCourse, name')
			->from(self::model()->tableName())
			->queryAll();

		foreach ($data as $row) {
			$cache[$row['idCourse']] = $row['name'];
		}
		return $cache;
	}


	static public function getNameById($courseId)
	{

		$data = Yii::app()->getDb()->createCommand()
			->select('name')
			->from(self::model()->tableName())
			->where('idCourse = :idCourse', array(':idCourse' => (int)$courseId))
			->queryScalar();

		return $data;
	}

	/**
	 * Check if the course type allows to be a "catch up" type
	 * So a catch up courses can be assigned
	 * @return bool
	 */
	public function isCatchupType()
	{
		$catchupTypes = LearningCourse::getCatchupTypes();
		return (in_array($this->course_type, $catchupTypes)) ? true : false;
	}

	/**
	 * Retrieve list of catchup course types
	 * @return array
	 */
	public static function getCatchupTypes()
	{
		$types = array(
			//LearningCourse::TYPE_WEBINAR,
			LearningCourse::TYPE_CLASSROOM,
		);

		//collect custom catchup courses types
		$customCatchupTypes = array();
		Yii::app()->event->raise('CollectCustomCourseCatchupTypes',
			new DEvent(self, array('types' => &$customCatchupTypes)));
		foreach ($customCatchupTypes as $type) {
			$types[] = $type;
		}

		return $types;
	}

	/**
	 * Get array of translated type=>translation catchup courses
	 * @return array
	 */
	public static function getCatchupTypesTranslated()
	{
		$res = array();
		$types = self::typesList();
		$catchupTypes = self::getCatchupTypes();
		foreach ($types as $type => $translation) {
			if (in_array($type, $catchupTypes)) {
				$res[$type] = $translation;
			}
		}
		return $res;
	}

	/**
	 * Get data provider for select a catchup course, excluding "this course"
	 * @return CActiveDataProvider
	 */
	public function dataProviderCatchupCourses($lpCourses = array())
	{
		$criteria = new CDbCriteria;

		$criteria->condition = 'idCourse != :idCourse';
		$criteria->params = array(':idCourse' => $this->idCourse);
		if (!empty($lpCourses)) {
			$criteria->addNotInCondition('idCourse', $lpCourses);
		}

		$criteria->compare('name', $this->search_input, true);
		$criteria->compare('code', $this->search_input, true, 'OR');

		return new CActiveDataProvider(new LearningCourse, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	/**
	 * Catchup courses should be visible only if the course is failed
	 * @param $idUser
	 * @return bool
	 */
	public function isCourseFailedByUser($idUser)
	{
		$res = false;
		switch ($this->course_type) {
			case LearningCourse::TYPE_CLASSROOM:
				$session = LtCourseSession::getLastSessionByDateBegin($this->idCourse, $idUser);
				if ($session && LtCourseuserSession::isSessionFailedByUser($session->id_session, $idUser)) {
					$res = true;
				}
				break;
		}
		return $res;
	}


	/**
	 * get the course LPs if the course is assigned to LPs, else return empty array
	 *
	 * @return array contains models or key/values depedning on $returnModels setting
	 */
	public function getCourseLPs($returnModels = false)
	{
		//initial value of the result array
		$courseLPs = array();

		// check is it activated the plugin first
		if (PluginManager::isPluginActive("CurriculaApp")) {
			// start getting the course LPs as models

			if ($returnModels) {
				//prepare models to return
				//get the LPs, to which the course is assigned
				$criteria = new CDbCriteria();
				$criteria->join = 'INNER JOIN learning_coursepath AS cp
                      ON cp.id_path = t.id_path';
				$criteria->condition = "id_item = :id_item";
				$criteria->params[':id_item'] = $this->idCourse;
				$itemsLPs = LearningCoursepathCourses::model()->findAll($criteria);

				// get count of items found
				$countLPs = count($itemsLPs);

				// if there are LPs found proceed with getting LPs details and pass them to the output
				if ($countLPs > 0) {
					// get LPs, that contain this course
					foreach ($itemsLPs as $lpItem) {
						//get corresponding LP item by relations
						$courseLPs[] = $lpItem->idPath;
					}
				}
			} else {
				// use PDO and get results as array of plain key/values
				//get the LPs, to which the course is assigned
				$itemsLPs = Yii::app()->db->createCommand()
					->select('cp.id_path, cp.path_name')
					->from(LearningCoursepathCourses::model()->tableName() . ' AS t')
					->join(LearningCoursepath::model()->tableName() . ' AS cp', 'cp.id_path = t.id_path')
					->where('t.id_item=:id_item', array(':id_item' => $this->idCourse))
					->queryAll();

				// get count of items found
				$countLPs = count($itemsLPs);

				// if there are LPs found proceed with getting LPs details and pass them to the output
				if ($countLPs > 0) {
					// get LPs, that contain this course
					foreach ($itemsLPs as $lpItem) {
						//get corresponding LP item by relations
						$courseLPs[] = array('id_path' => $lpItem['id_path'], 'path_name' => $lpItem['path_name']);
					}
				}

			}
		} // otherwise return initial empty array - avoid redundant else clause


		return $courseLPs;
	}


	/**
	 * Render links to LPs, that contains the course if the course is assigned to LP(-s)
	 * @return string
	 */
	public function renderLPs($params = array('forceOutput' => false, 'tooltipTitleLP' => ''))
	{
		// set default values
		$content = "";
		$output = "";
		$titleContent = $params['tooltipTitleLP'];

		$itemsLPs = array();

		if (!empty($this->idCourse)) {
			//set course by id
			$courseModel = LearningCourse::model()->findByPk($this->idCourse);

			//get the LPs, to which the course is assigned
			//$itemsLPs = $courseModel->getCourseLPs();
			$itemsLPs = $this->getCourseLPs();
		} else { // there is no course id passed return empty string
			return $output;
		}

		// get count of items found
		$countLPs = count($itemsLPs);

		// if there are LPs found proceed with getting LPs details and pass them to the output
		if ($countLPs > 0) {
			// build displayed icon + count in the LPs column
			$content = '<span><u>' . $countLPs . '</u><i class="curricula-ico"></i></span>';

			// start building link tooltip title
			$titleContent .= '<br />';

			// get LPs details and set them in the tooltip
			foreach ($itemsLPs as $lpItem) {
				//to set s link to LP uncomment next or second line and comment the third line - better use the third line
				//$titleContent .= CHtml::link($lpItem['path_name'], Yii::app()->urlManager->createUrl('CurriculaApp/curriculaManagement/courses', array('id' => $lpItem['id_path'])));
				//$titleContent .= '<u>' . $lpItem['path_name'] . '</u>';

				$titleContent .= CHtml::link($lpItem['path_name'],
					Docebo::createAdminUrl('CurriculaApp/curriculaManagement/courses',
						array('id' => $lpItem['id_path']))
				);

				$titleContent .= '<br />';
			}

		} else {
			//the course doesn't belongs to any LP - display dash
			$content = '<span class="center-aligned">&mdash;</span>';
		}

		// prepare output
		if ($countLPs > 0) {
			$output = CHtml::link($content, '#!', array(
				'title' => $titleContent,
				'rel' => 'tooltip',
				'data-html' => true,
				'class' => 'lps-list',
				'onclick' => 'javascript:void(0);'
			));
		} else {
			$output = CHtml::link($content, 'javascript:void(0);');
		}

		return $output;
	}

	static public function isCourseSelling($idCourse)
	{
		static $coursesForSale = null;

		if (!$coursesForSale) {
			$coursesForSale = Yii::app()->getDb()->createCommand()
				->select('idCourse')
				->from(self::model()->tableName())
				->where('selling=1')
				->queryColumn();
		}

		return in_array($idCourse, $coursesForSale);
	}

	/**
	 * Get details on the Learning Object marked as "Final bookmark"
	 * or the last one (ordered by iLeft DESC) inside the course
	 *
	 * @param $idCourse
	 *
	 * @return mixed
	 */
	static public function getFinalObjectforCourse($idCourse)
	{
		$params = array(
			':type_aicc' => LearningOrganization::OBJECT_TYPE_AICC,
			':type_scorm' => LearningOrganization::OBJECT_TYPE_SCORMORG,
			':type_test' => LearningOrganization::OBJECT_TYPE_TEST,
			':type_tincan' => LearningOrganization::OBJECT_TYPE_TINCAN,
			':type_deliverable' => LearningOrganization::OBJECT_TYPE_DELIVERABLE,
			':type_elucidat'	=> LearningOrganization::OBJECT_TYPE_ELUCIDAT,
			':type_lti'         => LearningOrganization::OBJECT_TYPE_LTI,
			':milestone'        => LearningOrganization::MILESTONE_END,
			':idCourse'=>$idCourse,
		);

		// Get all courses with their respective "last/final" Learning Object
		$courseMilestoneOrLastObject = Yii::app()->db->createCommand()
			->select('c.idCourse, COALESCE(loMilestone.idOrg, loGreatest.idOrg) idOrg, COALESCE(loMilestone.objectType, loGreatest.objectType) objectType, COALESCE(loMilestone.idResource, loGreatest.idResource) idResource, loMilestone.milestone')
			->from(LearningCourse::model()->tableName() . ' c')
			// Where the LO is marked as END OBJECT MARKER:
			->leftJoin(LearningOrganization::model()->tableName() . ' loMilestone',
				'c.idCourse=loMilestone.idCourse AND loMilestone.milestone=:milestone AND (loMilestone.objectType = :type_aicc OR loMilestone.objectType = :type_scorm OR loMilestone.objectType = :type_test OR loMilestone.objectType = :type_tincan OR loMilestone.objectType = :type_deliverable)')
			// Or just get the highest available LO of certain types (latest uploaded):
			->leftJoin('(
                                              SELECT idOrg, idResource, objectType, idCourse
                                              FROM learning_organization
                                              WHERE (objectType = :type_aicc OR objectType = :type_scorm OR objectType = :type_test OR objectType = :type_tincan OR objectType = :type_deliverable OR objectType = :type_elucidat OR objectType = :type_lti)
                                              	AND idCourse = :idCourse
                                              ORDER BY iLeft DESC
                                              LIMIT 1) loGreatest
                                                 ', 'c.idCourse=loGreatest.idCourse')
			// Only get courses with at least one LO:
			->where('COALESCE(loMilestone.idOrg, loGreatest.idOrg) IS NOT NULL')
			->andWhere('c.idCourse=:idCourse')
			// Only a single LO per course can be "final"/"last":
			->group('c.idCourse')
			->queryRow(true, $params);
		return $courseMilestoneOrLastObject;
	}

	static public function getFinalScoreMode($idCourse)
	{
		if (isset(self::$final_score_mode_cache[$idCourse])) {
			return self::$final_score_mode_cache[$idCourse];
		}

		$fsc = Yii::app()->getDb()->createCommand()
			->select('final_score_mode')
			->from(self::model()->tableName())
			->where('idCourse = :idCourse', array(':idCourse' => (int)$idCourse))
			->queryScalar();

		if (in_array($fsc, array_keys(self::getFinalScoreModesList()))) {
			self::$final_score_mode_cache[$idCourse] = $fsc;
		} else {
			self::$final_score_mode_cache[$idCourse] = self::FINAL_SCORE_TYPE_STANDARD;
		}

		return self::$final_score_mode_cache[$idCourse];
	}


	/**
	 * Build and array used to render a dropdown list for Course -> Enrollments filtering functionality.
	 *
	 * Returns an array of possible options, for example (2,3,4,5 are coaching session IDs):
	 *
	 *    [all] => All users
	 *        [unassigned_only] => Unassigned users only
	 *        [2015] => Array
	 *        (
	 *            [2] => From 6/10/2015 to 6/10/2015 - coach: puser1 puser1
	 *            [3] => From 6/10/2015 to 6/10/2015 - coach: Firstname001 Lastname001
	 *        )
	 *
	 *        [2016] => Array
	 *            (
	 *            [4] => From 6/10/2015 to 6/10/2015 - coach: Firstname005 Lastname005
	 *            [5] => From 6/10/2015 to 6/10/2015 - coach: Firstname008 Lastname008
	 *        )
	 *
	 * @param integer $idCourse
	 * @return array
	 */
	public function buildCoachingSessionFilteringArrayForEnrollments()
	{

		$result = array(
			self::COACHING_FILTER_ALL_USERS => Yii::t('standard', 'All users'),
			self::COACHING_FILTER_UNASSIGNED => Yii::t('coaching', 'Unassigned users only'),
		);

		$command = Yii::app()->db->createCommand()
			->select('s.*, coach.firstname AS cFirstName, coach.lastname AS cLastName, YEAR(datetime_start) y')
			->from('learning_course_coaching_session s')
			->join('core_user coach', 's.idCoach=coach.idst')
			->andWhere('s.idCourse=:idCourse')
			->order('y');

		$params = array(
			':idCourse' => $this->idCourse,
		);


		$dataReader = $command->query($params);
		foreach ($dataReader as $row) {
			$result[$row['y']][$row['idSession']] = Yii::t('coaching', 'From {from} to {to} - coach: {coach}', array(
				'{from}' => Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($row['datetime_start'])),
				'{to}' => Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($row['datetime_end'])),
				'{coach}' => $row['cFirstName'] . " " . $row['cLastName'],
			));
		}

		return $result;

	}

	/**
	 * List ALL users (IDs) assigned to ANY (or selected) coaching session in THIS course.
	 *
	 * Returns array of <user ID> -> <session ID>  pairs
	 *
	 * @return array
	 */
	public function getUsersAssignedToCourseCoachingSession($idSession = false, $flatList = true)
	{

		$command = Yii::app()->db->createCommand()
			->select('su.idUser as idUser, s.idSession as idSession')
			->from('learning_course c')
			->join('learning_course_coaching_session s', '(s.idCourse=c.idCourse) AND (s.idCourse=:idCourse)')
			->join('learning_course_coaching_session_user su', 'su.idSession=s.idSession');

		$params = array(
			':idCourse' => $this->idCourse,
		);

		// Filter by specific session
		if ($idSession !== false) {
			$command->andWhere('su.idSession=:idSession');
			$params[':idSession'] = (int)$idSession;
		}

		$dataReader = $command->query($params);
		$result = array();
		foreach ($dataReader as $row) {
			if ($flatList) {
				$result[] = $row['idUser'];
			} else {
				$result[$row['idUser']] = $row['idSession'];
			}
		}

		return $result;

	}


	/**
	 * Get coaching sessions of the given coach in THIS course
	 */
	public function getCoachSessions($idCoach, $returnModels = false)
	{

		$command = Yii::app()->db->createCommand()
			->select('s.idSession as idSession')
			->from('learning_course_coaching_session s')
			->andWhere('s.idCourse=:idCourse')
			->andWhere('s.idCoach=:idCoach');

		$params = array(
			':idCourse' => $this->idCourse,
			':idCoach' => $idCoach,
		);

		$dataReader = $command->query($params);
		$result = array();
		foreach ($dataReader as $row) {
			if ($returnModels) {
				$result[] = LearningCourseCoachingSession::model()->findByPk($row['idSession']);
			} else {
				$result[] = $row['idSession'];
			}
		}

		return $result;

	}


	/**
	 * Get available coaching sessions for a course (or all courses [default]), filtered by their status
	 *
	 * Using a standard class objec: filterObject which passes various filtering options, like:<br>
	 *
	 * <li>from/to dates<br>
	 * <li>ignore started/ended/future session<br>
	 * <li>search string<br>
	 * <li>ignore FULL sessions (no available seats)<br>
	 * <li>filter by COACH ID<br>
	 * <li>and many more can be added later<br>
	 * <br>
	 * Rturns a rich array of data about session(s), coach(es) and course(s)
	 *
	 * @param boolean|integer $idCourse
	 * @param boolean|stdClass $filterObject
	 * @param boolean|integer $pageSize
	 * @param boolean|string $select
	 *
	 * @return CSqlDataProvider
	 */
	public static function coachingSessionsSqlDataProvider(
		$idCourse = false,
		stdClass $filterObject = null,
		$pageSize = false,
		$select = false
	) {

		$pivotalDate = Yii::app()->localtime->getUTCNow('Y-m-d');

		// Start building SQL parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand()
			->from('learning_course_coaching_session s')
			->join('learning_course c', 'c.idCourse=s.idCourse')
			->join('core_user u', 'u.idst=s.idCoach');

		if ($idCourse !== false) {
			$commandBase->andWhere('s.idCourse = :idCourse');
			$params = array(':idCourse' => (int)$idCourse);
		}

		// Additional filtering
		if (get_class($filterObject) == 'stdClass') {

			// Should we use this date as point in time when comapring session dates ? (instead of CURRENT date)
			if ($filterObject->pivotalDate) {
				$pivotalDate = $filterObject->pivotalDate;
			}


			// FROM Date ?
			if ($filterObject->fromDate) {
				$params[':fromDate'] = $filterObject->fromDate;
				$commandBase->andWhere('(s.datetime_start  >= :fromDate)');
			}

			// TO Date ?
			if ($filterObject->toDate) {
				$params[':toDate'] = $filterObject->toDate;
				$commandBase->andWhere('(s.datetime_end  <= :toDate)');
			}

			// Ignore STARTED ?
			if ($filterObject->ignoreStarted) {
				$params[':date1'] = $pivotalDate;
				$commandBase->andWhere('(DATE(s.datetime_start)  > :date1)');
			}

			// Ignore ENDED ?
			if ($filterObject->ignoreEnded) {
				$params[':date2'] = $pivotalDate;
				$commandBase->andWhere('DATE(s.datetime_end) >= :date2');
			}

			// Ignore NOT YET STARTED ?
			if ($filterObject->ignoreFuture) {
				$params[':date3'] = $pivotalDate;
				$commandBase->andWhere('(DATE(s.datetime_start)  < :date3)');
			}

			// Should we ignore session NOT having free seats ?
			if ($filterObject->ignoreFull) {
				$commandBase->andWhere('((s.max_users IS NULL) OR (s.max_users=0)) OR (s.max_users > (SELECT count(su.idSession) FROM learning_course_coaching_session_user su WHERE su.idSession=s.idSession))');
			}
			//Show sessions with enough available seats for merging
			if ($filterObject->minAvailableSeats) {
				$commandBase->andWhere('((s.max_users IS NULL) OR (s.max_users=0))  OR ((s.max_users - (SELECT count(su.idSession) FROM learning_course_coaching_session_user su WHERE su.idSession=s.idSession)) >= :minAvailableSeats)');
				$params[':minAvailableSeats'] = $filterObject->minAvailableSeats;
			}
			if ($filterObject->excludedSessions && is_array($filterObject->excludedSessions)) {
				$commandBase->andWhere('s.idSession NOT IN (' . implode($filterObject->excludedSessions, ',') . ')');
			}

			// Filter by COACH ID?
			if ($filterObject->idCoach) {
				if (!is_array($filterObject->idCoach)) {
					$filterObject->idCoach = array($filterObject->idCoach);
				}
				$commandBase->andWhere('s.idCoach IN (' . implode(',', $filterObject->idCoach) . ')');
			}

			// Filter by a string (like coach name, course name, etc)
			if ($filterObject->search_input) {
				$params[':search'] = '%' . $filterObject->search_input . '%';
				$cond = "CONCAT(COALESCE(u.userid,''), ' ', COALESCE(u.firstname,''), ' ', COALESCE(u.lastname,''), ' ', COALESCE(c.name,''), ' ', COALESCE(s.idSession,'')) LIKE :search";
				$commandBase->andWhere($cond);
			}


		}

		// DATA
		$commandData = clone $commandBase;

		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(s.idSession) AS C');
		$numRecords = $commandCounter->queryScalar($params);

		// DATA-II
		if ($select !== false) {
			$commandData->select("s.idSession AS key_idSession, " . $select);
		} else {
			$commandData->select("s.idSession AS idSession, s.*, u.*, c.*");
		}

		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'startDate' => array(
				'asc' => 's.datetime_start ASC',
				'desc' => 's.datetime_start DESC',
			),
			'name' => array(
				'asc' => 'u.userid',
				'desc' => 'u.userid DESC',
			),
		);
		$sort->defaultOrder = array(
			'startDate' => CSort::SORT_ASC,
		);

		// END
		$pagination = array();
		if ($pageSize) {
			$pagination = array('pageSize' => $pageSize);
		}

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => $pagination,
			'keyField' => 'key_idSession',
			'sort' => $sort,
			'params' => $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData->gettext(), $config);

		return $dataProvider;


	}


	/**
	 * Return list of coaching session available for learners in a given course (or all courses)
	 *
	 *        - ended sessions are ignored
	 *        - started sessions are ignored
	 *
	 * @param integer $idCourse
	 * @return array
	 */
	public static function getAvailableCoachingSessions($idCourse = false)
	{

		$filterObject = new stdClass();
		$filterObject->ignoreEnded = true;
		$filterObject->ignoreFull = true;

		$dp = self::coachingSessionsSqlDataProvider($idCourse, $filterObject);

		$result = array();
		foreach ($dp->data as $row) {
			$result[] = $row['idSession'];
		}
		return $result;

	}


	/**
	 * Return list of coaches unavailable for a session in the dates(from-to)
	 *
	 * Usually for a course
	 *
	 * @param string $startDate
	 * @param string $endDate
	 * @param integer $idCourse
	 * @return array
	 */
	public static function getUnavailableCoachesByDates(
		$idCourse = false,
		$startDate = false,
		$endDate = false,
		$currentSession = false
	) {
		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser courseUser')
			->join('core_user user', 'user.idst=courseUser.idUser')
			->leftJoin('learning_course_coaching_session s', 'user.idst=s.idCoach')
			->andWhere('courseUser.level = :userLevel')
			->group('idst')
			->select('idst');
		$params = array(
			':userLevel' => LearningCourseuser::$USER_SUBSCR_LEVEL_COACH
		);
		if ($idCourse) {
			$commandBase->andWhere('courseUser.idCourse = :idCourse');
			$commandBase->andWhere('s.idCourse = :idCourse');
			$params[':idCourse'] = $idCourse;
		}
		if ($startDate) {
			$commandBase->andWhere('datetime_end>=:startDate');
			$params[':startDate'] = $startDate;
		}
		if ($endDate) {
			$commandBase->andWhere('datetime_start<=:endDate');
			$params[':endDate'] = $endDate;
		}
		if ($currentSession) {
			$commandBase->andWhere('idSession != :currentSession');
			$params[':currentSession'] = $currentSession;
		}
		$sqlResult = $commandBase->query($params);
		$result = array();
		for ($i = 0; $i < $sqlResult->count(); $i++) {
			$sqlResult->next();
			$row = $sqlResult->current();
			$result[] = $row['idst'];
		}
		return $result;
	}


	/**
	 * Return list of all coaching session in a given course (or all courses)
	 *
	 *        - ended sessions are not ignored
	 *        - started sessions are not ignored
	 *
	 * @param integer $idCourse
	 * @return array
	 */
	public static function getAllCoachingSessions($idCourse = false)
	{

		$filterObject = new stdClass();
		$filterObject->ignoreEnded = false;
		$filterObject->ignoreFull = false;

		$dp = self::coachingSessionsSqlDataProvider($idCourse, $filterObject);

		$result = array();
		foreach ($dp->data as $row) {
			$result[] = $row['idSession'];
		}
		return $result;
	}


	/**
	 * Returns the deadline (soft or not) for the passed user in the current course
	 *
	 * @param $userId The id of the user
	 * @param $onlyIfExpired Returns a deadline only if it has expired
	 *
	 * @return the deadline (if any)
	 */
	public function getDeadlineForUser($userId, $onlyIfExpired = false)
	{
		// Check user enrollment
		$learningCourseUser = LearningCourseuser::model()->findByAttributes(array(
			'idCourse' => $this->idCourse,
			'idUser' => $userId
		));
		if ($learningCourseUser && ($learningCourseUser->status != LearningCourseuser::$COURSE_USER_END)) {
			$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

			// Time to check for datetimes (first at the enrollment level)
			if ($learningCourseUser->date_expire_validity && (strpos($learningCourseUser->date_expire_validity,
						'0000-00-00') === false)
			) {
				$utcDateExpireValidity = str_replace(array("T", "+0000"), array(" ", ""),
					Yii::app()->localtime->fromLocalDateTime($learningCourseUser->date_expire_validity));
				if (!$onlyIfExpired || ($nowTimestamp > strtotime($utcDateExpireValidity))) {
					return $learningCourseUser->date_expire_validity;
				}
			}

			// ... and then at the course level)
			if ($this->date_end && ($this->date_end != '0000-00-00')) {
				$date = Yii::app()->localtime->fromLocalDate($this->date_end);
				$time_end = strtotime($date . ' 23:59:59');
				if (!$onlyIfExpired || ($time_end < $nowTimestamp)) {
					return Yii::app()->localtime->toLocalDateTime($date . ' 23:59:59');
				}
			}
		}

		return null;
	}


	/**
	 * @return string
	 */
	public static function getAllIdCoursesList()
	{
		$reader = Yii::app()->db->createCommand()
			->select('idCourse')
			->from(LearningCourse::model()->tableName())
			->query();
		$first = true;
		$output = '';
		while ($record = $reader->read()) {
			if ($first) {
				$first = false;
			} else {
				$output .= ',';
			}
			$output .= $record['idCourse'];
		}
		return $output;
	}


	/**
	 * Return list of Course IDs of courses marked for selling
	 */
	public static function getItemsForSale()
	{
		$result = Yii::app()->db->createCommand()
			->select('idCourse')
			->from(self::model()->tableName())
			->where('selling > 0')
			->queryColumn();

		return $result;
	}

	/**
	 * getCourseFields() - Returns all additional course fields models
	 * @param bool $ignore_custom - whatever to ignore custom course fields
	 * @return array|mixed|null
	 */
	public function getCourseAdditionalFields($ignore_custom = false, $visibilityFilter = false)
	{
		if ($ignore_custom) {
			$in_array = "'" . implode("','", LearningCourseField::getCourseFieldsTypes()) . "'";
			$condition = "`type` IN (" . $in_array . ")";
			if ($visibilityFilter) {
				$condition .= " AND  `invisible_to_user` = 1";
			}
			return LearningCourseField::model()->findAllByAttributes(
				array(),
				array(
					'condition' => $condition,
					'order' => 'sequence ASC'
				)
			);
		} else {
			$condition = "";
			if ($visibilityFilter) {
				$condition .= "`invisible_to_user` = 1";
			}
			return LearningCourseField::model()->findAllByAttributes(
				array(),
				array(
					'condition' => $condition
				,
					'order' => 'sequence ASC'
				)
			);
		}
	}

	/**
	 * Returns the value of the field for this course (NO MODELS)
	 * @param $fieldId - Id of the course field
	 * @param $courseId - Optional id of the course to take it's value
	 * @param $dropdownKeyInsteadOfValue - Determines whether we want the key instead of the value of a dropdown field
	 */
	public function getAdditionalFieldValue($fieldId, $courseId = false, $dropdownKeyInsteadOfValue = false)
	{
	    
	    static $cache;
	    $cacheKey = md5(serialize(func_get_args()));
	    
	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }
	    
		$fieldType = Yii::app()->db->createCommand()
			->select('type')
			->from(LearningCourseField::model()->tableName())
			->where('id_field = ' . ((int)$fieldId))
			->queryScalar();

		$cid = ($courseId) ? $courseId : $this->idCourse;
		$result = Yii::app()->db->createCommand()
			->select('field_' . $fieldId . ' v')
			->from(LearningCourseFieldValue::model()->tableName() . ' fv')
			->where('id_course = ' . $cid);

		$result = $result->queryScalar();
		if ($fieldType == LearningCourseField::TYPE_FIELD_DROPDOWN && $result) {
			if ($dropdownKeyInsteadOfValue) {
			    $cache[$cacheKey] = $result;
				return $result;
			} else {
				$translation = Yii::app()->db->createCommand()
					->select('translation')
					->from(LearningCourseFieldDropdownTranslations::model()->tableName())
					->where('id_option = ' . $result . ' AND lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"')
					->queryScalar();
				if (!$translation) {
					$translation = Yii::app()->db->createCommand()
						->select('translation')
						->from(LearningCourseFieldDropdownTranslations::model()->tableName())
						->where('id_option = ' . $result . ' AND lang_code = "' . Settings::get('default_language') . '"')
						->queryScalar();
				}

				$result = $translation;
			}
		}
		
		$cache[$cacheKey] = $result;
		
		return $result;
	}

	/**
	 * @return array where each key is the idCourse of a classroom course
	 *               and the value is an array of user IDs that are enrolled
	 *               in some sessions in this course
	 */
	static private function _getClassroomCoursesAndUsersInSessions()
	{
		Yii::app()->getDb()->createCommand('SET SESSION group_concat_max_len = 10000000;')->execute();
		$query = Yii::app()->getDb()->createCommand()
			->select('c.idCourse, GROUP_CONCAT(DISTINCT su.id_user) usersInClassroomCourse')
			->from('learning_course c')
			->join('lt_course_session s', 's.course_id=c.idCourse')
			->join('lt_courseuser_session su', 's.id_session=su.id_session')
			->group('c.idCourse')
			->queryAll();

		$res = array();
		foreach ($query as $row) {
			$res[$row['idCourse']] = explode(',', $row['usersInClassroomCourse']);
		}
		return $res;
	}

	static private function _getWebinarCoursesAndUsersInSessions()
	{
		Yii::app()->getDb()->createCommand('SET SESSION group_concat_max_len = 10000000;')->execute();
		$query = Yii::app()->getDb()->createCommand()
			->select('c.idCourse, GROUP_CONCAT(DISTINCT su.id_user) usersInWebinarCourse')
			->from('learning_course c')
			->join('webinar_session s', 's.course_id=c.idCourse')
			->join('webinar_session_user su', 's.id_session=su.id_session')
			->group('c.idCourse')
			->queryAll();

		$res = array();
		foreach ($query as $row) {
			$res[$row['idCourse']] = explode(',', $row['usersInWebinarCourse']);
		}
		return $res;
	}

	/**
	 * Retrieves an array of courses and the number of waiting users
	 * Courses with no enrolled users or no waiting users will be excluded
	 *
	 * @param bool $includeElearning
	 * @return array Each element is an array with the following values:
	 *               ---
	 *               idCourse => integer
	 *               name => string (name of the course)
	 *               waiting => Number of users enrolled but not part of any session)
	 *               ---
	 *               The array will be ordered in descending order by the number of "waiting" users
	 *
	 * @throws \CDbException
	 */
	static public function getCoursesWithAllKindOfWaitingUsers($includeElearning = false)
	{
		$res = array();
		$params = array();
		//Get courses with waiting users for elearning courses
		if ($includeElearning) {
			$command = Yii::app()->db->createCommand()
				->select('c.idCourse, c.name, count(*) as waiting, c.course_type')
				->from(LearningCourse::model()->tableName() . ' as c')
				->join(LearningCourseuser::model()->tableName() . ' as cu', 'cu.idCourse = c.idCourse')
				->join(CoreUser::model()->tableName() . ' as u', 'u.idst = cu.idUser')
				->where('cu.waiting = 1 and c.course_type = :elearning')
				->group('c.idCourse');
			$params[':elearning'] = LearningCourse::TYPE_ELEARNING;

			if (Yii::app()->user->getIsPu()) {
				$command->join(CoreUserPuCourse::model()->tableName() . ' as puc',
					'puc.course_id = cu.idCourse AND puc.puser_id = :puser_id');
				$command->join(CoreUserPU::model()->tableName() . ' as puu',
					'puu.user_id = cu.idUser AND puu.puser_id = :puser_id');
				$params[':puser_id'] = Yii::app()->user->getIdst();
			}

			$reader = $command->query($params);

			if ($reader) {
				while ($record = $reader->read()) {
					$res[$record['idCourse']] = array(
						'idCourse' => (int)$record['idCourse'],
						'name' => $record['name'],
						'course_type' => $record['course_type'],
						'waiting' => (int)$record['waiting']
					);
				}
			}
		}

		//Get course with waiting users for webinar course
		$command = Yii::app()->db->createCommand()
			->select('c.idCourse, c.name, count(*) as waiting, c.course_type')
			->from(LearningCourse::model()->tableName() . ' as c')
			->join(LearningCourseuser::model()->tableName() . ' as cu', 'cu.idCourse = c.idCourse')
			->join(CoreUser::model()->tableName() . ' as u', 'u.idst = cu.idUser')
			->where('c.course_type = :webinar and cu.idUser not in (
						select distinct(wu.id_user) from ' . WebinarSession::model()->tableName() . ' as w
						join ' . WebinarSessionUser::model()->tableName() . ' as wu on wu.id_session = w.id_session
						where cu.idCourse = w.course_id
						)')
			->group('c.idCourse');

		$params = array();
		$params[':webinar'] = LearningCourse::TYPE_WEBINAR;

		if (Yii::app()->user->getIsPu()) {
			$command->join(CoreUserPuCourse::model()->tableName() . ' as puc',
				'puc.course_id = cu.idCourse AND puc.puser_id = :puser_id');
			$command->join(CoreUserPU::model()->tableName() . ' as puu',
				'puu.user_id = cu.idUser AND puu.puser_id = :puser_id');
			$params[':puser_id'] = Yii::app()->user->getIdst();
		}

		$reader = $command->query($params);

		if ($reader) {
			while ($record = $reader->read()) {
				$res[$record['idCourse']] = array(
					'idCourse' => (int)$record['idCourse'],
					'name' => $record['name'],
					'course_type' => $record['course_type'],
					'waiting' => (int)$record['waiting']
				);
			}
		}

		//Get courses with waiting users for classroom courses
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$command = Yii::app()->db->createCommand()
				->select('c.idCourse, c.name, count(*) as waiting, c.course_type')
				->from(LearningCourse::model()->tableName() . ' as c')
				->join(LearningCourseuser::model()->tableName() . ' as cu', 'cu.idCourse = c.idCourse')
				->join(CoreUser::model()->tableName() . ' as u', 'u.idst = cu.idUser')
				->where('c.course_type = :classroom and cu.idUser not in (
							select distinct(ltu.id_user) from ' . LtCourseSession::model()->tableName() . ' as lt
							join ' . LtCourseuserSession::model()->tableName() . ' as ltu on ltu.id_session = lt.id_session
							where cu.idCourse = lt.course_id
							)')
				->group('c.idCourse');

			$params = array();
			$params[':classroom'] = LearningCourse::TYPE_CLASSROOM;

			if (Yii::app()->user->getIsPu()) {
				$command->join(CoreUserPuCourse::model()->tableName() . ' as puc',
					'puc.course_id = cu.idCourse AND puc.puser_id = :puser_id');
				$command->join(CoreUserPU::model()->tableName() . ' as puu',
					'puu.user_id = cu.idUser AND puu.puser_id = :puser_id');
				$params[':puser_id'] = Yii::app()->user->getIdst();
			}

			$reader = $command->query($params);

			if ($reader) {
				while ($record = $reader->read()) {
					$res[$record['idCourse']] = array(
						'idCourse' => (int)$record['idCourse'],
						'name' => $record['name'],
						'course_type' => $record['course_type'],
						'waiting' => (int)$record['waiting']
					);
				}
			}
		}

		//Search for webinar waiting users into a session
		$command = Yii::app()->db->createCommand()
			->select('c.idCourse, c.name, count(*) as waiting, c.course_type')
			->from(LearningCourse::model()->tableName() . ' as c')
			->join(LearningCourseuser::model()->tableName() . ' as cu', 'cu.idCourse = c.idCourse')
			->join(CoreUser::model()->tableName() . ' as u', 'u.idst = cu.idUser')
			->join(WebinarSession::model()->tableName() . ' as w', 'w.course_id = c.idCourse')
			->join(WebinarSessionUser::model()->tableName() . ' as wu',
				'wu.id_user = cu.idUser and wu.id_session = w.id_session')
			->where('wu.status = :waitingStatus')
			->group('c.idCourse');

		$params = array();
		$params[':waitingStatus'] = WebinarSessionUser::$SESSION_USER_WAITING_LIST;

		if (Yii::app()->user->getIsPu()) {
			$command->join(CoreUserPuCourse::model()->tableName() . ' as puc',
				'puc.course_id = cu.idCourse AND puc.puser_id = :puser_id');
			$command->join(CoreUserPU::model()->tableName() . ' as puu',
				'puu.user_id = cu.idUser AND puu.puser_id = :puser_id');
			$params[':puser_id'] = Yii::app()->user->getIdst();
		}

		$reader = $command->query($params);

		if ($reader) {
			while ($record = $reader->read()) {
				if (isset($res[$record['idCourse']])) {
					$res[$record['idCourse']]['waiting'] += (int)$record['waiting'];
				} else {
					$res[$record['idCourse']] = array(
						'idCourse' => (int)$record['idCourse'],
						'name' => $record['name'],
						'course_type' => $record['course_type'],
						'waiting' => (int)$record['waiting']
					);
				}
			}
		}

		//Search for classroom waiting users into a session
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$command = Yii::app()->db->createCommand()
				->select('c.idCourse, c.name, count(*) as waiting, c.course_type')
				->from(LearningCourse::model()->tableName() . ' as c')
				->join(LearningCourseuser::model()->tableName() . ' as cu', 'cu.idCourse = c.idCourse')
				->join(CoreUser::model()->tableName() . ' as u', 'u.idst = cu.idUser')
				->join(LtCourseSession::model()->tableName() . ' as lt', 'lt.course_id = c.idCourse')
				->join(LtCourseuserSession::model()->tableName() . ' as ltu',
					'ltu.id_user = cu.idUser and ltu.id_session = lt.id_session')
				->where('ltu.status = :waitingStatus')
				->group('c.idCourse');

			$params = array();
			$params[':waitingStatus'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

			if (Yii::app()->user->getIsPu()) {
				$command->join(CoreUserPuCourse::model()->tableName() . ' as puc',
					'puc.course_id = cu.idCourse AND puc.puser_id = :puser_id');
				$command->join(CoreUserPU::model()->tableName() . ' as puu',
					'puu.user_id = cu.idUser AND puu.puser_id = :puser_id');
				$params[':puser_id'] = Yii::app()->user->getIdst();
			}

			$reader = $command->query($params);

			if ($reader) {
				while ($record = $reader->read()) {
					if (isset($res[$record['idCourse']])) {
						$res[$record['idCourse']]['waiting'] += (int)$record['waiting'];
					} else {
						$res[$record['idCourse']] = array(
							'idCourse' => (int)$record['idCourse'],
							'name' => $record['name'],
							'course_type' => $record['course_type'],
							'waiting' => (int)$record['waiting']
						);
					}
				}
			}
		}

		usort($res, function ($a, $b) {
			return $b['waiting'] - $a['waiting'];
		});

		return $res;
	}

	/**
	 * Retrieves an array of courses and the number of waiting users
	 * Courses with no enrolled users or no waiting users will be excluded
	 *
	 * @param bool $includeElearning
	 * @return array Each element is an array with the following values:
	 *               ---
	 *               idCourse => integer
	 *               name => string (name of the course)
	 *               waiting => Number of users enrolled but not part of any session)
	 *               ---
	 *               The array will be ordered in descending order by the number of "waiting" users
	 *
	 * @throws \CDbException
	 */
	static public function getCoursesWithWaitingUsers($includeElearning = false)
	{

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		/* @var $command CDbCommand */
		$params = array();
		//build query
		$command = $db->createCommand()
			->select('cu.idCourse, c.name, c.course_type, cu.idUser')
			->from(LearningCourseuser::model()->tableName() . ' cu')
			->join(LearningCourse::model()->tableName() . ' c', 'c.idCourse=cu.idCourse')
			->join(CoreUser::model()->tableName() . ' u', 'u.idst=cu.idUser')
			->group('cu.idUser, cu.idCourse');
		$elearningCondition = '';
		if ($includeElearning) {
			$elearningCondition = ' OR (c.course_type = :elearning AND cu.waiting = 1)';
			$params[':elearning'] = LearningCourse::TYPE_ELEARNING;
		}
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$command->where('c.course_type=:classroom OR c.course_type=:webinar' . $elearningCondition);
			$params[':classroom'] = LearningCourse::TYPE_CLASSROOM;
			$params[':webinar'] = LearningCourse::TYPE_WEBINAR;
		} else {
			$command->where('c.course_type=:webinar' . $elearningCondition);
			$params[':webinar'] = LearningCourse::TYPE_WEBINAR;
		}
		//power user filters
		if (Yii::app()->user->getIsPu()) {
			$command->join('core_user_pu_course puc', 'puc.course_id=cu.idCourse AND puc.puser_id=:puser_id');
			$command->join('core_user_pu puu', 'puu.user_id=cu.idUser AND puu.puser_id=:puser_id');
			$params[':puser_id'] = Yii::app()->user->getIdst();
		}
		$reader = $command->query($params);

		//retrieve and store data from DB
		$coursesWithEnrolledUsers = array();
		if ($reader) {
			while ($record = $reader->read()) {
				$index = $record['idCourse'];
				if (!isset($coursesWithEnrolledUsers[$index])) {
					//make sure that the course row exists
					$coursesWithEnrolledUsers[$record['idCourse']] = array(
						'idCourse' => (int)$record['idCourse'],
						'name' => $record['name'],
						'course_type' => $record['course_type'],
						'enrolled' => array(), //just initialized, will be filled later
						'waiting' => 0 //as above
					);
				}
				//fill enrolled users
				$coursesWithEnrolledUsers[$index]['enrolled'][] = (int)$record['idUser'];
			}
		}

		$coursesWithWaitingUsers = array();

		foreach ($coursesWithEnrolledUsers as $singleCourse) {
			$cnt = 0;

			switch ($singleCourse['course_type']) {
				case LearningCourse::TYPE_CLASSROOM:
					$course = LearningCourse::model()->findByPk($singleCourse['idCourse']);
					$waitingForSessions = $course->countWaitingForAllIltSessions();
					$cnt = $course->getUnassignedUsersCount() + ((is_int($waitingForSessions) && $waitingForSessions > 0) ? $waitingForSessions : 0);
					break;
				case LearningCourse::TYPE_WEBINAR:
					$course = LearningCourse::model()->findByPk($singleCourse['idCourse']);
					$waitingForSessions = $course->countWaitingForAllWebinarSessions();
					$cnt = $course->getUnassignedWebinarUsersCount() + ((is_int($waitingForSessions) && $waitingForSessions > 0) ? $waitingForSessions : 0);
					break;
				case LearningCourse::TYPE_ELEARNING:
					$cnt = count($singleCourse['enrolled']);
					break;
			}

			if ($cnt > 0) {
				$singleCourse['waiting'] = $cnt;
				$coursesWithWaitingUsers[] = $singleCourse;
			}
		}

		// Sort the courses by the number of "waiting" users, descending
		usort($coursesWithWaitingUsers, function ($a, $b) {
			return $b['waiting'] - $a['waiting'];
		});

		return $coursesWithWaitingUsers;
	}

	/**
	 * @param $courseId
	 * @param $userId
	 * @return bool
	 */
	public static function isUserAssignedToInstructorSessions($courseId, $userId)
	{
		$instructorUsers = self::getInstructorUsers($courseId, false);

		if (in_array($userId, $instructorUsers)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * get all users, assigned to instructor sessions
	 * @param $courseId
	 * @param bool $skipAdmins skip superadmin users and power users with assigned course
	 * @return array
	 */


	public static function getInstructorUsers($courseId, $skipAdmins = true)
	{
		$pUserRights = Yii::app()->user->checkPURights($courseId);

		//godadmin OR power user with assigned course
		if ($skipAdmins && (Yii::app()->user->getIsGodadmin() || $pUserRights->all)) {
			return array();
		}

		$instructorId = Yii::app()->user->id;
		$instructorUsers = array();
		$course = LearningCourse::model()->findByPk($courseId);

		if ($pUserRights->isInstructor) {
			if ($course) {
				switch ($course->course_type) {
					case LearningCourse::TYPE_CLASSROOM :
						$instructorUsers = Yii::app()->db->createCommand()
							->select('id_user')
							->from(LtCourseSession::model()->tableName() . ' cs')
							->join(LtCourseuserSession::model()->tableName() . ' cus', 'cus.id_session = cs.id_session')
							->join('(SELECT id_session FROM lt_courseuser_session WHERE id_user = :instructorId) instructorSessions',
								'instructorSessions.id_session = cus.id_session',
								array(':instructorId' => $instructorId))
							->where('cs.course_id = :idCourse', array(':idCourse' => $courseId))
							->queryColumn();
						break;
					case LearningCourse::TYPE_WEBINAR :
						$instructorUsers = Yii::app()->db->createCommand()
							->select('id_user')
							->from(WebinarSession::model()->tableName() . ' ws')
							->join(WebinarSessionUser::model()->tableName() . ' wsu', 'wsu.id_session = ws.id_session')
							->join('(SELECT id_session FROM webinar_session_user WHERE id_user = :instructorId) instructorSessions',
								'instructorSessions.id_session = wsu.id_session',
								array(':instructorId' => $instructorId))
							->where('ws.course_id = :idCourse', array(':idCourse' => $courseId))
							->queryColumn();
						break;
					default:
						$instructorUsers = Yii::app()->db->createCommand()
							->select('idUser')
							->from(LearningCourseuser::model()->tableName() . ' cu')
							->where('cu.idCourse = :idCourse', array(':idCourse' => $courseId))
							->queryColumn();
						break;
				}
			}
		}

		return $instructorUsers;
	}

	/**Returns whether the course has learning objects which can complete it .
	 * @return bool
	 */
	public function  hasEndObjectLo()
	{
		$objects = LearningObjectsManager::getCourseLearningObjects($this->idCourse);
		foreach ($objects as $obj) {
			if (isset($obj['isTerminator']) && $obj['isTerminator'] == 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param integer $uid
	 */
	public function getDeepLink($uid = false)
	{
		$genById = (!$uid) ? Yii::app()->user->id : (int)$uid;

		$key = Yii::app()->params['deeplink_secret_key'];
		$hash = hash('sha1', $this->idCourse . "," . $genById . "," . $key);
		$deepLink = Docebo::createAbsoluteLmsUrl('course/deeplink', array(
			'course_id' => $this->idCourse,
			'generated_by' => $genById,
			'hash' => $hash
		));
		return $deepLink;
	}

	/**
	 * Get all categories, translated on the current LMS language
	 * @return array
	 */
	public function getAllCategoriesTitlesWithoutRoot()
	{
		$catTitles = array();

		if ($this->coursesCategoryTree) {
			if (!$this->coursesCategoryTree->isRoot()) {
				$catTitles[] = $this->coursesCategoryTree->coursesCategoryTranslated->translation;
			}

			$parentCats = $this->coursesCategoryTree->ancestors()->findAll();
			foreach ($parentCats as $cat) {
				if (!$cat->isRoot()) {
					$catTitles[] = $cat->coursesCategoryTranslated->translation;
				}
			}

		}

		return $catTitles;
	}

	public static function getCoursesIdsVisibleInExternalCatalog()
	{
		return Yii::app()->db->createCommand()
			->select('idCourse')->from(LearningCourse::model()->tableName())
			->where('show_rules=:all', array(':all' => self::DISPLAY_EVERYONE_HOMEPAGE))->queryColumn();
	}


	/**
	 * Counts the users enrolled in the classroom course sessions, but their status is 'waiting'
	 * @return integer
	 */
	public function countWaitingForAllIltSessions()
	{
		$result = 0;
		if ($this->course_type == self::TYPE_CLASSROOM) {
			$sessionsIds = array();
			$sessions = LtCourseSession::model()->findAllByAttributes(array('course_id' => $this->idCourse));
			foreach ($sessions as $session) {
				$sessionsIds [$session->id_session] = $session->id_session;
			}

			$params = array(
				':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST
			);

			$query = "SELECT COUNT(*) AS num_waiting "
				. " FROM " . LtCourseuserSession::model()->tableName() . " cus ";
			if (Yii::app()->user->getIsPu()) {
				$query .= " JOIN " . CoreUserPU::model()->tableName() . " pu ON (pu.puser_id = :puser_id AND cus.id_user = pu.user_id)";
				$params[':puser_id'] = Yii::app()->user->id;
			}

			if (is_array($sessionsIds) && $sessionsIds) {
				$query .= " WHERE cus.id_session IN (" . implode(',', $sessionsIds) . ") AND cus.status = :status";
				$result = Yii::app()->db->createCommand($query)->queryScalar($params);
			}
		}

		return (int)$result;
	}

	/**
	 * Counts the users enrolled to the webinar course sessions, but their status is 'waiting'
	 * @return integer
	 */
	public function countWaitingForAllWebinarSessions()
	{
		$result = 0;
		if ($this->course_type == self::TYPE_WEBINAR) {
			$sessionsIds = array();
			$sessions = WebinarSession::model()->findAllByAttributes(array('course_id' => $this->idCourse));
			foreach ($sessions as $session) {
				$sessionsIds [$session->id_session] = $session->id_session;
			}

			$params = array(
				':status' => WebinarSession::$SESSION_USER_WAITING_LIST
			);

			$query = "SELECT COUNT(*) AS num_waiting "
				. " FROM " . WebinarSessionUser::model()->tableName() . " wsu ";
			if (Yii::app()->user->getIsPu()) {
				$query .= " JOIN " . CoreUserPU::model()->tableName() . " pu ON (pu.puser_id = :puser_id AND wsu.id_user = pu.user_id)";
				$params[':puser_id'] = Yii::app()->user->id;
			}

			if (is_array($sessionsIds) && $sessionsIds) {
				$query .= " WHERE wsu.id_session IN (" . implode(',', $sessionsIds) . ") AND wsu.status = :status";
				$result = Yii::app()->db->createCommand($query)->queryScalar($params);
			}
		}

		return (int)$result;
	}

	public static function courseDetailsIsVisibleForUser($idCourse)
	{
		if (LearningCourseuser::model()->isSubscribed(Yii::app()->user->getIdst(), $idCourse)) {
			return true;
		}

		$coursesToNotDisplayList = LearningCourse::model()->getCoursesToNotDisplay();

		$coursesExcludedByPlugins = array();
		Yii::app()->event->raise('OnCatalogQueryCourses',
			new DEvent(self::model(), array('excludedCourses' => &$coursesExcludedByPlugins)));
		if (!empty($coursesExcludedByPlugins)) {
			$coursesToNotDisplayList = array_merge($coursesToNotDisplayList, $coursesExcludedByPlugins);
		}

		if (array_search($idCourse, $coursesToNotDisplayList) !== false) {
			return false;
		}

		$showAllCoursesIfNoCatalogueIsAssigned = ('on' == Settings::get('on_catalogue_empty', 'on'));
		$idUser = Yii::app()->user->getIdst();

		// Load the list of catalogs this use can see
		$userCatalogs = DashletCatalog::getUserAvailableCatalogs(false);
		$userAllCatalogs = DashletCatalog::getUserAvailableCatalogs(true, true);

		if (empty($userCatalogs) || (is_array($userCatalogs) && count($userCatalogs) == 1 && isset($userCatalogs['all']))) {
			if (!$showAllCoursesIfNoCatalogueIsAssigned || (count($userAllCatalogs) > 1)) {
				return false;
			} else {
				return true;
			}
		} else {
			$command = Yii::app()->db->createCommand();
			$command->select('
					t.idEntry AS id,
					t.type_of_entry AS catalog_item_type
				')
				->from(LearningCatalogueEntry::model()->tableName() . ' t');

			$courseOnCondition = 'course.status IN (:effective, :available)';
			$coursePathOnCondition = 'coursepath.visible_in_catalog=1';
			$command->params[':effective'] = LearningCourse::$COURSE_STATUS_EFFECTIVE;
			$command->params[':available'] = LearningCourse::$COURSE_STATUS_AVAILABLE;
			$command->andWhere(array('IN', 't.idCatalogue', array_keys($userCatalogs)));
			$command->join(LearningCatalogue::model()->tableName() . ' cat', 'cat.idCatalogue = t.idCatalogue')
				->leftJoin(LearningCourse::model()->tableName() . ' course',
					't.type_of_entry="course" AND t.idEntry=course.idCourse AND ' . $courseOnCondition)
				->leftJoin(LearningCoursepath::model()->tableName() . ' coursepath',
					't.type_of_entry="coursepath" AND t.idEntry=coursepath.id_path AND ' . $coursePathOnCondition)
				->andWhere('course.idCourse IS NOT NULL OR coursepath.id_path IS NOT NULL');

			$courses = array();

			$records = $command->queryAll();

			foreach ($records as $data) {
				if ($data['catalog_item_type'] == LearningCatalogueEntry::ENTRY_COURSE) {
					$courses[] = $data['id'];
				} elseif ($data['catalog_item_type'] == LearningCatalogueEntry::ENTRY_COURSEPATH) {
					$courses = array_merge($courses, LearningCoursepath::getCoursesOfLP($data['id']));
				}
			}

			unset($records);

			if (array_search($idCourse, $courses) === false) {
				return false;
			}
		}

		return true;
	}



	/**
	 * @param bool $onlyEnrollments - when true, returns only courses, in which the user is enrolled
	 * @return array - list of course ids, which are equivalent to current course
	 */
	public function getEquivalentCoursesIds($onlyEnrollments = false, $userId = false, $completedOnly = true){
		$command = Yii::app()->db->createCommand()
			->select('target_course_id')
			->from(EquivalentCourse::model()->tableName())
			->where('source_course_id=:source', array(':source' => $this->idCourse));
		if ($onlyEnrollments === true) {
			// get not yet finished courses ids for the current user
			// and filter the result by them
			$enrollmentsCommand = Yii::app()->db->createCommand()
				->select('idCourse')
				->from(LearningCourseuser::model()->tableName())
				->where('idUser=:user', array(':user' => (($userId)? (int)$userId : Yii::app()->user->id)));
			if($completedOnly) {
				$enrollmentsCommand->andWhere('status!=:completed', array(':completed' => LearningCourseuser::$COURSE_USER_END));
			}
			$userEnrollments = $enrollmentsCommand->queryColumn();
			$command->andWhere(array('IN', 'target_course_id', $userEnrollments));
		}

		$result = $command->queryColumn();
		return $result;
	}



	/**
	 * @param bool|false $onlyEnrollments
	 * @return mixed
	 */
	public function getEquivalentSourceCoursesIds($onlyEnrollments = false, $userId = false, $completedOnly = true){
		$command = Yii::app()->db->createCommand()
				->select('source_course_id')
				->from(EquivalentCourse::model()->tableName())
				->where('target_course_id=:target', array(':target' => $this->idCourse));

		if ($onlyEnrollments === true) {
			// get not yet finished courses ids for the current user
			// and filter the result by them
			$enrollmentsCommand = Yii::app()->db->createCommand()
					->select('idCourse')
					->from(LearningCourseuser::model()->tableName())
					->where('idUser=:user', array(':user' => (($userId)? (int)$userId : Yii::app()->user->id)));
			if($completedOnly) {
				$enrollmentsCommand->andWhere('status=:completed', array(':completed' => LearningCourseuser::$COURSE_USER_END));
			}
			$userEnrollments = $enrollmentsCommand->queryColumn();
			$command->andWhere(array('IN', 'source_course_id', $userEnrollments));
		}

		$result = $command->queryColumn();
		return $result;
	}



	/**
	 * Check if in the course specified by $idCourse parameter there are any C.S.P. learning objects
	 *
	 * @param $idCourse ID of the course to be checked
	 */
	public static function hasCspObjects($idCourse) {
		$query = "SELECT COUNT(*) AS num_csp "
			." FROM ".LearningOrganization::model()->tableName()." "
			." WHERE from_csp IS NOT NULL AND objectType <> '' AND idCourse = :id_course";
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand($query);
		$result = $cmd->queryScalar([':id_course' => $idCourse]);
		return (!empty($result) && $result > 0);
	}


	/**
	 * Check if current course is part of one or more selling learning plan
	 *
	 * @return bool
	 */
	public function isPartOfSellingLearningPlans() {
		if (!PluginManager::isPluginActive('CurriculaApp')) { return false; }
		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$t1 = $db->quoteTableName(LearningCoursepathCourses::model()->tableName());
		$t2 = $db->quoteTableName(LearningCoursepath::model()->tableName());
		$query = "SELECT COUNT(*) "
			." FROM ".$t1." cpc JOIN ".$t2." cp ON (cp.id_path = cpc.id_path AND cp.is_selling = 1) "
			." WHERE cpc.id_item = :id_course";
		$num = $db->createCommand($query)->queryScalar(array(':id_course' => $this->idCourse));
		return ($num > 0);
	}

}
