<?php


/**
 * This is the model class for table "core_user_field_dropdown_translations".
 *
 * @property integer $id_option
 * @property string $lang_code
 * @property string $translation
 *
 */
class CoreUserFieldDropdownTranslations extends \CActiveRecord {


	/**
	 * @inheritdoc
	 */
	public  function tableName() {
		return 'core_user_field_dropdown_translations';
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['lang_code, id_option', 'required'],
			['id_option', 'numerical', 'integerOnly' => true],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [];
	}

    /**
     * Inserts missing field translations
     *
     * @param array $langCodes
     * @return void
     */
    public static function insertMissing(array $langCodes)
    {
        foreach ($langCodes as $code) {
            static::insertMissingTranslationForLanguage($code);
        }
    }

    /**
     * Inserts missing field translations for the specified language
     *
     * @param string $langCode
     * @return void
     */
    public static function insertMissingTranslationForLanguage($langCode)
    {
        // Join core_user_field_dropdown_translations with itself, seeking ids of fields that have no translation
        // for the specified language code, inserting a new row in the table with an empty user value,
        // as this would apply for each row that has no translation for the language code
        $trTbl = '`core_user_field_dropdown_translations`';
        $sql = 'INSERT INTO ' . $trTbl . ' (`id_option`, `lang_code`, `translation`) 
            (SELECT 
                DISTINCT `cuft`.`id_option`,
                :langCode AS `lang_code`,
                \'\' AS `translation`
            FROM ' . $trTbl . ' `cuft` 
                LEFT JOIN ' . $trTbl . ' `seekMissing` 
                    ON `seekMissing`.`id_option` = `cuft`.`id_option` AND `seekMissing`.`lang_code` = :langCode 
            WHERE `seekMissing`.`id_option` IS NULL)';

        /** @var CDbCommand $cmd */
        $cmd = Yii::app()->db->createCommand($sql);
        $cmd->execute(['langCode' => $langCode]);
    }

    /**
     * Builds a command for fetching the label of a dropdown in a language with a fallback language
     *
     * @param string $language - try to fetch for this language
     * @param string $fallbackLanguage - fallback language
     * @param integer $fieldId - ID of the field
     * @param integer $optionId - ID of the option
     * @return CDbCommand
     */
    public static function getLabelForLanguage($language = null, $fallbackLanguage = null, $fieldId = null, $optionId = null)
    {
        $language = $language ?: Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $fallbackLanguage = $fallbackLanguage ?: Settings::get('default_language', 'english');

        /** @var CDbCommand $cmd */
        $cmd = Yii::app()->db->createCommand()
            ->select('IF(cufdt.translation NOT LIKE "", cufdt.translation, cufdt2.translation) AS translation')
            ->from(CoreUserFieldDropdown::model()->tableName() . ' cufd')
            ->join('core_user_field_dropdown_translations cufdt', 'cufd.id_option = cufdt.id_option AND cufdt.lang_code LIKE :lang_code', [':lang_code' => $language])
            ->join('core_user_field_dropdown_translations cufdt2', 'cufd.id_option = cufdt2.id_option AND cufdt2.lang_code LIKE :def_lang_code', [':def_lang_code' => $fallbackLanguage])
        ;

        if ($fieldId !== null) {
            $cmd->where('cufd.id_field = :id_field', [':id_field' => $fieldId]);
        }

        if ($optionId !== null) {
            $cmd->andWhere('cufd.id_option = :id_option', [':id_option' => $optionId]);
        }

        return $cmd;
    }
    
    /**
     * Get ALL possible options (translated string) in a given language (or current one) for all Dropdown fields or for a given field
     * 
     * @param string $language  Optional language, e.g. "en", "bg", ... 
     * @param integer $fieldId  Optional field ID
     * @return unknown
     */
    public static function getAllOptions($language=false, $fieldId=false) {

        // Always script-cache methods which might be called many times and doing SQL queries!!
        static $cache;
        $cacheKey = md5(serialize(func_get_args()));

        // Input language or current one
        $language = $language ?: Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        // Return cached result if available
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }
        
        $params = array(
            ":language" => $language,
        );
            
        $cmd = Yii::app()->db->createCommand();
        $cmd->select("trans.id_option as id_option, translation");
        $cmd->from("core_user_field_dropdown_translations trans");
        
        if ($fieldId !== false) {
            $cmd->join("core_user_field_dropdown dd", "dd.id_option=trans.id_option");
            $cmd->andWhere("dd.id_field=:id_field");
            $params[':id_field'] = (int) $fieldId;
        }
        
        $cmd->andWhere("lang_code=:language");

        $options = $cmd->queryAll(true, $params);

        // Cache & Return 
        $cache[$cacheKey] = array();
        if (is_array($options)) 
            foreach ($options as $option) 
                $cache[$cacheKey][$option['id_option']] = $option['translation'];

        return $cache[$cacheKey];
    }


}
