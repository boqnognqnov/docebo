<?php

/**
 * This is the model class for table "core_field_son".
 *
 * The followings are the available columns in table 'core_field_son':
 * @property integer $idSon
 * @property integer $idField
 * @property integer $id_common_son
 * @property string $lang_code
 * @property string $translation
 * @property integer $sequence
 */
class CoreFieldSon extends CActiveRecord
{
	
	public $lastIdCommonSon = false;
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreFieldSon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_field_son';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idField, id_common_son, sequence', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('translation', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idSon, idField, id_common_son, lang_code, translation, sequence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'field' => array(self::HAS_ONE, 'CoreField', 'idField'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idSon' => 'Id Son',
			'idField' => 'Id Field',
			'id_common_son' => 'Id Common Son',
			'lang_code' => 'Lang Code',
			'translation' => 'Translation',
			'sequence' => 'Sequence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idSon',$this->idSon);
		$criteria->compare('idField',$this->idField);
		$criteria->compare('id_common_son',$this->id_common_son);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation',$this->translation,true);
		$criteria->compare('sequence',$this->sequence);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
	
	
	
	/**
	 * Search the table for "translation" and "idField" and return the idSon.
	 * This is sort of reverse-engineering the user entry field, at the end of the day
	 * 
	 * @param string $translation
	 * @param number $idField
	 * @return number
	 */
	public static function getFieldSonId($translation, $idField, $language) {
	
		static $ids = array();
		
		$key = sha1(trim($translation) . $idField); 
		if (isset($ids[$key])) {
			return $ids[$key];
		}
		
		$criteria = new CDbCriteria();
		$criteria->addCondition('idField=:idField');
		$criteria->addCondition('LOWER(translation)=:translation');
		$criteria->addCondition('LOWER(lang_code)=:lang_code');
		$criteria->params = array(
			':idField' 		=> $idField,
			':translation' 	=> mb_strtolower($translation, 'UTF-8'),
			':lang_code' 	=> mb_strtolower($language,  'UTF-8'),				
		);
		
		$model = self::model()->find($criteria);
		if($model) {
			$ids[$key] = $model->idSon;
			return $ids[$key];
		}
		
		return false;
		
		
	}
	

	
	

	/**
	 * Search the table for "translation" and "idField" and return the FIRST id_common_son found
	 * This is sort of reverse-engineering the user entry field, at the end of the day
	 *
	 * @param string $translation
	 * @param number $idField
	 * @return number
	 */
	public static function getFieldIdCommonSon($translation, $idField, $language) {
	
		static $ids = array();
	
		$key = sha1(trim($translation) . $idField);
		if (isset($ids[$key])) {
			return $ids[$key];
		}
	
		$criteria = new CDbCriteria();
		$criteria->addCondition('idField=:idField');
		$criteria->addCondition('LOWER(translation)=:translation');
		$criteria->addCondition('LOWER(lang_code)=:lang_code');
		$criteria->params = array(
				':idField' 		=> $idField,
				':translation' 	=> mb_strtolower($translation, 'UTF-8'),
				':lang_code' 	=> mb_strtolower($language,  'UTF-8'),
		);
	
		$model = self::model()->find($criteria);
		if($model) {
			$ids[$key] = $model->id_common_son;
			return $ids[$key];
		}
	
		return false;
	
	
	}
	
	
	
	/**
	 * Get the MAX() of 'id_common_son' for a given FIELD and LANGUAGE (both optional) or 1 if NOT found
	 * @param string $idField
	 * @param string $langcode
	 */
	public function getLastIdCommonSon($idField = null, $langcode = null) {
		
		$criteria=new CDbCriteria;
		$criteria->select = 'MAX(id_common_son) as lastIdCommonSon';
		$criteria->compare('idField', $idField);
		$criteria->compare('lang_code', $langcode);
		
		$model = $this->find($criteria);
		
		
		return (int) $model->lastIdCommonSon;
		
	}

	public function getTranslation() {
		$defaultLanguage = Settings::get('default_language', false);
		if ( Yii::app()->session['current_lang'] != $defaultLanguage ) {
			$translation = Yii::app()->db->createCommand()
				->select('translation')
				->from(CoreFieldSon::model()->tableName())
				->where('idField = :idField AND id_common_son = :idCommonSon AND lang_code = :lang_code', array(
					':idField' => $this->idField,
					':idCommonSon' => $this->id_common_son,
					':lang_code' => Yii::app()->session['current_lang']
				))
				->queryScalar();

			if($translation)
				return $translation;
		}

		return $this->translation;
	}

	/**
	 * insert records in core_field_son for language $language if not exist
	 * @param $language
     * @deprecated
	 */
	public static function insertFieldSonsForLanguage($language) {
		$coreFieldSonTable = self::model()->tableName();
		$language = addslashes($language);

		$sql = "INSERT INTO $coreFieldSonTable
			SELECT NULL, cfs.idField, cfs.id_common_son, '$language', '', cfs.sequence
			FROM
				(SELECT * FROM $coreFieldSonTable GROUP BY id_common_son) cfs
			LEFT JOIN $coreFieldSonTable cfs1
			ON cfs.id_common_son = cfs1.id_common_son AND cfs1.lang_code = '$language'
			WHERE cfs1.id_common_son IS NULL";

		Yii::app()->db->createCommand($sql)->execute();
	}

}