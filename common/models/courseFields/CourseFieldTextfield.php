<?php

class CourseFieldTextfield extends LearningCourseField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render() {
		$htmlOptions = array();
		$courseEntry = $this->courseEntry;
		Yii::app()->event->raise('RenderAdditionalCourseField_Textfield', new DEvent($this, array(
			'field' => $this,
			'htmlOptions' => &$htmlOptions,
			'courseEntry' => &$courseEntry
		)));
		if($this->translation == '' || !$this->translation)
		{
			$tmp = LearningCourseField::model()->findByPk($this->id_field);
			$this->translation = $tmp->getTranslation();
			unset($tmp);
		}

		$htmlOptions = array(
		    "data-default-lang-translation" => $this->getTranslation(false, Settings::get("default_language")),
		);
		
		$html = CHtml::textfield('LearningCourse[additional]['.$this->id_field.'][value]', $courseEntry, $htmlOptions);
		$html .= CHtml::hiddenField('LearningCourse[additional]['.$this->id_field.'][type]', $this->type);
		return $html;
	}

	public function renderFilterField() {
		Yii::app()->controller->renderPartial('common.models.courseFields.views._textfield',
			array(
				'label'                 => $this->getTranslation(),
				'id_field'              => $this->id_field,
				'conditionsOptions'     => $this->getConditionsOptions(),
				'type'                  => $this->type
			),
			false,
			false
		);
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel=null, $whereFrom = null) {
		return CHtml::encode($courseEntry);
	}

}