<?php
/**
 * Custom course field of type Accrediation Iframe (ported from Thomson Reuters custom plugin) 
 *
 */
class CourseFieldAccrediframe extends LearningCourseField {

    /**
     * Authorization code lifetime (seconds)
     * @var integer
     */
    const AUTH_CODE_LIFETIME=30;
    
    public static $secret_key = '1a6c25d788faa1d1e61852f31e76d872';
    public static $default_iframe_height = 300;

    public static $emptyAccreditationObject = array(
        "accreditationColumns" => null,
        "accreditation" => array()
    );

    public static $jurisdictionArray = array(
        ''  =>'',
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming'
    );

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function render() {
        return Yii::app()->controller->renderPartial('common.models.courseFields.views._iframe_edit', array(
			'renderer' => $this
		));
    }

    public function renderFilterField() {
        Yii::app()->controller->renderPartial('common.models.courseFields.views._iframe_filter',
            array(
                'label'                 => $this->getTranslation(),
                'id_field'              => $this->id_field,
                'jurisditionsArray'     => self::$jurisdictionArray,
                'type'                  => $this->type
            ),
            false,
            false
        );
    }

    /**
     * @param $idField integer
     * @param $filterValue array
     * @param $sql_courses string
     */
    public static function applyCourseCatalogFilter($idField, $filterValue, &$sql_courses) {
        $credits = (int) $filterValue['credits'];
        if($sql_courses instanceof CDbCommand) {
            if($filterValue['jurisdiction'])
                $sql_courses->andWhere('lcfv.field_' . $idField . ' LIKE \'%"jurisdiction":"'.$filterValue['jurisdiction'].'"%\'');

            if($filterValue['credits'])
                $sql_courses->andWhere('(lcfv.field_' . $idField . ' LIKE \'%"credits":"'.$credits.'"%\') OR (lcfv.field_' . $idField . ' LIKE \'%"credits":'.$credits.'%\')');
        } else {
            if ($filterValue['jurisdiction'])
                $sql_courses .= ' and lcfv.field_' . $idField . ' LIKE \'%"jurisdiction":"' . $filterValue['jurisdiction'] . '"%\'';

            if ($filterValue['credits'])
                $sql_courses .= ' and ((lcfv.field_' . $idField . ' LIKE \'%"credits":"' . $credits . '"%\') OR (lcfv.field_' . $idField . ' LIKE \'%"credits":'.$credits.'%\'))';
        }
    }

    /**
     * @param $idField integer
     * @param $filterValue array
     * @param $command CDbCommand
     */
    public static function applyMyCoursesFilter($idField, $filterValue, $command) {
        if($filterValue['jurisdiction'])
            $command->andWhere('lcfv.field_' . $idField . ' LIKE \'%"jurisdiction":"'.$filterValue['jurisdiction'].'"%\'');

        if($filterValue['credits']) {
            $credits = (int) $filterValue['credits'];
            $command->andWhere('(lcfv.field_' . $idField . ' LIKE \'%"credits":"' . $credits . '"%\') OR (lcfv.field_' . $idField . ' LIKE \'%"credits":'.$credits.'%\')');
        }
    }

    /**
     * @param $courseEntry
     * @param LearningCourseField $courseEntryModel
     * @param null $whereFrom
     * @return string
     */
    public static function renderFieldValue($courseEntry, $courseEntryModel = null, $whereFrom = null) {
        try {
            // Extract data to show
            $data = CJSON::decode($courseEntry);
            if(!isset($data['accreditation']) || empty($data['accreditation']))
                throw new CException("Invalid or empty \"accreditation\" array");
            $rows = $data['accreditation'];

            // Determine which columns to show
            $settings = $courseEntryModel->getSettingsArray();
            if($whereFrom == 'course_description')
                $columnIds = (isset($settings['columns_widget']) && $settings['columns_widget']) ? explode(",", $settings['columns_widget']) : array();
            else
                $columnIds = (isset($settings['columns_catalog']) && $settings['columns_catalog']) ? explode(",", $settings['columns_catalog']) : array();
            if(empty($columnIds))
                throw new CException("No columns selected");

            // Build gridview columns
            $columns = array();
            foreach($columnIds as $columnId) {
                $columns[] = array(
                    'header' => isset($data['accreditationColumns'][$columnId]) ? $data['accreditationColumns'][$columnId] : '',
                    'name' => $columnId
                );
            }

            // Build the data provider
            $dataProvider = new CArrayDataProvider($rows, array(
                'pagination' => array (
                    'pageSize'  => 4,
                    'pageVar'   => 'accreditation_page_'.$courseEntryModel->id_field
                ),
            ));

            return Yii::app()->controller->renderPartial('common.models.courseFields.views._iframe_table', array(
                'columns' => $columns,
                'dataProvider' => $dataProvider,
                'renderer' => $courseEntryModel
            ), true);
        } catch(Exception $e) {
            Log::_($e->getMessage());
            return Yii::t('thomsonreuters', 'No accreditation info available');
        }
    }


    public static function shouldDisplayInColumn($renderedWhere) {
        return $renderedWhere == 'course_catalog_inpage';
    }

    /**
     * Renders the settings page of course fields metadata
     * @return mixed
     */
    public function renderFieldSettings() {
        $clientsList = array();
        foreach (OauthClients::sqlDataProvider()->data as $client) {
            $clientsList[$client['client_id']] = $client["app_name"];
        }
        return Yii::app()->controller->renderPartial('common.models.courseFields.views._iframe_settings', array(
            'fieldModel'    => $this,
            'clientsList'   => $clientsList,
        ));
    }

    /**
     * Returns the value of a "course field" (Jurisdiction area or accreditation duration)
     */
    public function getFieldId($translation) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('lang_code = :lang_code')
            ->addCondition('LOWER(translation) = LOWER(:translation)');
        $criteria->params = array(
            ':lang_code' => 'english',
            ':translation' => $translation
        );
        $fieldTranslation = LearningCourseFieldTranslation::model()->find($criteria);
        if($fieldTranslation)
            return 'LearningCourse_additional_'.$fieldTranslation->id_field.'_value';
        else
            return '';
    }

    /**
     * Returns accreditation object (i.e. without the course field values for Jurisdiction area and accreditation duration)
     * @return string
     */
    public function getAccreditationObject($asJson = true) {
        if($this->courseEntry)
            $accreditationObjectJs = $this->courseEntry;
        else
            $accreditationObjectJs = CJSON::encode(self::$emptyAccreditationObject);

        if($asJson)
            return $accreditationObjectJs;
        else
            return CJSON::decode($accreditationObjectJs);
    }

    /**
     * Returns formatted iframe URL.
     * 
     * URL query parts are:<br>
     *      <strong>course_code</strong> Course CODE or ID<br>
     *      <strong>user_id</strong>     i.e. LMS username<br>
     *      <strong>auth_code</strong>   OAuth2 authorization code, sent to client, used to obtain access token from us later<br>
     *      <strong>hash</strong>        SHA256 of the above parameters and a secret key added at the end (salt)<br>
     *      
     * This URL points to a Client and it should make an API request back to US (oauth/token) to exchange the above Auth Code for an access token.
     * Autho Code expires in certain amount of time (@see self::AUTH_CODE_LIFETIME)!
     * 
     */
    public function getIframeUrl() {
        $settings = $this->getSettingsArray();
        $course = $this->course; /* @var $course LearningCourse */

        // Parse current iframe url
        $url_parts = parse_url($settings['url']);
        parse_str($url_parts['query'], $params);

        // Build the params array
        $paramsToHash = array(
            'course_code' => $course->code ? $course->code : ("course_".$course->idCourse),
            'user_id' => urlencode(Yii::app()->user->getUsername()),
            'auth_code' => $this->getOAuthAuthorizationCode(),
        );
        
        $params = array_merge($params, $paramsToHash);

        // Calculate the sha1 hash of the previous params + the secret
        $hashSalt = isset($settings["hash_secret"]) ? $settings["hash_secret"] : self::$secret_key;
        $paramsToHash = array_merge($paramsToHash, array($hashSalt));
        $params['hash'] = hash("sha256", implode(",", $paramsToHash));

        // this will url_encode all values
        $url_parts['query'] = http_build_query($params);

        
        $fragment = !empty($url_parts["fragment"]) ? "#" . $url_parts["fragment"] : "";
        
        // Return url
        $url = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query'] . $fragment;
        
        return $url;

    }

    /**
     * Returns the iframe height using the course field metadata
     * @return int
     */
    public function getIframeHeight() {
        $settings = $this->getSettingsArray();
        return $settings['height'] ? $settings['height'] : self::$default_iframe_height;
    }

    /**
     * Generate and return OAuth2 authorization code for TR Accreditation client
     * @return string
     */
    private function getOAuthAuthorizationCode() 
    {
        $authCode = "";
        $settings = $this->getSettingsArray();
        $clientId = $settings['client_id'];
        $server = DoceboOauth2Server::getInstance();
        if ($server) {
            $server->init();
            $authCode = $server->generateAuthorizationCode(Yii::app()->user->id, $clientId, null, 'api', self::AUTH_CODE_LIFETIME);
        }
        return $authCode;
    }
    
    
    
}

