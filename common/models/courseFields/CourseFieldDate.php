<?php

class CourseFieldDate extends LearningCourseField
{

	//date width to be used
	protected $_dateWidth = LocalTime::DEFAULT_DATE_WIDTH;//LocalTime::MEDIUM;
	
	public function render(){
		$htmlOptions = array();
		$courseEntry = $this->courseEntry;
		Yii::app()->event->raise('RenderAdditionalCourseField_Date', new DEvent($this, array(
				'field' => $this,
				'htmlOptions' => &$htmlOptions,
				'courseEntry' => &$courseEntry
		)));
		if($this->translation == ''){
			$tmp = LearningCourseField::model()->findByPk($this->id_field);
			$this->translation = $tmp->getTranslation();
			unset($tmp);
		}
		
		$htmlOptions = array(
			'class'                              => 'datepicker',
		    "data-default-lang-translation"      => $this->getTranslation(false, Settings::get("default_language")),
		);

		$valueDate = $courseEntry ? Yii::app()->localtime->toLocalDate($courseEntry) : null;

		$html = CHtml::textField('LearningCourse[additional][' . $this->id_field . '][value]', $valueDate, $htmlOptions);

		$html .= '<i class="p-sprite calendar-black-small settings-date-icon"></i>';
		$html .= CHtml::hiddenField('LearningCourse[additional]['.$this->id_field.'][type]', $this->type);
		
		return $html;
	}
	
	public function getDateWidth()
	{
		//check date width for input fields
		if (!Yii::app()->localtime->forceLongYears()) {
			//trying to prevent short year format to be used in input fields
			switch ($this->_dateWidth) {
				case LocalTime::LONG:
					$dateWidth = LocalTime::LONG;
					break;
				default:
					$dateWidth = LocalTime::MEDIUM;
					break;
			}
		} else {
			//long years are forced, all date widths are safe
			$dateWidth = $this->_dateWidth;
		}
		return $dateWidth;
	}

	public function setDateWidth($dateWidth)
	{
		switch ($dateWidth) {
			case LocalTime::SHORT:
			case LocalTime::MEDIUM:
			case LocalTime::LONG:
				$this->_dateWidth = $dateWidth;
				break;
			default:
				$this->_dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
				break;
		}
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel = null, $wherefrom = null) {
        if(!empty($courseEntry))
            $courseEntry = Yii::app()->localtime->toLocalDate($courseEntry);

		return CHtml::encode($courseEntry);
	}

	public function renderFilterField() {
		Yii::app()->controller->renderPartial('common.models.courseFields.views._date',
			array(
				'label'                 => $this->getTranslation(),
				'id_field'              => $this->id_field,
				'conditionsOptions'     => $this->getConditionsOptions(),
				'type'                  => $this->type
			),
			false,
			false
		);
	}

}

?>


