<div class="row-fluid row-course-field row-field-<?=$id_field ?>" id="row-field-<?=$id_field ?>" data-filter-value="<?=$id_field ?>">
	<div class="span3 span3-label">
		<?= CHtml::label($label, 'advancedSearch[additional]['.$id_field.']'); ?>
	</div>
	<div class="span9 dropdown-span9">
		<div class="row-fluid course-field-dropdown-value">
			<div class="span11 dropdown-span11">
				<?php

				$fieldParams = array('style' => 'width: 100%;', 'class' => 'dropdown-course-field-select');
				if(!isset($_REQUEST['advancedSearch']['additional'][$id_field]['value'])) {
					$fieldParams['disabled'] = 'disabled';
				}

				?>
				<?= CHtml::dropDownlist(
					'advancedSearch[additional]['.$id_field.'][value]',
					(!empty($_REQUEST['advancedSearch']['additional'][$id_field]['value'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['value']) : '',
					$options,
					$fieldParams
				); ?>
			</div>
			<div class="span1 dropdown-span1">
				<a class="remove-advanced-filter-link" data-filter-value="<?=$id_field ?>" data-filter-text="<?=$label ?>" style="">X</a>
			</div>
		</div>


		<?=CHtml::hiddenField('advancedSearch[additional][' . $id_field . '][type]', $type) ?>
	</div>
</div>

