<?php /* @var $renderer LearningCourseField */ ?>
<?php /* @var $dataProvider CArrayDataProvider */ ?>
<?php /* @var $columns array */ ?>
<div class="accreditation-table">
	<div id="table-header"><?= Yii::t('thomsonreuters', 'Course Accredations')?></div>
	<?php $this->widget('DoceboCGridView', array(
		'id' => 'accreditation-'.$renderer->id_field.'-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $dataProvider,
		'columns' => $columns,
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			options.data = {};
			options.url += "&htmlOnly=true";
			$.extend(options.data, '.CJSON::encode($_POST).');
		}',
		'pager' => array(
			'class' => 'DoceboCLinkPager',
			'maxButtonCount' => 4,
		)
	)); ?>
</div>
