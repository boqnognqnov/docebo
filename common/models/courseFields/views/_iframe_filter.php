<div class="row-fluid row-course-field row-field-<?=$id_field ?>" id="row-field-<?=$id_field ?>" data-filter-value="<?=$id_field ?>">
	<div class="span3 span3-label">
		<?= CHtml::label($label, 'advancedSearch[additional]['.$id_field.']'); ?>
	</div>
	<div class="span9 dropdown-span9">
		<div class="row-fluid course-field-dropdown-value">
			<div class="span11">
				<div class="iframe-subfilter-name"><?= Yii::t('thomsonreuters', 'Jurisdiction');?></div>
				<div class="iframe-subfilter">
				<?= CHtml::dropDownlist(
					'advancedSearch[additional]['.$id_field.'][value][jurisdiction]',
					(!empty($_REQUEST['advancedSearch']['additional'][$id_field]['value']['jurisdiction'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['value']['jurisdiction']) : '',
					$jurisditionsArray,
					array('style' => 'width: 100%;', 'class' => 'dropdown-course-field-select')
				); ?>
				</div>
				<div class="iframe-subfilter-name"><?= Yii::t('thomsonreuters', 'Credits');?></div>
				<div class="iframe-subfilter">
					<?= CHtml::textfield(
						'advancedSearch[additional]['.$id_field.'][value][credits]',
						(!empty($_REQUEST['advancedSearch']['additional'][$id_field]['value']['credits'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['value']['credits']) : '',
						array('class' => 'course-field-textfield-input')
					); ?>
				</div>
			</div>
			<div class="span1 dropdown-span1 iframe-filter-delete-icon">
				<a class="remove-advanced-filter-link" data-filter-value="<?=$id_field ?>" data-filter-text="<?=$label ?>" style="">X</a>
			</div>
		</div>

		<?=CHtml::hiddenField('advancedSearch[additional][' . $id_field . '][type]', $type) ?>
	</div>
</div>

