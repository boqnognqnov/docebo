<?php

$fieldParams = array(
    'id' => 'text_area_' . $model->id_field,
    "data-default-lang-translation"   => $model->getTranslation(false, Settings::get("default_language")),
);
/*if(!isset($_REQUEST['advancedSearch']['additional'][$model->id_field]['value'])) {
	$fieldParams['disabled'] = 'disabled';
}*/

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo CHtml::textArea('LearningCourse[additional]['.$model->id_field.'][value]', $courseEntry, $fieldParams);

echo CHtml::hiddenField('LearningCourse[additional]['.$model->id_field.'][type]', $model->type);

?>

<script type="text/javascript">    
    $(document).ready(function(){                        
        TinyMce.attach('#' + $('#<?='text_area_' . $model->id_field?>').attr('id'), {height: 200,
			plugins : [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste moxiemanager textcolor"
			],
			toolbar : "undo redo | styleselect | bold italic fontsizeselect forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			language: yii.language,
		});
        
        $('input').styler();
    });
    </script>