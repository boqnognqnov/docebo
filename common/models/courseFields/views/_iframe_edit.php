<? /* @var $renderer CourseFieldAccrediframe */?>
<?php
    $accreditationObject = $renderer->getAccreditationObject();
    $iframeId = "accred-iframe-".$renderer->id_field;
    
    // Get all Classroom sessions (ID, Name, Start/End, Hours)
    $dp = LtCourseSession::model()->dataProvider($renderer->course->idCourse);
    $sessions = $dp->data;
    $courseClassroomSessions = array();
    if (is_array($sessions) && count($sessions) > 0) {
        foreach ($sessions as $session) {
            $courseClassroomSessions[] = array(
                "id"            => $session->id_session,
                "name"          => $session->name,
                "date_begin"    => $session->date_begin,
                "date_end"      => $session->date_end,
                "total_hours"   => $session->total_hours
            );            
        }
    }
    
    
    // Get all Webinar sessions (ID, Name, Start/End)
    $dp = WebinarSession::model()->dataProvider($renderer->course->idCourse);
    $sessions = $dp->data;
    $courseWebinarSessions = array();
    if (is_array($sessions) && count($sessions) > 0) {
    	foreach ($sessions as $session) {
    		$courseWebinarSessions[] = array(
    				"id"            => $session->id_session,
    				"name"          => $session->name,
    				"date_begin"    => $session->date_begin,
    				"date_end"      => $session->date_end,
    		);
    	}
    }
    
    


?>
<input id="accred-iframe-<?=$renderer->id_field?>_field" type="hidden" name="LearningCourse[additional][<?=$renderer->id_field?>][value]" value="<?=htmlspecialchars($accreditationObject)?>"/>
<iframe id="accred-iframe-<?=$renderer->id_field?>" src="<?=$renderer->getIframeUrl()?>" width="100%" height="<?=$renderer->getIframeHeight()?>" frameborder="0"></iframe>

<script type="text/javascript">
<!--

$(function () {
	// Init the accreditation behaviour on this iframe
	if ($.fn.accreditation) {
		$("#accred-iframe-<?=$renderer->id_field?>").accreditation({
			initObject: <?=$accreditationObject?>,
			jurisdictionAreaFieldId: '#<?= $renderer->getFieldId('Jurisdiction Area') ?>',
			accreditationDurationFieldId: '#<?= $renderer->getFieldId('Accreditation duration') ?>',
			courseInfo : {
				courseCode	: <?= json_encode($renderer->course->code) ?>,
				courseId	: <?= json_encode($renderer->course->idCourse) ?>,
				courseName	: <?= json_encode($renderer->course->name) ?>,
                courseDescription : <?= json_encode($renderer->course->description) ?>,
                courseMaxAttempts : <?= json_encode($renderer->course->lo_max_attempts) ?>,
                courseStatus : <?= json_encode($renderer->course->status) ?>,
                courseType   : <?= json_encode($renderer->course->course_type) ?>
			},
			courseClassroomSessions    : <?= json_encode($courseClassroomSessions) ?>,
			courseWebinarSessions	   : <?= json_encode($courseWebinarSessions) ?>
		});
	}
});


//-->
</script>
