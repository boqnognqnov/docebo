<?php /* @var $fieldModel LearningCourseField */?>
<?php $settings = $fieldModel->getSettingsArray(); ?>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Iframe URL')?></span>
	<input type="text" name="LearningCourseField[settings][url]" value="<?= $settings['url'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Iframe height (px)')?></span>
	<input type="text" name="LearningCourseField[settings][height]" value="<?= $settings['height'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Table columns (course catalog)') ?></span>
	<input type="text" name="LearningCourseField[settings][columns_catalog]" value="<?= $settings['columns_catalog'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Table columns (course description widget)') ?></span>
	<input type="text" name="LearningCourseField[settings][columns_widget]" value="<?= $settings['columns_widget'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Salt Secret') ?></span>
	<input type="password" name="LearningCourseField[settings][hash_secret]" value="<?= $settings['hash_secret'] ?>" />
</div>
<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'Repeat Salt Secret') ?></span>
	<input type="password" name="LearningCourseField[settings][hash_secret_repeat]" value="<?= $settings['hash_secret_repeat'] ?>" />
</div>

<div class="course-fields-settings">
	<span><?= Yii::t('thomsonreuters', 'OAuth Client') ?></span>
	<?= CHtml::dropDownList("LearningCourseField[settings][client_id]", $settings['client_id'], $clientsList, array("prompt" => ""))	?>
</div>
