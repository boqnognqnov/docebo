<?php

class CourseFieldDropdown extends LearningCourseField
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function render(){
		$htmlOptions = array();
		$courseEntry = $this->courseEntry;
		
		$htmlOptions = array(
		    "prompt"                          => Yii::t('standard', '_SELECT'),
		    "data-default-lang-translation"   => $this->getTranslation(false, Settings::get("default_language")),
		);
		
		$html =  CHtml::dropDownList('LearningCourse[additional]['.$this->id_field.'][value]', $courseEntry, $this->getOptions(), $htmlOptions);
		$html .= CHtml::hiddenField('LearningCourse[additional]['.$this->id_field.'][type]', $this->type);
		
		return $html;
	}

	public function getOptions()
	{
		$fieldSons = LearningCourseFieldDropdown::model()->findAllByAttributes(
			array(
				'id_field' => $this->id_field,
			));
		if (empty($this->lang_code)) {
			$this->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		}
		$options = array();
		foreach ($fieldSons as $fieldSon) {
			$optionModel = LearningCourseFieldDropdownTranslations::model()
                ->findByAttributes(array(
                    'id_option' => $fieldSon->id_option,
                    'lang_code' => $this->lang_code
                ));
            if ( empty($optionModel->translation) || $optionModel == '') {
                $langCode = Settings::get('default_language', 'english');
                $optionModel = LearningCourseFieldDropdownTranslations::model()
                    ->findByAttributes(array(
                        'id_option' => $fieldSon->id_option,
                        'lang_code' => $langCode
                    ));
            }
			if(!empty($optionModel->translation)) {
				$options[$fieldSon->id_option] = $optionModel->translation;
			}
		}
        asort($options);
		return $options;
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel=null, $whereFrom = null) {

		$currentLanguage = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$fieldSon = LearningCourseFieldDropdownTranslations::model()->findByAttributes(array(
			'id_option' 	=> $courseEntry,
			'lang_code' 	=> $currentLanguage
		));

		$lmsDefaultLanguage = Settings::get('default_language', 'english');

		if (!empty($fieldSon->translation)) {
			return CHtml::encode($fieldSon->translation);
		} else {
			$fieldSon = LearningCourseFieldDropdownTranslations::model()->findByAttributes(array(
				'id_option' 	=> $courseEntry,
				'lang_code' 	=> $lmsDefaultLanguage
			));

			if (!empty($fieldSon->translation)) {
				return CHtml::encode($fieldSon->translation);
			}
		}

		return "";
	}

	public function renderFilterField() {
		Yii::app()->controller->renderPartial('common.models.courseFields.views._dropdown',
				array(
					'label'     => $this->getTranslation(),
					'id_field'  => $this->id_field,
					'options'   => $this->getOptions(),
					'type'      => $this->type
				),
				false,
				false
			);
	}

}

?>