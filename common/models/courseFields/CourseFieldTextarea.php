<?php

class CourseFieldTextarea extends LearningCourseField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render() {
		$htmlOptions = array();
		$courseEntry = $this->courseEntry;
		Yii::app()->event->raise('RenderAdditionalCourseField_Textarea', new DEvent($this, array(
			'field' => $this,
			'htmlOptions' => &$htmlOptions,
			'courseEntry' => &$courseEntry
		)));
		if($this->translation == '')
		{
			$tmp = LearningCourseField::model()->findByPk($this->id_field);
			$this->translation = $tmp->getTranslation();
			unset($tmp);
		}
        
        Yii::app()->controller->renderPartial('common.models.courseFields.views._textarea_fill', array(
				'model' => $this,
                'courseEntry' => $courseEntry
			),
			false,
			false
		);                
	}

	public function renderFilterField() {
		Yii::app()->controller->renderPartial('common.models.courseFields.views._textarea',
			array(
				'label'                 => $this->getTranslation(),
				'id_field'              => $this->id_field,
				'conditionsOptions'     => $this->getTextareaConditionsOptions(),
				'type'                  => $this->type
			),
			false,
			false
		);
	}


	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel=null, $whereFrom = null) {
        return $courseEntry;
	}

}