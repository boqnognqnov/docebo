<?php

/**
 * This is the model class for table "learning_commontrack".
 *
 * The followings are the available columns in table 'learning_commontrack':
 * @property integer $idReference
 * @property integer $idUser
 * @property integer $idTrack
 * @property string $objectType
 * @property string $dateAttempt
 * @property string $status
 * @property string $firstAttempt
 * @property string $first_complete
 * @property string $last_complete
 * @property double $first_score
 * @property double $score
 * @property double $score_max
 * @property string $total_time
 * @property string $idResource
 * @property string $idMasterOrg
 * @property integer $failed_attempts_taken
 * @property integer $failed_attempts_allowed
 * @property boolean $allowedFailedAttemptsReached
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property LearningOrganization $organization
 * @property LearningRepositoryObjectVersion $objectVersion
 */
class LearningCommontrack extends CActiveRecord
{

	const STATUS_COMPLETED = 'completed';
	const STATUS_PASSED = 'passed';
	const STATUS_AB_INITIO = 'ab-initio';
	const STATUS_ATTEMPTED = 'attempted';
	const STATUS_FAILED = 'failed';

	const UPDATE_STATUS_ALWAYS = 1;         // always update the status if a new score is added
	const UPDATE_STATUS_IMPROVE_ONLY = 2;   // only update the status if changing from eg. failed > passed, not vice versa
	const UPDATE_STATUS_NEVER = 3;          // never update the status if a new score is added: first result is final

	const SCENARIO_SYNC = 'syncronizing';
	const SCENARIO_USER_SUBSCRIBED_COURSE = 'userSubscribedCourse';

	protected $oldAttributes = array();

	public $skip_certificate_generation = false;

	/**
	 * Calculated model attribute (failed_attempts_taken >= failed_attempts_allowed) 
	 * 
	 * @var boolean
	 */
	public $allowedFailedAttemptsReached = false;

	public static function getStatusLabelsList()
	{
		$output = array(
			//When there is no record for user-LO tracking, but he is enrolled, show "Not started" message
			null => Yii::t('standard', '_NOT_STARTED'),
			self::STATUS_AB_INITIO => Yii::t('standard', '_NOT_STARTED'),
			self::STATUS_ATTEMPTED => Yii::t('standard', '_USER_STATUS_BEGIN'),
			self::STATUS_COMPLETED => Yii::t('standard', '_COMPLETED'),
			self::STATUS_PASSED => Yii::t('standard', '_COMPLETED'),
            self::STATUS_FAILED => Yii::t('standard', 'failed')
		);
		return $output;
	}

	public static function isStatusValid($status) {
		return array_key_exists($status, self::getStatusLabelsList());
	}

	public function getStatusLabel($status = null)
	{
		$status = $status ? $status :  $this->status;
		$statuses = self::getStatusLabelsList();
		return isset($statuses[$status]) ? $statuses[$status] : null;
	}

	public function renderStatusIcon($status = null)
	{

		$css = '';
		if($this->objectType == LearningOrganization::OBJECT_TYPE_DELIVERABLE && !$this->getEvaluatedDeliverableObjectByUser()){
			$css = 'orange';
		}else{
			$status = $status ? $status : $this->status;

			switch ($status) {
				case self::STATUS_COMPLETED:
					$css = 'green';
					break;
				case self::STATUS_FAILED:
					$css = 'red';
					break;
				default:
					$css = 'grey';
					break;
			}
		}
		$html = '<span class="i-sprite is-circle-check '.$css.' wtf"></span>';
		return $html;
	}

    public function renderScore($sep = '/') {
	    $res = intval($this->score) . $sep . intval($this->score_max);
	    if(($this->objectType == LearningOrganization::OBJECT_TYPE_DELIVERABLE && !$this->score) || !$this->getEvaluatedDeliverableObjectByUser()){
		    $res = '-';
	    }
        return $res;
    }


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCommontrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_commontrack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idReference, idUser, idTrack, idResource, idMasterOrg', 'numerical', 'integerOnly'=>true),
			array('first_score, score, score_max', 'numerical'),
			array('objectType, status', 'length', 'max'=>20),
			array('total_time', 'length', 'max'=>15),
			array('dateAttempt, firstAttempt, first_complete, last_complete', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idReference, idUser, idTrack, objectType, dateAttempt, status, firstAttempt, first_complete, last_complete, first_score, score, score_max, total_time, idResource, idMasterOrg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'organization' => array(self::BELONGS_TO, 'LearningOrganization', 'idReference'),
			'course' => array(self::HAS_ONE, 'LearningCourse', array('idCourse' => 'idCourse'), 'through' => 'organization'),
			'objectVersion' => array(self::BELONGS_TO, 'LearningRepositoryObjectVersion', array('objectType' => 'object_type', 'idResource' => 'id_resource'))
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('dateAttempt medium', 'firstAttempt medium', 'first_complete medium', 'last_complete medium'),
			 'dateAttributes' => array()
            )
        );
    }

	public function byUser($userId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'learningCommontracks.idUser=:scopeUserId',
			'params'=>array(':scopeUserId'=>$userId),
		));
		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idReference' => 'Id Reference',
			'idUser' => 'Id User',
			'idTrack' => 'Id Track',
			'objectType' => Yii::t('standard', 'Object Type'),
			'dateAttempt' => Yii::t('standard', 'Date Attempt'),
			'status' => Yii::t('standard', '_STATUS'),
			'firstAttempt' => Yii::t('report', '_LO_COL_FIRSTATT'),
			'first_complete' => 'First Complete',
			'last_complete' => 'Last Complete',
			'first_score' => 'First Score',
			'score' => Yii::t('standard', '_SCORE'),
			'score_max' => 'Score Max',
			'total_time' => 'Total Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('objectType',$this->objectType,true);
		$criteria->compare('dateAttempt',Yii::app()->localtime->fromLocalDateTime($this->dateAttempt),true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('firstAttempt',Yii::app()->localtime->fromLocalDateTime($this->firstAttempt),true);
		$criteria->compare('first_complete',Yii::app()->localtime->fromLocalDateTime($this->first_complete),true);
		$criteria->compare('last_complete',Yii::app()->localtime->fromLocalDateTime($this->last_complete),true);
		$criteria->compare('first_score',$this->first_score);
		$criteria->compare('score',$this->score);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('total_time',$this->total_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	protected function afterFind() {
		parent::afterFind();
		
		// allowed failed attempts is counted ONLY if ALLOWED number is set!
        $this->allowedFailedAttemptsReached = ((int) $this->failed_attempts_allowed > 0) && ((int) $this->failed_attempts_taken >= (int) $this->failed_attempts_allowed);
        
		$this->oldAttributes = $this->attributes;
	}


	/**
	 * Check if a given status has a valid value
	 * @param string $status the status value to check
	 * @return boolean
	 */
	public static function isValidStatus($status) {
		if ($status == LearningCommontrack::STATUS_COMPLETED) return true;
		if ($status == LearningCommontrack::STATUS_AB_INITIO) return true;
		if ($status == LearningCommontrack::STATUS_ATTEMPTED) return true;
		if ($status == LearningCommontrack::STATUS_FAILED) return true;
		return false;
	}

	public static function getDateByParameters($dateColumn, $organizationId, $userId)
	{
		$model = self::model()->findByAttributes(array(
			'idReference' => $organizationId,
			'idUser' => $userId
		));
		if ($model && $model->$dateColumn)
			return $model->$dateColumn;
		else
			return '-';
	}

	//not really usefull function as it returns wrong data + not filtered by user... Not sure if needed at all
	public function getUserCourseProgress($courseId = null) {
		$organizations = LearningOrganization::model()->findAllByAttributes(array('idCourse' => $courseId));
		$organizationsList = CHtml::listData($organizations, 'idOrg', 'idOrg');
		$totalCourseTracksCount = count($organizationsList);

		$criteria = new CDbCriteria();
		$criteria->addCondition('objectType != ""');
		$criteria->addCondition('status = :status');
		$criteria->params = array(
			':status' => LearningCommontrack::STATUS_COMPLETED,
		);
		$criteria->addInCondition('idReference', $organizationsList);
		$currentCourseTracksCount = LearningCommontrack::model()->count($criteria);

		if ($totalCourseTracksCount > 0) {
			return $currentCourseTracksCount / $totalCourseTracksCount * 100;
		}
		return 0;
	}

	public function updateStatus($newStatus) {

		if ($this->isNewRecord) { return false; }

		//check input validity
		if (!self::isValidStatus($newStatus)) { return false; }

		if ($this->status == $newStatus) { return true; }

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		$updateException = new CException('Error while updating status');
		$invalidStatusException = new CException('Invalid status for specified object');

		try {

			switch ($this->objectType) {

				case LearningOrganization::OBJECT_TYPE_FILE:
				case LearningOrganization::OBJECT_TYPE_LTI:
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
				case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE:
				case LearningOrganization::OBJECT_TYPE_ITEM: {
					if ($newStatus == LearningCommontrack::STATUS_FAILED) { throw $invalidStatusException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_TINCAN: {
					$this->status = $newStatus;
					if (!$this->save()) { throw $updateException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_AUTHORING: {
					$specific = LearningConversion::model()->findByPk($this->idTrack);
					$specificStatus = false;
					switch ($newStatus) {
						case LearningCommontrack::STATUS_AB_INITIO:
						case LearningCommontrack::STATUS_ATTEMPTED: { $specificStatus = LearningConversion::STATUS_INCOMPLETE; } break;
						case LearningCommontrack::STATUS_COMPLETED: { $specificStatus = LearningConversion::STATUS_COMPLETED; } break;
						default: { throw $invalidStatusException; } break;
					}
					$specific->completion_status = $specificStatus;
					if (!$specific->save()) { throw $updateException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_POLL: {
					$specific = LearningPolltrack::model()->findByPk($this->idTrack);
					$specificStatus = false;
					switch ($newStatus) {
						case LearningCommontrack::STATUS_AB_INITIO:
						case LearningCommontrack::STATUS_ATTEMPTED: { $specificStatus = LearningPolltrack::STATUS_NOT_COMPLETE; } break;
						case LearningCommontrack::STATUS_COMPLETED: { $specificStatus = LearningPolltrack::STATUS_VALID; } break;
						default: { throw $invalidStatusException; } break;
					}
					$specific->status = $specificStatus;
					if (!$specific->save()) { throw $updateException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_TEST: {
					$specific = LearningTesttrack::model()->findByPk($this->idTrack);
					$specificStatus = false;
					switch ($newStatus) {
						case LearningCommontrack::STATUS_AB_INITIO:
						case LearningCommontrack::STATUS_ATTEMPTED: { $specificStatus = LearningTesttrack::STATUS_NOT_COMPLETE; } break;
						case LearningCommontrack::STATUS_COMPLETED: { $specificStatus = LearningTesttrack::STATUS_VALID; } break;
						case LearningCommontrack::STATUS_FAILED: { $specificStatus = LearningTesttrack::STATUS_FAILED; } break;
						default: { throw $invalidStatusException; } break;
					}
					$specific->score_status = $specificStatus;
					if (!$specific->save()) { throw $updateException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_VIDEO: {
					if ($newStatus == LearningCommontrack::STATUS_FAILED) { throw $invalidStatusException; }
					$specific = LearningVideoTrack::model()->findByPk($this->idTrack);
					$specific->status = $newStatus; //videotrack table and commontrack table share same statuses (except for "failed" status, which do not apply for videos
					if (!$specific->save()) { throw $updateException; }
				} break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG: {
					$specific = LearningScormTracking::model()->findByPk($this->idTrack);
					if (empty($specific)) {
						throw new CException('Invalid tracking data');
					}

					// Be sure item tracking records are available. It will "try" to create if not existant
					LearningScormTracking::setupScormItemsTrack($specific->idUser, $specific->scorm_item->idscorm_organization, $specific->idReference);

					$specificStatus = false;
					switch ($newStatus) {
						case LearningCommontrack::STATUS_AB_INITIO: { $specificStatus = LearningScormTracking::STATUS_NOT_ATTEMPTED; } break;
						case LearningCommontrack::STATUS_ATTEMPTED: { $specificStatus = LearningScormTracking::STATUS_INCOMPLETE; } break;
						case LearningCommontrack::STATUS_COMPLETED: { $specificStatus = LearningScormTracking::STATUS_COMPLETED; } break;
						case LearningCommontrack::STATUS_FAILED: { $specificStatus = LearningScormTracking::STATUS_FAILED; } break;
						default: { throw new CException('Invalid specified status: '.$newStatus); } break;
					}
					if ($specific->lesson_status != $specificStatus) {
						if (!$specific->updateStatus($specificStatus)) { throw $updateException; }
					}
				} break;
				default: {
					throw new CException('Invalid object type: '.$this->objectType);
				} break;
			}

			//update common tracking
			$this->status = $newStatus;
			if (!$this->save()) { throw new CException('Error while updating commontrack status'); }

			if (isset($transaction)) { $transaction->commit(); }
			return true;

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;//return false;
		}
	}

	public function getObjectTypeTranslation($objectType)
	{
		$form = new ReportFieldsForm(array());
		$translations = $form->getLearningObjectTypes();
		if(isset($translations[$objectType]))
			return $translations[$objectType];
		return $objectType;
	}


	public function beforeDelete() {
		// If this is a central LO track with shared tracking
		// let's remove all slave tracks (if any) for this user
		if($this->organization && $this->organization->repositoryObject && $this->organization->repositoryObject->shared_tracking && !$this->idMasterOrg)
			LearningCommontrack::model()->deleteAllByAttributes(array('idUser' => $this->idUser, 'idMasterOrg' => $this->idReference));

		return parent::beforeDelete();
	}

	public function afterSave()
	{
		$courseuserModel = LearningCourseuser::model()->findByAttributes(array(
			'idCourse' => $this->organization ? $this->organization->idCourse : null,
			'idUser'   => $this->idUser,
		));

        // Just informing the Plugins that we just saved LearningCommontrack model
        $event = new DEvent($this, array('commonTrack' => $this, 'courseUser' => $courseuserModel));
        Yii::app()->event->raise('afterLearningCommontrackSaved', $event);




			//If it's a TEST then raise the notification event for a completed test.
		if($this->objectType == LearningOrganization::OBJECT_TYPE_TEST){
			if($this->oldAttributes['status'] == self::STATUS_ATTEMPTED && $this->getScenario() != self::SCENARIO_SYNC) //|| $$this->oldAttributes['status'] == self::STATUS_FAILED)
			{
				$modOrigin = LearningOrganization::getModOrigin($this->idReference);
				$loModel = LearningOrganization::model()->findByPk($this->idReference);

				//Raising TestCompleted Notification
				if($loModel) {
					$courseModel = ($modOrigin && $modOrigin != $this->idReference)? LearningCourse::model()->findByPk(LearningOrganization::model()->findByPk($modOrigin)->idCourse) : LearningCourse::model()->findByPk($loModel->idCourse);

					$userModel = CoreUser::model()->findByPk($this->idUser);
					switch($this->status){
						case self::STATUS_COMPLETED:
						case self::STATUS_PASSED:
						case self::STATUS_FAILED:
							//Check if there is an "extended text" or "upload" question. Then evaluation is needed.
							if ($loModel->testHasSpecialQuestions()) {
								//Raising TestNeedsEvaluation Notification
								$event = new DEvent($this, array(
										'user' => $userModel,
										'course' => $courseModel,
										'learningobject' => $loModel,
								));
								Yii::app()->event->raise('UserTestEvaluationNeeded', $event);
							}

							//Raising TestCompleted Notification
							$event = new DEvent($this, array(
									'user' => $userModel,
									'course' => $courseModel,
									'learningobject' => $loModel,
							));
							Yii::app()->event->raise('UserCompletedTest', $event);

							break;
						}

				}

			}

		}

		if ($this->status == self::STATUS_COMPLETED) {

			// LO Completed

			if ($courseuserModel) {
				$courseuserModel->skip_certificate_generation = $this->skip_certificate_generation;

				//classroom courses are managed differently
				$courseModel = LearningCourse::model()->findByPk($this->organization->idCourse);
				if ($courseModel->isClassroomCourse() && (($this->objectType == LearningOrganization::OBJECT_TYPE_TEST && !$courseModel->hasEndObjectLo()) || $this->organization->isTerminator == 1)) {
					$manualEvaluationSessions = Yii::app()->db->createCommand()
							->select('cs.id_session, cs.course_id,cus.id_user')
							->from(LtCourseSession::model()->tableName().' cs')
							->join(LtCourseuserSession::model()->tableName() . ' cus', 'cs.id_session=cus.id_session')
							->where('cs.evaluation_type=:type AND cs.course_id=:courseId AND cus.id_user=:idUser', array(
								':type' => LtCourseSession::EVALUATION_TYPE_SCORE,
								':courseId' => $courseModel->idCourse,
								':idUser' => Yii::app()->user->idst,
							))->queryAll();
					if($courseModel->hasEndObjectLo()){
						$onlineSessions = LtCourseSession::model()->findAllByAttributes(array(
							'course_id' => $courseModel->getPrimaryKey()
						));
					}else{
						$onlineSessions = LtCourseSession::model()->findAllByAttributes(array(
							'evaluation_type' => LtCourseSession::EVALUATION_TYPE_ONLINE,
							'course_id' => $courseModel->getPrimaryKey()
						));
					}

					//TODO: we should refactor this process finding user sessions with online test using a single query, it is more efficient
					// if we HAVE AT LEAST ONE session, that is mark as "manual evaluation" setting, in which current user is enrolled,
					// DO NOT pass session evaluation and not allow the course itself to be completed
					if (!empty($onlineSessions) && empty($manualEvaluationSessions)) {
						//for each retrieved session, find user session enrollment and update the status
						foreach ($onlineSessions as $onlineSession) {
							$sessionEnrollment = LtCourseuserSession::model()->findByPk(array(
								'id_user' => $this->idUser,
								'id_session' => $onlineSession->getPrimaryKey()
							));
							if ($sessionEnrollment) {
								/* @var $sessionEnrollment LtCourseuserSession */

								$rs = $sessionEnrollment->setEvaluation(LtCourseuserSession::EVALUATION_STATUS_PASSED, $this->score, null);
								//NOTE: the course is considered completed only when all user sessions have been completed.
								//this check is performed in LtCourseuserSession::afterSave(), which is called when updating the
								//user evaluation with above "setEvaluation()" function
								if (!$rs) {
									//TODO: what to do when errors occur ?
								}
							}
						}
					}
					if($this->organization){
						// Update 'score_given' in learning_courseuser
						if(empty($this->oldAttributes) || $this->oldAttributes['score'] != $this->score){
							LearningCourseuser::recalculateLastScoreByUserAndCourse($this->organization->idCourse, $this->idUser);

							// update initial_score_given in learning_courseuser
							LearningCourseuser::recalculateInitialScoreGiven($this->idUser, $this->organization->idCourse);
						}
					}
					// If we're updating/creating the master track -> synch slave tracks
					if(!$this->idMasterOrg)
						$this->synchSlaveTracks();

					//find all sessions evaluated by online test
					parent::afterSave();
					return; //end classroom checks
				}
				else if ($courseModel->isWebinarCourse())
				{
					$onlineSessions = WebinarSession::model()->findAllByAttributes(array(
						'evaluation_type' => WebinarSession::EVAL_TYPE_ONLINE_TEST,
						'course_id' => $courseModel->getPrimaryKey()
					));
					//TODO: we should refactor this process finding user sessions with online test using a single query, it is more efficient
					if (!empty($onlineSessions) && (($this->objectType == LearningOrganization::OBJECT_TYPE_TEST && !$courseModel->hasEndObjectLo()) || $this->organization->isTerminator == 1)) {
						//for each retrieved session, find user session enrollment and update the status
						foreach ($onlineSessions as $onlineSession) {
							$sessionEnrollment = WebinarSessionUser::model()->findByPk(array(
								'id_user' => $this->idUser,
								'id_session' => $onlineSession->getPrimaryKey()
							));
							if ($sessionEnrollment) {
								/* @var $sessionEnrollment WebinarSessionUser */
								$rs = $sessionEnrollment->setEvaluation(WebinarSessionUser::EVALUATION_STATUS_PASSED, $this->score, null);
								//NOTE: the course is considered completed only when all user sessions have been completed.
								//this check is performed in LtCourseuserSession::afterSave(), which is called when updating the
								//user evaluation with above "setEvaluation()" function
								if (!$rs) {
									//TODO: what to do when errors occur ?
								}
							}
						}
					}
					if($this->organization){
						// Update 'score_given' in learning_courseuser
						if(empty($this->oldAttributes) || $this->oldAttributes['score'] != $this->score){
							LearningCourseuser::recalculateLastScoreByUserAndCourse($this->organization->idCourse, $this->idUser);

							// update initial_score_given in learning_courseuser
							LearningCourseuser::recalculateInitialScoreGiven($this->idUser, $this->organization->idCourse);
						}
					}
					// If we're updating/creating the master track -> synch slave tracks
					if(!$this->idMasterOrg)
						$this->synchSlaveTracks();

					//find all sessions evaluated by online test
					parent::afterSave();
					return; //end classroom checks

				} elseif($courseModel->isWebinarCourse() || $courseModel->isClassroomCourse()) {
					// If we're updating/creating the master track -> synch slave tracks
					if(!$this->idMasterOrg)
						$this->synchSlaveTracks();

					parent::afterSave();
					return; //end classroom checks
				}

				//do checks for standard e-learning online courses
				if ($this->organization->isTerminator == 1) {

					// The current LO is Completed and it is a "End object marker",
					// so mark the user's subscription for the whole course as Completed

					$courseuserModel->status = LearningCourseuser::$COURSE_USER_END;
					$courseuserModel->save(FALSE);

				} else {

					$numberOfLos = Yii::app()->db->createCommand()
						->select('COUNT(lo.idOrg)')
						->from(LearningOrganization::model()->tableName().' lo')
						->where('lo.idCourse = :idCourse AND lo.objectType != "" AND lo.visible = 1', array(
							':idCourse' => $this->organization->idCourse,
						))
						->queryScalar();
					if((int)$numberOfLos > 0) {
						// Check if all LOs are completed by the user and if yes, mark his
						// course subscription as completed
						$notCompletedLos = Yii::app()->db->createCommand()
							->select('COUNT(lo.idOrg)')
							->from(LearningOrganization::model()->tableName().' lo')
							->leftJoin(LearningCommontrack::model()->tableName().' ct',
								'lo.idOrg = ct.idReference AND lo.objectType = ct.objectType AND lo.idCourse=:idCourse AND ct.idUser =:idUser AND
								 ct.idTrack IN (
										SELECT MIN(idTrack) FROM learning_commontrack as ctt WHERE  ctt.idReference = lo.idOrg  AND  ctt.objectType = lo.objectType  AND  ctt.idUser =:idUser
								)',
								array(
									':idCourse' => $this->organization->idCourse,
									':idUser' => $this->idUser,
								)
							)
							->where('lo.visible = 1')
							->andWhere('lo.objectType != ""')
							->andWhere('lo.idCourse=:idCourse', array(':idCourse' => $this->organization->idCourse))
							// The user hasn't started the LO yet at all
							// Or the LO is in progress (status!=completed)
							->andWhere('ct.idReference IS NULL OR ct.status != :completed', array(':completed' => LearningCommontrack::STATUS_COMPLETED))
							->queryScalar();

						if((int)$notCompletedLos == 0){
							// All LOs completed by the user
							$courseuserModel->status = LearningCourseuser::$COURSE_USER_END;
							$courseuserModel->save(FALSE);
						}
					}
				}

			}
		}

		if($courseuserModel && $this->organization && $this->getScenario() != self::SCENARIO_USER_SUBSCRIBED_COURSE){
			// Update 'score_given' in learning_courseuser
			if(empty($this->oldAttributes) || $this->oldAttributes['score'] != $this->score){
				LearningCourseuser::recalculateLastScoreByUserAndCourse($this->organization->idCourse, $this->idUser, false, false, $this->skip_certificate_generation);

				// update initial_score_given in learning_courseuser
				LearningCourseuser::recalculateInitialScoreGiven($this->idUser, $this->organization->idCourse, $this->skip_certificate_generation);
			}
		}

		// If we're updating/creating the master track -> synch slave tracks
		if(!$this->idMasterOrg)
			$this->synchSlaveTracks();

		parent::afterSave();
	}

	/**
	 * Updates/creates the slave tracks for central LO shared object
	 */
	private function synchSlaveTracks() {
		LearningOrganization::removeModOrigin($this->organization->idOrg);
		$organization = $this->organization;
		$idUser = $this->idUser;
		if(!$organization)
			return;

		// Central LO: check if this object is shared and has shared tracking ON
		if($organization->repositoryObject && $organization->repositoryObject->shared_tracking && $organization->objectVersion) {
			// Find all objects with a placeholder for this central LO object version and where the current user is enrolled in
			$placeholdersIdOrgs = $organization->objectVersion->getCoursePlaceholdersForThisObject($idUser, array($organization->idOrg));

			// Shared tracking is ON -> propagate this update to all Slaves tracks
			foreach($placeholdersIdOrgs as $placeholdersIdOrg) {

				$slaveTrack = LearningCommontrack::model()->findByAttributes(array('idUser' => $idUser, 'idReference' => $placeholdersIdOrg));
				if(!$slaveTrack) {
					$slaveTrack = new LearningCommontrack();
					$slaveTrack->idReference = $placeholdersIdOrg;
					$slaveTrack->idUser = $idUser;
					$slaveTrack->idTrack = $this->idTrack;
					$slaveTrack->objectType = $this->objectType;
					$slaveTrack->idMasterOrg = $this->idReference;
				}

				$slaveTrack->idResource = $this->idResource;
				$slaveTrack->dateAttempt = $this->dateAttempt ? Yii::app()->localtime->toLocalDateTime($this->dateAttempt) : null;
				$slaveTrack->status = $this->status;
				$slaveTrack->firstAttempt = $this->firstAttempt ? Yii::app()->localtime->toLocalDateTime($this->firstAttempt) : null;
				$slaveTrack->first_complete = $this->first_complete ? Yii::app()->localtime->toLocalDateTime($this->first_complete) : null;
				$slaveTrack->last_complete = $this->last_complete ? Yii::app()->localtime->toLocalDateTime($this->last_complete) : null;
				$slaveTrack->first_score = $this->first_score;
				$slaveTrack->score = $this->score;
				$slaveTrack->score_max = $this->score_max;
				$slaveTrack->total_time = $this->total_time;
				$slaveTrack->setScenario(self::SCENARIO_SYNC);
				$slaveTrack->save();
			}
		}
	}



	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
		// If this tracking info is related to CURRENT Logged in user, update CourseUser access as well.
		// Note: Do NOT user $this->idUser as this operation here might be initiated by Admin changing someone elses LO statuses!
		try {
			if (Yii::app()->user->id == $this->idUser) {
				$courseId = Yii::app()->session['idCourse'];
				if(!$courseId || $courseId == $this->course->idCourse)
					LearningCourseuser::updateCourseUserAccess($this->idUser, $this->course->idCourse);
			}
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), 'error', __CLASS__);
		}

		if($this->getIsNewRecord()){
			$criteria = new CDbCriteria();
			$criteria->compare('idReference', $this->idReference);
			$criteria->compare('idUser', $this->idUser);
			$criteria->compare('objectType', $this->objectType);
			$exists = LearningCommontrack::model()->find($criteria);
			if($exists){
				// Trying to create a duplicate idReference+idUser+objectType pair
				// Usually we should fail this operation since it shouldn't exist,
				// but for now we just log it in an attempt to find out the cause
				Yii::log('=============', CLogger::LEVEL_ERROR);
				Yii::log('Attempting to create duplicate idReference+idUser+objectType pair in learning_commontrack', CLogger::LEVEL_ERROR);
				Yii::log('Current user: '. Yii::app()->user->getId(), CLogger::LEVEL_ERROR);
				Yii::log('Attributes for current record:'.print_r($this->attributes), CLogger::LEVEL_ERROR);
				Yii::log('Attributes for existing record:'.print_r($exists->attributes), CLogger::LEVEL_ERROR);
				Yii::log('Current action:'.Yii::app()->controller->id.'/'.Yii::app()->controller->action->id, CLogger::LEVEL_ERROR);
				Yii::log('=============', CLogger::LEVEL_ERROR);
			}
			
			if ($this->organization) {
                $this->failed_attempts_allowed = $this->organization->course->lo_max_attempts;
			}
			
		}


		return parent::beforeSave();
	}


	public function getEvaluatedDeliverableObjectByUser(){
		$res = false;
		$organizationModel = LearningOrganization::model()->with(array('learningCommontrack'=>array(
			'alias'=>'lc',
			'condition'=>'lc.idUser = :idUser ',
			'params'=> array(':idUser'=>$this->idUser)
		)))->findByPk($this->idReference);

		$criteria = new CDbCriteria();
		$criteria->with = array(
			'deliverableObjects' => array(
				'alias' => 'd',
				'condition' => 'd.id_user = :id_user AND d.evaluation_status = :evaluated',
				'params' => array(':id_user'=>$this->idUser , ':evaluated' => LearningDeliverableObject::STATUS_ACCEPTED),
				'order' => 'created ASC'
			)
		);
		$criteria->addCondition('t.id_deliverable = :id_deliverable');
		$criteria->params[':id_deliverable'] = $organizationModel->idResource;
		$deliverable = LearningDeliverable::model()->find($criteria);
		if($deliverable ){
			$res = true;
		}

		return $res;
	}

	public function getLastCreatedDeliverableObject(){

		$organizationModel = LearningOrganization::model()->with(array('learningCommontrack'=>array(
			'alias'=>'lc',
			'condition'=>'lc.idUser = :idUser ',
			'params'=> array(':idUser'=>$this->idUser)
		)))->findByPk($this->idReference);

	    $criteria = new CDbCriteria();
		$criteria->with = array(
			'deliverable' => array(
				'alias' => 'd',
				'condition' => 'd.id_deliverable = :id_deliverable',
				'params' => array(':id_deliverable'=>$organizationModel->idResource),
			)
		);
		$criteria->addCondition('t.id_user = :id_user');
		$criteria->params[':id_user'] = $this->idUser ;
		$criteria->order = 'created DESC';
		$criteria->limit = '1';
		$lastDeliverable = LearningDeliverableObject::model()->find($criteria);
		return $lastDeliverable;
	}

	/**
	 * return the highest score by scorm
	 * @param $idResource
	 * @param string $fallback
	 * @return string
	 */
	public static function getHighestScoreByScorm($idResource, $fallback = '-') {
		$result = Yii::app()->db->createCommand()
			->select('MAX(ct.score)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG))
			->where('lo.idResource=:idResource', array(':idResource' => $idResource))
			->queryScalar();

		return ($result)? $result : $fallback;
	}

	/**
	 * return the lowest score by scorm
	 * @param $idResource
	 * @param string $fallback
	 * @return string
	 */
	public static function getLowestScoreByScorm($idResource, $fallback = '-') {
		$result = Yii::app()->db->createCommand()
			->select('MIN(ct.score)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG))
			->where('lo.idResource=:idResource', array(':idResource' => $idResource))
			->queryScalar();

		return ($result)? $result : $fallback;
	}

	/**
	 * return the max score by scorm
	 * @param $idResource
	 * @param string $fallback
	 * @return string
	 */
	public static function getMaxScoreByScorm($idResource, $fallback = '-') {
		$result = Yii::app()->db->createCommand()
			->select('MAX(ct.score_max)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG))
			->where('lo.idResource=:idResource', array(':idResource' => $idResource))
			->queryScalar();

		return ($result)? $result : $fallback;
	}

	/**
	 * return the average score by scorm
	 * @param $idResource
	 * @param string $fallback
	 * @return string
	 */
	public static function getAverageScoreByScorm($idResource, $fallback = '-') {
		$result = Yii::app()->db->createCommand()
			->select('AVG(ct.score)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG))
			->where('lo.idResource=:idResource', array(':idResource' => $idResource))
			->queryScalar();

		return ($result)? $result : $fallback;
	}

	/**
	 * return the number of attempts by scorm
	 * @param $idResource
	 * @return mixed
	 */
	public static function countAttemptsByScorm($idResource) {
		$count = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG))
			->where('lo.idResource=:idResource', array(':idResource' => $idResource))
			->queryScalar();

		return $count;
	}

	/**
	 * @param $idResource
	 * @param $status
	 * @return mixed
	 */
	public static function countByStatusAndScorm($idResource, $status) {

		$command = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from('learning_commontrack ct')
			->join('learning_organization lo', 'ct.idReference=lo.idOrg AND lo.objectType = :objectType', array(':objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG));

		if($status == LearningCommontrack::STATUS_PASSED || $status == LearningCommontrack::STATUS_COMPLETED) {
			$command->where('lo.idResource=:idResource AND (ct.status = :passed OR ct.status = :completed)', array(
				':idResource' => $idResource,
				':passed' => LearningCommontrack::STATUS_PASSED,
				':completed' => LearningCommontrack::STATUS_COMPLETED
			));
		} else {
			$command->where('lo.idResource=:idResource AND ct.status = :status', array(
				':idResource' => $idResource,
				':status' => $status
			));
		}

		return $command->queryScalar();
	}

	/**
	 * Reset tracking for a learning organization and user
	 *
	 * @param $idOrg integer ID of LearningOrganization
	 * @param $idCourse integer ID of LearningCourse
	 * @param $idUser integer ID of CoreUser
	 */
	public static function resetTracking($idOrg, $idCourse, $idUser){
		$object = LearningOrganization::model()->findByPk($idOrg);
		// Actually clear LO tracking data for user
		// (confirm checkbox checked)

		// Delete LO tracking data, depending on the LO type they are stored in different tables
		switch($object->objectType){
			case LearningOrganization::OBJECT_TYPE_TEST:
				LearningTesttrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_SCORMORG:
				LearningScormTracking::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				LearningScormItemsTrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_FILE:
				LearningMaterialsTrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_VIDEO:
				LearningVideoTrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_TINCAN:
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				LearningTcTrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_POLL:
				LearningPolltrack::model()->deleteAllByAttributes(array('id_reference' => $idOrg, 'id_user' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_AUTHORING:
				LearningConversion::model()->deleteAllByAttributes(array('authoring_id' => $object->idResource, 'user_id' => $idUser));
				break;
			case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
				$deliverables = LearningDeliverableObject::model()->findAllByAttributes(array('id_deliverable' => $object->idResource, 'id_user' => $idUser));
				$tracked = LearningCommontrack::model()->findAllByAttributes(array('idReference' => $object->idOrg, 'idUser' => $idUser));
				if($tracked) {
					foreach($tracked as $track) {
						$track->delete();
					}
				}
				if($deliverables)
					foreach($deliverables as $deliverable)
						$deliverable->removeEvaluation();
				break;
			case LearningOrganization::OBJECT_TYPE_AICC:
				// Get the package associated to this LO and wipe out trackings for this user
				$package = $object->aiccPackage;
				$package->deleteAllTrackingForUsers($idUser);
				break;

			default:
				break;
		}

		// Finally, delete the common track row itself
		//for deliverable remove only evaluation
		if($object->objectType != LearningOrganization::OBJECT_TYPE_DELIVERABLE)
			LearningCommontrack::model()->deleteAllByAttributes(array('idReference' => $idOrg, 'idUser' => $idUser));

		// If this is a central LO track with shared tracking
		// let's remove all slave tracks (if any) for this user
		if($object->repositoryObject && $object->repositoryObject->shared_tracking)
			LearningCommontrack::model()->deleteAllByAttributes(array('idUser' => $idUser, 'idMasterOrg' => $idOrg));

		// Just informing the Plugins that we just reset User - LO status
		$event = new DEvent(self, array('lo' => $object, 'id_user' => $idUser, 'id_course' => $idCourse));
		Yii::app()->event->raise('afterResetUserLoStatus', $event);
	}

	/**
	 * Finds another "candidate" slave track and makes it the master (instead of the current one)
	 *
	 * @param $idReference The idOrg of the LO
	 * @param $idUser ID of the user
	 * @param $objectType Type of LO
	 *
	 * @return boolean Whether or not a candidate track was elected
	 */
	public static function electNewMasterTrack($idReference, $idUser, $objectType) {
		// Find a slave track to elect as the new master track for this user
		$criteria = new CDbCriteria();
		$criteria->addCondition('idMasterOrg = :idMasterOrg');
		$criteria->addCondition('idUser = :idUser');
		$criteria->params = array(
			':idMasterOrg' => $idReference,
			':idUser' => $idUser
		);
		$candidateMasterTrack = LearningCommontrack::model()->find($criteria);
		if($candidateMasterTrack) {
			// Update found track to make it master
			LearningCommontrack::model()->updateAll(array('idMasterOrg' => null),
				"(idReference = :idReference) AND (idUser = :idUser)", array (
					':idReference' => $candidateMasterTrack->idReference,
					':idUser' => $idUser
				)
			);

			// Link other slave tracks for this user to the new master
			LearningCommontrack::model()->updateAll(array('idMasterOrg' => $candidateMasterTrack->idReference),
				"(idMasterOrg = :idOldMasterOrg) AND (idUser = :idUser)", array (
					':idOldMasterOrg' => $idReference,
					':idUser' => $idUser
				)
			);

			// It's time to update the resource specific tracking to point to the new track (not all of them)
			$sql = null;
			switch($objectType) {
				case LearningOrganization::OBJECT_TYPE_AICC:
					$sql = "UPDATE learning_aicc_item_track SET idOrg = :idNewOrg WHERE idOrg = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				case LearningOrganization::OBJECT_TYPE_TINCAN:
					$sql = "UPDATE learning_tc_track SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_FILE:
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
					$sql = "UPDATE learning_materials_track SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_POLL:
					$sql = "UPDATE learning_polltrack SET id_reference = :idNewOrg WHERE id_reference = :idOldOrg AND id_user = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_TEST:
					$sql = "UPDATE learning_testtrack SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					$sql = "UPDATE learning_scorm_items_track SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser;
									UPDATE learning_scorm_tracking SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser;";
					break;
				case LearningOrganization::OBJECT_TYPE_VIDEO:
					$sql = "UPDATE learning_video_track SET idReference = :idNewOrg WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
			}

			if($sql)
				Yii::app()->db->createCommand($sql)
					->bindValues(array(":idNewOrg" => $candidateMasterTrack->idReference, ":idOldOrg" => $idReference, ':idUser' => $idUser))
					->execute();
		} else {
			Yii::log("No candidate track can be found for LO = ".$idReference." and idUser = ".$idUser.". We need to clean up object specific tracks that were referencing the old commontrack.", CLogger::LEVEL_INFO);

			$idResource = Yii::app()->db->createCommand("SELECT idResource FROM learning_organization WHERE idOrg = :idOrg")
				->queryScalar(array(':idOrg' => $idReference));
			$sql = null;
			$bindValues = array(":idOldOrg" => $idReference, ':idUser' => $idUser);
			switch($objectType) {
				case LearningOrganization::OBJECT_TYPE_AICC:
					$sql = "
						DELETE FROM learning_aicc_package_track WHERE id_package = :idResource AND idUser = :idUser;
						DELETE FROM learning_aicc_item_track WHERE idOrg = :idOldOrg AND idUser = :idUser;";
					$bindValues[":idResource"] = $idResource;
					break;
				case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				case LearningOrganization::OBJECT_TYPE_TINCAN:
					$sql = "DELETE FROM learning_tc_track WHERE idReference = :idOldOrg AND idUser = :idUser;";
					break;
				case LearningOrganization::OBJECT_TYPE_FILE:
				case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
					$sql = "DELETE FROM learning_materials_track WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_POLL:
					$sql = "DELETE FROM learning_polltrack WHERE id_reference = :idOldOrg AND id_user = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_TEST:
					$sql = "DELETE FROM learning_testtrack WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					$sql = "DELETE FROM learning_scorm_tracking WHERE idReference = :idOldOrg AND idUser = :idUser;
							DELETE FROM learning_scorm_items_track WHERE idReference = :idOldOrg AND idUser = :idUser;";
					break;
				case LearningOrganization::OBJECT_TYPE_VIDEO:
					$sql = "DELETE FROM learning_video_track WHERE idReference = :idOldOrg AND idUser = :idUser";
					break;
			}

			if($sql)
				Yii::app()->db->createCommand($sql)
					->bindValues($bindValues)
					->execute();
		}

		return !$candidateMasterTrack;
	}
}