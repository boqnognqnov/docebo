<?php
/**
 * 
 * AR class representing so called "IFRAME" enrollment custom fields.
 *
 */
class LearningEnrollmentFieldIframe extends LearningEnrollmentField {
    
    const AUTH_CODE_LIFETIME = 30;

    /**
     * Return list of available OAuth App/Clients
     * 
     * @return array
     */
    public function getOuathClients() {
        $clientsList = array();
        foreach (OauthClients::sqlDataProvider()->data as $client) {
            $clientsList[$client['client_id']] = $client["app_name"];
        }
        return $clientsList;
    }
    
    /**
     * Render field settings HTML
     * 
     * {@inheritDoc}
     * @see LearningEnrollmentField::renderFieldSettings()
     */
    public function renderFieldSettings() {
        return Yii::app()->controller->renderPartial("admin.protected.views.additionalEnrollmentFields._iframe_settings", array(
            'model'       => $this,
            'clientsList' => $this->getOuathClients(),
        ));
    }
    
    /**
     * Render IFRAME custom enrollment field.
     * 
     * Can be used for single or mass enrollment selection UI and operations
     * 
     * @param array  Array of arrays, which elements represent one single enrollment (user + course info) 
     * @return string
     */
    public function renderIframe($enrollments) {
    	$html = Yii::app()->controller->renderPartial("admin.protected.views.additionalEnrollmentFields._iframe_view", array(
    		'fieldModel'	=> $this,
    		'enrollments' 	=> $enrollments,
    	));
    	return $html;
    }
    
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LearningCourseField the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    

    /**
     * Returns formatted iframe URL.
     *
     * URL query parts are:<br>
     *      <strong>course_code</strong> Course CODE or ID<br>
     *      <strong>user_id</strong>     i.e. LMS username<br>
     *      <strong>auth_code</strong>   OAuth2 authorization code, sent to client, used to obtain access token from us later<br>
     *      <strong>hash</strong>        SHA256 of the above parameters and a secret key added at the end (salt)<br>
     *
     * This URL points to a Client and it should make an API request back to US (oauth/token) to exchange the above Auth Code for an access token.
     * Autho Code expires in certain amount of time (@see self::AUTH_CODE_LIFETIME)!
     *
     */
    public function getIframeUrl() {
    	
    	$settings = $this->getSettingsArray();
    	
    	// Parse current iframe url
    	$url_parts = parse_url($settings['url']);
    	parse_str($url_parts['query'], $params);
    
    	// Build the params array
    	$paramsToHash = array(
   			'user_id' 	=> urlencode(Yii::app()->user->getUsername()),
   			'auth_code' => $this->getOAuthAuthorizationCode(),
    	);
    	
    	$params = array_merge($params, $paramsToHash);
    
    	// Calculate the sha1 hash of the previous params + the secret
    	$hashSalt = $settings["hash_secret"];
    	$paramsToHash = array_merge($paramsToHash, array($hashSalt));
    	$params['hash'] = hash("sha256", implode(",", $paramsToHash));
    
    	// this will url_encode all values
    	$url_parts['query'] = http_build_query($params);
    
    	$fragment = !empty($url_parts["fragment"]) ? "#" . $url_parts["fragment"] : "";
    	
    	// Return url
    	$url = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . '?' . $url_parts['query'] . $fragment;
    
    	return $url;
    
    }
    
    /**
     * Returns the iframe height using the course field metadata
     * @return int
     */
    public function getIframeHeight() {
    	$settings = $this->getSettingsArray();
    	return $settings['height'] ? $settings['height'] : self::$default_iframe_height;
    }
    
    /**
     * Generate and return OAuth2 authorization code for TR Accreditation client
     * @return string
     */
    private function getOAuthAuthorizationCode()
    {
    	$authCode = "";
    	$settings = $this->getSettingsArray();
    	$clientId = $settings['client_id'];
    	$server = DoceboOauth2Server::getInstance();
    	if ($server) {
    		$server->init();
    		$authCode = $server->generateAuthorizationCode(Yii::app()->user->id, $clientId, null, 'api', self::AUTH_CODE_LIFETIME);
    	}
    	return $authCode;
    }
    
    /**
     * Prepare array of information for ONE enrollment, passed to IFRAME field(s) (postMessage)
     * 
     * @param array  $enrollment
     * @return array
     */
    public function prepareSingleEnrollmentInfo($enrollment) {
        return array(
            "user_id"       => $enrollment->idUser,
            "username"      => $enrollment->user->getUserNameFormatted(),
            "course_id"     => $enrollment->idCourse,
            "course_code"   => ($enrollment->course->code) ? $enrollment->course->code : $enrollment->idCourse
        );
    }
 
    /**
     * Accepts a data structure (see function body comment), prepare a special array of data and update enrollment fields.
     * The incoming data containes iformation about ONE enrollment field for ONE course AND many users.
     * 
     * "fieldData" array comes from IFRAME itself (sent through a postMessage).
     * The rest is a result of user selection in Enrollments Management UI (so, NOT from IFRAME) 
     * This methods tries to fill the gaps: missing enrollments (usin UI selection), missing default and so on
     * 
     * @param array $data
     */
    public function processSingleFieldManyEnrollments($data) {
        
        /*
         * Example $data
         
                Array
                (
                    [fieldData] => Array
                    (
                            [enrollments] => Array
                                (
                                    [0] => Array
                                        (
                                            [course_id] => 12345
                                            [user_id] => 14333
                                            [data] => Array
                                                (
                                                    [f1] => 101
                                                    [f2] => something1
                                                )
                
                                        )
                
                                    [1] => Array
                                        (
                                            [course_id] => 987
                                            [user_id] => 11987
                                            [data] => Array
                                                (
                                                    [f1] => 102
                                                    [f2] => something2
                                                )
                
                                        )
                
                                )
                
                            [default] => Array
                                (
                                    [f1] => 55
                                    [f2] => something default
                                )
                
                     )
                
                    [fieldId] => 13
                    [courseId] => 866
                    [sessionId] => 
                    [userIds] => Array
                    (
                      [0] => 13064
                    )
                
                )
         
         */
        
        
        // If enrollments is not passed, we build our own, based on list of users and the (single!) course we get
        if (!isset($data['fieldData']['enrollments'])) {
            // But if there is no "default" field data provided, get out
            if (!isset($data['fieldData']['default'])) {
                return;
            }
            $enrollments = array();
            foreach ($data['userIds'] as $userId) {
                $enrollments[] = array(
                    'course_id'         => $data['courseId'],
                    'user_id'           => $userId,
                    'data'              => $data['fieldData']['default'], 
                );
            }
        }
        else {
            $enrollments = $data['fieldData']['enrollments'];
        }

        $defaultData = isset($data['fieldData']['default']) ? $data['fieldData']['default'] : false;
        $finalEnrollments = array();
        foreach ($enrollments as $index => $enrollment) {
            if (!isset($enrollment['data'])) {
                if ($defaultData !== false) {
                    $enrollment['data'] = $defaultData;
                }
            }

            // Set the field ID we are dealing with
            $enrollment['fieldId'] = $data['fieldId'];

            // No 'data' is NOT good. We just skip this enrollment (nothing to update)
            if (isset($enrollment['data'])) {
                $finalEnrollments[] = $enrollment;
         
            }
        }
        
        foreach ($finalEnrollments as $info) {
            $enrollmentModel = LearningCourseuser::model()->findByAttributes(array("idUser" => $info['user_id'], "idCourse" => $info['course_id']));
            if ($enrollmentModel) {
                $fieldsData = json_decode($enrollmentModel->enrollment_fields, true);
                $fieldsData[$info['fieldId']] = $info['data'];
                $enrollmentModel->enrollment_fields = json_encode($fieldsData);
                $enrollmentModel->save();
            }
        }
        
    
    }
    
}