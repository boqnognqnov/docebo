<?php

/**
 * This is the model class for table "mobile_pushnotification".
 *
 * The followings are the available columns in table 'core_field':
 * @property string $device_token
 * @property string $os
 * @property integer $id_user
 * @property string $registration_id
 */
class MobilePushnotification extends CActiveRecord {

	const OS_IOS = 'ios';
	const OS_ANDROID = 'android';
	const OS_WINDOWS = 'windows';


	/**
	 * Check if a OS definition is of allowed value
	 * @param string $os
	 * @return boolean
	 */
	public static function isValidOS($os) {

		$os = strtolower($os);
		if ($os == self::OS_IOS) return true;
		if ($os == self::OS_ANDROID) return true;
		if ($os == self::OS_WINDOWS) return true;
		return false;
	}



	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreField the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function init() {
		parent::init();
		//...
		return true;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'mobile_pushnotification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user', 'numerical', 'integerOnly'=>true),
			array('device_token, os', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('device_token, os, id_user', 'safe', 'on'=>'search')
		);
	}



	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'core_user'=>array(self::HAS_MANY, 'CoreUser', '', 'on'=> ' mobile_pushnotification.id_user=idst ')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'device_token' => 'Device token',
			'os' => 'Operative system',
			'id_user' => 'Id user',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('device_token',$this->device_token);
		$criteria->compare('os',$this->os);
		$criteria->compare('id_user',$this->id_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page')
			)
		));
	}



	public function dataProvider() {

		$criteria = new CDbCriteria;

		//TODO: this is work in progress
		//$criteria->compare('device_token',$this->device_token);
		//...

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
		));
	}


}