<?php

/**
 * This is the model class for table "core_notification_transport".
 *
 * The followings are the available columns in table 'core_notification_transport':
 * @property integer $id
 * @property string $name
 * @property string $plugin_id
 * @property string $info
 *
 * The followings are the available model relations:
 * @property CoreNotificationTranslation[] $coreNotificationTranslations
 * @property CorePlugin $plugin
 * @property CoreNotification[] $coreNotifications
 */
class CoreNotificationTransport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_notification_transport';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, plugin_id', 'required'),
			array('name', 'length', 'max'=>255),
			array('plugin_id', 'length', 'max'=>11),
			array('info', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, plugin_id, info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreNotificationTranslations' => array(self::HAS_MANY, 'CoreNotificationTranslation', 'transport_id'),
			'plugin' => array(self::BELONGS_TO, 'CorePlugin', 'plugin_id'),
			'coreNotifications' => array(self::MANY_MANY, 'CoreNotification', 'core_notification_transport_activation(transport_id, notification_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'plugin_id' => 'Plugin',
			'info' => 'Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('plugin_id',$this->plugin_id,true);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreNotificationTransport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * This method just retrieves all currently active delivery systems in the LMS
	 * @return array the list of delivery system, each cell of the array is a record with some info
	 */
	public static function getActiveTransportsList() {

		$output = array();

		/* @var $cms CDbCommand */
		$cmd = Yii::app()->db->createCommand();
		$cmd->select("nt.*");
		$cmd->from(self::model()->tableName()." nt");
		$cmd->join(CorePlugin::model()->tableName()." p", "p.plugin_id = nt.plugin_id AND p.is_active = 1");
		$reader = $cmd->query();
		while ($record = $reader->read()) {
			$additionalInfo = CJSON::decode($record['info']);
			if (!empty($additionalInfo) && isset($additionalInfo['manager_class'])) {
				$className = $additionalInfo['manager_class'];
				$transport = array(
					'id' => $record['id'],
					'name' => $record['name'],
					'additional_info' => $additionalInfo,
					'editor_class' => new $className()
				);
				$output[] = $transport;
			}
		}

		return $output;
	}


	/**
	 * Creates a new active transport, checking if it already exists and eventually updating it
	 * @param $name the name of the active transport
	 * @param $pluginId the ID of the referral LMS plugin
	 * @param $info array of various informations to be stored in the transport itself
	 * @return bool result of the operation
	 */
	public static function addActiveTransport($name, $pluginId, $info) {
		$newActiveTransport = self::model()->findByAttributes(array(
			'name' => $name,
			'plugin_id' => $pluginId
		));
		if (empty($newActiveTransport)) {
			$newActiveTransport = new self();
			$newActiveTransport->name = $name;
			$newActiveTransport->plugin_id = $pluginId;
		}
		$newActiveTransport->info = CJSON::encode($info);
		try {
			$result = $newActiveTransport->save();
			if (!$result) {
				throw new CException('Error while saving notification transport info ('.$name.', '.$pluginId.')');
			}
		} catch(CException $e) {
			return false;
		}
		return true;
	}


	/**
	 * Retrieve specific manager class object for the current transport
	 * @return mixed|null
	 */
	public function getManagerObject() {
		$output = null;
		$additionalInfo = CJSON::decode($this->info);
		if (!empty($additionalInfo) && isset($additionalInfo['manager_class'])) {
			$className = $additionalInfo['manager_class'];
			$output = new $className();
		}
		return $output;
	}


	/**
	 * Retrieves a parameter stored in the "additional_info" column, handling all required JSON operations
	 * @param $index the name of the parameter required
	 * @return mixed|null the value of the required paraemter. NULL is returned if parameter doesn't exists
	 */
	public function retrieveAdditionalInfo($index) {
		if (empty($index)) { return null; }
		$additionalInfo = CJSON::decode($this->info);
		if (is_array($additionalInfo) && isset($additionalInfo[$index])) {
			return $additionalInfo[$index];
		}
		return null;
	}


	/**
	 * Stores a parameter in the "additional_info" column, handling all requried JSON operations
	 * @param $index the parameter name
	 * @param $value the value of the parameter to be stored
	 * @return bool success of the operation
	 */
	public function insertAdditionalInfo($index, $value) {
		if (empty($index)) { return false; }
		$additionalInfo = CJSON::decode($this->info);
		if (!is_array($additionalInfo)) {
			$additionalInfo = array();
		}
		$additionalInfo[$index] = $value;
		$this->info = CJSON::encode($additionalInfo);
		return true;
	}

}
