<?php

/**
 * This is the model class for table "core_group_fields".
 *
 * The followings are the available columns in table 'core_group_fields':
 * @property integer $idst
 * @property integer $id_field
 * @property string $mandatory
 * @property string $useraccess
 * @property integer $user_inherit
 * @property integer $user_modify
 */
class CoreGroupFields extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreGroupFields the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_group_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst, id_field, user_inherit, user_modify', 'numerical', 'integerOnly'=>true),
			array('mandatory', 'length', 'max'=>5),
			array('useraccess', 'length', 'max'=>9),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, id_field, mandatory, useraccess, user_inherit, user_modify', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst' => 'Idst',
			'id_field' => 'Id Field',
			'mandatory' => 'Mandatory',
			'useraccess' => 'Useraccess',
			'user_inherit' => 'User Inherit',
			'user_modify' => 'User Modify',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst',$this->idst);
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('mandatory',$this->mandatory,true);
		$criteria->compare('useraccess',$this->useraccess,true);
		$criteria->compare('user_inherit',$this->user_inherit);
		$criteria->compare('user_modify',$this->user_modify);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	/**
	 * Used internally by propagateBranchFieldAssignments() method as a cache pool
	 * @var null
	 */
	private static $_propagationCache = NULL;

	/**
	 * Given a node, check if its fields are assigned to all its descendants
	 * @param int $idNode idOrg (of CoreOrgChartTree table) of the node to be propagated
	 */
	private static function _propagateBranchFields($idNode) {

		$allFields = &self::$_propagationCache['allFields'];
		$allChildren = &self::$_propagationCache['allChildren'];

		$node = self::$_propagationCache['allNodes'][$idNode];
		$children = (isset($allChildren[$node['idOrg']]) ? $allChildren[$node['idOrg']] : array());

		if (is_array($children) && !empty($children)) {

			$nodeIsRoot = (($node['lev']) == 1);
			if (!$nodeIsRoot) {
				$nodeFields = (isset($allFields[$node['idOrg']]) && is_array($allFields[$node['idOrg']]) ? $allFields[$node['idOrg']] : array());
			}
			foreach ($children as $child) {
				$childNode =  self::$_propagationCache['allNodes'][$child];
				if (!$nodeIsRoot) {
					//non-root node: check fields to be assigned (if any)
					$childFields = (isset($allFields[$childNode['idOrg']]) && is_array($allFields[$childNode['idOrg']])
						? $allFields[$childNode['idOrg']]
						: array());
					$fieldsToAdd = !empty($nodeFields)
						? array_diff($nodeFields, $childFields) //extract fields present in parent node but not in the child node
						: array();
					if (!empty($fieldsToAdd)) {
						foreach ($fieldsToAdd as $fieldToAdd) {
							if (!isset($cmd)) {
								//direct insert is more efficient then AR operations (this is because sometimes HUGE trees are involved)
								$cmd = Yii::app()->db->createCommand("INSERT INTO " . CoreGroupFields::model()->tableName() . " (idst, id_field, mandatory) VALUES (:idst, :id_field, 'true')");
							}
							$cmd->execute(array(':idst' => $childNode['idst_ocd'], ':id_field' => $fieldToAdd));
							$allFields[$childNode['idOrg']][] = $fieldToAdd; //update fields assignments information
						}
						self::_propagateBranchFields($childNode['idOrg']);
					}
				} else {
					//root node, just go on with children
					self::_propagateBranchFields($childNode['idOrg']);
				}
			}

		}
	}


	/**
	 * Generic function to make sure that all additional fields assignments to org. branches are set correctly
	 * @param bool $targetNode CoreOrgChartTree|bool (optional) a node to be adjusted. Root is used if no node is passed
	 * @throws CDbException
	 */
	public static function propagateBranchFieldAssignments($targetNode = false) {

		//preliminary check
		if (is_object($targetNode) && get_class($targetNode) == 'CoreOrgChartTree') {
			if ($targetNode->isLeaf()) {
				//no descendants for target node, so no operations have to be performed ... just exit from the function without further operations
				return;
			}
		} else {
			$targetNode = CoreOrgChartTree::getOrgRootNode();
		}

		/* @var $db CDbConnection */
		$db = Yii::app()->db;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		try {

			//prepare cache variable
			self::$_propagationCache = array(
				'allChildren' => array(),
				'allNodes' => array(),
				'allFields' => array()
			);

			// List all existing additional fields (will be useful to avoid non-existing fields references when checking assignments to branches)
			$fields = $db->createCommand("SELECT id_field FROM core_user_field")->queryColumn();

			// Create a map: idst_oc/ocd --> idOrg
			$map = array();
			$tmpLevels = array();
			$reader = $db->createCommand("SELECT * FROM ".CoreOrgChartTree::model()->tableName()." ORDER BY iLeft")->query();
			
			while ($row = $reader->read()) {
				$map[$row['idst_oc']] = $row['idOrg'];
				$map[$row['idst_ocd']] = $row['idOrg'];
				$tmpLevels[$row['lev']][] = array('idOrg' => $row['idOrg'], 'iLeft' => $row['iLeft'], 'iRight' => $row['iRight']);
				self::$_propagationCache['allNodes'][$row['idOrg']] = array(
					'idOrg' => $row['idOrg'],
					'lev' => $row['lev'],
					'iLeft' => $row['iLeft'],
					'iRight' => $row['iRight'],
					'idst_oc' => $row['idst_oc'],
					'idst_ocd' => $row['idst_ocd']
				);
			}
			//create a map for node children too
			foreach ($tmpLevels as $level => $levelNodes) {
				if (!empty($levelNodes)) {
					foreach ($levelNodes as $levelNode) {
						if (($levelNode['iRight'] - $levelNode['iLeft']) > 1) { //if the current node is a leaf, no further checks are needed
							if (isset($tmpLevels[($level + 1)])) {
								foreach ($tmpLevels[($level + 1)] as $subLevelNode) { //search for all current node children in sublevel
									if ($subLevelNode['iLeft'] > $levelNode['iLeft'] && $subLevelNode['iRight'] < $levelNode['iRight']) {
										self::$_propagationCache['allChildren'][$levelNode['idOrg']][] = $subLevelNode['idOrg'];
									}
								}
							}
						}
					}
				}
			}
			$tmpLevels = NULL;
			unset($tmpLevels); //free memory


			//retrieve all assigned fields
			$reader = $db->createCommand("SELECT * FROM ".self::model()->tableName())->query();

			while ($row = $reader->read()) {
				//check if the field is valid
				if (!in_array($row['id_field'], $fields)) {
					//old field IDs, no more existent. Invalid data --> clean it
					$del = $db->createCommand("DELETE FROM ".self::model()->tableName()." WHERE id_field = :id_field");
					$del->execute(array(':id_field' => $row['id_field']));
					continue;
				}
				//retrieve branch idOrg
				$index = $map[$row['idst']];
				if (empty($index)) {
					//this means that some dirt data is present in core_group_fields table, referring to no more existent nodes
					//remove this data
					$del = $db->createCommand("DELETE FROM ".self::model()->tableName()." WHERE idst = :idst");
					$del->execute(array(':idst' => $row['idst']));
					continue;
				}
				//make sure that the fields list exists for this branch
				if (!isset(self::$_propagationCache['allFields'][$index])) {
					self::$_propagationCache['allFields'][$index] = array();
				}
				//store field in the branch fields list
				if (!in_array($row['id_field'], self::$_propagationCache['allFields'][$index])) {
					self::$_propagationCache['allFields'][$index][] = $row['id_field'];
				}
			}

			//recursively check fields assignemnt
			self::_propagateBranchFields($targetNode->getPrimaryKey());

			if (isset($transaction)) { $transaction->commit(); }
			self::$_propagationCache = NULL; //clear cache and free memory

		} catch (Exception $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			self::$_propagationCache = NULL; //clear cache and free memory
			throw $e; //let the above stack to handle this
		}
	}


}