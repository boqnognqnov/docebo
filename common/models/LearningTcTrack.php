<?php

/**
 * This is the model class for table "learning_tc_track".
 *
 * The followings are the available columns in table 'learning_tc_track':
 * @property integer $idTrack
 * @property integer $idReference
 * @property integer $idResource
 * @property integer $idUser
 */
class LearningTcTrack extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTcTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_tc_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idReference, idResource, idUser', 'required'),
			array('idReference, idResource, idUser', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idReference, idResource, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningOrganization' => array(self::HAS_ONE, 'LearningOrganization',
										array('idOrg'=>'idReference'),
										'condition'=>'LearningOrganization.objectType = :type',
										'params'=> array(':type'=>LearningOrganization::OBJECT_TYPE_TINCAN)),
			'learningTcMainActivity' => array(self::HAS_ONE, 'LearningTcActivity', array('id_tc_activity'=>'idResource')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idReference' => 'Id Reference',
			'idResource' => 'Id Resource',
			'idUser' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idResource',$this->idResource);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}