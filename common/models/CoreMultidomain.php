<?php

/**
 * This is the model class for table "core_multidomain".
 *
 * The followings are the available columns in table 'core_multidomain':
 * @property integer $id
 * @property string $name
 * @property string $domain
 * @property string $domain_type
 * @property integer $org_chart
 * @property integer $logo
 * @property integer $active
 * 
 * LOGO & COLORS
 * @property string $pageTitle
 * @property integer $companyLogo
 * @property integer $favicon
 * @property integer $colorScheme
 * 
 * SIGN IN
 * @property integer $signInPageLayoutEnabled
 * @property string $signInPageLayoutOption
 * @property integer $signInPageImage
 * @property string $signInPageImageAspect
 * @property string $signInPageWebPages
 * @property string $signInMinimalType
 * @property string $signInMinimalBackground
 * @property string $signInMinimalVideoFallbackImg
 * @property integer $enableLoginRestriction
 * @property integer $hide_signin_form
 *
 * CUSTOM CSS
 * @property string $customCss
 * 
 * COURSE PLAYER
 * @property integer $coursePlayerEnabled
 * @property string $coursePlayerViewLayout
 * @property integer $coursePlayerImage
 * @property string $coursePlayerImageAspect
 * @property string $coursePlayerHtmlCss
 * 
 * WHITE LABEL
 * @property integer $whiteLabelEnabled
 * @property integer $whiteLabelOverrideSettings
 * @property integer $whiteLabelNamingOverride
 * @property string $whiteLabelPoweredByOption
 * @property string $whiteLabelPoweredByCustomText
 * @property string $whiteLabelContactOption
 * @property string $whiteLabelContactCustomEmail
 * @property integer $replaceDoceboWordOption
 * @property string $replaceDoceboCustomWord
 * @property string $replaceDoceboCustomLink
 * @property string $whiteLabelHeader
 * @property string $whiteLabelHeaderExtUrl
 * @property integer $whiteLabelHeaderExtHeight
 * @property string $whiteLabelFooterExtUrl
 * @property integer $whiteLabelFooterExtHeight
 * @property integer $whitelabelNamingSiteEnable
 *
 * E-COMMERCE
 * @property integer $ecommerceEnabled
 * @property string $currencySymbol
 * @property string $currency
 * @property integer $payPalEnabled
 * @property string $payPalAccount
 * @property integer $payPalSandbox
 * @property integer $authorizeNetEnabled
 * @property string $authorizeNetLoginId
 * @property string $authorizeNetTransactionKey
 * @property string $authorizeNetMD5Hash
 * @property integer $authorizeNetSandbox
 * @property integer $httpsEnabled
 * @property integer $adyen_enabled
 * @property string $adyen_merchant_account
 * @property string $adyen_hmac_key
 * @property string $adyen_skin_code
 * @property integer $adyen_single_page_hpp
 * @property integer $adyen_sandbox
 * @property integer $cybersource_enabled
 * @property string $cybersource_access_key
 * @property string $cybersource_secret_key
 * @property string $cybersource_profile_id
 * @property integer $cybersource_sandbox_mode
 *
 * Custom Course Catalog
 * @property integer $catalog_custom_settings
 * @property integer $catalog_use_categories_tree
 * @property integer $catalog_external
 * @property string $catalog_external_selected_catalogs
 *
 * Stripe options
 * @property integer stripe_enabled
 * @property string $stripe_type
 * @property string $stripe_tax_code
 * @property integer stripe_alipay_enabled
 * @property string stripe_public_key
 * @property string stripe_private_key
 * @property string stripe_account_email
 * @property integer stripe_sandbox
 * @property string stripe_public_key_test
 * @property string stripe_private_key_test
 *
 * Self Registration
 * @property integer $enable_self_registration_settings
 * @property integer $register_type
 * @property integer $disable_registration_email_confirmation
 * @property string $allowed_domains
 *
 * Language
 * @property string $default_language
 * @property string $disabled_languages
 * @property integer $use_custom_settings
 * @property integer $header_message_active
 *
 * The followings are the available model relations:
 * @property CoreOrgChartTree $orgChart
 * @property CoreMultidomainSigninText[] $signinTexts
 * @property CoreMultidomainWebpage[] $webpages
 * @property CoreHttps[] $https
 *
 * @property string subscription_custom_settings
 */
class CoreMultidomain extends CActiveRecord
{
	const DOMAINTYPE_SUBDOMAIN 		= 'subdomain';
	const DOMAINTYPE_SUBFOLDER 		= 'subfolder';
	const DOMAINTYPE_CUSTOMDOMAIN 	= 'customdomain';
	
	const LOGIN_LAYOUT_TOP 			= 'layout2';
	const LOGIN_LAYOUT_BOTTOM 		= 'layout3';
	const LOGIN_LAYOUT_SIDE 		= 'layout1';
	const LOGIN_LAYOUT_MINIMAL 		= 'layout4';

	const IMAGE_ASPECT_FILL			= 'fill';
	const IMAGE_ASPECT_TILE			= 'tile';
	
	
	const WL_POWEREDBY_DOCEBO		= 'show_docebo';
	const WL_POWEREDBY_HIDE_DOCEBO	= 'hide_docebo';
	const WL_POWEREDBY_CUSTOM_TEXT	= 'custom_text';
	const WL_CONTACT_OPTION_DOCEBO	= 'to_docebo';
	const WL_CONTACT_OPTION_CUSTOM	= 'to_email';
	const WL_FOOTER_IFRAME_HEIGHT           = 30;
	const WL_HEADER_IFRAME_HEIGHT           = 50;

	CONST FREE_SELF_REGISTRATION_CODE = 0;
	CONST MODERATED_SELF_REGISTRATION_CODE = 1;
	CONST REGISTRATION_BY_ADMIN_CODE = 2;

	CONST FREE_SELF_REGISTRATION = 'self';
	CONST MODERATED_SELF_REGISTRATION = 'moderate';
	CONST REGISTRATION_BY_ADMIN = 'admin';

	/**
	 * Helper Identifier for various settings, grouped by logic
	 * Also used in JS/CSS, so mind when you change them! Better DON'T CHANGE!
	 * 
	 * @var string
	 */
	const GROUPSETTING_LOGO_AND_COLORS			= 'multidomain-settings-logo-and-colors';
	const GROUPSETTING_SIGNIN					= 'multidomain-settings-sign-in-page';
	const GROUPSETTING_CUSTIOM_CSS				= 'multidomain-settings-custom-css';
	const GROUPSETTING_COURSE_PLAYER			= 'multidomain-settings-course-player';
	const GROUPSETTING_WHITE_LABEL				= 'multidomain-settings-white-label';
	const GROUPSETTING_HTTPS					= 'multidomain-settings-https';
	const GROUPSETTING_ECOMMERCE				= 'multidomain-settings-ecommerce';
	const GROUPSETTING_COURSECATALOG			= 'multidomain-settings-coursecatalog';
	const GROUPSETTING_SELF_REGISTRATION		= 'multidomain-settings-self-registration';
	const GROUPSETTING_LOCALIZATION				= 'multidomain-settings-localization';
	const GROUPSETTING_SUBSCRIPTION_SETTINGS	= 'multidomain-settings-subscription';

	public $search_input = null;

	public $allowed_domains;
	public $hide_signin_form;

	public $register_types = array(
		self::FREE_SELF_REGISTRATION_CODE => self::FREE_SELF_REGISTRATION,
		self::MODERATED_SELF_REGISTRATION_CODE => self::MODERATED_SELF_REGISTRATION,
		self::REGISTRATION_BY_ADMIN_CODE => self::REGISTRATION_BY_ADMIN,
	);

	public $register_type_codes = array(
		self::FREE_SELF_REGISTRATION => self::FREE_SELF_REGISTRATION_CODE,
		self::MODERATED_SELF_REGISTRATION => self::MODERATED_SELF_REGISTRATION_CODE,
		self::REGISTRATION_BY_ADMIN => self::REGISTRATION_BY_ADMIN_CODE,
	);


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_multidomain';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, domain, domain_type', 'required'),
			array('org_chart,hide_signin_form, logo, active, companyLogo, favicon, colorScheme, signInPageLayoutEnabled, signInPageImage,
			 coursePlayerEnabled, coursePlayerImage, whiteLabelEnabled, replaceDoceboWordOption, ecommerceEnabled, payPalEnabled,
			  payPalSandbox, authorizeNetEnabled, authorizeNetSandbox, httpsEnabled, adyen_enabled, adyen_single_page_hpp,
			  adyen_sandbox, whitelabelNamingSiteEnable, use_custom_settings, header_message_active, enableLoginRestriction', 'numerical', 'integerOnly'=>true),
			array('whiteLabelHeaderExtHeight', 'numerical', 'integerOnly'=>true, 'min' => 50),
			array('whiteLabelFooterExtHeight', 'numerical', 'integerOnly'=>true, 'min' => 30),
			array('name, default_language', 'length', 'max'=>128),
			array('domain', 'length', 'max'=>256),
			array('domain_type, signInPageLayoutOption, coursePlayerViewLayout, whiteLabelPoweredByOption, whiteLabelContactOption', 'length', 'max'=>32),
			array('pageTitle, whiteLabelPoweredByCustomText, whiteLabelContactCustomEmail, replaceDoceboCustomWord, payPalAccount,
			authorizeNetLoginId, adyen_merchant_account, adyen_hmac_key, adyen_skin_code, whiteLabelHeader, stripe_tax_code', 'length', 'max'=>255),
			array('stripe_type', 'length', 'max'=>10),
			array('signInPageImageAspect, coursePlayerImageAspect', 'length', 'max'=>16),
			array('whiteLabelHeaderExtUrl, whiteLabelFooterExtUrl', 'url', 'allowEmpty'=>true),
			array('replaceDoceboCustomLink, disabled_languages', 'length', 'max'=>1024),
			array('currencySymbol, currency', 'length', 'max'=>8),
			array('authorizeNetTransactionKey, authorizeNetMD5Hash', 'length', 'max'=>512),
			array('allowed_domains', 'domainValidation'),
			array('signInPageWebPages, hide_signin_form, customCss, coursePlayerHtmlCss, catalog_external_selected_catalogs,
			catalog_custom_settings, catalog_use_categories_tree, catalog_external, signInMinimalType,
			signInMinimalBackground, signInMinimalVideoFallbackImg, stripe_enabled, stripe_alipay_enabled, stripe_private_key_test,
			stripe_public_key_test, stripe_private_key, stripe_public_key, stripe_sandbox, stripe_account_email,
			enable_self_registration_settings, register_type, disable_registration_email_confirmation, allowed_domains, whiteLabelOverrideSettings, whiteLabelNamingOverride,
			cybersource_enabled, cybersource_access_key, cybersource_secret_key, cybersource_profile_id, cybersource_sandbox_mode', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, domain, domain_type, org_chart, hide_signin_form, logo, active, pageTitle, companyLogo, favicon, colorScheme,
			signInPageLayoutEnabled, signInPageLayoutOption, signInPageImage, signInPageWebPages, customCss, coursePlayerEnabled,
			coursePlayerViewLayout, coursePlayerImage, coursePlayerHtmlCss, whiteLabelEnabled, whiteLabelPoweredByOption,
			whiteLabelPoweredByCustomText, whiteLabelContactOption, whiteLabelContactCustomEmail, replaceDoceboWordOption,
			replaceDoceboCustomWord, replaceDoceboCustomLink, ecommerceEnabled, currencySymbol, currency, payPalEnabled,
			payPalAccount, payPalSandbox, authorizeNetEnabled, authorizeNetLoginId, authorizeNetTransactionKey,
			authorizeNetMD5Hash, authorizeNetSandbox, httpsEnabled, whiteLabelHeader, whiteLabelHeaderExtUrl, whiteLabelHeaderExtHeight,
			whiteLabelFooterExtUrl, whiteLabelFooterExtHeight, whitelabelNamingSiteEnable, signInMinimalType,
			signInMinimalBackground, signInMinimalVideoFallbackImg, whiteLabelOverrideSettings, whiteLabelNamingOverride, enableLoginRestriction', 'safe', 'on'=>'search'),
				
		);
	}

	public function domainValidation($attribute, $params){
		if($this->enable_self_registration_settings){
			foreach($this->allowed_domains as $key => $domain){
				$tmp = "@" . trim($domain, "@");
				$valid = preg_match('/^@[\w]+\.[\w]+/', $tmp, $matches);
				if(!$valid){
					$this->addError('allowed_domains', Yii::t('notification', 'Please provide only valid domain names'));
					return false;
				}
				$this->allowed_domains[$key] = $tmp;
			}
		}
		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orgChart' 		=> array(self::BELONGS_TO, 'CoreOrgChartTree', 'org_chart'),
			'signinTexts' 	=> array(self::HAS_MANY, 'CoreMultidomainSigninText', 'id_multidomain'),
			'webpages' 		=> array(self::HAS_MANY, 'CoreMultidomainWebpage', 'id_multidomain'),
			'https' 		=> array(self::HAS_MANY, 'CoreHttps', 'idMultidomainClient'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		
		return array(
			'id' 							        => 'ID',
			'name' 							        => 'Name',
			'domain' 						        => 'Domain',
			'domain_type' 					        => 'Domain Type',
			'org_chart' 					        => 'Org Chart',
			'logo' 							        => 'Logo',
			'active' 						        => 'Active',
			'pageTitle' 					        => 'Page Title',
			'companyLogo' 					        => 'Company Logo',
			'favicon' 						        => 'Favicon',
			'colorScheme' 					        => 'Color Scheme',
			'signInPageLayoutEnabled' 		        => Yii::t('multidomain','Enable custom settings for this client'),
			'signInPageLayoutOption' 		        => 'Sign In Page Layout Option',
			'signInPageImage' 				        => 'Sign In Page Image',
			'signInPageImageAspect' 		        => 'Sign In Page Image Aspect',
			'signInPageWebPages' 			        => 'Sign In Page Web Pages',
			'signInMinimalType' 			        => 'Sign In Page Minimal Type',
			'signInMinimalBackground'               => 'Sign In Page Minimal Background',
			'signInMinimalVideoFallbackImg'         => 'Sign In Page Minimal Video Fallback Image',
			'enableLoginRestriction'		        => Yii::t('multidomain','Enable multidomain log in restriction'),
			'hide_signin_form'         				=> 'Login form',


			'customCss' 					        => 'Custom Css',
			'coursePlayerEnabled'			        => Yii::t('multidomain','Enable custom settings for this client'),
			'coursePlayerViewLayout' 		        => 'Course Player View Layout',
			'coursePlayerImage' 			        => 'Course Player Image',
			'coursePlayerImageAspect' 		        => 'Course Player Image Aspect',
			'coursePlayerHtmlCss' 			        => 'Course Player Html Css',
			'whiteLabelEnabled'				        => Yii::t('multidomain','Enable custom settings for this client'),
			'whiteLabelOverrideSettings'            => Yii::t('multidomain','Override White Label settings'),
			'whiteLabelNamingOverride'              => Yii::t('multidomain','Override White Label Naming settings'),
			'whiteLabelPoweredByOption' 	        => 'White Label Powered By Option',
			'whiteLabelPoweredByCustomText'         => 'White Label Powered By Custom Text',
			'whiteLabelHeader'                      => Yii::t('templatemanager', 'Header'),
			'whiteLabelHeaderExtUrl'                => Yii::t('templatemanager', 'Header') . ' ' . Yii::t('standard', '_URL'),
			'whiteLabelHeaderExtHeight'             => Yii::t('templatemanager', 'Header') .' '. Yii::t('templatemanager', 'height'),
			'whiteLabelFooterExtUrl'                => Yii::t('templatemanager', 'Foooter') . ' ' . Yii::t('standard', '_URL'),
			'whiteLabelFooterExtHeight'             => Yii::t('templatemanager', 'Foooter') .' '. Yii::t('templatemanager', 'height'),
			'whitelabelNamingSiteEnable'            => Yii::t('branding', 'Replace, where used, the website link www.docebo.com with'),


			'whiteLabelContactOption' 		        => 'White Label Contact Option',
			'whiteLabelContactCustomEmail' 	        => 'White Label Contact Custom Email',
			'replaceDoceboWordOption'		        => Yii::t('branding', 'Replace, in every text, the word Docebo with the word'),
			'replaceDoceboCustomWord' 		        => 'Replace Docebo Custom Word',
			'replaceDoceboCustomLink' 		        => 'Replace Docebo Custom Link',
			'ecommerceEnabled'				        => Yii::t('multidomain','Enable custom settings for this client'),
			'currencySymbol' 				        => 'Currency Symbol',
			'currency' 						        => 'Currency',
			'payPalEnabled'					        => Yii::t('standard', 'Enable'),
			'payPalAccount'					        => Yii::t('configuration', '_PAYPAL_MAIL'),
			'payPalSandbox'					        => Yii::t('configuration', 'Sandbox mode'),
			'authorizeNetEnabled'			        => Yii::t('configuration', 'Enable'),
			'authorizeNetLoginId' 			        => Yii::t('configuration', 'Login ID'),
			'authorizeNetTransactionKey' 	        => Yii::t('configuration', 'Transaction key'),
			'authorizeNetMD5Hash' 			        => Yii::t('configuration', 'MD5 Hash'),
			'authorizeNetSandbox' 			        => Yii::t('configuration', 'Sandbox mode'),
			'httpsEnabled' 					        => Yii::t('multidomain','Enable custom settings for this client'),
			//Adyen gateway
			'adyen_enabled' 				        => Yii::t('configuration', 'Enable'),
			'adyen_merchant_account' 		        => Yii::t('configuration', 'Merchant account'),
			'adyen_hmac_key' 				        => Yii::t('configuration', 'HMAC key'),
			'adyen_skin_code' 				        => Yii::t('configuration', 'Skin Code'),
			'adyen_single_page_hpp' 		        => Yii::t('configuration', 'Single page HPP'),
			'adyen_sandbox' 				        => Yii::t('configuration', 'Sandbox mode'),
			'catalog_custom_settings'               => Yii::t('catalogue', 'Enable custom settings for this client'),
			'catalog_use_categories_tree'           => Yii::t('catalogue', 'Use categories tree'),
			'catalog_external'                      => 'Catalog External',
			'catalog_external_selected_catalogs'    => 'Catalog External Selected Catalogs',
			//Stripe gateway
			'stripe_enabled' 				    	=> Yii::t('configuration', 'Enabled'),
			'stripe_type' 				        	=> Yii::t('standard', '_TYPE'),
			'stripe_tax_code' 				    	=> Yii::t('configuration', 'Tax code'),
			'stripe_alipay_enabled' => Yii::t('configuration', 'Enable Alipay stripe integration'),
			'stripe_public_key_test'			    => Yii::t('configuration', 'Test Publishable Key'),
			'stripe_private_key_test'        		=> Yii::t('configuration', 'Test Secret Key'),
			'stripe_public_key' 				    => Yii::t('configuration', 'Live Publishable Key'),
			'stripe_private_key' 		        	=> Yii::t('configuration', 'Live Secret Key'),
			'stripe_account_contact' 				=> Yii::t('configuration', 'Stripe Account Contact'),
			'stripe_sandbox' 				    	=> Yii::t('configuration', 'Sandbox'),
			//Self Registration
			'enable_self_registration_settings' 	=> Yii::t('multidomain','Enable custom settings for this client'),
			'use_custom_settings'				 	=> Yii::t('multidomain','Enable custom settings for this client'),
			'register_type' 						=> Yii::t('configuration', '_REGISTER_TYPE'),
			'disable_registration_email_confirmation'=> Yii::t('configuration', 'Do not send confirmation email at self-registration'),
			'allowed_domains' 						=> Yii::t('configok auration', 'Allowed Domains'),

			'cybersource_enabled'					=> Yii::t('configuration', 'Enable'),
			'cybersource_sandbox_mode'				=> Yii::t('configuration', 'Sandbox mode')
		);
		
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('domain_type',$this->domain_type,true);
		$criteria->compare('org_chart',$this->org_chart);
		$criteria->compare('logo',$this->logo);
		$criteria->compare('active',$this->active);
		$criteria->compare('pageTitle',$this->pageTitle,true);
		$criteria->compare('companyLogo',$this->companyLogo);
		$criteria->compare('favicon',$this->favicon);
		$criteria->compare('colorScheme',$this->colorScheme);
		$criteria->compare('signInPageLayoutEnabled',$this->signInPageLayoutEnabled);
		$criteria->compare('signInPageLayoutOption',$this->signInPageLayoutOption,true);
		$criteria->compare('signInPageImage',$this->signInPageImage);
		$criteria->compare('signInPageImageAspect',$this->signInPageImageAspect,true);
		$criteria->compare('signInPageWebPages',$this->signInPageWebPages,true);
		$criteria->compare('signInMinimalType',$this->signInMinimalType,true);
		$criteria->compare('signInMinimalBackground',$this->signInMinimalBackground,true);
		$criteria->compare('signInMinimalVideoFallbackImg',$this->signInMinimalVideoFallbackImg,true);
		$criteria->compare('hide_signin_form',$this->hide_signin_form,true);

		$criteria->compare('customCss',$this->customCss,true);
		$criteria->compare('coursePlayerEnabled',$this->coursePlayerEnabled);
		$criteria->compare('coursePlayerViewLayout',$this->coursePlayerViewLayout,true);
		$criteria->compare('coursePlayerImage',$this->coursePlayerImage);
		$criteria->compare('coursePlayerImageAspect',$this->coursePlayerImageAspect,true);
		$criteria->compare('coursePlayerHtmlCss',$this->coursePlayerHtmlCss,true);
		$criteria->compare('whiteLabelEnabled',$this->whiteLabelEnabled);
		$criteria->compare('whiteLabelOverrideSettings',$this->whiteLabelOverrideSettings);
		$criteria->compare('whiteLabelNamingOverride',$this->whiteLabelNamingOverride);
		$criteria->compare('whiteLabelPoweredByOption',$this->whiteLabelPoweredByOption,true);
		$criteria->compare('whiteLabelPoweredByCustomText',$this->whiteLabelPoweredByCustomText,true);

		$criteria->compare('whiteLabelHeader',$this->whiteLabelHeader,true);
		$criteria->compare('whiteLabelHeaderExtUrl',$this->whiteLabelHeaderExtUrl,true);
		$criteria->compare('whiteLabelHeaderExtHeight',$this->whiteLabelHeaderExtHeight,true);
		$criteria->compare('whiteLabelFooterExtUrl',$this->whiteLabelFooterExtUrl,true);
		$criteria->compare('whiteLabelFooterExtHeight',$this->whiteLabelFooterExtHeight,true);
		$criteria->compare('whitelabelNamingSiteEnable',$this->whitelabelNamingSiteEnable,true);

		$criteria->compare('whiteLabelContactOption',$this->whiteLabelContactOption,true);
		$criteria->compare('whiteLabelContactCustomEmail',$this->whiteLabelContactCustomEmail,true);
		$criteria->compare('replaceDoceboWordOption',$this->replaceDoceboWordOption);
		$criteria->compare('replaceDoceboCustomWord',$this->replaceDoceboCustomWord,true);
		$criteria->compare('replaceDoceboCustomLink',$this->replaceDoceboCustomLink,true);
		$criteria->compare('ecommerceEnabled',$this->ecommerceEnabled);
		$criteria->compare('currencySymbol',$this->currencySymbol,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('payPalEnabled',$this->payPalEnabled);
		$criteria->compare('payPalAccount',$this->payPalAccount,true);
		$criteria->compare('payPalSandbox',$this->payPalSandbox);
		$criteria->compare('authorizeNetEnabled',$this->authorizeNetEnabled);
		$criteria->compare('authorizeNetLoginId',$this->authorizeNetLoginId,true);
		$criteria->compare('authorizeNetTransactionKey',$this->authorizeNetTransactionKey,true);
		$criteria->compare('authorizeNetMD5Hash',$this->authorizeNetMD5Hash,true);
		$criteria->compare('authorizeNetSandbox',$this->authorizeNetSandbox);
		$criteria->compare('httpsEnabled',$this->httpsEnabled);
		$criteria->compare('catalog_custom_settings',$this->catalog_custom_settings);
		$criteria->compare('catalog_use_categories_tree',$this->catalog_use_categories_tree);
		$criteria->compare('catalog_external',$this->catalog_external);
		$criteria->compare('catalog_external_selected_catalogs',$this->catalog_external_selected_catalogs,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreMultidomain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProvider() {
	
		// SQL Parameters
		$params = array();
	
		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from($this->tableName(). ' t');
	
		// Also search by user related text
		if ($this->search_input != null) {
			$commandBase->andWhere('CONCAT(t.name, " ", t.domain) LIKE :search');
			$params[':search'] = '%'.$this->search_input.'%';
		}
	
		// DATA
		$commandData = clone $commandBase;
	
	
		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(t.id)');
		$numRecords = $commandCounter->queryScalar($params);
	
	
		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
				'name' => array(
					'asc' 	=> 't.name',
					'desc'	=> 't.name DESC',
				),
				'domain' => array(
					'asc' 	=> 't.domain',
					'desc'	=> 't.domain DESC',
				),
		);
		$sort->defaultOrder = array(
			'name'		=>	CSort::SORT_ASC,
		);
	
		// DATA-II
		$commandData->select("*");
	
	
		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> array('pageSize' => $pageSize),
			'keyField'			=> 'id',
			'sort' 				=> $sort,
			'params'			=> $params,
		);
	
		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);
	
		return $dataProvider;
	
	}


	/**
	 * Return an array of available color schemes, suitable to be used in active form dropdowns
	 * @return array
	 */
	public function getColorSchemesDropdownList() {
		$data = array();
		$schemes = CoreScheme::model()->findAll();
		if (!empty($schemes)) {
			foreach ($schemes as $scheme) {
				$data[$scheme->id] = $scheme->title;
			}
		}
		return $data;
	}
	
	/**
	 * Return array of currency codes
	 *
	 * @return array
	 */
	public static function getCurrencyCodes() {
		return array_change_key_case(LocaleManager::getCurrencyArr(), CASE_UPPER);
	}
	
	
	/**
	 * Check all settings and set default values if required
	 */
	public function setDefaultAttributeValues() {
	
		if (!$this->colorScheme)
			$this->colorScheme = 1;
	
		if (!$this->signInPageLayoutOption)
			$this->signInPageLayoutOption = self::LOGIN_LAYOUT_TOP;
	
		if (!$this->whiteLabelPoweredByOption)
			$this->whiteLabelPoweredByOption = self::WL_POWEREDBY_DOCEBO;
	
		if (!$this->whiteLabelContactOption)
			$this->whiteLabelContactOption = self::WL_CONTACT_OPTION_DOCEBO;

		// SELF REGISTRATION
		if($this->register_type === null)
			$this->register_type = $this->register_type_codes[Settings::get('register_type')];
		if($this->disable_registration_email_confirmation === null)
			$this->disable_registration_email_confirmation = Settings::get('disable_registration_email_confirmation');
		if(!$this->allowed_domains){
			$allowedDomains = Settings::get('allowed_domains');
			if(is_string($allowedDomains)){
				$this->allowed_domains = CJSON::decode($allowedDomains);
			}elseif(is_array($allowedDomains)){
				$this->allowed_domains = $allowedDomains;
			}
		}
	}

	
	public static function getGroupAttributesMap() {
		
		$map = array(
			self::GROUPSETTING_LOGO_AND_COLORS => array(
				'pageTitle',
				'companyLogo',
				'favicon',
				'colorScheme',
			),
			self::GROUPSETTING_SIGNIN => array(
				'signInPageLayoutEnabled',
				'signInPageLayoutOption',
				'signInPageImage',
				'signInPageImageAspect',
				'signInPageWebPages',
				'signInMinimalType',
				'signInMinimalBackground',
				'signInMinimalVideoFallbackImg',
				'hide_signin_form',
			),
			self::GROUPSETTING_CUSTIOM_CSS => array(
				'customCss',
			),
			self::GROUPSETTING_COURSE_PLAYER => array(
				'coursePlayerEnabled',
				'coursePlayerViewLayout',
				'coursePlayerImage',
				'coursePlayerImageAspect',
				'coursePlayerHtmlCss',
			),
			self::GROUPSETTING_WHITE_LABEL => array(
				'whiteLabelEnabled',
				'whiteLabelNamingOverride',
				'whiteLabelOverrideSettings',
				'whiteLabelPoweredByOption',
				'whiteLabelPoweredByCustomText',
				'whiteLabelHeader',
				'whiteLabelHeaderExtUrl',
				'whiteLabelHeaderExtHeight',
				'whiteLabelFooterExtUrl',
				'whiteLabelFooterExtHeight',
				'whitelabelNamingSiteEnable',
				'whiteLabelContactOption',
				'whiteLabelContactCustomEmail',
				'replaceDoceboWordOption',
				'replaceDoceboCustomWord',
				'replaceDoceboCustomLink',
			),
			self::GROUPSETTING_HTTPS => array(
			),
			self::GROUPSETTING_ECOMMERCE => array(
				'ecommerceEnabled',
				'currencySymbol',
				'currency',
				'payPalEnabled',
				'payPalAccount',
				'payPalSandbox',
				'authorizeNetEnabled',
				'authorizeNetLoginId',
				'authorizeNetTransactionKey',
				'authorizeNetMD5Hash',
				'authorizeNetSandbox',
			),
			self::GROUPSETTING_COURSECATALOG => array(
			),
		);
		
		return $map;
		
	}


	/**
	 * 
	 * @param unknown $attribute
	 * @return Ambigous <multitype:, multitype:string >|boolean
	 */
	public static function getAttributeGroup($attribute) {
		foreach (self::getGroupAttributesMap() as $group => $attributes) {
			if (in_array($attribute, $attributes))
				return $group;
		}
		return false;
	}


	/**
	 * @see CModel::validate()
	 */
	public function validate($attributes=null, $clearErrors=true) {
		return parent::validate($attributes, $clearErrors);
		
	}

	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
//		if(!$this->isNewRecord){
//			$this->disable_registration_email_confirmation = $this->disable_registration_email_confirmation == 'on' ? 1 : 0;
//		}
		if(is_array($this->allowed_domains))
			$this->allowed_domains = CJSON::encode($this->allowed_domains);

		return true;
	}

	public function afterSave(){
		//$this->disable_registration_email_confirmation = $this->disable_registration_email_confirmation == 1 ? 'on' : 'off';
		// If for Some reason the allowed domains is still string let's cast it into array
		if(is_string($this->allowed_domains))
			$this->allowed_domains = (array) CJSON::decode($this->allowed_domains, 1);
	}

	/**
	 * @see CActiveRecord::afterFind()
	 */
	protected function afterFind () {
		parent::afterFind();
		$this->allowed_domains = (array) CJSON::decode($this->allowed_domains, 1);
	}


	/**
	 * @see CModel::beforeValidate()
	 */
	public function beforeValidate() {
		
		// Before running the validation, we tranform possible uploaded files into an asset ID, which are validation correctly
		 
		// Company Logo
		$uploadedFile = CUploadedFile::getInstance($this, 'companyLogo');
		if ($uploadedFile) {
			$asset = new CoreAsset();
			$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
			$asset->sourceFile = $uploadedFile;
			$asset->save();
			$this->companyLogo = $asset->id; 
		}
		
		// Favicon
		$uploadedFile = CUploadedFile::getInstance($this, 'favicon');
		if ($uploadedFile) {
			$asset = new CoreAsset();
			$asset->type = CoreAsset::TYPE_GENERAL_IMAGE;
			$asset->sourceFile = $uploadedFile;
			$asset->save();
			$this->favicon = $asset->id;
		}
		
		return parent::beforeValidate();
	}

	/**
	 * Based on the domain type and "domain" attribute itself, return resulting client's URI,
	 * e.g.  client.domain.com or client.domain.com
	 * 
	 */
	public function getClientUri($removeWww=true) {
		
		$currentLmsDomain = trim(Docebo::getCurrentDomain(true), '/');

		// remove "www."
		if ($removeWww) {
			$currentLmsDomain = preg_replace('/^www\\./', '', strtolower($currentLmsDomain));
		}
		switch ($this->domain_type) {
			case self::DOMAINTYPE_SUBFOLDER:
				$result = $currentLmsDomain . "/" . $this->domain;  
				break;
				
			case self::DOMAINTYPE_SUBDOMAIN:
				$result = $this->domain . '.' . $currentLmsDomain;
				break;
				
			case self::DOMAINTYPE_CUSTOMDOMAIN:
				$result = $removeWww ? preg_replace('/^www\\./', '', strtolower($this->domain)): $this->domain;
				break;
				
			default:
				$result = $currentLmsDomain;
				break;	
		}
		return $result;
		
	}
	
	
	/**
	 * Based on the request, the current user and other stuff, resolve the client record to use (from CoreMultidomain)
	 *
	 * @param string $ignoreSessionSaved Ignore any previously resolved client, saved in session (related to API calls)
	 *
	 * @return CoreMultidomain
	 */
	public static function resolveClient($ignoreSessionSaved=false) {
		if(!Yii::app()->user)
			return false;

		$requestHost = $_SERVER['HTTP_HOST'];
		
		// www. as a last subdomain is always subject of removal
		$requestHost = preg_replace('/^www\\./', '', strtolower($requestHost));
		$subfolder = self::getSubfolder();
		$host = $subfolder.".".$_SERVER['HTTP_HOST'];
		$sessionKey = 'multidomain_client';
		$sessData = array();
		
		// Check if we need to invalidate the domainbranding session or return the client saved there
		if (Yii::app()->session[$sessionKey]) {
			$isGuest = Yii::app()->user->getIsGuest();
			$isSessLoggedIn = isset(Yii::app()->session[$sessionKey]['logged_in']) &&  Yii::app()->session[$sessionKey]['logged_in'];
			$hostCheck = isset(Yii::app()->session[$sessionKey]['host']) && (strcasecmp(Yii::app()->session[$sessionKey]['host'], $host) !== 0);
			if( 
				($isGuest && $isSessLoggedIn) ||
				(!$isGuest && !$isSessLoggedIn) ||
				($hostCheck)
			) {
				Yii::app()->session[$sessionKey] = null;
			}
			else if (!$ignoreSessionSaved) {
				$sessData = Yii::app()->session[$sessionKey];
				$client = $sessData['client'];
				return $client;
			}
		}
		
		
		// Collect clients, but first get those with subfolders, then the rest
		$clientsWithSubFolders 		= self::model()->findAllBySql("select * from core_multidomain as cm
inner join core_org_chart_tree as tree on tree.idOrg = cm.org_chart
where (domain_type =  'subfolder') AND (cm.active > 0)
order by tree.iLeft ASC");
		$clientsWitnNoSubfolders 	= self::model()->findAllBySql("select * from core_multidomain as cm
inner join core_org_chart_tree as tree on tree.idOrg = cm.org_chart
where (domain_type <>  'subfolder') AND (cm.active > 0)
order by tree.iLeft ASC");
		$clients = array_merge($clientsWithSubFolders, $clientsWitnNoSubfolders);

		// Check if user is a member of branch assigned to some client (logged in users only)
		if (!Yii::app()->user->getIsGuest()){
			
			$foundClient = null;
			foreach ($clients as $client){
				if (CoreOrgChartTree::isUserMemberOf(Yii::app()->user->id, $client->org_chart, true)){
					$foundClient = $client;
					$userIsMemberOfRequestedDomain = self::checkCurrentDomainIsMultidomainClient($client, $requestHost, $subfolder);
					if($userIsMemberOfRequestedDomain){
						break;
					}
				}
			}

			if($foundClient) {
				$sessData['client'] = $foundClient;
				$sessData['logged_in'] = true;
				$sessData['host'] = $host;
				Yii::app()->session[$sessionKey] = $sessData;
				return $foundClient;
			}
		}
		
		// So, user is not a member of any branch of any client
		// Resolve by URI
		$found = false;
		foreach ($clients as $client) {
			$found = self::checkCurrentDomainIsMultidomainClient($client, $requestHost, $subfolder);

			if ($found) {
				$sessData['client'] = $found;
				$sessData['logged_in'] = !Yii::app()->user->getIsGuest();
				$sessData['host'] = $host;
				Yii::app()->session[$sessionKey] = $sessData;
				return $found;
			}
		}

		$sessData['client'] = false;
		$sessData['logged_in'] = !Yii::app()->user->getIsGuest();
		$sessData['host'] = $host;
		Yii::app()->session[$sessionKey] = $sessData;
		return false;
	}

	public static function checkCurrentDomainIsMultidomainClient($client ,$requestHost = null ,$subfolder = null){
		$result = false;
		if($requestHost && $subfolder){
			$clientUri = $client->getClientUri();
			// Handle different multidomain-ing type differently
			switch ($client->domain_type) {
				case CoreMultidomain::DOMAINTYPE_SUBFOLDER:
					if ($clientUri == ($requestHost . "/" . $subfolder) ) {
						$result = $client;
					}
					break;

				case CoreMultidomain::DOMAINTYPE_SUBDOMAIN:
				case CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN:
					if ($clientUri == $requestHost) {
						$result = $client;
					}
					break;
			}
		}

		return $result;
	}



	public static function resolveWhiteLabelClient(){
		$requestHost = $_SERVER['HTTP_HOST'];

		// www. as a last subdomain is always subject of removal
		$requestHost = preg_replace('/^www\\./', '', strtolower($requestHost));
		$subfolder = self::getSubfolder();
		$host = $subfolder.".".$_SERVER['HTTP_HOST'];
		$sessData = array();
		// Collect clients, but first get those with subfolders, then the rest
		$clientsWithSubFolders 		= self::model()->findAllBySql("select * from core_multidomain as cm
inner join core_org_chart_tree as tree on tree.idOrg = cm.org_chart
where (domain_type =  'subfolder') AND (cm.active > 0)
order by tree.iLeft ASC");
		$clientsWitnNoSubfolders 	= self::model()->findAllBySql("select * from core_multidomain as cm
inner join core_org_chart_tree as tree on tree.idOrg = cm.org_chart
where (domain_type <>  'subfolder') AND (cm.active > 0)
order by tree.iLeft ASC");
		$clients = array_merge($clientsWithSubFolders, $clientsWitnNoSubfolders);

		// So, user is not a member of any branch of any client
		// Resolve by URI
		$found = false;
		foreach ($clients as $client) {

			// Calculating Client URI, internally removing "www."
			$clientUri = $client->getClientUri();

			// Handle different multidomain-ing type differently
			switch ($client->domain_type) {
				case CoreMultidomain::DOMAINTYPE_SUBFOLDER:
					if ($clientUri == ($requestHost . "/" . $subfolder) ) {
						$found = $client;
					}
					break;

				case CoreMultidomain::DOMAINTYPE_SUBDOMAIN:
				case CoreMultidomain::DOMAINTYPE_CUSTOMDOMAIN:
					if ($clientUri == $requestHost) {
						$found = $client;
					}
					break;
			}

			if ($found) {
				$sessData['client'] = $found;
				$sessData['logged_in'] = !Yii::app()->user->getIsGuest();
				$sessData['host'] = $host;
				return $found;
			}

		}

		$sessData['client'] = false;
		$sessData['logged_in'] = !Yii::app()->user->getIsGuest();
		$sessData['host'] = $host;
		return false;
	}
	
	/**
	 * Load SIGNIN PAGE Title & Texts for 
	 * 
	 * @param string $languages
	 * @return multitype:multitype:unknown  Ambigous <string, NULL, multitype:multitype: > Ambigous <multitype:, string>
	 */
	public function getSigninTextsTranslations($languages = false) {

		if (!is_array($languages)) {
			$languages = CoreLangLanguage::getActiveLanguages();
		}
		$translations = array();
		 
		// Get available signin texts
		// We do NOT use model relation here
		// We can, but I find it more convenient to use this approach.
		$criteria = new CDbCriteria();
		$criteria->addInCondition('lang_code', array_keys($languages));
		$criteria->addCondition('id_multidomain=' . $this->id);
		$textModels = CoreMultidomainSigninText::model()->findAll($criteria);
		
		// Enumerate all models and create data array for translations
		foreach ($textModels as $model) {
			$translations[$model->lang_code]['title'] = $model->title;
			$translations[$model->lang_code]['text'] = $model->text;
		}
		
		// Fill up  missing ones (per language)
		$translated = array();
		foreach ($languages as $lc => $lname) {
			if (!isset($translations[$lc])) {
				$translations[$lc]['text'] = '';
				$translations[$lc]['title'] = '';
			}
			else if ($translations[$lc]['text'] || $translations[$lc]['title']) {
				$translated[] = $lc;
			}
		}

		// Resolve current selection:
		// 1. If current langauge is translated - select it
		// 2. If current language is among allowed language - select if
		// 3. If there is ANY translated language, select the first one
		$currentLangCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$preselectedTextsLanguage = "0";
		if (in_array($currentLangCode, $translated)) {
			$preselectedTextsLanguage = $currentLangCode;
		}
		else if (in_array($currentLangCode, array_keys($languages))) {
			$preselectedTextsLanguage = $currentLangCode;
		}
		else if (isset($translated[0])) {
			$preselectedTextsLanguage = $translated[0];
		}
		
		
		return array(
			'languages'			=> $languages,
			'translations'		=> $translations,
			'translated'		=> $translated,
			'preselectedTextsLanguage'		=> $preselectedTextsLanguage,		
		);
		
	}

	
	
	public static function getSubfolder(){
		$requestUri  = trim($_SERVER['REQUEST_URI'], '/');
		$subs = preg_split('/\//', $requestUri);
		$subfolder = $subs[0];
		
		// Compute the host to use
		if(!empty($_GET['domain'])) {
			$eventSubfolder = new DEvent(new self(), array('subfolder' => $_GET['domain']));
			Yii::app()->event->raise("BeforeSubfolderProcessing", $eventSubfolder);
			if(!$eventSubfolder->shouldPerformAsDefault())
				$subfolder = $eventSubfolder->return_value['branding_host'];
		}

		return $subfolder;
	}

	
	/**
	 * Check if this multidomain's domain can set up separate HTTPS,
	 * because if the "general custom domain's" certificate is a Wildcard one, we should NOT allow.
	 * Also, not allowed for sub folders. New custom domains are ok.
	 * 
	 * @return boolean
	 */
	public function allowHttps() {

		if(!PluginManager::isPluginActive('HttpsApp'))
			return false;

		if ($this->domain_type == self::DOMAINTYPE_SUBFOLDER) {
			return false;
		}
		
		if ($this->domain_type == self::DOMAINTYPE_CUSTOMDOMAIN) {
			return true;
		}
		
		$genHttpsModel = CoreHttps::getGeneralHttps();
		
		if ($genHttpsModel->sslStatus != CoreHttps::SSL_STATUS_INSTALLED) {
			return true;
		}
		
		if (($this->domain_type == self::DOMAINTYPE_SUBDOMAIN) ) {
			return true;
		}
		
		// Ok, we have general Wildcard certificate & a subdomain type of multidomain
		// Checking if general custom domain is part of the subdomain URI
		$wcDomain = $genHttpsModel->domain;
		$myDomain = $this->getClientUri(true);
		
		$res = preg_match("/" . $wcDomain . "/", $myDomain);
		
		return $res <= 0;
		
	}
	
}
