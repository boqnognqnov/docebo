<?php

/**
 * This is the model class for table "core_enroll_log".
 *
 * The followings are the available columns in table 'core_enroll_log':
 * @property integer $enroll_log_id
 * @property integer $rule_id
 * @property string $date_created
 *
 * The followings are the available model relations:
 * @property CoreEnrollRule $rule
 * @property CoreEnrollLogItem[] $coreEnrollLogItems
 */
class CoreEnrollLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreEnrollLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_enroll_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rule_id, date_created', 'required'),
			array('rule_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('enroll_log_id, rule_id, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rule' => array(self::BELONGS_TO, 'CoreEnrollRule', 'rule_id'),
			'coreEnrollLogItems' => array(self::HAS_MANY, 'CoreEnrollLogItem', 'enroll_log_id'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			    'timestampAttributes' => array('date_created medium'),
			    'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enroll_log_id' => 'Enroll Log',
			'rule_id' => 'Rule',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enroll_log_id',$this->enroll_log_id);
		$criteria->compare('rule_id',$this->rule_id);
		$criteria->compare('date_created',Yii::app()->localtime->fromLocalDateTime($this->date_created),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		if ($this->isNewRecord)
			$this->date_created = Yii::app()->localtime->getUTCNow();

		return parent::beforeSave();
	}

	public function rollback()
	{
		foreach ($this->coreEnrollLogItems as $item)
		{
			$item->rollback();
		}
	}

	/**
	 * Check if all log items are with rollback = 1
	 * This means that a rollback is performed OR each item rollback is executed
	 * @return bool
	 */
	public function getIsRollbackPerformed()
	{
		$sql = 'SELECT COUNT(log_item_id) FROM '.CoreEnrollLogItem::model()->tableName().'
				WHERE enroll_log_id = '.(int)$this->enroll_log_id.'
				AND rollback = 0';
		$res = Yii::app()->db->createCommand($sql)->queryScalar();
		return $res ? false : true;
	}

	public function beforeDelete()
	{
		CoreEnrollLogItem::model()->deleteAllByAttributes(array(
			'enroll_log_id' => $this->enroll_log_id,
		));
		return parent::beforeDelete();
	}
}