<?php

/**
 * This is the model class for table "learning_poll".
 *
 * The followings are the available columns in table 'learning_poll':
 * @property integer $id_poll
 * @property integer $author
 * @property string $title
 * @property string $description
 */
class LearningPoll extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningPoll the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_poll';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('author', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_poll, author, title, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questions' => array(self::HAS_MANY, 'LearningPollquest', 'id_poll'),
			'questionsSequence' => array(self::HAS_MANY, 'LearningPollquest', 'id_poll', 'order'=>'sequence ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_poll' => 'Id Poll',
			'author' => 'Author',
			'title' => 'Title',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_poll',$this->id_poll);
		$criteria->compare('author',$this->author);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'learningObjects' => array(
				'class' => 'LearningObjectsBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_POLL,
				'idPropertyName' => 'id_poll',
				'titlePropertyName' => 'title'
			)
		);
	}


	/**
	 * Retrieve poll questions
	 * @param boolean $all if set to true, titles and breakpages will be retrieved too
	 * @return array the list of poll questions
	 */
	public function getQuestions($all = false) {
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		if (!$all) {
			$criteria->addCondition("type_quest <> :type_title");
			$criteria->addCondition("type_quest <> :type_break_page");
			$criteria->params[':type_title'] = LearningPollquest::POLL_TYPE_TITLE;
			$criteria->params[':type_break_page'] = LearningPollquest::POLL_TYPE_BREAK_PAGE;
		}
		return LearningPollquest::model()->findAllByAttributes(array('id_poll' => $this->getPrimaryKey()), $criteria);
	}

	/**
	 * Retrieve poll liert scales
	 * @return array the list of poll likert scales questions
	 */
	public function getLikertScales($all = false){
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		return LearningPollLikertScale::model()->findAllByAttributes(array('id_poll' => $this->getPrimaryKey()), $criteria);
	}

	/**
	 * Retrieve poll questions for a specific page
	 * @param int $pageNumber 
	 * @return array the list of poll questions in the page
	 */
	public function getQuestionsForPage($pageNumber) {
		$criteria = new CDbCriteria();
		$criteria->order = "sequence ASC";
		return LearningPollquest::model()->findAllByAttributes(array(
			'id_poll' => $this->getPrimaryKey(), 
			'page' => $pageNumber
		), $criteria);
	}



	public function getTotalPageNumber() {

		$result = Yii::app()->db->createCommand()
			->select("MAX(page) AS num_pages")
			->from(LearningPollquest::model()->tableName())
			->where("id_poll = :id_poll", array(':id_poll' => $this->getPrimaryKey()))
			->queryRow();
		$totalPages = (int)$result['num_pages'];

		return $totalPages;
	}



	public function getInitQuestionSequenceNumberForPage($pageNumber) {

		$db = Yii::app()->db;
		$result = $db->createCommand()
			->select("COUNT(*) AS num")
			->from(LearningPollquest::model()->tableName())
			->where($db->quoteColumnName('id_poll')." = :id_poll", array(':id_poll' => $this->getPrimaryKey()))
			->andWhere($db->quoteColumnName('page')." < :page_number", array(':page_number' => $pageNumber))
			->andWhere($db->quoteColumnName('type_quest')." <> :qtype_1", array(':qtype_1' => LearningPollquest::POLL_TYPE_TITLE))
			->andWhere($db->quoteColumnName('type_quest')." <> :qtype_2", array(':qtype_2' => LearningPollquest::POLL_TYPE_BREAK_PAGE))
			->queryRow();
		return (int)$result['num'];

	}



	/**
	 * This function creates a copy of the record in the same table
	 * 
	 * @return CActiveRecord the copy record
	 */
	public function copy($organization = true) {
		$copy = new LearningPoll();
		$copy->author = Yii::app()->user->id;
		$copy->title = $this->title;
		$copy->description = $this->description;
		if (!$organization) {
			$copy->disableLearningObjectsEvents(); //this is important, otherwise it will break DB
		}
		if ($copy->save()) {
			//copy poll questions and answers
			$questions = $this->getQuestions(true);
			if (!empty($questions)) {
				$questionIdList = array();
				foreach ($questions as $question) { $questionIdList[] = $question->getPrimaryKey(); }
				$criteria = new CDbCriteria();
				$criteria->addInCondition('id_quest', $questionIdList);
				$answers = LearningPollquestanswer::model()->findAll($criteria);
				$questionsMirror = array();
				foreach ($questions as $question) {
					$copiedQuestion = new LearningPollquest();
					$copiedQuestion->id_poll = $copy->getPrimaryKey();
					$copiedQuestion->id_category = $question->id_category;
					$copiedQuestion->type_quest = $question->type_quest;
					$copiedQuestion->title_quest = $question->title_quest;
					$copiedQuestion->sequence = $question->sequence;
					$copiedQuestion->page = $question->page;
					if (!$copiedQuestion->save()) {
						$errorMessage = 'Error while copying LO: '.implode("; ", $copiedQuestion->getErrors());
						Yii::log($errorMessage);
						throw new CException($errorMessage);
					}
					$questionsMirror[$question->getPrimaryKey()] = $copiedQuestion->getPrimaryKey();
				}
				if (!empty($answers)) {
					foreach ($answers as $answer) {
						$copiedAnswer = new LearningPollquestanswer();
						$copiedAnswer->id_quest = $questionsMirror[$answer->id_quest];
						$copiedAnswer->sequence = $answer->sequence;
						$copiedAnswer->answer = $answer->answer;
						if (!$copiedAnswer->save()) {
							$errorMessage = 'Error while copying LO: '.implode("; ", $copiedAnswer->getErrors());
							Yii::log($errorMessage);
							throw new CException($errorMessage);
						}
					}
				}
			}
			// Copy the Likert scale question since the scales are related to a poll LO
			$likertScales = $this->getLikertScales();
			if (!empty($likertScales)) {
				foreach($likertScales as $scale){
					$copiedScale = new LearningPollLikertScale();
					$copiedScale->id_poll = $copy->getPrimaryKey();
					$copiedScale->title = $scale->title;
					$copiedScale->sequence = $scale->sequence;
					if (!$copiedScale->save()) {
						$errorMessage = 'Error while copying LO: '.implode("; ", $copiedScale->getErrors());
						Yii::log($errorMessage);
						throw new CException($errorMessage);
					}
				}
			}
			if (!$organization) { $copy->enableLearningObjectsEvents(); }
			return $copy;
		} else {
			$errorMessage = 'Error while copying LO: '.implode("; ", $copy->getErrors());
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}
	}

}