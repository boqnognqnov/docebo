<?php

/**
 * This is the model class for table "core_bug".
 *
 * The followings are the available columns in table 'bug':
 * @property integer $bug_id
 * @property string $title
 * @property string $description
 * @property string $file
 * @property string $project
 * @property string $status
 * @property string $created_at
 */
class CoreBug extends CActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bug the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public function init() {
        parent::init();

        // Check configuration parameters
        if (!isset(Yii::app()->params['bug_report']['projects']) || !is_array(Yii::app()->params['bug_report']['projects']))
            throw new ExceptionClass('Missing parameter bug_report.projects');
        elseif (!is_array(Yii::app()->params['bug_report']['projects']))
            throw new ExceptionClass('Parameter bug_report.projects must be array');
        elseif (!Yii::app()->params['bug_report']['projects'])
            throw new ExceptionClass('Parameter bug_report.projects can not be empty');

        if (!isset(Yii::app()->params['bug_report']['types']))
            throw new ExceptionClass('Missing parameter bug_report.types');
        elseif (!is_array(Yii::app()->params['bug_report']['types']))
            throw new ExceptionClass('Parameter bug_report.types must be array');
        elseif (!Yii::app()->params['bug_report']['types'])
            throw new ExceptionClass('Parameter bug_report.types can not be empty');

        if (!isset(Yii::app()->params['bug_report']['usr']) && Yii::app()->params['bug_report']['usr'])
            throw new ExceptionClass('Missing parameter bug_report.usr');

        if (!isset(Yii::app()->params['bug_report']['pwd']) && Yii::app()->params['bug_report']['pwd'])
            throw new ExceptionClass('Missing parameter bug_report.pwd');

        if (!isset(Yii::app()->params['bug_report']['folder']) && Yii::app()->params['bug_report']['folder'])
            throw new ExceptionClass('Missing parameter bug_report.folder');

        if (!isset(Yii::app()->params['bug_report']['system_info']))
            throw new ExceptionClass('Missing parameter bug_report.system_info');

        if (!isset(Yii::app()->params['bug_report']['send_mail']))
            throw new ExceptionClass('Missing parameter bug_report.send_mail');

        if (Yii::app()->params['bug_report']['send_mail'] && !isset(Yii::app()->params['bug_report']['mail_to']))
            throw new ExceptionClass('Missing parameter bug_report.mail_to');
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_bug';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, file', 'length', 'max' => 255),
			array('project', 'length', 'max' => 40),
			array('status', 'length', 'max' => 8),
			array('title, description, file, project, type', 'safe'),
			array('file', 'file', 'allowEmpty' => true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bug_id, title, description, file, project, status, created_at, type', 'safe', 'on' => 'search'),
			array('title, description, project, type', 'required', 'on' => 'report'),
			array('title, description, file, project', 'report'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('created_at medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'bug_id' => Yii::t('bug_report', '_BUG'),
			'title' => Yii::t('bug_report', '_BUG_TITLE'),
			'description' => Yii::t('bug_report', '_BUG_DESCRIPTION'),
			'file' => Yii::t('bug_report', '_BUG_FILE'),
			'project' => Yii::t('bug_report', '_BUG_PROJECT'),
			'status' => Yii::t('bug_report', '_BUG_STATUS'),
			'created_at' => Yii::t('bug_report', '_BUG_CREATED_AT'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('bug_id', $this->bug_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('file', $this->file, true);
		$criteria->compare('project', $this->project, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('created_at', Yii::app()->localtime->fromLocalDateTime($this->created_at), true);

		return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
			));
	}

	public function report($attribute, $params) {
	}

	public function getFileWebPath() {
		return Yii::app()->params['base_url'].Yii::app()->params['bug_report']['folder'].$this->getPrimaryKey().'_'.$this->file;
	}

	public function getFilePath() {
		return Yii::app()->basePath.Yii::app()->params['bug_report']['folder'].$this->getPrimaryKey().'_'.$this->file;
	}

	public function sendToPivotal() {
        $project = false;
        foreach (Yii::app()->params['bug_report']['projects'] as $key=>$val) {
            if ($this->project == md5($key)) {
                $project = Pivotal::getInstance()->setProject($key);
                $this->project = "#{$key}: {$val}";
                break;
            }
        }

        if (Pivotal::getInstance()->isError())
            return Pivotal::getInstance()->getError();
        elseif ($project === false)
            return Yii::t('bug_report', '_BUG_REPORT_SELECT_PROJECT');

		$story = $project->addStory($this->type, $this->title, $this->description);
		if ($story && $story->id) {
			$this->url = $story->url;
			$this->status = 'sent';
			if($this->file) $project->addStoryAttachment($story->id, $this->getFilePath());

            if (Yii::app()->params['bug_report']['send_mail'] == true) {
                $user_model = Yii::app()->user->loadUserModel();
                $msg = "New bug reported by ".$user_model->email."\n\n".$this->description."\n\n".$this->url;
                $headers = 'From: bugs@docebo.com'."\r\n" .
                    'Reply-To: '.$user_model->email."\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                mail(Yii::app()->params['bug_report']['mail_to'], "[".$this->project." bug] ".$this->title, $msg, $headers);
            }

			return true;
		}
		return false;
	}

    public function beforeSave() {
        if (Yii::app()->params['bug_report']['system_info'] == true) {
            $this->description .= "\n------------------------------\n";
            $this->description .= "HTTP_HOST: {$_SERVER['HTTP_HOST']}\n";
            //$this->description .= "HTTP_COOKIE: {$_SERVER['HTTP_COOKIE']}\n";
            $this->description .= "REMOTE_ADDR: {$_SERVER['REMOTE_ADDR']}\n";
            $this->description .= "User: ".Yii::app()->user->loadUserModel()->email."\n";
        }

        return parent::beforeSave();
    }

    public static function getProjectList() {
        $arr = array();
        foreach (Yii::app()->params['bug_report']['projects'] as $key=>$val) {
            $arr[md5($key)] = $val;
        }
        return $arr;
    }

    public static function getTypes() {
        $arr = array();
        foreach (Yii::app()->params['bug_report']['types'] as $key=>$val) {
            $arr[$key] = $val;
        }
        return $arr;
    }

}