<?php

/**
 * This is the model class for table "learning_course_shopify_product".
 *
 * The followings are the available columns in table 'learning_course':
 * @property integer $id_session
 * @property string $shopifyVariantId
 * @property float $price
 *
 */
class WebinarSessionShopifyVariant extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'webinar_session_shopify_variant';
	}

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}
