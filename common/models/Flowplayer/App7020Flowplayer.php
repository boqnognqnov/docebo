<?php

/** 
 * @author Stanimir
 * 
 */
class App7020Flowplayer extends FlowplayerModel {
	
	/**
	 * Store content model
	 * @var App7020Asset|LearningVideo
	 */
	public $content;
	/**
	 * 
	 * @var unknown
	 */
	public $storage;
	
	/**
	 * 
	 * @var unknown
	 */
	public $contentUrl;
	
	/**
	 * 
	 * @var unknown
	 */
	private $_currentCollection = false;
	/**
	 * 
	 * @var unknown
	 */
	private $_isVideo = false;
	
	/**
	 * 
	 * @var unknown
	 */
	public $cuepoints = array();

	/**
	 * 
	 * @param App7020Assets $content
	 */
	function __construct(App7020Assets $content) {
		$helper = new App7020Helpers();
		Yii::app()->event->on('add7020Options', array($helper, 'registerApp7020Options'));
 		$this->content =  $content;
 		$this->storage = $this->_getCollection();
   		if ( !in_array($this->content->contentType, array(App7020Assets::CONTENT_TYPE_VIDEO, App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC)) && !App7020Assets::isConvertableDocument( $this->content->filename) && $this->content->userId != Yii::app()->user->id) {
				App7020Assets::viewContent($this->content->id);
 		} else {
			if($this->content->contentType == App7020Assets::CONTENT_TYPE_VIDEO){
				$this->cuepoints[] = array(
					'time' =>  $this->content->duration * App7020Assets::VIDEO_VIEW_PERCENTAGE,
					'type' => App7020Assets::CUEPOINT_VIDEO_VIEW
				);
			}
 		}
 		try {
			if ($this->content instanceof App7020Assets) {
				$this->_isVideo = $this->content->contentType == App7020Assets::CONTENT_TYPE_VIDEO || $this->content->contentType == App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC ? true : false;
				$sources = $this->_cosntructApp7020Playlist();
 				if( $this->content->contentType == App7020Assets::CONTENT_TYPE_VIDEO) {
					$this->mediaUrlHlsPlaylist = $sources['mediaUrlHlsPlaylist'];
					$this->mediaUrlMp4 = $sources['mediaUrlMp4'];
				}
				else if ($this->content->contentType ==  App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC) {
					$this->mediaUrlMp4 = $sources['audio'];
				}
				
			}
		
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
		}
		parent::__construct($content);
		$this->playerOptions['cuepoints'] = array_merge($this->cuepoints,  App7020Tooltips::generateQuepoints($this->content->id)) ;
 	}

	/**
	 *  Append collection for usage in storage instances
	 * @return boolean|CS3StorageSt|CS3Storage
	 */
	private function _getCollection() {
		try {
			switch (true) {
				case $this->content instanceof App7020Assets :
					$this->_currentCollection = CS3StorageSt::COLLECTION_7020;
					break;
				default:
					return false;
			}
			return CS3Storage::getDomainStorage($this->_currentCollection);
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}
	
	/**
	 * Return all playlist sources
	 * @return array
	 */
	private function _cosntructApp7020Playlist() {
		return CS3StorageSt::getPlayerSources($this->content->filename, $this->storage);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see FlowplayerModel::get()
	 */
	function get() {
	 	return parent::get();
	}
}

?>