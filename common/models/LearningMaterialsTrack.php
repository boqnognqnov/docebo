<?php

/**
 * This is the model class for table "learning_materials_track".
 *
 * The followings are the available columns in table 'learning_materials_track':
 * @property integer $idTrack
 * @property integer $idResource
 * @property integer $idReference
 * @property integer $idUser
 */
class LearningMaterialsTrack extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningMaterialsTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_materials_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idResource, idReference, idUser', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idResource, idReference, idUser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idResource' => 'Id Resource',
			'idReference' => 'Id Reference',
			'idUser' => 'Id User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idResource',$this->idResource);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idUser',$this->idUser);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function behaviors() {
		return array(
			'commonTrack' => array(
				'class' => 'CommontrackBehavior',
				'idResourcePropertyName' => 'idResource',
				'idUserPropertyName' => 'idUser',
				'idOrgPropertyName' => 'idReference',
				//this table is "shared" by different object types, so "objectType" property will be manually set at the moment of tracking.
				//'statusPropertyName' => 'status',
				//this table does not define a "status" property, so the attribute "statusPropertyName" cannot be set (at the moment of tracking, status will be instantiated in an internal behavior field).
			)
		);
	}
}