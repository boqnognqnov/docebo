<?php

/**
 * This is the model class for table "lt_course_session_date".
 *
 * The followings are the available columns in table 'lt_course_session_date':
 * @property integer $id_session
 * @property string $day
 * @property string $name
 * @property string $time_begin
 * @property string $time_end
 * @property string $break_begin
 * @property string $break_end
 * @property string $timezone
 * @property integer $id_location
 * @property integer $id_classroom
 *
 * The followings are the available model relations:
 * @property LtCourseSession $idSession
 * @property LtLocation $learningLocation
 * @property LtClassroom $learningClassroom
 * @property LtCourseSessionDateAttendance[] $learningCourseSessionDateAttendances
 * @property LtCourseSessionDateAttendance[] $learningCourseSessionDateAttendances1
 */
class LtCourseSessionDate extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'lt_course_session_date';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id_session, day, time_begin, time_end, timezone, id_location', 'required'),
            array('id_session, id_location, id_classroom', 'numerical', 'integerOnly' => true),
            array('name, timezone', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_session, day, name, time_begin, time_end, break_begin, break_end, timezone, id_location, id_classroom', 'safe', 'on' => 'search'),
            array('break_begin ,break_end' , 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idSession' => array(self::BELONGS_TO, 'LtCourseSession', 'id_session'),
            'ltLocation' => array(self::BELONGS_TO, 'LtLocation', 'id_location'),
            'ltClassroom' => array(self::BELONGS_TO, 'LtClassroom', 'id_classroom'),
            'learningCourseSessionDateAttendances' => array(self::HAS_MANY, 'LtCourseSessionDateAttendance', 'id_session'),
            'learningCourseSessionDateAttendances1' => array(self::HAS_MANY, 'LtCourseSessionDateAttendance', 'day'),
        );
    }

	public function beforeSave(){


		if (!$this->break_begin){
			$this->break_begin = '00:00:00';
		}

		if(!$this->break_end){
			$this->break_end = '00:00:00';
		}

		
		return parent::beforeSave();
	}


	/**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_session' => 'Id Session',
            'day' => 'Day',
            'name' => 'Name',
            'time_begin' => 'Time Begin',
            'time_end' => 'Time End',
            'break_begin' => 'Break Begin',
            'break_end' => 'Break End',
            'timezone' => 'Timezone',
            'id_location' => 'Id Location',
            'id_classroom' => 'Id Classroom',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_session', $this->id_session);
        $criteria->compare('day', $this->day, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('time_begin', $this->time_begin, true);
        $criteria->compare('time_end', $this->time_end, true);
        $criteria->compare('break_begin', $this->break_begin, true);
        $criteria->compare('break_end', $this->break_end, true);
        $criteria->compare('timezone', $this->timezone, true);
        $criteria->compare('id_location', $this->id_location);
        $criteria->compare('id_classroom', $this->id_classroom);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LtCourseSessionDate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function sessionInfoDataProvider($sessionId)
    {
        $countSessionDates = Yii::app()->db->createCommand()
            ->select('csd.id_session')
            ->from(LtCourseSessionDate::model()->tableName() . ' csd')
            ->where('csd.id_session=:id', array(':id' => $sessionId))->queryAll();

        $criteria = new CDbCriteria();

        $criteria->with = array(
            'ltLocation'
        );
        $criteria->addCondition('id_session = :id_session');
        $criteria->params[':id_session'] = $sessionId;

        $config = array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => count($countSessionDates)
            ),
        );
        $config['sort']['defaultOrder'] = 'day ASC';

        return new CActiveDataProvider(get_class(self::model()), $config);
    }


    //-------

    protected $_cache_location = NULL;
    protected $_cache_classroom = NULL;

    /**
     * Retrieve AR of the classroom location through id_location
     *
     * @return /LtLocation the AR of the location
     */
    public function getLocation()
    {
        if ($this->_cache_location !== NULL) {
            //first try to read cache; if it's filled, then check validity and return cached value
            if ($this->_cache_location->id_location == $this->id_location) {
                return $this->_cache_location;
            }
            //if cache value is not valid, nullify it and retrieve data from DB
            $this->_cache_location = NULL;
        }
        //retrieve location info from DB
        $tmp = LtLocation::model()->findByPk($this->id_location);
        if ($tmp) {
            $this->_cache_location = $tmp;
        } //fill internal cache value
        return $this->_cache_location;
    }

    /**
     * Retrieve AR of the classroom through id_classroom
     *
     * @return /LtClassroom the AR of the classroom
     */
    public function getClassroom()
    {
        if ($this->_cache_classroom !== NULL) {
            //first try to read cache; if it's filled, then check validity and return cached value
            if ($this->_cache_classroom->id_classroom == $this->id_classroom) {
                return $this->_cache_classroom;
            }
            //if cache value is not valid, nullify it and retrieve data from DB
            $this->_cache_classroom = NULL;
        }
        //retrieve location info from DB
        $tmp = LtClassroom::model()->findByPk($this->id_classroom);
        if ($tmp) {
            $this->_cache_classroom = $tmp;
        } //fill internal cache value
        return $this->_cache_classroom;
    }


    public function getDateBegin()
    {
        return $this->day . ' ' . $this->time_begin;
    }

    public function getDateEnd()
    {
        return $this->day . ' ' . $this->time_end;
    }


    /**
     * Copy the date into another session
     *
     * @param int $intoSession the id_session of the destination session
     * @param int $daysShift days to move the day value of the table
     */
    public function copy($intoSession, $daysShift = 0)
    {
        $newDay = $this->day;
        if ($daysShift != 0) {
            $dayTime = CDateTimeParser::parse($newDay, 'yyyy-MM-dd');
            $dayTime += $daysShift * 3600 * 24;
            $newDay = date("Y-m-d H:i:s", $dayTime);
        }

        $output = new LtCourseSessionDate();
        $output->id_session = $intoSession;
        $output->day = $newDay;
        $output->name = $this->name;
        $output->time_begin = $this->time_begin;
        $output->time_end = $this->time_end;
        $output->timezone = $this->timezone;
        $output->break_begin = $this->break_begin;
        $output->break_end = $this->break_end;
        $output->id_location = $this->id_location;
        $output->id_classroom = $this->id_classroom;

        if (!$output->save())
            throw new CException('Error while saving session date: ' . print_r($output->getErrors(), true));

        return $output;
    }

    public function renderDaySchedule($separator = '<br>')
    {
        $html = array();
        if (empty($this->break_begin) || ($this->break_begin == '00:00:00' && $this->break_end == '00:00:00') || ($this->break_begin == $this->break_end)) {
            $intervals = array(
                array($this->time_begin, $this->time_end)
            );
        } else {
            $intervals = array(
                array($this->time_begin, $this->break_begin),
                array($this->break_end, $this->time_end)
            );
        }
        foreach ($intervals as $interval) {
            $html[] = date('H:i', strtotime($interval[0])) . ' - ' . date('H:i', strtotime($interval[1]));
        }
        return implode($separator, $html) . " " . $this->renderTimezone();
    }

    /**
     * Returns the timezone label for this date
     */
    public function renderTimezone() {
        return Yii::app()->localtime->getTimezoneOffset($this->timezone);
    }

    /**
     * Set attendance for a specific day for one or more users
     *
     * @param boolean $hasAttended if we have to confirm or delete the user attendance
     * @param int /array $users the users to update
     * @return boolean operation succesfull or not
     */
    public function updateAttending($hasAttended, $users)
    {

        //validate input data
        if (is_numeric($users)) {
            $users = array((int)$users);
        }
        if (!is_array($users)) {
            return false;
        }

        $db = Yii::app()->db;
        if ($db->getCurrentTransaction() === NULL) {
            $transaction = $db->beginTransaction();
        }
        $model = LtCourseSessionDateAttendance::model();

        try {

            foreach ($users as $idUser) {

                //check which action we have to perform by $hasAttended parameter
                if ($hasAttended) {

                    //check if the attendance for this user is already stored in database
                    $existent = $model->findByAttributes(array('id_user' => $idUser, 'day' => $this->day, 'id_session' => $this->id_session));

                    if (!$existent) {
                        //no record found, user has not attended yet
                        $newAttendance = new LtCourseSessionDateAttendance();
                        $newAttendance->id_user = $idUser;
                        $newAttendance->day = $this->day;
                        $newAttendance->id_session = $this->id_session;
                        $rs = $newAttendance->save();
                        if (!$rs) {
                            throw new CException('Error while saving attendance info');
                        }

                    } else {
                        //user has already attended this day, do nothing here
                    }

                } else {

                    //we are deleting attendance info
                    $model->deleteByAttributes(array('id_user' => $idUser, 'day' => $this->day, 'id_session' => $this->id_session));
                }
            }

            if ($transaction) {
                $transaction->commit();
            }
            $result = true;

        } catch (CException $e) {

            if ($transaction) {
                $transaction->rollback();
            }
            throw $e; //$result = false;
        }


        return $result;
    }


    /**
     * Check if an user has attended a day or not
     *
     * @param int $idUser IDST of the user to be checked
     * @return boolean
     */
    public function hasAttended($idUser)
    {
        $attendance = LtCourseSessionDateAttendance::model()->findByPk(array(
            'id_user' => $idUser,
            'day' => $this->day,
            'id_session' => $this->id_session
        ));
        return (!empty($attendance));
    }


    /**
     * Calculate the number of seconds of this day
     *
     * @return int
     */
    public function countSeconds()
    {

        // TODO: this is MYSQL date format, try to make it as general as possible
        $datetimeFormat = 'yyyy-MM-dd hh:mm:ss';
        $time = 0;

        //first get the total time in seconds
        if (!empty($this->break_begin) && !empty($break_end)) {

            $date_1 = $this->day . ' ' . $this->time_begin;
            $date_2 = $this->day . ' ' . $this->break_begin;

            $time_1 = CDateTimeParser::parse($date_1, $datetimeFormat);
            $time_2 = CDateTimeParser::parse($date_2, $datetimeFormat);
            $time += abs($time_2 - $time_1); //abs() shouldn't be necessary, but 'break_begin' granted to be > 'time_begin'

            $date_1 = $this->day . ' ' . $this->break_end;
            $date_2 = $this->day . ' ' . $this->time_end;

            $time_1 = CDateTimeParser::parse($date_1, $datetimeFormat);
            $time_2 = CDateTimeParser::parse($date_2, $datetimeFormat);
            $time += abs($time_2 - $time_1); //abs() shouldn't be necessary, but 'time_end' granted to be > 'break_end'

        } else {

            $date_1 = $this->day . ' ' . $this->time_begin;
            $date_2 = $this->day . ' ' . $this->time_end;

            $time_1 = CDateTimeParser::parse($date_1, $datetimeFormat);
            $time_2 = CDateTimeParser::parse($date_2, $datetimeFormat);
            $time += abs($time_2 - $time_1); //abs() shouldn't be necessary, but 'time_end' granted to be > 'time_begin'
        }

        return $time;
    }


    /**
     * Calculate the number of minutes of this day
     *
     * @return float
     */
    public function countMinutes()
    {
        return ($this->countSeconds() / 60);
    }


    /**
     * Calculate the number of hours of this day
     *
     * @return float
     */
    public function countHours()
    {
        return ($this->countSeconds() / 3600);
    }

	public function dateIcsLocation()
	{
		$location = null;
		$dateLocation = $this->getLocation();

		if($dateLocation && $dateLocation->address && $dateLocation->country->name_country){
			$location = $dateLocation->address.', '.$dateLocation->country->name_country;
		}elseif($dateLocation && $dateLocation->country->name_country){
			$location = $dateLocation->country->name_country;
		}

		return $location;
	}

}
