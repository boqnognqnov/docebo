<?php

/**
 * This is the model class for table "learning_scorm_items_track".
 *
 * The followings are the available columns in table 'learning_scorm_items_track':
 * @property integer $idscorm_item_track
 * @property integer $idscorm_organization
 * @property integer $idscorm_item
 * @property integer $idReference
 * @property integer $idUser
 * @property integer $idscorm_tracking
 * @property string $status
 * @property integer $nChild
 * @property integer $nChildCompleted
 * @property integer $nDescendant
 * @property integer $nDescendantCompleted
 */
class LearningScormItemsTrack extends CActiveRecord
{
	const STATUS_COMPLETED = 'completed';
	const STATUS_FAILED = 'failed';
	const STATUS_INCOMPLETE = 'incomplete';
	const STATUS_NOT_ATTEMPTED = 'not attempted';
	const STATUS_PASSED = 'passed';


	public function isValidStatus($status) {
		return array_key_exists($status, self::statusLabelsList());
	}

	public static function statusLabelsList()
	{
		return array(
			self::STATUS_COMPLETED => Yii::t('standard', '_COMPLETED'),
			self::STATUS_FAILED => Yii::t('standard', 'failed'),
			self::STATUS_INCOMPLETE => Yii::t('player', 'Incomplete'),
			self::STATUS_NOT_ATTEMPTED => Yii::t('standard', 'not_attempted'),
			self::STATUS_PASSED => Yii::t('standard', 'passed')
		);
	}

	public static function getStatusValue($index)
	{
		$labels = self::statusLabelsList();
		return isset($index) ? $labels[$index] : $labels[self::STATUS_NOT_ATTEMPTED];
	}

	public function getStatusLabel()
	{
		$labels = self::statusLabelsList();
		return isset($labels[$this->status]) ? $labels[$this->status] : '';
	}
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormItemsTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_items_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idscorm_organization, idReference, idUser, nChild, nChildCompleted, nDescendant, nDescendantCompleted', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>16),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_item_track, idscorm_organization, idscorm_item, idReference, idUser, idscorm_tracking, status, nChild, nChildCompleted, nDescendant, nDescendantCompleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scormItemsTracking' => array(self::HAS_ONE, 'LearningScormTracking', array('idscorm_tracking'=>'idscorm_tracking')),
			'scormItemsTrackingHistory' => array(self::HAS_MANY, 'LearningScormTrackingHistory', 'idscorm_tracking'),
			'scormItem' => array(self::HAS_ONE, 'LearningScormItems', array('idscorm_item' => 'idscorm_item')),
		);
	}

	public function byUser($userId)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=>'scormItemsTrack.idUser=:scopeUserId',
			'params'=>array(':scopeUserId'=>$userId),
		));
		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_item_track' => 'Idscorm Item Track',
			'idscorm_organization' => 'Idscorm Organization',
			'idscorm_item' => 'Idscorm Item',
			'idReference' => 'Id Reference',
			'idUser' => 'Id User',
			'idscorm_tracking' => 'Idscorm Tracking',
			'status' => 'Status',
			'nChild' => 'N Child',
			'nChildCompleted' => 'N Child Completed',
			'nDescendant' => 'N Descendant',
			'nDescendantCompleted' => 'N Descendant Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_item_track',$this->idscorm_item_track);
		$criteria->compare('idscorm_organization',$this->idscorm_organization);
		$criteria->compare('idscorm_item',$this->idscorm_item);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idscorm_tracking',$this->idscorm_tracking);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('nChild',$this->nChild);
		$criteria->compare('nChildCompleted',$this->nChildCompleted);
		$criteria->compare('nDescendant',$this->nDescendant);
		$criteria->compare('nDescendantCompleted',$this->nDescendantCompleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}


	public function behaviors() {
		return array();
	}

	public function getOrganizationItemTrack() {
		if (empty($this->idscorm_item)) { return $this; }
		return self::model()->findByAttributes(array(
			'idUser' => $this->idUser,
			'idReference' => $this->idReference,
			'idscorm_item' => NULL
		));
	}

	/**
	 * Update an item's track status and handle all ascading operations
	 * @param string $newStatus
	 * @return boolean
	 * @throws CException
	 */
	public function updateStatus($newStatus) {

		if ($this->isNewRecord) { return false; } //track records have not yet been created
		if (!self::isValidStatus($newStatus)) { return false; } //status is invalid, cannot update

		$oldStatus = $this->status;
		if ($oldStatus == $newStatus) { return true; } //nothig to do here

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$this->status = $newStatus;
			if (!$this->save()) { throw new CException('Error while updating item track status'); }
			$this->setScormTrackingStatus();
			$this->_propagateStatus($this);

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return true;
	}


	/**
	 * Update status of parent and children sco items, if needed
	 * @param LearningScormItemsTrack $track
	 * @param bool $checkingAncestor
	 * @throws CException
	 */
	protected function _propagateStatus(LearningScormItemsTrack $track, $checkingAncestor = false) {

		//retrieve parent item info
		$item = LearningScormItems::model()->findByPk($track->idscorm_item);
		if (!$item) { throw new CException('Error while retrieving SCO item info'); }
		$idParent = ($item->idscorm_parentitem ? $item->idscorm_parentitem : NULL);

		//retrieve parent item tracking record
		$parentTrack = self::model()->findByAttributes(array(
			'idUser' => $track->idUser,
			'idReference' => $track->idReference,
			'idscorm_item' => $idParent
		));

		if ($track->status == self::STATUS_COMPLETED || $track->status == self::STATUS_PASSED) {
			//update parent's counting
			/*
			if ($parentTrack) {
				$parentTrack->nChildCompleted += 1;
				$parentTrack->nDescendantCompleted += 1;
				$parentTrack->save();
			}
			*/
			//if we set a sco node as "completed", all of its children must be updated too
			if (!$checkingAncestor && $track->nChild > 0) {
				$this->_setChildrenCompleted($track);
			}
		}

		//retrieve all siblings tracking
		$query = "SELECT sit.* "
			." FROM ".self::model()->tableName()." sit "
			." JOIN ".LearningScormItems::model()->tableName()." si "
			." ON (si.idscorm_item = sit.idscorm_item AND sit.idUser = :id_user AND sit.idReference = :id_reference) "
			." WHERE si.idscorm_parentitem ".($idParent === NULL ? "IS NULL" : "= :id_parent");
		$params = array(
			':id_user' => $track->idUser,
			':id_reference' => $track->idReference
		);
		if ($idParent) { $params[':id_parent'] = $idParent; }
		$reader = Yii::app()->db->createCommand($query)->query($params);

		//then count completed and unattended objects
		$completed = 0;
		$notAttempted = 0;
		$nChild = 0;
		$nChildCompleted = 0;
		$nDescendant = 0;
		$nDescendantCompleted = 0;

		if ($reader) {
			while ($row = $reader->read()) {

				//count all items/subitems
				$nChild++;
				if ($row['nChild'] > 0) {
					$nDescendant += $row['nDescendant'];
				} else {
					$nDescendant++;
				}

				//count completed items/subitems
				if ($row['status'] == self::STATUS_COMPLETED || $row['status'] == self::STATUS_PASSED) {
					$completed++;
					$nChildCompleted++;
					if ($row['nChild'] > 0) {
						$nDescendantCompleted += $row['nDescendantCompleted'];
					} else {
						$nDescendantCompleted++;
					}
				} elseif ($row['status'] == self::STATUS_NOT_ATTEMPTED) {
					$notAttempted++;
				} else {
					if ($row['nChild'] > 0) {
						$nDescendantCompleted += $row['nDescendantCompleted'];
					}
				}

			}
		}


		//calculate parent status and update completed children count
		if ($nDescendantCompleted >= $parentTrack->nDescendant) {
			$parentTrack->status = self::STATUS_COMPLETED;
		} elseif ($notAttempted >= $parentTrack->nChild) {
			$parentTrack->status = self::STATUS_NOT_ATTEMPTED;
		} else {
			$parentTrack->status = self::STATUS_INCOMPLETE;
		}
		$parentTrack->nChildCompleted = $nChildCompleted;
		$parentTrack->nDescendantCompleted = $nDescendantCompleted;
		$parentTrack->save();

		//"synchronize" status in learning_scorm_tracking table, if the record exists
		$parentTrack->setScormTrackingStatus();

		if ($idParent) {
			//propagate status calculation to the next level
			$this->_propagateStatus($parentTrack, true);
		} else {
			//we are at the top of the tree: update main common tracking info
			//NOTE: at this time $track should contain the AR of the "root" sco item tracking
			switch ($track->status) {
				case LearningScormItemsTrack::STATUS_COMPLETED: { $commonStatus = LearningCommontrack::STATUS_COMPLETED; } break;
				case LearningScormItemsTrack::STATUS_FAILED: { $commonStatus = LearningCommontrack::STATUS_FAILED; } break;
				case LearningScormItemsTrack::STATUS_INCOMPLETE: { $commonStatus = LearningCommontrack::STATUS_ATTEMPTED; } break;
				case LearningScormItemsTrack::STATUS_NOT_ATTEMPTED: { $commonStatus = LearningCommontrack::STATUS_AB_INITIO; } break;
				case LearningScormItemsTrack::STATUS_PASSED: { $commonStatus = LearningCommontrack::STATUS_COMPLETED; } break;
				default: { $commonStatus = false; } break;
			}
			if ($commonStatus) { CommonTracker::track($track->idReference, $track->idUser, $commonStatus); }
		}
	}


	/**
	 * Propagate status "completed" to children nodes
	 * @param LearningScormItemsTrack $track
	 */
	protected function _setChildrenCompleted(LearningScormItemsTrack $track) {

		//retrieve all children trackings
		$query = "SELECT sit.* "
			." FROM ".self::model()->tableName()." sit "
			." JOIN ".LearningScormItems::model()->tableName()." si "
			." ON (si.idscorm_item = sit.idscorm_item AND sit.idUser = :id_user AND sit.idReference = :id_reference) "
			." WHERE si.idscorm_parentitem ".(!empty($track->idscorm_item) ? "= :id_parent" : "IS NULL");
		$params = array(
			':id_user' => $track->idUser,
			':id_reference' => $track->idReference
		);
		if (!empty($track->idscorm_item)) { $params[':id_parent'] = $track->idscorm_item; }
		$reader = Yii::app()->db->createCommand($query)->query($params);

		//process all found child nodes
		while ($row = $reader->read()) {
			$childTrack = self::model()->findByPk($row['idscorm_item_track']); //retrieve AR object
			if ($childTrack && $childTrack->status != self::STATUS_COMPLETED && $childTrack != self::STATUS_PASSED) {
				//this child has to be set as completed
				$childTrack->status = self::STATUS_COMPLETED;
				$childTrack->nChildCompleted = $childTrack->nChild;
				$childTrack->nDescendantCompleted = $childTrack->nDescendant;
				$childTrack->save();
				//"synchronize" status in learning_scorm_tracking table, if the record exists
				$childTrack->setScormTrackingStatus();
				//check if we have grandchildren to set too
				if ($childTrack->nChild > 0) {
					$this->_setChildrenCompleted($childTrack);
				}
			}
		}

		//update completed children/descendants counting
		$track->nChildCompleted = $track->nChild;
		$track->nDescendantCompleted = $track->nDescendant;
		$track->save();
	}


	/**
	 * Synchronize item tracking and scorm tracking status. If no scorm tracking is set for this item, then create it from scratch
	 * @return array|bool|LearningScormTracking|mixed|null|static
	 */
	public function setScormTrackingStatus() {
		$output = false;
		if (!$this->isFolder()) {
			//"synchronyze" status in learning_scorm_tracking table
			$scormTrack = false;
			if ($this->idscorm_tracking) {
				$scormTrack = LearningScormTracking::model()->findByPk($this->idscorm_tracking);
			}
			/*
			$scormTrack = LearningScormTracking::model()->findByAttributes(array(
				'idUser' => $this->idUser,
				'idReference' => $this->idReference,
				'idscorm_item' => $this->idscorm_item
			));
			*/
			if ($scormTrack) {
				//set new status to the scormtracking record
				$scormTrack->lesson_status = $this->status;
			} else {
				//scorm tracking record does not exists, so create it (although with missing data)
				$userModel = CoreUser::model()->findByPk($this->idUser);
				$scormTrack = new LearningScormTracking();
				$scormTrack->idUser = $this->idUser;
				$scormTrack->idReference = $this->idReference;
				$scormTrack->idscorm_item = $this->idscorm_item;
				$scormTrack->user_name = LearningScormTracking::formatUserName($userModel->firstname, $userModel->lastname);
				$scormTrack->lesson_location = NULL;
				$scormTrack->credit = NULL;
				$scormTrack->lesson_status = $this->status;
				$scormTrack->entry = NULL;
				$scormTrack->score_raw = NULL;
				$scormTrack->score_max = NULL;
				$scormTrack->score_min = NULL;
				$scormTrack->total_time = LearningScormTracking::formatTotalTime(0);
				$scormTrack->lesson_mode = NULL;
				$scormTrack->exit = NULL;
				$scormTrack->session_time = NULL;
				$scormTrack->suspend_data = NULL;
				$scormTrack->launch_data = NULL;
				$scormTrack->comments = NULL;
				$scormTrack->comments_from_lms = NULL;
				$scormTrack->xmldata = NULL;
				$scormTrack->first_access = NULL;
				$scormTrack->last_access = NULL;
			}
			$scormTrack->save();
			//bond the new track record to item tracking
			if (!$this->idscorm_tracking) {
				$cmd = Yii::app()->db->createCommand("UPDATE ".self::model()->tableName()." SET idscorm_tracking = :idscorm_tracking WHERE idscorm_item_track = :idscorm_item_track");
				$cmd->execute(array(
					':idscorm_tracking' => $scormTrack->getPrimaryKey(),
					':idscorm_item_track' => $this->getPrimaryKey()
				));
			}
			//prepare output
			$output = $scormTrack;
		}
		return $output;
	}


	/**
	 * Detect if the current sco chapter is a folder or a playable chapter
	 * @return bool
	 */
	public function isFolder() {
		return (empty($this->idscorm_item) || (int)$this->nChild > 0 || (int)$this->nDescendant > 0);
	}



	public function afterSave() {
	}


	public function beforeDelete() {
		CommonTracker::delete($this->idReference, $this->idUser);
	}

	public function renderScore() {
		$html = '-';

		if ($this->status != self::STATUS_NOT_ATTEMPTED) {
			$scormItemsTracking = $this->scormItemsTracking;
			if ($scormItemsTracking)
				$html = $scormItemsTracking->renderScore();
		}

		return $html;
	}

	public function renderScoXmlReportUrl() {
		$url = Yii::app()->createUrl('player/report/scoXmlReport', array(
			'objectId' => $this->idscorm_item,
			'course_id' => Yii::app()->request->getParam('course_id'),
			'user_id' => $this->idUser
		));
		return $url;
	}

}