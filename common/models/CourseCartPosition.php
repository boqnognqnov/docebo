<?php
/**
 * Represents one shopping cart 'position', part of shoppingCart extension.
 * 
 * Example usage:
 * 	    $course_id = Yii::app()->request->getParam('course_id');
 *	    $position = CourseCartPosition::model()->findByPk($course_id);
 *	    Yii::app()->shoppingCart->put($position);
 *
 * @author Plamen
 * @package shopping cart
 *
 */
class CourseCartPosition extends LearningCourse implements IECartPosition {

    /** @var mixed Additional options to save, per position  */
    private $_options = null;
    
    //@todo Clarify editions and classrooms !!!!
    
    /**  @var array Set of editions this cart position is comprised, IF ANY */
    private $_editions = array();

    /**
     * @var Course session to enroll into
     */
    private $_session;

    /** @var array Set of course dates (classrooms) this cart position is comprised, IF ANY */
    private $_classrooms = array();


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourse the static model class
	 */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
            
    /**
     * Returns a unique shopping cart id.
     *
     * @see IECartPosition::getCartItemId()
     * @return int
     */
    public function getCartItemId(){
        return "course_".$this->idCourse;
    }
    
    
    /**
     * Returns the price of this course
     *
     * @see IECartPosition::getPrice()
     * @return float
     */
    public function getPrice() {

        // let plugins change the price
        Yii::app()->event->raise('OnRetrievingShoppingItemPrice', new DEvent($this, array('position'=>&$this)));

        $price = $this->prize;

        return $price;
    }

    /**
     * Updates the cart position's price
     * @param float $price
     */
    public function setPrice($price) {
        $this->prize = $price . '';
    }
    

    /**
     * Set position options
     * 
     * @param mixed $options
     */
    public function setOptions($options) {
        $this->_options = $options;
    }
    
    
    public function setEditions($editions) {
        $this->_editions = $editions;
    }

    public function setSession($session) {
        $this->_session = $session;
    }
    
    
    public function setClassrooms($classrooms) {
        $this->_classrooms = $classrooms;
    }
    
    
    
    /**
     * Returns position options
     * 
     * @return mixed
     */
    public function getOptions() {
        return $this->_options;
    }
    
    
    public function getEditions() {
        return $this->_editions;
    }

    public function getSession() {
        return $this->_session;
    }
    
    public function getClassrooms() {
        return $this->_classrooms;
    }


	public function getImage() {
		return $this->getCourseLogoUrl();
	}

	public function getPositionName() {
		return $this->name;
	}
}
