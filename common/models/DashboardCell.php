<?php

/**
 * This is the model class for table "dashboard_cell".
 *
 * The followings are the available columns in table 'dashboard_cell':
 * @property integer $id
 * @property integer $row
 * @property integer $position
 * @property integer $width
 *
 * The followings are the available model relations:
 * @property DashboardRow $dashboardRow
 * @property DashboardDashlet[] $dashlets
 */
class DashboardCell extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_cell';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('row, width', 'required'),
			array('row, position, width', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, row, position, width', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dashboardRow' => array(self::BELONGS_TO, 'DashboardRow', 'row'),
			'dashlets' => array(self::HAS_MANY, 'DashboardDashlet', 'cell', 'order' => 'dashlets.position ASC'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'row' => 'Row',
			'position' => 'Position',
			'width' => 'Width',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('row',$this->row);
		$criteria->compare('position',$this->position);
		$criteria->compare('width',$this->width);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardCell the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Count dashlets associated to this CELL
	 *
	 * @param integer $id
	 *
	 * @return integer
	 */
	public function countCellDashlets($id=false) {
		
		$availHandlers = array();
		foreach (Yii::app()->mydashboard->getAvailHandlers() as $key => $value)
			$availHandlers[] = "'" . $value . "'";
		
		
		if (!$id)
			$id = $this->id;
		$sql = "
			SELECT count(*) FROM dashboard_cell c
				JOIN dashboard_dashlet dl ON (dl.cell=c.id)
				WHERE (c.id=:id) AND (dl.handler IN (" . implode(",", $availHandlers) . "))
		";
		$command = Yii::app()->db->createCommand($sql);
		$number = $command->queryScalar(array(':id' => $id));
		return $number;
	}
	
	
}
