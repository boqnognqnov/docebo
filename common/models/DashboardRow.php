<?php

/**
 * This is the model class for table "dashboard_row".
 *
 * The followings are the available columns in table 'dashboard_row':
 * @property integer $id
 * @property integer $id_layout
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property DashboardCell[] $cells
 * @property DashboardLayout $layout
 */
class DashboardRow extends CActiveRecord
{
	
	const ROW_LAYOUT_1_12 	= 1;
	const ROW_LAYOUT_2_66 	= 2;
	const ROW_LAYOUT_3_4 	= 3;
	const ROW_LAYOUT_2_8_4 	= 4;
	const ROW_LAYOUT_2_4_8 	= 5;
	
	
	const ROW_LAYOUT_DEFAULT = self::ROW_LAYOUT_3_4;
	
	public $maxPosition;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dashboard_row';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_layout', 'required'),
			array('id_layout, position', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_layout, position', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cells' 	=> array(self::HAS_MANY, 'DashboardCell', 'row', 'order' => 'cells.id ASC'),
			'layout' => array(self::BELONGS_TO, 'DashboardLayout', 'id_layout'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_layout' => 'Id Layout',
			'position' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_layout',$this->id_layout);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DashboardRow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	
	/**
	 * Count dashlets associated to this row
	 * 
	 * @param integer $id
	 * 
	 * @return integer
	 */
	public function countRowDashlets($id=false) {
		
		$availHandlers = array();
		foreach (Yii::app()->mydashboard->getAvailHandlers() as $key => $value)
			$availHandlers[] = "'" . $value . "'";
		
		
		if (!$id)
			$id = $this->id;
		
		$sql = "
			SELECT count(*) FROM dashboard_row r
				JOIN dashboard_cell c ON (c.row=r.id)
				JOIN dashboard_dashlet dl ON (dl.cell=c.id)
				WHERE (r.id=:id) AND (dl.handler IN (" . implode(",", $availHandlers) . "))				
		";
		
		$command = Yii::app()->db->createCommand($sql);
		$number = $command->queryScalar(array(':id' => $id));
		return $number;
	}
	
	
	
	/**
	 * Create a new dashboard row using a given row layout/format
	 * 
	 * @param integer $dashboardLayoutId
	 * @param integer $rowLayout
	 * 
	 * @return DashboardRow
	 */
	public static function createNewRow($dashboardLayoutId, $rowLayout = self::ROW_LAYOUT_DEFAULT) {


		/*
		// Variant: New row on top
		// First, increase the position of all current rows by 1 (the requested layout only)
		$layoutModel = DashboardLayout::model()->findByPk($dashboardLayoutId);
		if (!$layoutModel) {
			return false;
		}
		$tmp = array();
		foreach ($layoutModel->rows as $row) {
			$row->position++;
			$tmp[] = $row->position;
			$row->save();
		}
		$position = 1;
		if (!empty($tmp))
			$position = min($tmp) - 1;
	
		*/
		
		
		// Variant: New row at the bottom
		// Get last positioned row in the given layout
		$criteria = new CDbCriteria;
		$criteria->select = 'max(position) AS maxPosition';
		$criteria->condition = 'id_layout=:id_layout';
		$criteria->params[':id_layout'] = (int) $dashboardLayoutId;
		$row = self::model()->find($criteria);
		$position = $row['maxPosition'];
		$position++;
		
		
		
		
		$newRow = new self();
		$newRow->id_layout = $dashboardLayoutId;
		$newRow->position = $position;
		$newRow->save();
		
		switch ($rowLayout) {
			case self::ROW_LAYOUT_1_12:
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 12;
				$cell->position = 1;
				$cell->save();
				
				break;
				
			case self::ROW_LAYOUT_2_66:
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 6;
				$cell->position = 1;
				$cell->save();
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 6;
				$cell->position = 2;
				$cell->save();
				
				break;
				
			case self::ROW_LAYOUT_3_4:
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 4;
				$cell->position = 1;
				$cell->save();
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 4;
				$cell->position = 2;
				$cell->save();
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 4;
				$cell->position = 3;
				$cell->save();
				
				
				break;
					
			case self::ROW_LAYOUT_2_8_4:
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 8;
				$cell->position = 1;
				$cell->save();
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 4;
				$cell->position = 2;
				$cell->save();
				
				break;
				
			case self::ROW_LAYOUT_2_4_8:
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 4;
				$cell->position = 1;
				$cell->save();
				
				$cell = new DashboardCell();
				$cell->row = $newRow->id;
				$cell->width = 8;
				$cell->position = 2;
				$cell->save();
				
				break;
				
		}
		
		return $newRow;
		
	}
	

	/**
	 * Detecet current row layout of a row
	 * 
	 * @param integer $id
	 * 
	 * @return string
	 */
	public function detectRowLayout($id=false) {
		if ($id === false)
			$id = $this->id;
		
		$sql = "SELECT group_concat(width) AS code FROM dashboard_cell c WHERE c.row=:id GROUP BY `row` ORDER BY c.position ASC";
		$code = Yii::app()->db->createCommand($sql)->queryScalar(array('id' => $id));
		
		switch ($code) {
			case "12": 
				$result = self::ROW_LAYOUT_1_12;
				break;
			case "6,6": 
				$result = self::ROW_LAYOUT_2_66;
				break;
				
			case "4,4,4": 
				$result = self::ROW_LAYOUT_3_4;
				break;
			case "8,4": 
				$result = self::ROW_LAYOUT_2_8_4;
				break;
			case "4,8": 
				$result = self::ROW_LAYOUT_2_4_8;
				break;
			
		}
		
		return $result;
		
	}
	
	
	
	
	public function rearrangeRowLayout($newRowLayout) {
		
		$currentCellsNumber = count($this->cells);
		$newCellsNumber 	= self::layoutCellsNumber($newRowLayout);
		$newCellWidths 		= self::layoutCellWidths($newRowLayout);

		// Rearrange in case of equal number of cells between old and new row layout, or when NEW cells are MORE than old ones
		if ($newCellsNumber >= $currentCellsNumber) {
			$i = 0;
			foreach ($this->cells as $cell) {
				$cell->width = $newCellWidths[$i];
				$cell->save();
				$i++;
			}

			$extraCells = $newCellsNumber - $currentCellsNumber;
			if ($extraCells > 0) {
				for ($i=0; $i < $extraCells; $i++) {
					$tmpCell = new DashboardCell();
					$tmpCell->width = $newCellWidths[$currentCellsNumber+$i];
					$tmpCell->row = $this->id;
					$tmpCell->save();
				}	
			}
		}
		// In case of new cells being LESS than current: we have to move all dashlets from beyond the last cell into the last NEW cell
		else {
			$i = 0;
			foreach ($this->cells as $cell) {
				// These are overlapping cells (old and new)
				if (isset($newCellWidths[$i])) {
					$cell->width = $newCellWidths[$i];
					$cell->save();
					$lastCell = $cell; // update last existing cell 
				}
				// Oops.. these current cell is beyond the new cells boundary; we have to move its dashlets into last one
				// Also, we have to remove this cell from database AFTER THAT
				else {
					foreach ($cell->dashlets as $dashlet) {
						$dashlet->cell = $lastCell->id;
						$dashlet->save();
					}
					$cell->delete();
				}
				$i++;
			}
			
		}
		
		
	}
	
	
	/**
	 * 
	 * @param unknown $rowLayout
	 * 
	 * @return number
	 */
	public static function layoutCellsNumber($rowLayout) {
		return count(self::layoutCellWidths($rowLayout));
	}
	
	
	/**
	 * Return array of widths for cells for a given row layout
	 * 
	 * @param integer $rowLayout
	 * 
	 * @return array
	 */
	public static function layoutCellWidths($rowLayout) {
		
		$result= array();
		
		switch ($rowLayout) {
			case self::ROW_LAYOUT_1_12:
				$result = array(12);
				break;
			case self::ROW_LAYOUT_2_66:
				$result = array(6,6);
				break;
			case self::ROW_LAYOUT_3_4:
				$result = array(4,4,4);
				break;
			case self::ROW_LAYOUT_2_8_4:
				$result = array(8,4);
				break;
			case self::ROW_LAYOUT_2_4_8:
				$result = array(4,8);
				break;
		}
		
		return $result;
		
		
	}


	/**
	 * Clone (copy) a given row (including its cell structure/layout and its dashlets)
	 * 
	 * @param integer $row
	 * @param boolean $cloneDashlets
	 * 
	 * @return DashboardRow
	 */
	public static function cloneRow($row, $cloneDashlets=true) {
		/* @var $newDashlet DashboardDashlet  */
		
		// Get source row model and create a brand new row of the same layout
		$sourceRowModel = self::model()->findByPk($row);
		$rowLayout = $sourceRowModel->detectRowLayout();
		$newRowModel = self::createNewRow($sourceRowModel->id_layout, $rowLayout);
		
		// If not asked to clone dashlets as well, get out, return the "sceleton" of the row only (empty cells) 
		if ($cloneDashlets === false)
			return $newRowModel;
		
		// Clone dashlets
		$sourceCells = $sourceRowModel->cells;
		$newCells	 = $newRowModel->cells;
		
		// Enumerate SOURCE cells and make a copy of their dashlets into the NEW row's cells		
		foreach ($sourceCells as $index => $sourceCell) {
			$newCell = $newCells[$index];
			foreach ($sourceCell->dashlets as $sourceDashlet) {

				// Create a new dashlet
				$newDashlet = new DashboardDashlet();

				$modelTranslations = DashboardDashletTranslation::model()->findAllByAttributes(array(
					'id_dashlet' => $sourceDashlet->id
				));

				// Get attributes of the Source dashlet and remove the "id", to avoid countless "cannot set safe attribute"
				$attrs = $sourceDashlet->attributes;
				unset($attrs['id']);

				// Assign attributes, but modify the CELL ID, to point to the newCell (part of the cloned row!)
				$newDashlet->attributes = $attrs;
				$newDashlet->cell 		= $newCell->id;
				$newDashlet->save();

				foreach($modelTranslations as $translation){
					$newTranslations = new DashboardDashletTranslation();
					$newTranslationsAttrs = $translation->attributes;
					$newTranslationsAttrs['id_dashlet'] = $newDashlet->id;
					$newTranslations->attributes = $newTranslationsAttrs;
					$newTranslations->save();
				}
			}
		}
		
		return $newRowModel;
	}
	
	
}
