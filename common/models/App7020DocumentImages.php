<?php
/**
 * This is the model class for table "app7020_document_images".
 *
 * @property integer $id
 * @property integer $idContent
 * @property string $imageName
 * @property timestamp $created
 *
 *
 * The followings are the available model relations:
 * @property App7020Content $content

 *
 */


class App7020DocumentImages extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'app7020_document_images';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        return array(
            array('idContent', 'required'),
            array('idContent', 'numerical', 'integerOnly' => true)
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'idContent' => 'Id Content',
            'imageName' => 'Image Name',
            'created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('idContent', $this->idContent);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return App7020DocumentImages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }


    /**
     * @param $filename
     * @param bool|false $storage
     * @return array
     */
    public static function getDocumentImagesFromS3($filename){


        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);

		$contentType = App7020Assets::$contentTypes[App7020Assets::getContentCodeByExt(App7020Helpers::getExtentionByFilename($filename))]['outputPrefix'];

		$pathToImages = CS3StorageSt::CONTRIBUTE_FOLDER .
						$contentType . "/" . 
						App7020Assets::DOCUMENT_IMAGES_S3_FOLDER . "/".
						App7020Helpers::getAmazonInputKey($filename);
        $files = $storage->findFiles($pathToImages);

        return $files;

    }
	
	 /**
     * Return public thumb URL for Video Content by their counter
     * @param int $countImg ex. 000001
     * @param App7020Content $content
     * @return string
     */
    public static function getDocumentImagesUrl($filename) {
        $url = CFileStorage::getS3DomainStorage(CFileStorage::COLLECTION_7020)
            ->fileUrl(
            CS3StorageSt::getConventionFolder($filename) . '/' .
            App7020Assets::DOCUMENT_IMAGES_S3_FOLDER .'/' .
            App7020Helpers::getAmazonInputKey($filename).'/'
             );
        return $url;
    }
	
	/**
     * Return an array of document images ordered by the page
     * @param int $countImg ex. 000001
     * @param App7020Content $content
     * @return string
     */
    public static function getDocumentImages($idContent) {
        $images = self::model()->findAllByAttributes(array('idContent' => $idContent));
		$contentObject = App7020Assets::model()->findByPk($idContent);
		$pages = array();
		foreach ($images as $key => $value) {
			$imageName = $value->imageName;
			$pages[$key]['imageName'] = $value->imageName;
			$tmpArray = explode(".", $imageName);
			$ext = end($tmpArray);
			$tmpImageName = str_replace('.'.$ext, '', $imageName);
			
			$fileName = $contentObject->filename;
			$tmpArray = explode(".", $fileName);
			$ext = end($tmpArray);
			$tmpFileName = str_replace('.'.$ext, '', $fileName);
			
			$pageNumber = str_replace($tmpFileName.'-', '', $tmpImageName);
			
			$pages[$key]['pageNumber'] = (int) $pageNumber;
		}
		$returnArray = App7020Helpers::sortMultyArray($pages, 'pageNumber', 'ASC');
		return $returnArray;
    }


    /**
     * Count document images ordered by the page
     * @param App7020Content $content
     * @return integer
     */
    public static function countDocumentImages($idContent) {
        $command = Yii::app()->db->createCommand();
        $command->select("COUNT(*)");
        $command->from(self::model()->tableName());
        $command->where("idContent=:id", array(':id' => $idContent));
        return $command->queryScalar();
    }
}

?>