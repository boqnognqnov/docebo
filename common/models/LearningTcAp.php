<?php

/**
 * This is the model class for table "learning_tc_ap".
 *
 * The followings are the available columns in table 'learning_tc_ap':
 * @property integer $id_tc_ap
 * @property string $registration
 * @property string $location
 * @property string $path
 * @property string $xapi_version
 * 
 * The followings are the available model relations:
 * @property LearningTcActivity[] $learningTcActivities
 * @property LearningTcActivity $mainActivity
 *
 * @package docebo.models
 * @subpackage table
 */
class LearningTcAp extends CActiveRecord
{
    
    const DOCEBO_CUSTOM_ACTIVITY_ID_BASE = "http://docebo.com/tincan/custom/activity";

    /**
     * A tincan.xml template, used to create TiCan LO playing external content, by URL
     * @var string
     */
    const TINCAN_XML_TEMPLATE = <<< EOT
<?xml version="1.0" encoding="utf-8" ?>
<tincan xmlns="http://projecttincan.com/tincan.xsd">
    <activities>
        <activity id="%ACTIVITY_ID%" type="http://adlnet.gov/expapi/activities/course">
            <name>%NAME%</name>
            <description lang="en-US">%DESCRIPTION%</description>
            <launch lang="en-us">%LAUNCH_URL%</launch>
        </activity>
    </activities>
</tincan>
EOT;
    

	/**
	 * Virtual property to save the id of Activity that is the launcher one (having <launch> node in tincan.xml)
	 * This is the id to save as idResource in LearningOrganization table, not the id of the Activity Provider!!!!)
	 * @var number
	 */
	public $launcher_tc_activity = 0;


	/**
	 * Another virtual property, to store the 'title' of the Activity Provider, again taken from the 'launcher' activity (name).
	 * @var string
	 */
	public $title = "n/a";


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTcAp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_tc_ap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('registration, location, path', 'required'),
			array('registration', 'required'),
			array('registration, location, path', 'length', 'max'=>255),
		    array('xapi_version', 'length', 'max'=>16),
		    
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_tc_ap, registration, location, path, xapi_version', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningTcActivities' => array(self::HAS_MANY, 'LearningTcActivity', 'id_tc_ap'),
			'mainActivity' => array(self::HAS_ONE, 'LearningTcActivity', array('id_tc_ap'=>'id_tc_ap'), 'condition'=>'mainActivity.launch <> ""'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tc_ap' => 'Id Tc Ap',
			'registration' => 'Registration',
			'location' => 'Location',
			'path' => 'Path',
		    'xapi_version' => 'Xapi Version',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tc_ap',$this->id_tc_ap);
		$criteria->compare('registration',$this->registration,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('xapi_version',$this->xapi_version, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}



	public function getId() { return $this->launcher_tc_activity; }
	public function setId($id) { $this->launcher_tc_activity = $id; }

	public function getTitle() { return $this->title; }
	public function setTitle($title) { $this->title = $title; }


	public function behaviors() {
		return array(
			'TinCan' => array(
				'class' => 'TinCanBehavior',
				'objectType' => LearningOrganization::OBJECT_TYPE_TINCAN,
				'idPropertyName' => 'launcher_tc_activity',
				'titlePropertyName' => 'title',
			),
		);
	}



	/**
	 * Generate random UUID in form of: 8-4-4-4-12
	 */
	public static function generateStatementUUID() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), // 8
			mt_rand( 0, 0xffff ), // 4
			mt_rand( 0, 0x0fff ) | 0x4000, // 4
			mt_rand( 0, 0x3fff ) | 0x8000, // 4
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) // 12
		);
	}

	/**
	 * Build the actor data with the learner data.
	 * @param object with all the current_user data
	 * @return array with actor data
	*/
	public function buildActor($userData) {
	    
	    $actor = array();
	     
	    if (version_compare($this->xapi_version, '0.9', '<=')) {
	        $actor['mbox'] = array("mailto:" . $userData->email);
	        $actor['name'] = array($userData->firstname." ".$userData->lastname);
	    }
	    else {
	        $actor['mbox'] = "mailto:" . $userData->email;
	        $actor['name'] = $userData->firstname." ".$userData->lastname;
	    }
	    
		return $actor;
	}

	/*
	 * Build the basic auth data.
	 * @param object with all the current_user data
	 * @return string
	*/
	public static function buildBasicAuth($userData) {
		return 'Basic '.base64_encode($userData->userid.':'.$userData->pass);
	}



	/**
	 * Retrieves package's activities
	 * @return array list of tha AP activities records
	 */
	public function getActivities() {
		return LearningTcActivity::model()->findAllByAttributes(array('id_tc_ap' => $this->getPrimaryKey()));
	}


	/**
	 * Unzip a given ZIP file into a given folder in the file storage.
	 * 
	 * @throws CException
	 */
	public function extractTincanPackage($archiveFilePath, $destFolderName) {

		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $destFolderName;

		try {
			// Check if a previous upload (path) exists; create folder if not
			if (!file_exists($unpackPath)) {
				if ( ! @mkdir($unpackPath, 0777) ) {
					throw new CException("Directory creation failed.");
				}
			}

			// ZIP Management
			$zip = new ZipArchive();
			// Open the zip file
			$zip_open = $zip->open($archiveFilePath);
			if ($zip_open !== true) {
				// unable to open the zip file
				throw new CException(Yii::t('player', 'This file is a not valid Zip Package (Error: {error_code})', array('{error_code}' => $zip_open)) .
					'. ' . Yii::t('tincan', 'Please check if your TINCAN package is correct and if the problem persist report the error message to the help desk team'));
			}
			// Checking if it is a valid TINCAN package
			if ($zip->locateName(LearningTcActivity::TINCAN_METADATA_FILE) === false) {
				// ok no tincan, throw this away
				throw new CException(Yii::t('tincan', 'The required tincan.xml file not found or is not in the zip root') .
					'. ' . Yii::t('tincan', 'Please check if your TINCAN package is correct and if the problem persist report the error message to the help desk team'));
			}
			// Extract the tin can package
			if( $zip->extractTo($unpackPath) !== true ) {
				throw new CException(Yii::t('player', 'Zip decompression failed (Error: {error_code})', array('{error_code}' => $zip->getStatusString())) .
					'. ' . Yii::t('tincan', 'Please check if your TINCAN package is correct and if the problem persist report the error message to the help desk team'));
				
			}
			$zip->close();

			// Save the tincan structure in it's final directory
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
			// Clean up old files, if exists
			if($storageManager->fileExists($destFolderName)) {
				$storageManager->removeFolder($destFolderName);
			}
			if ( ! $storageManager->store($unpackPath)) { throw new CException("Error while saving file to final storage."); }
			
			
			// Reading all activities
			$xml = @simplexml_load_file($unpackPath . DIRECTORY_SEPARATOR . LearningTcActivity::TINCAN_METADATA_FILE);
			if (!$xml) { throw new CException('Invalid metadata file'); }
			$activities = $xml->activities->children();
			
			if ($activities){
				// Better delete old activities from this TinCan package to avoid conflicts
				LearningTcActivity::model()->deleteAllByAttributes(array('id_tc_ap' => $this->id_tc_ap));
			}

			//other DB manipulation
			foreach ($activities as $activity) {
				$activityModel = new LearningTcActivity();
				$activityModel->id_tc_ap = (int) $this->getPrimaryKey();
				$activityModel->id = $activity->attributes()->id;
				$activityModel->type = $activity->attributes()->type;
				// database accept only 512 as the max number of char for the activity, but since storyline save
				// question that are longer we are going to cut it
				$activityModel->name = mb_substr($activity->name, 0, 512, "UTF-8");
				$activityModel->description = $activity->description;
				$activityModel->launch = $activity->launch;


				//storing all activities that belong to this AP
				$rs = $activityModel->save();

				// Save the id of an activity that has "launch" node in XML; also save 'name' as 'title' of the LO
				// There should be only one there (according to TinCan Spec.)
				if ($activityModel->launch != "") {
					//$this->owner->launcher_tc_activity = $activityModel->id_tc_activity;

					//If we are in Central Repository Context - skip the Learning Organization things
					if($this->scenario !== 'centralRepo'){
						$this->setId($activityModel->id_tc_activity);
						$this->setTitle($activityModel->name);
					}
				}

				if (!$rs) {
					throw new CException('Error while saving DB activity information; error(s): '.print_r($activityModel->getErrors(), true));
				}
			}

			//all went ok: commit DB changes
			if (isset($transaction)) { $transaction->commit(); }

			// remove tmp unpack dir and zip file
			FileHelper::removeDirectory($unpackPath);
			FileHelper::removeFile($archiveFilePath);

		}
		
		catch(Exception $e){
			//check transaction
			if (isset($transaction)) { $transaction->rollback(); }

			// remove tmp unpack dir and zip file
			FileHelper::removeDirectory($unpackPath);
			FileHelper::removeFile($archiveFilePath);

			//pass the exception at above level
			throw $e;
		}

		return true;

	}


	/**
	 * This function creates a copy of the record in the same table
	 * while also duplicating all activities in it
	 *
	 * @param bool $organization - Should we create the LearningOrganization model as well (unused for now)
	 *
	 * @throws CException
	 * @return LearningTcActivity the main activity in the copied TinCan
	 */
	public function copy($organization = true) {

		/*if (!$organization) {
			$copy->disableLearningObjectsEvents(); //this is important, otherwise it will break DB
		}*/

		// Copy the TinCan package in DB (but use the same resource files from storage),
		// and generate a new 'registration' since lrs_* tracking
		// tables depend on its uniqueness
		$copiedPackage = clone $this;
		$copiedPackage->id_tc_ap = null;
		$copiedPackage->registration = self::generateStatementUUID();
		$copiedPackage->setIsNewRecord(true);
		if (!$copiedPackage->save()) {
			$errorMessage = 'Error while copying TinCan package with regenerated "registration": '.print_r($copiedPackage->getErrors(), true);
			Yii::log($errorMessage);
			throw new CException($errorMessage);
		}

		// After duplicating all activities in the TinCan package
		// also, this will hold the Main Activity in the duplicated package
		$mainActivity = false;

		$activities = $this->getActivities();
		if (!empty($activities)) {
			foreach ($activities as $activity) {
				$copiedActivity = new LearningTcActivity();
				$copiedActivity->id_tc_ap = $copiedPackage->getPrimaryKey();
				$copiedActivity->id = $activity->id;
				$copiedActivity->type = $activity->type;
				$copiedActivity->name = $activity->name;
				$copiedActivity->description = $activity->description;
				$copiedActivity->launch = $activity->launch;
				if (!$copiedActivity->save()) {
					$errorMessage = 'Error while copying LO: '.print_r($copiedActivity->getErrors(), true);
					Yii::log($errorMessage);
					throw new CException($errorMessage);
				}

				if($copiedActivity->launch){
					// This is a main TinCan activity
					$mainActivity = $copiedActivity;
				}
			}
		}

		return $mainActivity;

	}

	
	/**
	 * @return Zip
	 */
	public function archivePackage() {
		
		$zipFileName 	= $this->path  . ".zip";
		$localFileDir 	= Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->path;
		$zipFilePath 	= realpath(Docebo::getUploadTmpPath()) . DIRECTORY_SEPARATOR . $zipFileName;

		$localFileDir = rtrim($localFileDir, "\\/");
		
		if (file_exists($localFileDir)) {
			FileHelper::removeDirectory($localFileDir);
		}
		
		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
		$result = $storageManager->downloadCollectionAs($localFileDir, $this->path);
		
		$localFileDir = realpath($localFileDir);
		if (!is_dir($localFileDir)) {
			Yii::log("Error while downloading package");
			Xapi::out(Xapi::HTTP_SERVER_ERROR, "Error while downloading package");
			return false;
		}
		
		// Use the LMS Component Zip wrapper we have, NOT directly the ZipArchive!
		$zip = new Zip($zipFilePath);
		$zip->addFolder($localFileDir);
		$zip->close();
		
		if ($zip->lastError) {
		    Yii::log("Error while creating a zip file: " . $zip->lastErrorMessage, CLogger::LEVEL_ERROR);
		    return false;
		}
		
		$uploadTmpStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
		
		$result = array(
		    'zipFilePath'     => $zipFilePath,
		    'zipFuleUrl'      => $uploadTmpStorage->fileUrl($zipFileName),
		);

		return $result;
		
	}
	

	
	/**
	 * Return the LO model associated to this activity provider
	 * 
	 * @return LearningOrganization
	 */
	public function learningOrganization() {
	    
	    $params = array();
	    $command = Yii::app()->db->createCommand();
	     
	    $command->select('idOrg')
	       ->from("learning_organization lo")
	       ->join("learning_tc_activity tca"   , "lo.idResource=tca.id_tc_activity AND (lo.objectType=:tincan OR lo.objectType=:elucidat)")
	       ->join("learning_tc_ap tcap"        , "tcap.id_tc_ap=tca.id_tc_ap AND tcap.registration=:registration");
	     
	    $params[":registration"] = $this->registration;
	    $params[":tincan"]      = LearningOrganization::OBJECT_TYPE_TINCAN;
	    $params[":elucidat"]    = LearningOrganization::OBJECT_TYPE_ELUCIDAT;
	     
	    $idOrg = $command->queryScalar($params);
	     
	    $model = LearningOrganization::model()->findByPk($idOrg);
	
	    return $model;
	
	}

	
	public static function createTemporaryTincanZip($urlId, $name, $description) {
	     
	    $randomHash    = Docebo::randomHash();
	    $activityId    = LearningTcAp::DOCEBO_CUSTOM_ACTIVITY_ID_BASE . "/" . $randomHash;
	    $urlModel      = CoreSettingTincanUrl::model()->findByPk($urlId);
	
	    if (!$name || !$urlModel || !$description) {
	        throw new CException(Yii::t("standard", "TinCan URL, title and description are all required"));
	    }
	     
	    $xmlText = LearningTcAp::TINCAN_XML_TEMPLATE;
	    $xmlText = str_replace("%ACTIVITY_ID%", $activityId, $xmlText);
	    $xmlText = str_replace("%NAME%", $name, $xmlText);
	    $xmlText = str_replace("%DESCRIPTION%", $description, $xmlText);
	    $xmlText = str_replace("%LAUNCH_URL%",  $urlModel->url, $xmlText);
	
	    $dir            = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $randomHash;
	    $tincanXmlFile  = $dir . DIRECTORY_SEPARATOR . "tincan.xml";
	    $zipFileName    = $randomHash . ".zip";
	    $zipFilePath    = Docebo::getUploadTmpPath(). DIRECTORY_SEPARATOR . $zipFileName;
	
	    // Create directory
	    FileHelper::createDirectory($dir, null, true);
	    file_put_contents($tincanXmlFile, $xmlText);
	
	    // ZIP Management
	    $zip = new ZipArchive();
	    $zip->open($zipFilePath, ZipArchive::CREATE);
	    $zip->addFile($tincanXmlFile, "tincan.xml");
	    $zip->close();
	
	    $file = array(
	        'id'            => $randomHash,
	        'name'          => $zipFileName,
	        'percent'       => 100,
	        'target_name'   => $zipFileName,
	    );
	
	    // Remove this temporary directory
	    FileHelper::removeDirectory($dir);
	
	    return $file;
	     
	}

	
	/**
	 * Generate and return OAuth2 authorization code for spceial xapi_auth_code OAuth check
	 * @return string
	 */
	public static function getOAuthAuthorizationCode($clientId)
	{
	    $authCode = "";
	    $server = DoceboOauth2Server::getInstance();
	    if ($server) {
	        $server->init();
	        $authCode = $server->generateAuthorizationCode(Yii::app()->user->id, $clientId, null, 'api', 30);
	    }
	    return $authCode;
	}
	
	
}