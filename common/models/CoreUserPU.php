<?php

/**
 * This is the model class for table "core_user".
 * The followings are the available columns in table 'core_user':
 * @property integer $puser_id
 * @property string $user_id
 *
 * @package docebo.models
 * @subpackage table
 */
class CoreUserPU extends CActiveRecord {

	public $confirm;
	public $search_input;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUser the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_user_pu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('userid', 'uniqueUserid', 'on' => 'create, update, import'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'powerUser' => array(self::BELONGS_TO, 'CoreUser', 'puser_id'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'user_id'),
		);
	}

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}

	public function beforeDelete() {
		//remove all assigned courses
		CoreUserPuCourse::model()->deleteAllByAttributes(array('puser_id' => $this->puser_id));
		//remove all assigned learning plans
		CoreUserPuCoursepath::model()->deleteAllByAttributes(array('puser_id' => $this->puser_id));
		//remove all locations
		CoreUserPuLocation::model()->deleteAllByAttributes(array('puser_id' => $this->puser_id));

		return parent::beforeDelete();
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			// 'idst' => 'Idst',
			// 'userid' => 'Username',
		);
	}

	public function dataProvider() {
		$criteria = new CDbCriteria;
		$config = array();

		// Only users assigned to the Power User
		$criteria->addCondition('puser_id = :userId');

		$criteria->params = array(':userId'=>Yii::app()->session['currentPowerUserId']);
		$criteria->compare('t.user_id', $this->user_id, true);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$sortAttributes['userLanguage'] = 'language.lang_description';

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}



	/**
	 * Takes the Core Admin Tree table and expands the list of items there (which are users OR groups) into a simple list of users only
	 *
	 * @param int $idAdmin  Power user id
	 * @throws CException
	 * @return boolean
	 */
	public function updatePowerUserSelection($idAdmin) {

		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
			$users = array();
			$groups = array();

			// First, Assign this power user to himself. if not yet assigned
			// This is to allow Power User to manage himself.
			$puModel = CoreAdminTree::model()->findByAttributes(array('idst' => $idAdmin, 'idstAdmin' => $idAdmin));
			if (!$puModel) {
				CoreAdminTree::addPowerUserMember($idAdmin, $idAdmin, false);  // false: do NOT update internally!!!!!!!
			}

			$command = Yii::app()->db->createCommand();
			$command->select('idst');
			$command->from(CoreAdminTree::model()->tableName());
			$command->where('idstAdmin = :idstAdmin');
			$command->params[':idstAdmin'] = $idAdmin;

			$list = $command->queryColumn();

			$users = CoreUser::getUsersByIdSt($list);
			$groups = CoreGroup::getGroupsByIdSt($list);

			//Ok remove the current presaved selection ... it's really needed ? Yes :P
			$rs = self::model()->deleteAllByAttributes(array('puser_id' => $idAdmin));
			if ($rs === FALSE) { throw new CException('Error while updating power user selection'); }

			// Insert the users directly (if any)
			if(!empty($users))
			{
				$sql = "INSERT IGNORE INTO " . CoreUserPU::model()->tableName() . " (puser_id, user_id) "
					. "( SELECT " . $idAdmin . ", idst"
					. " FROM " . CoreUser::model()->tableName()
					. "	WHERE idst IN (" . implode(",", $users) . ") ) ";
				$rs = Yii::app()->db->createCommand($sql)->execute();
				// IMPORTANT : execute return the number of affected row, not true of false, this query can
				// return 0 rows in some cases, so the correct behaviour is to listen for exceptions

				/*if (!$rs) {
					throw new CException('Error while updating power user selection');
				}*/
			}

			if (!empty($groups))
			{
				$sql = "INSERT IGNORE INTO " . CoreUserPU::model()->tableName() . " (puser_id, user_id) "
					. "( SELECT " . $idAdmin . ", idst"
					. " FROM " . CoreGroup::model()->tableName()
					. "	WHERE idst IN (" . implode(",", $groups) . ") ) ";
				$rs = Yii::app()->db->createCommand($sql)->execute();

				$command = Yii::app()->db->createCommand();
				$command->select('count(*)');
				$command->from(CoreGroupMembers::model()->tableName());
				$command->where('idst IN ('.implode(',', $groups).')');

				$count = $command->queryScalar();
				if($count > 0)
				{
					// for groups we are going to do the insertion with select insert
					$sql = "INSERT IGNORE INTO " . CoreUserPU::model()->tableName() . " (puser_id, user_id) "
						. "( SELECT " . $idAdmin . ", idstMember"
						. " FROM " . CoreGroupMembers::model()->tableName()
						. "	WHERE idst IN (" . implode(",", $groups) . ") ) ";
					$rs = Yii::app()->db->createCommand($sql)->execute();
					/*if (!$rs) {
						throw new CException('Error while updating power user selection: '. $sql);
					}*/
				}
			}

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return true;
	}

	/**
	 * Get the list of user/group IDs assigned to a given Power User (with optional cache).
	 * If the PU is not provided, the currently logged in user's ID is used.
	 *
	 * @param bool $idAdmin - The ID of the PU for which to retrieve users or null/false for current user
	 * @param bool $cache - whether or not to cache the list of users (during the current request only)
	 *
	 * @return array User/group IDs
	 */
	static public function getList($idAdmin = false, $cache = true) {
		if(!$idAdmin) $idAdmin = Yii::app()->user->getId();

		static $_listCache = array();

		if ($cache){

			if(isset($_listCache[$idAdmin]))
				return $_listCache[$idAdmin];
		}

		$db = Yii::app()->getDb();

		$output = $db->createCommand()
			->select("user_id")
			->from(self::model()->tableName())
			->where("puser_id = :id_admin", array(':id_admin' => $idAdmin))
			->queryColumn();

		// Preserve the result of assigned user IDs
		// for the provided Power user ID
		if($cache && $output)
			$_listCache[$idAdmin] = $output;

		return $output;
	}

	static public function checkFolderAssociation($idst_oc, $idst_ocd, $pu_id = false)
	{
		$command = Yii::app()->db->createCommand();
		$command->select("count(*)");
		$command->from(self::model()->tableName());
		$condition = '(user_id = :idst_oc or user_id = :idst_ocd)';
		$params = array(':idst_oc' => $idst_oc, ':idst_ocd' => $idst_ocd);
		if($pu_id) {
			$condition .= ' AND puser_id = :pu_id';
			$params[':pu_id'] = $pu_id;
		}
		$command->where($condition, $params);

		if($command->queryScalar() > 0)
			return true;
		return false;
	}
	
	/**
	 * 
	 * @param int $powerUserId
	 * @return static
	 */
	static public function listUsersOfPowerUserInWaitingList( $powerUserId )
	{

		if ($powerUserId){
			
			$criteria = new CDbCriteria;
			$criteria->select = 'idst'; // select fields which you want in output
			$idsObj = CoreUserTemp::model()->findAll($criteria);
		
		
			foreach ($idsObj as $idTmp){
				$idsTempArr[]=$idTmp->idst;
			}

			if ( is_array($idsTempArr) ){	
				$CriteriaPU = new CDbCriteria();
				$CriteriaPU->condition = "(puser_id = $powerUserId) and (user_id in (".implode(' , ',$idsTempArr)."))";
				$userPuSlaves = CoreUserPU::model()->findAll($CriteriaPU);
				}
		
			}
		
		return $userPuSlaves;

		}
	
	static public function isPUAndSeatManager(){
		return Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats');
	}

	/**
	 * By passing a list of users, retrieve it's P.U. from branches/groups and update power users assignments
	 * @param $users a list of idst
	 * @return bool successful operation or not
	 */
	static public function updatePowerUsersOfUsers($users) {
		if (is_numeric($users) && (int)$users > 0) {
			$users = array((int)$users);
		}

		if (!is_array($users)) {
			return false;
		}

		if (empty($users)) {
			return true; //no users to be processed ... just go away from here
		}

		//retrieve involved PUs with new users
		$pus = array();

		//users list may be too big for the queries: break it in parts of safe size if needed
		$parts = array_chunk($users, 1000);
		foreach ($parts as $part) {

			if (empty($part)) { continue; }

			// extract pus from users groups
			$cmd = Yii::app()->db->createCommand()
				->select("at.idstAdmin")
				->from(CoreGroupMembers::model()->tableName() . " gm")
				->join(CoreAdminTree::model()->tableName() . " at", "gm.idst = at.idst")
				->where(array("IN", "gm.idstMember", $part));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[] = $row['idstAdmin'];
			}

		}

		//update every PU's users selection
		if (!empty($pus)) {
			$pus = array_unique($pus); //avoid double PUs references
			foreach ($pus as $pu) {
				self::model()->updatePowerUserSelection($pu);
			}
		}

		return true;
	}


	/**
	 * Increase or decrease the available seats inside a course
	 * for a given PU (decrease by passing a negative integer)
	 *
	 * @param      $idCourse
	 * @param int  $seatsNumToIncreaseOrDecrease - may be a positive or negative number
	 * @param null $forPuUserId
	 *
	 * @return bool
	 */
	static public function modifyAvailableSeats($idCourse, $seatsNumToIncreaseOrDecrease = 0, $forPuUserId = null){
		if(!$forPuUserId) $forPuUserId = Yii::app()->user->getIdst();

		$puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id'=>$forPuUserId,
			'course_id'=>$idCourse,
		));
		if($puCourseModel){
			$puCourseModel->available_seats += intval($seatsNumToIncreaseOrDecrease);
			$save = $puCourseModel->save();

			if($save) {
				if ( $seatsNumToIncreaseOrDecrease > 0 ) {
					Yii::log( 'Increased seats for course ' . $idCourse . ' and power user ' . $forPuUserId . ' by ' . $seatsNumToIncreaseOrDecrease, CLogger::LEVEL_INFO );
				} else {
					Yii::log( 'Decreased seats for course ' . $idCourse . ' and power user ' . $forPuUserId . ' by ' . $seatsNumToIncreaseOrDecrease, CLogger::LEVEL_INFO );
				}
				return true;
			}
		}

		return false;
	}

	/**
	 * getCoursePUAndSeatManagers()
	 * @param $idCourse
	 * @return array of (power) user ids, that are seat managers
	 */
	public function getCoursePUAndSeatManagers($idCourse) {
		$courseSeatManagers = array();
		$coursePus = CoreUserPuCourse::model()->findAllByAttributes(array('course_id' => (int) $idCourse));

		if(is_array($coursePus) && count($coursePus)) {
			foreach($coursePus as $coursePu) {
				if (CoreUser::adminRuleEnabled($coursePu->puser_id, 'allow_buy_seats')) {
					$courseSeatManagers[$coursePu->puser_id] = $coursePu->puser_id;
				}

			}
		}

		return $courseSeatManagers;
	}

	/**
	 * Check if a course is assigned to a power user
	 * @param $idUser
	 * @param $idCourse
	 * @return mixed
	 */
	public static function isAssignedToPU($id, $type = false) {
                  $pUser = Yii::app()->user->id;
                  switch($type) {
                      case 'user':
                      default:
                          $table = self::model('CoreUserPU');
                          $field = 'user_id';
                          break;
                      case 'course':
                          $table = self::model('CoreUserPuCourse');
                          $field = 'course_id';
                          break;
                  }
		$count = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from($table->tableName())
			->where("puser_id = :puser_id AND {$field} = :{$field}", array(
				':puser_id' => $pUser,
				":{$field}" => $id
			));
                  
		return (bool) ($count->queryScalar());
	}
}