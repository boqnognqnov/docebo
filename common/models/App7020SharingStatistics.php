<?php

/**
 * Description of App7020SharingStatistics
 *
 * @author vbelev
 */
class App7020SharingStatistics extends CModel {

	/**
	 * Timeframe for query to get "Activity". The value is number of days ago from now
	 * @var array
	 */
	const TIMEFRAME_DAILY = 1;
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;

	public static $timeline;
	public static $idUser;

	public function attributeNames() {
		return true;
	}

	/**
	 * Calculates average rating of one or more assets, uploaded by given user, restricted by timeframe
	 * @param type $timeline
	 * @param type $idUser
	 * @param type $idContent
	 */
	public static function averageRating($timeline = null, $idUser = null, $idContent = null) {
		self::_getDefaultStatisticsValues($timeline, $idUser);
		$command = Yii::app()->db->createCommand();
		$command->select("SUM(rating) AS contentRating, COUNT(cr.id) AS contentRatingCount");
		$command->from(App7020ContentRating::model()->tableName() . " cr");
		$command->join(App7020Assets::model()->tableName() . " a", "a.id = cr.idContent");
		$command->andWhere('a.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()', array(':timeInterval' => self::$timeline));
		$command->andWhere('a.userId = :idUser', array(':idUser' => self::$idUser));
		if ((int) $idContent) {
			$command->andWhere('a.id = :idContent', array(':idContent' => $idContent));
		}
		$result = $command->queryRow();
		return ($result['contentRatingCount']) ? round($result['contentRating'] / $result['contentRatingCount'], 1) : 0;
	}

	/**
	 * Get uploaded contents by user restricted by timeframe
	 * @param type $timeline
	 * @param type $idUser
	 */
	public static function sharedContents($timeline = null, $idUser = null) {
		self::_getDefaultStatisticsValues($timeline, $idUser);
		$command = Yii::app()->db->createCommand();
		$command->select("COUNT(id)");
		$command->from(App7020Assets::model()->tableName());
		$command->andWhere('created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()', array(':timeInterval' => self::$timeline));
		$command->andWhere('userId = :idUser', array(':idUser' => self::$idUser));

		return $command->queryScalar();
	}

	/**
	 * Calculates average watch rate of assets, uploaded by given user, restricted by timeframe
	 * @param type $timeline
	 * @param type $idUser
	 * @param type $idContent
	 */
	public static function averageWatchRate($timeline = null, $idUser = null) {
		self::_getDefaultStatisticsValues($timeline, $idUser);

		$command = Yii::app()->db->createCommand();
		$command->select("c.id, "
			. "(SELECT COUNT(id) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = c.id) as invitations, "
			. "(SELECT COUNT(DISTINCT idUser) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent = c.id) AS views");
		$command->from(App7020Assets::model()->tableName() . ' c');

		$command->andWhere('c.userId = :idUser', array(':idUser' => self::$idUser));
		$command->andWhere('c.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()', array(':timeInterval' => self::$timeline));

		$result = $command->queryAll();

		$watchRates = array();
		foreach ($result as $value) {
			$watchRates[] = ($value['invitations']) ? ($value['views'] * 100 / $value['invitations']) : 0;
		}

		return ($watchRates) ? round(array_sum($watchRates) / count($watchRates), 2) : 0;
	}

	/**
	 * Get channels sharing data, grouped bu channels, restricted to the assets uploaded by the given user and timeframe
	 * @param type $timeline
	 * @param type $idUser
	 * @return array
	 */
	public static function chartMyActivityPerChannel($timeline = null, $idUser = null) {

		self::_getDefaultStatisticsValues($timeline, $idUser);

		$command = Yii::app()->db->createCommand();
		$command->select("c.id AS channel_id, COUNT(*) AS count_assets");
		$command->from(App7020Channels::model()->tableName() . " c");
		$command->join(App7020ChannelAssets::model()->tableName() . " ca", "ca.idChannel=c.id");
		$command->join(App7020Assets::model()->tableName() . " a", "ca.idAsset=a.id");
		$command->andWhere('a.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()', array(':timeInterval' => self::$timeline));
		$command->andWhere('a.userId = :idUser', array(':idUser' => self::$idUser));
		$command->group("c.id");


		$results = $command->queryAll();
		$resultArray = array();
		$allAssets = 0;
		if ($results) {
			// Get Count of all Assets to calculate percenrage of every channel
			$allAssets = array_sum(array_column($results, 'count_assets'));

			foreach ($results as $result) {
				$channelModel = App7020Channels::model()->findByPk($result['channel_id']);
				$translation = $channelModel->translation();
				$resultArray[] = array(
					'label' => $translation['name'],
					'value' => round(($result['count_assets'] / $allAssets) * 100, 2),
					//'value' => $result['count_assets'],
					'color' => $channelModel->icon_bgr,
					'highlight' => $channelModel->icon_bgr,
					'text_color' => $channelModel->icon_color
				);
			}
		} else {
			$resultArray[] = array(
				'label' => Yii::t('app7020', 'No data available'),
				'value' => 0.1,
				'color' => '#030303',
				'highlight' => '#030303',
				'text_color' => '#ffffff'
			);
		}
		return $resultArray;
	}

	/**
	 * Get most voted asset
	 * @param type $idUser
	 * @return array
	 */
	public static function getMostVottedAsset($idUser = null, $asArrayDataProvider = false, $customConfig = array('pagination' => true, 'pageSize' => 7)) {

		self::_getDefaultStatisticsValues(null, $idUser);

		$command = Yii::app()->db->createCommand();
		$command->select("c.id AS id, c.title AS title, c.contentType AS contentType, c.duration AS duration, "
			. "(SELECT COUNT(id) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent = c.id) as votes, "
			. "(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent = c.id) AS contentRating");
		$command->from(App7020Assets::model()->tableName() . ' c');

		$command->andWhere('c.userId = :idUser', array(':idUser' => self::$idUser));

		$command->order('votes DESC');

		$results = $command->queryAll();


		$assetArray = array();


		foreach($results as $result){
			$assetArray[] = array(
				'id_asset' => $result['id'],
				'title' => $result['title'],
				'type' => $result['contentType'],
				'typename' => App7020Assets::$contentTypes[$result['contentType']]['outputPrefix'],
				'duration' => gmdate("H:i:s", $result['duration']),
				'votes' => $result['votes'],
				'rating' => $result['contentRating'],
			    'url' => Docebo::createApp7020AssetsViewUrl($result['id']),
				'thumbnail' => App7020Assets::getAssetThumbnailUri($result['id']),
				'sections' => array(
					array(
						'title' => Yii::t('app7020', 'Asset Rating'),
						'counter' => $result['contentRating'],
						'rating' => true
					),
					array(
						'title' => Yii::t('app7020', 'Number of votes'),
						'counter' => $result['votes'],
					)
				)
			);
		}
		
		if (!$asArrayDataProvider) {
			return $assetArray;
		} else {
			if (!empty($customConfig['pagination'])) {
				$pageSize = $customConfig['pageSize'] ? $customConfig['pageSize'] : 7;
				$providerConfig['pagination'] = array('pageSize' => $pageSize);
			} else {
				$providerConfig['pagination'] = false;
			}
			
			$dataProvider = new CArrayDataProvider($assetArray, $providerConfig);
			return $dataProvider;
		}
	}

	/**
	 * Get most watched asset
	 * @param type $idUser
	 * @return array
	 */
	public static function getMostWatchedAsset($idUser = null, $asArrayDataProvider = false, $customConfig = array('pagination' => true, 'pageSize' => 7), $timeframe = false, $limit) {

		self::_getDefaultStatisticsValues(null, $idUser);
        /**
         *
         * SELECT c.id, count(*) as top
        FROM app7020_content c
        LEFT JOIN app7020_content_history cp ON cp.idContent = c.id
        WHERE cp.idUser = 12301 AND cp.viewed BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW()
        GROUP BY cp.idContent
        ORDER BY top DESC
        LIMIT 3


         */
		$command = Yii::app()->db->createCommand();
		$command->select("c.id AS id, c.title AS title, c.contentType AS contentType, c.duration AS duration,"
			. "(SELECT COUNT(DISTINCT idUser) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent = c.id) AS top");

        $command->from(App7020Assets::model()->tableName() . ' c');
        $command->leftJoin(App7020ContentHistory::model()->tableName(). ' ch', 'ch.idContent = c.id');

		$command->where('c.userId = :idUser', array(':idUser' => $idUser));
		if($timeframe) {
			$command->andWhere('ch.viewed BETWEEN (NOW() - INTERVAL :timeframe DAY) AND NOW()', array(":timeframe" => $timeframe));
            $command->group('ch.idContent');
		}
		$command->order('top DESC');
		if($limit) {
			$command->limit($limit);	
		}
		$results = $command->queryAll();

		$assetArray=array();
		foreach($results as $result){
			$assetArray[] = array(
				'id_asset' => $result['id'],
				'title' => $result['title'],
				'type' => $result['contentType'],
				'typename' => App7020Assets::$contentTypes[$result['contentType']]['outputPrefix'],
				'duration' => gmdate("H:i:s", $result['duration']),
				'views' => App7020ContentHistory::getContentViews($result['id']),
				'current_views' => $result['top'],
			    'url' => Docebo::createApp7020AssetsViewUrl($result['id']),
				'thumbnail' => App7020Assets::getAssetThumbnailUri($result['id']),
				'sections' => array(
					array(
						'title' => Yii::t('app7020', 'Number of views'),
						'counter' => $result['watched']
					),
					array(
						'title' => Yii::t('app7020', 'Total Watch Time'),
						'counter' => 'N/A',
					)
				)
			);
		}
		if (!$asArrayDataProvider) {
			return $assetArray;
		} else {
			if (!empty($customConfig['pagination'])) {
				$pageSize = $customConfig['pageSize'] ? $customConfig['pageSize'] : 7;
				$providerConfig['pagination'] = array('pageSize' => $pageSize);
			} else {
				$providerConfig['pagination'] = false;
			}
			
			$dataProvider = new CArrayDataProvider($assetArray, $providerConfig);
			return $dataProvider;
		}
	}

	/**
	 * Get most shared asset
	 * @param type $idUser
	 * @return array
	 */
	public static function getMostSharedAsset($idUser = null, $asArrayDataProvider = false, $customConfig = array('pagination' => true, 'pageSize' => 7)) {

		self::_getDefaultStatisticsValues(null, $idUser);

		$command = Yii::app()->db->createCommand();
		$command->select("c.id AS id, c.title AS title, c.contentType AS contentType, c.duration AS duration, "
			. "(SELECT COUNT(id) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = c.id) as invitations, "
			. "(SELECT COUNT(id) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = c.id AND idInviter <> " . self::$idUser . ") as invitations_by_users, "
			. "(SELECT COUNT(id) FROM " . App7020Invitations::model()->tableName() . " WHERE idContent = c.id AND idInviter = " . self::$idUser . ")  as invitations_by_me, "
			. "(SELECT COUNT(DISTINCT idUser) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent = c.id) AS views");
		$command->from(App7020Assets::model()->tableName() . ' c');

		$command->andWhere('c.userId = :idUser', array(':idUser' => self::$idUser));

		$command->order('invitations DESC');
		//$command->limit(1);

		$results = $command->queryAll();
		$assetArray = array();


		foreach ($results as $result) {
			$assetArray[] = array(
				'id_asset' => $result['id'],
				'title' => $result['title'],
				'type' => $result['contentType'],
				'typename' => App7020Assets::$contentTypes[$result['contentType']]['outputPrefix'],
				'duration' => gmdate("H:i:s", $result['duration']),
				'invitations_by_users' => $result['invitations_by_users'],
				'invitations_by_me' => $result['invitations_by_me'],
			    'url' => Docebo::createApp7020AssetsViewUrl($result['id']),
				'thumbnail' => App7020Assets::getAssetThumbnailUri($result['id']),
				'watch_rate' => ($result['invitations']) ? round($result['views'] * 100 / $result['invitations'], 2) : 0,
				'sections' => array(
					array(
						'title' => Yii::t('app7020', 'Invitations sent by the users'),
						'counter' => $result['invitations_by_users']
					),
					array(
						'title' => Yii::t('app7020', 'Social shares'),
						'counter' => 'N/A'
					),
					array(
						'title' => Yii::t('app7020', 'Invitations sent by me'),
						'counter' => $result['invitations_by_me']
					),
					array(
						'title' => Yii::t('app7020', 'Watch rate'),
						'counter' => (($result['invitations']) ? round($result['views'] * 100 / $result['invitations'], 2) : 0).'%'
					)
				)
			);
		}
		if (!$asArrayDataProvider) {
			return $assetArray;
		} else {
			if (!empty($customConfig['pagination'])) {
				$pageSize = $customConfig['pageSize'] ? $customConfig['pageSize'] : 7;
				$providerConfig['pagination'] = array('pageSize' => $pageSize);
			} else {
				$providerConfig['pagination'] = false;
			}
			
			$dataProvider = new CArrayDataProvider($assetArray, $providerConfig);
			return $dataProvider;
		}
	}

	protected static function _getDefaultStatisticsValues($timeline, $idUser) {
		self::$timeline = (!(int) $timeline) ? self::TIMEFRAME_WEEKLY : $timeline;
		self::$idUser = (!(int) $idUser) ? Yii::app()->user->id : $idUser;
	}

	public static function sharedAssetsViews($timeline = null, $idUser = null, $idContent = null) {

		self::_getDefaultStatisticsValues($timeline, $idUser);
		$command = Yii::app()->db->createCommand();
		if (self::$timeline == self::TIMEFRAME_YEARLY) {
			$command->select("DATE_FORMAT(created, '%Y-%m') AS period, "
                . "COUNT(DISTINCT content.id) AS assets");
			$groupBy = "DATE_FORMAT(created, '%m')";
		} else {
			$command->select("DATE_FORMAT(created, '%c-%d') AS period, "
                . "COUNT(DISTINCT content.id) AS assets");
			$groupBy = "DATE(created)";
		}
		$command->from(App7020Content::model()->tableName() . " content");
		$command->andWhere("content.userId = :idUser", array(":idUser" => self::$idUser));
		$command->andWhere('created BETWEEN (NOW() - INTERVAL :timeline DAY) AND NOW()', array(":timeline" => self::$timeline));
		$command->group($groupBy);
		$command->order('created');


        //views
		if (self::$timeline == self::TIMEFRAME_YEARLY) {
			$sql = '
			SELECT  a.idContent,a.idUser,DATE_FORMAT(a.viewed, "%Y-%m") as date
			FROM    app7020_content_history a
				   INNER JOIN
				   (
					   SELECT idUser, idContent,  MIN(viewed) min_val
					   FROM    app7020_content_history
					   GROUP   BY idContent, idUser
				   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
						   a.viewed = b.min_val

				   LEFT JOIN app7020_content c on  c.id=a.idContent

						   WHERE a.viewed BETWEEN (NOW() - INTERVAL '.self::$timeline.' DAY) AND NOW() AND c.userId='.self::$idUser.'
						   ORDER BY date DESC
			';
		} else {
			$sql = '
			SELECT  a.idContent,a.idUser,DATE_FORMAT(a.viewed, "%c-%d") as date
			FROM    app7020_content_history a
				   INNER JOIN
				   (
					   SELECT idUser, idContent,  MIN(viewed) min_val
					   FROM    app7020_content_history
					   GROUP   BY idContent, idUser
				   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
						   a.viewed = b.min_val

				   LEFT JOIN app7020_content c on  c.id=a.idContent

						   WHERE a.viewed BETWEEN (NOW() - INTERVAL '.self::$timeline.' DAY) AND NOW() AND c.userId='.self::$idUser.'
						   ORDER BY date DESC
			';
		}

        $command2 = Yii::app()->db->createCommand($sql);
        $result = $command2->queryAll(true);

		$resultArray = array(
			'periods' => array(),
			'assets' => array(),
			'views' => array()
		);
		if (self::$timeline == self::TIMEFRAME_YEARLY) {
			$months = array();
			for ($i = 0; $i <= 12; $i++) {
				$months[] = date('Y-m', strtotime(date('Y-m') . " -$i months"));
			}
			$months = array_reverse($months);
			$count = count($command->queryAll(true));

            foreach ($months as $month) {
                //Contributed assets
				if (count($command->queryAll(true)) > 0) {

					foreach ($command->queryAll(true) as $answer) {
						if ($answer['period'] == $month) {
							$resultArray["periods"][] = $answer['period'];
							$resultArray["assets"][] = $answer['assets'];
                            $resultArray["views"][] = 0;
							$count--;
						} else {
							if (!in_array($month, $resultArray['periods']) && !in_array($answer['period'], $resultArray['periods']) && $count > 0) {
								$resultArray["periods"][] = $month;
								$resultArray["assets"][] = 0;
                                $resultArray["views"][] = 0;
							} else if (!in_array($month, $resultArray['periods']) && $count == 0) {
								$resultArray["periods"][] = $month;
								$resultArray["assets"][] = 0;
                                $resultArray["views"][] = 0;
							}
						}
					}
				} else {
					$resultArray["periods"][] = $month;
					$resultArray["assets"][] = 0;
                    $resultArray["views"][] = 0;
                }

			}
		} else {
			$days = array();
			for ($i = 0; $i <= self::$timeline; $i++) {
				$days[] = date('n-d', strtotime("-$i day"));
			}
			$days = array_reverse($days);
			$count = count($command->queryAll(true));
			foreach ($days as $day) {
				if (count($command->queryAll(true)) > 0) {
					foreach ($command->queryAll(true) as $answer) {
						if ($answer['period'] == "$day") {
							$resultArray["periods"][] = $answer['period'];
							$resultArray["assets"][] = $answer['assets'];
                            $resultArray["views"][] = 0;
							$count--;
						} else {
							if (!in_array($day, $resultArray['periods']) && !in_array($answer['period'], $resultArray['periods']) && $count > 0) {
								$resultArray["periods"][] = $day;
								$resultArray["assets"][] = 0;
                                $resultArray["views"][] = 0;
							} else if (!in_array($day, $resultArray['periods']) && $count == 0) {
								$resultArray["periods"][] = $day;
								$resultArray["assets"][] = 0;
                                $resultArray["views"][] = 0;
							}
						}
					}
				} else {
					$resultArray["periods"][] = $day;
					$resultArray["assets"][] = 0;
                    $resultArray["views"][] = 0;
                }
			}
		}
        //Asset views by other users
        if (count($result) > 0) {
            foreach ($result as $answer) {
                $key = array_search($answer['date'], $resultArray['periods']);
                if ($key) {
                    $resultArray['views'][$key] = $resultArray['views'][$key] + 1;
                }
            }
        }
        $resultArray['count'] = array_sum($resultArray['views']);
		return $resultArray;
	}

	public static function getAllSharingStatistics($timeline = null, $idUser = null, $idContent = null) {
		self::_getDefaultStatisticsValues($timeline, $idUser);
		$result = array_merge(
			array('averageRating' => self::averageRating($timeline, $currentUser)), array('averageWatchRate' => self::averageWatchRate($timeline, $currentUser)), array('chartMyActivityPerChannel' => self::chartMyActivityPerChannel($timeline, $currentUser)), array('mostSharedAsset' => self::getMostSharedAsset()), array('mostVotedAsset' => self::getMostVottedAsset()), array('mostWatchedAsset' => self::getMostWatchedAsset()), array('sharedAssetsViews' => self::sharedAssetsViews($timeline, $currentUser)), array('sharedContents' => self::sharedContents($timeline, $currentUser))
		);
		return $result;
	}

}
