<?php

/**
 * This is the model class for table "learning_catalogue_entry".
 *
 * The followings are the available columns in table 'learning_catalogue_entry':
 * @property integer $idCatalogue
 * @property integer $idEntry
 * @property string $type_of_entry
 *
 * The followings are the available model relations:
 * @property LearningCatalogue $catalog
 * @property LearningCourse $course
 * @property LearningCoursepath $coursepath
 */
class LearningCatalogueEntry extends CActiveRecord
{

    const ENTRY_COURSE          = 'course';
    const ENTRY_COURSEPATH      = 'coursepath';
    const ENTRY_CATALOGUE       = 'catalogue';
    
	public $search_input;
	public $confirm;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCatalogueEntry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_catalogue_entry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCatalogue, idEntry', 'numerical', 'integerOnly'=>true),
			array('type_of_entry', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCatalogue, idEntry, type_of_entry', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catalog' => array(self::BELONGS_TO, 'LearningCatalogue', 'idCatalogue'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'idEntry', 'on'=>'t.type_of_entry="course"'),
			'coursepath' => array(self::BELONGS_TO, 'LearningCoursepath', 'idEntry', 'on'=>'t.type_of_entry="coursepath"')
		);
	}

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCatalogue' => 'Id Catalogue',
			'idEntry' => 'Id Entry',
			'type_of_entry' => 'Type Of Entry',
		);
	}

	public function dataProvider()	{

		$params = array();

		$command = Yii::app()->getDb()->createCommand()
			->from(self::model()->tableName().' t');

		if($this->idCatalogue){
			$command->andWhere('t.idCatalogue=:idCatalogue');
			$params[':idCatalogue'] = $this->idCatalogue;
		}

		if($this->idEntry){
			$command->andWhere('t.idEntry=:idEntry');
			$params[':idEntry'] = $this->idEntry;
		}

		if($this->type_of_entry){
			$command->andWhere('t.type_of_entry=:type_of_entry');
			$params[':type_of_entry'] = $this->type_of_entry;
		}

		$command->leftJoin(LearningCourse::model()->tableName().' c', 'c.idCourse=t.idEntry AND t.type_of_entry="course"');

		if(PluginManager::isPluginActive('CurriculaApp')){
			// Only join entries that are valid COURSES or LEARNING PLANS
			$command->leftJoin(LearningCoursepath::model()->tableName().' cp', 't.idEntry=cp.id_path AND t.type_of_entry="coursepath"');
			$command->andWhere('c.idCourse IS NOT NULL OR cp.id_path IS NOT NULL');
			$command->select('t.*, c.name courseName, cp.path_name cpName, c.course_type course_type');
			if($this->search_input) {
				$command->andWhere('c.name LIKE :search_input OR cp.path_name LIKE :search_input');
				$params[':search_input'] = "%$this->search_input%";
			}
		}else{
			// Only join entries that are valued courses
			$command->andWhere('c.idCourse IS NOT NULL');
			$command->select('t.*, c.name courseName, c.course_type course_type');
			if($this->search_input) {
				$command->andWhere('c.name LIKE :search_input');
				$params[':search_input'] = "%$this->search_input%";
			}
		}

		if(Yii::app()->user->getIsPu())
		{
			$command->leftJoin(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = "  . (int)Yii::app()->user->id .   " AND puc.course_id = t.idEntry AND t.type_of_entry='course'");
			if(PluginManager::isPluginActive('CurriculaApp'))
			{
				$command->leftJoin(CoreUserPuCoursepath::model()->tableName()." pucp", "pucp.puser_id = "  . (int)Yii::app()->user->id .   " AND pucp.path_id = t.idEntry AND t.type_of_entry='coursepath'");
				$command->andWhere('puc.course_id IS NOT NULL OR pucp.path_id IS NOT NULL');
			}
			else
				$command->andWhere('puc.course_id IS NOT NULL');
		}

		if (!PluginManager::isPluginActive('ClassroomApp')) {
			// Exclude CLASSROOM courses if the plugin is not active
			$command->andWhere('c.course_type IS NULL OR c.course_type <> :type_classroom');
			$params[':type_classroom'] = LearningCourse::TYPE_CLASSROOM;
		}

		$config = array();
		$criteria = new CDbCriteria;
		$criteria->compare('t.idCatalogue', $this->idCatalogue);
		$criteria->compare('t.idEntry', $this->idEntry);
		$criteria->compare('t.type_of_entry', $this->type_of_entry);

		$criteria->with['course'] = array(
			'joinType' => 'INNER JOIN'
		);

		//check if we are allowed to see classroom courses. If not, put a filter on course_type, allowing elearning courses only
		if (!PluginManager::isPluginActive('ClassroomApp')) {
			$criteria->with = array('course' => array(
				'joinType' => 'INNER JOIN',
				'condition' => 'course.course_type <> :course_type',
			));
			$criteria->params[':course_type'] = LearningCourse::TYPE_CLASSROOM;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['totalItemCount'] = count($command->queryAll(false,$params));
		$config['params'] = $params;
		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CSqlDataProvider($command->getText(), $config);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCatalogue',$this->idCatalogue);
		$criteria->compare('idEntry',$this->idEntry);
		$criteria->compare('type_of_entry',$this->type_of_entry,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	

	/**
	 * 
	 * {@inheritDoc}
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {
	    if ($this->isNewRecord) {
	       Yii::app()->event->raise(EventManager::EVENT_ENTRY_ASSIGNED_TO_CATALOG, new DEvent($this, array(
	           'model' => $this,
	       )));
	    }
	    parent::afterSave();
	}
	
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {
        Yii::app()->event->raise(EventManager::EVENT_ENTRY_UNASSIGNED_FROM_CATALOG, new DEvent($this, array(
            'model'   => $this->idCatalogue,
        )));
	    parent::afterDelete();
	}
	
	
}