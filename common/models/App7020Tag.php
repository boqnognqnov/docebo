<?php

/**
 * This is the model class for table "app7020_tag".
 *
 * The followings are the available columns in table 'app7020_tag':
 * @property integer $id
 * @property string $tagText
 *
 * The followings are the available model relations:
 * @property App7020TagLink[] $contentTags
 */
class App7020Tag extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tagText', 'required'),
			array('tagText', 'length', 'max' => 32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tagText', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contentTags' => array(self::HAS_MANY, 'App7020TagLink', 'idTag'),
			// Through relation (1 Tag -> many content links -> many content items
			// Array of CONTENT items (e.g. videos) related to THIS TAG
			'contentItems' => array(self::HAS_MANY, 'App7020Assets', array('idContent' => 'id'), 'through' => 'contentTags'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'tagText' => 'Tag Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('tagText', $this->tagText, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020Tag the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Returns all tags
	 * @return object
	 */
	public static function getAllTags() {
		$tags = new App7020Tag();
		return $tags->findAll();
	}

	/**
	 * Add new tag with check for existing 
	 * @return boolean
	 */
	public static function addNewTags($tagName) {
		try {
			$model = new self;
			$newCriteria = new CDbCriteria;
			$newCriteria->condition = 'tagText =:tagText';
			$newCriteria->params = array('tagText' => $tagName);
			if (!$model->exists($newCriteria)) {
				$model->setAttribute('tagText', $tagName);
				return $model->save();
			}
		} catch (Exception $ex) {
			return false;
		}
	}

	/**
	 * Get all results for tags maching to function argumentapp700
	 * @param type $tag
	 * @return boolean|array
	 */
	public static function getMachingTagsTo($tag) {
		try {
			$tags = Yii::app()->db->createCommand()
				->from('app7020_tag')
				->where('tagText LIKE :match', array(
					':match' => "%$tag%"
				))
				->queryAll();
			return $tags;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

}
