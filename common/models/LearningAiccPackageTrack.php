<?php
/**
 * 
 * This table holds tracking records for a given AICC package as a whole, for given user. PER ITEM tracking is in the _aicc_item_track table
 * 
 * This is the model class for table "learning_aicc_package_track".
 *
 * The followings are the available columns in table 'learning_aicc_package_track':
 * @property integer $id
 * @property integer $id_package
 * @property integer $idUser
 * @property string $status
 * @property integer $nChildCompleted
 * @property integer $nDescendantCompleted
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property LearningAiccPackage $package
 * 
 */
class LearningAiccPackageTrack extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_aicc_package_track';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_package, idUser', 'required'),
			array('id_package, idUser, nChildCompleted, nDescendantCompleted', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>16),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_package, idUser, status, nChildCompleted, nDescendantCompleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// User involved	
			'user' 			=> array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			// Package involved	
			'package' 		=> array(self::BELONGS_TO, 'LearningAiccPackage', 'id_package'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_package' => 'Id Package',
			'idUser' => 'Id User',
			'status' => 'Status',
			'nChildCompleted' => 'N Child Completed',
			'nDescendantCompleted' => 'N Descendant Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_package',$this->id_package);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('nChildCompleted',$this->nChildCompleted);
		$criteria->compare('nDescendantCompleted',$this->nDescendantCompleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningAiccPackageTrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	

	/**
	 * Get a package tracking record, create new one if not existent 
	 * 
	 * @param unknown $idUser
	 * @param unknown $idPackage
	 * 
	 * @return LearningAiccPackageTrack
	 */
	public static function getTrack($idUser, $idPackage) {
		
		$attributes = array(
			'idUser'		=> (int) $idUser,
			'id_package'	=> (int) $idPackage,
		);
		
		$model = self::model()->findByAttributes($attributes);
		if (!$model) {
			$model 					= new self();
			$model->status			= LearningAiccItemTrack::ENTRY_AB_INITIO;
			$model->idUser 			= $idUser;
			$model->id_package 		= $idPackage;
			$model->save();
		}
		
		return $model;
		
	}
	

	/**
	 * Pre-create tracking records for this package, for all descendant items of the master item
	 * 
	 * @param unknown $idUser
	 */
	public function preCreateItemTrackingRecords($idUser) {
		$masterItem = $this->package->masterItem;
		$descendants = $masterItem->getDescendants();
		foreach ($descendants as $item) {
			// This will just get a model if it exists, otherwise, it will create it
			LearningAiccItemTrack::getTrack($idUser, $item->id);
		}
	} 
	
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {
		$this->preCreateItemTrackingRecords($this->idUser);
		return parent::afterSave();
	}
	
	
	
}
