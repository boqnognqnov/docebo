<?php

/**
 * This is the model class for table "learning_certificate_assign".
 *
 * The followings are the available columns in table 'learning_certificate_assign':
 * @property integer $id_certificate
 * @property integer $id_course
 * @property integer $id_user
 * @property string $on_date
 * @property string $cert_file
 * @property integer $cert_number
 */
class LearningCertificateAssign extends CActiveRecord
{

	public $search_input;

	const CERTIFICATE_PATH = '../files/doceboLms/certificate/';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCertificateAssign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_certificate_assign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_certificate, id_course, id_user, cert_number', 'numerical', 'integerOnly'=>true),
			array('cert_file', 'length', 'max'=>255),
			array('on_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_certificate, id_course, id_user, on_date, cert_file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'learningCourse' => array(self::BELONGS_TO, 'LearningCourse', 'id_course'),
			'learningCourseuser' => array(self::BELONGS_TO, 'LearningCourseuser', array('id_user' => 'idUser', 'id_course' => 'idCourse')),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('on_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_certificate' => 'Id Certificate',
			'id_course' => 'Id Course',
			'id_user' => 'Id User',
			'on_date' => 'On Date',
			'cert_file' => 'Cert File',
			'cert_number' => 'Certificate number'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_certificate',$this->id_certificate);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('on_date',Yii::app()->localtime->fromLocalDateTime($this->on_date),true);
		$criteria->compare('cert_file',$this->cert_file,true);
		$criteria->compare('cert_number',$this->cert_number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function dataProvider() {
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->compare('user.userid', $this->user->userid, true);
		$criteria->compare('t.id_course', $this->id_course);

		$criteria->with = array('user', 'learningCourseuser');

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		// pr($this->findAll($criteria));
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));
		return new CActiveDataProvider(get_class($this), $config);
	}


	/**
	 * Load model by course and user
	 * @param $userId
	 * @param $courseId
	 * @return CActiveRecord
	 */
	public static function loadByUserCourse($userId, $courseId)
	{
		$certificate = null;
		// The course has a certificate assigned to it?
		$certificateAssignedToCourse = LearningCertificateCourse::model()->findByAttributes(array('id_course'=>$courseId));

		//if the course certificate is generated on another level (branch for instance)
		$event = new DEvent(new self(), array(
			'id_user' => $userId,
			'id_course' => $courseId)
		);
		Yii::app()->event->raise('FindStudentCourseCertificate', $event);

		if($certificateAssignedToCourse) {
			$certificate = $certificateAssignedToCourse;
		} else if($event->return_value['certificate']) {
			$certificate = $event->return_value['certificate'];
		}

		// Is the certificate generated?
		$generated = self::model()->findByAttributes(array(
			'id_user' => $userId,
			'id_course' => $courseId,
			'id_certificate' => ($certificate) ? $certificate->id_certificate : 0
		));

		if(!$generated){

			// No certificate generated for this user/course combination yet
			// See if there is a need and generate the certificate below

			if(!$certificate)
				return FALSE; // no certificate assigned to course, nothing to generate

			$userCompletedCourse = LearningCourseuser::model()->countByAttributes(array(
				'idUser' => $userId,
				'idCourse' => $courseId,
				'status' => LearningCourseuser::$COURSE_USER_END,
			));

			if($userCompletedCourse){

				// !!! Generate the certificate (PDF + DB row) !!!
				$course = LearningCourse::model()->findByAttributes(array('idCourse'=>$courseId));
				$user = CoreUser::model()->findByPk($userId);

				$event = new DEvent(Yii::app()->getController(), array('course'=>$course, 'user'=>$user));
				Yii::app()->event->raise('GenerateCertificate', $event);

				if(!$event->handled) {
					try {
						$substituton = Yii::app()->certificateTag->getSubstitution($userId, $courseId);
						Yii::app()->certificate->create($certificate->id_certificate, $userId, $courseId, $substituton, FALSE);
					} catch (Exception $e) {
						Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
					}
				}

				// The certificate should be existing now, since we just created it
				$generated = self::model()->findByAttributes(array(
					'id_user' => $userId,
					'id_course' => $courseId,
					'id_certificate' => $certificate->id_certificate
				));
			}

		}

		return $generated;

	}

	public function deleteCertFile() {
		if ($this->cert_file) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
			$storage->remove($this->cert_file);
		}
	}

	public function beforeDelete()
	{
		$this->deleteCertFile();
		return parent::beforeDelete();
	}


	/**
	 * Download certificate file if exists for the current model
	 */
	public function downloadCertificate() {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
		$storage->sendFile($this->cert_file, $this->cert_file);
	}


}