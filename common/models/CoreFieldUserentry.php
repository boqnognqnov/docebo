<?php

/**
 * This is the model class for table "core_field_userentry".
 *
 * The followings are the available columns in table 'core_field_userentry':
 * @property string  $id_common
 * @property integer $id_common_son
 * @property integer $id_user
 * @property string  $user_entry
 * @property string  $user_entry_extra
 */
class CoreFieldUserentry extends CActiveRecord {

	/**
	 * Just set this file to a LOCAL file and it will trigger saving it to Core Field Collection storage.
	 * You can set it also to an instance of CUploadedFile; the model will detect it and will act accordingly to save the uploaded file.
	 *
	 * @var string
	 */
	public $sourceFile = FALSE;


	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return CoreFieldUserentry the static model class
	 */
	public static function model ( $className = __CLASS__ ) {
		return parent::model( $className );
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName () {
		return 'core_field_userentry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules () {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('user_entry', 'required'),
			array( 'id_common, id_common_son, id_user', 'numerical', 'integerOnly' => TRUE ),
			array( 'id_common', 'length', 'max' => 11 ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array( 'id_common, id_common_son, id_user, user_entry, user_entry_extra', 'safe', 'on' => 'search' ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations () {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'core_field' => array(
				self::BELONGS_TO,
				'CoreField',
				array( 'id_common' => 'id_common' ),
				'joinType' => 'LEFT JOIN'
			),
			'core_user'  => array( self::HAS_ONE, 'CoreUser', '', 'on' => ' id_user=core_user.idst ' ),
			'core_user2' => array( self::HAS_ONE, 'CoreUser', array( 'idst' => 'id_user' ) )
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels () {
		return array(
			'id_common'        => 'Id Common',
			'id_common_son'    => 'Id Common Son',
			'id_user'          => 'Id User',
			'user_entry'       => 'User Entry',
			'user_entry_extra' => 'User Entry - extra info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search () {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare( 'id_common', $this->id_common, TRUE );
		$criteria->compare( 'id_common_son', $this->id_common_son );
		$criteria->compare( 'id_user', $this->id_user );
		$criteria->compare( 'user_entry', $this->user_entry, TRUE );
		$criteria->compare( 'user_entry_extra', $this->user_entry_extra, TRUE );

		return new CActiveDataProvider( $this, array(
			'criteria'   => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get( 'elements_per_page' )
			)
		) );
	}

	public function renderReportField ( $user_entry ) {
		$content = $this->core_field->renderValue( $user_entry, $this );

		return $content;
	}


	/**
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete () {

		// Uploaded files clean up, if any
		if( $this->core_field ) {
			if( $this->core_field->type_field == CoreField::TYPE_FIELD_UPLOAD ) {
				$filename = $this->user_entry;
				if( ! empty( $filename ) ) {
					$storage = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_CORE_FIELD );
					$storage->remove( $filename );
				}
			}
		}


	}


	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave () {
		if( isset( $this->sourceFile ) && is_string( $this->sourceFile ) ) {
			$this->saveFileUserEntry( $this->sourceFile );
		} else if( $this->core_field->type_field == CoreField::TYPE_FIELD_DATE ) {
			$width = Yii::app()->localtime->detectLocalDateWidth( $this->user_entry );
			if( ! $width ) {
				$width = NULL;
			}
			$this->user_entry = Yii::app()->localtime->fromLocalDate( $this->user_entry, $width );
		}

		return parent::beforeSave();
	}


	/**
	 * Save passed local file to core field collection storage and save referrence info in the model.
	 *
	 * @param string $filePath
	 */
	protected function saveFileUserEntry ( $filePath ) {
		if( ! is_file( $filePath ) ) {
			return;
		}
		$pathInfo    = pathinfo( $filePath );
		$newFileName = Docebo::randomHash() . '.' . $pathInfo['extension'];
		$storage     = CFileStorage::getDomainStorage( CFileStorage::COLLECTION_CORE_FIELD );
		if( $storage->storeAs( $filePath, $newFileName ) ) {
			$this->user_entry       = $newFileName;
			$this->user_entry_extra = basename( $filePath );
		} else {
			Yii::log( 'Unable to save local file: ' . $filePath . ' to storage: ' . get_class( $storage ) . '. Remote file name to save: ' . $newFileName, CLogger::LEVEL_ERROR );
		}
	}


	public function getLocalizedValue ( $lang = NULL ) {
		if( ! $lang ) {
			$lang = Lang::getCodeByBrowserCode( Yii::app()->getLanguage() );
		}

		$fieldModel = $this->core_field;

		$value = $this->user_entry; // fallback

		switch ( $fieldModel->type_field ) {
			case CoreField::TYPE_FIELD_DROPDOWN:
				$value = Yii::app()->getDb()->createCommand()
							->select( 's.translation' )
							->from( CoreFieldSon::model()->tableName() . ' s' )
							->where( 's.id_common_son=:idSon AND s.idField=:idCommon AND s.lang_code=:lang', array(
								':lang'     => $lang,
								':idCommon' => $fieldModel->id_common,
								':idSon'    => $this->user_entry
							) )
							->queryScalar();
				break;
			case CoreField::TYPE_FIELD_COUNTRY:
				$value = Yii::app()->getDb()->createCommand()
							->select( 'name_country' )
							->from( CoreCountry::model()->tableName() )
							->where( 'id_country=:ue', array(
								':ue' => $this->user_entry
							) )
							->queryScalar();
				break;
			case CoreField::TYPE_FIELD_YESNO:
				$value = FieldYesno::getLabelFromValue($this->user_entry);
				break;
			default:
				$value = CoreField::renderFieldValueByUserId( $this->id_common, $this->id_user );
		}

		return $value;

	}
}