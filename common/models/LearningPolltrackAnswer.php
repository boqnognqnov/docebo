<?php

/**
 * This is the model class for table "learning_polltrack_answer".
 *
 * The followings are the available columns in table 'learning_polltrack_answer':
 * @property integer $id_track
 * @property integer $id_quest
 * @property integer $id_answer
 * @property string $more_info
 */
class LearningPolltrackAnswer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningPolltrackAnswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_polltrack_answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('more_info', 'required'),
			array('id_track, id_quest, id_answer', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_track, id_quest, id_answer, more_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_track' => 'Id Track',
			'id_quest' => 'Id Quest',
			'id_answer' => 'Id Answer',
			'more_info' => 'More Info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_track',$this->id_track);
		$criteria->compare('id_quest',$this->id_quest);
		$criteria->compare('id_answer',$this->id_answer);
		$criteria->compare('more_info',$this->more_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}