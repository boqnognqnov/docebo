<?php

/**
 * This is the model class for table "core_https".
 *
 * The followings are the available columns in table 'core_https':
 * @property integer $id
 * @property string $domain
 * @property integer $mode
 * @property string $certFile
 * @property string $keyFile
 * @property string $keyFileOrigin
 * @property string $intermCaFile
 * @property string $csrFile
 * @property string $sslStatus
 * @property string $sslInstallTime
 * @property string $sslExpireTime
 * @property integer $isWildCard
 * @property string $sslStatusLastError
 * @property string $csrGenerationStatus
 * @property string $csrGenerationTime
 * @property string $csrGenerationLastMessage
 * @property string $csrCountryName
 * @property string $csrOrgUnitName
 * @property string $csrStateOrProvinceName
 * @property string $csrCommonName
 * @property string $csrLocalityName
 * @property string $csrEmail
 * @property string $csrOrgName
 * @property integer $idMultidomainClient
 * @property string $httpsAgentToken
 *
 * The followings are the available model relations:
 * @property CoreMultidomain $multidomainClient
 */
class CoreHttps extends CActiveRecord
{

	const MODE_OFF 						= 0;    // No https redirection
	const MODE_ON_DOCEBO_WILDCARD_SSL 	= 1;	// Redirection is ON, Docebo installed wildard SSL Certifcate is used
	const MODE_ON_CUSTOM_SSL			= 2;	// Redirection is ON, Customer uploads its own Custom Domain related SSL cert.

	const SSL_STATUS_NOT_INSTALLED			= 'notinstalled';
	const SSL_STATUS_PENDING_INSTALLATION	= 'pending_installation';
	const SSL_STATUS_INSTALLED				= 'installed';
	const SSL_STATUS_PARTS_REMOVED			= 'parts_removed';
	const SSL_STATUS_ERROR					= 'error';


	const STATUS_CANCELLED					= 'cancelled';


	const CSRGEN_STATUS_PENDING_GENERATION  = 'pending_generation';
	const CSRGEN_STATUS_READY  				= 'ready';
	const CSRGEN_STATUS_ERROR  				= 'error';

	/**
	 * See main-devel/main-live for these keys in Yii App params[] array
	 * @var string
	 */
	const FILESTORAGE_CONFIG		= 'ssl_storage';
	const FILESTORAGE_CONFIG_TMP	= 'ssl_storage_tmp';

	const CUSTOMCERT_OWN_UPLOAD		= 1;	// Customer owns an SSL certificate and is uploading it
	const CUSTOMCERT_NEEDS_CSR		= 2;	// Customer doesn't have an SSL certificate and we generate a CSR for him to download

	const SCENARIO_UPLOAD_OWN_CERT 	= 'uploadOwnCert';   	// When user decides to upload both Certificate and Key, and optionally IntermCA
	const SCENARIO_GENERATE_CSR 	= 'generateCsr';		// When user requests a CSR generation
	const SCENARIO_UPLOAD_CERT_ONLY = 'uploadCertOnly';		// When user generated a CSR through us and we already have the KEY, but need the Certificate


	const CONTEXT_GENERAL_HTTPS			= 'advanced_settings';
	const CONTEXT_MULTIDOMAIN_HTTPS		= 'multidomain_settings';

	const KEYFILE_ORIGIN_CUSTOMER		= 'customer';
	const KEYFILE_ORIGIN_CSRGEN			= 'csrgen';


	const FILETYPE_KEY			= 'key';
	const FILETYPE_CSR			= 'csr';


	/**
	 * If customer owns a certificate to upload or neesds a CSR generation
	 *
	 * @var integer
	 */
	public $customCertificateOption = self::CUSTOMCERT_OWN_UPLOAD;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_https';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('domain', 'required'),

			array('mode, certFile, keyFile, intermCaFile, customCertificateOption, csrCountryName, csrOrgUnitName, csrStateOrProvinceName, csrCommonName, csrLocalityName, csrEmail, csrOrgName', 'safe'),

			// Certificate file is always required
			array('certFile', 'file', 'types' => 'p8,key,p10,csr,cer,crl,p7c,crt,der,pem,p12,pfx,p7b,spc,p7r',  'on' => array(self::SCENARIO_UPLOAD_OWN_CERT, self::SCENARIO_UPLOAD_CERT_ONLY)),

			// Key file is not required when Key file is system genarated as a result of CSR generation from us
			array('keyFile', 'file', 'types' => 'p8,key,p10,csr,cer,crl,p7c,crt,der,pem,p12,pfx,p7b,spc,p7r', 'on' => self::SCENARIO_UPLOAD_OWN_CERT),

			array('intermCaFile', 'file', 'types' => 'p8,key,p10,csr,cer,crl,p7c,crt,der,pem,p12,pfx,p7b,spc,p7r', 'allowEmpty' => true),

			array('csrCountryName, csrOrgUnitName, csrStateOrProvinceName, csrCommonName, csrLocalityName, csrEmail, csrOrgName', 'required', 'on' => self::SCENARIO_GENERATE_CSR),


			array('mode, isWildCard, idMultidomainClient', 'numerical', 'integerOnly'=>true),
			array('domain, certFile, keyFile, intermCaFile, csrFile, sslStatusLastError, csrCountryName, csrOrgUnitName, csrStateOrProvinceName, csrCommonName, csrLocalityName, csrEmail, csrOrgName', 'length', 'max'=>255),
			array('keyFileOrigin, sslStatus, csrGenerationStatus', 'length', 'max'=>32),
			array('csrGenerationLastMessage', 'length', 'max'=>1024),
			array('csrEmail', 'email'),
			array('httpsAgentToken', 'length', 'max'=>64),
			array('sslInstallTime, sslExpireTime, csrGenerationTime', 'safe'),

			array('id, domain, mode, certFile, keyFile, keyFileOrigin, intermCaFile, csrFile, sslStatus,  sslInstallTime, sslExpireTime, isWildCard, sslStatusLastError, csrGenerationStatus, csrGenerationTime, csrGenerationLastMessage, csrCountryName, csrOrgUnitName, csrStateOrProvinceName, csrCommonName, csrLocalityName, csrEmail, csrOrgName, idMultidomainClient, httpsAgentToken', 'safe', 'on'=>'search'),



		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'multidomainClient' => array(self::BELONGS_TO, 'CoreMultidomain', 'idMultidomainClient'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 							=> 'ID',
			'domain'						=> 'Domain',
			'mode'							=> Yii::t('configuration', 'Activate Https'),
			'certFile'						=> Yii::t('player', 'Certificate'),
			'keyFile'						=> Yii::t('configuration', 'Key'),
			'keyFileOrigin' 				=> 'Key File Origin',
			'intermCaFile'					=> Yii::t('configuration', 'Intermediate CA'),
			'csrCountryName'				=> Yii::t('configuration', 'Country name'),
			'csrOrgUnitName'				=> Yii::t('configuration', 'Organizational unit name'),
			'csrStateOrProvinceName'		=> Yii::t('configuration', 'State or province name (full name)'),
			'csrCommonName'					=> Yii::t('configuration', 'Common name'),
			'csrLocalityName'				=> Yii::t('configuration', 'Locality name'),
			'csrEmail'						=> Yii::t('standard', '_EMAIL'),
			'csrOrgName'					=> Yii::t('configuration', 'Organization name'),
			'csrFile' 						=> Yii::t('configuration', 'Certificate Signing Request'),
			'sslStatus' 					=> Yii::t('configuration', 'SSL Installation status'),
			'sslInstallTime' 				=> Yii::t('configuration', 'Certificate installation time'),
			'sslExpireTime' 				=> Yii::t('configuration', 'Certificate expire time'),
			'isWildCard' 					=> Yii::t('configuration', 'Is the certificate wildcard'),
			'sslStatusLastError'			=> 'SSL Installation last error message',
			'csrGenerationStatus' 			=> Yii::t('configuration', 'CSR Generation Status'),
			'csrGenerationTime'				=> Yii::t('configuration', 'CSR Generation time'),
			'csrGenerationLastMessage' 		=> 'Csr Generation Last Message',
			'idMultidomainClient' 			=> 'Multidomain Client Id',
			'httpsAgentToken'				=> 'Https Agent Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->id);
		$criteria->compare('mode',$this->mode);
		$criteria->compare('certFile',$this->certFile,true);
		$criteria->compare('keyFile',$this->keyFile,true);
		$criteria->compare('keyFileOrigin',$this->keyFileOrigin,true);
		$criteria->compare('intermCaFile',$this->intermCaFile,true);
		$criteria->compare('csrFile',$this->csrFile,true);
		$criteria->compare('sslStatus',$this->sslStatus,true);
		$criteria->compare('sslInstallTime',$this->sslInstallTime,true);
		$criteria->compare('sslExpireTime',$this->sslExpireTime,true);
		$criteria->compare('isWildCard',$this->isWildCard);
		$criteria->compare('sslStatusLastError',$this->sslStatusLastError,true);
		$criteria->compare('csrGenerationStatus',$this->csrGenerationStatus, true);
		$criteria->compare('csrGenerationTime',$this->csrGenerationTime,true);
		$criteria->compare('csrGenerationLastMessage',$this->csrGenerationLastMessage,true);
		$criteria->compare('csrCountryName',$this->csrCountryName,true);
		$criteria->compare('csrOrgUnitName',$this->csrOrgUnitName,true);
		$criteria->compare('csrStateOrProvinceName',$this->csrStateOrProvinceName,true);
		$criteria->compare('csrCommonName',$this->csrCommonName,true);
		$criteria->compare('csrLocalityName',$this->csrLocalityName,true);
		$criteria->compare('csrEmail',$this->csrEmail,true);
		$criteria->compare('csrOrgName',$this->csrOrgName,true);
		$criteria->compare('idMultidomainClient',$this->idMultidomainClient);
		$criteria->compare('httpsAgentToken',$this->httpsAgentToken,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreHttps the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 *
	 * @param string $createIfNotExists
	 * @return CoreHttps
	 */
	public static function getGeneralHttps($createIfNotExists = true) {

		$model = self::model()->findByAttributes(array('idMultidomainClient' => null));
		if (!$model && $createIfNotExists) {
			$model = new self();
			$model->domain = trim(Docebo::getCurrentDomain(true, true), '/');
			$model->idMultidomainClient = null;
			$model->httpsAgentToken = Docebo::randomHash();
			$model->mode = self::MODE_ON_DOCEBO_WILDCARD_SSL;
			$model->isWildCard = 1;
			$model->save();
		}
		else if ($model) {
			if ($model->sslStatus == self::STATUS_CANCELLED)
				$model->sslStatus = null;

			if ($model->csrGenerationStatus == self::STATUS_CANCELLED)
				$model->csrGenerationStatus = null;

			$model->save();
		}


		// Set some default values if not set
		$model->setDefaultValues();

		return $model;

	}


	/**
	 *
	 * @param unknown $id
	 * @param string $createIfNotExists
	 * @return Ambigous <CoreHttps, static>
	 */
	public static function getMultidomainClientHttps($idClient, $createIfNotExists = false) {

		/* @var $clientModel CoreMultidomain */

		$clientModel = CoreMultidomain::model()->findByPk($idClient);
		$model = self::model()->findByAttributes(array('idMultidomainClient' => $idClient));

		if (!$model && $createIfNotExists) {
			$model = new self();
			if ($clientModel && $clientModel->domain_type != CoreMultidomain::DOMAINTYPE_SUBFOLDER) {
				$model->domain = $clientModel->getClientUri(true);
				$model->idMultidomainClient = $idClient;
				$model->httpsAgentToken = Docebo::randomHash();
				$model->save();
			}
		}
		else if ($model) {
			$model->domain = $clientModel->getClientUri(true);

			if ($model->sslStatus == self::STATUS_CANCELLED)
				$model->sslStatus = null;

			if ($model->csrGenerationStatus == self::STATUS_CANCELLED)
				$model->csrGenerationStatus = null;

			$model->save();
		}


		// Set some default values if not set
		$model->setDefaultValues();


		return $model;
	}


	/**
	 *
	 * @param array $data Should be the current $_REQUEST
	 *
	 * @return boolean
	 */
	public function processSettingsRequest($request, $context) {

		if (!empty($request[__CLASS__])) {

			$preserveKeyFileName = $this->keyFile;
			$this->attributes = $request[__CLASS__];

			// Detect scenario
			$this->detectScenario($request, $context);

			if ($this->scenario == self::SCENARIO_UPLOAD_CERT_ONLY) {
				$this->keyFile = $preserveKeyFileName;
			}

			if ($this->validate()) {
				if($context == CoreHttps::CONTEXT_GENERAL_HTTPS) {
					// Update the domain
					switch ($this->mode) {
						case self::MODE_ON_DOCEBO_WILDCARD_SSL:
							$this->domain = trim(Docebo::getOriginalDomain());
							break;
						case self::MODE_ON_CUSTOM_SSL:
							$this->domain = trim(Docebo::getCurrentDomain(true), '/');
							break;

						default: break;
					}
				}

				// Depending on scenario, follow different workflow path
				switch ($this->scenario) {

					// User is uploading his own Certificate, Key and optionally an Intermediate CA files
					case self::SCENARIO_UPLOAD_OWN_CERT:
					case self::SCENARIO_UPLOAD_CERT_ONLY:
						if ($this->processUploadOwnCertFiles($request, $context)) {

							// Make an API call to request Certificate installation
							$data = array(
								'lms'				=> Docebo::getOriginalDomain(),
								'cert_filename'		=> $this->certFile,
								'key_filename'		=> $this->keyFile,
								'callback_url'		=> HttpsApiClient::generateCallbackUrl($this,  HttpsApiClient::API_ACTION_CHECK_CERT),
							);

							$resultArray = Yii::app()->httpsapi->checkCertificate($data);

							if (!$resultArray['success']) {
								Yii::app()->user->setFlash('error', Yii::t("standard", "Error calling API: " . $resultArray['status'] . ' ' . $resultArray['error']));
								return false;
							}
							if (!$this->save()) {
								Yii::log('Error saving a model: ' . print_r($this->errors, true), CLogger::LEVEL_ERROR);
							}
							return true;
						}
						break;

					// User is sending data and requesting a CSR generation
					case self::SCENARIO_GENERATE_CSR:
						if (isset($request['generate_new_csr'])) {
							$this->csrGenerationStatus = null;
							$this->keyFile = null;
							$this->keyFileOrigin = null;
							$this->save(false);
						}
						else if ($this->processRequestCsrGeneration($request, $context)) {

							// Make an API call to request New CSR generation
							$args = array(
								'C'					=> $this->csrCountryName,
								'OU'				=> $this->csrOrgUnitName,
								'ST'				=> $this->csrStateOrProvinceName,
								'CN'				=> $this->csrCommonName,
								'L'					=> $this->csrLocalityName,
								'O'					=> $this->csrOrgName,
							);

							$data = array(
								'lms'			=> Docebo::getOriginalDomain(),
								'args'			=> json_encode($args),
								'callback_url'	=> HttpsApiClient::generateCallbackUrl($this,  HttpsApiClient::API_ACTION_GENCSR),
							);

							$resultArray = Yii::app()->httpsapi->requestCsr($data);
							if (!$resultArray['success']) {
								Yii::app()->user->setFlash('error_csr', Yii::t("standard", "Error calling API: " . $resultArray['status'] . ' ' . $resultArray['error']));
								return false;
							}
							$this->save();
							return true;
						}

						break;

					default:
						$this->save();
						break;

				}

			}
		}

		return false;
	}


	/**
	 *
	 * @param unknown $request
	 * @return string
	 */
	public function detectScenario($request, $context) {

		$scenario = '';

		// If another sibling domain (or parent one) has wildcard certificate already installed, don't bother uploading new certs
		if (self::sibling2ndLvlDomainIsWildcarded($this->domain)) {
		    $scenario = '';
		}
		else if 		(	$this->customCertificateOption == self::CUSTOMCERT_OWN_UPLOAD
					&& $this->mode == self::MODE_ON_CUSTOM_SSL
					&& ($this->sslStatus == self::SSL_STATUS_ERROR || $this->sslStatus == self::SSL_STATUS_NOT_INSTALLED || !$this->sslStatus)
					&& ($this->keyFileOrigin == self::KEYFILE_ORIGIN_CSRGEN)) {  // (!!!!! Key file is already set and is System Generated)
			$scenario = self::SCENARIO_UPLOAD_CERT_ONLY;
		}
		else if (	$this->customCertificateOption == self::CUSTOMCERT_OWN_UPLOAD
					&& $this->mode == self::MODE_ON_CUSTOM_SSL
					&& ($this->sslStatus == self::SSL_STATUS_ERROR || $this->sslStatus == self::SSL_STATUS_NOT_INSTALLED || !$this->sslStatus)) {
			$scenario = self::SCENARIO_UPLOAD_OWN_CERT;
		}
		else if (	$this->customCertificateOption == self::CUSTOMCERT_NEEDS_CSR
					&& $this->mode == self::MODE_ON_CUSTOM_SSL
					&& ($this->sslStatus == self::SSL_STATUS_ERROR || $this->sslStatus == self::SSL_STATUS_NOT_INSTALLED || !$this->sslStatus)) {
			$scenario = self::SCENARIO_GENERATE_CSR;
		}

		$this->scenario = $scenario;



	}



	/**
	 * Process incoming data (request) and upload ALL certificate related files to file storage
	 *
	 * @param array $request HTTP Request data
	 * @return boolean
	 */
	private function processUploadOwnCertFiles($request, $context) {

		// Get Certificate, Key & Intermediate CA files and save them to docebo-ssl storage, in /tmp folder
		$fileStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_NONE, self::FILESTORAGE_CONFIG_TMP);

		$fileNames = array();

		$fileCert = CUploadedFile::getInstance($this, "certFile");
		$hasFileCert = $fileCert instanceof CUploadedFile;
		if ($hasFileCert) {
			$fileNames[] = $fileCert->name;
		}

		$fileKey = CUploadedFile::getInstance($this, "keyFile");
		$hasFileKey = $fileKey instanceof CUploadedFile;
		if ($hasFileKey) {
			$fileNames[] = $fileKey->name;
		}

		$fileInternCa = CUploadedFile::getInstance($this, "intermCaFile");
		$hasInternCa = $fileInternCa instanceof CUploadedFile;
		if ($hasInternCa) {
			$fileNames[] = $fileInternCa->name;
		}

		// Check for duplicated file names
		$count1 = count($fileNames);
		$count2 = count(array_unique($fileNames));
		if ($count1 != $count2) {
			Yii::app()->user->setFlash('error', Yii::t("standard", "Duplicated file names"));
			return false;
		}

		if ($hasFileCert)
			$fileStorage->storeAs($fileCert->tempName, $fileCert->name);

		if ($hasFileKey)
			$fileStorage->storeAs($fileKey->tempName, $fileKey->name);

		if ($hasInternCa)
			$fileStorage->storeAs($fileInternCa->tempName, $fileInternCa->name);

		$this->certFile = $fileCert->name;

		if ($this->scenario != self::SCENARIO_UPLOAD_CERT_ONLY) {
			$this->keyFile = $fileKey->name;
			$this->keyFileOrigin = self::KEYFILE_ORIGIN_CUSTOMER;
		}

		$this->intermCaFile = $hasInternCa ?  $fileInternCa->name : null;
		$this->sslStatus = CoreHttps::SSL_STATUS_PENDING_INSTALLATION;

		return true;


	}



	/**
	 * Process incoming data (request) and initiate CSR generation
	 *
	 * @param array $request
	 * @param string $context
	 *
	 * @return boolean
	 */
	private function processRequestCsrGeneration($request, $context) {
		$this->csrGenerationStatus = self::CSRGEN_STATUS_PENDING_GENERATION;
		return true;
	}


	/**
	 * Upon loading or creating a model, some attributes may need setting default values
	 */
	public function setDefaultValues() {

 		if (!$this->mode)
		{
 			$this->mode = self::MODE_ON_DOCEBO_WILDCARD_SSL;
			$this->isWildCard = 1;
		}
	}


	/**
	 * Check the core_https table and see if there is a domain, having the same <strong>2nd level</strong> as the input $domain.
	 * Additionally, also check if that domain is having WildCard certificate installed.
	 * Note also, domains like "something.co.uk" are considered 2nd level too!
	 *
	 * @param string $domain
	 * @param boolean $havingWildcard
	 */
	public static function sibling2ndLvlDomainExists($domain, $havingWildcard=false, $id_multidomain = false) {

	    if ($havingWildcard) {
			if($id_multidomain !== false && $id_multidomain)
				$models = self::model()->findAll("(isWildCard > 0) AND (sslStatus=:status) and idMultidomainClient <> :id", array(":status" => self::SSL_STATUS_INSTALLED, 'id' => $id_multidomain));
			else
				$models = self::model()->findAll("(isWildCard > 0) AND (sslStatus=:status)", array(":status" => self::SSL_STATUS_INSTALLED));
	    }
	    else {
	        $models = self::model()->findAll();
	    }

	    if (!$models || !is_array($models)) {
	        return false;
	    }

	    $input2ndLvlDomain = Docebo::get2ndLvlDomainName($domain);

	    foreach ($models as $model) {
	        $tmp2ndLvlDomain = Docebo::get2ndLvlDomainName($model->domain);
	        if ($input2ndLvlDomain === $tmp2ndLvlDomain) {
	            return true;
	        }
	    }

	    return false;

	}


	/**
	 * A shortcut method to check if there is a "sibling" domain having installed wild card SSL Certificate
	 *
	 * @param string $domain
	 */
	public static function sibling2ndLvlDomainIsWildcarded($domain, $id_multidomain = false) {
	    return self::sibling2ndLvlDomainExists($domain, true, $id_multidomain);
	}


}
