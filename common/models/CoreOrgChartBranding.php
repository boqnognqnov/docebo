<?php

/**
 * This is the model class for table "core_org_chart_branding".
 *
 * The followings are the available columns in table 'core_org_chart_branding':
 * @property integer $id_org
 * @property integer $id_template
 * @property string $logo
 * @property string $favicon
 * @property string $login_img
 * @property string $player_bg
 * @property string $custom_url
 */
class CoreOrgChartBranding extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_org_chart_branding';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_org', 'required'),
			array('id_org, id_template', 'numerical', 'integerOnly'=>true),
			array('logo, favicon, login_img, player_bg, custom_url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_org, id_template, logo, favicon, login_img, player_bg, custom_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_org' => 'Id Org',
			'id_template' => 'Id Template',
			'logo' => 'Logo',
			'favicon' => 'Favicon',
			'login_img' => 'Login Img',
			'player_bg' => 'Player Bg',
			'custom_url' => 'Custom URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('id_template',$this->id_template);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('favicon',$this->favicon,true);
		$criteria->compare('login_img',$this->login_img,true);
		$criteria->compare('player_bg',$this->player_bg,true);
		$criteria->compare('custom_url',$this->custom_url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreOrgChartBranding the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns the custom set logo image url
     * @return string
     */
    public function getLogoUrl() {
        $url = '';

        if (!empty($this->logo)) {
            /* @var $storageManager CFileSystemStorage */
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $url = $storageManager->fileUrl($this->logo);
        }

        return $url;
    }

    /**
     * Returns the custom set favicon url
     * @return string
     */
    public function getFaviconUrl() {
        $url = '';

        if (!empty($this->favicon)) {
            /* @var $storageManager CFileSystemStorage */
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $url = $storageManager->fileUrl($this->favicon);
        }

        return $url;
    }

    /**
     * Returns the custom set login image url
     * @return string
     */
    public function getLoginImgUrl() {
        $url = '';

        if (!empty($this->login_img)) {
            /* @var $storageManager CFileSystemStorage */
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $url = $storageManager->fileUrl($this->login_img);
        }

        return $url;
    }

    /**
     * Returns the custom set course player image url
     * @return string
     */
    public function getPlayerBgUrl() {
        $url = '';

        if (!empty($this->player_bg)) {
            /* @var $storageManager CFileSystemStorage */
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COMMON);
            $url = $storageManager->fileUrl($this->player_bg);
        }

        return $url;
    }
}
