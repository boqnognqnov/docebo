<?php

/**
 * This is the model class for table "lt_location_photo".
 *
 * The followings are the available columns in table 'lt_location_photo':
 * @property integer $id_photo
 * @property string $file_path
 * @property integer $order
 * @property string $description
 * @property integer $id_location
 *
 * The followings are the available model relations:
 * @property LtLocation $idLocation
 */
class LtLocationPhoto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lt_location_photo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('file_path, id_location', 'required'),
			array('order, id_location', 'numerical', 'integerOnly'=>true),
			array('file_path, description', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_photo, file_path, order, description, id_location', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLocation' => array(self::BELONGS_TO, 'LtLocation', 'id_location'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_photo' => 'Id Photo',
			'file_path' => 'File Path',
			'order' => 'Order',
			'description' => 'Description',
			'id_location' => 'Id Location',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_photo',$this->id_photo);
		$criteria->compare('file_path',$this->file_path,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('id_location',$this->id_location);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LtLocationPhoto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Returns the url of the photo
     * @return string
     */
    public function getUrl() {
        $url = '';

        if (!empty($this->file_path)) {
            /* @var $storageManager CFileSystemStorage */
            $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_LOCATION);
            $url = $storageManager->fileUrl($this->file_path);
        }

        return $url;
    }

    public function beforeDelete() {
        $returnValue = parent::beforeDelete();
        if (true === $returnValue) {
            if (!empty($this->file_path)) {
                /* @var $storageManager CFileSystemStorage */
                $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_LOCATION);
                $storageManager->remove($this->file_path);
            }
        }
        return $returnValue;
    }
}
