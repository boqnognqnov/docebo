<?php

/**
 * This is the model class for table "learning_course_shopify_product".
 *
 * The followings are the available columns in table 'learning_course':
 * @property integer $idCourse
 * @property string $shopifyProductId
 * @property string $shopifyProductMeaningfulId
 *
 */
class LearningCourseShopifyProduct extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_course_shopify_product';
	}

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idCourse' => 'Id Course',
			'shopifyProductId' => Yii::t('course', 'Shopify product Id'),
			'shopifyProductMeaningfulId' => Yii::t('course', 'Shopify product Meaningful Id'),
		);
	}

}
