<?php

/**
 * This is the model class for table "app7020_channel_assets".
 *
 * The followings are the available columns in table 'app7020_channel_assets':
 * @property integer $id
 * @property integer $idChannel
 * @property integer $idAsset
 * @property integer $asset_type
 *
 * The followings are the available model relations:
 * @property App7020Channels $channel
 */
class App7020ChannelAssets extends CActiveRecord {

	const ASSET_TYPE_COACH_SHARE_ASSET = 1;
	const ASSET_TYPE_COACH_SHARE_PLAYLIST = 2;
	const ASSET_TYPE_COURSES = 3;
	const ASSET_TYPE_LERNING_PLANS = 4;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_channel_assets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idChannel, idAsset', 'required'),
			array('idChannel, idAsset, asset_type', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idChannel, idAsset, asset_type', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channel' => array(self::BELONGS_TO, 'App7020Channels', 'idChannel'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idChannel' => 'Id Channel',
			'idAsset' => 'Id Asset',
			'asset_type' => 'Asset Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idChannel', $this->idChannel);
		$criteria->compare('idAsset', $this->idAsset);
		$criteria->compare('asset_type', $this->asset_type);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ChannelAssets the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public static function getAssetsByChannelId($id) {
		$sqlCommand = Yii::app()->db->createCommand();
		$sqlCommand->select('*, app7020_channel_assets.idAsset as assetId, (SELECT COUNT(id) FROM app7020_channel_assets WHERE idChannel = ' . $id . ') as allAssetsInChannel,
		(SELECT COUNT(id) FROM app7020_channel_assets WHERE idAsset = assetId) as lonelyAssets');
		$sqlCommand->from(self::model()->tableName());
		$sqlCommand->where(" idChannel = :idChannel", array(":idChannel" => $id));
		$sqlCommand->having('lonelyAssets = 1');
		
		$dataArray = $sqlCommand->queryAll();
		
		return $dataArray;
	}

	/**
	 * 
	 * @param integer $channelId
	 * @param integer $from
	 * @param integer $count
	 * @return array
	 */
	public static function getChannelAssets($channelId, $from = 0, $count = 0, $userId = null) {
		
		if(!$userId)
			$userId = Yii::app()->user->id;
		
		$sqlCommand = Yii::app()->db->createCommand();
		$sqlCommand->select("*, a.created AS create_date, "
				. "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idUser = " . $userId . " AND idContent = a.id) as watched, "
				. "(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent=a.id) AS contentRating, "
				. "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent=a.id) AS contentViews ");
		$sqlCommand->from(self::model()->tableName() . " t");
		$sqlCommand->where("idChannel = :idChannel", array(":idChannel" => $channelId));
		$sqlCommand->leftJoin(App7020Assets::model()->tableName() . " a", "(t.asset_type < 3) AND (a.id = t.idAsset)");

		if ($userId) {
			$sqlCommand->leftJoin(LearningCourseuser::model()->tableName() . " lcu", "lcu.idUser=:idUser AND t.asset_type=3 AND lcu.idCourse = t.idAsset", array(":idUser" => $userId));
			$sqlCommand->leftJoin(LearningCoursepathUser::model()->tableName() . " lcpu", "lcpu.idUser=:idUser AND t.asset_type=4 AND lcpu.id_path = t.idAsset", array(":idUser" => $userId));
			$sqlCommand->andWhere("(t.asset_type < 3 AND a.is_private = ".App7020Assets::PRIVATE_STATUS_INIT." "
					. " AND a.conversion_status = ".App7020Assets::CONVERSION_STATUS_APPROVED.") "
					. " OR (t.asset_type = 3 AND lcu.idUser IS NOT NULL) OR (t.asset_type = 4 AND lcpu.idUser IS NOT NULL)");
		}

		if ($count > 0 && $from >= 0)
			$sqlCommand->limit($count, $from);

		$sqlCommand->order("a.created DESC");

		return $sqlCommand->queryAll();
	}

	/**
	 * SELECT  `idChannel` FROM `app7020_channel_assets` WHERE `idAsset`  = 1 GROUP BY `idChannel` 
	 * @param type $asset_id
	 * @return type
	 */
	public static function getChannelKeysForAsset($asset_id, $asset_type = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET) {
		$sqlCommand = Yii::app()->db->createCommand();
		$sqlCommand->select('idChannel');
		$sqlCommand->from(self::model()->tableName() . " t");
		$sqlCommand->where("idAsset = :idAsset AND asset_type = :asset_type", array(
			":idAsset" => (int) $asset_id,
			':asset_type' => (int) $asset_type
				)
		);
		$sqlCommand->group('idChannel');

		return $sqlCommand->queryColumn();
	}

	/**
	 * 
	 * @param array $array_channel_ids
	 * @param integer $assetID
	 * @param integer $asset_type
	 */
	public static function massAppendChannelToAnAsset($array_channel_ids, $assetID, $asset_type = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET) {
		$transaction = Yii::app()->db->beginTransaction();
		$sqlCommand = Yii::app()->db->createCommand();
		try {
			foreach ($array_channel_ids as $ids) {
				$sqlCommand->insert(self::model()->tableName(), array('idChannel' => $ids, 'idAsset' => $assetID, 'asset_type' => $asset_type));
			}
			$transaction->commit();
			return true;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);		    
			$transaction->rollback();
			return false;
		}
	}

	/**
	 * Remove all requested channels from asset
	 * @param aray $array_channel_ids
	 * @param integer $assetID
	 * @param integer $asset_type
	 * @return boolean
	 */
	public static function massRemoveChannelsForAsset($array_channel_ids, $assetID, $asset_type = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET) {
		$transaction = Yii::app()->db->beginTransaction();
		$sqlCommand = Yii::app()->db->createCommand();
		if (empty($array_channel_ids)) {
			return true;
		}
		try {

			$sqlCommand->delete(
					self::model()->tableName(), 'idAsset = :idAsset and asset_type = :asset_type AND idChannel IN (' . implode(',', $array_channel_ids) . ')', array(
				'idAsset' => $assetID,
				'asset_type' => $asset_type
			));
			$transaction->commit();
			return true;
		} catch (Exception $ex) {
		    Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);		    
			$transaction->rollback();
			return false;
		}
	}

	/**
	 * Returns an array of channel Ids assigned to a specific asset
	 *
	 * @param integer $idAsset
	 * @param
	 *
	 * @return array array of channels IDs
	 */

	public static function getChannelsByContent($idAsset, $asset_type = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET) {
		$dbCommand =  Yii::app()->db->createCommand();
		$dbCommand->select("idChannel");
		$dbCommand->from(App7020ChannelAssets::model()->tableName());
		$dbCommand->where("idAsset = :idAsset", array(":idAsset"=>$idAsset));
		$dbCommand->andWhere("asset_type = :asset_type", array(":asset_type"=>$asset_type));
		return $dbCommand->queryColumn();
	}
}
