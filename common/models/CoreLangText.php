<?php

/**
 * This is the model class for table "core_lang_text".
 *
 * The followings are the available columns in table 'core_lang_text':
 * @property integer $id_text
 * @property string $text_key
 * @property string $text_module
 * @property string $text_attributes
 * @property string $context
 *
 * The followings are the available model relations:
 * @property CoreLangTranslation $translations
 * @property CoreLangLanguage[] $languages
 *
 * @package docebo.models
 * @subpackage table
 */
class CoreLangText extends CActiveRecord
{
    const DEFAULT_TRANSLATION_LANGLAGE = 'english';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreLangText the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_lang_text';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('context', 'required'),
			array('text_key', 'length', 'max'=>255),
			array('text_module', 'length', 'max'=>50),
			array('text_attributes', 'safe'),
		);
	}

	/**
	 * @return array the scope definition.
	 */
	public function scopes() {
		return array(
			'login' => array(
				'condition' => 't.text_module = "login"',
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'translations' => array(self::BELONGS_TO, 'CoreLangTranslation', 'id_text'),
			'languages' => array(self::MANY_MANY, 'CoreLangLanguage', 'core_lang_translation(id_text, lang_code)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_text' => 'Id Text',
			'text_key' => 'Text Key',
			'text_module' => 'Module',
			'text_attributes' => 'Text Attributes',
			'context' => 'Context',
		);
	}




	/**
	 * Return all the list of modules translated
	 * @param bool $with_path
	 *
	 * @return array
	 */
	public function getModuleList($with_path = false)
	{
		$module_list = array();

		if (isset(Yii::app()->session['text_module_list']))
			return Yii::app()->session['text_module_list'];

		$criteria = new CDbCriteria;
		$criteria->select = 'DISTINCT t.text_module';
		$criteria->order = 't.text_module ASC';

		$res = CoreLangText::model()->findAll($criteria);
		foreach ($res as $row)
			$module_list[$row->text_module] = $row->text_module;

		$basePath = Yii::getPathOfAlias('common.messages');

		$files = glob("{$basePath}/en/*");
		if (!empty($files))
			foreach ($files as $file) {
				$text_module = str_replace('.php', '', basename($file));
				if ($with_path) $module_list[$text_module] = $basePath;
				else $module_list[$text_module] = $text_module;
			}

		// Check language extensions by plugins
		if (!empty(Yii::app()->messages->extensionPaths)) {
			foreach (Yii::app()->messages->extensionPaths as $module_name => $alias) {

				$base_alias = Yii::getPathOfAlias($alias);
				$files = glob($base_alias . "/en/*");
				if (!empty($files))
					foreach ($files as $file) {
						$text_module = str_replace('.php', '', basename($file));
						if ($with_path) $module_list[$module_name . "." . $text_module] = Yii::getPathOfAlias($base_alias);
						else $module_list[$module_name . "." . $text_module] = $module_name . "." . $text_module;
					}
			}
		}

		ksort($module_list, SORT_STRING);

		Yii::app()->session['text_module_list'] = $module_list;

		return $module_list;
	}




    /**
     * Same functionality as above, only that the id is also returned in an array
     *
     * @param string $module
     * @param string|bool $lang_code
     * @param string|bool $lang_code_diff
     * @param string $filter_status
     * @return array
     */
    public function getTranslationDetailed($module, $lang_code = false, $lang_code_diff = false, $filter_status = 'all')
    {
        $values = array();

        if ($lang_code_diff == false) {
            $sql = 'SELECT t.id_text, t.text_key, ta.translation_text, t.context, ta.translation_status, ta.save_date FROM '.self::model()->tableName().' t';
            $sql .= ' LEFT JOIN '.CoreLangTranslation::model()->tableName().' AS ta ON ( t.id_text = ta.id_text AND ta.lang_code = :lang_code)';
            $sql .= ' WHERE t.text_module = :text_module';
            $values[':lang_code'] = $lang_code;
            $values[':text_module'] = $module;

            if ($filter_status != 'all' && $filter_status) {
                $sql .= ' AND ta.translation_status = :translation_status';
                $values[':translation_status'] = $filter_status;
            }
        } else {
            $sql = 'SELECT t.id_text, t.text_key, t.text_module, t.context, ta.translation_text, ta.translation_status, tad.translation_text as translation_text_diff, ta.save_date';
            $sql .= ' FROM ('.self::model()->tableName().' AS t LEFT JOIN '.CoreLangTranslation::model()->tableName().' AS ta ON ( t.id_text = ta.id_text AND ta.lang_code = :lang_code))';
            $sql .= ' LEFT JOIN '.CoreLangTranslation::model()->tableName().' AS tad ON ( t.id_text = tad.id_text AND tad.lang_code = :lang_code_diff)';
            $sql .= ' WHERE t.text_module = :text_module';
            $values[':lang_code'] = $lang_code;
            $values[':lang_code_diff'] = $lang_code_diff;
            $values[':text_module'] = $module;

            if ($filter_status != 'all' && $filter_status) {
                $sql .= ' AND ta.translation_status = :translation_status';
                $values[':translation_status'] = $filter_status;
            }
        }

        $data = array();
        $result = Yii::app()->db->createCommand($sql)->bindValues($values)->queryAll();

        // Used for comparison
        foreach ($result as $obj) {
            $translation_text_diff = (isset($obj['translation_text_diff']) ? $obj['translation_text_diff'] : null);

            $data[$obj['text_key']] = array(
                'id_text' => $obj['id_text'],
                'translation_text' => $obj['translation_text'],
                'context' => $obj['context'],
                'translation_status' => $obj['translation_status'],
                'translation_text_diff' => (($translation_text_diff) ? $translation_text_diff : ''),
                'save_date' => $obj['save_date']
            );
        }
        return $data;
    }



	/**
	 * Loads the translation module from a php file
	 * @static
	 * @param $module
	 * @param bool $lang_code
	 * @param bool $basePath
	 * @return array|mixed
	 */
	public static function get_module_from_file($module, $lang_code = false, $basePath = false)
	{
		$langs = array();

		// check if is also available a file for the translations
		// !important: include_once returns boolean sometimes, so I had to use include

		$lang_code = Lang::getBrowserCodeByCode($lang_code);

		if (!$basePath) $basePath = Yii::getPathOfAlias('common.messages');

		$moduleArr = explode(".", $module);
		if (count($moduleArr) == 1) {

			//regular module
			if (file_exists("{$basePath}/{$lang_code}/{$module}.php"))
				$langs = include("{$basePath}/{$lang_code}/{$module}.php");

		} else {

			//extension module
			list($extension, $extensionModule) = $moduleArr;
			$extensionPaths = Yii::app()->messages->extensionPaths;
			if (!empty($extensionPaths) && isset($extensionPaths[$extension])) {
				$alias = $extensionPaths[$extension];
				$path = Yii::getPathOfAlias($alias).DIRECTORY_SEPARATOR.$lang_code.DIRECTORY_SEPARATOR.$extensionModule.'.php';
				if (file_exists($path)) {
					$langs = include($path);
				}
			}

		}

		return $langs;
	}



    /**
     * Data provider for grid
     *
     * @return \CActiveDataProvider
     */
    public function dataProvider() {
        Yii::import('application.extensions.arrayDataProvider.*');

        $filter_module = isset($_REQUEST['LangManagementFilter']['module']) ? $_REQUEST['LangManagementFilter']['module'] : null;
        $lang_code = isset($_REQUEST['LangManagementFilter']['langcode']) ? $_REQUEST['LangManagementFilter']['langcode'] : (isset($_GET['langcode']) ? $_GET['langcode'] : null);
        $lang_code_diff = isset($_REQUEST['LangManagementFilter']['lang_diff']) ? $_REQUEST['LangManagementFilter']['lang_diff'] : (isset($_GET['lang_diff']) ? $_GET['lang_diff'] : null);
        $text = isset($_REQUEST['LangManagementFilter']['search']) ? $_REQUEST['LangManagementFilter']['search'] : null;

        $translationModules = array();
        $modules = $filter_module && $filter_module != 'all' ? array($filter_module) : $this->getModuleList();
        foreach ($modules as $module) {
            $translationModules[$module] = array();

            // get from file
            $default_rows = self::get_module_from_file($module, self::DEFAULT_TRANSLATION_LANGLAGE);
            $module_rows = self::get_module_from_file($module, $lang_code);
            if ($lang_code_diff) $diff_row = self::get_module_from_file($module, $lang_code_diff);

            foreach ($default_rows as $k => $v) {
                $translationModules[$module][$k] = array(
                    'context' => '',
                    'delete' => '',
                    'id' => 0,
                    'save_date' => '',
                    'text_key' => $k,
                    'text_module' => $module,
                    'translation_status' => '',
                    'translation_text' => isset($module_rows[$k]) ? $module_rows[$k] : null,
                    'translation_text_diff' => ($lang_code_diff ? ArrayUtil::find_by_key($k, $diff_row) : ''),
                    'is_custom' => 0
                );
            }

            // get from db
            // now we overwrite the translation with the one from db
            $dbLangs = $this->getTranslationDetailed($module, $lang_code, $lang_code_diff);//, $lang_code, $lang_code_diff, $filter_status

            foreach ($dbLangs as $k => $arr) {
                if (empty($default_rows)) {
                    // we got nothing from static files
                    $row = array(
                        'context' => (empty($arr['context']) ? '' : $arr['context']),
                        'delete' => 'ajax.adm_server.php?r=adm/lang/deleteKey&id_text=' . $arr['id_text'],
                        'id' => $arr['id_text'],
                        'save_date' => $arr['save_date'],
                        'text_key' => $k,
                        'text_module' => $module,
                        'translation_status' => $arr['translation_status'],
                        'translation_text' => $arr['translation_text'],
                        'translation_text_diff' => $arr['translation_text_diff'],
                        'is_custom' => 1
                    );
                    $translationModules[$module][] = $row;
                } else {
					// We are looping through the DB entries now.
					// So loop through the selected module in translation files
					// and if DB translation exists, overwrite the files translation.
					if(isset($translationModules[$module]) && isset($translationModules[$module][$k])){
						// We have the currently looped DB translation in the
						// translations array that we got from static files.
						// Overwrite the translation from files with the one from DB.

						$row =& $translationModules[$module][$k]; // Get a reference for less typing :-)

						$row['id'] = $arr['id_text'];

                        if ($arr['translation_text_diff'])
                            $row['translation_text_diff'] = $arr['translation_text_diff'];

						if ($arr['translation_text'] != null) { // DB translation exists
							if ($arr['translation_text'])
                                $row['translation_text'] = $arr['translation_text'];
							$row['context'] = $arr['context'];
							$row['translation_status'] = $arr['translation_status'];
							$row['save_date'] = $arr['save_date'];
							$row['is_custom'] = 1;
						}
					}
                }
            }
        }

        // filters
        /*if ($filter_module) {
            if (isset($translationModules[$filter_module]))
                $translationModules = array($translationModules[$filter_module]);
            else
                $translationModules = array();
        }*/

		// Make all translations a single-level array
        $translation = array();
        foreach ($translationModules as $module) {
            foreach ($module as $single) {
                $translation[] = $single;
            }
        }

        // applying filters
        if ($text) {
            $only_empty = false;
            $tmp_data = array();
            foreach ($translation as $row) {
                $skip = false;

                // Regular search for string inside translation text
                if ($text != false && $only_empty == false) {
                    $pos = stripos($row['translation_text'].$row['text_key'].$row['translation_text_diff'], $text);
                    if ($pos === FALSE) { // If the searched string is not found in this phrase, ignore it
                        $skip = true;
                    }
                }
                // Get only non-empty phrases
                if ($only_empty != false) {
                    if (!empty($row['translation_text'])) {
                        $skip = true;
                    } // If the phrase is not empty, ignore it
                }

                // Get only approved English phrases
                if ($only_unapproved == TRUE && $lang_code == 'english') {
                    if ($row['translation_status'] == self::LANG_APPROVED || $row['translation_status'] == self::LANG_OK) {
                        $skip = true;
                    } // If the phrase is approved and with a status set
                }

                if (!$skip) {
                    $tmp_data[] = $row;
                }
            }
            $translation = $tmp_data;
        }

        $data = new CArrayDataProvider($translation, array('pagination' => array(
            'pageSize' => 50
        )));

        return $data;
    }


    public function getAllTranslations($target_language) {

        $translations = array(
            'english'        => array(),
            $target_language => array()
        );
        $modules = $this->getModuleList(true);

        // Retrive all the translations from file

        foreach ($modules as $module => $file_path) {

            // get from file
            $source_rows = self::get_module_from_file($module, 'english', $file_path);
            foreach ($source_rows as $k => $v) {

                $translations['english'][$module . '-' . $k] = array(
                    'id_text' => 0,
                    'text_key' => $k,
                    'text_module' => $module,
                    'translation_text' => $source_rows[$k],
                );
            }

            $target_rows = self::get_module_from_file($module, $target_language, $file_path);
            foreach ($target_rows as $k => $v) {

                $translations[$target_language][$module.'-'.$k] = array(
                    'id_text' => 0,
                    'text_key' => $k,
                    'text_module' => $module,
                    'translation_text' => $target_rows[$k],
                );
            }

        }

        // Retrive all the translation from db
        $results = Yii::app()->db->createCommand()
            ->select('t.id_text, t.text_key, t.text_module, ta.translation_text')
            ->from(self::model()->tableName() . ' t')
            ->join(CoreLangTranslation::model()->tableName() . ' ta', 't.id_text = ta.id_text')
            ->where('lang_code = :lang_code', array(':lang_code' => 'english'))
            ->queryAll();
        foreach ($results as $translation) {

            if (!isset($translations['english'][$translation['text_module'] . '-' . $translation['text_key']])) {

                $translations['english'][$translation['text_module'] . '-' . $translation['text_key']]['text_key'] = $translation['text_key'];
                $translations['english'][$translation['text_module'] . '-' . $translation['text_key']]['text_module'] = $translation['text_module'];
            }
            $translations['english'][$translation['text_module'] . '-' . $translation['text_key']]['id_text'] = $translation['id_text'];
            $translations['english'][$translation['text_module'] . '-' . $translation['text_key']]['translation_text'] = $translation['translation_text'];

        }

        $results = Yii::app()->db->createCommand()
            ->select('t.id_text, t.text_key, t.text_module, ta.translation_text')
            ->from(self::model()->tableName() . ' t')
            ->join(CoreLangTranslation::model()->tableName() . ' ta', 't.id_text = ta.id_text')
            ->where('lang_code = :lang_code', array(':lang_code' => $target_language))
            ->queryAll();
        foreach ($results as $translation) {

            if (isset($translations[$target_language][$translation['text_module'] . '-' . $translation['text_key']])) {

                $translations[$target_language][$translation['text_module'] . '-' . $translation['text_key']]['text_key'] = $translation['text_key'];
                $translations[$target_language][$translation['text_module'] . '-' . $translation['text_key']]['text_module'] = $translation['text_module'];
            }
            $translations[$target_language][$translation['text_module'] . '-' . $translation['text_key']]['id_text'] = $translation['id_text'];
            $translations[$target_language][$translation['text_module'] . '-' . $translation['text_key']]['translation_text'] = $translation['translation_text'];
        }

        return $translations;
    }


}