<?php

/**
 * This is the model class for table "core_running_operation".
 *
 * The followings are the available columns in table 'core_running_operation':
 * @property string $id_operation
 * @property integer $id_user
 * @property string $time_start
 * @property integer $time_start_ms
 * @property string $time_end
 * @property integer $time_end_ms
 * @property string $status
 */
class CoreRunningOperation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_running_operation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_operation', 'required'),
			array('id_user', 'numerical', 'integerOnly'=>true),
			array('id_operation', 'length', 'max'=>50),
			array('time_start', 'safe'),
			array('time_start_ms', 'safe'),
			array('time_end', 'safe'),
			array('time_end_ms', 'safe'),
			array('status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_operation, id_user, time_start, time_start_ms, time_end, time_end_ms, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_operation' => 'Id Operation',
			'id_user' => 'Id User',
			'time_start' => 'Time Start',
			'time_start_ms' => 'Time Start Ms',
			'time_end' => 'Time End',
			'time_end_ms' => 'Time End Ms',
			'status' => Yii::t('standard', '_STATUS')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_operation',$this->id_operation,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('time_start',$this->time_start,true);
		$criteria->compare('time_start_ms',$this->time_start_ms);
		$criteria->compare('time_start',$this->time_end,true);
		$criteria->compare('time_start',$this->time_end_ms);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreRunningOperation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	//---------------------

	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('time_start', 'time_end'),
				'dateAttributes' => array()
			)
		);
	}

	//---------------------

	const RUNNING_TIMEOUT = 5; //seconds, maximum allowed time for an operation to run

	const STATUS_RUNNING = 'running';
	const STATUS_DONE = 'done';
	const STATUS_ABORTED = 'aborted';
	const STATUS_TIMEOUT = 'timeout';


	protected static function _lockTable() {
		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$tableName = $db->quoteColumnName(self::model()->tableName());
		$db->createCommand("LOCK TABLES $tableName WRITE, $tableName AS t WRITE")->execute();
	}

	protected static function _unlockTable() {
		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$db->createCommand("UNLOCK TABLES")->execute();
	}


	/**
	 * Start a new operation of specified type. If operation cannot be started return false.
	 * @param $idOperation
	 * @return bool
	 */
	public static function startOperation($idOperation) {

		//validate input
		if (!$idOperation || !is_string($idOperation)) return false;

		$success = false; //initialize output variable

		self::_lockTable();

		//first: read operations table to check if there is an already running operation
		$model = self::model()->findByPk($idOperation);

		//check possible running operation timeout
		if ($model) { $model->checkTimeout(); }

		// if we were able to obtain a model from previous search then an operation of our type is already running and we 
		// have to stop, otherwise we proceed by inserting a record in the table and proceed with our stuff
		if (!$model || $model->status != self::STATUS_RUNNING) {
			$now = microtime();
			list($microseconds, $seconds) = explode(' ', $now);

			/* @var $lt LocalTime */
			$lt = Yii::app()->localtime;
			$timeStart = $lt->getLocalNow();
			$timeStartMs = intval((float)$microseconds * 1000000); //retrieve effective number of microseconds

			//populate operation properties and save it
			if (!$model) { $model = new self(); }
			$model->setAttributes(array(
				'id_operation' => $idOperation,
				'id_user' => Yii::app()->user->id,
				'time_start' => $timeStart,
				'time_start_ms' => $timeStartMs,
				'status' => self::STATUS_RUNNING,
			));
			$model->save();
			$success = true;
		}

		self::_unlockTable();

		return $success;
	}


	/**
	 * End a running operation.
	 * @param $idOperation
	 * @return bool
	 */
	public static function endOperation($idOperation) {

		//validate input
		if (!$idOperation || !is_string($idOperation)) return false;

		$success = false; //initialize output variable

		self::_lockTable();

		//first: read operations table to check if there is an already running operation
		$model = self::model()->findByPk($idOperation);
		if (!$model) {
			//we expect to find a running operation
			self::_unlockTable();
			return false;
		}

		//check possible running operation timeout
		$model->checkTimeout();

		if ($model->status == self::STATUS_DONE) {
			//operation is already ended
			self::_unlockTable();
			return false;
		}

		//update operation status and info
		$now = microtime();
		list($microseconds, $seconds) = explode(' ', $now);

		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;
		$timeEnd = $lt->getLocalNow();
		$timeEndMs = intval((float)$microseconds * 1000000); //retrieve effective number of microseconds

		//populate properties and save operation status
		$model->status = self::STATUS_DONE;
		$model->time_end = $timeEnd;
		$model->time_end_ms = $timeEndMs;
		$model->save();

		self::_unlockTable();

		return $success;
	}


	/**
	 * End a running operation with "aborted" status.
	 * @param $idOperation
	 * @return bool
	 */
	public static function abortOperation($idOperation) {

		//validate input
		if (!$idOperation || !is_string($idOperation)) return false;

		$success = false; //initialize output variable

		self::_lockTable();

		//first: read operations table to check if there is an already running operation
		$model = self::model()->findByPk($idOperation);
		if (!$model) {
			self::_unlockTable();
			//we expect to find a running operation
			return false;
		}

		//check possible running operation timeout
		$model->checkTimeout();

		if ($model->status == self::STATUS_DONE || $model->status == self::STATUS_ABORTED) {
			self::_unlockTable();
			//operation is already ended or aborted
			return false;
		}

		//update operation status and info
		$now = microtime();
		list($microseconds, $seconds) = explode(' ', $now);

		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;
		$timeEnd = $lt->getLocalNow();
		$timeEndMs = intval((float)$microseconds * 1000000); //retrieve effective number of microseconds

		//populate properties and save operation status
		$model->status = self::STATUS_ABORTED;
		$model->time_end = $timeEnd;
		$model->time_end_ms = $timeEndMs;
		$model->save();

		self::_unlockTable();

		return $success;
	}


	/**
	 * Return operation start time formatted as number of microseconds. If no time is specified then return null.
	 * @return int|null
	 */
	public function getStartTotalMicroseconds() {
		//first of all we need to actually have a start time
		$start = $this->time_start;
		if (empty($start)) { return NULL; }

		//start time has been retrieved as local date format --> convert it in a numeric timestamp
		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;
		$startTimestamp = $lt->getPHPTimestamp($start); //number of seconds, in UTC

		//add microseconds too
		return (intval($startTimestamp) * 1000000 + $this->time_start_ms);
	}


	/**
	 * Return operation end time formatted as number of microseconds. If no time is specified then return null.
	 * @return int|null
	 */
	public function getEndTotalMicroseconds() {
		//first of all we need to actually have an end time
		$end = $this->time_end;
		if (empty($end)) { return NULL; }

		//end time has been retrieved as local date format --> convert it in a numeric timestamp
		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;
		$endTimestamp = $lt->getPHPTimestamp($end); //number of seconds, in UTC

		//add microseconds too
		return (intval($endTimestamp) * 1000000 + $this->time_end_ms);
	}


	/**
	 * Check if a specific operation is running for too long time. Timeout limit is specified by RUNNIN_TIMEOUT constant.
	 */
	public function checkTimeout() {

		//we have to check timeout condition only for running operations
		if ($this->status != self::STATUS_RUNNING) return;

		//update operation status and info
		$now = microtime();
		list($microseconds, $seconds) = explode(' ', $now);

		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;

		//current time
		$timeNow = $lt->getLocalNow();
		$timeNowMs = intval((float)$microseconds * 1000000); //retrieve effective number of microseconds
		$nowTimeStamp =  $lt->getPHPTimestamp($timeNow); //number of seconds, in UTC

		//read operation starting time
		$timeStart = $this->time_start;
		$startTimestamp = $lt->getPHPTimestamp($timeStart); //number of seconds, in UTC

		//obtain final times to compare
		$finalTimeStart = (intval($startTimestamp) * 1000000 + $this->time_start_ms);
		$finalTimeNow = (intval($nowTimeStamp) * 1000000 + $timeNowMs);

		//do comparation
		if (($finalTimeNow - $finalTimeStart) > (self::RUNNING_TIMEOUT * 1000000)) {
			//target operation has been runnig more than allowed timeout time, so we have to change its status
			$this->status = self::STATUS_TIMEOUT;
			$this->save();
		}
	}


	/**
	 * Given a time, check if it is prior to last operation
	 * @param string $idOperation the operation type to check
	 * @param float $timeToBeChecked the time to be checked; format is (seconds.microseconds)
	 * @return bool
	 */
	public static function isBeforeLastOperationTime($idOperation, $timeToBeChecked) {
		$output = false; //initialize output variable
		$operation = self::model()->findByPk($idOperation); //retrieve the last executed operation of specified type
		if (!empty($operation)) {
			//we just need to check if an operation has a starting time greater than our time to be checked
			$operationStartTime = $operation->getStartTotalMicroseconds();
			if (!empty($operationStartTime)) { //check if we have a valid start time to check
				if ($operationStartTime > ((float)$timeToBeChecked * 1000000)) {
					// if we are arrived here, then it means that the target operation has been performed after our checking time
					$output = true;
				}
			}
		}
		return $output;
	}


	/**
	 * Return the current time in the format {seconds}.{microseconds} and UTC timezone
	 * @return string
	 */
	public static function getCurrentTimeMicroseconds() {

		//return number_format(microtime(true), 6, '.', '');

		//extract current microseconds
		$now = microtime();
		list($microseconds, $seconds) = explode(' ', $now);

		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;

		//current time
		$timeNow = $lt->getLocalNow();
		$timeNowMs = intval((float)$microseconds * 1000000); //retrieve effective number of microseconds
		$nowTimeStamp =  $lt->getPHPTimestamp($timeNow); //number of seconds, in UTC

		//compose final time {seconds.microseconds}, UTC timezone
		return $nowTimeStamp.'.'.$timeNowMs;
	}


}
