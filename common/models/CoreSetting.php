<?php

/**
 * This is the model class for table "core_setting".
 *
 * The followings are the available columns in table 'core_setting':
 * @property string $param_name
 * @property string $param_value
 * @property string $value_type
 * @property integer $max_size
 * @property string $pack
 * @property integer $regroup
 * @property integer $sequence
 * @property integer $param_load
 * @property integer $hide_in_modify
 * @property string $extra_info
 */
class CoreSetting extends CActiveRecord
{
	/**
	 * An instance of the Redis client used for caching settings in Hydra
	 * @var Predis\Client
	 */
	static private $redisCache = null;

    static public $params = array(
        'default_language' => 'default_language',
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreSetting the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function get($paramName, $default = null) {
		$setting = self::model()->findByPk($paramName);

		if ($setting) {
			return $setting->param_value;
		}

		return $default;
	}

    /**
     * Get param entity
     * 
     * @param string $paramName
     * @param string $default
     * @return \CoreSetting|boolean
     */
	public static function getEntity($paramName, $default = null) {
		$setting = self::model()->findByPk($paramName);

		if (!$setting)
            return false;

		return $setting;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('max_size, regroup, sequence, param_load, hide_in_modify', 'numerical', 'integerOnly'=>true),
			array('param_name', 'length', 'max'=>255),
			array('value_type, pack', 'length', 'max'=>25),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'param_name' => 'Param Name',
			'param_value' => 'Param Value',
			'value_type' => 'Value Type',
			'max_size' => 'Max Size',
			'pack' => 'Pack',
			'regroup' => 'Regroup',
			'sequence' => 'Sequence',
			'param_load' => 'Param Load',
			'hide_in_modify' => 'Hide In Modify',
			'extra_info' => 'Extra Info',
		);
	}

	public function afterSave(){
		$this->removeOldCache();
		parent::afterSave();
	}
	public function afterDelete(){
		$this->removeOldCache();
		parent::afterDelete();
	}

	/**
	 * Remove old (pre-Yii code) cache if exists
	 */
	private function removeOldCache(){
		if(is_file(Docebo::filesPathAbs().DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'dcache_chacing_setting.txt')){
			unlink(Docebo::filesPathAbs().DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'dcache_chacing_setting.txt');
		}

		// Empty Redis cache for Hydra (if it exists)
		if(Yii::app() instanceof CWebApplication) {

			// Init cache redis connection only the first time
			if(!self::$redisCache) {
				require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
				Predis\Autoloader::register();

				$_redisHost = Yii::app()->params['cache_redis_host'];
				$_redisPort = Yii::app()->params['cache_redis_port'];
				$_redisDb = Yii::app()->params['cache_redis_db'];

				/**
				 * Redis client initialization
				 */
				self::$redisCache = new Predis\Client(array(
					'host' => $_redisHost,
					'port' => $_redisPort,
					'database' => $_redisDb,
				));
			}

			// If component was initialized and settings key exists, delete it (so that it's refreshed in next hydra API call)
			$domain = Docebo::getOriginalDomain();
			if(self::$redisCache && self::$redisCache->exists($domain . ':settings'))
				self::$redisCache->del($domain . ':settings');
		}
	}


}