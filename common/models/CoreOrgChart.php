<?php

/**
 * This is the model class for table "core_org_chart".
 *
 * The followings are the available columns in table 'core_org_chart':
 * @property integer $id_dir
 * @property string $lang_code
 * @property string $translation
 *
 * The followings are the available model relations:
 * @property CoreOrgChartTree $coreOrgChartTree
 */
class CoreOrgChart extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreOrgChart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_org_chart';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_dir', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('translation', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_dir, lang_code, translation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreOrgChartTree' => array(self::HAS_ONE, 'CoreOrgChartTree', 'idOrg'),
			'coreOrgChartBranding' => array(self::HAS_ONE, 'CoreOrgChartBranding', 'id_org'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_dir' => 'Id Dir',
			'lang_code' => 'Lang Code',
			'translation' => 'Translation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_dir',$this->id_dir);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('translation',$this->translation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
	
	
	
	/**
	 * Provide a way to get the model translation and fall back to current language OR to 'english' as last option.
	 * 
	 * @param string $language e.g. 'en', 'de', 'it' (browsercode!)
	 * @return string
	 */
	public function translation($language = false) {
	    return CoreOrgChartTree::getTranslatedName($this->id_dir, $language);
	}

	public function afterSave(){
		$browserCode = Yii::app()->getLanguage();
		$language = Lang::getCodeByBrowserCode($browserCode);

		//raise event only after save item with current language
		if(!empty($this->lang_code) && ($this->lang_code == $language)) {
			Yii::app()->event->raise('CoreOrgChartAfterSave', new DEvent($this, array(
				'orgChart' => $this
 			)));
 		}

		// Event raised for YnY app to hook on to
 		return parent::afterSave();
 	}
	
}