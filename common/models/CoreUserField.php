<?php



/**
 * This is the model class for table "core_user_field".
 *
 * @property integer $id_field
 * @property string $type
 * @property integer $mandatory
 * @property integer $invisible_to_user
 * @property integer $sequence
 * @property string $settings
 * @property CoreUserFieldTranslations $translationEntry
 * @property array<CoreUserFieldTranslations> $translations
 * @property array<CoreUserFieldTranslations> $fieldTranslations
 * @property array<CoreUserFieldTranslations> $enTranslation
 *
 */
class CoreUserField extends \CActiveRecord {

	const TYPE_DATE			= 'date';
	const TYPE_DROPDOWN		= 'dropdown';
	const TYPE_TEXT			= 'textfield';
	const TYPE_TEXTAREA		= 'freetext';
	const TYPE_FISCALCODE	= 'codicefiscale';
	const TYPE_COUNTRY		= 'country';
	const TYPE_UPLOAD		= 'upload';
	const TYPE_YESNO		= 'yesno';

	//FOR YES-NO fields
	const VALUE_YES = '1';
	const VALUE_NO = '2';

	/**
	 * Mapping between field types and value column type
	 * @var array
	 */
	private $_valueColumnType = [
		self::TYPE_DATE			=> 'DATE',
		self::TYPE_DROPDOWN		=> 'SMALLINT',
		self::TYPE_TEXTAREA		=> 'TEXT',
		self::TYPE_TEXT			=> 'VARCHAR(255)',
		self::TYPE_COUNTRY		=> 'SMALLINT',
		self::TYPE_FISCALCODE	=> 'VARCHAR(255)',
		self::TYPE_UPLOAD		=> 'VARCHAR(255)',
		self::TYPE_YESNO		=> 'TINYINT'
	];

    const MANDATORY_SYMBOL_HTML = '<span class="mandatory"> *</span>';

    public $userEntry;
    public $confirm;
    public $lang_code;
    protected $translation;
    protected $translatedInLanguage;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
    public function tableName() {
        return 'core_user_field';
    }

	/**
	 * @inheritdoc
	 */
    public function rules()	{
		return [
			['type, mandatory, invisible_to_user', 'required'],
			['sequence, mandatory, invisible_to_user', 'numerical', 'integerOnly' => true],
            ['userEntry', 'validateFieldEntry', 'on' => 'additionalValidate', 'skipOnError' => true],
		];
	}

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dropdowns' => array(self::HAS_MANY, 'CoreUserFieldDropdown', 'id_field'),
            'translationEntry' => array(self::HAS_ONE, 'CoreUserFieldTranslations', 'id_field'),
            'translations' => array(self::HAS_MANY, 'CoreUserFieldTranslations', 'id_field'),
            // To be used when 'translations' key when there is a conflict due to matching relation names
            'fieldTranslations' => array(self::HAS_MANY, 'CoreUserFieldTranslations', 'id_field'),
            'enTranslation' => array(self::HAS_MANY, 'CoreUserFieldTranslations', 'id_field', 'on' => "enTranslation.lang_code = '" . Settings::get('default_language', 'english') . "'"),
        );
    }

    /**
     * @return array the scope definition.
     */
    public function scopes() {
        return array(
            'filters' => array(
                'condition' => 't.type <> "upload"',
            ),
        );
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [];
	}


	public function afterSave()
	{

		if ($this->isNewRecord) {
			// Add new column to the value table
			$sql = "ALTER TABLE core_user_field_value 
					CHARACTER SET = utf8 , COLLATE = utf8_general_ci,
  					ADD COLUMN field_" . $this->id_field . " " . $this->_valueColumnType[$this->type] . " DEFAULT NULL";

			$cmd = Yii::app()->db->createCommand($sql);
			$cmd->execute();
			
			// Invalidate the table schema cache
			Yii::app()->db->schema->getTable('core_user_field_value', true);
			
		}


		parent::afterSave();
	}

	public function afterDelete()
	{
		$sql = "ALTER TABLE core_user_field_value DROP COLUMN field_" . $this->id_field;

		$cmd = Yii::app()->db->createCommand($sql);
		$cmd->execute();


		parent::afterDelete();
	}

    /**
     * Returns the type of the field
     * Introduced as a common method of fetching field type for the current model and CoreField
     *
     * @return string
     */
    public function getFieldType()
    {
        return $this->type;
    }

    /**
     * Generating a unified string to be used as category name
     *
     * @uses CoreUserField::getFieldType()
     * @return string
     */
    public function getCategoryName()
    {
        return Yii::t('field', '_' . strtoupper($this->getFieldType()));
    }

    /**
     * Retrieving the next expected sequence
     *
     * @return int
     */
    public static function getNextSequence()
    {
        $queryString  = 'SELECT max(sequence) as maxSequence FROM ' . static::model()->tableName();
        $maxSequence  = Yii::app()->db->createCommand($queryString)->queryScalar();
        $nextSequenct = intval($maxSequence) + 1;

        return $nextSequenct;
    }

    /**
     * Returns the type of a specified by id field
     *
     * @param integer $id
     * @return string
     */
    public static function getFieldTypeById($id)
    {
        $instance = self::model()->findByAttributes(['id_field' => $id]);
        return $instance ? $instance->getFieldType() : null;
    }

    /**
     * Returns the ID of the field
     * Introduced as a common method of fetching field id for the current model and CoreField
     *
     * @return string|integer
     */
    public function getFieldId()
    {
        return $this->id_field;
    }

	public static function getFieldsTranslations($idField = 0, $longLangCode = null)
	{
		$longLangCode = $longLangCode ?: Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
		$defaultLangCode = Settings::get('default_language', 'english');

		$cmd = Yii::app()->db->createCommand()
			->select('cuf.id_field, cuf.type, cuf.sequence, cuf.invisible_to_user, cuf.settings, cuf.mandatory , IF(cuft.translation NOT LIKE "", cuft.translation, cuft2.translation) AS translation')
			->from(CoreUserField::model()->tableName() . ' cuf')
			->join('core_user_field_translation cuft', 'cuf.id_field = cuft.id_field AND cuft.lang_code LIKE :lang_code', [':lang_code' => $longLangCode])
			->join('core_user_field_translation cuft2', 'cuf.id_field = cuft2.id_field AND cuft2.lang_code LIKE :def_lang_code', [':def_lang_code' => $defaultLangCode]);

		if ($idField > 0) {
			$cmd->andWhere('cuf.id_field = :id_field', [':id_field' => $idField]);
		}

		$fields = $cmd->queryAll();
	
		return $fields;
	}

    /**
     * Retriving the translation value of the field's label
     *
     * @param string $language - return translation in the specified language
     * @return string
     */
    public function getTranslation($language = null) {
        if (empty($this->translation) === true) {
            $translation = static::getFieldsTranslations($this->getFieldId(), $language);
            $translation = current($translation);

            return $this->translation = $translation['translation'];
        }

        return $this->translation;
    }

    public function getTranslationLanguage()
    {
        return $this->translation->lang_code;
    }

    /**
     * Retrieving the field's translations, as default value could be used for default language if there
     * is no translation for the specified one - depending on the @param bool $withFallbackToDefaultLang
     *
     * @param string $language = null
     * @param bool $withFallbackToDefaultLang = true
     * @return array
     */
    public function getTranslations($language = null, $withFallbackToDefaultLang = true)
    {
        if ($language === null) {
            $language = $this->translatedInLanguage;
        }

        if ($language === null) {
            $currentLanguage = CoreLangLanguage::model()->findByAttributes(array(
                'lang_browsercode' => Yii::app()->getLanguage(),
            ));
            $language = $currentLanguage->lang_code;
        }

        $cmd = Yii::app()->db->createCommand();
        $selectStatements = [
            'cuf.id_field',
            'cuf.type',
            'cuf.sequence',
            'cuf.invisible_to_user',
            'cuf.settings',
            'cuf.mandatory',
        ];

        // Load fallback label - the translation for the default language
        if ($withFallbackToDefaultLang === true) {
            $defaultLangCode = Settings::get('default_language', 'english');
            $selectStatements[] = 'IF(cuft.translation NOT LIKE "", cuft.translation, cuft2.translation) AS translation';
            $cmd->join('core_user_field_translation cuft2', 'cuf.id_field = cuft2.id_field AND cuft2.lang_code LIKE :def_lang_code', [':def_lang_code' => $defaultLangCode]);
        } else {
            $selectStatements[] = 'cuft.translation';
        }

        $cmd
            ->select(implode(', ', $selectStatements))
            ->from(CoreUserField::model()->tableName() . ' cuf')
            ->join('core_user_field_translation cuft', 'cuf.id_field = cuft.id_field AND cuft.lang_code LIKE :lang_code', [':lang_code' => $language])
        ;

        $translations = $cmd->queryAll();

        return $translations;
    }

    /**
     * Adds components to model's query criteria in order to load language- depending details,
     * depending on the currently used language or optionally uses the passed @param $forceLanguage
     *
     * @param string $forceLanguage = null
     * @param boolean $useLangRaw = false - using the @param string $forceLanguage as passed, or fetch a language with matching 'lang_browsercode'
     * @return $this
     */
    public function language($forceLanguage = null, $useLangRaw = false)
    {
        static $cache = array();

        $language = $lang = null;
        if ($forceLanguage !== null && $useLangRaw === true) {
            $language = $forceLanguage;
        } else {
            $lang = ($forceLanguage === null ? Yii::app()->getLanguage() : $forceLanguage);

            if(!isset($cache[$lang])){
                $cache[$lang] = CoreLangLanguage::model()->findByAttributes(array(
                    'lang_browsercode' => $lang,
                ));
            }
            $language = $cache[$lang]->lang_code;
        }

        // Saving the specified language as a property, to be used as a reference
        // if needed later in other methods, so it would be taken into account
        $this->translatedInLanguage = $language;

        $params = array(':language' => $language);

        $filter = Yii::app()->request->getParam('filter', '');

        if(empty($filter) === false) {
            $params[':filter'] = '%'.$filter.'%';
        }

        // Joining the additional fields translations table details
        $joinDetails = $this->getJoinTranslationDetails();
        $trTableName = $joinDetails['joinTableName'];
        $trTableAlias = $joinDetails['joinTableAlias'];
        $fldTableAlias = $joinDetails['fieldsTableAlias'];
        $trKey = $trTableAlias . '.' . $joinDetails['joinedFieldName'];
        $fldKey = $fldTableAlias . '.' . $joinDetails['ownFieldName'];

        $this->getDbCriteria()->mergeWith(array(
            'condition' => $trTableAlias . '.`lang_code` = :language'.($filter !== '' ? ' AND '. $trTableAlias . '.`translation` like :filter' : ''),
            'params' => $params,
            'order' => $this->getTableAlias() . '.`sequence` ASC',
            'join' => 'INNER JOIN ' . $trTableName . ' AS ' . $trTableAlias . ' ON ' . $trKey . ' = ' . $fldKey,
        ));

        return $this;
    }

    /**
     * Provides a mechanism for modifying the CDbCommand of the model for fetching entries
     *
     * @param $modifyCallback
     * @return $this
     */
    public function modify($modifyCallback)
    {
        $modifyCallback($this->getDbCriteria());

        return $this;
    }

    /**
     * Returns the additional fields for the group
     * Copied from CoreField model
     *
     * @param int $idst_ocd
     * @return static[]
     * @todo The param idst_ocd doesn't really get used in the code, so should be probably removed?
     *       (The only usage of the method is in PowerUserApp's ProfileManagementController)
     */
    public function getAllAdditionalFields($idst_ocd = null)
    {
        if ($idst_ocd) {
            $fields = CoreGroupFields::model()->findAll(array(
                'select' => 'id_field',
                'condition' => "idst = $idst_ocd",
            ));
        } else {
            $fields = CoreUserField::model()->findAll(array(
                'select' => 'id_field',
                'group' => 'id_field',
                'distinct' => true,
                'order' => 'sequence ASC'
            ));
        }

        return $fields;
    }

    /**
     * Returns the criteria for joining the translations table working with `core_user_field` and related tables
     *
     * @param boolean $wrapInBackticks = true - if set to false - trims the backticks
     *        from the parameter values before returning those
     * @return []
     */
    public function getJoinTranslationDetails($wrapInBackticks = true)
    {
        $details = [
            'joinTableName'     => (new CoreUserFieldTranslations)->getTableSchema()->rawName,
            'joinTableAlias'    => '`cuft`',
            'joinedFieldName'   => '`id_field`',
            'fieldsTableName'   => $this->getTableSchema()->rawName,
            'fieldsTableAlias'  => '`' . $this->getTableAlias() . '`',
            'ownFieldName'      => '`id_field`',
        ];

        if ($wrapInBackticks === true) {
            return $details;
        }

        $details = array_map(function($value) {
            $value = ltrim($value, '`');
            $value = rtrim($value, '`');

            return $value;
        }, $details);

        return $details;
    }

    /**
     * Renders the field
     * Copied from CoreField
     *
     * @param bool $forAjax
     * @return string
     */
    public function renderField($forAjax = false) {
        $field = $this->populateField();

        $field->addErrors($this->errors);
        $field->userEntry = $this->userEntry;

        $html = "";

        if ($field->hasErrors()) {
            $html .= '<div class="clearfix" style="text-align: right;">';
            $html .= CHtml::error($field, 'userEntry');
            $html .= '</div>';
        }
        $html .= $field->render($forAjax);

        return $html;
    }

    /**
     * Renders the field as a filter
     * Copied from CoreField
     *
     * @return mixed
     */
    public function renderFilter() {
        $field = $this->populateField();
        return $field->renderFilterField();
    }

    /**
     * Returns the field using an instance of it's render class
     * Copied from CoreField
     *
     * @return mixed
     */
    private function populateField() {
        return $this->getTypeModel();
        // $className = 'Field' . ucfirst($this->getFieldType());
        // return $className::model()->findByAttributes(array('id_field' => $this->getFieldId()));
    }

    /**
     * Renders the user entry's value via the rendering classes
     * Copied from CoreField
     *
     * @param $userEntry
     * @param null $userEntryModel
     * @return mixed
     */
    public function renderValue($userEntry=false, $userEntryModel=null) {
        if ($userEntry === false) {
            $userEntry = $this->userEntry;
        }
        $field = $this->getTypeModel();
        return $field->renderFieldValue($userEntry, $userEntryModel);
    }

    /**
     * Renders the user entry's value
     * Copied from CoreField
     *
     * @param $userEntry
     * @param null $userEntryModel
     * @return string
     */
    public function renderFieldValue($userEntry, $userEntryModel=null) {
        return CHtml::encode($userEntry);
    }

    public static function renderFieldValueByUserId($idCommon, $idUser, $htmlEncode = true)
	{

		$fieldUserEntry = CoreUserFieldValue::model()->findByAttributes(array(
			'id_user' => $idUser
		));

		$fieldName = 'field_' . $idCommon;
		$userEntry = $fieldUserEntry->$fieldName;


		if ($htmlEncode)
			return CHtml::encode($userEntry);
		else
			return $userEntry;
	}

	/**
     * Return a collection of additional field model instances
     * Uses the data of `CoreUser` and the related models
     * 
	 * @param CoreUser $user User model
	 * @param string $withUserEntryValues If TRUE, Fill in field values for the user 
	 * @param string $visibleOnly If TRUE, Filter out fields not visible by the user (through branches)
	 * @return static[]
	 */
	public static function getForUser(CoreUser $user, $withUserEntryValues = false, $visibleOnly=true)
    {
        $fields = [];

        $criteria = new CDbCriteria();

        // Filtering fields invisible to users
        if($user->getScenario() == 'selfEditProfile'){
            // If the user is self-editing his profile
            // don't get fields marked as "Invisible to user".
            // Those are only visible for admins editing
            // the user's profile.
            $criteria->addCondition('invisible_to_user=0');
        }
        
        // Get visible field IDs, so we can filter, but ONLY if that setting is ON and $user is not a Super Admin and we are requested to do so
        $visibleFieldIds = false;
        if ($visibleOnly && Settings::get('use_node_fields_visibility', 'off') === 'on' && !CoreUser::isUserGodadmin($user->idst)) {
            $visibleFieldIds = $user->getVisibleFields();
        }
       
        // If we've got a filtering array and user is NOT a GodAdmin, ok, lets filter the result
        // Note, PowerUser is handled inside the above call!
        if (is_array($visibleFieldIds)) {
            // Edge case: empty list of visible field. That means: NO VISIBLE FIELDS! 
            if (empty($visibleFieldIds)) {
                return array();
            }
            $criteria->addInCondition("id_field", $visibleFieldIds);
        }

        // Loading the fitered by criteria fields
        $fields = static::model()->findAll($criteria);

        // Load Additional fields data if this is an existing model OR we
        // are creating user from Temporary User and if there are results
        if ($withUserEntryValues === false) {
            return $fields;
        }

        // Loading the additional field values for the specified user
        $fields = static::setUserValuesToAdditionalFields($fields, $user);

        return $fields;
    }

    /**
     * Returns an array of IDs of the additional fields, filtered by branch.
     * Also applying some basic filtering based on input User Model, BUT NOT real visibility per-se!
     * For true visibility filtering, use self::getForUser()
     *
     * @param $user - the user to load the CoreField ids for
     * @param array $nodes - ids of the branches to filter by
     * @return array
     * @todo Moved from CoreUser and modified only for using the current model! Needs refactoring for performance improvement!
     */
    public static function getIdsForUserByBranch($user, array $nodes = [])
    {
        $criteria = new CDbCriteria();

        // If not editing own profile and nodes visibility usage setting is off - return the loaded data
        if (($user->scenario != 'selfEditProfile') && (Settings::get('use_node_fields_visibility', 'off') == 'off')) {
            $fields = CoreUserField::model()->language()->findAll($criteria);

            return CHtml::listData($fields, 'id_field', 'id_field');
        }

        $allowedFields = array();
        $rootNode = CoreOrgChartTree::model()->getOrgRootNode();

        if(empty($nodes)) {
            if($user->isNewRecord && isset(Yii::app()->session['currentNodeId']) && Yii::app()->session['currentNodeId']){
                // Selected branch from org chart tree
                $nodes[] = Yii::app()->session['currentNodeId'];
            }
        }

        if(Yii::app()->user->getIsPu()) {
            $puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();
            // PU with at least one assigned branch
            if(count($puBranchIdsArray) > 0) {
                // show only additional fields, that are visible for the branches, assigned to PU
                $nodes = array_intersect($nodes, $puBranchIdsArray);
                if(empty($nodes))
                    $nodes = $puBranchIdsArray;
            }
        }

        if (count($nodes) !== 0) {
            foreach($nodes as $nodeId){
                $node = CoreOrgChartTree::model()->findByPk($nodeId);
                if($node && !$node->isRoot()){
                    $cmd = Yii::app()->db->createCommand("SELECT gf.id_field "
                        ." FROM ".CoreOrgChartTree::model()->tableName()." oct "
                        ." JOIN ".CoreGroupFields::model()->tableName()." gf ON (gf.idst = oct.idst_oc OR gf.idst = oct.idst_ocd) "
                        ." WHERE oct.iLeft < :iLeft AND oct.iRight > :iRight AND oct.idOrg <> :root");
                    $reader = $cmd->query(array(
                        ':iLeft' => $node->iLeft,
                        ':iRight' => $node->iRight,
                        ':root' => $rootNode->getPrimaryKey()
                    ));

                    while ($row = $reader->read()) {
                        $allowedFields[] = $row['id_field'];
                    }

                    $cmd = Yii::app()->db->createCommand("SELECT * FROM ".CoreGroupFields::model()->tableName()." WHERE idst = :idst_oc OR idst = :idst_ocd");
                    $reader = $cmd->query(array(
                        ':idst_oc' => $node->idst_oc,
                        ':idst_ocd' => $node->idst_ocd
                    ));
                    while ($row = $reader->read()) {
                        $allowedFields[] = $row['id_field'];
                    }
                }
            }
        }

        if(Yii::app()->user->getIsPu() || (!empty($allowedFields) && is_array($allowedFields))){
            $instance = new static;
            $criteria->addInCondition($instance->getTableAlias() . '.id_field', $allowedFields);
        }

        $fields = CoreUserField::model()->language()->findAll($criteria);

        return CHtml::listData($fields, 'id_field', 'id_field');
    }

    /**
     * Iterates the passed fields collection and adds the values related to the specified user
     *
     * @param array $fields
     * @param CoreUser $user
     * @return array
     */
    public static function setUserValuesToAdditionalFields(array $fields, CoreUser $user)
    {
        $userValues = static::getValuesForUsers([$user->idst], true);
        $userValues = $userValues[$user->idst];

        foreach ($fields as &$field) {
            $field->userEntry = isset($userValues[$field->id_field])
                ? $userValues[$field->id_field]
                : null
            ;
        }

        return $fields;
    }

    /**
     * Updating a field's user entry value
     * Immediately saves the changes to the database
     *
     * @param integer $fieldId
     * @param integer|CoreUser $user
     * @param scalar $value
     * @return boolean
     */
    public static function updateUserValue($fieldId, $user, $value)
    {
        $userId = $user instanceof CoreUser ? $user->idst : $user;
        $userEntry = CoreUserFieldValue::model()->findByAttributes(['id_user' => $userId]);

        if (empty($userEntry) === true) {
            $userEntry = new CoreUserFieldValue;
            $userEntry->id_user = $userId;
        }

        $fieldColumn = 'field_' . $fieldId;
        $userEntry->{$fieldColumn} = $value;

        return $userEntry->save();
    }

    /**
     * Returns the additional field values for a collection of user(s) the specified by id
     *
     * @param array $userId
     * @param boolean $returnAsArray = false
     * @param array $fieldIds = [] - ids array of the fields
     * @return array
     */
    public static function getValuesForUsers(array $userId, $returnAsArray = false, array $fieldIds = [])
    {
        // Breaking the loaded single row of field values
        $queryParams = [];
        $queryParams['id_user'] = $userId;
        $fieldUserEntryDetails = CoreUserFieldValue::model()->findAllByAttributes($queryParams);

        $grouped = [];

        foreach ($fieldUserEntryDetails as $index => $entry) {
            $userId = $entry['id_user'];

            if ($returnAsArray === false) {
                $grouped[$userId] = $entry;
                continue;
            }

            $entry = $entry->getAttributes();

            unset($entry['id_user']);
            $vals = array_values($entry);
            $keys = array_keys($entry);
            $keys = array_map(function($key) {
                // $key = str_replace('field_', '', $key);

                // Alternative to string replace for performance enhancement, such as
                // we know the length and the string itself that needs to be removed
                $key = substr($key, 6);
                return intval($key);
            }, $keys);

            $grouped[$userId] = array_combine($keys, $vals);

            if (count($fieldIds)) {
                $fieldIdKeys = array_flip($fieldIds);
                $grouped[$userId] = array_intersect_key($grouped[$userId], $fieldIdKeys);
            }
        }

        return $grouped;
    }

    /**
     * Retrieving an array of CoreField instances, that are mandatory and there is no value for the user
     *
     * @param integer $userId = null
     * @return array<CoreField>
     */
    public static function getUnfilledMandatoryFields($userId = null)
    {
        if ($userId === null) {
            $userId = Yii::app()->user->getId();
        }

        $anonymous = CoreUser::model()->findByAttributes(array('userid' => '/Anonymous'));
        $id_anonymous = $anonymous->getPrimaryKey();

        if(!$userId || $id_anonymous == $userId) {
            return [];
        }

        $user = CoreUser::model()->findByPk($userId);
        $visibleFieldIds = $user->getVisibleFields(true);
        $values = static::getValuesForUsers([$userId], true, $visibleFieldIds);
        $values = $values[$userId];
        $withValues = array_keys(array_filter($values));
        $noValues = array_diff($visibleFieldIds, $withValues);

        $fields = static::model()->findAllByPk($noValues);

        return $fields;
    }

	public static function renderYesNoFieldByUser($fieldId,$userId)
	{

		$fieldName = 'field_' . $fieldId;

		$cmd = Yii::app()->db->createCommand()
			->select([$fieldName])
			->from(CoreUserFieldValue::model()->tableName() . ' cufv')
			->where('cufv.id_user = :id_user', [':id_user' => (int)$userId]);

		$yesNoValue = $cmd->queryColumn();

		if (!$yesNoValue) {
			return Yii::t('standard', 'Not set');
		}


		switch ($yesNoValue[0]) {
			case self::VALUE_YES:
				return Yii::t('standard', '_YES');
			case self::VALUE_NO:
				return Yii::t('standard', '_NO');
			default:
				return Yii::t('standard', 'Not set');

		}

	}

    /**
     * Mirrors the result of CoreField::additionalFieldsSearch() and CoreField::getAdditionalFieldsDataProvider()
     * To be used in additionalFields/index view
     *
     * @param bool $forceLanguage
     * @return CArrayDataProvider
     */
    public function getAdditionalFieldsDataProvider($forceLanguage=false)
    {
        static $cache = array();

        $lang = ($forceLanguage === false ? Yii::app()->getLanguage() : $forceLanguage);

        if(!isset($cache[$lang])){
            $cache[$lang] = CoreLangLanguage::model()->findByAttributes(array(
                'lang_browsercode' => $lang,
            ));
        }

        $language = $cache[$lang];
        $params = array(':language' => $language->lang_code);
        $filter = Yii::app()->request->getParam('filter', '');

        if($filter !== '')
            $params[':filter'] = '%'.$filter.'%';

        $this->getDbCriteria()->mergeWith(array(
            'condition' => ($filter !== '' ? ' AND enTranslation.translation like :filter' : ''),
            'params' => $params,
            'order' => $this->getTableAlias() . '.sequence ASC'
        ));

        $rawData = $this->with('enTranslation')->findAll();

        return new CArrayDataProvider($rawData, array(
            'keyField' => 'id_field',
            'pagination' => false,
        ));
    }

    /**
     * Returns a data provider used for OrgManagementController's editFieldsVisibility,
     * copied from CoreField
     *
     * @return CActiveDataProvider
     */
    public function getOrgChartVisibilityDataProvider()
    {
        $trTbl = CoreUserFieldTranslations::model()->tableName();
        $joinTranslations = 'INNER JOIN ' . $trTbl . ' ON ' . $trTbl . '.id_field = ' . $this->getTableAlias() . '.id_field';

        $criteria = new CDbCriteria();
        $criteria->join = $joinTranslations;
        $criteria->addCondition($trTbl . ".lang_code = :language");
        if(!Yii::app()->user->getIsGodadmin())
            $criteria->addCondition("invisible_to_user = 0");
        $criteria->params[':language'] = Yii::app()->session['current_lang'];//'bulgarian';
        $criteria->order = "sequence ASC";
        //$fields = CoreField::model()->findAll($criteria);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false
        ));
    }

    /**
     * Validation the user entry for the field
     * Copied from CoreField
     *
     * @return void
     */
    public function validateFieldEntry() {
        if ($this->mandatory) {
            if (empty($this->userEntry) && strlen($this->userEntry) <= 0) {
                $this->addError('userEntry', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
            }
        }
    }

    /**
     * Retrieving the possible options for additional field condition rules
     *
     * @return array
     */
    public function getConditionsOptions() {
        return array(
            'contains' => 'Contains',
            'equal' => 'Equal to',
            'not-equal' => 'Not equal to',
        );
    }
    

    /**
     * Create and return a new object, representing the specific field type of this Core User Field
     * 
     * @return FieldTextfield|FieldCountry|FieldDate|FieldDropdown|FieldUpload|...   
     */
    public function getTypeModel() {
        /** @var CoreUserField $subtypeModel */
        $className = "Field" . ucfirst($this->type);
        $subtypeModel = new $className();
        $subtypeModel->setAttributes($this->attributes, false);
        return $subtypeModel;
    }

}
