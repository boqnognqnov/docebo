<?php

/**
 * This is the model class for table "lt_classroom".
 *
 * The followings are the available columns in table 'lt_classroom':
 * @property integer $id_classroom
 * @property string $name
 * @property string $details
 * @property integer $id_location
 * @property integer $seats
 * @property string $equipment
 *
 * The followings are the available model relations:
 * @property LtLocation $learningLocation
 */
class LtClassroom extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lt_classroom';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, id_location, seats', 'required'),
			array('id_location, seats', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('details, equipment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_classroom, name, id_location', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'ltLocation' => array(self::BELONGS_TO, 'LtLocation', 'id_location'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_classroom' => 'Classroom id',
			'name' => Yii::t('classroom', 'Classroom name'),
			'id_location' => 'Location id',
			'seats' => Yii::t('standard', 'Available seats'),
			'details' => Yii::t('standard', '_DETAILS'),
			'equipment' => Yii::t('classroom', 'Equipment')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_classroom',$this->id_classroom);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_location',$this->id_location);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LtClassroom the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	public function dataProvider() {
		$config = array();
		$criteria = new CDbCriteria();

		if (!empty($this->name)) {
			$criteria->addCondition('t.name LIKE :search');
			$criteria->params[':search'] = "%$this->name%";
		}
		if (isset($this->id_location)) {
			$criteria->addCondition('t.id_location = :id_location');
			$criteria->params[':id_location'] = $this->id_location;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = array('name' => false);
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	/**
	 * Returns true if a classroom is used in at least one session date in the future
	 * @param $classroomId
	 * @return bool
	 */
	public static function isUsedInFutureSessionDate($classroomId) {
		$activeSessionDates = Yii::app()->db->createCommand()
			->select('id_session, CONVERT_TZ(CONCAT_WS(" ", day, time_end), timezone, "UTC") AS utcEndTimestamp')
			->from(LtCourseSessionDate::model()->tableName())
			->where('id_classroom = :id_classroom', array(':id_classroom' => $classroomId))
			->having('utcEndTimestamp > :utcNow', array(':utcNow' => Yii::app()->localtime->getUTCNow()))
			->queryAll();

		if(count($activeSessionDates) > 0)
			return true;
		else
			return false;
	}

}
