<?php

/**
 * This is the model class for table "learning_certificate_meta_assign".
 *
 * The followings are the available columns in table 'learning_certificate_meta_assign':
 * @property integer $idUser
 * @property integer $idMetaCertificate
 * @property integer $idCertificate
 * @property string $on_date
 * @property string $cert_file
 */
class LearningCertificateMetaAssign extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCertificateMetaAssign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_certificate_meta_assign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idMetaCertificate, idCertificate', 'numerical', 'integerOnly'=>true),
			array('cert_file', 'length', 'max'=>255),
			array('on_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUser, idMetaCertificate, idCertificate, on_date, cert_file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('on_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUser' => 'Id User',
			'idMetaCertificate' => 'Id Meta Certificate',
			'idCertificate' => 'Id Certificate',
			'on_date' => 'On Date',
			'cert_file' => 'Cert File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idMetaCertificate',$this->idMetaCertificate);
		$criteria->compare('idCertificate',$this->idCertificate);
		$criteria->compare('on_date',Yii::app()->localtime->fromLocalDateTime($this->on_date),true);
		$criteria->compare('cert_file',$this->cert_file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}