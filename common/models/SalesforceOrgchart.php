<?php

/**
 * This is the model class for table "salesforce_orgchart".
 *
 * The followings are the available columns in table 'salesforce_orgchart':
 * @property integer $id
 * @property integer $id_org
 * @property integer $id_parent
 * @property integer $sf_lms_id
 * @property string $sf_account_id
 * @property string $sf_contract_id
 * @property string $sf_type
 * @property integer $active
 * @property integer $is_root
 *
 * The followings are the available model relations:
 * @property CoreOrgChartTree $idOrg
 */
class SalesforceOrgchart extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_orgchart';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_org, id_parent, sf_lms_id, sf_type', 'required'),
			array('id_org, id_parent, sf_lms_id, active, is_root', 'numerical', 'integerOnly'=>true),
			array('sf_account_id, sf_contract_id', 'length', 'max'=>64),
			array('sf_type', 'length', 'max'=>16),
			array('active, is_root', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_org, id_parent, sf_lms_id, sf_account_id, sf_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idOrg' => array(self::BELONGS_TO, 'CoreOrgChartTree', 'id_org'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_org' => 'Id Org',
            'id_parent' => 'Id Parent',
			'sf_lms_id' => 'Sf Lms',
			'sf_account_id' => 'Sf Account',
            'sf_contract_id' => 'Sf Contract',
			'sf_type' => 'Sf Type',
			'active' => 'Active',
			'is_root' => 'Is root',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_org',$this->id_org);
        $criteria->compare('id_parent',$this->id_parent);
		$criteria->compare('sf_lms_id',$this->sf_lms_id);
		$criteria->compare('sf_account_id',$this->sf_account_id,true);
        $criteria->compare('sf_contract_id',$this->sf_contract_id,true);
		$criteria->compare('sf_type',$this->sf_type,true);
		$criteria->compare('active',$this->active,true);
		$criteria->compare('is_root',$this->is_root,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceOrgchart the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public static function isStructured($sfLmsAccount, $type){
        
        if(empty($sfLmsAccount)){
            return false;
        }
        
        $params = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition('sf_lms_id = :id');
        $params[':id'] = $sfLmsAccount;
        if(is_array($type)){
            $criteria->addInCondition('sf_type', $type);
        } else{
            $criteria->addCondition('sf_type = :type', 'AND');
            $params[':type'] = $type;
        }
        
        $criteria->params = $params;
        $sfOrg = self::model()->find($criteria);
        
        if(!empty($sfOrg)){
            return true;
        }
        
        return false;
    }

	/**
	 * Get the idOrg of the Salesforce Orgchart Branch with specific type (user, contatct, lead)
	 *
	 * @param $type User|Contact|Lead
	 * @return bool|int idOrg of the branch
	 */
	public static function getSfBranchIdOrg($type){
		$self = Yii::app()->db->createCommand()
			->select('id_org')
			->from(self::model()->tableName())
			->where('sf_type = :sfType', array(
				':sfType' => $type
			))
			->queryScalar();
		if($self){
			return $self;
		} else {
			return false;
		}
	}

	public static function getAllOrgChats($sf_lms_id){
		$command = Yii::app()->db->createCommand();
		$command->select('id_org');
		$command->from(self::model()->tableName());
		$command->where('sf_lms_id = :account', array(
			':account' => $sf_lms_id
		));

		$orgs = $command->queryAll();

		$data = array();

		foreach($orgs as $org){
			$data[] = $org['id_org'];
		}


		return $data;
	}
}
