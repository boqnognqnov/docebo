<?php

/**
 * This is the model class for table "learning_coursepath_courses".
 *
 * The followings are the available columns in table 'learning_coursepath_courses':
 * @property integer            $id_path
 * @property integer            $id_item
 * @property integer            $in_slot
 * @property string             $prerequisites
 * @property integer            $sequence
 * @property integer            $delay_time
 * @property string             $mode
 * @property string             $delay_time_unit
 *
 * The followings are the available model relations:
 * @property LearningCourse     $idItem
 * @property LearningCoursepath $idPath
 * @property LearningCourse $learningCourse
 */
class  LearningCoursepathCourses extends CActiveRecord
{

	const _CUS_WAITING_LIST = -2; //_CUS_WAITING_PAYMENT
	const _CUS_CONFIRMED   = -1;
	const _CUS_SUBSCRIBED  = 0;
	const _CUS_BEGIN       = 1;
	const _CUS_END         = 2;
	const _CUS_SUSPEND     = 3;
	const _CUS_OVERBOOKING = 4; //the user is overbooked

	public $courses; // get count of courses
	/**
	 * Helper property to specify the minimum number of prerequisite courses out of the total selected
	 * @var integer
	 */
	public $min_courses;


	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningCoursepathCourses the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_coursepath_courses';
	}

	public function behaviors()
	{
		$behaviors                = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');

		return $behaviors;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_path, id_item', 'required'),
			array('id_path, id_item, in_slot, sequence, delay_time', 'numerical', 'integerOnly' => TRUE),
			array('mode, delay_time_unit', 'length', 'max' => 10),
			array('mode, min_courses', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_path, id_item, in_slot, prerequisites, sequence, mode, delay_time, delay_time_unit', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idItem'             => array(self::BELONGS_TO, 'LearningCourse', 'id_item'),
			'idPath'             => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
			'learningCourse'     => array(self::BELONGS_TO, 'LearningCourse', 'id_item'),
			'learningCourseuser' => array(self::HAS_MANY, 'LearningCourseuser', array('idCourse' => 'id_item')),
			'learningCoursepath' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
			'learningCoursepathUser'=>array(self::BELONGS_TO, 'LearningCoursepathUser', 'id_path', 'on'=>'learningCoursepathUser.idUser=:idUser', 'params'=>array(':idUser'=>Yii::app()->user->getIdst())),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_path'       	=> 'Id Path',
			'id_item'       	=> 'Id Item',
			'in_slot'       	=> 'In Slot',
			'prerequisites' 	=> 'Prerequisites',
			'sequence'			=> 'Sequence',
			'mode'				=> 'Mode',
			'delay_time'		=> 'Delay Time',
			'delay_time_unit'	=> 'Delay Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_path', $this->id_path);
		$criteria->compare('id_item', $this->id_item);
		$criteria->compare('in_slot', $this->in_slot);
		$criteria->compare('prerequisites', $this->prerequisites, TRUE);
		$criteria->compare('sequence', $this->sequence);
		$criteria->compare('mode', $this->mode);
		$criteria->compare('delay_time', $this->delay_time);
		$criteria->compare('delay_time_unit', $this->delay_time_unit);

		return new CActiveDataProvider($this, array(
			'criteria'   => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	/**
	 * Check is course is locked
	 *
	 * @return boolean
	 */
	public function isLocked($userId = false)
	{
		if (!$this->prerequisites) {
			return FALSE;
		}
		$prerequisites = explode(',', $this->prerequisites);

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.idUser = :idUser AND t.status = :status');
		if ($userId) {
			$criteria->params[':idUser'] = $userId;
		} else {
			$criteria->params[':idUser'] = Yii::app()->user->id;
		}
		$criteria->params[':status'] = LearningCourseuser::$COURSE_USER_END;
		$criteria->addInCondition('idCourse', $prerequisites);

		$res = (int) LearningCourseuser::model()->count($criteria);

		if ($this->mode === 'all') {
			// test if ALL prerequisite courses have been completed
			$unlockedCondition = ($res == count($prerequisites));
		} else {
			// test if AT LEAST n courses have been completed
			$unlockedCondition = ($res >= intval($this->mode));
		}
		if ($unlockedCondition) {
			$isTimeDelayed = $this->isDelayTimeLock(true);
			if ($isTimeDelayed[0]) {
				// first parameter is for locked condition/true - it's locked/, second parameter is the time of unlocking moment/
				return array(true, $isTimeDelayed[1]);
			}
		}

		return $unlockedCondition ? FALSE : TRUE;
	}

//	return true or false if the delay time/if is set/ is in charge
	private function isDelayTimeLock($subscore = false)
	{
		$localToUTCTime = Yii::app()->localtime->toUTC(Yii::app()->localtime->getLocalNow());
		$localToUTCTime = str_replace('T', ' ', $localToUTCTime);
		$localToUTCTime = substr($localToUTCTime, 0, count($localToUTCTime) - 6);
		$timestampLocal = strtotime($localToUTCTime);
		$maxDateCompletedRequiredCourseFromDB = Yii::app()->db->createCommand()->
		select('MAX(lcu.`date_complete`)')->
		from('learning_courseuser lcu')->
		where('lcu.idUser=:idUser', array(':idUser' => Yii::app()->user->id))->
		andWhere(array('IN', 'lcu.idCourse', explode(',', $this->prerequisites)))->queryScalar();
		$timestampMaxDateFromDB = strtotime($maxDateCompletedRequiredCourseFromDB);
		switch ($this->delay_time_unit) {
			case 'day':
				$delay = strtotime('+1 day', 0) * $this->delay_time;
				break;
			case 'week':
				$delay = strtotime('+1 week', 0) * $this->delay_time;
				break;
			case 'month':
				$delay = strtotime('+1 month', 0) * $this->delay_time;
				break;
		}
		$conditionForTimeDelay = ($timestampLocal <= $timestampMaxDateFromDB + $delay);
		if ($subscore) {
			return array($conditionForTimeDelay, $timestampMaxDateFromDB + $delay);
		}
		return $conditionForTimeDelay;
	}

	/**
	 * @param LearningCourseuser $enrollment
	 * @return string
	 */
	public function getPrerequisiteMessage($enrollment = null)
	{
		$message = null;
		$isTimeDelayedArray = $this->isLocked();
		if ($isTimeDelayedArray[0]) {
			$dateUnlockFromDb = str_replace('/', '-', date('Y-m-d H:m:s', $isTimeDelayedArray[1]));
			$dateToUnlock = Yii::app()->localtime->toLocalDateTime($dateUnlockFromDb);
			$message = Yii::t('curricula', 'The course will be available from {datetime}', array('{datetime}' => $dateToUnlock));
		} else if ($enrollment && $enrollment->status == LearningCourseuser::$COURSE_USER_WAITING_LIST) {
			$message = Yii::t('curricula', 'You must purchase this course in order to unlock it.');
		}

		if(!is_array($isTimeDelayedArray) && $isTimeDelayedArray == true) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idCourse', explode(',', $this->prerequisites));
			$courses = LearningCourse::model()->findAll($criteria);
			if(!$courses) {
				// No prerequisites, no tooltip
				return null;
			}
			$courses = CHtml::listData($courses, 'idCourse', 'name');

			if($this->mode === 'all') {
				$message = Yii::t('curricula', 'Please complete the following courses to unlock this course:');
			} else {
				$message = Yii::t('curricula', 'Please complete at least {n} of the following courses to unlock this course:', array(
					'{n}' => intval($this->mode)
				));
			}
			$message .= '<br/><br/>"<b>' . implode('</b>",<br/><br/><b>"', $courses) . '</b>"';
		}
		return $message;
	}

	private static function getLocalDateFormat()
	{
		$localFormat = Yii::app()->localtime->getLocalDateTimeFormat(LocalTime::DEFAULT_DATE_WIDTH);
		$localFormat = Yii::app()->localtime->YiitoPHPDateFormat($localFormat);
		return $localFormat;
	}

	public function dataProvider($prerequisites = FALSE)
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.id_path', $this->id_path);
		if ($prerequisites) {
			$criteria->compare('t.id_item', '<>' . $this->id_item);
			$pagination = array('pageSize' => Settings::get('elements_per_page', 10));
		} else {
			$pagination = FALSE;
		}

		if ($this->idItem !== NULL) {
			$criteria->compare('idItem.name', $this->idItem->name, TRUE);
		}
		$criteria->compare('t.in_slot', $this->in_slot);
		$criteria->with = array('idItem');

		$criteria->together = TRUE;
		$criteria->order    = 't.sequence';

		return new CActiveDataProvider($this, array(
			'criteria'   => $criteria,
			'pagination' => $pagination,
		));
	}

	public function renderMove()
	{
		return '<div class="course-move">' . $this->id_item . '</div>';
	}

	public function prerequisitesToArray()
	{
		if ($this->prerequisites === '') {
			return array();
		} else {
			return explode(',', $this->prerequisites);
		}
	}

	public function renderPrerequisites()
	{
		if ($this->prerequisites == '') {
			$prerequisites = 0;
		} else {
			$prerequisites = count(explode(',', $this->prerequisites));
		}
		$content = $prerequisites . '<span class="curricula-course-prerequisites"></span>';

		return CHtml::link($content, Yii::app()->createUrl('CurriculaApp/curriculaManagement/assignPrerequisites', array('id' => $this->id_path, 'id_item' => $this->id_item)), array(
			'class'            => 'popup-handler',
			'data-modal-title' => Yii::t('standard', '_PREREQUISITES'),
			'data-modal-class' => 'curricula-assign-prerequisites',
			'data-after-send'  => 'curriculaPrerequisitesAfterSubmit(data)',
		));
	}




	protected static $_cache_pu_courses = array();

	protected static function getPowerUserCourses($idPuser) {
		if (!isset(self::$_cache_pu_courses[(int)$idPuser])) {
			self::$_cache_pu_courses[(int)$idPuser] = array();
			$criteria = new CDbCriteria();
			$criteria->addCondition("puser_id = :puser_id");
			$criteria->params[':puser_id'] = $idPuser;
			$list = CoreUserPuCourse::model()->findAll($criteria);
			if (!empty($list)) {
				foreach ($list as $item) {
					self::$_cache_pu_courses[(int)$idPuser][] = (int)$item->course_id;
				}
			}
		}
		return self::$_cache_pu_courses[(int)$idPuser];
	}

	public static function isPowerUserCourse($idPuser, $idCourse) {
		return in_array((int)$idCourse, self::getPowerUserCourses($idPuser));
	}



	protected static $_cache_assigned_courses = array();

	/**
	 * Check if a course is assigned to some learning plans. Internal cache is used in case this function is called multiple times
	 * @param $course integer|LearningCourse a course ID or AR
	 * @return bool whether a course is assigned to some learning plans
	 */
	public static function isCourseAssignedToLearningPaths($course) {

		$idCourse = false;

		//validate input, make sure to have a valid course id
		if (is_numeric($course) && (int)$course > 0) {
			$idCourse = (int)$course;
		}
		if (is_object($course) && get_class($course) == 'LearningCourse') {
			$idCourse = $course->idCourse;
		}
		if ($idCourse === false) {
			return false;
		}

		if (!isset(self::$_cache_assigned_courses[$idCourse])) {
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".LearningCoursepathCourses::model()->tableName()." WHERE id_item = :id_course");
			$count = $cmd->queryScalar(array(':id_course' => $idCourse));
			self::$_cache_assigned_courses[$idCourse] = ($count > 0);
		}

		return self::$_cache_assigned_courses[$idCourse];
	}



}