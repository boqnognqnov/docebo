<?php

/**
 * This is the model class for table "learning_category".
 *
 * The followings are the available columns in table 'learning_category':
 * @property integer          $idCategory
 * @property string           $lang_code
 * @property string           $translation
 *
 * The followings are the available model relations:
 * @property LearningCourse[] $learningCourses
 */
class LearningCourseCategory extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningCourseCategory the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
				'idCategory',
				'numerical',
				'integerOnly' => true
			),
			array(
				'lang_code',
				'length',
				'max' => 50
			),
			array(
				'translation',
				'length',
				'max' => 255
			),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'idCategory, lang_code, translation',
				'safe',
				'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courses' => array(
				self::HAS_MANY,
				'LearningCourse',
				'idCategory'
			),
			'coursesCategoryTree' => array(
				self::HAS_ONE,
				'LearningCourseCategoryTree',
				'idCategory'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCategory' => 'Id Category',
			'lang_code' => 'Lang Code',
			'translation' => Yii::t('standard', 'Course Category'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idCategory', $this->idCategory);
		$criteria->compare('idParent', $this->idParent);
		$criteria->compare('lev', $this->lev);
		$criteria->compare('path', $this->path, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('iLeft', $this->iLeft);
		$criteria->compare('iRight', $this->iRight);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}


	/**
	 * Category name, the last child of the 'path':  Cat1/Cat2/Cat3 => Cat3
	 *
	 * @return string
	 */
	public function getName()
	{
		$path = $this->path;

		$result = '';
		if (!empty($path)) {
			$elements = explode('/', $path);
			$result = $elements[count($elements) - 1];
		}

		return $result;

	}

	/**
	 * Creates tree structure from DB data
	 * @param $data
	 * @return array
	 */
	private static function filterOutEmptySubtrees($data) {

		// Get courses to exclude
		$excludedCourses = array();
		$event = new DEvent(new stdClass(), array('excludedCourses' => &$excludedCourses));
		Yii::app()->event->raise('OnCatalogQueryCourses', $event);

		// Calculate category ids with at least one course assigned
		$nonEmptyCategories = Yii::app()->db->createCommand()
			->select("count(*) as courses_count, c.idCategory")
			->from("learning_course c")
			->where('c.idCategory > 0')
			->group('idCategory');

		if(!empty($excludedCourses))
			$nonEmptyCategories->andWhere("c.idCourse NOT IN ('".implode("','", $excludedCourses)."')");

		$nonEmptyCategories = $nonEmptyCategories->queryAll();

		$categoryCounters = array();
		foreach ($nonEmptyCategories as $_row)
			$categoryCounters[$_row['idCategory']] = $_row['courses_count'];

		$output = array();
		if (!empty($data)) {
			$rootNodeId = Yii::app()->getDb()->createCommand()
				->select('idCategory')
				->from(LearningCourseCategoryTree::model()->tableName())
				->order('iLeft ASC')
				->queryScalar();

			$createTree = function ($sourceData, $left = 0, $right = null) use (&$createTree, $rootNodeId, $categoryCounters, &$output) {
				$subtree = array();
				foreach ($sourceData as $item) {
					$isDescendant = ($item['iLeft'] > $left) && (is_null($right) || $item['iRight'] < $right);
					if ($isDescendant) {
						$left = $item['iRight'];
						$subtotal = isset($categoryCounters[$item['idCategory']]) ? $categoryCounters[$item['idCategory']] : 0;
						$children = array();
						$output[] = $item;
						if (($item['iRight'] - $item['iLeft']) > 1) // If this node is not a leaf, call for its children
							$children = $createTree($sourceData, $item['iLeft'], $item['iRight']);

						foreach($children as $child)
							$subtotal += $child['subtotal'];

						$item['subtotal'] = $subtotal;
						if(($subtotal == 0) && $item['idCategory'] != $rootNodeId)
							array_pop($output);

						$subtree[] = $item;
					}
				}

				return $subtree;
			};

			$createTree($data);
		}

		return $output;
	}

	/**
	 * Generate array for dropdownlist usage - with subcat spacing
	 * @return array
	 */
	public static function getCategoriesList($language = false, $separator = false, $filterEmpty = false)
	{
		//validate and process inputs
		if (empty($language)) {
			$browserCode = Yii::app()->getLanguage();
			$language = Lang::getCodeByBrowserCode($browserCode);
		}

		if (empty($separator))
			$separator = '&nbsp;&nbsp;';

		//begin calculating output
		$output = array();

		$categories = Yii::app()->db->createCommand()
			->select("ct.*, if (ISNULL(NULLIF(c.translation,'')), cd.translation, c.translation) as translation, c.lang_code")
			->from("learning_category_tree ct")
			->leftJoin("learning_category c", "c.idCategory = ct.idCategory AND c.lang_code = :lang_code", array(':lang_code' => $language))
			->leftJoin("learning_category cd", "cd.idCategory = ct.idCategory AND cd.lang_code = :default_lang_code", array(':default_lang_code' => CoreLangLanguage::getDefaultLanguage()))
			->order("c.translation ASC");

		if (Yii::app()->user->getIsAdmin()) {
			$criteria = new CDbCriteria;
			$criteria->condition = 'powerUserManagers.idst = :idst';
			$criteria->params[':idst'] = Yii::app()->user->id;
			$criteria->group = 't.idCategory';
			$res = LearningCourse::model()->with('powerUserManagers')->findAll($criteria);

			$_categories = array();

			$cac = Yii::app()->db->createCommand()
				->select('id_entry, type_of_entry')
				->from(CoreAdminCourse::model()->tableName())
				->where('idst_user = :idst_user', array('idst_user' => Yii::app()->user->id))
				->queryAll();

			foreach($cac as $adminCourse){

				if($adminCourse['type_of_entry'] !== CoreAdminCourse::TYPE_CATEGORY && $adminCourse['type_of_entry'] !== CoreAdminCourse::TYPE_CATEGORY_DESC){
					continue;
				}

				if(in_array($adminCourse['id_entry'], $_categories)){
					continue;
				}

				$_categories[] = $adminCourse['id_entry'];
				$_subcat = LearningCourseCategoryTree::model()->findByPk($adminCourse['id_entry']);

				if($adminCourse['type_of_entry'] === CoreAdminCourse::TYPE_CATEGORY_DESC){
					$_subCats1 = Yii::app()->db->createCommand()
						->select('idCategory')
						->from(LearningCourseCategoryTree::model()->tableName().' t')
						->where('t.lev > :lev AND t.iLeft > :iLeft AND t.iRight < :iRight', array(
							':lev' => $_subcat->lev,
							':iLeft' => $_subcat->iLeft,
							':iRight' => $_subcat->iRight,
						))
						->queryAll();

					foreach ($_subCats1 as $_row1) {
						$_categories[] = $_row1['idCategory'];
					}
				}

				$_subCats = Yii::app()->db->createCommand()
					->select('idCategory')
					->from(LearningCourseCategoryTree::model()->tableName().' t')
					->where('t.lev < :lev AND t.iLeft < :iLeft AND t.iRight > :iRight', array(
						':lev' => $_subcat->lev,
						':iLeft' => $_subcat->iLeft,
						':iRight' => $_subcat->iRight,
					))
					->queryAll();

				foreach ($_subCats as $_row) {
					$_categories[] = $_row['idCategory'];
				}
			}

			foreach ($res as $_row) {
				$_categories[] = $_row->idCategory;
				$_subcat = LearningCourseCategoryTree::model()->findByPk($_row->idCategory);

				$_subCats = Yii::app()->db->createCommand()
					->select('idCategory')
					->from(LearningCourseCategoryTree::model()->tableName().' t')
					->where('t.lev < :lev AND t.iLeft < :iLeft AND t.iRight > :iRight', array(
						':lev' => $_subcat->lev,
						':iLeft' => $_subcat->iLeft,
						':iRight' => $_subcat->iRight,
					))
					->queryAll();

				foreach ($_subCats as $_row)
					$_categories[] = $_row['idCategory'];
			}

			if (!empty($_categories))
				$categories = $categories->andWhere('ct.idCategory IN (' . join(',', $_categories) . ')');
		}

		$categories = $categories->queryAll();
		$categories = LearningCourseCategoryTree::arrangeTreeAlphabetical($categories);

		//Filter out empty Subtrees or not, depending on the view's request
		if($filterEmpty)
			$categories = self::filterOutEmptySubtrees($categories);

		foreach ($categories as $category) {
			$prefix = '';
			if (!empty($separator)) {
				for ($i = 1; $i < $category['lev']; $i++) {
					$prefix .= $separator;
				}
			}
			$output[(int)$category['idCategory']] = $prefix . $category['translation'];
		}

		return $output;
	}

	public static function buildFancyTreeCategoriesData()
	{
		$rootNodeId = Yii::app()->getDb()->createCommand()
			->select('idCategory')
			->from(LearningCourseCategoryTree::model()->tableName())
			->order('iLeft ASC')
			->queryScalar();

		$createTree = function ($sourceData, $left = 0, $right = null, $lev = 0) use (&$createTree, $rootNodeId) {

			$tree = array();
			foreach ($sourceData as $idOrg => $item) {

				// The list of nodes is sorted by iLeft. We add nodes to tree not only if it is
				// an immediate child, but if it is just a descendant of the calling node.
				// This is to handle the scenario when intermediate nodes are NOT available.
				// This must be heavily tested, please
				$isDescendant = (($item['iLeft'] > $left) && (is_null($right) || $item['iRight'] < $right)  && (($lev + 1) == $item['lev']));

				if ($isDescendant) {
					$element = array();
					$element['folder'] = false;
					$element['title'] = $item['translation'];
					$element['key'] = $item['idCategory'];
					$element['data'] = $item;
					$element['expanded'] = $rootNodeId && $item['idCategory'] == $rootNodeId ? true : false;
					$element['hideCheckbox'] = false;//(isset($item['unselectable']) && $item['unselectable'] == true) ? true : false;

					$children = null;

					// If this node is not a leaf, call for its children
					if (($item['iRight'] - $item['iLeft']) > 1) {
						$children = $createTree($sourceData, $item['iLeft'], $item['iRight'], $item['lev']);
						$element['folder'] = true;
					}

					$element['children'] = $children;
					$tree[] = $element;

				}
			}
			return $tree;
		};

		$tmpCategories = Yii::app()->getDb()->createCommand()
			->select("t.*, if (ISNULL(NULLIF(c.translation,'')), cd.translation, c.translation) as translation, c.lang_code")
			->from(LearningCourseCategoryTree::model()->tableName() . ' t')
			->leftJoin(LearningCourseCategory::model()->tableName() . ' c', 'c.idCategory=t.idCategory AND c.lang_code=:lang', array(':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())))
			->leftJoin("learning_category cd", "cd.idCategory = t.idCategory AND cd.lang_code = :default_lang_code", array(':default_lang_code' => CoreLangLanguage::getDefaultLanguage()))
			->order('c.translation')
			->queryAll();

		return $createTree(LearningCourseCategoryTree::arrangeTreeAlphabetical($tmpCategories));
	}

    public function afterSave(){
        $browserCode = Yii::app()->getLanguage();
        $language = Lang::getCodeByBrowserCode($browserCode);

        //if it is an update operation and we need to do it just once for one language
        if(($this->scenario == 'update') && !empty($this->lang_code) && ($this->lang_code  == $language)) {
            Yii::app()->event->raise('UpdateCategory', new DEvent($this, array(
                'category' => $this
            )));
        }

		// Event raised for YnY app to hook on to
		//raise event only after save item with current language
		if(!empty($this->lang_code) && ($this->lang_code == $language)) {
			Yii::app()->event->raise('CourseCategoryAfterSave', new DEvent($this, array(
				'category' => $this
			)));
		}

        self::flushTreeCache();

        return parent::afterSave();
    }

    /**
     * @param null $categories
     * @return string
     */
    public static function getCoursesListFromCategories( $categories = null, $types = false ) {

        $idCourses = array();
        if ( is_array($categories) ) {

            $command = Yii::app()->db->createCommand()
                ->select('idCourse')
				->from(LearningCourse::model()->tableName())
				->where(array('in', 'idCategory', $categories));

            // Filter by course type, if any
            if (is_array($types) && !empty($types)) {
                $command->andWhere(array('IN', 'course_type', $types));
            }

            // Get result
            $tempRes = $command->queryAll();

			if ( is_array($tempRes) && !empty($tempRes)) {
				foreach ($tempRes as $k => $v) {
					$idCourses[] = $v['idCourse'];
				}
				$res = $idCourses;
			} else {
				$res = array();
			}

			return $res;
        } else {
            return false;
        }

    }

    /**
     * Flush category tree cache used in Catalog Dashlet
     */
    public static function flushTreeCache() {
        if (isset(Yii::app()->cache_categorytree)) {
            Yii::app()->cache_categorytree->flush();
        }
    }


}
