<?php

/**
 * This is the model class for table "app7020_channel_experts".
 *
 * The followings are the available columns in table 'app7020_channel_experts':
 * @property integer $id
 * @property integer $idChannel
 * @property integer $idExpert
 *
 * The followings are the available model relations:
 * @property App7020Channels $channel
 * @property App7020Experts $idExpert0
 */
class App7020ChannelExperts extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_channel_experts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idChannel, idExpert', 'required'),
			array('idChannel, idExpert', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idChannel, idExpert', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'channel' => array(self::BELONGS_TO, 'App7020Channels', 'idChannel'),
			'expert' => array(self::BELONGS_TO, 'App7020Experts', 'idExpert'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idChannel' => 'Id Channel',
			'idExpert' => 'Id Expert',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idChannel', $this->idChannel);
		$criteria->compare('idExpert', $this->idExpert);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ChannelExperts the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Returns an array of channel models assigned to a specific expert
	 *
	 * @param integer $expertId
	 * @param boolean $withChilds
	 * @return array array of topic models
	 */
	public static function getExpertsChannels($idExpert, $customConfig = array()) {
		if ($customConfig['search_input']) {
			$searchInput = $customConfig['search_input'];
		} else {
			$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		}

		$channels = self::model()->findAllByAttributes(array('idExpert' => $idExpert));

		$channelsModels = array();
		foreach ($channels as $value) {
			$model = App7020Channels::model()->findByPk($value->idChannel);
			$modelArray['id'] = $model->id;
			$modelArray['translation'] = $model->translation();
			$channelsModels[] = $modelArray;
		}

		if ($searchInput) {

			$tmpArray = array();
			foreach ($channelsModels as $key => $value) {

				if (stripos($value['translation']['name'], $searchInput) !== false || stripos($value['translation']['description'], $searchInput) !== false) {
					$tmpArray[] = $channelsModels[$key];
				}
			}
			$channelsModels = $tmpArray;
		}


		if (!isset($customCofig['pagination'])) {
			$customCofig['pagination'] = array('pageSize' => 10);
		}
		$dataProvider = new CArrayDataProvider($channelsModels, $customCofig);
		return $dataProvider;
	}

	/**
	 * Make SQL Query and Configurate ComboListView
	 * @return \CSqlDataProvider
	 */
	public function sqlDataProvider($customConfig = array()) {
		$idChannel = (int) Yii::app()->request->getParam('id', false);
		if ($customConfig['search_input']) {
			$searchInput = $customConfig['search_input'];
		} else {
			$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		}

		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020ChannelExperts::model()->tableName() . ' ex');
		$commandBase->leftJoin(App7020Experts::model()->tableName() . ' e', 'ex.idExpert = e.id');
		$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'e.idUser = u.idst');
		$commandBase->andWhere('ex.idChannel = :idChannel', array(':idChannel' => $idChannel));

		if ($searchInput != null) {
			$commandBase->andWhere("CONCAT(COALESCE(u.firstname,''), ' ', COALESCE(u.lastname,'')) LIKE :search", array(':search' => '%' . $searchInput . '%'));
		}

		$commandData = clone $commandBase;
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(ex.idExpert)');
		$numRecords = $commandCounter->queryScalar();

		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			//
		);
		$sort->defaultOrder = array(
			'idExpert' => CSort::SORT_DESC,
		);

		$pageSize = Settings::get('elements_per_page', 10);
		if (!empty($customConfig['elements_per_page'])) {
			$pageSize = $customConfig['elements_per_page'];
		}

		$config = array(
			'totalItemCount' => $numRecords,
			'pagination' => array('pageSize' => $pageSize),
			'keyField' => 'idExpert',
			'sort' => $sort,
		);

		$dataProvider = new CSqlDataProvider($commandData, $config);
		return $dataProvider;
	}

	public static function getExpertCountsByChannelId($id) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from(App7020ChannelExperts::model()->tableName() . ' ex');
		$commandBase->leftJoin(App7020Experts::model()->tableName() . ' e', 'ex.idExpert = e.id');
		$commandBase->leftJoin(CoreUser::model()->tableName() . ' u', 'e.idUser = u.idst');
		$commandBase->andWhere('ex.idChannel = :idChannel', array(':idChannel' => $id));

		$commandCounter = clone $commandBase;
		$commandCounter->select('count(ex.idExpert)');
		$numRecords = $commandCounter->queryScalar();

		return $numRecords;
	}

	/**
	 * Return all assignet to user channel ID-s 
	 * @return boolean|array
	 */
	public static function getAssignedChannelKeysToUserAsExpert($idExpert = false) {
		if (!$idExpert) {
			$idExpert = Yii::app()->user->idst;
		}
		$sqlCommand = Yii::app()->db->createCommand();
		try {
			$sqlCommand->select('idChannel');
			$sqlCommand->from(self::model()->tableName() . " t");
			$sqlCommand->join(App7020Experts::model()->tableName() . ' e', 't.idExpert = e.id');
			$sqlCommand->where("idUser = :idUser", array(
				":idUser" => (int) $idExpert)
			);

			$sqlCommand->group('idChannel');
			return $sqlCommand->queryColumn();
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * get all experts
	 * @return array with IDs of all experts
	 */
	public static function getAllExperts() {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("DISTINCT(idExpert) as idUser");
		$dbCommand->from(App7020ChannelExperts::model()->tableName());
		return $dbCommand->queryAll();
	}

	/**
	 * get all experts for any channel
	 * @return array with IDs of all experts
	 */
	public static function getAllChannelExperts($idAsset = false) {


		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("ae.idUser");
		$dbCommand->join(App7020Experts::model()->tableName() . '   ae', 'ace.idExpert = ae.id ');
		$dbCommand->join(App7020ChannelAssets::model()->tableName() . ' ac', ' ac.idChannel = ace.idChannel ');
		$dbCommand->from(App7020ChannelExperts::model()->tableName() . ' ace');
		$dbCommand->where("idAsset = :idAsset", array(
			":idAsset" => (int) $idAsset)
		);
		$dbCommand->group('idExpert');
		return $dbCommand->queryColumn();
	}

	/**
	 * Get All Experts per Channel
	 * @param int $channelId
	 * @return array
	 */
	public static function getChannelExpertIds($channelId, $asArray = true) {
		$command = Yii::app()->db->createCommand();
		$command->select('cu.* , CONCAT(cu.firstname," ",cu.lastname) as fullname,TRIM(LEADING "/" FROM userid) as username');
		$command->from(self::model()->tableName() . ' ce');
		$command->join(App7020Experts::model()->tableName() . ' e', 'ce.idExpert = e.id');
		$command->join(CoreUser::model()->tableName() . ' cu', 'e.idUser = cu.idst');
		$command->andWhere('idChannel=:idChannel');
		$channelExperts = $command->queryAll(true, array(
			':idChannel' => $channelId
		));

		if($asArray){
			return $channelExperts;
		} else {
			$customCofig['pagination'] = array('pageSize' => 10);
			$dataProvider = new CArrayDataProvider($channelExperts, $customCofig);
			return $dataProvider;
		}
		
	}

	/**
	 * Get All Count of experts by given list of channels
	 * @param array $channels
	 * @return array
	 */
	public static function countExpertsPerChannelList(array $channels) {
		$experts = array();
		foreach ($channels as $channel) {
			$channelExperts = App7020ChannelExperts::getChannelExpertIds($channel["id"]);
			foreach ($channelExperts as $expert) {
				$experts[] = $expert["idst"];
			}
		}
		$experts = count(array_unique($experts));
		return $experts;
	}

}
