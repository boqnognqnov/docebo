<?php

/**
 * This is the model class for table "webinar_session_date_recording".
 *
 * The followings are the available columns in table 'webinar_session_date_recording':
 * @property integer $id_session
 * @property string $day
 * @property integer $id_course
 * @property integer $type
 * @property string $path
 * @property integer $status
 * @property integer $progress
 *
 * The followings are the available model relations:
 * @property WebinarSessionDate $idSession
 */
class WebinarSessionDateRecording extends CActiveRecord
{

	const RECORDING_TYPE_NONE = 0;
	const RECORDING_TYPE_API_GRAB = 1;
	const RECORDING_TYPE_UPLOAD = 2;
	const RECORDING_TYPE_WEBLINK = 3;
	const RECORDING_TYPE_API_LINK = 4;

	const RECORDING_STATUS_NON_EXISTANT = 0;
	const RECORDING_STATUS_DOWNLOADING = 1;
	const RECORDING_STATUS_UPLOADING = 2;
	const RECORDING_STATUS_CONVERTING = 3;
	const RECORDING_STATUS_FAILED = 4;
	const RECORDING_STATUS_COMPLETED = 5;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_session_date_recording';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_session, day, id_course, type, path, status', 'required'),
			array('id_session, id_course, type, status, progress', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, day, id_course, type, path, status, progress', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idSession' => array(self::BELONGS_TO, 'WebinarSessionDate', 'id_session')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'day' => 'Day',
			'id_course' => 'Id Course',
			'type' => 'Type',
			'path' => 'Path',
			'status' => 'Status',
			'progress' => 'Progress',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('type',$this->type);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('progress',$this->progress);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarSessionDateRecording the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



}
