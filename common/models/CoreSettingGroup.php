<?php

/**
 * This is the model class for table "core_setting_group".
 *
 * The followings are the available columns in table 'core_setting_group':
 * @property string $path_name
 * @property integer $idst
 * @property string $value
 */
class CoreSettingGroup extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreSettingGroup the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_setting_group';
    }

    public function init() {
        $this->value = 'off';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('value', 'required'),
            array('idst', 'numerical', 'integerOnly'=>true),
            array('path_name', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('path_name, idst, value', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'path_name' => 'Path Name',
            'idst' => 'Idst',
            'value' => 'Value',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('path_name',$this->path_name,true);
        $criteria->compare('idst',$this->idst);
        $criteria->compare('value',$this->value,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }

	/**
	 * Return list of group's settings, optionally filtered by rule path name(s).
	 * If none found, returns a default list with default values set.
	 *
	 * @param number $id Group Id
	 *
	 * @param array  $pathFilter List of rule pathnames to filter the result
	 *
     * @param array @selectedSettings List of pathnames to be auto checked
     *
	 * @return array
	 */
	public static function initSettings($id = null, $pathFilter = array(), $selectedSettings = array())
	{

		$criteria = new CDbCriteria();
		$criteria->compare('t.idst', $id);
	    $criteria->index = 'path_name';
		$criteria->order = 'path_name DESC';

		if (is_array($pathFilter) && (count($pathFilter) > 0)) {
			$criteria->addInCondition('t.path_name', $pathFilter);
		}

	    $settings = $id ? self::model()->findAll($criteria) : array();

		$defaultSettings = array(
			'direct_course_subscribe',
			'direct_user_insert',
            'allow_only_student_enrollments',
	        'allow_buy_seats',
			'allow_power_users_to_skip_mandatory_fields',
			'view_assigned_user_certificates_only',
		);

		$tmpSettings = array();
		foreach ($defaultSettings as $pathName) {
			$adminRuleKey = 'admin_rules.' . $pathName;
			if (!isset($settings[$adminRuleKey])) {
				$setting = new CoreSettingGroup;
				$setting->path_name = $adminRuleKey;
				// if the setting would be set to 'on', then in the case of adding new settings,
				// for existing configurations the checkboxes will appear checked although
				// the setting is not actually saved

                $setting->value = array_key_exists($adminRuleKey, $selectedSettings) ? 'on' : 'off';
                $tmpSettings[$adminRuleKey] = $setting;
			} else {
				$tmpSettings[$adminRuleKey] = $settings[$adminRuleKey];
			}
		}

		return $tmpSettings;
	}

}