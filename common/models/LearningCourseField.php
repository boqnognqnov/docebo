<?php

/**
 * This is the model class for table "learning_course_field".
 *
 * The followings are the available columns in table 'learning_course_field':
 * @property integer $id_field
 * @property string $type
 * @property integer $sequence
 * @property integer $invisible_to_user
 * @property string $settings
 *
 * The followings are the available model relations:
 * @property LearningCourseFieldDropdown[] $learningCourseFieldDropdowns
 * @property LearningCourseFieldTranslation[] $learningCourseFieldTranslations
 *
 * @property string $courseEntry
 * @property string $lang_code
 * @property LearningCourse $course
 */
class LearningCourseField extends CActiveRecord
{
	/**
	 * Array of translations for this field in all available languages
	 */
	private $fieldTranslations = array();

	public $maxSequence;
	public $courseEntry = '';
	public $lang_code;
	public $course = null;

	/* FIELD TYPES */
	const TYPE_FIELD_DATE = 'date';
	const TYPE_FIELD_DROPDOWN = 'dropdown';
	const TYPE_FIELD_TEXTFIELD = 'textfield';
	const TYPE_FIELD_TEXTAREA = 'textarea';
	const TYPE_FIELD_ACCREDIFRAME = 'accrediframe';

	/**
	 * Mapping between field types and value column type
	 * @var array
	 */
	private $_valueColumnType = array(
		self::TYPE_FIELD_DATE => 'DATE',
		self::TYPE_FIELD_DROPDOWN => 'SMALLINT',
		self::TYPE_FIELD_TEXTAREA => 'TEXT',
		self::TYPE_FIELD_TEXTFIELD => 'VARCHAR(255)',
	    self::TYPE_FIELD_ACCREDIFRAME => 'VARCHAR(2048)',
	    
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('sequence, invisible_to_user', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>60),
			array('id_field, type, sequence, invisible_to_user', 'safe', 'on'=>'search'),
			array('id_field, type, sequence, invisible_to_user, settings, mandatory', 'safe', 'on'=>'clone')
		);
	}
	
	public function renderFilterField(){
		return '';
	}

	public function getSubclassedInstance() {
		$subclassType = $this->type ? 'CourseField' . ucfirst($this->type) : 'LearningCourseField';
		$instance = new $subclassType('clone');
		$instance->attributes = $this->attributes;
		return $instance;
	}

	public function renderFieldSettings() {
		return '';
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'learningCourseFieldDropdowns' => array(self::HAS_MANY, 'LearningCourseFieldDropdown', 'id_field'),
			'learningCourseFieldTranslations' => array(self::HAS_MANY, 'LearningCourseFieldTranslation', 'id_field'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_field'          => 'Id Field',
			'type'              => Yii::t('standard', '_TYPE'),
			'sequence'          => Yii::t('manmenu', '_ORDER'),
			'invisible_to_user' => Yii::t('standard', 'Show in Course Catalog and Course Description widget')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('invisible_to_user',$this->invisible_to_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseField the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave () {
		// Load all language codes
		$activeLanguagesList = CoreLangLanguage::getActiveLanguages();

		// 1. Translations handling
		if($this->getIsNewRecord()) {

			// New record -> create all translations
			$sql = "INSERT INTO ".LearningCourseFieldTranslation::model()->tableName()." (id_field, lang_code, translation) VALUES ";
			$rows= array();
			foreach($activeLanguagesList as $key => $language)
				$rows[] = "(".$this->id_field.",'".$key."', ".Yii::app()->db->quoteValue(isset($this->fieldTranslations[$key]) ? $this->fieldTranslations[$key] : '').")";
			$sql .= implode(", ", $rows);
			Yii::app()->db->createCommand($sql)->execute();

		} else if(!empty($this->fieldTranslations)) {

			// Existing record -> update translations only if set in fieldTranslations
			$sql = '';
			foreach($this->fieldTranslations as $key => $translation)
				$sql .= "UPDATE " . LearningCourseFieldTranslation::model()->tableName() . " SET translation = " . Yii::app()->db->quoteValue($translation) .
					" WHERE id_field = " . $this->id_field . " AND lang_code = '" . $key . "'";
			if($sql)
				Yii::app()->db->createCommand($sql)->execute();
		}

		// 2. Value table handling
		if($this->getIsNewRecord()) {
			$event = new DEvent();
			Yii::app()->event->raise('BeforeCourseAdditionalFieldAfterSave', $event);
                    
			if($event->return_value)
				$this->_valueColumnType = array_merge($this->_valueColumnType, $event->return_value);

			// Add new column to the value table
			$sql = "ALTER TABLE ".LearningCourseFieldValue::model()->tableName()."
					CHARACTER SET = utf8 , COLLATE = utf8_general_ci,
  					ADD COLUMN field_".$this->id_field." ".$this->_valueColumnType[$this->type]." DEFAULT NULL";

			try {
				// Syntax only available for MYSQL >= 5.6
				Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
			} catch(CDbException $e) {
				// Well, looks like MYSQL version doesn't like this syntax
				Yii::app()->db->createCommand($sql)->execute();
			}
		}

        parent::afterSave();
        
        // Raise an event and tell the world we've just created a new course additional field
        Yii::app()->event->raise(EventManager::EVENT_AFTER_NEW_COURSE_ADDITIONAL_FIELD_SAVED, new DEvent($this, array(
        	'field_name'	=> 'field_' . $this->id_field,
        	'field_type'	=> $this->type,
        )));
        
        
	}

    public function afterDelete() {

        // After deleting the field, remove the column from learning_course_field_value
        try {
            $sql = "ALTER TABLE ".LearningCourseFieldValue::model()->tableName()."
  					DROP COLUMN field_".$this->id_field;

            try {
                // Syntax only available for MYSQL >= 5.6
                Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
            } catch(CDbException $e) {
                // Well, looks like MYSQL version doesn't like this syntax
                Yii::app()->db->createCommand($sql)->execute();
            }
		$allFilters = LearningReportFilter::model()->findAll();
		foreach($allFilters as $filter) {
			$new_filter = new stdClass();
			$data = json_decode($filter->filter_data);
			if($data->fields->course) {
				foreach($data->fields->course as $key=>$value) {
					if($key != $this->id_field) {
					$new_filter->$key = $value;
					}
				}
				$data->fields->course = $new_filter;
				$filter->filter_data = json_encode($data);
				if($filter->save()) {
				}
			}	
		}
            parent::afterDelete();
        } catch(Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
        
        // Raise an event and tell the world we've just deleted an course additional field
        Yii::app()->event->raise(EventManager::EVENT_AFTER_COURSE_ADDITIONAL_FIELD_DELETED, new DEvent($this, array(
        		'field_name'	=> 'field_' . $this->id_field,
        		'field_type'	=> $this->type,
        )));
        
        
    }

	/**
	 * Returns the next sequence number
	 * @return int
	 */
	public function getNextSequence()
	{
		$criteria=new CDbCriteria;
		$criteria->select = 'max(sequence) as maxSequence';
		$res = $this->model()->find($criteria);
		if(is_null($res->maxSequence))
			$max_seq = 0;
		else
			$max_seq = $res->maxSequence + 1;

		return $max_seq;
	}

	/**
	 * Sets the fields translations to be saved in afterSave
	 * @param $translations
	 */
	public function setFieldTranslations($translations) {
		if(!empty($translations))
			$this->fieldTranslations = $translations;
	}

    /**
     * @return CArrayDataProvider
     */
    public function getAdditionalCourseFieldsDataProvider()
    {
        $params = Yii::app()->request->getParam('LearningCourseFieldTranslation');
        $translation = $params['translation'];
        $criteria = new CDbCriteria(array('order' => 'sequence asc'));
        if ( $translation ) {
            $criteria->join = 'LEFT JOIN learning_course_field_translation l ON t.id_field = l.id_field';
            $criteria->addSearchCondition('translation', $translation);
        }

        $rawData = $this->findAll($criteria);
        $defaultElements = 10;
        $coreSetting = CoreSetting::model()->findByAttributes(array('param_name' => 'elements_per_page'));
        $elementsPerPage = $coreSetting->param_value ? $coreSetting->param_value : $defaultElements;
        /*=- If there is not set elements per page in Advance Setting default is 10 -=*/
        return new CArrayDataProvider($rawData, array(
            'keyField'      => 'id_field',
            'pagination' => array(
                'pageSize' => $elementsPerPage,
            ),
        ));
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return self::getCategoryNameForType($this->type);
    }

	/**
	 * Generates the course field label (localized)
	 * @param $type
	 * @return string
	 */
	public static function getCategoryNameForType($type) {
		if(!in_array($type, LearningCourseField::getCourseFieldsTypes())) {
			$event = new DEvent("self", array('type' => $type));
			Yii::app()->event->raise('GetCourseFieldLabel', $event);
			if($event->return_value['label'])
				return $event->return_value['label'];
			else
				return 'Unknown field type';
		} else {
			// Here we try to reuse the "freetext" translation
			return Yii::t('field', '_' . strtoupper(($type == self::TYPE_FIELD_TEXTAREA) ? 'freetext' : $type));
		}
	}

    /**
     * @return array
     */
    public static function getCourseFieldsTypes()
    {
        $fieldTypes = array(self::TYPE_FIELD_DATE, self::TYPE_FIELD_DROPDOWN, self::TYPE_FIELD_TEXTAREA, self::TYPE_FIELD_TEXTFIELD, self::TYPE_FIELD_ACCREDIFRAME);
        return $fieldTypes;
    }

    /**
     * @param null $id_field
     * @return mixed
     */
    public function getAdditionalFieldsTranslationName($id_field=null)
    {
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        static $translations = array();

        if(!isset($translations[$id_field])) {
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => $lang,
                ))
                ->queryScalar();
        }

        if ( empty($translations[$id_field]) || $translations[$id_field] === '') {
            /* If there is no translation for this $id_field apply the default(english) */
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => Settings::get('default_language', 'english'),
                ))
                ->queryScalar();
			// if there is no translation for the default language also, we get the first translated language
			if(empty($translations[$id_field]) || $translations[$id_field] === ''){
				$translations[$id_field] = Yii::app()->getDb()->createCommand()
					->select('translation')
					->from(LearningCourseFieldTranslation::model()->tableName())
					->where('id_field=:id_field', array(':id_field' => $id_field))
					->andWhere('translation <> ""')
					->queryScalar();
			}
        }

        return $translations[$id_field];
    }


    public function getTranslation($idField=false, $lang_code = false, $returnDefault = true){
        $lang_code = !$lang_code ? Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) : $lang_code;
        $fid = ($idField)? $idField : $this->id_field;
        $translation = Yii::app()->db->createCommand()
            ->select('translation')
            ->from(LearningCourseFieldTranslation::model()->tableName().' ft')
            ->where('id_field = '.$fid.' AND lang_code = "'.$lang_code.'"')
            ->queryScalar();
        if(!$translation && $returnDefault){
            $translation = Yii::app()->db->createCommand()
                ->select('translation')
                ->from(LearningCourseFieldTranslation::model()->tableName().' ft')
                ->where('id_field = '.$fid.' AND lang_code = "'.Settings::get('default_language').'"')
                ->queryScalar();

			if(empty($translation) || $translation == ''){
				$translation = Yii::app()->db->createCommand()
					->select('translation')
					->from(LearningCourseFieldTranslation::model()->tableName().' ft')
					->where('id_field = '.$fid.' AND translation <> ""')
					->queryScalar();
			}
        }
        return $translation;
    }

	public function getConditionsOptions() {
		return array(
			'contains' => Yii::t('standard','_CONTAINS'),
			'equal' => Yii::t('standard','_EQUAL'),
			'not-equal' => Yii::t('standard','_NOT_EQUAL'),
		);
	}

	public function getTextareaConditionsOptions() {
		return array(
			'contains' => Yii::t('standard','_CONTAINS'),
			'not-contains' => Yii::t('standard','_NOT_CONTAINS')
		);
	}

	public function validateFieldEntry() {
		if ($this->mandatory) {
			if (empty($this->courseEntry)) {
				$this->addError('courseEntry', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
			}
		}
	}

	public function renderValue($courseEntry, $courseEntryModel=null) {
		$className = 'CourseField' . ucfirst($this->type);
		$field = $className::model()->findByAttributes(array('id_field' => $this->id_field));
		return $field->renderFieldValue($courseEntry, $courseEntryModel);
	}

	public static function renderFieldValue($courseEntry, $courseEntryModel=null, $renderedWhere) {
		return '';
	}

	private function populateField() {
		$className = 'CourseField' . ucfirst($this->type);
		return $className::model()->findByAttributes(array('id_field' => $this->id_field));
	}

	public function renderFilter() {
		$field = $this->populateField();
		return $field->renderFilterField();
	}
	
	public static function hasAnyField(){
		$fieldCount = self::model()->count();
		
		return (($fieldCount > 0) ? true : false);
	}

    public function prepareDropdownData( ) {
        $data = array();
        $data[$this->id_field] = array();
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
        $dropdownModels = LearningCourseFieldDropdown::model()->findAllByAttributes(array(
            'id_field'  => $this->id_field
        ));
        $dropdownOptions = array();
        foreach ( $dropdownModels as $model ) {
            $dropdownOptions[$model->id_option] = $model->id_option;

        }
        foreach ( $activeLanguagesList as $lang_code => $language) {

            $result = Yii::app()->db->createCommand()
                ->select('*')
                ->from(LearningCourseFieldDropdownTranslations::model()->tableName())
                ->where('lang_code = :lang_code',
                    array(
                        ':lang_code'    => $lang_code))
                ->andWhere(array('in', 'id_option', $dropdownOptions))
                ->queryAll();

            $data[$this->id_field][$lang_code] = array(
                'label'     => $this->getTranslation(false, $lang_code, false),
                'options'   => array()
            );
            foreach ( $result as $res ) {
                $data[$this->id_field][$lang_code]['options'][$res['id_option']] = $res['translation'];
            }
        }

        return $data;
    }

	/**
	 * Returns a deserialized settings array for this field model
	 * @return array|mixed
	 */
	public function getSettingsArray() {
		return $this->settings ? CJSON::decode($this->settings) : array();
	}

	public function getFieldEntry($idCourse){
		$fieldClass = 'CourseField' . ucfirst($this->type);
		$courseField = $fieldClass::model()->findByPk($this->id_field);
		$tempValue = Yii::app()->getDb()->createCommand()
				->select('field_'.$this->id_field)
				->from(LearningCourseFieldValue::model()->tableName().' v')
				->where('v.id_course=:id_course', array(':id_course' => $idCourse))
				->queryScalar();
		$courseField->courseEntry = $tempValue;
		$resultValue = '';
		$resultValue = $fieldClass::renderFieldValue($courseField->courseEntry);
		return $resultValue;
	}
	
	/**
	 * Return an array of defined additional field names
	 * @return string[]
	 */
	public static function getAllFieldNames() {
		$result = array();
		$attrs = array('type' => array('textfield', 'textarea'));
		$fields = self::model()->findAllByAttributes($attrs);
		foreach ($fields as $field) {
			$result[] = "field_" . $field->id_field;
		}
		return $result;
	}
	
}
