<?php

/**
 * This is the model class for table "learning_webpage".
 *
 * The followings are the available columns in table 'learning_webpage':
 * @property integer $id
 * @property integer $sequence
 * @property integer $publish
 * @property integer $in_home
 * @property string $visible_nodes
 *
 * The followings are the available model relations:
 * @property CoreMultidomain[] $domainSettings
 * @property LearningWebpageTranslation[] $translations
 * @property LearningWebpageTranslation $translation
 */
class LearningWebpage extends CActiveRecord {
	
	public $idMultidomain;
	
	public $confirm;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningWebpage the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_webpage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, sequence, publish, in_home', 'numerical', 'integerOnly'=>true),
			array('sequence', 'length', 'max' => 999),
			array('publish', 'length', 'max' => 1),
			array('in_home', 'length', 'max' => 1),
			array('visible_nodes', 'length', 'max' => 10),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sequence, publish, in_home, visible_nodes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {

		return array(
			'translations' => array(self::HAS_MANY, 'LearningWebpageTranslation', 'page_id'),

			// Get a single model for current App language from translations table
			'translation' => array(self::HAS_ONE, 'LearningWebpageTranslation', 'page_id', 'condition' => "translation.lang_code='" .  Lang::getCodeByBrowserCode(Yii::app()->getLanguage())   ."'"),

            'brandingSettings' => array(self::MANY_MANY, 'CoreOrgChartBranding', 'learning_webpage_domainbranding(id_page, id_branding)'),
			'domainSettings' => array(self::MANY_MANY, 'CoreMultidomain', 'core_multidomain_webpage(id_multidomain, id_webpage)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'sequence' => 'Sequence',
			'publish' => 'Publish',
			'in_home' => 'In Home',
            'visible_nodes' => 'Visible Nodes',
		);
	}

	public function dataProvider($idMultidomain = false, $ignoreNodesVisibility = false) {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('publish', $this->publish);
		$criteria->compare('in_home', $this->in_home);
		if ($ignoreNodesVisibility !== true) {
        	$criteria->compare('visible_nodes',$this->visible_nodes,true);
		}

        //$criteria->with = array('brandingSettings');

		$criteria->order = 'sequence ASC';

		if ($idMultidomain) {
			$criteria->select = '*, ' . $idMultidomain . ' AS idMultidomain';
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
		));
	}

	public function getMaxSequence() {
		return Yii::app()->db->createCommand()->select('MAX(sequence)')->from($this->tablename())->queryScalar();
	}


}