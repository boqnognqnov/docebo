<?php

/**
 * This is the model class for table "webinar_tool_account".
 *
 * The followings are the available columns in table 'webinar_tool_account':
 * @property integer $id_account
 * @property string $tool
 * @property string $name
 * @property stdClass $settings
 * @property string $additional_info
 *
 * The followings are the available model relations:
 * @property WebinarSession[] $webinarSessions
 */
class WebinarToolAccount extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_tool_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tool, name, settings', 'required'),
			array('tool, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_account, tool, name, settings, additional_info', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'webinarSessions' => array(self::HAS_MANY, 'WebinarSession', 'id_tool_account'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_account' => 'Id Account',
			'tool' => 'Tool',
			'name' => 'Name',
			'settings' => 'Settings',
			'additional_info' => 'Additional info',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('tool',$this->tool,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('settings',$this->settings,true);
		$criteria->compare('additional_info',$this->additional_info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarToolAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * This method is invoked after each record is instantiated by a find method.
	 * The default implementation raises the {@link onAfterFind} event.
	 * You may override this method to do postprocessing after each newly found record is instantiated.
	 * Make sure you call the parent implementation so that the event is raised properly.
	 */
	protected function afterFind()
	{
		$this->settings = json_decode($this->settings);
		return parent::afterFind();
	}

	/**
	 * @return bool
	 */
	protected function beforeSave()
	{
		$this->settings = json_encode($this->settings);
		return parent::beforeSave();
	}

	/**
	 * Data provider for list of accounts
	 * @return CActiveDataProvider
	 */
	public function dataProvider($type) {
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->compare('t.tool', $type);

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName)
			$sortAttributes[$attributeName] = 't.' . $attributeName;

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'name desc';
		$config['pagination'] = false;

		return new CActiveDataProvider(get_class($this), $config);
	}
}
