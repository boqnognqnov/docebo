<?php

/**
 * Represents one shopping cart 'position', part of shoppingCart extension.
 *
 * Example usage:
 *        $lp_id = Yii::app()->request->getParam('id_path');
 *        $position = CourseCartPosition::model()->findByPk($lp_id);
 *        Yii::app()->shoppingCart->put($position);
 *
 * @author Dzhuneyt
 * @package shopping cart
 *
 */
class CoursepathCartPosition extends LearningCoursepath implements IECartPosition {

	/** @var mixed Additional options to save, per position */
	private $_options = NULL;

	/**  @var $_courses LearningCourse[] */
	private $_courses = array();

	// Recalculated on the fly based on the
	// number of seats requested to buy and
	// the selected courses, or if the LP can be
	// purchased in full only
	// See LearningCoursepath::getCoursepathPrice()
	private $totalPrice = 0.0;
	private $seatsToBuy = 1;


	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningCourse the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}


	/**
	 * Returns a unique shopping cart id.
	 *
	 * @see IECartPosition::getCartItemId()
	 * @return int
	 */
	public function getCartItemId() {
		return "coursepath_" . $this->id_path;
	}


	/**
	 * Returns the price of this course
	 *
	 * @see IECartPosition::getPrice()
	 * @return float
	 */
	public function getPrice() {

		// let plugins change the price
		Yii::app()->event->raise('OnRetrievingShoppingItemPrice', new DEvent($this, array('position' => &$this)));

		return $this->totalPrice;
	}

	/**
	 * Updates the cart position's price
	 *
	 * @param float $price
	 */
	public function setPrice($price) {
		$this->totalPrice = $price;
	}


	/**
	 * Set position options
	 *
	 * @param mixed $options
	 */
	public function setOptions($options) {
		$this->_options = $options;
	}

	public function setCourses($courses) {
		$this->_courses = $courses;

		$this->recalculatePrice();
	}
	public function setSeats($seatsToBuy){
		$this->seatsToBuy = (int) $seatsToBuy;

		$this->recalculatePrice();
	}

	/**
	 * Returns position options
	 *
	 * @return mixed
	 */
	public function getOptions() {
		return $this->_options;
	}

	/**
	 * Recalculate the price for this cart item
	 * based on the purchase type of this LP
	 * (course by course in sequence or full pack only)
	 * and depending on the seats to purchase
	 * Call this method whenever the courses or seats (number) is changed
	 */
	private function recalculatePrice(){
		$calculatedPrice = 0;
		if($this->purchase_type == self::PURCHASE_TYPE_FULL_PACK_ONLY){
			$calculatedPrice = $this->getCoursepathPrice();
		}elseif($this->purchase_type == self::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE){
			if($this->price && count($this->_courses) == count($this->learningCourse)){
				// LP has a FULLPACK price set
				// and the user has selected all courses
				// in LP to buy. Use the FULLPACK price
				$calculatedPrice = $this->getCoursepathPrice();
			}else {
				// Item price will equal the sum of prices
				// of the selected courses
				foreach($this->_courses as $course){
					$calculatedPrice = $calculatedPrice + floatval($course->getCoursePrice());
				}
			}
		}

		$this->setPrice($calculatedPrice);
	}

	public function getCourses() {
		return $this->_courses;
	}

	public function getImage() {
		$asset = CoreAsset::model()->findByPk($this->img);
		if($asset){
			return $asset->getUrl(CoreAsset::VARIANT_SMALL);
		}
	}
	public function getPositionName(){
		return $this->path_name;
	}
}
