<?php



/**
 * This is the model class for table "core_user_field_translations".
 *
 * @property integer $id_field
 * @property string $lang_code
 * @property string $translation
 *
 */
class CoreUserFieldTranslations extends \CActiveRecord {


	/**
	 * @inheritdoc
	 */
	public  function tableName() {
		return 'core_user_field_translation';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			['lang_code, id_field', 'required'],
			['id_field', 'numerical', 'integerOnly' => true],
		];
	}

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [];
	}

    /**
     * Inserts missing field translations
     *
     * @param array<string> $langCodes
     * @return void
     */
    public static function insertMissing(array $langCodes)
    {
        foreach ($langCodes as $code) {
            static::insertMissingTranslationForLanguage($code);
        }
    }

    /**
     * Inserts missing field translations for the specified language
     *
     * @param string $langCode
     * @return void
     */
    public static function insertMissingTranslationForLanguage($langCode)
    {
        // Join core_user_field_translation with itself, seeking ids of fields that have no translation
        // for the specified language code, inserting a new row in the table with an empty user value,
        // as this would apply for each row that has no translation for the language code
        $trTbl = '`core_user_field_translation`';
        $sql = 'INSERT INTO ' . $trTbl . ' (`id_field`, `lang_code`, `translation`) 
            (SELECT 
                DISTINCT `cuft`.`id_field`,
                :langCode AS `lang_code`,
                \'\' AS `translation`
            FROM ' . $trTbl . ' `cuft` 
                LEFT JOIN ' . $trTbl . ' `seekMissing` 
                    ON `seekMissing`.`id_field` = `cuft`.`id_field` AND `seekMissing`.`lang_code` = :langCode 
            WHERE `seekMissing`.`id_field` IS NULL)';

        /** @var CDbCommand $cmd */
        $cmd = Yii::app()->db->createCommand($sql);
        $cmd->execute(['langCode' => $langCode]);
    }

}
