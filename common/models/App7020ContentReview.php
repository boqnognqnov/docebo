<?php

/**
 * This is the model class for table "app7020_question_history".
 *
 * The followings are the available columns in table 'app7020_question_history':
 * @property integer $id
 * @property integer $idContent
 * @property integer $idUser
 * @property text $message
 * @property integer $systemMessage
 * @property integer $idTooltip
 * @property datetime $created
 * @property datetime $lastUpdated
 *
 * The followings are the available model relations:
 * @property App7020Content $content
 * @property CoreUser $user
 */
class App7020ContentReview extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_content_reviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {

		return array(
			array('idContent, idUser, systemMessage, message', 'required'),
			array('idContent, idUser, systemMessage', 'numerical', 'integerOnly' => true)
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idContent' => 'Id Content',
			'message' => 'Message',
			'idUser' => 'Id User',
			'review' => 'Review'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('idTooltip', $this->idTooltip);
		$criteria->compare('systemMessage', $this->systemMessage);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020TopicContent the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Returns all peer reviews by given content
	 * @param integer $idContent
	 * @param number $idFrom
	 * @param number $limit
	 * @param string $order
	 * @return array
	 */
	public static function getPeerReviewesByContent($idContent, $idFrom = 0, $limit = 0, $order = "created ASC", $idTo = 0) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("cr.*, CONCAT( u.lastname , ' ',  u.firstname ) as fullname, IF(c.userId=cr.idUser, 1, 0) AS isContributor, IF(c.userId=cr.idUser, 'guru', 'contributor') AS kind");
		$dbCommand->from(App7020ContentReview::model()->tableName() . " cr");
		$dbCommand->join(App7020Assets::model()->tableName() . " c", "cr.idContent = c.id");
		$dbCommand->join(CoreUser::model()->tableName() . " u", " cr.idUser = u.idst");
		$dbCommand->where("cr.idContent = :idContent", array(":idContent" => $idContent));
		if ($idFrom) {
			$dbCommand->andWhere("cr.id > :idFrom", array(":idFrom" => intval($idFrom)));
		}
		if ($idTo) {
			$dbCommand->andWhere("cr.id <= :idTo", array(":idTo" => intval($idTo)));
		}
		if ($limit) {
			$dbCommand->limit(intval($limit));
		}
		$dbCommand->order($order);

		$returnArray = array();
		$result = $dbCommand->queryAll();
		foreach ($result as $value) {
			$value['avatar'] = CoreUser::getAvatarByUserId($value['idUser']);
			$returnArray[] = $value;
		}
		return $returnArray;
	}

	/**
	 * Returns all new peer reviews by given content
	 * @param integer $idContent
	 * @return array
	 */
	public static function getNewPeerReviewesByContent($idContent, $idUser) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("cr.*, (SELECT MAX(viewed) FROM app7020_content_history WHERE idContent=" . $idContent . " AND idUser=" . $idUser . " ) as viewedDate");
		$dbCommand->from(App7020ContentReview::model()->tableName() . " cr");
		$dbCommand->where("cr.idContent = :idContent", array(":idContent" => $idContent));
		$dbCommand->andWhere("cr.idUser = :idUser", array(":idUser" => $idUser));
		$dbCommand->having("cr.lastUpdated > viewedDate");

		return count($dbCommand->queryAll());
	}

	/**
	 * @see CActiveRecord::beforeSave()
	 */
	protected function beforeSave() {
		$this->lastUpdated = Yii::app()->localtime->getUTCNow();

		return true;
	}

	/**
	 * @see CActiveRecord::afterSave()
	 */
	protected function afterSave() {
		
		// Save in table "app7020_content_published" content as "Edited"
		App7020ContentPublished::saveEditAction($this->idContent);
		// Raise event for Expert adds peer review badge
		$_SESSION['time']=NULL;
		Yii::app()->event->raise('ExpertReachedAGoal', new DEvent($this, array('userId'=> Yii::app()->user->id,'contentId' => $assetId , 'goal'=>1)));
		$contentObject = App7020Assets::model()->findByPk($this->idContent);
		if ($contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_FINISHED) {
			$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_INREVIEW;
			$contentObject->save(false);
		}
		return true;
	}

	/**
	 * @see CActiveRecord::afterDelete()
	 */
	protected function afterDelete() {
		$contentObject = App7020Assets::model()->findByPk($this->idContent);
		$reviewsCount = count(self::getPeerReviewesByContent($this->id));

		if ($contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_INREVIEW && $reviewsCount == 0) {
			$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_FINISHED;
			$contentObject->save(false);
		}

		if ($contentObject->conversion_status == App7020Assets::CONVERSION_STATUS_APPROVED) {
			$contentObject->conversion_status = App7020Assets::CONVERSION_STATUS_INREVIEW;
			$contentObject->save(false);
		}
		return true;
	}

	/**
	 * Returns the array of experts by the content, which are attended in peer reviews of the content
	 * @param integer $idContent
	 * @return array
	 */
	public static function getAttendedExperts($idContent, $excludeContributor = false){
		$assetObject = App7020Assets::model()->findByPk($idContent);
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->selectDistinct('idUser');
		$dbCommand->from(App7020ContentReview::model()->tableName());
		$dbCommand->where("idContent = :idContent", array(":idContent" => $idContent));
		$expertIds = $dbCommand->queryAll();
		$returnArray = array();
		foreach ($expertIds as $value){
			if($excludeContributor){
				if($value['idUser'] == $assetObject->userId){
					//if the contributor is an expert - add it to the results, otherwise skip it
					if(App7020Experts::model()->findByAttributes(array('idUser' => $value['idUser']))->id)
						$returnArray[] = CoreUser::model()->findByPk($value['idUser']);
				} else {
					$returnArray[] = CoreUser::model()->findByPk($value['idUser']);
				}
			} else {
				$returnArray[] = CoreUser::model()->findByPk($value['idUser']);
			}
			
		}
		return $returnArray;
	}
}
