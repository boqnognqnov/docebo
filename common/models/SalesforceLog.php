<?php

/**
 * This is the model class for table "salesforce_log".
 *
 * The followings are the available columns in table 'salesforce_log':
 * @property integer $id
 * @property integer $id_account
 * @property string $type_log
 * @property string $datetime
 * @property string $data
 * @property string $job_id
 */
class SalesforceLog extends CActiveRecord
{
    
//    const TYPE_LOG_ACCOUNTS = 'accounts';
    const TYPE_LOG_USERS = 'users';
    const TYPE_LOG_COURSES = 'courses';
    const TYPE_LOG_UPGRADE = 'upgrade';
    const TYPE_LOG_UPDATE_METADATA = 'update_metadata';
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_account, type_log, datetime, job_id', 'required'),
			array('id_account', 'numerical', 'integerOnly'=>true),
			array('type_log', 'length', 'max'=>45),
            array('job_id', 'length', 'max'=>64),
			array('data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_account, type_log, datetime, data, job_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_account' => 'Id Account',
			'type_log' => 'Type Log',
			'datetime' => 'Datetime',
			'data' => 'Data',
            'job_id' => 'Job',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('type_log',$this->type_log,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('data',$this->data,true);
        $criteria->compare('job_id',$this->job_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public static function getLastAccountsLog($idAccount){
        $criteria = new CDbCriteria();
        $criteria->condition = 'id_account = :acc AND type_log = :log';
        $criteria->params = array(
            ':acc' => $idAccount,
            ':log' => self::TYPE_LOG_ACCOUNTS
        );
        $criteria->order = 'id DESC';
        $logModel = self::model()->find($criteria);
        
        if(empty($logModel)){
            return false;
        }
        
        return $logModel;
    }
    
    public static function getLastUsersLog($idAccount){
        $criteria = new CDbCriteria();
        $criteria->condition = 'id_account = :acc AND type_log = :log';
        $criteria->params = array(
            ':acc' => $idAccount,
            ':log' => self::TYPE_LOG_USERS
        );
        $criteria->order = 'id DESC';
        $logModel = self::model()->find($criteria);
        
        if(empty($logModel)){
            return false;
        }
        
        return $logModel;
    }
    
    public static function getLastCoursesLog($idAccount){
        $criteria = new CDbCriteria();
        $criteria->condition = 'id_account = :acc AND type_log = :log';
        $criteria->params = array(
            ':acc' => $idAccount,
            ':log' => self::TYPE_LOG_COURSES
        );
        $criteria->order = 'id DESC';
        $logModel = self::model()->find($criteria);
        
        if(empty($logModel)){
            return false;
        }
        
        return $logModel;
    }
    
    public function getItemsCount(){        
        $jsonData = json_decode($this->data);        
        return $jsonData->itemsCount;
    }
    
    public function getLogErrors(){
        $jsonData = json_decode($this->data, true);                
        
        $errors = array();
        if(empty($jsonData['errors'])){
            return $errors;
        }                
                
        foreach($jsonData['errors'] as $error){        	             
//         	foreach ($error as $er){
//         		$errors[] = $er[0];
//         	}
        	$errors[] = $error;
        }                             
        
        return $errors;
    }
    
    public static function getLogTypes(){
    	return array(
            self::TYPE_LOG_COURSES,
            self::TYPE_LOG_USERS
    	);
    }
}
