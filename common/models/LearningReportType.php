<?php

/**
 * This is the model class for table "learning_report_type".
 * The followings are the available columns in table 'learning_report_type':
 * @property integer $id
 * @property string $title
 * The followings are the available model relations:
 * @property LearningReportFilter[] $reports
 */
class LearningReportType extends CActiveRecord {

	// These must match the PK of learning_report_type row they correspond to
	const USERS_COURSES             = 1;
	const USERS_DELAY               = 2;
	const USERS_LEARNING            = 3;
	const COURSES_USERS             = 4;
	const GROUPS_COURSES            = 5;
	const ECOMMERCE_TRANSACTIONS    = 6;
	const AUDIT_TRAIL               = 7;
    const USERS_NOTIFICATIONS       = 9;
    const USERS_COURSEPATH          = 10;
	const USERS_SESSION             = 20;
	const CERTIFICATION_USERS       = 26;
	const USERS_CERTIFICATION       = 27;
    const USERS_EXTERNAL_TRAINING   = 28;
    const USERS_BADGES              = 29;
	const USERS_CONTESTS			= 30;
	const APP7020_ASSETS			= 50;
	const APP7020_EXPERTS			= 51;
	const APP7020_CHANNELS			= 52;
	const APP7020_USER_CONTRIBUTIONS= 53;


	// Grouped

	static $USERS_REPORTS = array(1,2,3,4,5,9,10,20,27,28,29,30);
	static $COURSES_REPORTS = array();
	static $APPS_REPORTS = array(LearningReportType::ECOMMERCE_TRANSACTIONS,LearningReportType::AUDIT_TRAIL,LearningReportType::CERTIFICATION_USERS);
	static $APP7020_REPORTS = array(50,51,52,53);

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningReportType the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_report_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max' => 120),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reports' => array(self::HAS_MANY, 'LearningReportFilter', 'report_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

  public static function getTypeTranslationKey($type) {
      $type = preg_replace('/_{1,}/', '_', strtoupper(str_replace(array(' ', '-'), '_', $type)));
      return '_' . $type;
  }
}