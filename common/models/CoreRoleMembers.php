<?php

/**
 * This is the model class for table "core_role_members".
 *
 * The followings are the available columns in table 'core_role_members':
 * @property integer $idst
 * @property integer $idstMember
 *
 * The followings are the available model relations:
 * @property CoreRole $role
 */
class CoreRoleMembers extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreRoleMembers the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'core_role_members';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idst, idstMember', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idst, idstMember', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'role' => array(self::BELONGS_TO, 'CoreRole', 'idst'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idst' => 'Idst',
            'idstMember' => 'Idst Member',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('idst',$this->idst);
        $criteria->compare('idstMember',$this->idstMember);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));
    }

    /*
     * Check if Power User Profile has Permission for current Role
     * @return true/false
     * */
    public static function checkPUAccess($puProfileId, $roleId){
       $check = Yii::app()->db->createCommand()->select('COUNT(*)')
            ->from(CoreRoleMembers::model()->tableName().' crm')
            ->join(CoreRole::model()->tableName().' cr', 'crm.idst = cr.idst')
            ->where('crm.idstMember = :idstMember and cr.roleid = :roleid', array(':idstMember' => $puProfileId, ':roleid' => $roleId))
            ->queryScalar();

        if($check > 0) {
            return true;
        } else {
            return false;
        }
    }
}