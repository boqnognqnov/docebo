<?php

/**
 * This is the model class for table "app7020_tag_link".
 *
 * The followings are the available columns in table 'app7020_tag_link':
 * @property integer $id
 * @property integer $idTag
 * @property integer $idContent
 *
 * The followings are the available model relations:
 * @property App7020Tag $tag
 * @property App7020Content $content
 */
class App7020TagLink extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app7020_tag_link';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTag, idContent', 'required'),
			array('idTag, idContent', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idTag, idContent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tag' => array(self::BELONGS_TO, 'App7020Tag', 'idTag'),
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idTag' => 'Id Tag',
			'idContent' => 'Id Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idTag',$this->idTag);
		$criteria->compare('idContent',$this->idContent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020TagLink the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    public static function getAssetsByTag($idTags){
        if(is_array($idTags) && !empty($idTags)){
            foreach($idTags as $idTag){
                $Tags[]=$idTag['idTag'];
            }
        }
        $dbCommand = Yii::app()->db->createCommand();
        $dbCommand->select('tags.idContent');
        $dbCommand->from(App7020TagLink::model()->tableName() . ' tags');
        $dbCommand->where('idTag IN (:idTag)', array(':idTag' => implode(',',$Tags)));
        return $dbCommand->queryColumn();

    }
	
	
}
