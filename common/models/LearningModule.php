<?php

/**
 * This is the model class for table "learning_module".
 *
 * The followings are the available columns in table 'learning_module':
 * @property integer $idModule
 * @property string $module_name
 * @property string $default_op
 * @property string $default_name
 * @property string $token_associated
 * @property string $file_name
 * @property string $class_name
 * @property string $module_info
 * @property string $mvc_path
 *
 * The followings are the available model relations:
 * @property LearningMenucourseUnder[] $learningMenucourseUnders
 */
class LearningModule extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningModule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_module';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('module_name, default_op, default_name, file_name, class_name, mvc_path', 'length', 'max'=>255),
			array('token_associated', 'length', 'max'=>100),
			array('module_info', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idModule, module_name, default_op, default_name, token_associated, file_name, class_name, module_info, mvc_path', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'learningMenucourseUnders' => array(self::HAS_MANY, 'LearningMenucourseUnder', 'idModule'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idModule' => 'Id Module',
			'module_name' => 'Module Name',
			'default_op' => 'Default Op',
			'default_name' => 'Default Name',
			'token_associated' => 'Token Associated',
			'file_name' => 'File Name',
			'class_name' => 'Class Name',
			'module_info' => 'Module Info',
			'mvc_path' => 'Mvc Path',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idModule',$this->idModule);
		$criteria->compare('module_name',$this->module_name,true);
		$criteria->compare('default_op',$this->default_op,true);
		$criteria->compare('default_name',$this->default_name,true);
		$criteria->compare('token_associated',$this->token_associated,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('class_name',$this->class_name,true);
		$criteria->compare('module_info',$this->module_info,true);
		$criteria->compare('mvc_path',$this->mvc_path,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}