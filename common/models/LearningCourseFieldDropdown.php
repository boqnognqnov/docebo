<?php

/**
 * This is the model class for table "learning_course_field_dropdown".
 *
 * The followings are the available columns in table 'learning_course_field_dropdown':
 * @property integer $id_option
 * @property integer $id_field
 * @property integer $sequence
 *
 * The followings are the available model relations:
 * @property LearningCourseField $idField
 * @property LearningCourseFieldDropdownTranslations[] $learningCourseFieldDropdownTranslations
 */
class LearningCourseFieldDropdown extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_field_dropdown';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_field', 'required'),
			array('id_field, sequence', 'numerical', 'integerOnly'=>true),
			array('id_option, id_field, sequence', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idField' => array(self::BELONGS_TO, 'LearningCourseField', 'id_field'),
			'learningCourseFieldDropdownTranslations' => array(self::HAS_MANY, 'LearningCourseFieldDropdownTranslations', 'id_option'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_option' => 'Id Option',
			'id_field' => 'Id Field',
			'sequence' => 'Sequence',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id_option',$this->id_option);
		$criteria->compare('id_field',$this->id_field);
		$criteria->compare('sequence',$this->sequence);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseFieldDropdown the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
