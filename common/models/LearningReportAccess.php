<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 07-Dec-15
 * The followings are the available columns in table 'learning_label':
 * @property integer id_report
 * @property string visibility_type
 * Time: 1:45 PM
 */
class LearningReportAccess extends CActiveRecord
{
	const TYPE_PUBLIC = 'public';
	const TYPE_PRIVATE = 'private';
	const TYPE_SELECTION = 'selection';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningReport the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_report_access';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array('id_report', 'numerical', 'integerOnly' => true),
				array('visibility_type', 'length', 'max' => 100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
				array('id_report, visibility_type', 'safe', 'on' => 'search'),
		);
	}

	public function renderNewAccessFilter($isOwner){
		$link = '';
		$text = '';
		if(Yii::app()->user->getIsGodadmin()) $isOwner = true;

		switch($this->visibility_type){
			case self::TYPE_PUBLIC :
				$text = Yii::t('report', '_TAB_REP_PUBLIC') . ': ' . Yii::t('standard', 'All users, groups and branches');
				break;
			case self::TYPE_PRIVATE :
				$text = Yii::t('calendar', '_PRIVATE');
				break;
			case self::TYPE_SELECTION :
				$users = LearningReportAccessMember::getMembersByMemberType($this->id_report, LearningReportAccessMember::TYPE_USER);
				$groups = LearningReportAccessMember::getMembersByMemberType($this->id_report, LearningReportAccessMember::TYPE_GROUP);
				$branches = LearningReportAccessMember::getMembersByMemberType($this->id_report, LearningReportAccessMember::TYPE_BRANCH);

				$countUsers = count($users);
				$countGroups = count($groups);
				$countBranches = count($branches);

				$text = strtolower($countUsers . ' ' . Yii::t('standard', '_USERS') . ', ' . $countGroups . ' ' . Yii::t('standard', '_GROUPS') . ', ' . $countBranches . ' ' . Yii::t('standard', 'branches'));
				break;
		}
		if(!$isOwner) return $text;
		$link = CHtml::link($text, Docebo::createLmsUrl('usersSelector/axOpen', array('id' => $this->id_report, 'type' => UsersSelector::TYPE_REPORT_VISIBILITY)), array(
			'class' => 'open-dialog',
			'style'=>'text-decoration: underline',
			'data-dialog-class' => 'customLoading'
		));
		return $link;
	}

	public static function checkPermissionToEdit($currentUserId, $idReport){
		$users = LearningReportAccessMember::getMembersByMemberType($idReport, LearningReportAccessMember::TYPE_USER);
		$groups = LearningReportAccessMember::getMembersByMemberType($idReport, LearningReportAccessMember::TYPE_GROUP);
		$branches = LearningReportAccessMember::getMembersByMemberType($idReport, LearningReportAccessMember::TYPE_BRANCH);

		$canEdit = false;
		$reportModel = LearningReportFilter::model()->findByPk($idReport);
		// check if current user is in allowed user list or is superadmin
		$isOwner = $reportModel->author == $currentUserId;
		if (Yii::app()->user->getIsGodadmin() || $isOwner || in_array($currentUserId, $users)) {
			$canEdit = true;
		} else {
			// First check if current user is member of some of the allowed groups
			if(!empty($groups)) {
				$usersInGroups = Yii::app()->db->createCommand()
						->select('idstMember')
						->from(CoreGroupMembers::model()->tableName())
						->where(array('IN', 'idst', $groups))->queryColumn();
				if (in_array($currentUserId, $usersInGroups)) $canEdit = true;
			}

			// If still not CanEdit, check if current user is in allowed branches
			if (!$canEdit) {
				// Get list of branches user MUST be member of (direct or in descendants)
				$directMemberBranches = array();
				$directAndDescendantBranches = array();
				foreach ($branches as $branch) {
					if ($branch['selectState'] == 2) {
						$directAndDescendantBranches[] = $branch['key'];
					}
					else {
						$directMemberBranches[] = $branch['key'];
					}
				}

				// Using new VIEW (user_org_assignment)
				// If something goes wrong, comment this line and uncomment the next block of code to use
				// the old but a bit haevier approach
				$canEdit = CoreOrgChartTree::isUserInNode($currentUserId, $directMemberBranches) || CoreOrgChartTree::isUserInBranch($currentUserId, $directAndDescendantBranches); 

				/*
				foreach ($directMemberBranches as $idOrg) {
					if (CoreOrgChartTree::isUserMemberOf($currentUserId, $idOrg)) {
						$canEdit = true;
						break;
					}
				}
				if (!$canEdit) {
					foreach ($directAndDescendantBranches as $idOrg) {
						if (CoreOrgChartTree::isUserMemberOf($currentUserId, $idOrg, true)) {
							$canEdit = true;
							break;
						}
					}
				}
				*/
				
			}
		}
		return $canEdit;
	}
}