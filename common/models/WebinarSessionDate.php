<?php

/**
 * This is the model class for table "webinar_session_date".
 *
 * The followings are the available columns in table 'webinar_session_date':
 * @property integer $id_session
 * @property string $day
 * @property string $name
 * @property string $description
 * @property string $time_begin
 * @property string $timezone_begin
 * @property integer $duration_minutes
 * @property string $webinar_tool
 * @property integer $id_tool_account
 * @property string $webinar_tool_params
 * @property integer $join_in_advance_time_user
 * @property integer $join_in_advance_time_teacher
 * @property string $recording_file
 *
 * The followings are the available model relations:
 * @property WebinarSession $webinarSession
 */
class WebinarSessionDate extends AuditActiveRecord
{
	/**
	 * Populated when the webinar tool selected is "Custom"
	 * @var
	 */
	public $webinar_custom_url;

	public $utcBeginTimestamp;

	public $utcEndTimestamp;

	public $remainingTime;

	protected $oldAttributes = array();

	public function afterFind() {
		parent::afterFind();

		if($this->webinar_tool_params){
			$params = CJSON::decode($this->webinar_tool_params);

			if($this->webinar_tool==WebinarSession::TOOL_CUSTOM){
				$this->webinar_custom_url = $params['custom_url'];
			}
		}
		if($this->time_begin){
			$this->time_begin = substr($this->time_begin, 0, 5);
		}

		$this->oldAttributes = $this->attributes;
	}

	public function beforeValidate(){

		// Rise event so the plugins know that we are about to validate this model
		Yii::app()->event->raise('onBeforeWebinarSessionDateValidate', new DEvent($this, array('model' => &$this)));

		return parent::beforeValidate();
	}

	public function beforeSave() {
		$toolParams = array();
		if ($this->webinar_tool == WebinarSession::TOOL_CUSTOM) {
			if ($this->webinar_custom_url) {
				$toolParams['custom_url'] = $this->webinar_custom_url;
			}
		} else {
			if (is_array($this->webinar_tool_params)) {
				$toolParams = $this->webinar_tool_params;
			} elseif (is_string($this->webinar_tool_params)) {
				$toolParams = CJSON::decode($this->webinar_tool_params);
			}
		}

		$tool = WebinarTool::getById($this->webinar_tool);

		$startDateTmp = $this->day . ' '. $this->time_begin. ':00';

		$timezoneOffset = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($this->timezone_begin);
		$startTimestamp = strtotime($startDateTmp) - $timezoneOffset;
		$endTimestamp = $startTimestamp + ($this->duration_minutes * 60);

		$startDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $startTimestamp));
		$startDateUTC = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDateTime($startDateUTC), 'Y-m-d H:i:sP');

		$endDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $endTimestamp));
		$endDateUTC = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDateTime($endDateUTC), 'Y-m-d H:i:sP');

		if (!$tool->canHandleDST()) {

			//tool cannot handle DST timezones by itself, so we need to provided already-shifted dates in UTC format

			$tz = new DateTimeZone($this->timezone_begin);
			$dt_now = new DateTime('now', $tz);
			$dt_date = new DateTime($startDateTmp, $tz);

			$offsetNow = $tz->getOffset($dt_now);
			$offsetDate = $tz->getOffset($dt_date);

			if ($offsetNow != $offsetDate) {

				//re-calculate dates
				$compensate = $offsetDate - $offsetNow;
				$startTimestamp = strtotime($startDateTmp) - $timezoneOffset - $compensate;
				$endTimestamp = $startTimestamp + ($this->duration_minutes * 60);

				$startDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $startTimestamp));
				$startDateUTC = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDateTime($startDateUTC), 'Y-m-d H:i:sP');

				$endDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $endTimestamp));
				$endDateUTC = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDateTime($endDateUTC), 'Y-m-d H:i:sP');
			}

		}


		// Needed by Gototraining specifically at the moment
		// If you try to call a "update" api call without actually
		// passing a modified timestamp the API returns a 502 Bad Gateway error
		$updateTimes = true;
		if (!$this->getIsNewRecord()) {
			$oldStartDT = new DateTime($this->oldAttributes['day'].' '.$this->oldAttributes['time_begin'], new DateTimeZone($this->oldAttributes['timezone_begin']));
			$oldEndDT = new DateTime($this->oldAttributes['day'].' '.$this->oldAttributes['time_begin'], new DateTimeZone($this->oldAttributes['timezone_begin']));
			$oldEndDT->modify('+'.$this->oldAttributes['duration_minutes'].' minutes');

			$startDT = new DateTime($this->day.' '.$this->time_begin, new DateTimeZone($this->timezone_begin));
			$endDT = new DateTime($this->day.' '.$this->time_begin, new DateTimeZone($this->timezone_begin));
			$endDT->modify('+'.$this->duration_minutes.' minutes');

			if($startDT->getTimestamp() == $oldStartDT->getTimestamp() && $endDT->getTimestamp() == $oldEndDT->getTimestamp()){
				$updateTimes = false;
			}
		}

		$nowUTCTimestamp = strtotime(Yii::app()->localtime->getUTCNow());

		if($this->webinar_tool !== WebinarSession::TOOL_CUSTOM && strtotime($startDateUTC) > $nowUTCTimestamp) {
			if($this->isNewRecord || $this->hasAttributeChanged('webinar_tool')){
				// Create room APi call

				// Just a note on "$this->hasAttributeChanged('webinar_tool')":
				// If you delete a date with Webinar Tool X and add a new date
				// with Webinar Tool Y, we can not call the "Update webinar date"
				// api call because the APIs between webinar tools are mostly incompatible.
				// Call the "create webinar date" API call instead in this special case
				// End of note;
				$toolParams = $tool->createRoom($this->name, $this->description, $this->id_tool_account, $startDateUTC, $endDateUTC, $this->webinarSession->max_enroll, $toolParams);
			}else{
				// Update room API call

				if($tool->getId() === 'gototraining') {
					/* @var $tool GototrainingWebinarTool */
					$toolParams = $tool->updateRoom($this->name, $this->description, $this->id_tool_account,
						is_array($toolParams) ? $toolParams : CJSON::decode($toolParams), $startDateUTC, $endDateUTC, $this->webinarSession->max_enroll, $updateTimes);

				} else {
					$toolParams = $tool->updateRoom($this->name, $this->description, $this->id_tool_account,
						is_array($toolParams) ? $toolParams : CJSON::decode($toolParams), $startDateUTC, $endDateUTC, $this->webinarSession->max_enroll);
				}
			}
		}

		$this->webinar_tool_params = CJSON::encode($toolParams);

//		if($this->isNewRecord) {
//			if($this->webinar_tool !== WebinarSession::TOOL_CUSTOM) {
//				$params = $this->webinar_tool_params;
//				if(is_string($params)){
//					$params = CJSON::decode($params);
//				}
//				$this->webinar_tool_params = CJSON::encode($tool->createRoom($this->name, $this->description, $this->id_tool_account, $startDateUtc, $endDateUTC, $this->webinarSession->max_enroll, $params));
//			}
//		} else {
//			// First decode the params to an array so that we can pass them
//			// to the individual webinar tools' APIs
//			if(is_string($this->webinar_tool_params)){
//				$this->webinar_tool_params = CJSON::decode($this->webinar_tool_params);
//			}
//
//			if($this->hasAttributeChanged('webinar_tool')){
//				// If you delete a date with Webinar Tool X and add a new date
//				// with Webinar Tool Y, we can not call the "Update webinar date"
//				// api call because the APIs between webinar tools are mostly incompatible.
//				// Call the "create webinar date" API call instead in this special case
//				$this->webinar_tool_params = CJSON::encode($tool->createRoom($this->name, $this->description, $this->id_tool_account, $startDateUtc, $endDateUTC, $this->webinarSession->max_enroll, $this->webinar_tool_params));
//			}else{
//				if($tool->getId() === 'gototraining') {
//					$this->webinar_tool_params = CJSON::encode($tool->updateRoom($this->name, $this->description, $this->id_tool_account,
//						is_array($this->webinar_tool_params) ? $this->webinar_tool_params : CJSON::decode($this->webinar_tool_params), $startDateUtc, $endDateUTC, $this->webinarSession->max_enroll, $updateTimes));
//
//				} else {
//					$this->webinar_tool_params = CJSON::encode($tool->updateRoom($this->name, $this->description, $this->id_tool_account,
//						is_array($this->webinar_tool_params) ? $this->webinar_tool_params : CJSON::decode($this->webinar_tool_params), $startDateUtc, $endDateUTC, $this->webinarSession->max_enroll));
//				}
//			}
//		}
		return parent::beforeSave();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_session_date';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_session, day, name', 'required'),
			array('time_begin, webinar_tool', 'required', 'on' => 'addTogetherWithSession'),
			array('webinar_tool', 'validateWebinarToolOptions', 'on' => 'addTogetherWithSession'),
			array('id_session, duration_minutes, id_tool_account, join_in_advance_time_user, join_in_advance_time_teacher', 'numerical', 'integerOnly'=>true),
			array('name, timezone_begin, webinar_tool, id_tool_account, recording_file', 'length', 'max'=>255),
			array('time_begin, webinar_tool_params, webinar_tool_url, description', 'safe'),
			array('day', 'validateStartTime'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_session, day, name, description, time_begin, timezone_begin, duration_minutes, webinar_tool, webinar_tool_params, join_in_advance_time_user, join_in_advance_time_teacher, recording_file, webinar_tool_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'webinarSession' => array(self::BELONGS_TO, 'WebinarSession', 'id_session'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_session' => 'Id Session',
			'day' => 'Day',
			'name' => 'Name',
			'description'=>'Description',
			'time_begin' => 'Time Begin',
			'timezone_begin' => 'Timezone Begin',
			'duration_minutes' => 'Duration Minutes',
			'webinar_tool' => 'Webinar Tool',
			'id_tool_account'=>'Webinar account',
			'webinar_tool_params' => 'Webinar Tool Params',
			'join_in_advance_time_user' => 'Join In Advance Time User',
			'join_in_advance_time_teacher' => 'Join In Advance Time Teacher',
			'recording_file' => 'Recording File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_session',$this->id_session);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('time_begin',$this->time_begin,true);
		$criteria->compare('timezone_begin',$this->timezone_begin,true);
		$criteria->compare('duration_minutes',$this->duration_minutes);
		$criteria->compare('webinar_tool',$this->webinar_tool,true);
		$criteria->compare('webinar_tool_params',$this->webinar_tool_params,true);
		$criteria->compare('join_in_advance_time_user',$this->join_in_advance_time_user);
		$criteria->compare('join_in_advance_time_teacher',$this->join_in_advance_time_teacher);
		$criteria->compare('recording_file',$this->recording_file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarSessionDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getHours($format = '%H:%I'){
		$durationMinutes = $this->duration_minutes;
		if($durationMinutes>=60) {
			$hours = floor($durationMinutes / 60);
			$minutes = $durationMinutes % 60;
			$interval = new DateInterval("PT{$hours}H{$minutes}M");
		}else{
			$interval = new DateInterval("PT{$durationMinutes}M");
		}
		return $interval->format($format);
	}

	public static function getDurationInFormat($duration, $format = '%H:%I'){
		if(!isset($duration) || empty($duration) || $duration < 0)
			$duration = 0;

		if($duration >= 60) {
			$hours = floor($duration / 60);
			$minutes = $duration % 60;
			$interval = new DateInterval("PT{$hours}H{$minutes}M");
		}else{
			$interval = new DateInterval("PT{$duration}M");
		}
		return $interval->format($format);
	}



	/**
	 * Prevent the creation of webinar sessions in the past
	 * @param $attribute
	 * @param $params
	 */
	public function validateStartTime($attribute,$params){
		if($this->getIsNewRecord()){
			$timestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

			if ($this->time_begin)
				$startDateTmp = $this->day . ' '. $this->time_begin. ':00';
			else
				$startDateTmp = $this->day . '00:00:00';

			$timezoneOffset = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($this->timezone_begin);
			$startTimestamp = strtotime($startDateTmp) - $timezoneOffset;
			$startDateUTC = date('Y-m-d H:i:s+00:00', $startTimestamp);

			if(strtotime($startDateUTC) < $timestamp){
				// Creation timestamp is in the past
				// Add the error either to the day or time field dynamically
				$currentDate = date('Y-m-d');
				if($currentDate==$this->day){
					$this->addError('time_begin', Yii::t('webinar', 'Can not create session date in the past'));
				}else{
					$this->addError('day', Yii::t('webinar', 'Can not create session date in the past'));
				}

			}
		}
	}

	public function validateWebinarToolOptions($attribute, $params)
	{
		if ($this->webinar_tool == WebinarSession::TOOL_CUSTOM)
		{
			if (filter_var($this->webinar_custom_url, FILTER_VALIDATE_URL) === false)
				$this->addError($attribute, 'Invalid webinar custom URL option');
		}
		else
		{
			if (empty($this->id_tool_account))
				$this->addError($attribute, 'Please fill all required webinar tool options');
		}
	}

	/**
	 * Check if the current tool for the date allows the grab of recorded session video file through API call
	 * @param null|string $tool
	 * @return bool
	 * @throws CHttpException
	 */
	public function toolAllowsApiGrabRecording($tool = null){
		if(!$tool){
			$tool = $this->webinar_tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->allowApiGrabRecording();
	}

	/**
	 * Check if the current tool for the date allows fetching a link for a recorded session video file through API call
	 * @param null|string $tool
	 * @return bool
	 * @throws CHttpException
	 */
	public function toolAllowsApiLinkRecording($tool = null){
		if(!$tool){
			$tool = $this->webinar_tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->allowApiFindRecording();
	}

	/**
	 * Check if selected tool only supports external playback
	 * @param null|string $tool
	 * @return bool
	 * @throws CHttpException
	 */
	public function getPlayRecordingType($tool = null){
		if(!$tool){
			$tool = $this->webinar_tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->getPlayRecordingType();
	}


	/**
	 * Returns webinar URL string
	 * Start Room URL for admins
	 * Join Room for other users
	 */
	public function getWebinarUrl($widgetContext = false)
	{
		$tool = WebinarTool::getById($this->webinar_tool);
		$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
		$webinarParams = (is_array($this->webinar_tool_params) ? $this->webinar_tool_params : CJSON::decode($this->webinar_tool_params));
		if ($this->webinarSession->allowAdminOperations())
			$url = $tool->getStartRoomUrl($this->name, $this->webinarSession->max_enroll, $this->id_tool_account, $webinarParams, $userModel, $this);
		else
			$url = $tool->getJoinRoomUrl($this->name, $this->webinarSession->max_enroll, $this->id_tool_account, $webinarParams, $userModel, $this, $widgetContext);
		Yii::log('WEBEX: user #'.Yii::app()->user->id.' generated start/join URL '.$url, CLogger::LEVEL_INFO);
		return $url;
	}


	/**
	 * Get how many minutes in advance can the $idUser see the "Join"
	 * button in Learner View of the session. This is a setting of the
	 * session that can be different for "users" and "teachers"
	 *
	 * @param $idUser
	 * @param $day
	 *
	 * @return int minutes in advance when the "Join" button can be seen
	 */
	static public function getJoinInAdvanceButtonTimeoutMinutes($idUser, $idSession, $day){

		// Get user's "level" in the session to which this date belongs
		static $cachedDates = array();

		if(!isset($cachedDates[$day . '_' . $idSession])){
			$cachedDates[$day . '_' . $idSession] = Yii::app()->getDb()->createCommand()
				->select('*')
				->from(WebinarSessionDate::model()->tableName())
				->where('day=:day AND id_session=:idSession', array(
					':day'=>$day,
					':idSession'=>$idSession,
				))
				->queryRow();
		}

		if(!isset($cachedDates[$day . '_' . $idSession])){
			Yii::log('Date '.$day.' not found for webinar session', CLogger::LEVEL_ERROR);
		}else {
			$level = WebinarSession::getUserLevelInSession($cachedDates[$day . '_' . $idSession]['id_session'], $idUser);

			if($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT || Yii::app()->user->getIsGodadmin() ) {
				// Is a teacher or godadmin or PU
				return $cachedDates[$day . '_' . $idSession]['join_in_advance_time_teacher'];
			} elseif($level == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) {
				// Is a learner
				return $cachedDates[$day . '_' . $idSession]['join_in_advance_time_user'];
			}
		}

		return 0;
	}

	/**
	 * This method is invoked after deleting a record.
	 */
	protected function afterDelete()
	{
		// Try calling the API
		$tool = WebinarTool::getById($this->webinar_tool);
		$params = CJSON::decode($this->webinar_tool_params);
		if(is_array($params) && (!empty($params['meeting_id']) || !empty($params['sco_id']) || !empty($params['training_id']) ||!empty($params['webinar_id'])))
			$tool->deleteRoom($this->id_tool_account, $params);

		return parent::afterDelete();
	}


	/**
	 * Check if a video recording exists for the current webinar session date
	 * @return bool
	 */
	public function hasRecording() {
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand();
		$cmd->select("COUNT(*)");
		$cmd->from(WebinarSessionDateRecording::model()->tableName());
		$cmd->where("id_session = :id_session AND day = :day");
		$result = $cmd->queryScalar(array(
			':id_session' => $this->id_session,
			':day' => $this->day
		));
		return ($result > 0);
	}


	/**
	 * Retrieve associated recording to the current webinar session date. If no recording is present, then NULL value is returned
	 * @return null|WebinarSessionDateRecording
	 */
	public function getRecording() {
		$recording = WebinarSessionDateRecording::model()->findByPk(array(
			'id_session' => $this->id_session,
			'day' => $this->day
		));
		return (!empty($recording) ? $recording : NULL);
	}

	/**
	 * @return string
	 */
	public function getEndTime()
	{
		$date = new DateTime($this->time_begin);
		$date->add(new DateInterval('PT' . $this->duration_minutes . 'M'));
		return $date->getTimestamp();
	}

	public function getDateEndTime(){
		$endingTime = Yii::app()->getDb()->createCommand()
			->select("DATE_ADD(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE) AS endtimestampUTC")
			->from(WebinarSessionDate::model()->tableName())
			->andWhere('day=:day and id_session=:id_session', array(':day'=>$this->day, ':id_session' =>$this->id_session))
			->queryScalar();
		return Yii::app()->localtime->toLocalDateTime($endingTime);
	}

	 public function getDateStartDate(){
		$lowestDate = Yii::app()->getDb()->createCommand()
			->select("CONVERT_TZ(concat(day,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC")
			->from(WebinarSessionDate::model()->tableName())
			->andWhere('day=:day and id_session=:id_session', array(':day'=>$this->day, ':id_session' =>$this->id_session))
			->queryScalar();
		return Yii::app()->localtime->toLocalDateTime($lowestDate);
	}


	/**
	 * Check if the tool exists and return it's name
	 * @return string
	 */
	public function getToolName()
	{
		$tools = WebinarSession::toolList();
		return $tools[$this->webinar_tool];
	}

	/**
	 * Returns the timezone label for this date
	 */
	public function renderTimezone() {
		return Yii::app()->localtime->getTimezoneOffset($this->timezone_begin);
	}
}
