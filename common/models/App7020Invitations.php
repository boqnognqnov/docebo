<?php

/**
 * This is the model class for table "app7020_invitations".
 *
 * The followings are the available columns in table 'app7020_invitations':
 * @property integer $id
 * @property string $idInvited
 * @property string $idInviter
 * @property integer $idContent
 * @property string $created
 *
 * The followings are the available model relations:
 * @property CoreUser $user
 * @property App7020Content $contributiuon
 */
class App7020Invitations extends CActiveRecord {

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_invitations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array();
	}

	/**
	 *
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array();
	}

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function beforeSave() {

		if (!$this->id) {
			$this->created = Yii::app()->localtime->getUTCNow();
		}

		$this->idInviter = Yii::app()->user->idst;

		return true;
	}

	/**
	 * Gets list of all assets which a user has benn invited to watch
	 * @param type $idInvited
	 * @return array
	 */
	public static function getAllInvitationsByInvited($idInvited, $from = 0, $count = 0, $excludeWatched = false) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("c.*, i.created AS create_date, u.idst, CONCAT(u.firstname, '.', u.lastname) as author, inviter.idst as inviterId, CONCAT(inviter.firstname, ' ', inviter.lastname) as invitedBy,i.created as invite_creation, "
			. "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idUser = " . $idInvited . " AND idContent = c.id) as watched, "
			. "(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent=c.id) AS contentRating, "
			. "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent=c.id) AS contentViews, sth.spentTime ");
		$dbCommand->from("app7020_invitations i");
		$dbCommand->join(App7020Assets::model()->tableName() . " c", "i.idContent=c.id");
		$dbCommand->join(CoreUser::model()->tableName() . " u", "u.idst=c.userId");
		$dbCommand->join(CoreUser::model()->tableName() . " inviter", "inviter.idst=i.idInviter");
		$dbCommand->leftJoin(App7020SpentTimeHistory::model()->tableName().' sth', 'i.idContent = sth.idContent');
		$dbCommand->where("i.idInvited = :idInvited", array(":idInvited" => $idInvited));
		//$dbCommand->andWhere("c.is_private=:private", array(':private' => App7020Assets::PRIVATE_STATUS_INIT)); //only public assets
		$dbCommand->andWhere("c.conversion_status=:status", array(':status' => App7020Assets::CONVERSION_STATUS_APPROVED));

		$dbCommand->group("c.id");
		$dbCommand->order("i.created desc");
		if($excludeWatched){
			$dbCommand->having("watched = 0 AND sth.spentTime IS NULL");
		}

		if ($count > 0)
			$dbCommand->limit($count, $from);
		return $dbCommand->queryAll();
	}

	/**
	 * Gets list of assets which a user has benn invited to watch
	 * @param type $idInvited
	 * @param type $watched 0-if there is no filter, 1-get only new ones, 2-get only
	 * @return cArrayDataProvider
	 */
	public static function getInvitationsByInvited($idInvited, $watched = 0, $customConfig = array()) {
		if (empty($watched)) {
			$watched = (int) Yii::app()->request->getParam('radioSwitch', false);
		}

		$searchInput = trim(Yii::app()->request->getParam('search_input', false));

		$customPageSize = (!empty($customConfig['pageSize'])) ? $customConfig['pageSize'] : false;
		$setPagination = (isset($customConfig['pagination'])) ? $customConfig['pagination'] : true;
		$tmp = Yii::app()->request->getParam('mainFilter', false);
		$mainFilter = !empty($tmp) ? Yii::app()->request->getParam('mainFilter', false) : $customConfig['sessionMainFilterKL'];


		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("c.*, u.userId, (SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idUser = " . $idInvited . " AND idContent = c.id) as watched, "
				. "(SELECT IF(COUNT(id) > 0, SUM(rating) / COUNT(id), 0) FROM " . App7020ContentRating::model()->tableName() . " WHERE idContent=c.id) AS contentRating, "
				. "(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idContent=c.id) AS contentViews ");
		$dbCommand->from("app7020_invitations i");
		$dbCommand->join(App7020Assets::model()->tableName() . " c", "i.idContent=c.id");
		$dbCommand->join(CoreUser::model()->tableName() . " u", "u.idst=c.userId");
		$dbCommand->where("i.idInvited = :idInvited", array(":idInvited" => $idInvited));
		if ($watched) {
			if ($watched == 1) {
				$dbCommand->andWhere("(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idUser = " . $idInvited . " AND idContent = c.id) = 0");
			}
			if ($watched == 2) {
				$dbCommand->andWhere("(SELECT COUNT(id) FROM " . App7020ContentHistory::model()->tableName() . " WHERE idUser = " . $idInvited . " AND idContent = c.id) > 0");
			}
		}
		$dbCommand->group("c.id");

		if (isset($customConfig['limit'])) {
			$dbCommand->limit($customConfig['limit']);
		}


		if ($searchInput) {
			$dbCommand->andWhere("CONCAT(COALESCE(c.title,''), ' ', COALESCE(c.description,'')) LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		// ASSETS FILTER
		if ($mainFilter['assets'] > 0) {
			switch ($mainFilter['assets']) {
				case 1:
					$dbCommand->andWhere('c.contentType = 1');
					break;
				case 2:
					$dbCommand->andWhere('c.contentType >= 2 AND c.contentType <= 6');
					break;
				case 3:
					$dbCommand->andWhere('c.contentType = 15');
					break;
				case 4:
					$dbCommand->andWhere('c.contentType >= 7 AND c.contentType < 15');
					break;
			}
		}

		// ORDERS
		switch ($mainFilter['sort']) {
			case 1:
				$dbCommand->order('contentRating DESC, created DESC');
				break;
			case 2:
				$dbCommand->order('contentViews DESC, created DESC');
				break;
			case 3:
				$dbCommand->order('c.duration DESC, created DESC');
				break;
			default :
				$dbCommand->order('created DESC');
				break;
		}

		switch ($mainFilter['uploaded']) {
			case 1: // THIS WEEK
				$dbCommand->andWhere('YEARWEEK(c.created, 1) = YEARWEEK(NOW(), 1)');
				break;
			case 2: // THIS MONTH
				$dbCommand->andWhere('EXTRACT(MONTH From c.created) = EXTRACT(MONTH From NOW())');
				break;
			case 3: // PAST 6 MONTHs
				$dbCommand->andWhere('c.created > DATE_SUB(NOW(),INTERVAL 6 MONTH)');
				break;
			case 4: // THIS YEAR
				$dbCommand->andWhere('EXTRACT(YEAR From c.created) = EXTRACT(YEAR From NOW())');
				break;
		}

		// ratings
		if ($mainFilter['rating']) {
			$dbCommand->having('contentRating > ' . $mainFilter['rating']);
		}

		$perPage = 12;
		if ($setPagination) {
			$pageSize = $perPage;

			if ($customPageSize)
				$pageSize = $customPageSize;

			$pagination = array('pageSize' => $pageSize);
		} else {
			$pagination = false;
		}


		$config = array(
			'pagination' => $pagination,
			'keyField' => 'id',
		);

		$resultArray = $dbCommand->queryAll();
		$returnArray = array();
		foreach ($resultArray as $value) {
			$value['inviters'] = self::getInviters($value['id'], $idInvited);
			$returnArray[] = $value;
		}
		$dataProvider = new CArrayDataProvider($returnArray, $config);
		return $dataProvider;
	}

	/**
	 * Get all inviters by asset and invited user
	 * @param type $idContent
	 * @param type $idInvited
	 * @return array
	 */
	public static function getInviters($idContent, $idInvited) {
		$arrayInviters = array();
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->selectDistinct("idInviter");
		$dbCommand->from('app7020_invitations');
		$dbCommand->where("idContent = :idContent", array(":idContent" => $idContent));
		$dbCommand->andWhere("idInvited = :idInvited", array(":idInvited" => $idInvited));
		$result = $dbCommand->queryAll();
		foreach ($result as $idInviter) {
			$value = CoreUser::model()->findByPk($idInviter);
			$tmpArray = $value->getAttributes();
			$tmpArray['imgSource'] = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $value->idst), true);
			$tmpArray['name'] = $value->firstname . " " . $value->lastname;
			if (!trim($tmpArray['name'])) {
				$tmpArray['name'] = ltrim($value->userid, "/");
			}
			$tmpArray['linkSource'] = "javascript:";
			$tmpArray['status'] = "online";
			$arrayInviters[] = $tmpArray;
		}
		return $arrayInviters;
	}

	public static function countMyInvitations() {
		$criteriaMyInvitations = new CDbCriteria();
		$criteriaMyInvitations->condition = 'idInvited = :idUser';
		$criteriaMyInvitations->group = 'idContent';
		$criteriaMyInvitations->params = array(':idUser' => Yii::app()->user->getIdst());
		$countMyInvitations = App7020Invitations::model()->count($criteriaMyInvitations);

		return array(
			'countMyInvitations' => $countMyInvitations,
		);
	}

    public static function countMySendInvitations() {
        $criteriaMyInvitations = new CDbCriteria();
        $criteriaMyInvitations->condition = 'idInviter = :idUser';
        $criteriaMyInvitations->params = array(':idUser' => Yii::app()->user->getIdst());
        $countMyInvitations = App7020Invitations::model()->count($criteriaMyInvitations);

        return array(
            'countMyInvitations' => $countMyInvitations,
        );
    }

	/*
	 * Returns an array with users and their types: 
	 * 0 - can view the asset but no invited yet
	 * 1 - can view the asset and already invited
	 * 2 - cannot view the asset
	 * 
	 */

	//public static function getVisibleUsers($idContent, $asJson = false, $idUser = false, $invitedTypes = array(0, 1),$filter=false) {
	public static function getVisibleUsers($idContent, $asJson = false, $idUser = false, $invitedTypes = array(0), $filter = false) {


		if (!$idUser) {
			$idUser = Yii::app()->user->idst;
		}
		$usersCanViewTheAsset = App7020Assets::getUsersWhichCanViewAnAsset($idContent, false, false);
		$invitedUsers = self::getInvitedUsers($idContent);
		if (!$invitedUsers)
			$invitedUsers = array(-1);
		/* @var $storage CS3Storage */
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$channels = App7020ChannelAssets::getChannelsByContent($idContent);
		$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		// Default (platform) language ('en')
		$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));

		// Resolve requested language
		$language = Yii::app()->getLanguage();

		$channelsWhereClause = "";

		if (!empty($channels))
			$channelsWhereClause = "cc.id IN (" . implode(",", $channels) . ") AND ";

		$sql = "
				SELECT
				idst AS uid,
				" . $idContent . " AS idcontent,
				" . $idUser . " AS idinviter,
				2 AS type,
				CONCAT(firstname, ' ', lastname) AS value,
				idst AS id,
				0 AS chanTrack,
				'" . $standartAvatar . "' as defaultAvatar,
				firstname AS first,
				lastname AS last,
				TRIM(LEADING '/' FROM userid) AS username,
				(
					IF(
						Length(avatar) < 1, '',CONCAT('" . $storage->absoluteBaseUrl . '/avatar/' . "',avatar)
					)
				) AS avatar,
				
				( SELECT COUNT(id) FROM app7020_invitations WHERE idInviter = " . $idUser . " AND idInvited=uid ) as popular,
					
				(
					IF(
						core_user.idst IN (" . implode(",", $invitedUsers) . "),
						1,
						IF (
							core_user.idst IN (" . implode(",", $usersCanViewTheAsset) . "), 
							0, 
							2
						)
					)

				) AS invited,
				(
					IF(
						core_user.expiration IS NULL,
						1,
						IF (
							core_user.expiration > NOW(),   
							1, 
							0
						)
					)

				) AS validuser,
				

				(
					IF(
						core_user.idst IN (" . implode(",", $invitedUsers) . "),
						(SELECT date_format(created, '%Y-%m-%d') FROM app7020_invitations  WHERE idContent = " . $idContent . " AND idInvited=uid ORDER BY created DESC LIMIT 1),
						IF (
							core_user.idst IN (" . implode(",", $usersCanViewTheAsset) . "), 
							0, 
							2
						)
					)

				) AS date
				
				FROM core_user
				WHERE core_user.idst <> " . Yii::app()->user->idst . " AND core_user.valid = 1";

		if ($filter)
			$sql = $sql . " AND value like '%" . $filter;

		$sql = $sql . " HAVING  invited IN (" . implode(",", $invitedTypes) . " AND validuser = 1 )
				ORDER BY invited ASC, popular DESC

				";

		//if(!$filter)
		$sql = $sql . "LIMIT 0,5";


		$users = Yii::app()->db->createCommand($sql)->queryAll();

		//Get the list of channels
		$sql2 = "
			SELECT
			app7020_channels.id,
			app7020_channels.id as cId,
			icon_fa AS avatar,
			icon_color AS icolor,
			icon_bgr AS ibgr,
			1 AS type,
			0 AS invited,
			0 AS chanTrack,
			" . $idContent . " AS idcontent,
			(CASE WHEN trans.name IS NULL THEN transDef.name ELSE trans.name END) AS value
			FROM app7020_channels
				LEFT JOIN app7020_channel_translation trans
				ON trans.idChannel = app7020_channels.id AND trans.lang=:language
				LEFT JOIN app7020_channel_translation transDef
				ON transDef.idChannel = app7020_channels.id AND transDef.lang=:languageDef
			WHERE app7020_channels.id IN (" . implode(",", $channels) . ")
		";

		$params = array(
			':language' => $language,
			':languageDef' => $defaultLanguage
		);


		$result = array();

		if (!empty($channels)) {
			$channels = Yii::app()->db->createCommand($sql2)->queryAll(true, $params);
			foreach ($channels as $key => $value) {
				//Calculate channel users
				$channelUsers = App7020Channels::extractUserIds($value['cId']);
				$channels[$key]['userIds'] = implode(",", $channelUsers);
				$channels[$key]['username'] = (string) count($channelUsers);
			}
			$result = array_merge($users, $channels);
		} else
			$result = $users;

		return $asJson ? json_encode($result) : $result;
	}
	
	/**
	 * 
	 * @param type $idContent
	 * @return type
	 */
	public static function getInvitedUsers($idContent) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->selectDistinct("idInvited");
		$dbCommand->from('app7020_invitations');
		$dbCommand->where("idContent = :idContent", array(":idContent" => $idContent));
//		$result = 
//		$arrayInvited = array();
//		foreach ($result as $idUser) {
//			$arrayInvited[] = $idUser['idInvited'];
//		}
		return $dbCommand->queryScalar();
	}

	/**
	 * Store invite to watch items and fire up notificitaions on success 
	 * 
	 * @param array $params which data arguments should be stored 
	 * @param int $idAsset id of asset that should be invited users 
	 * @param array $fields  fields that are used to save into table 
	 * @return boolean
	 */
	public static function saveInvitations($params = array(), $idAsset, $fields = array('idInvited', 'idContent', 'idInviter')) {
		$transaction = null;
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}

        $inv = self::countMySendInvitations()["countMyInvitations"];

		try {
			$table = App7020Invitations::model()->tableName();
			$sql_values_statement = null;
			$sql = "INSERT IGNORE INTO {$table}(##fields##) VALUES ##values##";
			
			if (!empty($fields) && is_array($fields)) {
				$sql = str_replace('##fields##', implode(',', $fields), $sql);
			}
			//we cannot use this method withoud valid values
			if (empty($params)) {
				$transaction->rollback();
				return false;
			}
			$array_to_notification = array();
			foreach ($params as $single_row) {
				$array_to_notification [] = $single_row['idInvited'];
				self::_buildInsertValuesSqlStatement($single_row, $sql_values_statement);
			}
			$sql = str_replace('##values##', $sql_values_statement, $sql);
			$sql = rtrim($sql, ',' . PHP_EOL);

			$command = $db->createCommand($sql);

			$exec = $command->execute();
			
			if ($exec) {
				// let's notify users 
				//raise notification for invite to watch
				Yii::app()->event->raise('App7020Invitation', new DEvent(self, array('users' => $array_to_notification,
					'contentId' => $idAsset, 'idInviter' => Yii::app()->user->idst)));
                $_SESSION['time'] = NULL;
                foreach ($array_to_notification as $entry) {
                    Yii::app()->event->raise('UserReachedAGoal', new DEvent(self, array('goal' => 4, 'user' => Yii::app()->user->idst,'inv'=>$inv)));
                    $inv++;
                }

			}
			$transaction->commit();

			return $exec;
		} catch (Exception $ex) {
			
			$transaction->rollback();
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Build SQL VALUES statement for later insert 
	 * 
	 * @param array $single_row
	 * @param string $sql_values_statement
	 */
	private static function _buildInsertValuesSqlStatement($single_row, &$sql_values_statement) {
		$sql_values_statement .= ' ';
		$idInvited = $single_row['idInvited'];
		$idContent = $single_row['idContent'];
		$idInviter = $single_row['idInviter'];
		$sql_values_statement .= '("' . $idInvited . '", "' . $idContent . '", "' . $idInviter . '"),' . PHP_EOL;
	}

	/**
	 * Get all invitations by asset
	 * @param type $idContent
	 * @param type $searchText
	 * @param type $onlyNonWatched , 1-returns the list of users which are not watched the asset yet, 2 -returns the list of users which are  watched the asset.
	 * @return \CSqlDataProvider
	 */
	public static function getInvitationsDataProvider($idContent, $searchInput = null, $onlyNonWatched = null) {
		if (!$searchInput) {
			$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		}

		if (!$onlyNonWatched) {
			$onlyNonWatched = trim(Yii::app()->request->getParam('onlyNonWatched', false));
		}

		$sql = "SELECT ai.idInvited idi, TRIM(LEADING '/' FROM cu.userid) as username, "
				. "CONCAT(cu.firstname, ' ', cu.lastname) as fullName, "
				. "DATE(ai.created) as created, "
				. "DATE(ch.viewed) as watched, ai.created as invitedDateFull, "
				. "ch.viewed as watchedFull,"
				. "TIMESTAMPDIFF(SECOND, ai.created, ch.viewed) as reactionTime "
				. "FROM app7020_invitations ai INNER JOIN core_user cu ON ai.idInvited=cu.idst "
				. "LEFT JOIN app7020_content_history ch ON ai.idInvited=ch.idUser AND ch.idContent=" . $idContent . " AND "
				. "ch.viewed = (SELECT MIN(viewed) FROM app7020_content_history WHERE idUser=ai.idInvited AND idContent=" . $idContent . ") "
				. "WHERE ai.idContent=" . $idContent;
		if ($searchInput) {
			$sql .= " AND CONCAT(cu.firstname, ' ', cu.lastname, ' ', cu.userid) LIKE '%" . $searchInput . "%'";
		}

		if ($onlyNonWatched == 1) {
			$sql .= " AND ISNULL(ch.viewed) ";
		}

		if ($onlyNonWatched == 2) {
			$sql .= " AND NOT ISNULL(ch.viewed) ";
		}


		$sql .= " GROUP BY ai.idInvited";
		$dbCommand = Yii::app()->db->createCommand($sql);
		$returnArray = $dbCommand->queryAll();
		$pageSize = Settings::get('elements_per_page', 10);
		$config = array(
			'totalItemCount' => count($returnArray),
			'pagination' => array('pageSize' => $pageSize)
		);
		$dataProvider = new CArrayDataProvider($returnArray, $config);
		return $dataProvider;
	}
	
	
	/**
	 * 
	 * @param type $idInvited
	 * @param type $watched
	 * @return string
	 */
	public static function renderDeleteLink($idInvited, $watched) {
		if ($watched) {
			return "";
		}
		$content = Chtml::link(' <span class="fa fa-times"></span>', 'javascript:void(0);', array(
					'id' => 'delete-invitation-' . $idInvited,
					'class' => 'app7020_invitation_delete',
					'data-invited-id' => $idInvited,
		));
		return $content;
	}
	
	
	/**
	 * 
	 * @param type $idInvited
	 * @param type $watched
	 * @return string
	 */
	public static function renderReinviteLink($idInvited, $watched) {
		if ($watched) {
			return "";
		}
		$content = Chtml::link(' <span class="fa fa-repeat"></span>', 'javascript:void(0);', array(
					'id' => 'resend-invitation-' . $idInvited,
					'class' => 'app7020_invitation_resend',
					'data-invited-id' => $idInvited,
		));
		return $content;
	}
	
	
	/**
	 * 
	 * @param type $idContent
	 * @return type
	 */
	public static function getStatisticsData($idContent) {
		$total = count(self::getInvitationsDataProvider($idContent)->rawData);
		$watched = self::getInvitationsDataProvider($idContent, '', 2)->rawData;
		$notWatched = self::getInvitationsDataProvider($idContent, '', 1)->rawData;
		if ($total) {
			$percent = count($watched) * 100 / $total;
		} else {
			$percent = 0;
		}

		$seconds = 0;
		foreach ($watched as $value) {
			if ($value['reactionTime'] >= 0) {
				$seconds += $value['reactionTime'];
			}
		}

		if ($watched) {
			$average = $seconds / count($watched);
		} else {
			$average = 0;
		}


		$data = array(
			'totalInvitedPeople' => $total,
			'globalWatchRate' => floor($percent),
			'avgReactionTime' => $average,
			'watched' => count($watched),
			'notWatched' => count($notWatched)
		);

		return array(
			'data' => $data,
			'id' => 'manageInvitationStatistic',
		);
	}


	public function afterSave(){

    }

}
