<?php
/**
 *
 * The followings are the available columns in table 'core_job':
 * @property integer $id_job
 * @property string $hash_id
 * @property string $name
 * @property integer $id_author
 * @property integer $active
 * @property integer $is_running
 * @property integer $allow_multiple
 * @property string $created
 * @property string $updated
 * @property string $handler_id
 * @property string $type
 * @property string $rec_period
 * @property integer $rec_length
 * @property integer $rec_minute
 * @property integer $rec_hour
 * @property string $rec_timezone
 * @property integer $rec_day_of_week
 * @property integer $rec_day_of_month
 * @property string $start_date
 * @property string $cron_timing
 * @property string $last_started
 * @property string $last_finished
 * @property string $last_status
 * @property string $last_error_message
 * @property string $params
 *
 * The followings are the available model relations:
 * @property CoreUser $author
 * @property CoreJobLog[] $logs
 * @property CoreNotification[] $notifications
 * @property LearningReportFilter[] $reports
 *
 */
class CoreJob extends CActiveRecord
{

	/**
	 * Job scheduled to run at specific time/periods, predictable "next run" time (e.g. report generation)
	 * @var string
	 */
	const TYPE_RECURRING 		= 'recurring';

	/**
	 * Can be run/called at any time, basically at unpredictable "next run" time
	 * @var string
	 */
	const TYPE_RANDOM			= 'random';

	/**
	 * Again, can be run/called at any time, basically at unpredictable "next run" time; also, self deleting as being ONE time
	 * @var string
	 */
	const TYPE_ONETIME			= 'onetime';



	/**
	 * Job Statuses
	 *
	 * @var string
	 */
	const STATUS_SUCCESS				= 'success';
	const STATUS_FAILED					= 'failed';

	/**
	 * Recurring Jobs periods
	 * @var string
	 */
	const PERIOD_HOUR					= 'hour';
	const PERIOD_DAY					= 'day';
	const PERIOD_WEEK					= 'week';
	const PERIOD_MONTH					= 'month';
	const PERIOD_YEAR					= 'year';


	const PERIOD_METANAME_HOURLY		= 'hourly';
	const PERIOD_METANAME_DAILY			= 'daily';
	const PERIOD_METANAME_WEEKLY		= 'weekly';
	const PERIOD_METANAME_MONTHLY		= 'monthly';
	const PERIOD_METANAME_EVERY3MONTHS	= 'every3months';
	const PERIOD_METANAME_EVERY6MONTHS	= 'every6months';
	const PERIOD_METANAME_YEARLY		= 'yearly';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_job';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hash_id, name, created', 'required'),
			array('id_author, active, is_running, allow_multiple, rec_length, rec_minute, rec_hour, rec_day_of_week, rec_day_of_month', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>32),
			array('rec_timezone', 'length', 'max'=>32),
			array('cron_timing', 'length', 'max'=>100),
			array('hash_id', 'length', 'max'=>64),
			array('name', 'length', 'max'=>128),
			array('handler_id', 'length', 'max'=>255),
			array('rec_period, last_status', 'length', 'max'=>32),
			array('updated, start_date, last_started, last_finished, last_error_message, params', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_job, hash_id, name, id_author, active, is_running, allow_multiple, created, updated, handler_id, type, rec_period, rec_length, rec_minute, rec_hour, rec_timezone, rec_day_of_week, rec_day_of_month, start_date, cron_timing, last_started, last_finished, last_status, last_error_message, params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' 		=> array(self::BELONGS_TO, 'CoreUser', 'id_author'),
			'logs' 			=> array(self::HAS_MANY, 'CoreJobLog', 'id_job'),
			'notifications' => array(self::HAS_MANY, 'CoreNotification', 'id_job'),
			'reports' 		=> array(self::HAS_MANY, 'LearningReportFilter', 'id_job'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_job' => 'Job ID',
			'hash_id' => 'Hash ID',
			'name' => 'Name',
			'id_author' => 'Author ID',
			'active' => 'Active',
			'is_running' => 'Is Running',
			'allow_multiple' => 'Allow Multiple',
			'created' => 'Created',
			'updated' => 'Updated',
			'handler_id' => 'Handler ID',
			'type' => 'Job Type',
			'rec_period' => 'Rec Period',
			'rec_length' => 'Rec Length',
			'rec_minute' => 'Rec Minute',
			'rec_hour' => 'Rec Hour',
			'rec_timezone' => 'Rec Time Zone',
			'rec_day_of_week' => 'Rec Day Of Week',
			'rec_day_of_month' => 'Rec Day Of Month',
			'start_date' => 'Start Date',
			'cron_timing' => 'Cron Timing',
			'last_started' => 'Last Started',
			'last_finished' => 'Last Finished',
			'last_status' => 'Last Status',
			'last_error_message' => 'Last Error Message',
			'params' => 'Parameters',
		);
	}


	/**
	 * @see CModel::behaviors()
	 */
	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('created medium', 'updated medium', 'last_started medium', 'last_finished medium'),
			)
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('id_job',$this->id_job);
		$criteria->compare('hash_id',$this->hash_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_author',$this->id_author);
		$criteria->compare('active',$this->active);
		$criteria->compare('is_running',$this->is_running);
		$criteria->compare('allow_multiple',$this->allow_multiple);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('handler_id',$this->handler_id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('rec_period',$this->rec_period,true);
		$criteria->compare('rec_length',$this->rec_length);
		$criteria->compare('rec_minute',$this->rec_minute);
		$criteria->compare('rec_hour',$this->rec_hour);
		$criteria->compare('rec_timezone',$this->rec_timezone);
		$criteria->compare('rec_day_of_week',$this->rec_day_of_week);
		$criteria->compare('rec_day_of_month',$this->rec_day_of_month);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('cron_timing',$this->cron_timing,true);
		$criteria->compare('last_started',$this->last_started,true);
		$criteria->compare('last_finished',$this->last_finished,true);
		$criteria->compare('last_status',$this->last_status,true);
		$criteria->compare('last_error_message',$this->last_error_message,true);
		$criteria->compare('params',$this->params,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreJob the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Decompose DateTime parts into an array
	 *
	 * @param DateTime $dateTime
	 * @return array
	 */
	public static function decomposeDateTime(DateTime $dateTime) {
		$result = array();

		$result['year'] 		= (int) $dateTime->format('Y');
		$result['isleap'] 		= (int) $dateTime->format('L');
		$result['timezone']		= $dateTime->format('e');
		$result['month'] 		= (int) $dateTime->format('n');
		$result['dayofmonth'] 	= (int) $dateTime->format('j');
		$result['dayofweek'] 	= (int) $dateTime->format('N');
		$result['hour'] 		= (int) $dateTime->format('H');
		$result['minute'] 		= (int) $dateTime->format('i');
		$result['second'] 		= (int) $dateTime->format('s');

		return $result;

	}


	/**
	 * Compose a <2014-01-31 21:59:00> date string (ISO... format)
	 *
	 * @param array $p  Array date-time parts (see decomposeDateTime method)
	 * @param string $zeroSecond
	 * @return string
	 */
	public static function composeDateTime($p, $zeroSecond = true) {
		if ($zeroSecond)
			return "${p[year]}-${p[month]}-${p[dayofmonth]} ${p[hour]}:${p[minute]}:00";
		else {
			return "${p[year]}-${p[month]}-${p[dayofmonth]} ${p[hour]}:${p[minute]}:${p[second]}";
		}
	}


	/**
	 * Correct the day of the month: if it is greater than possible days for the month of the given date, set to last day of the month
	 *
	 * @param DateTime $dateTime
	 * @param integer $day
	 * @return integer
	 */
	protected function adjustDayOfMonth($dateTime, $day) {

		$month = (int) $dateTime->format('n');
		$year  = (int) $dateTime->format('Y');

		$numDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$day = $day > $numDays ? $numDays : $day;

		return $day;


	}


	/**
	 * Given a DateTime object, set its timing parts.
	 * Follows the policy: "out of range day is considered the last day of month"
	 *
	 * @param DateTime $dateTime
	 * @param DateTimeZone $timezone
	 * @param array $parts
	 * @return DateTime
	 */
	protected function setDateTimeParts(DateTime $dateTime, DateTimeZone $timezone, $parts) {
		$partNames = array('year', 'month', 'dayofmonth', 'hour', 'minute');
		$tmpParts = self::decomposeDateTime($dateTime);
		foreach ($partNames as $name) {
			$tmpParts[$name] = isset($parts[$name]) ? $parts[$name] : $tmpParts[$name];
		}

		$saveDay = $tmpParts['dayofmonth'];
		$tmpParts['dayofmonth'] = 1; // just temporary, will be restored and adjusted according to the "out of range days" policy

		// we need this to adjust the "out of range day"
		$tmp  = new DateTime(self::composeDateTime($tmpParts), $timezone);
		$tmpParts['dayofmonth'] = $this->adjustDayOfMonth($tmp, $saveDay);

		$result = new DateTime(self::composeDateTime($tmpParts), $timezone);

		return $result;
	}

	/**
	 * Helper method to get a job by hash id
	 *
	 * @param string $hash_id
	 *
	 * @return CoreJob
	 */
	public static function findByHashId($hash_id) {
		return self::model()->findByAttributes(array('hash_id' => $hash_id));
	}


	/**
	 * Build Cron job timing string for THIS job
	 */
	public function buildCronTiming() {

		$minutes = 0;
		$hours_diff = 0;
		if(!is_null($this->rec_timezone) && $this->rec_timezone <> '')
		{
			$utcTime = new DateTime('now', new DateTimeZone('GMT'));
			$currentTimezone = new DateTimeZone($this->rec_timezone);
			$offset = (int)$currentTimezone->getOffset($utcTime);
			$hours_diff = (int)($offset / 3600);
			if($hours_diff < 0 && ($offset%3600) != 0)
			{
				$minutes = ($offset%3600) / 60;
			}
			elseif($hours_diff > 0 && ($offset%3600) != 0)
			{
				$minutes = 60 - ($offset%3600) / 60;
				$hours_diff -= 1;
			}
			elseif($hours_diff == 0 && ($offset%3600) != 0)
			{
				if($offset < 0)
				{
					$minutes = ($offset%3600) / 60;
				}
				else
				{
					$minutes = 60 - ($offset%3600) / 60;
					$hours_diff -= 1;
				}
			}
		}
		
		switch ($this->type) {

			case self::TYPE_RANDOM:
			case self::TYPE_RECURRING:
				
				switch ($this->rec_period) {
					case self::PERIOD_HOUR:
						// Between 1..23 :  use  '0 */N * * *'   format
						if (( (int) $this->rec_length >= 1) && ( (int) $this->rec_length <= 23)) {
							$timing = $minutes.' */' . $this->rec_length . ' * * *';
						}
						// =24h, means every day. We set it to run every day at middle of the night UTC
						else if ((int) $this->rec_length == 24) {
							$hours = 0;  // @TODO play with timezone maybe; also use the current hour of the day???
							$timing = $minutes." $hours * * *";
						}
						else {
							$timing = $minutes.' * * * *';
						}
						break;
						
					case self::PERIOD_DAY:
						$hours = $this->rec_hour;

						if($hours_diff <> 0)
						{
							if(($this->rec_hour - $hours_diff) >= 24)
								$hours = $this->rec_hour - $hours_diff - 24;
							elseif(($this->rec_hour - $hours_diff) < 0)
								$hours = $this->rec_hour - $hours_diff + 24;
							else
								$hours = $this->rec_hour - $hours_diff;
						}

						// If more than one day is specified, it means "every N days"; use  "* * */N * *" format
						if ($this->rec_length > 1) {
							$timing = $minutes.' ' . (int) $hours . ' */' . $this->rec_length . ' * *';
						}
						else {
							$timing = $minutes.' ' . (int) $hours . ' * * *';
						}
						
						
						break;
					case self::PERIOD_WEEK:
						$hours = $this->rec_hour;
						$day = $this->rec_day_of_week;

						if($hours_diff <> 0)
						{
							if(($this->rec_hour - $hours_diff) >= 24)
							{
								$hours = $this->rec_hour - $hours_diff - 24;
								if($this->rec_day_of_week == 7)
									$day = 1;
								else
									$day = $this->rec_day_of_week + 1;
							}
							elseif(($this->rec_hour - $hours_diff) < 0)
							{
								$hours = $this->rec_hour - $hours_diff + 24;
								if($this->rec_day_of_week == 0)
									$day = 6;
								else
									$day = $this->rec_day_of_week - 1;
							}
							else
							{
								$hours = $this->rec_hour - $hours_diff;
							}
						}

						$timing = $minutes.' ' . (int) $hours . ' * * ' . (int) $day;
						break;
					case self::PERIOD_MONTH:
						$hours = $this->rec_hour;
						$day = $this->rec_day_of_month;
						$extra = '';

						if($hours_diff <> 0)
						{
							if(($this->rec_hour - $hours_diff) >= 24)
							{
								$hours = $this->rec_hour - $hours_diff - 24;

								if($this->rec_day_of_month == 31)
									$day = 1;
								else
									$day = $this->rec_day_of_month + 1;
							}
							elseif(($this->rec_hour - $hours_diff) < 0)
							{
								$hours = $this->rec_hour - $hours_diff + 24;
								if($this->rec_day_of_month == 1)
								{
									$day = '28-31';
									$extra = ' [ &quot;$(date +%d -d tomorrow)&quot; = &quot;01&quot; ] &amp;&amp;';
								}
								else
									$day = $this->rec_day_of_month - 1;
							}
							else
								$hours = $this->rec_hour - $hours_diff;
						}

						if($this->rec_length > 1) {
							$day = '*';
							$extra = '';
						}

						$timing = $minutes.' ' . (int) $hours . ' ' . $day . ' * *'.$extra;
						break;
					default:
						$timing = $minutes.' 0 * * *';
						break;
				}
				break;

			default:
				// Every day at middle of the night
				$timing = '0 0 * * *';
				break;
		}

		return $timing;
	}


	/**
	 * Create a NEW job
	 *
	 * @param string $name
	 * @param string $handler
	 * @param string $type
	 * @param string $recurringPeriod
	 * @param string $recurringLength
	 * @param string $atMinute
	 * @param string $atHour
	 * @param string $atDayOfWeek
	 * @param string $atDayOfMonth
	 * @param string $startDate
	 * @param string $params
	 * @param string $allowMultiple
	 * @param string $idUser
	 *
	 * @return CoreJob
	 */
	public static function createJob(
			$name,
			$handler,
			$type,
			$recurringPeriod= false,  // hourly, daily, weekly, monthly, yearly
			$recurringLength= false,  // every <N> <period>
			$atMinute		= false,  //
			$atHour			= false,
			$timeZone		= false,
			$atDayOfWeek	= false,
			$atDayOfMonth	= false,
			$startDate		= false,  // At which date the job must be run for the first time
			$params			= false,
			$allowMultiple 	= false,
			$idUser			= false
	) {

	    $idUser = $idUser ? (int) $idUser : (isset(Yii::app()->user) ? Yii::app()->user->id : null);
	    
		if (!$params) {
			$params = array();
		}
		$job = new self();
		$job->hash_id 			= Docebo::randomHash();
		$job->name 				= $name;
		$job->id_author			= $idUser;
		$job->active			= 1;
		$job->is_running		= 0;
		$job->allow_multiple	= $allowMultiple ? 1 : 0;
		$job->created			= Yii::app()->localtime->getLocalNow();

		// We assume last start was when job has been created and it was successful
		$job->last_started		= $job->created;
		$job->last_finished		= $job->created;
		$job->last_status		= self::STATUS_SUCCESS;

		$job->handler_id		= $handler;
		$job->type				= $type;
		$job->rec_period		= $recurringPeriod;
		$job->rec_length		= $recurringLength ? (int) $recurringLength : 1;
		$job->rec_minute		= (int) $atMinute;
		$job->rec_hour			= (int) $atHour;
		$job->rec_timezone		= $timeZone;
		$job->rec_day_of_week	= $atDayOfWeek ? $atDayOfWeek : 1;
		$job->rec_day_of_month	= $atDayOfMonth ? $atDayOfMonth : 1;
		$job->start_date		= $startDate ? $startDate :  Yii::app()->localtime->getLocalNow();
		$job->params			= CJSON::encode($params);
		$job->cron_timing		= $job->buildCronTiming();

		$job->save();

		return $job;

	}


	/**
	 * Return when this job is due to run next time since <dtPivot> point in time, in <strong>UTC TZ</strong>
	 * NOTE: everything is in UTC! Please pass parameters in UTC TZ
	 *
	 * @param DateTime $dtPivot (UTC) Return job due time AFTER this point in time (or after NOW if omitted)
	 * @param DateTime $dtRecStart (UTC) For <every Nth month> only. When job is sheduled to start for the first time ever
	 * @return DateTime (UTC) Next time of run
	 */
	public function nextRun(DateTime $dtPivot = null, DateTime $dtRecStart = null) {

		// Make this DateTimeZone object, to have it for later use
		$tzUTC = new DateTimeZone('UTC');

		// Lets have a today DateTime object
		$dtNow = new DateTime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'), $tzUTC);
		$partsNow = self::decomposeDateTime($dtNow);

		// Looking for Next Run After ....
		$dtPivot = $dtPivot ? $dtPivot : $dtNow;
		$partsPivot = self::decomposeDateTime($dtPivot);

		// Init
		$dtNextRun = null;


		switch ($this->rec_period) {

			// Monthly runs: every <rec_length>-th month (3|6|12|x) STARTING from <start_date> at <rec_hour:rec_minute>
			// Monthly runs: every month on <rec_day_of_month> at <rec_hour:rec_minute>
			case self::PERIOD_MONTH:
				$recurringLength = $this->rec_length;
				// If we get a Specific STARTING POINT (job must start running AT that start point)
				if ($dtRecStart) {
					$diff = $dtRecStart->diff($dtPivot);
					// If Pivot is After Start.... we need to walk ahead until we pass over the pivot
					// Increment by number of months of the recurring period
					if ($diff->invert == 0) {
						$interval = new DateInterval('P' . $recurringLength . 'M');
						$dtNextRun = $dtRecStart;
						while ($dtNextRun->add($interval)->diff($dtPivot)->invert == 0) {}
					}
					// Well, looks like START is After Pivot, then next run is just the START point
					else {
						$dtNextRun = $dtRecStart;
					}
				}
				// No starting point, this is "EVERY SINGLE MONTH on specific date" case
				else {
					$jobDay = $this->adjustDayOfMonth($dtPivot, $this->rec_day_of_month);
					$partsJob = $partsPivot;
					$partsJob['dayofmonth'] = $jobDay;
					$partsJob['hour'] 	= $this->rec_hour;
					$partsJob['minute'] = $this->rec_minute;
					$dtNextRun = new DateTime(self::composeDateTime($partsJob), $tzUTC);

					$diff = $dtPivot->diff($dtNextRun);
					// Pivot is After the monthly job time (job time passed)? Move to next month then
					if ($diff->invert == 1) {
						$partsJob['dayofmonth'] = 1;  // set this temporary, umtil we move to next month and adjust the day according to the month's number of days
						$dtNextRun = new DateTime(self::composeDateTime($partsJob), $tzUTC);
						$dtNextRun->add(new DateInterval('P1M'));
						$jobDay = $this->adjustDayOfMonth($dtNextRun, $this->rec_day_of_month);
						$dtNextRun = $this->setDateTimeParts($dtNextRun, $tzUTC, array('dayofmonth' => $jobDay));
					}
				}
				break;


			// Weekly runs: Every week on <rec_day_of_week> at <rec_hour:rec_minute>
			case self::PERIOD_WEEK:
				$absDiffDays = abs($this->rec_day_of_week - $partsPivot['dayofweek']);

				// If the day of the weeks are different (target day and pivot day)
				if ($partsPivot['dayofweek'] != $this->rec_day_of_week) {

					if ($partsPivot['dayofweek'] < $this->rec_day_of_week) {
						$daysAhead 	= $absDiffDays;
					}
					else {
						$daysAhead = 7  - $absDiffDays;
					}

					$interval 	= new DateInterval('P' . $daysAhead . 'D');
					$partsJob 	= self::decomposeDateTime($dtPivot);
					$partsJob['hour'] 	= $this->rec_hour;
					$partsJob['minute'] = $this->rec_minute;
					$dtNextRun 			= new DateTime(self::composeDateTime($partsJob), $tzUTC);
					$dtNextRun->add($interval);
				}
				// Same day of week? Lets check if the Time Of day have passed or not: passed? => Next week. Not yet? => Today !
				else {
					$partsJob = $partsPivot;
					$partsJob['hour'] 		= $this->rec_hour;
					$partsJob['minute'] 	= $this->rec_minute;
					$dtNextRun				= new DateTime(self::composeDateTime($partsJob), $tzUTC);
					$diff 					= $dtNextRun->diff($dtPivot);

					if (!$diff->invert) { //  $dtNextRun > $dtPivot
						$interval = new DateInterval('P7D');
						$dtNextRun->add($interval);
					}

				}
				break;


			// Daily runs (every day at rec_hour:rec_minute)
			case self::PERIOD_DAY:
				$partsJob = $partsPivot;
				$partsJob['hour'] 		= $this->rec_hour;
				$partsJob['minute'] 	= $this->rec_minute;
				$dtNextRun				= new DateTime(self::composeDateTime($partsJob), $tzUTC);
				$diff 					= $dtNextRun->diff($dtPivot);
				if (!$diff->invert) {
					$interval = new DateInterval('P1D');
					$dtNextRun->add($interval);
				}
				break;

			// Run every rounded hour
			case self::PERIOD_HOUR:
				$dtNextRun = new DateTime($dtPivot->format('Y-m-d H:00:00'), $tzUTC); // round hour of the pivot
				$dtNextRun->add(new DateInterval('PT1H')); // get next hour
				break;

		}


		return $dtNextRun;

	}



	/**
	 * Return the next run from NOW, in <strong>UTC TZ</strong>
	 *
	 * @return DateTime  The next run time in <strong>UTC TZ</strong>
	 */
	public function nextRunFromNow() {
		$dtRecStart = null;
		$dtPivot = null;
		// For periods like "every N months" and "every Year" (every 12 months) we need to provide the job STARTING DATE
		if (($this->rec_period == self::PERIOD_MONTH) && ((int) $this->rec_length > 1) && $this->start_date) {
			$utcStartDate = Yii::app()->localtime->fromLocalDateTime($this->start_date) . " " . $this->rec_hour . ":" . $this->rec_minute . ":00";
			$dtRecStart = new DateTime($utcStartDate, new DateTimeZone('UTC'));
		}

		if($this->rec_timezone) {
			$now = new DateTime('NOW', new DateTimeZone($this->rec_timezone));
			$dtPivot = new DateTime($now->format('Y-m-d H:i:s'), new DateTimeZone('UTC'));
		}
		$nextRun = $this->nextRun($dtPivot, $dtRecStart);
		return $nextRun;
	}


	/**
	 * Check if this job is due (e.g. must be executed)
	 *
	 * @return boolean
	 */
	public function isRecurringJobDue() {

		//Yii::log("Check if Job is Due [$this->id_job] [$this->hash_id] [$this->name]", 'info');

		$dtRecStart = null;
		$currentTimezone = ($this->rec_timezone)? $this->rec_timezone : 'UTC';
		if (($this->rec_period == self::PERIOD_MONTH) && ((int) $this->rec_length > 1) && $this->start_date) {
			$utc = Yii::app()->localtime->fromLocalDateTime($this->start_date) . " " . $this->rec_hour . ":" . $this->rec_minute . ":00";
			$dtRecStart = new DateTime($utc, new DateTimeZone($currentTimezone));
			$dtRecStart->setTimezone(new DateTimeZone('UTC'));
		}
		if ($this->last_started) {
			$utc = Yii::app()->localtime->fromLocalDateTime($this->last_started);
			$dtPivot = new DateTime($utc, new DateTimeZone('UTC'));
		}

		//convert to UTC from the selected timezone(everything in nextRun() must be in UTC)
		$this->convertTime(new DateTimeZone($currentTimezone), new DateTimeZone('UTC'));
		$dtNextRun = $this->nextRun($dtPivot, $dtRecStart);
		$dtNow = new DateTime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'), new DateTimeZone('UTC'));

		//return back the selected timezone
		$this->convertTime(new DateTimeZone('UTC'), new DateTimeZone($currentTimezone));

		//Yii::log('Pivotal Time : ' . $dtPivot->format('Y-m-d H:i:s'), 'info');
		//Yii::log('Next Run Time: ' . $dtNextRun->format('Y-m-d H:i:s'), 'info');

		$diffSecs =  $dtNextRun->getTimestamp() - $dtNow->getTimestamp();

		// Job is due starting from 60 secs before the NextRun (just a threshold)
		$isDue = $diffSecs < 60;

		//Yii::log('Is Due ?: ' . ($isDue ? 'YES' : 'NO'), 'info');

		return  $isDue;  // allow cron job to call us 60 secs before the expected Runtime



	}


	/**
	 * Retrun recurring job period metaname/metaid for this job (used in various UIs)
	 *
	 * @return string
	 */
	public static function resolveRecurringPeriodMetaname($period, $length) {

		switch($period) {

			case self::PERIOD_HOUR:
				return self::PERIOD_METANAME_HOURLY;
				break;

			case self::PERIOD_DAY:
				return self::PERIOD_METANAME_DAILY;
				break;

			case self::PERIOD_WEEK:
				return self::PERIOD_METANAME_WEEKLY;
				break;

			case self::PERIOD_MONTH:
				if ($length == 3) {
					return self::PERIOD_METANAME_EVERY3MONTHS;
				}
				else if ($length == 6) {
					return self::PERIOD_METANAME_EVERY6MONTHS;
				}
				else if ($length == 1) {
					return self::PERIOD_METANAME_MONTHLY;
				}
				else if ($length == 12) {
					return self::PERIOD_METANAME_YEARLY;
				}
				break;

			case self::PERIOD_YEAR:
				return self::PERIOD_METANAME_YEARLY;

		}

		return '';


	}




	/**
	 * Return an array of possible RECURRING periods
	 *
	 * @return array An associative array of <meta-name> => <translated-label>
	 */
	public static function getRecurringPeriodsList() {

		$data = array(
			self::PERIOD_METANAME_HOURLY 		=> Yii::t('report', 'Hourly'),
			self::PERIOD_METANAME_DAILY 		=> Yii::t('classroom', 'Daily'),
			self::PERIOD_METANAME_WEEKLY		=> Yii::t('report', 'Weekly'),
			self::PERIOD_METANAME_MONTHLY		=> Yii::t('classroom', 'Monthly'),
			self::PERIOD_METANAME_EVERY3MONTHS	=> Yii::t('report', 'Every 3 months'),
			self::PERIOD_METANAME_EVERY6MONTHS	=> Yii::t('report', 'Every 6 months'),
			self::PERIOD_METANAME_YEARLY		=> Yii::t('report', 'Every year'),
		);

		return $data;

	}


	/**
	 * Return "schedule information" in a humanly readable format, like 'Weekly, on Monday, at 12:00'
	 *
	 */
	public function getRecurringScheduleInfoText() {

		$weekDays = Docebo::getWeekDays();
		$periodsList = self::getRecurringPeriodsList();
		$periodMetaname = self::resolveRecurringPeriodMetaname($this->rec_period, $this->rec_length);
		$periodText = $periodsList[$periodMetaname];

		$parts = array();
		$parts[] = $periodsList[$periodMetaname];

		$hour = $this->rec_hour;
        $currentTimeZone = ($this->rec_timezone)? $this->rec_timezone : 'UTC' ;
        $offset = " ".(Yii::app()->localtime->getTimezoneOffset($currentTimeZone))." ".$currentTimeZone;

		switch($this->rec_period) {
			case self::PERIOD_HOUR:
				break;

			case self::PERIOD_DAY:
				$parts[] = Yii::t('report', 'at') . ' ' . $hour . ":00" . $offset;
				break;

			case self::PERIOD_WEEK:
				$parts[] = Yii::t('report', 'On') . ' ' . $weekDays[$this->rec_day_of_week];
				$parts[] = Yii::t('report', 'at') . ' ' . $hour . ":00" . $offset;
				break;

			case self::PERIOD_MONTH:
				if ($this->rec_length> 1) {
					$parts[] = Yii::t('report', 'Starting from') . ' ' . $this->start_date;
				}
				else  {
					$parts[] = Yii::t('report', 'On day') . ' ' . $this->rec_day_of_month;
				}
				$parts[] = Yii::t('report', 'at') . ' ' . $hour . ":00" . $offset;
				break;

			case self::PERIOD_YEAR:
				$parts[] = Yii::t('report', 'Starting from') . ' ' . $this->start_date;
				$parts[] = Yii::t('report', 'at') . ' ' . $hour . ":00" . $offset;
				break;
		}


		$result = implode(', ', $parts);

		return $result;

	}


	/**
	 * @return string
	 */
	public function getNextRunFromNowInfoText() {
		$dtNextRun = $this->nextRunFromNow();
		$dateNextRun = $dtNextRun->format('Y-m-d'); // UTC format!!!
		$dateNextRun = Yii::app()->localtime->toLocalDate($dateNextRun); // Local format
		$hour = $dtNextRun->format('G'); // Hour, UTC TZ
        $currentTimeZone = ($this->rec_timezone)? $this->rec_timezone : 'UTC' ;
        //$currentTimeZone = '';
        $offset = " ".(Yii::app()->localtime->getTimezoneOffset($currentTimeZone))." ".$currentTimeZone;

		$text = $dateNextRun . " " . $hour . ":00" . $offset;
		return $text;
	}

	/**
	 * Convert scheduled time
	 * @param DateTimeZone $fromTZ
	 * @param DateTimeZone $toTZ
	 */
	public function convertTime(DateTimeZone $fromTZ, DateTimeZone $toTZ) {
		$dt = new DateTime('NOW', $fromTZ);
		$dt->setTime($this->rec_hour, 0);

		if($this->rec_period == self::PERIOD_MONTH)
			$dt->setDate($dt->format('Y'), $dt->format('m'), $this->rec_day_of_month);

		$dt->setTimezone($toTZ);
		$this->rec_hour = $dt->format('G');

		switch($this->rec_period) {
			case self::PERIOD_WEEK :
				$this->rec_day_of_week = $dt->format('N');
				break;
			case self::PERIOD_MONTH :
				$this->rec_day_of_month = $dt->format('j');
				break;
			default:
				break;
}
	}

}
