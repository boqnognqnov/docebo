<?php

/**
 * This is the model class for table "core_enroll_rule_item".
 *
 * The followings are the available columns in table 'core_enroll_rule_item':
 * @property integer $rule_item_id
 * @property integer $rule_id
 * @property string $item_type
 * @property integer $item_id
 * @property integer $selection_state
 *
 * The followings are the available model relations:
 * @property CoreEnrollRule $rule
 */
class CoreEnrollRuleItem extends CActiveRecord
{
	const ITEM_TYPE_GROUP = 'group';
	const ITEM_TYPE_BRANCH = 'branch';
	const ITEM_TYPE_COURSE = 'course';
	const ITEM_TYPE_CURRICULA = 'curricula';

	const SELECTION_STATE_BRANCH_ONLY = 1;
	const SELECTION_STATE_BRANCH_AND_DESCENDANTS = 2;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreEnrollRuleItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_enroll_rule_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rule_id, item_type, item_id', 'required'),
			array('rule_id, item_id, selection_state', 'numerical', 'integerOnly'=>true),
			array('item_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rule_id, item_type, item_id, selection_state', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rule' => array(self::BELONGS_TO, 'CoreEnrollRule', 'rule_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rule_id' => 'Rule',
			'item_type' => 'Item Type',
			'item_id' => 'Item',
			'selection_state' => 'Selection State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('rule_id',$this->rule_id);
		$criteria->compare('item_type',$this->item_type,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('selection_state',$this->selection_state);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function removeAllByTypeAndRule($type, $ruleId)
	{
		self::model()->deleteAllByAttributes(array(
			'item_type' => $type,
			'rule_id' => $ruleId,
		));
	}

}