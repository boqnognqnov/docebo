<?php
/**
 * @property integer $id
 * @property integer $idReference
 * @property string $lms_original_filename
 * @property integer $lms_id_user
 * @property string $path
 *
 * AICC CRS mandatory data (CMI001v4)
 *
 * @property string $course_creator
 * @property string $course_id
 * @property string $course_system
 * @property string $course_title
 * @property string $level
 * @property integer $max_fields_cst
 * @property integer $total_aus
 * @property integer $total_blocks
 * @property string $version
 * @property integer $max_normal
 * @property string $course_description
 *
 * The followings are the available model relations:
 * @property LearningAiccItem[] $items
 * @property LearningAiccItem $masterItem
 * @property LearningOrganization $learningObject
 * @property LearningAiccPackageTrack[] $packageTracks
 */

class LearningAiccPackage extends CActiveRecord {

	static $FILES_BLACK_LIST = array('asp','aspx', 'bat', 'cgi', 'com', 'exe', 'fla', 'jsp', 'php', 'cfm', 'cfml');

	/**
	 * Various Model scenarios
	 *
	 * @var string
	 */
	const SCENARIO_CREATENEW = 'createnew';
	const SCENARIO_UPDATEOLD = 'update';


	const ITEM_TYPE_AU 				= 'au';
	const ITEM_TYPE_OBJECTIVE 		= 'objective';
	const ITEM_TYPE_BLOCK 			= 'block';


	/**
	 * These attributes will hold course metadata temporarily (filled upon package extraction) , until they are parsed and other tables are populated
	 *
	 * @var mixed
	 */
	private $crs;	// Course description
	private $au;	// AUs list (Assignable Units)
	private $des;	// Elements descriptions
	private $cst;	// Course structure
	private $pre;	// Prerequisite logic
	private $ort;   // Object rerlations
	private $cmp;	// Completion logic

	/**
	 * Aicc elements, a.k.a. Aicc Items
	 * @var array
	 */
	private $elements;


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_aicc_package';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

		$rules =  array(
			array('lms_original_filename, lms_id_user, path', 'required'),
			array('idReference, lms_id_user, max_fields_cst, total_aus, total_blocks, max_normal', 'numerical', 'integerOnly'=>true),
			array('lms_original_filename, path, course_creator, course_id, course_system, course_title', 'length', 'max'=>255),
			array('level, version', 'length', 'max'=>8),
			array('course_description', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idReference, lms_original_filename, lms_id_user, path, course_creator, course_id, course_system, course_title, level, max_fields_cst, total_aus, total_blocks, version, max_normal, course_description', 'safe', 'on'=>'search'),
		);

		// Follow STRICT AICC specification if Yii parameter "aicc_strict" is set to 1 or true
		// By default, as of this typing, it is NOT (1 May 2015), so we start with more relaxed implementation
		if (isset(Yii::app()->params['aicc_strict']) && Yii::app()->params['aicc_strict']) {
			$rules[] = array('path, course_creator, course_id, course_system, course_title, level, max_fields_cst, total_aus, total_blocks, version, max_normal, course_description', 'required');
		}

		return $rules;

	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			// All items from the AICC package
			'items' 			=> array(self::HAS_MANY, 'LearningAiccItem', 'id_package'),

			// The "master" item, the root of all items of this package
			'masterItem'		=> array(self::HAS_ONE, 'LearningAiccItem', 'id_package', 'on' => "parent='/'"),

			// All saved tracking for this package (all users); NOTE:  package level tracking (master item)
			'packageTracks' 	=> array(self::HAS_MANY, 'LearningAiccPackageTrack', 'id_package'),

			// The LO this AICC package is associated with
			'learningObject' 	=> array(self::BELONGS_TO, 'LearningOrganization', 'idReference'),

		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idReference' => 'Id Reference',
			'lms_original_filename' => 'Original Filename',
			'lms_id_user' => 'Lms User',
			'path' => 'Relative Path',
			'course_creator' => 'Course Creator',
			'course_id' => 'Course ID',
			'course_system' => 'Course System',
			'course_title' => 'Course Title',
			'level' => 'Level of complexity',
			'max_fields_cst' => 'Max Fields Cst',
			'total_aus' => 'Total ASs',
			'total_blocks' => 'Total Blocks',
			'version' => 'AICC Version',
			'max_normal' => 'Max Normal',
			'course_description' => 'Course Description',
		);
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() 	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('lms_original_filename',$this->lms_original_filename,true);
		$criteria->compare('lms_id_user',$this->lms_id_user);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('course_creator',$this->course_creator,true);
		$criteria->compare('course_id',$this->course_id,true);
		$criteria->compare('course_system',$this->course_system,true);
		$criteria->compare('course_title',$this->course_title,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('max_fields_cst',$this->max_fields_cst);
		$criteria->compare('total_aus',$this->total_aus);
		$criteria->compare('total_blocks',$this->total_blocks);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('max_normal',$this->max_normal);
		$criteria->compare('course_description',$this->course_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/**
	 * @see CModel::behaviors()
	 */
 	public function behaviors() {
 		return array(
 			'learningObjects' => array(
 				'class' => 'LearningObjectsBehavior',
 				'objectType' => LearningOrganization::OBJECT_TYPE_AICC,
 				'idPropertyName' => 'id',
 				'titlePropertyName' => 'course_title'
 			)
 		);
 	}


	/**
	 * Extract Aicc ZIP package locally, load course metadata and then store course data into final storage collection
	 *
	 * @param string $archiveFilePath  The ZIP file path
	 * @param string $destPath Optional, if not set the model path will be used if set, otherwise new one will be generated randomly
	 *
	 * @throws CException
	 * @return boolean
	 */
	public function extractPackage($archiveFilePath, $destPath=false, $copyFile=false) {

		if ($destPath)
			$this->path = $destPath;

		if (!$this->path)
			$this->path = Docebo::randomHash();

		// Local path where ZIP is going to be extracted
		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->path;

		// check if a previous upload (path) exists; create folder if not
		if (!file_exists($unpackPath)) {
			if ( ! mkdir($unpackPath, 0777) ) {
				throw new CException("Directory creation failed.");
			}
		}

		// ZIP Management
		$zip = new ZipArchive();

		// Open the zip file
		$zip_open = $zip->open($archiveFilePath);

		// Did we succeed in opening the ZIP file?
		if ($zip_open !== true) {
			throw new CException(Yii::t('player', 'This file is a not valid Zip Package (Error: {error_code})', array('{error_code}' => $zip_open)) .
					'. ' . Yii::t('player', 'Please check if your package is correct and if the problem persist report the error message to the help desk team'), $zip_open);
		}

		// Checking if it is a valid AICC package
		$error = false;
		if (!$this->validatePackage($zip, $error)) {
			$zip->close();
			throw new CException(Yii::t('player', 'Invalid AICC package') . ($error ? " : " . $error : ""));
		}

		// Extract the package
		if($zip->extractTo($unpackPath) !== true) {
			FileHelper::removeDirectory($unpackPath);
			throw new CException(Yii::t('player', 'Zip decompression failed (Error: {error_code})', array('{error_code}' => $zip->getStatusString())) .
					'. ' . Yii::t('player', 'Please check if your package is correct and if the problem persist report the error message to the help desk team'), $zip->getStatusString());
		}
		$zip->close();

		// Save the package structure in it's final directory
		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
		if ( ! $storageManager->store($unpackPath)) { throw new CException("Error while saving files to final storage."); }

		// Rename the original zip using the hash
		$newZippedAicc = $unpackPath.".zip";
		if($copyFile){
			@copy($archiveFilePath, $newZippedAicc);
		}else{
			@rename($archiveFilePath, $newZippedAicc);
		}

		if ( ! $storageManager->store($newZippedAicc)) { throw new CException("Error while saving file to final storage."); }


		// Load metadata from local unpacked files into object
		// Do this BEFORE removing the local files!!
		$this->loadCourseMetadata();

		// remove tmp unpack dir and zip file
		FileHelper::removeDirectory($unpackPath);
		FileHelper::removeFile($newZippedAicc);

		return true;



	}


	/**
	 * Check if the archive contains minimal valid files, constituting it as AICC package (CMI001v4; CMI012)
	 *
	 * @param ZipArchive $zipObject
	 * @return boolean
	 */
	private function validatePackage($zipObject, &$error = false) {

		$crs 	= false;
		$au 	= false;
		$des 	= false;
		$cst 	= false;

		for( $i = 0; $i < $zipObject->numFiles; $i++ ){
			$stat = $zipObject->statIndex( $i );

			$path_parts = pathinfo($stat['name']);
			if (in_array($path_parts['extension'], self::$FILES_BLACK_LIST)) {
				$error = Yii::t('standard',  'Forbidden content found in the package ({ext})', array('{ext}' => $path_parts['extension']));
				return false;
			}

			// CMI012: Zip SHALL include at least 4 files:
			// CRS : Course description
			// AU  : Assignable Unit
			// DES : Descriptor
			// CST : Course Structure
			if (preg_match('/\.crs$/i', $stat['name'])) 	$crs = true;
			if (preg_match('/\.au$/i', $stat['name']))	$au = true;
			if (preg_match('/\.des$/i', $stat['name'])) 	$des = true;
			if (preg_match('/\.cst$/i', $stat['name'])) 	$cst = true;

		}

		// All files available in the package? Good!
		if ($crs && $au && $des && $cst) {
			return true;
		}

		$error = Yii::t('standard',  'Could not find all required files in the package');

		return false;
	}


	/**
	 * Load course metadata into private object attributes for later examination and analysys
	 *
	 */
	private function loadCourseMetadata() {

		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->path;

		if (!is_dir($unpackPath)) {
			throw new Exception('Could not find uploaded data in the temporary folder');
		}

		// Files must be in ROOT level! (0)
		$files =  FileHelper::findFiles($unpackPath, array('fileTypes' => array('crs', 'CRS','au', 'AU', 'des', 'DES', 'cst', 'CST', 'pre', 'PRE', 'ort', 'ORT', 'cmp', 'CMP'), 'level' => 0));
		if ($files && is_array($files) && count($files) > 0) {
			foreach ($files as $file) {
				$extension = strtolower(FileHelper::getExtension($file));
				if (property_exists($this, $extension)) {
					$this->$extension = file_get_contents($file);
				}
			}
		}
	}


	/**
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave() {
		$this->parseCrs();
		return parent::beforeSave();
	}


	/**
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {

		// Now that we have the package saved, parse SCO information and create relevant records
		$rootSystemId = $this->course_id;
		if($this->scenario != 'copy'){

			if($this->scenario == 'centralRepo'){
				$this->detachBehavior('learningObjects');
			}

			// Create Root element ("root" block)
			$this->elements = array();
			$this->elements[] = array(
				'system_id' => $rootSystemId,
				'title'		=> $this->course_title,
				'parent'	=> '/',
			);

			// Parse all AICC metadata files, IN THIS order
			$this->parseDes();

			// the system_id of the "root" block has to be unique
			$cnt = 0;
			foreach($this->elements as $element) {
				if($element['system_id'] == $rootSystemId)
					$cnt++;
			}
			if($cnt > 1) {
				$rootSystemId .= '_course_id';
				$this->elements[0]['system_id'] = $rootSystemId;
			}

			$this->parseAus();
			$this->parseCst();
			$this->parseOrt();
			$this->parsePre();
			$this->parseCmp();

			// Did we collect AICC elements (items) at all?
			if (isset($this->elements)) {

				// Use this counter for sorting order
				$counter = 0;

				LearningAiccItem::model()->deleteAll('id_package=:id_package', array(':id_package' => $this->id));

				// Enymerate all collected elements and create Aicc Aitems, one per element
				foreach ($this->elements as $index => $element) {


					// Make sure we have a title, it is required
					$element['title'] = $element['title'] ? $element['title'] : $this->course_title;

					$item = new LearningAiccItem();

					// If we have an item model
					if ($item) {

						// Enumerate Elements Array and assign its values to AICC Item Model attribute, if it exists
						// This is to make the workflow more flexible and allow us to add more attributes to the model without hard coding
						foreach ($element as $key => $value) {
							// Check if model attribute exists with the same name
							if (in_array($key, $item->attributeNames()) && $key != $item->tableSchema->primaryKey) {
								$item->$key = $value;
							}
						}

						// Assign AICC package ID to the item
						$item->id_package 	= $this->id;

						// And the "launch" attribute
						$item->launch 		= $element['file_name'];  // we use just another name

						// Normalize parent

						// No parent means item is a member of the "root" block
						if (!isset($item->parent)) {
							// Using "/" ruins the structure very easy if some element is not listed in Course Structure files
							//$item->parent = "/";
							$item->parent = $rootSystemId;
						}
						// The root block's system_id is equal to the course_id, so every item being a child of "root" is a child of system_id=course_id
						else if (strtolower($item->parent) == 'root') {
							$item->parent = $rootSystemId;
						}

						// Set sorting, for now, natural, in the order of adding the elements
						$item->sorting		= $counter;


						// Objectives are: having parent NON "/" && having no type defined
						if ( ($item->parent != "/") && empty($item->launch) && empty($item->item_type)) {
							$item->item_type = LearningAiccPackage::ITEM_TYPE_OBJECTIVE;
						}

						// Validate and save
						if (!$item->save()) {
							Yii::log('Unable to save Aicc Item: ', CLogger::LEVEL_ERROR);
							Yii::log('Errors: ' . var_export($item->errors, true), CLogger::LEVEL_ERROR);
							throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
						}
					}
					else {
						Yii::log('Unable to create or find an Aicc Item model', CLogger::LEVEL_ERROR);
						throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
					}

					$counter++;

				}


			}
			else {
				Yii::log('AICC Ietem elements is empty', CLogger::LEVEL_ERROR);
				throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			// Update all items with information about childs and descendants
			$this->updateItemsTreeCounters();
		}
		// --- don't forget this!
		// --- It will create the Learning Object
		return parent::afterSave();
	}


	/**
	 * Given the package items populated in the AIIC Items table, update thier children/descendants counters
	 */
	protected function updateItemsTreeCounters() {
		$items = $this->items;
		foreach ($items as $item) {

			// Count & Save playable units (AUs)among children and among all descendants
			$countAus = 0;
			$item->countChildren($countAus);
			$item->nChild = $countAus;

			$countAus = 0;
			$item->countDescendants(false, $countAus);
			$item->nDescendant = $countAus;

			$item->save();
		}
	}


	/**
	 * Parse CRS data, LO level information (a course, in terms of AICC)
	 */
	private function parseCrs() {

		if (isset($this->crs)) {
			//$rows = explode("\r\n", $this->crs);
			$rows = preg_split('/(\r\n)|(\n\r)|(\n)|(\r)/', $this->crs);
			if (is_array($rows)) {
				foreach ($rows as $row) {
					if (preg_match("/^(.+)=(.+)$/", $row, $matches)) {
						$parameterName 	= strtolower(trim($matches[1]));  	// left side
						$parameterValue = trim($matches[2]);				// right side
						if (in_array($parameterName, $this->attributeNames())) {
							$this->$parameterName = $parameterValue;
						}
					}
				}
			}
		}
	}


	/**
	 * Parse and load course elements, i.e. SCOs, from course descriptor (DES)
	 *
	 * @return array
	 */
	private function parseDes() {
		$csvParser = new CSVParser();
		if (isset($this->des)) {
			$this->elements = array_merge($this->elements, $csvParser->parseText($this->des, array('hasHeader' => true)));
		}
	}


	/**
	 * Parse list of Assignable Units (AU) and add information to already built earlier array of elements (per element additional info)
	 */
	private function parseAus() {
		$csvParser = new CSVParser();
		if (isset($this->au)) {
			$aus = $csvParser->parseText($this->au, array('hasHeader' => true));
			foreach ($this->elements as $index => $element) {
				foreach ($aus as $au) {
					if ($au['system_id'] == $element['system_id']) {
						foreach ($au as $auParamName => $auParamValue) {
							$this->elements[$index][$auParamName] = $auParamValue;
						}
						// Set item_type to AU (assignable unit), the aonly unit to play
						$this->elements[$index]['item_type'] = self::ITEM_TYPE_AU;
					}
				}
			}
		}
	}


	/**
	 * Parse Course Structure data (CST) and add "parent" to all elements involved
	 *
	 */
	private function parseCst() {
		$csvParser = new CSVParser();
		$masterKey = 'block';
		if (isset($this->cst)) {
			$cst = $csvParser->parseText($this->cst, array('hasHeader' => true, 'masterKey' => $masterKey));

			foreach ($this->elements as $index => $element) {
				foreach ($cst as $relation) {
					foreach ($relation as $relKey => $relValue) {
						if (($relKey != $masterKey) && ($element['system_id'] == $relValue)) {
							$this->elements[$index]['parent'] = $relation[$masterKey];
						}
					}
					// Also set item_type=block to all non "root" block elements
					if ($relation[$masterKey] != 'root') {
						$tmpElements = $this->elements;
						foreach ($tmpElements as $index2 => $element2) {
							if ($element2['system_id'] == $relation[$masterKey]) {
								$this->elements[$index2]['item_type'] = self::ITEM_TYPE_BLOCK;
							}
						}
					}

				}
			}

		}
	}



	/**
	 *  Parse Objective Relationships (ORT)
	 *
	 */
	private function parseOrt() {
		$csvParser = new CSVParser();
		$masterKey = 'course_element';
		if (isset($this->ort)) {
			$ort = $csvParser->parseText($this->ort, array('hasHeader' => true, 'masterKey' => $masterKey));

			foreach ($this->elements as $index => $element) {
				foreach ($ort as $relation) {
					foreach ($relation as $relKey => $relValue) {
						if (($relKey != $masterKey) && ($element['system_id'] == $relValue)) {
							$this->elements[$index]['parent'] = $relation[$masterKey];
						}
					}
				}
			}
		}
	}

	/**
	 * Parse Prerequisite (PRE)
	 */
	private function parsePre() {
		$csvParser = new CSVParser();
		if (isset($this->pre)) {
			$pre = $csvParser->parseText($this->pre, array('hasHeader' => true));
			foreach ($this->elements as $index => $element) {
				foreach ($pre as $relation) {
					if ($element['system_id'] == $relation['structure_element']) {
						$this->elements[$index]['prerequisite'] = $relation['prerequisite'];
					}
				}
			}
		}
	}


	/**
	 * Parse Completions (CMP)
	 */
	private function parseCmp() {
		$csvParser = new CSVParser();
		if (isset($this->pre)) {
			$cmp = $csvParser->parseText($this->cmp, array('hasHeader' => true));
			foreach ($this->elements as $index => $element) {
				foreach ($cmp as $relation) {
					if ($element['system_id'] == $relation['structure_element']) {
						$this->elements[$index]['completion'] = $relation['completion'];
					}
				}
			}
		}
	}

	/**
	 * Load Aicc items into an array; for THIS package only OR for a list of packages
	 * @param unknown $packages
	 * @return unknown
	 */
	public function itemsData($packages = array(), $joinTracking=false, $targetUser=false) {

		if (empty($packages)) {
			$packages = array($this->id);
		}

		$command = Yii::app()->db->createCommand();

		$command->select("items.*, iit.lesson_status AS status")
			->from("learning_aicc_item items")
			->where(array('in', "items.id_package", $packages))
			->order("items.sorting");

		if ($joinTracking) {
			if (!$targetUser) {
				$targetUser = Yii::app()->user->id;
			}
			$command->leftJoin('learning_aicc_item_track iit', '(iit.idItem=items.id) AND (iit.idUser=:idUser)', array(':idUser' => $targetUser));
		}

		$data = $command->queryAll();

		foreach($data as $key => $info)
		{
			if($info['launch'] === '' && $info['item_type'] === LearningAiccPackage::ITEM_TYPE_AU)
				$data[$key]['launch'] = 'story.html';
		}

		return $data;

	}

	/**
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {
		// Do this only if other delete events are ok
		if (parent::beforeDelete()) {
			// Be careful, we need to have a path here
			if ($this->path) {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
				$storageManager->removeFolder($this->path);

				// Also try to delete .ZIP file that might be sitting there
				$storageManager->remove($this->path . ".zip");
				$storageManager->remove($this->path . ".ZIP");

			}
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {
		return parent::afterDelete();
	}


	/**
	 * Return MASTER (package level) tracking for THIS package for a given user
	 *
	 * @param integer $idUser
	 *
	 * @return LearningAiccPackageTrack
	 */
	public function getTrackForUser($idUser) {
		$model = LearningAiccPackageTrack::getTrack($idUser, $this->id);
		return $model;
	}


	/**
	 * Return list (IDs or models) of tracking records for all package items, for THIS package, for a given user
	 *
	 * @param integer|array $idUsers   Single user ID or array user IDs
	 * @param boolean $returnModels
	 *
	 * @return array of LearningAiccPackageTrack or array of integers
	 */
	public function getItemTracks($idUsers=false, $returnModels=false, $havingScore=false) {

		// This package onlt
		$params = array(':idPackage' => $this->id);

		$command = Yii::app()->db->createCommand();
		$command->select('itt.id')
			->from(self::model()->tableName() . " p")
			->join(LearningAiccItem::model()->tableName() . " it", "p.id=it.id_package")
			->join(LearningAiccItemTrack::model()->tableName() . " itt", "it.id=itt.idItem")
			->where('p.id=:idPackage');


		// Filter by user or users ?
		if ($idUsers) {
			if (!is_array($idUsers))
				$idUsers = array((int) $idUsers);
			$command->andWhere("itt.idUser IN (" . implode(',', $idUsers) . ")");
		}

		if ($havingScore) {
			$command->andWhere("itt.score_raw > 0");
		}


		// First, get IDs
		$tracksIds = $command->queryAll(true, $params);

		$result = $tracksIds;

		// Then build MODELs list if requested
		if ($returnModels) {
			$result = array();
			foreach ($tracksIds as $id) {
				$result[] = LearningAiccItemTrack::model()->findByPk($id);
			}
		}

		return $result;

	}


	/**
	 * Return LAST (by ID) tracking record among all item trackings for THIS package, for a given user (optionaly)
	 *
	 * @param integer|array $idUsers
	 * @param boolean $returnModel
	 *
	 * @return integer|LearningAiccPackageTrack
	 */
	public function getLastItemTrack($idUsers=false, $returnModel=false, $havingScore=false) {

		$params = array(':idPackage' => $this->id);

		$command = Yii::app()->db->createCommand();
		$command->select('itt.id')
			->from(self::model()->tableName() . " p")
			->join(LearningAiccItem::model()->tableName() . " it", "p.id=it.id_package")
			->join(LearningAiccItemTrack::model()->tableName() . " itt", "it.id=itt.idItem")
			->where('p.id=:idPackage')
			->order('itt.id DESC')
			->limit(1);


		if ($idUsers) {
			if (!is_array($idUsers))
				$idUsers = array((int) $idUsers);
			$command->andWhere("itt.idUser IN (" . implode(',', $idUsers) . ")");
		}


		if ($havingScore) {
			$command->andWhere("itt.score_raw > 0");
		}

		$tracksIds = $command->queryAll(true, $params);

		if ((count($tracksIds) <= 0) || !is_array($tracksIds)) {
			return null;
		}

		// ID
		$result = $tracksIds[0];

		// Model ?
		if ($returnModel) {
			$result = LearningAiccItemTrack::model()->findByPk($result);
		}

		return $result;

	}


	/**
	 * Delete (clean up, wipe out) all tracking related to THIS package and to a given user or array of users
	 *
	 * @param array|integer $users
	 */
	public function deleteAllTrackingForUsers($users) {

		if (!is_array($users))
			$users = array($users);

		// Array of users must be provided; prevent accidental tracking mass-deletion disaster
		if (empty($users) || !is_array($users)) {
			return;
		}

		// A. Wipe out package level tracking (LO level)
		$users 	= implode(',', $users);
		$params = array(':idPackage' => $this->id);
		$command = Yii::app()->db->createCommand();
		$command->delete('learning_aicc_package_track', "(id_package=:idPackage) AND (idUser IN ($users))", $params);
		$command->execute();

		// B. Wipe out ITEMs tracking
		// 	B.1. Get list of items first
		$itemsList = array();
		foreach ($this->items as $item)
			$itemsList[] = $item->id;

		// 	B.2. Delete tracking records if items list is NOT empty
		if (!empty($itemsList)) {
			$itemsList 	= implode(',', $itemsList);
			$command 	= Yii::app()->db->createCommand();
			$command->delete('learning_aicc_item_track', "(idItem IN ($itemsList)) AND (idUser IN ($users))");
			$command->execute();
		}

		return true;

	}

	public function copy(){
		$package = new LearningAiccPackage('copy');
		$package->idReference = null;
		$package->lms_id_user = Yii::app()->user->id;
		$package->path = $this->path;
		$package->lms_original_filename = $this->lms_original_filename;
		$originalFileName = $this->lms_original_filename;
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_AICC);
		$localTempFilePath = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR.Docebo::randomHash().'.zip';
		$exist = $storage->fileExists($this->path);

		$package->disableBehavior('learningObjects');
		if(!$exist){
			$package->setScenario('copy');
			$package->setAttributes($this->attributes);
			$package->idReference = null;
			$package->lms_id_user = Yii::app()->user->id;
			$items = LearningAiccItem::model()->findAllByAttributes(array('id_package'=> $this->id));
			$itemsArray = array();
			foreach($items as $item){
			    $newItem = new LearningAiccItem();
				$newItem->setAttributes($item->attributes);
				if($newItem->save()){
					$itemsArray[] = $newItem;
				}
			}

			if (!$package->save()) {
				Yii::log('Error saving AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
				Yii::log(var_export($package->errors, true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			foreach($itemsArray as $newItem){
				$newItem->id_package = $package->id;
				$newItem->save(false);
			}

			return $package;
		}

		$storage->downloadAs($this->path.'.zip',$localTempFilePath);

		// Try to extract the package into the final destination (S3, file system,...)
		if ($package->extractPackage($localTempFilePath, false, true)) {
			// Save uploaded package (handle its content, create all the intermediate table records, creating LO and so on)
			// Be aware: behavior is attached !!!
			if (!$package->save()) {
				Yii::log('Error saving AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
				Yii::log(var_export($package->errors, true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
		}
		else {
			Yii::log('Unable to extract AICC package: ' . $originalFileName, CLogger::LEVEL_ERROR);
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}
		FileHelper::removeFile($localTempFilePath);
		return $package;
	}

}
