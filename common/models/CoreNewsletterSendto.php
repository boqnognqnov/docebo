<?php

/**
 * This is the model class for table "core_newsletter_sendto".
 * The followings are the available columns in table 'core_newsletter_sendto':
 * @property integer $id_send
 * @property integer $idst
 * @property string $stime
 * @property integer $processed
 */
class CoreNewsletterSendto extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreNewsletterSendto the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_newsletter_sendto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		return array(
			array('id_send, idst, processed', 'numerical', 'integerOnly' => true),
			array('stime', 'safe'),
			array('id_send, idst, stime', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idst'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
                 'timestampAttributes' => array('stime medium'),
                 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id_send' => 'Id Send',
			'idst' => 'Idst',
			'stime' => 'Stime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id_send', $this->id_send);
		$criteria->compare('idst', $this->idst);
		$criteria->compare('stime', Yii::app()->localtime->fromLocalDateTime($this->stime), true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}