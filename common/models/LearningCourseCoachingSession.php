<?php

/**
 * This is the model class for table "learning_course_coaching_session".
 *
 * The followings are the available columns in table 'learning_course_coaching_session':
 * @property integer $idSession
 * @property integer $idCourse
 * @property integer $idCoach
 * @property integer $max_users
 * @property string $datetime_start
 * @property string $datetime_end
 *
 * The followings are the available model relations:
 * @property CoreUser $coach
 * @property LearningCourse $course
 * @property LearningCourseCoachingSessionUser[] $sessionUsers
 */
class LearningCourseCoachingSession extends CActiveRecord
{
	
	const AUTO_ASSIGN_BALANCED 	= 1;
	const AUTO_ASSIGN_MAXIMIZED = 2;
	
	
	const UNLIMITED_AVAIL_SEATS = -1;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_coaching_session';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('datetime_start, datetime_end', 'required'),
                        array('datetime_start', 'validateDateStartInsert', 'on' => 'insert'),
                        array('datetime_start', 'validateDateStartUpdate', 'on' => 'update'),
			array('idCourse, idCoach, max_users', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idSession, idCourse, idCoach, max_users, datetime_start, datetime_end', 'safe', 'on'=>'search'),
		);
	}
        
        /**
         * 
         * Validating datetime_start and datetime_end on INSERT Scenario
         * 
         */
        public function validateDateStartInsert(){     
            $dateNow = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate(date('now')).' 00:00:00');
            if($this->datetime_start < $dateNow){
                $this->addError('datetime_start', Yii::t('coaching', "Starting date cannot be earlier than today"));
            }
            
            if($this->datetime_end < $dateNow){
                $this->addError('datetime_end', Yii::t('coaching', "Ending date cannot be earlier than today"));
            } 
            if($this->datetime_end < $this->datetime_start){
                $this->addError('datetime_end', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
            }
        }
        
        /**
         * 
         * Validating datetime_end on UPDATE Scenario
         * 
         */
        public function validateDateStartUpdate(){     
            $dateNow = Yii::app()->localtime->toUTC(Yii::app()->localtime->fromLocalDate(date('now')).' 00:00:00');            
            
            if($this->datetime_end < $dateNow){
                $this->addError('datetime_end', Yii::t('coaching', "Ending date cannot be earlier than today"));
            }
            if($this->datetime_end < $this->datetime_start){
                $this->addError('datetime_end', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coach' => array(self::BELONGS_TO, 'CoreUser', 'idCoach'),
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'idCourse'),
			'sessionUsers' => array(self::HAS_MANY, 'LearningCourseCoachingSessionUser', 'idSession'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idSession' => 'Id Session',
			'idCourse' => 'Id Course',
			'idCoach' => 'Id Coach',
			'max_users' => 'Max Users',
			'datetime_start' => 'Datetime Start',
			'datetime_end' => 'Datetime End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idSession',$this->idSession);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idCoach',$this->idCoach);
		$criteria->compare('max_users',$this->max_users);
		$criteria->compare('datetime_start',$this->datetime_start,true);
		$criteria->compare('datetime_end',$this->datetime_end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseCoachingSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {
		parent::afterSave();

		if ($this->isNewRecord) {
			$event = new DEvent($this, array('idCourse' => $this->idCourse, 'idSession' => $this->idSession));
			Yii::app()->event->raise('NewCoachingSession', $event);
		} else {
			$event = new DEvent($this, array('idCourse' => $this->idCourse, 'idSession' => $this->idSession));
			Yii::app()->event->raise('UpdateCoachingSession', $event);
		}
	}

	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {
		// Trigger deletion event
		$enrolledUsers = array();
		foreach($this->sessionUsers as $user)
			$enrolledUsers[] = $user->idUser;
		$event = new DEvent($this, array('sessionModel' => $this, 'sessionUsers' => $enrolledUsers));
		Yii::app()->event->raise('DeletedCoachingSession', $event);

		return parent::beforeDelete();
	}


	/**
	 * Get all coaching sessions user is assigned to, optionally filtered by course, coach and so on
	 * Returns list of session IDs (or models, if requested) 
	 * 
	 * @param integer $idUser
	 * @param boolean $returnModels
	 * @param integer $idCourse
	 * @param integer $idCoach
	 * @param string $idSession
	 * 
	 * @return array  Either array of models or array of IDs
	 */
	public static function userAssignedSessions($idUser, $returnModels = false, $idCourse = false, $idCoach = false, $idSession = false) {
		
		// 
		$params = array(':idUser' => $idUser);
		
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select('s.idSession as idSession')
			->from('learning_course_coaching_session s')
			->join('learning_course_coaching_session_user su', ' (s.idSession = su.idSession) AND (su.idUser=:idUser)');
			
		// Filter by Course ID?
		if ($idCourse !== false) {
			$commandBase->andWhere('s.idCourse=:idCourse');
			$params[':idCourse'] = $idCourse;
		}
		
		// Filter by Coach ID?
		if ($idCoach !== false) {
			$commandBase->andWhere('su.idUser=:idCoach');
			$params[':idCoach'] = $idCoach;
		}

		// Filter by Session ID?
		if ($idSession !== false) {
			$commandBase->andWhere('su.idSession=:idSession');
			$params[':idSession'] = $idSession;
		}
		
		$rows = $commandBase->queryAll(true, $params);
		$result = array();
		foreach ($rows as $row) {
			if ($returnModels)
				$result[] = self::model()->findByPk($row['idSession']);
			else
				$result[] = $row['idSession'];
		}
		
		return $result;
		
	}


	/**
	 * Check if user is assigned to a coaching session, optionally by Course and Coach
	 * 
	 * @param integer $idUser
	 * @param integer $idCourse
	 * @param integer $idCoach
	 * @param string $idSession
	 *
	 * @return boolean
	 */
	public static function isUserAssignedToSession($idUser, $idCourse = false, $idCoach = false, $idSession = false) {
		$sessions = self::userAssignedSessions($idUser, false, $idCourse, $idCoach, $idSession);
		return count($sessions) > 0;
	}
	
	/**
	 * Return an array of course session details for a given course Id
	 * @param int $idCourse
	 * @param timestamp/bool $start_date
	 * @param timestamp/bool $end_date
	 * @return array
	 */
	public static function courseCoachingSessionSqlDataProvider($idCourse, $start_date = false, $end_date = false) {

		$commandBase = Yii::app()->db->createCommand()
			->from('learning_course_coaching_session courseCoachingSessions')
			->join('core_user user', 'user.idst=courseCoachingSessions.idCoach')
			->andWhere('courseCoachingSessions.idCourse = '.$idCourse)
			->andWhere('courseCoachingSessions.max_users > (SELECT count(*) FROM learning_course_coaching_session_user su WHERE su.idSession=courseCoachingSessions.idSession)')
			->select('courseCoachingSessions.idSession,
						user.userid,
						user.firstname,
						user.lastname,
						courseCoachingSessions.datetime_start AS datetime_start,
						courseCoachingSessions.datetime_end AS datetime_end,
						courseCoachingSessions.max_users,
						(SELECT count(*) FROM learning_course_coaching_session_user su WHERE su.idSession=courseCoachingSessions.idSession) AS userCount
					');

		$params=array();

		// If there is start date passed
		if ($start_date){
			$commandBase->andWhere('courseCoachingSessions.datetime_start >= DATE(FROM_UNIXTIME(:start_date))');
			$params[':start_date'] = $start_date;
		}

		// If there is end date passed
		if ($end_date){
			$commandBase->andWhere('courseCoachingSessions.datetime_end <= DATE(FROM_UNIXTIME(:end_date))');
			$params[':end_date'] = $end_date;
		}


		//For sorting the table by any of the given column
		$sort = new CSort();
		$sort->attributes = array(
			'username' => array(
				'asc' 	=> 'user.userid',
				'desc'	=> 'user.userid DESC',
			),
			'firstname' => array(
				'asc' 	=> 'user.firstname',
				'desc'	=> 'user.firstname DESC',
			),
			'lastname' => array(
				'asc' 	=> 'user.lastname',
				'desc'	=> 'user.lastname DESC',
			),
			'session_dates' => array(
				'asc' 	=> 'courseCoachingSessions.datetime_start',
				'desc'	=> 'courseCoachingSessions.datetime_start DESC',
			),
		);
		$sort->defaultOrder = array(
			'name'		=>	CSort::SORT_ASC,
		);
		//To show "Total:[number]"
		$commandCounter = clone $commandBase;
		$commandCounter->select('courseCoachingSessions.idSession');
		$numRecords = ($start_date || $end_date) ? count($commandCounter->query($params)) : count($commandCounter->queryColumn());


		$config = ($start_date || $end_date) ? array(
			'totalItemCount' => $numRecords,
			'sort' => $sort,
			'params' => $params
		)
			: array(
				'totalItemCount' => $numRecords,
				'sort' => $sort
			);

		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}
	
	/**
	 * Assign user or users to THIS session using native SQL to easy the mass operations
	 * 
	 * @param array|integer $idUsers
	 * @return integer Number of successfully inserted records into the relevant table
	 */
	public function assignUsers($idUsers) {
		if (!is_array($idUsers)) {
			$idUsers = array($idUsers);
		}
		
		$counter = 1;
		$setArray = array();
		foreach ($idUsers as $idUser) {
			$idSessionParam = ":idSession" 	. $counter;
			$idUserParam 	= ":idUser" 	. $counter;
			$setArray[] 	= "($idSessionParam, $idUserParam)";
			$param[$idSessionParam] = $this->idSession;
			$param[$idUserParam] 	= $idUser;
			$counter++;
		}

		$result = 0;
		if (!empty($setArray)) {
			$sql = "INSERT IGNORE INTO learning_course_coaching_session_user (idSession, idUser) VALUES " . implode(',', $setArray);
			$command = Yii::app()->db->createCommand($sql);
			$result = $command->execute($param);
		} 
		
		return $result;
		
	}
	
	/**
	 * Un-assign list of users from THIS session
	 * 
	 * @param array $idUsers
	 * @return number
	 */
	public function deassignUsers($idUsers) {
		
		if (!is_array($idUsers)) {
			$idUsers = array($idUsers);
		}
		
		$result = 0;
		if (!empty($idUsers)) {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idUser', $idUsers);
			$criteria->addCondition('idSession=:idSession');
			
			// Because of IN condition, this is the way we have to ADD SQL parameters
			$criteria->params[':idSession'] = $this->idSession;
			
			LearningCourseCoachingSessionUser::model()->deleteAll($criteria);
		}
		
		return $result;
		
	}
	
	
	
	/**
	 * Deassign all users assigned to THIS session and optionally delete it after that
	 * 
	 * @param string $deleteAfter Delete THIS session after a successful move/metrge
	 * 
	 * @return NULL|LearningCourseCoachingSession
	 * 
	 * @throws Exception
	 */
	public function deassignAllUsers($deleteAfter = false) {
		
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		
		try {
			LearningCourseCoachingSessionUser::model()->deleteAll('idSession=:idSession', array(
				':idSession'	=>	$this->idSession,
			));
			if ($deleteAfter) {
				$this->delete();
			}
			if (isset($transaction)) { $transaction->commit(); }
		}
		catch (Exception $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}
		
	}
	

	/**
	 * Return number of assigned users
	 */
	public function countAssignedUsers() {
		return LearningCourseCoachingSessionUser::model()->countByAttributes(array('idSession' => $this->idSession));
	}

	/**
	 * Merge (move) users from THIS session into target session.
	 * 
	 * @param LearningCourseCoachingSession $targetSession
	 * @param boolean $deleteAfter  Delete THIS session after a successful move/metrge
	 * 
	 * @throws Exception
	 */
	public function mergeInto($targetSession, $deleteAfter = false) {
		
		$assignedUsers = $this->sessionUsers;
		$usersToMove = array();
		foreach ($assignedUsers as $assignment) {
			$usersToMove[] = $assignment->idUser;
		}		

		$db = Yii::app()->db;
		
		// Use DB transaction to atomize this operation
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }
		
		try {
			$targetSession->assignUsers($usersToMove);
			$this->deassignAllUsers();
			
			// Delete THIS session (source) if requested
			if ($deleteAfter) {
				$this->delete();
			}
			
			if (isset($transaction)) { $transaction->commit(); }

			$event = new DEvent($this, array('idCourse' => $this->idCourse, 'sourceSession' => $this, 'targetSession' => $targetSession));
			Yii::app()->event->raise('MergedCoachingSession', $event);

		}
		catch (Exception $e) {
			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;			
		}
		
	}
	

	/**
	 * Return number of available seats
	 * 
	 * @return number
	 */
	public function getAvailableSeats() {
		
		if (!$this->max_users)
			return self::UNLIMITED_AVAIL_SEATS;
		 
		return $this->max_users - $this->countAssignedUsers();
	}
	
	

	/**
	 * Automatically assign a student in a session in a given course
	 * 
	 * @param integer $idCourse
	 * @param integer $idUser
	 * @param integer $autoType
	 */
	public static function autoAssign($idCourse, $idUser, $autoType = self::AUTO_ASSIGN_BALANCED) {
			// Get available coaching sessions(where user can be assigned)
			$sessions= LearningCourse::getAvailableCoachingSessions($idCourse);
			
			// Set big enough value for min to be overwritten on first iteration of the foreach iteration
			$minValue = 999999999999999999;
			$minId = false;	// Session with min users count ID
			$maxValue = -1; // Set default max to be overwritten on first foreach iteration
			$maxId = false; // Session with max users count ID

			// Is there just one session - get the first and only session id
			if (count($sessions) == 1) {
				$maxId = $minId = current($sessions);
				$currentSessionModel = $sessionModel = self::model()->findByPk($minId);
				$maxValue = $minValue = $currentSessionModel->countAssignedUsers();
			} elseif (count($sessions) > 1) {
				// Start comparing the sessions users countings
				foreach ($sessions as $session) {
					// Get current session model
					$sessionModel = self::model()->findByPk($session);
					// Get users count in this coaching session
					$count = $sessionModel->countAssignedUsers();

					// Ignore sessions with unlimited max users set (with 0  set as max_users)
					if ($sessionModel->max_users > 0) {
						// Do we need to set a new max values
						if ($count > $maxValue) {
							$maxValue = $count;
							$maxId = $session;
						}

						// Do we need to set a new min values
						if ($count < $minValue) {
							$minValue = $count;
							$minId = $session;
						}
					}

				}

				// ensure there are session ids set for the minId and maxId
				$minId = ($minId) ? $minId : current($sessions);
				$maxId = ($maxId) ? $maxId : current($sessions);

			} else {
				// There are no available coaching sessions assigned to this course
				// So we cannot assign user to any coaching session and we skip here
				return false;
			}


			// Choose a session depending of the auto assign settings
			switch ($autoType) {
				case self::AUTO_ASSIGN_BALANCED:
					$idSession = $minId; // Balanced -  it is also the default behaviour 
					break;
				case self::AUTO_ASSIGN_MAXIMIZED:
					$idSession = $maxId; // Maximized
					break;
				default:
					break;
			}
			
			// If there is no idSession set skip further processing
			if ($idSession === false) {
				return false;
			}

			// Create a session model from the chosen session id, that user will be assigned to
			$targetSessionModel = self::model()->findByPk($idSession);

			// Do actual user assign to a session
			$targetSessionModel->assignUsers($idUser);
	}
	
	
	/**
	 * Show  number of available seats against max allowed (or just "UNLIMITED" if max_users=0)
	 * 
	 * @param array $data
	 * @param index $index
	 */
	protected function renderAvailabilityColumn($data, $index) {
		$coachingSessionModel = self::model()->findByPk($data['idSession']);
		if ($coachingSessionModel) {
			$takenSeats = $coachingSessionModel->countAssignedUsers();
			$totalSeats = ($coachingSessionModel->max_users <> 0) ? $coachingSessionModel->max_users : '<b>&infin;</b>';
			echo '<div class="pull-left">';
			echo $takenSeats . ' / '  . $totalSeats;
			echo '</div>';
			echo '<div class="pull-left"><i class="admin-ico users"></i></div>';
		}
	}

	/**
	 * Remove corresponding messages on delete of a session
	 */
	public function afterDelete() {
		parent::afterDelete();

		// Remove all messages for this session - cleanup garbage
		LearningCourseCoachingMessages::model()->deleteAllByAttributes(array('idSession' => $this->idSession));
	}

    /**
     * Return array of active records for all webinar sessions for this coaching session
     */
    public function getAllWebinarSessions(){
        return LearningCourseCoachingWebinarSession::model()->findAllByAttributes(array('id_coaching_session'=>$this->idSession));
    }

    /**
     * Return array of active records for all future and in-progress
     * */
    public function getFutureAndPresentWebinarSessions($idLearner = false){
        $result = LearningCourseCoachingWebinarSession::model()->findAllByAttributes(array('id_coaching_session'=>$this->idSession),array('condition' => 'date_end >= :todaydate', 'params' => array(':todaydate'=>Yii::app()->localtime->getUTCNow()), 'order' => 'date_begin ASC'));

		if ($idLearner && is_array($result) && count($result) > 0) {
			foreach ($result as $key => $session) {
				$coachingWebinarUser = LearningCourseCoachingWebinarSessionUser::model()->findByAttributes(array('id_user' => $idLearner, 'id_session' => $session->id_session));

				// If there is no learner record in the "learning_course_coaching_webinar_session_user" table
				if(empty($coachingWebinarUser->id_session)) {
					// Remove the current array element/webinar session as it should not be shown to the learner
					unset($result[$key]);
				}
			}
		}

        return $result;
    }

    /**
     * Return array of active records for the past coaching webinar sessions
     * */
    public function getPastWebinarSessions($filterByDate = false){
	    // Initial value for the date/time to filter by
	    $fromDate = Yii::app()->localtime->getUTCNow();

	    if ($filterByDate) {
		    $fromDate = $filterByDate;
	    }

        $result = LearningCourseCoachingWebinarSession::model()->findAllByAttributes(array('id_coaching_session'=>$this->idSession), array('condition' => 'date_end < :todaydate', 'params' => array(':todaydate'=>$fromDate), 'order' => 'date_begin ASC'));
        return $result;
    }
    /**
     * Return array of idst of the assigned users
     * */
    public function listOfAssignedUsers(){
        $users = Yii::app()->db->createCommand()
            ->select('csu.idUser')
            ->from('learning_course_coaching_session_user csu')
            ->where('csu.idSession=:idCoachingSession',
                array(
                    ':idCoachingSession' => (int) $this->idSession
                ))
            ->queryAll();
        $result = array();
        if(!empty($users))
            foreach($users as $key => $value){
                $result[] = $value['idUser'];
            }
        else {
            $result = $users;
        }
        return $result;
    }
}
