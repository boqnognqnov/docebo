<?php

/**
 * This is the model class for table "learning_testtrack_page".
 *
 * The followings are the available columns in table 'learning_testtrack_page':
 * @property integer $idTrack
 * @property integer $page
 * @property string $display_from
 * @property string $display_to
 * @property integer $accumulated
 */
class LearningTesttrackPage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrackPage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack_page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTrack, page, accumulated', 'numerical', 'integerOnly'=>true),
			array('display_from, display_to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, page, display_from, display_to, accumulated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('display_from medium', 'display_to medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'page' => 'Page',
			'display_from' => 'Display From',
			'display_to' => 'Display To',
			'accumulated' => 'Accumulated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('page',$this->page);
		$criteria->compare('display_from',Yii::app()->localtime->fromLocalDateTime($this->display_from),true);
		$criteria->compare('display_to',Yii::app()->localtime->fromLocalDateTime($this->display_to),true);
		$criteria->compare('accumulated',$this->accumulated);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

}