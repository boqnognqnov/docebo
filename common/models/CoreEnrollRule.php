<?php

/**
 * This is the model class for table "core_enroll_rule".
 *
 * The followings are the available columns in table 'core_enroll_rule':
 * @property integer $rule_id
 * @property string $title
 * @property string $rule_type
 * @property integer $active
 * @property string $date_created
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property CoreEnrollRuleItem[] $coreEnrollRuleItems
 */
class CoreEnrollRule extends CActiveRecord
{
	public $subtype_first;
	public $subtype_second;

	const RULE_GROUP_COURSE = 'group_course';
	const RULE_BRANCH_COURSE = 'branch_course';
	const RULE_GROUP_CURRICULA = 'group_curricula';
	const RULE_BRANCH_CURRICULA = 'branch_curricula';

	public static function typeLabelList()
	{
		return array(
			self::RULE_GROUP_COURSE => Yii::t('standard', '_GROUPS').'/'.Yii::t('standard', '_COURSE'),
			self::RULE_BRANCH_COURSE => Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE').'/'.Yii::t('standard', '_COURSE'),
			self::RULE_GROUP_CURRICULA => Yii::t('standard', '_GROUPS').'/'.Yii::t('standard', '_COURSEPATH'),
			self::RULE_BRANCH_CURRICULA => Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE').'/'.Yii::t('standard', '_COURSEPATH'),
		);
	}


	public static function getFirstSubTypeList()
	{
		return array(
			CoreEnrollRuleItem::ITEM_TYPE_GROUP => Yii::t('standard', '_GROUPS'),
			CoreEnrollRuleItem::ITEM_TYPE_BRANCH => Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE'),
		);
	}

	public static function getSecondSubTypeList()
	{
		$result = array(
			CoreEnrollRuleItem::ITEM_TYPE_COURSE => Yii::t('standard', '_COURSE'),
		);
		if (PluginManager::isPluginActive('CurriculaApp'))
			$result[CoreEnrollRuleItem::ITEM_TYPE_CURRICULA] = Yii::t('standard', '_COURSEPATH');

		return $result;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreEnrollRule the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_enroll_rule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, subtype_first, subtype_second', 'required', 'on' => 'create'),
			array('title', 'required', 'on' => 'update'),
			array('active', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rule_id, title, active, date_created, date_updated', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coreEnrollRuleItems' => array(self::HAS_MANY, 'CoreEnrollRuleItem', 'rule_id'),
			'coreEnrollLogs' => array(self::HAS_MANY, 'CoreEnrollLog', 'rule_id'),
			'groupItems' => array(self::HAS_MANY, 'CoreEnrollRuleItem', 'rule_id', 'condition' => 'groupItems.item_type = "'.CoreEnrollRuleItem::ITEM_TYPE_GROUP.'"'),
			'courseItems' => array(self::HAS_MANY, 'CoreEnrollRuleItem', 'rule_id', 'condition' => 'courseItems.item_type = "'.CoreEnrollRuleItem::ITEM_TYPE_COURSE.'"'),
			'branchItems' => array(self::HAS_MANY, 'CoreEnrollRuleItem', 'rule_id', 'condition' => 'branchItems.item_type = "'.CoreEnrollRuleItem::ITEM_TYPE_BRANCH.'"'),
			'curriculaItems' => array(self::HAS_MANY, 'CoreEnrollRuleItem', 'rule_id', 'condition' => 'curriculaItems.item_type = "'.CoreEnrollRuleItem::ITEM_TYPE_CURRICULA.'"'),
		);
	}

	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array('date_created medium', 'date_updated medium'),
				'dateAttributes' => array()
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'rule_id' => 'Id',
			'title' => Yii::t('standard', '_TITLE'),
			'active' => Yii::t('standard', '_ACTIVE'),
			'type' => Yii::t('standard', '_TYPE'),
			'date_created' => Yii::t('report', '_CREATION_DATE'),
			'date_updated' => Yii::t('standard', 'Last update'),
            'subtype_first' => Yii::t('enrollrules', 'Subtype First'),
            'subtype_second' => Yii::t('enrollrules', 'Subtype Second'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('rule_id', $this->rule_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('date_created', Yii::app()->localtime->fromLocalDateTime($this->date_created), true);
		$criteria->compare('date_updated', Yii::app()->localtime->fromLocalDateTime($this->date_updated), true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	public function beforeSave()
	{
		if ($this->isNewRecord)
			$this->date_created = Yii::app()->localtime->getUTCNow();
		else
			$this->date_updated = Yii::app()->localtime->getUTCNow();
		return parent::beforeSave();
	}

	/**
	 * Remove all relational data before deletion
	 * @return bool
	 */
	public function beforeDelete()
	{
		CoreEnrollRuleItem::model()->deleteAllByAttributes(array(
			'rule_id' => $this->rule_id,
		));
		foreach ($this->coreEnrollLogs as $log) //foreach instead of deleteAll because of beforeDelete method of the child class
			$log->delete();

		return parent::beforeDelete();
	}

	public function afterFind()
	{
		switch ($this->rule_type)
		{
			case self::RULE_BRANCH_COURSE:
				$this->subtype_first = CoreEnrollRuleItem::ITEM_TYPE_BRANCH;
				$this->subtype_second = CoreEnrollRuleItem::ITEM_TYPE_COURSE;
				break;
			case self::RULE_BRANCH_CURRICULA:
				$this->subtype_first = CoreEnrollRuleItem::ITEM_TYPE_BRANCH;
				$this->subtype_second = CoreEnrollRuleItem::ITEM_TYPE_CURRICULA;
				break;
			case self::RULE_GROUP_COURSE:
				$this->subtype_first = CoreEnrollRuleItem::ITEM_TYPE_GROUP;
				$this->subtype_second = CoreEnrollRuleItem::ITEM_TYPE_COURSE;
				break;
			case self::RULE_GROUP_CURRICULA:
				$this->subtype_first = CoreEnrollRuleItem::ITEM_TYPE_GROUP;
				$this->subtype_second = CoreEnrollRuleItem::ITEM_TYPE_CURRICULA;
				break;
		}
		parent::afterFind();
	}

	/**
	 * Generate rule type from the both dropdown values
	 * @return string
	 */
	public function generateRuleType()
	{
		$ruleType = '';
		if ($this->subtype_first == 'group') {
			if ($this->subtype_second == 'course')
				$ruleType = self::RULE_GROUP_COURSE;
			else if ($this->subtype_second == 'curricula')
				$ruleType = self::RULE_GROUP_CURRICULA;
		} else if ($this->subtype_first == 'branch') {
			if ($this->subtype_second == 'course')
				$ruleType = self::RULE_BRANCH_COURSE;
			else if ($this->subtype_second == 'curricula')
				$ruleType = self::RULE_BRANCH_CURRICULA;
		}
		return $ruleType;
	}

	public function groupSearch()
	{
		$model = new CoreGroup();
		if (isset($_REQUEST['search_input']))
			$model->groupid = $_REQUEST['search_input'];

		$dataProvider = $model->dataProvider();
		return $dataProvider;
	}

	public function courseSearch()
	{
		$model = new LearningCourse();
		if (isset($_REQUEST['search_input']))
			$model->name = $_REQUEST['search_input'];
		$dataProvider = $model->dataProvider();
		return $dataProvider;
	}

	public function curriculaSearch()
	{
		$model = new LearningCoursepath();
		if (isset($_REQUEST['search_input']))
			$model->path_name = $_REQUEST['search_input'];
		$dataProvider = $model->search();
		return $dataProvider;
	}

	public function getTypeLabel()
	{
		$list = self::typeLabelList();
		return (isset($list[$this->rule_type]) ? $list[$this->rule_type] : '');
	}

	/**
	 * Return all active rules for the given group id
	 * @param $groupId
	 * @return array
	 */
	public static function getRulesByGroup($groupId)
	{
		$rules = self::model()->findAll(array(
			'condition' => 'groupItems.item_id = :item_id AND t.active = 1',
			'params' => array(':item_id' => $groupId),
			'with' => array('groupItems' => array('joinType' => 'INNER JOIN')),
			'together' => true,
		));
		return $rules;
	}

	/**
	 * Return all active rules for the given branch id
	 * @param $branchId
	 * @return array
	 */
	public static function getRulesByBranch($branchId)
	{
		$orgChartTreeModel = CoreOrgChartTree::model()->findByPk($branchId);
		$rules = array();
		if($orgChartTreeModel) {
			$ancestors = $orgChartTreeModel->ancestors()->findAll();
			$ancestorsArray = CHtml::listData($ancestors, 'idOrg', 'idOrg');

			$condition = 'branchItems.item_id = :item_id AND t.active = 1';
			$params[':item_id'] = $branchId;
			if(!empty($ancestorsArray)) {
				$condition = '(branchItems.item_id = :item_id OR
					(branchItems.item_id IN ('.implode(',', $ancestorsArray).') AND branchItems.selection_state = :selection_state)
				) AND t.active = 1';
				$params[':selection_state'] = CoreEnrollRuleItem::SELECTION_STATE_BRANCH_AND_DESCENDANTS;
			}


			$rules = self::model()->findAll(array(
				'condition' => $condition,
				'params' => $params,
				'with' => array('branchItems' => array('joinType' => 'INNER JOIN')),
				'together' => true,
			));
		}

		return $rules;
	}
}