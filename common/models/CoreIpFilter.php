<?php

/**
 * This is the model class for table "core_ip_filter".
 *
 * The followings are the available columns in table 'core_ip_filter':
 * @property integer $id
 * @property string  $ip
 */
class CoreIpFilter extends CActiveRecord {
	/**
	 * @return string the associated database table name
	 */
	public function tableName () {
		return 'core_ip_filter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules () {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array( 'ip', 'length', 'max' => 255 ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array( 'id, ip', 'safe', 'on' => 'search' ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations () {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels () {
		return array(
			'id' => 'ID',
			'ip' => 'Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search () {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare( 'id', $this->id );
		$criteria->compare( 'ip', $this->ip, TRUE );

		return new CActiveDataProvider( $this, array(
			'criteria' => $criteria,
		) );
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return CoreIpFilter the static model class
	 */
	public static function model ( $className = __CLASS__ ) {
		return parent::model( $className );
	}

	/**
	 * Check if the current/passed IP matches
	 * the IP or the IP subset of the current model
	 *
	 * @param $ip
	 *
	 * @return bool
	 *
	 * @see IPHelper::ipInRange() for information on the accepted parameters
	 */
	public function ipMatchesFilter ( $ip ) {
		return IPHelper::ipInRange( $ip, $this->ip );
	}
}
