<?php

/**
 * This is the model class for table "learning_scorm_resources".
 *
 * The followings are the available columns in table 'learning_scorm_resources':
 * @property integer $idscorm_resource
 * @property string $idsco
 * @property integer $idscorm_package
 * @property string $scormtype
 * @property string $href
 * 
 * The followings are the available model relations:
 * @property LearningScormPackage $scormPackage 
 */
class LearningScormResources extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningScormResources the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_scorm_resources';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idscorm_package', 'numerical', 'integerOnly'=>true),
			array('idsco, href', 'length', 'max'=>255),
			array('scormtype', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idscorm_resource, idsco, idscorm_package, scormtype, href', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'scormPackage' => array(self::BELONGS_TO, 'LearningScormPackage', 'idscorm_package'),				
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idscorm_resource' => 'Idscorm Resource',
			'idsco' => 'Idsco',
			'idscorm_package' => 'Idscorm Package',
			'scormtype' => 'Scormtype',
			'href' => 'Href',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idscorm_resource',$this->idscorm_resource);
		$criteria->compare('idsco',$this->idsco,true);
		$criteria->compare('idscorm_package',$this->idscorm_package);
		$criteria->compare('scormtype',$this->scormtype,true);
		$criteria->compare('href',$this->href,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}