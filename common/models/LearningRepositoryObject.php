<?php

/**
 * This is the model class for table "learning_repository_object".
 *
 * The followings are the available columns in table 'learning_repository_object':
 * @property integer $id_object
 * @property string $object_type
 * @property integer $id_category
 * @property string $title
 * @property string $description
 * @property string $short_description
 * @property string $params_json
 * @property string $resource
 * @property integer $shared_tracking
 * @property integer $from_csp
 *
 * The followings are the available model relations:
 * @property LearningCourseCategoryTree $idCategory
 * @property LearningRepositoryObjectVersion[] $versions
 */
class LearningRepositoryObject extends CActiveRecord
{
    public $containChildren;

    public $desktop_openmode;
    public $tablet_openmode;
    public $smartphone_openmode;
    public $titleObj;

    public $confirm;
    
    // TinCan specific virtual attributes
    public $enable_oauth      		= false;  // Add xapi_auth_code in the launch URL
    public $oauth_client         	= null;   // OAuth Client ID to use for generating the authorization code above
    public $use_xapi_content_url    = false;  // Instead uploading file, use/play XAPI content hosted on remote machine
    

    public static $objects_with_versioning = array(
        LearningOrganization::OBJECT_TYPE_SCORMORG,
        LearningOrganization::OBJECT_TYPE_AICC,
        LearningOrganization::OBJECT_TYPE_TINCAN,
        LearningOrganization::OBJECT_TYPE_VIDEO,
        LearningOrganization::OBJECT_TYPE_FILE,
    );

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'learning_repository_object';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_category, containChildren, shared_tracking, from_csp', 'numerical', 'integerOnly'=>true),
            array('object_type', 'length', 'max'=>20),
            array('title, resource', 'length', 'max'=>255),
            array('description, short_description, params_json, shared_tracking, from_csp', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('containChildren, id_object, object_type, id_category, title, description, short_description, params_json, resource, shared_tracking, from_csp', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'repoCategoryTree' => array(self::BELONGS_TO, 'LearningCourseCategoryTree', 'id_category'),
            'versions' => array(self::HAS_MANY, 'LearningRepositoryObjectVersion', 'id_object'),
            'CspInfo' => array(self::BELONGS_TO, 'LtiProvidersInfo', 'from_csp'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_object' => 'Id Object',
            'object_type' => 'Object Type',
            'id_category' => 'Id Category',
            'title' => 'Title',
            'description' => 'Description',
            'short_description' => 'Short Description',
            'params_json' => 'Params Json',
            'resource' => 'Resource',
            'shared_tracking' => 'Shared Tracking',
            'from_csp' => 'From CSP'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_object',$this->id_object);
        $criteria->compare('object_type',$this->object_type,true);
        $criteria->compare('id_category',$this->id_category);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('short_description',$this->short_description,true);
        $criteria->compare('params_json',$this->params_json,true);
        $criteria->compare('resource',$this->resource,true);
        //$criteria->compare('shared_tracking',$this->shared_tracking,true);
        $criteria->compare('from_csp',$this->from_csp,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LearningRepositoryObject the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind(){
        $params = CJSON::decode($this->params_json);
        if(!empty($params)){
            foreach ($params as $key => $value){
                if(property_exists($this, $key)){
                    $this->$key = $value;
                }
            }
        }
    }

    public function afterSave(){
        // Update the related Course Data IF ANY
        Yii::app()->db->createCommand()->update(
            LearningOrganization::model()->tableName(),
            array(
                'title' => $this->title,
                "resource" => $this->resource,
                "short_description" => $this->short_description,
            ),
            'id_object = :object',
            array(":object" => $this->id_object)
        );

        // Get Learning Organization Model for current object
        $criteria = new CDbCriteria();
        $criteria->addCondition("id_object = :idobject");
        $criteria->params[':idobject'] = $this->id_object;
        $learningObj = LearningOrganization::model()->findAll($criteria);

        // Raise Event for Elastic Search
        Yii::app()->event->raise(EventManager::EVENT_LRO_AFTER_SAVE, new DEvent($this, array(
            'learningobject' => $learningObj[0],
            'user' => Yii::app()->user->loadUserModel(),
        )));
    }

    public function dataProvider($options = array()){

        $criteria = new CDbCriteria;
        //$criteria->compare('t.name', $this->name, true);
        if(!empty($this->title)) {
            $criteria->addSearchCondition('t.title', $this->title);
        }

        if(!empty($this->object_type)){
            $criteria->addCondition("t.object_type = :type");
            $criteria->params[':type'] = $this->object_type;
        }elseif($objType = Yii::app()->request->getParam("object_type", false)){
            $criteria->addCondition("t.object_type = :type");
            $criteria->params[':type'] = $objType;
        }

        if(!empty($this->from_csp)){
            $criteria->addCondition("t.from_csp = :from_csp");
            $criteria->params[':from_csp'] = $this->from_csp;
        }elseif($objType = Yii::app()->request->getParam("from_csp", false)){
            $criteria->addCondition("t.from_csp = :from_csp");
            $criteria->params[':from_csp'] = $objType;
        }

        if(isset($options['courseId']) && $options['courseId']){
            $criteria->join = 'LEFT JOIN learning_organization as lorg ON t.id_object = lorg.id_object AND lorg.idCourse = '. $options['courseId'];
            $conditionStr = '(lorg.idCourse IS NULL)';
            switch($options['courseType']){
                case LearningCourse::TYPE_CLASSROOM:
                case LearningCourse::TYPE_WEBINAR:
                    $conditionStr = $conditionStr . ' AND object_type NOT IN ("'.LearningOrganization::OBJECT_TYPE_SCORMORG.'","'.LearningOrganization::OBJECT_TYPE_AICC.'","'.LearningOrganization::OBJECT_TYPE_ELUCIDAT.'","'.LearningOrganization::OBJECT_TYPE_TINCAN.'")';
            }

            $criteria->addCondition($conditionStr);
            $criteria->select = 't.title as titleObj, t.id_object, t.object_type, t.id_category, t.description, t.short_description, t.params_json, t.resource, t.shared_tracking';
            $criteria->group = 't.id_object';
        }

        if(isset($options['not']) && $options['not']){
            $criteria->addCondition("t.id_object NOT IN (:notIn)");
            $notIn = '';
            foreach($options['not'] as $key=>$value){
                $notIn = $notIn .  $value . ",";
            }
            rtrim($notIn, ",");
            $criteria->params[':notIn'] = $notIn;
        }

        if (isset(Yii::app()->session['currentCentralRepoNodeId']) && Yii::app()->session['currentCentralRepoNodeId']) {
            $currentNodeId = Yii::app()->session['currentCentralRepoNodeId'];
            if (!empty($currentNodeId) && $currentNodeId != LearningCourseCategoryTree::getRootNodeId()) {
                if ($this->containChildren) {
                    $allChildrenNodesIds = $this->getCurrentAndChildrenNodesList(Yii::app()->session['currentCentralRepoNodeId']);
                    if (!empty($allChildrenNodesIds)) {
                        $criteria->with['repoCategoryTree'] = array(
                            'joinType' => 'INNER JOIN',
                            'condition' => 'repoCategoryTree.idCategory IN (\'' . implode('\',\'', $allChildrenNodesIds) . '\')',
                        );
                    }

                } else {
                    $criteria->with['repoCategoryTree'] = array(
                        'joinType' => 'INNER JOIN',
                        'scopes' => array('nodeObjectsOnly'),
                    );
                }
                $criteria->together = true;
            }
        } else {
            // By Default show all Category Items
            // If this is the first request get the root ID
            $criteria->with['repoCategoryTree'] = array( 'joinType' => 'INNER JOIN');
            $criteria->together = true;
        }

        $config = array();
        $config['criteria'] = $criteria;
        $config['sort']['defaultOrder'] = 'id_object desc';
        $config['pagination'] = array( 'pageSize' => isset($options['pagination']) ? $options['pagination'] : Settings::get('elements_per_page', 10));

        return new CActiveDataProvider(get_class($this), $config);
    }

    public function pushDataProvider($options = array()){

        $criteria = new CDbCriteria;

        if (isset($options['id']) && $options['id']) {
            $currentNodeId = $options['id'];
            if (!empty($currentNodeId) && $currentNodeId != LearningCourseCategoryTree::getRootNodeId()) {
                if (isset($options['containChildren']) && $options['containChildren']) {
                    $allChildrenNodesIds = $this->getCurrentAndChildrenNodesList($currentNodeId);
                    if (!empty($allChildrenNodesIds)) {
                        $criteria->with['repoCategoryTree'] = array(
                            'joinType' => 'INNER JOIN',
                            'condition' => 'repoCategoryTree.idCategory IN (\'' . implode('\',\'', $allChildrenNodesIds) . '\')',
                        );
                    }

                } else {
                    $criteria->with['repoCategoryTree'] = array(
                        'joinType' => 'INNER JOIN',
                        'condition' => 'idCategory = :idCategory',
                        'params' => array(":idCategory" => $options['id']),
                    );
                }
                $criteria->together = true;
            }
        } else {
            // By Default show all Category Items
            // If this is the first request get the root ID
            $criteria = new CDbCriteria;
            $criteria->with['repoCategoryTree'] = array( 'joinType' => 'INNER JOIN');
            $criteria->together = true;
        }

        //$criteria->compare('t.name', $this->name, true);
        if(!empty($this->title)) {
            $criteria->addSearchCondition('t.title', $this->title);
        }

        if(!empty($this->object_type)){
            $criteria->addCondition("t.object_type = :type");
            $criteria->params[':type'] = $this->object_type;
        }elseif($objType = Yii::app()->request->getParam("object_type", false)){
            $criteria->addCondition("t.object_type = :type");
            $criteria->params[':type'] = $objType;
        }

        if(isset($options['courseId']) && $options['courseId']){
            $criteria->join = 'LEFT JOIN learning_organization as lorg ON t.id_object = lorg.id_object AND lorg.idCourse = '. $options['courseId'];
            $conditionStr = '(lorg.idCourse IS NULL)';
            switch($options['courseType']){
                case LearningCourse::TYPE_CLASSROOM:
                case LearningCourse::TYPE_WEBINAR:
                    $conditionStr = $conditionStr . ' AND object_type NOT IN ("'.LearningOrganization::OBJECT_TYPE_SCORMORG.'","'.LearningOrganization::OBJECT_TYPE_AICC.'","'.LearningOrganization::OBJECT_TYPE_ELUCIDAT.'","'.LearningOrganization::OBJECT_TYPE_TINCAN.'")';
            }

            $criteria->addCondition($conditionStr);
            $criteria->select = 't.title as titleObj, t.id_object, t.object_type, t.id_category, t.description, t.short_description, t.params_json, t.resource, t.shared_tracking';
            $criteria->group = 't.id_object';
        }

        if(isset($options['not']) && $options['not']){
            $criteria->addCondition("t.id_object NOT IN (:notIn)");
            $notIn = '';
            foreach($options['not'] as $key=>$value){
                $notIn = $notIn .  $value . ",";
            }
            rtrim($notIn, ",");
            $criteria->params[':notIn'] = $notIn;
        }

        $config = array();
        $config['criteria'] = $criteria;
        $config['sort']['defaultOrder'] = 't.id_object desc';
        $config['pagination'] = array( 'pageSize' => isset($options['pagination']) ? $options['pagination'] : Settings::get('elements_per_page', 10));

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * @return array
     */
    public function getCurrentAndChildrenNodesList($currentNodeId) {
        $allChildrenNodesIds = array();
        if (!empty($currentNodeId) && $currentNodeId != LearningCourseCategoryTree::getRootNodeId()) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'idCategory = ' . $currentNodeId;
            $currentNode = LearningCourseCategoryTree::model()->find($criteria);
            if ($currentNode) {
                $allChildrenNodes = $currentNode->descendants()->findAll();
                $allChildrenNodesIds = CHtml::listData($allChildrenNodes, 'idCategory', 'idCategory');
                $allChildrenNodesIds = array_merge(array($currentNode->idCategory => $currentNode->idCategory), $allChildrenNodesIds);
            }
        }
        return $allChildrenNodesIds;
    }

    public function getTitleWithIcon($haseNewTitleColumnName = false){
        return '<div class="centralLOGridItem"><span class="' . $this->getObjectIconClass() . '"></span>' . ((!$haseNewTitleColumnName)? $this->title:$this->titleObj) . '</div>';
    }

    public function getObjectIconClass(){
        return self::getObjectIconClassByType($this->object_type);
    }

    public static function getObjectIconClassByType($type){
        switch($type){
            case LearningOrganization::OBJECT_TYPE_SCORMORG:    return "i-sprite is-zip";break;
            case LearningOrganization::OBJECT_TYPE_AICC:        return "fa fa-archive";break;
            case LearningOrganization::OBJECT_TYPE_TINCAN:      return "i-sprite is-tincan";break;
            case LearningOrganization::OBJECT_TYPE_VIDEO:       return "i-sprite is-video";break;
            case LearningOrganization::OBJECT_TYPE_FILE:        return "i-sprite is-file";break;
            case LearningOrganization::OBJECT_TYPE_LTI:         return "i-sprite is-lti";break;
            case LearningOrganization::OBJECT_TYPE_ELUCIDAT:    return "i-sprite is-elucidat";break;
            case LearningOrganization::OBJECT_TYPE_HTMLPAGE:    return "i-sprite is-htmlpage";break;
            case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE: return "i-sprite is-googledrive";break;
            case LearningOrganization::OBJECT_TYPE_POLL:        return "i-sprite is-poll";break;
            case LearningOrganization::OBJECT_TYPE_TEST:        return "i-sprite is-test";break;
            case LearningOrganization::OBJECT_TYPE_AUTHORING:   return "i-sprite objlist-sprite is-converter"; break;
        }
    }

    static public function prepareThumbnails($lroId = null, $gallerySize = 10) {
        if($lroId)
            $model = self::model()->findByPk($lroId);
        else
            $model = new LearningRepositoryObject();

        // Get list of URLs of SHARED and USER player backgrounds
        $assets = new CoreAsset();
        $defaultImages   = $assets->urlList(CoreAsset::TYPE_COURSELOGO, CoreAsset::SCOPE_SHARED, false, CoreAsset::VARIANT_SMALL);
        $userImages   	= $assets->urlList(CoreAsset::TYPE_LO_IMAGE, CoreAsset::SCOPE_PRIVATE, false, CoreAsset::VARIANT_SMALL);

        $count = array(
            'default' => count($defaultImages),
            'user' => count($userImages),
        );

        $reorderArray = function(&$array, $key, $value) {
            unset($array[$key]);
            $array = array($key => $value) + $array;
        };

        foreach ($defaultImages as $key => $value) {
            if ($key == $model->resource) {
                $selected = 'default';

                $reorderArray($defaultImages, $key, $value);
                break;
            }
        }

        if (empty($selected)) {
            foreach ($userImages as $key => $value) {
                if ($key == $model->resource) {
                    $selected = 'user';
                    $reorderArray($userImages, $key, $value);
                    break;
                }
            }
        }

        $selected = empty($selected) ? 'default' : $selected;

        $defaultImages = array_chunk($defaultImages, $gallerySize, true);
        $userImages = array_chunk($userImages, $gallerySize, true);

        return array($count, $defaultImages, $userImages, $selected);
    }

    public function getLatestVersion(){
        return Yii::app()->db->createCommand()
            ->select('id_resource, object_type, version')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = ' . $this->id_object)
            ->order('version DESC')
            ->limit(1)
            ->queryRow();
    }

    public function getFullListOfObjectVersions($options = array()){

        $command = Yii::app()->db->createCommand();

        $command->select(($options['select'])? $options['select'] : '*')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = ' . $this->id_object)
            ->order('version DESC');
//            ->limit(1);
        $versions = $command->queryAll();

        return $versions;
    }

    // Get list of all LO versions but only with visible status 1
    public function getFullListOfObjectVisibleVersions($options = array()){
        $command = Yii::app()->db->createCommand();

        $command->select(($options['select'])? $options['select'] : '*')
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where('id_object = ' . $this->id_object . ' AND object_type = "video" AND visible = 1')
            ->order('version DESC');
        $versions = $command->queryAll();

        return $versions;
    }

    public function getVersionsDropdown(){
        $options = array('select'=>'version, version_name, create_date, author');
        $versions = $this->getFullListOfObjectVersions($options);
        // Raffaele: We're going to disable the "Always keep updated ..." option because it's too dangerous
        // Commenting this part -> array( 'keep_updated' => Yii::t('standard', 'Always keep updated to the latest version') );
        $versionsList = array();
        foreach($versions as $version){
            $authorName = CJSON::decode($version['author'])['username'];
            $title = Yii::app()->localtime->toLocalDateTime($version['create_date']) . ' ' . Yii::t('standard', 'uploaded by') . ' ' . $authorName;
            $versionsList[$version['version']] = $title;
            if(count($versions) > 1) {
                $versionsList[$version['version']] = $version['version_name'] . ' - '  . $title;
            }
        }
        return CHtml::dropDownList('versions_'.$this->id_object,'keep_updated',$versionsList, array('class' => 'lorepo-push-lo-modal-version-select'));
    }

    // Get dropdown with LO but only with at least one version with visible status 1 (for videos only)
    public function getVisibleVersionsDropdown(){
        $options = array('select'=>'version, version_name, create_date, author');
        $versions = $this->getFullListOfObjectVisibleVersions($options);
        $versionsList = array();
        foreach($versions as $version){
            $authorName = CJSON::decode($version['author'])['username'];
            $title = Yii::app()->localtime->toLocalDateTime($version['create_date']) . ' ' . Yii::t('standard', 'uploaded by') . ' ' . $authorName;
            $versionsList[$version['version']] = $title;
            if(count($versions) > 1) {
                $versionsList[$version['version']] = $version['version_name'] . ' - '  . $title;
            }
        }
        return CHtml::dropDownList('versions_'.$this->id_object,'keep_updated',$versionsList, array('class' => 'lorepo-push-lo-modal-version-select'));
    }

    public function getVersionsListData(){
        $options = array('select'=>'version, id_resource, object_type, version_description, author');
        $versions = $this->getFullListOfObjectVersions($options);
        foreach($versions as $index=>$version){
            $versions[$index]['author'] = CJSON::decode($version['author'])['username'];
            $versions[$index]['usedIn'] = LearningRepositoryObjectVersion::numberOfCoursesVersionIsUsedIn($version['id_resource'], $version['object_type']);
        }

        return $versions;
    }

    /**
     * @param $idObj
     * @return mixed
     */
    public static function getJSONParamsById($idObj){
        return Yii::app()->db->createCommand()
            ->select("params_json")
            ->from(self::model()->tableName())
            ->where("id_object = :object")
            ->queryScalar(array(":object => $idObj"));
    }


    /**
     * @param $idObj
     * @param array $columns
     * @return mixed
     */
    public static function getNewestVersionByObjectId($idObj, $columns = array()){

        $initColumns = array("id_resource", "object_type");
        if($columns !== array())
            $initColumns = $columns;

        return Yii::app()->db->createCommand()
            ->select($initColumns)
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where("id_object = :object")
            ->order("create_date DESC")
            ->queryRow(true,array(":object" => $idObj));
    }


    /**
     * @param $idObj
     * @param $idVersion
     * @param array $columns
     * @return mixed
     */
    public static function getVersionByObjectAndVersion($idObj, $idVersion, $columns = array()){

        $initColumns = array("id_resource", "object_type");
        if($columns !== array())
            $initColumns = $columns;

        return Yii::app()->db->createCommand()
            ->select($initColumns)
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where(array("AND", "id_object = :object", "version = :version"))
            ->queryRow(true, array(":object" => $idObj, ":version" => $idVersion));
    }

    /**
     * @param $idObj
     * @return mixed
     */
    public static function getObjectTypeById($idObj){
        return Yii::app()->db->createCommand()
            ->select("object_type")
            ->from(self::model()->tableName())
            ->where("id_object = :object")
            ->queryScalar(array(":object" => $idObj));
    }

    public static function getCourseAssignmentsIds($idObject) {
        return Yii::app()->db->createCommand()
            ->select('idCourse')
            ->from(LearningOrganization::model()->tableName())
            ->where("id_object = :object")
            ->queryColumn(array(":object" => $idObject));
    }

    /**
     * Converts a course local LO into a shared Central LO object.
     * Supported configuration options:
     *  - "local_tracking": only supported for survey LOs
     *
     * @param $lo LearningOrganization The local object to convert into a shared LO
     * @param $options array A set of options needed to configure the shared LO.
     *
     * @return LearningRepositoryObject The shared lo created and linked to the passed local LO
     */
    public static function createFromCourseLearningObject($lo, $options = array()) {
        try {
            // Let's check this object is not a central LO placeholder already
            if($lo->id_object)
                throw new CException("Passed object is already a central LO object!");

            // Let's check this object is not a deliverable one
            if($lo->objectType == LearningOrganization::OBJECT_TYPE_DELIVERABLE)
                throw new CException("Passed object is not allowed in central!");

            // 0. Transaction start
            $currentTransaction = Yii::app()->db->getCurrentTransaction();
            if(!$currentTransaction)
                $currentTransaction = Yii::app()->db->beginTransaction();

            // 1. Create the instance of the central LO object
            $sharedLo = new LearningRepositoryObject();
            $sharedLo->object_type = $lo->objectType;
            $sharedLo->id_category = ($lo->course->idCategory == 0 ? LearningCourseCategoryTree::getRootNodeId() : $lo->course->idCategory);
            $sharedLo->title = $lo->title;
            $sharedLo->short_description = $lo->short_description;
            $sharedLo->params_json = $lo->params_json;
            $sharedLo->resource = $lo->resource;
            $sharedLo->shared_tracking = ($lo->objectType == LearningOrganization::OBJECT_TYPE_POLL) ? $options['local_tracking'] : 1;
            if (!$sharedLo->save())
                throw new CException("Error while saving central LO from local LO. Error message: " . CHtml::errorSummary($sharedLo));

            // 2. Create version 1 of this object
            $sharedLoVersion = new LearningRepositoryObjectVersion();
            $sharedLoVersion->id_resource = $lo->idResource;
            $sharedLoVersion->object_type = $lo->objectType;
            $sharedLoVersion->version = 1;
            $sharedLoVersion->id_object = $sharedLo->id_object;
            $sharedLoVersion->version_name = "V1";
            $sharedLoVersion->version_description = Yii::t("standard", "Created from local course object");
            $sharedLoVersion->author = CJSON::encode(array('idUser' => Yii::app()->user->id, 'username' => Yii::app()->user->loadUserModel()->getUserNameFormatted()));
            $sharedLoVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
            // Get and set status from DB (for videos only)
            if($lo->objectType == "video") {
                $sharedLoVersion->visible = $lo->visible;
            } else {
                $sharedLoVersion->visible = 1;
            }
            if (!$sharedLoVersion->save())
                throw new CException("Error while saving central LO version 1 from local LO. Error message: " . CHtml::errorSummary($sharedLoVersion));

            // 3. Update local object to transform it into a central LO placeholder
            $lo->id_object = $sharedLo->id_object;
            $lo->saveNode();

	        //There was a foreing key constraint which lead to the deletion of AICC data when the IACC was pushed from a course to the CLOR
	        // and then deleted . That is why i removing it when the push is done .
	        if($lo && $lo->objectType == LearningOrganization::OBJECT_TYPE_AICC){
		        $aiccPackage = LearningAiccPackage::model()->findByAttributes(array('idReference' => $lo->idOrg));
		        if($aiccPackage && $aiccPackage->idReference){
			        $command = Yii::app()->db->createCommand();
			        $command->update($aiccPackage->tableName(), array('idReference' => NULL), 'id = :id', array(':id' =>$aiccPackage->id ));
			        $command->execute();
		        }
	        }

            // 4. Update all learning_commontracks to point to version 1 of the shared LO
            Yii::app()->db->createCommand("UPDATE learning_commontrack SET idResource=:idResource WHERE idReference=:idOrg")
                ->execute(array(
                    ':idResource' => $lo->idResource,
                    ':idOrg' => $lo->idOrg
                ));

            // 5. Commit transaction and return object
            $currentTransaction->commit();
            return $sharedLo;

        } catch(Exception $e) {
            if($currentTransaction)
                $currentTransaction->rollback();

			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }

        return null;
    }

    /**
     * Returns the "Edit" URL for the central Repo
     *
     * @return string Edit URL inside the repo for the current object
     */
    public function getEditUrl() {
        switch ($this->object_type) {
            case LearningOrganization::OBJECT_TYPE_AUTHORING:
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array("id_object" => $this->id_object));
                if ($versionModel)
                    return Docebo::createAbsoluteAuthoringUrl("main/create", array("id" => $versionModel->id_resource, "opt" => "centralrepo" , "editSlideFile"=>true, 'idObject' => $versionModel->id_object));
                break;
            case LearningOrganization::OBJECT_TYPE_LTI:
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $this->id_object));
                if ($versionModel) {
                    return Docebo::createAbsoluteLmsUrl('LtiApp/LtiApp/editCentralRepo', array(
                        'id_object' => $this->id_object
                    ));
                }
                break;
            case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $this->id_object));
                if ($versionModel) {
                    return Docebo::createAbsoluteAdminUrl('ElucidatApp/ElucidatApp/editCentralRepo', array(
                        'id_object' => $this->id_object,
                        'step' => 2
                    ));
                }
                break;
            case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE:
                $versionModel = LearningRepositoryObjectVersion::model()->findByAttributes(array('id_object' => $this->id_object));
                if ($versionModel) {
                    return Docebo::createAbsoluteLmsUrl('GoogleDriveApp/googleDriveApp/editCentralRepo', array(
                        'id_object' => $this->id_object
                    ));
                }
                break;
            case LearningOrganization::OBJECT_TYPE_POLL:
            case LearningOrganization::OBJECT_TYPE_TEST:
                return Yii::app()->createUrl($this->object_type . '/default/edit', array(
                    'id_object' => $this->id_object,
                    'opt' => 'centralrepo'
                ));
                break;
            default:
                return Yii::app()->createUrl("centralrepo/" . $this->object_type . "Lo/edit", array("id_object" => $this->id_object));
                break;
        }

        return "";
    }

    /**
     * @param $idObject the unique ID of the LearningRepositoryObject instance
     *
     * @return Count of the existing versions for certain LearningRepositoryObject
     */
    public static function getObjectVersionsCount($idObject){
        return Yii::app()->db->createCommand()
            ->select("COUNT(1)")
            ->from(LearningRepositoryObjectVersion::model()->tableName())
            ->where("id_object = :object")
            ->queryScalar(array(":object" => $idObject));
    }

    /**
     * @param $idObject the unique ID of the LearningRepositoryObject instance
     *
     * @return boolean If a Object can be overridden in a course in course
     */
    public static function canObjectInCourseBeOverridden($idObject) {

        $type = self::getObjectTypeById($idObject);
        $versionsCount = self::getObjectVersionsCount($idObject);
        $coursesParticipation = self::getCourseAssignmentsIds($idObject);
        /*
         * Few conditions mu be passed in order a Object to be able to override any of hi's versions in a Course
         * 1 - If the Object support versioning
         * 2 - If the Object has more than one version
         * 3 - If the Object is already a part of any course
         */
        return in_array($type, self::$objects_with_versioning) && $versionsCount > 1 && count($coursesParticipation) > 0;
    }

    public static function getCourseObjectVersionOverlapping($params){

        $subQuery = Yii::app()->db->createCommand()
            ->select(array("idReference as idOrg", "count(idUser) as inProgress"))
            ->from(LearningCommontrack::model()->tableName() . " ilct")
            ->where(array("OR", "ilct.status = :abInito", "ilct.status = :attempted"))
            ->group("ilct.idReference");

        $items = Yii::app()->db->createCommand()
            ->selectDistinct(array('lc.idCourse', 'lc.code', "lc.name", 'lrov.version_name', "lct.inProgress"))
            ->from(LearningOrganization::model()->tableName() . " lo")
            ->join(LearningCourse::model()->tableName() . " lc", "lc.idCourse = lo.idCourse")
            ->join(LearningRepositoryObjectVersion::model()->tableName() . " lrov", "lrov.id_object = lo.id_object AND lrov.id_resource = idResource")
            ->join("( " . $subQuery->getText() . ") lct", "lct.idOrg = lo.idOrg")
            ->where(array(
                'AND',
                array('in', 'lc.idCourse', $params['courses']),
                'lrov.id_resource != :resource'
            ))
            ->queryAll(true, array(
                ":abInito" => LearningCommontrack::STATUS_AB_INITIO,
                ":attempted" => LearningCommontrack::STATUS_ATTEMPTED,
                ":resource" => $params['id_resource']
            ));

        $keys = array();
        foreach($items as $item) {
            $keys[] = $item['idCourse'];
        }

        $dataProvider = new CArrayDataProvider($items, array(
            'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
            'totalItemCount' => count($items),
        ));
        return $options = array(
            'dataProvider' => $dataProvider,
            'keys' => $keys
        );
    }

    // Check for versions in progress (for videos)
    public function checkForProgressVersion($idObject){
        $versionsProgress = count(LearningRepositoryObjectVersion::model()->findAllByAttributes(array('id_object' => $idObject, 'object_type' => "video", 'visible' => '-1')));

        if($versionsProgress > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Check if object has at least one version with visible status 1 (for videos)
    public function checkAllVersionErrors($idObject){
        $allVersionsVisible = count(LearningRepositoryObjectVersion::model()->findAllByAttributes(array('id_object' => $idObject, 'object_type' => "video", 'visible' => '1')));

        if($allVersionsVisible > 0) {
            return true;
        } else {
            return false;
        }
    }

    // Check version for progress or error status (for videos)
    public function checkProgressAndErrors($idObject, $objType){
        if($objType != "video") {
            return true;
        } else {
            $versionsProgress = $this->checkForProgressVersion($idObject);
            if(!$versionsProgress) {
                return $this->checkAllVersionErrors($idObject);
            } else {
                return false;
            }
        }
    }

}