<?php

/**
 * This is the model class for table "learning_courseuser".
 *
 * The followings are the available columns in table 'learning_courseuser':
 * @property integer $idUser
 * @property integer $idCourse
 * @property integer $edition_id
 * @property integer $level
 * @property string $date_inscr
 * @property string $date_first_access
 * @property string $date_complete
 * @property integer $deadline
 * @property string $fromDate
 * @property string $toDate
 * @property integer $status
 * @property integer $waiting
 * @property integer $subscribed_by
 * @property integer $rule_log
 * @property integer $score_given
 * @property integer $forced_score_given
 * @property integer $initial_score_given
 * @property string $imported_from_connection
 * @property integer $absent
 * @property integer $cancelled_by
 * @property integer $new_forum_post
 * @property string $date_begin_validity
 * @property string $date_expire_validity
 * @property integer $requesting_unsubscribe
 * @property string $requesting_unsubscribe_date
 * @property integer $deeplinked_by
 * @property json $enrollment_fields
 * @property integer $status_propagator
 *
 * The followings are the available model relations:
 * @property LearningCourse $course
 * @property CoreUser $learningUserSubscribedBy
 * @property CoreUser $user
 * @property CoreUser $learningUser The related user model
 *
 * @package docebo.models
 * @subpackage table
 */
class LearningCourseuser extends CActiveRecord
{

	// User/Course subscription levels (per course, per user); learning_courseuser->level
	public static $USER_SUBSCR_LEVEL_GUEST = 1;
	public static $USER_SUBSCR_LEVEL_GHOST = 2;
	public static $USER_SUBSCR_LEVEL_STUDENT = 3;  // initial
	public static $USER_SUBSCR_LEVEL_TUTOR = 4;
	public static $USER_SUBSCR_LEVEL_MENTOR = 5;
	public static $USER_SUBSCR_LEVEL_INSTRUCTOR = 6;
	public static $USER_SUBSCR_LEVEL_ADMIN = 7;
	public static $USER_SUBSCR_LEVEL_COACH = 8;

	// User/Course subscription statuses; learning_courseuser->status
	public static $COURSE_USER_WAITING_LIST = -2; // _CUS_WAITING_LIST; includes waiting for payment
	public static $COURSE_USER_CONFIRMED = -1;    // _CUS_CONFIRMED
	public static $COURSE_USER_SUBSCRIBED = 0;    // _CUS_SUBSCRIBED
	public static $COURSE_USER_BEGIN = 1;         // _CUS_BEGIN
	public static $COURSE_USER_END = 2;           // _CUS_END
	public static $COURSE_USER_SUSPEND = 3;       // _CUS_SUSPEND
	public static $COURSE_USER_OVERBOOKING = 4;   // _CUS_OVERBOOKING


	// Various useful constants
	public static $SUBSCRIBE_FAILURE = -1;
	public static $ALREADY_SUBSCRIBED = -2;
	public static $UNSUBSCRIBE_FAILURE = -3;

	// Constants for Deadline filter (radio buttons)
	public static $DEADLINE_SELECT_ALL = 0;
	public static $DEADLINE_SELECT_WITHOUT = 1;
	public static $DEADLINE_DATE_RANGE = 2;

	public $errorMessage; //used in subscribe/unsubscribe methods to keep the errors
	public $confirm;
	public $count;
	public $idUser;
	public $overbooked; //used for search purposes

	public $completedByEquivalenceOfCourse = false;

	public $validPropagatorId = false;

	protected $_subscribedBy;

	protected $oldAttributes; // Stores old attributes on afterFind() so we can compare against them during save

	public $auditContext;

	public $skip_certificate_generation = false;

	public $deadline = 0; // used for deadline filter

	public $fromDate; // used for deadline filter

	public $toDate; // used for deadline filter

	public static function exportTypes()
	{
		return array(
			'Excel5' => Yii::t('standard', 'Excel'),
			'CSV' => Yii::t('standard', 'CSV'),
			'PDF' => Yii::t('standard', 'PDF'), //currently there are problems with the export
			'HTML' => Yii::t('standard', 'HTML'),
		);
	}


	public static function exportTypes2()
	{
		return array(
				'Excel5' => Yii::t('standard', 'Excel'),
				'CSV' => Yii::t('standard', 'CSV'),
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourseuser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_courseuser';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idCourse, edition_id, level, status, waiting, subscribed_by, rule_log, absent, cancelled_by, new_forum_post, requesting_unsubscribe, deeplinked_by,  status_propagator', 'numerical', 'integerOnly'=>TRUE),
			array('score_given, initial_score_given', 'numerical'),
			array('imported_from_connection', 'length', 'max'=>255),
			array('deadline', 'numerical'),
			array('fromDate, toDate', 'date'),
			array('overbooked, levelActivate, statusActivate, date_inscr, date_first_access, date_complete, date_begin_validity, date_expire_validity, requesting_unsubscribe_date', 'safe'),
                array('date_begin_validity', 'validateDateBeginValidity', 'on' => 'insert'),
                array('date_begin_validity', 'validateDateBeginValidity', 'on' => 'update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUser, idCourse, edition_id, level, date_inscr, date_first_access, date_complete, status, waiting, subscribed_by, rule_log, score_given, initial_score_given, imported_from_connection, absent, cancelled_by, new_forum_post, date_begin_validity, date_expire_validity, requesting_unsubscribe, requesting_unsubscribe_date, deeplinked_by, status_propagator', 'safe', 'on'=>'search'),
		);
	}
    /**
     *
     * Validating date_begin_validity and date_expire_validity on INSERT Scenario
     *
     */
    public function validateDateBeginValidity(){
        if ($this->date_expire_validity && $this->date_expire_validity!='0000-00-00 00:00:00') {
            $begin = Yii::app()->localtime->fromLocalDateTime($this->date_begin_validity);
            $end = Yii::app()->localtime->fromLocalDateTime($this->date_expire_validity);
            if ($begin > $end) {
                $this->addError('date_begin_validity', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
                $this->addError('date_expire_validity', Yii::t('coaching', "Ending date cannot be earlier than the starting date"));
            }
        }
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'idCourse', 'joinType' => 'INNER JOIN'),
			'user' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'memberGroups' => array(self::HAS_MANY, 'CoreGroupMembers', array('idstMember' => 'idUser'), 'joinType' => 'INNER JOIN'),
			'groups' => array(self::HAS_MANY, 'CoreGroup', array('idst' => 'idst'), 'through' => 'memberGroups'/*, 'condition' => 'adminLevel.groupid LIKE "%framework/level%"'*/),
			'learningUser' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'learningUserSubscribedBy' => array(self::BELONGS_TO, 'CoreUser', 'subscribed_by'),
			'learningCoursepathCourses' => array(self::HAS_MANY, 'LearningCoursepathCourses', array('id_item' => 'idCourse')),
			'learningCertificateAssign' => array(self::HAS_ONE, 'LearningCertificateAssign', array('id_user' => 'idUser', 'id_course' => 'idCourse')),
			'learningCertificateCourse' => array(self::HAS_ONE, 'LearningCertificateCourse', array('id_course' => 'idCourse')),
		);
	}

	public function scopes()
	{
		return array(
			'students' => array('condition' => 'level = :student', 'params' => array(':student' => self::$USER_SUBSCR_LEVEL_STUDENT)),
			'notStarted' => array('condition' => 'status IN (:statuses)', 'params' => array(':statuses' => array(self::$COURSE_USER_WAITING_LIST, self::$COURSE_USER_CONFIRMED, self::$COURSE_USER_SUBSCRIBED))),
			'inProgress' => array('condition' => 'status = :status', 'params' => array(':status' => self::$COURSE_USER_BEGIN)),
			'completed' => array('condition' => 'status = :status', 'params' => array(':status' => self::$COURSE_USER_END)),
			'coaches' => array('condition' => 'level = :coach', 'params' => array(':coach' => self::$USER_SUBSCR_LEVEL_COACH)),
		);
	}



	public function getErrorMessage() {
		return $this->errorMessage;
	}


    /**
     * Parameterized name scope to filter by the users assigned to a power user
     * @param $powerUserId
     * @return $this
     */
    public function powerUserAssigned($powerUserId) {
        $this->getDbCriteria()->mergeWith(array(
            'join' => 'INNER JOIN core_user_pu cup ON ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )',
            'params' => array(':idPowerUser' => $powerUserId)
        ));

        $this->getDbCriteria()->mergeWith(array(
            'join' => 'INNER JOIN core_user_pu_course cupc ON ( (t.idCourse=cupc.course_id) AND (cupc.puser_id=:idPowerUser) )',
            'params' => array(':idPowerUser' => $powerUserId)
        ));
        return $this;
    }

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			    'timestampAttributes' => array('date_inscr medium', 'date_first_access medium', 'date_complete medium',
                    'date_begin_validity medium', 'date_expire_validity medium', 'date_last_access medium', 'requesting_unsubscribe_date medium'),
			    'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idUser' => 'Id User',
			'idCourse' => 'Id Course',
			'edition_id' => 'Edition',
			'level' => Yii::t('standard', '_LEVEL'),
			'date_inscr' => Yii::t('report', '_DATE_INSCR'),
			'date_first_access' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
			'date_last_access' => Yii::t('standard', '_DATE_LAST_ACCESS'),
			'date_complete' => Yii::t('report', '_DATE_COURSE_COMPLETED'),
			'status' => Yii::t('standard', '_STATUS'),
			'waiting' => Yii::t('standard', '_WAITING'),
			'subscribed_by' => Yii::t('standard', '_SUBSCRIBED_BY'),
			'rule_log' => 'Rule Log',
			'score_given' => Yii::t('standard', '_FINAL_SCORE'),
			'forced_score_given' => Yii::t('standard', '_FINAL_SCORE_FORCED'),
			'initial_score_given' => Yii::t('report', '_COURSES_FILTER_SCORE_INIT'),
			'imported_from_connection' => 'Imported From Connection',
			'absent' => 'Absent',
			'cancelled_by' => 'Cancelled By',
			'new_forum_post' => 'New Forum Post',
			'date_begin_validity' => Yii::t('subscribe', '_DATE_BEGIN_VALIDITY'),
			'date_expire_validity' => Yii::t('subscribe', '_DATE_EXPIRE_VALIDITY'),
			'requesting_unsubscribe' => 'Requesting Unsubscribe',
			'requesting_unsubscribe_date' => 'Requesting Unsubscribe Date',
			'statusActivate' => Yii::t('standard', '_STATUS'),
			'levelActivate' => Yii::t('standard', '_LEVEL'),
			'enrollment_fields'=> 'Enrollment fields'
		);
	}

	public function getUser() {
		if ($this->user === NULL) {
			$this->user = new CoreUser();
		}
		return $this->user;
	}

	public function getSubscribedBy() {
		if ($this->_subscribedBy == NULL) {
			$this->_subscribedBy = CoreUser::model()->findByPk($this->subscribed_by);
		}
		return $this->_subscribedBy;
	}



	protected $_course = null;

	public function getCourse() {
		if ($this->_course === null) {
			$this->_course = LearningCourse::model()->findByPk($this->idCourse);
		}
		return $this->_course;
	}


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idCourse',$this->idCourse);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * used in course report for exporting the data to pdf
	 * @return CSqlDataProvider
	 */
	public function reportSqlDataProvider() {
		$command = Yii::app()->db->createCommand();
		$command->select('t.idUser, t.idCourse, (SUBSTRING(user.userid, 2)) as userid, t.date_inscr, t.date_first_access, t.date_complete, t.status');
		$command->from('learning_courseuser t');
		$command->join('core_user user', 't.idUser = user.idst');
		$command->join('learning_course course', 't.idCourse = course.idCourse');

		//if the user is a power user, select only assigned to him users and courses
		if(Yii::app()->user->getIsPu()){
			$command->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = :userId AND t.idUser = pu.user_id", array(':userId' => Yii::app()->user->id));
			$command->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = :userId AND puc.course_id = t.idCourse", array(':userId' => Yii::app()->user->id));
		}

		if(!empty($this->idUser))
			$command->andWhere('t.idUser = :idUser', array(':idUser' => $this->idUser));
		if(!empty($this->idCourse))
			$command->andWhere('t.idCourse = :idCourse', array(':idCourse' => $this->idCourse));
		if(!empty($this->level))
			$command->andWhere('t.level = :level', array(':level' => $this->level));
		if(!empty($this->waiting))
			$command->andWhere('t.waiting = :waiting', array(':waiting' => $this->waiting));
		if(!empty($this->user->userid))
			$command->andWhere('user.userid LIKE :userid', array(':userid' => '%'.$this->user->userid.'%'));

		if ($this->user->search_input) {
			$command->andWhere('CONCAT(user.firstname, " ", user.lastname) like :search OR user.email LIKE :search OR user.userid LIKE :search', array(
				':search' => '%'.$this->user->search_input.'%'
			));
		}

		switch ($this->overbooked)
		{
			case 1:
				$command->andWhere('t.status = :overbooked', array(':overbooked' => self::$COURSE_USER_OVERBOOKING));
				break;
			case 2:
				$command->andWhere('t.status != :overbooked', array(':overbooked' => self::$COURSE_USER_OVERBOOKING));
				break;
			default:
				break;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		if (!empty($this->user)) {
			foreach ($this->user->attributeNames() as $attributeName) {
				$sortAttributes['user.'.$attributeName] = 'user.'.$attributeName;
			}
			$sortAttributes['user.fullname'] = array(
				'asc'=>'user.firstname, user.lastname',
				'desc'=>'user.firstname DESC, user.lastname DESC',
			);
		}

		$sort = new CSort();
		$sort->attributes = $sortAttributes;
		$sort->defaultOrder = 'user.userid ASC';
		$config = array(
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
			'sort' => $sort,

		);

		$dataProvider = new CSqlDataProvider($command, $config);

		return $dataProvider;
	}
	public function dataProvider() {
		$criteria = new CDbCriteria;

		$criteria->with = array('user'=>array('joinType' => 'INNER JOIN'), 'course');

		if (!empty($_REQUEST['advancedSearch']['fields']) && $_REQUEST['advancedSearch']['visible'] == 1) {
			$params = $_REQUEST['advancedSearch'];
			foreach ($params['fields'] as $key => $data) {
				if ($key == 'userid') {
					$this->user->userid = '';

					if ($data['condition'] != 'contains') {
						$data['keywords'] = Yii::app()->user->getAbsoluteUsername($data['keywords']);
					}
				}

				if ($data['condition'] == 'contains') {
					$criteria->addSearchCondition('user.'.$key, "{$data['keywords']}", TRUE, $params['condition']);
				} elseif ($data['condition'] == 'equal') {
					$criteria->addCondition('user.'.$key . ' = :' . $key . '_keywords', $params['condition']);
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				} elseif ($data['condition'] == 'not-equal') {
					$criteria->addCondition('user.'.$key . ' <> :' . $key . '_keywords', $params['condition']);
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				}
			}
		}

		$criteria->compare('t.idUser', $this->idUser);
		$criteria->compare('t.idCourse', $this->idCourse);
		$criteria->compare('t.level', $this->level);
		$criteria->compare('t.waiting', $this->waiting);
		$criteria->compare('user.userid', $this->user->userid, TRUE);

		if ($this->user->search_input) {
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) like :search OR user.email LIKE :search OR user.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->user->search_input.'%';
		}

		switch ($this->overbooked)
		{
			case 1:
				$criteria->addCondition('t.status = :overbooked');
				$criteria->params[':overbooked'] = self::$COURSE_USER_OVERBOOKING;
				break;
			case 2:
				$criteria->addCondition('t.status != :overbooked');
				$criteria->params[':overbooked'] = self::$COURSE_USER_OVERBOOKING;
				break;
			default:
				break;
		}

		// @Raffaele: this is never used. Yii::app()->session['enrollNodeId'] is only set in the Admin -> Course Management -> Enrollment page (Advanced filter)
		/*if (isset(Yii::app()->session['enrollNodeId'])) {
			$enrollNodeId = Yii::app()->session['enrollNodeId'];
			if (!empty($enrollNodeId) && $enrollNodeId > 1) {
				if ($this->containChildren) {
					$allChildrenNodesIds = $this->getCurrentAndChildrenNodesList();
					if (!empty($allChildrenNodesIds)) {
						$criteria->with = array(
							'orgChartGroups' => array(
								'joinType' => 'INNER JOIN',
								'condition' => 'orgChartGroups.idOrg IN (\'' . implode('\',\'', $allChildrenNodesIds) . '\')',
							),
						);
					}
				} else {
					$criteria->addCondition('orgChartGroups.idOrg IN (102)');
				}
				$criteria->together = TRUE;
			}
		}*/

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		if (!empty($this->user)) {
			foreach ($this->user->attributeNames() as $attributeName) {
				$sortAttributes['user.'.$attributeName] = 'user.'.$attributeName;
			}
			$sortAttributes['user.fullname'] = array(
				'asc'=>'user.firstname, user.lastname',
				'desc'=>'user.firstname DESC, user.lastname DESC',
			);
		}

		if(Yii::app()->user->getIsPu()){
			// Current user is Power User. Limit the results to only
			// those users and courses assigned to him

			$currentPuUserUsers = Yii::app()->getDb()->createCommand()
				->select('user_id')
				->from(CoreUserPU::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idUser', $currentPuUserUsers);

			if((isset($this->user->idst) && (Yii::app()->user->getId() != $this->user->idst)) || (!isset($this->user->idst))){
				$currentPuCourses = Yii::app()->getDb()->createCommand()
					->select('course_id')
					->from(CoreUserPuCourse::model()->tableName())
					->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
					->queryColumn();
				$criteria->addInCondition('t.idCourse', $currentPuCourses);
			}

		}

		if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin()){

				$criteria->addNotInCondition('t.idCourse', Docebo::learnerNotAllowedCoursesIds());
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 'register_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


		return new CActiveDataProvider($this, $config);
	}


	public function dataProviderCourseReport() {
		$criteria = new CDbCriteria;

		$criteria->with = array('user'=>array('joinType' => 'INNER JOIN'), 'course');

		if (!empty($_REQUEST['advancedSearch']['fields']) && $_REQUEST['advancedSearch']['visible'] == 1) {
			$params = $_REQUEST['advancedSearch'];
			foreach ($params['fields'] as $key => $data) {
				if ($key == 'userid') {
					$this->user->userid = '';

					if ($data['condition'] != 'contains') {
						$data['keywords'] = Yii::app()->user->getAbsoluteUsername($data['keywords']);
					}
				}

				if ($data['condition'] == 'contains') {
					$criteria->addSearchCondition('user.'.$key, "{$data['keywords']}", TRUE, $params['condition']);
				} elseif ($data['condition'] == 'equal') {
					$criteria->addCondition('user.'.$key . ' = :' . $key . '_keywords', $params['condition']);
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				} elseif ($data['condition'] == 'not-equal') {
					$criteria->addCondition('user.'.$key . ' <> :' . $key . '_keywords', $params['condition']);
					$criteria->params[':' . $key . '_keywords'] = $data['keywords'];
				}
			}
		}

		$criteria->compare('t.idUser', $this->idUser);
		$criteria->compare('t.idCourse', $this->idCourse);
		$criteria->compare('t.level', $this->level);
		$criteria->compare('t.waiting', $this->waiting);
		$criteria->compare('user.userid', $this->user->userid, TRUE);

		if ($this->user->search_input) {
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) like :search OR user.email LIKE :search OR user.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->user->search_input.'%';
		}

		switch ($this->overbooked)
		{
			case 1:
				$criteria->addCondition('t.status = :overbooked');
				$criteria->params[':overbooked'] = self::$COURSE_USER_OVERBOOKING;
				break;
			case 2:
				$criteria->addCondition('t.status != :overbooked');
				$criteria->params[':overbooked'] = self::$COURSE_USER_OVERBOOKING;
				break;
			default:
				break;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		if (!empty($this->user)) {
			foreach ($this->user->attributeNames() as $attributeName) {
				$sortAttributes['user.'.$attributeName] = 'user.'.$attributeName;
			}
			$sortAttributes['user.fullname'] = array(
				'asc'=>'user.firstname, user.lastname',
				'desc'=>'user.firstname DESC, user.lastname DESC',
			);
		}

		if(Yii::app()->user->getIsPu()){
			// Current user is Power User. Limit the results to only
			// those users and courses assigned to him

			$currentPuUserUsers = Yii::app()->getDb()->createCommand()
				->select('user_id')
				->from(CoreUserPU::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idUser', $currentPuUserUsers);

			$currentPuCourses = Yii::app()->getDb()->createCommand()
				->select('course_id')
				->from(CoreUserPuCourse::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idCourse', $currentPuCourses);
		}

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));


		return new CActiveDataProvider($this, $config);
	}


	public function renderCourseLink($name)
	{
		$link = '<a href="../lms/index.php?r=player&course_id='.$this->idCourse.'">'.$name.'</a>';
		$canEnterCourse = $this->canEnterCourse();

		return ($canEnterCourse['can'] ? $link : $this->course->name);
	}



	public function dataProviderCertificate() {
		$criteria = new CDbCriteria;
		$config = array();
		$criteria->compare('t.idCourse', $this->idCourse);
		$criteria->compare('user.userid', $this->user->userid, TRUE);
		$criteria->compare('t.idUser', '<>/Anonymous');
		$criteria->compare('t.status', self::$COURSE_USER_END);

		if(Yii::app()->user->getIsPu()){
			// Current user is Power User. Limit the results to only
			// those users and courses assigned to him

			$currentPuUserUsers = Yii::app()->getDb()->createCommand()
				->select('user_id')
				->from(CoreUserPU::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idUser', $currentPuUserUsers);

			$currentPuCourses = Yii::app()->getDb()->createCommand()
				->select('course_id')
				->from(CoreUserPuCourse::model()->tableName())
				->where('puser_id=:puser', array(':puser'=>Yii::app()->user->getId()))
				->queryColumn();
			$criteria->addInCondition('t.idCourse', $currentPuCourses);
		}

		$criteria->with = array(
			'user' => array('joinType' => 'INNER JOIN'),
			'learningCertificateAssign' => array(
				'on' => 'learningCertificateAssign.id_certificate = :id_certificate',
				'params' => array(':id_certificate' => $this->learningCertificateAssign->id_certificate)
			),
			'learningCertificateCourse' => array(
				'on' => 'learningCertificateCourse.id_certificate = :id_certificate',
				'params' => array(':id_certificate' => $this->learningCertificateCourse->id_certificate)
			)
		);
		$criteria->group = 'user.idst';

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;

		$config['sort']['attributes'] = $sortAttributes;
		// $config['sort']['defaultOrder'] = 'create_date desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}



	public function dataProviderCustomReport($data = array(), $isApplySort = FALSE, $reportType = FALSE, $adminFilter = FALSE) {

		//power user filters
		if ($adminFilter && Yii::app()->user->getIsAdmin()) {
			//users
			$adminUsers = CoreUserPU::model()->findAllByAttributes(array('puser_id' => Yii::app()->user->id));
			if (!empty($adminUsers)) {
				$list = array();
				foreach ($adminUsers as $adminUser) { $list[] = $adminUser->user_id; }
				$data['users'] = array_intersect($data['users'], $list);
				if (empty($data['users'])) { $data['users'] = array(0); }
			} else {
				$data['users'] = array(0);
			}
			//courses
			$adminCourses = CoreUserPuCourse::model()->findAllByAttributes(array('puser_id' => Yii::app()->user->id));
			if (!empty($adminCourses)) {
				$list = array();
				foreach ($adminCourses as $adminCourse) { $list[] = $adminCourse->course_id; }
				$data['courses'] = array_intersect($data['courses'], $list);
				if (empty($data['course'])) { $data['courses'] = array(0); }
			} else {
				$data['courses'] = array(0);
			}
		}

		$criteria = new CDbCriteria;
		if (!empty($data['users'])) {
			$criteria->addInCondition('t.idUser', $data['users']);
		}
		if (!empty($data['courses'])) {
			$criteria->addInCondition('t.idCourse', $data['courses']);
		}

		$statusCondition = array();
		$statusParams = array();

		if ($data['filters']['subscription_status'] == 'not_started') {
			$statusCondition[] = 't.status = :statusSubscribed';
			$criteria->params[':statusSubscribed'] = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
		}
		else if ($data['filters']['subscription_status'] == 'in_progress') {
			$statusCondition[] = 't.status = :statusInProgress';
			$criteria->params[':statusInProgress'] = LearningCourseuser::$COURSE_USER_BEGIN;
		}

		/*
		if (!empty($data['filters']['subscribed_status'])) {
			$statusCondition[] = 't.status = :statusSubscribed';
			$criteria->params[':statusSubscribed'] = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
		}
		if (!empty($data['filters']['in_progress_status']) && (int) $data['filters']['in_progress_status'] === 1) {
			$statusCondition[] = 't.status = :statusInProgress';
			$criteria->params[':statusInProgress'] = LearningCourseuser::$COURSE_USER_BEGIN;
		}
		*/

		if (!empty($statusCondition)) {
			$statusConditionStr = implode(' OR ', $statusCondition);
			$criteria->addCondition($statusConditionStr);
		}

		$criteria->with = array('course', 'course.coursesCategory', 'user', 'user.fieldsByKey.core_field');

		// Some reports must be grouped differently
		if ($reportType == LearningReportType::COURSES_USERS) {
			$criteria->group = 't.idCourse';
		}
		else if ($reportType == LearningReportType::USERS_LEARNING) {
			$criteria->with = array('course', 'course.coursesCategory', 'user', 'course.learningOrganizations');
			$criteria->together = TRUE;
		}
		else {
			$criteria->group = 't.idUser, t.idCourse';
		}

		$config = array();

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}

		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';

		if ($isApplySort) {
			if (!empty($_REQUEST[get_class($this).'_sort']) && empty($_REQUEST['ajax']) && empty($_REQUEST[get_class($this).'_page'])) {
				if (!empty(Yii::app()->session[get_class($this).'_sort'])) {
					$sorting = Yii::app()->session[get_class($this).'_sort'];
				} else {
					$sorting = array('field' => $_REQUEST[get_class($this).'_sort'], 'direction' => 'ASC');
				}
				if ($sorting['field'] == $_REQUEST[get_class($this).'_sort']) {
					$sorting['direction'] = ($sorting['direction'] == 'ASC') ? 'DESC' : 'ASC';
				} else {
					$sorting['field'] = $_REQUEST[get_class($this).'_sort'];
					$sorting['direction'] = 'ASC';
				}
				Yii::app()->session[get_class($this).'_sort'] = $sorting;
				$criteria->order = $sorting['field'] . ' ' . $sorting['direction'];
			} elseif (!empty(Yii::app()->session[get_class($this).'_sort'])) {
				$sorting = Yii::app()->session[get_class($this).'_sort'];
				$criteria->order = $sorting['field'] . ' ' . $sorting['direction'];
			} else {
				Yii::app()->session[get_class($this).'_sort']	= array('field' => 'user.userid', 'direction' => 'ASC');
			}
		}
		$config['criteria'] = $criteria;

		$dataProvider = new CActiveDataProvider(get_class($this), $config);

		return $dataProvider;
	}

	public function getCertificateUserLevel() {
		$available_for_status = $this->learningCertificateCourse->available_for_status;

		if (!empty($this->learningCertificateAssign)) {
			return 1;
		} elseif (($available_for_status == 1) || ($available_for_status == 2 && $this->status == self::$COURSE_USER_BEGIN) || ($available_for_status == 3 && $this->status == self::$COURSE_USER_END)) {
			return 2;
		}
		return 0;
	}

	public function renderCertificateStatus() {
		$level = $this->getCertificateUserLevel();

		if ($level === 1) {
			$content = CHtml::link(
				'<span class="cert-generated"></span>', // Link label

				// Link URL
				Docebo::createAdminUrl('certificateManagement/download',
					array(
						'id' => $this->learningCertificateAssign->id_certificate,
						'user_id' => $this->learningCertificateAssign->id_user,
						'course_id' => $this->learningCertificateAssign->id_course,
					)
				),

				// HTML Options
				array(
					'data-title'=>Yii::t('catalogue', '_CERT_RELESABLE'),
					'title'=>Yii::t('catalogue', '_CERT_RELESABLE'),
					'data-toggle'=>'tooltip',
				)
			);
		} elseif ($level === 2) {
			$content = CHtml::link(
				'<span class="cert-cant-generated"></span>', // Link label
				NULL, // URL
				// HTML Options
				array(
					'data-title'=>Yii::t('certificate', '_NO_CERT_AVAILABLE'),
					'title'=>Yii::t('certificate', '_NO_CERT_AVAILABLE'),
					'data-toggle'=>'tooltip',
				)
			);
		}
		return $content;
	}

	/**
	 * This function calculates if an user can enter into the course or not.
	 * It returns an array with the access status and the reason for a blocked access
	 * @return array	on key 'can'' => true or false
	 *					on key 'reason' => it's possible to find these values : 'prerequisites', 'waiting',
	 *                         'course_date', 'course_valid_time', 'user_status', 'course_status',
	 *                         'subscription_not_started', 'subscription_expired'
	 *					on key 'expiring_in' => report the day remaining before the course expire
	 */
	public function canEnterCourse() {
		// control if the user is in a status that cannot enter
		$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:sO'));
		$expiring = FALSE;
		$softDeadline = $this->course->getSoftDeadlineEnabled();

		if ($this->course->date_end && ($this->course->date_end != '0000-00-00')) {
			$utcDateEnd = Yii::app()->localtime->fromLocalDateTime($this->course->date_end) . ' 23:59:59';
			$utcDateEndTimestamp = strtotime($utcDateEnd);
			$exp_time = $utcDateEndTimestamp - $nowTimestamp;
			if ($exp_time > 0) {
				$expiring = round($exp_time/(24*60*60));
			}
		}

		// Checking if there is a valid period for subscription
		if ($this->date_begin_validity && (strpos($this->date_begin_validity, '0000-00-00') === FALSE) && ($this->level <= self::$USER_SUBSCR_LEVEL_STUDENT)) {
			$utcDateBeginValidity = Yii::app()->localtime->fromLocalDateTime($this->date_begin_validity);
			if ($nowTimestamp < strtotime($utcDateBeginValidity))
				return array('can' => FALSE, 'reason' => 'subscription_not_started', 'expiring_in' => $expiring);
		}

		// Checking if there is a valid period for subscription
		if ($this->date_expire_validity && (strpos($this->date_expire_validity, '0000-00-00') === FALSE) && ($this->level <= self::$USER_SUBSCR_LEVEL_STUDENT)) {
			$utcDateExpireValidity = Yii::app()->localtime->fromLocalDateTime($this->date_expire_validity);
			if ($nowTimestamp > strtotime($utcDateExpireValidity) && !$softDeadline)
				return array('can' => FALSE, 'reason' => 'subscription_expired', 'expiring_in' => $expiring);
		}

		// Is course cancelled?
		if ($this->course->status == LearningCourse::$COURSE_STATUS_CANCELLED) {
			return array('can' => FALSE, 'reason' => 'course_status', 'expiring_in' => $expiring);
		}

		if ($this->course->status == LearningCourse::$COURSE_STATUS_PREPARATION) {
			if ($this->level > self::$USER_SUBSCR_LEVEL_STUDENT) {
				return array('can' => TRUE, 'reason' => 'user_status', 'expiring_in' => $expiring);
			} else {
				return array('can' => FALSE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}
		}

		if ($this->course->status == LearningCourse::$COURSE_STATUS_CONCLUDED) {
			if ($this->status == self::$COURSE_USER_END || $this->level > self::$USER_SUBSCR_LEVEL_STUDENT) {
				return array('can' => TRUE, 'reason' => 'user_status', 'expiring_in' => $expiring);
			} else {
				return array('can' => FALSE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}
		}


		if ($this->level > self::$USER_SUBSCR_LEVEL_STUDENT) {
			return array('can' => TRUE, 'reason' => '', 'expiring_in' => $expiring);
		}


		// waiting
		if($this->waiting>=1) {
			return array('can' => FALSE, 'reason' => 'waiting', 'expiring_in' => $expiring);
		}

		// control if the course is elapsed
		if($this->course->date_begin && ($this->course->date_begin != '0000-00-00')) {
            // date/time width MUST BE PROVIDED here
			$time_begin = strtotime(Yii::app()->localtime->fromLocalDate($this->course->date_begin).' 00:00:00');
			if ($nowTimestamp < $time_begin)
				return array('can' => FALSE, 'reason' => 'course_date', 'expiring_in' => $expiring);
		}

		if ($this->course->date_end && ($this->course->date_end != '0000-00-00')) {
			// date/time width MUST BE PROVIDED here
			$time_end = strtotime(Yii::app()->localtime->fromLocalDateTime($this->course->date_end) . ' 23:59:59');
			if ($nowTimestamp > $time_end && !$softDeadline) return array('can' => FALSE, 'reason' => 'course_date', 'expiring_in' => $expiring);
		}

		if ($this->status == LearningCourseuser::$COURSE_USER_SUSPEND || $this->status == LearningCourseuser::$COURSE_USER_OVERBOOKING)
		{
			return array('can' => FALSE, 'reason' => 'user_status', 'expiring_in' => $expiring);
		}


		//Yii::log('Prerequisites idUser ' . $this->idUser . ' idCourse ' . $this->idCourse . '','debug',__CLASS__);
		// Prerequisites
		// Get the list of paths related to the user and the course
		$criteria = new CDbCriteria();
		$criteria->join ='JOIN learning_coursepath_user cu ON t.id_path = cu.id_path';
		$criteria->compare('cu.idUser', $this->idUser);
		$criteria->compare('t.id_item', $this->idCourse);
		$criteria->index = 'id_path'; // Array key will be LP ID


		$userPaths = LearningCoursepathCourses::model()->findAll($criteria);

		$courseBelongsToTheseLPs = array_keys($userPaths);
		$courseBelongsToTheseLPs = array_unique($courseBelongsToTheseLPs);

		if(!empty($courseBelongsToTheseLPs)){
			// Course belongs to at least one Learning Plan
			// Check if the current user's subscription is expired
			// in at least one of these LPs (LP date validity is in the
			// future or its end date has passed)
			$expiringDate = Yii::app()->getDb()->createCommand()
				->select('date_end_validity')
				->from(LearningCoursepathUser::model()->tableName())
				->where('idUser=:idUser', array(':idUser'=>$this->idUser))
				->andWhere(array('IN', 'id_path', $courseBelongsToTheseLPs))
				->andWhere('
					(date_begin_validity IS NOT NULL AND date_begin_validity <> "0000-00-00 00:00:00" AND date_begin_validity > NOW())
					OR
					(date_end_validity IS NOT NULL AND date_end_validity <> "0000-00-00 00:00:00" AND date_end_validity < NOW())
				')
				->order('date_end_validity ASC')
				->limit(1)
				->queryScalar();

			if($expiringDate){
				$expiringDate = Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($expiringDate));
				return array('can' => FALSE, 'reason' => 'subscription_expired', 'expiring_in' => $expiringDate);
			}
		}

		$prerequisitesAvailable = FALSE;
		foreach ($userPaths as $path) {

			// Get the list of prerequisites
			$criteria = new CDbCriteria;
			$criteria->compare('id_path', $path->id_path);
			$criteria->compare('id_item', $this->idCourse);
			$criteria->select = 'prerequisites, mode, delay_time, delay_time_unit';
			$courseUserpaths = LearningCoursepathCourses::model()->findAll($criteria);

			foreach ($courseUserpaths as $path) {
				// Are there the prerequisites ?
				if ($path->prerequisites !== '') {
					$prerequisitesAvailable = TRUE;
					$num_prerequisites = count(explode(',', $path->prerequisites));
					$criteria = new CDbCriteria();
					$criteria->addInCondition("idCourse", explode(',', $path->prerequisites));
					$criteria->compare('idUser', $this->idUser);
					$criteria->compare('status', LearningCourseuser::$COURSE_USER_END);
					// Count
					$countCompleted = LearningCourseUser::model()->count($criteria);

					if ($path->mode !== 'all') {
						// at least n prerequisites are required
						$num_prerequisites = intval($path->mode);
					}

					if ($countCompleted >= $num_prerequisites) {
						$isTimeDelayed = $path->isLocked();
						if (isset($isTimeDelayed[0]) && $isTimeDelayed[0]) {
							// first parameter is for locked condition/true - it's locked/, second parameter is the time of unlocking moment/
							return array('can' => FALSE, 'reason' => 'prerequisites', 'expiring_in' => $expiring);
						}else{
							return array('can' => TRUE, 'reason' => 'prerequisites', 'expiring_in' => $expiring);
						}

					}

				}
			}

			if ($prerequisitesAvailable) {
				return array('can' => FALSE, 'reason' => 'prerequisites', 'expiring_in' => $expiring);
			}

		}

		if ($this->course->course_type == LearningCourse::TYPE_CLASSROOM) {

			$sessionId = Yii::app()->request->getParam('session_id', false);

			$ILTSessionUser = Yii::app()->getDB()->createCommand()
				->select('ltsu.status'.(LtCourseuserSession::model()->hasAttribute('waiting') ? ', ltsu.waiting' : '')) // NOTE: "waiting" column is deprecated
				->from(LtCourseuserSession::model()->tableName() . ' ltsu')
				->join(LtCourseSession::model()->tableName() . ' lts', 'ltsu.id_session = lts.id_session')
				->where('ltsu.id_user = :user AND lts.id_session = :session', array(
					':user' => $this->idUser,
					':session' => $sessionId
				))
				->queryRow();

			if (!$ILTSessionUser){
				return array('can' => TRUE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}

			$allowedStatuses = array(
				LtCourseuserSession::$SESSION_USER_SUBSCRIBED,
				LtCourseuserSession::$SESSION_USER_BEGIN,
				LtCourseuserSession::$SESSION_USER_END
			);
			if (in_array($ILTSessionUser['status'], $allowedStatuses) /* && $ILTSessionUser['waiting'] <= 0*/) { //NOTE: not sure if "waiting" column is still in use
				return array('can' => TRUE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			} else{
				return array('can' => FALSE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}

		} elseif ($this->course->course_type == LearningCourse::TYPE_WEBINAR) {
			$sessionId = Yii::app()->request->getParam('session_id', false);

			$webinarSessionUser = Yii::app()->getDB()->createCommand()
				->select('wsu.status'.(WebinarSessionUser::model()->hasAttribute('waiting') ? ', wsu.waiting' : '')) // NOTE: "waiting" column is deprecated
				->from(WebinarSessionUser::model()->tableName() . ' wsu')
				->join(WebinarSession::model()->tableName() . ' ws', 'wsu.id_session = ws.id_session')
				->where('wsu.id_user = :user AND ws.id_session = :session', array(
					':user' => $this->idUser,
					':session' => $sessionId
				))
				->queryRow();

			if (!$webinarSessionUser){
				return array('can' => TRUE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}

			$allowedStatuses = array(
				WebinarSessionUser::$SESSION_USER_SUBSCRIBED,
				WebinarSessionUser::$SESSION_USER_BEGIN,
				WebinarSessionUser::$SESSION_USER_END
			);
			if (in_array($webinarSessionUser['status'], $allowedStatuses)/* && $webinarSessionUser['waiting'] <= 0*/) { //NOTE: not sure if "waiting" column is still in use
				return array('can' => TRUE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			} else {
				return array('can' => FALSE, 'reason' => 'course_status', 'expiring_in' => $expiring);
			}
		}

		// user is not a tutor or a prof and the course isn't active
		if ($this->status != 1 && $this->level <= self::$USER_SUBSCR_LEVEL_STUDENT) {
			return array('can' => TRUE, 'reason' => 'course_status', 'expiring_in' => $expiring);
		}

		return array('can' => TRUE, 'reason' => '', 'expiring_in' => $expiring);

	}

	public function cantEnterCourseMessage() {
		$canEnterCourse = $this->canEnterCourse();
		$message = '';
		if(isset($canEnterCourse['can']) && $canEnterCourse['can'] == false && isset($canEnterCourse['reason'])){
			switch($canEnterCourse['reason'])
			{
				//add more from the canEnterCourse method
				case 'course_date':
					$nowTimestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
					if($this->course->date_begin && ($this->course->date_begin != '0000-00-00')) {
						$time_begin = strtotime(Yii::app()->localtime->fromLocalDate($this->course->date_begin).' 00:00:00');
						if ($nowTimestamp < $time_begin)
							$message = Yii::t('curricula', 'Course enrollment period has not started');
					}

					if ($this->course->date_end && ($this->course->date_end != '0000-00-00')) {
						$time_end = strtotime(Yii::app()->localtime->fromLocalDateTime($this->course->date_end) . ' 23:59:59');
						if ($nowTimestamp > $time_end && ! $this->course->getSoftDeadlineEnabled())
							$message = Yii::t('curricula', 'Course enrollment period has passed');
					}

					return $message;
				case 'subscription_not_started' :
					return Yii::t('curricula', 'Course enrollment period has not started');
				case 'subscription_expired' :
					return Yii::t('curricula', 'Course enrollment period has passed');
			}
		}

		return $message;
	}
	/**
	 * Pretty basic function to subscribe a user to a course.
	 * It does not make anu check against permissions, overbooking, e-commerce whatsoever.
	 * Just subscribe the user to a course. All check should be done at controller/action level.
	 *
	 * @param $idUser
	 * @param $subscribedByIdUser
	 * @param $idCourse
	 * @param int $waiting
	 * @param bool $level
	 * @param bool $is_cart_presubscription
	 * @param bool $date_begin_valid
	 * @param bool $date_expire_valid
	 * @param bool $learningPlanContext
	 * @param bool $sessionId
	 * @param bool $deeplinkedBy
	 * @param bool $updatePowerUserSelection
	 * @return bool
	 */
	public function subscribeUser($idUser, $subscribedByIdUser, $idCourse, $waiting = 1, $level = FALSE, $is_cart_presubscription = false,$date_begin_valid=false,$date_expire_valid=false, $learningPlanContext=false, $sessionId = false, $deeplinkedBy = false, $fields = null, $updatePowerUserSelection = false) {
		$result = TRUE;
		// Check if user is already subscribed
		if (LearningCourseuser::isSubscribed($idUser, $idCourse)) {
			Yii::log("User ($idUser) is already subscribed to this course ($idCourse)", 'warning', __CLASS__);
			$this->errorMessage = self::$ALREADY_SUBSCRIBED;
			return FALSE;
		}

		// Incoming Level ?
		if ($level === FALSE) {
			$this->level = self::$USER_SUBSCR_LEVEL_STUDENT;
		} else {
			$this->level = $level;
		}
		$this->idUser = $idUser;
		$this->idCourse = $idCourse;
		$this->edition_id = 0;
		$this->date_inscr = Yii::app()->localtime->toLocalDateTime();
		$this->subscribed_by = $subscribedByIdUser;

		$this->waiting = $waiting;
		if ($waiting) {
			$this->status = self::$COURSE_USER_WAITING_LIST;
		}
		if ($date_begin_valid)
			$this->date_begin_validity = $date_begin_valid;
		if ($date_expire_valid)
			$this->date_expire_validity = $date_expire_valid;

		if($fields)
			$this->enrollment_fields = $fields;

        // DB Start transaction
		if (Yii::app()->db->getCurrentTransaction() === NULL) {
			$db_transaction = Yii::app()->db->beginTransaction();
		}
		if ($deeplinkedBy) {
			$this->deeplinked_by = (int)$deeplinkedBy;
		}

		try {
			$result &= $this->save();
			if (!$result) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			// Assign auth group/item to user
			// $auth = Yii::app()->authManager;

			// This will throw an exception in case of error
			//$token = '/lms/course/'.$idCourse.'/subscribed/'.$this->level;
			//$auth->assign($token, $idUser);

			$course = LearningCourse::model()->findByPk($idCourse);
			$user = CoreUser::model()->findByPk($idUser);

			if($waiting){
				// User subscription for course needs admin approval
				if(!$is_cart_presubscription)
				{
					Yii::app()->event->raise('UserWaitingSubscribe', new DEvent($this, array(
						'course' => $course,
						'user' => $user,
						'date_expire_validity' => $this->date_expire_validity,
						'sessionId' => $sessionId,
					)));
				}
			}else{
				// User subscribed himself successfully
				Yii::app()->event->raise(EventManager::EVENT_USER_SUBSCRIBED_COURSE, new DEvent($this, array(
					'course' => $course,
					'user' => $user,
					'enrollment'	=> $this,
					'date_expire_validity' => $this->date_expire_validity ? Yii::app()->localtime->fromLocalDateTime($this->date_expire_validity) : null,
					'date_end' => ($course->date_end != '0000-00-00') ? Yii::app()->localtime->fromLocalDate($course->date_end) : null,
					'learningPlanContext' => $learningPlanContext,
					'updatePowerUserSelection' => $updatePowerUserSelection
				)));
			}

			if (isset($db_transaction)) {
				$db_transaction->commit();
			}
			$result = TRUE;

		} catch (Exception $e) {
			if (isset($db_transaction)) { $db_transaction->rollback(); }
			Yii::log($e->getMessage(), 'debug', __CLASS__);
			$result = FALSE;
			$this->errorMessage = self::$SUBSCRIBE_FAILURE;
		}
		return $result;

	}


	/**
	 * Pretty basic function to unsubscribe user from a course/edition
	 * @param $idUser
	 * @param $idCourse
	 * @param null $idEdition
	 * @return bool
	 */
	public function unsubscribeUser($idUser, $idCourse, $idEdition = NULL)  {
		$result = TRUE;
		$criteria = new CDbCriteria;
		$criteria->compare("idUser", $idUser);
		$criteria->compare("idCourse", $idCourse);
		if ($idEdition !== NULL)
			$criteria->compare("edition_id", $idEdition);

		$courseUserModel = $this->find($criteria);
		$level = $courseUserModel->level;

		// If coaching is enabled try to find and clean all user messages
		if($courseUserModel->course->enable_coaching == 1) {
			// Remove garbage coaching messages here
			if ($level == self::$USER_SUBSCR_LEVEL_STUDENT) {
				// Get all user course coaching sessions
				$coachingSessions = LearningCourseCoachingSession::model()->userAssignedSessions($idUser, false, $idCourse);

				foreach ($coachingSessions as $idSession) {
					LearningCourseCoachingMessages::model()->deleteAllByAttributes(array('idLearner' => $idUser, 'idSession' => $idSession));
				}
			} elseif ($level == self::$USER_SUBSCR_LEVEL_COACH) {
				// Get all user course coaching sessions
				$coachingSessions = $courseUserModel->course->getCoachSessions($idUser);

				foreach ($coachingSessions as $idSession) {
					LearningCourseCoachingMessages::model()->deleteAllByAttributes(array('idCoach' => $idUser, 'idSession' => $idSession));
				}
			}
		}


		Yii::log("Unsubscribing: User/Course/Edition/Level: $idUser, $idCourse, $idEdition, $level", 'debug', __CLASS__ );

		$result &= $this->deleteAll($criteria);

		if ($result) {

			// Cleanup coaching session <-->  user relations : unsubscribed users must be removed from any coaching session
			if($this->course->coachingSessions){
				foreach ($this->course->coachingSessions as $session) {
					$session->deassignUsers($idUser);
				}
			}
			// Raise event that user is unsubscribed from course
			Yii::app()->event->raise(EventManager::EVENT_USER_UNSUBSCRIBED, new DEvent($this, array(
				'user' => $this->user, // AR relation
				'course' => $this->course, // AR relation
			)));
		}

		return $result;
	}


	/**
	 * Pretty basic function to activate a subscription (sets "waiting" to "0")
	 *
	 * @param number $idUser
	 * @param number $idCourse
	 * @param number $idEdition
	 *
	 * @param bool   $activate
	 *
	 * @return bool
	 */
	public function activateSubscription($idUser, $idCourse, $idEdition, $activate=TRUE)  {
	    $criteria = new CDbCriteria;
	    $criteria->compare("idUser", $idUser);
	    $criteria->compare("idCourse", $idCourse);
	    $criteria->compare("edition_id", $idEdition);

	    Yii::log("Activate subscription: User/Course/Edition: $idUser, $idCourse, $idEdition", 'debug', __CLASS__ );

	    $model = $this->find($criteria);

	    if ( !$model ) {
	        Yii::log("An attempt to activate non-existent User/Course/Edition: $idUser, $idCourse, $idEdition", 'error', __CLASS__ );
	        return FALSE;
	    }

        if ($activate) {
            $model->waiting = 0;
			if($model->status == self::$COURSE_USER_WAITING_LIST)
				$model->status  = self::$COURSE_USER_SUBSCRIBED;
        } else {
            $model->waiting = 1;
            $model->status  = self::$COURSE_USER_WAITING_LIST;
        }
	    $save = $model->save();

		if($save){
			$event = new DEvent($this, array(
				'idCourse'=>$idCourse,
				'idUser'=>$idUser,
				'idEdition'=>$idEdition,
				'activate'=>$activate,
			));
			Yii::app()->event->raise('onAfterActivateCourseSubscription', $event);
			if(!$event->shouldPerformAsDefault()) return $event->return_value;
		}

		return $save;
	}


	/**
	 * Change "waiting" status of a subscription to "1", meaning, "wait for approval"
	 *
	 * @param number $idUser
	 * @param number $idCourse
	 * @param number $idEdition
	 */
	public function suspendSubscription($idUser, $idCourse, $idEdition) {
		return $this->activateSubscription($idUser, $idCourse, $idEdition, FALSE);
	}


	/**
	 * Return if the user is subscribed or not
	 * @param $idUser
	 * @param $idCourse
	 * @return bool
	 */
	public static function isSubscribed($idUser, $idCourse)
	{
		return self::model()->exists('idUser=:idUser AND idCourse=:idCourse', array(":idUser" => $idUser, ":idCourse" => $idCourse));
	}

	/**
	 * Returns the number of courses a user is subscribed to
	 * @param $idUser
	 * @return int
	 */
	public static function getCountSubscriptions($idUser)
	{
		// "with course" => Count only enrollments to existing courses; just as a precaution measure
		$countSubscriptions = self::model()->with('course')->countByAttributes(array("idUser" => $idUser));
		return $countSubscriptions;
	}



	/**
	 * Get list of enrollments
	 *
	 * @param number $idCourse
	 * @param number $idUser
	 * @param object $filterObject  Standard Class object, used to do filtering. Read the code and see what you can use (level, status, etc.)
	 * 				 As of this writing: filterAdmin (power user filter), level, status, search, withTracksession, customSelect
	 *
	 * @return array
	 */
	public static function getEnrollments(
			$idCourse=false,
			$idUser=false,
			$filterObject=null,
			$justCount=false,
			$customSelect=false) {


		// Apply simple  caching mechanic
		static $cache = array();
		$cacheKey = sha1(serialize(func_get_args()));
		if (isset($cache[$cacheKey])) {
			return $cache[$cacheKey];
		}


		if ($filterObject === null) {
			$filterObject = new stdClass();
		}

		// Start building SQL parameters array
		$params = array();


		$commandBase = Yii::app()->db->createCommand()
			->from('learning_courseuser enrollment')
			->join('core_user user', 'user.idst=enrollment.idUser') // existing users only
			->join('learning_course course', 'course.idCourse=enrollment.idCourse'); // existing courses only


		// FILTER BY: Course
		if ($idCourse) {
			$commandBase->andWhere('enrollment.idCourse=:idCourse');
			$params[':idCourse'] = $idCourse;
		}

		// FILTER BY: User
		if ($idUser) {
			$commandBase->andWhere('enrollment.idUser=:idUser');
			$params[':idUser'] = $idUser;
		}


		// FILTER BY: Power User (by default: YES
                  $pUserRights = Yii::app()->user->checkPURights($idCourse);

		if ($pUserRights->isPu && !$pUserRights->isInstructor && ( ($filterObject->filterAdmin == null) || ($filterObject->filterAdmin) )  ) {
			$commandBase->join(CoreUserPU::model()->tableName()." pu", "pu.puser_id = " . Yii::app()->user->id . " AND enrollment.idUser = pu.user_id");
			$commandBase->join(CoreUserPuCourse::model()->tableName()." puc", "puc.puser_id = "  . Yii::app()->user->id .  " AND puc.course_id = :idCourse");
		}
		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($idCourse);
			if(!empty($instructorUsers))
				$commandBase->andWhere('enrollment.idUser IN ('.implode(',', $instructorUsers).')');
		}

		// FILTER BY: LEVEL
		if ($filterObject->level) {
			if (is_array($filterObject->level)) {
				$filterObject->level = implode(',', $filterObject->level);
			}
			$commandBase->andWhere('enrollment.level IN (' . $filterObject->level . ')');
		}

		// FILTER BY: STATUS
		if ($filterObject->status) {
			if (is_array($filterObject->status)) {
				$filterObject->status = implode(',', $filterObject->status);
			}
			$commandBase->andWhere('enrollment.status IN (' . $filterObject->status . ')');
		}

		// FILTER BY: Search for some text for user or course ?
		if ($filterObject->search) {
			$commandBase->andWhere('CONCAT(user.firstname, user.lastname, user.email, user.userid, course.name, course.description) LIKE :search');
			$params[':search'] = '%' . $filterObject->search . '%';
		}


		// Also collect learning tracksession statistics for every user
		if ($filterObject->withTracksession) {
			$commandBase->leftJoin(
				'(
				 SELECT SUM(UNIX_TIMESTAMP(lts.lastTime) - UNIX_TIMESTAMP(lts.enterTime)) AS sessionTime, lts.idUser as idUserInner
				 FROM learning_tracksession lts
				 WHERE (lts.idCourse=:idCourse)
				 GROUP BY lts.idUser
				 ) trackSession',
				'trackSession.idUserInner=enrollment.idUser'
			);
		}


		// SELECT
 		$select = array();
 		if ($filterObject->withTracksession) {
 			$select[] = "trackSession.sessionTime";
 		}

		if ($filterObject->customSelect) {
			$select[] = $filterObject->customSelect;
		}
		else {
			$select[] = 'user.idst AS idUser';
		}


		if ($customSelect) {
			$select[] = $customSelect;
		}

		// Maybe we are called just to count the users??
		if ($justCount) {
			$select = array('count(user.idst) as countUsers');
		}

		$commandBase->select(implode(',', $select));

		// APPLY Parameters
		$commandBase->params = $params;

		// Get data
		$rows = $commandBase->queryAll();

		// Just count the enrolled users? Return scalar integer
		if ($justCount) {
			$cache[$cacheKey] = (int) $rows[0]["countUsers"];
			return $cache[$cacheKey];
		}

		$cache[$cacheKey] = $rows;

		return $rows;
	}

	/**
	 * Count subscriptions by course and level
	 *
	 * @param number $idCourse
	 * @param number $level
	 * @return number
	 */
	public static function getCountEnrolledByLevel($idCourse, $level = NULL, $withWaiting = TRUE) {
		$attributes = array('idCourse' => $idCourse);
		if ($level !== NULL)
			$attributes['level'] = $level;
		if(!$withWaiting)
			$attributes['waiting'] = 0;

        $model = self::model();
        $model->with(array('user' => array('joinType' => 'INNER JOIN')));

        if(Yii::app()->user->getIsAdmin()) {
            $criteria = new CDbCriteria();
            $criteria->together = true;
            $criteria->with = array( 'user.onlyAssignedUsers' => array(
                'joinType' => 'INNER JOIN',
                'on' => 'onlyAssignedUsers.puser_id = :puser_id',
                'params' => array(
                    ':puser_id' => Yii::app()->user->id,
                )
            ));

            $model->getDbCriteria()->mergeWith($criteria);
        }

        return $model->countByAttributes($attributes);
	}



	/**
	 * Count enrolled Admins to a given course
	 *
	 * @param number $idCourse
	 * @return number
	 */
	public static function getCountEnrolledAdmins($idCourse) {
		//$adminTypes = array(self::$USER_SUBSCR_LEVEL_ADMIN, self::$USER_SUBSCR_LEVEL_INSTRUCTOR, self::$USER_SUBSCR_LEVEL_MENTOR, self::$USER_SUBSCR_LEVEL_TUTOR);
		$adminTypes = array(self::$USER_SUBSCR_LEVEL_ADMIN, self::$USER_SUBSCR_LEVEL_INSTRUCTOR, self::$USER_SUBSCR_LEVEL_MENTOR, self::$USER_SUBSCR_LEVEL_TUTOR, self::$USER_SUBSCR_LEVEL_COACH);
		$condition = self::model()->getTableAlias().'.idCourse=' . $idCourse . ' AND '.self::model()->getTableAlias().'.level IN (' . implode(',',$adminTypes) . ')';

		$model = self::model();
		$model->with(array('user' => array('joinType' => 'INNER JOIN')));

		if(Yii::app()->user->getIsAdmin()) {
			$criteria = new CDbCriteria();
			$criteria->together = true;
			$criteria->with = array( 'user.onlyAssignedUsers' => array(
				'joinType' => 'INNER JOIN',
				'on' => 'onlyAssignedUsers.puser_id = :puser_id',
				'params' => array(
					':puser_id' => Yii::app()->user->id,
				)
			));

			$model->getDbCriteria()->mergeWith($criteria);
		}

		return $model->count($condition);

	}

	/**
	 * Get he subscriber role level in a course (student, teacher, admin, ...)
	 * @param $idUser
	 * @param $idCourse
	 * @return int
	 */
	public static function userLevel($idUser, $idCourse) {
	    $model = self::model()->findByAttributes(array("idUser" => $idUser, "idCourse" => $idCourse));
	    if ($model) {
	        return $model->level;
	    }
	    else {
	        return 0;
	    }
	}
    /**
     * Checks the subscriber role level in a course (studen, teacher, admin, ...) if it's the desired level
     * @param int $idUser
     * @param int $idCourse
     * @param int $level Default is $USER_SUBSCR_LEVEL_INSTRUCTOR
     * @return bool
     */
    public static function isUserLevel($idUser, $idCourse, $level = null) {
        $level = $level != null ? $level : self::$USER_SUBSCR_LEVEL_INSTRUCTOR;
        $lvl = self::userLevel($idUser, $idCourse);
        return $lvl == $level;
    }

	/**
	 * Get level label - translated
	 * @return string
	 */
	public function getLevelLabel()
	{
		return Yii::t("levels", "_LEVEL_".$this->level);
	}

	/**
	 * Get status label - translated
	 * @return string
	 */
	public static function getStatusLabel($status)
	{
		$languageVariable = self::getStatusLangLabel($status);
		return Yii::t('subscribe', $languageVariable);
	}

	/**
	 * Color status label for the grids
	 * @return string
	 */
	public static function getStatusLabelColored($status, $textOnly=false)
	{
		$color = '#000000';
		$label = '';
		switch($status)
		{
			case self::$COURSE_USER_SUBSCRIBED:
				$label = Yii::t('standard', 'To start');
				$color = '#DB0800';
				break;
			case self::$COURSE_USER_END:
				$color = '#00661A';
				break;
			case self::$COURSE_USER_BEGIN:
				$color = '#FFBF00';
				break;
			case self::$COURSE_USER_CONFIRMED:
			case self::$COURSE_USER_OVERBOOKING:
			case self::$COURSE_USER_SUSPEND:
			case self::$COURSE_USER_WAITING_LIST:
				break;
		}
		if ($label == '') //we can set something custom like "Not yet started" instead of "Subscribed"
			$label = self::getStatusLabel($status);


		if ($textOnly) {
			return $label;
		}

		return '<span style="color: '.$color.'; font-weight: bold;">'.$label.'</span>';
	}

	/**
	 * Return the specific level language constant string
	 * @param $status
	 * @return string
	 */
	public static function getStatusLangLabel($status)
	{
		$languageVariable = '';
		switch($status)
		{
			case self::$COURSE_USER_WAITING_LIST:
				$languageVariable = '_WAITING_USERS';
				break;
			case self::$COURSE_USER_CONFIRMED:
				$languageVariable = '_USER_STATUS_CONFIRMED';
				break;
			case self::$COURSE_USER_SUBSCRIBED:
				$languageVariable = '_USER_STATUS_SUBS';
				break;
			case self::$COURSE_USER_BEGIN:
				$languageVariable = '_USER_STATUS_BEGIN';
				break;
			case self::$COURSE_USER_END:
				$languageVariable = '_USER_STATUS_END';
				break;
			case self::$COURSE_USER_SUSPEND:
				$languageVariable = '_USER_STATUS_SUSPEND';
				break;
			case self::$COURSE_USER_OVERBOOKING:
				$languageVariable = '_USER_STATUS_OVERBOOKING';
				break;
		}
		return $languageVariable;
	}

    /**
     * Get array of all available levels
     * @param array $restrict Return only the levels contained in this array
     * @param array $remove Exclude the levels contained in this array
     * @return array
     */
	public static function getLevelsArray($restrict = array(), $remove = array())
	{
		$levels = array(
			self::$USER_SUBSCR_LEVEL_STUDENT => Yii::t("levels", "_LEVEL_".self::$USER_SUBSCR_LEVEL_STUDENT),
			self::$USER_SUBSCR_LEVEL_TUTOR => Yii::t("levels", "_LEVEL_".self::$USER_SUBSCR_LEVEL_TUTOR),
			self::$USER_SUBSCR_LEVEL_INSTRUCTOR => Yii::t("levels", "_LEVEL_".self::$USER_SUBSCR_LEVEL_INSTRUCTOR)
		);

		if(PluginManager::isPluginActive('CoachingApp'))
			$levels[self::$USER_SUBSCR_LEVEL_COACH] = Yii::t("levels", "_LEVEL_".self::$USER_SUBSCR_LEVEL_COACH);

		// Prepare the levels to be returned here and exclude those, that are not in the $restrict array
        if (!empty($restrict)) {
            foreach ($levels as $level => $label) {
                if (!in_array($level, $restrict))
                    unset($levels[$level]);
            }
        }

		// Remove excluded levels here
		if (!empty($remove)) {
			foreach ($levels as $level => $label) {
				if (in_array($level, $remove))
					unset($levels[$level]);
			}
		}

        return $levels;
	}


	/**
	 * Return Lowest and Highest ID of a User Level in a course (student, tutor, admin, ...)
	 *
	 * @return array Of "min" => <value-number>, "max" => <value-number>
	 */
	public static function getMinMaxLevelId() {
		$levels = array_keys(self::getLevelsArray());
		return array(
			'min'	=> min($levels),
			'max'	=> max($levels),
		);
	}



	/**
	 * Get array of all available statuses
	 * @return array
	 */
	public static function getStatusesArray()
	{
		return array(
			self::$COURSE_USER_WAITING_LIST => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_WAITING_LIST)),
			self::$COURSE_USER_CONFIRMED => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_CONFIRMED)),
			self::$COURSE_USER_SUBSCRIBED => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_SUBSCRIBED)),
			self::$COURSE_USER_BEGIN => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_BEGIN)),
			self::$COURSE_USER_END => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_END)),
			self::$COURSE_USER_SUSPEND => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_SUSPEND)),
			self::$COURSE_USER_OVERBOOKING => Yii::t("subscribe", self::getStatusLangLabel(self::$COURSE_USER_OVERBOOKING)),
		);
	}

	/**
	 * Reset active dates method
	 * @param $userId
	 * @param $courseId
	 * @return bool
	 */
	public function resetActiveDates($userId, $courseId)
	{
		$model = $this->findByAttributes(array("idUser" => $userId, "idCourse" => $courseId));
		if ($model)
		{
			$model->date_begin_validity = NULL;
			$model->date_expire_validity = NULL;
			return $model->save(FALSE);
		}
		else
			return FALSE;
	}


	/**
	 * Returns true if it the first time when the user enters in this course
	 * @param $idUser
	 * @param $idCourse
	 * @return bool
	 */
	public static function isFirstAccess($idUser, $idCourse) {
		$enroll = LearningCourseuser::model()->findByPk(array(
			'idCourse' => (int)$idCourse,
			'idUser' => (int)$idUser,
			'edition_id' => 0 //this will hopefully be removed in future database revisions of the application
		));

		return ($enroll && (!$enroll->date_first_access || $enroll->date_first_access=='0000-00-00 00:00:00'));
	}


	public static function updateCourseUserAccess($idUser, $idCourse, $lastAccessDate = FALSE) {
		//first step: check input data validity
		$enroll = LearningCourseuser::model()->findByPk(array(
			'idCourse' => (int)$idCourse,
			'idUser' => (int)$idUser,
			'edition_id' => 0 //this will hopefully be removed in future database revisions of the application
		));
		if (!$enroll) {
			return;
			//throw new CException('The user is not enrolled to the course');
		}
//		$course = LearningCourse::model()->findByPk(array(
//			'idCourse' => (int)$idCourse,
//		));

		// Get local datetime
		$nowLocal = Yii::app()->localtime->toLocalDateTime();

		// Set the last access to LOCAL time (or to the time passed as parameter, if any)
		$enroll->date_last_access = (!empty($lastAccessDate) ? $lastAccessDate : $nowLocal);

		// If first access is not yet registered, set it to Local Now
		if ( ($enroll->date_first_access == null) || ($enroll->date_first_access == '0000-00-00 00:00:00') ) {
			$enroll->date_first_access = $nowLocal;

            // Inform the world about first access in a course by a user
			Yii::app()->event->raise(EventManager::EVENT_USER_FIRST_ACCESS_IN_COURSE, new DEvent(self, array(
			    'idCourse'   => $idCourse,
			    'idUser'     => $idUser,
			)));
		}

		// If status is COURSE_USER_SUBSCRIBED (0), make it BEGIN
		if ( (int)$enroll->status === self::$COURSE_USER_SUBSCRIBED) {
			$enroll->status = self::$COURSE_USER_BEGIN;
		}

		$rs = $enroll->save();
		if (!$rs) {
			throw new CException('Error while updating course last access date');
		}

		// Now lets track Course User SESSION
		LearningTracksession::track($idUser, $idCourse);

	}

	public function afterFind()
	{
		// Store unchanged/original attributes so we can compare against them during AR save
		$this->oldAttributes = $this->attributes;

		return parent::afterFind();
	}

	public function beforeSave()
	{
		if(!$this->getIsNewRecord()) {
			if(isset($this->oldAttributes['level']) && $this->level != $this->oldAttributes['level']){
				// User level changed in course
				Yii::app()->event->raise('UserCourselevelChanged', new DEvent($this, array(
					'course' => $this->course, // AR relation
					'user' => $this->user, // AR relation
					'courseuser' => $this,
				)));
			}

			if(isset($this->oldAttributes['status'])   && ($this->status != $this->oldAttributes['status']) && (Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPu())){
				// User status changed in course
				Yii::app()->event->raise('UserEnrollmentStatusChanged', new DEvent($this, array(
					'course' => $this->course, // AR relation
					'user' => $this->user, // AR relation
					'courseuser' => $this,
				)));

			}

			// if the user is a Power User make some validations for the actions that he can do with enrollments statuses.
			if (Yii::app()->user->getIsPu() && LearningCourse::isCourseSelling($this->idCourse)) {
				$statuses_translations = self::getStatusesArray();
				$power_user_set_status_error = false;

				// Conditions that has to be met for the Power User to change the status of the enrolled user
				$conditions = array(
					array(
						'cond' => ($this->oldAttributes['status'] == self::$COURSE_USER_END && $this->status != $this->oldAttributes['status']),
						'available' => array($this->oldAttributes['status'])
					),
					array(
						'cond' => ($this->oldAttributes['status'] == self::$COURSE_USER_SUBSCRIBED),
						'available' => array(self::$COURSE_USER_BEGIN, self::$COURSE_USER_END, self::$COURSE_USER_SUSPEND, $this->oldAttributes['status'])
					),
					array(
						'cond' => ($this->oldAttributes['status'] == self::$COURSE_USER_BEGIN),
						'available' => array(self::$COURSE_USER_END/*, self::$COURSE_USER_SUSPEND*/, $this->oldAttributes['status'])
					)
				);

				// Executing the conditions
				foreach ($conditions as $condition) {
					if ($condition['cond']) {
						if (!in_array($this->status, $condition['available'])) {
							$power_user_set_status_error = true;
							break;
						}
					}
				}

				// if the conditions are not met then there is an error 'power_user_set_status_error' and return a message to the Power User:
				if ($power_user_set_status_error) {
					$this->addError('status', Yii::t('course_management', 'Power Users cannot change enrollment status from {status_1} to {status_2}',
						array(
							'{status_1}' => $statuses_translations[$this->oldAttributes['status']],
							'{status_2}' => $statuses_translations[$this->status]
						)
					));
					return false;
				}
			}

			// The user has completed the course
			if ($this->status == self::$COURSE_USER_END) {

				// Set the 'date_complete' if not set previously
				if (!$this->date_complete || $this->date_complete == NULL) {
					$this->date_complete = Yii::app()->localtime->getUTCNow();
				}

                if($this->oldAttributes['status'] != self::$COURSE_USER_END){
                    // Enrollment status changed from [something else] to Ended
                    // Update the score_given with the dynamic calculation
                    // if not set already (e.g. manually by admin)
                    // Not calculating score for enrollments that have forced set score value
                    if(empty($this->score_given) && ((boolean) $this->forced_score_given) !== true){
                        $this->score_given = self::recalculateLastScoreByUserAndCourse($this->idCourse, $this->idUser);
                    }

					// Update the initial_score_given with the dynamic calculation
					// if not set already (e.g. manually by admin)
					if(empty($this->initial_score_given)){
						$this->initial_score_given = self::recalculateInitialScoreGiven($this->idUser, $this->idCourse);
					}
                }
            }

			// Prevent "seat manager" Power users from changing
			// user enrollments status back to Subscribed, because
			// this will allow him to unenroll that user regularly
			// afterwards, as if that user never used a "seat"
			if(CoreUserPU::isPUAndSeatManager()){
				$validFromStatusesForTransition = array(self::$COURSE_USER_WAITING_LIST, self::$COURSE_USER_CONFIRMED, self::$COURSE_USER_SUBSCRIBED);
				if(!in_array($this->oldAttributes['status'], $validFromStatusesForTransition) && $this->status == LearningCourseuser::$COURSE_USER_SUBSCRIBED){
					// Seat was used and is not reclaimable by switching
					// the enrollment back to "Subscribed" and unenrolling
					$this->addError('status', Yii::t('course_management', 'Unable to change status to Subscribed because the seat is used'));
					return false;
				}
			}

			// Update date_expire_validity to (first access + valid time days, if any)
			if (intval($this->course->valid_time) > 0
				&& $this->course->valid_time_type == LearningCourse::VALID_TIME_TYPE_FIRST_COURSE_ACCESS
				&& $this->date_first_access != '' && ($this->oldAttributes['date_first_access'] != $this->date_first_access)
				&& (is_null($this->date_expire_validity) || $this->date_expire_validity == '0000-00-00 00:00:00' || $this->date_expire_validity == '')
			) {
				$this->date_begin_validity = $this->date_first_access;
				$this->date_expire_validity = Yii::app()->localtime->addInterval($this->date_first_access, 'P' . intval($this->course->valid_time) . 'D');
			}
		}
		else {
            if(Yii::app()->getController()->action->id != 'axRegister')
                $this->updateFromEquivalentEnrollments();

			if ($this->scenario != 'self-subscribe'/* && $this->idUser != $this->subscribed_by*/) {
				// If current user is a Power Admin, if 'direct_course_subscribe' is NOT enabled, "waiting" must be set to 1
				// which means "user will be subscribed but need to be Approved"
				if (Yii::app()->user->getIsAdmin()) {
					if (!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe')) {
						$this->waiting = 1;
					}
				}
			}
		}

		//set expire date if 'Days of validity' is set and 'after being enrolled in the course' is checked in course settings
		if (intval($this->course->valid_time) > 0
			&& $this->course->valid_time_type == LearningCourse::VALID_TIME_TYPE_BEING_ENROLLED
			&& ($this->status != $this->oldAttributes['status'] || $this->getIsNewRecord())
			&& $this->status == self::$COURSE_USER_SUBSCRIBED
			&& (is_null($this->date_expire_validity) || $this->date_expire_validity == '0000-00-00 00:00:00' || $this->date_expire_validity == '')
		) {
			//$this->date_begin_validity = $this->date_inscr;
			$this->date_begin_validity 	= ($this->status != $this->oldAttributes['status']) ?
				Yii::app()->localtime->toLocalDateTime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:sO')) :
				$this->date_inscr;
			$this->date_expire_validity = Yii::app()->localtime->addInterval($this->date_begin_validity, 'P' . intval($this->course->valid_time) . 'D');
		}

		if (isset($this->oldAttributes['status']) && $this->status != $this->oldAttributes['status'] && !$this->validPropagatorId){
			$this->status_propagator = null;
		}

		return parent::beforeSave();
	}

	public function afterSave(){

		if($this->status == self::$COURSE_USER_END){
			// User ended course.

			// See if there is a certificate assigned to this course
			$certificate = LearningCertificateCourse::model()->findByAttributes(array(
				'id_course'=>$this->idCourse
			));

			//Triggering an event that check if there is a certificate for the branch that the user is assigned to
 			$branchCertificate = Yii::app()->event->raise('StudentCourseCompleteBranchCertificateCheck', new DEvent($this, array(
				'user' => $this->user, // AR relation
			)));

			if(is_array($branchCertificate) && isset($branchCertificate['certificate']) ){
				$certificate = $branchCertificate['certificate'];
			}

			if($certificate && !$this->skip_certificate_generation){
				// The course has a Certificate for completion assigned

				$certificateId = $certificate->id_certificate;

				$oldCertificate = LearningCertificateAssign::model()->findByAttributes(array(
					'id_certificate' => $certificateId,
					'id_user' => $this->idUser,
					'id_course' => $this->idCourse,
				));

              if(!$oldCertificate || (isset($this->oldAttributes['score_given']) && $this->score_given != $this->oldAttributes['score_given'])){
                // Override certificate even if previously generated
                if($oldCertificate)
	                $oldCertificate->delete();

                // Trigger event in case listeners want to handle
                // certificate creation instead
                $event = new DEvent($this, array('course'=>$this->course, 'user'=>$this->user));
                Yii::app()->event->raise('GenerateCertificate', $event);

                if(!$event->handled) {
	                try{
		                $substituton = Yii::app()->certificateTag->getSubstitution($this->idUser, $this->idCourse);
		                Yii::app()->certificate->create($certificateId, $this->idUser, $this->idCourse, $substituton, FALSE);
	                }catch(Exception $e){
		                Yii::log('Can not generate certificate for course. LearningCourseuser::afterSave', CLogger::LEVEL_ERROR);
	                }
                }
              }
			}

			//assign a badge
			if((isset($this->oldAttributes['status']) && $this->status != $this->oldAttributes['status']) || $this->completedByEquivalenceOfCourse){
				// Course completion event
				Yii::app()->event->raise(EventManager::EVENT_STUDENT_COMPLETED_COURSE, new DEvent($this, array(
					'course' => $this->course, // AR relation
					'user' => $this->user, // AR relation
				)));
				$event = new DEvent($this, array(
					'course' => $this->course,
					'user' => $this->user,
				));
				Yii::app()->event->raise('NewCourseUnlockedInLearningPlan', $event);
				$this->completeEquivalentCourses();
			}
		}

		//delete generated certificate if change user`s status from "completed" to "non-completed"
		if(isset($this->oldAttributes['status']) && $this->oldAttributes['status'] == self::$COURSE_USER_END  && $this->status != self::$COURSE_USER_END) {
			$issuedCertificates = LearningCertificateAssign::model()->findAllByAttributes(array(
				'id_user' => $this->idUser,
				'id_course' => $this->idCourse,
			));
			if (!empty($issuedCertificates)) {
				foreach ($issuedCertificates as $certificate) {
					$certificate->delete();
				}
			}
		}

		if($this->course->course_type == LearningCourse::TYPE_ELEARNING
			&& ($this->getIsNewRecord() //new enrollment
				|| (!$this->getIsNewRecord() && isset($this->oldAttributes['waiting']) && $this->oldAttributes['waiting'] == 1) // or approved enrollment
			&& $this->waiting == 0)) // not in waiting list
		{
			$this->course->updateMaxSubscriptionQuota();
		}

		if (!$this->getIsNewRecord()){
			//Update course Learning Plans end validity, if is set "Days of validity" and checked "after first course access" in Learning Plan settings
			if ($this->course->curriculas) {
				foreach ($this->course->curriculas as $learningPlan) {
					if ($learningPlan->days_valid > 0 && $learningPlan->days_valid_type == LearningCoursepath::DAYS_VALID_TYPE_FIRST_COURSE_ACCESS) {
						$learningPlanUser = LearningCoursepathUser::model()->findByPk(array(
							'id_path' => (int)$learningPlan->id_path,
							'idUser' => $this->idUser
						));

						if ($learningPlanUser && (is_null($learningPlanUser->date_end_validity)
								|| $learningPlanUser->date_end_validity == '0000-00-00 00:00:00'
								|| $learningPlanUser->date_end_validity == '')
						) {
							$courses = $learningPlan->getLPCoursesIds();
							$learningPlan->updateLPUser($courses, $this->idUser);
						}
					}
				}
			}
		}

		parent::afterSave();
	}

	public function renderEnrollmentActions() {
		$enrollmentActions = Yii::app()->getController()->renderPartial('_enrollmentActions', array(
			'model' => $this,
			'activeFrom' => ((!$this->date_begin_validity) ? '-' : $this->date_begin_validity),
			'activeTo' => ((!$this->date_expire_validity) ? '-' : $this->date_expire_validity),
		), TRUE);

		$content = CHtml::link('', 'javascript:void(0);', array(
			'id' => 'popover-'.uniqid(),
			'class' => 'popover-action popover-trigger select-columns',
			'data-toggle' => 'popover',
			'data-html' => TRUE,
			'data-content' => $enrollmentActions,
		));
		return $content;
	}

	public function renderTimeInCourse() {
		$content = LearningTracksession::model()->getUserTotalTime($this->idUser, $this->idCourse, 'G\h i\m s\s');
		return $content;
	}

	/**
	 * Calculates the time spent by all users in a given course (this->idCourse)
	 *
	 * @return string
	 */
	public function renderTimeInCourseOfAllUsers() {
		$content = LearningTracksession::model()->getUserTotalTime(NULL, $this->idCourse, 'G\h i\m s\s');
		return $content;
	}


	public function renderProgress() {
		$content = LearningCommontrack::model()->getUserCourseProgress($this->idCourse);
		return $content .'%';
	}

	public function getUserCourseSummary($userId, $userCourseCount) {
		$learningCourseuserAlias = self::model()->getTableAlias();
		$criteria = new CDbCriteria();
		$criteria->select = array(
			$learningCourseuserAlias.'.status',
			'COUNT(*) as count'
		);
		$criteria->addCondition('idUser = :userid');
		$criteria->with = 'course';
		$criteria->params = array(
			'userid' => $userId,
		);
		$criteria->group = $learningCourseuserAlias.'.status';

		$courseSummaryList = $this->getSummary($criteria, $userCourseCount);

		return $courseSummaryList;
	}


	public function getCourseUserSummary($courseId, $userCourseCount, $checkPowerUser=false, $idPowerUser=false) {

		if ($checkPowerUser) {
			if ( !$idPowerUser && Yii::app()->user->getIsAdmin()) {
					$idPowerUser = Yii::app()->user->id;
			}
		}

		$criteria = new CDbCriteria();
		$criteria->select = array(
			'status',
			'COUNT(*) count'
		);

		if ((int) $idPowerUser > 0) {
			$criteria->join = "INNER JOIN core_user_pu cup on ( (t.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )";
			$criteria->params[':idPowerUser'] = $idPowerUser;
			$criteria->together = true;
		}


		$criteria->addCondition('idCourse = :courseid');
		$criteria->params['courseid'] = $courseId;


		$criteria->group = 'status';
		$courseSummaryList = $this->getSummary($criteria, $userCourseCount);
		return $courseSummaryList;
	}

	private function getSummary(CDbCriteria $courseUsersCriteria, $totalCount) {
		$allowedStatuses = array(
			self::$COURSE_USER_SUBSCRIBED,
			self::$COURSE_USER_BEGIN,
			self::$COURSE_USER_END,
		);
		$allStatuses = $this->getStatusesArray();
		$courseUsersSummaryList = array();
		foreach ($allowedStatuses as $key => $status) {
			switch ($status) {
				case self::$COURSE_USER_SUBSCRIBED: $statusTitle = Yii::t('report', '_MUSTBEGIN'); break;
				default: $statusTitle = (isset($allStatuses[$status]) ? $allStatuses[$status] : ''); break;
			}
			$courseUsersSummaryList[$status] = array(
				'title' => $statusTitle,//$allStatuses[$status],
				'count' => 0,
				'percent' => 0,
			);
		}
		$courseUsersSummary = LearningCourseuser::model()->findAll($courseUsersCriteria);
		if (!empty($courseUsersSummary)) {
			foreach ($courseUsersSummary as $status) {
				if (in_array($status->status, $allowedStatuses)) {
					$courseUsersSummaryList[$status->status]['count'] = $status->count;
					$courseUsersSummaryList[$status->status]['percent'] = 0;
					if ($totalCount > 0) {
						$courseUsersSummaryList[$status->status]['percent'] = round($status->count * 100 / $totalCount);
					}
				}
			}
		}
		return $courseUsersSummaryList;
	}

	public function renderReportTotalInCourse() {
		$content = $this->countByAttributes(array('idCourse' => $this->idCourse));
		return (!empty($content) ? $content : '-');
	}

	public function renderReportTotalNotStarted() {
		$content = $this->countByAttributes(array('idCourse' => $this->idCourse, 'status' => LearningCourseuser::$COURSE_USER_SUBSCRIBED));
		return (!empty($content) ? $content : '-');
	}

	public function renderReportTotalInProgress() {
		$content = $this->countByAttributes(array('idCourse' => $this->idCourse, 'status' => LearningCourseuser::$COURSE_USER_BEGIN));
		return (!empty($content) ? $content : '-');
	}

	public function renderReportTotalCompleted() {
		$content = $this->countByAttributes(array('idCourse' => $this->idCourse, 'status' => LearningCourseuser::$COURSE_USER_END));
		return (!empty($content) ? $content : '-');
	}


	/**
	 * Return number of sessions for given User/Course
	 * @return number|string
	 */
	public function renderNumberOfSessions() {
		$sql = "SELECT COUNT(*) AS CC FROM " . LearningTracksession::model()->tableName()  ." WHERE idCourse=" . $this->idCourse . " AND idUser=" . $this->idUser;
		$count = Yii::app()->db->createCommand($sql)->queryScalar();
		return $count > 0 ? $count : '-';
	}



	/**
	 * Bulk subscribe list of users to a list of courses
	 *
	 * @param array $users
	 * @param array $courses
	 */
	public static function subscribeUsersToCourses($users, $courses, $level = false, $learningPlanContext=false, $fields = array()) {

		if (false === $level) {
			$level = self::$USER_SUBSCR_LEVEL_STUDENT;
		}

		if(CoreUserPU::isPUAndSeatManager()){
			// The current user is a Power User and he is only
			// allowed to enroll users to courses through "seats"
			// Do a check if the PU has enough seats to distribute in
			// the course (enough to cover the selected users)
			$coursesWithInsufficientSeats = CoreUserPuCourse::getMassCoursesWithInsufficientSeats($courses, count($users));

			if(!empty($coursesWithInsufficientSeats)){
				throw new CException('Not enough seats available for at least one course');
			}
		}

		foreach ($courses as $idCourse) {

			$subscribedCountInCourse = 0;

			foreach ($users as $idUser) {
				$model = new self();

				$subscribed = $model->subscribeUser($idUser, Yii::app()->user->id, $idCourse, 0, $level, false, false, false, $learningPlanContext, false, false, $fields);

				if($subscribed && LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()){
					$subscribedCountInCourse++;

					Yii::app()->event->raise('onPuSeatConsume', new DEvent(self, array(
						'idUser'=>$idUser,
						'idCourse'=>$idCourse,
						'idPowerUser'=>Yii::app()->user->id,
					)));
				}
			}

			if(LearningCourse::isCourseSelling($idCourse) && CoreUserPU::isPUAndSeatManager()){
				// Reduce seats inside course with number of enrolled users
				CoreUserPU::modifyAvailableSeats($idCourse, -$subscribedCountInCourse);
			}
		}
	}



	/**
	 * Bulk UN-subscribe list of users to a list of courses
	 *
	 * @param array $users
	 * @param array $courses
	 */
	public static function unSubscribeUsersFromCourses($users, $courses) {
		foreach ($courses as $idCourse) {
			foreach ($users as $idUser) {
				$model = new self();
				$model->unsubscribeUser($idUser, $idCourse);
			}
		}
	}


	/**
	 * Get the score from the LO marked as "final" bookmark
	 * or the last LO if "final bookmark" is not present.
	 * Note: "Last" is determined by its iLeft attribute, not by idOrg!
	 *
	 * This method will just return the 'learning_courseuser.score_given'
	 * value, if present. If not, a dynamic calculation will be done
	 * by digging inside the individual LO tracking tables and 'score_given'
	 * will be updated with the calculated score for future calls
	 *
	 * @param         $courseId
	 * @param         $userId
	 * @param boolean $associativeArray If TRUE (rarely used), return result as an associative array; FALSE: returns a single float
	 * @param bool    $check If true, forces recalculation of the score dynamicall (should not be used in grids or other mass score retrievals)
	 *
	 * @return array|string (depending on the passed $associativeArray parameter) - 1) an array with keys: hasScore, score, maxScore or  2) the score as float
	 *
	 * @see LearningCourseuser::recalculateLastScoreByUserAndCourse()
	 */
	public static function getLastScoreByUserAndCourse($courseId, $userId, $associativeArray=FALSE, $check=false)
	{

		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $userId,
			'idCourse'  => $courseId
		));

		$res = array(
			'hasScore' => false,
			'score' => 0,
			'maxScore' => 100,
		);

		if($enrollment){
			if(empty($enrollment->score_given)) {
				// No 'score_given' value. Lookup the value by digging deeper
				// and set the 'score_given' if user's score is found
				// (to speed up future lookups)
				$res = self::recalculateLastScoreByUserAndCourse( $courseId, $userId, true, $check);
			}else{
				if($associativeArray){
					// score_given value set, but the caller requested the full array
					// including the score_max, which can only be retrieved by lookup
					// in internal tables (depending on the LO type, different tables
					// are involved)
					$res = self::recalculateLastScoreByUserAndCourse( $courseId, $userId, $associativeArray, $check);
				}else{
					// Score can be retrieved directly from the 'score_given'
					// column. This is all that was requested by the method
					// anyway (calling with $associativeArray=false)
					$res = array(
						'hasScore' => true,
						'score' => $enrollment->score_given,
						'maxScore' => 100,
					);
				}
			}
		}

		if ($associativeArray) {
			return $res;
		}else{
			return $res['score'];
		}
	}

	public static function getLastInitialScoreByUserAndCourse($courseId, $userId, $associativeArray = FALSE, $check = false)
	{

		$enrollment = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $userId,
			'idCourse'  => $courseId
		));

		$res = null;

		if ($enrollment) {
			if (empty($enrollment->initial_score_given)) {
				$res = self::recalculateInitialScoreGiven($userId, $courseId);
			} else {
				$res = $enrollment->initial_score_given;
			}
		}

		return $res;
	}

	public static function recalculateInitialScoreGiven($userId, $courseId, $skip_certificate_generation = false)
	{
		$courseModel = LearningCourse::model()->findByPk($courseId);
		$learningCourseUserModel = LearningCourseuser::model()->findByAttributes(array(
			'idUser' => $userId,
			'idCourse' => $courseId,
		));
		if ($courseModel->initial_score_mode == LearningCourse::INITIAL_SCORE_TYPE_KEY_OBJECT) {
			$idLo = $courseModel->initial_object;
			$object = LearningOrganization::model()->findByPk($idLo);
			$res = LearningOrganization::getLastScoreOfUserInLO($idLo, $userId);
			$learningCourseUserModel->initial_score_given = $res;
			$learningCourseUserModel->skip_certificate_generation = $skip_certificate_generation;
			$learningCourseUserModel->save();
			return $res;
		}

		return null;
	}
	/**
	 * Dynamically get the result of the last score for a user/course pair
	 * based on the course's final score calculation setting:
	 * - STANDARD - the final object marker or the last object's score
	 * - SPECIFIC OBJECT - take the score from that object only
	 * - AVERAGE OF ALL OBJECTS - take the average of all scores by the user among all object
	 * - SUM OF ALL OBJECTS - sum the score of the user among all objects in course
	 *
	 * Note that calling this method the learning_courseuser.score_given will automatically be updated
	 * In most cases you would need to call LearningCourseuser::getLastScoreByUserAndCourse()
	 * which is a wrapper around this method and first relies on the 'score_given' value,
	 * then tries to calculate dynamically
	 *
	 * @param      $courseId
	 * @param      $userId
	 * @param bool $returnAsAssociativeArray
	 *
	 * @return array|int|string
	 * @throws CException
	 * @see LearningCourseuser::getLastScoreByUserAndCourse()
	 */
	public static function recalculateLastScoreByUserAndCourse($courseId, $userId, $returnAsAssociativeArray=FALSE, $check=false, $skip_certificate_generation = false)
	{
		$enrollmentModel = LearningCourseuser::model()->findByAttributes( array(
			'idUser'   => $userId,
			'idCourse' => $courseId,
		) );

        $isScoreValueForced = (boolean) $enrollmentModel->forced_score_given;
        // If the score value is manually set (by a super-admin/power-user) and
        // the return type is scalar -> return the score without recalculating it
        if ($isScoreValueForced === true && $returnAsAssociativeArray === false) {
            return $enrollmentModel->score_given;
        }

		// Default values in case of "associative array" output
		$hasScore = FALSE;
		$maxScore = 100;
		$res      = 0;

		$finalScoreCalculationType = LearningCourse::getFinalScoreMode( $courseId );

		switch ( $finalScoreCalculationType ) {
			case LearningCourse::FINAL_SCORE_TYPE_KEY_OBJECT:
				$courseModel = LearningCourse::model()->findByPk($courseId);
				if($courseModel){
					if(!$courseModel->final_object) {
						Yii::log("Final score calculation for course {$courseId} set to specific LO but the LO itself is not set", CLogger::LEVEL_WARNING);
						$res = 0;
					} else {
						$keyObjectScore = LearningOrganization::getLastScoreOfUserInLO($courseModel->final_object, $userId, true);
						if (!empty($keyObjectScore)) {
							$res = $keyObjectScore['score'];
							$hasScore = $keyObjectScore['hasScore'];
							$maxScore = $keyObjectScore['maxScore'];
						}
					}
				}
				break;
			case LearningCourse::FINAL_SCORE_TYPE_SUM_ALL_COMPLETED:
				$courseObjectsWithPossibleScore = Yii::app()->getDb()->createCommand()
					->select('idOrg')
					->from(LearningOrganization::model()->tableName())
					->where('idCourse=:idCourse', array(':idCourse'=>$courseId))
					->andWhere(array('IN', 'objectType', array(
						// Those are the only LO types that can have a score
						LearningOrganization::OBJECT_TYPE_AICC,
						LearningOrganization::OBJECT_TYPE_SCORMORG,
						LearningOrganization::OBJECT_TYPE_TEST,
						LearningOrganization::OBJECT_TYPE_TINCAN,
						LearningOrganization::OBJECT_TYPE_ELUCIDAT,
						LearningOrganization::OBJECT_TYPE_DELIVERABLE,
						LearningOrganization::OBJECT_TYPE_LTI,
					)))
					->queryColumn();

				// Calculate the user's score for each of the LOs inside course
				if(count($courseObjectsWithPossibleScore) > 0){
					$maxScore = 0;
					foreach($courseObjectsWithPossibleScore as $singleObject){
						$userScoreForLO = LearningOrganization::getLastScoreOfUserInLO($singleObject, $userId, true);
						$res += $userScoreForLO['score'];

						if(isset($userScoreForLO['maxScore']) && $userScoreForLO['maxScore']){
							$maxScore += $userScoreForLO['maxScore'];
						}
					}
				}
				break;
			case LearningCourse::FINAL_SCORE_TYPE_AVG_ALL_COMPLETED:
				$courseObjectsWithPossibleScore = Yii::app()->getDb()->createCommand()
				                     ->select('idOrg, objectType')
				                     ->from(LearningOrganization::model()->tableName())
				                     ->where('idCourse=:idCourse', array(':idCourse'=>$courseId))
				                     ->andWhere(array('IN', 'objectType', array(
					                     // Those are the only LO types that can have a score
					                     LearningOrganization::OBJECT_TYPE_AICC,
					                     LearningOrganization::OBJECT_TYPE_SCORMORG,
					                     LearningOrganization::OBJECT_TYPE_TEST,
					                     LearningOrganization::OBJECT_TYPE_TINCAN,
					                     LearningOrganization::OBJECT_TYPE_ELUCIDAT,
					                     LearningOrganization::OBJECT_TYPE_DELIVERABLE,
										 LearningOrganization::OBJECT_TYPE_LTI,
				                     )))
				                     ->queryAll();

				$objectsCount = 0;



				$tmp = 0;
				$tmpMax = 0;
				foreach($courseObjectsWithPossibleScore as $singleObject){
					$userScoreForLO = LearningOrganization::getLastScoreOfUserInLO($singleObject['idOrg'], $userId, true);

					if($singleObject['objectType']==LearningOrganization::OBJECT_TYPE_SCORMORG){
						if($userScoreForLO['hasScore']) {
							$tmp += $userScoreForLO['score'];
							$objectsCount ++;
							if(isset($userScoreForLO['maxScore']) && $userScoreForLO['maxScore']){
								$tmpMax += $userScoreForLO['maxScore'];
							}
						}
					}else{
						$tmp += $userScoreForLO['score'];
						$objectsCount++;
						if(isset($userScoreForLO['maxScore']) && $userScoreForLO['maxScore']){
							$tmpMax += $userScoreForLO['maxScore'];
						}
					}

				}

				if($objectsCount > 0) {
					$maxScore = ( $tmpMax / $objectsCount ); // average
					$res = ( $tmp / $objectsCount ); // average
				}

				break;
			case LearningCourse::FINAL_SCORE_TYPE_STANDARD:
			default:
				// Get the score of the LO marked as "End object Bookmark"
				// or the last one (highest iLeft column value)

				$finalBookmarkOrLastLO = LearningCourse::getFinalObjectforCourse( $courseId );

				if ( ! empty( $finalBookmarkOrLastLO ) ) {
					$res = LearningOrganization::getLastScoreOfUserInLO($finalBookmarkOrLastLO['idOrg'], $userId);
				}

				if($check && !empty( $finalBookmarkOrLastLO)){
					$lastScore = LearningOrganization::getLastScoreOfUserInLO($finalBookmarkOrLastLO['idOrg'], $userId, true);
					if(isset($lastScore['maxScore']) && isset($lastScore['hasScore']) && $lastScore['hasScore'] && $lastScore['maxScore']){
						$maxScore = $lastScore['maxScore'];
					}
				}

				break;
		}

		$course = LearningCourse::model()->findByPk($courseId);

		if($course)
		{
			//final score is equal to the last manually completed session score
			if($course->course_type === LearningCourse::TYPE_CLASSROOM)
			{
				$score = Yii::app()->db->createCommand()
					->select('ltcu.evaluation_score, ltc.score_base')
					->from(LtCourseuserSession::model()->tableName() . ' AS ltcu')
					->join(LtCourseSession::model()->tableName() . ' AS ltc', 'ltc.id_session = ltcu.id_session')
					->where('ltc.course_id = :course_id', array(':course_id' => $courseId))
					->andWhere('ltcu.id_user = :id_user', array(':id_user' => $userId))
					->andWhere('ltcu.evaluation_score IS NOT NULL')
					->andWhere('ltc.evaluation_type = :evaluation_type', array(':evaluation_type' => LtCourseSession::EVALUATION_TYPE_SCORE))
					->order('ltcu.evaluation_date DESC')
					->limit(1)
					->queryRow();

				if(is_array($score))
				{
					$res = $score['evaluation_score'];
					$maxScore = $score['score_base'];
				}
			}
			else if($course->course_type === LearningCourse::TYPE_WEBINAR)
			{
				$score = Yii::app()->db->createCommand()
					->select('wsu.evaluation_score, ws.evaluation_type, ws.score_base')
					->from(WebinarSessionUser::model()->tableName() . ' AS wsu')
					->join(WebinarSession::model()->tableName() . ' AS ws', 'ws.id_session = wsu.id_session')
					->where('ws.course_id = :course_id', array(':course_id' => $courseId))
					->andWhere('wsu.id_user = :id_user', array(':id_user' => $userId))
					->andWhere('wsu.evaluation_score IS NOT NULL')
					->andWhere('ws.evaluation_type != :evaluation_type', array(':evaluation_type' => WebinarSession::EVAL_TYPE_ONLINE_TEST))
					->order('wsu.evaluation_date DESC')
					->limit(1)
					->queryRow();

				if(is_array($score))
				{
					$res = $score['evaluation_score'];
					$maxScore = $score['score_base'];
				}
			}
		}

		if ( $enrollmentModel && ! empty( $res ) ) {
			$enrollmentModel->skip_certificate_generation = $skip_certificate_generation;
			// Update 'score_given'
            // ... only if the score value was not manually set (by a super-admin/power-user)
            if ($isScoreValueForced === false) {
                $enrollmentModel->score_given = $res;
            }

			if ($check === false) {
                if ($enrollmentModel->save()) {
                    Yii::log( 'Recalculated score ' . $res . ' for course ' . $courseId . ' using final score method ' . $finalScoreCalculationType, CLogger::LEVEL_INFO );
                }
			} else if ($isScoreValueForced === false) { // not updating `score_given` if the value was manually set
                if ($enrollmentModel->saveAttributes(array('score_given'=>$res)) ) {
                    Yii::log( 'Recalculated score ' . $res . ' for course ' . $courseId . ' using final score method ' . $finalScoreCalculationType, CLogger::LEVEL_INFO );
                }
			}
		}

		if($res > 0) $hasScore = true;

        // If the score was manually set - return it, instead of the recalculated value
        if ($isScoreValueForced === false) {
            $res = $enrollmentModel->score_given;
        }

		// Caller may need NOT a string, but numerical information about scores; return an array of data
		if ( $returnAsAssociativeArray ) {
			$response = array(
				'hasScore' => $hasScore,
				'score'    => $res,
				'maxScore' => $maxScore,
			);

			return $response;
		}else{
			// This would always a string like "30.5" or "55"
			return $res;
		}
	}

	/**
	 * return array with format
	 *
	 * array(
	 *    id_user => array(
	 *        id_course => score
	 *            ...
	 *        )
	 *  ...
	 * )
	 *
	 * @param $courseIds
	 * @return array
	 */
	public static function getLastScores($courseIds) {

		// get score_given
		$results = Yii::app()->db->createCommand()
			->select('idCourse, idUser, score_given')
			->from(LearningCourseuser::model()->tableName())
			->where('score_given IS NOT NULL AND idCourse IN ('.implode(',', $courseIds).')')
			->queryAll();
		foreach($results as $result) {
			$scores[$result['idUser']][$result['idCourse']] = number_format(LearningCourseuser::model()->getLastScoreByUserAndCourse($result['idCourse'], $result['idUser']), 2);
		}

		return $scores;
	}



	/**
	 * Return a list of instructor(s) for this course
	 *
	 * @return array list of CoreUser ARs of instructors for this record's user
	 */
	public function findInstructors() {

		//check if we are dealing with a classroom course or not
		$course = $this->getCourse();
		if (!$course) { throw new CException('Invalid course'); }
		if ($course->course_type == LearningCourse::TYPE_CLASSROOM) {

			//in case of classroom cours, we have to find instructor(s) only for users session(s)

			//list of session from which search for instructor
			$checkList = array();

			//check if the user is enrolled to other sessions of the course
			$sessions = Yii::app()->db->createCommand()
				->select("cus.id_session")
				->from(LtCourseuserSession::model()->tableName()." cus")
				->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
				->where("cs.course_id = :id_course", array(':id_course' => $this->idCourse))
				->andWhere("cus.id_user = :id_user", array(':id_user' => $this->idUser))
				->queryAll();
			if (!empty($sessions)) {
				foreach ($sessions as $session) { $checkList[] = $session['id_session']; }
			}

			//now retrieve IDSTs of the instructors
			$instructorsIdsts = array();
			$idsts = Yii::app()->db->createCommand()
				->select("cus.id_user")
				->from(LtCourseuserSession::model()->tableName()." cus")
				->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
				->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
				->where("cs.course_id = :id_course", array(':id_course' => $this->idCourse))
				->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
				->andWhere("cus.id_user <> :id_user", array(':id_user' => $this->idUser))
				->queryAll();
			if (!empty($idsts)) {
				foreach ($idsts as $idst) {
					$instructorsIdsts[] = $idst['id_user'];
				}
			}

			//check if we have any instructor to return
			if (empty($instructorsIdsts)) { return array(); }

			//find user records from instructors idsts
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $instructorsIdsts);
			$criteria->order = "firstname ASC, lastname ASC";

			//return result directly
			return CoreUser::model()->findAll($criteria);
		}

		//==========
		//non-classroom courses use generic search for instructor(s)

		//find all instrctors idsts
		$instructorsIdsts = array();
		$idsts = Yii::app()->db->createCommand()
			->select("cu.idUser")
			->from(self::model()->tableName()." cu")
			->where("cu.idCourse = :id_course", array(':id_course' => $this->idCourse))
			->andWhere("cu.level = :level", array(':level' => self::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			->andWhere("cu.idUser <> :id_user", array(':id_user' => $this->idUser))
			->queryAll();
		if (!empty($idsts)) {
			foreach ($idsts as $idst) {
				$instructorsIdsts[] = $idst['idUser'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		//find user records from instructors idsts
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst', $instructorsIdsts);
		$criteria->order = "firstname ASC, lastname ASC";
		return CoreUser::model()->findAll($criteria);
	}



	/**
	 * Generic method for instructor list retrieving
	 *
	 * @param int $idCourse
	 * @return array
	 * @throws CException
	 */
	public static function findCourseInstructors($idCourse) {

		//retrieve IDSTs of the instructors
		$instructorsIdsts = array();
		$idsts = Yii::app()->db->createCommand()
			->select("cu.idUser")
			->from(self::model()->tableName()." cu")
			->where("cu.idCourse = :id_course", array(':id_course' => $idCourse))
			->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			->queryAll();
		if (!empty($idsts)) {
			foreach ($idsts as $idst) {
				$instructorsIdsts[] = $idst['idUser'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		//find user records from instructors idsts
		$criteria = new CDbCriteria();
		$criteria->addInCondition('idst', $instructorsIdsts);
		$criteria->order = "firstname ASC, lastname ASC";
		return CoreUser::model()->findAll($criteria);
	}



	/**
	 * Subscribe a give user to course(s), applying the "old" codes approach.
	 * Provides cross-acceptance of codes between Old Codes and New  Subscription codes plugin
	 *
	 * @param string $code
	 * @param integer $idUser
	 * @param boolean $trySubscriptionCodes Should we try to use the NEW Subscription Codes Plugin too? (using the same code)
	 * @return string
	 */
	public static function subscribeUserByOldCode($code, $idUser=false, $trySubscriptionCodes=false) {

		$originalCode = $code;
		$idUser = $idUser ? $idUser :  Yii::app()->user->id;

		$registration_code_type = Settings::get('registration_code_type', 'unknown');

		if ($registration_code_type == 'tree_course' && $trySubscriptionCodes)
			$code = substr($code, 10, 10);

		$subscribedAutoRegCourses = LearningCourse::getCoursesHavingAutoregCode($code, $idUser);

		$errorMessage = "";
		// No course found having the code; is that the end?
		if ($subscribedAutoRegCourses === false) {
			$errorMessage = Yii::t('course_autoregistration', 'The code you typed is invalid or unknown. Please try again!');
		}
		// Course or courses are found and SOME are already subscribed; we consider this 'code already used'
		else if (isset($subscribedAutoRegCourses['subscribed'])) {
			$errorMessage = Yii::t('course_autoregistration', '_CODE_ALREDY_USED');
		}

		// Oook, NEW FEATURE Here (IF AND ONLY IF above "code" checks failed)
		// If SubscriptionCodes plugin is activated... search the entered code in the plugin data
		$handled = false;
		if ($errorMessage && $trySubscriptionCodes && PluginManager::isPluginActive('SubscriptionCodesApp')) {
			// Call the Subscription Codes enrollment, but avoid circular calls (false et the end)
			$pluginResult = SubscriptionCodesManager::enrollUser($originalCode, $idUser, false);
			if (!$pluginResult['success']) {
				$errorMessage = $pluginResult['message'];  // PLUGIN error messages take higher priority!!!
			}
			else {
				$errorMessage = '';
				$handled = true;
			}
		}

		// If not yet handled, by any chance, by the above SubscriptionCodes plugin, or somewhere in the code earlier
		if (!$handled && !$errorMessage) {
			// Subscribe the user to the FIRST course of the array
			$courseToSubscribe = $subscribedAutoRegCourses['notsubscribed'][0];

			$courseUserModel = new self();
			$courseUserModel->scenario = 'self-subscribe';

			// We do not make any checks if user can subscribe this course, because
			// by definition Auto-registration overrides all restrictions.
			$ok = $courseUserModel->subscribeUser(
				$idUser, Yii::app()->user->id, $courseToSubscribe->idCourse, 0, self::$USER_SUBSCR_LEVEL_STUDENT
			);

			if (!$ok) {
				$errorMessage = Yii::t("standard", '_OPERATION_FAILURE');
			}
		}

		return $errorMessage;

	}


	/**
	 * Returns the score for each session
	 * @return array
	 */
	public function getSessionScores() {
		$idCourse = $this->idCourse;
		$idUser = $this->idUser;

		$criteria = new CDbCriteria();
		$criteria->with = array(
			'ltCourseSession' => array(
				'alias' => 'session',
				'joinType' => 'INNER JOIN',
				'condition' => 'session.course_id = :course_id',
				'params' => array(
					':course_id' => $idCourse
				)
			)
		);

		$criteria->addCondition('t.id_user = :id_user');
		$criteria->params[':id_user'] = $idUser;

		$rows = LtCourseuserSession::model()->findAll($criteria);

		$data = array();
		if (!empty($rows)) {
			foreach ($rows as $row) {
				$data[$row->id_session] = array(
					'evaluation_score' => $row->evaluation_score,
					'score_base' => $row->session->score_base
				);
			}
		}

		return $data;
	}


	/**
	 * Reset subscription tracking data
	 *
	 * @param integer $idCourse
	 * @param integer $idUser
	 */
	public static function reset($idCourse, $idUser) {
		/* @var $model LearningCourseuser */

		$model = self::model()->findByAttributes(array('idCourse' => $idCourse, 'idUser' => $idUser));

		if (!$model)
			return false;

		// Set Audit Context
		$model->auditContext = 'reset_subscription';

		// Before restting all the subscriptions data, lets save SCORE (if not already having a value there)
		if (!$model->score_given) {
			$model->score_given = self::getLastScoreByUserAndCourse($model->idCourse, $model->idUser);
			$model->setAttributes($model->attributes);
		}


		if($model->course->course_type == LearningCourse::TYPE_CLASSROOM){
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'ltCourseSession' => array(
					'alias' => 'cs',
					'condition' => 'cs.course_id = :course_id'
				)
			);

			$criteria->condition = 'id_user = :idUser';
			$criteria->params = array(':idUser'=> $idUser, ':course_id' => $idCourse);
			$sessionUserModels = LtCourseuserSession::model()->findAll($criteria);
			if($sessionUserModels && is_array($sessionUserModels)){
				foreach($sessionUserModels as $sessionUserModel){
					$sessionUserModel->scenario = 'selfUnEnroll';
					if ($sessionUserModel->delete()){
						Yii::log("User ($idUser) was removed from ILT session ($sessionUserModel->id_session) due to course reset.", 'info');
					}
				}
			}

		}

		if($model->course->course_type == LearningCourse::TYPE_WEBINAR){
			$criteria = new CDbCriteria();
			$criteria->with = array(
				'sessionModel' => array(
					'alias' => 'ws',
					'condition' => 'ws.course_id = :course_id'
				)
			);
			$criteria->condition = 'id_user = :idUser';
			$criteria->params = array(':idUser'=> $idUser, ':course_id' => $idCourse);
			$sessionUserModels = WebinarSessionUser::model()->findAll($criteria);
			if($sessionUserModels && is_array($sessionUserModels)){
				foreach($sessionUserModels as $sessionUserModel){
					$sessionUserModel->scenario = 'selfUnEnroll';
					if ($sessionUserModel->delete()){
						Yii::log("User ($idUser) was removed from webinar session ($sessionUserModel->id_session) due to course reset.", 'info');
					}
				}
			}

		}


		// Tracking data
		$model->resetTrackingData();

		// Reset subscription itself
		$model->date_first_access 	= null;
		$model->date_last_access	= null;
		$model->date_complete		= null;
		$model->status				= self::$COURSE_USER_SUBSCRIBED;
		$model->score_given			= null;
		$model->date_begin_validity	= null;
		$model->date_expire_validity= null;
		$model->save(false);

	}


	/**
	 * Reset/Clear all tracking data related to this subscription
	 */
	protected function resetTrackingData() {

		$idUser 	= (int) $this->idUser;
		$idCourse 	= (int) $this->idCourse;

		$params = array();
		$command = Yii::app()->db->createCommand();
		$command->select('lo.idOrg AS idOrg, lo.idResource AS idResource');
		$command->from('learning_organization lo');
		$command->andWhere('lo.idCourse=:idCourse');
		$params[':idCourse'] = $idCourse;

		$reader = $command->query($params);
		$los = array();
		$resources = array();
		foreach ($reader as $row) {
			$los[] 			= $row['idOrg'];
			$resources[] 	= $row['idResource'];
		}
		if (empty($los)) {
			return;
		}

		// List of Course learning objects, in a list format, e.g. "1,2,4,7,8"
		$losList 		= implode(',', $los);
		$resourcesList 	= implode(',', $resources);

		// SCORM
		$sql = "
			DELETE FROM learning_scorm_items_track 	WHERE (idUser=$idUser) AND (idReference IN ($losList));
			DELETE FROM learning_scorm_tracking 	WHERE (idUser=$idUser) AND (idReference IN ($losList));
		";
		Yii::app()->db->createCommand($sql)->execute();


		// TinCan
		// @TODO Clean Statements?
		$sql = "DELETE FROM learning_tc_track 	WHERE (idUser=$idUser) AND (idReference IN ($losList));";
		Yii::app()->db->createCommand($sql)->execute();


		//VIDEO
		$sql = "DELETE FROM learning_video_track 	WHERE (idUser=$idUser) AND (idReference IN ($losList));";
		Yii::app()->db->createCommand($sql)->execute();


		// FILE, HTML
		$sql = "DELETE FROM learning_materials_track 	WHERE (idUser=$idUser) AND (idReference IN ($losList));";
		if ($sql) Yii::app()->db->createCommand($sql)->execute();


		// SURVEY
		$sql = "SELECT id_track FROM learning_polltrack WHERE (id_user=$idUser) AND (id_reference IN ($losList));";
		$reader = Yii::app()->db->createCommand($sql)->query();
		$sql = "";
		foreach ($reader as $row) {
			$idTrack = (int) $row['id_track'];
			$sql .= "DELETE FROM learning_polltrack_answer 	WHERE id_track=$idTrack;";

			$sql .= "DELETE FROM learning_polltrack 		WHERE id_track=$idTrack;";
		}
		if ($sql) Yii::app()->db->createCommand($sql)->execute();



		// TEST
		$sql = "SELECT idTrack FROM learning_testtrack WHERE (idUser=$idUser) AND (idReference IN ($losList));";
		$reader = Yii::app()->db->createCommand($sql)->query();
		$sql = "";
		foreach ($reader as $row) {
			$idTrack = (int) $row['idTrack'];
			$sql .= "DELETE FROM learning_testtrack_answer 	WHERE idTrack=$idTrack;";
			$sql .= "DELETE FROM learning_testtrack_page 	WHERE idTrack=$idTrack;";
			$sql .= "DELETE FROM learning_testtrack_quest 	WHERE idTrack=$idTrack;";
			$sql .= "DELETE FROM learning_testtrack_times 	WHERE idTrack=$idTrack;";

			$sql .= "DELETE FROM learning_testtrack 		WHERE idTrack=$idTrack;";

		}
		if ($sql) Yii::app()->db->createCommand($sql)->execute();

		// DELIVERABLES
		// learning_deliverable_object: id_user / idObject -> learning_organization::idResource
		$sql = "DELETE FROM learning_deliverable_object WHERE (id_user=$idUser) AND (id_deliverable IN ($resourcesList));";
		Yii::app()->db->createCommand($sql)->execute();


		// COURSE SESSIONS TRACKING
		// learning_tracksession: idCourse / idUser
		$sql = "DELETE FROM learning_tracksession WHERE (idUser=$idUser) AND (idCourse=$idCourse);";
		Yii::app()->db->createCommand($sql)->execute();


		// COMMON TRACKING
		$sql = "DELETE FROM learning_commontrack WHERE (idUser=$idUser) AND (idReference IN ($losList));";
		Yii::app()->db->createCommand($sql)->execute();
	}

	/**
	 * This function will update incorrect enrollment level values in learning_courseuser table
	 */
	public static function fixEnrollmentsLevel() {

		$minMax = self::getMinMaxLevelId();

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$query = "UPDATE ".$db->quoteTableName(self::model()->tableName())." "
			." SET ".$db->quoteColumnName('level')." = :std_level "
			." WHERE ".$db->quoteColumnName('level')." < :min_level "
			." OR ".$db->quoteColumnName('level')." > :max_level";
		$db->createCommand($query)->execute(array(
			':std_level' => self::$USER_SUBSCR_LEVEL_STUDENT,
			':min_level' => $minMax['min'],
			':max_level' => $minMax['max']
		));
	}

	/**
	 * Return enrolled users by course
	 * @param $idCourse
	 * @return mixed
	 */
	public static function getEnrolledByCourse($idCourse)
	{
		$rows = Yii::app()->getDb()->createCommand()
			->selectDistinct('idUser')
			->from(LearningCourseuser::model()->tableName())
			->where('idCourse = :idCourse', array(':idCourse'=>$idCourse))
			->queryColumn();
		return $rows;
	}


	public static function isUserCoach($idUser, $idCourse) {
		$rows = Yii::app()->getDb()->createCommand()
			->selectDistinct('idUser')
			->from(LearningCourseuser::model()->tableName())
			->where('idCourse = :idCourse', array(':idCourse'=>$idCourse))
			->andWhere('level = :level', array(':level'=>LearningCourseuser::$USER_SUBSCR_LEVEL_COACH))
			->andWhere('idUser = :idUser', array(':idUser'=>$idUser))
			->queryColumn();

		return (count($rows) > 0) ? true : false;
	}

	public static function getCourseCoachUsers($idCourse, $idCoach) {
		$rows = Yii::app()->getDb()->createCommand()
			->selectDistinct('lccsu.idUser')
			->from(LearningCourseCoachingSessionUser::model()->tableName() . ' lccsu')
			->join(LearningCourseCoachingSession::model()->tableName() . ' lccs', 'lccs.idSession=lccsu.idSession')
			->where('lccs.idCourse = :idCourse', array(':idCourse'=>$idCourse))
			->andWhere('lccs.idCoach = :idCoach', array(':idCoach'=>$idCoach))
			->queryAll();

		$result = array();
		if (count($rows) > 0) {
			foreach ($rows as $row) {
				if (!empty($row[idUser])) {
					$result[$row[idUser]] = $row[idUser];
				}
			}
		}

		return $result;
	}
    /** Return the common date_begin/date_expire    _validity, if common for all given users IDs for the particular course
     * @param $courseId - id of the course
     * @param $userIds - array(or single id) of the users
     * @param bool $dateEnd - should the common date be date_begin_validity or date_expire_validity
     * @param bool $inLocalFormat - should the date be returned in local format
     * @param bool $returnArray - should the date be returned as a 'date' element of array along with the count of different dates found
     * @return mixed - false if no records or no common date; the specified date if common for all users
     * */
    public static function getCommonDate($courseId,$userIds,$dateEnd=false,$inLocalFormat=true, $returnArray = false){
        if(!is_array($userIds))
            $allIds = array($userIds);
        else
            $allIds = $userIds;
        $dateToFind = ($dateEnd)? 'date_expire_validity' : 'date_begin_validity' ;
        $rows = Yii::app()->getDb()->createCommand()
            ->select('lcu.'.$dateToFind)
            ->from(LearningCourseuser::model()->tableName().' lcu')
            ->where('idCourse = '.$courseId)
            ->andWhere('idUser IN ('.implode(',',$allIds).')')
            ->group($dateToFind)
            ->queryAll();

	    $countRows = count($rows);

        if ($countRows == 0 || $countRows > 1) {
	        if (!$returnArray) {
		        return false;
	        } else {
		        return array('date' => false, 'count' => $countRows);
	        }
        }
        elseif($countRows == 1 && $rows[0][$dateToFind] != '0000-00-00 00:00:00' && !empty($rows[0][$dateToFind])) {
            if($inLocalFormat){
	            if (!$returnArray) {
		            return Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($rows[0][$dateToFind]));
	            } else {
		            return array(
			            'date'  => Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($rows[0][$dateToFind])),
			            'count' => 1
		            );
	            }
            }
            else {
	            if (!$returnArray) {
		            return Yii::app()->localtime->stripTimeFromLocalDateTime($rows[0][$dateToFind]);
	            } else {
		            return array(
			            'date'  => Yii::app()->localtime->stripTimeFromLocalDateTime($rows[0][$dateToFind]),
			            'count' => 1
		            );
	            }
            }
        }

	    if (!$returnArray) {
		    return false;
	    } else {
		    return array('date' => false, 'count' => $countRows);
	    }
    }


	public static function  checkUsersCourseAccess($userCatalogs, $courseId, $isPowerUser = false, $activePlugin = true){
		$accessAllowed = false;
		$courses = LearningCatalogue::getCatalogCoursesListV2($userCatalogs);
		if($courses && is_array($courses) &&  in_array($courseId, $courses) && $activePlugin){
			$accessAllowed = true;
		}

		if($isPowerUser){
			$puCourses =  CoreUserPuCourse::model()->getList(Yii::app()->user->getId());
			if($puCourses && is_array($puCourses) && in_array($courseId, $puCourses)){
				$accessAllowed = true;
			}
		}

		return $accessAllowed;
	}


	/**
	 * Checks if the course is part of Learning Plan and the user is enrolled in!
	 *
	 * @param $idUser CoreUser idsts
	 * @return bool true | false
	 */
	public function isSubscribedtoLearningPlan($idUser = false){

		if(!$idUser){
			$idUser = $this->idUser;
		}

		$isSubscibed = false;

		if (PluginManager::isPluginActive('CurriculaApp')) {
			$lpAssigned = LearningCoursepathCourses::isCourseAssignedToLearningPaths($this->idCourse);
			if (!empty($lpAssigned)) {
				$lpEnrollments = LearningCoursepath::checkEnrollment($idUser, $this->idCourse, true);
				if (!empty($lpEnrollments)) {
					$isSubscibed = true;
				}
			}
		}

		return $isSubscibed;
	}

	/**
	 * Return available sorting fields and their labels for User - Elearning Course enrollment list
	 *
	 * @return array
	 */
	public static function getElearningEnrollmentsOrderList(){
		return array(
			'learningCourse.date_inscr'					=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'learningCourse.date_inscr DESC' 			=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			't.userid'									=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.userid DESC' 							=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			't.firstname'								=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.firstname DESC' 							=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			't.lastname'								=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			't.lastname DESC' 							=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}

	/**
	 * Return available sorting fields and their labels for User - ILT Course sessions enrollment list
	 *
	 * @return array
	 */
	public static function getSessionEnrollmentsOrderList(){
		return array(
			'cus.date_subscribed'						=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'cus.date_subscribed DESC' 					=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.userid'									=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.userid DESC' 							=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.firstname'								=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.firstname DESC' 							=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.lastname'								=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.lastname DESC' 							=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}

	/**
	 * Return available sorting fields and their labels for User - Webinar sessions enrollment list
	 *
	 * @return array
	 */
	public static function getWebinarEnrollmentsOrderList(){
		return array(
			'cus.date_subscribed'						=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'cus.date_subscribed DESC' 					=> Yii::t('standard', 'Enrollment date').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.userid'									=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.userid DESC' 							=> Yii::t('standard', '_USERNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.firstname'								=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.firstname DESC' 							=> Yii::t('standard', '_FIRSTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
			'u.lastname'								=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_ASC_TITLE').')',
			'u.lastname DESC' 							=> Yii::t('standard', '_LASTNAME').' ('.Yii::t('standard', '_ORD_DESC_TITLE').')',
		);
	}

    /**
     * Check is the array of $idCourse is completed from this $idUser
     * return false if array of courses is empty or $idUser is empty
     *
     * @param $idUser - single user id
     * @param array $idCourse - array of id's of courses
     * @return bool
     */
    public static function checkCompletedCoursesByUser($idUser, $idCourses = array()) {
        if ( !$idUser || empty( $idUser)) return false;
        if ( empty( $idCourses) || !isset( $idCourses) || ( isset( $idCourses) && !is_array( $idCourses)) ) return false;

        $query = Yii::app()->getDb()->createCommand()
            ->selectDistinct('*')
            ->from(LearningCourseuser::model()->tableName().' lcu')
            ->where('lcu.idUser = '. $idUser)
            ->andWhere('lcu.idCourse IN ('. implode(',', $idCourses). ')')
            ->andWhere('lcu.status = '. self::$COURSE_USER_END)
            ->queryAll();

        $queryCount = count($query);

        if ($queryCount == count($idCourses))
            return true;
        else
            return false;
    }


    /**
     * Return an SQL data provider to retrieve arbitrary USER's "My Courses"
     *
     * @param integer $idUser
     * @param array $args Filtering parameters
     *
     * @return CSqlDataProvider
     */
    public static function sqlDataProviderMyCourses($idUser, $args = array(), $pageSize=false, $offset=false)
    {

        $fltEnrollmentStatus    = isset($args["fltEnrollmentStatus"]) ? $args["fltEnrollmentStatus"] : null;
        $fltLabelId             = isset($args["fltLabelId"]) ? $args["fltLabelId"] : null;
        $remove_plan_courses    = isset($args["remove_plan_courses"]) ? $args["remove_plan_courses"] : null;
        $fltSearch              = isset($args["fltSearch"]) ? $args["fltSearch"] : null;
        $filterObject           = isset($args["filterObject"]) ? $args["filterObject"] : new stdClass();
        $sortingFields          = isset($args["selected_sorting_fields"]) ? $args["selected_sorting_fields"] : null;

        $isGodAdmin = CoreUser::isUserGodadmin($idUser);
        $isPu = CoreUser::isPowerUser($idUser);

        // SQL Parameters
        $params = array();

        // BASE
        $commandBase = Yii::app()->db->createCommand();
        $commandBase->from('learning_course t');

        // Filter by enrollment status of the current user
        // By default, we get ALL courses user is enrolled to
        $params[':idUser'] = $idUser;
        $conditionBase = "(enrollment.idCourse=t.idCourse) AND (enrollment.idUser=:idUser)";
        $condition = $conditionBase;
        if ($fltEnrollmentStatus !== null) {
            switch ($fltEnrollmentStatus) {
                case 'active':
                    $condition = $conditionBase . " AND (enrollment.status != :completed)";
                    $params[':completed'] = LearningCourseuser::$COURSE_USER_END;
                    break;
                case 'completed':
                    $condition = $conditionBase . " AND (enrollment.status = :completed)";
                    $params[':completed'] = LearningCourseuser::$COURSE_USER_END;
					break;
				case 'in_progress':
					$condition = $conditionBase . " AND (enrollment.status = :status)";
					$params[':status'] = LearningCourseuser::$COURSE_USER_BEGIN;
					break;
				case 'not_started':
					$condition = $conditionBase . " AND (enrollment.status = :status)";
					$params[':status'] = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
					break;
            }
        }
        $commandBase->join('learning_courseuser enrollment', $condition);

        // Do a left join with Course Field Value db table
        $commandBase->leftJoin(LearningCourseFieldValue::model()->tableName() . " lcfv", "lcfv.id_course = t.idCourse  ");

        // Filter by a particular LABEL ID
        if (($fltLabelId !== null) && ($fltLabelId != 'all')) {
            if ($fltLabelId === 'none') {
                $commandBase->leftJoin('learning_label_course ll', 'll.id_course=t.idCourse');
                $commandBase->andWhere('ll.id_common_label IS NULL');
            } else {
                $commandBase->join('learning_label_course ll', '(ll.id_course=t.idCourse) AND (ll.id_common_label=:labelId)');
                $params[':labelId'] = $fltLabelId;
            }
        }

        // If we are asked to, select only courses NOT being part of a learning plan (left join + id_path = NULL)
        if ($remove_plan_courses) {
            $user_learning_plan = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser($idUser);
			if(!empty($user_learning_plan))
			{
				//Reoving the coursepath courses
				$courses =	Yii::app()->db->createCommand()
							->select("id_item")
							->from(LearningCoursepathCourses::model()->tableName())
							->where(array('in', 'id_path', $user_learning_plan))
							->queryColumn();

				if(!empty($courses))
					$commandBase->andWhere('t.idCourse  NOT IN (' .implode(',', $courses).  ')');
			}
        }

        // Search a string in the course name
        if ($fltSearch) {
            $commandBase->andWhere('CONCAT(t.name, " ", t.description, " ", t.code) LIKE :search');
            $params[':search'] = '%' . str_replace(' ', '%', $fltSearch) . '%';
        }

        if ((!$isGodAdmin) && (!$isPu)) {
            $notAllowedCoursesIds = Docebo::learnerNotAllowedCoursesIds();
            if ($notAllowedCoursesIds && is_array($notAllowedCoursesIds)) {
                $commandBase->andWhere(array("NOT IN", "t.idCourse", $notAllowedCoursesIds));
            }
        }

        // Set filter on course type
        $allowedTypes = array(
            LearningCourse::TYPE_ELEARNING,
            LearningCourse::TYPE_WEBINAR
        );

        if (PluginManager::isPluginActive('ClassroomApp')) {
            $allowedTypes[] = LearningCourse::TYPE_CLASSROOM;
        }

        // Let custom plugins add their own custom course types
        $_customTypesList = array();
        Yii::app()->event->raise('CollectCustomCourseTypes', new DEvent(self, array(
            'types' => &$_customTypesList
        )));

        if (! empty($_customTypesList)) {
            $allowedTypes = array_merge($allowedTypes, array_keys($_customTypesList));
        }

        // Filter on course types
        $commandBase->andWhere(array("IN", "t.course_type", $allowedTypes));

        // If there is any filter by course fields to be applied
        if (is_array($filterObject->courseFields) && count($filterObject->courseFields) > 0) {
            foreach ($filterObject->courseFields as $filterItemId => $filterItem) {
                if (! empty($filterItem['type'])) {
                    switch ($filterItem['type']) {
                        case 'dropdown':
                            if (! empty($filterItem['value'])) {
                                $commandBase->andWhere('lcfv.field_' . $filterItemId . ' =' . addslashes($filterItem['value']));
                            }
                            break;

                        case 'date':
                            if (! empty($filterItem['value'])) {
                                $commandBase->andWhere('lcfv.field_' . $filterItemId . $filterItem['condition'] . " '" . Yii::app()->localtime->fromLocalDate($filterItem['value']) . "' ");
                            }

                            break;

                        case 'textfield':
                            if (! empty($filterItem['value'])) {
                                switch ($filterItem['condition']) {
                                    case 'contains':
                                        $commandBase->andWhere('lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) . "%' ");
                                        break;

                                    case 'equal':
                                        $commandBase->andWhere('lcfv.field_' . $filterItemId . " = '" . addslashes($filterItem['value']) . "' ");
                                        break;

                                    case 'not-equal':
                                        $commandBase->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') != '" . addslashes($filterItem['value']) . "' ");
                                        break;

                                    default:
                                        break;
                                }
                            }

                            break;

                        case 'textarea':
                            if (! empty($filterItem['value'])) {
                                switch ($filterItem['condition']) {
                                    case 'contains':
                                        $commandBase->andWhere('lcfv.field_' . $filterItemId . " LIKE '%" . addslashes($filterItem['value']) . "%' ");
                                        break;

                                    case 'not-contains':
                                        $commandBase->andWhere("COALESCE(lcfv.field_" . $filterItemId . ", '') NOT LIKE '%" . addslashes($filterItem['value']) . "%' ");
                                        break;

                                    default:
                                        break;
                                }
                            }
                            break;
                        default:
                            // Not an internal field type -> instantiate the custom field handler and let it process the case
                            $fieldName = 'CourseField' . ucfirst($filterItem['type']);
                            if (method_exists($fieldName, "applyMyCoursesFilter")) {
                                $fieldName::applyMyCoursesFilter($filterItemId, $filterItem['value'], $commandBase);
                            }
                            break;
                    }
                }
            }
        }

        // DATA
        $commandData = clone $commandBase;

        // COUNTER
        $commandCounter = clone $commandBase;
        $commandCounter->select('count(t.idCourse)');
        $numRecords = $commandCounter->queryScalar($params);

        // ORDERING
        if (! empty($sortingFields)) {
            $commandData->order = implode(',', $sortingFields);
        }
        else {
            $commandData->order = 'name';
        }

        // DATA-II
        $commandData->select("*");

        // If offset is provided, we do not rely on CPagination object, but directly put limit/offset in sql
        if ($offset !== false) {
        	$pagination = false;
        	if ($pageSize !== false) {
        		$commandData->limit((int) $pageSize);
        	}
        	$commandData->offset((int) $offset);
        }
        else {
        	if ($pageSize == false) {
        		$pagination = false;
        	} else {
        		$pagination = new CPagination();
        		// We MUST set item count here!!!! The object will calculate page count for us
        		$pagination->itemCount = $numRecords;
        		$pagination->pageSize = $pageSize;
        	}
        }

        $config = array(
            'totalItemCount' => $numRecords,
            'pagination' => $pagination,
            'keyField' => 'idCourse',
            'params' => $params
        );

        // Pass dbCommand, not SQL text, to apply parameters binding !!!
        $dataProvider = new CSqlDataProvider($commandData, $config);
        return $dataProvider;
    }

    /**
     * @param $status
     * @return array|string|void
     */
    public static function getStatusForMobileApp($status) {

        switch($status)
        {
            case self::$COURSE_USER_SUBSCRIBED:
                $class = 'icon-play-circle gray';
                break;
            case self::$COURSE_USER_END:
                $class = 'icon-completed green';
                break;
            case self::$COURSE_USER_BEGIN:
                $class = 'icon-play-circle yellow';
                break;
            case self::$COURSE_USER_CONFIRMED:
            case self::$COURSE_USER_OVERBOOKING:
            case self::$COURSE_USER_SUSPEND:
            case self::$COURSE_USER_WAITING_LIST:
                $class = '';
                break;
        }

        return $class;
    }

	public static function transformSessionData($data){
		if($data instanceof LearningCourseuser){
			return $data->learningUser;
		}else if($data instanceof CoreUser){
			return $data;
		}
	}
	private function completeEquivalentCourses(){
		// get enrollment equivalencies and complete them also
		$equivalentCoursesIds = $this->course->getEquivalentCoursesIds();
		foreach ($equivalentCoursesIds as $equivalentCourseId) {
			$courseuserModel = LearningCourseuser::model()->findByAttributes(array(
				'idUser' => $this->idUser,
				'idCourse' => $equivalentCourseId,
			));
			if($courseuserModel && $courseuserModel->status != self::$COURSE_USER_END){
				// complete also the other course
				$courseuserModel->status = self::$COURSE_USER_END;
				$courseuserModel->status_propagator = $this->idCourse;
				$courseuserModel->validPropagatorId = true;
				$courseuserModel->save();
			}
		}
	}

	private function updateFromEquivalentEnrollments(){
		if(isset($this->course) && $this->course) {
			$equivalentSourceCoursesIds = $this->course->getEquivalentSourceCoursesIds(true, $this->idUser);
			foreach ($equivalentSourceCoursesIds as $equivalentSourceCourseId) {
				$courseuserModel = LearningCourseuser::model()->findByAttributes(array(
						'idUser' => $this->idUser,
						'idCourse' => $equivalentSourceCourseId,
				));
				if($courseuserModel && $courseuserModel->status == self::$COURSE_USER_END && $this->status != self::$COURSE_USER_END) {
					$this->status = $courseuserModel->status;
					$courseuserModel->status_propagator = $this->idCourse;
					$courseuserModel->validPropagatorId = true;
					$this->completedByEquivalenceOfCourse = true;
				}
			}
		}
	}

	/**
	 * @param $courseId
	 * @param $userId
	 * @param $fieldId
	 * @return string
	 */
	public static function getAdditionalFieldValueByFieldId($courseId, $userId, $fieldId)
	{
	    
	    static $cache;
	    $cacheKey = md5(serialize(func_get_args()));
	     
	    if (isset($cache[$cacheKey])) {
	        return $cache[$cacheKey];
	    }
	    
		$fieldValue = '';

		$sql = 'SELECT 
			lef.type AS type
			FROM learning_enrollment_fields lef
			WHERE lef.id='.$fieldId;
		$connection = Yii::app()->db;
		$field = $connection->createCommand($sql)->queryRow();


		$sql = 'SELECT
  				JSON_EXTRACT(t.enrollment_fields, CONCAT(\'$."\', '.$fieldId.', \'"\')) AS field_value
				FROM learning_courseuser t
				WHERE t.idUser=' . $userId . ' AND t.idCourse=' . $courseId;
		$connection = Yii::app()->db;
		$fieldValue = $connection->createCommand($sql)->queryRow();
		
		$fieldValue = str_replace('"','',$fieldValue['field_value']);

		if ($field['type'] == 2) {

			$sql = 'SELECT
  						JSON_UNQUOTE(lefd.translation->\'$[0]."'.Yii::app()->getLanguage().'"\') AS dropdown_value
    				  FROM learning_enrollment_fields_dropdown lefd
  						WHERE lefd.id=' .(int) $fieldValue;

			$connection = Yii::app()->db;
			$fieldValue = $connection->createCommand($sql)->queryRow();

			$fieldValue = $fieldValue['dropdown_value'];

		}

		$cache[$cacheKey] = $fieldValue;
		
		return $fieldValue;
	}
	
	
}