<?php

class EmailForm extends CModel {

	public $subject;
	public $message;
	public $confirm;

	public function rules() {
		return array(
			array('subject, message, confirm', 'required'),
		);
	}

	public function attributeNames() {
		return array(
			'subject',
			'message',
			'confirm',
		);
	}

	public function attributeLabels() {
		return array(
			'subject' => Yii::t('standard', '_SUBJECT'),
			'message' => Yii::t('standard', '_MESSAGE'),
			// 'confirm' => Yii::t('standard', '_CONFIRM'),
		);
	}
}