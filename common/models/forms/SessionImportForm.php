<?php
/**
 * Created by PhpStorm.
 * User: ivelin
 * Date: 2015-06-18
 * Time: 10:20 AM
 */

class SessionImportForm extends BaseImportForm{

	/**
	 * Holds the map between CSV fields and session fields (or session virtual attributes_
	 * @var Array
	 */
	CONST COURSE_NAME 		= 'course name';
	CONST COURSE_LANGUAGE 	= 'course language';
	CONST SESSION_NAME 		= 'session name';
	CONST SESSION_INFO 		= 'session information';
	CONST MAX_ENROLL 		= 'maximum enrollments';
	CONST MIN_ENROLL 		= 'minimum enrollments';
	CONST NOTIFY 			= 'notify';
	CONST INSTRUCTOR 		= 'instructor';
	CONST DATE 				= 'date';
	CONST NAME 				= 'name';
	CONST START_TIME 		= 'start time';
	CONST END_TIME 			= 'end time';
	CONST BREAK_START 		= 'break start';
	CONST BREAK_END 		= 'break end';
	CONST TIMEZONE 			= 'timezone';
	CONST LOCATION_NAME 	= 'location name';
	CONST CLASSROOM_NAME 	= 'classroom name';
	CONST LAST_SUB_DATE 	= 'last subscription date';
	CONST EVALUATION_SCORE 	= 'evaluation score base';

	CONST SKIPP = 'ignoredfield';

	public $userImportMap;

	protected $_importMap = array(
		'course name' => 'course name',
		'course language' => 'course language',
		'session name' => 'session name',
		'session information' => 'session information',
		'maximum enrollments' => 'maximum enrollments',
		'minimum enrollments' => 'minimum enrollments',
		'notify' => 'notify',
		'instructor' => 'instructor',
		'date' => 'date',
		'name' => 'name',
		'start time' => 'start time',
		'end time' => 'end time',
		'break start' => 'break start',
		'break end' => 'break end',
		'timezone' => 'timezone',
		'location name' => 'location name',
		'classroom name' => 'classroom name',
		'last subscription date' => 'last subscription date',
		'evaluation score base' => 'evaluation score base',
	);

	private static $mandatoryFields = array(
		self::COURSE_NAME => 'course names',
		self::COURSE_LANGUAGE => 'course language',
		self::SESSION_NAME => 'session name',
		self::MAX_ENROLL => 'maximum enrollments',
		self::DATE => 'date',
		self::START_TIME => 'start time',
		self::END_TIME => 'end time',
		self::TIMEZONE => 'timezone',
		self::LOCATION_NAME => 'location name'
	);

	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels() {
		return array(
			//Common
			'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
			'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
			'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
			'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET'),

			//Course
			'course name' => Yii::t('standard', '_COURSE_NAME'),
			'course language' => Yii::t('course', '_COURSE_LANG_METHOD'),
			//Session
			'session name' => Yii::t('classroom', 'Session name'),
			'session information' => Yii::t('classroom', 'Session info'),
			'maximum enrollments' => Yii::t('classroom', 'Maximum enrollments'),
			'minimum enrollments' => Yii::t('classroom', 'Minimum enrollments'),
			'notify' => Yii::t('classroom', 'Notify enrolled users about session changes'),
			'instructor' => Yii::t('levels', '_LEVEL_6'),
			'date' => Yii::t('standard', '_DATE'),
			//Session Date
			'name' => Yii::t('standard', '_NAME'),
			'start time' => Yii::t('certificate', 'Session hour begin'),
			'end time' => Yii::t('certificate', 'Session hour end'),
			'break start' => Yii::t('classroom', 'Break begin'),
			'break end' => Yii::t('classroom', 'Break end'),
			'timezone' => Yii::t('classroom', 'Select Timezone'),
			'location name' => Yii::t('classroom', 'Location name'),
			'classroom name' => Yii::t('classroom', 'Classroom name'),
			'last subscription date' => Yii::t('classroom', 'Last subscription date'),
			'evaluation score base' => Yii::t('classroom', 'Evaluation score base'),
		);
	}

	/**
	 * _importMap getter
	 *
	 * @return array:
	 */
	public function getImportMap() {
		return $this->_importMap;
	}

	/**
	 * _importMap setter
	 *
	 * @param array $importMap
	 * @return boolean
	 */
	public function setImportMap($importMap) {
		if (is_array($importMap)) {
			$this->_importMap = $importMap;
			return true;
		}
		return false;
	}

	/**
	 * Runs a few consistency checks and returns their status
	 */
	public function getConsistencyChecks()
	{
		// Get First 10 rows
		$data = $this->getData(0,1);
		// Check file not empty
		if(empty($data))
			$this->addError('file', Yii::t("classroom", "CSV file is empty or cannot be read"));

		// Check the number of columns
		$firstRow = reset($data);
		if(count($firstRow) < count(self::$mandatoryFields))
			$this->addError('file', Yii::t("classroom" , 'CSV file should contain at least {N} columns', array('{N}' => count(self::$mandatoryFields))));

		return $this->hasErrors() ? false : true;
	}

	/**
	 * Process items in chunks
	 * @param $offset
	 * @param $limit
	 * @return mixed
	 */
	public function processItems($offset, $limit)
	{
		$data = $this->getData($offset,$limit);
		$i = $offset;
		// Loop thought all rows in this portion
		foreach($data as $key => $row) {

			// If is checked "consider first row as header" go to the next row if this is the first loop(ever)
			if( (bool)$this->firstRowAsHeader && $key == 0 && $offset == 0)continue;
			$summaryErrorTexts = array();

			$transaction = Yii::app()->db->getCurrentTransaction();
			if(!$transaction)
				$transaction = Yii::app()->db->beginTransaction();

			try{
				/*
				 * First we are mapping the columns as the user were ordered them
				 */
				$remapped = $this->reMapData($row);

				/*
				 * First checking if the row is OK with all the condition that should satisfy
				 * and if yes import the data into db otherwise,
				 * create file with all unsuccessful rows and show it to the user
				 */
				$this->checksBeforeImporting($remapped);

				$this->successCounter++;
				// Everything related to this was successful, COMMIT database changes please
				$transaction->commit();
			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$summaryErrorTexts[] = $e->getMessage();
				$transaction->rollback();
			}

			// We Finished handling a given item. If errors have been collected, add it to Failed list, together with all the errors as
			// the first field, enclosed in "" and separated by a pipe |)
			// All these failed branches can be used to report errors (save to file, show on screen, etc)
			if (count($summaryErrorTexts) > 0) {
				$tmpArray = array(implode('|', $summaryErrorTexts));
				$tmpArray = array_merge($tmpArray, $row);
				$this->failedLines[] = CSVParser::arrayToCsv($tmpArray);
			}
		}

		return true;
	}

	/**
	 * @param $data
	 * @return array
	 */
	private function reMapData($data)
	{
		$row = array();
		foreach($this->userImportMap as $key => $map){
			if($map == self::SKIPP) continue;
			$row[$map] = $data[$key];
		}
		return $row;
	}

	private function checksBeforeImporting($data)
	{
		// If the row's mandatory fields are empty add the row to the file with the errors
		foreach(self::$mandatoryFields as $key => $cell) {
			if(!isset($data[$key]) || empty($data[$key]))
				throw new Exception('Missing mandatory fields: '.$key);
		}

		if(!$this->validateDate($data[self::DATE]))
			throw new Exception("Invalid date format: ".$data[self::DATE]);
		if(isset($data[self::LAST_SUB_DATE]) && !empty($data[self::LAST_SUB_DATE]) && !$this->validateDate($data[self::LAST_SUB_DATE]))
			throw new Exception("Invalid date format: ".$data[self::LAST_SUB_DATE]);

		/*
		 * If the course name + course language (fields 1 and 2) do not return any result - ERROR “course not found”;
		 * Or if the result return more then one record - ERROR "More then one ..."
		 */
		$course = LearningCourse::model()->findAllByAttributes(array('name' => $data[self::COURSE_NAME], 'lang_code' => $data[self::COURSE_LANGUAGE]));
		if(count($course) != 1)
			throw new Exception(count($course) == 0 ? 'Course not found' : 'More than one Course with the same name and language');

		$model = array_shift($course);
		// Check if the course is not a Classroom course - ERROR
		if($model->course_type != LearningCourse::TYPE_CLASSROOM)
			throw new Exception('Course do not allow Classroom sessions');

		$session = LtCourseSession::model()->findAllByAttributes(array('course_id' => $course->idCourse, 'name' => $data[self::SESSION_NAME]));
		if(!empty($session) && count($session) > 1)
			throw new Exception('More than one Session with the same name');

		/*
		 * If the given location don't exist - ERROR
		 * Or if the location exists more then one time - ERROR
		 */
		$location = LtLocation::model()->findAllByAttributes(array('name' => $data[self::LOCATION_NAME]));
		if(count($location) != 1)
			throw new Exception(count($location) == 0 ? 'Location not found' : 'More than one Location with the same name');

		/*
		 * If the Classroom on the current location don't exist - ERROR
		 * Or if the Classroom location exists more then one time - ERROR
		 */
		$classroomLocation = LtClassroom::model()->findAllByAttributes(array('name' => $data[self::CLASSROOM_NAME], 'id_location' => $location[0]->id_location));
		if(count($classroomLocation) > 1)
			throw new Exception('More than one Location or Classroom with the same name');
		elseif(count($classroomLocation) == 0)
		{
			$classroom = new LtClassroom();
			$classroom->id_location = $location[0]->id_location;
			$classroom->id_classroom = 0;
			$classroomLocation = array(0 => $classroom);
		}

		$classroomLocationModel = array_shift($classroomLocation);
			// If the row satisfy all the condition import it into DB
		if (Yii::app()->user->getIsPu() && !CoreUserPU::isAssignedToPU($model->idCourse, 'course')) {
			throw new Exception('You do not have enough permissions to conduct this action.');
		}
		$this->saveRow($data, $model->idCourse, $classroomLocationModel);
	}

	private function validateDate($inputDate, $format = 'Y-m-d') {
		$date = DateTime::createFromFormat($format, $inputDate);
		return $date && $date->format($format) === $inputDate;
	}

	private function saveRow($data, $course_id, $classroomLocation)
	{
		/*
		 * Because each row contain info about one day in the session i.e only one date,
		 * we don't have date_begin and date_end
		 * getSession() will update the lt_course_session.end_date
		 * if the session with the current name and course_id already exists
		 */
		$session = $this->getSession($data, $course_id);

		/*
		 * In the CSV file we may have filled the instructor field
		 * if the field is not empty we have to assign the user as instructor
		 * for this session
		 */
		if(isset($data[self::INSTRUCTOR]))
			$this->handleInstructor($data, $session, $course_id);

		/*
		 * Check if the session_id and give date already exist in DB show error message
		 */
		$sessionDate = LtCourseSessionDate::model()->findByAttributes(array('id_session' => $session->id_session, 'day' => $data[self::DATE]));
		if($sessionDate !== null)
			throw new Exception('Session date already exists');

		// Save the record in the DB
		$sessionDate = new LtCourseSessionDate();
		$sessionDate->id_session = $session->id_session;
		$sessionDate->day = $data[self::DATE]; // Date is mandatory field so we don't need to be worried
		$sessionDate->name = isset($data[self::NAME]) ? $data[self::NAME] : '';
		$sessionDate->time_begin = $data[self::START_TIME]; // Mandatory field
		$sessionDate->time_end = $data[self::END_TIME]; // Mandatory field
		$sessionDate->break_begin = isset($data[self::BREAK_START]) ? $data[self::BREAK_START] : '00-00-00';
		$sessionDate->break_end = isset($data[self::BREAK_END]) ? $data[self::BREAK_END] : '00-00-00';
		$sessionDate->timezone = $data[self::TIMEZONE]; // Mandatory field
		$sessionDate->id_location = $classroomLocation->id_location;
		$sessionDate->id_classroom = $classroomLocation->id_classroom;
		if($sessionDate->validate())
			$sessionDate->save();
		else
			throw new Exception(json_encode($sessionDate->getErrors()));

		//Because in the file we don't get the total hours, we have to calculate them manually
		$this->updateSessionTotalHours($session);
	}

	private function getSession($data, $course_id)
	{
		// Check if the session exist get the current one ....
		/* @var $session LtCourseSession */
		$session = LtCourseSession::model()->findByAttributes(array('course_id' => $course_id, 'name' => $data[self::SESSION_NAME]));
		if($session !== null){
			$session->date_end = $this->manageSessionEndDate($session, $data);
			$session->date_begin = $this->manageSessionBeginDate($session, $data);
			$session->save();
			return $session;
		}

		$session = new LtCourseSession();
		$session->course_id = $course_id;
		$session->name = $data[self::SESSION_NAME]; // Session name is mandatory field so it will be set and it will have value (otherwise it wouldn't be here)
		$session->max_enroll = $data[self::MAX_ENROLL]; // Max Enrollment is also mandatory
		$session->min_enroll = isset($data[self::MIN_ENROLL]) ? $data[self::MIN_ENROLL] : 0;
		$session->score_base = isset($data[self::EVALUATION_SCORE]) && $data[self::EVALUATION_SCORE] ? $data[self::EVALUATION_SCORE] : 100; // By default if the Evaluation type is Online Test this field will be with value of 100
		$session->other_info = isset($data[self::SESSION_INFO]) && $data[self::SESSION_INFO] ? $data[self::SESSION_INFO] : '';
		$session->notify_enrolled = isset($data[self::NOTIFY]) && $data[self::NOTIFY] == 'TRUE' ? 1 : 0;
		$session->date_begin = Yii::app()->localtime->toLocalDateTime($this->convertDateTimeFromGivenTimeZoneToUTC($data[self::DATE], $data[self::START_TIME], $data[self::TIMEZONE]));
		$session->date_end = Yii::app()->localtime->toLocalDateTime($this->convertDateTimeFromGivenTimeZoneToUTC($data[self::DATE], $data[self::END_TIME], $data[self::TIMEZONE]));
		if ($data[self::LAST_SUB_DATE])
			$session->last_subscription_date = Yii::app()->localtime->toLocalDateTime($this->convertDateTimeFromGivenTimeZoneToUTC($data[self::LAST_SUB_DATE], '00:00:00', $data[self::TIMEZONE]));
		$session->total_hours = 0;
		// If we have value in score_base field the evaluation type should be 0;
		$session->evaluation_type = isset($data[self::EVALUATION_SCORE]) && $data[self::EVALUATION_SCORE] != '' ? LtCourseSession::EVALUATION_TYPE_SCORE : LtCourseSession::EVALUATION_TYPE_ONLINE;
		if($session->save())
			return $session;
		else
			throw new Exception(json_encode($session->getErrors()));
	}

	/**
	 * @param $data
	 * @param $session
	 * @param $course_id
	 */
	private function handleInstructor($data, $session, $course_id)
	{
		//First we check if the user exists
		$userName = strpos($data[self::INSTRUCTOR], '/') === 0 ? $data[self::INSTRUCTOR] : '/'.$data[self::INSTRUCTOR];
		$user = CoreUser::model()->findByAttributes(array('userid' => $userName));
		if($user !== null){
			// Assign the user on this session if he is not subscribed already
			$sessionUser = LtCourseuserSession::model()->findByAttributes(array('id_session' => $session->id_session, 'id_user' => $user->idst));
			if($sessionUser == null){
				// Enroll User to Session and enroll him as Instructor
				$courseUser = new LtCourseuserSession();
				$courseUser->id_session =  $session->id_session;
				$courseUser->id_user = $user->idst;
				$courseUser->status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;

				if(!CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe') && Yii::app()->user->getIsPu())
					$courseUser->status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

				$courseUser->date_subscribed = Yii::app()->localtime->toLocalDateTime();
				$courseUser->setLevel(LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
				if($courseUser->save()){
					if($courseUser->status != LtCourseuserSession::$SESSION_USER_WAITING_LIST)
					{
						Yii::app()->event->raise(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION, new DEvent($this, array(
							'session_id' => $courseUser->id_session,
							'user' => intval($courseUser->id_user),
						)));
					}
				}
			}
		}
	}

	private function manageSessionEndDate(LtCourseSession $session, $data)
	{
		$existingEndDateToUtc = Yii::app()->localtime->fromLocalDateTime($session->date_end);
		$newEndDateToUTC = $this->convertDateTimeFromGivenTimeZoneToUTC($data[self::DATE], $data[self::END_TIME], $data[self::TIMEZONE]);
		return strtotime($existingEndDateToUtc) >= strtotime($newEndDateToUTC) ? $session->date_end :Yii::app()->localtime->toLocalDateTime($newEndDateToUTC);
	}

	private function manageSessionBeginDate(LtCourseSession $session, $data)
	{
		$existingEndDateToUtc = Yii::app()->localtime->fromLocalDateTime($session->date_begin);
		$newBeginDateToUTC = $this->convertDateTimeFromGivenTimeZoneToUTC($data[self::DATE], $data[self::START_TIME], $data[self::TIMEZONE]);
		return strtotime($existingEndDateToUtc) <= strtotime($newBeginDateToUTC) ? $session->date_begin :Yii::app()->localtime->toLocalDateTime($newBeginDateToUTC);
	}

	private function convertDateTimeFromGivenTimeZoneToUTC($date, $time, $timezone)
	{
		$newDateObj = new DateTime($date . ' ' . $time, new DateTimeZone($timezone));
		$newDateObj->setTimezone(new DateTimeZone("UTC"));
		return $newDateObj->format('Y-m-d H:i:s');
	}

	private function updateSessionTotalHours($session)
	{
		$wholeSession = 0;
		$sessionDates = $session->learningCourseSessionDates;
		foreach($sessionDates as $sessionDate)
		{
			$break = false;
			$currentDay = strtotime($sessionDate->time_end) - strtotime($sessionDate->time_begin);
			if($sessionDate->break_begin != '00:00:00' && $sessionDate->break_end != '00:00:00')
				$break = strtotime($sessionDate->break_end) - strtotime($sessionDate->break_begin);

			if($break)
				$currentDay -= $break;

			$wholeSession += $currentDay;
		}

		// Format the number with 2 decimals after the comma but first devide to 3600 to convert it to hours otherwise it will be in seconds
		$session->total_hours = number_format($wholeSession / 3600, 2);
		$session->save();
	}

	public function dataProvider()
	{
		$items = $this->getData(0, 6);
		if ($this->firstRowAsHeader)
			unset($items[0]);

		return new CArrayDataProvider($items);
	}

	public function getGridColumnsImportSessions()
	{
		$columns = array();
		//Get only the first row;
		$items = $this->getData(0, 1);
		foreach ($items[0] as $key => $value) {
			$column = array(
				'name' => $key,
				'htmlOptions' => array('class' => 'column-'.$key),
			);
			if ($this->firstRowAsHeader)
				$column['header'] = $value;

			if(isset($column['header']) && $column['header']){
				$tmp = CHtml::dropDownList(
					'import_map['.$column['name'].']',
					isset($this->_importMap[$value]) ? $this->_importMap[$value] : 'ignoredfield',
					$this->importMapDropDownList()
				);
				$tmp .= '<p>'.$column['header'].'</p>';
				$column['header'] = $tmp;
				$column['cssClassExpression'] = '"session-import-col"';
			}
			$columns[] = $column;
		}

		return $columns;
	}

	private function importMapDropDownList()
	{
		$list = array();
		$data = $this->getData(0,1);
		$list['ignoredfield'] = Yii::t('organization_chart', '_IMPORT_IGNORE');
		foreach($this->_importMap as $key => $label)
			$list[$label] = $this->getAttributeLabel($label);

		return $list;
	}

	public function getTotalRowCount()
	{
		$count =  count($this->getData());
		return $this->firstRowAsHeader ? $count - 1 : $count;
	}

}