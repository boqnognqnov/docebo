<?php

class DashboardFiltersForm extends CFormModel {

    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string
     */
    public $dateTo;
    /**
     * @var string
     */
    public $filterBy;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('', 'required'),
            array('dateFrom, dateTo, filterBy', 'safe'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
        );
    }

    /**
     * Will simply save the filters in the current user session
     */
    public function saveFilters()
    {
        switch($this->filterBy){
            case 'begin':
                $this->dateFrom = date('Y-m-d', 0);
                $this->dateTo = date('Y-m-d');
                break;
            case 'thisMonth':
                $currentMonth = intval(date('m'));
                $lastDay = new DateTime(date('Y') . '-' . $currentMonth . '-1');
                $lastDay = $lastDay->format('t');
                $this->dateFrom = date('Y') . '-' . $currentMonth . '-1';
                $this->dateTo = date('Y') . '-' . $currentMonth . '-' . $lastDay;
                break;
            case 'lastMonth':
                $lastMonth = intval(date('m')) - 1;
                $lastDay = new DateTime(date('Y') . '-' . $lastMonth . '-1');
                $lastDay = $lastDay->format('t');
                $this->dateFrom = date('Y') . '-' . $lastMonth . '-1';
                $this->dateTo = date('Y') . '-' . $lastMonth . '-' . $lastDay;
                break;
            case 'dataRange':
                break;
        }
        $dateFrom = strtotime($this->dateFrom);
        Yii::app()->session['dashboard_date_from'] = ($dateFrom) ? date('Y-m-d', $dateFrom) : '';
        $dateTo = strtotime($this->dateTo);
        Yii::app()->session['dashboard_date_to'] = ($dateTo) ? date('Y-m-d', $dateTo) : '';
    }
} 