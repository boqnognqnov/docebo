<?php

class ReportFieldsForm extends CModel {

	private $user;
	private $course;
	private $enrollments;
	public $subscription_status;
	public $subscribed_status;
	public $in_progress_status;
	public $days_from_course;
	public $courses_expiring_in;
	public $courses_expiring_before;
	public $certification_status;
	public $condition_status;
	public $consider_suspended_users;
	public $consider_other_than_students;
	public $date_from;
	public $date_to;
	public $expiration_date_from;
	public $expiration_date_to;
	public $lo_type;
	public $milestone;
	public $temp;

	public $version;


	private static $dateAttributes = array(
		'date_from',
		'date_to',
		'courses_expiring_before',
		'expiration_date_from',
		'expiration_date_to'
	);

	public function rules() {
		return array(
			array('lo_type, milestone', 'required', 'on' => 'LO'),
			array('days_from_course, courses_expiring_in', 'numerical', 'integerOnly' => true),
			array('subscription_status, subscribed_status, in_progress_status, courses_expiring_before, consider_suspended_users, consider_other_than_students, date_from, date_to, lo_type, milestone, certification_status, condition_status, version', 'safe'),
			array('date_from, date_to,courses_expiring_before, expiration_date_from, expiration_date_to', 'convertToMysql'),
		);
	}

	public function attributeNames() {
		return array(
			'subscription_status',
			'subscribed_status',
			'in_progress_status',
			'days_from_course',
			'courses_expiring_in',
			'courses_expiring_before',
			'consider_suspended_users',
			'consider_other_than_students',
			'date_from',
			'date_to',
			'lo_type',
			'milestone',
		);
	}

	public function attributeLabels() {
		return array(
			'subscription_status' => Yii::t('standard', 'Subscription status'),
			'subscribed_status' => Yii::t('standard', '_USER_STATUS_SUBS'),
			'in_progress_status' => Yii::t('standard', '_USER_STATUS_BEGIN'),
			'days_from_course' => Yii::t('report', 'Include only users enrolled '),
			'courses_expiring_in' => Yii::t('report', 'Course(s) expiring in'),
			'courses_expiring_before' => Yii::t('report_filters', '_COURSES_EXPIRING_BEFORE'),
			'lo_type' => Yii::t('report', '_RU_LO_TYPES'),
			'milestone' => Yii::t('organization', '_ORGMILESTONE'),
			'consider_suspended_users' => Yii::t('organization_chart', '_SHOW_SUSPENDED'),
			'consider_other_than_students' => Yii::t('report_filters', '_CONSIDER_OTHER_THAN_STUDENTS'),
			'date_from' => Yii::t('standard', '_FROM'),
			'date_to' => Yii::t('standard', '_TO'),

			'subscription_code' => Yii::t('standard', 'Subscription code'),
			'subscription_code_set' => Yii::t('standard', 'Subscription code set'),

			// Ecommerce related
			'id_trans'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ID_TRANSACTION),
			'quantity'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_QUANTITY),
			'single_price'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_SINGLE_PRICE),
			'discount'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_DISCOUNT),
			'total_price'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_TOTAL_PRICE),
			'paid'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_PAYMENT_STATUS),
			'item_code'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_CODE),
			'item_name'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_NAME),
			'session_name'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_SESSION_NAME),
			'session_start_date'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_SESSION_START_DATE),
			'session_end_date'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_SESSION_END_DATE),
			'item_type'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_ITEM_TYPE),
			'payment_type'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_PAYMENT_TYPE),
			'payment_txn_id'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_PAYMENT_TXN_ID),
			'bill_address1'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_ADDRESS_1),
			'bill_address2'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_ADDRESS_2),
			'bill_city'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_BILL_CITY),
			'bill_state'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_BILL_STATE),
			'bill_zip'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_BILL_ZIP),
			'bill_company_name'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_BILL_COMPANY_NAME),
			'bill_vat_number'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_BILLING_VAT_NUMBER),
			'coupon_code'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_COUPON_CODE),
			'coupon_description'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_COUPON_DESCRIPTION),
			'location'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_LOCATION),

            //Notifications related
            'notif_type' => LearningReportFilter::fieldName(LearningReportFilter::F_NOTIFICATION_TYPE),
            'notif_timestamp' => LearningReportFilter::fieldName(LearningReportFilter::F_NOTIFICATION_TIMESTAMP),
            'notif_json_data' => LearningReportFilter::fieldName(LearningReportFilter::F_NOTIFICATION_JSON),
            'notif_is_read' => LearningReportFilter::fieldName(LearningReportFilter::F_NOTIFICATIONS_DELIVERED_READ),

            //Users - Learning Plan (Coursepath)
            'plan_code' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_CODE),
            'plan_credits' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_CREDITS),
            'plan_subDate' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_SUBSCRIPTION_DATE),
            'plan_compDate' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_COMPLETION_DATE),
            'plan_compStatus' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_COMPLETION_STATUS),
            'plan_compPercent' => LearningReportFilter::fieldName(LearningReportFilter::F_COURSEPATH_COMPLETION_PERCENT),

			// Aggregated field names; they are NOT part of models
			'number_of_sessions' => Yii::t("report", "_TH_USER_NUMBER_SESSION"),
			'total_time_in_course' => Yii::t("statistic", "_USER_TOTAL_TIME"),
			'coursesCategory.translation' => Yii::t("standard", "Course Category"),
			'total_subscribed_users' => Yii::t("course", "_COURSE_USERISCR"),

			'number_of_users_who_has_not_started_yet' => Yii::t("report", "number_of_users_who_has_not_started_yet"),
			'number_of_users_in_progress' => Yii::t("report", "number_of_users_in_progress"),
			'number_of_users_completed' => Yii::t("stats", "Users that finished the course"),
			'total_time_spent_by_users_in_the_course' => Yii::t("certificate", "_TOTAL_TIME"),
			'show_percents' => Yii::t("report", "show_percents"),
			'rate_average' => Yii::t('social_rating', 'Rating'),

			'group_name' => Yii::t('organization_chart', '_GROUPUSER_groupid'),
			'type' => Yii::t('standard', '_TYPE'),
			'total_users_in_group' => Yii::t("report", "total_users_in_group"),

			'course_name' => Yii::t('standard', '_COURSE_NAME'),
			'date_creation'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_DATE_CREATED),
			'date_activated'=>LearningReportFilter::fieldName(LearningReportFilter::F_ECOMMERCE_DATE_PAID),
			'show_deleted_too'=>Yii::t('report', 'Show also transactions for deleted users and courses'),

			'show_all_users' => 'Show all users, not only learnings',
			'show_suspended_users' => 'Show suspended users',

			'contest_name' => Yii::t('standard', '_NAME'),
            'contest_description' => Yii::t('standard', '_DESCRIPTION'),
            'contest_period' => Yii::t('gamification', 'Period'),
            'contest_goal' => Yii::t('gamification', 'Goal'),
            'rank_position' => Yii::t('gamification', 'User position'),
            'rank_rewards' => Yii::t('gamification', 'Rewards'),
            'rank_goal_stats' => Yii::t('gamification', 'Goal Stats'),

			// External training fields
			'to_date' => Yii::t('standard', '_DATE'),
			'status' => Yii::t('standard', 'Approval Status'),

			//Users - LO
			'version' => Yii::t('standard', 'Version'),
			
			'percent_peer_reviews' => Yii::t('standard', '% of peer reviews compared to total channels'),
			'percent_shared_assets' => Yii::t('standard', '% of shared assets compared to total channels'),
			'percent_qa' => Yii::t('standard', '% of q&a compared to total channels'),
			
			'unique_id' => Yii::t('standard', 'User unique ID'),
			'firstname' => Yii::t('standard', 'First Name'),
			'lastname' => Yii::t('standard', 'Last Name'),
			'active_date' => Yii::t('standard', 'Active'),
			'published_status' => Yii::t('standard', 'Published assets'),
			'unpublished_status' => Yii::t('standard', 'Unpublished assets'),
			
		);
	}

	public function getMilestones() {
		return array(
			'none' => Yii::t('standard', '_NONE'),
			'start' => Yii::t('organization', '_ORGMILESTONE_START'),
			'end' => Yii::t('organization', '_ORGMILESTONE_END'),
		);
	}

	public function getContestFields($customFieldsOnly = false){
		$fields = array(
				'selectable' => array(
						'contest_name','contest_description','contest_period','contest_goal',
				),
		);
		return $this->processFields($fields);
	}

	public function getRankingFields(){
		$fields = array(
				'selectable' => array(
						'rank_position','rank_rewards','rank_goal_stats',
				),
		);
		return $this->processFields($fields);
	}

	public function getUserFields($customFieldsOnly=false,  $preventEvent=false) {

		$result = array();

		//THIS IS OLD LOGIC THE NEW ONE USE core_user_field table
//		$fields = CoreField::model()->language()->findAll();

		$customFieldsRaw=CoreUserField::getFieldsTranslations();


//		$event = new DEvent($this, array('fields' => &$fields, 'preventEvent'=>$preventEvent));
//		Yii::app()->event->raise('GetUserFieldsForCustomReportWithCodes', $event);
//		if(!$event->shouldPerformAsDefault()){
//			return $event->return_value;
//		}

		$customFields = array();

		foreach ($customFieldsRaw as $item) {
			$customFields[$item['id_field']] = $item['translation'];
		}


		if ($customFieldsOnly) {
			$result['custom_fields'] = $customFields;
			return $result;
		}

		$fields = array(
			'fixed' => array(
				'userid',
			),
			'selectable' => array(
				'idst', 'firstname', 'lastname', 'email', 'register_date', 'valid', 'suspend_date', 'expiration', 'email_status'
			),
		);

		$result = $this->processFields($fields, CoreUser::model());

		// This is actually BRANCHES, not groups! But that's just how we choose to name them in the beginning
		$result['selectable']['groups_list'] = LearningReportFilter::fieldName(LearningReportFilter::F_USER_GROUPS_LIST);

		$result['custom_fields'] = $customFields;

		$result['selectable'] 	+= $customFields;


		// this will allow custom plugins to change the field list
		Yii::app()->event->raise('ModifyFields', new DEvent($this, array('fields' => &$result)));


		return $result;
	}

    public function getCoursepathFields(){
        $fields = array(
            'selectable' => array(
                'plan_code', 'plan_name', 'plan_credits',
            ),
        );
        return $this->processFields($fields);
    }

    public function getCoursepathEnrollmentsFields(){
        $fields = array(
            'selectable' => array(
                'plan_subDate','plan_compDate','plan_compStatus','plan_compPercent',
            ),
        );
        return $this->processFields($fields);
    }

	public function getCourseFields($reportType = false) {
		$event = new DEvent($this, array());
		Yii::app()->event->raise('GetCustomCourseFields', $event);
		if(!$event->shouldPerformAsDefault()){
			return $event->return_value;
		}

        $result = array();
        $criteria = new CDbCriteria();
        $criteria->order = 'sequence';
        $fields = LearningCourseField::model()->findAll($criteria);
        $customFields = array();
        foreach($fields as $field) {
            $customFields[$field->id_field] = $field->getTranslation();
        }

        $fields = array(
        'fixed' => array(
            'name',
		),
			'selectable' => array(
				'coursesCategory.translation', 'code', 'status', 'credits', 'date_begin', 'date_end', 'course_type', 'course_internal_id' 
			),
		);

		/*
		* If the report type is one of the following
		*  - Groups-Courses
		* there is aditional columns  that should be added
		*/
		if($reportType == LearningReportType::GROUPS_COURSES){
			$fields['selectable'][] = 'group_description';
		}
		/*
		 * If the report type is one of the following
		 *  - Users-Courses
		 *  - Courses-Users
		 *  - Groups-Courses
		 * there is aditional columns "Expired" that should be added
		 */
		if ($reportType == LearningReportType::USERS_COURSES || $reportType == LearningReportType::COURSES_USERS || $reportType == LearningReportType::GROUPS_COURSES)
			$fields['selectable'][] = 'expired';
        $result = $this->processFields($fields, LearningCourse::model());

        $result['custom_fields'] = $customFields;
        $result['selectable'] 	+= $customFields;

        return $result;
	}

	public function getSessionFields(){
		$fields = array(
				'fixed' => array(),
				'selectable' => array(
				'name', 'score_base', 'date_begin','date_end', 'total_hours',
				),
		);
		$result = $this->processFields($fields, LtCourseSession::model());
		return $result;
	}

	public function getSessionEnrollmentsFields(){
		$fields = array(
			'selectable' => array(
				'level', 'date_inscr', 'status',
				'learningCourseuserSessions.evaluation_score',
				'learningCourseuserSessions.evaluation_status',
				'learningCourseuserSessions.evaluation_text',
				'learningCourseuserSessions.attendance_hours'
			),
		);
		$result = $this->processFields($fields, LtCourseSession::model());
		return $result;
	}

	public function getLoEnrollmentFields(){
		$fields = array(
			'selectable' => array(
				'date_inscr', 'date_complete'
			)
		);

		$result = $this->processFields($fields, LearningCourseuser::model());
		return $result;
	}

    public function getNotificationFields(){
        $fields = array(
            'selectable' => array(
                'notif_type', 'notif_timestamp', 'notif_json_data'
            ),
        );

        return $this->processFields($fields);
    }

    public function getNotificationDeliveredFields(){
        $fields = array(
            'selectable' => array(
                'notif_is_read'
            ),
        );

        return $this->processFields($fields);
    }



	public function getEcommerceFields(){
		$fields = array(
			//'fixed'=>array(),
			'selectable' => array(
				'id_trans', 'quantity', 'single_price', 'discount', 'total_price', 'paid', 'date_creation', 'date_activated', 'bill_address1', 'bill_address2', 'bill_city', 'bill_state', 'bill_zip', 'bill_company_name', 'bill_vat_number', 'payment_type', 'payment_txn_id',
					'coupon_code','coupon_description'
			),
		);

        $event = new DEvent($this, array('fields' => &$fields));
        Yii::app()->event->raise('onBeforeSelectFieldsEcommercerReport', $event);

		return $this->processFields($fields);
	}

	public function getEcommerceItemsFields(){
		$fields = array(
			'selectable' => array(
				'item_code', 'item_name', 'session_name', 'session_start_date', 'session_end_date', 'item_type', 'location'
			),
		);

		return $this->processFields($fields);
	}

	public function getAuditTrailFields(){
		$fields = array(
			'fixed' => array(
				'action', 'json_data', 'ip', 'timestamp',
			),
		);

		return $this->processFields($fields);
	}


	public function getEnrollmentsFields($reportType = false) {
		$event = new DEvent($this, array());
		Yii::app()->event->raise('GetCustomEnrollmentsFields', $event);
		if (!$event->shouldPerformAsDefault()) {
			return $event->return_value;
		}

		$fields = array(
			'selectable' => array(
				'level', 'date_inscr', 'date_first_access', 'date_last_access', 'date_complete', 'status', 'date_begin_validity', 'date_expire_validity', 'score_given', 'initial_score_given'
			),
			'aggregate' => array(
				'number_of_sessions', 'total_time_in_course',
			),
		);


		$sql='SELECT JSON_UNQUOTE(t.translation->\'$[0]."'.Yii::app()->getLanguage().'"\') as field_name, t.id FROM learning_enrollment_fields t';
		$connection=Yii::app()->db;
//		$customFieldsRaw=$connection->createCommand($sql)->queryColumn();
		$customFieldsRaw=$connection->createCommand($sql)->queryAll();


		if (sizeof($customFieldsRaw > 0) && is_array($customFieldsRaw)) {

			foreach ($customFieldsRaw as $oneCustomField) {
				$fields['custom_fields'][$oneCustomField['id']] = $oneCustomField['field_name'];
			}
		}

		if($reportType && $reportType == LearningReportType::USERS_COURSES && PluginManager::isPluginActive('SubscriptionCodesApp')){
			$fields = array_merge_recursive($fields, array(
				'selectable' => array(
					'subscription_code', 'subscription_code_set'
				)
			));
		}


		return $this->processFields($fields, LearningCourseuser::model());
	}

	public function getCertificationEnrollmentFields(){
		$fields = array(
				'selectable' => array(
						'issued_on', 'to_renew_in', 'completed_activity'
				)
		);

		return $this->processFields($fields);
	}

	public function getLearningObjectsFields() {
		$fields = array(
			'selectable' => array(
				'objectType', 'firstAttempt', 'dateAttempt', 'status', 'score', 'version'
			),
		);

		return $this->processFields($fields, LearningCommontrack::model());
	}

	public function getCoursesStatsFields($reportType = false) {
		$fields = array(
			'aggregate' => array(
				'total_subscribed_users', 'number_of_users_who_has_not_started_yet', 'number_of_users_in_progress', 'number_of_users_completed', 'total_time_spent_by_users_in_the_course', 'show_percents',
			),
		);
		if($reportType == LearningReportType::COURSES_USERS){
			$fields['aggregate'][] = 'rate_average';
		}

		return $this->processFields($fields);
	}

	private function processFields($fields, $model = null) {
		$result = array();
		foreach ($fields as $group => $list) {
			foreach ($list as $fieldId=>$field) {
				if ($group == 'aggregate' || $model == null) {
					$result[$group][$field] = $this->getAttributeLabel($field);
					continue;
				}

				if($group=='custom_fields'){
					$result[$group][$fieldId] = $this->getAttributeLabel($field);
					continue;
				}

				//This applies only for Exporting users to CSV/XLS
				if($field == 'idst'){
					$result[$group][$field] = $model->getAttributeLabel('idUser');
				} else{
					$result[$group][$field] = $model->getAttributeLabel($field);
				}
			}
		}


		return $result;
	}

	public function getLearningObjectTypes() {

		$result = array(
				'authoring' => Yii::t('authoring', 'Slides converter'),
				'htmlpage' => Yii::t('storage', '_LONAME_htmlpage'),
				'file' => Yii::t('organization', 'File'),
				'poll' => Yii::t('storage', '_LONAME_poll'),
				'scormorg' => Yii::t('organization', 'Scorm'),
				'test' => Yii::t('organization', 'Test'),
				'tincan' => Yii::t('storage', 'TinCan'),
				'video' => Yii::t('organization', 'Video'),
				'deliverable' => Yii::t('deliverable', '_LONAME_deliverable')
		);
		return $result;
	}

	public function getGroupsFixedFields() {
		$fields = array(
			'fixed' => array(
				'group_name', 'groupid', 'type', 'total_users_in_group', 'course_name',
			),
		);
		return $this->processFields($fields);
	}

	public function prepareFields($reportType) {
		$out = array();

		switch ($reportType) {
			case LearningReportType::USERS_COURSES:
				$out['user'] = $this->getUserFields();
				$out['course'] = $this->getCourseFields($reportType);
				$out['enrollment'] = $this->getEnrollmentsFields($reportType);
				break;

			case LearningReportType::USERS_DELAY:
				$out['user'] = $this->getUserFields();
				$out['course'] = $this->getCourseFields();
				break;

			case LearningReportType::USERS_LEARNING:
				$out['user'] = $this->getUserFields();
				$out['course'] = $this->getCourseFields();
				$out['learning_object'] = $this->getLearningObjectsFields();
				$out['enrollment'] = $this->getLoEnrollmentFields();
				break;

			case LearningReportType::COURSES_USERS:
				$out['user'] = $this->getUserFields(true);
				$out['course'] = $this->getCourseFields($reportType);
				$out['stat'] = $this->getCoursesStatsFields($reportType);
				break;

			case LearningReportType::GROUPS_COURSES:
				$out['course'] = $this->getCourseFields($reportType);
				$out['stat'] = $this->getCoursesStatsFields();
				$out['group'] = $this->getGroupsFixedFields();
				break;
			case LearningReportType::ECOMMERCE_TRANSACTIONS:
				$userFieldsTmp = $this->getUserFields();
				unset($userFieldsTmp['custom_fields']);

				$out['ecommerce'] = $this->getEcommerceFields();
				$out['user'] = $userFieldsTmp;
				$out['ecommerce_item'] = $this->getEcommerceItemsFields();
				break;
			case LearningReportType::AUDIT_TRAIL:
				$userFieldsTmp = $this->getUserFields();
				unset($userFieldsTmp['custom_fields']);

				// those fields are get always, so no need to show them as selectable to the user
				unset($userFieldsTmp['selectable']['email']);
				unset($userFieldsTmp['selectable']['groups_list']);

                $userFieldsTmp['selectable']['approved_by'] = Yii::t('audit_trail', 'Approved By');
				$out['user'] = $userFieldsTmp;
				$out['course'] = $this->getCourseFields();
				$out['audit_trail'] = $this->getAuditTrailFields();
				break;
            case LearningReportType::USERS_COURSEPATH:
                $userFieldsTmp = $this->getUserFields();
                unset($userFieldsTmp['custom_fields']);
                $out['user'] = $userFieldsTmp;
                $out['coursepaths'] = $this->getCoursepathFields();
                $out['plansUsers'] = $this->getCoursepathEnrollmentsFields();
                break;

			case LearningReportType::USERS_SESSION:
				$out['user'] = $this->getUserFields();
				$out['course'] = $this->getCourseFields();
				$out['session'] = $this->getSessionFields();
				$out['enrollment'] = $this->getSessionEnrollmentsFields();
				break;

			case LearningReportType::CERTIFICATION_USERS:
				$out['certification'] = $this->getCertificationFields();
				$out['stat'] = $this->getStatisticsFields();
				break;

			case LearningReportType::USERS_CERTIFICATION:
				$out['user'] = $this->getUserFields();
				$out['certification'] = $this->getCertificationFields();
				$out['enrollment'] = $this->getCertificationEnrollmentFields();
				break;
            case LearningReportType::USERS_EXTERNAL_TRAINING :
                $out['user']                = $this->getUserFields();
                $out['external_trainings']  = $this->getExternalTrainings();
                break;

            case LearningReportType::USERS_BADGES :
                $out['user']        = $this->getUserFields();
                $out['badges']      = $this->getBadgesFields();
                $out['assignment']  = $this->getAssignmentFields();

                break;

			case LearningReportType::USERS_CONTESTS:
				$out['user']		= $this->getUserFields();
				$out['contest']	= $this->getContestFields();
				$out['ranking'] 	= $this->getRankingFields();
				break;
			
			case LearningReportType::APP7020_ASSETS:
				$out['asset'] = $this->getAssetFields();
				$out['stat'] = $this->getAssetStatFields();
				break;
			case LearningReportType::APP7020_EXPERTS:
				$out['experts'] = $this->getExpertsFields();
				$out['caf'] = $this->getCoachActivityFields();
				$out['pr'] = $this->getPRActivityFields();
				break;
			case LearningReportType::APP7020_CHANNELS:
				$out['channel'] = $this->getChannelsFields();
				$out['stat'] = $this->getChannelsStatFields();
				break;
			case LearningReportType::APP7020_USER_CONTRIBUTIONS:
				$out['us'] = $this->getUCFields();
				$out['as'] = $this->getUCStatFields();
				break;

			default:
				$event = new DEvent($this, array(
					'reportType' => $reportType,
					'out' => &$out,
					'that' => $this,
				));
				Yii::app()->event->raise('OnReportFiltersFormFieldsPreparation', $event);
		}

		$event = new DEvent($this, array('reportType' => $reportType, 'out' => $out));
		Yii::app()->event->raise('beforeReturnReportSelectableFields', $event);
		if (!$event->shouldPerformAsDefault()) {
			return $event->return_value;
		}

		return $out;
	}

	public function getBadgesFields() {
		$fields = array(
			'selectable' => array(
				'icon', 'name', 'description', 'score',
			),
		);
		return $this->processFields($fields);
	}

	public function getAssignmentFields() {
		$fields = array(
			'selectable' => array(
				'issued_on', 'event_key',
			),
		);
		return $this->processFields($fields);
	}

	public function getCertificationFields() {
		$fields = array(
			'fixed' => array(
				'title'
			),
			'selectable' => array(
				'title', 'description', 'expiration',
			),
		);
		return $this->processFields($fields);
	}

	public function getStatisticsFields() {
		$fields = array(
			'aggregate' => array(
				'issued', 'expired', 'active',
			),
		);
		return $this->processFields($fields);
	}

	/**
	 * Called by model 'rules' to convert date and datetimes FROM User's format to MySQL format
	 * @param string $attribute
	 * @param array $params
	 */
	public function convertToMysql($attribute, $params) {
		$this->$attribute = trim($this->$attribute);
		if (!empty($this->$attribute))
			$this->$attribute = Yii::app()->localtime->fromLocalDate($this->$attribute);
		return true;
	}

	/**
	 * Enumerates a predefined, hardcoded list of attributes and convert them from MySQL to User's local date format
	 */
	public function convertDatesToLocal() {

		foreach (self::$dateAttributes as $attribute) {
			$this->$attribute = trim($this->$attribute);
			if (!empty($this->$attribute))
				$this->$attribute = Yii::app()->localtime->toLocalDate($this->$attribute);
		}
	}

	public function getExternalTrainings() {

		$fields = TranscriptsField::model()->findAll(array('order'=>'sequence'));
		$customFields = array();
		foreach($fields as $field) {
			$customFields[$field->id_field] = $field->getTranslation();
		}

        $fields = array(
            'selectable' => array(
                'course_name','course_type','to_date', 'score', 'credits', 'training_institute', 'certificate', 'status'),
        );
		$result = $this->processFields($fields);

		$result['custom_fields'] = $customFields;
		$result['selectable'] 	+= $customFields;

		return $result;
    }
	
	public function getAssetFields(){
        $fields = array(
            'selectable' => array(
                'title', 'published_by', 'published_on', 'channels'
            ),
        );
        return $this->processFields($fields);
    }
	
	public function getAssetStatFields(){
        $fields = array(
            'aggregate' => array(
                'total_views', 'asset_rating', 'questions', 'answers', 'best_answers', 'answers_likes', 'answers_dislikes', 'total_invited_people' ,'global_watch_rate', 'average_reaction_time', 'watched', 'not_watched'
			),
		);
		return $this->processFields($fields);
	}
	
	public function getChannelsFields(){
        $fields = array(
            'selectable' => array(
                'name', 'description', 'published_assets'
            ),
        );
        return $this->processFields($fields);
    }
	
	public function getChannelsStatFields(){
        $fields = array(
            'aggregate' => array(
                'submitted_assets', 
				'unpublished_assets', 
				'average_assets_rating', 
				'total_contributors', 
				'questions', 
				'answers', 
				'best_answers',
				'channel_experts', 
				'askers_involved', 
				'percent_peer_reviews', 
				'percent_shared_assets', 
				'percent_qa'
			),
		);
		return $this->processFields($fields);
	}

	/**
	 * Get all Coach selectable fields for raport step 4
	 * @return array
	 */
	public function getExpertsFields() {
		$fields = array(
			'selectable' => array(
				'username', 'expert_full_name', 'email', 'assigned_channels'
			),
		);
		return $this->processFields($fields);
	}
	
	/**
	 * Get all Coach selectable fields for raport step 4
	 * @return array
	 */
	public function getCoachActivityFields() {
		$fields = array(
			'aggregate' => array(
				'questions_answered',
				'average_answer_time',
				'requests_satisfied',
				'answers_likes',
				'answers_dislikes',
				'best_answer',
				'rank_by_answers_quality',
				'rank_by_first_to_answer_rate',
				'rank_by_partecipation_rate'
			),
		);
		return $this->processFields($fields);
	}
	/*
	 * Get all Coach selectable fields for raport step 4
	 * @return array
	 */
	public function getPRActivityFields() {
		$fields = array(
			'aggregate_3th' => array(
				'review_assets',
				'writen_peer_reviews',
				'asset_published',
				'average_review_time',
				'rank_by_partecipation_rate'
			),
		);
		return $this->processFields($fields);
	}

	public function getUCFields(){
        $fields = array(
			'selectable' => array(
				'unique_id', 
				'firstname', 
				'lastname', 
				'email', 
				'creation_date', 
				'active_date', 
				'suspend_date', 
				'expiry', 
				'email_validation_status',
				'branches', 
				'role'
			),
		);
        return $this->processFields($fields);
    }
	
	public function getUCStatFields(){
        $fields = array(
            'aggregate' => array(
                'number_of_uploaded_assets', 
				'involved_channels', 
				'published_status', 
				'unpublished_status', 
				'private_assets', 
			),
		);
		return $this->processFields($fields);
	}
}
