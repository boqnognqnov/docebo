<?php
/**
 *
 * This model is used to save the state between CSV import UI steps and between Progressive Importer runs (chunked import)
 * Stored in session as a whole object.
 */
abstract class BaseImportForm extends CFormModel {

	/**
	 * Various model attributes
	 * @var mixed
	 */
	public $file; 								// PHP temporary file (CUploadedFile instance)
	public $localFile;  						// locally saved, temporary copy of the uploaded file (in upload_tmp)
	public $originalFilename;					// Just the original name of the uploaded file. Name only! Not the path!
	public $separator = 'auto';					// CSV separator (1st Step UI option)
	public $manualSeparator;
	public $firstRowAsHeader = true;			// Consider first row in CSV as a header? (1st Step UI option)	
	public $charset = 'UTF-8'; 					// charset of csv file (1st Step UI option)
	public $totalItems = 0;						// Keep total number of users in the imported CSV, counted at the first run of importer
	public $failedLines = array();   			// Collecta failing users for a single run; reset before call saveUsers() please 
	public $cumulativeFailedLines = array();	// Collects ALL failed lines for ALL runs of the progressive import (session!)
	public $successCounter	= 0;
	public $_maxFileSize = 36700160; 			// 35 Mb

	/**
	 * @var CustomS3Storage
	 */
	protected $customS3Storage;

	public $key;								// Amazon S3 key
	public $secret;								// Amazon S3 secret
	public $bucketName;							// Amazon S3 bucketName
	public $region;								// Amazon S3 region
	public $folder;								// Amazon S3 root folder


	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		$rules = array(
			array(
				'file',
				'file',
				'on' => 'step_one',
				'maxSize' => $this->_maxFileSize,
				'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
				'types' => array('csv'),
				'wrongType' => 'File type should be in "csv" format',
			),
			array('file', 'file', 'on' => 'step_one',
				'mimeTypes' => array(
					'text/x-comma-separated-values',
					'text/comma-separated-values',
					'application/octet-stream',
					'application/vnd.ms-excel',
					'text/csv',
					'text/plain',
					'application/csv',
					'application/excel',
					'application/vnd.msexcel',
					'text/x-c',
					'text/x-c++'
				),
				'wrongMimeType' => 'File type should be in "csv" format',
				'skipOnError' => true,
			),
			array('separator, manualSeparator, firstRowAsHeader, charset', 'safe'),
		);

		if($this->hasS3Settings())
			$rules[] = array('key, secret, bucketName, region, folder', 'safe');

		return $rules;
	}

	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels() {
		$label = array(
			'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
			'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
			'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
			'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET')
		);

		if($this->hasS3Settings()) {
			$label['key'] = Yii::t('migration', 'Key');
			$label['secret'] = Yii::t('migration', 'Secret');
			$label['region'] = Yii::t('migration', 'Region');
			$label['bucketName'] = Yii::t('migration', 'Bucket Name');
			$label['folder'] = Yii::t('migration', 'Root folder');
		}

		return $label;
	}

	/**
	 *
	 * @param string $type
	 * @return number
	 */
	public function getMaxFileSize($type = 'B') {
		switch ($type) {
			case 'B':
				return $this->_maxFileSize;
				break;
			case 'KB':
				return (int) ($this->_maxFileSize / 1024);
				break;
			case 'MB':
				return (int) ($this->_maxFileSize / (1024 * 1024));
				break;
		}
	}

	/**
	 * Returns the delimiter to use
	 * @return string
	 */
	private function resolveDelimiter() {
		$delimiter = '';
		switch ($this->separator) {
			case 'comma':
				$delimiter = ',';
				break;
			case 'semicolon':
				$delimiter = ';';
				break;
			case 'manual':
				$delimiter = $this->manualSeparator;
				break;
			case 'auto':
				$delimiter = 'auto';
				break;
		}

		return $delimiter;
	}


	/**
	 * Saves the CSV file and the total number of lines in the instance variable
	 */
	public function saveCSVFile() {
		// Save it on S3 for progressive processing
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
		$storage->store($this->localFile);

		$csvFile = new QsCsvFile(array(
			'path' => $this->localFile,
			'charset' => $this->charset,
			'delimiter' => $this->resolveDelimiter(),
			'firstRowAsHeader' => $this->firstRowAsHeader
		));

		$this->totalItems = $csvFile->getTotalLines();
	}

	/**
	 *
	 * @param string $numUsers
	 * @return array
	 */
	public function getData($offset = 0, $limit = -1) {
		if(!$this->localFile || !file_exists($this->localFile)) {
			// Load file from S3 in local uploadtmp
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
			$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $this->originalFilename;
			$this->localFile = $storage->downloadAs($this->originalFilename, $localFilePath);
		}

		// Read CSV file
		$csvFile = new QsCsvFile(array(
			'path' => $this->localFile,
			'charset' => $this->charset,
			'delimiter' => $this->resolveDelimiter(),
			'firstRowAsHeader' => $this->firstRowAsHeader
		));

		if($limit < 0)
			$data = $csvFile->getArray();
		else
			$data = $csvFile->getChunk($limit, $offset);

		// Remove empty rows
		foreach ($data as $index => $row) {
			if ((count($row) == 1) && empty($row[0]))
				unset($data[$index]);
		}

		return $data;
	}

	/**
	 * Init methods
	 */
	public function initForm() {
	}


	/**
	 * Initialize the S3 storage (call this only if needed in subclass)
	 */
	protected function initS3Storage() {
		if(!$this->customS3Storage) {
			// Instantiate custom S3 storage manager
			$params['key'] = $this->key;
			$params['secret'] = $this->secret;
			$params['region'] = $this->region;
			$params['bucket_name'] = $this->bucketName;
			$this->customS3Storage = new CustomS3Storage(CS3Storage::COLLECTION_COMMON, $params);
		}
	}

	/**
	 * Downloads a file from S3
	 * @param $filename
	 * @return bool|string
	 * @throws CException
	 */
	protected function downloadFileFromS3($filename) {
		$this->initS3Storage();

		$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $filename;
		$savedLocalFilePath = $this->customS3Storage->downloadAs($filename, $localFilePath, $this->folder);
		if(!$savedLocalFilePath || !is_file($savedLocalFilePath)) {
			FileHelper::removeFile($localFilePath);
			throw new CException("Missing S3 file '".$filename."'");
		}

		return $savedLocalFilePath;
	}

	/**
	 * Downloads a file locally from an S3 storage and creates asset
	 * @param $filename
	 * @param $type
	 *
	 * @return ID of the created asset
	 */
	public function createLocalAsset($filename, $type) {
		try {
			// Download file in upload tmp
			$savedLocalFilePath = $this->downloadFileFromS3($filename);

			// Create core_asset for image file
			$asset = new CoreAsset();
			$asset->type = $type;
			$asset->sourceFile = $savedLocalFilePath;
			if(!$asset->save())
				throw new CException(CHtml::errorSummary($asset));
			$asset->refresh();
			$assetId = $asset->id;

			// Delete local file and clean asset
			FileHelper::removeFile($savedLocalFilePath);
			unset($asset);
		} catch(Exception $e) {
			// Delete local file
			if($savedLocalFilePath)
				FileHelper::removeFile($savedLocalFilePath);
			throw $e;
		}

		// Return created asset id
		return $assetId;
	}

	/**
	 * Returns true if S3 settings are needed
	 * @return bool
	 */
	public function hasS3Settings() {
		return false;
	}

	/**
	 * Returns errors for AR models
	 * @param $model
	 * @return string
	 */
	protected function errorSummary($model) {
		$content='';
		if(!is_array($model))
			$model=array($model);

		foreach($model as $m) {
			foreach($m->getErrors() as $errors) {
				foreach($errors as $error) {
					if($error!='')
						$content.="$error\t";
				}
			}
		}

		return rtrim($content, "\t");
	}

	/**
	 * Runs a few consistency checks and returns their status
	 */
	public abstract function getConsistencyChecks();

	/**
	 * Process items in chunks
	 * @param $offset
	 * @param $limit
	 * @return mixed
	 */
	public abstract function processItems($offset, $limit);
}