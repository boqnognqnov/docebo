<?php

/**
 * Class LanguageImportForm
 * Used for import of a new or existent language from a .xliff file
 */
class LanguageImportForm extends CFormModel {

	public $file;
	public $replaceIfExist;

	protected $data;
	protected $language;
	protected $maxFileSize = 3145728; // 314572800 = 300 Mb

	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			array(
				'file',
				'file',
				'maxSize' => $this->maxFileSize,
				'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
				'types' => array('xliff', 'xlf'),
				'wrongType' => 'File type should be in "xliff" format',
			),
			array('replaceIfExist', 'safe'),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels() {
		return array(
			'replaceIfExist' => Yii::t('admin_lang', '_OVERWRITE_EXISTENT'),
			'file' => Yii::t('standard', '_FILE'),
		);
	}

	/**
	 * @param string $type
	 * @return number
	 */
	public function getMaxFileSize($type = 'B') {
		switch ($type) {
			case 'B':
				return $this->maxFileSize;
				break;
			case 'KB':
				return (int) ($this->maxFileSize / 1024);
				break;
			case 'MB':
				return (int) ($this->maxFileSize / (1024 * 1024));
				break;
		}
	}

	/**
	 * import language
	 */
	public function importLanguage() {
		$this->getData();
		$success = false;
		if (is_array($this->data)) {

			//make sure that provided language is "normalized"
			$this->language = strtolower(trim($this->language));

			//detect which code type we have provided and handle it consequently
			switch (Lang::getCodeType($this->language)) {
				case Lang::$BROWSERCODE:
					$languageCode = Lang::getCodeByBrowserCode($this->language);
					$browserCode = $this->language;
					break;
				case Lang::$LANGCODE:
					$languageCode = $this->language;
					$browserCode = Lang::getBrowserCodeByCode($this->language);
					break;
				default:
					//if we arrive here, then the user is trying to load a non-existent language
					throw new CException('admin_lang', 'The target language is not valid');
					break;
			}
			$coreLangLanguage = CoreLangLanguage::model()->findByPk($languageCode);

			//create new CoreLangLanguage record if not exist
			if (!isset($coreLangLanguage) || empty($coreLangLanguage)) {
				/*
				$languageModel = new CoreLangLanguage();
				$languageModel->lang_code = $languageCode;
				$languageModel->lang_browsercode = $browserCode;
				$languageModel->save();
				*/
				//NOTE: we must stick to existing languages
				throw new CException('admin_lang', 'The target language is not valid');
			}
			foreach ($this->data as $module => $data) {
				foreach ($data as $key => $translation) {
					//create or update translation
					$this->createOrUpdateTranslation($module, $key, $translation);
				}
			}

			//insert records in core_field for this language if not exist
			CoreUserFieldTranslations::insertMissingTranslationForLanguage($languageCode);

			//insert records in core_field_son for this language if not exist
			CoreUserFieldDropdownTranslations::insertMissingTranslationForLanguage($languageCode);

			//insert labels for this language if not exist
			if(PluginManager::isPluginActive('LabelApp')) {
				LearningLabel::model()->insertLabelsForLanguage($languageCode);
			}

			$success = true;
		}
		return $success;
	}

	/**
	 * create new CoreLangTranslation or update the existing one if $this->replaceIfExist is true
	 * If the new translation is the same as the previous
	 * @param $module
	 * @param $key
	 * @param $translation
	 * @return bool
	 */
	public function createOrUpdateTranslation($module, $key, $translation) {
		//initialize output variable
		$success = true;

		// NOTE: we export language files applying htmlspecialchars() function to translation texts.
		// So we need to invert the process when importing.
		// ( see: LangManagementController::actionExportLang() )
		$translation = html_entity_decode($translation);

		//make sure to be using the proper language code
		$this->language = strtolower(trim($this->language));
		switch (Lang::getCodeType($this->language)) {
			case Lang::$BROWSERCODE:
				$languageCode = Lang::getCodeByBrowserCode($this->language);
				$browserCode = $this->language;
				break;
			case Lang::$LANGCODE:
				$languageCode = $this->language;
				$browserCode = Lang::getBrowserCodeByCode($this->language);
				break;
			default:
				throw new CException('admin_lang', 'The target language is not valid');
				break;
		}

		// Do not save standard translations
		$comparation = strcmp($translation, Yii::t($module, $key, array(), null, $browserCode));
		if ($comparation === false) return true;

		$model = CoreLangText::model()->findByAttributes(
			array(
				'text_module' => $module,
				'text_key' => $key
			)
		);
		if(!isset($model)) {
			$model = new CoreLangText();
			$model->text_module = $module;
			$model->text_key = $key;
			$model->text_attributes = '';
			$model->context = '';
			$success = $model->save(false);
		}

		if($success && !empty($translation)) {
			$translationModel = CoreLangTranslation::model()->findByAttributes(
				array(
					'id_text' => $model->id_text,
					'lang_code' => $languageCode
				)
			);
			if(!isset($translationModel)) {
				$translationModel = new CoreLangTranslation();
				$translationModel->id_text = $model->id_text;
				$translationModel->lang_code = $languageCode;
			} elseif (!$this->replaceIfExist) {
				return true;
			}

			$translationModel->translation_text = $translation;
			$success = $translationModel->save();

			return $success;
		}

		return $success;
	}

	/**
	 * gets data from the imported file and write it in array
	 *
	 * 	array(
	 *		'module' => array(
	 * 			'key' => 'translation'
	 * 			)
	 * 		...
	 * 	)
	 */
	protected function getData() {
		$fileTempName = $this->file->getTempName();
		if(file_exists($fileTempName)) {
			$tempFileName = $fileTempName;
			$xmlData = file_get_contents($tempFileName);
		}

		if (isset($xmlData)) {
			$data = array();
			try {

				//read xml file content and parse it
				libxml_use_internal_errors(true); //avoid XML fatal errors, let the user handle them
				$xml = simplexml_load_string($xmlData);
				if (!$xml) {
					$log = 'Invalid XML language file:';
					foreach (libxml_get_errors() as $error) { $log .= "\n".'- '.$error->level.': '.$error->message; }
					$log = str_replace("\n\n", "\n", $log); //this is just for human readability in application.log file
					Yii::log($log, 'error');
					libxml_clear_errors();
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
				//$xml = new SimpleXmlElement($xmlData);

				$fileAttributes = $xml->file->attributes();
				$this->language = strtolower( trim( (string)$fileAttributes['target-language']) ); //normally this is expected to be a browser code

				if (empty($this->language)) {
					throw new CException(Yii::t('admin_lang', 'The target language is required'));
				}
				switch (Lang::getCodeType($this->language)) {
					case Lang::$BROWSERCODE: $isAvailable = Lang::isLangCodeAvail(Lang::getCodeByBrowserCode($this->language), false); break;
					case Lang::$LANGCODE: $isAvailable = Lang::isLangCodeAvail($this->language, false); break;
					default: $isAvailable = false; break;
				}
				if (!$isAvailable) {
					throw new CException(Yii::t('admin_lang', 'The target language is not valid'));
				}

				$body = $xml->file->body;
				foreach($body->children() as $translation) {
					$attributes = $translation->attributes();

					$module = (string) $attributes['module'];
					if(empty($module)) {
						throw new CException(Yii::t('admin_lang', 'The attribute "module" is required.'));
					}
					$key = (string) $attributes['key'];
					if(empty($key)) {
						throw new CException(Yii::t('admin_lang', 'The attribute "key" is required.'));
					}
					if(!isset($translation->target)) {
						throw new CException(Yii::t('admin_lang', 'target element is required.'));
					} else {
						$translationText = (string)$translation->target;
					}

					$data[$module][$key] = $translationText;
				}
				$this->data = $data;
			} catch (CException $e) {
				$this->addError('language', $e->getMessage());
			}
		} else {
			throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
		}
	}

}