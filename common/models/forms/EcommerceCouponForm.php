<?php

class EcommerceCouponForm extends CFormModel {

    /**
     * @var EcommerceCoupon
     */
    public $coupon;
    /**
     * @var string
     */
    public $validityType;
    /**
     * @var string
     */
    public $usageType;
    /**
     * @var bool
     */
    public $useOnAllCoursesChecked;
    /**
     * @var bool
     */
    public $minimumOrderChecked;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('', 'required'),
            array('validityType, usageType, useOnAllCoursesChecked, minimumOrderChecked', 'safe'),
            // The following rule is used by search().
            array('validityType, usageType, useOnAllCoursesChecked, minimumOrderChecked', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
        );
    }

    /**
     * Init coupon edit form
     */
    public function init() {
        parent::init();

        // Initialize coupon
        $coupon = EcommerceCoupon::model()->findByPk(Yii::app()->request->getParam('id_coupon'));
        if (!$coupon)
            $coupon = new EcommerceCoupon();
        $this->coupon = $coupon;

        // Set form values derived from coupon values
        if (!$coupon->isNewRecord) {
            if (!empty($coupon->valid_from) && !empty($coupon->valid_to)) {
                $this->validityType = EcommerceCoupon::VALIDITY_SPECIFIED;
                $this->coupon->valid_to = $this->coupon->valid_to;
                $this->coupon->valid_from = $this->coupon->valid_from;
            }

            $this->usageType = ($coupon->usage_count > 0)
                ? EcommerceCoupon::USAGE_LIMITED_USERS
                : EcommerceCoupon::USAGE_UNLIMITED_USERS;

            $this->useOnAllCoursesChecked = $coupon->can_have_courses ? 0 : 1;
            $this->minimumOrderChecked = (floatval($coupon->minimum_order) > 0) ? 1 : 0;
        } else {
            // Default values for new coupon
            $this->validityType = EcommerceCoupon::VALIDITY_ALWAYS;
            $this->usageType = EcommerceCoupon::USAGE_UNLIMITED_USERS;
            $this->useOnAllCoursesChecked = 0;
            $this->minimumOrderChecked = 0;
        }
    }


    /**
     * Saves coupon form to DB
     * @return bool
     */
    public function save() {
        // Fill in form and coupon attributes
        $this->attributes = $_POST['EcommerceCouponForm'];
        $this->coupon->attributes = $_POST['EcommerceCoupon'];

        // Reset missing checkbox values
        if(!isset($_POST['EcommerceCouponForm']['useOnAllCoursesChecked']))
            $this->useOnAllCoursesChecked = 0;
        if(!isset($_POST['EcommerceCouponForm']['minimumOrderChecked']))
            $this->minimumOrderChecked = 0;

        // Validation
        if (!$this->coupon || !$this->coupon->validate())
            return false;

        // Handle validity
        if ($this->validityType == EcommerceCoupon::VALIDITY_ALWAYS) {
            $this->coupon->valid_from   = null;
            $this->coupon->valid_to     = null;
        } else {
        	// Modify the TO time to be "end of the day", adding 23:59:59 at the end
        	$utc = Yii::app()->localtime->fromLocalDate($this->coupon->valid_to) . " 23:59:59";
        	$this->coupon->valid_to = Yii::app()->localtime->toLocalDateTime($utc);
        }

        $this->coupon->can_have_courses = $this->useOnAllCoursesChecked ? 0 : 1;
        if ($this->usageType == EcommerceCoupon::USAGE_UNLIMITED_USERS)
            $this->coupon->usage_count = 0;

        // handle min order
        if ($this->minimumOrderChecked) {
            if (!floatval($this->coupon->minimum_order))
                return false;
        } else {
            $this->coupon->minimum_order = 0;
        }

	    if($this->coupon->discount_type == EcommerceCoupon::DISCOUNT_TYPE_PERCENT ){
		    if($this->coupon->discount < 0){
			    $this->coupon->discount = 0;
		    }elseif($this->coupon->discount > 100){
			    $this->coupon->discount = 100;
		    }
	    }elseif($this->coupon->discount_type == EcommerceCoupon::DISCOUNT_TYPE_AMOUNT){
		    if($this->coupon->discount < 0){
			    $this->coupon->discount = 0;
		    }
	    }

        return $this->coupon->save();
    }
} 