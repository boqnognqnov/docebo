<?php
/**
 *
 * This model is used to save the state between CSV Users import UI steps and between Progressive Importer runs (chunked import)
 * Stored in session as a whole object.
 *
 *
 */
class UserImportForm extends CFormModel {

	const SCENARIO_ACTIVATION = 'activation';
	/**
	 * Various model attributes
	 * @var mixed
	 */
	public $file; 								// PHP temporary file (CUploadedFile instance)
	public $localFile;  						// locally saved, temporary copy of the uploaded file (in upload_tmp)
	public $originalFilename;					// Just the original name of the uploaded file. Name only! Not the path!
	public $separator = 'auto';					// CSV separator (1st Step UI option)
	public $manualSeparator;
	public $firstRowAsHeader = true;			// Consider first row in CSV as a header? (1st Step UI option)
	public $charset = 'UTF-8'; 					// charset of csv file (1st Step UI option)
	public $node;								// Branch/Orgchart ID (1st Step UI option)
	public $passwordChanging = 'no';			// Force imported users to change their password on their first login? (1st Step UI option)
    public $branchOption = 'existing';
	public $sendAlertToUsers = true;
	public $insertUpdate = false;				// Update users if they exist? (2nd Step UI option)
	public $totalUsers = 0;						// Keep total number of users in the imported CSV, counted at the first run of importer
	public $failedLines = array();   			// Collecta failing users for a single run; reset before call saveUsers() please
	public $cumulativeFailedLines = array();	// Collects ALL failed lines for ALL runs of the progressive import (session!)
	public $powerUserBranches = array();		// Keeps the list of Power User branches (if current user is PU anyway)
	public $successCounter	= 0;
	public $unenroll_deactivated = false;		// Unenroll user from all Classroom and Webinar sessions if the valid is "false"
	public $activation = '';					// Activate / Deactivate users from import
	public $importTabType = '';
    public $ignorePasswordChangeForExistingUsers = false; // Ignore Force password change for Already Existing Users
	public $autoAssignBranches =  true;         //When importing Power Users if the branch in the file should be assigned or not


	public static $charsets = array(
		'Windows-1252'	=> 'ANSI (Windows-1252)',
		'BIG-5'			=> 'BIG-5',
		'UCS-2'			=> 'UCS-2',
		'UCS-2BE'		=> 'UCS-2 Big Endian',
		'UCS-2LE'		=> 'UCS-2 Little Endian',
		'UTF-8'			=> 'UTF-8',
		'UTF-16'		=> 'UTF-16',
		'UTF-16BE'		=> 'UTF-16 Big Endian',
		'UTF-16LE'		=> 'UTF-16 Little Endian'
	);

	protected $_data;							// Array of CSV lines, e.g. users

	/**
	 * Holds the map between CSV fields and user fields (or user virtual attributes_
	 * @var Array
	 */
	protected $_importMap = array(
		'userid',
		'firstname',
		'lastname',
		'email',
		'new_password',
	);

	/**
	 * Returns a json representation of this form (to be serialized)
	 * @return string
	 */
	public function getParams() {
		return array(
			'originalFilename' => $this->originalFilename,
			'separator' => $this->separator,
			'manualSeparator' => $this->manualSeparator,
			'firstRowAsHeader' => $this->firstRowAsHeader,
			'charset' => $this->charset,
			'node' => $this->node,
			'passwordChanging' => $this->passwordChanging,
            'branchOption' => $this->branchOption,
			'insertUpdate' => $this->insertUpdate,
			'totalUsers' => $this->totalUsers,
			'failedLines' => $this->failedLines,
			'cumulativeFailedLines' => $this->cumulativeFailedLines,
			'successCounter' => $this->successCounter,
			'importMap' => $this->_importMap,
			'unenroll_deactivated' => $this->unenroll_deactivated,
			'activation' => $this->activation,
            'ignorePasswordChangeForExistingUsers' => $this->ignorePasswordChangeForExistingUsers,
			'autoAssignBranches' => $this->autoAssignBranches
		);
	}

	protected $_maxFileSize = 3145728; // 314572800 = 300 Mb

	protected $_createdUsers = false;

	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			array(
				'file',
				'file',
				'on' => 'step_one',
				'maxSize' => $this->_maxFileSize,
				'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
				'types' => array('csv'),
				'wrongType' => 'File type should be in "csv" format',
			),
			array('file', 'file', 'on' => 'step_one',
				'mimeTypes' => array(
					'text/x-comma-separated-values',
					'text/comma-separated-values',
					'application/octet-stream',
					'application/vnd.ms-excel',
					'text/csv',
					'text/plain',
					'application/csv',
					'application/excel',
					'application/vnd.msexcel',
					//NOTE: sometimes due to an error of mime-type interpreter, csv text content is erroneously seen as C or Assembler code,
					// so we have included those types in the list. The most important thing is that we have a true text file with csv
					// extension, whatever text is contained (if content is not a valid csv, then simply nothing can be imported from that file).
					'text/x-c',
					'text/x-asm'
				),
				'wrongMimeType' => 'File type should be in "csv" format',
				'skipOnError' => true,
			),
			array('importMap', 'checkImportMap', 'on' => 'step_two'),
			array('separator, manualSeparator, firstRowAsHeader, charset, node, passwordChanging, sendAlertToUsers, insertUpdate, ,$unenroll_deactivated, activation, autoAssignBranches', 'safe'),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels() {
		return array(
			'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
			'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
			'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
			'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET'),
			'node' => Yii::t('user_management', '_IMPORT_FORM_NODE'),
			'branchOption' => Yii::t('user_management', '_BRANCH_OPTIONS'),
			'passwordChanging' => Yii::t('admin_directory', '_FORCE_PASSWORD_CHANGE'),
			'sendAlertToUsers' => Yii::t('user_management', '_IMPORT_FORM_SENDALERTTOUSERS'),
			'insertUpdate' => Yii::t('user_management', '_IMPORT_FORM_INSERTUPDATE'),
			'activation' => Yii::t('standard', 'Action to perform'),
            'ignorePasswordChangeForExistingUsers' => Yii::t('standard', 'Ignore force password change for existing users'),
			'autoAssignBranches' =>  Yii::t('standard', 'Auto assign branches to all power users'),
		);
	}


	/**
	 *
	 * @param string $numUsers
	 * @return CArrayDataProvider
	 */
	public function dataProvider($numUsers=false) {

		$items = $this->getData($numUsers);

		if ($this->firstRowAsHeader) {
			unset($items[0]);
		}

		$dp = new CArrayDataProvider($items, array(
			'pagination' => array('pageSize' => 10),
		));

		return $dp;
	}


	/**
	 * _importMap setter
	 *
	 * @param array $importMap
	 * @return boolean
	 */
	public function setImportMap($importMap) {
		if (is_array($importMap)) {
			$this->_importMap = $importMap;
			return true;
		}
		return false;
	}

	/**
	 * _importMap getter
	 *
	 * @return array:
	 */
	public function getImportMap() {
		return $this->_importMap;
	}

	/**
	 *
	 * @param string $type
	 * @return number
	 */
	public function getMaxFileSize($type = 'B') {
		switch ($type) {
			case 'B':
				return $this->_maxFileSize;
				break;
			case 'KB':
				return (int) ($this->_maxFileSize / 1024);
				break;
			case 'MB':
				return (int) ($this->_maxFileSize / (1024 * 1024));
				break;
		}
	}

	/**
	 * Import map validator
	 *
	 * @param string $attribute
	 * @param array $params
	 */
	public function checkImportMap($attribute, $params) {
		$selectedItems = array();
		foreach ($this->$attribute as $item) {
			if ($item != 'ignoredfield') {
				if (!in_array($item, $selectedItems)) {
					$selectedItems[] = $item;
				} else {
					$this->addError('importMap', Yii::t('standard', 'Duplicated field(s)'));
					break;
				}
			}
		}
		if (empty($selectedItems)) {
			$this->addError('importMap', Yii::t('standard', 'You must select field(s)'));
		}
	}



	public function getCreatedUsers() {
		return $this->_createdUsers;
	}


	public function addCreatedUsers($users) {
		if (!is_array($this->_createdUsers)) {
			$this->_createdUsers = array();
		}
		if (is_numeric($users) && (int)$users > 0) {
			$this->_createdUsers[] = (int)$users;
		}
		if (is_array($users) && !empty($users)) {
			$this->_createdUsers = array_merge($this->_createdUsers, $users);
		}
	}


	public function resetCreatedUsers() {
		$this->_createdUsers = false;
	}




	/**
	 * Enumerate all users in _data and save them (create/update etc.)
	 *
	 * @param string $validate  Validate this model data?
	 * @param string $skipHeader Skip first row IF it is declared a as a header?
	 * @throws CException
	 * @return boolean
	 */
	public function saveUsers($validate=true, $skipHeader=true, $startLineIndex = 0) {

 		if ($validate && !$this->validate()) {
			return false;
		}

		$data = $this->_data;

		if ($skipHeader && $this->firstRowAsHeader) {
			$startLineIndex++;
			unset($data[0]);
		}

		foreach ($this->_importMap as $i => $name) {
			if ($name == 'ignoredfield') {
				unset($this->_importMap[$i]);
			}
			if ($name == 'userid') {
				$useridKey = $i;
			}
		}

		// validation for required fields
		$tempUser = new CoreUser('import');
		foreach ($tempUser->errors as $key => $value) {
			if (!in_array($key, $this->_importMap)) {
				$this->addError('user_all_field_'.$key, $tempUser->getError($key));
			}
		}

		$users = array();
		$valid = !$this->hasErrors();
		$lineIndex = 0;


		/*
		 * The big cycle: enumerating all data rows (a portion of CSV)
		 *
		 * Single user operations are enclosed in try/cacth and a DB transaction.
		 *
		 * Process tries to keep running all the way to the end of the data set, collecting errors on its way, but not interrupting the whole import.
		 *
		 * Valid user data (lines) result in a successfully inserted/updated users. With that said, we can have PARTIALY successful import.
		 *
		 * Bad/Failed lines (having errors) are collected in $this->filedLines (per chunk) and in $cumulativeFailedLines (for the whole CSV being imported)
		 *
		 * Existing users are skipped in not being in "update" mode.
		 *
		 * Branch, requested by the import takes higher priority then the general optionfrom step 1.
		 *
		 * Incoming users are checked for existence in CoreUserTemp table and are rejected as this may create bad mess.
		 *
		 */


		// Root Branch
		$rootBranch = CoreOrgChartTree::model()->getOrgRootNode();

		$existingUsersCounter = 0;
		$notManageUsersCounter = 0;

		// Reset failed lined
		$this->failedLines = array();

		$lineIndex = $startLineIndex;

 		foreach ($data as $item) {

 			// Collects all possible error texts fo the user
 			$userSummaryErrorTexts = array();
 			$perUserBranchId = array();
 			$perUserBranchName = '';
			$is_update = false;
			$puGroup = null;
		    $branch = null;

 			$lineIndex++;

			//sometimes CSVs can contain empty lines which will result in confusing errors. Just avoid them.
			if (!is_array($item) || empty($item) || (count($item) <= 0) || (count($item) == 1 && empty($item[0]))) {
				continue;
			}

 			$transaction = Yii::app()->db->beginTransaction();

 			try {
				$user = null;
				$newUser = false;
				$userCheckModel = null;

				$badLine = !is_array($item) || (count($item) <= 0) || empty($item[0]);
				if ($badLine) {
					throw new CException('Invalid user line (probably empty): #' . $lineIndex, 601);
				}


				// If we have Username (userid) in the CSV file, lets try to get a model of a user, if it exists
				if (isset($useridKey)) {
					$userCheckModel = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($item[$useridKey])));
				}

				// the import is from second tab, so just activate/deactivate the user
				if(!empty($this->activation) && $this->importTabType == self::SCENARIO_ACTIVATION) {

					if($userCheckModel) {
						$canEdit = Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsAdmin() && CoreUserPU::isAssignedToPU($userCheckModel->idst));
						if ($canEdit === true) {
							if ($this->activation === 'yes') {
								$userCheckModel->valid = 1;
							} elseif ($this->activation === 'no') {
								$userCheckModel->valid = 0;
							}
							$userCheckModel->save(false);
							$transaction->commit();
							$this->successCounter++;
						}else{
							throw new CException('Cannot manage this user', 605);
						}
					}else{
						throw new CException('User not exists', 604);
					}
					continue;
				}

				// Are we going to UPDATE user by user ID?
				if ($this->insertUpdate && isset($useridKey)) {
					$user = $userCheckModel;
				} else {
				// If not, check if this user already exists and throw an exception to cancel improting this user and move to the next one
				// NOTE!!!: This is NOT an error, we assign code 601 to indicate this, so exception catcher to be able to analyze and count existing users!
					if ($userCheckModel) {
						throw new CException('User already exists [' . ltrim($userCheckModel->userid,'/') . ']', 601);
					}
				}


				// Protect CoreUserTemp table (waiting users)
				// Also, lets check if user exists in Core User Temp table, where users are waiting to be approved.
				// We definitely can NOT Insert OR Update Temporary users, that will create a mess.
				// This might be a result of previous import run. Lets not consider it an error, but still log a message.
				// Again, Code = 601 is assigned to advise the exception catcher about non-breaking error
				$userTempCheckModel = CoreUserTemp::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($item[$useridKey])));
				if ($userTempCheckModel) {
					throw new CException('Temporary User already exists [' . ltrim($userTempCheckModel->userid,'/') .  ']', 601);
				}


				// Ok, we must have a nice CoureUser model, lets set it
				if($user)
					$is_update = true;
				else
					$user = new CoreUser('import');

				$user->scenario = 'import';

				// Handle Additional fields
				$additionalFields = array();
				$userLang = false;
				$currentLangCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
				$userStatus = null;

				/**
				 * E.g. '/framework/level/admin' or null (regular user level)
				 */
				$userLevel = null;

				/**
				 * If $userLevel is set to '/framework/level/admin' (Power User)
				 * this variable will be populated with the incoming PU profile ID
				 * if a PU profile is provided in the CSV. Otherwise null.
				 */
				$puProfileGroupID = null;
				// If the user has selected that the flag for "forcePasswordChange" should be checked from the CSV and not from the General option
				$csvForcePasswordChange = false;

				foreach ($this->_importMap as $i => $name) {
					if (property_exists(get_class($user), $name) || $user->hasAttribute($name)) {

					    // Check if the Property that is coming from the CSV Columns if the force_change password
						if ($name == 'force_change') {

						    // Skip the Column of Overriding the User's previous value for force_change password
                            // If the settings was checked and the user is not new user
						    if( $this->ignorePasswordChangeForExistingUsers && $user->getIsNewRecord() === false ) {
						        continue;
                            }

							// If the value is SET then this value should override the general Options for Force password change
							if( isset($item[$i]) ) {
								$csvForcePasswordChange = true;
							}
						}

						$user->$name = trim($item[$i]);
					} else if($name  == 'suspend') {
						$value = strtolower(trim($item[$i]));
						if($value)
							$userStatus = $value == "true" ? 0 : ($value == "false" ? 1 : null);
					} else if($name == 'language') {
						$userLang = $item[$i];
					} else if($name=='user_level'){
						switch($item[$i]){
							case "poweruser":
								if(Yii::app()->user->getIsGodadmin())
									$userLevel = Yii::app()->user->level_admin; // Power user level
							break;
						}
					} else if ($name == 'pu_profile') {
						if ($item[$i]) {
							// Try to find the PU group idst by its name (from the CSV cell value)
							$puProfileGroupID = Yii::app()->getDb()->createCommand()
								->select('idst')
								->from(CoreGroup::model()->tableName())
								->where('groupid LIKE :search', array(':search' => '/framework/adminrules/' . $item[$i] . ''))
								->order('idst ASC')
								->queryScalar();
							if ($puProfileGroupID === false)
								throw new CException('Not existing profile [' . $item[$i] . ']', 603);
						}
					} else if  ($name == 'branch') {
						// Branch is a new feature, virtual property of the user, to allow assigning users to a branch upon importing
						$requestedBranchName = trim($item[$i]);

						if (!empty($requestedBranchName)) {
							// Check if branch name is valid, in current LMS language
							$branch = CoreOrgChartTree::getBranchByName($requestedBranchName);

							if (!$branch) {
								throw new CException(Yii::t('standard', "Branch does not exists [$requestedBranchName]"), 602);
							}
							else if ($rootBranch->idOrg == $branch->idOrg) {
								throw new CException(Yii::t('standard', "Assign to root branch not allowed"), 602);
							}
							if(Yii::app()->user->getIsAdmin()) {
								$puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();
								if(!in_array($branch->idOrg, $puBranchIdsArray))
									throw new CException(Yii::t('standard', "Branch does not exists [$requestedBranchName]"), 602);
							}
							// Ok, we found the branch, ask the code below to use it
							$perUserBranchId[] 	= (int) $branch->idOrg;
							$perUserBranchName 	= $requestedBranchName;
						}
					} else if  ($name == 'branch_code') {
						// Branch is a new feature, virtual property of the user, to allow assigning users to a branch upon importing
						$requestedBranchCode = trim($item[$i]);

						if (!empty($requestedBranchCode)) {
							// Check if branch name is valid, in current LMS language
							$branch = CoreOrgChartTree::model()->findByAttributes(array('code' => $requestedBranchCode));

							if (!$branch) {
								throw new CException(Yii::t('standard', "Branch does not exists [$requestedBranchCode]"), 602);
							}
							else if ($rootBranch->idOrg == $branch->idOrg) {
								throw new CException(Yii::t('standard', "Assign to root branch not allowed"), 602);
							}
							if(Yii::app()->user->getIsAdmin()) {
								$puBranchIdsArray = CoreOrgChartTree::getPuBranchIds();
								if(!in_array($branch->idOrg, $puBranchIdsArray))
									throw new CException(Yii::t('standard', "Branch does not exists [$requestedBranchCode]"), 602);
							}
							// Ok, we found the branch, ask the code below to use it
							$perUserBranchId[] 	= (int) $branch->idOrg;
							$perUserBranchName 	= $requestedBranchCode;
						}
					} else { // Looks like an additional field.
						// $name is the id_common of the core_field
						// $item[$i] is the additional field value (i.e. user entry; could be the REAL entry, or pointing to core_field_son)
						$idCommon = $name;
						$userEntry = trim($item[$i]);

						// Get the field type by its idCommon field
						$fieldType = CoreUserField::getFieldTypeById($idCommon);

						// Different types require different interpretation of the imported data
						// Please do NOT try to optimize the code! Yes, it is kinda repeating, but keep it simple! Please!
						switch ($fieldType) {
							case CoreUserField::TYPE_DROPDOWN:
                                $additionalFields[$idCommon] = $userEntry;
								// Check if the incoming value is numeric (assuming it represents internal value ID)
								// If it is NOT numeric, we treat it as String and.. search the DB for ID to use
								if (!is_numeric($userEntry)) {
									$id = CoreUserFieldDropdown::getOptionValueByLabel($userEntry, $idCommon, $currentLangCode);
									if ($id) {
										$additionalFields[$idCommon] = $id;
									}
								}
								break;

							case CoreUserField::TYPE_COUNTRY:
								$additionalFields[$idCommon] = $userEntry;
								// Check if the incoming value is numeric (assuming it represents internal value ID)
								// If it is NOT numeric, we treat it as String and.. search the DB for ID to use
								if (!is_numeric($userEntry)) {
									$id = CoreCountry::getIdCountryByName($userEntry);
									if ($id) {
										$additionalFields[$idCommon] = $id;
									}
								}
								break;

							case CoreUserField::TYPE_YESNO:
								// NOTE:  Yes ==> 1, No ==> 2
								// Allow case insensitive yes/no
								if (strtolower($userEntry) == 'yes') {
									$userEntry = 1;
								}
								else if (strtolower($userEntry) == 'no') {
									$userEntry = 2;
								}
								// Also, allow ZERO as NO
								else if ($userEntry == '0') {
									$userEntry = 2;
								}
								$additionalFields[$idCommon] = $userEntry;
								break;

							// By default, what is in the CSV file is what should go into the user entry (e.g. text)
							default:
								$additionalFields[$idCommon] = $userEntry;
						}

					}
				}

				// Finally, Apply values to additional fields
				foreach ($user->additionalFields as $i => $field) {
					// Insert or update ONLY fields that are part of the import map
					// Additional fields in import map are refered by their common ID (integer)
					if (in_array($field->id_field, $this->_importMap)) {
						$user->additionalFields[$i]->userEntry = $additionalFields[$field->id_field];
					}
				}

				// Check if we have to set the valid/not valid status
				if(!is_null($userStatus)) {
					$user->valid = $userStatus;
				}

				// Set the force-change Password policy
				// Check if we should ignore the force password change for new users
				if( $this->ignorePasswordChangeForExistingUsers && $user->getIsNewRecord() === false ) {
				    // We MUST totally skip the setting of any force_change password flag
                } else {
                    $forcePasswordChange = $csvForcePasswordChange === true ? $user->force_change : null;
                    $user->force_change = $this->resolvePasswordChanging($forcePasswordChange);
                }

				if ($is_update) {
					//user is being updated: do nothing with its last enter date, leave it as it is !
				} else {
					// set to false rather than NULL (or else it will be replaced with CURRENT_TIMESTAMP)
					// and we don't want lastenter to be set when importing users
					$user->lastenter = false;
				}

				// Resolve Branch
				// If the user select any branch on second step put user into that branch
				if (!empty($perUserBranchId)) {
					$user->chartGroups = array_merge($user->chartGroups, $perUserBranchId);
				}

				// If the user has selected a branch on the first step, we will import the users into that branch
				if ($this->node != -1) {
					$user->chartGroups[] = $this->node;
				}

				// If we still didn't associate the user to certain branch he goes at the root
				if ($user->getIsNewRecord()) {
					$user->chartGroups[] = $rootBranch->idOrg;
				}

				// Now apply Power User limitation: PU can not assign users to a branch he is not assigned to
				if (Yii::app()->user->getIsPu() && $this->node != -1) {

					$puNodes = CoreOrgChartTree::getPuBranchIds();
					//NOTE: when reading PU assigned nodes, root node may not be included, but anyway required for this specific check. Add it manually.
					if (!in_array($rootBranch->idOrg, $puNodes)) { $puNodes[] = $rootBranch->idOrg; }

					$intersect = array_intersect($user->chartGroups, $puNodes);
					if (empty($intersect)) {
						throw new CException("Branch does not belong to power user", 602);
					}
				}


				// Creating NEW user with NO password is NOT allowed. Throw an exception
				if ($user->isNewRecord && !$user->new_password) {
					throw new CException(Yii::t('standard', "Password is required"), 602);
				}


				// Now, Validate the user data
				$validUser = $user->validate();

				// Collect (cumulative) errors into THIS class attribute. It will contain all errors
				if ($user->hasErrors()) {

					$additionalFieldIds = array();
					foreach ($user->getAdditionalFields() as $field) {
						$additionalFieldIds[] = $field->id_field;
					}

					$modelErrorTexts = array();
					// Enumerate all [user] errors and add them to an array
					foreach ($user->errors as $key => $errors) {
						if ($key !== 'userid') {
							foreach ($errors as $error) {
								$this->addError('user_'.$lineIndex.'_field_'.$key, $error);
								$modelErrorTexts[] = $error;
							}
						}
					}
				}

				$user->clearErrors('userid');

				// If user did not pass the validation, throw an exception, BUT pass a "non-breaking" code: 602
				if (!$validUser) {
					throw new CException(implode('|', $modelErrorTexts), 602);
				}
				else  {

                    $isNewRecord = $user->getIsNewRecord();
					$saveResult = $user->save();

					if ($saveResult) {

						if($this->unenroll_deactivated && !$user->valid) {
							$this->unenrollUserFromAllSessions($user->idst);
						}

						$this->addCreatedUsers($user->idst);

						/*
						 * If the user didn't select a branch on the first step
						 * and if he didn't select a branch column in the second step
						 * then do nothing
						 */
						if ($this->node == -1 && empty($perUserBranchId)) {
							// do nothing here
							// ... but all created users must be assigned to the org. chart root node anyway
							if ($isNewRecord) {
								$originalUserChartGroups = $user->chartGroups;
								$user->chartGroups = array($rootBranch->idOrg);
								$user->saveChartGroups();
								$user->chartGroups = $originalUserChartGroups;
							}
						} else {
							// Otherwise update the user selected branches
							$user->saveChartGroups();
							$newNodes = $user->chartGroups;
							if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
								unset($newNodes[$key]);
							}
							foreach ($newNodes as $node) {
								Yii::app()->event->raise(CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH, new DEvent($this, array(
									'targetUser' => $user->idst,
									'BranchIdOrg' => $node
								)));
							}
						}


						switch($userLevel){
							case Yii::app()->user->level_admin:

								//retrieve old user level and check it
								$canChangeUserLevel = true;
								$levelsList = Yii::app()->user->getUserLevelIds();
								$flippedLevelsList = array_flip($levelsList);
								$levelIds = (is_array($levelsList) ? array_keys($levelsList) : array()); //try to avoid any possible fatal error here
								if (!empty($levelIds)) { //as above
									$queryParams = array(':idst_member' => $user->idst);
									$query = "SELECT * FROM " . CoreGroupMembers::model()->tableName() . " WHERE idstMember = :idst_member AND (";
									$counter = 1;
									foreach ($levelIds as $levelId) {
										if ($counter > 1) { $query .= " OR "; }
										$query .= "idst = :group_idst_$counter";
										$queryParams[':group_idst_'.$counter] = $levelId;
										$counter++;
									}
									$query .= ") ORDER BY idst DESC"; //NOTE: we rely in the fact that user levels have progressive IDs assigned, with higher level having lesser ID
									$userLevelInfo = Yii::app()->db->createCommand($query)->queryAll(true, $queryParams);
									$userLevelSingle = false;
									if (is_array($userLevelInfo) && count($userLevelInfo) >= 1) {
										// prevent the possibility that multiple levels may be assigned to the user
										// NOTE: user MUST have ONLY ONE level assigned, otherwise some error/bug occurred elsewhere
										$userLevelSingle = $userLevelInfo[0];
									} else {
										// in this specific case, the user has not an assigned level. We have nothing else to do here, but
										// keep in mind that this is an inconsistent situation and should never happen, because it means
										// that something is broken elsewhere !!
									}
									//check the old user level
									if (!empty($userLevelSingle)) {
										// if we are power users and we are required to change the level of a super admin into power user,
										// make sure that this action is prevented
										if (Yii::app()->user->getIsPu() && $userLevelSingle['idst'] == $flippedLevelsList[Yii::app()->user->level_godadmin]) {
											$canChangeUserLevel = false;
										} else {
											//remove old level for the target users, new one will be added just after
											$queryParams = array(':idst_member' => $user->idst);
											$deleteLevelQuery = "DELETE FROM " . CoreGroupMembers::model()->tableName() . " "
												. " WHERE idstMember = :idst_member AND (";
											$counter = 1;
											foreach ($levelIds as $levelId) {
												if ($counter > 1) {
													$deleteLevelQuery .= " OR ";
												}
												$deleteLevelQuery .= "idst = :group_idst_$counter";
												$queryParams[':group_idst_' . $counter] = $levelId;
												$counter++;
											}
											$deleteLevelQuery .= ")";
											Yii::app()->db->createCommand($deleteLevelQuery)->execute($queryParams);
										}
									}
								}

								if ($canChangeUserLevel) {
									// Change the newly created user's level to Power User
									$puGroup = CoreGroup::model()->findByAttributes(array('groupid' => $userLevel));
									if ($puGroup) {
										$puLevelAssign = new CoreGroupMembers();
										$puLevelAssign->idst = $puGroup->idst;
										$puLevelAssign->idstMember = $user->idst;
										$puLevelAssign->save();

										// Sync the User Role with the Hydra RBAC
										HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $puGroup->idst ));
									}

									if($puProfileGroupID)
									{
										// And assign a PU profile
										// (a collection of PU permissions) to him
										$puProfileAssign = CoreGroupMembers::model()->getPowerUserProfile($user->idst);
										if(!$puProfileAssign)
											$puProfileAssign = new CoreGroupMembers();

										$puProfileAssign->idst = $puProfileGroupID;
										$puProfileAssign->idstMember = $user->idst;
										$puProfileAssign->save();

										// Sync the User Role with the Hydra RBAC
										HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $puProfileGroupID ), true);
									}
								}

								//If the branch columns is inserted ($branch will the object of the given branch) and the current user is PU, assign this branch to him
								if ($branch && !(isset($this->autoAssignBranches) && !$this->autoAssignBranches)) {
									// Assign the branch to this user
									CoreAdminTree::updatePowerUserSelection($user->idst, array($branch->idst_oc));
								}

								//If there is a branch set in the initial import page and the  branch column is not inserted that branch will be assign to the PU
								if(($this->node != -1) && !(isset($this->autoAssignBranches) && !$this->autoAssignBranches) ){
									$initialBranch = CoreOrgChartTree::model()->findByAttributes(array('idOrg'=>$this->node));
									if($initialBranch && !$branch){
										// Assign the branch to this user
										CoreAdminTree::updatePowerUserSelection($user->idst, array($initialBranch->idst_oc));
									}
								}

								break;
						}


						$usersGroup = CoreGroup::model()->find('groupid = :group', array(
							':group' => Yii::app()->user->level_user
						));

						if (!CoreGroupMembers::model()->countByAttributes(array(
								'idst' => array_keys(Yii::app()->user->getUserLevelIds()),
								'idstMember' => $user->idst,
							)) && $usersGroup) {
							$newGroupMember = new CoreGroupMembers();
							$newGroupMember->idst = $usersGroup->idst;
							$newGroupMember->idstMember = $user->idst;
							$newGroupMember->save();
						}

						// IF the user is not assigned as power user ONLY THEN we should assign him as normal user
						if ( !$puGroup ) {
							// Sync the User Role with the Hydra RBAC
							HydraRBAC::manageUserToRole(array( "user" => $user->idst, "group" => $usersGroup->idst ));
						}

						// The CSV file contained a column with languages and the current
						// row/user has a value for that column
						$newSettingUser = CoreSettingUser::model()->findByAttributes(array(
							'path_name' => 'ui.language',
							'id_user' => $user->idst,
						));
						if (!$newSettingUser)
						{
							$newSettingUser = new CoreSettingUser();
							$newSettingUser->path_name = 'ui.language';
							$newSettingUser->id_user = $user->idst;
							$newSettingUser->value = $userLang ? strtolower($userLang) : CoreLangLanguage::getDefaultLanguage();
							$newSettingUser->save();
						}
						elseif($userLang)
						{
							$newSettingUser->value = $userLang ? strtolower($userLang) : CoreLangLanguage::getDefaultLanguage();
							$newSettingUser->save();
						}

						if ($is_update) {

						}
						else
						{
							// Raise event
							$user->passwordPlainText = $user->new_password;
							$user->setScenario('import'); //make sure to have the correct scenario set
							Yii::app()->event->raise('NewUserCreated', new DEvent($this, array(
								'user' => $user,
							)));
						}

					}

					// POWER USER CASE:
					// Save was not successful, but maybe because it was intercepted by the fact that current user is
					// a Power User having NO permission to directly insert users?? (in which case model scenario is modified on return)
					// If that's the case, at this point a TEMP user is already created and saved!!
					else if ($user->scenario == 'powerUserCreateTempUser') {

						if (!CoreGroupMembers::model()->countByAttributes(array(
								'idst' => array(3, 4, 6),
								'idstMember' => $user->idst,
						))) {
							$newGroupMember = new CoreGroupMembers();
							$newGroupMember->idst = 6;
							$newGroupMember->idstMember = $user->idst;
							$newGroupMember->save();
						}
						if($userLang){
							$tempUser = CoreUserTemp::model()->findByPk($user->idst);
							$tempUser->language = $userLang ? strtolower($userLang) : CoreLangLanguage::getDefaultLanguage();
							$tempUser->save();
						}

						if ($is_update) {

						}
						else
						{
							// Raise event
							$user->passwordPlainText = $user->new_password;
							Yii::app()->event->raise('NewUserCreated', new DEvent($this, array(
							'user' => $user,
							)));
						}
					}

					$this->successCounter++;

					// Everything related to this user was successful, COMMIT database changes please
					$transaction->commit();
				}
 			}

 			// Catch exceptions
 			catch (CException $e) {
 				// User already exists code = 601
 				if ($e->getCode() == 601) {
 					$userSummaryErrorTexts[] = $e->getMessage();
 					$existingUsersCounter++;
 					$transaction->commit();
 					Yii::log('Importing user: ' . $e->getMessage(), 'error');
 				}
 				// Other non-breakable error = 602
 				else if ($e->getCode() == 602) {
 					$userSummaryErrorTexts[] = $e->getMessage();
 					$transaction->commit();
 					Yii::log('Importing user: ' . $e->getMessage(), 'error');
 				// Power User profile don't exists code = 603
 				} else if ($e->getCode() == 603) {
					$userSummaryErrorTexts[] = $e->getMessage();
					$transaction->commit();
					Yii::log('Importing user: ' . $e->getMessage(), 'error');
				} else if ($e->getCode() == 604) {
					$userSummaryErrorTexts[] = $e->getMessage();
					$existingUsersCounter++;
					$transaction->rollback();
					Yii::log('Importing user: ' . $e->getMessage(), 'error');
				} else if ($e->getCode() == 605) {
					$userSummaryErrorTexts[] = $e->getMessage();
					$notManageUsersCounter++;
					$transaction->rollback();
					Yii::log('Importing user: ' . $e->getMessage(), 'error');
				} else {
 				// Otherwise...
 					Yii::log($e->getMessage(), 'error');
					$transaction->rollback();
					return false;
				}
			}


 			// We Finished handling a given user. If errors have been collected for THIS user, add it to Failed list, together with all the errors as
 			// the first field, enclosed in "" and separated by a pipe |)
 			// All these failed users can be used to report errors (save to file, show on screen, etc)
  			if (count($userSummaryErrorTexts) > 0) {
  				$tmpArray  	= array(implode('|', $userSummaryErrorTexts));
  				$tmpArray 	= array_merge($tmpArray, $item);
  				$this->failedLines[] = CSVParser::arrayToCsv($tmpArray);
  			}


		}



		// "Exising users" IS sort of an error; If we have some, lets add a SINGLE error about this, just to inform the user
		// Like "XXX users already exist"
		if ($existingUsersCounter > 0) {
			$this->addError('users_exist', Yii::t("standard", "{number} existing user(s) skipped", array('{number}' => $existingUsersCounter)));
		}

		// "Cannot manage users" IS sort of an error; If we have some, lets add a SINGLE error about this, just to inform the user
		if ($notManageUsersCounter > 0) {
			$this->addError('cannot_manage', Yii::t("standard", "{number} user(s) you cannot manage have been skipped", array('{number}' => $notManageUsersCounter)));
		}

		// If we've collected some errors...
		if ($this->hasErrors()) {
			return false;
		}

		return true;
	}



	/**
	 *
	 * @param string $numUsers
	 * @return array
	 */
	public function getData($numUsers=false) {

		if (!is_array($this->_data) || $numUsers) {
			$delimiter = '';
			switch ($this->separator) {
				case 'comma':
					$delimiter = ',';
					break;
				case 'semicolon':
					$delimiter = ';';
					break;
				case 'manual':
					$delimiter = $this->manualSeparator;
					break;
				case 'auto':
					$delimiter = 'auto';
					break;
			}
			$csvFile = new QsCsvFile(array(
				'path' => $this->file->tempName,
				'charset' => $this->charset,
				'delimiter' => $delimiter,
				'firstRowAsHeader' => $this->firstRowAsHeader,
				//'numLines' => $numUsers, // excluding header, if any
			));
			// NOTE: we cannot apply "numUsers" filter here because we don't know in advance how many invalid lines are to be excluded.
			//$this->totalUsers = $csvFile->getTotalLines();
			//$this->_data = $csvFile->getArray();


			$tmpData = $csvFile->getArray(); //raw reading of all CSV lines.

			//validate CSV lines
			foreach ($tmpData as $item) {

				$invalid = false;

				//clean empty lines
				if (empty($item) || (is_array($item) && count($item) == 1 && empty($item[0]))) {
					$invalid = true;
				}

				if (!$invalid) {
					$this->_data[] = $item;
				}
			}

			$this->totalUsers = count($this->_data);
			if ($this->firstRowAsHeader && $this->totalUsers > 0) { $this->totalUsers--; } //remove the first line from counting if it is header

			if ((int)$numUsers > 0 && count($this->_data) > $numUsers) {
				array_splice($this->_data, $numUsers); //remove all array portion after "$numUsers" offset
			}
		}

		return $this->_data;
	}

	protected function resolvePasswordChanging($forcePasswordChange) {

		// If the User Has selected Force change password for specific user from the CSV
		if($forcePasswordChange !== null) {

			$possibleValues = array("TRUE", "FALSE", "0", "1");
			$tmpValue = strtoupper($forcePasswordChange);
			// If the value is one of the possible values
			// then we should consider that the CSV row's values should override the general Option
			if(in_array($tmpValue, $possibleValues)) {
				// Depending on the Value that the user has entered, this values should be TRUE or FALSE
				return ($tmpValue == "TRUE" || $tmpValue == "1") ? 1 : 0;
			}
		}

 		// If the Column is not selected we rely on the Generic Force password change
		switch ($this->passwordChanging) {
			case 'no':
				return 0;
				break;
			case 'yes':
				return 1;
				break;
			case 'server':
				if (CoreSetting::model()->findByPk('pass_change_first_login')->param_value == 'on') {
					return 1;
				} else {
					return 0;
				}
				break;
			case 'nothing':
				// todo i/ maybe need to change
				return 0;
				break;
		}
		return false;
	}

    public static function getImportMapList() {
        $isHydraThemeActive = Yii::app()->legacyWrapper->hydraFrontendEnabled();
        $fields = array(
            'userid',
            'firstname',
            'lastname',
            'email',
            'new_password',
        );

        $list = array(
            'ignoredfield' => Yii::t('organization_chart', '_IMPORT_IGNORE'),
        );
        foreach ($fields as $value) {
            $list[$value] = CoreUser::model()->getAttributeLabel($value);
        }

        $list['force_change'] = Yii::t('standard', 'Force password change');
        $list['language'] = Yii::t('admin_directory', '_DIRECTORY_FILTER_language');

        if ($isHydraThemeActive) {
            $list['timezone'] = Yii::t('configuration', 'Time zone');
            $list['date_format'] = Yii::t('standard', '_DATE_FORMAT');
            $list['active'] = Yii::t('standard', 'Active');
            $list['expiration_date'] = Yii::t('standard', '_EXPIRATION_DATE');
            $list['password_hash'] = Yii::t('standard', '_PASSWORD_HASH');
        }

        if(PluginManager::isPluginActive('PowerUserApp')){
            $list['user_level'] = Yii::t('standard', '_LEVEL');
            $list['pu_profile'] = Yii::t('power_users', 'Power User Profile');
        }

        $list['branch'] = Yii::t('standard', '_ORGCHART');
        $list['branch_code'] = Yii::t('standard', '_ORGCHART') . ' - ' . Yii::t('standard', '_CODE');

        if ($isHydraThemeActive) {
            $list['branch_name_path'] = Yii::t('automation', 'Branch name path');
            $list['branch_code_path'] = Yii::t('automation', 'Branch code path');
        }

        $fields = CoreUserField::model()->language()->modify(function(CDbCriteria $criteria) {
            // array of attributes "type" for CoreUserField model
            $additionalFieldsExcept = array('upload');
            $criteria->addNotInCondition('type', $additionalFieldsExcept);
        })->getTranslations();

        /** @var CoreUser $currentUserModel */
        $currentUserModel = Yii::app()->user->loadUserModel();
        $visibleFields = $currentUserModel->getVisibleAdditionalFieldsByBranch(array());
        $visibleFieldIDs = array_keys($visibleFields);

        foreach ($fields as $field) {
            if (in_array($field['id_field'], $visibleFieldIDs)) {
                $list[$field['id_field']] = $field['translation'];
            }
        }

        return $list;
    }


	/**
	 * (non-PHPdoc)
	 * @see CModel::validate()
	 */
	public function validate($attributes=null, $clearErrors=true) {

		if ($this->node != -1) {
			// NOTE: the value -1 (meaning "Do nothing about orgchart assignment") applies to power users too. If a power user
			// creates users with this option, new users are created in root node (and assigned to the power user of course).
			// This looks legit, since single users can already be assigned to power users despite their orgchart placement.
			// IF THE IMPORT is for activation/deactivation, we don't need to choose branch, so in this case skip that check!
			if (!CoreUser::validatePowerAdminChartSelection(array($this->node))
					&& $this->importTabType != self::SCENARIO_ACTIVATION) {
				$this->addError('node', Yii::t('user_management', 'You are required to select a branch'));
				return false;
			}
		}

		return parent::validate($attributes, $clearErrors);
	}


	/**
	 * Set model _data array (users)
	 * @param unknown $data
	 */
	public function setData($data) {
		$this->_data = $data;
	}


	/**
	 * Translate human readable separator name to real delimiter character
	 * @param string $separatorName
	 * @return string
	 */
	public static function translateSeparatorName($separatorName, $manualSeparator = '') {
		$delimiter = '';
		switch ($separatorName) {
			case 'comma':
				$delimiter = ',';
				break;
			case 'semicolon':
				$delimiter = ';';
				break;
			case 'manual':
				$delimiter = $manualSeparator;//$this->manualSeparator;
				break;
			case 'auto':
				$delimiter = 'auto';
				break;
		}
		return $delimiter;
	}

	public static function getDropDownCharsets()
	{
		return self::$charsets;
	}

	private function unenrollUserFromAllSessions($idst){

		$classroomSessions = array();
		$webinarSessions = array();

		$params = array(
			':user' => $idst,
			':now' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
		);

		$classroomIds = Yii::app()->db->createCommand()
			->select('ls.id_session')
			->from(LtCourseSession::model()->tableName() . ' ls')
			->join(LtCourseuserSession::model()->tableName() . ' lsu', 'ls.id_session = lsu.id_session')
			->where('lsu.id_user = :user AND ls.date_end >= :now', $params)
			->queryAll();


		foreach($classroomIds as $sessId){
			$classroomSessions[] = $sessId['id_session'];
		}

		if(!empty($classroomSessions)){

			$condition = "id_session IN (" . implode(',', $classroomSessions) . ") AND id_user = :id_user";

			Yii::app()->db->createCommand()
				->delete(LtCourseuserSession::model()->tableName(), $condition, array(':id_user' => $idst));
		}

		$webinarIds = Yii::app()->db->createCommand()
			->select('ws.id_session')
			->from(WebinarSession::model()->tableName() . ' ws')
			->join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session = wsu.id_session')
			->where('wsu.id_user = :user AND ws.date_end >= :now', $params)
			->queryAll();


		foreach($webinarIds as $sessId){
			$webinarSessions[] = $sessId['id_session'];
		}

		if(!empty($webinarSessions)){

			$condition = "id_session IN (" . implode(',', $webinarSessions) . ") AND id_user = :id_user";

			Yii::app()->db->createCommand()
				->delete(WebinarSessionUser::model()->tableName(), $condition, array(':id_user' => $idst));
		}
	}
}