<?php
/**
 * 
 * This model is used to save the state between CSV Groups import UI steps and between Progressive Importer runs (chunked import)
 * Stored in session as a whole object.
 */
class GroupImportForm extends CFormModel {

	/**
	 * Various model attributes
	 * @var mixed
	 */
	public $file; 								// PHP temporary file (CUploadedFile instance)
	public $localFile;  						// locally saved, temporary copy of the uploaded file (in upload_tmp)
	public $originalFilename;					// Just the original name of the uploaded file. Name only! Not the path!
	public $separator = 'auto';					// CSV separator (1st Step UI option)
	public $manualSeparator;
	public $firstRowAsHeader = true;			// Consider first row in CSV as a header? (1st Step UI option)	
	public $charset = 'UTF-8'; 					// charset of csv file (1st Step UI option)

	public $totalItems = 0;						// Keep total number of items in the imported CSV, counted at the first run of importer
	public $failedLines = array();   			// Collect failing rows for a single run;
	public $successCounter	= 0;
	public $operationType = null;				// Type of operation ("assign" or "unassign")

	protected $_data;							// Array of CSV lines

	/**
	 * Holds the map for CSV columns
	 * @var Array
	 */
	protected $_importMap = array(
		'user_column' => 'id_user',
		'group_column' => 'id_group'
	);

	/**
	 * Returns a json representation of this form (to be serialized)
	 * @return string
	 */
	public function getParams() {
		return array(
			'originalFilename' => $this->originalFilename,
			'separator' => $this->separator,
			'manualSeparator' => $this->manualSeparator,
			'firstRowAsHeader' => $this->firstRowAsHeader,
			'charset' => $this->charset,
			'totalItems' => $this->totalItems,
			'failedLines' => $this->failedLines,
			'successCounter' => $this->successCounter,
			'importMap' => $this->_importMap,
			'operationType' => $this->operationType
		);
	}

	protected $_maxFileSize = 3145728; // 314572800 = 300 Mb

	/**
	 * @see CModel::rules()
	 */
	public function rules() {
		return array(
			array(
				'file',
				'file',
				'on' => 'step_one',
				'maxSize' => $this->_maxFileSize,
				'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
				'types' => array('csv'),
				'wrongType' => 'File type should be in "csv" format',
			),
			array('file', 'file', 'on' => 'step_one',
				'mimeTypes' => array(
					'text/x-comma-separated-values',
					'text/comma-separated-values',
					'application/octet-stream',
					'application/vnd.ms-excel',
					'text/csv',
					'text/plain',
					'application/csv',
					'application/excel',
					'application/vnd.msexcel',
					//NOTE: sometimes due to an error of mime-type interpreter, csv text content is erroneously seen as C or Assembler code,
					// so we have included those types in the list. The most important thing is that we have a true text file with csv
					// extension, whatever text is contained (if content is not a valid csv, then simply nothing can be imported from that file).
					'text/x-c',
					'text/x-asm'
				),
				'wrongMimeType' => 'File type should be in "csv" format',
				'skipOnError' => true,
			),
			array('importMap', 'checkImportMap', 'on' => 'step_two'),
			array('separator, manualSeparator, firstRowAsHeader, charset', 'safe'),
		);
	}
	
	/**
	 * _importMap setter
	 * 
	 * @param array $importMap
	 * @return boolean
	 */
	public function setImportMap($importMap) {
		if (is_array($importMap)) {
			$this->_importMap = $importMap;
			return true;
		}
		return false;
	}

	/**
	 * _importMap getter
	 * 
	 * @return array:
	 */
	public function getImportMap() {
		return $this->_importMap;
	}

	/**
	 * 
	 * @param string $type
	 * @return number
	 */
	public function getMaxFileSize($type = 'B') {
		switch ($type) {
			case 'B':
				return $this->_maxFileSize;
				break;
			case 'KB':
				return (int) ($this->_maxFileSize / 1024);
				break;
			case 'MB':
				return (int) ($this->_maxFileSize / (1024 * 1024));
				break;
		}
	}

	/**
	 * Import map validator
	 * 
	 * @param string $attribute
	 * @param array $params
	 */
	public function checkImportMap($attribute) {
		// Check that the two columns are mapped correctly
		if(!$this->_importMap['user_column'])
			$this->addError('user_column', 'Missing user_column mandatory param');
		else if(!in_array($this->_importMap['user_column'], array('id_user', 'username')))
			$this->addError('user_column', 'Invalid value for user_column param');

		if(!$this->_importMap['group_column'])
			$this->addError('group_column', 'Missing group_column mandatory param');
		else if(!in_array($this->_importMap['group_column'], array('id_group', 'groupname')))
			$this->addError('group_column', 'Invalid value for group_column param');
	}

	
	/**
	 * Enumerate all rows in _data and process them
	 * 
	 * @param string $validate  Validate this model data?
	 * @param string $skipHeader Skip first row IF it is declared a as a header?
	 * @param string $startLineIndex The line where to start from
	 *
	 * @throws CException
	 * @return boolean
	 */
	public function runItems($validate=true, $skipHeader=true, $startLineIndex = 0) {
 		if ($validate && !$this->validate())
			return false;

		$data = $this->_data;
		if ($skipHeader && $this->firstRowAsHeader) {
			$startLineIndex++;
			unset($data[0]);
		}

		// Reset failed lines
		$this->failedLines = array();

		// First collect user idst and group idst
		$userMapping = null;
		$validUserIds = array();
		if($this->_importMap['user_column'] == 'username') {
			$userNames = array_column($data, 0);
			foreach($userNames as &$userName)
				$userName = Yii::app()->db->quoteValue("/".$userName);
			$sql = "SELECT idst as id_user, SUBSTR(userid, 2) as username FROM core_user WHERE userid IN (".implode(",", $userNames).")";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			foreach($rows as $row)
				$userMapping[$row['username']] = $row['id_user'];
		} else {
			$validUserIds = array_column($data, 0);
			foreach($validUserIds as &$validUserId)
				$validUserId = Yii::app()->db->quoteValue($validUserId);
			$sql = "SELECT idst FROM core_user WHERE idst IN (".implode(",", $validUserIds).")";
			$validUserIds = Yii::app()->db->createCommand($sql)->queryColumn();
		}

		$groupMapping = null;
		$validGroupIds= array();
		if($this->_importMap['group_column'] == 'groupname') {
			$groupNames = array_column($data, 1);
			foreach($groupNames as &$groupName)
				$groupName = Yii::app()->db->quoteValue("/".$groupName);
			$sql = "SELECT idst as id_group, SUBSTR(groupid, 2) as groupname FROM core_group WHERE hidden = 'false' AND groupid IN (".implode(",", $groupNames).")";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			foreach($rows as $row)
				$groupMapping[$row['groupname']] = $row['id_group'];
		} else {
			$validGroupIds = array_column($data, 1);
			foreach($validGroupIds as &$validGroupId)
				$validGroupId = Yii::app()->db->quoteValue($validGroupId);
			$sql = "SELECT idst FROM core_group WHERE idst IN (".implode(",", $validGroupIds).") AND hidden = 'false'";
			$validGroupIds = Yii::app()->db->createCommand($sql)->queryColumn();
		}

		$lineIndex = $startLineIndex;
 		foreach ($data as $item) {
			$lineIndex++;
			$errorText = null;

			//sometimes CSVs can contain empty lines which will result in confusing errors. Just avoid them.
			if (empty($item) || (count($item) == 1 && empty($item[0])))
				continue;

			$transaction = Yii::app()->db->beginTransaction();

			try {
				// Get user id & group id
				$id_user = $userMapping ? $userMapping[$item[0]] : (in_array($item[0], $validUserIds) ? $item[0] : null);
				$id_group = $groupMapping ? $groupMapping[$item[1]] : (in_array($item[1], $validGroupIds) ? $item[1] : null);

				// Check if user is valid
				if(!$id_user)
					throw new CException("Invalid user provided");

				// Check if group is valid
				if(!$id_group)
					throw new CException("Invalid group provided");

				// Are we assigning or un-assigning?
				// Check if user is already assigned
				$coreGroupMember = CoreGroupMembers::model()->findByAttributes(array('idst' => $id_group, 'idstMember' => $id_user));
				switch($this->operationType) {
					case 'assign':
						if($coreGroupMember)
							throw new CException("User already assigned to group");

						// Create new instance
						$coreGroupMember = new CoreGroupMembers();
						$coreGroupMember->idst = $id_group;
						$coreGroupMember->idstMember = $id_user;
						$coreGroupMember->save();
						break;
					case 'unassign':
						if(!$coreGroupMember)
							throw new CException("User is not assigned to group");

						$coreGroupMember->delete();
						break;
				}

				// Done..
				$this->successCounter++;

				// Everything related to this user was successful, COMMIT database changes please
				$transaction->commit();

			} catch (Exception $e) {
				$errorText = $e->getMessage();
				$transaction->rollback();
			}

			// Save error lines
			if ($errorText) {
				$tmpArray = array_merge(array($errorText), $item);
				$this->failedLines[] = CSVParser::arrayToCsv($tmpArray);
			}
		}

		// If we've collected some errors... 
		if ($this->hasErrors())
			return false;
		
		return true;
	}

	/**
	 * Set model _data array (users)
	 * @param unknown $data
	 */
	public function setData($data) {
		$this->_data = $data;
	}

	/**
	 * Translate human readable separator name to real delimiter character
	 * @param string $separatorName
	 * @return string
	 */
	public static function translateSeparatorName($separatorName, $manualSeparator) {
		$delimiter = '';
		switch ($separatorName) {
			case 'comma':
				$delimiter = ',';
				break;
			case 'semicolon':
				$delimiter = ';';
				break;
			case 'manual':
				$delimiter = $manualSeparator;
				break;
			case 'auto':
				$delimiter = 'auto';
				break;
		}
		return $delimiter;
	}
	

}