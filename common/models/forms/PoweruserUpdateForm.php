<?php

/**
 * This model is used to change/assign Poweruser profile or assign users to Poweruser from CSV file 
 * 
 * @author Georgi
 *
 */
class PoweruserUpdateForm extends CFormModel{
	
	const JOB_TYPE_UPDATE = 'update';
	const JOB_TYPE_ASSIGN = 'assign';
	
	const USER_COLUMN_USERNAME = 'username';
	const USER_COLUMN_ID_USER = 'id_user'; 
	
	const ASSIGN_TYPE_ID_USER = 'id_user';
	const ASSIGN_TYPE_USERID = 'userid';
	const ASSIGN_TYPE_ID_GROUP = 'id_group';
	const ASSIGN_TYPE_GROUP_NAME = 'group_name';
	const ASSIGN_TYPE_ID_ORG = 'id_org';
	const ASSIGN_TYPE_BRANCH_NAME = 'branch_name';
	
	const RESET_TYPE_USERS = 'users';
	const RESET_TYPE_BRANCHES = 'branches';
	const RESET_TYPE_GROUPS = 'groups';
	const RESET_TYPE_ALL = 'all';
	
	/**
	 * Various model attributes
	 */
	public $file;
	public $localFile;
	public $originalFilename;
	public $separator = 'auto';
	public $manualSeparator = '';
	public $firstRowAsHeader = false;
	public $charset = 'UTF-8';
	public $totalUsers = 0;
	public $userColumn;	
	public $jobType;
	public $resetProfile = null;
	public $resetAssigment = null;
	
	public $failedLines = array();
	
	protected $_data;
	
	public static $charsets = array(
			'Windows-1252'	=> 'ANSI (Windows-1252)',
			'BIG-5'			=> 'BIG-5',
			'UCS-2'			=> 'UCS-2',
			'UCS-2BE'		=> 'UCS-2 Big Endian',
			'UCS-2LE'		=> 'UCS-2 Little Endian',
			'UTF-8'			=> 'UTF-8',
			'UTF-16'		=> 'UTF-16',
			'UTF-16BE'		=> 'UTF-16 Big Endian',
			'UTF-16LE'		=> 'UTF-16 Little Endian'
	);
	
	public function getParams(){
		return array(
				'originalFilename' => $this->originalFilename,
				'separator' => $this->separator,
				'charset' => $this->charset,
				'totalUsers' => $this->totalUsers,
				'userColumn' => $this->userColumn,
				'manualSeparator' => $this->manualSeparator,
				'firstRowAsHeader' => $this->firstRowAsHeader,
				'jobType' => $this->jobType,
				'failedLines' => $this->failedLines,
				'resetProfile' => $this->resetProfile,
				'resetAssigment' => $this->resetAssigment				
		);
	}
	
	protected $_maxFileSize = 3145728; //300MB
	
	public function rules(){
		return array(
				array(
						'file',
						'file',
						'on' => 'step_one',
						'maxSize' => $this->_maxFileSize,
						'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
						'types' => array('csv'),
						'wrongType' => 'File type should be in "csv" format',
				),
				array('file', 'file', 'on' => 'step_one',
						'mimeTypes' => array(
								'text/x-comma-separated-values',
								'text/comma-separated-values',
								'application/octet-stream',
								'application/vnd.ms-excel',
								'text/csv',
								'text/plain',
								'application/csv',
								'application/excel',
								'application/vnd.msexcel',
								//NOTE: sometimes due to an error of mime-type interpreter, csv text content is erroneously seen as C or Assembler code,
								// so we have included those types in the list. The most important thing is that we have a true text file with csv
								// extension, whatever text is contained (if content is not a valid csv, then simply nothing can be imported from that file).
								'text/x-c',
								'text/x-asm'
						),
						'wrongMimeType' => 'File type should be in "csv" format',
						'skipOnError' => true,
				),
				array('separator, charset, totalUsers, manualSeparator, firstRowAsHeader, jobType, resetProfile, resetAssigment, failedLines', 'safe')
		);
	}
	
	public function getMaxFileSize($type = 'B'){
		switch ($type) {
			case 'B':
				return $this->_maxFileSize;
				break;
			case 'KB':
				return (int) ($this->_maxFileSize / 1024);
				break;
			case 'MB':
				return (int) ($this->_maxFileSize / (1024 * 1024));
				break;
		}
	}
	
	public function setData($data){
		$this->_data = $data;
	}
	
	public function runItems($validate = true, $skipHeader = true, $startLineIndex = 0){
		if($validate && !$this->validate()){
			return false;
		}
		
		$data = $this->_data;
		if($skipHeader && $this->firstRowAsHeader){
			$startLineIndex++;
		}
		
		$this->failedLines = array();
		
		$userMapping = null;
		$validUserIds = array();					
		if($this->userColumn == self::USER_COLUMN_USERNAME){
			$userNames = array_column($data, 0);				
			foreach($userNames as &$userName){
				$userName = Yii::app()->db->quoteValue("/" . $userName);
			}
			
			$sql = "SELECT idst as id_user, SUBSTR(userid, 2) as username FROM core_user WHERE userid IN (" .implode(",", $userNames) .")";
			$rows = Yii::app()->db->createCommand($sql)->queryAll();
			foreach ($rows as $row){
				$userMapping[$row['username']] = $row['id_user'];
			}
		}else{
			$validUserIds = array_column($data, 0);
			foreach ($validUserIds as &$validUserId){
				$validUserId = Yii::app()->db->quoteValue($validUserId);
			}
			
			$sql = "SELECT idst FROM core_user WHERE idst IN (" . implode(',', $validUserIds) .")";
			$validUserIds = Yii::app()->db->createCommand($sql)->queryColumn();
		}		
		
		$profilesMapping = null;
		$assignMapping = null;
		$assignTypeMapping = null;
		$validAssignIds = array();
		if($this->jobType === self::JOB_TYPE_UPDATE){
			$profileNames = array_column($data, 1);
			
			foreach ($profileNames as $profileName){
				$profileName = Yii::app()->db->quoteValue($validProfileName);
			}					
			
			foreach ($profileNames as $profile){
				$profilesMapping[$profile] = $profile;
			}
		} else{
			$assignTypes = array_column($data, 1);
			$assignValue = array_column($data, 2);			
			
			foreach ($assignTypes as $id => $assignType){				
				if(!empty($assignType))
					$assignMapping[$id] = $assignValue[$id];
			}					
		}
		
		$lineIndex = $startLineIndex;
		foreach($data as $id => $item){
			$lineIndex++;
			$errorText = null;
			
			if(empty($item) || (count($item) == 1 && empty($item[0]))){
				continue;
			}
			
			$transaction = Yii::app()->db->beginTransaction();
			
			try{
				$id_user = $userMapping ? $userMapping[$item[0]] : (in_array($item[0], $validUserIds) ? $item[0] : null);							
				
				if(!$id_user){
					throw new CException("Invalid user provided");
				}				
				
				$groupMember = CoreGroupMembers::model()->findAdminLevel($id_user);				
				
				if ($groupMember === null) {
					$groupMember = new CoreGroupMembers();
					$groupMember->idstMember = $user->idst;
				}
				else {
					// check if the user is already a power user
					if ($groupMember->group->groupid !== Yii::app()->user->level_admin)
						throw new CHttpException(403, 'User is not a power user');
				}
				
				if($this->jobType == self::JOB_TYPE_UPDATE){									
					
					$profileName = $profilesMapping ? $profilesMapping[$item[1]] : null;									
					
					if(!empty($profileName)){
						$groupMemeber = CoreGroupMembers::model()->findAdminLevel($id_user);											
							
						if($groupMemeber->group->groupid !== Yii::app()->user->level_admin){
							throw new CHttpException(403, 'User is not a Power User');
						}
						
						$puProfile = CoreGroup::model()->findByAttributes(array('groupid' => '/framework/adminrules/' . $profileName));
							
						if(!$puProfile){
							throw new CHttpException(404, 'PU Profile not found');
						}
							
						$profileMember = CoreGroupMembers::model()->getPowerUserProfile($id_user);						
						$profileMember->idst = $puProfile->idst;
						$profileMember->save();
						// Sync the User Role with the Hydra RBAC
						HydraRBAC::manageUserToRole(array( "user" => $id_user, "group" => $puProfile->idst ), true);
					} else{
						if($this->resetProfile == true){
							$criteria = new CDbCriteria();
							$criteria->with = array('group');
							$criteria->compare('group.groupid', '/framework/adminrules/', true);
							$criteria->compare('t.idstMember', $id_user);
							
							$model = CoreGroupMembers::model()->find($criteria);
							
							if($model)	{
								$model->delete();
								// Sync the User Role with the Hydra RBAC
								HydraRBAC::manageUserToRole(array( "user" => $id_user, "group" => null ), true);
							}
						}
					}
				} else{									
					$type = $item[1];									
					
					$idst = array();
					
					switch ($type){
						case self::ASSIGN_TYPE_ID_USER:
							$_user = CoreUser::model()->findByPk($assignMapping[$id]);
							
							if(!$_user){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$idst[] = $_user->idst;
							
							break;
						case self::ASSIGN_TYPE_USERID:
							$_user = CoreUser::model()->find('userid LIKE :user', array(
									':user' => '/' . $assignMapping[$id]
							));
							
							if(!$_user){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$idst[] = $_user->idst;
							
							break;
						case self::ASSIGN_TYPE_ID_GROUP:
							$group = CoreGroup::model()->findByPk($assignMapping[$id]);
							
							if(!$group){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$idst[] = $group->idst;
							break;
						case self::ASSIGN_TYPE_GROUP_NAME:
							$group = CoreGroup::model()->find('groupid = :groupName', array(
									':groupName' => '/' . $assignMapping[$id]
							));
							
							if(!$group){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$idst[] = $group->idst;
							break;
						case self::ASSIGN_TYPE_ID_ORG:
							$orgChart = CoreOrgChartTree::model()->findByPk($assignMapping[$id]);
							
							if(!$orgChart){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$orgChartTree = CoreOrgChartTree::model()->findByPk($assignMapping[$id]);
							
							$idst[] = $orgChartTree->idst_ocd;
							break;
						case self::ASSIGN_TYPE_BRANCH_NAME:
							$language = Lang::getNameByBrowserCode(Yii::app()->getLanguage());
							
							$orgChart = CoreOrgChart::model()->find('lang_code = :lang AND translation = :value', array(
									':lang' => strtolower ($language),
									':value' => $assignMapping[$id]
							));
							
							if(!$orgChart){
								throw new CHttpException(405, 'Invalid item_value for item_type');
							}
							
							$orgChartTree = CoreOrgChartTree::model()->findByPk($orgChart->id_dir);
							
							$idst[] = $orgChartTree->idst_ocd;
							
							break;
						default:
							break;
					}														
					
					if(!empty($this->resetAssigment)){
						
						if($this->resetAssigment !== 'none'){																		
							if($this->resetAssigment == self::RESET_TYPE_ALL){
								$this->unassignPoweruserIdsts($id_user);
								
							} else{																						
								$resetTypes = explode(",", $this->resetAssigment);
								
								$ids = array();
								
								if(!empty($resetTypes)){
									switch ($resetTypes){
										case self::RESET_TYPE_USERS:										
											$users = CoreAdminTree::getPowerUserUsers($id_user);										
											$ids = array_keys($users);										
											break;
										case self::RESET_TYPE_BRANCHES:
											$branches = CoreAdminTree::getPowerUserOrgchartDesc($id_user);
											$ids = array_keys($branches);
											break;
										case self::RESET_TYPE_GROUPS:
											$groups = CoreAdminTree::getPowerUserGroups($id_user);
											$ids = array_keys($groups);
											break;
										default:
											break;
									}															
									
									$this->unassignPoweruserIdsts($id_user, $ids);
								}													
							}	
						}
					}
					
					CoreAdminTree::updatePowerUserSelection($id_user, $idst);
					
					CoreOrgChartTree::model()->flushCache();
				}
				
				$transaction->commit();
			} catch(Exception $e){
				$errorText = $e->getMessage();
				$transaction->rollback();
			}
			
			if($errorText){
				$tmpArray = array_merge(array($errorText), $item);
				$this->failedLines[] = CSVParser::arrayToCsv($tmpArray);
			}
		}
	}
	
	private function unassignPoweruserIdsts($puId, $ids = null){
		
		$idstsToRemove = array();
		
		if(!$ids){
			$list = CoreAdminTree::model()->findAllByAttributes(array('idstAdmin' => $puId));
			$idstsToRemove = array();
			if (is_array($list) && !empty($list)) {
				foreach ($list as $record) {
					$idstsToRemove[] = $record['idst'];
				}
			}						
		} else{
			$idstsToRemove = $ids;
		}			
		
		$condition = new CDbCriteria();
		$condition->addInCondition('idst', $idstsToRemove);
		$condition->addCondition('idstAdmin=:pu');
		$condition->params[':pu'] = $puId;			
		
		// Actually unassing the users/groups from the power user
		CoreAdminTree::model()->deleteAll($condition);
		//$this->updateUsersInReportFilters((int)$currentPowerUserId,$idsArr);
		// Recalculate cached tables for this power user		
	}
}