<?php
/**
 *
 * This model is used to save the state between CSV branch import UI steps and between Progressive Importer runs (chunked import)
 * Stored in session as a whole object.
 *
 *
 */
class OrgChartImportForm extends CFormModel {


    CONST CODE = 'code';
    CONST PARENT = 'parent_code';
    CONST LEVEL = 'level';

    const SCENARIO_ACTIVATION = 'activation';
    /**
     * Various model attributes
     * @var mixed
     */
    public $file; 								// PHP temporary file (CUploadedFile instance)
    public $localFile;  						// locally saved, temporary copy of the uploaded file (in upload_tmp)
    public $originalFilename;					// Just the original name of the uploaded file. Name only! Not the path!
    public $separator = 'auto';					// CSV separator (1st Step UI option)
    public $manualSeparator;
    public $firstRowAsHeader = true;			// Consider first row in CSV as a header? (1st Step UI option)
    public $charset = 'UTF-8'; 					// charset of csv file (1st Step UI option)
    public $insertUpdate = false;				// Update Branch if they exist? (2nd Step UI option)
    public $failedLines = array();   			// Collecta failing users for a single run; reset before call saveUsers() please
    public $successCounter	= 0;

    public static $charsets = array(
        'Windows-1252'	=> 'ANSI (Windows-1252)',
        'BIG-5'			=> 'BIG-5',
        'UCS-2'			=> 'UCS-2',
        'UCS-2BE'		=> 'UCS-2 Big Endian',
        'UCS-2LE'		=> 'UCS-2 Little Endian',
        'UTF-8'			=> 'UTF-8',
        'UTF-16'		=> 'UTF-16',
        'UTF-16BE'		=> 'UTF-16 Big Endian',
        'UTF-16LE'		=> 'UTF-16 Little Endian'
    );

    protected $_data;							// Array of CSV lines, e.g. users

    /**
     * Holds the map between CSV fields and user fields (or user virtual attributes_
     * @var Array
     */
    protected $_importMap = array();

    /**
     * Translate human readable separator name to real delimiter character
     * @param string $separatorName
     * @return string
     */
    public static function translateSeparatorName($separatorName, $manualSeparator = '') {
        $delimiter = '';
        switch ($separatorName) {
            case 'comma':
                $delimiter = ',';
                break;
            case 'semicolon':
                $delimiter = ';';
                break;
            case 'manual':
                $delimiter = $manualSeparator;//$this->manualSeparator;
                break;
            case 'auto':
                $delimiter = 'auto';
                break;
        }
        return $delimiter;
    }

    /**
     * _importMap setter
     *
     * @param array $importMap
     * @return boolean
     */
    public function setImportMap($importMap) {
        if (is_array($importMap)) {
            $this->_importMap = $importMap;
            return true;
        }
        return false;
    }

    /**
     * _importMap getter
     *
     * @return array:
     */
    public function getImportMap() {
        return $this->_importMap;
    }

    /**
     *
     * @param string $type
     * @return number
     */
    public function getMaxFileSize($type = 'B') {
        switch ($type) {
            case 'B':
                return $this->_maxFileSize;
                break;
            case 'KB':
                return (int) ($this->_maxFileSize / 1024);
                break;
            case 'MB':
                return (int) ($this->_maxFileSize / (1024 * 1024));
                break;
        }
    }

    /**
     * Returns a json representation of this form (to be serialized)
     * @return string
     */
    public function getParams() {
        return array(
            'originalFilename' => $this->originalFilename,
            'separator' => $this->separator,
            'manualSeparator' => $this->manualSeparator,
            'firstRowAsHeader' => $this->firstRowAsHeader,
            'charset' => $this->charset,
            'insertUpdate' => $this->insertUpdate,
            'failedLines' => $this->failedLines,
            'successCounter' => $this->successCounter,
            'importMap' => $this->_importMap,
        );
    }

    protected $_maxFileSize = 3145728; // 314572800 = 300 Mb

    protected $_createdUsers = false;

    /**
     * @see CModel::rules()
     */
    public function rules() {
        return array(
            array(
                'file',
                'file',
                'on' => 'step_one',
                'maxSize' => $this->_maxFileSize,
                'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
                'types' => array('csv'),
                'wrongType' => 'File type should be in "csv" format',
            ),
            array('file', 'file', 'on' => 'step_one',
                'mimeTypes' => array(
                    'text/x-comma-separated-values',
                    'text/comma-separated-values',
                    'application/octet-stream',
                    'application/vnd.ms-excel',
                    'text/csv',
                    'text/plain',
                    'application/csv',
                    'application/excel',
                    'application/vnd.msexcel',
                    //NOTE: sometimes due to an error of mime-type interpreter, csv text content is erroneously seen as C or Assembler code,
                    // so we have included those types in the list. The most important thing is that we have a true text file with csv
                    // extension, whatever text is contained (if content is not a valid csv, then simply nothing can be imported from that file).
                    'text/x-c',
                    'text/x-asm'
                ),
                'wrongMimeType' => 'File type should be in "csv" format',
                'skipOnError' => true,
            ),
            array('importMap', 'checkImportMap', 'on' => 'step_two'),
            array('separator, manualSeparator, firstRowAsHeader, charset, insertUpdate', 'safe'),
        );
    }

    /**
     * (non-PHPdoc)
     * @see CModel::attributeLabels()
     */
    public function attributeLabels() {
        return array(
            'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
            'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
            'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
            'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET'),
            'node' => Yii::t('user_management', '_IMPORT_FORM_NODE'),
            'passwordChanging' => Yii::t('admin_directory', '_FORCE_PASSWORD_CHANGE'),
            'sendAlertToUsers' => Yii::t('user_management', '_IMPORT_FORM_SENDALERTTOUSERS'),
            'insertUpdate' => Yii::t('automation', 'Update branch title'),
            'activation' => Yii::t('standard', 'Action to perform'),

            // Form Labels
            'code' => Yii::t('automation', 'Branch code'),
            'parent_code' => Yii::t('automation', 'Parent code'),
            'level' => Yii::t('automation', 'Level'),
        );
    }

    /**
     * Import map validator
     *
     * @param string $attribute
     * @param array $params
     */
    public function checkImportMap($attribute, $params) {
        $selectedItems = array();
        foreach ($this->$attribute as $item) {
            if ($item != 'ignoredfield') {
                if (!in_array($item, $selectedItems)) {
                    $selectedItems[] = $item;
                } else {
                    $this->addError('importMap', Yii::t('standard', 'Duplicated field(s)'));
                    break;
                }
            }
        }
        if (empty($selectedItems)) {
            $this->addError('importMap', Yii::t('standard', 'You must select field(s)'));
        }
    }

    /**
     * Set model _data array (users)
     * @param unknown $data
     */
    public function setData($data) {
        $this->_data = $data;
    }

    public static function getImportMapList() {
        $list = array(
            'ignoredfield' => Yii::t('organization_chart', '_IMPORT_IGNORE'),
            'code' => Yii::t('automation', 'Branch code'),
            'parent_code' => Yii::t('automation', 'Parent code'),
            'level' => Yii::t('automation', 'Level'),
        );
        // Add all active languages as possible column names
        $langs = CoreLangLanguage::getActiveLanguages();
        foreach($langs as $code => $lang)
            $list[$code] = Yii::t('automation', 'Branch name ({name})', array('{name}' => $lang));

        return $list;
    }

    public function saveOrgCharts(){
        // Preload some useful info
        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();
        $existingNodeCodesSql = Yii::app()->db->createCommand()
            ->select('idOrg,code')
            ->from(CoreOrgChartTree::model()->tableName())
            ->where("code <> ''")
            ->queryAll();

        $existingNodes = array();
        $existingNodeCodes = array();
        foreach($existingNodeCodesSql as $row)
            $existingNodeCodes[$row['code']] = $row['idOrg'];

        $map = array_flip($this->_importMap);
        foreach ($this->_data as $key => $node) {
            $branchSummaryErrorTexts = array();
            try {
                $filtered = array_filter($node);
                //sometimes CSVs can contain empty lines which will result in confusing errors. Just avoid them.
                if (!is_array($filtered) || empty($filtered) || (count($filtered) <= 0) || (count($filtered) == 1 )) {
                    continue;
                }

                $transaction = Yii::app()->db->beginTransaction();
                // Determine parent node
                $parent = null;
                $code = $node[$map[self::CODE]];
                // If there is no parent then the parent is the ROOT
                if (!$node[$map[self::PARENT]]){
                    $parent = CoreOrgChartTree::getOrgRootNode();
                }else if (isset($existingNodeCodes[$node[$map[self::PARENT]]])) {
                    if(!isset($existingNodes[$node[$map[self::PARENT]]]))
                        $existingNodes[$node[$map[self::PARENT]]] = CoreOrgChartTree::model()->findByAttributes(array('code' => $node[$map[self::PARENT]]));

                    $parent = $existingNodes[$node[$map[self::PARENT]]];
                }

                if (!$parent || !($parent instanceof CoreOrgChartTree)){
                    throw new Exception("Node ".$code." has no valid parent node: " . $node[$map[self::PARENT]]);
                }else {

                    // Check if the node is already in the LMS
                    if(isset($existingNodeCodes[$code])){
                        // If we should force update then update the language only
                        if($this->insertUpdate)
                            $this->manageNode($activeLanguagesList, $map, $parent, $code, $node, $existingNodeCodes[$code]); // pass the id org as last in order to update the node
                        else
                            throw new Exception("Node ".$code." already exists");
                    }else{
                        // Otherwise the node does not exists create it
                        $this->manageNode($activeLanguagesList, $map, $parent, $code, $node);
                        // Update the existing nodes array
                        $existingNodeCodes[$code] = Yii::app()->db->createCommand()
                            ->select('idOrg')
                            ->from(CoreOrgChartTree::model()->tableName())
                            ->where("code = :code")
                            ->queryScalar(array(':code' => $code));
                    }

                    // Everything related to this branch was successful, COMMIT database changes please
                    $transaction->commit();
                    $this->successCounter++;
                }

            } catch (Exception $e) {
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                $branchSummaryErrorTexts[] = $e->getMessage();
                $transaction->rollback();
            }

            // We Finished handling a given branch. If errors have been collected for THIS user, add it to Failed list, together with all the errors as
            // the first field, enclosed in "" and separated by a pipe |)
            // All these failed branches can be used to report errors (save to file, show on screen, etc)
            if (count($branchSummaryErrorTexts) > 0) {
                $tmpArray = array(implode('|', $branchSummaryErrorTexts));
                $tmpArray = array_merge($tmpArray, $node);
                $this->failedLines[] = CSVParser::arrayToCsv($tmpArray);
            }

        }
    }

    private function newNodeSql(){
        return  "UPDATE `core_org_chart_tree` SET `iRight`=`iRight`+2 WHERE `iRight`>=:iRight;
            UPDATE `core_org_chart_tree` SET `iLeft`=`iLeft`+2 WHERE `iLeft`>=:iRight;
            INSERT INTO `core_st` (`idst`) VALUES (NULL);
            SET @id_oc = LAST_INSERT_ID();
            INSERT INTO `core_st` (`idst`) VALUES (NULL);
            SET @id_ocd = LAST_INSERT_ID();
            INSERT INTO `core_org_chart_tree` (`idParent`, `lev`, `iLeft`, `iRight`, `idst_oc`, `idst_ocd`, `code`) VALUES (0, :level, :iRight, :iRight + 1, @id_oc, @id_ocd, :code);
            SET @id_org = LAST_INSERT_ID();
            INSERT INTO `core_group` (`groupid`, `hidden`, `type`, `assign_rules`, `assign_rules_logic_operator`, `idst`, `description`) SELECT concat('/oc_', @id_org), 1, 'free', 0, 'AND', @id_oc, '';
            INSERT INTO `core_group` (`groupid`, `hidden`, `type`, `assign_rules`, `assign_rules_logic_operator`, `idst`, `description`) SELECT concat('/ocd_', @id_org), 1, 'free', 0, 'AND', @id_ocd, '';
        ";
    }

	private function moveNodeSql($id_parent, $id_folder)
	{
		return "set group_concat_max_len = 2048;

set @idFolder := ".(int)$id_folder.";
set @newIdParent := ".(int)$id_parent.";

set @diff := (select iRight - iLeft + 1
from core_org_chart_tree
where idOrg = @idFolder);

set @iLeft = (select iLeft
from core_org_chart_tree
where idOrg = @idFolder);

set @iRight = (select iRight
from core_org_chart_tree
where idOrg = @idFolder);

set @foldersToUpdate = (select concat(',', group_concat(idOrg), ',')
from core_org_chart_tree
where ileft >= @iLeft
and iRight <= @iRight);

update core_org_chart_tree
set iRight = iRight - @diff
where iLeft < @iLeft
and iRight > @iRight;

update core_org_chart_tree
set iRight = iRight - @diff,
iLeft = iLeft - @diff
where iLeft > @iLeft
and iRight > @iRight;

set @newDataDiff := (select t2.iRight - t1.iLeft
from core_org_chart_tree as t1
join core_org_chart_tree as t2 on t1.idOrg = @idFolder and t2.idOrg = @newIdParent);

set @newDataDiffLevel := (select t1.lev - t2.lev - 1
from core_org_chart_tree as t1
join core_org_chart_tree as t2 on t1.idOrg = @idFolder and t2.idOrg = @newIdParent);

set @newParentILeft = (select iLeft
from core_org_chart_tree
where idOrg = @newIdParent);

set @newParentIRight = (select iRight
from core_org_chart_tree
where idOrg = @newIdParent);

update core_org_chart_tree
set iRight = iRight + @diff
where iLeft <= @newParentILeft
and iRight >= @newParentIRight
and locate(concat(',', idOrg, ','), @foldersToUpdate) = 0;

update core_org_chart_tree
set iLeft = iLeft + @newDataDiff,
iRight = iRight + @newDataDiff,
lev = lev - @newDataDiffLevel
where locate(concat(',', idOrg, ','), @foldersToUpdate) > 0;

update core_org_chart_tree
set iRight = iRight + @diff,
iLeft = iLeft + @diff
where iLeft > @newParentILeft
and iRight > @newParentIRight
and locate(concat(',', idOrg, ','), @foldersToUpdate) = 0;";
	}

    private function manageNode($activeLanguagesList, $map, $parent, $code, $node,$existing = false){
        $params = array(
            ':iRight' => $parent->iRight,
            ':level'  => $parent->lev + 1,
            ':code'	  => $code
        );
        // Prepare the sql query
        $insertNodeSQL = "SET autocommit=0;SET unique_checks=0;SET foreign_key_checks=0;";
        // IF we are in create mode then add the create statement
        // otherwise it will update the translations
        if($existing === false)
            $insertNodeSQL .= $this->newNodeSql();
		else
		{
			$current_parent = Yii::app()->db->createCommand("select idOrg from core_org_chart_tree where iLeft < (select iLeft from core_org_chart_tree where idOrg = ".(int)$existing.") and iRight > (select iRight from core_org_chart_tree where idOrg = ".(int)$existing.") and lev = (select lev - 1 from core_org_chart_tree where idOrg = ".(int)$existing.")")->queryScalar();

			if($current_parent != $parent->idOrg)
				$insertNodeSQL .= $this->moveNodeSql($parent->idOrg, $existing);
		}

        // Add translation to the node (one per language)
        foreach($activeLanguagesList as $langCode => $lang){
            $idOrg = $existing === false ? '@id_org' : $existing;
            $value = isset($node[$map[$langCode]]) ? $node[$map[$langCode]] : '';
            // Update node Translation title
            $insertNodeSQL .= "INSERT INTO `" . CoreOrgChart::model()->tableName() . "`";
            $insertNodeSQL .= " (`id_dir`, `lang_code`, `translation`) VALUES (" . $idOrg . ", '" . $langCode . "', '" . $value ."')";
            $insertNodeSQL .= " ON DUPLICATE KEY UPDATE `translation` = VALUES(translation);";
        }

        // Run the query in bulk
        $insertNodeSQL .= "SET unique_checks=1;SET foreign_key_checks=1;SET autocommit=1; ";

		Yii::app()->db->createCommand($insertNodeSQL)->execute($params);
    }
}