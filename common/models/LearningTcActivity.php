<?php

/**
 * This is the model class for table "learning_tc_activity".
 *
 * The followings are the available columns in table 'learning_tc_activity':
 * @property integer $id_tc_activity
 * @property integer $id_tc_ap
 * @property string $id
 * @property string $type
 * @property string $name
 * @property string $description
 * @property string $launch
 *
 * The followings are the available model relations:
 * @property LearningTcAp $ap
 */
class LearningTcActivity extends CActiveRecord
{

	const TINCAN_METADATA_FILE = 'tincan.xml';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTcActivity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_tc_activity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tc_ap, id, type, name', 'required'),
			array('id_tc_ap', 'numerical', 'integerOnly'=>true),
			array('id, type, name, launch', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_tc_activity, id_tc_ap, id, type, name, description, launch', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ap' => array(self::BELONGS_TO, 'LearningTcAp', 'id_tc_ap'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tc_activity' => 'Id Tc Activity',
			'id_tc_ap' => 'Id Tc Ap',
			'id' => 'ID',
			'type' => 'Type',
			'name' => 'Name',
			'description' => 'Description',
			'launch' => 'Launch',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tc_activity',$this->id_tc_activity);
		$criteria->compare('id_tc_ap',$this->id_tc_ap);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('launch',$this->launch,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}
}