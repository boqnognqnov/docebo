<?php

/**
 * This is the model class for table "learning_deliverable_object".
 *
 * The followings are the available columns in table 'learning_deliverable_object':
 * @property integer             $idObject
 * @property integer             $id_deliverable
 * @property integer             $id_user
 * @property string              $name
 * @property string              $type
 * @property string              $content
 * @property integer             $evaluation_score
 * @property integer             $evaluation_status
 * @property string              $evaluation_date
 * @property string              $evaluation_comments
 * @property string              $evaluation_file
 * @property integer             $evaluation_user_id
 * @property string              $student_comments
 * @property integer             $allow_another_upload
 * @property string              $created
 * @property string              $user_reply
 *
 * The followings are the available model relations:
 * @property LearningDeliverable $deliverable
 * @property CoreUser            $user
 */
class LearningDeliverableObject extends CActiveRecord {
	const STATUS_PENDING       = 0;
	const STATUS_ACCEPTED      = 1;
	const STATUS_REJECTED      = -1;
	const EXTENSIONS_WHITELIST = 'mp4,flv,avi,mpeg,mpg,mov,mkv';

	public $search_input, $search_status, $courseId;

	/** @var array Array containing the uploaded files names */
	public $files;

	/** @var idCoachingSession id of a coaching session in which the LDO should be evaluated; default value: null  */
	public $idCoachingSession = null;

	protected $oldAttributes = array();

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_deliverable_object';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_deliverable, id_user, name, content, created', 'required'),
			array('id_deliverable, id_user, evaluation_score, evaluation_status, evaluation_user_id, allow_another_upload', 'numerical', 'integerOnly' => TRUE),
			array('name, content, evaluation_file', 'length', 'max' => 21844),
			array('type', 'length', 'max' => 6),
			array('evaluation_date, evaluation_comments, student_comments, user_reply', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('search_input, idObject, id_deliverable, id_user, name, type, content, evaluation_score, evaluation_status, evaluation_date, evaluation_comments, evaluation_file, evaluation_user_id, student_comments, allow_another_upload, created, search_input, search_status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deliverable' => array(self::BELONGS_TO, 'LearningDeliverable', 'id_deliverable'),
			'user'        => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('evaluation_date medium', 'created medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idObject'             => 'Id Object',
			'id_deliverable'       => 'Id Deliverable',
			'id_user'              => 'Id User',
			'name'                 => 'Name',
			'type'                 => 'Type',
			'content'              => 'Content',
			'evaluation_score'     => 'Evaluation Score',
			'evaluation_status'    => 'Evaluation Status',
			'evaluation_date'      => 'Evaluation Date',
			'evaluation_comments'  => 'Evaluation Comments',
			'evaluation_file'      => 'Evaluation File',
			'student_comments'     => 'Student Comments',
			'allow_another_upload' => 'Allow Another Upload',
			'created'              => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idObject', $this->idObject);
		$criteria->compare('id_deliverable', $this->id_deliverable);
		$criteria->compare('id_user', $this->id_user);
		$criteria->compare('name', $this->name, TRUE);
		$criteria->compare('type', $this->type, TRUE);
		$criteria->compare('content', $this->content, TRUE);
		$criteria->compare('evaluation_score', $this->evaluation_score);
		$criteria->compare('evaluation_status', $this->evaluation_status);
		$criteria->compare('evaluation_date', Yii::app()->localtime->fromLocalDateTime($this->evaluation_date), TRUE);
		$criteria->compare('evaluation_comments', $this->evaluation_comments, TRUE);
		$criteria->compare('evaluation_file', $this->evaluation_file, TRUE);
		$criteria->compare('evaluation_user_id', $this->evaluation_user_id, TRUE);
		$criteria->compare('student_comments', $this->student_comments, TRUE);
		$criteria->compare('allow_another_upload', $this->allow_another_upload);
		$criteria->compare('created', Yii::app()->localtime->fromLocalDateTime($this->created), TRUE);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return LearningDeliverableObject the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function getDeliverablesListStatusIconCss() {
		switch ($this->evaluation_status) {
			case self::STATUS_ACCEPTED:
				return 'green';
			case self::STATUS_REJECTED:
				return 'red';
			default:
				return '';
		}
	}

	public function getScore() {
		if ($this->evaluation_status != self::STATUS_PENDING) {
			return intval($this->evaluation_score) . '/100';
		}

		return '-';
	}

	public function getVideoIconCss() {
		$css = 'is-file';
		if ($this->type == 'link') {
			if (strpos($this->content, 'youtube') > 0 || strpos($this->content, 'youtu.be') > 0) {
				$css = 'is-youtube';
			} else if (strpos($this->content, 'vimeo') > 0) {
				$css = 'is-vimeo';
			}
		} else {
			$extension = strtolower(CFileHelper::getExtension($this->content));

			switch ($extension) {
				case 'ppt':
				case 'pptx':
					$css = 'is-converter';
					break;
				case 'zip':
				case 'rar':
				case '7zip':
					$css = 'is-zip';
					break;
				case 'pdf':
					$css = 'is-pdf';
					break;
				case 'jpg':
				case 'png';
				case 'gif':
					$css = 'is-image';
					break;
				case 'doc':
				case 'docx':
					$css = 'is-word';
					break;
				case 'xls':
				case 'xlsx':
				case 'ods':
				case 'ots':
				case 'csv':
					$css = 'is-xsl';
					break;
				case 'mp3':
				case 'wav';
				case 'ogg':
					$css = 'is-arrow-solid-right';
					break;
				case 'mp4':
					$css = 'is-video';
					break;
			}
		}

		return $css;
	}

	public function getUploadedVideoUrl() {
		$url = '';
		if ($this->type == 'upload') {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
			$url            = $storageManager->fileUrl($this->content);
		}

		return $url;
	}

	/**
	 * Returns an embed code for video
	 * @return string
	 */
	public function loadVideo($width = 560, $height = 315) {
		$html = '';
		if ($this->type == 'link') {
			$url       = $this->content;
			$linkTypes = array('youtube', 'vimeo', 'youtu.be');
			foreach ($linkTypes as $linkType) {
				if (strpos($url, $linkType) > 0) {
					$linkType = str_replace('.', '', $linkType);
					$method = 'get' . ucfirst($linkType) . 'EmbedCode';
					$html   = $this->$method($url, $width, $height);

					return $html;
				}
			}
		}

		return $html;
	}

	protected function getYoutubeId($url) {
		if(strpos($url, 'youtu.be') > 0)
		{
			$explode = explode('/', $url);
			$id = end($explode);
		}
		else
		{
			$start = strpos($url, 'v=');// extract the 'Ufi_IdoQDDk' part from 'http://www.youtube.com/watch?feature=player_detailpage&v=Ufi_IdoQDDk#t=61'
			// the id is always 11 characters long
			$id = substr($url, $start + 2, 11);
		}

		return $id;
	}

	protected function getYoutubeEmbedCode($url, $width, $height) {
		$id   = $this->getYoutubeId($url);
		$src  = '//www.youtube.com/embed/' . $id;
		$html = '<iframe width="' . $width . '" height="' . $height . '" src="' . $src . '" frameborder="0" allowfullscreen></iframe>';

		return $html;
	}

	protected function getVimeoEmbedCode($url, $width, $height) {
		$html    = '';
		$matches = array();
		preg_match('#vimeo\.com/(\w*/)*(\d+)#i', $url, $matches);
		if (isset($matches[2])) {
			$src  = '//player.vimeo.com/video/' . $matches[2];
			$html = '<iframe src="' . $src . '" width="' . $width . '" height="' . $height . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}

		return $html;
	}

	public function getEvaluationAttachmentUrl() {
		$url = '';
		if ($this->evaluation_file) {
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
			$url            = $storageManager->fileUrl($this->evaluation_file);
		}

		return $url;
	}

	public function dataProvider() {
		$criteria = new CDbCriteria();
		$criteria->compare('id_deliverable', $this->id_deliverable);

		$courseDeliverableLOIds = $this->getCourseDeliverableLOIds($this->courseId);

		$criteria->addInCondition('t.id_deliverable', $courseDeliverableLOIds);
		$criteria->with = array(
			'deliverable',
                           'user' => array('alias' => 'user')
                  );

                  $criteria->join = 'INNER JOIN '.LearningCourseuser::model()->tableName().' AS lcu ON (`t`.`id_user` = lcu.idUser AND lcu.idCourse = '.$this->courseId.')';

                  $pUserRights = Yii::app()->user->checkPURights($this->courseId);
		if ($pUserRights->isPu && !$pUserRights->isInstructor) {
			$criteria->join .= ' JOIN '.CoreUserPU::model()->tableName()." pu ON pu.puser_id = :puser_id AND lcu.idUser = pu.user_id";
			$criteria->join .= ' JOIN '.CoreUserPuCourse::model()->tableName()." puc ON puc.puser_id = :puser_id AND puc.course_id = :id_course";
			$criteria->params[':puser_id'] = Yii::app()->user->id;
			$criteria->params[':id_course'] = $this->courseId;
		}

		//PU instructor OR user instructor
		if(!Yii::app()->user->getIsGodadmin() && LearningCourseuser::isUserLevel(Yii::app()->user->id, $this->courseId, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			$instructorUsers = LearningCourse::getInstructorUsers($this->courseId);
			if(!empty($instructorUsers))
				$criteria->addInCondition('t.id_user', $instructorUsers);
		}

		if ($this->idCoachingSession) {
			$criteria->join .= ' JOIN '.LearningCourseCoachingSessionUser::model()->tableName()." lccsu ON (`t`.`id_user` = lccsu.idUser AND lccsu.idSession = :idSession)";
			$criteria->params[':idSession'] = $this->idCoachingSession;
		}

		if ($this->id_user) {
			$criteria->addCondition('t.id_user = :id_user');
			$criteria->params[':id_user'] = $this->id_user;
		}
		if ($this->search_input) {
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :search OR user.email LIKE :search OR user.userid LIKE :search OR deliverable.title LIKE :search');
			$criteria->params[':search'] = '%' . $this->search_input . '%';
		}
		if ($this->search_status && $this->search_status != 'all') {
			if ($this->search_status == 'pending') {
				$criteria->addCondition('t.evaluation_status = :status_pending');
			} else {
				$criteria->addCondition('t.evaluation_status <> :status_pending');
			}
			$criteria->params[':status_pending'] = self::STATUS_PENDING;
		}

		$criteria->order = 'created DESC';

		return new CActiveDataProvider($this, array(
			'criteria'   => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	/**
	 * Returns a list of LearningDeliverable ids
	 *
	 * @param $courseId
	 *
	 * @return array
	 */
	protected function getCourseDeliverableLOIds($courseId) {
		$output = array();

		$lom       = new LearningObjectsManager($courseId);
		$courseLOs = $lom->getCourseLearningObjects($courseId);

		if (!empty($courseLOs)) {
			foreach ($courseLOs as $lo) {
				if ($lo['objectType'] == LearningOrganization::OBJECT_TYPE_DELIVERABLE) {
					$output[] = $lo['idResource'];
				}
			}
		}

		return $output;
	}

	public function beforeDelete() {
		$return = parent::beforeDelete();

		if ($return) {
			// delete uploaded files
			if ($this->type == 'upload') {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
				if (!$storageManager->remove($this->content))
					return FALSE;
			}

			// delete evaluation attachment
			if (!empty($this->evaluation_file)) {
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
				if (!$storageManager->remove($this->evaluation_file))
					return FALSE;
			}
		}

		return $return;
	}

	public function renderStatusIcon() {
		$css = '';
		switch ($this->evaluation_status) {
			case self::STATUS_ACCEPTED:
				$css = 'green';
				break;
			case self::STATUS_REJECTED:
				$css = 'red';
				break;
			default:
				$css = 'orange';
				break;
		}
		$html = '<span class="i-sprite is-circle-check ' . $css . '"></span>';

		return $html;
	}

	public function renderScore($pendingPlaceholder = FALSE) {

		if ($this->evaluation_status == self::STATUS_PENDING && $pendingPlaceholder === TRUE) {
			return $pendingPlaceholder;
		}elseif($this->evaluation_status == self::STATUS_PENDING && !$pendingPlaceholder === TRUE){
			return '-';
		}

		return intval($this->evaluation_score) . '/100';
	}

	/**
	 * Use this method instead of directly returning $this->user_reply
	 * to avoid XSS attacks to the godadmin
	 *
	 * @return string The user's feedback/reply to the instructor comment/evaluation
	 */
	public function getUserReplyToInstructorFeedback(){
		return Yii::app()->htmlpurifier->purify($this->user_reply);
	}

	/**
	 * Track the deliverable object if this is the first video uploaded
	 * Else update the tracking information
	 */
	public function afterSave() {
		$orgObject = LearningOrganization::model()->findByAttributes(array(
			'objectType' => LearningOrganization::OBJECT_TYPE_DELIVERABLE,
			'idResource' => $this->id_deliverable
		));

		if ($orgObject) {
			//track the page
			$idUser = $this->id_user;
			$track  = LearningMaterialsTrack::model()->findByAttributes(array('idReference' => $orgObject->getPrimaryKey(), 'idUser' => $idUser));
			if (!$track) {
				//the object has not been tracked yet for this user: create the record now
				$track              = new LearningMaterialsTrack();
				$track->idUser      = intval($idUser);
				$track->idReference = intval($orgObject->getPrimaryKey());
				$track->idResource  = intval($this->id_deliverable);
			}

			/** @var LearningDeliverable $deliverableObj */
			$deliverableObj = LearningDeliverable::model()->findByPk($this->id_deliverable);
			// the deliverable's status will be set to completed when the file is uploaded if this option is set
			if (intval($deliverableObj->learner_policy) !== LearningDeliverable::NAVRULE_PASSED) {
				$status = LearningCommontrack::STATUS_COMPLETED;
			}
			else {
				switch ($this->evaluation_status) {
					case self::STATUS_ACCEPTED:
						$status = LearningCommontrack::STATUS_COMPLETED;
						break;
					case self::STATUS_REJECTED:
						if ($this->allow_another_upload) {
							$status = LearningCommontrack::STATUS_ATTEMPTED;
						} else {
							$status = LearningCommontrack::STATUS_FAILED;
						}
						break;
					default:
						$status = LearningCommontrack::STATUS_ATTEMPTED;
						break;
				}
			}
			$track->setStatus($status);
			$track->setObjectType(LearningOrganization::OBJECT_TYPE_DELIVERABLE);
			$track->setTrackParam('deliverableObjectId', $this->idObject);
			$track->save();

			// Trigger update of 'score_given'
			if($this->getIsNewRecord() || $this->oldAttributes['evaluation_score']!=$this->evaluation_score){
				LearningCourseuser::recalculateLastScoreByUserAndCourse($orgObject->idCourse, $idUser);

				// also update initial_score_given
				LearningCourseuser::recalculateInitialScoreGiven($idUser, $orgObject->idCourse);
			}
		}

		parent::afterSave();

		return TRUE;
	}

	public function removeEvaluation() {
		$this->evaluation_score = 0;
		$this->evaluation_status = self::STATUS_PENDING;
		$this->evaluation_date = null;
		$this->evaluation_comments = null;
		$this->evaluation_user_id = null;
		$this->allow_another_upload = 1;
                $storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);
		if (!empty($this->evaluation_file)) {

			if (!$storageManager->remove($this->evaluation_file))
				return FALSE;
			$this->evaluation_file = null;
		}

		//$this->save();
                $storageManager->remove($this->content);
                $this->delete();
	}

	public function afterFind() {
		parent::afterFind();

		if ($this->type == 'link') return;

		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_DELIVERABLE);

		$files = empty($this->content) ? array() : CJSON::decode($this->content);
		// THE OLD functionality allows to upload only 1 file at a time
		// and the old files in DB are saved as strings not as jsons like the new ones
		// In the new functionality we need to show the files as array
		$files = $files ? $files : array($this->content);

		if (!is_array($files))
			$files = array($files);

		$index = 0;
		foreach ($files as $file) {
			$extension = strtolower(CFileHelper::getExtension($file));

			$this->files[$index] = Docebo::createLmsUrl('deliverable/default/streamFile', array(
				'file' => $storageManager->fileUrl($file),
				'ext' => $extension
			));

			$ie = isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false);
			// If the file is a video and the browser is not a IE show direct link to the video,
			// because in IE we are forcing the videos to play

			if (!empty($extension) && strpos(self::EXTENSIONS_WHITELIST, $extension) !== false && !$ie)
				$this->files[$index] = $storageManager->fileUrl($file);
			$index++;
		}

		$this->oldAttributes = $this->attributes;
	}
}
