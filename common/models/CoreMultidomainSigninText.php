<?php

/**
 * This is the model class for table "core_multidomain_signin_text".
 *
 * The followings are the available columns in table 'core_multidomain_signin_text':
 * @property integer $id
 * @property integer $id_multidomain
 * @property string $lang_code
 * @property string $title
 * @property string $text
 *
 * The followings are the available model relations:
 * @property CoreMultidomain $multidomainClient
 */
class CoreMultidomainSigninText extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_multidomain_signin_text';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_multidomain, lang_code', 'required'),
			array('id_multidomain', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>32),
			array('title', 'length', 'max'=>512),
			array('text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_multidomain, lang_code, title, text', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'multidomainClient' => array(self::BELONGS_TO, 'CoreMultidomain', 'id_multidomain'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_multidomain' => 'Id Multidomain',
			'lang_code' => 'Lang Code',
			'title' => 'Title',
			'text' => 'Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_multidomain',$this->id_multidomain);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreMultidomainSigninText the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
	public static function updateTranslations($idClient, $titles, $texts, $purge = false) {

		// Purge all CLIENT translations first, if requested
		if ($purge === true) {
			$criteria = new CDbCriteria();
			$criteria->addCondition('id_multidomain=:id', array(':id' => $idClient));
			self::model()->deleteAll($criteria);
		}
		
		// Enumerate titles and texts and update/create models
		foreach ($titles as $langCode => $title) {
			$model = self::model()->findByAttributes(array('id_multidomain' => $idClient, 'lang_code' => $langCode));
			if (!$model) {
				$model = new self();
				$model->id_multidomain = $idClient;
			}
			$model->lang_code = $langCode;
			$model->title = $title;
			$model->save();
		}
		
		foreach ($texts as $langCode => $text) {
			$model = self::model()->findByAttributes(array('id_multidomain' => $idClient, 'lang_code' => $langCode));
			if (!$model) {
				$model = new self();
				$model->id_multidomain = $idClient;
			}
			$model->lang_code = $langCode;
			$model->text = $text;
			$model->save();
		}
		
		
	}
	
}
