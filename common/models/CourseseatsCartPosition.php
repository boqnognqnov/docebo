<?php
/**
 * @author Dzhuneyt dzuneyt@dzhuneyt.com
 */

class CourseseatsCartPosition extends LearningCourse implements IECartPosition {

	private $numSeats = 0;
	private $totalPrice = 0;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CourseseatsCartPosition the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return mixed id
	 */
	public function getCartItemId() {
		return "courseseats_" . $this->idCourse;
	}

	public function setSeats($numSeats){
		$this->numSeats = intval($numSeats);

		// Update item total price (num seats X course price)
		$this->getPrice();
	}
	public function getSeats(){
		return intval($this->numSeats);
	}

	/**
	 * @return float price
	 */
	public function getPrice() {
		// let plugins change the price
		Yii::app()->event->raise('OnRetrievingShoppingItemPrice', new DEvent($this, array('position' => &$this)));

		if($this->selling)
			$this->totalPrice = intval($this->numSeats) * floatval($this->prize);
		else
			$this->totalPrice = 0;

		return $this->totalPrice;
	}


	/**
	 * @return float price of a single seat
	 */
	public function getPriceSingleSeat() {
		// let plugins change the price
		Yii::app()->event->raise('OnRetrievingShoppingItemPrice', new DEvent($this, array('position' => &$this)));

		if ($this->selling) {
			return floatval($this->prize);
		} else {
			return 0;
		}
	}

	/**
	 * Required by plugins. DO NOT REMOVE
	 *
	 * @param float $price
	 *
	 * @throws CException
	 */
	public function setPrice( $price ) {
		Yii::log('Trying to set total price for cart item of type "courseseats". This is not recommended, since the price is calculated on the fly in CourseseatsCartPosition::getPrice()', CLogger::LEVEL_ERROR);

		$this->totalPrice = $price;
	}

	public function getPositionName() {
		return Yii::t('course', '{seats} seats for course {course}', array('{course}'=>$this->name, '{seats}'=>$this->numSeats));
	}

	public function getPositionNameSingleSeat() {
		return Yii::t('course', '{seats} seats for course {course}', array('{course}'=>$this->name, '{seats}'=>1));
	}

	public function getImage() {
		return $this->getCourseLogoUrl();
	}
}