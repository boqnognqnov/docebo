<?php

/**
 * This is the model class for table "app7020_ignore_list".
 *
 * The followings are the available columns in table 'app7020_ignore_list':
 * @property integer $id
 * @property integer $idUser
 * @property integer $idObject
 * @property string $objectType
 *
 * The followings are the available model relations:
 * @property CoreUser $idUser
 */
class App7020IgnoreList extends CActiveRecord{
	
	const OBJECT_TYPE_QUESTION = 'question';
	const OBJECT_TYPE_ASSET = 'asset';
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app7020_ignore_list';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idObject, objectType', 'required'),
			array('idUser, idObject', 'numerical', 'integerOnly'=>true),
			array('objectType', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, idObject, objectType', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUser' => 'Id User',
			'idObject' => 'Id Object',
			'objectType' => 'Object Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idObject',$this->idObject);
		$criteria->compare('objectType',$this->objectType,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020IgnoreList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getIgnoredExpertsRequests($userId, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()){
		if (empty($userId)) {
			return false;
		}
		
		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select(" q.*, cu.firstname, cu.lastname, cu.userid, eu.firstname, eu.lastname,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) as answersCount,
		                       (SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) AS viewesCount,
	                           (SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id ORDER BY created DESC LIMIT 1) as lastAnswerId,
	                           (SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id=lastAnswerId) as lastAnswerText,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id AND bestAnswer = 2) as haveBestAnswer, 
		                       (SELECT COUNT(a.id) FROM " . App7020Answer::model()->tableName() . " a 
	                               LEFT JOIN " . App7020QuestionHistory::model()->tableName() . " qh 
	                               ON  a.idQuestion=qh.idQuestion
                                   WHERE a.idQuestion = q.id AND (a.created > qh.viewed OR ISNULL(qh.viewed)) 
	                               AND (qh.idUser = :userId OR ISNULL(qh.idUser))) as newAnswers");

		$commandBase->params = array(":userId" => $userId);
		$commandBase->from(App7020IgnoreList::model()->tableName() . " il");
		
		$commandBase->join(App7020Question::model()->tableName() . " q", "il.idObject = q.id");
		
		$commandBase->join(App7020QuestionRequest::model()->tableName() . " qr", "qr.idQuestion = q.id");
		
		$commandBase->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		
		$commandBase->leftJoin(CoreUser::model()->tableName() . " eu", "qr.idExpert = eu.idst");
		
		$commandBase->leftJoin(App7020Assets::model()->tableName() . " c", "qr.idContent = c.id");
		
		$commandBase->where("il.idUser = :idUser", array(':idUser' => $userId));
		$commandBase->andWhere("il.objectType = '".App7020IgnoreList::OBJECT_TYPE_QUESTION."'");
		
		if($searchInput){
			$commandBase->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}

		$returnArray = $commandBase->queryAll(true);

		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);
		
		if(!empty($providerConfig['comboListViewId'])){
			unset($providerConfig['comboListViewId']);
		}
		
		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}
	
	
	public static function getIgnoredExpertsQuestions($userId, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()){
		if (empty($userId)) {
			return false;
		}
		
		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		
		
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select(" q.*, cu.firstname, cu.lastname, cu.userid,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id) as answersCount,
		                       (SELECT COUNT(id) FROM " . App7020QuestionHistory::model()->tableName() . " WHERE idQuestion = q.id) AS viewesCount,
	                           (SELECT id FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion=q.id ORDER BY created DESC LIMIT 1) as lastAnswerId,
	                           (SELECT content FROM " . App7020Answer::model()->tableName() . " WHERE id=lastAnswerId) as lastAnswerText,
	                           (SELECT COUNT(id) FROM " . App7020Answer::model()->tableName() . " WHERE idQuestion = q.id AND bestAnswer = 2) as haveBestAnswer, 
		                       (SELECT COUNT(a.id) FROM " . App7020Answer::model()->tableName() . " a 
	                               LEFT JOIN " . App7020QuestionHistory::model()->tableName() . " qh 
	                               ON  a.idQuestion=qh.idQuestion
                                   WHERE a.idQuestion = q.id AND (a.created > qh.viewed OR ISNULL(qh.viewed)) 
	                               AND (qh.idUser = :userId OR ISNULL(qh.idUser))) as newAnswers");

		$commandBase->params = array(":userId" => $userId);
		$commandBase->from(App7020IgnoreList::model()->tableName() . " il");
		
		$commandBase->join(App7020Question::model()->tableName() . " q", "il.idObject = q.id");
		
		$commandBase->leftJoin(CoreUser::model()->tableName() . " cu", "q.idUser = cu.idst");
		
		$commandBase->where("il.idUser = :idUser", array(':idUser' => $userId));
		
		if($searchInput){
			$commandBase->andWhere("q.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}
		
		$commandBase->andWhere("il.objectType = '".App7020IgnoreList::OBJECT_TYPE_QUESTION."'");

		$returnArray = array();

		$expertTopics = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($userId);//App7020TopicExpert::getTopicsByUser($userId);

		$returnArray = $commandBase->queryAll(true);

		/*foreach ($resultArray as $key => $value) {
			$questionTopics = App7020QuestionTopic::getTopicsByQuestion($value['id']);
			if (count(array_intersect($expertTopics, $questionTopics)) > 0) {
				if($topicFilter){
					if(count(array_intersect($topicFilter, $questionTopics)) > 0){
						$value['topics'] = App7020QuestionTopic::getQuestionsTopics($value['id']);
						$returnArray[] = $value;
					}
				} else {
					$value['topics'] = App7020QuestionTopic::getQuestionsTopics($value['id']);
					$returnArray[] = $value;
				}
			}
		}*/
		
		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);
		
		if(!empty($providerConfig['comboListViewId'])){
			unset($providerConfig['comboListViewId']);
		}
		
		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}
	
	public static function getIgnoredExpertsAssets($userId, $orderField = array('field' => 'created', 'vector' => 'DESC'), $asArrayDataProvider = true, $providerConfig = array()){
		if (empty($userId)) {
			return false;
		}

		$searchInput = trim(Yii::app()->request->getParam('search_input', false));
		
		
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("c.*");
		$commandBase->from(App7020IgnoreList::model()->tableName()." il");
		$commandBase->join(App7020Assets::model()->tableName()." c", $userId." = il.idUser AND il.idObject=c.id AND il.objectType='".App7020IgnoreList::OBJECT_TYPE_ASSET."'");
		
		if($searchInput){
			$commandBase->andWhere("c.title LIKE :search_input", array(':search_input' => '%' . $searchInput . '%'));
		}
		
		$returnArray = array();
		$expertTopics = App7020ChannelExperts::getAssignedChannelKeysToUserAsExpert($userId); //App7020TopicExpert::getTopicsByUser($userId);

		$returnArray = $commandBase->queryAll(true);
		/*foreach ($resultArray as $key => $value) {
			$contentTopics = App7020TopicContent::getTopicsByContent($value['id']);
			if (count(array_intersect($expertTopics, $contentTopics)) > 0) {
				if($topicFilter){
					if(count(array_intersect($topicFilter, $contentTopics)) > 0){
						$value['topics'] = App7020TopicContent::getContentsTopics($value['id']);
						$returnArray[] = $value;
					}
				} else {
					$value['topics'] = App7020TopicContent::getContentsTopics($value['id']);
					$returnArray[] = $value;
				}
			}
		}*/
		
		$returnArray = App7020Helpers::sortMultyArray($returnArray, $orderField['field'], $orderField['vector']);
		
		if(!empty($providerConfig['comboListViewId'])){
			unset($providerConfig['comboListViewId']);
		}

		if (!$asArrayDataProvider) {
			return $returnArray;
		} else {
			if (empty($providerConfig['pagination'])) {
				$providerConfig['pagination'] = array('pageSize' => 5);
			}
			$dataProvider = new CArrayDataProvider($returnArray, $providerConfig);
			return $dataProvider;
		}
	}
}
