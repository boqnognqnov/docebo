<?php

/**
 * This is the model class for table "learning_video_subtitles".
 *
 * The followings are the available columns in table 'learning_video_subtitles':
 * @property integer $id
 * @property integer $id_video
 * @property string $lang_code
 * @property integer $asset_id
 * @property integer $fallback
 * @property string $date_added
 * @property string $date_modified
 * @property integer $added_by
 *
 * The followings are the available model relations:
 * @property LearningVideo $video
 */
class LearningVideoSubtitles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_video_subtitles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_video, lang_code, asset_id, date_added', 'required'),
			array('id_video, asset_id, fallback, added_by', 'numerical', 'integerOnly'=>true),
			array('lang_code', 'length', 'max'=>50),
			array('date_modified', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_video, lang_code, asset_id, fallback, date_added, date_modified, added_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'video' => array(self::BELONGS_TO, 'LearningVideo', 'id_video'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_video' => 'Id Video',
			'lang_code' => 'Lang Code',
			'asset_id' => 'Asset',
			'fallback' => 'Fallback',
			'date_added' => 'Date Added',
			'date_modified' => 'Date Modified',
			'added_by' => 'Added By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_video',$this->id_video);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('fallback',$this->fallback);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_modified',$this->date_modified,true);
		$criteria->compare('added_by',$this->added_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningVideoSubtitles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
