<?php

/**
 * This is the model class for table "learning_certificate_assign_cp".
 *
 * The followings are the available columns in table 'learning_certificate_assign_cp':
 * @property integer $id_certificate
 * @property integer $id_path
 * @property integer $id_user
 * @property string $on_date
 * @property string $cert_file
 * @property integer $cert_number
 */
class LearningCertificateAssignCp extends CActiveRecord
{
    const CERTIFICATE_PATH = '../files/doceboLms/certificate/';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_certificate_assign_cp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_certificate, id_path, id_user', 'numerical', 'integerOnly'=>true),
			array('cert_file', 'length', 'max'=>255),
			array('on_date', 'safe'),
			// The following rule is used by search().
			array('id_certificate, id_path, id_user, on_date, cert_file, cert_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
            'learningCoursepath' => array(self::BELONGS_TO, 'LearningCoursepath', 'id_path'),
            'learningCoursepathUser' => array(self::BELONGS_TO, 'LearningCoursepathUser', array('id_user' => 'idUser', 'id_path' => 'idPath')),
        );
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('on_date medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_certificate' => 'Id Certificate',
			'id_path' => 'Id Path',
			'id_user' => 'Id User',
			'on_date' => 'On Date',
			'cert_file' => 'Cert File',
			'cert_number' => 'Cert Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_certificate',$this->id_certificate);
		$criteria->compare('id_path',$this->id_path);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('on_date',Yii::app()->localtime->fromLocalDateTime($this->on_date),true);
		$criteria->compare('cert_file',$this->cert_file,true);
		$criteria->compare('cert_number',$this->cert_number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCertificateAssignCp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Load model by id_path and user
     * @param $userId
     * @param $idPath
     * @param $idCertificate
     * @return CActiveRecord
     */
    public static function loadByUserPath($userId, $idPath, $idCertificate)
    {
        // Is the certificate generated?
        $generated = self::model()->findByAttributes(array(
            'id_user' => $userId,
            'id_path' => $idPath,
            'id_certificate' => $idCertificate,
        ));

        if(!$generated){
            // No certificate generated for this user/learning plan combination yet
            // See if there is a need and generate the certificate below
            if(self::userCompletedLearningPlan($userId, $idPath)) {
                try {
                    $substitution = Yii::app()->certificateTag->getSubstitution($userId, $idPath, TRUE);
                    Yii::app()->certificate->createForLearningPlan($idCertificate, $userId, $idPath, $substitution, FALSE);
                } catch(Exception $e){
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                }

                // The certificate should be existing now, since we just created it
                $generated = self::model()->findByAttributes(array(
                    'id_user' => $userId,
                    'id_path' => $idPath,
                    'id_certificate' => $idCertificate,
                ));
            }
        }

        return $generated;
    }

    /**
     * Checks if a user has completed the learning plan
     * @param $idUser
     * @param $idPath
     *
     * @return true if the passed user completed the learning plan
     */
    public static function userCompletedLearningPlan($idUser, $idPath) {
			// Calculate number of completed courses for this user
			$criteria = new CDbCriteria();
			$criteria->with['learningCoursepathCourses'] = array('joinType' => 'INNER JOIN');
			$criteria->condition = '(t.status = :status) AND t.idUser = :idUser AND learningCoursepathCourses.id_path = :id_path';
			$criteria->params[':status'] = LearningCourseuser::$COURSE_USER_END;
			$criteria->params[':id_path'] = $idPath;
			$criteria->params[':idUser'] = $idUser;
			$completed = LearningCourseuser::model()->count($criteria);

			// Calculate the total number of courses in the learning plan
			$criteria = new CDbCriteria();
			$criteria->condition = 't.id_path = :id_path';
			$criteria->params[':id_path'] = $idPath;
			$total = LearningCoursepathCourses::model()->count($criteria);

			return $total == $completed;
    }

    /**
     * Download certificate file if exists for the current model
     */
    public function downloadCertificateOLD() {
        $file = Docebo::filesPathAbs() .DIRECTORY_SEPARATOR. 'doceboLms'.DIRECTORY_SEPARATOR.'certificate'. DIRECTORY_SEPARATOR . $this->cert_file;
        if (file_exists($file)) {
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=" . urlencode($this->cert_file));
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Description: File Transfer");
            header("Content-Length: " . filesize($file));
            flush();
            $fp = fopen($file, "r");
            while (!feof($fp))
            {
                echo fread($fp, 65536);
                flush();
            }
            fclose($fp);
        }else{
	        Yii::log('Certificate file not found, can not download. Requested: '.$file, CLogger::LEVEL_ERROR);
        }
    }


    /**
     * Download certificate file if exists for the current model
     */
    public function downloadCertificate() {
    	$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
    	$storage->sendFile($this->cert_file, $this->cert_file);
    }



    public function deleteCertFile() {
    	if ($this->cert_file) {
    		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
    		$storage->remove($this->cert_file);
    	}
    }

    /**
     * (non-PHPdoc)
     * @see CActiveRecord::beforeDelete()
     */
    public function beforeDelete()
    {
    	// Delete also files
    	$this->deleteCertFile();
    	return parent::beforeDelete();
    }



}
