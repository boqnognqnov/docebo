<?php

/**
 * This is the model class for table "core_group".
 * The followings are the available columns in table 'core_group':
 * @property integer $idst
 * @property string $groupid
 * @property string $description
 * @property string $hidden
 * @property string $type
 * @property string $show_on_platform
 * @property string $additional_fields
 * @property integer $assign_rules
 * @property string $assign_rules_logic_operator
 * @property string $assignment_type
 * The followings are the available model relations:
 * @property CoreGroupAssignRules[] $coreGroupAssignRules
 * @property CoreGroupMembers[] $coreGroupMembers
 */
class CoreGroup extends CActiveRecord {
	public static $ACL_SEPARATOR = '/';
	public static $ACL_INFO_GROUPHIDDEN = 'hidden';
	public $confirm;

    const MANUALY_ASSIGN_RULES = 0;
    const AUTO_ASSIGN_RULES = 1;
	const GROUP_PU = '/framework/level/admin';
	const GROUP_GODADMINS = '/framework/level/godadmin';
	const GROUP_USERS = '/framework/level/user';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreGroup the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_group';
	}

	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['modelSelect'] = array('class' => 'ModelSelectBehavior');
		return $behaviors;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst', 'numerical', 'integerOnly' => true),
			array('groupid', 'unique', 'message' => Yii::t('standard', 'This element already exists')),
			array('groupid', 'required', 'message' => Yii::t('standard', 'Please fill all required fields')),
			array('groupid, assignment_type', 'length', 'max' => 255),
			array('hidden', 'length', 'max' => 5),
			array('type', 'length', 'max' => 9),
			array('description', 'safe'),
            array('assign_rules_logic_operator', 'safe'),
            array('assign_rules', 'safe'),
            array('additional_fields', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, groupid, description, hidden, type, show_on_platform, additional_fields', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'coreGroupAssignRules' => array(self::HAS_MANY, 'CoreGroupAssignRules', 'idGroup'),
			'settings' => array(self::HAS_MANY, 'CoreSettingGroup', 'idst'),
			'coreGroupMembers' => array(self::HAS_MANY, 'CoreGroupMembers', 'idst'),
			'coreGroupFields' => array(self::HAS_MANY, 'CoreGroupFields', 'idst'),
			'users' => array(self::MANY_MANY, 'CoreUser', 'core_group_members(idst, idstMember)'),
			'roles' => array(self::MANY_MANY, 'CoreRole', 'core_role_members(idstMember, idst)'),
		);
	}

	/**
	 * @return array the scope definition.
	 */
	public function scopes() {
		return array();
	}

	public function afterDelete()
	{
		//cleaening up enrollment rules assignments
		CoreEnrollRuleItem::model()->deleteAllByAttributes(array('item_type' => 'group', 'item_id' => $this->idst));

		//cleaning up catalogs assignments
		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			LearningCatalogueMember::model()->deleteAllByAttributes(array('idst_member' => $this->idst));
		}

		if(PluginManager::isPluginActive('GamificationApp') && $this->isUserGroup) {
			GamificationContest::cleanUpGroups($this->idst);
			GamificationLeaderboard::cleanUpGroups($this->idst);
		}

		//cleaning up notifications
		if(PluginManager::isPluginActive('NotificationApp')) {
			CoreNotificationUserFilter::model()->deleteAllByAttributes(array('idItem' => $this->idst, 'type' => CoreNotificationUserFilter::NOTIF_FILTER_GROUP));
			if(strpos($this->groupid, '/framework/adminrules/') !== false)
				CoreNotification::model()->updateAll(array('puProfileId' => 0), 'puProfileId = :puProfileId', array('puProfileId' => $this->idst));
		}

		return true;
	}

	public function group($groupKey) {
		$criteria = new CDbCriteria;
		$criteria->addSearchCondition('t.groupid', $groupKey);
		$this->getDbCriteria()->mergeWith($criteria);
		return $this;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'idst' => 'Id',
			'groupid' => Yii::t('standard', '_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'hidden' => Yii::t('standard', '_HIDDEN'),
			'type' => Yii::t('standard', '_TYPE'),
			'show_on_platform' => 'Show on platform',
			'assignment_type' => 'Assignment type',
		);
	}

	public function beforeSave() {
		if (!parent::beforeSave()) {
			return false;
		}
		// if idst is null we have to generate it through core_st
		if ($this->isNewRecord && !$this->idst) {

			$st = new CoreSt();
			if (!$st->save()) {
				return false;
			}
			$this->idst = $st->idst;
		}
		return true;
	}

	public function dataProvider() {
		$criteria = new CDbCriteria;
//		$criteria->compare('idst', $this->idst);
		$criteria->compare('t.groupid', $this->groupid, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('t.hidden', $this->hidden, true);
		$criteria->compare('t.type', $this->type, true);
//		$criteria->compare('show_on_platform', $this->show_on_platform, true);

		//check for power user visibility
		//TO DO: implement this in a better way;
		if (in_array($this->scenario, array('group_management', 'search', 'insert'))) {
			if (Yii::app()->user->getIsPu()) {
				$criteria->join = " JOIN ".CoreAdminTree::model()->tableName()." at ON (t.idst = at.idst AND at.idstAdmin = :idst_admin)";
				$criteria->params[':idst_admin'] = Yii::app()->user->id;
			}
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.idst desc';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderProfile() {
		$this->hidden = 'true';

		$criteria = new CDbCriteria;
		$criteria->compare('t.groupid', $this->groupid, true);
		$criteria->compare('t.hidden', $this->hidden, true);
		$criteria->compare('t.type', $this->type, true);
		$criteria->with = array('coreGroupMembers');
		$criteria->scopes = array('group' => array('groupKey' => '/framework/adminrules'));

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.'.$attributeName;
		}
		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.idst desc';
		$config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

		return new CActiveDataProvider(get_class($this), $config);
	}

	public function dataProviderReport() {
		$criteria = new CDbCriteria();

		$criteria->compare('t.hidden', 'false');

		$config = array(
			'criteria' => $criteria,
		);
		return new CActiveDataProvider(get_class($this), $config);
	}

	public function countGroupMembers() {
		return Yii::app()->db->createCommand()
			->select('count(*)')
			->from(CoreGroupMembers::model()->tableName())
			->where('idst = :idst', array(':idst' => $this->idst))
			->queryScalar();
    }

	public function renderGroupMembers($icon = false) {
		$content = CHtml::link(
			$this->countGroupMembers().($icon
				? ' <span class="enrolled"></span>'
				: null),
			Yii::app()->createUrl('groupManagement/assign', array('id' => $this->idst))
		);
		return $content;
	}

    public function renderEditGroupButton(){
        $content = CHtml::link('', Yii::app()->createUrl('groupManagement/updateGroup', array('idst' => $this->idst)), array(
            'class' => 'open-dialog edit-action',
            'data-dialog-class' => 'new-group',
            'data-dialog-title' => ''
        ));

        return $content;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('idst', $this->idst);
		$criteria->compare('groupid', $this->groupid, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('hidden', $this->hidden, true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('show_on_platform', $this->show_on_platform, true);
        $criteria->compare('assign_rules',$this->assign_rules);
		$criteria->compare('assign_rules_logic_operator',$this->assign_rules_logic_operator,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}


	/**
	 * An userid/groupid/roleid can be absolute (if start with ACL_SEPARATOR charater) or
	 * relative to actual DoceboACLManager context.
	 * This function return always the absolute id of the given id. ( If it's
	 *  an absolute id return it unchanged)
	 * @param string $id relative or absolute userid
	 * @return string absolute id
	 */
	public static function absoluteId($id) {

		if (strlen($id) == 0) {
			return $id;
		}
		if ($id{0} == self::$ACL_SEPARATOR) {
			return $id;
		}
		return self::$ACL_SEPARATOR.$id;
	}

	/**
	 * An userid/groupid/roleid can be absolute (if start with ACL_SEPARATOR charater) or
	 * relative to actual DoceboACLManager context.
	 * This function return always the relative id of the given id; normally
	 *  remove the context from $id if it's absolute.
	 * If it's an relative id return it unchanged.
	 * @param string $id relative or absolute userid
	 * @return string absolute id
	 */
	public function relativeId($id = null) {
		if ($id === null) {
			$id = $this->groupid;
		}
		if ($id{0} != self::$ACL_SEPARATOR) {
			return $id;
		}
		$context = self::$ACL_SEPARATOR;
		$max = (strlen($context) < strlen($id)) ? strlen($context) : strlen($id);
		if (strncmp($context, $id, $max) == 0) {
			return (substr($id, $max));
		} else {
			return $context.$id;
		}
	}

	public function relativeProfileId() {
		return str_replace('/framework/adminrules/', '', $this->groupid);
	}

	public function absoluteProfileId() {
		if (strpos($this->groupid, '/framework/adminrules/') === false) {
			return '/framework/adminrules/' . $this->groupid;
		}

		return $this->groupid;
	}

	public function getOrgChartId()
	{
		$id = null;
		if (strpos($this->groupid, '/oc_') !== false)
			$id = str_replace("/oc_", "", $this->groupid);
		else if (strpos($this->groupid, '/ocd_') !== false)
			$id = str_replace("/ocd_", "", $this->groupid);

		return $id;
	}

	/**
	 * get security token of a group
	 * @param string $groupid id of the group
	 * @return mixed security token associated to groups or FALSE
	 **/
	public static function getGroupST($groupid) {

		$row = CoreGroup::model()->findByAttributes(array(
			'groupid' => self::absoluteId($groupid)
		));

		return ($row ? $row->idst : false);
	}


	/**
	 * return the group info
	 * @param mixed $idst the security token of the group to get, FALSE if
	 *          $groupid is assigned
	 * @param string $groupid the groupid of the group to get, FALSE if
	 *          $idst is assigned
	 * @return mixed array with user informations:
	 *        - idst, groupid, description, hidden
	 *        - FALSE if user is not found
	 */
	public static function getGroup($idst, $groupid) {

		$criteria = new CDbCriteria();
		$criteria->select = 'idst, groupid, description, hidden, type, show_on_platform';
		$params = array();

		if ($idst !== false) {
			$criteria->condition = 'idst = :idst';
			$params[':idst'] = $idst;
		} elseif ($groupid !== false) {
			$criteria->condition = 'groupid = :groupid';
			$params[':groupid'] = self::absoluteId($groupid);
		} else {
			return false;
		}

		if ($params) {
			$criteria->params = $params;
		}

		$rows = CoreGroup::model()->findAll($criteria);
		if (!empty($rows)) {
			$row = $rows[0];
			$row[self::$ACL_INFO_GROUPHIDDEN] = ($row[self::$ACL_INFO_GROUPHIDDEN] == 'true');
			return $row;
		} else {
			return false;
		}
	}

	/**
	 * Return list of User Levels (User, Power User, Superadmin, ...)
	 *
	 * @param string $checkPowerUserApp  Do or Do NOT check if Power User App is enabled
	 * @param string $excludeGodadmin
	 * @param string $includePowerUser
	 * @param string $groupIdAsKey If TRUE, 'groupid' will be used as key in the returned array (e.g. /framework/../.. instead of <integer ID>)
	 * @return array
	 */
	public static function getAdminLevels($checkPowerUserApp = false, $excludeGodadmin = false, $includePowerUser=true, $groupIdAsKey=false) {
		$criteria = new CDbCriteria();
		$criteria->compare('groupid', '/framework/level', true);
		$criteria->order = 'groupid DESC';

		if($excludeGodadmin){
			$criteria->addCondition("groupid <> '/framework/level/godadmin'");
		}

		if(!$includePowerUser){
			$criteria->addCondition("groupid <> '/framework/level/admin'");
		}


		$models = self::model()->findAll($criteria);

		$levels = array();
		foreach ($models as $model) {
			$key = $groupIdAsKey ? $model->groupid : $model->idst;
            if ($model->groupid != '/framework/level/admin' || !$checkPowerUserApp || (PluginManager::isPluginActive('PowerUserApp') === true))
                $levels[$key] = Yii::t('admin_directory', '_DIRECTORY_'.$model->groupid);
		}

		return $levels;
	}

	/**
	 * Return list of User Levels (User, Power User, Superadmin, ...) in json format
	 * [{$value: 'value, $text: 'text'},{$value: 'value1', $text: 'text1'}, ...]
	 *
	 * @param bool $checkPowerUserApp
	 * @param bool $excludeGodadmin
	 * @param bool $includePowerUser
	 * @param string $value
	 * @param string $text
	 * @return string
	 */
	public static function getAdminLevelsToJson($checkPowerUserApp = false, $excludeGodadmin = false, $includePowerUser=true, $value = 'value', $text = 'text') {
		$levels = array();
		foreach (self::getAdminLevels($checkPowerUserApp, $excludeGodadmin, $includePowerUser) as $key=>$lang) {
			$levels[] = array(
				$value => $key,
				$text => $lang
			);
		}

		return json_encode($levels);
	}

	/**
	 * Convert a "groupid" to "idst" in this model
	 *
	 * @param $label
	 *
	 * @return bool|int
	 */
	/**
	 * Convert a "groupid" to "idst" in this model
	 *
	 * @param $label
	 *
	 * @return bool|int
	 */
	public function getGroupIdByLabel($label){
		$res = self::model()->findByAttributes(array('groupid'=>$label));
		if($res) return $res->idst;
		return false;
	}

	public function getIsOrgChart()
	{
		return (strpos($this->groupid, '/oc_') !== false || strpos($this->groupid, '/ocd_') !== false) ? true : false;
	}

	public function getIsUserGroup()
	{
		return $this->hidden == 'false' ? true : false;
	}


	/**
	 * Return array of existing (not deleted) IDs of all users, members of THIS group. NOT MODELS!!
	 */
	public function getUsers() {
		$command = Yii::app()->db->createCommand();
		$command->select('user.idst idUser');
		$command->from('core_group_members gm');
		$command->join('core_user user','gm.idstMember=user.idst');

		$command->where('gm.idst=:idst', array(':idst' => $this->idst));

		$rows = $command->queryAll();
		$result = array();
		foreach ($rows as $row) {
			$result[] = (int) $row['idUser'];
		}
		return $result;
	}

	public static function getGroupsByIdSt($idsts, $descendats = true)
	{
		if(!is_array($idsts) || empty($idsts))
			return array();

		$users = array();

		//Cast all element to in, just for security reasons
		foreach($idsts as $key => $value)
			$idsts[$key] = (int)$value;

		//Retrive the users
		$users = array_merge($users, CoreUser::getUsersByIdSt($idsts));

		//Retrive the groups
		$groups = array_diff($idsts, $users);

		if($descendats && !empty($groups))
		{
			$command = Yii::app()->db->createCommand();
			$command->select('g.idst, ct.iLeft, ct.iRight');
			$command->from(CoreGroup::model()->tableName().' AS g');
			$command->join(CoreOrgChartTree::model()->tableName().' AS ct', 'ct.idst_ocd = g.idst');
			$command->where("g.idst IN (".implode(',', $groups).")");

			$descendants_folders = $command->queryAll();
			$query_filters = array();

			foreach($descendants_folders as $folder)
				$query_filters[] = '(iLeft >= '.$folder['iLeft'].' AND iRight <= '.$folder['iRight'].')';

			if(!empty($query_filters))
			{
				$command = Yii::app()->db->createCommand();
				$command->select('idst_oc');
				$command->from(CoreOrgChartTree::model()->tableName());
				$command->where(implode(' OR ', $query_filters));

				$groups = array_merge($groups, $command->queryColumn());
			}
		}

		return $groups;
	}

	/**
	 * Check is this org chart(branch) with the descendants or not
	 * @param int $idsts org chart(branch) id
	 * @return bool
	 */
	public static function isDescendantsBranch($idsts)
	{
		//
		$coreGroupId = Yii::app()->db->createCommand('SELECT idOrg FROM core_org_chart_tree WHERE idst_ocd = ' . $idsts)->queryRow();
		$coreGroupId = !empty($coreGroupId['idOrg']) ? $coreGroupId['idOrg'] : -1;

		// if there is a group id
		if ($coreGroupId != -1) {
			// this is an org chart group/branch/ with descendants
			return true;
		}

		// it is not an org chart group/branch/ with descendants
		return false;
	}


	public static function hasGroupAssignRules($groupId)
	{
		if (!empty($groupId)) {
			$group = self::model()->findByPk($groupId);
			if ($group->assign_rules == self::AUTO_ASSIGN_RULES) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}



	public function renderGroupAssigendRules()
	{
		$content = null;
		if ($this->assign_rules == 0) {
			$content = "<span>" . Yii::t('standard', '_MANUAL') . "</span>";
		} else {
			$content = CHtml::link(Yii::t('group_management', 'Automatic'), Yii::app()->createUrl('groupManagement/editGroupAssignment', array('idst' => $this->idst)), array(
				'class' => 'assign-auto open-dialog',
				'data-dialog-class' => 'new-group',
				'data-dialog-title' => Yii::t('standard', '_MOD')
			));
		}
		return $content;
	}

	public static function getGroupsList($includeHidden = false)
	{
		$hidden = ($includeHidden) ? 'true' : 'false';
		$command = Yii::app()->db->createCommand();
		$command->select('g.idst, TRIM(LEADING "/" FROM g.groupid) as gid');
		$command->from(CoreGroup::model()->tableName() . ' AS g');
		$command->where("g.hidden =:hidden", array(':hidden' => $hidden));
		$visibleGroups = $command->queryAll();

		return ($visibleGroups && !empty($visibleGroups)) ? $visibleGroups : false;
	}



	/**
	 * Clean an users group from invalid users references
	 */
	public function cleanUsersGroup() {
		$idstGroup = $this->idst;
		//first check if the requested group is a valid users group
		if ($this->hidden == 'false') {
			//search for invalid user references
			$query = "SELECT gm.idstMember "
				. " FROM " . CoreGroupMembers::model()->tableName() . " gm "
				. " LEFT JOIN " . CoreUser::model()->tableName() . " u ON (gm.idstMember = u.idst) "
				. " WHERE gm.idst = :idst_group AND u.idst IS NULL";
			$reader = Yii::app()->db->createCommand($query)->query(array(':idst_group' => $idstGroup));
			//if any invalid reference can be found then clean it
			while ($record = $reader->read()) {
				if (!isset($deleteCommand)) {
					$deleteQuery = "DELETE FROM " . CoreGroupMembers::model()->tableName() . " WHERE idst = :idst_group AND idstMember = :idst_member";
					$deleteCommand = Yii::app()->db->createCommand($deleteQuery);
				}
				$params = array(
					':idst_group' => $idstGroup,
					':idst_member' => $record['idstMember']
				);
				$deleteCommand->execute($params);
				//log the action
				Yii::log("Invalid user reference #".$record['idstMember']." detected in group #".$idstGroup.". It has been removed from database", 'warning');
			}
		}
	}

	/**
	 * Check if a group is a normal, general user group
	 * @param integer $id
	 */
	public static function isGroup($id) {
		return self::model()->exists("idst=:id AND hidden='false'", array(":id" => $id));
	}

	/**
	 * Check if a group is an Orgchart group (oc)
	 * @param integer $id
	 */
	public static function isOrgchartNode($id) {
		return self::model()->exists("idst=:id AND hidden='true' AND groupid LIKE '/oc\_%'", array(":id" => $id));
	}

	/**
	 * Check if a group is an Orgchart Folder group (ocd), i.e. contains all users of the branch and its descendants
	 * @param integer $id
	 */
	public static function isOrgchartFolder($id) {
		return self::model()->exists("idst=:id AND hidden='true' AND groupid LIKE '/ocd\_%'", array(":id" => $id));
	}
}
