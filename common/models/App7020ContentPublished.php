<?php

/**
 * This is the model class for table "app7020_content_published".
 *
 * The followings are the available columns in table 'app7020_content_published':
 * @property integer $id
 * @property integer $idUser
 * @property integer $idContent
 * @property string $datePublished
 * @property string $actionType
 *
 * The followings are the available model relations:
 * @property CoreUser $publisher
 * @property App7020Content $content
 */
class App7020ContentPublished extends CActiveRecord {

	const ACTION_TYPE_PUBLISH = 1;
	const ACTION_TYPE_EDIT = 2;
	const ACTION_TYPE_FINISHED = 3;

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'app7020_content_published';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idContent', 'required'),
			array('id, idUser, idContent, actionType', 'numerical', 'integerOnly' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUser, idContent, datePublished', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'publisher' => array(self::BELONGS_TO, 'CoreUser', 'idUser'),
			'content' => array(self::BELONGS_TO, 'App7020Assets', 'idContent'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'idUser' => 'Id User',
			'idContent' => 'Id Content',
			'datePublished' => 'Date Published',
		);
	}
	public function afterSave() {
		if($this->actionType == self::ACTION_TYPE_PUBLISH) {
			// Raise event for expert approves asset.
			$_SESSION['time'] = NULL;
			Yii::app()->event->raise('ExpertReachedAGoal', new DEvent($this, array('userId'=> Yii::app()->user->id, 'goal'=>2)));
		}
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('idUser', $this->idUser);
		$criteria->compare('idContent', $this->idContent);
		$criteria->compare('datePublished', $this->datePublished, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return App7020ContentPublished the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Get Local time for publishing of this asset
	 * @return string
	 */
	function getLocalTime() {
		return Yii::app()->localtime->toLocalTime($this->datePublished);
	}

	/**
	 * Get Local date for publishing of this asset
	 * @return string
	 */
	function getLocalDate() {
		return Yii::app()->localtime->toLocalDate($this->datePublished);
	}

	/**
	 * Save in table "app7020_content_published" content as "Edited"
	 * @param int $idContent
	 * @return boolean
	 */
	public static function saveEditAction($idContent, $convertionStatus = false) {
		if ($convertionStatus == App7020Assets::CONVERSION_STATUS_FINISHED) {
			$contentFinished = App7020ContentPublished::model()->findByAttributes(array(
				'idContent' => $idContent,
				'actionType' => App7020ContentPublished::ACTION_TYPE_FINISHED
			));
			if (!$contentFinished) {
				$contentFinished = new App7020ContentPublished();
				$contentFinished->idUser = Yii::app()->user->id;
				$contentFinished->idContent = $idContent;
				$contentFinished->actionType = App7020ContentPublished::ACTION_TYPE_FINISHED;
				if ($contentFinished->save()) {
					return true;
				} else {
					return false;
				}
			}
		}
		
		$modelContentPublished = App7020ContentPublished::model()->findByAttributes(array(
			'idContent' => $idContent,
			'actionType' => App7020ContentPublished::ACTION_TYPE_EDIT
		));
		if ($modelContentPublished) {
			$modelContentPublished->idUser = Yii::app()->user->id;
			$modelContentPublished->datePublished = new CDbExpression('NOW()');
		} else {
			$modelContentPublished = new App7020ContentPublished();
			$modelContentPublished->idUser = Yii::app()->user->id;
			$modelContentPublished->idContent = $idContent;
			$modelContentPublished->actionType = App7020ContentPublished::ACTION_TYPE_EDIT;
		}
		if ($modelContentPublished->save()) {
			return true;
		} else {
			return false;
		}
	}

	public static function getLastEdited($idContent) {
		$modelContentEdited = App7020ContentPublished::model()->findAllByAttributes(
				array(
					'idContent' => $idContent,
					'actionType' => App7020ContentPublished::ACTION_TYPE_EDIT
				), array(
					'order' => 'datePublished DESC',
					'limit' => 1
				)
		);
		if ($modelContentEdited) {
			return $modelContentEdited[0];
		} else {
			$modelContentEdited = App7020ContentPublished::model()->findAllByAttributes(
					array(
						'idContent' => $idContent,
					), array(
						'condition' => 'actionType = '.App7020ContentPublished::ACTION_TYPE_PUBLISH.' OR actionType = '.App7020ContentPublished::ACTION_TYPE_FINISHED,
						'order' => 'datePublished DESC',
						'limit' => 1
					)
			);

			if ($modelContentEdited) {
				return $modelContentEdited[0];
			} else {
				return false;
			}
		}
	}

}
