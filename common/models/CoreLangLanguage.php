<?php

/**
 * This is the model class for table "core_lang_language".
 *
 * The followings are the available columns in table 'core_lang_language':
 * @property string $lang_code
 * @property string $lang_description
 * @property string $lang_browsercode
 * @property string $lang_direction
 * @property integer $lang_active
 *
 * The followings are the available model relations:
 * @property CoreLangText[] $texts
 */
class CoreLangLanguage extends CActiveRecord {

    const STATUS_VALID = 1;
    const STATUS_NOTVALID = 0;

	public $confirm; // filter if true contain users from children nodes
    public $set_as_default = false; // if true make it default language for system

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return CoreLangLanguage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'core_lang_language';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_active', 'numerical', 'integerOnly' => true),
            array('lang_code, lang_browsercode', 'length', 'max' => 50),
            array('lang_description', 'length', 'max' => 255),
            array('lang_direction', 'length', 'max' => 3),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('lang_code, lang_description, lang_browsercode, lang_direction, lang_active', 'safe', 'on' => 'search'),
            array('lang_description, lang_browsercode, lang_direction, set_as_default', 'safe', 'on' => 'update'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'texts' => array(self::MANY_MANY, 'CoreLangText', 'core_lang_translation(lang_code, id_text)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'lang_code' => 'Lang Code',
            'lang_description' => Yii::t('standard', '_DESCRIPTION'),
            'lang_browsercode' => Yii::t('admin_lang', '_LANG_BROWSERCODE'),
            'lang_direction' => Yii::t('admin_lang', '_ORIENTATION'),
            'lang_active' => 'Lang Active',
            'set_as_default' => Yii::t('standard', '_DEFAULT_LANGUAGE'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('lang_code', $this->lang_code, true);
        $criteria->compare('lang_description', $this->lang_description, true);
        $criteria->compare('lang_browsercode', $this->lang_browsercode, true);
        $criteria->compare('lang_direction', $this->lang_direction, true);
        $criteria->compare('lang_active', $this->lang_active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
        ));
    }


	//active languages list internal cache variable, so the list is retrieved from DB one time only in the session
	protected static $_cache_active_languages = null;

	//active languages cache loader
	protected static function _loadActiveLanguages()
	{
		if (self::$_cache_active_languages === null) { //first check if cache is already filled, if so then do nothing else
			//prepare cache variable
			self::$_cache_active_languages = array(
				'lang_code' => array(),
				'browser_code' => array()
			);
			//retrieve data from DB
			$models = self::model()->findAllByAttributes(array('lang_active' => 1));
			if (!empty($models)) {
				foreach ($models as $model) {
					//fill data in cache
					self::$_cache_active_languages['lang_code'][$model->lang_code] = $model->lang_description;
					self::$_cache_active_languages['browser_code'][$model->lang_browsercode] = $model->lang_description;
				}
			}
		}
	}

	//active languages cache unloader
	protected static function _unloadActiveLanguages()
	{
		self::$_cache_active_languages = null; //just dispose pre-load data, if any
	}

	public static function getActiveLanguages()
	{
		/*
		$models = self::model()->findAllByAttributes(array('lang_active' => 1));

		$languages = array();
		foreach ($models as $model) {
			$languages[$model->lang_code] = $model->lang_description;
		}
		return $languages;
		*/
		self::_loadActiveLanguages();
		return self::$_cache_active_languages['lang_code'];
	}

	/**
	 * Return list of active languages in json format
	 * [{$value: 'value, $text: 'text'},{$value: 'value1', $text: 'text1'}, ...]
	 *
	 * @param string $value
	 * @param string $text
	 * @return string
	 */
	public static function getActiveLanguagesToJson($value = 'value', $text = 'text')
	{
		$languages = array();
		foreach (self::getActiveLanguages() as $key=>$lang) {
			$languages[] = array(
				$value => $key,
				$text => $lang
			);
		}

		return json_encode($languages);
	}


	/**
	 * Returns an array of active languages in form of "en" => "English", i.e. using browsercodes as key
	 *
	 * @return array
	 */
	public static function getActiveLanguagesByBrowsercode()
	{
		/*
		$models = self::model()->findAllByAttributes(array('lang_active' => 1));

		$languages = array();
		foreach ($models as $model) {
			$languages[$model->lang_browsercode] = $model->lang_description;
		}
		return $languages;
		*/
		self::_loadActiveLanguages();
		return self::$_cache_active_languages['browser_code'];
	}


	/**
	 * After save behavior
	 */
	public function afterSave()
	{
		parent::afterSave();

		//clear cached active languages list, since it may have been modified
		self::_unloadActiveLanguages();

		//if language is set as default, relative lms setting must be updated
		if ($this->set_as_default == true) {
			$setting = CoreSetting::getEntity(CoreSetting::$params['default_language']);
			if ($setting === false) {
				$setting = new CoreSetting;
				$setting->param_name = CoreSetting::$params['default_language'];
				$setting->param_value = $this->lang_code;
				$setting->value_type = 'language';
				$setting->max_size = 255;
				$setting->pack = 0;
				$setting->regroup = 1;
				$setting->sequence = 3;
				$setting->param_load = 1;
				$setting->hide_in_modify = 0;
				$setting->save();
			} else {
				$setting->param_value = $this->lang_code;
				$setting->update();
			}

			Yii::app()->setLanguage($this->lang_code);
		}
	}

    /**
     * Check is this language is default for system
     * 
     * @return boolean
     */
    public function isDefaultLanguage() {
        $default = Settings::get(CoreSetting::$params['default_language']);

        if ($default == $this->lang_code) {
            $this->set_as_default = true;
            return true;
        } else
            return false;
    }

	/**
	 * Activate language
     * 
     * @return boolean
	 */
	public function activate() {
		$this->lang_active = self::STATUS_VALID;
		return $this->save();
	}

	/**
	 * Deactivate language
     * 
     * @return boolean
	 */
	public function deactivate() {
		$this->lang_active = self::STATUS_NOTVALID;
		return $this->save();
	}

    /**
     * Data provider for grid
     * 
     * @return \CActiveDataProvider
     */
    public function dataProvider() {
        $criteria = new CDbCriteria;
        $criteria->compare('t.lang_code', $this->lang_code, true);
        $criteria->compare('lang_description', $this->lang_description, true);

        #$criteria->with = array('coreGroupMembers');

        $sortAttributes = array();
        foreach ($this->attributeNames() as $attributeName) {
            $sortAttributes[$attributeName] = 't.' . $attributeName;
        }
        $config['criteria'] = $criteria;
        $config['sort']['attributes'] = $sortAttributes;
        $config['sort']['defaultOrder'] = 't.lang_code asc';
        $config['pagination'] = array('pageSize' => Settings::get('elements_per_page', 10));

        return new CActiveDataProvider(get_class($this), $config);
    }

    /**
     * Get list of options for lang_direction column
     * 
     * return array
     */
    public static function getDirectionOptions() {
        $arr = array(
            'rtl' => Yii::t('admin_lang', '_DIRECTION_RTL'),
            'ltr' => Yii::t('admin_lang', '_DIRECTION_LTR'),
        );
        return $arr;
    }

    /**
     * Get source of flag icon
     * 
     * return string
     */
    public function getFlagSrc() {
    	return Lang::getFlagSrcByCode($this->lang_code);
    }

    /**
     * Widget for activate and deactivate of language
     * 
     * @return string
     */
    public function getChangeStatusLink() {
        if ($this->lang_active == self::STATUS_VALID) {
            $class = 'suspend-action';
            $linkTitle = 'Deactivate';
            $modalTitle = 'Deactivate';
            $status = self::STATUS_NOTVALID;
        } else {
            $class = 'activate-action';
            $linkTitle = 'Activate';
            $modalTitle = 'Activate';
            $status = self::STATUS_VALID;
        }


        /*$content = Yii::app()->controller->renderPartial('//common/_modal', array(
            'config' => array(
                'class' => $class,
                'linkTitle' => $linkTitle,
                'modalTitle' => $modalTitle,
                'url' => 'langManagement/changeStatus&status=' . $status . '&lang=' . $this->lang_code,
                'buttons' => array(
                    array(
                        'type' => 'submit',
                        'title' => 'Confirm',
                    ),
                    array(
                        'type' => 'cancel',
                        'title' => 'Cancel',
                    ),
                ),
                'afterLoadingContent' => 'hideConfirmButton();',
                'afterSubmit' => 'updateLangContent',
            ),
            ), true);*/
        $content = 'Oks';
        return $content;
    }

    /**
     * Get default language from localization tool
     *
     * @return string
     */
    public static function getDefaultLanguage() {
        $setting = CoreSetting::getEntity(CoreSetting::$params['default_language']);
        if ($setting)
            return $setting->param_value;
        else
            return 'english';
    }

}