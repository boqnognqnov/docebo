<?php

/**
 * This is the model class for table "core_group_assign_rules".
 *
 * The followings are the available columns in table 'core_group_assign_rules':
 * @property integer $id
 * @property integer $idGroup
 * @property integer $idFieldCommon
 * @property string $condition_type
 * @property string $condition_value
 *
 * The followings are the available model relations: 
 * @property CoreGroup $idGroup0
 */
class CoreGroupAssignRules extends CActiveRecord
{
    
    const CONDITION_TYPE_EQUAL = 'equal';
    const CONDITION_TYPE_NOT_EQUAL = 'not equal';
    const CONDITION_TYPE_CONTAINS = 'contains';
    const CONDITION_TYPE_IN = 'in';
    const CONDITION_TYPE_IS = 'is';
    const CONDITION_TYPE_FROM_TO = 'from-to';
    const CONDITION_TYPE_BEFORE = 'before';
    const CONDITION_TYPE_AFTER = 'after';
    
    const LOGIC_OPERATOR_AND = 'AND';
    const LOGIC_OPERATOR_OR = 'OR';
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_group_assign_rules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idGroup, idFieldCommon, condition_type, condition_value', 'required'),
			array('idGroup, idFieldCommon', 'numerical', 'integerOnly'=>true),
			array('condition_type', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idGroup, idFieldCommon, condition_type, condition_value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(			
			'group' => array(self::BELONGS_TO, 'CoreGroup', 'idGroup'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idGroup' => 'Id Group',
			'idFieldCommon' => 'Id Field Common',
			'condition_type' => 'Condition Type',
			'condition_value' => Yii::t('group_management', 'Condition Value')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idGroup',$this->idGroup);
		$criteria->compare('idFieldCommon',$this->idFieldCommon);
		$criteria->compare('condition_type',$this->condition_type,true);
		$criteria->compare('condition_value',$this->condition_value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CoreGroupAssignRules the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    /**
     * Returns the rule type.
     * If it didnt find any additional field, returns false.
     * 
     * @return boolean
     */
    public function getFieldType()
    {
        $field = CoreUserField::model()->findByPk($this->idFieldCommon);
        
       if (!empty($field)) {
           return $field->getFieldType();
       } else {
           return false;
       }
    }        
    
    /**
     * Function is rendering a single Assign rule.
     * It's will be users for rendering when editing an rule.
     * 
     * @param int $index - the index of the model, for psoting an array of CoreGroupAssignRules
     * @return string - HTML data of the rule.
     * @throws Exception if there is no existing field entry related to the rule
     */
    public function renderSingleRule($index)
    {
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        /** @var $field CoreUserField */
        $field = CoreUserField::model()->findByAttributes(array('id_field' => $this->idFieldCommon));

        // When migrating CoreField entries to CoreUserField the new entries should have the same ID as the old ones,
        // so there should not be a case of grouping rule referring to an an invalid field id, but just in case this
        // happens there's an exception being thrown here indicating such a case
        if ($field === null) {
            if (CoreField::model()->findByAttributes(array('id_common' => $this->idFieldCommon)) !== null) {
                throw new Exception('Grouping rule with id [' . $this->id . '] refers to CoreField instead of CoreUserField model!');
            }

            // Just an exception if there is no field with the specified ID
            throw new Exception('Field with id of [' . $this->idFieldCommon. '] could not be found for rule with id of [' . $this->id . ']');
        }

        $type    = $field->getFieldType();
        $label   = "";
        $rule    = "";
        $fieldID = 'rule' . $field->getFieldId() . time();
        $idRule  = CHtml::hiddenField('CoreGroupAssignRules[' . $index . '][id]', $this->id);
        $idFieldHidden = CHtml::hiddenField('CoreGroupAssignRules[' . $index . '][idFieldCommon]', $field->getFieldId());

        switch($type){
            case CoreUserField::TYPE_TEXT:
                $label = CHtml::label($field->translation, 'field'. $field->getFieldId()) . "&nbsp&nbsp";
                $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_type]', $this->condition_type, array(
                    CoreGroupAssignRules::CONDITION_TYPE_CONTAINS => Yii::t('standard', '_CONTAINS'),
                    CoreGroupAssignRules::CONDITION_TYPE_EQUAL => Yii::t('standard', '_EQUAL'),
                    CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL => Yii::t('standard', '_NOT_EQUAL')
                ), array(
                    'class' => 'input-small'
                )) . "&nbsp&nbsp";
                $rule .= CHtml::textField('CoreGroupAssignRules['.$index.'][condition_value]', $this->condition_value, array('id' => 'filed'.$field->getFieldId(), 'class' => 'input-medium'), array(
                    'class' => 'input-medium'
                )) . "&nbsp&nbsp";
                break;
            case CoreUserField::TYPE_COUNTRY:
                $data = CoreCountry::getCountryListWithId();
                $label = CHtml::label($field->translation . ' is', 'rule' . $field->getFieldId()) . "&nbsp&nbsp";
                $rule = "<select name='CoreGroupAssignRules[$index][condition_value]' multiple='multiple' class='fbkcompleate' id='fcbk" . $index . "'>";
                $selected = explode(',', $this->condition_value);
                foreach($data as $id => $c){
                    $rule .= "<option " . (in_array($id, $selected) ? "class='selected' selected='selected'" : "")  .  " value='" . $id . "'>" . $c . "</option>";
                }
                $rule .= "</select>";
                $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IN);
                break;
            case CoreUserField::TYPE_YESNO:
                $label = CHtml::label($field->translation . ' is', $fieldID) . "&nbsp&nbsp";
                $rule = CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_value]', $this->condition_value, array(
                    '1' => 'Yes',
                    '2' => 'No'
                ), array('class' => 'input-small'));
                $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IS);
                break;
            case CoreUserField::TYPE_DATE:
                $label = CHtml::label($field->translation, $fieldID) . "&nbsp&nbsp";
                $rule = "<div class='span4'>" . CHtml::dropDownList('CoreGroupAssignRules['.$index.'][condition_type]', $this->condition_type, array(
                    '0' => '---',
                    CoreGroupAssignRules::CONDITION_TYPE_FROM_TO => Yii::t('standard', '_FROM') . '/' . Yii::t('standard', '_TO'),
                    CoreGroupAssignRules::CONDITION_TYPE_BEFORE => Yii::t('group_management','before'),
                    CoreGroupAssignRules::CONDITION_TYPE_AFTER => Yii::t('group_management','after'),
                    CoreGroupAssignRules::CONDITION_TYPE_IS => Yii::t('group_management', 'is')
                ), array(
                    'style' => 'width: 113px;',
                    'data-div' => $fieldID,
                    'data-index' => $index,
                    'id' => 'date_select_' . $fieldID
                )) . "</div>";
                $data = "";
                switch($this->condition_type){
                    case self::CONDITION_TYPE_FROM_TO:
                        $dateArray = explode(',', $this->condition_value);
                        $data = "<div class='span6'><label for=''>From</label>&nbsp&nbsp<input value='" . $dateArray[0] . "' type='text' size='12' class='datepicker input-small' name='CoreGroupAssignRules[" . $index  ."][condition_value][]'><i class='p-sprite calendar-black-small date-icon'></i></div><div class='span6'><label for=''>To</label>&nbsp&nbsp<input value='" . $dateArray[1] . "'  type='text' class='datepicker input-small' name='CoreGroupAssignRules[" . $index . "][condition_value][]'><i class='p-sprite calendar-black-small date-icon'></i></div>";
                        break;
                    case self::CONDITION_TYPE_BEFORE:
                        $data = "<label for=''>Before</label>&nbsp&nbsp<input value='" . $this->condition_value . "' style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules[" . $index . "][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>";
                        break;
                    case self::CONDITION_TYPE_AFTER:
                        $data = "<label for=''>After</label>&nbsp&nbsp<input value='" . $this->condition_value . "' style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules[" . $index . "][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>";
                        break;
                    case self::CONDITION_TYPE_IS:
                        $data = "<label for=''>Is</label>&nbsp&nbsp<input value='" . $this->condition_value . "' style='width: 100px;' type='text' size='12' class='datepicker' name='CoreGroupAssignRules[" . $index . "][condition_value]'><i class='p-sprite calendar-black-small date-icon'></i>";
                        break;
                }
                $rule .= "<div class='span8' id='" .$field->getFieldType() . '_dates_' . $fieldID ."'>" . $data ."</div>";
                break;
            case CoreUserField::TYPE_DROPDOWN:
                $dropdown = new FieldDropdown;
                $dropdown->id_field = $field->getFieldId();
                $dropdown->type = $field->getFieldType();
                $options = $dropdown->getOptions($lang);
                $label = CHtml::label($field->translation . ' is', '' . $field->getFieldId()) . "&nbsp&nbsp";
                $rule = "<select name='CoreGroupAssignRules[$index][condition_value]' multiple='multiple' class='fbkcompleate' id='fcbk" . $index . "'>";
                $selected = explode(',', $this->condition_value);
                foreach($options as $id => $c){
                    $rule .= "<option " . (in_array($id, $selected) ? " class='selected' selected='selected'" : "")  .  " value='" . $id . "'>" . $c . "</option>";
                }
                $rule .= "</select>";
                $rule .= CHtml::hiddenField('CoreGroupAssignRules['.$index.'][condition_type]', CoreGroupAssignRules::CONDITION_TYPE_IN);
                break;
        }
        $content = "<div class='row-fluid controls form-inline'><div class='span3 text-center'>" . $label . "</div><div class='span8'>" . $rule . $idFieldHidden . $idRule . "</div><div class='span1' style='padding-top: 6px;'><a href='#' class='delete-action'></a></div>" . (($this->hasErrors()) ? "<div class='span12'>" . CHtml::error($this, 'condition_value') . "</div>" : "") ."</div>";

        return $content;
    }
}
