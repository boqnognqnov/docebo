<?php

/**
 * This is the model class for table "learning_course_rating".
 *
 * The followings are the available columns in table 'learning_course_rating':
 * @property integer $idCourse
 * @property integer $rate_count
 * @property integer $rate_sum
 * @property double $rate_average
 *
 * The followings are the available model relations:
 * @property LearningCourse $idCourse0
 */
class LearningCourseRating extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_rating';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCourse', 'required'),
			array('idCourse, rate_count, rate_sum', 'numerical', 'integerOnly'=>true),
			array('rate_average', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idCourse, rate_count, rate_sum, rate_average', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCourse0' => array(self::BELONGS_TO, 'LearningCourse', 'idCourse'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCourse' => 'Id Course',
			'rate_count' => 'Rate Count',
			'rate_sum' => 'Rate Sum',
			'rate_average' => 'Rate Average',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('rate_count',$this->rate_count);
		$criteria->compare('rate_sum',$this->rate_sum);
		$criteria->compare('rate_average',$this->rate_average);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseRating the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
