<?php

/**
 * This is the model class for table "core_notification".
 *
 * The followings are the available columns in table 'core_notification':
 * @property string $id
 * @property string $type
 * @property string $recipient Type (or level) of the recipients (Users, Instructors, Super Admins, ...)
 * @property integer $active
 * @property string $schedule_type
 * @property integer $schedule_time
 * @property string $schedule_shift_period
 * @property string $schedule_shift_number
 * @property string $job_handler_id
 * @property string $id_job
 * @property integer $id_author
 * @property integer $ufilter_option
 * @property integer $cfilter_option
 * @property integer $puProfileId
 * @property string $schedule_shift_period_every
 * @property string $schedule_day
 * @property integer $schedule_on_day
 * @property integer $schedule_time_every
 *
 *
 *
 * The followings are the available model relations:
 * @property CoreNotificationTranslation[] $translations
 * @property CoreNotificationTranslation $lang
 * @property CoreNotificationTranslation $assignedCount
 * @property CoreNotificationAssoc[] $associatedItems
 * @property CoreNotificationUserFilter[] $userFilterItems
 * @property CoreJob $scheduledJob
 * @property CoreUser $author
 *
 */
class CoreNotification extends CActiveRecord {

	/**
	 * Fixed list of Notification types
	 *
	 * @var number
	 * =====IMPORTANT=====
	 * if we need to change some constant value, we also need to rename the coresponding file under NotificationApp/views folder if there is the same filename!!!
	 * that files must named like value of the constant, but lowercase first letter!
	 */
	const NTYPE_COURSE_PROP_CHANGED = 'CoursePropChanged';  // *
	const NTYPE_NEW_CATEGORY = 'NewCategory';  // *
	const NTYPE_NEW_REPLY = 'NewReply';
	const NTYPE_NEW_THREAD = 'NewThread';
	const NTYPE_NEW_COMMENT = 'NewComment';
	const NTYPE_NEW_MESSAGE_RECEIVED = 'NewMessage';
	const NTYPE_NEWSLETTER_SENT = 'NewsletterSent';
	const NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT = 'InstructorEvaluatedAssignment';
	const NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN = 'NewCourseUnlockedInLearningPlan';
	const NTYPE_LEARNER_SUBMITTED_ASSIGNMENT = 'CourseDeliverableSubmitted';
	const NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED = 'AssignmentWaitingReview';
	const NTYPE_USER_EMAIL_MUST_VERIFY = 'UserEmailVerified';
	const NTYPE_ECOMMERCE_SETTINGS_CHANGED = 'EcommerceSettingsChanged';
	// User subscription for course approved by admin
	const NTYPE_USER_APPROVAL = 'UserApproval';  // *
	const NTYPE_USER_BOUGHT_COURSE = 'UserBoughtCourse';
	const NTYPE_STUDENT_COMPLETED_COURSE = EventManager::EVENT_STUDENT_COMPLETED_COURSE;  // *
	// User subscribed himself to a course successfully
	const NTYPE_USER_SUBSCRIBED_COURSE = EventManager::EVENT_USER_SUBSCRIBED_COURSE;  // *
	// User subscribed himself to course, but needs admin approval
	const NTYPE_USER_WAITING_SUBSCRIBE = 'UserWaitingSubscribe'; // *
	const NTYPE_USER_COURSELEVEL_CHANGED = 'UserCourselevelChanged'; // *
	const NTYPE_USER_UNSUBSCRIBED = 'UserUnsubscribed'; // *
	const NTYPE_USER_DELETED = 'UserDeleted'; // *
	const NTYPE_USER_SUSPENDED = 'UserSuspended'; // *
	const NTYPE_USER_UNSUSPENDED = 'UserUnSuspended'; // *
	const NTYPE_USER_SUBSCRIBED_GROUP = 'UserSubscribedGroup'; // *
	const NTYPE_USER_REMOVED_FROM_GROUP = 'UserRemovedFromGroup'; // *
	const NTYPE_USER_ASSIGNED_TO_BRANCH = 'UserAssignedToBranch'; // *
	const NTYPE_USER_REMOVED_FROM_BRANCH = 'UserRemovedFromBranch'; // *
	const NTYPE_USER_MODIFIED = 'UserModified'; // *
	const NTYPE_NEW_COURSE = 'NewCourse'; // *
	const NTYPE_NEW_LO_ADDED_TO_COURSE = 'NewLoAddedCourse'; // *
	const NTYPE_COURSE_DELETED = 'CourseDeleted'; // *
	const NTYPE_COURSE_NOT_YET_COMPLETE = 'AssignedCourseNotYetCompleted'; // *
	const NTYPE_USER_COMPLETED_TEST = 'UserCompletedTest';
	const NTYPE_USER_TEST_EVALUATION_NEEDED = 'UserTestEvaluationNeeded';
	const NTYPE_INSTRUCTOR_EVALUATED_QUESTION = 'InstructorEvaluatedQuestion';
	const NTYPE_STUDENT_COMPLETED_LEARNINGPLAN = EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN; // *
	const NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN = 'UserUnenrolledFromLearningPlan';
	const NTYPE_USER_ENROLLED_IN_LEARNINGPLAN = EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN;
	const NTYPE_LEARNINGPLAN_NEW_CONTENT = 'NewContentInLearningPlan';
	const NTYPE_LEARNINGPLAN_CREATED = 'NewLPCreated';
	const NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT = 'LPEnrollmentExpire';
	//In case create subscription plan
	const NTYPE_SUBSCRIPTION_PLAN_CREATE = 'SubPlanCreated';
	const NTYPE_SUB_BUNDLE_CREATED = 'SubBundleCreated';
	const NTYPE_SUB_BUNDLE_EDITED = 'SubBundleEdited';
	const NTYPE_SUB_BUNDLE_DELETED = 'SubBundleDeleted';
	const NTYPE_SUB_BUNDLE_ITEM_DELETED = 'SubBundleDeleteItem';
	const NTYPE_SUB_BUNDLE_ADD_ITEM = 'SubBundleAddItem';
	const NTYPE_SUB_BUNDLE_ADD_VISIBILITY = 'SubBundleVisibilityUpdate';
	const NTYPE_SUB_BUNDLE_DELETE_VISIBILITY = 'SubBundleVisibilityDelete';
	// Subscription Bundle
	const NTYPE_SUB_SEAT_ASSIGNED = 'SubSeatAssigned';
	const NTYPE_SUB_SEAT_UNASSIGNED = 'SubSeatUnassigned';
	const NTYPE_SUB_SEAT_EDITED = 'SubSeatEdited';
	const NTYPE_SUB_PLAN_CREATED = 'SubPlanCreated';
	const NTYPE_SUB_PLAN_EDITED = 'SubPlanEdited';
	const NTYPE_SUB_PLAN_DELETED = 'SubPlanDeleted';
	const NTYPE_SUB_RECORD_CREATED = 'SubRecordCreated';
	const NTYPE_SUB_RECORD_EDITED = 'SubRecordEdited';
	const NTYPE_SUB_RECORD_DELETED = 'SubRecordDeleted';
	const NTYPE_SUB_RECORD_ADD_ITEM = 'SubRecordItemCreated';
	const NTYPE_SUB_RECORD_DELETE_ITEM = 'SubRecordItemDeleted';
	// When moderated self-registration is enabled and a user registers + confirms email
	const NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION = 'UserWaitingApprovalSubscription'; // *
	// When user is created by administrator
	const NTYPE_NEW_USER_CREATED = 'NewUserCreated'; // *
	// When user is created as a result of Self registration "confirm" click, with NO admin approval needed
	const NTYPE_NEW_USER_CREATED_SELFREG = 'NewUserCreatedSelfreg';
	// When course is no more available for PLAYING by its subscribers
	const NTYPE_COURSE_PLAY_HAS_EXPIRED = 'CoursePlayHasExpired';
	const NTYPE_ILT_SESSION_CREATED = 'NewILTSessionCreated';
	const NTYPE_ILT_SESSION_CHANGED = 'ILTSessionChanged';
	const NTYPE_ILT_SESSION_DELETED = 'ILTSessionDeleted';
	const NTYPE_ILT_SESSION_STARTING = 'ILTSessionStarting';
	const NTYPE_ILT_SESSION_USER_ENROLLED = EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION;
	const NTYPE_ILT_SESSION_USER_ACTIVATED = EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED;
	const NTYPE_ILT_SESSION_USER_DECLINED = EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_DECLINED;
	const NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL = EventManager::EVENT_USER_CLASSROOM_SESSION_WAITING_FOR_APPROVAL;
	const NTYPE_ILT_SESSION_USER_UNENROLLED = 'ILTSessionUserUnenrolled';
	const NTYPE_ILT_SESSION_USER_PASSED = 'StudentPassedILTSession';
	const NTYPE_ILT_SESSION_USER_FAILED = 'StudentFailedILTSession';
	const NTYPE_CATCHUP_COURSE_AVAILABLE = 'CatchupCourseAvailable';
	const NTYPE_CATCHUP_LIMIT_CHANGED = 'CatchupLimitChanged';

	/* Coaching sessions related notifications */
	const NTYPE_NEW_COACHING_SESSION = 'NewCoachingSession';
	const NTYPE_UPDATED_COACHING_SESSION = 'UpdateCoachingSession';
	const NTYPE_DELETED_COACHING_SESSION = 'DeletedCoachingSession';
	const NTYPE_MERGED_COACHING_SESSION = 'MergedCoachingSession';
	const NTYPE_FIRST_COACHING_SESSION_REQUESTED = 'FirstCoachingSessionRequest';
	const NTYPE_OTHER_COACHING_SESSION_REQUESTED = 'AnotherCoachingSessionRequest';
	const NTYPE_COACHING_SESSION_EXPIRED = 'CoachingSessionExpired';
	const NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER = 'SentMessageToCoachByLearner';
	const NTYPE_COACHING_WEBINAR_SESSION_CREATED = 'NewCoachingWebinarSessionCreated';
	const NTYPE_COACHING_WEBINAR_SESSION_STARTS = 'LiveCoachSessionStarts';
	const NTYPE_COACHING_COACH_MESSAGED_LEARNER = 'CoachMessagedLearner';

	/* Webinar Sessions */
	const NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL = EventManager::EVENT_USER_WEBINAR_SESSION_WAITING_FOR_APPROVAL;
	const NTYPE_WEBINAR_SESSION_USER_ACTIVATED = EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED;
	const NTYPE_WEBINAR_SESSION_USER_DECLINED = EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_DECLINED;
	const NTYPE_WEBINAR_SESSION_CREATED = 'WebinarSessionCreated';
	const NTYPE_WEBINAR_SESSION_CHANGED = 'WebinarSessionChanged';
	const NTYPE_WEBINAR_SESSION_DELETED = 'WebinarSessionDeleted';
	const NTYPE_WEBINAR_SESSION_STARTING = 'ILTWebinarSessionStarting';
	const NTYPE_WEBINAR_SESSION_USER_ENROLLED = EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION;
	const NTYPE_WEBINAR_SESSION_USER_UNENROLLED = EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION;
	//digest notifications
	const NTYPE_DIGEST_USER_ENROLL_TO_COURSE = 'DigestUserEnrolledToCourse';
	const NTYPE_DIGEST_COURSE_HAS_EXPIRED = 'DigestCourseHasExpired';
	const NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE = 'DigestUserCompleteCourse';


	/* BLOG Notifications */
	const NTYPE_BLOG_COMMENT = 'StudentCommentedOnBlog';
	const NTYPE_BLOG_POST_IN_COURSE = 'StudentPostedOnBlog';
	const NTYPE_BLOG_COMMENT_IN_COURSE = 'StudentCommentedOnBlogInCourse';

	/* App7020 Notifications */
	const NTYPE_APP7020_INVITATION = 'App7020Invitation';
	const NTYPE_APP7020_NEW_QUESTION = 'App7020NewQuestion';
	const NTYPE_APP7020_NEW_CONTENTS_QUESTION = 'App7020NewContentsQuestion';
	const NTYPE_APP7020_NEW_CONTENT = 'App7020NewContent';
	const NTYPE_APP7020_NEW_PEER_REVIEW = 'App7020NewPeerReview';
	const NTYPE_APP7020_PUBLISHING = 'App7020Publishing';
	const NTYPE_APP7020_NEW_ANSWER = 'App7020NewAnswer';
	const NTYPE_APP7020_NEW_ASSET_TO_REVIEW = 'App7020NewAssetToReview';
	const NTYPE_APP7020_BEST_ANSWER = 'App7020BestAnswer';
	const NTYPE_APP7020_TOPIC_ASSIGNMENT = 'App7020TopicAssignment';
	const NTYPE_APP7020_CHANNEL_ASSIGNMENT = 'App7020ChannelAssignment';
	const NTYPE_APP7020_NEW_TOOLTIP = 'App7020NewTooltip';
	const NTYPE_APP7020_DELETE_ASSET = 'App7020DeleteAsset';
	const NTYPE_APP7020_DELETE_ASSET_BY_ADMIN = 'App7020DeleteAssetByAdmin';
	const NTYPE_APP7020_MENTIONED_ANSWER = 'App7020MentionedAnswer';

	/* Gamification Contest Notifications */
	const NTYPE_GAMIFICATION_CONTEST_START = 'GamificationContestStart';
	const NTYPE_GAMIFICATION_CONTEST_END = 'GamificationContestEnd';

	/* Gamification Reward Notificaions */
	const NTYPE_GAMIFICATION_REWARD_PENDING = 'UserAskedToRedeemReward';
	const NTYPE_GAMIFICATION_REWARD_APPROVED = 'SuperadministratorApprovedRewardRequest';
	const NTYPE_GAMIFICATION_REWARD_REJECTED = 'SuperadministratorRejectedRewardRequest';
	// Subscription Notifications
	const NTYPE_SUB_PLAN_PURCHASED = 'SubPlanPurchased';
	const NTYPE_SUB_PLAN_EXPIRED = 'SubPlanExpired';
	const NTYPE_SUB_RECORD_EXCEEDED_CAPACITY = 'SubRecordExceededCapacity';
	const NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED = 'SubRecordCapacityAlmostExhausted';

	/* Event type groups */
	const GROUP_COURSES = 'courses';
	const GROUP_LEARNINGPLANS = 'learning_plans';
	const GROUP_GAMIFICATION = 'gamification';
	const GROUP_COACH_SHARE = 'coach_share';
	const GROUP_USERS = 'users';
	const GROUP_FORUM = 'forum';
	const GROUP_BLOG = 'blog';
	const GROUP_COACHING = 'coaching';
	const GROUP_SUBSCRIPTION = 'subscription';

	/**
	 * Recipients type
	 *
	 * @var number
	 */
	const NOTIFY_RECIPIENTS_USER = 'user';
	const NOTIFY_RECIPIENTS_COURSEADMIN = 'courseadmin';
	const NOTIFY_RECIPIENTS_GODADMIN = 'godadmin';
	const NOTIFY_RECIPIENTS_TEACHER = 'instructor';
	const NOTIFY_RECIPIENTS_POWER_USER = 'poweruser';
	const NOTIFY_RECIPIENTS_MANAGER = 'manager';


	/**
	 * Shortcodes
	 */
	const SC_TEST_NAME = '[test_name]';
	const SC_TEST_COMPLETION_DATE = '[completion_date]';
	const SC_TEST_SCORE = '[test_score]';
	const SC_TEST_RESULTS = '[test_results]';
	const SC_QUESTION_SCORE = '[question_score]';
	const SC_QUESTION_TITLE = '[question_title]';
	const SC_QUESTION_ANSWER = '[question_answer]';
	const SC_FIRSTNAME = '[first_name]';
	const SC_LASTNAME = '[last_name]';
	const SC_COURSENAME = '[course_name]';
	const SC_CATEGORYNAME = '[category_name]';
	const SC_GROUPNAME = '[group_name]';
	const SC_GROUPID = '[group_id]';
	const SC_LO_TITLE = '[lo_title]';
	const SC_FORUM_THREAD = '[thread_title]';
	const SC_FORUM = '[forum_name]';
	const SC_USER_COURSE_LEVEL = '[user_course_level]';
	const SC_COURSE_LINK = '[course_link]';
	const SC_FORUM_THREAD_LINK = '[thread_link]';
	const SC_FORUM_LINK = '[forum_link]';
	const SC_FORUM_COMMENT = '[comment]';
	const SC_USER_EMAIL = '[user_email]';
	const SC_USER_PASSWORD = '[user_password]';
	const SC_LMS_LINK = '[lms_link]';
	const SC_USERNAME = '[username]';
	const SC_LEARNINGPLAN_NAME = '[learningplan_name]';
	const SC_LEARNINGPLAN_USER_VALIDITY_DATE_START = '[learningplan_user_validity_start]';
	const SC_LEARNINGPLAN_USER_VALIDITY_DATE_END = '[learningplan_user_validity_end]';
	const SC_LEARNINGPLAN_URL = '[learningplan_url]';
	const SC_ORDER_RECEIPT_CONTENT = '[order_receipt]';
	const SC_ORDER_ID = '[order_id]';
	const SC_ORDER_PAYMENT_DATE = '[payment_date]';
	const SC_ORDER_SUBTOTAL = '[subtotal]';
	const SC_ORDER_DISCOUNT = '[discount]';
	const SC_ORDER_TOTAL_PAID = '[total_paid]';
	const SC_ORDER_COMPANY = '[company]';
	const SC_ORDER_ADDRESS = '[address]';
	const SC_ORDER_STATE = '[state]';
	const SC_ORDER_ZIP = '[zip]';
	const SC_ORDER_CITY = '[city]';
	const SC_ORDER_COUNTRY = '[country]';
	const SC_ORDER_ITEMS = '[order_items]';
	const SC_EXPIRE_AT = '[expire_at]';
	const SC_SUBSCRIBED_AT = '[subscribed_at]';
	const SC_COMPLETED_AT = '[completed_at]';
	const SC_CERTIFICATE_URL = '[certificate_url]';
	const SC_LO_TYPE = '[lo_type]';
	const SC_USER_REGISTER_DATE = '[register_date]';
	const SC_CALENDAR_ATTACHMENT = '[calendar_attachment]';
	const SC_SESSION_NAME = '[session_name]';
	const SC_SESSION_DATES = '[session_dates]';
	const SC_SESSION_LOCATION = '[session_location]';
	const SC_EVALUATION = '[evaluation]';
	const SC_EVALUATION_MESSAGE = '[evaluation_message]';
	const SC_ASSIGNMENT_STATUS = '[assignment_status]';
	const SC_ASSIGNMENT_TITLE = '[assignment_title]';
	const SC_ASSIGNMENT_SUBMISSION_DATE = '[assignment_date]';
	const SC_CATCHUP_LIMIT = '[catchup_limit]';
	const SC_SSO_EXPIRING_LINK = '[sso_link]'; // e.g. [sso_link expires_in="X"] to set the days of expiration (default is 1)
	const SC_RESET_PASSWORD_LINK = '[reset_pwd_link]';
	const SC_VERIFICATION_LINK = '[verification_link]';
	const SC_FINAL_SCORE = '[final_score]'; // value from LearningCourseuser::score_given

	const SC_COACH_NAME = '[coach_name]';
	const SC_COACH_EMAIL = '[coach_email]';
	const SC_COACH_SESSION_DATES = '[coaching_session_dates]';
	const SC_COACH_SESSION_TARGET = '[coaching_session_target_dates]';
	const SC_COACH_CHAT_MESSAGE = '[coaching_chat_message]';
	const SC_COACH_WEBINAR_DATE_BEGIN = '[coaching_webinar_date_begin]';
	const SC_COACH_WEBINAR_DATE_END = '[coaching_webinar_date_end]';
	const SC_COACH_WEBINAR_NAME = '[coaching_webinar_name]';
	const SC_COACH_WEBINAR_URL = '[coaching_webinar_url]';
	const SC_COACH_WEBINAR_DETAILS = '[coaching_webinar_meeting_details]';

	//Webinar Session
	const SC_WEBINAR_WEBINAR_TOOL = '[webinar_tool]';
	const SC_WEBINAR_SESSION_DETAILS = '[webinar_session_details]';
	// BLOG Notification
	CONST SC_BLOG_TITLE = '[blog_title]';
	CONST SC_BLOG_TEXT = '[blog_text]';
	CONST SC_BLOG_WRITER = '[blog_writer]';
	CONST SC_BLOG_COMMENT = '[comment]';
	CONST SC_BLOG_COMMENT_USER = '[comment_user]';
	CONST SC_BLOG_DATE_PUBLISHED = '[date_published]';
	// APP7020 Notifications
	CONST SC_APP7020_CONTENT_URL = '[app7020_content_url]';
	CONST SC_APP7020_CONTENT_THUMB = '[app7020_content_thumb]';
	CONST SC_APP7020_CONTENT_TITLE = '[app7020_content_title]';
	CONST SC_APP7020_CONTENT_AUTHOR = '[app7020_owner_content]';
	CONST SC_APP7020_CONTENT_INVITER = '[app7020_inviter_name]';
	CONST SC_APP7020_CONTENT_INVITER_AVATAR = '[app7020_inviter_avatar]';
	CONST SC_APP7020_CONTENT_RECIPIENT_EMAIL = '[app7020_recipient_email]';
	CONST SC_APP7020_QUESTION_TITLE = '[app7020_question_title]';
	CONST SC_APP7020_QUESTION_AUTHOR = '[app7020_owner_question]';
	CONST SC_APP7020_QUESTION_RECIPIENT_FIRSTNAME = '[app7020_first_name]';
	CONST SC_APP7020_QUESTION_OWNER_AVATAR = '[app7020_owner_question_avatar]';
	CONST SC_APP7020_QUESTION_EMAIL_USERS = '[app7020_email_users]';
	CONST SC_APP7020_QUESTION_URL = '[app7020_question_url]';
	CONST SC_APP7020_PEER_REVIEW_TEXT = '[app7020_peer_review_body]';
	CONST SC_APP7020_PEER_REVIEW_AUTHOR = '[app7020_owner_peer_review]';
	CONST SC_APP7020_RECIPIENT_EMAIL = '[app7020_recipient_email]';
	CONST SC_APP7020_REVIEW_OWNER_AVATAR = '[app7020_review_owner_avatar]';
	CONST SC_APP7020_RECIPIENT_FIRST_NAME = '[first_name]';
	CONST SC_APP7020_PUBLISHER = '[app7020_publisher]';
	CONST SC_APP7020_SUPER_ADMIN = '[app7020_super_admin]';
	CONST SC_APP7020_ANSWER_AUTHOR = '[app7020_owner_answer]';
	CONST SC_APP7020_ANSWER_OWNER_AVATAR = '[app7020_answer_owner_avatar]';
	CONST SC_APP7020_ANSWER_TITLE = '[app7020_answer_title]';
	CONST SC_APP7020_QUESTION_OWNER_EMAIL = '[app7020_question_owner_email]';
	CONST SC_APP7020_TOPIC_NAME = '[app7020_topic_name]';
	CONST SC_APP7020_CHANNEL_NAME = '[app7020_channel_name]';
	CONST SC_APP7020_TOOLTIP_OWNER = '[app7020_tooltip_owner]';
	CONST SC_APP7020_TOOLTIP_BODY = '[app7020_tooltip_body]';
	CONST SC_APP7020_TOOLTIP_POSITION = '[app7020_tooltip_position]';
	CONST SC_APP7020_MENTIONED_ANSWER_FIRST_NAME = '[app7020_first_name]';
	CONST SC_APP7020_MENTIONED_ANSWER_OWNER = '[app7020_owner_mention]';
	CONST SC_APP7020_MENTIONED_ANSWER_OWNER_AVATAR = '[app7020_owner_mention_avatar]';
	CONST SC_APP7020_MENTIONED_ANSWER_QUESTION_URL_PAGE = '[app7020_question_url_page]';
	CONST SC_APP7020_MENTIONED_ANSWER_QUESTION_TITLE = '[app7020_question_title]';
	CONST SC_APP7020_MENTIONED_USER_EMAIL = '[app7020_user_email]';
	CONST SC_APP7020_ASSET_TO_REVIEW_URL = '[app7020_asset_url]';
	CONST SC_APP7020_ASSET_TO_REVIEW_THUMB = '[app7020_asset_thumb]';
	CONST SC_APP7020_ASSET_TO_REVIEW_TITLE = '[app7020_asset_tittle]';
	CONST SC_APP7020_ASSET_TO_REVIEW_AUTHOR = '[app7020_asset_author]';
	CONST SC_APP7020_ASSET_TO_REVIEW_FIRSTNAME = '[first_name]';
	CONST SC_APP7020_ASSET_TO_REVIEW_EMAIL = '[app7020_user_email]';
	CONST SC_APP7020_ASSET_TO_REVIEW_AVATAR = '[app7020_user_avatar]';


	/* Gamification Contest Shortcodes */
	const SC_GAMIFICATION_CONTEST_CONTEST_NAME          = '[contest_name]';
	const SC_GAMIFICATION_CONTEST_CONTEST_DESCRIPTION   = '[contest_description]';
	const SC_GAMIFICATION_CONTEST_CONTEST_GOAL          = '[contest_goal]';
	const SC_GAMIFICATION_CONTEST_CONTEST_START_DATE    = '[contest_start_date]';
	const SC_GAMIFICATION_CONTEST_CONTEST_END_DATE      = '[contest_end_date]';
	const SC_GAMIFICATION_CONTEST_CONTEST_LINK          = '[contest_link]';

	// Gamification Reward Shortcodes
	const SC_GAMIFICATION_REWARD_NAME = '[reward_name]';
	const SC_GAMIFICATION_REWARD_REQUEST_DATE = '[reward_request_date]';
	const SC_GAMIFICATION_REWARD_COINS = '[reward_coins]';
	// Subscription Record Shortcodes
	const SC_SUBSCRIPTION_RECORD_PURCHASE_DATE = '[purchase_date]';
	const SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE = '[transaction_code]';
	const SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE = '[expiration_date]';
	const SC_SUBSCRIPTION_RECORD_BUNDLE_NAME = '[bundle_name]';
	const SC_SUBSCRIPTION_RECORD_PLAN_NAME = '[plan_name]';
	const SC_SUBSCRIPTION_RECORD_TYPE = '[type]';
	const SC_SUBSCRIPTION_RECORD_CAPACITY = '[capacity]';
	const SC_SUBSCRIPTION_RECORD_USED_CAPACITY = '[used_capacity]';
	const SC_SUBSCRIPTION_RECORD_REMAINING_CAPACITY = '[remaining_capacity]';
	const SC_SUBSCRIPTION_RECORD_PRICE = '[price]';
	const SC_SUBSCRIPTION_RECORD_NOTE = '[note]';
	const SC_SUBSCRIPTION_RECORD_DESCRIPTION = '[description]';
	const SC_SUBSCRIPTION_RECORD_SOLD_TO = '[sold_to]';
	// Subscription Bundle Shortcodes
	const SC_SUBSCRIPTION_BUNDLE_NAME = '[bundle_name]';
	const SC_SUBSCRIPTION_BUNDLE_CODE = '[bundle_code]';
	const SC_SUBSCRIPTION_BUNDLE_DESCRIPTION = '[bundle_description]';
	const SC_SUBSCRIPTION_BUNDLE_TYPE = '[bundle_type]';
	const SC_SUBSCRIPTION_BUNDLE_CATALOG_LIST = '[catalog_list]';
	const SC_SUBSCRIPTION_BUNDLE_PLAN_LIST = '[plan_list]';
	//digest and repeatable shortcodes
	/**
	 * Describe how repeatable elements would be started and recognized as  repeatable
	 * @type string
	 */
	const SC_REPEATABLE_BEGIN = '[items]';

	/**
	 * Describe how repeatable elements would be closed into template
	 * @type string
	 */
	const SC_REPEATABLE_END = '[/items]';

	/**
	 * Preset of shortcodes used for repeatable case in "Digest: user enrolled to a course" notification
	 * @var array
	 */
	static $SC_PRESET_DIGEST_USER_ENROLLED_COURSE = array(
		self::SC_FIRSTNAME,
		self::SC_LASTNAME,
		self::SC_USERNAME,
		self::SC_COURSENAME,
		self::SC_COURSE_LINK,
		self::SC_SUBSCRIBED_AT,
		self::SC_EXPIRE_AT,
		self::SC_SSO_EXPIRING_LINK,
		self::SC_RESET_PASSWORD_LINK,
	);

	/**
	 * Preset of shortcodes used for repeatable case in "Digest: course has expired" notification
	 * @var array
	 */
	static $SC_PRESET_DIGEST_COURSE_EXPIRED = array(
		self::SC_FIRSTNAME,
		self::SC_LASTNAME,
		self::SC_USERNAME,
		self::SC_COURSENAME,
		self::SC_COURSE_LINK,
		self::SC_SUBSCRIBED_AT,
		self::SC_EXPIRE_AT,
		self::SC_SSO_EXPIRING_LINK,
		self::SC_RESET_PASSWORD_LINK,
	);

	/**
	 * Preset of shortcodes used for repeatable case in "Digest: learner has yet to complete the course" notification
	 * @var array
	 */
	static $SC_PRESET_DIGEST_COMPLETE_COURSE = array(
		self::SC_FIRSTNAME,
		self::SC_LASTNAME,
		self::SC_USERNAME,
		self::SC_COURSENAME,
		self::SC_COURSE_LINK,
		self::SC_SUBSCRIBED_AT,
		self::SC_EXPIRE_AT,
		self::SC_SSO_EXPIRING_LINK,
		self::SC_RESET_PASSWORD_LINK,
	);

	// scheduling types and some preset arrays
	const SCHEDULE_AT = 'at';
	const SCHEDULE_BEFORE = 'before';
	const SCHEDULE_AFTER = 'after';
	const SCHEDULE_EVERY = 'every';


	//----------
	//TODO: remove these types as soon as possible
	const NOTIFY_TYPE_EMAIL_ONLY = 'email';
	const NOTIFY_TYPE_INBOX_ONLY = 'inbox';
	const NOTIFY_TYPE_INBOX_AND_EMAIL = 'email_and_inbox';
	//NOTE: see above, use these new types
	const NOTIFY_TYPE_EMAIL = 'email';
	const NOTIFY_TYPE_INBOX = 'inbox';

	//----------


	static $SCHEDULE_PRESET_ALL = array(
		self::SCHEDULE_AT,
		self::SCHEDULE_BEFORE,
		self::SCHEDULE_AFTER,
	);

	static $SCHEDULE_PRESET_NO_AT = array(
		self::SCHEDULE_BEFORE,
		self::SCHEDULE_AFTER,
	);

	static $SCHEDULE_PRESET_ONLY_AT = array(
		self::SCHEDULE_AT,
	);

	static $SCHEDULE_PRESET_ONLY_AFTER = array(
		self::SCHEDULE_AFTER,
	);

	static $SCHEDULE_PRESET_NO_BEFORE = array(
		self::SCHEDULE_AT,
		self::SCHEDULE_AFTER,
	);

	static $SCHEDULE_PRESET_NO_AFTER = array(
		self::SCHEDULE_AT,
		self::SCHEDULE_BEFORE,
	);

	static $SCHEDULE_PRESET_ONLY_BEFORE = array(
		self::SCHEDULE_BEFORE
	);

	static $SCHEDULE_PRESET_ONLY_EVERY = array(
		self::SCHEDULE_EVERY
	);

	const USER_NO_USER_FILTER = 1;		// all users
	const USER_DO_USER_FILTER = 2;		// filtered users

	const COURSES_NO_ASSOC 	= 1;   	// all courses
	const COURSES_DO_ASSOC 	= 2;   	// selected courses/plans


	/**
	 * Possible event shifting period (after/before N days|week|hours)
	 * @var string
	 */
	const SCHED_SHIFT_PERIOD_HOUR	 = 'hour';
	const SCHED_SHIFT_PERIOD_DAY 	 = 'day';
	const SCHED_SHIFT_PERIOD_WEEK  = 'week';
	const SCHED_SHIFT_PERIOD_MONTH  = 'month';
	const SCHED_SHIFT_PERIOD_YEAR  = 'year';

	/**
	 * Possible weekdays
	 * @var string
	 */

    const SCHED_SHIFT_PERIOD_MONDAY = 1;
    const SCHED_SHIFT_PERIOD_TUESDAY = 2;
    const SCHED_SHIFT_PERIOD_WEDNESDAY = 3;
    const SCHED_SHIFT_PERIOD_THURSDAY = 4;
    const SCHED_SHIFT_PERIOD_FRIDAY = 5;
    const SCHED_SHIFT_PERIOD_SATURDAY = 6;
    const SCHED_SHIFT_PERIOD_SUNDAY = 7;


	/**
	 * Represent different properies that a notification may have (not attributes, but some configurable parts).
	 * Because SOME notifications are, for example, not relevant to any course (e.g. "User has been created").
	 * In that case, PROP_COURSE_ASSOC would not be part of the "properties" of that notification and will be skipped in UI
	 *
	 * @var string
	 */
	const PROP_SCHEDULE 			= 'schedule';
	const PROP_USER_FILTER 		    = 'userfilter';
	const PROP_COURSE_ASSOC		    = 'courseassoc';
	const PROP_COURSE_ONLY		    = 'coursesonly';
	const PROP_RECIPIENTS 		    = 'recipients';

	const NOTIFICATION_JOB_HANDLER_ID = 'plugin.NotificationApp.components.NotificationJobHandler';

	/**
	 * Useful presets of properties
	 *
	 * @var array
	 */
	// ALL properties applicable
	static $PROP_ALL = array(
		self::PROP_SCHEDULE,
		self::PROP_USER_FILTER,
		self::PROP_COURSE_ASSOC,
		self::PROP_RECIPIENTS,
	);

	static $PROP_ALL_NO_LP = array(
		self::PROP_SCHEDULE,
		self::PROP_USER_FILTER,
		self::PROP_COURSE_ONLY,
		self::PROP_RECIPIENTS
	);

	// COURSE asscoiation is NOT applicable
	static $PROP_NO_COURSE = array(
		self::PROP_SCHEDULE,
		self::PROP_USER_FILTER,
		self::PROP_RECIPIENTS,
	);

	// USER filtering is NOT applicable
	static $PROP_NO_USER = array(
		self::PROP_SCHEDULE,
		self::PROP_COURSE_ASSOC,
		self::PROP_RECIPIENTS,
	);

	// COURSE asscoiation && USER filtering are NOT applicable
	static $PROP_NO_USERCOURSE = array(
		self::PROP_SCHEDULE,
		self::PROP_RECIPIENTS,
	);


	static $RECIP_ALL = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_TEACHER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);

	static $RECIP_ALL_WITH_MANAGER = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_TEACHER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
		self::NOTIFY_RECIPIENTS_MANAGER,
	);

	static $RECIP_NO_COURSE_RELATED = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);

	static $RECIP_NO_INSTRUCTOR = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);

	static $RECIP_NO_USER = array(
		self::NOTIFY_RECIPIENTS_TEACHER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);

	static $RECIP_NO_USER_WITH_MANAGER = array(
		self::NOTIFY_RECIPIENTS_TEACHER,
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
		self::NOTIFY_RECIPIENTS_MANAGER,
	);

	static $RECIP_NO_POWER_USER = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_TEACHER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);
	static $RECIP_GODADMIN_ONLY = array(
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);
	static $RECIP_USER_AND_GODADMIN = array(
		self::NOTIFY_RECIPIENTS_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN,
	);
	static $RECIP_ONLY_USER = array(
		self::NOTIFY_RECIPIENTS_USER,
	);
	static $RECIP_ONLY_TEACHER = array(
		self::NOTIFY_RECIPIENTS_TEACHER,
	);

	static $RECIP_POWER_USER_AND_GODADMIN = array(
		self::NOTIFY_RECIPIENTS_POWER_USER,
		self::NOTIFY_RECIPIENTS_GODADMIN
	);

	public static $FORMATTABLE_DATE_TAGS = array(
		self::SC_BLOG_DATE_PUBLISHED
	);

	public static $ILT_NOTIFICATIONS = array(
		self::NTYPE_ILT_SESSION_CREATED,
		self::NTYPE_ILT_SESSION_CHANGED,
		self::NTYPE_ILT_SESSION_DELETED,
		self::NTYPE_ILT_SESSION_STARTING,
		self::NTYPE_ILT_SESSION_USER_ENROLLED,
		self::NTYPE_ILT_SESSION_USER_ACTIVATED,
		self::NTYPE_ILT_SESSION_USER_DECLINED,
		self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL,
		self::NTYPE_ILT_SESSION_USER_UNENROLLED,
		self::NTYPE_ILT_SESSION_USER_PASSED,
		self::NTYPE_ILT_SESSION_USER_FAILED
	);

	public static $WEBINAR_NOTIFICATIONS = array(
		self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL,
		self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED,
		self::NTYPE_WEBINAR_SESSION_USER_DECLINED,
		self::NTYPE_WEBINAR_SESSION_STARTING,
		self::NTYPE_WEBINAR_SESSION_CREATED,
		self::NTYPE_WEBINAR_SESSION_CHANGED,
		self::NTYPE_WEBINAR_SESSION_DELETED,
		self::NTYPE_WEBINAR_SESSION_USER_ENROLLED,
		self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED,
	);

	/** @var string */
	public $search;
	public $filterType;
	public $schedule_day;
	public $schedule_on_day;
	public $schedule_shift_period_every;
	public $schedule_time_every;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreNotification the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'core_notification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, recipient', 'required'),
			array('active, schedule_shift_number, schedule_time, id_job, id_author, ufilter_option, cfilter_option, puProfileId', 'numerical', 'integerOnly' => true),
			array('type', 'length', 'max' => 64),
			array('code, job_handler_id', 'length', 'max' => 255),
			array('recipient, schedule_type', 'length', 'max' => 32),
			array('schedule_shift_period', 'length', 'max' => 16),
			array('search, filterType', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, code, recipient, active, schedule_type, schedule_shift_period, schedule_shift_number, schedule_time, job_handler_id, id_job, id_author, ufilter_option, cfilter_option, puProfileId', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'translations' => array(self::HAS_MANY, 'CoreNotificationTranslation', 'notification_id'),
			// Get a single model for current language translation
			'lang' => array(self::HAS_ONE, 'CoreNotificationTranslation', 'notification_id', 'condition' => "lang.language='" . Yii::app()->getLanguage() . "'"),
			'assignedCount' => array(self::STAT, 'CoreNotificationTranslation', 'notification_id'),
			// Assicated Courses && Learning Plans
			'associatedItems' => array(self::HAS_MANY, 'CoreNotificationAssoc', 'id'),
			// User filters
			'userFilterItems' => array(self::HAS_MANY, 'CoreNotificationUserFilter', 'id'),
			'scheduledJob' => array(self::BELONGS_TO, 'CoreJob', 'id_job'),
			// User Author
			'author' => array(self::BELONGS_TO, 'CoreUser', 'id_author'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'Notification ID',
			'type' => 'Type',
			'recipient' => 'Recipients Type',
			'active' => 'Active',
			'schedule_type' => 'Schedule Type',
			'schedule_shift_period' => 'Schedule shifting period',
			'schedule_shift_number' => 'Schedule shifting period number',
			'schedule_time' => 'Schedule Time',
			'job_handler_id' => 'Job handler id',
			'id_job' => 'Job ID',
			'id_author' => 'User/Author ID',
			'ufilter_option' => 'User filtering option',
			'cfilter_option' => 'Course association option',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('code', $this->code, true);
		$criteria->compare('recipient', $this->recipient, true);
		$criteria->compare('schedule_type', $this->schedule_type, true);
		$criteria->compare('schedule_shift_period', $this->schedule_shift_period, true);
		$criteria->compare('schedule_shift_number', $this->schedule_shift_number);
		$criteria->compare('job_handler_id', $this->job_handler_id, true);
		$criteria->compare('schedule_time', $this->schedule_time);
		$criteria->compare('id_job', $this->id_job);
		$criteria->compare('id_author', $this->id_author);
		$criteria->compare('ufilter_option', $this->ufilter_option);
		$criteria->compare('cfilter_option', $this->cfilter_option);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
		));
	}

	/**
	 * Return list of type => list of allowed properies (e.g. allow User filtering, Allow Course association, etc.)
	 *
	 * @return array
	 */
	public static function propsPerType() {
		$result = array(
			self::NTYPE_USER_COMPLETED_TEST => self::$PROP_ALL,
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => self::$PROP_ALL,
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => self::$PROP_ALL,
			self::NTYPE_COURSE_PROP_CHANGED => self::$PROP_ALL,
			self::NTYPE_NEW_CATEGORY => self::$PROP_NO_COURSE,
			self::NTYPE_NEW_REPLY => self::$PROP_ALL,
			self::NTYPE_NEW_THREAD => self::$PROP_ALL,
			self::NTYPE_NEW_COMMENT => self::$PROP_ALL,
			self::NTYPE_NEW_MESSAGE_RECEIVED => self::$PROP_ALL,
			self::NTYPE_USER_APPROVAL => self::$PROP_ALL,
			self::NTYPE_USER_BOUGHT_COURSE => self::$PROP_ALL,
			self::NTYPE_STUDENT_COMPLETED_COURSE => self::$PROP_ALL,
			self::NTYPE_USER_SUBSCRIBED_COURSE => self::$PROP_ALL,
			self::NTYPE_USER_WAITING_SUBSCRIBE => self::$PROP_ALL,
			self::NTYPE_USER_COURSELEVEL_CHANGED => self::$PROP_ALL,
			self::NTYPE_USER_UNSUBSCRIBED => self::$PROP_ALL,
			self::NTYPE_USER_DELETED => self::$PROP_NO_COURSE,
			self::NTYPE_USER_SUBSCRIBED_GROUP => self::$PROP_NO_COURSE,
			self::NTYPE_USER_REMOVED_FROM_GROUP => self::$PROP_NO_COURSE,
			self::NTYPE_USER_MODIFIED => self::$PROP_NO_COURSE,
			self::NTYPE_NEW_COURSE => self::$PROP_NO_COURSE,
			self::NTYPE_NEW_LO_ADDED_TO_COURSE => self::$PROP_NO_USER,
			self::NTYPE_COURSE_DELETED => self::$PROP_ALL,
			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => self::$PROP_NO_USERCOURSE,
			self::NTYPE_NEW_USER_CREATED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_NEW_USER_CREATED_SELFREG => self::$PROP_NO_USERCOURSE,
			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => self::$PROP_ALL,
			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => self::$PROP_ALL,
			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => self::$PROP_ALL,
			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => self::$PROP_ALL,
			self::NTYPE_LEARNINGPLAN_CREATED => self::$PROP_NO_USER,
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => self::$PROP_ALL,
			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_STARTING => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_STARTING => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_ENROLLED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_DECLINED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_PASSED => self::$PROP_ALL,
			self::NTYPE_ILT_SESSION_USER_FAILED => self::$PROP_ALL,
			self::NTYPE_NEWSLETTER_SENT => self::$PROP_NO_USERCOURSE,
			self::NTYPE_CATCHUP_LIMIT_CHANGED => self::$PROP_NO_COURSE,
			self::NTYPE_CATCHUP_COURSE_AVAILABLE => self::$PROP_NO_COURSE,
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => self::$PROP_ALL,
			self::NTYPE_NEW_COACHING_SESSION => self::$PROP_NO_USER,
			self::NTYPE_UPDATED_COACHING_SESSION => self::$PROP_ALL,
			self::NTYPE_DELETED_COACHING_SESSION => self::$PROP_ALL,
			self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => self::$PROP_ALL,
			self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => self::$PROP_ALL,
			self::NTYPE_COACHING_SESSION_EXPIRED => self::$PROP_ALL,
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => self::$PROP_ALL,
			self::NTYPE_MERGED_COACHING_SESSION => self::$PROP_ALL,
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => self::$PROP_ALL,
			self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => self::$PROP_ALL,
			self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => self::$PROP_ALL,
			self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => self::$PROP_ALL,
			self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_CREATED => self::$PROP_NO_USER,
			self::NTYPE_WEBINAR_SESSION_CHANGED => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_DELETED => self::$PROP_ALL,
			//digest
			self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE => self::$PROP_ALL,
			self::NTYPE_DIGEST_COURSE_HAS_EXPIRED => self::$PROP_ALL,
			self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE => self::$PROP_ALL,
			self::NTYPE_COURSE_NOT_YET_COMPLETE => self::$PROP_ALL_NO_LP,
			self::NTYPE_USER_EMAIL_MUST_VERIFY => self::$PROP_NO_COURSE,
			self::NTYPE_ECOMMERCE_SETTINGS_CHANGED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_BLOG_COMMENT => self::$PROP_NO_USERCOURSE,
			self::NTYPE_BLOG_POST_IN_COURSE => self::$PROP_ALL,
			self::NTYPE_BLOG_COMMENT_IN_COURSE => self::$PROP_ALL,
			self::NTYPE_APP7020_INVITATION => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_QUESTION => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_CONTENTS_QUESTION => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_CONTENT => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_PEER_REVIEW => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_PUBLISHING => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_ANSWER => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_BEST_ANSWER => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_TOPIC_ASSIGNMENT => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_CHANNEL_ASSIGNMENT => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_TOOLTIP => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_DELETE_ASSET => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_DELETE_ASSET_BY_ADMIN => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_MENTIONED_ANSWER => self::$PROP_NO_USERCOURSE,
			self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => self::$PROP_NO_USERCOURSE,
			self::NTYPE_GAMIFICATION_CONTEST_START => self::$PROP_NO_COURSE,
			self::NTYPE_GAMIFICATION_CONTEST_END => self::$PROP_NO_COURSE,
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => self::$PROP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => self::$PROP_ALL,
			// Subscription
			self::NTYPE_SUB_PLAN_PURCHASED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_SUB_PLAN_EXPIRED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_SUB_BUNDLE_CREATED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => self::$PROP_NO_USERCOURSE,
			self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => self::$PROP_NO_USERCOURSE,
			self::NTYPE_SUB_BUNDLE_EDITED => self::$PROP_NO_USERCOURSE,
		);
		return $result;
	}




	/**
	 * Multidimensional array of shorcodes per notification type
	 *
	 * @return array
	 */
	public static function shortcodesPerType() {
		$result = array(
			self::NTYPE_USER_COMPLETED_TEST => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_TEST_NAME,
				self::SC_TEST_COMPLETION_DATE,
				self::SC_TEST_SCORE,
				self::SC_TEST_RESULTS,
			),
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_TEST_NAME,
				self::SC_TEST_COMPLETION_DATE,
				self::SC_TEST_SCORE,
				self::SC_TEST_RESULTS,
			),
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_TEST_NAME,
				self::SC_TEST_COMPLETION_DATE,
				self::SC_TEST_SCORE,
				self::SC_TEST_RESULTS,
				self::SC_QUESTION_SCORE,
				self::SC_QUESTION_TITLE,
				self::SC_QUESTION_ANSWER,
			),
			self::NTYPE_COURSE_PROP_CHANGED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
			),

			self::NTYPE_NEW_CATEGORY => array(
				self::SC_CATEGORYNAME,
			),

			self::NTYPE_NEW_REPLY => array(
				self::SC_FORUM,
				self::SC_FORUM_THREAD,
				self::SC_FORUM_THREAD_LINK,
				self::SC_FORUM_LINK
			),

			self::NTYPE_NEW_THREAD => array(
				self::SC_FORUM,
				self::SC_FORUM_THREAD,
				self::SC_FORUM_THREAD_LINK,
				self::SC_FORUM_LINK,
				self::SC_FIRSTNAME,
			),

			self::NTYPE_NEW_COMMENT => array(
				self::SC_COURSENAME,
				self::SC_FORUM,
				self::SC_FORUM_THREAD_LINK,
				self::SC_FORUM_LINK,
				self::SC_FORUM_COMMENT,
			),

			self::NTYPE_NEW_MESSAGE_RECEIVED => array(// @TODO
			),
			self::NTYPE_NEWSLETTER_SENT => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
			),


			self::NTYPE_USER_APPROVAL => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
			),

			self::NTYPE_USER_BOUGHT_COURSE => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_ORDER_RECEIPT_CONTENT,
				self::SC_ORDER_ID,
				self::SC_ORDER_PAYMENT_DATE,
				self::SC_ORDER_SUBTOTAL,
				self::SC_ORDER_DISCOUNT,
				self::SC_ORDER_TOTAL_PAID,
				self::SC_ORDER_COMPANY,
				self::SC_ORDER_ADDRESS,
				self::SC_ORDER_STATE,
				self::SC_ORDER_ZIP,
				self::SC_ORDER_CITY,
				self::SC_ORDER_COUNTRY,
				self::SC_ORDER_ITEMS,
			),

			self::NTYPE_STUDENT_COMPLETED_COURSE => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_COMPLETED_AT,
				self::SC_FINAL_SCORE,
			),

			self::NTYPE_USER_SUBSCRIBED_COURSE => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SUBSCRIBED_AT,
				self::SC_EXPIRE_AT,
				self::SC_SSO_EXPIRING_LINK,
				self::SC_RESET_PASSWORD_LINK,
			),

			self::NTYPE_USER_WAITING_SUBSCRIBE => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SUBSCRIBED_AT,
				self::SC_SESSION_NAME . '<b> Only for Classroom courses.</b>',
				self::SC_SESSION_DATES . '<b> Only for Classroom courses.</b>',
				self::SC_SESSION_LOCATION . '<b> Only for Classroom courses.</b>',
			),

			self::NTYPE_USER_COURSELEVEL_CHANGED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_USER_COURSE_LEVEL,
				self::SC_COURSE_LINK,
			),

			self::NTYPE_USER_UNSUBSCRIBED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
			),

			self::NTYPE_USER_DELETED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
			),

			self::NTYPE_USER_SUBSCRIBED_GROUP => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_GROUPNAME,
				self::SC_GROUPID,
			),

			self::NTYPE_USER_REMOVED_FROM_GROUP => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_GROUPNAME,
				self::SC_GROUPID,
			),

			self::NTYPE_USER_MODIFIED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
			),

			self::NTYPE_NEW_COURSE => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
			),

			self::NTYPE_NEW_LO_ADDED_TO_COURSE => array(
				self::SC_COURSENAME,
				self::SC_LO_TITLE,
				self::SC_COURSE_LINK,
				self::SC_LO_TYPE,
			),

			self::NTYPE_COURSE_DELETED => array(
				self::SC_COURSENAME,
			),

			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_USERNAME,
			),

			self::NTYPE_NEW_USER_CREATED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_USER_PASSWORD,
				self::SC_USERNAME,
				self::SC_LMS_LINK,
				self::SC_USER_REGISTER_DATE,
				self::SC_VERIFICATION_LINK,
			),

			self::NTYPE_NEW_USER_CREATED_SELFREG => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_USERNAME,
				self::SC_LMS_LINK,
			),

			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_LEARNINGPLAN_NAME,
				self::SC_COMPLETED_AT,
				self::SC_CERTIFICATE_URL,
			),

			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_LEARNINGPLAN_NAME,
			),

			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_LEARNINGPLAN_NAME,
				self::SC_LEARNINGPLAN_URL,
			),

			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => array(
				self::SC_LEARNINGPLAN_NAME,
			),
			self::NTYPE_LEARNINGPLAN_CREATED => array(
				self::SC_LEARNINGPLAN_NAME,
			),
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => array(
				self::SC_LEARNINGPLAN_NAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_LEARNINGPLAN_USER_VALIDITY_DATE_START,
				self::SC_LEARNINGPLAN_USER_VALIDITY_DATE_END
			),
			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_EXPIRE_AT,
				self::SC_SSO_EXPIRING_LINK,
				self::SC_RESET_PASSWORD_LINK,
			),
			self::NTYPE_ILT_SESSION_CREATED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_CHANGED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_DELETED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_STARTING => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_WEBINAR_SESSION_STARTING => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS
			),
			self::NTYPE_ILT_SESSION_USER_ENROLLED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_USER_DECLINED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_SESSION_LOCATION,
				self::SC_COURSE_LINK,
				self::SC_CALENDAR_ATTACHMENT,
			),
			self::NTYPE_ILT_SESSION_USER_PASSED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME
			),
			self::NTYPE_ILT_SESSION_USER_FAILED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME
			),
			self::NTYPE_CATCHUP_LIMIT_CHANGED => array(
				self::SC_CATCHUP_LIMIT,
				self::SC_LEARNINGPLAN_NAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
			),
			self::NTYPE_CATCHUP_COURSE_AVAILABLE => array(
				self::SC_LEARNINGPLAN_NAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
			),
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_EVALUATION,
				self::SC_ASSIGNMENT_STATUS,
				self::SC_EVALUATION_MESSAGE
			),
			self::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
			),
			self::NTYPE_NEW_COACHING_SESSION => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES
			),
			self::NTYPE_UPDATED_COACHING_SESSION => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES
			),
			self::NTYPE_DELETED_COACHING_SESSION => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES
			),
			self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME
			),
			self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME
			),
			self::NTYPE_COACHING_SESSION_EXPIRED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES
			),
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_ASSIGNMENT_TITLE,
				self::SC_ASSIGNMENT_SUBMISSION_DATE
			),
			self::NTYPE_MERGED_COACHING_SESSION => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES,
				self::SC_COACH_SESSION_TARGET
			),
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_ASSIGNMENT_TITLE,
				self::SC_ASSIGNMENT_SUBMISSION_DATE
			),
			self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES,
				self::SC_COACH_CHAT_MESSAGE
			),
			self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES,
				self::SC_COACH_CHAT_MESSAGE
			),
			self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES,
				self::SC_COACH_WEBINAR_DATE_BEGIN,
				self::SC_COACH_WEBINAR_DATE_END,
				self::SC_COACH_WEBINAR_NAME,
				self::SC_COACH_WEBINAR_URL,
				self::SC_COACH_WEBINAR_DETAILS
			),
			self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COACH_NAME,
				self::SC_COACH_EMAIL,
				self::SC_COACH_SESSION_DATES,
				self::SC_COACH_WEBINAR_DATE_BEGIN,
				self::SC_COACH_WEBINAR_DATE_END,
				self::SC_COACH_WEBINAR_NAME,
				self::SC_COACH_WEBINAR_URL,
				self::SC_COACH_WEBINAR_DETAILS
			),
			self::NTYPE_WEBINAR_SESSION_CREATED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS
			),
			self::NTYPE_WEBINAR_SESSION_CHANGED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS
			),
			self::NTYPE_WEBINAR_SESSION_DELETED => array(
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL
			),
			self::NTYPE_COURSE_NOT_YET_COMPLETE => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_COURSENAME,
				self::SC_COURSE_LINK,
				self::SC_SUBSCRIBED_AT,
				self::SC_EXPIRE_AT,
				self::SC_SSO_EXPIRING_LINK,
				self::SC_RESET_PASSWORD_LINK,
			),

			/* BLOG NOTIFICATION */
			self::NTYPE_BLOG_COMMENT => array(
				self::SC_BLOG_COMMENT_USER,
				self::SC_BLOG_TEXT,
				self::SC_BLOG_COMMENT,
				self::SC_BLOG_DATE_PUBLISHED,
			),
			self::NTYPE_BLOG_POST_IN_COURSE => array(
				self::SC_COURSENAME,
				self::SC_BLOG_WRITER,
				self::SC_BLOG_TITLE,
				self::SC_BLOG_TEXT,
				self::SC_BLOG_DATE_PUBLISHED,
			),
			self::NTYPE_BLOG_COMMENT_IN_COURSE => array(
				self::SC_COURSENAME,
				self::SC_BLOG_WRITER,
				self::SC_BLOG_COMMENT_USER,
				self::SC_BLOG_TITLE,
				self::SC_BLOG_TEXT,
				self::SC_BLOG_COMMENT,
				self::SC_BLOG_DATE_PUBLISHED,
			),
			self::NTYPE_APP7020_INVITATION => array(
				self::SC_FIRSTNAME,
				self::SC_APP7020_CONTENT_TITLE,
				self::SC_APP7020_CONTENT_URL,
				self::SC_APP7020_CONTENT_THUMB,
				self::SC_APP7020_CONTENT_INVITER,
				self::SC_APP7020_CONTENT_INVITER_AVATAR,
			),
			self::NTYPE_APP7020_NEW_QUESTION => array(
				self::SC_APP7020_QUESTION_TITLE,
				self::SC_APP7020_QUESTION_AUTHOR,
				self::SC_APP7020_QUESTION_URL,
				self::SC_APP7020_QUESTION_OWNER_AVATAR,
				self::SC_APP7020_QUESTION_EMAIL_USERS,
				self::SC_APP7020_QUESTION_RECIPIENT_FIRSTNAME,
			),
			self::NTYPE_APP7020_NEW_CONTENTS_QUESTION => array(
				self::SC_APP7020_QUESTION_TITLE,
				self::SC_APP7020_QUESTION_AUTHOR,
				self::SC_APP7020_QUESTION_URL,
				self::SC_APP7020_CONTENT_URL,
				self::SC_APP7020_CONTENT_TITLE
			),
			self::NTYPE_APP7020_NEW_CONTENT => array(
				self::SC_APP7020_CONTENT_TITLE,
				self::SC_APP7020_CONTENT_URL,
				self::SC_APP7020_CONTENT_AUTHOR,
			),
			self::NTYPE_APP7020_NEW_PEER_REVIEW => array(
				self::SC_APP7020_CONTENT_TITLE,
				self::SC_APP7020_CONTENT_URL,
				self::SC_APP7020_PEER_REVIEW_TEXT,
				self::SC_APP7020_PEER_REVIEW_AUTHOR,
				self::SC_APP7020_RECIPIENT_FIRST_NAME,
			),
			self::NTYPE_APP7020_PUBLISHING => array(
				self::SC_APP7020_CONTENT_TITLE,
				self::SC_APP7020_CONTENT_URL,
				self::SC_APP7020_PUBLISHER,
			),
			self::NTYPE_APP7020_NEW_ANSWER => array(
				self::SC_APP7020_ANSWER_AUTHOR,
				self::SC_APP7020_QUESTION_URL,
				self::SC_APP7020_ANSWER_OWNER_AVATAR,
				self::SC_APP7020_QUESTION_AUTHOR,
				self::SC_APP7020_ANSWER_TITLE,
				self::SC_APP7020_QUESTION_OWNER_EMAIL,
			),
			self::NTYPE_APP7020_BEST_ANSWER => array(
				self::SC_APP7020_QUESTION_AUTHOR,
				self::SC_APP7020_QUESTION_URL,
			),
			self::NTYPE_APP7020_TOPIC_ASSIGNMENT => array(
				self::SC_APP7020_SUPER_ADMIN,
				self::SC_APP7020_TOPIC_NAME,
			),
			self::NTYPE_APP7020_CHANNEL_ASSIGNMENT => array(
				self::SC_APP7020_SUPER_ADMIN,
				self::SC_APP7020_CHANNEL_NAME,
			),
			self::NTYPE_APP7020_MENTIONED_ANSWER => array(
				self::SC_APP7020_MENTIONED_ANSWER_FIRST_NAME,
				self::SC_APP7020_MENTIONED_ANSWER_OWNER,
				self::SC_APP7020_MENTIONED_ANSWER_OWNER_AVATAR,
				self::SC_APP7020_MENTIONED_ANSWER_QUESTION_URL_PAGE,
				self::SC_APP7020_MENTIONED_ANSWER_QUESTION_TITLE
			),
			self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => array(
				self::SC_APP7020_ASSET_TO_REVIEW_FIRSTNAME,
				self::SC_APP7020_ASSET_TO_REVIEW_AUTHOR,
				self::SC_APP7020_ASSET_TO_REVIEW_TITLE,
				self::SC_APP7020_ASSET_TO_REVIEW_THUMB,
				self::SC_APP7020_ASSET_TO_REVIEW_EMAIL,
				self::SC_APP7020_ASSET_TO_REVIEW_URL,
				self::SC_APP7020_ASSET_TO_REVIEW_AVATAR,
			),
			self::NTYPE_USER_EMAIL_MUST_VERIFY => array(
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USERNAME,
				self::SC_USER_EMAIL,
				self::SC_LMS_LINK,
				self::SC_VERIFICATION_LINK,
			),
			self::NTYPE_APP7020_NEW_TOOLTIP => array(
				self::SC_APP7020_TOOLTIP_OWNER,
				self::SC_APP7020_TOOLTIP_BODY,
				self::SC_APP7020_TOOLTIP_POSITION,
				self::SC_APP7020_CONTENT_TITLE,
				self::SC_APP7020_CONTENT_URL,
			),
			self::NTYPE_APP7020_DELETE_ASSET => array(
				self::SC_APP7020_CONTENT_AUTHOR,
				self::SC_APP7020_CONTENT_TITLE
			),
			self::NTYPE_APP7020_DELETE_ASSET_BY_ADMIN => array(
				self::SC_APP7020_SUPER_ADMIN,
				self::SC_APP7020_CONTENT_TITLE
			),
			self::NTYPE_GAMIFICATION_CONTEST_START => array(
				self::SC_GAMIFICATION_CONTEST_CONTEST_DESCRIPTION,
				self::SC_GAMIFICATION_CONTEST_CONTEST_END_DATE,
				self::SC_GAMIFICATION_CONTEST_CONTEST_GOAL,
				self::SC_GAMIFICATION_CONTEST_CONTEST_LINK,
				self::SC_GAMIFICATION_CONTEST_CONTEST_NAME,
				self::SC_GAMIFICATION_CONTEST_CONTEST_START_DATE
			),
			self::NTYPE_GAMIFICATION_CONTEST_END => array(
				self::SC_GAMIFICATION_CONTEST_CONTEST_DESCRIPTION,
				self::SC_GAMIFICATION_CONTEST_CONTEST_END_DATE,
				self::SC_GAMIFICATION_CONTEST_CONTEST_GOAL,
				self::SC_GAMIFICATION_CONTEST_CONTEST_LINK,
				self::SC_GAMIFICATION_CONTEST_CONTEST_NAME,
				self::SC_GAMIFICATION_CONTEST_CONTEST_START_DATE
			),
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS,
			),
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS,
			),
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS,
			),
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS,
			),
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => array(
				self::SC_SESSION_NAME,
				self::SC_SESSION_DATES,
				self::SC_COURSE_LINK,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_COURSENAME,
				self::SC_CALENDAR_ATTACHMENT,
				self::SC_WEBINAR_WEBINAR_TOOL,
				self::SC_WEBINAR_SESSION_DETAILS,
			),
			self::NTYPE_ECOMMERCE_SETTINGS_CHANGED => array(
				self::SC_USERNAME,
				self::SC_FIRSTNAME,
				self::SC_LASTNAME,
				self::SC_USER_EMAIL,
			),
			// Subscription Record
			self::NTYPE_SUB_PLAN_PURCHASED => array(
				self::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE,
				self::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE,
				self::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE,
				self::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_RECORD_TYPE,
				self::SC_SUBSCRIPTION_RECORD_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_PRICE,
				self::SC_SUBSCRIPTION_RECORD_NOTE,
				self::SC_SUBSCRIPTION_RECORD_PLAN_NAME,
				self::SC_SUBSCRIPTION_RECORD_SOLD_TO,
			),
			self::NTYPE_SUB_PLAN_EXPIRED => array(
				self::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE,
				self::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE,
				self::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE,
				self::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_RECORD_TYPE,
				self::SC_SUBSCRIPTION_RECORD_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_PRICE,
				self::SC_SUBSCRIPTION_RECORD_NOTE,
				self::SC_SUBSCRIPTION_RECORD_PLAN_NAME,
				self::SC_SUBSCRIPTION_RECORD_SOLD_TO
			),
			self::NTYPE_SUB_BUNDLE_CREATED => array(
				self::SC_SUBSCRIPTION_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_BUNDLE_CODE,
				self::SC_SUBSCRIPTION_BUNDLE_DESCRIPTION,
				self::SC_SUBSCRIPTION_BUNDLE_TYPE,
				self::SC_SUBSCRIPTION_BUNDLE_CATALOG_LIST,
				self::SC_SUBSCRIPTION_BUNDLE_PLAN_LIST,
			),
			self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => array(
				self::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE,
				self::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE,
				self::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE,
				self::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_RECORD_TYPE,
				self::SC_SUBSCRIPTION_RECORD_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_USED_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_REMAINING_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_PRICE,
				self::SC_SUBSCRIPTION_RECORD_NOTE,
				self::SC_SUBSCRIPTION_RECORD_PLAN_NAME,
				self::SC_SUBSCRIPTION_RECORD_SOLD_TO,
			),
			self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => array(
				self::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE,
				self::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE,
				self::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE,
				self::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_RECORD_TYPE,
				self::SC_SUBSCRIPTION_RECORD_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_USED_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_REMAINING_CAPACITY,
				self::SC_SUBSCRIPTION_RECORD_PRICE,
				self::SC_SUBSCRIPTION_RECORD_NOTE,
				self::SC_SUBSCRIPTION_RECORD_PLAN_NAME,
				self::SC_SUBSCRIPTION_RECORD_SOLD_TO,
			),
			self::NTYPE_SUB_BUNDLE_EDITED => array(
				self::SC_SUBSCRIPTION_BUNDLE_NAME,
				self::SC_SUBSCRIPTION_BUNDLE_CODE,
				self::SC_SUBSCRIPTION_BUNDLE_DESCRIPTION,
				self::SC_SUBSCRIPTION_BUNDLE_TYPE,
				self::SC_SUBSCRIPTION_BUNDLE_CATALOG_LIST,
				self::SC_SUBSCRIPTION_BUNDLE_PLAN_LIST,
			),
			//digest shortcodes
			self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE => self::$SC_PRESET_DIGEST_USER_ENROLLED_COURSE,
			//digest shortcodes
			self::NTYPE_DIGEST_COURSE_HAS_EXPIRED => self::$SC_PRESET_DIGEST_COURSE_EXPIRED,
			//digest shortcodes
			self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE => self::$SC_PRESET_DIGEST_COMPLETE_COURSE,
		);

		// Raise an event that has access to the shortcodes
		// and their hints, organized by type
		// so the listener may modify/remove/add to these
		// arrays for modifying the displayed shortcodes UI
		Yii::app()->event->raise('NotificationShortcodesGet', new DEvent(self, array(
			'shortcodes' => &$result
		)));

		return $result;
	}



	/**
	 * Return allowed (selectable) Schedule types (array) PER notification type
	 *
	 * @return array
	 */
	public static function scheduleTypesPerType() {

		$result = array(
			self::NTYPE_USER_COMPLETED_TEST => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COURSE_PROP_CHANGED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_CATEGORY => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_REPLY => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_THREAD => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_COMMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_MESSAGE_RECEIVED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_APPROVAL => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_BOUGHT_COURSE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_STUDENT_COMPLETED_COURSE => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_USER_SUBSCRIBED_COURSE => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_USER_WAITING_SUBSCRIBE => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_USER_COURSELEVEL_CHANGED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_UNSUBSCRIBED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_DELETED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_SUBSCRIBED_GROUP => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_REMOVED_FROM_GROUP => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_MODIFIED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_COURSE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_LO_ADDED_TO_COURSE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COURSE_DELETED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_USER_CREATED => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_NEW_USER_CREATED_SELFREG => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => self::$SCHEDULE_PRESET_ONLY_AT, // @TODO, implement: self::$SCHEDULE_PRESET_NO_BEFORE
			self::NTYPE_LEARNINGPLAN_CREATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_ILT_SESSION_CREATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_CHANGED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_DELETED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_STARTING => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_WEBINAR_SESSION_STARTING => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_ILT_SESSION_USER_ENROLLED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_DECLINED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_PASSED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ILT_SESSION_USER_FAILED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEWSLETTER_SENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_CATCHUP_LIMIT_CHANGED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_CATCHUP_COURSE_AVAILABLE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN => self::$SCHEDULE_PRESET_NO_BEFORE,
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => self::$SCHEDULE_PRESET_ONLY_AFTER,
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_USER_EMAIL_MUST_VERIFY => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_NEW_COACHING_SESSION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_UPDATED_COACHING_SESSION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_DELETED_COACHING_SESSION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COACHING_SESSION_EXPIRED => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_MERGED_COACHING_SESSION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => self::$SCHEDULE_PRESET_ONLY_BEFORE,
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_CREATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_CHANGED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_DELETED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_COURSE_NOT_YET_COMPLETE => self::$SCHEDULE_PRESET_ONLY_AFTER,

			self::NTYPE_BLOG_COMMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_BLOG_POST_IN_COURSE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_BLOG_COMMENT_IN_COURSE => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_INVITATION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_QUESTION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_CONTENTS_QUESTION => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_CONTENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_PEER_REVIEW => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_PUBLISHING => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_ANSWER => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_BEST_ANSWER => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_TOPIC_ASSIGNMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_CHANNEL_ASSIGNMENT => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_TOOLTIP => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_DELETE_ASSET => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_DELETE_ASSET_BY_ADMIN => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_MENTIONED_ANSWER => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_GAMIFICATION_CONTEST_START => self::$SCHEDULE_PRESET_ALL,
			self::NTYPE_GAMIFICATION_CONTEST_END => self::$SCHEDULE_PRESET_ALL,
			// Subscription
			self::NTYPE_SUB_PLAN_PURCHASED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_SUB_PLAN_EXPIRED => self::$SCHEDULE_PRESET_NO_AT,
			self::NTYPE_SUB_BUNDLE_CREATED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => self::$SCHEDULE_PRESET_ONLY_AT,
			self::NTYPE_SUB_BUNDLE_EDITED => self::$SCHEDULE_PRESET_ONLY_AT,

            //digest
            self::NTYPE_DIGEST_COURSE_HAS_EXPIRED => self::$SCHEDULE_PRESET_ONLY_EVERY,
            self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE => self::$SCHEDULE_PRESET_ONLY_EVERY,
            self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE => self::$SCHEDULE_PRESET_ONLY_EVERY,
		);

		return $result;
	}


	/**
	 * Return allowed (selectable) recipients (array) PER notification type
	 *
	 * @return array
	 */
	public static function recipientTypesPerType() {

		$tmp = array(
			self::NTYPE_USER_COMPLETED_TEST => self::$RECIP_NO_USER,
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => self::$RECIP_NO_USER_WITH_MANAGER,
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_COURSE_PROP_CHANGED => self::$RECIP_ALL,
			self::NTYPE_NEW_CATEGORY => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_NEW_REPLY => self::$RECIP_ALL,
			self::NTYPE_NEW_THREAD => self::$RECIP_ALL,
			self::NTYPE_NEW_COMMENT => self::$RECIP_ALL,
			self::NTYPE_NEW_MESSAGE_RECEIVED => self::$RECIP_ALL,
			self::NTYPE_USER_APPROVAL => self::$RECIP_ALL,
			self::NTYPE_USER_BOUGHT_COURSE => self::$RECIP_ALL,
			self::NTYPE_STUDENT_COMPLETED_COURSE => self::$RECIP_ALL_WITH_MANAGER,
			self::NTYPE_USER_SUBSCRIBED_COURSE => self::$RECIP_ALL,
			self::NTYPE_USER_WAITING_SUBSCRIBE => self::$RECIP_ALL_WITH_MANAGER,
			self::NTYPE_USER_COURSELEVEL_CHANGED => self::$RECIP_ALL,
			self::NTYPE_USER_UNSUBSCRIBED => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_USER_DELETED => self::$RECIP_GODADMIN_ONLY,
			self::NTYPE_USER_SUBSCRIBED_GROUP => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_USER_REMOVED_FROM_GROUP => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_USER_MODIFIED => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_NEW_COURSE => self::$RECIP_POWER_USER_AND_GODADMIN,
			self::NTYPE_NEW_LO_ADDED_TO_COURSE => self::$RECIP_ALL,
			self::NTYPE_COURSE_DELETED => self::$RECIP_GODADMIN_ONLY,
			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => self::$RECIP_GODADMIN_ONLY,
			self::NTYPE_NEW_USER_CREATED => self::$RECIP_USER_AND_GODADMIN,
			self::NTYPE_NEW_USER_CREATED_SELFREG => self::$RECIP_USER_AND_GODADMIN,
			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => self::$RECIP_ALL,
			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => self::$RECIP_NO_COURSE_RELATED,
			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => self::$RECIP_NO_COURSE_RELATED,
			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => self::$RECIP_NO_COURSE_RELATED,
			self::NTYPE_LEARNINGPLAN_CREATED => self::$RECIP_GODADMIN_ONLY,
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => self::$RECIP_USER_AND_GODADMIN,
			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_CREATED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_CHANGED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_DELETED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_STARTING => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_STARTING => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_ENROLLED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_DECLINED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => self::$RECIP_ALL_WITH_MANAGER,
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_PASSED => self::$RECIP_ALL,
			self::NTYPE_ILT_SESSION_USER_FAILED => self::$RECIP_ALL,
			self::NTYPE_NEWSLETTER_SENT => self::$RECIP_USER_AND_GODADMIN,
			self::NTYPE_CATCHUP_LIMIT_CHANGED => self::$RECIP_USER_AND_GODADMIN,
			self::NTYPE_CATCHUP_COURSE_AVAILABLE => self::$RECIP_ONLY_USER,
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => self::$RECIP_ALL,
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => self::$RECIP_NO_USER_WITH_MANAGER,
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => self::$RECIP_ALL_WITH_MANAGER,
			self::NTYPE_USER_EMAIL_MUST_VERIFY => self::$RECIP_ONLY_USER,
			self::NTYPE_NEW_COACHING_SESSION => self::$RECIP_ALL,
			self::NTYPE_UPDATED_COACHING_SESSION => self::$RECIP_ALL,
			self::NTYPE_DELETED_COACHING_SESSION => self::$RECIP_ALL,
			self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => self::$RECIP_NO_USER,
			self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => self::$RECIP_NO_USER,
			self::NTYPE_COACHING_SESSION_EXPIRED => self::$RECIP_ALL,
			self::NTYPE_MERGED_COACHING_SESSION => self::$RECIP_ALL,
			self::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN => self::$RECIP_ALL,
			self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => self::$RECIP_ONLY_TEACHER,
			self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => self::$RECIP_ALL,
			self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => self::$RECIP_ALL,
			self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => self::$RECIP_ALL_WITH_MANAGER,
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_CREATED => self::$RECIP_POWER_USER_AND_GODADMIN,
			self::NTYPE_WEBINAR_SESSION_CHANGED => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_DELETED => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => self::$RECIP_ALL,
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => self::$RECIP_ALL,
			self::NTYPE_COURSE_NOT_YET_COMPLETE => self::$RECIP_ALL_WITH_MANAGER,

			self::NTYPE_BLOG_COMMENT => self::$RECIP_ONLY_USER,
			self::NTYPE_BLOG_POST_IN_COURSE => self::$RECIP_ALL,
			self::NTYPE_BLOG_COMMENT_IN_COURSE => self::$RECIP_ALL,
			self::NTYPE_GAMIFICATION_CONTEST_START => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_GAMIFICATION_CONTEST_END => self::$RECIP_NO_INSTRUCTOR,
			// Subscription
			self::NTYPE_SUB_PLAN_PURCHASED => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_SUB_PLAN_EXPIRED => self::$RECIP_NO_INSTRUCTOR,
			self::NTYPE_SUB_BUNDLE_CREATED => self::$RECIP_POWER_USER_AND_GODADMIN,
			self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => self::$RECIP_POWER_USER_AND_GODADMIN,
			self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => self::$RECIP_POWER_USER_AND_GODADMIN,
			self::NTYPE_SUB_BUNDLE_EDITED => self::$RECIP_POWER_USER_AND_GODADMIN,
		);

		$result = array();
		foreach ($tmp as $type => $recipients) {
			foreach ($recipients as $recipient) {
				$result[$type][$recipient]['name'] = self::recipientName($recipient);
				$result[$type][$recipient]['descr'] = self::recipientDescr($recipient);
			}
		}

		return $result;

	}

	/**
	 * Return an array of templates for certain types of notifications
	 *
	 * @return array
	 */
	public static function templatesPerType() {
		$templates = array();

		// ******************* IMPORTANT *************
		// IF YOU ADD NEW LANGUAGE KEYS, ADD THEM AS COMMENTS FOR THE TRANSLATION SCRIPT TO FIND THEM
		// *******************************************
		$languages = CoreLangLanguage::getActiveLanguagesByBrowsercode();

		/**
		 *  ========= IMPORTANT!!! Please use the format of the array just like described and follow the steps exactly: =========
		 * when you need to add a new template, set a new element of the array:
		 * 'name' - required - the name of the notification
		 * 'subject' - required - array of elements:
		 * 		- 'module' - optional, pointing the desired translation module for the subject message. If is set will override the default "notification" translation module
		 * 		- 'key' - required, pointing the desired translation phrase, which will be executed by the above module
		 * 'message' - required - array of elements:
		 * 		- 'module' - optional, pointing the desired translation module for the message body. If is set will override the default "notification" translation module
		 * 		- 'key' - optional!!!, pointing the desired translation phrase, which will be executed by the above module
		 * 		- 'template' - optional!!!, pointing the desired template file, which need to be under \plugins\NotificationApp\views\
		 * !!! - at least one of these two parameters should be passed, if we pass the both, 'template' will override the translation string!
		 * THAT'S IT!!! Enjoy!!!
		 */
		$listOfNotificationTemplates = array(
			// Yii::t('notification', 'Admin has just created a new user')
			array(
				'name' => self::NTYPE_NEW_USER_CREATED,
				'subject' => array(
					'key' => 'Admin has just created a new user'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_NEW_USER_CREATED),
				),
			),
			// Yii::t('notification', 'Welcome onboard!')
			array(
				'name' => self::NTYPE_NEW_USER_CREATED_SELFREG,
				'subject' => array(
					'key' => 'Welcome onboard!'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_NEW_USER_CREATED_SELFREG),
				),
			),
			// Yii::t('notification', 'You have just been enrolled in course [course_name]')
			array(
				'name' => self::NTYPE_USER_SUBSCRIBED_COURSE,
				'subject' => array(
					'key' => 'You have just been enrolled in course [course_name]'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_USER_SUBSCRIBED_COURSE),
				),
			),
			// Yii::t('notification', 'You have completed course [course_name]')
			array(
				'name' => self::NTYPE_STUDENT_COMPLETED_COURSE,
				'subject' => array(
					'key' => 'You have completed course [course_name]'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_STUDENT_COMPLETED_COURSE),
				),
			),
			// Yii::t('app7020', 'A new content to watch')
			array(
				'name' => self::NTYPE_APP7020_INVITATION,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new content to watch'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_INVITATION),
				),
			),
			// Yii::t('app7020', 'A new published content')
			array(
				'name' => self::NTYPE_APP7020_PUBLISHING,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new published content'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_PUBLISHING),
				),
			),
			// Yii::t('app7020', 'A new answer to your question')
			array(
				'name' => self::NTYPE_APP7020_NEW_ANSWER,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new answer to your question'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_NEW_ANSWER),
				),
			),
			// Yii::t('app7020', 'A new peer review message')
			array(
				'name' => self::NTYPE_APP7020_NEW_PEER_REVIEW,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new peer review message'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_NEW_PEER_REVIEW),
				),
			),
			// Yii::t('app7020', 'A new question for you')
			array(
				'name' => self::NTYPE_APP7020_NEW_QUESTION,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new question for you'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_NEW_QUESTION),
				),
			),
			// Yii::t('app7020', 'A new mentioned for you')
			array(
				'name' => self::NTYPE_APP7020_MENTIONED_ANSWER,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new mentioned for you'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_MENTIONED_ANSWER),
				),
			),
			array(
				'name' => self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW,
				'subject' => array(
					'module' => 'app7020',
					'key' => 'A new asset to review'
				),
				'message' => array(
					'module' => 'app7020',
					'template' => lcfirst(self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW),
				)
			),
			// Yii::t('notification', 'Impending expiry of course');
			array(
				'name' => self::NTYPE_COURSE_PLAY_HAS_EXPIRED,
				'subject' => array(
					'key' => 'Impending expiry of course'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_COURSE_PLAY_HAS_EXPIRED),
				),
			),
			// Yii::t('notification', 'New forum discussion');
			array(
				'name' => self::NTYPE_NEW_THREAD,
				'subject' => array(
					'key' => 'New forum discussion'
				),
				'message' => array(
					'template' => lcfirst(self::NTYPE_NEW_THREAD),
				),
			),
			array(
				'name'=>self::NTYPE_DIGEST_COURSE_HAS_EXPIRED,
				'subject'=>array(
					'key'=>'Course has expired'
				),
				'message'=>array(
					'template'=>lcfirst(self::NTYPE_DIGEST_COURSE_HAS_EXPIRED)
				),
			),
			array(
				'name'=>self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE,
				'subject'=>array(
					'key'=>'User enrolled to course'
				),
				'message'=>array(
					'template'=>lcfirst(self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE)
				),
			),
			array(
				'name'=>self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE,
				'subject'=>array(
					'key'=>'User has yet to complete course'
				),
				'message'=>array(
					'template'=>lcfirst(self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE)
				),
			),
		);

		foreach ($listOfNotificationTemplates as $notificationTemplate) {
			$translationModuleSubject = 'notification';
			if (!empty($notificationTemplate['subject']['module']))
				$translationModuleSubject = $notificationTemplate['subject']['module'];
			$textToTranslateSubject = $notificationTemplate['subject']['key'];

			$translationModuleMessage = 'notification';
			if (!empty($notificationTemplate['message']['module']))
				$translationModuleMessage = $notificationTemplate['message']['module'];
			$textToTranslateMessage = !empty($notificationTemplate['message']['key']) ? $notificationTemplate['message']['key'] : '';

			$useTemplate = true;
			$viewFile = $notificationTemplate['message']['template'];
			if (empty($notificationTemplate['message']['template']))
				$useTemplate = false;
			foreach ($languages as $shortLang => $language) {
				// make translation of the subject and message
				$templates[$notificationTemplate['name']]['subjects'][$shortLang] = Yii::t($translationModuleSubject, $textToTranslateSubject, array(), null, $shortLang);
				if ($useTemplate) {
					$message = '';
					if (Yii::app() instanceof CWebApplication) {
						$message = Yii::app()->getController()->renderPartial('NotificationApp.views.' . $viewFile, array('shortLang' => $shortLang), true);
					} else if (Yii::app() instanceof CConsoleApplication) {
						$message = Yii::app()->getCommand()->renderFile(Yii::getPathOfAlias('NotificationApp') . '/views/' . $viewFile . '.php', array('shortLang' => $shortLang), true);
					}
				} else
					$message = Yii::t($translationModuleMessage, $textToTranslateMessage, array(), null, $shortLang);
				$templates[$notificationTemplate['name']]['messages'][$shortLang] = $message;
			}
		}

		return $templates;
	}

	/**
	 * Returns an array of shortcodes for all notification types
	 *
	 * @return array  Array of  [<type>] => array(<code> => <code>)
	 */
	public static function shortcodes() {
		$result = array();
		$shortcodesPerType = self::shortcodesPerType();

		foreach ($shortcodesPerType as $type => $shortcodes) {
			$tmp = array();
			foreach ($shortcodes as $index => $shortcode) {
				$tmp[$shortcode] = $shortcode;
			}
			$result[$type] = $tmp;
		}
		return $result;
	}


	/**
	 * Simplify the way to get the Notification subject/message in current App language.
	 * Use it like  $notificationModel->subject or $notificationModel->message
	 *
	 * (non-PHPdoc)
	 * @see CActiveRecord::__get()
	 */
	public function __get($name) {

		if ($name == 'from_email' || $name == 'from_name') {
			// NOTE: some notification handlers still use these fields, although they have been removed. This is just a patch
			// to allow them running the same without generating fatal errors and breaking the execution of the code.
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select("ta.additional_info");
			$cmd->from(CoreNotificationTransportActivation::model()->tableName() . " ta");
			$cmd->join(CoreNotificationTransport::model()->tableName() . " t", "ta.transport_id = t.id AND t.name = :transport_name");
			$cmd->where("ta.notification_id = :notification_id");
			$record = $cmd->queryRow(true, array(
				':notification_id' => $this->id,
				':transport_name' => 'email'
			));
			if (!empty($record)) {
				$info = CJSON::decode($record['additional_info']);
				if (!empty($info) && isset($info[$name])) {
					return $info[$name];
				} else {
					return '';
				}
			} else {
				return '';
			}
		} else if ($name == 'notify_type') {
			// NOTE: there are still some handlers using this old fields (now removed). To allow them working again, we have
			// set this patch. It retrieves all transports names used by this notification and packs then in an array.
			$output = array();
			/* @var $cmd CDbCommand */
			$cmd = Yii::app()->db->createCommand();
			$cmd->select("t.id, t.name");
			$cmd->from(CoreNotificationTransportActivation::model()->tableName() . " ta");
			$cmd->join(CoreNotificationTransport::model()->tableName() . " t", "ta.transport_id = t.id AND ta.notification_id = :notification_id");
			$records = $cmd->queryAll(true, array(':notification_id' => $this->id));
			if (!empty($records) && is_array($records)) {
				foreach ($records as $record) {
					$output[] = $record['name'];
				}
			}
			return $output;
		} else if ($name == 'subject') {
			return $this->lang->subject;
		} else if ($name == 'message') {
			return $this->lang->message;
		} else
			return parent::__get($name);
	}


	/**
	 * Returns the localized name of a notification group
	 * @param $groupId
	 */
	public static function groupTranslation($groupId) {
		switch ($groupId) {
			case self::GROUP_COURSES:
				return Yii::t('standard', '_COURSES');
			case self::GROUP_COACH_SHARE:
				return Yii::t('app7020', 'Coach & Share');
			case self::GROUP_GAMIFICATION:
				return Yii::t('gamification', 'Gamification');
			case self::GROUP_LEARNINGPLANS:
				return Yii::t('myactivities', 'Learning plans');
			case self::GROUP_USERS:
				return Yii::t('standard', '_USERS');
			case self::GROUP_FORUM:
				return Yii::t('course', 'Forums');
			case self::GROUP_BLOG:
				return Yii::t('myblog', 'Blog');
			case self::GROUP_COACHING:
				return Yii::t('course', 'Coaching');
			case self::GROUP_SUBSCRIPTION:
				return Yii::t('standard', 'Subscription Plans');
			default:
				return '';
		}
	}

	/**
	 * Maps notifications into groups
	 */
	public static function groupsPerType() {
		return array(
			self::NTYPE_USER_COMPLETED_TEST => self::GROUP_COURSES,
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => self::GROUP_COURSES,
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => self::GROUP_COURSES,
			self::NTYPE_COURSE_PROP_CHANGED => self::GROUP_COURSES,
			self::NTYPE_NEW_CATEGORY => self::GROUP_COURSES,
			self::NTYPE_NEW_REPLY => self::GROUP_FORUM,
			self::NTYPE_NEW_THREAD => self::GROUP_FORUM,
			self::NTYPE_NEW_COMMENT => self::GROUP_FORUM,
			self::NTYPE_NEW_MESSAGE_RECEIVED => self::GROUP_FORUM,
			self::NTYPE_NEWSLETTER_SENT => self::GROUP_USERS,
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => self::GROUP_COURSES,
			self::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN => self::GROUP_LEARNINGPLANS,
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => self::GROUP_COURSES,
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => self::GROUP_COURSES,
			self::NTYPE_USER_EMAIL_MUST_VERIFY => self::GROUP_USERS,
			self::NTYPE_USER_APPROVAL => self::GROUP_USERS,
			self::NTYPE_USER_BOUGHT_COURSE => self::GROUP_COURSES,
			self::NTYPE_STUDENT_COMPLETED_COURSE => self::GROUP_COURSES,
			self::NTYPE_USER_SUBSCRIBED_COURSE => self::GROUP_COURSES,
			self::NTYPE_USER_WAITING_SUBSCRIBE => self::GROUP_COURSES,
			self::NTYPE_USER_COURSELEVEL_CHANGED => self::GROUP_COURSES,
			self::NTYPE_USER_UNSUBSCRIBED => self::GROUP_COURSES,
			self::NTYPE_USER_DELETED => self::GROUP_USERS,
			self::NTYPE_USER_SUSPENDED => self::GROUP_USERS,
			self::NTYPE_USER_UNSUSPENDED => self::GROUP_USERS,
			self::NTYPE_USER_SUBSCRIBED_GROUP => self::GROUP_USERS,
			self::NTYPE_USER_REMOVED_FROM_GROUP => self::GROUP_USERS,
			self::NTYPE_USER_ASSIGNED_TO_BRANCH => self::GROUP_USERS,
			self::NTYPE_USER_REMOVED_FROM_BRANCH => self::GROUP_USERS,
			self::NTYPE_USER_MODIFIED => self::GROUP_USERS,
			self::NTYPE_NEW_COURSE => self::GROUP_COURSES,
			self::NTYPE_NEW_LO_ADDED_TO_COURSE => self::GROUP_COURSES,
			self::NTYPE_COURSE_DELETED => self::GROUP_COURSES,
			self::NTYPE_COURSE_NOT_YET_COMPLETE => self::GROUP_COURSES,
			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => self::GROUP_LEARNINGPLANS,
			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => self::GROUP_LEARNINGPLANS,
			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => self::GROUP_LEARNINGPLANS,
			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => self::GROUP_LEARNINGPLANS,
			self::NTYPE_LEARNINGPLAN_CREATED => self::GROUP_LEARNINGPLANS,
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => self::GROUP_LEARNINGPLANS,
			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => self::GROUP_COURSES,
			self::NTYPE_NEW_USER_CREATED => self::GROUP_USERS,
			self::NTYPE_NEW_USER_CREATED_SELFREG => self::GROUP_USERS,
			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_CREATED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_CHANGED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_DELETED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_STARTING => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_STARTING => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_ENROLLED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_DECLINED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_PASSED => self::GROUP_COURSES,
			self::NTYPE_ILT_SESSION_USER_FAILED => self::GROUP_COURSES,
			self::NTYPE_CATCHUP_COURSE_AVAILABLE => self::GROUP_COURSES,
			self::NTYPE_CATCHUP_LIMIT_CHANGED => self::GROUP_COURSES,
			self::NTYPE_NEW_COACHING_SESSION => self::GROUP_COACHING,
			self::NTYPE_UPDATED_COACHING_SESSION => self::GROUP_COACHING,
			self::NTYPE_DELETED_COACHING_SESSION => self::GROUP_COACHING,
			self::NTYPE_MERGED_COACHING_SESSION => self::GROUP_COACHING,
			self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => self::GROUP_COACHING,
			self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => self::GROUP_COACHING,
			self::NTYPE_COACHING_SESSION_EXPIRED => self::GROUP_COACHING,
			self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => self::GROUP_COACHING,
			self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => self::GROUP_COACHING,
			self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => self::GROUP_COACHING,
			self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => self::GROUP_COACHING,
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_CREATED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_CHANGED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_DELETED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => self::GROUP_COURSES,
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => self::GROUP_COURSES,
			//digest
			self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE => self::GROUP_COURSES,
			self::NTYPE_DIGEST_COURSE_HAS_EXPIRED => self::GROUP_COURSES,
			self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE => self::GROUP_COURSES,
			self::NTYPE_BLOG_COMMENT => self::GROUP_BLOG,
			self::NTYPE_BLOG_POST_IN_COURSE => self::GROUP_BLOG,
			self::NTYPE_BLOG_COMMENT_IN_COURSE => self::GROUP_BLOG,
			self::NTYPE_APP7020_INVITATION => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_QUESTION => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_CONTENTS_QUESTION => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_CONTENT => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_PEER_REVIEW => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_PUBLISHING => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_ANSWER => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_BEST_ANSWER => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_TOPIC_ASSIGNMENT => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_CHANNEL_ASSIGNMENT => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_TOOLTIP => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_DELETE_ASSET => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_DELETE_ASSET_BY_ADMIN => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_MENTIONED_ANSWER => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_INVITATION => self::GROUP_COACH_SHARE,
			self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => self::GROUP_COACH_SHARE,
			self::NTYPE_GAMIFICATION_CONTEST_START => self::GROUP_GAMIFICATION,
			self::NTYPE_GAMIFICATION_CONTEST_END => self::GROUP_GAMIFICATION,
			// Subscription
			self::NTYPE_SUB_PLAN_PURCHASED => self::GROUP_SUBSCRIPTION,
			self::NTYPE_SUB_PLAN_EXPIRED => self::GROUP_SUBSCRIPTION,
			self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => self::GROUP_SUBSCRIPTION,
			self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => self::GROUP_SUBSCRIPTION,
			self::NTYPE_SUB_BUNDLE_CREATED => self::GROUP_SUBSCRIPTION,
			self::NTYPE_SUB_BUNDLE_EDITED => self::GROUP_SUBSCRIPTION
		);
	}

	/**
	 * Return an array of translated notification names
	 */
	public static function types() {
		$result = array(
			self::NTYPE_USER_COMPLETED_TEST => Yii::t('notification', 'User completed or failed a test in a course'),
			self::NTYPE_USER_TEST_EVALUATION_NEEDED => Yii::t('notification', 'User submitted a test and evaluation is needed'),
			self::NTYPE_INSTRUCTOR_EVALUATED_QUESTION => Yii::t('notification', 'Instructor evaluated a test question'),
			self::NTYPE_COURSE_PROP_CHANGED => Yii::t('event_manager', '_EVENT_CLASS_CoursePorpModified'),
			self::NTYPE_NEW_CATEGORY => Yii::t('event_manager', '_EVENT_CLASS_ForumNewCategory'),
			self::NTYPE_NEW_REPLY => Yii::t('event_manager', '_EVENT_CLASS_ForumNewResponse'),
			self::NTYPE_NEW_THREAD => Yii::t('event_manager', '_EVENT_CLASS_ForumNewThread'),
			self::NTYPE_NEW_COMMENT => Yii::t('event_manager', 'New Comment'),
			self::NTYPE_USER_APPROVAL => Yii::t('event_manager', '_EVENT_CLASS_UserApproved'),
			self::NTYPE_USER_BOUGHT_COURSE => Yii::t('event_manager', '_EVENT_CLASS_UserCourseBuy'),
			self::NTYPE_STUDENT_COMPLETED_COURSE => Yii::t('event_manager', '_EVENT_CLASS_UserCourseEnded'),
			self::NTYPE_USER_SUBSCRIBED_COURSE => Yii::t('event_manager', '_EVENT_CLASS_UserCourseInserted'),
			self::NTYPE_USER_WAITING_SUBSCRIBE => Yii::t('event_manager', '_EVENT_CLASS_UserCourseInsertModerate'),
			self::NTYPE_USER_COURSELEVEL_CHANGED => Yii::t('event_manager', '_EVENT_CLASS_UserCourseLevelChanged'),
			self::NTYPE_USER_UNSUBSCRIBED => Yii::t('event_manager', '_EVENT_CLASS_UserCourseRemoved'),
			self::NTYPE_USER_DELETED => Yii::t('event_manager', '_EVENT_CLASS_UserDel'),
			self::NTYPE_USER_SUBSCRIBED_GROUP => Yii::t('event_manager', '_EVENT_CLASS_UserGroupInsert'),
			self::NTYPE_USER_REMOVED_FROM_GROUP => Yii::t('event_manager', '_EVENT_CLASS_UserGroupRemove'),
			self::NTYPE_NEW_USER_CREATED => Yii::t('event_manager', 'User has been created (by administrator)'),
			self::NTYPE_NEW_USER_CREATED_SELFREG => Yii::t('notification', 'User has been created (confirmed registration)'),
			self::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION => Yii::t('event_manager', '_EVENT_CLASS_UserNewModerated'),
			self::NTYPE_USER_MODIFIED => Yii::t('event_manager', '_EVENT_CLASS_UserMod'),
			self::NTYPE_NEW_COURSE => Yii::t('standard', '_NEW_COURSE'),
			self::NTYPE_NEW_LO_ADDED_TO_COURSE => Yii::t('notification', 'New learning object added to course'),
			self::NTYPE_COURSE_DELETED => Yii::t('notification', 'Course deleted'),

			self::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN => Yii::t('notification', 'Student has completed a Learning Plan'),
			self::NTYPE_USER_UNENROLLED_FROM_LEARNINGPLAN => Yii::t('notification', 'User was unenrolled from a Learning Plan'),
			self::NTYPE_USER_ENROLLED_IN_LEARNINGPLAN => Yii::t('notification', 'User was enrolled in a Learning Plan'),
			self::NTYPE_LEARNINGPLAN_NEW_CONTENT => Yii::t('notification', 'New content in Learning Plan'),
			self::NTYPE_LEARNINGPLAN_CREATED => Yii::t('notification', 'New Learning Plan created'),
			self::NTYPE_LEARNINGPLAN_EXPIRING_ENROLLMENT => Yii::t('notification', 'Learning Plan enrollment expiring'),

			self::NTYPE_COURSE_PLAY_HAS_EXPIRED => Yii::t('notification', 'Course has expired'),

			self::NTYPE_ILT_SESSION_CREATED => Yii::t('notification', 'New ILT session created'),
			self::NTYPE_ILT_SESSION_CHANGED => Yii::t('notification', 'ILT session changed'),
			self::NTYPE_ILT_SESSION_DELETED => Yii::t('notification', 'ILT session deleted'),
			self::NTYPE_ILT_SESSION_STARTING => Yii::t('notification', 'ILT session starting'),
			self::NTYPE_WEBINAR_SESSION_STARTING => Yii::t('notification', 'Webinar session starting'),
			self::NTYPE_ILT_SESSION_USER_ENROLLED => Yii::t('notification', 'Users enrolled to ILT session'),
			self::NTYPE_ILT_SESSION_USER_ACTIVATED => Yii::t('notification', 'Users approved into ILT session'),
			self::NTYPE_ILT_SESSION_USER_DECLINED => Yii::t('notification', 'User declined from an ILT session'),
			self::NTYPE_ILT_SESSION_USER_WAITING_FOR_APPROVAL => Yii::t('notification', 'User waiting to be approved to an ILT session'),
			self::NTYPE_ILT_SESSION_USER_UNENROLLED => Yii::t('notification', 'Users unenrolled from ILT session'),
			self::NTYPE_ILT_SESSION_USER_PASSED => Yii::t('notification', 'User has passed ILT session'),
			self::NTYPE_ILT_SESSION_USER_FAILED => Yii::t('notification', 'User has failed ILT session'),

			self::NTYPE_NEWSLETTER_SENT => Yii::t('notification', 'Newsletter Sent'),

			self::NTYPE_CATCHUP_COURSE_AVAILABLE => Yii::t('notification', 'Catchup course available'),
			self::NTYPE_CATCHUP_LIMIT_CHANGED => Yii::t('notification', 'Catchup Limit Changed'),
			self::NTYPE_INSTRUCTOR_EVALUATED_ASSIGNMENT => Yii::t('notification', 'Assignment has been evaluated'),
			self::NTYPE_NEW_COURSE_UNLOCKED_IN_LEARNING_PLAN => Yii::t('notification', 'New course unlocked in learning plan'),
			self::NTYPE_ASSIGNMENT_WAITING_TO_BE_REVIEWED => Yii::t('notification', 'Assignment waiting to be reviewed since N hours'),
			self::NTYPE_LEARNER_SUBMITTED_ASSIGNMENT => Yii::t('notification', 'Learner submitted assignment'),
			self::NTYPE_WEBINAR_SESSION_USER_WAITING_FOR_APPROVAL => Yii::t('notification', 'User waiting to be approved to a Webinar session'),
			self::NTYPE_WEBINAR_SESSION_USER_ACTIVATED => Yii::t('notification', 'User approved into Webinar session'),
			self::NTYPE_WEBINAR_SESSION_USER_DECLINED => Yii::t('notification', 'User declined from a Webinar session'),
			self::NTYPE_WEBINAR_SESSION_CREATED => Yii::t('notification', 'Webinar session created'),
			self::NTYPE_WEBINAR_SESSION_CHANGED => Yii::t('notification', 'Webinar session changed'),
			self::NTYPE_WEBINAR_SESSION_DELETED => Yii::t('notification', 'Webinar session deleted'),
			self::NTYPE_WEBINAR_SESSION_USER_ENROLLED => Yii::t('notification', 'Users enrolled in Webinar session'),
			self::NTYPE_WEBINAR_SESSION_USER_UNENROLLED => Yii::t('notification', 'Users unenrolled from Webinar session'),
			self::NTYPE_COURSE_NOT_YET_COMPLETE => Yii::t('notification', 'Learner has yet to complete a course'),
			self::NTYPE_BLOG_COMMENT => Yii::t('notification', 'New comment in My Blog'),
			self::NTYPE_BLOG_POST_IN_COURSE => Yii::t('notification', 'New blog in course'),
			self::NTYPE_BLOG_COMMENT_IN_COURSE => Yii::t('notification', 'New comment in course blog'),
			self::NTYPE_USER_EMAIL_MUST_VERIFY => Yii::t('notification', 'User email must be verified'),
			self::NTYPE_ECOMMERCE_SETTINGS_CHANGED => Yii::t('notification', 'E-commerce settings were changed'),
			//digest notifications
			self::NTYPE_DIGEST_USER_ENROLL_TO_COURSE => Yii::t('notification', 'Digest: user enrolled to a course'),
			self::NTYPE_DIGEST_COURSE_HAS_EXPIRED => Yii::t('notification', 'Digest: course has expired'),
			self::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE => Yii::t('notification', 'Digest: learner has yet to complete the course'),
		);

		if (PluginManager::isPluginActive('Share7020App')) {
			$result = array_merge($result, array(
				self::NTYPE_APP7020_INVITATION => Yii::t('app7020', 'New To Watch invitation'),
				self::NTYPE_APP7020_NEW_CONTENT => Yii::t('app7020', 'New content uploaded'),
				self::NTYPE_APP7020_NEW_PEER_REVIEW => Yii::t('app7020', 'New peer review'),
				self::NTYPE_APP7020_PUBLISHING => Yii::t('app7020', 'New content published'),
				self::NTYPE_APP7020_NEW_TOOLTIP => Yii::t('app7020', 'New tooltip added'),
				self::NTYPE_APP7020_DELETE_ASSET => Yii::t('app7020', 'Asset deleted by contributor'),
				self::NTYPE_APP7020_DELETE_ASSET_BY_ADMIN => Yii::t('app7020', 'Asset deleted by Super Admin'),
				self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => Yii::t('app7020', 'New Asset To Review'),
				)
			);
		}

		if (PluginManager::isPluginActive('Share7020App')) {
			$result = array_merge($result, array(
				self::NTYPE_APP7020_NEW_QUESTION => Yii::t('app7020', 'New Question to Expert'),
				self::NTYPE_APP7020_NEW_CONTENTS_QUESTION => Yii::t('app7020', "New Content's question to Expert"),
				self::NTYPE_APP7020_NEW_ANSWER => Yii::t('app7020', 'New answer for a question'),
				self::NTYPE_APP7020_BEST_ANSWER => Yii::t('app7020', 'Answer marked as Best Answer'),
//					self::NTYPE_APP7020_TOPIC_ASSIGNMENT		=> Yii::t('app7020', 'New topic assignement'),
				self::NTYPE_APP7020_CHANNEL_ASSIGNMENT => Yii::t('app7020', 'New channel assignment'),
				self::NTYPE_APP7020_MENTIONED_ANSWER => Yii::t('app7020', 'Mentioned in Answer'),
				self::NTYPE_APP7020_NEW_ASSET_TO_REVIEW => Yii::t('app7020', 'New Asset To Review'),
				)
			);
		}

		if (PluginManager::isPluginActive('CoachingApp')) {
			$result = array_merge($result, array(
				self::NTYPE_NEW_COACHING_SESSION => Yii::t('notification', 'New coaching session created'),
				self::NTYPE_UPDATED_COACHING_SESSION => Yii::t('notification', 'Coaching session has been updated'),
				self::NTYPE_DELETED_COACHING_SESSION => Yii::t('notification', 'Coaching session has been deleted'),
				self::NTYPE_FIRST_COACHING_SESSION_REQUESTED => Yii::t('notification', 'Learner asked to have coaching session assigned in a course'),
				self::NTYPE_OTHER_COACHING_SESSION_REQUESTED => Yii::t('notification', 'Learner requested another coaching session in a course'),
				self::NTYPE_COACHING_SESSION_EXPIRED => Yii::t('notification', 'Coaching session has expired'),
				self::NTYPE_MERGED_COACHING_SESSION => Yii::t('notification', 'Coaching session has been merged'),
				self::NTYPE_COACHING_SESSION_MESSAGE_BY_LEARNER => Yii::t('notification', 'New message posted by learner in coach chat'),
				self::NTYPE_COACHING_WEBINAR_SESSION_CREATED => Yii::t('notification', 'New live webinar session by coach'),
				self::NTYPE_COACHING_WEBINAR_SESSION_STARTS => Yii::t('notification', 'Live coach session starts'),
				self::NTYPE_COACHING_COACH_MESSAGED_LEARNER => Yii::t('notification', 'Coach sent a message to a learner'),
			));
		}

		if (PluginManager::isPluginActive('GamificationApp')) {
			$result = array_merge($result, array(
				self::NTYPE_GAMIFICATION_CONTEST_START => Yii::t('gamification', 'New Gamification contest started'),
				self::NTYPE_GAMIFICATION_CONTEST_END => Yii::t('gamification', 'Gamification contest finished')
			));
		}

		// Subscription
		if (PluginManager::isPluginActive('SubscriptionsApp')) {
			$result = array_merge($result, array(
				self::NTYPE_SUB_PLAN_PURCHASED => Yii::t('notification', 'Subscription plan has been purchased'),
				self::NTYPE_SUB_PLAN_EXPIRED => Yii::t('notification', 'Subscription plan has expired'),
				self::NTYPE_SUB_BUNDLE_CREATED => Yii::t('notification', 'Subscription bundle has been created'),
				self::NTYPE_SUB_RECORD_EXCEEDED_CAPACITY => Yii::t('notification', 'Subscription record has exceeded capacity'),
				self::NTYPE_SUB_RECORD_CAPACITY_ALMOST_EXHAUSTED => Yii::t('notification', 'Subscription record has almost exhausted capacity'),
				self::NTYPE_SUB_BUNDLE_EDITED => Yii::t('notification', 'Subscription bundle has been changed'),
			));
		}

		return $result;
	}


	/**
	 * Return translated name of the Notification type, based on Model type OR another type id, passed as an argument.
	 *
	 * @param number $type
	 * @return string
	 */
	public static function typeName($type = false) {
		$nds = self::getNotificationsDescriptors();

		foreach ($nds as $notType => $notification) {
			if ($type == $notType)
				return $notification['title'];
		}
		return '';

// 		$names = self::types();
// 		return isset($names[$type]) ?  $names[$type] : '';
	}


	/**
	 * Return notification subject in arbitrary language.
	 * By default returns in current App language.
	 *
	 * @param int|CoreNotificationTransport the ID/AR or the requested transport
	 * @param string $language the browser code of the requested language
	 * @return string
	 */
	public function subject($transportId, $language = false, $fallbackToDefault = true) {
		return $this->lang('subject', $transportId, $language, $fallbackToDefault);
	}


	/**
	 * Return notification message in arbitrary language
	 * By default returns in current App language.
	 *
	 * @param int|CoreNotificationTransport the ID/AR or the requested transport
	 * @param string $language the browser code of the requested language
	 * @return string
	 */
	public function message($transportId, $language = false, $fallbackToDefault = true) {
		return $this->lang('message', $transportId, $language, $fallbackToDefault);
	}



	/**
	 * Returns translated attribute (taken from the *translation* model CoreNotificationTranslation)
	 *
	 * @param string $attribute the name of the requested attribute ('subject' or 'message')
	 * @param int|CoreNotificationTransport the ID/AR or the requested transport
	 * @param string $language  (en, it, bg, etc.)
	 * @param boolean $fallbackToDefault
	 */
	public function lang($attribute, $transportId, $language = null, $fallbackToDefault = true) {
		$criteria = new CDbCriteria();
		$langModel = null;

		//add some flexibility to arguments
		if (is_object($transportId) && $transportId instanceof CoreNotificationTransport) {
			$transportId = $transportId->id;
		}

		if ($language) {
			$criteria->addCondition("notification_id = :notification_id");
			$criteria->params[':notification_id'] = $this->id;
			$criteria->addCondition("transport_id = :transport_id");
			$criteria->params[':transport_id'] = $transportId;
			$criteria->addCondition("language = :language");
			$criteria->params[':language'] = $language;

			$langModel = CoreNotificationTranslation::model()->find($criteria);
		}

		if (!$langModel && $fallbackToDefault) {

			// Language not provided or a translation in this language is not present, so
			// use the platform default language
			$platformLang = Lang::getBrowserCodeByCode(Settings::get('default_language'));
			$language = $platformLang ? $platformLang : Yii::app()->getLanguage();

			$criteria = new CDbCriteria();
			$criteria->addCondition("notification_id = :notification_id");
			$criteria->params[':notification_id'] = $this->id;
			$criteria->addCondition("transport_id = :transport_id");
			$criteria->params[':transport_id'] = $transportId;
			$criteria->addCondition("language = :language");
			$criteria->params[':language'] = $language;

			$langModel = CoreNotificationTranslation::model()->find($criteria);
			if (!$langModel)
				return '';
		}

		if (!$langModel || !isset($langModel->$attribute)) {
			return '';
		}

		// In case this attribute is encoded by HTMLPurifier, decode back well known characters ("[" and "]" for example)
		$attr = $langModel->$attribute;
		if (is_string($langModel->$attribute)) {
			$attr = Docebo::decodeSquareBrackets($attr);
		}

		return $attr;
	}


	/**
	 * Get array of recipients type (ALL POSSIBLE)
	 *
	 * @return array
	 */
	public static function recipients() {

		$result = array(
			self::NOTIFY_RECIPIENTS_USER => array(
				'name' => Yii::t('standard', '_USERS'),
				'descr' => '' // Users description
			),
			self::NOTIFY_RECIPIENTS_TEACHER => array(
				'name' => Yii::t('standard', 'Teacher(s)'),
				'descr' => '' // Instructors description
			),
			self::NOTIFY_RECIPIENTS_POWER_USER => array(
				'name' => Yii::t('standard', 'Power users'),
				'descr' => '' // Power users description'
			),
			self::NOTIFY_RECIPIENTS_GODADMIN => array(
				'name' => Yii::t('dashboard', '_SUPERADMIN_USER'),
				'descr' => '' // Super-admins description
			),
			self::NOTIFY_RECIPIENTS_MANAGER => array(
				'name' => Yii::t('dashboard', 'Manager'),
				'descr' => '' // Manager description
			),
		);

		return $result;

	}


	/**
	 * Return translated list of all default recipient type, incl. NAME and DESCRIPTION
	 */
	public static function defaultAllRecipients() {
		$recipAll = array();
		foreach (self::$RECIP_ALL as $recipient) {
			$recipAll[$recipient]['name'] = self::recipientName($recipient);
			$recipAll[$recipient]['descr'] = self::recipientDescr($recipient);
		}
		return $recipAll;
	}

	/**
	 * Returns Recipient type name, translated
	 *
	 * @param string $id
	 * @return string
	 */
	public static function recipientName($id) {
		$array = self::recipients();
		return isset($array[$id]) ? $array[$id]['name'] : '';
	}

	/**
	 * Returns Recipient Description, translated  ('user' => "Users")
	 *
	 * @param string $id
	 * @return string
	 */
	public static function recipientDescr($id) {
		$array = self::recipients();
		return isset($array[$id]) ? $array[$id]['descr'] : '';
	}

	/**
	 * Check if THIS notification (or by $id) has ANY user filtering
	 *
	 * @param integer $id
	 * @return boolean
	 */
	public function hasUserFilter() {
		return $this->ufilter_option == self::USER_DO_USER_FILTER;
	}


	/**
	 * Get filtering info for THIS notification (or by ID)
	 *
	 * @param string $id
	 * @return array of data
	 */
	public function getUserFilterInfo($id = false) {

		$id = $id ? $id : $this->id;

		$users = array();
		$groups = array();
		$branches = array();
		$branchesFlatList = array();

		$command = Yii::app()->db->createCommand()
			->select('*')
			->from(CoreNotificationUserFilter::model()->tableName() . ' t')
			->where('id=:id');

		$params = array(
			':id' => (int) $id,
		);
		$reader = $command->query($params);

		foreach ($reader as $item) {
			if ($item['type'] == CoreNotificationUserFilter::NOTIF_FILTER_USER) {
				$users[] = $item['idItem'];
			} else if ($item['type'] == CoreNotificationUserFilter::NOTIF_FILTER_GROUP) {
				$groups[] = $item['idItem'];
			} else if ($item['type'] == CoreNotificationUserFilter::NOTIF_FILTER_BRANCH) {
				if ($item['selectionState'] == CoreOrgChartTree::SELECT_THIS) {
					$branchesFlatList[] = $item['idItem'];
				} else {
					$orgNode = CoreOrgChartTree::model()->findByPk($item['idItem']);
					$branchesFlatList = array_merge($branchesFlatList, $orgNode->getBranchGroupsPdo(true, 'idOrg'));
				}
				$branches[] = array('key' => $item['idItem'], 'selectState' => $item['selectionState']);
			}
		}

		$result = array(
			'groups' => $groups,
			'branches' => $branches,
			'branchesFlatList' => array_unique($branchesFlatList),
			'users' => $users
		);

		return $result;

	}


	/**
	 * Update core_notification_user_filter table
	 *
	 * @param array $params
	 * @throws Exception
	 * @return boolean
	 */
	public static function updateUserFilter($params) {

		// Params structure
		$id = $params['id'];   // Notification ID
		$filterOption = $params['filterOption']; // The filter option: do filter, do NOT filter
		$users = $params['users'];
		$groups = $params['groups'];
		$branches = $params['branches'];


		$model = CoreNotification::model()->findByPk($id);
		if (!$model) {
			throw new Exception('Invalid notification');
		}

		// First, clean up current user filtering
		CoreNotificationUserFilter::model()->deleteAllByAttributes(array('id' => $id));

		// If DO filter requested, save incoming groups and branches to user filtering table
		if ($filterOption == CoreNotification::USER_DO_USER_FILTER) {

			foreach ($users as $idUser) {
				$tmpFilter = new CoreNotificationUserFilter();
				$tmpFilter->id = $id;
				$tmpFilter->idItem = $idUser;
				$tmpFilter->type = CoreNotificationUserFilter::NOTIF_FILTER_USER;
				$tmpFilter->save();
			}

			foreach ($groups as $idGroup) {
				$tmpFilter = new CoreNotificationUserFilter();
				$tmpFilter->id = $id;
				$tmpFilter->idItem = $idGroup;
				$tmpFilter->type = CoreNotificationUserFilter::NOTIF_FILTER_GROUP;
				$tmpFilter->save();
			}

			foreach ($branches as $branch) {
				$tmpFilter = new CoreNotificationUserFilter();
				$tmpFilter->id = $id;
				$tmpFilter->idItem = $branch['key'];
				$tmpFilter->type = CoreNotificationUserFilter::NOTIF_FILTER_BRANCH;
				$tmpFilter->selectionState = $branch['selectState'];
				$tmpFilter->save();
			}

		}

		return true;


	}



	/**
	 * Check if THIS notification (or by $id) has ANY associated courses or learning plans
	 *
	 * @param integer $id
	 * @return boolean
	 */
	public function hasAssoc($id = false) {
		return $this->cfilter_option == self::COURSES_DO_ASSOC;
	}


	/**
	 * Get Courses Association Info for THIS notification (or by ID)
	 *
	 * @param string $id
	 * @param bool $filterByType If the notification is only for classroom/webinar courses, return only classroom/webinar courses
	 * @param bool $type The type of the notification
	 * @return array of data
	 */
	public function getAssocInfo($id = false, $filterByType = false, $type = false) {

		$id = $id ? $id : $this->id;
		$type = $type ? $type : $this->type;

		$courses = array();
		$plans = array();
		$plansCourses = array();

		$command = Yii::app()->db->createCommand()
			->select('*')
			->from(CoreNotificationAssoc::model()->tableName() . ' t')
			->where('id=:id');

		$params = array(
			':id' => (int) $id,
		);

		$coursesCommand = clone $command;
		$plansCommand = clone $command;

		// Courses
		$coursesCommand->andWhere('type = "' . CoreNotificationAssoc::NOTIF_ASSOC_COURSE . '"');

		if ($filterByType) {
			$coursesCommand->join(LearningCourse::model()->tableName() . ' c', 'c.idCourse = t.idItem');
			if (in_array($type, CoreNotification::$ILT_NOTIFICATIONS))
				$coursesCommand->andWhere('c.course_type = "' . LearningCourse::CLASSROOM . '"');
			if (in_array($type, CoreNotification::$WEBINAR_NOTIFICATIONS))
				$coursesCommand->andWhere('c.course_type = "' . LearningCourse::TYPE_WEBINAR . '"');
		}
		$reader = $coursesCommand->query($params);

		$effectiveCoursesCount = 0;
		foreach ($reader as $item) {
			$courses[] = $item['idItem'];
			$effectiveCoursesCount++;
		}

		// Learning plans
		$plansCommand->andWhere('type = "' . CoreNotificationAssoc::NOTIF_ASSOC_PLAN . '"');

		if ($filterByType) {
			$courseType = false;
			if (in_array($type, CoreNotification::$ILT_NOTIFICATIONS))
				$courseType = LearningCourse::TYPE_CLASSROOM;
			if (in_array($type, CoreNotification::$WEBINAR_NOTIFICATIONS))
				$courseType = LearningCourse::TYPE_WEBINAR;

			if ($courseType == LearningCourse::TYPE_CLASSROOM || $courseType == LearningCourse::TYPE_WEBINAR) {
				$plansCommand->join('(SELECT cpc.id_path FROM learning_coursepath_courses cpc JOIN learning_course c ON c.idCourse = cpc.id_item AND c.course_type = :course_type GROUP BY cpc.id_path) coursepaths', 'coursepaths.id_path = t.idItem', array(':course_type' => $courseType)
				);
			}
		}
		$reader = $plansCommand->query($params);
		foreach ($reader as $item) {
			$plans[] = $item['idItem']; // count plans
			$planModel = LearningCoursepath::model()->findByPk($item['idItem']);
			if ($planModel) {
				$thisPlanCourses = $planModel->getCoursesIdList();
				$plansCourses = array_merge($plansCourses, $thisPlanCourses);
			}
		}

		$result = array(
			'courses' => $courses,
			'plans' => $plans,
			'plansCourses' => $plansCourses,
		);

		return $result;
	}

	/**
	 * Update core_notification_assoc table
	 *
	 * @param array $params
	 * @throws Exception
	 * @return boolean
	 */
	public static function updateCourseAssoc($params) {

		// Params structure
		$id = $params['id'];   // Notification ID
		$filterOption = $params['filterOption']; // The course filter option: do filter (associate courses), do NOT filter (no asscoiated courses)
		$courses = $params['courses'];
		$plans = $params['plans'];

		$model = CoreNotification::model()->findByPk($id);
		if (!$model) {
			throw new Exception('Invalid notification');
		}

		// First, clean up
		CoreNotificationAssoc::model()->deleteAllByAttributes(array('id' => $id));

		if ($filterOption == CoreNotification::COURSES_DO_ASSOC) {
			foreach ($courses as $idCourse) {
				$tmpFilter = new CoreNotificationAssoc();
				$tmpFilter->id = $id;
				$tmpFilter->idItem = $idCourse;
				$tmpFilter->type = CoreNotificationAssoc::NOTIF_ASSOC_COURSE;
				$tmpFilter->save();
			}

			foreach ($plans as $idPlan) {
				$tmp = new CoreNotificationAssoc();
				$tmp->id = $id;
				$tmp->idItem = $idPlan;
				$tmp->type = CoreNotificationAssoc::NOTIF_ASSOC_PLAN;
				$tmp->save();
			}
		}

		return true;


	}



	/**
	 * Get a list of ALL notifications descriptors (every descriptor describes ONE notification TYPE)
	 * This is essential method, providing information about ALL possible notification in the LMS, including plugins, if any
	 *
	 * @return array array of <type> => array(<attribute> => <value>)
	 */
	public static function getNotificationsDescriptors() {
		/* @var $nd NotificationDescriptor */

		// Do some Web-Request-wide caching
		static $cache;
		if (!empty($cache)) {
			return $cache;
		}

		$coreNotifications = array();
		$customNotifications = array();

		$allowedScheduleTypes = self::scheduleTypesPerType();   // at, after, before
		$allowedRecipientTypes = self::recipientTypesPerType(); // user, instructor, ...
		$allowedProperiesPerType = self::propsPerType(); // user filtering, course assoc., ...
		$allShortcodes = self::shortcodes(); // [cdfdf], [dfdf]
		$templatesPerType = self::templatesPerType();

		foreach (self::types() as $type => $title) {
			$n['title'] = $title;
			$n['description'] = '';
			$n['template'] = isset($templatesPerType[$type]) ? $templatesPerType[$type] : null;

			// AT, Before, After,....
			$n['allowedScheduleTypes'] = isset($allowedScheduleTypes[$type]) ? $allowedScheduleTypes[$type] : self::$SCHEDULE_PRESET_ALL;

			// Recipient types and their translated names and descriptions (user, power user, super admin,...)
			$n['allowedRecipientTypes'] = isset($allowedRecipientTypes[$type]) ? $allowedRecipientTypes[$type] : self::defaultAllRecipients();

			// Allowed "properties", e.g. is there User Filter? Course association?
			$n['allowedProperties'] = isset($allowedProperiesPerType[$type]) ? $allowedProperiesPerType[$type] : self::$PROP_ALL;

			// List of shortcodes, Array of  (<short-code> => <short-code>), !yes, for historical reason it is key-CODE => value-CODE!
			$n['shortcodes'] = isset($allShortcodes[$type]) ? $allShortcodes[$type] : array();

			// Assigned Job Handler
			$n['jobHandler'] = self::NOTIFICATION_JOB_HANDLER_ID; // core notification job handler

			$coreNotifications[$type] = $n;
		}


		// Add CUSTOM notifications (from plugins or just from other parts of the LMS)
		$customDescriptors = self::collectCustomNotifications($coreNotifications);
		if (!empty($customDescriptors) && is_array($customDescriptors)) {
			foreach ($customDescriptors as $nd) {

				// Shortcode and recipients must be "transofmed/translated"
				$shortCodesTmp = array();
				foreach ($nd->shortcodes as $sc) {
					$shortCodesTmp[$sc] = $sc;
				}
				$recipTmp = array();
				foreach ($nd->allowedRecipientTypes as $recipient) {
					$recipTmp[$recipient]['name'] = self::recipientName($recipient);
					$recipTmp[$recipient]['descr'] = self::recipientDescr($recipient);
				}

				$customNotifications[$nd->type] = array(
					'title' => $nd->title,
					'description' => $nd->description,
					'template' => $nd->template,
					'group' => $nd->group,
					'allowedScheduleTypes' => $nd->allowedScheduleTypes,
					'allowedRecipientTypes' => $recipTmp,
					'allowedProperties' => $nd->allowedProperties,
					'shortcodes' => $shortCodesTmp,
					'jobHandler' => $nd->jobHandler,
				);
			}
		}

		$result = array_merge($customNotifications, $coreNotifications);

		// Sort notifications alphabetically
		uasort($result, function($a, $b) {
			return strcmp($a['title'], $b['title']);
		});

		// Cache it for this request
		$cache = $result;

		return $result;

	}


	/**
	 * Return ONE particular notification type descriptor
	 *
	 * @param string $type
	 * @return array|boolean
	 */
	public static function getNotificationInfo($type) {
		$nds = self::getNotificationsDescriptors();
		if (isset($nds[$type]))
			return $nds[$type];
		return false;
	}


	/**
	 * Return translated list of schedule type labels ('At the time of event..", "After... " , "Before..")
	 *
	 * @return array
	 */
	public static function scheduleTypeLabels() {

		$result = array(
			self::SCHEDULE_AT 		=> Yii::t('notification','At the time of the event'),
			self::SCHEDULE_BEFORE 	=> Yii::t('notification','Before the event_capital'),
			self::SCHEDULE_AFTER 	=> Yii::t('notification','After the event_capital'),
			self::SCHEDULE_EVERY 	=> Yii::t('notification','Every'),
		);

		return $result;

	}


	/**
	 * Return ONE translated schedule type label
	 *
	 * @param string $type
	 */
	public static function scheduleTypeLabel($type) {
		$labels = self::scheduleTypeLabels();
		if (isset($labels[$type]))
			return $labels[$type];
		return "unknown schedule type";
	}




	/**
	 * Return list of possible schedule shifting periods (hour, day, week, ...)
	 *
	 * @param array $type
	 */
	public static function scheduleShiftPeriods() {
		$result = array(
			self::SCHED_SHIFT_PERIOD_HOUR => Yii::t('standard', '_HOURS'),
			self::SCHED_SHIFT_PERIOD_DAY => Yii::t('standard', 'Days'),
			self::SCHED_SHIFT_PERIOD_WEEK => Yii::t('standard', 'Weeks')
		);
		return $result;
	}


	/**
	 * Return list of possible schedule shifting periods (hour, day, week, ...) for reccuring event
	 *
	 * @param array $type
	 */
	public static function scheduleShiftPeriodsEvery() {
		$result = array(
			self::SCHED_SHIFT_PERIOD_HOUR 	=> Yii::t('standard', 'Hour'),
			self::SCHED_SHIFT_PERIOD_DAY 	=> Yii::t('standard', 'Day'),
			self::SCHED_SHIFT_PERIOD_WEEK 	=> Yii::t('standard', 'Week'),
			self::SCHED_SHIFT_PERIOD_MONTH 	=> Yii::t('standard', 'Month'),
		);
		return $result;
	}

	/**
	 * Return list of possible week days (Monday, Tuesday, ...) for reccuring event
	 *
	 * @param array $type
	 */
	public static function scheduleShiftPeriodsDay() {
		$result = array(
			self::SCHED_SHIFT_PERIOD_MONDAY 	=> Yii::t('standard', 'Monday'),
			self::SCHED_SHIFT_PERIOD_TUESDAY 	=> Yii::t('standard', 'Tuesday'),
			self::SCHED_SHIFT_PERIOD_WEDNESDAY 	=> Yii::t('standard', 'Wednesday'),
			self::SCHED_SHIFT_PERIOD_THURSDAY 	=> Yii::t('standard', 'Thursday'),
			self::SCHED_SHIFT_PERIOD_FRIDAY 	=> Yii::t('standard', 'Friday'),
			self::SCHED_SHIFT_PERIOD_SATURDAY 	=> Yii::t('standard', 'Saturday'),
			self::SCHED_SHIFT_PERIOD_SUNDAY 	=> Yii::t('standard', 'Sunday'),
		);
		return $result;
	}

	/**
	 * Return translated schedule description in human readable format.
	 * Sort of complex... For your convenience, here are required Yii translations:
	 *
	 * notification, 'after'
	 * notification, 'before'
	 * notification, '{number} {periods} {before_after} the event {time}'
	 * notification, 'hour(s)',
	 * notification, 'day(s)',
	 * notification, 'weeks(s)',
	 *
	 * @return string
	 */
	public function getScheduleText() {
		$result = '';
		switch ($this->schedule_type) {
			case self::SCHEDULE_AT:
				$result = CoreNotification::scheduleTypeLabel(CoreNotification::SCHEDULE_AT);
				break;
			case self::SCHEDULE_AFTER:
			case self::SCHEDULE_BEFORE:
				$this->convertTime(new DateTimeZone('UTC'), new DateTimeZone(Yii::app()->localtime->getTimeZone()));
				$time = " - " . $this->schedule_time . ":00";
				switch ($this->schedule_shift_period) {
					case self::SCHED_SHIFT_PERIOD_HOUR:
						$periods = Yii::t('standard', 'hour(s)');
						$time = "";
						break;
					case self::SCHED_SHIFT_PERIOD_DAY:
						$periods = Yii::t('standard', 'day(s)');
						break;
					case self::SCHED_SHIFT_PERIOD_WEEK:
						$periods = Yii::t('standard', 'week(s)');
						break;

					default:
						return '';
				}
				$result = $this->schedule_shift_number
					. " " . $periods
					. " " . self::scheduleTypeLabel($this->schedule_type)
					. " " . $time;
            break;
            case self::SCHEDULE_EVERY:
                switch ($this->schedule_shift_period) {
                    case self::SCHED_SHIFT_PERIOD_HOUR:
                        $periods = Yii::t('standard', 'hour(s)');
                        $time = "";
                        $result = Yii::t('standard', 'Hourly');
                        break;
                    case self::SCHED_SHIFT_PERIOD_DAY:
                        $result = Yii::t('standard', 'Daily') . ' at ' . $this->schedule_time .':00';
                        break;
                    case self::SCHED_SHIFT_PERIOD_WEEK:
                        $days = self::scheduleShiftPeriodsDay();
                        $result = Yii::t('standard', 'Weekly') . ' ' . Yii::t('standard',
                                'on') . ' ' . $days[$this->schedule_shift_number] . ' '
                            . Yii::t('standard', 'at') . ' ' . $this->schedule_time . ':00';
                        break;
                    case self::SCHED_SHIFT_PERIOD_MONTH:
                        $result = Yii::t('standard', 'Monthly') . ' ' . Yii::t('standard',
                                'on') . ' ' . $this->schedule_shift_number . ' ' . Yii::t('standard',
                                'at') . ' ' . $this->schedule_time . ':00';
                        break;

                    default:
                        return '';
                }

		}
		return $result;
	}


	/**
	 * @see CActiveRecord::beforeDelete()
	 */
	public function beforeDelete() {

		// Delete any assigned job
		if ($this->scheduledJob) {
			Yii::app()->scheduler->deleteJob($this->scheduledJob, true);
		}

		return parent::beforeDelete();
	}


	public function beforeSave() {
		// make sure schedule type to be OK with the TYPE of the notification
		$this->adjustScheduleFromType();

		return parent::beforeSave();
	}


	/**
	 * Check if current model scheduling is compliant with the model's notification type. If not, force to the first allowed by type.
	 *
	 */
	public function adjustScheduleFromType() {
		$notInfo = self::getNotificationInfo($this->type);

		if (!$notInfo)
			return;

		if (!in_array($this->schedule_type, $notInfo['allowedScheduleTypes'])) {
			$this->schedule_type = $notInfo['allowedScheduleTypes'][0];
		}
	}

	/**
	 * Check if Notification's auhtor is a power user (at the moment)
	 *
	 * @return boolean
	 */
	public function isPowerUserNotification() {
		return CoreUser::isPowerUser($this->id_author);
	}


	/**
	 * Return resulting, effective list of users from notification user filter. Unique.
	 *
	 * @return array
	 */
	public function getEffectiveUsersFromUserFilter() {

		$info = $this->getUserFilterInfo();
		$users = array();

		// From Groups
		foreach ($info['groups'] as $idGroup) {
			$coreGroupModel = CoreGroup::model()->findByPk($idGroup);
			if ($coreGroupModel)
				$users = array_merge($users, $coreGroupModel->getUsers());
		}

		// From branches
		$branches = $info['branchesFlatList'];
		foreach ($branches as $idOrg) {
			$coreOrgChartTreeModel = CoreOrgChartTree::model()->findByPk($idOrg);
			if ($coreOrgChartTreeModel)
				$users = array_merge($users, $coreOrgChartTreeModel->getNodeUsers());
		}

		// And finally from directly selected users
		$users = array_merge($users, $info['users']);

		return array_unique($users);

	}



	/**
	 * Return resulting, effective list of associated courses
	 *
	 * @return array
	 */
	public function getEffectiveCoursesFromAssoc() {
		$info = $this->getAssocInfo();
		$courses = array_merge($info['courses'], $info['plansCourses']);
		return array_unique($courses);
	}


	/**
	 * Check if notification is translated in a given language
	 *
	 * @param string $language "en", "bg", "it", ...
	 * @return boolean
	 */
	public function isTranslatedInLanguage($language) {
		$found = CoreNotificationTranslation::model()->findByAttributes(array('notification_id' => $this->id, 'language' => $language));
		return (!empty($found));
	}


	/**
	 * Get the the first translation model available
	 *
	 * @return CoreNotificationTranslation
	 */
	public function getFirstAvailTranslation() {
		return CoreNotificationTranslation::model()->findByAttributes(array('notification_id' => $this->id));
	}


	/**
	 * Resolve language to use, based on available translations,
	 * i.e. return a proper (translated) language if there is no translation for a given one (th input parameter)
	 *
	 * @param string $languageCode  (e.g. "english")
	 * @return string
	 */
	public function resolveUsableLanguage($languageCode) {

		$newLanguageCode = $languageCode;

		if (!$this->isTranslatedInLanguage(Lang::getBrowserCodeByCode($languageCode))) {

			// Try with Default language
			$defaultLanguage = Settings::get('default_language', false) ? Settings::get('default_language') : "english";
			if ($this->isTranslatedInLanguage(Lang::getBrowserCodeByCode($defaultLanguage))) {
				$newLanguageCode = $defaultLanguage;
			}
			// If not translated in default language
			else {
				// Check if English translation is available, as the most popular one
				if ($this->isTranslatedInLanguage('en') && false) {
					$newLanguageCode = 'english';
				} else {
					// Still not translated... Ok, get the first translation and use that language
					$first = $this->getFirstAvailTranslation();
					if ($first) {
						$newLanguageCode = Lang::getCodeByBrowserCode($first->language);
					} else {
						// THIS IS VERY STRANGE.. NO translations at all ???
						// 	Ok, we set language to EMPTY value, can't do much about this weird situation...
						$newLanguageCode = 'empty';
					}
				}
			}
		}

		return $newLanguageCode;
	}

	public static function autocomplete($id, $query) {
		/**
		 * Query the items
		 */
		$sql = "(
			SELECT t.id, c.idCourse as id_item, c.name, c.course_type, '" . CoreAdminCourse::TYPE_COURSE . "' AS item_type, CONCAT(id, '_', idItem, '_', type) as pk
			FROM core_notification_assoc t
			LEFT JOIN learning_course c ON (c.idCourse = t.idItem)
			WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_COURSE . " AND t.id = " . $id . " AND c.name LIKE '%" . $query . "%'
		)";
		if (PluginManager::isPluginActive('CurriculaApp')) {
			$sql .= " UNION (
				SELECT t.id, c.id_path as id_item, c.path_name as name, '' AS course_type, '" . CoreAdminCourse::TYPE_COURSEPATH . "' AS item_type, CONCAT(id, '_', idItem, '_', type) as pk
				FROM core_notification_assoc t
				LEFT JOIN learning_coursepath c ON (c.id_path = t.idItem)
				WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_PLAN . " AND t.id = " . $id . " AND c.path_name LIKE '%" . $query . "%'
			)";
		}

		$rows = Yii::app()->db->createCommand($sql)->queryAll();

		return CHtml::listData($rows, 'pk', 'name');
	}

	/**
	 * Build SQL Data Data Provider for Courses-Users custom report.
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProviderAssocCourses() {

		$sqlCoursesFilter = '';

		$showElearning = (!$this->filterType || in_array('elearning', $this->filterType));
		$showClassroom = (!$this->filterType || in_array('classroom', $this->filterType));
		$showLearningPlan = (!$this->filterType || in_array('learning-plan', $this->filterType));

		if (!empty($this->filterType)) {
			$courseTypes = array();

			if ($showElearning)
				$courseTypes[] = "'elearning'";
			if ($showClassroom)
				$courseTypes[] = "'classroom'";

			$sqlCoursesFilter = " AND c.course_type IN (" . implode(',', $courseTypes) . ")";
		}

		/**
		 * Query the total number of rows
		 */
		$countItems = 0;
		$sqlCourses = $sqlLearningPlan = false;

		if ($showElearning || $showClassroom) {
			$sqlCourses = "
				SELECT COUNT(*) AS num_items
				FROM core_notification_assoc t
				LEFT JOIN learning_course c ON (c.idCourse = t.idItem)
				WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_COURSE . " AND t.id = " . $this->id . ($this->search ? " AND c.name LIKE '%" . $this->search . "%'" : '') . $sqlCoursesFilter;
		}
		if ($showLearningPlan && PluginManager::isPluginActive('CurriculaApp')) {
			$sqlLearningPlan = "
				SELECT COUNT(*) AS num_items
				FROM core_notification_assoc t
				LEFT JOIN learning_coursepath c ON (c.id_path = t.idItem)
				WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_PLAN . " AND t.id = " . $this->id . ($this->search ? " AND c.path_name LIKE '%" . $this->search . "%'" : '');
		}

		$sql = ($sqlCourses && $sqlLearningPlan) ? '(' . $sqlCourses . ') UNION (' . $sqlLearningPlan . ')' : ($sqlCourses ? $sqlCourses : $sqlLearningPlan);

		$countList = Yii::app()->db->createCommand($sql)->queryAll(true);
		if (!empty($countList)) {
			foreach ($countList as $countItem) {
				$countItems += $countItem['num_items'];
			}
		}

		$sqlCourses = $sqlLearningPlan = false;
		/**
		 * Query the items
		 */
		if ($showElearning || $showClassroom) {
			$sqlCourses = "
			SELECT t.id, c.idCourse as id_item, c.name, c.course_type, '" . CoreAdminCourse::TYPE_COURSE . "' AS item_type, CONCAT(id, '_', idItem, '_', type) as pk
			FROM core_notification_assoc t
			LEFT JOIN learning_course c ON (c.idCourse = t.idItem)
			WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_COURSE . " AND t.id = " . $this->id . ($this->search ? " AND c.name LIKE '%" . $this->search . "%'" : '') . $sqlCoursesFilter;
		}
		if ($showLearningPlan && PluginManager::isPluginActive('CurriculaApp')) {
			$sqlLearningPlan = "
				SELECT t.id, c.id_path as id_item, c.path_name as name, '' AS course_type, '" . CoreAdminCourse::TYPE_COURSEPATH . "' AS item_type, CONCAT(id, '_', idItem, '_', type) as pk
				FROM core_notification_assoc t
				LEFT JOIN learning_coursepath c ON (c.id_path = t.idItem)
				WHERE t.type = " . CoreNotificationAssoc::NOTIF_ASSOC_PLAN . " AND t.id = " . $this->id . ($this->search ? " AND c.path_name LIKE '%" . $this->search . "%'" : '');
		}

		$sql = ($sqlCourses && $sqlLearningPlan) ? '(' . $sqlCourses . ') UNION (' . $sqlLearningPlan . ')' : ($sqlCourses ? $sqlCourses : $sqlLearningPlan);

		// Data provider config
		$config = array(
			'totalItemCount' => $countItems,
			'keyField' => 'pk',
			'pagination' => array('pageSize' => Settings::get('elements_per_page', 10)),
		);

		return new CSqlDataProvider($sql, $config);
	}



	/**
	 * Convert schedule_time
	 * @param DateTimeZone $fromTZ
	 * @param DateTimeZone $toTZ
	 */
	public function convertTime(DateTimeZone $fromTZ, DateTimeZone $toTZ) {
		$dt = new DateTime('NOW', $fromTZ);
		$dt->setTime($this->schedule_time, 0);
		$dt->setTimezone($toTZ);
		$this->schedule_time = $dt->format('G');
	}

	/**
	 * Raise an event to collect additional/custom notifications descriptiors
	 *
	 * @param array $coreNotifications
	 */
	protected static function collectCustomNotifications($coreNotifications) {
		if (!PluginManager::isPluginActive('NotificationApp'))
			return array();

		/* @var $nd NotificationDescriptor */

		$notifications = array();
		$event = new DEvent(self, array(
			'notifications' => &$notifications,
		));

		Yii::app()->event->raise('CollectCustomNotifications', $event);

		$takenTypes = array_keys($coreNotifications);
		$result = array();
		if (is_array($notifications)) {
			foreach ($notifications as $nd) {
				if (get_class($nd) === 'NotificationDescriptor') {
					// Overriding notification handlers is forbidden
					if (in_array($nd->type, $takenTypes)) {
						Yii::log('Notitifcation overriding REJECTED: ' . $nd->type . ' is already taken! Requested by: ' . $nd->requester, CLogger::LEVEL_ERROR);
					}
					// Validate incoming notification(s) and log errors, if any. Otherwise, just add the object to valid list of notifications
					else {
						if (!$nd->validate() && $nd->hasErrors()) {
							foreach ($nd->errors() as $error) {
								Yii::log($error, CLogger::LEVEL_ERROR);
							}
						} else {
							$result[] = $nd;
							$takenTypes[] = $nd->type;
						}
					}
				}
			}
		}

		return $result;
	}

	public static function isActiveNotificationOfType($type) {
		$count = Yii::app()->db->createCommand()
				->select('COUNT(id)')
				->from(CoreNotification::model()->tableName())
				->where('type = :type', array(':type' => $type))
				->andWhere('active = 1')->queryScalar();
		if ($count != 0)
			return true;
		return false;
	}


}
