<?php

/**
 * This is the model class for table "learning_course_file".
 *
 * The followings are the available columns in table 'learning_course_file':
 * @property integer $id_file
 * @property integer $id_course
 * @property string $title
 * @property string $path
 * @property integer $position
 * @property string $created
 * @property string $original_filename
 * @property integer $idUser
 * @property integer $lev
 * @property integer $iLeft
 * @property integer $iRight
 * @property integer $idParent
 * @property integer $item_type
 * @property string $viewable_by
 * @property integer $file_source
 * @property string $settings
 * @property integer $file_permission
 *
 */
class LearningCourseFile extends CActiveRecord
{
	CONST FILE = 5;
	CONST FOLDER = 10;

	const VIEWABLE_IN_ANY_SESSION = '*';
	const VIEWABLE_IN_SELECTED_SESSIONS_ONLY = 'selected_sessions';

	const FILE_PERMISSION_SELECTIBLE	= 1;
	const FILE_PERMISSION_LEARNER		= 2;
	const FILE_PERMISSION_COACH			= 4;
	const FILE_PERMISSION_TUTOR			= 8;
	const FILE_PERMISSION_INSTRUCTOR	= 16;

	public static $LEVELS = [self::FILE_PERMISSION_LEARNER, self::FILE_PERMISSION_COACH, self::FILE_PERMISSION_TUTOR, self::FILE_PERMISSION_INSTRUCTOR];

	public $folder;
	public $file_link;

	// FILE SOURCES
	CONST FILE_SOURCE_UPLOAD = 1;
	CONST FILE_SOURCE_LINK = 2;
	CONST FILE_SOURCE_CONTENT_RAVEN = 3;

	public $types = array(
		self::FILE => 'file',
		self::FOLDER => 'folder'
	);

	public $file_sources = array(
		self::FILE_SOURCE_UPLOAD,
		self::FILE_SOURCE_LINK,
		self::FILE_SOURCE_CONTENT_RAVEN,
	);

	// Content Raven Attributes
	public $content_id;
	public $permission_id;

	public $permissionType;
	public $permissionLevels;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourseFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content_id, permission_id', 'required', 'on' => 'content_raven_link'),
			array('file_link', 'required', 'on' => 'file_link'),
			array('file_link', 'url', 'defaultScheme' => 'http'),
			array('id_course, title', 'required'),
			array('id_course, position, idUser', 'numerical', 'integerOnly'=>true),
			array('title, path, original_filename, file_link, viewable_by', 'length', 'max'=>255),
			array('created, lev, iLeft, iRight, idParent, item_type, file_source, settings, permissionType, permissionLevels, file_permission', 'safe'),
			array('viewable_by', 'validateCustomSessionsVisibility'),
			array('permissionType', 'validateEnrollmentPermission'),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_file, id_course, title, path, position, created, original_filename, idUser, viewable_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * Does all password validations
	 * @return boolean
	 */
	public function validateCustomSessionsVisibility($attrName){
		if($this->viewable_by == self::VIEWABLE_IN_SELECTED_SESSIONS_ONLY){
			if(empty($_POST['selectedItemsString'])){
				$this->addError('viewable_by', Yii::t('classroom','Select session'));
			}
		}
	}

	/**
	 * Permission validation
	 * @return boolean
	 */
	public function validateEnrollmentPermission(){
		if($this->permissionType == self::FILE_PERMISSION_SELECTIBLE){
			if(empty($this->permissionLevels)){
				$this->addError('permissionLevels', Yii::t('player','Select enrollment levels'));
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('created medium'),
			 'dateAttributes' => array()
            ),
			'nestedSetBehavior'=>array(
				'class' => 'common.components.DoceboNestedSetBehavior',
				'leftAttribute' => 'iLeft',
				'rightAttribute' => 'iRight',
				'levelAttribute' => 'lev',
				'idParentAttribute' => 'idParent',
				'subTreeAttribute' => 'id_course',
			),
        );
    }

	/**
	 * @return array customized permission types labels (name=>label)
	 */
	public static function permissionTypesLabels()
	{
		return array(
			0 => Yii::t('player', 'All'),
			1 => Yii::t('player', 'Select enrollment levels')
		);
	}

	/**
	 * @return array customized permission labels (name=>label)
	 */
	public static function permissionLabels()
	{
		$levels =  array(
			self::FILE_PERMISSION_LEARNER 	=> Yii::t('levels', '_LEVEL_3'),
			self::FILE_PERMISSION_TUTOR 	=> Yii::t('levels', '_LEVEL_4'),
			self::FILE_PERMISSION_INSTRUCTOR => Yii::t('levels', '_LEVEL_6'),
		);

		if (PluginManager::isPluginActive('CoachingApp'))
			$levels[self::FILE_PERMISSION_COACH] = Yii::t('levels', '_LEVEL_8');

		ksort($levels, SORT_NUMERIC);
		return $levels;
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_file' => 'Id File',
			'id_course' => 'Id Course',
			'title' => 'Title',
			'path' => 'Path',
			'position' => 'Position',
			'created' => 'Created',
			'original_filename' => 'Original Filename',
			'idUser' => 'Id User',
			'viewable_by'=>'Viewable by',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_file',$this->id_file);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('original_filename',$this->original_filename,true);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('lev',$this->lev);
		$criteria->compare('iLeft',$this->iLeft);
		$criteria->compare('iRight',$this->iRight);
		$criteria->compare('idParent',$this->idParent);
		$criteria->compare('item_type',$this->item_type);
		$criteria->compare('viewable_by',$this->viewable_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function getEnrollmentFilePermission($userLevel){
		switch($userLevel) {
			case LearningCourseuser::$USER_SUBSCR_LEVEL_COACH:
				return self::FILE_PERMISSION_COACH;
			case LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR:
				return self::FILE_PERMISSION_TUTOR;
			case LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT:
				return self::FILE_PERMISSION_LEARNER;
			case LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR:
				return self::FILE_PERMISSION_INSTRUCTOR;
			default:
				return 0;
		}
	}

	/**
	 * @param $idCourse
	 * @param $idUser
	 * @param $root
	 * @return CActiveDataProvider
	 */
	public function getCoursedocsListDataProvider($idCourse, $idUser, $root)
	{
		$cu = LearningCourseuser::model()->findByAttributes(array(
			'idCourse'=>$idCourse,
			'idUser'=>$idUser,
		));

		$criteria = new CDbCriteria();

		if($cu && !Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsPu()){
			switch($cu->level){
				case LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT:
				case LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR:
					// Only see files that are visible in "All sessions"
					// or files visible to the sessions the user/instructor is enrolled in
					$courseModel = LearningCourse::model()->findByPk($idCourse);

					$table = false;
					switch($courseModel->course_type){
						case LearningCourse::TYPE_CLASSROOM:
							$table = LtCourseSession::model()->tableName();
							break;
						case LearningCourse::TYPE_WEBINAR:
							$table = WebinarSession::model()->tableName();
							break;
					}
					$params = array(':type' => $courseModel->course_type);
					$fileVisibilityQuery =  Yii::app()->getDb()->createCommand()
						->select('t.*')
						->from(LearningCourseFileVisibility::model()->tableName() . ' t')
						->where('t.session_type = :type');
					if($table !== false) {
						$fileVisibilityQuery->join($table.' lcs', 'lcs.id_session = t.id_session');
						$fileVisibilityQuery->andWhere('lcs.course_id = :idCourse');
						$params[':idCourse'] = $idCourse;
					}

					$filesVisibleToSessionsInCourse = $fileVisibilityQuery->queryAll(true, $params);
					// If there is any files with visibility set for this course then we need to check for which sessions are they
					if(!empty($filesVisibleToSessionsInCourse)) {
						$currentSession = Yii::app()->request->getParam('session_id', false);
						$filesToShow = array();
						if($currentSession !== false){
							foreach($filesVisibleToSessionsInCourse as $k => $file){
								if ($file['id_session'] == $currentSession) {
									$filesToShow[] = $file['id_file'];
								}
							}
						} else {
							// user is instructor, he can see only files from assigned to him sessions
							switch ($table) {
								case LtCourseSession::model()->tableName():
									$joinTable = LtCourseuserSession::model()->tableName();
									break;
								case WebinarSession::model()->tableName():
									$joinTable = WebinarSessionUser::model()->tableName();
									break;
							}
							$allowedSessions = Yii::app()->db->createCommand()
									->select('t.id_session')
									->from($table.' t')
									->join($joinTable . ' lcu', 'lcu.id_session=t.id_session AND id_user=:user', array(':user' => $idUser))->queryColumn();
							$sessionList = array_keys($courseModel->getSessions());
							$allowedSessions = array_values(array_intersect($allowedSessions, $sessionList));
							foreach ($filesVisibleToSessionsInCourse as $k => $file) {
								if(in_array($file['id_session'], $allowedSessions))
									$filesToShow[] = $file['id_file'];
							}
						}

						// If there is no files to show then hide all the files from the user
						if($filesToShow === array()){
							// Hide all files for the user
							$criteria->addCondition('viewable_by=:all OR id_file = 0');
						}else{
							$criteria->addCondition('viewable_by=:all OR id_file IN (' . implode(',', $filesToShow) . ')');
						}

						$criteria->params[':all'] = self::VIEWABLE_IN_ANY_SESSION;
					}

					break;
			}

			$criteria->addCondition('file_permission = 0 OR file_permission & :level != 0');
			$criteria->params[':level'] = $this->getEnrollmentFilePermission($cu->level);
		}

		$criteria->addCondition('id_course = :id_course');
		$criteria->addCondition('path<>"root"');
		$criteria->addCondition('lev=:lev');
		$criteria->params[':id_course'] = $idCourse;
		$criteria->params[':lev'] = 2;// by default the level is 2 because we need only items in the root
		$criteria->order = 'position ASC';

		if($root){
			$criteria->addCondition('idParent=:root');
			$criteria->params[':root'] = $root->id_file;
			$criteria->params[':lev'] = $root->lev+1;
		}

		$provider = new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 1000
			)
		));

		return $provider;
	}


	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterDelete()
	 */
	public function afterDelete() {
		parent::afterDelete();

		// Delete the document from the storage
		if ($this->path) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_COURSEDOCS);
			$storage->remove($this->path);
		}
	}

	public function getUsername()
	{
		// Check if the record has idUser, the old records will not have such
		if($this->idUser)
		{
			$username = '';
			// Get user first, last name
			$queryParams = Yii::app()->db->createCommand('SELECT userid, firstname,lastname FROM core_user where idst = :user')->queryRow(true ,array(':user'=>$this->idUser));

			if($queryParams['firstname'] && $queryParams['lastname']){
				$username = $queryParams['firstname'] . ' ' . $queryParams['lastname'];
			} elseif ($queryParams['firstname']) {
				$username = $queryParams['firstname'];
			}else{
				$username = str_replace('/', '', $queryParams['userid']);
			}

			return $username;
		}
		else
		{
			return ' ';
		}
	}

    public function afterSave() {
        if($this->isNewRecord) {
            //it is a new Learning Course File created
            Yii::app()->event->raise('CourseFileAdded', new DEvent($this, array(
                'courseFile' => $this
            )));
        }

        parent::afterSave();
    }

	public function getContentRavenUrl(){
		$settings = (array) CJSON::decode($this->settings, 1);
		$url = PluginSettings::get('content_raven_url', "ContentRavenApp");
		$token = PluginSettings::get('token', "ContentRavenApp");
		$sender = PluginSettings::get('sender_id', "ContentRavenApp");
		$mail = Yii::app()->user->getEmail() ? Yii::app()->user->getEmail() : Yii::app()->user->getUsername();
		$params = array(
			'tokenid=' . $token,
			'senderid=' . $sender,
			'contentid=' . (isset($settings['content_id']) ? $settings['content_id'] : ""),
			'permissionid=' . (isset($settings['permission_id']) ? $settings['permission_id'] : ""),
			'useremail=' . $mail
		);
		$stringParams = implode('&', $params);
		return implode("?", array($url, $stringParams));
	}

	public static function checkPermission($permissions, $permission){
		return  $permissions & $permission != 0;
	}

	public static function setPermissions($permissions, $permissionType){
		$permission = 0;
		if ($permissions) {
			foreach ($permissions as $selPermission) {
				$permission = $permission | $selPermission;
			}
		}
		return $permission | $permissionType;
	}

	public static function getSelectedPermission($permissions){
		$selectedLevels = array();
		foreach (self::$LEVELS as $level) {
			if (intval($permissions & $level) != 0) {
				array_push($selectedLevels, $level);
			}
		}
		return $selectedLevels;
	}
}