<?php

class FieldDropdown extends CoreUserField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

    /**
     * Retrieving options list for the dropdown instance
     *
     * @param string $language = null
     * @param bool $withFallbackTranslation = false - whether or not to return translations for the default language if there aren't ones for the specified language
     * @return array
     */
	public function getOptions($language = null, $withFallbackTranslation = false)
    {
		$defaultLanguage = Settings::get('default_language', false);
        $rawOptions = $this->getOptionsCommandBase($language, $defaultLanguage)->queryAll();

        $rawOptions = array_map(function($row) {
            if (empty($row['translation'])) {
                $row['translation'] = $row['fallbackTranslation'];
            }

            if (isset($row['fallbackTranslation'])) {
                unset($row['fallbackTranslation']);
            }
            return $row;
        }, $rawOptions);

        $options = [];
        foreach ($rawOptions as $optDetails) {
            $options[$optDetails['id_option']] = $optDetails['translation'];
        }

		return $options;
	}

	public function render() {
		$options = array('' => '--') + $this->getOptions();

		$html = CHtml::dropDownlist('CoreUser[additional]['.$this->getFieldId().'][value]', $this->userEntry, $options, array('class' => 'additional-dropdown'));
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][id_common_son]', $this->userEntry, array('class' => 'additional-dropdown-hidden'));
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldType());

		return $html;
	}

	public function renderFilterField() {
		$html = CHtml::label($this->getTranslation(), 'advancedSearch[additional]['.$this->getFieldId().']');
		$html .= CHtml::dropDownlist('advancedSearch[additional]['.$this->getFieldId().'][value]', '', $this->getOptions(), array('disabled' => 'disabled'));

		return $html;
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public function renderFieldValue($userEntry, $userEntryModel=null)
    {
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        $command = $this->getOptionsCommandBase($lang, null, $userEntry);
        $labelValue = $command->queryRow();
        return $labelValue ? $labelValue['translation'] : '';
	}

    /**
     * Returns a command for fetching dropdown's options by passed criteria
     *
     * @param string $langCode - fetching option labels for the specified language
     * @param string $fallbackLangCode - fallback language for option labels
     * @param string $selectedOption
     * @return CDbCommand
     */
	public function getOptionsCommandBase($langCode = null, $fallbackLangCode = null, $selectedOption = null)
    {
        $joinFallbackLang = $fallbackLangCode !== null;
        $select = ['cufd.id_option', 'cufd.id_field', 'cufd.sequence'];

        $command = Yii::app()->getDb()->createCommand()
            ->from(CoreUserFieldDropdown::model()->tableName() . ' cufd')
            ->leftJoin(CoreUserFieldDropdownTranslations::model()->tableName() . ' cufdt', 'cufd.id_option = cufdt.id_option')
            ->where('cufd.id_field = :fieldId', array('fieldId' => $this->getFieldId()))
        ;

        if ($selectedOption) {
            $command->andWhere('cufd.id_option = :optionValue', array('optionValue' => $selectedOption));
        }

        if ($langCode) {
            $command->andWhere('cufdt.lang_code = :langCode', array('langCode' => $langCode));
            array_push($select, 'cufdt.translation AS translation');
        }

        if ($joinFallbackLang === true) {
            // Fallback option labels
            // Workaround for an issue with joining two tables with different aliases - the selected columns get overriden
            $command->leftJoin(CoreUserFieldDropdownTranslations::model()->tableName() . ' cufdtf', 'cufd.id_option = cufdtf.id_option AND cufdtf.lang_code = "' . $fallbackLangCode . '"');
            // $command->andWhere('cufdtf.lang_code = :langCode', array('langCode' => $fallbackLangCode));
            array_push($select, 'cufdtf.translation AS fallbackTranslation');
        }

        $select = implode(', ', $select);
        $command->select($select);

        return $command;
    }

}
