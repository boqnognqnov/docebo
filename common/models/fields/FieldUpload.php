<?php

class FieldUpload extends CoreUserField {



	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * Render "file upload" additional field HTML (used in forms)
	 *
	 * @return string
	 */
	public function render($forAjax = false) {
		$additionalFieldModel = new AdditionalFieldFile();
		$html = "";
		$html .= "<div class='fieldupload-edituser pull-right'>" . $additionalFieldModel->getPluploaderHtml("FileUpload[" . $this->getFieldId() . "]", $this->getFieldId());
		$html .= $this->renderFieldValue($this->userEntry);
		$html .= '</div>';
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldType());

		return $html;
	}

	public function renderFilterField() {
		return $this->render();
	}

	public function renderFieldValue($userEntry, $userEntryModel=null) {
        $originalFilename = $userEntry;

		if (!empty($userEntry)) {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CORE_FIELD);

			$url = Docebo::createAbsoluteLmsUrl('stream/file', array(
				'fn' 	=> urlencode($userEntry),
				'fndl' 	=> urlencode($originalFilename),
				'col' 	=> urlencode(CFileStorage::COLLECTION_CORE_FIELD),
			));

			$html =
				CHtml::link
				(
					'<span class="i-sprite is-take"></span>&nbsp;' . $originalFilename,
					$url,
					array
					(
						'target' => '_blank',
						'class' => 'uploaded-file',
						'style' => 'display: '
					)
				);

		} else {
			$html = "";
		}

		return $html;
	}



	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}



}