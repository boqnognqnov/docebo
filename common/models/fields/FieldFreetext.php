<?php

class FieldFreetext extends CoreUserField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render() {
		$html = CHtml::textArea('CoreUser[additional]['.$this->getFieldId().'][value]', $this->userEntry);
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldType());

		return $html;
	}

	public function renderFilterField() {
		$html = CHtml::label($this->getTranslation(), 'advancedSearch[additional]['.$this->getFieldId().']');
		$html .= CHtml::dropDownlist('advancedSearch[additional]['.$this->getFieldId().'][condition]', '', $this->getConditionsOptions(), array('disabled' => 'disabled'));
		$html .= CHtml::textfield('advancedSearch[additional]['.$this->getFieldId().'][value]', '', array('disabled' => 'disabled'));

		return $html;
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

}