<?php

class FieldDate extends CoreUserField {

	//date width to be used
	protected $_dateWidth = LocalTime::DEFAULT_DATE_WIDTH;//LocalTime::MEDIUM;

	public function getDateWidth() {
		//check date width for input fields
		if (!Yii::app()->localtime->forceLongYears()) {
			//trying to prevent short year format to be used in input fields
			switch ($this->_dateWidth) {
				case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
				default: $dateWidth = LocalTime::MEDIUM; break;
			}
		} else {
			//long years are forced, all date widths are safe
			$dateWidth = $this->_dateWidth;
		}
		return $dateWidth;
	}

	public function setDateWidth($dateWidth) {
		switch ($dateWidth) {
			case LocalTime::SHORT:
			case LocalTime::MEDIUM:
			case LocalTime::LONG: $this->_dateWidth = $dateWidth; break;
			default: $this->_dateWidth = LocalTime::DEFAULT_DATE_WIDTH; break;
		}
	}

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render() {

		$dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat($this->getDateWidth()));

		//since user entries can come from different sources, the date format is not always guaranteed
		$value = ''; //initialize the formatted date value
		if (!empty($this->userEntry)) {
			$detectedEntryFormat = Yii::app()->localtime->detectFormat($this->userEntry);
			switch ($detectedEntryFormat) {
				case LocalTime::FORMAT_UTC_DATE:
				case LocalTime::FORMAT_UTC_DATETIME:
					//standard ISO format, convert the provided date entry into local format
					$value = Yii::app()->localtime->toLocalDate($this->userEntry, $this->getDateWidth());
					break;
				default:
					//we are assuming that the provided date entry is already in local format
					$value = $this->userEntry;
					break;
			}
		}

		/*$value = (!empty($this->userEntry)
			? Yii::app()->localtime->toLocalDate($this->userEntry, $this->getDateWidth())
			: ''
		);*/

		$html = "<div class='input-append date' data-date-format='$dpFormat'>";
		$html .= CHtml::textfield('CoreUser[additional]['.$this->getFieldId().'][value]', $value,
					array(
						"data-date-format" => $dpFormat,
						"data-date-language" => Yii::app()->getLanguage(),
						"readonly" => "readonly"
					));
		$html .= '<span class="add-on"><i class="icon-calendar"></i></span></div>';
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', 'date');
		return $html;
	}

	public function renderFilterField() {
		$html = "<div class='input-append date' data-date-format='" . Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat($this->getDateWidth())) . "' data-date-language='" . Yii::app()->getLanguage() . "'>";
		$html .= CHtml::label($this->getTranslation(), 'advancedSearch[additional][' . $this->getFieldId() . ']');
		$html .= CHtml::dropDownlist('advancedSearch[additional][' . $this->getFieldId() . '][condition]', '',
			array('>' => '>', '<' => '<', '=' => '=', '>=' => '>=', '<=' => '<=', '<>' => '!='), array('disabled' => 'disabled'));
		$html .= CHtml::textfield('advancedSearch[additional][' . $this->getFieldId() . '][value]', '', array(
				'disabled' => 'disabled',
				'readonly' => 'readonly',
				'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat($this->getDateWidth())),
				'data-date-language' => Yii::app()->getLanguage()
			));
		$html .= '<span class="add-on"><i class="icon-calendar"></i></span></div>';

		return $html;
	}



	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}



	public function renderFieldValue($userEntry, $userEntryModel=null) {
		if (!empty($userEntry)) {
			//the date is rendered in local date format
			//TODO: may it be useful adding a check on entry validity ?
			$formattedDate = Yii::app()->localtime->toLocalDate($userEntry);
		}
		return parent::renderFieldValue($formattedDate, $userEntryModel);
	}

}