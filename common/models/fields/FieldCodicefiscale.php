<?php

class FieldCodicefiscale extends CoreUserField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render()
    {
		$html = CHtml::textfield('CoreUser[additional]['.$this->getFieldId().'][value]', $this->userEntry);
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldId());

		return $html;
	}

	public function renderFilterField()
    {
		$html = CHtml::label($this->getTranslation(), 'advancedSearch[additional]['.$this->getFieldId().']');
		$html .= CHtml::textfield('advancedSearch[additional]['.$this->getFieldId().'][value]', '', array('disabled' => 'disabled'));

		return $html;
	}

	public function validateFieldEntry()
    {
		parent::validateFieldEntry();

		if (!preg_match('/^(\d|[a-zA-z])+$/', $this->userEntry) && $this->userEntry != '') {
			$this->addError('userEntry', 'Fiscal code should be alphanumeric');
		}
	}

}