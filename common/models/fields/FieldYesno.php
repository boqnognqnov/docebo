<?php

class FieldYesno extends CoreUserField {

	const VALUE_NOTASSIGNED = '0'; //or NULL, or "" or no record in DB
	const VALUE_YES = '1';
	const VALUE_NO = '2';

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}

	public function render() {
		$html = CHtml::radioButtonList('CoreUser[additional][' . $this->getFieldId() . '][value]', $this->userEntry, array(
			// radios: value=>label
			self::VALUE_YES => Yii::t('standard', '_YES'),
			self::VALUE_NO => Yii::t('standard', '_NO')
		), array(
			'class' => 'yes-no-field',
			'separator' => '',
			'container' => 'span class="additional-yes-no"'
		));
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional][' . $this->getFieldId() . ']');
		$html .= CHtml::hiddenField('CoreUser[additional][' . $this->getFieldId() . '][type_field]', $this->getFieldType());

		return $html;
	}

	public function renderFilterField() {
		$html = CHtml::label($this->getTranslation(), 'advancedSearch[additional][' . $this->getFieldId() . ']');
		$html .= CHtml::radioButtonList(
			'advancedSearch[additional][' . $this->getFieldId() . '][value]',
			$this->userEntry,
			array(self::VALUE_YES => Yii::t('standard', '_YES'), self::VALUE_NO => Yii::t('standard', '_NO')),
			array('class' => 'yes-no-field', 'separator' => '', 'container' => 'span class="additional-yes-no"', 'disabled' => 'disabled'));

		return $html;
	}

	public function renderFieldValue($userEntry, $userEntryModel=null) {
		return self::getLabelFromValue($userEntry);
	}

	static public function getLabelFromValue($userEntry){
		switch ($userEntry) {
			case self::VALUE_YES:
				return Yii::t('standard', '_YES');
				break;
			case self::VALUE_NO:
				return Yii::t('standard', '_NO');
				break;
			default:
				return Yii::t('standard', 'Not set');
				break;
		}
	}

}