<?php

class FieldTextfield extends CoreUserField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function render() {
		$htmlOptions = array();
		$userEntry = $this->userEntry;
		Yii::app()->event->raise('RenderAdditionalField_Textfield', new DEvent($this, array(
			'field' => $this,
			'htmlOptions' => &$htmlOptions,
			'userEntry' => &$userEntry
		)));
		$html = CHtml::textfield('CoreUser[additional]['.$this->getFieldId().'][value]', $userEntry, $htmlOptions);
		$html .= CHtml::label(CHtml::encode($this->getTranslation()) . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldType());
		return $html;
	}

	public function renderFilterField() {
		$html = CHtml::label(CHtml::encode($this->getTranslation()), 'advancedSearch[additional]['.$this->getFieldId().']');
		$html .= CHtml::dropDownlist('advancedSearch[additional]['.$this->getFieldId().'][condition]', '', $this->getConditionsOptions(), array('disabled' => 'disabled'));
		$html .= CHtml::textfield('advancedSearch[additional]['.$this->getFieldId().'][value]', '', array('disabled' => 'disabled'));

		return $html;
	}

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}


}