<?php

class FieldCountry extends CoreUserField {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function getCountriesList() {
	    static $cache;
	    if ($cache) 
	        return $cache;
		$countries = CoreCountry::model()->findAll(array('order' => 'name_country'));
		$cache = CHtml::listData($countries, 'id_country', 'name_country');
		return $cache;
	}

	public function render() {
		$html = CHtml::dropDownlist('CoreUser[additional]['.$this->getFieldId().'][value]', $this->userEntry, $this->getCountriesList(), array('prompt' => '--'));
		$html .= CHtml::label($this->getTranslation() . ($this->mandatory ? self::MANDATORY_SYMBOL_HTML : ''), 'CoreUser[additional]['.$this->getFieldId().']');
		$html .= CHtml::hiddenField('CoreUser[additional]['.$this->getFieldId().'][type_field]', $this->getFieldType());

		return $html;
	}

	public function renderFilterField() {
		$html = CHtml::label($this->getTranslation(), 'advancedSearch[additional]['.$this->getFieldId().']');
		$html .= CHtml::dropDownlist('advancedSearch[additional]['.$this->getFieldId().'][value]', '', $this->getCountriesList(), array('disabled' => 'disabled'));

		return $html;
	}

	/**
     * Renders the value by provided field user value
     *
	 * @see CoreUserField::renderFieldValue()
     * @param null $userEntryModel
     * @param integer $userEntry - the ID of the country, specified as user entry for the field
     * @return string
	 */
	public function renderFieldValue($userEntry, $userEntryModel = null)
	{
	    $countries = $this->getCountriesList();
	    if (!isset($countries[$userEntry])) return '';
		return $countries[$userEntry];
	}

    /**
     * Renders the value by provided field and user IDs
     * Extracted from old logic of FieldCountry::renderFieldValue()
     *
     * @param string|integer $fieldId
     * @param string|integer $userId = null
     * @return string
     */
	public function renderFieldValueByUserAndField($fieldId, $userId = null)
    {
        if ($userId === null) {
            $userId = Yii::app()->user->idst;
        }
        
        if (!is_numeric($fieldId)) {
            $fieldId = str_replace('field_', '', $fieldId);
            $fieldId = str_replace('_value', '', $fieldId);
            $fieldId = (int) $fieldId;
        }

        $columnName = 'field_' . ((int) $fieldId);

        $coreUserFieldRow = Yii::app()->db->createCommand()
            ->select()
            ->from(CoreUserFieldValue::model()->tableName() . ' cufv')
            ->where('cufv.id_user = :userId', [':userId' => (int) $userId])
            ->queryRow();

        if (isset($coreUserFieldRow[$columnName]) === false) {
            // @todo Should throw an exception instead?
            return '';
        }

        return $this->renderFieldValue($coreUserFieldRow[$columnName]);
    }

	public function validateFieldEntry() {
		parent::validateFieldEntry();
	}


}