<?php

/**
 * This is the model class for table "learning_webpage_translation".
 *
 * The followings are the available columns in table 'learning_webpage_translation':
 * @property integer $id
 * @property integer $page_id
 * @property string $lang_code
 * @property string $title
 * @property string $description
 *
 */
class LearningWebpageTranslation extends CActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningWebpage the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'learning_webpage_translation';
	}

	public function beforeSave() {
		$this->title = strip_tags($this->title);
		$this->description =  preg_replace('#<script(.*?)>(.*?)</script>#is', '', html_entity_decode( $this->description)) ;

		return parent::beforeSave();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, lang_code', 'required'),
			array('id, page_id', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 255),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, page_id, lang_code', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		return array(
			'page' => array(self::BELONGS_TO, 'LearningWebpage', 'page_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'page_id' => 'Sequence',
			'lang_code' => 'Publish',
			'title' => 'In Home',
			'description' => 'In Home',
		);
	}

	public function language() {
		$language = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));

		$this->getDbCriteria()->mergeWith(array(
			'condition' => $this->getTableAlias() . '.lang_code = :language',
			'params' => array(':language' => $language->lang_code),
		));

		return $this;
	}

	public function dataProvider() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->with = array('page');
		$criteria->scopes = array('language');

		$criteria->compare('id', $this->id);
		$criteria->compare('page_id', $this->page_id);
		$criteria->compare('lang_code', $this->lang_code);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description);


		$criteria->order = 'page.sequence ASC';

		$config['criteria'] = $criteria;
		$config['pagination'] = false;

		return new CActiveDataProvider($this, $config);
	}
}