<?php

/**
 * This is the model class for table "learning_forumthread".
 *
 * The followings are the available columns in table 'learning_forumthread':
 * @property integer $idThread
 * @property integer $id_edition
 * @property integer $idForum
 * @property string $posted
 * @property string $title
 * @property integer $author
 * @property integer $num_post
 * @property integer $num_view
 * @property integer $last_post
 * @property integer $locked
 * @property integer $erased
 * @property string $emoticons
 * @property integer $rilevantForum
 *
 * The followings are the available model relations:
 * @property LearningForummessage[] $messages
 * @property LearningForum $forum
 * @property CoreUser $coreUser
 * */
class LearningForumthread extends CActiveRecord
{
	public $search_input; //used for search purposes for different columns

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningForumthread the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_forumthread';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_edition, idForum, author, num_post, num_view, last_post, locked, erased, rilevantForum', 'numerical', 'integerOnly'=>true),
			array('title, emoticons', 'length', 'max'=>255),
			array('posted, search_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idThread, id_edition, idForum, posted, title, author, num_post, num_view, last_post, locked, erased, emoticons, rilevantForum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'messages' => array(self::HAS_MANY, 'LearningForummessage', 'idThread', 'order' => 'posted ASC'),
			'forum' => array(self::BELONGS_TO, 'LearningForum', 'idForum'),
			'threadUser' => array(self::BELONGS_TO, 'CoreUser', 'author'),
			'msgCount'=>array(self::STAT, 'LearningForummessage', 'idThread'),
			'lastMessage' => array(self::HAS_ONE, 'LearningForummessage', 'idThread', 'order' => 'lastMessage.posted DESC'),
			'userTiming' => array(self::HAS_ONE, 'LearningForumTiming', 'idThread', 'on' => 'idUser = :userId', 'params' => array('userId' => Yii::app()->user->id)),
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('posted medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idThread' => 'Thread Id',
			'id_edition' => 'Edition Id',
			'posted' => 'Posted',
			'idForum' => Yii::t('standard', '_FORUM'),
			'title' => Yii::t('forum', '_NUMTHREAD'),
			'author' => Yii::t('standard', '_AUTHOR'),
			'num_post' => Yii::t('forum', '_NUMPOST'),
			'num_view' => Yii::t('standard', '_PLAY_CHANCE'),
			'last_post' => Yii::t('forum', '_LASTPOST'),
			'locked' => 'Locked',
			'erased' => 'Erased',
			'emoticons' => 'Emoticons',
			'rilevantForum' => 'Relevant Forum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('t.idThread',$this->idThread);
		$criteria->compare('t.id_edition',$this->id_edition);
		$criteria->compare('t.idForum',$this->idForum);
		$criteria->compare('t.posted',Yii::app()->localtime->fromLocalDateTime($this->posted),true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.author',$this->author);
		$criteria->compare('t.num_post',$this->num_post);
		$criteria->compare('t.num_view',$this->num_view);
		$criteria->compare('t.last_post',$this->last_post);
		$criteria->compare('t.locked',$this->locked);
		$criteria->compare('t.emoticons',$this->emoticons,true);
		$criteria->compare('t.rilevantForum',$this->rilevantForum);

		if ($this->search_input)
		{
			$criteria->addCondition('t.title like :search_input');
			$criteria->params[':search_input'] = '%'.$this->search_input.'%';
		}

		if (!isset($_GET['LearningForumthread_sort']))
			$criteria->order = 't.important DESC, t.idThread DESC';

		$criteria->with = array('msgCount', 'userTiming', 'threadUser');
		$criteria->together = true;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function getReplyNumber()
	{
		return $this->msgCount ? $this->msgCount - 1 : 0;
	}


	public function beforeSave() {
		$this->title = Yii::app()->htmlpurifier->purify($this->title);
		return parent::beforeSave();
	}

	public function afterSave(){
		if($this->getIsNewRecord()){
			// Raised when a new discussion is created inside a forum
			Yii::app()->event->raise('NewThread', new DEvent($this, array(
				'thread' => $this,
				'user'=> $this->threadUser, // AR relation
				'course' => $this->forum->course, // AR relation
			)));
		}

		parent::afterSave();
	}

	/**
	 * Remove all thread messages on delete
	 * @return bool
	 */
	public function beforeDelete()
	{
		LearningForummessage::model()->deleteAllByAttributes(array(
			'idThread' => $this->idThread,
		));
		return parent::beforeDelete();
	}


	/**
	 * Count how many threads a specified user has opened in the forums
	 * @param $userId idst of the user to be checked
	 * @return int the number of thread opened by the specified user
	 */
	public static function getCountOfTotalUserThreads($userId) {
		$count = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(self::model()->tableName())
			->where("author = :author", array(':author' => $userId))
			->queryScalar();
		return (int)$count;
	}
	
	
	/**
	 * Returns list of IDs of all users posted a message in THIS thread
	 * 
	 * @return array
	 */
	public function getPostersList() {
		
		$params = array();
		$select = array(
			'DISTINCT(m.author) AS author',
		);
		
		$command = Yii::app()->db->createCommand()->from('learning_forummessage m');
		
		$command->join('core_user user', 'm.author=user.idst');
		
		$command->where('m.idThread=:idThread');
		$params = array(
			':idThread' => $this->idThread	
		);
		
		$command->select(implode(',', $select));
		
		$reader = $command->query($params);
		foreach ($reader as $row) {
			$list[] = $row['author'];
		}
		
		
		return $list;
		
	}
	

}