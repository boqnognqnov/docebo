<?php

/**
 * Description of App7020PeerReviewStatistic
 *
 * @author vbelev
 */
class App7020PeerReviewStatistic extends CModel {

	/**
	 * Timeframe for query to get "Peer review Activity". The value is number of days ago from now
	 * @var array 
	 */
	const TIMEFRAME_DAILY = 1;
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;

	public function attributeNames() {
		return;
	}

	/**
	 * Statistic data for page "Peer review activity":
	 * 	1. "Reviewed assets"
	 * 	2. " Written peer reviews" 
	 * @param int $timeInterval
	 * @param int $userId
	 * @return array
	 */
	public static function reviewAssetStats($timeInterval, $userId) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("
				count(*) as peer_reviews, count(DISTINCT idContent) as reviewed_assets
			");
			$commandBase->from(App7020ContentReview::model()->tableName());
			$commandBase->andWhere("idUser = :idUser");
			$commandBase->andWhere('created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()');
			$resultArray = $commandBase->queryAll(true, array(':idUser' => $userId, ':timeInterval' => $timeInterval));
			$resultArray = array(
				'peer_reviews' => $resultArray[0]['peer_reviews'],
				'reviewed_assets' => $resultArray[0]['reviewed_assets'],
			);
			return $resultArray;
		} else {
			return array();
		}
	}

	/**
	 * Statistic data for page "Peer review activity":
	 * 	1. "Assets I have published"
	 * @param int $timeInterval
	 * @param int $userId
	 * @return int
	 */
	public static function assetsIpublished($timeInterval, $userId) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("count(*) as assets_i_published");
			$commandBase->from(App7020Assets::model()->tableName());
			$commandBase->andWhere("conversion_status = :conversionStatus");
			$commandBase->andWhere("userId = :userId");
			$commandBase->andWhere('created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()');
			return $commandBase->queryScalar(array(
						':userId' => $userId,
						':timeInterval' => $timeInterval,
						':conversionStatus' => App7020Assets::CONVERSION_STATUS_APPROVED
			));
		}
	}

	/**
	 * Get all "Peer review activity" data for that page
	 * @param int $timeInterval
	 * @param int $userId
	 * @param bool $json
	 * @return array/JSON
	 */
	public static function getAllPeerReviewActivity($timeInterval, $userId, $json = true) {
		$chartData = self::chartMyActivityPerChannel($timeInterval, $userId);
		$result = array_merge(
				self::reviewAssetStats($timeInterval, $userId), array('assetsIpublished' => self::assetsIpublished($timeInterval, $userId)), array('avgReviewTime' => self::avgReviewTime($timeInterval, $userId)), array('chartReviewedAssets' => self::chartReviewedAssets($timeInterval, $userId)), array('chartMyPeerActivityPerChannel' => self::chartMyActivityPerChannel($timeInterval, $userId)), array('topExpertsByPartecipationRate' => self::topExpertsByPartecipationRate()), array('topExpertsByFirstToReview' => self::topExpertsByFirstToReview()), array('chartMyPeerActivityPerChannelHTML' => Yii::app()->getController()->widget('common.widgets.app7020ActivityChannels', array('data' => $chartData), true))
		);
		if ($json) {
			json_encode($result);
		}
		return $result;
	}

	/**
	 * Avarage review time - for page "Peer review activity":
	 * @param int $timeInterval
	 * @param int $userId
	 * @return array - ['hours'=> 3 , 'minutes' => 38]
	 */
	public static function avgReviewTime($timeInterval, $userId) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			$commandBase->select("FLOOR(AVG(UNIX_TIMESTAMP(created_review) - UNIX_TIMESTAMP(content_published_date ))/60) as avg_review_time_minutes");
			$commandBase->from(App7020Assets::model()->tableName() . ' t1');
			$commandBase->join("
				(
					SELECT 
						idContent,
						MIN(created) as created_review  
					FROM " . App7020ContentReview::model()->tableName() . " 
					GROUP BY idContent
				) as t2", "t2.idContent = t1.id
			");
			$commandBase->join("
				(
					SELECT 
						idContent,
						MIN(datePublished) as content_published_date  
					FROM " . App7020ContentPublished::model()->tableName() . " 
					GROUP BY idContent
				) as t3", "t3.idContent = t1.id
			");
			$commandBase->andWhere("conversion_status >= :conversion_status");
			$commandBase->andWhere("userId = :userId");
			$commandBase->andWhere('t1.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()');
			$minutes = $commandBase->queryScalar(array(
				':conversion_status' => App7020Assets::CONVERSION_STATUS_FINISHED,
				':userId' => $userId,
				':timeInterval' => $timeInterval
			));
			return array(
				'hours' => floor($minutes / 60),
				'minutes' => ($minutes % 60)
			);
		} else {
			return array();
		}
	}

	/**
	 * Data for chart "Reviewed assets" for page "Peer review activity":
	 * @param int $timeInterval
	 * @param int $userId
	 * @return array
	 */
	public static function chartReviewedAssets($timeInterval, $userId) {
		if (is_int($timeInterval)) {
			$commandBase = Yii::app()->db->createCommand();
			// If query period is for 1 year ago to now
			if ($timeInterval == 365) {
				$commandBase->select("count(DISTINCT idContent) as reviewed_assets, DATE_FORMAT(created, '%Y-%m') AS period");
				$groupBy = "DATE_FORMAT(created, '%m')";
			} else {
				$commandBase->select("count(DISTINCT idContent) as reviewed_assets, DATE_FORMAT(created, '%c-%d') AS period");
				$groupBy = "DATE(created)";
			}
			$commandBase->from(App7020ContentReview::model()->tableName());
			$commandBase->andWhere("idUser = :idUser");
			$commandBase->andWhere('created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()');
			$commandBase->group($groupBy);
			$commandBase->order('created');
			$reviewedAssets = $commandBase->queryAll(true, array(
				':idUser' => $userId,
				':timeInterval' => $timeInterval
			));
			$resultArray = array(
				'labels' => array(),
				'points' => array()
			);


			if ($timeInterval == 365) {
				$months = array();
				for ($i = 0; $i <= 12; $i++) {
					$months[] = date('Y-m', strtotime(date('Y-m') . " -$i months"));
				}
				$months = array_reverse($months);
				$count = count($reviewedAssets);
				foreach ($months as $month) {
					if (count($reviewedAssets) > 0) {

						foreach ($reviewedAssets as $reviewedAsset) {
							if ($reviewedAsset['period'] == $month) {
								$resultArray['labels'][] = $reviewedAsset['period'];
								$resultArray['points'][] = $reviewedAsset['reviewed_assets'];
								$count--;
							} else {
								if (!in_array($month, $resultArray['labels']) && !in_array($reviewedAsset['period'], $resultArray['labels']) && $count > 0) {
									$resultArray['labels'][] = $month;
									$resultArray['points'][] = 0;
								} else if (!in_array($month, $resultArray['labels']) && $count == 0) {
									$resultArray['labels'][] = $month;
									$resultArray['points'][] = 0;
								}
							}
						}
					} else {
						$resultArray['labels'][] = $month;
						$resultArray['points'][] = 0;
					}
				}
			} else {
				$days = array();
				for ($i = 0; $i <= $timeInterval; $i++) {
					$days[] = date('n-d', strtotime("-$i day"));
				}
				$days = array_reverse($days);
				$count = count($reviewedAssets);
				foreach ($days as $day) {
					if (count($reviewedAssets) > 0) {
						foreach ($reviewedAssets as $reviewedAsset) {
							if ($reviewedAsset['period'] == "$day") {
								array_push($resultArray['labels'], $reviewedAsset['period']);
								array_push($resultArray['points'], $reviewedAsset['reviewed_assets']);
								$count--;
							} else {
								if (!in_array($day, $resultArray['labels']) && !in_array($reviewedAsset['period'], $resultArray['labels']) && $count > 0) {
									array_push($resultArray['labels'], $day);
									array_push($resultArray['points'], 0);
								} else if (!in_array($day, $resultArray['labels']) && $count == 0) {
									array_push($resultArray['labels'], $day);
									array_push($resultArray['points'], 0);
								}
							}
						}
					} else {
						array_push($resultArray['labels'], $day);
						array_push($resultArray['points'], 0);
					}
				}
			}

			return $resultArray;
		} else {
			return array();
		}
	}

	/**
	 * Data for Chart 2 - "My Activity per channel" - it should be sum of all my assets to given channel and calculate percenatge
	 * @param int $timeInterval
	 * @param int $userId
	 * @return array
	 */
	public static function chartMyActivityPerChannel($timeInterval, $userId) {
		if (!$timeInterval)
			$timeInterval = 30;
		if (is_int($timeInterval)) {
			// Default (platform) language ('en')
			$defaultLanguage = Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
			// Resolve requested language
			$language = Yii::app()->getLanguage();

			$sql = "
					SELECT
						trans.name AS channel_trans_name,
						transDef.name AS channel_transDef_name,
						app7020_channels.id AS channel_id,
						app7020_channels.icon_bgr AS icon_bgr,
						app7020_channels.icon_color AS icon_color,
						count(*) AS count_reviews
					FROM `app7020_channels`
					LEFT JOIN app7020_channel_translation AS trans ON trans.idChannel = app7020_channels.id AND trans.lang = :language
					LEFT JOIN app7020_channel_translation AS transDef ON transDef.idChannel = app7020_channels.id AND transDef.lang = :languageDef
					INNER JOIN app7020_channel_assets ON app7020_channel_assets.idChannel = app7020_channels.id
					INNER JOIN app7020_content ON app7020_content.id = app7020_channel_assets.idAsset
					INNER JOIN app7020_content_reviews AS content_reviews ON content_reviews.idContent = app7020_content.id
					WHERE 1 
					AND content_reviews.idUser = :currenUser
					AND content_reviews.created BETWEEN (NOW() - INTERVAL :timeInterval DAY) AND NOW()
					GROUP BY app7020_channels.id
					ORDER BY count_reviews DESC
				";

			$params = array(
				':language' => $language,
				':languageDef' => $defaultLanguage,
				':currenUser' => $userId,
				':timeInterval' => $timeInterval
			);
			$results = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
			// Get Count of all Assets to calculate percenrage of every channel
			$sumAllQuestions = App7020Helpers::arrSumByKey($results, 'count_reviews');

			$resultArray = array();
			if (count($results) > 0) {
				foreach ($results as $result) {
					if (!$result['color'])
						$result['color'] = '#030303';
					if (!$result['icon_bgr'])
						$result['icon_bgr'] = '#030303';
					if (!$result['text_color'])
						$result['text_color'] = '#ffffff';
					$resultArray[] = array(
						'label' => mb_strlen($result['channel_trans_name']) ? $result['channel_trans_name'] : $result['channel_transDef_name'],
						'value' => round(($result['count_reviews'] / $sumAllQuestions) * 100),
						'color' => $result['icon_bgr'],
						'highlight' => $result['icon_bgr'],
						'text_color' => $result['icon_color']
					);
				}
			} else {
				$resultArray[] = array(
					'label' => Yii::t('app7020', 'No data available'),
					'value' => 0.1,
					'color' => '#030303',
					'highlight' => '#030303',
					'text_color' => '#ffffff'
				);
			}
			return $resultArray;
		} else {
			return array();
		}
	}

	/**
	 * Data for rateing list "Top 5 Experts by participation rate"
	 * @return array
	 */
	public static function topExpertsByPartecipationRate() {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
				idst, 
				CONCAT(firstname, ' ', lastname) AS expert_name,
				firstname as firstname,
				lastname as lastname,
				TRIM(LEADING '/' FROM userid) AS username,
				avatar,
				count(*) as count_reviews
			");
		$commandBase->from(CoreUser::model()->tableName() . ' cu');
		$commandBase->join(
				App7020ContentReview::model()->tableName() . ' content_reviews', "content_reviews.idUser = cu.idst"
		);
		$commandBase->group('idst');
		$commandBase->order('count_reviews DESC');

		$resultArray = array();
		foreach ($commandBase->queryAll(true) as $result) {
			$avatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$resultArray[] = array(
				'idst' => $result['idst'],
				'expert_name' => $result['expert_name'],
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'username' => $result['username'],
				'avatar' => $avatar,
				'count_reviews' => $result['count_reviews']
			);
		}
		$user = Yii::app()->request->getParam('id');
		$keyCurrent = array_search($user, array_keys($resultArray));
		$resultArray = array_values($resultArray);
		if ($keyCurrent == 0) {
			$users[$keyCurrent] = $resultArray[$keyCurrent];
			$users[$keyCurrent + 1] = $resultArray[$keyCurrent + 1];
			$users[$keyCurrent + 2] = $resultArray[$keyCurrent + 2];
			return array_filter($users);
		} else {
			$users[$keyCurrent - 1] = $resultArray[$keyCurrent - 1];
			$users[$keyCurrent] = $resultArray[$keyCurrent];
			$users[$keyCurrent + 1] = $resultArray[$keyCurrent + 1];
			return array_filter($users);
		}
	}

	/**
	 * Data for rateing list "Top 5 Experts by first to review"
	 * @return array
	 */
	public static function topExpertsByFirstToReview() {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
		$standartAvatar = Yii::app()->theme->baseUrl . '/images/standard/user.png';
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
				idst, 
				CONCAT(firstname, ' ', lastname) AS expert_name,
				firstname as firstname,
				lastname as lastname,
				TRIM(LEADING '/' FROM userid) AS username,
				avatar,
				count(*) as count_reviews
			");
		$commandBase->from(App7020ContentReview::model()->tableName() . ' cr');
		$commandBase->join(
				"(
						SELECT
							MIN(created) AS min_val,
							idContent AS idc
						FROM " . App7020ContentReview::model()->tableName() . " 
						GROUP BY idContent
					) as cr2", "cr.idContent = cr2.idc AND cr.created = cr2.min_val"
		);
		$commandBase->join(
				CoreUser::model()->tableName() . " cu", "cr.idUser=cu.idst"
		);
		$commandBase->group('idst');
		$commandBase->order('count_reviews DESC');

		$resultArray = array();
		foreach ($commandBase->queryAll(true) as $result) {
			$avatar = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['idst']),true);
			$resultArray[] = array(
				'idst' => $result['idst'],
				'expert_name' => $result['expert_name'],
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'username' => $result['username'],
				'avatar' => $avatar,
				'count_reviews' => $result['count_reviews']
			);
		}

		$user = Yii::app()->request->getParam("id", false);
		$keyCurrent = array_search($user, array_keys($resultArray));
		$resultArray = array_values($resultArray);
		if ($keyCurrent == 0) {
			$users[$keyCurrent] = $resultArray[$keyCurrent];
			$users[$keyCurrent + 1] = $resultArray[$keyCurrent + 1];
			$users[$keyCurrent + 2] = $resultArray[$keyCurrent + 2];
			return array_filter($users);
		} else {
			$users[$keyCurrent - 1] = $resultArray[$keyCurrent - 1];
			$users[$keyCurrent] = $resultArray[$keyCurrent];
			$users[$keyCurrent + 1] = $resultArray[$keyCurrent + 1];
			return array_filter($users);
		}
	}

}
