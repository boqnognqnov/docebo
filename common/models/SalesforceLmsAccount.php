<?php

/**
 * This is the model class for table "salesforce_lms_account".
 *
 * The followings are the available columns in table 'salesforce_lms_account':
 * @property integer $id_account
 * @property string $agent
 * @property string $password
 * @property string $token
 * @property string $sync_user_types
 * @property integer $replicate_courses_as_products
 * @property integer $replicate_courses_highest_history
 * @property integer $sync_frequency
 * @property string $selected_items
 * @property integer $id_org
 * @property string $sync_job_hash
 * @property integer $use_sandbox
 */
class SalesforceLmsAccount extends CActiveRecord
{
    
    const SYNC_USER_TYPE_USERS = 'user';
    const SYNC_USER_TYPE_CONTACTS = 'contact';
    const SYNC_USER_TYPE_LEADS = 'lead';

    const SYNC_COURSE_TYPE_ALL = 'all';
    const SYNC_COURSE_TYPE_CATEGORIES = 'categories';
    const SYNC_COURSE_TYPE_CATALOGS = 'catalogs';

    const SF_TYPE_ACCOUNT = 'account';
    
    const TYPE_REPLICATE_NONE           = 0;         // Replicate Only Courses (Custom objects Only)
    const TYPE_REPLICATE_COURSES        = 1;         // Replicate Courses As Salesforce Products
    const TYPE_REPLICATE_SALES_ORDERS   = 2;         // Replicate them also as Sales Orders
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_lms_account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agent, password, token', 'checkDecrypted', 'on' => 'update'),
			array('agent', 'checkForNewAccount', 'on' => 'update', 'old' => $this->getDecryptedField($this->agent)),
			array('agent, password, token', 'required'),				
			array('replicate_courses_as_products, replicate_courses_highest_history, sync_frequency, id_org, use_sandbox', 'numerical', 'integerOnly'=>true),
			array('agent, password, token', 'length', 'max'=>255),
            array('sync_user_types', 'default', 'setOnEmpty' => true, 'value' => ''),
			array('sync_user_types', 'length', 'max'=>32),       
			array('sync_job_hash', 'length', 'max'=>64),
            array('selected_items', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_account, agent, password, token, sync_user_types, replicate_courses_as_products, replicate_courses_highest_history, sync_frequency, selected_items', 'safe', 'on'=>'search'),
		);
	}
	
	public function checkDecrypted($attribute, $value){		
		if($this->$attribute == $this->getDecryptedField($this->$attribute)){
			return true;
		}
	}

    public $replicate_courses_highest_history = 0;

	public function beforeSave(){
		
		if($this->getScenario() == 'encrypt'){
			
			$securityManager = new CSecurityManager();
			$securityManager->setEncryptionKey(self::getEncryptionKey());
			$securityManager->cryptAlgorithm = self::getCryptAlgorithm();
			
			$this->agent = base64_encode($securityManager->encrypt($this->agent));
			$this->password = base64_encode($securityManager->encrypt($this->password));
			$this->token = base64_encode($securityManager->encrypt($this->token));
		}
		
		if(parent::beforeSave()){
			return true;
		}
	}
	
	/**
	 * Preventing from messing with 2 Salesforce accounts, when changing the Salesforce account agent
	 * 
	 * @param string $attribute
	 * @param array $value
	 */
	public function checkForNewAccount($attribute, $value){
		if($this->getDecryptedField($this->$attribute) !== $value['old']){
			$this->addError($attribute, Yii::t('salesforce', 'Please, click "Clear configuration" first, when changing your Salesforce account'));
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_account' => 'Id Account',
			'agent' => 'Agent',
			'password' => 'Password',
			'token' => 'Token',
			'sync_user_types' => 'Sync User Types',
            'replicate_courses_as_products' => 'Replicate Courses as Products',
            'replicate_courses_highest_history' => 'Replicate Courses Highest History',
			'sync_frequency' => 'Sync Frequency',
            'selected_items' => 'Selected Items',
            'id_org' => 'Id Org',
			'sync_job_hash' => 'Sync Job Hash',
		    'use_sandbox' => 'Use Sandbox',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_account',$this->id_account);
		$criteria->compare('agent',$this->agent,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('sync_user_types',$this->sync_user_types,true);
		$criteria->compare('replicate_courses_as_products',$this->replicate_courses_as_products);
        $criteria->compare('replicate_courses_highest_history',$this->replicate_courses_highest_history);
		$criteria->compare('sync_frequency',$this->sync_frequency);
        $criteria->compare('selected_items',$this->selected_items,true);
        $criteria->compare('id_org',$this->id_org);
        $criteria->compare('sync_job_hash',$this->sync_job_hash,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceLmsAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Return list of IDs of selected course catalogs to sync with Saleforce
	 */
	public function getSelectedCourseCatalogs() {
		
	}
	
	/**
	 * Return list of IDs of selected course categories to sync with Saleforce
	 */
	public function getSelectedCourseCategories() {

	}
    /**
     * Returns the first Salesforce account int the database
     *
     * @return array SalesforceLmsAccount
     */
    public static function getFirstAccount(){
        return self::model()->find();
    }

    /**
     *
     * @return int - next available number of the username from Salesforce
     */
    public static function getNextSaleforceUsername(){
        $criteria = new CDbCriteria();        
        $criteria->order = 'id_user_lms DESC';
        $criteria->limit = 1;
        $userSetting = SalesforceUser::model()->find($criteria);
        
        if(empty($userSetting)){
            return 1;
        }                
        
        $userModel = CoreUser::model()->findByPk($userSetting->id_user_lms);
        
        $username = $userModel->getUserNameFormatted();
        
        $usernameCurrent = 0;
        //If the username is something like "d21412"
        if(preg_match("/(\w+)(\d+)/", $username)){
            $usernameCurrent = floatval( filter_var($username, FILTER_SANITIZE_NUMBER_INT));
        }
        
        return $usernameCurrent + 1;
    }
    
    public function getUserReplications(){
        $userRep = $this->sync_user_types;

        if(!empty($userRep)){
            return explode(',', $userRep);
        }

        return null;
    }

    /**
     * Return array of all possible user types
     * 
     * @return array
     */
    public static function getUserTypes(){
        return array(
            self::SYNC_USER_TYPE_USERS,
            self::SYNC_USER_TYPE_CONTACTS,
            self::SYNC_USER_TYPE_LEADS,
        );
    }

    public static function createSalesforceOrgChart($translation, $activeLanguages, $sfOrgChart = null){
        set_time_limit(3*3600); // 3 hours
        if(empty($sfOrgChart)){
            /****- Get $idOrg of the Root Node -****/
            $sfOrgChart = CoreOrgChartTree::getOrgRootNode()->idOrg;
        }

        $coreOrgChartTree = new CoreOrgChartTree();

        $root = CoreOrgChartTree::model()->findByPk($sfOrgChart);
        $coreOrgChartTree->appendTo($root, true, null, false );

        $currentId = $coreOrgChartTree->getPrimaryKey();

        foreach ($activeLanguages as $language){
            Yii::app()->db->createCommand()
                ->insert(CoreOrgChart::model()->tableName(), array(
                    'id_dir' => $currentId,
                    'lang_code' => $language,
                    'translation' => $translation
                ));
        }

        return $currentId;
    }

    public static function clearConfig( $accountId ) {
        $salesforceModel = SalesforceLmsAccount::model()->findByPk( $accountId );
    	if ( $accountId ) {
           /* if(!empty($salesforceModel->sync_job_hash)){
                $job = CoreJob::model()->find(
                    'hash_id = :hash', array(
                        ':hash' => $salesforceModel->sync_job_hash
                    )
                );
                if(!empty($job)) {
                    Yii::app()->scheduler->deleteJob($job, true);
                }
            }*/
            CoreJob::model()->deleteAll('name = :name AND params LIKE :param', array(
                ':name' => 'SalesforceAutoSync',
                ':param' => '%"sfAccountId":"'.$accountId.'"%'
            ));
            CoreJob::model()->deleteAll('name = :name AND params LIKE :param', array(
                ':name' => 'SalesforceSyncManual',
                ':param' => '%"id_account":"'.$accountId.'"%'
            ));
            //Renaming the existing Salesforce org chart
            $orgChart = CoreOrgChart::model()->findAll('id_dir = :idOrg', array(
            		':idOrg' => $salesforceModel->id_org
            ));
            
            foreach($orgChart as $org){
            	$org->translation = $org->translation .  '_' . date('dMY') ;
            	$org->save();
            }
            
    		SalesforceLog::model()->deleteAllByAttributes(array(
    				'id_account' => $accountId
    		));
    		SalesforceOrgchart::model()->deleteAllByAttributes(array(
    				'sf_lms_id' => $accountId
    		));
    		SalesforceUser::model()->deleteAllByAttributes(array(
                'sf_lms_id' => $accountId
            ));
    		SalesforceLmsAccount::model()->deleteByPk( $accountId );
    	}
    }

    /**
     * Return the Selected Course type from $selected_items
     * 'all', 'categories', 'catalogs'
     */
    public function getSelectionType() {
        $selectionInfo = CJSON::decode($this->selected_items);
        if (!$selectionInfo) {
            return $this->selected_items;
        }
        return $selectionInfo['type'];
    }

    /**
     * Return list of selected Catalogs OR Categories meta-selection, depending on the type of seletion (All, Categories, Catalogs)
     * 
     * @return array
     */
    public function getSelectedIds() {
        $tmpArray = array();
        $selectionInfo = CJSON::decode($this->selected_items);
        switch ( $this->getSelectionType() ) {
            case self::SYNC_COURSE_TYPE_CATEGORIES :
            	$tmpArray = $selectionInfo['selected'];
                break;
                
            case self::SYNC_COURSE_TYPE_CATALOGS:
            	$tmpArray = $selectionInfo['selected'];
                if ( count($tmpArray == 1) && empty($tmpArray[0]) ){
                    $tmpArray = array();
                }
            	foreach ($tmpArray as $index => $value) $tmpArray[$index] = (int) $value;
                break;
        }
        
        return $tmpArray;
    }

    /**
     * Return an array of  ("courses" => array(), "plans" => array()), i.e. list of COURSES and PLANS in THIS account selection
     *
     * @param boolean $setToAll Forces the script to return all courses/plans
     * @return array
     */
    public function getSelectedItems($setToAll = false) {
        if($setToAll == false) {
            $selectionType = $this->getSelectionType();
        } else {
            $selectionType = self::SYNC_COURSE_TYPE_ALL;
        }
        
        $plans = array();
        $courses = array();
        $categories = array();
        switch ($selectionType) {
            
            // CATALOGS
            case self::SYNC_COURSE_TYPE_CATALOGS:
                // Selected catalogs are...
                $catalogs   = $this->getSelectedIds();
                $courses    = LearningCatalogue::getCatalogCoursesListV2($catalogs, array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR));
                $plans      = LearningCatalogue::getCatalogPlansList($catalogs);
                break;
                
            // CATEGORIES    
            case self::SYNC_COURSE_TYPE_CATEGORIES:
                // Meta-Selected categories are... (Meta, because: key - selectState)
                $categoriesMetaSelection = $this->getSelectedIds();
                $categories = array();
                foreach ( $categoriesMetaSelection as $categorySelection ) {
                    if ( $categorySelection['selectState'] == 2 ) {
                        $categories = array_merge($categories, LearningCourseCategoryTree::model()->getCategoryAndDescendants( $categorySelection['key'] ));
                    } else {
                        $categories[] = $categorySelection['key'];
                    }
                }
                $courses = LearningCourseCategory::getCoursesListFromCategories($categories, array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR));
                break;

              
            // ALL
            case self::SYNC_COURSE_TYPE_ALL:
            default:
            	$plans = Yii::app()->db->createCommand()
            		->select('id_path')
            		->from(LearningCoursepath::model()->tableName())
            		->queryColumn();
            	
            	$courses = Yii::app()->db->createCommand()
            		->select('idCourse')
            		->from(LearningCourse::model()->tableName())
            		->where(array('IN', 'course_type', array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR)))
            		->queryColumn();
            	break;
        }
        
        
        $result = array(
            'courses'   => $courses,
            'plans'     => $plans,
            'categories'=> $categories
        );
        
        return $result;
        
    }
    
    /**
     * Get the Encryption key (HARDCODED)
     */
    public static function getEncryptionKey(){
    	return sha1('DoceboIsTheBest');
    }
 	
    /**
     * Get the ecryption algorithm
     * 
     * @return string
     */
    public static function getCryptAlgorithm(){
    	return 'rijndael-256';
    }
    
    /**
     * Decrypt string
     * 
     * @param string $value Value to be decrypted
     * @return unknown|string
     */
    public function getDecryptedField($value){    	    
    	
    	if(!$this->isBase64($value)){
    		return $value;
    	}
    	
    	$securityManager = new CSecurityManager();
    	$securityManager->setEncryptionKey(self::getEncryptionKey());
    	$securityManager->cryptAlgorithm = self::getCryptAlgorithm();
    	
    	return  $securityManager->decrypt(base64_decode($value));
    }
    
    /**
     * Check if string is in base64 format
     * 
     * @param string $field Value of the string to be checked
     * @return boolean Returns true if the value is base64 format
     */
    public function isBase64($field){
    	$base64LastChars = substr($field, -2, 2);    	     
    	
    	if($base64LastChars !== '=='){
    		return false;
    	}
    	
    	return true;
    }
}
