<?php

/**
 * This is the model class for table "webinar_session".
 *
 * The followings are the available columns in table 'webinar_session':
 * @property integer $id_session
 * @property integer $course_id
 * @property integer $id_tool_account
 * @property string $name
 * @property integer $max_enroll
 * @property integer $score_base
 * @property string $description
 * @property string $date_begin
 * @property string $date_end
 * @property string $tool
 * @property string $custom_url
 * @property string $tool_params
 * @property integer $time_spent_to_complete
 * @property string $evaluation_type
 * @property integer $created_by
 * @property string $recording_file
 * @property string $complete_on_registration_watched
 * @property string $join_in_advance_time_user
 * @property string $join_in_advance_time_teacher
 * @property integer $min_attended_dates_for_completion
 * @property integer $allow_recording_completion
 *
 * The followings are the available model relations:
 * @property LearningCourse $course
 * @property WebinarToolAccount $idToolAccount
 * @property WebinarSessionUser[] $webinarSessionUsers
 */
class WebinarSession extends CActiveRecord
{
	//temp values from the form before converting it to "date_begin" and "date_end"
	/**
	 * @deprecated
	 */
	public $date;
	/**
	 * @deprecated
	 */
	public $start;
	/**
	 * @deprecated
	 */
	public $duration;

	/**
	 * @deprecated
	 */
	public $dateEnd;
	/**
	 * @deprecated
	 */
	public $end;
	/**
	 * @deprecated
	 */
	public $tool;
	/**
	 * @deprecated
	 */
	public $custom_url;

	const PERIOD_DAY = 'D';
	const PERIOD_WEEK = 'W';
	const PERIOD_MONTH = 'M';
	const PERIOD_YEAR = 'Y';

	//Used for Web Conference widget
	const STATUS_ANYTIME = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_HISTORY = 2;
	const STATUS_ONAIR = 4;
	const STATUS_SCHEDULED = 5;

	const TOOL_CUSTOM = 'custom';

	const EVAL_TYPE_ON_JOIN = 'joined_webinar';
	const EVAL_TYPE_MANUAL = 'manual';
	const EVAL_TYPE_ONLINE_TEST = 'online_test';

	const EVENT_WATCHED_LIVE = 'live';
	const EVENT_WATCHED_RECORDING = 'recording';

	//if the completion type is "on_join", here are the sub options
	public $eval_spent_time_on_webinar;

	public $timestampAttributes = array();

	public static $SESSION_USER_WAITING_LIST = -2;
	public static $SESSION_USER_CONFIRMED = -1;
	public static $SESSION_USER_SUBSCRIBED = 0;
	public static $SESSION_USER_BEGIN = 1;
	public static $SESSION_USER_END = 2;
	public static $SESSION_USER_SUSPEND = 3;

	public static function toolList()
	{
		static $cache = null;
		if($cache===null) {
			$tools = WebinarTool::getAllTools();
			$cache   = array();
			foreach ( $tools as $tool ) {
				$cache[ $tool->getId() ] = $tool->getName();
			}

			return $cache;
		}
		return $cache;
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'webinar_session';
	}



	public function behaviors()
	{
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => $this->timestampAttributes,
			)
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course_id, max_enroll, name', 'required', 'on' => 'create, update'),
			array('course_id, max_enroll, score_base, time_spent_to_complete, created_by, min_attended_dates_for_completion, allow_recording_completion', 'numerical', 'integerOnly'=>true),
			array('name, evaluation_type', 'length', 'max'=>45),
			//array('tool', 'validateTool', 'on' => 'create, update'),
			//array('tool', 'validateRoom', 'on' => 'create, update'),
			//array('date', 'validateDate', 'on' => 'create, update'),
			array('description, complete_on_registration_watched, eval_spent_time_on_webinar, min_attended_dates_for_completion, allow_recording_completion', 'safe'),
			// The following rule is used by search().
			array('id_session, course_id, name, max_enroll, score_base, description, time_spent_to_complete, evaluation_type, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * Custom validator for Tool input fields
	 */
	public function validateTool()
	{
		if ($this->tool != WebinarSession::TOOL_CUSTOM)
		{
			if (!$this->id_tool_account)
				$this->addError('id_tool_account', Yii::t('webinar', 'Please select webinar account'));
		}
		else if ($this->tool == WebinarSession::TOOL_CUSTOM)
		{
			if (!$this->custom_url)
				$this->addError('custom_url', Yii::t('webinar', 'Please add custom webinar URl'));
		}
	}

	/**
	 * Check if the session input data is suitable for creating/updating a room
	 */
	public function validateRoom()
	{
		$utcNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		if ($this->isNewRecord)
		{
			if ($this->tool) { //there is separate validation for this, but before it's selection there is no need to validate room anyway
				if (!ConferenceManager::canCreateNewRoom($this->course_id, $this->tool, $utcNow)) {
					$this->addError('tool', Yii::t('conference', 'Max number of active rooms has been reached.'));
				}
			}
		}

        // Check if the room has overlapping dates with an old rooms but only for the Adobe because in the widget only Adobe has accounts
			$this->configureDates();
			$hasOverlapping = ConferenceManager::manageRoomDateOverlapping($this->id_session, $this->id_tool_account, $this->date_begin, $this->date_end);
			if($hasOverlapping === true)
				$this->addError('id_tool_account', Yii::t('webinar', 'Max concurrent rooms reached.'));
	}

	/**
	 * Check if the date selected is not in the past
	 */
	public function validateDate()
	{
		if ($this->date && $this->start)
		{
			$timezone = Yii::app()->localtime->getTimeZone();
			$nowDate = new DateTime();
			$formDate = $this->date.' '.$this->start;
			$formDateTime = new DateTime($formDate, new DateTimeZone($timezone)); //set the input date in the user's specific timezone

			//then compare the two DT objects
			if ($nowDate > $formDateTime)
				$this->addError('date', Yii::t('standard', 'Invalid starting date or time.'));
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'getTotalHours' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
			'idToolAccount' => array(self::BELONGS_TO, 'WebinarToolAccount', 'id_tool_account'),
			'webinarSessionUsers' => array(self::HAS_MANY, 'WebinarSessionUser', 'id_session'),
			'sessionUser' => array(self::HAS_ONE, 'WebinarSessionUser', 'id_session', 'condition' => 'sessionUser.id_user = '.Yii::app()->user->id), //used for loading current user session enrollment directly
			'course' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
			'learningCourse' => array(self::BELONGS_TO, 'LearningCourse', 'course_id'),
			'webinarSessionDates' => array(self::HAS_MANY, 'WebinarSessionDate', 'id_session'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'date' => Yii::t('standard', '_DATE'),
			'start' => Yii::t('standard', '_START'),
			'duration' => Yii::t('statistic', '_TIME_IN'),
			'id_session' => 'Id Session',
			'course_id' => 'Course',
			'id_tool_account' => Yii::t('webinar', 'Tool account'),
			'name' => Yii::t('standard', '_NAME'),
			'max_enroll' => Yii::t('classroom', 'Maximum enrollments'),
			'score_base' => 'Score Base',
			'description' => 'Description',
			'date_begin' => 'Date Begin',
			'date_end' => 'Date End',
			'tool' => Yii::t('webinar', 'Tool'),
			'custom_url' => Yii::t('webinar', 'Webinar URL'),
			'tool_params' => 'Tool Params',
			'time_spent_to_complete' => 'Time Spent To Complete',
			'evaluation_type' => 'Evaluation Type',
			'created_by' => 'Created By',
			'recording_file' => 'Recording File',
			'complete_on_registration_watched' => Yii::t('webinar', 'or the user watched the registration (if available)'),
			'allow_recording_completion' => 'Allow Recording Completion'
		);
	}

	public function dataProvider($idCourse, $puFilter = false, $showInstructorSessions = false) {

		// Read filters from HTTP request
		$rq = Yii::app()->request;
		$input = new stdClass();
		$input->searchText = trim($rq->getParam('searchText', ''));
		$input->searchMonth = $rq->getParam('selectMonth', 0); // default: all months
		$input->searchYear = $rq->getParam('selectYear', 0); // default: all years
		$input->sort = $rq->getParam(get_class($this).'_sort', false);

		// Prepare selection criteria
		$criteria = new CDbCriteria();
		$criteria->addCondition("course_id = :course_id");
		$criteria->params[':course_id'] = $idCourse;
		$criteria->select = "t.*";

		// 1. Text filter
		if ($input->searchText != "") {
			$criteria->addCondition("name LIKE :search_text");
			$criteria->params[':search_text'] = '%'.$input->searchText.'%';
		}

		// 2. Time filters
		$filterMonth = ($input->searchMonth >= 1 && $input->searchMonth <= 12);
		$filterYear = ($input->searchYear > 0);
		if ($filterMonth && $filterYear) {
			// Both filters have been set
			$monthDays = cal_days_in_month(CAL_GREGORIAN, $input->searchMonth, $input->searchYear);
			$monthIndex = str_pad($input->searchMonth, 2, '0', STR_PAD_LEFT);
			$startCheck = $input->searchYear.'-'.$monthIndex.'-01 00:00:00';
			$endCheck = $input->searchYear.'-'.$monthIndex.'-'.$monthDays.' 23:59:59';
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->join = 'JOIN webinar_session_date ON webinar_session_date.id_session = t.id_session';
			$tmpCriteria->group = 't.id_session';
			$tmpCriteria->addBetweenCondition("CONVERT_TZ(concat(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC')", $startCheck, $endCheck);
			$tmpCriteria->addBetweenCondition("DATE_ADD(CONVERT_TZ(CONCAT(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC'), INTERVAL webinar_session_date.duration_minutes MINUTE) ", $startCheck, $endCheck, "OR");
			$criteria->mergeWith($tmpCriteria, "AND");
		} elseif ($filterMonth && !$filterYear) {
			// Only month has been set
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->join = 'JOIN webinar_session_date ON webinar_session_date.id_session = t.id_session';
            $tmpCriteria->group = 't.id_session';
			$tmpCriteria->addCondition("MONTH(CONVERT_TZ(concat(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC')) = :search_month", "OR");
			$tmpCriteria->addCondition("MONTH(DATE_ADD(CONVERT_TZ(CONCAT(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC'), INTERVAL webinar_session_date.duration_minutes MINUTE) ) = :search_month", "OR");
			$tmpCriteria->params[':search_month'] = $input->searchMonth;
			$criteria->mergeWith($tmpCriteria, "AND");
		} elseif (!$filterMonth && $filterYear) {
			// Only year has been set
			$startCheck = $input->searchYear.'-01-01 00:00:00';
			$endCheck = $input->searchYear.'-12-31 23:59:59';
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->join = 'JOIN webinar_session_date ON webinar_session_date.id_session = t.id_session';
            $tmpCriteria->group = 't.id_session';
			$tmpCriteria->addBetweenCondition("CONVERT_TZ(concat(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC')", $startCheck, $endCheck);
			$tmpCriteria->addBetweenCondition("DATE_ADD(CONVERT_TZ(CONCAT(webinar_session_date.day,' ',webinar_session_date.time_begin), webinar_session_date.timezone_begin, 'UTC'), INTERVAL webinar_session_date.duration_minutes MINUTE) ", $startCheck, $endCheck, "OR");
			$criteria->mergeWith($tmpCriteria, "AND");
		}

		// Check if list should be filtered for instructors
		$pUserRights = Yii::app()->user->checkPURights($idCourse);
		if(!Yii::app()->user->getIsGodadmin() && !$pUserRights->all && ($pUserRights->isInstructor || $pUserRights->isTutor)) {
			$criteria->join = 'INNER JOIN webinar_session_user cus ON cus.id_session = '.$this->getTableAlias().'.id_session AND cus.id_user = '.Yii::app()->user->id;
		}

		if ($puFilter) {
			$instructorCondition = '';
			if($showInstructorSessions && $pUserRights->isInstructor) {
				$criteria->join .= ' LEFT JOIN webinar_session_user instructor_sessions ON instructor_sessions.id_session = ' . $this->getTableAlias() . '.id_session AND instructor_sessions.id_user = ' . Yii::app()->user->id . ' ';
				$instructorCondition = ' OR instructor_sessions.id_user IS NOT NULL';
			}
			$criteria->addCondition("created_by = :created_by".$instructorCondition);
			$criteria->params[':created_by'] = Yii::app()->user->id;
		}

		if(!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsPu()){
			// current user is just a user or instructor, so we filter the sessions to that, in which this user is enrolled in
			$enrolledSessionIds = Yii::app()->db->createCommand()
					->select('id_session')->from(WebinarSessionUser::model()->tableName())
					->where('id_user=:id', array(':id' => Yii::app()->user->id))->queryColumn();
			$criteria->addCondition('t.id_session IN ("' . implode('","', $enrolledSessionIds) . '")');
		}

		// Pass search criteria to the data provider
		$config = array();
		$config['criteria'] = $criteria;
		$sortAttributes = array(
			'date_begin' => array(
				'asc' => "(SELECT CONVERT_TZ(CONCAT(DAY,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC FROM webinar_session_date WHERE id_session=t.id_session ORDER BY beginTimestampUTC ASC LIMIT 1)",
				'desc' => "(SELECT CONVERT_TZ(CONCAT(DAY,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC FROM webinar_session_date WHERE id_session=t.id_session ORDER BY beginTimestampUTC ASC LIMIT 1) DESC",
			),
		);
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = "(SELECT CONVERT_TZ(CONCAT(DAY,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC FROM webinar_session_date WHERE id_session=t.id_session ORDER BY beginTimestampUTC ASC LIMIT 1) DESC";
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WebinarSession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Compile dates tmp fields for the form representation
	 */
	public function afterFind()
	{
		parent::afterFind();
		if ($this->time_spent_to_complete > 0)
			$this->eval_spent_time_on_webinar = 1;

		if ($this->date_begin != '0000-00-00 00:00:00') {

			$date_begin = new DateTime(Yii::app()->localtime->fromLocalDatetime($this->date_begin), Yii::app()->localtime->getServerDateTimeZone());
			$date_begin->setTimezone(Yii::app()->localtime->getLocalDateTimeZone());

			$this->date = $date_begin->format('Y-m-d');
			$this->start = $date_begin->format('H:i');
		}

		if ($this->date_end != '0000-00-00 00:00:00') {
			$date_end = new DateTime(Yii::app()->localtime->fromLocalDatetime($this->date_end), Yii::app()->localtime->getServerDateTimeZone());
			$date_end->setTimezone(Yii::app()->localtime->getLocalDateTimeZone());
			$since_start = $date_begin->diff($date_end);
			$minutes = $since_start->days * 24 * 60;
			$minutes += $since_start->h * 60;
			$minutes += $since_start->i;
			$this->dateEnd = $date_end->format('Y-m-d');
			$this->end = $date_end->format('H:i');
			$this->duration = $minutes;
		}

		if ($this->tool == WebinarSession::TOOL_CUSTOM)
		{
			$params = CJSON::decode($this->tool_params);
			if (isset($params['custom_url']))
				$this->custom_url = $params['custom_url'];
		}

	}

	public function beforeValidate(){

		// Rise event so the plugins know that we are about to validate this model
		Yii::app()->event->raise('onBeforeWebinarSessionValidate', new DEvent($this, array('model' => &$this)));

		return parent::beforeValidate();
	}

	public function beforeSave()
	{
		//Save custom URL to tool_params as JSON object
//		if ($this->custom_url && $this->tool == WebinarSession::TOOL_CUSTOM) {
//			$params = array('custom_url' => $this->custom_url);
//			$this->tool_params = CJSON::encode($params);
//		}

		// Try calling the API
//		$tool = WebinarTool::getById($this->tool);
//		$startDateUtc = Yii::app()->localtime->fromLocalDateTime($this->date_begin);
//		$endDateUtc = Yii::app()->localtime->fromLocalDateTime($this->date_end);
//
//		$currentModel = self::findByPk($this->id_session);
//		$updateTimes = true;
//		if($currentModel && !$this->isNewRecord && ($tool->getId() === 'gototraining')){
//			$newStartTime = strtotime(Yii::app()->localtime->toLocalDateTime($this->date_begin, 'short', 'medium', 'Y-m-d H:i:s'));
//			$newEndTime = strtotime(Yii::app()->localtime->toLocalDateTime($this->date_end, 'short', 'medium', 'Y-m-d H:i:s'));
//			$currentEndTime = strtotime(Yii::app()->localtime->toLocalDateTime($currentModel->date_end, 'short', 'medium', 'Y-m-d H:i:s'));
//			$currentStartTime = strtotime(Yii::app()->localtime->toLocalDateTime($currentModel->date_begin, 'short', 'medium', 'Y-m-d H:i:s'));
//			if(($newStartTime === $currentStartTime) && ($newEndTime ==  $currentEndTime)){
//				$updateTimes = false;
//			}
//		}

		if ($this->isNewRecord) {
			$this->created_by = Yii::app()->user->id;
//			if($this->tool !== WebinarSession::TOOL_CUSTOM) {
//				$this->tool_params = CJSON::encode($tool->createRoom($this->name, $this->description, $this->id_tool_account, $startDateUtc, $endDateUtc, $this->max_enroll, $this->tool_params));
//			}
		}
//		else{
//			if($tool->getId() === 'gototraining'){
//				$this->tool_params = CJSON::encode($tool->updateRoom($this->name, $this->description, $this->id_tool_account,
//					is_array($this->tool_params) ? $this->tool_params : CJSON::decode($this->tool_params), $startDateUtc, $endDateUtc, $this->max_enroll, $updateTimes));
//
//			}else{
//				$this->tool_params = CJSON::encode($tool->updateRoom($this->name, $this->description, $this->id_tool_account,
//					is_array($this->tool_params) ? $this->tool_params : CJSON::decode($this->tool_params), $startDateUtc, $endDateUtc, $this->max_enroll));
//			}
//		}

		return parent::beforeSave();
	}

	/**
	 * This method is invoked after deleting a record.
	 */
	protected function afterDelete()
	{
		// Try calling the API
		$tool = WebinarTool::getById($this->tool);
		$tool->deleteRoom($this->id_tool_account, CJSON::decode($this->tool_params));

		$result = parent::afterDelete();

		Yii::app()->event->raise(
			SELF::EVENT_AFTER_DELETE,
			new DEvent($this, [])
		);

		return result;
	}

	/**
	 * Renders the 'waiting' column of the sessions grid
	 * @return string
	 */
	public function renderWaiting() {

		$count = $this->countWaiting(true);

		if ($count == 0)
			return '0';

		if(Yii::app()->user->getIsPu()){
			// Check for permissions
			if((!Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") || !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assing"))&&
				 (!Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign"))  && (!Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") || $this->created_by != Yii::app()->user->getIdst())) {
				// PU doesn't have Sessions edit permission OR
				// he has it, but the session was not created by him
				// In this case, just display the number of waiting users as string
				return $count;
			}
		}
		return CHtml::link($count, Docebo::createAppUrl('admin:webinar/session/waiting', array(
			'course_id' => $this->course_id,
			'id_session' => $this->getPrimaryKey()
		)), array('class' => 'waiting-users'));
	}

	/**
	 * Renders the enroll column of session grid
	 * @param bool $isInstructor
	 * @return string
	 */
	public function renderEnrolled($isInstructor = false) {
		//enrollments screen is visible to any power users (with or without enroll permission)
		//actions that admins/PUs can perform on users depend on permission, though

                  $pUserRights = Yii::app()->user->checkPURights($this->course_id);

		//enrollments screen is visible to any power users (with or without enroll permission)
		//actions that admins/PUs can perform on users depend on permission, though
		$isPuWithCourseEnrollPerm = $pUserRights->all && Yii::app()->user->checkAccess('enrollment/view');
		$isPuWithCourseModPerm = $pUserRights->all && Yii::app()->user->checkAccess('/lms/admin/course/mod');
		$isPuWithSessionAddPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add");
		$isPuWithSessionEditPerm = $pUserRights->all && (
				Yii::app()->user->checkAccess('/lms/admin/webinarsessions/mod') ||
				(Yii::app()->user->checkAccess('/lms/admin/webinarsessions/add') && ($this->created_by==Yii::app()->user->getIdst()))
			);
		$isSessionOwnedByPU = ($this->created_by == Yii::app()->user->getIdst());
		$course_associated = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $this->course_id)) ? true : false;

		$isPuAndCanEnroll = false;
		if (Yii::app()->user->getIsPu() && $course_associated) {
			if ($isPuWithCourseEnrollPerm) {
				if ($isSessionOwnedByPU) {
					$isPuAndCanEnroll = (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign") ||  $isPuWithSessionAddPerm );
				} else {
					$isPuAndCanEnroll =  (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/assign") || (Yii::app()->user->checkAccess("/lms/admin/webinarsessions/add") && !Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod") ));
				}
			}
		}

		if ($isInstructor)
			$canEnroll = false;
		else
			//$canEnroll = (Yii::app()->user->getIsGodadmin() || $isPuWithCourseModPerm || $isPuWithSessionEditPerm);
			$canEnroll = (Yii::app()->user->getIsGodadmin() || $isPuAndCanEnroll);

		$content = '';

		// Count already enrolled users
		$countEnrollments = $this->countEnrollments(!$pUserRights->isInstructor && $pUserRights->isPu, true);
		if ($countEnrollments > 0 || ($countEnrollments <= 0 && !$canEnroll)) {
			$countUsers = $countEnrollments;
			$content = $countUsers.'<span class="enrolled"></span>';
		} else {
			//this will be showed only to god admin and power users with enroll permission
			$content = '<span class="enroll">'.Yii::t('standard', 'Enroll').'</span>';
		}

		if ($canEnroll || $isInstructor) {
			if(!$pUserRights->isPu || $isPuWithCourseEnrollPerm || $pUserRights->isInstructor) {
				$content = CHtml::link($content, Docebo::createAppUrl('admin:webinar/enrollment/index', array(
					'course_id'  => $this->course_id,
					'id_session' => $this->getPrimaryKey()
				)));
			}
		}
		return $content;
	}




	/**
	 * Counts the users enrolled to this session. Users can be filtered by power user visibility.
	 *
	 * @param boolean $adminFilter IDST of the power user (if set to false, no filter will be applied and all users will be counted)
	 * @return integer the calculated number of users enrolled to this session
	 */
	public function countEnrollments($adminFilter = true, $excludeWaiting= false) {
                  $pUserRights = Yii::app()->user->checkPURights($this->course_id);
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*) AS num_enrolled")
			->from(WebinarSessionUser::model()->tableName()." wsu")
			->join(WebinarSession::model()->tableName()." ws", 'wsu.id_session = ws.id_session')
			->join(LearningCourseuser::model()->tableName().' cu', 'cu.idUser = wsu.id_user and cu.idCourse = ws.course_id')
			->join(CoreUser::model()->tableName().' u', 'u.idst = wsu.id_user'); // TODO: it may be better to join this with learning_courseuser, to avoid potential incoherent data
		if ($adminFilter && Yii::app()->user->getIsAdmin() && !$pUserRights->isInstructor) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = wsu.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}
		$command->where("wsu.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
		if ($excludeWaiting){
			$command->andWhere("wsu.status <> :waiting_status", array(':waiting_status' => WebinarSession::$SESSION_USER_WAITING_LIST));
			$command->andWhere("wsu.status <> :subs_confirm", array(':subs_confirm' => WebinarSession::$SESSION_USER_CONFIRMED));
		}

		$rs = $command->queryAll();
		return (int)$rs[0]['num_enrolled'];
	}

	/**
	 * Counts the users enrolled to this session, but their status is 'waiting'
	 * @return string
	 */
	public function countWaiting($filterByPowerUser = false)
	{
		$params = array(
			':id_session' => $this->id_session,
			':status' => WebinarSessionUser::$SESSION_USER_WAITING_LIST
		);
		$query = "SELECT COUNT(*) AS num_waiting "
			." FROM ".WebinarSessionUser::model()->tableName()." wsu ";
		if ($filterByPowerUser && Yii::app()->user->getIsPu()) {
			$query .= " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND wsu.id_user = pu.user_id)";
			$params[':puser_id'] = Yii::app()->user->id;
		}
		$query .= " WHERE wsu.id_session = :id_session AND wsu.status = :status";

		return Yii::app()->db->createCommand($query)->queryScalar($params);
	}

	public function renderSessionActions($viewPath = false) {

		if (!$viewPath) {
			$viewPath = '_sessionTableActions';
		}
		$sessionActions = Yii::app()->getController()->renderPartial($viewPath, array(
			'idSession' => $this->id_session,
			'idCourse' => $this->course_id,
			'name' => $this->name,
			'model'=>$this,
		), true);

		if(strpos($sessionActions, '<li') !== FALSE) {
			$content = Chtml::link('', 'javascript:void(0);', array(
				'id' => 'popover-'.uniqid(),
				'class' => 'popover-action popover-trigger',
				'data-toggle' => 'popover',
				'data-content' => $sessionActions,
			));
		} else
			$content = '';

		return $content;
	}

	/**
	 * Get start hours for the session create form
	 * @return array
	 */
	public static function arrayForHours()
	{
		$res = array();
		for ($m = 0; $m <= 86400; $m += 900)
		{
			$res[gmdate('H:i', $m)] = gmdate('g:i A', $m);
		}
		return $res;
	}

	/**
	 * Get duration array for the session create form
	 *
	 * @param bool $addZero
	 *
	 * @return array
	 */
	public static function arrayForDuration($addZero = false)
	{
		return TimeHelpers::arrayForDurations($addZero, 15);
	}

	/**
	 * get all accounts by a given tool
	 * @param $tool
	 * @return static[]
	 */
	public static function accountListByTool($tool)
	{
		if(!$tool)
			$tool = 'custom';

		return WebinarTool::getById($tool)->getAccounts();
	}

	/**
	 * convert input values to the DB representation
	 * simply calculate date_end with the start date/hour and duration
	 * @deprecated
	 */
	public function configureDates()
	{
		/**
		 * NOTE: $this->date ALWAYS need to came as UTC date format!!!!!
		 */

		if($this->date && $this->start && $this->duration){
			$this->date_begin = $this->date.' '.$this->start;
			$time = new DateTime($this->date_begin);
			$time->add(new DateInterval('PT' . $this->duration . 'M'));
			$this->date_end = $time->format('Y-m-d H:i');

			$this->date_begin = Yii::app()->localtime->toLocalDateTime($this->date_begin, 'short', 'short', null, Yii::app()->localtime->getTimeZone());
			$this->date_end   = Yii::app()->localtime->toLocalDateTime($this->date_end, 'short', 'short', null, Yii::app()->localtime->getTimeZone());
		}
	}

	/**
	 * get total hours via start/end dates
	 */
	public function getTotalHours($format = '%H:%I')
	{
		$interval = $this->getInterval();
		return $interval->format($format);
	}

	/**
	 *
	 * Get the total minutes for the session
	 *
	 * @return int Minutes
	 */
	public function getTotalMinutes(){
		$interval = $this->getInterval();

		$hours = $interval->format('%h');
		$minuites = $interval->format('%i');

		$total = ($hours * 60) + $minuites;

		return $total;
	}

	/**
	 * Get total hours in decimal format i.e. 3:30h -> 3.5 hours
	 * @return float|string
	 */
	public function getTotalHoursDecimal()
	{
		$interval = $this->getInterval();
		$hours = $interval->format('%h');
		$decimalMinutes = $interval->format('%i');
		if ($decimalMinutes > 0)
		{
			$decimalMinutes = $decimalMinutes / 60;
		}
		$hoursDecimal = $hours + $decimalMinutes;
		$hoursDecimal = round($hoursDecimal, 2);
		return $hoursDecimal;
	}

	/**
	 * Get interval between date_begin and date_end
	 * @return bool|DateInterval
	 */
	public function getInterval()
	{
		$durationsMinutes = (int) Yii::app()->getDb()->createCommand()
			->select('SUM(duration_minutes)')
			->from(WebinarSessionDate::model()->tableName())
			->where('id_session=:idSession', array(':idSession'=>$this->id_session))
			->queryScalar();
		if($durationsMinutes<0){
			// Something wrong
			return new DateInterval("PT0M");
		}
		if($durationsMinutes>=60) {
			$hours = floor($durationsMinutes / 60);
			$minutes = $durationsMinutes % 60;
			$di = new DateInterval("PT{$hours}H{$minutes}M");
		}else{
			$di = new DateInterval("PT{$durationsMinutes}M");
		}
		return $di;
	}

	/**
	 * Check if the tool exists and return it's name
	 * @return string
	 */
	public function getToolName($listingTools = false)
	{
		$toolCodes = Yii::app()->getDb()->createCommand()
			->selectDistinct('webinar_tool')
			->from(WebinarSessionDate::model()->tableName())
			->where('id_session=:idS', array(':idS'=>$this->id_session))
			->queryColumn();

		$tools = self::toolList();

		if(count($toolCodes)==0){
			return Yii::t('standard', 'None');
		}elseif(count($toolCodes)==1){
			$toolCode = reset($toolCodes);
			return (isset($tools[$toolCode])) ? $tools[$toolCode] : '';
		}elseif(count($toolCodes)>1){
			if ($listingTools) {
				$toolsNames = array();
				foreach ($toolCodes as $toolCode) {
					$toolsNames[] = $tools[$toolCode];
				}
				return Yii::t('standard', 'Multiple tools ({tools})', array('{tools}' => implode(", ", $toolsNames)));
			}
			else
				return Yii::t('standard', 'Multiple tools ({count})', array('{count}'=>count($toolCodes)));
		}
	}

	/**
	 * Render tools in session grids (shows the tooltip as well)
	 * @return string
	 */
	public function renderTools($onlyText = false) {
		$output = Yii::t('standard', 'None');
		$title = '';

		//all tools
		$tools = self::toolList();

		// Get the list of used tools
		$toolCodes = Yii::app()->getDb()->createCommand()
			->selectDistinct('webinar_tool')
			->from(WebinarSessionDate::model()->tableName())
			->where('id_session=:idS', array(':idS'=>$this->id_session))
			->queryColumn();

		if (!empty($toolCodes)) {
			if(count($toolCodes)==1){
				$toolCode = reset($toolCodes);
				$title = $text = (isset($tools[$toolCode])) ? $tools[$toolCode] : '';
			}elseif(count($toolCodes)>1){
				$text = Yii::t('standard', 'Multiple tools ({count})', array('{count}'=>count($toolCodes)));
				foreach ($toolCodes as $code)
					$title .= $tools[$code] . "<br/>";
			}

			$output = CHtml::link($text, '#', array(
				'title' => $title,
				'rel' => 'tooltip',
				'data-html' => true,
				'class' => 'tool-list',
				'onclick' => 'javascript:;'
			));
			if ($onlyText === true) {
				$output = $title;
			}
		}

		return $output;
	}

	/**
	 * Copy this session into a new record.
	 * Dates and hours total will be calculated separately, since they depend on related data.
	 *
	 * @param string $newName the name of the new copied session
	 * @param boolean $duplicateDates if set to true, dates will be copied too
	 * @param integer $daysShift if dates are copied too, specify how many days they will be shifted in the calendar
	 * @return WebinarSession the copied session AR
	 * @throws CException
	 */
	public function copy($newName, $interval) {

		$db = Yii::app()->db;
		$output = false;

		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {
			// Clone session object
			$session = new WebinarSession();
			$session->setAttributes($this->getAttributes(), false);
			$session->name = (!empty($newName) ? $newName : 'Copy of ' . $this->name);
			$session->id_session = null;

			if (!$session->save(false))
				throw new CException('Error while copying session');

			$datesSaved = true;
			$dates = $this->getDates();
			/* @var $date WebinarSessionDate */
			foreach ($dates as $date) {

				$copyOfDate = new WebinarSessionDate();
				$copyOfDate->setAttributes($date->getAttributes(), false);
				$copyOfDate->day = Yii::app()->localtime->addInterval($copyOfDate->day, $interval);
				$copyOfDate->webinar_custom_url = $date->webinar_custom_url;
				$copyOfDate->id_session = $session->id_session;

				$res = $copyOfDate->save();

				$sessionDateRecording = new WebinarSessionDateRecording();
				$sessionDateRecording->id_session = $session->id_session;
				$sessionDateRecording->day = $copyOfDate->day;
				$sessionDateRecording->id_course = $session->course_id;
				$sessionDateRecording->type = WebinarSessionDateRecording::RECORDING_TYPE_NONE;
				$sessionDateRecording->path = '-';
				$sessionDateRecording->status = WebinarSessionDateRecording::RECORDING_STATUS_NON_EXISTANT;
				$sessionDateRecording->save();

				if(!$res){
					Yii::log(print_r($copyOfDate->getErrors(), true), CLogger::LEVEL_ERROR);
					throw new CException('Error saving webinar date ' . $copyOfDate->day);
				}

				// even if one date fails, $datesSaved will turn to false
				$datesSaved &= $res;
			}

			if($datesSaved){
				Yii::app()->event->raise('WebinarSessionCreated', new DEvent($this, array(
					'session' => $session,
					'session_dates' => $dates
				)));
			}else{
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			if (isset($transaction)) { $transaction->commit(); }

			$output = $session;

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}

		return $output;
	}

	/**
	 * Deletes a session with its related tables (dates, attendance, and so on)
	 */
	public function deleteSession() {
		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) {
			$transaction = $db->beginTransaction();
		}
		$eventData = array(
			'session_data' => array(
				'id_session' => $this->id_session,
				'name' => $this->name,
				'date_begin' => Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getStartDate($this->id_session)),
				'date_end' => Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getEndDate($this->id_session)),
				'other_info' => $this->description,
				'session_id' => $this->id_session,
				'users_list' => $this->getEnrolledUsers(),
				'tool' => $this->tool,
				'toolName'=>$this->getToolName(true),
			),
			'dates'=>$this->getDates(),
			'sessionModel'=>$this,
			'course_id' => $this->course_id,
		);

		try {

			WebinarSessionUser::model()->deleteAllByAttributes(array('id_session' => $this->id_session));

			/* @var $date WebinarSessionDate */
			foreach($this->getDates() as $date){
				$date->delete();
			}

			// Now delete session object
			$this->delete();

			// Commit everything
			if (isset($transaction)) {
				$transaction->commit();
			}

		} catch (CException $e) {

			if (isset($transaction)) {
				$transaction->rollback();
			}
			throw $e;
		}

		if (Yii::app()->user->isPu || Yii::app()->user->isGodAdmin)
		{
			Yii::app()->event->raise('WebinarSessionDeleted', new DEvent($this, $eventData));
		}

		return true;
	}

	/**
	 * Returns the data provider object for session enrollment listings
	 * @return CSqlDataProvider
	 */
	public function dataProviderEnrollment($searchInput = null, $orderByInput = null) {
		$db = Yii::app()->db;
                  $pUserRights = Yii::app()->user->checkPURights($this->course_id);
		$command = $db->createCommand()
			->select("COUNT(*)")
			->from(WebinarSessionUser::model()->tableName() . " cus")
			->join(CoreUser::model()->tableName() . " u", "u.idst = cus.id_user")
			->join(LearningCourseuser::model()->tableName() . " cu", "u.idst = cu.idUser AND cu.idCourse = :id_course", array(':id_course' => $this->course_id))
			->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->andWhere("cus.status <> :waiting_status", array(':waiting_status' => WebinarSession::$SESSION_USER_WAITING_LIST))
			->andWhere("cus.status <> :subs_confirm", array(':subs_confirm' => WebinarSession::$SESSION_USER_CONFIRMED));

		//--- power user filter ---
		if ($pUserRights->isPu && !$pUserRights->isInstructor)
			$command->join(CoreUserPU::model()->tableName().' pu', 'u.idst = pu.user_id AND puser_id = :puser_id', array(':puser_id' => Yii::app()->user->id));
		//---

		if ($searchInput)
			$command->andWhere('CONCAT(u.firstname, " ", u.lastname) like :search OR u.email LIKE :search OR u.userid LIKE :search', array(':search' => '%' . $searchInput . '%'));

		$count = $command->queryScalar();

		$command = $db->createCommand()
			->select("u.idst, u.userid, u.firstname, u.lastname, u.email, cus.id_user, cu.level, cus.status, cu.idCourse, cus.id_session")
			->from(WebinarSessionUser::model()->tableName() . " cus")
			->join(CoreUser::model()->tableName() . " u", "u.idst = cus.id_user")
			->join(LearningCourseuser::model()->tableName() . " cu", "u.idst = cu.idUser AND cu.idCourse = :id_course", array(':id_course' => $this->course_id))
			->where("cus.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()))
			->andWhere("cus.status <> :waiting_status", array(':waiting_status' => WebinarSession::$SESSION_USER_WAITING_LIST))
			->andWhere("cus.status <> :subs_confirm", array(':subs_confirm' => WebinarSession::$SESSION_USER_CONFIRMED))
			->order($orderByInput ? $orderByInput : "u.userid ASC, u.firstname ASC, u.lastname ASC");

		//--- power user filter ---
		if (Yii::app()->user->isAdmin && !$pUserRights->isInstructor)
			$command->join(CoreUserPU::model()->tableName().' pu', 'u.idst = pu.user_id AND puser_id = :puser_id', array(':puser_id' => Yii::app()->user->id));
		//---

		if ($searchInput)
			$command->andWhere('CONCAT(u.firstname, " ", u.lastname) like :search OR u.email LIKE :search OR u.userid LIKE :search', array(':search' => '%' . $searchInput . '%'));

		$dataProvider = new CSqlDataProvider($command, array(
			'totalItemCount' => $count,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
		));

		// $dataProvider->getData() will return a list of arrays.
		return $dataProvider;
	}


	/**
	 * Get a list of enrolled users to this session. Users can be filtered by level.
	 *
	 * @param bool|int $level if set, only users with the specifed level will be retrieved
	 * @param bool $excludeWaiting
	 * @param bool $noAdminCheck
	 * @return array a list of records (arrays) retrieved
	 */
	public function getEnrolledUsers($level = false, $excludeWaiting = false, $noAdminCheck = false) {
		$command = Yii::app()->db->createCommand()
			->select("wsu.*, cs.course_id, cu.level, cu.status")
			->from(LearningCourseuser::model()->tableName()." cu")
			->join(WebinarSession::model()->tableName()." cs", "cs.course_id = cu.idCourse")
			->join(WebinarSessionUser::model()->tableName()." wsu", "cu.idCourse = cs.course_id AND wsu.id_session = cs.id_session AND wsu.id_user = cu.idUser")
			->where("cs.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
		if ($level != false) {
			$command->andWhere("cu.level = :level", array(':level' => $level));
		}

		if ($excludeWaiting) {
			$command->andWhere("wsu.status <> :status", array(':status' => WebinarSessionUser::$SESSION_USER_WAITING_LIST));
		}

		if (!$noAdminCheck && Yii::app()->user->getIsPu()) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = wsu.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}

		return $command->queryAll();
	}



	/**
	 * Get a list of enrolled users to this session. Users can be filtered by level.
	 *
	 * @param integer $level if set, only users with the specifed level will be retrieved
	 * @return array a list of records (arrays) retrieved
	 */
	public function isUserEnrolledInSession() {
		$sessionEnrollment = WebinarSessionUser::model()->findByPk(array(
			'id_user' => Yii::app()->user->id,
			'id_session' => $this->getPrimaryKey()
		));

		return ($sessionEnrollment != null);
	}

	/**
	 * Get a count of enrolled users to this session. USers can be filtered by level.
	 *
	 * @param bool|int $level if set, only users with the specifed level will be counted
	 * @param bool $excludeWaiting
	 * @return integer the number of users enrolled
	 */
	public function countEnrolledUsers($level = false, $excludeWaiting = false) {
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(LearningCourseuser::model()->tableName()." cu")
			->join(WebinarSession::model()->tableName()." cs", "cs.course_id = cu.idCourse")
			->join(WebinarSessionUser::model()->tableName()." wsu", "cu.idCourse = cs.course_id AND wsu.id_session = cs.id_session AND wsu.id_user = cu.idUser")
			->where("cs.id_session = :id_session", array(':id_session' => $this->getPrimaryKey()));
		if ($level != false) { $command->andWhere("cu.level = :level", array(':level' => $level)); }

		if ($excludeWaiting) {
			$command->andWhere("wsu.status <> :status", array(':status' => WebinarSessionUser::$SESSION_USER_WAITING_LIST));
		}

		return $command->queryScalar();
	}

	/**
	 * Returns webinar URL string
	 * Start Room URL for admins
	 * Join Room for other users
	 */
	public function getWebinarUrl($widgetContext = false)
	{
		$tool = WebinarTool::getById($this->tool);
		$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
		if ($this->allowAdminOperations())
			$url = $tool->getStartRoomUrl($this->name, $this->max_enroll, $this->id_tool_account, CJSON::decode($this->tool_params), $userModel, $this);
		else
			$url = $tool->getJoinRoomUrl($this->name, $this->max_enroll, $this->id_tool_account, CJSON::decode($this->tool_params), $userModel, $this, $widgetContext);
		return $url;
	}

	/**
	 * Is current user allowed to manage sessions
	 * @return bool
	 */
	public function allowAdminOperations()
	{
		if ($this->sessionUser)
			$level = $this->sessionUser->level;
		elseif (LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->course_id))
			$level = LearningCourseuser::userLevel(Yii::app()->user->id, $this->course_id);
		else
			$level = LearningCourseuser::$USER_SUBSCR_LEVEL_GUEST;

		if(Yii::app()->user->getIsPu()) {
			// Is this a power user and the course was assigned to him?
			$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $this->course_id
			));
		} else
			$pUserCourseAssigned = null;

		return Yii::app()->user->getIsGodadmin() ||
			(Yii::app()->user->getIsPu() && $pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')) ||
			($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

	}

	public function sessionEnrollmentDataProvider($courseId, $showOverbookedSessions = true, $hideCurrentUserSubscribedSessions = false)
	{
		$rq = Yii::app()->request;
		$input = new stdClass();
		$input->searchText = trim($rq->getParam('searchText', ''));
		$input->searchMonth = $rq->getParam('selectMonth', 0); // default: all months
		$input->searchYear = $rq->getParam('selectYear', 0); // default: all years
		$input->sort = $rq->getParam(get_class($this).'_sort', false);

		// Prepare selection criteria
		$searchCriteria = new CDbCriteria();

		// 1. Text filter
		if ($input->searchText != "") {
			$searchCriteria->addCondition("name LIKE :search_text");
			$searchCriteria->params[':search_text'] = '%'.$input->searchText.'%';
		}

		if($hideCurrentUserSubscribedSessions)
		{
			$searchCriteria->join = 'LEFT JOIN '.WebinarSessionDateAttendance::model()->tableName().' as users ON users.id_session = t.id_session AND users.id_user = '.Yii::app()->user->id;
			$searchCriteria->addCondition('users.id_user IS NULL');
		}

		// 2. Time filters
		$filterMonth = ($input->searchMonth >= 1 && $input->searchMonth <= 12);
		$filterYear = ($input->searchYear > 0);
		if ($filterMonth && $filterYear) {
			// Both filters have been set
			$monthDays = cal_days_in_month(CAL_GREGORIAN, $input->searchMonth, $input->searchYear);
			$monthIndex = str_pad($input->searchMonth, 2, '0', STR_PAD_LEFT);
			$startCheck = $input->searchYear.'-'.$monthIndex.'-01 00:00:00';
			$endCheck = $input->searchYear.'-'.$monthIndex.'-'.$monthDays.' 23:59:59';
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->addBetweenCondition("date_begin", $startCheck, $endCheck);
			$tmpCriteria->addBetweenCondition("date_end", $startCheck, $endCheck, "OR");
			$searchCriteria->mergeWith($tmpCriteria, "AND");
		} elseif ($filterMonth && !$filterYear) {
			// Only month has been set
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->addCondition("MONTH(date_begin) = :search_month", "OR");
			$tmpCriteria->addCondition("MONTH(date_end) = :search_month", "OR");
			$tmpCriteria->params[':search_month'] = $input->searchMonth;
			$searchCriteria->mergeWith($tmpCriteria, "AND");
		} elseif (!$filterMonth && $filterYear) {
			// Only year has been set
			$startCheck = $input->searchYear.'-01-01 00:00:00';
			$endCheck = $input->searchYear.'-12-31 23:59:59';
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->addBetweenCondition("date_begin", $startCheck, $endCheck);
			$tmpCriteria->addBetweenCondition("date_end", $startCheck, $endCheck, "OR");
			$searchCriteria->mergeWith($tmpCriteria, "AND");
		}

		$criteria = new CDbCriteria();

		$criteria->addCondition('course_id = :course_id');
		$criteria->params[':course_id'] = $courseId;

		// Prevent showing ended sessions
		$searchCriteria->join .= " JOIN webinar_session_date dates ON t.id_session=dates.id_session ";
		$searchCriteria->addCondition("DATE_ADD(CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC'), INTERVAL dates.duration_minutes MINUTE) > NOW()");

		if ( ! $showOverbookedSessions ) {
			// show only sessions that are not overbooked
			$criteria->addCondition('t.max_enroll > (
				select count(*)
				from '.WebinarSessionUser::model()->tableName().' cus
				join '.LearningCourseuser::model()->tableName().' as cu on cu.idUser = cus.id_user and cu.idCourse = :course_id
				where cus.id_session = t.id_session)');
		}

		$excludedSessions = array();
		Yii::app()->event->raise('OnCourseSessionsQuery', new DEvent($this, array(
			'excludedSessions' => &$excludedSessions
		)));
		if (!empty($excludedSessions)) {
			$criteria->addNotInCondition('t.id_session', $excludedSessions);
		}

		$criteria->group = 't.id_session';

		$criteria->mergeWith($searchCriteria);
		$config = array(
			'criteria' => $criteria,
			'sort' => array(
				'attributes' => 'date_begin',
				'defaultOrder' => 'date_begin ASC'
			)
		);

		$criteria->select = 't.*';

		$dp = new CActiveDataProvider($this, $config);

		return $dp;

	}

	public function renderDetailedSessionName($separator = '<br/>') {
		$html = $this->name . $separator
			. ucfirst(Yii::t('standard', '_FROM')) . ' ' . self::getStartDate($this->id_session)
			. ' ' . Yii::t('standard', '_TO') . ' ' . self::getEndDate($this->id_session);

		return $html;
	}

	/**
	 * Returns an array containing the number and the percentage of the students who passed the session
	 *
	 * @return array
	 */
	public function getPassStats() {

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.id_session = :id_session');
		$criteria->params[':id_session'] = $this->id_session;
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---
		$sessionModel = WebinarSession::model()->findByPk($this->id_session);
                  $pUserRights = Yii::app()->user->checkPURights($sessionModel->course_id);

		if ($pUserRights->isPu && !$pUserRights->isInstructor)
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//---
		$totalEnrolled = WebinarSessionUser::model()->with(array('sessionModel', 'learningCourseuser'))->count($criteria);

		$criteria = new CDbCriteria();
		$criteria->addCondition('t.id_session = :id_session');
		$criteria->addCondition('t.evaluation_status = :evaluation_status');
		$criteria->params[':id_session'] = $this->id_session;
		$criteria->params[':evaluation_status'] = WebinarSessionUser::EVALUATION_STATUS_PASSED;
		$criteria->addCondition('learningCourseuser.level = '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		//--- power user filter ---
		if ($pUserRights->isPu)
		{
			$puManagedUsers = CoreUserPU::getList();
			$criteria->addInCondition('t.id_user', $puManagedUsers);
		}
		//---
		$countPassed = WebinarSessionUser::model()->with('sessionModel', 'learningCourseuser')->count($criteria);

		if (0 == $totalEnrolled)
			$percentPassed = 0;
		else
			$percentPassed = number_format(doubleval($countPassed / $totalEnrolled) * 100, 1);

		return array(
			'total' => $totalEnrolled,
			'count' => $countPassed,
			'percent' => $percentPassed
		);
	}

	public function isMaxEnrolledReached()
	{
		$criteria = new CDbCriteria();

        $criteria->addCondition('t.id_session = :id_session');
        $criteria->params[':id_session'] = $this->id_session;

		$criteria->addCondition('t.status <> :status');
		$criteria->params[':status'] = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

		$students = WebinarSessionUser::model()->with(array('sessionModel', 'learningCourseuser'))->count($criteria);

        return $students >= $this->max_enroll;
	}

	public function dataProviderWaiting($params = array()) {

		// get assigned users to this session with status 'waiting'
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'userModel' => array(
				'alias' => 'user'
			),
			'sessionModel',
			'learningCourseuser' => array(
				'with' => array(
					'learningUserSubscribedBy'
				),
				'condition' => 'learningCourseuser.idCourse = :cu_idCourse',
				'params' => array(
					':cu_idCourse' => $this->course_id
				)
			)
		);
		$criteria->addCondition('t.id_session = :id_session');
		$criteria->addCondition('t.status = :status');
		$criteria->params[':id_session'] = $this->id_session;
		$criteria->params[':status'] = WebinarSessionUser::$SESSION_USER_WAITING_LIST;

		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%'.$userQuery.'%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%'.$userSearch[0].'%';
		}

		//check if we have to apply filter for power user
		if (isset($params['applyPowerUserFilter']) && $params['applyPowerUserFilter'] && Yii::app()->user->getIsPu()) {
			$criteria->join = " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND pu.user_id = t.id_user) ";
			$criteria->params[':puser_id'] = Yii::app()->user->id;
		}

		$config['criteria'] = $criteria;

		//$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
		$config['pagination'] = array(
			'pageSize' => Settings::get('elements_per_page', 10)
		);

		$dataProvider = new CActiveDataProvider(WebinarSessionUser::model(), $config);

		// manually set the keys array so that the grid selector works
		if ($dataProvider->getTotalItemCount() > 0) {
			$keys = array();
			foreach ($dataProvider->getData() as $i => $data)
				$keys[$i] = $data->id_user;
			$dataProvider->setKeys($keys);
		}

		return $dataProvider;
	}

	/**
	 * Get count of the available enrollments
	 * @return int
	 */
	public function getAvailableEnrollments()
	{
		$enrolled = $this->countEnrolledUsers(false, true);
		return ($enrolled >= $this->max_enroll) ? 0 : ($this->max_enroll - $enrolled);
	}

	/**
	 * Check if enrollments are available for the given session and users number (used from mass enrollment)
	 * @param $sessionId
	 * @param $usersCount
	 * @return bool
	 */
	public static function checkIfEnrollmentsAreAvailable($sessionId, $usersCount)
	{
		$res = false;
		$model = WebinarSession::model()->findByPk($sessionId);
		if ($model)
		{
			$freeEnrollments = $model->getAvailableEnrollments();
			if ($usersCount <= $freeEnrollments)
				$res = true;
		}

		return $res;
	}

	/**
	 * Return a list of instructor(s) for this course session
	 * @param bool $models type of returned data: if true a list of AR models will be returned, otherwise only numeric IDs
	 * @return array list of instructors for this record's user (ARs or numeric IDs)
	 */
	public function findInstructors($models = true) {

		//now retrieve IDSTs of the instructors
		$instructorsIdsts = array();
		$cmd = Yii::app()->db->createCommand()
			->select("cus.id_user")
			->from(WebinarSessionUser::model()->tableName()." cus")
			->join(self::model()->tableName()." cs", "cs.id_session = cus.id_session")
			->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
			->where("cs.course_id = :id_course", array(':id_course' => $this->course_id))
			->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			//->andWhere("cus.id_user <> :id_user", array(':id_user' => $this->id_user))
			->andWhere("cus.id_session = :id_session", array(':id_session' => $this->id_session));
		$reader = $cmd->query();
		if ($reader) {
			while ($row = $reader->read()) {
				$instructorsIdsts[] = $row['id_user'];
			}
		}

		//check if we have any instructor to return
		if (empty($instructorsIdsts)) { return array(); }

		if ($models) {
			//find user records from instructors idsts
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $instructorsIdsts);
			$criteria->order = "firstname ASC, lastname ASC";
			return CoreUser::model()->findAll($criteria);
		} else {
			return $instructorsIdsts;
		}
	}

	public function compileShortcodeDates($textFormat = false) {

		if (!class_exists('NotificationTransportManager')) {
			//make sure to avoid a fatal error if notifications are not active and consequently abova class is not loaded
			$path = Yii::getPathOfAlias('plugin.NotificationApp.components.transports');
			spl_autoload_unregister(array('YiiBase','autoload'));
			require_once($path . DIRECTORY_SEPARATOR . 'NotificationTransportManager.php');
			spl_autoload_register(array('YiiBase','autoload'));
		}
		if (empty($textFormat)) $textFormat = NotificationTransportManager::TEXT_FORMAT_HTML; //TODO: this is just for backward compatibility, but should be removed later

		$sessionDates = '';
		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				$sessionDates .= '<ul>';
				$sessionDates .= '<li>' . Yii::app()->localtime->toLocalDate($this->date) . ' ' . $this->start. ' - ' . Yii::app()->localtime->toLocalDate($this->date) . ' ' . $this->end . '</li>';
				$sessionDates .= '</ul>';
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				$sessionDates .= "\t" . Yii::app()->localtime->toLocalDate($this->date) . ' ' . $this->start. ' - ' . Yii::app()->localtime->toLocalDate($this->date) . ' ' . $this->end . "\n";
				break;
		}

		return $sessionDates;
	}


	/**
	 * Tell if the conference tool is directly joinable in the course or a intermediate LMS link is necessary
	 * @return bool
	 */
	public function isToolDirectJoin($tool = NULL) {
		if(!$tool){
			$tool = $this->tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->isDirectJoin();
	}

	/**
	 * Checks if the tool allows to grab recording via API call
	 * @return bool
	 */
	public function toolAllowsApiGrabRecording($tool = null){
		if(!$tool){
			$tool = $this->tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->allowApiGrabRecording();
	}

	/**
	 * Check if the current tool for the date allows fetching a link for a recorded session video file through API call
	 * @param null|string $tool
	 * @return bool
	 * @throws CHttpException
	 */
	public function toolAllowsApiLinkRecording($tool = null){
		if(!$tool){
			$tool = $this->tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->allowApiFindRecording();
	}

	/**
	 * Check if selected tool only supports external playback
	 * @param null|string $tool
	 * @return bool
	 * @throws CHttpException
	 */
	public function getPlayRecordingType($tool = null){
		if(!$tool){
			$tool = $this->webinar_tool;
		}
		$tool = WebinarTool::getById($tool);
		return $tool->getPlayRecordingType();
	}

	/**
	 * Check if the user is valid for current webinar (depending on the tool used)
	 * @param int|CoreUser|bool $user user IDST or AR (optional, if false then current user will be used)
	 * @param null $toolId
	 * @return string
	 * @throws CHttpException
	 */
	public function validateWebinarUser($user = false, $toolId = null)
	{
		$tool = WebinarTool::getById($toolId ? $toolId : $this->tool);
		if (!$tool) {
			return array(
				'valid' => false,
				'message' => Yii::t('standard', '_OPERATION_FAILURE')
			);
		}
		if (is_object($user) && get_class($user) == 'CoreUser') {
			$userModel = $user;
		} else {
			if (!$user || $user <= 0) { $user = Yii::app()->user->id; } //if no user has been specified, do fallback to current user
			$userModel = CoreUser::model()->findByPk($user);
		}
		if (!$userModel) {
			return array(
				'valid' => false,
				'message' => Yii::t('standard', 'Invalid user')
			);
		}
		return $tool->validateUser($userModel);
	}

	static public function getUserLevelInSession($idSession, $idUser){
		static $caches = array();

		$cacheKey = $idSession.'_'.$idUser;

		if(!isset($caches[$cacheKey])){
			$caches[$cacheKey] = Yii::app()->getDb()->createCommand()
				->select('cu.level')
				->from('webinar_session w')
				->join('learning_courseuser cu', 'w.course_id=cu.idCourse AND cu.idUser=:idUser', array(
					':idUser'=>$idUser
				))
				->where('w.id_session=:idS', array(':idS'=>$idSession))
				->queryScalar();
		}

		return $caches[$cacheKey];
	}

	/**
	 * Get how many minutes in advance can the $idUser see the "Join"
	 * button in Learner View of the session. This is a setting of the
	 * session that can be different for "users" and "teachers"
	 *
	 * @param $idUser
	 * @param $idSession
	 *
	 * @return int minutes in advance when the "Join" button can be seen
	 * @deprecated in favor of WebinarSessionDate::getJoinInAdvanceButtonTimeoutMinutes()
	 */
	static public function getJoinInAdvanceButtonTimeoutMinutes($idUser, $idSession){
		$row = Yii::app()->getDb()->createCommand()
			->select('cu.idCourse, cu.idUser, cu.level, w.join_in_advance_time_user, w.join_in_advance_time_teacher')
			->from('webinar_session w')
			->join('learning_courseuser cu', 'w.course_id=cu.idCourse AND cu.idUser=:idUser', array(
				':idUser'=>$idUser
			))
			->join('webinar_session_user su', 'su.id_user=cu.idUser AND su.id_session=w.id_session')
			->where('w.id_session=:id', array(':id'=>$idSession))
			->queryRow();


		if(!empty($row)){
			$level = $row['level'];

			if($level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
				// Is a teacher or godadmin or PU
				return $row['join_in_advance_time_teacher'];
			}elseif($level==LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
				// Is a learner
				return $row['join_in_advance_time_user'];
			}
		}
		return 0;
	}

	public static function filterOutAccountsWithOverlappingSessions($model, $listData){
		$htmlOptions = array();
		if(!empty($listData) && $model->date && $model->start && $model->duration){
			foreach($listData as $id_tool_account => $account){
				// Check if the room has overlapping dates with an old rooms but only for the Adobe because in the widget only Adobe has accounts
				$model->configureDates();
				$hasOverlapping = ConferenceManager::manageRoomDateOverlapping($model->id_session, $id_tool_account, $model->date_begin, $model->date_end);
				if($hasOverlapping === true)
					$htmlOptions[$id_tool_account] = array('disabled' => true);
			}
		}

		return $htmlOptions;
	}

	public function getRoomsListDataProvider($idCourse, $timeConstrain = self::STATUS_ANYTIME, $skip_pagination = false)
	{
//		$availableConfSystems = ConferenceManager::getAvailableSystems();
		$availableConfSystems = WebinarTool::getAllTools();

		// Get Server time converted to UTC
		$utcNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

		$command = Yii::app()->getDb()->createCommand()
			->select("s.id_session,
			s.course_id,
			s.name,
			dates.webinar_tool as tool,
			dates.day as day,
			CONVERT_TZ(concat(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC') AS date_begin,
			DATE_ADD(CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC'), INTERVAL dates.duration_minutes MINUTE) AS date_end
			")
			->from(WebinarSession::model()->tableName().' s')
			->join(
				WebinarSessionDate::model()->tableName().' dates',
				's.id_session=dates.id_session AND s.course_id=:idCourse'
			)
			->andWhere(array('IN', 'dates.webinar_tool', array_keys($availableConfSystems)))
			->group('s.id_session')
            ->order('date_begin ASC');

		$params = array();

		$params[':idCourse'] = $idCourse;

		// Search for PAST rooms?  (no more joins; it is ended)
		if ($timeConstrain == self::STATUS_HISTORY) {
			$command->andWhere("DATE_ADD(CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC'), INTERVAL dates.duration_minutes MINUTE) < :now");
			$params[':now'] = $utcNow;
		}
		// All rooms not yet finished (or maybe not yet started)
		else if ($timeConstrain == self::STATUS_ACTIVE) {
			$command->andWhere("DATE_ADD(CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC'), INTERVAL dates.duration_minutes MINUTE) >= :now");
			$params[':now'] = $utcNow;
		}
		// Active rooms running NOW?
		else if ($timeConstrain == self::STATUS_ONAIR) {
			$command->andWhere("(CONVERT_TZ(concat(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC') <= :now) AND (DATE_ADD(CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC'), INTERVAL dates.duration_minutes MINUTE) >= :now)");
			$params[':now'] = $utcNow;
		}
		// Active rooms not yet started?
		else if ($timeConstrain == self::STATUS_SCHEDULED) {
			$command->andWhere("CONVERT_TZ(concat(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC') > :now");
			$params[':now'] = $utcNow;
		}

		$commandCount = clone $command;
		$commandCount->selectDistinct('s.id_session');
		$cnt = count($commandCount->queryColumn($params));

		return new CSqlDataProvider($command, array(
			'params'=>$params,
			'totalItemCount'=>$cnt,
		));

		$criteria=new CDbCriteria;
		$criteria->compare('course_id',$idCourse);

		// Get only *available* conf. systems
		$criteria->addInCondition('tool', array_keys($availableConfSystems));

		// Search for PAST rooms?  (no more joins; it is ended)
		if ($timeConstrain == self::STATUS_HISTORY) {
			$criteria->addCondition("date_end < :now");
		}
		// All rooms not yet finished (or maybe not yet started)
		else if ($timeConstrain == self::STATUS_ACTIVE) {
			$criteria->addCondition("date_end >= :now");
		}
		// Active rooms running NOW?
		else if ($timeConstrain == self::STATUS_ONAIR) {
			$criteria->addCondition("(date_begin <= :now) AND (date_end >= :now)");
		}
		// Active rooms not yet started?
		else if ($timeConstrain == self::STATUS_SCHEDULED) {
			$criteria->addCondition("date_begin > :now");
		}

		$criteria->params[':now'] = $utcNow;

		$dateFilter = Yii::app()->request->getParam('videoconf_history_date_filter');
		if ($dateFilter) {
			$starttimeTimestamp = strtotime($dateFilter);
			$endtimeTimestamp = strtotime('+1 day', $starttimeTimestamp);

			$criteria->addCondition("date_begin >= :dateBegin");

			$date = new DateTime();
			$date->setTimestamp($endtimeTimestamp);

			$criteria->addCondition("date_end < :endDate", 'AND');

			$criteria->params[':endDate'] = $date->format('Y-m-d');
			$criteria->params[':dateBegin'] = $dateFilter;
		}

		$provider = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => ($skip_pagination ? false : array(
				'pageSize' => Settings::get('elements_per_page', 10)
			))
		));

		return $provider;
	}

	public function getLauncherLink($moderator = false, $joinRoute = "videoconference/joinMeeting") {

		$result = Yii::t('standard', '_PLANNED');;

		$now = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

		if ( ($now >= $this->date_begin) && ($now <= $this->date_end) || $moderator || (true)) {
			$text = Yii::t('standard', '_ENTER');
			if ($moderator) {
				$text .= " (" . Yii::t('standard', 'as moderator') . ")";
			}
			$url = Yii::app()->createUrl($joinRoute, array("room_id" => $this->id_session));
			$result = "<a target='_new' href='$url'>$text</a>";
		}

		return $result;
	}



	const EVENT_AFTER_SAVE = 'WebinarSessionAfterSave';
	const EVENT_AFTER_DELETE = 'WebinarSessionAfterDelete';

	public function onAfterSave($event)
	{
		parent::onAfterSave($event);
		Yii::app()->event->raise(
			SELF::EVENT_AFTER_SAVE,
			new DEvent($this, [])
		);
	}



	static public function getStartDate($idSession){
		$lowestDate = Yii::app()->getDb()->createCommand()
			->select("CONVERT_TZ(concat(day,' ',time_begin), timezone_begin, 'UTC') AS beginTimestampUTC")
			->from(WebinarSessionDate::model()->tableName())
			->andWhere('id_session=:idsession', array(':idsession'=>$idSession))
			->order("beginTimestampUTC ASC")
			->queryScalar();
		return Yii::app()->localtime->toLocalDateTime($lowestDate);
	}
	static public function getEndDate($idSession){
		$biggestEndDate = Yii::app()->getDb()->createCommand()
			->select("DATE_ADD(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE) AS endtimestampUTC")
			->from(WebinarSessionDate::model()->tableName())
			->andWhere('id_session=:idsession', array(':idsession'=>$idSession))
			->order("endtimestampUTC DESC")
			->queryScalar();
		return Yii::app()->localtime->toLocalDateTime($biggestEndDate);
	}

	static function sessionsSelectGridRenderStatus($status) {
		$css = '';
		switch ($status) {
			case WebinarSessionUser::$SESSION_USER_END:
				$css = 'green';
				break;
			case WebinarSessionUser::$SESSION_USER_BEGIN:
				$css = 'orange';
				break;
			case WebinarSessionUser::$SESSION_USER_SUBSCRIBED:
			default:
				$css = 'grey';
				break;
		}
		$statuses = LtCourseuserSession::model()->getStatusesArray();
		$title = (isset($statuses[$status]) ? $statuses[$status] : '');
		$html = '<span class="i-sprite is-circle-check ' . $css . '"'.($title ? ' title='.CJSON::encode($title).' rel="tooltip"' : '').'></span>';
		return $html;
	}

	/**
	 * @return WebinarSessionDate[]
	 */
	public function getDates(){
		if(!$this->id_session) return array();
		$c = new CDbCriteria();
		$c->select .= ", date_format(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), '%Y-%m-%d %h:%i:%s') AS utcBeginTimestamp, date_format(DATE_ADD(CONVERT_TZ(CONCAT(day,' ',time_begin), timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE), '%Y-%m-%d %h:%i:%s') AS utcEndTimestamp";
		$c->index = 'day';
		$c->compare('id_session', $this->id_session);
		$c->order = "utcBeginTimestamp ASC"; // order ASC by UTC timestamp
		$dates = WebinarSessionDate::model()->findAll($c);
		return $dates;
	}

	public function getSessionDatesAsJson(){
		$dates = $this->getDates();

		$arr = array();
		foreach($dates as $date){
			$elem = $date->attributes;
			if($date->webinar_tool==self::TOOL_CUSTOM){
				$elem['webinar_custom_url'] = $date->webinar_custom_url;
			}
			$arr[$date['day']] = $elem;
		}
		if(!empty($arr)) {
			return CJSON::encode($arr);
		}else{
			return '{}';
		}
	}

	public function getDateAttendanceList(){
		return Yii::app()->getDb()->createCommand()
			->select("d.*, CONVERT_TZ(CONCAT(d.day,' ',d.time_begin), d.timezone_begin, 'UTC') AS utcBeginTimestamp,
				TIMESTAMPDIFF(SECOND, NOW(), CONVERT_TZ(CONCAT(d.day,' ',d.time_begin), d.timezone_begin, 'UTC')) AS remainingTimeToBegin,
				TIMESTAMPDIFF(SECOND, NOW(), DATE_ADD(CONVERT_TZ(CONCAT(d.day,' ',d.time_begin), d.timezone_begin, 'UTC'), INTERVAL duration_minutes MINUTE)) AS remainingTimeToEnd,
				COUNT(da.id_user) AS isAttendedByUser")
			->from(WebinarSessionDate::model()->tableName() . ' d')
			->leftJoin(WebinarSessionDateAttendance::model()->tableName() . ' da', 'd.id_session=da.id_session AND d.day=da.day AND da.id_user=:idUser', array(
				':idUser' => Yii::app()->user->getIdst()
			))
			->where('d.id_session=:idSession', array(':idSession' => $this->id_session))
			->group('d.day')
			->order('utcBeginTimestamp ASC')// order ASC by UTC timestamp
			->queryAll();
	}


	/**
	 *
	 * @param $userId
	 * @param timestamp $start - In UTC timezone
	 * @param timestamp $end - In UTC timezone
	 *
	 * @return WebinarSession webinars
	 */
	public static function getWebinar($filter, $userId, $infinity = false, $calendarView = false, $start = null, $end = null, $limit = 0){

		$result = array();

		$rows = self::getWebinarsForPeriod($filter, $userId, $infinity, $calendarView, $start, $end, $limit);

		foreach($rows as $course)
		{
			$startTimestamp = strtotime($course['dateBegin']);
			$endTimestamp = $startTimestamp + ($course['duration_minutes'] * 60);

			$result[] = array(
				'id' => $course['idCourse'] . '_0_' . $course['day'] . '_' . $course['id_session'] . '_0',
				'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
				'start' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $startTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', "GMT"),
				'end' => Yii::app()->localtime->toLocalDateTime(date("Y-m-d H:i:s", $endTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', "GMT"),
				'allDay' => false
			);
		}

		if(!$calendarView){

			//Then Webinars
			$start_date = date('Y-m-d', Yii::app()->request->getParam('start', time())).' 00:00:00';
			$end_date = date('Y-m-d', Yii::app()->request->getParam('end', time())).' 23:59:59';

			$command = Yii::app()->db->createCommand();
			$command->select('cs.course_id, cs.id_session, CONVERT_TZ( CONCAT_WS(" ", wsd.day, wsd.time_begin), wsd.timezone_begin, :timezone) as dateBegin, wsd.day, wsd.duration_minutes, wsd.name');
			$command->from(WebinarSession::model()->tableName().' cs');
			$command->join(WebinarSessionUser::model()->tableName().' cus', 'cs.id_session = cus.id_session');
			$command->join(WebinarSessionDate::model()->tableName().' wsd', 'cs.id_session = wsd.id_session');
			$command->where('cus.id_user = :id_user');
			$command->params[':id_user'] = $userId;
			$command->andWhere('CONVERT_TZ(cs.date_begin, "GMT", :timezone) BETWEEN :start_date AND :end_date');
			$command->params[':start_date'] = Yii::app()->localtime->toUTC($start_date);
			$command->params[':end_date'] = Yii::app()->localtime->toUTC($end_date);
			$command->params[':timezone'] = Yii::app()->localtime->getTimeZone();

			//Apply text filter if needed
			if($filter != '')
			{
				$command->andWhere('wsd.name LIKE :name');
				$command->params[':name'] = '%'.$filter.'%';
			}
			$rows = $command->queryAll();

			foreach($rows as $course)
			{
				$startTimestamp = strtotime($course['dateBegin']);
				$endTimestamp = $startTimestamp + ($course['duration_minutes'] * 60);

				$result[] = array(
					'id' => $course['course_id'].'_0_'.str_replace('-', '', $course['day']).'_'.$course['id_session'].'_0',
					'title' => (strlen($course['name']) > 23 ? substr($course['name'], 0, 23).'...' : $course['name']),
					'start' => $course['dateBegin'],
					'end' => $endTimestamp,
					'allDay' => false
				);
			}
		}

		return $result;
	}

	public static function getWebinarsForPeriod($filter, $userId, $infinity = false, $calendarView = false, $start = null, $end = null, $limit = 0){
		//Add first the Web Conferences that are part of the Web Conference widget!!!
		$command = Yii::app()->db->createCommand();
		$command->select('c.idCourse, ws.id_session, ws.name, wsd.day, wsd.time_begin, wsd.duration_minutes, wsd.timezone_begin, c.course_type,
		 		CONVERT_TZ( CONCAT_WS(" ", wsd.day, wsd.time_begin), wsd.timezone_begin, "GMT") as dateBegin');
		$command->from(LearningCourse::model()->tableName().' c');
		$command->join(LearningCourseuser::model()->tableName().' cu', 'c.idCourse = cu.idCourse');
		$command->join(WebinarSession::model()->tableName().' ws', 'c.idCourse = ws.course_id');
		$command->join(WebinarSessionDate::model()->tableName().' wsd', 'ws.id_session = wsd.id_session');
		$command->leftJoin(WebinarSessionUser::model()->tableName().' cus', 'ws.id_session = cus.id_session');

		$command->where('cu.idUser = :id_user');
		$command->andWhere('cus.id_user = :id_user OR (cus.id_user IS NULL  AND c.course_type !="webinar")');
		$command->params[':id_user'] = $userId;

		if(!$infinity){
			if (isset($start) && isset($end)) {
				$command->having('dateBegin BETWEEN FROM_UNIXTIME(:start_date) AND FROM_UNIXTIME(:end_date)');
				$command->params[':start_date'] = $start;
				$command->params[':end_date'] = $end;
			}
			else {
				$command->andWhere('wsd.day > NOW()');
			}
		}

		//Apply text filter if needed
		if($filter != '')
		{
			$command->andWhere('ws.name LIKE :name');
			$command->params[':name'] = '%'.$filter.'%';
		}

		if($limit>0)
			$command->limit($limit);

		return $command->queryAll();
	}

	public function getIcsDatesData($name, $description, $location = null, $organizerName = null, $organizerEmail = null, $type){
		$icsDatesData = '';
		$dates = $this->getDates();
		if($dates){
			foreach($dates as $date){
				$startDateTmp = $date->day . ' '. $date->time_begin. ':00';

				$timezoneOffset = Yii::app()->localtime->getTimezoneOffsetSecondsFromUTC($date->timezone_begin);
				$startTimestamp = strtotime($startDateTmp) - $timezoneOffset;

				$endTimestamp = $startTimestamp + ($date->duration_minutes*60);

				$startDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $startTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', $date->timezone_begin);
				$endDateUTC = Yii::app()->localtime->toLocalDateTime(date('Y-m-d H:i:s+00:00', $endTimestamp), LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s', $date->timezone_begin);

				$dateBeginUTC = Yii::app()->localtime->toUTC($startDateUTC, 'Ymd\THis\Z'); // 'Ymd\THis\Z' format for .ics file
				$dateEndUTC = Yii::app()->localtime->toUTC($endDateUTC, 'Ymd\THis\Z');

				$uid = $this->getPrimaryKey().'_'.$date->day;
				if($type == ICSManager::TYPE_CANCEL){
					$status = "STATUS:CANCELLED\r\n";
				}else{
					$status = "";
				}

				// Some checks for Organizer name and email if they are missing
				$organizer = "ORGANIZER;CN=";
				if($organizerName && $organizerEmail) {
					$organizer .= "{$organizerName}:MAILTO:{$organizerEmail}\r\n";
				} elseif($organizerName && !$organizerEmail) {
					$organizer .= "{$organizerName}\r\n";
				} elseif(!$organizerName && $organizerEmail) {
					$organizer .= "{$organizerEmail}\r\n";
				} else {
					$organizer = "";
				}

				$summary = Yii::t('standard', '_COURSE') . ': ' . $this->course->name . ', ' . Yii::t('classroom', 'Session') . ': ' . $name;
				$icsDatesData .=   "BEGIN:VEVENT\r\n"
					."DTSTART:" . $dateBeginUTC . "\r\n"
					."DTEND:" . $dateEndUTC . "\r\n"
					."FREEBUSY;FBTYPE=BUSY:" . $dateBeginUTC . "/" . $dateEndUTC . "\r\n"
					."TRANSP: OPAQUE\r\n"
					."SEQUENCE:0\r\n"
					."UID:".$uid."\r\n"
					.$status
					."DTSTAMP:".date("Ymd\THis\Z")."\r\n"
					."SUMMARY:".$summary."\r\n"
					."DESCRIPTION:".$description."\r\n"
					."PRIORITY:1\r\n"
					//."ORGANIZER;CN=\"{$organizerName}\":{$organizerEmail}\r\n"
					.$organizer
					."CLASS:PUBLIC\r\n"
					."X-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\n"
					."X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\n"
					."X-MICROSOFT-CDO-IMPORTANCE:1\r\n"
					."BEGIN:VALARM\r\n"
					."TRIGGER:-PT15M\r\n" // reminder 60 minutes before the event
					."ACTION:DISPLAY\r\n"
					."DESCRIPTION:Reminder\r\n"
					."END:VALARM\r\n"
					."END:VEVENT\r\n";
			}
		}

		return $icsDatesData;
	}


	/**
	 * Check if any recording object is available in any session date for this session
	 * @return bool
	 */
	public function hasRecordings() {
		$query = "SELECT COUNT(*) "
			." FROM ".WebinarSessionDate::model()->tableName()." wsd "
			." JOIN ".WebinarSessionDateRecording::model()->tableName()." wsdr "
			." ON (wsd.id_session = wsdr.id_session AND wsd.day = wsdr.day AND wsd.id_session = :id_session)";
		$rs = $cmd = Yii::app()->db->createCommand($query)->queryScalar(array(':id_session' => $this->id_session));
		return ($rs > 0);
	}

	public function dataProviderUserWaiting($params = array()){
		$criteria = new CDbCriteria();

		$criteria->with = array(
			'sessionModel' => array(
				'condition' => 'sessionModel.course_id = :courseId',
			),
			'userModel' => array(
				'alias' => 'user'
			)
		);


		// find users for the autocomplete dropdown
		if (!empty($params['user_query'])) {
			$userQuery = addcslashes(trim($params['user_query']), '%_');
			$criteria->addCondition('CONCAT(user.firstname, " ", user.lastname) LIKE :user_query OR user.email LIKE :user_query OR user.userid LIKE :user_query');
			$criteria->params[':user_query'] = '%' . $userQuery . '%';
		}

		// filter users by the value we get from autocomplete
		if (!empty($params['user_search'])) {
			$userSearch = explode(' ', $params['user_search']);
			$criteria->addCondition("user.firstname LIKE :user_search OR user.lastname LIKE :user_search OR user.email LIKE :user_search OR user.userid LIKE :user_search");
			$criteria->params[':user_search'] = '%' . $userSearch[0] . '%';
		}




		if (isset($params['applyPowerUserFilter']) && $params['applyPowerUserFilter'] && Yii::app()->user->getIsPu()) {
			$criteria->join = " JOIN ".CoreUserPU::model()->tableName()." pu ON (pu.puser_id = :puser_id AND pu.user_id = t.id_user) ";
			$criteria->params[':puser_id'] = Yii::app()->user->id;

			$PU_hideOtherSessions = false;
			if(Yii::app()->user->isPu) {
				$pUserRights = Yii::app()->user->checkPURights($params['courseId']);
				$isPuWithSessionEditPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/webinarsessions/mod");
				if(!$isPuWithSessionEditPerm) {
					$PU_hideOtherSessions = true;
				}
			}

			if($PU_hideOtherSessions){
				$criteria->addCondition("created_by = :created_by");
				$criteria->params['created_by'] =  Yii::app()->user->id;
			}
		}



		$criteria->together = true;
		$criteria->params['courseId'] = $params['courseId'];
		$criteria->params['waitingStatus'] = WebinarSession::$SESSION_USER_WAITING_LIST;
		$criteria->addCondition('(t.waiting = 1 OR t.status = :waitingStatus)');

		$config['criteria'] = $criteria;
		$config['sort']['defaultOrder'] = 'user.userid ASC';
		$dataProvider = new CActiveDataProvider(WebinarSessionUser::model(), $config);

		return $dataProvider;
	}

	/**
	 * @param $idSession
	 * @param $idUser
	 * @return bool
	 */
	public static function isUserCreator($idSession, $idUser){
		return ($idSession && $idUser)? (boolean)(Yii::app()->getDb()->createCommand()
				->select("*")
				->from(self::model()->tableName())
				->where("id_session = :idSession AND created_by = :idUser",array(
					":idSession" => $idSession,
					":idUser" => $idUser
				))->queryRow()) : false;
	}

}
