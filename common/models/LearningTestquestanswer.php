<?php

/**
 * This is the model class for table "learning_testquestanswer".
 *
 * The followings are the available columns in table 'learning_testquestanswer':
 * @property integer $idAnswer
 * @property integer $idQuest
 * @property integer $sequence
 * @property integer $is_correct
 * @property string $answer
 * @property string $comment
 * @property double $score_correct
 * @property double $score_incorrect
 * @property string $settings
 */
class LearningTestquestanswer extends AuditActiveRecord
{
	const SCORE_CORRECT = 1;

	/**
	 * Intercept "get property" to account for wrong use of $this->id_answer instead $this->idAnswer
	 * @param string $name
	 * @return array|mixed|null
	 */
	public function __get($name) {
		if($name == 'id_answer')
			$name = 'idAnswer';

		return parent::__get($name);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTestquestanswer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testquestanswer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('answer', 'required'),
			array('idQuest, sequence, is_correct', 'numerical', 'integerOnly'=>true),
			array('score_correct, score_incorrect', 'numerical'),
			array('settings', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idAnswer, idQuest, sequence, is_correct, answer, comment, score_correct, score_incorrect', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idAnswer' => 'Id Answer',
			'idQuest' => 'Id Quest',
			'sequence' => 'Sequence',
			'is_correct' => 'Is Correct',
			'answer' => 'Answer',
			'comment' => '_COMMENTS',
			'score_correct' => 'Score Correct',
			'score_incorrect' => 'Score Incorrect',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idAnswer',$this->idAnswer);
		$criteria->compare('idQuest',$this->idQuest);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('is_correct',$this->is_correct);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('score_correct',$this->score_correct);
		$criteria->compare('score_incorrect',$this->score_incorrect);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	public function init () {
		parent::init();

		if(!$this->settings){
			$this->settings = array();
		}
	}

	protected function afterFind () {
		parent::afterFind();
		$this->settings = is_string($this->settings) ? (array) CJSON::decode($this->settings, 1) : $this->settings;
	}

	protected function beforeSave () {
		$this->settings = is_array($this->settings) ? CJSON::encode($this->settings) : $this->settings;
		return parent::beforeSave();
	}

	public function afterSave()
	{
		// When answer is updated and score_correct is updated as well - recalculate all tests score_max related to this answer
		if ($this->hasAttributeChanged('score_correct'))
		{
			$quest = LearningTestquest::model()->findByPk($this->idQuest);
			if ($quest && $quest->getTestCount() > 0)
			{
				LearningTest::updateScoreMaxByQuestion($quest->primaryKey);
			}
		}
		parent::afterSave();
	}
}