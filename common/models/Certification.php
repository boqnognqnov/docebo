<?php

/**
 * This is the model class for table "certification".
 *
 * The followings are the available columns in table 'certification':
 * @property integer $id_cert
 * @property string $title
 * @property string $description
 * @property integer $duration
 * @property integer $allow_same_item
 * @property string $duration_unit
 *
 * The followings are the available model relations:
 * @property CertificationItem[] $certificationItems
 */
class Certification extends CActiveRecord
{

	const DURATION_UNIT_DAY 	= 'day';
	const DURATION_UNIT_WEEK	= 'week';
	const DURATION_UNIT_MONTH 	= 'month';
	const DURATION_UNIT_YEAR 	= 'year';


	public static $CERTIFICATION_SOFT_DELETED = 1;
	public $search_input = null;
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'certification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, duration, duration_unit', 'required'),
			array('duration, allow_same_item, deleted', 'numerical', 'integerOnly'=>true),

			array('title', 'safe'),
			array('title', 'length', 'min'=>3, 'max'=>255),

			array('duration_unit', 'length', 'max'=>16),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_cert, title, description, duration, allow_same_item, duration_unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificationItems' => array(self::HAS_MANY, 'CertificationItem', 'id_cert'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cert' => 'Id Cert',
			'title' => Yii::t('standard', '_NAME'),
			'description' => Yii::t('standard', '_DESCRIPTION'),
			'duration' => Yii::t('standard', 'Expiration (0 for never)'),
			'allow_same_item' => Yii::t('certification', 'Allow users to retake the same course or learning plan to renew their certification. <strong>Every single user\'s previous tracking will be reset as soon as the certification is renewed.</strong>'),
			'duration_unit' => 'Duration Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_cert',$this->id_cert);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('allow_same_item',$this->allow_same_item);
		$criteria->compare('duration_unit',$this->duration_unit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Certification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	/**
	 *
	 * @return CSqlDataProvider
	 */
	public function sqlDataProvider($all = false) {

		// SQL Parameters
		$params = array();

		// BASE
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->from('certification cert');

		// Also search by user related text
		if ($this->search_input != null) {
			$commandBase->andWhere('CONCAT(cert.title, " ", cert.description) LIKE :search');
			$params[':search'] = '%'.$this->search_input.'%';
		}
		$commandBase->andWhere('cert.deleted = 0');

		// DATA
		$commandData = clone $commandBase;


		// COUNTER
		$commandCounter = clone $commandBase;
		$commandCounter->select('count(cert.id_cert)');
		$numRecords = $commandCounter->queryScalar($params);


		// ORDERING
		$commandData->order = "";
		$sort = new CSort();
		$sort->attributes = array(
			'name' => array(
				'asc' 	=> 'cert.title',
				'desc'	=> 'cert.title DESC',
			),
			'description' => array(
				'asc' 	=> 'cert.description',
				'desc'	=> 'cert.description DESC',
			),
		);
		$sort->defaultOrder = array(
			'name'		=>	CSort::SORT_ASC,
		);

		// DATA-II
		$commandData->select("*");


		// END
		$pageSize = Settings::get('elements_per_page', 20);
		$config = array(
			'totalItemCount'	=>	$numRecords,
			'pagination' 		=> array(
				'pageSize' => $all ? $numRecords : $pageSize
			),
			'keyField'			=> 'id_cert',
			'sort' 				=> $sort,
			'params'			=> $params,
		);

		// Pass dbCommand, not SQL text, to apply parameters binding !!!
		$dataProvider = new CSqlDataProvider($commandData, $config);

		return $dataProvider;

	}


	/**
	 * List of expiration types labels (value => label)
	 * @return array
	 */
	public static function expirationDurationUnits() {
		$result = array(
			self::DURATION_UNIT_DAY 	=> Yii::t('standard', '_DAYS'),
			self::DURATION_UNIT_WEEK 	=> Yii::t('standard', 'Weeks'),
			self::DURATION_UNIT_MONTH 	=> Yii::t('standard', 'Months'),
			self::DURATION_UNIT_YEAR 	=> Yii::t('standard', 'Years')
		);
		return $result;
	}


	/**
	 * Get assigned items information for THIS certification
	 *
	 * @param number $id
	 * @return array of data
	 */
	public function getAssignmentInfo($idCert=false) {

		$idCert = $idCert ? $idCert : $this->id_cert;

		$courses = array();
		$plans = array();
		$transcripts = array();

		$plansCourses = array();

		$command = Yii::app()->db->createCommand()
			->select('*')
			->from(CertificationItem::model()->tableName() . ' t')
			->where('id_cert=:id_cert');

		$params = array(
			':id_cert' => (int) $idCert,
		);
		$reader = $command->query($params);

		foreach ($reader as $item) {
			if ($item['item_type'] == CertificationItem::TYPE_COURSE) {
				$courses[] = $item['id_item'];
			}
			else if ($item['item_type'] == CertificationItem::TYPE_TRANSCRIPT) {
				$transcripts[] = $item['id_item'];
			}
			else if ($item['item_type'] == CertificationItem::TYPE_LEARNING_PLAN ) {
				$plans[] = $item['id_item']; // count plans
				$planModel = LearningCoursepath::model()->findByPk($item['id_item']);
				if ($planModel) {
					$thisPlanCourses = $planModel->getCoursesIdList();
					$plansCourses = array_merge($plansCourses, $thisPlanCourses);
				}
			}
		}

		$result = array(
			'courses' => $courses,
			'plans' => $plans,
			'transcripts' => $transcripts,
			'plansCourses' => $plansCourses,
		);

		return $result;

	}

	/**
	 * Check if a list of certifications are issued to a list of users, or to ANY one at all.
	 * Just let us know if any of the certification is issued to any of the user!
	 *
	 * @param array $certifications
	 * @param array $users
	 */
	public static function isIssuedTo($certifications=array(), $users=array(), $includeArchived=false) {

		// SQL Parameters
		$params = array();

		$command = Yii::app()->db->createCommand();
		$command->from('certification cert');
		$command->join('certification_item ci', 'cert.id_cert=ci.id_cert');
		$command->join('certification_user cu', 'ci.id=cu.id_cert_item');

		if (!empty($users)) {
			$command->andWhere('cu.id_user IN (' . implode(',', $users) . ')');
		}

		if (!empty($certifications)) {
			$command->andWhere('cert.id_cert IN (' . implode(',', $certifications) . ')');
		}

		// Ignore "archived"?
		if (!$includeArchived) {
			$command->andWhere('cu.archived=0');
		}

		$command->select('count(cert.id_cert)');

		return (int) $command->queryScalar($params) > 0;


	}



	/**
	 * Return FALSE, if the specified learning item is NOT used by ANY issued user ceritifcation so far
	 * OR an arry of certification IDs if otherwise
	 *
	 * @param integer $idLearningItem  (course ID, plan ID, ...)
	 * @param string $itemType (course, plan, ...)
	 * @param integer $idCert
	 * @param integer $idUser
	 * @param boolean $includeArchived
	 * @return boolean|multitype:mixed
	 */
	public static function certificationsUsedLearningItemToCertifyUser($idLearningItem, $itemType, $idUser, $includeArchived=true) {

		// SQL Parameters
		$params = array();

		$command = Yii::app()->getDb()->createCommand();
		$command->from('certification_item ci');
		$command->join('certification_user cu', 'ci.id=cu.id_cert_item');

		$command->	 where('ci.item_type=:item_type');
		$command->andWhere('ci.id_item=:id_item');
		$command->andWhere('cu.id_user=:id_user');

		if (!$includeArchived) {
			$command->andWhere('cu.archived=0');
		}

		$params[':item_type'] 	= $itemType;
		$params[':id_item'] 	= (int) $idLearningItem;
		$params[':id_user'] 	= (int) $idUser;

		$command->select('ci.id_cert AS idCert');

		$reader = $command->query($params);
		$certs = array();
		foreach ($reader as $row) {
			$certs[] = $row['idCert'];
		}

		if (empty($certs))
			return false;

		return $certs;

	}

	/**
	 * Gets the exact certificate expiration date
	 * (in local time zone; using the passed id user and course)
	 *
	 * The expiration of a certification is usually represented
	 * by the course completion date + an arbitrary (configurable)
	 * expiration period
	 *
	 * @param $idUser
	 * @param $idCourse
	 *
	 * @return string
	 */
	static public function getCertificationExpiration($idUser, $idCourse, $asUTC = false){
		$criteria = new CDbCriteria();
		$criteria->select = 'date_complete, status';
		$criteria->compare('idUser', $idUser);
		$criteria->compare('idCourse', $idCourse);
		$courseUser = LearningCourseuser::model()->find($criteria);

		if(!$courseUser || $courseUser->status!=LearningCourseuser::$COURSE_USER_END){
			// Course not yet completed or not started
			return;
		}

		$local_format = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'medium'));

		$query="SELECT c.`duration`,c.`duration_unit` FROM
				certification c
				JOIN certification_item ci
				ON c.id_cert = ci.id_cert
				JOIN learning_certificate_course lcc
				ON lcc.id_course = ci.id_item
				JOIN learning_certificate lc
				ON lc.id_certificate = lcc.id_certificate
				WHERE ci.item_type = 'course' AND lcc.`id_course`=:courseId";
		$certificationDurationData = Yii::app()->db->createCommand($query)->queryRow(true, array(':courseId' => $idCourse));

		$expirationDate = null;
		if(!empty($certificationDurationData)) {
			$certificationExpirationPlusOffset = date( 'Y-m-d', strtotime( Yii::app()->localtime->convertDateString( $courseUser->date_complete, $local_format, DATE_ISO8601 ) . "+ " . $certificationDurationData['duration'] . " " . $certificationDurationData['duration_unit'] . "s" ) ) ;

			if($asUTC){
				$expirationDate = $certificationExpirationPlusOffset;
			}else{
				// Convert the UTC date to local time zone
				$expirationDate = Yii::app()->localtime->toLocalDate( $certificationExpirationPlusOffset);
			}
		}
		return $expirationDate;
	}


}
