<?php

/**
 * This is the model class for table "learning_course_coaching_messages".
 *
 * The followings are the available columns in table 'learning_course_coaching_messages':
 * @property integer $idMessage
 * @property integer $idSession
 * @property integer $idCoach
 * @property integer $idLearner
 * @property string $message
 * @property integer $status
 * @property integer $sent_by_coach
 * @property string $date_added
 * @property string $date_edited
 *
 * The followings are the available model relations:
 * @property CoreUser $coach
 * @property CoreUser $learner
 * @property LearningCourseCoachingSession $session
 */
class LearningCourseCoachingMessages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course_coaching_messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idSession, idCoach, idLearner, message', 'required'),
			array('idSession, idCoach, idLearner, status, sent_by_coach', 'numerical', 'integerOnly'=>true),
			array('date_edited', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idMessage, idSession, idCoach, idLearner, message, status, sent_by_coach, date_added, date_edited', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coach' => array(self::BELONGS_TO, 'CoreUser', 'idCoach'),
			'learner' => array(self::BELONGS_TO, 'CoreUser', 'idLearner'),
			'session' => array(self::BELONGS_TO, 'LearningCourseCoachingSession', 'idSession'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idMessage' => 'Id Message',
			'idSession' => 'Session',
			'idCoach' => Yii::t('coaching', 'Coach'),
			'idLearner' => Yii::t('levels', '_LEVEL_3'),
			'message' => Yii::t('htmlframechat', '_MSGTXT'),
			'status' => Yii::t('standard', 'Status'),
			'sent_by_coach' => Yii::t('coaching', 'Sent By') . ' ' . Yii::t('coaching', 'Coach'),
			'date_added' => Yii::t('standard', '_DATE'),
			'date_edited' => Yii::t('standard', 'Date Modified'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idMessage',$this->idMessage);
		$criteria->compare('idSession',$this->idSession);
		$criteria->compare('idCoach',$this->idCoach);
		$criteria->compare('idLearner',$this->idLearner);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('date_edited',$this->date_edited,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LearningCourseCoachingMessages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * getUserMessages() - Get the user messages by passed params
	 * @param bool $idCourse id of a course
	 * @param int $enrollmentType - learner(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT 3) or coach(LearningCourseuser::$USER_SUBSCR_LEVEL_COACH 8)
	 * @param bool $idLearner id of a learner
	 * @param bool $idSession id of a coaching session
	 * @param bool $idCoach id of a coach
	 * @param bool $status add status to filter by messages - unread(0) or read (1)
	 * @return array of messages between coach and learner of a coaching session !!!
	 */
	public function getUserMessages($idCourse = false, $enrollmentType = -1, $idLearner = false, $idSession = false, $idCoach = false, $status = false) {
		// if no enrollment type is passed use by default 'coach' level
		if ($enrollmentType == -1) {
			$enrollmentType = LearningCourseuser::$USER_SUBSCR_LEVEL_COACH;
		}

		// if no id of a learner is passed get current user id and we need to get a learners messages
		if (!$idLearner && $enrollmentType = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) {
			$idLearner = Yii::app()->user->id;
		}

		// if no id of a coach is passed get current user id and we need to get a coach messages
		if (!$idLearner && $enrollmentType = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) {
			$idCoach = Yii::app()->user->id;
		}

		// Get user coaching messages base command
		$command = Yii::app()->db->createCommand()
			->select('uccm.*')
			->from('learning_course_coaching_messages uccm');

		// If we are getting messages of a learner
		if ($enrollmentType == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT) {
			// If idSession and idCoach are not passed(false), try to get learner active session
			if (!$idSession && !$idCoach && $idCourse != false && $idLearner) {
				// Try to get learners active coaching session
				$learnerActiveSessions = LearningCourseCoachingSessionUser::model()->getUserActiveCoachingSessions($idLearner, $idCourse);
				$learnerActiveSession = (is_array($learnerActiveSessions) && count($learnerActiveSessions) > 0) ? $learnerActiveSessions[0] : false;

				// If there is an active user session set its id
				if ($learnerActiveSessions) {
					$idSession = $learnerActiveSession['idSession'];
					$idCoach = $learnerActiveSession['idCoach'];
				} else {
					// Return no messages found
					return array();
				}
			} // end of find learner session and coach

			// If there is no session found and set above or passed return empty array
			if (!$idSession && !$idCoach) {
				// There are no user coaching sessions details passed and we cannot determine correctly the user messages
				return array();
			}
		} // End of learner determined part

		// If we are getting messages of a coach
		if ($idCoach && $idLearner && $idSession) {
			// Add Filter by id of a learner
			$command->where('uccm.idLearner =' . $idLearner);

			// Add filter by id of a session
			$command->andWhere('uccm.idSession =' . $idSession);

			// Add filter by id of a coach
			$command->andWhere('uccm.idCoach =' . $idCoach);

			// If there is filter by status set
			if ($status !== false) { // 0 or 1 passed as status will filter by status (unread/read messages)
				$command->andWhere('uccm.status =' . $status);
			}

			// Order by message date added !!!
			$command->order('uccm.date_added ASC');

			// Get and return messages
			return $command->queryAll();
		} else {
			// There are not enough parameters passed to correctly filter messages
			return array();
		}
	}

	/**
	 * getUserLastMessage() get last message by learner and session passed
	 * @param $idLearner
	 * @param $idSession
	 * @return mixed
	 */
	public function getUserLastMessage($idLearner, $idSession) {
		// Get user last message
		$lastMessage = Yii::app()->db->createCommand()
			->select('uccm.*')
			->from('learning_course_coaching_messages uccm')
			->where('idSession=:idSession', array(':idSession' => $idSession))
			->andWhere('idLearner=:idLearner', array(':idLearner' => $idLearner))
			->order('date_added DESC')
			->queryRow();

		return $lastMessage;
	}

	/**
	 * getUserUnreadMessages() get unread messages by learner and session passed
	 * @param $idLearner
	 * @param $idSession
	 * @return mixed
	 */
	public function getUserUnreadMessages($idLearner, $idSession, $unreadByCoach = 1) {
		// Get user last message
		$getCount = Yii::app()->db->createCommand()
			->select('count(*) AS count')
			->from('learning_course_coaching_messages')
			->where('idSession=:idSession', array(':idSession' => $idSession))
			->andWhere('idLearner=:idLearner', array(':idLearner' => $idLearner))
			->andWhere('sent_by_coach=:sent_by_coach', array(':sent_by_coach' => ($unreadByCoach == 1) ? 0 : 1 ))
			->andWhere('status=0')
			->queryRow();

		// Verify we always return a number
		if(is_array($getCount) && count($getCount) > 0) {
			$getCount = !empty($getCount['count']) ? intval($getCount['count']) : 0;
		}

		return $getCount;
	}
}
