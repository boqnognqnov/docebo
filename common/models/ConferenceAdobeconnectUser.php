<?php

/**
 * This is the model class for table "conference_adobeconnect_user".
 *
 * The followings are the available columns in table 'conference_adobeconnect_user':
 * @property string $id_user
 * @property string $email
 * @property string $pwd
 * @property string $login
 * @property string $display_uid
 * @property string $name
 */
class ConferenceAdobeconnectUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConferenceAdobeconnectUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conference_adobeconnect_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, email, pwd, login, display_uid, name', 'required'),
			array('id_user', 'length', 'max'=>10),
			array('email, pwd, login, display_uid, name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_user, email, pwd, login, display_uid, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'email' => 'Email',
			'pwd' => 'Pwd',
			'login' => 'Login',
			'display_uid' => 'Display Uid',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pwd',$this->pwd,true);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('display_uid',$this->display_uid,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page')
            )
		));
	}
}