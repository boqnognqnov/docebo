<?php
 
/**
 * Right now this class only handles file uploads from the Additional Fields,
 * but we may find other uses for it. 
 */
class AdditionalFieldFile extends CFormModel {
 
    public $uploadedFile;
 
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            //note you wont need a safe rule here
            array('uploadedFile', 'file', 'allowEmpty' => false, 'types' => 'zip,doc,xls,ppt,jpg,gif,png,txt,docx,pptx,ppsx,xlsx,pdf,csv,bmp'), // types taken from /lib/lib.upload.php->sl_upload()
        );
    }
    
    
    /**
     * Return an HTML of SingleFile widget (pluploader based upload field)
     * @param string $fieldName
     * @param string $fieldId
     * @return string
     */
    public function getPluploaderHtml($fieldName, $fieldId) {
    	
    	$html = Yii::app()->controller->widget('common.extensions.plupload.widgets.SingleFile', array(
			'maxFileSize' => 1000,
			'fieldName' => $fieldName,
    		'fieldId' => $fieldId,	
		), true);
    
    	
    	return $html;
    }
    
 
}