<?php

/**
 * This is the model class for table "core_user_temp".
 *
 * The followings are the available columns in table 'core_user_temp':
 * @property integer $idst
 * @property string $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $pass
 * @property string $email
 * @property string $language
 * @property string $request_on
 * @property string $random_code
 * @property integer $create_by_admin
 * @property integer $confirmed
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $linkedin_id
 * @property string $google_id
 */
class CoreUserTemp extends CActiveRecord
{

	const STATUS_UNCONFIRMED = 0;
	const STATUS_CONFIRMED = 1;

	public $search_input;

	public $passwordPlainText = false;

	public $confirm;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUserTemp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user_temp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idst, create_by_admin, confirmed, self_subscribe_course', 'numerical', 'integerOnly'=>true),
			array('userid, email, random_code, facebook_id, twitter_id, linkedin_id, google_id', 'length', 'max'=>255),
			array('firstname, lastname', 'length', 'max'=>100),
			array('language', 'length', 'max'=>50),
			array('pass', 'length', 'max'=>64),
			array('request_on', 'safe'),
			array('userid', 'uniqueUserid'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, userid, firstname, lastname, pass, email, language, request_on, random_code, create_by_admin, confirmed, facebook_id, twitter_id, linkedin_id, google_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('request_on medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst' => 'ID',
			'userid' => Yii::t('standard', '_USERNAME'),
			'firstname' => Yii::t('standard', '_FIRSTNAME'),
			'lastname' => Yii::t('standard', '_LASTNAME'),
			'pass' => Yii::t('standard', '_PASSWORD'),
			'email' => Yii::t('standard', '_EMAIL'),
			'language' => Yii::t('standard', '_LANGUAGE'),
			'request_on' => Yii::t('admin_directory', '_DIRECTORY_FILTER_register_date'),
			'random_code' => 'Random code',
			'create_by_admin' => Yii::t('report', '_TAB_REP_CREATOR'),
			'confirmed' => Yii::t('course', '_T_USER_STATUS_CONFIRMED'),
			'facebook_id' => Yii::t('standard', '_FACEBOOK'),
			'twitter_id' => Yii::t('standard', '_TWITTER'),
			'linkedin_id' => Yii::t('standard', '_LINKEDIN'),
			'google_id' => Yii::t('standard', '_GOOGLE'),
		);
	}

	public function scopes() {
		return array(
			'unconfirmed' => array(
				'condition' => $this->getTableAlias(false, false) . '.confirmed = :status',
				'params' => array(
					':status' => CoreUserTemp::STATUS_UNCONFIRMED,
				),
			),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		if (Yii::app()->user->getIsPu()) {
			$criteria->compare('idst', CoreUserPU::getList(Yii::app()->user->id));
		} else {
			$criteria->compare('idst', $this->idst);
		}

		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('request_on',Yii::app()->localtime->fromLocalDateTime($this->request_on),true);
		$criteria->compare('random_code',$this->random_code,true);
		$criteria->compare('create_by_admin',$this->create_by_admin);
		$criteria->compare('confirmed',$this->confirmed);
		$criteria->compare('facebook_id',$this->facebook_id,true);
		$criteria->compare('twitter_id',$this->twitter_id,true);
		$criteria->compare('linkedin_id',$this->linkedin_id,true);
		$criteria->compare('google_id',$this->google_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )

		));
	}

	public function dataProvider() {

		$criteria = new CDbCriteria;
		$config = array();

		if (Yii::app()->user->getIsPu()) {
			$criteria->compare('idst', CoreUserPU::getList(Yii::app()->user->id));
		} else {
			$criteria->compare('idst', $this->idst);
		}

		$criteria->compare('userid', $this->userid, true);
		$criteria->compare('firstname', $this->firstname, true);
		$criteria->compare('lastname', $this->lastname, true);
		$criteria->compare('email', $this->email);
		$criteria->compare('confirmed', $this->confirmed);
		$criteria->compare('request_on', Yii::app()->localtime->fromLocalDateTime($this->request_on));

		if ($this->search_input)
		{
			$criteria->addCondition('CONCAT(t.firstname, " ", t.lastname) LIKE :search OR t.email LIKE :search OR t.userid LIKE :search');
			$criteria->params[':search'] = '%'.$this->search_input.'%';
		}

		$sortAttributes = $this->attributeNames();

		$config['criteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 'request_on desc';
		$config['pagination'] = array(
            'pageSize' => Settings::get('elements_per_page', 10)
        );

		return new CActiveDataProvider(get_class($this), $config);
	}


	public function uniqueUserid($attribute, $params)
	{
		if($this->isNewRecord){
			$userid = trim($this->userid);
			$criteria = new CDbCriteria();
			$criteria->addCondition('LOWER(userid) = :userid');
			$criteria->params['userid'] = strtolower(CoreUser::getAbsoluteUsername($userid));
			$user = CoreUser::model()->find($criteria);
			if (!$user) {
				$criteria = new CDbCriteria();
				$criteria->addCondition('LOWER(userid) = :userid');
				$criteria->params['userid'] = strtolower($userid);
				$user = self::model()->find($criteria);
			}
			if($user){
				$this->addError($attribute, 'Username "'.$userid.'" has already been taken.');
			}
		}

	}

	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::afterSave()
	 */
	public function afterSave() {

		if (in_array($this->scenario, array('create', 'import'))) {
			if (Yii::app()->user->getIsAdmin()) {
				$association = new CoreAdminTree();
				$association->idstAdmin = Yii::app()->user->id;
				$association->idst = $this->idst;
				$association->save();
				$associationPU = new CoreUserPU();
				$associationPU->puser_id = Yii::app()->user->id;
				$associationPU->user_id = $this->idst;
				$associationPU->save();
			}
		}

		parent::afterSave();



	}

	/**
	 * If this temp user has been rejected/not approved, we need to do some cleaning
	 */
	public function doRejectedCleanup() {

		// Do some clean up, because on TEMP user creation some addional data were saved as a preparation for the "approval" process
		// -- groups, if assigned
		// -- org charts
		// -- power user assignment
		// -- core setting user
		// -- ...

		try {
			CoreGroupMembers::model()->deleteAllByAttributes(array('idstMember' => $this->idst));
			CoreAdminTree::model()->deleteAllByAttributes(array('idst' => $this->idst));
			CoreUserPU::model()->deleteAllByAttributes(array('user_id' => $this->idst));
			CoreSettingUser::model()->deleteAllByAttributes(array('id_user' => $this->idst));
			CoreUserFieldValue::model()->deleteAllByAttributes(array('id_user' => $this->idst));
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}


	}


	/**
	 * Convert a temp user to a real user. Must be only done by an admin
	 * if self-registration is "moderated". If self registration is "free"
	 * this is done after the user confirms his email.
	 */
	public function confirm() {
		$newUser = new CoreUser('fromTempUser');
		$newUser->runGroupAutoassign = true;
		$confirm = $newUser->createFromTempUser($this);
		if($confirm){

			// Trigger enrollment rules
			$c = new CDbCriteria();
			$c->addCondition('idstMember=:idstMember');
			$c->params[':idstMember'] = $newUser->idst;
			$c->with = 'group';
			$c->select = 't.*';
			//$c->addNotInCondition('group.groupid', array('/oc_0', '/ocd_0'));
			//NOTE: root node CAN be selected as enrollment rule target !!
			$c->addCondition("group.groupid <> :root_group");
			$c->params[':root_group'] = '/ocd_0';
			$groups = CoreGroupMembers::model()->findAll($c);

            $newNodes = array();
			foreach($groups as $group){
				if ($group->group->isUserGroup)
				{
					Yii::app()->event->raise(EventManager::EVENT_NEW_GROUP_USER, new DEvent($group, array()));
				}
				else if ($group->group->isOrgChart)
				{
                    if($group->idst!=CoreOrgChartTree::getOrgRootNode()->idst_oc && $group->idst!=CoreOrgChartTree::getOrgRootNode()->idst_ocd)
                        $newNodes[] = CoreOrgChartTree::getIdOrgByOcOrOcd($group->idst);
					Yii::app()->event->raise(EventManager::EVENT_NEW_BRANCH_USER, new DEvent($group, array()));
				}
			}
            $newNodes = array_unique($newNodes);
            if (($key = array_search(CoreOrgChartTree::getOrgRootNode()->idOrg, $newNodes)) !== false) {
                unset($newNodes[$key]);
            }
            foreach($newNodes as $node){
                Yii::app()->event->raise(CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH, new DEvent($this, array(
                    'targetUser' => $newUser->idst,
                    'BranchIdOrg'  => $node
                )));
            }
			$this->delete();

			return $newUser->idst;
		}

		return FALSE;
	}

	public function getClearUserId()
	{
		if (substr($this->userid, 0, 1) == '/')
			return substr($this->userid, 1);
		else
			return $this->userid;
	}
}