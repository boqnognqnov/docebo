<?php

/**
 * This is the model class for table "learning_log_marketplace_course_login".
 *
 * The followings are the available columns in table 'learning_log_marketplace_course_login':
 * @property integer $idLog
 * @property integer $mpCourseId
 * @property integer $idCourse
 * @property integer $idst
 * @property string $dateFirstAccess
 */
class LearningLogMarketplaceCourseLogin extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningLogMarketplaceCourseLogin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_log_marketplace_course_login';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mpCourseId, idCourse, idst', 'required'),
			array('mpCourseId, idCourse, idst', 'numerical', 'integerOnly'=>true),
			array('dateFirstAccess', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idLog, mpCourseId, idCourse, idst, dateFirstAccess', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function behaviors() {
        return array(
            'LocalTimeConversionBehavior' => array(
                'class' => 'common.components.LocalTimeConversionBehavior',
			 'timestampAttributes' => array('dateFirstAccess medium'),
			 'dateAttributes' => array()
            )
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idLog' => 'Id Log',
			'mpCourseId' => 'Mp Course',
			'idCourse' => 'Id Course',
			'idst' => 'Idst',
			'dateFirstAccess' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idLog',$this->idLog);
		$criteria->compare('mpCourseId',$this->mpCourseId);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idst',$this->idst);
		$criteria->compare('dateFirstAccess',Yii::app()->localtime->fromLocalDateTime($this->dateFirstAccess),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
		));
	}

	/**
	 * Return count of all taken seats for a course
	 * @param $idCourse
	 * @return int
	 */
	public static function takenSeatsByCourse($idCourse)
	{
		$course = LearningCourse::model()->findByPk($idCourse);
		$criteria = new CDbCriteria();
		$criteria->addCondition('idCourse=:idCourse AND mpCourseId=:mpCourseId');
		$criteria->params[':idCourse'] = (int) $idCourse;
		$criteria->params[':mpCourseId'] = $course->mpidCourse;
		return self::model()->count($criteria);
	}
	
	
	/**
	 * Check if user already took a seat for a given idCourse+mpidCourse
	 * This is a positive information: that means that user can play LOs again and again.
	 *  
	 * @param number $idCourse
	 * @param number $idUser
	 * @return boolean
	 */
	public static function userHasSeat($idCourse, $idUser) {
		$course = LearningCourse::model()->findByPk((int)$idCourse);
		$criteria = new CDbCriteria();
		$criteria->addCondition('idCourse=:idCourse AND mpCourseId=:mpCourseId AND idst=:idUser');
		$criteria->params[':idCourse'] = (int) $idCourse;
		$criteria->params[':mpCourseId'] = (int) $course->mpidCourse;
		$criteria->params[':idUser'] = (int) $idUser;
		return self::model()->count($criteria) > 0;
	} 
	
	
	/**
	 * Take a seat in a marketplace course
	 * 
	 * @param number $idCourse
	 * @param number $idUser
	 */
	public static function takeASeat($idCourse, $idUser) {
		$course = LearningCourse::model()->findByPk((int) $idCourse);
		if ($course && ((int) $course->mpidCourse > 0))  {
			$model = new self();
			$model->mpCourseId = $course->mpidCourse;
			$model->idCourse = $idCourse;
			$model->idst = $idUser;
			$model->dateFirstAccess = Yii::app()->localtime->toLocalDateTime();
			$model->save();
		}
		return true;
	}
	
	

}