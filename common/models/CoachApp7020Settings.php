<?php

/**
 * CoachApp7020Settings
 *
 * @plugin    Coach7020App
 * @author     Ignatov <devignatov@gmail.com>
 */
class CoachApp7020Settings extends CFormModel {

    const ON = 'on';
    const OFF = 'off';
    const SETTING_TYPE = 'coachApp';
	const ENABLE_ANSWER_QUESTIONS = 'enable_answer_questions';

	private $_SETTINGS_FIELDS = array( //This must be constant but php doesn't allow array in class constants...
        'enable_answer_questions',
        //'limit_questions',
        'app7020_show_ask_expert_button',
        'app7020_ask_button_lo',
        //'app7020_limit_characters_question'
    );


    public
        $enable_answer_questions,
        //$limit_questions,
        $app7020_limit_characters_question,
        $app7020_ask_button_lo,
        $app7020_show_ask_expert_button;

    /**
     *
     * Constructor for the model if first param is true will load automaticly settings
     *
     * @param loadSettings bool
     * @return array
     */
    public function __construct($loadSettings = false){
        if($loadSettings){
            $this->getSettings();
        }
    }
 

    public function rules() {
        return array(
            array(
                'enable_answer_questions,app7020_ask_button_lo',
                'in',
                'range' => array(self::ON, self::OFF),
                'message' => Yii::t('app7020', 'The {attribute} is set incorrect')
            ),
        );
    }

    /**
     *
     * Attributed labels for CActiveForm
     *
     * @return array
     */
    public function attributeLabels() {
        return array(
            'enable_answer_questions' => '<span></span>' . Yii::t('app7020', 'Enable contributor to answer Questions'),
            'limit_questions' => '<span></span>' . Yii::t('app7020', 'Limit questions to'),
            'app7020_ask_button_lo' => '<span></span>'. Yii::t('app7020', 'Enable Ask the Expert on courses of training material'),
            'app7020_show_ask_expert_button' => '<span></span>'. Yii::t('app7020', 'Enable Ask the Expert button in the main menu')
        );
    }

    /**
     *
     * Loading settings from core_settings table
     *
     * @return void
     */
    public function getSettings() {
        foreach ($this->_SETTINGS_FIELDS as $param) {
            if (property_exists($this, $param)) {
                $this->$param = Settings::get($param, 'off');
            }
        }
     }

    /**
     *
     * Saving settings to database return true on success, false otherwise
     *
     * @return bool
     */
    public function save(){
 
        if (is_array($this->attributes)) {
            foreach ($this->attributes as $param => $value) {
                if(in_array($param, $this->_SETTINGS_FIELDS)){
                    Settings::save($param, $value, self::SETTING_TYPE);
                }
            }
            return true;
        } else {
            return false;
        }
    }
 

    /**
     *
     * Return length of characters of false if setting for limit questions is disabled.
     *
     * @return int/bool
     */
    public static function getLimitCharacters(){
       /*$isEnabled = Settings::get('limit_questions', self::OFF);
        if($isEnabled != self::OFF){
            $length = Settings::get('app7020_limit_characters_question', App7020Question::MIN_CHARACTERS);
            return $length;
        }
       */
        return false;
    }

    /**
     *
     * Retrieve instance of the model
     * @param $loadData if true will load automaticly settings from database
     * @return CoachApp7020Settings
     */
    public static function model($loadData = false){
        return new CoachApp7020Settings($loadData);
    }

	
	/**
	 * Check if 'enable_answer_questions' is set to 'ON' in core_settings
	 * @return bool
	 */
	public static function isEnableAnswerQuestionsGlobal() {
		return Settings::get(self::ENABLE_ANSWER_QUESTIONS) == self::ON ? true : false;
	}


}