<?php

/**
 * This is the model class for table "core_user_pu_course".
 *
 * The followings are the available columns in table 'core_user_pu_course':
 * @property integer $puser_id
 * @property integer $course_id
 * @property integer $total_seats Only applies to PUs with the "seats" permission for now
 * @property integer $available_seats Only applies to PUs with the "seats" permission for now
 * @property integer $extra_seats Only applies to PUs with the "seats" permission for now
 *
 * Available AR relations:
 * @property LearningCourse $course
 */
class CoreUserPuCourse extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUserPuCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user_pu_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('puser_id, course_id', 'required'),
			array('puser_id, course_id, total_seats, available_seats, extra_seats', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('puser_id, course_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courses' => array(self::HAS_MANY, 'LearningCourse', array('idCourse' => 'course_id'), 'joinType' => 'JOIN'),
			'course'=>array(self::HAS_ONE, 'LearningCourse', '', 'on'=>'t.course_id=course.idCourse')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'puser_id' => 'Puser',
			'course_id' => 'Course',
			'total_seats' => Yii::t('course', 'Total Seats'),
			'available_seats' => Yii::t('course', 'Available seats'),
			'extra_seats' => Yii::t('course', 'Extra Seats'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('puser_id',$this->puser_id);
		$criteria->compare('course_id',$this->course_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		));
	}

	protected function beforeSave() {
		if($this->available_seats > $this->total_seats){
			Yii::log('Warning! Possible Power User related bug. Available seats more than total seats for the course '. $this->course_id.' and user '.$this->puser_id.' pair. Equalizing both values to total seat amount...', CLogger::LEVEL_ERROR);
			$this->available_seats = $this->total_seats;
		}
		return parent::beforeSave();
	}

	public function updatePowerUserSelection($idAdmin, $courses = array(), $courseAssignMode = false) {


		$db = Yii::app()->db;
		if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

		try {

			$list = array(
				CoreAdminCourse::TYPE_COURSE => array_keys(CoreAdminCourse::model()->getPowerUserCourses($idAdmin)),
				CoreAdminCourse::TYPE_COURSEPATH => array_keys(CoreAdminCourse::model()->getPowerUserCoursepaths($idAdmin)),
				CoreAdminCourse::TYPE_CATALOG => array_keys(CoreAdminCourse::model()->getPowerUserCatalogs($idAdmin)),
				CoreAdminCourse::TYPE_CATEGORY => array_keys(CoreAdminCourse::model()->getPowerUserCategories($idAdmin)),
				CoreAdminCourse::TYPE_CATEGORY_DESC => array_keys(CoreAdminCourse::model()->getPowerUserCategories($idAdmin, true)),
			);

			$newCourses = array();
			foreach ($list as $type => $typedList) {
				switch ($type) {
					case CoreAdminCourse::TYPE_COURSE: {
						if (!empty($typedList)) { $newCourses = array_merge($newCourses, $typedList); }
					} break;
					case CoreAdminCourse::TYPE_COURSEPATH: {
						if (!empty($typedList)) {
							$reader = Yii::app()->db->createCommand()
								->select("id_path, id_item")
								->from(LearningCoursepathCourses::model()->tableName())
								->where(array("IN", "id_path", $typedList))
								->query();
							if ($reader) {
								while ($cpCourse = $reader->read()) {
									$newCourses[] = $cpCourse['id_item'];
							}
						}
						}
					} break;
					case CoreAdminCourse::TYPE_CATALOG: {
						/*
						 @SOW108-10
						Assigning catalogs to PUs doesn't mean he should
						automatically manage the courses in those catalogs
						(programatically speaking: we shouldn't carry the
						assigned catalog's course IDs to core_user_pu_course
						table since that table holds the "resolved"
						(during login of the PU) courses to be managed by the PU

						if (!empty($typedList)) {
							foreach($typedList as $id_cat)
								$newCourses = array_merge($newCourses, LearningCatalogue::model()->getCatalogCoursesList($id_cat, false));
						}
						*/
					} break;
					case CoreAdminCourse::TYPE_CATEGORY:{
						// Extract courses from this category
						$coursesFromThisCat = Yii::app()->getDb()->createCommand()
							->select('idCourse')
							->from(LearningCourse::model()->tableName())
							->where(array('IN', 'idCategory', $typedList))
							->queryColumn();
						$newCourses = array_merge($newCourses, $coursesFromThisCat);
					} break;
					case CoreAdminCourse::TYPE_CATEGORY_DESC:{
						// Extract courses from this category and its descendant categories
						$catIds = Yii::app()->getDb()->createCommand()
							->selectDistinct('desc.idCategory')
							->from(LearningCourseCategoryTree::model()->tableName().' parents')
							->join(LearningCourseCategoryTree::model()->tableName().' desc', 'desc.iLeft >= parents.iLeft AND desc.iRight <= parents.iRight')
							->where(array('IN', 'parents.idCategory', $typedList))
							->queryColumn();
						if(!empty($catIds)){
							$coursesFromTheseCatParentsAndDescendants = Yii::app()->getDb()->createCommand()
							                                               ->select('idCourse')
							                                               ->from(LearningCourse::model()->tableName())
							                                               ->where(array('IN', 'idCategory', $catIds))
							                                               ->queryColumn();
							$newCourses = array_merge($newCourses, $coursesFromTheseCatParentsAndDescendants);
						}
					} break;
				}
			}

			// And finally, if there is a special course assign mode
			// for this Power User, apply that mode (e.g. "All Courses"
			// or "All courses in visible catalogs"
			if ( !$courseAssignMode ) {
				$courseAssignMode = CoreAdminCourse::getCourseAssignModeForPU($idAdmin);
			}
			switch($courseAssignMode){
				case CoreAdminCourse::COURSE_SELECTION_MODE_ALL:
					$newCourses = Yii::app()->getDb()->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->queryColumn();
					break;
				case CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS:

					$idsts = array();
					$idsts[] = $idAdmin;
					$userGroupIds = Yii::app()->getDb()->createCommand()
						->select('cgm.idst')
						->from('core_group_members cgm')
						->where('idstMember=:idUser', array(':idUser'=>$idAdmin))
						->join('core_group g', 'cgm.idst=g.idst')
						->queryColumn();
					$idsts = array_merge($idsts, $userGroupIds);
					// Since a catalog may have a "orgchart+descendats" assigned to it,
					// walk though the current user's groups upwards to their parent
					// groups and get those groups' OCD IDs. Then if one of those IDs
					// match an ID assigned to a catalog, we should include this catalog,
					// since the user is a member of a descendant group of a
					// "group+descendants" assigned to the catalog.
					$ocdArr = array();
					$c2 = new CDbCriteria();
					$c2->addCondition('groupMembers.idstMember=:idstMember');
					$c2->params[':idstMember'] = $idAdmin;
					$c2->with = array('groupMembers');
					$c2->select = 't.lev, t.iLeft, t.iRight';
					$userGroups = CoreOrgChartTree::model()->findAll($c2);
					foreach($userGroups as $group){
						$tempCriteria = new CDbCriteria();
						$tempCriteria->addCondition('t.iLeft <= :currentLeft');
						$tempCriteria->addCondition('t.iRight >= :currentRight');
						$tempCriteria->params[':currentLeft'] = $group->iLeft;
						$tempCriteria->params[':currentRight'] = $group->iRight;
						$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);
						if($groupParent) {
							foreach($groupParent as $parent){
								$ocdArr[] = $parent->idst_ocd;
								if($group->iLeft === $parent->iLeft)
									$ocdArr[] = $parent->idst_oc;
							}
						}
					}
					$ocdArr = array_unique($ocdArr);
					$idsts = array_merge($idsts, $ocdArr);

					$newCourses = Yii::app()->getDb()->createCommand()
					   ->selectDistinct( 'lce.idEntry' )
					   ->from( 'learning_catalogue_entry lce' )
						->join('learning_catalogue c', 'c.idCatalogue=lce.idCatalogue')
						->join('learning_catalogue_member lcm', 'c.idCatalogue=lcm.idCatalogue')
					   ->where( 'lce.type_of_entry="course"' )
						->andWhere(array('IN', 'lcm.idst_member', $idsts))
					   ->queryColumn();

					break;
                case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS:
					$newCourses = Yii::app()->getDb()->createCommand()
						->select('idCourse')
						->from(LearningCourse::model()->tableName())
						->queryColumn();

                    $newCoursePaths = Yii::app()->getDb()->createCommand()
                        ->select('id_path')
                        ->from(LearningCoursepath::model()->tableName())
                        ->queryColumn();
                    break;
				case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS:
					$newCoursePaths = Yii::app()->getDb()->createCommand()
						->select('id_path')
						->from(LearningCoursepath::model()->tableName())
						->queryColumn();

					$newCourses = Yii::app()->getDb()->createCommand()
						->select('id_item')
						->from(LearningCoursepathCourses::model()->tableName())
						->queryColumn();

					break;
				case CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD:
					// Standard mode already handled by the logic above the switch()
					// We shouldn't reach here ever
					break;
				
				case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS:

					$idsts[] = $idAdmin;
					$userGroupIds = Yii::app()->getDb()->createCommand()
						->select('cgm.idst')
						->from('core_group_members cgm')
						->where('idstMember=:idUser', array(':idUser'=>$idAdmin))
						->join('core_group g', 'cgm.idst=g.idst')
						->queryColumn();
					$idsts = array_merge($idsts, $userGroupIds);
					// Since a catalog may have a "orgchart+descendats" assigned to it,
					// walk though the current user's groups upwards to their parent
					// groups and get those groups' OCD IDs. Then if one of those IDs
					// match an ID assigned to a catalog, we should include this catalog,
					// since the user is a member of a descendant group of a
					// "group+descendants" assigned to the catalog.
					$ocdArr = array();
					$c2 = new CDbCriteria();
					$c2->addCondition('groupMembers.idstMember=:idstMember');
					$c2->params[':idstMember'] = $idAdmin;
					$c2->with = array('groupMembers');
					$c2->select = 't.lev, t.iLeft, t.iRight';
					$userGroups = CoreOrgChartTree::model()->findAll($c2);
					foreach($userGroups as $group){
						$tempCriteria = new CDbCriteria();
						$tempCriteria->addCondition('t.iLeft <= :currentLeft');
						$tempCriteria->addCondition('t.iRight >= :currentRight');
						$tempCriteria->params[':currentLeft'] = $group->iLeft;
						$tempCriteria->params[':currentRight'] = $group->iRight;
						$groupParent = CoreOrgChartTree::model()->findAll($tempCriteria);
						if($groupParent) {
							foreach($groupParent as $parent){
								$ocdArr[] = $parent->idst_ocd;
								if($group->iLeft === $parent->iLeft)
									$ocdArr[] = $parent->idst_oc;
							}
						}
					}
					$ocdArr = array_unique($ocdArr);
					$idsts = array_merge($idsts, $ocdArr);

					$newCourses = Yii::app()->getDb()->createCommand()
					   ->selectDistinct( 'lce.idEntry' )
					   ->from( 'learning_catalogue_entry lce' )
						->join('learning_catalogue c', 'c.idCatalogue=lce.idCatalogue')
						->join('learning_catalogue_member lcm', 'c.idCatalogue=lcm.idCatalogue')
					   ->where( 'lce.type_of_entry="course"' )
						->andWhere(array('IN', 'lcm.idst_member',  $idsts))
					   ->queryColumn();

					$newCoursePaths = Yii::app()->getDb()->createCommand()
					   ->selectDistinct( 'lce.idEntry' )
					   ->from( 'learning_catalogue_entry lce' )
						->join('learning_catalogue c', 'c.idCatalogue=lce.idCatalogue')
						->join('learning_catalogue_member lcm', 'c.idCatalogue=lcm.idCatalogue')
					   ->where( 'lce.type_of_entry="coursepath"' )
						->andWhere(array('IN', 'lcm.idst_member', $idsts))
					   ->queryColumn();


					$coursesFromLP = Yii::app()->getDb()->createCommand()
						->selectDistinct('lcc.id_item')
						->from('learning_coursepath_courses lcc')
						->where(['IN', 'lcc.id_path', $newCoursePaths])
						->queryColumn();




					break;
				
			}

			$newCourses = array_unique($newCourses);

			$filterByCourses = false;
			if(!empty($courses)) {
				$filterByCourses = true;
				$newCourses = array_intersect($newCourses, $courses);
			}

			// Preserve the PUs purchased course seats
			// in case he has some during resolving associated courses
			// on login
			$criteria = new CDbCriteria();
			$criteria->index = 'course_id';
			$criteria->compare('puser_id', $idAdmin);
			if($filterByCourses && !empty($newCourses))
				$criteria->addInCondition('course_id', $newCourses);
			$seatInfoPreResolve = self::model()->findAll($criteria);

			$attributes = array('puser_id' => $idAdmin);
			if($filterByCourses)
				$attributes['course_id'] = $courses;
			$deleteExistingAssociations = self::model()->deleteAllByAttributes($attributes);
			if ($deleteExistingAssociations === FALSE) { throw new CException('Error while updating power user selection'); }
			foreach ($newCourses as $newCourse) {
				$ar = new CoreUserPuCourse();
				$ar->puser_id = $idAdmin;
				$ar->course_id = $newCourse;
				if(isset($seatInfoPreResolve[$newCourse])){
					$ar->available_seats = $seatInfoPreResolve[$newCourse]->available_seats;
					$ar->total_seats = $seatInfoPreResolve[$newCourse]->total_seats;
					$ar->extra_seats = $seatInfoPreResolve[$newCourse]->extra_seats;
				}
				if (!$ar->save()) { throw new CException('Error while updating power user selection'); }
			}


			if (isset($coursesFromLP)) {
				//This add courses from learning plans if exists in to core_user_pu_course
				foreach ($coursesFromLP as $newCourse) {
					$ar = new CoreUserPuCourse();
					$ar->puser_id = $idAdmin;
					$ar->course_id = $newCourse;
					if (isset($seatInfoPreResolve[$newCourse])) {
						$ar->available_seats = $seatInfoPreResolve[$newCourse]->available_seats;
						$ar->total_seats = $seatInfoPreResolve[$newCourse]->total_seats;
						$ar->extra_seats = $seatInfoPreResolve[$newCourse]->extra_seats;
					}
					if (!$ar->save()) {
						throw new CException('Error while updating power user selection');
					}
				}
			}


			if(!empty($newCoursePaths)){
			    foreach ($newCoursePaths as $path){

			        $oldPath = CoreUserPuCoursepath::model()->findByAttributes(array(
			            'path_id' => $path,
                        'puser_id' => $idAdmin
                    ));

                    if(!$oldPath){
                        $cp = new CoreUserPuCoursepath();
                        $cp->path_id = $path;
                        $cp->puser_id = $idAdmin;
                        if (!$cp->save()) { throw new CException('Error while updating power user selection'); }
                    }
                }
            }

			if (isset($transaction)) { $transaction->commit(); }

		} catch (CException $e) {

			if (isset($transaction)) { $transaction->rollback(); }
			throw $e;
		}


		Yii::app()->event->raise(EventManager::EVENT_POWER_USER_OBJECTS_SELECTION_UPDATED, new DEvent($this, array(
		    'puser_id'       => $idAdmin,
		)));

		return true;
	}


	public function getList($idAdmin) {
		static $cache = array();

		if(!isset($cache[$idAdmin])) {
			$cache[ $idAdmin ] = Yii::app()->db->createCommand()
											   ->select( "course_id" )
											   ->from( self::model()->tableName() )
											   ->where( "puser_id = :id_admin", array( ':id_admin' => $idAdmin ) )
											   ->queryColumn();
		}
		return $cache[$idAdmin];
	}




	/**
	 * Get the number of seats the given user has
	 * for distribution in the given course (only applies to PU users for now)
	 *
	 * @param $idUser
	 * @param $idCourse
	 *
	 * @return array For example: array('total'=>X, 'available'=>Y)
	 */
	public static function getTotalAndAvailableSeats($idCourse, $idUser = null ) {
		if(!$idUser) $idUser = Yii::app()->user->getIdst();

		static $cache = array();

		if(!isset($cache[$idUser])){
			$query = Yii::app()->getDb()->createCommand()
			            ->select('course_id, total_seats, available_seats')
			            ->from(self::model()->tableName())
			            ->where('puser_id=:idUser', array(':idUser'=>$idUser))
			            ->queryAll(true);

			$cache[$idUser] = array();

			if(!empty($query)){
				foreach($query as $row){
					$cache[$idUser][$row['course_id']] = array(
						'total' => intval($row['total_seats']),
						'available' => intval($row['available_seats']),
					);
				}
			}
		}

		if(isset($cache[$idUser][$idCourse])){
			return $cache[$idUser][$idCourse];
		}

		return array(
			'total'=>0,
			'available'=>0,
		);
	}




	public static function checkIfPuHasEnoughSeatsInCourse($numUsersNeeded, $idCourse, $idUser = null){
		if(!$idUser) $idUser = Yii::app()->user->getIdst();

		$seatInfo = self::getTotalAndAvailableSeats($idCourse, $idUser);

		$availableSeats = $seatInfo['available'];

		return $availableSeats >= $numUsersNeeded;
	}




	/**
	 * Pass an array of courses and the seats needed (in each) and
	 * get a boolean value if each of these courses have enough free
	 * seats to cover the needed count
	 *
	 * @param      $idCourses
	 * @param      $seatsNeeded
	 * @param null $forPuUserId
	 *
	 * @return array
	 */
	static public function getMassCoursesWithInsufficientSeats($idCourses, $seatsNeeded, $forPuUserId = null)
	{
		if (!$forPuUserId) $forPuUserId = Yii::app()->user->getIdst();

		return Yii::app()->getDb()->createCommand()
			->select('c.*')
			->from(CoreUserPuCourse::model()->tableName() . ' t')
			->join(LearningCourse::model()->tableName() . ' c', 'c.idCourse=t.course_id')
			->where('t.puser_id=:idUser', array(':idUser' => $forPuUserId))
			->andWhere('c.selling=1')
			->andWhere(array('IN', 'c.idCourse', $idCourses))
			->andWhere('t.available_seats < :usersCount', array(':usersCount' => $seatsNeeded))
			->queryAll();
	}


	/**
	 * renderAvailableAndTotalSeats() - renders available and total seats column
	 * @param $idCourse - id of a course
	 * @param mixed $idUser - if not set or empty it gets current user id
	 * @return html to be displayed in the courses seats column of the UserSelector
	 */
	public static function renderAvailableAndTotalSeats($idCourse, $idUser = false, $forceShow = false)
	{
		$output = '-';

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		$learningCourse = LearningCourse::model()->findByPk($idCourse);

		if ($learningCourse->selling && (CoreUserPU::isPUAndSeatManager() || $forceShow)) {
			$seats = CoreUserPuCourse::getTotalAndAvailableSeats($idCourse, $idUser);
			if ((!empty($seats['total']) AND isset($seats['available']))) {
				$addClass = '';
				if ($seats['available'] == 0) $addClass = 'orange';
				$output = '<span class="underline ' . $addClass . '">' . $seats['available']
					. '/' . $seats['total'] . '</span> <a><span class="maxAvailable"></span></a>';
			}
		}

		return $output;
	}



	/**
	 * To be called after some courses have been deleted
	 * @param $courses
	 */
	public static function updateAfterCoursesDeletion($courses) {

		if (is_numeric($courses) && (int)$courses > 0) {
			$courses = array((int)$courses);
		}

		if (!is_array($courses)) {
			return false;
		}

		if (empty($courses)) {
			return true; //no courses to be processed ... just go away from here
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition('course_id', $courses);
		self::model()->deleteAll($criteria);
	}



	/**
	 * By passing a list of users, retrieve it's P.U. from branches/groups and update power users assignments
	 * @param $users a list of idst
	 * @return bool successful operation or not
	 */
	public static function updatePowerUsersOfCourses($courses) {

		if (is_numeric($courses) && (int)$courses > 0) {
			$courses = array((int)$courses);
		}

		if (!is_array($courses)) {
			return false;
		}

		if (empty($courses)) {
			return true; //no courses to be processed ... just go away from here
		}

		//retrieve involved PUs with new courses
		$pus = array();

		//courses list may be too big for the queries: break it in parts of safe size if needed
		$parts = array_chunk($courses, 1000);
		foreach ($parts as $part) {

			if (empty($part)) { continue; }

			// extract pus from course categories
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCourseCategoryTree::model()->tableName() . " cct")
				->join(LearningCourse::model()->tableName() . " c", "cct.idCategory = c.idCategory")
				->join(CoreAdminCourse::model()->tableName() . " ac", "cct.idCategory = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATEGORY))
				->where(array("IN", "c.idCourse", $part));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			// extract pus from categories+descendants case
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCourseCategoryTree::model()->tableName() . " cct_1")
				->join(LearningCourseCategoryTree::model()->tableName() . " cct_2", "cct_2.iLeft <= cct_1.iLeft AND cct_2.iRight >= cct_1.iRight")
				->join(LearningCourse::model()->tableName() . " c", "cct_1.idCategory = c.idCategory")
				->join(CoreAdminCourse::model()->tableName() . " ac", "cct_2.idCategory = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATEGORY_DESC))
				->where(array("IN", "c.idCourse", $part));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			// extract pus from catalogs
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCatalogueEntry::model()->tableName() . " ce")
				->join(CoreAdminCourse::model()->tableName() . " ac", "ce.idCatalogue = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATALOG))
				->where(array("IN", "ce.idEntry", $part))
				->andWhere("ce.type_of_entry = :catalog_type_of_entry", array(':catalog_type_of_entry' => 'course'));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}
			// ... and from catalogs learning paths
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCatalogueEntry::model()->tableName() . " ce")
				->join(CoreAdminCourse::model()->tableName() . " ac", "ce.idCatalogue = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATALOG))
				->join(LearningCoursepathCourses::model()->tableName() . " cpc", "cpc.id_path = ce.idEntry AND ce.type_of_entry = :catalog_type_of_entry", array(':catalog_type_of_entry' => 'coursepath'))
				->where(array("IN", "cpc.id_item", $part));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			//extract pus from learning paths
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCoursepathCourses::model()->tableName() . " cpc")
				->join(CoreAdminCourse::model()->tableName() . " ac", "cpc.id_path = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_COURSEPATH))
				->where(array("IN", "cpc.id_item", $part));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			$cmd = Yii::app()->db->createCommand()
				->select("idst_user")
				->from(CoreAdminCourse::model()->tableName())
				->where('type_of_entry = :type AND id_entry = :entry', array(':type' => CoreAdminCourse::TYPE_COURSE_ASSIGN_MODE, ':entry' => CoreAdminCourse::COURSE_SELECTION_MODE_ALL));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}
		}

		//update every PU's users selection
		if (!empty($pus)) {
			$pus = array_keys($pus); //avoid double PUs references
			foreach ($pus as $pu) {
				self::model()->updatePowerUserSelection($pu, $courses);
			}
		}

		return true;
	}


	/**
	 * This is used to update PU selection when a course changes its category
	 * @param $category the ID of the category
	 * @param $idCourse
	 */
	public static function updateCourseCategory($category, $idCourse) {
		if (is_numeric($category) && (int)$category > 0) {

			//find all power users owning the input category (either directly or by descendants)
			$pus = array();

			//single categories
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(CoreAdminCourse::model()->tableName() . " ac")
				->where("ac.id_entry = :id_category AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATEGORY, ':id_category' => $category));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			//check categories + descendants
			$cmd = Yii::app()->db->createCommand()
				->select("ac.idst_user")
				->from(LearningCourseCategoryTree::model()->tableName() . " cct_1")
				->join(LearningCourseCategoryTree::model()->tableName() . " cct_2", "cct_2.iLeft >= cct_1.iLeft AND cct_2.iRight <= cct_1.iRight")
				->join(CoreAdminCourse::model()->tableName() . " ac", "cct_1.idCategory = ac.id_entry AND ac.type_of_entry = :type_of_entry", array(':type_of_entry' => CoreAdminCourse::TYPE_CATEGORY_DESC))
				->where("cct_2.idCategory = :id_category", array(':id_category' => $category));
			$reader = $cmd->query();
			while ($row = $reader->read()) {
				$pus[$row['idst_user']] = $row['idst_user'];
			}

			//updates power users, if any has been found
			if (!empty($pus)) {
				foreach ($pus as $idPu) {
					self::model()->updatePowerUserSelection($idPu, array($idCourse));
				}
			}
		}
	}

	/**
	 * Check if a course is assigned to a power user
	 * @param $idUser
	 * @param $idCourse
	 * @return mixed
	 */
	public static function isCourseAssignedToPU($idUser, $idCourse) {
		$count = Yii::app()->db->createCommand()
			->select('COUNT(*)')
			->from(self::model()->tableName())
			->where('puser_id = :puser_id AND course_id = :course_id', array(
				':puser_id' => $idUser,
				':course_id' => $idCourse
			))
			->queryScalar();

		if($count > 0)
			return true;
		return false;
	}

	/**
	 * renderExtraSeats() - renders extra seats column(seats given to PU by admin(-s))
	 * @param $idCourse - id of a course
	 * @param mixed $idUser - if not set or empty it gets current user id
	 * @return html to be displayed in the courses seats column of the UserSelector
	 */
	public static function renderExtraSeats($idCourse, $idUser = false, $forceShow = false)
	{
		$output = '0';

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		$learningCourse = LearningCourse::model()->findByPk($idCourse);

		if ($learningCourse->selling && (CoreUserPU::isPUAndSeatManager() || $forceShow)) {
			$coreUserPuCourse = CoreUserPuCourse::model()->findAllByAttributes(
				array('course_id' => $idCourse, 'puser_id' => $idUser)
			);

			$coreUserPuCourse = (is_array($coreUserPuCourse) && count($coreUserPuCourse)) ? current($coreUserPuCourse) : false;

			if($coreUserPuCourse) {
				$output = $coreUserPuCourse->extra_seats;
			}
		}

		return $output;
	}

	/**
	 * renderTotalSeats() - renders total seats column
	 * @param $idCourse - id of a course
	 * @param mixed $idUser - if not set or empty it gets current user id
	 * @return html to be displayed in the courses seats column of the UserSelector
	 */
	public static function renderTotalSeats($idCourse, $idUser = false, $forceShow = false)
	{
		$output = '0';

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		$learningCourse = LearningCourse::model()->findByPk($idCourse);

		if ($learningCourse->selling && (CoreUserPU::isPUAndSeatManager() || $forceShow)) {
			//$seats = CoreUserPuCourse::getTotalAndAvailableSeats($idCourse, $idUser);
			$coreUserPuCourse = CoreUserPuCourse::model()->findAllByAttributes(
				array('course_id' => $idCourse, 'puser_id' => $idUser)
			);

			$coreUserPuCourse = (is_array($coreUserPuCourse) && count($coreUserPuCourse)) ? current($coreUserPuCourse) : false;

			if($coreUserPuCourse) {
				$output = $coreUserPuCourse->total_seats;
			}
		}

		return $output;
	}


	/**
	 * renderSeats() - renders seats columns by column name passed
	 * @param $idCourse - id of a course
	 * @param mixed $idUser - if not set or empty it gets current user id
	 * @param string $column - if not set or empty it gets 'available_seats'
	 * @param bool $forceShow whatever to show seats only to PU or (to PU and admins)
	 * @return html to be displayed in the courses seats column of the UserSelector
	 */
	public static function renderSeats($idCourse, $idUser = false, $column = 'available_seats', $forceShow = false)
	{
		$output = '0';

		if (!$idUser) {
			$idUser = Yii::app()->user->id;
		}

		$learningCourse = LearningCourse::model()->findByPk($idCourse);

		if ($learningCourse->selling && (CoreUserPU::isPUAndSeatManager() || $forceShow)) {
			$coreUserPuCourse = CoreUserPuCourse::model()->findAllByAttributes(
				array('course_id' => $idCourse, 'puser_id' => $idUser)
			);

			$coreUserPuCourse = (is_array($coreUserPuCourse) && count($coreUserPuCourse)) ? current($coreUserPuCourse) : false;

			if($coreUserPuCourse && $column != 'used_seats' && $column != 'purchased_seats') {
				$output = $coreUserPuCourse->$column;
			} elseif ($column == 'used_seats') {
				$output = $coreUserPuCourse->total_seats - $coreUserPuCourse->available_seats;
			} elseif ($column == 'purchased_seats') {
				$output = $coreUserPuCourse->total_seats - $coreUserPuCourse->extra_seats;
				$output .= ' <a><span class="maxAvailable"></span></a>';
			}
		}

		return $output;
	}
}

