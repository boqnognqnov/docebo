<?php

require_once(dirname(__FILE__).'/tcpdf.php');

class TCPDF_EX extends TCPDF
{
	/**
	 * Puts an image in the page.
	 * The upper-left corner must be given.
	 * The dimensions can be specified in different ways:<ul>
	 * <li>explicit width and height (expressed in user unit)</li>
	 * <li>one explicit dimension, the other being calculated automatically in order to keep the original proportions</li>
	 * <li>no explicit dimension, in which case the image is put at 72 dpi</li></ul>
	 * Supported formats are JPEG and PNG images whitout GD library and all images supported by GD: GD, GD2, GD2PART, GIF, JPEG, PNG, BMP, XBM, XPM;
	 * The format can be specified explicitly or inferred from the file extension.<br />
	 * It is possible to put a link on the image.<br />
	 * Remark: if an image is used several times, only one copy will be embedded in the file.<br />
	 * @param $file (string) Name of the file containing the image or a '@' character followed by the image data string. To link an image without embedding it on the document, set an asterisk character before the URL (i.e.: '*http://www.example.com/image.jpg').
	 * @param $x (float) Abscissa of the upper-left corner (LTR) or upper-right corner (RTL).
	 * @param $y (float) Ordinate of the upper-left corner (LTR) or upper-right corner (RTL).
	 * @param $w (float) Width of the image in the page. If not specified or equal to zero, it is automatically calculated.
	 * @param $h (float) Height of the image in the page. If not specified or equal to zero, it is automatically calculated.
	 * @param $type (string) Image format. Possible values are (case insensitive): JPEG and PNG (whitout GD library) and all images supported by GD: GD, GD2, GD2PART, GIF, JPEG, PNG, BMP, XBM, XPM;. If not specified, the type is inferred from the file extension.
	 * @param $link (mixed) URL or identifier returned by AddLink().
	 * @param $align (string) Indicates the alignment of the pointer next to image insertion relative to image height. The value can be:<ul><li>T: top-right for LTR or top-left for RTL</li><li>M: middle-right for LTR or middle-left for RTL</li><li>B: bottom-right for LTR or bottom-left for RTL</li><li>N: next line</li></ul>
	 * @param $resize (mixed) If true resize (reduce) the image to fit $w and $h (requires GD or ImageMagick library); if false do not resize; if 2 force resize in all cases (upscaling and downscaling).
	 * @param $dpi (int) dot-per-inch resolution used on resize
	 * @param $palign (string) Allows to center or align the image on the current line. Possible values are:<ul><li>L : left align</li><li>C : center</li><li>R : right align</li><li>'' : empty string : left for LTR or right for RTL</li></ul>
	 * @param $ismask (boolean) true if this image is a mask, false otherwise
	 * @param $imgmask (mixed) image object returned by this function or false
	 * @param $border (mixed) Indicates if borders must be drawn around the cell. The value can be a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul> or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul> or an array of line styles for each border group - for example: array('LTRB' => array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)))
	 * @param $fitbox (mixed) If not false scale image dimensions proportionally to fit within the ($w, $h) box. $fitbox can be true or a 2 characters string indicating the image alignment inside the box. The first character indicate the horizontal alignment (L = left, C = center, R = right) the second character indicate the vertical algnment (T = top, M = middle, B = bottom).
	 * @param $hidden (boolean) If true do not display the image.
	 * @param $fitonpage (boolean) If true the image is resized to not exceed page dimensions.
	 * @param $alt (boolean) If true the image will be added as alternative and not directly printed (the ID of the image will be returned).
	 * @param $altimgs (array) Array of alternate images IDs. Each alternative image must be an array with two values: an integer representing the image ID (the value returned by the Image method) and a boolean value to indicate if the image is the default for printing.
	 * @return image information
	 * @public
	 * @since 1.1
	 */
	public function Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()) {
		if ($x === '') {
			$x = $this->x;
		}
		if ($y === '') {
			$y = $this->y;
		}
		// check page for no-write regions and adapt page margins if necessary
		list($x, $y) = $this->checkPageRegions($h, $x, $y);
		$cached_file = false; // true when the file is cached
		$exurl = ''; // external streams
		// check if we are passing an image as file or string
		if ($file{0} === '@') {
			// image from string
			$imgdata = substr($file, 1);
			$file = K_PATH_CACHE.'img_'.md5($imgdata);
			$fp = fopen($file, 'w');
			fwrite($fp, $imgdata);
			fclose($fp);
			unset($imgdata);
			$cached_file = true;
			$imsize = @getimagesize($file);
			if ($imsize === FALSE) {
				unlink($file);
				$cached_file = false;
			}
		} else { // image file
			if ($file{0} === '*') {
				// image as external stream
				$file = substr($file, 1);
				$exurl = $file;
			}
			// check if is local file
			if (!@file_exists($file)) {
				// encode spaces on filename (file is probably an URL)
				$file = str_replace(' ', '%20', $file);
			}
			if (@file_exists($file)) {
				// get image dimensions
				$imsize = @getimagesize($file);
			} else {
				$imsize = false;
			}
			if ($imsize === FALSE) {
				if (function_exists('curl_init')) {
					// try to get remote file data using cURL
					$cs = curl_init(); // curl session
					curl_setopt($cs, CURLOPT_URL, $file);
					curl_setopt($cs, CURLOPT_BINARYTRANSFER, true);
					curl_setopt($cs, CURLOPT_FAILONERROR, true);
					curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
					if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')){
						curl_setopt($cs, CURLOPT_FOLLOWLOCATION, true);
					}
					curl_setopt($cs, CURLOPT_CONNECTTIMEOUT, 5);
					curl_setopt($cs, CURLOPT_TIMEOUT, 30);
					$imgdata = curl_exec($cs);
					curl_close($cs);
					if ($imgdata !== FALSE) {
						// copy image to cache
						$file = K_PATH_CACHE.'img_'.md5($imgdata);
						$fp = fopen($file, 'w');
						fwrite($fp, $imgdata);
						fclose($fp);
						unset($imgdata);
						$cached_file = true;
						$imsize = @getimagesize($file);
						if ($imsize === FALSE) {
							unlink($file);
							$cached_file = false;
						}
					}
				} elseif (($w > 0) AND ($h > 0)) {
					// get measures from specified data
					$pw = $this->getHTMLUnitToUnits($w, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
					$ph = $this->getHTMLUnitToUnits($h, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
					$imsize = array($pw, $ph);
				}
			}
		}
		if ($imsize === FALSE) {
			if (substr($file, 0, -34) == K_PATH_CACHE.'msk') { // mask file
				// get measures from specified data
				$pw = $this->getHTMLUnitToUnits($w, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
				$ph = $this->getHTMLUnitToUnits($h, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
				$imsize = array($pw, $ph);
			} else {
				Yii::log('TCPDF Error: [Image] Unable to get image: '.$file);
				return '';
			}
		}
		// get original image width and height in pixels
		list($pixw, $pixh) = $imsize;
		// calculate image width and height on document
		if (($w <= 0) AND ($h <= 0)) {
			// convert image size to document unit
			$w = $this->pixelsToUnits($pixw);
			$h = $this->pixelsToUnits($pixh);
		} elseif ($w <= 0) {
			$w = $h * $pixw / $pixh;
		} elseif ($h <= 0) {
			$h = $w * $pixh / $pixw;
		} elseif (($fitbox !== false) AND ($w > 0) AND ($h > 0)) {
			if (strlen($fitbox) !== 2) {
				// set default alignment
				$fitbox = '--';
			}
			// scale image dimensions proportionally to fit within the ($w, $h) box
			if ((($w * $pixh) / ($h * $pixw)) < 1) {
				// store current height
				$oldh = $h;
				// calculate new height
				$h = $w * $pixh / $pixw;
				// height difference
				$hdiff = ($oldh - $h);
				// vertical alignment
				switch (strtoupper($fitbox{1})) {
					case 'T': {
						break;
					}
					case 'M': {
						$y += ($hdiff / 2);
						break;
					}
					case 'B': {
						$y += $hdiff;
						break;
					}
				}
			} else {
				// store current width
				$oldw = $w;
				// calculate new width
				$w = $h * $pixw / $pixh;
				// width difference
				$wdiff = ($oldw - $w);
				// horizontal alignment
				switch (strtoupper($fitbox{0})) {
					case 'L': {
						if ($this->rtl) {
							$x -= $wdiff;
						}
						break;
					}
					case 'C': {
						if ($this->rtl) {
							$x -= ($wdiff / 2);
						} else {
							$x += ($wdiff / 2);
						}
						break;
					}
					case 'R': {
						if (!$this->rtl) {
							$x += $wdiff;
						}
						break;
					}
				}
			}
		}
		// fit the image on available space
		list($w, $h, $x, $y) = $this->fitBlock($w, $h, $x, $y, $fitonpage);
		// calculate new minimum dimensions in pixels
		$neww = round($w * $this->k * $dpi / $this->dpi);
		$newh = round($h * $this->k * $dpi / $this->dpi);
		// check if resize is necessary (resize is used only to reduce the image)
		$newsize = ($neww * $newh);
		$pixsize = ($pixw * $pixh);
		if (intval($resize) == 2) {
			$resize = true;
		} elseif ($newsize >= $pixsize) {
			$resize = false;
		}
		// check if image has been already added on document
		$newimage = true;
		if (in_array($file, $this->imagekeys)) {
			$newimage = false;
			// get existing image data
			$info = $this->getImageBuffer($file);
			if (substr($file, 0, -34) != K_PATH_CACHE.'msk') {
				// check if the newer image is larger
				$oldsize = ($info['w'] * $info['h']);
				if ((($oldsize < $newsize) AND ($resize)) OR (($oldsize < $pixsize) AND (!$resize))) {
					$newimage = true;
				}
			}
		} elseif (substr($file, 0, -34) != K_PATH_CACHE.'msk') {
			// check for cached images with alpha channel
			$filehash = md5($file);
			$tempfile_plain = K_PATH_CACHE.'mskp_'.$filehash;
			$tempfile_alpha = K_PATH_CACHE.'mska_'.$filehash;
			if (in_array($tempfile_plain, $this->imagekeys)) {
				// get existing image data
				$info = $this->getImageBuffer($tempfile_plain);
				// check if the newer image is larger
				$oldsize = ($info['w'] * $info['h']);
				if ((($oldsize < $newsize) AND ($resize)) OR (($oldsize < $pixsize) AND (!$resize))) {
					$newimage = true;
				} else {
					$newimage = false;
					// embed mask image
					$imgmask = $this->Image($tempfile_alpha, $x, $y, $w, $h, 'PNG', '', '', $resize, $dpi, '', true, false);
					// embed image, masked with previously embedded mask
					return $this->Image($tempfile_plain, $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign, false, $imgmask);
				}
			}
		}
		if ($newimage) {
			//First use of image, get info
			$type = strtolower($type);
			if ($type == '') {
				$type = $this->getImageFileType($file, $imsize);
			} elseif ($type == 'jpg') {
				$type = 'jpeg';
			}
			$mqr = $this->get_mqr();
			$this->set_mqr(false);
			// Specific image handlers
			$mtd = '_parse'.$type;
			// GD image handler function
			$gdfunction = 'imagecreatefrom'.$type;
			$info = false;
			if ((method_exists($this, $mtd)) AND (!($resize AND (function_exists($gdfunction) OR extension_loaded('imagick'))))) {
				// TCPDF image functions
				$info = $this->$mtd($file);
				if ($info == 'pngalpha') {
					return $this->ImagePngAlpha($file, $x, $y, $pixw, $pixh, $w, $h, 'PNG', $link, $align, $resize, $dpi, $palign, $filehash);
				}
			}
			if (!$info) {
				if (function_exists($gdfunction)) {
					// GD library
					$img = $gdfunction($file);
					if ($resize) {
						$imgr = imagecreatetruecolor($neww, $newh);
						if (($type == 'gif') OR ($type == 'png')) {
							$imgr = $this->_setGDImageTransparency($imgr, $img);
						}
						imagecopyresampled($imgr, $img, 0, 0, 0, 0, $neww, $newh, $pixw, $pixh);
						if (($type == 'gif') OR ($type == 'png')) {
							$info = $this->_toPNG($imgr);
						} else {
							$info = $this->_toJPEG($imgr);
						}
					} else {
						if (($type == 'gif') OR ($type == 'png')) {
							$info = $this->_toPNG($img);
						} else {
							$info = $this->_toJPEG($img);
						}
					}
				} elseif (extension_loaded('imagick')) {
					// ImageMagick library
					$img = new Imagick();
					if ($type == 'SVG') {
						// get SVG file content
						$svgimg = file_get_contents($file);
						// get width and height
						$regs = array();
						if (preg_match('/<svg([^\>]*)>/si', $svgimg, $regs)) {
							$svgtag = $regs[1];
							$tmp = array();
							if (preg_match('/[\s]+width[\s]*=[\s]*"([^"]*)"/si', $svgtag, $tmp)) {
								$ow = $this->getHTMLUnitToUnits($tmp[1], 1, $this->svgunit, false);
								$owu = sprintf('%.3F', ($ow * $dpi / 72)).$this->pdfunit;
								$svgtag = preg_replace('/[\s]+width[\s]*=[\s]*"[^"]*"/si', ' width="'.$owu.'"', $svgtag, 1);
							} else {
								$ow = $w;
							}
							$tmp = array();
							if (preg_match('/[\s]+height[\s]*=[\s]*"([^"]*)"/si', $svgtag, $tmp)) {
								$oh = $this->getHTMLUnitToUnits($tmp[1], 1, $this->svgunit, false);
								$ohu = sprintf('%.3F', ($oh * $dpi / 72)).$this->pdfunit;
								$svgtag = preg_replace('/[\s]+height[\s]*=[\s]*"[^"]*"/si', ' height="'.$ohu.'"', $svgtag, 1);
							} else {
								$oh = $h;
							}
							$tmp = array();
							if (!preg_match('/[\s]+viewBox[\s]*=[\s]*"[\s]*([0-9\.]+)[\s]+([0-9\.]+)[\s]+([0-9\.]+)[\s]+([0-9\.]+)[\s]*"/si', $svgtag, $tmp)) {
								$vbw = ($ow * $this->imgscale * $this->k);
								$vbh = ($oh * $this->imgscale * $this->k);
								$vbox = sprintf(' viewBox="0 0 %.3F %.3F" ', $vbw, $vbh);
								$svgtag = $vbox.$svgtag;
							}
							$svgimg = preg_replace('/<svg([^\>]*)>/si', '<svg'.$svgtag.'>', $svgimg, 1);
						}
						$img->readImageBlob($svgimg);
					} else {
						$img->readImage($file);
					}
					if ($resize) {
						$img->resizeImage($neww, $newh, 10, 1, false);
					}
					$img->setCompressionQuality($this->jpeg_quality);
					$img->setImageFormat('jpeg');
					$tempname = tempnam(K_PATH_CACHE, 'jpg_');
					$img->writeImage($tempname);
					$info = $this->_parsejpeg($tempname);
					unlink($tempname);
					$img->destroy();
				} else {
					return;
				}
			}
			if ($info === false) {
				//If false, we cannot process image
				return;
			}
			$this->set_mqr($mqr);
			if ($ismask) {
				// force grayscale
				$info['cs'] = 'DeviceGray';
			}
			$info['i'] = $this->numimages;
			if (!in_array($file, $this->imagekeys)) {
				++$info['i'];
			}
			if ($imgmask !== false) {
				$info['masked'] = $imgmask;
			}
			if (!empty($exurl)) {
				$info['exurl'] = $exurl;
			}
			// array of alternative images
			$info['altimgs'] = $altimgs;
			// add image to document
			$this->setImageBuffer($file, $info);
		}
		if ($cached_file) {
			// remove cached file
			unlink($file);
		}
		// set alignment
		$this->img_rb_y = $y + $h;
		// set alignment
		if ($this->rtl) {
			if ($palign == 'L') {
				$ximg = $this->lMargin;
			} elseif ($palign == 'C') {
				$ximg = ($this->w + $this->lMargin - $this->rMargin - $w) / 2;
			} elseif ($palign == 'R') {
				$ximg = $this->w - $this->rMargin - $w;
			} else {
				$ximg = $x - $w;
			}
			$this->img_rb_x = $ximg;
		} else {
			if ($palign == 'L') {
				$ximg = $this->lMargin;
			} elseif ($palign == 'C') {
				$ximg = ($this->w + $this->lMargin - $this->rMargin - $w) / 2;
			} elseif ($palign == 'R') {
				$ximg = $this->w - $this->rMargin - $w;
			} else {
				$ximg = $x;
			}
			$this->img_rb_x = $ximg + $w;
		}
		if ($ismask OR $hidden) {
			// image is not displayed
			return $info['i'];
		}
		$xkimg = $ximg * $this->k;
		if (!$alt) {
			// only non-alternative immages will be set
			$this->_out(sprintf('q %.2F 0 0 %.2F %.2F %.2F cm /I%u Do Q', ($w * $this->k), ($h * $this->k), $xkimg, (($this->h - ($y + $h)) * $this->k), $info['i']));
		}
		if (!empty($border)) {
			$bx = $this->x;
			$by = $this->y;
			$this->x = $ximg;
			if ($this->rtl) {
				$this->x += $w;
			}
			$this->y = $y;
			$this->Cell($w, $h, '', $border, 0, '', 0, '', 0, true);
			$this->x = $bx;
			$this->y = $by;
		}
		if ($link) {
			$this->Link($ximg, $y, $w, $h, $link, 0);
		}
		// set pointer to align the next text/objects
		switch($align) {
			case 'T': {
				$this->y = $y;
				$this->x = $this->img_rb_x;
				break;
			}
			case 'M': {
				$this->y = $y + round($h/2);
				$this->x = $this->img_rb_x;
				break;
			}
			case 'B': {
				$this->y = $this->img_rb_y;
				$this->x = $this->img_rb_x;
				break;
			}
			case 'N': {
				$this->SetY($this->img_rb_y);
				break;
			}
			default:{
				break;
			}
		}
		$this->endlinex = $this->img_rb_x;
		if ($this->inxobj) {
			// we are inside an XObject template
			$this->xobjects[$this->xobjid]['images'][] = $info['i'];
		}
		return $info['i'];
	}
}

class PDF extends TCPDF_EX
{

	var $angle=0;
	var $encrypted;          //whether document is protected
	var $Uvalue;             //U entry in pdf document
	var $Ovalue;             //O entry in pdf document
	var $Pvalue;             //P entry in pdf document
	var $enc_obj_id;         //encryption object id
	var $last_rc4_key;       //last RC4 key encrypted (cached for optimisation)
	var $last_rc4_key_c;     //last RC4 computed key

	function PDF($orientation='P',$unit='mm',$format='A4', $language = '', $unicode=true, $encoding="UTF-8")
	{
		parent::__construct($orientation, $unit, $format, $unicode, $encoding);

		// set document information
		$this->SetCreator(PDF_CREATOR);

		// remove default header/footer
		$this->setPrintHeader(false);
		$this->setPrintFooter(false);

		//set margins
		$this->SetMargins(0, 0, 0);

		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		if($language == '')
			$language = CoreLangLanguage::getDefaultLanguage();


		$font  = 'dejavusans'; //default

		if ($language == 'thai') {
			$font = ("thsarabun");
		} else if(array_search($language, array('japanese', 'korean', 'simplified_chinese')) !== false) {
			$font = "droidsansfallbacki";
		}

		$this->SetFont($font, "", 10);
	}

	function getPdf($html, $name, $img = false, $download = true, $facs_simile = false, $for_saving = false, $bgImageDimens = false)
	{
		//when generating the pdf in a grid, don't clean the buffers because Yii grid view
		//won't ne fully rendered.
		$cleanBuffer = true;
		if (class_exists('Yii')) {
			$event = new DEvent($this);
			Yii::app()->event->raise('cleanCertificateOutputBuffer', $event);

			if(isset($event->return_value['output_buffer']) && $event->return_value['output_buffer'] == false){
				$cleanBuffer = false;
			}
		}

		if($cleanBuffer)
			@ob_end_clean();

		if (class_exists('Yii')) {
			$lang = CoreLangLanguage::model()->findByAttributes(array('lang_browsercode' => Yii::app()->getLanguage()));
			$lang_code = $lang->lang_code;
			$lang_direction = $lang->lang_direction;
		}
		else {
		 	$query =	"SELECT lang_browsercode, lang_direction"
		 			." FROM ".$GLOBALS['prefix_fw']."_lang_language"
		 			." WHERE lang_code = '". getLanguage() ."'";
		 	list($lang_code, $lang_direction) = sql_fetch_row(sql_query($query));
		}
		if(strpos($lang_code, ';') !== false) {
			$lang_code = current(explode(';', $lang_code));
		}
		$lg = array();
		$lg['a_meta_charset'] = "UTF-8";
		$lg['a_meta_dir'] = $lang_direction;
		$lg['a_meta_language'] = $lang_code;
		$lg['w_page'] = "page";
		$this->setLanguageArray($lg);

		//If the language is right to left say Arabic we need to add the background image to the left so that it does not get removed from the PDF
		$palign = '';
		if($lang_direction == 'rtl'){
			$paling = 'L';
		}

		/**
		 * Protection for the PDF
		 *
		 * print : Print the document;
		 * modify : Modify the contents of the document by operations other than those controlled by 'fill-forms', 'extract' and 'assemble';
		 * copy : Copy or otherwise extract text and graphics from the document;
		 * annot-forms : Add or modify text annotations, fill in interactive form fields, and, if 'modify' is also set, create or modify interactive form fields (including signature fields);
		 * fill-forms : Fill in existing interactive form fields (including signature fields), even if 'annot-forms' is not specified;
		 * extract : Extract text and graphics (in support of accessibility to users with disabilities or for other purposes);
		 * assemble : Assemble the document (insert, rotate, or delete pages and create bookmarks or thumbnail images), even if 'modify' is not set;
		 * print-high : Print the document to a representation from which a faithful digital copy of the PDF content could be generated. When this is not set, printing is limited to a low-level representation of the appearance, possibly of degraded quality.
		 * owner : (inverted logic - only for public-key) when set permits change of encryption and enables all other permissions.
		 */
		$this->SetProtection(array('modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), '', 'BQ�EqW7895w!Ee0134aSDf$');

		$this->AddPage();

        // disable page break
        $this->SetAutoPageBreak(FALSE, 0);

		if(!$this->empty_string($img)) {
            // set JPEG quality
            $this->setJPEGQuality(80);

			// The standard BG image dimensions are to fit the
			// A4 dimensions: 21cm X 29.7 cm
            $w = ($this->CurOrientation == 'P') ? 210 : 297;
            $h = ($this->CurOrientation == 'P') ? 297 : 210;

			// If we have custom BG image dimensions passed, use them
			if($bgImageDimens && is_array($bgImageDimens)){
				if(isset($bgImageDimens['width']))
					$w = $bgImageDimens['width'];
				if(isset($bgImageDimens['height']))
					$h = $bgImageDimens['height'];
			}

            $this->Image($img, 0, 0, $w, $h, '', '', '', true, 300, $paling, false, false, 0, 'CM');
        }

		$arrRemoveFromEnd = array('<br />');

		foreach($arrRemoveFromEnd as $end) {
			if (substr($html, 0-strlen($end)) == $end) {
				$html = substr($html, 0, strlen($html)-strlen($end));
				break;
			}
		}
		$this->WriteHTML($html);

		$name = str_replace(
			array('\\', '/', 	':', 	'\'', 	'\*', 	'?', 	'"', 	'<', 	'>', 	'|'),
			array('', 	'', 	'', 	'', 	'', 	'', 	'', 	'', 	'', 	'' ),
			$name
		);

		// maybe there is a way to understand why ie6 doesn't use correctly the 'D' option, but this work so....

		if($for_saving)
			return $this->Output(('"'.$name.'.pdf"'), 'S');

		$pdf = $this->Output(('"'.$name.'.pdf"'), 'S');

		session_write_close();
		//ini_set("output_buffering", 0);
		//Download file
		//send file length info
		header('Content-Length:'. strlen($pdf));
		//content type forcing dowlad
		header("Content-type: application/download\n");
		//cache control
		header("Cache-control: private");
		//sending creation time
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		//content type
		header('Content-Disposition: attachment; filename="'.$name.'.pdf"');

		echo $pdf;
		exit();
	}

    function PlaceWater()
    {
        //Put watermark  Author: Ivan
        $this->SetFont('times','',50);
        $this->SetTextColor(230, 230, 230);
        $this->RotatedText(0, 0, 'F a c - s i m i l e', 90);
    }

    function RotatedText($x,$y,$txt,$angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->Text($x,$y,$txt);
        $this->Rotate(0);
    }


    function Rotate($angle,$x=-1,$y=-1)
    {
        //Author: Olivier
        if($x==-1)
            $x=$this->x;
        if($y==-1)
            $y=$this->y;
        if($this->angle!=0)
            $this->_out('Q');
        $this->angle=$angle;
        if($angle!=0)
        {
            $angle*=M_PI/180;
            $c=cos($angle);
            $s=sin($angle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    function _endpage()
    {
        if($this->angle!=0)
        {
            $this->angle=0;
            $this->_out('Q');
        }
        parent::_endpage();
    }
}

class PDFRS extends TCPDF_EX
{

	var $angle=0;
	var $encrypted;          //whether document is protected
	var $Uvalue;             //U entry in pdf document
	var $Ovalue;             //O entry in pdf document
	var $Pvalue;             //P entry in pdf document
	var $enc_obj_id;         //encryption object id
	var $last_rc4_key;       //last RC4 key encrypted (cached for optimisation)
	var $last_rc4_key_c;     //last RC4 computed key

	function __construct($orientation='P',$unit='px',$format='A4', $language = '', $unicode=true, $encoding="UTF-8")
	{
		parent::__construct($orientation, $unit, $format, $unicode, $encoding);

		// set document information
		$this->SetCreator(PDF_CREATOR);

		// remove default header/footer
		$this->setPrintHeader(false);
		$this->setPrintFooter(false);

		//set margins
		$this->SetMargins(-1, -1, -1, true);

		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, -1);

		//set image scale factor
		//$this->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		if($language == '')
			$language = CoreLangLanguage::getDefaultLanguage();

		$font  = 'dejavusans'; //default

		if ($language == 'thai') {
			$font = ("thsarabun");
		} else if(array_search($language, array('japanese', 'korean', 'portuguese', 'portuguese-br', 'simplified_chinese')) !== false) {
			$font = "droidsansfallbacki";
		}

		$this->SetFont($font, "", '10px');
	}

	function PlaceWater()
	{
		//Put watermark  Author: Ivan
		$this->SetFont('times','',50);
		$this->SetTextColor(230, 230, 230);
		$this->RotatedText(0, 0, 'F a c - s i m i l e', 90);
	}

	function RotatedText($x,$y,$txt,$angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt,false,false,true,0,0,'',false,'',0,true,'T','M',false);
		$this->Rotate(0);
	}


	function Rotate($angle,$x=-1,$y=-1)
	{
		//Author: Olivier
	    if($x==-1)
	        $x=$this->x;
	    if($y==-1)
	        $y=$this->y;
	    if($this->angle!=0)
	        $this->_out('Q');
	    $this->angle=$angle;
	    if($angle!=0)
	    {
	        $angle*=M_PI/180;
	        $c=cos($angle);
	        $s=sin($angle);
	        $cx=$x*$this->k;
	        $cy=($this->h-$y)*$this->k;
	        $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
	    }
	}

	function _endpage()
	{
	    if($this->angle!=0)
	    {
	        $this->angle=0;
	        $this->_out('Q');
	    }
	    parent::_endpage();
	}

	function getPdf($html, $name, $img = false, $download = true, $facs_simile = false, $for_saving = false, $bgImageDimens_NOT_IMPLEMENTED = false)
	{
		@ob_end_clean();

		$query =	"SELECT lang_browsercode, lang_direction"
					." FROM ".$GLOBALS['prefix_fw']."_lang_language"
					." WHERE lang_code = '".getLanguage()."'";
		list($lang_code, $lang_direction) = sql_fetch_row(sql_query($query));

		if(strpos($lang_code, ';') !== false) {
			$lang_code = current(explode(';', $lang_code));
		}
		$lg = array();
		$lg['a_meta_charset'] = "UTF-8";
		$lg['a_meta_dir'] = $lang_direction;
		$lg['a_meta_language'] = $lang_code;
		$lg['w_page'] = "page";
		$this->setLanguageArray($lg);

		/**
		 * Protection for the PDF
		 *
		 * print : Print the document;
		 * modify : Modify the contents of the document by operations other than those controlled by 'fill-forms', 'extract' and 'assemble';
		 * copy : Copy or otherwise extract text and graphics from the document;
		 * annot-forms : Add or modify text annotations, fill in interactive form fields, and, if 'modify' is also set, create or modify interactive form fields (including signature fields);
		 * fill-forms : Fill in existing interactive form fields (including signature fields), even if 'annot-forms' is not specified;
		 * extract : Extract text and graphics (in support of accessibility to users with disabilities or for other purposes);
		 * assemble : Assemble the document (insert, rotate, or delete pages and create bookmarks or thumbnail images), even if 'modify' is not set;
		 * print-high : Print the document to a representation from which a faithful digital copy of the PDF content could be generated. When this is not set, printing is limited to a low-level representation of the appearance, possibly of degraded quality.
		 * owner : (inverted logic - only for public-key) when set permits change of encryption and enables all other permissions.
		 */
		$this->SetProtection(array('modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), '', 'BQ�EqW7895w!Ee0134aSDf$');

		//$this->AliasNbPages();

		$this->AddPage('','',true);
		// set JPEG quality
		//$this->setJPEGQuality(80);

		if($img != '')
			$this->Image($GLOBALS['where_files_relative'].'/doceboLms/certificate/'.$img, 0, 0, ( $this->CurOrientation == 'P' ? 595.2 : 841.8 ), ( $this->CurOrientation == 'P' ? 841.8 : 595.2 ), '', '', '', false, 300, '', false, false, 0);

		$this->setXY(0,0);
		//$this->UseCSS(true);
		$this->WriteHTML($html);

		if($facs_simile) {
			//Put watermark  Author: Ivan
			$this->setXY(0, 0);
			$this->SetFont('dejavusans','',40);
			$this->SetTextColor(255, 0, 0);
			$this->RotatedText(15, 50, 'F a c - s i m i l e', 270);
			$this->SetFont("dejavusans", "", 10);
			$this->SetTextColor(0, 0, 0);
		}

		$name = str_replace(
			array('\\', '/', 	':', 	'\'', 	'\*', 	'?', 	'"', 	'<', 	'>', 	'|'),
			array('', 	'', 	'', 	'', 	'', 	'', 	'', 	'', 	'', 	'' ),
			$name
		);

		// maybe there is a way to understand why ie6 doesn't use correctly the 'D' option, but this work so....

		if($for_saving)
			return $this->Output(('"'.$name.'.pdf"'), 'S');

		$pdf = $this->Output(('"'.$name.'.pdf"'), 'S');

		session_write_close();
		//ini_set("output_buffering", 0);
		//Download file
		//send file length info
		header('Content-Length:'. strlen($pdf));
		//content type forcing dowlad
		header("Content-type: application/download\n");
		//cache control
		header("Cache-control: private");
		//sending creation time
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		//content type
		header('Content-Disposition: attachment; filename="'.$name.'.pdf"');

		echo $pdf;
		exit();
	}
}