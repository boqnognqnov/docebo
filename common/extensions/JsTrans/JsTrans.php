<?php
/**
 * JsTrans
 *
 * Use Yii translations in Javascript
 *
 */

/**
 * Publish translations in JSON and append to the page
 *
 * @param mixed $categories the categories that are exported (accepts array and string)
 * @param mixed $languages the languages that are exported (accepts array and string)
 * @param string $defaultLanguage the default language used in translations
 */
class JsTrans
{

	static $DEFAULT_CATEGORIES = array("standard", "app7020", "order");

	static $languages 	= array();
	static $categories 	= array();

	protected static function getAssetsPath() {
		return realpath(dirname(__FILE__) . '/assets');
	}


	public static function addLanguages($languages) {
		if (!is_array($languages)) $languages = array($languages);
		self::$languages = array_merge(self::$languages, $languages);
		self::$languages = array_unique(self::$languages);
		return self::$languages;
	}

	public static function addCategories($categories) {
		if (!is_array($categories)) $categories = array($categories);
		self::$categories = array_merge(self::$categories, $categories);
		self::$categories = array_unique(self::$categories);
		return self::$categories;
	}

	public function __construct($categories=false, $languages=false, $defaultLanguage = null)
	{

		if ($categories === false) {
			$categories = self::$DEFAULT_CATEGORIES;
		}

		if ($languages === false) {
			$languages = array(Yii::app()->getLanguage());
		}

		// set default language
		if (!$defaultLanguage) $defaultLanguage = Yii::app()->getLanguage();

		// create arrays from params
		if (!is_array($categories)) $categories = array($categories);
		if (!is_array($languages)) $languages = array($languages);

		$languages = self::addLanguages($languages);
		$categories= self::addCategories($categories);

		// publish assets folder
		$assets = self::getAssetsPath();

		// create hash
		$hash = substr(md5(implode($categories) . ':' . implode($languages)), 0, 10);

        // getting the underscored lms name
        $lms_underscore_identifier = Docebo::getOriginalDomainCode(); 

        // if the LMS is a SAAS installation add its name with undcerscores instead of dots.
        $dictionaryFile = "dictionary-{$hash}-{$lms_underscore_identifier}.js";


		// By default, do NOT force-copy to final assets folder
		$forceCopy = false;

		// generate dictionary file if not exists or YII DEBUG is set
		if (!file_exists($assets . DIRECTORY_SEPARATOR . $dictionaryFile)) {
			// declare config (passed to JS)
			$config = array('language' => $defaultLanguage);

			// base folder for message translations
			$messagesFolder = rtrim(Yii::app()->messages->basePath, '\/');

			// loop message files and store translations in array
			$dictionary = array();
			foreach ($languages as $lang) {
				if (!isset($dictionary[$lang])) $dictionary[$lang] = array();

				foreach ($categories as $cat) {
					$messagefile = $messagesFolder . DIRECTORY_SEPARATOR . $lang . DIRECTORY_SEPARATOR . $cat . '.php';
					if (file_exists($messagefile)) $dictionary[$lang][$cat] = array_filter(require($messagefile));
				}
			}

			//check if the DB contains some custom translations
			//TODO: this part can be optimized with a single query, but since it is not called often it's not that necessary
			foreach ($categories as $category) {
				foreach ($languages as $language) {
					$reader = Yii::app()->db->createCommand("SELECT tx.text_key, tr.translation_text "
						." FROM ".CoreLangText::model()->tableName()." tx "
						." JOIN ".CoreLangTranslation::model()->tableName()." tr ON (tx.id_text = tr.id_text) "
						." WHERE tr.lang_code = :lang_code AND tx.text_module = :text_module")->query(array(
							':lang_code' => Lang::getCodeByBrowserCode($language),
							':text_module' => $category
						));
					if ($reader) {
						while ($record = $reader->read()) {
							//overwrite translation from messages with the one read from DB
							$dictionary[$language][$category][$record['text_key']] = $record['translation_text'];
						}
					}
				}
			}

			// save config/dictionary
			$data = 'Yii.translate.config=' . CJSON::encode($config) . ';' .
				'Yii.translate.dictionary=' . CJSON::encode($dictionary);


			// save to dictionary file
			if (!file_put_contents($assets . DIRECTORY_SEPARATOR . $dictionaryFile, $data))
				Yii::log('Error: Could not write dictionary file, check file permissions', 'trace', 'jstrans');

			// We've been asked to publish (create) into jsTrans assets subfolder; that means, we MIGHT have something to
			// publish to final LMS assets folder. Force copy please.
			$forceCopy = true;

		}

		// E.g. taken from: http://www.yiiframework.com/doc/api/1.1/CAssetManager#publish-detail
		// "If the asset is a directory, all files and subdirectories under it
		// will be published recursively. Note, in case $forceCopy is false the
		// method only checks the existence of the target directory to avoid repetitive copying."
		// We had this exact same issue here, a file is added to the same $assets folder
		// and publish() is called on the folder, causing the newly added file
		// to not go published. We solve this below by manually publishing the single file
		// we are interested in ($dictionaryFile)


		// forceCopy depends on IF we have just published something into jsTrans's assets folder
		$baseUrl = Yii::app()->assetManager->publish($assets, true, -1);

		// publish library and dictionary
		if (file_exists($assets . DIRECTORY_SEPARATOR . $dictionaryFile)) {
		    $publishedDictionary = Yii::app()->assetManager->publish($assets . DIRECTORY_SEPARATOR . $dictionaryFile);
		    // CALL THESE IN REVERSE ORDER!! Because they both go on top of the list
			Yii::app()->clientScript->registerScriptFile($publishedDictionary, CClientScript::POS_HEAD, array(), DoceboClientScript::ORDER_FIRST);
		    Yii::app()->clientScript->registerScriptFile($baseUrl . '/JsTrans.min.js', CClientScript::POS_HEAD, array(), DoceboClientScript::ORDER_FIRST);
		} else {
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/JsTrans.min.js', CClientScript::POS_HEAD, array(), DoceboClientScript::ORDER_FIRST);
			Yii::log('Error: Could not publish dictionary file, check file permissions: '.$assets.DIRECTORY_SEPARATOR.$dictionaryFile, 'warning', 'jstrans');
		}
	}


	/**
	 * Remove all created dictionary files
	 */
	public static function flushDictionaries() {
		//$path = Yii::getPathOfAlias('common').DIRECTORY_SEPARATOR.'extensions'.DIRECTORY_SEPARATOR.'JsTrans'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR;
		$path = self::getAssetsPath() . DIRECTORY_SEPARATOR;
		foreach (new DirectoryIterator($path) as $fileInfo) {
			//ignore non-dictionary files
			if ($fileInfo->isDot()) continue;
			if ($fileInfo->isDir()) continue;
			//if (strtolower($fileInfo->getExtension()) != 'js') continue;
			//NOTE: DirectoryIterator::getExtension() is implemented from php version 5.3.6, but at the moment of releasing this, lower version is running on Docebo servers
			//--- manually extract file extension ---
			$tmp = explode('.', $fileInfo->getFilename());
			if (!is_array($tmp) || count($tmp) < 2) continue;
			$extension = end($tmp);
			if (strtolower($extension) != 'js') continue;
			//---
			if (strpos($fileInfo->getFilename(), 'dictionary-') !== 0) continue;
			//remove dictionary file
			unlink($path.$fileInfo->getFilename());
		}
		//files have to be deleted from assets too
		$assetsPath = Yii::app()->assetManager->getBasePath();
		$rdi = new RecursiveDirectoryIterator($assetsPath);
		$rii = new RecursiveIteratorIterator($rdi);
		foreach ($rii as $fileInfo) {
			if ($fileInfo->isDir()) continue;
			//if (strtolower($fileInfo->getExtension()) != 'js') continue;
			//NOTE: DirectoryIterator::getExtension() is implemented from php version 5.3.6, but at the moment of releasing this, lower version is running on Docebo servers
			//--- manually extract file extension ---
			$tmp = explode('.', $fileInfo->getFilename());
			if (!is_array($tmp) || count($tmp) < 2) continue;
			$extension = end($tmp);
			if (strtolower($extension) != 'js') continue;
			//---
			if (strpos($fileInfo->getFilename(), 'dictionary-') !== 0) continue;
			unlink($fileInfo->getPathname());
		}
	}

}
