/**********************************************************************************************
 * Custom FancyTree actions that can be activated and customized when initializing the FancyTree Widget
 */
var FancyTreeActions = function () {
    this.init();
};

FancyTreeActions.prototype = {

    init: function(options){},

    generateAction: function (node, option){
        var innerOptions = { url: "", class: "node-management ", title: "", other: ""};
        // Define the template that each action should be using
        var template = "<li class='node-action'><a href='{url}' class='{class}' {other} >{title}</a></li>";
        // If the url parameter is passed let's handle it as it should be
        if (option.url !== undefined){
            // Check if the url want's the node key
            innerOptions.url = (option.url.indexOf("{nodeKey}") > -1) ? option.url.replace("{nodeKey}", node.key) : option.url;

            // If the user want's to add additional Parammeters to the tree
            if (option.urlParams !== undefined && option.urlParams.length){
                for (attr in option.urlParams) {
                    var element = ("&" + attr + "=" + option.urlParams[attr]);
                    innerOptions.url += element;
                }
                delete option.urlParams;
            }

            delete option.url;
        }
        // Handle the link class
        if (option.class !== undefined){
            innerOptions.class += option.class;
            delete option.class;
        }
        // Handle link title
        if (option.title !== undefined){
            innerOptions.title = option.title;
            delete option.title;
        }

        // We handled most of the known attributes, so let's handle the additional ones if any
        if (option != {}){
            for (attr in option) {
                var key = attr;
                var value = option[attr];
                // Check if any of the parameters want's the node key
                if(value.indexOf("{nodeKey}") > -1) {
                    value = value.replace("{nodeKey}", node.key);
                }

                var element = attr + "='" + value + "' ";
                innerOptions.other += element;
            }
        }

        // Replace all the existing options
        for (key in innerOptions) {
            var attr = "{" + key + "}";
            var code = new RegExp(attr, 'g');
            var value = innerOptions[key];
            template = template.replace( code, value );
        }

        return template;
    },

    generateNodeActions: function(node, options) {

        var actions = "<div class='node-actions'><ul>";

        // If this is the root node let's remove all othe options EXCEPT the edit
        if(node.data.is_root){
            for (action in options) {
                if( action !== 'edit'){
                    delete options[action];
                }
            }
        }

        // Let's loop trough all the actions passed by the user
        for (action in options) {
            var actionOptions = options[action];

            // Let's generate and add the action to the menu of actions
            actions += this.generateAction(node, actionOptions);
        }

        actions += "</ul></div>";
        // Return Complete actions
        return "<div class='actions'><a href='javascript:void(0);'" +
            "id='popover-" + node.key + "'" +
            "class='popover-trigger popover-bottom select-columns'" +
            "data-toggle='popover'" +
            "data-content=\"" + actions + "\"></a></div>";
    },

    nodeActionsManager: function( event, data, options, callback ) {
        var node = data.node;
        var $span = $(node.span);

        // Check if the user passed any action options
        if( options == undefined || options.length == 0 ) {
            console.log("Please provide options in order to show the Actions");
            return;
        }

        // Attach the actions content
        if ($span) {
            var actions = this.generateNodeActions(node, options);
            $span.after(actions);
        }

        // If the user did added hi's own event then let's trigger it
        callback(event, data);
    },

    nodeActionsEventManager: function(e, data, callback) {

        if (typeof(data.originalEvent)  !== "undefined"){

            var element = $(data.originalEvent.target);

            // The user just clicked on the node, but if he did clicked on the Node Actions let's prevent the rest of the default execution
            if(element.hasClass("popover-bottom") || element.closest("div.popover-content").hasClass('popover-content') ) {
                // In order to prevent Collapsing / Expanding we WILL change the status
                //data.node.expanded = ! data.node.expanded;
                return false;
            }

        }

        // If the user did added hi's own event then let's trigger it
        callback(e, data);
    },

    nodeClickActionsEventManager: function(e, data, callback) {

        if (typeof(data.originalEvent)  !== "undefined"){

            var element = $(data.originalEvent.target);

            // The user just clicked on the node, but if he did clicked on the Node Actions let's prevent the rest of the default execution
            if(element.hasClass("popover-bottom") || element.closest("div.popover-content").hasClass('popover-content') ) {
                // In order to prevent Collapsing / Expanding we WILL change the status
                data.node.expanded = ! data.node.expanded;
            }

        }

        // If the user did added hi's own event then let's trigger it
        callback(e, data);
    }
};