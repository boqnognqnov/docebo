<?php

	// Live is easier when things are shorter :-)
	$id =  $this->id;
	
	// NOTE: DEV#id is not allowed to be set through htmlOptions
	$attributes = "";
	foreach ($this->htmlOptions as $attr => $value) {
		if ( strtolower($attr) != "id" ) {
			$attributes .= " $attr=\"$value\"";
		}
	}
	 
	
?>

<div class="fancytree-wrapper">
	<?php if (!$this->isTableTree) : ?>
		<div id="<?= $id ?>" <?= $attributes ?>></div>
	<?php else: ?>
		<?php echo $this->tableTemplate; ?>	
	<?php endif;?>
</div>






<?php 
	// Create HIDDEN fields where later selection result will ne saved. Add "-json" to the incoming name.
	// Note this -json is bad we have to keep compatibility withthe OLD fashioned data submission... too long to explain... just trust me..
	echo CHtml::hiddenField($this->formFieldNames['selectedInputName'] . '-json');
	echo CHtml::hiddenField($this->formFieldNames['activeInputName'] . '-json');
	echo CHtml::hiddenField($this->formFieldNames['selectModeInputName'] . '-json');
?>


<script type="text/javascript">
/*<![CDATA[*/

	// If the user enabled the FancyTree actions let's add some JS
	<?php if($this->showActions):?>
		var TreeActions = new FancyTreeActions();
	<?php endif;?>

	// Create Fancytree Options object
	var treeOptions = {
			extensions: ['docebo', 'filter'],
			docebo: <?= CJSON::encode($this->formFieldNames) ?>,   		// extension options
			source: <?= CJSON::encode($this->treeData) ?>,  			// data array
			preSelected: <?= CJSON::encode($this->preSelected) ?>,		// array of preselected nodes to show upon loading		
			checkbox: true,												// show/not show checkboxes
			clickFolderMode: 3,						
			fx: null,													// disable animations
			selectMode: <?= $this->selectMode ?>,						// select mode: 0|1|2|3|4|....
			debugLevel: 0,
			generateIds: true,
			idPrefix: "<?= $id ?>-node-",
			icons: false,

			<?php
				// If the user wants to show the actions
				if($this->showActions) {

					$callbackActions = isset($this->eventHandlers['click']) ? $this->eventHandlers['click'] : "function(){}";
					$this->eventHandlers['click'] = "function(event, data) { return TreeActions.nodeClickActionsEventManager(event, data, " . $callbackActions . ")}";

					// If the user wants the actions to be visible then let's override the default event
					$callback = isset($this->eventHandlers['createNode']) ? $this->eventHandlers['createNode'] : "function(){}";
					$this->eventHandlers['createNode'] = "function(event, data) { return TreeActions.nodeActionsManager(event, data, " . CJSON::encode($this->actions, true) . " , " . $callback . ")}";
					$callbackActions = isset($this->eventHandlers['beforeActivate']) ? $this->eventHandlers['beforeActivate'] : "function(){}";
					$this->eventHandlers['beforeActivate'] = "function(event, data) { return TreeActions.nodeActionsEventManager(event, data, " . $callbackActions . ")}";
				}

				// Enumerate event handlers
				// http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents
				foreach ($this->eventHandlers as $event => $function) {
					echo "$event: $function ,";
				}
			?>
			
			////////////////////////
			dummyCommaKiller: false

	};

	<?php 
		// Merge incoming FANCYTREE specific options, if any
		// See http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
		foreach ($this->fancyOptions as $key => $val) {
			// If the key is exactly 'dnd', this is "Drag & Drop extension" options object; echo it literally
			if (is_string($val) && ($key=='dnd')) {
				echo 'treeOptions.'.$key . "=" . $val . ";\n";
			}
			// If it "looks" like a function/callback definition, just echo again
			else if (is_string($val) && preg_match("/function.*\(/", $val)) {
				echo 'treeOptions.'.$key . "=" . $val . ";\n";
			}
			else {
				echo 'treeOptions.'.$key . "=" . CJSON::encode($val) . ";\n";
			}
		}
	?>
	
	// Document ready
	$(function(){
		try{
			$('#<?= $id ?>').fancytree(treeOptions);
		}catch(e){
			console.log(e);
		}
	});

	// Hook to Form's on-submit to generate <input> elements with selection result (if there is a parent form anyway)
	// This is to provide compatibility with the OLD users selector where all Org Chart selections are
	// POST-ed as separate fields 
	$('#<?= $id ?>').closest('form').submit(function(){
		var tree = $('#<?= $id ?>').fancytree("getTree");
		tree.ext.docebo._generateFormElements();
	});


	// Prevent submitting form with ENTER while typing in "filter field"
	$(document).on('keydown', 'input[name="'+ '<?= $this->filterInputFieldName ?>' +'"]', function(e){
		if(e && e.which == 13){
			e.preventDefault();
			return false;
		}
	});
	

	// Handle tree filtering: filter as you type text in the INPUT
	$(document).on('keyup', 'input[name="'+ '<?= $this->filterInputFieldName ?>' +'"]', function(e){
		var tree = $('#<?= $id ?>').fancytree("getTree");
		if(e && e.which === $.ui.keyCode.ESCAPE){
			$(this).val('');
			tree.clearFilter();
			return false;
	    }

	    var match = $(this).val();
	    if (match == '') {
	    	tree.clearFilter();
	    }
	    else {
			tree.applyFilter($(this).val());
	    }
	}).focus();


	// Handle "click to clear filter" event
	$(document).on('click', '#'+ '<?= $this->filterClearElemId ?>', function(e){
		var tree = $('#<?= $id ?>').fancytree("getTree");
		tree.clearFilter();
		$('input[name="'+ '<?= $this->filterInputFieldName ?>' +'"]').val('');
	});
	
	
/*]]>*/	
</script>



