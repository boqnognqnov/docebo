/*!
 * jquery.fancytree.docebo.js
 *
 * Add Docebo specific functionality to Fancytree.  
 * (Extension module for jquery.fancytree.js: https://github.com/mar10/fancytree/)
 *
 * Copyright (c) 2014, Docebo SAP (http://www.doecbo.com)
 *
 * Released under the MIT license
 * https://github.com/mar10/fancytree/wiki/LicenseInfo
 * 
 * Added various extension methods and, the most important, NEW selectModes (1..3 are native)
 * 
 * 10:  Allow selection in terms of  THIS / THIS & DESC,  list of selected nodes does NOT include descendants of THIS & DESC selected nodes
 * 11: Same as 10, but list of reported selection includes descendants  
 *
 * @version DEVELOPMENT
 * @date DEVELOPMENT
 */

;(function($, window, document, undefined) {

	"use strict";

	/**
	 * A semaphor used to prevent recursion
	 */
	var inFilling = false;
	
	
	/**
	 * INPUT Field names
	 */
	var selectedName;
	var activeName;
	var selectModeName;

	/*
	// Example methods extending core Fancytree functionality  
	$.ui.fancytree._FancytreeClass.prototype.<method> = function(){
		var tree = this;
	};
	$.ui.fancytree._FancytreeNodeClass.prototype.<method> = function(){
		var node = this;
	};
	$.ui.fancytree.prototype.<tree-widget-method> = function(arg1){
		var tree = this.tree;
	};
	*/

	/**
	 * Count selected nodes
	 */
	$.ui.fancytree._FancytreeClass.prototype.countSelected = function(){
		var tree = this;
		var count = 0;
		tree.rootNode.visit(function(node){
			if (node.selected || node.partsel) {
				count++;
			}
		});
		return count;
	};
	
	
	/**
	 * Node method to check if it is "partialy" selected (analog to Select state = 1 /this node only/)
	 */
	$.ui.fancytree._FancytreeNodeClass.prototype.isPartsel = function(){
		return (!this.partsel) ? false : !!this.partsel; 
	}
	
	/**
	 * Add a method to directly set a selectState to a given node
	 */
	$.ui.fancytree._FancytreeNodeClass.prototype.setSelectState = function(selectState){
		if (typeof selectState == "undefined") return;
		
		// We are calling the "hooked" method which is the standard behavior. It will take care calling node's method
		// and if the Node knows about selectState, it should use it (as in our case). Original Fancytree method does not care about it.
		return this.tree._callHook("nodeSetSelected", this, false, selectState);
	}
	
	
	
	/* This new Node property will hold our specific "select" state (0: none  1: this node only, 2: this node and descendants */
	$.ui.fancytree._FancytreeNodeClass.prototype.selectState = 0;

	/* Introducing  NEW node status attribute, calculated as "Any children selected out there???". Used for modes 10+ only */
	$.ui.fancytree._FancytreeNodeClass.prototype.hasSelectedChildren = 0;
	
	
	/* 'Docebo' extension */
	$.ui.fancytree.registerExtension({
		name: "docebo",
		version: "1.0.0",
		options: {
			// INPUT name(s) for selected nodes
			selectedInputName: "fancytree_selected_nodes",
			
			// INPUT name to hold active node at submition time
			activeInputName: "fancytree_active_node",
			
			// INPUT name to hold current select mode
			selectModeInputName: "fancytree_select_mode",
			
			stopOnParents: true  
		},
		
		
		/**
		 * Preselect nodes, using information in tree.options.preSelected array.
		 * Format:  [{key : "<key>", selectState: "<select-state>"}, ... ]
		 * Where <select-state> is checked only if selectMode = 10+ and can be 0 (none),1 (this node only), 2 (this node and descendants)	
		 */
		_preSelectNodes: function(ctx) {
			
			// Get the tree instance
			var tree = ctx.tree;
			
			// Raise semaphore to prevent some operations/events during the preselection process
			tree.inPreselection = true;
			
			// If this option is defined at all...
			if (tree.options.preSelected) {
				
				// First, deselect all nodes
				tree.rootNode.children[0].setSelectState(0);
				
				// Enumerate
				$.each(tree.options.preSelected, function(index, entry){
					var node = tree.widget.getNodeByKey('' + entry.key);	
					
					if (node) {
						if ((tree.options.selectMode >= 10) && (entry.selectState)) {
							node.setSelectState(entry.selectState);
						}
						else {
							node.setSelected(entry.selectState > 0);
						}
					}
					
				});
			}
			
			// We are done, lower the semaphore
			tree.inPreselection = false;

			
		},
		
		
		/*** OVERRIDE virtual methods ***/ 
		// `this`       : is this extension object
		// `this._base` : the Fancytree instance
		// `this._super`: the virtual function that was overridden (member of prev. extension or Fancytree)
		
		treeInit: function(ctx) {
			var tree = this;  // equal to ctx.tree
			var opts = tree.options.docebo;
			
			// This must be first!!!
			this._super(ctx);

			// See selectMode definitions above
			if (tree.options.selectMode === 11) {
				tree.options.docebo.stopOnParents = false;
			}
			
			// INPUT names 
			selectedName = opts.selectedInputName ? opts.selectedInputName + '-json' : "fancytree_selected_nodes";
			activeName = opts.activeInputName ? opts.activeInputName + '-json'  : "fancytree_active_node";
			selectModeName =  opts.selectModeInputName ? opts.selectModeInputName + '-json' : "fancytree-selectmode";

			//tree reloading options
			if (opts.reloadTreeDialog) {
				if (opts.reloadTreeDialog.title) { tree.ext.docebo.reloadTreeDialogTitle = opts.reloadTreeDialog.title; }
				if (opts.reloadTreeDialog.text) { tree.ext.docebo.reloadTreeDialogText = opts.reloadTreeDialog.text; }
				if (opts.reloadTreeDialog.button) { tree.ext.docebo.reloadTreeDialogButton = opts.reloadTreeDialog.button; }
			}
			if (opts.reloadTreeHandler) { tree.ext.docebo.reloadTreeHandler = opts.reloadTreeHandler; }

			// Preselect nodes
			tree._triggerTreeEvent('beforePreselection')
			tree._local._preSelectNodes(ctx);
			tree._triggerTreeEvent('afterPreselection')
			
			// List on Selected INPUT field change and use its content to set tree selections
			// That way if someone wants to set the tree selection, just have to fill that field with JSON string and .trigger('change')
			$(tree.$container).closest('form').on('change', 'input[name="' + selectedName + '"]', function(event) {
				if (!inFilling) {
					tree.options.preSelected = jQuery.parseJSON($(this).val());
					if (tree.options.preSelected) {
						tree.ext.docebo._preSelectNodes(ctx);
					}
				}
			});

			// Listen around for a submit button clicked, as part of a surrounding form and fill the inpu fields
			$(tree.$container).closest('form').on('click', 'input[type="submit"]', function() {
				tree.ext.docebo._fillSelectionResultFields();
			});
			
			
			
		},
		
		treeDestroy: function(ctx){
			this._super(ctx);
		},



		/**
		 * Overriding Hooked method called to render a node. Calling native one for native selectModes
		 */
		nodeRenderStatus: function(ctx, flag, selectState) {
			// Handle Fancytree "standard" select modes using parent, overriden method (select modes 1..3)
			this._super(ctx, flag);
			
			// Any select mode less than 10 is not ours (docebo). Get out
			/*
			if (ctx.tree.options.selectMode < 10) {
				return;
			}
			*/
			
			var node = ctx.node;
			// Add this class if node has any children selected, giving visual feedback to the user 
			// BUT ONLY if the node itself is not selected 
			if (!node.isRoot() && (node.hasSelectedChildren > 0) && !(node.partsel) && !(node.selected) && !(node.selectState) ) {
				$(node.span).addClass('fancytree-has-children-selected');
			}
			else {
				$(node.span).removeClass('fancytree-has-children-selected');
			}
			
		},
		
		
		/**
		 * Behave differently if Title is clicked when it is expanded and is a folder: do NOT collapse
		 * 
		 */
		nodeClick: function(ctx) {
			var event = ctx.originalEvent;
			var	targetType = ctx.targetType;
			var	node = ctx.node;
			
			// If: Folder, Expanded, Title.... set focus and activate node only
			if (node.folder && node.expanded && targetType=='title') {
				// This code is taken from the super method
				//
				this.nodeSetFocus(ctx);
				this._callHook("nodeSetActive", ctx, true);
				if(event.target.localName === "a" && event.target.className === "fancytree-title"){
					event.preventDefault();
				}
				//
				
				// ? Return... what? (not clear from super method)
				return;
			}
			// Otherwise, call the super method
			else {
				return this._super(ctx);
			}
			
		},
		
		/**
		 * 
		 */
		_updateParentsSelectionStatus: function(node) {
			
			var that = this;
			
			node.visitParents(function(parentNode){
				
				// If current node (node) is selected in any way, then all parents must be set to have selected child/children
				if (node.selected || node.partsel || node.selectState) {
					parentNode.hasSelectedChildren = 1;
				}
				// Otherwise, we have to travers the parent's children to check if there is some node selected
				else {
					var i, l, child, state,
						children = parentNode.children,
						someChildSelected = false;

					// Enumerate children and check selections. Trust children about its status and do not recurse!
					for( i=0, l=children.length; i<l; i++ ){
						child = children[i];
						// No recurse! We trust this children: if it has already set hasSelectedChildren.. then ok
						if( child.selected || child.partsel || child.selectState || child.hasSelectedChildren) {
							someChildSelected = true;
							break;
						}
					}
					
					parentNode.hasSelectedChildren = someChildSelected;
				}
				
				// Finally, render the parent status
				parentNode.renderStatus();
			});
			
		},
		
		
		/**
		 * Overriding Hooked method, called when node "checkbox" is "selected" (clicked or otherwise set). 
		 * Handle selectMode=10+. If it is less than 10, call original (parent) method.
		 * 
		 * This method can be used to directly set a selectState and selection status OR,
		 * if called repeatedly, will cycle the node selectstate/selection status.
		 * Original fancytree provides only Yes/No, while we need None/This node only/This & Descendants.
		 * This is select Mode 10+.
		 * 
		 * We add another (optional) parameter (selectState), which is Docebo specific  (0,1,2)
		 * 
		 */
		nodeSetSelected: function(ctx, flag, selectState) {
			// Make out live easyier, define local variables
			var node = ctx.node,
				tree = ctx.tree,
				opts = ctx.options;
			
			// Handle Fancytree "standard" select modes using parent, overriden method (select modes 1..3)
			if (tree.options.selectMode < 10) {
				// But add our own visual feedback, to indicate if any branch is selected down the tree (grey box)
				this._super(ctx, flag);
				tree.ext.docebo._updateParentsSelectionStatus(node);
				return;
			}
			

			// Skip unselectable nodes
			if( node.unselectable){
				return;
			}

			// Skip node if "beforeSelect" event returns false
			if ( this._triggerNodeEvent("beforeSelect", node, ctx.originalEvent) === false ){
				return !!node.selected;
			}			

			if (node.folder) {
				// Directly set selectState
				
				if (typeof selectState != "undefined") {
					selectState = parseInt(selectState);
					node.selectState = selectState;
					
					switch (selectState) {
						case 0:
							node.selected = false;
							node.partsel = false;
							node.visit(function(targetNode){
								targetNode.partsel = false;
								targetNode.selected = false;
								targetNode.unselectable = false;
								targetNode.selectState = 0;
								targetNode.hasSelectedChildren = false;
							},false);
							break;
							
						case 1: 	
							node.selected = false;
							node.partsel = true;
							break;
							
						case 2:
							node.selected = true;
							node.partsel = false;
							node.visit(function(targetNode){
								targetNode.partsel = true;
								targetNode.selected = false;
								targetNode.unselectable = true;
								targetNode.selectState = 1;
							},false);
							break;
					}
				}
				// OR Cycle selectStates instead
				else {
					if (!node.isSelected() && !node.isPartsel()) {
						node.selected = false;
						node.partsel = true;
						node.selectState = 1;
					}
					else if (!node.isSelected() && node.isPartsel()) {
						node.selected = true;
						node.partsel = false;
						node.selectState = 2;
						
						node.visit(function(targetNode){
							targetNode.partsel = true;
							targetNode.selected = false;
							targetNode.unselectable = true;
							targetNode.selectState = 1;
						},false);
					}
					else if (node.isSelected() && !node.isPartsel()) {
						node.selected = false;
						node.partsel = false;
						node.selectState = 0;
						
						node.visit(function(targetNode){
							targetNode.partsel = false;
							targetNode.selected = false;
							targetNode.unselectable = false;
							targetNode.selectState = 0;
							targetNode.hasSelectedChildren = false;
						},false);
					}
				}
			}
			// NON-folder nodes may have only 2 states:  None or Partialy selected (this only)
			else {
				
				// Directly set selectState
				if (typeof selectState != "undefined") {
					selectState = parseInt(selectState);
					node.selectState = selectState;
					
					switch (selectState) {
						case 0:
							node.selected = false;
							node.partsel = false;
							break;
						case 1: 	
							node.selected = false;
							node.partsel = true;
							break;
						case 2:
							node.selected = true;
							node.partsel = false;
							break;
					}
					
				}
				// Or cycle stets
				else {	
					if (!node.isSelected() && !node.isPartsel()) {
						node.selected = false;
						node.partsel = true;
						node.selectState = 1;
					}
					else if (!node.isSelected() && node.isPartsel()) {
						node.selected = false;
						node.partsel = false;
						node.selectState = 0;
					}
				}
				
			}


			// Render newly acquired statuses of the children
			var someSelected = false;
			node.visit(function(targetNode) {
				if( targetNode.selected || targetNode.partsel || targetNode.selectState) {
					someSelected = true;
				}
				targetNode.renderStatus();
			});
			node.hasSelectedChildren = someSelected;
			node.renderStatus();

			tree.ext.docebo._updateParentsSelectionStatus(node);
			
			tree.lastSelectedNode = (node.selected || node.partsel)  ? node : null;
			
			// Do not fire "select" event during filling preselections
			if (!tree.inPreselection) {
				tree._triggerNodeEvent("select", ctx);
			}	
				
		},
		
		
		/* 
		 * Extension's custom methods
		 * 
		 * These methods are called like this: 
		 * 		$('#tree').fancytree("getTree").ext.<extension-name>._customMethod() 
		 * 
		 * Note: method name must start with "underscore"!
		 */
		
		
		/**
		 * Return list of selected nodes. Selection logic depends on Tree select mode.
		 * Callable as $('#tree').fancytree("getTree").ext.docebo._getSelectedNodes();
		 * 
		 * this: Fancytree instance
		 */
		_getSelectedNodes: function(stopOnParents) {
			var tree = this;  // fancytree instance
			var nodeList = [];
			
			// Use original method for selectMode less than 10!!
			if (tree.options.selectMode < 10) {
				return tree.getSelectedNodes(stopOnParents);
			}
			
			// Walk through all nodes, starting from root
			// SKIP whole branche if it's own 'root' is in selectState = 2 and if stopOnParents == true
			tree.rootNode.visit(function(node){
				if( node.selectState != 0 ) {
					nodeList.push(node);
					if( (stopOnParents === true) && (node.selectState == 2) ){
						return "skip"; // stop processing this branch
					}
				}
			});
			return nodeList;
		},
		
		/**
		 * Return list of selected nodes keys. Selection logic depends on Tree select mode.
		 * Callable as $('#tree').fancytree("getTree").ext.docebo._getSelectedNodesKeys();
		 * 
		 * this: Fancytree instance
		 */
		_getSelectedNodesKeys: function(stopOnParents) {
			var tree = this;  // fancytree instance
			var nodeList = [];
			
			// Use original method for selectMode less than 10!!
			if (tree.options.selectMode < 10) {
				return tree.getSelectedNodes(stopOnParents);
			}
			
			// Walk through all nodes, starting from root
			// SKIP whole branche if it's own 'root' is in selectState = 2 and if stopOnParents == true
			tree.rootNode.visit(function(node){
				if( node.selectState != 0 ) {
					nodeList.push(node.key);
					if( (stopOnParents === true) && (node.selectState == 2) ){
						return "skip"; // stop processing this branch
					}
				}
			});
			return nodeList;
		},
		
		
		
		/**
		 * 
		 */
		_fillSelectionResultFields: function() {
			
						
			
			var tree = this;
			var docebo = tree.ext.docebo;
			var opts = tree.options.docebo;
			var nodeList = [];
			var dataArray = [];
			
			// Get selected nodes; skip branches according to Docebo extension option (defaults to true) for select mode 3 and 10
			if ( (tree.options.selectMode === 10) || (tree.options.selectMode === 3) ) {	
				nodeList = docebo._getSelectedNodes(opts.stopOnParents);
			}
			else {
				nodeList = docebo._getSelectedNodes();
			}
			
			$.each(nodeList, function(idx, node){
				dataArray.push({key: node.key, selectState: (tree.options.selectMode >= 10) ? node.selectState : (node.selected ? 1 : 0) });
			});
			
			
			inFilling = true;
			$(':input[name="' + activeName + '"]').val((tree.activeNode) ? tree.activeNode.key : "");
			$(':input[name="' + selectModeName + '"]').val(tree.options.selectMode);
			$(':input[name="' + selectedName + '"]').val(JSON.stringify(dataArray));
			inFilling = false;
			
		},
		
		
		/**
		 * Generate Docebo specific LIST of <input>s, one per selected node, along with its selection state.
		 * New inputs are put in a hidden DIV container.
		 * 
		 * Example usage:
		 * 		$('#form-containing-fancytree').submit(function(){
		 *			$('#my-tree').fancytree("getTree").ext.docebo._generateFormElements();
		 * 			return true;
		 *		}); 
		 *
		 * 
		 */
		_generateFormElements: function(singleInputMode) {
			var tree = this;
			var docebo = tree.ext.docebo;
			var opts = tree.options.docebo;
			var nodeList = [];

			// INPUT names for selected nodes, one INPUT per selected node!
			var selectedName = opts.selectedInputName ? opts.selectedInputName : "fancytree_selected_nodes";
			
			// INPUT name for "active" node at the time of form submission		
			var activeName = opts.activeInputName ? opts.activeInputName : "fancytree_active_node";
			
			// INPUT name for "selectMode" 
			var selectModeName =  opts.selectModeInputName ? opts.selectModeInputName : "fancytree-selectmode";
			
			
			// DIV id, a hidden container wrapping INPUTs
			var id = "fancytree-selection-result-" + tree._id;
			
			// Check if there is already a DIV with inputs, remove it and create another one inside the Fancytree container
			var $result = tree.$container.find("div#" + id);
			
			if ($result.length) {
				$result.empty();
			}
			else {
				$result = $("<div>", {
					id: id
				}).hide().appendTo(tree.$container);
			}

			
			// Add INPUT with the currently Active node
			$result.append($("<input>", {
				type	: "text",
				name	: activeName,
				value	: (tree.activeNode) ? tree.activeNode.key : ""
			}));
			
			// Add INPUT with the current "selectMode" to help server side code make proper decisions 
			$result.append($("<input>", {
				type	: "text",
				name	: selectModeName,
				value	: tree.options.selectMode
			}));
			
			

			// Get selected nodes; skip branches according to Docebo extension option (defaults to true) for select mode 3 and 10
			if ( (tree.options.selectMode === 10) || (tree.options.selectMode === 3) ) {
				nodeList = docebo._getSelectedNodes(opts.stopOnParents);
			}
			else {
				nodeList = docebo._getSelectedNodes();
			}
			
			// Generate one INPUT per selected node....
			if (!(singleInputMode === true) )  {
				$.each(nodeList, function(idx, node){
					$result.append($("<input>", {
						type: "text",
						name: selectedName + "[" + node.key + "]",
						value: (tree.options.selectMode >= 10) ? node.selectState : (node.selected ? 1 : 0) 
					}));
				});
			}
			// ... OR a single input with JSON infor about selected nodes
			else {
				var dataArray = [];
				$.each(nodeList, function(idx, node){
					dataArray.push({key: node.key, state: (tree.options.selectMode >= 10) ? node.selectState : (node.selected ? 1 : 0) });
				});
					
				$result.append($("<input>", {
					type	: "text",
					name	: selectedName,
					value	: JSON.stringify(dataArray)
				}));
			}
			
			
		},
		
		
		/**
		 * Select all tree nodes; use different approaches for different select modes
		 * For "native" modes, visit all nodoes and set select to true; for Docebo specific (10,11...), just select root node at state 2 (this and desc)
		 */
		_selectAllNodes: function() {
			var tree = this;
			
			if (tree.options.selectMode >= 10) {
				tree.rootNode.children[0].setSelectState(2);
			}
			else {
				tree.visit(function(node) {
					node.setSelected(1);
				});
			}
		},

		/**
		 * Deselect ALL nodes
		 */
		_deSelectAllNodes: function(){
			var tree = this;
			if (tree.options.selectMode >= 10) {
				tree.rootNode.children[0].setSelectState(0);
			}
			else {
				tree.visit(function(node) {
					node.setSelected(false);
				});
			}
		},



		//tree reloading handling parameters
		reloadTreeDialogTitle: '',
		reloadTreeDialogText: '',
		reloadTreeDialogButton: '',
		reloadTreeHandler: false,
		isRequestingUpdate: false,


		/**
		 * Handle all necessary operations to handle tree forced reloading
		 * @param ctx context (usually contains info about the tree itself and the involved node)
		 * @param source source object for ajax request
		 * @param data ajax response data from operation request
		 */
		_handleTreeReloading: function(ctx, source, data) {

			var tree = ctx.tree;

			if (!tree.ext.docebo.isRequestingUpdate) {
				//opening a dialog informing the user that the tree has changed and needs to be reloaded
				tree.ext.docebo.isRequestingUpdate = true;

				//create the reload message dialog
				var unique = '_'+(new Date().getTime());
				var dialogId = 'update_tree_confirm' + unique;
				$('<div/>').dialog2({
					autoOpen: true, // We will open the dialog later
					id: dialogId,
					modalClass: 'modal-update-tree',
					title: tree.ext.docebo.reloadTreeDialogTitle,
					showCloseHandle: true,
					removeOnClose: false,
					closeOnEscape: true,
					closeOnOverlayClick: true
				});
				$('.modal.modal-update-tree').find('.modal-body').html(tree.ext.docebo.reloadTreeDialogText);
				$('.modal.modal-update-tree').find('.modal-footer').html('<input class="btn confirm-btn" type="button" value="'+tree.ext.docebo.reloadTreeDialogButton+'" name="confirm" id="update-tree-confirm-button">');

				//manage the dialog closing
				$('.modal.modal-update-tree').off('dialog2.closed');
				$('.modal.modal-update-tree').on('dialog2.closed', function() {
					tree.ext.docebo.isRequestingUpdate = false;
					//allow to call a custom function for tree reloading
					if (tree.ext.docebo.reloadTreeHandler) {
						tree.ext.docebo.reloadTreeHandler.call(this, ctx, source);
					}
				});

				//manage dialog confirm button click
				$('.modal.modal-update-tree .modal-footer .confirm-btn').off('click');
				$('.modal.modal-update-tree .modal-footer .confirm-btn').on('click', function() {
					tree.ext.docebo.isRequestingUpdate = false;
					$('#' + dialogId).dialog2('close');
				});

			} else {
				//avoid the opening of multiple confirm dialog, we need just one !
				Docebo.log("Tree updating dialog already opened ...");
			}
		},


		/**
		 * OVERRIDDEN METHOD
		 */
		nodeLoadChildren: function(ctx, source) {
			var ajax, delay, dfd,
				tree = ctx.tree,
				node = ctx.node;

			var _assert = $.ui.fancytree.assert;

			if($.isFunction(source)){
				source = source();
			}

			// TOTHINK: move to 'ajax' extension?
			if(source.url){
				// `source` is an Ajax options object
				ajax = $.extend({}, ctx.options.ajax, source);
				if(ajax.debugDelay){
					// simulate a slow server
					delay = ajax.debugDelay;
					if($.isArray(delay)){ // random delay range [min..max]
						delay = delay[0] + Math.random() * (delay[1] - delay[0]);
					}

					node.debug("nodeLoadChildren waiting debug delay " + Math.round(delay) + "ms");
					ajax.debugDelay = false;
					dfd = $.Deferred(function (dfd) {
						setTimeout(function () {
							$.ajax(ajax)
								.done(function () {	dfd.resolveWith(this, arguments); })
								.fail(function () {	dfd.rejectWith(this, arguments); });
						}, delay);
					});
				}else{
					dfd = $.ajax(ajax);
				}

				// Defer the deferred: we want to be able to reject, even if ajax
				// resolved ok.
				source = new $.Deferred();
				dfd.done(function (data, textStatus, jqXHR) {
					var errorObj, res;
					if(this.dataType === "json" && typeof data === "string"){
						$.error("Ajax request returned a string (did you get the JSON dataType wrong?).");
					}

					//--- CUSTOM PART: allow the tree reloading if the server requested it ---
					if (data.update_tree) {
						tree.ext.docebo._handleTreeReloading.call(tree, ctx, source, data);
						return; //don't do other actions here, tree needs to be destroyed and re-created
					}
					//--- ---

					// postProcess is similar to the standard ajax dataFilter hook,
					// but it is also called for JSONP
					if( ctx.options.postProcess ){
						res = tree._triggerNodeEvent("postProcess", ctx, ctx.originalEvent, {response: data, error: null, dataType: this.dataType});
						if( res.error ) {
							errorObj = $.isPlainObject(res.error) ? res.error : {message: res.error};
							errorObj = tree._makeHookContext(node, null, errorObj);
							source.rejectWith(this, [errorObj]);
							return;
						}
						data = $.isArray(res) ? res : data;

					} else if (data && data.hasOwnProperty("d") && ctx.options.enableAspx ) {
						// Process ASPX WebMethod JSON object inside "d" property
						data = (typeof data.d === "string") ? $.parseJSON(data.d) : data.d;
					}
					source.resolveWith(this, [data]);
				}).fail(function (jqXHR, textStatus, errorThrown) {
					var errorObj = tree._makeHookContext(node, null, {
						error: jqXHR,
						args: Array.prototype.slice.call(arguments),
						message: errorThrown,
						details: jqXHR.status + ": " + errorThrown
					});
					source.rejectWith(this, [errorObj]);
				});
			}
			// #383: accept and convert ECMAScript 6 Promise
			if( $.isFunction(source.then) && $.isFunction(source["catch"]) ) {
				dfd = source;
				source = new $.Deferred();
				dfd.then(function(value){
					source.resolve(value);
				}, function(reason){
					source.reject(reason);
				});
			}
			if($.isFunction(source.promise)){
				// `source` is a deferred, i.e. ajax request
				_assert(!node.isLoading(), "recursive load");
				// node._isLoading = true;
				tree.nodeSetStatus(ctx, "loading");

				source.done(function (children) {
					tree.nodeSetStatus(ctx, "ok");
				}).fail(function(error){
					var ctxErr;
					if (error.node && error.error && error.message) {
						// error is already a context object
						ctxErr = error;
					} else {
						ctxErr = tree._makeHookContext(node, null, {
							error: error, // it can be jqXHR or any custom error
							args: Array.prototype.slice.call(arguments),
							message: error ? (error.message || error.toString()) : ""
						});
					}
					if( tree._triggerNodeEvent("loadError", ctxErr, null) !== false ) {
						tree.nodeSetStatus(ctx, "error", ctxErr.message, ctxErr.details);
					}
				});
			}
			// $.when(source) resolves also for non-deferreds
			return $.when(source).done(function(children){
				var metaData;

				if( $.isPlainObject(children) ){
					// We got {foo: 'abc', children: [...]}
					// Copy extra properties to tree.data.foo
					_assert($.isArray(children.children), "source must contain (or be) an array of children");
					_assert(node.isRootNode(), "source may only be an object for root nodes");
					metaData = children;
					children = children.children;
					delete metaData.children;
					$.extend(tree.data, metaData);
				}
				_assert($.isArray(children), "expected array of children");
				node._setChildren(children);
				// trigger fancytreeloadchildren
				tree._triggerNodeEvent("loadChildren", node);
				// }).always(function(){
				// 	node._isLoading = false;
			});
		}

	});
	
	
}(jQuery));