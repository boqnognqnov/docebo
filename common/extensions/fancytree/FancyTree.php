<?php

/**
 * Widget to display Fancytree based .. Tree
 *
 *
 */
class FancyTree extends CWidget {
	
	/**
	 * Orgchart (fancytree) selection modes constants (other than native ones: 1,2,3)
	 * These modes are added in/by the FancyTree Doecebo extension.
	 * @var number
	 */
	const SELECT_MODE_SINGLE			= 1; 	// Allow selection of ONLY ONE node
	const SELECT_MODE_MULTI				= 2; 	// Allow multiple selection
	const SELECT_MODE_MULTI_AND_PARENTS	= 3; 	// Allow multiple selection
	const SELECT_MODE_DOCEBO  			= 10;   // Allow selection of  THIS / THIS & DESC; do NOT include descentans in selections
	const SELECT_MODE_DOCEBO2   		= 11;	// Same as above, but DO include descentans in selections
	const SELECT_MODE_DEFAULT 			= self::SELECT_MODE_DOCEBO;

	/**
	 * 
	 * @var string
	 */
	public $id = false;

	// If true, load non-minified scripts
	public $debug = false;

	// Tree Skin
	public $skin = "skin-docebo";

	// Associative array of data, compatible with Fancytree
	public $treeData = array();

	// Array of selected Keys:  array( array("key" => "<key>" "selectState" => "0|1|2"), ... )
	// where selectState has only meaning if selectMode >= 10
	public $preSelected = array();

	// Array of form INPUT field names used to generate selection results
	public $formFieldNames = array(
		'selectedInputName' => 'fancytree-selected-nodes',
		'activeInputName' => 'fancytree-active-node',
		'selectModeInputName' => 'fancytree-selectmode'
	);

	// Fancytree instance select mode 
	public $selectMode = self::SELECT_MODE_DEFAULT;

	
	/**
	 * Fancytree options (http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions)
	 * An array of  (<option> => <string>)
	 * 
	 * Pay attention:
	 * 		1. If the <string> consist "function" it will be echoed literaly, e.g.
	 * 			'lazyLoad' => 'function(event,data) {}'
	 * 			ends up as  JS object  lazyLoad: function() {.....}
	 * 		2. If the <options> === 'dnd', the <string> is treated again literaly (like in case 1.), e.g.
	 * 			'dnd' => '{ x: "10",  y: "100",  myfunc: function() {....}}'
	 * 			
	 *		3. Otherwise, it is considered a string to be JS-encoded (JSON encoded), e.g.
	 *			'option' =>  array('x' => 10, 'y' => '100')
	 *			Result is JS object (by simply JSON encoding the above):   {option: {x:"10", y: "100"}}  			
	 * 		
	 * @var array
	 */
	public $fancyOptions = array();
	
	/**
	 * Additional attributes for the DIV element surrounding the fancytree
	 * Note: "id" attribute is IGNORED!
	 * @var array
	 */
	public $htmlOptions = array();
	
	/**
	 * Array of  (<event> => <function-string>), e.g.
	 * 		'click' => 'function(event,data) { console.log("Node clicked"); }'
	 * 
	 * Quite similar to case 1. of $fancyOptions     
	 * @var array
	 */
	public $eventHandlers = array();

	
	/**
	 * Are folders loaded in "lazy mode" or not. By default, they are not, i.e. they are loaded (their children) upon building the tree.
	 * You really should know what you are doing if you enable lazyLoad!  
	 * @var boolean
	 */
	public $lazyLoad = false;
	
	public $view = 'index';
	
	
	/**
	 * Table template used for 'table' view extension, if activated (check Fancytree Wiki for extensions)
	 * @var string
	 */
	public $tableTemplate = false;
	
	/**
	 * SEARCH INPUT field name to search the tree, powered by 'filter' extension 
	 * and
	 * Element ID (!) to clear the filter upon clikcing on it 
	 * @var string
	 */
	public $filterInputFieldName 	= "fancytree-filter-input";
	public $filterClearElemId 		= "fancytree-filter-clear";
	

	// Holds assets url
	protected $_assetsUrl;
	
	/**
	 * If "table" view extension is going to be used (see tableTemplate). NO by default.
	 * @var boolean
	 */
	protected $isTableTree = false;

	public $showActions = false;

	public $actions = array();

	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init() {
		// Prevents the extension from registering scripts and publishing assets when ran from the command line.
		if (Yii::app() instanceof CConsoleApplication) {
			return;
		}
		
		$this->register();

		if (!$this->id) {
			$this->id = 'fancytree-widget-' . time();
		}
		
		
		// Build a default TABLE for "table" extension, if activated, of course. By default, it is NOT.
		// This is because we MUST have a table, some table
		if ($this->tableTemplate === false) {
			$this->tableTemplate = '	
				<table id="'.$this->id.'" width="100%">
    				<colgroup>
    					<col></col>
    					<col></col>
    					<col></col>
	    				<col></col>
    					<col></col>
    				</colgroup>
    				<thead>
		        		<tr> 
	        				<th></th>
	        				<th></th> 
	        				<th></th> 
	        				<th></th> 
	        				<th></th> 
	        			</tr>
	    			</thead>
	    			<tbody>
	    			</tbody>
				</table>	
			';
		}
		
		
	}


	/**
	 * Register Fancytree JS/CSS for later rendeing.
	 *
	 * Call this method directly if you are NOT using this widget rendering, but want to make your own Fancytree JS calls/code/whatever
	 * and you just need Fancytree JS/CSS loaded.
	 * Note: The widget rendering is calling this automaticaly. No need to call it explicitely!
	 *
	 */
	public function register() {

		// Register path alias
		if (Yii::getPathOfAlias('fancytree') === false) {
			Yii::setPathOfAlias('fancytree', realpath(dirname(__FILE__)));
		}

		$cs = Yii::app()->getClientScript();
		$baseUrl = $this->getAssetsUrl();

		// jQuery UI is Required
//		$cs->registerPackage('jquery.ui');

		// Register Fancytree JS/CSS
		//For now don't use the .min version of fancytree, there is some problem with require.js
		$cs->registerScriptFile($baseUrl . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($baseUrl . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);

		// If someone wants to add actions to the Tree, let's add some Script to help him do so
		if ($this->showActions) {
			$cs->registerScriptFile($baseUrl . '/jquery.fancytree.docebo-actions.js', CClientScript::POS_HEAD);
		}

		if ($this->skin !== false) {
			$cs->registerCssFile($baseUrl . '/' . $this->skin . '/ui.fancytree.css');
		}


	}


	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl() {
		if(!isset($this->_assetsUrl)){
			$assetsPath = Yii::getPathOfAlias('common.extensions.fancytree');
			$assetsUrl = Yii::app()->getAssetManager()->publish($assetsPath);
			$this->_assetsUrl = $assetsUrl;
		}
		return $this->_assetsUrl;
	}


	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run() {
		// Maybe we are running this tree using "table" extension? In this case a table template is required and will be put in tree wrapper
		if (	
				$this->tableTemplate &&
				isset($this->fancyOptions['extensions']) &&
				is_array($this->fancyOptions['extensions']) && 
				preg_match('/table/', implode(',',$this->fancyOptions['extensions']))) {
			$this->isTableTree = true;
		}
		$html = $this->render($this->view);
	}


}
