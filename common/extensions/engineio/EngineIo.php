<?php
class EngineIo extends CApplicationComponent
{
    protected $assetsUrl;
    
    public function init() {
        if(Yii::getPathOfAlias('engineio') === false) Yii::setPathOfAlias('engineio', realpath(dirname(__FILE__)));

        //Don't load it here, because it's too late and if you have require.js in the page this is loaded after it and breaks everything!!!!!
        
//        if (!Yii::app() instanceof CConsoleApplication && (Yii::app()->user->isAdmin || Yii::app()->user->isGodadmin)) {
//			Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl().'/js/engine.io.js');
//		}

        parent::init();
    }
    
    public function getAssetsUrl() {
        if(!isset($this->assetsUrl))
            $this->assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('engineio.assets'));

       	return $this->assetsUrl;
    }
}
