<?php 
	$id = $this->id;
	Yii::app()->getClientScript()->registerScriptFile(Yii::app()->player->getPlayerAssetsUrl() . '/js/jquery.knob.js' , CClientScript::POS_HEAD);
?>






	<div class="row-fluid">
	
		<div class="span10">
		
			<div id="pluploader-container-<?= $id ?>" class="span6">
				<div class="pickfiles-wrapper">
					<a id="pickfiles-<?= $id ?>" href="javascript:;" class="btn-docebo green"><?= Yii::t('course_management', 'CHOOSE FILE') ?></a>
				</div>
			</div>
			
			<div class="span6">
				<div id="knob-wrapper-<?= $id ?>" style="display: none;">
					<input id="pluploader-knob-<?= $id ?>" value="0" />
				</div>
			</div>
			
		</div>
		
		
	</div>
	
	
	<div class="row-fluid filename">
		<div id="pluploader-filename-<?= $id ?>"></div>
		<div id="pluploader-error-<?= $id ?>" style="display: none; color: red;"><span></span></div>
	</div>




<script type="text/javascript">

	function layoutInit<?= $id ?>(up,params) {
	} 
	

	function layoutBeforeUpload<?= $id ?>(up,file) {
		$('#pluploader-error-<?= $id ?>').hide();
		$('#pluploader-filename-<?= $id ?>').text('');
	} 

	function layoutFilesAdded<?= $id ?>(up,file) {
		$('#knob-wrapper-<?= $id ?>').show();
		$('#pluploader-filename-<?= $id ?>').text(file.name);
		$('#pickfiles-<?= $id ?>').attr('disabled', true);
		up.disableBrowse(true);
	} 


	function layoutUploadProgress<?= $id ?>(up,file) {
		$("#pluploader-knob-<?= $id ?>").val(file.percent).trigger('change');
	}

	function layoutError<?= $id ?>(up,error) {
		$('#pluploader-filename-<?= $id ?>').text('');
		$('#pluploader-error-<?= $id ?>').show();
		$('#pluploader-error-<?= $id ?> > span').text('Error ' + error.code + ': ' + error.message);
		$('#knob-wrapper-<?= $id ?>').hide();
		$('#pickfiles-<?= $id ?>').attr('disabled', false);
		up.disableBrowse(false);
	} 


	function layoutUploadComplete<?= $id ?>(up,file) {
		up.disableBrowse(false);
		$('#pickfiles-<?= $id ?>').attr('disabled', false);
	} 
	

	$(function(){
	
		$("#pluploader-knob-<?= $id ?>").knob({
			'min':0,
			'max':100,
			'readOnly': true,
			'fgColor': '#6cc267',
			'bgColor': '#cccccc',
			'thickness':.3,
			'width': 30,
			'height':30,
			'draw' : function () {
						//var displayValue = Math.round(this.cv);
						$(this.i).val('');
			}
		});

	
	});
	
	
	 
	

</script>




