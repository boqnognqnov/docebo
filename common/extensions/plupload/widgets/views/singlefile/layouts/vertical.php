	<?php $id = $this->id; ?>


	
	<div class="row-fluid">
	
		<div class="span12">
		
			<div class="row-fluid pickfiles-button">
				<div class="span12">
					<div id="pluploader-container-<?= $id ?>">
						<a id="pickfiles-<?= $id ?>" href="javascript:;" class="span12 btn-docebo green"><?= $this->buttonText ?></a>
					</div>
				</div>
			</div>
			
			<div class="row-fluid progress-bar">
				<div id="pluploader-progress-<?= $id ?>" class="span12">
					<div class="docebo-progress progress progress-striped active">
						<div id="pluploader-progress-bar-<?= $id ?>" class="bar full-height" style="width: 0%;"></div>
					</div>
				</div>
				<!-- 
				<div class="span3">
					<div class="text-right"><span id="pluploader-progress-number-<?= $id ?>"></span></div>
				</div>
				 -->
			</div>
			
			<div class="row-fluid filename">
				<div class="span12">
					<span id="pluploader-filename-<?= $id ?>"></span>
					<br>
					<div id="pluploader-error-<?= $id ?>" style="display: none;" class="alert-danger"><span></span></div>
				</div>
			</div>
			
		
		
		</div>
		
		
	</div>
	
	
	
	
	




<script type="text/javascript">

	function layoutInit<?= $id ?>(up,params) {
	} 
	

	function layoutBeforeUpload<?= $id ?>(up,file) {
		$('#pluploader-error-<?= $id ?>').hide();
		$('#pluploader-progress-<?= $id ?>').show();
		$('#pluploader-progress-bar-<?= $id ?>').css('width', '0%');
		$('#pluploader-filename-<?= $id ?>').text('');
	} 

	function layoutFilesAdded<?= $id ?>(up,file) {
		$('#pluploader-filename-<?= $id ?>').text(file.name);
		$('#pluploader-progress-<?= $id ?> > div').removeClass('progress-success').addClass('progress-striped');
	} 



	function layoutUploadProgress<?= $id ?>(up,file) {
		$('#pluploader-progress-bar-<?= $id ?>').css('width', file.percent + '%');
		$('#pluploader-progress-number-<?= $id ?>').text(file.percent + ' %');
	}

	function layoutError<?= $id ?>(up,error) {
		$('#pluploader-filename-<?= $id ?>').text('');
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-error-<?= $id ?>').show();
		$('#pluploader-error-<?= $id ?> > span').text('Error ' + error.code + ': ' + error.message);
	} 


	function layoutUploadComplete<?= $id ?>(up,file) {
		$('#pluploader-progress-<?= $id ?> > div').addClass('progress-success').removeClass('progress-striped');
	} 
	
	
	 
	

</script>




