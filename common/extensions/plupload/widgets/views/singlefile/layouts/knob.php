<?php 
	$id = $this->id;
	Yii::app()->getClientScript()->registerScriptFile(Yii::app()->player->getPlayerAssetsUrl() . '/js/jquery.knob.js' , CClientScript::POS_HEAD);
?>


<div class="row-fluid">

	<div class="span6">
		<div id="pluploader-container-<?= $id ?>">
			<!-- 
			<a id="pickfiles-<?= $id ?>" href="javascript:;" class="btn-docebo green">Select file</a>
			 -->
		</div>

		<div id='pluploader-dropzone-<?= $id ?>'? class="plup-knob-dropzone" style="position: relative;">
		
			<div id="drop-hint-<?= $id ?>" class="plup-knob-drophint">Click to select a file or <br>Drag & Drop file</div>
		
			<div id="knob-wrapper-<?= $id ?>" style="display: none;" class="plup-knob-wrapper">
				<input id="pluploader-knob-<?= $id ?>" value="0" />
			</div>
		
			<div id="pluploader-error-<?= $id ?>" style="display: none;" class="alert-danger"><span></span></div>
		
		</div>
		
	</div>
	

</div>


<div class="row-fluid">
	<div class="span6">
		<span id="pluploader-filename-<?= $id ?>" class="plup-knob-filename"></span>
	</div>
</div>





<script type="text/javascript">

	function layoutInit<?= $id ?>(up,params) {
	} 
	

	function layoutBeforeUpload<?= $id ?>(up,file) {
		$('#pluploader-error-<?= $id ?>').hide();
		$('#pluploader-filename-<?= $id ?>').text('');
	} 

	function layoutFilesAdded<?= $id ?>(up,file) {
		$('#drop-hint-<?= $id ?>').hide();
		$('#knob-wrapper-<?= $id ?>').show();
		$('#pluploader-filename-<?= $id ?>').text(file.name);
	} 


	function layoutUploadProgress<?= $id ?>(up,file) {
		$("#pluploader-knob-<?= $id ?>").val(file.percent).trigger('change');
	}

	function layoutError<?= $id ?>(up,error) {
		$('#pluploader-filename-<?= $id ?>').text('');
		$('#pluploader-error-<?= $id ?>').show();
		$('#pluploader-error-<?= $id ?> > span').text('Error ' + error.code + ': ' + error.message);
	} 


	function layoutUploadComplete<?= $id ?>(up,file) {
		
	} 
	

	$(function(){
	
		$("#pluploader-knob-<?= $id ?>").knob({
			'min':0,
			'max':100,
			'readOnly': true,
			'fgColor': '#6cc267',
			'bgColor': '#cccccc',
			'thickness':.25,
			'width': 80,
			'height':80,
			'draw' : function () {
						var displayValue = Math.round(this.cv);
						$(this.i).val(displayValue + '%')
			}
		});

	
	});
	
	
	 
	

</script>




