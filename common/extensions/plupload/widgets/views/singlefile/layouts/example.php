<?php 
	$id = $this->id;
?>


<div class="row-fluid">

	<div class="span6">
		<div id="pluploader-container-<?= $id ?>">
			<a id="pickfiles-<?= $id ?>" href="javascript:return false;" class="btn-docebo green plup-button">Select file</a>
		</div>
	</div>
	
	<div class="span5">
		<div id="pluploader-progress-<?= $id ?>">
			<div class="docebo-progress progress progress-striped active plup-thin">
				<div id="pluploader-progress-bar-<?= $id ?>" class="bar full-height" style="width: 0%;"></div>
			</div>
		</div>
		
		<div id="pluploader-error-<?= $id ?>" style="display: none;" class="alert-danger"><span></span></div>
		
	</div>
	
	
	<div class="span1">
		<div class="pull-right"><span id="pluploader-progress-number-<?= $id ?>"></span></div>
	</div>
	
	

</div>


<div class="row-fluid">
	<div class="span6">
		<span id="pluploader-filename-<?= $id ?>"></span>
	</div>
</div>





<script type="text/javascript">

	function layoutInit<?= $id ?>(up,params) {
	} 
	

	function layoutBeforeUpload<?= $id ?>(up,file) {
		$('#pluploader-error-<?= $id ?>').hide();
		$('#pluploader-progress-<?= $id ?>').show();
		$('#pluploader-progress-bar-<?= $id ?>').css('width', '0%');
		$('#pluploader-filename-<?= $id ?>').text('');

		$('#pluploader-progress-<?= $id ?> > div').addClass('progress-striped');
		$('#pluploader-progress-<?= $id ?> > div').removeClass('progress-success');
		
	} 

	function layoutFilesAdded<?= $id ?>(up,file) {
		$('#pluploader-filename-<?= $id ?>').text(file.name);
		$('#pickfiles-<?= $id ?>').addClass('disabled');
		up.disableBrowse(true);
	} 



	function layoutUploadProgress<?= $id ?>(up,file) {
		$('#pluploader-progress-bar-<?= $id ?>').css('width', file.percent + '%');
		$('#pluploader-progress-number-<?= $id ?>').text(file.percent + ' %');
	}

	function layoutError<?= $id ?>(up,error) {
		$('#pluploader-filename-<?= $id ?>').text('');
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-error-<?= $id ?>').show();
		$('#pluploader-error-<?= $id ?> > span').text('Error ' + error.code + ': ' + error.message);
		$('#pickfiles-<?= $id ?>').removeClass('disabled');
		up.disableBrowse(false);
	} 


	function layoutUploadComplete<?= $id ?>(up,file) {
		$('#pluploader-progress-<?= $id ?> > div').addClass('progress-success');
		$('#pluploader-progress-<?= $id ?> > div').removeClass('progress-striped');
		$('#pickfiles-<?= $id ?>').removeClass('disabled');
		up.disableBrowse(false);
	} 
	
	
	

</script>




