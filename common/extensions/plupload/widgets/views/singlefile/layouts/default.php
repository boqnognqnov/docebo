<?php $id = $this->id; ?>

<div class="row-fluid">

	<div class="span10">


		<div id="pluploader-container-<?= $id ?>" class="span6">
			<a id="pickfiles-<?= $id ?>" href="javascript:;" class="btn-docebo green"><?= $this->buttonText ?></a>
		</div>

		<div id="pluploader-progress-<?= $id ?>" class="span6" style="display: none">
			<div class="docebo-progress progress progress-striped active">
				<div id="pluploader-progress-bar-<?= $id ?>" class="bar full-height" style="width: 0%;"></div>
			</div>
		</div>

		<?= CHtml::hiddenField('toRemoveUpload' . $id, ''); ?>

	</div>


	<div>
		<div class="pull-right">
			<span style="margin-right: -3px" id="pluploader-progress-number-<?= $id ?>"></span>
			<a
				id="remove-uploaded-file-btn-<?= $id ?>"
				style="font-weight: bold; font-size:15px; color: red; background: url('/themes/spt/images/popover-icons.png') -2px -66px;margin-left: 4px; display: none;"
				onclick="javascript:clearFile<?= $id ?>();"
				href="#"
				class="node-delete delete-node">
				&nbsp;&nbsp;&nbsp;&nbsp;
			</a>
		</div>
	</div>

</div>


<div class="row-fluid filename">
	<div class="span12">
		<span id="pluploader-filename-<?= $id ?>"></span>
	</div>

	<div id="pluploader-error-<?= $id ?>" style="display: none;" class="alert-danger"><span></span></div>
</div>


<script type="text/javascript">

	function findFieldId(){

		var checkInput = function(el) {
			if (el && el.length > 0) {
				var tagName = el.prop('tagName');
				if (tagName && tagName.toUpperCase() == 'INPUT') {
					var id = el.attr('id');
					if (id && id != '' && id.substr(0, 9) == 'CoreUser_') {
						return true;
					}
				}
			}
			return false;
		}

		var wrapper = $('#pluploader-wrapper-<?= $id ?>');
		var parent = wrapper.parent();

		var prev = parent.prev();
		if (checkInput(prev)) {
			var id = prev.attr('id');
			if (id) {
				return id.replace('_type_field', '').replace('CoreUser_additional_', '');
			}
		}

		var next = parent.next();
		if (!checkInput(next)) {
			next = next.next();
			if (!next || !checkInput(next)) {
				return '';
			}
		}

		if (next && next.length > 0) {
			var id = next.attr('id');
			if (id) {
				return id.replace('_type_field', '').replace('CoreUser_additional_', '');
			}
		}

		return '';
	}

	var fieldId = findFieldId();

	(function associateAdditionalFieldIdToRemoveAttachmentField() {
		$('#toRemoveUpload<?= $id; ?>').attr('id', 'toRemoveUpload<?= $id; ?>___' + fieldId);
		var removalHiddenFld = $('#toRemoveUpload<?= $id; ?>___' + fieldId);
		removalHiddenFld.attr('name', removalHiddenFld.attr('id'));
	})()

	function layoutInit<?= $id ?>(up,params) {
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-wrapper-<?= $id ?>').hide();
	}

	function layoutBeforeUpload<?= $id ?>(up,file) {
		$('#pluploader-error-<?= $id ?>').hide();
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-progress-bar-<?= $id ?>').css('width', '0%');
		$('#pluploader-filename-<?= $id ?>').text('');
	}

	function stopRunningUpload(){
		publicPlUploader.pl_uploader.stop();
	}

	function clearFile<?= $id ?>(){
		layoutBeforeUpload<?= $id ?>();
		stopRunningUpload();
		$('#pluploader-progress-number-<?= $id ?>').text('');
		$('#upload-file-link<?= $id ?>').remove();
		var progressBarContainer = $('#pluploader-progress-<?= $id ?>').parent();
		var uploadedFileAnchor = progressBarContainer.children().last();
		if (uploadedFileAnchor[0] && uploadedFileAnchor[0].nodeName == 'A')
			uploadedFileAnchor.remove();

		$('#toRemoveUpload<?= $id ?>___' + fieldId).val(true);
	}

	function layoutFilesAdded<?= $id ?>(up,file) {
		$('#pluploader-filename-<?= $id ?>').text(file.name);
		$('#pluploader-progress-<?= $id ?> > div').removeClass('progress-success').addClass('progress-striped');
	}

	function layoutUploadProgress<?= $id ?>(up,file) {
		$('#pluploader-progress-bar-<?= $id ?>').css('width', file.percent + '%');
		$('#pluploader-progress-number-<?= $id ?>').text(file.percent + ' %');
		$('#pluploader-progress-number-<?= $id ?>').show();
		$('#remove-uploaded-file-btn-<?= $id ?>').css('display', 'inline');
		$('#pluploader-progress-<?= $id ?>').css('display', 'inline');
		$('#upload-file-link<?= $id ?>').remove();
	}

	function layoutError<?= $id ?>(up,error) {
		$('#pluploader-filename-<?= $id ?>').text('');
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-error-<?= $id ?>').show();
		$('#pluploader-error-<?= $id ?> > span').text('Error ' + error.code + ': ' + error.message);
	}

	function layoutUploadComplete<?= $id ?>(up,file) {
		$('#pluploader-progress-<?= $id ?> > div').addClass('progress-success').removeClass('progress-striped');
		$('#pluploader-progress-<?= $id ?>').hide();
		$('#pluploader-progress-number-<?= $id ?>').hide();
		$('#pluploader-filename-<?= $id ?>').hide();

		var progressBarContainer = $('#pluploader-progress-<?= $id ?>').parent();
		$('#upload-file-link<?= $id ?>').remove();
		if (progressBarContainer) {
			if (progressBarContainer.children().last()[0] && progressBarContainer.children().last()[0].nodeName == 'A')
				progressBarContainer.children().last().remove();
			progressBarContainer.append(
				'<span href="" id="upload-file-link<?= $id ?>">' + $('#pluploader-filename-<?= $id ?>')[0].innerHTML + '</span>'
			);
		}

		$('#toRemoveUpload<?= $id ?>___' + fieldId).val('');
	}

	((function repositionUploadedFileAnchor (){
		var pluploaderWrapper = $('#pluploader-wrapper-<?= $id ?>');
		if (pluploaderWrapper.next()[0] && pluploaderWrapper.next()[0].nodeName == 'A') {
			var uploadedFileAnchorContents = pluploaderWrapper.next()[0].outerHTML;
			pluploaderWrapper.next().remove();

			var progressBarContainer = $('#pluploader-progress-<?= $id ?>').parent();
			progressBarContainer.append(uploadedFileAnchorContents);
			$('#remove-uploaded-file-btn-<?= $id ?>').show();
		}

	})())



</script>






