

/**
 * This array will hold all uploaded (COMPLETED!) files from all SingleFile widgets currently loaded 
 */
var SingleFileWidgetsUploadedFiles = [];

var pluploaderSingleFile = function(options) {
	
	// Default Options
	this.options = {
			chunk_size			: '1mb',
			multi_selection		: false,
			runtimes			: 'html5,flash',
			browse_button		: 'pickfiles',
			container			: 'pluploader-container',
			drop_element		: 'pluploader-dropzone',
			max_file_size		: '4mb',
			url					: '',
			multipart			: true,
			multipart_params	: {},
			flash_swf_url 		: '',
			allowedExtensions	: '',
			formId				: '',
			fieldName			: '',
			fieldId				: '',
			onInit				: '',
			onUploadComplete	: '',
			onFilesAdded		: '',
			onBeforeUpload		: '',
			onUploadProgress	: '',
			onError				: ''
	};
		
	this.init(options);
		
}

/**
 * 
 */
pluploaderSingleFile.prototype = {
		
		/**
		 * 
		 */
		init: function (options) {
			
			// Merge incoming options with default ones
			this.options = $.extend({}, this.options, options);
			
			// Set file extensions whitelist, if any
			if (this.options.allowedExtensions != '') {
				this.options.filters = [{title: 'Allowed extensions', extensions: this.options.allowedExtensions}];
			}
			
			// Provide CSRF token for upload POST requests
			this.options.multipart_params.YII_CSRF_TOKEN = this.options.csrfToken;
			
			// Create PLUpload instance
			this.pl_uploader = new plupload.Uploader(this.options);
		},
			
		
		/**
		 * 
		 */
		run: function() {
			
			var me = this; 
			var pl_uploader = this.pl_uploader;
		
			if (!pl_uploader) {
				return false;
			}
			
			pl_uploader.init();
			
			// Init event hander
			pl_uploader.bind('Init', function (up, params) {
				var layoutCallback = 'layoutInit' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,params);
				}
				
				// If globally defined, call this JS callback function
				if (me.options.onInit && typeof window[me.options.onInit] == 'function') {
					window[me.options.onInit](me.options.id, params, up);
				}
			});
			
			// Just before uploading file
			pl_uploader.bind('BeforeUpload', function (up, file) {
				var layoutCallback = 'layoutBeforeUpload' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,file);
				}

				// If globally defined, call this JS callback function
				if (me.options.onBeforeUpload && typeof window[me.options.onBeforeUpload] == 'function') {
					window[me.options.onBeforeUpload](me.options.id, file, up);
				}
			});


			// Handle 'files added' event. SInce this is SingleFile uploader, we start upload immediately
			pl_uploader.bind('FilesAdded', function (up, files) {
				this.files = [files[0]];
				pl_uploader.start();
				
				var layoutCallback = 'layoutFilesAdded' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,files[0]);
				}
				
				// If globally defined, call this JS callback function
				if (me.options.onFilesAdded && typeof window[me.options.onFilesAdded] == 'function') {
					window[me.options.onFilesAdded](me.options.id, this.files[0], up);
				}
			});
			
			
			// Upload progress
			pl_uploader.bind('UploadProgress', function (up, file) {
				var layoutCallback = 'layoutUploadProgress' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,file);
				}
				
				// If globally defined, call this JS callback function
				if ( me.options.onUploadProgress && typeof window[me.options.onUploadProgress] == 'function' ) {
					window[me.options.onUploadProgress](me.options.id, file, up);
				}
			});
			
			
			// On error
			pl_uploader.bind('Error', function (up, error) {
				var layoutCallback = 'layoutError' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,error);
				}
				
				// If globally defined, call this JS callback function
				if (me.options.onError && typeof window[me.options.onError] == 'function') {
					window[me.options.onError](me.options.id, error, up);
				}
			});

			
			// On Upload complete
			pl_uploader.bind('UploadComplete', function (up, files) {
				
				// This is SINGLE file uploader, we are interested on LAST file only!
				var file = files.last();
				
				var layoutCallback = 'layoutUploadComplete' + me.options.id;
				if (typeof window[layoutCallback] == 'function') {
					window[layoutCallback](up,file);
				}
				
				// Update uploaded list
				me.updateUploadedList(file);
				
				// Find/Add <input> element and set its value to the filename
				me.setFormField(file.name);
				
				// If globally defined, call this JS callback function
				if (me.options.onUploadComplete && typeof window[me.options.onUploadComplete] == 'function') {
					window[me.options.onUploadComplete](me.options.id, file, up);
				}
			});
			

		},
		
		
		/**
		 * Find (or create) FORM field element and set its value
		 *  
		 */
		setFormField: function(value) {
			
			// No field name? Ok, get out!
			if (this.options.fieldName == '') {
				return false;
			}
			
			// Find the FORM, either closest one or directly by Id
			var $form = (this.options.formId == '') ? $('#' + this.options.container).closest('form') : $form = $('#'+this.options.formId);
			
			// No form found ? Get out! 
			if ($form.length <= 0) {
				return false;
			}

			// Ok, lets find the <input> or create it if not existant
			var $field = $form.find('input[name="'+this.options.fieldName+'"]');
			if ($field.length <= 0) {
				$field = $('<input type="hidden" />')
					.attr('name', this.options.fieldName)
					.attr('id', this.options.fieldId);
				$form.prepend($field);
			}
			
			if ($field.length <= 0) {
				return false;
			}
			
			$field.val(value);
			
		},
		
		/**
		 * A global JS variable exists to keep a list of all COMPLETED (uploaded) files by all existing widgets.
		 * 
		 * You can access this variable from anywhere as:
		 * 		SingleFileUploadedFiles['widgetID']  -> returns PlUploader 'file' Object 
		 *  
		 */
		updateUploadedList:  function(file) {
			SingleFileWidgetsUploadedFiles[this.options.id] = file;			
		}
		

		
			
};
	



