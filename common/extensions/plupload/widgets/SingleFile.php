<?php
/**
 *
 * A widget to show PLUploader based form field and upload a file through AJAX, before submiting the form.
 * 
 * Layouts
 * The widget can be skinned and to use custom layouts (which require HTML, CSS and JS coding!). Please see the 'default' and 'example' layouts.
 * Remember that MANY widgets can exists in the same UI/Page, so using $id in layouts is absolutely required!!! Not using it can lead to unexpected results.
 * You have been warned!

 * 
 * Usage (you can use more than one widgets in a single form):
 * 
 * 	<form>
 * 
 * 	$this->widget('common.extensions.plupload.widgets.SingleFile', array(
 *		'maxFileSize' => 1000,
 *		'fieldName' => 'fieldname1',
 *		'onUploadComplete' => 'onUploadComplete1',
 *		'onError'	=> 'onError1',	
 *		'layout' => 'mylayout',	
 *	));
 *
 *  </form>
 * 
 * 
 * Notes:
 * 1. Always use CHtml::beginForm()  to ensure sending CSRF token. Yii will throw exceptions otherwise during upload.
 *  
 * 2. onXXXXXXXXXXX callbacks are custom defined JavaScript functions having specific signature. 
 *    Please check singlefile.js code for all available callbacks and their parameters.
 *    
 * 3. You need some knowledge about pluploader plugin  (http://www.plupload.com/)
 * 
 * 4. By default AJAX uploading process is handled by /lms/?r=site/AxUploadFile and all files can be found
 *    in /files/<domain-storage>/upload_tmp (which is the default behavior of the mentioned action).
 *    You can redefine the action by setting  'uploadUrl' option of the widget. 
 *   
 * 5. You do not have to create <input> fields in advance. Just give the name, it will be created on the fly automatically.
 *    However, you CAN create it and it will be used, searched by the given name. 
 *    (<input> field is hidden and consists the name of uploaded file).
 *    
 * 6. Although you can put MORE than one widget in a single form, thus uploading MANY files in a single form, this is
 *    still SINGLE file widget!  Another widget will be made for multi-file uploads (i.e. where you can select many files
 *    when you click "Select files").
 *    
 * 
 * 
 * @author Plamen
 *
 */
class SingleFile extends CWidget {
	
	/**
	 * Widget ID. Generated automaticaly if not provided by caller.
	 * @var string
	 */
	public $id = null;	
	
	/**
	 * Layout name
	 * @var string
	 */
	public $layout = 'default';
	
	/**
	 * Should Yii copy assets folder on every request?
	 * @var unknown
	 */
	public $forceCopyAssets = true;

	/**
	 * URL of the controller/action which will serve the AJAX uploading process
	 * Example: /lms/index.php?r=site/axUploadFile
	 *  
	 * @var string
	 */
	public $uploadUrl = '';
	
	
	/**
	 * Max File Size in Megabytes
	 * @var integer
	 */
	public $maxFileSize = 4;  
	
	
	/**
	 * Comma separated list of allowed extensions
	 * @var string
	 */
	public $allowedExtensions = '';
	
	
	/**
	 * ID of the surrounding FORM element. If not specified (empty string), closest parent one will be used, if any.
	 * If set to false, no form fields will be generated or updated.
	 * @var string|boolean
	 */
	public $formId = '';
	
	/**
	 * Form field name associated to this widget
	 * REQUIRED!
	 * @var string
	 */
	public $fieldName = '';
	
	
	/**
	 * Form field ID associated to this widget
	 * REQUIRED!
	 * @var string
	 */
	public $fieldId = '';
	
	
	/**
	 * URL to Flash. must be web accessible; Usually part of the "assets" 
	 * @var string
	 */
	public $flashUrl = '';
	
	
	/**
	 * CALLBACK functions.
	 * In all signatures, the first parameter of the function signature (id) is the widget ID
	 */
	
	
	
	/**
	 * JS Callback called upon pluploader init
	 * Signature:  function(id, params, Object uploader)
	 *  
	 * @var string
	 */
	public $onInit = '';
	
	/**
	 * JavaScript function name called upon file upload is completed.
	 * Signature:  function(id, Object file, Object uploader)
	 * 
	 * @var string
	 */
	public $onUploadComplete = '';
	
	/**
	 * JS Callback called upon selecting a file
	 * Signature:  function(id, Object file, Object uploader)
	 * 
	 * @var string
	 */
	public $onFilesAdded = '';
	
	/**
	 * JS Callback called just before uploads starts
	 * Signature:  function(id, Object file, Object uploader)
	 *
	 * @var string
	 */
	public $onBeforeUpload = '';
	
	/**
	 * JS Callback called periodically during file upload progress
	 * Signature:  function(id, Object file, Object uploader)
	 * 
	 * @var string
	 */
	public $onUploadProgress = '';
	
	/**
	 * JS Callback called if error occurs
	 * Signature:  function(id, Object error, Object uploader)
	 * 
	 * @var string
	 */
	public $onError = '';
	
	
	
	public $buttonText = false;
	
	
	/**
	 * Holds widget's assets URL
	 * @var string
	 */
	protected $_assetsUrl;
	
	
	private $_uploadFieldOptionsJs;
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init() {
		
		$this->validateParameters();
		

		// If widget ID is not provided by caller, set it to a random UUID
		if (!$this->id) {
			$this->id = $this->generateUuid();
		}
		
		// Just in case, remove invalid characters from widget ID
		$this->id = preg_replace('/[^a-z0-9_]/i', '_', $this->id);
		
		// If FLASH URL is not provided, set to plupload's one (this widget is part of 'plupload' extension!
		if (empty($this->flashUrl)) {
			$this->flashUrl = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";
		}
		
		// URL to controller/action serving AJAX upload
		if (empty($this->uploadUrl)) {
			$this->uploadUrl = Docebo::createLmsUrl('site/AxUploadFile');
		}
		
		// Register resources
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($this->getAssetsUrl() . '/layouts/' . $this->layout  .'.css');
		$cs->registerScriptFile($this->getAssetsUrl() . '/singlefile.js' , CClientScript::POS_HEAD);
		
		$options = array(
			'csrfToken' 				=> Yii::app()->request->csrfToken,
			'id' => $this->id,
			'flash_swf_url' 			=> $this->flashUrl,
			'url'						=> $this->uploadUrl,
			'browse_button' 			=> 'pickfiles-' .  $this->id,
			'container'     			=> 'pluploader-container-' .  $this->id,
			'drop_element'  			=> 'pluploader-dropzone-' .  $this->id,
			'max_file_size'				=> $this->maxFileSize . "m",
			'allowedExtensions'			=> $this->allowedExtensions,
			'formId'					=> $this->formId,
			'fieldName'					=> $this->fieldName,	
			'fieldId'					=> $this->fieldId,
			'onInit'					=> $this->onInit,	
			'onUploadComplete'			=> $this->onUploadComplete,
			'onFilesAdded'				=> $this->onFilesAdded,
			'onBeforeUpload'			=> $this->onBeforeUpload,
			'onUploadProgress'			=> $this->onUploadProgress,
			'onError'					=> $this->onError,
		);

		$optionsJs = CJavaScript::encode($options);
		$this->_uploadFieldOptionsJs = $optionsJs;

		$script =
			<<<EOD
				var options = $optionsJs;
				plUploader = new pluploaderSingleFile(options);
				plUploader.run();
				var publicPlUploader = plUploader;
EOD;

		$cs->registerScript($this->id, $script, CClientScript::POS_READY);

	}
	

	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run() {
		
		if (!$this->buttonText) {
			$this->buttonText = Yii::t('course_management', 'CHOOSE FILE');
		}
		
		$html = '<div id="pluploader-wrapper-' . $this->id . '">' . "\n";
		$html .= '<div class="pluploader-wrapper-inner">';
		$html .= $this->render(strtolower(__CLASS__) . '/layouts/' . $this->layout, array('options' => $this->_uploadFieldOptionsJs), true);
		$html .= "</div></div>\n";
		echo $html;
	}
	

	
	
	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl()
	{
		if (isset($this->_assetsUrl))
			return $this->_assetsUrl;
		else
		{
			$assetsPath = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . strtolower(__CLASS__);
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath, true);
			return $this->_assetsUrl = $assetsUrl;
		}
	}
	
	
	
	public static function getWidgetAssetsUrl() {
		$assetsPath = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . strtolower(__CLASS__);
		$assetsUrl = Yii::app()->assetManager->publish($assetsPath, true);
		return $assetsUrl;
	}
	
	
	/**
	 * Generate random UUID in form of: 8-4-4-4-12
	 */
	private function generateUuid() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 8
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
	
				// 4
				mt_rand( 0, 0xffff ),
	
				// 4
				mt_rand( 0, 0x0fff ) | 0x4000,
	
				// 4
				mt_rand( 0, 0x3fff ) | 0x8000,
	
				// 12
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
	

	
	private function validateParameters() {
		
		if (empty($this->fieldName)) {
			throw new CException('SingleFile widget requires a field name');
		}
		
		$matches = preg_match('/[^a-z0-9_]/i', $this->id);
		if (!empty($matches)) {
			throw new CException('SingleFile widget ID must contain only alphanumeric and/or underscore charactes');
		}
		
		
	}
	
	
	
	

}