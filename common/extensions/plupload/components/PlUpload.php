<?php
/**
 * A component used to load PlUpload
 * http://www.plupload.com/
 *
 * @TODO Make also some widgets, for God sake!
 *
 */
class PlUpload extends CApplicationComponent  {

	const QUEUED = 1;
	const UPLOADING = 2;

	const STOPPED = 1;
	const STARTED = 2;
	const FAILED = 4;
	const DONE = 5;



	protected $_assetsUrl;

	/**
	 * (non-PHPdoc)
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		// Register the path alias.
		if (Yii::getPathOfAlias('plupload') === false)
			Yii::setPathOfAlias('plupload', realpath(dirname(__FILE__) . '/..'));

		// Prevents the extension from registering scripts and publishing assets when ran from the command line.
		if (Yii::app() instanceof CConsoleApplication)
			return;

		// Prevent loading for AJAX calls
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}

		$cs = Yii::app()->getClientScript();

		// Still have no idea about this "browserplus" (?)
		//$cs->registerScriptFile('http://bp.yahooapis.com/2.4.21/browserplus-min.js', CClientScript::POS_HEAD);

		// Main JS file
		$cs->registerScriptFile($this->getAssetsUrl() . '/plupload.full.js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($this->getAssetsUrl() . '/jquery.plupload.queue/jquery.plupload.queue.js', CClientScript::POS_HEAD);

		parent::init();

	}


	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl()
	{
		if (isset($this->_assetsUrl))
			return $this->_assetsUrl;
		else
		{
			$assetsPath = Yii::getPathOfAlias('common.extensions.plupload.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			return $this->_assetsUrl = $assetsUrl;
		}
	}


}
