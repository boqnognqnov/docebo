<?php
/**
 * An extension to load jQuery Dialog2 stuff as assets from protected area.
 *
 * See:
 * https://github.com/Nikku/jquery-bootstrap-scripting
 *
 * @author Plamen Petkov
 *
 */
class Dialog2 extends CApplicationComponent {

    public $useMinified = true;

    protected $_assetsUrl;

    /**
     *
     * @return string
     */
    public function getAssetsUrl()
    {
        if (!isset($this->_assetsUrl)){
            $assetsPath = Yii::getPathOfAlias('common.extensions.dialog2.assets');
	        if(is_dir($assetsPath))
	            $this->_assetsUrl = Yii::app()->assetManager->publish($assetsPath);
	        else
		        Yii::log('Can not publish dialog2 assets', CLogger::LEVEL_ERROR);
        }

	    return $this->_assetsUrl;
    }


    /**
     * (non-PHPdoc)
     * @see CApplicationComponent::init()
     */
    public function init() {

    	// Prevent loading for AJAX calls
    	if (Yii::app()->request->isAjaxRequest) {
    		return;
    	}
    	 
    	
        // Register alias if not registered
        if (Yii::getPathOfAlias('dialog2') === false) {
            Yii::setPathOfAlias('dialog2', realpath(dirname(__FILE__)));
        }

        // Register all JS/CSS files
        $this->registerAll();

    }


    /**
     * Register Javascript files from assets pool.
     *
     * @param int $position Where to put the JS code
     */
    public function registerAll($position = CClientScript::POS_HEAD) {
        $cs = Yii::app()->getClientScript();

        if ($this->useMinified) {
            // css is included into base.css
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/dialog2-all.min.js', $position);
        } else {
        	$cs->registerScriptFile($this->getAssetsUrl() . '/js/dialog2-all.full.js', $position);
        	/*
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/jquery.controls.js', $position);
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/jquery.form.js', $position);
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/jquery.dialog2.js', $position);
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/jquery.dialog2.helpers.js', $position);
            $cs->registerScriptFile($this->getAssetsUrl() . '/js/bootbox.js', $position);
            */
        }

        $cs->registerScript('dialog2_controls', "$(document).controls();", CClientScript::POS_READY);
    }


}

