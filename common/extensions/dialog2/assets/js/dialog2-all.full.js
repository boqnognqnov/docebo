/**
 * This file contains all Dialog2 related JS from Yii Dialog2 extension, namely:
 * 
 * Controls
 * Forms
 * Dialog2,
 * Dialog2 helpers
 * Bootbox
 * 
 * 
 */


/*
 * Ajax controls plugin for jquery.
 * 
 * Licensed under the MIT license 
 * http://www.opensource.org/licenses/mit-license.php 
 * 
 * Has to be invoked once on page load like
 * 
 *    $(function() {
 *        $(document).controls();
 *    });
 * 
 * to apply all handlers registered to $.fn.controls.controls.
 * 
 * @version: 0.9 (29/10/2010)
 * @requires jQuery v1.4 or later
 * 
 * @author nico.rehwaldt
 */
(function($) {
    $.extend($.fn, {
        controls: function(options) {
            var element = this;
            
            $.each($.fn.controls.bindings, function(selector, action) {
                element
                    .find(selector)
                    .each(action)
                    .end();
            });

            return this;
        }
    });

    /**
     * Space for other jquery plugins to register javascript controls
     */
    $.fn.controls.bindings = {
        /**
         * Register specific handler for ajax annotations here, e.g.
         * 
         * "a.ajax": function() {
         *     // load ajax page or alert("FOO, ajax link clicked");
         * }
         */
    };
})(jQuery);




/*!
 * jQuery Form Plugin
 * version: 3.20 (20-NOV-2012)
 * @requires jQuery v1.5 or later
 *
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses:
 *    http://malsup.github.com/mit-license.txt
 *    http://malsup.github.com/gpl-license-v2.txt
 */
/*global ActiveXObject alert */
;(function($) {
"use strict";

/*
    Usage Note:
    -----------
    Do not use both ajaxSubmit and ajaxForm on the same form.  These
    functions are mutually exclusive.  Use ajaxSubmit if you want
    to bind your own submit handler to the form.  For example,

    $(document).ready(function() {
        $('#myForm').on('submit', function(e) {
            e.preventDefault(); // <-- important
            $(this).ajaxSubmit({
                target: '#output'
            });
        });
    });

    Use ajaxForm when you want the plugin to manage all the event binding
    for you.  For example,

    $(document).ready(function() {
        $('#myForm').ajaxForm({
            target: '#output'
        });
    });
    
    You can also use ajaxForm with delegation (requires jQuery v1.7+), so the
    form does not have to exist when you invoke ajaxForm:

    $('#myForm').ajaxForm({
        delegation: true,
        target: '#output'
    });
    
    When using ajaxForm, the ajaxSubmit function will be invoked for you
    at the appropriate time.
*/

/**
 * Feature detection
 */
var feature = {};
feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
feature.formdata = window.FormData !== undefined;

/**
 * ajaxSubmit() provides a mechanism for immediately submitting
 * an HTML form using AJAX.
 */
$.fn.ajaxSubmit = function(options) {
    /*jshint scripturl:true */

    // fast fail if nothing selected (http://dev.jquery.com/ticket/2752)
    if (!this.length) {
        log('ajaxSubmit: skipping submit process - no element selected');
        return this;
    }
    
    var method, action, url, $form = this;

    if (typeof options == 'function') {
        options = { success: options };
    }

    method = this.attr('method');
    action = this.attr('action');
    url = (typeof action === 'string') ? $.trim(action) : '';
    url = url || window.location.href || '';
    if (url) {
        // clean url (don't include hash vaue)
        url = (url.match(/^([^#]+)/)||[])[1];
    }

    options = $.extend(true, {
        url:  url,
        success: $.ajaxSettings.success,
        type: method || 'GET',
        iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
    }, options);

    // hook for manipulating the form data before it is extracted;
    // convenient for use with rich editors like tinyMCE or FCKEditor
    var veto = {};
    this.trigger('form-pre-serialize', [this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
        return this;
    }

    // provide opportunity to alter form data before it is serialized
    if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSerialize callback');
        return this;
    }

    var traditional = options.traditional;
    if ( traditional === undefined ) {
        traditional = $.ajaxSettings.traditional;
    }
    
    var elements = [];
    var qx, a = this.formToArray(options.semantic, elements);
    if (options.data) {
        options.extraData = options.data;
        qx = $.param(options.data, traditional);
    }

    // give pre-submit callback an opportunity to abort the submit
    if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
        log('ajaxSubmit: submit aborted via beforeSubmit callback');
        return this;
    }

    // fire vetoable 'validate' event
    this.trigger('form-submit-validate', [a, this, options, veto]);
    if (veto.veto) {
        log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
        return this;
    }

    var q = $.param(a, traditional);
    if (qx) {
        q = ( q ? (q + '&' + qx) : qx );
    }    
    if (options.type.toUpperCase() == 'GET') {
        options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
        options.data = null;  // data is null for 'get'
    }
    else {
        options.data = q; // data is the query string for 'post'
    }

    var callbacks = [];
    if (options.resetForm) {
        callbacks.push(function() { $form.resetForm(); });
    }
    if (options.clearForm) {
        callbacks.push(function() { $form.clearForm(options.includeHidden); });
    }

    // perform a load on the target only if dataType is not provided
    if (!options.dataType && options.target) {
        var oldSuccess = options.success || function(){};
        callbacks.push(function(data) {
            var fn = options.replaceTarget ? 'replaceWith' : 'html';
            $(options.target)[fn](data).each(oldSuccess, arguments);
        });
    }
    else if (options.success) {
        callbacks.push(options.success);
    }

    options.success = function(data, status, xhr) { // jQuery 1.4+ passes xhr as 3rd arg
        var context = options.context || this ;    // jQuery 1.4+ supports scope context 
        for (var i=0, max=callbacks.length; i < max; i++) {
            callbacks[i].apply(context, [data, status, xhr || $form, $form]);
        }
    };

    // are there files to upload?

    // [value] (issue #113), also see comment:
    // https://github.com/malsup/form/commit/588306aedba1de01388032d5f42a60159eea9228#commitcomment-2180219
    var fileInputs = $('input[type=file]:enabled[value!=""]', this); 

    var hasFileInputs = fileInputs.length > 0;
    var mp = 'multipart/form-data';
    var multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);

    var fileAPI = feature.fileapi && feature.formdata;
    log("fileAPI :" + fileAPI);
    //var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;
    var shouldUseFrame = hasFileInputs && multipart && !fileAPI; // Fixes ajax submit with enctype=multipart/form-data

    var jqxhr;

    // options.iframe allows user to force iframe mode
    // 06-NOV-09: now defaulting to iframe mode if file input is detected
    if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
        // hack to fix Safari hang (thanks to Tim Molendijk for this)
        // see:  http://groups.google.com/group/jquery-dev/browse_thread/thread/36395b7ab510dd5d
        if (options.closeKeepAlive) {
            $.get(options.closeKeepAlive, function() {
                jqxhr = fileUploadIframe(a);
            });
        }
        else {
            jqxhr = fileUploadIframe(a);
        }
    }
    else if ((hasFileInputs || multipart) && fileAPI) {
        jqxhr = fileUploadXhr(a);
    }
    else {
        jqxhr = $.ajax(options);
    }

    $form.removeData('jqxhr').data('jqxhr', jqxhr);

    // clear element array
    for (var k=0; k < elements.length; k++)
        elements[k] = null;

    // fire 'notify' event
    this.trigger('form-submit-notify', [this, options]);
    return this;

    // utility fn for deep serialization
    function deepSerialize(extraData){
        var serialized = $.param(extraData).split('&');
        var len = serialized.length;
        var result = {};
        var i, part;
        for (i=0; i < len; i++) {
            part = serialized[i].split('=');
            result[decodeURIComponent(part[0])] = decodeURIComponent(part[1]);
        }
        return result;
    }

     // XMLHttpRequest Level 2 file uploads (big hat tip to francois2metz)
    function fileUploadXhr(a) {
        var formdata = new FormData();

        for (var i=0; i < a.length; i++) {
            formdata.append(a[i].name, a[i].value);
        }

        if (options.extraData) {
            var serializedData = deepSerialize(options.extraData);
            for (var p in serializedData)
                if (serializedData.hasOwnProperty(p))
                    formdata.append(p, serializedData[p]);
        }

        options.data = null;

        var s = $.extend(true, {}, $.ajaxSettings, options, {
            contentType: false,
            processData: false,
            cache: false,
            type: method || 'POST'
        });
        
        if (options.uploadProgress) {
            // workaround because jqXHR does not expose upload property
            s.xhr = function() {
                var xhr = jQuery.ajaxSettings.xhr();
                if (xhr.upload) {
                    xhr.upload.onprogress = function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position; /*event.position is deprecated*/
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        options.uploadProgress(event, position, total, percent);
                    };
                }
                return xhr;
            };
        }

        s.data = null;
            var beforeSend = s.beforeSend;
            s.beforeSend = function(xhr, o) {
                o.data = formdata;
                if(beforeSend)
                    beforeSend.call(this, xhr, o);
        };
        return $.ajax(s);
    }

    // private function for handling file uploads (hat tip to YAHOO!)
    function fileUploadIframe(a) {
        var form = $form[0], el, i, s, g, id, $io, io, xhr, sub, n, timedOut, timeoutHandle;
        var useProp = !!$.fn.prop;
        var deferred = $.Deferred();

        if ($('[name=submit],[id=submit]', form).length) {
            // if there is an input with a name or id of 'submit' then we won't be
            // able to invoke the submit fn on the form (at least not x-browser)
            alert('Error: Form elements must not have name or id of "submit".');
            deferred.reject();
            return deferred;
        }
        
        if (a) {
            // ensure that every serialized input is still enabled
            for (i=0; i < elements.length; i++) {
                el = $(elements[i]);
                if ( useProp )
                    el.prop('disabled', false);
                else
                    el.removeAttr('disabled');
            }
        }

        s = $.extend(true, {}, $.ajaxSettings, options);
        s.context = s.context || s;
        id = 'jqFormIO' + (new Date().getTime());
        if (s.iframeTarget) {
            $io = $(s.iframeTarget);
            n = $io.attr('name');
            if (!n)
                 $io.attr('name', id);
            else
                id = n;
        }
        else {
            $io = $('<iframe name="' + id + '" src="'+ s.iframeSrc +'" />');
            $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });
        }
        io = $io[0];


        xhr = { // mock object
            aborted: 0,
            responseText: null,
            responseXML: null,
            status: 0,
            statusText: 'n/a',
            getAllResponseHeaders: function() {},
            getResponseHeader: function() {},
            setRequestHeader: function() {},
            abort: function(status) {
                var e = (status === 'timeout' ? 'timeout' : 'aborted');
                log('aborting upload... ' + e);
                this.aborted = 1;
                // #214
                if (io.contentWindow.document.execCommand) {
                    try { // #214
                        io.contentWindow.document.execCommand('Stop');
                    } catch(ignore) {}
                }
                $io.attr('src', s.iframeSrc); // abort op in progress
                xhr.error = e;
                if (s.error)
                    s.error.call(s.context, xhr, e, status);
                if (g)
                    $.event.trigger("ajaxError", [xhr, s, e]);
                if (s.complete)
                    s.complete.call(s.context, xhr, e);
            }
        };

        g = s.global;
        // trigger ajax global events so that activity/block indicators work like normal
        if (g && 0 === $.active++) {
            $.event.trigger("ajaxStart");
        }
        if (g) {
            $.event.trigger("ajaxSend", [xhr, s]);
        }

        if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
            if (s.global) {
                $.active--;
            }
            deferred.reject();
            return deferred;
        }
        if (xhr.aborted) {
            deferred.reject();
            return deferred;
        }

        // add submitting element to data if we know it
        sub = form.clk;
        if (sub) {
            n = sub.name;
            if (n && !sub.disabled) {
                s.extraData = s.extraData || {};
                s.extraData[n] = sub.value;
                if (sub.type == "image") {
                    s.extraData[n+'.x'] = form.clk_x;
                    s.extraData[n+'.y'] = form.clk_y;
                }
            }
        }
        
        var CLIENT_TIMEOUT_ABORT = 1;
        var SERVER_ABORT = 2;

        function getDoc(frame) {
            var doc = frame.contentWindow ? frame.contentWindow.document : frame.contentDocument ? frame.contentDocument : frame.document;
            return doc;
        }
        
        // Rails CSRF hack (thanks to Yvan Barthelemy)
        var csrf_token = $('meta[name=csrf-token]').attr('content');
        var csrf_param = $('meta[name=csrf-param]').attr('content');
        if (csrf_param && csrf_token) {
            s.extraData = s.extraData || {};
            s.extraData[csrf_param] = csrf_token;
        }

        // take a breath so that pending repaints get some cpu time before the upload starts
        function doSubmit() {
            // make sure form attrs are set
            var t = $form.attr('target'), a = $form.attr('action');

            // update form attrs in IE friendly way
            form.setAttribute('target',id);
            if (!method) {
                form.setAttribute('method', 'POST');
            }
            if (a != s.url) {
                form.setAttribute('action', s.url);
            }

            // ie borks in some cases when setting encoding
            if (! s.skipEncodingOverride && (!method || /post/i.test(method))) {
                $form.attr({
                    encoding: 'multipart/form-data',
                    enctype:  'multipart/form-data'
                });
            }

            // support timout
            if (s.timeout) {
                timeoutHandle = setTimeout(function() { timedOut = true; cb(CLIENT_TIMEOUT_ABORT); }, s.timeout);
            }
            
            // look for server aborts
            function checkState() {
                try {
                    var state = getDoc(io).readyState;
                    log('state = ' + state);
                    if (state && state.toLowerCase() == 'uninitialized')
                        setTimeout(checkState,50);
                }
                catch(e) {
                    log('Server abort: ' , e, ' (', e.name, ')');
                    cb(SERVER_ABORT);
                    if (timeoutHandle)
                        clearTimeout(timeoutHandle);
                    timeoutHandle = undefined;
                }
            }

            // add "extra" data to form if provided in options
            var extraInputs = [];
            try {
                if (s.extraData) {
                    for (var n in s.extraData) {
                        if (s.extraData.hasOwnProperty(n)) {
                           // if using the $.param format that allows for multiple values with the same name
                           if($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty('name') && s.extraData[n].hasOwnProperty('value')) {
                               extraInputs.push(
                               $('<input type="hidden" name="'+s.extraData[n].name+'">').attr('value',s.extraData[n].value)
                                   .appendTo(form)[0]);
                           } else {
                               extraInputs.push(
                               $('<input type="hidden" name="'+n+'">').attr('value',s.extraData[n])
                                   .appendTo(form)[0]);
                           }
                        }
                    }
                }

                if (!s.iframeTarget) {
                    // add iframe to doc and submit the form
                    $io.appendTo('body');
                    if (io.attachEvent)
                        io.attachEvent('onload', cb);
                    else
                        io.addEventListener('load', cb, false);
                }
                setTimeout(checkState,15);
                form.submit();
            }
            finally {
                // reset attrs and remove "extra" input elements
                form.setAttribute('action',a);
                if(t) {
                    form.setAttribute('target', t);
                } else {
                    $form.removeAttr('target');
                }
                $(extraInputs).remove();
            }
        }

        if (s.forceSync) {
            doSubmit();
        }
        else {
            setTimeout(doSubmit, 10); // this lets dom updates render
        }

        var data, doc, domCheckCount = 50, callbackProcessed;

        function cb(e) {
            if (xhr.aborted || callbackProcessed) {
                return;
            }
            try {
                doc = getDoc(io);
            }
            catch(ex) {
                log('cannot access response document: ', ex);
                e = SERVER_ABORT;
            }
            if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                xhr.abort('timeout');
                deferred.reject(xhr, 'timeout');
                return;
            }
            else if (e == SERVER_ABORT && xhr) {
                xhr.abort('server abort');
                deferred.reject(xhr, 'error', 'server abort');
                return;
            }

            if (!doc || doc.location.href == s.iframeSrc) {
                // response not received yet
                if (!timedOut)
                    return;
            }
            if (io.detachEvent)
                io.detachEvent('onload', cb);
            else    
                io.removeEventListener('load', cb, false);

            var status = 'success', errMsg;
            try {
                if (timedOut) {
                    throw 'timeout';
                }

                var isXml = s.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                log('isXml='+isXml);
                if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                    if (--domCheckCount) {
                        // in some browsers (Opera) the iframe DOM is not always traversable when
                        // the onload callback fires, so we loop a bit to accommodate
                        log('requeing onLoad callback, DOM not available');
                        setTimeout(cb, 250);
                        return;
                    }
                    // let this fall through because server response could be an empty document
                    //log('Could not access iframe DOM after mutiple tries.');
                    //throw 'DOMException: not available';
                }

                //log('response detected');
                var docRoot = doc.body ? doc.body : doc.documentElement;
                xhr.responseText = docRoot ? docRoot.innerHTML : null;
                xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                if (isXml)
                    s.dataType = 'xml';
                xhr.getResponseHeader = function(header){
                    var headers = {'content-type': s.dataType};
                    return headers[header];
                };
                // support for XHR 'status' & 'statusText' emulation :
                if (docRoot) {
                    xhr.status = Number( docRoot.getAttribute('status') ) || xhr.status;
                    xhr.statusText = docRoot.getAttribute('statusText') || xhr.statusText;
                }

                var dt = (s.dataType || '').toLowerCase();
                var scr = /(json|script|text)/.test(dt);
                if (scr || s.textarea) {
                    // see if user embedded response in textarea
                    var ta = doc.getElementsByTagName('textarea')[0];
                    if (ta) {
                        xhr.responseText = ta.value;
                        // support for XHR 'status' & 'statusText' emulation :
                        xhr.status = Number( ta.getAttribute('status') ) || xhr.status;
                        xhr.statusText = ta.getAttribute('statusText') || xhr.statusText;
                    }
                    else if (scr) {
                        // account for browsers injecting pre around json response
                        var pre = doc.getElementsByTagName('pre')[0];
                        var b = doc.getElementsByTagName('body')[0];
                        if (pre) {
                            xhr.responseText = pre.textContent ? pre.textContent : pre.innerText;
                        }
                        else if (b) {
                            xhr.responseText = b.textContent ? b.textContent : b.innerText;
                        }
                    }
                }
                else if (dt == 'xml' && !xhr.responseXML && xhr.responseText) {
                    xhr.responseXML = toXml(xhr.responseText);
                }

                try {
                    data = httpData(xhr, dt, s);
                }
                catch (e) {
                    status = 'parsererror';
                    xhr.error = errMsg = (e || status);
                }
            }
            catch (e) {
                log('error caught: ',e);
                status = 'error';
                xhr.error = errMsg = (e || status);
            }

            if (xhr.aborted) {
                log('upload aborted');
                status = null;
            }

            if (xhr.status) { // we've set xhr.status
                status = (xhr.status >= 200 && xhr.status < 300 || xhr.status === 304) ? 'success' : 'error';
            }

            // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
            if (status === 'success') {
                if (s.success)
                    s.success.call(s.context, data, 'success', xhr);
                deferred.resolve(xhr.responseText, 'success', xhr);
                if (g)
                    $.event.trigger("ajaxSuccess", [xhr, s]);
            }
            else if (status) {
                if (errMsg === undefined)
                    errMsg = xhr.statusText;
                if (s.error)
                    s.error.call(s.context, xhr, status, errMsg);
                deferred.reject(xhr, 'error', errMsg);
                if (g)
                    $.event.trigger("ajaxError", [xhr, s, errMsg]);
            }

            if (g)
                $.event.trigger("ajaxComplete", [xhr, s]);

            if (g && ! --$.active) {
                $.event.trigger("ajaxStop");
            }

            if (s.complete)
                s.complete.call(s.context, xhr, status);

            callbackProcessed = true;
            if (s.timeout)
                clearTimeout(timeoutHandle);

            // clean up
            setTimeout(function() {
                if (!s.iframeTarget)
                    $io.remove();
                xhr.responseXML = null;
            }, 100);
        }

        var toXml = $.parseXML || function(s, doc) { // use parseXML if available (jQuery 1.5+)
            if (window.ActiveXObject) {
                doc = new ActiveXObject('Microsoft.XMLDOM');
                doc.async = 'false';
                doc.loadXML(s);
            }
            else {
                doc = (new DOMParser()).parseFromString(s, 'text/xml');
            }
            return (doc && doc.documentElement && doc.documentElement.nodeName != 'parsererror') ? doc : null;
        };
        var parseJSON = $.parseJSON || function(s) {
            /*jslint evil:true */
            return window['eval']('(' + s + ')');
        };

        var httpData = function( xhr, type, s ) { // mostly lifted from jq1.4.4

            var ct = xhr.getResponseHeader('content-type') || '',
                xml = type === 'xml' || !type && ct.indexOf('xml') >= 0,
                data = xml ? xhr.responseXML : xhr.responseText;

            if (xml && data.documentElement.nodeName === 'parsererror') {
                if ($.error)
                    $.error('parsererror');
            }
            if (s && s.dataFilter) {
                data = s.dataFilter(data, type);
            }
            if (typeof data === 'string') {
                if (type === 'json' || !type && ct.indexOf('json') >= 0) {
                    data = parseJSON(data);
                } else if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                    $.globalEval(data);
                }
            }
            return data;
        };

        return deferred;
    }
};

/**
 * ajaxForm() provides a mechanism for fully automating form submission.
 *
 * The advantages of using this method instead of ajaxSubmit() are:
 *
 * 1: This method will include coordinates for <input type="image" /> elements (if the element
 *    is used to submit the form).
 * 2. This method will include the submit element's name/value data (for the element that was
 *    used to submit the form).
 * 3. This method binds the submit() method to the form for you.
 *
 * The options argument for ajaxForm works exactly as it does for ajaxSubmit.  ajaxForm merely
 * passes the options argument along after properly binding events for submit elements and
 * the form itself.
 */
$.fn.ajaxForm = function(options) {
    options = options || {};
    options.delegation = options.delegation && $.isFunction($.fn.on);
    
    // in jQuery 1.3+ we can fix mistakes with the ready state
    if (!options.delegation && this.length === 0) {
        var o = { s: this.selector, c: this.context };
        if (!$.isReady && o.s) {
            log('DOM not ready, queuing ajaxForm');
            $(function() {
                $(o.s,o.c).ajaxForm(options);
            });
            return this;
        }
        // is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
        log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
        return this;
    }

    if ( options.delegation ) {
        $(document)
            .off('submit.form-plugin', this.selector, doAjaxSubmit)
            .off('click.form-plugin', this.selector, captureSubmittingElement)
            .on('submit.form-plugin', this.selector, options, doAjaxSubmit)
            .on('click.form-plugin', this.selector, options, captureSubmittingElement);
        return this;
    }

    return this.ajaxFormUnbind()
        .bind('submit.form-plugin', options, doAjaxSubmit)
        .bind('click.form-plugin', options, captureSubmittingElement);
};

// private event handlers    
function doAjaxSubmit(e) {
    /*jshint validthis:true */
    var options = e.data;
    if (!e.isDefaultPrevented()) { // if event has been canceled, don't proceed
        e.preventDefault();
        $(this).ajaxSubmit(options);
    }
}
    
function captureSubmittingElement(e) {
    /*jshint validthis:true */
    var target = e.target;
    var $el = $(target);
    if (!($el.is("[type=submit],[type=image]"))) {
        // is this a child element of the submit el?  (ex: a span within a button)
        var t = $el.closest('[type=submit]');
        if (t.length === 0) {
            return;
        }
        target = t[0];
    }
    var form = this;
    form.clk = target;
    if (target.type == 'image') {
        if (e.offsetX !== undefined) {
            form.clk_x = e.offsetX;
            form.clk_y = e.offsetY;
        } else if (typeof $.fn.offset == 'function') {
            var offset = $el.offset();
            form.clk_x = e.pageX - offset.left;
            form.clk_y = e.pageY - offset.top;
        } else {
            form.clk_x = e.pageX - target.offsetLeft;
            form.clk_y = e.pageY - target.offsetTop;
        }
    }
    // clear form vars
    setTimeout(function() { form.clk = form.clk_x = form.clk_y = null; }, 100);
}


// ajaxFormUnbind unbinds the event handlers that were bound by ajaxForm
$.fn.ajaxFormUnbind = function() {
    return this.unbind('submit.form-plugin click.form-plugin');
};

/**
 * formToArray() gathers form element data into an array of objects that can
 * be passed to any of the following ajax functions: $.get, $.post, or load.
 * Each object in the array has both a 'name' and 'value' property.  An example of
 * an array for a simple login form might be:
 *
 * [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
 *
 * It is this array that is passed to pre-submit callback functions provided to the
 * ajaxSubmit() and ajaxForm() methods.
 */
$.fn.formToArray = function(semantic, elements) {
    var a = [];
    if (this.length === 0) {
        return a;
    }

    var form = this[0];
    var els = semantic ? form.getElementsByTagName('*') : form.elements;
    if (!els) {
        return a;
    }

    var i,j,n,v,el,max,jmax;
    for(i=0, max=els.length; i < max; i++) {
        el = els[i];
        n = el.name;
        if (!n) {
            continue;
        }

        if (semantic && form.clk && el.type == "image") {
            // handle image inputs on the fly when semantic == true
            if(!el.disabled && form.clk == el) {
                a.push({name: n, value: $(el).val(), type: el.type });
                a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
            }
            continue;
        }

        v = $.fieldValue(el, true);
        if (v && v.constructor == Array) {
            if (elements) 
                elements.push(el);
            for(j=0, jmax=v.length; j < jmax; j++) {
                a.push({name: n, value: v[j]});
            }
        }
        else if (feature.fileapi && el.type == 'file' && !el.disabled) {
            if (elements) 
                elements.push(el);
            var files = el.files;
            if (files.length) {
                for (j=0; j < files.length; j++) {
                    a.push({name: n, value: files[j], type: el.type});
                }
            }
            else {
                // #180
                a.push({ name: n, value: '', type: el.type });
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            if (elements) 
                elements.push(el);
            a.push({name: n, value: v, type: el.type, required: el.required});
        }
    }

    if (!semantic && form.clk) {
        // input type=='image' are not found in elements array! handle it here
        var $input = $(form.clk), input = $input[0];
        n = input.name;
        if (n && !input.disabled && input.type == 'image') {
            a.push({name: n, value: $input.val()});
            a.push({name: n+'.x', value: form.clk_x}, {name: n+'.y', value: form.clk_y});
        }
    }
    return a;
};

/**
 * Serializes form data into a 'submittable' string. This method will return a string
 * in the format: name1=value1&amp;name2=value2
 */
$.fn.formSerialize = function(semantic) {
    //hand off to jQuery.param for proper encoding
    return $.param(this.formToArray(semantic));
};

/**
 * Serializes all field elements in the jQuery object into a query string.
 * This method will return a string in the format: name1=value1&amp;name2=value2
 */
$.fn.fieldSerialize = function(successful) {
    var a = [];
    this.each(function() {
        var n = this.name;
        if (!n) {
            return;
        }
        var v = $.fieldValue(this, successful);
        if (v && v.constructor == Array) {
            for (var i=0,max=v.length; i < max; i++) {
                a.push({name: n, value: v[i]});
            }
        }
        else if (v !== null && typeof v != 'undefined') {
            a.push({name: this.name, value: v});
        }
    });
    //hand off to jQuery.param for proper encoding
    return $.param(a);
};

/**
 * Returns the value(s) of the element in the matched set.  For example, consider the following form:
 *
 *  <form><fieldset>
 *      <input name="A" type="text" />
 *      <input name="A" type="text" />
 *      <input name="B" type="checkbox" value="B1" />
 *      <input name="B" type="checkbox" value="B2"/>
 *      <input name="C" type="radio" value="C1" />
 *      <input name="C" type="radio" value="C2" />
 *  </fieldset></form>
 *
 *  var v = $('input[type=text]').fieldValue();
 *  // if no values are entered into the text inputs
 *  v == ['','']
 *  // if values entered into the text inputs are 'foo' and 'bar'
 *  v == ['foo','bar']
 *
 *  var v = $('input[type=checkbox]').fieldValue();
 *  // if neither checkbox is checked
 *  v === undefined
 *  // if both checkboxes are checked
 *  v == ['B1', 'B2']
 *
 *  var v = $('input[type=radio]').fieldValue();
 *  // if neither radio is checked
 *  v === undefined
 *  // if first radio is checked
 *  v == ['C1']
 *
 * The successful argument controls whether or not the field element must be 'successful'
 * (per http://www.w3.org/TR/html4/interact/forms.html#successful-controls).
 * The default value of the successful argument is true.  If this value is false the value(s)
 * for each element is returned.
 *
 * Note: This method *always* returns an array.  If no valid value can be determined the
 *    array will be empty, otherwise it will contain one or more values.
 */
$.fn.fieldValue = function(successful) {
    for (var val=[], i=0, max=this.length; i < max; i++) {
        var el = this[i];
        var v = $.fieldValue(el, successful);
        if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length)) {
            continue;
        }
        if (v.constructor == Array)
            $.merge(val, v);
        else
            val.push(v);
    }
    return val;
};

/**
 * Returns the value of the field element.
 */
$.fieldValue = function(el, successful) {
    var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
    if (successful === undefined) {
        successful = true;
    }

    if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
        (t == 'checkbox' || t == 'radio') && !el.checked ||
        (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
        tag == 'select' && el.selectedIndex == -1)) {
            return null;
    }

    if (tag == 'select') {
        var index = el.selectedIndex;
        if (index < 0) {
            return null;
        }
        var a = [], ops = el.options;
        var one = (t == 'select-one');
        var max = (one ? index+1 : ops.length);
        for(var i=(one ? index : 0); i < max; i++) {
            var op = ops[i];
            if (op.selected) {
                var v = op.value;
                if (!v) { // extra pain for IE...
                    v = (op.attributes && op.attributes['value'] && !(op.attributes['value'].specified)) ? op.text : op.value;
                }
                if (one) {
                    return v;
                }
                a.push(v);
            }
        }
        return a;
    }
    return $(el).val();
};

/**
 * Clears the form data.  Takes the following actions on the form's input fields:
 *  - input text fields will have their 'value' property set to the empty string
 *  - select elements will have their 'selectedIndex' property set to -1
 *  - checkbox and radio inputs will have their 'checked' property set to false
 *  - inputs of type submit, button, reset, and hidden will *not* be effected
 *  - button elements will *not* be effected
 */
$.fn.clearForm = function(includeHidden) {
    return this.each(function() {
        $('input,select,textarea', this).clearFields(includeHidden);
    });
};

/**
 * Clears the selected form elements.
 */
$.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
    var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i; // 'hidden' is not in this list
    return this.each(function() {
        var t = this.type, tag = this.tagName.toLowerCase();
        if (re.test(t) || tag == 'textarea') {
            this.value = '';
        }
        else if (t == 'checkbox' || t == 'radio') {
            this.checked = false;
        }
        else if (tag == 'select') {
            this.selectedIndex = -1;
        }
        else if (includeHidden) {
            // includeHidden can be the value true, or it can be a selector string
            // indicating a special test; for example:
            //  $('#myForm').clearForm('.special:hidden')
            // the above would clean hidden inputs that have the class of 'special'
            if ( (includeHidden === true && /hidden/.test(t)) ||
                 (typeof includeHidden == 'string' && $(this).is(includeHidden)) )
                this.value = '';
        }
    });
};

/**
 * Resets the form data.  Causes all form elements to be reset to their original value.
 */
$.fn.resetForm = function() {
    return this.each(function() {
        // guard against an input with the name of 'reset'
        // note that IE reports the reset function as an 'object'
        if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType)) {
            this.reset();
        }
    });
};

/**
 * Enables or disables any matching elements.
 */
$.fn.enable = function(b) {
    if (b === undefined) {
        b = true;
    }
    return this.each(function() {
        this.disabled = !b;
    });
};

/**
 * Checks/unchecks any matching checkboxes or radio buttons and
 * selects/deselects and matching option elements.
 */
$.fn.selected = function(select) {
    if (select === undefined) {
        select = true;
    }
    return this.each(function() {
        var t = this.type;
        if (t == 'checkbox' || t == 'radio') {
            this.checked = select;
        }
        else if (this.tagName.toLowerCase() == 'option') {
            var $sel = $(this).parent('select');
            if (select && $sel[0] && $sel[0].type == 'select-one') {
                // deselect all other options
                $sel.find('option').selected(false);
            }
            this.selected = select;
        }
    });
};

// expose debug var
$.fn.ajaxSubmit.debug = false;

// helper fn for console logging
function log() {
    if (!$.fn.ajaxSubmit.debug) 
        return;
    var msg = '[jquery.form] ' + Array.prototype.join.call(arguments,'');
    if (window.console && window.console.log) {
        window.console.log(msg);
    }
    else if (window.opera && window.opera.postError) {
        window.opera.postError(msg);
    }
}

})(jQuery);



/*
 * Dialog2: Yet another dialog plugin for jQuery.
 * 
 * This time based on bootstrap styles with some nice ajax control features, 
 * zero dependencies to jQuery.UI and basic options to control it.
 * 
 * Licensed under the MIT license 
 * http://www.opensource.org/licenses/mit-license.php 
 * 
 * @version: 2.0.0 (22/03/2012)
 * 
 * @requires jQuery >= 1.4 
 * 
 * @requires jQuery.form plugin (http://jquery.malsup.com/form/) >= 2.8 for ajax form submit 
 * @requires jQuery.controls plugin (https://github.com/Nikku/jquery-controls) >= 0.9 for ajax link binding support
 * 
 * @requires bootstrap styles (twitter.github.com/bootstrap) in version 2.x to look nice
 * 
 * @author nico.rehwaldt
 */
(function($) {
    
    /**
     * Dialog html markup
     */
    var __DIALOG_HTML = "<div class='modal' style=\"display: none;\">" +  
        "<div class='modal-header'>" +
        "<a href='#' class='close'></a>" + 
        //"<span class='loader'></span><h3></h3>" +
        "<h3></h3>" +
        "</div>" + 
        "<div class='modal-body'>" + 
        "</div>" + 
        "<div class='modal-footer'>" + 
        "</div>" + 
        "</div>";
    
    /**
     * Constructor of Dialog2 internal representation 
     */
    var Dialog2 = function(element, options) {
    	
        this.__init(element, options);
        
        var dialog = this;
        var handle = this.__handle;
        
        this.__ajaxCompleteTrigger = $.proxy(function() {
            this.trigger("dialog2.ajax-complete");
            this.trigger("dialog2.content-update");
        }, handle);
        
        this.__ajaxStartTrigger = $.proxy(function() {
            this.trigger("dialog2.ajax-start");
        }, handle);
        
		/* 
		 * Allows us to change the xhr based on our dialog, e.g. 
		 * attach url parameter or http header to identify it) 
		 */
		this.__ajaxBeforeSend = $.proxy(function(xhr, settings) {
			handle.trigger("dialog2.before-send", [xhr, settings]);
			
			if ($.isFunction($.fn.dialog2.defaults.beforeSend)) {
				$.fn.dialog2.defaults.beforeSend.call(this, xhr, settings);
			}
		}, handle);
		
        this.__removeDialog = $.proxy(this.__remove, this);
        
        handle.bind("dialog2.ajax-start", function() {
            dialog.options({buttons: options.autoAddCancelButton ? localizedCancelButton() : {}});
            $('<span />', {
            	'class': 'loader'
            }).prependTo(handle);
            //handle.html('<span class="loader"></span>'); // xxx
            handle.parent().addClass("loading");
        });
        
        handle.bind("dialog2.content-update", function() {
            dialog.__ajaxify();
            dialog.__updateMarkup();
            dialog.__focus();
        });
        
        handle.bind("dialog2.ajax-complete", function() {
            handle.parent().removeClass("loading");
        });
        
        // Apply options to make title and stuff shine
        this.options(options);

        // We will ajaxify its contents when its new
        // aka apply ajax styles in case this is a inpage dialog
        handle.trigger("dialog2.content-update");
    };
    
    /**
     * Dialog2 api; methods starting with underscore (__) are regarded internal
     * and should not be used in production environments
     */
    Dialog2.prototype = {
    
        /**
         * Core function for creating new dialogs.
         * Transforms a jQuery selection into dialog content, following these rules:
         * 
         * // selector is a dialog? Does essentially nothing
         * $(".selector").dialog2();
         * 
         * // .selector known?
         * // creates a dialog wrapped around .selector
         * $(".selector").dialog2();
         * 
         * // creates a dialog wrapped around .selector with id foo
         * $(".selector").dialog2({id: "foo"});
         * 
         * // .unknown-selector not known? Creates a new dialog with id foo and no content
         * $(".unknown-selector").dialog2({id: "foo"});
         */    
        __init: function(element, options) {
            var selection = $(element);
            var handle;

            if (!selection.is(".modal-body")) {
                var overlay = $('<div class="modal-backdrop"></div>').hide();
                var parentHtml = $(__DIALOG_HTML);
                
                if (options.modalClass) {
                    parentHtml.addClass(options.modalClass);
                    delete options.modalClass;
                }
                
                $(".modal-header a.close", parentHtml)
                    .text(unescape("%D7"))
                    .click(function(event) {
                        event.preventDefault();

                        $(this)
                            .parents(".modal")
                            .find(".modal-body")
                                .dialog2("close");
                    });

                $("body").append(overlay).append(parentHtml);
                
                handle = $(".modal-body", parentHtml);

                // Create dialog body from current jquery selection
                // If specified body is a div element and only one element is 
                // specified, make it the new modal dialog body
                // Allows us to do something like this 
                // $('<div id="foo"></div>').dialog2(); $("#foo").dialog2("open");
                if (selection.is("div") && selection.length == 1) {
                    handle.replaceWith(selection);
                    selection.addClass("modal-body").show();
                    handle = selection;
                }
                // If not, append current selection to dialog body
                else {
                    handle.append(selection);
                }

                if (options.id) {
                    handle.attr("id", options.id);
                }
            } else {
                handle = selection;
            }
            
            this.__handle = handle;
            this.__overlay = handle.parent().prev(".modal-backdrop");
            
            this.__addFocusCatchers(parentHtml);
        }, 
        
        __addFocusCatchers: function(parentHandle) {
        	if (parentHandle===undefined) return;
            parentHandle.prepend(new FocusCatcher(this.__handle, true));
            parentHandle.append(new FocusCatcher(this.__handle, false));
        }, 
        
        /**
         * Parse dialog content for markup changes (new buttons or title)
         */
        __updateMarkup: function() {
            var dialog = this;
            var e = dialog.__handle;
            
            e.trigger("dialog2.before-update-markup");
            
            // New options for dialog
            var options = {};

            // Add buttons to dialog for all buttons found within 
            // a .form-actions area inside the dialog
			
			// Instead of hiding .form-actions we remove it from view to fix an issue with ENTER not submitting forms 
			// when the submit button is not displayed
            var actions = $(".form-actions", e).css({ position: "absolute", top: "-9999px", height: "1px" });
			
            var buttons = actions.find("input[type=submit], input[type=button], input[type=reset], button, .btn");

            if (buttons.length) {
                options.buttons = {};

                buttons.each(function() {
                    var button = $(this);
                    var name = button.is("input") ? button.val() || button.attr("type") : button.text();

                    options.buttons[name] = {
                        primary: button.is("input[type=submit] .btn-primary"),
                        type: button.attr("class"), 
                        click: function(event) {
                            if (button.is("a")) { window.location = button[0].href }
                            // simulate click on the original button
                            // to not destroy any event handlers
                            button.click();

                            if (button.is(".close-dialog")) {
                                dialog.close();
                            }
                        }
                    };
                });
            }

            // set title if content contains a h1 element
            var titleElement = e.find("h1").hide();
            if (titleElement.length > 0) {
                options.title = titleElement.html();
            }

            // apply options on dialog
            dialog.options(options);

            e.trigger("dialog2.after-update-markup");
        },
        
        /**
         * Apply ajax specific dialog behavior to the dialogs contents
         */
        __ajaxify: function() {
            var dialog = this;
            var e = this.__handle;

            e.trigger("dialog2.before-ajaxify");
            
            e.find("a.ajax").click(function(event) {
                var url = $(this).attr("href");
                dialog.load(url);
                event.preventDefault();
            }).removeClass("ajax");

            // Make submitable for an ajax form 
            // if the jquery.form plugin is provided
            if ($.fn.ajaxForm) {
				var options = {
                    target: e,
                    success: dialog.__ajaxCompleteTrigger,
                    beforeSubmit: dialog.__ajaxStartTrigger, 
					beforeSend: dialog.__ajaxBeforeSend, 
                    error: function(context, status, error) {
                        $(".modal-body").html("<div>Form submit failed: " + status + " - " + error + '<div>');
                        e.trigger("dialog2.content-update");
                    }
                };
				
                $("form.ajax", e)
					.removeClass("ajax")
					.ajaxForm(options);
            }
            
            e.trigger("dialog2.after-ajaxify");
        },
        
        /**
         * Removes the dialog instance and its 
         * overlay from the DOM
         */
        __remove: function() {
            this.__overlay.remove();
            this.__handle.removeData("dialog2").parent().remove();
        }, 
        
        /**
         * Focuses the dialog which will essentially focus the first
         * focusable element in it (e.g. a link or a button on the button bar).
         * 
         * @param backwards whether to focus backwards or not
         */
        __focus: function(backwards) {
            var dialog = this.__handle;
            
            // Focus first focusable element in dialog
            var focusable = dialog
                              .find("a, input:not([type=hidden]), .btn, select, textarea, button")
                              .filter(function() {
                                  return $(this).parents(".form-actions").length == 0;
                              }).eq(0);
            
            // may be a button, too
            var focusableButtons = dialog
                                      .parent()
                                      .find(".modal-footer")
                                      .find("input[type=submit], input[type=button], .btn, button");
            
            var focusableElements = focusable.add(focusableButtons);
            var focusedElement = focusableElements[backwards ? "last" : "first"]();
            
            // Focus the element
            focusedElement.focus();
            
            dialog.trigger("dialog2.focussed", [focusedElement.get(0)]);
            return this;
        }, 
        
        /**
         * Focuses the dialog which will essentially focus the first
         * focusable element in it (e.g. a link or a button on the button bar).
         */
        focus: function() {
            return this.__focus();
        }, 
        
        /**
         * Close the dialog, removing its contents from the DOM if that is
         * configured.
         */
        close: function() {
            var dialog = this.__handle;
            var overlay = this.__overlay;
            
            overlay.hide();
            
            dialog
                .parent().hide().end()
                .trigger("dialog2.closed")
                .removeClass("opened");
        },
        
        /**
         * Open a dialog, if it is not opened already
         */
        open: function() {
            var dialog = this.__handle;
            
            if (!dialog.is(".opened")) {
                this.__overlay.show();
                
                dialog
                    .trigger("dialog2.before-open")
                    .addClass("opened")
                    .parent()
                        .show()
                        .end()
                    .trigger("dialog2.opened");
                    
                this.__focus();
            }
        }, 
        
        /**
         * Add button with the given name and options to the dialog
         * 
         * @param name of the button
         * @param options either function or options object configuring 
         *        the behaviour and markup of the button
         */
        addButton: function(name, options) {
            var handle = this.__handle;
            
            var callback = $.isFunction(options) ? options : options.click;
            var footer = handle.siblings(".modal-footer");

            var button = $("<a href='#' class='btn'></a>")
                                .text(name)
                                .click(function(event) {
                                    callback.apply(handle, [event]);
                                    event.preventDefault();
                                });

            // legacy
            if (options.primary) {
                button.addClass("btn-primary");
            }

            if (options.type) {
                button.addClass(options.type);
            }

            footer.append(button);
        }, 
        
        /**
         * Remove button with the given name
         * 
         * @param name of the button to be removed
         */
        removeButton: function(name) {
            var footer = this.__handle.siblings(".modal-footer");
                
            footer
                .find("a.btn")
                    .filter(function(i, e) {return $(e).text() == name;})
                        .remove();
			
			return this;
        }, 
        
        /**
         * Load the given url as content of this dialog
         * 
         * @param url to be loaded via GET or options.ajaxType, if defined
         */
        load: function(url) {
            var handle = this.__handle;
            
            if (handle.is(":empty")) {
                var loadText = this.options().initialLoadText;
                handle.html($("<span></span>").text(loadText));
            }
            
			handle
				.trigger("dialog2.ajax-start");
			
			dialogLoad.call(handle, url, this.__ajaxCompleteTrigger, this.__ajaxBeforeSend, this.options().ajaxType, this.options());
			
			return this;
        },
        
        /**
         * Apply the given options to the dialog
         * 
         * @param options to be applied
         */
        options: function(options) {
            var storedOptions = this.__handle.data("options");
            
            // Return stored options if getter was called
            if (!options) {
                return storedOptions;
            }
            
            var buttons = options.buttons;
            delete options.buttons;
            
            // Store options if none have been stored so far
            if (!storedOptions) {
                this.__handle.data("options", options);
            }
            
            var dialog = this;
            
            var handle = dialog.__handle;
            var overlay = dialog.__overlay;
            
            var parentHtml = handle.parent();
            
            if (options.title) {
                $(".modal-header h3", parentHtml).html(options.title);
            }
            
            if (buttons) {
                if (buttons.__mode != "append") {
                    $(".modal-footer", parentHtml).empty();
                }
                
                $.each(buttons, function(name, value) {
                    dialog.addButton(name, value);
                });
            }
            
            if (__boolean(options.closeOnOverlayClick)) {
                overlay.unbind("click");
                
                if (options.closeOnOverlayClick) {
                    overlay.click(function(event) {
                        if ($(event.target).is(".modal-backdrop")) {
                            dialog.close();
                        }
                    });
                }
            }
            
            if (__boolean(options.showCloseHandle)) {
                var closeHandleMode = options.showCloseHandle ? "show" : "hide";
                $(".modal-header .close", parentHtml)[closeHandleMode]();
            }
            
            if (__boolean(options.removeOnClose)) {
                handle.unbind("dialog2.closed", this.__removeDialog);
                
                if (options.removeOnClose) {
                    handle.bind("dialog2.closed", this.__removeDialog);
                }
            }
            
            if (options.autoOpen === true) {
                this.open();
            }
            
            if (options.content) {
                this.load(options.content);
            }
            
            delete options.buttons;
            
            options = $.extend(true, {}, storedOptions, options);
            this.__handle.data("options", options);
            
            return this;
        }, 
        
        /**
         * Returns the html handle of this dialog
         */
        handle: function() {
            return this.__handle;
        }
    };
    
    /**
     * Returns a simple DOM node which -- while being invisible to the user -- 
     * should focus the given argument when the focus is directed to itself. 
     */
    function FocusCatcher(dialog, reverse) {
        return $("<span />")
            .css({"float": "right", "width": "0px"})
            .attr("tabindex", 0)
            .focus(function(event) {
                  $(dialog).dialog2("__focus", reverse);
                  event.preventDefault();
            });
    };
    
    /**
     * Plugging the extension into the jQuery API
     */
    $.extend($.fn, {
        
        /**
         * options = {
         *   title: "Some title", 
         *   id: "my-id", 
         *   buttons: {
         *     "Name": Object || function   
         *   }
         * };
         * 
         * $(".selector").dialog2(options);
         * 
         * or 
         * 
         * $(".selector").dialog2("method", arguments);
         */
        dialog2: function() {
            var args = $.makeArray(arguments);
            var arg0 = args.shift();
            
            var dialog = $(this).data("dialog2");
            if (!dialog) {
                var options = $.extend(true, {}, $.fn.dialog2.defaults);
                if ($.isPlainObject(arg0)) {
                    options = $.extend(true, options, arg0);
                }
                
                dialog = new Dialog2(this, options);
                dialog.handle().data("dialog2", dialog);
            } else {
                if (typeof arg0 == "string") {
                    var method = dialog[arg0];
                    if (method) {
                        var result = dialog[arg0].apply(dialog, args);
                        return (result == dialog ? dialog.handle() : result);
                    } else {
                        throw new __error("Unknown API method '" + arg0 + "'");
                    }
                } else 
                if ($.isPlainObject(arg0)) {
                    dialog.options(arg0);
                } else {
                    throw new __error("Unknown API invocation: " + arg0 + " with args " + args);
                }
            }

            return dialog.handle();
        }
    });
    
    /***********************************************************************
     * Closing dialog via ESCAPE key
     ***********************************************************************/
    
    $(document).ready(function() {
        $(document).keyup(function(event) {
            if (event.which == 27) { // ESCAPE key pressed
                $(this).find(".modal > .opened").each(function() {
                    var dialog = $(this);
                    if (dialog.dialog2("options").closeOnEscape) {
                        dialog.dialog2("close");
                    }
                });
            }
        });
    });
    
    
    /***********************************************************************
     * Limit TAB integration in open modals via keypress
     ***********************************************************************/

    $(document).ready(function(event) {

        $(document).keyup(function(event) {
            if (event.which == 9) { // TAB key pressed
                // There is actually a dialog opened
                if ($(".modal .modal-body.opened").length && $('.modal .modal-body.opened .jq-selectbox.jqselect.opened').length == 0) {
                    // Set timeout (to let the browser perform the tabbing operation
                    // and check the active element)
                    setTimeout(function() {
                        var activeElement = document.activeElement;
                        if (activeElement) {
                            var activeElementModal = $(activeElement).parents(".modal").find(".modal-body.opened");
                            // In the active modal dialog! Everything ok
                            if (activeElementModal.length != 0) {
                                return;
                            }
                        }
    
                        // Did not return; have to focus active modal dialog
                        $(".modal-body.opened").dialog2("focus");
                    }, 0);
                }
            }
        });
    });

    /**
     * Random helper functions; today: 
     * Returns true if value is a boolean
     * 
     * @param value the value to check
     * @return true if the value is a boolean
     */
    function __boolean(value) {
        return typeof value == "boolean";
    };
    
    /**
     * Creates a dialog2 error with the given message
     * 
     * @param errorMessage stuff to signal the user
     * @returns the error object to be thrown
     */
    function __error(errorMessage) {
        new Error("[jquery.dialog2] " + errorMessage);
    };
    
    /**
     * Dialog2 plugin defaults (may be overriden)
     */
    $.fn.dialog2.defaults = {
        autoOpen: true, 
        closeOnOverlayClick: true, 
        removeOnClose: true, 
        showCloseHandle: true, 
        initialLoadText: "", 
        closeOnEscape: true, 
		beforeSend: null
    };
    
    /***********************************************************************
     * Localization
     ***********************************************************************/
    
    $.fn.dialog2.localization = {
        "de": {
            cancel: "Abbrechen"
        },
        "en": {
            cancel: "Cancel"
        }
    };
    
    var lang = $.fn.dialog2.localization["en"];
    
    /**
     * Localizes a given key using the selected language
     * 
     * @param key the key to localize
     * @return the localization of the key or the key itself if it could not be localized.
     */
    function localize(key) {
        return lang[key.toLowerCase()] || key;
    };
    
    /**
     * Creates a localized button and returns the buttons object specifying 
     * a number of buttons. May pass a buttons object to add the button to.
     * 
     * @param name to be used as a button label (localized)
     * @param functionOrOptions function or options to attach to the button
     * @param buttons object to attach the button to (may be null to create new one)
     * 
     * @returns buttons object or new object with the button added
     */
    function localizedButton(name, functionOrOptions, buttons) {
        buttons = buttons || {};
        buttons[localize(name)] = functionOrOptions;
        return buttons;
    };
    
    /**
     * Expose some localization helper methods via $.fn.dialog2.localization
     */
    $.extend($.fn.dialog2.localization, {
        localizedButton: localizedButton, 
        get: localize, 
        
        setLocale: function(key) {
            var localization = $.fn.dialog2.localization[key];

            if (localization == null) {
                throw new Error("No localizaton for language " + key);
            } else {
                lang = localization;
            }
        }
    });
    
    /**
     * Returns a localized cancel button
     * @return a buttons object containing a localized cancel button 
     *         (including its close functionality)
     */
    function localizedCancelButton() {
        return localizedButton("close", function() {
            $(this).dialog2("close");
        });
    };
    
    /***********************************************************************
     * jQuery load with before send integration
	 * copied from jQuery.fn.load but with beforeSendCallback support
     ***********************************************************************/
    
	function dialogLoad(url, completeCallback, beforeSendCallback, type, options) {
	
		// Don't do a request if no elements are being requested
		if ( !this.length ) {
			return this;
		}

		var selector, response,
			self = this,
			off = url.indexOf(" ");

		if ( off >= 0 ) {
			selector = url.slice( off, url.length );
			url = url.slice( 0, off );
		}
		
		// Request the remote document
		jQuery.ajax({
			url: url,

			// if "type" variable is undefined, then "GET" method will be used
			type: type,
			data: options.data,
			dataType: "html",
			beforeSend: beforeSendCallback, 
			complete: function( jqXHR, status ) {
				if ( completeCallback ) {
					self.each( completeCallback, response || [ jqXHR.responseText, status, jqXHR ] );
				}
			}
		}).done(function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			// See if a selector was specified
			self.html( selector ?

				// Create a dummy div to hold the results
				jQuery("<div>")

					// inject the contents of the document in, removing the scripts
					// to avoid any 'Permission Denied' errors in IE
					.append( responseText.replace( rscript, "" ) )

					// Locate the specified elements
					.find( selector ) :

				// If not, just inject the full result
				responseText );

		});

		return this;
	}
	
    /***********************************************************************
     * Integration with jquery.controls
     ***********************************************************************/
    
    /**
     * Register opening of a dialog on annotated links
     * (works only if jquery.controls plugin is installed). 
     */
    if ($.fn.controls && $.fn.controls.bindings) {
        $.extend($.fn.controls.bindings, {
            "a.open-dialog": function() {
                var a = $(this).removeClass("open-dialog");
                
                var id = a.attr("rel");
                var content = a.attr("href");
                
                if (a.attr("data-dialog-id")) {
                	id = a.attr("data-dialog-id");
                }
                
                
                var options = {
                    modal: true
                };

                var element;

                if (id) {
                    var e = $("#" + id);
                    if (e.length) element = e;
                }

                if (!element) {
                    if (id) {
                        options.id = id;
                    }
                }
                
                if (a.attr("data-dialog-class")) {
                    options.modalClass = a.attr("data-dialog-class");
                }
                
                if (a.attr("data-dialog-title")) {
                    options.title = a.attr("data-dialog-title");
                }
                else {
                    if (a.attr("title")) {
                        options.title = a.attr("title");
                    }
                }
                
                // GET/POST
                if (a.attr("data-ajaxType")) {
                	var ajaxType = a.attr("data-ajaxType");
                	if (ajaxType.toLowerCase() == 'post' || ajaxType.toLowerCase() == 'get') {
                		options.ajaxType = ajaxType;
                	}
                }

                
                
                $.each($.fn.dialog2.defaults, function(key, value) {
                    if (a.attr(key)) {
                        options[key] = a.attr(key) == "true";
                    }
                });
                if (content && content != "#") {
                    options.content = content;
                    
                    a.click(function(event) {
                        event.preventDefault();
                        $(element || "<div></div>").dialog2(options);
                    });
                } else {
                    options.removeOnClose = false;
                    options.autoOpen = false;
                    
                    element = element || "<div></div>";
                    
                    // Pre initialize dialog
                    $(element).dialog2(options);
                    
                    a.attr("href", "#")
                     .click(function(event) {
                         event.preventDefault();
                         $(element).dialog2("open");
                     });
                }
            }
        });
    };
})(jQuery);




/*
 * Dialog2: Yet another dialog plugin for jQuery.
 * 
 * This time based on bootstrap styles with some nice ajax control features, 
 * zero dependencies to jQuery.UI and basic options to control it.
 * 
 * Licensed under the MIT license 
 * http://www.opensource.org/licenses/mit-license.php 
 * 
 * @version: 2.0.0 (22/03/2012)
 * 
 * @requires jQuery >= 1.4 
 * 
 * @requires jQuery.form plugin (http://jquery.malsup.com/form/) >= 2.8 for ajax form submit 
 * @requires jQuery.controls plugin (https://github.com/Nikku/jquery-controls) >= 0.9 for ajax link binding support
 * 
 * @requires bootstrap styles (twitter.github.com/bootstrap) in version 2.x to look nice
 * 
 * @author nico.rehwaldt
 */

/**
 * This script file extends the plugin to provide helper functions
 * alert(), confirm() and prompt()
 * 
 * Thanks to ulrichsg for the contribution
 */
(function($) {
    var localizedButton = $.fn.dialog2.localization.localizedButton;
    
    var __helpers = {
        
        /**
         * Creates an alert displaying the given message.
         * Will call options.close on close (if specified).
         * 
         *   $.fn.dialog2.alert("This dialog is non intrusive", { 
         *       close: function() {
         *           alert("This one is!");
         *       }
         *   });
         * 
         * @param message to be displayed as the dialog body
         * @param options (optional) to be used when creating the dialog
         */
        alert: function(message, options) {
            options = $.extend({}, options);
            var labels = $.extend({}, $.fn.dialog2.helpers.defaults.alert, options);
            
            var dialog = $("<div />");
            
            var closeCallback = options.close;
            delete options.close;
            
            var buttons = localizedButton(labels.buttonLabelOk, __closeAndCall(closeCallback, dialog));
            
            return __open(dialog, message, labels.title, buttons, options);
        }, 
        
        /**
         * Creates an confirm dialog displaying the given message.
         * 
         * Will call options.confirm on confirm (if specified).
         * Will call options.decline on decline (if specified).
         * 
         *   $.fn.dialog2.confirm("Is this dialog non intrusive?", {
         *       confirm: function() { alert("You said yes? Well... no"); }, 
         *       decline: function() { alert("You said no? Right choice!") }
         *   });
         * 
         * @param message to be displayed as the dialog body
         * @param options (optional) to be used when creating the dialog
         */
        confirm: function(message, options) {
            options = $.extend({}, options);
            var labels = $.extend({}, $.fn.dialog2.helpers.defaults.confirm, options);
            
            var dialog = $("<div />");
            
            var confirmCallback = options.confirm;
            delete options.confirm;
            
            var declineCallback = options.decline;
            delete options.decline;
            
            var buttons = {};
            localizedButton(labels.buttonLabelYes, __closeAndCall(confirmCallback, dialog), buttons);
            localizedButton(labels.buttonLabelNo, __closeAndCall(declineCallback, dialog), buttons);
            
            return __open(dialog, message, labels.title, buttons, options);
        },
        
        /**
         * Creates an prompt dialog displaying the given message together with 
         * an element to input text in.
         * 
         * Will call options.ok on ok (if specified).
         * Will call options.cancel on cancel (if specified).
         * 
         *   $.fn.dialog2.prompt("What is your age?", {
         *       ok: function(event, value) { alert("Your age is: " + value); }, 
         *       cancel: function() { alert("Better tell me!"); }
         *   });
         * 
         * @param message to be displayed as the dialog body
         * @param options (optional) to be used when creating the dialog
         */
        prompt: function(message, options) {
            // Special: Dialog has to be closed on escape or multiple inputs
            // with the same id will be added to the DOM!
            options = $.extend({}, options, {closeOnEscape: true});
            var labels = $.extend({}, $.fn.dialog2.helpers.defaults.prompt, options);
            
            var inputId = 'dialog2.helpers.prompt.input.id';
            var input = $("<input type='text' class='span6' />")
                                .attr("id", inputId)
                                .val(options.defaultValue || "");
                                
            var html = $("<form class='form-stacked'></form>");
            html.append($("<label/>").attr("for", inputId).text(message));
            html.append(input);
            
            var dialog = $("<div />");
            
            var okCallback;
            if (options.ok) {
                var fn = options.ok;
                okCallback = function(event) { fn.call(dialog, event, input.val()); };
            }
            delete options.ok;
            
            var cancelCallback = options.cancel;
            delete options.cancel;
            
            var buttons = {};
            localizedButton(labels.buttonLabelOk, __closeAndCall(okCallback, dialog), buttons);
            localizedButton(labels.buttonLabelCancel, __closeAndCall(cancelCallback, dialog), buttons);
            
			// intercept form submit (on ENTER press)
			html.bind("submit", __closeAndCall(okCallback, dialog));
			
            __open(dialog, html, labels.title, buttons, options);
        }, 
        
        /**
         * Default helper options
         */
        defaults: {}
    };
    
    function __closeAndCall(callback, dialog) {
        return $.proxy(function(event) {
			event.preventDefault();
			
            $(this).dialog2("close");
            
            if (callback) {
                callback.call(this, event);
            }
        }, dialog || this);
    };
    
    function __open(e, message, title, buttons, options) {
        options.buttons = buttons;
        options.title = title;
        
        return e.append(message).dialog2(options);
    };
    
    $.extend(true, $.fn.dialog2, {
        helpers: __helpers
    });
    
    $.extend($.fn.dialog2.helpers.defaults, {
        alert: {
            title: 'Alert', 
            buttonLabelOk: 'Ok' 
        }, 
        
        prompt: {
            title: 'Prompt',
            buttonLabelOk: 'Ok', 
            buttonLabelCancel: 'Cancel' 
        }, 
        
        confirm: {
            title: 'Confirmation',
            buttonLabelYes: 'Yes',
            buttonLabelNo: 'No'
        }
    });
})(jQuery);




/**
 * bootbox.js v3.2.0
 *
 * http://bootboxjs.com/license.txt
 */
var bootbox = window.bootbox || (function(document, $) {
    /*jshint scripturl:true sub:true */

    var _locale        = 'en',
        _defaultLocale = 'en',
        _animate       = true,
        _backdrop      = 'static',
        _defaultHref   = 'javascript:;',
        _classes       = '',
        _btnClasses    = {},
        _icons         = {},
        /* last var should always be the public object we'll return */
        that           = {};


    /**
     * public API
     */
    that.setLocale = function(locale) {
        for (var i in _locales) {
            if (i == locale) {
                _locale = locale;
                return;
            }
        }
        throw new Error('Invalid locale: '+locale);
    };

    that.addLocale = function(locale, translations) {
        if (typeof _locales[locale] === 'undefined') {
            _locales[locale] = {};
        }
        for (var str in translations) {
            _locales[locale][str] = translations[str];
        }
    };

    that.setIcons = function(icons) {
        _icons = icons;
        if (typeof _icons !== 'object' || _icons === null) {
            _icons = {};
        }
    };

    that.setBtnClasses = function(btnClasses) {
        _btnClasses = btnClasses;
        if (typeof _btnClasses !== 'object' || _btnClasses === null) {
            _btnClasses = {};
        }
    };

    that.alert = function(/*str, label, cb*/) {
        var str   = "",
            label = _translate('OK'),
            cb    = null;

        switch (arguments.length) {
            case 1:
                // no callback, default button label
                str = arguments[0];
                break;
            case 2:
                // callback *or* custom button label dependent on type
                str = arguments[0];
                if (typeof arguments[1] == 'function') {
                    cb = arguments[1];
                } else {
                    label = arguments[1];
                }
                break;
            case 3:
                // callback and custom button label
                str   = arguments[0];
                label = arguments[1];
                cb    = arguments[2];
                break;
            default:
                throw new Error("Incorrect number of arguments: expected 1-3");
        }

        return that.dialog(str, {
            // only button (ok)
            "label"   : label,
            "icon"    : _icons.OK,
            "class"   : _btnClasses.OK,
            "callback": cb
        }, {
            // ensure that the escape key works; either invoking the user's
            // callback or true to just close the dialog
            "onEscape": cb || true
        });
    };

    that.confirm = function(/*str, labelCancel, labelOk, cb*/) {
        var str         = "",
            labelCancel = _translate('CANCEL'),
            labelOk     = _translate('CONFIRM'),
            cb          = null;

        switch (arguments.length) {
            case 1:
                str = arguments[0];
                break;
            case 2:
                str = arguments[0];
                if (typeof arguments[1] == 'function') {
                    cb = arguments[1];
                } else {
                    labelCancel = arguments[1];
                }
                break;
            case 3:
                str         = arguments[0];
                labelCancel = arguments[1];
                if (typeof arguments[2] == 'function') {
                    cb = arguments[2];
                } else {
                    labelOk = arguments[2];
                }
                break;
            case 4:
                str         = arguments[0];
                labelCancel = arguments[1];
                labelOk     = arguments[2];
                cb          = arguments[3];
                break;
            default:
                throw new Error("Incorrect number of arguments: expected 1-4");
        }

        var cancelCallback = function() {
            if (typeof cb === 'function') {
                return cb(false);
            }
        };

        var confirmCallback = function() {
            if (typeof cb === 'function') {
                return cb(true);
            }
        };

        return that.dialog(str, [{
            // first button (cancel)
            "label"   : labelCancel,
            "icon"    : _icons.CANCEL,
            "class"   : _btnClasses.CANCEL,
            "callback": cancelCallback
        }, {
            // second button (confirm)
            "label"   : labelOk,
            "icon"    : _icons.CONFIRM,
            "class"   : _btnClasses.CONFIRM,
            "callback": confirmCallback
        }], {
            // escape key bindings
            "onEscape": cancelCallback
        });
    };

    that.prompt = function(/*str, labelCancel, labelOk, cb, defaultVal*/) {
        var str         = "",
            labelCancel = _translate('CANCEL'),
            labelOk     = _translate('CONFIRM'),
            cb          = null,
            defaultVal  = "";

        switch (arguments.length) {
            case 1:
                str = arguments[0];
                break;
            case 2:
                str = arguments[0];
                if (typeof arguments[1] == 'function') {
                    cb = arguments[1];
                } else {
                    labelCancel = arguments[1];
                }
                break;
            case 3:
                str         = arguments[0];
                labelCancel = arguments[1];
                if (typeof arguments[2] == 'function') {
                    cb = arguments[2];
                } else {
                    labelOk = arguments[2];
                }
                break;
            case 4:
                str         = arguments[0];
                labelCancel = arguments[1];
                labelOk     = arguments[2];
                cb          = arguments[3];
                break;
            case 5:
                str         = arguments[0];
                labelCancel = arguments[1];
                labelOk     = arguments[2];
                cb          = arguments[3];
                defaultVal  = arguments[4];
                break;
            default:
                throw new Error("Incorrect number of arguments: expected 1-5");
        }

        var header = str;

        // let's keep a reference to the form object for later
        var form = $("<form></form>");
        form.append("<input autocomplete=off type=text value='" + defaultVal + "' />");

        var cancelCallback = function() {
            if (typeof cb === 'function') {
                // yep, native prompts dismiss with null, whereas native
                // confirms dismiss with false...
                return cb(null);
            }
        };

        var confirmCallback = function() {
            if (typeof cb === 'function') {
                return cb(form.find("input[type=text]").val());
            }
        };

        var div = that.dialog(form, [{
            // first button (cancel)
            "label"   : labelCancel,
            "icon"    : _icons.CANCEL,
            "class"   : _btnClasses.CANCEL,
            "callback":  cancelCallback
        }, {
            // second button (confirm)
            "label"   : labelOk,
            "icon"    : _icons.CONFIRM,
            "class"   : _btnClasses.CONFIRM,
            "callback": confirmCallback
        }], {
            // prompts need a few extra options
            "header"  : header,
            // explicitly tell dialog NOT to show the dialog...
            "show"    : false,
            "onEscape": cancelCallback
        });

        // ... the reason the prompt needs to be hidden is because we need
        // to bind our own "shown" handler, after creating the modal but
        // before any show(n) events are triggered
        // @see https://github.com/makeusabrew/bootbox/issues/69

        div.on("shown", function() {
            form.find("input[type=text]").focus();

            // ensure that submitting the form (e.g. with the enter key)
            // replicates the behaviour of a normal prompt()
            form.on("submit", function(e) {
                e.preventDefault();
                div.find(".btn-primary").click();
            });
        });

        div.modal("show");

        return div;
    };

    that.dialog = function(str, handlers, options) {
        var buttons    = "",
            callbacks  = [];

        if (!options) {
            options = {};
        }

        // check for single object and convert to array if necessary
        if (typeof handlers === 'undefined') {
            handlers = [];
        } else if (typeof handlers.length == 'undefined') {
            handlers = [handlers];
        }

        var i = handlers.length;
        while (i--) {
            var label    = null,
                href     = null,
                _class   = null,
                icon     = '',
                callback = null;

            if (typeof handlers[i]['label']    == 'undefined' &&
                typeof handlers[i]['class']    == 'undefined' &&
                typeof handlers[i]['callback'] == 'undefined') {
                // if we've got nothing we expect, check for condensed format

                var propCount = 0,      // condensed will only match if this == 1
                    property  = null;   // save the last property we found

                // be nicer to count the properties without this, but don't think it's possible...
                for (var j in handlers[i]) {
                    property = j;
                    if (++propCount > 1) {
                        // forget it, too many properties
                        break;
                    }
                }

                if (propCount == 1 && typeof handlers[i][j] == 'function') {
                    // matches condensed format of label -> function
                    handlers[i]['label']    = property;
                    handlers[i]['callback'] = handlers[i][j];
                }
            }

            if (typeof handlers[i]['callback']== 'function') {
                callback = handlers[i]['callback'];
            }

            if (handlers[i]['class']) {
                _class = handlers[i]['class'];
            } else if (i == handlers.length -1 && handlers.length <= 2) {
                // always add a primary to the main option in a two-button dialog
                _class = 'btn-primary';
            }

            if (handlers[i]['label']) {
                label = handlers[i]['label'];
            } else {
                label = "Option "+(i+1);
            }

            if (handlers[i]['icon']) {
                icon = "<i class='"+handlers[i]['icon']+"'></i> ";
            }

            if (handlers[i]['href']) {
                href = handlers[i]['href'];
            }
            else {
                href = _defaultHref;
            }

            buttons = "<a data-handler='"+i+"' class='btn "+_class+"' href='" + href + "'>"+icon+""+label+"</a>" + buttons;

            callbacks[i] = callback;
        }

        // @see https://github.com/makeusabrew/bootbox/issues/46#issuecomment-8235302
        // and https://github.com/twitter/bootstrap/issues/4474
        // for an explanation of the inline overflow: hidden
        // @see https://github.com/twitter/bootstrap/issues/4854
        // for an explanation of tabIndex=-1

        var parts = ["<div class='bootbox modal' tabindex='-1' style='overflow:hidden;'>"];

        if (options['header']) {
            var closeButton = '';
            if (typeof options['headerCloseButton'] == 'undefined' || options['headerCloseButton']) {
                closeButton = "<a href='"+_defaultHref+"' class='close'>&times;</a>";
            }

            parts.push("<div class='modal-header'>"+closeButton+"<h3>"+options['header']+"</h3></div>");
        }

        // push an empty body into which we'll inject the proper content later
        parts.push("<div class='modal-body'></div>");

        if (buttons) {
            parts.push("<div class='modal-footer'>"+buttons+"</div>");
        }

        parts.push("</div>");

        var div = $(parts.join("\n"));

        // check whether we should fade in/out
        var shouldFade = (typeof options.animate === 'undefined') ? _animate : options.animate;

        if (shouldFade) {
            div.addClass("fade");
        }

        var optionalClasses = (typeof options.classes === 'undefined') ? _classes : options.classes;
        if (optionalClasses) {
            div.addClass(optionalClasses);
        }

        // now we've built up the div properly we can inject the content whether it was a string or a jQuery object
        div.find(".modal-body").html(str);

        function onCancel(source) {
            // for now source is unused, but it will be in future
            var hideModal = null;
            if (typeof options.onEscape === 'function') {
                // @see https://github.com/makeusabrew/bootbox/issues/91
                hideModal = options.onEscape();
            }

            if (hideModal !== false) {
                div.modal('hide');
            }
        }

        // hook into the modal's keyup trigger to check for the escape key
        div.on('keyup.dismiss.modal', function(e) {
            // any truthy value passed to onEscape will dismiss the dialog
            // as long as the onEscape function (if defined) doesn't prevent it
            if (e.which === 27 && options.onEscape) {
                onCancel('escape');
            }
        });

        // handle close buttons too
        div.on('click', 'a.close', function(e) {
            e.preventDefault();
            onCancel('close');
        });

        // well, *if* we have a primary - give the first dom element focus
        div.on('shown', function() {
            div.find("a.btn-primary:first").focus();
        });

        div.on('hidden', function() {
            div.remove();
        });

        // wire up button handlers
        div.on('click', '.modal-footer a', function(e) {

            var handler   = $(this).data("handler"),
                cb        = callbacks[handler],
                hideModal = null;

            // sort of @see https://github.com/makeusabrew/bootbox/pull/68 - heavily adapted
            // if we've got a custom href attribute, all bets are off
            if (typeof handler                   !== 'undefined' &&
                typeof handlers[handler]['href'] !== 'undefined') {

                return;
            }

            e.preventDefault();

            if (typeof cb === 'function') {
                hideModal = cb();
            }

            // the only way hideModal *will* be false is if a callback exists and
            // returns it as a value. in those situations, don't hide the dialog
            // @see https://github.com/makeusabrew/bootbox/pull/25
            if (hideModal !== false) {
                div.modal("hide");
            }
        });

        // stick the modal right at the bottom of the main body out of the way
        $("body").append(div);

        div.modal({
            // unless explicitly overridden take whatever our default backdrop value is
            backdrop : (typeof options.backdrop  === 'undefined') ? _backdrop : options.backdrop,
            // ignore bootstrap's keyboard options; we'll handle this ourselves (more fine-grained control)
            keyboard : false,
            // @ see https://github.com/makeusabrew/bootbox/issues/69
            // we *never* want the modal to be shown before we can bind stuff to it
            // this method can also take a 'show' option, but we'll only use that
            // later if we need to
            show     : false
        });

        // @see https://github.com/makeusabrew/bootbox/issues/64
        // @see https://github.com/makeusabrew/bootbox/issues/60
        // ...caused by...
        // @see https://github.com/twitter/bootstrap/issues/4781
        div.on("show", function(e) {
            $(document).off("focusin.modal");
        });

        if (typeof options.show === 'undefined' || options.show === true) {
            div.modal("show");
        }

        return div;
    };

    /**
     * #modal is deprecated in v3; it can still be used but no guarantees are
     * made - have never been truly convinced of its merit but perhaps just
     * needs a tidyup and some TLC
     */
    that.modal = function(/*str, label, options*/) {
        var str;
        var label;
        var options;

        var defaultOptions = {
            "onEscape": null,
            "keyboard": true,
            "backdrop": _backdrop
        };

        switch (arguments.length) {
            case 1:
                str = arguments[0];
                break;
            case 2:
                str = arguments[0];
                if (typeof arguments[1] == 'object') {
                    options = arguments[1];
                } else {
                    label = arguments[1];
                }
                break;
            case 3:
                str     = arguments[0];
                label   = arguments[1];
                options = arguments[2];
                break;
            default:
                throw new Error("Incorrect number of arguments: expected 1-3");
        }

        defaultOptions['header'] = label;

        if (typeof options == 'object') {
            options = $.extend(defaultOptions, options);
        } else {
            options = defaultOptions;
        }

        return that.dialog(str, [], options);
    };


    that.hideAll = function() {
        $(".bootbox").modal("hide");
    };

    that.animate = function(animate) {
        _animate = animate;
    };

    that.backdrop = function(backdrop) {
        _backdrop = backdrop;
    };

    that.classes = function(classes) {
        _classes = classes;
    };

    /**
     * private API
     */

    /**
     * standard locales. Please add more according to ISO 639-1 standard. Multiple language variants are
     * unlikely to be required. If this gets too large it can be split out into separate JS files.
     */
    var _locales = {
        'en' : {
            OK      : 'OK',
            CANCEL  : 'Cancel',
            CONFIRM : 'OK'
        },
        'fr' : {
            OK      : 'OK',
            CANCEL  : 'Annuler',
            CONFIRM : 'D\'accord'
        },
        'de' : {
            OK      : 'OK',
            CANCEL  : 'Abbrechen',
            CONFIRM : 'Akzeptieren'
        },
        'es' : {
            OK      : 'OK',
            CANCEL  : 'Cancelar',
            CONFIRM : 'Aceptar'
        },
        'br' : {
            OK      : 'OK',
            CANCEL  : 'Cancelar',
            CONFIRM : 'Sim'
        },
        'nl' : {
            OK      : 'OK',
            CANCEL  : 'Annuleren',
            CONFIRM : 'Accepteren'
        },
        'ru' : {
            OK      : 'OK',
            CANCEL  : 'Отмена',
            CONFIRM : 'Применить'
        },
        'it' : {
            OK      : 'OK',
            CANCEL  : 'Annulla',
            CONFIRM : 'Conferma'
        }
    };

    function _translate(str, locale) {
        // we assume if no target locale is probided then we should take it from current setting
        if (typeof locale === 'undefined') {
            locale = _locale;
        }
        if (typeof _locales[locale][str] === 'string') {
            return _locales[locale][str];
        }

        // if we couldn't find a lookup then try and fallback to a default translation

        if (locale != _defaultLocale) {
            return _translate(str, _defaultLocale);
        }

        // if we can't do anything then bail out with whatever string was passed in - last resort
        return str;
    }

    return that;

}(document, window.jQuery));

// @see https://github.com/makeusabrew/bootbox/issues/71
window.bootbox = bootbox;





