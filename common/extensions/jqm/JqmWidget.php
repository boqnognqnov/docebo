<?php

/**
 * A widget to use jquery mobile
 *
 * @author Fabio Pirovano
 */

class JqmWidget extends CWidget {


	/**
	 * @var null|string Keeps published assets URL
	 */
	protected $_assetsUrl = null;


	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	protected function getAssetsUrl() {

		if (isset($this->_assetsUrl)) {

			return $this->_assetsUrl;
		} else {

			$assetsPath = realpath(dirname(__FILE__)) . '/assets/';
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			return $this->_assetsUrl = $assetsUrl;
		}
	}


	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init() {

		Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/jquery.mobile-1.4.5.min.css");
		// Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/docebo-theme.css");
		Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/jquery-1.11.2.min.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/jquery.mobile-1.4.5.min.js', CClientScript::POS_HEAD);
	}


	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run() {
		// only the init is needed so far
	}


}
