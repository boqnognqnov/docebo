<?php

/**
 * A simple component to serve several purposes:
 *
 * 1. Publish tinymce assets
 * 2. Register relevant script(s)
 * 3. Create a globaly available JS function to attach an element a tinymce editor.
 * 	  - TinyMce.attach() is defined and put in the HEAD upon component preloading.
 *    - Options passed as second argument are merged to defaultOptions defined in
 *      /common/config/main.php  (see tintmce component class)
 *
 * Usage:
 * 		$(function(){
 * 			TinyMce.attach("#my-textarea-field", {height: 400, ...});
 * 		});
 *
 *
 *
 */
class TinyMce extends CApplicationComponent {

	// defaultOptions=array() can be set in config
	// See Tinymce web site for available options
	public $defaultOptions = array();
	// Holds assets url
	protected $_assetsUrl;
	// If true, loads uniminified scripts
	public $debug = false;

	/**
	 * (non-PHPdoc)
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		// Register path alias
		if (Yii::getPathOfAlias('tinymce') === false) {
			Yii::setPathOfAlias('tinymce', realpath(dirname(__FILE__)));
		}

		// Prevents the extension from registering scripts and publishing assets when ran from the command line.
		if (Yii::app() instanceof CConsoleApplication)
			return;


		// Prevent loading for AJAX calls
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}

		$cs = Yii::app()->getClientScript();

		// Register the jQuery integration part (the loader)
		$cs->registerScriptFile($this->getAssetsUrl() . '/jquery.tinymce.min.js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($this->getAssetsUrl() . '/jquery.tinymce.' . (!$this->debug ? 'min.' : null) . 'js', CClientScript::POS_HEAD);
		$cs->registerScriptFile($this->getAssetsUrl() . '/tinymce.' . (!$this->debug ? 'min.' : null) . 'js', CClientScript::POS_HEAD);

		// script_url is NOT allowed in config; will be overwritten anyway
		$this->defaultOptions['script_url'] = $this->getScriptUrl();

		foreach ($this->defaultOptions as $optionsTypeName => $optionsTypeOptions) {
			if (is_array($optionsTypeOptions)) {

				if (!isset(Yii::app()->params['moxiemanager']['disabled']) || !Yii::app()->params['moxiemanager']['disabled']) {
					// Check if MoxieManager is completely disabled from params, if not, add it to TinyMCE

					$moxieAllowed = $this->isWhitelistedRoute();

					$event = new DEvent($this, array());
					Yii::app()->event->raise('BeforeAllowMoxieManager', $event);
					if (!$event->shouldPerformAsDefault())
						$moxieAllowed = $event->return_value['moxie_allowed'];
					else {
						if (Yii::app()->user->getIsGodadmin()) {
							$moxieAllowed = true;
						} elseif (Yii::app()->user->getIsPu()) {
							$moxieAllowed = true;
						} elseif (Yii::app()->getController() && property_exists(Yii::app()->getController(), 'userCanAdminCourse') && Yii::app()->getController()->userCanAdminCourse) {
							// Instructors who have control of the current course
							// Usually course related controllers extend from PlayerBaseController
							// which has the property 'userCanAdminCourse'=true when
							// a person has access to administer the course (e.g. Instructor)
							$moxieAllowed = true;
						}
					}

					// Add MoxieManager for Godadmins, Power Users and Instructors of the course only
					if ($moxieAllowed) {
						if (!isset($optionsTypeOptions['external_plugins']))
							$optionsTypeOptions['external_plugins'] = array();

						if (!isset($optionsTypeOptions['external_plugins']['moxiemanager'])) {
							$optionsTypeOptions['external_plugins']['moxiemanager'] = '../../../../moxiemanager/plugin.js'; // Always use forward slash "/"
						}

						// Overwrite this translation here, since main.php can't access Yii::t() yet
						$optionsTypeOptions['moxiemanager_title'] = Yii::t('templatemanager', 'Your images collection');
						$optionsTypeOptions['moxiemanager_leftpanel'] = false;
					}
				}
			}

			$this->defaultOptions[$optionsTypeName] = $optionsTypeOptions;
		}

		// JSON encode options array
		$options = CJavaScript::encode($this->defaultOptions);

		// Get current language, check if lang file exists in TinyMce langs folder; if not, set to English
		$language = Yii::app()->getLanguage();  //en, it, ...
		$assetsPath = Yii::getPathOfAlias('tinymce.assets');
		if (!is_file($assetsPath . "/langs/" . $language . ".js")) {
			$language = 'en';
		}

		$script = '
			var TinyMce = {};
			var uiLanguage = "' . $language . '";
			var defaultTinyMceConf = ' . $options . ';	
			TinyMce.attach = function(selector, options, custom) {
				var internalOptions = ' . $options . ', toUse, toExtend;
				if ($.type(options) == "string") {
					toUse = (internalOptions[options] || {});
					toExtend = custom;
				} else {
					toUse = (internalOptions.standard || {});
					toExtend = options;
				}
				$.extend(toUse, toExtend);
				TinyMce.destroy(selector);
				if (uiLanguage != "")
					toUse.language=uiLanguage;
				$(selector).tinymce(toUse);
			};
			TinyMce.destroy = function(selector) {
				if (window.tinymce) {
					//window.tinymce.remove(selector);
					$(selector).each(function(index, element) {
						var elementId = $(element).attr("id");
						if (elementId) {
							TinyMce.removeEditorById(elementId);
						}
					});
				}
			};
			TinyMce.removeEditorById = function(elementId) {   // ID!! Not Selector!!! No #
				if (TinyMce.checkEditorExistenceById(elementId)) {
					tinymce.editors[elementId].remove();
				}
			};
			TinyMce.checkEditorExistenceById = function(elementId) {   // ID!! Not Selector!!! No #
				return ( (tinymce.editors.length > 0) && (tinymce.editors[elementId]) ? true : false);
			};
			';

		// Put it in the HEAD
		$cs->registerScript("docebo.tinymce", $script, CClientScript::POS_HEAD);

		parent::init();
	}

	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$assetsPath = Yii::getPathOfAlias('common.extensions.tinymce.assets');
			if (is_dir($assetsPath))
				$this->_assetsUrl = Yii::app()->getAssetManager()->publish($assetsPath);
			else
				Yii::log('Can not publish tinymce assets', CLogger::LEVEL_ERROR);
		}
		return $this->_assetsUrl;
	}

	/**
	 *
	 * @return string
	 */
	public function getScriptUrl() {
		return $this->getAssetsUrl() . '/tinymce.min.js';
	}

	/**
	 * Check if Route is subject to whitelisting
	 */
	private function isWhitelistedRoute($route=false) {

		if (!$route) {
			$module				= strtolower(Yii::app()->controller->module->id);
			$controller 		= strtolower(Yii::app()->controller->id);
			$action 			= strtolower(Yii::app()->controller->action->id);
			$route = "$controller/$action";
			if ($module) $route = $module . "/" . $route;
		}

		foreach (Yii::app()->params['moxiemanager']['whiteListRoute'] as $pattern) {
			if (preg_match('#'.$pattern.'#i', $route)) {
				return true;
			}
		}

		return false;
	}

}
