<?
/* @var $this TabbedWidget */
?>
<div class="tabbed-widget <?= $this->cssClass ?>">
	<ul class="nav nav-tabs">
		<? $i = 0;
		foreach ( $tabs as $tab ): ?>
			<?php $active = ( ( $this->active_tab === false ? ($i == 0) : ($this->active_tab == $i) ) ? 'active' : '' ) ?>
			<li class="<?= $active ?>">
				<a href="#" data-toggle="tab" data-target=".<?= $tab['slug'] ?>">
					<?= $tab['label'] ?>
				</a>
			</li>
			<? $i ++; ?>
		<? endforeach; ?>
	</ul>
	<div class="tab-content" style="margin-left: 210px;">
		<? $i = 0; ?>
		<? foreach ( $tabs as $tab ): ?>
			<?php $active = ( ( $this->active_tab === false ? ($i == 0) : ($this->active_tab == $i) ) ? 'active' : '' ) ?>
			<div class="tab-pane <?= $active ?> <?= $tab['slug'] ?>">
				<?= $tab['html'] ?>
			</div>
			<? $i ++; ?>
		<? endforeach; ?>
	</div>
</div>