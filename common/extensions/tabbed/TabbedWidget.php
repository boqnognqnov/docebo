<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 * Date: 8.2.2016 г.
 * Time: 17:08 ч.
 */
class TabbedWidget extends CWidget {

	public $tabs = array();
	public $cssClass = '';
	public $active_tab = false;

	public function init () {
		// Register helper CSS
		Yii::app()->clientScript->registerCssFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias( 'common.extensions.tabbed.assets' )) . '/tabbed_widget.css');
	}

	public function run () {
		$tabsFormatted = array();
		foreach ( $this->tabs as $tab ) {
			$label = Yii::app()->htmlpurifier->purify( $tab['label'] );
			$html  = $tab['content'];

			// Create a unique slug to be used as the tab ID
			$slug = strtolower( trim( preg_replace( '/[^A-Za-z0-9-]+/', '-', $label . '-'.md5( microtime(1).Docebo::generateUuid() ) ) ) );

			$tabsFormatted[] = array(
				'label' => $label,
				'html'  => $html,
				'slug'  => $slug,
			);
		}

		$this->render( 'tabbed_widget', array(
			'tabs' => $tabsFormatted
		) );
	}

}