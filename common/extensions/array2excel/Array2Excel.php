<?php

/**
* @author Simeon Ivanov
* @license GPL
* @version 0.2
*/
class Array2Excel
{
	// PHP Excel Path
	public static $phpExcelPathAlias = 'common.extensions.phpexcel.Classes.PHPExcel';

	//the PHPExcel object
	public static $objPHPExcel = null;
	public static $activeSheet = null;

	//Document properties
	public $creator = 'Simeon Ivanov';
	public $title = null;
	public $subject = 'Subject';
	public $description = '';
	public $category = '';

	//config
	public $autoWidth = true;
	public $exportType = 'Excel5';
	public $disablePaging = true;
	public $filename = null; //export FileName
	public $stream = true; //stream to browser

	//data
	public $data = array();
	public $columns = array();
	public $types = array();

	//mime types used for streaming
	public $mimeTypes = array(
		'Excel5' => array(
			'Content-type'=>'application/vnd.ms-excel',
			'extension'=>'xls',
		),
		'Excel2007'	=> array(
			'Content-type'=>'application/vnd.ms-excel',
			'extension'=>'xlsx',
		),
		'PDF' =>array(
			'Content-type' => 'application/pdf',
			'extension'=>'pdf',
		),
		'HTML' =>array(
			'Content-type'=>'text/html',
			'extension'=>'html',
		),
		'CSV' =>array(
			'Content-type'=>'application/csv',
			'extension'=>'csv',
		)
	);

	/**
	 * @param $data array of data rows
	 * @param null $columns array of column names
	 * @param null $types (string or default)
	 */
	public function __construct($data, $columns = null, $types = null)
	{
		$this->data = $data;
		$this->types = $types;
		$this->columns = $columns ? $columns : $this->columns;
		//Autoload fix
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import(self::$phpExcelPathAlias, true);
		self::$objPHPExcel = new PHPExcel();
		self::$activeSheet = self::$objPHPExcel->getActiveSheet();
		spl_autoload_register(array('YiiBase','autoload'));

		// Creating a workbook
		$properties = self::$objPHPExcel->getProperties();
		$properties
		->setTitle($this->title)
		->setCreator($this->creator)
		->setSubject($this->subject)
		->setDescription($this->description)
		->setCategory($this->category);
	}

	/**
	 * Export file
	 */
	public function run()
	{
		$this->renderSheet();

		if($this->autoWidth)
		{
			foreach($this->columns as $n=>$column)
			{
				// echo $this->columnName($n+1) . 1;
				self::$activeSheet->getColumnDimension($this->columnName($n+1))->setAutoSize(true);
			}
		}

		//create writer for saving
		$objWriter = PHPExcel_IOFactory::createWriter(self::$objPHPExcel, $this->exportType);
		if(!$this->stream)
			$objWriter->save($this->filename);
		else //output to browser
		{
			if(!$this->filename)
				$this->filename = $this->title;
			ob_end_clean();
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-type: '.$this->mimeTypes[$this->exportType]['Content-type']);
			header('Content-Disposition: attachment; filename="'.$this->filename.'.'.$this->mimeTypes[$this->exportType]['extension'].'"');
			header('Cache-Control: max-age=0');
			$objWriter->save('php://output');
			Yii::app()->end();
		}
	}

	/**
	 * Render sheet
	 */
	public function renderSheet()
	{
		$rowNum = 1;
		if (count($this->columns))
		{
			foreach ($this->columns as $i => $col)
			{
				self::$activeSheet->setCellValue($this->columnName($i+1) . $rowNum, $col);
				self::$activeSheet->getStyle($this->columnName($i+1) . $rowNum)->getFont()->setBold(true);
			}
			$rowNum++;
		}

		foreach ($this->data as $i => $row)
		{
			foreach ($row as $colNum => $val)
            {
                $cell = $this->columnName($colNum+1) . $rowNum;
                if (isset($this->types[$colNum]) && $this->types[$colNum] == 'string')
                    self::$activeSheet->setCellValueExplicit($cell, $val, PHPExcel_Cell_DataType::TYPE_STRING);
                else
                    self::$activeSheet->setCellValue($cell, $val);
            }

			$rowNum++;
		}
	}

	public function columnName($c)
	{
		$c = intval($c);
		if ($c <= 0) return '';
		$letter = '';
		while($c != 0){
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}
		return $letter;
	}


}