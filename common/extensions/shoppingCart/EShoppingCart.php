<?php
/**
 * Shopping cart class
 *
 * @author Plamen Petkov
 *
 * Based on https://github.com/yiiext/shopping-cart-component.
 * @author pirrat <mrakobesov@gmail.com>
 * 
 */
class EShoppingCart extends CMap {

    /**
     * Update the model on session restore?
     * @var boolean
     */
    public $refresh = true;
	public $cartId = __CLASS__;
	public $currencyCode = "EUR";

    /**
     * @var string The currently applied coupon
     */
    private $coupon;

    /**
     * @var integer The id of the transaction which this cart comes from
     */
    private $transactionId;

	public $currencyHtmlSymbols = array(
	    "eur" => "&euro;",
	    "usd" => "&dollar;",
	);
	
    /**
     * Cart-wide discount sum
     * @var float
     */
    protected $discountPrice = 0.0;

    /**
     * Restores the cart from either the session or the transaction
     */
    public function init(){
	    // Set shopping cart currency, using LMS wide settings
	    $currency = Settings::get("currency_code", false);
	    if(!$currency) {
		    $currency = Settings::get("paypal_currency", '');
		    Settings::save("currency_code", $currency);
	    }
	    if(PluginManager::isPluginActive('EcommerceApp'))
		    $this->setCurrency($currency);

        // Get cart from the user session
        $data = unserialize(Yii::app()->getUser()->getState($this->cartId));
        if (is_array($data) || $data instanceof Traversable) {
            if(!empty($data)) {
                // Something in the session -> restore it
                $this->coupon = Yii::app()->getUser()->getState('coupon_code');
                $this->transactionId = Yii::app()->getUser()->getState('transaction_id');
                foreach ($data as $key => $product)
                    parent::add($key, $product);
            } else
                $this->restoreFromLastUnpaidTransaction();
        } else
            $this->restoreFromLastUnpaidTransaction();
    }

    /**
     * Recreates a cart from the last unpaid (and not abandoned) transaction
     */
    protected function restoreFromLastUnpaidTransaction() {
        if(!Yii::app()->user->getIsGuest()) {
            $criteria = new CDbCriteria();
            $criteria->addCondition("id_user = :id_user");
            $criteria->params = array(
                ':id_user' => Yii::app()->user->idst
            );
            $criteria->order = 'date_creation DESC';

            /* @var $transaction EcommerceTransaction */
            $transaction = EcommerceTransaction::model()->find($criteria);
            if($transaction && !$transaction->paid && !$transaction->abandoned) {
                // Restore all items
                /* @var $items EcommerceTransactionInfo[] */
                $items = $transaction->transaction_items;
                if(!empty($items)) {
                    $this->setTransaction($transaction->id_trans);

                    foreach($items as $itemFromDb) {
                        $restoredCartPosition = false;
                        /* @var $restoredCartPosition IECartPosition */
						if($itemFromDb->item_type == EcommerceTransactionInfo::TYPE_COURSE)
                        	$restoredCartPosition = CourseCartPosition::model()->findByPk($itemFromDb->id_course);
						else if (PluginManager::isPluginActive('CurriculaApp') && ($itemFromDb->item_type == EcommerceTransactionInfo::TYPE_COURSEPATH)) {
							$lp = CoursepathCartPosition::model()->findByPk($itemFromDb->id_path);
							$allCoursesInLp = $lp->learningCourse;
							if(!empty($allCoursesInLp)) {
								if($lp->purchase_type == LearningCoursepath::PURCHASE_TYPE_FULL_PACK_ONLY) {
									$lp->setCourses($lp->learningCourse);
									$restoredCartPosition = $lp;
								} elseif($lp->purchase_type == LearningCoursepath::PURCHASE_TYPE_SINGLE_COURSES_IN_SEQUENCE) {
									$itemData = json_decode($itemFromDb->item_data_json);
									$selectedCourseIds = isset($itemData->courses) ? $itemData->courses : array();
									if(!empty($selectedCourseIds)) {
										$selectedCoursesToBuy = array();
										foreach($allCoursesInLp as $lpCourse) {
											if(in_array($lpCourse->idCourse, $selectedCourseIds))
												$selectedCoursesToBuy[] = $lpCourse;
										}

										$lp->setCourses($selectedCoursesToBuy);
										$restoredCartPosition = $lp;
									}
								}
							}
						}elseif($itemFromDb->item_type == EcommerceTransactionInfo::TYPE_COURSESEATS){
                            $seatModel = CourseseatsCartPosition::model()->findByPk($itemFromDb->id_course);
                            $itemData = json_decode($itemFromDb->item_data_json, false);
                            if(isset($itemData->seats)){
                                $seatModel->setSeats($itemData->seats);
                            }
                            $restoredCartPosition = $seatModel;
                        }

						if(!$restoredCartPosition)
							continue;

                        if($itemFromDb->id_date && $itemFromDb->id_course) {
							$course = LearningCourse::model()->findByPk($itemFromDb->id_course);
							if ($course) {
								if ($course->course_type == LearningCourse::TYPE_CLASSROOM)
									$session = LtCourseSession::model()->findByPk($itemFromDb->id_date);
								else
									$session = WebinarSession::model()->findByPk($itemFromDb->id_date);

								if ($session)
									$restoredCartPosition->setSession($session);
							}
                        }
                        $this->put($restoredCartPosition, 1);
                    }

                    if($transaction->ecommerceCoupon)
                        $this->setCoupon($transaction->ecommerceCoupon->code);
                }
            }
        }
    }

    /**
     * Add item to the shopping cart
     * If the position was previously added to the cart,
     * then information about it is updated, and count increases by $quantity
     * @param IECartPosition $position
     * @param int count of elements positions
     */
    public function put(IECartPosition $position, $quantity = 1) {
        $key = $position->getCartItemId();
        if ($this->itemAt($key) instanceof IECartPosition) {
            $position = $this->itemAt($key);
            $oldQuantity = $position->getQuantity();
            $quantity += $oldQuantity;
        }

        $this->update($position, $quantity);

    }


    /**
     * Add $value items to position with $key specified
     * @return void
     * @param mixed $key
     * @param mixed $value
     */
    public function add($key, $value) {
        $this->put($value, 1);
    }

    /**
     * Removes position from the shopping cart of key
     * @param mixed $key
     */
    public function remove($key) {
        parent::remove($key);
        $this->onRemovePosition(new CEvent($this));
        $this->saveState();

        Yii::app()->event->raise('OnAfterRemoveItemFromCart', new DEvent($this,array()));
    }

    /**
     * Updates the position in the shopping cart
     * If position was previously added, then it will be updated in shopping cart,
     * if position was not previously in the cart, it will be added there.
     * If count is less than 1, the position will be deleted.
     *
     * @param IECartPosition $position
     * @param int $quantity
     */
    public function update(IECartPosition $position, $quantity) {
        if (!($position instanceof CComponent))
            throw new InvalidArgumentException('invalid argument 1, product must implement CComponent interface');

        $key = $position->getCartItemId();
		
		$position->detachBehavior("CartPosition");
        $position->attachBehavior("CartPosition", new ECartPositionBehaviour());
        $position->setRefresh($this->refresh);

        $position->setQuantity($quantity);

        if ($position->getQuantity() < 1)
            $this->remove($key);
        else
            parent::add($key, $position);

        $this->onUpdatePosition(new CEvent($this));
        $this->saveState();
    }

    /**
     * Saves the state of the object in the session.
     * @return void
     */
    protected function saveState() {
        Yii::app()->getUser()->setState($this->cartId, serialize($this->toArray()));
        Yii::app()->getUser()->setState('coupon_code', $this->coupon);
        Yii::app()->getUser()->setState('transaction_id', $this->transactionId);
    }

    /**
     * Returns count of items in shopping cart
     * @return int
     */
    public function getItemsCount() {
        $count = 0;
        foreach ($this as $position) {
            $count += $position->getQuantity();
        }

        return $count;
    }


    /**
     * Returns total price for all items in the shopping cart.
     * @param bool $withDiscount
     * @return float
     */
    public function getCost($withDiscount = true) {
        $price = 0.0;
        foreach ($this as $position)
        {
            $price += $position->getSumPrice($withDiscount);
        }

        if($withDiscount)
            $price -= $this->discountPrice;

        return $price;
    }

    /**
     * onRemovePosition event
     * @param  $event
     * @return void
     */
    public function onRemovePosition($event) {
        $this->raiseEvent('onRemovePosition', $event);
    }

    /**
     * onUpdatePoistion event
     * @param  $event
     * @return void
     */
    public function onUpdatePosition($event) {
        $this->raiseEvent('onUpdatePosition', $event);
    }

    /**
     * Set cart-wide discount sum
     *
     * @param float $price
     * @return void
     */
    public function setDiscountPrice($price){
        $this->discountPrice = $price;
    }

    /**
     * Add $price to cart-wide discount sum
     *
     * @param float $price
     * @return void
     */
    public function addDiscountPrice($price){
        $this->discountPrice += $price;
    }

    /**
     * Returns array of all positions
     *
     * @return IECartPosition[]|ECartPositionBehaviour[] Classes extending from IECartPosition that have the ECartPositionBehaviour behavior attached
     */
    public function getPositions()
    {
        return $this->toArray();
    }

    /**
     * Returns if cart is empty
     * @return bool
     */
    public function isEmpty()
    {
        return !(bool)$this->getCount();
    }


    /**
     * Remove all positions from the cart
     */
    public function clear() {
        foreach ( $this->getPositions() as $position ) {
            $this->remove($position->getCartItemId());
        }

        // Reset the current coupon
        $this->setCoupon(null);

        // Reset the current transaction id
        if($this->transactionId)
            $this->setTransaction(null);

	    $this->saveState();
    }

    /**
     * Saves the transaction id in session
     * @param $transactionId
     */
    protected function setTransaction($transactionId) {
        $this->transactionId = $transactionId;
        $this->saveState();
    }

    /**
     * Sets the transaction as abandoned
     */
    public function abandonCart() {
        if($this->transactionId)  {
            $model = EcommerceTransaction::model()->findByPk($this->transactionId);
            if($model) {
                $model->abandoned = 1;
                $model->save();
            }

            $this->transactionId = null;
            $this->saveState();
        }
    }
    
    
    /**
     * Set shopping cart currency code (EUR, USD, etc.)
     * 
     * @param string $currency
     */
    public function setCurrency($currency) {
        $this->currencyCode = $currency;
    }
    
    /**
     * Return Currency used in the cart 
     * 
     * @return string
     */
    public function getCurrency() {
        return $this->currencyCode;
    }
    

    /**
     * Return HTML currency symbol: &euro; &dollar; etc.
     * 
     * @return string
     */
    public function getCurrencyHtmlSymbol() {
        if ( isset($this->currencyHtmlSymbols[strtolower($this->currencyCode)]) ) {
            return $this->currencyHtmlSymbols[strtolower($this->currencyCode)];
        }
        else {
			$paypalCurrencies = LocaleManager::getCurrencyArr(true);
			if(isset($paypalCurrencies[strtoupper($this->currencyCode)])){
				return $paypalCurrencies[strtoupper($this->currencyCode)];
			}
        }
		
		return "???";      // unknown currency code?; there is something buggy; this will prevent from payment to proceed!
    }

    /**
     * Retrieves the currently applied coupon for this cart
     * @return string
     */
    public function getCoupon() {
        return $this->coupon;
    }

    /**
     * Saves a coupon in this cart
     * @param $coupon
     */
    public function setCoupon($coupon) {
        $this->coupon = $coupon;
        $this->saveState();
    }

}
