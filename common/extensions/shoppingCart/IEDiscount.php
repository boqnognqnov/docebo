<?php
/**
 * Discount abstract class
 *
 * (needs improvement)
 * 
 * @author Plamen Petkov
 *
 * Based on https://github.com/yiiext/shopping-cart-component.
 * @author pirrat <mrakobesov@gmail.com>
 * 
 */
abstract class IEDiscount {

    protected $shoppingCart;

    public function setShoppingCart(EShoppingCart $shoppingCart) {
        $this->shoppingCart = $shoppingCart;
    }

    /**
     * Apply discount
     *
     * @abstract
     * @return void
     */
    abstract public function apply();

}
