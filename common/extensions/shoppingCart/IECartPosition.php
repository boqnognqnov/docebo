<?php
/**
 * Interface; methods implemented by CActiveRecord child model
 *
 * @author Plamen Petkov
 *
 * Based on https://github.com/yiiext/shopping-cart-component.
 * @author pirrat <mrakobesov@gmail.com>
 * 
 */
interface IECartPosition {
    /**
     * @return mixed id
     */
    public function getCartItemId();
    /**
     * @return float price
     */
    public function getPrice();

    /**
     * Required by plugins. DO NOT REMOVE
     * @param float $price
     */
    public function setPrice($price);

	public function getPositionName();
	public function getImage();
    
    
}
