<?php
/**
 * Created by PhpStorm.
 * User: Dzhuneyt
 * Date: 25.9.2014 г.
 * Time: 16:49 ч.
 */
/*
 * A simple theme companion specific to Docebo that helps us combine
 * and publish assets, taken relative to the theme folder and either published
 * as Yii assets or served directly from their new location (web accessible)
 */

class DoceboAssetMerger {

	const LOG_DEBUG_TAG = '[DoceboAssetMerger] ';

	static public $TARGET_SAME_FOLDER = "_same";
	static public $TARGET_THEME_ASSETS_FOLDER = "_theme_assets";
	static public $TARGET_ASSETS_FOLDER = "_assets";

	/**
	 * @see publish() - This method will simply echo the result of that method
	 * if a result is returned, otherwise echoes the original array
	 *
	 * Some examples using the 3 available $targetFolder param values:
	 *
	 * == Example 1 (DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER): ==
	 * DoceboAssetMerger::publishAndRender(array('/css/admin.css', '/css/base.css'), false, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);
	 * will result in those two files being combined
	 * and placed in /themes/spt/assets/merged_XXXXXXXX.css
	 * This way relative file associations like background:url(../images/bla.png)
	 * in the merged CSS files will not be broken ==
	 *
	 * == Example 2 (DoceboAssetMerger::$TARGET_SAME_FOLDER): ==
	 * DoceboAssetMerger::publishAndRender(array('/randomPath/file1.css', '/randomPath/file2.css'), false, DoceboAssetMerger::$TARGET_SAME_FOLDER);
	 * will result in those two files being combined and placed in
	 * /randomPath/merged_XXXXXXXXX.css
	 * Note that all passed files here must originate from the same path,
	 * otherwise an error will be thrown ==
	 *
	 * == Example 3 (DoceboAssetMerger::$TARGET_ASSETS_FOLDER): ==
	 * DoceboAssetMerger::publishAndRender(array('/js/formstyler/jquery.formstyler.css', '/js/fancybox/jquery.fancybox.css'), false, DoceboAssetMerger::$TARGET_ASSETS_FOLDER);
	 * will result in those two files being combined and placed
	 * in the general Yii assets folder (/lms/assets/XXXX/XXXXXX.css)
	 * Note that this "target" is only recommended for standalone and independant
	 * CSS files that do not contain references to relative resources,
	 * e.g. background:url(../images/bla.png) ==
	 *
	 * @param      $itemsRelativeToTheme $items Array of files, relative
	 *              to the theme dir that should be merged, e.g.
	 *              array('/css/i-sprite.css', '/css/player-sprite.css')
	 *              The order of this array will be the order of merging
	 * @param bool $trim - Should we remove extra whitespaces in the merged result file
	 * @param null $targetFolder See the constants in this class for valid values and read the examples in the PHPDoc above
	 */
	public static function publishAndRender(array $itemsRelativeToTheme, $trim = FALSE, $targetFolder = NULL){
		if ($targetFolder == NULL)
			$targetFolder = self::$TARGET_THEME_ASSETS_FOLDER;

		$mergedAndPublishedAsset = self::publish($itemsRelativeToTheme, $trim, $targetFolder);

		if(!$mergedAndPublishedAsset){
			// Something failed, just render the original assets as they are
			foreach($itemsRelativeToTheme as $item){
				echo '<link href="'.Yii::app()->theme->baseUrl.$item.'" rel="stylesheet">';
			}
		}else{
			if(defined('YII_DEBUG') && YII_DEBUG)
				echo '<!-- Merged: '.implode(', ', $itemsRelativeToTheme).' -->';

			if(is_array($mergedAndPublishedAsset)){
				foreach($mergedAndPublishedAsset as $singleAsset){
					echo '<link href="'.$singleAsset.'" rel="stylesheet">';
				}
			}elseif(is_string($mergedAndPublishedAsset)){
				echo '<link href="'.$mergedAndPublishedAsset.'" rel="stylesheet">';
			}else{
				Yii::log(self::LOG_DEBUG_TAG.'Error with merging', CLogger::LEVEL_WARNING);
			}
		}
	}

	/**	 *
	 * A wrapper method that takes an array of paths to resources/files, relative
	 * to the theme directory, merges them in a local file and (in some scenarios)
	 * publishes them as Yii asset and serves the final public URLs
	 *
	 * The order of merging depends on the order of entries in the passed array
	 *
	 * @see publishAndRender() PHPDoc for examples of usage
	 *
	 * @param array $items Array of files, relative to the theme dir
	 *              that should be merged, e.g.
	 *              array('/css/i-sprite.css', '/css/player-sprite.css')
	 *              The order of this array will be the order of merging
	 *
	 * @param bool  $trim If whitespaces should be stripped in the merged file
	 *              Newline characters are always preserved
	 *
	 * @param null  $targetFolder See the constants in this class for valid values
	 *
	 * @internal param bool $combineInOriginalFolder
	 *
	 * @return string The URL to the published single merged asset file (a Yii published
	 *          asset or a direct serve of the merged file from the theme directory)
	 */
	private static function publish($items = NULL, $trim = FALSE, $targetFolder = NULL) {
		// Combine and publish files if needed (timestamp of one of them
		// is newer than timestamp of final asset)

		if ($targetFolder == NULL)
			$targetFolder = self::$TARGET_THEME_ASSETS_FOLDER;

		return self::_combineIfNeededAndGetUrl($items, $trim, $targetFolder);
	}

	/**
	 * Take an array of files, relative to the theme folder and
	 * merge them in the folder /assets/ in the current extension dir,
	 * but only if
	 * (1) any of these files in the array is newer than the already merged
	 *      copy we have in the /assets dir
	 * (2) the merged file in the /assets/ dir doesn't exist yet
	 *
	 * @param      $load - Array of files to merge, relative to the theme dir
	 * @param bool $trim - Remove whitespaces from the merged file (newlines are always preserved)
	 *
	 * @param      $targetFolder
	 *
	 * @throws CException
	 * @return string URL to the published final asset, ready for display to the user
	 */
	private static function _combineIfNeededAndGetUrl($load, $trim = FALSE, $targetFolder) {
		if (!$load || empty($load))
			return FALSE;

		if (is_string($load))
			$load = array($load);

		// Try to find the end result file's extension
		list($firstToLoad) = $load;
		$firstToLoadExt = explode('.', $firstToLoad);
		$ext            = !empty($firstToLoadExt) ? end($firstToLoadExt) : '.css';

		// Generate a unique filename based on the passed array of files
		// This file will be the local merged result
		$mergedCssHashedName = 'merged_' . md5(implode(',', $load)) . ($ext ? '.' . $ext : NULL);

		// Path where the final merged file will go
		$saveMergeInPath = NULL;

		switch ($targetFolder) {
			case self::$TARGET_SAME_FOLDER:
				// The merged file will be saved in the same folder as the original
				// files. In this case, the passed array of files NEED to originate
				// from the same folder. The below will check for this and throw error if needed

				// Iterate the array of files check if they are all located
				// in the same folder (in relation to one another). If not, throw
				// an error since the target is set to "same as origin", but we have
				// multiple origins here!!!
				$pathFromLastFile = NULL;
				foreach ($load as $fileToLoad) {
					$realPathCurrentFile = dirname(realpath(Yii::app()->theme->basePath . $fileToLoad));

					if ($pathFromLastFile != NULL /* <--(first array iteration) */ && $realPathCurrentFile != $pathFromLastFile) {
						Yii::log(self::LOG_DEBUG_TAG."Trying to merge files from different folders but the target merge folder is set as same as origin. We don't know which folder to choose as target then", CLogger::LEVEL_ERROR);

						return FALSE;
					} else {
						$pathFromLastFile = $realPathCurrentFile;
					}

				}

				if (!$pathFromLastFile) {
					Yii::log(self::LOG_DEBUG_TAG."Can not determinate target folder for merged file", CLogger::LEVEL_ERROR);

					return FALSE;
				}

				$saveMergeInPath = $pathFromLastFile;
				break;
			case self::$TARGET_ASSETS_FOLDER:
				// Continue as usual

				// Save the merged file in this extension's specific asset folder and later
				// publish it using Yii's AssetManager to the regular assets directory
				if (!is_dir(Yii::app()->assetManager->getBasePath() . DIRECTORY_SEPARATOR . 'doceboAssetMerger' . DIRECTORY_SEPARATOR . 'assets')) {
					mkdir(Yii::app()->assetManager->getBasePath() . DIRECTORY_SEPARATOR . 'doceboAssetMerger' . DIRECTORY_SEPARATOR . 'assets', 0777, TRUE);
				}
				$saveMergeInPath = Yii::app()->assetManager->getBasePath() . DIRECTORY_SEPARATOR . 'doceboAssetMerger' . DIRECTORY_SEPARATOR . 'assets';
				break;
			case self::$TARGET_THEME_ASSETS_FOLDER:
				// Write the merged file under [theme_dir]/assets/
				// and return a URL to the file there. No Yii asset publishing here

				if (!is_dir(Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . 'assets')) {
					mkdir(Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . 'assets', 0777, TRUE);
				}

				$saveMergeInPath = Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . 'assets';
				break;
		}

		$localMergedFile = $saveMergeInPath . DIRECTORY_SEPARATOR . $mergedCssHashedName;

		$regenerate = FALSE;
		if (is_file($localMergedFile)) { // We have already merged this file before

			// Check if one of the original files has a newer timestamp
			// than the merged file we have and if needed regenerate it

			foreach ($load as $fileToLoadRelativeToTheme) {
				$singleFilePath = Yii::app()->theme->basePath . $fileToLoadRelativeToTheme;
				if (is_file($singleFilePath)) {
					if (filemtime($singleFilePath) > filemtime($localMergedFile)) {
						// At least one of the files in the array to be merged
						// is newer than the local merged asset so mark the asset
						// for regeneration
						$regenerate = TRUE;
						break;
					}
				} else {
					Yii::log(self::LOG_DEBUG_TAG . "Can not retrieve original file requested for merging: " . $singleFilePath . '; Skipping...', CLogger::LEVEL_WARNING);
				}
			}
		} else {
			// Local merged cache file not found. Generate and publish it
			$regenerate = TRUE;
		}

		if ($regenerate) {
			// Create a local cached merged file

			if ($mergedFinalResultString = self::_getContentsOfFilesToMerge($load, $trim)) {

				// DEBUGGING:
				if($targetFolder == self::$TARGET_ASSETS_FOLDER) {
					if (defined('YII_DEBUG') && YII_DEBUG) {
						// For debugging purposes, check if the final file (CSS) may contain
						// references to relative resources (e.g. images) but the target
						// directory will move the CSS somewhere else, essentially breaking
						// the relative path relation to those. This is only intended to warn
						// developers who accidentally try to use self::$TARGET_ASSETS
						// on CSS assets that are intended to stay in their original directory
						// Note that this will impact performance a little bit that's why
						// it is only enabled with YII_DEBUG
						if (strpos($mergedFinalResultString, 'url(".') !== FALSE || strpos($mergedFinalResultString, "url('.") !== FALSE
							|| strpos($mergedFinalResultString, "url(.") !== FALSE || strpos($mergedFinalResultString, "url(/") !== FALSE
						) {
							Yii::log(self::LOG_DEBUG_TAG . "Possible CSS+images relative path breaking. Do not merge CSS files that contain paths to relative images inside or use TARGET_THEME_ASSETS constant", CLogger::LEVEL_WARNING);
						}
					}
				}

				// Put the merged file in the target folder (different path depending on $targetFolder param)
				if (!file_put_contents($localMergedFile, $mergedFinalResultString)) {
					Yii::log(self::LOG_DEBUG_TAG . "Can not write to local merged file " . $localMergedFile, CLogger::LEVEL_ERROR);

					return FALSE;
				}
			} else {
				Yii::log(self::LOG_DEBUG_TAG . "No content retrieved from those files for merging: " . implode(', ', $load), CLogger::LEVEL_WARNING);

				return FALSE;
			}
		}

		$forcePublish = (isset(Yii::app()->params['forceCopyAssets']) && Yii::app()->params['forceCopyAssets'] ? (bool) Yii::app()->params['forceCopyAssets'] : NULL);

		if (!is_file($localMergedFile)) return FALSE;

		// Return the URL to the merged file
		// Depending on $targetFolder this may be a local file
		// under the theme folder or a Yii published asset URL
		switch ($targetFolder) {
			case self::$TARGET_ASSETS_FOLDER:
				return Yii::app()->assetManager->publish($localMergedFile, FALSE, -1, $forcePublish);
				break;
			case self::$TARGET_SAME_FOLDER:
				$relativePathToMergedFile = str_ireplace(Yii::app()->theme->basePath, '', $localMergedFile);
				$absolutePathToMergedFile = Yii::app()->theme->baseUrl . str_ireplace('\\', '/', $relativePathToMergedFile);

				return $absolutePathToMergedFile;
				break;
			case self::$TARGET_THEME_ASSETS_FOLDER:
				return Yii::app()->theme->baseUrl . '/assets/' . $mergedCssHashedName;
				break;
			default:
				throw new CException(self::LOG_DEBUG_TAG."Invalid target specified. Use one of the predefined constants");
		}
	}

	private static function _getContentsOfFilesToMerge(array $load, $trim) {
		if (!$load || empty($load)) return NULL;

		$mergedFinalResultString = '/* Docebo Asset Merger' . PHP_EOL;
		$mergedFinalResultString .= '   Combined at: ' . date('Y-m-d H:i:s') . PHP_EOL;
		$mergedFinalResultString .= '   Merged files: ' . implode(', ', $load) . ' */' . PHP_EOL . PHP_EOL . PHP_EOL;

		foreach ($load as $fileToLoadRelativeToTheme) {
			$singleFilePath = Yii::app()->theme->basePath . DIRECTORY_SEPARATOR . $fileToLoadRelativeToTheme;
			if (is_file($singleFilePath)) {
				$capturedContents = file_get_contents($singleFilePath);
				if ($trim) {
					// Exclude all whitespaces except newline character
					$capturedContents = preg_replace("/[^\S\r\n]/", " ", $capturedContents);
				}
				$mergedFinalResultString .= PHP_EOL . PHP_EOL . '/* Contents from file: ' . $fileToLoadRelativeToTheme . ' */' . PHP_EOL . PHP_EOL . $capturedContents;
			}
		}

		return $mergedFinalResultString;
	}

} 