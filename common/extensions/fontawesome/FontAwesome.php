<?php
class FontAwesome extends CApplicationComponent
{
    protected $assetsUrl;
    
    public function init() {
        if(Yii::getPathOfAlias('fontawesome') === false) Yii::setPathOfAlias('fontawesome', realpath(dirname(__FILE__)));
        
        if (!Yii::app() instanceof CConsoleApplication && !Yii::app()->request->isAjaxRequest) 
        	Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl().'/css/font-awesome.min.css');
        parent::init();
    }
    
    public function getAssetsUrl()
    {
        if(!isset($this->assetsUrl)) {
            $this->assetsUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('fontawesome.assets'));
        }
       	return $this->assetsUrl;
       	
    }

	/**
	 * Loads the fontawesome icon picker
	 */
	public function loadIconPicker() {
		Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl().'/css/bootstrap-iconpicker.min.css');
		Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl().'/js/iconset/iconset-fontawesome-4.4.0.min.js');
		Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl().'/js/bootstrap-iconpicker.js');
	}
}
