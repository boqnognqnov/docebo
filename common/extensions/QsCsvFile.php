<?php

class QsCsvFile extends CComponent {

	public $numLines = false;
	public $path;
	public $delimiter = ',';
	public $charset = 'UTF-8';
	public $firstRowAsHeader = true;

	protected $data;

	public function __construct($params = array()) {
		ini_set("auto_detect_line_endings", true);
		foreach ($params as $key => $value) {
			if (property_exists(get_class($this), $key)) {
				$this->$key = $value;
			}
		}
	}

	/**
	 * Load CSV data. The number of value lines (non-header lines) depends on object property numLines.
	 *
	 * @param integer $numLines Forcefully get only that number of value lines (i.e. excluding header, if any)
	 * @return array
	 */
	public function getData() {

		if (!is_array($this->data)) {
			if (!file_exists($this->path)) {
				return false;
			}

			$content = '';
			if ($this->numLines === false) {
				$content = file_get_contents($this->path);
			} else if (!intval($this->numLines) > 0) {
				return false;
			} else {
				$count = 0;
				$fh = @fopen($this->path,'r');
				$limit = $this->firstRowAsHeader ? $this->numLines+1 : $this->numLines;
				while(!feof($fh) && ($count < $limit)) {
					$line = fgets($fh);
					$count++;
					$content .= $line;
				}
				fclose($fh);
			}

			if (trim($content) == '') {
				return false;
			}

			// Convert content to UTF-8
			if($this->charset != 'UTF-8')
			{
				$tmp = @iconv($this->charset, 'UTF-8//TRANSLIT', $content);

				if($tmp == false)
				{
					$original_encoding = mb_detect_encoding($content);
					if($original_encoding != 'UTF-8' && $original_encoding != false){
						$tmp = @iconv($original_encoding, 'UTF-8//TRANSLIT', $content);
						if($tmp == false)
							throw new Exception('Test message');
						$content = $tmp;
						unset($tmp);
					}
				}
			}

			// As long as $content is UTF-8, the following functions are "safe"
			$content = str_replace(array("\r\n", "\n"),"\r", $content);
			$content = str_replace("\r", "\r\n", $content);
			$content = explode("\r\n", $content);

			$this->data = $content;

		}
		return $this->data;
	}


	/**
	 * Return CSV data (full or limited to number of lines, see the class property numLines), as an array of arrays
	 *
	 * @return array
	 */
	public function getArray() {
		$data = array();

		if ($this->getData() == false) {
			return false;
		}

		$this->resolveDelimiter($this->delimiter);

		foreach ($this->getData() as $i => $line) {
			$tempData = str_getcsv($line, $this->delimiter);
 			foreach ($tempData as $i => $value) {
 				$tempData[$i] = html_entity_decode($value, ENT_NOQUOTES);
 			}
			$data[] = $tempData;
		}
		return $data;
	}


	/**
	 * Resolve CSV delimiter
	 *
	 * @param string $delimiter
	 */
	protected function resolveDelimiter($delimiter) {
		if ($delimiter === 'auto') {
			$data = $this->getData();
			if (substr_count($data[0], ',') > substr_count($data[0], ';')) {
				$this->delimiter = ',';
			} else {
				$this->delimiter = ';';
			}
		}
	}



	/**
	 * Count number of value lines in the CSV file.
	 * Excludes header!
	 *
	 * @return number
	 */
	public function getTotalLines() {

		$linecount = 0;
		$fh = @fopen($this->path,'r');
		while(!feof($fh)){
			$line = fgets($fh);
			$linecount++;
		}
		fclose($fh);
		if ($this->firstRowAsHeader && ($linecount > 0)) $linecount--;
		return $linecount;

	}


	/**
	 * Read portion of a CSV file and return array of data
	 *
	 * @param string $limit
	 * @param string $offset
	 */
	public function getChunk($limit, $offset, $skipHeader=false) {

		$this->resolveDelimiter($this->delimiter);

		$file = new SplFileObject($this->path);

		if ($skipHeader && $this->firstRowAsHeader) $offset++;

		$file->seek($offset);

		$data = array();

		$i = 0;
		while (!$file->eof() && $i < $limit) {
			$line = $file->current();
			$line = trim($line);
			$tmp = str_getcsv($line, $this->delimiter, '"');
			$data[] = $tmp;
			$file->next();
			$i++;
		}

		return $data;
	}

	/**
	 * Read portion of a CSV file and return array of data, but avoiding empty line
	 *
	 * @param string $limit
	 * @param string $offset NOTE: offset does not exclude empty lines prior to its index
	 */
	public function getChunkNoEmptyLines($limit, $offset, $skipHeader=false) {

		$this->resolveDelimiter($this->delimiter);

		$file = new SplFileObject($this->path);
		$file->setFlags(SplFileObject::READ_CSV | SplFileObject::READ_AHEAD);
		$file->setCsvControl($this->delimiter, '"');

		if ($skipHeader && $this->firstRowAsHeader) $offset++;

		$file->seek($offset);

		$data = array();
		$emptyLines = 0;
		$i = 0;

		while ($file->valid() && $i < $limit) {
			$line = $file->current();
			if (count($line) == 1 && empty($line[0])) {
				$emptyLines++;
				$file->next();
				continue;
			}
			$data[] = $line;
			$file->next();
			$i++;
		}

		return array(
			'data' => $data,
			'emptyLines' => $emptyLines
		);
	}
}