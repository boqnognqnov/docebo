<?php
    $_id = 'dialog2_jplayer_'.time();
?>

<div id="jp_container_<?= $_id ?>" class="jp-video">

	<div id="jp-big-play" class="player-launchpad-big-blue-play"></div>

	<div class="jp-type-single">
		<div id="<?= $_id ?>" class="jp-jplayer"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<a class="jp-video-play-icon" tabindex="1"></a>
			</div>
			<div class="jp-interface">
				<a class="jp-play" tabindex="1"></a> <a class="jp-pause"
					tabindex="1"></a>
				<div class="jp-volume">
					<a href="javascript:;" class="jp-mute" tabindex="1" title="Volume"></a>
					<div class="jp-volume-box">
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value">
								<span class="handle"></span>
							</div>
						</div>
					</div>
				</div>


				<div class="jp-controls-holder">

					<div class="jp-volume-bar">
						<div class="jp-volume-bar-value">
							<span class="handle"></span>
						</div>
					</div>
					<ul class="jp-toggles">
						<li></li>
						<li><a class="jp-restore-screen" tabindex="1"
							title="restore screen"></a>
						</li>
						<li><a class="jp-repeat" tabindex="1" title="repeat"></a>
						</li>
						<li><a class="jp-repeat-off" tabindex="1" title="repeat off"></a>
						</li>
					</ul>
				</div>
				<div class="jp-progress-box">

					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>

					</div>
					<div class="jp-time">
						<!--time notifications-->
						<div class="jp-current-time"></div>
						<span class="time-sep">/</span>
						<div class="jp-duration"></div>
						<span class="separator sep-2"></span>
					</div>

				</div>

				<a class="jp-full-screen" tabindex="1" title="full screen"></a> <a
					href="javascript:;" class="jp-restore-screen" tabindex="1"
					title="full screen"></a>
			</div>
		</div>

		<div class="jp-no-solution">
			<span>Update Required</span> To play the media you will need to
			either update your browser to a recent version or update your <a
				href="http://get.adobe.com/flashplayer/" target="_blank">Flash
				plugin</a>.
		</div>
	</div>
</div>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
/*<![CDATA[*/

    //$(document).delegate(".modal", "dialog2.content-update", function() {
    $(document).ready(function() {

        var mediaUrl = '<?= $mediaUrl ?>';

        var $jPlayerContainer = $('#jp_container_<?= $_id ?>');
        var $jPlayButton = $jPlayerContainer.find('#jp-big-play');
        var $jPlayer = $('#<?= $_id ?>');

        // Create Instance of jPlayer
        $jPlayer.jPlayer({
            cssSelectorAncestor: '#jp_container_<?= $_id ?>',
            ready: function () {
                $(this).jPlayer("setMedia", {
                    m4v: mediaUrl
                });
                $(this).jPlayer("playHead");
				<?=($autoplay === true ? '$(this).jPlayer("play");' : '')?>
            },
            play: function(){
                $jPlayButton.hide();
            },
            //Comment this 3 rows for remove respawn of the big blue button when video is paused
            pause: function(){
                $jPlayButton.show();
            },
            swfPath: '<?= $this->getAssetsUrl() . '/js' ?>',
            supplied: "m4v",
            solution: "html,flash",
            size: {
                height: '100%',
                width: '100%',
                cssClass: "jp-video-extended"
            },
            verticalVolume: true
        });
        $jPlayerContainer.find('.jp-volume-box').css("display","none");
        $jPlayerContainer.find('.jp-volume').hover(
            function () {
                $jPlayerContainer.find('.jp-mute').addClass('hover');
                $jPlayerContainer.find('.jp-volume-box').css("display","block");
            },
            function () {
                $jPlayerContainer.find('.jp-mute').removeClass('hover');
                $jPlayerContainer.find('.jp-volume-box').css("display","none");
            }
        );
        $jPlayButton.on('click', function(){
            $jPlayer.jPlayer("play");
        });

    });


/*]]>*/
</script>
