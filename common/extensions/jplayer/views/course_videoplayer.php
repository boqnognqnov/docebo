<?php
/* @var $videoModel LearningVideo */
?>
<div class="player-launchpad-header">
	<h2><?= $title ?></h2>
	<a id="player-inline-close" class="player-close" href="#"><?= Yii::t('standard', '_CLOSE') ?> </a>
</div>



<div id="jp_container_<?= $videoId ?>" class="jp-video">

	<div id="jp-big-play" class="player-launchpad-big-blue-play"></div>

	<div class="jp-type-single">
		<div id="jquery_jplayer" class="jp-jplayer"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<a class="jp-video-play-icon" tabindex="1"></a>
			</div>
			<div class="jp-interface">
				<a class="jp-play" tabindex="1"></a> <a class="jp-pause"
					tabindex="1"></a>
				<span id="volume-container">
					<a href="javascript:;" class="jp-mute" tabindex="1" title="Volume"></a>
					<div class="jp-volume">
						<div class="jp-volume-box">
							<div class="jp-volume-bar">
								<div class="jp-volume-bar-value">
									<span class="handle"></span>
								</div>
							</div>
						</div>
					</div>
				</span>

				<div class="jp-controls-holder">

					<div class="jp-volume-bar">
						<div class="jp-volume-bar-value">
							<span class="handle"></span>
						</div>
					</div>
					<ul class="jp-toggles">
						<li></li>
						<li><a class="jp-restore-screen" tabindex="1"
							title="restore screen"></a>
						</li>
						<li><a class="jp-repeat" tabindex="1" title="repeat"></a>
						</li>
						<li><a class="jp-repeat-off" tabindex="1" title="repeat off"></a>
						</li>
					</ul>
				</div>
				<div class="jp-progress-box">

					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>

					</div>
					<div class="jp-time">
						<!--time notifications-->
						<div class="jp-current-time"></div>
						<span class="time-sep">/</span>
						<div class="jp-duration"></div>
						<span class="separator sep-2"></span>
					</div>

				</div>

				<a class="jp-full-screen" tabindex="1" title="full screen"></a> <a
					href="javascript:;" class="jp-restore-screen" tabindex="1"
					title="full screen"></a>
			</div>
		</div>

		<div class="jp-no-solution">
			<span>Update Required</span> To play the media you will need to
			either update your browser to a recent version or update your <a
				href="http://get.adobe.com/flashplayer/" target="_blank">Flash
				plugin</a>.
		</div>
	</div>
</div>

<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
/*<![CDATA[*/

MEDIAURL = '<?= $mediaUrl ?>';

$(document).ready(function(){
	// Create Instance of jPlayer
	var solution = "html,flash";
	if(!!navigator.userAgent.match(/Trident.*rv\:11\./))	//IE 11
		solution = "flash,html";

	$('#jp_container_<?= $videoId ?>').bind('contextmenu',function() { return false; });

	$("#jquery_jplayer").jPlayer({
		cssSelectorAncestor: '#jp_container_<?= $videoId ?>',
		ready: function () {
			$(this).jPlayer("setMedia", {
				m4v: MEDIAURL
			});
			$(this).jPlayer("playHead");
		},
		play: function(){
			$('#jp-big-play').hide();
		},
		//Comment this 3 rows for remove respawn of the big blue button when video is paused
		pause: function(){
			$('#jp-big-play').show();
		},
		swfPath: '<?= $this->getAssetsUrl() . '/js' ?>',
		supplied: "m4v",
		solution: solution,
		size: {
			height: '100%',
			width: '100%',
			cssClass: "jp-video-extended"
		},
		verticalVolume: true
	});
	$(this).find('.jp-volume-box').css("display","none");

	//if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
	if(<?=(Docebo::isMobile() ? 'true' : 'false');?>)
	{
		$('#volume-container').css("display","none");
	}
	else
	{
		$('#volume-container').hover(
			function () {
				$(this).find('.jp-mute').addClass('hover');
				$(this).find('.jp-volume-box').css("display","block");
				$(this).find('.jp-volume-box .jp-volume-bar').css("display","block");
				$(this).find('.jp-volume-box .jp-volume-bar-value').css("display","block");
			},
			function () {
				$(this).find('.jp-mute').removeClass('hover');
				$(this).find('.jp-volume-box').css("display","none");
				$(this).find('.jp-volume-box .jp-volume-bar').css("display","none");
				$(this).find('.jp-volume-box .jp-volume-bar-value').css("display","none");
			}
		);
	}

	$('#jp-big-play').on('click', function(){
		$("#jquery_jplayer").jPlayer("play");
	});

	if(navigator.appVersion.indexOf("Win") != -1 && Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0)
	{
		$("div .jp-progress-box").css("display", "inline-block");
		$("div .jp-volume-box").css("left", "38px");
	}
});


/*]]>*/
</script>
