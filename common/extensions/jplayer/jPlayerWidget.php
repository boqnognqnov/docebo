<?php

/**
 * A widget to display jquery.jplayer Media player (http://jplayer.org/)
 *
 * @TODO Improve this, please
 * -- layouts
 * -- CSS
 * -- different medias
 * -- more config Options and htmlOptions
 * -- lot more can be done..
 * -- thanks
 *
 * @author Plamen Petkov
 *
 */
class jPlayerWidget extends CWidget {

	public $layout = 'default'; // see 'views' folder of the widget
	public $skin = 'bluemonday'; // see 'skin' folder

    public $videoId;
	public $title;
    public $mediaUrl = ''; // URL of the media to play
	public $autoplay = false;

	public $options = array(); // not used yet
	public $htmlOptions = array(); // not used yet

	// Keeps published assets URL
	protected $_assetsUrl = null;


	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init() {
		// Register path alias.
		if (Yii::getPathOfAlias('jplayer') === false) {
			Yii::setPathOfAlias('jplayer', realpath(dirname(__FILE__)));
		}

		// Prevents the extension from registering scripts and publishing assets when ran from the command line.
		if (Yii::app() instanceof CConsoleApplication) {
			return;
		}

		Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/skin/" . $this->skin . "/jplayer.css");
		Yii::app()->getClientScript()->registerScriptFile($this->getAssetsUrl() . '/js/jquery.jplayer.min.js', CClientScript::POS_HEAD);

		parent::init();
	}


	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run() {
		echo $this->render($this->layout, array(
			'videoId' => $this->videoId,
			'title' => $this->title,
			'mediaUrl' => $this->mediaUrl,
			'autoplay' => $this->autoplay
		), true);
	}


	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL
	 */
	public function getAssetsUrl() {
		if (isset($this->_assetsUrl)) {
			return $this->_assetsUrl;
		} else {
			$assetsPath = Yii::getPathOfAlias('jplayer.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			return $this->_assetsUrl = $assetsUrl;
		}
	}


}