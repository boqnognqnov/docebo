<?php
Yii::import('common.extensions.tfpdf.*');
Yii::import('common.extensions.fpdi.*');

class PDF_Table extends fpdi
{
	public $widths;
	public $align;
	public $fontFamily = 'DejaVu';
	public $fontSize = 10;

	protected $headers = array();

	public function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	public function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=5*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->align) ? $this->align : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Print the text
			$this->MultiCell($w,5,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	public function exportTable($headers, $data, $totalText, $orientation = 'P') {
		$this->AddPage($orientation);
		$this->headers = $headers;

		$this->AddFont('DejaVu','','DejaVuSans.ttf',true);
		$this->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);

		$this->printTableHeaders();

		//Data
		foreach($data as $row)
			$this->Row($row);

		//Total
		$this->SetFont($this->fontFamily,'B', $this->fontSize);
		$this->Cell(50, 5, $totalText);
	}

	public function addExistingPdf($existingPdfSourceFile, $boxName = '/MediaBox', $orientation = 'P'){
		if(file_exists($existingPdfSourceFile)) {
			$pageCount = $this->setSourceFile($existingPdfSourceFile);			
			for($i = 0; $i < $pageCount; $i++) {
				$tplIdx = $this->importPage($i + 1, $boxName);
				$this->addPage($orientation);
				$this->useTemplate($tplIdx);
			}
		}
	}

	protected function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger) {
			$this->AddPage($this->CurOrientation);
			$this->printTableHeaders();
		}
	}

	protected function printTableHeaders() {
		//Header
		$this->align = 'C';
		$this->SetFont($this->fontFamily,'B', $this->fontSize);
		$this->Row($this->headers);
		$this->SetFont($this->fontFamily,'', $this->fontSize);
		$this->align = null;
	}

	protected function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=mb_ereg_replace("\r",'',$txt, "si");
		$nb=mb_strlen($s);
		$s = preg_split('//u', $s, -1, PREG_SPLIT_NO_EMPTY);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$this->GetStringWidth($c)/$this->FontSize*1000;
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}