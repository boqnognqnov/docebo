<?php
/**
 * TbDatepicker class file.
 * @author Andrei Amariutei
 * @package bootstrap.widgets
 */

/**
 * Bootstrap datepicker widget.
 */
class TbDatepicker extends CWidget
{
	/**
	 * @var string
	 */
	public $label;

	/**
	 * @var string
	 */
	public $value;

	/**
	 * @var string
	 */
	public $format = 'dd-mm-yyyy';

	/**
	 * @var array the HTML attributes for the widget container.
	 */
	public $htmlOptions = array();

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		$this->htmlOptions = array_merge(array(
			'class' => 'input-medium',
			'id' => __CLASS__.'_datepicker',
			'name' => 'datepicker'
		), $this->htmlOptions);
	}

	/**
	 * Runs the widget.
	 */
	public function run()
	{
		/*
		 <form class="form-search" method="POST" action="">
			<label for="">Search by date</label>
			<input type="text" class="input-medium search-query">
			<span class="i-sprite calendar-date"></span>
		 </form>
		 */
		echo CHtml::openTag('div', array('class' => 'widget-bdatepicker'));
		echo CHtml::label($this->label, $this->htmlOptions['name']);

		echo CHtml::textField($this->htmlOptions['name'], $this->value, $this->htmlOptions);

		echo CHtml::tag('span', array('class' => 'i-sprite is-calendar-date'), '');
		echo CHtml::closeTag('div');

		$selector = '#'.$this->htmlOptions['id'];

		/* @var CClientScript $cs */
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(__CLASS__.$selector, "jQuery('{$selector}').bdatepicker({format: '{$this->format}'});");
	}
}