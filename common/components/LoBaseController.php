<?php
/**
 * Base controller for Player module and all Learning Objects Modules (test, file, scormorg, ...)
 *
 * ! Register Common assets
 * ! Provides access to Player's shared assets (getPlayerAssetsUrl)
 *
 */
class LoBaseController extends Controller
{

	private $_playerAssetsUrl;
	private $_idCourse = null;

	//manage classroom course additional info
	private $_idSession = null;

	/**
	 * Load or NOT resources (JS, CSS,...)
	 * 
	 * @var boolen
	 */
	public $registerResources = true;

	//get / set methods

	public function getIdCourse() {
		return $this->_idCourse;
	}

	public function setIdCourse($idCourse) {
		if ((int)$idCourse > 0) {
			$this->_idCourse = (int)$idCourse;
			return true;
		} else {
			return false;
		}
	}

	public function getIdSession() {
		return (PluginManager::isPluginActive('ClassroomApp') ? $this->_idSession : false);
	}

	public function setIdSession($idSession) {
		if (!PluginManager::isPluginActive('ClassroomApp')) return false;
		if ((int)$idSession > 0) {
			$this->_idSession = (int)$idSession;
			return true;
		} else {
			return false;
		}
	}



	/**
	 * (non-PHPdoc)
	 * @see CController::init()
	 */
	public function init()
	{
		$this->setIdCourse(Yii::app()->request->getParam("course_id", false));
		if (PluginManager::isPluginActive('ClassroomApp')) { $this->setIdSession(Yii::app()->request->getParam("session_id", false)); }
		return parent::init();
	}

	/**
	 * Creates a relative URL for the specified action defined in this controller.
	 * @param string $route the URL route. This should be in the format of 'ControllerID/ActionID'.
	 * If the ControllerID is not present, the current controller ID will be prefixed to the route.
	 * If the route is empty, it is assumed to be the current action.
	 * If the controller belongs to a module, the {@link CWebModule::getId module ID}
	 * will be prefixed to the route. (If you do not want the module ID prefix, the route should start with a slash '/'.)
	 * @param array $params additional GET parameters (name=>value). Both the name and value will be URL-encoded.
	 * If the name is '#', the corresponding value will be treated as an anchor
	 * and will be appended at the end of the URL.
	 * @param string $ampersand the token separating name-value pairs in the URL.
	 * @return string the constructed URL
	 */
	public function createUrl($route,$params=array(),$ampersand='&') {
		/*
		 * add course id here so we don't have to add it in every view
		 * each time we create an url
		 */
		$idCourse = $this->getIdCourse();
		if (intval($idCourse) > 0 && !isset($params['course_id'])) {
			$params['course_id'] = $idCourse;
		}

		/*
		 * if classroom are enabled then add session id here too
		 */
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$idSession = $this->getIdSession();
			if (intval($idSession) > 0 && !isset($params['session_id'])) {
				$params['session_id'] = $idSession;
			}
		}

		return parent::createUrl($route, $params, $ampersand);
	}

	/**
	 * (non-PHPdoc)
	 * Method invoked before any action, used for register some scripts or other prepares
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		// Register JS/CSS ??
		if ($this->registerResources)
 			$this->registerResources();
		
		return parent::beforeAction($action);
	}


	/**
	 * Register resources (CSS/JS), common to Player and all LO modules
	 *
	 */
	public function registerResources() {
		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();

			// Core Yii
			$cs->registerCoreScript('jquery');
			$cs->registerCoreScript('jquery.ui');
			$cs->registerCssFile($cs->getCoreScriptUrl(). '/jui/css/base/jquery-ui.css');

			// Player assets we share
			$cs->registerCssFile ($this->getPlayerAssetsUrl() 		. '/css/base.css')
				->registerCssFile($this->getPlayerAssetsUrl() 		. '/css/base-responsive.css')
				//->registerCssFile($this->getPlayerAssetsUrl() 		. '/css/player-sprite.css')  // moved to themes
				//->registerCssFile($this->getPlayerAssetsUrl() 		. '/css/i-sprite.css')  // moved to themes
				->registerCssFile(Yii::app()->theme->baseUrl 		. '/css/yiiform.css');

			// Also register Player.js (sharing it)
			$cs->registerScriptFile($this->getPlayerAssetsUrl() . '/js/player.js', CClientScript::POS_HEAD);

			// Player.init() JS in Document Ready. If we need some initial options
			$options = array(
			);
			$options = CJavaScript::encode($options);
			$script = "Player.init($options);";
			$cs->registerScript("player_lobasecontroller_init", $script , CClientScript::POS_READY);
		}
	}


	/**
	 * Get PLAYER's assets url path
	 * We have plenty of CSS/JS code in Player module's assets already, which we do share.
	 *
	 * @return mixed
	 */
	public function getPlayerAssetsUrl()
	{
		if ($this->_playerAssetsUrl === null) {
			if(!is_dir(Yii::getPathOfAlias('player.assets'))){
				Yii::log("Can not publish player assets", CLogger::LEVEL_ERROR);
				return false;
			}

			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}



}