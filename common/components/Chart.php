<?php

class Chart extends CApplicationComponent {

	private $isIE8;

	/**
	 * (non-PHPdoc)
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		$this->isIE8 = $this->isIE8();
	}

	public function isIE8() {
		$isIE8 = preg_match('/(?i)msie [1-8]\./',$_SERVER['HTTP_USER_AGENT']);
		// $isIE8 = 1;

		return $isIE8;
	}

	protected function hex2rgb($hex, $imploded = false)
	{
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3)
		{
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		}
		else
		{
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}

		$rgb = array($r, $g, $b);

		if($imploded)
			return implode(',', $rgb);
		return $rgb;
	}

	private function generateChartFileName() {
		return md5(microtime()) . '.png';
	}

	private function resolveTempChartImagePath() {
		$path = dirname(Yii::app()->basePath) . '/assets/pchart';
		if (!file_exists($path)) {
			mkdir($path);
		}
		return $path;
	}

	private function getChartFileUrl($chartFileName) {
		return Yii::app()->getBaseUrl(true) . '/assets/pchart/' . $chartFileName;
	}

	public function getPieChart($chartData, $useLocalPath = false) {
		if ($this->isIE8) {
			$pieChart = $this->getPHPPieChart($chartData, $useLocalPath);
		} else {
			$pieChart = $this->getSVGPieChart($chartData);
		}
		return $pieChart;
	}

	public function getSinglePieChart($chartValue, $chartPercent, $useLocalPath = false) {
		if ($this->isIE8) {
			$singlePieChart = $this->getPHPSinglePieChart($chartPercent, $useLocalPath);
		} else {
			$singlePieChart = $this->getSVGSinglePieChart($chartValue, $chartPercent);
		}
		return $singlePieChart;
	}

	public function getLineChart($chartData, $chartCategories, $properties = array(), $useLocalPath = false) {
		if ($this->isIE8) {
			$lineChart = $this->getPHPLineChart($chartData, $chartCategories, $properties, $useLocalPath);
		} else {
			$lineChart = $this->getSVGLineChart($chartData, $chartCategories, $properties);
		}
		return $lineChart;
	}

	public function getPieChartLegend($chartData) {
		$legendContent = '';
		if (!empty($chartData)) {
			$colors = Yii::app()->theme->getColors();
			$styles = array();
			$styles[] = (is_array($colors) && isset($colors['@charts1']) ? ' style="color:'.$colors['@charts1'].' !important"' : '');
			$styles[] = (is_array($colors) && isset($colors['@charts2']) ? ' style="color:'.$colors['@charts2'].' !important"' : '');
			$styles[] = (is_array($colors) && isset($colors['@charts3']) ? ' style="color:'.$colors['@charts3'].' !important"' : '');
			foreach ($chartData as $key => $data) {
				$legendContent .= '<div class="legend-field'.($key + 1).'">';
					$legendContent .= '<span class="legend-count"'.$styles[$key].'>'. $data['count'] .'</span>';
					$legendContent .= '<span class="legend-percent"'.$styles[$key].'>('. $data['percent'] .'%)</span>';
					$legendContent .= '<div class="legend-title"'.$styles[$key].'>'. $data['title'] .'</div>';
				$legendContent .= '</div>';
			}
		}
		return $legendContent;
	}

	protected function getSVGPieChart($chartData) {
		$content = '';
		if (!empty($chartData)) {
			$colors = Yii::app()->theme->getColors();
			$donutData = array(
				'initRadius' => 32,
				'thickness' => 16,
				'data' => array(
					array('color' => array($colors['@charts3'])),
					array('color' => array($colors['@charts2'])),
					array('color' => array($colors['@charts1'])),
				),
			);

			foreach ($chartData as $key => $data) {
				$donutData['data'][$key] += array(
					'color' => array_push($donutData['data'][$key]['color'], CoreSchemeColor::CHART_BACKGROUND_COLOR),
					'data' => array($data['percent'], 100 - $data['percent']),
				);
			}
			$content = CHtml::image(Yii::app()->theme->baseUrl. '/images/report/active-courses.png', '', array('class' => 'report-donut-img'));
			$content .= '<div class="donutContainer" id="donut-'. time() .'" data-dataset=\''. json_encode($donutData) .'\'></div>';
		}
		return $content;
	}

	protected function getPHPPieChart($chartData, $useLocalPath = false) {
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pChart.class');
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pData.class');

		$dataPoints = array();
		//$dataLabels = array();
		foreach ($chartData as $dataRecord) {
			$dataPoints[] = $dataRecord['count'];
			//$dataLabels[] = $dataRecord['title'];
		}
		$DataSet = new pData;
		$DataSet->AddPoint($dataPoints, "Serie1");
		//$DataSet->AddPoint($dataLabels, "Serie2");
		$DataSet->AddAllSeries();
		//$DataSet->SetAbsciseLabelSerie("Serie2");

		// Initialise the graph
		$pChart = new pChart(300, 200);
		$colors = Yii::app()->theme->getColors();
		$color_0 = $this->hex2rgb($colors['@charts1']);
		$pChart->setColorPalette(0, $color_0[0], $color_0[1], $color_0[2]);
		$color_1 = $this->hex2rgb($colors['@charts2']);
		$pChart->setColorPalette(1, $color_1[0], $color_1[1], $color_1[2]);
		$color_2 = $this->hex2rgb($colors['@charts3']);
		$pChart->setColorPalette(2, $color_2[0], $color_2[1], $color_2[2]);
		$pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 8);
		$pChart->drawFilledRoundedRectangle(7, 7, 293, 193, 5, 240, 240, 240);
		$pChart->drawRoundedRectangle(5, 5, 295, 195, 5, 230, 230, 230);

		// Draw the pie chart
		$pChart->AntialiasQuality = 0;
		$pChart->setShadowProperties(2, 2, 200, 200, 200);
		$pChart->drawFlatPieGraphWithShadow($DataSet->GetData(), $DataSet->GetDataDescription(), 150, 100, 70, PIE_PERCENTAGE, 8);
		$pChart->clearShadow();

		//$pChart->drawPieLegend(230, 15, $DataSet->GetData(), $DataSet->GetDataDescription(), 250, 250, 250);

		$chartFileName = $this->generateChartFileName();
		$chartFileFullName = $this->resolveTempChartImagePath() . DIRECTORY_SEPARATOR . $chartFileName;
		$pChart->Render($chartFileFullName);

		if($useLocalPath)
			$imageUrl = $chartFileFullName;
		else
			$imageUrl = $this->getChartFileUrl($chartFileName);

		$pieChartContent = '<div id="donut-'. time() .'" class="donutContainer">'. CHtml::image($imageUrl) .'</div>';
		return $pieChartContent;
	}

	protected function getSVGSinglePieChart($chartValue, $chartPercent) {
		$content = '';

		// if (!empty($chartPercent)) {
			$donutSingleData = array(
				'initRadius' => 38,
				'thickness' => 12,
				'data' => array(
					array(
						'data' => array($chartPercent, 100 - $chartPercent),
						'color' => array(CoreSchemeColor::CHART_SINGLE_GREEN_COLOR, CoreSchemeColor::CHART_BACKGROUND_COLOR)
					),
				),
			);
			$chartPercent = round($chartPercent, 2);
			$content .= '<div class="completed-stat-icon"><div class="completed-stat-percent"><strong>'. $chartPercent .'</strong>%</div><div>'. $chartValue .'</div></div>';
			$content .= '<div class="donutContainer" id="donut-single-'. time() .'" data-dataset=\''. json_encode($donutSingleData) .'\'></div>';
		// }
		return $content;
	}

	protected function getPHPSinglePieChart($chartPercent, $useLocalPath = false) {
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pChart.class');
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pData.class');

		$dataPoints = array();
		//$dataLabels = array();
		// foreach ($chartData as $dataRecord) {
		// 	$dataPoints[] = $dataRecord['count'];
		// 	//$dataLabels[] = $dataRecord['title'];
		// }
		$DataSet = new pData;
		$DataSet->AddPoint(array($chartPercent, 100 - $chartPercent), "Serie1");
		//$DataSet->AddPoint($dataLabels, "Serie2");
		$DataSet->AddAllSeries();
		//$DataSet->SetAbsciseLabelSerie("Serie2");

		// Initialise the graph
		$pChart = new pChart(300, 200);
		$pChart->loadColorPalette(Yii::getPathOfAlias('adminExt.pChart') . DIRECTORY_SEPARATOR . 'paletteSingle.txt');
		$pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 8);
		$pChart->drawFilledRoundedRectangle(7, 7, 293, 193, 5, 240, 240, 240);
		$pChart->drawRoundedRectangle(5, 5, 295, 195, 5, 230, 230, 230);

		// Draw the pie chart
		$pChart->AntialiasQuality = 0;
		$pChart->setShadowProperties(2, 2, 200, 200, 200);
		$pChart->drawFlatPieGraphWithShadow($DataSet->GetData(), $DataSet->GetDataDescription(), 150, 100, 70, PIE_PERCENTAGE, 8);
		$pChart->clearShadow();

		//$pChart->drawPieLegend(230, 15, $DataSet->GetData(), $DataSet->GetDataDescription(), 250, 250, 250);

		$chartFileName = $this->generateChartFileName();
		$chartFileFullName = $this->resolveTempChartImagePath() . DIRECTORY_SEPARATOR . $chartFileName;
		$pChart->Render($chartFileFullName);

		if($useLocalPath)
			$imageUrl = $chartFileFullName;
		else
			$imageUrl = $this->getChartFileUrl($chartFileName);

		$pieChartContent = '<div id="donut-single-'. time() .'" class="donutContainer">'. CHtml::image($imageUrl, '', array('class' => 'ie-image')) .'</div>';
		return $pieChartContent;
	}

	protected function getSVGLineChart($chartData, $chartCategories, $properties = array()) {
		//detect properties
		$styleProperties = array( //default styling properties
			'width' => '560px',
			'height' => '250px',
            'overflow' => 'visible'
		);
	//style="height: 250px; width: 560px;"
		if (is_array($properties) && isset($properties['width'])) { $styleProperties['width'] = $properties['width']; }
		if (is_array($properties) && isset($properties['height'])) { $styleProperties['height'] = $properties['height']; }
		if (is_array($properties) && isset($properties['overflow'])) { $styleProperties['overflow'] = $properties['overflow']; }
		$style = '';
		if (!empty($styleProperties)) {
			$style = 'style="';
			foreach ($styleProperties as $key => $value) {
				$style .= $key.':'.$value.';';
			}
			$style .= '"';
		}
		//subtitle
		$subtitle = (is_array($properties) && isset($properties['subtitle'])
			? $properties['subtitle']
			: Yii::t('report', '_TH_USER_NUMBER_SESSION'));
		//compose chart markup
		$uniqueId = rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9);
		$lineChartContent	= '<div id="line-chart-'. time() .'-'.$uniqueId.'" class="lineChartContainer" '.$style.' data-dataset=\''. json_encode($chartData) .'\' data-subtitle="'. $subtitle .'" data-categories=\''. json_encode($chartCategories) .'\'></div>';
		$lineChartContent	.= '<div id="chart-tooltip"></div>';
		return $lineChartContent;
	}

	protected function getPHPLineChart($chartData, $chartCategories, $properties = array(), $useLocalPath = false) {
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pChart.class');
		include_once(Yii::getPathOfAlias('adminExt.pChart.pChart') . DIRECTORY_SEPARATOR . 'pData.class');

		$DataSet = new pData;
		$DataSet->AddPoint($chartData, "Serie1");
		$DataSet->AddPoint($chartCategories, "Serie2");
		$DataSet->SetAbsciseLabelSerie("Serie2");
		$DataSet->AddAllSeries();

		// Initialise the graph
		$pChart = new pChart(500, 300);
		$colors = Yii::app()->theme->getColors();
		$color_0 = $this->hex2rgb($colors['@charts1']);
		$pChart->setColorPalette(0, $color_0[0], $color_0[1], $color_0[2]);
		$color_1 = $this->hex2rgb($colors['@charts2']);
		$pChart->setColorPalette(1, $color_1[0], $color_1[1], $color_1[2]);
		$color_2 = $this->hex2rgb($colors['@charts3']);
		$pChart->setColorPalette(2, $color_2[0], $color_2[1], $color_2[2]);
		$pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 8);
		$pChart->setGraphArea(30, 30, 480, 200);
		$pChart->drawFilledRoundedRectangle(7, 7, 693, 223, 5, 240, 240, 240);
		$pChart->drawRoundedRectangle(5, 5, 695, 225, 5, 230, 230, 230);
		$pChart->drawGraphArea(255, 255, 255, true);
		$pChart->drawScale($DataSet->GetData(), $DataSet->GetDataDescription(), SCALE_NORMAL, 150, 150, 150, true, 0, 2);
		$pChart->drawGrid(4, true, 230, 230, 230, 50);

		$pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 6);
		$pChart->drawTreshold(0, 143, 55, 72, true, true);

		// Draw the line graph
		$pChart->drawLineGraph($DataSet->GetData(), $DataSet->GetDataDescription());
		$pChart->drawPlotGraph($DataSet->GetData(), $DataSet->GetDataDescription(), 3, 2, 255, 255, 255);

		// Finish the graph
		$pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 8);
		//$pChart->drawLegend(75, 35, $DataSet->GetDataDescription(), 255, 255, 255);
		//pChart->setFontProperties(dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "fonts/tahoma.ttf", 10);
		//$pChart->drawTitle(60, 22, "example 1", 50, 50, 50, 585);

		$chartFileName = $this->generateChartFileName();
		$chartFileFullName = $this->resolveTempChartImagePath() . DIRECTORY_SEPARATOR . $chartFileName;
		$pChart->Render($chartFileFullName);

		if($useLocalPath)
			$imageUrl = $chartFileFullName;
		else
			$imageUrl = $this->getChartFileUrl($chartFileName);

		$lineChartContent = '<div id="line-chart-'. time() .'" class="lineChartContainer">'. CHtml::image($imageUrl) .'</div>';
		return $lineChartContent;
	}
}