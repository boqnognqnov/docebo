<?php
Yii::import('zii.widgets.CListView');

class DoceboCListView extends CListView {

	public $columns = array();
	public $template = '{header}{items}';
	public $pagerCssClass = 'doceboPager';

	public function init()
	{
		$this->cssFile = Yii::app()->theme->baseUrl . '/css/DoceboCListView.css';

		$this->emptyText = Yii::t('report', '_NULL_REPORT_RESULT');

		parent::init();
	}

	/**
	 * Renders the header of the list (similar style to that of the grid).
	 */
	public function renderHeader()
	{
		if (empty($this->columns)) return;
		echo CHtml::openTag('div', array('class' => 'header')) . "\n";
		foreach ($this->columns as $column) {
			$header = isset($column['header']) ? $column['header'] : '';
			echo CHtml::tag('div', array('class' => 'item-col col-' . $column['name']), $header) . "\n";
		}
		echo CHtml::closeTag($this->itemsTagName);
	}

	public function renderPager()
	{
		if(!$this->enablePagination)
			return;

		$pager=array();
		$class='DoceboCLinkPager';
		if(is_string($this->pager))
			$class=$this->pager;
//		else if(is_array($this->pager))
//		{
//			$pager=$this->pager;
//			if(isset($pager['class']))
//			{
//				$class=$pager['class'];
//				unset($pager['class']);
//			}
//		}
		$pager['pages']=$this->dataProvider->getPagination();

		//if($pager['pages']->getPageCount()>1)
		//{
		echo '<div class="'.$this->pagerCssClass.'">';
		$this->widget($class,$pager);
		echo '</div>';
		//}
		//else
		//	$this->widget($class,$pager);
	}

}
