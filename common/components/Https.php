<?php
/**
 * Yii application component to provide various SSL related functionality
 *
 */
class Https extends CApplicationComponent {

	/**
	 * Array of strings, patterns, to match against current request route
	 * See main.php and main-xxx.php configuration files.
	 *
	 * @var array
	 */
	public $whiteListRoute = array();

	/**
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		parent::init();
	}


	/**
	 * Get HTTPS mode for current request, 0 = no redirect to https, 1 = force redirect
	 *
	 * @return boolean
	 */
	public function mustRedirect() {

		/* @var $model CoreHttps */
		static $cache = null;

		// See if current request route is NOT subject to https redirection
		// and refuse redirection without any further checks if it is white listed
		$isWhitelistedRoute = $this->isWhitelistedRoute();
		if ($isWhitelistedRoute) {
		    return false;
		}

		// Also, HTTPS connections are definitely NOT subject for redirection (loop)!!
		// Note please, usually callers do this check, but lets be sure we do not order https redirection for already secure connections!
		if (Yii::app()->request->isSecureConnection) {
		    return false;
		}

		if ($cache !== null) {
			return $cache;
		}

		$forceRedirect = Settings::get('force_saas_https_redirect', 0);
		if  (
				(
					//preg_match("/\.scavaline.com$/", strtolower($_SERVER['HTTP_HOST'])) ||
				    //preg_match("/\.petkoff.eu$/", strtolower($_SERVER['HTTP_HOST'])) ||
					preg_match("/\.docebosaas.com$/", strtolower($_SERVER['HTTP_HOST'])) ||
					preg_match("/\.docebosandbox.com$/", strtolower($_SERVER['HTTP_HOST'])) ||
					preg_match("/\.docebo.info$/", strtolower($_SERVER['HTTP_HOST']))   // allow test server to handle this
				)
		          &&
		        (
				    PluginManager::isPluginActive('HttpsApp') || $forceRedirect
		        )
		    ) {
		    $cache = true;
			return $cache;
		}

		if(!PluginManager::isPluginActive('HttpsApp'))
		{
			$cache = false;
			return false;
		}

		// Resolve HTTPS redirection based on Multidomain if
		// 1. MultidomainApp is ON
		// 2. There is a valid client resolved
		$multidomainClientModel = false;
		if (PluginManager::isPluginActive('MultidomainApp')) {

			// Resolve multidomain client
			$multidomainClientModel = CoreMultidomain::resolveClient();

			// However, "subfolder" domains are NOT considered subject of "https redirection" policy; reset model to false
			if ($multidomainClientModel && ($multidomainClientModel->domain_type == CoreMultidomain::DOMAINTYPE_SUBFOLDER)) {
				$multidomainClientModel = false;
			}

			// Also, if we found a multidomain client, check if HTTPS settings are enabled for it
			// @todo implement httpsEnabled (meaning, the whole setting at all)
			//if ($multidomainClientModel && !$multidomainClientModel->httpsEnabled) {
			//	$multidomainClientModel = false;
			//}

		}

		// Any multodomain ID ?
		$idMultidomainClient = $multidomainClientModel ? $multidomainClientModel->id : null;

		// Now, find CoreHttps model, respecting Multidomain ID, if any, otherwise return "general https", if any
		$model = CoreHttps::model()->findByAttributes(array('idMultidomainClient' => $idMultidomainClient));

		// Have we found a model describig https redirection policy (be it multidomain or GENERAL one /having MD Id= null/)  ?
		if ($model) {

			// Get the current HOST from the request
			$host = preg_replace('/^www\\./', '', strtolower($_SERVER['HTTP_HOST']));

			if (PluginManager::isPluginActive('CustomdomainApp')) {
				// Redirect if: If Certificate is "for custom domain" AND
				// Has its own certificate installed OR There is a sibling domain wildcarded
				// AND current Host matches the Https domain
				if ($model->mode == CoreHttps::MODE_ON_CUSTOM_SSL && ($model->sslStatus == CoreHttps::SSL_STATUS_INSTALLED || CoreHttps::sibling2ndLvlDomainIsWildcarded($host))) {
			        $redirect = ($host == $model->domain);
				}
				// If above fails... check for Original Domain Wildcard redirection (i.e. .docebosaas.com).
				// It doesn't have to be installed, because *.docebosaas.com is covered
				else if ($model->mode == CoreHttps::MODE_ON_DOCEBO_WILDCARD_SSL) {
					$redirect = $host == trim(Docebo::getOriginalDomain());
				}
			}
			else {
				if ($model->mode == CoreHttps::MODE_ON_CUSTOM_SSL && $model->sslStatus == CoreHttps::SSL_STATUS_INSTALLED) {
					$redirect = ($host == $model->domain);
				}
				else {
					$redirect = ($model->mode == CoreHttps::MODE_ON_DOCEBO_WILDCARD_SSL);
				}
			}

			$cache = $redirect;
		}
		else {
			$cache = false;
		}

		return $cache;
	}

	/**
	 * Check if Route is subject to whitelisting, i.e. NOT to be https redirected
	 */
	public function isWhitelistedRoute($route=false) {

		if (!$route) {
			$module				= strtolower(Yii::app()->controller->module->id);
			$controller 		= strtolower(Yii::app()->controller->id);
			$action 			= strtolower(Yii::app()->controller->action->id);
			$route = "$controller/$action";
			if ($module) $route = $module . "/" . $route;
		}

		foreach ($this->whiteListRoute as $pattern) {
			if (preg_match('#'.$pattern.'#i', $route)) {
				return true;
			}
		}

		return false;

	}


}