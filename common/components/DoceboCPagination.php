<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 19-Apr-16
 * Time: 5:41 PM
 */
class DoceboCPagination extends CPagination
{
	private $offset;
	private $pageSize;

	public function setOffset($value){
		$value = intval($value);
		if ($value < 0) $value = 0;
		$this->offset = $value;
	}

	public function getOffset()
	{
		return $this->offset;
	}

	public function setLimit($value)
	{
		$value = intval($value);
		if ($value <= 0) $value = Settings::get('elements_per_page', 10);
		$this->pageSize = $value;
	}

	public function getLimit()
	{
		return $this->pageSize;
	}
}