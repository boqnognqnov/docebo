<?php

/*
 * Recreation of some functionality found in plugins/userlimit/
 */

class UserLimit {

	static public function logActiveUserLoginOLD($userId = false){

		if(!$userId)
			$userId = Yii::app()->user->id;
		
		// No used Id or user is a guest? Get out!
		if (!$userId || Yii::app()->user->getIsGuest()) {
			return;
		}
		
		$subscription_begin_date = Settings::get('subscription_begin_date');
		$UtcTimeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

		if($subscription_begin_date && strtotime($subscription_begin_date)){ // Valid subscription date
			if ( Docebo::dateExists($subscription_begin_date) && (strtotime($subscription_begin_date) < $UtcTimeNow) ) { // Is a valid date

					$period_end_date = $subscription_begin_date; // Temporary thing

					while(strtotime($period_end_date) < $UtcTimeNow){ // Loop until today's active period
						$period_end_date = date('Y-m-d', Docebo::addToDate($period_end_date, "+1 months"));
						$period_begin_date = date('Y-m-d', Docebo::addToDate($period_end_date, "-1 months"));
					}

					// Get the activity/user logins for the current period
					$criteria = new CDbCriteria();
					$criteria->addCondition('DATE(first_period_login)>=DATE(:period_begin_date)');
					$criteria->addCondition('DATE(first_period_login)<=DATE(:period_end_date)');
					$criteria->addCondition('userIdst=:user_id');
					$criteria->params[':period_begin_date'] = $period_begin_date;
					$criteria->params[':period_end_date'] = $period_end_date;
					$criteria->params[':user_id'] = $userId;

					$userAlreadyLoggedAsActive = LearningUserlimitLoginLog::model()->exists($criteria);

					if(!$userAlreadyLoggedAsActive){
						// Get User's local datetime (string)
						$localDateTimeNow = Yii::app()->localtime->toLocalDateTime();
						
						// Log the user as active (now, meaning that he is
						// counted towards the current subscription period)
						$newLogModel = new LearningUserlimitLoginLog();
						$newLogModel->userIdst = $userId;
						$newLogModel->first_period_login = $localDateTimeNow;
						Yii::log('New active user registered: ' . $localDateTimeNow . ", User ID: " . $userId, 'debug');
						$newLogModel->save();

						if(Docebo::isSaas()){
							// Check if it's neccesary to send a notifications (90% or 100% User limit reached)
							self::activeUserLimitNotifications();
						}
					}

			}

		}
	}
	

	
	static public function logActiveUserLogin($userId = false, $allowGuest = false){

		if(!$userId)
			$userId = Yii::app()->user->id;
		// No used Id or user is a guest? Get out!
		if (!$userId || (Yii::app()->user->getIsGuest() && !$allowGuest)) {
			return;
		}

		//make sure that no "staff.docebo" user will be ever logged !!
		$user = CoreUser::model()->findByPk($userId);
		if ($user) {
			$toCheck = strtolower(Yii::app()->user->getRelativeUsername($user->userid));
			if ($toCheck == 'staff.docebo') return; //never log any "staff.docebo" user !!
		}

		$subscription_begin_date = Settings::get('subscription_begin_date');
		$UtcTimeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

		if($subscription_begin_date && strtotime($subscription_begin_date)){ // Valid subscription date
			if ( Docebo::dateExists($subscription_begin_date) && (strtotime($subscription_begin_date) < $UtcTimeNow) ) { // Is a valid date
				
				$dates = Billing::getSubscriptionPeriodDates();

				if (count($dates) <= 0) return;
				
				$currentPeriod = end($dates);
				$period_begin_date = $currentPeriod["from"];
				$period_end_date = $currentPeriod["to"];
	
				// Get the activity/user logins for the current period
				$criteria = new CDbCriteria();
				$criteria->addCondition('first_period_login >= :period_begin_date');
				$criteria->addCondition('first_period_login <= :period_end_date');
				$criteria->addCondition('userIdst=:user_id');
				$criteria->params[':period_begin_date'] = $period_begin_date;
				$criteria->params[':period_end_date'] = $period_end_date;
				$criteria->params[':user_id'] = $userId;
	
				$userAlreadyLoggedAsActive = LearningUserlimitLoginLog::model()->exists($criteria);

				if(!$userAlreadyLoggedAsActive){
					// Get User's local datetime (string)
					$localDateTimeNow = Yii::app()->localtime->toLocalDateTime();
	
					// Log the user as active (now, meaning that he is
					// counted towards the current subscription period)
					$newLogModel = new LearningUserlimitLoginLog();
					$newLogModel->userIdst = $userId;
					$newLogModel->first_period_login = $localDateTimeNow;
					Yii::log('New active user registered: ' . $localDateTimeNow . ", User ID: " . $userId, 'debug');

                    if ($newLogModel->save()) {
                        Yii::app()->event->raise(EventManager::EVENT_ACTIVE_USER_LOGGED, new DEvent(self, array(
                            'logEntry'  => $newLogModel,
                        )));
                    }

                    if(Docebo::isSaas()){
						// Check if it's neccesary to send a notifications (90% or 100% User limit reached)
						self::activeUserLimitNotifications();
					}
				}
	
			}
	
		}
	}
	
	
	/**
	 * The definition of "user is active" has changed as of mid December 2013, 
	 * so we use this method for better readability and correctness of method names. It just forward the call to old one.
	 * 
	 *   Old definition: 
	 *   	User is active when he/she logs in the LMS
	 *   
	 *   New Definition:
	 	*   User is counted as active  when he/she enters some course (no need to PLAY LO, just entering the course in Player zone).
	 *   
	 * @param string $userId
	 */
	static public function logActiveUserAccess($userId = false){
		self::logActiveUserLogin($userId);
	}
	

	static public function activeUserLimitNotifications() {
		$cur_period_logins = Docebo::getActiveUsers();
		$max_users = Settings::get('max_users');
		if(!$max_users) return false;

		$platformExpired = Docebo::isPlatformExpired();
		if(!$platformExpired){
			$godAdminsEmails = false;
				
            if (
                PluginManager::isPluginActive('WhitelabelApp') &&
                Settings::get('whitelabel_menu', null) == 1 &&
                Settings::get('whitelabel_menu_userid', null) !== null
            ) {
                $user = CoreUser::model()->findByPk(Settings::get('whitelabel_menu_userid', null));
                if ($user)
                    $godAdminsEmails = $user->email;
                else {
                	// Commented, to prevent sending emails to god admins if user is not found in the DB [plamen]
                    // $godAdminsEmails = Yii::app()->user->getGodadminsEmails();
                }
            } else
                $godAdminsEmails = Yii::app()->user->getGodadminsEmails();

			if(!empty($godAdminsEmails)){
				if(is_string($godAdminsEmails)){
					$godAdminsEmails = array($godAdminsEmails);
				}
                
				 // Email notification when 90% active users from the current plan are reached
				if(($cur_period_logins / $max_users * 100) < 100 && ($cur_period_logins / $max_users * 100)>=90 && (($cur_period_logins-1) / $max_users * 100)<90){
					ErpApiClient::sendEmailByCode(array('code' => ErpApiClient::_EXTRA_USERS_WARNING));
					ErpApiClient::sendEmailByCode(array('code' => ErpApiClient::_EXTRA_USERS_WARNING_DOCEBO));
				}
				// Email notification when 100% active users from the current plan are reached
				if(($cur_period_logins / $max_users * 100) == 100 && (($cur_period_logins-1) / $max_users * 100)<100){
					ErpApiClient::sendEmailByCode(array('code' => ErpApiClient::_EXTRA_USERS_REACHED_CUSTOMER));
				}
				
				// Send ERP a history notification (lms/save-history) when active users for the current period hits max users + 1 (!)
				if($cur_period_logins == ($max_users + 1)){
				    $api = new ErpApiClient2();
				    $result = $api->lmsSaveHistory(array(
				        "type"          => ErpApiClient2::API_EXCEEDED_PLAN,
				        "active_users"  => $cur_period_logins, // ERP may use this 
				    ));
				}
				

			}
		}
	}




	/**
	 * Count how many active users we have in a given generic period
	 * @param $dateBegin string date in format "Y-m-d H:i:s" (empty = no bagin date specified)
	 * @param $dateEnd string date in format "Y-m-d H:i:s" (empty = no end date specified)
	 * @return mixed the number of counted active users (false if some errors occur)
	 */
	public static function countActiveUsers($dateBegin = false, $dateEnd = false) {

		//validate input
		$checker = '/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/';
		if (!empty($dateBegin) && !preg_match($checker, $dateBegin)) { return false; }
		if (!empty($dateEnd) && !preg_match($checker, $dateEnd)) { return false; }

		//create counter command
		$searchParams = array(':userid' => '/staff.docebo%'); //"staff.docebo" user will be excluded from retrieval
		if (!empty($dateBegin)) { $searchParams[':period_begin_date'] = $dateBegin; }
		if (!empty($dateEnd)) { $searchParams[':period_end_date'] = $dateEnd; }

		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand();
		$cmd->select('count(DISTINCT lul.userIdst)'); //double user references shouldn't belong here
		$cmd->from(LearningUserlimitLoginLog::model()->tableName().' lul');
		$cmd->leftJoin(CoreUser::model()->tableName().' cu', 'lul.userIdst = cu.idst');
		$cmd->where("(cu.userid NOT LIKE :userid) OR (cu.idst IS NULL)");  //last condition is to retrieve also records with NO core user (left join)
		if (!empty($dateBegin)) { $cmd->andWhere("lul.first_period_login >= :period_begin_date"); }
		if (!empty($dateEnd)) { $cmd->andWhere("lul.first_period_login <= :period_end_date"); }
        $activeUsers = $cmd->queryScalar($searchParams);

		return $activeUsers;
	}



	/**
	 * Sometimes some invalid records are found into "learning_userlimit_login_log" table (due to multiple inserts of the
	 * same user in the same billing period).
	 * This function will clean them.
	 */
	public static function activeUsersCleanUp() {

		//in every subscription period search for users with multiple entries, then make them single entry
		$periods = Billing::getSubscriptionPeriodDates();

		//add manually an initial period, prior to activation date (trial users are reported too)
		if (!empty($periods)) {
			//calculate date limit of first period
			$dateTmp = new DateTime($periods[0]['from']);
			$untilTo = $dateTmp->sub(new DateInterval('PT1S'))->format('Y-m-d H:i:s');  // substract 1 second
			//set initial period, not counted from Billing::getSubscriptionPeriodDates()
			$firstPeriod = array(
				array(
					'from' => '2000-01-01 00:00:00', //just a conventional date, prior to any saas service living
					'to' => $untilTo
				)
			);
			$periods = array_merge($firstPeriod, $periods);
		} else {
			//there are not valid periods yet, just count users until now
			$periods = array(
				array(
					'from' => '2000-01-01 00:00:00', //just a conventional date, prior to any saas service living
					'to' => Yii::app()->localtime->getUTCNow('Y-m-d H:i:s')
				)
			);
		}

		//do cleaning
		if (!empty($periods)) {

			foreach ($periods as $period) {

				//in current period, search user with multiple entries
				$params = array(
					':date_from' => $period['from'],
					':date_to' => $period['to']
				);
				$cmd = Yii::app()->db->createCommand("SELECT `userIdst`, count(`id`) as num "
					." FROM `learning_userlimit_login_log` "
					." WHERE `first_period_login` >= :date_from AND `first_period_login` <= :date_to "
					." GROUP BY `userIdst` "
					." HAVING num > 1");
				$reader = $cmd->query($params);

				//enlist found multiple entries users
				$multiples = array();
				if ($reader) {
					while ($record = $reader->read()) {
						$multiples[] = (int)$record['userIdst'];
					}
				}

				if (!empty($multiples)) {
					//process each retrieved multiple user
					foreach ($multiples as $multiple) {

						//for each multiple user found, retrieve all relative single records
						$params[':id_user'] = $multiple;
						$multipleCmd = Yii::app()->db->createCommand("SELECT `id`, `userIdst`, `first_period_login`  "
							." FROM `learning_userlimit_login_log` "
							." WHERE `userIdst` = :id_user "
							." AND `first_period_login` >= :date_from AND `first_period_login` <= :date_to");
						$multipleReader = $multipleCmd->query($params);
						if ($multipleReader) {
							$first = true;
							while ($toBeDeleted = $multipleReader->read()) {
								if ($first) {
									//the first record must be preserved
									$first = false;
								} else {
									//every other record is redundant and must be removed
									$deleteCmd = Yii::app()->db->createCommand("DELETE FROM `learning_userlimit_login_log` WHERE `id` = :id");
									$deleteCmd->execute(array(':id' => $toBeDeleted['id']));
								}
							}
						}
					}
				}

			}
		}
	}

}
