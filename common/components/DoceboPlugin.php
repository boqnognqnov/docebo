<?php
/**
 * DoceboPlugin class
 * Base class for Docebo Plugins based on the new framework (yii)
 */

class DoceboPlugin extends CComponent {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'dummy_plugin';
	
	// If this plugin/app has settings to configure
	public static $HAS_SETTINGS = true;
	//
	public static function settingsUrl(){}

	// Array used to store singleton of the plugin objects
	private static $plugin_obj_arr = array();

	/**
	 * @var bool The plugin should set this variable to true if the plugin should be loaded also on the mobile/ version
	 */
	public $is_mobile_capable = false;

	public function init() { // on init
		if ($this->plugin_name == 'dummy_plugin') {
			throw new Exception('Please extend the plugin_name static property!');
		}
	}


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return DoceboPlugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		if (!isset(self::$plugin_obj_arr[$class_name])) {
			self::$plugin_obj_arr[$class_name] = new $class_name;
		}

		return self::$plugin_obj_arr[$class_name];
	}


	/**
	 * This method can be used to change the default install popup
	 * If this method return false the default behaviour is forbidden and the popup will be empty.
	 * @param $app
	 * @param $lang
	 * @param $hasToContactResellerToInstall
	 * @param $isInTrialPeriod
	 * @param $cycle
	 * @return bool
	 */
	public static function installDetail($app, $lang, $hasToContactResellerToInstall, $isInTrialPeriod, $cycle) {
		return true;
	}

	public function run() {}

	public function listen() {}

	public function finalize() {}

	public function install() {}

	public function uninstall() {}

	public function path() {
		return _plugins_ . '/' . $this->plugin_name;
	}


	/**
	 * Activate / enable the current plugin for the installation
	 * @return bool
	 */
	public function activate() {
		$plugin_model = CorePlugin::model()->find('plugin_name=:name', array(':name'=>$this->plugin_name));

		if (!$plugin_model) {
			$plugin_model = new CorePlugin();
			$plugin_model->plugin_name = $this->plugin_name;
			$plugin_model->plugin_title = ucfirst($this->plugin_name);
		}

		$plugin_model->is_active = 1;
		$activated = $plugin_model->save();

		$pluginName = $this->plugin_name;

		// Also run migrations of this plugin now, if present:
		self::runMigrationsByAppName($pluginName);

		return $activated;
	}


	/**
	 * Deactivate / disabled the current plugin for the installation
	 * @return bool
	 */
	public function deactivate() {
		$res = true;
		$plugin_model = CorePlugin::model()->find('plugin_name=:name', array(':name'=>$this->plugin_name));

		if ($plugin_model) {
			$plugin_model->is_active = 0;
			$plugin_model->save();
		}

		return $res;
	}

	static public function runMigrationsByAppName($pluginName){
		$hasMigrations       = FALSE;
		$thisPluginMigrationsPath = FALSE;

		if( is_dir( Yii::getPathOfAlias( "plugin.{$pluginName}.migrations" ) ) ) {
			$hasMigrations       = TRUE;
			$thisPluginMigrationsPath = "plugin.{$pluginName}.migrations";
		} elseif( is_dir( Yii::getPathOfAlias( "custom_plugins.{$pluginName}.migrations" ) ) ) {
			$hasMigrations       = TRUE;
			$thisPluginMigrationsPath = "custom_plugins.{$pluginName}.migrations";
		}

		if( $hasMigrations ) {
			$runner      = new CConsoleCommandRunner();
			$commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
			$runner->addCommands( $commandPath );
			$args = array(
				'yiic-domain ' . Docebo::getOriginalDomain(),
				'migrate',
				// Everything else is optional parameters, appended to the migrate command:
				"--migrationPath={$thisPluginMigrationsPath}",
				"--interactive=0",
			);
			ob_start();
			$runner->run( $args );
			$result = htmlentities( ob_get_clean(), NULL, Yii::app()->charset );

			Yii::log('Applying migrations after activating '.$pluginName, CLogger::LEVEL_INFO);
		}
	}

}
