<?php

class StatsHelper extends CApplicationComponent {

	protected $_enableCache;
	protected $_cacheDuration;



	/**
	 * @return CDbCommand
	 */
	private function getDbCommand()
	{
		if ($this->_enableCache) {
			return Yii::app()->db->cache($this->_cacheDuration)->createCommand();
		} else {
			return Yii::app()->db->createCommand();
		}
	}

	private function getARmodel($classname, $queryCount = 1) {
		if ($this->_enableCache) {
			return $classname::model()->cache($this->_cacheDuration, null, $queryCount);
		} else {
			return $classname::model();
		}
	}

	public function getEnableCache() {
		return $this->_enableCache;
	}

	public function setEnableCache($value) {
		$this->_enableCache = $value;
		return true;
	}

	public function getCacheDuration() {
		return $this->_cacheDuration;
	}

	public function setCacheDuration($value) {
		$this->_cacheDuration = $value;
		return true;
	}



	/**
	 * @return array
	 */
	public function getDashboardFilters($datetime = false)
	{
		if($datetime)
		{
			$from = Yii::app()->session['dashboard_date_from'];
			$to = Yii::app()->session['dashboard_date_to'];

			if(!empty(Yii::app()->session['dashboard_date_from']))
				$from .= ' 00:00:00';
			if(!empty(Yii::app()->session['dashboard_date_to']))
				$to .= ' 23:59:59';

			return array(
				'date_from' => $from,
				'date_to' => $to
			);
		}
		else
			return array(
				'date_from' => Yii::app()->session['dashboard_date_from'],
				'date_to' => Yii::app()->session['dashboard_date_to']
			);
	}



	public function getDashboardStats() {
		$stats = array();

		$stats['alerts'] = $this->getAlertsStats();
		$stats['statuses'] = $this->getStatusesStats();
		$stats['course_completion'] = $this->getCourseCompletionStats();
		$stats['popular_courses'] = $this->getMostPopularCourses();
		$stats['courses_with_waitlist'] = LearningCourse::getCoursesWithWaitingUsers(true);

		$activities = $this->getRecentActivities();

		foreach ($activities as $key => &$activity) {
			$stats['legends'][$key] = $activity['legend'];
			$tempActivities[$key] = $activity['activity'];
			unset($activity['legend']);
		}

		foreach ($tempActivities as $key => $activity) {
			$i = 0;
			foreach ($activity as $row) {
				$out[$i]['date'] = $row['date'];
				$out[$i][$key] = $row['value'];
				$i++;
			}
		}

		$stats['activity'] = $out;
		$stats['titles'] = array(
			Yii::t('report', '_TH_USER_NUMBER_SESSION'),
			Yii::t('standard', 'Course completion'),
			Yii::t('standard', 'Enrollments'),
		);

		return $stats;
	}

	public function getDashboardCoachShareStats(){
		$stats = array();

		$stats['topWatchedAssets'] = $this->getTop3CoachShareMostWatchedAssets();
		$stats['topContributors'] = $this->getTop3CoachShareContributors();

		$activities = $this->getRecentCoachShareActivities();

		foreach ($activities as $key => &$activity) {
			$stats['legends'][$key] = $activity['legend'];
			$tempActivities[$key] = $activity['activity'];
			unset($activity['legend']);
		}

		foreach ($tempActivities as $key => $activity) {
			$i = 0;
			foreach ($activity as $row) {
				$out[$i]['date'] = $row['date'];
				$out[$i][$key] = $row['value'];
				$i++;
			}
		}

		$stats['activity'] = $out;
		$stats['titles'] = array(
			'questions' => Yii::t('standard', 'Questions made'),
			'answer' => Yii::t('standard', 'Answer given'),
			'share_asset' => Yii::t('standard', 'Share assets (contibutions)'),
			'asset_view' => Yii::t('standard', 'Asset views')
		);

		return $stats;
	}

	protected function prepareChartData($values) {
		$range = $this->getDbCommand()->select('DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 MONTH), "%Y-%m-%d") as `from`, DATE_FORMAT(NOW(), "%Y-%m-%d") as `to`')
			->from('learning_courseuser')
			->queryRow();

		$dates = $out = array();
		if (isset($range['from'])) {
			$i = 0;
			do {
				$date = date('Y-m-d', strtotime("-$i day"));
				$dates[] = $date;
				$i++;
			} while ($date != $range['from']);
		}

		foreach (array_reverse($dates) as $date) {
			$out[] = array(
				'date' => $date,
				'value' => isset($values[$date]) ? (int) $values[$date] : 0,
			);
		}

		if (!$out) { // flush some example data for plot
			for ($x = 0; $x < 31; $x++) {
				$out[] = array(
					'date' => date('Y-m-d', strtotime("-$x day")),
					'value' => 0,
				);
			}
		}

		return $out;
	}

	protected function revealTrends(&$data) {
		$data['dayTrendClass'] = '';
		$data['weekTrendClass'] = '';

		if ($data['today'] > $data['yesterday']) {
			$data['dayTrendClass'] = 'up';
		} elseif ($data['today'] < $data['yesterday']) {
			$data['dayTrendClass'] = 'down';
		}

		if ($data['thisWeek'] > $data['lastWeek']) {
			$data['weekTrendClass'] = 'up';
		} elseif ($data['thisWeek'] < $data['lastWeek']) {
			$data['weekTrendClass'] = 'down';
		}
	}

	/**
	 * Set the correct scenario for the course model
	 * @return LearningCourse
	 */
	protected function getCourseModel()
	{
		$courseModel = new LearningCourse();
		if(isset($courseModel->course_type)){
			unset($courseModel->course_type);
		}

		if (Yii::app()->user->getIsPu()) {
			// exclude the courses that are not assigned to the current power user
			$courseModel->setScenario('power_user_assigned');
			$courseModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
		}

		return $courseModel;
	}




	protected function getAlertsStats()
	{
		$out = array();

		$isPowerUser = Yii::app()->user->getIsPu();
		$courseModel = $this->getCourseModel();
		$filters = $this->getDashboardFilters(true);
		$noDateFilters = (empty($filters['date_from']) && empty($filters['date_to']));

		$allowedCoursesIds = Yii::app()->db->createCommand()
		->select('idCourse')
		->from(LearningCourse::model()->tableName())
		->queryColumn($courseModel->dataProvider(false, false, true)->getCriteria());

		$allowedDateCoursesIds = array();
		$dateFilter = false;
		if(!empty($filters['date_from']) || !empty($filters['date_to'])){
			$dateIdsCommand = Yii::app()->db->createCommand();
			$dateIdsCommand->where(array('in', 'c.idCourse', $allowedCoursesIds));
			if(!empty($filters['date_from']) && empty($filters['date_to'])){
				$dateIdsCommand->andWhere(array('and', 'create_date >= :date_from'));
				$dateIdsCommand->params[':date_from'] = $filters['date_from'];
			}elseif(empty($filters['date_from']) && !empty($filters['date_to'])){
				$dateIdsCommand->andWhere(array('and', 'create_date <= :date_to'));
				$dateIdsCommand->params[':date_to'] = $filters['date_to'];
			}elseif(!empty($filters['date_from']) && !empty($filters['date_to'])){
				$dateIdsCommand->andWhere(array('and', 'create_date >= :date_from'));
				$dateIdsCommand->andWhere(array('and', 'create_date <= :date_to'));
				$dateIdsCommand->params[':date_from'] = $filters['date_from'];
				$dateIdsCommand->params[':date_to'] = $filters['date_to'];
			}
			$dateFilter = true;
			$dateIdsCommand
				->selectDistinct('c.idCourse')
				->from('learning_course c');

			$allowedDateCoursesIds = $dateIdsCommand->queryColumn();
		}

		$enrollmentsCommand = $this->getDbCommand();
		$enrollmentsCommand->where(array('in', 'cu.idCourse', $allowedCoursesIds));

		// applying date filters
		if (!empty($filters['date_from'])) {
			$enrollmentsCommand->andWhere(array('and', 'date_inscr >= :date_from'));
			$enrollmentsCommand->params[':date_from'] = $filters['date_from'];
		}

		if (!empty($filters['date_to'])) {
			$enrollmentsCommand->andWhere(array('and', 'date_inscr <= :date_to'));
			$enrollmentsCommand->params[':date_to'] = $filters['date_to'];
		}

		/* @var $enrollmentsCommand CDbCommand */
		$enrollmentsCommand
			->selectDistinct('cu.idCourse')
			->from('learning_courseuser cu')
			->join('learning_course c', 'c.idCourse=cu.idCourse');

		if ($isPowerUser)
			$enrollmentsCommand->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));

		$notEmptyEnrollments = $enrollmentsCommand->queryColumn();

		/**
		 * Find courses with no enrollments
		 */
		$emptyEnrollmentsCriteria = $courseModel->dataProvider(false, false, true)->getCriteria();
		$emptyEnrollmentsCriteria->addNotInCondition('t.idCourse', $notEmptyEnrollments);
		if($dateFilter){
			$emptyEnrollmentsCriteria->addInCondition('t.idCourse', $allowedDateCoursesIds);
		}

		$out['emptyEnrollments'] = LearningCourse::model()->count($emptyEnrollmentsCriteria);

		/**
		 * Find courses with no completion
		 * We ignore the from filter here, because a completed course before "from" is still completed after "from".
		 */
		$enrollmentsCommand = $this->getDbCommand();
		$enrollmentsCommand->where(array('in', 'cu.idCourse', $allowedCoursesIds));
		$enrollmentsCommand
			->selectDistinct('cu.idCourse')
			->from('learning_courseuser cu')
			->join('learning_course c', 'c.idCourse=cu.idCourse');
		if ($isPowerUser)
			$enrollmentsCommand->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id=:idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
		$notEmptyEnrollments = $enrollmentsCommand->queryColumn();

		$noCompletionDataCommand = Yii::app()->db->createCommand()
		->selectDistinct('idCourse')
		->from(LearningCourseuser::model()->tableName());

		$noCompletionDataCommand->andWhere('date_complete IS NOT NULL');
		if($notEmptyEnrollments && !empty($notEmptyEnrollments))
			$noCompletionDataCommand->andWhere(array('IN', 'idCourse', $notEmptyEnrollments));
		else
			$noCompletionDataCommand->andWhere('1=0');

		if($isPowerUser)
		{
			$noCompletionDataCommand->join(CoreUserPU::model()->tableName().' AS cup', 'idUser=cup.user_id AND cup.puser_id=:idPowerUser', array(':idPowerUser' => Yii::app()->user->id))
			->join(CoreUserPuCourse::model()->tableName().' AS cupc', 'idCourse=cupc.course_id AND cupc.puser_id=:idPowerUser', array(':idPowerUser' => Yii::app()->user->id));
		}

		$noCompletionData = $noCompletionDataCommand->queryColumn();

		$noCompletionCoursesCommand = Yii::app()->db->createCommand()
			->select('COUNT(c.idCourse)')
			->from(LearningCourse::model()->tableName().' c');

		if($isPowerUser) {
			$noCompletionCoursesCommand->join(CoreUserPuCourse::model()->tableName(). ' pc', 'pc.puser_id=:puser_id AND pc.course_id=c.idCourse', array(
				':puser_id' => Yii::app()->user->id
			));
		}

		if($noCompletionData && !empty($noCompletionData))
			$noCompletionCoursesCommand->andWhere(array('NOT IN', 'c.idCourse', $noCompletionData));
		if($dateFilter)
			if($allowedDateCoursesIds && !empty($allowedDateCoursesIds))
				$noCompletionCoursesCommand->andWhere(array('IN', 'c.idCourse', $allowedDateCoursesIds));
			else
				$noCompletionCoursesCommand->andWhere('1=0');


		$out['noCompletion'] = $noCompletionCoursesCommand->queryScalar();

		/**
		 * Find courses with no training resources
		 */
		$noTrainingResourcesCommand = Yii::app()->db->createCommand()
			->select('COUNT(DISTINCT c.idCourse)')
			->from(LearningCourse::model()->tableName(). ' c')
			->leftJoin(LearningOrganization::model()->tableName(). ' lo', 'lo.idCourse = c.idCourse AND lo.objectType <> ""');

		if($allowedCoursesIds && !empty($allowedCoursesIds))
			$noTrainingResourcesCommand->andWhere(array('IN', 'c.idCourse', $allowedCoursesIds));
		else
			$noTrainingResourcesCommand->andWhere('1=0');

		if($isPowerUser) {
			$noTrainingResourcesCommand->join(CoreUserPuCourse::model()->tableName(). ' pc', 'pc.puser_id=:puser_id AND pc.course_id=c.idCourse', array(
				':puser_id' => Yii::app()->user->id
			));
		}

		if ($noDateFilters) {
			$noTrainingResourcesCommand->andWhere('lo.idOrg IS NULL');
			$out['noTrainingResources'] = $noTrainingResourcesCommand->queryScalar();
		} else {
			$excludedCourses = Yii::app()->db->createCommand()
				->selectDistinct('idCourse')
				->from(LearningOrganization::model()->tableName())
				->where(array('in', 'idCourse', $allowedCoursesIds))
				->andWhere('objectType != ""')
				->queryColumn();

			if($excludedCourses && !empty($excludedCourses))
				$noTrainingResourcesCommand->andWhere(array('NOT IN', 'c.idCourse', $excludedCourses));

			if($dateFilter){
				if($allowedDateCoursesIds && !empty($allowedDateCoursesIds))
					$noTrainingResourcesCommand->andWhere(array('IN', 'c.idCourse', $allowedDateCoursesIds));
				else
					$noTrainingResourcesCommand->andWhere('1=0');
			}

			$out['noTrainingResources'] = $noTrainingResourcesCommand->queryScalar();
		}

		return $out;
	}



	protected function getStatusesStats() {
		$out = array(
			'users' => array(),
			'courses' => array(),
		);

		$isPowerUser = Yii::app()->user->getIsPu();
		$courseModel = $this->getCourseModel();
		$filters = $this->getDashboardFilters(true);

		$condition = new CDbCriteria();
		$studentsCondition = new CDbCriteria();

		if ($isPowerUser) {
			$condition->with['powerUserManagers'] = array(
				'joinType' => 'INNER JOIN',
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => Yii::app()->user->id),
			);
			$condition->together = true;
			$condition->group = 't.idCourse';
		}

		// applying date filters
		if (!empty($filters['date_from'])) {
			$condition->addCondition('create_date >= :date_from');
			$condition->params[':date_from'] = $filters['date_from'];
			$studentsCondition->addCondition('date_inscr >= :date_from');
			$studentsCondition->params[':date_from'] = $filters['date_from'];
		}

		if (!empty($filters['date_to'])) {
			$condition->addCondition('create_date <= :date_to');
			$condition->params[':date_to'] = $filters['date_to'];
			$studentsCondition->addCondition('date_inscr <= :date_to');
			$studentsCondition->params[':date_to'] = $filters['date_to'];
		}

		$coursesCount = $courseModel->count($condition);
		$active = $courseModel->active()->count($condition);

		//$out['courses']['closed'] 			= $coursesCount  ? floor($completed / $coursesCount * 100) : 0;
		$out['courses']['active'] 			= $coursesCount  ? floor(($active / $coursesCount) * 100) 	: 0;
		$out['courses']['under_maintenance'] = $coursesCount ? (100 - $out['courses']['active']) : 0;

		$usersCount =
			$isPowerUser
				? $this->getARmodel('LearningCourseuser', 3)->powerUserAssigned(Yii::app()->user->id)->students()->count($studentsCondition)
				: $this->getARmodel('LearningCourseuser', 3)->students()->count($studentsCondition);
		$notStarted =
			$isPowerUser
				? LearningCourseuser::model()->powerUserAssigned(Yii::app()->user->id)->students()->notStarted()->count($studentsCondition)
				: LearningCourseuser::model()->students()->notStarted()->count($studentsCondition);
		$inProgress =
			$isPowerUser
				? LearningCourseuser::model()->powerUserAssigned(Yii::app()->user->id)->students()->inProgress()->count($studentsCondition)
				: LearningCourseuser::model()->students()->inProgress()->count($studentsCondition);

		$out['users']['not_yet_started'] 	= ($usersCount ? floor($notStarted / $usersCount * 100) : 0);
		$out['users']['in_progress'] 		= ($usersCount ? floor($inProgress / $usersCount * 100) : 0);
		$out['users']['completed'] 			= ($usersCount ? (100 - $out['users']['not_yet_started'] - $out['users']['in_progress']) : 0);

		return $out;
	}



	protected function getCourseCompletionStats() {

		$isPowerUser = Yii::app()->user->getIsPu();
		$filters = $this->getDashboardFilters(true);
		$out = array('most' => array(), 'least' => array());

		for ($i = 0; $i < 2; $i++) {
			/* @var $command CDbCommand */
			$command = $this->getDbCommand()
				->select('c.name, cu.idCourse, COUNT(*) as totalCount, SUM(IF(cu.status = :status, 1, 0)) as completedCount, SUM(IF(cu.status = :status, 1, 0)) / COUNT(*) as orderField')
				->from('learning_courseuser cu')
				->join('learning_course c', 'c.idCourse = cu.idCourse')
				->limit(3)
				->group('cu.idCourse');

			if (!empty($filters['date_from'])) {
				$command->andWhere('(cu.date_complete >= :date_from OR cu.date_complete IS NULL)', array(':date_from' => $filters['date_from']));
			}
			if (!empty($filters['date_to'])) {
				$command->andWhere('(cu.date_complete <= :date_to OR cu.date_complete IS NULL)', array(':date_to' => $filters['date_to']));
			}

			if ($i == 0) {
				$command->order('orderField DESC, c.name ASC');
				$key = 'most';
			} else {
				$command->order('orderField ASC, c.name ASC');
				$key = 'least';
			}

			if ($isPowerUser) {
				// filter by assigned users
				$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
				// filter by assigned courses
				$courseModel = $this->getCourseModel();
				$assignedCourses = CHtml::listData($courseModel->dataProvider(false, false, true)->getData(), 'idCourse', 'idCourse');
				$command->andWhere(array('in', 'c.idCourse', $assignedCourses));
			}

			$command->params[':status'] = LearningCourseuser::$COURSE_USER_END;

			foreach ($command->queryAll() as $row) {
				$out[$key][] = array(
					'title' => $row['name'],
					'total' => $row['totalCount'],
					'completed' => $row['completedCount'],
					'percent' => explode('.', sprintf("%1.2f", $row['completedCount'] / $row['totalCount'] * 100)),
				);
			}
		}

		return $out;
	}

	protected function getMostPopularCourses() {

		$isPowerUser = Yii::app()->user->getIsPu();

		$command = $this->getDbCommand()
			->select('c.name, cu.idCourse, COUNT(*) as totalCount,
				SUM(IF(cu.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW(), 1, 0)) as thisWeek,
				SUM(IF(cu.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 2 WEEK) AND DATE_SUB(NOW(), INTERVAL 1 WEEK), 1, 0)) as lastWeek')
			->from('learning_courseuser cu')
			->join('learning_course c', 'c.idCourse = cu.idCourse')
			->join('core_user u', 'u.idst = cu.idUser')
			->order('totalCount DESC')
			->group('cu.idCourse')
			->limit(3);

		if ($isPowerUser) {
			// filter by assigned users
			$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
			// filter by assigned courses
			$courseModel = $this->getCourseModel();
			$assignedCourses = CHtml::listData($courseModel->dataProvider(false, false, true)->getData(), 'idCourse', 'idCourse');
			$command->andWhere(array('in', 'c.idCourse', $assignedCourses));
		}

		$courses = $command->queryAll();

		// We have 3 positions/levels a cours may fall under:
		// 1. The one with most number of enrollments
		// 2. Second by number of enrollments
		// 3. Third
		// Note that two or more courses may share the same position
		// if they have equal enrollments. That's why this machanism is put in place
		$positionInTopThree = 1;

		// Used for caching the previous course in the loop below
		$previousCourse = FALSE;

		if (!empty($courses)) {
			foreach ($courses as &$course) {
				if ($course['thisWeek'] > $course['lastWeek']) {
					$course['trendClass'] = 'up';
				} elseif ($course['thisWeek'] < $course['lastWeek']) {
					$course['trendClass'] = 'down';
				} else {
					$course['trendClass'] = '';
				}

				// Current course has less enrollments than previous one.
				// Move it one position below
				if($previousCourse && $previousCourse['totalCount'] > $course['totalCount']){
					$positionInTopThree++;
				}

				$course['positionInTopThree'] = $positionInTopThree;

				// Cache current course for next iteration for easier comparision
				$previousCourse = $course;
			}
		}

		return $courses;
	}

	protected function getRecentActivities() {
		$out = array();

		$out['sessions'] = $this->getNumberOfSessions();
		$out['completions'] = $this->getCourseCompletions();
		$out['enrollments'] = $this->getNumberOfEnrollments();

		return $out;
	}

	protected function getRecentCoachShareActivities() {
		$out = array();
		//TODO
        $out['questions'] = $this->getQuestionStatistic();
        $out['answer'] = $this->getAnswerStatistic();
        $out['share_asset'] = $this->getShareAssetStatistic();
        $out['asset_view'] = $this->getAssetViewsStatistic();
		return $out;
	}



	protected function getNumberOfSessions() {

		$isPowerUser = Yii::app()->user->getIsPu();
		$assignedCourses = null;

		$command = $this->getDbCommand()
            ->select('
				SUM(IF(ts.lastTime BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW(), 1, 0)) as thisWeek,
				SUM(IF(ts.lastTime BETWEEN DATE_SUB(NOW(), INTERVAL 2 WEEK) AND DATE_SUB(NOW(), INTERVAL 1 WEEK), 1, 0)) as lastWeek,
				SUM(IF(DATE(NOW())=DATE(ts.lastTime), 1, 0)) as today,
				SUM(IF(ts.lastTime BETWEEN DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00") AND DATE_SUB(DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00"), INTERVAL 1 DAY), 1, 0)) as yesterday')
            ->from('learning_tracksession ts');

        if ($isPowerUser) {
            // filter by assigned users
            $command->join('core_user_pu cup', '( (ts.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
            // filter by assigned courses
            $courseModel = $this->getCourseModel();
            $assignedCourses = CHtml::listData($courseModel->dataProvider(false, false, true)->getData(), 'idCourse', 'idCourse');
            $command->andWhere(array('in', 'ts.idCourse', $assignedCourses));
        }

        $sessions = $command->queryRow();
        $this->revealTrends($sessions);

        $command = $this->getDbCommand()
            ->select('DATE_FORMAT(ts.lastTime, "%Y-%m-%d") as date, COUNT(*) as totalCount')
            ->from('learning_tracksession ts')
            ->where('ts.lastTime BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW()')
            ->group('DATE_FORMAT(ts.lastTime, "%Y-%m-%d")');

        if ($isPowerUser) {
            // filter by assigned users
            $command->join('core_user_pu cup', '( (ts.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
            // filter by assigned courses
            $command->andWhere(array('in', 'ts.idCourse', $assignedCourses));
        }

        $sessionsActivity = $command->queryAll();

        foreach ($sessionsActivity as $session) {
            $thisMonthSessions[$session['date']] = $session['totalCount'];
        }
        return array(
            'legend' => $sessions,
            'activity' => $this->prepareChartData($thisMonthSessions),
        );
	}



	protected function getCourseCompletions() {

		$isPowerUser = Yii::app()->user->getIsPu();
		$assignedCourses = null;

		$command = $this->getDbCommand()
			->select('
				SUM(IF(cu.date_complete BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW(), 1, 0)) as thisWeek,
				SUM(IF(cu.date_complete BETWEEN DATE_SUB(NOW(), INTERVAL 2 WEEK) AND DATE_SUB(NOW(), INTERVAL 1 WEEK), 1, 0)) as lastWeek,
				SUM(IF(cu.date_complete BETWEEN DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00") AND NOW(), 1, 0)) as today,
				SUM(IF(cu.date_complete BETWEEN DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00") AND DATE_SUB(DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00"), INTERVAL 1 DAY), 1, 0)) as yesterday')
			->from('learning_courseuser cu')
			->where('cu.status = :status');

		$command->params[':status'] = LearningCourseuser::$COURSE_USER_END;

		if ($isPowerUser) {
			// filter by assigned users
			$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
			// filter by assigned courses
			$courseModel = $this->getCourseModel();
			$assignedCourses = CHtml::listData($courseModel->dataProvider(false, false, true)->getData(), 'idCourse', 'idCourse');
			$command->andWhere(array('in', 'cu.idCourse', $assignedCourses));
		}

		$completions = $command->queryRow();
		$this->revealTrends($completions);

		$command = $this->getDbCommand()
			->select('DATE_FORMAT(cu.date_complete, "%Y-%m-%d") as date, COUNT(*) as totalCount')
			->from('learning_courseuser cu')
			->where('cu.date_complete BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW()')
			->group('DATE_FORMAT(cu.date_complete, "%Y-%m-%d")');

		if ($isPowerUser) {
			// filter by assigned users
			$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
			// filter by assigned courses
			$command->andWhere(array('in', 'cu.idCourse', $assignedCourses));
		}

		$completionsActivity = $command->queryAll();

		foreach ($completionsActivity as $completion) {
			$thisMonthCompletions[$completion['date']] = $completion['totalCount'];
		}

		return array(
			'legend' => $completions,
			'activity' => $this->prepareChartData($thisMonthCompletions),
		);
	}



	protected function getNumberOfEnrollments() {

		$isPowerUser = Yii::app()->user->getIsPu();
		$assignedCourses = null;

		$command = $this->getDbCommand()
			->select('
				SUM(IF(cu.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW(), 1, 0)) as thisWeek,
				SUM(IF(cu.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 2 WEEK) AND DATE_SUB(NOW(), INTERVAL 1 WEEK), 1, 0)) as lastWeek,
				SUM(IF(cu.date_inscr BETWEEN DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00") AND NOW(), 1, 0)) as today,
				SUM(IF(cu.date_inscr BETWEEN DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00") AND DATE_SUB(DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00"), INTERVAL 1 DAY), 1, 0)) as yesterday')
			->from('learning_courseuser cu');

		if ($isPowerUser) {
			// filter by assigned users
			$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
			// filter by assigned courses
			$courseModel = $this->getCourseModel();
			$assignedCourses = CHtml::listData($courseModel->dataProvider(false, false, true)->getData(), 'idCourse', 'idCourse');
			$command->andWhere(array('in', 'cu.idCourse', $assignedCourses));
		}

		$enrollments = $command->queryRow();
		$this->revealTrends($enrollments);

		$command = $this->getDbCommand()
			->select('DATE_FORMAT(cu.date_inscr, "%Y-%m-%d") as date, COUNT(*) as totalCount')
			->from('learning_courseuser cu')
			->where('cu.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 1 MONTH) AND NOW()')
			->group('DATE_FORMAT(cu.date_inscr, "%Y-%m-%d")');

		if ($isPowerUser) {
			// filter by assigned users
			$command->join('core_user_pu cup', '( (cu.idUser=cup.user_id) AND (cup.puser_id = :idPowerUser) )', array(':idPowerUser' => Yii::app()->user->id));
			// filter by assigned courses
			$command->andWhere(array('in', 'cu.idCourse', $assignedCourses));
		}

		$enrollmentsActivity = $command->queryAll();

		foreach ($enrollmentsActivity as $enrollment) {
			$thisMonthEnrollments[$enrollment['date']] = $enrollment['totalCount'];
		}

		return array(
			'legend' => $enrollments,
			'activity' => $this->prepareChartData($thisMonthEnrollments),
		);
	}

    protected function getQuestionStatistic()
    {
        $command = $this->getDbCommand()
            ->select('SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK)
                                AND NOW(), 1, 0
                            )
                    ) as thisWeek,

                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_SUB(NOW(),
                            INTERVAL 2 WEEK
                            )
                            AND DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK
                                ), 1, 0)
                            ) as lastWeek,
                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                            ) AND NOW(), 1, 0)
                        ) as today,
                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                                ) AND DATE_SUB(
                                    DATE_FORMAT(
                                        NOW(), "%Y-%m-%d 00:00:00"
                                    ),
                                INTERVAL 1 DAY
                            ), 1, 0
                        )
                    ) as yesterday')
            ->from('app7020_question cu');

        $questions = $command->queryRow();
        $this->revealTrends($questions);

        $command = $this->getDbCommand()
            ->select('DATE_FORMAT(cu.created, "%Y-%m-%d")as date, COUNT(*) as totalCount')
            ->from('app7020_question cu')
            ->where('cu.created BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) AND NOW()')
            ->group('DATE_FORMAT(cu.created, "%Y-%m-%d")');

        $questionActivity = $command->queryAll();

        foreach ($questionActivity as $question) {
            $thisMonthQuestions[$question['date']] = $question['totalCount'];
        }

        return array(
            'legend' => $questions,
            'activity' => $this->prepareChartData($thisMonthQuestions),
        );
    }

    protected function getAnswerStatistic()
    {
        $command = $this->getDbCommand()
            ->select('SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK)
                                AND NOW(), 1, 0
                            )
                    ) as thisWeek,

                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_SUB(NOW(),
                            INTERVAL 2 WEEK
                            )
                            AND DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK
                                ), 1, 0)
                            ) as lastWeek,
                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                            ) AND NOW(), 1, 0)
                        ) as today,
                    SUM(
                        IF(
                            cu.created
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                                ) AND DATE_SUB(
                                    DATE_FORMAT(
                                        NOW(), "%Y-%m-%d 00:00:00"
                                    ),
                                INTERVAL 1 DAY
                            ), 1, 0
                        )
                    ) as yesterday')
            ->from('app7020_answer cu');

        $answers = $command->queryRow();
        $this->revealTrends($answers);

        $command = $this->getDbCommand()
            ->select('DATE_FORMAT(cu.created, "%Y-%m-%d")as date, COUNT(*) as totalCount')
            ->from('app7020_answer cu')
            ->where('cu.created BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) AND NOW()')
            ->group('DATE_FORMAT(cu.created, "%Y-%m-%d")');

        $answerActivity = $command->queryAll();

        foreach ($answerActivity as $answer) {
            $thisMonthAnswers[$answer['date']] = $answer['totalCount'];
        }

        return array(
            'legend' => $answers,
            'activity' => $this->prepareChartData($thisMonthAnswers),
        );
    }

    protected function getAssetViewsStatistic()
    {
        $thisWeek = 'SELECT  a.*
		FROM    app7020_content_history a
			   INNER JOIN
			   (
				   SELECT idUser, idContent,  MIN(viewed) min_val
				   FROM    app7020_content_history
				   GROUP   BY idContent, idUser
			   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
					   a.viewed = b.min_val
      WHERE a.viewed BETWEEN DATE_SUB(
			NOW(), INTERVAL 1 WEEK
		) AND NOW()';
        $lastWeek = 'SELECT  a.*
		FROM    app7020_content_history a
			   INNER JOIN
			   (
				   SELECT idUser, idContent,  MIN(viewed) min_val
				   FROM    app7020_content_history
				   GROUP   BY idContent, idUser
			   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
					   a.viewed = b.min_val
WHERE a.viewed  BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                            ) AND NOW()';

		$today = '
		SELECT  a.*
		FROM    app7020_content_history a
			   INNER JOIN
			   (
				   SELECT idUser, idContent,  MIN(viewed) min_val
				   FROM    app7020_content_history
				   GROUP   BY idContent, idUser
			   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
					   a.viewed = b.min_val
		WHERE a.viewed  BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                            ) AND NOW()
		';
        $yesterday = 'SELECT  a.*
		FROM    app7020_content_history a
			   INNER JOIN
			   (
				   SELECT idUser, idContent,  MIN(viewed) min_val
				   FROM    app7020_content_history
				   GROUP   BY idContent, idUser
			   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
					   a.viewed = b.min_val
WHERE a.viewed  BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                                ) AND DATE_SUB(
                                    DATE_FORMAT(
                                        NOW(), "%Y-%m-%d 00:00:00"
                                    ),
                                INTERVAL 1 DAY
                            )';

        $thisWeekCount = count(Yii::app()->db->createCommand($thisWeek)->queryAll());
        $lastWeekCount = count(Yii::app()->db->createCommand($lastWeek)->queryAll());
        $todayCount = count(Yii::app()->db->createCommand($today)->queryAll());
        $yesterdayCount = count(Yii::app()->db->createCommand($yesterday)->queryAll());
        $views = array(
            'thisWeek' => $thisWeekCount,
            'lastWeek' => $lastWeekCount,
            'today' => $todayCount,
            'yesterday' => $yesterdayCount
        );
        $this->revealTrends($views);

        /// */**************
		$sql = '
			SELECT  a.idContent,a.idUser,DATE_FORMAT(a.viewed, "%Y-%m-%d") as date
				FROM    app7020_content_history a
					   INNER JOIN
					   (
						   SELECT idUser, idContent,  MIN(viewed) min_val
						   FROM    app7020_content_history
						   GROUP BY idContent, idUser
					   ) b  ON a.idUser = b.idUser AND a.idContent = b.idContent AND
							   a.viewed = b.min_val
		WHERE a.viewed BETWEEN (NOW() - INTERVAL 1 MONTH) AND NOW()
		ORDER BY date DESC
		';


        $command2= Yii::app()->db->createCommand($sql);

        $viewActivity = $command2->queryAll();

		$thisMonthViews = array();
        foreach ($viewActivity as $view) {
            $thisMonthViews[$view['date']] =  $thisMonthViews[$view['date']] + 1;
        }

        return array(
            'legend' => $views,
            'activity' => $this->prepareChartData($thisMonthViews),
        );
    }

    protected function getShareAssetStatistic()
    {
        $command = $this->getDbCommand()
            ->select('SUM(
                        IF(
                            cp.datePublished
                            BETWEEN DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK)
                                AND NOW(), 1, 0
                            )
                    ) as thisWeek,

                    SUM(
                        IF(
                            cp.datePublished
                            BETWEEN DATE_SUB(NOW(),
                            INTERVAL 2 WEEK
                            )
                            AND DATE_SUB(
                                NOW(),
                                INTERVAL 1 WEEK
                                ), 1, 0)
                            ) as lastWeek,
                    SUM(
                        IF(
                            cp.datePublished
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                            ) AND NOW(), 1, 0)
                        ) as today,
                    SUM(
                        IF(
                            cp.datePublished
                            BETWEEN DATE_FORMAT(
                                NOW(), "%Y-%m-%d 00:00:00"
                                ) AND DATE_SUB(
                                    DATE_FORMAT(
                                        NOW(), "%Y-%m-%d 00:00:00"
                                    ),
                                INTERVAL 1 DAY
                            ), 1, 0
                        )
                    ) as yesterday')
            ->from('app7020_content cu')
            ->leftJoin('app7020_content_published cp', 'cp.idContent = cu.id')
            ->where('cp.actionType = 1');

        $contributions = $command->queryRow();
        $this->revealTrends($contributions);

        $command = $this->getDbCommand()
            ->select('DATE_FORMAT(cp.datePublished, "%Y-%m-%d")as date, COUNT(*) as totalCount')
            ->from('app7020_content cu')
            ->leftJoin('app7020_content_published cp', 'cp.idContent = cu.id')
			->where('cp.actionType = 1')
            ->andWhere('cp.datePublished BETWEEN DATE_SUB(NOW(), INTERVAL 1 YEAR) AND NOW()')
            ->group('DATE_FORMAT(cp.datePublished, "%Y-%m-%d")');

        $contributionActivity = $command->queryAll();

        foreach ($contributionActivity as $contribution) {
            $thisMonthContributions[$contribution['date']] = $contribution['totalCount'];
        }

        return array(
            'legend' => $contributions,
            'activity' => $this->prepareChartData($thisMonthContributions),
        );
    }

    protected function getTop3CoachShareMostWatchedAssets()
    {
        $filters = $this->getDashboardFilters(true);
        $command = $this->getDbCommand()
            ->select('idContent as id, count(DISTINCT idUser) as top')
            ->from(App7020ContentHistory::model()->tableName() . ' ch');
        if (!empty($filters['date_from']) || !empty($filters['date_to'])) {
            if (!empty($filters['date_from'])) {
                $command->andWhere('(ch.viewed >= :date_from)', array(':date_from' => $filters['date_from']));
            }
            if (!empty($filters['date_to'])) {
                $command->andWhere('(ch.viewed <= :date_to)', array(':date_to' => $filters['date_to']));
            }
        } else {
            $command->andWhere('ch.viewed');
        }
        $command->group('ch.idContent')
            ->order('top DESC')->limit('3');
        $results = $command->queryAll();

        foreach ($results as $key => $result) {
            $assetObject[$key]['id'] = $result['id'];
            $assetObject[$key]['thumb'] = App7020Assets::getAssetThumbnailUri($result['id']);
            $assetObject[$key]['totalCount'] = $result['top'];
            $assetObject[$key]['title'] = App7020Content::model()->findByPk($result['id'])->title;

        }
        return $assetObject;

    }


    protected function getTop3CoachShareContributors()
    {
        $filters = $this->getDashboardFilters(true);
        $command = $this->getDbCommand()
            ->select('c.userId, count(*) as top')
            ->from(App7020Content::model()->tableName() . ' c')
            ->leftjoin(App7020ContentPublished::model()->tableName() . ' cp', 'cp.idContent = c.id')
            ->where('cp.actionType = 1');
            if (!empty($filters['date_from']) || !empty($filters['date_to'])) {
                if (!empty($filters['date_from'])) {
                    $command->andWhere('(cp.datePublished >= :date_from)', array(':date_from' => $filters['date_from']));
                }
                if (!empty($filters['date_to'])) {
                    $command->andWhere('(cp.datePublished <= :date_to)', array(':date_to' => $filters['date_to']));
                }
            }
       $command->group('userId')
        ->order('top DESC')
        ->limit('3');
        $results = $command->queryAll();
        foreach ($results as $key => $result) {
            $userObject[$key]['id'] = $result['userId'];
            $userObject[$key]['avatar'] = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => $result['userId']), true);
            $userObject[$key]['totalCount'] = $result['top'];
            $userObject[$key]['name'] = CoreUser::getForamattedNames($result['userId'],' ', false);
        }
        return $userObject;

    }
}

