<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

/**
 * Class DoceboMainMenu
 * This componet is used in order to collect and manipulate the mainmenu, the info saved in this class will then be
 * user by the MainMenu widget to render things
 */
class DoceboMainMenu extends CComponent {

	protected $apps_items = array();


	/**
	 * Initialize the component
	 */
	public function init() {}


	/**
	 * Add an app menu to the main menu
	 * @param $app_id
	 * @param $main_element
	 * @param $items
	 */
	public function addAppMenu($app_id, $main_element, $items) {
		if (!isset($main_element['settings']))
			$main_element['settings'] = false;

		// Set icon only if not provided by the app itself and without the initials (e.g. VI for Vivocha)
		if(!$main_element['app_icon'] && !$main_element['app_initials'])
			$main_element['app_icon'] = 'fa-gear';

		$this->apps_items[$app_id] = array(
			'id' => $app_id,
			'main_element' => $main_element,
			'items' => $items,
		);
	}


	/**
	 * Return the menu apps info
	 * @return array
	 */
	public function getAppsItems() {
		uasort($this->apps_items, function($a, $b) {
			return strcmp($a['main_element']['label'], $b['main_element']['label']);
		});
		return $this->apps_items;
	}

}