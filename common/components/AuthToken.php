<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 *
 *
 * This class will handle the temporary token that can be generated on a user base to sign external api call,
 * it's used by the mobile app and by the scorm player to authorize the request that comes from the DCD
 */
class AuthToken extends CComponent {


	const MAX_AGE = 1800;


	public $id_user = false;


	public function __construct($id_user) {

		$this->id_user = $id_user;
	}

	public function getLifeTime() {
		if(Settings::get('rest_auth_lifetime'))
			// the setting is in minute !
			$lifeTime = Settings::get('rest_auth_lifetime') * 60;
		else
			$lifeTime = self::MAX_AGE;

		// boundaries, no less than 30 min, no more than 4 hours
		if ($lifeTime < 60*30) $lifeTime = 60*30;
		if ($lifeTime > 60*60*3) $lifeTime = 60*60*3;

		return $lifeTime;
	}

	/**
	 * Generate an auth token
	 * 
	 * @return string|boolean   Either a valid token or false in case of failure to create one
	 */
	public function generate() {

		// Let's search for a valid token for this user and delete it if exists
		$cra = CoreRestAuthentication::model()->find('id_user = :id_user', array(
			':id_user' => $this->id_user
		));
		if ($cra) {
			$cra->delete();
		}

		// Create a brand new token
		$cra = new CoreRestAuthentication();
		$cra->id_user 			= $this->id_user;
		$cra->token 			= Docebo::generateUuid();

		$lifeTime = $this->getLifeTime();

		// Set default timezone to UTC (because of strtotime())
		$currentTimeZone = date_default_timezone_get();
		date_default_timezone_set('UTC');

		$cra->generation_date 	= date("Y-m-d H:i:s");
		// time is returned in UTC, so the expire is that plus the lifetime
		$cra->expiry_date       = date("Y-m-d H:i:s", time() + $lifeTime);

		// Set back the TZ
		date_default_timezone_set($currentTimeZone);

		if ($cra->save()) {
			return $cra->token;
		}
		return false;
	}


	public function get($auto_generate = true) {

		// Search for a token associated with this user
		$cra = CoreRestAuthentication::model()->find('id_user = :id_user', array(
			':id_user' => $this->id_user
		));

		// Check if a token exists
		if (!$cra) {
			if ($auto_generate) return $this->generate();
			else return false;
		}

        // Set default timezone to UTC (because of strtotime())
        $currentTimeZone = date_default_timezone_get();
        date_default_timezone_set('UTC');

		if ( (!$cra->expiry_date) || (time() > strtotime($cra->expiry_date)) ) {
			$cra->delete();
			if ($auto_generate) return $this->generate();
			else return false;
		}

		$lifeTime = $this->getLifeTime();

		// Extend (refresh) the expire date
		$cra->expiry_date = gmdate("Y-m-d H:i:s", time() + $lifeTime);
		$cra->save();

		// Set back the TZ 		
		date_default_timezone_set($currentTimeZone);
		
		// ok we can return it
		return $cra->token;
	}


	public function check($time, $signature) {
		// todo: implement

	}


	public function invalidate() {
		// Search for a token associated with this user
		$cra = CoreRestAuthentication::model()->find('id_user = :id_user', array(
			':id_user' => $this->id_user
		));
		// check if a token exists
		if (!$cra) {
			return true;
		}

		$cra->delete();
		return true;
	}
	

	/**
	 * Clean up: delete all expired tokens
	 * 
	 */
	public function cleanup($userOnly = false) {
		$criteria = new CDbCriteria();
		
		if ($userOnly) {
			$criteria->addCondition('id_user = :id_user');
			$criteria->params[':id_user'] = $this->id_user;
		}
		
		$criteria->addCondition('expiry_date < :utc_now');
		$criteria->params[':utc_now'] = Yii::app()->localtime->UTCNow;
		
		try {
			CoreRestAuthentication::model()->deleteAll($criteria);
		}
		catch (CException $e) {
			// nothing, we don't care
		}
		
	}
	

}
