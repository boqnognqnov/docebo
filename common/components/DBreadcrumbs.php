<?php
/**
 * CBreadcrumbs class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * CBreadcrumbs displays a list of links indicating the position of the current page in the whole website.
 *
 * For example, breadcrumbs like "Home > Sample Post > Edit" means the user is viewing an edit page
 * for the "Sample Post". He can click on "Sample Post" to view that page, or he can click on "Home"
 * to return to the homepage.
 *
 * To use CBreadcrumbs, one usually needs to configure its {@link links} property, which specifies
 * the links to be displayed. For example,
 *
 * <pre>
 * $this->widget('zii.widgets.CBreadcrumbs', array(
 *     'links'=>array(
 *         'Sample post'=>array('post/view', 'id'=>12),
 *         'Edit',
 *     ),
 * ));
 * </pre>
 *
 * Because breadcrumbs usually appears in nearly every page of a website, the widget is better to be placed
 * in a layout view. One can define a property "breadcrumbs" in the base controller class and assign it to the widget
 * in the layout, like the following:
 *
 * <pre>
 * $this->widget('zii.widgets.CBreadcrumbs', array(
 *     'links'=>$this->breadcrumbs,
 * ));
 * </pre>
 *
 * Then, in each view script, one only needs to assign the "breadcrumbs" property as needed.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @package zii.widgets
 * @since 1.1
 */

Yii::import('zii.widgets.CBreadcrumbs');

class DBreadcrumbs extends CBreadcrumbs {

	/**
	 * Parent init()
	 * -------------
	 * Basically i'm doing the setup of the extended class
	 */
	public function init() {
		parent::init();
		$this->tagName 				= 'ul';

		if(!$this->htmlOptions){
			$this->htmlOptions 			= array('class' => 'breadcrumb');
		}

		$this->homeLink	= CHtml::link('<i class="icon-home"></i>', Docebo::createAbsoluteLmsUrl('site/index'), array(
									'title' => Yii::t('login', '_HOMEPAGE'),
		));
		$this->activeLinkTemplate 	= '<li><a href="{url}">{label}</a>{separator}</li>';
		$this->inactiveLinkTemplate = '<li>{label}{separator}</li>';
		$this->separator 			= ' <span class="divider">&rsaquo;</span>';
	}

	/**
	 * Renders javascript code to communicate with the hydra frontend breadcrumb.
	 */
	private function renderHydraBreadcrumbBridge() {
		$pattern = '/(\?r=)|((&).*(=))/';
		$count = 0;
		$links = array();
		$pageTitle = null;
		if (is_array($this->links)) {
			foreach ($this->links as $label => $url) {
				$count++;

				// NOTE: this is a patch to allow the handling of "numeric" labels, otherwise they will be interpreted as simple
				// indexes and the wrong branch of the "if" is selected.
				if (is_array($url) && array_key_exists('label', $url) && array_key_exists('url', $url)) {
					//this is a very specific case, handled apart from the rest
					$label = (isset($url['label']) ? $url['label'] : '');
					$url = (isset($url['url']) ? $url['url'] : '');
				}

				if (is_array($url)) {
					foreach ($url as $key => $value)
						preg_match_all($pattern, $value, $matches);
				} else
					preg_match_all($pattern, $url, $matches);

				if (!empty($matches[0]) && $count != count($this->links))
					$label = strval($label);

				if (is_string($label) || is_array($url)) {
					$label = strip_tags(CHtml::decode($label));
					$links[] = array(
						'url' => isset($url['route']) ? $url['route'] : '/legacy'.CHtml::normalizeUrl($url),
						'label' => $this->encodeLabel ? CHtml::encode($label) : $label,
					);
				} else
					$pageTitle = strip_tags(CHtml::decode($url));
			}
		}

		return Yii::app()->legacyWrapper->setHydraBreadcrumbs($links, true, $pageTitle);
	}

	/**
	 * Renders the content of the portlet.
	 */
	public function run() {
		echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
		$links=array();
		if($this->homeLink===null)
			$links[] = '<li>' . CHtml::link(Yii::t('zii','Home'),Yii::app()->homeUrl) . ( !empty($this->links) ? $this->separator : '' ) . '</li>';
		elseif($this->homeLink!==false)
			$links[] = '<li>' . $this->homeLink . ( !empty($this->links) ? $this->separator : '' )  . '</li>';
		$i = 0;

		$pattern = '/(\?r=)|((&).*(=))/';
		$count = 0;
		if (is_array($this->links)) {
			foreach ($this->links as $label => $url) {
				$count++;
				//---
				// NOTE: this is a patch to allow the handling of "numeric" labels, otherwise they will be interpreted as simple
				// indexes and the wrong branch of the "if" is selected.
				if (is_array($url) && array_key_exists('label', $url) && array_key_exists('url', $url)) {
					//this is a very specific case, handled apart from the rest
					$label = (isset($url['label']) ? $url['label'] : '');
					$url = (isset($url['url']) ? $url['url'] : '');
				}
				//---
				if (is_array($url)) {
					foreach ($url as $key => $value) {
						preg_match_all($pattern, $value, $matches);
					}
				} else {
					preg_match_all($pattern, $url, $matches);
				}

				if (!empty($matches[0]) && $count != count($this->links)) {
					$label = strval($label);
				}

				if (is_string($label) || is_array($url)) {
					$label = strip_tags(CHtml::decode($label));
					$link = strtr($this->activeLinkTemplate, array(
						'{url}' => CHtml::normalizeUrl($url),
						'{label}' => $this->encodeLabel ? CHtml::encode($label) : $label,
					));
				} else {
					$url = strip_tags(CHtml::decode($url));
					$link = str_replace('{label}', $this->encodeLabel ? CHtml::encode($url) : $url, $this->inactiveLinkTemplate);
				}
				// $links[] = str_replace('{separator}',$this->separator);
				if ($i++ == count($this->links) - 1) $links[] = str_replace('{separator}', '', $link);
				else $links[] = str_replace('{separator}', $this->separator, $link);
			}
		}
		echo implode($links);
		echo CHtml::closeTag($this->tagName);

		// This works only if embedded in iframe
		echo $this->renderHydraBreadcrumbBridge();
	}
}