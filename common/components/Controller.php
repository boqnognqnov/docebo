<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {


	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/login',
	 * Internally the method resolveLayout() is usedin order to select the best layout based on the environment
	 */
	public $layout='//layouts/login';

    public $faviconUrl;

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();


	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();
	public $breadcrumbsHtmlOptions = array();

	public $pageTitle = '';

	/**
	 * Initialize the controller
	 */
	public function init() {
        // Checks whether the user has entered all additional fields, denies access and redirects to user/additionalFields
        // in case he hasn't
        $action = explode("/", Yii::app()->request->getParam('r'));
        if((Yii::app()->getController()->getId() != 'user' || $action[1] != 'additionalFields')
        && (Yii::app()->getController()->getId() != 'site' && $action[1] != 'logout')
		&& !in_array(Yii::app()->getController()->getId(), array('scormapi', 'oauth2'))
		&& ($action[0] !== 'mobileapp')
        && Settings::get('request_mandatory_fields_compilation')=='on'
        && !Yii::app()->user->getIsGuest() && Yii::app()->user->getIsUser()) {
            $hasToFill = CoreUserField::getUnfilledMandatoryFields();
            $defaultFields = CoreUser::getUnfilledDefaultFields();
            if(($hasToFill && count($hasToFill)) || count($defaultFields) > 0){
                if(Yii::app()->request->isAjaxRequest) {
                    echo '<script>window.location.href="'.Docebo::createLmsUrl('user/additionalFields').'"; </script>';
                    Yii::app()->end();
                } else {
                    $this->redirect(Docebo::createLmsUrl('user/additionalFields'));
                }
            }
        }

		// check for one time message exists and set flash in order to show it
		$this->checkForOneTimeMsgAndSetFlash();

		$overridePageTitle = false;
		Yii::app()->event->raise('BaseControllerInitStarted', new DEvent($this, array(
			'page_title'	=> &$overridePageTitle,
		)));

		// call parent folder
		parent::init();

		// Check if we should load a theme different from the standard one
		$this->resolveTheme();

		// And now we can choose the correct layout to apply as the standard one
		$this->resolveLayout();

		// Add Yii CSRF support for Ajax jQuery calls so the crsf token wil be added automatically to every POST call made with jquery
		$js_code = "useAjaxPrefilter = true; $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
			if((originalOptions.type !== 'GET' || options.type !== 'GET') && useAjaxPrefilter) {
				var original_data = ($.isPlainObject(originalOptions.data) ? $.param(originalOptions.data) : originalOptions.data);
				if (!options.data) { options.data = original_data = ''; }
				if (options.data.indexOf('".Yii::app()->request->csrfTokenName."') == -1) {
					options.data += (original_data.length > 0 ? '&' : '') + ".CJavaScript::encode(Yii::app()->request->csrfTokenName."=".Yii::app()->htmlpurifier->purify(Yii::app()->request->csrfToken)).";
				}
			}
		});";
		Yii::app()->getClientScript()->registerScript('ajaxPrefilter', $js_code, CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.textPlaceholder.js', CClientScript::POS_HEAD);
        Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/rightPanel.css');
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl . '/js/rightPanel/rightPanel.js', CClientScript::POS_HEAD);

		// For some reason Internet Explorer does not manage to apply AJAX loaded CSS for Flowplayer
		// We need to have its CSS loaded with all pages, everywhere.
		// To avoid repeating this in all layouts, lets have it here (but only for non AJAX calls)
		// Since Flowplayer is globally used, this should be ok
		if (!Yii::app()->request->isAjaxRequest) {
		  //Yii::import('common.widgets.Flowplayer');
		  Flowplayer::registerCssFile(Flowplayer::SKIN_FUNCTIONAL);
		}


		$this->initGlobalJsObjects();

		/*
		 * loading callout messages only for godadmins
		*/
		/* @var $user DoceboWebUser */
		$user = Yii::app()->user;

		if (!$user->getIsGuest() && $user->getIsGodadmin()) {
			// for debugging only
			if ('1' === Yii::app()->request->getParam('clear_callout_read_log')) {
				CalloutMessages::clearReadLog();
			}
			// callout messages will only be loaded once per login session
			CalloutMessages::load($user->getIdst());
		}

		if (!$overridePageTitle) {
			$this->pageTitle = Settings::get('page_title', $this->pageTitle ? $this->pageTitle : null);
		}
		else {
			$this->pageTitle = $overridePageTitle;
		}

		// Pre-load some extensions, unless we are going to Mobileapp module
		if (!is_null(Yii::app()->controller->module) && (Yii::app()->controller->module->id == 'mobileapp')) {
		}
		else {
			if(Yii::app()->bootstrap)
				Yii::app()->bootstrap->init();
			if(Yii::app()->dialog2)
				Yii::app()->dialog2->init();
			Yii::app()->plupload->init();
		}

		// Preload the central LO module (to call init()) and the channels module
		Yii::app()->getModule('channels');
		Yii::app()->getModule('centralrepo');

		// Redirect to Mobile App, not always, just if this is site controller and the device is mobile (and if not_smartphone is not set yet)

		// not_smartphone will come from MobileApp code, a redirection if mobile device is larger than a predefined size
		// We set a session variable to inform the rest of the code about this fact for next calls and to prevent infinite redirects
		$notSmartphone = Yii::app()->request->getParam('not_smartphone', false);
		if ($notSmartphone) {
			$_SESSION['not_smartphone'] =  1;
		}
		$isSmartphone = !isset($_SESSION['not_smartphone']) || !$_SESSION['not_smartphone'];

		// Handle specific redirects if:  Anonymous, isMobile and IS
		// We let sso request go through for compatibility with 3rd party systems using the "/lms" even on mobile
		$action = explode("/", Yii::app()->request->getParam('r'));

		if ((Yii::app()->controller->id == 'site') && !in_array(strtolower($action[1]), array('sso', 'loginrest', 'hybridauthendpoint', 'gappsLogin')) && Docebo::isMobile() && $isSmartphone) {

			// White Label Mobile option is kinda special
			if (!isset($_SESSION['force_desktop'])) {
				if (PluginManager::isPluginActive('WhitelabelApp')) {
					$url = Docebo::createAbsoluteLmsUrl('site/index', array());
					$_SESSION['force_desktop'] = 1;
					$_SESSION['lasturl'] = $url;
					$this->redirect($url, true);
				}
				$this->redirect(Docebo::createAbsoluteMobileUrl('site/index', array()), true);
			} else {
				if (PluginManager::isPluginActive('WhitelabelApp')) {
					unset($_SESSION['force_desktop']);
					$this->redirect(Docebo::createAbsoluteMobileUrl('site/index', array()), true);
				}
			}
		}

		Yii::app()->user->updateLastEnter();
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::beforeAction()
	 */
	protected function beforeAction($action)  {
		if(!Yii::app()->user->getIsGuest() && is_null(Yii::app()->user->loadUserModel()))
		{
			Yii::app()->user->logout();
			$this->redirect(Docebo::getRelativeUrl('.'));
		}

		IPHelper::checkForIPwhitelist();

		// Sanitize/Purify user input
		// See Docebo::purifyRequestWhitelist() and DoceboCHtmlPurifier::__constructor()
		Docebo::purifyRequest();


		// if(!Yii::app()->request->isSecureConnection && Settings::get('https_force_redirect', 'off') == 'on')
		if(!Yii::app()->request->isSecureConnection && Yii::app()->https->mustRedirect())
		{
			// https redirect (with the exception of some delicate controllers)
			// TODO: should these be made as general application config params ? maybe making them extendable by Apps
			$controllers_whitelist = array('appsMpApi', 'cart', 'googleappsApp');
			$module_whitelist = array('job', 'oauth');
			$action_whitelist = array(
				'site' => array('autoLogin', 'sso', 'hybridauthEndpoint', 'gappsLogin'),
				'ShopifyApp' => array('webhookOnCheckoutCreated', 'webhookOnCheckoutUpdated', 'webhookOnOrderPaid', 'webhookOnCustomerCreated', 'webhookOnCustomerDeleted', 'webhookOnProductUpdated'),
				'pensServer' => array('index')
			);

			$force_logged_redirect = false;

			// Specific action to check for the redirect to https though the user is logged in
			if(!Yii::app()->user->isGuest)
			{
				if($this->getId() == 'site' && (is_null($this->getAction()) || $this->getAction()->getId() == 'index'))
					$force_logged_redirect = true;

				if($this->getId() == 'user' && ($this->getAction()->getId() == 'changePassword' || $this->getAction()->getId() == 'additionalFields'))
					$force_logged_redirect = true;

				if($this->module->id == 'player' && $this->getId() == 'training' && $this->getAction()->getId() == 'index')
					$force_logged_redirect = true;

				if($this->getId() == 'mainmenu' && $this->getAction()->getId() == 'marketplace')
					$force_logged_redirect = true;
			}

			//check actions whitelist
			$redirect = true;
			foreach($action_whitelist as $w_controller => $w_action) {
				if (strcasecmp($w_controller, $this->getId()) == 0) {
					foreach ($w_action as $w_action_item) {
						if (strcasecmp($w_action_item,$action->getId()) == 0) {
							$redirect = false;
							break;
						}
					}
				}
				if (!$redirect) {
					break; //no further checks are needed
				}
			}

			if ($redirect && (Yii::app()->user->isGuest || $force_logged_redirect) && !in_array($this->getId(), $controllers_whitelist) && !in_array($this->module->id, $module_whitelist)) {
				if(!$force_logged_redirect)
				{
					// Temporary disable this (cookie leanup), to see if we still have problems described in https://docebo.atlassian.net/browse/LB-289
					// With cookies cleared we have problems redirecting users to internal pages after login
					// Ask Plamen and Marco
					//// Yii::app()->request->cookies->clear();
					$this->redirect( str_ireplace('http://', 'https://' , Docebo::getRootBaseUrl(true)) );
				}
				else
				{
					$this->redirect(str_ireplace('http://', 'https://' , Docebo::getRootBaseUrl(true)).Yii::app()->request->requestUri);
				}
			}
		}

		// Check if we have to resume LMS setup
		$user = Yii::app()->user;
		if (!$user->getIsGuest() && $user->getIsGodadmin()) {

			// We need to pass-through some routes in 'resume setup' mode
			if (
				(strtolower(Yii::app()->controller->route) == 'stream/file') ||
				(strtolower(Yii::app()->controller->route) == 'stream/image') ||
				(strtolower($action->id) == 'axuploadfile')
			) {
				return parent::beforeAction($action);
			}

			if (!Yii::app()->request->isAjaxRequest && Yii::app()->controller->id != 'setup' && false===strpos(Yii::app()->request->url, 'site/logout')) {
				Docebo::resumeSaasInstallation();
			}

		}
		if(!$user->getIsGuest() && !$user->getIsGodadmin()) {
			if (!Yii::app()->request->isAjaxRequest) {
				$this->checkIfMustRenewPassword();
			}
		}

		Yii::app()->event->raise('OnBeforeCControllerAction', new DEvent($this, array( 'appType' => 'lms' )));

		if (!isset(Yii::app()->session['reinsert-cc-data'])) {
			$erpClient = new ErpApiClient2();
			$result = $erpClient->call('lms/reinsert-cc-data');

			Yii::app()->session['reinsert-cc-data'] = $result;
		}

		return parent::beforeAction($action);
	}

	public function missingAction($actionID)
	{
		Log::_(Yii::t('yii','The system is unable to find the requested action "{action}".',array('{action}'=>$actionID==''?$this->defaultAction:$actionID)), CLogger::LEVEL_WARNING);
		$this->redirect(Docebo::createLmsUrl(''));
	}

	private function checkIfMustRenewPassword(){
		// User forced to change password, so redirect him to the
		// Change Password area from everywhere until he does

		// e.g. "site/AxHelpDeskFile"
		$currentControllerAndAction = Yii::app()->getController()->getId().'/'.Yii::app()->getController()->getAction()->getId();

		// Paths that should not redirect to the change password screen when visited
		$whitelistedPaths = array(
			'site/AxHelpDeskFile'
		);
		// You may also whitelist a whole controller
		$whitelistedControllers = array(
			'user',
			'scormapi',
			'oauth2'
		);

		if(isset($_SESSION['must_renew_pwd']) && $_SESSION['must_renew_pwd']){
			$controllerId 	= Yii::app()->getController()->id;


			// Avoid the infinite redirect with the check below since this class
			// is inherited by all controllers, including UserController (the one
			// where we are redirecting to).
			// Also, add any protected rules, which you want whitelist (these won't
			// redirect to the Change Password area, even if the user is forced to
			// change his password).
			if(!in_array($currentControllerAndAction, $whitelistedPaths) && !in_array($controllerId, $whitelistedControllers) && Yii::app()->getController()->getAction()->getId() != 'changePassword'){

				// Filter any protected routes here. All other pages, if visited,
				// will redirect the user to the Change Password screen until he does so.
				// One example page where we don't want this applied is ?r=site/logout.
				// We just want to logout the user there - not force him to change pass first.
				if($controllerId!='site' || stripos($_SERVER['REQUEST_URI'], 'logout')===false){
					$this->redirect(Docebo::createLmsUrl('user/changePassword'));
				}

			}
		}
	}



	public function beforeRender($view) {
		if((CoreUser::isUserGodadmin(Yii::app()->user->idst) || CoreUser::isPowerUser(Yii::app()->user->idst))){
			Yii::app()->getModule('backgroundjobs');
		}
		Yii::app()->clientScript->registerScript('helpers', '
				yii = {
					urls: {
						base: 			' . json_encode(Yii::app()->baseUrl) . '
					},
					language: "' . Yii::app()->getLanguage() . '"
				};
			', CClientScript::POS_HEAD);

		// Bundle, create && publish JsTrans; set as a bare minimum the default categories set in the class JsTrans
		$jsTrans = new JsTrans();

		Docebo::showPopupRedirect();

		return parent::beforeRender($view);
	}


	/**
	 * Check if there's another theme (different from standard) to be used
	 */
	protected function resolveTheme() {
		$template = Settings::get('defaultTemplate', 'standard');
		if ($template !== 'standard') {
			$nonStandardTheme = Yii::app()->themeManager->getTheme($template);
			if ($nonStandardTheme)
				Yii::app()->theme = $nonStandardTheme;
		}

		// Get calculated favicon URL
		$this->faviconUrl = Yii::app()->theme ? Yii::app()->theme->getFaviconUrl() : null;
	}


	/**
	 * Change the layout properties of the controller based on the request/environment in which the controller is called
	 */
	public function resolveLayout() {
		if (Yii::app()->name == 'admin') {
			// Admin, only one layout is needed :)
			$this->layout = '//layouts/adm';
		} else {
			if (Yii::app()->name == 'authoring') {
				$this->layout = '//layouts/authoring';
			} else {
				// In the lms we have the need of select the correct layout
				if (Yii::app()->user->getIsGuest()) {
					// Anonymous user
					if ( /*(Settings::get('module_ecommerce') == 'on') && */(Settings::get('catalog_external') == 'on') && (!Docebo::isPlatformExpired()) ) {
						// external public catalog enabled
						$this->layout = "//layouts/public_catalog";
					} else { // normal login layout
						$this->layout = '//layouts/login';
					}
				} else {
					// internal lms !
					$this->layout = "//layouts/lms";
				}
			}
		}
	}


	public function sendJSON($data) {
		header('Content-Type: application/json; charset="UTF-8"');
		echo CJSON::encode($data);
		Yii::app()->end();
	}


	/**
	 * Register and init (if any) global JS objects
	 *
	 */
	public function initGlobalJsObjects() {
		if (!Yii::app()->request->isAjaxRequest) {
			$cs = Yii::app()->getClientScript();
			$options = array(
					'rootBaseUrl' 				=> Docebo::getRootBaseUrl(),			// ""
					'rootAbsoluteBaseUrl' 		=> Docebo::getRootBaseUrl(true),		// http://my.docebosaas.com
					'lmsBaseUrl'  				=> Yii::app()->getBaseUrl(),			// /lms
					'lmsAbsoluteBaseUrl'  		=> Yii::app()->getBaseUrl(true),		// http://my.docebosaas.com/lms
					'absLmsUrl' 				=> Docebo::createAbsoluteLmsUrl(),  	// http://my.docebosaas.com/lms/index.php
					'absAdminUrl' 				=> Docebo::createAbsoluteAdminUrl(),	// http://my.docebosaas.com/admin/index.php
					'absApp7020Url' 			=> Docebo::createAbsoluteApp7020Url(), 	//  http://my.docebosaas.com/app7020/index.php
					'language'					=> Yii::app()->getLanguage(),
                    'hydraAccessToken'          => Yii::app()->user->getAccessToken()
			);
			$cs->registerScript("docebo_global_init", "Docebo.init(". CJavaScript::encode($options) . ");", CClientScript::POS_END);
			$cs->registerScript("docebo_global_ready", "Docebo.ready();", CClientScript::POS_READY);
		}
	}


	/**
	 * This is a deniedCallback method used in some (or all) controllers accessRules to handle denied access to some or all its actions
	 *
	 * @param object $rule
	 */
	public function accessDeniedCallback($rule) {

		$url = Docebo::createAbsoluteLmsUrl('site/index');
		$message = Yii::t('standard', '_OPERATION_FAILURE');
		$message = $rule ? ($rule->message ? $rule->message : $message) : $message;

		// Ajax requests are handled differently!
		// The idea is to be able to redirect users during AJAX calls.
		// This method works in conjunction with a global AJAX event listener, namely
		// listening to ajaxSuccess, defined in /themes/spt/js/app.js, loaded globally, always.
		// We send a custom header watched by that listener and redirects accordingly.
		if (Yii::app()->request->isAjaxRequest && !headers_sent()) {

			// Send custom header.
			header('X-Ajax-Redirect-Url: ' . $url);

			if ( Yii::app()->user->getIsGuest() ) {
				$message = Yii::t( 'standard', 'Your session is expired. Redirecting ... ' );
			} else {
				if ( ! empty( $message ) ) {
					Yii::app()->user->setFlash( 'error', $message );
				}
			}


			// Try to determine the expected content type and echo empty JSON or just a text
			echo (stripos(Yii::app()->request->acceptTypes, 'json') === false) ? $message : CJSON::encode(array('html' => $message));
			Yii::app()->end();
		}

		if(YII_DEBUG && !Yii::app()->user->getIsGuest()){
			throw new CException($message);
		}else{
			if(!Yii::app()->user->getIsGuest() || $rule->message) {
				Yii::app()->user->setFlash('error', $message);
				Yii::log($message, CLogger::LEVEL_ERROR, __METHOD__);
			}

			$this->redirect($url);
		}
	}


	/**
	 * (non-PHPdoc)
	 * @see CController::redirect()
	 */
	public function redirect($url, $terminate=true, $statusCode=302, $noReferrer = false) {
		// Set session variable to last URI before redirect. Used in after-login redirect
		// But for not already set one. It will be unset right after login

		if (!isset(Yii::app()->session['lasturl']) && !in_array(Yii::app()->controller->action->id, array('linkedinLogin','fbLogin','sso','hybridauthendpoint','gappsLogin'))) {
			if (Yii::app()->legacyWrapper->hydraFrontendEnabled())
				Yii::app()->session['lasturl'] = Yii::app()->urlManager->convertToHydraUrl();
			else
				Yii::app()->session['lasturl'] = $_SERVER['REQUEST_URI'];
		}

		if($noReferrer)
			Yii::app()->session['lasturl'] = null;

		// This route is not a legit/normal url to go after login
		if (Yii::app()->controller->route == 'user/recoverPwd') {
			unset(Yii::app()->session['lasturl']);
		}

		// Allow AJAX calls to redirect user
		if (Yii::app()->request->isAjaxRequest && !headers_sent()) {
			// Disable for now
			//header('X-Ajax-Redirect-Url: ' . $url);
			//$message = Yii::t('standard', 'Redirecting...');
			//echo (stripos(Yii::app()->request->acceptTypes, 'json') === false) ? $message : CJSON::encode(array('html' => $message, 'data' => $message));
			//Yii::app()->end();
		}

		parent::redirect($url, $terminate, $statusCode);
	}

	private function checkForOneTimeMsgAndSetFlash(){
		$userId = Yii::app()->user->id;
		$messageParams = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreSettingUser::model()->tableName())
				->where('path_name = "one_time_flash_msg" AND id_user = :id', array(':id' => $userId))->queryScalar();
		if($messageParams) {
			$messageParams = json_decode($messageParams);
			// may add more types later
			switch($messageParams->type){
				case 'flash':
					switch($messageParams->text){
						case 'emailSendToUser':
							$userValidityDate = Yii::app()->localtime->toUserLocalDatetime($userId, date('Y-m-d H:i:s', $messageParams->timestamp));
							Yii::app()->user->setFlash('info',
								Yii::t('standard', 'Please check your email for confirmation link in order to confirm your email until {date}',
										array('{date}' => $userValidityDate)));
							break;
						default:
							Yii::app()->user->setFlash('info', $messageParams->text);
							break;
					}
					break;
			}

			// delete one time message
			Yii::app()->db->createCommand()
					->delete(CoreSettingUser::model()->tableName(),
							'path_name="one_time_flash_msg" AND id_user = :id', array(':id' => $userId));

		}
	}

	public function createBackgroundJob($name, $handler, $data = array(), $params = array()){
		Yii::app()->backgroundJob->createJob($name, $handler, $data, $params);

		$this->renderPartial("lms.protected.modules.backgroundjobs.views.default._createBackgroundJob");
	}
}
