<?php
/**
 * Global helper component providing sanitization of any kind.
 * Add more methods for
 * 
 * 	- html
 * 	- mysql
 * 	- emails
 * 	- etc.
 * 
 * @author Plamen
 *
 */
class Sanitize extends CComponent {
	
	/**
	 * Sanitize file names
	 * 
	 * @param string $fileName
	 * @param boolean $slugify Replace white spaces with
	 * @param string $slugChar Replacement character if slugify is enabled 
	 * @return string
	 */
	public static function fileName($fileName, $slugify=false, $slugChar="_") {
		$badChars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
		$fileName = str_replace($badChars, '', $fileName);
		if ($slugify) {
			$fileName = preg_replace('/[\s-]+/', $slugChar, $fileName);
		}
		$fileName = trim($fileName, '.-_');
		
		return $fileName;
		
	}

	/**
	 * Convert part of string to lowercase depending on regular expression match
	 *
	 * @param string $expression : regular expression
	 * @param string $string : the string
	 * @return string
	 */

	public static function pregLowercaseCall($expression,$string){
		$pregCall = preg_replace_callback(
			$expression,
			function($m) { return strtolower($m[0]); },
			$string
		);
		return $pregCall;
	}
	
}