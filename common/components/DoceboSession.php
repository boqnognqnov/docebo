<?php


class DoceboSession extends CHttpSession {

	public $min_session_timeout = 900; //default 15 min
	public $max_session_timeout = 10800; //default 3 hours



	public function init() {
		// Mark the cookie as secure if we're running on HTTPS
		if(Yii::app()->request->isSecureConnection)
			$this->setCookieParams(array('secure' => true));
		parent::init();

		// Store in the session that the passed cookie is valid only for the current domain
		$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
		if(!$this->get('session_domain'))
			$this->add('session_domain', $domain);

		// Validate the cookie for this domain
		if($this->get('session_domain') != $domain) {
			Yii::log("Attempt to use cookie for domain '".$this->get('session_domain')."' with another domain '".$domain."'.", CLogger::LEVEL_ERROR);
			exit;
		}
	}


	/**
	 * Chack session time validity, change them if value is not between minimum and maximum allowed
	 * @param $value session timeout value
	 * @return int validated session time value
	 */
	public function validateSessionTimeout($value) {
		$output = $value;
		if ($output < $this->min_session_timeout) { $output = $this->min_session_timeout; }
		if ($output > $this->max_session_timeout) { $output = $this->max_session_timeout; }
		return $output;
	}



	/**
	 * Override parent function
	 * @param integer $value the number of seconds after which data will be seen as 'garbage' and cleaned up
	 */
	public function setTimeout($value) {
		$validatedTimeout = $this->validateSessionTimeout($value);
		parent::setTimeout($validatedTimeout);
	}
	
	
}
