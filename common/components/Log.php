<?php

/**
 * Slightly improved Log utility, based on core Yii::log();
 *
 * @author Plamen Petkov
 *
 */
class Log {

	/**
	 * Add possibility to easily log VARIABLES rather tnan STRINGS only.
	 *
	 * Always flush log to avoid loosing logs if Yii application does not end properly.
	 * So, use it for specific debug only!  For normal Logging use Yii::log() as usual.
	 *
	 * Usage:  Log::_($somevariable);
	 *
	 * @param mixed $msg
	 * @param string $level
	 * @param string $category
	 */
	public static function _($msg,$level=CLogger::LEVEL_INFO,$category='application') {
		$message = print_r($msg,true);
		Yii::log($message,$level,$category);

		// Immediatyely flush log! Could be resource consuming!
		Yii::getLogger()->flush(true);
	}


}