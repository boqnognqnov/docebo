<?php
/**
 * Yii LogRoute class to save Yii log messages in Mongo database.
 * 
 * Based on Yii 1.1: mongodblogroute (http://www.yiiframework.com/extension/mongodblogroute/).
 * 
 * Adapted for MongoDB version 2.6.x
 *
 * Options
 * connectionString        : host:port                      : defalut localhost:27017
 * dbName                  : database name                  : default yiilog_test
 * collectionName          : collaction name                : default yiilog
 * collectionSize          : capped collection size         : default 10000
 * collectionMax           : capped collection max          : default 100
 * cappedCollection 	   : use capped collection          : default true
 * fsync                   : fsync flag                     : defalut false
 * w                       : Write Concerns                 : defalut 1
 * socketTimeoutMS         : Socket timeout, miliseconds  	: defalut 30000 (30 secs)
 *
 * Example
   'log'=>array(
      'class'=>'CLogRouter',
      'routes'=>array(
        array(
          'class'=>'ext.EMongoDbLogRoute',
          'levels'=>'trace, info, error, warning',
          'categories' => 'system.*',
          'connectionString' => 'localhost:27017',
          'dbName' => 'test',
          'collectionName' => 'yiilog',
          'collectionSize' => 10000,
          'collectionMax' => 100,
          'cappedCollection' => true,
        ),
      ),
    ),
 *
 *
 */
class MongoDbLogRoute extends CLogRoute {

	/**
	 * Enable/Disable this log route
	 * @var boolean
	 */
	public $enable = false;
	
	
	/**
	 *
	 * @var string Mongo Db host + port
	 */
	public $connectionString = "localhost:27017";
	
	/**
	 *
	 * @var string Mongo Db Name
	 */
	public $dbName = "yiilog_test";
	
	/**
	 *
	 * @var string Collection name
	 */
	public $collectionName = "yiilog";
	
	/**
	 *
	 * @var integer capped collection size
	 */
	public $collectionSize = 10000000;
	
	/**
	 *
	 * @var integer capped collection max
	 */
	public $collectionMax = 5000;
	
	/**
	 * If collection should be "capped" or not
	 * @var unknown
	 */
	public $cappedCollection = true;
	
	/**
	 *
	 * @var boolean forces the update to be synced to disk before returning success.
	 */
	public $fsync = false;
	
	/**
	 *
	 * @var integer Write Concerns
	 */
	public $w = 1;
	
	/**
	 *
	 * @var integer This option specifies the time limit, in milliseconds, for socket communication
	 */
	public $socketTimeoutMS = 30000; // 30 seconds
	
	/**
	 *
	 * @var Mongo mongo Db collection
	 */
	private $collection;
	
	/**
	 *
	 * @var array insert options
	 */
	private $options;
	
	/**
	 * Initializes the route.
	 * This method is invoked after the route is created by the route manager.
	 */
	public function init() {

		/*
		@ini_set ( 'display_errors', 1 );
		@ini_set ( 'error_reporting', E_ALL & ~ E_NOTICE );
		*/
		
		parent::init ();
		
		if (!$this->enable) {
			return FALSE;
		}
		
		// Try to Connect
		try {
			$connection = new MongoClient ( $this->connectionString );
		}
		catch (MongoException $e) {
			Yii::log($e->getMessage(), 'error', __CLASS__);
			$this->enable = false;
			return false;
		}
				
		$dbName = $this->dbName;
		$collectionName = $this->collectionName;
		
		// If collection does not exists, create it
		// It it exists, but is not capped AND we WANT IT capped, drop it and create new one, Capped
		$stats = $connection->lmslogs->$collectionName->validate();
		if (!$stats['ok'] || ($this->cappedCollection && !$stats['capped'])) {
			$connection->lmslogs->dropCollection($collectionName);
			$this->collection = $connection->lmslogs->createCollection ( $collectionName, array (
					'capped' 	=> $this->cappedCollection,
					'size' 		=> 10000000,
					'max' 		=> 5000
			) );
		}
		else {
			$this->collection = $connection->$dbName->$collectionName;
		}
		
		$this->options = array (
			'fsync' 			=> $this->fsync,
			'w' 				=> $this->w,
			'socketTimeoutMS' 	=> $this->socketTimeoutMS 
		);
		
	}
	
	/**
	 * @see CLogRoute::processLogs()
	 */
	protected function processLogs($logs) {
		
		if (!$this->enable) {
			return FALSE;
		}
		
		
		foreach ( $logs as $log ) {
			$message 		= $log [0];
			$level 			= $log [1];
			$category 		= $log [2];
			$timestamp 		= $log [3];
			$mongoDateTime 		= new MongoDate ( round ( $timestamp ) );
			$document = array (
				'domainCode'	=> Docebo::getOriginalDomainCode(),
				'message' 		=> $this->formatLogMessage($message, $level, $category, $timestamp),
				'level' 		=> $level,
				'category' 		=> $category,
				'timestamp' 	=> $timestamp,
				'datetime'		=> @date('Y/m/d H:i:s',$timestamp),
			);
			$this->collection->insert ( $document, $this->options );
		}
	}
	
	
	/**
	 * @see CLogRoute::formatLogMessage()
	 */
	protected function formatLogMessage($message,$level,$category,$time)
	{
		$domainCode = Docebo::getOriginalDomainCode();
		return @date('Y/m/d H:i:s',$time)." [$domainCode] [$level] [$category] $message";
	}
	
	
}
