<?php
class AjaxResult
{
	//properties used directly in toJSON method, so please pay attention if you add more
	private $success; 	//ajax status true|false
	private $message; 	//ajax main message - string
	private $data;		//ajax data result
	private	$html;		//ajax html response 

	/**
	 * Set ajax response message
	 * @param $msg
	 * @return $this
	 */
	public function setMessage($msg)
	{
		$this->message = $msg;
		return $this;
	}

	/**
	 * Get current message
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Set ajax response success status
	 * @param $status
	 * @return $this
	 */
	public function setStatus($status)
	{
		$this->success = ($status ? true : false);
		return $this;
	}

	/**
	 * Check if the response has error already set
	 * @return bool
	 */
	public function hasError()
	{
		return $this->success ? false : true;
	}

	/**
	 * Set data parameter with whatever you want - array, string etc (PHP allows it :D )
	 * @param $data
	 * @return $this
	 */
	public function setData($data)
	{
		$this->data = $data;
		return $this;
	}
	
	/**
	 * Set very specific AJAX Result - 'html' 
	 * @param string $html
	 * @return AjaxResult
	 */
	public function setHtml($html)
	{
		$this->html = $html;
		return $this;
	}
	

	/**
	 * @param bool $print whether the result should be printed or returned - by default true (print)
	 * @return string
	 */
	public function toJSON($print = true)
	{
		$output = json_encode(get_object_vars($this));
		if ($print)
			echo $output;
		else
			return $output;
	}
}