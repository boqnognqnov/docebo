<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */


class DEvent extends CEvent {

    /**
     * Name of the triggered event
     * @var string
     */
    public $event_name = false;

    /**
     * Allows handlers to prevent the notifier default behaviour
     * @var bool
     */
    protected $prevent_default = false;

    /**
     * Return values from the event handler must be set in this object
     * @var bool | mixed
     */
    public $return_value = false;

    /**
     * Sets the prevent default to true, so that the call knows that
     */
    public function preventDefault() {
        $this->prevent_default = true;
    }

    /**
     * Retrieves the default behaviour flag
     * @return bool
     */
    public function shouldPerformAsDefault() {
        return !$this->prevent_default;
    }

	/**
	 * Returns the value of the param being passed to the vent, or $defaultValue if set, or null
	 *
	 * @param      $paramName
	 * @param null $defaultValue
	 *
	 * @return null
	 */
	public function getParamValue($paramName, $defaultValue = null){
		if(!is_array($this->params)) return $defaultValue;

		if(isset($this->params[$paramName])){
			return $this->params[$paramName];
		}

		return $defaultValue;

	}
}