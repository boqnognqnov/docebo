<?php
/**
 * A compagnion component for ProgressController, defining constants and helper methods
 * 
 * @see ProgressController
 */
class Progress extends CApplicationComponent {
	
	const JOBTYPE_CUSTOM_REPORT 				= 1;
	const JOBTYPE_COURSE_USERS_REPORT 			= 2;
	const JOBTYPE_COURSE_OBJECTS_REPORT 		= 3;
	const JOBTYPE_COURSE_SINGLE_OBJECT_STATS	= 4;

	const JOBTYPE_LDAP_IMPORT                   = 5;
	const JOBTYPE_IMPORT_USERS_CSV              = 6;

	const JOBTYPE_ASSIGN_USERS_TO_BRANCH 		= 7;

	const JOBTYPE_ENROLL_USERS_TO_COURSE 		= 8;
	const JOBTYPE_ENROLL_USERS_TO_MANY_COURSES 	= 9;
	const JOBTYPE_CHANGE_ENROLLMENT_STATUS 		= 10;
	const JOBTYPE_DELETE_USER_ENROLLMENT 		= 11;
	const JOBTYPE_SEND_EMAIL_TO_ENROLLED_USERS	= 12;
    const JOBTYPE_ENROLL_USERS_TO_SESSION       = 13;
    const JOBTYPE_ENROLL_USERS_TO_WEBINAR_SESSION = 14;
    const JOBTYPE_DELETE_USERS_FROM_WEBINAR_SESSION = 15;
	const JOBTYPE_DELETE_USERS_FROM_CLASSROOM_SESSION = 16;
	const JOBTYPE_CHANGE_USER_STATUS = 17;
    const JOBTYPE_AUTO_ASSIGN_USERS_TO_SINGLE_GROUP = 18;
    const JOBTYPE_UNASSIGN_USERS_FROM_BRANCH = 19;
    const JOBTYPE_EXPORT_USERS_TO_CSV = 20;
    const JOBTYPE_CHANGE_ENROLLMENT_DEADLINE = 21;
    const JOBTYPE_EXPORT_SALESFORCE_LOG = 22;
	const JOBTYPE_DELETE_USERS = 23;
	const JOBTYPE_ASSIGN_USERS_TO_SINGLE_BRANCH = 24;
	const JOBTYPE_ASSIGN_USERS_TO_BADGE = 25;
	const JOBTYPE_DELETE_MULTI_ASSIGNED_BADGE = 26;
    const JOBTYPE_UNASSIGN_USERS_FROM_GROUP = 27;


	//Import sessions from Course management
	const JOBTYPE_IMPORT_SESSIONS = 30;
	const JOBTYPE_EXPORT_ACTIVE_USERS_REPORT = 35;

	/**
	 * Check if a specified job type is standard defined in this class or not
	 * @param $def the type definition to be checked
	 * @return bool true if it is a standard type, otherwise false
	 */
	public static function isStandardJobType($def) {
		$output = false;
		switch ($def) {
			case self::JOBTYPE_CUSTOM_REPORT:
			case self::JOBTYPE_COURSE_USERS_REPORT:
			case self::JOBTYPE_COURSE_OBJECTS_REPORT:
			case self::JOBTYPE_COURSE_SINGLE_OBJECT_STATS:
			case self::JOBTYPE_LDAP_IMPORT:
			case self::JOBTYPE_IMPORT_USERS_CSV:
			case self::JOBTYPE_ASSIGN_USERS_TO_BRANCH:
			case self::JOBTYPE_ASSIGN_USERS_TO_SINGLE_BRANCH:
			case self::JOBTYPE_ENROLL_USERS_TO_COURSE:
			case self::JOBTYPE_ENROLL_USERS_TO_MANY_COURSES:
			case self::JOBTYPE_CHANGE_ENROLLMENT_STATUS:
            case self::JOBTYPE_CHANGE_ENROLLMENT_DEADLINE:
			case self::JOBTYPE_DELETE_USER_ENROLLMENT:
			case self::JOBTYPE_SEND_EMAIL_TO_ENROLLED_USERS:
            case self::JOBTYPE_ENROLL_USERS_TO_SESSION:
            case self::JOBTYPE_ENROLL_USERS_TO_WEBINAR_SESSION:
            case self::JOBTYPE_DELETE_USERS_FROM_WEBINAR_SESSION:
            case self::JOBTYPE_DELETE_USERS_FROM_CLASSROOM_SESSION:
            case self::JOBTYPE_EXPORT_USERS_TO_CSV:
            case self::JOBTYPE_AUTO_ASSIGN_USERS_TO_SINGLE_GROUP:
            case self::JOBTYPE_UNASSIGN_USERS_FROM_BRANCH:
			case self::JOBTYPE_CHANGE_USER_STATUS:
			case self::JOBTYPE_EXPORT_SALESFORCE_LOG:
			case self::JOBTYPE_IMPORT_SESSIONS:
			case self::JOBTYPE_EXPORT_ACTIVE_USERS_REPORT:
			case self::JOBTYPE_DELETE_USERS:
			case self::JOBTYPE_ASSIGN_USERS_TO_BADGE:
			case self::JOBTYPE_DELETE_MULTI_ASSIGNED_BADGE:
			case self::JOBTYPE_UNASSIGN_USERS_FROM_GROUP:
				$output = true;
				break;
		}
		return $output;
	}

}



