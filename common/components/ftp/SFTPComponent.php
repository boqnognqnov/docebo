<?php

// Problems with RSA.php (line 84) without this magic trick
spl_autoload_unregister(array('YiiBase','autoload'));
require_once('Net/SFTP.php');
require_once('Crypt/RSA.php');
require_once('Crypt/AES.php');
spl_autoload_register(array('YiiBase','autoload'));

class SFTPComponent extends CApplicationComponent
{
    /**
     * @var string $host sftp host ip.
     */
    public $host = null;

    /**
     * @var string $port sftp host port default 22.
     */
    public $port = '22';

    /**
     * @var string $username username of remote sftp account.
     */
    public $username = null;

    /**
     * @var string $username username of remote sftp account.
     */
    public $password = null;

	/**
	 * @var string The path to the .ppk file to be used for the SSH connection
	 *             If not set, the regular username and password authentication
	 *             will be used (less secure)
	 */
	public $sshFilePath = false;

	/**
	 * @var string The password for the private .ppk file used for SFTP auth
	 */
	public $sshFilePassword = null;

    /**
     * @var Net_SFTP $objSftp sftp class object.
     */
    private $objSftp = null;

    /**
     * @var SSH2 $objSsh SSH class object.
     */
    private $objSsh = null;

	/**
	 * @param string $host
	 * @param string $username
	 * @param string $password
	 * @param string $port
	 * @param bool   $sshFilePath
	 * @param null   $sshFilePassword
	 */
    public function __construct($host = null, $username = null, $password = null, $port = '22', $sshFilePath = false, $sshFilePassword = null)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;

		$this->sshFilePath = $sshFilePath;
		$this->sshFilePassword = $sshFilePassword;
    }

    /**
     * Initializes the component.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Establish SFTP connection
     * @return bool true when connection success
     * @throws CException if connection fails
     */
    public function connect()
    {
        $this->objSftp = new Net_SFTP($this->host, $this->port);

		if($this->sshFilePath){
			if(!is_file($this->sshFilePath)){
				Yii::log('Invalid SSH file path for SFTP connection:'. $this->sshFilePath, CLogger::LEVEL_ERROR);
			}else{
				// Connect to SFTP server using "private key" SSH
				$privateKey = new Crypt_RSA();
				$privateKey->setPassword($this->sshFilePassword);
				$privateKey->loadKey(file_get_contents($this->sshFilePath));

				if ($this->objSftp->login($this->username, $privateKey))
				{
					$this->objSsh = new Net_SSH2($this->host, $this->port);
					$this->objSsh->login($this->username, $privateKey);
					return true;
				}else{
					Yii::log($this->objSftp->getLastError(), CLogger::LEVEL_ERROR);
					throw new CException('SFTP: Invalid username or private key');
				}
			}
		}

		// Fall back to regular username/password connection to SFTP server
        if ($this->objSftp->login($this->username, $this->password))
        {
            $this->objSsh = new Net_SSH2($this->host, $this->port);
            $this->objSsh->login($this->username, $this->password);

            return true;
        }
        else
        {
            throw new CException('SFTP: Invalid username or password');
        }
    }

    /**
     * list directory contents
     * @param string $directory Directory path
     * @param bool $showHiddenFiles default false, if true list hidden files also
     * @return array $files list of contents including directories
     */
    public function listFiles($directory = '.', $showHiddenfiles = false)
    {
        $res_files = $this->objSftp->nlist($directory);

        $files = array();

        foreach($res_files as $file)
        {
            if (!$showHiddenfiles && ('.' == $file || '..' == $file || '.' == $file[0]))
                continue;

            $files[] = $file;
        }

        return $files;
    }

    /**
     * Returns the current directory
     * @return string Current directory path
     */
    public function getCurrentDir()
    {
        return $this->objSftp->pwd();
    }

    /**
     * Check for directory
     * @param string $directory Directory path
     * @return bool true if is a directory otherwise false
     */
    public function isDir($directory)
    {
        if ($this->objSftp->chdir($directory))
        {
            $this->objSftp->chdir('..');
            return true;
        }

        return false;
    }

    /**
     * Change directory
     * @param string $directory Directory path
     * @return bool true if directory change success
     * @throws CException if directory change fails
     */
    public function chdir($directory)
    {
        if ($this->objSftp->chdir($directory))
        {
            return true;
        }
        else
        {
            throw new CException("SFTP: Can't change directory to: '" .$directory . "'");
        }
    }

    /**
     * Put file to a sftp location
     * @param string $localFile Local file path
     * @param string $remoteFile Remote file path
     * @return bool true if file send success
     * @throws CException if file transfer fails
     */
    public function sendFile($localFile, $remoteFile)
    {
        if ($this->objSftp->put($remoteFile, $localFile, NET_SFTP_LOCAL_FILE))
        {
            return true;
        }
        else
        {
            throw new CException('File send failed.');
        }
    }

    /**
     * Get file from sftp location
     * @param string $remoteFile Remote file path
     * @param string $localFile Local file path
     * @return bool true if file retreival success
     * @throws CException if file transfer fails
     */
    public function getFile($remoteFile, $localFile)
    {
        if ($this->objSftp->get($remoteFile, $localFile))
        {
            return true;
        }
        else
        {
            throw new CException('SFTP: Cannot find file: "'.$remoteFile.'" on the remote server');
        }
    }

    /**
     * Retreive file attributes
     * @param string $file Remote file path
     * @param string $attribute Required attribute (size, gid, uid, atime, mtime, mode)
     * @return string Attribute value
     */
    private function getFileStat($file, $attribute)
    {
        $statinfo = $this->objSftp->stat($file);

        return $statinfo[$attribute];
    }

    /**
     * Retreive file size
     * @param string $file Remote file path
     * @return string File size
     */
    function getFileSize($file)
    {
        return $this->getFileStat($file, 'size');
    }

    /**
     * Retreive file modified datetime
     * @param string $file Remote file path
     * @return string File modified timestamp
     */
    function getMdtm($file)
    {
        return $this->getFileStat($file, 'mtime');
    }

    /**
     * Retreive file created datetime
     * @param string $file Remote file path
     * @return string File created timestamp
     */
    function getAtime($file)
    {
        return $this->getFileStat($file, 'atime');
    }

    /**
     * Create directory on sftp location
     * @param string $directory Remote directory path
     * @return bool true if directory creation success
     * @throws CException if directory creation fails
     */
    function mkdir($directory)
    {
        if ($this->objSftp->mkdir($directory))
        {
            return true;
        }
        else
        {
            throw new CException('Directory creation failed.');
        }
    }

    /**
     * Remove directory on sftp location
     * @param string $directory Remote directory path
     * @param bool $foreceRemove If true remove directory even it is not empty
     * @return bool true if directory removal success
     * @throws CException if directory removal fails
     */
    function rmdir($directory, $foreceRemove=false)
    {
        if ($foreceRemove)
        {
            $this->execCmd("rm -rf {$directory}");

            return true;
        }
        else
        {
            if ($this->objSftp->delete($directory))
            {
                return true;
            }
            else
            {
                throw new CException('Directory removal failed.');
            }
        }
    }

    /**
     * Remove file on sftp location
     * @param string $file Remote file path
     * @return bool true if file removal success
     * @throws CException if file removal fails
     */
    function removeFile($file, $foreceRemove=false)
    {
        if ($foreceRemove)
        {
            $this->execCmd("rm -rf {$file}");

            return true;
        }
        else
        {
            if ($this->objSftp->delete($file))
            {
                return true;
            }
            else
            {
                throw new CException('File removal failed.');
            }
        }
    }

    /**
     * Change directory ownership
     * @param string $path Directory or file path
     * @param string $user User
     * @param string $group Group
     * @param bool $recursive Change ownership to subcontens also
     * @return bool true
     */
    function chown($path, $user, $group, $recursive=false)
    {
        if ($recursive)
        {
            $cmd = "chown -R {$user}:{$group} {$path}";
        }
        else
        {
            $cmd = "chown {$user}:{$group} {$path}";
        }

        $this->execCmd($cmd);

        return true;
    }

    /**
     * Change directory permission
     * @param string $path Directory or file path
     * @param string $permission Permission
     * @param bool $recursive Change permission to subcontens also
     * @return bool true
     */
    function chmod($path, $permission, $recursive=false)
    {
        if ($recursive)
        {
            $cmd = "chmod -R {$permission} {$path}";
        }
        else
        {
            $cmd = "chmod {$permission} {$path}";
        }

        $this->execCmd($cmd);

        return true;
    }

    /**
     * Execute command on remote shell
     * @param string $cmd Command ex:pwd
     * @return string $output Command output
     */
    function execCmd($cmd)
    {
        $output = $this->objSsh->exec($cmd);

        return $output;
    }
}