<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class DoceboUserIdentity extends CUserIdentity {

	private $_id;

	/*
	 * Maximum login attempts before the user is blocked for a few minutes.
	 */
	private $max_failed_login_attempts = 20;
	/*
	 * How many minutes the user is blocked after reaching $max_failed_login_attempts.
	 */
	private $failed_logins_wait_minutes = 10;


	/**
	 * @return bool
	 */
	public function authenticate() {
		$res = false;

		// Raise a pre-login event for plugins to override the login mechanism
		// (as of writing this, only LdapApp does it)
		// NOTE: Before this step only the following check is performed: if the username
		// and password are provided (not empty). Their validity and correctness is NOT
		// checked (that is left to the event listener to do)
		$event = new DEvent($this, array (
			'username' => $this->username,
			'password' => $this->password,
		));
		Yii::app()->event->raise('UserLoginPreProcess', $event);


		// If LDAP (or another plugin) already logged in the user but are not forcing
		// redirection, we'll end up here with "logged_in" = true. This allows the Mobile App
		// to work with LDAP as well.
		if(isset($event->return_value['logged_in']) && $event->return_value['logged_in'])
			return true;

		if (!empty($this->username)) {
			$user = CoreUser::model()->find('userid = :userid AND valid = :valid', array(
				':userid' => Yii::app()->user->getAbsoluteUsername($this->username),
				':valid' => 1
			));

			if($user){
				// Check if password is correct using the old algorithm and new one
				if($user->pass != md5($this->password) && !Yii::app()->security->validatePassword($this->password, $user->pass)){
					// Old (md5) or new (crypt()) password encryption methods failed to
					// match the entered password with the stored one, which means
					// the entered password is wrong

					// Clear the variable so the code below doesn't get confused
					$user = false;
				}
			}

			if ($user) {

				// If the user's current password uses the old encryption
				// mechanism, update it to the new encryption mechanism here
				// (during login), since now we have his plaintext password
				// since he just entered it during login
				if(!Yii::app()->security->isPasswordModern($user->pass)){

					$user->scenario = 'changePassword';
					$newEncryptedPassword = Yii::app()->security->hashPassword($this->password);

					$oldHashedPwdForReference = $user->pass;
					if($newEncryptedPassword != $user->pass){
						$user->pass = $newEncryptedPassword;
						$user->save(false);

						// Failsafe check, in case the new hashed password we just saved
						// is not correctly saved (e.g. migrations were not run on core_user.pwd)
						// restore the old md5 based password

						$user->refresh();
						if($user->pass != $newEncryptedPassword){
							$user->pass = $oldHashedPwdForReference;
							$user->save(false);
							Yii::log('Can not save password in new encryption algorithm. Migrations run?', CLogger::LEVEL_ERROR);
						}
					}
				}

				$res = true;

                // Trigger event to let plugins make further checks on the user
                // and possibly prevent a login (for some reason
                $event = new DEvent($this, array('user' => $user));
                Yii::app()->event->raise("ValidateUserLogin", $event);
                if($event->shouldPerformAsDefault()) {
                    $this->_id = $user->idst;
                    Yii::app()->user->oldLmsUserSignIn($this->_id);
                    // Refresh this user's assigned users if he is a PU
                    
                    if (!Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
                        $this->refreshPuAssignedUsers($user);
                    }

					Yii::app()->event->raise('UserLoginPostProcess', new DEvent($this, array(
						'user' => $user
					)));
                }
			} else {
				// Give plugins a second chance to validate user login
				$event = new DEvent($this, array (
					'username' => $this->username,
					'password' => $this->password,
				));
				Yii::app()->event->raise("UserLoginFailed", $event);

				// If LDAP (or another plugin) already logged in the user but are not forcing
				// redirection, we'll end up here with "logged_in" = true. This allows the Mobile App
				// to work with LDAP as well.
				if(isset($event->return_value['logged_in']) && $event->return_value['logged_in'])
					$res = true;
			}
		}

		return $res;
	}

	public function checkUserAlreadyLoggedIn()
	{
		if(Settings::get('stop_concurrent_user', 'off') === 'off')
			return false;

		$check = LearningTracksession::model()->findAllByAttributes(array('idUser' => $this->_id, 'active' => 1));

		if(empty($check))
			return false;

		foreach($check as $ts)
			if(Yii::app()->localtime->diff($ts->lastTime, Yii::app()->localtime->getLocalNow()) < 300)
				return true;

		return false;
	}


	/**
	 * @param $idst
	 * @param $username
	 * @return bool
	 */
	public function authenticateFromOldLms($idst, $username) {
		$res = false;

		$user = CoreUser::model()->findByPk($idst);
		if ($user && $user->idst == $idst) {
			// Username check was here, removed because it was way too problematic
			$this->_id = (int)$idst;
			$res = true;
		}
		return $res;
	}


	public function getId() {
		return $this->_id;
	}

	/*
	 * See if the user is blocked due to reaching the maximum failed
	 * login attempts. If true, reset the failed login attempts counter (cookie)
	 * and redirect to the login page with an appropriate error message.
	 */
	public function  isUserHacker(){
		$maxLoginAttemptsReached = (boolean) $_COOKIE['max_login_attempts_reached_expire_time'];

		$ret = false;

		if($maxLoginAttemptsReached){
			$ret = true;

			// Reset the failed login attempts counter
			setcookie('failed_login_attempts', 0, time()-3600);
		}

		return $ret;
	}

	/*
	 * Logs how much times the user has failed to login in a cookie.
	 * If a specified number of times is reached, the system blocks him
	 * for a specified amount of minutes from logging in with any login details.
	 */
	public function logFailedLoginAttempt(){

		// How much failed login attempts in the last 10 minutes?
		$loginAttempts = ((int)$_COOKIE['failed_login_attempts']) + 1;

		// Set a new cookie with the old value +1 for the next 10 minutes
		setcookie('failed_login_attempts', $loginAttempts, time()+60*10);


		$maxLogAttempts = Settings::get('max_log_attempt', null, true);


		// The user has reached the maximum failed attempts amount in the last 10 minutes
		if($maxLogAttempts && $loginAttempts >= $maxLogAttempts){

			// Did we already block him (by setting a cookie)
			$blockCookieExists = (boolean) $_COOKIE['max_login_attempts_reached_expire_time'];

			if( !$blockCookieExists ){
				// Set a cookie that will block the user from any further login attempts
				// for a specified amount of minutes ($this->failed_logins_wait_minutes).
				// Note: The cookie's value will be its expire time (seconds since UNIX epoch)

				$expireTime = (time()+($this->failed_logins_wait_minutes * 60));
				setcookie('max_login_attempts_reached_expire_time', $expireTime, $expireTime);
			}
		}
	}


	/**
	 * Authenticate the user without checking his password. This is
	 * used in some SSO situations and in login via REST authentication token
	 *
	 * @return bool
	 */
	public function authenticateWithNoPassword($skipOldSignin=false) {

		$res = false;

		if (!empty($this->username)) {
			$user = CoreUser::model()->find('userid = :userid AND valid = :valid', array(
					':userid' => CoreUser::getAbsoluteUsername($this->username),
					':valid' => 1
			));

			if ($user) {
				$res = true;
				$this->_id = $user->idst;
				if (!$skipOldSignin) {
					Yii::app()->user->oldLmsUserSignIn($this->_id);
				}

				// Refresh this user's assigned users if he is a PU
//				if (!Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
				    $this->refreshPuAssignedUsers($user);
//				}
			}
		}

		return $res;

	}

	/**
	 * Authenticate with email instead of username
	 * For now this is used during login from the SSO icons (Facebook, LinkedIn, Google Apps, etc)
	 * @return boolean
	 */
	public function authenticateWithEmail(){

		if (!empty($this->username)) {
			$user = CoreUser::model()->find('email = :email AND valid = :valid', array(
					':email' => $this->username,
					':valid' => 1
			));

			if ($user) {
				$this->_id = $user->idst;
				Yii::app()->user->oldLmsUserSignIn($this->_id);

				// Refresh this user's assigned users if he is a PU
				if (!Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
				    $this->refreshPuAssignedUsers($user);
				}
				return true;
			}
		}

		return false;
	}


	/**
	 * Update power user visibility, if necessary. This happens at every user access/login
	 *
	 * @param CoreUser $user
	 *
	 * @return bool
	 */
	private function refreshPuAssignedUsers(CoreUser $user){
		// NOTE: we can't call "Yii::app()->user->getIsPu()" here yet, because accesses are
		// not initialized, so we have to check if the current user is a PU manually
		$group = CoreGroup::model()->findByAttributes(array('groupid' => Yii::app()->user->level_admin));
		if(!$group){
			// The PU group is not defined
			return false;
		}
		$level = CoreGroupMembers::model()->findByAttributes(array('idst' => $group->idst, 'idstMember' => $user->idst));
		if ($level) { //authenticated user is a power user
			$rs = CoreUserPU::model()->updatePowerUserSelection($user->idst);
			$rs = CoreUserPuCourse::model()->updatePowerUserSelection($user->idst);
		}
	}

}