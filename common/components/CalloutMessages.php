<?php
/**
 * Component to handle callout messages (load, read)
 */
class CalloutMessages
{
	private static $_sessionKey = 'callout_messages';

	/*
	 * This method loads in session the callout messages
	 * for the current logged in user based on the priority
	 *
	 * @param $idst user
	 */
	public static function load($idst) {

		// callout messages are only loaded once per login session
		$queue = self::getQueue();
		if ($queue !== null) {
			return;
		}

		// get callouts messages from the erp
		$callouts = array();

		try
		{
			//echo 'retrieving callouts...<br>';
			$client = new CalloutApiClient('callout/api', array('type' => ''));
			$apiResult = $client->call('getCallouts');
			//var_dump($apiResult);exit;
			$callouts = $apiResult['data'];
		}
		catch (Exception $e)
		{
			/*echo $e->getCode() . " " . $e->getMessage();
			exit;*/
		}

		self::_saveQueue($callouts, $idst);

		//echo 'done...<br>';
		//var_dump(Yii::app()->session['callout_messages']);
	}

	/**
	 * Saves an array of callout messages in session
	 * User id is required to filter out read messages
	 *
	 * @param $messages array
	 * @param $idst int
	 */
	private static function _saveQueue($messages, $idst) {
		$queue = array();

		// first find out which of these messages are already read
		$readCalloutModels = CoreCalloutReadLog::model()->findAllByAttributes(array(
			'idst' => $idst
		));
		$readMessagesArray = array();
		if ($readCalloutModels) {
			// CHtml::listData iterates the array of CoreCalloutReadLog models and
			// returns an array having callout_id as the key and idst as the value
			$readMessagesArray = CHtml::listData($readCalloutModels, 'callout_id', 'idst');
			// we only need the keys, ie an array of callout ids
			$readMessagesArray = array_keys($readMessagesArray);
		}

		foreach ($messages as $message) {
			$id = $message['id'];

			// filter out read messages
			if (in_array($id, $readMessagesArray)) {
				continue;
			}

			// filter out expired messages
			if (!(false === strtotime($message['enddate']))) {
				// 'enddate' => string '2013-04-13 00:00:00'
				// re-format 'enddate' to include the end of the day represented
				$enddate = substr($message['enddate'], 0, 10) . ' 23:59:59';
                $enddateUtc = Yii::app()->localtime->toUtc($enddate);
				// result should be like this
				// 'enddate' => string '2013-04-13 23:59:59'
                $utcnow = Yii::app()->localtime->UTCNow;
				if (strtotime($utcnow) > strtotime($enddateUtc)) {
					continue;
				}
			}

			// filter out messages that have the startdate in the future
			if (!(false === strtotime($message['startdate']))) {
				// re-format current gmt date to include the end of the current day
				$today = substr(Yii::app()->localtime->UTCNow, 0, 10) . ' 23:59:59';
				// result should be like this
				// today => string '2013-04-13 23:59:59'
                $startdateUtc = Yii::app()->localtime->toUtc($message['startdate']);
				if (strtotime($startdateUtc) > strtotime($today)) {
					continue;
				}
			}

			$queue[$id] = $message;
		}
		Yii::app()->session[self::$_sessionKey] = $queue;
		//echo 'saved '.count($queue).' messages in session<br>';
		//var_dump($queue);
	}

	/**
	 * Reading a message will remove it from the message queue
	 *
	 * @param $calloutId
	 */
	public static function read() {
        $calloutId = Yii::app()->request->getParam('id');
        $calloutMessages = Yii::app()->session[self::$_sessionKey];

        if ($calloutId && $calloutMessages[$calloutId]) {
			unset($calloutMessages[$calloutId]);
			Yii::app()->session[self::$_sessionKey] = $calloutMessages;

			// save the read message in the db
			$readCallout = new CoreCalloutReadLog();
			$readCallout->idst = Yii::app()->user->getIdst();
			$readCallout->callout_id = $calloutId;
			echo $readCallout->save();
		}
	}

    /**
     * Only to be used for debugging purposes!
     */
    public static function clearReadLog() {
        CoreCalloutReadLog::model()->deleteAll();
    }

	/**
	 * Returns the queue of messages saved in session
	 *
	 * @return array|null
	 */
	public static function getQueue() {
		return (isset(Yii::app()->session[self::$_sessionKey])) ? Yii::app()->session[self::$_sessionKey] : null;
	}
}
