<?php
/**
 * General usage component. Provide Player functionality/services to all parts of the LMS
 *
 * Usage example:
 * 	No matter which part of LMS you are (widget, view, even in authoringm etc.) just check
 *
 * 	if (Yii::app()->player->course) {
 * 		echo Yii::app()->player->course->title  // course is LearningCourse model instance
 *  }
 *
 */
class Certificate extends CApplicationComponent {

    protected $_certificateRealSize;
	public $certificatePath;

    /**
     * (non-PHPdoc)
     * @see CApplicationComponent::init()
     */
    public function init() {

    }


    public function getCertificateRealSize() {
        return $this->_certificateRealSize;
    }

    public function setCertificateRealSize($value) {
        $this->_certificateRealSize = $value;
        return true;
    }

    /**
     * Creates a course certificate and assigns it to the user
     * @param $certificateId
     * @param $userId
     * @param $courseId
     * @param array $array_substitution
     * @param bool $download
     * @return bool
     */
    public function create($certificateId, $userId, $courseId, $array_substitution = array(), $download = false) {
        // Check if certificate exists
        $certificateModel = LearningCertificate::model()->findByPk($certificateId);
        if (empty($certificateModel))
            return false;

        try {
            // Start transaction
            $db = Yii::app()->db; /* @var $db CDbConnection */
            if(!Yii::app()->db->getActive())
                $transaction = $db->beginTransaction();

            // Pre-create the certificate assignment to generate the certificate number
            $certificateAssign = new LearningCertificateAssign();
            $certificateAssign->id_certificate = $certificateId;
            $certificateAssign->id_user = $userId;
            $certificateAssign->id_course = $courseId;
            $certificateAssign->on_date = Yii::app()->localtime->toLocalDateTime();
            if(!$certificateAssign->save())
                throw new Exception(implode('; ', $certificateAssign->getErrors()));

            // Reload to get the auto-increment certificate number
            $certificateAssign->refresh();

            // Play tags substitution with real values
            if(!$array_substitution)
                $array_substitution = array();
            $array_substitution = array_merge($array_substitution, array('[course_certificate_number]' => "C".$certificateAssign->cert_number));
            if(!empty($array_substitution))
                $certificateModel->cert_structure = str_replace(array_keys($array_substitution), $array_substitution, $certificateModel->cert_structure);
            $certificateModel->cert_structure = $this->replaceBaseUrl($certificateModel->cert_structure);

            // Prepare file name and paths

			$name = preg_replace('/[^\s|^\pL|^\pN|^\_|^\-|^\.]+/u', '', $certificateModel->name);

            $cert_file = $courseId .'_'. $certificateId .'_'. $userId .'_'. time() .'_'. $name .'.pdf';

            // Get background image URL, if any
            $bgImage = $certificateModel->getBgImageUrl(CoreAsset::VARIANT_ORIGINAL, true);

	        $event = new DEvent($this, array(
		        'certificateModel'=>$certificateModel
	        ));
	        Yii::app()->event->raise('getCertificatePaperSize', $event);

	        // Plugins may force the generated PDF to be of different paper size, e.g. "USLETTER" or "USLEGAL"
	        // The default is A4
	        $customFormat = $event->return_value ? $event->return_value : "A4";

	        // Generate PDF
            $pdfContent = $this->getPdf($certificateModel->cert_structure, $certificateModel->name, $bgImage, $certificateModel->orientation, false, false, true, $customFormat, $certificateModel->base_language);

            // Save the PDF into the local temporary folder
            $tempFolder = Docebo::getUploadTmpPath();
            $tempFilePath = $tempFolder . DIRECTORY_SEPARATOR . $cert_file;

            if (file_put_contents($tempFilePath, $pdfContent)) {
            	$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
            	$storage->store($tempFilePath);

            	@unlink($tempFilePath);

                $certificateAssign->cert_file = addslashes($cert_file);
                if(!$certificateAssign->save())
                    throw new Exception(implode('; ', $certificateAssign->getErrors()));
            } else
                throw new Exception("Unable to save certificate");

            // Commit transaction
            if($transaction)
                $transaction->commit();

            return true;

        } catch(Exception $e) {
            if($transaction)
                $transaction->rollback();

			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            return false;
        }
    }

    /**
     * Creates a learning plan certificate and assigns it to the user
     * @param $certificateId
     * @param $userId
     * @param $learningPlanId
     * @param array $array_substitution
     * @param bool $download
     * @return bool
     */
    public function createForLearningPlan($certificateId, $userId, $learningPlanId, $array_substitution = array(), $download = false) {
        // Check if certificate exists
        $certificateModel = LearningCertificate::model()->findByPk($certificateId);
        if (empty($certificateModel))
            return false;

        try {
            // Start transaction
            $db = Yii::app()->db; /* @var $db CDbConnection */
			if ($db->getCurrentTransaction() === NULL) { $transaction = $db->beginTransaction(); }

            // Pre-create the certificate assignment to generate the certificate number
            $certificateAssign = new LearningCertificateAssignCp();
            $certificateAssign->id_certificate = $certificateId;
            $certificateAssign->id_user = $userId;
            $certificateAssign->id_path = $learningPlanId;
            $certificateAssign->on_date = Yii::app()->localtime->toLocalDateTime();
            if(!$certificateAssign->save())
                throw new Exception(implode('; ', $certificateAssign->getErrors()));

            // Reload to get the auto-increment certificate number
            $certificateAssign->refresh();

            // Play tags substitution with real values
            if(!$array_substitution)
                $array_substitution = array();

            $array_substitution = array_merge($array_substitution, array(
                  '[learning_plan_certificate_number]' => "LP".$certificateAssign->cert_number,
				  '[learning_plan_certification_date]' => Yii::app()->localtime->stripTimeFromLocalDateTime($certificateAssign->on_date)
                )
            );

			// Format dates
			$array_substitution = CertificateTag::checkForTagOptions($array_substitution, $learningPlanId, $learningPlan = true);

            if(!empty($array_substitution))
                $certificateModel->cert_structure = str_replace(array_keys($array_substitution), $array_substitution, $certificateModel->cert_structure);
            $certificateModel->cert_structure = $this->replaceBaseUrl($certificateModel->cert_structure);

			$name = preg_replace('/[^\s|^\pL|^\pN|^\_|^\-|^\.]+/u', '', $certificateModel->name);

            // Prepare file name and paths
            $cert_file = 'lp_' . $learningPlanId .'_'. $certificateId .'_'. $userId .'_'. time() .'_'. $name .'.pdf';

            // Get background image URL, if any
            $bgImage = $certificateModel->getBgImageUrl(CoreAsset::VARIANT_ORIGINAL, true);

	        $event = new DEvent($this, array(
		        'certificateModel'=>$certificateModel
	        ));
	        Yii::app()->event->raise('getCertificatePaperSize', $event);

	        // Plugins may force the generated PDF to be of different paper size, e.g. "USLETTER" or "USLEGAL"
	        // The default is A4
	        $customFormat = $event->return_value ? $event->return_value : "A4";

            // Generate PDF
            $pdfContent = $this->getPdf($certificateModel->cert_structure , $certificateModel->name, $bgImage, $certificateModel->orientation, false, false, true, $customFormat, $certificateModel->base_language);

            // Save the PDF into the local temporary folder
            $tempFolder = Docebo::getUploadTmpPath();
            $tempFilePath = $tempFolder . DIRECTORY_SEPARATOR . $cert_file;


            if (file_put_contents($tempFilePath, $pdfContent)) {
            	$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_CERTIFICATE);
            	$storage->store($tempFilePath);

            	@unlink($tempFilePath);

            	$certificateAssign->cert_file = addslashes($cert_file);
            	if(!$certificateAssign->save())
            		throw new Exception(implode('; ', $certificateAssign->getErrors()));
            } else
            	throw new Exception("Unable to save certificate");

            // Commit transaction
            if($transaction) {
                $transaction->commit();
            }



            return true;

        } catch(Exception $e) {
            if($transaction)
                $transaction->rollback();

            return false;
        }
    }


	/**
	 *
	 * @param unknown     $html
	 * @param unknown     $name
	 * @param bool|string $img
	 * @param string      $orientation
	 * @param bool|string $download
	 * @param bool|string $facs_simile
	 * @param bool|string $for_saving
	 * @param string      $customPaperFormat
	 *
	 * @return Ambigous <string, buffer, mixed>
	 */
    public function getPdf($html, $name, $img = false, $orientation = 'P', $download = true, $facs_simile = false, $for_saving = false, $customPaperFormat = "A4", $language = '') {
        require_once(_base_.'/common/vendors/tcpdf/pdf.php');

        if(!$this->_certificateRealSize) {
            $pdf = new PDF($orientation, 'mm', $customPaperFormat, $language);
        } else {
            $pdf = new PDFRS($orientation, 'px', $customPaperFormat, $language);
        }

	    $bgImageDimens = array(
		    // A4 standard dimensions in "mm"
		    'width'=>($orientation=='P' ? 210 : 297),
		    'height'=>($orientation == 'P' ? 297 : 210),
	    );
	    if($customPaperFormat=='USLEGAL'){
		    $bgImageDimens = array( // in mm:
			    'width'=>($orientation=='P' ? 215.9 : 355.6),
			    'height'=>($orientation == 'P' ? 355.6 : 215.9),
		    );
	    }

        if($for_saving) {
            return $pdf->getPdf($html, $name, $img, $download, $facs_simile, $for_saving, $bgImageDimens);
        } else {
            $pdf->getPdf($html, $name, $img, $download, $facs_simile, $for_saving, $bgImageDimens);
        }
    }

    public function replaceBaseUrl($content) {
        $fullAdminUrl = Yii::app()->request->getBaseUrl(true);
        $fullBaseUrl = str_replace('/admin', '/', $fullAdminUrl);
        return str_replace("{site_base_url}", $fullBaseUrl, $content);
    }

}