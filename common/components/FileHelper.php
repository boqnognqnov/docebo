<?php
/**
 * FileHelper extends Yii's CFileHelper class prividing more methods for file operations
 *
 * Author: Andrei Amariutei
 */
class FileHelper extends CFileHelper
{
	
	/**
	 * Remove a file and only if it is a file
	 * 
	 * @param string $file
	 * @return boolean
	 */
	public static function removeFile($file)
	{
		if (!file_exists($file) || !is_file($file)) return TRUE;
		@unlink($file);
		return true;
	}
	


	/**
	 * Removes a directory recursively.
	 * 
	 * We need to return true, so, just override and return true :-)
	 * Also, use '@'. 
	 * 
	 * @param string $directory
	 * @return boolean
	 */
	public static function removeDirectory($directory) {
		
		$items=glob($directory.DIRECTORY_SEPARATOR.'{,.}*',GLOB_MARK | GLOB_BRACE);
		foreach($items as $item)
		{
			if(basename($item)=='.' || basename($item)=='..')
				continue;
			if(substr($item,-1)==DIRECTORY_SEPARATOR)
				self::removeDirectory($item);
			else
				@unlink($item);
		}
		if(is_dir($directory)) {
			@rmdir($directory);
		}
		
		return true;
		
	}
	
	/**
	 * 
	 * @param unknown $size
	 * @param string $unit
	 * @return string
	 */
	public static function humanFileSize($size, $unit = "")
	{
		if ((!$unit && $size >= 1 << 30) || $unit == "GB")
			return number_format($size / (1 << 30), 2) . "GB";
		if ((!$unit && $size >= 1 << 20) || $unit == "MB")
			return number_format($size / (1 << 20), 2) . "MB";
		if ((!$unit && $size >= 1 << 10) || $unit == "KB")
			return number_format($size / (1 << 10), 2) . "KB";

		return number_format($size) . " bytes";
	}

}
