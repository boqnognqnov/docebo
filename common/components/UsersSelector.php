<?php

/**
 * A compagnion component for the New UsersSelector mechanic, comprising of:
 * 
 * 		lms/protected/controllers/UsersSelectorController.php
 * 		themes/spt/js/usersselector.js
 * 		themes/spt/css/usersselector.css
 * 		...
 * 
 *
 */
class UsersSelector extends CApplicationComponent {

	/**
	 * Default selector name
	 * @var unknown
	 */
	const NAME = 'uni-selector';

	/**
	 * Selector types, as per its purpose/zone of usage. Assigned to this->type
	 * @var string
	 */
	const TYPE_GROUPS_MANAGEMENT = 'groups';
	const TYPE_POWERUSER_MANAGEMENT = 'puman';
	const TYPE_COURSE_ENROLLMENT = 'enroll';
	const TYPE_CATALOG_MANAGEMENT = 'catalogman';
	const TYPE_CATALOG_MANAGEMENT_COURSES = 'catalogmancourses';
	const TYPE_CURRICULA_MANAGEMENT = 'curricula';
	const TYPE_CLASSROOM_ENROLLMENT = 'classroomenroll';
	const TYPE_MODULE_MANAGEMENT = 'moduleman';
	const TYPE_CUSTOM_REPORTS_COURSES = 'reportcourses';
	const TYPE_CUSTOM_REPORTS_USERS = 'reportsusers';
	const TYPE_ENROLLRULES_BRANCHES = 'enrollrulesbranches';
	const TYPE_ENROLLRULES_GROUPS = 'enrollrulesgroups';
	const TYPE_ENROLLRULES_COURSES = 'enrollrulescourses';
	const TYPE_NEWSLETTER_RECIPIENTS = 'newsletterusers';
	const TYPE_GAMIFICATION_ASSIGN_BADGES = 'gamassignbadges';
	const TYPE_ORGCHART_ASSIGN_USERS = 'orgchartassign';
	const TYPE_SUBSCRIPTIONCODES_COURSES = 'subscodescourses';
	const TYPE_MASS_ENROLLMENT_USERS = 'massenrollusers';
	const TYPE_MASS_ENROLLMENT_COURSES = 'massenrollcourses';
	const TYPE_NOTIFICATIONS_USER_FILTER = 'notifufilter';
	const TYPE_NOTIFICATIONS_COURSES_ASSOC = 'notifassoc';
	const TYPE_CERTIFICATIONS_ASSIGN_ITEMS = 'certitems';
	const TYPE_DASHBOARD_USER_FILTER = 'dashufilter';
	const TYPE_DASHBOARD_PU_PROFULES = 'dashpuprofiles';
	const TYPE_WEBINAR_ENROLLMENT = 'webinarenroll';
	const TYPE_COURSEPATH_MASS_ENROLLMENT = 'coursepathmassenroll';
	const TYPE_MASS_ENROLL_LP_USERS = 'massenrolllpusers';
	const TYPE_MASS_ENROLL_LP_COURSES = 'massenrolllpcourses';
	const TYPE_ASSIGN_USERS_COACHING = 'assignuserscoaching';
	//Filter Menu Items for certain users
	const TYPE_FILTER_MENU_ITEMS_FOR_USERS = 'filtermenuitems';
	const TYPE_APP7020_ASSIGN_EXPERTS = 'assignexperts';
	const TYPE_APP7020_ADD_EXPERTS = 'addexperts';
	const TYPE_APP7020_EXPERTS = 'getexperts';
	const TYPE_APP7020_TOPICS = 'gettopics';
	const TYPE_CUSTOM_REPORT_CERTIFICATION = 'certitemsreport';
	const TYPE_GAMIFICATION_LEADERBOARD = 'leaderboard';
	const TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD = 'leaderboard_no_wizard';
    const TYPE_GAMIFICATION_CONTEST_NO_WIZARD = 'contest_no_wizard';
    const TYPE_CUSTOM_REPORT_BADGES = 'badgesreport';
    const TYPE_REPORT_VISIBILITY = 'report_visibility';
	const TYPE_CUSTOM_REPORT_CONTESTS = 'contestsreport';
	const TYPE_REWARDS_SET_FILTER = 'rewards_set';
	const TYPE_AUTOMATION_RULE = 'automation_rule_wizard';
	const TYPE_CENTRALREPO_PUSH_TO_COURSE = 'centralrepo_push_lo';
	const TYPE_CUSTOM_REPORT_ASSETS_STATS = 'report_assets_stats';
	const TYPE_CUSTOM_REPORT_CHANNELS_STATS = 'report_channels_stats';
	const TYPE_CUSTOM_REPORT_EXPERT_STATS = 'report_experts_stats';

	/**
	 * INPUT field names for various selections, POSTed upon dialog submission
	 * @var string
	 */
	const FIELD_SELECTED_USERS = 'selected_users';
	const FIELD_SELECTED_GROUPS = 'selected_groups';
	const FIELD_SELECTED_ORGCHARTS = 'orgcharts_selected';
	const FIELD_SELECTED_ORGCHARTS_JSON = 'orgcharts_selected-json';
	const FIELD_SELECTED_COURSES = 'selected_courses';
	const FIELD_SELECTED_PLANS = 'selected_plans';
	const FIELD_SELECTED_PU_PROFILES = 'selected_puprofiles';
	const FIELD_SELECTED_CERTIFICATIONS = 'selected_certifications';
    const FIELD_SELECTED_BADGES = 'selected_badges';
	const FIELD_SELECTED_CONTESTS = 'selected_contests';
	const FIELD_SELECTED_ASSETS = 'selected_assets';
	const FIELD_SELECTED_CHANNELS = 'selected_channels';
	const FIELD_SELECTED_EXPERTS = 'selected_experts';
	
	/**
	 * Orgchart Select State, as per FancyTree; NOTE: this is really hard coded! Check FancyTree!
	 * @var unknown
	 */
	const ORGCHART_THIS_ONLY = 1;   // brnach is selected as "this only"
	const ORGCHART_THIS_AND_DESC = 2; // brnach is selected as "this and descendants"

	/**
	 * Possible selector Tabs in selector
	 * @var string
	 */
	const TAB_USERS = 'users';
	const TAB_GROUPS = 'groups';
	const TAB_ORGCHARTS = 'orgcharts';
	const TAB_COURSES = 'courses';
	const TAB_PLANS = 'plans';
	const TAB_PU_PROFILES = 'puprofiles';
	const TAB_CERTIFICATION = 'certification';
    const TAB_BADGES = 'badges';
	const TAB_CONTESTS = 'contests';
	const TAB_ASSETS = 'assets';
	const TAB_CHANNELS = 'channels';
	
	/**
	 * Name of the top level session key used to PASS various data to selector(s), if any and if selector is advised to USE it!
	 * Individual selectors' data is saved in one level down: uniSelectorData[<selector-name>]. 
	 * So, if you need/handle data designated to "my-selector" you should refer to  ->session['uniSelectorData']['my-selector'].
	 * 
	 * @var string
	 */
	const SELECTOR_SESSION_KEY = 'uniSelectorData';

	/**
	 * List of tabs for Users Selector Combo variant
	 * @var array
	 */
	public static $TABS_PRESET_USERS_COMBO = array(
		self::TAB_USERS,
		self::TAB_GROUPS,
		self::TAB_ORGCHARTS,
	);

	/**
	 * List of tabs for Courses selector variant 
	 * @var array
	 */
	public static $TABS_PRESET_COURSES_ONLY = array(
		self::TAB_COURSES,
	);

	public static $TABS_CERTIFICATION_ONLY = array(
		self::TAB_CERTIFICATION
	);
    public static $TABS_BADGES_ONLY = array(
        self::TAB_BADGES
    );

	/**
	 * List of tabs for Groups + Charts only 
	 * @var array
	 */
	public static $TABS_PRESET_GROUPS_CHARTS_ONLY = array(
		self::TAB_GROUPS,
		self::TAB_ORGCHARTS,
	);

	/**
	 * List of tabs for Courses + Plans only
	 * @var array
	 */
	public static $TABS_PRESET_COURSES_PLANS_ONLY = array(
		self::TAB_COURSES,
		self::TAB_PLANS,
	);

	/**
	 * List of tabs for Power Users Profiles only
	 * @var array
	 */
	public static $TABS_PRESET_PU_PROFILES_ONLY = array(
		self::TAB_PU_PROFILES,
	);

	/**
	 * List of selector types NOT showing LEGEND in Org Chart screen
	 * @var array
	 */
	public static $LEGEND_DISABLED_TYPES = array(
		self::TYPE_ENROLLRULES_BRANCHES,
	);

	public static $TABS_PRESET_ASSETS_AND_CHANNELS = array(
	    self::TAB_ASSETS,
	    self::TAB_CHANNELS,
	);
	
	
	/**
	 * Return an array of enabled Tabs in selector. Not included tabs will NOT be shown nor rendered as if they do not exists at all!
	 * 
	 * @param string $selectorType
	 * @return array
	 */
	public static function enabledTabs($selectorType) {

		switch ($selectorType) {

			case self::TYPE_CUSTOM_REPORTS_COURSES:
				return self::$TABS_PRESET_COURSES_ONLY;
				break;

            case self::TYPE_CUSTOM_REPORT_BADGES:
                return self::$TABS_BADGES_ONLY;
                break;

			case self::TYPE_CUSTOM_REPORT_CERTIFICATION:
				return self::$TABS_CERTIFICATION_ONLY;
				break;

			case self::TYPE_ENROLLRULES_BRANCHES:
				return array(self::TAB_ORGCHARTS);
				break;

			case self::TYPE_ENROLLRULES_GROUPS:
				return array(self::TAB_GROUPS);
				break;

			case self::TYPE_ENROLLRULES_COURSES:
			case self::TYPE_SUBSCRIPTIONCODES_COURSES:
			case self::TYPE_MASS_ENROLLMENT_COURSES:
			case self::TYPE_MASS_ENROLL_LP_COURSES:
				return array(self::TAB_COURSES);
				break;

			case self::TYPE_ORGCHART_ASSIGN_USERS:
			case self::TYPE_ASSIGN_USERS_COACHING:
			case self::TYPE_APP7020_ASSIGN_EXPERTS:
			case self::TYPE_APP7020_ADD_EXPERTS:
				return array(self::TAB_USERS);
				break;

			case self::TYPE_NOTIFICATIONS_USER_FILTER:
			case self::TYPE_GAMIFICATION_LEADERBOARD:
			case self::TYPE_GAMIFICATION_LEADERBOARD_NO_WIZARD:
			case self::TYPE_REWARDS_SET_FILTER:
            case self::TYPE_GAMIFICATION_CONTEST_NO_WIZARD:
				return self::$TABS_PRESET_GROUPS_CHARTS_ONLY;
				break;

            case self::TYPE_CATALOG_MANAGEMENT_COURSES:	
			case self::TYPE_NOTIFICATIONS_COURSES_ASSOC:
				return self::$TABS_PRESET_COURSES_PLANS_ONLY;
				break;

			case self::TYPE_CERTIFICATIONS_ASSIGN_ITEMS:
				return self::$TABS_PRESET_COURSES_PLANS_ONLY;
				break;


			case self::TYPE_MODULE_MANAGEMENT:
				return self::$TABS_PRESET_GROUPS_CHARTS_ONLY;
				break;

			case self::TYPE_DASHBOARD_PU_PROFULES:
				return self::$TABS_PRESET_PU_PROFILES_ONLY;
				break;


			case self::TYPE_FILTER_MENU_ITEMS_FOR_USERS:
				return self::$TABS_PRESET_GROUPS_CHARTS_ONLY;
				break;
				
			case self::TYPE_CUSTOM_REPORT_ASSETS_STATS:
			    return self::$TABS_PRESET_ASSETS_AND_CHANNELS;
			    break;

		    case self::TYPE_CUSTOM_REPORT_CHANNELS_STATS:
		        return array(self::TAB_CHANNELS);
		        break;
			         
			default:
				return self::$TABS_PRESET_USERS_COMBO;
		}

		return array();
	}

	/**
	 * Check if LEGEND is enabled. 
	 * Legend is the visual hint to the user describing possible BRANCH selection status in Org Charts tab pane.
	 * SOME selectors do NOT need it. 
	 * 
	 * @param string $selectorType
	 * @return boolean
	 */
	public static function orgChartLegendEnabled($selectorType) {
		if (in_array($selectorType, self::$LEGEND_DISABLED_TYPES)) {
			return false;
		}
		return true;
	}

	/**
	 * Check if given Tab is enabled for given selector type
	 * 
	 * @param string $selectorType
	 * @param string $tab
	 * @return boolean
	 */
	public static function tabIsEnabled($selectorType, $tab) {
		return in_array($tab, self::enabledTabs($selectorType));
	}

	/**
	 * Get list of courses directly selected
	 * @param array $data
	 */
	public static function getCoursesListOnly($data) {
		$tmp = array();
		if (isset($data[self::FIELD_SELECTED_COURSES])) {
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_COURSES]);
			return $tmp;
		}
		return $tmp;
	}

	/**
	 * Get list of learning plans directly selected
	 * @param array $data
	 */
	public static function getPlansListOnly($data) {
		$tmp = array();
		if (isset($data[self::FIELD_SELECTED_PLANS])) {
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_PLANS]);
			return $tmp;
		}
		return $tmp;
	}

	/**
	 * Get list of selected power users profiles
	 * @param array $data
	 */
	public static function getPuProfilesList($data) {
		$tmp = array();
		if (isset($data[self::FIELD_SELECTED_PU_PROFILES])) {
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_PU_PROFILES]);
			return $tmp;
		}
		return $tmp;
	}

	/**
	 * Get list of users directly selected
	 * @param array $data
	 * @param array $includeUsers
	 */
	public static function getUsersListOnly($data, $includeUsers = false) {
		$tmp = array();
		if (isset($data[self::FIELD_SELECTED_USERS])) {
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_USERS]);
			// Check if we have to filter the users list by $includeUsers
			if (is_array($includeUsers)) {
				$tmp = array_intersect($tmp, $includeUsers);
			}
			return $tmp;
		}
		return $tmp;
	}

    public static function getBadgesListOnly($data, $includeBadges = false){
        $tmp = array();
        if(isset($data[self::FIELD_SELECTED_BADGES])){
            $tmp = CJSON::decode($data[self::FIELD_SELECTED_BADGES]);

            if(is_array($includeBadges)){
                $tmp = array_intersect($tmp, $includeBadges);
            }

            return $tmp;
        }

        return $tmp;
    }

	public static function getCertificationsListOnly($data, $includeCertifications = false){
		$tmp = array();
		if(isset($data[self::FIELD_SELECTED_CERTIFICATIONS])){
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_CERTIFICATIONS]);

			if(is_array($includeCertifications)){
				$tmp = array_intersect($tmp, $includeCertifications);
			}

			return $tmp;
		}

		return $tmp;
	}

	public static function getContestsListOnly($data, $includeContests = false){
		$tmp = array();
		if(isset($data[self::FIELD_SELECTED_CONTESTS])){
			$tmp = CJSON::decode($data[self::FIELD_SELECTED_CONTESTS]);

			if(is_array($includeContests)){
				$tmp = array_intersect($tmp, $includeContests);
			}

			return $tmp;
		}

		return $tmp;
	}

	/**
	 * Get list of groups directly selected
	 * @param array $data
	 * @param array $includeGroups
	 */
	public static function getGroupsListOnly($data, $includeGroups = false) {
		$groupsArray = CJSON::decode($data[self::FIELD_SELECTED_GROUPS]);
		// Filter by list of allowed groups (in case of PU, for example)
		if (is_array($includeGroups)) {
			$groupsArray = array_intersect($groupsArray, $includeGroups);
		}
		return $groupsArray;
	}

	/**
	 * Get list of branches directly selected, usual structure (key => value,  selectState => value)
	 * @param array $data
	 */
	public static function getBranchesListOnly($data) {
		return CJSON::decode($data[self::FIELD_SELECTED_ORGCHARTS_JSON]);
	}

	public static function getAssetsListOnly($data, $includeItems=false){
	    return self::getItemsListOnly($data, self::FIELD_SELECTED_ASSETS, $includeItems);
	}
	
	public static function getChannelsListOnly($data, $includeItems=false){
	    return self::getItemsListOnly($data, self::FIELD_SELECTED_CHANNELS, $includeItems);
	}
	
	public static function getExpertsListOnly($data, $includeItems=false){
	    return self::getItemsListOnly($data, self::FIELD_SELECTED_USERS, $includeItems);
	}

	/**
	 * General method to get list of ITEMS directly selected in a selector
	 * 
	 * @param array $data Array, haing selectItemsField (json) in it
	 * @param string $field
	 * @param array $includeUsers
	 */
	public static function getItemsListOnly($data, $selectItemsField, $includeItems = false) {
	    $tmp = array();
	    if (isset($data[$selectItemsField])) { 
	        $tmp = CJSON::decode($data[$selectItemsField]);
	        // Check if we have to filter the users list by $includeUsers
	        if (is_array($includeItems)) {
	            $tmp = array_intersect($tmp, $includeItems);
	        }
	        return $tmp;
	    }
	    return $tmp;
	}

	/**
	 * Extract a complete list of USERS (their IDs) from $data, including those derived from selected GROUPS and ORG Charts.
	 * Applies POWER USER filtering if current user is a power user OR if requested to filter by arbitrary Power User ID (idAdmin).
	 *
	 * Since this method is working with HTTP Request (i.e. user data), more restrictions could be applied here
	 * to protect from fraud or missbehave (like posting unallowed list of users/groups to subscribe to a course)
	 *
	 * @param array $data An array of FULL data coming as POST/REQUEST to be analyzed/searched.
	 * @param bool|string $idAdmin Optional Power USer ID to filter the data against; overrides current user PU ID, if any
	 * @param bool $excludeSuspended
	 * @return array
	 */
	public static function getUsersList($data, $idAdmin = false, $excludeSuspended = false) {

		$result = array();

		// Check if we were passed a Power USer ID or if NOT, check current user
		if (($idAdmin === false) && Yii::app()->user->getIsPu()) {
			$idAdmin = Yii::app()->user->id;
		}

		// Restrict groups IF user is a PowerUser by getting the full list of PU groups (direct groups, org chart groups, etc.)
		$includeGroups = false;
		$includeUsers = false;
		if ((int) $idAdmin > 0) {
			$includeGroups = CoreAdminTree::getPowerUserFullListOfGroups($idAdmin);
			$includeUsers = CoreUserPU::getList($idAdmin);
		}

		// Direct USERS
		$list = self::getUsersListOnly($data, $includeUsers);
		if (is_array($list) && !empty($list)) {
			$result = array_merge($result, $list);
		}


		// Groups
		if (isset($data[self::FIELD_SELECTED_GROUPS])) {

			$groupsArray = self::getGroupsListOnly($data, $includeGroups);

			if (is_array($groupsArray) && !empty($groupsArray)) {
				$members = CoreGroupMembers::model()->findAllByAttributes(array('idst' => $groupsArray));
				$tmp = CHtml::listData($members, 'idstMember', 'idstMember');
				if($excludeSuspended && !empty($tmp)) {
					$tmp = Yii::app()->db->createCommand()
						->select('cu.idst')
						->from(CoreUser::model()->tableName().' cu')
						->where('cu.valid=1')
						->andWhere(array('IN', 'cu.idst', $tmp))
						->queryColumn();
				}
				if (!empty($tmp)) {
					$result = array_merge($result, $tmp);
				}
			}
		}

		// ORG Charts
		if (isset($data[self::FIELD_SELECTED_ORGCHARTS_JSON])) {

			// Get selected branches info from data
			$branchesArray = self::getBranchesListOnly($data);

			// It must be an array and must have some elements
			if (is_array($branchesArray) && !empty($branchesArray)) {

				// Initialize array of user IDs
				$userIds = array();

				// Enumerate all selected branches
				// Incoming array of selected branches is an array of array('key' => <idOrg>,  'selectState' => <1 | 2>)
				// Apply filtering by $includeGroups
				foreach ($branchesArray as $index => $info) {
					$orgModel = CoreOrgChartTree::model()->findByPk($info['key']);

					if ($info['selectState'] == self::ORGCHART_THIS_ONLY) { // oc_
						$userIds = array_merge($userIds, $orgModel->getNodeUsers($includeGroups, false, false, $excludeSuspended));
					} else if ($info['selectState'] == self::ORGCHART_THIS_AND_DESC) {  // ocd_
						$userIds = array_merge($userIds, $orgModel->getBranchUsers(true, $includeGroups, false, $excludeSuspended));
					}
				}
				$result = array_merge($result, $userIds);
			}
		}


		// Filter by $includeUsers array, if any (for example, if we are filtering users by Power User
		if (is_array($includeUsers)) {
			$result = array_intersect($result, $includeUsers);
		}

		// Make the array unique by values
		$result = array_unique($result);

		return $result;
	}

	/**
	 * 
	 * @param array  $data  TWO elements array(<org-charts-selected-as-only>,  <org-charts-selected-with-desc>)
	 */
	public static function getSelectedOrgchartsInfo($data, $puCheck = false) {

		// Orgcharts
		$orgChartList = array();
		$orgChartDescList = array();
		$isPowerUser = Yii::app()->user->getIsPu();
		$powerUserBranchesArray = array();
		if($isPowerUser){
			$powerUserBranchesArray = CoreOrgChartTree::getPuBranchIds();
		}

		if (!empty($data[UsersSelector::FIELD_SELECTED_ORGCHARTS_JSON])) {
			$orgcharts = CJSON::decode($data[UsersSelector::FIELD_SELECTED_ORGCHARTS_JSON]);
			foreach ($orgcharts as $info) {
				switch ((int) $info['selectState']) {
					case UsersSelector::ORGCHART_THIS_ONLY:
						$ar = CoreOrgChartTree::model()->findByPk($info['key']);
						if($puCheck && $isPowerUser){
							if(!$powerUserBranchesArray || (is_array($powerUserBranchesArray) && empty($powerUserBranchesArray))){
								$powerUserBranchesArray = CoreOrgChartTree::getPuBranchIds();
							}

							if(is_array($powerUserBranchesArray) && in_array($ar->idOrg, $powerUserBranchesArray)){
								$orgChartList[] = $ar->idst_oc;
							}
						}else{
							if ($ar) {
								$orgChartList[] = $ar->idst_oc;
							}
						}

						break;
					case UsersSelector::ORGCHART_THIS_AND_DESC:
						$ar = CoreOrgChartTree::model()->findByPk($info['key']);
						if($puCheck && $isPowerUser){
							if ($ar) {
								if(!$powerUserBranchesArray || (is_array($powerUserBranchesArray) && empty($powerUserBranchesArray))){
									$powerUserBranchesArray = CoreOrgChartTree::getPuBranchIds();
								}

								$descendants = $ar->descendants()->findAll();
								if(is_array($powerUserBranchesArray) && in_array($ar->idOrg, $powerUserBranchesArray)){
										$orgChartList[] = $ar->idst_oc;
								}

								foreach($descendants as $descendant){
									if( is_array($powerUserBranchesArray) && in_array($descendant->idOrg, $powerUserBranchesArray)){
										$orgChartList[] = $descendant->idst_oc;
									}
								}
							}
						}else{
							if ($ar) {
								$orgChartDescList[] = $ar->idst_ocd;
							}
						}
						break;
				}
			}
		}

		$result = array($orgChartList, $orgChartDescList);

		return $result;
	}

	/**
	 * Register scripts required by Selector UI
	 */
	public static function registerClientScripts() {

		$cs = Yii::app()->getClientScript();
		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/jwizard.js');
		$cs->registerCssFile($themeUrl . '/css/usersselector.css');
		$cs->registerScriptFile($themeUrl . '/js/usersselector.js');
	}

	/**
	 * Return session key for the given selector. A helper to ensure the identical key is built
	 * @param string $selectorName
	 */
	public static function getSessionKey($selectorName = false) {
		if (false === $selectorName) {
			return self::SELECTOR_SESSION_KEY . '_' . self::NAME;
		} else {
			return self::SELECTOR_SESSION_KEY . '_' . $selectorName;
		}
	}

	/**
	 * Save single sub-key SELECTOR related data into session, using selector name to form the session key.
	 * 
	 * @param string $subKey Array key to save data into (result is is :  session[<selector-session-key>][<subKey>] = data) 
	 * @param mixed $keyData This sub key data to save
	 * @param string $selectorName Name of the selector
	 */
	public static function storeDataInSession($subKey, $keyData, $selectorName = false) {
		$sessionKey = self::getSessionKey($selectorName);
		$data = Yii::app()->session[$sessionKey];
		$data[$subKey] = $keyData;
		Yii::app()->session[$sessionKey] = $data;
	}

	/**
	 * Store all possible selector data into session. Add here whatever we may need
	 * @param mixed $data
	 * @param string $selectorName
	 */
	public static function storeAllDataInSession($data, $selectorName = false) {
		self::storeDataInSession('preselectedUsers', self::getUsersListOnly($data), $selectorName);
		self::storeDataInSession('preselectedGroups', self::getGroupsListOnly($data), $selectorName);
		self::storeDataInSession('preselectedBranches', self::getBranchesListOnly($data), $selectorName);
		self::storeDataInSession('preselectedCourses', self::getCoursesListOnly($data), $selectorName);
		self::storeDataInSession('preselectedPlans', self::getPlansListOnly($data), $selectorName);
		self::storeDataInSession('preselectedPuProfiles', self::getPuProfilesList($data), $selectorName);
		self::storeDataInSession('preselectedAssets', self::getAssetsListOnly($data), $selectorName);
		self::storeDataInSession('preselectedChannels', self::getChannelsListOnly($data), $selectorName);
	}

	/**
	 * Extract SELECTOR related data from session, if any
	 * @param string $subKey Like 'preselectedUsers', 'preselectedGroups', etc.
	 * @param string $selectorName  Selector instance name, optional (will use self::NAME) 
	 * @param string $destroySubKey  Destroy extracted sub-key data or not
	 */
	public static function getDataFromSession($subKey, $selectorName = false, $destroySubKey = true) {
		$sessionKey = self::getSessionKey($selectorName);
		if (isset(Yii::app()->session[$sessionKey])) {
			$data = Yii::app()->session[$sessionKey];
			if (isset($data[$subKey])) {
				$result = $data[$subKey];
				if ($destroySubKey) {
					unset($data[$subKey]);
					Yii::app()->session[$sessionKey] = $data;
				}
				return $result;
			}
		}
		return array();
	}

	/**
	 * Destroy (unset) ALL selector data stored in session
	 * @param string $selectorName
	 */
	public static function destroyAllSessionData($selectorName = false) {
		$sessionKey = self::getSessionKey($selectorName);
		if (isset(Yii::app()->session[$sessionKey])) {
			unset(Yii::app()->session[$sessionKey]);
		}
	}

	/**
	 * Check and eventually adjust orgchart selection in order to avoid inconsistencies (e.g. a leaf node with descendants selection)
	 * @param $selection array the list of selected nodes
	 */
	public static function checkOrgchartSelection($selection) {
		$output = array();

		$leafNodes = NULL;
		if (is_array($selection)) {
			foreach ($selection as $item) {
				if (is_array($item) && isset($item['key']) && isset($item['selectState'])) {
					$outputItem = $item;
					if ($outputItem['selectState'] == self::ORGCHART_THIS_AND_DESC) {
						if ($leafNodes === NULL) { //this will be execute the first time only
							//find all organization chart leaf nodes and enlist them
							$leafNodes = array();
							$cmd = Yii::app()->db->createCommand("SELECT idOrg FROM " . CoreOrgChartTree::model()->tableName() . " WHERE iRight - iLeft = 1");
							$reader = $cmd->query();
							while ($record = $reader->read()) {
								$leafNodes[$record['idOrg']] = 1;
							}
						}
						if (array_key_exists($outputItem['key'], $leafNodes)) {
							//we are effectively trying to assign a descendants selection to a leaf node with no descendants .. correct manually the selection
							$outputItem['selectState'] = self::ORGCHART_THIS_ONLY;
						}
					}
					$output[] = $outputItem;
				}
			}
		}

		//cleaned selection
		return $output;
	}

}
