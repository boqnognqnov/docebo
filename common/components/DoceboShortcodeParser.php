<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class DoceboShortcodeParser {

	/**
	 * Pass an HTML and an array of [tag_name]=>[callable]
	 * pairs where the value is a callable function with the following structure:
	 * function($tagName, array $attributes, $fillTextTagMatches, $html)
	 * @param       $html
	 * @param array $tagParsers
	 */
	public static function parse(&$html, $tagParsers = array()){
		foreach($tagParsers as $tagName=>$callback){
			$strippedKey = str_replace(array("[", "]"), '', $tagName);
			// Build regex to check if the current tag has an option added
			$exp = '#\[(?P<tag_name>'.$strippedKey.'+)(?P<attributes_html>.*)\]#Ui';

			preg_match_all($exp, $html, $matchesOfThisTag, PREG_SET_ORDER);

			foreach($matchesOfThisTag as $tagMatch){
				$fullTextTagMatch = $tagMatch[0];
				$tagName = $tagMatch['tag_name'];
				$attributesHtml = $tagMatch['attributes_html'];
				$matches = array();
				preg_match_all('#((?P<attr>\w*)=[",\'](?P<value>\w*)[",\'])+#Ui', $attributesHtml, $matches, PREG_SET_ORDER);

				$attributes = array();

				foreach($matches as $matchedAttr){
					$attrName = $matchedAttr['attr'];
					$attrValue = $matchedAttr['value'];

					$attributes[$attrName] = $attrValue;
				}

				if($callback instanceof Closure){
					// Anonymous function
					call_user_func_array($callback, array(
						$tagName,
						$attributes,
						$fullTextTagMatch,
						&$html
					));
				}
			}
		}
	}
}