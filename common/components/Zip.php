<?php

/**
 * ZipArchive wrapper  (http://php.net/manual/en/class.ziparchive.php)
 * 
 * 
 *
 */
class Zip extends CComponent {
	
	/**
	 * @var ZipArchive
	 */
	private $zip = null;
	
	/**
	 * @var integer
	 */
	public $lastError = null;
	
	/**
	 * 
	 * @var string
	 */
	public $lastErrorMessage = null;

	
	/**
	 * Last Zip Archive status string
	 * @var string
	 */
	public $statusString = '';
	
	
	/**
	 * Constructor 
	 * 
	 * @param string $zipFilePath Target ZIP file
	 * @param string $createIfNotExist
	 * @return boolean
	 */
	public function __construct($zipFilePath, $createIfNotExist = true) {
		
		// If archive does not exists and we are not asked to create it, stop the constructor, set error message
		if (!is_file($zipFilePath)) {
			if (!$createIfNotExist) {
				$this->lastError = 9999;
				$this->lastErrorMessage = Yii::t('standard', 'Zip File does not exist');
				return;
			}
		}
		
		// Create Zip object and open the zip file
		$this->zip = new ZipArchive();
		$result = $this->zip->open($zipFilePath, ZipArchive::CREATE); 
		if (!$result) {
			$this->lastError = $result; 
			$this->lastErrorMessage = Yii::t('standard', 'Zip Error while opening the archive');
		}
		
	}

	/**
	 * Add a single file to the archive. 
	 * No (source) directories involved, a file is just added to the zip under requested entryname (which may have folder part!)
	 *
	 * @param string $filePath The file to add 
	 * @param boolean $overwrite
	 * @param string $entryFileName The name of the file in the archive
	 */
	public function addFile($filePath, $overwrite=true, $entryFileName='') {
		
		if (!is_file($filePath)) {
			return false;
		}
		
		if (!$entryFileName) {
			$entryFileName = pathinfo($filePath, PATHINFO_BASENAME);
		}
		
		// Check if file already exists and if we are asked to overwrite
		$index = $this->zip->locateName($entryFileName);
		if ($index !== false && !$overwrite) {
			return true;
		}
		
		
		// Add file
		if (!$this->zip->addFile($filePath, $entryFileName)) {
			$this->lastError = 9997;
			$this->lastErrorMessage = Yii::t('standard', 'Zip Error while adding a file to archive');
			$this->statusString = $this->zip->getStatusString();
			return false;
		}
		
		// Could be usefull for after-run analyzis
		$this->statusString = $this->zip->getStatusString();
		
		return true;
		
	}
	
	
	/**
	 * Add a folder content to the Zip, recursively (default)
	 * The source folder path itself is NOT stored as part of the entry names! Only relative paths are stored.
	 * 
	 * @param string $folderPath
	 * @param string $recursive
	 */
	public function addFolder($folderPath, $recursive=true, $overwrite=true) {

		// Get the Zip Archive object
		$zip = $this->zip;

		// Must be a folder...
    	if (is_dir($folderPath) === true) {

    		// Traverse folder in a way depending on recursive/non-recusrsive
    		if ($recursive)
        		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folderPath), RecursiveIteratorIterator::SELF_FIRST);
    		else 
    			$files = new FilesystemIterator($folderPath);
    		
        	
        	$folderPath = str_replace('\\', '/', realpath($folderPath));

        	// Iterate over files (see the Iterator)
        	foreach ($files as $file) {
            	$file = str_replace('\\', '/', $file);

	            // Ignore "." and ".." folders
    	        if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) {
    	        	continue;
    	        }
        	        
            	$file = realpath($file);
            	$tmpFile = str_replace('\\', '/', $file);

            	// Add the folder to Zip as empty folder (will be "filled" later with files, if any)
            	if (is_dir($file) === true && $recursive) {
            		$dir = str_replace($folderPath . '/', '', $tmpFile . '/');
            		$dirIndex = $zip->locateName($dir);
            		if ($dirIndex !== false && !$overwrite) {
            			continue;
            		}
                	$zip->addEmptyDir($dir);
            	}
            	// Or, if this is a FILE, add it, storing RELATIVE path as well
            	else if (is_file($file) === true) {
            		$entryName = str_replace($folderPath . '/', '', $tmpFile);
            		if (!$this->addFile($file, $overwrite, $entryName)) {
            			$this->statusString = $this->zip->getStatusString();
            			return false;
            		}
            	}
        	}
    	}
    	
    	// Could be usefull for after-run analyzis
    	$this->statusString = $this->zip->getStatusString();
		
	}
	


	/**
	 * Close the Zip Archive. Call it whenever you are done with the whole job.
	 */
	public function close() {
		$this->zip->close();
	}

	

	/**
	 * Getter
	 * 
	 * @see CComponent::__get()
	 */
	public function __get($name) {
		
		switch ($name) {
			// Return Zip Archive object (private to the class)
			case 'zip' : 
				return $this->zip;
				break;
		}
		return null;
	}
	
	
}