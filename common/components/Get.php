<?php

class Get {
	
	/** NOTE: Copied from LMS root/lib/lib.get.php @TODO May need some adjusting
	 * Return the calculated relative path form the current zone (platform) to the requested one
	 * @param <string> $item (base, lms, cms, ...)
	 * @return <string> the relative path
	 */
	public static function rel_path($to = false) {
		// where are we ?
		if($to === false) {
			if (defined("CORE"))        {
				$to = 'adm';
			}
			elseif (defined("LMS")) {
				$to = 'lms';
			}
		}
		if (!defined('_' . $to . '_')) {
			$to = 'base';
		}
		$path = str_replace(_base_, '..', constant('_'.$to.'_'));
		return str_replace(array('//', '\\/', '/./'), '/', $path);
	}
	
}

?>
