<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class DoceboUI extends CComponent {


	protected static $_flashHandlers = array(
		'success' => array('DoceboUI', 'standardFlashMessageHandler'),
		'danger' => array('DoceboUI', 'standardFlashMessageHandler'),
		'error' => array('DoceboUI', 'standardFlashMessageHandler'),
		'info' => array('DoceboUI', 'standardFlashMessageHandler')
	);


	protected static function handleFlashMessage($key, $message) {
		$output = "";
		if (isset(self::$_flashHandlers[$key])) {
			$output .= call_user_func(self::$_flashHandlers[$key], $key, $message);
		}
		return $output;
	}

	public static function isFlashMessageHandled($key) {
		return array_key_exists($key, self::$_flashHandlers);
	}

	public static function setFlashMessageHandler($key, $handler) {
		if (is_callable($handler)) {
			self::$_flashHandlers[$key] = $handler;
			return true;
		}
		return false;
	}

	public static function unsetFlashMessageHandler($key) {
		if (isset(self::$_flashHandlers[$key])) {
			self::$_flashHandlers[$key] = NULL;
			unset(self::$_flashHandlers[$key]);
		}
		return true;
	}

	/**
	 * Default flash message handler
	 * @param string $key key of the flash message
	 * @param mixed $message value of the flash message
	 * @return string
	 */
	public static function standardFlashMessageHandler($key, $message) {
		return '<div class="alert alert-'.$key.'">'
			.'<button type="button" class="close" data-dismiss="alert">&times;</button>'
			.$message.'</div>';
	}

	/**
	 * Print feedback to the user (set using Yii::app()->user->setFlash)
	 * @param bool $return_only if false the result won't be printed
	 * @return string
	 */
	public static function printFlashMessages($return_only = false) {
		$res = '';
		foreach(Yii::app()->user->getFlashes() as $key=>$message) {
			/*
			$res .= '<div class="alert alert-'.$key.'">'.
				'<button type="button" class="close" data-dismiss="alert">&times;</button>'.
				$message."</div>\n";
			*/
			if (self::isFlashMessageHandled($key)) {
				$res .= self::handleFlashMessage($key, $message);
			}
		}
		if (!$return_only) { echo $res; }

		return $res;
	}

	/**
	 * This method will print in the footer the code needed in order to show the popup for an expired platform
	 */
	public function showExpiredPopup() {
		$contactSalesUrl = Docebo::createLmsUrl('site/axContactSalesTrialExpired');
		Yii::app()->getController()->renderPartial('common.components.views.doceboUI.expired_popup', array(
			'contactSalesUrl' => $contactSalesUrl,
		));

		/*
		// The popup is different from user to godadmin
		if (Yii::app()->user->isGodAdmin) {
			Yii::app()->getController()->renderPartial('common.components.views.doceboUI.expired_popup', array());
		} else {
			Yii::app()->getController()->renderPartial('common.components.views.doceboUI.expired_popup_user', array());
		}
		*/
	}

	/**
	 * This method will print in the footer the code needed in order to show the popup for reinsert credit card data
	 */
	public function showReinsertCCDataPopup() {
		if ((PluginManager::isPluginActive('WhitelabelApp') &&
			Settings::get('whitelabel_menu', null) == 1 &&
			Settings::get('whitelabel_menu_userid', null) === Yii::app()->user->id) ||
			!PluginManager::isPluginActive('WhitelabelApp') ||
			(PluginManager::isPluginActive('WhitelabelApp') && Settings::get('whitelabel_menu', null) !== 1)
		) {
			Yii::app()->getController()->renderPartial('common.components.views.doceboUI.reinsert-cc-data', array(
				'userName' => Yii::app()->user->getDisplayName(),
			));
		}
	}

	/**
	 * This helps retrieve the correct error messsage if login fails for some reason
	 * @param boolean $return_only - Return the message if true and display it if false
	 */
	public static function getLoginError($return_only = true){
		$ret = '';
		$errorCode = (int) $_GET['error'];

		// The block above is only used by the max failed logins protection
		$expireTime = $_COOKIE['max_login_attempts_reached_expire_time'];
		if($expireTime){
			$minutesRemaining = (($expireTime - time()) / 60);
			$minutesRemaining = (($minutesRemaining<1) ? '< 1' : floor($minutesRemaining));
		}else{
			$minutesRemaining = '< 1';
		}

		$accountExpireMsg = Yii::app()->user->getFlash("expired_account");
		$messagesArr = array(
			1=>Yii::t('login', '_NOACCESS'),
			2=>Yii::t('login', '_NOACCESS'),
			3=>Yii::t('test', '_MAX_ATTEMPT_REACH'),
			4=>Yii::t('login', '_TWO_USERS_LOGGED_WITH_SAME_USERNAME'),
			// no need of translations
			5=>$accountExpireMsg,
			6=>Yii::t('login', 'Your platform has expired'),
		);

        // Trigger event to let plugin add custom error messages
        $event = new DEvent(self, array());
        Yii::app()->event->raise('InstallCustomLoginErrors', $event);
        if(isset($event->return_value['other_errors']) && !empty($event->return_value['other_errors'])) {
        	$customMessages = $event->return_value['other_errors'];
        	if (is_array($customMessages)) {
        		$currentKeys = array_keys($messagesArr);
        		foreach ($customMessages as $key => $message) {
        			if (!in_array($key, $currentKeys)) {
        				$messagesArr[$key] = $message;
        			}
        			else {
        				$messagesArr[] = $message;
        			}
        			$currentKeys = array_keys($messagesArr);
        		}
        	}
        }

		if(isset($messagesArr[$errorCode]) && !empty($messagesArr[$errorCode])){
			$ret = '<div class="login-error label label-important text-center">' . $messagesArr[$errorCode] .'</div>';
		}

		if($return_only) return $ret;

		echo $ret;
	}


	/**
	 * Get the user created pages from Admin->Gear->Sign in web pages
	 */
	public static function getUserPages(){
		// $langCode = );

		// $allPages = LearningWebpageTranslation::model()->with('page')->findAll(array('order' => 'page.sequence'));

		$allPages = LearningWebpageTranslation::model()->with('page')->findAll(array(
			'condition' => 'lang_code = :lang_code',
			'params' => array(
				':lang_code' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
			),
			'order' => 'page.sequence'
		));
		if($allPages){

            // Raise event to let plugins override the user pages
			$event = new DEvent(self, array('signin_pages' => &$allPages));
            Yii::app()->event->raise('FilterSigninPages', $event);

			if($event->shouldPerformAsDefault()) {
				foreach ($allPages as $k => $page) {
					if(!isset($page->page->publish) || ($page->page->publish != 1) ){
						unset($allPages[$k]);
					}

				}
			}

			return $allPages;
		}
		return FALSE;
	}

	/*
	 * Display the custom CSS, entered in Admin -> Template manager
	 */
	public static function custom_css($return_only = true){
		$customCSS = Settings::get('custom_css');
		
		Yii::app()->event->raise('ResolvingCustomCss', new DEvent(self, array(
			'custom_css_content'	=> &$customCSS,
		)));
		
		if($customCSS){

			$res = '<style type="text/css">'.$customCSS.'</style>';

			if($return_only)
				return $res;
			else
				echo $res;

		}
	}

}
