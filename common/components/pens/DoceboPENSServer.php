<?php

require_once(Yii::getPathOfAlias('common.vendors.pens').DIRECTORY_SEPARATOR.'pens_server.php');


class DoceboPENSServer extends PENSServer {

	/**
	 * Instance of the DoceboPENSServer
	 * @var PENSServer
	 */
	private static $_instance;


	/**
	 * Source of parameters for requests. If NULL value is assigned, then $_REQUEST will be used.
	 * This is useful when request parameters are not passed through $_REQUEST (e.g. in an async. job)
	 * @var null
	 */
	protected static $_requestSourceParams = NULL;


	/**
	 * The instance for the currently used instance of PENS application in the LMS
	 * @var null
	 */
	protected static $_pensApplication = NULL;


	/**
	 * Private constructor
	 */
	private function __construct() {
	}
	
	/**
	 * Singleton method
	 */
	public static function singleton() {
		if(!isset(self::$_instance)) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}
		return self::$_instance;
	}



	public function setRequestSourceParams($params) {
		if (is_array($params)) {
			self::$_requestSourceParams = $params;
			return true;
		}
		return false;
	}



	protected function getRequestParam($name) {
		if (is_array(self::$_requestSourceParams)) {
			return (isset(self::$_requestSourceParams[$name]) ? self::$_requestSourceParams[$name] : NULL);
		} else {
			return (isset($_REQUEST[$name]) ? $_REQUEST[$name] : NULL);
		}
	}


	protected function getRequestParams() {
		if (is_array(self::$_requestSourceParams)) {
			return self::$_requestSourceParams;
		} else {
			return $_REQUEST;
		}
	}


	/**
	 * Returns current PENS application destination category ID. If no PENS app is currently set then NULL is returned.
	 * @return null/integer
	 */
	public function getApplicationDestinationCategory() {
		if (!empty(self::$_pensApplication) && self::$_pensApplication->category_id !== NULL) {
			return self::$_pensApplication->category_id;
		}
		return NULL;
	}


	/**
	 * Sets the specific Docebo package handler.
	 * It does nothing if the argument is not an instance of PENSPackageHandler
	 *
	 * @param PENSPackageHandler Package handler
	 */
	public function setPackageHandler($package_handler) {
		if ($package_handler instanceof DoceboPENSPackageHandler) {
			if (!empty(self::$_pensApplication) && self::$_pensApplication->category_id !== NULL) {
				$package_handler->setDestinationCategory(self::$_pensApplication->category_id);
			}
		}
		if ($package_handler instanceof PENSPackageHandler) {
			parent::setPackageHandler($package_handler);
		}
	}


	/**
	 * Parses a request based on the values present in class field $_requestSourceParams (or $_REQUEST if no internal
	 * parameters are specified)
	 *
	 * @return PENSRequest Request generated
	 * @throws PENSException if the request could not be parsed
	 */
	protected function parseRequest() {
		if (is_array(self::$_requestSourceParams)) {
			$request = PENSRequestFactory::createPENSRequest(self::$_requestSourceParams);
		} else {
			$request = PENSRequestFactory::createPENSRequest($_REQUEST);
		}
		return $request;
	}



	/**
	 * Verifies that the package url is not expired
	 *
	 * @param DateTime DateTime object to verify against current time
	 */
	protected function isExpiryDateValid($expiry) {
		$originalTimezone = date_default_timezone_get();
		date_default_timezone_set('UTC');

		/* @var $lt LocalTime */
		$lt = Yii::app()->localtime;
		$current_date = $lt->getUTCNow('Y-m-d H:i:s');
		$expiry_date = $expiry->format('Y-m-d H:i:s');

		date_default_timezone_set($originalTimezone);

		return (strcmp($current_date, $expiry_date) >= 0 ? false : true);
	}



	protected function checkSystemAccess(PENSRequest $request) {
		$pensApplication = PensApplication::model()->findByAttributes(array(
			'user_id' => $request->getSystemUserId(),
			'password' => $request->getSystemPassword(),
			'enable' => 1,
		)); //TODO: in the future Application ID will be involved (when new PENS standard version will be released)

		if (empty($pensApplication)) {
			return false;
		}

		self::$_pensApplication = $pensApplication;
		return true;
	}



	/**
	 * Receives a collect request and treats it
	 */
	public function receiveCollect() {
		$request = null;
		try {

			// First, try to parse the request
			$request = $this->parseRequest();

			//check request access info (user ID and password) validity
			if (!$this->checkSystemAccess($request)) {
				throw new DoceboPENSException(1490);
			}

			//check command validity
			if($request->getCommand() == "collect") {
				if ($this->getRequestParam('process') == 1) {
					// Collect the package and process it
					$receipt = null;
					$path_to_package = null;
					try {
						// Collect the package
						$path_to_package = $this->collectPackage($request);
						$receipt = new PENSResponse(0, "package successfully collected");
					} catch(PENSException $e) {
						$receipt = new PENSResponse($e);
					}
					// Send receipt
					$response = $this->sendReceipt($request, $receipt);
					if(!is_null($response) && !is_null($path_to_package)) {
						if($response->getError() === 0) {
							// Process package
							$this->processPackage($request, $path_to_package);
						} else Yii::log(__METHOD__.' : response error = '.$response->getError(), 'error');
						//FileHelper::removeFile($path_to_package);
					} else Yii::log(__METHOD__.' : receipt error = '.print_r($response, true), 'error');
				} else {
					// Then, send a success response to the client
					$this->sendResponse(new PENSResponse(0, "collect command received and understood"));
					// Send a request to process the package: fake multithreading
					$params = $this->getRequestParams();
					$params['process'] = 1;

					// Creating an async. job to handle all package-related operations, such as loading SCORM/XAPI/AICC package
					// on storage system, create the LO in DB and so on ...
					/* @var $scheduler SchedulerComponent */
					$scheduler = Yii::app()->scheduler;
					$scheduler->createImmediateJob(PENSJobs::JOB_NAME, PENSJobs::id(), array('request_params' => $params));
				}
			}
				
		} catch(PENSException $e) {
			// If we could not parse the request, send the error back to the client
			$this->sendResponse(new PENSResponse($e));
Yii::log(__METHOD__.' : EXCEPTION = '.$e->getCode().' - '.$e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}




	/**
	 * Collects the package onto the local server
	 * 
	 * @param PENSRequest request
	 * @return string Path to the package on the hard drive
	 * @throws PENSException if an exception occured
	 */
	protected function collectPackage($request) {
		$supported_package_types = $this->_package_handler->getSupportedPackageTypes();
		if(!in_array($request->getPackageType(), $supported_package_types)) {
			throw new PENSException(1430);
		}
		$supported_package_formats = $this->_package_handler->getSupportedPackageFormats();
		if(!in_array($request->getPackageFormat(), $supported_package_formats)) {
			throw new PENSException(1431);
		}
		if(!$this->isExpiryDateValid($request->getPackageUrlExpiry())) {
			throw new PENSException(1322);
		}

		// Try to download the package in the temporary directory
		//===
		// NOTE:
		// some http URLs do not publish the file name in the URL, preventing us to use the method "$request->getFilename()"
		// which is guessing the file name from the URL itself.
		// As workaround we simply create a temporary random name ($tmpFileName) to download the package file on our system.
		$tmpFileName = '_pens_'.time().'_'.rand(10000, 99999).'.'.$request->getPackageFormat();
		$uploadTmpFile = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $tmpFileName;//$request->getFilename();
		$fp = fopen($uploadTmpFile, 'w');
		//===

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request->getPackageUrl());
		curl_setopt($ch, CURLOPT_HEADER, false);	
		curl_setopt($ch, CURLOPT_FILE, $fp);
		if(!is_null($request->getPackageUrlUserId())) {
			curl_setopt($ch, CURLOPT_USERPWD, $request->getPackageUrlUserId().":".$request->getPackageUrlPassword());
		}
		if(curl_exec($ch) === false) {
			$errno = curl_errno($ch);
			curl_close($ch);
			// Error occured. Throw an exception
			switch($errno) {
				case CURLE_UNSUPPORTED_PROTOCOL:
					throw new PENSException(1301);
					break;
				case CURLE_URL_MALFORMAT:
				case CURLE_COULDNT_RESOLVE_PROXY:
				case CURLE_COULDNT_RESOLVE_HOST:
				case CURLE_COULDNT_CONNECT:
				case CURLE_OPERATION_TIMEOUT:
				case CURLE_REMOTE_FILE_NOT_FOUND:
					throw new PENSException(1310);
					break;
				case CURLE_REMOTE_ACCESS_DENIED:
					throw new PENSException(1312);
					break;
				default:
					throw new PENSException(1301);
					break;
			}
			return null;
				
		} else {
			curl_close($ch);
			return $uploadTmpFile;
		}
	}


	/**
	 * Sends an alert or a receipt. Called by sendReceipt and sendAlert
	 * 
	 * @param PENSRequest Original collect request
	 * @param PENSResponse Reponse to send in the receipt or the alert
	 * @param string Mode (alert | receipt)
	 * @return PENSResponse Response
	 */
	protected function sendAlertOrReceipt($request, $response, $mode) {
		if ($mode == "alert") {
			$url = $request->getAlerts();
		} else {
			$url = $request->getReceipt();
		}
		if (!empty($url)) {
			$url_components = parse_url($url);
			$scheme = $url_components["scheme"];
			if ($scheme == "mailto") {
				$to = trim($url_components["path"]);
				if ($mode == "alert") {
					$subject = "PENS Alert for ".$request->getPackageId();
				} else {
					$subject = "PENS Receipt for ".$request->getPackageId();
				}
				$message = $response->__toString();
				//=== using LMS mailer
				if (Settings::get('PENS_notification_debug', 'off') == 'on') {
					if (empty($to)) {
						Yii::log(__METHOD__.': empty recipient for PENS notification email', CLogger::LEVEL_WARNING);
					} else {
						try {
							$mailer = new MailManager();
							$fromArray = array(Settings::get('sender_event', 'noreply@docebo.com'));
							$toArray = array($to);
							$mailResult = $mailer->mail($fromArray, $toArray, $subject, $message, false, false, false, array(), false);
						} catch (Exception $e) {
							Yii::log(__METHOD__ . ': Error while sending PENS notification email (from: ' . $fromArray[0] . ', to: ' . $toArray[0] . ')' . "\n" . 'Error message: ' . $e->getMessage(), CLogger::LEVEL_WARNING);
							//make sure that the mail sending method is non-blocking, also in case of mail errors
						}
					}
				}
				//===
				return new PENSResponse(0, "");
			} else if ($scheme == "http" || $scheme == "https") {
				if ($mode == "alert") {
					$params = array_merge($request->getSendAlertArray(), $response->getArray());
				} else {
					$params = array_merge($request->getSendReceiptArray(), $response->getArray());
				}
				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$data = curl_exec($ch);
				curl_close($ch);
				if ($data === false) {
					return null;
				} else {
					return new PENSResponse($data);
				}
			}
		}
	}

}
