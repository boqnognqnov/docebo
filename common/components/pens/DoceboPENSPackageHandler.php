<?php

require_once(Yii::getPathOfAlias('common.vendors.pens').DIRECTORY_SEPARATOR.'pens_package_handler.php');


class DoceboPENSPackageHandler extends PENSPackageHandler {


	/**
	 * The course category in where newly created LOs have to be placed. If NULL/empty root folder is used
	 * @var null
	 */
	protected $_destinationCategory = NULL;


	/**
	 * A standard set of options for newly created learning objects.
	 * @var array
	 */
	protected static $_standardLoParams = array(
		'desktop_openmode' => LearningOrganization::OPENMODE_LIGHTBOX,
		'tablet_openmode' => LearningOrganization::OPENMODE_FULLSCREEN,
		'smartphone_openmode' => LearningOrganization::OPENMODE_FULLSCREEN
	);


	public function __construct() {
		PENSConfig::$allowed_package_types[] = "xapi-pkg"; //TODO: put this in a better position
		parent::__construct();
	}


	/**
	 * Set the destination category ID
	 * @param $categoryId
	 * @return bool if the operation has been successfull
	 */
	public function setDestinationCategory($categoryId) {
		if (!is_numeric($categoryId) || $categoryId < 0) { return false; }
		$this->_destinationCategory = intval($categoryId);
		return true;
	}

	/**
	 * Get the destination category ID
	 * @param $categoryId
	 * @return null/integer
	 */
	public function getDestinationCategory() {
		return $this->_destinationCategory;
	}


	/**
	 * Processes the package.
	 *
	 * @param PENSRequestCollect Collect request sent by the client
	 * @param string Path to the collected package on the hard drive
	 */
	public function processPackage($request, $path_to_package) {
		try {
			//first check destination category
			$server = DoceboPENSServer::singleton();
			$this->setDestinationCategory($server->getApplicationDestinationCategory());
			//then do effective processing
			switch ($request->getPackageType()) {
				case "scorm-pif":
					$this->processScormPackage($request, $path_to_package);
					break;
				case "aicc-pkg":
					$this->processAiccPackage($request, $path_to_package);
					break;
				case "xapi-pkg":
					$this->processXapiPackage($request, $path_to_package);
					break;
				default:
					throw new DoceboPENSException(1430); // Package type not supported
					break;
			}
		} catch (PENSException $e) {
			throw $e;
		} catch (CException $e) {
			throw new DoceboPENSException(1491, $e->getMessage()); // Generic internal LMS error
		}
	}


	/**
	 * Returns an user model to be used as author for created learning objects.
	 * @return CoreUser
	 * @throws CException if no user can be retrieved
	 */
	protected function getPENSAgentUser() {
		/* @var $user DoceboWebUser */
		$user = Yii::app()->user;
		$levelsList = $user->getUserLevelIds();
		$flippedLevelsList = array_flip($levelsList);
		$godAdminsGroupId = $flippedLevelsList[$user->level_godadmin];

		/* @var $cmd CDbCommand */
		$cmd = YiI::app()->db->createCommand();
		$cmd->select("u.idst");
		$cmd->from(CoreUser::model()->tableName()." u");
		$cmd->join(CoreGroupMembers::model()->tableName()." gm", "u.idst = gm.idstMember");
		$cmd->where("gm.idst = :godadmins_group_id", array(':godadmins_group_id' => $godAdminsGroupId));
		$cmd->limit(1, 0);
		$record = $cmd->queryRow(true);
		if (empty($record)) { throw new CException(Yii::t('standard', '_OPERATION_FAILURE')); }
		$userId = $record['idst'];

		$userModel = CoreUser::model()->findByPk($userId);
		if (!$userModel) { throw new CException(Yii::t('standard', '_OPERATION_FAILURE')); }
		return $userModel;
	}





	protected function processScormPackage($request, $fullFilePath) {

		$fileName = basename($fullFilePath);
		$categoryNodeId = $this->getDestinationCategory();
		if ($categoryNodeId === NULL) {
			$categoryNodeId = LearningCourseCategoryTree::getRootNodeId(); // fallback to default category
		}

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$db->beginTransaction();

		try {

			/*********************************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			$repoObject = new LearningRepositoryObject();
			$repoObject->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
			$repoObject->params_json = CJSON::encode(self::$_standardLoParams);
			$repoObject->id_category = $categoryNodeId;
			$repoObject->short_description = '';
			$repoObject->resource = NULL;

			if (!$repoObject->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== central repo object has been created, now creates the effective learning object (which will be first version) ===

			$userModel = $this->getPENSAgentUser();

			$package = new LearningScormPackage();
			$package->idUser = $userModel->idst;
			$package->setFile($fileName);
			$package->setScenario('centralRepo');

			if (!$package->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$repoObjectVersion = new LearningRepositoryObjectVersion();
			$repoObjectVersion->author = CJSON::encode(array('idUser' => $userModel->idst, 'username' => $userModel->getUserNameFormatted()));
			$repoObjectVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
			$repoObjectVersion->id_object = $repoObject->id_object;
			$repoObjectVersion->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
			$repoObjectVersion->id_resource = $package->scormOrganization->idscorm_organization;
			$repoObjectVersion->version = 1;
			$repoObjectVersion->version_name = 'V1';
			$repoObjectVersion->version_description = '';

			if (!$repoObjectVersion->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$repoObject->title = $package->scormOrganization->title;
			if (!$repoObject->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== end LO creation and versioning ===

			$db->getCurrentTransaction()->commit();

		} catch (CException $e) {

			$db->getCurrentTransaction()->rollback();
			//re-throw exception so that it can be caught and handled in above levels
			throw $e;
		}

	}






	protected function processXapiPackage($request, $fullFilePath) {

		$fileName = basename($fullFilePath);
		$categoryNodeId = $this->getDestinationCategory();
		if ($categoryNodeId === NULL) {
			$categoryNodeId = LearningCourseCategoryTree::getRootNodeId(); // fallback to default category
		}

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$transaction = $db->beginTransaction();

		try {

			/*** VIRUS SCAN ******************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			$repoObject = new LearningRepositoryObject();
			$repoObject->object_type = LearningOrganization::OBJECT_TYPE_TINCAN;
			$repoObject->params_json = CJSON::encode(self::$_standardLoParams);
			$repoObject->id_category = $categoryNodeId;
			$repoObject->short_description = '';
			$repoObject->resource = NULL;

			if (!$repoObject->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== central repo object has been created, now creates the effective learning object (which will be first version) ===

			$userModel = $this->getPENSAgentUser();

			$object = new LearningTcAp();
			$object->setScenario('centralRepo');
			$object->registration = LearningTcAp::generateStatementUUID();
			$object->location = ''; //TODO: get cloudfront information from storage class
			$object->setFile($fileName);

			if (!$object->save()) {
				Yii::log('Error while saving DB information; error(s): '.print_r($object->getErrors(), true), CLogger::LEVEL_ERROR);
				FileHelper::removeFile($fullFilePath);
				throw new CException('Error while saving DB information; error(s): '.print_r($object->getErrors(), true));
			}

			$version = new LearningRepositoryObjectVersion();
			$version->id_resource = $object->mainActivity->getPrimaryKey();
			$version->object_type = LearningOrganization::OBJECT_TYPE_TINCAN;
			$version->version = 1;
			$version->id_object = $repoObject->id_object;
			$version->version_name = 'V1';
			$version->version_description = '';
			$version->author = CJSON::encode(array('idUser' => $userModel->idst, 'username' => $userModel->getUserNameFormatted()));
			$version->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

			if (!$version->save()) {
				throw new CException('Error while saving DB information; error(s): '.print_r($object->getErrors(), true));
			}

			$title = $object->mainActivity->name;
			$repoObject->title = $title;
			if (!$repoObject->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}
			//=== end LO creation and versioning ===

			//=== create OAuth2 Client Application ===

			$clientModel = new OauthClients();
			$clientModel->client_id = $object->registration;
			$clientModel->client_secret = Docebo::randomHash();
			$clientModel->redirect_uri = "";
			$clientModel->app_name = $title;
			$clientModel->app_description = $title;
			$clientModel->hidden = 1;

			// NOTE: "...->save(false)", the FALSE argument (use validation) is important because 'redirect_uri' field
			// is REQUIRED by the model rules, but in this case we need to set it empty.
			if (!$clientModel->save(false)) {
				Yii::log('Failed to save OAuthClient', CLogger::LEVEL_ERROR);
				Yii::log('Errors:', CLogger::LEVEL_ERROR);
				Yii::log($clientModel, CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== end OAuth2 client application creation ===

			$transaction->commit();

		} catch (CException $e) {

			$transaction->rollback();
			//re-throw exception so that it can be caught and handled in above levels
			throw $e;
		}
	}







	protected function processAiccPackage($request, $fullFilePath) {

		$fileName = basename($fullFilePath);
		$categoryNodeId = $this->getDestinationCategory();
		if ($categoryNodeId === NULL) {
			$categoryNodeId = LearningCourseCategoryTree::getRootNodeId(); // fallback to default category
		}

		/* @var $db CDbConnection */
		$db = Yii::app()->db;
		$transaction = $db->beginTransaction();

		try {

			/*********************************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					Yii::log('File is not clean (VirusScanner): ' . $fullFilePath, CLogger::LEVEL_ERROR);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}
			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			$repoObject = new LearningRepositoryObject();
			$repoObject->object_type = LearningOrganization::OBJECT_TYPE_AICC;
			$repoObject->params_json = CJSON::encode(self::$_standardLoParams);
			$repoObject->id_category = $categoryNodeId;
			$repoObject->short_description = '';
			$repoObject->resource = NULL;
			if (!$repoObject->save()) {
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== central repo object has been created, now creates the effective learning object (which will be first version) ===

			$userModel = $this->getPENSAgentUser();

			$package = new LearningAiccPackage();
			$package->idReference = null;
			$package->lms_id_user = $userModel->idst;
			$package->lms_original_filename = $fileName;
			$package->setScenario('centralRepo');

			// Try to extract the package into the final destination (S3, file system,...)
			// Save uploaded package (handle its content, create all the intermediate table records, creating LO and so on)
			// Be aware: behavior is attached !!!
			if (!$package->extractPackage($fullFilePath)) {
				Yii::log('Error extractingg AICC package: ' . $fileName, CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			if (!$package->save()) {
				Yii::log('Error saving AICC package: ' . $fileName, CLogger::LEVEL_ERROR);
				Yii::log(var_export($package->errors, true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$versionModel = new LearningRepositoryObjectVersion();
			$versionModel->version = 1;
			$versionModel->version_name = 'V1';
			$versionModel->version_description = '';
			$versionModel->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
			$versionModel->author = CJSON::encode(array('idUser' => $userModel->idst, 'username' => $userModel->getUserNameFormatted()));
			$versionModel->object_type = LearningOrganization::OBJECT_TYPE_AICC;
			$versionModel->id_resource = $package->id;
			$versionModel->id_object = $repoObject->id_object;

			if (!$versionModel->save()) {
				Yii::log('Error saving AICC central repository object version: ' . print_r($versionModel->getErrors(), true), CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			$repoObject->title = $package->course_title;

			if (!$repoObject->save()) {
				Yii::log('Error while updating AICC package', CLogger::LEVEL_ERROR);
				throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
			}

			//=== end LO creation and versioning ===

			$transaction->commit();

			// Trigger event to let plugins save their own data from POST
			Yii::app()->event->raise('AfterSaveLo', new DEvent($this, array(
				'loModel' => $package->learningObject
			)));

		} catch (CException $e) {

			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$transaction->rollback();
			//re-throw exception so that it can be caught and handled in above levels
			throw $e;
		}
	}


}