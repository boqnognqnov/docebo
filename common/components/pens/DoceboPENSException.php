<?php

require_once(Yii::getPathOfAlias('common.vendors.pens').DIRECTORY_SEPARATOR.'pens_exception.php');

class DoceboPENSException extends PENSException {


	/**
	 * Array that provides an association between exception codes and messages
	 * @var array
	 */
	protected static $_docebo_code_to_messages = array(
		// The following error code is not specified in the PENS specification and was added in this implementation
		1490 => "System User ID or password are incorrect",
		1491 => "An error occurred while importing the package in the LMS"
	);

	/**
	 * @param string $code Exception code
	 * @param null $message Message to display
	 */
	public function __construct($code, $message = null) {
		parent::__construct($code, $message);
	}

	/**
	 * Sets the message based on the code. Checks LMS-specific codes first.
	 */
	protected function setMessageBasedOnCode() {
		if(empty($this->message) && !empty(self::$_docebo_code_to_messages[$this->code])) {
			$this->message = self::$_docebo_code_to_messages[$this->code];
		} else {
			parent::setMessageBasedOnCode();
		}
	}

}
