<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class DoceboAuthManager extends CAuthManager {

	/**
	 * @var string The name of the roles table, each role is a specific permission, aka itemName
	 */
	public $coreRole = 'core_role';

	/**
	 * @var string The name of the table that contains the association between the roles and the group, a role cannot
	 * be assigned directly to a user, it must be related to a group
	 */
	public $coreRoleMembers = 'core_role_members';

	/**
	 * @var string The name of the table that contain the groups of the platform,
	 */
	public $coreGroup = 'core_group';

	/**
	 * @var string The name of the table that contains the association between groups and his members, a group can
	 * contain other groups or user
	 */
	public $coreGroupMembers = 'core_group_members';

	protected $assignments = array();

	protected $assignments_id = array();

	/**
	 * Initializes the application component.
	 */
	public function init()
	{
		parent::init();
		// resolve the accesses, i need to do this because we need to set the session var also for the old lms
		$this->solveAccessRecursive(Yii::app()->user->id, array());
	}

	/**
	 * Performs access check for the specified user.
	 * @param string $itemName the name of the operation that need access check
	 * @param mixed $userId the user ID. This should can be either an integer and a string representing
	 * the unique identifier of a user. See {@link IWebUser::getId}.
	 * @param array $params name-value pairs that would be passed to biz rules associated
	 * with the tasks and roles assigned to the user.
	 * Since version 1.1.11 a param with name 'userId' is added to this array, which holds the value of <code>$userId</code>.
	 * @return boolean whether the operations can be performed by the user.
	 */
	public function checkAccess($itemName,$userId,$params=array()) {
		// Return false if the user is anonymous
		if($userId == null) return false;
		if (empty($this->assignments[$userId])) {
		    $this->solveAccessRecursive($userId, $params);
		}
		$result = empty($this->assignments[$userId][$itemName]) ? false : true;
		return $result;
	}

	/**
	 * Resolve access permission resolving recursive group assignement and so on.
	 * This method is internally called by {@link checkAccess}.
	 * @param mixed $userId the user ID. This must can be an integer
	 * the unique identifier of a user. See {@link IWebUser::getId}.
	 * @param array $params name-value pairs that would be passed to biz rules associated
	 * with the tasks and roles assigned to the user.
	 */
	protected function solveAccessRecursive($userId, $params) {
	    
		$cacheid = md5('assignments-' . $userId);
		
		// Return empty array if the user is anonymous (this will not arrive here)
		if($userId == null) return true;
		
		// Get data from session, if any
		$session_assignments_id = false;
		if (Yii::app() instanceof CWebApplication && isset(Yii::app()->session['public_area_stlist'])) {
		    $session_assignments_id = CJSON::decode(Yii::app()->session['public_area_stlist'] );		    
		}

		// Maybe there is something in the session from which we can start (only if current App user is the requested one)
		if (!empty($session_assignments_id)
              && (Yii::app()->user->id == $userId)
              && is_array($session_assignments_id) 
		      && $session_assignments_id[0] !== false) {

		          
			$this->assignments[$userId] 	= array();
			$this->assignments_id[$userId] 	= $session_assignments_id;

			$rows = Yii::app()->db->createCommand()
				->select('*')
				->from($this->coreGroup)
				->where(array('in', 'idst', $session_assignments_id))
				->queryAll();
			
			foreach ($rows as $row) {
				$this->assignments[$userId][$row['groupid']] = new CAuthAssignment($this,$row['groupid'],$userId,'',null);
			}
			
			$rows = Yii::app()->db->createCommand()
				->select('roleid')
				->from($this->coreRole)
				->where(array('in', 'idst', $session_assignments_id))
				->queryAll();

			foreach ($rows as $row) {
				$this->assignments[$userId][$row['roleid']] = new CAuthAssignment($this,$row['roleid'],$userId,'',null);
			}

			return true;
			
		}

		// No data in session, we need to retrive the info from scratch
		// -------------------------------------------------------------------
		// Resolve recurrency for groups, the first execution will retrive the group directly associated with
		// the user, then we will cycle in order to retrive groups of groups, in order to limit the maximum recursion
		// we will stop after 20 iteration at max
		$loop_check = 0;
		$groups 	= array((int)$userId);
		$new_groups = array((int)$userId);

		do {
			$loop_check++;
			$rows = Yii::app()->db->createCommand()
				->select('g.idst, g.groupid')
				->from($this->coreGroupMembers . ' gm')
				->join($this->coreGroup . ' g', 'gm.idst = g.idst')
				->where(array('in', 'gm.idstMember', $new_groups))
				->queryAll();
			
			$new_groups = array();
			
			foreach ($rows as $row) {
				$new_groups[] = (int)$row['idst'];
				$this->assignments[$userId][$row['groupid']] = new CAuthAssignment($this,$row['groupid'],$userId,'',null);
			}
			
			if(!empty($new_groups)) $groups = array_merge( $groups, array_diff($new_groups, $groups ));

		} while(!empty($new_groups)&& ($loop_check < 20)); // WHY 20???

		// Retrive role assigned to the groups
		$this->assignments_id[$userId] = $groups;
		
		$rows = Yii::app()->db->createCommand()
			->select('r.idst, r.roleid')
			->from($this->coreRoleMembers . ' rm')
			->join($this->coreRole . ' r', 'rm.idst = r.idst')
			->where(array('in', 'rm.idstMember', $groups))
			->queryAll();
		
		foreach ($rows as $row) {
			$this->assignments_id[$userId][] = (int)$row['idst'];
			$this->assignments[$userId][$row['roleid']] = new CAuthAssignment($this,$row['roleid'],$userId,'',null);
		}

		// Save the idst in session (ONLY if current App user is valid and equal to the requested user)
		// If we save data in session for EVERY user, session will just blow up if this method is called in cycle for many users
		if (Yii::app()->user->id == $userId) {
			if(Yii::app() instanceof CWebApplication){
				Yii::app()->session['public_area_stlist'] = CJSON::encode($this->assignments_id[$userId]);
			}
		}

		
		return true;
		
	}

	/**
	 * Adds an item as a child of another item.
	 * @param string $itemName the parent item name
	 * @param string $childName the child item name
	 * @return boolean whether the item is added successfully
	 * @throws CException if either parent or child doesn't exist or if a loop has been detected.
	 */
	public function addItemChild($itemName,$childName)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Removes a child from its parent.
	 * Note, the child item is not deleted. Only the parent-child relationship is removed.
	 * @param string $itemName the parent item name
	 * @param string $childName the child item name
	 * @return boolean whether the removal is successful
	 */
	public function removeItemChild($itemName,$childName)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Returns a value indicating whether a child exists within a parent.
	 * @param string $itemName the parent item name
	 * @param string $childName the child item name
	 * @return boolean whether the child exists
	 */
	public function hasItemChild($itemName,$childName)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Returns the children of the specified item.
	 * @param mixed $names the parent item name. This can be either a string or an array.
	 * The latter represents a list of item names.
	 * @return array all child items of the parent
	 */
	public function getItemChildren($names)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Assigns an authorization item to a user.
	 * @param string $itemName the item name
	 * @param mixed $userId the user ID (see {@link IWebUser::getId})
	 * @param string $bizRule the business rule to be executed when {@link checkAccess} is called
	 * for this particular authorization item.
	 * @param mixed $data additional data associated with this assignment
	 * @return CAuthAssignment the authorization assignment information.
	 * @throws CException if the item does not exist or if the item has already been assigned to the user
	 */
	public function assign($itemName,$userId,$bizRule=null,$data=null,$registerType=null)
	{
		$cacheid = md5('assignments-' .$userId);

		// $itemName can be a role or a group, so we need to search in both.
		// search in groups
		$group = CoreGroup::model()->find('groupid=:groupid', array(':groupid' => $itemName));
		if ($group) {

			// It's a group, we can add the user to the group
			$cgm = CoreGroupMembers::model()->findByPk(array(
				'idst' 			=> $group->idst,
				'idstMember' 	=> $userId,
			));
			if ($cgm) {
				// item already assigned
				throw new CException("The item {$group->idst} {$itemName} is already assigned to {$userId}.");
			}

			$cgm = new CoreGroupMembers();
			if($registerType == 'self'){
				$cgm->setScenario('self-register');
			}
			$cgm->idst = $group->idst;
			$cgm->idstMember = $userId;
			$cgm->save();

			$this->assignments[$userId] 	= array();
			$this->assignments_id[$userId]  = array();

			// Session
			if (Yii::app()->user->id == $userId) {
				unset(Yii::app()->session['public_area_stlist']);
			}

			// Delete the cache, next time will regenerate
			Yii::app()->cache->delete($cacheid);
			return new CAuthAssignment($this,$itemName,$userId,'',null);
		}

		// search in roles ... well you cannot assign a user to a role so we don't need this for now
		/*
		$role = CoreRole::model()->findByPk($itemName);
		if ($role) {

			// It's a group, we can add the user to the group
			$crm = new CoreRoleMembers();
			$crm->idst = $role->idst;
			$crm->idstMember = $userId;
			$crm->save();

			$this->assignments[$userId] 	= array();
			$this->assignments_id[$userId]  = array();

			// Session
			if (Yii::app()->user->id == $userId) {
				Yii::app()->session['public_area_stlist'] = array();
			}

			// Delete the cache, next time will regenerate
			Yii::app()->cache->delete($cacheid);

			return new CAuthAssignment($this,$itemName,$userId,'',null);
		}
		*/
		// find the id of $itemName
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Revokes an authorization assignment from a user.
	 * @param string $itemName the item name
	 * @param mixed $userId the user ID (see {@link IWebUser::getId})
	 * @return boolean whether removal is successful
	 */
	public function revoke($itemName,$userId) {

		$cacheid = md5('assignments-' .$userId);

		// $itemName must be group, a user can be assigned only to a group

		// search in groups
		$group = CoreGroup::model()->findByAttributes(array("groupid" => $itemName));
		if ($group) {

			// We can remove the user to the group
			$deleted = CoreGroupMembers::model()->deleteByPk(array(
				'idst' 			=> $group->idst,
				'idstMember' 	=> $userId,
			));
			
			if (!$deleted) { 
			    return true;
			}

			$this->assignments[$userId] 	= array();
			$this->assignments_id[$userId]  = array();

			// Session
			if (Yii::app()->user->id == $userId) {
				unset(Yii::app()->session['public_area_stlist']);
			}

			// Delete the cache, next time will regenerate
			Yii::app()->cache->delete($cacheid);

			return true;
		}
		//throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission');
	}

	/**
	 * Returns a value indicating whether the item has been assigned to the user.
	 * @param string $itemName the item name
	 * @param mixed $userId the user ID (see {@link IWebUser::getId})
	 * @return boolean whether the item has been assigned to the user.
	 */
	public function isAssigned($itemName,$userId)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Returns the item assignment information.
	 * @param string $itemName the item name
	 * @param mixed $userId the user ID (see {@link IWebUser::getId})
	 * @return CAuthAssignment the item assignment information. Null is returned if
	 * the item is not assigned to the user.
	 */
	public function getAuthAssignment($itemName,$userId)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Starting from the iduser we need to retrive the group associated to him, from them we can extract all the rest.
	 * Due to the DoceboUserIdentity implementation the id is the numeric id from core_user
	 * @param int $userId the user ID (see {@link IWebUser::getId})
	 * @return array the item assignment information for the user. An empty array will be
	 * returned if there is no item assigned to the user.
	 */
	public function getAuthAssignments($userId)
	{
	    if (!$userId) {
	        return array();
	    }
		if (empty($this->assignments[$userId])) {
		    $this->solveAccessRecursive($userId, array());
		}
		$result = !empty($this->assignments[$userId]) ? $this->assignments[$userId] : array();
		return $result;
		
	}

	/**
	 * Starting from the iduser we need to retrive the group associated to him, from them we can extract all the rest.
	 * Due to the DoceboUserIdentity implementation the id is the numeric id from core_user
	 * @param int $userId the user ID (see {@link IWebUser::getId})
	 * @return array the item assignment information for the user. An empty array will be
	 * returned if there is no item assigned to the user.
	 */
	public function getAuthAssignmentsIds($userId) {
	    if (!$userId) {
	        return null;
	    }
		if (empty($this->assignments_id[$userId])) {
		    $this->solveAccessRecursive($userId, array());
		}
		$result = ( !empty($this->assignments_id[$userId]) ? $this->assignments_id[$userId] : null );
		return $result;
	}

	/**
	 * Saves the changes to an authorization assignment.
	 * @param CAuthAssignment $assignment the assignment that has been changed.
	 */
	public function saveAuthAssignment($assignment)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Returns the authorization items of the specific type and user.
	 * @param integer $type the item type (0: operation, 1: task, 2: role). Defaults to null,
	 * meaning returning all items regardless of their type.
	 * @param mixed $userId the user ID. Defaults to null, meaning returning all items even if
	 * they are not assigned to a user.
	 * @return array the authorization items of the specific type.
	 */
	public function getAuthItems($type=null,$userId=null)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Creates an authorization item.
	 * An authorization item represents an action permission (e.g. creating a post).
	 * It has three types: operation, task and role.
	 * Authorization items form a hierarchy. Higher level items inheirt permissions representing
	 * by lower level items.
	 * @param string $name the item name. This must be a unique identifier.
	 * @param integer $type the item type (0: operation, 1: task, 2: role).
	 * @param string $description description of the item
	 * @param string $bizRule business rule associated with the item. This is a piece of
	 * PHP code that will be executed when {@link checkAccess} is called for the item.
	 * @param mixed $data additional data associated with the item.
	 * @return CAuthItem the authorization item
	 * @throws CException if an item with the same name already exists
	 */
	public function createAuthItem($name,$type,$description='',$bizRule=null,$data=null)
	{
		switch($type) {
			case CAuthItem::TYPE_ROLE : {
				// a role in Docebo is a group, so create a group !
				throw new CException('To be implemented');
			};break;
			case CAuthItem::TYPE_OPERATION : {
				// a role in Docebo is a group, so create a group !
				throw new CException('To be implemented');
			};break;
			case CAuthItem::TYPE_TASK :
			default: {
				throw new CException('Unrecognized or unsopported type');
			}
		}
	}

	/**
	 * Removes the specified authorization item.
	 * @param string $name the name of the item to be removed
	 * @return boolean whether the item exists in the storage and has been removed
	 */
	public function removeAuthItem($name)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Returns the authorization item with the specified name.
	 * @param string $name the name of the item
	 * @return CAuthItem the authorization item. Null if the item cannot be found.
	 */
	public function getAuthItem($name)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}

	/**
	 * Saves an authorization item to persistent storage.
	 * @param CAuthItem $item the item to be saved.
	 * @param string $oldName the old item name. If null, it means the item name is not changed.
	 */
	public function saveAuthItem($item,$oldName=null)
	{
		throw new CException('You cannot do this with DoceboAuthManager due to the structure of the permission, use the appropiates models instead');
	}


    /**
     * Reset the authentication saved for the given user and reload them from the database
     * @param mixed $userId the user ID (see {@link IWebUser::getId})
     */
    public function refreshCache($userId) {
        $cacheid = md5('assignments-' .$userId);

        $this->assignments[$userId] 	= array();
        $this->assignments_id[$userId]  = array();

        // Session
        if (Yii::app()->user->id == $userId) {
            unset(Yii::app()->session['public_area_stlist']);
        }

        // Delete the cache
        Yii::app()->cache->delete($cacheid);

        // This will resolve the access again
        $this->solveAccessRecursive($userId, array());
    }

	/**
	 * Saves the authorization data to persistent storage.
	 */
	public function save()
	{
		// No need to do this later it's automatically done for each assignement
	}

	/**
	 * Removes all authorization data.
	 */
	public function clearAll()
	{
		// Really ? No way that you can do this on application level.
	}

	/**
	 * Removes all authorization assignments.
	 */
	public function clearAuthAssignments()
	{
		// Really ? No way that you can do this on application level.
	}


}