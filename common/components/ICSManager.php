<?php

/**
 * Class ICSManager
 * Used to generate .ics files for calendars/events etc
 */
class ICSManager extends CComponent
{

	private $data;
	public $fileName;

	const TYPE_REQUEST = 'REQUEST';
	const TYPE_CANCEL = 'CANCEL';

    public $orgName;
    public $orgEmail;

	public function __construct($type, $start, $end, $name, $description, $location = null, $organizerName = null, $organizerEmail = null, $sessionModel = null, $sessionDatesData = null)
	{
        $this->orgName = $organizerName;
        $this->orgEmail = $organizerEmail;

		$description = str_replace(array("\r\n", "\n", "\r",), array('\n', '\n', '\n'), html_entity_decode(strip_tags($description)));

		if($sessionModel && ($sessionModel instanceof  LtCourseSession || $sessionModel instanceof  WebinarSession)){
			$sessionDatesData = $sessionModel->getIcsDatesData($name, $description, $location, $organizerName, $organizerEmail, $type);
		}

		$this->fileName = date("Y_m_d", strtotime(Yii::app()->localtime->fromLocalDateTime($start)))."_".uniqid().'.ics';

        // get inside only if there is dates in the session else...
		if($sessionDatesData){
			$this->data = "BEGIN:VCALENDAR\r\n"
				."VERSION:2.0\r\n"
				."METHOD:".$type."\r\n"
				.$sessionDatesData
				."END:VCALENDAR\r\n";
		}else{

			// Some checks for Organizer name and email if they are missing
			$organizer = "ORGANIZER;CN=";
			if($organizerName && $organizerEmail) {
				$organizer .= "{$organizerName}:MAILTO:{$organizerEmail}\r\n";
			} elseif($organizerName && !$organizerEmail) {
				$organizer .= "{$organizerName}\r\n";
			} elseif(!$organizerName && $organizerEmail) {
				$organizer .= "{$organizerEmail}\r\n";
			} else {
				$organizer = "";
			}

			$this->data = "BEGIN:VCALENDAR\r\n"
				."VERSION:2.0\r\n"
				."METHOD:{$type}\r\n"
				."BEGIN:VEVENT\r\n"
				."DTSTART:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($start), 0, 19)).'Z'."\r\n"
				."DTEND:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($end), 0, 19)).'Z'."\r\n"
				."FREEBUSY;FBTYPE=BUSY:".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($start), 0, 19)).'Z'."/".str_replace(array('-', ':'), array('',''), substr(Yii::app()->localtime->fromLocalDateTime($end), 0, 19)).'Z'."\r\n"
				."TRANSP: OPAQUE\r\n"
				."SEQUENCE:0\r\n"
				."UID:\r\n"
				."DTSTAMP:".date("Ymd\THis\Z")."\r\n"
				."SUMMARY:{$name}\r\n"
				."DESCRIPTION:{$description}\r\n"
				."PRIORITY:1\r\n"
				//."ORGANIZER;CN=\"{$organizerName}\":{$organizerEmail}\r\n"
				.$organizer
				."CLASS:PUBLIC\r\n"
				."X-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\n"
				."X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\n"
				."X-MICROSOFT-CDO-IMPORTANCE:1\r\n"
				."BEGIN:VALARM\r\n"
				."TRIGGER:-PT15M\r\n" // reminder 60 minutes before the event
				."ACTION:DISPLAY\r\n"
				."DESCRIPTION:Reminder\r\n"
				."END:VALARM\r\n"
				."END:VEVENT\r\n"
				."END:VCALENDAR\r\n";
		}

		$this->save();
	}

	public function save()
	{
		file_put_contents(Docebo::getUploadTmpPath().'/'.$this->fileName, $this->data);
	}

	public function filePath()
	{
		return Docebo::getUploadTmpPath().'/'.$this->fileName;
	}

	public function delete()
	{
		if (file_exists($this->filePath()))
			unlink($this->filePath());
	}

}