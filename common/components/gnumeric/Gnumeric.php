<?php
/**
 * A component providing an interface to OS based Gnumeric commands.
 * In order to use it, Gnumeric must be istalled.
 * Most used command is "ssconvert".
 *
 * @version Built on top of Gnumeric v1.12.15
 *
 * @link  	http://www.gnumeric.org/
 * @see		http://linuxcommand.org/man_pages/ssconvert1.html
 *
 * @property array $convertOptions Options passed to ssconvert command
 *
 */
class Gnumeric extends CComponent {


	const MAX_ROWS_XLS = 65000;

	// Importers  (--import-type=....)
	const Gnumeric_applix__applix       = 'Gnumeric_Excel__excel'; 			//  MS ExcelT (*.xls)
	const Gnumeric_Excel__xlsx          = 'Gnumeric_Excel__xlsx'; 			//  ECMA 376 / Office Open XML [MS ExcelT 2007/2010] (*.xlsx)
	const Gnumeric_Excel__excel_enc     = 'Gnumeric_Excel__excel_enc'; 		//  MS ExcelT (*.xls) requiring encoding specification
	const Gnumeric_html__html           = 'Gnumeric_html__html'; 			//  HTML (*.html, *.htm)
	const Gnumeric_oleo__oleo           = 'Gnumeric_oleo__oleo'; 			//  GNU Oleo (*.oleo)
	const Gnumeric_paradox__paradox     = 'Gnumeric_paradox__paradox'; 		//  Paradox database or primary index file (*.db, *.px)
	const Gnumeric_QPro__qpro           = 'Gnumeric_QPro__qpro'; 			//  Quattro Pro (*.wb1, *.wb2, *.wb3)
	const Gnumeric_xbase__xbase         = 'Gnumeric_xbase__xbase'; 			//  Xbase (*.dbf) file format
	const Gnumeric_sc__sc               = 'Gnumeric_sc__sc'; 				//  SC/xspread
	const Gnumeric_XmlIO__sax           = 'Gnumeric_XmlIO__sax'; 			//  Gnumeric XML (*.gnumeric)
	const Gnumeric_lotus__lotus         = 'Gnumeric_lotus__lotus'; 			//  Lotus 123 (*.wk1, *.wks, *.123)
	const Gnumeric_dif__dif             = 'Gnumeric_dif__dif'; 				//  Data Interchange Format (*.dif)
	const Gnumeric_Excel__excel_xml     = 'Gnumeric_Excel__excel_xml'; 		//  MS ExcelT 2003 SpreadsheetML
	const Gnumeric_mps__mps             = 'Gnumeric_mps__mps'; 				//  Linear and integer program (*.mps) file format
	const Gnumeric_OpenCalc__openoffice = 'Gnumeric_OpenCalc__openoffice'; 	//  Open Document Format (*.sxc, *.ods)
	const Gnumeric_plan_perfect__pln    = 'Gnumeric_plan_perfect__pln'; 	//  Plan Perfect Format (PLN) import
	const Gnumeric_sylk__sylk           = 'Gnumeric_sylk__sylk'; 			//  MultiPlan (SYLK)
	const Gnumeric_stf__stf_csvtab      = 'Gnumeric_stf__stf_csvtab'; 		//  Comma or tab separated values (CSV/TSV)

	// Exporters (--export-type=....)
	//const Gnumeric_sylk__sylk 			= 'Gnumeric_sylk:sylk';  			// MultiPlan (SYLK)
	//const Gnumeric_paradox__paradox 	= 'Gnumeric_paradox:paradox';    	//  Paradox database (*.db)
	const Gnumeric_OpenCalc__odf 		= 'Gnumeric_OpenCalc:odf';      	//  ODF 1.2 extended conformance (*.ods)
	//const Gnumeric_OpenCalc__openoffice = 'Gnumeric_OpenCalc:openoffice'; 	//  ODF 1.2 strict conformance (*.ods)
	const Gnumeric_lpsolve__lpsolve 	= 'Gnumeric_lpsolve:lpsolve';   	//  LPSolve Linear Program Solver
	const Gnumeric_html__roff 			= 'Gnumeric_html:roff';         	//  TROFF (*.me)
	const Gnumeric_html__latex_table 	= 'Gnumeric_html:latex_table';  	//  LaTeX 2e (*.tex) table fragment
	const Gnumeric_html__latex 			= 'Gnumeric_html:latex';        	//  LaTeX 2e (*.tex)
	const Gnumeric_html__xhtml_range 	= 'Gnumeric_html:xhtml_range';  	//  XHTML range - for export to clipboard
	const Gnumeric_html__xhtml 			= 'Gnumeric_html:xhtml';        	//  XHTML (*.html)
	const Gnumeric_html__html40frag 	= 'Gnumeric_html:html40frag';   	//  HTML (*.html) fragment
	const Gnumeric_html__html40 		= 'Gnumeric_html:html40';       	//  HTML 4.0 (*.html)
	const Gnumeric_html__html32 		= 'Gnumeric_html:html32';       	//  HTML 3.2 (*.html)
	const Gnumeric_glpk__glpk 			= 'Gnumeric_glpk:glpk';         	//  GLPK Linear Program Solver
	const Gnumeric_Excel__xlsx2 		= 'Gnumeric_Excel:xlsx2';       	//  ISO/IEC 29500:2008 & ECMA 376 2nd edition (2008); [MS ExcelT 2010]
	//const Gnumeric_Excel__xlsx 			= 'Gnumeric_Excel:xlsx';        	//  ECMA 376 1st edition (2006); [MS ExcelT 2007]
	const Gnumeric_Excel__excel_dsf 	= 'Gnumeric_Excel:excel_dsf';   	//  MS ExcelT 97/2000/XP & 5.0/95
	const Gnumeric_Excel__excel_biff7 	= 'Gnumeric_Excel:excel_biff7'; 	//  MS ExcelT 5.0/95
	const Gnumeric_Excel__excel_biff8 	= 'Gnumeric_Excel:excel_biff8'; 	//  MS ExcelT 97/2000/XP
	//const Gnumeric_dif__dif				= 'Gnumeric_dif:dif';           	//  Data Interchange Format (*.dif)
	const Gnumeric_stf__stf_csv			= 'Gnumeric_stf:stf_csv';       	//  Comma separated values (CSV)
	const Gnumeric_stf__stf_assistant 	= 'Gnumeric_stf:stf_assistant'; 	//  Text (configurable)
	const Gnumeric_XmlIO__sax__0		= 'Gnumeric_XmlIO__sax:0';       	//  Gnumeric XML uncompressed (*.xml)
	//const Gnumeric_XmlIO__sax 			= 'Gnumeric_XmlIO:sax';         	//  Gnumeric XML (*.gnumeric)
	const Gnumeric_pdf__pdf_assistant 	= 'Gnumeric_pdf:pdf_assistant'; 	//  PDF export


	const FORMAT_CSV 	= 'csv';
	const FORMAT_XLS	= 'xls';
	const FORMAT_HTML	= 'html';
	const FORMAT_PDF	= 'pdf';

	/**
	 * Error code, if any
	 * @var integer
	 */
	private $_errorCode = false;

	/**
	 * Possible gnumeric errors
	 * @var array
	 */
	private $_errors = array(
		'1' 	=> '[gnumeric] Error while converting file',
		'500' 	=> '[gnumeric] Gnumeric ssconvert executable not found',
	);


	/**
	 * Return error text
	 * @param integer $code
	 * @return string
	 */
	public function getError($code=false) {
		if ($code === false) {
			$code = $this->_errorCode;
		}

		if ($code === false) {
			return Yii::t('gnumeric', 'No error');
		}

		if (!isset($this->_errors[$code])) {
			return Yii::t('gnumeric', 'Unknown Gnumeric error');
		}

		return Yii::t('gnumeric', $this->_errors[$code]);

	}

	/**
	 *
	 * @return number
	 */
	public function getErrorCode() {
		return $this->_errorCode;
	}



	/**
	 * Default ssconvert options
	 * @var array
	 */
	private $_convertOptions = array(
		'--import-encoding' => 'UTF-8',
		//'format' => 'raw',
		//'separator' => ',',
		//'quote' => '"'
		'-O' => "'format=raw'"
	);


	/**
	 * Constructor
	 *
	 * @param array $convertOptions *Replace* default, hard coded options
	 */
	public function __construct($convertOptions = array()) {

		// Resolve "ssconvert" options, taking from the incoming parameter, then from Yii params config and finally using default hard coded here
		if (!is_array($convertOptions)) {
			$this->setConvertOptions($convertOptions);
		}
		else {
			if (isset(Yii::app()->params['gnumeric']['convert_options'])) {
				$this->setConvertOptions(Yii::app()->params['gnumeric']['convert_options']);
			}
		}
	}

	/**
	 * Add (merge) extra converting options to current one
	 * @param array $options
	 */
	private function addConvertOptions($options = array()) {
		$this->_convertOptions = array_merge($this->_convertOptions, $options);
	}


	/**
	 * Transform an array of options into a string, suitable for passing to command line
	 *
	 * @param array $options
	 * @return string
	 */
	private function getOptionsString($options = array()) {
		$result = '';
		foreach ($options as $key => $value) {
			$result .= " $key=$value ";
		}
		return $result;
	}

	/**
	 * Set converting options
	 *
	 * @param array $options
	 */
	public function setConvertOptions($options) {
		$this->_convertOptions = $options;
	}

	/**
	 * Get converting options
	 *
	 * @return array
	 */
	public function getConvertOptions() {
		return $this->_convertOptions;
	}


	/**
	 * Convert input file and save the result to output file
	 *
	 * @param string $format Export format (see this class constants: csv, xls, html, ... )
	 * @param string $input  Absolute path to INPUT CSV file, incl. file name
	 * @param string $output Absolute path to OUPUT file, incl. file name
	 * @param array $options Additional conversion options (or options to relpace with, see mergeOptions)
	 * @param boolen $mergeOptions Replace OR merge current options with the incoming ones
	 */
	public function convertTo($input, $output, $format = false, $options = array(), $mergeOptions = true) {
		if (is_array($options)) {
			if ($mergeOptions) {
				$this->addConvertOptions($options);
			}
			else {
				$this->_convertOptions = $options;
			}
		}

		// We are converting FROM CSV format; This option IS absolutely required!
		$this->addConvertOptions(array('--import-type' => 'Gnumeric_stf:stf_csvtab'));

		// Add more options
		$extraOptions = array();

		// FORCE export format to.. ?
		if ($format) {
			$exportType = false;
			switch ($format) {
				case self::FORMAT_CSV:
					$exportType = self::Gnumeric_stf__stf_csv;
					break;
				case self::FORMAT_XLS:
					$exportType = self::Gnumeric_Excel__excel_biff8;
					break;
				case self::FORMAT_HTML:
					$exportType = self::Gnumeric_html__html40;
					break;
				case self::FORMAT_PDF:
					$exportType = self::Gnumeric_pdf__pdf_assistant;
					break;
			}
			if ($exportType) {
				$this->addConvertOptions(array('--export-type' => $exportType));
			}

		}

		// Add whatever extra options we have
		$this->addConvertOptions($extraOptions);

		// Convert options to string
		$opttionsString = $this->getOptionsString($this->convertOptions);

		// Build command string
		$command = '"' . Yii::app()->params['gnumeric']['convert_program'] . '"' . " " . $opttionsString . " " . $input . " " . $output;

		// Execute the command
		$returnVar = "";
		$dummy = array();
 		exec($command, $dummy, $returnVar);

 		if ($returnVar != 0) {
 			// There is no documentation regarding ssconvert return error codes, just use 1
 			$this->_errorCode = 1;
 			return false;
 		}
		return true;
	}


	/**
	 * Check if we have gnumeric installed
	 *
	 * @return boolean
	 */
	public function checkGnumeric() {
		if (!isset(Yii::app()->params['gnumeric']['convert_program']) ||
			!is_file(Yii::app()->params['gnumeric']['convert_program'])) {
			$this->_errorCode = 500;
			return false;
		}
		return true;
	}

}