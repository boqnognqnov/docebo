<?php
/**
 * Extending Yii Url manager to allow URL modifications
 *
 * @author Plamen Petkov
 *
 */
class DoceboUrlManager extends CUrlManager {

	/**
	 * Force stripping "release" part from URL, set in Yii parameters (Yii::app()->params['releaseSubfolder']), if set
	 *
	 * If you want this part NOT stripped, set this to false before calling createUrl()/createAbsoluteUrl()
	 * 		Yii::app()->urlManager->setStripRelease(false)
	 *
	 * If you want this part not stripped at all, just comment the Yii parameter 'releaseSubfolder' in config files.
	 *
	 * @var Boolean
	 */
	private  $_stripRelease = true;


	/**
	 * Override base createUrl to provide "stripping" "releaseXX" from URL
	 *
	 * (non-PHPdoc)
	 * @see CUrlManager::createUrl()
	 */
	public function createUrl($route, $params = array(), $ampersand = '&', $includeMultidomain = true) {

		// Clear the domain params before URL construction
		unset($params['domain']);

		$url = parent::createUrl($route, $params, $ampersand);

		if ($includeMultidomain && !empty($_GET['domain'])) {
			$url = "/" . $_GET['domain'] . $url;
		}

		// Sometimes, we may have a multi-version environment, where different LMS versions live
		// under the same web root in their own subfolders. In that case, each LMS MAY provide
		// its subfolder as a parameter in main-saas.php config file. If it is set, we do this trick
		// to remove the "release subfolder" from URLs we are creating.
		// Note, this only works in combination with the load balancer proxy, release dispatcher, etc.
		if (isset(Yii::app()->params['releaseSubfolder']) && !empty(Yii::app()->params['releaseSubfolder']) && ($this->_stripRelease === true)) {
			$pattern = '/\/' . Yii::app()->params['releaseSubfolder'] . '/';
			$url = preg_replace($pattern, '', $url);
		}

		return $url;

	}

    /**
     * Creates a url, using the passed route name and arguments, that would be rendered successfully in
     * Hydra Frontend application. If there is a page equivalent existing - it's url is returned, otherwise
     * it's loaded using the LegacyWrapper component in an iframe.
     *
     * @param  string $route
     * @param  array  $params
     * @param  string $ampersand
     *
     * @return string
     */
    public function createHydraUrl($route, $params = [], $ampersand = '&', $urlEncoded = false)
    {
        $legacyUrl = $this->createUrl($route, $params, $ampersand, false);
        $equivalent = Yii::app()->legacyWrapper->getHydraEquivalent($route);
        $prefixed   = '/legacy' . $legacyUrl;

        if(!empty($_GET['domain'])){
            $prefixed = '/'.$_GET['domain'].$prefixed;
        }

        if ($equivalent) {
            return $equivalent;
        }

        if ($urlEncoded === false) {
            return $prefixed;
        }

        $prefixed = parse_url($prefixed);
        $prefixed['query'] = urlencode('?' . $prefixed['query']);
        $prefixed = $prefixed['path'] . $prefixed['query'];

        return $prefixed;
    }

    /**
     * Converts the current url to a Hydra FE application one
     *
     * @param  null
     *
     * @return string
     */
    public function convertToHydraUrl()
    {
        $queryParams = $_GET;
        if (isset($queryParams['r'])) {
            unset($queryParams['r']);
        }

        $cleanParams = array_diff_key($queryParams, array_flip(Yii::app()->legacyWrapper->dirtyQueryParametes));

        return $this->createHydraUrl(Yii::app()->request->getQuery('r'), $cleanParams);
    }

    /**
     * Return the relative path to the Hydra FE application's homepage
     *
     * @return string
     */
    public function hydraHomeUrl()
    {
        return '/learn';
    }

    /**
     * Cleanses a passed url from unwanted GET parameters
     *
     * @param  array  $dirtyParams - the parameters to be removed
     * @param  string $dirtyUrl    - the url to be cleansed, if not passed uses the current request's url
     *
     * @return string
     *
     * @todo   Find a better way for getting all GET parameters
     */
    public function cleanseUrl($dirtyParams, $dirtyUrl = null)
    {
        $dirtyUrl    = $dirtyUrl ?: Yii::app()->request->getUrl();
        $urlBase     = strtok($dirtyUrl, '?');
        $cleanParams = $this->getCleanQueryParams($dirtyParams);
        $cleanUrl    = urldecode($urlBase . '?' . http_build_query($cleanParams));

        return $cleanUrl;
    }

    /**
     * Cleanses the current GET params, removing the passed array of param names
     *
     * @param array $dirtyParams
     *
     * @return array
     */
    public function getCleanQueryParams($dirtyParams)
    {
        $queryParams = $_GET;
        $flippedDirtyParams = array_flip($dirtyParams);

        return array_diff_key($queryParams, $flippedDirtyParams);
    }

	public function setStripRelease($value) {
		$this->_stripRelease = $value;
	}

	public function getStripRelease() {
		return $this->_stripRelease;
	}

    /**
     * Generates an url-safe string
     * Non letter digits are replaced by '-' (dashes)
     * Words are lower-cased
     * @static
     * @param $text
     * @return mixed|string
     */
    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
		$text = $this->transliterateString($text);
        //$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

	private function transliterateString($txt) {
		$transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A',
			'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE',
			'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C',
			'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh',
			'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E',
			'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g',
			'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i',
			'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k',
			'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n',
			'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o',
			'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r',
			'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's',
			'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U',
			'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U',
			'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W',
			'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z',
			'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e',
			'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l',
			'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't',
			'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch',
			'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
		return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
	}

}
