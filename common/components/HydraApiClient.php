<?php

class HydraApiClient
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PATCH = 'PATCH';

    private $name;
    private $version = 1;
    private $accessToken;

    private $baseUrl;
    private $apiEndpointUrl;
    
    public function __construct($microservice = 'skill', $version = 1){
        $this->baseUrl = Yii::app()->request->hostInfo;
        $this->version = $version;
        $this->name = $microservice;
        $this->apiEndpointUrl = "{$this->baseUrl}/{$this->name}/v{$this->version}/";
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getApiEndpointUrl()
    {
        return $this->apiEndpointUrl;
    }

    /**
     * @param mixed $apiEndpointUrl
     */
    public function setApiEndpointUrl($apiEndpointUrl)
    {
        $this->apiEndpointUrl = $apiEndpointUrl;
    }


    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Execute an immediate call to the microservice and get response
     *
     * @param $apiUrl
     * @param array $params
     * @param string $method
     * @param boolean $useJsonHeaders
     * @return mixed
     */
    public function execute($apiUrl, $params = [], $method = self::METHOD_POST, $useJsonHeaders = true)
    {
        // Get cURL resource
        $curl = curl_init();

        // set the universal options (independent of request METHOD)
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->apiEndpointUrl . $apiUrl,
        ));

        switch ($method) {
            case self::METHOD_GET:
                $queryStr = http_build_query($params);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $this->apiEndpointUrl . $apiUrl . '?' . $queryStr
                ));
                break;
            case self::METHOD_POST:
                curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($params),
                ));
                break;
            default:
                curl_setopt_array($curl, array(
                CURLOPT_URL => $this->apiEndpointUrl . $apiUrl,
                CURLOPT_CUSTOMREQUEST => mb_strtoupper($method),
                CURLOPT_POSTFIELDS => http_build_query($params),
                ));
        }

        $headers = [];
        if ($useJsonHeaders) {
            $headers[] = 'Content-type: application/json;charset=UTF-8';
        }

        if (!empty($this->accessToken))
            array_push($headers, "Authorization: Bearer {$this->accessToken}");
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        if (curl_error($curl)) {
            Yii::log( "Api call FAILED: " . $this->apiEndpointUrl . " Error: ".curl_error($curl), CLogger::LEVEL_ERROR, 'hydra-api-client' );
            curl_close($curl);
        } else {
            $json = json_decode($resp, true);
            curl_close($curl);
            if ($json) {
                if(isset($json['data'])) {
                    return $json['data'];
                }elseif(isset($json['status']) && $json['status'] >= 400){
                    Yii::log( 'Microservice '.$this->name.' call failed with error '.$json['status'].': '.print_r($json['message'],true), CLogger::LEVEL_ERROR, 'hydra-api-client' );
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }
    
    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

}