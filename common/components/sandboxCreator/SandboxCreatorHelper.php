<?php

/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class SandboxCreatorHelper
{

	/**
	 * The core_setting param name where the sandbox details are saved (as JSON)
	 */
	const SANDBOX_DETAILS_SETTING_NAME = 'sandbox_details';

	const SANDBOX_STATUS_NOT_REQUESTED_YET = '';
	const SANDBOX_STATUS_ACTIVATING = 'activating';
	const SANDBOX_STATUS_ACTIVATED = 'activated';

	/**
	 * Returns an array of information from core_setting (param name: sandbox_details)
	 * with information about the current LMSs' sandbox if one is created or creating.
	 *
	 * This information includes the URL, expiration date and the user ID that
	 * initiated the sandbox creation process.
	 *
	 * With the following values:
	 *      id_user
	 *      sandbox_details
	 *              requested_timestamp
	 *              created_timestamp
	 *              status
	 *              expiration_timestamp
	 *              url
	 *
	 * @return array
	 *
	 */
	static private function getSandboxDetails()
	{
		$idUserInitiator = false;
		$status = self::SANDBOX_STATUS_NOT_REQUESTED_YET;
		$dateRequested = null;
		$dateSandboxCreated = null;
		$dateExpireSandbox = null;
		$sandboxUrl = 'http://example.com';

		$raw = Settings::get('sandbox_details');
		if (!empty($raw)) {
			$json = CJSON::decode($raw);

			// super admin who initiated the sandbox creation pocess
			$idUserInitiator = isset($json['id_user']) ? (int)$json['id_user'] : false;

			$sandboxDetails = $json['sandbox_details'];

			// UTC timestamp (integer) when the sandbox creation was requested
			if (isset($sandboxDetails['date_requested'])) {
				$dateRequested = $sandboxDetails['date_requested'];
			}
			// UTC timestamp (integer) when the sandbox was fully created and configured
			if (isset($sandboxDetails['date_finished'])) {
				$dateSandboxCreated = $sandboxDetails['date_finished'];
			}
			// UTC timestamp (integer) when the sandbox expires
			if (isset($sandboxDetails['date_expire'])) {
				$dateExpireSandbox = $sandboxDetails['date_expire'];
			}
			if (isset($sandboxDetails['sandbox_url'])) {
				$sandboxUrl = $sandboxDetails['sandbox_url'];
			}

			// Is the sandbox?
			// 1. Not yet requested
			// 2. Requested and activating now
			// 3. Requested and fully activated
			if (isset($sandboxDetails['status'])) {

				if (in_array($sandboxDetails['status'], array(
					self::SANDBOX_STATUS_NOT_REQUESTED_YET,
					self::SANDBOX_STATUS_ACTIVATING,
					self::SANDBOX_STATUS_ACTIVATED
				))) {
					$status = $sandboxDetails['status'];
				}
			}
		}

		return array(
			'id_user' => $idUserInitiator,
			'sandbox_details' => array(
				'requested_timestamp' => $dateRequested,
				'created_timestamp' => $dateSandboxCreated,
				'status' => $status,
				'expiration_timestamp' => $dateExpireSandbox,
				'url' => $sandboxUrl,
			),
		);
	}

	/**
	 * @return mixed One of the following: "activating", "activated" or empty string
	 * which means sandbox creation has not been started at all yet
	 */
	static public function getSandboxActivationStatus()
	{
		$details = self::getSandboxDetails();
		return $details['sandbox_details']['status'];
	}

	/**
	 * Whether or not the sandbox for this LMS has been created
	 * @return bool
	 */
	static public function isCreated()
	{
		return self::getSandboxActivationStatus() == self::SANDBOX_STATUS_ACTIVATED;
	}

	/**
	 * Whether or not the sandbox creation process for this LMS is in progress at the moment
	 * @return bool
	 */
	static public function isInProgress()
	{
		return self::getSandboxActivationStatus() == self::SANDBOX_STATUS_ACTIVATING;
	}

	/**
	 * Call the ERP (only once per session) and check if the current LMS
	 * is able to initiate a sandbox creation process.
	 * Sandbox creation is currently possible 3 weeks before the official
	 * release of the next version (but can change in the future)
	 *
	 * @return bool|int
	 */
	static public function isCreationAllowedByERP()
	{
		$isAllowed = true;

		if (isset(Yii::app()->session['sandbox_creation_allowed'])) {
			$isAllowed = (bool)Yii::app()->session['sandbox_creation_allowed'];
		} else {
			$client = new ErpApiClient2();
			$isCreationAllowedByErp = $client->getIsSandboxAvailable();

			if ($isCreationAllowedByErp) {
				$isAllowed = 1;
			} else {
				$isAllowed = 0;
			}
			Yii::app()->session['sandbox_creation_allowed'] = $isAllowed;
		}
		return $isAllowed;
	}

	/**
	 * Get the locally saved sandbox URL.
	 * Only available and populated by an ERP api call
	 * when the sandbox has finished setting up
	 * @return mixed
	 */
	public static function getSandboxUrl()
	{
		$details = self::getSandboxDetails();
		$url = $details['sandbox_details']['url'];

		if($url && stripos('http://', $url)===false && stripos('https://', $url)===false){
			$url = 'http://'.$url;
		}
		return $url;
	}

	public static function shouldShowSandboxUI()
	{
		// Manually override sandbox creation logic completely
		if(Settings::get('disable_sandbox_creation'))
			return false;

		$res = true;
		if (!Yii::app()->user->getIsGodadmin()) {
			// Only super admins can see the sandbox creation UI or new sandbox link generally
			$res = false;
		} else {
			// Current user is a super admin

			if (PluginManager::isPluginActive('WhitelabelApp')) {
				// Super admins with LMS where the WhiteLabelApp is active
				// can choose to show "special" UI elements like billing related,
				// sandbox creation, etc - to just one super admin (himself)

				$showOnlyToSingleSuperAdmin = Settings::get('whitelabel_menu', null);

				if ($showOnlyToSingleSuperAdmin) {
					$superAdminIdToShowTo = Settings::get('whitelabel_menu_userid', null);
					if ($superAdminIdToShowTo == Yii::app()->user->getIdst()) {
						$res = true;
					} else {
						$res = false;
					}
				}
			}
		}

		return $res;
	}

} 