<?php

/**
 * Description of App7020ShareLink
 *
 * @author velislav
 */
class App7020ShareLink extends HttpRequest {

    /**
     *
     * @var type 
     */
    public $title;

    /**
     *
     * @var type 
     */
    public $description;

    /**
     *
     * @var type 
     */
    public $image;

    /**
     * 
     * @param type $url
     */
    public function __construct($url = false) {
        if ($url) {
            parent::__construct($url);
            $this->init();
        }
    }

    /**
     * It returns needed html elements (title, description)
     * @param str $url
     * @return string
     */
    public function getElements() {

        $tempElements = array();
        $tempElements['title'] = false;
        $tempElements['description'] = false;
        $tempElements['image'] = false;
        $dom = $this->simpleDom->str_get_html($this->getResponse());
         if ($this->responce !== false) {
            $tempElements = array();

            $tempElements['title'] = $dom->find("title", 0)->content;
            $tempElements['description'] = $dom->find("meta[name='description']", 0)->content;

            // Get temp "Open Graph" elements
            $tempElements['meta_og_title'] = $dom->find("meta[property='og:title']", 0)->content;
            if (!empty($tempElements['meta_og_title'])) {
                $tempElements['title'] = $tempElements['meta_og_title'];
                unset($tempElements['meta_og_title']);
            }

            //directives for grab desc
            $tempElements['meta_og_description'] = $dom->find("meta[property='og:description']", 0)->content;
            if (!empty($tempElements['meta_og_description'])) {
                $tempElements['description'] = $tempElements['meta_og_description'];
                unset($tempElements['meta_og_description']);
            } else if (empty($tempElements['description'])) {
                $tempElements['description'] = false;
            }

            //directives for greab image 
            $tempElements['image'] = $dom->find("meta[property='og:image']", 0)->content;
            if (empty($tempElements['imege'])) {
                $tempElements['imege'] = false;
            }
        }
        return $tempElements;
    }

    /**
     * Init of  components
     */
    function init() {
        $el = $this->getElements();
        $this->image = $el['image'];
        $this->title = $el['title'];
        $this->description = $el['description'];
    }

    /**
     * Get Title
     * @return string
     */
    function getTitle() {
        return $this->title;
    }

    /**
     * GET grabed description
     * @return string
     */
    function getDescription() {
        return $this->description;
    }

    /**
     * Get grabed image
     * @return type
     */
    function getImage() {
        return $this->image;
    }

    /**
     * Set the title 
     * @param string $title
     */
    function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Set the description
     * @param string $description
     */
    function setDescription($description) {
        $this->description = $description;
    }

    /**
     * Set the image
     * @param string $image
     */
    function setImage($image) {
        $this->image = $image;
    }

    /**
     * Check for image 
     * @return boolean
     */
    function hasImage() {
        if (empty($this->image)) {
            return false;
        } else {
            return true;
        }
    }

}
