<?php
/**
 * Specific class to connect to REDIS server dedicated for LMS Settings
 *  
 */
class RedisSettings extends RedisBase {
    
    /**
     * Redis Branch where all settings hash keys go, e.g.
     * 
     *      settings:m:y:mylms.docebo.com
     *      settings:a:n:anotherlms.docebo.com
     * 
     * @var string
     */
    const LMS_SETTINGS_KEYPREFFIX = "settings";
    
    public $key;
    
    public function __construct() {
        parent::__construct(REDIS_CONFIG_HOST, REDIS_CONFIG_PORT, REDIS_CONFIG_DB);
        $domain = trim(Docebo::getOriginalDomain());
        $this->key = self::LMS_SETTINGS_KEYPREFFIX . ":" . $domain[0] . ":" . $domain[1] . ":" . $domain;
    }
    
    /**
     * 
     * @return multitype:|multitype:CoreSetting
     */
    public static function getAllLmsSettingsAsModels() {
        
        $object = self::getInstance();
        if (!$object) return array();
        
        $settings = $object->client->hgetall($object->key);
        
        $models = array();
        if (is_array($settings) && !empty($settings)) {
            foreach ($settings as $paramName => $paramValue) {
                $model = new CoreSetting();
                $model->param_name = $paramName;
                $model->param_value = $paramValue;
                $models[] = $model;
            }
        }
        
        return $models;
        
        
    }
    
    /**
	 * Returns all settings in an array of strings
	 * 
	 * @return array
     */
    public static function getAll() {

        $object = self::getInstance();
        if (!$object) return array();
        
        $settings = $object->client->hgetall($object->key);
        
        $data = array();
        if (is_array($settings) && !empty($settings)) {
            foreach ($settings as $paramName => $paramValue) {
                $data[$paramName] =  $paramValue;
            }
        }
        
        return $data;
        
        
    }
    
    
    /**
     * 
     * @param unknown $paramName
     */
    public static function exists($paramName) {
        $object = self::getInstance();
        if (!$object) return false;
        return $object->client->hexists($object->key, $paramName);
    }
    
    /**
     * 
     * @param unknown $paramName
     * @param string $defaultValue
     */
    public static function get($paramName, $defaultValue = null) {
        
        $object = self::getInstance();
        if (!$object) return $defaultValue;
        
        $value = $object->client->hget($object->key, $paramName);
        
        if (!$value) {
            return $defaultValue;
        }
    }

    /**
     * Remove a named setting from REDIS
     * 
     * @param array $fields
     */
    public static function remove($fields) {
        
        // Just try to be careful, check everything
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        foreach ($fields as $index => $field) {
            if (!is_string($field)) {
                unset($fields[$index]);
            }
        }
        
        $object = self::getInstance();
        if (!$object) return 0;
        
        $result = $object->client->hdel($object->key, $fields);

        return $result;  // number of deleted 
        
    }
    

    /**
     * 
     * @param unknown $field
     * @param unknown $value
     * @param string $existentOnly
     */
    public static function save($field, $value, $existentOnly=false) {
        $object = self::getInstance();
        if (!$object) return true;
        $result = $object->client->hset($object->key, $field, $value);
    }
    
    
}