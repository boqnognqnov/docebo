<?php
/**
 * Class RedisStats
 * 
 * Write LMS Statistics in a Redis database
 */
class RedisStats extends \RedisBase
{

    /**
     * Redis Statistics branch 
     * @var string
     */
    const REDIS_STATS_BRANCH = 'lms_stats';
    
    
    /**
     * Redis Statistics Database number 
     * @var integer
     */
    const REDIS_STATS_DB = 11;

    /**
     * THIS LMS statistics hash key in the Redis DB, e.g.  "lms_stats:my.domain.com"
     * @var string
     */
    public $key;

    
    /**
     * Get the LMS domain name and connect to Redis database. Prepare the KEY as well.
     */
    public function __construct($host=false, $port=false, $database=false) {
        if (!$host) $host = REDIS_CONFIG_HOST;
        if (!$port) $port = REDIS_CONFIG_PORT;
        if (!$database) $database = self::REDIS_STATS_DB;
        parent::__construct($host, $port, $database);
        $domain = trim(Docebo::getOriginalDomain());
        $this->key = self::REDIS_STATS_BRANCH .":". $domain;
    }

    /**
     * Get all stored statistics about THIS LMS
     */
    public static function getAll() {
        $object = self::getInstance();
        if ( !$object ) return array();
        $data = array();
        try {
            $stats = $object->client->hgetall( $object->key );
            if (is_array( $stats ) && !empty( $stats )) {
                foreach ($stats as $paramName => $paramValue) {
                    $data[$paramName] =  $paramValue;
                }
            }
        } catch ( Exception $e ) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
        return $data;
    }

    /**
     * Check if a given statistical value (hash field, the parameter) exists in THIS LMS stats hash
     * 
     * @param $paramName
     * @return bool|int
     */
    public static function exists($paramName) {
        $object = self::getInstance();
        if ( !$object) return false;
        $response = $object->client->hexists($object->key, $paramName);
        return $response;
    }

    /**
     * Write a field in hash 
     *
     * @param $field
     * @param $value
     */
    public static function set( $field, $value) {
        try {
            $object = self::getInstance();
            if ( !$object ) return;
            $object->client->hset( $object->key, $field, $value);
        } catch ( Exception $e ) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Remove a field from hash
     * 
     * @param string $fields
     */
    public static function delField( $fields) {
        try {
            $object = self::getInstance();
            if ( !$object ) return;
            $object->client->hdel( $object->key, $fields);
        } catch ( Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Increment the int value of a field in a the hash 
     *
     * @param $field 
     * @param number $increment
     */
    public static function inc($field, $increment=1) {
        try {
            $object = self::getInstance();
            if ( !$object ) return;
            $object->client->hincrby($object->key, $field, $increment);
        }
        catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Decrement a field in the LMS stats hash
     *
     * @param $field name
     * @param number $decrement Value to decrement
     * @param string $allowNegativeResult  Allow negative values after decrementing ?
     */
    public static function dec( $field, $decrement=1, $allowNegativeResult = false) {
        try {
            $object = self::getInstance();
            $current = $object->client->hget($object->key, $field);
            $expected = $current - $decrement;
            if ( ($expected < 0) && !$allowNegativeResult) return;
            $object->client->hset($object->key, $field, $expected);
        }
        catch (Exception $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }


}