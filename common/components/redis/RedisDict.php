<?php
/**
 * Specific class to connect to REDIS server dedicated for LMS Settings
 *  
 */
class RedisDict extends RedisBase {
	
	const DICTIONARY_KEY = "dictionary";
	
	public function __construct() {
		parent::__construct(REDIS_CONFIG_HOST, REDIS_CONFIG_PORT, REDIS_CONFIG_DB);
	}
	
	
	/**
	 * Scan the SET key (must be a SET) for a given word and return true/false.
	 * Note, empty word (empty string) returns TRUE!
	 * 
	 * @param string $word
	 */
	public static function wordExists($word) {

		//@TODO 
		return false;
		
		
	}
	
	
	public static function addWords($words) {
		
		if (!is_array($words)) {
			$words = array($words);
		}
		
		$object = self::getInstance();
		$object->client->sadd(self::DICTIONARY_KEY, $words);
		
	}
	
	public static function importDictionary($filePath) {
		
		$allWords = file($filePath);
		
		self::addWords($allWords);
		

	}
	
	
	
}