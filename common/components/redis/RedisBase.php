<?php
/**
 * Base class for connecting any REDIS server & db
 * 
 * @property Predis\Client $client
 * 
 */
class RedisBase extends CComponent {

    /**
     * 
     * @var Predis\Client
     */
    private $_client;
    
    
    protected static $_instance;
    
    
    /**
     * @return RedisBase
     */
    protected static function getInstance($forceReload = false) {
    	if (self::$_instance && !$forceReload) {
    		return self::$_instance;
    	}
    	
    	$className = get_called_class();
    	
    	$object = new $className();
    
    	if (!$object->isConnected()) return null;
    
    	self::$_instance = $object;
    	return $object;
    }
    
    
    /**
     * Constructor 
     * 
     * @param string $host
     * @param string $port
     * @param string $database
     * 
     */
    public function __construct($host, $port, $database) {
    	
    	if (get_called_class() == __CLASS__) {
    		throw new Exception('You cannot create instances of RedisBase. Please extend the class!');
    	}
        
        require_once(realpath(__DIR__ . "/../../vendors") . "/Predis/Autoloader.php");
        Predis\Autoloader::register();

        $client = new Predis\Client(array(
            'host' 		=> $host,
            'port' 		=> $port,
            'database' 	=> $database,
        ));
        
        $this->client = $client;
        
        try {
            $client->connect();
        }
        catch (Predis\Connection\ConnectionException $e) {
            $message = "Failed to connect to REDIS server and database. Host: $host Port: $port Database#: $database";
            Yii::log($message, CLogger::LEVEL_ERROR);
            
            // Rethrow the exception so upper level can decide what to do
            throw new Exception($message);
        }
        
    }
    

    /**
     * 
     * @param Predis\Client $client
     */
    public function setClient($client) {
        $this->_client = $client;
    }
    
    /**
     * 
     * @return Predis\Client
     */
    public function getClient() {
        return $this->_client;
    }

    
    /**
     * @return boolean
     */
    public function isConnected() {
        return $this->client->isConnected();
    }
    
 
}