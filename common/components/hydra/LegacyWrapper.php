<?php

/**
 * LegacyWrapper
 *
 * A helper class for managing processes of loading the LMS
 * interface embedded into an iframe into Hydra FE application
 *
 * @package /common/components/hydra
 * @author  Plamen Boychev (boychev@scavaline.com)
 */
class LegacyWrapper extends CComponent
{
    /**
     * GET parameters that sould be cleared out if exist for the current request and a redirect should be done,
     * helps for building a clear url version of the current reqeust
     *
     * @var array $dirtyQueryParametes
     */
    public $dirtyQueryParametes = [
        'access_token'
    ];

    public $tileSize = 'square';

    /**
     * A flag for monitoring if the `core_setting`.`hydra_frontend` is turned on
     * At this point the setting should habe been already checked enabled state (`core_setting`.`hydra_frontend` === 'on')
     *
     * @var bool $hydraFrontendEnabled
     */
    private $isHydraFrontendEnabled = false;

    /**
     * Contains the passed GET 'access_token' parameter if passed
     *
     * @var string $accessToken
     */
    private $accessToken;

    /**
     * A prepared value for the current request's clean url (entry script + route name)
     *
     * @var string|null $requestCleanRoute
     */
    private $requestCleanRoute;

    /**
     * An instance of the HydraFrontendBridge class
     * It's a helper class for creating a bridge between the legacy interface and the Hydra FE application
     * With it we can check if there is an equivalent Hydra FE implementation of some page from the legacy application
     *
     * @var HydraFrontendBridge $frontendBridge
     */
    private $frontendBridge;

    /**
     * Triggered when the component gets initialized
     * Loads the flags used to load hydra themes
     *
     * @param  null
     *
     * @return LegacyWrapper
     */
    public function init()
    {
        $this->frontendBridge = new HydraFrontendBridge();
		$this->accessToken = Yii::app()->request->getQuery('access_token', null);

        // Load (one time only) the configuration setting for new hydra frontend
		// checking both for main domain or for multidomain
		$multidomainClient = CoreMultidomain::resolveClient();
		$idMultidomain = $multidomainClient->id ? $multidomainClient->id : 0;
		$loadedTheme = Yii::app()->db->createCommand()
			->select()->from("theme_variant")
			->where("id_multidomain = :id_multidomain", array(":id_multidomain" => $idMultidomain))
			->andWhere("active = 1")
			->queryRow();

		$themeSettings = CJSON::decode($loadedTheme['settings']);

		if(isset($themeSettings['tiles']['size'])){
		    $this->tileSize = $themeSettings['tiles']['size'];
        }


		$this->hydraFrontendEnabled($loadedTheme);
        return $this;
    }

	/**
	 * Initializes the session and logs the user in, if a valid oauth2 token is passed
	 * as GET param
	 */
	public function interceptRequest()
	{
		// Try to restore the user's session using a passed GET access_token parameter
		if (!is_null($this->accessToken)) {
			// Login the user if there is an access token passed
			Yii::app()->user->loginByAccessToken($this->accessToken);
		}
	}

    /**
     * Flag for whether or not the hydra frontend option is enabled for the installation
     *
     * @param  boolean $enabled
     *
     * @return boolean
     */
    public function hydraFrontendEnabled($enabled = null)
    {
        if ($enabled === null)
            return (boolean) $this->isHydraFrontendEnabled;

        $this->isHydraFrontendEnabled = (boolean) $enabled;
        return $enabled;
    }

	/**
	 * Enables the hydra frontend main theme
	 */
	public function enableHydraFrontend()
	{
		// Set the "internal" theme to active
		Yii::app()->db->createCommand()->update("theme_variant", array("id_multidomain" => 0, "active" => 1), "theme_code = 'internal'");
		$this->hydraFrontendEnabled(true);
	}

    /**
     * Guarding the embedded mode view via a Javascript guard
     *
     * @param $isLoginPage boolean Whether the guard is being rendered in legacy login pages (used to redirect to /learn)
     *
     * @return boolean
     */
    public function applyJsGuard($isLoginPage = false)
    {
    	$isNonWhitelabeledSuperadmin = Yii::app()->user->isGodadmin && (!PluginManager::isPluginActive("WhitelabelApp") || (Yii::app()->user->getId() == Settings::get('whitelabel_menu_userid', null)));
    	if($this->hydraFrontendEnabled() || $isNonWhitelabeledSuperadmin) {
			ob_start();
			require Yii::getPathOfAlias('lms.protected.views') . '/legacyWrapper/jsGuard/guard-definition.php';
			echo ob_get_clean();
		}
	}

    /**
     * Returns a boolean value for whether or not the current request has an equivalent available in Hydra FE
     *
     * @param  string $route = null
     *
     * @return boolean
     */
    public function checkRequestHasHydraEquivalent($route = null)
    {
        $route = $route ?: (isset($_GET['r']) ? $_GET['r'] : '');
        $this->frontendBridge->route($route);
        return $this->frontendBridge->hasPageEquivalent();
    }

    /**
     * Returns Hydra FE application equivalent for the passed legacy url if exists
     *
     * @param  string $route
     *
     * @return string
     */
    public function getHydraEquivalent($route)
    {
        if ($this->checkRequestHasHydraEquivalent($route) === false) {
            return;
        }

        return $this->frontendBridge->getPageEquivalent();
    }

    /**
     * Returns the route parameter of the url
     * Consists of:
     *  - If using clean urls - ([url] - [query params])
     *  - If not using clean urls - ([entry script] + [route name])
     *
     * @param  null
     *
     * @return string
     */
    public function getCleanRoute()
    {
        if ($this->requestCleanRoute !== null) {
            return $this->requestCleanRoute;
        }

        return $this->requestCleanRoute = Yii::app()->urlManager->cleanseUrl($this->dirtyQueryParametes);
    }

	/**
	 * Sets a flag into the current session to invalidate the hydra and trigger reboot at next page load
	 */
	public function invalidate() {
    	Yii::app()->session['hydra_invalidate'] = true;
	}

	/**
	 * Returns (and clears) the reboot status for hydra
	 *
	 * @return boolean Whether the hydra environment should be reboot to reflect core changes
	 */
	public function needsReboot() {
		if(isset(Yii::app()->session['hydra_invalidate'])) {
			unset(Yii::app()->session['hydra_invalidate']);
			return true;
		}

		return false;
	}

		/**
	 * Returns a JS snippet that posts a message to hydra FE to set the breadcrumbs
	 * @param $links The array of links in the breadcrumbs
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string the produced JS code
	 */
    public function setHydraBreadcrumbs($links, $wrapIntoScript = true, $title = null) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent)
			window.parent.postMessage({action: "legacy.setBreadcrumbItems",  message: {items: '.json_encode($links).', title: '.json_encode($title).'}}, \'*\');';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to set the current route
	 * @param $url The route to set
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function setHydraRoute($url, $wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent) {
			$("html").hide();
			window.parent.postMessage({action: "legacy.updateRoute", message:'.json_encode($url).'}, \'*\');
		}';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to set action buttons
	 * @param $buttons The array of action buttons to set
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function setHydraActionButtons($buttons, $wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent)
			window.parent.postMessage({action: "legacy.setActionButtons", message: {buttons: '.json_encode($buttons).'}}, \'*\');';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/**
	 * Returns the course player URL for Hydra FE
	 * @param $course LearningCourse The course for which the url is to be returned
	 */
	public function getHydraCourseUrl($course) {
		return $this->frontendBridge->getHydraCourseUrl($course);
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to reboot. Used when some critical option is changed
	 *
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function triggerReboot($wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent) {
			$("html").hide();
			window.parent.postMessage({action: "legacy.reboot"}, \'*\');
		}';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/** Returns a JS snippet that posts a message to hydra FE to reload notifications API
	 * @return string
	 */
	public function updateNotificationsCounter(){
		$output = 'if(window.parent) {
			window.parent.postMessage({action: "legacy.updateNotificationsCount"}, \'*\');
		}';

		return $output;
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to open the communication center.
	 *
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function openCommunicationCenter($wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent) {
			var hydraMessage = (typeof eventData == \'undefined\') ? {state: \'sales\'} : eventData;
			window.parent.postMessage({action: "legacy.openCommunicationCenter",  message: hydraMessage}, \'*\');
		}';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to display the stripe overlay.
	 *
	 * @param bool $show Whether the overlay should be displayed or not
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function toggleStripeOverlay($show, $wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent) {
			window.parent.postMessage({action: "legacy.toggleStripeOverlay",  message: {show: '.json_encode($show).'}}, \'*\');
		}';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}

	/**
	 * Returns a JS snippet that posts a message to hydra FE to reload billing info
	 *
	 * @param bool $wrapIntoScript Whether the JS code should be wrapped into a <script> tag
	 * @return string  the produced JS code
	 */
	public function reloadBillingInfo($wrapIntoScript = true) {
		$output = $wrapIntoScript ?  '<script>' : '';
		$output .= 'if(window.parent) {
			window.parent.postMessage({action: "legacy.reloadBillingInfo",  message: {}}, \'*\');
		}';
		if($wrapIntoScript) $output .= '</script>';

		return $output;
	}
}
