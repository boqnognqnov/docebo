<?php

/**
 * Docebo Spa
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */

class HydraRBAC
{
    private static $item_table = 'rbac_item';

    private static $item_child_table = 'rbac_item_child';

    private static $item_assignments_table = 'rbac_assignment';

    private static $role_prefix = '/framework/adminrules/';

    public static function syncPermissionsTable($post, $oldRoleName = false) {

        if(isset($post['CoreGroup']) && $post['CoreRoleMembers']) {

            $newRoleName = self::buildRoleName($_POST['CoreGroup']['groupid']);
            $newPermissions = $_POST['CoreRoleMembers'];
            // We should Create new Role
            if ($oldRoleName === false) {
                // If the Role does not exists in the new RBAC ..... then create it
                if (!self::roleExists($newRoleName)) {
                    self::createRole($newRoleName);
                }
            } else {
                if ($newRoleName !== $oldRoleName) {
                    self::updateRole($oldRoleName,$newRoleName);
                }
            }

            // Get all Permissions for the Given Role
            $existingRolePermission = self::getRolePermissions($newRoleName);
            $mappedPermissions = self::mapOldToNewPermissions($newPermissions,$post);
            $permissionsToAssign = array();
            $toDelete = array();

            foreach ( $mappedPermissions as $name => $assign ) {
                if ( $assign === true ) {
                    // If the Permission is FOR assign .... and it was NOT assigned before
                    if ( !in_array($name, $existingRolePermission) ) {
                        $permissionsToAssign[] = $name;
                    }
                } else {
                    // If the Permission is NOT for Assignment but was Assigned before .... we should DELETE it
                    if( in_array($name, $existingRolePermission) ) {
                        $toDelete[] = $name;
                    }
                }
            }

            if($permissionsToAssign !== array()) {
                self::assignPermissionsToRole($newRoleName, $permissionsToAssign);
            }
            if($toDelete !== array()) {
                self::removePermissionsFromRole($newRoleName, $toDelete);
            }
        }

    }



    private static function buildRoleName($name) {
        return self::$role_prefix . $name;
    }

    /*
     * Create new RBAC Role
     * @param $name role name
     */
    public static function createRole($name) {
        $time = time();
        Yii::app()->db->createCommand()->insert(
            self::$item_table,
            array(
                'name' => $name,
                'type' => 1, // This is the Role ... so the type should be 1
                'created_at' => $time,
                'updated_at' => $time
            )
        );
        // Every Custom Role should inherit everything from the Power User Role
        Yii::app()->db->createCommand()->insert(
            self::$item_child_table,
            array(
                "parent" => $name,
                "child" => Yii::app()->user->level_admin
            )
        );
    }

    /*
     * Update Certain RBAC Role with new name
     * @param $oldName
     * @param $newName
     */
    public static function updateRole($oldName, $newName) {
        Yii::app()->db->createCommand()
            ->update(
                self::$item_table,
                array('name' => $newName),
                "name = :oldName",
                array(":oldName" => $oldName)
            );
    }

    /*
     * Check if given Role already exists in the New RBAC
     * @param $name Certain Role name
     * @return boolean
     */
    private static function roleExists($name) {
        $result = Yii::app()->db->createCommand()
            ->from(self::$item_table)
            ->where('name = :role', array(':role' => $name))
            ->queryRow();
        return $result !== false;
    }

    /*
     * Extract all Permissions for the given Role
     * @param $name Role name
     */
    private static function getRolePermissions($name) {
        return Yii::app()->db->createCommand()
            ->select("child")
            ->from(self::$item_child_table)
            ->where("parent = :parent",array(":parent" => $name))
            ->queryColumn();
    }

    /*
     * Map all OLD permissions id's to new Permission names
     * From: old_idst => 0|1
     * To: new_name => true|false
     * @param array $permissions
     */
    private static function mapOldToNewPermissions($permissions,$post) {
        // Extract the Old Role names by their IDs
        $oldPermissionNames = Yii::app()->db->createCommand()
            ->select(array("idst","roleid"))
            ->from(CoreRole::model()->tableName())
            ->where(array('in', 'idst', array_keys($permissions)))
            ->queryAll(true);

        $formattedPerms = array();
        foreach( $oldPermissionNames as $perm ) {
            $permAllowed = boolval($permissions[$perm['idst']]);
            $permNewName = HydraRBACPermissionMapper::$permission_map[$perm['roleid']];
            $formattedPerms[$permNewName] = $permAllowed;
        }


        $formattedPerms=self::appendSubPrefixPermissions($formattedPerms,$post);
        
        return $formattedPerms;
    }

    //THIS FUNCTION IS ONLY FOR SUB_ PREFIX RULES
    private static function appendSubPrefixPermissions($formattedPerms, $post)
    {
        $additionalPerm = $post['permissions'];

        foreach ($additionalPerm as $permissionItem) {
            foreach ($permissionItem as $ruleName => $ruleValue) {
                if ($ruleValue == 1) {
                    $formattedPerms[$ruleName] = true;
                } else {
                    $formattedPerms[$ruleName] = false;
                }

            }
        }
        
        //THIS BLOCK ADD GENERAL PERMISSIONS (CoreSettingGroup) TO HYDRA RBAC TABLE
        if (isset($post['CoreSettingGroup']['admin_rules.manage_seat_for_subscription']['value'])) {
            if ($post['CoreSettingGroup']['admin_rules.manage_seat_for_subscription']['value'] == 1) {
                $formattedPerms['general/manage_seat_for_subscription'] = true;
            }else{
                $formattedPerms['general/manage_seat_for_subscription'] = false;
            }
        }

        return $formattedPerms;
    }

    /*
     * Assign new Permissions to given Role
     * @param string $role the Name of the Role that will receive the new permissions
     * @param array $permissions List of permissions to be applied to given Role
     */
    public static function assignPermissionsToRole($role, $permissions) {
        $insert = array();
        foreach( $permissions as $name ) {
            $insert[] = array('parent' => $role, 'child' => $name );
        }

        // Execute the Query
        Yii::app()->db->getCommandBuilder()
            ->createMultipleInsertCommand(self::$item_child_table, $insert)->execute();
    }

    /*
     * Remove Permissions from given Role
     * @param string $role the Name of the Role from which we will remove permissions
     * @param array $permissions List of permissions to be removed from given Role
     */
    private static function removePermissionsFromRole($role, $permissions) {
        $deleteCondition = array('OR');
        $params = array(":role" => $role);
        foreach( $permissions as $key => $name ) {
            $childKey = ":{$key}";
            $deleteCondition[] = array( "AND", "parent = :role", "child = {$childKey}" );
            $params[$childKey] = $name;
        }

        // Delete All Permission from Certain Role
        Yii::app()->db->createCommand()->delete( self::$item_child_table, $deleteCondition, $params );
    }

    /*
     * Delete given role from the rbac_item table
     * @param string $name Name of the given Role
     * @return boolean if the Deletion was successful
     */
    public static function deleteRole($role_idst) {
        // Extract the Role name using the CoreGroup idst
        $roleName = Yii::app()->db->createCommand()
            ->select('groupid')
            ->from(CoreGroup::model()->tableName())
            ->where('idst = :id', array(":id" => $role_idst))
            ->queryScalar();

        // Delete the Role from the Hydra RBAC table
        $res = Yii::app()->db->createCommand()->delete(
            self::$item_table,
            "name = :roleName",
            array(":roleName" => $roleName)
        );

        return boolval($res);
    }

    /*
     * Assign | Unassign user (to | from) Role
     * @params array $post the $_POST request
     */
    public static function manageUserToRole($data, $managePUProfile = false) {
        $user = $data["user"];
        $group = $data["group"];
        if (!empty($user)) {

            $where = array("AND", "user_id = :user");
            $params = array(":user" => $user);
            if( $managePUProfile === true ) {
                $where[] = "item_name NOT LIKE :role";
                $params[":role"] = Yii::app()->user->level_admin;
            }

            // We should always clear the OLD role that the user have
            Yii::app()->db->createCommand()->delete(self::$item_assignments_table, $where, $params);

            // The profile is not empty ... let's assign the Role to the user
            // But first we must get the profile name by its idst
            if( !empty($group) ) {

                $roleName = Yii::app()->db->createCommand()
                    ->select("groupid")
                    ->from(CoreGroup::model()->tableName())
                    ->where("idst = :id", array(":id" => $group))
                    ->queryScalar();

                Yii::app()->db->createCommand()->insert(
                    self::$item_assignments_table,
                    array(
                        'item_name' => $roleName,
                        'user_id' => $user,
                        'created_at'=> time()
                    )
                );
            }
        }
    }

    /*
     * Create New permission to Hydra RBAC
     * @param string $name Name of the new Permission
     */
    public static function createPermission($name, $description = false, $rule_name = false) {
        $time = time();
        $insert = array(
            'name' => $name,
            'type' => 2,
            'created_at' => $time,
            'updated_at' => $time
        );

        if( $description !== false ) {
            $insert['description'] = $description;
        }

        if( $rule_name !== false ) {
            $insert['rule_name'] = $rule_name;
        }

        Yii::app()->db->createCommand()->insert(self::$item_table, $insert );
    }
}