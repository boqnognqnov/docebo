<?php

/**
 *
 */
class HydraFrontendBridge
{

    /**
     * @var string $methodPrefix
     */
    private $methodPrefix = 'bridge';

    /**
     * @var array $processReference
     */
    private $processReference = null;

    /**
     * @var array $processEquivalentUrl
     */
    private $processEquivalentUrl = null;

    /**
     * @var string $route - if null - works with the current route, if set - works with the passed route value
     */
    private $route = null;

    /**
     * @var CController $controller
     */
    private $controller = null;

    /**
     * @var CAction $action
     */
    private $action = null;

    /**
     * Getter / setter method for @var $route
     *
     * @param  $route = null
     *
     * @return string|null|HydraFrontendBridge
     */
    public function route($route = null)
    {
        if ($route === null) {
            return $this->route;
        }

        $this->route = $route;
        $this->buildControllerActionPair();

        return $this;
    }

    private function buildControllerActionPair()
    {
        if ($this->route === null) {
            $this->controller = Yii::app()->controller;
            $this->action = Yii::app()->controller->action;
        } else {
            $controller = Yii::app()->createController($this->route);
            $actionName = 'index';

            if (is_array($controller) === true) {
                $actionName = array_pop($controller);
                $controller = array_shift($controller);
            }

            $this->controller = $controller;
            $this->action = $this->controller->createAction($actionName);
        }

        return $this;
    }

    /**
     * Fetches the current controller instance or creates one for the injected @var $route
     *
     * @param  null
     *
     * @return CController
     */
    private function getController()
    {
        if ($this->controller !== null)
        {
            return $this->controller;
        }

        $this->buildControllerActionPair();

        return $this->controller;
    }

    /**
     * Fetches the current action instance or creates one for the injected @var $route
     *
     * @param  null
     *
     * @return CAction
     */
    private function getAction()
    {
        if ($this->action !== null)
        {
            return $this->action;
        }

        $this->buildControllerActionPair();

        return $this->action;
    }

    /**
     * The process reference is a method name of the current class dedicated to
     * making the decision for whether or not the legacy page should redirect to
     * a Hydra Frontend application's 'native' page's url
     *
     * The convention for building method names is as follows:
     *  - entry script directory
     *  - controller name
     *  - action name
     *
     * @example for url "/lms/index.php?r=site/index"                     the method's name would be "bridgeLmsSiteIndex"
     * @example for url "/test/sub-2/index.php?r=site/index"              the method's name would be "bridgeTestSub2SiteIndex"
     * @example for url "/test/sub-2/index.php?r=site_Class/method_Name"  the method's name would be "bridgeTestSub2SiteClassMethodName"
     *
     * @param  null
     *
     * @return boolean
     */
    private function getProcessReference()
    {
        if ($this->processReference !== null) {
            return $this->processReference;
        }

        $scriptDirectory    = $this->studlyCase($this->getScriptSubdirectory());
        $controller         = $this->getController();
        $moduleName			= $this->getController()->module ? $this->studlyCase($this->getController()->module->name) : '';
        $controllerName     = $this->studlyCase($this->getController()->id);
        $actionName         = $this->studlyCase($this->getAction()->id);

        $methodName = compact(
            'scriptDirectory',
			'moduleName',
            'controllerName',
            'actionName'
        );
        $methodName = array_map(function($piece) { return ucfirst($piece); }, $methodName);
        $methodName = $this->methodPrefix . implode('', $methodName);
        $hasMethod  = method_exists($this, $methodName);
        $isCallable = false;
        $notStatic  = false;

        if ($hasMethod === true) {
            $reflection = new ReflectionMethod($this, $methodName);
            $isCallable = $reflection->isPublic();
            $notStatic  = ! $reflection->isStatic();
        }

        return $this->processReference =
            $isCallable === true && $notStatic === true
                ? $methodName
                : null
        ;
    }

    /**
     * Returns the used entry script's subdirectory
     *
     * @param  null
     *
     * @return string
     */
    private function getScriptSubdirectory()
    {
        $pieces = explode('/', $_SERVER['SCRIPT_NAME']);
        $pieces = array_slice($pieces, 0, -1);

        return implode('', $pieces);
    }

    /**
     * Converts the passed string to StudlyCase
     *
     * @param  string
     *
     * @return string
     */
    private function studlyCase($string)
    {
        $pieces = preg_split("/[\_\-\.\/]+/", $string);
        $pieces = array_map(function($val) { return ucfirst($val); }, $pieces);
        $string = implode('', $pieces);
        return preg_replace('/[^a-z0-9]+/i', '', $string);
    }

    /**
     * Returns a flag indicating whether or not there is an available Hydra Frontend application page equivalent
     *
     * @uses   HydraFrontendBridge::getPageEquivalent()
     *
     * @param  null
     *
     * @return boolean
     */
    public function hasPageEquivalent()
    {
        return (boolean) $this->getPageEquivalent();
    }

    /**
     * Returns the process' availability of Hydra Frontend application page
     * After the first call to the method the result is cached
     */
    public function getPageEquivalent()
    {
        // Return the cached value if equivalent page availability is already determined
        if ($this->processEquivalentUrl !== null) {
            return $this->processEquivalentUrl;
        }

        $processReference = $this->getProcessReference();

        // If there is no process reference cache the unavailability for further calls
        if (((boolean) $processReference) === false) {
            return $this->processEquivalentUrl = false;
        }

        // Cache the availability for further calls
        return $this->processEquivalentUrl = $this->{$processReference}();
    }


    /**
     * Player page for E-Learning objects
     *
     * @return string
     */
    public function bridgeLmsPlayerTrainingIndex()
    {
        $cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
        $isAdminView = Yii::app()->request->getParam('ref_src')=='admin';

        if($isAdminView) return false;

        $courseId    = $cleanParams['course_id'];

        $course = LearningCourse::model()->findByAttributes([
            'idCourse'  => $courseId,
        ]);

        return $this->getHydraCourseUrl($course);
    }

    /**
     * Player page with deeplink parameters
     *
     * @return string
     */
    public function bridgeLmsCourseDeeplink()
    {
        $cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
        $courseId    = $cleanParams['course_id'];
        unset($cleanParams['course_id']);

        $course = LearningCourse::model()->findByAttributes(['idCourse'  => $courseId]);
        if ($course)
            return $this->getHydraCourseUrl($course) . urldecode('?' . http_build_query($cleanParams));

        return false;
    }

    /**
     * Player page for learning plan with deeplink parameters
     *
     * @return string
     */
    public function bridgeLmsCoursepathDeeplink()
    {
        $cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
        $planId    = $cleanParams['id_path'];
        unset($cleanParams['id_path']);

        $plan = LearningCoursepath::model()->findByAttributes(['id_path'  => $planId]);
        if ($plan)
            return $this->getHydraLearningPlanUrl($plan)  . urldecode('?' . http_build_query($cleanParams));

        return false;
    }


	/**
	 * Player page for Webinar objects
	 *
	 * @return string
	 */
    public function bridgeLmsPlayerTrainingWebinarSession() {
		$session = null;
		$cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
		$courseId    = $cleanParams['course_id'];
		$sessionId    = $cleanParams['session_id'];
		if($sessionId)
			$session = WebinarSession::model()->findByAttributes(['id_session' => $sessionId]);

		if($session)
			return $this->getHydraSessionUrl($session);
		else
			return false;
	}


	/**
	 * Player page for ILT Classroom objects
	 *
	 * @return string
	 */
	public function bridgeLmsPlayerTrainingSession() {
		$session = null;
		$cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
		$courseId    = $cleanParams['course_id'];
		$sessionId    = $cleanParams['session_id'];
		if($sessionId)
			$session = LtCourseSession::model()->findByAttributes(['id_session' => $sessionId]);

		if($session)
			return $this->getHydraSessionUrl($session);
		else
			return false;
	}


	/**
	 * My courses
	 *
	 * @return string
	 */
	public function bridgeLmsSiteIndex()
	{
		$optQueryParam = Yii::app()->request->getParam('opt');
		switch($optQueryParam) {
			case 'fullmycourses':
			case 'mycourses':
				return '/learn/mycourses';
				break;
			case 'catalog':
				return '/learn/catalog';
				break;
			default:
				return false;
		}
	}

	/**
	 * Channels dashboard
	 *
	 * @return string
	 */
	public function bridgeLmsChannelsIndexIndex()
	{
		return '/learn/dashboard';
	}

	/**
	 * Themes management
	 *
	 * @return string
	 */
	public function bridgeAdminThemesIndex()
	{
		return '/manage/themes';
	}

	/**
	 * Marketplace new hydra page
	 *
	 * @return string
	 */
	public function bridgeAdminMarketplaceIndex()
	{
		return '/marketplace';
	}

	/**
	 * Users management
	 *
	 * @return string
	 */
	public function bridgeAdminUserManagementIndex()
	{
		return '/manage/users';
	}

	/**
	 * Learning plan page
	 *
	 * @return string
	 */
	public function bridgeLmsCurriculaShowIndex()
	{
		$cleanParams = Yii::app()->urlManager->getCleanQueryParams(['r']);
		$planId    = $cleanParams['id_path'];
		$plan = null;
		if($planId)
			$plan = LearningCoursepath::model()->findByAttributes(['id_path'  => $planId]);

		if($plan)
			return $this->getHydraLearningPlanUrl($plan);
		else
			return '/learn/mycourses;show_only=learning_plan';
	}

	/**
	 * Returns the session player URL for Hydra FE
	 * @param $session LtCourseSession | WebinarSession The session (ILT or webinar) for which the url is to be returned
	 */
	public function getHydraSessionUrl($session) {
		return '/learn/course/' . $session->course_id . '/session/' . $session->id_session . '/' . $this->slug($session->name);
	}

	/**
	 * Returns the course player URL for Hydra FE
	 * @param $course LearningCourse The course for which the url is to be returned
	 */
    public function getHydraCourseUrl($course) {
		return '/learn/course/' . $course->idCourse . '/' . (trim($course->slug_name) ? $course->slug_name : $this->slug($course->name));
	}

    /**
     * Returns course view url for Hydra FE
     * @param $course LearningCourse The course for which the url is to be returned
     * @return string
     */
    public function getHydraCourseViewUrl($course) {
        return '/learn/course/view/' . $course->course_type . '/' . $course->idCourse . '/' . (trim($course->slug_name) ? $course->slug_name : $this->slug($course->name));
    }

	/**
	 * Returns the LP player URL for Hydra FE
	 * @param $plan LearningCoursepath The LP for which the url is to be returned
	 */
	public function getHydraLearningPlanUrl($plan) {
		return '/learn/lp/' . $plan->id_path . '/' . $this->slug($plan->path_name);
	}

	/**
	 * Returns the LP view URL for Hydra FE
	 * @param $plan LearningCoursepath The LP for which the url is to be returned
     * @return string
	 */
	public function getHydraLearningPlanViewUrl($plan) {
		return '/learn/learning_plan/view/' . $plan->id_path . '/' . $this->slug($plan->path_name);
	}

	/**
	 * Yii2 slug function partially ported to Yii1
	 * @param $string
	 * @param string $replacement
	 * @param bool $lowercase
	 * @return string
	 */
	public function slug($string, $replacement = '-', $lowercase = true)
	{
		$string = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $string);
		$string = preg_replace('/[=\s—–-]+/u', $replacement, $string);
		$string = trim($string, $replacement);

		return $lowercase ? strtolower($string) : $string;
	}
}
