<?php

/**
 * Docebo Spa
 *
 * @link https://www.docebo.com/
 * @copyright Copyright (c) 2016 Docebo Spa
 */
class HydraRBACPermissionMapper
{
    // Role based permissions
    CONST SUPER_ADMIN                   = '/framework/level/godadmin';
    CONST POWER_USER                    = '/framework/level/admin';
    CONST USER                          = '/framework/level/user';

    // OLD Permission Names
    CONST OLD_DASHBOARD_VIEW            = '/framework/admin/dashboard/view';

    CONST OLD_GROUP_CREATE              = '/framework/admin/groupmanagement/add';
    CONST OLD_GROUP_UPDATE              = '/framework/admin/groupmanagement/mod';
    CONST OLD_GROUP_DELETE              = '/framework/admin/groupmanagement/del';
    CONST OLD_GROUP_VIEW                = '/framework/admin/groupmanagement/view';

    CONST OLD_NEWSLETTER_VIEW           = '/framework/admin/newsletter/view';

    CONST OLD_USER_CREATE               = '/framework/admin/usermanagement/add';
    CONST OLD_USER_UPDATE               = '/framework/admin/usermanagement/mod';
    CONST OLD_USER_DELETE               = '/framework/admin/usermanagement/del';
    CONST OLD_USER_VIEW                 = '/framework/admin/usermanagement/view';
    CONST OLD_USER_APPROVE_WAITING      = '/framework/admin/usermanagement/approve_waiting_user';

    CONST OLD_CERTIFICATE_UPDATE        = '/lms/admin/certificate/mod';
    CONST OLD_CERTIFICATE_VIEW          = '/lms/admin/certificate/view';

    CONST OLD_COURSE_CREATE             = '/lms/admin/course/add';
    CONST OLD_COURSE_UPDATE             = '/lms/admin/course/mod';
    CONST OLD_COURSE_DELETE             = '/lms/admin/course/del';
    CONST OLD_COURSE_VIEW               = '/lms/admin/course/view';
    CONST OLD_COURSE_MANAGE_WAITING     = '/lms/admin/course/moderate';
    CONST OLD_COURSE_ENROLL             = '/lms/admin/course/subscribe';
    CONST OLD_COURSE_EVALUATE           = '/lms/admin/course/evaluation';

    CONST OLD_LEARNING_PLAN_UPDATE      = '/lms/admin/coursepath/mod';
    CONST OLD_LEARNING_PLAN_VIEW        = '/lms/admin/coursepath/view';

    CONST OLD_REPORT_UPDATE             = '/lms/admin/report/mod';
    CONST OLD_REPORT_VIEW               = '/lms/admin/report/view';

    CONST OLD_LOCATION_UPDATE           = '/lms/admin/location/mod';
    CONST OLD_LOCATION_VIEW             = '/lms/admin/location/view';

    CONST OLD_TRANSCRIPTS_UPDATE        = '/framework/admin/transcripts/mod';
    CONST OLD_TRANSCRIPTS_VIEW          = '/framework/admin/transcripts/view';

    CONST OLD_GAMIFICATION_VIEW         = '/framework/admin/gamificationapp/view';

    CONST OLD_CLASSROOM_SESSION_CREATE  = '/lms/admin/classroomsessions/add';
    CONST OLD_CLASSROOM_SESSION_UPDATE  = '/lms/admin/classroomsessions/mod';
    CONST OLD_CLASSROOM_SESSION_VIEW    = '/lms/admin/classroomsessions/view';
    CONST OLD_CLASSROOM_SESSION_DELETE  = '/lms/admin/classroomsessions/del';
    CONST OLD_CLASSROOM_SESSION_ASSIGN  = '/lms/admin/classroomsessions/assign';

    CONST OLD_WEBINAR_SESSION_CREATE    = '/lms/admin/webinarsessions/add';
    CONST OLD_WEBINAR_SESSION_UPDATE    = '/lms/admin/webinarsessions/mod';
    CONST OLD_WEBINAR_SESSION_VIEW      = '/lms/admin/webinarsessions/view';
    CONST OLD_WEBINAR_SESSION_DELETE    = '/lms/admin/webinarsessions/del';
    CONST OLD_WEBINAR_SESSION_ASSIGN    = '/lms/admin/webinarsessions/assign';

    CONST OLD_CATALOG_CREATE            = '/framework/admin/catalogManagement/add';
    CONST OLD_CATALOG_UPDATE            = '/framework/admin/catalogManagement/mod';
    CONST OLD_CATALOG_DELETE            = '/framework/admin/catalogManagement/del';
    CONST OLD_CATALOG_VIEW              = '/framework/admin/catalogManagement/view';

    // New PERMISSION NAMES
    CONST NEW_DASHBOARD_VIEW            = 'dashboard/view';

    CONST NEW_GROUP_CREATE              = 'group/create';
    CONST NEW_GROUP_UPDATE              = 'group/update';
    CONST NEW_GROUP_DELETE              = 'group/delete';
    CONST NEW_GROUP_VIEW                = 'group/view';

    CONST NEW_NEWSLETTER_VIEW           = 'newsletter/view';

    CONST NEW_USER_CREATE               = 'user/create';
    CONST NEW_USER_UPDATE               = 'user/update';
    CONST NEW_USER_DELETE               = 'user/delete';
    CONST NEW_USER_VIEW                 = 'user/view';
    CONST NEW_USER_APPROVE_WAITING      = 'user/approve-waiting-user';

    CONST NEW_CERTIFICATE_UPDATE        = 'certificate/update';
    CONST NEW_CERTIFICATE_VIEW          = 'certificate/view';

    CONST NEW_COURSE_CREATE             = 'course/create';
    CONST NEW_COURSE_UPDATE             = 'course/update';
    CONST NEW_COURSE_DELETE             = 'course/delete';
    CONST NEW_COURSE_VIEW               = 'course/view';
    CONST NEW_COURSE_MANAGE_WAITING     = 'enrollment/manage-waiting';
    CONST NEW_COURSE_ENROLL             = 'enrollment/create';
    CONST NEW_COURSE_EVALUATE           = 'enrollment/evaluate';

    CONST NEW_LEARNING_PLAN_UPDATE      = 'learning-plan/update';
    CONST NEW_LEARNING_PLAN_VIEW        = 'learning-plan/view';

    CONST NEW_REPORT_UPDATE             = 'report/update';
    CONST NEW_REPORT_VIEW               = 'report/view';

    CONST NEW_LOCATION_UPDATE           = 'location/update';
    CONST NEW_LOCATION_VIEW             = 'location/view';

    CONST NEW_TRANSCRIPTS_UPDATE        = 'transcripts/update';
    CONST NEW_TRANSCRIPTS_VIEW          = 'transcripts/view';

    CONST NEW_GAMIFICATION_VIEW         = 'gamification/view';

    CONST NEW_CLASSROOM_SESSION_CREATE  = 'classroom-sessions/create';
    CONST NEW_CLASSROOM_SESSION_UPDATE  = 'classroom-sessions/update';
    CONST NEW_CLASSROOM_SESSION_VIEW    = 'classroom-sessions/view';
    CONST NEW_CLASSROOM_SESSION_DELETE  = 'classroom-sessions/delete';
    CONST NEW_CLASSROOM_SESSION_ASSIGN  = 'classroom-sessions/assign';

    CONST NEW_WEBINAR_SESSION_CREATE    = 'webinar-sessions/create';
    CONST NEW_WEBINAR_SESSION_UPDATE    = 'webinar-sessions/update';
    CONST NEW_WEBINAR_SESSION_VIEW      = 'webinar-sessions/view';
    CONST NEW_WEBINAR_SESSION_DELETE    = 'webinar-sessions/delete';
    CONST NEW_WEBINAR_SESSION_ASSIGN    = 'webinar-sessions/assign';

    CONST NEW_CATALOG_CREATE            = 'catalog/create';
    CONST NEW_CATALOG_UPDATE            = 'catalog/update';
    CONST NEW_CATALOG_DELETE            = 'catalog/delete';
    CONST NEW_CATALOG_VIEW              = 'catalog/view';

    // Map OLD => NEW permission name
    public static $permission_map = array(
        self::OLD_DASHBOARD_VIEW            => self::NEW_DASHBOARD_VIEW,
        self::OLD_GROUP_CREATE              => self::NEW_GROUP_CREATE,
        self::OLD_GROUP_UPDATE              => self::NEW_GROUP_UPDATE,
        self::OLD_GROUP_DELETE              => self::NEW_GROUP_DELETE,
        self::OLD_GROUP_VIEW                => self::NEW_GROUP_VIEW,

        self::OLD_NEWSLETTER_VIEW           => self::NEW_NEWSLETTER_VIEW,

        self::OLD_USER_CREATE               => self::NEW_USER_CREATE,
        self::OLD_USER_UPDATE               => self::NEW_USER_UPDATE,
        self::OLD_USER_DELETE               => self::NEW_USER_DELETE,
        self::OLD_USER_VIEW                 => self::NEW_USER_VIEW,
        self::OLD_USER_APPROVE_WAITING      => self::NEW_USER_APPROVE_WAITING,

        self::OLD_CERTIFICATE_UPDATE        => self::NEW_CERTIFICATE_UPDATE,
        self::OLD_CERTIFICATE_VIEW          => self::NEW_CERTIFICATE_VIEW,

        self::OLD_COURSE_CREATE             => self::NEW_COURSE_CREATE,
        self::OLD_COURSE_UPDATE             => self::NEW_COURSE_UPDATE,
        self::OLD_COURSE_DELETE             => self::NEW_COURSE_DELETE,
        self::OLD_COURSE_VIEW               => self::NEW_COURSE_VIEW,
        self::OLD_COURSE_MANAGE_WAITING     => self::NEW_COURSE_MANAGE_WAITING,
        self::OLD_COURSE_ENROLL             => self::NEW_COURSE_ENROLL,
        self::OLD_COURSE_EVALUATE           => self::NEW_COURSE_EVALUATE,

        self::OLD_LEARNING_PLAN_UPDATE      => self::NEW_LEARNING_PLAN_UPDATE,
        self::OLD_LEARNING_PLAN_VIEW        => self::NEW_LEARNING_PLAN_VIEW,

        self::OLD_REPORT_UPDATE             => self::NEW_REPORT_UPDATE,
        self::OLD_REPORT_VIEW               => self::NEW_REPORT_VIEW,

        self::OLD_LOCATION_UPDATE           => self::NEW_LOCATION_UPDATE,
        self::OLD_LOCATION_VIEW             => self::NEW_LOCATION_VIEW,

        self::OLD_TRANSCRIPTS_UPDATE        => self::NEW_TRANSCRIPTS_UPDATE,
        self::OLD_TRANSCRIPTS_VIEW          => self::NEW_TRANSCRIPTS_VIEW,

        self::OLD_GAMIFICATION_VIEW         => self::NEW_GAMIFICATION_VIEW,

        self::OLD_CLASSROOM_SESSION_CREATE  => self::NEW_CLASSROOM_SESSION_CREATE,
        self::OLD_CLASSROOM_SESSION_UPDATE  => self::NEW_CLASSROOM_SESSION_UPDATE,
        self::OLD_CLASSROOM_SESSION_VIEW    => self::NEW_CLASSROOM_SESSION_VIEW,
        self::OLD_CLASSROOM_SESSION_DELETE  => self::NEW_CLASSROOM_SESSION_DELETE,
        self::OLD_CLASSROOM_SESSION_ASSIGN  => self::NEW_CLASSROOM_SESSION_ASSIGN,

        self::OLD_WEBINAR_SESSION_CREATE    => self::NEW_WEBINAR_SESSION_CREATE,
        self::OLD_WEBINAR_SESSION_UPDATE    => self::NEW_WEBINAR_SESSION_UPDATE,
        self::OLD_WEBINAR_SESSION_VIEW      => self::NEW_WEBINAR_SESSION_VIEW,
        self::OLD_WEBINAR_SESSION_DELETE    => self::NEW_WEBINAR_SESSION_DELETE,
        self::OLD_WEBINAR_SESSION_ASSIGN    => self::NEW_WEBINAR_SESSION_ASSIGN,

        self::OLD_CATALOG_CREATE            => self::NEW_CATALOG_CREATE,
        self::OLD_CATALOG_UPDATE            => self::NEW_CATALOG_UPDATE,
        self::OLD_CATALOG_DELETE            => self::NEW_CATALOG_DELETE,
        self::OLD_CATALOG_VIEW              => self::NEW_CATALOG_VIEW
    );

}