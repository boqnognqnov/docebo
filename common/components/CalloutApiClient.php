<?php
/**
 * API Client for ERP API controller (written for Callouts, but could be easily made universal).
 * Provides full scale error handling/reporting (exceptions based) 
 * 
 * 
 * == Requires: EHttpClient Yii extension
 * 
 * == Parameters are sent to API contrller as POST fields (form)
 *  
 * == Response from controller must be JSON in the response body (payload).
 * 
 * == This client returns an ARRAY, NOT a JSON 
 * 
 * == Authentication is done using X-Authorization header, hashed parameters, etc. @see getXAuthHeader 
 * 
 * == All methods are throwable, use them in try/catch blocks:
 * 
 *    try {
 *        // Example 1, params in instance creation call only
 *        $client = CalloutApiClient::getInstance('someController', array('param1' => 'value1'));
 *        $result = $client->call('someAction');
 *
 *        // Example2, params will be merged and sent to API controller
 *        $client = new CalloutApiClient('someController', array('param1' => 'value1'));
 *        $result = $client->call('someAction', array('param2' => 'value2'));
 *
 *        // Example 3, params set in call() only 
 *        $client = new CalloutApiClient('someController');
 *        $result = $client->call('someAction', array('param1' => 'value1'));
 *        
 *        // Example 4, one line call, no parameters at all
 *        $result = CalloutApiClient::getInstance('someController')->call('someAction');
 *        
 *        // Example 5, create with param1 and then add different set of params for all calls
 *        $client = new CalloutApiClient('someController', array('param1' => 'value1'));   
 *        $result = $client->call('someAction', array('param2' => 'value2'));  // param1 and 2 sent
 *        $result = $client->call('someAction', array('param3' => 'value3'));  // param1 and 3 sent
 *        
 *    }
 *    catch (Exception $e) {
 *        echo $e->getMessage();
 *    }
 *    
 *    
 * == API controller is expected to return an array (JSON-ed) of type
 * array(
 *     'success' =>  true/false
 *     'errorcode' => arbitrary number
 *     'message' => arbitary string 
 *     'data' => array()   // actual data returned by controller must go here
 * )
 * 
 * == Additionally, API controller may return HTTP Error responses (>= 400) and
 * still provide more information in the same response array, In this 
 * case errorcode/message from the array will be used as code/message,
 * in the exception, thrown by this client.   
 *    
 *    
 * == LMS Installation ID is internally added to parameters array, but is
 * overriden by params['installation_id'], if present. That is, do this
 * if you want to override internally acquired LMS ID:
 * 
 *     $params["installtion_id"] = '<some ID>'; 
 *    
 *    
 *    
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 */
class CalloutApiClient extends CComponent {
    
    const API_REALM_NAME = "DoceboErp";
    
    
    // Keeps a class instance; provides singletong use pattern 
    static $instance = null;
    
    // Used for debug
    public $proxyHost = false;
    public $proxyPort = false;
    
    // Holding EHttpClient instance
    protected $_client = null;
    
    // Holding ERP API URL
    protected $_apiBaseUrl = "";
    protected $_controllerPath = "";
    
    // These are taken from config/settings
    protected $_erp_key = "";
    protected $_erp_secret_key = "";
    protected $_installation_id = "";
    
    // Holds API parameters sent to API
    protected $_params = array();
    
    
    
    /**
     * Controller part of the Yii route to API controller, e.g. "someApiController".
     * Relative to API base URL!
     * 
     * @param string $controllerPath e.g. "someController" (from http://www.some.com/index.php?r=someControler/someAction)
     * @param array|false $params (optional) Parameters to be sent to API
     */
    public function __construct($controllerPath, $params = false) {
        $this->init($controllerPath, $params);
    }
    
    
    
    /**
     * Initialize
     * 
     * @see __construct()
     */
    public function init($controllerPath, $params) {
        
        // Controller path is required
        if (!$controllerPath) {
            throw new Exception('ERP API controller is required', 5000);
        }
        
        // Make sure we have trailing slash at the end of controller path
        $controllerPath = rtrim($controllerPath,"/") . "/";
        
        // If $params is passed and is an array, use it
        if ( ($params != false) && is_array($params)) {
            $this->_params = $params;
        }
        
        
        Yii::import('common.extensions.httpclient.*');
        
        // From config/settings
        $this->_apiBaseUrl = Yii::app()->params['erp_url'] . "www/index.php?r=" . $controllerPath;
        $this->_erp_key = Settings::getCfg('erp_key','');
        $this->_erp_secret_key = Settings::getCfg('erp_secret_key','');
        $this->_installation_id = Docebo::getErpInstallationId();
        
        
        $this->_controllerPath = $controllerPath;
        
        // Debuggingthrough proxy; 
        // Like: Fiddler2 (http://www.fiddler2.com/fiddler2/)
        //$this->proxyHost = '127.0.0.1';
        //$this->proxyPort = '8888';
         
    }
    
    
    /**
     * Return a class instance; create if not existent yet 
     * 
     * @param string $controllerPath
     * @param array $params (optional)
     * @see __construct()
     * 
     * @throws Exception
     * @return CalloutApiClient
     */
    static function getInstance($controllerPath, $params = false) {
        if (self::$instance != null) {
            return self::$instance;
        }
        self::$instance = new CalloutApiClient($controllerPath, $params);
        
        if (!self::$instance) {
            throw new Exception('Could not create an ERP API client instance', 5000);
        }
        
        return self::$instance;
    }
    

    /**
     * Build the X-Authorization header used to authenticate with ERP
     * 
     * @param array $params
     * @return string
     */
    protected function getXAuthHeader($params) {
        
        // For some reason, the old API client exclude fields having value starting with '@'
        // We do the same, though no idea why.
        $hashedParams = array();
        foreach($params as $k => $v) {
            if (substr($v, 0, 1) != '@') {
                $hashedParams[$k] = $v;
            }
        }
        
        
        $hash = sha1(implode(',', $hashedParams) . ',' . $this->_erp_secret_key);
        $header = self::API_REALM_NAME . ' ' . base64_encode($this->_erp_key . ':' . $hash);
        return $header;
    }
    
    
    
    /**
     * Execute API call and return result. 
     * Throws an exception in case of errors.
     * 
     * @param string $action Action part of the ERP API URL, e.g. "someAction" from http://www.some.com/index.php?r=someControler/someAction 
     * @param array $params
     * 
     * @throws Exception
     * 
     * @return array
     */
    public function call($action, $params = false) {

        // Add Controller Action name
        $url = $this->_apiBaseUrl . $action;
        
        // Create EHttpClient
        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
        ));

        // If no $params is passed during the call, use the one passed during creation of the class
        // Otherwise, merge call-time parameters with creation-time parameters.
        // Example: Create the class with some base parameters and make next calls with specific parameters
        if ($params == false) {
            $params = $this->_params;  // this is already initialized in init()
        }
        else {
            $params = array_merge($this->_params, $params);
        }
        
        // We need this LMS installation Id always sent to ERP API Controller!
        if (!isset($params["installation_id"])) {
            $params["installation_id"] = $this->_installation_id;
        }        
        
        // X-Auth Header
        $xAuthHeader = $this->getXAuthHeader($params);
        $this->_client->setHeaders('X-Authorization', $xAuthHeader);
        
        // Add POST fields (array)
        $this->_client->setParameterPost($params);
        
        // Do the request
        $response = $this->_client->request(EHttpClient::POST);
        
        //CVarDumper::dump($response, 20, true); die;
        
        // Check if we've got HTTP error; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            $status = $this->_client->getLastResponse()->getStatus();
            $errorMessage = $this->_client->getLastResponse()->getMessage();
            $body = $response->getBody();
            if ($body) {
                $info = CJSON::decode($body);
                if (isset($info["message"])) {
                    $errorMessage = $errorMessage . " (" . $info['message'] . ")";
                }
                if (isset($info["errorcode"])) {
                    $satus = $info["errorcode"];
                }
            }
            
            throw new Exception($errorMessage, $status);
        }
        
        // No response?
        if (!$response) {
            throw new Exception('Invalid response from server', 5000);
        }
        
        // We are expecting a JSON encoded data; decode
        $result = CJSON::decode($response->getBody());
        
        // If format is invalid
        if ( ($result == null) || !is_array($result) ) {
            throw new Exception('Invalid data format received from server. Must be an array.', 5000);
        }
        
        // Result might be HTTP OK, but we may get API Controller specific error (success=false).
        if ( (isset($result["success"]) && ($result["success"]==false)) || (isset($result["errorcode"]) && ((int)$result["errorcode"] > 0))) {
            $apiRoute = $this->_controllerPath . $action;
            if (isset($result["message"])) {
                throw new Exception("API action $apiRoute returns error: " . $result["message"], isset($result["errorcode"]) ? $result["errorcode"] : 5000);
            }
            else {
                throw new Exception("API action $apiRoute returns error, but message not specified", isset($result["errorcode"]) ? $result["errorcode"] : 5000);
            }
        }
        
        // Return an array of data for direct usage
        return $result;
        
        
        
    }
    
    
    public function test() {
    }
    
}

?>
