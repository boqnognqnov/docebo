<?php

/*
 * Behavior that extends the functionality of the CoreUser model
 */
class UserBehavior extends CBehavior{
	
	public function events() {
		return array(
			'onBeforeSave' => 'beforeSave',
			'onAfterSave' => 'afterSave',
			'onBeforeDelete' => 'beforeDelete',
		);
	}
	
	public function beforeSave(){
		// return parent::beforeSave();
	}
	
	public function afterSave(){
		//return parent::afterSave();
	}
	
	public function beforeDelete(){
		Yii::app()->event->raise('UserDeleted', new DEvent($this, array(
			'user' => $this->getOwner(),
		)));
		return true;
	}

}

?>
