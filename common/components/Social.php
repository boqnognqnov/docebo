<?php

class Social
{

	/**
	 * Shows the Twitter, Linkedin, Facebook, etc login buttons
	 * if their apps are correctly configured
	 */
	static public function displaySocialLoginButtons(){
		$html = '';
		if (self::isActive('twitter')) {
			$html .= '<div class="sso-login-btn">
											<a title="Twitter" href="' . Docebo::createAbsoluteLmsUrl('socialLogin/twitterLogin') . '">
											<i class="fa fa-twitter"></i><span>'.Yii::t('login','Twitter').'</span></a>
									</div>';
		}
		if (self::isActive('linkedin')) {
			$html .= '<div class="sso-login-btn">
											<a title="LinkedIn" href="' . Docebo::createAbsoluteLmsUrl('socialLogin/linkedinLogin') . '">
											<i class="fa fa-linkedin"></i><span>'.Yii::t('login','LinkedIn').'</span></a>
									</div>';
		}
		if (self::isActive('facebook')) {
			$fbObject = self::getFacebookObject();

			$loginUrl = $fbObject->getLoginUrl(array(
					'redirect_uri' => Docebo::createAbsoluteLmsUrl('socialLogin/fbLogin'),
					'scope'        => 'email'
			));

			$html .= '<div class="sso-login-btn">
											<a title="Facebook"href="' . $loginUrl . '">
											<i class="fa fa-facebook"></i><span>'.Yii::t('login','Facebook').'</span></a>
									</div>';
		}
		echo $html;
	}

	/**
	 * Shows the Twitter, Linkedin, Facebook, etc login icons
	 * if their apps are correctly configured
	 */
	static public function displaySocialLoginIcons()
	{
		$html = '';
		if (self::isActive('twitter')) {
			$html .= '<div class="pull-left">
											<a title="Twitter" class="header-lostpass" href="' . Docebo::createAbsoluteLmsUrl('socialLogin/twitterLogin') . '">
												' . CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/twitter-24.png") . '
											</a>&nbsp;&nbsp;&nbsp;
									</div>';
		}
		if (self::isActive('linkedin')) {
			$html .= '<div class="pull-left">
											<a title="LinkedIn" class="header-lostpass" href="' . Docebo::createAbsoluteLmsUrl('socialLogin/linkedinLogin') . '">
												' . CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/linkedin-24.png") . '
											</a>&nbsp;&nbsp;&nbsp;
									</div>';
		}
		if (self::isActive('facebook')) {
			$fbObject = self::getFacebookObject();

			$loginUrl = $fbObject->getLoginUrl(array(
				'redirect_uri' => Docebo::createAbsoluteLmsUrl('socialLogin/fbLogin'),
				'scope'        => 'email'
			));

			$html .= '<div class="pull-left">
											<a title="Facebook" class="header-lostpass" href="' . $loginUrl . '">
												' . CHtml::image(Yii::app()->theme->baseUrl . "/images/loginexternal/facebook-24.png") . '
											</a>&nbsp;&nbsp;&nbsp;
									</div>';
		}

		echo $html;

		YiI::app()->event->raise('afterSocialLoginIcons', new DEvent(Yii::app()->getController(), array()));
	}

	static public function isActive($network_code)
	{
		$res = FALSE;

		$cfg = self::getConfig();

		switch ($network_code) {
			case 'twitter':
			{
				$res = (Settings::get('social_twitter_active') == 'on' &&
					!empty($cfg['twitter_key']) &&
					!empty($cfg['twitter_secret']));
			}
				break;
			case 'linkedin':
			{
				$res = (Settings::get('social_linkedin_active') == 'on' &&
					!empty($cfg['linkedin_key']) &&
					!empty($cfg['linkedin_secret']));
			}
				break;
			case 'facebook':
			{
				$res = (Settings::get('social_fb_active') == 'on' &&
					!empty($cfg['fb_api']) &&
					!empty($cfg['fb_secret']));
			}
				break;
			case 'google':
			{
				$res = (Settings::get('social_google_active') == 'on');
			}
				break;
			case 'google_apps':
			{
				$res = FALSE;
			}
				break;
		}

		return $res;
	}

	static private function getConfig()
	{
		$config = array();

		$config['twitter_key']     = Settings::get('social_twitter_consumer');
		$config['twitter_secret']  = Settings::get('social_twitter_secret');
		$config['linkedin_key']    = Settings::get('social_linkedin_access');
		$config['linkedin_secret'] = Settings::get('social_linkedin_secret');
		$config['fb_api']          = Settings::get('social_fb_api');
		$config['fb_secret']       = Settings::get('social_fb_secret');

		return $config;
	}

	static public function getFacebookObject()
	{
		// Include the Facebook API library
		Yii::import('addons.social.facebook.Facebook');

		$conf = self::getConfig();

		$object = new Facebook(array(
			'appId'  => $conf['fb_api'],
			'secret' => $conf['fb_secret'],
			'cookie' => TRUE,
			'trustForwarded' => true
		));

		Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = FALSE;

		$_SESSION['fb_from'] = 'login';

		return $object;
	}
}

?>
