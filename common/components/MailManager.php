<?php
/* ======================================================================== \
| 	DOCEBO - The E-Learning Suite											|
| 																			|
| 	Copyright (c) 2009 (Docebo)												|
| 	http://www.docebo.com													|
|   License 	http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt		|
\ ======================================================================== */


class MailManager {

	private $transport;
	private $mailer;
	private $content_type ='text/html';
	private $_attachment_key_for_filename = false;


	function __construct() {

		/*
		$common_web_root = Yii::getPathOfAlias('common');
		if (!defined('SWIFT_LIB_DIRECTORY')) define('SWIFT_LIB_DIRECTORY', $common_web_root.'/vendors/swiftmailer5');
		require_once $common_web_root.'/vendors/swiftmailer5/swift_required.php';
		*/

		// Solve "autoloading" issue by temporary unregister Yii and then register back
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import('common.vendors.swiftmailer5.swift_required', true);
		spl_autoload_register(array('YiiBase','autoload'));

		//Create the Mail Transport
		// If SMTP relay host is set (and relay is enabled") use it
		// Otherwise, use the old style - native PHP mail mechanic 
		if (
				isset(Yii::app()->params['smtp_relay']['enabled']) && 
				Yii::app()->params['smtp_relay']['enabled'] &&
				isset(Yii::app()->params['smtp_relay']['host'])
			)  {
			$this->transport =Swift_SmtpTransport::newInstance(Yii::app()->params['smtp_relay']['host']);
		}
		else {
			$this->transport =Swift_MailTransport::newInstance();
		}

		//Create the Mailer using your created Transport
		$this->mailer =Swift_Mailer::newInstance($this->transport);
	}


	/**
	 * Send mail method
	 * @param $from_arr
	 * @param $to_arr
	 * @param $subject
	 * @param $message
	 * @param bool $attach_arr
	 * @param bool $cc_arr
	 * @param bool $bcc_arr
	 * @param array $reply_to
	 * @return bool|int
	 */
	public function mail($from_arr, $to_arr, $subject, $message, $attach_arr = false, $cc_arr = false, $bcc_arr = false, $reply_to = array(), $useUndisclosed = true) {

		if (!empty(Yii::app()->params['on_localhost']) && !Yii::app()->params['localhost_sendmail']) {
			return true;
		}

		$msg_obj = Swift_Message::newInstance()
				->setEncoder(Swift_Encoding::get8BitEncoding())
				->setCharset('utf-8')
				->setContentType($this->getContentType())
				->setMaxLineLength(70)

				//Give the message a subject
				->setSubject($subject)

				//Set the From address with an associative array
				->setFrom($from_arr)

				//Give it a body
				->setBody($message);

		// If more than one recipients and if we are asked to use Undiclosed/BCC approach, apply it
		if ((count($to_arr) > 1) && $useUndisclosed) {
			$to = false;
			//Get the first email address of the Senders. This is for showing e-mail address of the sender. If this fails, it will be nobody@docebo.info.
			if(!$to){
				if(is_array($from_arr)){
					foreach ($from_arr as $from) {
						if(!empty($from)){
							$to = $from;
							break;
						}
					}
				} else{
					$to = $from_arr;
				}
			}
			if (!$to) $to = 'nobody@docebo.com';
			$msg_obj->setTo(array($to => 'Undisclosed Recipients')); // and send the rest using Bcc
			$msg_obj->setBcc($to_arr);
		}
		else {
			$msg_obj->setTo($to_arr); // Normal send
		}


		// If set, add Advanced Settings -> CC Emails to CC list
		$ccEmailSetting = Settings::get('send_cc_for_system_emails', false);
		if ($ccEmailSetting) {
			if (!is_array($cc_arr)) {
				$cc_arr = array();
			}
			// Allow adding more than one email, comma separated
			$tmpArray = preg_split('/,/', $ccEmailSetting);
			foreach ($tmpArray as $ccEmail) {
				$ccEmail = trim($ccEmail);
				if (Swift_Validate::email($ccEmail)) {
					$cc_arr[] = $ccEmail;
				}
				else {
					Yii::log('Invalid email specified in Advanced Setting - CC Emails: ' . $ccEmail, 'debug');
				}
			}
		}


		if (!empty($cc_arr)) {
			$msg_obj->setCc($cc_arr);
		}

		if (!empty($bcc_arr)) {
			foreach ($bcc_arr as $bcc_email) {
				$msg_obj->addBcc($bcc_email);
			}
		}


		if (!empty($reply_to)) {
			foreach ($reply_to as $reply_to_email) {
				$msg_obj->addReplyTo($reply_to_email);
			}//$message->setReplyTo(current($reply_to));
		}

		if (is_array($attach_arr)) {
			foreach ($attach_arr as $filename => $path) {
				if ($this->_attachment_key_for_filename) // use key of row like name of file
					$attachment = Swift_Attachment::fromPath($path)->setFilename($filename);
				else
					$attachment = Swift_Attachment::fromPath($path);
				// Attach it to the message
				$msg_obj->attach($attachment);
			}
		}

		/*- Add additional return_path -*/
		if ( Settings::get('return_path', FALSE) )
			$msg_obj->setReturnPath(Settings::get('return_path', FALSE));

		return $this->mailer->send($msg_obj);
	}


	public function getContentType() {
		return $this->content_type;
	}


	public function setContentType($content_type) {
		$valid =array('text/plain', 'text/html');
		if (in_array($content_type, $valid)) {
			$this->content_type = $content_type;
		}
	}


	public function useAttachmentKeyForFilename() {
		$this->_attachment_key_for_filename = true;
	}


	/**
	 * Return an array from a text list of email addresses like:
	 * "Claudio ABCD" <claudio.abcd@xxy.com>, "Giovanni ABCD" <giovanni.abcd@xxy.com>, a4834.abcd@xxy.com,
	 * @param $plain_list string
	 * @return array
	 */
	public static function getEmailListArray($plain_list) {
		$res = array();

		$plain_list = trim(trim($plain_list), ",");
		$email_list = explode(",", $plain_list);

		foreach ($email_list as $address) {

			$found = array();
			$info_count = preg_match('/([^<]*)<(.*)>/is', $address, $found);

			if ($info_count > 0 && !empty($found[2])) {
				$name = trim(trim($found[1]), '"'); // example: Array ( [0] => "Giovanni ABCD" [1] => "Giovanni ABCD" [2] => giovanni.abcd@xxy.com )
				$email = trim($found[2]);
				$res[$email]=$name;
			}
			else {
				$res[]=trim($address);
			}
		}

		return $res;
	}


}
