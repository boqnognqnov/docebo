<?php



class DoceboWebUser extends CWebUser {

	// groups leveles names
	public $level_godadmin		= '/framework/level/godadmin';
	public $level_admin 		= '/framework/level/admin';
	//public $level_publicadmin	= '/framework/level/publicadmin';
	public $level_user 			= '/framework/level/user';

	protected $user_info;

	/**
	 * This is the cache for the user dateformat, is null because false is a possible value and NULL is the not intialized value
	 * @var null
	 */
	protected $userDateFormatSetting = NULL;

	// How often should we update the user's 'lastenter' timestamp as he navigates
	const REFRESH_LAST_ENTER = 600; // 5 minutes
    const ACCESS_TOKEN_COOKIE_NAME = 'hydra_access_token';

	/**
	 * @var string Timezone forced for the user (e.g. set by api controller)
	 */
	private $_timezone = null;

    private $_accessToken = null;


	public function init()
	{
        if(!$this->_accessToken){
            $request = Yii::app()->request;
            /**
             * @var $request DoceboHttpRequest
             */
            $cookies = $request->getCookies();

            $accessTokenCookie = $cookies->itemAt(self::ACCESS_TOKEN_COOKIE_NAME);
            /**
             * @var $accessTokenCookie CHttpCookie
             */
            if($accessTokenCookie){
                $this->_accessToken = $accessTokenCookie->value;
            }
        }
		parent::init();
		$this->checkLightAuth();
	}

	public function loginRequired()
	{
		$app=Yii::app();
		$request=$app->getRequest();

		if(!$request->getIsAjaxRequest())
		{
			$this->setReturnUrl($request->getUrl());
			if(($url=$this->loginUrl)!==null)
			{
				if(is_array($url))
				{
					$route=isset($url[0]) ? $url[0] : $app->defaultController;
					$url=$app->createUrl($route,array_splice($url,1));
				}
				$request->redirect($url);
			}
		}
		elseif(isset($this->loginRequiredAjaxResponse))
		{
			echo $this->loginRequiredAjaxResponse;
			Yii::app()->end();
		}

		Log::_(Yii::t('yii','Login Required'), CLogger::LEVEL_WARNING);
		$this->redirect(Docebo::createLmsUrl(''));
	}

	/**
	 * Overwriting the getIsGuest() function from CWebUser to add a check for auth expiration
	 * @return bool
	 */
	public function getIsGuest()
	{
		$this->setStateKeyPrefix("DoceboWebUser");
		$isGuest = $this->getState('__id') === null;
		$expires = $this->getState('__expires');

		if (Settings::get('ttlSession')) {
			$this->authTimeout = Settings::get('ttlSession');
		}
		if (!$this->authTimeout) $this->authTimeout = 30 * 3600;

		if (!$isGuest && $this->authTimeout !== null) {
			if ($expires !== null && $expires < time())  // authentication expired
			{
				// TBD:
				//   - Either always (true) or never (false) destroys session data! Not what everyone wants...
				//   - Make sure __expires is also cleared from session in logout()
				$this->logout();
				$isGuest = true;
			} else // update expiration timestamp
			{
				$this->setState('__expires', time() + $this->authTimeout);
			}
		}
		return $isGuest;
	}

	/**
	 * Return user model of the currently logged in user
	 * @return CoreUser
	 */
	public function loadUserModel($refresh = false) {
		if (empty($this->user_info) || $refresh) {
			$this->user_info = CoreUser::model()->findByPk($this->id);
		}

		return $this->user_info;
	}

	public function getEmail() {
		return $this->user_info->email;
	}

	public function getIsGodadmin() {
		return Yii::app()->user->checkAccess( $this->level_godadmin );
	}

	public function getIsAdmin() {

		return Yii::app()->user->checkAccess( $this->level_admin );
	}

	public function getIsPublicadmin() {

		return Yii::app()->user->checkAccess( $this->level_publicadmin );
	}

	public function getIsPu(){
		return !$this->getIsGodadmin() && $this->getIsAdmin();
	}



	/**
	 * Checks if the current user is a Power User and NOT an Instructor for the given course id.
	 * While using this function uses of "getIsGodAdmin" method with "NOT AND" {!Yii::app()->user->getIsGodAdmin() && .....->checkPURights()} operator is overkill.
	 * @param int $idCourse Course ID to check if the current PU is Instructor in the course.
	 * @param int $idUser Set User Id if you want the function to check if the PU has the permission to edit that user. Is the User with ID $idUser assigned to the current PU?
	 * @return array array('user' => (bool), 'course' => (bool), 'instructor' => (bool), 'justPU' => (bool))
	 */
	public function checkPURights($idCourse, $idUser = -1)
	{
		// I've extended this function with these functionalities to try not to duplicate one code several times in the needed uses.
		// I know this isn't the right way, but until i figure a better way this will do the trick.
		$user_id = Yii::app()->user->getId(); // Current user id to check if he is PU
		$result = array(
			'courseIsAssigned' => CoreUserPU::isAssignedToPU($idCourse, 'course'),
			'isPu' => $this->getIsPu()
		);

		if ($idUser > 0) {
			$result['userIsAssigned'] = CoreUserPU::isAssignedToPU($idUser, 'user');
		}

		$i = 0;
		$count = count($result);

		foreach ($result as $statement => $bool) {
			$i = $bool ? $i + 1 : $i;
		}

		$result['isInstructor'] = LearningCourseuser::isUserLevel($user_id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
		$result['isTutor'] = LearningCourseuser::isUserLevel($user_id, $idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR);
		$result['all'] = ($i == $count);

		return (object)$result;
	}




	public function getIsUser() {

		return Yii::app()->user->checkAccess( $this->level_user );
	}

	public function getIdst() {
		$userInfo = $this->loadUserModel();
		return $userInfo['idst'];
	}

	public function getGodadminLevelLabel(){
		return $this->level_godadmin;
	}
	public function getAdminLevelLabel(){
		return $this->level_admin;
	}
	public function getUserLevellabel(){
		return $this->level_user;
	}

	public function getAllGodadmins($onlyIds = false){
		$adminGroup = $this->getGodadminLevelLabel();
		$godadminIds = Yii::app()->getDb()->createCommand()
			->selectDistinct('u.idst')
			->from(CoreUser::model()->tableName().' u')
			->join(CoreGroupMembers::model()->tableName().' gm', 'gm.idstMember=u.idst')
			->join(CoreGroup::model()->tableName().' g', 'g.idst = gm.idst AND g.groupid = :godadmins', array(':godadmins'=>$adminGroup))
			->queryColumn();
		if($onlyIds === true) return $godadminIds;
		return CoreUser::model()->findAllByAttributes(array('idst'=>$godadminIds));
	}

	public function getGodadminsEmails(){
		$godAdmins = $this->getAllGodadmins();

		$emails = array();
		if($godAdmins){
			foreach($godAdmins as $godAdmin){
				if(isset($godAdmin->email) && !in_array($godAdmin->email, $emails)){
					$emails[] = $godAdmin->email;
				}
			}
		}

		return $emails;
	}


	/**
	 * Return array with attributes for the logged in user or default attributes if anonymous
	 * @return array
	 */
	public function getUserInfoArr() {
		$user_info_arr = array();

		if (!Yii::app()->user->getIsGuest()) {
			$user = $this->loadUserModel();
			if ($user) {
				$user_info_arr = $user->getAttributes();
			}
		}

		if (empty($user_info_arr)) { // anonymous user:
			$tmp_user = new CoreUser();
			$user_info_arr = $tmp_user->getAttributes();

			$extra_default_arr = array(
				'userid'=>'/Anonymous',
			);

			$user_info_arr = array_merge($user_info_arr, $extra_default_arr);
		}

		return $user_info_arr;
	}


	/**
	 * Return the required attribute for the logged in user
	 * @param $attrib_name
	 * @return mixed|null
	 */
	public function getUserAttrib($attrib_name) {
		$user_info_arr = array();

		if (!Yii::app()->user->getIsGuest()) {
			$user = $this->loadUserModel();
			if ($user) {
				$user_info_arr = $user->getAttributes();
			}
		}

		if (empty($user_info_arr)) { // anonymous user:
			$user_info_arr = array(
				'idst'=>0,
				'userid'=>'/Anonymous',
			);
		}

		return (isset($user_info_arr[$attrib_name]) ? $user_info_arr[$attrib_name] : null);
	}


	/**
	 * Get the lastenter field of the currently logged in user
	 * @return string|bool
	 */
	public function getLastEnter() {
		if (!$this->getIsGuest()) {
			$row = $this->loadUserModel();
			return $row->lastenter;
		}
		else {
			return false;
		}
	}


	/**
	 * Set the lastenter field of the currently logged in user
	 * @param $lastenter date is expected in Y-m-d H:i:s format and UTC timezone
	 * @return bool
	 */
	public function setLastEnter($lastenter) {
		if (!Yii::app()->user->getIsGuest()) {
			Yii::app()->event->raise('BeforeUpdatingUserLastEnter', new DEvent($this, array()));
			$row = CoreUser::model()->findByPk($this->id);
			if(!$row)
				return false;

			$row->lastenter = Yii::app()->localtime->toLocalDateTime($lastenter);
			$row->scenario = 'updateLastEnter';
			$row->save(false);
			$row->refresh(); //do this to ensure avoiding any problem with local time dates (although it should be already handled in LocalTimeConversionBehavior component)

			return true;
		}
		else {
			return true;
		}
	}

	public function updateLastEnter(){
		if (Yii::app()->user->getIsGuest())
			return;

		if(isset($_SESSION['user_enter_mark'])) {
			if($_SESSION['user_enter_mark'] < (time() - self::REFRESH_LAST_ENTER)) {
				$this->setLastEnter(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
				$_SESSION['user_enter_mark'] = time();
			}
		} else {
			$this->setLastEnter(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
			$_SESSION['user_enter_mark'] = time();
		}
	}


	/**
	 * Transform username from "user" to "/user"
	 * @param $username
	 * @return string
	 */
	public function getAbsoluteUsername($username) {
		if (substr($username, 0, 1) != '/') {
			return '/'.$username;
		}

		return $username;
	}


	/**
	 * Transform username from "/user" to "user"
	 * @param $username
	 * @return string
	 */
	public function getRelativeUsername($username) {
		$res = $username;

		if (substr($username, 0, 1) == '/') {
			$res = substr($username, 1);
		}

		return $res;
	}

	/**
	 * Checks whether the passed user is expired
	 * @param $user
	 */
	public function isAccountExpired($user) {
		if($user->expiration && !CoreUser::isUserGodadmin($user->idst)) {
			$currentDayInLms = Yii::app()->localtime->getLocalNow('Y-m-d');
			$expirationDate = CoreUser::getExpirationDate($user->idst);
			if ($currentDayInLms > $expirationDate)
				return true;
		}

		return false;
	}

	/**
	 * Call the original login method and sync the old LMS session / auth
	 * @param IUserIdentity $identity
	 * @param int $duration
	 * @return bool
	 */
	public function login($identity,$duration=0) {
		$user = CoreUser::model()->findByPk($identity->getId());

		// check if the current user have expiration and if his account is expired!
		if($this->isAccountExpired($user)) {
			// the user account is expired, don't let him login, but show the message for that error
			Yii::app()->user->setFlash('expired_account', Yii::t('standard', 'Your account has expired on {date}. Please, contact your administrator for further information.', array('{date}' => $user->expiration)));
			Yii::app()->getController()->redirect(Yii::app()->createAbsoluteUrl('site/index', array('error' => 5)), true, 302, true);
			return false;
		}

		if($user->lastenter == 0 && Settings::get('enable_email_verification') === 'on' && $user->email_status == 0){
			// event raise to run notification, if enabled, first create hash for email validation
			$ok = $user->setVerificationLink();
			if ($ok) {
				Yii::app()->event->raise('UserEmailVerified', new DEvent($this, array('user' => $user, 'fromLoginPage' => true)));
			} else {
				Yii::app()->user->setFlash('email_verification_reminder', Yii::t('standard', '_OPERATION_FAILURE') . '! ' . Yii::t('course', 'Please contact your administrator'));
			}
		}

		// if the user is godadmin, skip the next check
		if(!CoreUser::isUserGodadmin($user->idst)) {
			/* @var $cm CoreMultidomain */
			$cm = CoreMultidomain::resolveClient(true);
			if ($cm) {
				if ($cm->signInPageLayoutEnabled == 1) {
					// we have custom setting activated
					if ($cm->enableLoginRestriction == 1) {
						$this->checkIsUserFromMultidomain($identity, $cm);
					}
				} else {
					// check global settings
					if (Settings::get('user_login_check', 0) == 1) {
						$this->checkIsUserFromMultidomain($identity, $cm);
					}
				}
			}
		}

		$login_ok = parent::login($identity, $duration);

		// Clear left menu cache
		Yii::app()->cache->delete('cached_menu_'.Yii::app()->user->id);
		if ($login_ok) {

			Yii::app()->event->raise('AfterUserLogin', new DEvent($this, array()));
			//set session timeout limit, reading from our settings
			$seconds = Settings::get('ttlSession');
			Yii::app()->getSession()->setTimeout($seconds);

			// force the initializaziont of the array of permission inside session and cache
			Yii::app()->getAuthManager();

			$_SESSION['last_enter'] = $this->getLastEnter();

			// prepare the in for related to the old lms session that are needed in order to correctly move the user back and forth the lms
			$this->oldLmsUserSignIn();

			// Check if the user has a language assigned as default. If not, save the
			// current language (the one he used on login page) as his default)
			$userSetting_lang = CoreSettingUser::model()->countByAttributes(array(
				'id_user' => $this->id,
				'path_name' => 'ui.language'
			));
			if (!$userSetting_lang && Yii::app()->getLanguage()) {
				$userSetting_lang = new CoreSettingUser();
				$userSetting_lang->path_name = 'ui.language';
				$userSetting_lang->id_user = $this->id;
				$userSetting_lang->value = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
				$userSetting_lang->save();
			}
		}

		if($login_ok){
			Yii::app()->event->raise(EventManager::EVENT_ON_AFTER_LOGIN, new DEvent($this, array('user'=>$user)));
		}

		return $login_ok;
	}

	private function checkIsUserFromMultidomain($identity, $cm){
		$result = true;
		$userId = $identity->getId();

		$orgId = $cm->org_chart;
		$orgModel = CoreOrgChartTree::model()->findByPk($orgId);

		// get all users in this branch and its descendants
		$allowedUsersForThisDomain = $orgModel->getBranchUsers();
		if (!in_array($userId, $allowedUsersForThisDomain)) {
			$result = false;
		}

		if(CoreUser::isPowerUser($userId) && $result === false){
			// additional check if the PU is managing selected branch or some of its descendants
			// only when this PU is not assigned in that branch or its descendants
			$branchesManagedByPU = array_keys(CoreAdminTree::getPowerUserOrgchart($userId));
			foreach ($branchesManagedByPU as $idst_oc) {
				$iLeft = Yii::app()->db->createCommand()
					->select('ileft')
					->from(CoreOrgChartTree::model()->tableName())
					->where('idst_oc=:id', array(':id' => $idst_oc))->queryScalar();
				if ($iLeft >= $orgModel->iLeft && $iLeft <= $orgModel->iRight) {
					$result = true;
					break;
				}
			}
		}

		if($result === false) {
			Yii::app()->getController()->redirect(Yii::app()->createAbsoluteUrl('site/index',
				array('error' => 2)),
				true, 302, true);
			Yii::app()->end();
		}

		return true;
	}


	/**
	 * Call the original login method
	 * @param IUserIdentity $identity
	 * @param int $duration
	 * @return bool
	 */
	public function parentLogin($identity,$duration=0) {
		return parent::login($identity,$duration);
	}


	//TODO: sync the session duration between the two environments (old / new lms)

	/**
	 * Login the user on the old LMS when the user perform login from Yii
	 * @param string $prefix
	 */
	public function oldLmsUserSignIn($prefix='public_area') {
		$idst = Yii::app()->user->id;

		if ($idst > 0) {
			$row = CoreUser::model()->findByPk($idst);

			if ($row) {
				$_SESSION[$prefix.'_idst'] = $idst;
				$_SESSION[$prefix.'_username'] = $this->getRelativeUsername($row->userid);

				$_SESSION['last_enter'] = $this->getLastEnter();
				$_SESSION['user_enter_mark'] = time();
				$this->setLastEnter(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

				// setting user language
				$userSetting_lang = CoreSettingUser::model()->findByAttributes(array(
					'id_user' => $idst,
					'path_name' => 'ui.language'
				));
				if ($userSetting_lang) {
					$_SESSION['current_lang'] = $userSetting_lang->value;
					$_SESSION['_lang'] = Lang::getBrowserCodeByCode($userSetting_lang->value);
				}
			}
		}
	}

	/**
	 * Checks if this is the first login of the user in the LMS
	 * (based on core_user.lastenter)
	 *
	 * @return bool
	 */
	public function isFirstLogin(){
		if(isset($_SESSION['last_enter']) && $_SESSION['last_enter'] != '0000-00-00 00:00:00' && strtotime($_SESSION['last_enter']))
			return FALSE;

		return TRUE;
	}

	/**
	 * Check the session to see if the user logged in the old LMS and
	 * if true the user will also be logged in in the Yii environment
	 * @param string $prefix
	 */
	public function checkOldLmsSession($prefix='public_area') {
		if (
			Yii::app()->user->getIsGuest() &&
			!Yii::app()->request->isAjaxRequest &&
			!empty($_SESSION[$prefix.'_username']) &&
			strcmp($_SESSION[$prefix.'_username'],'/Anonymous') != 0
		) {
			$idst = $_SESSION[$prefix.'_idst'];
			$username = $_SESSION[$prefix.'_username'];

			$ui = new DoceboUserIdentity($username, null);

			if ($ui->authenticateFromOldLms($idst, $username)) {
				$login_ok = $this->parentLogin($ui);
				if ($login_ok) {
					// Refresh the current page:
					// (commented as it doesn't seems to be required, as we execute this code before the Yii::app()->run();)
//					$url = Yii::app()->request->getRequestUri();
//					Yii::app()->request->redirect($url);
				}
			}
		}
	}


	/**
	 * Return the username of the currently logged in user
	 * This is the old getUserName method of Docebo::user
	 * @return string
	 */
	public function getDisplayName() {
		$res = '';
		$user_info = $this->getUserInfoArr();

		if (!empty($user_info['firstname']) && !empty($user_info['lastname'])) {
            $showFirstNameFirst = Settings::get('show_first_name_first', 'off');
            if($showFirstNameFirst == 'on')
                $res = $user_info['firstname'].' '.$user_info['lastname'];
            else
                $res = $user_info['lastname'].' '.$user_info['firstname'];
		}
		else {
			$res = $this->getRelativeUsername($user_info['userid']);
		}

		return $res;
	}


	/**
	 * Return the username of the logged in user (userid db field)
	 * @return string
	 */
	public function getUsername() {
		return $this->getRelativeUsername($this->getUserAttrib('userid'));
	}


	/**
	 * Return <img> HTNL element
	 *
	 * @param string $classname
	 * @return string
	 */
	public function getAvatar($classname = '') {
		// Use the generic CoreUser model's method
		if (empty($this->user_info)) {
			$this->loadUserModel();
		}
		return $this->user_info->getAvatarImage(false, $classname);
	}


    /**
     * Returns the time zone for the user
     * @return string
     */
    public function getTimeZone() {
        if($this->_timezone)
            $timeZone = $this->_timezone;
        else {
            $timeZone = Settings::get('timezone_default');
            if (!$this->getIsGuest() && 'on' === Settings::get('timezone_allow_user_override', 'off')) {
                // get user specific timezone
                $userTimeZoneSetting = CoreSettingUser::model()->findByAttributes(array(
                    'id_user' => Yii::app()->user->id,
                    'path_name' => 'timezone'
                ));
                if ($userTimeZoneSetting) {
                    $timeZone = $userTimeZoneSetting->value;
                }
            }
        }
        return $timeZone;
    }

    /**
     * Method called by the apis to force the timezone of the user to a certain value (usually UTC)
     * @param $timezone
     */
    public function forceTimezone($timezone) {
        $this->_timezone = $timezone;
		Yii::app()->localtime->resetLocalTimezone();
    }

    /**
     * Returns the local code for the user
     * @return string
     */
    public function getLocale() {
        $locale = Yii::app()->getLanguage();
        return $locale;
    }

    /**
     * Returns the date/time format selected by the user
     * @return bool|string
     */
    public function getSelectedDateTimeFormat() {
	    // ca
        if ( !$this->getIsGuest() ) {
            $dateFormatType = Settings::get('date_format');
            if ($dateFormatType === 'user_selected') {
	            if ($this->userDateFormatSetting === NULL) {

		            // get the date format specified by the user AND CACHE IT !
		            $userDateFormatSetting = CoreSettingUser::model()->findByAttributes(array(
			            'id_user' => Yii::app()->user->id,
			            'path_name' => 'date_format'
		            ));

		            if ($userDateFormatSetting) {
			            $this->userDateFormatSetting = $userDateFormatSetting->value;
		            } else {
			            $this->userDateFormatSetting = false;
		            }
	            }
	            return $this->userDateFormatSetting;
            }
        }
        return false;
    }


    public function checkLightAuth() {

	    // if the user is not anon this is useless, let's just skip the process
	    if (!$this->getIsGuest()) {

		    return array(
			    'success' 	=> false,
			    'message'	=> 'User already authenticated.',
		    );
	    }

    	$requestIdUser 			= Yii::app()->request->getParam('id_user', false);
    	$requestAuthCode 		= Yii::app()->request->getParam('auth_code', false);

    	if (($requestIdUser == false) || ($requestAuthCode == false)) {
    		return array(
    				'success' 	=> false,
    				'message'	=> 'No user/token provided. No token based authentication.',
    		);
    	}

    	$auth_token = new AuthToken($requestIdUser);

    	$result = array('success' 	=> true);
    	if ($auth_token->get(false) != $requestAuthCode) {
    		$result = array(
    				'success' => false,
    				'message' => 'Invalid authorization token',
    		);
    	}
    	else {
    		$user = CoreUser::model()->findByPk($requestIdUser);
    		if ($user) {
    			$ui = new DoceboUserIdentity($user->userid, null);
    			if ($ui->authenticateWithNoPassword(true)) {
    				$this->login($ui);
					LearningTracksession::model()->updateAll(array('active' => 0), 'active = 1 AND idUser = '.$user->idst);
    			}
    			else {
    				$result = array(
    						'success' => false,
    						'message' => 'Unable to authenticate user',
    				);
    			}
    		}
    		else {
    			$result = array(
    					'success' => false,
    					'message' => 'Invalid user',
    			);
    		}
    	}

    	return $result;


    }


    /**
	 * Get IDs of all "user" related groups/levels, e.g. the IDs of:
	 * /framework/level/godadmin, /framework/level/admin, /framework/level/user
	 *
	 * @param bool $flip - if true, use the label itself as array keys when returning. If false, use IDs as keys
	 *
	 * @return array
	 */
	public function getUserLevelIds($flip = false){
		$query = Yii::app()->getDb()->createCommand()
		->select('cg.idst, cg.groupid')
		->from(CoreGroup::model()->tableName(). ' cg')
		->where('cg.groupid LIKE "%/framework/level%"')
		->queryAll(1);

		$res = array();
		foreach($query as $row){
			$res[$row['idst']] = $row['groupid'];
		}

		if($flip) return array_flip($res);
		return $res;
	}

	/**
	 * Get highest available "level" for a given user (or the current user) as a string
	 *
	 * @param null $userId
	 *
	 * @return string, e.g. "/framework/level/godadmin"
	 */
	public function getHighestLevel($userId = null){
		if($userId==null)
			$userId = Yii::app()->user->getId();

		$groupdIdstForUser = Yii::app()->getDb()->createCommand()
			->select('cg.groupid')
			->from(CoreGroupMembers::model()->tableName().' cgm')
			->join(CoreGroup::model()->tableName(). ' cg', 'cg.idst=cgm.idst AND cg.groupid LIKE :label', array(':label'=>'%/framework/level%'))
			->join(CoreUser::model()->tableName(). ' cu', 'cu.idst=cgm.idstMember AND cu.idst=:idst', array(':idst'=>$userId))
			->queryColumn();

		if (in_array($this->getGodadminLevelLabel(), $groupdIdstForUser)){
			return $this->getGodadminLevelLabel();
		} elseif(in_array($this->getAdminLevelLabel(), $groupdIdstForUser)){
			return $this->getAdminLevelLabel();
		} else{
			return $this->getUserLevellabel();
		}

	}

	/**
	 * Return list of user levels, in terms of /framework/level
	 *
	 * @param integer $userId
	 * @return array
	 */
	public function getUserLevels($userId = null) {

		if($userId==null)
			$userId = Yii::app()->user->getId();

		$result = Yii::app()->getDb()->createCommand()
			->select('cg.groupid')
			->from(CoreGroupMembers::model()->tableName().' cgm')
			->join(CoreGroup::model()->tableName(). ' cg', 'cg.idst=cgm.idst AND cg.groupid LIKE :label', array(':label'=>'%/framework/level%'))
			->join(CoreUser::model()->tableName(). ' cu', 'cu.idst=cgm.idstMember AND cu.idst=:idst', array(':idst'=>$userId))
			->queryColumn();

		return $result;
	}

    /**
     * Genarate access token to communicate with Hydra API
     *
     * @param $timeout integer Number of seconds that the access token will be valid from now
     * @return array Access Token Data
     */
	private function _generateAccessToken($timeout){
	    $oAuthServer = DoceboOauth2Server::getInstance();
        $oAuthServer->init($timeout);

        return $oAuthServer->createAccessToken('hydra_frontend', $this->getId(), 'api', $oAuthServer->serverConfiguration);
    }

    /**
     * Remove all expired access token related to Hydra API
     */
    private function _removeExpiredAccessTokens(){
        /**
         * @var $command CDbCommand
         */
        $command = Yii::app()->db->createCommand();
        $command->delete('oauth_access_tokens', 'expires < :now AND client_id = :hydra_client AND scope = :scope_api', array(
            ':now' => date('Y-m-d H:i:s'),
            ':hydra_client' => 'hydra_frontend',
            ':scope_api' => 'api'
        ));
    }

    /**
     * @overrated
     *
     * Updates the authentication status according to {@link authTimeout}.
     * If the user has been inactive for {@link authTimeout} seconds, or {link absoluteAuthTimeout} has passed,
     * he will be automatically logged out.
     * @since 1.1.7
     */
    protected function updateAuthStatus()
    {
        if(($this->authTimeout!==null || $this->absoluteAuthTimeout!==null) && !$this->getIsGuest())
        {
            $expires=$this->getState(self::AUTH_TIMEOUT_VAR);
            $expiresAbsolute=$this->getState(self::AUTH_ABSOLUTE_TIMEOUT_VAR);

            if ($expires!==null && $expires < time() || $expiresAbsolute!==null && $expiresAbsolute < time())
                $this->logout(false);
            else {
                $this->setState(self::AUTH_TIMEOUT_VAR, time() + $this->authTimeout);

                if(!$this->_accessToken){
                    $accessToken = $this->_generateAccessToken($this->authTimeout);

                    if($accessToken){
                        $this->_accessToken = $accessToken['access_token'];

                        $cookie = new CHttpCookie(self::ACCESS_TOKEN_COOKIE_NAME, $this->_accessToken);
                        $cookie->expire = 0;
                        $cookie->domain = Yii::app()->request->serverName;
                        $cookie->httpOnly = true;

                        Yii::app()->request->cookies[self::ACCESS_TOKEN_COOKIE_NAME] = $cookie;
                    }
                } else{
                    /**
                     * @var $command CDbCommand
                     */
                    $command = Yii::app()->db->createCommand();
                    $command->update('oauth_access_tokens', array(
                        'expires' => date('Y-m-d H:i:s', time() + $this->authTimeout)
                    ),
                        'access_token = :token', array(
                            ':token' => $this->_accessToken
                        ));
                }

//                $this->_removeExpiredAccessTokens();
            }
        }
    }

    public function getAccessToken(){
        return $this->_accessToken;
    }

    /**
     * User login via passed GET[access_token] or HTTP Authorization header with Bearer token
     * Once the sessions has been restored it stays active for as long as the service setting
     * is defined a sessions life is
     *
     * @see    Session lifetime configuration:
     *          - http://www.php.net/manual/en/session.configuration.php
     *          - http://www.yiiframework.com/doc/api/1.1/CHttpSession
     *
     * @param  string $token = null
     *
     * @return null
     */
    public function loginByAccessToken($token = null){
        // Check if the current request has the Oauth2 header or access_token GET param
        $token = $token === null
            ? Yii::app()->request->getParam('access_token', false)
            : $token
        ;

        $isOauth2Request = $token || (stripos($_SERVER['HTTP_AUTHORIZATION'], 'Bearer ') === 0);

        if($isOauth2Request) {

        	// Search if the passed token matches a user
            $oauthServer = DoceboOauth2Server::getInstance();
            $oauthServer->init();
            $user = $oauthServer->getAuthorizedUser();
            if($user instanceof CoreUser) {

            	// Invalid previous session (if any)
				if(!Yii::app()->user->getIsGuest() && (Yii::app()->user->id !== $user->idst))
					Yii::app()->user->logout();

				// Session is clean now -> check if we need to login
                if(Yii::app()->user->getIsGuest()) {
                    $ui = new DoceboUserIdentity($user->userid, null);
                    if ($ui->authenticateWithNoPassword()){
                        Yii::app()->user->login($ui);
                        $this->restoreFromCookie();
                    }
                }
            }
        }
    }

}
