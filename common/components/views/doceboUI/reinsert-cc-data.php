<div id="reinsertCCDataModal" class="modal">
    <div class="modal-header">
        <h2><?php echo  Yii::t('userlimit', 'Please, re-enter your credit card information'); ?></h2>
    </div>

    <div class="modal-body">
        <div class="row-fluid">
            <div class="span12 text-center"><img src="<?= Docebo::getRootBaseUrl(true) . '/themes/spt/images/logo_stripe.png'; ?>"></div>
        </div>
        <div class="row-fluid text-left">
            <div class="span3"><i class="fa fa-credit-card" aria-hidden="true"></i><i class="fa  fa-2x fa-warning"></i></div>
            <div class="span9 action-text">
                <?=Yii::t('order', 'Dear <b>{userName}</b>,<br>in order to make any <b>payment transaction faster</b> and <b>safer</b>, we recently updated our payment gateway.', array('{userName}'=>$userName));?>
            </div>
            <div class="span12 action-text">
                <?=Yii::t('order', 'To continue renewing your LMS subscription, we kindly ask you 1 minute to <b>re-enter your credit card information</b>.');?><br><br>
                <?=Yii::t('order', 'Thank you!');?>
            </div>
        </div>
    </div>


    <div class="modal-footer">
        <a 	class="notnow" id="notnow" href="../lms/index.php?r=site/index"><?= Yii::t('standard', 'Not now'); ?></a>
        <a 	class="open-dialog btn btn-docebo big green"  data-dialog-class="editCCInfo-dialog" closeOnEscape="false" closeOnOverlayClick="false" id="toProceed" href="<?=Docebo::createLmsUrl('billing/default/upgrade', array('edit_ccard' => 1));?>"
              rel="edit-creditcard-modal"><?= Yii::t('standard', 'Click here to proceed'); ?></a>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function() {
		// Lower the 'menu' z-index to hide it behind the modal backdrop (currently 'menu' is 7500)
		$('.menu').css('z-index', '1040');

        var reinsertCCDataModal = $("#reinsertCCDataModal");
        reinsertCCDataModal.modal({
            backdrop: "static",
            keyboard: false
        });

        $(document).on('click', '#notnow', function(e){
            e.preventDefault();
            reinsertCCDataModal.modal("hide");
            $.get('<?= Docebo::createLmsUrl('billing/default/axSetReinsertCCDataPopup') ?>', {'status':'hide'}, null, 'json');
        });

        $(document).on('click', '#toProceed', function(e){
            reinsertCCDataModal.modal("hide");
            $.get('<?= Docebo::createLmsUrl('billing/default/axSetReinsertCCDataPopup') ?>', {'status':'unset'}, null, 'json');
        });

        $(document).on('click', '.editCCInfo-dialog .close', function(e){
            e.preventDefault();
            $.get('<?= Docebo::createLmsUrl('billing/default/axSetReinsertCCDataPopup') ?>', {'status':'hide'}, null, 'json');
        });

    });
</script>