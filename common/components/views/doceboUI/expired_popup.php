<?php
	$enable_user_billing = (Settings::get('enable_user_billing', 'on') === 'on') || isset(Yii::app()->request->cookies['reseller_cookie']);
?>

<div id="subscriptionUpgradeModal" class="wide modal hide fade">
    <div class="modal-header">
        <h2><!--<span class="i-sprite is-timer white"></span> <?php echo (Docebo::isExpiredPeriod()
			? Yii::t('userlimit', '_RE_ENABLE_SUBSCRIPTION')
			: Yii::t('userlimit', 'Your trial period is expired')
		); ?>--></h2>
    </div>

    <div class="modal-body">

    	<?php if (Yii::app()->user->isGodAdmin) : ?>
    		<div class="p-sprite timer-large-black big-timer"></div>
    		<div class="expired-note"><?= Yii::t('userlimit', 'Your trial period is expired') ?></div>
    		<?php if ($enable_user_billing) : ?>
    		<div class="row-fluid user-action">
    			<div class="span8 action-text">
    				<?= Yii::t('userlimit', 'To continue using Docebo choose your yearly or monthly plan') ?>
    			</div>
    			<div class="span4 text-left">
    				<a href="<?= Docebo::createLmsUrl('billing/default/upgrade') ?>" style="margin-top:4px;" class="btn btn-docebo big green"><?= Yii::t('userlimit', 'Click to continue') ?></a>
    			</div>
    		</div>
    		<?php else :?>
    		<div class="row-fluid">
    			<div class="span12 action-text" style="text-align: center;">
    				<a href="javascript:;"><?= Yii::t('userlimit', 'For further information, please contact your local docebo representative.') ?> </a>
    			</div>
    		</div>
    		<?php endif; ?>
    	<?php else: ?>
    		<div class="expired-note for-user">
    			<?= Yii::t('userlimit', 'The trial period of this installation has finished. Please contact the administrator for further inquiries.') ?>
    		</div>
    	<?php endif; ?>

    </div>


    <div class="modal-footer">
		<?php if (Docebo::isTrialExpired()) : ?>

			<?php if (Yii::app()->user->isGodAdmin) : ?>
            	<span class="i-sprite is-elapsed"></span> <?= Yii::t('userlimit', 'Do you need more information before choosing?') ?>
            	<a class="contact-sales-team" href="<?= $contactSalesUrl ?>"><?= Yii::t('userlimit', 'Contact our Team!') ?></a>
            <?php else : ?>
				<a 	class="btn btn-docebo big black close-dialog"
					href="../lms/index.php?r=site/logout"><?= Yii::t('standard', '_LOGOUT'); ?></a>

            <?php endif; ?>

		<?php endif; ?>
    </div>


</div>

<script type="text/javascript">
    $(document).ready(function() {

		// Lower the 'menu' z-index to hide it behind the modal backdrop (currently 'menu' is 7500)
		$('.menu').css('z-index', '1040');

        var subscriptionUpgradeModal = $("#subscriptionUpgradeModal");
        subscriptionUpgradeModal.modal({
            backdrop: "static",
            keyboard: false
        });

        // On "contact sales" click (available to Admins pnly!)
        $('#subscriptionUpgradeModal .modal-footer a.contact-sales-team').click(function () {
            subscriptionUpgradeModal.modal("hide");

            var $this = $(this);
            var contactSalesDialog = $('<div/>');

            contactSalesDialog.bind('dialog2.before-open', function (event) {
                contactSalesDialog.parents('.modal').eq(0).addClass('contact-sales-modal');
            });
            contactSalesDialog.bind('dialog2.closed', function (event) {
                window.location = '<?= Docebo::createLmsUrl('//site/index') ?>';
            });
            contactSalesDialog.dialog2({
                content:$this.attr('href')
            });

            contactSalesDialog.bind('dialog2.content-update', function (event) {
        		if ($(this).find("a.auto-close").length > 0) {
        			contactSalesDialog.dialog2('close');
        		}
            });

            return false;
        });
    });
</script>