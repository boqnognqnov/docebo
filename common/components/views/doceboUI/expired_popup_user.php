<div id="trialEndedModal" class="wide modal fade">
    <div class="modal-header">
		<h2><?php echo (Docebo::isExpiredPeriod()
				? Yii::t('userlimit', '_RE_ENABLE_SUBSCRIPTION')
				: Yii::t('userlimit', 'Your trial period is expired')
			); ?></h2>
    </div>
    <div class="modal-body">
		<?= Yii::t('userlimit', 'The trial period of this installation has finished. Please contact the administrator for further inquiries.') ?>
    </div>
    <div class="modal-footer">
        <a class="btn btn-primary close-dialog"
           href="../lms/index.php?r=site/logout"><?= Yii::t('standard', '_LOGOUT'); ?></a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var trialEndedModal = $("#trialEndedModal");
        trialEndedModal.modal({
            backdrop:"static",
            keyboard:false
        });
    });
</script>