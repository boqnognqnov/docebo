<div class="docebo-simple-barchart">
	<? foreach($this->data as $bar): ?>
		<div class="pull-left single-bar-holder" style="width: <?=100/count($this->data)?>%;">
			<div class="single-bar" style="height: <?=ceil($bar['height'])?>px;">
				<div class="value-holder">
					<?=$bar['label']?>
				</div>
			</div>

			<div class="label-holder" style="bottom: <?=ceil($bar['height'])+20?>px;">
				<?=$bar['value']?>
			</div>

		</div>
	<? endforeach; ?>
	<div class="clearfix"></div>
</div>