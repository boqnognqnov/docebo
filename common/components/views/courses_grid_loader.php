<?php
/**
 * Shows a container, makes AJAX call(s) to load actual grid.
 */
?>

<?php

//check if we have to display categories tree due to settings and grid type
$_gridCatalogTypes = array(
	CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG,
	CoursesGrid::$GRIDTYPE_EXTERNAL,
);

$showCategoriesTree = (Settings::get('catalog_use_categories_tree') === 'on' && in_array($this->gridType, $_gridCatalogTypes));
if ($showCategoriesTree) {
	//retrieve categories tree data
    $_treeData = $this->buildTreeData();
    if(empty($_treeData))
        $showCategoriesTree = false;
}

if ($showCategoriesTree) {

	$cs = Yii::app()->getClientScript();
	$cs->registerCssFile(Yii::app()->baseUrl.'/../plugins/CoursecatalogApp/css/catalog.css');
	if ($this->gridType == CoursesGrid::$GRIDTYPE_EXTERNAL) {
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/player-sprite.css');
	}
	
	echo '<div id="catalog-main-container" class="row-fluid">';

	//display the tree
	echo '<div id="catalog-categories-tree-container" class="span3 catalog-categories-tree-container">';
	echo '<div id="catalog-categories-label" class="title">'.Yii::t('reservation', '_CATEGORY_CAPTION').'</div>';
	$idCategoriesTree = 'catalog-categories-tree';
	$this->widget('common.extensions.fancytree.FancyTree', array(
		'id' => $idCategoriesTree,
		'treeData' => $_treeData,
		'fancyOptions' => array(
			'checkbox' => false,
		),
		'eventHandlers' => array(
			'focus' => 'function(e, d) { focusEvent(e, d); }',
			'renderNode' => 'function(e, d) { renderNodeEvent(e, d); }',
			'beforeSelect' => 'function(e, d) { selectEvent(e, d); }',
			'select' => 'function(e, d) { selectEvent(e, d); }',
		)
	));

	$topRatedCourses = LearningCourse::getTopRated();
	if (!empty($topRatedCourses) && (Settings::get('show_course_details_dedicated_page') =='on')): ?>
		<div class="course-details-sidebox">
			<h2><?= Yii::t('course', 'Top {count} rated courses', array('{count}'=>count($topRatedCourses))) ?></h2>
			<ul class="top-rated-courses clearfix">
				<?php
				$_topRatedIndex = 1;
				foreach ($topRatedCourses as $topRatedCourse) : ?>
					<li class="clearfix">
						<span class="index"><?= $_topRatedIndex ?></span>
						<span class="course-image">
							<img src="<?= $topRatedCourse->getCourseLogoUrl() ?>" class="">
						</span>
						<span class="course-title">
							<?php
								$already_subscribed = (!Yii::app()->user->getIsGuest() && LearningCourseuser::model()->isSubscribed(Yii::app()->user->id, $topRatedCourse->idCourse));
								if($already_subscribed){
									$topRatedCourseLink = Docebo::createAbsoluteUrl('lms:player', array('course_id' => $topRatedCourse->idCourse));
								}else{
									$topRatedCourseLink = Docebo::createAppUrl('lms:course/details', array('id' => $topRatedCourse->idCourse));
								}
							?>
							<a href="<?= $topRatedCourseLink ?>"><?= $topRatedCourse->name ?></a>
							<span class="course-rating">
								<?php
								$this->widget('common.widgets.DoceboStarRating',array(
									'id'=>'top_rated_course-rating-'.$topRatedCourse->idCourse,
									'name'=>'top_rated_course_rating_'.$topRatedCourse->idCourse,
									'type' => 'star',
									'readOnly' => true,
									'value' => $topRatedCourse->getRating(),
									'ratingUrl' => Docebo::createAppUrl('lms:course/axRate'),
									'urlParams' => array(
										'idCourse' => $topRatedCourse->idCourse
									)
								));
								?>
							</span>
						</span>
					</li>
					<?php
					$_topRatedIndex++;
				endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	</div>
	<div id="catalog-grid-container" class="span9">
<? } ?>


<!-- Here come all course portlets, loaded by an ajax call -->
<!-- <div id="courses-grid" class="tiles-grid"></div> -->
<ul id="courses-grid" class="thumbnails fixed tiles"></ul>
<div class="clearfix"></div>
<div id="grid-bottom-marker"></div>

<div class="ajaxloader" style="display: none;"><?= Yii::t("standard", "_LOADING") ?>...</div>

<?php if ($this->useLoadMoreButton) : ?>
	<!-- LOAD MORE BUTTON. Id='load-more' is the key!!!! -->
	<!-- Show only if allowed! -->
	<div id='load-more' class='row-fluid' class='load-more' style='display: none;'>
		<span class='btn span12'><a><?= Yii::t('standard', 'Load more results') ?></a></span>
	</div>
<?php endif; ?>

<?php if ($this->useScrollAndLoad) : ?>
	<!-- This element triggers loading next page when enter the view port (screen) -->
	<!-- KEEP THIS at the bottom of HTML -->
	<!-- Show only if allowed! -->
	<!--
	<div id='load-trigger'></div>
	 -->
<?php endif; ?>

<?php
if ($showCategoriesTree) {
	echo '</div>';	
}
?>
	 
<!-- --- JS ------------------------------------------------------------------------------------------------------ -->

<script type="text/javascript">
/*<![CDATA[*/

// Add loader indicator
// 'Global' variables
var FLT_LABEL = '<?= $this->flt_label ?>'; // filter My Courses by Label
var FLT_CATALOG = '<?= $this->flt_catalog ?>';  // filter Catalog by Catalog Id
var COURSES_GRID_SOURCE_URL = '<?= $this->coursesGridSourceUrl ?>';
var GRID_TYPE = '<?= $this->gridType ?>';
var USE_LOADMORE_BUTTON = <?= $this->useLoadMoreButton ? 'true' : 'false'?>;
var USE_SCROLL_AND_LOAD = <?= $this->useScrollAndLoad ? 'true' : 'false'?>;
var DUMMY_FILTER = { flt_query: '', flt_type: '', flt_label: FLT_LABEL, flt_catalog: FLT_CATALOG };
var ECOMMERCE_ENABLED = <?= $this->eCommerceEnabled ? 'true' : 'false' ?>;
var CATALOG_ENABLED = <?= $this->catalogEnabled ? 'true' : 'false' ?>;
var MORE_LINKS_AVAIL = <?= count($this->moreDropdownLinks) > 0 ? 'true' : 'false' ?>;

var currentPage = 0;
var pageCount = 0;



// New version of "is element in viewport checker"
// Not tested and not used at the moment
$.isElemInViewport = function(el) {
	var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document. documentElement.clientHeight) &&   /*or $(window).height() */
        rect.right <= (window.innerWidth || document. documentElement.clientWidth)   /*or $(window).width() */
        );	
}


// Define functions to handle scroll & load
// Calculate if an element (top of it) is in Viewport (i.e. visible on screen)
$.inViewport = function (elem) {
	var elemTop = $(elem).offset().top;
	var inside = (elemTop >= $(window).scrollTop()) && (elemTop <= ($(window).scrollTop()+$(window).height()));
	return inside;
}

// Event: When element enters the Viewport raise a semaphore (triggerFired) and run the callback.
$.fn.onEnterViewPort = function (callback) {
	var obj = this;
	$(window).scroll(function () {
		obj.each(function () {
			if ($.inViewport(this) && !$(this).data('triggerFired')) {
				$(this).data('triggerFired', true);
				callback(this);
			}
		});
	});
	return this;
}

// Event: When element leaves the Viewport 'release' the trigger 'semaphore' so next time it comes back to trigger it again
$.fn.onExitViewport = function (callback) {
	var obj = this;
	$(window).scroll(function () {
		obj.each(function () {
			if (!$.inViewport(this)) {
				$(this).data('triggerFired', false);
				callback(this);
			}
		});
	});
	return this;
}

// Makes an Ajax call to given URL
// Return an XHR object used later
function xhr_loadCoursesGrid(filter, page, grid_type, url) {
	// URL
	url = (typeof url !== 'undefined') ? url : COURSES_GRID_SOURCE_URL;

	// Request data object
	data = (typeof filter !== 'undefined') ? filter : DUMMY_FILTER;
	data.page = (typeof page !== 'undefined') ? parseInt(page) : 0;
	data.grid_type = (typeof grid_type !== 'undefined') ? grid_type : GRID_TYPE;

	var xhr = $.ajax({
		url: url,
		type: 'post',  // !! don't change
		dataType: 'json',
		data: data,
		beforeSend: function () {
			$('.ajaxloader').show();
		}
	}).always(function () {
			$('.ajaxloader').hide();
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
			if (console) {
				console.log(jqXHR);
				console.log(errorThrown);
			}
		});
	return xhr;
}

// Depending on different conditions (from AJAX response, etc.), some elements of the form may be shown/hidden
// By Default the whole search form is hidden (see grid_search_form.php)
function updateSearchFormElements(res) {
	hasForSale = res.has_for_sale;
	hasForFree = res.has_for_free;
	showViewAllTab = res.show_viewall_tab; // show "View All" button ? (Multi Catalog page)
	showCatalogTab = res.show_catalog_tab;

	gridSubtype = res.grid_subtype;

	// Initially: hide filter type dropdown
	$('#label-filter-dropdown').hide();

	if (GRID_TYPE == '<?= CoursesGrid::$GRIDTYPE_EXTERNAL ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-search").show();

		if (ECOMMERCE_ENABLED) {
			if (hasForSale && hasForFree) {
				$("#grid-filter-tabs").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_FULLLIST ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_PAID ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_FREE ?>").show();
			}
		}
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_MCATALOG ?>') {
		if (CATALOG_ENABLED) {
		}
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_MIXED ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-search").show();
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-tabs").show();
		$("#grid-filter-search").show();

		$('.inner-container #back-to-mixed').show();

		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_MYCOURSES ?>").show();
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_COMPLETED ?>").show();

		if (CATALOG_ENABLED) {
		}
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_LABELS ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-tabs").show();
		$("#grid-filter-search").show();

		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_MYCOURSES ?>").show();
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_COMPLETED ?>").show();
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_NOLABELS ?>").show();
		$('#label-filter-dropdown').show();

		if (CATALOG_ENABLED) {
		}
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-search").show();

		if (ECOMMERCE_ENABLED) {
			if (hasForSale && hasForFree) {
				$("#grid-filter-tabs").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_FULLLIST ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_PAID ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_FREE ?>").show();
			}
		}
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG_ONECATALOG ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-search").show();

		if (ECOMMERCE_ENABLED) {
			if (hasForSale && hasForFree) {
				$("#grid-filter-tabs").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_VIEWALL ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_PAID ?>").show();
				$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_FREE ?>").show();
			}
		}
		$("#grid-form-backs").show();
		$("#back-to-catalog").show();
	}

	else if (gridSubtype == '<?= CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_ONELABEL ?>') {
		$("#grid-search-form").show();
		$("#grid-filter-tabs").show();
		$("#grid-filter-search").show();

		// Change the item text
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_MYCOURSES ?> .item-text").text('<?= addslashes(Yii::t('standard', '_ALL')) ?>');
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_MYCOURSES ?>").show();

		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_COMPLETED ?>").show();
		$("#gsf-btn-type-<?= CoursesGrid::$FILTER_TYPE_NOLABELS ?>").hide();

		$("#grid-form-backs").show();
		$("#back-to-mycourses").show();

		if (CATALOG_ENABLED) {}
	}

	if (MORE_LINKS_AVAIL) {
		$("#grid-search-form").show();
		$("#grid-filter-tabs").show();
	}

	// Set "current/active" item as button content
	// First, try to get the first Active, Visible
	var el = $("#type-filter-dropdown li.active").filter(function () {
		return $(this).css("display") != "none";
	}).first();

	// If not, find first Visible at all
	if (!el.length) {
		el = $("#type-filter-dropdown li").filter(function () {
			return $(this).css("display") != "none";
		}).first();
	}

	if (el.length) {
		$("#type-filter-button").html(el.html());
	}

	var el2 = $("#label-filter-dropdown li.active").filter(function () {
		return $(this).css("display") != "none";
	}).first();

	// If not, find first Visible at all
	if (!el2.length) {
		el2 = $("#label-filter-dropdown li").filter(function () {
			return $(this).css("display") != "none";
		}).first();
	}

	if (el2.length) {
		$("#label-filter-button").html(el2.html());
	}
}

//Actions AFTER grid is loaded
function afterGridLoad(res, mode) {

	// Update current pagination info
	pageCount = res.page_count;
	currentPage = parseInt(res.current_page);
	gridSubtype = res.grid_subtype;

	// Grid types and subtypes, that must load as many times as it is required to flush (move out)
	// the "grid bottom marker" element out of the view port
	flushMarkerGridSubtypes = [
		'<?= CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG_ONECATALOG ?>',
		'<?= CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES_ONELABEL ?>'
	];

	flushMarkerGridTypes = [
		'<?= CoursesGrid::$GRIDTYPE_EXTERNAL ?>',
		'<?= CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES ?>',
		'<?= CoursesGrid::$GRIDTYPE_INTERNAL_CATALOG ?>'
	];

	switch (mode) {
		case 'append' : 	{
			$('#courses-grid').append(res.html);
		} break;
		case 'replace' : {
			$('#courses-grid').html(res.html);

		} break;
		default: {
			Docebo.Feedback.show('error', 'Unknown afterGridLoad mode: '+mode);
		}
	}

	if (res.override_mode != undefined) {
		if (res.override_mode == 'error') {
			Docebo.Feedback.show('error', res.error_html);
		}
	}
	

	// Lets see if current gridtype OR subtype is eligible to request "marker flush"
	doFlushMarker = ($.inArray(GRID_TYPE, flushMarkerGridTypes) >= 0) || ($.inArray(gridSubtype, flushMarkerGridSubtypes) >= 0);

	// Recursively call xhr_loadCoursesGrid+this function until marker is out of viewport.
	// Flushing is requested only if there are more pages of course
	if ($.inViewport("#grid-bottom-marker") && doFlushMarker && (currentPage < (pageCount-1))) {
		//if( $.inViewport("#grid-bottom-marker") && doFlushMarker ) {
		xhr_loadCoursesGrid(filter, currentPage+1).done(function (res) {
			afterGridLoad(res, 'append');
		});
	}

	// Set Load More button status (show/hide, if enabled at all)
	setLoadMoreButtonIf();

	// Update search form
	updateSearchFormElements(res);

	$(document).controls();

}

// Create an array of filter options using buttons, text input, etc.
// of the Grid Search Form
function composeFilterArray() {
	var filter = {};

	filter.flt_query = $('#gsf-search-text').val();  // search text
	// filter.flt_type = $('[id^="gsf-btn-type-"].active').data('type');  // course type
	filter.flt_type = $('input:radio[name=mycourse-type]:checked').val();
	filter.flt_catalog = FLT_CATALOG;
	// label filter may not exist
	if ($('#label-filter-dropdown').length && $('#label-filter-dropdown').is(':hidden')) {
		filter.flt_label = FLT_LABEL;
	} else {
		filter.flt_label = ( $('[id^="gsf-label-"].active').length ) ? $('[id^="gsf-label-"].active').data('type') : FLT_LABEL;
		FLT_LABEL = filter.flt_label;
	}

<?php if ($showCategoriesTree): ?>
	if (focusNode && (focusNode.key > 1)) { filter.flt_category = focusNode.key; }
<?php endif; ?>

	return filter;
}

//AJAX call to load the grid and show returned content
function doSearchAndLoad() {
	// do nothing if search is disabled
	/*if ($('#gsf-search-text').is(':disabled')) {
	 return false;
	 }*/

	// Reset page
	currentPage = 0;

	// Clear buttons status
	//$('[id^="gsf-btn-type-"]').removeClass('active');
	//$('[id^="gsf-label-"]').removeClass('active');

	// Get filter and load data
	filter = composeFilterArray();
	xhr_loadCoursesGrid(filter).done(function (res) {
		afterGridLoad(res, 'replace');
	});
}


//Show/Hide LOAD MORE button (IF!!)
function setLoadMoreButtonIf() {
	if (USE_LOADMORE_BUTTON && (currentPage < (pageCount-1) )) {
		$('#load-more').show();
	}
	else {
		$('#load-more').hide();
	}
}


//Document ready
$(function () {

	// Reposition "buy/subscribe to course dialog" vertically so it opens at the center
	// of the screen and the user doesn't have to scroll up if he is at the bottom of the page
	/*
	$(document).delegate(".course-details-modal", "dialog2.content-update", function() {
		var e = $(this);
		var modal = e.find('.modal-body');

		var modalWrapper = modal.closest('.modal');

		var currentDocumentScrollTop = $(document).scrollTop();
		var addTopMargin = ($(window).height()/2 - modalWrapper.height()/2);

		var totalMarginFromTop = (currentDocumentScrollTop + addTopMargin);

		modalWrapper.css('top', totalMarginFromTop);
	});
	*/

	$('#course-search input[type="radio"]').on("click", function (event) {
		//doSearchAndLoad();
		// Reset page
		currentPage = 0;
		filter = composeFilterArray();
		$('#courses-grid').html('');
		// Load (fresh) data into grid element
		xhr_loadCoursesGrid(filter).done(function (res) {
			afterGridLoad(res, 'replace');
		});
	});
	$('#course-search input[type="text"]').keypress(function (event) {
		if (event.which == 13) {
			event.stopPropagation();
			// Reset page
			currentPage = 0;
			filter = composeFilterArray();
			$('#courses-grid').html('');
			// Load (fresh) data into grid element
			xhr_loadCoursesGrid(filter).done(function (res) {
				afterGridLoad(res, 'replace');
			});
		}
	});
	$('#course-search').on("submit", function (event) {
		e.stopPropagation();
		// Reset page
		currentPage = 0;
		filter = composeFilterArray();
		$('#courses-grid').html('');
		// Load (fresh) data into grid element
		xhr_loadCoursesGrid(filter).done(function (res) {
			afterGridLoad(res, 'replace');
		});
	});

	// Fix fo tablets touch on dropdown-filter
	$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) {
		e.stopPropagation();
	});

	// Handle One-click button filtering (catalog, free, my courses, completed, etc.
	$('[id^="gsf-btn-type-"] li a').on('click', function (e) {
		e.preventDefault();

		// do nothing if button is disabled
		if ($(this).hasClass('disabled')) {
			return false;
		}

		// Reset page
		currentPage = 0;

		// Add 'active' class to the button (clear all buttons first)
		$('[id^="gsf-btn-type-"]').removeClass('active');
		$(this).addClass('active');

		// Clear search text field
		// Maybe we should keep the "query string" ??? (though not much intuitive)
		//$('#gsf-search-text').val(null);

		// Get the final filter
		filter = composeFilterArray();

		// Load (fresh) data into grid element
		xhr_loadCoursesGrid(filter).done(function (res) {
			afterGridLoad(res, 'replace');
		});

		// Toggle (assume "close") dropdown menu
		$("#type-filter-dropdown ul.dropdown-menu").dropdown("toggle");

	});

	// Handle One-click button filtering (catalog, free, my courses, completed, etc.
	$('li[id^="gsf-label-"]').on('click', function (e) {
		e.preventDefault();

		// do nothing if button is disabled
		if ($(this).hasClass('disabled')) {
			return false;
		}

		// Reset page
		currentPage = 0;

		// Add 'active' class to the button (clear all buttons first)
		$('[id^="gsf-label-"]').removeClass('active');
		$(this).addClass('active');


		// Get the final filter
		filter = composeFilterArray();

		// Load (fresh) data into grid element
		xhr_loadCoursesGrid(filter).done(function (res) {
			afterGridLoad(res, 'replace');
		});

		// Toggle (assume "close") dropdowdropdown menu
		$("#label-filter-dropdown ul.dropdown-menu").dropdown("toggle");
	});


	// Handle Search button click
	$('[id^="gsf-btn-search"]').on('click', function (event) {
		event.preventDefault();
		doSearchAndLoad();
	});

	$('#gsf-search-text').keypress(function (event) {
		if (event.which == 13) {
			doSearchAndLoad();
			return false;
		}
	});

	//x1

	// This binding is needed to 'release semaphore' when element is out of the screen
	$('#grid-bottom-marker').onExitViewport(function(){
		// Do NOT use this callback please! [plamen]
	});

	// Here is the work when element becomes part of the viewport (Scroll & Load)
	$('#grid-bottom-marker').onEnterViewPort(function () {

		if (!USE_SCROLL_AND_LOAD) {
			return;
		}

		// If last page already reached, get out
		if (currentPage >= (pageCount-1)) {
			return;
		}

		// Get filter
		filter = composeFilterArray();

		// next page, please
		currentPage++;

		// Load html and append to current one
		xhr_loadCoursesGrid(filter, currentPage).done(function (res) {
			afterGridLoad(res, 'append');
		});

	});


	//x2


	// Load More button clicked....
	$('#load-more').on('click', function () {
		// Get final filter
		filter = composeFilterArray();

		// next page, please
		currentPage++;

		// Load html and append to current one
		xhr_loadCoursesGrid(filter, currentPage).done(function (res) {
			afterGridLoad(res, 'append');
		});

	});

	// x3

	// Ajax call to get the course details when hovering over a course box
	var event = 'hover touchstart';

	$(document).on(event, '.tile.course', function (e) {
		if (e.type == 'mouseenter' || e.type == 'touchstart') {
			var $this = $(this);
			var courseInfo = $this.find('.course-hover-details');
			if (courseInfo.hasClass('empty')) {
				courseInfo.load($(this).attr('data-details-url'), function () {
					courseInfo.removeClass('empty');
					$(document).controls();
				});
			}
		}
	});


	// Ajax call to get courses grid upon page load, no filter
	filter = composeFilterArray();

	xhr_loadCoursesGrid(filter).done(function (res) {
		afterGridLoad(res, 'replace');
	});

});


// Ajax call to get courses grid upon page load, no filter
/*
filter = composeFilterArray();

xhr_loadCoursesGrid(filter).done(function (res) {
	afterGridLoad(res, 'replace');
});
*/

<?php if ($showCategoriesTree): ?>

var focusNode = null, focusIsLoading = false;
var focusEvent = function(event, data) {

	event.stopPropagation();
	
	if (!focusIsLoading) {
		focusNode = data.node;
		// Reset page
		currentPage = 0;
		filter = composeFilterArray();
		$('#courses-grid').html('');
		// Load (fresh) data into grid element
		focusIsLoading = true;
		xhr_loadCoursesGrid(filter).done(function (res) {
			afterGridLoad(res, 'replace');
			focusIsLoading = false;
		});
	}
};
var selectEvent = function(event, data) { if (focusIsLoading) { event.stopPropagation(); return false; } };

var renderNodeEvent = function(event, data) {
	var node = data.node;
	var $span = $(node.span);
	
	var $title = $span.find('.fancytree-title');
	if ($title) {
		var $folder = $title.find('.is-folder');
		if ($folder.length <= 0) {
			$title.prepend($('<span class="p-sprite is-folder"></span>'));
			$folder = $title.find('.is-folder');
		}
		if (node.expanded) {
			$folder.removeClass('closed');
			$folder.addClass('opened');
		} else {
			$folder.removeClass('opened');
			$folder.addClass('closed');
		}
	}
};


<?php endif; ?>

/*]]>*/
</script>
