<?php
/* @var $this OverlayHints */
?>

<script type="text/javascript">
//<![CDATA[
	var localBootstroLang = {
		bootstroMenuNoApps: '<?= CHtml::link(Yii::t('overlay_hints', 'Extend your Docebo with our <span class="text-colored green">Free apps</span> &amp; <span class="text-colored green">Integrations</span>{arrow}', array('{arrow}' => '')), Docebo::createLmsUrl('app/index')); ?>',

		// first step after installation
		bootstroAdmin: '<?= addslashes(Yii::t('overlay_hints', 'installer - Create your first course{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-w"></div>'))); ?>' +
			'<div class="overlay-hints-admin"><ul>' +
			'<li><?= addslashes(Yii::t('menu', '_DASHBOARD')); ?></li>' +
			'<li><?= addslashes(Yii::t('menu', '_USERS')); ?></li>' +
			'<li><?= addslashes(Yii::t('menu', '_GROUPS')); ?></li>' +
			'<li class="active"><?= addslashes(Yii::t('standard', '_COURSES')); ?></li>' +
			'<li><?= addslashes(Yii::t('menu', '_REPORTS')); ?></li>' +
			'<li><?= addslashes(Yii::t('menu', '_NEWSLETTER')); ?></li>' +
			'</ul></div>',

		bootstroMarketplace: '<?= addslashes(Yii::t('overlay_hints', 'try one of our featured course from the  marketplace{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-w"></div>'))); ?>',
		// in case of empty course management
		bootstroNewCourse: '<?= addslashes(Yii::t('overlay_hints', 'Create your first course{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-nw1"></div>'))); ?>',
		// empty training materials inside a course
		bootstroAddTrainingResources: '<?= addslashes(Yii::t('overlay_hints', 'Upload now one of your training materials{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-e1"></div>'))); ?>',
		bootstroAddBlockLauncher: '<?= addslashes(Yii::t('overlay_hints', 'Choose between different widgets{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-s"></div>'))); ?>',
		bootstroNewAppsDesign: '<?= addslashes(Yii::t('overlay_hints', '{arrow}Browse our APPs categories and Extend your Docebo', array('{arrow}'=>'<div class="bootstro-arrow arrow-nw"></div>'))); ?>',
		// after the first lo is created
		bootstroManageCourse: '<?= addslashes(Yii::t('overlay_hints', 'What\'s next? Go to the User Management{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-w"></div>'))); ?>',
		// empty enrollments area
		bootstroEnrollUsers: '<?= addslashes(Yii::t('overlay_hints', 'Enroll Users!{arrow}', array(
			'{arrow}'=>'<div class="bootstro-arrow arrow-nw"></div>',
			'{admin-link}'=>Docebo::createAppUrl('admin:userManagement/index'),
		))); ?>',

		// student, without course assigned
		bootstroNoCourses: '<?= addslashes(Yii::t('overlay_hints', 'It looks like you have no coures available yet!', array())); ?>',
		// suffix: no catalog available
		bootstroNoCoursesWithoutCatalog: '<?= addslashes(Yii::t('overlay_hints', 'Try again later', array())); ?>',
		// suffix:  catalog available
		bootstroNoCoursesWithCatalo: '<?= addslashes(Yii::t('overlay_hints', 'You may want to take a look at your catalog', array(
			'{catalog-link}'=>Docebo::createLmsUrl('site/index', array('opt' => 'catalog')),
		))); ?>',
		// studente, first time in the course
		bootstroCourseStructure: '<?= addslashes(Yii::t('overlay_hints', 'Click here to expand the course\'s table of contents{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-w"></div>'))); ?>',
		bootstroLaunchpad: '<?= addslashes(Yii::t('overlay_hints', 'Click here to start your course{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-s"></div>'))); ?>',
        bootstroConfigureSalesforce: '<?= addslashes(Yii::t('overlay_hints', 'Start here to configure your SF app for your first usage {arrow}', array('{arrow}'=>'<div class="bootstro-arrow bootstro-salesforce-arrow"></div>'))); ?>',
		bootstroAddSubtitle: '<?= addslashes(Yii::t('overlay_hints', 'Start here to configure your SF app for your first usage {arrow}', array('{arrow}'=>'<div class="bootstro-arrow bootstro-salesforce-arrow"></div>'))); ?>',
		bootstroNewContest: '<?= addslashes(Yii::t('overlay_hints', '<span class="text-large">Start here!</span><br />Create your first contest {arrow}', array('{arrow}'=>'<div class="bootstro-arrow bootstro-newcontest-arrow"></div>'))); ?>',
		bootstroAddLikertScaleSurveyQuestion: '<?= addslashes(Yii::t('overlay_hints', '<span class="text-large">Add here<br />your questions</span>{arrow}', array('{arrow}'=>'<div class="bootstro-arrow arrow-ne"></div>'))); ?>',
		bootstroAddLikertScaleSurveyScales: '<?= addslashes(Yii::t('overlay_hints', '{arrow}<span class="text-large">Define here<br />your likert scale</span>', array('{arrow}'=>'<div class="bootstro-arrow arrow-nw"></div>'))); ?>',
	};

	// Merge with any plugin added translations for Bootstros, taking care of duplicates
	var bootstroLang = $.extend(localBootstroLang, bootstroLang);


	var hints = [<?= implode(',', $this->hints) ?>];

	/*function showBootstroMarketplace(bootstro) {
		// we will need this to appear after the menu animation
		bootstro.start('bootstroAdmin', {}, {
			placement:'right',
			width:'375px'
		});
		bootstro.start('bootstroMarketplace', {}, {
			placement:'right',
			width:'330px'
		});
		$('#menu .tile:not(.sidebar-tile-admin,.sidebar-tile-marketplace)').css('opacity', '0.5');
	}*/

	$(document).ready(function(){

		// Merge any global hints with ones provided to this view
		var localhints = arrayUnique(hints.concat([<?= implode(',', $this->hints) ?>]));

		$.each(localhints, function(index, value) {

			switch (value) {
				case 'bootstroMarketplace':
//					setTimeout("showBootstroMarketplace( bootstro );", 1000);
				break;
				case 'bootstroMenuNoApps':
					$('#popup_bootstroMenuNoApps .popover-content').html(bootstroLang.bootstroMenuNoApps);
				break;
				case 'bootstroNewCourse':
					$('#course-management-form').css('opacity', '0');
					$('#content > div:not(.main-actions)').css('opacity', '0');
					bootstro.start('bootstroNewCourse', {
						onStopAll: function(b) {
							$('#course-management-form').fadeTo( 500 , 1);
							$('#content > div:not(.main-actions)').fadeTo( 500 , 1);
						}
					}, {
						placement:'bottom',
						width:'400px'
					});
				break;
				case 'bootstroAdmin':
					bootstro.start('bootstroAdmin', {}, {
						placement:'right',
						width:'600px'
					});
				break;
				case 'bootstroLaunchpad':
					// $('#player-courseheader-container .span7').css('opacity', '0');
					bootstro.start('bootstroLaunchpad', {
						onStopAll: function(b) {
							$('#player-courseheader-container .span7').fadeTo( 500 , 1);
						}
					}, {
						placement:'top',
						width:'400px'
					});
				break;
				case 'bootstroCourseStructure':
					// $('#player-courseheader-container .span7').css('opacity', '0');
					bootstro.start('bootstroCourseStructure', {
						onStopAll: function(b) {
							$('#player-courseheader-container .span7').fadeTo( 500 , 1);
						}
					}, {
						placement:'right',
						width:'600px'
					});
				break;
				case 'bootstroManageCourse':
					var ignoreUploadCompleteEvent = false;
					bootstroManageCourse = function() {
						if (ignoreUploadCompleteEvent) return;
						ignoreUploadCompleteEvent = true;
						bootstro.start('bootstroManageCourse', {}, {
							placement:'right',
							width:'990px'
						});
					};
					$(document).on('lo-upload-complete', bootstroManageCourse);
				break;
				case 'bootstroEnrollUsers':
					if($("#bootstroEnrollUsers").length > 0) {
						$('.bootstroEnrollUsers-hide-content').css('opacity', '0');
						bootstro.start('bootstroEnrollUsers', {
							onStopAll: function(b) {
								$('.bootstroEnrollUsers-hide-content').fadeTo( 500 , 1);
							}
						}, {
							placement:'bottom',
							width:'600px'
						});
					}
				break;
				case 'bootstroNewUser':
					bootstro.start('bootstroNewUser', {}, {
						placement:'bottom',
						width:'400px'
					});
				break;
				case 'bootstroViewCourse':
					bootstro.start('bootstroViewCourse', {}, {
						placement:'left',
						width:'400px'
					});
				break;
				case 'bootstroAddTrainingResources':
					setTimeout(function() {
						bootstro.start('bootstroAddTrainingResources', {}, {
							placement:'left',
							width:'450px'
						});
					}, 1000);
				break;
				case 'bootstroAddBlockLauncher':
					setTimeout(function() {
						bootstro.start('bootstroAddBlockLauncher', {}, {
							placement:'top',
							width:'320px'
						});
					}, 2500);
				break;
				case 'bootstroNewAppsDesign':
					/*  This is now triggered after loading the My Apps
						tab if no apps are found */
					break;
                case 'bootstroConfigureSalesforce':
                    bootstro.start('bootstroConfigureSalesforce', {}, {
                        placement:'top',
                        width:'300px'
                    });
                    break;

				case 'bootstroAddSubtitle':
					bootstro.start('bootstroAddSubtitle', {
						onStopAll: function(b) {
							$('#popup_bootstroAddSubtitle').fadeTo( 500 , 1);
					}
					}, {
						placement:'top',
						width:'370px'
					});
					break;
                case 'bootstroNewContest':
					$('#contest-content').css({opacity: '0'});
					bootstro.start('bootstroNewContest', {
                        onStopAll: function(b) {
                            $('#contest-content').fadeTo(500,1);
                        }
                    }, {placement:'bottom',float:'right',width:'600px'});
                break;

				case 'bootstroAddLikertScaleSurveyQuestion':
					$('div.leftContainer').css({opacity: '0', 'margin-bottom': '130px'});
					setTimeout(function() {
						bootstro.start('bootstroAddLikertScaleSurveyQuestion', {}, {
							placement:'bottom',
							width:'450px'
						});
					}, 1000);
					break;

				case 'bootstroAddLikertScaleSurveyScales':
					$('div.rightContainer').css({opacity: '0', 'margin-bottom': '130px'});
					setTimeout(function() {
						bootstro.start('bootstroAddLikertScaleSurveyScales', {
							onStopAll: function(b) {
								$('div.leftContainer').fadeTo( 500 , 1).css({'margin-bottom': '30px'});
								$('div.rightContainer').fadeTo( 500 , 1).css({'margin-bottom': '30px'});
							}
						}, {
							placement:'bottom',
							width:'450px'
						});
					}, 1000);
					break;

				default: break;
			}
		});

	});
//]>
</script>