<?php

/**
 * Utility component to execute video conversion/transcoding on demand, 
 * implementing Amazon Elastic Transcoder (http://aws.amazon.com/elastictranscoder/) 
 *   
 */
use Aws\ElasticTranscoder\ElasticTranscoderClient,
    Aws\Sns\SnsClient;

class VideoConverter extends CComponent {

    /**
     * File extension for converted files
     * @var string
     */
    const ET_VIDEO_EXTENSION = 'mp4';
    const HLS_ID = 'hls_id';

    /**
     * Elastic Transcoding Client
     * 
     * @var ElasticTranscoderClient
     */
    protected $etClient = null;

    /**
     * File Storage object calling this converter
     *  
     * @var CFileStorage
     */
    protected $storage = null;

    /**
     * Holds parameters passed to converter (see __constructr)
     * 
     * @var array
     */
    protected $params = array();

    /**
     * Amazon SNS topics used to deliver Job status notifications.
     * These are extracted from the preset pipeline by making a single call to Amazon ET
     * 
     * @var string
     */
    public $snsTopicProgressing = "";
    public $snsTopicCompleted = "";
    public $snsTopicWarning = "";
    public $snsTopicError = "";

    /**
     * Constructor
     * 
     * @param CFileStorage $storage
     * @param array $params (optional)  Free form, receiver method should know what is inside
     */
    public function __construct($storage, $params = array()) {

        // Set AWS Autoloader (use this trick to preserve Yii autoloader)
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('common.vendors.aws2.aws-autoloader', true);
        spl_autoload_register(array('YiiBase', 'autoload'));

        // Save Storage and Params in local attaributes
        $this->storage = $storage;
        $this->params = $params;

        // Call init method
        $this->init();
    }

    /**
     * Init method, called by __constructor() 
     */
    public function init() {

        // So far only S3 storage is supported. This may change in the future 
        if ($this->storage->type != CFileStorage::TYPE_S3) {
            throw new CException(Yii::t("standard", "Video transcoding is supported by S3 storage type only"));
        }

        // Do some initialization based on storage type
        if ($this->storage->type == CFileStorage::TYPE_S3) {
            $config = array(
                'key' => $this->storage->getS3Key(),
                'secret' => $this->storage->getS3Secret(),
                'region' => $this->storage->getS3Region(),
            );
            $this->etClient = ElasticTranscoderClient::factory($config);
            if (!$this->etClient) {
                throw new CException(Yii::t("standard", "Error creating Elastic Transcoder client."));
            }
        }

        // Read the pipline and save assigned Topics 
        $pipeline = $this->etClient->readPipeline(array('Id' => $this->storage->serviceParams['transcode_pipeline_id']));

        $this->snsTopicProgressing = isset($pipeline['Pipeline']['Notifications']['Progressing']) ? $pipeline['Pipeline']['Notifications']['Progressing'] : "";
        $this->snsTopicCompleted = isset($pipeline['Pipeline']['Notifications']['Completed']) ? $pipeline['Pipeline']['Notifications']['Completed'] : "";
        $this->snsTopicWarning = isset($pipeline['Pipeline']['Notifications']['Warning']) ? $pipeline['Pipeline']['Notifications']['Warning'] : "";
        $this->snsTopicError = isset($pipeline['Pipeline']['Notifications']['Error']) ? $pipeline['Pipeline']['Notifications']['Error'] : "";
    }

    /**
     * Update parameters of the pipeline we are using
     *  
     * @param array $params
     */
    public function updatePipeline($params) {

        $pipelineId = isset($params['PipelineId']) ? $params['PipelineId'] : $this->storage->serviceParams['transcode_pipeline_id'];

        $config = array(
            'Id' => $pipelineId,
            'Notifications' => array(),
        );
    }

    /**
     * Start conversion. Use proper one, depending on storage type
     */
    public function runTranscoding() {
        $result = false;
        switch ($this->storage->type) {
            case CFileStorage::TYPE_S3:
                $result = $this->runElasticTranscoderJob();
                break;

            case CFileStorage::TYPE_FILESYSTEM:
            default:
                throw new CException(Yii::t("standard", "Video transcoding is NOT supported. Check your cofniguration files and settings."));
                break;
        }
        return $result;
    }

    /**
     * Start Amazon Elastic Transcoder job
     * 
     * @param string $pipelineId  Optional PipelineId to use, different from the Storage calling this component
     * @throws CException
     * @return boolean
     */
    protected function runElasticTranscoderJob($pipelineId = false) {
        if (!$pipelineId) {
            $pipelineId = $this->storage->serviceParams['transcode_pipeline_id'];
        }

        // ET parameters
        $inputKey = $this->params['inputKey'];   //  ex. files/tmp/video_in/somefile.ext  
        $outputKey = $this->params['outputKey'];  //  ex. files/l/m/lms.docebo.com/video/8b7c23a8ccb47f9bb9b390a6f92b809d82454918.mp4
        $outputFile = pathinfo($outputKey, PATHINFO_BASENAME);   // 8b7c23a8ccb47f9bb9b390a6f92b809d82454918.mp4
        $transcodePresedId = $this->storage->serviceParams['transcode_preset_id'];  // The OLD one, single preset
        // File name without extension 
        $fileName = pathinfo($outputKey, PATHINFO_FILENAME);  // 8b7c23a8ccb47f9bb9b390a6f92b809d82454918
        // We create a subfolder in S3 "video" collection, having the same name as the filename (8b7c23a8ccb47f9bb9b390a6f92b809d82454918)
        // In that folder we put all converted materials (HLS, playlists, thumbs, mp4 etc.)
        $subFolderName = $fileName;

        // So, the prefix is now something like (NOTE THE TRAILING SLASH! VERY IMPORTANT!!!!!!!!!)
        // files/l/m/lms.docebo.com/8b7c23a8ccb47f9bb9b390a6f92b809d82454918/
        $outputKeyPrefix = pathinfo($outputKey, PATHINFO_DIRNAME) . "/";

        // Build array of transcoding OUTPUTS
        $outputs = array();
        $forPlayList = array();  // To be used in the Master Playlist
        // Enumerate pre-chosen PRESETS (see common/config/parts/params.php
        foreach ($this->storage->serviceParams['transcode_presets'] as $code => $presetId) {
            $output = array();
            // HLS variants ? ('h', 'n', 'l')   (High quality, Normal quality, Low quality) 
            if ($code != 'g') {
                $output['SegmentDuration'] = 5;
                $output['Key'] = $subFolderName . "/" . $fileName . "_" . $code . "_";
                $output['PresetId'] = $presetId;
                // We need ONLY ONE thumbnails set, based on 'h' (high quality)
                if ($code == 'h') {
                    $output['ThumbnailPattern'] = $subFolderName . "/thumbs/" . $fileName . "_" . "{count}";
                }
                // We inlcude in the master playlist ONLY HLS variants
                $forPlayList[] = $output['Key'];
            }
            // MP4 variant ? (g),   (General 1080i)
            else {
                $output['Key'] = $subFolderName . "/" . $outputFile;
                $output['PresetId'] = $presetId;
            }
            $outputs[] = $output;
        }

        // Backward compatibility:: (THIS SHOULD BE DELETED after we release the HLS/flowplayer revision of the LMS)
        // Also create an MP4 file in "video" folder level (not in the subfolder). This is the old way to save and play videos
        // @TODO:  When a VIDEO LO is deleted, only THIS file is deleted (7 Sep 2015): Please check the LearningVideo cleanup procedure
        // to delete also the SUB-folder
        $outputs[] = array(
            'Key' => $outputFile,
            'PresetId' => $transcodePresedId,
        );

        // Make list of master PlayLists. We use ONLY one.
        $playLists = array(
            array(
                'Name' => $subFolderName . "/" . $fileName,
                'Format' => 'HLSv3',
                'OutputKeys' => $forPlayList,
            ),
        );


        // Finally, Job config
        $jobConfig = array(
            'PipelineId' => $pipelineId,
            'Input' => array(
                'Key' => ltrim($inputKey, '/'),
            ),
            'OutputKeyPrefix' => rtrim(ltrim($outputKeyPrefix, "/"), "/") . "/", // make sure we have TRAILING slash and NO prefix slash
            'Outputs' => $outputs,
            'Playlists' => $playLists,
        );


        // Subscribe URL to Amazon SNS topic(s)
        $url = AmazonSnsHelper::getSnsTranscodingEndpoint($fileName, $this->storage->getCollection());
        $result1 = AmazonSnsHelper::subscribeUrlToTopic(
                $this->storage->getS3Key(), $this->storage->getS3Secret(), $this->storage->getS3Region(), $url, $this->snsTopicCompleted);

        $result2 = AmazonSnsHelper::subscribeUrlToTopic(
                $this->storage->getS3Key(), $this->storage->getS3Secret(), $this->storage->getS3Region(), $url, $this->snsTopicError);

        if (!$result1 || !$result2) {
            throw new CException(Yii::t('standard', 'Error while trying to subscribe an endpoint to Amazon SNS topic.'));
        }

        // Create the job and get response
        $job = $this->etClient->createJob($jobConfig);

        // Analyze response
        if (!get_class($job) == 'Guzzle\Service\Resource\Model') {
            throw new CException("Invalid response object from S3 storage (Guzzle\Service\Resource\Model expected)");
        }

        if ($job['Job']['Status'] != 'Error') {
            Yii::log('Elastic Transcoder Job successfully submitted. Pipeline Id: ' . $job['Job']['PipelineId'] . ', Job Id: ' . $job['Job']['Id'], CLogger::LEVEL_INFO);
        } else {
            throw new CException("Elastic Transcoder Job returned status 'ERROR'.");
        }


        return true;
    }

    /**
     * Return status of a specific Elastic Transcoder Job
     * 
     * @param string $jobId
     * @return string  Submitted|Progressing|Complete|Canceled|Error
     */
    public function getElasticJobStatus($jobId) {
        $job = $this->readElasticJob($jobId);
        return $job['Job']['Status'];
    }

    /**
     * Read an Elastic Transcoder Job and return an Info array.
     * Use this to POLL Elastic Transcoder and get job status
     *  
     * @param string $jobId
     * @throws CException
     * 
     * @return array
     */
    public function readElasticJob($jobId) {
        $config = array(
            'Id' => $jobId,
        );
        $job = $this->etClient->readJob($config);
        if (!$job) {
            throw new CException(Yii::t("standard", "Error while reading Elastic Transcoder job."));
        }
        return $job;
    }

    /**
     * Return list of JOBS ran/running in the storage pipeline
     * By default, the fixed Pipeline ID is used (pre-created and pre-configured).
     * Optionally, an aribitrary pipeline Id can be passed.
     *  
     * @param string $pipelineId  Optional PipelineId to use, different from the Storage calling this component
     * @throws CException
     * @return array
     */
    public function getElasticJobsList($pipelineId = false) {
        if (!$pipelineId) {
            $pipelineId = $this->storage->serviceParams['transcode_pipeline_id'];
        }
        $config = array(
            'PipelineId' => $pipelineId,
        );
        $jobsList = $this->etClient->listJobsByPipeline($config);
        if (!$jobsList) {
            throw new CException(Yii::t("standard", "Error while reading list of Elastic Transcoder jobs."));
        }
        return $jobsList;
    }

}
