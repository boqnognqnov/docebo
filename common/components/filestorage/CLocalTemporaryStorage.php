<?php
/**
 * A storage for temporary stuff. Just like CFileSystemStorage, but in a different base folder ("lmstmp")
 *
 */
class CLocalTemporaryStorage extends CFileSystemStorage {
	/**
	 * Constructor
	 *
	 * @param struct $collection
	 */
	public function __construct($collection = self::COLLECTION_UPLOADS, $params = array()) {
		
		$params['filesBaseRelPath'] = Yii::app()->params['tempFolder']; // e.g.  "lmstmp"
		parent::__construct($collection, $params);
		
		$this->type = CFileStorage::TYPE_LOCAL_TEMP;
	}
	
	
}