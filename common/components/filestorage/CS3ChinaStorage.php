<?php
use Aws\S3\S3Client;

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 15.12.2015 г.
 * Time: 13:43 ч.
 */
class CS3ChinaStorage extends CS3Storage {

	// Holds S3 Client
	private $_client = NULL;

	/**
	 * Constructor
	 *
	 * @param string $collection
	 * @param array  $params
	 */
	public function __construct ( $collection = self::COLLECTION_COMMON, $params = array() ) {
		parent::__construct( $collection, $params );

		$this->type = self::TYPE_S3_CHINA;

		// Load credentials and other storage parameters
		$this->getParameters();

		// Create and save AWS S3 client
		$this->_client = $this->getClient();

		if(isset(Yii::app()->params['cdn']['distributions']['cdn-china'])){
			$this->cdn = Yii::app()->params['cdn']['distributions']['cdn-china'];
		}
	}

	/**
	 * Create, save, and return an S3 client
	 *
	 * @return S3Client object
	 */
	protected function getClient () {

		// Set AWS Autoloader (use this trick to preserve Yii autoloader)
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import('common.vendors.aws2.aws-autoloader', true);
		spl_autoload_register(array('YiiBase','autoload'));

		static $cacheClients = array();

		$config = array(
			'key'          => $this->key,
			'secret'       => $this->secret,
			'curl.options' => $this->curlOptions,
			//'base_url' => $this->baseUrl, // Needed to be COMMENTED for Amazon China, otherwise breaks
			'region'       => $this->region, // Needed for Amazon China, otherwise breaks
			'validation'   => FALSE,
		);

		// Cache key
		$key = $this->key . $this->secret . $this->baseUrl;

		if( isset( $cacheClients[ $key ] ) ) {
			$this->_client = $cacheClients[ $key ];

			return $this->_client;
		}

		$this->_client        = S3Client::factory( $config );
		$cacheClients[ $key ] = $this->_client;

		return $this->_client;

	}
}