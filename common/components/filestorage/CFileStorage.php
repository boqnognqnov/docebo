<?php
/**
 * Provides an abstraction layer and API to manipulate files in different LMS file storage types.
 * Supported types are S3 and FILE SYSTEM.

 * Methods returning URL(s) are capable to detect if Collection should be served through CDN and uses
 * CDN configuration to build the URL using CDN domain, bucket name and so on.
 *
 * Storage type used for given Collection is configured in the Yii main-*.php configuration file.
 *
 * Collections served by CDN are also listed in that configuration file.
 *
 * It is perfectly possible to use S3 and File system, CDN and non-CDN in a mixed fashion,
 * e.g. SCORM and TinCan using S3 and CDN,  FILE LO uses S3 and NO CDN, etc.
 *
 * Example Usages:
 *
 * 	<i>Save /tmp/myvideo.mp4 into VIDEO collection of the current LMS's files container:</i>
 *
 * 		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
 * 		$localFilePath = "/tmp/myvideo.mp4";
 * 		$storage->store($localFilePath);
 *
 *  <i>Remove "somefile.txt" from FILES LO collection, from S3 storage of the running LMS:</i>
 *  (Note: not recommended; better use getDomainStorage which knows better where LMS Collections are stored):
 *
 *  	$storage = CFileStorage::getS3DomainStorage(CFileStorage::COLLECTION_LO_FILE);
 *  	$storage->remove("somefile.txt");
 *
 *  <i>Get URL to "index.html" in FILES LO collection:</i>
 *
 *  	$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_FILE);
 *  	$url = $storage->fileUrl("index.html");
 *  	// Result:  http://some.domain.com/some/path/to/index.html
 *
 *
 * @see IFileStorage for available public methods.
 *
 *
 */
abstract class CFileStorage extends CComponent implements IFileStorage {


	// File storage type
	const TYPE_FILESYSTEM 	= 'filesystem'; 		// local file system
	const TYPE_LOCAL_TEMP	= 'localtemp'; 			// on local file system, but in different location (i.e. "lmstmp")
	const TYPE_S3 		  	= 's3'; 				// S3
	const TYPE_S3_CHINA		= 's3_china';			// Chinese Amazon S3 is separate from AWS Global
	const TYPE_DEFAULT	 	= 'filesystem';


	// Default main 'files' relative path
	// @TODO Make this Storage type dependent, like Collection subfolders
	const DEFAULT_FILES_BASE_REL_PATH = 'files';

	// Generic resource type constants, i.e. Collections
	// This generic type will determine the base sub-folder (under the absoluteBasePath folder).

	const COLLECTION_LO_SCORM 			= 'lo_scorm';    	// doceboLms/scorm
	const COLLECTION_LO_AICC 			= 'lo_aicc';    	// doceboLms/scorm
	const COLLECTION_LO_FILE 			= 'lo_file';    	// doceboLms/file
	const COLLECTION_LO_VIDEO 			= 'lo_video';    	// doceboLms/video
	const COLLECTION_LO_AUTHORING		= 'lo_authoring';	// doceboLms/authoring
	const COLLECTION_LO_TINCAN 			= 'lo_tincan';    	// doceboLms/tincan
	const COLLECTION_LO_DELIVERABLE 	= 'lo_deliverable';	// lo_deliverable

	const COLLECTION_LT_LOCATION 		= 'lt_location'; // classrooms location files
	const COLLECTION_LT_EVALUATION 		= 'lt_evaluation'; // classrooms evaluation files

	const COLLECTION_USERFILES          = 'userfiles';     // userimages/[userID]
	
	const COLLECTION_CERTIFICATE        = 'certificate';  // User's issued certificates (PDF files)

	const COLLECTION_LO_RESOURCE 		= 'lo_resource';    // doceboLms
	const COLLECTION_COURSEDOCS 		= 'coursedocs';    	// doceboLms/course,  course documents
	const COLLECTION_LO_TEST 			= 'lo_test';    	// doceboLms/test
	const COLLECTION_COURSE_LOGO 		= 'courselogo';     // like logo files
	const COLLECTION_MP_COURSE_LOGO 	= 'mp_courselogo';  // marketplace courses logos
	const COLLECTION_COMMON				= 'common';			// common
	const COLLECTION_CORE				= 'core';			// doceboCore
	const COLLECTION_CORE_FIELD			= 'core_field';		// doceboCore/field
	const COLLECTION_AVATARS			= 'avatars';		// doceboCore/photo

	const COLLECTION_UPLOADS 			= 'uploads';  		//
	
	const COLLECTION_ASSETS 			= 'assets';  		// Various GLOBAL or LMS specific assets (images, documents, etc.)

    const COLLECTION_FTP	            = 'ftp';            // Ftp exchange folder

	const COLLECTION_NONE 				= 'none';  			// these kind of files go to root (absolute base path)

	// Collections are below "catalogo_docebo", which is the marketplace "container"
	const COLLECTION_MP_SCORM			= 'mp_scorm';  //
	const COLLECTION_MP_TEST			= 'mp_test';  //
	const COLLECTION_MP_TINCAN			= 'mp_tincan';  //
	const COLLECTION_MP_THUMBS_COURSES	= 'mp_thumbscourses';  //
	const COLLECTION_MP_THUMBS_PARTNERS	= 'mp_thumbspartners';  //

	const COLLECTION_SIMPLESAML 		= 'simplesaml'; // simplesaml uploads

	// Webinar recordings
	const COLLECTION_WEBINAR_RECORDING  = 'webinar_recording';

	// We have TWO different general storage configurations possible
	const STORAGE_CONFIG_STANDARD		= 'standard_storage';
	const STORAGE_CONFIG_MARKETPLACE    = 'marketplace_storage';
	const STORAGE_CONFIG_AMAZON_CHINA 	= 'china_storage'; // Only used by AWSChinaApp at the moment
	const COLLECTION_7020               = 'app7020';
	const COLLECTION_WEBAPP             = 'webapp';

	/**
	 * User ID which this file storage might be related to (like COLLECTION_USERFILES)
	 * @var number
	 */
	public $idUser;

	/**
	 * Key of the general storage config to use for this storage. See main-xxx.php, General storage configuration.
	 * Caller MAY request different ones
	 * @var string
	 */
	protected $storageConfig = self::STORAGE_CONFIG_STANDARD;

	/**
	 * Transcode/Convert VIDEO files or not or not?
	 * Note 'transcoding/conversion' is not necessarly S3 only process! It may happen we have other storages providing some sort of transcoding
	 *
	 * @var boolean
	 */
	public $transcodeVideo = false;

	/**
	 * Folder path, relative to e.g. "files". Where UPLOADED or temporary files that are about to be converted are stored at first place
	 * @var string
	 */
	public $transcodeVideoIncomingFolder = '';

	/**
	 * Allow the storage class instantiator to force plugins to
	 * not be able to override the default behavior of the file storage
	 * (e.g. if set to true, plugins will not be able to override the fileUrl()
	 * method and return their own S3 URL)
	 * @var bool
	 */
	public $ignoreEvents = false;

	// Maps Collection to Subfolder for different storage types
	// Just as an added flexibility.
	protected static $collectionSubfolders = array(

		self::TYPE_S3 => array(
			self::COLLECTION_LO_SCORM 			=> "scorm",
			self::COLLECTION_LO_AICC 			=> "aicc",
			self::COLLECTION_LO_FILE 			=> "item",
			self::COLLECTION_LO_VIDEO 			=> "video",
			self::COLLECTION_LO_AUTHORING		=> "authoring",
			self::COLLECTION_LO_TINCAN 			=> "tincan",
			self::COLLECTION_LO_DELIVERABLE 	=> 'deliverable',

			self::COLLECTION_LT_LOCATION 		=> 'lt_location',
			self::COLLECTION_LT_EVALUATION 		=> 'lt_evaluation',

			self::COLLECTION_USERFILES         	=> "userfiles",
				
			self::COLLECTION_CERTIFICATE        => 'certificate',

			self::COLLECTION_LO_RESOURCE 		=> "doceboLms",
			self::COLLECTION_COURSEDOCS 		=> "coursedocs",
			self::COLLECTION_LO_TEST 			=> "test",
			self::COLLECTION_COURSE_LOGO 		=> "course_logos",
			self::COLLECTION_MP_COURSE_LOGO 	=> "mp_course_logos",
			self::COLLECTION_COMMON  			=> "common",
			self::COLLECTION_CORE 				=> "doceboCore",
			self::COLLECTION_CORE_FIELD 		=> "field",
			self::COLLECTION_AVATARS 			=> "avatar",
			self::COLLECTION_UPLOADS 			=> "upload_tmp",
			self::COLLECTION_ASSETS				=> "assets",
            self::COLLECTION_FTP				=> "ftp",
			self::COLLECTION_NONE 				=> "",

			self::COLLECTION_MP_SCORM			=> 'scorm',
			self::COLLECTION_MP_TEST			=> 'test',
			self::COLLECTION_MP_TINCAN			=> 'tincan',
			self::COLLECTION_MP_THUMBS_COURSES	=> 'thumbnails/courses',
			self::COLLECTION_MP_THUMBS_PARTNERS	=> 'thumbnails/partners',

            self::COLLECTION_SIMPLESAML         => 'simplesaml',
			self::COLLECTION_7020               => 'app7020',
			self::COLLECTION_WEBAPP             => 'webapp'
		),

		// Basically a replica of the above array:
		self::TYPE_S3_CHINA => array(
			self::COLLECTION_LO_SCORM 			=> "scorm",
			self::COLLECTION_LO_AICC 			=> "aicc",
			self::COLLECTION_LO_FILE 			=> "item",
			self::COLLECTION_LO_VIDEO 			=> "video",
			self::COLLECTION_LO_AUTHORING		=> "authoring",
			self::COLLECTION_LO_TINCAN 			=> "tincan",
			self::COLLECTION_LO_DELIVERABLE 	=> 'deliverable',

			self::COLLECTION_LT_LOCATION 		=> 'lt_location',
			self::COLLECTION_LT_EVALUATION 		=> 'lt_evaluation',

			self::COLLECTION_USERFILES         	=> "userfiles",

			self::COLLECTION_CERTIFICATE        => 'certificate',

			self::COLLECTION_LO_RESOURCE 		=> "doceboLms",
			self::COLLECTION_COURSEDOCS 		=> "coursedocs",
			self::COLLECTION_LO_TEST 			=> "test",
			self::COLLECTION_COURSE_LOGO 		=> "course_logos",
			self::COLLECTION_MP_COURSE_LOGO 	=> "mp_course_logos",
			self::COLLECTION_COMMON  			=> "common",
			self::COLLECTION_CORE 				=> "doceboCore",
			self::COLLECTION_CORE_FIELD 		=> "field",
			self::COLLECTION_AVATARS 			=> "avatar",
			self::COLLECTION_UPLOADS 			=> "upload_tmp",
			self::COLLECTION_ASSETS				=> "assets",
			self::COLLECTION_FTP				=> "ftp",
			self::COLLECTION_NONE 				=> "",

			self::COLLECTION_MP_SCORM			=> 'scorm',
			self::COLLECTION_MP_TEST			=> 'test',
			self::COLLECTION_MP_TINCAN			=> 'tincan',
			self::COLLECTION_MP_THUMBS_COURSES	=> 'thumbnails/courses',
			self::COLLECTION_MP_THUMBS_PARTNERS	=> 'thumbnails/partners',

			self::COLLECTION_SIMPLESAML         => 'simplesaml',
			self::COLLECTION_7020               => 'app7020',
			self::COLLECTION_WEBAPP             => 'webapp'
		),

		self::TYPE_FILESYSTEM => array(
			self::COLLECTION_LO_RESOURCE 		=> "doceboLms",
			self::COLLECTION_LO_SCORM 			=> "doceboLms/scorm",
			self::COLLECTION_LO_AICC 			=> "doceboLms/aicc",
			self::COLLECTION_LO_FILE 			=> "doceboLms/item",
			self::COLLECTION_LO_VIDEO 			=> "doceboLms/video",
			self::COLLECTION_LO_AUTHORING		=> "doceboLms/authoring",
			self::COLLECTION_COURSEDOCS 		=> "doceboLms/course",
			self::COLLECTION_LO_TINCAN 			=> "doceboLms/tincan",
			self::COLLECTION_LO_TEST 			=> "doceboLms/test",
			self::COLLECTION_LO_DELIVERABLE 	=> 'deliverable',
			self::COLLECTION_COURSE_LOGO 		=> "course_logos",
			self::COLLECTION_MP_COURSE_LOGO 	=> "mp_course_logos",
			self::COLLECTION_COMMON  			=> "common",
			self::COLLECTION_CORE 				=> "doceboCore",
			self::COLLECTION_CORE_FIELD 		=> "doceboCore/field",
			self::COLLECTION_AVATARS 			=> "doceboCore/photo",

			self::COLLECTION_LT_LOCATION 		=> 'lt_location',
			self::COLLECTION_LT_EVALUATION 		=> 'lt_evaluation',

			self::COLLECTION_UPLOADS 			=> "upload_tmp",
			self::COLLECTION_ASSETS				=> "assets",
			self::COLLECTION_USERFILES         	=> "userfiles",
			self::COLLECTION_CERTIFICATE        => 'doceboLms/certificate',
            self::COLLECTION_FTP                => 'ftp',
			self::COLLECTION_NONE 				=> "",

			// This is faking; we are NOT going to have Marketplace into local file system; never ever; but we must have this; just for.. completeness :-)
			self::COLLECTION_MP_SCORM			=> 'mp/scorm/partner',
			self::COLLECTION_MP_TEST			=> 'mp/test/partner',
			self::COLLECTION_MP_TINCAN			=> 'mp/tincan/partner',
			self::COLLECTION_MP_THUMBS_COURSES	=> 'mp/thumbnails/courses',
			self::COLLECTION_MP_THUMBS_PARTNERS	=> 'mp/thumbnails/partners',

            self::COLLECTION_SIMPLESAML         => 'simplesaml',
			self::COLLECTION_7020               => 'app7020',
			self::COLLECTION_WEBAPP             => 'webapp'
		),
			
		self::TYPE_LOCAL_TEMP => array(
			self::COLLECTION_UPLOADS			=> 'upload_tmp',	
		),
			
			
	);



	// Storage Manager instance type (set by descendant constructors)
	public $type = "";  // TYPE_FILESYSTEM, TYPE_S3, ...

	// Holds the object's collection name
	protected $collection = "";

	// Absolute path to root of the storage, i.e.  /var/www/vhost/mydomain/files
	public  $absoluteBasePath = "";

	// Respresents different COLLECTION_*s   (doceboLms, doceboCore, common..., doceboCore/photo, ... )
	public $subFolder = "";

	// Absolute URI to 'files', like http://my.domain.com/files,    http://mybucket.s3.amazonaws.com/files, ...
	protected $absoluteBaseUrl = "";

	// Relative path to 'super container', e.g.  'files'; NO SLASH!
	protected $filesBaseRelPath = "";  // relative to bucket

	// Holds Container, e.g.:  m/y/my_domain_code
	protected $container = false;

	// Holds CDN configuration array, if any; read from Yii config OR from LMS configuration/settings
	protected $cdn = false;
	
	protected $cdnBackup = false;

	/**
	 * Class Constructor
	 * @param string $collection
	 * @param array $params
	 */
	public function __construct($collection = self::COLLECTION_NONE, $params=array()) {

		if (isset($params['idUser']) && $params['idUser'] > 0) {
			$this->idUser = $params['idUser'];
		}

		// Set general storage config to use (from Yii params); defaults "the standard storage"
		// Caller may request another one (available: standard_storage, marketplace_storage)
		$this->storageConfig = isset($params['storage_config']) ? $params['storage_config'] : self::STORAGE_CONFIG_STANDARD;

		//if (!in_array($this->storageConfig, array(self::STORAGE_CONFIG_STANDARD, self::STORAGE_CONFIG_MARKETPLACE))) {
		if (!isset(Yii::app()->params[$this->storageConfig])) {
			throw new CException("Unknown general storage confguration requested: " . $this->storageConfig);
		}

		/**
		 * Create http_response_code() function if it does not exists (for 4.3.0 <= PHP <= 5.4.0)
		 */
		if (!function_exists('http_response_code')) {
			function http_response_code($newcode = NULL) {
				static $code = 200;
				if ($newcode !== NULL) {
					header('X-PHP-Response-Code: '.$newcode, true, $newcode);
					if (!headers_sent())
						$code = $newcode;
				}
				return $code;
			}
		}

		// Set the base relative path to this storage object  (e.g. 'files'). NO SLASH!
		$this->filesBaseRelPath = isset(Yii::app()->params[$this->storageConfig]['filesBaseRelPath']) ? Yii::app()->params[$this->storageConfig]['filesBaseRelPath'] : self::DEFAULT_FILES_BASE_REL_PATH;
		
		// ... OR, if there is an incoming parameter with this value...
		if (isset($params['filesBaseRelPath'])) {
			$this->filesBaseRelPath = $params['filesBaseRelPath'];
		}

		// Set domain container, if any.
		// If container is not specified, "collections" will result directly below base relative path,
		// e.g.  'files/doceboLms', rather than 'files/my_domainn_code_container/doceboLms'
		if (isset($params['container'])) {
			$this->container = $params['container'];
		}

		// Save collection to class attribute
		$this->collection = $collection;

		// Read and Load Content Distribution Network configuration
		$this->resolveCdn();
	}


	/**
	 * Returns subfolder (below $absoluteBasePath) for THIS object's collection
	 * Depends on Storage type (s3, filesystem, ...)
	 *
	 */
	protected function getCollectionSubfolder($params = array()) {
		
		$subfolder = "";
		
		// Check if there is a subfolder set for the collection of this object
		if (isset(self::$collectionSubfolders[$this->type][$this->collection])) {
			$subfolder = self::$collectionSubfolders[$this->type][$this->collection];
			return $subfolder;
		}
		else {
			$subfolder = $this->collection;
			/*
			// I wonder if we should throw this exception, but otherwise unexpected results may happen
			// Like writing files in wrong location!
			throw new CHttpException(404, "Unknown Storage Collection Name ($this->collection)");
			*/
		}
		
		return $subfolder;
	}


	/**
	 * Read LMS specific CDN configuration
	 *
	 * @return boolean  If LMS configuration has been loaded or not
	 */
	protected function getLmsCdnConfig() {
		return false;
	}


	/**
	 * @see IFileStorage::getHostUrl()
	 */
	public function getHostUrl($noSlash=false) {
		$url_parsed = parse_url($this->fileUrl('dummy.txt', ''));
		return $url_parsed['scheme'] . '://' . $url_parsed['host'] . ($noSlash ? '' : '/');
	}


	/**
	 * Read & Load CDN configuration. LMS takes precedence
	 */
	protected function resolveCdn() {

		// First, try to load LMS specific CDN config. If loaded, return
		$cdn = $this->getLmsCdnConfig();
		if ($cdn) {
			$this->cdn = $cdn;
			return;
		}

		// If NO LMS CDN is found, check Yii config

		// NOTE, yes, plenty of checks, but we need them to be sure we have valid config.

		// We must have these! 
		if (!isset(Yii::app()->params['cdn'])) return;

		// List of collections using CDN; the rest use direct read from storage. Must be an array.
		if (!isset(Yii::app()->params['cdn']['collections']) || !is_array(Yii::app()->params['cdn']['collections'])) return;

		// We need definitions of CDNs (distributions)
		if (!isset(Yii::app()->params['cdn']['distributions']) ||  !is_array(Yii::app()->params['cdn']['distributions']) || count(Yii::app()->params['cdn']['distributions']) <= 0) return;
		
		// If current object's collection is not in the enabled list, get out
		if (!key_exists($this->collection, Yii::app()->params['cdn']['collections'])) return;
		
		// Get the CDN distribution name
		$cdnName = Yii::app()->params['cdn']['collections'][$this->collection];
		
		// Unknown CDN distribution?
		if (!isset(Yii::app()->params['cdn']['distributions'][$cdnName])) return;

		
		// Well well well... seems we have something useful
		$cdn = Yii::app()->params['cdn']['distributions'][$cdnName];

		// Assign the resulting CDN config to this object
		$this->cdn = $cdn;

	}


	/**
	 * Load LMS specific collection => storage type map, if any.
	 * User can define which collection goes where.
	 *
	 */
	protected static function getLmsCollectionStorageTypes() {
		/*
		// -- Debug >>
		$result = array(
			'lo_file' 			=> 's3',
			'lo_scorm'			=> 's3',
			'lo_video'			=> 's3',
			'lo_tincan' 		=> 's3',
		);
		return $result;
		// -- Debug <<
		 */

		$result = array();
		// This setting must be JSON
		$lmsSetiing = Settings::get('collection_storage_types', false);
		if (!$lmsSetiing) {
			return false;
		}
		$result = CJSON::decode($lmsSetiing);
		return $result;
	}


	/**
	 * Different collections MAY reside in different storage. Get the storage type of the given collection.
	 * Reads data from in Yii->params[] AND from LMS settings
	 *
	 * @param string $collection
	 * @return string  Default = local filesystem
	 */
	protected static function getCollectionStorageType($collection, $storageConfig = false) {
		$lmsCollectionMap = self::getLmsCollectionStorageTypes();

		$storageConfig = $storageConfig ?  $storageConfig : self::STORAGE_CONFIG_STANDARD;

		$res = false;
		if (! is_array($lmsCollectionMap)) {
			if (isset(Yii::app()->params[$storageConfig]['collectionToStorageTypeMap'][$collection])) {
				$res = Yii::app()->params[$storageConfig]['collectionToStorageTypeMap'][$collection];
			}
		}
		else {
			if (isset($lmsCollectionMap[$collection])) {
				$res = $lmsCollectionMap[$collection];
			}
		}
		return $res ? $res : self::TYPE_DEFAULT;
	}


	/**
	 * Build object retriever compliant signature: envelope to sign is = <token><domain-code><expire-at-epoch>
	 *
	 * @param number $expireAt Seconds since epoch, when the signature expires. Rounded to an hour
	 * @return string
	 */
	protected function getCdnSignature($expireAt) {
		$domainCode = Docebo::getOriginalDomainCode();
		$envelope = $this->cdn['params']['token']  . $domainCode . $expireAt;
		$signature = str_replace('=', '',strtr(base64_encode(md5($envelope, TRUE)), '+/', '-_'));
		return $signature;
	}


	/**
	 * Signature made using different ebvelope:  <token><this-container><expire-at-epoch>
	 *
	 * @param number $expireAt
	 * @return string
	 */
	protected function getCdnSignatureV2($expireAt) {
		$envelope = $this->cdn['params']['token']  . $this->container . $expireAt;
		$signature = str_replace('=', '',strtr(base64_encode(md5($envelope, TRUE)), '+/', '-_'));
		return $signature;
	}



	/**
	 * Returns the appropriate file storage manager instance
	 *
	 * @param string $collection (COLLECTION_LO_RESOURCE, ...)
	 * @param bool $type Storage type; FALSE to derive storage type from Collection
	 * @param array $params Additional parameters
	 *
	 * @return CFileSystemStorage|CS3Storage
	 * @throws CException
	 */
	protected static function getFileStorageManager($collection = self::COLLECTION_NONE, $type=false, $params = array())
	{

		// If caller did not specify the Storage type, derive it from requested collection
		// This allows per-collection storage type, e.g. For SCORMs use S3, for Course documents: filesystem, etc.
		if (!$type) {
			$storageConfig = isset($params['storage_config']) ? $params['storage_config'] : false;
			$type = self::getCollectionStorageType($collection, $storageConfig);
		}

		switch ($type) {
			case self::TYPE_S3:
				if ($collection == CFileStorage::COLLECTION_7020) {
					$object = new CS3StorageSt($collection, $params);
				} else {
					$object = new CS3Storage($collection, $params);
				}
				break;
			case self::TYPE_S3_CHINA:
				$object = new CS3ChinaStorage($collection, $params);
				break;
			case self::TYPE_FILESYSTEM:
				$object = new CFileSystemStorage($collection, $params);
				break;
			case self::TYPE_LOCAL_TEMP:
				$object = new CLocalTemporaryStorage($collection, $params);
				break;
				
			default:
				throw new CException("Unknown file storage type requested");
			break;
		}

		$object->subFolder = $object->getCollectionSubfolder($params);

		return $object;
	}


	/**
	 * URI to domain container
	 *
	 * @return string
	 */
	protected function getAbsoluteBaseUrl() {
		return $this->absoluteBaseUrl;
	}



	/**
	 * Path to domain container.
	 * For file system storage, it will be just the absolute path
	 * For S3, it will the path that starts from the bucket
	 *
	 * @return string
	 */
	protected function getAbsoluteBasePath() {
		return $this->absoluteBasePath;
	}

	/**
	 * @see IFileStorage::path()
	 */
	public function path(){
		$separator = $this->type == self::TYPE_S3 ?  CS3Storage::S3_GROUPING_DELIMITER  : DIRECTORY_SEPARATOR;
		return $this->getAbsoluteBasePath() .(empty($this->subFolder) ? '' : $separator . $this->subFolder );
	}

	/**
	 * @see IFileStorage::containerPath()
	 */
	public function containerPath(){
		return $this->absoluteBasePath;
	}


	/**
	 * Return CDN URL to a file. It does not depend on the physiscal storage type (s3 or filesystem)
	 *
	 * @param string $filename
	 * @param string $customSubfolder
	 * @return string
	 */
	protected function fileCdnUrl($filename, $customSubfolder = "") {

		// If we don't have CDN config loaded... well.. this is bad...
		if (!$this->cdn) {
			return false;
		}

		$hoursAhead 	= $this->cdn['params']['expire_period'];
		$expireAt 		= Yii::app()->localtime->nextRoundHourTime($hoursAhead);
		$httpSchema 	= $this->cdn['params']['http_schema'];

		if(is_null($httpSchema)) $httpSchema = 'http';

		// if the lms is running in https let's force https for the contents
		if (Yii::app()->request->getIsSecureConnection()) $httpSchema = 'https';

		$domain 		= $this->cdn['params']['domain'];
		$signatureType  = isset($this->cdn['params']['signature_type']) ? $this->cdn['params']['signature_type'] : 1;



		// !!!!! NOTE !!!!!: The "signature" we are implementing is NOT the Amazon URL signing!
		// This is our own signature policy, related to NGINX proxy we use to route CDN requests (like static.docebo.com)!
		// Ask Fabio/Andrea/Plamen

		// Build signature dpending on signature type (if any, because it could be 'false')
		switch ($signatureType) {

			case 1: // Standard URL, signed
				$signature = $this->getCdnSignature($expireAt);
				break;

			case 2:  // Marketplace URL, signed
				$signature = $this->getCdnSignatureV2($expireAt);
				break;

			case false:
			default:
				$signature = false;
		}

		// Should we sign or not? (see main configuration file)
		if ($signature === false) {   // NO
			// Example: http[s]://bucket-name.s3storage-service.com/files/m/y/my_domain_com/doceboLms/vide/somevideo.mp4
			$url =
			"{$httpSchema}://{$domain}"
			.  	$this->absoluteBasePath
			.  	($this->subFolder ? "/" . $this->subFolder : "")
			.  ($customSubfolder ? "/" . $customSubfolder : "")
			. "/" . $filename;
		}
		else {  // YES
			// Example: http[s]://bucket-name.s3storage-service.com/files/m/y/my_domain_com/<expire-time-epoch>/<signature>/video/somevideo.mp4
			$url =
			"{$httpSchema}://{$domain}"
				.  	$this->absoluteBasePath
				. 	"/{$expireAt}"
				. 	"/" . $signature
				.  	($this->subFolder ? "/" . $this->subFolder : "")
				.  ($customSubfolder ? "/" . $customSubfolder : "")
				. "/" . $filename;
		}


		return $url;

	}


	/**
	 * Shortcut method to get S3 Storage manager for given collection
	 *
	 * @param string  $collection
	 *
	 * @return CS3Storage
	 */
	public static function getS3DomainStorage($collection, $storageConfig = false) {
		$domainCode = Docebo::getOriginalDomainCode();
		$params = array('container' => $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode);
		if ($storageConfig) {
			$params['storage_config'] = $storageConfig;
		}
		return self::getFileStorageManager($collection, self::TYPE_S3, $params);
	}

	/**
	 * Shortcut method to get S3 Storage manager for given collection
	 *
	 * @param string  $collection
	 *
	 * @return CS3Storage
	 */
	public static function getS3ChinaDomainStorage($collection, $storageConfig = false) {
		$domainCode = Docebo::getOriginalDomainCode();
		$params = array('container' => $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode);
		if ($storageConfig) {
			$params['storage_config'] = $storageConfig;
		}
		return self::getFileStorageManager($collection, false, $params);
	}

	/**
	 * Shortcut method to get Local filesystem Storage manager for given collection
	 *
	 * @param string  $collection
	 *
	 * @return CFileSystemStorage
	 */
	public static function getLocalDomainStorage($collection, $storageConfig = false) {
		$domainCode = Docebo::getOriginalDomainCode();
		$params = array('container' => $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode);
		if ($storageConfig) {
			$params['storage_config'] = $storageConfig;
		}
		return self::getFileStorageManager($collection, self::TYPE_FILESYSTEM, $params);
	}

	
	/**
	 * 
	 * @param string $collection
	 * @return CFileSystemStorage|CS3Storage
	 */
	public static function getLocalTempDomainStorage($collection, $storageConfig = false) {
		$domainCode = Docebo::getOriginalDomainCode();
		$params = array('container' => $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode);
		if ($storageConfig) {
			$params['storage_config'] = $storageConfig;
		}
		return self::getFileStorageManager($collection, self::TYPE_LOCAL_TEMP, $params);
	}
	

	/**
	 * Shortcut to get a Marketplace storage. It is very specific and does override some types/values; always S3/CDN type
	 *
	 * At the end of the day, this storage provides access to:
	 * 		<mp-bucket>/<config-filesBaseRelPath>/catalogo_docebo/<collection-subfolder>/partner/<partner-id>/courses/<course-id>/<LO-id>
	 *
	 * Note: Collection is
	 *
	 * @param string $collection  Collection identifier, defines the LO-type base path to ALL partners's LO's of this type
	 * @param string $loPath Path to partner/course/LoId
	 * @return CS3Storage
	 */
	public static function getMarketplaceStorage($collection, $loPath) {

		// Using "collection" path and "lo path" to form the "container"
		$container = 'catalogo_docebo/' . self::$collectionSubfolders[self::TYPE_S3][$collection] . "/" . $loPath;

		// Sepcify that we NEED to use this very storage config:  marketplace_storage (see config file)
		$params = array('container' => $container, 'storage_config' => self::STORAGE_CONFIG_MARKETPLACE);

		// Since we moved "collection" path to container (up), now collection is actually empty, none
		$storage = self::getFileStorageManager(self::COLLECTION_NONE, self::TYPE_S3, $params);

		// Marketplace storage is always s3
		$storage->type = self::TYPE_S3;

		// Use this very specifi CDN configuration
		$storage->cdn = Yii::app()->params['cdn']['distributions']['marketplace'];

		// We are done.
		return $storage;
	}


	/**
	 * Shortcut to get a domain based collection storage.
	 * The type of storage is derived from Yii->params[]. Sefault would be LOCAL Filesystem.
	 *
	 * @param string $collection
	 */
	/**
	 * 
	 * @param string $collection
	 * @param string $storageConfig  Array key of Yii app params[<storageConfig>] describing top level storage info, like bucket name, relative path, etc.
	 *  
	 * @throws CException
	 * @return CS3Storage|CS3ChinaStorage|CFileSystemStorage
	 */
	public static function getDomainStorage($collection, $storageConfig = false) {
		// Get this collection storage type. Fallback to default.
		$type = self::getCollectionStorageType($collection, $storageConfig);
		if (!$type) {
			$type = self::TYPE_DEFAULT;
		}

		// Allow plugins to override the type or the file storage class
		$fileStorageHandler = null;
		Yii::app()->event->raise('onBeforeGetDomainStorage', new DEvent(self, array(
			'type'=>&$type,
			'fileStorageHandler'=>&$fileStorageHandler,

			'collection'=>$collection,
			'storageConfig'=>$storageConfig,
		)));

		// Plugin has overriden the file storage class
		if($fileStorageHandler && $fileStorageHandler instanceof CFileStorage){
			return $fileStorageHandler;
		}

		switch ($type) {
			case self::TYPE_S3:
				$fileStorageHandler = self::getS3DomainStorage($collection, $storageConfig);
				break;
			case self::TYPE_S3_CHINA:
				$fileStorageHandler = self::getS3ChinaDomainStorage($collection, $storageConfig);
				break;
			case self::TYPE_FILESYSTEM:
				$fileStorageHandler = self::getLocalDomainStorage($collection, $storageConfig);
				break;
			case self::TYPE_LOCAL_TEMP:
				$fileStorageHandler = self::getLocalTempDomainStorage($collection, $storageConfig);
				break;
				
				
			default:
				throw new CException("Unknown file storage type requested");
				break;
		}

		return $fileStorageHandler;

	}

	
	/**
	 * Shortcut to get a storage like "<host>/files/<my_collection>" of selected type.
	 * Note: NOT domain specific, but system wide "files" folder 
	 * 
	 * @param string $collection Named collection (see constants in this class) 
	 * @param string $type Storage type
	 * @return CFileStorage
	 */
	public static function getSystemFilesStorage($collection, $type=false) {
		$params = array('container' => "");
		return self::getFileStorageManager($collection, $type, $params);
	}
	
	/**
	 * @see IFileStorage::getVideoWhitelistType()
	 */
	public function getVideoWhitelistType() {
		switch ($this->type) {
			case self::TYPE_S3:
				// If Storage is S3 and Transcoding is enabled, allow extended list of uploaded file types
				if ($this->transcodeVideo)
					$wlType = FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST;
				else
					$wlType = FileTypeValidator::TYPE_VIDEO;
				break;
			default:
				$wlType = FileTypeValidator::TYPE_VIDEO;
				break;
		}

		return $wlType;

	}


	
	/**
	 * @see IFileStorage::getVideoWhitelistArray()
	 */
	public function getVideoWhitelistArray() {
		return FileTypeValidator::getWhiteListArray($this->getVideoWhitelistType());
	}



	/**
	 * @see IFileStorage::uri() 
	 */
	public function uri($noSlash=true) {
		$tmp = $this->fileUrl('', '');
		if ($noSlash)
			$tmp = rtrim($tmp,'/');	
		return $tmp;
	}

	/**
	 * @see IFileStorage::disableCdn()
	 */
	public function disableCdn() {
		$this->cdnBackup = $this->cdn;
		$this->cdn = false;		
	}

	/**
	 * @see IFileStorage::enableCdn()
	 */
	public function enableCdn() {
		// Do not destroy orignaly loaded configuration in case cdnBackup is empty.
		if ($this->cdnBackup) {
			$this->cdn = $this->cdnBackup;
			$this->cdnBackup = false;
		}
	}


	public function getCollection() {
		return $this->collection;
	}


	/**
	 * Abstract methods. Must be implemented in descendant classes
	 */

// 	abstract public function store($sourcePath, $customSubfolder = "", $params = array());
// 	abstract public function storeAs($sourcePath, $filename, $customSubfolder = "", $params = array());
// 	abstract public function remove($filename, $customSubfolder = "");
// 	abstract public function removeFolder($folderName);
// 	abstract public function fileExists($filename, $customSubfolder = "");
// 	abstract public function fileUrl($filename, $customSubfolder = "");
// 	abstract public function fileSize($filename, $customSubfolder="");
// 	abstract public function sendFile($filename, $displayFileName = "", $customSubfolder = "");
// 	abstract public function getFileInfo($filename, $customSubfolder = "");
// 	abstract public function rename($filename, $newname, $customSubfolder = "");
// 	abstract public function filesBasePath();
// 	abstract public function findFiles($subFolder='', $options);
//  abstract public function disableCdn();
//  abstract public function enableCdn();
	
}
