<?php
/**
 *
 * S3 File storage
 * Based on AWS SDK for PHP v2  (http://aws.amazon.com/sdkforphp2/)
 *
 *
 */

use Aws\S3\Sync\UploadSyncBuilder;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Guzzle\Service\Exception\CommandTransferException;
use Guzzle\Http\EntityBody;
use Guzzle\Http\Url;

class CS3Storage extends CFileStorage
{

	/**
	 * A delimiter is a character used to group keys in S3 storage.
	 *
	 * @var character
	 */
	const S3_GROUPING_DELIMITER = "/";


	//S3 related
	protected $key = null;
	protected $secret = null;
	protected $region = null;
	protected $bucket_name = null;
	protected $curlOptions = array();
	protected $baseUrl = null;           // e.g. 'http://seewebstorage.it'
	protected $defaultAcl = 'public-read';
	protected $url_expire_period = false;



	// Holds S3 Client
	private  $_client = null;


	// S3 Storage service parameters (see main--xxx.php:  s3Storages array)
	public $serviceParams = array();


	/**
	 * Constructor
	 *
	 * @param string $collection
	 * @param array $params
	 */
	public function __construct($collection = self::COLLECTION_COMMON, $params = array()) {

		parent::__construct($collection, $params);

		// Set AWS Autoloader (use this trick to preserve Yii autoloader)
		spl_autoload_unregister(array('YiiBase','autoload'));
		Yii::import('common.vendors.aws2.aws-autoloader', true);
		spl_autoload_register(array('YiiBase','autoload'));

		// Storage type: S3
		$this->type = parent::TYPE_S3;

		// Load credentials and other storage parameters
		$this->getParameters();

		// Create and save AWS S3 client
		$this->_client = $this->getClient();

		// Register a stream wrapper, so we can use s3://<bucket><object-key> notation and requests
		$this->_client->registerStreamWrapper();

		// Absolute base path to container  (Ex:  /files/m/y/my_domain_code  OR  /files/my_domain_code)
		$this->absoluteBasePath =
				( !empty($this->filesBaseRelPath) ?  "/" . $this->filesBaseRelPath : "") .
				( $this->container  ? "/" . $this->container : "");

		// And set the absolute URL to the contaner (E.g.:   http://my-bucket.s3.amazonaws.com/files/m/y/my_domain_code)
		$url = Url::factory($this->_client->getBaseUrl());
		$url->setHost($this->bucket_name . "." . $url->getHost());
		$this->absoluteBaseUrl = $url . $this->absoluteBasePath;   // the second includes "/"

	}


	/**
	 * Create, save, and return an S3 client
	 *
	 * @return S3Client object
	 */
	protected function getClient() {

	    static $cacheClients = array();

	    $config = array(
	        'key'    => $this->key,
	        'secret' => $this->secret,
	        'curl.options' => $this->curlOptions,
	        'base_url' => $this->baseUrl,
	        'validation' => false,
	    );

	    // Cache key
	    $key =  $this->key . $this->secret . $this->baseUrl;

	    if (isset($cacheClients[$key])) {
	        $this->_client = $cacheClients[$key];
	        return $this->_client;
	    }

	    $this->_client = S3Client::factory($config);
	    $cacheClients[$key] = $this->_client;

	    return $this->_client;

	}


	/**
	 * Get the per-LMS S3 storage
	 *
	 * @todo Decide where from (core_setting, Redis database, ...)
	 *
	 * @return boolean|array
	 */
	protected function getLmsS3StorageService($generalStorage) {
		return false;
	}



	/**
	 * Read S3 related parameteres
	 *
	 * Also could be taken from LMS database (core_settings), Redis database, whatever...
	 *
	 */
	protected function getParameters() {

		// Any LMS specific settings for the requested "General storage"?
		$serviceParams = $this->getLmsS3StorageService($this->storageConfig);

		// If not, get our own from main-xxx.php Yii configuration
		if ($serviceParams === false) {
			$s3StorageService 	= Yii::app()->params[$this->storageConfig]['s3DefaultStorage'];
			$serviceParams 		= Yii::app()->params['s3Storages'][$s3StorageService];
		}

		// First, hold all S3 service parameters in a class variable; just in case
		$this->serviceParams 				= $serviceParams;

		// Now, separate parameters into separate class variable, for easy usage only
		$this->key 							= $serviceParams['key'];
		$this->secret 						= $serviceParams['secret'];
		$this->region 						= $serviceParams['region'];
		$this->bucket_name 					= $serviceParams['bucket_name'];
		$this->curlOptions 					= $serviceParams['curl_options'];
		$this->baseUrl 						= $serviceParams['base_url'];
		$this->defaultAcl 					= $serviceParams['defaultAcl'];
		$this->url_expire_period 			= $serviceParams['url_expire_period'];
		$this->transcodeVideo 				= isset($serviceParams['transcode_video']) ? $serviceParams['transcode_video'] : false;
		$this->transcodeVideoIncomingFolder = isset($serviceParams['transcode_video_incoming_folder']) ? $serviceParams['transcode_video_incoming_folder'] : 'tmp/video_in';


	}



	/**
	 * Read S3 object (file) and send it to the browser
	 *
	 * @param string $filename  The file name to send. NOTE! Not a path! Just filename taken from our database tables
	 * @param string $displayFileName  Give this name to the downloaded file
	 * @param string $customSubfolder  A custom subdirectory inside the collection
	 */
	private function readObject($filename, $displayFileName = "", $customSubfolder = "") {

		// Resolve the doanload name
		if (empty($displayFileName)) {
			$displayFileName = $filename;
		}

		// S3 Key of the file
		$fileKey = $this->getFileKey($filename, $customSubfolder);

		// S3 Object
		$object = "s3://" . $this->bucket_name . $fileKey;

		// Prepare options
		$options = array(
			'Bucket' => $this->bucket_name,
			'Key' => $fileKey,
		);

		// Check if the client sent the If-None-Match header
		if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
			$options['IfNoneMatch'] = $_SERVER['HTTP_IF_NONE_MATCH'];
		}

		// Check if the client sent the If-Modified-Since header
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
			$options['IfModifiedSince'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
		}

		// Prepare client command to get info about the S3 object
		$command = $this->_client->getCommand('HeadObject', $options);

		// Execute command
		try {
			$response = $command->getResponse();
		} catch (Aws\S3\Exception\S3Exception $e) {
			http_response_code(404);
			exit;
		}

		// Just pass through the status code
		$statusCode = $response->getStatusCode();
		http_response_code($statusCode);

		// Selectively pass through (proxy) other headers coming from S3 server
		$proxyHeaders = array(
				'Last-Modified',
				'ETag',
				'Content-Type',
				'Content-Length',
		);

		$headers = $response->getHeaders();
		foreach ($proxyHeaders as $header) {
			if ($response->hasHeader($header)) {
				header("{$header}: " . $response->getHeader($header));
			}
		}


		/* Option1: Downloading the file and send to user: BAD!
		$fileContent = file_get_contents($object);
		Yii::app()->request->sendFile($filename, $fileContent, NULL, false, $displayFileName);
		return true;
		*/


		/* Option2:  read and send to user: Good! */

		if ($statusCode == 200) {
			header('Pragma: public');
			header('Expires: 0');
			header("Content-Disposition: attachment; filename=\"$displayFileName\"");

			// Mind the cache and maintain it
			if (ob_get_level()) {
				ob_end_flush();
			}
			flush();

			// Read the object from S3 server and send it to the client browser.
			readfile($object);
		}


	}


	/**
	 * Replace slashes with the default S3 grouping delimiter
	 *
	 * @param string $prefix
	 * @return string
	 */
	private function normalizeDelimiter($prefix) {
		return str_replace(array('\\','/'), self::S3_GROUPING_DELIMITER, $prefix);

	}


	/**
	 * Return S3 file key inside this storage object (/files/<container>/<collection>/<custom_subfolder>/filename.ext)
	 *
	 * @param string $filename
	 * @param string $customSubfolder Optional subdirectory inside the storage object
	 */
	public function getFileKey($filename, $customSubfolder = "") {
		$key = $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "")  . ($customSubfolder ? "/" . $customSubfolder: "") . "/" . $filename;

		// Encode S3 key into UTF in case 'bad' Unicode characters are used in filenames (well, they are NOT bad, just not UTF8).
		// Example: Unicode(ó)=F3, but UTF8(ó)=C3B3.
		// If F3 is left in the key, S3 will not be happy and return InvalidUriException (during upload)
		// and Key Not found, during download/requests
		if (strtolower(mb_detect_encoding($key,mb_detect_order(),true)) != 'utf-8') {
			$key = utf8_encode($key);
		}
		return $key;
	}


	/**
	 * Get key prefix of an Object. Returns the Key with NO prefix slash.
	 * The difference between Key and Key Prefix is... the missing "/" at the begining of the string
	 *
	 * @param string $filename
	 * @param string $customSubfolder
	 * @return string
	 */
	protected function getFileKeyPrefix($filename, $customSubfolder = "") {
		$key= $this->getFileKey($filename, $customSubfolder);
		return ltrim($key,'/');
	}



	/**
	 * Get key prefix for the collection and optional custom subfolder. Returns with NO prefix slash.
	 *
	 * @param string $customSubfolder
	 * @return string
	 */
	protected function getCollectionKeyPrefix($customSubfolder = "") {
		$key = $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "")  . ($customSubfolder ? "/" . $customSubfolder: "");
		return ltrim($key,'/');
	}


	/**
	 * Recursively upload folder content (unlimited number of files)
	 *
	 * @param string $sourcePath Folder to upload
	 * @param string $targetFolderName Optional target folder name (effectively renaming uploaded folder)
	 * @param string $customSubfolder  Optionsl Subfolder below collection
	 * @param array $params Optional additional AWS S3 specific options
	 * @return S3Client
	 */
	protected function uploadFolder($sourcePath, $targetFolderName = "", $customSubfolder = "", $acl = "private", $params = array()) {

		// If we are going to rename the folder during upload...
		if (!empty($targetFolderName)) {

			$targetFolderName = $this->normalizeDelimiter($targetFolderName);

			$base_dir = $sourcePath;
			$keyPrefix = $this->getCollectionKeyPrefix($customSubfolder) . "/" . $targetFolderName;
		}
		else {
			$base_dir = dirname($sourcePath);
			$keyPrefix = $this->getCollectionKeyPrefix($customSubfolder);
		}

		// We need to set our own key converter so that we can UTF-encode filenames during FOLDER uploads
		$builder = UploadSyncBuilder::getInstance();
		$builder->setSourceFilenameConverter(new S3KeyConverter($base_dir, $keyPrefix));

		// Upload files
		try {
			$this->_client->uploadDirectory(
				$sourcePath,
				$this->bucket_name,
				$keyPrefix,
				array(
					'params' => array(
						'ACL' => $acl,
					),
					'force'		=> true,  // overwrite exisitng objects
					'base_dir' 	=> $base_dir,
					'builder'	=> $builder,
				)
			);
		}
		catch (CommandTransferException $e) {
			$succeeded = $e->getSuccessfulCommands();
			Yii::log("S3Storage/AWS Failed Commands (uploadFolder). See next log messages.", "error", "aws-s3");
			foreach ($e->getFailedCommands() as $failedCommand) {
				Yii::log($e->getExceptionForFailedCommand($failedCommand)->getMessage(), "error", "aws-s3");
			}
		}


		return true;

	}



	/**
	 * ! KEEP THIS FOR AWHILE! Do not delete!
	 * Recursively upload folder content (unlimited number of files)
	 *
	 * @param string $sourcePath Folder to upload
	 * @param string $targetFolderName Optional target folder name (effectively renaming uploaded folder)
	 * @param string $customSubfolder  Optionsl Subfolder below collection
	 * @param array $params Optional additional AWS S3 specific options
	 * @return S3Client
	 */
	protected function uploadFolder_NEW($sourcePath, $targetFolderName = "", $customSubfolder = "", $acl = "private", $params = array()) {

		// If we are going to rename the folder during upload...
		if (!empty($targetFolderName)) {
			$targetFolderName = $this->normalizeDelimiter($targetFolderName);
			$base_dir = $sourcePath;
			$finalSubfolder = $customSubfolder . "/" . $targetFolderName;
		}
		else {
			$base_dir = dirname($sourcePath);
			$finalSubfolder = $customSubfolder;
		}


		$directory = new RecursiveDirectoryIterator($sourcePath,
				FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS | FilesystemIterator::FOLLOW_SYMLINKS);
		$iterator = new RecursiveIteratorIterator($directory);
		foreach ($iterator as $name => $splObject) {
			if ($splObject instanceof SplFileInfo) {
				if ($splObject->isFile()) {
					$fileName = $splObject->getBasename();
					$path = $splObject->getPath();
					if (mb_substr($path, 0, mb_strlen($base_dir)) == $base_dir) {
						$relativePath = trim(mb_substr($path, mb_strlen($base_dir)),"/\\");
					}
					$finalSubfolder = trim($finalSubfolder,"/\\");
					$this->store($splObject->getPathname(), $finalSubfolder . "/" . $relativePath);
				}
			}
		}


		return true;

	}



	/**
	 * Helper method to check credentials, if bucket exists and other pre-checks
	 *
	 * Good to be called before do anything else.
	 *
	 * @throws CException
	 * @return boolean
	 */
	protected function storageHealthCheck() {

		$client = $this->getClient();

		// Do health checks
		if ($client->doesBucketExist($this->bucket_name, false) === false) {
			throw new CException('Requested S3 bucket does not exist');
		}

		return true;

	}


	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::filesBasePath()
	 */
	public function filesBasePath() {
		return (!empty($this->filesBaseRelPath) ?  "/" . $this->filesBaseRelPath : "");
	}


	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::removeFolder()
	 */
	public function removeFolder($folderName)  {

	    // It is dangerous to delete an EMPTY folder - it will delete the whole collection!!
	    $folderName = trim($folderName);
	    if (!is_string($folderName) || empty($folderName)) {
	        return true;
	    }
	    
		$keyPrefix = $this->getCollectionKeyPrefix($folderName);
		$keyPrefix = trim($keyPrefix);

		// Be paranoic! Avoid accidental deleting of folder NOT belonging to this collection
		if (empty($keyPrefix)) {
			return false;
		}

		// Key prefix with NO subfolders, just the collection
		$baseCollectionKeyPrefix = $this->getCollectionKeyPrefix();
		$good = mb_ereg_match("^{$baseCollectionKeyPrefix}", $keyPrefix);
		if (!$good) {
			return false;
		}

		$deletedCount = $this->_client->deleteMatchingObjects($this->bucket_name, $keyPrefix, '/.*/');

		return true;
	}


	/**
	 * Store (upload) local file to S3 storage
	 *
	 * (non-PHPdoc)
	 * @see IFileStorage::store()
	 */
	public function store($sourcePath, $customSubfolder = "", $params = array())
	{
		$filename = basename($sourcePath);
		return $this->storeAs($sourcePath, $filename, $customSubfolder, $params);
	}



	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::storeAs()
	 *
	 * @return boolean|string   True, False OR the KEY to the file inside the bucket that just has been deployed/stored
	 */
	public function storeAs($sourcePath, $filename, $customSubfolder = "", $params = array()) {

		// Permission to set to uploaded object
		$ACL = isset($params['ACL']) ? $params['ACL'] : $this->defaultAcl;

		// If the source path is a directory, use special method
		if (is_dir($sourcePath)) {
			$folderUploaded = $this->uploadFolder($sourcePath, $filename, $customSubfolder, $ACL, $params);
			if($folderUploaded){
				if(!$this->ignoreEvents) {
					Yii::app()->event->raise( 'onFileUploadedToStorage', new DEvent( $this, array(
						// Internal params
						'storageType'     => $this->type,
						'collection'      => $this->collection,
						// Public params
						'sourcePath'      => $sourcePath,
						'filename'        => $filename,
						'customSubfolder' => $customSubfolder,
						'params'          => $params,
					) ) );
				}
			}
			return $folderUploaded;
		}

		// Open local file
		$fh = fopen($sourcePath,'r');

		if (!$fh) {
			throw new CException("Unable to open local file $sourcePath");
		}

		$body = EntityBody::factory($fh);

		// In case this is a VIDEO LO  **AND** Video transcoding/conversion is enabled, use the "Video Converting" process

		$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_VIDEO_EXTENDED_LIST);
		$isVideoCollection = ($this->collection == CFileStorage::COLLECTION_LO_VIDEO || $this->collection == CFileStorage::COLLECTION_WEBINAR_RECORDING);

		if (($isVideoCollection || ($this->collection == CFileStorage::COLLECTION_COURSEDOCS && $fileValidator->validateLocalFile($filename))) && $this->transcodeVideo) {

			// We upload the file to a special temporary folder into the object storage
			// where it will be taken from and converted by the Elastic transcoding a bit later.
			$key = ( !empty($this->filesBaseRelPath) ?  "/" . $this->filesBaseRelPath : "")
						. "/" . $this->transcodeVideoIncomingFolder
						. "/" . $filename;

			// Encode S3 key into UTF in case 'bad' Unicode characters are used in filenames (well, they are NOT bad, just not UTF8).
			// Example: Unicode(ó)=F3, but UTF8(ó)=C3B3. If F3 is left in the key, S3 will not be happy and return InvalidUriException
			if (strtolower(mb_detect_encoding($key,mb_detect_order(),true)) != 'utf-8') {
				$key = utf8_encode($key);
			}

			$resModel = $this->_client->upload($this->bucket_name, $key, $body, $ACL, $params);

			// Check response object. Must be of a certain type
			if (!get_class($resModel) == 'Guzzle\Service\Resource\Model') {
				throw new CException("Invalid response object from S3 storage (Guzzle\Service\Resource\Model expected)");
			}

			// This is where file become public for play
			$convertedFileKey = $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "")  . ($customSubfolder ? "/" . $customSubfolder: "") . "/" . $filename;
			$convertResult = $this->convertVideo($key, $convertedFileKey);
			if (!$convertResult) {
				return false;
			}
			return $convertedFileKey;

		}
		else {
			$key = $this->absoluteBasePath . ($this->subFolder ? "/" . $this->subFolder : "")  . ($customSubfolder ? "/" . $customSubfolder: "") . "/" . $filename;
			if (strtolower(mb_detect_encoding($key,mb_detect_order(),true)) != 'utf-8') {
				$key = utf8_encode($key);
			}

			// Upload file to S3; get response object
			$resModel = $this->_client->upload($this->bucket_name, $key, $body, $ACL, $params);

			// Check response object. Must be of a certain type
			if (!get_class($resModel) == 'Guzzle\Service\Resource\Model') {
				throw new CException("Invalid response object from S3 storage (Guzzle\Service\Resource\Model expected)");
			}

			if(!$this->ignoreEvents) {
				Yii::app()->event->raise( 'onFileUploadedToStorage', new DEvent( $this, array(
					// Internal params
					'storageType'     => $this->type,
					'collection'      => $this->collection,
					// Public params
					'sourcePath'      => $sourcePath,
					'filename'        => $filename,
					'customSubfolder' => $customSubfolder,
					'params'          => $params,
				) ) );
			}

			return $key;
		}



	}


	/**
	 * Delete a file from S3 storage
	 *
	 * (non-PHPdoc)
	 * @see IFileStorage::remove()
	 */
	public function remove($filename, $customSubfolder = "")
	{
	    
	    // It is dangerous to delete an EMPTY FILENAME - it MAY delete the whole collection!!
	    $filename = trim($filename);
	    if (!is_string($filename) || empty($filename)) {
	        return false;
	    }
	    
		$key = $this->getFileKey($filename, $customSubfolder);
		$model = $this->_client->deleteObject(array(
			"Key" => $key,
			"Bucket" => $this->bucket_name,
		));
		return $model;
	}


	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::fileExists()
	 */
	public function fileExists($filename, $customSubfolder = "")
	{
		// Check for object
		$result = $this->_client->doesObjectExist($this->bucket_name, $this->getFileKey($filename, $customSubfolder));

		// Maybe this is a directory?
		// NOTE:: S3 is an object storage and there are no 'directoies'.
		// If the object itself does not exists (the file) then we have to check if there is
		// ANY object that has key prefix including the $filename
		if (!$result) {
			$keyPrefix = $this->getFileKeyPrefix($filename, $customSubfolder);
			$response = $this->_client->listObjects(array(
				'Bucket' 	=> $this->bucket_name,
				'Prefix' 	=> $keyPrefix,
				'MaxKeys'	=> 1,
			));
			$result = ($contents = $response->get('Contents')) && (count($contents) > 0);
		}

		return $result;
	}



	/**
	 * Return file URL
	 *
	 * @param string $filename
	 * @param string $customSubfolder
	 * @return string
	 */
	public function fileUrl($filename, $customSubfolder = "")
	{

		if(!$this->ignoreEvents) {
			// Allow plugins to override some of the parameters
			// or the method in general
			$event = new DEvent( $this, array(
				'storageType'     => $this->type,
				'collection'      => $this->collection,
				// Modifiable fields:
				'filename'        => &$filename,
				'customSubfolder' => &$customSubfolder,
			) );
			if( ! Yii::app()->event->raise( 'onBeforeGetFileUrlFromStorage', $event ) ) {
				return $event->return_value ? $event->return_value : FALSE;
			}
		}

		// If CDN configuration is successfully loaded, use it for building URLs
		if ($this->cdn) {
			$url = $this->fileCdnUrl($filename, $customSubfolder);
			return $url;
		}

		// Otherwise, just build the URL for direct reading from this storage type
		$expires = false;
		if ( (int) $this->url_expire_period > 0) {
			$expires = time() + $this->url_expire_period;
		}
		$key = $this->getFileKey($filename, $customSubfolder);
		$url = $this->_client->getObjectUrl($this->bucket_name, $key, $expires);
		return $url;
	}


	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::fileSize()
	 */
	public function fileSize($filename, $customSubfolder="") {
		$size = filesize("s3://" . $this->bucket_name . $this->getFileKey($filename));
		return $size;
	}



	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::sendFile()
	 */
	public function sendFile($filename, $displayFileName = "", $customSubfolder = "") {
		if(!$this->ignoreEvents) {
			// Allow plugins to override the functionality completely
			// or some of the parameters
			$event = new DEvent( $this, array(
				// Read-only global params
				'storageType'     => $this->type,
				'collection'      => $this->collection,
				// Dynamic params, can be overriden
				'filename'        => &$filename,
				'displayFileName' => &$displayFileName,
				'customSubfolder' => &$customSubfolder,
			) );
			if( ! Yii::app()->event->raise( 'onBeforeSendFileFromStorage', $event ) ) {
				return $event->return_value ? $event->return_value : FALSE;
			}
		}

		$this->readObject($filename, $displayFileName, $customSubfolder);
	}


	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::getFileInfo()
	 */
	public function getFileInfo($filename, $customSubfolder = "") {
		$info = array();

		$key = $this->getFileKey($filename, $customSubfolder);

		$model = $this->_client->headObject(array(
			'Key' => $key,
			'Bucket' => $this->bucket_name
		));

		return $info;
	}



	/**
	 * Copy file
	 *
	 * NOTE:  Filenames are relative to COLLECTION! e.g. "somefolder/anotherfolder/myfile.txt"
	 *
	 * (this is not a cross-collection or cross-bucket copy! It is same-collection copy!)
	 *
	 * @param string $sourceFileName  Filepath, relative to collection! e.g. somefolder/anotherfolder/myfile.txt
	 * @param string $destFileName    Filepath, relative to collection!
	 */
	public function copy($sourceFileName, $destFileName){
		$sourceFileName    = trim($sourceFileName);
		$destFileName    = trim($destFileName);
		if (!is_string($sourceFileName) || empty($sourceFileName)) {
			return false;
		}

		$srcKey       = $this->getFileKey($sourceFileName);
		$destKey      = $this->getFileKey($destFileName);

		if (strtolower(mb_detect_encoding($destKey,mb_detect_order(),true)) != 'utf-8') {
			$destKey = utf8_encode($destKey);
		}
		$srcKey    = ltrim($srcKey,"/");
		$destKey   = ltrim($destKey,"/");

		$config = array(
				'Bucket'     => $this->bucket_name,
				'Key'        => $destKey,
				'CopySource' => $this->bucket_name."/".$srcKey,
		);

		$this->_client->copyObject($config);
	}


	/**
	 * NOTE: filenames must be relative to the current collection collection!
	 *
	 * {@inheritDoc}
	 * @see IFileStorage::rename()
	 */
	public function rename($filename, $newname, $customSubfolder = ""){
		// Custom folder : NOT used!!!
		$this->copy($filename, $newname);
		$this->remove($filename);
	}





//	/**
//	 * Copy an object from a bucket to another
//	 * @param $sourceBucket
//	 * @param $sourceKeyname
//	 * @param $destinationKey
//	 */
//	public function copyFrom($sourceBucket, $sourceKeyname, $destinationKey) {
//		$key = $this->getFileKey($destinationKey);
//
//		try {
//
//			$this->_client->copyObject(array(
//				'Bucket'     => $this->bucket_name,
//				'Key'        => $key,
//				'CopySource' => "{$sourceBucket}/{$sourceKeyname}",
//			));
//		}
//		catch (S3Exception $e) {
//			throw new CException($e->getMessage());
//		}
//
//		return true;
//	}

	/**
	 * Performs a batch copy of S3 files
	 * @param $itemsToCopy
	 * @return array
	 */
	public function copyBatch($itemsToCopy) {
		$batch = array();
		foreach ($itemsToCopy as $item)
			$batch[] = $this->_client->getCommand('CopyObject', array(
				'Bucket'     => $this->bucket_name,
				'Key'        => $this->getFileKey($item['destinationKey']),
				'CopySource' => "{$item['sourceBucket']}/{$item['sourceKeyname']}",
			));

		try {
			$successful = $this->_client->execute($batch);
			$failed = array();
		} catch (\Guzzle\Service\Exception\CommandTransferException $e) {
			$successful = $e->getSuccessfulCommands();
			$failed = $e->getFailedCommands();
		}

		return array('successful' => $successful, 'failed' => $failed);
	}

	/**
	 * Use VideoConverter component to run an Amazon Elastic Transcoding Job and start polling for status until we get some result (success, error,...)
	 *
	 * @param string $inKey  S3 object key of the file to be converted
	 * @param string $outKey S3 object key of the resulting, converted file
	 * @return boolean
	 */
	public function convertVideo($inKey, $outKey) {
		$vc = new VideoConverter($this, array(
				'inputKey' 	=> $inKey,
				'outputKey' => $outKey
		));
		$result = $vc->runTranscoding();
		return $result;
	}


	/**
	 *
	 */
	public function getS3BucketName(){
		return $this->bucket_name;
	}

	/**
	 *
	 */
	public function getS3Key() {
		return $this->key;
	}

	/**
	 *
	 */
	public function getS3Secret() {
		return $this->secret;
	}

	/**
	 *
	 */
	public function getS3Region() {
		return $this->region;
	}

	/**
	 *
	 * @return multitype:
	 */
	public function getS3ServiceParams() {
		return $this->serviceParams;
	}



	/**
	 * S3 specific method returning list of files (keys) inside a 'folder' (prefix)
	 *
	 * @see CFileStorage::findFiles()
	 */
	public function findFiles($subFolder='', $options=array()) {

		// Normalize subfolder
		$subFolder = $this->normalizeDelimiter($subFolder);
		$keyPrefix = $this->getFileKeyPrefix('', $subFolder);

		// S3 client config
		$config = array(
				'Bucket' => $this->bucket_name,
				'Prefix' => $keyPrefix,
		);

		$iterator = $this->_client->getIterator('ListObjects', $config);

		$list = array();
		foreach ($iterator as $object) {
    		$list[] = $object['Key'];
		}

		return $list;
	}


	/**
	 * Download file from the storage
	 *
	 * @see IFileStorage::downloadAs()
	 */
	public function downloadAs($filename, $localFilePath, $customSubfolder = "") {

		$key = $this->getFileKey($filename, $customSubfolder);
		try {
			$result = $this->_client->getObject(array(
				'Bucket' 	=> $this->bucket_name,
				'Key' 		=> $key,
				'SaveAs'	=> $localFilePath,
			));
			return $localFilePath;
		}
	 	catch (S3Exception $e) {
	 		Yii::log($e->getMessage(), 'error');
	 		Yii::log($key, 'error');
    		return false;
		}

	}


	/**
	 * Recursively download all files from the storage collection (or from its subfolder) and save them into a local path
	 *
	 * Example usage:
	 * 		Download a collection subfolder only into "/var/home/some/local/path"
	 * 		$storage->downloadCollectionAs('/var/home/some/local/path', 'collection-subfolder');

	 * 		Download the whole collection content into "/var/home/some/local/path"
	 * 		$storage->downloadCollectionAs('/var/home/some/local/path');
	 *
	 * 		Download files from collection's "subfolder/path" into "/var/home/local/path/subfolder/path", i.e. keep collection subfolder name(s)
	 * 		$storage->downloadCollectionAs('/var/home/local/path', 'subfolder/path', true);
	 *
	 * @see IFileStorage::downloadCollectionAs()
	 */
	public function downloadCollectionAs($localPath, $customSubfolder="", $keepCustomSubfolder=false) {

		if (!is_string($localPath)) {
			return false;
		}

		$customSubfolder = trim($customSubfolder,'\\/');

		// Normalize subfolder
		$subFolder = $this->normalizeDelimiter($customSubfolder);
		$keyPrefix = $this->getFileKeyPrefix('', $customSubfolder);

		// S3 client config
		$config = array(
				'Bucket' => $this->bucket_name,
				'Prefix' => $keyPrefix,
		);

		// Get objects iterator. We use iterator to avoid AWS client limits (max number of objects to list)
		$iterator = $this->_client->getIterator('ListObjects', $config);

		// Iterate over objects
		foreach ($iterator as $object) {

			// Prepare LOCAL path information
			$keyPath 		= str_replace(self::S3_GROUPING_DELIMITER, DIRECTORY_SEPARATOR, $object['Key']);
			$collectionPath = str_replace(self::S3_GROUPING_DELIMITER, DIRECTORY_SEPARATOR, $this->path());
			$subFolder		= str_replace(self::S3_GROUPING_DELIMITER, DIRECTORY_SEPARATOR, $subFolder);

			$localFilePath = $localPath . DIRECTORY_SEPARATOR . $keyPath;

			$toRemove = $collectionPath;
			if (!$keepCustomSubfolder) {
				$toRemove .= ($subFolder ? DIRECTORY_SEPARATOR . $subFolder : '');
			}

			// Remove "system" part of the path
			$localFilePath = str_replace($toRemove, '', $localFilePath);

			// Create local target directory if it doesn't yet exist; recursively
			$pathInfo = pathinfo($localFilePath);
			if (!is_dir($pathInfo['dirname'])) {
				FileHelper::createDirectory($pathInfo['dirname'], null, true);
			}

			// Download object and save it as $localFilePath
			$result = $this->_client->getObject(array(
					'Bucket' 	=> $this->bucket_name,
					'Key' 		=> $object['Key'],
					'SaveAs'	=> $localFilePath,
			));


			// Go iterate again
		}


	}


}


