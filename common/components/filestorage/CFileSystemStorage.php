<?php
/**
 * Local file system storage
 * 
 * @TODO Make hardcoded values configurable:
 * 
 */
class CFileSystemStorage extends CFileStorage
{
	
	
	/**
	 * Constructor 
	 * 
	 * @param struct $collection 
	 */
	public function __construct($collection = self::COLLECTION_COMMON, $params = array()) {
		
		// This is not just because we can! We MUST call this!
		parent::__construct($collection, $params);
		
		// Storage type: FILE SYSTEM
		$this->type = parent::TYPE_FILESYSTEM;
		
		// Absolte base PATH to container
		$this->absoluteBasePath = Docebo::getRootBasePath() .
			( !empty($this->filesBaseRelPath) ?  DIRECTORY_SEPARATOR . $this->filesBaseRelPath : "") .
			( $this->container  ? DIRECTORY_SEPARATOR . $this->container : "");
		
		// Absolte base URL to container
		$this->absoluteBaseUrl =  Docebo::getRootBaseUrl(true) .
			"/" . $this->filesBaseRelPath . 
			( $this->container  ? "/" . $this->container : ""); 
			
		
		
	}
	
	
	/**
	 * Return absolute file path 
	 * 
	 * @param string $filename  
	 * @param string $customSubfolder Additional (custom) subfolder 
	 * @return string
	 */
	protected function filePath($filename, $customSubfolder = "") {
		$fullPath = $this->absoluteBasePath .
			(empty($this->subFolder) 	    ? "" : DIRECTORY_SEPARATOR . $this->subFolder ) . // generic file type subfolder
			(empty($customSubfolder) 		? "" : DIRECTORY_SEPARATOR . $customSubfolder);      // caller specified subfolder
		$fullPath = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $fullPath);
		return 	$fullPath . DIRECTORY_SEPARATOR . $filename;
		//return 	realpath($fullPath) . DIRECTORY_SEPARATOR . $filename;
		
	}

	
	
	/**
	 * Store file or a folder (recursively) 
	 * 
	 * (non-PHPdoc)
	 * @see IFileStorage::store()
	 */
	public function store($sourcePath, $customSubfolder = "", $params = array())
	{
		$filename = basename($sourcePath);
		return $this->storeAs($sourcePath, $filename, $customSubfolder, $params);
	}

	
	/**
	 * Store file or a folder (recursively), under specified name.
	 * If destination folder does not exist, it is created (recursively) 
	 * 
	 * (non-PHPdoc)
	 * @see IFileStorage::storeAs()
	 * 
	 * @return boolean|string   True, False OR the PATH to the file in local file system that just has been deployed/stored
	 *  
	 */
	public function storeAs($sourcePath, $filename, $customSubfolder = "", $params = array()) {
		$destinationPath = $this->filePath($filename, $customSubfolder);
		
		// Create destination directory recursively, if it does not exists yet
		$dirName = dirname($destinationPath);
		if (!is_dir($dirName)) {
			@mkdir($dirName, 0777, true);
		}

		$res = false;
		if (is_dir($sourcePath)) {
			CFileHelper::copyDirectory($sourcePath, $destinationPath);
			$res = true;
		} 
		else {
			if (@copy($sourcePath, $destinationPath)) {
				$res =  $destinationPath;
			}
		}

		if($res){
			if(!$this->ignoreEvents) {
				Yii::app()->event->raise( 'onFileUploadedToStorage', new DEvent( $this, array(
					// Internal params
					'storageType'     => $this->type,
					'collection'      => $this->collection,
					// Public params
					'sourcePath'      => $sourcePath,
					'filename'        => $filename,
					'customSubfolder' => $customSubfolder,
					'params'          => $params,
				) ) );
			}
		}

		return $res;
	}	
	
	
	/**
	 * Delete a file 
	 * 
	 * (non-PHPdoc)
	 * @see IFileStorage::remove()
	 */
	public function remove($filename, $customSubfolder = "")
	{
		$resourcePath = $this->filePath($filename, $customSubfolder);
		return FileHelper::removeFile($resourcePath);
	}


	
	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::removeFolder()
	 */
	public function removeFolder($folderName)  {
		$folder = $this->filePath($folderName);
		return FileHelper::removeDirectory($folder);
	}
	

	/**
	 * Check if a file exists
	 * 
	 * (non-PHPdoc)
	 * @see IFileStorage::exists()
	 */
	public function fileExists($filename, $customSubfolder = "")
	{
		$resourcePath = $this->filePath($filename, $customSubfolder);
		return file_exists($resourcePath);
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::fileUrl()
	 */
	public function fileUrl($filename, $customSubfolder = "") {

		if(!$this->ignoreEvents) {
			// Allow plugins to override some of the parameters
			// or the method in general
			$event = new DEvent( $this, array(
				'storageType'     => $this->type,
				'collection'      => $this->collection,
				// Modifiable fields:
				'filename'        => &$filename,
				'customSubfolder' => &$customSubfolder,
			) );
			if( ! Yii::app()->event->raise( 'onBeforeGetFileUrlFromStorage', $event ) ) {
				return $event->return_value ? $event->return_value : FALSE;
			}
		}

		// If CDN configuration is successfully loaded, use it for building URLs
		if ($this->cdn) {
			$url = $this->fileCdnUrl($filename, $customSubfolder);
			return $url;
		}
		
		
		$url = $this->absoluteBaseUrl .
		(empty($this->subFolder) 	? '' : '/' . $this->subFolder ) .
		(empty($customSubfolder) 	? '' : '/' . $customSubfolder)
		. '/' . $filename;
	
		return $url;
	}
	

	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::fileSize()
	 */
	public function fileSize($filename, $customSubfolder = "") {
		return filesize($this->filePath($filename, $customSubfolder));
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::sendFile()
	 */
	public function sendFile($filename, $displayFileName = "", $customSubfolder = "") {

		if(!$this->ignoreEvents) {
			// Allow plugins to override the functionality completely
			// or some of the parameters only
			$event = new DEvent( $this, array(
				// Read-only global params
				'storageType'     => $this->type,
				'collection'      => $this->collection,
				// Dynamic params, can be overriden to modify the standard behavior
				'filename'        => &$filename,
				'displayFileName' => &$displayFileName,
				'customSubfolder' => &$customSubfolder,
			) );
			if( ! Yii::app()->event->raise( 'onBeforeSendFileFromStorage', $event ) ) {
				return $event->return_value ? $event->return_value : FALSE;
			}
		}

		if (empty($displayFileName)) {
			$displayFileName = $filename;
		}
		$filePath = $this->filePath($filename, $customSubfolder);
		if (file_exists($filePath)) {
			//$fileContent = file_get_contents($filePath);
			//Yii::app()->request->sendFile($filename, $fileContent, NULL, false, $displayFileName);
			Yii::app()->request->sendFileDirect($filePath, $displayFileName);
		} 
	}
	

	
	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::getFileInfo()
	 */
	public function getFileInfo($filename, $customSubfolder = "") {
		$info = array();
		return $info;
	}
	

	
	public function rename($filename, $newname, $customSubfolder = "") {
	
	}
	
	/**
	 * (non-PHPdoc)
	 * @see IFileStorage::filesBasePath()
	 */
	public function filesBasePath() {
		return Docebo::getRootBasePath() . ( !empty($this->filesBaseRelPath) ?  DIRECTORY_SEPARATOR . $this->filesBaseRelPath : "");
	}

	
	/**
	 * @see CFileStorage::findFiles()
	 */
	public function findFiles($subFolder='', $options=array()) {
		$subFolder = trim($subFolder);
		$subFolder = trim($subFolder, DIRECTORY_SEPARATOR);
		$finalDir = rtrim($this->path(), DIRECTORY_SEPARATOR) . (empty($subFolder) ? '' : DIRECTORY_SEPARATOR . $subFolder);
		$finalDir = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $finalDir);

		$list = array();
		if (is_dir($finalDir)) {
			$list = FileHelper::findFiles($finalDir, $options);
		}
		else {
			return false;
		}
		
		return $list;
	}
	
	/**
	 * Download file from the storage
	 *
	 * @see IFileStorage::downloadAs()
	 */
	public function downloadAs($filename, $localFilePath, $customSubfolder = "") {
		$resourcePath = $this->filePath($filename, $customSubfolder);
		if (!is_file($resourcePath)) {
			return false;
		}
		@copy($resourcePath, $localFilePath);
		return $localFilePath;
	}
	

	/**
	 * Recursively download all files from the storage collection (or from its subfolder) and save them into a local path
	 * @see IFileStorage::downloadCollectionAs()
	 */
	public function downloadCollectionAs($localPath, $customSubfolder="", $keepCustomSubfolder=false) {
		$collectionPath = $this->path();
		$sourcePath = $collectionPath . DIRECTORY_SEPARATOR . $customSubfolder;
		$sourcePath = rtrim($sourcePath, '/\\');
		
 		$toRemove = $collectionPath;
 		if (!$keepCustomSubfolder) {
 			$toRemove .= ($customSubfolder ? DIRECTORY_SEPARATOR . $customSubfolder : '');
 		}
 		$localFilePath = str_replace($toRemove, '', $localFilePath);
		
		FileHelper::copyDirectory($sourcePath, $localPath);		
	}
	
}
