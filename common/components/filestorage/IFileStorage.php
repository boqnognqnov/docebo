<?php
/**
 * Interface class, implemented by other FileStorage classes.
 * 
 * See all public methods for available File operations
 * 
 */
interface IFileStorage
{
	
	/**
	 * Stores a file to file storage
	 * 
	 * @param string $sourcePath  Source file from local file system
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 * @param array Array of options compliant to  Guzzle\Service\Resource\Model  putObject()
	 *              http://docs.aws.amazon.com/aws-sdk-php-2/latest/class-Aws.S3.S3Client.html#_putObject
	 * @return bool
	 */
	public function store($sourcePath, $customSubfolder = "", $params = array()); 
	
	
	/**
	 * Stores a file to file storage, while changing its name
	 * 
	 * @param string $sourcePath  Source file from local file system
	 * @param string $asFileName New file name
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 * @param array Array of options compliant to  Guzzle\Service\Resource\Model  putObject()
	 *              http://docs.aws.amazon.com/aws-sdk-php-2/latest/class-Aws.S3.S3Client.html#_putObject
	 */
	public function storeAs($sourcePath, $filename, $customSubfolder = "", $params = array());
	

	/**
	 * Deletes a file 
	 *
	 * @param string $filename File Name
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 * @return bool
	 */
	public function remove($filename, $customSubfolder = "");

	/**
	 * Recursively remove all files and folders below specified folder, which is relative to collection 
	 * 
	 * @param string $folderName  Folder, relative to collection 
	 */
	public function removeFolder($folderName);
	
	
	/**
	 * Check if file or directory exists
	 *
	 * @param string $filename File Name
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 * @return bool
	 */
	public function fileExists($filename, $customSubfolder = "");
	
	
	/**
	 * Return absolute URL to a given file from this storage && collection, optionaly specifying custom path
	 *  
	 * @param string $filename
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 */
	public function fileUrl($filename, $customSubfolder);	
	
	
	/**
	 * Return file size
	 * 
	 * @param string $filename
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 */
	public function fileSize($filename, $customSubfolder = "");
	
	
	
	/**
	 * Send file content to browser for download
	 * 
	 * @param string $filename Real physical file name
	 * @param string $displayFileName  Filename to use during download
	 * @param string $customSubfolder  Optional subfolder inside collection, where $filename is written or read from
	 */
	public function sendFile($filename, $displayFileName = "", $customSubfolder = "");
	
	
	
	/**
	 * Returns an associative array with various file information 
	 * 
	 * @param string $filename
	 * @param string $customSubfolder
	 */
	public function getFileInfo($filename, $customSubfolder = "");
	
	

	/**
	 * Rename file 
	 * 
	 * @param string $filename
	 * @param string $newname
	 * @param string $customSubfolder Optional subfolder inside collection, where $filename is written or read from
	 */
	public function rename($filename, $newname, $customSubfolder = "");


	/**
	 * Return absolute path to storage collection folder
	 * Examples:
	 * 		Local file system	:  /var/www/vhosts/lms/files/my_domain_code/doceboLms/video
	 * 		S3 (below bucket)	:  /files/m/y/my_domain_code/video
	 *
	 * @return string
	 */
	public function path();	
	
	
	/**
	 * Return absolute path to storage container folder
	 * Examples:
	 * 		Local file system	:  /var/www/vhosts/lms/files/my_domain_code
	 * 		S3 (below bucket)	:  /files/m/y/my_domain_code
	 *
	 * @return string
	 */
	public function containerPath();
	
	
	/**
	 * Return absolute path to "files" path.
	 * Examples:
	 * 		Local file system	:  /var/www/vhosts/lms/files
	 * 		S3 (below bucket)	:  /files
	 */
	public function filesBasePath();

	
	/**
	 * Return the Video extensions Whitelist TYPE, as per the FileTypeValidator definitions
	 * Depends on the Storage type: S3, Filesystem, etc.
	 *
	 * @return string
	 */
	public function getVideoWhitelistType();
	

	/**
	 * Return an array of allowed VIDEO extensions for THIS storage
	 * @return array
	 */
	public function getVideoWhitelistArray();
	
	
	/**
	 * Return URI of the storage, like  http://my.domain.com/files/.../somecollectionfolder
	 *
	 * @param boolean $noSlash  Return the URI with or without the slash at the end
	 * @return string
	 */
	public function uri($noSlash=true);
	

	/**
	 * Return URL of the storage host itself
	 * 
	 * @param boolean $noSlash  With or Without a slash at the end
	 * @param string $noSlash
	 */
	public function getHostUrl($noSlash=false);
	
	
	/**
	 * Return list of files in a directory
	 * 
	 * @param string $subFolder  Optional sub-folder inside the collection
	 * @param array $options  Array of various options (could be specific for different storage types). No predefined set. Arbitrary.
	 */
	public function findFiles($subFolder='', $options); 

	
	/**
	 * Suspend using CDN temporary, even if it is configured in Yii config.<br>
	 * Current configuration is stored in internal class attribute and can be restored later by calling enableCdn(), which merely copies back configuration from the backup variable.<br>
	 * <br>
	 * <b>Note however</b>, CDN still have to be properly configured for current collection; If it is not, enableCdn/disableCdn have no effect.
	 */
	public function disableCdn();
	
	/**
	 * Enable CDN, by restoring CDN configuration from internal backup variable, saved earlier by calling diableCdn()<br>
	 * These two methods (disable/enableCdn) are usefull for temporary suspend building CDN URLs<br>
	 * <br> 
	 * <b>Note however</b>, CDN still have to be properly configured for current collection; If it is not, enableCdn/disableCdn have no effect.
	 */
	public function enableCdn();
	

	/**
	 * Download file from the storage
	 *  
	 * @param string  $filename File name in the storage we are going to "download"
	 * @param string $localFilePath Full path and file name of the local file to save the downloaded file as
	 * @param string $customSubfolder Optional subfolder inside the storage collection wher the downloaded file is located
	 */
	public function downloadAs($filename, $localFilePath, $customSubfolder = "");	
	
	
	/**
	 * Recursively download all files from the storage collection (or from its subfolder) and save them into a local path
	 *  
	 * @param string $localPath  Wher file are going to
	 * @param string $customSubfolder  (Optional) Collection subfolder to be scanned (as opposed to the WHOLE collection if omitted)
	 * @param boolean $keepCustomSubfolder Keeep the custom subfolder (if any) as part of the local file path
	 */
	public function downloadCollectionAs($localPath, $customSubfolder="", $keepCustomSubfolder=false);
	
	
}
