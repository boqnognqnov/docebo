<?php
/**
 * 
 * Helper class to do various things related to Cloudfront Distributions, used by S3 file storage
 *
 */
class Cloudfront extends CComponent {

    /**
     * Provide singleton pattern by caching instances in this class static variable
     * @var Array
     */
    private static $instances;
    
    /**
     * Absolute Path to Cloudfront Private key file
     * @var string
     */
    private $_keyPath;
    
    /**
     * Holds the Cloudfront signed cookies policy (JSON)
     * @var string
     */
    private $_policy;
    
    /**
     * Cloudfront Key pair ID
     * @var string
     */
    private $_keyPairId;
    
    /**
     * Holds cloudfront distribution parameters
     * @var array
     */
    private $_distributionParams;
    
    
    private $_expireAt;
    
    /**
     * Constructor
     * @param string $distribution
     */
    public function __construct($distribution) {
        
        if (Yii::app()->params["cdn"]["distributions"][$distribution]["type"] !== "cloudfront") {
            return;
        }
        
        $this->_distributionParams = Yii::app()->params["cdn"]["distributions"][$distribution]["params"];
        
        // If distribution is for signed cookes...
        if (isset($this->_distributionParams["cloudfront_signed_cookies"]) && $this->_distributionParams["cloudfront_signed_cookies"]) {
            
            $this->_keyPath = dirname(dirname(realpath(__DIR__))) . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . $this->_distributionParams["cloudfront_rsa_private_key_file"];
            
            if (!is_file($this->_keyPath)) {
                Yii::log("Missing Cloudfront key file (/common/config/" . $this->_distributionParams["cloudfront_rsa_private_key_file"] . ")", CLogger::LEVEL_ERROR);
            }
            
            if (isset($this->_distributionParams["cloudfront_key_pair_id"])) {
                $this->_keyPairId = $this->_distributionParams["cloudfront_key_pair_id"];
            }
            else {
                Yii::log("Missing Cloudfront Key Pair ID in configuration file", CLogger::LEVEL_ERROR);
            }

            // We set the expire time to the next rounded hour after expire period (seconds)
            // The same expire time (epoch) is used in Cloudfront URL as last parameter to provide CF caching during the valid period
            $expirePeriod = 3600;
            if (isset($this->_distributionParams["cloudfront_cookie_policy_expire"])) {
            	$expirePeriod = (int) $this->_distributionParams["cloudfront_cookie_policy_expire"];
            }
            
            $hours = (int) round($expirePeriod/3600);
            $hours = ($hours > 0) ? $hours : 1;  // minimum 1 hour
            $this->_expireAt = (int) Yii::app()->localtime->nextRoundHourTime($hours);
            
            $this->_policy = $this->buildPolicy();
        }
        
    }
    
    /**
     * Get an object instance for particular CF distribution
     * @param string $distribution
     */
    public static function getInstance($distribution) {
        if (isset(self::$instances[$distribution])) {
            return self::$instances[$distribution];
        }
        self::$instances[$distribution] = new self($distribution);
        return self::$instances[$distribution];
    }

    /**
     * Generate 3 cookies required by Amazon Cloudfront signed cookies mechanic
     */
    public function getCookiesTriade() {
        $result = array();
        if (!$this->_keyPath || !$this->_keyPairId || !$this->_policy) {
        	return $result;
        }
        $signature = $this->rsa_sha1_sign($this->_policy, $this->_keyPath);
        $result['CloudFront-Policy']        = $this->url_safe_base64_encode($this->_policy);
        $result['CloudFront-Signature']     = $this->url_safe_base64_encode($signature);
        $result['CloudFront-Key-Pair-Id']   = $this->_keyPairId;
        return $result;
    }
    
    /**
     * Return an URL to a script (located on cloudfront domain) to set client's browser cookies
     *  
     */
    public function getCfCookieBakerUrl() {
        $cookies = $this->getCookiesTriade();
        if (!is_array($cookies) || empty($cookies)) {
            return false;
        }
        
        $path = "/files/" . Docebo::getSplitOriginalDomainCode();
        
        // URL will follow the current HTTP schema (http or https)
        $url = "//". $this->_distributionParams["domain"]. "/" . $this->_distributionParams["cookie_baker_relative_url"];
        
        $url .= "?p=" . $cookies["CloudFront-Policy"];
        $url .= "&s=" . $cookies["CloudFront-Signature"];
        $url .= "&k=" . $cookies["CloudFront-Key-Pair-Id"];
        $url .= "&d=" . Docebo::getOriginalDomain();
        $url .= "&path=" . $path;
        $url .= "&t=" . $this->_expireAt;
        return $url;        
    }

    /**
     * Sign
     * @param unknown $policy
     * @param unknown $private_key_filename
     * @return string
     */
    private function rsa_sha1_sign($policy, $private_key_filename) {
        $signature = "";
    
        // load the private key
        $fp = fopen($private_key_filename, "r");
        $priv_key = fread($fp, 8192);
        fclose($fp);
        $pkeyid = openssl_get_privatekey($priv_key);
    
        // compute signature
        openssl_sign($policy, $signature, $pkeyid);
    
        // free the key from memory
        openssl_free_key($pkeyid);
    
        return $signature;
    }
    
    /**
     * 
     * @param unknown $value
     */
    private function url_safe_base64_encode($value) {
        $encoded = base64_encode($value);
        return str_replace(
            array('+', '=', '/'),
            array('-', '_', '~'),
            $encoded);
    }
    
    
    private function buildPolicy() {
        $policy = array(
            "Statement" => array(
                array(
                    "Resource"      => "http*://" . $this->_distributionParams["domain"] . "/*",
                    "Condition"     => array(
                        "DateLessThan"      => array(
                            "AWS:EpochTime" => $this->_expireAt,
                        ),
                    ),
                ),
            ),
        );
        return json_encode($policy);
    }
    
    
    
}