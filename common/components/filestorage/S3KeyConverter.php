<?php
/**
 * Extends core AWS SDK KeyConverter to allow filename/key conversion
 */

use Aws\S3\Sync\KeyConverter;

class S3KeyConverter extends KeyConverter {
	
	/**
	 * @see \Aws\S3\Sync\KeyConverter::convert()
	 */
    public function convert($filename) {
    	 
    	// Normalize baseDir: convert "/" and "\" to OS-dependent DIRECTORY_SEPARATOR
    	// ! This is required for AWS SDK v2.6.6+  only!
    	// This is very important, because the key builder is confused if there are mixed slashes
    	$this->baseDir = str_replace(array('\\','/'), DIRECTORY_SEPARATOR, $this->baseDir);

    	$key = parent::convert($filename);
    	if (strtolower(mb_detect_encoding($key, mb_detect_order(),true)) != 'utf-8') {
    		$key = utf8_encode($key);
    	}
    	 
		return $key;
    }
	
}
