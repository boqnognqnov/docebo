<?php

/**
 * Class DoceboMessageSource
 *
 * This class mange the translation of messages inside the Docebo application, unlike the standard php
 * source from yii framework it does a combination of files and db allowing the end user to customize
 * some of the translation from the user interface.
 * Those customization are saved in database, that means that on message loading first we load from php
 * files and then override the custom transaltion with the ones setted up by the end user.
 *
 * Also, unlinke yii standard, if a phrase is not found in the category passed to Yii::t, before launching
 * the language not found exception the standard category is checked in order to see if a general
 * translation is available for the same key.
 */
class DoceboMessageSource extends CPhpMessageSource {

	/**
	 * This constant contain the name of the default module
	 */
	const STANDARD_CAT = 'standard';


	public $standard_messages = array();


	public function init() {
		parent::init();
		$this->basePath = Yii::getPathOfAlias('common.messages');

		// add the listener to missing translation in order to perform a standard module check fallback
		$this->attachEventHandler('onMissingTranslation', array('DoceboMessageSource', 'missingFallback'));
	}


	/**
	 * Custom method to load messages; differences with the original one:
	 * - we load messages also from DB and if found the one in the db will override the default one
	 * - we always load the standard category and if translation is not found in the given one we look in the standard
	 * @param string $category
	 * @param string $language
	 * @return array
	 */
	protected function loadMessages($category, $language) {

		$messageFile = null;
		if ($category != self::STANDARD_CAT) {
			if (empty($this->standard_messages[$language])) {
				$this->standard_messages[$language] = $this->loadMessages(self::STANDARD_CAT, $language);
			}
		}
		if(($pos=strpos($category,'.'))!==false)
		{
			$extensionClass=substr($category,0,$pos);
			$extensionCategory=substr($category,$pos+1);
			if(isset(Yii::app()->messages->extensionPaths[$extensionClass]) && Yii::app()->messages->extensionPaths[$extensionClass]){
				$messageFile = parent::getMessageFile($category, $language);
			}
		}else{
			$messageFile = parent::getMessageFile($category, $language);
		}


		$key = 'messages-' . $language . '-'  . $category;
		if ($this->cachingDuration > 0 && $this->cacheID !== false && ($cache = Yii::app()->getComponent($this->cacheID)) !== null) {
			if ((($data = $cache->get($key)) !== false) && !$cache->isInvalid($key)) {
				return unserialize($data);
			}
		}

		if($messageFile){
			$messages = is_file($messageFile) ? include($messageFile) : array();
		}else{
			$messages = array();
		}


		if (!is_array($messages)) {
			$messages = array();
		}

		$messages_db = $this->loadMessagesFromDb($category, $language);

		if($messages_db)
			$messages = array_merge($messages, $messages_db);

		if (isset($cache) && $messageFile) {
			$dependency = new CFileCacheDependency($messageFile);
			$cache->set($key, serialize($messages), $this->cachingDuration, $dependency);
		}
		return $messages;

	}


	/**
	 * Load messages from the database and if found the translation from the
	 * database will override the one from the message file
	 * @param $category
	 * @param $language
	 * @return array
	 */
	protected function loadMessagesFromDb($category, $language) {
		$messages = array();
		$full_lang_name = Lang::getFullLangName($language);

		$categories = array('standard');
		if (!empty($category)) {
			$categories[] = $category;
		}
		
		$q = Yii::app()->db->createCommand()
			->select('lang.text_key, lang.text_module, tr.translation_text')
			->from('core_lang_text lang')
			->join('core_lang_translation tr', 'lang.id_text = tr.id_text')
			->where(array('and',
				array('in', 'lang.text_module', $categories),
				array('in', 'tr.lang_code', array($language, $full_lang_name))
			))->queryAll();

		foreach($q as $row) {
			$messages[$row['text_key']] = $row['translation_text'];
		}

		return $messages;
	}


	/**
	 * If a translation is not found in the proper translation category then we should try to fallback on the
	 * standard category before using the message untranslated
	 * @param CMissingTranslationEvent $event
	 */
	public static function missingFallback(CMissingTranslationEvent $event) {

		$key = $event->language; //.'.'.$event->category; category is not used as a key for standard
		$ms = Yii::app()->getComponent('messages');

		if (isset($ms->standard_messages[$key][$event->message])) {
			$event->message = $ms->standard_messages[$key][$event->message];
		}
	}


	/**
	 * This is a possible listener for the translation missing envent
	 * if cofigured in the config this callback will save the language not found in database
	 * @param CMissingTranslationEvent $event
	 * @return bool
	 */
	public static function writeMissingToDb(CMissingTranslationEvent $event) {

		$res = false;
		// check to specific config parameter in order to limit the creation of
		// the missing keys for a single installation / database:
		$log_missing = Settings::get('log_missing_translations_to_db', false);
		if (!$log_missing) {
			return $res;
		}

		if (empty($event->message)) {
			return $res;
		}

		$lang_text = CoreLangText::model()->findAllByAttributes(array(
			'text_key' => $event->message,
			'text_module' => $event->category,
		));

		if (!$lang_text) {
			$lang_text = new CoreLangText();
			$lang_text->text_key = $event->message;
			$lang_text->text_module = $event->category;
			try {
				$res = $lang_text->save(false);
			} catch(Exception $e) {
				$res = false;
				Yii::log(var_export($lang_text->getErrors(),true), CLogger::LEVEL_ERROR);	
			}
		}

		return $res;
	}

	/**
	 * Replace the "Docebo" placeholder with the White Label plugin site name
	 * If the plugin is not active - the word "Docebo" will be used.
	 * @param string $category
	 * @param string $message
	 * @param null $language
	 * @return mixed|string
	 */
	public function translate($category,$message,$language=null)
	{
		$message = parent::translate($category,$message,$language);
		if ($category != 'branding') {
			$message = self::replaceDocebo($message);
		}

		return $message;
	}

	public static function replaceDocebo($message)
	{
		if (stripos($message, 'www.docebo.com') !== false)
			$message = str_ireplace('www.docebo.com', BrandingWhiteLabelForm::getSiteReplacement(), $message);
		if (stripos($message, 'docebo') !== false) {
			$whiteLabelNaming = Settings::get('whitelabel_naming',  null, false, false);
			$whiteLabelDisableNaming = Settings::get('whitelabel_disable_naming',  null, false, false);

			Yii::app()->event->raise('GetWhiteLabelSettings', new DEvent(self, array(
				'whitelabel_disable_naming' => &$whiteLabelDisableNaming,
				'whitelabel_naming' => &$whiteLabelNaming
			)));

			//"Replace, in every page of your E-Learning platform, the word Docebo with a custom word" is enabled
			if($whiteLabelNaming && (stripos($message, '<img') === false)) {
				//"Do not replace the text in translations containing hyperlinks" is enabled
				if($whiteLabelDisableNaming){
					$message = preg_replace('/(?!<a[^>]*?>)(docebo)(?![^<]*?<\/a>)/mi', BrandingWhiteLabelForm::getLabelReplacement(), $message);
				} else {
					$message = str_ireplace('docebo', BrandingWhiteLabelForm::getLabelReplacement(), $message);
				}
			}
		}

		return $message;
	}

	public function resetCache($category) {

		$languages = CoreLangLanguage::model()->findAll();
		foreach ($languages as $language) {
			$key = 'messages-' . $language->lang_browsercode . '-'  . $category;
			if ($this->cacheID !== false && ($cache = Yii::app()->getComponent($this->cacheID)) !== null) {
				if (($data = $cache->get($key)) !== false) {
					$cache->delete($key);
				}
			}
		}

		return true;
	}


	/**
	 * Detect if a cache file key is a message file or not
	 * @param $key the name of the cache file to be checked
	 * @return bool if the cache file is effectively a message file or not
	 */
	public function isMessageCacheFile($key) {
		//example of $key : 'messages-' . {language browsercode, es. "en", "pt-br"} . '-'  . {language category, es. "admin_lang", "standard"}
		return preg_match('/^messages-([a-z]{2}-[\w]+|[a-z]{2}-[a-z]{2,4}-[\w]+)$/', $key);
	}

}
