<?php

class DoceboCLinkPagerInfinite extends CLinkPager {

	private $baseUrl;

	/**
	 * @var string the text label for the next page button. Defaults to 'Next &gt;'.
	 */
	public $nextPageLabel;

	/**
	 * @var string the text label for the previous page button. Defaults to '&lt; Previous'.
	 */
	public $prevPageLabel;

	/**
	 * @var string the text label for the first page button. Defaults to '&lt;&lt; First'.
	 */
	public $firstPageLabel;

	/**
	 * @var string the text label for the last page button. Defaults to 'Last &gt;&gt;'.
	 */
	public $lastPageLabel;

	public function init() {
		parent::init();
		$this->baseUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/infinite';

		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$cs->registerCSSFile($this->baseUrl . '/jquery.ias.css');

		if ($this->nextPageLabel === null)
			$this->nextPageLabel = '';
		if ($this->prevPageLabel === null)
			$this->prevPageLabel = '';
		if ($this->firstPageLabel === null)
			$this->firstPageLabel = Yii::t('standard','First');
		if ($this->lastPageLabel === null)
			$this->lastPageLabel = Yii::t('standard','Last');
	}

	public function run() {
		$buttons = $this->createPageButtons();
		if (empty($buttons)) return;
		echo $this->header; // if any
		echo CHtml::tag('ul', $this->htmlOptions, implode("\n", $buttons));
		echo $this->footer;  // if any
	}

	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons()
	{
		if (($pageCount = $this->getPageCount()) <= 1)
			return array();

		list($beginPage, $endPage) = $this->getPageRange();
		$currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons = array();

		// first page
		$buttons[] = $this->createPageButton($this->firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);

		// internal pages
		for ($i = $beginPage; $i <= $endPage; ++$i)
			$buttons[] = $this->createPageButton($i + 1, $i, $this->internalPageCssClass, false, $i == $currentPage);

		// last page
		$buttons[] = $this->createPageButton($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);

		// prev page
		if (($page = $currentPage - 1) < 0)
			$page = 0;
		$buttons[] = $this->createPageButton($this->prevPageLabel, $page, $this->previousPageCssClass, $currentPage <= 0, false);

		// next page
		if (($page = $currentPage + 1) >= $pageCount - 1)
			$page = $pageCount - 1;
		$buttons[] = $this->createPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);


		return $buttons;
	}
	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
		if ($hidden || $selected)
			$class .= ' ' . $this->selectedPageCssClass;
		return '<li class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page)) . '</li>';
	}

}
