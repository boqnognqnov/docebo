<?php

class AppsMpApiClient {

	// can't extend parent class ApiClient so we have to duplicate this..
	// solution will be available with php 5.3 using static:: instead of self::
	static public function getOptions($url, $data_params) {
		$http_header = self::getHeaders($data_params);

		$opt = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HTTPHEADER => $http_header,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data_params,
		);

		return $opt;
	}


	static public function apiGetHash($data_params, $api_key, $api_secret) {
		if (empty($api_key) || empty($api_secret)) {
			die('Invalid key/secret');
		}
		$res = array('sha1' => '', 'x_auth' => '');

		$params = array();
		foreach($data_params as $k => $v) {
			if (substr($v, 0, 1) != '@') {
				$params[$k] = $v;
			}
		}

		$res['sha1'] = sha1(implode(',', $params) . ',' . $api_secret);

		$res['x_auth'] = base64_encode($api_key . ':' . $res['sha1']);

		return $res;
	}


	/**
	 * @static
	 * @param $url
	 * @param $data_params
	 * @return mixed|bool
	 */
	static public function execApiCall($url, $data_params) {
		$curl = curl_init();
		curl_setopt_array($curl, self::getOptions($url, $data_params));

		$output = curl_exec($curl);

		// it closes the session
		curl_close($curl);
		return $output;
	}


	// ---------------------------------------------------------------------------


	static public function getUrl($action) {
		$base_url = Yii::app()->params['erp_url'];

		//$url = 'http://127.0.0.1/docebo/erp/svn/trunk/www/index.php?r=erpApi/'.$action;
		//$url = 'http://erp.docebo.com/www/index.php?r=erpApi/'.$action;
		$url = $base_url . 'www/index.php?r=appsMpApi/' . $action; // DO NOT REMOVE the www/ !


		return $url;
	}


	static public function getHeaders($data_params) {
		$erp_key 		= Settings::getCfg('erp_key','');
		$erp_secret_key = Settings::getCfg('erp_secret_key','');

		$hash_info = self::apiGetHash($data_params, $erp_key, $erp_secret_key);
		$http_header = array(
			"Content-Type: multipart/form-data",
			'X-Authorization: DoceboErp ' . $hash_info['x_auth'],
		);

		return $http_header;
	}

	/**
	 * Returns an array of app images
	 *
	 * @param string $filter all|free|paid|my_apps
	 * @return array
	 */
	public static function getAppsImages($filter = 'all') {
		$images = array();
		try {
			// Call ERP to get all My Apps
			$params = array(
				'installation_id' => Docebo::getErpInstallationId(),
				'filter' => $filter,
				'lang' => Yii::app()->getLanguage(),
			);
			$apiResponse = self::apiListAvailableApps($params);
			$response = CJSON::decode($apiResponse);
			if(!$response) {
				Yii::log('Cannot retrieve apps from ERP. Invalid JSON response. (' . $apiResponse . ')', CLogger::LEVEL_ERROR);
			} else {
				if(isset($response['success']) && $response['success']===true) {
					$apps = $response['data'];
					if(!empty($apps)) {
						foreach($apps as $app) {
							if (is_array($app['image'])) {
								$imgUrls = array_values($app['image']);
								$appImage = $imgUrls[0];
							} else
								$appImage = $app['image'];

							$images[$app['internal_codename']] = $appImage;
						}
					}
				} else
					Yii::log('ERP apps api failed with message: '. isset($response['msg']) ? $response['msg'] : null, CLogger::LEVEL_INFO);
			}
		} catch(Exception $e) {
			Yii::log('Cannot retrieve apps from ERP. Error: '.$e->getMessage(), CLogger::LEVEL_ERROR);
		}

		return $images;
	}

	/**
	 * List available / "my" apps
	 * params['installation_id']
	 * params['filter'] = all|free|paid|my_apps
	 * params['lang'] = en|it|... OPTIONAL - defaults to english (en)
	 */
	public static function apiListAvailableApps($data_params) {

		$url = self::getUrl('listAvailableApps');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}

	public static function apiSetShopifyDomain($data_params) {

		$url = self::getUrl('setShopifyDomain');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}
	/**
	 * Get the details of a specified app
	 * params['installation_id']
	 * params['app_id']
	 * params['lang'] = en|it|... OPTIONAL - defaults to english (en)
	 */
	public static function apiGetAppDetails($data_params) {

		$url = self::getUrl('getAppDetails');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	/**
	 * Install this app for the current installation
	 * The api should activate the app and schedule payment if required
	 *
	 * params['installation_id']
	 * params['app_id']
	 */
	public static function apiAddApplication($data_params) {

		$url = self::getUrl('addApplication');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	/**
	 * Remove this app for the current installation
	 *
	 * params['installation_id']
	 * params['app_id']
	 */
	public static function apiAppCancel($data_params) {

		$url = self::getUrl('appCancel');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	/**
	 * Reactivates an app for the current installation
	 *
	 * params['installation_id']
	 * params['app_id']
	 */
	public static function apiAppReactivate($data_params) {

		$url = self::getUrl('reactivateSubscription');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	/**
	 * Reactivates a subscription app for the current installation
	 *
	 * params['installation_id']
	 * params['app_id']
	 */
	public static function apiAppReactivateSubscription($data_params) {

		$url = self::getUrl('reactivateSubscription');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	/**
	 * Reactivates an one time app for the current installation
	 *
	 * params['installation_id']
	 * params['app_id']
	 */
	public static function apiAppReactivateOneTimeApp($data_params) {

		$url = self::getUrl('reactivateOneTimeApp');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}

}

?>