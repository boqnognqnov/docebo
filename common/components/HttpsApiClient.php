<?php
/**
 * A component dedicated to communicate with Https Service API to upload & install SSL, generate CSR and so on
 *
 */
class HttpsApiClient extends CApplicationComponent {

	/**
	 * Https Agen actions
	 * 
	 * @var string
	 */
	const API_ACTION_GENCSR			= 'NewCSR';
	const API_ACTION_INSTALL_SSL	= 'SetupCert';
	const API_ACTION_CHECK_CERT		= 'CheckCert';
	
	/**
	 * Component parameters, set in Yii application configuration
	 * 
	 * @var mixed
	 */
	public $apiHost = 'localhost';
	public $apiPort = false;
	public $httpSchema = 'http';
	
	
	// ---
	private $_enabled = false;
	private $_lastError = '';

	// Sometimes used to test with fiddler
	public $proxyHost = false;
	public $proxyPort = false;
	
	// Set to true for development
	public $emulateApiSuccess = false;
	
	
	/**
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		
		parent::init();
		
		if (!$this->apiHost) {
			$this->_enabled = false;
			$this->_lastError = 'Invalid Https API setting(s) (empty). Please check LMS main configuration.';
			Yii::log($this->_lastError, 'error');
			return;
		}			
		$this->_enabled = true;
		
		Yii::import('common.extensions.httpclient.*');
		
		
	}
	

	/**
	 * 
	 * @param unknown $action
	 * @return string
	 */
	public function buildUrl($action) {
		$url = $this->httpSchema . '://' . $this->apiHost . ($this->apiPort ? ':' . $this->apiPort : '') . '/api/' . $action;
		return $url;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	public function isEnabled() {
		return $this->_enabled;
	}
	
	/**
	 * 
	 */
	public function installSsl($inputData) {
		
		$url = $this->buildUrl(self::API_ACTION_INSTALL_SSL);
		
		$client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true, 
		));
		
		
		foreach ($inputData as $name => $value) {
			$client->setParameterPost($name, $value);
		}
		
		$response = $client->request('POST');
		
		$result = array(
			'success'	=> (!$client->getLastResponse()->isError() && $client->getLastResponse()->isSuccessful()) || $this->emulateApiSuccess,
			'error'		=> $client->getLastResponse()->getMessage(),
			'status'	=> $client->getLastResponse()->getStatus(),
		);
		
		return $result;
		
	}
	
	
	public function checkCertificate($inputData) {

		$url = $this->buildUrl(self::API_ACTION_CHECK_CERT);
		
		$client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true,
		));
		
		
		Yii::log('API Call to Https Agent: Check Certificate, data:', CLogger::LEVEL_INFO);
		Yii::log(var_export($inputData,true), CLogger::LEVEL_INFO);
		Yii::log('URL: ' . $url, CLogger::LEVEL_INFO);
		
		foreach ($inputData as $name => $value) {
			$client->setParameterPost($name, $value);
		}
		
		$response = $client->request('POST');
		
		Yii::log('API Response from Https Agent: Check Certificate, data:', CLogger::LEVEL_INFO);
		Yii::log(var_export($response,true), CLogger::LEVEL_INFO);
		
		$result = array(
			'success'	=> (!$client->getLastResponse()->isError() && $client->getLastResponse()->isSuccessful()) || $this->emulateApiSuccess,
			'error'		=> $client->getLastResponse()->getMessage(),
			'status'	=> $client->getLastResponse()->getStatus(),
		);
		
		return $result;
		
		
	}
	
	
	/**
	 *
	 */
	public function requestCsr($inputData) {
		
		
		$url = $this->buildUrl(self::API_ACTION_GENCSR);
	
		$client = new EHttpClient($url, array(
			'adapter'      => 'EHttpClientAdapterCurl',
			'proxy_host'   => $this->proxyHost,
			'proxy_port'   => $this->proxyPort,
			'disable_ssl_verifier' => true,
		));
	
		Yii::log('API Call to Https Agent: Generate CSR, data:', CLogger::LEVEL_INFO);
		Yii::log(var_export($inputData,true), CLogger::LEVEL_INFO);
		Yii::log('URL: ' . $url, CLogger::LEVEL_INFO);
				
		foreach ($inputData as $name => $value) {
			$client->setParameterPost($name, $value);
		}
	
		$response = $client->request('POST');
		
		Yii::log('API Response from Https Agent: Generate CSR, data:', CLogger::LEVEL_INFO);
		Yii::log(var_export($response,true), CLogger::LEVEL_INFO);
		
	
		$result = array(
			'success'	=> (!$client->getLastResponse()->isError() && $client->getLastResponse()->isSuccessful())  || $this->emulateApiSuccess,
			'error'		=> $client->getLastResponse()->getMessage(),
			'status'	=> $client->getLastResponse()->getStatus(),
		);
	
		return $result;
	
	}
	
	

	/**
	 * 
	 * @param CoreHttps $httpsModel
	 * @param string $apiAction
	 */
	public static function generateCallbackUrl(CoreHttps $httpsModel, $apiAction) {
		$url = Docebo::createAbsoluteAdminUrl('https/agent', array(
			'apiAction' 	=> $apiAction, 
			'id' 			=> $httpsModel->id, 
			'token' 		=> $httpsModel->httpsAgentToken
		), 'http');  // forcing HTTP schema for callbacks
		
		return $url;
	}
	
	
}