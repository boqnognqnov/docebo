<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 */
class ErpApiClient2 extends ApiClient {

    // API constants, please align them to ERP
    const API_TERMINATE_SUBSCRIPTION_CLICK        = 'terminate_subscription_click';
    const API_EXCEEDED_PLAN 			  	      = 'exceeded_plan';
    

	function getApiKey () {
		return Settings::getCfg( 'erp_key', '' );
	}

	function getApiSecret () {
		return Settings::getCfg( 'erp_secret_key', '' );
	}

	public function call ( $apiAction ) {
		$this->params['installation_id'] = self::getInstallationId();
		return parent::call( $apiAction );
	}


	/**
	 * Return base url of the target API server
	 * @return mixed
	 */
	function getApiBaseUrl () {
		return Yii::app()->params['erp_url'] . 'en/api/';
	}

	static public function getUrl ( $action ) {
		return self::getApiBaseUrl() . $action;
	}

	public static function getInstallationId () {
		return Docebo::getErpInstallationId();
	}

	public function saveInstallationEmails ( array $billingEmails, array $contactEmails ) {
		$emailsData            = array();
		$emailsData['billing'] = $billingEmails;
		$emailsData['product'] = $contactEmails;

		$this->params['emails'] = CJSON::encode( $emailsData );

		$output = $this->call( 'lms/communication-contacts-saving' );

		return $output;
	}

	public function getInstallationEmails () {
		$output = $this->call( 'lms/communication-contacts-list' );

		return $output;
	}

	public function getSandboxExpireDate(){
		$output = $this->call('lms/sandbox-info');

		$isAvailable = $output['show'];
		$sandboxExpireDate = $output['expire_date'];

		return $sandboxExpireDate;
	}

	public function getIsSandboxAvailable()
	{
		$output = $this->call('lms/sandbox-info');

		$isAvailable = $output['show'];
		$sandboxExpireDate = $output['expire_date'];

		return (bool)$isAvailable;
	}

	public function createSandbox()
	{
		$output = $this->call('lms/sandbox-creation');
		return $output;
	}
	
	public function getInstallationCrmData () {
	    $output = $this->call( 'lms/get-crm-data' );
	    if (is_array($output)) return $output;
	    return false;
	}
	
	/**
	 * Return list of possible company departments.
	 */
	public static function getDepartments() {
	    
	    if (strtolower(Yii::app()->getLanguage()) === "it") {
	        $deps =  array(
	            "talent"       => "Organizzazione & Gestione Risorse Umane",
	            "learning"     => "Formazione & sviluppo",
	            "channel"      => "Gestione Partnership",
	            "support"      => "Customer Care",
	            "sales"        => "Sale/Rete Vendita",
	            "compliance"   => "Compliance/Risk",
	            "it"           => "IT",
	            "other"        => "Altro",
	        );
	    }
	    else {
	        $deps =  array(
	            "talent"       => Yii::t("setup", "People/Talent Development"),
	            "learning"     => Yii::t("setup", "Learning and Development"),
	            "channel"      => Yii::t("setup", "Channel/Partner Development"),
	            "support"      => Yii::t("setup", "Customer Support/Success"),
	            "sales"        => Yii::t("setup", "Sales Enablement"),
	            "compliance"   => Yii::t("setup", "Compliance/Risk"),
	            "it"           => Yii::t("setup", "IT"),
	            "other"        => Yii::t("setup", "Other"),
	        );
	    }
	    
	    return $deps;
	    
	}
	
	
	/**
	 * Return list of possible values
	 */
	public static function getJobRoles() {
	    
	    if (strtolower(Yii::app()->getLanguage()) === "it") {
	        $r =  array(
	            "c_level"                  => "C-level / SVP",
	            "vp_director"              => "VP / Director",
	            "manager"                  => "Manager",
	            "individual_contractor"    => "Ditta individuale",
	            "student_intern"           => "Studente / Stagista",
	            "other"                    => "Altro",
	        );
	    }
	    else {
	        $r =  array(
	            "c_level"                  => "C-level / SVP",
	            "vp_director"              => "VP / Director",
	            "manager"                  => "Manager",
	            "individual_contractor"    => "Individual contractor",
	            "student_intern"           => "Student / intern",
	            "other"                    => "Other",
	        );
	    }
	     
	    return $r;
	     
	}
	
	
	/**
	 * Return list of possible values
	 */
	public static function getElearningChallenges() {
	     
	    if (strtolower(Yii::app()->getLanguage()) === "it") {
	        $r =  array(
	            "internal_training"                => "Training interno",
	            "employee_onboarding"              => "Onboarding Neoassunti",
	            "sales_channel_enablement"         => "Formazione Rete Vendita",
	            "partner_channel_enablement"       => "Formazione Rete Partner",
	            "customer_training"                => "Formazione Clienti",
	            "hard_to_access_knowledge"         => "Valorizzare le competenze dell'azienda",
	            "retaining_talent"                 => "Riduzione del Turnover",
	            "none_of_the_above"                => "Altro",
	        );
	    }
	    else {
	        $r =  array(
	            "internal_training"                => "Internal training",
	            "employee_onboarding"              => "Employee onboarding",
	            "sales_channel_enablement"         => "Sales channel enablement",
	            "partner_channel_enablement"       => "Partner channel enablement",
	            "customer_training"                => "Customer training",
	            "hard_to_access_knowledge"         => "Hidden/hard to access knowledge",
	            "retaining_talent"                 => "Retaining talent",
	            "none_of_the_above"                => "Other/None of the above",
	        );
	    }
	
	    return $r;
	
	}
	

	/**
	 * Return list of possible values
	 */
	public static function getAlreadyLmsOprions() {
	
	    if (strtolower(Yii::app()->getLanguage()) === "it") {
	        $r =  array(
	            "not_looking_to_change" => "Sì, e non intendo cambiarlo",
	            "considering_replacing" => "Sì, ma sto valutando di cambiarlo",
	            "not_interested_in_lms" => "No, e non sono interessato ad un LMS",
	            "evaluating_lms_providers" => "No, ma sto valutando di acquistarne uno",
	        );
	    }
	    else {
	        $r =  array(
	            "not_looking_to_change" => "Yes, not looking to change",
	            "considering_replacing" => "Yes, but we’re considering replacing it",
	            "not_interested_in_lms" => "No, I’m not interested in an LMS",
	            "evaluating_lms_providers" => "No, but I’m evaluating LMS providers",
	        );
	    }
	
	    return $r;
	
	}
	
	/**
	 * Return list of possible values
	 */
	public static function getEndUsersServe() {
	
	    if (strtolower(Yii::app()->getLanguage()) === "it") {
	        $r =  array(
				"users_elearning_one_hundred" => "0 - 100",
				"users_elearning_two_hundred_half" => "100 - 250",
				"users_elearning_three_hundred_half" => "250 - 350",
				"users_elearning_five_hundred" => "350 - 500",
				"users_elearning_five_thousand" => "500 - 5,000",
				"users_elearning_ten_thousand" => "5,000 - 10,000",
				"users_elearning_one_hundred_thousand" => "10,000 - 100,000",
				"users_elearning_more_than_one_hundred_thousand" => "+100,000"
	        );
	    }
	    else {
	        $r =  array(
				"users_elearning_one_hundred" => "0 - 100 users",
				"users_elearning_two_hundred_half" => "100 - 250 users",
				"users_elearning_three_hundred_half" => "250 - 350 users",
				"users_elearning_five_hundred" => "350 - 500 users",
				"users_elearning_five_thousand" => "500 - 5,000 users",
				"users_elearning_ten_thousand" => "5,000 - 10,000 users",
				"users_elearning_one_hundred_thousand" => "10,000 - 100,000 users",
				"users_elearning_more_than_one_hundred_thousand" => "+100,000 users"
	        );
	    }
	
	    return $r;
	
	}
	
	
	
	/**
	 * Send array of data to ERP through lms/save-history endpoint
	 * 
	 * @param array $data
	 */
	public function lmsSaveHistory($data=array()) {
	    $this->params = $data;
	    $output = $this->call( 'lms/save-history' );
	    return $output;
	}
	
}