<?php
/**
 * 
 * 
 *
 */
class DoceboAssetManager extends CAssetManager {
	
	const SHARED_S3 = 's3';
	const SHARED_FS = 'filesystem';
	
	/**
	 * File used to collect all published asset paths during the script run. It will be saved in "upload-tmp" folder.
	 * @var string
	 */
	public $assetPathsFilename="asset_paths.txt";

	/**
	 * Should this component collect all published paths (in a persistent storage) or not.
	 * This is to help us collect all published paths through Asset Manager. Should stay FALSE and only temporary set to TRUE.
	 * Note, collected paths are relative to LMS root!
	 * @var string
	 */
	public $collectPublishedPaths = false;
	
	/**
	 * When assets are generated, their hash is based on the directory (or file path) and the mtime() of that directory (or file).
	 * 
	 * In case we save assets in a shared storage (shared between different servers/hosts/platforms), it is a good idea to avoid using the mtime().
	 * That way, the hash would be equal, regardless where (and when) that directory or file is created (on which server, platform). 
	 *  
	 * @var bool
	 */
	public $hashByName = false;
	

	/**
	 * Type of the shared storage (S3, Shared File System, etc.)
	 * @var string
	 */
	public $sharedStorage = self::SHARED_S3;
	
	/**
	 * Keep list of already published paths (for this script pnly!!)
	 * @var array
	 */
	protected $alreadyPublished = array();
	
	/**
	 * Array of paths (directory or file) which are going to be shared (saved in shared storage, e.g. S3) and used by different LMS hosts/servers.
	 * This is hardcoded list. Built manually. 
	 * 
	 * It is NOT enough just to list assets folder (or file) here. This is not a magic wand!
	 * That published asset MUST be used in proper way in the code, e.g. using $assetManager->publish()! 
	 * 
	 * @var array
	 */
	protected static $sharedAssetPaths = array(
		"admin/js",
		"api/protected/modules/swagger/assets",
		"app7020/assets",
		"authoring/css",
		"authoring/js",
		"common/components/doceboFilterIcon/assets",
		"common/components/sandboxCreator/assets",
		"common/extensions/JsTrans/assets",
		"common/extensions/bootstrap/assets",
		"common/extensions/dialog2/assets",
		"common/extensions/dzRaty/assets",
		"common/extensions/engineio/assets",
		"common/extensions/fancybox/assets",
		"common/extensions/fancytree",
		"common/extensions/fontawesome/assets",
		"common/extensions/jplayer/assets",
		"common/extensions/jqm/assets",
		"common/extensions/jqplot/assets",
		"common/extensions/plupload/assets",
		"common/extensions/plupload/widgets/assets/singlefile",
		"common/extensions/tabbed/assets",
		"common/extensions/tinymce/assets",
		"common/widgets/warningStrip/assets",
		"lms/protected/modules/backgroundjobs/assets",
		"lms/protected/modules/billing/assets",
		"lms/protected/modules/centralrepo/assets",
		"lms/protected/modules/channels/assets",
		"lms/protected/modules/curricula/assets",
		"lms/protected/modules/elasticsearch/assets",
		"lms/protected/modules/forum/assets",
		"lms/protected/modules/inbox/assets",
		"lms/protected/modules/mobileapp/assets",
		"lms/protected/modules/mycalendar/assets",
		"lms/protected/modules/mydashboard/assets",
		"lms/protected/modules/player/assets",
		"lms/protected/modules/player/assets/local/widgets/assets",
		"lms/protected/modules/poll/assets",
		"lms/protected/modules/test/assets",
		"lms/protected/modules/video/assets",
		"lms/protected/modules/webinar/assets",
		"lms/protected/modules/webinar/widgets/assets",
		"lms/protected/modules/xapi/assets",
		"plugins/AutomationApp/assets",
		"plugins/AutomationApp/components/rules/assets",
		"plugins/CertificationApp/assets",
		"plugins/ClassroomApp/assets",
		"plugins/CurriculaApp/assets",
		"plugins/ElucidatApp/assets",
		"plugins/GamificationApp/assets",
		"plugins/LdapApp/assets",
		"plugins/LectoraApp/assets",
		"plugins/MultidomainApp/assets",
		"plugins/MyBlogApp/assets",
		"plugins/NotificationApp/assets",
		"plugins/OfflinePlayerApp/assets",
		"plugins/SalesforceApp/assets",
		"plugins/ShopifyApp/assets",
		"plugins/SimpleSamlApp/assets",
		"plugins/SlackApp/assets",
		"plugins/SubscriptionCodesApp/assets",
		"plugins/TranscriptsApp/assets",
		"plugins/WebApp/assets",
		"plugins/ZoomApp/assets",
		"yii/gii/assets",
		"yii/web/js/source",
		"yii/web/widgets/pagers/pager.css",
		"yii/zii/widgets/assets",
	);
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		parent::init();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see CAssetManager::publish()
	 */
	public function publish($path,$hashByName=false,$level=-1,$forceCopy=null) {
		/*
		 * STILL NOT USED!!
		// Spawn a descendant class object, depending on the shared storage type
		// This is not singleton pattern!! Watch out!!
		$relative = $this->getRelativePath($path);
		if (in_array($relative, self::$sharedAssetPaths)) {
			switch ($this->sharedStorage) {
				case self::SHARED_S3: 
					$assetManagerS3 = Yii::app()->getComponent('assetManagerS3');
					if ($assetManagerS3) {
						return $assetManagerS3->publish($path, $hashByName, $level, $forceCopy);
					}
					else {
						throw new Exception("Unable to create Application component assetManagerS3");
					}
					break;
				default:
					break;
			}
		}
		*/
		
		if (!$this->alreadyPublished[$path] && $this->collectPublishedPaths) {
			$this->collectPublishedPath($path);
		}
		if (isset(Yii::app()->params['forceCopyAssets']) && Yii::app()->params['forceCopyAssets'] && $forceCopy === null) {
			$forceCopy = true;
		}
		$url = parent::publish($path,$hashByName,$level,$forceCopy);
		if ($url) {
			$this->alreadyPublished[$path] = $url;	
		}
		
		return $url;

	}
	
	/**
	 * Add (if not already there) a path to a file for later usage, if at all. 
	 * This is sort of helper method when we need to have a list of all paths, which are published through Yii Asset Manager.
	 * Note, collected paths are relative to LMS root! 
	 * 
	 * See self::collectPublishedPaths
	 * 
	 * @param string $path
	 */
	protected function collectPublishedPath($path) {
		if (empty($path)) {
			return;
		}
		$path = realpath($path);
		// If still something, we are ok
		if (!empty($path)) {
			$path = $this->getRelativePath($path);
			$assetPathsCollectionFile = Docebo::getUploadTmpPath() . "/" . $this->assetPathsFilename;
			$collection = array();
			if (is_file($assetPathsCollectionFile) && !empty($data = file_get_contents($assetPathsCollectionFile))) {
				$collection = explode("\n", $data);
			}
			$collection[] = $path;
			$collection = array_unique($collection);
			asort($collection);
			file_put_contents($assetPathsCollectionFile, implode("\n", $collection));
		}
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see CAssetManager::hash()
	 */
	protected function hash($path)
	{
		$path = $this->getRelativePath($path);
		return sprintf('%x',crc32($path.Docebo::platformVersion()));
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see CAssetManager::generatePath()
	 */
	protected function generatePath($file,$hashByName=false)
	{
		// If we want to share assets between LMS hosts/platforms, set hashByName to true
		if ($this->hashByName === true) {
			$hashByName = true;
		}
		return parent::generatePath($file, $hashByName);
	}

	/**
	 * Return the relative part of the path only. Relative to LMS root.
	 * 
	 * @param string $path
	 * @return string
	 */
	protected function getRelativePath($path) {
		$lmsRootPath = Docebo::getRootBasePath();
		$relative = $path;
		if (substr($path, 0, $l=strlen($lmsRootPath)) == $lmsRootPath) {
			$relative = ltrim(substr($path, $l),"/\\");
		}
		return $relative;
	}
	
}