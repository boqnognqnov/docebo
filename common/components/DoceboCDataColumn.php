<?php
Yii::import('zii.widgets.grid.CDataColumn');
class DoceboCDataColumn extends CDataColumn {
	public $inlineEdit = array();
	public $toolTip = array();
	public $toolTipClass = 'gridToolTip';
	public $toolTipUrl;
	private $_cellContent;
	private $_row;
	private $_model;
	private $_pk;
	private $_value;
	private $_ajaxUrl;
	
	/*
	 * Modifying this method to add inline edit functionality for each cell
	 */
	protected function renderDataCellContent($row,$data)
	{
		$this->_row = $row;
		$this->_model = isset($this->inlineEdit['model']) ? $this->inlineEdit['model'] : $data;

		if($this->value!==null)
			$value=$this->evaluateExpression($this->value,array('data'=>$data,'row'=>$row));
		else if($this->name!==null)
			$value=CHtml::value($data,$this->name);
		
		$this->_cellContent = $value===null ? $this->grid->nullDisplay : $this->grid->getFormatter()->format($value,$this->type);

		if (isset($this->inlineEdit['pk']))
			$this->_pk = $this->evaluateExpression($this->inlineEdit['pk'],array('data'=>$data,'row'=>$row));
		else
			$this->_pk = $this->_model->primaryKey;

		if (isset($this->inlineEdit['value']))
			$this->_value = $this->evaluateExpression($this->inlineEdit['value'],array('data'=>$data,'row'=>$row));
		else
			$this->_value = $this->_cellContent;
		
		if (isset($this->toolTip['url']))
			$this->toolTipUrl = $this->evaluateExpression($this->toolTip['url'],array('data'=>$data,'row'=>$row));
		
		if (isset($this->inlineEdit['ajaxUrl']))
			$this->_ajaxUrl = $this->inlineEdit['ajaxUrl'];
		else
		{
			$this->_ajaxUrl = $this->grid->inlineEditAjaxUrl;
		}
		
		if ($row == 0 && isset($this->inlineEdit['type']))
			echo '<div style="display:none;"><div id="inlineForm_'.$this->name.'">'.$this->renderInlineEditForm().'</div></div>';
		
		if (isset($this->inlineEdit['type']))
			echo $this->renderInlineEditPrepare();
		else
		{
			if (isset($this->toolTip['url']))
				echo '<span onmouseover="gridToolTipInit(this);" onmouseout="gridToolTipClose();" href="'.$this->toolTipUrl.'" class="'.$this->toolTipClass.'">'.$this->_cellContent.'</span>';
			else
				echo $this->_cellContent;
		}
	}
	
	protected function renderInlineEditForm()
	{
		switch ($this->inlineEdit['type'])
		{
			case 'textField':
				return $this->renderInlineTextFieldForm();
				break;
			case 'dropDownList':
				return $this->renderInlineDropDownListForm();
				break;
			case 'textArea':
				return $this->renderInlineTextAreaForm();
				break;
			case 'checkBox':
				return $this->renderInlineCheckBoxForm();
				break;
			case 'calendar':
				return $this->renderInlineCalendarForm();
				break;
			default:
				throw new CException('Invalid "type" for inlineEdit.');
		}
	}
	
	/**
	 * Modifying cell data to fit inline edit requirements
	 */
	protected function renderInlineEditPrepare()
	{
		$mouseOver = (isset($this->toolTip['url'])) ? 'onmouseover="gridToolTipInit(this);"' : '';
		$mouseOut = (isset($this->toolTip['url'])) ? 'onmouseout="gridToolTipClose();"' : '';
		$toolHref = (isset($this->toolTip['url'])) ? 'href="'.$this->toolTipUrl.'"' : '';
		return '<div class="inlineEditTextField">
					<span '.$toolHref.' '.$mouseOver.' '.$mouseOut.' onclick="initInlineEdit(this, '.$this->_pk.');" alt="'.$this->_value.'">'.$this->_cellContent.'</span>
					<div class="inlineEditBox" alt="'.$this->name.'"></div>
				</div>';
	}
	
	protected function renderInlineTextFieldForm()
	{
		return $this->renderInlineForm(CHtml::activeTextField($this->_model, $this->name));
	}
	
	protected function renderInlineDropDownListForm()
	{
		return $this->renderInlineForm(CHtml::activeDropDownList($this->_model, $this->name, $this->inlineEdit['data'], array('id'=>$this->name.$this->_row)));
	}
	
	protected function renderInlineTextAreaForm()
	{
		return $this->renderInlineForm(CHtml::activeTextArea($this->_model, $this->name, array('rows'=>3)));
	}
	
	protected function renderInlineCheckBoxForm()
	{
		return $this->renderInlineForm(CHtml::activeCheckBox($this->_model, $this->name, array('uncheckValue'=>0)));
	}

	protected function renderInlineCalendarForm()
	{
		$calendar = Yii::app()->controller->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$this->_model,
			'attribute'=>$this->name,
			// additional javascript options for the date picker plugin
			'options'=>array(
				'dateFormat'=>$this->inlineEdit['format'],
			),
			'htmlOptions'=>array(
				'class'=>'inlineEditCalendar',
				'style'=>'height:20px;',
				'alt'=>$this->inlineEdit['format'],
			),
		), true);

		return $this->renderInlineForm($calendar);
	}
	
	/**
	 * @param string $html
	 * @return string Generated form for inline edit
	 */
	protected function renderInlineForm($html)
	{
		return '<form action="'.$this->_ajaxUrl.'" alt="" id="inlineEditForm_'.$this->name.'" onsubmit="submitInlineEdit(\'inlineEditForm_'.$this->name.'\');return false;" method="post" style="margin: 0px;">
					<p style="margin: 0px;">'.$html.'</p>
				</form>
				<span class="inlineEditButtons">
					<input type="button" value="Save" onclick="submitInlineEdit(\'inlineEditForm_'.$this->name.'\');" />
					<input type="button" value="Cancel" onclick="closeInlineEdit();" />
				</span>';
	}
}
?>
