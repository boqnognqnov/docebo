<?php

/**
 * A simple widget for controlling the visibility of some items
 * on the page, e.g. grid columns by displaying a small icon that
 * opens a popover with a vertical list of "items" that can be clicked.
 * Clicking on a item will toggle show/hide a specific selector on the page
 * (e.g. the grid column)
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class DoceboFilterIcon extends CWidget {

	/**
	 * Try to pass as unique ID as possible here in case
	 * there will be multiple widgets of this type on the same page.
	 * However, keep in mind that this widget preserves selections
	 * across page reloads in a session and this ID is used as the
	 * session identifier, so if you use a randomly generated ID here
	 * your selections may not persist because the cookie name will
	 * change on reload.
	 * @var bool
	 */
	public $id = FALSE;

	/**
	 * Array of elements that should be shown in the widget as a list.
	 * Clicking the elements will toggle their corresponding target
	 * "selector" to be hidden/shown. Also, the selections will be
	 * persisted in a cookie
	 * @var array each element is an array with 2 keys
	 *            label (visible in UI as a clickable name)
	 *            and selector (a jQuery selector that is the target
	 *            to which the show/hide is applied)
	 */
	public $list = array();

	/**
	 * A common label visible at the top of the list of items in this widget
	 * @var bool
	 */
	public $label = FALSE;

	public $width = 350; // popover width in pixels

	public function init () {
		if( ! $this->id ) {
			$this->id = md5( rand( 1, 5000 ) . rand( 1, 5000 ) . rand( 1, 5000 ) . rand( 1, 5000 ) );
		}

		$css = Yii::app()->assetManager->publish( dirname( __FILE__ ) . '/assets'). '/docebo_filter_icon.css';
		$js  = Yii::app()->assetManager->publish( dirname( __FILE__ ) . '/assets'). '/docebo_filter_icon.js';
		Yii::app()->clientScript->registerCssFile( $css );
		Yii::app()->clientScript->registerScriptFile( $js );
		Yii::app()->clientScript->registerScriptFile( Yii::app()->theme->baseUrl . '/js/jquery.cookie.js' );
	}

	public function run () {
		$this->render( 'icon_picker' );
	}


}