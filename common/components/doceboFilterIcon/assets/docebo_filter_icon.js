/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
var DoceboGridFilter = {

	instances:   {},
	getInstance: function (id) {
		return this.instances[id];
	},

	id:                          null,
	options:                     {},
	init:                        function (options) {
		this.options = $.extend(this.options, options);

		if (options.id) {
			this.id = options.id;
		} else {
			console.error('Please provide a unique ID while initializing DoceboGridFilter');
			return false;
		}

		// Error validation
		if (!this.validateOptions(this.options)) {
			return false;
		}

		this.restoreSelectionsFromCookie();
		this.attachClickHandlers();

		this.instances[this.id] = this;

		return this;
	},
	getContainer:                function () {
		var container = $('.picker-wrapper-' + this.id);
		if (container.length === 0) {
			console.error('DoceboGridFilter can not find its container');
		} else if (container.length > 1) {
			console.warn('DoceboGridFilter found more than one element with ID ' + this.id + ' on the page. This can cause problems, so try to provide a unique ID for each instance');
		}
		return container;
	},
	validateOptions:       function (options) {
		if (!options.id) {
			console.error('The DoceboGridFilter instance needs a (unique) ID');
			return false;
		}
		if (this.getInstance(options.id) !== undefined) {
			console.error('DoceboGridFilter duplicate IDs found on the same page: ' + options.id);
			return false;
		}

		return true;
	},
	attachClickHandlers:         function () {
		var that = this;
		var container = this.getContainer();

		// Attach click handler to filter icon
		$('.picker-holder', container).click(function (e) {
			e.preventDefault();

			// Show or hide the picker on icon click
			$(this).closest('.picker-wrapper').find('.popover-content').fadeToggle();
		});

		// Attach click handlers to the filters
		$('.popover-content ul li', container).click(function () {
			var selector = $(this).data('selector');
			if($(this).hasClass('locked')) return;

			if ($(this).hasClass('selected')) {
				// Remove selection and hide the clicked column
				$(this).removeClass('selected');
			} else {
                if(container.find('li.selected').length <= 5){
                    $(this).addClass('selected');
                }
			}

			that.saveSelectionsToCookie();

			that.applySelectionsToUI();
		});
	},
	applySelectionsToUI:         function () {
		var container = this.getContainer();

		$('.popover-content ul li', container).each(function () {
			var selector = $(this).data('selector');
			if (selector.length === 0) return;

			// Show or hide the target element,
			// depending on if its enabled in the filter
			if ($(this).hasClass('selected')) {
				$(selector).show();
			} else {
				$(selector).hide();
			}
		});
	},
	saveSelectionsToCookie:      function () {
		var container = this.getContainer();

		var newSelectionsList = [];
		$('.popover-content ul li.selected', container).each(function () {
			newSelectionsList.push($(this).data('selector'));
		});

		$.cookie('grid-filter-' + this.id, JSON.stringify(newSelectionsList), {expires: 30});

		if(typeof console.info != 'undefined')
			console.info('Saving selections to cookie named: ' + 'grid-filter-' + this.id);
	},
	restoreSelectionsFromCookie: function () {
		if(typeof console.info != 'undefined')
			console.info('Restoring old selections from cookie named: ' + 'grid-filter-' + this.id);
		var restoredSelections = $.parseJSON($.cookie('grid-filter-' + this.id));

		var container = this.getContainer();

		if(!restoredSelections){
			// No cookie/selections in session, restore those that are default
			if($('.popover-content ul li.default', container).length===0){
				// No default elements present, just enable all
				$('.popover-content ul li', container).addClass('selected');
			}else{
				$('.popover-content ul li', container).removeClass('selected');
				$('.popover-content ul li.default', container).addClass('selected');
			}
			this.applySelectionsToUI();
			this.saveSelectionsToCookie();
			return;
		}

		$('.popover-content ul li', container).removeClass('selected');

		if (restoredSelections instanceof Array) {
			$('.popover-content ul li', container).each(function () {
				if ($.inArray($(this).data('selector'), restoredSelections) === -1) {
					$(this).removeClass('selected');
				} else {
					$(this).addClass('selected');
				}
			});
		}

		this.applySelectionsToUI();
	}
};