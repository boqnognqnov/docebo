<div class="popover-content" style="<?=($this->width ? 'width:'.$this->width.'px;' : null)?>">
	<? if($this->label): ?>
		<p class="description"><?=Yii::app()->htmlpurifier->purify($this->label)?></p>
	<? endif; ?>
	<ul>
		<? foreach($this->list as $element): ?>
			<li class="selected <?=isset($element['class']) ? $element["class"]: null;?>" data-selector="<?=$element['selector']?>"><?=Yii::app()->htmlpurifier->purify($element['label'])?></li>
		<? endforeach; ?>
	</ul>
</div>