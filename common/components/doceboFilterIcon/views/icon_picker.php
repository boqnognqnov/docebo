<div class="picker-wrapper picker-wrapper-<?=$this->id?>">
	<div class="picker picker-holder">
		<a href="#"></a>
	</div>

	<? $this->render('picker_list') ?>
</div>

<script type="text/javascript">
$(function(){
	// Use DoceboGridFilter.getInstance(id) to get the instance
	// and be able to call API methods (after init() )
	DoceboGridFilter.init({
		id: '<?=$this->id?>'
	});

});
</script>