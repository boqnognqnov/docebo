<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class Docebo extends CComponent {

    const SORT_TYPE_STRING      = "string";
    const SORT_TYPE_TIMESTAMP   = "timestamp";
    const SORT_TYPE_NUMBER      = "number";
    const SORT_DIR_DESC         = "desc";
    
	/**
	 * Normalized Export type names
	 * @var string
	 */
	const EXPORT_TYPE_CSV 	= 'csv';
	const EXPORT_TYPE_XLS 	= 'xls';
	const EXPORT_TYPE_HTML 	= 'html';
	const EXPORT_TYPE_PDF 	= 'pdf';

	/**
	 * Export file extensions
	 * @var string
	 */
	const EXPORT_EXT_CSV 	= 'csv';
	const EXPORT_EXT_XLS 	= 'xls';
	const EXPORT_EXT_HTML 	= 'html';
	const EXPORT_EXT_PDF 	= 'pdf';


	/**
	 * Environment types
	 * @var string
	 */
	const ENVTYPE_SAAS = 'saas';
	const ENVTYPE_GAPPS = 'gapps';

	/**
	 * Payment methods (not payment processors!!!)
	 * Must cope with values defined in ERP!
	 *
	 * @var string
	 */
	const PAYMENT_METHOD_CREDIT_CARD = 'credit_card';
	const PAYMENT_METHOD_WIRE_TRANSFER = 'wire_transfer';
	public static $PAYMENT_METHOD = array(
			'credit_card' => 'Credit Card',
			'wire_transfer' => 'Wire transfer',
	);

	/**
	 * List of routes that pass through the Trial Expired popup
	 *
	 * @var array
	 */
	public static $trialExpiredSafeRoutes = array(
		'site/logout',
		'billing/default/upgrade',
	);

	/**
	 * Known app names, used for urls processing
	 * TO DO: these should not be hardcoded, but imported from somewhere when initializating application
	 * @var array
	 */
	protected static $knownAppNames = array('lms', 'admin', 'authoring', 'api', 'webapp', 'app7020');
	public static function getKnownAppNames() { return self::$knownAppNames; }


	/**
	 * This method is called before the Yii::app()->run();
	 * @param $environment string lms|adm
	 */
	public static function beforeRun($environment) {

		// Be sure mb_* functions are using THIS encoding. Helps a lot!! Especially if PHP internal_encoding is not set to UTF-8!
		mb_internal_encoding('UTF-8');  

		// Check & Purify the request, looking for bad things (XSS, ...)
		// PLEASE DO NOT UNCOMMENT UNLESS YOU KNOW WHAT ARE YOU DOING!!!! [plamen]
		// self::purifyRequest();

		// -- setup themes path: ----------------------------------
		// Set the correct path for the themes folder that is shared
		Yii::app()->themeManager->setBasePath(dirname(Yii::app()->getRequest()->getScriptFile()) . DIRECTORY_SEPARATOR . '../themes');

		// Using DIRECTORY_SEPARATOR for Paths is Ok, but NOT for URLs. (Windows)
		Yii::app()->themeManager->setBaseUrl(Docebo::getRootBaseUrl() . '/themes');

		// -- setup user from old lms: ----------------------------
		Yii::app()->user->checkOldLmsSession();

		// -- Lets resolve language issue -------------------------
		self::resolveLanguage();

		// Managing the response for wrapping the legacy responses in an iframe
		Yii::app()->legacyWrapper->interceptRequest();

		if (isset($_GET['r']) && $_GET['r']==='site/isAlive') {
			// "health check" URL should never load plugins
		} else {
			// -- Load the plugins (as Yii Modules): ------------------
			PluginManager::loadActivePlugins();
		}
	}


	/**
	 * Return the installation id corresponding to the backup_list
	 * table of the ERP (from the config / "specific" config file)
	 */
	public static function getErpInstallationId() {
		return (int) Settings::getCfg('erp_installation_id');
	}


	/**
	 * Return true if the SAAS installation is currently in the trial
	 * period.
	 *
	 * @return bool
	 */
	public static function isTrialPeriod() {
		$max_users = Settings::get('max_users');
		return ($max_users > 0 ? FALSE : TRUE);
	}


	/**
	 * Returns an unix timestamp (2013-01-31 23:59:59) representing the datetime when the trial period expires.
	 * Always ending with 23:59:59
	 *
	 * @return int
	 */
	public static function getTrialExpirationDateTime() {
		$trial_expiration_date = Settings::get('trial_expiration_date');
		if (!$trial_expiration_date) {
			return FALSE;
		}
		$trial_expiration_datetime = date('Y-m-d', strtotime($trial_expiration_date)) . " 23:59:59";
		return $trial_expiration_datetime;
	}


	/**
	 * Return time (in seconds) remaining until end of trial period
	 */
	public static function getTrialRemainingTime() {
		$trial_expiration_datetime = self::getTrialExpirationDateTime();
		if ($trial_expiration_datetime) {
			return strtotime($trial_expiration_datetime)-time();

		} else {
			return FALSE;
		}
	}


	/**
	 * Returns the number of (whole) days remaining for the trial period.
	 * 0 (zero) means 0 whole days, i.e. less than a day.
	 * -1 means no time remained, i.e. passed/expired
	 * @return int
	 */
	public static function getTrialRemainingDays() {
		$trial_remaining_days = -1; // meaning, it passed (expired)
		$trial_remaining_time = self::getTrialRemainingTime();
		if ($trial_remaining_time > 0) {
			$trial_remaining_days = (int)floor($trial_remaining_time/86400);
		}

		return $trial_remaining_days;
	}


	/**
	 * Returns true if the trial period is expired.
	 * Optionally, accepts a DATETIME parameter as expiration datetime to check
	 *
	 * @param bool $datetime
	 * @return bool
	 */
	public static function isTrialExpired($datetime = FALSE) {
		if ($datetime == FALSE) {
			$trial_expiration_date = Settings::get('trial_expiration_date');
		} else {
			$trial_expiration_date = $datetime;
		}
		$trial_expiration_time = self::endOfDayTime($trial_expiration_date);
		return (time() > $trial_expiration_time);
	}


	/**
	 * Return true if the platform is in the so called "grace period"
	 *
	 * @deprecated We don't use this anymore as we just rely on the expired flag.
	 * @return bool
	 */
	public static function isGracePeriod() {
		$grace_period = Settings::get('grace_period');
		return ($grace_period ? TRUE : FALSE);
	}


	/**
	 * Return true if the main subscription / platform is expired
	 *
	 * @return bool
	 */
	public static function isExpiredPeriod() {
		$expired_period = Settings::get('expired_period');
		return ($expired_period ? TRUE : FALSE);
	}


	public static function isSaas() {
		return (Settings::getCfg('install_from') == 'saas_docebo' || Settings::getCfg('install_from') == 'gapps_docebo');
	}

	/**
	 * Checks whether the current installation is an SMB (Small Medium Business) or an Enterprise
	 * The criteria is based on the "pp_end_users" value returned from ERP
	 *
	 * @return bool
	 */
	public static function isSMB() {
		try {
			$api       = new ErpApiClient2();
			$crmData   = $api->getInstallationCrmData();
			$pCompanySize = (isset($crmData['pp_end_users']) ? $crmData['pp_end_users'] : '');
		} catch(Exception $e) {
			$pCompanySize = null;
		}

		if(!$pCompanySize || in_array($pCompanySize, array("users_elearning_one_hundred", "users_elearning_two_hundred_half", "users_elearning_three_hundred_half")))
			return true; // SMB
		else
			return false; // Enterprise business
	}

	/**
	 * Check if the platform is expired or is outside of the trial period, if yes a proper popup will be shown
	 */
	public static function checkPlatformValidity() {

		// we should check for trial or
		if (!self::isSaas()) {
			return;
		}

		$controller = Yii::app()->controller->id;
		$action = Yii::app()->controller->action->id;

		if (self::isInMaintenance()) {
			if($controller != 'site' || $action != 'maintenance')
				Yii::app()->request->redirect(Docebo::createLmsUrl('/site/maintenance'));
			else
				return;
		}

		// protect safe routes from popup
		/*
 		$controllerId = Yii::app()->getController()->id;
 		$actionId = Yii::app()->getController()->getAction()->id;
 		if ($controllerId == 'site' && $actionId == 'logout') {
 			return;
 		}
	 	*/

		// Check if App route is in the list of SAFE routes, that does not activate "Trial expired" popup
		// Lower the case of route, just in case someone typed 'sITe', instead of 'site'
		array_walk(self::$trialExpiredSafeRoutes, function(&$item) {$item = strtolower($item);});
		if (in_array(strtolower(Yii::app()->controller->route), self::$trialExpiredSafeRoutes) ) {
			return;
		}


		if (self::isPlatformExpired()) {
			$appName = Yii::app()->name;
			$moduleName = Yii::app()->controller->module->id;
			if (
				($appName == 'lms')
				&&
				(($controller == 'site' && $action == 'index')
				|| ($controller == 'site' && $action == 'axContactSalesTrialExpired')
				|| ($moduleName == 'billing' && $controller == 'default' && $action == 'upgrade')))
			{
				$ui = new DoceboUI;
				$ui->showExpiredPopup();
			}
			else
			{
				// Do NOT redirect to billing/upgrade if LMS is not yet completely setup!!!
				$saas_installation_status = intval(Settings::get('saas_installation_status', 0));
				if ($saas_installation_status == 0) {
					Yii::app()->request->redirect(Docebo::createLmsUrl('/billing/default/upgrade'));
				}
			}


		}
	}

	/**
	 * Check is customer reenter CC in order to replace Wirecard (old gateway) with Stripe.
	 */
	public static function checkReinsertCCData() {
		if (Yii::app()->user->isGodadmin && Docebo::isSaas() && BrandingWhiteLabelForm::isMenuVisible() && (Settings::get('enable_user_billing', 'on') == 'on' || isset(Yii::app()->request->cookies['reseller_cookie'])) && Settings::get('demo_platform', 0) != 1) {
			if (isset(Yii::app()->session['reinsert-cc-data']) && Yii::app()->session['reinsert-cc-data'] == true) {
				$ui = new DoceboUI;
				$ui->showReinsertCCDataPopup();
			}
		}
		return;
	}

	/**
	 * Return the current max number of active user setted for the platform
	 * @return int
	 */
	public static function getMaxUsers() {
		return (int)Settings::get('max_users', 0);
	}


	/**
	 * Return the number ofactive users calculated for the current active billing period
	 * @return int
	 */
	public static function getActiveUsers() {

		$actv_users = 0;
		$subscription_begin_date = Settings::get('subscription_begin_date');
		$UtcTimeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

		if ($subscription_begin_date && strtotime($subscription_begin_date)) { // Valid subscription date
			if (self::dateExists($subscription_begin_date) && strtotime($subscription_begin_date) < $UtcTimeNow) { // Is a valid date

				$dates = Billing::getSubscriptionPeriodDates();

				if (count($dates) <= 0) { return 0; }

 				$currentPeriod = end($dates);

				$count = UserLimit::countActiveUsers($currentPeriod['from'], $currentPeriod['to']);
				if ($count !== false) { $actv_users = $count; }
			}
		}
		return $actv_users;
	}




	/**
	 * Return history of active users since LMS started being paid (subscription_begin_date)
	 *
	 * @return array  [ ["from"=>x, "to"=>y, "active_users"]=>z, ... ]
	 */
	public static function getActiveUsersHistory() {

		$subscription_begin_date = Settings::get('subscription_begin_date');
		$UtcTimeNow = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));

		$result = array();

		if ($subscription_begin_date && strtotime($subscription_begin_date)) { // Valid subscription date
			if (self::dateExists($subscription_begin_date) && strtotime($subscription_begin_date) < $UtcTimeNow) { // Is a valid date

				$dates = Billing::getSubscriptionPeriodDates();

				foreach ($dates as $currentPeriod) {

					$onePeriodResult = array('from' => $currentPeriod['from'], 'to' => $currentPeriod['to'], 'active_users' => 0);

					// Get the activity/user logins for the current period
					$command = Yii::app()->db->createCommand()
						->select('count(*) as count')
						->from(LearningUserlimitLoginLog::model()->tableName().' lul')
						//->join(CoreUser::model()->tableName().' cu', 'lul.userIdst=cu.idst')
						->leftJoin(CoreUser::model()->tableName().' cu', 'lul.userIdst=cu.idst')
						->where("first_period_login >= :period_begin_date AND first_period_login <= :period_end_date", array(
								':period_begin_date' => $currentPeriod['from'],
								':period_end_date' => $currentPeriod['to'],
					));

					//$command->andWhere("cu.userid NOT LIKE '%/staff.docebo%' ");
					$command->andWhere("(cu.userid NOT LIKE '%/staff.docebo%') OR (cu.idst IS NULL)");   // last one is to get also records with NO core user (left join)

					$row = $command->queryRow();
					if ($row || $row['count'] > 0) {
						$onePeriodResult['active_users'] = (int)$row['count'];
					}
					$result[] = $onePeriodResult;
				}
			}
		}


		return $result;
	}







	/**
	 * Return true if the platform is expired (after subscription or trial period)
	 *
	 * @return bool
	 */
	public static function isPlatformExpired() {
		$res = FALSE;

		if (Docebo::isTrialPeriod()) {
			$res = Docebo::isTrialExpired();
		} else {
			// We're still checking for "isGracePeriod" even if deprecated as a double check..
			//$res = (Docebo::isGracePeriod() || Docebo::isExpiredPeriod());
			$res = (Docebo::isExpiredPeriod());
		}

		return $res;
	}


	/**
	 * saas_installation_status will contain the installation step that need to be showed to the final user
	 *     0 -> completed, don't redirect
	 *     > 0 -> redirect to setup
	 */
	public static function resumeSaasInstallation() {
		$saas_installation_status = intval(Settings::get('saas_installation_status', 0));
		$adminSetupUrl = Docebo::createAdminUrl('setup/index');
		if ($saas_installation_status > 0 && !(Yii::app()->controller->id == 'setup')) {
			Yii::app()->request->redirect($adminSetupUrl);
		} // endif
	}


	/**
	 * Return Platform version set in core_settings
	 * @return string
	 */
	public static function platformVersion() {
		return defined('PLATFORM_VERSION') ? PLATFORM_VERSION : Settings::get('core_version', '7.0');
	}



	public static function isBasicVersion() {
		return (Settings::get('admin_version', 'basic') == 'basic');
	}


	public static function isAdvancedVersion() {
		return (Settings::get('admin_version', 'basic') != 'basic');
	}


	public static function isCatalogSetAsHomePage() {
		return ('on' == Settings::get('first_catalogue', 'off'));
	}


	public static function isCurriculaSetAsHomePage() {
		return ('on' == Settings::get('curricula_is_home_page', 'off'));
	}



	/**
	 * Returns the url for the home button in the main menu for logged in users
	 * @static
	 * @return string
	 */
	public static function getUserHomePageUrl() {
		$urlParams = array();

		$currentUserInfo = Yii::app()->user->getUserInfoArr();

		// Force password change on first login active
		if(isset($currentUserInfo['force_change']) && $currentUserInfo['force_change'] == 1 && (!isset($currentUserInfo['password_changed']) || (isset($currentUserInfo['password_changed']) && !$currentUserInfo['password_changed'])))
		{
			$_SESSION['must_renew_pwd'] = 1;
			return Docebo::createLmsUrl('user/changePassword');// createAbsoluteUrl
		}

		// Use default menu item if any
		$menuDefaultUrl = CoreMenuItem::getLmsDefaultUrl();
		if($menuDefaultUrl['url'] !== null)
			return Docebo::createLmsUrl($menuDefaultUrl['url'], $menuDefaultUrl['prop']);

		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			/*
			 * NOTE: check for catalogs plugin activation is not really needed, because a further check is applied in the homepage rendering.
			 * Anyway having more check won't hurt.
			 */
			//catalog_type = mixed|mycourses|catalog
			$catalogType = Settings::get('catalog_type', 'catalog');
			$isSetAsHomePage = Docebo::isCatalogSetAsHomePage();

			if ($isSetAsHomePage && $catalogType != 'mycourses') {
				// catalog is set as home page and is not hidden
				$urlParams['opt'] = 'catalog';
			}
		}

		$urlParams['login'] = 1;

		// If Curricula App is active and is set as home page... ?
		if (self::isCurriculaSetAsHomePage() && PluginManager::isPluginActive('CurriculaApp') && LearningCoursepathUser::getUserCoursepathNumber(Yii::app()->user->idst)) {
			return Docebo::createLmsUrl('curricula/show');
		}

		return Docebo::createLmsUrl('site/index', $urlParams);
	}

	/**
	 * Returns the url to be redirected after the login to the platform
	 * @static
	 * @return string
	 */
	public static function getUserAfetrLoginRedirectUrl() {

		$urlParams = array();

		$currentUserInfo = Yii::app()->user->getUserInfoArr();

		// Force password change on first login active
		if(isset($currentUserInfo['force_change']) && $currentUserInfo['force_change'] == 1 && (!isset($currentUserInfo['password_changed']) || (isset($currentUserInfo['password_changed']) && !$currentUserInfo['password_changed'])))
		{
			$_SESSION['must_renew_pwd'] = 1;
			return Docebo::createLmsUrl('user/changePassword');// createAbsoluteUrl
		}

		// Mandatory fields must be completed?
		if(Settings::get('request_mandatory_fields_compilation', 'off') == 'on')
		{
			$mandatoryFields = CoreUserField::getUnfilledMandatoryFields();
            $defaultFields = CoreUser::getUnfilledDefaultFields();
			if(($mandatoryFields && !empty($mandatoryFields)) || count($defaultFields) > 0)
				return Docebo::createLmsUrl('user/additionalFields');
		}

		// Redirect the user to a specific link from before the registration process
		if(isset(Yii::app()->session['redirect_url_after_login']) && Yii::app()->session['redirect_url_after_login'] !== '') {
			return urldecode(Yii::app()->session['redirect_url_after_login']);
		}


		// Redirect the user to the previous requested link
		$afterloginRedirectUrl = isset(Yii::app()->session['lasturl']) ? Yii::app()->session['lasturl'] : false;

		if($afterloginRedirectUrl !== false && (strpos($afterloginRedirectUrl, 'curricula/show/downloadCertificate'))) {
			self::savePopupRedirect($afterloginRedirectUrl);
		}
		else if($afterloginRedirectUrl !== false) {
			return $afterloginRedirectUrl;
		}

		// Check for direct course login
		$id_course = Yii::app()->request->getParam('id_course', 0);
		if($id_course > 0) {
			$learningCourseuser = LearningCourseuser::model()->findByPk(array(
				'idCourse' => (int)$id_course,
				'idUser' => (int)Yii::app()->user->idst,
				'edition_id' => 0,
			));
			if ($learningCourseuser) {
				$canEnterCourse = $learningCourseuser->canEnterCourse();
				if($canEnterCourse['can']) {
					return Docebo::createLmsUrl('player', array('course_id' => $id_course));
				}
			}
		}

		// LMS Default?
		$menuDefaultUrl = CoreMenuItem::getLmsDefaultUrl();
		if($menuDefaultUrl['url'] !== null) {
			return self::getUserHomePageUrl();
		}

		// Looks like we don't have special place to redirect, go to site/index
		return Docebo::createLmsUrl('site/index', $urlParams);

	}


	/**
	 * todo: move this in a Util class
	 * @param $date
	 * @return bool
	 */
	public static function dateExists($date) {
		if (!strtotime($date)) {
			return FALSE;
		}
		$e = explode('-', $date);
		$year = $e[0];
		$month = $e[1];
		$day = substr($e[2], 0, 2);
		return checkdate($month, $day, $year);
	}


	/**
	 * todo: move this in a Util class
	 * @param $date
	 * @param string $add
	 * @return bool|int
	 */
	public static function addToDate($date, $add = '+1 month') {
		if (!strtotime($date)) {
			return FALSE;
		}
		$date = strtotime($date);
		return strtotime($add, $date);
	}


	/**
	 * Returns text of the Privacy Terms Of Service, based on language.
	 * This is the TOS created by the LMS Admin/Owner, NOT Docebo TOS!!!
	 *
	 * @param string $language  ('en', 'it',...)
	 * @return string
	 */
	public static function getPrivacyTos($language = NULL) {
		if (!$language) {
			$language = Yii::app()->getLanguage();
		}
		$tosText = nl2br(Yii::t("privacy_tos", "_TERMS_AND_CONDITIONS", array(), NULL, $language));
		return $tosText;
	}


	/**
	 * We assume that the root is always one level under the current "app" folder ("../")
	 * So we just remove one level from the "app" baseUrl path
	 *
	 * example: /docebo/svn/docebo_yii_feature/lms -> /docebo/svn/docebo_yii_feature
	 *
	 * @return string
	 */
	public static function getRootBaseUrl($absolute = FALSE) {
		$res = Yii::app()->getBaseUrl($absolute);

		$res = str_replace("\\", '/', $res);
		$res = substr($res, 0, strrpos($res, '/'));

		return $res;
	}


	/**
	 * Return the root base path
	 *
	 * @return string
	 */
	public static function getRootBasePath() {

		if (defined('_base_')) {
			$res = _base_;
		} else {
			$res = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
			$res = realpath($res);
		}

		return $res;
	}


	/**
	 * Return the relative path of a given url according to the root baseUrl of the platform compared
	 * with the homeUrl (launch script) of the platform.
	 *
	 * @param $url the relative baseUrl (example: /docebo_yii_feature/lms/images) obtained with $xyz->getBaseUrl(false)
	 * @return string
	 */
	public static function getRelativeUrl($url) {

		// we get the root base url (ex: /docebo/svn/docebo_yii_feature)
		$root = Docebo::getRootBaseUrl();
        $root_depth = substr_count($root, '/');

		// we get the home base url (ex: /docebo/svn/docebo_yii_feature/lms/index.php)
		$home_url = Yii::app()->getHomeUrl();
		if (Yii::app()->urlManager->showScriptName) { // if file name is included we remove it
			$home_url = dirname($home_url); // (ex: /docebo/svn/docebo_yii_feature/lms)
		}

        if(!empty($_GET['domain']))
            $home_url = "/".$_GET['domain'].$home_url;

		$home_url = str_replace("\\", '/', $home_url);
		$home_url_depth = substr_count($home_url, '/');

		$url = str_replace("\\", '/', $url);

		// we remove the root part from the url:
		$res = substr($url, strlen($root)+1);

		// we add relative directory prefix:
		for($i = 0; $i < $home_url_depth-$root_depth; $i++) {
			$res = '../' . $res;
		}

		return $res;
	}


	/**
	 * Get the domain for the selected LMS, taking into account (and using; if any)
	 * the value from the Custom Domain App settings. If the Custom Domain app is
	 * deactivated or uninstalled, the original (Docebo Saas) domain will be returned.
	 * @return string - the current custom domain or the original (saas) domain
	 */
	public static function getCurrentDomain($removeSchema = false, $removeWww = false) {

		if (Settings::get('module_customdomain') == 'on') {
			// The Custom Domain app is active
			$result = Settings::get('url');
		} else {
			// Custom Domain app is inactive, but may still be in the
			// period where we can reactivate it, so try to use the
			// backup param with the original domain if present
			// (since the main one - core_setting.url -  still contains
			// the custom domain during this period).
			$originalDomain = Settings::get('custom_domain_original');
			$result = $originalDomain ? $originalDomain : rtrim(Settings::get('url'), '/');
		}

		// For some reason, sometimes we get double schemas; clean up
		$result = str_ireplace('http://http://', 'http://', $result);
		$result = str_ireplace('https://https://', 'https://', $result);
		$result = str_ireplace('https://http://', 'https://', $result);
		$result = str_ireplace('http://https://', 'https://', $result);

		// Should we remove the schema, if any ?
		if ($removeSchema) {
			$result = str_ireplace(array('http://', 'https://'), '', $result);
		}

		if ($removeWww) {
			$result = preg_replace('/^www\\./', '', strtolower($result));
		}

		return $result;
	}

	/**
	 * Check if the domain from the request is the current (main) LMS domain
	 * @return bool
	 */
	public static function isCurrentLmsDomain() {
		$domainName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] :  gethostname();
		$domainName = preg_replace('/^www\\./', '', strtolower($domainName));
		$currentLmsDomain = trim(self::getCurrentDomain(true, true), '/');
		$subfolder = CoreMultidomain::getSubfolder();

		$subfolderCheck = true;
		// multidomain subfolder
		if($subfolder && !in_array($subfolder, self::getKnownAppNames()))
			$subfolderCheck = false;

		if($currentLmsDomain && $domainName && ($currentLmsDomain != $domainName || !$subfolderCheck))
			return false;

		return true;
	}

	/**
	 * todo: move this in a Util class
	 * Takes a datetime string (2013-01-31 11:12:13) and returns the same date, end of the day timestamp (2013-01-31 23:59:59).
	 * @param bool $datetime
	 * @return string
	 */
	public static function endOfDayDatetime($datetime = FALSE) {
		if ($datetime === FALSE) {
			$datetime = date("Y-m-d", time()) . " 23:59:59";
		} else {
			$datetime = date("Y-m-d", strtotime($datetime)) . " 23:59:59";
		}
		return $datetime;
	}


	/**
	 * todo: move this in a Util class
	 * Takes a datetime string (2013-01-31 11:12:13) and returns the same date, end of the day TIME (seconds)
	 * @param bool $datetime
	 * @return int
	 */
	public static function endOfDayTime($datetime = FALSE) {
		$datetime = self::endOfDayDatetime($datetime);
		return strtotime($datetime);
	}


	/**
	 * Get absolute path to where temporary uploaded files are sent.
	 * @return string
	 */
	public static function getUploadTmpPath() {
		// Runtime path is actualla "lmstmp/l/m/lms_domain_com"
		$path = Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . "upload_tmp";
		if (!is_dir($path)) {
			FileHelper::createDirectory($path, 0777, true);
		}
		$path = realpath($path);
		return $path;
	}

	/**
	 * Get URL to where temporary uploaded files are sent
	 *
	 * @param string $filename
	 * @return string
	 */
	public static function getUploadTmpUrl($filename = '') {
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
		return rtrim($storage->fileUrl(''), "/");
	}


	/**
	 * Clean the filename composed in the old way, 3 numbers separated by _ and the real filename at the end
	 * @param $filename the original filname 111_222_33333333333333_originalfilenam.ext
	 * @return string the cleaned filename, like originalfilenam.ext
	 */
	public static function cleanFilename($filename) {

		// If the 6.0 format is used (e.g. '12302_1383317994_ElectricalSafetyConstTincan_zip')
		if(stripos($filename, '.') === false){
			$parts = explode('_', $filename);
			if(count($parts) == 4){
				$sendname = $parts[2]. '.' .$parts[3];
				return $sendname;
			}
		}
		// this was cutting too much
		$sendname = implode('_', array_slice(explode('_', $filename), 2));
		//if($sendname == '') $sendname = implode('_', array_slice(explode('_', $filename), 2));
		if($sendname == '') $sendname = $filename;
		return $sendname;
	}


	/**
	 * Make some checks and resolve the application language
	 */
	public static function resolveLanguage() {

		if (Yii::app() instanceof CConsoleApplication) {
			return self::resolveConsoleLanguage();
		}

		// we set a flag, showing if the user is here for the first time or not
		// this is used in MultidomainApp
		if(empty(Yii::app()->session['current_lang'])){
			Yii::app()->session['first_time_language'] = true;
		}else{
			Yii::app()->session['first_time_language'] = false;
		}
		// Get browser prefered language. If it is NOT in the list of active languages, set to FALLBACK
		$browser_language = Yii::app()->getRequest()->getPreferredLanguage();

		if(stripos($browser_language, '_')!==FALSE){
			$browser_language = explode('_', $browser_language);
			$browser_language = $browser_language[0];
		}
		if(stripos($browser_language, '-')!==FALSE){
			$browser_language = explode('-', $browser_language);
			$browser_language = $browser_language[0];
		}

		// LMS-yii session language
		$yii_session_language = isset(Yii::app()->session['_lang']) ? Yii::app()->session['_lang'] : FALSE;

		// Incoming request to change language ('lang'-style:  ?lang=xxx) ?
		$new_yii_language = Yii::app()->request->getParam('lang');

		// LMS Session language
		$lms_session_language = !empty(Yii::app()->session['current_lang']) ? Yii::app()->session['current_lang'] : FALSE;

		// Incoming request to change language (LMS style: ?new_lang=xxx) ?
		$new_lms_language = Yii::app()->getRequest()->getParam('new_lang', FALSE);


		// This variable must end-up in ISO Style ('en')!!!!!!!!
		$run_language = FALSE;

		$activeLangs = Lang::getLanguages(TRUE);
//		$activeLangs = Yii::app()->db->createCommand()
//				->select('lang_browsercode as browsercode, lang_description as description, lang_code as code, lang_direction as direction')
//				->from(CoreLangLanguage::model()->tableName())
//				->where('lang_active=:active', array(':active' => 1))->queryAll();
		if($activeLangs && count($activeLangs)===1){
			// Only one language is active in the LMS. Use it in all cases.
			$run_language = $activeLangs[0]['browsercode'];
		}else{
			// lang=XXXXX takes highest priority
			if ($new_yii_language) {
				$run_language = $new_yii_language;
			}
			else if ( $new_lms_language ) {
				$run_language = Lang::getBrowserCodeByCode($new_lms_language);
			}
			else if ($lms_session_language) {
				$run_language = Lang::getBrowserCodeByCode($lms_session_language);
			}
			else if ($yii_session_language) {
				$run_language = $yii_session_language;
			}
			else if ($browser_language) {
				$run_language = $browser_language;
			}

			if(!Lang::isBrowserCodeAvail($run_language)){
				// If the lang we ended up with is not active in the LMS, use the default one
				if(Settings::get('default_language')){
					$run_language = Lang::getBrowserCodeByCode(Settings::get('default_language'));
				}

				// If the default LMS lang is not active as well, fallback to 'en' as
				// last resort. And if THAT is not available as well, just use the
				// first available language. This shouldn't happen ever generally!
				if(!Lang::isBrowserCodeAvail($run_language)){
					if(Lang::isBrowserCodeAvail(Lang::$LANGUAGE_FALLBACK)){
						$run_language = Lang::$LANGUAGE_FALLBACK;
					}else{
						$run_language = $activeLangs[0]['browsercode'];
					}
				}
			}
		}


		// save language as user preference (in order for it to be remembered at next login)
		if ( ! Yii::app()->user->getIsGuest())
		{
			// only on language change
			if ($new_yii_language || $new_lms_language)
			{
				$userSetting_lang = CoreSettingUser::model()->findByAttributes(array(
					'id_user' => Yii::app()->user->id,
					'path_name' => 'ui.language'
				));
				if ( ! $userSetting_lang) {
					$userSetting_lang = new CoreSettingUser();
					$userSetting_lang->path_name = 'ui.language';
					$userSetting_lang->id_user = Yii::app()->user->id;
				}
				$lang_code = Lang::getCodeByBrowserCode($run_language);
				if ($userSetting_lang->value != $lang_code) {
					$userSetting_lang->value = $lang_code;
					if ($userSetting_lang->save()) {
						Yii::app()->session['changed_lang'] = TRUE;
						Yii::app()->session['forced_lang'] = TRUE;
					}
				}
			}
		}


		Yii::app()->language = $run_language;  // browser code, like 'en'
		Yii::app()->session['current_lang'] = Lang::getCodeByBrowserCode($run_language); // lang-code style (for LMS)
		Yii::app()->session['_lang'] = $run_language;  // browser code style
	}



	/**
	 * Resolve Console Application Language
	 */
	public static function resolveConsoleLanguage() {

		// This variable must end-up in ISO Style ('en')!!!!!!!!
		$run_language = false;

		$activeLangs = Lang::getLanguages(TRUE);

		// Only one language is active in the LMS. Use it in all cases.
		if($activeLangs && count($activeLangs)===1){
			$run_language = $activeLangs[0]['browsercode'];
		}
		else {
			// Get user language
			$userModel = Yii::app()->user->loadUserModel();
			if ($userModel) {
				$run_language = Lang::getBrowserCodeByCode(Yii::app()->user->loadUserModel()->getCurrentUserLanguage());
			}

			if(!Lang::isBrowserCodeAvail($run_language)){
				// If the lang we ended up with is not active in the LMS, use the default one
				if(Settings::get('default_language')){
					$run_language = Lang::getBrowserCodeByCode(Settings::get('default_language'));
				}

				// If the default LMS lang is not active as well, fallback to 'en' as
				// last resort. And if THAT is not available as well, just use the
				// first available language. This shouldn't happen ever generally!
				if(!Lang::isBrowserCodeAvail($run_language)){
					if(Lang::isBrowserCodeAvail(Lang::$LANGUAGE_FALLBACK)){
						$run_language = Lang::$LANGUAGE_FALLBACK;
					}else{
						$run_language = $activeLangs[0]['browsercode'];
					}
				}
			}
		}

		Yii::app()->language = $run_language;  // browser code, like 'en'

	}


	/**
	 * todo: move this in a Util class
	 * @param bool $last_count
	 * @param bool $return
	 * @return string
	 */
	public static function getLastSqlLog($last_count = FALSE, $return = FALSE) {
		$res = "";
		$log_arr = Yii::getLogger()->getLogs('', 'system.db.CDbCommand', $last_count);

		if ($last_count > 0) {
			$new_arr = array();
			for($i = count($log_arr)-$last_count; $i < count($log_arr); $i++) {
				$new_arr[] = $log_arr[$i];
			}
			$log_arr = $new_arr;
			unset($new_arr);
		}

		foreach($log_arr as $log) {
			$res .= $log[0] . "<br />\n\n";
		}

		if (!$return) {
			echo $res;
		} else {
			return $res;
		}
	}





	//=== URLs management ===


	/**
	 * Parameters that will always added to created url
	 * @var array the list of default parameters to be added to the urls
	 */
	protected static $_defaultUrlParams = array();


	/**
	 * Read a pre-set default parameter. NULL is returned if parameter has not been set
	 * @param string $key
	 * @return mixed
	 */
	public static function getDefaultUrlParam($key) {
		return (isset(self::$_defaultUrlParams[$key]) ? self::$_defaultUrlParams[$key] : NULL);
	}

	/**
	 * Set a default parameter by key and value
	 * @param string $key
	 * @param mixed $value
	 */
	public static function setDefaultUrlParam($key, $value) {
		self::$_defaultUrlParams[$key] = $value;
	}

	/**
	 * Remove a pre-set default parameter by its key
	 * @param string $key
	 */
	public static function unsetDefaultUrlParam($key) {
		if (isset(self::$_defaultUrlParams[$key])) {
			self::$_defaultUrlParams[$key] = NULL;
			unset(self::$_defaultUrlParams[$key]);
		}
	}

	/**
	 * Merge default params with user-specified params. If some keys are euals, user params win.
	 * @param array $params
	 * @return array
	 */
	protected static function _mergeDefaultUrlParams($params) {
		$output = $params;
		if (!empty(self::$_defaultUrlParams)) {
			foreach (self::$_defaultUrlParams as $key => $value) {
				if (!isset($output[$key])) {
					$output[$key] = $value;
				}
			}
		}
		return $output;
	}


	/**
	 * Provide any LMS URL (e.g. the result of Docebo::createLmsUrl())
	 * and pass it here to replace it with the custom domain (if the
	 * Custom Domain App is active and a custom domain is configured)
	 *
	 * @param $url Some LMS URL with SAAS url
	 *
	 * @return string
	 */
	public static function useCustomDomain($url){
		if(!$url) return false;

		// Make the URL valid by adding the protocol, if not found
		if(stripos($url, 'http://')===false && stripos($url, 'https://')===false)
			$url = 'http://'.$url;

		$originalDomain = parse_url($url, PHP_URL_HOST);
		if($originalDomain){
			$customDomain = str_ireplace(array('https://', 'http://'), '', rtrim(Docebo::getCurrentDomain(), '/'));
			if($customDomain && $originalDomain)
				$url = str_ireplace($originalDomain, $customDomain, $url);
		}

		return $url;
	}

	/**
	 * CreateUrl() for given App; App must be put in front of the route.
	 *
	 * Example:
	 * 	$url = createAppUrl('admin:site/index')
	 *  Result:  /admin/index.php?r=site/index
	 *
	 * @param $route
	 * @param array $params
	 * @param string $ampersand
	 * @return mixed|string
	 */
	public static function createAppUrl($route, $params = array(), $ampersand = '&') {

		//$knownAppNames = array('/^\/admin\//i', '/^\/lms\//i', '/^\/authoring\//i');

		// Look for 'appname:'
		$matches = array();
		preg_match('/^(.+):/i', $route, $matches);

		if (isset($matches[1]) && in_array($matches[1], self::getKnownAppNames())) {

			// Remove '<appname>:'
			$route = preg_replace('/^(.+):/i', '', $route);
			/*if ($matches[1] == 'lms') {
				return self::createLmsUrl($route, $params, $ampersand);
			} else {
				return self::createAdminUrl($route, $params, $ampersand);
			}*/
			return forward_static_call(array(self, 'create'.ucfirst($matches[1]).'Url'), $route, $params, $ampersand);
		} else {
			return Yii::app()->createUrl($route, self::_mergeDefaultUrlParams($params), $ampersand);
		}
	}

	/**
	 * Return base url for a specified app, note that strstr search in the server path so it use
	 * DIRECTORY_SEPARATOR while the replace is done on the url that always has "/"
	 * @param string $appName
	 * @param string $route
	 * @param array $params
	 * @param string $ampersand
	 * @return mixed|string
	 */
	protected static function _createUrl($appName, $route, $params = array(), $ampersand = '&') {

		$params = self::_mergeDefaultUrlParams($params);
		foreach (self::getKnownAppNames() as $check) {
			if (strstr(Yii::app()->basePath, DIRECTORY_SEPARATOR . $check . DIRECTORY_SEPARATOR) !== FALSE) {
				return str_replace('/'.$check.'/', '/'.$appName.'/', Yii::app()->createUrl($route, $params, $ampersand));
			}
		}
		return Yii::app()->createUrl($route, $params, $ampersand);
	}


	protected static function _createAbsoluteUrl($appName, $route, $params = array(), $schema = '', $ampersand = '&') {

		$params = self::_mergeDefaultUrlParams($params);

		// If this is a Console application...
		// Note please, it works only if the application is a result of a call to yiic-domain <some.domain.name>
		// Otherwise domain/host is unknown and result is unpredicatble 
		if (Yii::app() instanceof CConsoleApplication) {
			Yii::app()->request->setScriptUrl($appName);
			return Yii::app()->createAbsoluteUrl($route, $params, $schema, $ampersand);
		}

		foreach (self::getKnownAppNames() as $check) {
			if (strstr(Yii::app()->basePath, DIRECTORY_SEPARATOR . $check . DIRECTORY_SEPARATOR) !== FALSE) {
				return str_replace('/'.$check.'/', '/'.$appName.'/', Yii::app()->createAbsoluteUrl($route, $params, $schema, $ampersand));
			}
		}
		return Yii::app()->createAbsoluteUrl($route, $params, $schema, $ampersand);
	}


	public static function createAbsoluteUrl($route, $params = array(), $schema = '', $ampersand = '&') {
		$matches = array();
		preg_match('/^(.+):/i', $route, $matches);

		if (isset($matches[1]) && in_array($matches[1], self::getKnownAppNames())) {

			// Remove '<appname>:'
			$route = preg_replace('/^(.+):/i', '', $route);
			return forward_static_call(array('Docebo', 'createAbsolute'.ucfirst($matches[1]).'Url'), $route, $params, $schema, $ampersand);
		} else {
			return Yii::app()->createAbsoluteUrl($route, self::_mergeDefaultUrlParams($params), $schema, $ampersand);
		}
	}

    /**
     * Create Hydra Url based on the passed params
     * @param $app
     * @param string $route
     * @param array $params
     * @return string
     */
	public static function createHydraUrl($app, $route = '', $params = array()) {

        $url = self::getRootBaseUrl(true) . ($app ? '/' .  trim(rtrim($app, '/'), '/') : '');
        if( $route ) {
            $url .= '/' . trim(rtrim($route, '/'), '/');
        }

        if( is_array($params)) {
            $tmpParams = array();
            foreach ($params as $k => $v) {
                $tmpParams[] = $k . '=' . $v;
            }
            $formattedParams = implode(';', $tmpParams);
        } else {
            $formattedParams = $params;
        }

        return  $url .  (!empty($formattedParams) ? (';' . $formattedParams) : '');
    }

    /**
     * Create Hydra Api Url based on the passed params
     * @param $app
     * @param string $route
     * @param array $params
     * @return string
     */
    public static function createHydraApiUrl($app, $route = '', $params = array()) {

        $url = self::getRootBaseUrl(true) . ($app ? '/' .  trim(rtrim($app, '/'), '/') : '');
        if( $route ) {
            $url .= '/v1/' . trim(rtrim($route, '/'), '/');
        }

        $formattedParams = array();
        foreach ( $params as  $k => $v ) {
            $formattedParams[] = $k . '=' . $v;
        }

        return  $url . ( $formattedParams ? '?'. implode('&', $formattedParams) : '' );
    }


	public static function __callStatic($method, $arguments) {
		if (preg_match('/^createAbsolute[A-Z][\w]*Url$/', $method)) {

			//catch all the "createAbsolute{app name}Url" static calls and handle them
			$appName = lcfirst(preg_replace(array('/^createAbsolute/', '/Url$/'), '', $method));
			if(in_array($appName, array('mobile', 'webapp')))
				return self::getRootBaseUrl(true) . '/webapp/';

			$route = (isset($arguments[0]) ? $arguments[0] : '');
			$params = (isset($arguments[1]) ? $arguments[1] : array());
			$schema = (isset($arguments[2]) ? $arguments[2] : '');
			$ampersand = (isset($arguments[3]) ? $arguments[3] : '&');
			return self::_createAbsoluteUrl($appName, $route, $params, $schema, $ampersand);

		} elseif (preg_match('/^create[A-Z][\w]*Url$/', $method)) {

			//catch all the "create{app name}Url" static calls and handle them
			$appName = lcfirst(preg_replace(array('/^create/', '/Url$/'), '', $method));
			if(in_array($appName, array('mobile', 'webapp')))
				return self::getRootBaseUrl(true) . '/webapp/';

			$route = (isset($arguments[0]) ? $arguments[0] : '');
			$params = (isset($arguments[1]) ? $arguments[1] : array());
			$ampersand = (isset($arguments[2]) ? $arguments[2] : '&');
			return self::_createUrl($appName, $route, $params, $ampersand);

		}
		//return parent::__callStatic($method, $arguments);
	}




	/**
	 * Return Environment type ('saas'/'gapps')
	 *
	 * @return string
	 */
	public static function environmentType() {
		if (Settings::getCfg('install_from') == 'saas_docebo')
				return self::ENVTYPE_SAAS;

		if (Settings::getCfg('install_from') == 'gapps_docebo')
			return self::ENVTYPE_GAPPS;

		return '';

	}


	/**
	 * Get Absolute path of the LMS 'files' folder (Single tenant or Multiplatform)
	 *
	 */
	public static function filesPathAbs() {
		if (self::isSaas()) {
			$path = _base_ . DIRECTORY_SEPARATOR . self::filesPathRel(false);
		}
		else {
			$path = Yii::getPathOfAlias("files");
		}
		return $path;
	}


	/**
	 * Get Relative path of the LMS 'files' folder (Single tenant or Multiplatform).
	 * Relative to LMS <root>
	 *
	 * Example:
	 * 	files/my_domain_com (multiplatform)
	 *  files  (single tenant)
	 *
	 */
	public static function filesPathRel($forUrl = true) {
		if ($forUrl)
			$sep = "/";
		else
			$sep = DIRECTORY_SEPARATOR;

		$path = 'files'.  $sep . self::getSplitOriginalDomainCode($forUrl);
		return $path;
	}

	/**
	 * URL of 'files' folder, no schema and domain
	 *
	 * @return string
	 */
	public static function filesBaseUrl() {
		$url = Docebo::getRootBaseUrl() . "/" . self::filesPathRel();
		return $url;
	}


	/**
	 * URL of 'files' folder, WITH schema and domain (i.e. absolute)
	 *
	 * @return string
	 */
	public static function filesAbsoluteBaseUrl() {
		$url = Docebo::getRootBaseUrl(TRUE) . "/" . self::filesPathRel();
		return $url;
	}


	/**
	 * Return Original LMS domain, taking into account custom domain settings
	 *
	 * @return string
	 */
	public static function getOriginalDomain() {

		$url = '';

		// If Custom daomain App is ON, get the domain from core_setting->custom_domain_original field.
		// If NOT, still check that field and if set, use it. Otherwise, just use the core_setting->url field  (host part)
		$originalDomain = trim(Settings::get('custom_domain_original',''));
		$url = trim(Settings::get('url'));
		if (Settings::get('module_customdomain') == 'on') {
			$domain = $originalDomain;
		} else {
			$url = self::fixUrlSchema($url);
			$domain = !empty($originalDomain) ? $originalDomain : parse_url($url, PHP_URL_HOST);
		}

		// If we didn't find ANY information in 'url' and 'custom_domain_original' fields, use $_SERVER or gethostname() as last resort
		if (!$domain) {
			$domain = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] :  gethostname();
		}

		$domain = preg_replace('/^www\\./', '', strtolower($domain));

		return $domain;

	}

	/**
	 * Return d/o/domain_code version of domain_code
	 * @return string
	 */
	public static function getSplitOriginalDomainCode($forUrl = true) {
		$domainCode = self::getOriginalDomainCode();
		if ($forUrl)
			return $domainCode[0] . "/" . $domainCode[1] . "/" . $domainCode;
		else
			return $domainCode[0] . DIRECTORY_SEPARATOR . $domainCode[1] . DIRECTORY_SEPARATOR . $domainCode;
	}


	/**
	 * Returns the ORIGINAL LMS domain (e.g. my-lms.docebosaas.com) code in the form of my_lms_docebosaas_com
	 *
	 * @return string
	 */
	public static function getOriginalDomainCode() {
		$domain = self::getOriginalDomain();
		$domainCode = preg_replace('/(\W)/is', '_', $domain);
		return $domainCode;
	}


	/**
	 * Generate random UUID in form of: 8-4-4-4-12
	 * @return string
	 */
	public static function generateUuid() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				// 8
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

				// 4
				mt_rand( 0, 0xffff ),

				// 4
				mt_rand( 0, 0x0fff ) | 0x4000,

				// 4
				mt_rand( 0, 0x3fff ) | 0x8000,

				// 12
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}


	/**
	 * Generate random UUID of 8 characters only. Less random, but used for shorter strings.
	 * @return string
	 */
	public static function generateUuid8() {
	    return sprintf( '%04x%04x', mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ));
	}
	
	
	/**
	 * Return a SHA1 of a random UUID.
	 * 
	 * @param boolean $short Generate shorter string (8)
	 * @return string
	 */
	public static function randomHash($short=false) {
	    if ($short) {
	        return self::generateUuid8();
	    }
		return sha1(self::generateUuid());
	}


	/**
	 * If user agent matches one of those defined in Advanced Settings
	 */
	static public function isMobile(){
		$ua_mobile = CJSON::decode(Settings::get('ua_mobile', "['android','iphone','ipod','ipad','opera mini','blackberry','webOS']"));

		if(!is_array($ua_mobile ) || empty($ua_mobile )) return false;
		foreach($ua_mobile as $ua) {
			if(preg_match('/'.$ua.'/i', $_SERVER['HTTP_USER_AGENT'])) return true;
		}
		return false;
	}

	/**
	 * Check if the latest TOS have been approved by the godadmin
	 */
	static public function isTosApproved(){
		$savedLastApproval = Settings::get('tos_last_approval_date');

		if(!$savedLastApproval || !strtotime($savedLastApproval))
			return false;

		$tosLastUpdate = Yii::app()->params['tos_last_update'];
		if($tosLastUpdate){
			if(strtotime($tosLastUpdate)){
				if(strtotime($tosLastUpdate) > strtotime($savedLastApproval)){
					// The latest version of the TOS is not approved yet
					// (the last approved date is older than the TOS' current date)
					return false;
				}else{
					return true;
				}
			}
		}else{
			Yii::log('Tos last update time not set as Yii param', CLogger::LEVEL_INFO);
		}

		return FALSE;
	}


	/**
	 * Convert bytes to human readable format
	 *
	 * @param     $bytes
	 * @param int $precision bytes Size in bytes to convert
	 *
	 * @return string
	 */
	static public function bytesToHumanReadable($bytes, $precision = 2){
		$kilobyte = 1024;
		$megabyte = $kilobyte * 1024;
		$gigabyte = $megabyte * 1024;
		$terabyte = $gigabyte * 1024;

		if (($bytes >= 0) && ($bytes < $kilobyte)) {
			return $bytes . ' B';

		} elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
			return round($bytes / $kilobyte, $precision) . ' KB';

		} elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
			return round($bytes / $megabyte, $precision) . ' MB';

		} elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
			return round($bytes / $gigabyte, $precision) . ' GB';

		} elseif ($bytes >= $terabyte) {
			return round($bytes / $terabyte, $precision) . ' TB';
		} else {
			return $bytes . ' B';
		}
	}


	/**
	 * Add/Replace HTTP protocol in front of a given domain/URL.
	 *
	 * @param string $input
	 * @param string $schema  'http'  OR 'https'
	 * @param boolean $forceSchema
	 * @return string
	 */
	public static function fixUrlSchema($input, $schema='http', $forceSchema=false) {

		// Just get out if schema is not http or https
		if (!in_array(strtolower($schema), array('http', 'https'))) {
			return $input;
		}

		$url = $input;
		// If there is NO http[s]://  at POSTION 0! then.. add the schema
		if( (stripos($input, 'http://') !== 0) && (stripos($input, 'https://') !==0) ) {
			$url = $schema . '://' . $input;
		}
		// If there IS schema and we are asked to force the new schema...
		else if ($forceSchema) {
			$currentSchema = parse_url($input, PHP_URL_SCHEME);
			$input = str_ireplace($currentSchema . "://", '', $input);
			$url = $url = $schema . '://' . $input;
		}
		return $url;
	}



	/**
	 * Having various names for export types around LMS, we ended up with a messy names, so, lets have this one to translate to something well known
	 *
	 * @param string  $inType
	 * @return string
	 */
	public static function normalizeExportType($inType) {

		$result = "unknown";

		switch (strtolower($inType)) {
			case 'xls':
			case 'excel5':
				$result = LearningReportFilter::EXPORT_TYPE_XLS;
				break;

			case 'csv':
				$result = LearningReportFilter::EXPORT_TYPE_CSV;
				break;

			case 'html':
				$result = LearningReportFilter::EXPORT_TYPE_HTML;
				break;
		}


		return $result;

	}


	/**
	 * Get export type's file extension
	 *
	 * @param string  $type
	 * @return string
	 */
	public static function exportExtension($type) {

		$result = "txt";

		switch (strtolower($type)) {
			case self::EXPORT_TYPE_XLS:
				$result = self::EXPORT_EXT_XLS;
				break;

			case self::EXPORT_TYPE_CSV:
				$result = self::EXPORT_EXT_CSV;
				break;

			case self::EXPORT_TYPE_HTML:
				$result = self::EXPORT_EXT_HTML;
				break;

			case self::EXPORT_TYPE_PDF:
				$result = self::EXPORT_EXT_PDF;
				break;


		}

		return $result;

	}


	/**
	 * Trunctate a string and add a string at the end
	 * @param string $text
	 * @param number $max
	 * @param string $append
	 * @return string
	 */
	public static function ellipsis($text, $max=100, $append='&hellip;') 	{
		if (strlen(utf8_decode($text)) <= $max) return $text;
		$out = mb_substr($text, 0, $max, "utf-8");
		if (strpos($text,' ') === FALSE) return $out.$append;
		return preg_replace('/ \w+$/','',$out).$append;
	}



	/**
	 *  Check and return true if Redis DB is used to store LMS/domain configuration data
	 */
	public static function isRedisConfigUsed() {
		static $result = null;
		if ($result) return $result;
		$result = isset($GLOBALS['redisConfigLoaded']) && $GLOBALS['redisConfigLoaded'];
		return $result;
	}

	/**
	 * Check if lms is "in maintenance"
	 * lms is "in maintenance" when "active" is set to true and "from" date has been reached
	 */
	public static function isInMaintenance() {
		if(isset(Yii::app()->params['maintenance_mode'])) {
			$dateFrom = Yii::app()->params['maintenance_mode']['from'];
			$today = Yii::app()->localtime->getLocalNow('Y-m-d H:i:s');

			if(Yii::app()->params['maintenance_mode']['active'] && strtotime($dateFrom) < strtotime($today))
				return true;
		}
		return false;
	}


	/**
	 * Are we running on Windows server?
	 * @return boolean
	 */
	public static function isWindows() {
		return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
	}



	/**
	 * Return an array of translated week days
	 *
	 * @param number $sunday Numerical id of the Sunday (0,1)
	 * @return array
	 */
	public static function getWeekDays($sunday = 7) {

		if (!in_array($sunday, array(0,7))) {
			$sunday = 7; // ISO-8601
		}

		$weekDays = array(
			1 	=> Yii::t('calendar', '_MONDAY'),
			2 	=> Yii::t('calendar', '_TUESDAY'),
			3 	=> Yii::t('calendar', '_WEDNESDAY'),
			4 	=> Yii::t('calendar', '_THURSDAY'),
			5 	=> Yii::t('calendar', '_FRIDAY'),
			6 	=> Yii::t('calendar', '_SATURDAY'),
		);
		$weekDays[$sunday] = Yii::t('calendar', '_SUNDAY');

		ksort($weekDays);
		return $weekDays;
	}

	public static function fail($errorMessageToLog, $context = 'application'){
		Yii::log($errorMessageToLog, CLogger::LEVEL_ERROR, $context);
		throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
	}

	public static function savePopupRedirect($url)
	{
		Yii::app()->session['popup_redirect'] = $url;
	}

	public static function showPopupRedirect()
	{
		if(isset(Yii::app()->session['popup_redirect']) && Yii::app()->session['popup_redirect'] !== '')
		{
			$url = self::getRootBaseUrl(true).Yii::app()->session['popup_redirect'];
			unset(Yii::app()->session['popup_redirect']);
			Yii::app()->controller->render('admin.protected.views.common._popup_redirect', array('url' => $url));
		}
	}

	public static function nbspToSpace($str){
		return preg_replace('~\x{00a0}~siu', ' ', $str);
	}


	public static function isAbsoluteUri($string, $mustHavePath=false) {
		$parsed = parse_url($string);
		if ( isset($parsed['scheme']) && isset($parsed['host']) && ($mustHavePath === false || isset($parsed['path']))) return true;
		return false;
	}


	public static function learnerNotAllowedCoursesIds(){
		$coursesIds = array();
		$allowedCoursesCommand = Yii::app()->db->createCommand()->select('idCourse')
			->from(LearningCourseuser::model()->tableName())
			->where('idUser = '.Yii::app()->user->getId().' AND level > '.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
		$allowedCourses = $allowedCoursesCommand->queryColumn();

		$user_coursepath = LearningCoursepathUser::getEnrolledLearningPlanIdsByUser(Yii::app()->user->getId());

		if(empty($user_coursepath))
			return $coursesIds;

		$catchupCommand = Yii::app()->db->createCommand()->select('id_catch_up')
			->from(LearningCourseCatchUp::model()->tableName())
			->where(array('IN', 'id_path', $user_coursepath));

		if($allowedCourses && is_array($allowedCourses)){
			$catchupCommand->andWhere('id_catch_up  NOT IN ('.implode(',', $allowedCourses).')');
		}

		$catchupCourses = $catchupCommand->queryColumn();
		if($catchupCourses && is_array($catchupCourses)){
			$coursesIds = $catchupCourses;
		}

		return $coursesIds;
	}

	/**
	 * Check if a string is a valid JSON
	 *
	 * @param string $string
	 * @return boolean
	 */
	public static function isJson($string) {
		return is_string($string) && is_object(json_decode($string)) ? true : false;
		// Here is another, faster but not so good approach
		//return preg_match('/^\s*[\[{]/', $string) > 0;
	}

	/**
	 * @param $value
	 * @return mixed
	 */
	public static function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
		$escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
		$replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
		$result = str_replace($escapers, $replacements, $value);
		return $result;
	}



	/**
	 * Check if string is enclosed between pair of given character (and NO such character is found inbetween)
	 *
	 * @param string $str
	 */
	public static function isStringQuoted($str, $char='"') {
	    $pattern = '/^'.$char.'(?:[^'.$char.']|\\.)*'.$char.'$/';
	    return preg_match($pattern, trim($str)) === 1;
	}



	public  static function formatQuestionText($question, $length = 0) {
		$output =  strip_tags($question, "<p><ul><ol><li>");
		if ($length > 3 && mb_strlen($output, 'UTF-8') > $length) {
			$output = mb_substr($output, 0, $length - 3, 'UTF-8').'...';
		}
		return $output;
	}

	public static function newLineToBr($string){
		$replaceOrder = array("\r\n", "\n", "\r");
		$replaceWith = "<br/>";
		$string = str_replace($replaceOrder, $replaceWith, $string);
		return $string;
	}

	static public function getDefaultSenderEmail() {
		$fromEmail = trim(Settings::get('sender_event', FALSE));
		if (empty($fromEmail))
			$fromEmail = isset(Yii::app()->params['support_sender_email']) ? Yii::app()->params['support_sender_email'] : '';

		// Fallback to info@docebo.com
		if (empty($fromEmail))
			$fromEmail = "info@docebo.com";

		return $fromEmail;
	}


	/**
	 * Sort a 2 dimensional array, by the value of a key from the second dimension
	 * array(
	 *      [0] => array(
	 *                  ["key1"] => "key1Value0",
	 *                  ["key2"] => "x",
	 *                  ["key3"] => "y",
	 *                  ["sort_key"] => "key1",
	 *             ),
	 *      [1] => array(
	 *                  ["key1"] => "key1Value1",
	 *                  ["key2"] => "x",
	 *                  ["key3"] => "y",
	 *                  ["sort_key"] => "key2",
	 *             )
	 * )
	 * @param array $array Input array
	 * @param string $sort_type  "string" | "timestamp" | "number"
	 * @param string $sort_dir "asc" | "desc"
	 * @param string $sort_key Arbitrary second-dimension key, pointing to key used to sort (compare) array items
	 * @return array
	 */
	public static function sort2DArray($array, $sort_type, $sort_dir, $sort_key) {

	    $sort_dir = strtolower($sort_dir);
	    $desc_dir = strtolower(self::SORT_DIR_DESC);

	    $sortResult = usort($array, function($a, $b) use ($sort_type, $sort_dir, $sort_key, $desc_dir) {

	        $v1Key = $a[$sort_key];
	        $v2Key = $b[$sort_key];

	        $v1 = $a[$v1Key];
	        $v2 = $b[$v2Key];

	        switch ($sort_type) {
	            case self::SORT_TYPE_STRING:
	                $compare = mb_strcasecmp($v1, $v2);
	                break;
	            case self::SORT_TYPE_TIMESTAMP:
	                $t1 = strtotime($v1);
	                $t2 = strtotime($v2);
	                $compare = ($t1 < $t2) ? -1 : (($t1 > $t2) ? 1 : 0);
	                break;
	            case self::SORT_TYPE_NUMBER:
	                $compare = ($v1 < $v2) ? -1 : (($v1 > $v2) ? 1 : 0);
	                break;
	            default:
	                $compare = 0;
	                break;
	        }

	        $m = ($sort_dir == $desc_dir) ? -1 : 1;
	        $result = $m * $compare;
	        return $result;

	    });

        return $array;

	}


	/**
	 * Returns the mobile app URL
	 * @return string
	 */
	public static function getWebappUrl($makeAbsolute = false) {
		return ($makeAbsolute ? Docebo::getRootBaseUrl(true) : self::getCurrentDomain()).'/webapp';
	}

	/**
	 * Return 2nd level domain of a host/domain
	 *
	 * @param string $domain
	 */
	public static function get2ndLvlDomainName($domain)
	{
	    // a list of decimal-separated TLDs
	    static $doubleTlds = [
	        'co.uk',
	        'me.uk',
	        'net.uk',
	        'org.uk',
	        'sch.uk',
	        'ac.uk',
	        'gov.uk',
	        'nhs.uk',
	        'police.uk',
	        'mod.uk',
	        'asn.au',
	        'com.au',
	        'net.au',
	        'id.au',
	        'org.au',
	        'edu.au',
	        'gov.au',
	        'csiro.au',
	        'br.com',
	        'com.cn',
	        'com.tw',
	        'cn.com',
	        'de.com',
	        'eu.com',
	        'hu.com',
	        'idv.tw',
	        'net.cn',
	        'no.com',
	        'org.cn',
	        'org.tw',
	        'qc.com',
	        'ru.com',
	        'sa.com',
	        'se.com',
	        'se.net',
	        'uk.com',
	        'uk.net',
	        'us.com',
	        'uy.com',
	        'za.com'
	    ];

	    // check if we can parse the URL
	    if (is_string($domain)) {

	        // Sanitize the domain name
	        $domain = strtolower($domain);

	        // get parts of the domain
	        $parts = explode('.', $domain);

	        // if we have just one part (eg localhost)
	        if (! isset($parts[1])) {
	            return $parts[0];
	        }

	        // grab the TLD
	        $tld = array_pop($parts);

	        // grab the hostname
	        $domain = array_pop($parts) . '.' . $tld;

	        // have we collected a double TLD?
	        if (! empty($parts) && in_array($domain, $doubleTlds)) {
	            $domain = array_pop($parts) . '.' . $domain;
	        }

	    }

	    return $domain;
	}


	/**
	 * Roll a 1-milion-sided dice and return true/false, based on requested probability
	 *
	 * @param number $probablity In terms of parts of a million (1..1000000)
	 *
	 * @return boolean  True/False
	 */
	public static function dice($probablity=0) {

		if ((int) $probablity <= 0) {
			return false;
		}
		if ((int) $probablity >= 1000000) {
			return true;
		}
		$rand = mt_rand(1,1000000);
		return ($rand >= 1 and $rand <= $probablity);
	}

	static public function deleteFolderRecursive($path){
		if (is_dir($path) === true)
		{
			$files = array_diff(scandir($path), array('.', '..'));

			foreach ($files as $file)
			{
				self::deleteFolderRecursive(realpath($path) . '/' . $file);
			}

			return rmdir($path);
		}

		else if (is_file($path) === true)
		{
			return unlink($path);
		}

		return false;
	}

	static public function copyFolderRecursive($srcFolder, $dstFolder) {
		$dir = opendir($srcFolder);
		@mkdir($dstFolder, 0777, true);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($srcFolder . '/' . $file) ) {
					self::copyFolderRecursive($srcFolder . '/' . $file,$dstFolder . '/' . $file);
				}
				else {
					copy($srcFolder . '/' . $file,$dstFolder . '/' . $file);
					@chmod($dstFolder . '/' . $file, 0777);
				}
			}
		}
		closedir($dir);
	}

	/**
	 * Create an angular routing based URL for App7020's  "assets/view"
	 * This is to create PRESENTATIONAL URL, can NOT be used for angular based deep linking!
	 *
	 * @param string $id String representing assets ID
	 * @param boolean $absolute Make the URL absolute or not (default)
	 */
	static function createApp7020AssetsViewUrl($id, $absolute=false) {
	    $angularRoute = "/assets/view/" .  $id;
	    if ($absolute) {
	       return self::createAbsoluteApp7020Url("", array("#" => $angularRoute));    
	    }
	    else {
	        return self::createApp7020Url("", array("#" => $angularRoute));
	    }
	}


	/**
	 * Create an angular routing based URL for App7020's  "assets/view".
	 * This is to create DEEP LINK URL, used to "land" directly on App7020 Asset View 
	 *
	 * @param string $id String representing assets ID
	 * @param boolean $absolute Make the URL absolute or not (default)
	 */
	static function createApp7020AssetsViewNgrUrl($id, $absolute=false) {
	    $angularRoute = "/assets/view/" .  $id;
	    if ($absolute) {
	        return self::createAbsoluteApp7020Url("", array("ngr" => $angularRoute));
	    }
	    else {
	        return self::createApp7020Url("", array("ngr" => $angularRoute));
	    }
	}


	/**
	 * Create an angular routing based URL for App7020's  "assets/peerReview"
	 * This is to create PRESENTATIONAL URL, can NOT be used for angular based deep linking!
	 *
	 * @param string $id String representing assets ID
	 * @param boolean $absolute Make the URL absolute or not (default)
	 */
	static function createApp7020AssetsPeerReviewUrl($id, $absolute=false) {
		$angularRoute = "/assets/peerReview/" .  $id;
		if ($absolute) {
			return self::createAbsoluteApp7020Url("", array("#" => $angularRoute));
		}
		else {
			return self::createApp7020Url("", array("#" => $angularRoute));
		}
	}


	/**
	 * Create an angular routing based URL for App7020's  "assets/peerReview".
	 * This is to create DEEP LINK URL, used to "land" directly on App7020 Asset View
	 *
	 * @param string $id String representing assets ID
	 * @param boolean $absolute Make the URL absolute or not (default)
	 */
	static function createApp7020AssetsPeerReviewNgrUrl($id, $absolute=false) {
		$angularRoute = "/assets/peerReview/" .  $id;
		if ($absolute) {
			return self::createAbsoluteApp7020Url("", array("ngr" => $angularRoute));
		}
		else {
			return self::createApp7020Url("", array("ngr" => $angularRoute));
		}
	}




	static public function publishScriptFile($fileName, $pathAlias){
		$path = Yii::getPathOfAlias($pathAlias).DIRECTORY_SEPARATOR.$fileName;
		$publishedUrl = Yii::app()->assetManager->publish($path);
		Yii::app()->clientScript->registerScriptFile($publishedUrl);
	}
	static public function publishStyleFile($fileName, $pathAlias){
		$path = Yii::getPathOfAlias($pathAlias).DIRECTORY_SEPARATOR.$fileName;
		$publishedUrl = Yii::app()->assetManager->publish($path);
		Yii::app()->clientScript->registerCssFile($publishedUrl);
	}

	/**
	 * List of white listed routes and fields, which are skipped in PurifyRequest.
	 * 
	 * IMPORTANT: Routes/Fields are Regex patterns, later put between ^ and $  !!!
	 * 
	 * Up to 1 one module only, e.g. this is ok:
	 * 
	 * 	   module/controller/action
	 * 
	 * while this won't be handled properly:
	 * 
	 *     m1/m2/controller/action
	 * 
	 * @return string[][]
	 */
	static public function purifyRequestWhitelist() {

		// Regex Patterns, without ^ (line start) and $ (line end) !!!!
		// Matched case insensitively!
		$whitelist =  array(
			array(
				'route'		=> 'reportManagement/createUserReport',
				'field'		=> 'donutContent',
			),
			array(
				'route'		=> 'reportManagement/createUserReport',
				'field'		=> 'lineContent',
			),
			array(
				'route'		=> 'reportManagement/createUserReport',
				'field'		=> 'singleDonutContent',
			),
			array(
				'route'		=> 'googleAnalyticsApp/googleAnalyticsApp/settings',
				'field'		=> 'GoogleAnalyticsAppSettingsForm\[ga_code\]',
			),
			array(
				'route'		=> 'vivochaApp/vivochaApp/settings',
				'field'		=> 'VivochaAppSettingsForm\[vivocha_code\]',
			),
			array(
				'route'		=> 'zendeskApp/zendeskApp/settings',
				'field'		=> 'ZendeskAppSettingsForm\[js_code\]',
			),

			// SimpleSamplApp, allow the whole route
			array(
				'route'		=> 'SimpleSamlApp/SimpleSamlApp/settings',
			),

			array(
				'route'		=> 'xapi/lrs/index',
			),

			array(
				'route'		=> 'reportManagement/createCourseReport',
			),

			// Field whitelisting, no matter the route
			array('field'		=> "r"),
			array('field'		=> "YII_CSRF_TOKEN"),
			array('field'		=> "ajax"),

			array('field'		=> "BrandingCustomizeCssForm\[custom_css\].*"),
			array('field'		=> "CoreMultidomain\[customCss\].*"),

			array(
				"route"			=> "certificateManagement/template",
				"field"			=> "LearningCertificate\[cert_structure\]"
			),

 			array('field'		=> "wizard\[edit-notification-form\]\[messages\].*"),
 			array('field'		=> "wizard\[edit-notification-form\]\[notification_message\].*"),

			array(
				"route"			=> "OktaApp/OktaApp/settings",
				"field"			=> "SimpleSamlAppSettingsForm\[simplesaml_federation_metadata\]"
			),

			// Because of Multidomain/SAML settings
			array(
				"field"			=> "SimpleSamlAppSettingsForm\[simplesaml_federation_metadata\]",
			),

			//Offline player
			array(
				'route'			=> 'OfflinePlayerApp/cmi/.*'
			),
				
			array(
				'route'			=> 'OfflinePlayerApp/branding/.*'
			)
		);
		return $whitelist;
	}

	/**
	 * Purify _GET, _POST and _REQUEST to prevent XSS
	 *
	 * NOTE: this will remove all <script> tags!
	 */
	static public function purifyRequest() {

		// Skip for console applications
		if (!(Yii::app() instanceof CWebApplication)) {
			return;
		}

		// Build route: max. 1 module is handled (!!!!)
		$route = Yii::app()->controller->id . "/"  . Yii::app()->controller->action->id;
		if (Yii::app()->controller->module) {
			$route = Yii::app()->controller->module->id . "/" . $route;
		}

		// Lower case, to avoid stupid typos in whitelisted routes and fields
		$route = strtolower($route);

		// Also, Make sure whitelised items are lower case too
		$whitelist = self::purifyRequestWhitelist();
		array_walk_recursive($whitelist, function(&$item, $key){
			$item = strtolower($item);
		});

		// Callback for array_walk.
		// We can't use array_walk_recursive, because it will not visit the sub-arrays as key => value
		// Example:
		//		$array = array(
		//			'k1'	=> 'v1',
		//			'subArray' => array(
		//				'sk1' => 'sv1'
		//			),
		//		)
		// Here array_walk_recursive will visit k1 and sk1, but will not visit "subArray".
		// It visits only elements of the array, but skips elements which are array themselves.
		// For this reason we have to "emulate" that function using simple array_walk, called recursively
		$walkCallBack = function (&$item, $key, $parentKey) use (&$walkCallBack, $whitelist, $route) {
			if (is_array($item) && !empty($item)) {
				if (count($item) <= 100) {
					if ($parentKey) {
						$key = $parentKey."[".$key."]";
					}
					array_walk($item, $walkCallBack, $key);
				}
			}
			else {

				if ($parentKey) {
					$field = $parentKey."[".$key."]";
				}
				else {
					$field = $key;
				}
				// Again, lower case
				$field = strtolower($field);

				$whitelisted = false;
				foreach ($whitelist as $wlItem) {

					// Make patterns. Escape square brackets in fields.
					$routePattern = isset($wlItem['route']) && !empty($wlItem['route']) ? $wlItem['route'] : ".*" ;
					$fieldPattern = isset($wlItem['field']) && !empty($wlItem['field']) ? $wlItem['field'] : ".*" ;

					$routeMatch = preg_match('#^'.$routePattern.'$#i', $route);
					$fieldMatch = preg_match('#^'.$fieldPattern.'$#i', $field);

					if ($routeMatch && $fieldMatch) {
						$whitelisted = true;
						//Log::_("WHITELISTED: " . $route . " --> " . $field);
						break;
					}
				}
				// if NOT whitelisted, purify
				if (!$whitelisted && is_string($item) && (mb_strlen($item) > 15)) {

					// If the item is JSON, decode and run the purifier over it (walk again)
					if (self::isJson($item)) {
						$itemArray = json_decode($item, true);
						array_walk($itemArray, $walkCallBack, false);
						$item = json_encode($itemArray);
					}
					// Otherwise .. check if it is an HTML
					else if (self::isHtml($item)) {
						//Log::_("PURIFY:"  .$item);
						$item = Yii::app()->htmlpurifier->purify($item, 'xssGuard');  // See DoceboCHtmlPurifier::__constructor() for xssGuard
					}
				}


			}
		};

		// Walk through input and check all fields
		array_walk($_GET, $walkCallBack, false);
		array_walk($_POST, $walkCallBack, false);

		// Instead of purifying again RQUEST elements, just take the already purified value from original GET/POST
		// NOTE: that means $_COOKIE is missed from purification process
		foreach ($_REQUEST as $k =>$v) {
			if (isset($_POST[$k])) {
				$_REQUEST[$k] = $_POST[$k];
			}
			else if (isset($_GET[$k])) {
				$_REQUEST[$k] = $_GET[$k];
			}
		}

	}

	/**
	 * Wrap a string with HTML code and then check if <body> has any HTML in it, i.e. non "#text" node
	 *
	 * @param string $string
	 * @return boolean
	 */
	static function isHtml($string) {
		$doc = new DOMDocument();
		$html = <<< EOT
			<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" >
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
			<body>
				$string
			</body>
			</html>
EOT;
		// Mute error handler to skip cases when string is broken HTML
		set_error_handler("muteErrorHandler");
		$doc->loadHTML($html);
		restore_error_handler();

		// Get the body
		$body = $doc->getElementsByTagName('body')->item(0);

		// Check all BODY children and see if ANY of it is NOT #text, i.e. it is a tag.
		foreach ($body->childNodes as $node) {
			if ($node->nodeName != "#text") {
				return true;
			}
		}
		return false;
	}

	/**
	 * Because of HTMLPurifier encoding [ and ], we need this method so we can decode them back.
	 */
	static function decodeSquareBrackets($input) {
		$result = $input;
		$result = str_replace("%5B", "[", $result);
		$result = str_replace("%5D", "]", $result);
		return $result;
	}
	

	/**
	 * 
	 * @param integer $amount
	 * @return string|boolean
	 */
	static function resolveLmsPricingPlan($amount) {
	    
	    if ($amount <= 100)
            return 'starter';
	    elseif ($amount == 300)
	        return 'advanced';
	    elseif ($amount > 300)
	        return 'enterprise';
	    
	    return false;
	    
	}


}


if (!function_exists('muteErrorHandler')) {
	/**
 	 * @param int $errno
 	 * @param string $errstr
 	 */
	function muteErrorHandler($errno, $errstr){}
}


/**
 *  If getallheaders() is not available, define it
 *  See http://www.php.net/manual/en/function.getallheaders.php
 *
 */
if (!function_exists('getallheaders'))
{
	function getallheaders()
	{
		$headers = '';
		foreach ($_SERVER as $name => $value)
		{
			if (substr($name, 0, 5) == 'HTTP_')
			{
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}


}


if (!function_exists('mb_strcasecmp')) {
    function mb_strcasecmp($s1, $s2, $enc=false) {
        if ($enc===false) {
            $enc=mb_internal_encoding();
        }
        return strcmp(mb_strtoupper($s1, $enc), mb_strtoupper($s2, $enc));
    }
}
