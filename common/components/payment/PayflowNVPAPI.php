<?php

/**
 * Class PayflowNVPAPI
 *
 * Required settings:
 * payflow_enabled = on|off
 * payflow_vendor
 * payflow_user
 * payflow_pwd
 */
class PayflowNVPAPI extends Payment {

	// Set this to "live" for the live server, "pilot" for the test server, or "sandbox"
	// for Payflow accounts created through a Website Payments Pro account on the Sandbox.
	protected $environment;

	public function __construct($mode) {
		parent::__construct($mode);

		$oldStripRelease = Yii::app()->urlManager->getStripRelease();
		Yii::app()->urlManager->setStripRelease(false);

		// Set self-describing attribute
		$this->payment_method = self::$PAYMENT_METHOD_PAYFLOW;

		// These two URL's will be changed on the fly later adding Transaction Id.
		$this->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled");
		$this->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished");

		Yii::app()->urlManager->setStripRelease($oldStripRelease);
	}

	// parse_payflow_string: Parses a response string from Payflow and returns an
	// associative array of response parameters.
	public function parse_payflow_string($str) {
		$workstr = $str;
		$out = array();

		while(strlen($workstr) > 0) {
			$loc = strpos($workstr, '=');
			if($loc === FALSE) {
				// Truncate the rest of the string, it's not valid
				$workstr = "";
				continue;
			}

			$substr = substr($workstr, 0, $loc);
			$workstr = substr($workstr, $loc + 1); // "+1" because we need to get rid of the "="

			if(preg_match('/^(\w+)\[(\d+)]$/', $substr, $matches)) {
				// This one has a length tag with it.  Read the number of characters
				// specified by $matches[2].
				$count = intval($matches[2]);

				$out[$matches[1]] = substr($workstr, 0, $count);
				$workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
			} else {
				// Read up to the next "&"
				$count = strpos($workstr, '&');
				if($count === FALSE) { // No more "&"'s, read up to the end of the string
					$out[$substr] = $workstr;
					$workstr = "";
				} else {
					$out[$substr] = substr($workstr, 0, $count);
					$workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
				}
			}
		}

		return $out;
	}

	// run_payflow_call: Runs a Payflow API call.  $params is an associative array of
	// Payflow API parameters.  Returns FALSE on failure, or an associative array of response
	// parameters on success.
	public function run_payflow_call($params) {

		$paramList = array();
		foreach($params as $index => $value)
			$paramList[] = $index . "[" . strlen($value) . "]=" . $value;

		Yii::log('running payflow call with these params at URL:'.$this->gatewayUrl, CLogger::LEVEL_INFO);
		Yii::log(CVarDumper::dumpAsString($paramList), CLogger::LEVEL_INFO);

		$apiStr = implode("&", $paramList);


		// Initialize our cURL handle.
		$curl = curl_init($this->gatewayUrl);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		// If you get connection errors, it may be necessary to uncomment
		// the following two lines:
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($curl, CURLOPT_POST, TRUE);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $apiStr);

		$result = curl_exec($curl);
		if($result === FALSE) {
			Yii::log('curl error: '.CVarDumper::dumpAsString(curl_error($curl)));
			echo curl_error($curl);
			return FALSE;
		}
		else return $this->parse_payflow_string($result);
	}

	/**
	 * Set test mode on or off
	 *
	 * @param bool $isTestMode
	 * @internal param bool $mode
	 */
	public function setTestMode($isTestMode)
	{
		if ($isTestMode) {
			$this->environment = 'sandbox';
		}
		else {
			$this->environment = 'pilot';
		}

		// Which endpoint will we be using?
		if($this->environment == "sandbox")
			$this->gatewayUrl = "https://pilot-payflowpro.paypal.com/";
		else
			$this->gatewayUrl = "https://payflowpro.paypal.com";
	}

	/**
	 * Sets payment data using the passed parameters
	 *
	 * @param int $idUser User Id
	 * @param  EShoppingCart $shoppingCart
	 * @param int $transaction_id Transaction Id (pre-created)
	 * @throws CException
	 */
	public function preparePayment($idUser, $shoppingCart, $transaction_id)
	{
		// First, get billing info data
		$b_info = CoreUserBilling::model()->getUserLastBillingInfo($idUser);

		// Get user information from database
		$u_info = CoreUser::model()->findByPk($idUser);

		// Enumerate cart items and add data for each item
		$totalAmount = 0;
		foreach ( $shoppingCart->getPositions() as $position ) {
			$totalAmount += intval( $position->getSumPrice() );
		}

		/////////
		////
		//// Otherwise, begin hosted checkout pages flow
		////

		//Build the Secure Token request
		$request = array(
			"VENDOR" => trim(Settings::get('payflow_vendor', '')),
			"PARTNER" => "PayPal",
			"USER" => Settings::get('payflow_user'),
			"PWD" => Settings::get('payflow_pwd'),
			"TRXTYPE" => "S",
			"AMT" => $totalAmount,
			"CURRENCY" => $shoppingCart->currencyCode,
			"CREATESECURETOKEN" => "Y",
			"SECURETOKENID" => uniqid('MySecTokenID-'), //Should be unique, never used before
			"RETURNURL" => $this->returnUrl,
			"ERRORURL" => $this->returnUrl,

			// Payflow will ignore the cancel URL field that you entered in PayPal Manager (https://manager.paypal.com/)
			// if you select layout template C.
			"CANCELURL" => $this->cancelUrl,

			// In practice you'd collect billing and shipping information with your own form,
			// then request a secure token and display the payment iframe.
			// --> See page 7 of https://cms.paypal.com/cms_content/US/en_US/files/developer/Embedded_Checkout_Design_Guide.pdf
			// This example uses hardcoded values for simplicity.

			"BILLTOFIRSTNAME" => $u_info->firstname,
			"BILLTOLASTNAME" => $u_info->lastname,
			"BILLTOSTREET" => $b_info->bill_address1,
			"BILLTOCITY" => $b_info->bill_city,
			"BILLTOSTATE" => $b_info->bill_country_code,
			"BILLTOZIP" => $b_info->bill_zip,
			"BILLTOCOUNTRY" => $b_info->bill_state,

			"SHIPTOFIRSTNAME" => $u_info->firstname,
			"SHIPTOLASTNAME" => $u_info->lastname,
			"SHIPTOSTREET" => $b_info->bill_address1,
			"SHIPTOCITY" => $b_info->bill_city,
			"SHIPTOSTATE" => $b_info->bill_country_code,
			"SHIPTOZIP" => $b_info->bill_zip,
			"SHIPTOCOUNTRY" => $b_info->bill_state,
		);

		//Run request and get the secure token response
		$response = $this->run_payflow_call($request);

		if ($response['RESULT'] != 0) {
			Yii::log("Payflow returned error:");
			Yii::log(CVarDumper::dumpAsString($response), CLogger::LEVEL_INFO);
			throw new CException("Payflow call failed: ");
		}

		$response['mode'] = $this->environment == 'sandbox' ? 'TEST' : 'LIVE';
		// save the data in session
		Yii::log('payflow data: '.CVarDumper::dumpAsString($response), CLogger::LEVEL_INFO);
		Yii::app()->session['payflowData'] = $response;
	}

	/**
	 * Return a form to redirect the user to the 2nd step of the checkout,
	 * where we render the embedded checkout iframe
	 * @return string
	 */
	public function getSubmitForm() {
		$html = "";
		$url = Docebo::createAppUrl('lms:cart/embeddedCheckout');
		$html .= CHtml::beginForm($url, 'post', array('target'=>'_top', 'id'=>'payment_redirect_form'));
		$html .= CHtml::endForm();
		return $html;
	}

	/**
	 * Validate incoming IPN notification / Authorize.net API response.
	 *
	 * Internally using HTTP REQUEST data
	 */
	protected function validateTransaction()
	{
		// don't need to validate, as this should be a direct payment
		// so we know in the api response if the transactions is valid or not
		return true;
	}
}