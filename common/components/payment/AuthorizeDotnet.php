<?php
/**
 * Authorize.net payment gateway
 * 
 * @author Andrei Amariutei
 *
 */
class AuthorizeDotnet extends Payment {

	/**
	 * Relay-Response URL: Authorize.net will POST transaction response there, getting HTML back to show to user
	 * @var string
	 */
	public $relayResponseUrl;
    
    /**
     * Constructor 
     * 
     * @param boolean $mode Test mode (true/false)
     */
    public function __construct($mode) {

        parent::__construct($mode);
        
        // Set self-describing attribute
        $this->payment_method = parent::$PAYMENT_METHOD_AUTHORIZE;
        
        // IPN notification URL ("silent POST URL", in terms of Authorize.net)
        $this->ipnUrl = Docebo::createAbsoluteLmsUrl("cart/ipnAuthnet");
        
        // These URL's will be changed on the fly later adding Transaction Id.
        $this->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled");
        $this->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished");
        
        // Relay-response URL: where Authorize.net will POST transaction response and get HTML to show to user after payment
        $this->relayResponseUrl = Docebo::createAbsoluteLmsUrl("cart/authnetRelayResponse");
        
        // Collect REQUEST data into a class attribute
        if ( isset($_REQUEST) ) {
        	foreach ( $_REQUEST as $key=>$value ) {
        		$this->ipnData["$key"] = $value;
        	}
        }
        $this->logIpnData(true);
        

    }

    /**
     * Set payment mode: test or live.
     *
     * @see Payment::setTestMode()
     */
    public function setTestMode($mode) {

        $this->testMode = $mode;

        if ( $this->testMode ) {
            $this->gatewayUrl = 'https://test.authorize.net/gateway/transact.dll';
        }
        else {
            $this->gatewayUrl = 'https://secure2.authorize.net/gateway/transact.dll';
        }

    }
    

    /**
     * Fill class attribute "data" with all required payment gateway specific data fields.
     * @todo Improve billing information collecting
     * 
     * @see Payment::setPaymentData()
     */
    public function preparePayment($idUser, $shoppingCart, $transaction_id) {

        // First, get billing info data
        $b_info = CoreUserBilling::model()->getUserLastBillingInfo($idUser);

        // Get user information from database
        $u_info = CoreUser::model()->findByPk($idUser);

        $countries = CoreCountry::model()->findAll(array('order' => 'name_country'));
        $countriesListData = CHtml::listData($countries, 'iso_code_country', 'name_country');

        $this->addData('x_first_name', $u_info->firstname);
        $this->addData('x_last_name', $u_info->lastname);
        $this->addData('x_cust_id', $u_info->idst);
        $this->addData('x_address', $b_info->bill_address1 . ' ' . $b_info->bill_address2);
        $this->addData('x_country', isset($countriesListData[$b_info->bill_country_code]) ? $countriesListData[$b_info->bill_country_code] : $b_info->bill_country_code);
        $this->addData('x_city', $b_info->bill_city);
        $this->addData('x_currency_code', Settings::get("currency_code"));
			if (!empty($b_info) && !empty($b_info->bill_company_name)) { $this->addData('x_company', $b_info->bill_company_name); }

        // Enumerate cart items and add data for each item
        // shopping cart class attributes _editions and _classrooms
        $amount = 0;
        $description = array();
        foreach ( $shoppingCart->getPositions() as $position ) {
            $amount += ($position->getQuantity() * $position->getSumPrice());
            $description[] = $position->getPositionName();
        }

        $transactionModel = EcommerceTransaction::model()->with('ecommerceCoupon')->findByPk($transaction_id);
        $discount = floatval($transactionModel->discount);
        if ($discount != 0) {
            $amount = $amount - $discount;
        }

        // Using method 2: Passing Individual Items to Authorize.net
        $authorizeLoginID = Settings::get('authorize_loginid');
        $authorizeTransactionKey = Settings::get('authorize_key');

        // an invoice is generated using the date and time
        $invoice	= date(YmdHis);
        // a sequence number is randomly generated
        $sequence	= $transaction_id . time();
        // a timestamp is generated
        $timeStamp	= time();
		//currency code
		$currencyCode = Settings::get('currency_code');

    	// The following lines generate the SIM fingerprint.  PHP versions 5.1.2 and
	    // newer have the necessary hmac function built in.  For older versions, it
    	// will try to use the mhash library.
        if( version_compare(phpversion(),'5.1.2') >= 0 ) {
            $fingerprint = hash_hmac("md5", $authorizeLoginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^" . $currencyCode, $authorizeTransactionKey);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $authorizeLoginID . "^" . $sequence . "^" . $timeStamp . "^" . $amount . "^" . $currencyCode, $authorizeTransactionKey));
        }

        $this->addData('x_login', $authorizeLoginID);
        $this->addData('x_amount', $amount);
        $this->addData('x_description', implode(', ', $description));
        $this->addData('x_invoice_num', $invoice);
        $this->addData('x_fp_sequence', $sequence);
        $this->addData('x_fp_timestamp', $timeStamp);
        $this->addData('x_fp_hash', $fingerprint);
        $this->addData('x_test_request', 'false');
        $this->addData('x_show_form', 'PAYMENT_FORM');
		$this->addData('x_currency_code', $currencyCode);


        // return link
        $this->addData('x_receipt_link_method', 'POST');
        $this->addData('x_receipt_link_text', Yii::t('standard', '_BACK'));
        $this->addData('x_receipt_link_url', $this->returnUrl);
        
        // Relay-response
        if ($this->relayResponseUrl) {
        	$this->addData('x_Relay_Response', 'true');
        	$this->addData('x_Relay_URL', $this->relayResponseUrl);
            $this->addData('id_trans', $transaction_id);
        }
        

        // cancel link
        $this->addData('x_cancel_url', $this->cancelUrl);
        $this->addData('x_cancel_url_text', Yii::t('standard', '_CANCEL'));

        $this->logData(true);
    }
    
    
    /**
     * Validates the transaction: just check hashes/fingerprints etc. to make sure the security is ok.
     *
     * @return bool
     */
    public function validateTransaction() {
        $authorizeHash 		= Settings::get('authorize_hash');
        $authorizeLoginId 	= Settings::get('authorize_loginid');
        $transactionId 		= Yii::app()->request->getParam('x_trans_id', -1);
        $amount 			= Yii::app()->request->getParam('x_amount', -1);
        
        // These two must match
        $theirHash 	= strtoupper(Yii::app()->request->getParam('x_MD5_Hash'));
        $ourHash 	= strtoupper(md5($authorizeHash . $authorizeLoginId . $transactionId . $amount));
        
        return ($theirHash === $ourHash);
    }
    
    
}