<?php

/**
 * Stripe payment gateway
 *
 * @author Boyan Georgiev
 * Date: 11/24/2015
 * Time: 3:17 PM
 */
class Stripe extends Payment
{

    public function __construct($mode)
    {
        parent::__construct($mode);

        // Set self-describing attribute
        $this->payment_method = parent::$PAYMENT_METHOD_STRIPE;

    }

    /**
     * Set test mode on or off
     *
     * @param boolean $mode
     */
    public function setTestMode($mode)
    {

    }

    /**
     * Sets payment data using the passed parameters
     *
     * @param int $idUser User Id
     * @param  EShoppingCart $shoppingCart
     * @param int $transaction_id Transaction Id (pre-created)
     */
    public function preparePayment($idUser, $shoppingCart, $transaction_id)
    {
		/*
        // Enumerate cart items and add data for each item
        // shopping cart class attributes _editions and _classrooms
        $amount = 0;
        $description = array();
        foreach ( $shoppingCart->getPositions() as $position ) {
            $amount += ($position->getQuantity() * $position->getSumPrice());
            $description[] = $position->getPositionName();
        }

        // remove the discount
        $transactionModel = EcommerceTransaction::model()->with('ecommerceCoupon')->findByPk($transaction_id);
        $discount = floatval($transactionModel->discount);
        if ($discount != 0) {
            $amount = $amount - $discount;
        }
		*/

    }


    /**
     * Validate incoming IPN notification / Authorize.net API response.
     *
     * Internally using HTTP REQUEST data
     */
    protected function validateTransaction()
    {
        return true;
    }


	/**
	 * Return a simple list of language codes supported by Stripe.
	 * Reference: https://support.stripe.com/questions/what-languages-does-stripe-checkout-support
	 * @return array
	 */
	public static function getSupportedLocales() {
		return array(
			'zh',//'Simplified Chinese'
			'nl',//'Dutch'
			'en',//'English'
			'fr',//'French'
			'de',//'German'
			'it',//'Italian'
			'ja',//'Japanese'
			'es',//'Spanish'
		);
	}


	/**
	 * Return current supported locale language for Stripe.
	 * If current language is not supported by Stripe then default "auto" value is returnede.
	 * @return string
	 */
	public static function getCurrentLocale() {
		$localeId = Yii::app()->locale->id;
		if (strlen($localeId) > 2) { //make sure not to have a locale variant, e.g. like en-gb or similar
			$localeId = substr($localeId, 0, 2);
		}
		return (in_array($localeId, self::getSupportedLocales()) ? $localeId : 'auto');
	}

}