<?php
/**
 * Paypal payment gateway
 * 
 * @author Plamen Petkov
 *
 */
class Paypal extends Payment {

    
    /**
     * Constructor 
     * 
     * @param boolean $mode Test mode (true/false)
     */
    public function __construct($mode) {
        
        parent::__construct($mode);
        
        $oldStripRelease = Yii::app()->urlManager->getStripRelease();
        Yii::app()->urlManager->setStripRelease(false);
        
        // Set self-describing attribute
        $this->payment_method = parent::$PAYMENT_METHOD_PAYPAL;
        
        // URLs
        $paypal_ipn_baseurl = Settings::get('paypal_ipn_baseurl','');
        if ( !empty($paypal_ipn_baseurl) ) {
            $paypal_notify_url = $paypal_ipn_baseurl . "/lms/?r=cart/ipnPaypal";
        }
        else {
            $paypal_notify_url = Docebo::createAbsoluteLmsUrl("cart/ipnPaypal");
        }
        
        $this->ipnUrl = $paypal_notify_url;
        
        // These two URL's will be changed on the fly later adding Transaction Id.
        $this->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled");
        $this->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished");

        Yii::app()->urlManager->setStripRelease($oldStripRelease);
                
    }
    
    /**
     * Set payment mode: test or live.
     * 
     * @see Payment::setTestMode()
     */
    public function setTestMode($mode) {
        
        $this->testMode = $mode;
        
        if ( $this->testMode ) {
            $this->gatewayUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        }
        else {
            $this->gatewayUrl = 'https://www.paypal.com/cgi-bin/webscr';
        }
        
    }
    

    /**
     * Fill class attribute "data" with all required payment gateway specific data fields.
     * @todo Improve billing information collecting
     * 
     * @see Payment::setPaymentData()
     */
    public function preparePayment($idUser, $shoppingCart, $transaction_id) {

        /*
        NOTE:
        The following are the most common issues reported by merchants on their integrations:
         • Passing values other than an integer or “.” in the amount field
         • Passing country code values greater than two letters
         • Passing more than 127 characters in the item_number field (for example, using item_number for Note to Buyer information)
         • Passing more than 64 characters in option fields (on0, on1, etc.)
         • Passing duplicate empty parameters
        */

        $billingInfo = array();

        // First, get billing info data
        $b_info = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
        $billingInfo['address1'] = $b_info->bill_address1;
        $billingInfo['address2'] = $b_info->bill_address2;
        $billingInfo['country']  = $b_info->bill_country_code;
        $billingInfo['city']     = $b_info->bill_city;

        // Get user information from database
        $u_info = CoreUser::model()->findByPk($idUser);

        // Using method 2: Passing Individual Items to PayPal (cmd=_cart && upload=1)
        $this->addData('cmd', "_cart");
        $this->addData('upload', "1");
        $this->addData('business', Settings::get('paypal_mail'));
        $this->addData('currency_code', $shoppingCart->currencyCode);
        $this->addData('address_override', "0");
        $this->addData('no_shipping', "1");
        $this->addData('shipping', "0.00");
        $this->addData('bn', "Docebo_SP");
        $this->addData('charset', "utf-8");

        // Unique *seller-side* Id of the transaction (invoice)
        $this->addData('invoice', $transaction_id);

        // Enumerate cart items and add data for each item
        // @todo Here editions and classrooms should be handled (if any) using
        // shopping cart class attributes _editions and _classrooms
        $i = 1;
        foreach ( $shoppingCart->getPositions() as $position ) {
            //NOTE: item name and number are limited at 127 characters max
            $_name = $position->getPositionName();
            $_number = $position->getQuantity();
            if (strlen($_name) > 127) { $_name = substr($_name, 0, 127); }
            if (strlen($_number) > 127) { $_number = substr($_number, 0, 127); }
            $this->addData("item_name_$i", $_name);
            $this->addData("item_number_$i", $_number);
            $this->addData("amount_$i", number_format($position->getSumPrice(), 2));
            $i++;
        }

        $transactionModel = EcommerceTransaction::model()->with('ecommerceCoupon')->findByPk($transaction_id);
        if (floatval($transactionModel->discount) != 0) {
            // must be a positive amount
            $this->addData("discount_amount_cart", floatval($transactionModel->discount));
        }


        $this->addData('email', $u_info->email);
        $this->addData('first_name', $u_info->firstname);
        $this->addData('last_name', $u_info->lastname);
        $this->addData('address1', $billingInfo['address1']);
        $this->addData('address2', $billingInfo['address2']);
        $this->addData('country', $billingInfo['country']);
        $this->addData('city', $billingInfo['city']);


        // URLs
        $this->addData('notify_url', $this->ipnUrl);
        $this->addData('cancel_return', $this->cancelUrl);
        $this->addData('return', $this->returnUrl);

        // Pass-through:
        // Here we can add some custom information, not disclosed to the user
        // which will be sent back from Paypal in its IPN call
        $this->addData('custom', '');


        $this->logData(true);
            
    }
    
    
    /**
     * Validate PayPal IPN call
     * NOTE: some official documentation at: https://developer.paypal.com/docs/classic/ipn/gs_IPN/
     * 
     * @see Payment::validateTransaction()
     */
    public function validateTransaction() {
        
        // Prepare Request string
        $requestArr = array();
        if ( isset($_POST) ) {
            foreach ( $_POST as $key=>$value ) {
                $this->ipnData["$key"] = $value;
                $value = urlencode(stripslashes($value));
                $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}', $value);// IPN fix
                $requestArr[] = $key . "=" . $value; 
            }                 
        }
        
        
        $requestArr[] = "cmd=_notify-validate";
        
        $requestStr = "";
        if ( count($requestArr) > 0 ) {
            $requestStr = implode("&", $requestArr);
        }
        
        // Parse Gateway URL
        $url_parsed=parse_url($this->gatewayUrl);
        
        // Request Header 
        $header = '';
        $header .= "POST /cgi-bin/webscr HTTP/1.1\r\n";
        $header .= "Host: " . $url_parsed['host'] . "\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($requestStr) . "\r\n";
        $header .= "Connection: close\r\n\r\n";
        
        // Connect to PayPal
        $fp = fsockopen('ssl://'.$url_parsed['host'], 443, $errno, $errstr, 30);
        
        // Check if connection is successful
        if ( !$fp ) {
            Yii::log("Could not connect to PayPal (fsockopen), $errno: $errstr", 'error', __CLASS__);
            $this->logIpnData(true);
            Yii::app()->end();
        }
        
        // Initiate request to PayPal
        Yii::log("Sending Validate request to PayPal: $requestStr", 'debug', __CLASS__);
        fputs($fp, $header . $requestStr);

        $success = false;
        
        // Loop
        while( !feof($fp) )
        {
            $res = fgets($fp, 1024);
            $this->ipnResponse .= $res;
            if ( stripos($res, "VERIFIED" ) !== false ) {
                $success = true;
                break;
            } else if ( stripos($res, "INVALID" ) !== false ) {
                //log invalid result
                Yii::log("PayPal IPN response is 'INVALID' (request string: '".$requestStr."')", CLogger::LEVEL_ERROR, __CLASS__);
            }
        }

        // Add Validation response into IPN data
        $this->ipnData["ipnResponse"] = $this->ipnResponse;
        
        fclose($fp); // close connection        
        
        // LOG
        $this->ipnData["ipnValidationSuccess"] = $success ? "TRUE" : "FALSE";
        $this->logIpnData(true);
        
        // Return to caller, all IPN data are in $this->ipnData
        return $success;
                   
    }
    
    
}