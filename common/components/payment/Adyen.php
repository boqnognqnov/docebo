<?php
/**
 * Authorize.net payment gateway
 *
 * @author Ivelin Dimitrov
 *
 */
class Adyen extends Payment{

	CONST REFUSED = 'REFUSED';
	CONST CANCELLED = 'CANCELLED';
	CONST PENDING = 'PENDING';
	CONST ERROR = 'ERROR';

	public static $KoMessages = array(
		self::REFUSED => '_OPERATION_ADYEN_REFUSED',
		self::CANCELLED => '_OPERATION_ADYEN_CANCELLED',
		self::PENDING => '_OPERATION_ADYEN_PENDING',
		self::ERROR => '_OPERATION_ADYEN_ERROR',
	);

	public static $currencyCodes = array(
		'GHC' => 0,
		'GNF' => 0,
		'IDR' => 0,
		'JPY' => 0,
		'KMF' => 0,
		'KRW' => 0,
		'RWF' => 0,
		'UGX' => 0,
		'VND' => 0,
		'VUV' => 0,
		'XAF' => 0,
		'XOF' => 0,
		'XPF' => 0,
		'BYR' => 0,
		'CVE' => 0,
		'DJF' => 0,
		'PYG' => 0,
		'MRO' => 1,
		'JOD' => 3,
		'KWD' => 3,
		'TND' => 3,
		'BHD' => 3,
		'LYD' => 3,
		'OMR' => 3,
	);
	/**
	 * Constructor
	 *
	 * @param boolean $mode Test mode (true/false)
	 */
	public function __construct($mode) {

		parent::__construct($mode);

		// Collect REQUEST data into a class attribute
		if ( isset($_REQUEST) ) {
			foreach ( $_REQUEST as $key=>$value ) {
				$this->ipnData["$key"] = $value;
			}
		}
		$this->logIpnData(true);


	}

	/**
	 * Set test mode on or off
	 *
	 * @param boolean $mode
	 */
	public function setTestMode($mode)
	{
		$this->testMode = $mode;

		//Set gateway url
		if ( $this->testMode )
			$this->gatewayUrl = 'https://test.adyen.com/hpp/';
		else
			$this->gatewayUrl = 'https://live.adyen.com/hpp/';

		// Change url, depending on the HPP option
		if((int) Settings::get('adyen_single_page_hpp'))
			$this->gatewayUrl .= 'pay.shtml';
		else
			$this->gatewayUrl .= 'select.shtml';
	}

	/**
	 * Sets payment data using the passed parameters
	 *
	 * @param int $idUser User Id
	 * @param  EShoppingCart $shoppingCart
	 * @param int $transaction_id Transaction Id (pre-created)
	 */
	public function preparePayment($idUser, $shoppingCart, $transaction_id)
	{
		// First, get billing info data
		$b_info = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
		// Get user information from database
		$u_info = CoreUser::model()->findByPk($idUser);

		$this->addData('merchantSig', '');// TODO: calculate signature
		$this->addData('sessionValidity', date("c",strtotime("+1 days")));
		$this->addData('shipBeforeDate', date('Y-m-d'));

		// Manage User Locale
		preg_match('#\w{2}-\w{2}#' , $_SERVER['HTTP_ACCEPT_LANGUAGE'] , $matches);
		if($matches){
			$locale = $matches[0];
		}else{
			$locale = Yii::app()->getLanguage();
		}
		
		$this->addData('shopperLocale', $locale);// user locale en_EN,bg_BG,it_IT
		$this->addData('merchantAccount', Settings::get('adyen_merchant_account'));

		// Manage amount
		// Enumerate cart items and add data for each item
		// shopping cart class attributes _editions and _classrooms
		$amount = 0;
		$description = array();
		foreach ( $shoppingCart->getPositions() as $position ) {
			$amount += ($position->getQuantity() * $position->getSumPrice());
			$description[] = $position->getPositionName();
		}

		$transactionModel = EcommerceTransaction::model()->with('ecommerceCoupon')->findByPk($transaction_id);
		$discount = floatval($transactionModel->discount);
		if ($discount != 0) {
			$amount = $amount - $discount;
		}

		$currencyCode = Settings::get("currency_code");
		$amount = $this->calculateAmountDependsOnCurrency($amount, $currencyCode);
		$this->addData('paymentAmount', $amount);
		$this->addData('currencyCode', $currencyCode);
		$this->addData('skinCode', Settings::get("adyen_skin_code"));
		//Refference
		$this->addData('merchantReference', 'LMS Course Purchase');

		$this->addData('shopperReference', $u_info->email);
		$this->addData('recurringContract', 'ONECLICK');
		$this->addData('shopperEmail', $u_info->email);
		$this->addData('merchantReturnData', $transaction_id);
		$this->addData('offset', 0);

		$this->calculateSignature();

		$this->logData(true);

	}

	private function calculateSignature()
	{
		$this->data['merchantSig'] = base64_encode(pack("H*",hash_hmac(
			'sha1',
			$this->data['paymentAmount'] . $this->data['currencyCode'] .
			$this->data['shipBeforeDate'] . $this->data['merchantReference'] .
			$this->data['skinCode'] . $this->data['merchantAccount'] .
			$this->data['sessionValidity'] . $this->data['shopperEmail'] .
			$this->data['shopperReference'] . $this->data['recurringContract'] .
			'' . '' . '' . $this->data['merchantReturnData'] .
			'' . '' . '' .
			0
			,Settings::get('adyen_hmac_key')
		)));
	}


	/**
	 * Validate incoming IPN notification / Authorize.net API response.
	 *
	 * Internally using HTTP REQUEST data
	 */
	public function validateTransaction()
	{
		$incomingSignature = Yii::app()->request->getParam('merchantSig');
		$ourSignature = base64_encode(pack("H*",hash_hmac(
			'sha1',
			Yii::app()->request->getParam('authResult', '') .
			Yii::app()->request->getParam('pspReference', '') .
			Yii::app()->request->getParam('merchantReference', '') .
			Yii::app()->request->getParam('skinCode', '') .
			Yii::app()->request->getParam('merchantReturnData')
			,Settings::get('adyen_hmac_key')
		)));
		return $incomingSignature === $ourSignature;
	}

	public function calculateAmountDependsOnCurrency($amount, $currencyCode)
	{
		$exponent = 2;// the exponent by default is 2
		if(isset(self::$currencyCodes[strtoupper($currencyCode)]))
			$exponent = self::$currencyCodes[strtoupper($currencyCode)];

		return $amount * pow(10,$exponent);
	}
}