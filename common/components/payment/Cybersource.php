<?php
/**
 * Cybersource payment gateway
 *
 * @author Asen Nikolov
 *
 */
class Cybersource extends Payment {

	const  HMAC_SHA256 = 'sha256';

	/**
	 * Constructor
	 *
	 * @param boolean $mode Test mode (true/false)
	 */
	public function __construct($mode) {

		parent::__construct($mode);

		$oldStripRelease = Yii::app()->urlManager->getStripRelease();
		Yii::app()->urlManager->setStripRelease(false);

		// Set self-describing attribute
		$this->payment_method = parent::$PAYMENT_METHOD_CYBERSOURCE;

		// URLs
		$cybersource_notify_url = Docebo::createAbsoluteLmsUrl("cart/ipnCybersource");

		$this->ipnUrl = $cybersource_notify_url;

		// These two URL's will be changed on the fly later adding Transaction Id.
		$this->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled");
		$this->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished");

		Yii::app()->urlManager->setStripRelease($oldStripRelease);

	}

	/**
	 * Set payment mode: test or live.
	 *
	 * @see Payment::setTestMode()
	 */
	public function setTestMode($mode) {

		$this->testMode = $mode;

		if ($this->testMode) {
			$this->gatewayUrl = 'https://testsecureacceptance.cybersource.com/pay';//'https://testsecureacceptance.cybersource.com/pay';
		} else {
			$this->gatewayUrl = 'https://secureacceptance.cybersource.com/pay';
		}
	}

	/**
	 * Fill class attribute "data" with all required payment gateway specific data fields.
	 *
	 * @see Payment::setPaymentData()
	 */
	public function preparePayment($idUser, $shoppingCart, $transaction_id) {

		$billingInfo = array();

		// First, get billing info data
		$b_info = CoreUserBilling::model()->getUserLastBillingInfo($idUser);
		$billingInfo['address1'] = $b_info->bill_address1;
		$billingInfo['address2'] = $b_info->bill_address2;
		$billingInfo['country']  = $b_info->bill_country_code;
		$billingInfo['city']     = $b_info->bill_city;

		// Get user information from database
		$u_info = CoreUser::model()->findByPk($idUser);

		// Using method 2: Passing Individual Items to PayPal (cmd=_cart && upload=1)
		$this->addData('access_key', Settings::get('cybersource_access_key'));
		$this->addData('profile_id', Settings::get('cybersource_profile_id'));
		$this->addData('transaction_uuid', uniqid());
		$signedFields = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_email,bill_to_forename,bill_to_surname,override_custom_receipt_page,override_custom_cancel_page,ignore_avs';
		$this->addData('signed_field_names', $signedFields);
		$this->addData('unsigned_field_names', '');
		$this->addData('signed_date_time', gmdate("Y-m-d\TH:i:s\Z"));
		$this->addData('locale', Yii::app()->getLanguage());
		$date = new DateTime();

		$this->addData('transaction_type', 'sale');
//		$this->addData('reference_number', $date->getTimestamp()*1000);

		$amount = 0;
		foreach ( $shoppingCart->getPositions() as $position ) {
			$amount += floatval($position->getSumPrice());
		}

		$discount_amount_cart = 0;
		$transactionModel = EcommerceTransaction::model()->with('ecommerceCoupon')->findByPk($transaction_id);
		if (floatval($transactionModel->discount) > 0) {
			// must be a positive amount
			$discount_amount_cart = floatval($transactionModel->discount);
		}

		$this->addData('amount', round($amount - $discount_amount_cart, 2));
		$this->addData('currency', Settings::get('currency_code'));

		$this->addData('bill_to_address_city', $b_info->bill_city);
		$this->addData('bill_to_address_country', $b_info->bill_country_code);
		$this->addData('bill_to_address_line1', $b_info->bill_address1);
		$this->addData('bill_to_email', $u_info->email);
		$this->addData('bill_to_forename', $u_info->firstname);
		$this->addData('bill_to_surname', $u_info->lastname);
		$receiptUrl = Yii::app()->getBaseUrl(true).'/index.php?r=cart/validateCybersource';
		$cancelUrl = Yii::app()->getBaseUrl(true).'/index.php?r=cart/declineCyberSource';

		$this->addData('override_custom_receipt_page', $receiptUrl);
		$this->addData('override_custom_cancel_page', $cancelUrl);
		$this->addData('ignore_avs', 'true');

		// save our id, we will use this in the responce from the Cybersource
		$this->addData('reference_number', $transaction_id);

		$this->addData('signature', $this->sign($this->data));

		$this->logData(true);
	}

	/**
	 * Validate Cybersource IPN call
	 *
	 * @see Payment::validateTransaction()
	 */
	public function validateTransaction() {
		$params = array();
		foreach($_POST as $name => $value) {
			$params[$name] = $value;
		}
		return (strcmp($params["signature"], $this->sign($params)) == 0) && $_POST['decision'] === 'ACCEPT';
	}

	public function validateCancelTransaction() {
		$params = array();
		foreach($_POST as $name => $value) {
			$params[$name] = $value;
		}
		return (strcmp($params["signature"], $this->sign($params)) == 0) && $_POST['decision'] === 'CANCEL';
	}

	private function sign ($params) {
		return $this->signData($this->buildDataToSign($params), Settings::get('cybersource_secret_key'));
	}

	private function signData($data, $secretKey) {
		return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
	}

	private function buildDataToSign($params) {
		$signedFieldNames = explode(",",$params["signed_field_names"]);
		foreach ($signedFieldNames as $field) {
			$dataToSign[] = $field . "=" . $params[$field];
		}
		return $this->commaSeparate($dataToSign);
	}

	private function commaSeparate ($dataToSign) {
		return implode(",",$dataToSign);
	}
}