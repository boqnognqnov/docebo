<?php
/**
 * Adaptive Paypal payment gateway
 *
 * @author Raffaele Quitadamo
 *
 */
class AdaptivePaypal extends Payment {

    /* API Credentials */
    protected $api_username;
    protected $api_password;
    protected $api_signature;

    /* Paypal Application ID */
    protected $application_id;

    /* Pay Key return after PAY action by Paypal */
    protected $payKey;

    /**
     * Constructor
     *
     * @param boolean $mode Test mode (true/false)
     */
    public function __construct($mode) {
        parent::__construct($mode);

        $oldStripRelease = Yii::app()->urlManager->getStripRelease();
        Yii::app()->urlManager->setStripRelease(false);

        // Set self-describing attribute
        $this->payment_method = self::$PAYMENT_METHOD_PAYPAL_ADAPTIVE;

        // These two URL's will be changed on the fly later adding Transaction Id.
        $this->cancelUrl = Docebo::createAbsoluteLmsUrl("cart/cancelled");
        $this->returnUrl = Docebo::createAbsoluteLmsUrl("cart/finished");

        Yii::app()->urlManager->setStripRelease($oldStripRelease);
    }

    /**
     * Set payment mode: test or live.
     *
     * @see Payment::setTestMode()
     */
    public function setTestMode($mode) {
        $this->testMode = $mode;

        // Set gateway URL
        if ($this->testMode)
            $this->gatewayUrl = 'https://svcs.sandbox.paypal.com/AdaptivePayments/';
        else
            $this->gatewayUrl = 'https://svcs.paypal.com/AdaptivePayments/';

        // Retrieve API username, password and signature
        $this->api_username = Settings::get('paypal_adaptive_username', '');
        $this->api_password = Settings::get('paypal_adaptive_password', '');
        $this->api_signature = Settings::get('paypal_adaptive_signature', '');

        // Retrieve Paypal application ID
        $this->application_id = Settings::get('paypal_application_id', '');
    }

    /**
     * Sends Pay request to Paypal
     */
    public function preparePayment($idUser, $shoppingCart, $transaction_id) {

		$transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);
		if($transactionModel) {
			if((round($transactionModel->totalPrice - $transactionModel->discount, 2)) <= 0)
				return true;
		}

        // Set up IPN URL
        $paypal_ipn_baseurl = Settings::get('paypal_ipn_baseurl','');
        if (!empty($paypal_ipn_baseurl))
            $paypal_notify_url = $paypal_ipn_baseurl . "/lms/?r=cart/ipnPaypalAdaptive&id_trans=".$transaction_id;
        else
            $paypal_notify_url = Docebo::createAbsoluteLmsUrl("cart/ipnPaypalAdaptive", array("id_trans" => $transaction_id));

        $this->ipnUrl = $paypal_notify_url;

        // required fields
        $nvpstr = "actionType=" . urlencode("PAY") . "&currencyCode=";
        $nvpstr .= urlencode($shoppingCart->currencyCode) . "&returnUrl=";
        $nvpstr .= urlencode($this->returnUrl) . "&cancelUrl=" . urlencode($this->cancelUrl);
        $nvpstr .= "&ipnNotificationUrl=" . urlencode($this->ipnUrl);
		$nvpstr .= "&memo=".urlencode("Docebo Transaction Id: ".$transaction_id);

        // Collect parallel payment receivers
        $receivers = $this->collectReceivers($shoppingCart);
        foreach($receivers as $index => $receiver) {
            $nvpstr .= "&receiverList.receiver(" . $index . ").amount=" . urlencode($receiver['amount']);
            $nvpstr .= "&receiverList.receiver(" . $index . ").email=" . urlencode($receiver['email']);

        }

        /* Make the Pay call to PayPal */
        $resArray = $this->hash_call("Pay", $nvpstr);

        // Check the response
        if(isset($resArray['responseEnvelope.ack']) && (strtoupper($resArray['responseEnvelope.ack']) == 'SUCCESS')) {
            $this->payKey = urldecode($resArray['payKey']);
            if($this->payKey){
                // Save the unique "pay key" as remote transaction ID
                // because this key will later be posted back during IPN
                // and we will use it to find the transaction model then
                $transactionModel = EcommerceTransaction::model()->findByPk($transaction_id);
                if($transactionModel){
                    $transactionModel->payment_txn_id = $this->payKey;
                    $transactionModel->save();
                }
            }
        } else
            throw new Exception("Cannot start payment");

    }

    public function getSubmitForm() {
        if ($this->testMode)
            $paypalRedirectUrl = "https://www.sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=".$this->payKey;
        else
            $paypalRedirectUrl = "https://www.paypal.com/webscr?cmd=_ap-payment&paykey=".$this->payKey;

        $html = "";
        $html .= CHtml::beginForm($paypalRedirectUrl, 'post', array('target'=>'_top', 'id'=>'payment_redirect_form'));
        $html .= CHtml::endForm();
        return $html;
    }

    private function collectReceivers($shoppingCart) {
        $receivers = null;
        Yii::app()->event->raise('GetPaypalPaymentReceivers', new DEvent($this, array(
            'shoppingCart' => $shoppingCart,
            'receivers' => &$receivers
        )));

        if(!$receivers) {
            $totalAmount = 0.00;
            foreach ( $shoppingCart->getPositions() as $position ) {
                $totalAmount += $position->getSumPrice();
            }

            $receivers[0] = array(
                'email' => Settings::get('paypal_mail'),
                'amount' => $totalAmount
            );
        }

        return $receivers;
    }

    private function hash_call($methodName, $nvpStr)
    {
        $ch = curl_init();

        // Setup up request header
        curl_setopt($ch, CURLOPT_URL, $this->gatewayUrl.$methodName);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        // Turning off the server and peer verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // Set the HTTP Headers
        curl_setopt($ch, CURLOPT_HTTPHEADER,  array(
            'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: NV',
            'X-PAYPAL-SECURITY-USERID: ' . $this->api_username,
            'X-PAYPAL-SECURITY-PASSWORD: ' .$this->api_password,
            'X-PAYPAL-SECURITY-SIGNATURE: ' . $this->api_signature,
            'X-PAYPAL-APPLICATION-ID: ' . $this->application_id
        ));

        // RequestEnvelope fields
        $errorLanguage = urlencode("en_US");

        // NVPRequest for submitting to server
        $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&$nvpStr";

        // Setting the nvpreq as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        //getting response from server
        $response = curl_exec($ch);

        // Converting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);

        if (curl_errno($ch))
            throw new Exception(curl_error($ch));
        else {
            //closing the curl
            curl_close($ch);
        }

        return $nvpResArray;
    }

    private function deformatNVP($nvpstr)
    {
        $intial=0;
        $nvpArray = array();

        while(strlen($nvpstr))
        {
            //postion of Key
            $keypos= strpos($nvpstr,'=');
            //position of value
            $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

            /*getting the Key and Value values and storing in a Associative Array*/
            $keyval=substr($nvpstr,$intial,$keypos);
            $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
            //decoding the respose
            $nvpArray[urldecode($keyval)] =urldecode( $valval);
            $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
        }
        return $nvpArray;
    }

    public function validateTransaction() {
        // Prepare Request string
        if ( isset($_POST) ) {
            foreach ( $_POST as $key=>$value )
                $this->ipnData["$key"] = $value;
        }

        $requestStr = 'cmd=_notify-validate&'.file_get_contents("php://input");

        // Parse Gateway URL
        if ($this->testMode)
            $ipnGatewayUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        else
            $ipnGatewayUrl = 'https://www.paypal.com/cgi-bin/webscr';

        $url_parsed = parse_url($ipnGatewayUrl);

        // Request Header
        $header = '';
        $header .= "POST /cgi-bin/webscr HTTP/1.1\r\n";
        $header .= "Host: " . $url_parsed['host'] . "\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . strlen($requestStr) . "\r\n";
        $header .= "Connection: close\r\n\r\n";

        // Connect to PayPal
        $fp = fsockopen('ssl://'.$url_parsed['host'], 443, $errno, $errstr, 30);

        // Check if connection is successful
        if ( !$fp ) {
            Yii::log("Could not connect to PayPal (fsockopen), $errno: $errstr", 'error', __CLASS__);
            $this->logIpnData(true);
            Yii::app()->end();
        }

        // Initiate request to PayPal
        Yii::log("Sending Validate request to PayPal: $requestStr", 'debug', __CLASS__);
        fputs($fp, $header . $requestStr);

        $success = false;

        // Loop
        while( !feof($fp) )
        {
            $res = fgets($fp, 1024);
            $this->ipnResponse .= $res;
            if ( stripos($res, "VERIFIED" ) !== false ) {
                $success = true;
                break;
            }
        }

        // Add Validation response into IPN data
        $this->ipnData["ipnResponse"] = $this->ipnResponse;

        fclose($fp); // close connection

        // LOG
        $this->ipnData["ipnValidationSuccess"] = $success ? "TRUE" : "FALSE";
        $this->logIpnData(true);

        if($success) {
            // Setup "fake" IPN data (not present in Adaptive Payments)
            $id_trans = Yii::app()->request->getParam('id_trans');
            $transactionModel = EcommerceTransaction::model()->findByPk($id_trans);
            $transactionTotalAmount = EcommerceTransactionInfo::getTransactionTotal($id_trans);

            $this->ipnData["payment_status"] = (strtoupper($this->ipnData['status']) == 'COMPLETED') ? "Completed" : $this->ipnData['status'];
            $this->ipnData['invoice'] = $id_trans;
            $this->ipnData['receiver_email'] = Settings::get('paypal_mail', '');
            $this->ipnData["mc_currency"] = $transactionModel->payment_currency;
            $this->ipnData["txn_id"] = null;
            $this->ipnData["receipt_id"] = null;
            $this->ipnData['mc_gross'] = $transactionTotalAmount;
        }

        // Return to caller, all IPN data are in $this->ipnData
        return $success;
    }
}
