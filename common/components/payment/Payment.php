<?php
/**
 * Base class for all payment gateways, e.g. Paypal, Wirecard, etc.
 *
 * @author Plamen Petkov
 *
 */
abstract class Payment  {

	/**
	 * Category and Level for PAYMENT PROCESS LOGGING!!
	 * Note: Do not use it for other logging, but only for Payment Process itself, i.e. logData() and logIpnData()
	 * @var string
	 */
	const LOG_CATEGORY 	= 'payment';   // You might be interested in MongoDB log router, re: special treatment for this category
	const LOG_LEVEL 	= 'debug';
	
    public static $PAYMENT_METHOD_PAYPAL = 'paypal';
    public static $PAYMENT_METHOD_PAYPAL_ADAPTIVE = 'paypal_adaptive';
    public static $PAYMENT_METHOD_PAYFLOW = 'paypal_payflow';
    // Authorize.net
    public static $PAYMENT_METHOD_AUTHORIZE = 'authorize';
    // Adyen
    public static $PAYMENT_METHOD_ADYEN = 'adyen';
    // Stripe
    public static $PAYMENT_METHOD_STRIPE = 'stripe';
    // CyberSource
    public static $PAYMENT_METHOD_CYBERSOURCE = 'cybersource';

    /**
     * @param $name
     * @return bool
     */
    public static function isMethodEnabled($name) {
        switch ($name) {
            case self::$PAYMENT_METHOD_PAYPAL:
                return Settings::get('paypal_enabled') === 'on';
                break;
            case self::$PAYMENT_METHOD_AUTHORIZE:
                return Settings::get('authorize_enabled') === 'on';
                break;
            case self::$PAYMENT_METHOD_PAYFLOW:
                return Settings::get('payflow_enabled') === 'on';
                break;
			case self::$PAYMENT_METHOD_PAYPAL_ADAPTIVE:
				return Settings::get('paypal_adaptive') === 'on';
				break;
            case self::$PAYMENT_METHOD_ADYEN:
                return Settings::get('adyen_enabled') === 'on';
                break;
            case self::$PAYMENT_METHOD_STRIPE:
                return Settings::get('stripe_enabled') == 1;
                break;
            case self::$PAYMENT_METHOD_CYBERSOURCE:
                return Settings::get('cybersource_enabled') == 1;
                break;
        }
        return false;
    }

    /**
     *
     * @var unknown
     */
    public $lastError;


    /**
     *
     * @var unknown
     */
    public $ipnResponse;


    /**
     * Self-describe
     * @var string
     */
    public $payment_method;

    /**
     * Test mode true/false
     *
     * @var boolean
     */
    public $testMode;

    /**
     * URL of the payment processor, e.g. 'https://www.sandbox.paypal.com/cgi-bin/webscr'
     *
     * @var string
     */
    public $gatewayUrl;

    /**
     * URL used by payment gateway for payment notifications
     * @var string
     */
    public $ipnUrl;


    public $cancelUrl;
    public $returnUrl;

    /**
     * Associative array of fields to send TO the gateway
     *
     * @var array
     */
    public $data = array();

    /**
     * To log or Not to log
     * @var boolean
     */
    public $doLog = false;

    /**
     * Name of the log file, if used (written in <root>/files/tmp)
     *
     * @var string
     */
    public $logFile = "payment.log";

    /**
     * Relative path to folder to write the log file
     *
     * @var string
     */
    protected $logRelativePath = "files/tmp";


    /**
     * Data collected FROM gateway during the IPN call
     *
     * @var array
     */
    public $ipnData = array();


    /**
     * Constructor
     *
     * @param boolean $mode Test mode (true/false)
     */
    public function __construct($mode=true) {
        $this->testMode = $mode;
        $this->setTestMode($this->testMode);
        $this->doLog = false;
    }


    /**
     * Add data item to OUTPUT data
     *
     * @param string $field Field name
     * @param string $value Field Value
     */
    public function addData($field, $value) {
        $this->data["$field"] = $value;
    }


    /**
     * Remove data item from OUTPUT data array
     *
     * @param string $field Field name
     */
    public function removeData($field) {
        unset($this->data["$field"]);
    }

    /**
     * Update or Add data item to OUTPUT data
     *
     * @param string $field Field name
     * @param string $value Field Value
     */
    public function updateData($field, $value) {
        if ( array_key_exists($field, $this->data) ) {
            $this->data["$field"] = $value;
        }
        else {
            $this->addData($field, $value);
        }
    }




    /**
     * Log $var to standard Yii log or to a file
     *
     * Log file (if used) is written in <root>/files/tmp
     *
     * @param mixed $var
     * @param string|boolean $toFile
     */
    public function log($var, $toFile=false ) { 
    
    	if (!$this->doLog) {
    		return;
    	}
    
    	$timestamp = '[' . date('m/d/Y g:i A') .']' . "\n";
    
    	$out = "";
    
    	if ( is_array($var) ) {
    		foreach ( $var as $key => $value ) {
    			$out .= print_r("$key = " . print_r($value, true), true) . "\n";
    		}
    	}
    	else {
    		$out .= print_r($var, true);
    	}
    
   		$out = "\n" . $out;
        Yii::log($out, self::LOG_LEVEL, self::LOG_CATEGORY);
   		    
    }
    
    
    /**
     * Log OUTPUT data (data sent TO payment gateway)
     *
     * @param string|boolen $toFile (Obsoleted, not used)
     */
    public function logData($toFile = false) {
    	$logTitle = array('Outgoing_Payment' => '[' . $this->payment_method . ']');
        $tmp = array_merge($logTitle, $this->data);
        $this->log($tmp, $toFile);
    }


    /**
     * Log raw data received FROM payment gateway during the IPN call
     *
     * @param string|boolen $toFile (Obsoleted, not used)
     */
    public function logIpnData($toFile = false) {
    	$logTitle = array('Incoming_IPN' => '[' . $this->payment_method . ']');
        $tmp = array_merge($logTitle, $this->ipnData);
        $this->log($tmp, $toFile);
    }

    /**
     * Returns current Test Mode
     *
     * @return boolean
     */
    public function getTestMode() {
        return $this->testMode;
    }


    public function getSubmitForm() {
        $html = "";
        $html .= CHtml::beginForm($this->gatewayUrl, 'post', array('target'=>'_top', 'id'=>'payment_redirect_form'));

        foreach ( $this->data as $field => $value ) {
            $html .= CHtml::hiddenField($field, $value);
        }

        $html .= CHtml::endForm();
        return $html;
    }


    /**
     * Set test mode on or off
     *
     * @param boolean $mode
     */
    abstract public function setTestMode($mode);

    /**
     * Sets payment data using the passed parameters
     *
     * @param int $idUser  User Id
     * @param  EShoppingCart $shoppingCart
     * @param int $transaction_id Transaction Id (pre-created)
     */
    abstract public function preparePayment($idUser, $shoppingCart, $transaction_id);


    /**
     * Validate incoming IPN notification / Authorize.net API response.
     *
     * Internally using HTTP REQUEST data
     */
    abstract protected function validateTransaction();

}