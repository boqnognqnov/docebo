<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class EventManager extends CComponent
{
    
	const EVENT_NEW_GROUP_USER                             = 'DEVENT_NewGroupUser';
	
	const EVENT_USER_SUBSCRIBED_GROUP                      = 'UserSubscribedGroup';

	const EVENT_USER_REMOVED_FROM_GROUP                    = 'UserRemovedFromGroup';

	const EVENT_NEW_BRANCH_USER                            = 'DEVENT_NewBranchUser';

	/** When a lot of users are being imported at once */
	const EVENT_MASS_IMPORT_BRANCH_USERS                   = 'DEVENT_NewMassImportBranchUsers';
	
	/** When user is subscribed to a course */
	const EVENT_USER_SUBSCRIBED_COURSE                     = 'UserSubscribedCourse';
	
	/** When user is UNsubscribed from a course */
	const EVENT_USER_UNSUBSCRIBED                          = 'UserUnsubscribed';
	
	/** When user accesses a course for the first time */
	const EVENT_USER_FIRST_ACCESS_IN_COURSE                = 'UserFirstAccessInCourse';
	
	/** When a student completed a course */
	const EVENT_STUDENT_COMPLETED_COURSE                   = 'StudentCompletedCourse';
	
	/** When a student completed a LO in a course  */
	const EVENT_STUDENT_COMPLETED_LO                       = 'StudentCompletedLO';

	/** When a student completed a webinar session */
	const EVENT_STUDENT_COMPLETED_WEBINAR_SESSION          = 'StudentCompletedWebinarSession';

	/** When a student completed an ILT session */
	const EVENT_STUDENT_COMPLETED_ILT_SESSION              = 'StudentCompletedILTSession';
	
	/** When a student is assigned to a learning plan */
	const EVENT_USER_ENROLLED_IN_LEARNING_PLAN             = 'UserEnrolledInLearningPlan';

	/**  When a learning plan is updated */
	const EVENT_LEARNING_PLAN_UPDATED                      = 'LearningPlanUpdated';
	
	/** When student is unenrolled from a LP */
	const EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN         = 'UserUnenrolledFromLearningPlan';
	
	/** When a student is enrolled into a classroom session */
	const EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION		   = 'ILTSessionUserEnrolled';
	
	/** When a student is UN-enrolled from a classroom session */
	const EVENT_USER_UNENROLLED_FROM_CLASSROOM_SESSION	   = 'ILTSessionUserUnenrolled';
	
	/** When a student is enrolled into a webinar session */
	const EVENT_USER_ENROLLED_IN_WEBINAR_SESSION		   = 'UserEnrolledInWebinarSession';

	/** When a student is enrolled into a webinar session */
	const EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION	   = 'UserUnenrolledFromWebinarSession';
	
	/** When a student completes a Learning Plan */
	const EVENT_STUDENT_COMPLETED_LEARNING_PLAN            = 'StudentCompletedLearningPlan';
	
	const EVENT_BEFORE_CREATE_ORGCHART_TREE                = 'BeforeCreateOrgchartTree';
	
	/** When Classroom Session enrollment status is set to "Subscribed" */
	const EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED    = 'UserClassroomSessionEnrollActivated';

	/** When Classroom Session winting enrollment is declined */
	const EVENT_USER_CLASSROOM_SESSION_ENROLL_DECLINED    = 'UserClassroomSessionEnrollDeclined';

	/** When Classroom Session enrollment status is set to "Waiting" */
	const EVENT_USER_CLASSROOM_SESSION_WAITING_FOR_APPROVAL = 'UserClassroomSessionWaitingApproval';

	/** When Webinar Session enrollment status is set to "Subscribed" */
	const EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED      	= 'UserWebinarSessionEnrollActivated';

	/** When Webinar Session enrollment status is set to "Waiting" */
	const EVENT_USER_WEBINAR_SESSION_WAITING_FOR_APPROVAL   = 'UserWebinarSessionWaitingApproval';

	/** When Webinar Session winting enrollment is declined */
	const EVENT_USER_WEBINAR_SESSION_ENROLL_DECLINED     	= 'UserWebinarSessionEnrollDeclined';

	const EVENT_LEARNER_HAS_YET_TO_COMPLETE_A_COURSE        = 'LearningHasYetToCompleteACourse';

	const EVENT_ON_RENDER_USERMAN_USER_ACTIONS				= 'OnRenderUsermanUserActions';

	const EVENT_ON_PLUGIN_DEACTIVATE						= 'OnPluginDeactivate';

	const EVENT_ON_PLUGIN_ACTIVATE							= 'OnPluginActivate';
	
	const EVENT_ACTIVE_USER_LOGGED                          = 'ActiveUserLogged';

	const EVENT_NEW_COURSE                                  = 'NewCourse';

	const EVENT_COURSE_PROP_CHANGED                         = 'CoursePropChanged';

	const EVENT_ADDED_LEARNING_COURSEPATH                   = 'AddedLearningCoursepath';

	const EVENT_UPDATED_LEARNING_COURSEPATH                 = 'UpdatedLearningCoursepath';

	const EVENT_DELETED_COURSE_LO                           = 'DeletedCourseLo';

	const EVENT_COURSE_DELETED                              = 'CourseDeleted';

	const EVENT_LO_AFTER_SAVE								= 'LoAfterSave';

	const EVENT_LRO_AFTER_SAVE								= 'LroAfterSave';

	const EVENT_LEARNING_PLAN_AFTER_SAVE					= 'LearningPlanAfterSave';

	const EVENT_DELETED_LEARNING_COURSEPATH					= 'DeletedLearningCoursepath';

	const EVENT_USER_WINNED_CONTEST							= 'UserWinnedContest';


	const EVENT_APP7020_ASSET_CREATED                      = 'App7020AssetCreated';
	const EVENT_APP7020_QUESTION_CREATED                   = 'App7020QuestionCreated';
	const EVENT_APP7020_ANSWER_CREATED                     = 'App7020AnswerCreated';

	const EVENT_APP7020_ASSET_UPDATED                      = 'App7020AssetUpdated';
	const EVENT_APP7020_QUESTION_UPDATED                   = 'App7020QuestionUpdated';
	const EVENT_APP7020_ANSWER_UPDATED                     = 'App7020AnswerUpdated';

	const EVENT_APP7020_BEFORE_ASSET_SAVE                  = 'App7020BeforeAssetSave';
	const EVENT_APP7020_EXPERTS_ASSIGNED_TO_CHANNEL        = 'App7020ChannelAssignment';
	
	const EVENT_APP7020_CHANNEL_VISIBILITY_UPDATED         	= 'App7020ChannelVisibilityUpdated';
	const EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_UPDATED   = 'App7020ChannelBeforeVisibilityUpdated';
	const EVENT_APP7020_CHANNEL_VISIBILITY_DELETED         	= 'App7020ChannelVisibilityDeleted';
	const EVENT_APP7020_CHANNEL_BEFORE_VISIBILITY_DELETED   = 'App7020ChannelBeforeVisibilityDeleted';
	
	const EVENT_APP7020_CHANNEL_UPDATED   					= 'App7020ChannelUpdated';
	const EVENT_APP7020_CHANNEL_DELETED						= 'App7020ChannelDeleted';
	const EVENT_APP7020_CHANNEL_TOGGLE_SHOW_HIDE			= 'App7020ChannelToggleShowHide';
	
	
	const EVENT_APP7020_ASSET_DELETED                      = 'App7020AssetDeleted';
	const EVENT_APP7020_QUESTION_DELETED                   = 'App7020QuestionDeleted';
	const EVENT_APP7020_ANSWER_DELETED                     = 'App7020AnswerDeleted';
	const EVENT_APP7020_ASSET_UNPUBLISHED                  = 'App7020AssetUnpublished';
	
	const EVENT_MEMBER_ASSIGNED_TO_CATALOG                 = 'MemberAssignedToCatalog';
	const EVENT_MEMBER_UNASSIGNED_FROM_CATALOG             = 'MemberUnassignedFromCatalog';

	const EVENT_ENTRY_ASSIGNED_TO_CATALOG                  = 'EntryAssignedToCatalog';
	const EVENT_ENTRIES_ASSIGNED_TO_CATALOG                = 'EntriesAssignedToCatalog';
	const EVENT_ENTRY_UNASSIGNED_FROM_CATALOG              = 'EntryUnassignedFromCatalog';
	const EVENT_ENTRIES_UNASSIGNED_FROM_CATALOG            = 'EntriesUnassignedFromCatalog';

	const EVENT_NEW_COURSE_ADDED                           = 'NewCourseAdded';

	const EVENT_ON_AFTER_LOGIN                             = 'onAfterLogin';

	const EVENT_USER_ASSIGNED_TO_BRANCH                    = 'UserAssignedToBranch';
	const EVENT_USER_REMOVED_FROM_BRANCH                   = 'UserRemovedFromBranch';

	const EVENT_POWER_USER_OBJECTS_SELECTION_UPDATED       = "PowerUserObjectsSelectionUpdated";

	/** Will be raised when a plugin is automatically activated during the creation of a LMS trial platform */
	const EVENT_INSTALL_PLUGIN_FROM_INSTALLER              = 'InstallPluginFromInstaller';
	
	const EVENT_AFTER_NEW_COURSE_ADDITIONAL_FIELD_SAVED    = "AfterNewCourseAdditionalFieldSaved";
	const EVENT_AFTER_COURSE_ADDITIONAL_FIELD_DELETED      = "AfterCourseAdditionalFieldDeleted";
	
	protected static $observers = array();

	public static $preventDefaultArray = array();

	public function __construct() {

		$this->init();
	}


	/**
	 * Initialize the istance of the controller, this method will be called from the constructor
	 */
	public function init() {

	}

	/**
	 * Attach an event to a handler
	 * @param $event_name
	 * @param $handler
	 */
	public function on($event_name, $handler) {
		// Make event names case IN-sensitive
		self::$observers[$event_name][] = $handler;
	}


	/**
	 * Execute event handler(s)
	 * 
	 * @param $event_name
	 * @param DEvent $event
	 * @return bool|mixed
	 * @throws CException
	 */
	public function raise($event_name, DEvent $event) {

		$res = true;
		
		// Event names are case IN-sensitive
		$event->event_name = $event_name;

		if (isset(self::$observers[$event_name])) {
			
			foreach(self::$observers[$event_name] as $handler) {
				if (is_string($handler)) {
					$res = call_user_func($handler, $event);
				} else {
					if (is_callable($handler, true)) {
						if (is_array($handler)) {
							// an array: 0 - object, 1 - method name
							list($object, $method) = $handler;
							if (is_string($object)) // static method call
							{
								$res = call_user_func($handler, $event);
							} else {
								if (method_exists($object, $method)) {
									$res = $object->$method($event);
								} else {
									throw new CException('Event "' . get_class($this) . '.' . $event_name . '" is attached with an invalid handler "' . $handler[1] . '".');
								}
							}
						} else // PHP 5.3: anonymous function
						{
							$res = call_user_func($handler, $event);
						}
					} else {
						throw new CException('Event "' . get_class($this) . '.' . $event_name . '" is attached with an invalid handler "' . $handler . '".');
					}
				}


				if (is_null($res)) { // last event handler didn't returned anything
					if (!empty(EventManager::$preventDefaultArray[$event_name])) {
						$res = false; // do not continue after the event
						unset(EventManager::$preventDefaultArray[$event_name]);
					} else {
						$res = true; // true = continue after the event has been raised
					}
				}

				// stop further handling if param.handled is set true
				if (($event instanceof DEvent) && $event->handled) {
					return $res;
				}
			}
		}

		return $res;
	}

	/**
	 * Check if the event has attached handlers
	 * @param string $event_name
	 * @return bool
	 */
	public function hasHandler($event_name) {
		return isset(self::$observers[$event_name]);
	}

}