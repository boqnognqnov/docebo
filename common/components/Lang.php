<?php
/**
 * Helper component to load and handle lang-code, browser codes,
 * language description, etc.
 *
 * Upon construction loads all languages. Used in sngleton pattern.
 *
 * @TODO Core-Lang-Language table has 'browser' codes like: 'en;en-us'
 *  This is NOT browser code but Accept-Language-Header format.
 *  Browser code is strictly a single string.
 *
 *
 * @author Plamen Petkov
 *
 */

class Lang extends CComponent {

	/**
	 * Define the ultimate 'default' language; if anything else fails, use this language
	 * Like: No session language && No Language or language not found etc.
	 *
	 * @var string
	 */
	public static $LANGUAGE_FALLBACK = 'en';

	/**
	 * Static properties to hold already loaded languages
	 *
	 * @var array Assoc arrays, see code for keys
	 */
	private $__languages_active_only = array();
	private $__languages_all = array();

	/**
	 * We are dealing with so called lang-codes ('english', italian' ...) and
	 * browser codes ('en', 'it', 'en;en-us', ...)
	 * Lets have these two contstants to indicate the type
	 *
	 * @var string
	 */
	public static $LANGCODE = 'code';
	public static $BROWSERCODE = 'browsercode';


	/**
	 * Hold the already loaded component
	 * @var unknown
	 */
	private static $__component = null;

	/**
	 * NOTE: this should be deprecated and we should rely on database specified languages only
	 * ISO 639-1 => language name
	 * http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
	 * @var array
	 */
	private static $_lang_names = array(
		'ar' => 'arabic',
		'bs' => 'bosnian',
		'bg' => 'bulgarian',
		'hr' => 'croatian',
		'cs' => 'czech',
		'da' => 'danish',
		'nl' => 'dutch',
		'en' => 'english',
		'fa' => 'farsi',
		'fi' => 'finnish',
		'fr' => 'french',
		'de' => 'german',
		'el' => 'greek',
		'he' => 'hebrew',
		'hu' => 'hungarian',
		'id' => 'indonesian',
		'it' => 'italian',
		'ja' => 'japanese',
		'ko' => 'korean',
		'no' => 'norwegian',
		'pl' => 'polish',
		'pt' => 'portuguese',
		'pt-br' => 'portuguese-br',
		'ro' => 'romanian',
		'ru' => 'russian',
		'zh' => 'simplified_chinese',
		'es' => 'spanish',
		'es-mx' => 'spanish-mex',
		'tr-cn' => 'traditional_chinese', //old and wrong one, to be removed
		'zh-tw' => 'traditional_chinese',
		'sv' => 'swedish',
		'th' => 'thai',
		'tr' => 'turkish',
		'uk' => 'ukrainian',
	);


	/**
	 * Get an instance, create object if not yet created
	 * @param string $classname
	 * @return Language
	 */
	public static function getInstance($classname = __CLASS__) {
		if (self::$__component) {
			return self::$__component;
		}
		return self::$__component = new $classname();
	}


	/**
	 * Load languages into a raw structure
	 */
	private function loadLanguages() {

		$result_active = array();
		$result_all = array();
		$multidomainAppIsActive = false;

		// Array of models found
		static $models = array();
		if(empty($models))
			$models = CoreLangLanguage::model()->findAll();

		if ($models) {
			/*
			 * We must check if the MultidomainApp is Active
			 * but we can be sure that the MultidomainApp will be never used in ConsoleApplication
			 */
			if(!Yii::app() instanceof CConsoleApplication){
				$multidomainAppIsActive = (Yii::app()->hasModule('MultidomainApp') || Yii::app()->user->isGuest) && PluginManager::isPluginActive('MultidomainApp');
			}

			foreach($models as $m) {
				if (strpos($m->lang_browsercode, ';')) {
					$lang_browsercode = substr($m->lang_browsercode, 0, strpos($m->lang_browsercode, ';'));
				} else {
					$lang_browsercode = $m->lang_browsercode;
				}

				$element = array(
					self::$BROWSERCODE => $lang_browsercode,
					'description' => $m->lang_description,
					self::$LANGCODE => $m->lang_code,
					'direction' => $m->lang_direction,
				);
				$result_all[] = $element;
				if ((isset($m->lang_active) && $m->lang_active == 1) || $multidomainAppIsActive) {
					$useCustomRules = false;

					if($multidomainAppIsActive){

						$domain = CoreMultidomain::resolveClient();
						if($domain) {
							// get current settings from DB, not these which are in session
							$domain = CoreMultidomain::model()->findByPk($domain->id);
							if ($domain->use_custom_settings == 1) {
								$disabledLanguages = explode(',', $domain->disabled_languages);
								if (!empty($disabledLanguages)) {
									$useCustomRules = true;
									if (!in_array($element['code'], $disabledLanguages))
										$result_active[] = $element;
								}
							}
						}
					}
					if (!$useCustomRules && $m->lang_active == 1)
						$result_active[] = $element;
				}
			}
			$this->__languages_active_only = $result_active;
			$this->__languages_all = $result_all;
		} else {
			Yii::log('No languages have been loaded', 'debug', __CLASS__ . ':' . __LINE__);
		}


		return true;
	}


	/**
	 * Return name (description) of a language from the core tables
	 *
	 * @param string $code
	 * @param string $browsercode
	 * @return string
	 */
	private function getLanguageName($code = '', $browsercode = '') {
		foreach ($this->__languages_all as $language) {
			if (($language[self::$LANGCODE] == $code) || ($language[self::$BROWSERCODE] == $browsercode)) {
				return $language['description'];
			}
		}
	}


	/**
	 * Check if given code (lang-code OR browsercode) is among the list of our languages
	 *
	 * @param string $code  lang-code (english, italian,...) OR browser code (en, en;en-us, it, bg,...)
	 * @param string $type  'browsercode' OR 'code'
	 * @param string $active
	 * @return boolean
	 */
	private function isValidCode($code, $type, $active = true) {
		foreach(self::getInstance()->getLanguages($active) as $language) {
			if ($code == $language[$type]) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Load languages upon construction
	 *
	 */
	public function __construct() {
		$this->loadLanguages();
	}


	/**
	 * Return raw languages associative array
	 *
	 * @param boolean $active  True=active only, False=ALL
	 * @return array:
	 */
	public static function getLanguages($active = true) {
		if ($active) {
			return self::getInstance()->__languages_active_only;
		} else {
			return self::getInstance()->__languages_all;
		}
	}


	/**
	 * Get language name by its 'lang-code' (like 'english')
	 *
	 * @param string $code
	 * @return string
	 */
	public static function getNameByCode($code) {
		return self::getInstance()->getLanguageName($code);
	}


	/**
	 * Get ALL language names indexed by lang_code
	 *
	 * @param $active boolean
	 * @return array Assoc array of  '<lang-code>' => '<Languagename>'
	 */
	public static function getAllNamesByCode($active = true) {
		$languages = self::getInstance()->getLanguages($active);
		$result = array();
		foreach($languages as $language) {
			$result[$language[self::$LANGCODE]] = $language['description'];
		}
		return $result;
	}


	/**
	 * Get language name by browser code
	 * @param string $browsercode
	 */
	public static function getNameByBrowserCode($browsercode) {
		return self::getInstance()->getLanguageName('', $browsercode);
	}


	/**
	 * Get ALL language names indexed by Browser Code
	 *
	 * @param $active boolean
	 * @return array Assoc array of  '<browser-code>' => '<Languagename>'
	 */
	public static function getAllNamesByBrowserCode($active = true) {
		$languages = self::getInstance()->getLanguages($active);
		$result = array();
		foreach($languages as $language) {
			$result[$language[self::$BROWSERCODE]] = $language['description'];
		}
		return $result;
	}


	/**
	 * "Translate"  lang-code to browser code
	 *
	 * @param string $code
	 * @return string
	 */
	public static function getBrowserCodeByCode($code) {
		foreach(self::getInstance()->__languages_all as $language) {
			if ($language[self::$LANGCODE] == $code) {
				return $language[self::$BROWSERCODE];
			}
		}
		return '';
	}


	/**
	 * "Translate"  browser code to lang-code
	 *
	 * @param string $browsercode
	 * @return string
	 */
	public static function getCodeByBrowserCode($browsercode) {
		foreach(self::getInstance()->__languages_all as $language) {
			if ($language[self::$BROWSERCODE] == $browsercode) {
				return $language[self::$LANGCODE];
			}
		}
		return '';
	}


	/**
	 * Check if given language browser code (en, end-us, it, bg, etc.) is among the list of our languages
	 *
	 * @param string $browsercode
	 * @return boolean true= Search in Active languages only, false = all languages searched
	 */
	public static function isBrowserCodeAvail($code, $active = true) {
		return self::getInstance()->isValidCode($code, self::$BROWSERCODE, $active);
	}


	/**
	 * Check if given language LANG-CODE (english, italian, etc) is among the list of our languages
	 *
	 * @param string $code
	 * @return boolean true= Search in Active languages only, false = all languages searched
	 */
	public static function isLangCodeAvail($code, $active = true) {
		return self::getInstance()->isValidCode($code, self::$LANGCODE, $active);
	}


	/**
	 * This is just a wrapper for the Yii::t translation method
	 * @static
	 * @param $key
	 * @param bool $module
	 * @return string
	 */
	public static function t($key, $module = false) {
		return Yii::t($module, $key);
	}


	/**
	 * Return the full language name for example "en" will return "english". If an already full language name is provided
	 * then it will be returned as it is.
	 * @param $lang_code
	 * @return string
	 */
	public static function getFullLangName($lang_code) {

		// Temporary fix for old databases in which the swedish was saved with the sw browser code instead of sv
		if ($lang_code == 'sw') {
			$lang_code = 'sv';
		}

		$lang_code = strtolower(trim($lang_code)); //"normalize" the language code
		switch (Lang::getCodeType($lang_code)) {
			case self::$BROWSERCODE: $res = self::getCodeByBrowserCode($lang_code); break;
			case self::$LANGCODE: $res = $lang_code; break;
			default:
				// Better return some language, instead of throwing this exception, english is a nice safe-harbour 
				Yii::log("Invalid language: " . $lang_code, CLogger::LEVEL_ERROR, __METHOD__);
				return self::getCodeByBrowserCode(self::$LANGUAGE_FALLBACK);
				//throw new Exception('No full language name defined for: ' . $lang_code);
				break;
		}

		return $res;
	}


	/**
	 * Checks if a given language (identified by a browsercode - e.g. 'en') is
	 *  RTL or LTR (right to left or left to right).
	 * @param string $browsercode - pass a valid browsercode (e.g. 'it', 'en')
	 *  to check if it's a right-to-left language
	 * @return boolean - true if the provided browsercode is RTL or false otherwise
	 */
	public static function isRTL($browserCode) {
		$languages = self::getInstance()->getLanguages(true);
		foreach($languages as $language) {
			if ($language['browsercode'] == $browserCode && $language['direction'] == 'rtl') {
				return TRUE;
			}
		}
		return false;
	}

	
	/**
	 * Get source of flag icon searched by Language Code (e.g. "english", "french", ...)
	 * 
	 * @param string $lang_code  
	 * @return string
	 */
	public static function getFlagSrcByCode($lang_code) {
		//in unix servers, file names are case-sensitive, so we have to lowerize the lang code
		return Yii::app()->theme->baseUrl . "/images/language/".strtolower($lang_code).".png";
	}
	

	/**
	 * Get source of flag icon searched by Browser code (e.g. "en", "fr", ...)
	 * 
	 * @param string $browsercode
	 * @return string
	 */
	public static function getFlagSrcByBrowserCode($browsercode) {
		$lang_code = self::getCodeByBrowserCode($browsercode);
		return self::getFlagSrcByCode($lang_code);
	}

	/**
	 * Get languages in list
	 * @return string
	 */
	public static function getLanguagesListHtml()
	{
		$result = "";
		$languages = self::getLanguages(true);
		foreach($languages as $language) {
			$route = Yii::app()->getController()->getId().'/'.Yii::app()->getController()->getAction()->getId();
			$href = Yii::app()->controller->createUrl($route, array('lang'=>$language['browsercode']));
			//$href = Yii::app()->getBaseUrl(true) . '/?lang=' . $language['browsercode']; // using browser codes
			if ($language['browsercode'] == Yii::app()->getLanguage()) {
				$text = "<strong>${language['description']}</strong>";
			} else {
				$text = $language['description'];
			}
			$result .= "<li><a href='$href'>" . $text . "</a></li>\n";
		}
		return $result;
	}

	/**
	 * Converts, e.g. "en-uk" to "en-UK".
	 * Currently Bootstrap Datepicker needs it in that casing
	 * so it can load the proper localization JS file
	 *
	 * @param $inproperlyCased - a valid browsercode - e.g. "en-uk"
	 *
	 * @return string - a properly localized browsercode: e.g. "en-uk" to "en-UK",
	 * "pt-br" to "pt-BR", but values such as just "uk" will be returned as is
	 * without a change
	 */
	static public function getProperCasedBrowserCode($inproperlyCased){
		if(stripos($inproperlyCased, '-')!==false){
			// Lang code in format "en-uk", convert it to "en-UK" so Bootstrap Datepicker
			// can understand it and load the proper localization JS file
			$langParts = explode('-', $inproperlyCased, 2);
			if(count($langParts)>=2)
				return $langParts[0].'-'.strtoupper($langParts[1]);
		}
		return $inproperlyCased;
	}


	/**
	 * Given a specific code, detect ots type (browser or language)
	 * @param $code string
	 * @return bool|string the type of the provided code, or false if none applies
	 */
	public static function getCodeType($code) {

		//normalize input
		$code = strtolower(trim($code));

		//retrieve languages information
		foreach (self::getInstance()->__languages_all as $language) {
			//check if the provided code is a browser code
			if ($language[self::$BROWSERCODE] == $code) {
				return self::$BROWSERCODE;
			}
			//check if the provided code is a language code
			if ($language[self::$LANGCODE] == $code) {
				return self::$LANGCODE;
			}
		}

		//the provided code is neither a browser code or language code
		return false;
	}

	
	/**
	 * This method checks if Date format is "user_selected" (table core_setting) if is, returns selected by user language otherwise returns default language
	 * @return string (browser code e.g. 'en', 'bg', etc.)
	 */
	public static function getCustomBrowserCode() {
		$criteria = new CDbCriteria();
		$criteria->condition = 'param_name = :paramName AND param_value = :paramValue';
		$criteria->params = array(':paramName' => 'date_format', ':paramValue' => 'user_selected');
		$isUserSelected = CoreSetting::model()->count($criteria);
		
		if($isUserSelected) {
			$defaultLang = Yii::app()->getLanguage(); 
		} else {
			$defaultLang =  Lang::getBrowserCodeByCode(Settings::get('default_language', 'english'));
		}
		
		return $defaultLang;
	}
	
	
}
