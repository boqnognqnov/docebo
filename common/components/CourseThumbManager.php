<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

class CourseThumbManager extends CComponent {

	public $allowed_extensions = array('jpg', 'png'); // Allowed image extensions

	// Get the directory where thumbnails are stored
	public function getBaseThumbUrl() {

		return Yii::app()->theme->baseUrl . '/images/course_tb';
	}

	// Get the directory where thumbnails are stored
	public function getBaseThumbPath() {

		return Yii::app()->theme->basePath
			. DIRECTORY_SEPARATOR . 'images'
			. DIRECTORY_SEPARATOR . 'course_tb';
	}

	// Get all thubmnails from the common images directory
	public function getAllThumbs() {

		$dir = $this->getBaseThumbPath();

		$results = array();
		$scan =  glob($dir . DIRECTORY_SEPARATOR . '*');

		$pathinfo=Yii::app()->request->pathInfo;
		if(($pos=strrpos($pathinfo,'.'))!==false)
		   $extension=substr($pathinfo,$pos+1);

		foreach ($scan as $filename) {

			$extension = '';
			if (($pos = strrpos($filename, '.')) !== false)
				$extension = substr($filename, $pos+1);
			if (is_file($filename) && in_array($extension, $this->allowed_extensions)) {
				$results[] = str_replace($dir . DIRECTORY_SEPARATOR, '', $filename);
			}
		}

		return $results;
	}

}
