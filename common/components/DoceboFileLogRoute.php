<?php
/**
 * Override core file log routing to allow routing all log files from all (or set of) LMSs to a single file system location.
 * Later they can be used by Logstash or any other Log transport/reading/statistics tool or process.
 *
 */
class DoceboFileLogRoute extends CFileLogRoute {

	/**
	 * Enable/Disable this log route
	 * @var boolean
	 */
	public $enable = false;
		
	/**
	 * If set to true (here or in the Yii config), parent methods shall be used
	 * @var bool
	 */
	public $legacyMode = false;  
	
	/**
	 * Path to ROOT of all logs, e.g. /var/log/lmslogs; Must be set in Yii config and must be writable by Web 
	 * @var StringAppend
	 */
	public $logPathRoot = null;  
	
	/**
	 * Split the domain specific portion like  m/y/mylms.docebo.com
	 * @var bool
	 */
	public $splitPath = true;  	 
	
	/**
	 * If set to true all LMS installations will write their log messages into one single <logPathRoot>/application.log
	 * @var bool
	 */
	public $sharedLogFile = false;
	
	
	/**
	 * 
	 * @see CFileLogRoute::getLogPath()
	 */
	public function getLogPath() {
		
		// Skip this routing in legacy mode
		if ($this->legacyMode) {
			return parent::getLogPath();
		}
		
		// Chek the ROOT OF ALL LOGS path, use the parent log path if this is not a valid path.
		// This will re-route logging to default Yii behavior, whatever it is
		$this->logPathRoot = rtrim($this->logPathRoot, '\\/');
		$path = realpath($this->logPathRoot);
		if($path===false || !is_dir($path) || !is_writable($path)) {
			return parent::getLogPath();
		}
		
		// 'Shared log' means ALL LMSs writing log messages into one single big combined application.log
		if ($this->sharedLogFile) {
			return $path;
		}
		
		// Get domain code and, if requested, split the last portion of the path
		$domainCode = Docebo::getOriginalDomainCode();
		if ($this->splitPath) {
			$destPath = $this->logPathRoot . DIRECTORY_SEPARATOR . $domainCode[0] . DIRECTORY_SEPARATOR . $domainCode[1] . DIRECTORY_SEPARATOR . $domainCode;
		}
		else {
			$destPath = $this->logPathRoot . DIRECTORY_SEPARATOR . $domainCode;
		}

		// Now check the final path and if the path does not exists, create it, recursively, and make it writable
		$finalPath = realpath($destPath);
		if($finalPath===false || !is_dir($finalPath)) {
			FileHelper::createDirectory($destPath, 0777, true);
		}
		// However, if the final path exists, but is NOT writable, throw an exception
		else if (!is_writable($finalPath)) {
 			throw new CException(Yii::t('yii','DoceboFileLogRoute log path "{path}" points to a valid directory, but is not writable by the Web server process.',
 					array('{path}'=>$finalPath)));
		}


		return $finalPath;
	}
	
	
	/**
	 * @see CLogRoute::formatLogMessage()
	 */
	protected function formatLogMessage($message,$level,$category,$time)
	{
		$domainCode = Docebo::getOriginalDomainCode();
		return @date('Y/m/d H:i:s',$time)." [$domainCode] [$level] [$category] $message\n";
	}
	
	
	/**
	 * @see CFileLogRoute::processLogs()
	 */
	protected function processLogs($logs) {
		
		if (!$this->enable) {
			return FALSE;
		}
		
		parent::processLogs($logs);		
	}
	
	
}