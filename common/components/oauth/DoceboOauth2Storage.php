<?php

/**
 * Provides custom implementation for some storage interfaces (e.g. UserCredentialsInterface)
 * Class DoceboOauth2Storage
 */
class DoceboOauth2Storage extends OAuth2\Storage\Pdo
{
	/**
	 * Grant access tokens for basic user credentials.
	 *
	 * Check the supplied username and password for validity.
	 *
	 * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
	 *
	 * @param $username Username to be check with.
	 * @param $password Password to be check with.
	 *
	 * @return TRUE if the username and password are valid, and FALSE if it isn't.
	 * Moreover, if the username and password are valid, and you want to
	 *
	 * @see http://tools.ietf.org/html/rfc6749#section-4.3
	 *
	 */
	public function checkUserCredentials($username, $password) {
		// Load user
		$ui = new DoceboUserIdentity($username, $password);
		if($ui->authenticate()) {
			$user = CoreUser::model()->findByPk($ui->getId());
			if(!Yii::app()->user->isAccountExpired($user)) {
				// Restrict "api" scope to superadministrators
				$scope = Yii::app()->request->getParam('scope');
				if ($scope == 'api') {
					$group = CoreGroup::model()->findByAttributes(array('groupid' => Yii::app()->user->level_godadmin));
					if (CoreGroupMembers::model()->findByAttributes(array('idst' => $group->idst, 'idstMember' => $ui->getId())))
						if(PluginManager::isPluginActive("AWSChinaApp"))
							CoreSettingUser::model()->deleteAllByAttributes( array('id_user'   => $ui->getId(), 'path_name' => AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME));
				} else
					if(PluginManager::isPluginActive("AWSChinaApp"))
						CoreSettingUser::model()->deleteAllByAttributes(array('id_user' => $ui->getId(), 'path_name' => AWSChinaAppModule::CHINA_REACHABLE_SETTING_NAME));
					return true;
			} else
				return false; // Expired non superadmin accounts cannot login via oauth2
		}

		return false;
	}

	/**
	 * @return ARRAY the associated "user_id" and optional "scope" values
	 * This function MUST return FALSE if the requested user does not exist or is
	 * invalid. "scope" is a space-separated list of restricted scopes.
	 * @code
	 * return array(
	 *     "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
	 *     "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
	 * );
	 * @endcode
	 */
	public function getUserDetails($username) {
		$user = CoreUser::model()->find('userid = :userid AND valid = :valid', array(
			':userid' => Yii::app()->user->getAbsoluteUsername($username),
			':valid' => 1
		));

		return array(
			'user_id' => $user->idst
		);
	}
}