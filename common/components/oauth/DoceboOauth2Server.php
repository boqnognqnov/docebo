<?php
/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2014 Docebo
 */

use OAuth2\ResponseType\AuthorizationCode as AuthorizationCodeResponseType;

class DoceboOauth2Server extends CComponent {

	const REFRESH_TOKEN_LIFETIME = 2419200;

	/**
	 * @var OAuth2\Server
	 */
	private $server;

	/**
	 * @var OAuth2\Request
	 */
	private $request;

	/**
	 * @var OAuth2\Response
	 */
	private $response;

	/**
	 * @var DoceboOauth2Storage
	 */
	private $storage;

	/**
	 * The singleton instance of this class
	 * @var DoceboOauth2Server
	 */
	private static $instance;

    public $serverConfiguration = array(
        'enforce_state' => false,
        'allow_public_clients' => false,
        'allow_credentials_in_request_body' => true,
        'allow_implicit' => true,
        'refresh_token_lifetime' => self::REFRESH_TOKEN_LIFETIME
    );

	/**
	 * Class constructor (private for singleton pattern)
	 */
	private function __construct() {}

	/**
	 * Returns the singleton instance of this server
	 *
	 * @return DoceboOauth2Server
	 */
	public static function getInstance() {
		if(!self::$instance) {
			self::$instance = new DoceboOauth2Server();
		}

		return self::$instance;
	}

	/**
	 * Initializer method
	 *
	 * @return DoceboOauth2Server
	 */
	public function init($tokenLifetime = null) {
		$db  = Yii::app()->db; /* @var $db CDbConnection */
		$dsn = $db->connectionString;

		// Autoloading library component
		require_once(Yii::getPathOfAlias('addons').'/OAuth2/Autoloader.php');
		OAuth2\Autoloader::register();

		// Initializing storage & server object
		$this->storage = new DoceboOauth2Storage(array('dsn' => $dsn, 'username' => $db->username, 'password' => $db->password));

        if($tokenLifetime){
            $this->serverConfiguration['access_lifetime'] = $tokenLifetime;
        }

		$this->server = new OAuth2\Server($this->storage, $this->serverConfiguration);

		// Prepare request & response objects for later use
		$this->request = OAuth2\Request::createFromGlobals();
		$this->response = new OAuth2\Response();

		// Set up grant types
		$this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($this->storage));
		$this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($this->storage));
		$this->server->addGrantType(new OAuth2\GrantType\UserCredentials($this->storage));
		$this->server->addGrantType(new OAuth2\GrantType\RefreshToken($this->storage, array (
			'always_issue_new_refresh_token' => false,
			'unset_refresh_token_after_use' => false
		)));

		return $this;
	}

	/**
	 * Send the right OAuth2 HTTP response
	 */
	public function sendResponse() {
		if($this->response) {
			$this->response->send();
		} else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Checks if the authorize request is a valid one
	 */
	public function validateAuthorizeRequest() {
		if($this->server) {
			if($this->server->validateAuthorizeRequest($this->request, $this->response))
				return true;
			else
				throw new CException("Invalid authorization request. Error: ".$this->response->getParameter('error').", Error description: ".$this->response->getParameter('error_description'));
		} else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Handles authorize request (approval or denial)
	 *
	 * @param $isAuthorized boolean
	 *
	 * @throws CException
	 */
	public function handleAuthorizeRequest($isAuthorized) {
		if($this->server) {
			$this->server->handleAuthorizeRequest($this->request, $this->response, $isAuthorized, Yii::app()->user->id);
		} else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Returns the current client id (for an authorize request)
	 */
	public function getClientId() {
		if($this->server) {
			return $this->server->getAuthorizeController()->getClientId();
		} else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Handles the token request call
	 * @throws CException
	 */
	public function handleTokenRequest() {
		if($this->server)
			$this->server->handleTokenRequest($this->request)->send();
		else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Runs the verification check on the oauth token passed
	 *
	 * @param $requested_scope The scope requested (e.g. api)
	 *
	 * @return true if validation is successfull
	 */
	public function verifyResourceRequest($requested_scope) {
		if($this->server)
			return $this->server->verifyResourceRequest($this->request, $this->response, $requested_scope);
		else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Returns the user currently authorized for the passed token
	 */
	public function getAuthorizedUser() {
		if($this->server) {
			$tokenData = $this->server->getAccessTokenData($this->request, $this->response);
			if($tokenData && $tokenData['user_id'])
				return CoreUser::model()->findByPk($tokenData['user_id']);
		}
		else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Generates an authorization code for a specified user in the specified course
	 *
	 * @param $userId
	 * @param $clientId
	 * @param $redirect_uri
	 * @param null $scope
	 * @param int $auth_code_lifetime
	 * @return string
	 * @throws CException
	 */
	public function generateAuthorizationCode($userId, $clientId, $redirect_uri, $scope = null, $auth_code_lifetime = 30) {
		if($this->server) {
			$response = new AuthorizationCodeResponseType($this->storage, array(
				'auth_code_lifetime' => $auth_code_lifetime
			));

			return $response->createAuthorizationCode($clientId, $userId, $redirect_uri, $scope);
		}
		else
			throw new CException("Oauth2 server not initialized");
	}

	/**
	 * Creates an access token for a certain user
	 *
	 * @param $clientId
	 * @param $userId
	 * @param $scope
     * @param $config array
	 */
	public function createAccessToken($clientId, $userId, $scope, $config = array()) {
		if($this->server) {
			$response = new \OAuth2\ResponseType\AccessToken($this->storage, $this->storage, $config);
			return $response->createAccessToken($clientId, $userId, $scope);
		}
		else
			throw new CException("Oauth2 server not initialized");
	}
}