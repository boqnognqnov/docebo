<?php

/**
 * DoceboCHtmlPurifier is an extensione of the CHtmlPurifier wrapper for {@link http://htmlpurifier.org HTML Purifier}.
 *
 * Example of usage as Yii components:
 *
 * $filtered_test = Yii::app()->htmlpurifier->setOptionSet('standard')->purify($text);
 * $filtered_test = Yii::app()->htmlpurifier->setOptionSet(array(...))->purify($text);
 * $filtered_test = Yii::app()->htmlpurifier->purify($text, 'standard');
 * $filtered_test = Yii::app()->htmlpurifier->purify($text, array(...));
 * $filtered_test = Yii::app()->htmlpurifier->purify($text);
 *
 * @package common.components
 */
class DoceboCHtmlPurifier extends CHtmlPurifier
{
	protected $options_sets;

	private $_purifier;

	public function __construct()
	{
		parent::__construct();

		// Try to get custom defined iframe sources(urls)
		$customIframeUrls = CoreSettingWhitelist::model()->findByAttributes(array('type' => 'lo_iframe_url'));
		$addIframeUrlRules = '';
		if($customIframeUrls) {
			$allcustomIframeUrls = CJSON::decode($customIframeUrls->whitelist);
			foreach($allcustomIframeUrls as $customIframeUrl) {
				$customIframeUrl = preg_replace('%^(http://|https://)%', '', $customIframeUrl);
				$addIframeUrlRules .= '|'.$customIframeUrl;

				if(!preg_match('%/$%', $customIframeUrl))
					$addIframeUrlRules .= '|'.$customIframeUrl . '/[\w]+/';
				else
					$addIframeUrlRules .= '|'.$customIframeUrl . '[\w]+/';
			}
		}

		$newIframeRules = Yii::app()->params['embed_whitelist'];
		if($addIframeUrlRules != '') {
			$newIframeRules = substr($newIframeRules, 0, -2);
			$newIframeRules .= $addIframeUrlRules . ')%';
		}

		$this->options_sets = array(
			'default' => array(
				'HTML.MaxImgLength' => NULL,
				'Attr.EnableID' => true,
				//'Attr.IDPrefix' => 'customer_'
			),
			'standard' => array(
				'Attr.AllowedFrameTargets' => array('_blank', '_top'),
				'HTML.MaxImgLength' => NULL,
				'Attr.EnableID' => true,
				//'Attr.IDPrefix' => 'customer_'
			),
			'allowFrame' => array(
				'HTML.SafeIframe' => true,
				'URI.SafeIframeRegexp' => Yii::app()->params['embed_whitelist'],
				'HTML.MaxImgLength' => NULL,
				'Attr.EnableID' => true,
				//'Attr.IDPrefix' => 'customer_'
			),
			'allowFrameAndTarget' => array(
				'HTML.MaxImgLength' => NULL,
				'Attr.EnableID' => true,
				//'Attr.IDPrefix' => 'customer_',
				'HTML.SafeIframe' => true,
				'Attr.AllowedFrameTargets' => array('_blank', '_top'),
				'URI.SafeIframeRegexp' => Yii::app()->params['embed_whitelist']
			),
			'allowFrameAndTargetLo' => array(
				'HTML.MaxImgLength' => NULL,
				'Attr.EnableID' => true,
				//'Attr.IDPrefix' => 'customer_',
				'HTML.SafeIframe' => true,
				'Attr.AllowedFrameTargets' => array('_blank', '_top', '_parent'),
				'URI.SafeIframeRegexp' => $newIframeRules
			),
				
			// General XSS prevention: allow all elements but forbid some
			// Note: current HTML Purifier version does not support HTML5
			// some element attributes are always stripped out (e.g. data-xxxx)  
			'xssGuard' => array(
				'Attr.EnableID' => true,
				'HTML.MaxImgLength' => NULL,
 				'HTML.Trusted'	=> true,
				'CSS.Trusted'   => true,
				'HTML.ForbiddenElements' => array(
					'script'
				),
				'Attr.AllowedFrameTargets' => array('_blank', '_self', '_parent', '_top'),
				'HTML.MaxImgLength' => NULL,
			),
		);
	}


	/**
	 * @param mixed $name an array of option to be passed to the HTML Purifier instance or the name of one of the default set of options defined before.
	 * @return DoceboCHtmlPurifier the istance of the DoceboCHtmlPurifier with the new option set.
	 */
	public function setOptionSet($name = false)
	{
		if(is_array($name))
			$this->options = $name;
		if(isset($this->options_sets[$name]))
			$this->options = $this->options_sets[$name];
		else
			$this->options = $this->options_sets['standard'];

		$this->setOptions($this->options);

		return $this;
	}

	protected function createNewHtmlPurifierInstance()
	{
		$options = $this->getOptions();

		$config = HTMLPurifier_Config::createDefault();
		$config->set('HTML.Doctype', 'HTML 4.01 Transitional');
		$config->set('CSS.AllowTricky', true);
		$config->set('Cache.SerializerPath', Yii::app()->getRuntimePath());
        $config->set('HTML.Attr.Name.UseCDATA', true);

		if($options){
			foreach($options as $key => $value)
				$config->set($key, $value);
		}


		// Set some HTML5 properties
		$config->set('HTML.DefinitionID', 'html5-definitions'); // unqiue id
		$config->set('HTML.DefinitionRev', 1);
		$config->set('CSS.DefinitionRev', 2);

		$cssDefinition = $config->getCSSDefinition();
		$cssDefinition->info['list-style-type'] = new HTMLPurifier_AttrDef_Enum(
			array(
				'disc',
				'circle',
				'square',
				'decimal',
				'lower-roman',
				'upper-roman',
				'lower-alpha',
				'upper-alpha',
				'lower-greek',
				'none'
			),
			false);

		if ($def = $config->maybeGetRawHTMLDefinition()) {
			// http://developers.whatwg.org/sections.html
			$def->addElement('section', 'Block', 'Flow', 'Common');
			$def->addElement('nav',     'Block', 'Flow', 'Common');
			$def->addElement('article', 'Block', 'Flow', 'Common');
			$def->addElement('aside',   'Block', 'Flow', 'Common');
			$def->addElement('header',  'Block', 'Flow', 'Common');
			$def->addElement('footer',  'Block', 'Flow', 'Common');
			// Content model actually excludes several tags, not modelled here
			$def->addElement('address', 'Block', 'Flow', 'Common');
			$def->addElement('hgroup', 'Block', 'Required: h1 | h2 | h3 | h4 | h5 | h6', 'Common');
			// http://developers.whatwg.org/grouping-content.html
			$def->addElement('figure', 'Block', 'Optional: (figcaption, Flow) | (Flow, figcaption) | Flow', 'Common');
			$def->addElement('figcaption', 'Inline', 'Flow', 'Common');
			// http://developers.whatwg.org/the-video-element.html#the-video-element
			$def->addElement('video', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
				'src' => 'URI',
				'type' => 'Text',
				'width' => 'Length',
				'height' => 'Length',
				'poster' => 'URI',
				'preload' => 'Enum#auto,metadata,none',
				'controls' => 'Text',
			));
			$def->addElement('source', 'Block', 'Flow', 'Common', array(
				'src' => 'URI',
				'type' => 'Text',
			));
			// http://developers.whatwg.org/text-level-semantics.html
			$def->addElement('s',    'Inline', 'Inline', 'Common');
			$def->addElement('var',  'Inline', 'Inline', 'Common');
			$def->addElement('sub',  'Inline', 'Inline', 'Common');
			$def->addElement('sup',  'Inline', 'Inline', 'Common');
			$def->addElement('mark', 'Inline', 'Inline', 'Common');
			$def->addElement('wbr',  'Inline', 'Empty', 'Core');
			// http://developers.whatwg.org/edits.html
			$def->addElement('ins', 'Block', 'Flow', 'Common', array('cite' => 'URI', 'datetime' => 'CDATA'));
			$def->addElement('del', 'Block', 'Flow', 'Common', array('cite' => 'URI', 'datetime' => 'CDATA'));
			// TinyMCE
			$def->addAttribute('img', 'data-mce-src', 'Text');
			$def->addAttribute('img', 'data-mce-json', 'Text');
			// Others
			$def->addAttribute('iframe', 'allowfullscreen', 'Bool');
			$def->addAttribute('table', 'height', 'Text');
			$def->addAttribute('td', 'border', 'Text');
			$def->addAttribute('th', 'border', 'Text');
			$def->addAttribute('tr', 'width', 'Text');
			$def->addAttribute('tr', 'height', 'Text');
			$def->addAttribute('tr', 'border', 'Text');
		}

		$this->_purifier = new HTMLPurifier($config);

		return $this->_purifier;
	}


	/**
	 * @param string $content The text to purify
	 * @param mixed $name an array of option to be passed to the HTML Purifier instance or the name of one of the default set of options defined before. Leave it to false to use the current loaded options. After the execution of the purify the option will set to the previous ones.
	 * @return string The purified text
	 */
	public function purify($content, $name = false)
	{
		$old_options = array();

		if($name !== false)
		{
			$old_options = $this->options;
			$this->setOptionSet($name);
		}

		$res = parent::purify($content);

		if($name !== false)
			$this->options = $old_options;

		return $res;
	}
}