<?php
/**
 * General usage component. Provide Player functionality/services to all parts of the LMS
 *
 * Usage example:
 * 	No matter which part of LMS you are (widget, view, even in authoringm etc.) just check
 *
 * 	if (Yii::app()->player->course) {
 * 		echo Yii::app()->player->course->title  // course is LearningCourse model instance
 *  }
 *
 */
class Player extends CApplicationComponent {


	const PLAYER_MODE_VIEWCOURSE = 'view_course';
	const PLAYER_MODE_EDITCOURSE = 'edit_course';


	/**
	 * Error constants
	 * @var number
	 */
	const PLAYER_ERR_INVALID_LO				= 201;
	const PLAYER_ERR_INVALID_LO_TYPE		= 202;
	const PLAYER_ERR_BAD_RESOURCE			= 203;
	const PLAYER_ERR_FAILED_LAUNCH_URL		= 204;
	const PLAYER_ERR_AUTH_TOKEN_NOT_FOUND	= 205;


	/**
	 * Array of errors, used in Player::getLoAuthPlayUrl()
	 * @var array(<errorn-number-constant> => <error-message>)
	 */
	public static $errorMessages = array(
			self::PLAYER_ERR_INVALID_LO 			=> "Invalid Learning Object requested",
			self::PLAYER_ERR_BAD_RESOURCE	 		=> "Bad or missing resource",
			self::PLAYER_ERR_FAILED_LAUNCH_URL		=> "Error while retrieving the launch URL for the requested resource",
			self::PLAYER_ERR_AUTH_TOKEN_NOT_FOUND	=> "Auth token not found for a user",
			self::PLAYER_ERR_INVALID_LO_TYPE		=> "Unsupported learning object type",

	);

	private $_playerAssetsUrl;


	// Will hold a CourseModel
	public $course = null;

	public $coursePath = null;

	/**
	 * (non-PHPdoc)
	 * @see CApplicationComponent::init()
	 */
	public function init() {
	}


	/**
	 * Assign Coursepath Model to player component
	 *
	 * @param $coursePathModel LearningCoursepath
	 */
	public function setCoursePath($coursePathModel){
		if($coursePathModel instanceof LearningCoursepath){
			$this->coursePath = $coursePathModel;
		}
	}


	/**
	 * Assign Course Model to player component.
	 * Called when user enters PLAYER module.
	 *
	 * @param LearningCourse $courseModel
	 */
	public function setCourse($courseModel) {
		if ($courseModel) {
			UserLimit::logActiveUserAccess();
			$this->course = $courseModel;
		}
	}

	/**
	 * Redirect to VIEW/PLAY COURSE UI
	 * @param int $course_id
	 */
	public function viewCourse($course_id) {
		$url = Yii::app()->controller->createUrl("//player/training", array(
				"mode" => self::PLAYER_MODE_VIEWCOURSE,
				"course_id" => $course_id,
		));
		Yii::app()->controller->redirect($url, true);
	}


	/**
	 * Redirect to EDIT COURSE UI
	 * @param int $course_id
	 */
	public function editCourse($course_id) {
		$url = Yii::app()->controller->createUrl("//player/training", array(
				"mode" => self::PLAYER_MODE_EDITCOURSE,
				"course_id" => $course_id,
		));
		Yii::app()->controller->redirect($url, true);
	}



	/**
	 * Get PLAYER's assets url path
	 * We have plenty of CSS/JS code in Player module's assets already, which we do share.
	 *
	 * @return mixed
	 */
	public function getPlayerAssetsUrl()
	{
		if ($this->_playerAssetsUrl === null) {
			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}


	/**
	 * Compose URL used by external clients (mobile devices, smartphones, etc) to play learning objects.
	 * Adds Light Authentication parameters to the URL which later allow "single-click auth & play".
	 * Uses REST Auth table to generate/retrieve authentication token/code for the user.
	 * Note that this request for video mark them as completed
	 *
	 * @param number $idOrg
	 * @param number $idUser
	 * @param number $idScormItem
	 * @return array("success" => true|false, "err" => <number>)
	 */
	public static function getLoAuthPlayUrl($idOrg, $idUser, $idScormItem=false, $launchType = false, $auth_code=false) {
		$loModel = LearningOrganization::model()->findByPk($idOrg);
		if (!$loModel) {
			return array("success" => false, "err" => self::PLAYER_ERR_INVALID_LO);
		}

		$auth = new AuthToken($idUser);
		if ($auth && !$auth_code) {
			// Get auth token, generate if not found
			$auth_code = $auth->get(true);
		}
        if(!$auth || empty($auth_code)) {
			return array("success" => false, "err" => self::PLAYER_ERR_AUTH_TOKEN_NOT_FOUND);
		}
		// Base parameters
		$urlParams = array(
				"id_object" 	=> $idOrg,
				"course_id" 	=> $loModel->idCourse,
				"id_user" 		=> $idUser,		// the user ID (number); using id_user to stay consistent with SCORM light-auth (the same parameter)
				"auth_code" 	=> $auth_code   // the token (see above RE: parameter name)
		);

		// Build launch url for each learning object type
		switch ($loModel->objectType) {

			case LearningOrganization::OBJECT_TYPE_AUTHORING :

				$authoring_model = LearningAuthoring::model()->findByPk($loModel->idResource);
				if (!$authoring_model) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$url = Docebo::createAbsoluteAuthoringUrl("main/play", array(
					'id' => $loModel->idResource,
					'id_object' => $loModel->idOrg,
					'id_course' => $loModel->idCourse,
					'stripped' => 1,
					'fullscreen' => 1,
				));

				break;

			case LearningOrganization::OBJECT_TYPE_VIDEO:
				$model = LearningVideo::model()->findByPk($loModel->idResource);
				$video_files = CJSON::decode($model->video_formats);
				$video_file_names = array_values($video_files);
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
				$url = $storage->fileUrl($video_file_names[0]);

				// We have low control on external, so we are marking them as completed
				$tracking = VideoComponent::track($idUser, $idOrg, $loModel->idResource, LearningCommontrack::STATUS_COMPLETED);
				UserLimit::logActiveUserLogin($idUser, true);
				CommonTracker::track($idOrg, $idUser, LearningCommontrack::STATUS_COMPLETED);
				if (!$tracking) return array("success" => false, "err" => 203);

				break;

			case LearningOrganization::OBJECT_TYPE_SCORMORG:
				if (!$idScormItem) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$scorm_item = LearningScormItems::model()->findByPk($idScormItem);
				if (!$scorm_item) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$scorm_nav = new ScormNav();
				$data = $scorm_nav->getScoLaunchInfo($idOrg, $scorm_item, $idUser, $launchType);
				if (!$data['sco_url']) {
					return array("success" => false, "err" => self::PLAYER_ERR_FAILED_LAUNCH_URL);
				}
				$url = $data['sco_url'];
				break;

			case LearningOrganization::OBJECT_TYPE_AICC:

				$mode			= Yii::app()->request->getParam("mode", LearningAiccItemTrack::LESSON_MODE_NORMAL);

				$criteria = new CDbCriteria();
				$criteria->addCondition('(id_package = :idPackage) AND (parent != "/") ');
				$criteria->params = array(':idPackage' => $loModel->idResource);
				$item = LearningAiccItem::model()->find($criteria);
				if (!$item) {
					throw new CException('Invalid Aicc item: ' . $loModel->idResource);
				}

				$session = LearningAiccSession::getSession($item->id, $mode, $idOrg);
				if (!$session) {
					throw new CException(Yii::t('standard', 'Invalid session ID'));
				}

				$url = $item->launchUrl($session, $idOrg);
				break;
			case LearningOrganization::OBJECT_TYPE_FILE:
				$idFile = (int) $loModel->idResource;
				$file = LearningMaterialsLesson::model()->findByPk($idFile);
				if (!$file) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				// Add one more URL parameter: id_file
				$urlParams["id_file"] = $file->idLesson;
				$url = Docebo::createAbsoluteLmsUrl("//file/default/sendFile", $urlParams);
				break;

			case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
				$htmlpage_model = LearningHtmlpage::model()->findByPk($loModel->idResource);
				if (!$htmlpage_model) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$url = Docebo::createAbsoluteLmsUrl("//htmlpage/default/content", $urlParams);
				break;

			case LearningOrganization::OBJECT_TYPE_TEST:
				$test_model = LearningTest::model()->findByPk($loModel->idResource);
				if (!$test_model) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$url = Docebo::createAbsoluteLmsUrl("//mobileapp/test/launch", $urlParams);
				break;

			case LearningOrganization::OBJECT_TYPE_TINCAN:
				$object = LearningOrganization::model()->findByPk($idOrg);
				$activity = LearningTcActivity::model()->findByPk($object->idResource);
				// Activity Provider (not to mess with Activity (which are children of AP)
				$activityProvider = $activity->ap;  // using AR relation
				$userData 		= CoreUser::model()->findByPk($idUser);
				$auth 			= LearningTcAp::buildBasicAuth($userData);
				$actor 			= $activityProvider->buildActor($userData);
				$endpoint		= Docebo::getRootBaseUrl(true) . "/tcapi/";
				$registration 	= $activityProvider->registration;
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_TINCAN);
				$launchUrl 		= $storageManager->fileUrl($activity->launch, $activityProvider->path);

				// Create TinCan launching URL.
				// NOTE: it does NOT require token authentication parameters, because .. well..  this is TinCan, auth is part of the call, no session whatsoever
				$url = $launchUrl
					. "?endpoint=" 		. rawurlencode($endpoint)
					. "&auth=" 			. rawurlencode($auth)
					. "&actor="			. rawurlencode(CJSON::encode($actor))
					. "&registration="	. rawurlencode($registration);

				break;
			case LearningOrganization::OBJECT_TYPE_LTI:
				$lti_model = LearningLti::model()->findByPk($loModel->idResource);
				if (!$lti_model) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$url = $lti_model->url;
				break;
			case LearningOrganization::OBJECT_TYPE_ELUCIDAT:
				$url = self::getElucidatUrl($idOrg);
				break;

			case LearningOrganization::OBJECT_TYPE_GOOGLEDRIVE:
				$gd_model = LearningGoogledoc::model()->findByPk($loModel->idResource);
				if (!$gd_model) {
					return array("success" => false, "err" => self::PLAYER_ERR_BAD_RESOURCE);
				}
				$url = $gd_model->url;
				break;

			default:
				return array("success" => false, "err" => self::PLAYER_ERR_INVALID_LO_TYPE);
		}

		return array("success" => true, "err" => 0, "loModel" => $loModel, "url" => $url);

	}


	private static function getElucidatUrl($idObject){
		$object = LearningOrganization::model()->findByPk($idObject);
		try {
			if (!$object || $object->objectType != LearningOrganization::OBJECT_TYPE_ELUCIDAT) {
				throw new CException('Invalid Elucidat Learning Object: ' . $idObject);
			}

			$learningElucidat = LearningElucidat::model()->findByAttributes(array('id_resource' => $object->idResource));

			if (!$learningElucidat) {
				throw new CException('Invalid Elucidat Learning Object: ' . $idObject);
			}

			// Launcher Activity (as per tincan.xml)
			$activity = LearningTcActivity::model()->findByPk($object->idResource);
			if (!$activity) {
				throw new CException('Invalid Activity: '.$object->idResource);
			}

			// Activity Provider (not to mess with Activity (which are children of AP)
			$activityProvider = $activity->ap;  // using AR relation

			$userData = CoreUser::model()->findByPk(Yii::app()->user->id);
			$actor = $activityProvider->buildActor($userData);

			// Get Elucidat keys from the settings
			$publicKey = Settings::get('elucidat_consumer_key', null);
			$secretKey = Settings::get('elucidat_secret_key', null);

			$client = new ElucidatApiClient($publicKey, $secretKey);

			// Get the Elucidat launch url
			$res = $client->getLaunchUrl($learningElucidat->r_release_code  , Yii::app()->user->loadUserModel()->getFullName(), Yii::app()->user->loadUserModel()->email, $res->data['url'] . '&iduser='.Yii::app()->user->id.'&lmsreg='.$activityProvider->registration);

			if ($res->success == true) {
				// Build Learner specific temporary URL
				$launch = $res->data['url'];
				return $launch;
			} else {
				return false;
			}

		}
		catch (CException $e) {
			$response->setStatus(false)->setMessage($e->getMessage())->toJSON();
			Yii::app()->end();
		}
	}

}

?>
