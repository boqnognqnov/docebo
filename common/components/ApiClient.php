<?php

/**
 * Class ApiClient
 * @author Simeon Ivanov
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
abstract class ApiClient {

	// Used to collect and send parameters, JSON encoded.
	// Also used to generate the authorization header
	public $params = array();

	public function call ( $apiAction ) {
		$res    = NULL;
		$curl   = curl_init();
		$error  = NULL;
		$apiUrl = $this->getApiBaseUrl() . $apiAction;
		Yii::trace( "Api called: " . $apiUrl, 'api-client' );

		$opt = array(
			CURLOPT_URL            => $apiUrl,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HTTPHEADER     => $this->getAuthHeader(),
			CURLOPT_POST           => 1,
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => (stripos($apiUrl, "https://") === 0) ? 1 : 0,
			CURLOPT_HEADER         => 1,
			CURLOPT_BINARYTRANSFER => 1,
			CURLOPT_POSTFIELDS     => $this->params,
			CURLOPT_CONNECTTIMEOUT => 5, // Timeout to 5 seconds
		);
		curl_setopt_array( $curl, $opt );

		// $output contains the output string
		$output = curl_exec( $curl );

		$success = FALSE;
		if( $output == FALSE ) {
			$error = curl_error( $curl );
		} else {
			$responseCode = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
			$header_len   = curl_getinfo( $curl, CURLINFO_HEADER_SIZE );
			$body         = substr( $output, $header_len );
			if( $responseCode == 200 ) {
				$success = TRUE;
				$res     = CJSON::decode( $body );
			} else {
				$error = 'Error: ' . $responseCode . ': ' . $body;
			}
		}
		if( ! $success ) {
			Yii::log( "Api call FAILED: " . $apiUrl . " Error: {$error}", CLogger::LEVEL_ERROR, 'api-client' );
			$this->sendErrorNotification( $apiUrl, $error, $this->params );
		}

		// it closes the session
		curl_close( $curl );

		return $res;
	}

	/**
	 * Generate authentication header
	 * @return array
	 */
	protected function getAuthHeader () {
		$key    = $this->getApiKey();
		$secret = $this->getApiSecret();

		$sha    = sha1( implode( ',', $this->params ) . ',' . $secret );
		$x_auth = base64_encode( $key . ':' . $sha );

		$headers = [
			"Host: " . parse_url( $this->getApiBaseUrl(), PHP_URL_HOST ),
			"Content-Type: multipart/form-data",
			'X-Authorization: DoceboErp ' . $x_auth,
		];

		return $headers;
	}

	/**
	 * each class should implement this and to return the API Key
	 * @return mixed
	 */
	abstract function getApiKey ();

	/**
	 * each class should implement this and to return the API Secret
	 * @return mixed
	 */
	abstract function getApiSecret ();

	/**
	 * Return base url of the target API server, i.e. http://orchestrator.docebo.com/api/
	 * @return mixed
	 */
	abstract function getApiBaseUrl ();

	/**
	 * @param $apiUrl
	 * @param $error
	 * @param $data_params
	 */
	private function sendErrorNotification ( $apiUrl, $error, $data_params ) {
		// @TODO
	}
}