<?php

Yii::import('common.components.ftp.*');

class FtpManager extends CApplicationComponent
{
    /**
     * @var string $host sftp host ip.
     */
    public $host = null;

    /**
     * @var string $port sftp host port default 22.
     */
    public $port = '22';

    /**
     * @var string $username username of remote sftp account.
     */
    public $username = null;

    /**
     * @var string $username username of remote sftp account.
     */
    public $password = null;

    public $ssh = null;

	/**
	 * @var string The path to the .ppk file to be used for the SSH connection
	 *             If not set, the regular username and password authentication
	 *             will be used (less secure)
	 */
	public $sshFilePath = false;

	/**
	 * @var string The password for the private .ppk file used for SFTP auth
	 */
	public $sshFilePassword = null;

    private $handler = null;

	/**
	 * @param string $host
	 * @param string $username
	 * @param string $password
	 * @param string $port
	 * @param string $ssh
	 * @param bool   $sshFilepath
	 * @param null   $sshFilePassword
	 */
    public function __construct($host = null, $username = null, $password = null, $port = '22', $ssh = null, $sshFilepath = false, $sshFilePassword = null)
    {
        $this->host     = $host;
        $this->username = $username;
        $this->password = $password;
        $this->port     = $port;
        $this->ssh      = $ssh;

		$this->sshFilePath = $sshFilepath;
		$this->sshFilePassword = $sshFilePassword;
    }

    /**
     * Initializes the component.
     */
    public function init()
    {
        parent::init();
    }

    public function resolveHandler(){

        /// the handler is already resolved
        if($this->handler !== null) return;

        if($this->ssh)
            $this->handler = new SFTPComponent($this->host, $this->username, $this->password, $this->port, $this->sshFilePath, $this->sshFilePassword);
        else
            $this->handler = new FTPComponent($this->host, $this->username, $this->password, $this->port);

		return $this->handler;
    }

    /**
     * Establish FTP/SFTP connection
     * @return bool true when connection success
     * @throws CException if connection fails
     */
    public function connect()
    {
        $this->resolveHandler();

        return $this->handler->connect();
    }

    /**
     * list directory contents
     * @param string $directory Directory path
     * @return array $files list of contents including directories
     */
    public function listFiles($directory){
        return $this->handler->listFiles($directory);
    }

    /**
     * Returns the current directory
     * @return string Current directory path
     */
    public function getCurrentDir(){
        return $this->handler->getCurrentDir();
    }

    /**
     * Change directory
     * @param string $directory Directory path
     * @return bool true if directory change success
     * @throws CException if directory change fails
     */
    public function chdir($dir){
        return $this->handler->chdir($dir);
    }

    /**
     * Put file to a sftp location
     * @param string $localFile Local file path
     * @param string $remoteFile Remote file path
     * @return bool true if file send success
     * @throws CException if file transfer fails
     */
    public function sendFile($localFile, $remoteFile){
        return $this->handler->sendFile($localFile, $remoteFile);
    }

    /**
     * Get file from sftp location
     * @param string $remoteFile Remote file path
     * @param string $localFile Local file path
     * @return bool true if file retreival success
     * @throws CException if file transfer fails
     */
    public function getFile($remoteFile, $localFile){
        return $this->handler->getFile($remoteFile, $localFile);
    }

    /**
     * Retreive file size
     * @param string $file Remote file path
     * @return string File size
     */
    public function getFileSize($file){
        return $this->handler->getFileSize($file);
    }

    /**
     * Retreive file modified datetime
     * @param string $file Remote file path
     * @return string File modified timestamp
     */
    public function getMdtm($file){
        return $this->handler->getMdtm($file);
    }

    /**
     * Create directory on sftp location
     * @param string $directory Remote directory path
     * @return bool true if directory creation success
     * @throws CException if directory creation fails
     */
    public function mkdir($directory){
        return $this->handler->mkdir($directory);
    }

    /**
     * Remove directory on sftp location
     * @param string $directory Remote directory path
     * @return bool true if directory removal success
     * @throws CException if directory removal fails
     */
    public function rmdir($directory){
        return $this->handler->rmdir($directory);
    }

    /**
     * Remove file on sftp location
     * @param string $file Remote file path
     * @return bool true if file removal success
     * @throws CException if file removal fails
     */
    public function removeFile($file){
        return $this->handler->removeFile($file);
    }

    /**
     * Change directory permission
     * @param string $path Directory or file path
     * @param string $permission Permission
     * @return bool true
     */
    public function chmod($path, $permission){
        return $this->handler->chmod($path, $permission);
    }

    /**
     * Execute command on remote shell
     * @param string $cmd Command ex:pwd
     * @return string $output Command output
     */
    public function execCmd($cmd){
        return $this->handler->execCmd($cmd);
    }
}