<?php
class AuditActiveRecord extends CActiveRecord
{
	private $oldAttributes;

	protected function afterFind()
	{
		$this->oldAttributes = $this->getAttributes();
		parent::afterFind();
	}

	public function hasAttributeChanged($name)
	{
		return $this->$name !== $this->oldAttributes[$name];
	}

	public function hasAttributesChanged($arrNames)
	{
		foreach ($arrNames as $name)
		{
			if ($this->hasAttributeChanged($name))
			{
				return true;
			}
		}
		return false;
	}

	public function getLoadedValue($name)
	{
		if (isset($this->oldAttributes[$name]))
			return $this->oldAttributes[$name];
		else
			return null;
	}

	public function getChangedAttributes($names = array())
	{
		if (count($names) == 0)
			$names = $this->attributeNames();
		$result = array();
		foreach ($names as $name)
		{
			if ($this->hasAttributeChanged($name))
				$result[] = $name;
		}
		return $result;
	}
}