<?php
/**
 * Created by PhpStorm.
 * User: Dzhuneyt
 * Date: 28.1.2015 г.
 * Time: 16:58 ч.
 */

class DoceboSimpleBarchart extends CPortlet{

	/**
	 * @var array each element must have the following (keys):
	 * "value" (integer)
	 * "label" (string)
	 */
	public $data = array();

	public $height = 150; // pixels

	protected function renderContent(){
		$highestValue = 0;

		// Reduce the total height of each bar by this factor,
		// so they don't reach the ceiling of their contrainer
		// and there is room for the actual value above the bar
		$weightFactor = 0.8;

		// Figure out the highest value, so we can use
		// it later as a basis while sizing the other
		// bars, relative to the highest one
		foreach($this->data as $item){
			if($item['value'] > $highestValue){
				$highestValue = $item['value'];
			}
		}

		foreach($this->data as &$item){
			$item['height'] = $highestValue==0 || $item['value']==0 ? 0 : $weightFactor*$this->height/$highestValue*$item['value'];
		}

		$this->render('docebo_simple_barchart');
	}

}