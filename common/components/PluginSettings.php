<?php

/**
 * Allows saving/retrieving simple (serialazable) settings
 * specific to LMS plugins/apps
 *
 * Usage:
 *
 * Get a plugin specific value:
 * PluginSettings::get('some_preference_name', 'MyCoolApp');
 *
 * Set/save a plugin specific value:
 * PluginSettings::save('some_preference_name', $value, 'MyCoolApp');
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class PluginSettings extends CComponent {


	static public function get ( $settingName, $appName = NULL ) {
		$appName = $appName ? $appName : self::_guessAppNameFromCurrentContext();
		if( ! $appName ) {
			throw new CException( __METHOD__ . '() - ' . 'Can not get plugin settings. Unknown plugin.' );
		}

		$settings = self::loadSettings();

		$plugin = isset( $settings[ $appName ] ) ? $settings[ $appName ] : NULL;

		if( $plugin ) {
			/* @var $plugin CorePlugin */
			if( isset( $plugin->settings[ $settingName ] ) ) {
				return $plugin->settings[ $settingName ];
			}
		}

		return FALSE;
	}

	static public function save ( $settingName, $value, $appName = NULL ) {
		$appName = $appName ? $appName : self::_guessAppNameFromCurrentContext();
		if( ! $appName ) {
			throw new CException( __METHOD__ . '() - ' . 'Can not save plugin settings for unknown plugin. Please, specify a plugin name. ' );
		}

		$pluginModels = self::loadSettings();

		if( isset( $pluginModels[ $appName ] ) ) {

			$plugin = &$pluginModels[ $appName ];
			/* @var $plugin CorePlugin */

			/*
			 * Note from Dzhuneyt: For some reason we can't directly
			 * set the value of $plugin->settings[$settingName]=$value;
			 * so we first get the original array, add a new key/value pair
			 * and set the new array as model property. A bit of a workaround
			 * until Yii fixes it
			 */
			$oldSettingsArr                 = $plugin->settings;
			$oldSettingsArr[ $settingName ] = $value;
			$plugin->settings               = $oldSettingsArr;

			if( $plugin->save() ) {
				return TRUE;
			} else {
				Yii::log( print_r( $plugin->getErrors(), 1 ), CLogger::LEVEL_ERROR );

				return FALSE;
			}
		}

		return FALSE;
	}

	/**
	 * @return CorePlugin[] models for plugins, whose $model->settings array you can ready/modify
	 */
	static private function loadSettings () {
		static $settings = NULL;

		if( $settings === NULL ) {
			$criteria        = new CDbCriteria();
			$criteria->index = 'plugin_name'; // use 'plugin_name' as array key
			$settings        = CorePlugin::model()->findAll( $criteria );
		}

		return $settings;
	}

	/**
	 * Try to "guess" the app name from the current controller module
	 * The module usually corresponds to the first part of the URL path
	 * e.g. "ApiApp" if the URL is "?r=ApiApp/SomeController/settings"
	 */
	private static function _guessAppNameFromCurrentContext () {
		if( Yii::app()->getController() && ( $module = Yii::app()->getController()->getModule() ) ) {
			$guessedPluginName = $module->getId();
			if( stripos( $guessedPluginName, 'App' ) !== FALSE ) { // "app" modules usually end with "App", e.g. XxxxApp
				return $guessedPluginName;
			}
		}

		return FALSE;
	}
}