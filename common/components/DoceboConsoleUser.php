<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 3/11/2016
 * Time: 10:09
 *
 *
 * This class is used in Console Application. Its used like DoceboWebUser, but not relaying on SESSIONS
 *
 * Useful thing is that you can change the "current" user at any moment you want with Yii::app()->user->setUser(<idst>);
 */
class DoceboConsoleUser extends DoceboWebUser {

    private $_id;
    private $_idst;
    private $_username;

    public function getState($key, $defaultValue = null)
    {
        if(isset($this->$key)){
            return $this->$key;
        } else{
            return $defaultValue;
        }
    }

    public function getStateKeyPrefix()
    {
        return '_';
    }

    public function setState($key, $value, $defaultValue = null)
    {
        $prefixedKey = $this->getStateKeyPrefix() . $key;
        $this->$prefixedKey = $value;
    }

    public function setId($value)
    {
        $this->_id = $value;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getIsGuest()
    {
        return false;
    }

    public function init()
    {
        $anonymousUser = CoreUser::model()->findByAttributes(array(
            'userid' => '/Anonymous'
        ));

        if($anonymousUser){
            $this->setId($anonymousUser->idst);
        }
    }

    public function setLastEnter($lastenter)
    {
        return false;
    }

    public function updateLastEnter()
    {
        return false;
    }

    public function login($identity, $duration = 0)
    {
        return true;
    }

    public function parentLogin($identity, $duration = 0)
    {
        return $this->login($identity, $duration);
    }

    public function oldLmsUserSignIn($prefix = 'public_area')
    {
        return false;
    }

    public function isFirstLogin()
    {
        return false;
    }

    public function checkOldLmsSession($prefix = 'public_area')
    {
        return false;
    }

    public function checkLightAuth()
    {
        return array(
            'success' 	=> false,
            'message'	=> 'User already authenticated.',
        );
    }

    public function setUser($idst){
        $this->setId($idst);
        Docebo::resolveLanguage();
    }

    public function checkAccess($operation,$params=array(),$allowCaching=true)
    {
        return Yii::app()->authManager->checkAccess($operation,$this->getId(),$params);
    }
}