<?php

class DoceboTheme extends CTheme {

	protected $_fileDir;
	protected $_fileName = 'custom.css';
	protected $_filePath;

	public function getFileDir() {
		return $this->_fileDir;
	}

	public function getFileName() {
		return $this->_fileName;
	}

	public function __construct($name, $basePath, $baseUrl) {
		parent::__construct($name, $basePath, $baseUrl);

		$this->_fileDir = Docebo::filesPathAbs();
		$this->_filePath = $this->_fileDir . '/' . $this->_fileName;
	}

	public function getLogoUrl($absolute = false, $variant = CoreAsset::VARIANT_ORIGINAL, $masterLogo = false) {
        $src = null;

        // Raise event to let plugins override the default LMS logo
        Yii::app()->event->raise('GetLogoPath', new DEvent($this, array('logo_image_url' => &$src, 'master_logo' => $masterLogo)));

        if(!$src) {

        	// Get logo setting from core_setting table.
        	// It could be:  1) A file name  2) An integer Asset ID
        	// Filename is checked first later
            $companyLogo = Settings::get('company_logo', false);

            // Ultimate default is the "docebo" logo from themes/../images
            $src = Yii::app()->theme->baseUrl . '/images/company_logo.png';

            // We've got some data in settings?
            if (!empty($companyLogo)) {
                // Check if we have a valid file in core_settings for logo
                // LMS 54 backward compatibility.
                // :: Logo file is expected to be in domain's "files" root
                $testLogoFilePath = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . $companyLogo;
                if (is_file($testLogoFilePath)) {
                    $src = Docebo::filesAbsoluteBaseUrl() . "/" . $companyLogo;
                    if ($absolute) {
                    	return Yii::app()->getBaseUrl(true) . '/' . $src;
                    }
                }
                else {
                	// Get Asset URL, but ensure it is not empty (perhaps wrong Asset ID?)
                	$tmpUrl = CoreAsset::url((int) $companyLogo, $variant);
                	if (!empty($tmpUrl)) {
                		$src = $tmpUrl;
                	}
                }
            }
        }

		return $src;
	}

	/**
	 * Returns the url of the favicon.
	 *
	 * @param bool   $showPlatformValue If set to true, it does not allow plugins to override the default favicon
	 * @param string $variant
	 *
	 * @return null|string
	 */
    public function getFaviconUrl($showPlatformValue = false, $variant = CoreAsset::VARIANT_ORIGINAL, $masterFavicon = false) {
        $src = null;

        if(!$showPlatformValue) {
            // Raise event to let plugins override the default favicon
            Yii::app()->event->raise('GetFaviconPath', new DEvent($this, array('favicon_url' => &$src, 'master_favicon' => $masterFavicon)));
        }

        // Did some plugin provide a favicon ?
        if(!$src) {
            // Go with the default setting
            $favicon = CoreSetting::model()->findByPk('favicon');
            if (!empty($favicon)) {
                // Check if we have a valid file in core_settings for favicon
                // Favicon file is expected to be in domain's "files" root
                $testFaviconFilePath = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . $favicon->param_value;
                if (is_file($testFaviconFilePath))
                    $src = Docebo::filesAbsoluteBaseUrl() . "/" . $testFaviconFilePath->param_value;
                else { // favicon as file id from core_file
                	$src = CoreAsset::url($favicon->param_value, $variant);
                }
            }

        }

        // Still no favicon? Use the default theme favicon
        if(!$src) {
        	if(is_file(Yii::app()->theme->baseUrl . '/images/favicon.ico'))
        		$src = Docebo::getRootBaseUrl(TRUE) . Yii::app()->theme->baseUrl . '/images/favicon.ico';
        	else
        		$src = Docebo::getRootBaseUrl(TRUE) . Yii::app()->theme->baseUrl . '/images/favicon.png';
        }

        return $src;
    }

    public function getLoginImageUrl($variant = CoreAsset::VARIANT_SMALL) {

	    $homeLoginImg = trim(Settings::get('home_login_img'));
	    $asset = CoreAsset::model()->findByPk($homeLoginImg);

	    if ($asset) {

		   $homeImageUrl = $asset->getUrl(CoreAsset::VARIANT_ORIGINAL);
	    } else {

		    // Maybe this field consists FILE NAME, instead of asset ID? Get the Home Login Image file name
		   $homeImageFileName = $homeLoginImg;

		    // Check if file exists in known folders and use it
		    $filePathInTheme = Yii::app()->theme->basePath . "/images/login/" .$homeImageFileName;
		    $filePathInFiles = Docebo::filesPathAbs() . "/" .$homeImageFileName;

		    // Try to find the image file in  1) /files/<domain>  2) /themes/spt/images/login
		    // If not found, use the LMS default from Yii config
		    if (is_file($filePathInFiles)) {
			   $homeImageBaseUrl 	= Docebo::filesBaseUrl();
		    }
		    else if (is_file($filePathInTheme)) {
			   $homeImageBaseUrl = Yii::app()->theme->baseUrl . '/images/login';
		    } else {
			   $homeImageBaseUrl 	= Yii::app()->theme->baseUrl . '/images/login';
			   $homeImageFileName 	= Yii::app()->params['defaultHomeImageFileName'];
		    }

		   $homeImageUrl = $homeImageBaseUrl . "/" .$homeImageFileName;
	    }

	    $homeImage = array(
		    'src' => $homeImageUrl,
		    'aspect' => Settings::get('home_login_img_position', 'fill')
	    );
	    // Raise event to let plugins override the default login image
	    Yii::app()->event->raise('GetLoginImage', new DEvent($this, array('login_image' => &$homeImage)));

    	return $homeImage['src'];
    }

	public function renderLogo($variant = CoreAsset::VARIANT_ORIGINAL, $alt = 'Docebo', $master = false) {
		$src = $this->getLogoUrl(false, $variant, $master);
		return CHtml::image($src, $alt);
	}

    public function renderFavicon($showPlatformValue = false, $variant = CoreAsset::VARIANT_ORIGINAL, $alt = null, $master = false) {
        $src = $this->getFaviconUrl($showPlatformValue, $variant, $master);

        return CHtml::image($src, $alt);
    }

	public function renderLoginImage($variant = CoreAsset::VARIANT_SMALL, $alt = 'Docebo') {
		$src = $this->getLoginImageUrl($variant);

		return CHtml::image($src, $alt);
	}

	public function checkCustomCss() {
		return file_exists($this->_filePath) ? $this->_filePath : false;
	}

	public function createCustomCss($cssString) {
		if (is_writable($this->_fileDir)) {
		/* || mkdir($this->_fileDir, 0777, true)*/
			file_put_contents($this->_filePath, $cssString);
			return $this->_filePath;
		}

		return false;
	}

	public function registerCustomCss() {
		if ($this->checkCustomCss()) {
			$assetsUrl = Yii::app()->getAssetManager()->publish($this->_fileDir . '/' . $this->_fileName);
			Yii::app()->clientScript->registerCssFile($assetsUrl);
		}
	}

	public function deleteCustomCss() {
		if ($this->checkCustomCss()) {
			unlink($this->_filePath);
		}
	}

	public function getColors() {
		if(isset(Yii::app()->session['selected_color_scheme']))
			$scheme = CoreScheme::model()->with('colorsSelection.schemeColor')->findByAttributes(array('id' => Yii::app()->session['selected_color_scheme']));
		else
			$scheme = CoreScheme::model()->with('colorsSelection.schemeColor')->enabled()->find();

		if(is_null($scheme))
			$scheme = CoreScheme::model()->with('colorsSelection.schemeColor')->enabled()->find();

		return CoreTemplatecolor::model()->loadColors($scheme->colorsSelection);
	}

	public function getChartColors($order = 'desc') {
		$colors = $this->getColors();

		if ($order == 'desc') {
			return array(
				$colors['@charts3'],
				$colors['@charts2'],
				$colors['@charts1'],
			);
		} else {
			return array(
				$colors['@charts1'],
				$colors['@charts2'],
				$colors['@charts3'],
			);
		}
	}


	/**
	 * Return Absolute URL to the theme, e.g.  http://my.domain.com/themes/spt
	 *
	 * @return string
	 */
	public function getAbsoluteBaseUrl() {
		return Docebo::getRootBaseUrl(true) . "/themes/" . $this->name;
	}



}