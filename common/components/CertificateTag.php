<?php

/**
 * General usage component. Provide Player functionality/services to all parts of the LMS
 *
 * Usage example:
 * 	No matter which part of LMS you are (widget, view, even in authoringm etc.) just check
 *
 * 	if (Yii::app()->player->course) {
 * 		echo Yii::app()->player->course->title  // course is LearningCourse model instance
 *  }
 *
 */
class CertificateTag extends CApplicationComponent {

    /**
     * (non-PHPdoc)
     * @see CApplicationComponent::init()
     */
    public function init() {

    }

    public function getSubstitutionTags() {
        $tagsArray = CMap::mergeArray(
				array('course' => $this->getCourseTags()),
				array('other' => array_merge($this->getMiscTags(), $this->getUserStatTags())),
				array('user' => $this->getUserTags())
		);

        if(PluginManager::isPluginActive('CurriculaApp'))
			$tagsArray = CMap::mergeArray($tagsArray, array('coursepath' => $this->getLearningPlanTags()));

        // Let plugins add their custom tags
        Yii::app()->event->raise("CollectCertificateTags", new DEvent($this, array('tagsArray' => &$tagsArray)));

        return $tagsArray;
    }

    public function getSubstitution($userId, $objectId, $isLearningPlanId = false, $useLastSession = false, $ignoreLastCompleted = false) {
        $substitution_array = CMap::mergeArray(
            (!$isLearningPlanId ? $this->getCourseSubstitution($objectId, $userId, $useLastSession, $ignoreLastCompleted) : $this->getLearningPlanSubstitution($objectId)),
			$this->getMiscSubstitution($userId),
            $this->getUserSubstitution($userId),
            (!$isLearningPlanId ? $this->getUserStatSubstitution($userId, $objectId) : $this->getUserStatLearningPlanSubstitution($userId, $objectId))
        );

        // Let plugins add their custom tag substitutions
        Yii::app()->event->raise("CollectCertificateTagsSubstitution", new DEvent($this, array(
            'substitution_array' => &$substitution_array,
            'user_id' => $userId,
            'object_id' => $objectId,
            'is_learning_plan' => $isLearningPlanId
        )));


		// Check if there is any tag options if is Course
		if($isLearningPlanId == false)
			$substitution_array = self::checkForTagOptions($substitution_array, $objectId, $isLearningPlanId);

        return $substitution_array;
    }

	public static function checkForTagOptions($substitution_array, $objectId, $isLearningPlanId)
	{
		if($isLearningPlanId)
			$object = LearningCoursepath::model()->with('certificate')->findByPk($objectId);
		else
			$object = LearningCourse::model()->with('certificate')->findByPk($objectId);

		if($object !== null){
			$structure = LearningCertificate::model()->findByPk($object->certificate->id_certificate);
			if($structure !== null){
				$html = $structure->cert_structure;
				// Loop all the tags
				foreach($substitution_array as $key => $dateTag){
					$strippedKey = str_replace(array("[", "]"), '', $key);
					// Build regex to check if the current tag has an option added
					$exp = '#\[('.$strippedKey.')\s+(\w+)\s?[=,:]\s?["\']([\w\W]+)["\']\s?]#Ui';
					preg_match_all($exp, $html, $matches);

					// If there is match we have and options for the current tag in the loop
					if($matches){
						$wholeNewIndex 		= isset($matches[0]) ? $matches[0] : false;
						$newStrippedKey 	= isset($matches[1]) ? $matches[1] : false;
						$operation 			= isset($matches[2]) ? $matches[2] : false;
						$operationFormat 	= isset($matches[3]) ? $matches[3] : false;
						// Check if all the needed values are available
						if(!empty($wholeNewIndex) && !empty($newStrippedKey) && !empty($operation) && !empty($operationFormat)) {
							foreach($wholeNewIndex as $i => $tag){
								$originalValue = $substitution_array[$key]; // Example: Jan/2/2015
								if($originalValue){
									$substitution_array[$tag] = Yii::app()->localtime->convertLocalDateToFormat($originalValue, $operationFormat[$i]);
								}else{
									$substitution_array[$tag] = '';
								}
							}
						}
					}
				}
			}
		}
		return $substitution_array;
	}

    /*****************************************************************************************************************/

    protected function getCourseTags() {
		$tags = array(
            '[course_code]' => Yii::t('standard', '_COURSE_CODE'),
            '[course_name]' => Yii::t('standard', '_COURSE_NAME'),
            '[course_description]' => Yii::t('certificate', '_COURSE_DESCRIPTION'),
            '[date_begin]' => Yii::t('certificate', '_COURSE_BEGIN'),
            //'[date_begin_en]' => Yii::t('certificate', '_COURSE_BEGIN'),
            '[date_end]' => Yii::t('certificate', '_COURSE_END'),
            //'[date_end_en]' => Yii::t('certificate', '_COURSE_END'),
            '[medium_time]' => Yii::t('course', '_MEDIUM_TIME'),
            '[teacher_list]' => Yii::t('course', '_THEACER_LIST'). " (".Yii::t('standard', '_FIRSTNAME').", ".Yii::t('standard', '_LASTNAME').")",
            '[teacher_list_inverse]' => Yii::t('course', '_THEACER_LIST'). " (".Yii::t('standard', '_LASTNAME').", ".Yii::t('standard', '_FIRSTNAME').")",
            '[course_credits]'	=> Yii::t('standard', '_CREDITS'),
            '[course_certificate_number]' => Yii::t('certificate', 'Course Certificate Number')
		);

		if(PluginManager::isPluginActive('CertificationApp'))
			$tags['[certification_expiration_date]'] = Yii::t('certificate', 'Certification Expiration Date');

		if(PluginManager::isPluginActive('ClassroomApp'))
		{
			$tags['[lt_classroom_name]'] = Yii::t('certificate', 'Session name');
			$tags['[lt_session_date_begin]'] = Yii::t('certificate', 'Session date begin');
			$tags['[lt_session_hour_begin]'] = Yii::t('certificate', 'Session hour begin');
			$tags['[lt_session_date_end]'] = Yii::t('certificate', 'Session date end');
			$tags['[lt_session_hour_end]'] = Yii::t('certificate', 'Session hour end');
			$tags['[lt_session_total_hours]'] = Yii::t('certificate', 'Session total hours');
			$tags['[lt_locations_names]'] = Yii::t('certificate', 'Locations names list');
			$tags['[lt_locations_addresses]'] = Yii::t('certificate', 'Locations addresses list');
			$tags['[lt_locations_telephones]'] = Yii::t('certificate', 'Locations telephones list');
			$tags['[lt_locations_emails]'] = Yii::t('certificate', 'Locations e-mails list');
			$tags['[lt_session_teacher_list]'] = Yii::t('course', '_THEACER_LIST') . " (".Yii::t('standard', '_FIRSTNAME').", ".Yii::t('standard', '_LASTNAME').")";
			$tags['[lt_session_teacher_list_inverse]'] = Yii::t('course', '_THEACER_LIST') . " (".Yii::t('standard', '_LASTNAME').", ".Yii::t('standard', '_FIRSTNAME').")";
		}
		$fields = LearningCourseField::model()->findAll();
		foreach($fields as $field){
			$tags['[coursefield_'.$field->id_field.']'] = $field->getTranslation();
		}
		return $tags;
    }

    protected function getLearningPlanTags() {
        return array(
            '[learning_plan_code]' => Yii::t('certificate', 'Learning Plan Code'),
            '[learning_plan_name]' => Yii::t('certificate', 'Learning Plan Name'),
            '[learning_plan_description]' => Yii::t('certificate', 'Learning Plan Description'),
            '[learning_plan_date_assign]' => Yii::t('certificate', 'Learning Plan Assign Date'),
            '[learning_plan_complete]' => Yii::t('certificate', 'Learning Plan Complete Date'),
            //'[learning_plan_complete_en]' => Yii::t('certificate', 'Learning Plan Complete Date'),
            '[learning_plan_certification_date]' => Yii::t('certificate', 'Learning Plan Certification Date'),
            '[learning_plan_certificate_number]' => Yii::t('certificate', 'Learning Plan Certificate Number'),
            '[learning_plan_credits]' => Yii::t('certificate', 'Learning Plan Credits'),
        );
    }



	protected function getCourseSubstitution($courseId, $userId, $useLastSession = false, $ignoreLastCompleted = false)
	{
		$course = LearningCourse::model()->findByPk($courseId);
		if (empty($course)) {
			return array();
		}

		$courseUsers = LearningCourseuser::model()->with('user')->findAllByAttributes(array('idCourse' => $courseId, 'level' => 6));
		$teacherList = array();
		$teacherListInverse = array();
		if (!empty($courseUsers)) {
			foreach ($courseUsers as $courseUser) {
				$teacherList[] = $courseUser->user->firstname . ' ' . $courseUser->user->lastname;
				$teacherListInverse[] = $courseUser->user->lastname . ' ' . $courseUser->user->firstname;
			}
		}

		$lt_session_name = '';
		$lt_session_date_begin = '';
		$lt_session_hour_begin = '';
		$lt_session_date_end = '';
		$lt_session_hour_end = '';
		$lt_session_total_hours = '';
		$lt_locations_names = '';
		$lt_locations_addresses = '';
		$lt_locations_telephones = '';
		$lt_locations_emails = '';
		$lt_session_teacher_list = '';
		$lt_session_teacher_list_inverse = '';

		if($course->course_type == 'classroom' && PluginManager::isPluginActive('ClassroomApp'))
		{
			$command = Yii::app()->db->createCommand();
			$command->select(LtCourseuserSession::model()->tableName().'.id_session');
			$command->from(LtCourseuserSession::model()->tableName());
			$command->join(LtCourseSession::model()->tableName(), LtCourseSession::model()->tableName().'.id_session = '.LtCourseuserSession::model()->tableName().'.id_session');
			if($ignoreLastCompleted){
				$command->where(LtCourseuserSession::model()->tableName().'.id_user = :id_iser AND '.LtCourseSession::model()->tableName().'.course_id = :id_course', array(':id_iser' => $userId, ':id_course' => $courseId));
			}else{
				$command->where(LtCourseuserSession::model()->tableName().'.id_user = :id_iser AND '.LtCourseSession::model()->tableName().'.course_id = :id_course AND (' .LtCourseuserSession::model()->tableName().'.evaluation_status = :evaluation_status OR '.LtCourseuserSession::model()->tableName().'.status = :ended)', array(':id_iser' => $userId, ':id_course' => $courseId, ':evaluation_status' => LtCourseuserSession::EVALUATION_STATUS_PASSED, ':ended' => LearningCourseuser::$COURSE_USER_END));

			}

			if($useLastSession) {
				$command->order(LtCourseuserSession::model()->tableName() . '.date_completed DESC');
			}

			$id_session = $command->queryScalar();

			if($id_session)
			{
				/* @var $lt_session LtCourseSession */
				$lt_session = LtCourseSession::model()->findByPk($id_session);
				if($lt_session)
				{
					$lt_session_name = $lt_session->name;
					$lt_session_date_begin = Yii::app()->localtime->stripTimeFromLocalDateTime($lt_session->date_begin);
					$lt_session_hour_begin = trim(str_replace($lt_session_date_begin, '', $lt_session->date_begin));
					$lt_session_date_end = Yii::app()->localtime->stripTimeFromLocalDateTime($lt_session->date_end);
					$lt_session_hour_end = trim(str_replace($lt_session_date_end, '', $lt_session->date_end));
					$lt_session_total_hours = $lt_session->total_hours;

					$locations = $lt_session->getLocations();
					$first = true;
					foreach($locations as $location)
					{
						if($first)
							$first = false;
						else
						{
							$lt_locations_names .= ', ';
							if($location['address'] != '' && $lt_locations_addresses != '')
								$lt_locations_addresses .= ', ';
							if($location['telephone'] != '' && $lt_locations_telephones != '')
								$lt_locations_telephones .= ', ';
							if($location['email'] != '' && $lt_locations_emails != '')
								$lt_locations_emails .= ', ';
						}

						$lt_locations_names .= $location['name'];
						if($location['address'] != '')
							$lt_locations_addresses .= $location['address'];
						if($location['telephone'] != '')
							$lt_locations_telephones .= $location['telephone'];
						if($location['email'] != '')
							$lt_locations_emails .= $location['email'];
					}

					//retrieve  teachers list specific for session
					$lt_session_teachers = $lt_session->findInstructors();
					if (is_array($lt_session_teachers)) {
						$lt_session_teachers_first = true;
						foreach ($lt_session_teachers as $lt_session_teacher) {
							if ($lt_session_teachers_first) {
								$lt_session_teachers_first = false;
							} else {
								$lt_session_teacher_list .= ', ';
								$lt_session_teacher_list_inverse .= ', ';
							}
							$teacher_fullname = trim($lt_session_teacher->firstname.' '.$lt_session_teacher->lastname);
							$teacher_fullname_inverse = trim($lt_session_teacher->lastname.' '.$lt_session_teacher->firstname);
							$lt_session_teacher_list .= $teacher_fullname;
							$lt_session_teacher_list_inverse .= $teacher_fullname_inverse;
						}
					}
				}
			}
		}

		if($course->course_type == LearningCourse::TYPE_WEBINAR)
		{
			$command = Yii::app()->db->createCommand();
			$command->select(WebinarSessionUser::model()->tableName().'.id_session');
			$command->from(WebinarSessionUser::model()->tableName());
			$command->join(WebinarSession::model()->tableName(), WebinarSession::model()->tableName().'.id_session = '.WebinarSessionUser::model()->tableName().'.id_session');
			$command->where(
				WebinarSessionUser::model()->tableName().'.id_user = :id_iser AND '.WebinarSession::model()->tableName().'.course_id = :id_course AND (' .WebinarSessionUser::model()->tableName().'.evaluation_status = :evaluation_status OR '.WebinarSessionUser::model()->tableName().'.status = :ended)',
				array(
					':id_iser' => $userId,
					':id_course' => $courseId,
					':evaluation_status' => WebinarSessionUser::EVALUATION_STATUS_PASSED,
					':ended' => LearningCourseuser::$COURSE_USER_END
				));
			if($useLastSession) {
				$command->order(WebinarSessionUser::model()->tableName() . '.date_completed DESC');
			}

			$id_session = $command->queryScalar();

			if($id_session)
			{
				/* @var $lt_session WebinarSession */
				$lt_session = WebinarSession::model()->findByPk($id_session);
				if($lt_session)
				{
					$lt_session_name = $lt_session->name;
					$lt_session_date_begin = Yii::app()->localtime->stripTimeFromLocalDateTime(WebinarSession::getStartDate($id_session));
					$lt_session_hour_begin = trim(str_replace($lt_session_date_begin, '', WebinarSession::getStartDate($id_session)));
					$lt_session_date_end = Yii::app()->localtime->stripTimeFromLocalDateTime(WebinarSession::getEndDate($id_session));
					$lt_session_hour_end = trim(str_replace($lt_session_date_end, '', WebinarSession::getEndDate($id_session)));
					$lt_session_total_hours = $lt_session->getTotalHours();

					//retrieve  teachers list specific for session
					$lt_session_teachers = $lt_session->findInstructors();
					if (is_array($lt_session_teachers)) {
						$lt_session_teachers_first = true;
						foreach ($lt_session_teachers as $lt_session_teacher) {
							if ($lt_session_teachers_first) {
								$lt_session_teachers_first = false;
							} else {
								$lt_session_teacher_list .= ', ';
								$lt_session_teacher_list_inverse .= ', ';
							}
							$teacher_fullname = trim($lt_session_teacher->firstname.' '.$lt_session_teacher->lastname);
							$teacher_fullname_inverse = trim($lt_session_teacher->lastname.' '.$lt_session_teacher->firstname);
							$lt_session_teacher_list .= $teacher_fullname;
							$lt_session_teacher_list_inverse .= $teacher_fullname_inverse;
						}
					}
				}
			}
		}

		$courseFields = $course->getCourseAdditionalFields();
		$courseAdditionalFields = array();
		foreach($courseFields as $field){
			$courseAdditionalFields['[coursefield_'.$field->id_field.']'] = $field->getFieldEntry($course->idCourse);
		}
		$mainSubstitutions = array(
			'[course_code]' => $course->code,
			'[course_name]' => $course->name,
			'[course_description]' => html_entity_decode(strip_tags(nl2br($course->description), '<ul><ol><li><br>'), ENT_QUOTES, "UTF-8"),
			'[date_begin]' => ($course->date_begin && $course->date_begin != '0000-00-00') ? $course->date_begin : '',
			'[date_end]' => ($course->date_end && $course->date_end != '0000-00-00') ? $course->date_end : '',
			'[date_begin_en]' => ($course->date_begin && $course->date_begin != '0000-00-00') ? $course->date_begin : '',
			'[date_end_en]' => ($course->date_end && $course->date_end != '0000-00-00') ? $course->date_end : '',
			'[medium_time]' => $course->formatAverageCourseTime(),
			'[ed_date_begin]'	=> '',
			'[ed_classroom]' => '',

			'[cl_date_begin]'	=> '',
			'[cl_date_end]'	=> '',
			'[cl_classroom]' => '',

			'[lt_classroom_name]' => $lt_session_name,
			'[lt_session_date_begin]' => $lt_session_date_begin,
			'[lt_session_hour_begin]' => $lt_session_hour_begin,
			'[lt_session_date_end]' => $lt_session_date_end,
			'[lt_session_hour_end]' => $lt_session_hour_end,
			'[lt_session_total_hours]' => $lt_session_total_hours,
			'[lt_locations_names]' => $lt_locations_names,
			'[lt_locations_addresses]' => $lt_locations_addresses,
			'[lt_locations_telephones]' => $lt_locations_telephones,
			'[lt_locations_emails]' => $lt_locations_emails,
			'[lt_session_teacher_list]' => $lt_session_teacher_list,
			'[lt_session_teacher_list_inverse]' => $lt_session_teacher_list_inverse,

			'[teacher_list]' => implode(', ', $teacherList),
			'[teacher_list_inverse]' => implode(', ', $teacherListInverse),
			'[course_credits]'	=> $course->credits,
			);
		    return array_merge($courseAdditionalFields, $mainSubstitutions);
		}



    protected function getLearningPlanSubstitution($learningPlanId) {
        $learningPlan = LearningCoursepath::model()->findByPk($learningPlanId);
        if (empty($learningPlan)) {
            return array();
        }

        return array(
            '[learning_plan_code]' => $learningPlan->path_code,
            '[learning_plan_name]' => $learningPlan->path_name,
            '[learning_plan_description]' => html_entity_decode(strip_tags(nl2br($learningPlan->path_descr), '<ul><ol><li><br>'), ENT_QUOTES, "UTF-8"),
			'[learning_plan_credits]' => $learningPlan->credits
        );
    }

    /*****************************************************************************************************************/

    protected function getMiscTags() {
        return array(
            '[today]' => Yii::t('calendar', '_TODAY'),
            '[year]' => Yii::t('certificate', '_COURSE_YEAR'),
            '[logo]' => Yii::t('templatemanager', 'Logo'),
        );
    }

    protected function getMiscSubstitution($userId = null) {
        $logoUrl = $this->getUserSpecificLogoUrl($userId);

        return array(
            '[today]' => Yii::app()->localtime->getLocalNow(Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateFormat())),
            '[year]' => Yii::app()->localtime->getLocalNow('Y'),
            '[logo]' => $logoUrl ? '<img src="'.$logoUrl.'">' : null,
        );
    }

    private function getUserSpecificLogoUrl($idUser){

        if(!PluginManager::isPluginActive('DomainBrandingApp')){
            // Use standard logo URL
            return Yii::app()->theme->getLogoUrl();
        }else{
            // We have to rewrite a portion of the DomainBrandingApp logic here
            // since we need to retrieve the branding template (and the logo from
            // that template) for the idUser for which we are generating the certificate
            // for. Just calling Yii::app()->theme->getLogoUrl() will not work here
            // with the DomainBrandingApp enabled, because it will retrieve the branding
            // template for the CURRENT USER, which may be the god admin, issuing the certificate
            // for another user. The user generating the certificate is not always the same
            // as the recipient of the certificate and those two users may have different
            // branding templates (and logos in these templates)

        	// If you can, make the code PHP backward compatible. If you can. In this case, we can.
        	// We get syntax error in release65.docebo.info, because the server is a bit old (PHP 5.3.3).
        	// $host = (new DomainBrandingLib())->getHost();
        	$tmp = new DomainBrandingLib();
            $host = $tmp->getHost();

            $branding = null;

            // Load the domain branding settings based on the custom url, if it is set
            $domainBranding = CoreOrgChartBranding::model()->findByAttributes(array('custom_url'=>$host));

            // If user ID provided, get the branding template specific to that user instead
            // of the current logged in user
            if ($idUser) {
							$orderCriteria = new CDbCriteria();
							$orderCriteria->order = "t.lev DESC, t.iLeft ASC"; //this ordering is needed by search loop below
                /* @var $orgNodesList CoreOrgChartTree[] */
                $orgNodesList = CoreOrgChartTree::model()->getOrgNodesByUserId($idUser, $orderCriteria);
                if (!empty($orgNodesList)) {
                    // Loop through nodes to find the first branding template set
                    foreach ($orgNodesList as $orgChartTree) {
                        /* @var $branding CoreOrgChartBranding */
                        $branding = CoreOrgChartBranding::model()->findByPk($orgChartTree->idOrg);
                        if(!$branding && ($orgChartTree->lev > 1)) {
                            // Search for a template up the hierarchy
                            $ancestors = CoreOrgChartTree::model()->getAncestorsNodes($orgChartTree);
                            foreach($ancestors as $ancestor) {
                                $branding = CoreOrgChartBranding::model()->findByPk($ancestor->idOrg);
                                if ($branding) // Stop at the first valid branding template
                                    break;
                            }
                        }

                        if ($branding) // Stop at the first valid branding template
                            break;
                    }
                }
            }

            // apply domain branding if no custom branding is set for the user's org node
            if (!$branding)
                $branding = $domainBranding;

            if ($branding) {
                // Branding template found (either user's orgchart specific or the fallack LMS wide template)
                return $branding->getLogoUrl();
            }else{
                // Get logo setting from core_setting table.
                // It could be:  1) A file name  2) An integer Asset ID
                // Filename is checked first later
                $companyLogo = Settings::get('company_logo', false);

                // Ultimate default is the "docebo" logo from themes/../images
                $src = Yii::app()->theme->baseUrl . '/images/company_logo.png';

                // We've got some data in settings?
                if (!empty($companyLogo)) {
                    // Check if we have a valid file in core_settings for logo
                    // LMS 54 backward compatibility.
                    // :: Logo file is expected to be in domain's "files" root
                    $testLogoFilePath = Docebo::filesPathAbs() . DIRECTORY_SEPARATOR . $companyLogo;
                    if (is_file($testLogoFilePath)) {
                        $src = Docebo::filesAbsoluteBaseUrl() . "/" . $companyLogo;
                        return Yii::app()->getBaseUrl(true) . '/' . $src;
                    }
                    else {
                        // Get Asset URL, but ensure it is not empty (perhaps wrong Asset ID?)
                        $tmpUrl = CoreAsset::url((int) $companyLogo, CoreAsset::VARIANT_ORIGINAL);
                        if (!empty($tmpUrl)) {
                            return $tmpUrl;
                        }
                    }
                }
            }
        }

        return null;
    }

    /*****************************************************************************************************************/

    protected function getUserTags() {
        $arr = array(
            '[display_name]' => Yii::t('certificate', '_DISPLAY_NAME'),
            '[username]' => Yii::t('standard', '_USERNAME'),
            '[firstname]' => Yii::t('standard', '_FIRSTNAME'),
            '[lastname]' => Yii::t('standard', '_LASTNAME'),
        );

        // Add Additional Fields as placeholders to be used in the Certificate template
        // (each Additional Field has its own placeholder in the format: [userfield_X]
        // where X is the ID of the field from core_field.id_common)
        $fields = CoreUserField::model()->language(Yii::app()->getLanguage())->findAll();

        if($fields){
            $additionalFields = array();
            foreach($fields as $field){
				/** @var CoreUserField $field */
                $additionalFields['[userfield_' . $field->getFieldId() . ']'] = $field->getTranslation();
            }
            if(count($additionalFields)){
                $arr = array_merge($arr, $additionalFields);
            }
        }

        return $arr;
    }

	/**
	 * @param $userId
	 * @return array
	 */
    public function getUserSubstitution($userId) {
        $user = CoreUser::model()->findByPk($userId);

        if (!empty($user)) {
            $arr = array(
                '[display_name]' => $user->firstname.' '.$user->lastname,
                '[username]' => Yii::app()->user->getRelativeUsername($user->userid),
                '[firstname]' => $user->firstname,
                '[lastname]' => $user->lastname,
            );

			$indexCriteria = new CDbCriteria;
			$indexCriteria->index = 'id_field';
			$fields = CoreUserField::model()->language(Yii::app()->getLanguage())->findAll($indexCriteria);
			$userValues = current(CoreUserField::getValuesForUsers([$userId], true));

			foreach ($userValues as $fieldId => $fieldValue) {
				/** @var integer $fieldId */
				/** @var mixed $fieldValue */
				/** @var CoreUserField $field */
				$field = isset($fields[$fieldId]) ? $fields[$fieldId] : null;

				if ($field === null || empty($fieldValue) === true) {
					continue;
				}

				switch($field->getFieldType()){
					case CoreUserField::TYPE_DROPDOWN:
						// Dropdown type Additional Fields should be treated differently
						// the user's entered value should not be treated as a string, but
						// instead as an integer that refers to the child/entry/son of that dropdown
						/** @var FieldDropdown $field */
						$field = new FieldDropdown;
						$field->id_field = $fieldId;
						$userValues[$fieldId] = $field->renderFieldValue($fieldValue);
						break;
					case CoreUserField::TYPE_COUNTRY:
//						// When the additional field is of type 'country' the user's entry is
//						// an integer referring to a country in CoreCountry so get that
						/** @var FieldCountry $field */
						$field = new FieldCountry;
						$field->id_field = $fieldId;
						$userValues[$fieldId] = $field->renderFieldValue($fieldValue);
						break;
					case CoreUserField::TYPE_YESNO:
						// In this case/type of additional field, the user's entry
						// can be either 0 (for no) or 1 (for yes)
						/** @var FieldYesno $field */
						$field = new FieldYesno;
						$field->id_field = $fieldId;
						$userValues[$fieldId] = $field->renderFieldValue($fieldValue);
						break;
					case CoreUserField::TYPE_DATE:
						$local_date = Yii::app()->localtime->toLocalDate($fieldValue);
						$userValues[$fieldId] = $local_date;
						break;
					// Column `user_entry_extra` is not supported in CoreUserField, so we're using the default user input value
					case CoreUserField::TYPE_UPLOAD:
					default:
						// All other additional fields should replace the tag in the certificate
						// with exactly what the user has entered as string
				}
			}

			$formattedKeys = array_combine(
				array_map(function($key) { return '[userfield_' . $key . ']'; }, array_keys($userValues)),
				$userValues
			);

			$arr = array_merge($arr, $formattedKeys);

            return $arr;
        }

        return array();
    }

    /*****************************************************************************************************************/


    protected function getUserStatTags() {

        return array(
            '[date_enroll]' => Yii::t('certificate', '_DATE_ENROLL'),
            //'[date_enroll_en]' => Yii::t('certificate', '_DATE_ENROLL'),
            '[date_first_access]' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
            //'[date_first_access_en]' => Yii::t('standard', '_DATE_FIRST_ACCESS'),
            '[date_complete]' => Yii::t('certificate', '_DATE_COMPLETE'),
            //'[date_complete_en]' => Yii::t('certificate', '_DATE_COMPLETE'),
            '[date_complete_year]' => Yii::t('certificate', '_DATE_COMPLETE'). ' - ' .Yii::t('standard', '_YEAR'),
            '[total_time]' => Yii::t('standard', '_TOTAL_TIME'),
            '[total_time_hour]' => Yii::t('certificate', '_TOTAL_TIME_HOUR'),
            '[total_time_minute]' => Yii::t('certificate', '_TOTAL_TIME_MINUTE'),
            '[total_time_second]' => Yii::t('standard', '_SECONDS'),
			// Deprecated tags (still resolved for old templates -> no more available in new certificates)
            //'[test_score_start]' => Yii::t('certificate', '_TEST_SCORE_START'),
			//'[test_score_start_max]' => Yii::t('certificate', '_TEST_SCORE_START_MAX'),
			//'[test_score_final]' => Yii::t('certificate', '_TEST_SCORE_FINAL'),
			//'[test_score_final_max]' => Yii::t('certificate', '_TEST_SCORE_FINAL_MAX'),
            '[course_score_final]' => Yii::t('standard', '_FINAL_SCORE'),
            '[course_score_final_max]' => Yii::t('certificate', '_COURSE_SCORE_FINAL_MAX'),
        );
    }

    protected function getUserStatSubstitution($userId, $courseId)
    {
		$res = array(
			'[date_enroll]' => '',
			'[date_first_access]' => '',
			'[date_complete]' => '',
			'[date_complete_year]' => '',
			'[total_time]' => '',
			'[total_time_hour]' => '',
			'[total_time_minute]' => '',
			'[total_time_second]' => '',
			'[test_score_start]' => '',
			'[test_score_start_max]' => '',
			'[test_score_final]' => '',
			'[test_score_final_max]' => '',
			'[course_score_final]' => '',
			'[course_score_final_max]' => '',
		);

		$courseUser = LearningCourseuser::model()->findByAttributes(array('idUser' => $userId, 'idCourse' => $courseId));
		if($courseUser)
		{
			$local_format = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'medium'));

			//extract the year from course completion date
			$completeDate = Yii::app()->localtime->fromLocalDateTime($courseUser->date_complete);
			$completeYear = '';
			if ($completeDate) {
				$dt = new DateTime($completeDate);
				$completeYear = $dt->format('Y');
			}
			$res['[date_complete_year]'] = $completeYear;

			//retrieve and convert other dates
			$res['[date_complete_en]'] = $res['[date_complete]'] = $courseUser->date_complete ? Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime(Yii::app()->localtime->convertDateString($courseUser->date_complete, $local_format, DATE_ISO8601)))) : null;
			$res['[date_first_access_en]'] = $res['[date_first_access]'] = $courseUser->date_first_access ? Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime(Yii::app()->localtime->convertDateString($courseUser->date_first_access, $local_format, DATE_ISO8601)))) : null;
			$res['[date_enroll_en]'] = $res['[date_enroll]'] = $courseUser->date_inscr ? Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime(Yii::app()->localtime->convertDateString($courseUser->date_inscr, $local_format, DATE_ISO8601)))) : null;

			if(PluginManager::isPluginActive('CertificationApp')) {

				// @TODO consider using Certification::getCertificationExpiration()

				// query to get the duration of the certificate
				$query="SELECT c.`duration`,c.`duration_unit` FROM
				certification c
				JOIN certification_item ci
				ON c.id_cert = ci.id_cert
				JOIN learning_certificate_course lcc
				ON lcc.id_course = ci.id_item
				JOIN learning_certificate lc
				ON lc.id_certificate = lcc.id_certificate
				WHERE ci.item_type = 'course' AND lcc.`id_course`=:courseId";
				$durationArray = Yii::app()->db->createCommand($query)->queryRow(true, array(':courseId' => $courseId));
				$expirationDate = Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime(Yii::app()->localtime->convertDateString($courseUser->date_complete, $local_format, DATE_ISO8601) . "+ " . $durationArray['duration'] . " " . $durationArray['duration_unit'] . "s")));

				$res['[certification_expiration_date]'] = $expirationDate;
			}
		}

		$usersModel = new CoreUser();
		$criteria = $usersModel->searchCriteria($courseId, true, $userId);
		$users = CoreUser::model()->findAll($criteria);
		if (count($users))
		{
			foreach ($users as $user)
			{
				if($user->idst == $userId)
				{
					$timeSeconds = $user->sessionTime;
					$res['[total_time]'] = Yii::app()->localtime->hoursAndMinutes($timeSeconds);
					$res['[total_time_hour]'] = floor($timeSeconds / 3600);
					$res['[total_time_minute]'] = round(($timeSeconds - (3600*$res['[total_time_hour]'])) / 60);
					$res['[total_time_second]'] = round($timeSeconds - ((3600*$res['[total_time_hour]']) + (60 * $res['[total_time_minute]'])));
					break;
				}
			}
		}

		$start_object = LearningOrganization::model()->findByAttributes(array('idCourse' => $courseId, 'milestone' => 'start'));
		$end_object = LearningOrganization::model()->findByAttributes(array('idCourse' => $courseId, 'milestone' => 'end'));

		if($start_object)
		{
			$user_track = LearningCommontrack::model()->findByAttributes(array('idUser' => $userId, 'idReference' => $start_object->idOrg, 'objectType' => $start_object->objectType));
			if($user_track)
			{
				$res['[test_score_start]'] = $user_track->score;
				$res['[test_score_start_max]'] = $user_track->score_max;
			}
		}

		if($end_object)
		{
			$user_track = LearningCommontrack::model()->findByAttributes(array('idUser' => $userId, 'idReference' => $end_object->idOrg, 'objectType' => $end_object->objectType));
			if($user_track)
			{
				$res['[test_score_final]'] = $user_track->score;
				$res['[test_score_final_max]'] = $user_track->score_max;
			}
		}


	    $userScore = LearningCourseuser::getLastScoreByUserAndCourse($courseId, $userId, true, true);
	    if($userScore && isset($userScore['hasScore']) && $userScore['hasScore'] && isset($userScore['score'])  && isset($userScore['maxScore'])){
		    $res['[course_score_final]'] = $userScore['score'];
		    $res['[course_score_final_max]'] = $userScore['maxScore'];
	    }

		return $res;
    }

    protected function getUserStatLearningPlanSubstitution($userId, $learningPlanId) {
        $res = array(
            '[learning_plan_date_assign]' => ''
        );
        $learningPlanUser = LearningCoursepathUser::model()->findByAttributes(array('idUser' => $userId, 'id_path' => $learningPlanId));
        if ($learningPlanUser) {
	    	$planCompletionDate = LearningCoursepath::getCoursepathCompletionDate($learningPlanId, $userId);
			$res['[learning_plan_date_assign]'] = Yii::app()->localtime->stripTimeFromLocalDateTime($learningPlanUser->date_assign);
   	        if($planCompletionDate)
   			   $res['[learning_plan_complete_en]'] = $res['[learning_plan_complete]'] = Yii::app()->localtime->stripTimeFromLocalDateTime($planCompletionDate);

			if(PluginManager::isPluginActive('CertificationApp')) {
				$query="SELECT c.`duration`,c.`duration_unit` FROM
					certification c
					JOIN certification_item ci
					ON c.id_cert = ci.id_cert
					JOIN learning_certificate_coursepath lccp
					ON lccp.`id_path` = ci.id_item
					JOIN learning_certificate lc
					ON lc.id_certificate = lccp.id_certificate
					WHERE ci.`item_type` = 'plan' AND lccp.`id_path`=:idPlan";

				$local_format = Yii::app()->localtime->YiitoPHPDateFormat(Yii::app()->localtime->getLocalDateTimeFormat('short', 'medium'));
				$durationArray = Yii::app()->db->createCommand($query)->queryRow(true, array(':idPlan'=>$learningPlanId));
				$expirationDate = Yii::app()->localtime->toLocalDate(date('Y-m-d', strtotime(Yii::app()->localtime->convertDateString($planCompletionDate, $local_format, DATE_ISO8601) . "+ " . $durationArray['duration'] . " " . $durationArray['duration_unit'] . "s")));
				$res['[certification_expiration_date]'] = $expirationDate;
			}
	    }

        return $res;
    }

}

?>
