<?php


class VirusScanner extends CComponent {
	
	public $maxStreamSize = 838860800;  
	public $host = "localhost";
	public $port = 3310;
	public $errorMessage = "";
	public $strict = true;
	
	
	
	public function __construct() {
		$this->init();
	}
	
	
	public function init() {
		
		// Read parameters from config
		if (isset(Yii::app()->params['virusscan_host'])) {
			$this->host = Yii::app()->params['virusscan_host'];
		}
		if (isset(Yii::app()->params['virusscan_port'])) {
			$this->port = Yii::app()->params['virusscan_port'];
		}
		if (isset(Yii::app()->params['virusscan_maxsize'])) {
			$this->maxStreamSize = Yii::app()->params['virusscan_maxsize'];
		}
		if (isset(Yii::app()->params['virusscan_strict'])) {
			$this->strict = Yii::app()->params['virusscan_strict'];
		}
		
		
	}
	
	
	
	public function scanFile($fullFilePath) {
		
		$port = $this->port;
		
		Yii::log("Virus Scanner: " . $fullFilePath, CLogger::LEVEL_INFO);
		
		$chunksize = (64 * (1024 * 1024)) - 1;
		
		if (!is_readable($fullFilePath)) {
			Yii::log("Virus Scanner: file is unreadable", CLogger::LEVEL_WARNING);
			return false;
		}
		
		if (filesize($fullFilePath) >= $this->maxStreamSize) {
			Yii::log("Virus Scanner: file is too big", CLogger::LEVEL_WARNING);
			return false;
		}
		
		if (($f = @fopen($fullFilePath, 'rb')) !== false) {
			ob_start();
			if ($socket = @fsockopen($this->host, $port, $errno, $errstr, 5)) {
				@stream_set_timeout($socket, 120);
				@fwrite($socket, "STREAM\n");
				if (!feof($socket)) {
					$port = trim(fgets($socket, 128));
				}
				if (preg_match("/PORT ([\d]{1,5})$/", $port, $matches)) {
					$port = $matches[1];
				}
				ob_end_flush();
				if ($scanner = @fsockopen($this->host, $port, $errno, $errstr, 5)) {
					$i = 0;
					while (!feof($f)) {
						@fwrite($scanner, @fread($f, $chunksize));
					}
					@fclose($scanner);
					@fclose($f);
					$result = '';
					while (!feof($socket)) {
						$result .= @fgets($socket);
					}
					@fclose($socket);
						
					if (preg_match("/: (.*) FOUND.*$/", $result, $found)) {
						$errorMessage = Yii::t('standard','Malware detected in file: {v}', array('{v}'=>$found[1]));
						Yii::log($errorMessage);
						return false;
					}
					elseif (preg_match("/: OK.*$/", $result)) {
						Yii::log("Virus Scanner: File is clean.", CLogger::LEVEL_INFO);
						return true;
					}
					else {
						$errorMessage = Yii::t('standard','Can not scan the file [1]: {v}', array('{v}'=>$fullFilePath));
						Yii::log($errorMessage);
						return !$this->strict;
					}
				}
			}
			else {
				$errorMessage = Yii::t('standard','Can not scan the file [2]: {v}', array('{v}'=>$fullFilePath));
				Yii::log($errorMessage);
				return !$this->strict;
			}
		}
		else {
			$errorMessage = Yii::t('standard','File is unreadable: {v}', array('{v}' => $fullFilePath));
			Yii::log($errorMessage);
			return false;
		}
		
		
	}
	
	
}