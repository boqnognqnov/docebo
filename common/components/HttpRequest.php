<?php

/**
 * HttpRequest
 * @author velislav
 */
class HttpRequest extends CComponent {

    /**
     * Store the URL for processing 
     * @var string 
     */
    public $url = false;

    /**
     * Storing the answer from HTTP Request
     * @var string 
     */
    public $responce = false;

    /**
     * The code of response
     * @var type 
     */
    public $code = false;

    /**
     * Instance for work 
     * @var type 
     */
    public $simpleDom = false;

    function __construct($url = false) {
        Yii::import('common.extensions.simplehtmldom.SimpleHTMLDOM');
        if ($url) {
            $this->url = $url;
            $this->simpleDom = new SimpleHTMLDOM;
            $this->setResponse($this->scrapeHTML($url));
        }
    }

    /**
     * Return given web page HTML as object
     * @param str $url
     * @return boolean/str
     */
    public function scrapeHTML($url) {
        $curlResult = $this->_curl($url);

        if (!$curlResult['errno']) {
            // If everything is fine
            $result = $curlResult['content'];
        } else {
            // If some error
            $result = false;
        }
        return $result;
    }

    /**
     * Get HTML by url
     * @param type $url
     * @return type
     */
    private function _curl($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0", // who am i
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
			// skip SSL certificate check
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;

        return $header;
    }

    /**
     * set and decode into text HTML response from CURL 
     * @return \HttpRequest
     */
    function setResponse($text) {
        $this->responce = $text;
        return $this;
    }

    /**
     * Get nomralized HTML 
     * @return string
     */
    function getResponse() {
        return htmlspecialchars_decode($this->responce);
    }
	
	 /**
     * It returns needed html elements (title, description)
     * @param str $url
     * @return string
     */
    public function getMainElements() {

        $tempElements = array();
        $tempElements['title'] = false;
        $tempElements['description'] = false;
        $tempElements['image'] = false;
        $dom = $this->simpleDom->str_get_html($this->getResponse());
         if ($this->responce !== false) {
            $tempElements = array();

            $tempElements['title'] = $dom->find("title", 0)->plaintext;
            $tempElements['description'] = $dom->find("meta[name='description']", 0)->content;

            // Get temp "Open Graph" elements
            $tempElements['meta_og_title'] = $dom->find("meta[property='og:title']", 0)->content;
            if (!empty($tempElements['meta_og_title'])) {
                $tempElements['title'] = $tempElements['meta_og_title'];
                unset($tempElements['meta_og_title']);
            }

            //directives for grab desc
            $tempElements['meta_og_description'] = $dom->find("meta[property='og:description']", 0)->content;
            if (!empty($tempElements['meta_og_description'])) {
                $tempElements['description'] = $tempElements['meta_og_description'];
                unset($tempElements['meta_og_description']);
            } else if (empty($tempElements['description'])) {
                $tempElements['description'] = false;
            }

            //directives for greab image 
            $tempElements['image'] = $dom->find("meta[property='og:image']", 0)->content;
            if (strlen($tempElements['image']) < 6) {
                $tempElements['image'] = false;
            }
        }
        return $tempElements;
    }
	    

}
