<?php

class Mask {

	/**
	 * Create mask from array integer values
	 * @param $keys
	 * @return int
	 */
	public static function createMask($keys)
	{
		$mask = 0;
		if (is_array($keys))
		{
			foreach ($keys as $key)
			{
				$mask |= ($key >= 0) ? (1 << $key) : 0;
			}
		}
		return $mask;
	}

	/**
	 * Match mask with array values and return only matched values
	 * @param $mask
	 * @param $keys
	 * @return array
	 */
	public static function matchMask($mask, $keys)
	{
		$matches = array();
		foreach ($keys as $key)
		{
			$maskTmp = ($key >= 0) ? (1 << $key) : 0;
			if ($maskTmp & $mask)
				$matches[] = $key;
		}
		return $matches;
	}
}