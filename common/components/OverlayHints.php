<?php

class OverlayHints extends CWidget {

	// comma separated bootstro ids (eg: 'bootstroMarketplace,bootstroAdmin')
	public $hints = '';

    public function init() {
        parent::init();

		$assetsUrl = Yii::app()->theme->baseUrl;

		/* @var CClientScript $cs */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile($assetsUrl . '/js/bootstro/bootstro.css');
		$cs->registerScriptFile($assetsUrl . '/js/bootstro/bootstro.js', CClientScript::POS_END);

		if ($this->hints) {
			$ids = explode(',', $this->hints);
			$jsArr = array();
			foreach ($ids as $id) {
				$jsArr[] = "'".trim($id)."'";
			}
			$this->hints = $jsArr;
		}
    }

    public function run() {
		if (!empty($this->hints)) {
        	$this->render('overlayHints');
		}
    }

}
