<?php

class Field {

	/**
	 * Returns all field types from the 'core_field_type' table (type_field column)
	 * @return array - a plan array of all values from core_field_type.type_field
     * @deprecated Should not be used anymore, if needed should return an array containing values in sync with CoreUserField
	 */
	static public function getAllFieldTypes(){
		$criteria = new CDbCriteria();
		$criteria->select = ' type_field ';
		$types = CoreFieldType::model()->findAll($criteria);

		$ret = array();
		foreach($types as $type){
			$ret[] = $type->type_field;
		}

		return $ret;
	}

	static public function getFieldHTML(CoreUserField $field, $symbolForMandatoryField = '', $inputClass = null, $translationFallback = false) {
		if(!$field) return false;

		$res = '';

		$type_field = $field->getFieldType();
		$id_common = $field->getFieldId();
		$translation = $field->getTranslation();

		//manage translations fallback to default language if actual language translation is empty
		$defaultLanguage = Settings::get('default_language', false);
		if (Yii::app()->session['current_lang'] != $defaultLanguage) {
			if (empty($translation) && $translationFallback && !empty($defaultLanguage)) {
				$df = CoreUserField::model()->findByAttributes(array('id_field' => $id_common));
				if ($df) {
					$translation = $df->getTranslation();
				}
			}
		}

		// The check for those must be done earlier
		$is_modifiable = true;
		$mandatory = $field->mandatory;

		//we get translation from DB so we encode special html chars that user might used
		$translation = CHtml::encode($translation);

		// Opening tags for the additional field's container
		$res .= '<div class="control-group">'
			.'<label class="control-label" for="field_'.$type_field.'_'.$id_common.'">'.$translation. ($mandatory ? ' '.$symbolForMandatoryField: '').'</label>'
			.'<div class="controls">';

		switch($type_field){
			case 'codicefiscale':
				$res .= DoceboForm::textField('field_'.$type_field.'['.$id_common.']', self::getFieldPostValue($type_field, $id_common), array('id'=>'field_'.$type_field.'_'.$id_common, 'class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				break;
			case 'country':
				$countries = self::getCountries();
				$res .= DoceboForm::dropDownList('field_'.$type_field.'['.$id_common.']', self::getFieldPostValue($type_field, $id_common), $countries, array('class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				break;
			case 'date':
				if (!Yii::app()->localtime->forceLongYears()) {
					//trying to prevent short year format to be used in input fields
					switch (LocalTime::DEFAULT_DATE_WIDTH) {
						case LocalTime::LONG: $dateWidth = LocalTime::LONG; break;
						default: $dateWidth = LocalTime::MEDIUM; break;
					}
				} else {
					//long years are forced, all date widths are safe
					$dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
				}
				$dpFormat = Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat($dateWidth));
				$res .= "<div class='input-append date' data-date-format='".$dpFormat."' data-date-language='".Yii::app()->getLanguage()."'>";
				$res .= CHtml::textfield(
					'field_'.$type_field.'['.$id_common.']',
					self::getFieldPostValue($type_field, $id_common),
					array(
						'id' => 'field_'.$type_field.'_'.$id_common,
						'class' => $inputClass,
						'disabled' => (!$is_modifiable?'disabled':''),
						'data-date-format'=> $dpFormat,
						'data-date-language'=> Yii::app()->getLanguage()
					)
				);
				$res .= '<span class="add-on"><i class="icon-calendar"></i></span></div>';
				break;
			case 'dropdown':
				$elems = self::getDropDownElements($id_common);
                natcasesort($elems);
				$res .= DoceboForm::dropDownList('field_'.$type_field.'['.$id_common.']', self::getFieldPostValue($type_field, $id_common), $elems, array('id'=>'field_'.$type_field.'_'.$id_common, 'class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				break;
			case 'freetext':
				$res .= DoceboForm::textArea('field_'.$type_field.'['.$id_common.']', self::getFieldPostValue($type_field, $id_common), array('id'=>'field_'.$type_field.'_'.$id_common, 'class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				break;
			case 'textfield':
				$res .= DoceboForm::textField('field_'.$type_field.'['.$id_common.']', self::getFieldPostValue($type_field, $id_common), array('id'=>'field_'.$type_field.'_'.$id_common, 'class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				break;
			case 'upload':
 				// $res .= DoceboForm::activeFileField(new AdditionalFieldFile(), 'field_'.$type_field.'['.$id_common.']', array('id'=>'field_'.$type_field.'_'.$id_common, 'class'=>$inputClass, 'disabled'=>(!$is_modifiable?'disabled':'')));
				$additionalFieldModel = new AdditionalFieldFile();
				$res .= CHtml::hiddenField('CoreUser[additional]['.$id_common.'][type_field]', $type_field);
				$res .= "<div style='width:220px;' class='".$inputClass."'>" . $additionalFieldModel->getPluploaderHtml('FileUpload['.$id_common.']', 'FileUpload_'.$id_common)  . "</div>";
				break;
			case 'yesno':
				$res .= '<fieldset style="min-width: 220px" class="' . $inputClass . '">';
				$yesChecked = (isset($_POST['field_' . $type_field][$id_common]) && intval($_POST['field_' . $type_field][$id_common]) == 1);
				$res .= DoceboForm::label(
					DoceboForm::radioButton('field_' . $type_field . '[' . $id_common . ']', $yesChecked, array(
						'value' => '1',
						'id' => 'field_' . $type_field . '_' . $id_common . '_1',
						'disabled' => (!$is_modifiable ? 'disabled' : '')
					))
					. ' ' . Yii::t('standard', '_YES'),
					'field_' . $type_field . '_' . $id_common . '_1'
				);

				$noChecked = (isset($_POST['field_' . $type_field][$id_common]) && intval($_POST['field_' . $type_field][$id_common]) == 2);
				$res .= DoceboForm::label(
					DoceboForm::radioButton('field_' . $type_field . '[' . $id_common . ']', $noChecked, array(
						'value' => '2',
						'id' => 'field_' . $type_field . '_' . $id_common . '_2',
						'disabled' => (!$is_modifiable ? 'disabled' : '')
					))
					. ' ' . Yii::t('standard', '_NO'),
					'field_' . $type_field . '_' . $id_common . '_2'
				);
				$res .= DoceboForm::label(
					DoceboForm::radioButton('field_' . $type_field . '[' . $id_common . ']', (!$yesChecked && !$noChecked), array(
						'value' => '0',
						'id' => 'field_' . $type_field . '_' . $id_common . '_0',
						'disabled' => (!$is_modifiable ? 'disabled' : ''),
					))
					. ' ' . Yii::t('standard', '_EMPTY_SELECTION'),
					'field_' . $type_field . '_' . $id_common . '_0',
					// htmloptions:
					array(
						'style' => 'display: none;'
					)
				);
				$res .= '</fieldset>';
				break;
		}

		// Closing tags for the additional field's container
		$res .= '</div>'
			.'</div>';

		return $res;
	}

    /**
     * Helper to set the value of the field after a POST request
     * @param $fieldType
     * @param $fieldId
     * @return string
     */
    static public function getFieldPostValue($fieldType, $fieldId) {
        return isset($_POST['field_'.$fieldType][$fieldId])
            ? $_POST['field_'.$fieldType][$fieldId] : '';
    }

	/**
	 * Store an Additional Field value by its common ID.
	 * Existing user entry will be overwritten, if present.
	 *
	 * Note that for Fields of the type "upload file",
	 * there is a special attribute to be filled - sourceFile,
	 * that is used by beforeSave().
	 *
	 * @see CoreFieldUserentry::beforeSave()
	 *
	 * @param int   $idCommon - The ID of the Field model
	 * @param       $idUser - User's ID for which we are saving the value
	 * @param mixed $value - The actual value that will be saved in CoreFieldUserentry
	 *
	 * @param bool  $forAjax - Is this param even used?
	 *
	 * @return bool - Whether the saving succeeded or not
	 */
	static public function store($idCommon, $idUser, $value, $forAjax=false){
		if(!$idCommon) return FALSE;

		$field = CoreUserField::model()->findByAttributes(array('id_field' => $idCommon));

		if(!$field) return FALSE; // Field not found
		if(!$field->getFieldType()) return FALSE; // Field type unknown

		if ($field->getFieldType() == CoreUserField::TYPE_UPLOAD) {
			// We have a specific case for Fields of the type "file upload".
			// beforeSave() in the model will handle saving this temp file to its
			// corresponding storage. In this case $value passed to this method
			// will the local filename of the uploaded file (in the temp directory)
			$filePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $value;
			if (is_file($filePath)) {
				$value = $filePath;
			}
		}

		return CoreUserField::updateUserValue($idCommon, $idUser, $value);
	}

	/**
	 * Get an array of id_country=>Country name values of all CoreCountry models
	 *
	 * @param bool $includeNoValueFirst - Prepended the array with the
	 * placeholder "- No value -" element. Useful for dropdowns which
	 * should not default to the first available alphabetical country
	 *
	 * @return array
	 */
	static private function getCountries($includeNoValueFirst = true){
		$criteria = new CDbCriteria();
		$criteria->select = ' id_country, name_country ';
		$criteria->index = 'id_country';
		$criteria->order = 'name_country';
		$countries = CoreCountry::model()->findAll($criteria);
		$ret = array();
		if ($countries) {
			if ($includeNoValueFirst && !empty($countries)) {
				$ret[0] = Yii::t('field', '_DROPDOWN_NOVALUE');
			}
			foreach ($countries as $id_country => $country) {
				$ret[$id_country] = $country->name_country;
			}
		}
		return $ret;
	}

	/**
	 * Get the child elements for a dropdown type additional field
	 *
	 * @param int  $idCommon
	 * @param bool $mandatory
	 *
	 * @return array - an associative array of [id]->[translation/name] pairs
	 * of the child elements of the given $idCommon. A blank array if no children.
	 */
	static private function getDropDownElements($idCommon){
		$defaultLanguage = Settings::get('default_language', false);

        // Options list, labels are for the specified language if exist, otherwise - for the default one
        $elems = self::getDropDownElementsInSelLanguage($idCommon,  Yii::app()->session['current_lang']);

		if (!$elems) {
			$elems = array();
		}

		//$options = array_merge(array(Yii::t('field', '_DROPDOWN_NOVALUE')), $options);
		//NOTE: beware array_merge() function, you will lose keys with that !!! use overloaded "+" operator instead !!!
		$elems = array('' => Yii::t('field', '_DROPDOWN_NOVALUE')) + $elems;
		return $elems;
	}

	static private function getDropDownElementsInSelLanguage($idCommon, $lang){
        $dropdown = new FieldDropdown;
        $dropdown->id_field = $idCommon;
        $options = $dropdown->getOptions('bulgarian', true);

		return $options;
	}

	/**
	 * Get and display all additional fields (doceboCore->[gear icon]->Additional Fields)
	 * that are assigned to the Root folder in User Management
	 *
	 * @param string $symbolForMandatoryField - The HTML to be displayed if the
	 * field is mandatory
	 * @param string $inputClass
	 * @param boolean|array $fieldsFilter array of fields ID to use as filter. If not an array, no filter will be applied
	 * @param boolean $returnCount return only the count of the additional fields. Used you you don't want to get rendered fields
	 *
	 * @return string - formatted HTML with all additional fields, assigned to the Root group.
	 */
	static public function getAdditionalFields($symbolForMandatoryField = '', $inputClass = '', $fieldsFilter = false, $returnCount = false)
	{
		$fields = CoreUserField::model()->language(Yii::app()->session['current_lang'], true)->findAllByAttributes(['invisible_to_user' => 0]);

		if (!$fields) { return ''; }

		$extraFieldsHTML = '';
		$count = 0;

		foreach($fields as $row){
			if (!is_array($fieldsFilter) || (is_array($fieldsFilter) && in_array($row->getFieldId(), $fieldsFilter))) {
				$count++;
				if(!$returnCount){
					$extraFieldsHTML .= self::getFieldHTML($row, $symbolForMandatoryField, $inputClass, true);
				}
			}
		}

		if($returnCount){
			return $count;
		}

		return $extraFieldsHTML;
	}

}
