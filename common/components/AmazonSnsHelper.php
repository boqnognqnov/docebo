<?php

/**
 * Utility component providing Amazon SNS functionality (Simple Notification System)
 */
use Aws\ElasticTranscoder\ElasticTranscoderClient,
	Aws\Sns\SnsClient;

class AmazonSnsHelper extends CComponent {

	/**
	 * Factory cache/pool
	 * @var array
	 */
	protected static $cache = array();

	/**
	 * An instance of AWS PHP SDK SNS Client
	 * @var unknown
	 */
	protected $snsClient = null;

	/**
	 * Constructor 
	 * 
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 */
	public function __construct($key, $secret, $region) {
		$config = array(
			'key' => $key,
			'secret' => $secret,
			'region' => $region,
		);
		$this->snsClient = SnsClient::factory($config);
	}

	/**
	 * Return instance of this class. Create new one if not existent.
	 *  
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @return AmazonSnsHelper
	 */
	public static function factory($key, $secret, $region) {
		$cacheKey = implode("_", func_get_args());
		if (isset(self::$cache[$cacheKey])) {
			return self::$cache[$cacheKey];
		}
		self::$cache[$cacheKey] = new self($key, $secret, $region);
		return self::$cache[$cacheKey];
	}

	/**
	 * Subscribe an URL endpoint to a given SNS Topic
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $endpoint The Endpoint to subscribe
	 * @param string $topic SNS Topic ARN (!)
	 * 
	 * @return mixed
	 */
	public static function subscribeUrlToTopic($key, $secret, $region, $endpoint, $topic) {
		$sns = self::factory($key, $secret, $region);
		$protocol = Yii::app()->request->getIsSecureConnection() ? 'https' : 'http';
		$config = array(
			"TopicArn" => $topic,
			"Protocol" => $protocol,
			"Endpoint" => $endpoint,
		);
		$result = $sns->snsClient->subscribe($config);
		if (!$result) {
			return false;
		}

		return $result;
	}

	/**
	 * Un-Subscribe an URL endpoint from a given SNS Topic
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $endpoint The Endpoint to subscribe
	 * @param string $topic SNS Topic ARN (!)
	 *
	 * @return mixed
	 */
	public static function unsubscribeUrlFromTopic($key, $secret, $region, $endpoint, $topic) {
		$sns = self::factory($key, $secret, $region);
		$result = self::isEndpointSubscribedToTopic($key, $secret, $region, $endpoint, $topic);

		Yii::log('Check if URL is subscribed to SNS topic: ', CLogger::LEVEL_INFO);
		Yii::log('TOPIC  ' . $topic, CLogger::LEVEL_INFO);
		Yii::log('URL: ' . $endpoint, CLogger::LEVEL_INFO);
		Yii::log('Result: ' . ($result !== false ? 'Yes' : 'No'), CLogger::LEVEL_INFO);

		if ($result !== false) {
			if ($result['SubscriptionArn'] == 'PendingConfirmation') {
				return true;
			}
			$config = array(
				"SubscriptionArn" => $result['SubscriptionArn'],
			);
			$result = $sns->snsClient->unsubscribe($config);

			Yii::log('Unsubscibe it... ', CLogger::LEVEL_INFO);

			if (!$result) {
				Yii::log('Error unsubscribing ' . $endpoint . ' from ' . $topic, CLogger::LEVEL_ERROR);
			} else {
				Yii::log('Done.', CLogger::LEVEL_INFO);
			}
		}
		return true;
	}

	/**
	 * Return subscription data by URL/Endpoint and TOPIC 
	 * 
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $endpoint The Endpoint to subscribe
	 * @param string $topic SNS Topic ARN (!)
	 * @return array
	 */
	public static function getSubscriptionByEndpointAndTopic($key, $secret, $region, $endpoint, $topic) {
		$result = self::isEndpointSubscribedToTopic($key, $secret, $region, $endpoint, $topic);
		return $result;
	}

	/**
	 * Get List of SNS subscriptions associated to this account. Optionally filter by TOPIC Arn
	 * 
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $topic SNS Topic ARN (!)
	 * 
	 * @return array
	 */
	public static function getSubscriptions($key, $secret, $region, $topic = false) {
		$sns = self::factory($key, $secret, $region);
		$result = array();
		if ($topic != false) {
			$iterator = $sns->snsClient->getListSubscriptionsByTopicIterator(array('TopicArn' => $topic));
		} else {
			$iterator = $sns->snsClient->getListSubscriptionsIterator();
		}
		foreach ($iterator as $item) {
			$result[] = $item;
		}
		return $result;
	}

	/**
	 * Checks if an Endpoint (URL) is subscribed to a given Topic. If YES, returns Subscription Info array.
	 * Otherwise, returns false.
	 *  
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $endpoint The Endpoint to subscribe
	 * @param string $topic SNS Topic ARN (!)
	 * @return array|boolean
	 */
	public static function isEndpointSubscribedToTopic($key, $secret, $region, $endpoint, $topic) {
		$sns = self::factory($key, $secret, $region);
		$iterator = $sns->snsClient->getListSubscriptionsByTopicIterator(array('TopicArn' => $topic));
		foreach ($iterator as $subscription) {
			if ($subscription['Endpoint'] == $endpoint) {
				return $subscription;
			}
		}
		return false;
	}

	/**
	 * Given TopicArn (!) and a Token, confirm a subscription.
	 * Topic and Token are usually received by a request from Amazon during the subscription confirmation stage. 
	 * 
	 * @param string $key AWS key
	 * @param string $secret AWS secret
	 * @param string $region AWS region
	 * @param string $token 
	 * @param string $topic SNS Topic ARN (!)
	 * 
	 * @return array|boolean
	 */
	public static function confirmSubscription($key, $secret, $region, $token, $topic) {
		$sns = self::factory($key, $secret, $region);
		$config = array(
			'TopicArn' => $topic,
			'Token' => $token,
		);
		$result = $sns->snsClient->confirmSubscription($config);
		if (!$result) {
			throw new CException("Error while trying to confirm SNS subscription");
		}
		Yii::log('SNS Subscription to TOPIC ' . $topic . " has been successfully confirmed.", CLogger::LEVEL_INFO);
		return $result;
	}

	/**
	 * URL/Endpoint to our AmazonSnsController that accepts calls/requests from Amazon SNS
	 * @param string $action  Like: "transoding",... (See controller)
	 * @param array $params  GET Parameters 
	 * @return string
	 */
	public static function getSnsTranscodingEndpoint($marker, $collection = '') {
		$url = Docebo::createAbsoluteLmsUrl("amazonSns/transcoding", array('marker' => $marker, 'collection' => $collection));
		return $url;
	}



}
