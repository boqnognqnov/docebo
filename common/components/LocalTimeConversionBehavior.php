<?php
/**
 * LocalTimeConversionBehavior class file
 *
 * Handles conversion in models for timestamp type fields
 */

class LocalTimeConversionBehavior extends CActiveRecordBehavior {

	/**
	 * @var array The list of attributes to be converted to/from timestamp from/to local date/time
	 */
	public $timestampAttributes = array();


	/**
	 * @var array The list of attributes to be converted to/from date from/to local date
	 */
	public $dateAttributes = array();


	/**
	 * @var null this will be used to save formatted date values when while saving the record into DB
	 */
	protected $_savedValues = NULL;


	/**
	 * Convert timestamp attributes to local date/time
	 * @param CEvent $event
	 */
	public function afterFind($event)
	{
		// Fetch "timestamp" attributes from DB schema and override them with model params
		$timestampAttributes = $this->overrideAttributes('timestamp', $this->timestampAttributes);
		foreach ($timestampAttributes as $attr) {
			list($attr, $dateWidth, $timezoneFlag) = array_pad(explode(' ', $attr),3,null);
			if ($this->getOwner()->{$attr}) {
				if ($this->getOwner()->{$attr} === '0000-00-00 00:00:00') {
					// do not display '0000-00-00 00:00:00' to the user
					$this->getOwner()->{$attr} = '';
				} else {
					$this->getOwner()->{$attr} = Yii::app()->localtime->toLocalDateTime($this->getOwner()->{$attr}, $dateWidth, LocalTime::DEFAULT_TIME_WIDTH, null, null, true, ($timezoneFlag == 'keep_utc') ? true : false);
				}
			}
		}

		// Fetch "date" attributes from DB schema and override them with model params
		$dateAttributes = $this->overrideAttributes('date', $this->dateAttributes);
		foreach ($dateAttributes as $attr) {
			list($attr, $dateWidth) = array_pad(explode(' ', $attr),2,null);
			if ($this->getOwner()->{$attr} && ($this->getOwner()->{$attr} !== '0000-00-00')) {
				$this->getOwner()->{$attr} = Yii::app()->localtime->toLocalDate($this->getOwner()->{$attr}, $dateWidth);
			}
		}
	}


	/**
	 * Convert attributes from local date/time to timestamp
	 * @param CModelEvent $event
	 */
	public function beforeSave($event) {

		//prepare internal variable to save actual converted values (they will be restored after saving operation)
		$this->_savedValues = array(
			'timestamp' => array(),
			'date' => array()
		);

		// Fetch "timestamp" attributes from DB schema and override them with model params
		$timestampAttributes = $this->overrideAttributes('timestamp', $this->timestampAttributes);
		foreach ($timestampAttributes as $attr) {
			list($attr, $dateWidth, $timezoneFlag) = array_pad(explode(' ', $attr),3 , null);
			$this->_savedValues['timestamp'][$attr] = $this->getOwner()->{$attr};
			if ($this->getOwner()->{$attr} && ($this->getOwner()->{$attr} !== '0000-00-00 00:00:00')) {
				$this->getOwner()->{$attr} = Yii::app()->localtime->fromLocalDateTime($this->getOwner()->{$attr}, $dateWidth, LocalTime::DEFAULT_TIME_WIDTH, null, ($timezoneFlag == 'keep_utc') ? true : false);
			}
		}

		// Fetch "date" attributes from DB schema and override them with model params
		$dateAttributes = $this->overrideAttributes('date', $this->dateAttributes);
		foreach ($dateAttributes as $attr) {
			list($attr, $dateWidth) = array_pad(explode(' ', $attr), 3, null);
			$this->_savedValues['date'][$attr] = $this->getOwner()->{$attr};
			if ($this->getOwner()->{$attr} && ($this->getOwner()->{$attr} !== '0000-00-00')) {
				$this->getOwner()->{$attr} = Yii::app()->localtime->fromLocalDate($this->getOwner()->{$attr}, $dateWidth);
			}
		}
	}


	/**
	 * Re-convert timestamp attributes to local date/time after having them saved in DB
	 * @param CEvent $event
	 */
	public function afterSave($event) {
		// restore previously saved values
		if (is_array($this->_savedValues)) {

			foreach ($this->_savedValues['timestamp'] as $attr => $value) {
				$this->getOwner()->{$attr} = $value;
			}

			foreach ($this->_savedValues['date'] as $attr => $value) {
				$this->getOwner()->{$attr} = $value;
			}
		}

		//saved values are no more useful now, so kill them
		$this->_savedValues = NULL;
	}


	/**
	 * This will return an array of all the model's date/timestamp attributes
	 * The date/time width of the attribute may be overridden in the model
	 * eg:
	 * ...
	 * 'LocalTimeConversionBehavior' => array(
	 *      'class' => 'common.components.LocalTimeConversionBehavior',
	 *      'timestampAttributes'    => array('date_activated medium keep_utc')
	 *      'dateAttributes'         => array('date_begin short', 'date_end short')
	 * )
	 * ...
	 * @param $type string timestamp|date
	 * @param array $override The timestamp/date attributes array that contain the format modifier (short|medium|long|full)
	 * @return array
	 */
	private function overrideAttributes($type, $override = array()) {

		$returnAttributes = array();

		// KV array to keep the attributes specified in the model
		$overrideMap = array();

		foreach ($override as $attr) {
			$parts = explode(' ', $attr);
			if (count($parts) > 1) {
				// Override DATE WIDTH with the DEFAULT one, used as function's date width parameter default values
				$overrideMap[$parts[0]] = $parts[0] . ' ' . LocalTime::DEFAULT_DATE_WIDTH;
				if (count($parts) > 2)
					$overrideMap[$parts[0]] .= ' ' . $parts[2];
			}
		}

		$schema = $this->getOwner()->tableSchema;
		foreach ($schema->columns as $column) {
			if ($column->dbType === $type) {
				$returnAttributes[] = isset($overrideMap[$column->name])
					? $overrideMap[$column->name]
					: $column->name;
			}
		}

		return $returnAttributes;
	}

}