<?php

/**
 * Widget to load and display courses grid.
 * Internally handles all type of grids, like: Catalog, Mixed, My courses + Catalog, Labels, etc.
 *
 * Renders a view where an AJAX call is made repeatedly (optionally) to load courses (and more courses on scroll, if any).
 * The controller action respnding to that call analyzes the Grid Type requested and other options
 * to return a properly formatted grid.
 *
 * @author Plamen Petkov
 *
 */
class CoursesGrid extends CPortlet {
	/* Options */

	public $gridType = null;

	/* More links for Dropdown menu */
	public $moreDropdownLinks = array();
	// URL to Ajax-called action to load courses grid
	public $coursesGridSourceUrl = '';
	public $useLoadMoreButton = false;
	// Use scroll & load trigger
	public $useScrollAndLoad = false;
	// Sometimes we get these GET parameters to filter by Label or Multicatalog Id
	public $flt_label = '';
	public $flt_catalog = '';
	public $flt_type = '';
	// All possible catalogs
	public static $GRIDTYPE_EXTERNAL = 'external';
	public static $GRIDTYPE_INTERNAL_MYCOURSES = 'mycourses';
	public static $GRIDTYPE_INTERNAL_MYCOURSES_LABELS = 'mycourses_labels';
	public static $GRIDTYPE_INTERNAL_MYCOURSES_ONELABEL = 'mycourses_onelabel';			// "View more..." from labels
	public static $GRIDTYPE_INTERNAL_CATALOG = 'catalog';								 // All Not-subscribed courses
	public static $GRIDTYPE_INTERNAL_MIXED = 'mixed';									 // My courses + Catalog
	public static $GRIDTYPE_INTERNAL_MCATALOG = 'multi_catalog';
	public static $GRIDTYPE_INTERNAL_CATALOG_ONECATALOG = 'catalog_onecatalog';				// "View more..." from Multi catalog
	public static $FILTER_TYPE_PAID = 'paid';
	public static $FILTER_TYPE_FREE = 'free';
	public static $FILTER_TYPE_FULLLIST = 'fulllist';
	public static $FILTER_TYPE_MYCOURSES = 'mycourses';
	public static $FILTER_TYPE_COMPLETED = 'completed';
	public static $FILTER_TYPE_VIEWALL = 'viewall';
	public static $FILTER_TYPE_NOLABELS = 'nolabels';
	public $eCommerceEnabled = false;
	public $catalogEnabled = false;

	/**
	 * (non-PHPdoc)
	 * @see CPortlet::init()
	 */
	public function init() {

		parent::init();

		// Set default grid type
		if (!$this->gridType) {
			$this->gridType = CoursesGrid::$GRIDTYPE_INTERNAL_MYCOURSES;
		}

		// Set default action Url (AJAX called to get actual grid boxes)
		if (empty($this->coursesGridSourceUrl)) {
			$this->coursesGridSourceUrl = 'course/axFilteredCoursesGrid';
		}

		$this->coursesGridSourceUrl = Yii::app()->createAbsoluteUrl($this->coursesGridSourceUrl);

		// If e-commerce is enabled (i.e. to SELL courses)
		$this->eCommerceEnabled = Settings::get("module_ecommerce", "off") == "on";

		// If showing catalog is enabled at all by Apps settings
		//$this->catalogEnabled = Settings::get('catalog_type', 'mycourses') != "mycourses";
		if (PluginManager::isPluginActive('CoursecatalogApp')) {
			$this->catalogEnabled = Settings::get('catalog_type', 'mycourses') != "mycourses";
		} else {
			$this->catalogEnabled = false;
		}

	}

	/**
	 * (non-PHPdoc)
	 * @see CPortlet::renderContent()
	 */
	public function renderContent() {
		$this->render('courses_grid_loader', array());
	}

    public function callBuildTreeData($skipEmptyCategories=true) {
       $treeData = $this->buildTreeData($skipEmptyCategories);

        return $treeData;
    }

    protected function buildTreeData($skipEmptyCategories=true) {
        $_treeData = LearningCourseCategoryTree::getCatalogTreeData();
        if (!empty($_treeData)) {
            //calculate informations about categories: number of catalog courses for each category
            
        	// IF: 1. Anonymous  2. Catalog App enabled  3. External catalog filtration enabled
        	if (Yii::app()->user->isGuest && PluginManager::isPluginActive('CoursecatalogApp') && LearningCatalogue::externalCatalogFiltrationEnabled())  {
        		$this->flt_catalog = LearningCatalogue::getVisiblePublicCatalogs($this->flt_catalog);
        	}
        	
            $_catalogCourses = LearningCatalogue::getCatalogCoursesPage(Yii::app()->user->id, 0, 0, array(
                'catalog' => $this->flt_catalog
            ));
            if (!empty($_catalogCourses)) {

                //find categories root node (NOTE: it MUST be present)
                $_roots = LearningCourseCategoryTree::model()->roots()->findAll();
                if (empty($_roots)) { throw new CException('Invalid categories tree'); }
                $_root = $_roots[0];

                //count courses for every category
                $_countList = array();
                if (!empty($_catalogCourses['courses'])) {
                    foreach ($_catalogCourses['courses'] as $_course) {
                        $_index = ($_course->idCategory <= 0 ? $_root->idCategory : $_course->idCategory); //uncategorized courses are put into root
                        if (!isset($_countList[(int)$_index])) { $_countList[(int)$_index] = 0; }
                        $_countList[(int)$_index]++;
                    }
                }

                //calculate courses in categories tree
                $_countSubNodes = function(&$tree) use (&$_countSubNodes, &$_countList) {
                    $output = 0;
                    if (is_array($tree) && !empty($tree)) {
                        for ($i=0; $i<count($tree); $i++) {
                            $key = (int)$tree[$i]['key'];
                            $output += (isset($_countList[$key]) ? $_countList[$key] : 0);
                            if (!empty($tree[$i]['children'])) { $output += $_countSubNodes($tree[$i]['children']); }
                        }
                    }
                    return $output;
                };
                $_recursiveCalculate = function(&$tree) use (&$_recursiveCalculate, &$_countSubNodes, &$_countList) {
                    if (is_array($tree) && !empty($tree)) {
                        for ($i=0; $i<count($tree); $i++) {
                            $key = (int)$tree[$i]['key'];
                            $count = (isset($_countList[$key]) ? $_countList[$key] : 0);
                            $tree[$i]['data']['courses'] = $count;
                            $tree[$i]['data']['courses_d'] = $count + $_countSubNodes($tree[$i]['children']);
                            if (!empty($tree[$i]['children'])) { $_recursiveCalculate($tree[$i]['children']); }
                        }
                    }
                };
                $_recursiveCalculate($_treeData);

                //clean tree from 0-courses nodes and subnodes
                $_clean = function(&$tree) use (&$_clean) {
                    if (is_array($tree) && !empty($tree)) {
                        for ($i=count($tree)-1; $i>=0; $i--) {
                            if (!$tree[$i]['data']['is_root'] && $tree[$i]['data']['courses_d'] <= 0) {
                                $tree[$i] = NULL;
                                unset($tree[$i]);
                            } else {
                                if (!empty($tree[$i]['children'])) {
                                    $_clean($tree[$i]['children']);
                                }
                            }
                        }
                    }
                };
                
                // Remove categories having NO courses ??
                if ($skipEmptyCategories) $_clean($_treeData);

                // Reorganize missing indexes in children array
                $_rearrangeIndexes = function(&$tree) use (&$_rearrangeIndexes) {
                    if (is_array($tree) && !empty($tree)) {
                        for ($i=count($tree)-1; $i>=0; $i--) {
                            if (!empty($tree[$i]['children'])) {
                                $tree[$i]['children'] = array_values($tree[$i]['children']);
                                $_rearrangeIndexes($tree[$i]['children']);
                            }
                        }
                    }
                };
                $_rearrangeIndexes($_treeData);
            }
        }

        return $_treeData;
    }
}
