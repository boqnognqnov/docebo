<?php 
/**
 * An application component used to preload the My Dashboard module.
 * We need it preloaded, because it must listen for events.
 * 
 * Also, it provides a nice interface to MyDashboard module from all Yii application components.
 * 
 * Like, Yii::app()->mydashboard->enable 
 * 
 */
class MyDashboard extends CApplicationComponent {
	
	/**
	 * Enable/Disable mydashboard, whatever it means.
	 * During the early loading stage (run time) we can enable/disable like this: 
	 * 		Yii::app()->mydashboard->enable = false;
	 * Initially, it is set in main.php and/or main-live.php.
	 * 
	 * @var boolean
	 */
	public $enable = true;
	
	/**
	 * Holds the My Dashboard module instance
	 * 
	 * @var MydashboardModule
	 */
	private $_module = null;
	
	
	/**
	 * Cache (per request) collected dashlet descriptors in this private attribute
	 * @var array
	 */
	private $_dashletDescriptors = false;
	
	
	private $_availableHandlers = false;
	
	/**
	 * @see CApplicationComponent::init()
	 */
	public function init() {
		parent::init();
		$this->_module = Yii::app()->getModule('mydashboard');
	}

	public function getModule(){
		return $this->_module;
	}
	

	/**
	 * Collect all dashlet descriptors by raising an event. 
	 * They should be unique by handler, acroos the whole LMS, including custom plugins!
	 * 
	 * @TODO Do something to prevent descriptors to override each other by HANDLER, e.g. to have two "mydashboard.widgets.DashletExample".
	 * 
	 * 
	 * @return array
	 */
	private function collectDashletDescriptors() {
		// Raise an event to collect dashlet descriptors from all over the LMS (core & plugins' dashlets)
		$descriptors = array();
		Yii::app()->event->raise('CollectMyDashboardDashletWidgets', new DEvent($this, array(
			'descriptors' => &$descriptors,
		)));
		return $descriptors;	
	}
	
	
	/**
	 * Return all dashlet descriptors. Re-collect them if forced. Cache them in private attribute.
	 * 
	 * @param boolean $forceCollect
	 * 
	 * @return array
	 */
	public function getDashletDescriptors($forceCollect=false) {
		if ($this->_dashletDescriptors && !$forceCollect)
			return $this->_dashletDescriptors;

		$this->_dashletDescriptors = $this->collectDashletDescriptors();
		return $this->_dashletDescriptors;
	}
	

	/**
	 * Return a dashlet descriptor, by its "handler" id, if any
	 * 
	 * @param string $handler
	 * 
	 * @return DashletDescriptor
	 */
	public function getDashletDescriptor($handler) {
		$descriptors = $this->getDashletDescriptors();
		$descriptor = false;
		foreach ($descriptors as $d) {
			if ($d->handler == $handler) {
				$descriptor = $d;
				break;
			}
		}
		return $descriptor;
	}
	

	/**
	 * Return array of available dashlet handlers, collected through the "collecting" event
	 * 
	 * @return array
	 */
	public function getAvailHandlers() {
		
		if ($this->_availableHandlers)
			return $this->_availableHandlers;
		$handlers = array();
		$descriptors = $this->getDashletDescriptors();
		foreach ($descriptors as $d) {
			$handlers[] = $d->handler;
		}
		$this->_availableHandlers = $handlers;
		return $this->_availableHandlers;
	}
	
	
	/**
	 * Check if a given handler is available
	 * 
	 * @param string $handler
	 */
	public function isHandlerAvail($handler) {
		return in_array($handler, $this->getAvailHandlers());
	}
	
}