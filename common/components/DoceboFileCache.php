<?php
/**
 * Extending core Yii CFileCache to allow setting default expire period (seconds) in Yii configuration file(s)
 *
 */
class DoceboFileCache extends CFileCache {

	/**
	 * Expire period, seconds, you can set it in Yii config file (main.php)
	 * @var integer
	 */
	public $defaultExpire = 31536000; // one year
	
	/**
	 * @see CFileCache::setValue()
	 */
	protected function setValue($key,$value,$expire=false) {
		if ($expire==false) {
			$expire = $this->defaultExpire;
		}
		return parent::setValue($key, $value, $expire);
	}

	/**
	 * @see CFileCache::addValue()
	 */
	protected function addValue($key, $value, $expire=false) {
		if ($expire==false) {
			$expire = $this->defaultExpire;
		}
		return parent::addValue($key, $value, $expire);
	}

	/**
	 * Sets timestamp in core_setting to invalidate caching on all dockers
	 */
	public function invalidateCacheFiles() {
		Settings::save('messages_cache_validity', time());
	}

	/**
	 * Checks if the cache file for the current key is still valid across dockers
	 * @param $key
	 */
	public function isInvalid($key) {
		$isInvalid = false;
		$cacheValidIfAfter = Settings::get('messages_cache_validity');
		if ($cacheValidIfAfter) {
			$cacheFile = $this->getCacheFile($this->generateUniqueKey($key));
			$lastUpdateTime = $this->filemtime($cacheFile);
			// NOTE: the following patch has been implemented specifically for language cache files, but different kinds of
			// cache files should be handled in their specific way.
			// Yii caching system uses files last update date as expiry date, so if we need specifically the "real" last update
			// date of a file, we have to recalculate it. In the specific case of message files this is obtained by subtracting
			// the "cachingDuration" value from the read timestamp returned by "filemtime" method.
			if (Yii::app()->messages && get_class(Yii::app()->messages) == 'DoceboMessageSource') {
				if (Yii::app()->messages->isMessageCacheFile($key) && Yii::app()->messages->cachingDuration > 0) {
					$lastUpdateTime -= Yii::app()->messages->cachingDuration;
					//alternatively it is possible to do: $cacheValidIfAfter += Yii::app()->messages->cachingDuration;
				}
			}
			// --- end of above patch ---
			$isInvalid = $cacheValidIfAfter && ($lastUpdateTime <= $cacheValidIfAfter);
			if ($isInvalid) {
				Yii::log("Detected invalid cache file for key << " . $key . " >> (file timestamp = ".$lastUpdateTime.", db marker = ".$cacheValidIfAfter.")", CLogger::LEVEL_ERROR);				
			}
		}

		return $isInvalid;
	}

	/**
	 * Returns cache file modification time. {@link $embedExpiry} aware.
	 * @param string $path to the file, modification time to be retrieved from.
	 * @return integer file modification time.
	 */
	protected function filemtime($path)
	{
		if ($this->embedExpiry) {
			return (int)@file_get_contents($path, false, null, 0, 10);
		} else {
			return @filemtime($path);
		}
	}
	
}