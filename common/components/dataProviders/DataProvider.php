<?php

/**
 * An attempt to make a general and somewhat "universal" data provider, 
 * returning a known set of resulting fields, namely (id, name, created).
 * 
 * Work fine with assets so far.
 * 
 * Incoming parameters (passed to constructor), common for all data providers:
 * 
 * @property string $search   
 * @property boolean $knownTotalCount 
 * @property integer $pageSize 
 * @property integer $from  
 * @property bool $idsOnly
 * @property string $language
 * @property boolean $ignoreSuspendedUsers (optional)Ignore suspended users 
 * 
 * Calculated on object creation:
 *  
 * @property array $pagination
 * @property array $languageFallback
 *
 */
class DataProvider extends stdClass {

	/**
	 * Arbitrary array of additional parameters
	 * See this class and its descendants properties 
	 *
	 * @var array
	 */
	protected $_params = array();
	protected $_channelsVisibleToAll;

	/**
	 * Constructor
	 * 
	 * @param array $params = <br>
	 *   <u>Optional, common for all providers:</u><br>
	 * 	 ['search']			=> (string) Search by textual info<br>
	 * 	 ['knownTotalCount']=> (int) If caller knows the number of records in advnace <br>
	 * 	 ['pageSize']		=> (int) Number of items per "page"<br>
	 * 	 ['from']			=> (int) Index in the result set to start from (must be consistent with "pageSize")<br> 
	 * 	 ['idsOnly']		=> (bool) (optional) Return unique asset IDs only<br>  
	 *   ['language']       => (bool) Language, where applicable (en, it, bg, ...)
	 * 	 ['ignoreSuspendedUsers'] => (int) (optional) Ignore suspended user <br>
	 */
	public function __construct($params) {

		// Save incoming parameters to this class variable, just in case we need them again somewhere
		$this->_params = $params;

		// Convert incoming parameters to class variables 
		$this->paramsToAttributes();

		/**
		 * Resolve pagination if not EXPLICITLY set by incoming parameter "pagination" (array)
		 * Remember, this is the default pafination. Once created, Data Provider can be called with different one.
		 * For instance, if you need ALL data, set DP pagination to false.
		 * @see CPagination 
		 */
		if (!isset($this->pagination)) {
			$pageSize = isset($this->pageSize) ? (int) $this->pageSize : Settings::get('elements_per_page', 10);
			$this->pagination = array(
				'pageSize' => (int) $pageSize,
			);
			if (isset($this->from)) {
				$this->pagination["currentPage"] = (int) floor((int) $this->from / $pageSize);
			}
		}

		// Resolve language (en,bg,..)
		if (!$this->language) {
			$this->language = Yii::app()->getLanguage();
		}

		

		$this->languageFallback = Lang::getBrowserCodeByCode(Settings::get('default_language', "english"));
	}

	/**
	 * Convert all incoming parameters to class variables
	 */
	protected function paramsToAttributes() {
		if (is_array($this->_params)) {
			foreach ($this->_params as $attr => $value) {
				$this->$attr = $value;
			}
		}
	}

	/**
	 * "Asset" is a general term and include C&S assets, Courses, Learning Plans, Playlists, etc.
	 * This is to check if an asset of given type exists at all.
	 * 
	 * @param integer $idAsset
	 * @param string $type
	 */
	protected function assetExists($idAsset, $type) {

		$result = false;

		switch ($type) {
			case App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET:
				$result = App7020Assets::model()->exists("id=:id", array(":id" => $idAsset));
				break;

			case App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_PLAYLIST:
				// @TODO For now, just say "Yes, it exists"
				$result = true;
				break;

			case App7020ChannelAssets::ASSET_TYPE_COURSES:
				$result = LearningCourse::model()->exists("idCourse=:id", array(":id" => $idAsset));
				break;

			case App7020ChannelAssets::ASSET_TYPE_LERNING_PLANS:
				$result = LearningCoursepath::model()->exists("id_path=:id", array(":id" => $idAsset));
				break;
		}

		return $result;
	}

	/**
	 * Wrapper, returning full list of channels visible to all, optionally filtered by passed channel(s)
	 */
	protected function getVisibleToAllChannels($idChannel = false) {
		$key = md5(json_encode(func_get_args()));
		if (!isset($this->_channelsVisibleToAll[$key])) {
			$this->_channelsVisibleToAll[$key] = App7020Channels::getVisibleToAllChannels($idChannel);
		}
		return $this->_channelsVisibleToAll[$key];
	}

	/**
	 * Finalize and return Data Command and number of records (counter)
	 *   
	 * @param CDbCommand $command
	 * @return array(CDbCommand, number)
	 */
	protected function finalizeCommand($command, $dpParams) {

		if (isset($this->knownTotalCount) && is_int($this->knownTotalCount)) {
			$numRecords = (int) $this->knownTotalCount;
		} else {
			$commandCounter = Yii::app()->db->createCommand();
			$commandCounter->select('count(*)')->from("(" . $command->text . ") AS baseSQL");
			$numRecords = $commandCounter->queryScalar($dpParams);
		}

		// Final command used to extract data
		$commandData = Yii::app()->db->createCommand();
		$commandData->from("(" . $command->text . ") X");
		if ($this->idsOnly) {
			$commandData->select("id");
		} else {
			$commandData->select("*");
		}

		// All data providers of this family return (id) or (id, name, created) tupples
		// Even if we set idsOnly=true, we still can sort by other fields!
		// This is the default sorting! You can request different one during consequent DataProvider calls
		// Or, if things are part of an HTTP request, pass "sort" GET parameter. Yii Grid View does that automatically for us)
		$sort = new CSort();
		$sort->attributes = array(
			'id' => array(
				'asc' => 'id',
				'desc' => 'id DESC',
			),
			'name' => array(
				'asc' => 'name',
				'desc' => 'name DESC',
			),
			'created' => array(
				'asc' => 'created',
				'desc' => 'created DESC',
			),

		);

		return array($commandData, $numRecords, $sort);
	}

	/**
	 * Add SEARCH filtering to passed CDbCommand
	 *
	 * <i>Note: Modifies $dpParams (data provider level SQL parameters)</i>
	 *
	 * @param CDbCommand $command
	 * @param string $field SQL expression to search IN
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function addSearchFilter(&$command, $field, &$dpParams) {
		if (is_string($this->search) && strlen($this->search) > 0) {
			$command->andWhere("$field LIKE :search");
			$dpParams[":search"] = '%' . strval($this->search) . '%';
		}
	}

	/**
	 * Add USER filtering to passed CDbCommand
	 *
	 * <i>Note: Modifies $dpParams (data provider level SQL parameters)</i>
	 * @param int|array $idUser  ID or IDs
	 * @param CDbCommand $command
	 * @param string $field Field (alias.field)
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function addUserFilter($idUser, &$command, $field, &$dpParams) {
		$this->addIdFilter($idUser, $command, $field, ":idUser", $dpParams);
	}

	/**
	 * Add ASSET filtering to passed CDbCommand
	 *
	 * <i>Note: Modifies $dpParams (data provider level SQL parameters)</i>
	 *
	 * @param int|array $idAsset  ID or IDs
	 * @param CDbCommand $command
	 * @param string $assetTableAlias Asset Table alias
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function addAssetFilter($idAsset, &$command, $field, &$dpParams) {
		$this->addIdFilter($idAsset, $command, $field, ":idAsset", $dpParams);
	}

	/**
	 * General method to add filter to a command, be it an array or scalar value
	 * 
	 * @param int|array $id  ID or array of IDs
	 * @param CDbCommand $command
	 * @param string $field
	 * @param string $placeHolder  SQL parameter, e.g. ":id"
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function addIdFilter($id, &$command, $field, $placeHolder, &$dpParams) {
		if (isset($id)) {
			if (is_array($id) && !empty($id)) {
				$command->andWhere(array("IN", $field, $id));
			} else if (empty($id)) {
				$command->andWhere("FALSE");
			} else {
				$command->andWhere("$field=$placeHolder");
				$dpParams["$placeHolder"] = intval($id);
			}
		}
	}

	/**
	 * General method to add filter to exclude some channels ids
	 * 
	 * @param int|array $idChannel  ID or array of IDs
	 * @param CDbCommand $command
	 * @param string $field
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function igonreChannelFileter($idChannel, &$command, $field, &$dpParams) {
		if (is_array($idChannel) && !empty($idChannel)) {
			$command->andWhere(array("NOT IN", $field, $idChannel));
		} else if (empty($idChannel)) {
			$command->andWhere("FALSE");
		} else {
			$command->andWhere("$field !=:idChannelNOT");
			$dpParams[":idChannelNOT"] = intval($idChannel);
		}
	}

	/**
	 * General method to add filter to exclude some users ids
	 * 
	 * @param int|array $idUser  ID or array of IDs
	 * @param CDbCommand $command
	 * @param string $field
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function igonreUserFileter($idUser, &$command, $field, &$dpParams) {
		if (isset($idUser)) {
			if (is_array($idUser) && !empty($idUser)) {
				$command->andWhere(array("NOT IN", $field, $idUser));
			} else if (empty($idUser)) {
				$command->andWhere("FALSE");
			} else {
				$command->andWhere("$field !=:idChannelNOT");
				$dpParams[":idChannelNOT"] = intval($idUser);
			}
		}
	}

	/**
	 * Add CHANNEL filtering to passed CDbCommand
	 *
	 * <i>Note: Modifies $dpParams (data provider level SQL parameters)</i>
	 * @param int|array $idChannel ID or IDs
	 * @param CDbCommand $command
	 * @param string $field Channel ID field
	 * @param array $dpParams DataProvider bind parameters (note the "&" !)
	 */
	protected function addChannelFilter($idChannel, &$command, $field, &$dpParams) {
		if (isset($idChannel)) {
			if (is_array($idChannel) && !empty($idChannel)) {
				$command->andWhere(array("IN", $field, $idChannel));
			} else if (empty($idChannel)) {
				$command->andWhere("FALSE");
			} else {
				$command->andWhere("$field=:idChannel");
				$dpParams[":idChannel"] = intval($idChannel);
			}
		}
	}

	/**
	 * Factory method to return specific data provider object 
	 * 
	 * @param string $providerClassName One of the descendant classes
	 * @param array $params 
	 * 
	 * @return DataProvider
	 */
	public static function factory($providerClassName, $params) {
		$dp = new $providerClassName($params);
		return $dp;
	}

	/**
	 * The actual data provider<br> 
	 * Must be overriden by descendant classes
	 *  
	 * @return CSqlDataProvider 
	 */
	public function provider() {
		return null;
	}

	/**
	 * Remove suspended or expired users
	 * @param cdbCommand $command
	 * @param string $alias
	 */
	protected function ignoreSuspendedUsersFilter(&$command, $alias) {
		if ($this->ignoreSuspendedUsers) {
			$command->andWhere("$alias.valid=1");
			$command->andWhere("$alias.expiration IS NULL OR $alias.expiration = '0000-00-00' AND $alias.expiration <= NOW()");
		}
	}

}
