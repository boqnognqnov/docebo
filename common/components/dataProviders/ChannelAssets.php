<?php
/**
 * Assets (ID, name, created) members of a given channel
 * 
 * @property integer $idChannel 
 * @property integer $idAsset 
 * @property string $assetType 
 * @property integer $idUser 
 * @property boolean $ignoreInvited
 * @property integer $conversionStatus
 * @property boolean $invitedOnly 
 * 
 * @see ChannelAssets::__construct
 * 
 */
class ChannelAssets extends DataProvider {

	/**
	 * Constructor 
	 *  
	 * @param array $params = <br>
     * 	 ['idChannel']		      => (int) Mandatory Channel ID<br>
     * 	 ['idAsset']		      => (int) (optional) Asset ID to filter by <br>
     * 	 ['assetType']		      => (string) (optional) Asset Type to filter by. Default is Coach & Share asset<br>
     *   ['idUser']			      => (int) (optional)User ID used to do more filtering <br> 
     *   ['ignoreInvited']        => (bool) Ignore assets the user (if provided) is already invited to watch<br>
     *   ['conversionStatus']        => (int) Filter by publish status 
     * 	 ['invitedOnly']          => (bool) ONLY Get assets user (if any) is invited to watch  
     *   <br>
     *   For parmeters, common for all providers: {@see DataProvider::__construct()}
     * 
     * @see DataProvider::__construct()  
     * 
	 * @throws CException
	 */
    public function __construct($params) {
        
        parent::__construct($params);
        
        if (!$this->assetType) {
            $this->assetType = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET; // default is coach & share assets only
        }
        
        // Always be an array! Easier to handle later
        if (isset($this->assetType) && !is_array($this->assetType)) {
            $this->assetType = array($this->assetType);
        }
        
        // By default, filter by published assets
        if (!$this->conversionStatus) {
            $this->conversionStatus = App7020Assets::CONVERSION_STATUS_APPROVED;
        }
        
        if (!$this->idChannel) {
            throw new CException("Invalid channel in data provider");
        }
        
    }
    
    /**
     * Build standard SELECT for THIS provider
     * @param string $assetsAlias
     */
    private function make7020AssetSelect($assetsAlias) {
        $select = array(
            "$assetsAlias.id as id",
            "$assetsAlias.title as name",
            "$assetsAlias.created as created",
            "ca.asset_type as type",
            "ca.idChannel as idChannel"
        );
        return $select;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see DataProvider::provider()
     */
    public function provider() {
        
        $command = Yii::app()->db->createCommand();
         
        // SQL parameters passed to Data Provider at the end
        $dpParams = array();

        $csAssetsSelect = $this->make7020AssetSelect("asset");

        $command->select($csAssetsSelect);
        $command->from("app7020_channel_assets ca");
        $command->join("app7020_content asset", "ca.idAsset=asset.id");
        $this->addAssetFilter($this->idAsset, $command, "ca.idAsset", $dpParams);
        $this->addChannelFilter($this->idChannel, $command, "ca.idChannel", $dpParams);
        $this->addIdFilter($this->assetType, $command, "ca.asset_type", ":asset_type", $dpParams);
        $this->addIdFilter($this->conversionStatus, $command, "asset.conversion_status", ":conversion_status", $dpParams);
        $this->addSearchFilter($command, "CONCAT(COALESCE(asset.title,''), ' ',COALESCE(asset.description,''))", $dpParams);
        
        // Get only assets related to a given user (if any) in this way:
        if ($this->idUser) {
            // ONLY assets user is invited
            if ($this->invitedOnly) {
                $dpParams[":idUser"] = $this->idUser;
                $command->join("app7020_invitations inv", "inv.idContent=asset.id AND inv.idInvited=:idUser");
            }
            // Exclude assets user is invited
            else if ($this->ignoreInvited) {
                $dpParams[":idUser"] = $this->idUser;
                $command->leftJoin("app7020_invitations inv", "inv.idContent=asset.id AND inv.idInvited=:idUser");
                $command->andWhere("inv.id IS NULL");
            }
        }
        
        
        
        
        // FINALIZE   
        list($commandData, $numRecords, $sort) = $this->finalizeCommand($command, $dpParams);
        $sort->defaultOrder = array(
            'created'    => CSort::SORT_DESC,
            'name'       => CSort::SORT_ASC,
        );
         
        // Create data provider and return to caller
        $dataProvider = new CSqlDataProvider($commandData, array(
            'totalItemCount'  => $numRecords,
            'pagination'      => $this->pagination,
            'params'          => $dpParams,
            'sort'            => $sort,
        ));
         
        return $dataProvider;
        
         
    }
    
    
}
