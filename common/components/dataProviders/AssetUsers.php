<?php

/**
 * Users (ID, name, created) who can see given asset(s) of given type
 * 
 * All these attributes (properties) come as $params array elements and are made class variables by the base class constructor
 * 
 * Properties, specific for this DP:
 *   
 * @property integer $idAsset 
 * @property integer|array $idUser 
 * @property string $assetType 
 * @property boolean $ignoreVisallChannels
 * @property boolean $ignoreInvited
 * @property boolean $invitedOnly 
 * @property boolean $ignoreGodAdminEffect
 * @property integer $idChannel 
 * 
 * //custom append criterias to select 
 * @property array $customSelect
 * 
 * // filter about which criteria should be ignore
 * @property integer|array $ignoreUsers
 * @property integer|array $ignoreChannels 
 * 
 * @see AssetUsers::__construct
 * 
 */
class AssetUsers extends DataProvider {

	/**
	 * Constructor 
	 * 
	 * @param array $params = <br>
	 * 	 ['idAsset']		      => (int)  Mandatory Asset ID<br>
	 * 	 ['idChannel']		      => (int) (optional) Channel ID to filter by <br>
	 * 	 ['idUser']		          => (int|array) Possible user ID or IDs for additional filtering  
	 *   ['assetType']		      => (int)  (optional) Could be real coach & share asset, or course, or learning plan, or whatever we call "asset", subject user/channel filtration
	 *   ['ignoreVisallChannels'] => (bool) Ignore users related to channels defined as "visible to all"<br>
	 *   ['ignoreInvited']        => (bool) Ignore users invited to see this asset 
	 *   ['ignoreGodAdminEffect'] => (bool) Ignore GodAdmin users <br>
	 * 	 ['invitedOnly']          => (bool) Get invited users ONLY  
	 * 	 ['ignoreUsers']		      => (int) (optional) User IDs  should be excluded <br>
	 * 	 ['ignoreChannels']		      => (int) (optional) Channel IDs should be excluded<br>
	 * 	 ['ignoreSuspendedUsers'] => (int) (optional) Ignore suspended user <br>
	 *   <br>
	 *   For parmeters, common for all providers: {@see DataProvider::__construct()}
	 * 
	 * @see DataProvider::__construct()  
	 * @throws CException
	 */
	public function __construct($params) {

		parent::__construct($params);

		if (!$this->assetType) {
			$this->assetType = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET; // default is coach & share asset        
		}

		// Invited only means .. invited only
		if ($this->invitedOnly) {
			$this->ignoreGodAdminEffect = true;
		}

		// Make possible channel(s) to array
		if ($this->idChannel) {
			if (!is_array($this->idChannel)) {
				$this->idChannel = array((int) $this->idChannel);
			}
		}

		//we have to set default channel for ignore 
		if (empty($this->ignoreChannels)) {
			$this->ignoreChannels = array(-1);
		}

		//we have to set default  ignore of user
		if (empty($this->ignoreUsers)) {
			$this->ignoreUsers = array(-1);
		}

		// we have to set default value for exclude invalid users 
		if (!isset($this->ignoreSuspendedUsers)) {
			$this->ignoreSuspendedUsers = false;
		}


		if (!$this->idAsset) {
			throw new CException("Invalid Asset ID in data provider");
		}
	}

	/**
	 * Build standard SELECT for THIS provider
	 * @param string $coreUserAlias
	 */
	private function makeSelect($coreUserAlias) {
		$select = array(
			"$coreUserAlias.idst as id",
			"CONCAT($coreUserAlias.firstname, ' ',$coreUserAlias.lastname) AS `name`",
			"$coreUserAlias.register_date as created"
		);

		if (!empty($this->customSelect) && is_array($this->customSelect)) {
			foreach ($this->customSelect as $selectField => $selectAs) {
				$select_area = str_replace('#alias#.', $coreUserAlias . '.', $selectField) . ' as ' . $selectAs;
				$select[] = $select_area;
			}
		}
		return $select;
	}

	/**
	 * Add JOIN to ignore users already invited to see the asset
	 * 
	 * @param CDbCommand $command
	 * @param string $idUserField
	 * @param string $alias
	 */
	private function ignoreInvitedJoin(&$command, $idUserField, $alias = false) {
		static $index = 1;
		if ($this->ignoreInvited) {
			if ($alias === false) {
				$alias = "i_n_v_" . $index;
				$index++;
			}
			$command->leftJoin("app7020_invitations $alias", "$alias.idContent=:idAsset AND $idUserField=$alias.idInvited");
			$command->andWhere("$alias.idContent IS NULL");
		}
	}

	/**
	 * 
	 * @staticvar int $index
	 * @param type $command
	 * @param type $idUserField
	 * @param string $alias
	 */
	private function invitedOnlyJoin(&$command, $idUserField, $alias = false) {
		static $index = 1;
		if ($this->invitedOnly) {
			if ($alias === false) {
				$alias = "i_n_v_io_" . $index;
				$index++;
			}
			$command->join("app7020_invitations $alias", "($alias.idInvited=$idUserField AND $alias.idContent=:idAsset)");
		}
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see DataProvider::provider()
	 */
	public function provider() {

		$command = Yii::app()->db->createCommand();

		// SQL parameters passed to Data Provider at the end
		$dpParams = array(
			':idAsset' => $this->idAsset,
		);

		// Check if asset of the given type exists at all.
		// If not, return data provider with empty set of data
		if (!$this->assetExists($this->idAsset, $this->assetType)) {
			$dataProvider = new CSqlDataProvider("SELECT 1 FROM core_user WHERE FALSE");
			return $dataProvider;
		}


		// 1. All God admins can see ANY asset
		$command->select($this->makeSelect("cu1"));
		$command->from("core_group_members cgm");
		$command->join("core_group cg", "cg.idst=cgm.idst AND cg.groupid='/framework/level/godadmin'");
		$command->join("core_user cu1", "cgm.idstMember=cu1.idst");
		// Add some filtering, if any
		$this->addSearchFilter($command, "CONCAT(COALESCE(cu1.firstname,''), ' ',COALESCE(cu1.lastname,''), ' ',COALESCE(cu1.userid,'') )", $dpParams);
		$this->addUserFilter($this->idUser, $command, "cu1.idst", $dpParams);
		$this->ignoreInvitedJoin($command, "cu1.idst");
		$this->igonreUserFileter($this->ignoreUsers, $command, "cu1.idst", $dpParams);
		$this->ignoreSuspendedUsersFilter($command, "cu1");
		// If channel filtering is requested, god-admin-effect must be ignored?

		if ($this->ignoreGodAdminEffect || $this->invitedOnly) {
			$command->andWhere("FALSE");
		}

		// 2. All INVITED users can see it
		if (!$this->ignoreInvited) {
			$commandInvited = Yii::app()->db->createCommand();
			$commandInvited->select($this->makeSelect("cu2"));
			$commandInvited->from("app7020_invitations inv");
			$commandInvited->join("core_user cu2", "inv.idInvited=cu2.idst");
			$commandInvited->andWhere("inv.idContent = :idAsset");
			if (isset($this->idChannel)) {
				$commandInvited->join("app7020_channel_assets ca", "ca.idAsset=inv.idContent");
			}
			// Add some filtering, if any
			$this->addSearchFilter($commandInvited, "CONCAT(COALESCE(cu2.firstname,''), ' ',COALESCE(cu2.lastname,''), ' ',COALESCE(cu2.userid,''))", $dpParams);
			$this->addUserFilter($this->idUser, $commandInvited, "cu2.idst", $dpParams);
			$this->igonreUserFileter($this->ignoreUsers, $commandInvited, "cu2.idst", $dpParams);
			$this->addChannelFilter($this->idChannel, $commandInvited, "ca.idChannel", $dpParams);
			$this->ignoreSuspendedUsersFilter($commandInvited, "cu2");

			$command->union($commandInvited->text);
		}


		// 3. Users seeing asset through Group/Node/Branch assignment and Channel Visibility rules
		// First, we get all channels this asset is in
		$c = Yii::app()->db->createCommand();
		$c->select("ca.idChannel AS id");
		$c->from("app7020_channel_assets ca");

		$tmpParams = array();
		$tmpParams[":idAsset"] = $this->idAsset;
		if (is_array($this->assetType)) {
			$c->andWhere(array("IN", "ca.asset_type", $this->assetType));
		} else {
			$c->andWhere("ca.asset_type=:assetType");
			$tmpParams[":assetType"] = $this->assetType;
		}
		$c->andWhere("ca.idAsset=:idAsset");
		$assignedChannels = $c->queryColumn($tmpParams);

		// If filtering by channel is enebled, get common elements between assigned and desired (by filter) channels
		// Note, idChannel is converted to array. always, in __construct())
		if ($this->idChannel) {
			$assignedChannels = array_intersect($assignedChannels, $this->idChannel);
		}

		// Finally, Remove VisAll channels if they must be ignored
		if ($this->ignoreVisallChannels) {
			$allChannelsVisibleToAll = $this->getVisibleToAllChannels();
			$assignedChannels = array_diff($assignedChannels, $allChannelsVisibleToAll);
		}



		if (is_array($assignedChannels) && !empty($assignedChannels)) {
			// Check if ANY of the assigned channels is VISIBLE TO ALL and if we are NOT ignoring VISALL channels...
			if (!$this->ignoreVisallChannels && !empty($this->getVisibleToAllChannels($assignedChannels))) {
				$c = Yii::app()->db->createCommand();
				$c->select($this->makeSelect("cuva"));
				$c->from("core_user cuva");
				$c->andWhere("cuva.userid <> '/Anonymous'");
				// Add some filtering, if any
				$this->addSearchFilter($c, "CONCAT(COALESCE(cuva.firstname,''), ' ',COALESCE(cuva.lastname,''), ' ',COALESCE(cuva.userid,''))", $dpParams);
				$this->addUserFilter($this->idUser, $c, "cuva.idst", $dpParams);
				$this->ignoreInvitedJoin($c, "cuva.idst");
				$this->invitedOnlyJoin($c, "cuva.idst");
				$this->igonreUserFileter($this->ignoreUsers, $c, "cuva.idst", $dpParams);
				$this->ignoreSuspendedUsersFilter($c, "cuva");
				$command->union($c->text);
			} else {
				// Collect Users from groups assigned to channels
				$c = Yii::app()->db->createCommand();
				$c->select($this->makeSelect("cu3"));
				$c->from("app7020_channel_visibility cv");
				$c->join("core_group_members cgm", "cgm.idst=cv.idObject AND cv.type=1");
				$c->join("core_user cu3", "cgm.idstMember=cu3.idst");
				$c->andWhere(array("IN", "cv.idChannel", $assignedChannels));
				$c->andWhere(array("NOT IN", "cv.idChannel", $this->ignoreChannels));
				// Add some filtering, if any
				$this->addSearchFilter($c, "CONCAT(COALESCE(cu3.firstname,''), ' ',COALESCE(cu3.lastname,''), ' ',COALESCE(cu3.userid,''))", $dpParams);
				$this->addUserFilter($this->idUser, $c, "cu3.idst", $dpParams);
				$this->ignoreInvitedJoin($c, "cu3.idst");
				$this->invitedOnlyJoin($c, "cu3.idst");
				$this->igonreUserFileter($this->ignoreUsers, $c, "cu3.idst", $dpParams);
				$this->ignoreSuspendedUsersFilter($c, "cu3");
				$command->union($c->text);

				// Collect users from Org Chart NODES (single node, no descendants)
				$c = Yii::app()->db->createCommand();
				$c->select($this->makeSelect("cu4"));
				$c->from("app7020_channel_visibility cv");
				$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
				$c->join("core_group_members cgm", "(cgm.idst=tree.idst_oc)");
				$c->join("core_user cu4", "cgm.idstMember=cu4.idst");
				$c->andWhere(array("IN", "cv.idChannel", $assignedChannels));
				$c->andWhere(array("NOT IN", "cv.idChannel", $this->ignoreChannels));
				// Add some filtering, if any
				$this->addSearchFilter($c, "CONCAT(COALESCE(cu4.firstname,''), ' ',COALESCE(cu4.lastname,''), ' ',COALESCE(cu4.userid,''))", $dpParams);
				$this->addUserFilter($this->idUser, $c, "cu4.idst", $dpParams);
				$this->ignoreInvitedJoin($c, "cu4.idst");
				$this->invitedOnlyJoin($c, "cu4.idst");
				$this->igonreUserFileter($this->ignoreUsers, $c, "cu4.idst", $dpParams);
				$this->ignoreSuspendedUsersFilter($c, "cu4");
				$command->union($c->text);

				// Collect users from Org Chart BRANCHES (node and descendants)
				$c = Yii::app()->db->createCommand();
				$c->select($this->makeSelect("cu5"));
				$c->from("app7020_channel_visibility cv");
				$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
				$c->join("core_org_chart_tree tree2", "(tree2.iLeft >= tree.iLeft) AND (tree2.iRight <= tree.iRight )");
				$c->join("core_group_members cgm", "(cgm.idst=tree2.idst_oc)");
				$c->join("core_user cu5", "cgm.idstMember=cu5.idst");
				$c->andWhere(array("IN", "cv.idChannel", $assignedChannels));
				$c->andWhere(array("NOT IN", "cv.idChannel", $this->ignoreChannels));
				$c->andWhere("cv.selectState=2");
				// Add some filtering, if any
				$this->addSearchFilter($c, "CONCAT(COALESCE(cu5.firstname,''), ' ',COALESCE(cu5.lastname,''), ' ',COALESCE(cu5.userid,''))", $dpParams);
				$this->addUserFilter($this->idUser, $c, "cu5.idst", $dpParams);
				$this->ignoreInvitedJoin($c, "cu5.idst");
				$this->invitedOnlyJoin($c, "cu5.idst");
				$this->igonreUserFileter($this->ignoreUsers, $c, "cu5.idst", $dpParams);
				$this->ignoreSuspendedUsersFilter($c, "cu5");
				$command->union($c->text);
			}
		}

		// FINALIZE   
		list($commandData, $numRecords, $sort) = $this->finalizeCommand($command, $dpParams);
		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);


		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, array(
			'totalItemCount' => $numRecords,
			'pagination' => $this->pagination,
			'params' => $dpParams,
			'sort' => $sort,
		));

		return $dataProvider;
	}

}
