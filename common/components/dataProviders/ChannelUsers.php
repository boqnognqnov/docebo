<?php

/**
 * 
 * Get list of users who can see a given channel (or channels)
 * 
 * @property integer $idChannel 
 * @property integer|array $idUser 
 * @property integer|array $ignoreUsers 
 * @property boolean $ignoreVisallChannels
 * @property boolean $ignoreGodAdminEffect
 * @property boolean $ignoreSuspendedUsers (optional)Ignore suspended users 
 * 
 * @todo ignoreInvited
 * @todo idAsset
 * 
 *  
 * @see ChannelUsers::__construct
 * 
 */
class ChannelUsers extends DataProvider {

	/**
	 * Constructor 
	 *  
	 * @param array $params = <br>
	 * 	 ['idChannel']		        => (int) Mandatory Channel ID<br>
	 * 	 ['idUser']		            => (int|array) Possible user ID or IDs for additional filtering  
	 * 	 ['ignoreUsers']		    => (int|array) Ignore users from  whole the results 
	 *   ['ignoreVisallChannels']   => (bool) Ignore users related to channels defined as "visible to all"<br>
	 *   ['ignoreGodAdminEffect']   => (bool) Ignore GodAdmin users <br>
	 *   ['ignoreInvited']			=> (bool) Ignore assets the user (if provided) is already invited to watch<br>
	 *   ['ignoreUsers']			=> (bool) Ignore assets the user (if provided) is already invited to watch<br>
	 *   ['idAsset']				=> (bool) add filter for id of asset <br>
	 *   ['ignoreSuspendedUsers']	=> (bool) (optional)Ignore suspended users  <br>
	 *   <br>
	 *   For parmeters, common for all providers: {@see DataProvider::__construct()}
	 * 
	 * @see DataProvider::__construct()  
	 * @throws CException
	 */
	public function __construct($params) {

		parent::__construct($params);

		if (!$this->idChannel) {
			throw new CException("Invalid channel in data provider");
		}

		if (empty($this->ignoreUsers)) {
			$this->ignoreUsers = array(-1);
		}
	}

	/**
	 * Build standard SELECT for THIS provider
	 * @param string $alias
	 */
	private function makeSelect($alias) {
		$select = array(
			"$alias.idst as id",
			"CONCAT($alias.firstname, ' ',$alias.lastname) AS `name`",
			"$alias.register_date as created"
		);
		if (!empty($this->customSelect) && is_array($this->customSelect)) {
			foreach ($this->customSelect as $selectField => $selectAs) {
				$select_area = str_replace('#alias#.', $alias . '.', $selectField) . ' as ' . $selectAs;
				$select[] = $select_area;
			}
		}
		return $select;
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see DataProvider::provider()
	 */
	public function provider() {

		$command = Yii::app()->db->createCommand();

		// SQL parameters passed to Data Provider at the end
		$dpParams = array(
			':idChannel' => $this->idChannel,
		);

		// God Admins can see all Channels, so just get the list of God Admins
		$command->select($this->makeSelect("cu1"));
		$command->from("core_group_members cgm");
		$command->join("core_group cg", "cg.idst=cgm.idst AND cg.groupid='/framework/level/godadmin'");
		$command->join("core_user cu1", "cgm.idstMember=cu1.idst");
		// Add some filtering, if any
		$this->addSearchFilter($command, "CONCAT(COALESCE(cu1.firstname,''), ' ',COALESCE(cu1.lastname,''))", $dpParams);
		$this->addUserFilter($this->idUser, $command, "cu1.idst", $dpParams);
		$this->igonreUserFileter($this->ignoreUsers, $command, "cu1.idst", $dpParams);
		$this->ignoreSuspendedUsersFilter($command, "cu1");
		if ($this->ignoreGodAdminEffect) {
			$command->andWhere("FALSE");
		}

		// Channel -> Group -> Users
		$c = Yii::app()->db->createCommand();
		$c->select($this->makeSelect("cu2"));
		$c->from("app7020_channel_visibility cv");
		$c->join("core_group_members cgm", "cgm.idst=cv.idObject AND cv.type=1");
		$c->join("core_user cu2", "cgm.idstMember=cu2.idst");
		$c->andWhere("cv.idChannel=:idChannel");
		// Add some filtering, if any
		$this->addSearchFilter($c, "CONCAT(COALESCE(cu2.firstname,''), ' ',COALESCE(cu2.lastname,''))", $dpParams);
		$this->addUserFilter($this->idUser, $c, "cu2.idst", $dpParams);
		$this->igonreUserFileter($this->ignoreUsers, $c, "cu2.idst", $dpParams);
		$this->ignoreSuspendedUsersFilter($c, "cu2");
		$command->union($c->text);


		// Channel -> Org chart NODE -> Users
		$c = Yii::app()->db->createCommand();
		$c->select($this->makeSelect("cu3"));
		$c->from("app7020_channel_visibility cv");
		$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
		$c->join("core_group_members cgm", "(cgm.idst=tree.idst_oc)");
		$c->join("core_user cu3", "cgm.idstMember=cu3.idst");
		$c->andWhere("cv.idChannel=:idChannel");
		// Add some filtering, if any
		$this->addSearchFilter($c, "CONCAT(COALESCE(cu3.firstname,''), ' ',COALESCE(cu3.lastname,''))", $dpParams);
		$this->addUserFilter($this->idUser, $c, "cu3.idst", $dpParams);
		$this->igonreUserFileter($this->ignoreUsers, $c, "cu3.idst", $dpParams);
		$this->ignoreSuspendedUsersFilter($c, "cu3");
		$command->union($c->text);

		// Channel -> Org chart BRANCH -> Users
		$c = Yii::app()->db->createCommand();
		$c->select($this->makeSelect("cu4"));
		$c->from("app7020_channel_visibility cv");
		$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
		$c->join("core_org_chart_tree tree2", "(tree2.iLeft >= tree.iLeft) AND (tree2.iRight <= tree.iRight )");
		$c->join("core_group_members cgm", "(cgm.idst=tree2.idst_oc)");
		$c->join("core_user cu4", "cgm.idstMember=cu4.idst");
		$c->andWhere("cv.idChannel=:idChannel");
		$c->andWhere("cv.selectState=2");
		
		// Add some filtering, if any
		$this->addSearchFilter($c, "CONCAT(COALESCE(cu4.firstname,''), ' ',COALESCE(cu4.lastname,''))", $dpParams);
		$this->addUserFilter($this->idUser, $c, "cu4.idst", $dpParams);
		$this->igonreUserFileter($this->ignoreUsers, $c, "cu4.idst", $dpParams);
		$this->ignoreSuspendedUsersFilter($c, "cu4");
		$command->union($c->text);


		// If The Channel is visible to all, all users can see it
		if (!$this->ignoreVisallChannels) {
			$visAllChannels = $this->getVisibleToAllChannels($this->idChannel);
			if (!empty($visAllChannels)) {
				$c = Yii::app()->db->createCommand();
				$c->select($this->makeSelect("cu5"));
				$c->from("core_user cu5");
				$c->andWhere("cu5.userid <> '/Anonymous'");
				$this->addSearchFilter($c, "CONCAT(COALESCE(cu5.firstname,''), ' ',COALESCE(cu5.lastname,''))", $dpParams);
				$this->addUserFilter($this->idUser, $c, "cu5.idst", $dpParams);
				$this->igonreUserFileter($this->ignoreUsers, $c, "cu5.idst", $dpParams);
				$this->ignoreSuspendedUsersFilter($c, "cu5");
				$command->union($c->text);
			}
		}


		// FINALIZE   
		list($commandData, $numRecords, $sort) = $this->finalizeCommand($command, $dpParams);
		$sort->defaultOrder = array(
			'name' => CSort::SORT_ASC,
		);

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, array(
			'totalItemCount' => $numRecords,
			'pagination' => $this->pagination,
			'params' => $dpParams,
			'sort' => $sort,
		));

		return $dataProvider;
	}
 

}
