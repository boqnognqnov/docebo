<?php

/**
 * Assets (ID, name, created) seen by given user
 * Covers Coach & Share assets only
 * 
 * @property integer $idUser 
 * @property integer $idAsset 
 * @property string $assetType 
 * @property integer $idChannel 
 * @property boolean $ignoreVisallChannels
 * @property boolean $ignoreInvited
 * @property boolean $ignorePrivate
 * @property boolean $igonreContributorAssets
 * @property boolean $onlyPrivate
 * @property array|string $customSelect
 * @property boolean $ignoreGodAdminEffect
 * @property integer $conversionStatus
 * 
 * @property integer $idPlaylist
 * @property boolean $ignorePlaylists
 * @property boolean $ignoreBlankPlaylists
 * @property boolean $ignorePlaylistAssets

 * 
 * 
 * @see UserAssets::__construct
 * 
 */
class UserAssets extends DataProvider {

	/**
	 * Constructor 
	 *  
	 * @param array $params = <br>
	 *   ['idUser']							=> (int) Mandatory User ID <br> 
	 * 	 ['idAsset']						=> (int) (optional) Asset ID to filter by <br>
	 * 	 ['idPlaylist']						=> (int) (optional) Asset ID which is used as playlist  <br>
	 * 	 ['assetType']						=> (string) (optional) Asset Type to filter by. Default is Coach & Share asset<br>
	 * 	 ['idChannel']						=> (int) (optional) Channel ID to filter by <br>
	 *   ['ignoreVisallChannels']			=> (bool) Ignore channels defined as "visible to all"<br>
	 *   ['ignoreInvited']					=> (bool) Ignore assets the user is already invited to watch<br>
	 *   ['ignorePrivate']					=> (bool) Ignore assets that are privete<br>
	 *   ['customSelect']					=> (array|string) set which data should be returned <br>
	 *   ['ignoreGodAdminEffect']			=> (bool) Ignore assets user sees because he is a God Admin<br>
	 *   ['conversionStatus']				=> (int) Filter by publish status 
	 *   ['ignorePlaylistAssets']			=> (array|integer) ignore all assets who are presenting into playlist (idAsset) and asset type should be 2  
	 *   ['ignorePlaylists']				=> (boolean) exclude  playlists 
	 *   ['ignoreBlankPlaylists']			=> (boolean) ignore blank playlists 
	 *   <br>
	 *   For parmeters, common for all providers: {@see DataProvider::__construct()}
	 * 
	 * @see DataProvider::__construct()  
	 * 
	 * @throws CException
	 */
	public function __construct($params) {

		parent::__construct($params);

		if (!$this->assetType) {
			$this->assetType = App7020ChannelAssets::ASSET_TYPE_COACH_SHARE_ASSET; // default is coach & share assets only
		}

		// Always be an array! Easier to handle later
		if (isset($this->assetType) && !is_array($this->assetType)) {
			$this->assetType = array($this->assetType);
		}

		// By default, filter by published assets
		if (!$this->conversionStatus) {
			$this->conversionStatus = App7020Assets::CONVERSION_STATUS_APPROVED;
		}

		// By default, filter by published assets
		if (!$this->igonreContribotorAssets) {
			$this->igonreContribotorAssets = true;
		}


		// By default, we have to use false as id playlist criteria 
		if (!$this->idPlaylist) {
			$this->idPlaylist = false;
		}

		if (!$this->idUser) {
			throw new CException("Invalid user in data provider");
		}
	}

	/**
	 * Add JOIN to ignore assets user is already invited to see
	 * 
	 * @param CDbCommand $command
	 * @param array $dpParams
	 * @param string $alias
	 */
	private function ignoreInvitedJoin(&$command, &$dpParams, $alias = false) {
		static $index = 1;
		if ($this->ignoreInvited) {
			if ($alias === false) {
				$alias = "i_n_v_" . $index;
				$index++;
			}
			$dpParams[":idUser"] = $this->idUser;
			$command->leftJoin("app7020_invitations $alias", "$alias.idContent=asset.id AND $alias.idInvited=:idUser");
			$command->andWhere("$alias.idContent IS NULL");
		}
	}

	/**
	 * Build standard SELECT for THIS provider
	 * @param string|array $assetsAlias
	 */
	private function make7020AssetSelect($assetsAlias) {
		$select = array();
		if (!empty($this->customSelect) && is_array($this->customSelect)) {
			foreach ($this->customSelect as $selectField => $selectAs) {
				$select[] = $assetsAlias . '.' . $selectField . ' as ' . $selectAs;
			}
		} else if (!empty($this->customSelect) && is_string($this->customSelect)) {
			$select = $this->customSelect;
		} else {
			$select = array(
				"$assetsAlias.id as id",
				"$assetsAlias.title as name",
				"$assetsAlias.created as created",
				"CONCAT('1') as type",
			);
		}
		return $select;
	}

	private function addConversionStatusFilter(&$command, $field, &$dpParams) {
		if (isset($this->conversionStatus)) {
			if (is_array($this->conversionStatus) && !empty($this->conversionStatus)) {
				$command->andWhere(array("IN", $field, $this->conversionStatus));
			} else if (empty($this->conversionStatus)) {
				$command->andWhere("FALSE");
			} else {
				$command->andWhere("$field=:status");
				$dpParams[":status"] = intval($this->conversionStatus);
			}
		}
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see DataProvider::provider()
	 */
	public function provider() {

		$command = Yii::app()->db->createCommand();

		// SQL parameters passed to Data Provider at the end
		$dpParams = array();

		$csAssetsSelect = $this->make7020AssetSelect("asset");

		// A: When user is a God Admin...
		if (CoreUser::isUserGodadmin($this->idUser) && !$this->ignoreGodAdminEffect) {
			// A.1. All C&S assets 
			$command = Yii::app()->db->createCommand();
			$command->select($csAssetsSelect);
			$command->from("app7020_content asset");
			$this->addConversionStatusFilter($command, "asset.conversion_status", $dpParams);
			$this->addAssetFilter($this->idAsset, $command, "asset.id", $dpParams);
			$this->addSearchFilter($command, "asset.title", $dpParams);
			$this->ignoreInvitedJoin($command, $dpParams);
			//playlists filters
			$this->_ignorePlaylists($command, $dpParams);
			$this->_ignoreBlankPlaylists($command, $dpParams);
			$this->_ignorePrivate($command);
			$this->_ignorePlaylistAssetsFilter($command, $dpParams);

			// A.2. Courses ?
			// @TODO
			// A.3. Learning plans ?
			// @TODO
			// A.4. PlayLists ?
			// @TODO
		}
		// B: OR a regular user
		else {
			$dpParams = array(
				':idUser' => $this->idUser
			);

			// ---------------------- Coach & Share assets ------------------------
			// B.1. User can see all PRIVATE Coach & Share assets he/she is invited to see
			$command = Yii::app()->db->createCommand();
			$command->select($csAssetsSelect);
			$command->from("app7020_content asset");
			$command->join("app7020_invitations inv", "asset.id=inv.idContent AND inv.idInvited=:idUser");
			$this->addConversionStatusFilter($command, "asset.conversion_status", $dpParams);
			$this->addAssetFilter($this->idAsset, $command, "asset.id", $dpParams);
			$this->addSearchFilter($command, "asset.title", $dpParams);
			$this->ignoreInvitedJoin($command, $dpParams);
			if ($this->idChannel) {
				$command->join("app7020_channel_assets ca", "ca.idAsset=asset.id");
				$this->addChannelFilter($this->idChannel, $command, "ca.idChannel", $dpParams);
			}
			//playlists filters
			$this->_ignorePlaylists($command, $dpParams);
			$this->_ignoreBlankPlaylists($command, $dpParams);
			$this->_ignorePrivate($command);
			$this->_ignorePlaylistAssetsFilter($command, $dpParams);

			// B.2. User -> Group -> Channel -> Assets
			$c = Yii::app()->db->createCommand();
			$c->select($csAssetsSelect);
			$c->from("app7020_channel_visibility cv");
			$c->join("core_group_members cgm", "cgm.idst=cv.idObject AND cv.type=1 AND cgm.idstMember=:idUser");
			$c->join("app7020_channel_assets ca", "ca.idChannel=cv.idChannel");
			$c->join("app7020_content asset", "asset.id=ca.idAsset AND asset.is_private=0");
			$this->addConversionStatusFilter($c, "asset.conversion_status", $dpParams);
			$this->addAssetFilter($this->idAsset, $c, "asset.id", $dpParams);
			$this->addSearchFilter($c, "asset.title", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "cv.idChannel", $dpParams);
			$this->ignoreInvitedJoin($c, $dpParams);

			//playlist filters
			$this->_ignorePlaylists($c, $dpParams);
			$this->_ignoreBlankPlaylists($c, $dpParams);
			$this->_ignorePrivate($c);
			$this->_ignorePlaylistAssetsFilter($c, $dpParams);

			$command->union($c->text);

			// B.3. User -> Org Chart NODE -> Channel -> Assets
			$c = Yii::app()->db->createCommand();
			$c->select($csAssetsSelect);
			$c->from("app7020_channel_visibility cv");
			$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
			$c->join("core_group_members cgm", "cgm.idst=tree.idst_oc AND cgm.idstMember=:idUser");
			$c->join("app7020_channel_assets ca", "ca.idChannel=cv.idChannel");
			$c->join("app7020_content asset", "asset.id=ca.idAsset AND asset.is_private=0");
			$this->addConversionStatusFilter($c, "asset.conversion_status", $dpParams);
			$this->addAssetFilter($this->idAsset, $c, "asset.id", $dpParams);
			$this->addSearchFilter($c, "asset.title", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "cv.idChannel", $dpParams);
			$this->ignoreInvitedJoin($c, $dpParams);
			//playlist filters
			$this->_ignorePlaylists($c, $dpParams);
			$this->_ignoreBlankPlaylists($c, $dpParams);
			$this->_ignorePrivate($c);
			$this->_ignorePlaylistAssetsFilter($c, $dpParams);

			$command->union($c->text);

			// B.4. User -> Org Chart BRANCH -> Channel -> Assets
			$c = Yii::app()->db->createCommand();
			$c->select($csAssetsSelect);
			$c->from("app7020_channel_visibility cv");
			$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=3");
			$c->join("core_org_chart_tree tree2", "(tree2.iLeft >= tree.iLeft) AND (tree2.iRight <= tree.iRight )");
			$c->join("core_group_members cgm", "cgm.idst=tree2.idst_oc AND cgm.idstMember=:idUser");
			$c->join("app7020_channel_assets ca", "ca.idChannel=cv.idChannel");

			//older behaviour 
			//$c->join("app7020_content asset", "asset.id=ca.idAsset AND asset.is_private=0");
			
			$c->join("app7020_content asset", "asset.id=ca.idAsset AND asset.is_private=0");
			// Add some filtering, if any
			$this->addConversionStatusFilter($c, "asset.conversion_status", $dpParams);
			$this->addAssetFilter($this->idAsset, $c, "asset.id", $dpParams);
			$this->addSearchFilter($c, "asset.title", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "cv.idChannel", $dpParams);
			$this->ignoreInvitedJoin($c, $dpParams);
			//playlist filters
			$this->_ignorePlaylists($c, $dpParams);
			$this->_ignoreBlankPlaylists($c, $dpParams);
			$this->_ignorePrivate($c);
			$this->_ignorePlaylistAssetsFilter($c, $dpParams);

			$command->union($c->text);

			// B.5. User -> Visible to all channels -> Assets
			// Get all channels "visible to all", filtered by idChannel parameter (if any)
			if (!$this->ignoreVisallChannels) {
				$visAllChannels = $this->getVisibleToAllChannels($this->idChannel);
				if (!empty($visAllChannels) && is_array($visAllChannels)) {
					$c = Yii::app()->db->createCommand();
					$c->select($csAssetsSelect);
					$c->from("app7020_channel_assets ca");
					$c->join("app7020_content asset", "asset.id=ca.idAsset AND asset.is_private=0");
					$c->andWhere(array("IN", "ca.idChannel", $visAllChannels));
					$this->addConversionStatusFilter($c, "asset.conversion_status", $dpParams);
					$this->addAssetFilter($this->idAsset, $c, "asset.id", $dpParams);
					$this->addSearchFilter($c, "asset.title", $dpParams);
					$this->ignoreInvitedJoin($c, $dpParams);
					//playlist filters
					$this->_ignorePlaylists($c, $dpParams);
					$this->_ignoreBlankPlaylists($c, $dpParams);
					$this->_ignorePrivate($c);
					$this->_ignorePlaylistAssetsFilter($c, $dpParams);

					$command->union($c->text);
				}
			}
			//B.6 User -> Visible all my assets
			if (!$this->igonreContributorAssets) {
				$c = Yii::app()->db->createCommand();
				$c->select($csAssetsSelect);
				$c->from("app7020_content asset");
				$this->ignoreInvitedJoin($c, $dpParams);
				$c->where('asset.userId = :idUser', array('idUser' => Yii::app()->user->idst));
				$this->addConversionStatusFilter($c, "asset.conversion_status", $dpParams);
				$this->addAssetFilter($this->idAsset, $c, "asset.id", $dpParams);
				$this->addSearchFilter($c, "asset.title", $dpParams);

				//playlist filters
				$this->_ignorePlaylists($c);
				$this->_ignoreBlankPlaylists($c);
				$this->_ignorePrivate($c);
				$this->_ignorePlaylistAssetsFilter($c, $dpParams);

				$command->union($c->text);
			}


			//@TODO Repeat B.2-5 for Courses, Learning Plans and PlayLists  "assets"
			// ---------------------- Course "assets" ------------------------
			// C.2 Group
			// C.3 Node
			// C.4 Branch
			// C.5 VisAll channels
			// ---------------------- Learning Plan "assets" ------------------------
			// D.2 Group
			// D.3 Node
			// D.4 Branch
			// D.5 VisAll channels
			// ---------------------- Playlist "assets" ------------------------
			// E.2 Group
			// E.3 Node
			// E.4 Branch
			// E.5 VisAll channels
		}

		// FINALIZE   
		list($commandData, $numRecords, $sort) = $this->finalizeCommand($command, $dpParams);
		$sort->defaultOrder = array(
			'created' => CSort::SORT_DESC,
			'name' => CSort::SORT_ASC,
		);

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, array(
			'totalItemCount' => $numRecords,
			'pagination' => $this->pagination,
			'params' => $dpParams,
			'sort' => $sort,
		));

		return $dataProvider;
	}

	/**
	 * Ignore all the playlist from whole sql queries into general provider 
	 * @param cdbCommand $command
	 * @param array $dpParams
	 */
	private function _ignorePlaylists(&$command) {
		
		// DO NOTHING. PlayLists are still not released
		return;
		
		if ($this->ignorePlaylists) {
			$command->andWhere('asset.contentType <> ' . App7020Assets::CONTENT_TYPE_PLAYLIST);
		}
	}

	/**
	 * Ignore all the playlist assets from common sql provider 
	 * @param cdbCommand $command
	 * @param array $dpParams
	 */
	private function _ignoreBlankPlaylists(&$command) {
		
		// DO NOTHING. PlayLists are still not released
		return;
		
		if ($this->ignoreBlankPlaylists) {
			$command->leftJoin('app7020_content_playlists acp', 'asset.id = acp.idContent ');
			$command->andWhere('acp.idPlaylist IS NOT NULL');
		}
	}

	/**
	 * Ignore all the private assets from common sql provider 
	 * @param cdbCommand $command
	 * @return void 
	 */
	private function _ignorePrivate(&$command) {
		if ($this->ignorePrivate) {
			$command->andWhere('asset.is_private > 0');
		}
	}

	/**
	 * 
	 * @param cdbCommand $command
	 * @param array $dpParams
	 */
	private function _ignorePlaylistAssetsFilter(&$command, &$dpParams) {
		
		// DO NOTHING. PlayLists are still not released
		return;
		
		
		if (empty($this->ignoreBlankPlaylists)) {
			$command->leftJoin('app7020_content_playlists acp', 'acp.idContent = asset.id');
		}

 		if ($this->ignorePlaylistAssets && $this->idPlaylist) {
 			$c = Yii::app()->db->createCommand();
			$c->select('idContent');
			$c->from('app7020_content_playlists');
			$c->where('idPlaylist = :idPlaylist');
			$command->andWhere('asset.id NOT IN (' . $c->getText() . ')');
			$dpParams['idPlaylist'] = $this->idPlaylist;
		}
	}

}
