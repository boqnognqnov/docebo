<?php

/**
 * 
 * Get list of channels a given user can see
 * 
 * @property integer $idUser
 * @property int $idAsset
 * @property integer|array $idChannel 
 * @property integer|array $ignoreChannels
 * 
 * //update after implementation of invite to watch
 * @property boolean $appendEnabledEffect (optional) all channels has enabled effect; we are remove this criteria when is not needed
 * @property boolean $ignoreSystemChannels (optional) we have to exclude the system channels when they are not needed;
 * @property boolean $ignoreGodAdminEffect (optional) Ignore channels user sees because he is a God Admin
 * @property boolean $ignoreVisallChannels (optional) Ignore channels defined as "visible to all"
 * @property array $customSelect (optional) Append custom select to get custom info array('#alias#.field'=>'alias_field_name')
 * @property boolean $igonreAvatar (optional) Ignore criteria to return avatar information
 * 
 * @see UserChannels::__construct
 * 
 */
class UserChannels extends DataProvider {

	/**
	 * @param array $params;
	 * @param integer $params['idUser']					Mandatory User ID
	 * @param integer $params['idAsset']				(optional) value if we have to see channels of asset per global visibility 
	 * @param integer|array $params['idChannel']		(optional) Channel ID to filter by
	 * @param boolean $params['ignoreChannels']			(optional) If we have to exclude some channels 
	 * @param boolean $params['appendEnabledEffect']	(optional) all channels has enabled effect; we are remove this criteria when is not needed 
	 * @param boolean $params['ignoreSystemChannels']	(optional) we have to exclude the system channels when they are not needed
	 * @param boolean $params['ignoreGodAdminEffect']	(optional) Ignore channels user sees because he is a God Admin
	 * @param boolean $params['ignoreVisallChannels']	(optional) Ignore channels defined as "visible to all"
	 * @param boolean $params['ignoreExpertChannels']	(optional) Ignore channel assigned to expert for common visibility
	 * @param array $params['customSelect']				(optional) Append custom select to get custom info array('#alias#.field'=>'alias_field_name')
	 * @param boolean $params['igonreAvatar']			(optional) Ignore criteria to return avatar information 
	 * 
 	 * For parmeters, common for all providers: {@see DataProvider::__construct()}
	 * @see DataProvider::__construct()  
	 * @throws CException
	 */
	public function __construct($params) {

		parent::__construct($params);

		if (!$this->idUser) {
			throw new CException("Invalid user in data provider");
		}

		if (!isset($this->igonreAvatar)) {
			$this->igonreAvatar = true;
		}
		//we have to set default idAsset because we will use this id for extend joins per asset 
		if (!isset($this->idAsset)) {
			$this->idAsset = false;
		}
		
		if (!isset($this->ignoreSuspendedUsers)) {
			$this->ignoreSuspendedUsers = false;
		}
		
		if (!isset($this->ignoreExpertChannels)) {
			$this->ignoreExpertChannels = false;
		}
	}

	/**
	 * Build standard SELECT for THIS provider
	 * @param string $alias
	 */
	private function makeSelect($alias, $trAlias, $trFbAlias) {
		$select = array(
			"$alias.id as id",
			"(CASE WHEN $trAlias.`name` IS NULL THEN $trFbAlias.`name` ELSE $trAlias.`name` END) as `name`",
			"CONCAT('1900-01-01 00:00:00') AS created"
		);
		if ($this->igonreAvatar === false) {
			$select[] = "CONCAT($alias.icon_bgr,'|',$alias.icon_color, '|', $alias.icon_fa) as avatar";
		}

		if (!empty($this->customSelect) && is_array($this->customSelect)) {
			foreach ($this->customSelect as $selectField => $selectAs) {
				$select_area = str_replace('#alias#.', $alias . '.', $selectField) . ' as ' . $selectAs;
				$select[] = $select_area;
			}
		}

		return $select;
	}

	/**
	 * Join (left) channel translations
	 * @param CDbCommand $command
	 */
	private function translationJoin(&$command, &$dpParams) {
		$command->leftJoin("app7020_channel_translation ct", "c.id=ct.idChannel AND ct.lang=:language");
		$command->leftJoin("app7020_channel_translation ct_fb", "c.id=ct_fb.idChannel AND ct_fb.lang=:langFallback");
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see DataProvider::provider()
	 */
	public function provider() {

		$command = Yii::app()->db->createCommand();
		// SQL parameters passed to Data Provider at the end
		$dpParams = array(
			":language" => $this->language,
			":langFallback" => $this->languageFallback,
		);

		$gr_br = CoreUser::getUsersGroupAndBranchAssignments($this->idUser);


		$parents = $gr_br[$this->idUser]['all_parent_branches'];
		$child = $gr_br[$this->idUser]['direct_branch_memberships'];
		$parents = explode(',', $parents);
		$child = explode(',', $child);

		// When user is a God Admin... he can see all channels
		if (CoreUser::isUserGodadmin($this->idUser) && !$this->ignoreGodAdminEffect) {
			$command = Yii::app()->db->createCommand();
			$command->select($this->makeSelect("c", "ct", "ct_fb"));
			$command->from("app7020_channels c");
			$this->translationJoin($command, $dpParams);
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$command->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $command, 'ac.idAsset', $dpParams);
			}

			$this->addSearchFilter($command, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $command, "c.id", $dpParams);

			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $command, "c.id", $dpParams);
			}
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($command, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($command, 'c.type');
		} else {
			$dpParams[':idUser'] = $this->idUser;
			// User can see ALL visible to all channels (filtered by idChannel parameter, if any)
			$visAllChannels = $this->getVisibleToAllChannels($this->idChannel);

			$command = Yii::app()->db->createCommand();
			$command->select($this->makeSelect("c", "ct", "ct_fb"));
			$command->from("app7020_channels c");
			$this->translationJoin($command, $dpParams);
			$this->addSearchFilter($command, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);

			$command->andWhere(array("IN", "c.id", $visAllChannels));
			if ($this->ignoreVisallChannels) {
				$command->andWhere("FALSE");
			}
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$command->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $command, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $command, "c.id", $dpParams);
			}
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($command, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($command, 'c.type');






			// User -> Group -> Channel
			$c = Yii::app()->db->createCommand();
			$c->select($this->makeSelect("c", "ct", "ct_fb"));
			$c->from("app7020_channel_visibility cv");
			$c->join("core_group_members cgm", "cgm.idst=cv.idObject AND cv.type=1 AND cgm.idstMember=:idUser");
			$c->join("app7020_channels c", "c.id=cv.idChannel");
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$c->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $c, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $c, "c.id", $dpParams);
			}
			$this->translationJoin($c, $dpParams);
			$this->addSearchFilter($c, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "c.id", $dpParams);
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($c, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($c, 'c.type');

			$command->union($c->text);




			// User -> Org Chart Node -> Channel
			$c = Yii::app()->db->createCommand();
			$c->select($this->makeSelect("c", "ct", "ct_fb"));
			$c->from("app7020_channel_visibility cv");
			$c->join("core_org_chart_tree tree", "tree.idOrg=cv.idObject AND cv.type=2");
			$c->join("core_group_members cgm", "(cgm.idst=tree.idst_oc) AND (cgm.idstMember=:idUser)");
			$c->join("app7020_channels c", "c.id=cv.idChannel");
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$c->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $c, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $c, "c.id", $dpParams);
			}

			$this->translationJoin($c, $dpParams);
			$this->addSearchFilter($c, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "c.id", $dpParams);
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($c, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($c, 'c.type');

			$command->union($c->text);




			// User -> Org Chart BRANCH -> Channel
			$c = Yii::app()->db->createCommand();
			$c->select($this->makeSelect("c", "ct", "ct_fb"));
			$c->from("app7020_channel_visibility cv");
 			$c->join("app7020_channels c", "c.id=cv.idChannel");
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$c->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $c, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $c, "c.id", $dpParams);
			}
			$c->andWhere(array('IN', 'cv.idObject', $parents));
			$c->andWhere('cv.type = 2');
			$c->andWhere('cv.selectState = 2');
			$this->translationJoin($c, $dpParams);
			$this->addSearchFilter($c, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "c.id", $dpParams);
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($c, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($c, 'c.type');
			$command->union($c->text);




			// User -> Org Chart BRANCH -> Channel
			$c = Yii::app()->db->createCommand();
			$c->select($this->makeSelect("c", "ct", "ct_fb"));
			$c->from("app7020_channel_visibility cv");
 			$c->join("app7020_channels c", "c.id=cv.idChannel");
			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$c->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $c, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $c, "c.id", $dpParams);
			}
			$c->andWhere(array('IN', 'cv.idObject', $parents));
			$c->andWhere(array('IN', 'cv.idObject', $child));
			$c->andWhere('cv.type = 2');
			$c->andWhere('cv.selectState = 1');
			$this->translationJoin($c, $dpParams);
			$this->addSearchFilter($c, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "c.id", $dpParams);
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($c, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($c, 'c.type');
			$command->union($c->text);


				// User -> Org Chart BRANCH -> Channel
			$c = Yii::app()->db->createCommand();
			$c->select($this->makeSelect("c", "ct", "ct_fb"));
			$c->from("app7020_channels c");
 			$c->join("app7020_channel_experts ce", "ce.idChannel=c.id");
 			$c->join("app7020_experts e", "e.id=ce.idExpert");
 			//we have to get only the channels for asset per my user visibility
			if (!empty($this->idAsset)) {
				$c->join("app7020_channel_assets ac", "ac.idChannel=c.id");
				$this->addAssetFilter($this->idAsset, $c, 'ac.idAsset', $dpParams);
			}
			//we have to ignore  channels which are predifined as excluded
			if (!empty($this->ignoreChannels)) {
				$this->igonreChannelFileter($this->ignoreChannels, $c, "c.id", $dpParams);
			}
  			$c->andWhere('e.idUser = :idUser');
 			$this->translationJoin($c, $dpParams);
			$this->addSearchFilter($c, "CONCAT(COALESCE(ct.name,''), ' ',COALESCE(ct_fb.name,''))", $dpParams);
			$this->addChannelFilter($this->idChannel, $c, "c.id", $dpParams);
			//we have to ignore all disabled channels
			$this->_addChannelEnabledFilter($c, 'c.enabled', $dpParams);
			//we have to exclude system channels
			$this->_addSystemChannelFilter($c, 'c.type');
			$command->union($c->text);
		}

		/**
		 *
		 * @param type $command
		 * @param array $dpParams
		 */
		// FINALIZE
		list($commandData, $numRecords, $sort) = $this->finalizeCommand($command, $dpParams);
		$sort->defaultOrder = array(
            'name' => CSort::SORT_ASC,
        );

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandData, array(
			'totalItemCount' => $numRecords,
			'pagination' => $this->pagination,
			'params' => $dpParams,
			'sort' => $sort,
		));
		return $dataProvider;
	}

	/**
	 * Append  channel enabled status to common criteria
	 * 
	 * @param cdbCommand $command
	 * @param string $field
	 * @param array $dpParams
	 * @return  void
	 */
	protected function _addChannelEnabledFilter(&$command, $field, &$dpParams) {
		if ($this->appendEnabledEffect) {
			$command->andWhere("{$field} = :enabled");
			$dpParams[':enabled'] = 1;
		}
	}

	/**
	 * ignore all system channels from general query 
	 * @param type $command
	 * @param type $field
	 * @return void
	 */
	protected function _addSystemChannelFilter(&$command, $field) {
		if ($this->ignoreSystemChannels) {
			$command->andWhere("{$field} = 2");
		}
	}

}
