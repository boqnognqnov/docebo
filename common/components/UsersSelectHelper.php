<?php

class UsersSelectHelper extends CApplicationComponent {

	protected $_users = '';
	protected $_groups = '';
	protected $_orgChart = '';

	private function setParams(array $params) {
		foreach ($params as $var => $value) {
			$this->$var = $value;
		}
	}

	public function getUsers() {
		return $this->_users;
	}
	
	public function setUsers($value) {
		$this->_users = $value;
		return true;
	}

	public function getGroups() {
		return $this->_groups;
	}
	
	public function setGroups($value) {
		$this->_groups = $value;
		return true;
	}

	public function getOrgChart() {
		return $this->_orgChart;
	}
	
	public function setOrgChart($value) {
		$this->_orgChart = $value;
		return true;
	}

	public function getUsersListFromRequest($params) {
		if (!empty($params)) {
			$this->setParams($params);
		}

		$usersList = array();

		if ($this->_users != '') {
			$this->getUsersList($usersList);
		}

		if ($this->_groups != '') {
			$this->getUsersFromGroups($usersList);
		}

		if ($this->_orgChart != '') {
			$this->getUsersFromOrgChart($usersList);
		}

		return $usersList;
	}

	protected function getUsersList(&$usersList) {
		$users = array();

		if (isset($_REQUEST[$this->_users]) && $_REQUEST[$this->_users] != '') {
			$users = explode(',', $_REQUEST[$this->_users]);
		}

		$usersList = array_merge($usersList, $users);
	}

	protected function getUsersFromGroups(&$usersList) {
		if (isset($_REQUEST[$this->_groups]) && $_REQUEST[$this->_groups] != '') {
			$grouplist = explode(',', $_REQUEST[$this->_groups]);
			$selectedGroupMembers = CoreGroupMembers::model()->findAllByAttributes(array('idst' => $grouplist));
			$selectedGroupMembersList = CHtml::listData($selectedGroupMembers, 'idstMember', 'idstMember');

			if (!empty($selectedGroupMembersList)) {
				$usersList = array_merge($usersList, $selectedGroupMembersList);
			}
		}
	}

	protected function getUsersFromOrgChart(&$usersList) {
		if (isset($_REQUEST[$this->_orgChart])) {
			$orgChartGroups = $_REQUEST[$this->_orgChart];
			$allNodes = array();
			
			// Get the Root Node ID
			$orgChartRoots = CoreOrgChartTree::model()->roots()->findAll();
			$orgChartRootId =  $orgChartRoots[0]->idOrg;
			
			foreach ($orgChartGroups as $id => $value) {
				// If ROOT node is selected (value != 0), consider this as ALL nodes selected and break the loop
				if (($id == $orgChartRootId) && ($value != '0')) {
					$allNodes = 'allnodes';
					break;
				}
				
				// If NODE ONLY is selected (depth=1)
				if ($value == '1') {
					if (!in_array($id, $allNodes)) {
						$allNodes[] = $id;
					}
				}
				// NODE+Decsendants selected (depth=2) 
				elseif ($value == '2') {
					$allChildrenNodesIds = CoreUser::model()->getCurrentAndChildrenNodesList($id);
					if (!empty($allChildrenNodesIds)) {
						foreach ($allChildrenNodesIds as $childId) {
							if (!in_array($childId, $allNodes)) {
								$allNodes[] = $childId;
							}
						}
					}
				}
			}

			
			// Now that we have NODES list, lets collect users
			if (!empty($allNodes)) {
				$criteria = new CDbCriteria();
				$criteriaAll = CoreUser::model()->dataProvider()->criteria;

				if (($allNodes !== 'allnodes') && (is_array($allNodes))) {
					$criteria->with     = array(
						'orgChartGroups' => array(
							'joinType' => 'INNER JOIN',
						),
					);
					$criteria->addInCondition('orgChartGroups.idOrg', $allNodes);
					$criteria->together = true;
				} else {
					$criteria = $criteriaAll;
				}

				$users = CoreUser::model()->findAll($criteria);
				foreach ($users as $user) {
					//if (!in_array($user->idst, $usersList)) {
						$usersList[] = $user->idst;
					//}
				}
				$usersList = array_unique($usersList);
			}
		}
	}

}