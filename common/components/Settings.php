<?php
/**
 * A component for accessing LMS settings, using CoreSetting model  (Singleton).

 * Basically a CoreSetting model wrapper, working with SET of models
 *
 * Usage:
 *     // Get Core Settings
 *     $value = Settings::get('parameter_name');
 *     $type= = Settings::getType('parameter_name');
 *     $allSettings = Settings::getAll();
 *
 *     // Get values from $cfg array (up to 3-dimenstional)
 *     $param = Settings::getCfg('paramName');
 *
 */
class Settings extends CComponent {


	public static $REG_TYPE_ADMIN = 'admin';


	/**
	 * Keeps loaded component instance
	 * @var Settings
	 */
	private static $_component = null;

	/**
	 * Keeps loaded models
	 * @var array of CoreSetting models (i.e. all rows from settings table)
	 */
	private $coreSettingModels = null;


	public function __construct() {

	}


	/**
	 * Get an instance of this component. Create one if not yet created.
	 *
	 * @param boolean $forceReload  Force creating new instance on demand
	 * @return Settings
	 */
	private static function getInstance($forceReload = false) {
		if ( isset(self::$_component) && (!$forceReload) ) {
			$component = self::$_component;
		}
		else
		{
			self::$_component = new Settings();
			$component = self::$_component;

			// Load all CoreSetting models
			$component->coreSettingModels = CoreSetting::model()->findAll();

		}
		return $component;
	}


	/**
	 * Find a CoreSetting model among all loaded models
	 *
	 * @param string $paramName
	 * @return boolean|CoreSetting
	 */
	private function findSettingModel($paramName) {

		if ( !$paramName ) {
			return false;
		}

		foreach ( $this->coreSettingModels as $model) {
			if ( strtolower($paramName) == strtolower($model->param_name) ) {
				return $model;
			}
		}

		return false;
	}


	/**
	 * Returns <strong>database based</strong> setting/parameter's value from <i>core_settings</i> DB table
	 *
	 * <strong>WARNING</strong>: Please be aware of the event raised inside the code (raised by default, you can disable it by setting $allowEvent=false).
	 * Observers, if any, can change the value of the parameter and create side effects.
	 * In case you REALLY, DEFINITELY need the "master" value (from core_setting table), set $allowEvent=false.
	 * An example of an observer is MultidomainApp
	 *
	 * @see MultidomainListeners::onGetCoreSetting()
	 *
	 * @param string $paramName
	 * @param string $defaultValue
	 * @param string $forceReload
	 * @param string $allowEvent Allow (default)/Disallow raising an event to allow observers to change settings ad-hoc.
	 *
	 * @return string/array/boolean  Setting value/All settings values/false
	 */
	public static function get($paramName, $defaultValue = null, $forceReload=false, $allowEvent=true) {

		$component = self::getInstance($forceReload);

		if ( ! empty($paramName) && is_string($paramName) ) {
			$model = $component->findSettingModel($paramName);
			if ( $model ) {

				$value = !empty($model->param_value) ? $model->param_value : $defaultValue;

				// Raise this event only if Yii App is NOT a Console application
				if (!(Yii::app() instanceof CConsoleApplication) && $allowEvent) {
					Yii::app()->event->raise('GetCoreSetting', new DEvent(new self(), array(
						'param_name' => $paramName,
						'value'			=> &$value,
					)));
				}


				return $value;
			}
			else {
				return $defaultValue;
			}
		}
		return false;
	}

	public static function getStandard($paramName, $defaultValue = null, $forceReload=false, $allowEvent=true)
	{
		$component = self::getInstance($forceReload);

		if ( ! empty($paramName) && is_string($paramName) ) {
			$model = $component->findSettingModel($paramName);
			if ( $model ) {

				$value = !empty($model->param_value) ? $model->param_value : $defaultValue;

				return $value;
			}
			else {
				return $defaultValue;
			}
		}
		return false;
	}


	/**
	 * Get config parameter saved in $GLOBALS[cfg] (domain configuration)
	 *
	 * @param String $paramName
	 * @return NULL || string
	 */
	public static function getCfg($paramName, $defaultValue=null) {
		if (isset($GLOBALS['cfg'][$paramName])) {
			return $GLOBALS['cfg'][$paramName];
		}
		return $defaultValue;
	}


	/**
	 * Get <strong>file-based</strong> config parameter, read previosly fron LMS's <i>config.php</i>
	 * or otherwise saved in $GLOBALS
	 *
	 *  @deprecated  Because OLD code "plugin" configurations are about to NO-MORE
	 *  @todo do not use and delete .. but now !
	 *
     * @param String $plugin
	 * @param String $paramName
	 * @return NULL || string
	 */
	public static function getPcfg($plugin, $paramName, $defaultValue=null) {
		if (isset($GLOBALS['cfg'][$plugin][$paramName])) {
			return $GLOBALS['cfg'][$plugin][$paramName];
		}
		return $defaultValue;
	}


	/**
	 * Returns all settings in an array of strings
	 *
	 * @return array
	 */
	public static function getAll($forceReload=false) {

		$component = self::getInstance($forceReload);

		$allSettings = array();
		foreach ( $component->coreSettingModels as $model) {
			$allSettings[$model->param_name] = $model->param_value;
		}

		return $allSettings;
	}


	/**
	 * Returns type of a given setting (set in DB table)
	 *
	 * @param string $paramName
	 * @return boolean|string
	 */
	public static function getType($paramName, $forceReload=false) {

		if ( !$paramName ) {
			return false;
		}

		$component = self::getInstance($forceReload);
		$model = $component->findSettingModel($paramName);
		if ( $model ) {
			return $model->value_type;
		}



	}


	/**
	 * Save or create the setting parameter
	 *
	 * @param string $name
	 * @param string $value
	 * @param string $value_type (default: string)
	 * @param int $max_size (default: 255)
	 * @param int $pack (default: false)
	 * @param int $regroup (default: 0)
	 * @param int $sequence (default: 0)
	 * @param int $param_load (default: 1) if false the parameter won't be automatically loaded at runtime
	 * @param int $hide_in_modify (default: 1) if true hide the param in the main settings area
	 * @param string $extra_info (default: '')
	 *
	 * @return CoreSetting
	 */
	public static function save($name, $value, $value_type='string', $max_size=255, $pack=false, $regroup=0, $sequence=0, $param_load=1, $hide_in_modify=1, $extra_info=false) {

		$setting_model = CoreSetting::model()->findByAttributes(array('param_name'=>$name));

		if (!$setting_model) {
			Yii::log("Creating new Settings parameter: $name = $value", CLogger::LEVEL_INFO);
			$setting_model = new CoreSetting();
			$setting_model->param_name = $name;
			$setting_model->param_value = $value;
			$setting_model->value_type = $value_type;
			$setting_model->max_size = $max_size;
			$setting_model->pack = $pack;
			$setting_model->regroup = $regroup;
			$setting_model->sequence = $sequence;
			$setting_model->param_load = $param_load;
			$setting_model->hide_in_modify = $hide_in_modify;
			$setting_model->extra_info = $extra_info;
			$setting_model->save(false);
			self::getInstance(true);
			return $setting_model;
		}
		else {
			return self::update($name, $value);
		}
	}


	/**
	 * Update core_setting parameter. Do NOT create if it does not exists.
	 *
	 * @param string $name
	 * @param string $value
	 *
	 * @return CoreSetting | false
	 */
	public static function update($name, $value) {

		$setting_model = CoreSetting::model()->findByAttributes(array('param_name'=>$name));
		if (!$setting_model) {
			return true;
		}

		$setting_model->param_value = $value;
		$setting_model->save(false);

		self::getInstance(true);
		return $setting_model;
	}


	/**
	 * Remove a given setting from core_setting table
	 *
	 * @param string $name
	 * @return boolean
	 */
	public static function remove($name) {
		$setting_model = CoreSetting::model()->findByAttributes(array('param_name'=>$name));
		if (!$setting_model) {
			return true;
		}
		$setting_model->delete();

		// Reload the data, just in case
		self::getInstance(true);
	}


	/**
	 * Check if name parameter exists in core_setting table
	 *
	 * @param string $paramName
	 * @param string $forceReload Should data be reloaded from database?
	 */
	public static function exists($paramName, $forceReload=false) {

	    $component = self::getInstance($forceReload);
	    if ( ! empty($paramName) && is_string($paramName) ) {
	        $model = $component->findSettingModel($paramName);
	        if ( $model ) {
	            return true;
	        }
	    }
	    return false;

	}


}
