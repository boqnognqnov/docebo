<?php
/**
 * 
 * Yii Application component to collect various statistics about THIS LMS and store them into dedicated REDIS database.
 * Statistics are collected on the fly, dynamically.  
 * Initialization is done once only, if the given statistic is not yet created in the storage. 
 * 
 */
class StatsCollection extends CApplicationComponent {

    /**
     * Statistical field names.
     * 
     * Each field should have a corresponding Initializing method, named "_init_<field-name>" in this class.
     * Those methods are called once only, if fields are not found in the LMS Stats Hash.
     * 
     * @var string
     */
    const ACTIVE_USERS      = 'active_users';
    const USERS             = 'users';
    const LEARNING_COURSES  = 'courses';
    const LEARNING_OBJECTS  = 'learning_objects';
    const SESSIONS          = 'sessions';
    
    /**
     * Just an array field names in the Stats Hash.
     * If you need translated titles, call getStatsTtilesTranslated()
     * 
     * @var array
     */
    static $STATS_FIELDS = array(
        self::ACTIVE_USERS,
        self::USERS,
        self::LEARNING_COURSES,
        self::LEARNING_OBJECTS,
        self::SESSIONS,
    );
    

    /**
     * Since this is an Yii Application component, this method is called upon its creation.
     * Bu default, StatsCollection is "preloaded" during Yii bootstrap!
     * 
     * {@inheritDoc}
     * @see CApplicationComponent::init()
     */
    public function init() {

        // MUST call this!!
        parent::init();
        
        // Check if we have this session variable set and skip this component at all        
        if (Yii::app()->session['skipStatsCollection']) {
            return;
        }

        // Try to connect to redis... 
        try {
            $redisStorage = new RedisStats();
            // Check if any field is missing the LMS Stats Hash and initialize its value accordingly
            $this->initializeMissingFields($redisStorage);
            
            // Set Listeners: we do all based on hooks
            Yii::app()->event->on(EventManager::EVENT_ACTIVE_USER_LOGGED, array($this, 'onActiveUserLogged'));
            Yii::app()->event->on('NewUserCreated', array($this, 'onNewUserCreated'));
            Yii::app()->event->on('NewUserCreatedSelfreg', array($this, 'onNewUserCreated'));
            Yii::app()->event->on('DeletedUser', array($this, 'onDeletedUser'));
            Yii::app()->event->on('NewCourseCreated', array($this, 'onNewCourseCreated'));
            Yii::app()->event->on('NewCourseDeleted', array($this, 'onNewCourseDeleted'));
            Yii::app()->event->on('NewLoAddedCourse', array($this, 'onNewLoAddedCourse'));
            Yii::app()->event->on('DeletedCourseLo', array($this, 'onDeletedCourseLo'));
            Yii::app()->event->on('BeforeUpdatingUserLastEnter', array($this, 'onBeforeUpdatingUserLastEnter'));
        }
        // If it was bad.. set a session variable to avoid further attempts.. until next time..
        catch (Exception $e) {
            Yii::app()->session['skipStatsCollection'] = 1;    
        }
        
    }
    
    
    /**
     * Return an array of translated Statistical field titles. Useful for APIs.
     * 
     */
    public function getStatsTitlesTranslated() {
        $result = array(
            self::ACTIVE_USERS        => Yii::t('standard', 'Number of active users for the current subscription period'),
            self::USERS               => Yii::t('standard', 'Total number of users'), 
            self::LEARNING_COURSES    => Yii::t('standard', 'Total number of courses'),
            self::LEARNING_OBJECTS    => Yii::t('standard', 'Total number of learning objects'),
            self::SESSIONS            => Yii::t('standard', 'Total number of user sessions'),
        );
    }
    
    
    // ----------------------------------------------------------------------------
    // -- EVENT LISTENERS
    // ----------------------------------------------------------------------------
    
    
    /**
     * 
     * @param DEvent $event
     */
    public function onActiveUserLogged(DEvent $event) {
        $activeUsers = Docebo::getActiveUsers();
        RedisStats::set(self::ACTIVE_USERS, $activeUsers);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onNewUserCreated(DEvent $event) {
        RedisStats::inc(self::USERS);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onDeletedUser(DEvent $event) {
        RedisStats::dec(self::USERS);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onNewCourseCreated(DEvent $event) {
        RedisStats::inc(self::LEARNING_COURSES);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onNewCourseDeleted(DEvent $event) {
        RedisStats::dec(self::LEARNING_COURSES);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onNewLoAddedCourse(DEvent $event) {
        RedisStats::inc(self::LEARNING_OBJECTS);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onDeletedCourseLo(DEvent $event) {
        RedisStats::dec(self::LEARNING_OBJECTS);
    }
    
    /**
     * 
     * @param DEvent $event
     */
    public function onBeforeUpdatingUserLastEnter(DEvent $event) {
        RedisStats::inc(self::SESSIONS);
    }


    /**
     * Initialize Stats Fields if needed
     * 
     */
    private function initializeMissingFields($redisStorage) {
        
        foreach (self::$STATS_FIELDS as $fieldName) {
            // Check if field exists in the LMS Stats Hash. Create/Initialize the field if not.
            $exists = $redisStorage->client->hexists($redisStorage->key, $fieldName);
            if (!$exists) {
                // Check if field specific initialization method exists and call it
                $method = "_init_" . $fieldName;
                Yii::log("Initializing LMS statistic field in Redis database ($redisStorage->key - $fieldName)", CLogger::LEVEL_INFO);
        
                if (method_exists($this, $method)) {
                    $this->$method();
                }
                // If it doesn't exists, we still need to create this field
                else {
                    $redisStorage->client->hset($redisStorage->key, $fieldName, "");
                }
            }
        }
    }
    
    
    // ----------------------------------------------------------------------------
    // -- INITIALIZATION Methods for Stat Fields:   _init_<stats-field-name>() {}
    // ----------------------------------------------------------------------------
    
    /**
     * 
     */
    private function _init_active_users() {
        $activeUsers = Docebo::getActiveUsers();
        RedisStats::set(self::ACTIVE_USERS, $activeUsers);
    }
    
    /**
     * 
     */
    private function _init_users() {
        $allUsers = CoreUser::getUsersCount();
        RedisStats::set(self::USERS, $allUsers);
    }
    
    /**
     * 
     */
    private function _init_courses() {
        $totalCourses = LearningCourse::model()->count();
        RedisStats::set(self::LEARNING_COURSES, $totalCourses);
    }
    
    /**
     * 
     */
    private function _init_learning_objects() {
        $totalLOs = LearningOrganization::model()->count();
        RedisStats::set(self::LEARNING_OBJECTS, $totalLOs);
    }
    
    /**
     * 
     */
    private function _init_sessions() {
        $counter = CoreUser::model()->countBySql("select count(idst) from core_user where lastenter IS NOT NULL and lastenter <> '0000-00-00 00:00:00'"); 
        RedisStats::set(self::SESSIONS, $counter);
    }
    
}