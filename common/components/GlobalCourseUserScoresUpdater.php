<?php

/**
 * --------------------------------------------------------
 * A helper component that is to be used very CAREFULLY!!!!
 *
 * ASK BEFORE USING. Can do a long running and resourve intensive
 * operation on large LMSes if not used properly!
 * --------------------------------------------------------
 *
 * Some of the features:
 * Causes a global update of all 'score_given' values for EVERY
 * course and user (every enrollment). Will pull the "final" score
 * for the course/user pair (the "milestone" SCORM, TinCan, Test, etc
 * or the last one if no milestone is set)
 *
 * Test if the scores in the 'score_given' columns are correct
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 *
 * @internal To be used in key moments only
 */
class GlobalCourseUserScoresUpdater {

	/**
	 * READ THE PHPDOC FOR THIS CLASS BEFORE CALLING
	 *
	 * @param array $forCourses - update score_given only for those courses, if set, else all
	 */
	static public function updateAllCourseuserScores($forCourses = array(), $milestone = LearningOrganization::MILESTONE_END, $column = 'score_given') {
		$migrateDebugEnd = function ( $beginTimestamp ) {
			return microtime( 1 ) - $beginTimestamp;
		};

		$beginDebug = microtime( 1 );

		$params = array(
			':type_aicc'        => LearningOrganization::OBJECT_TYPE_AICC,
			':type_scorm'       => LearningOrganization::OBJECT_TYPE_SCORMORG,
			':type_test'        => LearningOrganization::OBJECT_TYPE_TEST,
			':type_tincan'      => LearningOrganization::OBJECT_TYPE_TINCAN,
			':type_deliverable' => LearningOrganization::OBJECT_TYPE_DELIVERABLE,
			':type_elucidat' 	=> LearningOrganization::OBJECT_TYPE_ELUCIDAT,
			':milestone'        => $milestone,
		);

		// Get all courses with their respective "last/final" Learning Object
		$command = Yii::app()->db->createCommand()
		                                       ->select( 'c.idCourse, COALESCE(loMilestone.idOrg, loGreatest.idOrg) idOrg, COALESCE(loMilestone.objectType, loGreatest.objectType) objectType, COALESCE(loMilestone.idResource, loGreatest.idResource) idResource, loMilestone.milestone' )
		                                       ->from( LearningCourse::model()->tableName() . ' c' )
			// Where the LO is marked as END OBJECT MARKER:
		                                       ->leftJoin( LearningOrganization::model()->tableName() . ' loMilestone', 'c.idCourse=loMilestone.idCourse AND loMilestone.milestone=:milestone AND (loMilestone.objectType = :type_aicc OR loMilestone.objectType = :type_scorm OR loMilestone.objectType = :type_test OR loMilestone.objectType = :type_tincan OR loMilestone.objectType = :type_deliverable OR loMilestone.objectType = :type_elucidat)' )
			// Or just get the highest available LO of certain types (latest uploaded):
		                                       ->leftJoin( '(
                                              SELECT MAX(idOrg) idOrg, MAX(idResource) idResource, objectType, idCourse
                                              FROM learning_organization
                                              WHERE (objectType = :type_aicc OR objectType = :type_scorm OR objectType = :type_test OR objectType = :type_tincan OR objectType = :type_deliverable OR objectType = :type_elucidat)
                                              GROUP BY idCourse) loGreatest
                                                 ', 'c.idCourse=loGreatest.idCourse' )
			// Only get courses with at least one LO:
		                                       ->where( 'COALESCE(loMilestone.idOrg, loGreatest.idOrg) IS NOT NULL' )
			// Only a single LO per course can be "final"/"last":
		                                       ->group( 'c.idCourse' );

		if(!empty($forCourses)){
			$command->where(array('IN', 'c.idCourse', $forCourses));
		}

		$coursesAndFinalLOsArr = $command->queryAll( TRUE, $params );

		// Yii::log( 'Course - main LO pairs retrieved in ' . $migrateDebugEnd( $beginDebug ) . ' seconds' );


// Organize the big array of all course=>LO objects
// into separate arrays based on the LO type so we can
// run them in batches
		$aiccBasedCourses        = array();
		$scormBasedCourses       = array();
		$testBasedCourses        = array();
		$tincanBasedCourses      = array();
		$deliverableBasedCourses = array();

		foreach ( $coursesAndFinalLOsArr as $row ) {
			switch ( $row['objectType'] ) {
				case LearningOrganization::OBJECT_TYPE_AICC:
					$aiccBasedCourses[ $row['idOrg'] ] = $row;
					break;
				case LearningOrganization::OBJECT_TYPE_SCORMORG:
					$scormBasedCourses[ $row['idOrg'] ] = $row;
					break;
				case LearningOrganization::OBJECT_TYPE_TEST:
					$testBasedCourses[ $row['idOrg'] ] = $row;
					break;
				case LearningOrganization::OBJECT_TYPE_TINCAN:
					$tincanBasedCourses[ $row['idOrg'] ] = $row;
					break;
				case LearningOrganization::OBJECT_TYPE_DELIVERABLE:
					$deliverableBasedCourses[ $row['idOrg'] ] = $row;
					break;
			}
		}


		// AICC course/user pair score retriever
		//
		$aiccCourseIds = array();
		$aiccLoIDs     = array();
		foreach ( $aiccBasedCourses as $aiccCourseInfo ) {
			$aiccCourseIds[] = $aiccCourseInfo['idCourse'];
			$aiccLoIDs[]     = $aiccCourseInfo['idOrg'];
		}

		$beginDebug           = microtime( 1 );
		$aiccCourseUserScores = Yii::app()->getDb()->createCommand()
		                           ->select( 'cu.idCourse, cu.idUser, itt.score_raw' )
		                           ->from( LearningOrganization::model()->tableName() . ' org' )
		                           ->join( LearningCourseuser::model()->tableName() . ' cu', 'cu.idCourse=org.idCourse' )
		                           ->where( array( 'IN', 'org.idOrg', $aiccLoIDs ) )
		                           ->andWhere( 'org.objectType=:aicc', array( ':aicc' => LearningOrganization::OBJECT_TYPE_AICC ) )
		                           ->join( LearningAiccPackage::model()->tableName() . ' p', 'p.id=org.idResource' )
		                           ->join( LearningAiccItem::model()->tableName() . " it", "p.id=it.id_package" )
		                           ->join( LearningAiccItemTrack::model()->tableName() . " itt", "it.id=itt.idItem AND itt.idUser=cu.idUser AND COALESCE(itt.score_raw,0) > 0" )
		                           ->join( '(SELECT MAX(id) AS idMax FROM `learning_aicc_item_track` WHERE COALESCE(score_raw,0)>0 GROUP BY idUser, idItem) itt2', 'itt2.idMax=itt.id' )
		                           ->group( array( 'cu.idCourse', 'cu.idUser' ) )
		                           ->queryAll();
		// Yii::log( 'AICC scores retrieved in seconds: ' . $migrateDebugEnd( $beginDebug ) );
		//var_dump($aiccCourseUserScores);exit;
		//
		// AICC course/user pair score retriever


		//--------------------------------------
		//--------------------------------------
		//--------------------------------------


		// SCORM course/user pair score retriever
		//
		$scormLOs = array();
		foreach ( $scormBasedCourses as $scormBasedCourse ) {
			$scormLOs[] = $scormBasedCourse['idOrg'];
		}
		$beginDebug            = microtime( 1 );
		$scormCourseUserScores = Yii::app()->getDb()->createCommand()
		                            ->select( 'cu.idUser, cu.idCourse, t.score_raw' )
		                            ->from( LearningOrganization::model()->tableName() . ' o' )
		                            ->join( LearningScormTracking::model()->tableName() . ' t', 'o.idOrg=t.idReference AND o.objectType=:scorm', array( ':scorm' => LearningOrganization::OBJECT_TYPE_SCORMORG ) )
		                            ->join( LearningCourseuser::model()->tableName() . ' cu', 'cu.idCourse=o.idCourse AND cu.idUser=t.idUser' )
		                            ->join( '(SELECT MAX(idscorm_tracking) as maxId FROM learning_scorm_tracking WHERE COALESCE(score_raw,0)>0 GROUP BY idUser, idReference) t2 ', 't.idscorm_tracking=t2.maxId' )
		                            ->where( 'COALESCE(t.score_raw, 0) > 0' )
		                            ->andWhere( array( 'IN', 'o.idOrg', $scormLOs ) )
		                            ->queryAll();
		// Yii::log( 'SCORM scores retrieved in seconds: ' . $migrateDebugEnd( $beginDebug ) );
		//var_dump( $scormCourseUserScores );	exit;
		//
		// SCORM course/user pair score retriever


		// TEST course/user pair score retriever
		$testLOs = array();
		foreach ( $testBasedCourses as $testBasedCourse ) {
			$testLOs[] = $testBasedCourse['idOrg'];
		}
		$beginDebug           = microtime( 1 );
		$testCourseUserScores = Yii::app()->getDb()->createCommand()
		                           ->select( 'cu.idUser, cu.idCourse, (coalesce(tt.score,0)+coalesce(tt.bonus_score,0)) AS score_raw' )
		                           ->from( LearningOrganization::model()->tableName() . ' o' )
		                           ->where( array( 'IN', 'o.idOrg', $testLOs ) )
		                           ->andWhere( 'o.objectType=:test', array( ':test' => LearningOrganization::OBJECT_TYPE_TEST ) )
		                           ->join( LearningCourseuser::model()->tableName() . ' cu', 'cu.idCourse=o.idCourse' )
		                           ->join( LearningTesttrack::model()->tableName() . ' tt', 'o.idOrg=tt.idReference AND tt.idUser=cu.idUser' )
			// The second join is to ensure we only get the latest track for the user/course pair in case there are more than one
		                           ->join( '(SELECT MAX(idTrack) AS idTrack FROM learning_testtrack WHERE ((coalesce(score,0)+coalesce(bonus_score,0)) > 0) GROUP BY idUser,idReference) tt2', 'tt2.idTrack=tt.idTrack' )
		                           ->queryAll();
		// Yii::log( 'TEST scores retrieved in seconds: ' . $migrateDebugEnd( $beginDebug ) );
		//var_dump( $testCourseUserScores );exit;
		//
		// TEST user/course scores retriever


		// TINCAN user/course scores retriever
		//
		$tincanLOs = array();
		foreach ( $tincanBasedCourses as $tincanBasedCourse ) {
			$tincanLOs[] = $tincanBasedCourse['idOrg'];
		}
		$beginDebug             = microtime( 1 );
		$tincanCourseUserScores = Yii::app()->getDb()->createCommand()
		                             ->select( 'cu.idUser, cu.idCourse, track.score score_raw' )
		                             ->from( LearningOrganization::model()->tableName() . ' o' )
		                             ->where( array( 'IN', 'o.idOrg', $tincanLOs ) )
		                             ->andWhere( 'o.objectType=:tincan', array( ':tincan' => LearningOrganization::OBJECT_TYPE_TINCAN ) )
		                             ->join( LearningCourseuser::model()->tableName() . ' cu', 'o.idCourse=cu.idCourse' )
		                             ->join( LearningCommontrack::model()->tableName() . ' track', 'cu.idUser=track.idUser AND track.idReference=o.idOrg AND COALESCE(track.score,0)>0' )
			// This second JOIN is needed to ensure that we get the last track for each user/course pair if there are more than one
		                             ->join( '(SELECT MAX(idTrack) AS idTrack FROM learning_commontrack GROUP BY idUser, idReference) track2', 'track.idTrack=track2.idTrack' )
		                             ->queryAll();
		// Yii::log( 'TINCAN scores retrieved in seconds: ' . $migrateDebugEnd( $beginDebug ) );
		//var_dump( $tincanCourseUserScores );exit;
		//
		// TINCAN user/course scores retriever

		// DELIVERABLE user/course scores retriever
		//
		$deliverableLOs = array();
		foreach ( $deliverableBasedCourses as $deliverable ) {
			$deliverableLOs[] = $deliverable['idOrg'];
		}
		$beginDebug                  = microtime( 1 );
		$deliverableCourseUserScores = Yii::app()->getDb()->createCommand()
		                                  ->select( 'cu.idUser, cu.idCourse, track.evaluation_score AS score_raw' )
		                                  ->from( LearningOrganization::model()->tableName() . ' o' )
		                                  ->where( array( 'IN', 'o.idOrg', $deliverableLOs ) )
		                                  ->andWhere( 'o.objectType=:deliverable', array( ':deliverable' => LearningOrganization::OBJECT_TYPE_DELIVERABLE ) )
		                                  ->join( LearningCourseuser::model()->tableName() . ' cu', 'cu.idCourse=o.idCourse' )
		                                  ->join( LearningDeliverableObject::model()->tableName() . ' track', 'o.idResource=track.id_deliverable AND track.id_user=cu.idUser AND COALESCE(track.evaluation_score,0)>0' )
			// This second join ensures that we only get the latest available track, if there are multiple for a given user/course pair
		                                  ->join( '(SELECT MAX(idObject) as idObject FROM learning_deliverable_object WHERE COALESCE(evaluation_score,0)>0 GROUP BY id_deliverable, id_user) track2', 'track.idObject=track2.idObject' )
		                                  ->queryAll();
		// Yii::log( 'DELIVERABLE scores retrieved in seconds: ' . $migrateDebugEnd( $beginDebug ) );
		//var_dump( $deliverableCourseUserScores );exit;
		//
		// DELIVERABLE user/course scores retriever

		$updateScoresByType = function ( $arrayOfCoursesAndScores, $column ) {
			$beginDebug = microtime( 1 );
			Yii::app()->getDb()->beginTransaction();
			foreach ( $arrayOfCoursesAndScores as $row ) {
				Yii::app()->getDb()->createCommand()
				   ->update( 'learning_courseuser', array( $column => $row['score_raw'] ), 'idUser=:idUser AND idCourse=:idCourse AND '.$column.' IS NULL', array(
					   ':idUser'   => $row['idUser'],
					   ':idCourse' => $row['idCourse'],
				   ) );
			}
			Yii::app()->getDb()->getCurrentTransaction()->commit();
			Yii::log( 'Batch of ' . count( $arrayOfCoursesAndScores ) . ' course-user score pairs processed in ' . number_format( ( microtime( 1 ) - $beginDebug ), 2 ) . ' seconds' );
		};

		$updateScoresByType( $scormCourseUserScores, $column );
		$updateScoresByType( $tincanCourseUserScores, $column );
		$updateScoresByType( $testCourseUserScores, $column );
		$updateScoresByType( $deliverableCourseUserScores, $column );
		$updateScoresByType( $aiccCourseUserScores, $column );

	}


	/**
	 * READ THE PHPDOC FOR THIS CLASS BEFORE CALLING
	 *
	 * @param int $offset
	 *
	 * @throws CException
	 *
	 * @internal
	 */
	static public function _checkScoreGivenColumns($offset = 0){
		$checksPerPage = 2000;

		$cus = Yii::app()->getDb()->createCommand()
		          ->select( 'idUser, idCourse, score_given' )
		          ->from( LearningCourseuser::model()->tableName() )
		          ->limit( $checksPerPage, $offset )
		          ->queryAll();

		$err = false;

		$i = 0;
		foreach ( $cus as $row ) {
			$i ++;
			$dynamicCalculatedScore = LearningCourseuser::recalculateLastScoreByUserAndCourse( $row['idCourse'], $row['idUser'] );
			$scoreGiven = $row['score_given'];

			if ( $dynamicCalculatedScore != $scoreGiven ) {
				$err = 'Incorect score for user/course pair: ' . $row['idUser'] . '/' . $row['idCourse'];
				$err .= '. Score given column is ' . $row['score_given'];
				$err .= '. The correct score is ' . $dynamicCalculatedScore;
				$err .= '. Processed rows before error: ' . ( $i - 1 );
			}
		}

		if(empty($cus)){
			// Current offset yielded no more rows
			Yii::app()->getController()->renderText('All results checked. All good');
			return;
		}

		if($err){
			throw new CException( $err );
		}

		$nextUrl = Docebo::createLmsUrl('test2/check', array('offset'=>($offset+$checksPerPage)));
		Yii::app()->clientScript->registerScript('check', "
			window.location = '{$nextUrl}';
		");
		Yii::app()->getController()->renderText("Checked enrollments ".$offset." to ".($offset+$checksPerPage).". No errors yet.<br/>");
	}


}