<?php

/**
 * Class MController
 */

class MController extends Controller {

	/**
	 * Initialize the controller
	 */
	public function init() {

		// parent::init();
		// Check if we should load a theme different from the standard one
		$this->resolveTheme();

		// And now we can choose the correct layout to apply as the standard one
		$this->resolveLayout();

		$this->layout = 'mobile';
	}


	/**
	 * This is the action to handle external exceptions.
	 * This action is set as errorHandler in application config.
	 */
	public function actionError() {

		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo CHtml::encode($error['message']);
			} else {
				// Perhaps we need a good "Uh-oh" error page
				// $this->render('error', $error);
				echo 'Uh oh';
			}
		}
	}


}
