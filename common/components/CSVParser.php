<?php

class CSVParser extends CApplicationComponent {

	public function parse($file, $options = array()) {
		$delimiter = isset($options['delimiter']) ? $options['delimiter'] : ',';
		$header = array();
		$outArray = array();

		$fileContent = file_get_contents($file->tempName);

		if(isset($options['carriageParse']) && $options['carriageParse']){
			$fileContent = str_replace("\r\n",'|~|', $fileContent);
			$fileContent = str_replace("\r",'|~|', $fileContent);
			$fileContent = str_replace("\n",'|~|', $fileContent);
			$fileContentArray = explode("|~|", $fileContent);
		}else{
			$fileContentArray = explode("\n", $fileContent);
		}


		if (isset($options['hasHeader']) && $options['hasHeader'] == true) {
			$header = explode($delimiter, array_shift($fileContentArray));

			foreach($header as &$h){
				$h = strtolower($h);
			}

			$this->sanitize($header);
		}

		foreach ($fileContentArray as $row) {
			if($row){
				$data = explode($delimiter, $row);


				$this->sanitize($data);


				if (!empty($header) && !empty($data) && (count($header)==count($data)) && !(isset($options['carriageParse']) && $options['carriageParse'])) {
					$outArray[] = array_combine($header, $data);
				} else {
					$outArray[] = $data;
				}
			}
		}

		return $outArray;
	}


	/**
	 * Parse a TEXT, presumably LINES ending with PHP_EOL into an array of arrays
	 * If "hasHeader" option is true, the first line is treated as "column headers" and used as array keys,
	 * i.e. the returned array is associative
	 *
	 * @param string $text
	 * @param array $options   array("delimiter" => ",", "hasHeader" => false, "enclosure" => '"', "escape" => "\\"
	 *
	 * @return array
	 */
	public function parseText($text, $options = array()) {
		$delimiter 	= isset($options['delimiter']) 	? $options['delimiter'] : ",";
		$enclosure 	= isset($options['enclosure']) 	? $options['enclosure'] : '"';
		$escape		= isset($options['escape']) 	? $options['escape'] : "\\";

		$header = array();

		//$contentArray = explode(PHP_EOL, $text);
		$contentArray = preg_split('/(\r\n)|(\n\r)|(\n)|(\r)/', $text);
		$this->sanitize($contentArray, true);

		// Call array map using anonymoues function, passing options
		$array = array_map(function($v) use ($delimiter, $enclosure, $escape) {
			$v = trim($v);
			return str_getcsv($v, $delimiter, $enclosure, $escape);
		}, $contentArray);

		if (isset($options['hasHeader']) && $options['hasHeader'] == true) {
			$header = array_shift($array);
			$this->sanitize($header);
			foreach($header as $hIndex => &$h){

				if (!isset($options['caseSensitiveKeys']) || !$options['caseSensitiveKeys'])
					$h = strtolower($h);

				// Special case, where ONLY one column (header key) is significant and others are NOT important and their names MIGHT be equal

				// Exsample	: 	"myMasterKey" is the unique and important, the rest is just a list of values and kesy are not important
				//				In this case we must rename those keys to make them unique
				// header row	:	"myMasterKey", "member", "member", "member", "member"
				// data row		:	"root", "m1", "m2", "m3", "m4"
				//
				if (isset($options['masterKey'])) {
					 if (strtolower($options['masterKey']) != $h) {
					 	$h = $h . "_" . $hIndex;
					 }
				}
			}

			// Walk the top array and convert it to array of ASSOCIATIVE arrays
			// DO NOT use array_combine(), because HEADER lenght MIGHT BE different from DATA ROW length!!!
			// Also, note, EXTRA fields in DATA rows are lost if HEADER is shorter
			array_walk($array, function(&$row, $key, $header) {
				$outputRow = array();
				foreach ($header as $index => $headerKey) {
					$outputRow[$headerKey] = isset($row[$index]) ? $row[$index] : '';
				}
				$row = $outputRow;
			}, $header);

		}

		return $array;
	}


	/**
	 * Formats a line (passed as a fields array) as CSV and returns the CSV as a string.
	 * Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
	 *
	 * @param array $fields
	 * @param string $delimiter
	 * @param string $enclosure
	 * @param string $encloseAll
	 * @param string $nullToMysqlNull
	 * @return string
	 */
	public static function arrayToCsv( array &$fields, $delimiter = ',', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {

		$delimiter_esc = preg_quote($delimiter, '/');
		$enclosure_esc = preg_quote($enclosure, '/');

		$output = array();
		foreach ( $fields as $field ) {
			if ($field === null && $nullToMysqlNull) {
				$output[] = 'NULL';
				continue;
			}

			// Enclose fields containing $delimiter, $enclosure or whitespace
			if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
				$output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
			}
			else {
				$output[] = $field;
			}
		}

		return implode( $delimiter, $output );
	}

	public static function filePutCsv($handler,  array &$fields, $delimiter = ',', $enclosure = '"', $encloseAll = true, $nullToMysqlNull = false )
	{
		$string = self::arrayToCsv($fields, $delimiter, $enclosure, $encloseAll, $nullToMysqlNull)."\n";
		fputs($handler, $string);
	}


	public static function filePutCsvMultiple($handler,  array &$rows, $delimiter = ',', $enclosure = '"', $encloseAll = true, $nullToMysqlNull = false )
	{
		$string = '';
		foreach ($rows as $fields) {
			$string .= self::arrayToCsv($fields, $delimiter, $enclosure, $encloseAll, $nullToMysqlNull) . "\n";
		}
		if (!empty($string)) { fputs($handler, $string); }
	}



	/**
	 *
	 * @param unknown $array
	 */
	private function sanitize(&$array, $removeEmpty=false) {
		foreach ($array as $index => &$row) {
			$row = trim($row);
			if (!$row && $removeEmpty) {
				unset($array[$index]);
			}
		}

	}

}