<?php

/**
 * Description of App7020Helpers
 *
 * @author Kristian
 * @author Ivanov
 */
class App7020Helpers extends CComponent {

	static $cuepoints = array();

	const TIMEFRAME_DAILY = 1;
	const TIMEFRAME_WEEKLY = 7;
	const TIMEFRAME_MONTHLY = 30;
	const TIMEFRAME_YEARLY = 365;

	public static function timeAgo($time) {
		$time = (time() + 1) - strtotime($time . ' GMT'); // to get the time since that moment
		$tokens = array(
			31536000 => Yii::t('app7020','year'),
			2592000 => Yii::t('app7020','month'),
			604800 => Yii::t('app7020','week'),
			86400 => Yii::t('app7020','day'),
			3600 => Yii::t('app7020','hour'),
			60 => Yii::t('app7020','minute'),
			1 => Yii::t('app7020','second')
		);

		foreach ($tokens as $unit => $text) {
			if ($time < $unit)
				continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
		}
	}

	public static function secondsToWords($seconds, $words = array('d' => 'days', 'h' => 'hours', 'm' => 'minutes', 's' => 'seconds'), $d = true, $h = true, $m = true, $s = true) {
		$ret = "";
		$days = intval(intval($seconds) / (3600 * 24));
		if ($d) {
			$ret .= "$days$words[d]";
		}
		$hours = (intval($seconds) / 3600) % 24;
		if ($h) {
			$ret .= "$hours$words[h]";
		}
		$minutes = (intval($seconds) / 60) % 60;
		if ($m) {
			$ret .= "$minutes$words[m]";
		}
		$seconds = intval($seconds) % 60;
		if ($s) {
			$ret .= "$seconds$words[s]";
		}
		return $ret;
	}

	/**
	 * Get module assets url path
	 * @return string
	 */
	public static function getAssetsUrl() {
		return Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('app7020.assets'));
	}

	/**
	 * By name, exclude extention and return clear input amazon input key on files
	 * @param string $param
	 * @return boolean
	 */
	public static function getAmazonInputKey($param) {
		if (preg_match('/^(.*)\.(.*)$/i', $param, $out)) {
			return $out[1];
		} else {
			return false;
		}
	}

	/**
	 * Get file extention by filename
	 * @param string $filename
	 * @return boolean|string
	 */
	public static function getExtentionByFilename($filename) {
		$path_parts = pathinfo($filename);
		return strtolower($path_parts['extension']);
	}

	public static function registerAMDScript(DEvent $event) {

		//predefine of var
		$lib = false;
		$where = false;
		$dataAttrs = array();

		//use of any library 
		if (isset($event->params['lib'])) {
			$lib = $event->params['lib'];
		} else {
			$lib = Yii::app()->theme->baseUrl . '/js/require_js/require.js';
		}

		// Replace if exist new place on script
		if (isset($event->params['where'])) {
			$where = $event->params['where'];
		} else {
			$where = CClientScript::POS_END;
		}

		//found data atributes for script 
		if (isset($event->params['data']) && is_array($event->params['data'])) {
			foreach ($event->params['data'] as $dataKey => $dataValue) {
				$dataAttrs[$dataKey] = $dataValue;
			}
		}

		Yii::app()->getClientScript()->registerScriptFile($lib, $where, $dataAttrs);
	}

	public static function loadTimelineCss() {
		$currentRoute = Yii::app()->urlManager->parseUrl(Yii::app()->request);
		if ($currentRoute == "knowledgeLibrary/tooltips") {
			Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/vis.css');
		}
	}

	/**
	 * Register Global settings for app7020 object in JS 
	 * @param int $modelId
	 */
	public static function registerApp7020Options() {
		$arrayLoad = array();
//		if ($event instanceof DEvent) {
//			if (isset($event->params['cuepoints'])) {
//				self::$cuepoints = array_merge(self::$cuepoints, $event->params['cuepoints']);
//			}
//		}
// 
//		if (!empty(self::$cuepoints)) {
//			$arrayLoad['cuepoints'] = self::$cuepoints;
//		}

		$arrayLoad['debug'] = (YII_DEBUG ? 'true' : 'false' );
		$arrayLoad['assetUrl'] = App7020Helpers::getAssetsUrl();
		$arrayLoad['askGuruController'] = Docebo::createApp7020Url('askGuru');
		$arrayLoad['analyticsController'] = Docebo::createApp7020Url('analytics');
		$arrayLoad['axAnalyticsController'] = Docebo::createApp7020Url('axAnalytics');
		$arrayLoad['knowledgeLibraryController'] = Docebo::createApp7020Url('knowledgeLibrary');
		$arrayLoad['peerReviewController'] = Docebo::createApp7020Url('knowledgeLibrary/axGetReviewes', array('time' => time()));
		$arrayLoad['peerReviewControllerAdd'] = Docebo::createApp7020Url('knowledgeLibrary/axAddReview');
		$arrayLoad['peerReviewControllerUpdate'] = Docebo::createApp7020Url('knowledgeLibrary/axUpdateReview');
		$arrayLoad['peerReviewControllerPublish'] = Docebo::createApp7020Url('knowledgeLibrary/axPublishContent');
		$arrayLoad['axAssetsController'] = Docebo::createApp7020Url('axAssets');
		$arrayLoad['AskTheExpertController'] = Docebo::createApp7020Url('AskTheExpert');
		$arrayLoad['AskTheExpertControllerAbs'] = Docebo::createAbsoluteApp7020Url('AskTheExpert');
		$arrayLoad['peerReviewControllerUnPublish'] = Docebo::createApp7020Url('knowledgeLibrary/axUnPublishContent');
		$arrayLoad['peerReviewControllerDelete'] = Docebo::createApp7020Url('knowledgeLibrary/axDeleteReview');
		$arrayLoad['peerReviewSong'] = Yii::app()->theme->baseUrl . '/audio/beep.wav';
		$arrayLoad['userAvatarUrl'] = CoreUser::getAvatarByUserId(Yii::app()->user->id);
		$arrayLoad['userAvatar'] = Yii::app()->controller->widget('common.widgets.App7020Avatar', array('userId' => Yii::app()->user->id), true);
		$arrayLoad['uploadingExtentions'] = App7020Assets::getAllowesTypesForUploader(false);
		$arrayLoad['guruDashboardController'] = Docebo::createApp7020Url('guruDashboard');
		$arrayLoad['assetsAxController'] = Docebo::createApp7020Url('axAssets');
		$arrayLoad['typehead'] = Docebo::createApp7020Url('axAssets/axAddTag');
		$arrayLoad['expertsController'] = Docebo::createApp7020Url('experts');
		$arrayLoad['contributeController'] = Docebo::createApp7020Url('axAssets');
		$arrayLoad['mainContributeController'] = Docebo::createApp7020Url('assets');
		$arrayLoad['assetSummaryController'] = Docebo::createApp7020Url('AssetSummary');
		$arrayLoad['channelSummaryController'] = Docebo::createApp7020Url('channelSummary');
		$arrayLoad['channelsController'] = Docebo::createApp7020Url('ChannelsManagement');
		$arrayLoad['axChannelsController'] = Docebo::createApp7020Url('axChannels');
		$arrayLoad['share7020AppController'] = Docebo::createAdminUrl('Share7020App/Share7020App/');
		$arrayLoad['lmsID'] = Docebo::getErpInstallationId();
		$arrayLoad['expertsChannelsController'] = Docebo::createApp7020Url('expertsChannels');
		$arrayLoad['expertsAutoComplete'] = Docebo::createApp7020Url('expertSummary/expertsAutocomplete');
		$arrayLoad['axExpertsChannelsController'] = Docebo::createApp7020Url('axExpertsChannels');
		$arrayLoad['xApiEndpoint'] = Docebo::getCurrentDomain() . 'tcapi/';
		$arrayLoad['inviteToWatch'] = Docebo::createApp7020Url('inviteToWatch');
		$arrayLoad['channels'] = Docebo::createApp7020Url('channels/index');
		$arrayLoad['activityController'] = Docebo::createApp7020Url('activity');
		$arrayLoad['reportManagementController'] = Docebo::createAdminUrl('reportManagement');
		$arrayLoad['user'] = self::userInfo();
		$arrayLoad['S3Path'] = Docebo::filesPathAbs();
		$arrayLoad['currentDomain'] = Docebo::getCurrentDomain();
		$arrayLoad['myActivitiesController'] = Docebo::createAppUrl('myActivities');
		$arrayLoad['dateFormat'] = Yii::app()->localtime->getDateFormatFromSettings(Yii::app()->user->id);
		$arrayLoad['userTimezone'] = Yii::app()->localtime->getTimeZone();
		$arrayLoad['isActiveGoogleDrive'] = PluginManager::isPluginActive('GoogleDriveApp');
		$arrayLoad['googleApiKey'] = Settings::get("googledrive_api_key");
		$arrayLoad['googleClientId'] = Settings::get("googledrive_client_id");

		// Timeframe for "Coaching Activity" page
		$arrayLoad['timeframeCoachActivity'] = array(
			'daily' => App7020QuestionAnswerStatistic::TIMEFRAME_DAILY,
			'weekly' => App7020QuestionAnswerStatistic::TIMEFRAME_WEEKLY,
			'monthly' => App7020QuestionAnswerStatistic::TIMEFRAME_MONTHLY,
			'yearly' => App7020QuestionAnswerStatistic::TIMEFRAME_YEARLY
		);
		$arrayLoad['timeframePeerReviewActivity'] = array(
			'daily' => App7020PeerReviewStatistic::TIMEFRAME_DAILY,
			'weekly' => App7020PeerReviewStatistic::TIMEFRAME_WEEKLY,
			'monthly' => App7020PeerReviewStatistic::TIMEFRAME_MONTHLY,
			'yearly' => App7020PeerReviewStatistic::TIMEFRAME_YEARLY
		);

		// Convert values to UTF-8 before doing any json encoding
		foreach ($arrayLoad as $key => $value) {
			if (is_string($value))
				$arrayLoad[$key] = utf8_encode($value);
		}

		$js = " var app7020Options = " . json_encode($arrayLoad);
		Yii::app()->getClientScript()->registerScript("app7020options" . null, $js, CClientScript::POS_HEAD);
	}

	/**
	 * Register All Upload settings and assets 
	 * ?deprecated since version 2
	 */
	public static function registerApp7020UploadSettings() {
		$uploadUrl = Docebo::createLmsUrl('site/AxUploadFile', array('type' => FileTypeValidator::TYPE_APP7020_CONTENT));
		$gotItUrl = Docebo::createLmsUrl('site/AxSetGotIt');
		$csrfToken = Yii::app()->request->csrfToken;
		$flashUrl = Yii::app()->plupload->getAssetsUrl() . "/plupload.flash.swf";
		$controllerPath = Docebo::createApp7020Url('axAssets');
		$fcbkAutocompleteUrl = Docebo::createApp7020Url('axAssets/axGetAutoCompleteTags');


		//Build JS String 
		$js = 'var $uploadUrl = ' . json_encode($uploadUrl) . PHP_EOL;
		$js = 'var $gotItUrl = ' . json_encode($gotItUrl) . PHP_EOL;
		$js .= 'var $csrfToken = ' . json_encode($csrfToken) . PHP_EOL;
		$js .= 'var $flashUrl = ' . json_encode($flashUrl) . PHP_EOL;
		$js .= 'var $controllerPath = ' . json_encode($controllerPath) . PHP_EOL;
		$js .= 'var $fcbkAutocompleteUrl = ' . json_encode($fcbkAutocompleteUrl) . PHP_EOL;
		//attach js to header of layout
		Yii::app()->getClientScript()->registerScript("App7020UploadSettings" . null, $js, CClientScript::POS_HEAD);
	}

	/**
	 * Clear blank spases before and after comas, preapre for implode
	 * @uses $tmpArray userd for Upload extentions definitions in PLUploader
	 * @param string $stringWithComaSeparatedData
	 * @return string
	 */
	public static function clearComaStringInArray($stringWithComaSeparatedData) {
		$arrayFromString = explode(',', $stringWithComaSeparatedData);
		$tmpArray = array();
		foreach ($arrayFromString as $ext) {
			$tmpArray[] = trim($ext);
		}
		return implode(',', $tmpArray);
	}

	/**
	 * Reduces an array to a given count. The returned elements are equidistant from each other. Always returns first and last elements.
	 * 
	 * @param array $givenArray
	 * @param integer $returnCount
	 * @return array
	 */
	public static function reduceArray($givenArray, $returnCount = 4) {

		if (!$givenArray || !is_array($givenArray)) {
			return array();
		}

		if (intval($returnCount) < 1) {
			return array();
		}

		if (count($givenArray) <= $returnCount) {
			return $givenArray;
		}

		$returnArray = array();
		$slices = round((count($givenArray) - 1) / ($returnCount - 1));
		$i = 1;
		$j = 1;
		foreach ($givenArray as $value) {
			if ($j == $i && $i != count($givenArray) && count($returnArray) < $returnCount - 1) {
				$returnArray[] = $value;
				$j += $slices;
			}
			if ($i == count($givenArray)) {
				$returnArray[] = $value;
				return $returnArray;
			}

			$i++;
		}
	}

	/**
	 * Check if there is <img> in $subject. If yes returns Font Awsome icon
	 * @param str $subject
	 * @return string
	 */
	public static function searchHTMLforImg($subject) {
		$faIcon = '<i class="fa fa-picture-o"></i>';
		$pattern = '/(\<img(.*)\/>)/';
		$isImg = preg_match($pattern, $subject);
		if ($isImg) {
			return $faIcon;
		}
	}

	/**
	 * Converts JSON node array to appropriate PHP array
	 * @param string $nodeArray
	 * @return array
	 *
	 */
	public static function convertNodeJSON($nodeArray) {
		$returnArray = array();
		$arrayTemp = json_decode($nodeArray, true);
		foreach ($arrayTemp as $value) {
			$returnArray[$value['key']] = $value['selectState'];
		}
		return $returnArray;
	}

	/**
	 * Returns number of selected Topics, includind theirself and there's children
	 * @param str $selectedTopics - string of comma separated topics
	 * @return number selected topicsd
	 */
	public static function getNumberSelectedTopics($selectedTopics) {
		return 0;
	}

	/**
	 * Get pure text without any html (removed "&nbsp;" to)
	 * @param str $html
	 * @return str - without any html
	 */
	public static function getPureText($html) {
		$pureText = trim(preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($html)));
		return $pureText;
	}

	/**
	 * Convert Object to Array
	 * @param obj $obj
	 * @return array
	 */
	public static function converObjToArray($obj) {
		$arr = array();
		foreach ($obj as $key => $value) {
			$arr[$key] = $value;
		}
		return $arr;
	}

	/**
	 * Converts 2 dimensional to flat array
	 * @param array $errors - 2 dimensional array
	 * @return array (flat)
	 */
	public static function convertErrorsToFlatArray($errors) {
		$errorsArray = array();
		if (is_array($errors)) {
			foreach ($errors as $error) {
				foreach ($error as $err) {
					$errorsArray[] = $err;
				}
			}
		}

		return $errorsArray;
	}

	/**
	 * Return video/audio meta data such as duration, author and etc.
	 * @param $file - Physical path to file stored on the server
	 * @return array File ID3 Meta Tags
	 */
	public static function getID3Tags($file) {
		Yii::import('common.vendors.*');
		require_once 'getid3/getid3.php';
		$getID3 = new getID3();
		$fileInfo = $getID3->analyze($file);
		return $fileInfo;
	}

	/**
	 * Return video/audio duration as string or secodns.
	 * @param $file - Physical path to file stored on the server
	 * @param $inSeconds if true the file duration will be returned in seconds else is returned as a string exmple "00:20"
	 * @return string
	 */
	public static function getVideoDuration($file, $inSeconds = false) {
		$videoFullInfo = self::getID3Tags($file);
		if (!$inSeconds) {
			$videoDuration = $videoFullInfo['playtime_string'];
		} else {
			$videoDuration = $videoFullInfo['playtime_seconds'];
		}
		return $videoDuration;
	}

	/**
	 * Compare 2 playtime durations.
	 * @param $biggerDuration playtime string - if this is bigger returns true
	 * @param $smallerDuration playtime string - if this is bigger returns false
	 * @return bool
	 */
	public static function compareDuration($biggerDuration, $smallerDuration) {
		$biggerDuration = self::durToSeconds($biggerDuration);
		$smallerDuration = self::durToSeconds($smallerDuration);
		if ($biggerDuration >= $smallerDuration) {
			return true;
		}
		return false;
	}

	/**
	 * Convert string duration to seconds
	 * @param $duration playtime string
	 * @return int
	 */
	public static function durToSeconds($duration) {
		$duration = explode(":", $duration);
		$time_seconds = count($duration) > 2 ? $duration[0] * 3600 + $duration[1] * 60 + $duration[2] : $duration[0] * 60 + $duration[1];
		return $time_seconds;
	}

	/**
	 * Sorts a mutidimentional array by given field and vector
	 * @param array $array 
	 * @param string $field 
	 * @param string $vector 
	 * @return array
	 */
	public static function sortMultyArray($array, $field, $vector) {
		$sort = array();
		foreach ($array as $key => $value) {
			$sort[$key] = $value[$field];
		}

		if (strtoupper($vector) == 'DESC') {
			array_multisort($sort, SORT_DESC, $array);
		} else {
			array_multisort($sort, SORT_ASC, $array);
		}

		return $array;
	}

	public static function sortArrayAlphabetical($array, $field, $vector) {
		$arr = array_column($array, $field);
		$keys = array_flip($arr);
		$key_map = array();
		foreach ($keys as $key=>$value){
			$key_map[strtolower($key)] = $key;
		}

		ksort($key_map);
		$trash = array();
		foreach ($key_map as $key=>$value) {
			$keyOfObject = array_search($value, array_column($array, $field));

			if (is_numeric($keyOfObject) && $keyOfObject !== false) {
				$trash[] = $array[$keyOfObject];
			}
		}
		if (strtoupper($vector) == 'DESC') {
			return array_reverse($trash);
		}

		return $trash;
	}

	/**
	 * Check if can "Invite to watch" for this content
	 * @param int $contentId
	 * @return boolean
	 */
	public static function hasContentInviteWatch($contentId) {
		$PeerReviewModel = App7020SettingsPeerReview::model()->find(array(
			'condition' => 'idContent = :idContent',
			'params' => array(':idContent' => $contentId),
		));
		if ($PeerReviewModel) {
			$isInviteWatch = $PeerReviewModel->inviteToWatch ? true : false;
		} else {
			$isInviteWatch = ShareApp7020Settings::isInviteWatchGlobal();
		}
		return $isInviteWatch;
	}

	/**
	 * This method check custom added rows inside CoreSettingUser and return if exist (bool)TRUE else (bool)FALSE
	 * @param string $key
	 * @param int $idUser
	 * @return boolean
	 */
	public static function actionGetGotItByAttributes($key = false, $idUser = false) {
		if ($key) {
			if ($idUser === false) {
				$idUser = Yii::app()->user->idst;
			}
			$userSetting = CoreSettingUser::model()->findByAttributes(array('path_name' => $key, 'id_user' => $idUser));
			if ($userSetting) {
				if ($userSetting->value == 1) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Return vlaid or not is url
	 * @param Array  $params
	 * @param string $attr
	 * @return boolean
	 */
	public static function isValidUrl($url) {

		$url = preg_replace("/(^:\/\/|^\/\/|^(?!https?:\/\/))(.*)/im", "http://$2", $url);

		$res = @get_headers($url);

		if (!$res) {
			return false;
		}

		preg_match('/(.*)\s(\d{3})\s(.*)$/', $res[0], $arr);

		if ((int) $arr[2] == 200 || substr($arr[2], 0, 2) == "30") {
			return $url;
		} else {
			return false;
		}
	}

	/**
	 * Prepare Policy for plUploader
	 * 
	 * @return String
	 */
	static public function getUploadPolicy() {
		$settings = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);

		$bucket = $settings->getS3BucketName();

		// these can be found on your Account page, under Security Credentials > Access Keys
		// prepare policy
		$arr = array(
			// ISO 8601 - date('c'); generates uncompatible date, so better do it manually
			'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),
			'conditions' => array(
				array('bucket' => $bucket),
				array('acl' => 'public-read'),
				array('starts-with', '$key', ''),
				// for demo purposes we are accepting only images
				array('starts-with', '$Content-Type', ''),
				// Plupload internally adds name field, so we need to mention it here
				array('starts-with', '$name', ''),
				// One more field to take into account: Filename - gets silently sent by FileReference.upload() in Flash
				// http://docs.amazonwebservices.com/AmazonS3/latest/dev/HTTPPOSTFlash.html
				array('starts-with', '$Filename', ''),
			)
		);

		return base64_encode(json_encode($arr));
	}

	/**
	 * Get Private signature
	 * @return string
	 */
	static public function getPlUploadSignature() {
		$settings = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);

		// sign policy
		$algo = 'sha1';
		$policy = self::getUploadPolicy();
		$secret = $settings->getS3Secret();
		return base64_encode(hash_hmac('sha1', $policy, $settings->getS3Secret(), true)
		);
	}

	static function getPlOptions() {
		$settings = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
		$arr = array();
		$arr['url'] = 'https://' . $settings->getS3BucketName() . '.s3.amazonaws.com/';

		$arr['multipart'] = true;
		$arr['multipart_params'] = array(
			'key' => '${filename}', // use filename as a key
			'Filename' => '${filename}', // adding this to keep consistency across the runtimes
			'acl' => 'public-read',
			'Content-Type' => '',
			'AWSAccessKeyId' => $settings->getS3Key(),
			'policy' => self::getUploadPolicy(),
			'signature' => self::getPlUploadSignature()
		);
		// optional, but better be specified directly
//        $arr['file_data_name'] = 'file';
		$arr['flash_swf_url'] = Yii::app()->theme->baseUrl . '/js/plupload2/Moxie.swf';

		$arr['filters'] = array(
			// Maximum file size
//            'max_file_size' => '100mb',
//            // Specify what files to browse for
			'mime_types' =>
			App7020Assets::getAllowesTypesForUploader(false)
		);

		return json_encode($arr, 128);
	}

	/**
	 * 
	 * @param type $needleExt
	 * @return boolean|integer
	 */
	public static function generateHashedFilename($filename) {

		$extention = self::getExtentionByFilename($filename);
		$hashedName = md5($filename . time() . Yii::app()->user->idst . rand(1, 1000000));

		return $hashedName . "." . $extention;
	}

	/**
	 * validate filename is audio or not
	 * @param $ext string
	 * @return bool
	 *
	 */
	public static function isAudioFile($ext) {
		$audio = App7020Assets::$contentTypes;
		if (in_array($ext, $audio[App7020Assets::CONTENT_TYPE_DEFAULT_MUSIC]['allowed'])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * redirects to index action of the current controller if the user comes from login page after session expired
	 * 
	 */
	public static function redirectToIndexIfSessionExpired($controllerName) {
		if (isset($_GET['login']) && $_GET['login'] == 1) {
			header("Location: " . Docebo::createAbsoluteApp7020Url($controllerName . '/index'));
		}
	}

	/**
	 * When is puted any model return their attributes as array
	 * @param mixed $modelObject
	 * @return array
	 */
	static function returnAtributesByObject($modelObject) {
		return $modelObject->getAttributes();
	}

	/**
	 * Delete folder from S3 Colection with all included files
	 * @param App7020Content $content model with contents
	 * @return boolean
	 */
	static function deleteCloudFile(App7020Assets $content) {
		try {
			$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020);
			return $storage->removeFolder(App7020Helpers::getAmazonInputKey($content->filename));
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Delete local LMS file 
	 * @param App7020Content $content model with contents
	 * @return boolean
	 */
	static function deleteLocalFile(App7020Assets $content) {
		try {
			$file = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $content->filename;
			return @unlink($file);
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Get current user info
	 * @return array
	 */
	public static function userInfo() {
		$user = Yii::app()->user->loadUserModel();
		$userModel = CoreUser::model()->findByPk(Yii::app()->user->id);
		return $userArr = array(
			'id' => $user->idst,
			'username' => $user->userid,
			'pass' => $user->pass,
			'firstname' => $user->firstname,
			'lastname' => $user->lastname,
			'email' => $user->email,
			'fullName' => $userModel->getFullName()
		);
	}

	/**
	 * Sum array values by given key
	 * @param 2D array $array
	 * @param str/int $key
	 * @return int
	 */
	public static function arrSumByKey($array, $key) {
		return array_sum(array_column($array, $key));
	}

	/**
	 * Get user ids of mentioned people
	 * @param str $html - entire html string to search in
	 * @param str $userElement - expample: <span class="tagged-name userId-12301">@Super Admin</span>
	 * @param str $userClass - class which holds user ids, example: 'userId-12301'
	 * @return array:
	 *  1) example: array(12301 => 12301, 15430=>15430) user id is used as array key to be sure that there is no duplicated users
	 * 	2) empty array if no users mantioned
	 */
	public static function getMentionedUsers($html, $userElement = 'span[class=tagged-name]', $userClass = 'userId-') {
		Yii::import('common.extensions.simplehtmldom.SimpleHTMLDOM');
		$simpledom = new SimpleHTMLDOM();
		$dom = $simpledom->str_get_html($html);
		$selectedUsers = array();
		foreach ($dom->find($userElement) as $tagged) {
			$classes = $tagged->getAllAttributes()['class'];
			$userClassPosition = strpos($classes, $userClass);
			if ($userClassPosition !== false) {
				$userId = substr($classes, $userClassPosition + strlen($userClass));
				$selectedUsers[$userId] = $userId;
			}
		}

		return $selectedUsers;
	}

	/**
	 * Standart cURL
	 * @param str $url
	 * @return array
	 */
	public static function curl($url) {
		$options = array(
			CURLOPT_RETURNTRANSFER => true, // return web page
			CURLOPT_HEADER => false, // don't return headers
			CURLOPT_FOLLOWLOCATION => true, // follow redirects
			CURLOPT_ENCODING => "", // handle all encodings
			CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0", // who am i
			CURLOPT_AUTOREFERER => true, // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			CURLOPT_TIMEOUT => 120, // timeout on response
			CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
			// skip SSL certificate check
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
		);

		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);
		curl_close($ch);

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $content;

		return $header;
	}

	/**
	 * Get formated data for ChartJs
	 * @param array $sqlResults - an exmple of usage can be seen App7020AssetSummary.php::viewsStat()
	 * @param int $timeInterval - days (from 1 to 365)
	 * @param str $periodKey - searched key in sql results
	 * @param str $countKey - searched key in sql results
	 * @return array
	 */
	public static function getChartData($sqlResults, $timeInterval = 7, $periodKey = 'period', $countKey = 'count') {
		$timeInterval = is_int($timeInterval) ? $timeInterval : self::TIMEFRAME_WEEKLY;
		$tempChartData = array();
		foreach ($sqlResults as $tempResult) {
			$tempChartData[$tempResult[$periodKey]] = $tempResult[$countKey];
		}

		if ($timeInterval == self::TIMEFRAME_YEARLY) {
			$countDaysMonths = 12;
			$addPeriod = 'month';
		} else {
			$countDaysMonths = $timeInterval;
			$addPeriod = 'day';
		}

		$counter = 1;
		$labels = array();
		$points = array();
		$startDate = strtotime('-' . $timeInterval . ' days', time());
		while ($countDaysMonths > 0) {
			$currDay = strtotime('+' . $counter++ . ' ' . $addPeriod, $startDate);
			$currentDay = date('Y-m-d', $currDay);

			if ($timeInterval == self::TIMEFRAME_YEARLY) {
				$shortDate = substr($currentDay, 0, 7);
				$checkedKey = $shortDate;
				$labels[$checkedKey] = $shortDate;
			} else {
				$checkedKey = $currentDay;
				$labels[$checkedKey] = substr($currentDay, 5);
			}

			if (!array_key_exists($checkedKey, $tempChartData)) {
				$points[$checkedKey] = 0;
			} else {
				$points[$checkedKey] = $tempChartData[$checkedKey];
			}

			$countDaysMonths--;
		}

		return array('labels' => $labels, 'points' => $points);
	}

	/**
	 * Get needed data to create avatar
	 * @param int $userId
	 * @return array
	 */
	public static function getUserAvatar($userId) {
		$commandBase = Yii::app()->db->createCommand();
		$commandBase->select("
			firstname,
			lastname,
			TRIM(LEADING '/' FROM userid) AS username,
			avatar
		");
		$commandBase->where('idst = :userId');
		$commandBase->from(CoreUser::model()->tableName());

		return $commandBase->queryRow(true, array(':userId' => $userId));
	}

	/**
	 * Make from ChartJs img and save it. Just get base64 from canvas generated by ChartJs.
	 * @param str $base64
	 * @param str $imgName
	 * @return boolean/str (path to img)
	 */
	public static function makeChartjsJpg($base64, $imgName = 'chart_img') {
		if ($base64) {
			try {
				$imgData = str_replace(' ', '+', $base64);
				$imgData = substr($imgData, strpos($imgData, ",") + 1);
				$imgData = base64_decode($imgData);
				$chartImgName = $imgName . microtime(true) * 10000 . '_' . substr(md5(uniqid(rand(), true)), 0, 5) . '.jpg';
				// Path where the image is going to be saved
				$filePath = Yii::app()->basePath . '/runtime/tmpPdf/' . $chartImgName;
				// Write $imgData into the image file
				$fileAssets = fopen($filePath, 'w');
				fwrite($fileAssets, $imgData);
				fclose($fileAssets);
			} catch (Exception $exc) {
				$filePath = false;
			}
		} else {
			$filePath = false;
		}
		return $filePath;
	}

	/**
	 * Get string for timeframe by given int (example: 1->Daily)
	 * @param int $timeframe
	 * @return str
	 */
	public static function timeframeStr($timeframe) {
		$timeframeArr = array(
			self::TIMEFRAME_DAILY => Yii::t('app7020', 'Daily'),
			self::TIMEFRAME_WEEKLY => Yii::t('app7020', 'Weekly'),
			self::TIMEFRAME_MONTHLY => Yii::t('app7020', 'Monthly'),
			self::TIMEFRAME_YEARLY => Yii::t('app7020', 'Yearly')
		);
		return $timeframeArr[$timeframe];
	}

    /**
     * @param string with html tags $html
     * @param int $maxLength | default 100
     * @param string $ending | default '...'
     * @return string
     */
    static function truncateAbstractHTMLContents($html, $maxLength = 100, $ending = '...')
    {
        mb_internal_encoding("UTF-8");
        $printedLength = 0;
        $position = 0;
        $tags = array();
        $newContent = '';

        $html = $content = preg_replace("/<img[^>]+\>/i", "", $html);

        while ($printedLength < $maxLength && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match, PREG_OFFSET_CAPTURE, $position)) {
            list($tag, $tagPosition) = $match[0];
            // Print text leading up to the tag.
            $str = mb_strcut($html, $position, $tagPosition - $position);
            if ($printedLength + mb_strlen($str) > $maxLength) {
                $newstr = mb_strcut($str, 0, $maxLength - $printedLength);
                $newstr = preg_replace('~\s+\S+$~', '', $newstr);
                $newContent .= $newstr;
                $printedLength = $maxLength;
                break;
            }
            $newContent .= $str;
            $printedLength += mb_strlen($str);
            if ($tag[0] == '&') {
                // Handle the entity.
                $newContent .= $tag;
                $printedLength++;
            } else {
                // Handle the tag.
                $tagName = $match[1][0];
                if ($tag[1] == '/') {
                    // This is a closing tag.
                    $openingTag = array_pop($tags);
                    assert($openingTag == $tagName); // check that tags are properly nested.
                    $newContent .= $tag;
                } else if ($tag[mb_strlen($tag) - 2] == '/') {
                    // Self-closing tag.
                    $newContent .= $tag;
                } else {
                    // Opening tag.
                    $newContent .= $tag;
                    $tags[] = $tagName;
                }
            }

            // Continue after the tag.
            $position = $tagPosition + mb_strlen($tag);
        }

        // Print any remaining text.
        if ($printedLength < $maxLength && $position < mb_strlen($html)) {
            $newstr = mb_strcut($html, $position, $maxLength - $printedLength);
            $newstr = preg_replace('~\s+\S+$~', '', $newstr);
            $newContent .= $newstr;
        }

        // Close any open tags.
        while (!empty($tags)) {
            $newContent .= sprintf('</%s>', array_pop($tags));
        }

        return $newContent;
    }

}
