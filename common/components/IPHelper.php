<?php

/**
 * General class for IP related helper functions
 *
 * Based on the following library with some modifications:
 * http://www.pgregg.com/projects/php/ip_in_range/ (v1.2)
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 * @author Paul Gregg <pgregg@pgregg.com>
 */

class IPHelper {

	/**
	 * Check if the IP whitelist functionality is enabled
	 * and if yes - enforce it
	 */
	static public function checkForIPwhitelist(){
		$ipFilerEnforced = Settings::get('ip_filter_enabled') && Settings::get('ip_filter_enabled')!='off';

		// Get the client IP
		$clientIps = IPHelper::getRemoteIp(true);

		$ipFilters = CoreIpFilter::model()->findAll();

		// ERP=>LMS calls are always allowed (skipping IP filter)
		$erpServerIPs   = isset(Yii::app()->params['erpIpAddresses']) ? Yii::app()->params['erpIpAddresses'] : array();
		if (is_array($clientIps) && is_array($erpServerIPs)) {
			$intersect = array_intersect($clientIps, $erpServerIPs);
			$isCallerERP = !empty($intersect);
			if($isCallerERP) return true;
		}


		if($ipFilerEnforced){

			$clientIpIsWhitelisted = false;

			// Check if the current request path is whitelisted (always skips IP filter)
			$currentRequestRoute = Yii::app()->getUrlManager()->parseUrl(Yii::app()->getRequest());

			$skipIpFilter = false;
			$excludedRulesfromIPfilter = isset(Yii::app()->params['ipfilter_ignored_paths']) ? Yii::app()->params['ipfilter_ignored_paths'] : array();
			$request = Yii::app()->request; /* @var $request DoceboHttpRequest */
			$globalExcludedPaths = $request->noCsrfValidationRoutes;
			$excludedRulesfromIPfilter = array_merge($globalExcludedPaths, $excludedRulesfromIPfilter);

			foreach($excludedRulesfromIPfilter as $cr){
				if (preg_match('#^'.$cr.'$#', $currentRequestRoute)) {
					$skipIpFilter = true;
					break;
				}
			}
			if($skipIpFilter) return true;

			if(empty($ipFilters)){
				Yii::log('IP filter enabled but no valid rules present. Deny all requests...', CLogger::LEVEL_ERROR);
			}else{
				// Finally, validate the client IP with the whitelist of IPs from DB
				foreach ($clientIps as $clientIp) {
					foreach ($ipFilters as $ipFilter) {
						if ($ipFilter->ipMatchesFilter($clientIp)) {
							$clientIpIsWhitelisted = TRUE;
							break;
						}
					}
					// One of the filters matched, no need to iterate further the client IPs
					if ($clientIpIsWhitelisted) break;
				}
			}

			if(!$clientIpIsWhitelisted){
				Yii::app()->controller->redirect('/offline.php');
				Yii::app()->end();
			}
		}
	}


	/**
	 * This function takes 2 arguments, an IP address and a "range"
	 * in several different formats.
	 *
	 * @param $ip
	 * @param $rangeOrSingleIp String One of the following possible formats:
	 *                         1. Wildcard format:     1.2.3.*
	 *                         2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
	 *                         3. Start-End IPv4 format: 1.2.3.0-1.2.3.255
	 *                         4. Exact IPv4 address:    111.111.111.111
	 *                         5. Exact IPv6 address:  fe80:01::af0 or fe80:0001:0000:0000:0000:0000:0000:0af0 (expanded)
	 *
	 *                         The function will return true if the supplied IP is within the range.
	 *                         Note little validation is done on the range inputs - it expects you to
	 *                         use one of the above formats.
	 *
	 * @return bool
	 */
	static public function ipInRange ( $ip, $rangeOrSingleIp ) {
		$isClientIpV6 = (bool) filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 );
		$isClientIpV4 = (bool) filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );

		if( ! $isClientIpV4 && ! $isClientIpV6 ) {
			// Invalid client IP
			return FALSE;
		}

		if( $isClientIpV6 ) {

			$rangeIsIPv4 = (bool) filter_var( $rangeOrSingleIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
			$rangeIsIPv6 = (bool) filter_var( $rangeOrSingleIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 );

			if( $rangeIsIPv6 ) {
				$str1 = self::ipv6ToNum( $rangeOrSingleIp );
				$str2 = self::ipv6ToNum( $ip );
				if( strlen( $str1 ) === strlen( $str2 ) && $str1 == $str2 ) {
					return TRUE;
				}

			} else {
				Yii::log( 'Client IP is in IPv6 (' . $ip . ') but the IP range/filter is not a valid and convertable IPv4 or a valid IPv6 address (' . $rangeOrSingleIp . ')', CLogger::LEVEL_WARNING );

				return FALSE;
			}

		} elseif( $isClientIpV4 ) {

			if( strpos( $rangeOrSingleIp, '/' ) !== FALSE ) {
				// $range is in IP/NETMASK format
				list( $rangeOrSingleIp, $netmask ) = explode( '/', $rangeOrSingleIp, 2 );
				if( strpos( $netmask, '.' ) !== FALSE ) {
					// $netmask is a full 255.255.0.0 format
					$netmask     = str_replace( '*', '0', $netmask );
					$netmask_dec = ip2long( $netmask );

					return ( ( ip2long( $ip ) & $netmask_dec ) == ( ip2long( $rangeOrSingleIp ) & $netmask_dec ) );
				} else {
					// $netmask is a CIDR size block
					// fix the range argument
					$x = explode( '.', $rangeOrSingleIp );
					while ( count( $x ) < 4 ) {
						$x[] = '0';
					}
					list( $a, $b, $c, $d ) = $x;
					$rangeOrSingleIp = sprintf( "%u.%u.%u.%u", empty( $a ) ? '0' : $a, empty( $b ) ? '0' : $b, empty( $c ) ? '0' : $c, empty( $d ) ? '0' : $d );
					$range_dec       = ip2long( $rangeOrSingleIp );
					$ip_dec          = ip2long( $ip );

					# Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
					#$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));

					# Strategy 2 - Use math to create it
					$wildcard_dec = pow( 2, ( 32 - $netmask ) ) - 1;
					$netmask_dec  = ~$wildcard_dec;

					return ( ( $ip_dec & $netmask_dec ) == ( $range_dec & $netmask_dec ) );
				}
			} else {
				// range might be 255.255.*.* or 1.2.3.0-1.2.3.255 or an exact IP address

				if( strpos( $rangeOrSingleIp, '*' ) !== FALSE ) { // a.b.*.* format
					// Just convert to A-B format by setting * to 0 for A and 255 for B
					$lower           = str_replace( '*', '0', $rangeOrSingleIp );
					$upper           = str_replace( '*', '255', $rangeOrSingleIp );
					$rangeOrSingleIp = "$lower-$upper";
				}

				if( strpos( $rangeOrSingleIp, '-' ) !== FALSE ) { // A-B format
					list( $lower, $upper ) = explode( '-', $rangeOrSingleIp, 2 );
					$lower_dec = (float) sprintf( "%u", ip2long( $lower ) );
					$upper_dec = (float) sprintf( "%u", ip2long( $upper ) );
					$ip_dec    = (float) sprintf( "%u", ip2long( $ip ) );

					return ( ( $ip_dec >= $lower_dec ) && ( $ip_dec <= $upper_dec ) );
				} else {
					// Probably an exact IP address provided for "range"
					// Compare the passed IP to that exact IP value
					$ip_dec          = (float) sprintf( "%u", ip2long( $ip ) );
					$rangeOrSingleIp = (float) sprintf( "%u", ip2long( $rangeOrSingleIp ) );

					return $ip_dec == $rangeOrSingleIp;
				}
			}
		}

		Yii::log( "IP filter validation failed. Client IP is ($ip), IP range value is {$rangeOrSingleIp}", CLogger::LEVEL_WARNING );

		return FALSE;
	}

	
	
	/**
	 * Try to detect remote IP address, based on various possible SERVER values
	 *
	 * All these values (from$_SERVER) are guaranteed to be strings, including empty one.
	 * We make each of them an array and then, based on passed parameter, return either the array or the first element as a string.
	 *
	 * The reason for array approach is because sometimes HTTP_X_FORWARDED_FOR may return comma separated list of IPs.
	 * Perhaps, other values also could be the same.
	 *
	 * @param boolean $asArray
	 * @return array|string
	 */
	public static function getRemoteIp($asArray=false) {

		if($asArray){
			// Collect all possible IPs from all request headers
			// We do this because the client's real IP may be included
			// in any of them depending on the server's load balancer
			// or Apache/NGINX configs
			$ips = array();
			foreach(array('REMOTE_ADDR', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP') as $header){
				if(array_key_exists($header, $_SERVER)){
					$parts = explode(',', $_SERVER[$header]);
					foreach($parts as $singleIp){
						$singleIp = trim($singleIp);
						if(!in_array($singleIp, $ips)){
							$ips[] = $singleIp;
						}
					}
				}
			}
			return $ips;
		}else {


			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ips = explode(',', trim($_SERVER['HTTP_X_FORWARDED_FOR']));
			} else if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ips = explode(',', trim($_SERVER['HTTP_CLIENT_IP']));
			} else if (isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR'])) {
				$ips = explode(',', trim($_SERVER['REMOTE_ADDR']));
			} else {
				// Total fail, no IP!
				return false;
			}

			// Trim spaces
			if (is_array($ips)) {
				foreach ($ips as $k => $v) {
					$ips[$k] = trim($v);
				}
			}

			// Return first element
			return reset($ips);
		}
	
	}
	
	
	/**
	 * Try to convert a given IPv4 or IPv6 to an expanded IPv6 version
	 *
	 * Examples:
	 * 127.0.0.1 => 7f00:0001
	 * fe80:01::af0 => fe80:0001:0000:0000:0000:0000:0000:0af0
	 *
	 * Already expanded IPv6 addresses are returned without changes, e.g.
	 * 2001:0db8:0abc:17ff:ffff:ffff:ffff:ffff
	 *
	 * @param $ip
	 *
	 * @return string
	 */
	private static function expandToIPv6 ( $ip ) {
		$hex = unpack( "H*hex", inet_pton( $ip ) );
		$ip  = substr( preg_replace( "/([A-f0-9]{4})/", "$1:", $hex['hex'] ), 0, - 1 );

		return $ip;
	}

	private static function ipv6ToNum ( $ip ) {
		$binaryNum = '';
		foreach ( unpack( 'C*', inet_pton( $ip ) ) as $byte ) {
			$binaryNum .= str_pad( decbin( $byte ), 8, "0", STR_PAD_LEFT );
		}
		$binToInt = base_convert( ltrim( $binaryNum, '0' ), 2, 10 );

		return $binToInt;
	}

	/**
	 * dtr_pton
	 *
	 * Converts a printable IP into an unpacked binary string
	 *
	 * @author Mike Mackintosh - mike@bakeryphp.com
	 *
	 * @param string $ip
	 *
	 * @return string $bin
	 * @throws \Exception
	 */
	static private function dtr_pton ( $ip ) {


		if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			return current( unpack( "A4", inet_pton( $ip ) ) );
		} elseif( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ) {
			return current( unpack( "A16", inet_pton( $ip ) ) );
		}

		throw new \Exception( "Please supply a valid IPv4 or IPv6 address" );
	}

	/**
	 * dtr_ntop
	 *
	 * Converts an unpacked binary string into a printable IP
	 *
	 * @author Mike Mackintosh - mike@bakeryphp.com
	 *
	 * @param string $str
	 *
	 * @return string $ip
	 * @throws \Exception
	 */
	static private function dtr_ntop ( $str ) {
		if( strlen( $str ) == 16 OR strlen( $str ) == 4 ) {
			return inet_ntop( pack( "A" . strlen( $str ), $str ) );
		}

		throw new \Exception( "Please provide a 4 or 16 byte string" );
	}

}