<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 4.1.2016 г.
 * Time: 18:54 ч.
 */
abstract class ProgressiveJobHandler extends CComponent implements IProgressiveJobHandler {

	/**
	 * Job model which this handler is working with (must be set upon descendants creation)
	 *
	 * @var CoreJob
	 */
	public $job = NULL;

	protected $currentOffset = 0;

	/**
	 * Initialize the job handler component
	 *
	 * @param array|bool $params Array of arbitrary parameters
	 *
	 * @throws \CException
	 */
	public function init ( $params = FALSE ) { }


	/**
	 * Takes a single chunk, processes it
	 *
	 * @param bool $dynamicParams
	 *
	 * @return mixed
	 * @throws \CException
	 */
	public function run ( $dynamicParams = FALSE ) {
		$existingJobParams = CJSON::decode( $this->job->params );

		$this->currentOffset = $existingJobParams['offset'];
		$totalCount          = $this->getTotalItemsCount();

		$chunk = static::getChunk( $this->currentOffset, $existingJobParams );

		$this->processChunk( $chunk, $existingJobParams );

		$nextOffset = $this->currentOffset + static::ITEMS_PAGE_PAGE;

		if( $nextOffset < $totalCount ) {
			// Schedule next iteration
			static::scheduleNextIteration( $nextOffset, $existingJobParams );
		} else {
			// No more rows
			static::afterProgressiveJobComplete();
		}
	}

	static public function scheduleNextIteration ( $offset = 0, array $newJobParams = array() ) {
		$newJobParams['offset'] = $offset;
		$scheduler              = Yii::app()->scheduler;

		/* @var $scheduler SchedulerComponent */

		return $scheduler->createImmediateJob( get_called_class(), static::id(), $newJobParams, FALSE, NULL, TRUE );
	}

	/**
	 * Returns whether this handler allows for immediate jobs to be reused
	 * @return bool
	 */
	public function canReuseSameImmediateJob () {
		return FALSE;
	}
}