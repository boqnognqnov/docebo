<?php
/**
 * Job Handlers Interface
 * 
 * All job handlers <strong>must</strong> implement this interface and extend JobHandler class. Period.
 * 
 * Which means, all methods in this interface are mandatory and must be declared/implemented in the Job Handler class.
 * 
 *  @package common.components.job.handlers
 *  
 */
interface IJobHandler {
	
	/**
	 * Initialize the job handler component
	 * @param array $params Array of arbitrary parameters
	 */
	public function init($params = false);
	
	/**
	 * Run the Job the handler is associated with (during creation)
	 * 
	 * Called in a try/catch block, i.e. all exceptions thrown will be catched.
	 * If the method ends with NO exception thrown, job is considered done with success.
	 * 
	 * @param array $params Array of arbitrary parameters
	 * @return void 
	 */
	public function run($params = false);
		
	
	/**
	 * Returns string identifier of the handler CLASS (handler id).
	 * Must be unique LMS-wide, including among plugins and core handlers.
	 * 
	 * Handler ID can be:
	 * 		1) Arbitrary/Random string, in which case, JobHandler::registerHandler(<handler-id>, <class-path>) must be used to
	 * 		   map the ID to a class path run-time. 
	 * 				Example (in plugin module init()): 
	 * 					JobHandler::registerHandler('myown_handler_id', 'plugins.my_plugin.MyJobHandler');
	 * 
	 *   	2) Class path (string), e.g. plugins.my_plugin.MyJobHandler, in which case the MyJobHandler.php will be used, no need to register.
	 *  
	 * Handler ID is associated with jobs in the database, so it is the connection between Jobs and Handlers.
	 * 
	 * @see ExampleJobHandler
	 * 
	 * @param array $params Array of arbitrary parameters
	 * @return string 
	 */
	public static function id($params = false);
	
	
} 