<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 4.1.2016 г.
 * Time: 18:47 ч.
 */
interface IProgressiveJobHandler extends IJobHandler {

	const ITEMS_PAGE_PAGE = 50;

	/**
	 * Progressive job handlers should rarely need to do anything
	 * in the overriden version of this method. They should instead
	 * extend the IProgressiveJobHandler and call parent::run()
	 * to allow the parent implementation to do the logic
	 *
	 * @param bool $params
	 *
	 * @return mixed
	 */
	public function run ( $params = FALSE );

	/**
	 * Implement a custom logic in this method that will
	 * return an integer with the number of total items to
	 * process (with disabled pagination)
	 *
	 * @return integer
	 */
	public function getTotalItemsCount ();

	/**
	 * Implementations should return an array of items
	 * to process, based on the passed offset and the
	 * self::$ITEMS_PER_ITERATION constant
	 *
	 * @param $offset
	 *
	 * @param $existingJobParams
	 *
	 * @return mixed
	 */
	public function getChunk ( $offset, &$existingJobParams = FALSE );

	public function processChunk ( array $chunk, &$existingJobParams = FALSE );

	/**
	 * A callback function, triggered after all "steps"
	 * of this progressive job are completed.
	 * Cleaning up code should go here.
	 *
	 * @return mixed
	 */
	public function afterProgressiveJobComplete ();
}