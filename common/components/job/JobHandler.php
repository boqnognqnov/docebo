<?php
/**
 * Base class for all Job Handler components
 *
 *  @package common.components.job.handlers
 *  
 */
class JobHandler extends CComponent {
	
	/**
	 * Job model which this handler is working with (must be set upon descendants creation)
	 * 
	 * @var CoreJob 
	 */
	public $job = null;
	
	/**
	 * Define Core job handlers IDs. These are essential part of LMS.
	 * 
	 * @var string
	 */
	const JH_EXAMPLE						= 'core.example';
	const JH_CUSTOMREPORT					= 'core.customreport';
	
	/**
	 * Map of job handlers (handler ID => Yii alias of a classpath).
	 * Can be extended by other code/plugins by calling JobHandler::registerHandler(<handler-id>, <handler-file-class-path>)
	 * 
	 * @var array
	 */
	public static $HANDLERS_MAP = array(
		self::JH_EXAMPLE 						=> 'common.components.job.handlers.ExampleJobHandler',
		self::JH_CUSTOMREPORT 					=> 'common.components.job.handlers.CustomReportGeneration',
	);
	
	
	/**
	 * Register a handler, adding it to the map of handlers
	 * 
	 * @param string $handler_id
	 * @param string $classPath
	 * @throws CException
	 */
	public static function registerHandler($handler_id, $classPath) {
		
		if (in_array($handler_id, self::$HANDLERS_MAP)) {
			throw new CException('Job handler ID already exists. Please choose another one.');
		}
		
		if (empty($handler_id) || empty($classPath)) {
			throw new CException('Handler ID and handler class path are required.');
		}
		
		$classFilePath = Yii::getPathOfAlias($classPath) . ".php";
		if (!is_file($classFilePath)) {
			throw new CException("Handler class file does not exist ($classPath)");
		}
		
		
		self::$HANDLERS_MAP[$handler_id] = $classPath;
		
		return true;
		
	}
	
	
	/**
	 * Remove specific job handler from available job handlers. This can be used to effectively DISABLE it
	 * 
	 * @param string $handler_id
	 */	
	public static function unregisterHandler($handler_id) {
		unset(self::$HANDLERS_MAP[$handler_id]);
	} 
	
	
	/**
	 * Create (if not yet created) and return Job Handler component, specific for the passed CoreJob model
	 * 
	 * @param CoreJob $job
	 * @return JobHandler|IJobHandler
	 * 
	 * @throws CException
	 */
	public static function getInstance(CoreJob $job) {
		
		/**
		 * Keep created handler instances in a static array 
		 * @var array Array of JobHandler objects
		 */
		static $handlers = array();
		
		// Use the cached handler, if exists
		if (isset($handlers[$job->handler_id])) {
			return $handlers[$job->handler_id];
		}
				
		// 1. Check if the handler_id is in the map of handlers (highest priority)  
		if (array_key_exists($job->handler_id, self::$HANDLERS_MAP)) {
			$config = array(
				'class' => 	JobHandler::$HANDLERS_MAP[$job->handler_id],
				'job'	=> $job,
			);
			$handlers[$job->handler_id] = Yii::createComponent($config);
			$handlers[$job->handler_id]->init();
			return  $handlers[$job->handler_id];
		}
		// 2. Check if handler_id is a valid classFilePath itself and use it
		else if (is_file(Yii::getPathOfAlias($job->handler_id) . ".php")) {
			$config = array(
				'class' => 	$job->handler_id,
				'job'	=> $job,
			);
			$handlers[$job->handler_id] = Yii::createComponent($config);
			$handlers[$job->handler_id]->init();
			return  $handlers[$job->handler_id];
		}
		else {
			throw new CException("Job Handler not found: ($job->handler_id)");
		}
			 
	}

	/**
	 * Returns whether this handler allows for immediate jobs to be reused
	 * @return bool
	 */
	public function canReuseSameImmediateJob() {
		return false;
	}
	
}