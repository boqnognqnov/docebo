<?php

class WebinarJobs extends JobHandler implements IJobHandler{

	const JOB_NAME = 'WebinarJob';

	public function init($params = false) {

	}

	public function run($params = false) {
		$job = $this->job;
		if(!$job){
			return;
		}

		//Get passed params the the Job
		$params = json_decode($job->params, true);
		$task = $params['task'];
		$sessionId = $params['session_id'];
		$date = $params['date'];
		$filename = $params['filename'];

		switch($task) {
			case "conversion":
				$log = new WebinarConversion();

				$result = WebinarRecordings::convertWebinar($sessionId, $date, $filename);

				$log->session_id = $sessionId;
				$log->date = $date;
				$log->conversion_date = date('Y-m-d H:i:s');
				$log->status = $result['status'];
				$log->filename = $result['filename'];
				$log->job_id = $job->hash_id;
				$log->error = $result['error'];
				$log->save();
				break;
			case "download":
				WebinarRecordings::downloadFileWithCurl($sessionId, $date, $filename);
				break;
		}
	}

	public static function id($params = false) {
		return 'common.components.job.handlers.WebinarJobs';
	}
}
