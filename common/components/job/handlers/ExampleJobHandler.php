<?php
/**
 * An example of Job Handler component.
 *  
 * Extends the base JobHandler class and implements the IJobHandler interface.
 *  
 * The main method is run(), executing the real job handler work. 
 * If the method ends with NO exception thrown, job is considered done with success.
 *  
 *  @package common.components.job.handlers
 *  
 */
class ExampleJobHandler extends JobHandler implements IJobHandler {
	
	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = JobHandler::JH_EXAMPLE;
	

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level
		$job = $this->job;
		Yii::log('Example Job Handler RUN started. Job hash: ' . $this->job->hash_id, CLogger::LEVEL_INFO);
	}

	
	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}
	
}

