<?php

class AutomationAppBranchImport extends JobHandler implements IJobHandler {

    /**
     * THIS handler identifier
     * @var string
     */
    const HANDLER_ID = 'common.components.job.handlers.AutomationAppBranchImport';
    const MAX_BRANCH_PER_RUN = 300;

    CONST TMP_FILE = "tmp_downloaded_branch.csv";

    CONST FTP = "ftp";
    CONST SFTP = "sftp";

    private $tableId = 'entityId';

    /**
     * @see IJobHandler::init()
     */
    public function init($params = false) {
        // do some initialization stuff here, specific for this job handler
    }

    /**
     * @see IJobHandler::run()
     */
    public function run($params = false) {
        // Already loaded at upper level
        $job = $this->job;
        Yii::log('AutomationApp BranchImport Immediate Job Handler RUN started. Job hash: ' . $this->job->hash_id, CLogger::LEVEL_INFO);

        $params = json_decode($job->params, true);
        $scheduler = Yii::app()->scheduler; /* @var $scheduler SchedulerComponent */
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
        $localLogFileName = $this->getErrorLogFileName();
        $localLogFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $localLogFileName;

        // Check if this is the first execution of the import
        if(isset($params['isFirst']) && $params['isFirst']){
            // Retrieve session params (including the import form)
            // In order to get the level column position
            $importForm = new OrgChartImportForm();
            foreach ($params['form']['OrgChartImportForm'] as $attribute => $value) {
                if (property_exists($importForm, $attribute)){
                    $importForm->$attribute = $value;
                }else if ($attribute == 'importMap'){
                    $importForm->setImportMap(explode(",", $value));
                }
            }
            $this->loadDataFromFTP($params, $localLogFilePath, $importForm);
        }
        try {
            // Get portion (chunk) of items...  and import them
            $chunkData = $this->getChunkDataFromDB(self::MAX_BRANCH_PER_RUN);
            // Ok we got the chunkData for this iteration
            // If the chunk is empty this means the we don't have next iteration
            // so we can send the log to the end user
            $finishedTask = empty($chunkData);
            if ($finishedTask) {
                // Deactivate job and delete it from Sidekiq
                $job->active = 0;
                $job->delete();
                // Send the log to the user
                $storage->downloadAs($localLogFileName, $localLogFilePath);
                $this->sendLog($params['form']['OrgChartImportForm']['sendLogToMail'], $localLogFilePath);
                // After the log is rend we can remove the error log file
                $storage->remove($localLogFilePath);
                $this->clearTmpTable();
            }else{
                // Retrieve session params (including the import form)
                $importForm = new OrgChartImportForm();
                foreach ($params['form']['OrgChartImportForm'] as $attribute => $value) {
                    if (property_exists($importForm, $attribute)){
                        $importForm->$attribute = $value;
                    }else if ($attribute == 'importMap'){
                        $importForm->setImportMap(explode(",", $value));
                    }
                }

                $importForm->failedLines = array();

                // DO THE REAL IMPORT
                $this->runBranchTask($importForm, $chunkData);

                // Save errors in S3 log file
                if (count($importForm->failedLines) > 0) {
                    // Download the error file
                    $storage->downloadAs($localLogFileName, $localLogFilePath);

                    // Open file in append mode
                    $fh = fopen($localLogFilePath, 'a');
                    foreach ($importForm->failedLines as $failedLine)
                        fputs($fh, $failedLine . "\n");
                    fclose($fh);

                    // Save file back to S3
                    $storage->store($localLogFilePath);
                }

                // Save session data back into the params column
                unset($params['isFirst']);
                $job->params = json_encode($params);
                $job->save();

                // Recreate the immediate job with the same hash
                $scheduler->createImmediateJob("AutomationAppUserImport", self::id(), false, false, $job);
            }

        } catch(Exception $e) {
            // In case of errors, just make sure that the job is still renewed
            $scheduler->createImmediateJob("AutomationAppUserImport", self::id(), false, false, $job);

            throw $e;
        }

    }

    private function consistencyChecks($schema){

        // Remove the ignored fields from the list if there are any
        foreach($schema as $key => $field){
            if ( $field == "ignoredfield" ){
                unset($schema[$key]);
            }
        }

        // Check if there is at least one fields provided
        if(empty($schema) || empty($schema[0])){
            throw new Exception('No fields provided into the Import Schema');
        }
        // Code fields is required
        if(!in_array(OrgChartImportForm::CODE, $schema)){
            // Code fields is required
            throw new Exception('Branch code fields is required into the Import Schema');
        }
        // Let's check if the user entered some of the column twice
        $countValues = array_count_values($schema);
        if(isset($countValues[OrgChartImportForm::CODE]) && $countValues[OrgChartImportForm::CODE] > 1){
            throw new Exception('Branch code fields is added more then once into the Import Schema');
        }
        if(isset($countValues[OrgChartImportForm::PARENT]) && $countValues[OrgChartImportForm::PARENT] > 1){
            throw new Exception('Parent code fields is added more then once into the Import Schema');
        }
        // Let's check if there are any translations provided
        if(isset($countValues[OrgChartImportForm::LEVEL])){
            unset($countValues[OrgChartImportForm::LEVEL]);
        }
        unset($countValues[OrgChartImportForm::CODE]);
        unset($countValues[OrgChartImportForm::PARENT]);
        if(empty($countValues)){
            throw new Exception('No branch translation provided into the Import Schema');
        }
    }

    /**
     * @see IJobHandler::id()
     */
    public static function id($params = false) {
        return self::HANDLER_ID;
    }

    /**
     * Returns whether this handler allows for immediate jobs to be reused
     * @return bool
     */
    public function canReuseSameImmediateJob() {
        return true;
    }

    private function getTableName(){
        return substr($this->job->hash_id, 0, 5) . "_automation_app_tmp_branches_import";
    }

    private function getErrorLogFileName(){
        return substr($this->job->hash_id, 0, 5) . "_automation_app_branches_import_failed_lines.csv";
    }

    public function loadDataFromFTP($params, $localLogFilePath, $form)
    {
        try{
            $ftpData = $params['ftp'];
            $formData = $params['form']['OrgChartImportForm'];
            // Define local and remote files
            $remoteDir = isset($ftpData['folder']) ? $ftpData['folder'] : null;
            $remoteFile = isset($ftpData['file']) ? $ftpData['file'] : false;
            $localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . self::TMP_FILE;

            // Right before doing anything let's check if the import schema is correctly configured
            $this->consistencyChecks($form->getImportMap());

            // Init the ftp manager and add params
            $ftp = Yii::app()->ftp;
            $ftp->host      = isset($ftpData['host'])       ? $ftpData['host']                      : null;
            $ftp->port      = isset($ftpData['port'])       ? $ftpData['port']                      : null;
            $ftp->username  = isset($ftpData['username'])   ? $ftpData['username']                  : null;
            $ftp->password  = isset($ftpData['password'])   ? $ftpData['password']                  : null;
            $ftp->ssh       = isset($ftpData['protocol'])   ? ($ftpData['protocol'] == self::SFTP)  : null;

            if($ftp->connect()){
                if($remoteDir)
                    $ftp->chdir($remoteDir);

                $ftp->getFile($remoteFile, $localFile);
            }else{
                throw new Exception("Can't connect to the server:" . $ftp->host);
            }
            // If the file exists we are about to load it into DATABASE
            if(file_exists($localFile)){
                if( filesize($localFile) === false )
                    throw new Exception("Downloaded file is empty");

                // Prepare some variables in order to properly import the file into DB
                $sep                = isset($formData['separator']) ? $formData['separator'] : 'auto';
                $manualSep          = isset($formData['manualSeparator']) ? $formData['manualSeparator'] : '';
                $delimeter          = OrgChartImportForm::translateSeparatorName($sep, $manualSep);
                $firstRowAsHeader   = isset($formData['firstRowAsHeader']) ? $formData['firstRowAsHeader'] : false;
                $encoding           = isset($formData['charset']) ? $formData['charset'] : "UTF-8";
                $orderBy = $this->resolveRowsOrder($form);
                // Load the data into the DB
                $this->loadCSVData($localFile, $delimeter, $firstRowAsHeader, $encoding,$orderBy);
            }else{
                throw new Exception("Can't download the file from the server");
            }
        } catch (Exception $e){
            // if we have any errors we will send them to the end user
            // Open file in append mode
            $fh = fopen($localLogFilePath, 'a');
            fputs($fh, $e->getMessage() . "\n");
            fclose($fh);
            $this->sendLog($formData['sendLogToMail'], $localLogFilePath);
            // With throwing another exception we want to prevent the next execution (the user import);
            throw new Exception($e->getMessage());
        } catch (CDbException $e){
            // if we have any errors we will send them to the end user
            // Open file in append mode
            $fh = fopen($localLogFilePath, 'a');
            fputs($fh, $e->getMessage() . "\n");
            fclose($fh);
            $this->sendLog($formData['sendLogToMail'], $localLogFilePath);
            // With throwing another exception we want to prevent the next execution (the user import);
            throw new Exception($e->getMessage());
        }
    }

    private function resolveRowsOrder($form){
        foreach($form->getImportMap()as $key => $col){
            if($col == 'level'){
                return 'Column' .($key+1);
            }
        }
        return false;
    }

    public function loadCSVData($fileName, $separator, $firstRowAsHeader, $encoding, $orderBy){
        $csvFile = new QsCsvFile(array(
            'path' => $fileName,
            'charset' => $encoding,
            'delimiter' => $separator,
            'firstRowAsHeader' => $firstRowAsHeader,
        ));
        $columns = $this->resolveColumns($csvFile);
        $this->mangeTempTable($orderBy, $columns);

        // Let's import the data from the CSV into the table that we just created
        // We are defining the the file is NOT empty
        $fileIsEmpty = false;
        $offset = 0;
        // While the file is not empty let's import its chunk into DB
        $table = $orderBy === false ? $this->getTableName() : ($this->getTableName(). '_tmp');
        while(!$fileIsEmpty){
            $data = $csvFile->getChunk(self::MAX_BRANCH_PER_RUN, $offset, $firstRowAsHeader);
            $offset += self::MAX_BRANCH_PER_RUN;
            // If the $data is empty this means that there is no more data in the file and we should stop
            if(empty($data)){
                $fileIsEmpty = true;
            }else{
                // If there is data from the file let's import it into DB
                $this->importData($table, $data);
            }
        }
        // IF we should order then let's create a new table
        // from the old one but order by certain column
        if($orderBy !== false){
            $sql = " CREATE TABLE `" . $this->getTableName() . "` (`".$this->tableId."` INT(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`".$this->tableId."`))";
            $sql .= " SELECT * FROM `" . ($this->getTableName() . '_tmp') . "` ORDER BY ".$orderBy." ASC; ";
            Yii::app()->db->createCommand($sql)->execute();
            // THEN remove the temporary one
            Yii::app()->db->createCommand(" DROP TABLE `" .($this->getTableName() . "_tmp`"))->execute();
        }
        // At the end remove the CSV file in order to clear the trash
        FileHelper::removeFile($fileName);
    }

    private function mangeTempTable($orderBy, $columns){
        if($orderBy === false){
            // If the column DOES NOT EXIST IN DATABASE
            if(!in_array($this->getTableName(), Yii::app()->db->schema->getTableNames())){
                $tableColumns = array($this->tableId => 'pk');
                foreach($columns as $column)
                    $tableColumns[$column] = "varchar(255)";
                // Create the table
                Yii::app()->db->createCommand()->createTable($this->getTableName(), $tableColumns);
            }else{
                // Well the table exists let's clear it
                Yii::app()->db->createCommand("TRUNCATE TABLE `" . $this->getTableName(). "`")->execute();
            }
        } else {
            // If the column DOES NOT EXIST IN DATABASE
            if (!in_array($this->getTableName() . '_tmp', Yii::app()->db->schema->getTableNames())) {
                $tableColumns = array();
                foreach ($columns as $column)
                    $tableColumns[$column] = "varchar(255)";
                // Create the table
                Yii::app()->db->createCommand()->createTable($this->getTableName() . '_tmp', $tableColumns);
            } else {
                // Well the table exists let's clear it
                Yii::app()->db->createCommand("TRUNCATE TABLE `" . $this->getTableName() . "_tmp`")->execute();
            }
        }
    }

    private function importData($table, $data){
        // Let's format the data a bit
        $formatedData = array();
        foreach($data as $user){
            //sometimes CSVs can contain empty lines which will result in confusing errors. Just avoid them.
            if (!is_array($user) || empty($user) || (count($user) <= 0)) {
                continue;
            }

            $tmpFormat = array();
            foreach($user as $key => $column)
                $tmpFormat[('Column' . ($key+1))] = $column;
            // If the $tmpFormat is empty this means that the row was empty
            if($tmpFormat !== array())
                $formatedData[] = $tmpFormat;
        }
        // When the CHUNK of data is formatted and ready, let's insert it with Query
        Yii::app()->getDb()->getCommandBuilder()
            ->createMultipleInsertCommand($table, $formatedData)->execute();
    }

    private function resolveColumns( $file ){
        $row = $file->getChunk(1,0);
        if(isset($row[0]) && $row[0]){

            $columns = array();
            foreach($row[0] as $key => $column)
                $columns[] = 'Column' . ($key+1);

            return $columns;
        }else{
            throw new Exception("Can't resolve file columns");
        }
    }

    private function resolveDelimeter($file, $delimeter){
        if ($delimeter === 'auto') {
            $data = $file->getData();
            if (substr_count($data[0], ',') > substr_count($data[0], ';')) {
                return ',';
            } else {
                return ';';
            }
        }
        return $delimeter;
    }

    /**
     * Runs the branch synch import form
     *
     * @param $importForm OrgChartImportForm
     * @param $chunkData array
     */
    private function runBranchTask(&$importForm, &$chunkData){

        $importForm->setData($chunkData);
        $importForm->saveOrgCharts();  // Save branches, do NOT validate, skip header if this is the very first 'page'
    }

    private function getChunkDataFromDB($limit){
        // Refresh the table shema
        Yii::app()->db->schema->refresh();
        // Check if the table exists on order to get the columns
        if(!in_array($this->getTableName(), Yii::app()->db->schema->getTableNames()))
            throw new Exception("Table: " . $this->getTableName() . " does not exists");

        // Let's try to get all the columns without the first one (the id)
        $columns=Yii::app()->db->schema->getTable($this->getTableName())->columns;
        if(!$columns)
            throw new Exception("Can't read table: " . $this->getTableName());

        array_shift($columns);
        $columNames = array();
        foreach($columns as $name => $value)
            $columNames[] = $name;

        // Let's get a few rows from DB
        $sql = "SELECT `" . implode("`, `", $columNames) . "` FROM `".$this->getTableName()."` ORDER BY ".$this->tableId." ASC LIMIT $limit";
        $rows = Yii::app()->db->createCommand($sql)->queryAll(false);

        // Clean up the retrieved data
        $sql = "DELETE FROM `".$this->getTableName()."` ORDER BY ".$this->tableId." ASC LIMIT $limit";
        Yii::app()->db->createCommand($sql)->execute();

        return $rows;
    }

    private function clearTmpTable(){
        // Clean up the tmp table
        $sql = "DROP TABLE IF EXISTS `".$this->getTableName(). "`";
        Yii::app()->db->createCommand($sql)->execute();
    }

    private function sendLog($email, $file){
        // If there is no mail or no fail, well I guess we wouldn't send anything
        if(!$email) return;

        // Create new MailManager instance
        $mm = new MailManager();
        $from_email = Settings::get('mail_sender');
        if(!$from_email){
            Yii::log('No email configured to send the error log');
            return;
        }
        $to_email = $email;
        $subject = 'Docebo log from AutomationApp Branch Import';
        $attachments = false;
        if(file_exists($file) && filesize($file) != false){
            $message = 'Error log attached';
            $attachments = array($file);
        }else{
            $message = Yii::t( 'automation', 'All branches were imported successfully' );
        }

        $result = $mm->mail(array($from_email => $from_email), array($to_email => $to_email), $subject, $message, $attachments);

    }

}

// ======================================================================
//            Implementation of array_column, if PHP <= 5.5
// ======================================================================
if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}