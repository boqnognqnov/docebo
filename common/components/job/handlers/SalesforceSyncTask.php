<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SalesforceSyncTask
 *
 * @author Georgi
 */
class SalesforceSyncTask extends JobHandler implements IJobHandler{
    
    const JOB_NAME = 'SalesforceSyncManual';
    
    public function init($params = false) {
        
    }

    public function run($params = false) {
        $job = $this->job;
        if(!$job){
            return;
        }                
        
        //Get passed params the the Job
        $params = json_decode($job->params, true);
        $type = $params['type'];
        
        $salesforceLmsAccount = SalesforceLmsAccount::getFirstAccount();
                
        $salesforceAgent = new SalesforceSyncAgent($salesforceLmsAccount->id_account);
        
        $salesforceLog = new SalesforceLog();
        $salesforceLog->id_account = $salesforceLmsAccount->id_account;
        $salesforceLog->datetime = date('Y-m-d H:i:s');
        $salesforceLog->type_log = $type;                                
        
        $data = array();
        $errors = array();
        $totalItems = 0;
        $saveLog = true;
        
        switch($type){
            case SalesforceLog::TYPE_LOG_USERS:
                $emptyTempTable = isset($params['subsequentRun'])?false:true;
                $log = $salesforceAgent->syncUsers($emptyTempTable);
                if($log['completed'] == false){
                    Yii::log('Not yet completed... keep going...', CLogger::LEVEL_INFO);
                    $saveLog = false;
                    $params['subsequentRun'] = true;
                    if(!isset($params['firstJobId'])) $params['firstJobId'] = $job->hash_id;
                    $scheduler = Yii::app()->scheduler;
                    $scheduler->createImmediateJob(self::JOB_NAME, self::id(), $params);
                } else {
                    Yii::log('Done-diddeli-done', CLogger::LEVEL_INFO);
                }
                break;
//            case salesforceLog::TYPE_LOG_ACCOUNTS:
//            	if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $salesforceLmsAccount->getUserReplications())){
//                	$log = $salesforceAgent->syncAccountsStructure();
//            	}
//                break;
            case SalesforceLog::TYPE_LOG_COURSES:
            	$log = $salesforceAgent->syncCourses();
                break;
            case SalesforceLog::TYPE_LOG_UPGRADE:
                $salesforceAgent->createCourseCompletionField();
                $log = array(
                    'itemsCount'    => 0,
                    'errors'        => array(),
                    'created'       => 0,
                    'updated'       => 0,
                    'failed'        => 0
                );
                break;
            case SalesforceLog::TYPE_LOG_UPDATE_METADATA:
                $return = $salesforceAgent->updateMetadata($params['profiles'], $params['fields'], $params['index']);
                $log = array(
                    'itemsCount'    => count($return->result),
                    'errors'        => array(),
                );
                break;
        }

        if($saveLog) {
            $errors = CMap::mergeArray($errors, $log['errors']);
            $totalItems += $log['itemsCount'];

            $data['itemsCount'] = $totalItems;
            $data['errors'] = $errors;
            $data['created'] = isset($log['created']) ? $log['created'] : 0;
            $data['updated'] = isset($log['updated']) ? $log['updated'] : 0;
            $data['failed'] = isset($log['failed']) ? $log['failed'] : 0;

            $salesforceLog->data = json_encode($data);
            $salesforceLog->job_id = isset($params['firstJobId'])?$params['firstJobId']:$job->hash_id;
            $salesforceLog->save();
        }
    }

    public static function id($params = false) {
        return 'common.components.job.handlers.SalesforceSyncTask';
    }            
}
