<?php 

/**
 * Job Handler for cronJob
 * 
 * @author Georgi
 *
 */
class SalesforceAutoSync extends JobHandler implements IJobHandler{
	
	const JOB_NAME = 'SalesforceAutoSync';
	
	public function init($params = false){
		
	}
	
	public function run($params = false){

		$job = $this->job;
		
		if(!$job){
			return;
		}
		
		//Checking if Manual job is running
		$manualJob = CoreJob::model()->find('name = :jobManual', array(
				':jobManual' => SalesforceSyncTask::JOB_NAME,
		));
		
		if($manualJob){
			return;
		}		
		
		$params = json_decode($job->params, true);
		
		$sfLmsAccount = null;
		
		$sfAccountId = $params['sfAccountId'];			
		
		if(empty($sfAccountId)){
			$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		} else{
			$sfLmsAccount = SalesforceLmsAccount::model()->find('id_account = :id', array(
					':id' => $sfAccountId
			));
		}			
		
		$sfSyncAgent = new SalesforceSyncAgent($sfLmsAccount->id_account);
		
		
		$syncTypes = SalesforceLog::getLogTypes();
		
		
		/**
		 * Loop throw all log types and synchronize what is needed.
		 * 
		 * SalesforceSyncAgent is carring about what are the settings for synchronization!!
		 */
		foreach ($syncTypes as $type){
			
			$salesforceLog = new SalesforceLog();
			$salesforceLog->id_account = $sfLmsAccount->id_account;
			$salesforceLog->datetime = date('Y-m-d H:i:s');
			$salesforceLog->type_log = $type;
			
			$data = array();
			$log = array();
			$saveLog = true;
			
			switch ($type){
				case SalesforceLog::TYPE_LOG_USERS:
					$emptyTempTable = isset($params['subsequentRun'])?false:true;
					$log = $sfSyncAgent->syncUsers($emptyTempTable);
					$saveLog = false;
					$newParams = array(
						'subsequentRun' => true,
						'firstJobId' => $job->hash_id,
						'type' => $type,
						'id_account' => $sfLmsAccount->id_account
					);
					$scheduler = Yii::app()->scheduler;
					$scheduler->createImmediateJob(SalesforceSyncTask::JOB_NAME, SalesforceSyncTask::id(), $newParams);
					break;
				case SalesforceLog::TYPE_LOG_COURSES:
					$log = $sfSyncAgent->syncCourses();
					break;
			}

			if($saveLog) {
				$errors = $log['errors'];
				$totalItems = $log['itemsCount'];

				$data['itemsCount'] = $totalItems;
				$data['errors'] = $errors;

				$data['created'] = isset($log['created']) ? $log['created'] : 0;
				$data['updated'] = isset($log['updated']) ? $log['updated'] : 0;
				$data['failed'] = isset($log['failed']) ? $log['failed'] : 0;


				$salesforceLog->data = json_encode($data);
				$salesforceLog->job_id = isset($params['firstJobId'])?$params['firstJobId']:$job->hash_id;
				$salesforceLog->save();
			}
		}
	}
	
	public static function id($params = false){
		return 'common.components.job.handlers.SalesforceAutoSync';
	}
	
}
?>