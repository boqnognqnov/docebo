<?php
/**
 *  @package common.components.job.handlers
 *
 */
class WebappArchiveCourses extends JobHandler implements IJobHandler {

	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID          = 'common.components.job.handlers.WebappArchiveCourses';
	const MAX_COURSES_PER_RUN = 1;
	const JOB_NAME            = 'WebappArchiveCourses';
	const ZIP_EXT             = '.zip';
	const HTML_EXT            = '.html';

	protected $webappStorage;
	protected $tempDir;

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
		$this->tempDir          = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR;
		$this->webappStorage    = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_WEBAPP);
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level

		$job = $this->job;
		$log = array();
		Yii::log('WebappArchiveCourses Immediate Job Handler RUN started. Job hash: ' . $this->job->hash_id, CLogger::LEVEL_INFO);

		$params = CJSON::decode($job->params);

		$scheduler = Yii::app()->scheduler; /* @var $scheduler SchedulerComponent */
		try {
			$finishTask = self::moveTrainingMaterials($params['courseId'], $params['idUser']);
			if ( $finishTask){
				$job->active = 0;
				$job->delete();
			}
		} catch(Exception $e){
			throw $e;
		}
	}

	private function moveTrainingMaterials($courseId, $idUser){
		$webappLearningObject = ($courseId ? new WebappLearningObject() : '');
		$learningMaterials = ($webappLearningObject ? $webappLearningObject->getLearningMaterials($courseId, $idUser) : '');
		if ( is_array($learningMaterials)){
			foreach ($learningMaterials['lo_metadata'] as $key => $material){
				$material['title'] = preg_replace('/\s+/', '_', $material['title']);
				switch ( $material['type']){
					case 'sco':
						$pathname = LearningScormPackage::getScormPath($material['id_organization']);
						$this->uploadMaterialToWebappS3($pathname, $material, 'scorm', self::ZIP_EXT, false);
						break;
					case LearningOrganization::OBJECT_TYPE_FILE:
						$pathname = LearningMaterialsLesson::model()->findByPk($material['id_resource'])->path;
						$this->uploadMaterialToWebappS3($pathname, $material, 'item');
						break;
					case LearningOrganization::OBJECT_TYPE_VIDEO:
						$pathname = LearningVideo::model()->findByPk($material['id_resource'])->getVideoHash();
						if ( $material['video_type'] === LearningVideo::VIDEO_TYPE_UPLOAD && $pathname != false)
							$this->uploadMaterialToWebappS3($pathname, $material, LearningOrganization::OBJECT_TYPE_VIDEO);
						break;
					case LearningOrganization::OBJECT_TYPE_HTMLPAGE:
						$this->uploadHTMLtoS3($material['id_resource'], $material['title']);
						break;
					case LearningOrganization::OBJECT_TYPE_AUTHORING:
						$this->uploadAuthoringToS3($material['id_resource'], $material['title']);
						break;
				}
			}
			return true;
		}
	}


	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}

	/**
	 * Validate Amazon S3 Configuration for the user
	 */
	static public function validateConfiguration(){
		if ( !isset( Yii::app()->params['amazon']) || empty (Yii::app()->params['amazon'])){
			Yii::log( 'The "amazon" param is not properly configured in the main.php configuration file. Amazon S3 app will not work without it.', CLogger::LEVEL_ERROR );
		}
	}

	/**
	 * @param $pathname         - Full path on S3 of the Training Material
	 * @param $material         - Array of information about the training material
	 * @param $collection       - The name of the S3 collection based on Learning Object type (file, video, html)
	 * @param string $extension - The desired extension of the archived package (.rar, .zip. .tar .gz )
	 * @param bool $toBeZipped  - All LO's must be zipped except SCORM packages
	 */
	private function uploadMaterialToWebappS3($pathname, $material, $collection, $extension='', $toBeZipped=true){

		$title  = $material['title'];
		$id     = $material['id_resource'];
		$standardStorage = CFileStorage::getS3DomainStorage($collection);
		if ( !$standardStorage->fileExists( $pathname) ) { return; }

		$filePath = ($extension!=='' ? $this->tempDir.$title."_".$id.$extension : $this->tempDir.$pathname);
		$standardStorage->downloadAs($pathname.$extension, $filePath);
		$extension = (pathinfo($filePath)['extension'] === 'zip' ? self::ZIP_EXT : ''); // set to zip if the file is

		if ( !$toBeZipped ){
			$this->webappStorage->storeAs( $filePath, $title."_".$id.$extension );
		}else{
			if ( $extension !== self::ZIP_EXT)
				$zippedFile = $this->zipMaterial($pathname, $title);
			else
				$zippedFile = $filePath;
			/* Upload to Webapp S3 */
			$this->webappStorage->storeAs( $zippedFile, $title."_".$id.".zip" );
		}
		/* Clean Local files*/
		try {
			if ( self::isFileValid($filePath)){
				chmod($filePath, 0777);
				unlink($filePath);
			}
		} catch (Exception $e){
			Log::_($e->getMessage());
			Log::_("Could not delete local file: ". $filePath);
		}

	}

	/**
	 *
	 * @param $pathname - full path to the training material (local cache storage)
	 * @param $title    - Title of the Training material
	 * Example .zip title - "TitleOfTheLO_134"     " *$title*_*$id* "
	 *
	 * @return string|void
	 */
	private function zipMaterial($pathname, $title){
		$zip        = new ZipArchive;
		$zippedFile = $title.self::ZIP_EXT;
		$res        = $zip->open($this->tempDir.$zippedFile, ZipArchive::CREATE);
		if ( !$res)
			return false;
		$ext        = pathinfo($pathname, PATHINFO_EXTENSION);
		$directory  = $this->tempDir.$pathname;

		$zip->addFile($directory, $title.'.'.$ext);
		$zip->close();
		return $this->tempDir.$zippedFile;
	}

	/**
	 * Check is the given path really a *file*, is it readable, does it exist and is it writable
	 *
	 * @param $file - path to the file
	 * @return bool
	 */
	static private function isFileValid($file){
		if ( strlen($file) < 1 || is_file($file) || file_exists($file) ){
			return true;
		}

		return false;
	}

	/**
	 * Get the HTML content from the DB and write it inside a file,
	 * then zip it and upload it to S3
	 *
	 * @param $idResource
	 * @param $title
	 * @return string
	 */
	private function uploadHTMLtoS3($idResource, $title){
		$HTMLContent = LearningHtmlpage::model()->findByPk($idResource)->textof;
		$filename    = $title.'_'.$idResource;
		$htmlname    = $filename.self::HTML_EXT;
		$filepath    = $this->tempDir.$htmlname;
		$fp          = fopen($filepath, 'wb');
		fwrite($fp, $HTMLContent);
		fclose($fp);

		$zippath    = $this->zipMaterial( $htmlname, $filename);
		while ( !file_exists( $zippath)){
			sleep(1);
		}
		if ( file_exists( $zippath)){
			$this->webappStorage->storeAs( $zippath, $filename.self::ZIP_EXT);
		}
		/* Clean local file */
		if ( self::isFileValid($filepath)){
			chmod($filepath, 0777);
			unlink($filepath);
		}
		if ( self::isFileValid($zippath)){
			chmod($zippath, 0777);
			unlink($zippath);
		}

		return $filename.self::HTML_EXT;
	}

	/**
	 * Upload Slide Converted materials to webapp S3 collection
	 *
	 * @param $idResource - id of the authoring material (learning object id)
	 * @param $title - Title of the Training Material
	 */
	private function uploadAuthoringToS3($idResource, $title){
		$Storage = CFileStorage::getS3DomainStorage(LearningOrganization::OBJECT_TYPE_AUTHORING);
		$Storage->downloadCollectionAs($this->tempDir, $idResource, true);

		$folder = $this->tempDir.$idResource;
		$zip        = new ZipArchive;
		$zipFile = $title.'_'.$idResource.self::ZIP_EXT;
		$zipPath = $this->tempDir.$zipFile;
		$zip->open($zipPath, ZipArchive::CREATE);   // Create the .zip file
		/* We have to exclude the original filename of the Authoring tool */
		$excludeOriginalFilename = $idResource."_1";
		( $zip ? $this->_addFolderToZip($folder, $zip, null, $excludeOriginalFilename) : '' );  // If is created successfully add the files inside
		if ( $zip ) $zip->close();      // CLOSE THE ZIP FILE

		if ( file_exists( $zipPath)){
			$this->webappStorage->storeAs( $zipPath, $zipFile);
		}
	}
	/** Adds an entire directory structure to the zip including subfolders
	 *
	 * @param $excludeOriginalFile - string with the filename of the excluded object
	 * @param $zipFile ZipArchive
	 * @param $folder
	 */
	private function _addFolderToZip($folder, $zipFile, $subfolder = null, $excludeOriginalFile='') {
		if ($zipFile == null)
			return false; // no resource given, exit

		// we check if $folder has a slash at its end, if not, we append one
		$folderParts = str_split($folder);
		$folder .= end($folderParts) == "/" ? "" : "/";
		$subFolderParts = str_split($subfolder);
		$subfolder .= end($subFolderParts) == "/" ? "" : "/";

		// we start by going through all files in $folder
		$handle = opendir($folder);

		while ($f = readdir($handle)) {
			if ($f != "." && $f != "..") {
				if (is_file($folder . $f)) {
					// if exist filename to exclude from the folder
					if ( strlen($excludeOriginalFile) > 0){
						$pathParts = pathinfo($f);
						// if we got a match skip adding it to the zip
						if ($pathParts && $pathParts['filename'] === $excludeOriginalFile){
							continue;
						}
					}
					// if we find a file, store it
					// if we have a subfolder, store it there
					if ($subfolder != null)
						$zipFile->addFile($folder . $f, $subfolder . $f);
					else
						$zipFile->addFile($folder . $f);
				} elseif (is_dir($folder . $f)) {
					// if we find a folder, create a folder in the zip
					$zipFile->addEmptyDir($f);
					// and call the function again
					$this->_addFolderToZip($folder . $f, $zipFile, $f);
				}
			}
		}
	}

}

