<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupAutoAssignTask
 *
 * @author Georgi
 */
class GroupAutoAssignTask extends JobHandler implements IJobHandler
{

	const JOB_NAME = 'GroupAutoAssign';

	/**
	 *
	 * @see IJobHandler::init()
	 */
	public function init($params = false)
	{

	}



	public static function id($params = false)
	{
		return 'common.components.job.handlers.GroupAutoAssignTask';
	}



	public static function setUpdateJob($users) {

		//check if some rules actually exists
		$hasSelfPopulatingGroups = Yii::app()->db->createCommand("SELECT COUNT(*) FROM core_group WHERE assign_rules = 1")->queryScalar();
		if($hasSelfPopulatingGroups <= 0) { return; }

		$idstList = array();

		//check input parameters
		if (is_numeric($users)) {
			$idstList[] = (int)$users;
		}
		if (is_string($users)) {
			$idstList = explode(',', trim($users, ','));
		}
		if (is_object($users) && get_class($users) == 'CoreUser') {
			$idstList[] = $users->idst;
		}
		if (is_array($users)) {
			if (empty($users)) { return; } //no real input to process
			foreach ($users as $item) {
				if (is_numeric($item)) {
					$idstList[] = (int)$item;
				}
				if (is_object($item) && get_class($item) == 'CoreUser') {
					$idstList[] = $item->idst;
				}
			}
		}

		if (empty($idstList)) {return; }

		$scheduler = Yii::app()->scheduler;
		$params = array(
			'user_idst' => implode(',', $idstList),
			'offset' => 0
		);
		$scheduler->createImmediateJob(self::JOB_NAME, self::id(), $params);
	}

    /**
     * Automatically assigning users to groups
     *
     * @param CoreUser $user
     */
    public static function singleUserRun(CoreUser $user)
    {
        $id_user = $user->idst;
        $userFieldDetails = $user->getAdditionalFields();

		//prepare preloading variables
		$_preloaded_userEntries = array();
		$_preloaded_groups = array();
		$_preloaded_members = array();
		$_preloaded_assignRules = array();
		$_preloaded_fields = array();
        $additionalFieldIds = array();

		// Pre-load all groups with assign rules
		$reader = Yii::app()->db->createCommand()
			->select('idst, assign_rules_logic_operator')
			->from(CoreGroup::model()->tableName())
			->where('assign_rules = 1')
			->query();
		while ($group = $reader->read()) {
			$_preloaded_groups[] = $group;
		}

        if (empty($_preloaded_groups)) { return; }

		$groupsList = array(); //this will be used in later queries
		foreach ($_preloaded_groups as $group) {
			$groupsList[] = $group['idst']; //extract groups single idsts as a flat list
		}

        // Pre-load additional fields for the passed user
        $_preloaded_userEntries = CoreUser::getAdditionalFieldEntryValuesArray([$id_user], true, true);

		// Pre-load group membership
		$reader = Yii::app()->db->createCommand()
			->select('idst, idstMember')
			->from(CoreGroupMembers::model()->tableName())
			->where("idstMember = :id_user", array("id_user" => $id_user))
			->andWhere(array("IN", "idst", $groupsList)) //NOTE: we don't chunk groups array, since it is very unlikely that it will be big selection
			->query();
		while ($member = $reader->read()) {
			$_preloaded_members[$member['idst']][] = $member['idstMember'];
		}

        // Pre-load all custom fields
        $_preloaded_fields = [];
        foreach ($userFieldDetails as $fieldData) {
            $_preloaded_fields[$fieldData->getFieldId()] = $fieldData->getFieldType();
        }
        $additionalFieldIds = array_keys($_preloaded_fields);

        // Pre-load all group assignment rules
		$reader = Yii::app()->db->createCommand()
			->select('*')
			->from(CoreGroupAssignRules::model()->tableName())
			->where(array("IN", "idGroup", $groupsList))
			->query();
		while ($rule = $reader->read()) {
            // Filtering out fields that do not exist in the $_preloaded_members collection of user fields proofing
            // against cases of mixed old and new data source of additional fields (CoreField vs CoreUserField and
            // their related models)
            if (in_array($rule['idFieldCommon'], $additionalFieldIds) === true) {
                $_preloaded_assignRules[$rule['idGroup']][] = $rule;
            }
		}

        //free some memory
		$groupsList = NULL;
		unset($groupsList);

		foreach ($_preloaded_groups as $group)
		{
			$idGroup = $group['idst'];

			//first check if the user is already part of the group
			$alreadyInGroup = (isset($_preloaded_members[$idGroup]) && !empty($_preloaded_members[$idGroup])
				? in_array($id_user, $_preloaded_members[$idGroup])
				: false);
//				if ($aleradyInGroup) { continue; } //no need to perform further actions, the user is already assigned to this group anyway

			//retrieve all rules for current group
			$rules = (isset($_preloaded_assignRules[$idGroup]) ? $_preloaded_assignRules[$idGroup] : false);

			if (!empty($rules) && is_array($rules) || true) {

				$success = array();

                $dateFieldType 		= CoreUser::getDateFieldTypeIdentifier();
                $countryFieldType 	= CoreUser::getCountryFieldTypeIdentifier();
                $dropdownFieldType 	= CoreUser::getDropdownFieldTypeIdentifier();
                $textfieldFieldType = CoreUser::getTextfieldFieldTypeIdentifier();
                $yesnoFieldType 	= CoreUser::getYesnoFieldTypeIdentifier();

                //go through every rule and check the condition(s)
				foreach ($rules as $rule) {

                    $idRule = $rule['id'];
					$idField = $rule['idFieldCommon'];

					//retrieve user field entry for this rule
					$fieldValue = isset($_preloaded_userEntries[$id_user])
						? (isset($_preloaded_userEntries[$id_user][$idField])
								? $_preloaded_userEntries[$id_user][$idField]
								: false)
						: false;

					//if a value exists check if the rule applies
					if ($fieldValue !== false) {
						$fieldType = $_preloaded_fields[$idField];
						$ruleConditionType = $rule['condition_type'];
						$ruleValue = $rule['condition_value'];
						switch ($fieldType) {
							case $dateFieldType:
								switch ($ruleConditionType) {
									case CoreGroupAssignRules::CONDITION_TYPE_AFTER:
										$ruleValue = date('Y-m-d', strtotime($ruleValue));
										$success[$idRule] = $ruleValue < $fieldValue;
										break;
									case CoreGroupAssignRules::CONDITION_TYPE_BEFORE:
										$ruleValue = date('Y-m-d', strtotime($ruleValue));
										$success[$idRule] = $ruleValue > $fieldValue;
										break;
									case CoreGroupAssignRules::CONDITION_TYPE_FROM_TO:
										$dates = explode(',', $ruleValue);
										foreach ($dates as $idDate => $date) {
											$dates[$idDate] = date('Y-m-d', strtotime($date));
										}
										$success[$idRule] = ($dates[0] <= $fieldValue) && ($dates[1] >= $fieldValue);
										break;
									case CoreGroupAssignRules::CONDITION_TYPE_IS:
										$ruleValue = date('Y-m-d', strtotime($ruleValue));
										$success[$idRule] = $ruleValue == $fieldValue;
										break;
								}
								break;
							case $countryFieldType:
							case $dropdownFieldType:
								$values = explode(',', $ruleValue);
								$success[$idRule] = (!empty($fieldValue) && !empty($values) && in_array($fieldValue, $values));
								break;
							case $textfieldFieldType:
								switch ($ruleConditionType) {
									case CoreGroupAssignRules::CONDITION_TYPE_CONTAINS:
										$words = explode(' ', $fieldValue); //I guess this is no more used, right ?
										$success[$idRule] = (strpos($fieldValue, $ruleValue) !== false) ? true : false;
										break;
									case CoreGroupAssignRules::CONDITION_TYPE_EQUAL:
										$success[$idRule] = strcmp($ruleValue, $fieldValue) == 0;
										break;
									case CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL:
										$success[$idRule] = strcmp($ruleValue, $fieldValue) != 0;
										break;
								}
								break;
							case $yesnoFieldType:
								$success[$idRule] = $ruleValue == $fieldValue;
								break;
						}
					}
				}

				// Check if we can enroll the user in the group based on the AND / OR condition
				if (!empty($success) && !$alreadyInGroup) {
                    $countRules = count($rules);
					switch ($group['assign_rules_logic_operator']) {
						case CoreGroupAssignRules::LOGIC_OPERATOR_AND:
							if (!in_array(false, $success) && count($success) == $countRules) {
								$newGroupMember = new CoreGroupMembers();
								$newGroupMember->idst = $idGroup;
								$newGroupMember->idstMember = $id_user;
								$newGroupMember->save();
							}
							break;
						case CoreGroupAssignRules::LOGIC_OPERATOR_OR:
							if (in_array(true, $success)) {
								$newGroupMember = new CoreGroupMembers();
								$newGroupMember->idst = $idGroup;
								$newGroupMember->idstMember = $id_user;
								$newGroupMember->save();
							}
							break;
					}
				}elseif(((!in_array(true, $success) && $group['assign_rules_logic_operator'] == CoreGroupAssignRules::LOGIC_OPERATOR_OR) || (in_array(false, $success) &&  $group['assign_rules_logic_operator'] == CoreGroupAssignRules::LOGIC_OPERATOR_AND))  && $alreadyInGroup){
					$groupMember = CoreGroupMembers::model()->findByAttributes(array('idst'=>$idGroup, 'idstMember'=>$id_user));
					if($groupMember){
						$groupMember->delete();
					}
				}
			}
		}
	}

    /**
     * Automatically assigning users to groups
     * Invoked as a background job
     *
     * @param bool $params
     * @todo Improve performance by breaking up into smaller pieces of data at once
     */
	public function run($params = false)
	{
		$job = $this->job;
		if (!$job) {
			return;
		}

		// Load params for this job
		$params = json_decode($job->params, true);

		//expand and check users selection
		$userIdstStr = $params['user_idst'];
		$userIdstArr = explode(',', $userIdstStr);
		if (empty($userIdstArr)) {return; } //no users to be processed
		$userIdstArr = array_unique($userIdstArr); //just make sure not to have duplicate stuff in there
		$chunks = array_chunk($userIdstArr, 1000); //idst selection can be huge, so we split it in chunks of lesser size in order to not overload SQL with enormous queries

        $users = CoreUser::model()->findAllByAttributes(array(
            'idst' => $userIdstArr
        ));

		//prepare preloading variables
		$_preloaded_userEntries = array();
		$_preloaded_groups = array();
		$_preloaded_members = array();
		$_preloaded_assignRules = array();
		$_preloaded_fields = array();

		// Pre-load all groups with assign rules
		$reader = Yii::app()->db->createCommand()
			->select('idst, assign_rules_logic_operator')
			->from(CoreGroup::model()->tableName())
			->where('assign_rules = 1')
			->query();
		while ($group = $reader->read()) {
			$_preloaded_groups[] = $group;
		}
		if (empty($_preloaded_groups)) { return; } //no groups to be checked, nothing else to do here

		$groupsList = array(); //this will be used in later queries
		foreach ($_preloaded_groups as $group) {
			$groupsList[] = $group['idst']; //extract groups single idsts as a flat list
		}

		// Pre-load additional fields for the passed user
        foreach ($chunks as $chunk) {
            $_preloaded_userEntries = CoreUser::getAdditionalFieldEntryValuesArray($chunk, true, true);
		}

        // Pre-load group membership
		foreach ($chunks as $chunk) {
			$reader = Yii::app()->db->createCommand()
				->select('idst, idstMember')
				->from(CoreGroupMembers::model()->tableName())
				->where(array("IN", "idstMember", $chunk))
				->andWhere(array("IN", "idst", $groupsList)) //NOTE: we don't chunk groups array, since it is very unlikely that it will be big selection
				->query();
			while ($member = $reader->read()) {
				$_preloaded_members[$member['idst']][] = $member['idstMember'];
			}
		}

        // Pre-load all custom fields
        // Load additional field details as key-value pairs, {field id} => {field type}
        $fields = CoreUser::getAdditionalFieldsByLanguage();
        $_preloaded_fields = [];
        foreach ($fields as $field) {
            $_preloaded_fields[$field->getFieldId()] = $field->getFieldType();
        }
        $fieldIds = array_keys($_preloaded_fields);

		// Pre-load all group assignment rules
		$reader = Yii::app()->db->createCommand()
			->select('*')
			->from(CoreGroupAssignRules::model()->tableName())
			->where(array("IN", "idGroup", $groupsList))
			->query();
		while ($rule = $reader->read()) {
            // Filtering out fields that do not exist in the $_preloaded_members collection of user fields proofing
            // against cases of mixed old and new data source of additional fields (CoreField vs CoreUserField and
            // their related models)
            if (in_array($rule['idFieldCommon'], $fieldIds) === true) {
                $_preloaded_assignRules[$rule['idGroup']][] = $rule;
            }
		}

		//free some memory
		$chunks = NULL;
		$groupsList = NULL;
		unset($chunks);
		unset($groupsList);

		//check fields assignemnts for every passed user
		foreach ($userIdstArr as $userIdst) {

			foreach ($_preloaded_groups as $group) {

				$idGroup = $group['idst'];

				//first check if the user is already part of the group
				$alreadyInGroup = (isset($_preloaded_members[$idGroup]) && !empty($_preloaded_members[$idGroup])
					? in_array($userIdst, $_preloaded_members[$idGroup])
					: false);
//				if ($aleradyInGroup) { continue; } //no need to perform further actions, the user is already assigned to this group anyway

				//retrieve all rules for current group
				$rules = (isset($_preloaded_assignRules[$idGroup]) ? $_preloaded_assignRules[$idGroup] : false);

				if (!empty($rules) && is_array($rules)) {

					$success = array();

                    $dateFieldType 		= CoreUser::getDateFieldTypeIdentifier();
                    $countryFieldType 	= CoreUser::getCountryFieldTypeIdentifier();
                    $dropdownFieldType 	= CoreUser::getDropdownFieldTypeIdentifier();
                    $textfieldFieldType = CoreUser::getTextfieldFieldTypeIdentifier();
                    $yesnoFieldType 	= CoreUser::getYesnoFieldTypeIdentifier();

					//go through every rule and check the condition(s)
					foreach ($rules as $rule) {

						$idRule = $rule['id'];
						$idField = $rule['idFieldCommon'];

						//retrieve user field entry for this rule
						$fieldValue = isset($_preloaded_userEntries[$userIdst])
							? (isset($_preloaded_userEntries[$userIdst][$idField])
									? $_preloaded_userEntries[$userIdst][$idField]
									: false)
							: false;

						//if a value exists check if the rule applies
						if ($fieldValue !== false) {
							$fieldType = $_preloaded_fields[$idField];
							$ruleConditionType = $rule['condition_type'];
							$ruleValue = $rule['condition_value'];
							switch ($fieldType) {
								case $dateFieldType:
									switch ($ruleConditionType) {
										case CoreGroupAssignRules::CONDITION_TYPE_AFTER:
											$ruleValue = date('Y-m-d', strtotime($ruleValue));
											$success[$idRule] = $ruleValue < $fieldValue;
											break;
										case CoreGroupAssignRules::CONDITION_TYPE_BEFORE:
											$ruleValue = date('Y-m-d', strtotime($ruleValue));
											$success[$idRule] = $ruleValue > $fieldValue;
											break;
										case CoreGroupAssignRules::CONDITION_TYPE_FROM_TO:
											$dates = explode(',', $ruleValue);
											foreach ($dates as $idDate => $date) {
												$dates[$idDate] = date('Y-m-d', strtotime($date));
											}
											$success[$idRule] = ($dates[0] <= $fieldValue) && ($dates[1] >= $fieldValue);
											break;
										case CoreGroupAssignRules::CONDITION_TYPE_IS:
											$ruleValue = date('Y-m-d', strtotime($ruleValue));
											$success[$idRule] = $ruleValue == $fieldValue;
											break;
									}
									break;
								case $countryFieldType:
								case $dropdownFieldType:
									$values = explode(',', $ruleValue);
									$success[$idRule] = (!empty($fieldValue) && !empty($values) && in_array($fieldValue, $values));
									break;
								case $textfieldFieldType:
									switch ($ruleConditionType) {
										case CoreGroupAssignRules::CONDITION_TYPE_CONTAINS:
											$words = explode(' ', $fieldValue); //I guess this is no more used, right ?
											$success[$idRule] = (strpos($fieldValue, $ruleValue) !== false) ? true : false;
											break;
										case CoreGroupAssignRules::CONDITION_TYPE_EQUAL:
											$success[$idRule] = strcmp($ruleValue, $fieldValue) == 0;
											break;
										case CoreGroupAssignRules::CONDITION_TYPE_NOT_EQUAL:
											$success[$idRule] = strcmp($ruleValue, $fieldValue) != 0;
											break;
									}
									break;
								case $yesnoFieldType:
									$success[$idRule] = $ruleValue == $fieldValue;
									break;
							}
						}
					}

					// Check if we can enroll the user in the group based on the AND / OR condition
					if (!empty($success) && !$alreadyInGroup) {
						$countRules = count($rules);
						switch ($group['assign_rules_logic_operator']) {
							case CoreGroupAssignRules::LOGIC_OPERATOR_AND:
								if (!in_array(false, $success) && count($success) == $countRules) {
									$newGroupMember = new CoreGroupMembers();
									$newGroupMember->idst = $idGroup;
									$newGroupMember->idstMember = $userIdst;
									$newGroupMember->save();
								}
								break;
							case CoreGroupAssignRules::LOGIC_OPERATOR_OR:
								if (in_array(true, $success)) {
									$newGroupMember = new CoreGroupMembers();
									$newGroupMember->idst = $idGroup;
									$newGroupMember->idstMember = $userIdst;
									$newGroupMember->save();
								}
								break;
						}
					}elseif(((!in_array(true, $success) && $group['assign_rules_logic_operator'] == CoreGroupAssignRules::LOGIC_OPERATOR_OR) || (in_array(false, $success) &&  $group['assign_rules_logic_operator'] == CoreGroupAssignRules::LOGIC_OPERATOR_AND))  && $alreadyInGroup){
						$groupMember = CoreGroupMembers::model()->findByAttributes(array('idst'=>$idGroup, 'idstMember'=>$userIdst));
						if($groupMember){
							$groupMember->delete();
						}
					}
				}
			}
		}
	}

}
