<?php

class PENSJobs extends JobHandler implements IJobHandler{

	const JOB_NAME = 'PENSJob';

	public function init($params = false) {
	}


	public function run($params = false) {
		$job = $this->job;
		if(!$job){
			return;
		}

		//Get passed params the the Job
		$params = json_decode($job->params, true);
		if (!is_array($params) || !isset($params['request_params'])) {
			throw new CException('Invalid parameters');
		}
		$requestParams = $params['request_params'];
		if (!isset($requestParams['process']) || empty($requestParams['process'])) {
			return;
		}

		$handler = new DoceboPENSPackageHandler();
		$handler->setSupportedPackageTypes(array("scorm-pif", "aicc-pkg", "xapi-pkg"));
		$handler->setSupportedPackageFormats(array("zip", "url"));

		$server = DoceboPENSServer::singleton();
		$server->setRequestSourceParams($requestParams);
		$server->setPackageHandler($handler);
		$server->receiveCollect();
	}

	public static function id($params = false) {
		return 'common.components.job.handlers.PENSJobs';
	}
}
