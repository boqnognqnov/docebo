<?php
/**
 * 	Custom Report Generation Job handler
 *
 *  Extends the base JobHandler class and implements the IJobHandler interface.
 *  The main method is run(), executing the real job handler work.
 *  If the method ends with NO exception thrown, job is considered done with success.
 *
 *  @package common.components.job.handlers
 *
 */
class CustomReportGeneration extends JobHandler implements IJobHandler {

	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = JobHandler::JH_CUSTOMREPORT;

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		Yii::app()->session['schedulerExport'] = 1;

		// $this->job is already set at upper level (Main Job Controller)
		$job = $this->job;

		// No job - no work
		if (!$job) {
			return;
		}

		$params = json_decode($job->params, true);

		if (
				empty($params) ||
				!isset($params['id_filter']) ||
				!isset($params['recipients']) ||
				empty($params['recipients']) ||
				(int) $params['id_filter'] <= 0
			) {
			throw new Exception('Bad job parameters');
		}

		if(!isset($params['compressed']))
			$params['compressed'] = 1;

		// Get report model
		$reportModel = LearningReportFilter::model()->findByPk($params['id_filter']);

		// Prepare data provider and other info
		$reportData		= LearningReportFilter::getReportDataProviderInfo($reportModel->id_filter);

		$dataProvider 	= $reportData['dataProvider'];
		$columns 		= $reportData['gridColumns'];
		$reportTitle	= $reportData['reportTitle'];
		$sanitizedTitle = Sanitize::fileName($reportTitle, true);

		$csvFileName = $sanitizedTitle . ".csv";
		$csvFileName = $fileName ? $fileName : $csvFileName;
		$csvFilePath = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $csvFileName;

		$zipFileName = $sanitizedTitle . ".zip";

		// Write Header in the CSV
		$fh = fopen($csvFilePath, 'w');
		$headers = array();
		$values = array();

		// HEADERS
		foreach ($columns as $column) {
			if (isset($column['header'])) {
				$headers[] = $column['header'];
			}
			else if (isset($column['name'])) {
				$headers[] = $column['name'];
			}
		}
		// Write a HEADER line
		CSVParser::filePutCsv($fh, $headers);


		// Mmm.... we are about to start....
		$totalNumber = $dataProvider->totalItemCount;

		// Here we iterate in chunks (two level cycle)
		$i = 0;
		$offset = 0;
		$limit = Yii::app()->params['report_export_chunk_size'];
		while ($i < $totalNumber) {
			// Get SQL, add LIMIT/OFFSET
			$sql = $dataProvider->sql;
			$sql = $sql . " LIMIT $offset,$limit";
			$dataReader = Yii::app()->db->createCommand($sql)->query($dataProvider->params);
			$value = null; // reused
			foreach ($dataReader as $data) {
				$i++; // overall progress counter, we will stop when it reaches the total number
				$values = array();
				foreach ($columns as $column) {
					if($column['value'] instanceof Closure){
						// Yii column values can be anonymous or external functions as well, not just strings
						// @see: LearningReportFilter::getSingleColumn()
						$value = call_user_func($column['value'], $data);
					}else{
						eval('$value = ' . $column['value'] .';');
					}
					$values[] = $value;
				}
				CSVParser::filePutCsv($fh, $values);
			}
			// Next offset....
			$offset = $offset + $limit;
		}


		// Close the CSV file
		if ($fh) {
			fclose($fh);
		}

		// Zip the report file and remove it
		if($params['compressed'] == 1)
		{
			$zipFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $zipFileName;
			$zip = new Zip($zipFilePath);
			$zip->addFile($csvFilePath);
			$zip->close();
			FileHelper::removeFile($csvFilePath);
		}
		else
		{
			$zipFileName = $csvFileName;
			$zipFilePath = $csvFilePath;
		}


		// Attach the file to email
		// Send to all recipients
		$attachments = array(
			$zipFileName => $zipFilePath
		);
		$mailer = new MailManager();
		$fromArray  = array(Settings::get('sender_event', 'noreply@docebo.com'));
		$toArray 	= $params['recipients'];
		$domain = str_ireplace(array('http://', 'https://'), '', rtrim(Docebo::getCurrentDomain(), '/'));

		$substitution = array(
			'{title}' => $reportTitle,
			'{url}'	=> Docebo::createAbsoluteAdminUrl('reportManagement/customView', array('id' => $params['id_filter'])),
			'{domain}' => $domain
		);

		$subject = Yii::t('report', 'Scheduled report subject', $substitution);
		$message = Yii::t('report', 'Scheduled report message', $substitution);

		// Send emails, NOT using undisclosed approach
		$mailResult = $mailer->mail($fromArray, $toArray, $subject, $message, $attachments, false, false, array(), false);

		Yii::log('Mails send to: '.print_r($toArray, true), CLogger::LEVEL_INFO);

		unset(Yii::app()->session['schedulerExport']);

		if (!$mailResult) {
			throw new CException(Yii::t('report', 'Could not send scheduled report to recipients. Report ID: ' . $params['id_filter']));
		}

	}


	/**
	 * @see IJobHandler::id()
	 */
 	public static function id($params = false) {
 		return self::HANDLER_ID;
 	}


}