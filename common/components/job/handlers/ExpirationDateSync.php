<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 24-Feb-16
 * Time: 3:36 PM
 */
class ExpirationDateSync extends JobHandler implements IJobHandler
{
	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'common.components.job.handlers.ExpirationDateSync';


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level
		$job = $this->job;
		if (!$job) {
			return;
		}
		$this->syncUserExpiredAccount();
	}


	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}

	private function syncUserExpiredAccount(){
		$currentDate = Yii::app()->localtime->getUTCNow('Y-m-d');
		$godAdminIds = Yii::app()->user->getAllGodAdmins(true);
		Yii::app()->db->createCommand()
				->update(CoreUser::model()->tableName(), array('valid' => 0),
						'expiration<:now AND expiration IS NOT NULL AND expiration != "0000-00-00" AND idst NOT IN("'.implode('","', $godAdminIds).'")',
						array(':now' => $currentDate));
	}
}