<?php
/**
 * 	Bulk API task Job handler
 *  
 *  Extends the base JobHandler class and implements the IJobHandler interface. 
 *  The main method is run(), executing the real job handler work. 
 *  If the method ends with NO exception thrown, job is considered done with success.
 *
 */
class BulkApiTask extends JobHandler implements IJobHandler {

	const MAX_ROWS_PER_RUN = 100;
	const MAX_USERS_PER_RUN = 300;
	const MAX_GROUPS_PER_RUN = 300;

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * Runs the user synch import form
	 *
	 * @param $importForm UserImportForm
	 * @param $chunkData array
	 * @param $offset integer
	 */
	private function runUserTask($importForm, &$chunkData, $offset) {
		// Fill in password (if not present) with randomly generated one
		$passwordIndex = array_search("new_password", $importForm->getImportMap());
		$useridIndex = array_search("userid", $importForm->getImportMap());
		$existingUserIds = array();
		$userIds = array();
		if((is_int($useridIndex) || is_string ($useridIndex)) && is_array($chunkData)){
			// Select existing users in the current chunk
			$userIds = array_column($chunkData, $useridIndex);
		}

		if($userIds){
			foreach($userIds as &$userId)
				$userId = Yii::app()->db->quoteValue("/".$userId);

			$existingUserIds = Yii::app()->db->createCommand("SELECT SUBSTR(userid,2) FROM core_user WHERE userid IN (".implode(",", $userIds).")")->queryColumn();

		}

			foreach ($chunkData as &$user) {
			// Random password generation only for new users
			if (!in_array($user[$useridIndex], $existingUserIds) && (!isset($user[$passwordIndex]) || !trim($user[$passwordIndex]))) {
				$randomPassword = chr(mt_rand(65, 90))
					. chr(mt_rand(97, 122))
					. mt_rand(0, 9)
					. chr(mt_rand(97, 122))
					. chr(mt_rand(97, 122))
					. chr(mt_rand(97, 122))
					. mt_rand(0, 9)
					. chr(mt_rand(97, 122));
				$user[$passwordIndex] = $randomPassword;
			}
		}

		$importForm->setData($chunkData);
		$importForm->saveUsers(false, $offset === 0, $offset);  // Save users, do NOT validate, skip header if this is the very first 'page'
		return $importForm->totalUsers;
	}

	/**
	 * Runs the group synch import form
	 *
	 * @param $importForm GroupImportForm
	 * @param $chunkData array
	 * @param $offset integer
	 */
	private function runGroupTask($importForm, &$chunkData, $offset) {
		$importForm->setData($chunkData);
		$importForm->runItems(false, $offset === 0, $offset);
		return $importForm->totalItems;
	}
	
	private function runPowerUserTask($importForm, &$chunkData, $offset){
		$importForm->setData($chunkData);
		$importForm->runItems(false, $offset === 0, $offset);
		return $importForm->totalUsers;
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {

		// Get job instance
		$job = $this->job; /* @var $job CoreJob */
		
		// No job - no work
		if (!$job)
			return;

		$scheduler = Yii::app()->scheduler; /* @var $scheduler SchedulerComponent */
		$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
		$jobName = '';

		try {
			// Retrieve session params (including the import form)
			$params = json_decode($job->params, true);
			$formType = isset($params['form_type']) ? $params['form_type'] : 'UserImportForm';
			switch($formType) {
				case 'UserImportForm':
					$jobName = 'ApiUserSynchronization';
					break;
				case 'GroupImportForm':
					$jobName = 'ApiGroupSynchronization';
					break;
				case 'PoweruserUpdateForm':
					$jobName = 'ApiPoweruserUpdate';
					break;
			}

			$offset = isset($params['offset']) ? $params['offset'] : 0;
			$limit = isset($params['limit']) ? $params['limit'] : self::MAX_ROWS_PER_RUN;
			$importForm = new $formType();
			$importForm->scenario = 'step_two';
			foreach ($params['form'] as $attribute => $value) {
				if (property_exists($importForm, $attribute))
					$importForm->$attribute = $params['form'][$attribute];
				else if ($attribute == 'importMap')
					$importForm->setImportMap($value);
			}

			// Load file from S3 in local uploadtmp
			$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $importForm->originalFilename;
			$importForm->localFile = $storage->downloadAs($importForm->originalFilename, $localFilePath);

			// Create CSV manager, set numLines = 1 (1 user at max, just to have the object)
			$csvFile = new QsCsvFile(array(
				'path' => $importForm->localFile,
				'charset' => $importForm->charset,
				'delimiter' => UserImportForm::translateSeparatorName($importForm->separator, $importForm->manualSeparator),
				'firstRowAsHeader' => $importForm->firstRowAsHeader,
				'numLines' => 1, // excluding header, if any
			));

			// Get portion (chunk) of items...  and import them
			$chunkData = $csvFile->getChunk($limit, $offset);
			$importForm->failedLines = array();
			switch($formType) {
				case 'UserImportForm':
					$totalNumber = $this->runUserTask($importForm, $chunkData, $offset);
					break;
				case 'GroupImportForm':
					$totalNumber = $this->runGroupTask($importForm, $chunkData, $offset);
					break;
				case 'PoweruserUpdateForm':
					$totalNumber = $this->runPowerUserTask($importForm, $chunkData, $offset);
					break;
			}

			// Save errors in S3 log file
			if (count($importForm->failedLines) > 0) {
				$logFileName = $job->id_job . "_failed_lines.csv";
				$localLogFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $logFileName;
				$storage->downloadAs($logFileName, $localLogFilePath);

				// Open file in append mode
				$fh = fopen($localLogFilePath, 'a');
				foreach ($importForm->failedLines as $failedLine)
					fputs($fh, $failedLine . "\n");
				fclose($fh);

				// Save file back to S3
				$storage->store($localLogFilePath);

				// Clean failed lines array
				$importForm->failedLines = array();
			}

			// Check if we're done processing chunks
			$nextOffset = $offset + $limit;
			$finishedTask = $nextOffset > $totalNumber;
			if ($finishedTask) {
				// Deactivate job and delete it from Sidekiq
				$job->active = 0;
				Yii::app()->scheduler->deleteJob($job);

				// Delete file from S3
				$storage->remove($importForm->originalFilename);
			}

			// Save session data back into the params column
			$job->params = json_encode(array('offset' => $nextOffset, 'limit' => $limit, 'form' => $importForm->getParams(), 'form_type' => $formType));
			$job->save();

			// Recreate the immediate job with the same hash
			if (!$finishedTask)
				$scheduler->createImmediateJob($jobName, BulkApiTask::id(), false, false, $job);

		} catch(Exception $e) {
			// In case of errors, just make sure that the job is still renewed
			$scheduler->createImmediateJob($jobName, BulkApiTask::id(), false, false, $job);

			throw $e;
		}
	}

	
	/**
	 * @see IJobHandler::id()
	 */
 	public static function id($params = false) {
 		return 'common.components.job.handlers.BulkApiTask';
 	}

	/**
	 * Returns whether this handler allows for immediate jobs to be reused
	 * @return bool
	 */
	public function canReuseSameImmediateJob() {
		return true;
	}

}

// ======================================================================
//            Implementation of array_column, if PHP <= 5.5
// ======================================================================
if (!function_exists('array_column')) {
	/**
	 * Returns the values from a single column of the input array, identified by
	 * the $columnKey.
	 *
	 * Optionally, you may provide an $indexKey to index the values in the returned
	 * array by the values from the $indexKey column in the input array.
	 *
	 * @param array $input A multi-dimensional array (record set) from which to pull
	 *                     a column of values.
	 * @param mixed $columnKey The column of values to return. This value may be the
	 *                         integer key of the column you wish to retrieve, or it
	 *                         may be the string key name for an associative array.
	 * @param mixed $indexKey (Optional.) The column to use as the index/keys for
	 *                        the returned array. This value may be the integer key
	 *                        of the column, or it may be the string key name.
	 * @return array
	 */
	function array_column($input = null, $columnKey = null, $indexKey = null)
	{
		// Using func_get_args() in order to check for proper number of
		// parameters and trigger errors exactly as the built-in array_column()
		// does in PHP 5.5.
		$argc = func_num_args();
		$params = func_get_args();

		if ($argc < 2) {
			trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
			return null;
		}

		if (!is_array($params[0])) {
			trigger_error(
				'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
				E_USER_WARNING
			);
			return null;
		}

		if (!is_int($params[1])
			&& !is_float($params[1])
			&& !is_string($params[1])
			&& $params[1] !== null
			&& !(is_object($params[1]) && method_exists($params[1], '__toString'))
		) {
			trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		if (isset($params[2])
			&& !is_int($params[2])
			&& !is_float($params[2])
			&& !is_string($params[2])
			&& !(is_object($params[2]) && method_exists($params[2], '__toString'))
		) {
			trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		$paramsInput = $params[0];
		$paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

		$paramsIndexKey = null;
		if (isset($params[2])) {
			if (is_float($params[2]) || is_int($params[2])) {
				$paramsIndexKey = (int) $params[2];
			} else {
				$paramsIndexKey = (string) $params[2];
			}
		}

		$resultArray = array();

		foreach ($paramsInput as $row) {
			$key = $value = null;
			$keySet = $valueSet = false;

			if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
				$keySet = true;
				$key = (string) $row[$paramsIndexKey];
			}

			if ($paramsColumnKey === null) {
				$valueSet = true;
				$value = $row;
			} elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
				$valueSet = true;
				$value = $row[$paramsColumnKey];
			}

			if ($valueSet) {
				if ($keySet) {
					$resultArray[$key] = $value;
				} else {
					$resultArray[] = $value;
				}
			}

		}

		return $resultArray;
	}

}