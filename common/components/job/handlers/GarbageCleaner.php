<?php
/**
 * A class and methods called as immediate job handler to run various cleaners, defined HERE!
 *  
 * Usage example: Run a job to run _cleanerJobs method
 * 
 *		$params = array(
 *			'target' => GarbageCleaner::TARGET_JOBS,
 *		);
 *		Yii::app()->scheduler->createImmediateJob('GarbageCleaner', GarbageCleaner::id(), $params);
 *
 * 
 *  
 */
class GarbageCleaner extends JobHandler implements IJobHandler {
	
	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'common.components.job.handlers.GarbageCleaner';
	
	/**
	 * 
	 * @var string
	 */
	const TARGET_JOBS = 'jobs';
	

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		
		$job = $this->job;
		$logCat = "job-".$job->id_job . " " . $job->handler_id;
		$params = json_decode($job->params, true);
		
		// This job REQUIRES at least the TARGET (the object to clean up)
		if (!isset($params['target']) || !is_string($params['target'])) {
			return;
		}
		
		$target = $params['target'];
		
		Yii::log('Garbage cleaner started', CLogger::LEVEL_INFO, $logCat);
		Yii::log('Target: ' . $target, CLogger::LEVEL_INFO, $logCat);
		
		// Call class method, named like "_cleanerJobs", where "Jobs" is capitalized (first character) target name 
		$method = "_cleaner" . ucfirst($target);
		
		if (!method_exists($this, $method)) {
			Yii::log("Cleaner method GarbageCleaner::$method does not exists", CLogger::LEVEL_ERROR, $logCat);
		}
		else {
			$this->{$method}();
		}
		
	}
	
	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}

	
	/**
	 * Get configured probability to run particualr cleaning target, e.g. jobs, etc.
	 *
	 * @param string $target ID of the cleaner target
	 */
	public static function getProbability($target) {
		$prob = 0; // fraction of million, ZERO probability means NO, never
		if (isset(Yii::app()->params['garbage_collection_probability'][$target])) {
			$prob = (int) Yii::app()->params['garbage_collection_probability'][$target];
		}
		else if (isset(Yii::app()->params['garbage_collection_probability']['default'])) {
			$prob = (int) Yii::app()->params['garbage_collection_probability']['default'];
		}
		return $prob;
	}
	
	
	//-- DEFINE CLEANERS BELOW
	// Follow this naming pattern:
	//
	//     private function _cleaner<TargetName> () {
    //     }
    //	
	// where <TargetName> is the target when you create onetime jobs like this:
	
	//     Yii::app()->scheduler->createImmediateJob('GarbageCleaner', GarbageCleaner::id(), array(
	//         'target' => GarbageCleaner::TARGET_JOBS,
	//         'days'	 => 10,
	//     ));
	
	
	/**
	 * Clean up core_job table
	 * @param unknown $jobParams
	 */
	private function _cleanerJobs() {
		
		$job = $this->job;
		$logCat = "job-".$job->id_job . " " . $job->handler_id;
		$params = json_decode($job->params, true);
		
		$days = 7;
		if (isset($params['days'])) {
			$days = (int) $params['days'];
		}
		
		Yii::log("Cleaning up core_job table, remove ONETIME jobs, older than $days days", CLogger::LEVEL_INFO, $logCat);
		
		$sql = "DELETE FROM core_job WHERE (created < NOW() - INTERVAL $days DAY) AND (type = 'onetime')";
		try {
			$affected_rows = Yii::app()->db->createCommand($sql)->execute();
			Yii::log("$affected_rows deleted", CLogger::LEVEL_INFO, $logCat);
		}
		catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, $logCat);
		}
		
	}
	
	
}

