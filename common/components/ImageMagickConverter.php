<?php

class ImageMagickConverter extends CApplicationComponent {

	/**
	 * Store the width of proccessed image
	 * @var integer|float
	 */
	private $_width;

	/**
	 * Store the height of proccessed image
	 * @var integer|float
	 */
	private $_height;

	/**
	 * quality in perecents from original thumbnail to destination image
	 * @var integer|float
	 */
	private $_quality = 90;

	/**
	 * Store the image original path
	 * @var string
	 */
	private $_sourcePath;

	/**
	 *
	 * Set the patg to destionation image
	 * @var string
	 */
	private $_destinationPath;

	/**
	 * If you want strict resize in pixels, not per small size,
	 * this prop will controll the cropping after resize on image
	 * @default: false;
	 * @var boolean
	 */
	private $_cropAfterResize = false;

	/**
	 * Default arrea for cropping
	 * @var string
	 */
	private $_gravity = 'center';

	/**
	 * Resize sufix modificators
	 * @var mixed
	 */
	private $_resizeModificators = array('^');

	/**
	 * @var array default convert options.
	 * Possible values may vary depending on particular convertor.
	 */
	protected $_defaultOptions = array();

	/**
	 * Sets up the default convert options.
	 * @param array $defaultOptions default convert options.
	 * @return boolean success.
	 */
	public function setDefaultOptions(array $defaultOptions) {
		$this->_defaultOptions = $defaultOptions;
		return true;
	}

	/**
	 * Returns the default convert options.
	 * @return array default convert options
	 */
	public function getDefaultOptions() {
		return $this->_defaultOptions;
	}

	/**
	 * @var string path in the directory, which stores ImageMagick binaries.
	 * For example: '/usr/local/bin'.
	 * This path will be used to compose console command name.
	 * You may leave this field blank if ImageMagick commands are available in OS shell.
	 */
	protected $_binPath = '';

	public function __construct($sourcePath = false) {
		if (!empty($sourcePath)) {
			$this->setSourcePath($sourcePath);
		}
	}

	// Set / Get :
	
	public function setBinPath($binPath) {
		if (!is_string($binPath)) {
			throw new CException('"'.get_class($this).'::binPath" should be a string!');
		}
		$this->_binPath = $binPath;
		return true;
	}

	public function getBinPath() {
		return $this->_binPath;
	}

	public function mogrify($fileName, array $options = array()) {
		$options = $this->composeOptions($options);

		if (array_key_exists('extent', $options)) {
			if (is_array($options['extent'])) {
				$dimensions        = array_slice($options['extent'], 0, 2);
				$extentOption      = implode('x', array_values($dimensions));
				$options['extent'] = $extentOption;
			}
		}
		$consoleCommandParams   = $options;
		$consoleCommandParams[] = escapeshellarg($fileName);
		$this->executeConsoleCommand('mogrify', $consoleCommandParams);
		return true;
	}

	public function colortone($imagePath, $color) {
		return true;
	}

	public function getImageColor($imagePath) {
		exec("convert '{$imagePath}' -colors 1 -depth 8 -format \"%c\" histogram:info:", $a);
		if (!empty($a[0])) {
			$find  = preg_match("/#(.*)r/", $a[0], $matches);
			$color = "#" . trim($matches['1']);
			$color = substr($color, 0, 7);
			return $color;
		} else return false;
	}

	/**
	 * Executes shell console command.
	 * @param string $commandName - console command name.
	 * @param array $params - console command parameters.
	 * @return array - command output lines.
	 */
	protected function executeConsoleCommand($commandName, array $params=array()) {
		$consoleCommandString = $this->composeConsoleCommand($commandName, $params);
		$this->log("Execute command: {$consoleCommandString}");
		exec($consoleCommandString, $output);
		$this->log("Command output: ".implode("\n",$output));
		return $output;
	}

	/**
	 * Composes shell console command string.
	 * @param string $commandName - console command name.
	 * @param array $params - console command parameters.
	 * @return string - command string.
	 */
	protected function composeConsoleCommand($commandName, array $params=array()) {
		$binPath = rtrim( $this->getBinPath(), DIRECTORY_SEPARATOR );
		if (!empty($binPath)) {
			$commandFullName = $binPath.DIRECTORY_SEPARATOR.$commandName;
		} else {
			$commandFullName = $commandName;
		}
		$consoleCommandString = "\"{$commandFullName}\" ";
		$consoleCommandString .= ' '.$this->composeConsoleCommandParams($params);
		$consoleCommandString .= ' 2>&1';
		return $consoleCommandString;
	}

	/**
	 * Composes console command params into a string,
	 * which is suitable to be passed to console.
	 * @param array $params - console command parameters.
	 * @return string - command params part.
	 */
	protected function composeConsoleCommandParams(array $params) {
		$consoleCommandParts = array();
		foreach ($params as $paramKey=>$paramValue) {
			if (is_numeric($paramKey)) {
				$consoleCommandParts[] = $paramValue;
			} else {
				$consoleCommandParts[] = "-{$paramKey} ".escapeshellarg($paramValue);
			}
		}
		return implode(' ', $consoleCommandParts);
	}

	/**
	 * Converts the file according to the given options.
	 * @param string $srcFileName - source full file name.
	 * @param string $outputFileName - output full file name.
	 * @param array $options - convert options.
	 * @return boolean - success.
	 */
	public function convert(array $options = array()) {
		try {
			$this->_proccessResize();
			if (!empty($this->_width) && $this->getCropAfterResize()) {
				$this->_proccessCropping();
			}
			$options = $this->composeOptions($options);
			$consoleCommandParams = array(
				escapeshellarg($this->getSourcePath())
			);
			$consoleCommandParams = array_merge($consoleCommandParams, $options);
			$consoleCommandParams[] = escapeshellarg($this->getDestinationPath());
			$this->executeConsoleCommand('convert', $consoleCommandParams);
			return true;
		} catch (CException $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Retrieves the information about the file.
	 * The returned data set may vary for the different convertors.
	 * @param string $fileName - full file name.
	 * @return array - list of file parameters.
	 */
	public function getFileInfo($fileName) {
		$consoleCommandParams = array(
			'-verbose',
			escapeshellarg($fileName)
		);
		$consoleOutputLines = $this->executeConsoleCommand('identify', $consoleCommandParams);
		return $this->parseFileInfo($consoleOutputLines);
	}

	/**
	 * Parse console command output in order to compose file info.
	 * @param array $consoleOutputLines - console command output lines.
	 * @return array file info data.
	 */
	protected function parseFileInfo(array $consoleOutputLines) {
		$consoleOutput = implode("\n", $consoleOutputLines);
		if ( count($consoleOutputLines)<=1 || stripos($consoleOutput, '@ error')!==false ) {
			throw new CException($consoleOutput);
		}

		$fileInfoString = trim($consoleOutput);
		$fileInfo = array();

		if ( preg_match_all('/^([a-z0-9 ]+):(.*)$/im', $fileInfoString, $matches) ) {
			foreach ($matches[1] as $paramKey => $paramName) {
				$paramName = trim($paramName);
				$paramValue = $matches[2][$paramKey];
				$fileInfo[$paramName] = $paramValue;
			}

			if (array_key_exists('Geometry',$fileInfo)) {
				if ( preg_match('/([0-9]+)x([0-9]+)/is', $fileInfo['Geometry'], $matches) ) {
					$imageWidth = $matches[1];
					$imageHeight = $matches[2];
					$fileInfo['imageSize'] = array(
						'width' => $imageWidth,
						'height' => $imageHeight,
					);
				}
			}
		}

		return $fileInfo;
	}

	/**
	 * Performs the file conversion with image resizing.
	 * This is a shortcut method for the {@link convert} with option 'resize'.
	 * @param array $options - additional convert options.
	 * @return boolean - success.
	 */
	public function resize(array $options = array()) {
		return $this->convert($options);
	}

	private function _proccessResize($withModificator = true) {
		$resizeOption = '';
		if (!empty($this->_height) && !empty($this->_width)) {
			$resizeOption .= implode('x', array($this->getWidth(), $this->getHeight()));
		} else if (!empty($this->_width)) {
			$resizeOption .= $this->getWidth();
		}
		if ((bool) $withModificator) {
			$resizeOption .= implode($this->getResizeModificators());
			$this->setOption('resize', $resizeOption);
		}
		return $resizeOption;
	}

	/**
	 * @todo To be dynamized cropping offsets s
	 * @return \ImageMagickConverter
	 */
	private function _proccessCropping() {
		$this->setOption('gravity', $this->getGravity());
		$croppingSizes = $this->_proccessResize(false);
		$croppingSizes .= '+0+0';
		$this->setOption('crop', $croppingSizes);
		return $this;
	}

	/**
	 * Composes options, merging default ones with the given ones.
	 * @param array $options - options list.
	 * @return array - composed options.
	 */
	protected function composeOptions(array $options) {
		$options = CMap::mergeArray($this->getDefaultOptions(), $options);
		return $options;
	}

	/**
	 * Logs a message.
	 * @see CLogRouter
	 * @param string $message message to be logged.
	 * @param string $level level of the message (e.g. 'trace', 'warning', 'error'). It is case-insensitive.
	 * @return boolean success.
	 */
	protected function log($message, $level=CLogger::LEVEL_INFO) {
		$category = 'qs.files.convert.'.get_class($this);
		Yii::log($message, $level, $category);
		return true;
	}

	/* ------------------------------------------------------------------------
	 * GETTERS
	 * -------------------------------------------------------------------- - */

	/**
	 * Get proccessed image width for output
	 * @return integer|float
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * Get proccessed image height for output
	 * @return integer|float
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * Get proccessed image quality for output
	 * @return integer|float
	 */
	public function getQuality() {
		return $this->_quality;
	}

	/**
	 * Get proccessed image source path in local storage
	 * @return string
	 */
	public function getSourcePath() {
		return $this->_sourcePath;
	}

	/**
	 * Get proccessed image destination path in local storage
	 * @return string
	 */
	public function getDestinationPath() {
		return $this->_destinationPath;
	}

	/**
	 * Get curent flag for smart resize with crop or only resize per small size
	 * @return bool
	 */
	public function getCropAfterResize() {
		return $this->_cropAfterResize;
	}

	/**
	 * Get Default gravity
	 * @return string
	 */
	public function getGravity() {
		return $this->_gravity;
	}

	/**
	 * Get single key option
	 * @param string $keyOption
	 * @return string|array
	 */
	public function getOption($keyOption) {
		return isset($this->_defaultOptions[$keyOption]) ? $this->_defaultOptions[$keyOption] : array();
	}

	/**
	 * Get resized modificators appended after resize dimensions
	 * @return array
	 */
	public function getResizeModificators() {
		return $this->_resizeModificators;
	}

	/* ------------------------------------------------------------------------
	 * SETTERS
	 * -------------------------------------------------------------------- - */

	/**
	 *
	 * @param integer|float $width pixels for image in X
	 * @return \ImageMagickConverter
	 */
	public function setWidth($width) {
		$this->_width = $width;
		return $this;
	}

	/**
	 *
	 * @param type $height pixels for image in Y
	 * @return \ImageMagickConverter
	 */
	public function setHeight($height) {
		$this->_height = $height;
		return $this;
	}

	/**
	 * Set the new quality for output image
	 * @param float|integer $quality output quality per original image quality
	 * @return \ImageMagickConverter
	 */
	public function setQuality($quality) {
		$this->setOption('quality', $quality);
		$this->_quality = $quality;
		return $this;
	}

	/**
	 * Set source path to proccessed image
	 * @param string $sourcePath path to image on the local storage
	 * @return \ImageMagickConverter
	 */
	public function setSourcePath($sourcePath) {
		$this->_sourcePath = $sourcePath;
		return $this;
	}

	/**
	 * Set destionation path for proccessed output file
	 * @param string $destinationPath path to destination folder
	 * @return \ImageMagickConverter
	 */
	public function setDestinationPath($destinationPath) {
		$this->_destinationPath = $destinationPath;
		return $this;
	}

	/**
	 * If this is set as true, will crop unused pixels from image
	 * @param bool $cropAfterResize
	 * @return \ImageMagickConverter
	 */
	public function setCropAfterResize($cropAfterResize) {
		$this->_cropAfterResize = $cropAfterResize;
		return $this;
	}

	/**
	 * Set gravity option
	 * @param string $gravity
	 * @return \ImageMagickConverter
	 */
	public function setGravity($gravity) {
		$this->_gravity = $gravity;
		return $this;
	}

	/**
	 * Set single option on general array with all options
	 * @param string $keyOption Key on setting for append
	 * @param mixed $optionValue Value for append
	 * @return boolean|\ImageMagickConverter
	 */
	public function setOption($keyOption, $optionValue) {
		try {
			if (isset($this->_defaultOptions[$keyOption]) && is_array($this->_defaultOptions[$keyOption])) {
				$this->_defaultOptions[$keyOption] = CMap::mergeArray((array) $this->_defaultOptions[$keyOption], $optionValue);
			} else {
				$this->_defaultOptions[$keyOption] = $optionValue;
			}
			return $this;
		} catch (Exception $ex) {
			Yii::log($ex->getMessage(), CLogger::LEVEL_ERROR);
			return false;
		}
	}

	/**
	 * Set set with modificators
	 * @param array $resizeModificators
	 * @return \ImageMagickConverter
	 */
	public function setResizeModificators($resizeModificators) {
		$this->_resizeModificators = $resizeModificators;
		return $this;
	}

	/**
	 * append new modificator suffix
	 * @param string $resizeModificator
	 * @return \ImageMagickConverter
	 */
	public function appendResizeModificator($resizeModificator) {
		$this->_resizeModificators[] = $resizeModificator;
		return $this;
	}

}
