<?php

class CloudConvert extends CComponent {

	private $apikey;
    private $url;
    private $data;
	private $initUrl = 'https://api.cloudconvert.com/process';
	
	public $collection;
	
	public function __construct($collection = CFileStorage::COLLECTION_7020) {
		$this->collection = $collection;
	}
	
	/*
     * creates new Process ID.
     * see: https://cloudconvert.org/page/api#start
     *
     */
    public static function createProcess($inputFormat, $outputFormat, $apiKey = null) {
		if(empty($apiKey)){
			$apiKey = Yii::app()->params['cloudconvert']['apikey'];
		}
        $instance = new self();
        $instance->apikey = $apiKey;
        $data = $instance->httpRequest($instance->initUrl, array(
            'inputformat' => $inputFormat,
            'outputformat' => $outputFormat,
            'apikey' => $apiKey
        ));
        if (strpos($data->url, 'http') === false){
			$data->url = "https:" . $data->url;
		}
            
        $instance->url = $data->url;
        return $instance;
    }
	
	public function getURL() {
        return $this->url;
    }
    
	/*
     * uses existing Process by given Process URL
     */
    public static function useProcess($url) {
        $instance = new self();
        if (strpos($url, 'http') === false)
            $url = "https:" . $url;
        $instance->url = $url;
        return $instance;
    }
	
	
	
	public function convertWebsite($idContent, $outputFormat = "png") {
		$instance = self::createProcess("website", $outputFormat);
		
		$contentModel = App7020Assets::model()->findByPk($idContent);
		$url = $contentModel->originalFilename;
		
		if($this->collection == CFileStorage::COLLECTION_7020){
			$postFields["input"] = "url";
			$postFields["callback"] = self::getNotificationUrl('webConvertNotify');
			$postFields["save"] = "true";
			$postFields["inputformat"] = "website";
			$postFields["outputformat"] = $outputFormat;
			$postFields["file"] = $url;
			$postFields["preset"] = Yii::app()->params['cloudconvert']['web_convert_preset'];
			$postFields["output"]["s3"]["accesskeyid"] = Yii::app()->params['s3Storages']['amazon']['key'];
			$postFields["output"]["s3"]["secretaccesskey"] = Yii::app()->params['s3Storages']['amazon']['secret'];
			$postFields["output"]["s3"]["bucket"] = Yii::app()->params['s3Storages']['amazon']['bucket_name'];
			$postFields["output"]["s3"][ "key"] = 'files/'.Yii::app()->params['cloudconvert']['web_convert_s3_temp_folder'] . '/' . $contentModel->filename;
		}
		
		$convertionInstance = $this->httpRequest($instance->url, $postFields);
		
		
		$convertionModel = new App7020DocumentConversions();
		$convertionModel->idContent = $idContent;
		$convertionModel->idPipe = $convertionInstance->id;
		$convertionModel->urlPipe = $instance->url;
		$convertionModel->idUser = Yii::app()->user->idst;
		$convertionModel->created = date("Y-m-d H:i:s", $convertionInstance->starttime);
		$convertionModel->expired = date("Y-m-d H:i:s",$convertionInstance->expire); 
		
		$convertionModel->used = 0;
		
		$convertionModel->save();
		
	}
	
	public function convertFromS3ToS3($idContent, $outputFormat = "png"){
		
		$contentModel = App7020Assets::model()->findByPk($idContent);
		$filename = $contentModel->filename;
		
		if(!$inputFormat = App7020Assets::isConvertableDocument($filename)){
			return false;
		}
		
		$instance = self::createProcess($inputFormat, $outputFormat);
		
		if(!$instance->url){
			return false;
		}
				
		$storage = CFileStorage::getDomainStorage($this->collection);
		$postFields = array();
		
		if($this->collection == CFileStorage::COLLECTION_7020){
			
			$outputFolder = $storage->getOutputFolder($filename);
			$postFields["callback"] = self::getNotificationUrl('docConvertNotify');
			$postFields["inputformat"] = $inputFormat;
			$postFields["outputformat"] = $outputFormat;
			$postFields["file"] = $outputFolder.$filename;
			$postFields["input"] = array("s3" => array());
			$postFields["output"] = array("s3" => array());
			$postFields["input"]["s3"]["accesskeyid"] = Yii::app()->params['s3Storages']['amazon']['key'];
			$postFields["input"]["s3"]["secretaccesskey"] = Yii::app()->params['s3Storages']['amazon']['secret'];
			$postFields["input"]["s3"]["bucket"] = Yii::app()->params['s3Storages']['amazon']['bucket_name'];
			$postFields["output"]["s3"]["accesskeyid"] = Yii::app()->params['s3Storages']['amazon']['key'];
			$postFields["output"]["s3"]["secretaccesskey"] = Yii::app()->params['s3Storages']['amazon']['secret'];
			$postFields["output"]["s3"]["bucket"] = Yii::app()->params['s3Storages']['amazon']['bucket_name'];
			$postFields["output"]["s3"]["acl"] = 'public-read';
			$postFields["output"]["s3"]["key"] = $outputFolder. App7020Assets::DOCUMENT_IMAGES_S3_FOLDER."/".App7020Helpers::getAmazonInputKey($filename)."/";
			
		}
		
		$convertionInstance = $this->httpRequest($instance->url, $postFields);
		
		if(!$convertionInstance->url){
			return false;
		}
		
		if (strpos($convertionInstance->url, 'http') === false){
			$convertionInstance->url = "https:" . $convertionInstance->url;
		}
		
		
		$convertionModel = new App7020DocumentConversions();
		$convertionModel->idContent = $idContent;
		$convertionModel->idPipe = $convertionInstance->id;
		$convertionModel->urlPipe = $convertionInstance->url;
		$convertionModel->idUser = Yii::app()->user->idst;
		$convertionModel->created = date("Y-m-d H:i:s", $convertionInstance->starttime);
		$convertionModel->expired = date("Y-m-d H:i:s",$convertionInstance->expire); 
		
		$convertionModel->used = 0;
		
		$convertionModel->save();
		
		
	}


	public function slidesConvertFromS3ToS3($idAuthoring, $outputFormat = "jpg", $organizationSync = true){

		$authoringModel = LearningAuthoring::model()->findByPk($idAuthoring);
		if(!$authoringModel)
			return false;

		$filename = $authoringModel->authoring_id . "_1." . $authoringModel->file_extension;

		$instance = self::createProcess($authoringModel->file_extension, $outputFormat);

		if(!$instance->url){
			return false;
		}

		$storage = CFileStorage::getDomainStorage($this->collection);
		$postFields = array();

		if($this->collection == CFileStorage::COLLECTION_LO_AUTHORING){

			$outputFolder = $storage->absoluteBasePath . "/" . $storage->subFolder . "/" . ((string)$authoringModel->authoring_id) . "/"; 
			$outputFolder = substr($outputFolder,1);
			$postFields["callback"] = Docebo::createAbsoluteUrl('authoring:main/updateConversion');
			$postFields["inputformat"] = $authoringModel->file_extension;
			$postFields["outputformat"] = $outputFormat;
			$postFields["file"] = $outputFolder.$filename;
			$postFields["input"] = array("s3" => array());
			$postFields["output"] = array("s3" => array());
			$postFields["input"]["s3"]["accesskeyid"] = Yii::app()->params['s3Storages']['amazon']['key'];
			$postFields["input"]["s3"]["secretaccesskey"] = Yii::app()->params['s3Storages']['amazon']['secret'];
			$postFields["input"]["s3"]["bucket"] = Yii::app()->params['s3Storages']['amazon']['bucket_name'];
			$postFields["output"]["s3"]["accesskeyid"] = Yii::app()->params['s3Storages']['amazon']['key'];
			$postFields["output"]["s3"]["secretaccesskey"] = Yii::app()->params['s3Storages']['amazon']['secret'];
			$postFields["output"]["s3"]["bucket"] = Yii::app()->params['s3Storages']['amazon']['bucket_name'];
			$postFields["output"]["s3"]["acl"] = 'public-read';
			$postFields["output"]["s3"]["key"] = $outputFolder;

		}

		$convertionInstance = $this->httpRequest($instance->url, $postFields);

		if(!$convertionInstance->url){
			return false;
		}

		if (strpos($convertionInstance->url, 'http') === false){
			$convertionInstance->url = "https:" . $convertionInstance->url;
		}

		$authoringModel->pipeline_id = $convertionInstance->id;

		$authoringModel->organizationSync = $organizationSync;
		$authoringModel->save();



	}


	
	public static function getNotificationUrl($method) {
		$url = Docebo::createAbsoluteApp7020Url("app7020CloudConvert/$method");
		if (YII_DEBUG && preg_match('/(.*)(lms.+)\.(.+)\.scavaline\.(\w{2,4})\/(.*)$/i', $url, $debugUrl)) {
			$port = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020)->serviceParams['app7020']['debug_port'];
			$url = $debugUrl[1] . $debugUrl[2] . '.' . $debugUrl[3] . '.scavaline.' . $debugUrl[4] . ':' . $port . '/' . $debugUrl[5];
			return $url;
		} elseif (YII_DEBUG && preg_match('/(.*)(lms.+)\.scavaline\.(\w{2,4})\/(.*)$/i', $url, $debugUrl)) {
			$port = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_7020)->serviceParams['app7020']['debug_port'];
			$url = $debugUrl[1] . $debugUrl[2] . '.scavaline.' . $debugUrl[3] . ':' . $port . '/' . $debugUrl[4];
			return $url;
		}
		return $url;
	}
    
    /*
     * Checks the current status of the process
     */
    public function status($action = null) {
        if (empty($this->url))
            throw new Exception("No process URL found! (Conversion not started)");
        $this->data = $this->httpRequest($this->url . ($action ? '/' . $action : ''));
        return $this->data;
    }
    
	public function cancel() {
        return $this->status('cancel');
    }
    public function delete() {
        return $this->status('delete');
    }
    
    public function httpRequest($url, $post = null) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        /*
         * If you have SSL cert errors, try to disable SSL verifyer.
         */
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, TRUE);
			if(is_array($post)){
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
			} else {
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			}
            
        }
        $return = curl_exec($ch);
        if ($return === FALSE) {
            throw new Exception(curl_error($ch));
        } else {
            $json = json_decode($return);
            if (isset($json->error)){
				throw new Exception($json->error);
			}
            return $json;
        }
        curl_close($ch);
    }
}
