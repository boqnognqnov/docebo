<?php
/**
 * Extending CHttpClass to provide some useful functionality
 *
 */
class DoceboHttpRequest extends CHttpRequest {

	public $noCsrfValidationRoutes = array();


	private $rawBody = null;

	public function getRawBody()
	{
		if($this->rawBody === null){
			$this->rawBody = parent::getRawBody();
		}
		return $this->rawBody;
	}

	public function setRawBody($body)
	{
		$this->rawBody = $body;
	}


	/**
	 * Override to handle trusted routes.
	 *
	 * To list trusted routes (which will not be checked against CSRF token) use the config file
	 * like this:
	 *
	 * <pre>
	 *    'request'=>array(
	 *        'class'=>'DoceboHttpRequest',
	 *        'noCsrfValidationRoutes'=>array(
	 *            'controller/action.*'   // ^ and $ are added internally !!!
	 *        ),
	 *        'enableCsrfValidation'=>true,
	 *        'enableCookieValidation'=>true,
	 *    ),
	 * </pre>
	 *
	 * @see CHttpRequest::normalizeRequest()
	 */
	protected function normalizeRequest() {
		parent::normalizeRequest();

        if(!empty($_GET['domain'])) {
            $this->setBaseUrl("/".$_GET['domain'].$this->getBaseUrl());
        }

		if ( $_SERVER['REQUEST_METHOD'] != 'POST' ) return;

		$route = Yii::app()->getUrlManager()->parseUrl($this);

		if ( $this->enableCsrfValidation ) {
			foreach ( $this->noCsrfValidationRoutes as $cr ) {
				if (preg_match('#^'.$cr.'$#', $route)) {
					Yii::app()->detachEventHandler('onBeginRequest', array($this,'validateCsrfToken'));
					break;
				}
			}
		}
	}

	/**
	 * @see CHttpRequest::createCsrfCookie()
	 * Creates a cookie with random value for CSRF protection.
	 * We only added the 'httpOnly' flag here (since the original YII
	 * method doesn't provide a parameter, we had to overwrite the
	 * whole CSRF cookie creation method).
	 * Called internally by CHttpRequest::getCsrfToken()
	 *
	 * @return CHttpCookie the generated cookie
	 */
	public function createCsrfCookie()
	{
		$cookie=new CHttpCookie($this->csrfTokenName,sha1(uniqid(mt_rand(),true)), array(
		    'httpOnly'=>true
		));
		if(is_array($this->csrfCookie))
		{
			foreach($this->csrfCookie as $name=>$value)
				$cookie->$name=$value;
		}
		return $cookie;
	}

	/**
	 * Event handler for the 'onBeginRequest' event. CSRF token validation
	 * is skipped if the request is added to the whitelist in the
	 * main.php->components->request->noCsrfValidationRoutes array
	 *
	 * @param CEvent $event
	 *
	 * @return bool|void
	 */
	public function validateCsrfToken($event) {

		$route = Yii::app()->getUrlManager()->parseUrl($this);

		if ( $this->enableCsrfValidation ) {
			foreach ( $this->noCsrfValidationRoutes as $cr ) {
				if (preg_match('#^'.$cr.'$#', $route)) {
					return true;
					break;
				}
			}
		}

		try
		{
			return parent::validateCsrfToken($event);
		}
		catch(CHttpException $e)
		{
			Log::_($e->getMessage(), CLogger::LEVEL_WARNING);
			Yii::app()->end();
		}
    }


    /**
     * Rewrite this method to allow giving custom name of the file being downloaded
     *
     * (non-PHPdoc)
     * @see CHttpRequest::sendFile()
     */
    public function sendFile($fileName,$content,$mimeType=null,$terminate=true, $customFilename=null) {

    	if($mimeType===null)
    	{
    		if(($mimeType=CFileHelper::getMimeTypeByExtension($fileName))===null)
    			$mimeType='text/plain';
    	}

    	$fileSize=(function_exists('mb_strlen') ? mb_strlen($content,'8bit') : strlen($content));
    	$contentStart=0;
    	$contentEnd=$fileSize-1;

    	if(isset($_SERVER['HTTP_RANGE']))
    	{
    		header('Accept-Ranges: bytes');

    		//client sent us a multibyte range, can not hold this one for now
    		if(strpos($_SERVER['HTTP_RANGE'],',')!==false)
    		{
    			header("Content-Range: bytes $contentStart-$contentEnd/$fileSize");
    			throw new CHttpException(416,'Requested Range Not Satisfiable');
    		}

    		$range=str_replace('bytes=','',$_SERVER['HTTP_RANGE']);

    		//range requests starts from "-", so it means that data must be dumped the end point.
    		if($range[0]==='-')
    			$contentStart=$fileSize-substr($range,1);
    		else
    		{
    			$range=explode('-',$range);
    			$contentStart=$range[0];

    			// check if the last-byte-pos presents in header
    			if((isset($range[1]) && is_numeric($range[1])))
    				$contentEnd=$range[1];
    		}

    		/* Check the range and make sure it's treated according to the specs.
    		 * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    		*/
    		// End bytes can not be larger than $end.
    		$contentEnd=($contentEnd > $fileSize) ? $fileSize-1 : $contentEnd;

    		// Validate the requested range and return an error if it's not correct.
    		$wrongContentStart=($contentStart>$contentEnd || $contentStart>$fileSize-1 || $contentStart<0);

    		if($wrongContentStart)
    		{
    			header("Content-Range: bytes $contentStart-$contentEnd/$fileSize");
    			throw new CHttpException(416,'Requested Range Not Satisfiable');
    		}

    		header('HTTP/1.1 206 Partial Content');
    		header("Content-Range: bytes $contentStart-$contentEnd/$fileSize");
    	}
    	else
    		header('HTTP/1.1 200 OK');

    	$length=$contentEnd-$contentStart+1; // Calculate new content length

    	header('Pragma: public');
    	header('Expires: 0');
    	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    	header("Content-Type: $mimeType");
    	header('Content-Length: '.$length);
    	if ($customFilename) {
    		header("Content-Disposition: attachment; filename=\"$customFilename\"");
    	}
    	else {
    		header("Content-Disposition: attachment; filename=\"$fileName\"");
    	}
    	header('Content-Transfer-Encoding: binary');
    	$content=function_exists('mb_substr') ? mb_substr($content,$contentStart,$length) : substr($content,$contentStart,$length);

    	if($terminate)
    	{
    		// clean up the application first because the file downloading could take long time
    		// which may cause timeout of some resources (such as DB connection)
    		ob_start();
    		Yii::app()->end(0,false);
    		ob_end_clean();
    		echo $content;
    		exit(0);
    	}
    	else
    		echo $content;
    }


	/**
	 * Read a file from local file system and send content to client for download.
	 *
	 * This method is by far better than the normal sendFile() in that it does NOT load the file content into
	 * the server memory, thus avoiding possible Apache/PHP crash due to large files.
	 *
	 * @param string      $file
	 * @param string      $customFilename Use this to give the downloaded file a different name
	 * @param bool $terminate
	 */
    public function sendFileDirect($file, $customFilename=null, $terminate=true) {

    	// Use 'friendly' file name if provided
    	$filename = $customFilename ? $customFilename : basename($file);

    	// Read and send the file (no memory allocation)
    	if (file_exists($file)) {
    		header('HTTP/1.1 200 OK');
    		header('Content-Description: File Transfer');
    		header('Content-Type: application/octet-stream');
    		header('Content-Disposition: attachment; filename="'. $filename . '"');
    		header('Content-Transfer-Encoding: binary');
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    		header('Pragma: public');
    		header('Content-Length: ' . filesize($file));

    		if ($terminate) {
    			ob_start();
    			Yii::app()->end(0,false);
    			ob_end_clean();
    			flush();
    			readfile($file);
    			exit;
    		}
    		else {
    			readfile($file);
    		}
    	}

    }





}
