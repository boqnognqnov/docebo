<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 11.12.2015 г.
 * Time: 13:39 ч.
 */
class TimeHelpers {

	/**
	 * Build an array of [minutes]=>[user friendly label], e.g.
	 * [0]=>00:00, [15]=>00:15, ... [60]=>01:00, ... [600]=>10:00
	 *
	 * @param bool $addZero
	 * @param int  $minIntervalMinutes
	 *
	 * @return array
	 */
	public static function arrayForDurations ( $addZero = FALSE, $minIntervalMinutes = 15 ) {
		$res = array();

		if( $addZero ) {
			$res[0] = '00:00';
		}

		$minIntervalSeconds = ($minIntervalMinutes*60);

		for ( $s = $minIntervalSeconds; $s < 86400; $s += $minIntervalSeconds ) {
			// minutes => duration in user friendly, e.g. "11:45"
			$minutes         = ( (int) $s / 60 );
			$res[ $minutes ] = gmdate( 'H:i', $s );
		}

		return $res;
	}

	static public function getLocalizedMonthNames() {
		$months = array(
			"_JANUARY", "_FEBRUARY", "_MARCH", "_APRIL", "_MAY", "_JUNE",
			"_JULY", "_AUGUST", "_SEPTEMBER", "_OCTOBER", "_NOVEMBER", "_DECEMBER"
		);

		foreach ($months as &$month)
			$month = Yii::t('calendar', $month);

		return $months;
	}
}