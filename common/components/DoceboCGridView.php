<?php
Yii::import('zii.widgets.grid.CGridView');
class DoceboCGridView extends CGridView {
	public $summaryText = false;
	public $inlineEditAjaxUrl;
	public $cssFile = null;
	public $pagerCssClass = 'doceboPager';
	public $tableInlineStyle = '';

	/**
	 * @var bool this will insert init script straight into dialog content, not in other positions (useful when rendering grids into dialogs etc.)
	 */
	public $loadIntoDialog = false;

	public function init()
	{
		Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/CGridViewInlineEdit.js');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/CGridViewInlineEdit.css');
		if($this->cssFile===null) {
			$this->cssFile = Yii::app()->theme->baseUrl . '/css/DoceboCGridView.css';
		}

//		$this->emptyText = Yii::t('report', '_NULL_REPORT_RESULT');

		parent::init();
	}

	/**
	 * Renders the data items for the grid view.
	 * Added one more container - gridItemsContainer
	 * It is used in case we want some items content scrolling or similar
	 */
	public function renderItems()
	{
		if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
		{
			echo "<div class=\"gridItemsContainer\"><table class=\"{$this->itemsCssClass}\" style=\"{$this->tableInlineStyle}\">\n";
			$this->renderTableHeader();
			ob_start();
			$this->renderTableBody();
			$body=ob_get_clean();
			$this->renderTableFooter();
			echo $body; // TFOOT must appear before TBODY according to the standard.
			echo "</table></div>";
		}
		else
			$this->renderEmptyText();
	}

	public function renderPager()
	{
		if(!$this->enablePagination)
			return;

		$pager=array();
		$class='DoceboCLinkPager';
		if(is_string($this->pager))
			$class=$this->pager;
//		else if(is_array($this->pager))
//		{
//			$pager=$this->pager;
//			if(isset($pager['class']))
//			{
//				$class=$pager['class'];
//				unset($pager['class']);
//			}
//		}
		$pager['pages']=$this->dataProvider->getPagination();

		//if($pager['pages']->getPageCount()>1)
		//{
			echo '<div class="'.$this->pagerCssClass.'">';
			$this->widget($class,$pager);
			echo '</div>';
		//}
		//else
		//	$this->widget($class,$pager);
	}

	public function renderTableRow($row)
	{
		if ($row == 0)
			echo '<div id="gridToolTip" onmouseover="gridToolTipDivOver()" onmouseout="gridToolTipClose()"></div>';

		$htmlOptions=array();
		if($this->rowHtmlOptionsExpression!==null)
		{
			$data=$this->dataProvider->data[$row];
			$options=$this->evaluateExpression($this->rowHtmlOptionsExpression,array('row'=>$row,'data'=>$data));
			if(is_array($options))
				$htmlOptions = $options;
		}

		if($this->rowCssClassExpression!==null)
		{
			$data=$this->dataProvider->data[$row];
			$class = $this->evaluateExpression($this->rowCssClassExpression,array('row'=>$row,'data'=>$data));
		}
		else if(is_array($this->rowCssClass) && ($n=count($this->rowCssClass))>0)
			$class = $this->rowCssClass[$row%$n];

		if(!empty($class))
		{
			if(isset($htmlOptions['class']))
				$htmlOptions['class'].=' '.$class;
			else
				$htmlOptions['class']=$class;
		}

		echo CHtml::openTag('tr', $htmlOptions)."\n";
		foreach($this->columns as $column) {
			$column->renderDataCell($row);
		}
		echo "</tr>\n";
	}

	/*
	 * Overrided method to change CDataColumn usage to DoceboCDataColumn
	 */
	protected function initColumns()
	{
		//Trigger event to let plugins to manipulate grid
		$event = new DEvent($this, array ());
		Yii::app()->event->raise('DoceboCGridView.beforeInitColumns', $event);
		//end event

		if($this->columns===array())
		{
			if($this->dataProvider instanceof CActiveDataProvider)
				$this->columns=$this->dataProvider->model->attributeNames();
			else if($this->dataProvider instanceof IDataProvider)
			{
				// use the keys of the first row of data as the default columns
				$data=$this->dataProvider->getData();
				if(isset($data[0]) && is_array($data[0]))
					$this->columns=array_keys($data[0]);
			}
		}
		$id=$this->getId();
		foreach($this->columns as $i=>$column)
		{
			if(is_string($column))
				$column=$this->createDataColumn($column);
			else
			{
				if(!isset($column['class']))
					$column['class']='DoceboCDataColumn';
				$column=Yii::createComponent($column, $this);
			}
			if(!$column->visible)
			{
				unset($this->columns[$i]);
				continue;
			}
			if($column->id===null)
				$column->id=$id.'_c'.$i;
			$this->columns[$i]=$column;
		}

		foreach($this->columns as $column)
			$column->init();

		//Trigger event to let plugins to manipulate grid
		$event = new DEvent($this, array ());
		Yii::app()->event->raise('DoceboCGridView.afterInitColumns', $event);
		//end event
	}

	/*
	 * Overrided method to change CDataColumn usage to DoceboCDataColumn
	 */
	protected function createDataColumn($text)
	{
		if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$text,$matches))
			throw new CException(Yii::t('zii','The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
		$column=new DoceboCDataColumn($this);
		$column->name=$matches[1];
		if(isset($matches[3]) && $matches[3]!=='')
			$column->type=$matches[3];
		if(isset($matches[5]))
			$column->header=$matches[5];
		return $column;
	}

	/**
	 * Registers necessary client scripts.
	 */
	public function registerClientScript()
	{
		$id=$this->getId();

		if($this->ajaxUpdate===false)
			$ajaxUpdate=false;
		else
			$ajaxUpdate=array_unique(preg_split('/\s*,\s*/',$this->ajaxUpdate.','.$id,-1,PREG_SPLIT_NO_EMPTY));
		$options=array(
			'ajaxUpdate'=>$ajaxUpdate,
			'ajaxVar'=>$this->ajaxVar,
			'pagerClass'=>$this->pagerCssClass,
			'loadingClass'=>$this->loadingCssClass,
			'filterClass'=>$this->filterCssClass,
			'tableClass'=>$this->itemsCssClass,
			'selectableRows'=>$this->selectableRows,
			'enableHistory'=>$this->enableHistory,
			'updateSelector'=>$this->updateSelector,
			'filterSelector'=>$this->filterSelector
		);
		if($this->ajaxUrl!==null)
			$options['url']=CHtml::normalizeUrl($this->ajaxUrl);
		if($this->ajaxType!==null) {
			$options['ajaxType']=strtoupper($this->ajaxType);
			$request=Yii::app()->getRequest();
			if ($options['ajaxType']=='POST' && $request->enableCsrfValidation) {
				$options['csrfTokenName']=$request->csrfTokenName;
				$options['csrfToken']=$request->getCsrfToken();
			}
		}
		if($this->enablePagination)
			$options['pageVar']=$this->dataProvider->getPagination()->pageVar;
		if($this->beforeAjaxUpdate!==null)
			$options['beforeAjaxUpdate']=(strpos($this->beforeAjaxUpdate,'js:')!==0 ? 'js:' : '').$this->beforeAjaxUpdate;
		if($this->afterAjaxUpdate!==null)
			$options['afterAjaxUpdate']='js:'.'function(id, data){customAfterAjaxUpdate('.(strpos($this->afterAjaxUpdate,'js:')===0 ? str_replace("js:", "", $this->afterAjaxUpdate) : $this->afterAjaxUpdate).', id, data)}';
		else
			$options['afterAjaxUpdate']='js:'.'function(id, data){customAfterAjaxUpdate(null, id, data)}';
		if($this->ajaxUpdateError!==null)
			$options['ajaxUpdateError']=(strpos($this->ajaxUpdateError,'js:')!==0 ? 'js:' : '').$this->ajaxUpdateError;
		if($this->selectionChanged!==null)
			$options['selectionChanged']=(strpos($this->selectionChanged,'js:')!==0 ? 'js:' : '').$this->selectionChanged;


		$options=CJavaScript::encode($options);
		$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('bbq');
		if($this->enableHistory)
			$cs->registerCoreScript('history');
		$cs->registerScriptFile($this->baseScriptUrl.'/jquery.yiigridview.js',CClientScript::POS_END);

		/**
		 * Wrap the initialization of the grid view inside a global function,
		 * so that it can be manually called within an ajax callback
		 */
		$funcId = ucwords(str_replace(array('-'), array(' '), strtolower($id)));
		$funcId = str_replace(' ', '', $funcId);
		$funcName = 'doceboCGridViewInit' . $funcId;
		//make gridview able to work in context like dialogs (usually dynamically loaded)
		if ($this->loadIntoDialog) {
			echo '<script type="text/javascript">';
			echo ";$funcName = function(){ jQuery('#$id').yiiGridView($options); };$($funcName);";
			echo '</script>';
		} else {
			$cs->registerScript(__CLASS__ . '#' . $id, ";function $funcName(){ jQuery('#$id').yiiGridView($options); }; $funcName();");
		}
	}

	public function renderEmptyText()
	{
		$emptyText=$this->emptyText===null ? Yii::t('report','_NULL_REPORT_RESULT') : $this->emptyText;
		echo CHtml::tag($this->emptyTagName, array('class'=>'empty'), $emptyText);
	}
}
