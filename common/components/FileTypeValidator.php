<?php
/**
 * Used to filter file-types on upload, like PHP files and so on.
 *
 * NOTES:
 *
 * 1. When you create an object of this class, you specify not just the FILE TYPE (although name imply that).
 * You choose a named "set" of files extensions to be whitelisted.
 *
 * 2. As an example see both TYPE_VIDEO_EXTENDED_LIST and TYPE_VIDEO. They are both intended for VIDEO files, but they
 * define two different SET of extension that should be white listed! Keep that in mind!
 *
 * 3.This is required because we have different places in LMS requiring/allowing different sets of formats for files of the same
 * MAJOR type (like VIDEO). At some point we allow ONLY 'mp4', at another place we allow 'mp4' and 'avi'.
 *
 *
 * Class FileTypeValidator
 */
class FileTypeValidator extends CValidator
{

	private $blackList = array(
		'pl', 'rb', 'aspx', 'py', 'jsp', 'asp', 'aspx', 'shtml', 'sh', 'cgi', 'php', 'ph3', 'php4', 'php5', 'phps', 'htaccess',
	);

	/**
	 * List of allowed file extension depending on the file type
	 *
	 * Note: ...or more likely: on the intended type of white list. We can have IMAGE, IMAGE_EXTENDED, IMAGE_FOR_SOMETHING, etc.
	 *
	 * @return array
	 */
	private function whiteListArray()
	{
		switch ($this->fileType)
		{
			case self::TYPE_IMAGE:
				return array('jpeg','jpg','gif','png', 'ico');
				break;
			case self::TYPE_VIDEO:
				return array('mp4', 'wmv');
				break;
			case self::TYPE_VIDEO_EXTENDED_LIST:
				return array('mp4', 'avi', 'mpg', 'mpeg', 'mpeg4', 'mpg2', '3gp', '3gp2', 'flv', 'mov', 'wmv', 'ogm', 'mkv', 'mp3');
			case self::TYPE_AUTHORING:
				return array('ppt', 'pptx', 'pdf', 'odp', 'doc', 'docx');
 			case self::TYPE_APP7020_CONTENT:
				// @TODO Call some App7020 method to return full WHITE LIST of file extensions
				// For now: hard coded:
				return array('zip','doc','xls','ppt','jpg','jpeg','gif','png','txt','docx','pptx','xlsx','pdf','csv','bmp','ico','mp3','ogg','epub','mp4','avi','mpg','mpeg','mpeg4','mpg2','3gp','3gp2','flv','mov','wmv','ogm','mkv','ppsx','vtt','srt','wrf','arf');
				break;

			case self::TYPE_SUBTITLES:
				return array('vtt', 'srt');
				break;

			case self::TYPE_FORUM_UPLOADS:
				return array('zip','doc','xls','ppt','jpg','jpeg','gif','png','txt','docx','pptx','xlsx','pdf','csv','bmp','ico');
				break;

			default:
				return array('zip','doc','xls','ppt','jpg','jpeg','gif','png','txt','docx','pptx','ppsx','xlsx','pdf','csv','bmp','ico','mp3','ogg','epub','mp4','avi','mpg','mpeg','mpeg4','mpg2','3gp','3gp2','flv','mov','wmv','ogm','mkv','vtt','srt','wrf','arf');
				break;
		}
	}

	private $fileType;
	private $isEmpty;

	const TYPE_IMAGE = 'image';
	const TYPE_ITEM = 'item';
	const TYPE_ARCHIVE = 'archive';
	const TYPE_VIDEO = 'video';
	const TYPE_VIDEO_EXTENDED_LIST = 'video_extended_list';
	const TYPE_AUTHORING = 'authoring';
	const TYPE_APP7020_CONTENT = 'app7020_content';
	const TYPE_SUBTITLES = 'subtitles';
	const TYPE_FORUM_UPLOADS = 'forum_uploads';

	public $errorMsg;

	public function __construct($type = self::TYPE_ITEM, $isEmpty = false)
	{
		$this->fileType = $type;
		$this->isEmpty = $isEmpty;
	}
	/**
	 * Active validator for where the Yii Active Form is used
	 * @param CModel $model
	 * @param string $attribute //should be instance of CUploadedFile
	 */
	protected function validateAttribute($model, $attribute)
	{
		if ($model->$attribute instanceof CUploadedFile)
		{
			if (!$this->isValidFileType($model->$attribute))
				$this->addError($model, $attribute, $this->errorMsg);
		}
	}

	/**
	 * Validate file with NON ActiveForm instance
	 * @param $fileName
	 * @return bool
	 */
	public function validateFile($fileName)
	{
		$file = CUploadedFile::getInstanceByName($fileName);
		return $this->isValidFileType($file);
	}

	/**
	 * Validate already uploaded file (not from $_FILES etc)
	 * @param $filePath
	 * @return bool
	 */
	public function validateLocalFile($filePath)
	{
		return $this->isValidFileType($filePath);
	}

	/*#####################################
	 * Internal methods used for validation:
	 ######################################*/


	private function validateZip()
	{
		switch ($this->fileType)
		{
			case self::TYPE_ARCHIVE:
				return true;
			break;
			default:
				return false;
		}
	}

	/**
	 * @param $file
	 * @return bool
	 */
	private function isValidFileType($file)
	{
        if ($this->isEmpty === true && $file === null)
            return true;

		$res = false;
		if ($file instanceof CUploadedFile)
		{
			$extension = $file->extensionName;
			$filePath = $file->tempName;
		}
		else
		{
			$fileHelper = new CFileHelper();
			$extension = $fileHelper->getExtension($file);
			$filePath = $file;
		}
		$extension = strtolower($extension);
		if (in_array($extension, $this->whiteListArray()))
		{
			if ($this->validateZip() && $extension == 'zip')
				$res = $this->validateArchive($filePath);
			else
				$res = true;
		}

		if (!$res)
		{
			if ($extension == 'zip')
				$this->errorMsg = Yii::t('fileValidator', 'Some of the archive files are not allowed');
			else
				$this->errorMsg = Yii::t('fileValidator', 'File type not allowed');
		}

		return $res;
	}

	/**
	 * Validate ZIP archive content - file extensions
	 * @param $filePath string
	 * @return bool
	 */
	private function validateArchive($filePath)
	{
		$res = true;
		$za = new ZipArchive();
		$za->open($filePath);

		for( $i = 0; $i < $za->numFiles; $i++ ){
			$stat = $za->statIndex( $i );
			if (substr($stat['name'], -1) != '/') // skip folders
			{
				if (!$this->validateString($stat['name']))
				{
					$res = false;
					break;
				}
			}
		}
		return $res;
	}

	/**
	 * Validate file name as string -> check if it ends with black listed extension name
	 * @param $string
	 * @return bool
	 */
	private function validateString($string)
	{
		$existsInList = false;
		foreach ($this->blackList as $ext)
		{
			if (preg_match("/\.".$ext."$/", strtolower($string)))
			{
				$existsInList = true;
				break;
			}
		}
		return $existsInList ? false : true; //reverse
	}



	public static function getWhiteListArray($type) {
 		$object = new self($type);
 		$result = $object->whiteListArray();
		if (!is_array($result))
			$result = array();
		return $result;
	}

	/**
	 * Returns a whitelist of mime types for the current object type
	 *
	 * @param $type The type of file upload
	 * @return array Array of mime types
	 */
	public static function getMimeWhiteListArray($type) {
		$result = array();
		$extensions = self::getWhiteListArray($type);
		if(!empty($extensions)) {
			$extensionsToMimeTypes = require(Yii::getPathOfAlias('common.components.mimeTypes').'.php');
			foreach($extensions as $extension)
				if(isset($extensionsToMimeTypes[$extension]))
					$result[] = $extensionsToMimeTypes[$extension];
		}
		return $result;
	}
}
