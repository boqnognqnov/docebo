<?php
Yii::import('zii.widgets.grid.CGridView');
class UsersSelectorCGridView extends CGridView {
	public $pagerCssClass = 'doceboPager';
	public $pager = 'DoceboCLinkPager';

	
	public function init()
	{
		$this->cssFile = Yii::app()->theme->baseUrl.'/css/DoceboCGridView.css';
		$this->emptyText = Yii::t('report', '_NULL_REPORT_RESULT');
		parent::init();
	}
	
	
	/**
	 * Renders the data items for the grid view.
	 * Added one more container - gridItemsContainer
	 * It is used in case we want some items content scrolling or similar
	 * 
	 * (non-PHPdoc)
	 * @see CGridView::renderItems()
	 */
	public function renderItems()
	{
		if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
		{
			echo "<div class=\"gridItemsContainer\"><table class=\"{$this->itemsCssClass}\">\n";
			$this->renderTableHeader();
			ob_start();
			$this->renderTableBody();
			$body=ob_get_clean();
			$this->renderTableFooter();
			echo $body; // TFOOT must appear before TBODY according to the standard.
			echo "</table></div>";
		}
		else
			$this->renderEmptyText();
	}


	
	/**
	 * Registers necessary client scripts.
	 */
	public function registerClientScript()
	{
		$id=$this->getId();
	
		if($this->ajaxUpdate===false)
			$ajaxUpdate=false;
		else
			$ajaxUpdate=array_unique(preg_split('/\s*,\s*/',$this->ajaxUpdate.','.$id,-1,PREG_SPLIT_NO_EMPTY));
		$options=array(
				'ajaxUpdate'=>$ajaxUpdate,
				'ajaxVar'=>$this->ajaxVar,
				'pagerClass'=>$this->pagerCssClass,
				'loadingClass'=>$this->loadingCssClass,
				'filterClass'=>$this->filterCssClass,
				'tableClass'=>$this->itemsCssClass,
				'selectableRows'=>$this->selectableRows,
				'enableHistory'=>$this->enableHistory,
				'updateSelector'=>$this->updateSelector,
				'filterSelector'=>$this->filterSelector
		);
		if($this->ajaxUrl!==null)
			$options['url']=CHtml::normalizeUrl($this->ajaxUrl);
		if($this->ajaxType!==null) {
			$options['ajaxType']=strtoupper($this->ajaxType);
			$request=Yii::app()->getRequest();
			if ($options['ajaxType']=='POST' && $request->enableCsrfValidation) {
				$options['csrfTokenName']=$request->csrfTokenName;
				$options['csrfToken']=$request->getCsrfToken();
			}
		}
		if($this->enablePagination)
			$options['pageVar']=$this->dataProvider->getPagination()->pageVar;
		foreach(array('beforeAjaxUpdate', 'afterAjaxUpdate', 'ajaxUpdateError', 'selectionChanged') as $event)
		{
			if($this->$event!==null)
			{
				if($this->$event instanceof CJavaScriptExpression)
					$options[$event]=$this->$event;
				else
					$options[$event]=new CJavaScriptExpression($this->$event);
			}
		}
	
		$options=CJavaScript::encode($options);
		$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('bbq');
		if($this->enableHistory)
			$cs->registerCoreScript('history');
		$cs->registerScriptFile($this->baseScriptUrl.'/jquery.yiigridview.js',CClientScript::POS_END);
		$cs->registerScript(__CLASS__.'#'.$id,"jQuery('#$id').yiiGridView($options);");
	}
	
	
	

}
?>
