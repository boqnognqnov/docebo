<?php


class BackgroundJobBridge
{
    const BACKGROUND_JOB_API_ENDPOINT = 'job';
    const USER_IMPORT_ENPOINT = '/manage/v1/user/batch';
    const IMPORT_TYPE_CSV_USER_IMPORT = 'csv_user_import';

    const IMPORTER_KEY_USERNAME = 'username';
    const IMPORTER_KEY_FIRSTNAME = 'firstname';
    const IMPORTER_KEY_LASTNAME = 'lastname';
    const IMPORTER_KEY_PASSWORD = 'password';
    const IMPORTER_KEY_PW_HASH = 'password_hash';
    const IMPORTER_KEY_EMAIL = 'email';
    const IMPORTER_KEY_TIMEZONE = 'timezone';
    const IMPORTER_KEY_DATE_FORMAT = 'date_format';
    const IMPORTER_KEY_ACTIVE = 'active';
    const IMPORTER_KEY_EXP_DATE = 'expiration_date';
    const IMPORTER_KEY_LEVEL = 'level';
    const IMPORTER_KEY_PROFILE = 'profile';
    const IMPORTER_KEY_LANGUAGE = 'language';
    const IMPORTER_KEY_BRANCH_NAME = 'branch_name';
    const IMPORTER_KEY_BRANCH_CODE = 'branch_code';
    const IMPORTER_KEY_BRANCH_NAME_PATH = 'branch_name_path';
    const IMPORTER_KEY_BRANCH_CODE_PATH = 'branch_code_path';

    public static function importUsersFromCSV($params)
    {
        $map = self::generateMap($params);
        $chunkSize = self::getChunkSize($map);

        $config = array(
            'name' => isset($params['name']) ? $params['name'] : '',
            'endpoint' => self::USER_IMPORT_ENPOINT,
            'method' => HydraApiClient::METHOD_POST,
            'chunk_size' => $chunkSize,
            'type' => 'importer',
            'data_source' => array(
                'type' => self::IMPORT_TYPE_CSV_USER_IMPORT,
                'datasource_params' => array(
                    'filename' =>
                        isset($params['filename']) ? $params['filename'] : null,

                    'skip_first_row' =>
                        isset($params['firstRowAsHeader']) ? (bool)$params['firstRowAsHeader'] : false,

                    'charset' =>
                        isset($params['charset']) ? $params['charset'] : 'UTF-8',

                    'delimiter' => self::getDelimiter($params)
                ),
                'mapping' => $map,
                'options' => array(
                    'change_user_password' =>
                        (isset($params['passwordChanging']) && $params['passwordChanging'] == 'yes') ? true : false,

                    'update_user_info' =>
                        isset($params['insertUpdate']) ? (bool)$params['insertUpdate'] : false,

                    'auto_assign_branch_to_pu' =>
                        isset($params['autoAssignBranches']) ? (bool)$params['autoAssignBranches'] : true,

                    'ignore_password_change_for_existing_users' =>
                        isset($params['ignorePasswordChangeForExistingUsers']) ? (bool)$params['ignorePasswordChangeForExistingUsers'] : false,

                    'branch_option' =>
                        isset($params['branchOption']) ? $params['branchOption'] : 'existing',

                    'destination_branch' =>
                        (isset($params['node']) && $params['node'] != -1) ? $params['node'] : null
                )
            ),
            'notify_email' => (isset($params['sendLogToMail'])) ? $params['sendLogToMail'] : null,
            'notify' => (isset($params['sendLogToMail']) && !empty($params['sendLogToMail']))
        );

        try {
            // generate an oauth token and make a request to the hydra API
            $idAuthor = isset($params['id_author']) ? $params['id_author'] : null;
            $server = DoceboOauth2Server::getInstance();
            $server->init();
            $token = $server->createAccessToken('hydra_frontend', $idAuthor, 'api');

            if ($token) {
                $apiObj = new HydraApiClient('manage');
                $apiObj->setAccessToken($token['access_token']);
                $apiObj->execute(self::BACKGROUND_JOB_API_ENDPOINT, $config, HydraApiClient::METHOD_POST, false);
            }
        } catch (Exception $e) {
            Yii::log("Api call FAILED: " . $e->getMessage(), CLogger::LEVEL_ERROR, 'hydra-api-client');
        }
    }

    /**
     * Creates an array that contains the indexes and codes of the mapped fields
     * @param array $params
     * @return array
     */
    private static function generateMap($params)
    {
        $map = array();
        if (isset($params['importMap'])) {
            $mapChunks = explode(',', $params['importMap']);
            foreach ($mapChunks as $index => $code) {
                $code = self::convertCode($code);
                if (!$code) {
                    continue;
                }

                $map[] = array(
                    'index' => $index,
                    'code' => $code
                );
            }
        }

        return $map;
    }

    /**
     * Converts a legacy code into a hydra one.
     * @param string $code import schema code
     * @return string
     */
    private static function convertCode($code)
    {
        switch ($code) {
            case 'userid' : return self::IMPORTER_KEY_USERNAME;
            case 'new_password' : return self::IMPORTER_KEY_PASSWORD;
            case 'user_level' : return self::IMPORTER_KEY_LEVEL;
            case 'pu_profile' : return self::IMPORTER_KEY_PROFILE;
            case 'branch' : return self::IMPORTER_KEY_BRANCH_NAME;
            case 'ignoredfield' : return false;
        }

        return $code;
    }

    /**
     * Determines the chunk size, if there's a password in the map we reduce it(generating passwords is slow)
     * @param array $map the map array
     * @return integer
     */
    private static function getChunkSize(array $map)
    {
        $result = 100;
        foreach ($map as $mapItem) {
            if ($mapItem['code'] == self::IMPORTER_KEY_PASSWORD) {
                return 30;
            }
        }
        return $result;
    }

    /**
     * Parses the config and returns a CSV delimiter
     * @return string
     */
    private static function getDelimiter($params)
    {
        $sepName = isset($params['separator']) ? $params['separator'] : 'auto';
        $manualSep = isset($params['manualSeparator']) ? $params['manualSeparator'] : '';
        $sep = UserImportForm::translateSeparatorName($sepName, $manualSep);
        //in hydra autodetect is empty string (not the string "auto")
        return ($sep == 'auto') ? '' : $sep;
    }
}