<?php

class ErpApiClient {

    const _EXTRA_USERS_WARNING = 'extra_users_warning';
    const _EXTRA_USERS_WARNING_DOCEBO = 'extra_users_warning_docebo';
    const _EXTRA_USERS_REACHED_CUSTOMER = 'extra_users_reached_customer';
    const _EXTRA_USERS_REACHED_DOCEBO = 'extra_users_reached_docebo';

	// can't extend parent class ApiClient so we have to duplicate this..
	// solution will be available with php 5.3 using static:: instead of self::

	static public function getOptions($url, $data_params) {
		$http_header = self::getHeaders($data_params);

		$opt = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HTTPHEADER => $http_header,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data_params,
		);

		return $opt;
	}


	static public function apiGetHash($data_params, $api_key, $api_secret) {

		$res = array('sha1' => '', 'x_auth' => '');
		if (empty($api_key) || empty($api_secret)) {
			throw new Exception('Invalid key-secret');
		}

		$params = array();
		foreach($data_params as $k => $v) {
			if (substr($v, 0, 1) != '@') {
				$params[$k] = $v;
			}
		}

		$res['sha1'] = sha1(implode(',', $params) . ',' . $api_secret);

		$res['x_auth'] = base64_encode($api_key . ':' . $res['sha1']);

		return $res;
	}


	/**
	 * @static
	 * @param $url
	 * @param $data_params
	 * @return mixed|bool
	 */
	static public function execApiCall($url, $data_params) {
		$curl = curl_init();

		try {
			$options = self::getOptions($url, $data_params);
		} catch(Exception $e) {
			//Yii::log('Invalid key-secret', CLogger::LEVEL_ERROR);
			return '{success:"false"}';
		}
		curl_setopt_array($curl, $options);

		$output = curl_exec($curl);

		// it closes the session
		curl_close($curl);
		return $output;
	}


	// ---------------------------------------------------------------------------


	static public function getUrl($action) {
		$base_url = Yii::app()->params['erp_url'];

		//$url = 'http://127.0.0.1/docebo/erp/svn/trunk/www/index.php?r=erpApi/'.$action;
		//$url = 'http://erp.docebo.com/www/index.php?r=erpApi/'.$action;
		$url = $base_url . 'www/index.php?r=erpApi/' . $action; // DO NOT REMOVE the www/ !


		return $url;
	}


	public static function getInstallationId() {
		return Docebo::getErpInstallationId();
	}


	static public function getHeaders($data_params) {
		$erp_key 		= Settings::getCfg('erp_key','');
		$erp_secret_key = Settings::getCfg('erp_secret_key','');

		$hash_info = self::apiGetHash($data_params, $erp_key, $erp_secret_key);
		$http_header = array(
			"Content-Type: multipart/form-data",
			'X-Authorization: DoceboErp ' . $hash_info['x_auth'],
		);

		return $http_header;
	}


	/**
	 * Check if a custom domain is already in use (for another installation)
	 * @param $domain
	 * @return bool|mixed
	 */
	public static function apiCheckIfCustomDomainExists($domain) {
		$url = self::getUrl('checkIfCustomDomainExists');
		$output = self::execApiCall($url, array('domain' => $domain, 'installation_id' => self::getInstallationId()));

		return CJSON::decode($output);
	}


	static public function apiErpGetCustomerInfo($data_params) {

		$url = self::getUrl('getCustomerInfo');
		$output = self::execApiCall($url, $data_params);

		return $output;
	}


	static public function apiErpGetExtHelpdeskManager($data_params) {
		$url = self::getUrl('getExtHelpdeskManager');
		$output =self::execApiCall($url, $data_params);

		return $output;
	}


	static public function apiErpAddCrmContactHistory($data_params) {
		$url = self::getUrl('addCrmContactHistory');
		$output =self::execApiCall($url, $data_params);

		return $output;
	}


	static public function apiErpUpdateCrmCompanyInfo($data_params) {
		$url = self::getUrl('updateCrmCompanyInfo');
		$output =self::execApiCall($url, $data_params);

		return $output;
	}


	static public function apiErpTracking($data_params) {
		$url = self::getUrl('tracking');
		$output =self::execApiCall($url, $data_params);


		return $output;
	}


	/**
	 * @param $domain - parent domain
	 * @param $alias - alias name
	 * @param $server_name
	 * @return mixed
	 */
	public static function addPleskAlias($domain, $alias, $server_name) {
		$url = self::getUrl('addPleskAlias');
		$output = self::execApiCall($url, array(
			'domain' => $domain,
			'alias' => $alias,
			'server_name' => $server_name,
			'installation_id' => self::getInstallationId()
		));

		return CJSON::decode($output);
	}


	public static function removePleskAlias($domain) {
		$url = self::getUrl('removePleskAlias');
		$output = self::execApiCall($url, array('installation_id' => self::getInstallationId()));

		return CJSON::decode($output);
	}


	public static function removeCustomDomain() {
		$url = self::getUrl('removeCustomDomain');
		$output = self::execApiCall($url, array('installation_id' => self::getInstallationId()));

		return CJSON::decode($output);
	}


	public static function updateCustomDomain($custom_domain) {
		$url = self::getUrl('updateCustomDomain');
		$output = self::execApiCall($url, array('installation_id' => self::getInstallationId(), 'custom_domain' => $custom_domain));

		return CJSON::decode($output);
	}


	/**
	 * Retrieve all the informations about the extension of a trial from ERP.
	 * @param $code - is the code generated by ERP in order to extend a trial of an LMS installation
	 * @return mixed - result of the api
	 */

	public static function apiGetTrialExtensionInfo($code) {
		$url = self::getUrl('trialExtensionInfo');
		$output = self::execApiCall($url, array('installation_id' => self::getInstallationId(), 'code' => $code));
		return CJSON::decode($output);
	}


	/**
	 * Extend the trial period of an LMS and update the code history table on ERP
	 * with the date of consumption and set the code as consumed.
	 * @param $code
	 * @param $contact_id
	 * @param $language
	 * @return mixed
	 */
	public static function apiExtendTrialPeriod($code, $contact_id, $language) {
		$url = self::getUrl('extendTrialPeriod');
		$output = self::execApiCall($url, array(
			'installation_id' => self::getInstallationId(),
			'code' => $code,
			'contact_id' => $contact_id,
			'language' => $language));
		return CJSON::decode($output);
	}


	/**
	 * Get SAAS information from erp tables.
	 * @return mixed
	 */
	public static function getInstallationInfo() {
		$url = self::getUrl('getInstallationInfo');
		$output = self::execApiCall($url, array('installation_id' => self::getInstallationId()));
		return CJSON::decode($output);
	}


	/**
	 * This method saves the contact information from the post-install form
	 */
	static public function apiErpSetupSetCustomerInfo($data_params) {
		$url = self::getUrl('setCustomerInfoFromLms');
		$output =self::execApiCall($url, $data_params);

		return $output;
	}


	static public function getErpCountriesArr() {
		return array(
			'0'=>'-',
			'afghanistan'=>"Afghanistan",
			'albania'=>"Albania",
			'algeria'=>"Algeria",
			'andorra'=>"Andorra",
			'angola'=>"Angola",
			'antigua_and_barbuda'=>"Antigua and Barbuda",
			'argentina'=>"Argentina",
			'armenia'=>"Armenia",
			'australia'=>"Australia",
			'austria'=>"Austria",
			'azerbaijan'=>"Azerbaijan",
			'bahamas'=>"Bahamas",
			'bahrain'=>"Bahrain",
			'bangladesh'=>"Bangladesh",
			'barbados'=>"Barbados",
			'belarus'=>"Belarus",
			'belgium'=>"Belgium",
			'belize'=>"Belize",
			'benin'=>"Benin",
			'bhutan'=>"Bhutan",
			'bolivia'=>"Bolivia",
			'bosnia_and_herzegovina'=>"Bosnia and Herzegovina",
			'botswana'=>"Botswana",
			'brazil'=>"Brazil",
			'brunei'=>"Brunei",
			'bulgaria'=>"Bulgaria",
			'burkina_faso'=>"Burkina Faso",
			'burundi'=>"Burundi",
			'cambodia'=>"Cambodia",
			'cameroon'=>"Cameroon",
			'canada'=>"Canada",
			'cape_verde'=>"Cape Verde",
			'central_african_republic'=>"Central African Republic",
			'chad'=>"Chad",
			'chile'=>"Chile",
			'china'=>"China",
			'colombia'=>"Colombia",
			'comoros'=>"Comoros",
			'congo_brazzaville'=>"Congo (Brazzaville)",
			'congo'=>"Congo",
			'costa_rica'=>"Costa Rica",
			'cote_d_ivoire'=>"Cote d'Ivoire",
			'croatia'=>"Croatia",
			'cuba'=>"Cuba",
			'cyprus'=>"Cyprus",
			'czech_republic'=>"Czech Republic",
			'denmark'=>"Denmark",
			'djibouti'=>"Djibouti",
			'dominica'=>"Dominica",
			'dominican_republic'=>"Dominican Republic",
			'east_timor'=>"East Timor (Timor Timur)",
			'ecuador'=>"Ecuador",
			'egypt'=>"Egypt",
			'el_salvador'=>"El Salvador",
			'equatorial_guinea'=>"Equatorial Guinea",
			'eritrea'=>"Eritrea",
			'estonia'=>"Estonia",
			'ethiopia'=>"Ethiopia",
			'fiji'=>"Fiji",
			'finland'=>"Finland",
			'france'=>"France",
			'gabon'=>"Gabon",
			'gambia'=>"Gambia, The",
			'georgia'=>"Georgia",
			'germany'=>"Germany",
			'ghana'=>"Ghana",
			'greece'=>"Greece",
			'grenada'=>"Grenada",
			'guatemala'=>"Guatemala",
			'guinea'=>"Guinea",
			'guinea_bissau'=>"Guinea-Bissau",
			'guyana'=>"Guyana",
			'haiti'=>"Haiti",
			'honduras'=>"Honduras",
			'hungary'=>"Hungary",
			'iceland'=>"Iceland",
			'india'=>"India",
			'indonesia'=>"Indonesia",
			'iran'=>"Iran",
			'iraq'=>"Iraq",
			'ireland'=>"Ireland",
			'israel'=>"Israel",
			'italy'=>"Italy",
			'jamaica'=>"Jamaica",
			'japan'=>"Japan",
			'jordan'=>"Jordan",
			'kazakhstan'=>"Kazakhstan",
			'kenya'=>"Kenya",
			'kiribati'=>"Kiribati",
			'korea_north'=>"Korea, North",
			'korea_south'=>"Korea, South",
			'kuwait'=>"Kuwait",
			'kyrgyzstan'=>"Kyrgyzstan",
			'laos'=>"Laos",
			'latvia'=>"Latvia",
			'lebanon'=>"Lebanon",
			'lesotho'=>"Lesotho",
			'liberia'=>"Liberia",
			'libya'=>"Libya",
			'liechtenstein'=>"Liechtenstein",
			'lithuania'=>"Lithuania",
			'luxembourg'=>"Luxembourg",
			'macedonia'=>"Macedonia",
			'madagascar'=>"Madagascar",
			'malawi'=>"Malawi",
			'malaysia'=>"Malaysia",
			'maldives'=>"Maldives",
			'mali'=>"Mali",
			'malta'=>"Malta",
			'marshall_islands'=>"Marshall Islands",
			'mauritania'=>"Mauritania",
			'mauritius'=>"Mauritius",
			'mexico'=>"Mexico",
			'micronesia'=>"Micronesia",
			'moldova'=>"Moldova",
			'monaco'=>"Monaco",
			'mongolia'=>"Mongolia",
			'morocco'=>"Morocco",
			'mozambique'=>"Mozambique",
			'myanmar'=>"Myanmar",
			'namibia'=>"Namibia",
			'nauru'=>"Nauru",
			'nepa'=>"Nepa",
			'netherlands'=>"Netherlands",
			'new_zealand'=>"New Zealand",
			'nicaragua'=>"Nicaragua",
			'niger'=>"Niger",
			'nigeria'=>"Nigeria",
			'norway'=>"Norway",
			'oman'=>"Oman",
			'pakistan'=>"Pakistan",
			'palau'=>"Palau",
			'panama'=>"Panama",
			'papua_new_guinea'=>"Papua New Guinea",
			'paraguay'=>"Paraguay",
			'peru'=>"Peru",
			'philippines'=>"Philippines",
			'poland'=>"Poland",
			'portugal'=>"Portugal",
			'qatar'=>"Qatar",
			'romania'=>"Romania",
			'russia'=>"Russia",
			'rwanda'=>"Rwanda",
			'saint_kitts_and_nevis'=>"Saint Kitts and Nevis",
			'saint_lucia'=>"Saint Lucia",
			'saint_vincent'=>"Saint Vincent",
			'samoa'=>"Samoa",
			'san_marino'=>"San Marino",
			'sao_tome_and_principe'=>"Sao Tome and Principe",
			'saudi_arabia'=>"Saudi Arabia",
			'senegal'=>"Senegal",
			'serbia_and_montenegro'=>"Serbia and Montenegro",
			'seychelles'=>"Seychelles",
			'sierra_leone'=>"Sierra Leone",
			'singapore'=>"Singapore",
			'slovakia'=>"Slovakia",
			'slovenia'=>"Slovenia",
			'solomon_islands'=>"Solomon Islands",
			'somalia'=>"Somalia",
			'south_africa'=>"South Africa",
			'spain'=>"Spain",
			'sri_lanka'=>"Sri Lanka",
			'sudan'=>"Sudan",
			'suriname'=>"Suriname",
			'swaziland'=>"Swaziland",
			'sweden'=>"Sweden",
			'switzerland'=>"Switzerland",
			'syria'=>"Syria",
			'taiwan'=>"Taiwan",
			'tajikistan'=>"Tajikistan",
			'tanzania'=>"Tanzania",
			'thailand'=>"Thailand",
			'togo'=>"Togo",
			'tonga'=>"Tonga",
			'trinidad_and_tobago'=>"Trinidad and Tobago",
			'tunisia'=>"Tunisia",
			'turkey'=>"Turkey",
			'turkmenistan'=>"Turkmenistan",
			'tuvalu'=>"Tuvalu",
			'uganda'=>"Uganda",
			'ukraine'=>"Ukraine",
			'united_arab_emirates'=>"United Arab Emirates",
			'united_kingdom'=>"United Kingdom",
			'united_states'=>"United States",
			'uruguay'=>"Uruguay",
			'uzbekistan'=>"Uzbekistan",
			'vanuatu'=>"Vanuatu",
			'vatican_city'=>"Vatican City",
			'venezuela'=>"Venezuela",
			'vietnam'=>"Vietnam",
			'yemen'=>"Yemen",
			'zambia'=>"Zambia",
			'zimbabwe'=>"Zimbabwe",
		);
	}


	public static function getErpCrmCompanySizes()
	{
		return array(
			"1-5" => "1-5",
			"6-50" => "6-50",
			"51-250" => "51-250",
			"251-500" => "251-500",
			"501-2000" => "501-2000",
			"2001-5000" => "2001-5000",
			"5001-10.000" => "5001-10.000",
			"10.000" => "> 10.000"
		);
	}

	public static function getErpCrmIndustries()
	{
		return array(
			'agriculture' => Yii::t('setup', 'Agriculture'),
			'airlines_aviation_aerospace' => Yii::t('setup', 'Airlines/Aviation & Aerospace'),
			'arts' => Yii::t('setup', 'Arts'),
			'automotive' => Yii::t('setup', 'Automotive'),
			'banking_finance_insurance' => Yii::t('setup', 'Banking, Finance & Insurance'),
			'chemicals' => Yii::t('setup', 'Chemicals'),
			'construction_real_estate' => Yii::t('setup', 'Construction & Real Estate'),
			'consulting' => Yii::t('setup', 'Consulting'),
			'eLearning' => Yii::t('standard', '_ELEARNING'),
			'fashion_luxury' => Yii::t('setup', 'Fashion & Luxury'),
			'food_beverages' => Yii::t('setup', 'Food & Beverages'),
			'goods' => Yii::t('setup', 'Goods'),
			'government' => Yii::t('setup', 'Government'),
			'healthcare' => Yii::t('setup', 'Healthcare'),
			'hospitality_tourism' => Yii::t('setup', 'Hospitality & Tourism'),
			'human_resources' => Yii::t('setup', 'Human Resources'),
			'it' => Yii::t('setup', 'IT'),
			'legal' => Yii::t('setup', 'Legal'),
			'logistics_import_export' => 'Logistics, Import, Export',
			'manufacturing' => Yii::t('setup', 'Manufacturing'),
			'media_entertainment' => Yii::t('setup', 'Media Company and Entertainment'),
			'military_defense_space' => Yii::t('setup', 'Military, Defense & Space'),
			'non_profit' => Yii::t('setup', 'Non profit'),
			'oil_energy_mining' => Yii::t('setup', 'Oil, Energy & Mining'),
			'pharma_biotech_nanotech' => Yii::t('setup', 'Pharma, Biotech & Nanotech'),
			'political_organization' => Yii::t('setup', 'Political Organization'),
			'recreation' => Yii::t('setup', 'Recreation'),
			'renewables_environment' => Yii::t('setup', 'Renewables & Environment'),
			'retail' => Yii::t('setup', 'Retail'),
			'school_education' => Yii::t('setup', 'School & Education'),
			'services' => Yii::t('setup', 'Services'),
			'software_internet' => Yii::t('setup', 'Software/Internet'),
			'telecommunications' => Yii::t('setup', 'Telecommunications'),
			'training' => Yii::t('setup', 'Training'),
			'transportation' => Yii::t('setup', 'Transportation'),
			'utilities' => Yii::t('setup', 'Utilities'),
			'other' => Yii::t('setup', 'Other')
		);
	}

	/**
	 * Send and set Subscription options of THIS LMS into ERP
	 *
	 * 			$data_params = array(
	 *					'installation_id' => #,
	 *					'status' => '',
	 *					'guwid' => null,
	 *					'ccard_expire_date' => ,
	 *					'ccard_last_four' => ,
	 *					'new_option' =>
	 *			);
	 *
	 *
	 * @param array $data_params
	 * @return mixed
	 */
	static public function apiErpSetSaasSubscriptionOptions($data_params) {
		$url = self::getUrl('setSaasSubscriptionOptions');
		$output =self::execApiCall($url, $data_params);
		return $output;
	}


	/**
	 * Get this LMS order information from ERP
	 *
	 * 			$data_params = array('installation_id' => #);
	 *
	 * @param array $data_params
	 * @return mixed
	 */
	static public function apiErpGetSaasOrderInfo($data_params) {
		$url = self::getUrl('getSaasOrderInfo');
		$output =self::execApiCall($url, $data_params);
		return $output;
	}


	/**
	 * Call ERP API and get billing information
	 *
	 * 			$data_params = array(
	 * 				'installation_id' => ###,
	 * 				'current_installation_only' => true/false
	 * 			);
	 *
	 * @param array $data_params
	 * @return mixed
	 */
	static public function apiErpGetBilling($data_params) {

		$url = self::getUrl('getBilling');
		$output =self::execApiCall($url, $data_params);

		return $output;
	}




	/**
	 * Call ERP API and get combined information information about LMS.
	 * This is to avoid multiple API calls.
	 *
	 * Currently:
	 *  -- LMS information
	 *  -- Billing information
	 *
	 * 			$data_params = array(
	 * 				'installation_id' => ###,
	 * 				'current_installation_only' => true/false
	 * 			);
	 *
	 * @param array $data_params
	 * @return mixed
	 */
	static public function apiErpGetComboInfo($data_params) {
		$url = self::getUrl('getComboInfo');
		$output =self::execApiCall($url, $data_params);
		return $output;
	}


	/**
	 * Call ERP API and get document PDF directly available to be sent to the user
	 *
	 *		$data_params = array(
	 *			'installation_id' => ###,
	 *			'invoice_id' => ###,
	 *			'type' => invoice | proforma | creditnote
	 *		);
	 *
	 *
	 *
	 * @param unknown $data_params
	 * @return Ambigous <mixed, boolean>
	 */
	static public function apiErpGetInvoice($data_params) {
		$url = self::getUrl('getInvoice');
		$output =self::execApiCall($url, $data_params);
		return $output;
	}

	/**
	 * Check if the subscription is with pending payment/renewal process in order to stop some actions during this period
	 * @param $data_params
	 * @return bool|mixed
	 */
	public static function getIsPendingRenewal($data_params)
	{
		$url = self::getUrl('getIsPendingRenewal');
		$output =self::execApiCall($url, $data_params);
		return $output;
	}

	/**
	 * Set value of some backup setting param
	 * @param $data_params
	 * @return bool|mixed
	 */
	public static function setBackupSetting($param, $value)
	{
		$url = self::getUrl('setBackupSetting');
		$output =self::execApiCall($url, array('installation_id' => self::getInstallationId(), 'param' => $param, 'value' => $value));
		return $output;
	}

	/**
	 * Get the value of some backup setting param
	 * @param $data_params
	 * @return bool|mixed
	 */
	public static function getBackupSetting($param)
	{
		$url = self::getUrl('getBackupSetting');
		$output =self::execApiCall($url, array('installation_id' => self::getInstallationId(), 'param' => $param));
		return $output;
	}

    /**
     * Send email by code
     * @param $data_params
     * @return bool|mixed
     */
    public static function sendEmailByCode($data_params)
    {
        $url = self::getUrl('sendEmailByCode');
		$output =self::execApiCall($url, array_merge($data_params, array('installation_id' => self::getInstallationId())));
		return $output;
    }

    /**
     * Renew in wire transfer
     * @return bool|mixed
     */
    public static function wireRenewOrder()
    {
        $url = self::getUrl('wireRenewOrder');
		$output =self::execApiCall($url, array('installation_id' => self::getInstallationId()));
		return $output;
    }

	public static function setInstallationParams($params)
	{
		$url = self::getUrl('setInstallationParams');
		$output =self::execApiCall($url, array_merge($params, array('installation_id' => self::getInstallationId())));
		return $output;
	}

}
?>
