<?php

class Dashboard extends CApplicationComponent {

	private $_blocksPositionName = 'blocksPosition';

	public function init() {
		$this->initJs();
	}

	protected function initBlocksPosition() {
		$arr = array(
			'top-left' => array('generateReports'),
			'top-right' => array('alerts'),
			'bottom-left' => array('statuses'),
			'bottom-right' => array('topBestContributors', 'topMostWatchedAssets', 'popularCourses', 'leastCompletedCourses', 'mostCompletedCourses')
		);

		if(Yii::app()->user->getIsGodadmin()
			|| (
					Yii::app()->user->getIsPu()
					&& (Yii::app()->user->checkAccess('/lms/admin/course/moderate') || Yii::app()->user->checkAccess('/lms/admin/course/assign'))
				)){
			$arr['bottom-left'][] = 'coursesWithWaitList';
		}

		return $arr;
	}

	protected function initJs() {
		$assetsPath = Yii::app()->getAssetManager()->publish(realpath(Yii::getPathOfAlias("admin.js")));

		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		Yii::app()->clientScript->registerScriptFile($assetsPath . '/dashboard.js');
		return true;
	}

	protected function setBlocksPosition(array $data) {
		$cookie = new CHttpCookie($this->_blocksPositionName, CJSON::encode($data));
		$cookie->expire = time()+60*60*24*30; // 30 days
		$cookie->domain = Yii::app()->request->serverName;
		$cookie->httpOnly = true;

		Yii::app()->request->cookies[$this->_blocksPositionName] = $cookie;
	}

	protected function clearBlocksPosition() {
		unset(Yii::app()->request->cookies[$this->_blocksPositionName]);
	}

	public function getStats() {
		return Yii::app()->statsHelper->getDashboardStats();
	}

	public function getCoachShareStats() {
		return Yii::app()->statsHelper->getDashboardCoachShareStats();
	}

	public function saveBlocksPosition($blocks) {
		$this->setBlocksPosition($blocks);
	}

	public function resetBlocksPosition() {
		$this->clearBlocksPosition();
	}

	public function getBlocksPosition() {
		if (isset(Yii::app()->request->cookies[$this->_blocksPositionName])) {
			return CJSON::decode(Yii::app()->request->cookies[$this->_blocksPositionName]);
		} else {
			return $this->initBlocksPosition();
		}
	}
}