<?php

class DoceboHtml extends CHtml {

    /**
     * Displays only the first $limit characters of a text, followed by an ellipsis.
     *
     * @param $text
     * @param $limit
     * @return string
     */
    public static function readmore($text, $limit) {
        if ( strlen($text) > $limit ) {
            $html = '';

            $truncated = trim(substr($text, 0, $limit)) . '&hellip; ';
            $truncated .= self::tag('a', array('href'=>'#', 'class'=>'readmore'), '[+]');
            $html .= self::tag('span', array('class'=>'readless-container'), $truncated);

            $expanded = $text . ' ' . self::tag('a', array('href'=>'#', 'class'=>'readless'), '[-]');
            $html .= self::tag('span', array('class'=>'readmore-container', 'style'=>'display:none;'), $expanded);

            return self::tag('span', array('class'=>'readmore-element'), $html);
        }
        return $text;
    }

} 