<?php
/**
 * An Asset Manager to save Yii assets to a shared S3 location, where all platforms (same LMS version) can use them.
 *
 */
class DoceboAssetManagerS3 extends DoceboAssetManager {
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see DoceboAssetManager::init()
	 */
	public function init() {
		parent::init();
		// MUST be done! To avoid infinite chain of asset manager creation
		// Clearing this up will make the parent class to execute normal publishing, if called
		self::$sharedAssetPaths = array();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 * @see DoceboAssetManager::publish()
	 */
	public function publish($path,$hashByName=false,$level=-1,$forceCopy=null) {
		// For now, just publish assets using the parent way (DoceboAssetManager)
		return parent::publish($path,$hashByName,$level,$forceCopy);			
	}
}