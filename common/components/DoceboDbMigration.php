<?php
/**
 * Lets have this class in case we need to improve the functionality 
 *
 */
class DoceboDbMigration extends CDbMigration {
	
	const LOG_CATEGORY = 'migration';
	const MESSAGE_FAILED = 'Migration FAILED';
	
	/**
	 * Constraint TYPE names
	 * @var string
	 */
	const CT_FOREIGN_KEY = 'FOREIGN KEY';
	const CT_PRIMARY_KEY = 'PRIMARY KEY';
	const CT_UNIQUE      = 'UNIQUE';

	/**
	 * Constructor 
	 * @param string $params
	 */
	public function __construct($params=false) {
	}

	/**
	 * 
	 * {@inheritDoc}
	 * @see CDbMigration::up()
	 */
	public function up() {
		Yii::log('>>>> Migration UP STARTED (' . get_class($this) . ')', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
		
		$transaction=$this->getDbConnection()->beginTransaction();
		try
		{
			if($this->safeUp()===false) {
				$transaction->rollback();
				return false;
			}
			$transaction->commit();
			Yii::log('<<<< Migration FINISHED (' .  get_class($this) . ')', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
		}
		catch(Exception $e)
		{
			$this->failedOutput($e);
			$transaction->rollback();
			return false;
		}
	}
	

	
	/**
	 *
	 * {@inheritDoc}
	 * @see CDbMigration::up()
	 */
	public function down() {
		Yii::log('>>>> Migration DOWN STARTED (' . get_class($this) . ')', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
	
		$transaction=$this->getDbConnection()->beginTransaction();
		try
		{
			if($this->safeDown()===false) {
				$transaction->rollback();
				return false;
			}
			$transaction->commit();
			Yii::log('<<<< Migration FINISHED (' .  get_class($this) . ')', CLogger::LEVEL_INFO, self::LOG_CATEGORY);
		}
		catch(Exception $e)
		{
			$this->failedOutput($e);
			$transaction->rollback();
			return false;
		}
	}
	
	/**
	 * On-screen and Log for a failing migration
	 * 
	 * @param Exception $e
	 */
	private function failedOutput(Exception $e) {
		$output1  = self::MESSAGE_FAILED . " (" . get_class($this) . ")\n";
		$output2  = $e->getMessage()." \n(".$e->getFile().':'.$e->getLine().")\n";
		$output2 .= $e->getTraceAsString()."\n";
			
		echo "\n" . $output1 . $output2 . "\n";
			
		Yii::log($output2, CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
		Yii::log("<<<< " . self::MESSAGE_FAILED . " (" . get_class($this) . ")", CLogger::LEVEL_ERROR, self::LOG_CATEGORY);
	}

	/**
	 * Check is there already a setting with the param_name
	 *
	 * @param $paramName
	 * @return bool - true if it already exists, and false if it doesn't exist
	 */
	public function settingParamExists($paramName) {
		$dbCommand = Yii::app()->db->createCommand();
		$dbCommand->select("count(*)");
		$dbCommand->from('core_setting cs');
		$dbCommand->where('cs.param_name = :param_name', array(':param_name' => $paramName));
		$found = $dbCommand->queryScalar();

		return (($found != 0) ? true : false);
	}
	

	/**
	 * Check if a constraint exists in currently connected and used database.
	 * Note please, this can be a slow operation. Better narrow down the result by specifying the table name as well.
	 * 
	 * @param string $name  Constraint name to check
	 * @param string $table (optionsl) Table name
	 * @param string $type (optional)  Constraint type (see self constants)
	 */
	public static function constraintExists($name, $table=false, $type=false) {

	    // Let user search case insensitive
	    $name = strtolower($name);
	    $command = Yii::app()->db->createCommand();
	    $command->select('LOWER(CONSTRAINT_NAME)');
	    $command->from('information_schema.TABLE_CONSTRAINTS c');
	    $command->where("TABLE_SCHEMA = DATABASE() AND LOWER(CONSTRAINT_NAME)=LOWER('$name')");
	    // Filter by constraint type ?
	    if (is_string($type)) {
	        $command->andWhere("CONSTRAINT_TYPE='$type'");
	    }
	    // Filter by table name (recommended) ?
	    if (is_string($table)) {
	        $table = strtolower($table);
	        $command->andWhere("LOWER(TABLE_NAME)=LOWER('$table')");
	    }
	    // Get all results and just return if they are more than 0
	    $result = $command->queryAll();
	    return count($result) > 0;
	}
	
	
	/**
	 * Get list of all table constraints. 
	 * Useful when you want to constraints cleanup.
	 * 
	 * @param string $table
	 */
	public static function getTableConstraints($table) {
	    $table     = strtolower($table);
	    $command   = Yii::app()->db->createCommand();
	    $command->select('CONSTRAINT_NAME AS name, CONSTRAINT_TYPE as type');
	    $command->from('information_schema.TABLE_CONSTRAINTS c');
	    $command->where("TABLE_SCHEMA = DATABASE()");
	    $command->andWhere("LOWER(TABLE_NAME)=LOWER('$table')");
	    $result    = $command->queryAll();
	    return $result;
	}
	
	/**
	 * Return list of Foreign Key references to a given table->column
	 *  
	 * @param string $table
	 * @param string $column
	 */
	public static function getColumnReferences($table, $column) {
		$table     = strtolower($table);
		$column    = strtolower($column);
		$params = array(
			":table"	=> $table,
			":column"	=> $column,	
		);
		$command   = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from('information_schema.KEY_COLUMN_USAGE kcu');
		$command->where("kcu.TABLE_SCHEMA = DATABASE()");
		$command->andWhere("LOWER(kcu.REFERENCED_TABLE_NAME)=:table");
		$command->andWhere("LOWER(kcu.REFERENCED_COLUMN_NAME)=:column");
		
		$result    = $command->queryAll(true, $params);
		
		return $result;
	}
	
	/**
	 * Return list of defined Table->Column foreign keys. Filter by referenced table and/or column.
	 *
	 * @param string $table
	 * @param string $column
	 * @param string $refTable
	 * @param string $refColumn
	 */
	public static function getColumnForeignKeys($table, $column, $refTable=false, $refColumn=false, $fkName=false) {
	    return self::getTableForeignKeys($table, $column, $refTable, $refColumn, $fkName);
	}
	
	
    /**
     * 
     * {@inheritDoc}
     * @see CDbMigration::createTable()
     */
	public function createTable ( $table, $columns, $options = NULL ) {
		if($options===null && $this->dbConnection->getDriverName()=== 'mysql'){
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		parent::createTable( $table, $columns, $options );
	}
	
	/**
	 * Return list of all FK defined in a table, optionaly filtered by column, referenced table and referenced column
	 * 
	 * @param string $table
	 * @param string $column
	 * @param string $refTable
	 * @param string $refColumn
	 */
	public static function getTableForeignKeys($table, $column=false, $refTable=false, $refColumn=false, $fkName=false) {
	    
	    $table     = strtolower($table);
	    
	    $params = array(
	        ":table"	=> $table,
	    );
	    $command   = Yii::app()->db->createCommand();
	    $command->select('*');
	    $command->from('information_schema.KEY_COLUMN_USAGE kcu');
	    $command->where("kcu.TABLE_SCHEMA = DATABASE()");
	    $command->andWhere("LOWER(kcu.TABLE_NAME)=:table");
	    $command->andWhere("kcu.REFERENCED_TABLE_NAME IS NOT NULL");
	    $command->andWhere("kcu.REFERENCED_COLUMN_NAME IS NOT NULL ");

	    // Apply filtering
	    
	    if ($column !== false) {
	        $column    = strtolower($column);
	        $command->andWhere("LOWER(kcu.COLUMN_NAME)=:column");
	        $params[':column'] = $column;
	    }
	    
	    if ($refTable !== false) {
	        $command->andWhere("kcu.REFERENCED_TABLE_NAME=:refTable");
	        $params[':refTable'] = $refTable;
	    }
	    
	    if ($refColumn!== false) {
	        $command->andWhere("kcu.REFERENCED_COLUMN_NAME=:refColumn");
	        $params[':refColumn'] = $refColumn;;
	    }
	    
	    if ($fkName!== false) {
	        $command->andWhere("kcu.CONSTRAINT_NAME=:fkName");
	        $params[':fkName'] = $fkName;;
	    }
	     
	    
	    $result    = $command->queryAll(true, $params);
	    
	    return $result;
	     
	}
	
	
	/**
	 * Get list of defined table specific indexes
	 * 
	 * @param string $table
	 * @param string $column (Optional)
	 * @param string $indexName (Optional)
	 * @param string $skipPrimary (Optional)  If set TRUE (default), skip PRIMARY index 
	 */
	public static function getTableIndexes($table, $column=false, $indexName=false, $skipPrimary=true) {

		$table     = strtolower($table);
		 
		$params = array(
				":table"	=> $table,
		);
		$command   = Yii::app()->db->createCommand();
		$command->select('*');
		$command->from('information_schema.STATISTICS t');
		$command->where("t.TABLE_SCHEMA = DATABASE()");
		$command->andWhere("LOWER(t.TABLE_NAME)=:table");
		
		// Apply filtering
		if ($column !== false) {
			$column    = strtolower($column);
			$command->andWhere("LOWER(t.COLUMN_NAME)=:column");
			$params[':column'] = $column;
		}
		 
		if ($indexName!== false) {
			$command->andWhere("t.INDEX_NAME=:indexName");
			$params[':indexName'] = $indexName;;
		}
		
		if ($skipPrimary) {
			$command->andWhere("t.INDEX_NAME <> 'PRIMARY'");
		}
		 
		$result    = $command->queryAll(true, $params);
		 
		return $result;
		
	}
	
	
	/**
	 * Drop list of foreign keys as defined according to passed parameters.
	 * 
	 * @param string $table
	 * @param string $column  (Optional)
	 * @param string $refTable (Optional)
	 * @param string $refColumn (Optional)
	 * @param string $fkName (Optional) If set, only delete FK having that exact name
	 * 
	 * @return number Number of droppped FKs
	 */
	public function dropForeignKeysByDefinition($table, $column=false, $refTable=false, $refColumn=false, $fkName=false) {

	    // Get list of defined FKs for this table
	    $fks = self::getTableForeignKeys($table, $column, $refTable, $refColumn, $fkName);

	    // If any FK is defined
	    $count = 0;
	    if (is_array($fks) && !empty($fks)) {
	        foreach ($fks as $fk) {
                $this->dropForeignKey($fk['CONSTRAINT_NAME'], $table);
                $count++;
	        }
	    }

	    return $count;

	}

    /**
     * @param string $name
     * @param string $table
     * @deprecated
     * @see dropForeignKeysByDefinition - Dropping a definition by name is not recommended (names are usually autogenerated)
     */
	public function dropForeignKey($name, $table)
    {
        parent::dropForeignKey($name, $table);
    }


    /**
	 * Drop list of indexes as defined according to passed parameters.
	 *
	 * @param string $table
	 * @param string $column  (Optional)
	 * @param string $indexName (Optional) If set, only delete index having that exact name
	 * @param string $skipPrimary (Optional)  If set TRUE (default), skip PRIMARY index 
	 *
	 * @return number Number of droppped FKs
	 */
	public static function dropIndexesByDefinition($table, $column=false, $indexName=false, $skipPrimary=true) {
		 
		// Get list of defined indexes/keys for this table
		$keys = self::getTableIndexes($table, $column, $indexName, $skipPrimary);
		 
		// If any key is defined
		$count = 0;
		if (is_array($keys) && !empty($keys)) {
			$m = new DoceboDbMigration();
			foreach ($keys as $key) {
				$m->dropIndex($key['INDEX_NAME'], $table);
				$count++;
			}
		}
		 
		return $count;
		 
	}
	

}