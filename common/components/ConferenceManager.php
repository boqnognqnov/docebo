<?php

/**
 * Video Conferencing manager component.
 * Create, Update, Delete conferencing rooms of various types
 * calling their respective API clients.
 *
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 */
class ConferenceManager extends CComponent
{

	// Conference systems codes, used in core_settings (<code>_<parameter>) and to identify the system
	const CONFTYPE_MEETECHO = 'meetecho';
	const CONFTYPE_ADOBECONNECT = 'adobeconnect';
	const CONFTYPE_BIGBLUEBUTTON = 'bigbluebutton';
	const CONFTYPE_GOTOMEETING = 'gotomeeting';
	const CONFTYPE_ONSYNC = 'onsync';
	const CONFTYPE_TELESKILL = 'teleskill';
	const CONFTYPE_WEBEX = 'webex';
	const CONFTYPE_SKYMEETING = 'skymeeting';
	const CONFTYPE_VIAAPP = 'viaapp';

	// Some hardcoded parameters
	const CONFERENCE_DEFAULT_MAXP = 30;    // max participants per room
	const CONFERENCE_DEFAULT_SYSTEM = self::CONFTYPE_ADOBECONNECT;


	// Reasons for not able to join a meeting
	public static $NOJOIN_ROOM_FULL = "Room full";
	public static $NOJOIN_NOT_SUBSCRIBED = "Not subscribed";
	public static $NOJOIN_SYSTEM_NOT_ENABLED = "System not enabled";
	public static $NOJOIN_OUTSIDE_TIMEFRAME = "Meeting outside timeframe";
	public static $NOJOIN_CANNOT_COUNT_PARTICIPANTS = "Can not count participants";

	public static $MSG_MEETING_CANT_BE_SAVED_LOCALLY = "Meeting can not be saved locally";
	public static $MSG_ROOM_CANT_BE_SAVED_LOCALLY = "Room can not be saved locally";
	public static $MSG_UNKNOWN_CONFERENCING_TYPE = "Unknown conference type";
	public static $MSG_INVALID_DATA_RETURNED = "Invalid data returned";
	public static $MSG_CONFERENCE_CREATION_FAILED = "Conference creation failed";
	public static $MSG_FAILED_TO_CONTACT_SERVER = "Failed to contact the server";


	// Last error message; to be used from component caller
	public $lastError = "";

	// Hardcoded [code] => [App/Plugin internal name] map
	public static $appMap = array(
		self::CONFTYPE_MEETECHO => 'DoceboWebconferenceApp',
		self::CONFTYPE_ADOBECONNECT => 'AdobeConnectApp',
		self::CONFTYPE_BIGBLUEBUTTON => 'BigBlueButtonApp',
		self::CONFTYPE_GOTOMEETING => 'GotomeetingApp',
		self::CONFTYPE_ONSYNC => 'OnsyncApp',
		self::CONFTYPE_TELESKILL => 'TeleskillApp',
		self::CONFTYPE_WEBEX => 'WebexApp',
		self::CONFTYPE_SKYMEETING => 'SkymeetingApp',
		self::CONFTYPE_VIAAPP => 'ViaApp',
	);


	// If BBB attendees can join even if there is still no host (meeting is not running)
	const BBB_ALLOW_ATTENDEE_JOIN_NOT_RUNNIN_MEETING = TRUE;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->init();
	}


	/**
	 * Init
	 *
	 */
	public function init()
	{
		// Translate messages
		self::$NOJOIN_ROOM_FULL = Yii::t('conference', 'Meeting room is full. Maximum participants number reached.');
		self::$NOJOIN_NOT_SUBSCRIBED = Yii::t('standard', 'You must be subscribed to this course.');
		self::$NOJOIN_SYSTEM_NOT_ENABLED = Yii::t('conference', 'Room type not allowed.');
		self::$NOJOIN_OUTSIDE_TIMEFRAME = Yii::t('conference', 'Meeting has not started yet.');
		self::$NOJOIN_CANNOT_COUNT_PARTICIPANTS = Yii::t('conference', 'Invalid room');

		self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY = Yii::t('conference', 'Meeting created at server but failed to save its data into local table.');
		self::$MSG_ROOM_CANT_BE_SAVED_LOCALLY = Yii::t('conference', 'Unable to save room data into local table.');
		self::$MSG_UNKNOWN_CONFERENCING_TYPE = Yii::t('conference', 'Invalid room');
		self::$MSG_INVALID_DATA_RETURNED = Yii::t('standard', '_OPERATION_FAILURE');
		self::$MSG_CONFERENCE_CREATION_FAILED = Yii::t('standard', '_OPERATION_FAILURE');
		self::$MSG_FAILED_TO_CONTACT_SERVER = Yii::t('standard', 'internal error - contact us');
	}


	/**
	 * Create New Room locally in LMS table && room/meeting remotely (on server)
	 *
	 * @param string $roomType
	 * @param string $roomName
	 * @param text $roomDescription
	 * @param int $idCourse
	 * @param int $userIdst
	 * @param string $startDateTime
	 * @param int $durationHours
	 * @param int $durationMinutes
	 * @param int $maxParticipants
	 * @param array $params
	 * @return boolean
	 */
	public function createRoom($roomType, $roomName, $roomDescription, $idCourse, $userIdst, $startDateTime, $durationHours, $durationMinutes, $maxParticipants, $params = array())
	{

		// Convert everything in Unix Timestamps;
		$startUT = strtotime($startDateTime);
		$durationSeconds = $durationHours * 3600 + $durationMinutes * 60;
		$endUT = $startUT + $durationSeconds;

		// Create remote room/meeting first
		$ok = FALSE;
		switch ($roomType) {
			case self::CONFTYPE_MEETECHO:
				$conferenceMode = (!empty($params['conference_mode'])) ? $params['conference_mode'] : 'flash';
				$meetechoDuration = $durationHours * 60 + $durationMinutes;
				// one or more elements that will be made available during the conference
				$media = array();
				$meeting = $this->createMeetechoMeeting($conferenceMode, $roomName, $startDateTime, $meetechoDuration, $media);
				if ($meeting !== FALSE && is_array($meeting)) {
					$ok = TRUE;
					$remoteRoomModel = new ConferenceMeetechoMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->attributes = $meeting;
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_ADOBECONNECT:
				$accountId = isset($params['accountId']) ? $params['accountId'] : false;
				$meeting = $this->createAdobeConnectMeeting($roomName, $idCourse, $userIdst, $startUT, $endUT, $accountId);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceAdobeconnectMeeting();
					$remoteRoomModel->attributes = $meeting;
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_BIGBLUEBUTTON:
				$meeting = $this->createBigBlueButtonMeeting($roomName, $idCourse, $userIdst, $startUT, $endUT);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceBigbluebuttonMeeting();
					$remoteRoomModel->attributes = $meeting;
					$remoteRoomModel->name = $roomName;
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_GOTOMEETING:
				$meeting = $this->createGotomeetingMeeting($roomName, $startDateTime, $durationSeconds);
				if ($meeting !== FALSE && is_array($meeting)) {
					$ok = TRUE;
					$remoteRoomModel = new ConferenceGotomeetingMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->attributes = $meeting;
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_ONSYNC:
				$meeting = $this->createOnsyncMeeting($roomName, $startDateTime, $durationSeconds);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceOnsyncMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->name = $roomName;
					$remoteRoomModel->session_id = $meeting['session_id'];
					$remoteRoomModel->friendly_id = $meeting['friendly_id'];
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;


			case self::CONFTYPE_TELESKILL:
				$meeting = $this->createTeleskillMeeting($roomName, $startDateTime, $durationSeconds, $maxParticipants);
				if ($meeting !== FALSE && is_array($meeting)) {
					$ok = TRUE;
					$remoteRoomModel = new ConferenceTeleskillMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->attributes = $meeting;
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;


			case self::CONFTYPE_WEBEX:
				$webexType = 'meeting';
				$allowedWebexTypes = array('meeting', 'event');

				if (isset($params['webex_type']) && in_array($params['webex_type'], $allowedWebexTypes)) {
					$webexType = $params['webex_type'];
				}

				$meeting = $this->createWebexMeeting($roomName, $startDateTime, $durationSeconds, $webexType);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceWebexMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->attributes = $meeting;
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_SKYMEETING:
				$meeting = $this->createSkymeetingMeeting($roomName, $startDateTime, $durationSeconds);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceSkyMeetingMeeting();
					$remoteRoomModel->id_room = 0;  // WILL BE UPDATED A BIT LATER!!!!
					$remoteRoomModel->name = $roomName;
					$remoteRoomModel->session_id = $meeting['session_id'];
					$remoteRoomModel->friendly_id = $meeting['friendly_id'];
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			case self::CONFTYPE_VIAAPP:
				$meeting = $this->createViaappMeeting($roomName, $startDateTime, $durationSeconds);
				if ($meeting !== FALSE && is_array($meeting)) {
					$remoteRoomModel = new ConferenceViaappMeeting();
					$remoteRoomModel->title = $roomName;
					$remoteRoomModel->activityId = $meeting['activityId'];
					$remoteRoomModel->hyperlink = $meeting['hyperlink'];
					$ok = $remoteRoomModel->save();
					if (!$ok) {
						$this->lastError = self::$MSG_MEETING_CANT_BE_SAVED_LOCALLY;
						$ok = FALSE;
					}
				} else {
					$ok = FALSE;
				}
				break;

			default:
				$this->lastError = self::$MSG_UNKNOWN_CONFERENCING_TYPE;
				$ok = FALSE;
				break;
		}

		// Could not create room/meeting on the remote server
		if (!$ok) {
			if (!$this->lastError)
				$this->lastError = self::$MSG_CONFERENCE_CREATION_FAILED;

			return FALSE;
		}

		// So far so good, now lets save the room locally, using 'common room data', not specific for any conf. system
		// We do this in this order, so we do not create local room/meeting and THEN create remotely, which MAY fail.
		// This way we keep our DB out of cluttering.

		//@todo Create calendar, get Id
		$calId = 0;


		$model = new ConferenceRoom();
		$model->idCal = $calId;
		$model->idCourse = $idCourse;
		$model->idSt = $userIdst;
		$model->name = $roomName;
		$model->description = $roomDescription;
		$model->room_type = $roomType;
		$model->starttime = $startUT;
		$model->endtime = $endUT;
		$model->meetinghours = $durationHours;
		$model->meetingminutes = $durationMinutes;
		$model->maxparticipants = $maxParticipants;

		if ($accountId)
			$model->accountId = $accountId;

		if ($model->save()) {
			// Save local room id to remote room id_room (common field for all conference systems!!)
			$remoteRoomModel->id_room = $model->id;
			$remoteRoomModel->save();
			$result = TRUE;
		} else {
			$this->lastError = self::$MSG_ROOM_CANT_BE_SAVED_LOCALLY;
			$remoteRoomModel->delete();
			$result = FALSE;
		}

		return $result;

	}


	/**
	 * Update a room
	 *
	 * @param int $id
	 * @param string $roomType
	 * @param string $roomName
	 * @param string $roomDescription
	 * @param int $idCourse
	 * @param int $userIdst
	 * @param string $startDateTime
	 * @param int $durationHours
	 * @param int $durationMinutes
	 * @param int $maxParticipants
	 * @param array $params
	 * @return boolean
	 */
	public function updateRoom($id, $roomType, $roomName, $roomDescription, $idCourse, $userIdst, $startDateTime, $durationHours, $durationMinutes, $maxParticipants, $params = array())
	{

		// Convert everything in Unix Timestamps;
		$startUT = strtotime($startDateTime);
		$durationSeconds = $durationHours * 3600 + $durationMinutes * 60;
		$endUT = $startUT + $durationSeconds;

		$ok = $roomType;

		switch ($roomType) {
			case ConferenceManager::CONFTYPE_MEETECHO:
				$ok = $this->updateMeetechoMeeting($id, $roomName, $idCourse, $startUT, $endUT);
				break;

			case ConferenceManager::CONFTYPE_ADOBECONNECT:
				$accountId = isset($params['accountId']) ? $params['accountId'] : false;
				$ok = $this->updateAdobeConnectMeeting($id, $roomName, $idCourse, $startUT, $endUT, $accountId);
				break;

			case ConferenceManager::CONFTYPE_BIGBLUEBUTTON:
				$ok = $this->updateBigBlueButtonMeeting($id, $roomName, $idCourse, $startUT, $endUT);
				break;

			case ConferenceManager::CONFTYPE_ONSYNC:
				$ok = $this->updateOnsyncMeeting($id, $roomName, $startDateTime, $durationSeconds);
				break;

			case ConferenceManager::CONFTYPE_GOTOMEETING:
				$ok = $this->updateGotomeetingMeeting($id, $roomName, $startDateTime, $durationSeconds);
				break;

			case ConferenceManager::CONFTYPE_TELESKILL:
				$ok = $this->updateTeleskillMeeting($id, $roomName, $startDateTime, $durationSeconds, $maxParticipants);
				break;

			case ConferenceManager::CONFTYPE_WEBEX:
				$webexType = 'meeting';
				$allowedWebexTypes = array('meeting', 'event');

				if (isset($params['webex_type']) && in_array($params['webex_type'], $allowedWebexTypes)) {
					$webexType = $params['webex_type'];
				}

				$ok = $this->updateWebexMeeting($id, $roomName, $startDateTime, $durationSeconds, $webexType);
				break;

			case ConferenceManager::CONFTYPE_SKYMEETING:
				$ok = $this->updateSkymeetingMeeting($id, $roomName, $startDateTime, $durationSeconds);
				break;

			case ConferenceManager::CONFTYPE_VIAAPP:
				$ok = $this->updateViaappMeeting($roomName, $roomDescription, $startDateTime, $durationSeconds / 60, $id);

			default:
				break;
		}


		if (!$ok) {
			return FALSE;
		}

		$roomModel = ConferenceRoom::model()->findByPk($id);
		$roomModel->name = $roomName;
		$roomModel->description = $roomDescription;
		$roomModel->starttime = $startUT;
		$roomModel->endtime = $endUT;
		$roomModel->meetinghours = $durationHours;
		$roomModel->meetingminutes = $durationMinutes;
		$roomModel->maxparticipants = $maxParticipants;
		$roomModel->save();

		$result = TRUE;

		return $result;


	}


	/**
	 * Delete Room by Id (Pk)
	 *
	 * @param int $id
	 * @return bool
	 */
	public function deleteRoom($id)
	{

		$roomModel = ConferenceRoom::model()->findByPk($id);

		try {

			// Check what is the room type and do action on meeting
			if ($roomModel->room_type == self::CONFTYPE_ADOBECONNECT) {
				$sco_id = $roomModel->adobeConnectMeetingData->sco_id;

				// Delete meeting from local AC meetings table
				if ($roomModel->adobeConnectMeetingData) {
					$roomModel->adobeConnectMeetingData->delete();
				}

				// delete the room itelsf (locally)
				ConferenceRoom::model()->deleteByPk($id);

				// Delete remotely
				$apiClient = new AdobeConnectApiClient(null, null, null, $roomModel->accountId);  // Internally logs in with App Admin
				$apiClient->deleteMeeting($sco_id);


			} else if ($roomModel->room_type == self::CONFTYPE_BIGBLUEBUTTON) {
				$endParams = array(
					'meetingId' => $roomModel->bigBlueButtonMeetingData->meeting_id,
					'password' => $roomModel->bigBlueButtonMeetingData->moderator_pw,
				);
				$bbb = new BbbApiClient();
				$result = $bbb->endMeetingWithXmlResponseArray($endParams);

				if ($roomModel->bigBlueButtonMeetingData) {
					$roomModel->bigBlueButtonMeetingData->delete();
				}
				ConferenceRoom::model()->deleteByPk($id);

			} else if ($roomModel->room_type == self::CONFTYPE_ONSYNC) {
				if ($roomModel->onSyncMeetingData) {
					$session_id = $roomModel->onSyncMeetingData->session_id;
					$roomModel->onSyncMeetingData->delete();
					ConferenceRoom::model()->deleteByPk($id);
					$apiClient = new OnsyncApiClient();
					$apiClient->deleteMeeting($session_id);
				}
			} else if ($roomModel->room_type == self::CONFTYPE_GOTOMEETING) {
				if ($roomModel->gotoMeetingMeetingData) {
					$meetingId = $roomModel->gotoMeetingMeetingData->meeting_id;
					$roomModel->gotoMeetingMeetingData->delete();
					ConferenceRoom::model()->deleteByPk($id);
					$apiClient = new GotomeetingApiClient();
					$apiClient->deleteMeeting($meetingId);
				}
			} else if ($roomModel->room_type == self::CONFTYPE_TELESKILL) {
				if ($roomModel->teleskillMeetingData) {
					$meetingId = $roomModel->teleskillMeetingData->meeting_id;
					$roomModel->teleskillMeetingData->delete();
					ConferenceRoom::model()->deleteByPk($id);
					$apiClient = new TeleskillApiClient();
					$apiClient->deleteMeeting($meetingId);
				}
			} else if ($roomModel->room_type == self::CONFTYPE_WEBEX) {
				if ($roomModel->webexMeetingData) {
					$meetingId = $roomModel->webexMeetingData->session_id;
					$webexType = $roomModel->webexMeetingData->type;
					$roomModel->webexMeetingData->delete();
					ConferenceRoom::model()->deleteByPk($id);
					$apiClient = new WebexApiClient();
					if ($webexType === 'event') {
						$apiClient->deleteEvent($meetingId);
					} else {
						$apiClient->deleteMeeting($meetingId);
					}
				}
			} else if ($roomModel->room_type == self::CONFTYPE_SKYMEETING) {
				if ($roomModel->onSkyMeetingMeetingData) {
					$session_id = $roomModel->skyMeetingMeetingData->session_id;
					$roomModel->skyMeetingMeetingData->delete();
					ConferenceRoom::model()->deleteByPk($id);
					$apiClient = new SkymeetingApiClient();
					$apiClient->deleteMeeting($session_id);
				}
			} else if ($roomModel->room_type == self::CONFTYPE_VIAAPP) {

				$meetingViaAppModel = ConferenceViaappMeeting::model()->
				findByAttributes(array(), 'id_room=:roomId', array(':roomId' => $roomModel->id));
//				$sessionId = $meetingViaAppModel->id;
				$apiClient = new ViaappMeetingApiClient();
				$apiClient->deleteMeeting($meetingViaAppModel);
				$meetingViaAppModel->delete();
				ConferenceRoom::model()->deleteByPk($id);

			} else {
				$this->lastError = self::$MSG_UNKNOWN_CONFERENCING_TYPE;
				return FALSE;
			}

		} catch (Exception $e) {
			$this->lastError = $e->getMessage();
			return FALSE;
		}

		return TRUE;
	}


	/**
	 * "Move" a room to history list
	 *
	 * @param int $id
	 */
	public function archiveRoom($id)
	{
		$roomModel = ConferenceRoom::model()->findByPk($id);
		$roomModel->is_history = 1;
		return $roomModel->save();
	}

	/**
	 * Delete all rooms related to given course Id
	 *
	 * @param int $idCourse
	 */
	public function deleteCourseRooms($idCourse)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "idCourse=:id";
		$criteria->params = array(":id" => $idCourse);

		$rooms = ConferenceRoom::model()->findAll($criteria);
		foreach ($rooms as $room) {
			$this->deleteRoom($room->id);
		}
	}


	/**
	 * Delete all rooms related to given LMS User Id
	 * @param int $userIdst
	 */
	public function deleteUserRooms($userIdst)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "idSt=:id";
		$criteria->params = array(":id" => $userIdst);

		$rooms = ConferenceRoom::model()->findAll($criteria);
		foreach ($rooms as $room) {
			$this->deleteRoom($room->id);
		}

	}


	/**
	 * Check if user is allowed to join a meeting
	 *
	 * @param ConferenceRoom $roomModel
	 * @param CoreUser $userModel
	 *
	 * @return array('success' => true/false,  'reason' +. <string>)
	 */
	public function userCanJoinMeeting($roomModel, $userModel)
	{

		// User must be subscribed to the course of the room/meeting
		$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $userModel->idst, 'idCourse' => $roomModel->idCourse));
		if (!$courseUserModel) {
			$result = array(
				"success" => FALSE,
				"reason" => self::$NOJOIN_NOT_SUBSCRIBED,
			);
			return $result;
		}

		// Do NOT allow students to enter a meeting if current time is outside the meeting's timeframe
		// Meeting timeframe (start and end) is assumed GMT!

// Temporarily allow all users to join, even if meeting time still didn't come through
//         $nowTime = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
//         $isWithinTimeframe = ($roomModel->starttime <= $nowTime && $roomModel->endtime > $nowTime);
//         if ($courseUserModel->level <= LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT && ( ! $isWithinTimeframe)) {
//             $result = array(
//                 "success" => false,
//                 "reason" => self::$NOJOIN_OUTSIDE_TIMEFRAME
//             );
//             return $result;
//         }

		// Conferencing type allowed (Conf. Apps) ?
		$roomType = $roomModel->room_type;
		$availConferenceSystems = $this->getAvailableSystems();
		if (!array_key_exists($roomType, $availConferenceSystems)) {
			$result = array(
				"success" => FALSE,
				"reason" => self::$NOJOIN_SYSTEM_NOT_ENABLED . " ($roomModel->room_type)",
			);
			return $result;
		}


		// Participants count check
		$participantCount = $this->getRoomActiveParticipants($roomModel);

		if ($participantCount === FALSE) {
			$result = array(
				"success" => FALSE,
				"reason" => self::$NOJOIN_CANNOT_COUNT_PARTICIPANTS,
			);
			return $result;
		}

		if ($participantCount >= $roomModel->maxparticipants) {
			$result = array(
				"success" => FALSE,
				"reason" => self::$NOJOIN_ROOM_FULL,
			);
			return $result;
		}


		$result = array(
			"success" => TRUE,
		);


		return $result;

	}

	/**
	 * Returns number of users in a meeting room
	 * (not finished and not much reliable)
	 *
	 * @param ConferenceRoom $roomModel
	 * @return int
	 */
	public function getRoomActiveParticipants($roomModel)
	{

		switch ($roomModel->room_type) {
			case self::CONFTYPE_ADOBECONNECT:

				$apiClient = new AdobeConnectApiClient();
				$meetingModel = $roomModel->adobeConnectMeetingData;
				$response = $apiClient->getMeeting($meetingModel->sco_id);
				$result = (int)$response["my-meetings"]["meeting"]["@attributes"]["active-participants"];
				break;

			case self::CONFTYPE_BIGBLUEBUTTON:
				$bbb = new BbbApiClient();
				$meetingModel = $roomModel->bigBlueButtonMeetingData;
				$infoParams = array(
					'meetingId' => $meetingModel->meeting_id,
					'password' => $meetingModel->moderator_pw,
				);
				$info = $bbb->getMeetingInfoWithXmlResponseArray($infoParams);
				if ($info['returncode'] == 'SUCCESS') {
					$result = $info['participantCount'];
				} else {
					// @todo 0 means NO active participants, which is OK, but fake. Change this... later
					$result = 0;
				}
				break;
			default:
				// @todo 0 means NO active participants, which is OK, but fake. Change this... later
				$result = 0;
				break;
		}

		return $result;
	}

	/**
	 * Return list of available (activated AND enabled) conferencing systems (Apps)
	 *
	 * @return array
	 */
	public static function getAvailableSystems()
	{
		$activatedSystems = self::getActivatedSystems();

		return $activatedSystems;
	}


	/**
	 * Returns list of *activated*  (but could be disabled!!) Conferencing Apps
	 *
	 * @return array
	 */
	public static function getActivatedSystems()
	{
		$result = array();

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_MEETECHO])) {
			$result[self::CONFTYPE_MEETECHO] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_MEETECHO]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_ADOBECONNECT])) {
			$result[self::CONFTYPE_ADOBECONNECT] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_ADOBECONNECT]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_BIGBLUEBUTTON])) {
			$result[self::CONFTYPE_BIGBLUEBUTTON] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_BIGBLUEBUTTON]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_GOTOMEETING])) {
			$result[self::CONFTYPE_GOTOMEETING] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_GOTOMEETING]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_ONSYNC])) {
			$result[self::CONFTYPE_ONSYNC] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_ONSYNC]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_TELESKILL])) {
			$result[self::CONFTYPE_TELESKILL] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_TELESKILL]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_WEBEX])) {
			$result[self::CONFTYPE_WEBEX] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_WEBEX]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_SKYMEETING])) {
			$result[self::CONFTYPE_SKYMEETING] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_SKYMEETING]);
		}

		if (PluginManager::isPluginActive(self::$appMap[self::CONFTYPE_VIAAPP])) {
			$result[self::CONFTYPE_VIAAPP] = PluginManager::getPluginTitle(self::$appMap[self::CONFTYPE_VIAAPP]);
		}

		return $result;
	}


	public function getJoinRedirectUrl($roomId, $idUser)
	{

		$userModel = CoreUser::model()->findByPk($idUser);
		$roomModel = ConferenceRoom::model()->findByPk($roomId);

		if (!$roomModel) {
			Yii::app()->user->setFlash('error', Yii::t('conference', 'Invalid room'));
			Yii::app()->controller->redirect($this->createUrl("site/index"), TRUE);
			Yii::app()->end();
		}

		// Check if user can join THIS ROOM: (subscribed, participants limits, etc.)
		$canJoin = $this->userCanJoinMeeting($roomModel, $userModel);
		if ($canJoin['success'] == FALSE) {
			Yii::app()->user->setFlash('error', $canJoin["reason"]);
			Yii::log($canJoin["reason"], CLogger::LEVEL_ERROR);
			Yii::app()->getController()->redirect(Yii::app()->createUrl("videoconference/index", array("course_id" => $roomModel->idCourse)), TRUE);
		}

		// Resolve Meeting User display name
		$userDisplayName = trim($userModel->firstname . " " . $userModel->lastname);
		$userDisplayName = !empty($userDisplayName) ? $userDisplayName : "User " . $userModel->idst;

		// Is this user a meeting host/moderator ?  (having level above some degree)
		$courseUserModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $roomModel->idCourse));
		if (Yii::app()->user->getIsPu()) {
			// Is this a power user and the course was assigned to him?
			$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
				'puser_id' => Yii::app()->user->id,
				'course_id' => $roomModel->idCourse
			));
		} else
			$pUserCourseAssigned = null;

		$moderator = Yii::app()->user->getIsGodadmin() ||
			(Yii::app()->user->getIsPu() && $pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')) ||
			($courseUserModel->level > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);

		$redirectUrl = FALSE;
		$roomType = $roomModel->room_type;

		// -------- Adobe connect
		if ($roomType == ConferenceManager::CONFTYPE_ADOBECONNECT) {

			try {
				$meetingModel = $roomModel->adobeConnectMeetingData;
				if ($meetingModel) {
					// Get the previously created/saved server url and url path to the meeting
					$url = rtrim($meetingModel->server_url, "/") . $meetingModel->url_path;
					if ($moderator) {
						$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'adobeconnect'));
						if ($account) {
							$login = $account->settings->admin_username;
							$password = $account->settings->admin_password;
						} else
							throw new CHttpException(500, "Adobe Connect account not configured");
						$url .= "?login=" . $login . "&password=" . $password;
					} else {
						$url .= "?guestname=" . urlencode($userDisplayName);
					}

					// Good
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}

		} // ---------- BBB
		else if ($roomType == ConferenceManager::CONFTYPE_BIGBLUEBUTTON) {

			try {

				$meetingModel = $roomModel->bigBlueButtonMeetingData;
				if ($meetingModel) {

					$bbb = new BbbApiClient();

					$creationParams = array(
						'meetingName' => $roomModel->name,
						'meetingId' => $meetingModel->meeting_id,
						'attendeePw' => $meetingModel->attendee_pw,
						'moderatorPw' => $meetingModel->moderator_pw,
						'welcomeMsg' => '',
						'dialNumber' => '',
						'voiceBridge' => '',
						'webVoice' => '',
						'logoutUrl' => '',
						'maxParticipants' => $roomModel->maxparticipants,    // Optional. -1 = unlimitted. Not supported in BBB. [number]
						'record' => 'true',
						'duration' => $meetingModel->duration,
						'meta_category' => '',
					);


					// Re-create the meeting using save ID, passowrds etc. (when we created the LMS room locally)
					$bbb->createMeetingWithXmlResponseArray($creationParams);

					// Common JOIN parameters
					$joinParams = array(
						'meetingId' => $meetingModel->meeting_id,
						'username' => $userDisplayName,
					);


					// Moderators and Attendees join differently
					if ($moderator) {
						$joinParams['password'] = $meetingModel->moderator_pw;
					} else {
						$joinParams['password'] = $meetingModel->attendee_pw;
					}

					// Get the JOIN URL
					$url = $bbb->getJoinMeetingURL($joinParams);

					// Everything is ok
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}

		} // ---------- OnSync
		else if ($roomType == ConferenceManager::CONFTYPE_ONSYNC) {
			try {
				// @TODO Distinguish Attendee vs Host/Moderator URL????
				$apiClient = new OnsyncApiClient();

				$meetingModel = $roomModel->onSyncMeetingData;
				if ($meetingModel) {
					$url = $apiClient->getJoinBaseUrl() . $meetingModel->friendly_id;
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}

		} // ---------- GoToMeeting
		else if ($roomType == ConferenceManager::CONFTYPE_GOTOMEETING) {
			try {
				$apiClient = new GotomeetingApiClient();
				$meetingModel = $roomModel->gotoMeetingMeetingData;
				if ($meetingModel) {
					if ($moderator) {
						$result = $apiClient->getStartMeetingUrl($meetingModel->meeting_id);
						$url = $result["data"]["hostURL"];
					} else {
						$url = $meetingModel->join_url;
					}
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}
		} // ---------- ViaApp
		else if ($roomType == ConferenceManager::CONFTYPE_VIAAPP) {
			try {
				$apiClient = new ViaappMeetingApiClient();
				$meetingModel = ConferenceViaappMeeting::model()->
				findByAttributes(array(), 'id_room=:roomId', array(':roomId' => $roomModel->id));
				if ($meetingModel) {
					// old logic, please leave it just in case
					// $redirectUrl = $meetingModel->hyperlink;

					// create the current user as a guest into ViaApp
					$userInVia = $apiClient->createGuest($meetingModel->activityId);
					$result = $apiClient->loginGuest($meetingModel->activityId, $userInVia['userIdInVia'], $userInVia['ApiID'], $userInVia['CieID']);
					$redirectUrl = $result['TokenURL'];
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}
		} // ----------- TELESKILL
		else if ($roomType == ConferenceManager::CONFTYPE_TELESKILL) {
			try {
				$apiClient = new TeleskillApiClient();
				$meetingModel = $roomModel->teleskillMeetingData;
				if ($meetingModel) {
					if ($moderator) {
						$url = $apiClient->getJoinMeetingUrl($meetingModel->meeting_id, $idUser, $userDisplayName, $userModel->email, TeleskillApiClient::ROLE_HOST);
					} else {
						$url = $apiClient->getJoinMeetingUrl($meetingModel->meeting_id, $idUser, $userDisplayName, $userModel->email, TeleskillApiClient::ROLE_ATTENDEE);
					}
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}
		} // ----------- WEBEX
		else if ($roomType == ConferenceManager::CONFTYPE_WEBEX) {
			try {
				$meetingModel = $roomModel->webexMeetingData;
				/* @var $meetingModel ConferenceWebexMeeting */
				if ($meetingModel && in_array($meetingModel->type, array('event', 'meeting'))) {
					$apiClient = new WebexApiClient();
					if ($moderator) {
						if ($meetingModel->type == 'meeting')
							$url = $apiClient->getHostStartMeetingUrl($meetingModel->session_id);
						else if ($meetingModel->type == 'event')
							$url = $apiClient->getHostStartEventUrl($meetingModel->session_id);
					} else {
						if ($meetingModel->type == 'meeting')
							$url = $apiClient->getAttendeeJoinMeetingUrl($meetingModel->session_id, $userDisplayName, $userModel->email);
						else if ($meetingModel->type == 'event') {
							$firstname = trim($userModel->firstname) ? trim($userModel->firstname) : ("User " . $userModel->idst);
							$lastname = trim($userModel->lastname) ? trim($userModel->lastname) : "Lastname";
							$url = $apiClient->getAttendeeRegisterEventUrl($meetingModel->session_id, $firstname, $lastname, $userModel->email, $roomId);
						}

						// Add Callback URL to handle various situations
						$callBack = urlencode(Yii::app()->createAbsoluteUrl("videoconference/webexCallback", array("course_id" => $roomModel->idCourse, 'room_id' => $roomId)));
						$url .= "&BU=$callBack";
					}
					$redirectUrl = $url;
				} else
					throw new CHttpException(500, "Invalid webex meeting type");
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}
		} // ---------- SkyMeeting
		else if ($roomType == ConferenceManager::CONFTYPE_SKYMEETING) {
			try {
				// @TODO Distinguish Attendee vs Host/Moderator URL????
				$apiClient = new SkymeetingApiClient();

				$meetingModel = $roomModel->skyMeetingMeetingData;
				if ($meetingModel) {
					$url = $apiClient->getJoinBaseUrl() . $meetingModel->friendly_id;
					$redirectUrl = $url;
				}
			} catch (Exception $e) {
				Yii::app()->user->setFlash('error', $e->getMessage());
				$redirectUrl = FALSE;
			}

		}

		return $redirectUrl;
	}


	/**
	 * Create new Meetecho meeting (remotely)
	 *
	 * @param string $conferenceMode
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param array $media
	 *
	 * @return mixed
	 */
	private function createMeetechoMeeting($conferenceMode, $roomName, $startDateTime, $durationMinutes, $media = array())
	{

		try {
			$apiClient = new DoceboWebconferenceApiClient();
			$roomCode = $apiClient->createMeeting($conferenceMode, $roomName, $startDateTime, $durationMinutes, $media);
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return array(
			'room_code' => $roomCode,
			'conference_mode' => $conferenceMode
		);
	}


	/**
	 * Create new Meetecho meeting (remotely)
	 *
	 * @param string $roomCode
	 * @param string $conferenceMode
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param array $media
	 *
	 * @return mixed
	 */
	private function updateMeetechoMeeting($roomCode, $conferenceMode, $roomName, $startDateTime, $durationMinutes, $media = array())
	{

		try {
			$apiClient = new DoceboWebconferenceApiClient();
			$roomCode = $apiClient->updateMeeting($roomCode, $conferenceMode, $roomName, $startDateTime, $durationMinutes, $media);
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return array(
			'room_code' => $roomCode,
			'mode' => $conferenceMode
		);
	}


	/**
	 * Create Adobe Connect Meeting (remotely)
	 *
	 * @param string $roomName
	 * @param int $idCourse
	 * @param int $userIdst
	 * @param int $startUT Seconds  /unix time/
	 * @param int $endUT Seconds  /unix time/
	 *
	 * @param bool $accountId
	 * @return mixed
	 */
	private function createAdobeConnectMeeting($roomName, $idCourse, $userIdst, $startUT, $endUT, $accountId = false)
	{

		// Compose and Slugify meeting name
		$meetingName = 'Docebo_' . trim(strtolower($roomName)) . '_course_' . $idCourse . '_' . time();
		$meetingName = $this->slugify($meetingName);

		// date time parameters
		$dateTimeBegin = date("Y-m-d\TH:i:s\Z", $startUT);
		$dateTimeEnd = date("Y-m-d\TH:i:s\Z", $endUT);
		// Get LMS user who runs this procedure, we gonna create and set him as Host of this meeting (later)
		$userModel = CoreUser::model()->findByPk($userIdst);

		try {
			// Get the API client; it will authenticate, internally using Admin credentials
			$apiClient = new AdobeConnectApiClient(null, null, null, $accountId);

			// Get folder id
			$folderId = $apiClient->getMeetingsFolderId();

			if (!$folderId) {
				$this->lastError = Yii::t('standard', '_OPERATION_FAILURE');
				return FALSE;
			}

			// Create meeting/room
			$meeting = $apiClient->createMeeting($folderId, $meetingName, $dateTimeBegin, $dateTimeEnd);
			$meetingId = $meeting["id"];
			$meetingUrlPath = $meeting["url-path"];
			if ($accountId)
				$account = WebinarToolAccount::model()->findByPk($accountId);
			if (!$account)
				$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'adobeconnect'));

			$hostLogin = $account->settings->admin_username;
			$apiClient->setMeetingHost($meetingId, $hostLogin);
			$apiClient->setMeetingPublic($meetingId);

			// Return full information RE: this meeting for further processing
			$result = array(
				"folder_id" => $folderId,
				"created_by" => $userModel->email,
				"server_url" => $apiClient->basedomain,
				"url_path" => $meetingUrlPath,
				"sco_id" => $meetingId,
				"name" => $meetingName,
			);

			return $result;


		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}

	}


	/**
	 * Update Adobe Connect meeting
	 *
	 * @param int $roomId
	 * @param string $roomName
	 * @param int $idCourse
	 * @param int $userIdst
	 * @param int $startUT
	 * @param int $endUT
	 *
	 * @return bool
	 */
	private function updateAdobeConnectMeeting($roomId, $roomName, $idCourse, $startUT, $endUT, $accountId = false)
	{

		$meetingModel = ConferenceAdobeconnectMeeting::model()->findByAttributes(array("id_room" => $roomId));
		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		// Slugify meeting name
		$meetingName = 'Docebo_' . trim(strtolower($roomName)) . '_course_' . $idCourse . '_' . time();
		$meetingName = $this->slugify($meetingName);

		// date time parameters
		$dateTimeBegin = date("Y-m-d\TH:i:s\Z", $startUT);
		$dateTimeEnd = date("Y-m-d\TH:i:s\Z", $endUT);

		try {
			// Get the API client; it will authenticate internally using Admin credentials
			$apiClient = new AdobeConnectApiClient(null, null, null, $accountId);
			$meeting = $apiClient->updateMeeting($meetingModel->sco_id, $meetingName, $dateTimeBegin, $dateTimeEnd);

			$meetingId = $meeting["id"];

			if ($accountId)
				$account = WebinarToolAccount::model()->findByPk($accountId);
			if (!$account)
				$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'adobeconnect'));

			$hostLogin = $account->settings->admin_username;
			$apiClient->setMeetingHost($meetingId, $hostLogin);
			$apiClient->setMeetingPublic($meetingId);

			// Update local meeting table
			$meetingModel->name = $meetingName;
			$meetingModel->save();
			return TRUE;
		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}


	}


	/**
	 * Create BibBlueButton meeting (remotely)
	 *
	 * @param string $roomName
	 * @param int $idCourse
	 * @param int $userIdst
	 * @param int $startUT
	 * @param int $endUT
	 * @param bool|string $logoutUrl
	 * @param bool $record
	 *
	 * @internal param int $maxParticipants
	 * @return mixed
	 */
	private function createBigBlueButtonMeeting($roomName, $idCourse, $userIdst, $startUT, $endUT, $logoutUrl = FALSE, $record = FALSE)
	{

		$bbb = new BbbApiClient();

		// Redirect URL after logout of the meeting
		if ($logoutUrl === FALSE) {
			$logoutUrl = Yii::app()->createAbsoluteUrl('site/index');
		}

		// Id, passwords
		$meetingId = $this->randomString();
		$attendeePw = $this->randomString();
		$moderatorPw = $this->randomString();

		// Meeting recording
		$record = true;

		// Duration
		$durationSeconds = (int)($endUT - $startUT);
		if ($durationSeconds > 0) {
			$durationMinutes = (int)floor($durationSeconds / 60);
		} else {
			$durationMinutes = 0; // infinite; until last person leaves
		}

		// Prepare parameters
		$params = array(
			'meetingId' => $meetingId,                    // REQUIRED
			'meetingName' => $roomName,                // REQUIRED
			'attendeePw' => $attendeePw,                // Match this value in getJoinMeetingURL() to join as attendee.
			'moderatorPw' => $moderatorPw,                // Match this value in getJoinMeetingURL() to join as moderator.
			'welcomeMsg' => '',                        // ''= use default. Change to customize.
			'dialNumber' => '',                        // The main number to call into. Optional.
			'voiceBridge' => '12345',                    // 5 digit PIN to join voice conference.  Required.
			'webVoice' => '',                            // Alphanumeric to join voice. Optional.
			'logoutUrl' => $logoutUrl,                    // Default in bigbluebutton.properties. Optional.
			'record' => $record,                        // New. 'true' will tell BBB to record the meeting.
			'duration' => $durationMinutes,            // Default = 0 which means no set duration in minutes. [number]
			//'meta_category' => '', 				    // Use to pass additional info to BBB server. See API docs.
		);


		$ok = TRUE;

		$result = NULL;
		try {
			$result = $bbb->createMeetingWithXmlResponseArray($params);
		} catch (Exception $e) {
			Yii::log('BigBlueButton error when getting XML response: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$this->lastError = $e->getMessage();
			$ok = FALSE;
		}

		if ($ok == TRUE) {
			if ($result == NULL) {
				$this->lastError = self::$MSG_FAILED_TO_CONTACT_SERVER;
				$ok = FALSE;
			} else {
				//@todo Analyze result; could be NOT an array
				if ($result['returncode'] == 'SUCCESS') {
					$ok = TRUE;
				} else {
					if ($result['message'])
						$this->lastError = $result['message'];
					else
						$this->lastError = self::$MSG_CONFERENCE_CREATION_FAILED;

					$ok = FALSE;
				}
			}
		}

		// Do the rest if OK
		if ($ok) {
			// !!!! Follow exact names from conference_bigbluebutton_meeting table !!!!
			$result = array(
				"meeting_id" => $meetingId,
				"attendee_pw" => $attendeePw,
				"moderator_pw" => $moderatorPw,
				"logout_url" => $logoutUrl,
				"duration" => $durationMinutes,
			);
		} else {
			$result = FALSE;
		}

		return $result;
	}


	/**
	 * Update BBB Meeting
	 *
	 * @param        $roomId
	 * @param string $roomName
	 * @param int $idCourse
	 * @param int $startUT
	 * @param int $endUT
	 * @param bool $record
	 *
	 * @internal param int $userIdst
	 * @internal param string $logoutUrl
	 *
	 * @return bool
	 */
	private function updateBigBlueButtonMeeting($roomId, $roomName, $idCourse, $startUT, $endUT, $record = TRUE)
	{

		$bbb = new BbbApiClient();

		$meetingModel = ConferenceBigbluebuttonMeeting::model()->findByAttributes(array("id_room" => $roomId));

		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		// Meeting recording
		$record = ($record ? 'true' : 'false');

		// Duration
		$durationSeconds = (int)($endUT - $startUT);
		if ($durationSeconds > 0) {
			$durationMinutes = (int)floor($durationSeconds / 60);
		} else {
			$durationMinutes = 0; // infinite; until last person leaves
		}

		// Prepare parameters
		$params = array(
			'meetingId' => $meetingModel->meeting_id,
			'meetingName' => $roomName,
			'attendeePw' => $meetingModel->attendee_pw,
			'moderatorPw' => $meetingModel->moderator_pw,
			'record' => $record,                        // New. 'true' will tell BBB to record the meeting.
			'duration' => $durationMinutes,            // Default = 0 which means no set duration in minutes. [number]
		);

		$ok = TRUE;
		try {
			$result = $bbb->createMeetingWithXmlResponseArray($params);
			$meetingModel->name = $roomName;
			$meetingModel->duration = $durationMinutes;
			$meetingModel->save();
		} catch (Exception $e) {
			$this->lastError = $e->getMessage();
			$ok = FALSE;
		}


		return $ok;


	}

	/**
	 * Create new OnSync meeting (remotely)
	 *
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return mixed
	 */
	private function createOnsyncMeeting($roomName, $startDateTime, $durationSeconds)
	{

		$friendlyId = $this->randomString(12);
		$durationMinutes = (int)floor($durationSeconds / 60);

		try {
			$apiClient = new OnsyncApiClient();
			$sessionId = $apiClient->createMeeting($roomName, $startDateTime, $durationMinutes, $friendlyId);
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		$result = array(
			'session_id' => $sessionId,
			'friendly_id' => $friendlyId,

		);

		return $result;
	}


	/**
	 * Update OnSync Meeting
	 *
	 * @param int $roomId
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return boolean
	 */
	private function updateOnsyncMeeting($roomId, $roomName, $startDateTime, $durationSeconds)
	{

		$meetingModel = ConferenceOnsyncMeeting::model()->findByAttributes(array("id_room" => $roomId));
		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		$durationMinutes = (int)floor($durationSeconds / 60);

		try {
			$apiClient = new OnsyncApiClient();
			$sessionId = $apiClient->updateMeeting($meetingModel->session_id, $roomName, $startDateTime, $durationMinutes);
			$meetingModel->name = $roomName;
			$meetingModel->save();
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Update Gotomeeting Meeting
	 *
	 * @param int $roomId
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return mixed
	 */
	private function createGotomeetingMeeting($roomName, $startDateTime, $durationSeconds)
	{

		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		try {
			$apiClient = new GotomeetingApiClient();
			$meeting = $apiClient->createMeeting($roomName, $startDateTime, $endDateTime, GotomeetingApiClient::MEETINGTYPE_SCHEDULED);

			if (!isset($meeting["data"]) || (!is_array($meeting["data"]))) {
				$this->lastError = self::$MSG_INVALID_DATA_RETURNED;
				return FALSE;
			}

			$data = $meeting["data"][0];

			$result = array(
				"name" => $roomName,
				"meeting_id" => $data["meetingid"],
				"unique_meeting_id" => $data["uniqueMeetingId"],
				"join_url" => $data["joinURL"],
				"conference_call_info" => $data["conferenceCallInfo"],
				"max_participants" => $data["maxParticipants"],
			);

			return $result;
		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}


	}


	/**
	 * Update Gotomeeting Meeting
	 *
	 * @param int $roomId
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return boolean
	 */
	private function updateGotomeetingMeeting($roomId, $roomName, $startDateTime, $durationSeconds)
	{

		$meetingModel = ConferenceGotomeetingMeeting::model()->findByAttributes(array("id_room" => $roomId));
		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		try {
			$apiClient = new GotomeetingApiClient();
			$apiClient->updateMeeting($meetingModel->meeting_id, $roomName, $startDateTime, $endDateTime);
			$meetingModel->name = $roomName;
			$meetingModel->save();
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return TRUE;
	}


	private function createWebexMeeting($roomName, $startDateTime, $durationSeconds, $webexType)
	{

		$durationMinutes = (int)floor((int)$durationSeconds / 60);

		try {
			$apiClient = new WebexApiClient();

			if ($webexType === 'event') {
				$event = $apiClient->createEvent($roomName, $startDateTime, $durationMinutes);

				if (!is_array($event) || !isset($event["sessionKey"])) {
					$this->lastError = self::$MSG_INVALID_DATA_RETURNED;
					return FALSE;
				}

				// Follow DB Table names
				$result = array(
					"type" => 'event',
					"session_id" => $event["sessionKey"],
					//"host_calendar_url" => '',
					//"attendee_calendar_url" => '',
					"guest_token" => $event["guestToken"],
				);
			} else {
				$meeting = $apiClient->createMeeting($roomName, $startDateTime, $durationMinutes);

				if (!is_array($meeting) || !isset($meeting["meetingkey"])) {
					$this->lastError = self::$MSG_INVALID_DATA_RETURNED;
					return FALSE;
				}

				// Follow DB Table names
				$result = array(
					"type" => 'meeting',
					"session_id" => $meeting["meetingkey"],
					"host_calendar_url" => $meeting["iCalendarURL"]["host"],
					"attendee_calendar_url" => $meeting["iCalendarURL"]["attendee"],
					"guest_token" => $meeting["guestToken"],
				);
			}


			return $result;
		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}


	}


	private function updateWebexMeeting($roomId, $roomName, $startDateTime, $durationSeconds, $webexType)
	{

		$meetingModel = ConferenceWebexMeeting::model()->findByAttributes(array("id_room" => $roomId));

		$durationMinutes = (int)floor((int)$durationSeconds / 60);

		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		try {
			if ($webexType === 'event') {
				$apiClient = new WebexApiClient();
				$apiClient->updateEvent($meetingModel->session_id, $roomName, $startDateTime, $durationMinutes);
				$meetingModel->save();
			} else {
				$apiClient = new WebexApiClient();
				$apiClient->updateMeeting($meetingModel->session_id, $roomName, $startDateTime, $durationMinutes);
				$meetingModel->save();
			}
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return TRUE;
	}


	private function createTeleskillMeeting($roomName, $startDateTime, $durationSeconds, $maxParticipants = FALSE)
	{
		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		try {
			$apiClient = new TeleskillApiClient();

			$meetingId = $apiClient->createMeeting($roomName, $startDateTime, $endDateTime, $maxParticipants);

			if (!$meetingId) {
				$this->lastError = self::$MSG_INVALID_DATA_RETURNED;
				return FALSE;
			}

			$result = array(
				"meeting_id" => $meetingId,
			);

			return $result;
		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}


	}


	private function updateTeleskillMeeting($roomId, $roomName, $startDateTime, $durationSeconds, $maxParticipants = FALSE)
	{
		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		$meetingModel = ConferenceTeleskillMeeting::model()->findByAttributes(array("id_room" => $roomId));
		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		try {
			$apiClient = new TeleskillApiClient();
			$apiClient->updateMeeting($meetingModel->meeting_id, $roomName, $startDateTime, $endDateTime, $maxParticipants);
			$meetingModel->save();
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return TRUE;

	}


	/**
	 * Create new Skymeeting meeting (remotely)
	 *
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return mixed
	 */
	private function createSkymeetingMeeting($roomName, $startDateTime, $durationSeconds)
	{

		$friendlyId = $this->randomString(12);
		$durationMinutes = (int)floor($durationSeconds / 60);

		try {
			$apiClient = new SkymeetingApiClient();
			$sessionId = $apiClient->createMeeting($roomName, $startDateTime, $durationMinutes, $friendlyId);
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		$result = array(
			'session_id' => $sessionId,
			'friendly_id' => $friendlyId,

		);

		return $result;
	}


	/**
	 * Update Skymeeting Meeting
	 *
	 * @param int $roomId
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationSeconds
	 *
	 * @return boolean
	 */
	private function updateSkymeetingMeeting($roomId, $roomName, $startDateTime, $durationSeconds)
	{

		$meetingModel = ConferenceSkyMeetingMeeting::model()->findByAttributes(array("id_room" => $roomId));
		if (!$meetingModel) {
			$this->lastError = Yii::t('conference', 'Invalid room');
			return FALSE;
		}

		$durationMinutes = (int)floor($durationSeconds / 60);

		try {
			$apiClient = new SkymeetingApiClient();
			$sessionId = $apiClient->updateMeeting($meetingModel->session_id, $roomName, $startDateTime, $durationMinutes);
			$meetingModel->name = $roomName;
			$meetingModel->save();
		} catch (Exception $e) {
			$this->lastError = $e->getCode() . " " . $e->getMessage();
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * Generate random string of an arbitrary length
	 *
	 * @param int $length
	 *
	 * @return string
	 */
	private function randomString($length = 12)
	{
		$str = "";
		$characters = array_merge(range('A', 'Z'), range('0', '9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}


	/**
	 * Convert a string replacing whitespaces to "_"  (slugify)
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	private function slugify($str)
	{
		return preg_replace("/[^\w\.]/", "_", trim(strtolower($str)));
	}


	/**
	 * Checks if room limit per course has been reached
	 *
	 * @param int $idCourse
	 * @param string $roomType
	 * @param string $utcNow
	 *
	 * @return bool
	 */
	public static function canCreateNewRoom($idCourse, $roomType, $utcNow)
	{
		// the following settings refer to a specific videoconference system ($roomType)
		$account = WebinarToolAccount::model()->findByAttributes(array('tool' => $roomType));
		$maxTotalRooms = intval($account->settings->max_rooms);
		$maxRoomsPerCourse = intval($account->settings->max_rooms_per_course);

		$countActiveRoomsPerCourse = ConferenceRoom::model()->countByAttributes(array(
			'idCourse' => $idCourse,
			'room_type' => $roomType,
			'is_history' => 0
		), 'starttime >= :now', array(':now' => strtotime($utcNow)));

		$countTotalActiveRooms = ConferenceRoom::model()->countByAttributes(array(
			'room_type' => $roomType,
			'is_history' => 0
		), 'starttime >= :now', array(':now' => strtotime($utcNow)));

		// If setting equals 0, regard it as infinite, so always return true
		return (
			(($maxRoomsPerCourse == 0)?true:($countActiveRoomsPerCourse < $maxRoomsPerCourse))
			&&
			(($maxTotalRooms == 0)?true:($countTotalActiveRooms < $maxTotalRooms))
		);
	}

	private function createViaappMeeting($roomName, $startDateTime, $durationSeconds)
	{
		$endTime = strtotime($startDateTime) + $durationSeconds;
		$endDateTime = date('Y-m-d H:i:s', $endTime);

		try {
			$apiClient = new ViaappMeetingApiClient();
			$meeting = $apiClient->createMeeting($roomName, $startDateTime, $durationSeconds, $endDateTime);

			if (!isset($meeting["data"]) || (!is_array($meeting["data"]))) {
				$this->lastError = self::$MSG_INVALID_DATA_RETURNED;
				return FALSE;
			}

			$result = array(
				"title" => $roomName,
				"activityId" => $meeting['data'][1],
				"hyperlink" => 'https://labs.sviesolutions.com/' . $meeting['data'][2],
			);

			return $result;
		} catch (Exception $e) {
			$this->lastError = "Exception: " . $e->getMessage();
			return FALSE;
		}
	}

	private function updateViaappMeeting($roomName, $roomDescription, $startDateTime, $durationMinutes, $roomId)
	{
		$apiClient = new ViaappMeetingApiClient();
		$result = $apiClient->editMeeting($roomName, $roomDescription, $startDateTime, $durationMinutes, $roomId);
		if($result){
			return true;
		}
		return false;
	}

    public static function manageAdobeRoomDateOverlapping($accountId, $startDateTime, $duration){
        // Check if there is already existing session with overlapping dates
        $selectedWebinarAccount = WebinarToolAccount::model()->findByPk($accountId);

        // Check if isset max concurrent rooms and if is difference then zero / zero means no limit
        if(isset($selectedWebinarAccount->settings->max_concurrent_rooms) && $selectedWebinarAccount->settings->max_concurrent_rooms != 0){
            // Calculate the start and the end date of the new session  because we have only start + duration
            $newStartDate = strtotime($startDateTime);
            $newEndDate = strtotime(date('Y/m/d H:i:s', $newStartDate) . ' +'.$duration[0]. 'hours ' . $duration[1]. 'minutes');

            // Get all meetings to check if there is old overlapping session
            $adobeApiClient = new AdobeConnectApiClient(null, null, null, $accountId);
            $meetings = $adobeApiClient->getAllMeetings()['my-meetings']['meeting'];

            // If there is no meetings we can go on
            if ($meetings != null && !empty($meetings)) {
                // The API can return one session or array of sessions so we should take care of that
                $meetingsList = isset($meetings['name']) ? array($meetings) : $meetings;
                $overlapped = 0;

                foreach ($meetingsList as $meeting) {
                    // Get a session start and end date, and convert them to UTC
                    $dateBegin = strtotime(Yii::app()->localtime->toUTC($meeting['date-begin'],  'Y/m/d H:i:s'));
                    $dateEnd = strtotime(Yii::app()->localtime->toUTC($meeting['date-end'], 'Y/m/d H:i:s'));

                    // Check if there is overlapping dates
                    if( ($newEndDate  > $dateBegin) && ($dateEnd > $newStartDate) ) {
                        // And if there is overlapping date check if the max overlapping dates is reached
                        if($overlapped >= $selectedWebinarAccount->settings->max_concurrent_rooms){
                            Yii::app()->user->setFlash('error', Yii::t('webinar', 'Max concurrent rooms reached.'));
                            return true;
                        }
                        $overlapped++;
                    }
                }
            }
        }
        return false;
    }

    public static function manageRoomDateOverlapping($webinarId, $accountId, $startDateTime, $endDatetime){
        // if we don't have account we can't check how many meetings are in certain account
        if(!$accountId) return false;

        // Check if there is already existing session with overlapping dates
        $selectedWebinarAccount = WebinarToolAccount::model()->findByPk($accountId);

        // Check if isset max concurrent rooms and if is difference then zero / zero means no limit
        if(isset($selectedWebinarAccount->settings->max_concurrent_rooms) && $selectedWebinarAccount->settings->max_concurrent_rooms != 0){
            // Get timestamps of start and end dates of the new room
            $newStartDate = strtotime(Yii::app()->localtime->fromLocalDateTime($startDateTime, LocalTime::SHORT, LocalTime::SHORT));
            $newEndDate = strtotime(Yii::app()->localtime->fromLocalDateTime($endDatetime, LocalTime::SHORT, LocalTime::SHORT));

            // Get system meetings that are only in the future
			$command = Yii::app()->db->createCommand()
				->select('duration_minutes, CONVERT_TZ(CONCAT_WS(" ", day, time_begin), timezone_begin, "GMT") as dateBegin')
				->from(WebinarSessionDate::model()->tableName())
				->where('id_tool_account = :account AND day >= CURDATE()', array(':account' => $accountId));

            if($webinarId){
				$command->andWhere('id_session != :session', array(':session' => $webinarId));
            }
            $meetings = $command->queryAll();

            // If there is no meetings we can go on
            if (!empty($meetings)) {
                /*
                 * Because the minimum duration is 15 min we will have to split the duration of the new session into chunks of 15 min
                 * and we will have to check for each chunk if there is overlapping dates
                 */
                $minDurationTime = key(WebinarSession::arrayForDuration()) * 60;
                $tempStartDate = $newStartDate;
                $tempEndDate = $newStartDate + $minDurationTime;
                while($tempEndDate <= $newEndDate){
                    $overlapped = 1;
                    foreach ($meetings as $meeting) {
                        // Get a timestamp of session start and end date
						$dateBegin = new DateTime($meeting['dateBegin'], new DateTimeZone('UTC'));
						$dateBegin = $dateBegin->getTimestamp();
						$dateEnd = $dateBegin + $meeting['duration_minutes']*60;

                        // Check if there is overlapping dates
                        if( ($tempEndDate  > $dateBegin) && ($dateEnd > $tempStartDate) ) {
                            if($overlapped >= $selectedWebinarAccount->settings->max_concurrent_rooms)
                                return true;

                            $overlapped++;
                        }
                    }
                    $tempStartDate = $tempEndDate;
                    $tempEndDate += $minDurationTime;
                }
            }
        }
        return false;
    }
}
