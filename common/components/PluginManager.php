<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2012 Docebo
 */

/**
 * PluginManager class
 * Used to perform plugin related operations
 */

if (!defined('_plugins_')) {
	define('_plugins_', dirname(__FILE__) . '/../../plugins');
	define('_custom_plugins_', dirname(__FILE__) . '/../../custom_plugins');
}

class PluginManager {

    /**
     * We may have list of plugins not used anymore, but forgotten in ACTIVE status in LMS's database.
     * Lets play safe and avoid them
     * @var array
     */
    static $DEAD_PLUGINS = array(
        'ThomsonReutersPoc2App',   // functionality is ported into core product
    );

	//keep the state of each plugin status in this variable for a single php request, it will save duplicate SQL queries (a lot..)
	public static $pluginActive = array();


	/**
	 * Loads all Webinar Tools that are active in core_plugin
	 *
	 * @param bool $only_modules
	 * @param bool $only_mobile
	 * @param bool $loadInConsole
	 */
	public static function loadWebinarToolPlugins($only_modules = false, $only_mobile = false, $loadInConsole = false){
		$webinarTools = WebinarTool::$webinarTools;

		//Get the active plugins first
		$activeWebinars = Yii::app()->db->createCommand()
			->select('plugin_name')
			->from('core_plugin')
			->where(array('IN', 'plugin_name', $webinarTools))
			->andWhere('is_active = 1')
			->queryAll();

		if(!empty($activeWebinars)){
			foreach($activeWebinars as $tool){
				self::loadPlugin($tool['plugin_name'], $only_modules, $only_mobile, $loadInConsole);
			}
		}
	}
	/**
	 * @param $plugin_name
	 * @param bool $only_modules
	 * @param bool $only_mobile
	 * @param bool $loadInConsole
	 * @return bool|mixed
	 */
	public static function loadPlugin($plugin_name, $only_modules = false, $only_mobile = false, $loadInConsole = false) {
	    
	    
	    // JUST DON'T LOAD DEAD PLUGINS!
	    if (in_array($plugin_name, self::$DEAD_PLUGINS)) {
	        return true;
	    }
	    
		$plugin_obj = false;
		$plugin_file = _plugins_ . '/' . $plugin_name . '/' . $plugin_name . '.php';
		$custom_plugin_file = _custom_plugins_ . '/' . $plugin_name . '/' . $plugin_name . '.php';
		$is_custom = false;

		if (file_exists($plugin_file)) {
			if (!$only_modules) {
				require_once($plugin_file);
				$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
				if ($only_mobile && !$plugin_obj->is_mobile_capable) return false;
				$plugin_obj->init();
			}
		} else if (file_exists($custom_plugin_file)) {
			$is_custom = true;
			if (!$only_modules) {
				require_once($custom_plugin_file);
				$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
				if ($only_mobile && !$plugin_obj->is_mobile_capable) return false;
				$plugin_obj->init();
			}
		}

		// if it is a new (yii) plugin (module) let's load it:
		if (self::isModulePlugin($plugin_name)) {

			if ($only_mobile) {

				if (file_exists($plugin_file)) {

					require_once($plugin_file);
					$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
					if (!$plugin_obj->is_mobile_capable) return false;
				} else if (file_exists($custom_plugin_file)) {

					require_once($custom_plugin_file);
					$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
					if (!$plugin_obj->is_mobile_capable) return false;
				} else {

					return false;
				}
			}

			$plugin_alias = 'plugin';
			if ($is_custom) $plugin_alias = 'custom_plugins';

			$module_to_add = array();
			$module_to_add[$plugin_name] = array(
				'class' => $plugin_alias . '.' . $plugin_name . '.' . ucfirst($plugin_name) . 'Module',
				'defaultController' => ucfirst($plugin_name),
			);

			// Import module's components and models
			Yii::import($plugin_alias . '.' . $plugin_name . '.components.*');
			Yii::import($plugin_alias . '.' . $plugin_name . '.models.*');

			// Add to loaded Yii Modules
			Yii::app()->setModules($module_to_add);



			if (!(Yii::app() instanceof CConsoleApplication) || $loadInConsole) {

				// Causes init() to be called
				$module = Yii::app()->getModule($plugin_name);
			}
		}

		return (!$only_modules ? $plugin_obj : true);
	}

	/**
	 * This method call the required methods on active plugin modules and give back the results.
	 * @param $callback
	 * @return array
	 */
	public static function moduleCallback($callback) {

		$plugins = CorePlugin::model()->findAllByAttributes(array('is_active' => 1));

		$result =array();
		foreach($plugins as $row) {

			if (self::isModulePlugin($row->plugin_name)) {

				$module = Yii::app()->getModule($row->plugin_name);
				if ($module && method_exists($module, $callback)) {
					$result[$row->plugin_name] = call_user_func(array($module, $callback));
				}
			}
		}
		return $result;
	}



	/**
	 * Activate an application by its internal codename (defined in Erp)
	 * @param $codename
	 * @return bool
	 */
	public static function activateAppByCodename($codename) {

		// changed in order
		$plugin_name = ucfirst($codename) . 'App';
		$plugin_file = _plugins_ . '/' . $plugin_name . '/' . $plugin_name . '.php';
		$custom_plugin_file = _custom_plugins_ . '/' . $plugin_name . '/' . $plugin_name . '.php';

		if (file_exists($plugin_file)) {

			require_once($plugin_file);
			$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
			$plugin_obj->init();

			//If the plugin have some menu items add them
			self::managePluginMenuItemsIfAny($plugin_name, $active = true);

		} else if (file_exists($custom_plugin_file)) {

			require_once($custom_plugin_file);
			$plugin_obj = call_user_func(array($plugin_name, 'plugin'));
			$plugin_obj->init();
		} else{
			return false;
		}

		/**
		 * Raise event to check if we can activate the plugin
		 */
		$event = new DEvent(self, array('pluginName' => $plugin_name));
		Yii::app()->event->raise(EventManager::EVENT_ON_PLUGIN_ACTIVATE, $event);

		if(isset($event->return_value['canProceed']) && $event->return_value['canProceed'] === false){
			return false;
		}

		$plugin_obj->activate();

		return true;
	}
	/**
	 * Activate an application by its internal codename (defined in Erp) both on local and on erp (this will apply
	 * a charge to the user if needed)
	 * @param $appId
	 * @param $codename
	 * @return bool
	 */
	public static function activateErpAndLocalAppByCodename($appId, $codename) {


		$apiParams = array(
			'installation_id' => Docebo::getErpInstallationId(),
			'app_id' => $appId,
			'reseller_cookie' => isset(Yii::app()->request->cookies['reseller_cookie']) ? Yii::app()->request->cookies['reseller_cookie'] : false
		);
		$apiRes = AppsMpApiClient::apiAddApplication($apiParams);
		$apiRes = CJSON::decode($apiRes);

		/*
		 * then activate the app (plugin)
		 */
		$res = self::activateAppByCodename($codename);

		return $res;
	}
	/**
	 * Deactivate an application by its internal codename (defined in Erp)
	 * @param $codename
	 * @return bool
	 */
	public static function deactivateAppByCodename($codename) {
		$app_plugin_name = ucfirst($codename) . 'App';
		$plugin = PluginManager::loadPlugin($app_plugin_name);
		if ($plugin) {

			/**
			 * Raise event to check if we can deactivate the plugin
			 */
			$event = new DEvent(self, array('pluginName' => $app_plugin_name));
			Yii::app()->event->raise(EventManager::EVENT_ON_PLUGIN_DEACTIVATE, $event);

			if(isset($event->return_value['canProceed']) && $event->return_value['canProceed'] === false){
				return false;
			}

			//If the plugin have some menu items hide them
			self::managePluginMenuItemsIfAny($app_plugin_name, $active = false);

			//$res = $plugin::plugin()->deactivate();
			$plugin_obj = call_user_func(array($plugin, 'plugin'));
			$res = $plugin_obj->deactivate();

			$res = true;

		} else {
			$res = false;
		}

		return $res;
	}

	protected static function managePluginMenuItemsIfAny($plugin, $active = false)
	{
		$models = CoreMenuItem::model()->findAllByAttributes(array('module_name' => $plugin));
		if(!empty($models))
		{
			foreach($models as $model)
			{
				$model->active = $active ? 1: 0;
				$model->saveNode();
			}
		}
	}

	/**
	 * Check if the plugin is also a Yii module
	 * @param $plugin_name
	 * @return bool
	 */
	public static function isModulePlugin($plugin_name) {
		$plugin_module_file = _plugins_ . '/' . $plugin_name . '/' . ucfirst($plugin_name) . 'Module.php';
		$custom_plugin_file = _custom_plugins_ . '/' . $plugin_name . '/' . ucfirst($plugin_name) . 'Module.php';
		if (file_exists($plugin_module_file)) return true;
		return file_exists($custom_plugin_file);
	}


	/**
	 * With the new LMS version (based on Yii Framework) we only load plugins that are
	 * also a Yii module by adding them to the application configuration.
	 */
	public static function loadActivePlugins($only_mobile = false, $loadInConsole = false) {
		$plugins = self::getActivePlugins();

		// load MultidomainApp first
		if(PluginManager::isPluginActive('MultidomainApp')) {
			foreach ($plugins as $key => $plugin) {
				if ($plugin->plugin_name == 'MultidomainApp') {
					self::loadPlugin($plugin->plugin_name, true, $only_mobile, $loadInConsole);
				}
			}
		}
		foreach($plugins as $row) {
			if ($row->plugin_name != 'MultidomainApp') {
				self::loadPlugin($row->plugin_name, true, $only_mobile, $loadInConsole);
			}
		}

		// Check if there is a menu to add for each plugin.
		// Do this after all plugins are already loaded
		// since any given plugin may register a event listener
		// and override the menus of any other plugin or
		// the core menus afterwards
		if (!(Yii::app() instanceof CConsoleApplication) ) {
			foreach($plugins as $row){
				if(!Yii::app()->hasModule($row->plugin_name)){
					continue;
				}

				$module = Yii::app()->getModule($row->plugin_name);
				if (method_exists($module, 'menu')) {
					$module->menu();
				}
			}

		}
	}

	/**
	 * Return if a specific plugin is active
	 * @param $plugin_name
	 * @return bool
	 */
	public static function isPluginActive($plugin_name) {
		if (!isset(self::$pluginActive[$plugin_name])) {
			$isActive = Yii::app()->getDb()->createCommand()
				->select('is_active')
				->from('core_plugin')
				->where('plugin_name=:pluginName', array(':pluginName'=>$plugin_name))
				->queryScalar();
			if ($isActive)
				self::$pluginActive[$plugin_name] = true;
			else
				self::$pluginActive[$plugin_name] = false;
		}

		return self::$pluginActive[$plugin_name];
	}


	/**
	 * Get plugin title
	 *
	 * @param string $plugin_name
	 * @return string
	 */
	public static function getPluginTitle($plugin_name) {
		$res = "";
		$pluginModel = CorePlugin::model()->findByAttributes(array('plugin_name' => $plugin_name));

		if ($pluginModel) {
			$res = $pluginModel->plugin_title;
		}
		return $res;
	}


	/**
	 * Get a list of active plugins
	 *
	 * @param $exclude_custom Whether custom plugins should be included in the list or not
	 *
	 * @return array list of plugin ARs
	 */
	public static function getActivePlugins($exclude_custom = false) {
		$allPlugins = CorePlugin::model()->findAllByAttributes(array('is_active' => 1));
		if(!$exclude_custom)
			return $allPlugins;

		// Filter out plugins not in the _plugins_ folder
		$corePlugins = array();
		foreach($allPlugins as $plugin){
			$file = _plugins_ . '/' . $plugin->plugin_name . '/' . ucfirst($plugin->plugin_name) . '.php';
			if(file_exists($file))
				$corePlugins[] = $plugin;
		}

		return $corePlugins;
	}

	/**
	 * Get a list of active custom plugins
	 *
	 * @return array list of plugin ARs
	 */
	public static function getActiveCustomPlugins(){
		$allPlugins = CorePlugin::model()->findAllByAttributes(array('is_active' => 1));
		$customPlugins = array();

		// Filter out core plugins not in the _custom_plugins_ folder
		foreach($allPlugins as $plugin) {
			$file = _custom_plugins_ . '/' . $plugin->plugin_name . '/' . ucfirst($plugin->plugin_name) . '.php';
			if(file_exists($file))
				$customPlugins[] = $plugin;
		}

		return $customPlugins;
	}

}
