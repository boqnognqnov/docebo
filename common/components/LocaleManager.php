<?php

class LocaleManager
{
	static public function getCurrencyArr($symbol=false) {
		// Full list:
		// https://cms.paypal.com/uk/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_currency_codes
		$res =array(
			'AUD' => ($symbol ? 'AUD' : 'AUD'),
			'BRL' => ($symbol ? 'BRL' : 'BRL'),
			'CAD' => ($symbol ? 'CAD' : 'CAD'),
			'CZK' => ($symbol ? 'CZK' : 'CZK'),
			'DKK' => ($symbol ? 'DKK' : 'DKK'),
			'EUR' => ($symbol ? '€' : 'EUR'),
			'HKD' => ($symbol ? 'HKD' : 'HKD'),
			'HUF' => ($symbol ? 'HUF' : 'HUF'),
			'ILS' => ($symbol ? 'ILS' : 'ILS'),
			'JPY' => ($symbol ? 'JPY' : 'JPY'),
			'MYR' => ($symbol ? 'MYR' : 'MYR'),
			'MXN' => ($symbol ? 'MXN' : 'MXN'),
			'NOK' => ($symbol ? 'NOK' : 'NOK'),
			'NZD' => ($symbol ? 'NZD' : 'NZD'),
			'PHP' => ($symbol ? 'PHP' : 'PHP'),
			'PLN' => ($symbol ? 'PLN' : 'PLN'),
			'GBP' => ($symbol ? 'GBP' : 'GBP'),
			'SGD' => ($symbol ? 'SGD' : 'SGD'),
			'SEK' => ($symbol ? 'SEK' : 'SEK'),
			'CHF' => ($symbol ? 'CHF' : 'CHF'),
			'TWD' => ($symbol ? 'TWD' : 'TWD'),
			'THB' => ($symbol ? 'THB' : 'THB'),
			'TRY' => ($symbol ? 'TRY' : 'TRY'),
			'USD' => ($symbol ? '$' : 'USD'),
		);
		return $res;
	}
}
