<?php

/**
 * Class DatePickerHelper
 *
 * helps the developer to handle some bootstrap datepicker tasks, such as loading js files, correct language, etc.
 */
class DatePickerHelper extends CComponent {


	/**
	 * internal cache for available bootstrap datepicker locale files
	 * @var null
	 */
	protected static $_locales = null;


	/**
	 * retrieve all available locale files for datepicker and store them in an array.
	 * Files are physically searched only the first time, then cached internally.
	 * @return array the list of found files
	 */
	protected static function getLocaleFiles() {
		if (self::$_locales === null) {
			self::$_locales = array();
			//list all files in the locales directory
			$path = Yii::getPathOfAlias('bootstrap.assets.js.locales');
			$localeFiles = scandir($path);
			if (!empty($localeFiles)) {
				//now filter all datepicker localization files
				foreach ($localeFiles as $localeFile) {
					if (is_string($localeFile) && stripos($localeFile, 'bootstrap-datepicker.') === 0) {
						//we have detected a datepicker localization file, add it to the output list
						$arr = explode('.', $localeFile);
						if (count($arr) == 3 && strtolower($arr[2]) == 'js') {
							$languageCode = strtolower($arr[1]);
							self::$_locales[$languageCode] = $localeFile;
						}
					}
				}
			}
		}
		return self::$_locales;
	}


	/**
	 * Retrieve the passed language localization js file name
	 * @param $language the requested language browser code, false for default language
	 * @return string/boolean file name, or false if file is not present for the requested language
	 */
	public static function getLocaleFileName($language = false) {
		$index = trim(strtolower((!$language ? Yii::app()->getLanguage() : $language)));
		$list = self::getLocaleFiles();
		return (isset($list[$index]) ? $list[$index] : false);
	}


	/**
	 * @param bool $language the required language's browser code (if false value is passed, then default language is used)
	 */
	public static function registerAssets($language = false, $position = false) {

		//analyze input parameter
		$languageCode = trim(strtolower((!$language ? Yii::app()->getLanguage() : $language)));
		$scriptPosition = ($position === false ? CClientScript::POS_END : $position);

		//load essential scripts and styles
		Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
		Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js', $scriptPosition);

		//load correct localization file, if existent
		$localeFiles = self::getLocaleFiles();
		if (isset($localeFiles[$languageCode])) {
			Yii::app()->bootstrap->registerAssetJs('locales/'.$localeFiles[$languageCode], $scriptPosition);
		}
	}

}