<?php
/**
 * A backport of Yii2's \yii\base\Security class
 * used for password encryption/hashing/checking
 *
 * (not everything has been backported, since we don't need everything)
 *
 * @author: Dzhuneyt
 */

class Security extends CApplicationComponent{

	public $passwordHashStrategy = 'crypt';

	/**
	 * Generates a secure hash from a password and a random salt.
	 *
	 * The generated hash can be stored in database (e.g. `CHAR(64) CHARACTER SET latin1` on MySQL).
	 * Later when a password needs to be validated, the hash can be fetched and passed fully
	 * to [[validatePassword()]].
	 *
	 * @param     $password
	 * @param int $cost
	 *
	 * @return string The password hash string, ASCII and not longer than 64 characters.
	 * @see validatePassword()
	 */
	public function hashPassword($password, $cost = 13){
		return CPasswordHelper::hashPassword($password, $cost);
	}

	/**
	 * Verifies a password against a hash.
	 *
	 * @param string $password The password to verify.
	 * @param string $hash The hash to verify the password against.
	 *
	 * @throws CException
	 * @return boolean whether the password is correct.
	 * @see hashPassword()
	 */
	public function validatePassword($password, $hash){
		return CPasswordHelper::verifyPassword($password, $hash);
	}

	/**
	 * Checks if the passed encrypted password is in the modern
	 * crypt() based format or the old md5() one.
	 *
	 * Since the password is encrypted and we can't know if
	 * crypt() was used or md5(), we have to make an intelligent
	 * guess based on the first few characters of the encrypted
	 * script (which if the password was ran through crypt()
	 * are special and part of the hash, that was used to encrypt
	 * the originals string)
	 *
	 * @see http://us.php.net/manual/bg/function.crypt.php
	 *
	 * @param $password
	 *
	 * @throws CException
	 * @return bool
	 */
	public function isPasswordModern($password){
		// Checks if the provided password HASH
		// looks like it's encrypted using crypt()
		// If it is, depending on the system's available
		// crypting methods, the provided hashed password will start with one of:
		// - If blowfish is enabled: $2$ or $2a$ ("a" can be any letter) - e.g. $2y$blabla
		// - If md5 is enabled: $1$ (e.g. $1$blabla)
		// - If DES is enabled: nine or two letter generic string
		// The last scenario/encryption method is not supported currently,
		// because we can't differentiate the result from the old plain md5('password')
		// password encryption method and password ran through crypt() based on the
		// DES algorithm (since both results MAY be 32 characters in length in some cases)

		if(!defined('CRYPT_BLOWFISH') || CRYPT_BLOWFISH==0){
			throw new CException('CRYPT_MD5, CRYPT_STD_DES and CRYPT_EXT_DES are not
								among the supported password encryption methods.
								Please update your PHP version to one that supports
								CRYPT_BLOWFISH. See: http://us.php.net/manual/bg/function.crypt.php');
		}

		// Is pass encrypted using crypt() using the Blowfish algorith
		$isCryptAndBlowfished = array();
		preg_match('#^\$2(.?)\$#Ui', $password, $isCryptAndBlowfished);
		if(count($isCryptAndBlowfished))
			return TRUE;

		// Is pass encrypted using crypt() using the md5 algorithm
		$isCryptAndMd5 = array();
		preg_match('#^\$1\$#Ui', $password, $isCryptAndMd5);
		if(count($isCryptAndMd5))
			return FALSE; // maybe we will allow crypt() with a md5 algo in the future? if so, change this to True

		// Just plain old md5('string'); old-mechanism
		if(strlen($password)==32)
			return FALSE;

		// Generic error. Password doesn't seem right
		return FALSE;
	}

	/**
	 * Generates a salt that can be used to generate a password hash.
	 *
	 * The PHP [crypt()](http://php.net/manual/en/function.crypt.php) built-in function
	 * requires, for the Blowfish hash algorithm, a salt string in a specific format:
	 * "$2a$", "$2x$" or "$2y$", a two digit cost parameter, "$", and 22 characters
	 * from the alphabet "./0-9A-Za-z".
	 *
	 * @param integer $cost the cost parameter
	 *
	 * @throws CException
	 * @return string the random salt value.
	 */
	protected function generateSalt($cost = 13){
		$cost = (int)$cost;
		if ($cost < 4 || $cost > 31) {
			throw new CException('Cost must be between 4 and 31.');
		}

		// Get 20 * 8bits of random entropy
		$rand = $this->generateRandomBytes(20);

		// Add the microtime for a little more entropy.
		$rand .= microtime(true);
		// Mix the bits cryptographically into a 20-byte binary string.
		$rand = sha1($rand, true);
		// Form the prefix that specifies Blowfish algorithm and cost parameter.
		$salt = sprintf("$2y$%02d$", $cost);
		// Append the random salt data in the required base64 format.
		$salt .= str_replace('+', '.', substr(base64_encode($rand), 0, 22));

		return $salt;
	}

	/**
	 * Generates specified number of random bytes.
	 * Note that output may not be ASCII.
	 * @see generateRandomKey() if you need a string.
	 *
	 * @param integer $length the number of bytes to generate
	 *
	 * @throws InvalidConfigException
	 * @throws Exception
	 * @return string the generated random bytes
	 */
	public function generateRandomBytes($length = 32)
	{
		if (!extension_loaded('mcrypt')) {
			throw new CException('The mcrypt PHP extension is not installed.');
		}
		$bytes = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
		if ($bytes === false) {
			throw new CException('Unable to generate random bytes.');
		}
		return $bytes;
	}

	/**
	 * Performs string comparison using timing attack resistant approach.
	 * @see http://codereview.stackexchange.com/questions/13512
	 * @param string $expected string to compare.
	 * @param string $actual string to compare.
	 * @return boolean whether strings are equal.
	 */
	protected function compareString($expected, $actual)
	{
		// timing attack resistant approach:
		$diff = 0;
		for ($i = 0; $i < mb_strlen($actual, '8bit'); $i++) {
			$diff |= (ord($actual[$i]) ^ ord($expected[$i]));
		}
		return $diff === 0;
	}

    /**
     * Returns array. 'success' says if validation is OK or not. 'message' corresponds to the message of the check that has not been passed.
     * @param $new_password
     * @param bool $password_retype
     * @param bool $username
     * @param bool $options
     * @return mixed
     */
    public function checkPasswordStrength($new_password,$password_retype=false, $username=false, $options=false){

        //Default settings are TRUE = All cheks are done
        $checkMinChars = true;
        $checkAlphaNumeric = true;
        $checkRepeating = true;
        $checkSequentialNumbers = true;
        $checkSequentialLetters = true;
        $checkSequentialKeyboard = true;
        $checkDictionary = true;

        //Set custom settings
        if(is_array($options) && !empty($options) && isset($options['minChars'])){
            $checkMinChars = (bool)$options['minChars'];
        }
        if(is_array($options) && !empty($options) && isset($options['alphaNum'])){
            $checkAlphaNumeric = (bool)$options['alphaNum'];
        }
        if(is_array($options) && !empty($options) && isset($options['repeating'])){
            $checkRepeating = (bool)$options['repeating'];
        }
        if(is_array($options) && !empty($options) && isset($options['sequentialNum'])){
            $checkSequentialNumbers = (bool)$options['sequentialNum'];
        }
        if(is_array($options) && !empty($options) && isset($options['sequentialLet'])){
            $checkSequentialLetters = (bool)$options['sequentialLet'];
        }
        if(is_array($options) && !empty($options) && isset($options['sequentialKey'])){
            $checkSequentialKeyboard = (bool)$options['sequentialKey'];
        }
        if(is_array($options) && !empty($options) && isset($options['dictionary'])){
            $checkDictionary = (bool)$options['dictionary'];
        }


        //ACTUAL CHECKS
        if($password_retype!==false) {
            // The two passwords must be equal.
            if ($new_password != $password_retype) {
                $result['message'] = Yii::t('register', '_ERR_PASSWORD_NO_MATCH');
                $result['success'] = false;
                return $result;
            }
        }
        // The password must contain at least C chars
        if ($checkMinChars && strlen($new_password) < Settings::get('pass_min_char', 0)) {
            $result['message'] = Yii::t('register','_PASSWORD_TOO_SHORT');
            $result['success'] = false;
            return $result;
        }
        // Checking if password must be alfanumeric
        if ($checkAlphaNumeric && Settings::get('pass_alfanumeric') == 'on' ) {
            if (!preg_match('/[a-z]/i', $new_password) || !preg_match('/[0-9]/', $new_password)) {
                $result['message'] = Yii::t('register','_ERR_PASSWORD_MUSTBE_ALPHA');
                $result['success'] = false;
                return $result;
            }
        }

        if($username) {
            // password should be different from username
            if (Settings::get('pass_not_username') == 'on' && strtolower($new_password) == strtolower($username)) {
                $result['message'] = Yii::t('configuration', '_PASS_NOT_USERNAME');
                $result['success'] = false;
                return $result;
            }
        }

        // Check for 3 or more repeating characters
        if ($checkRepeating && preg_match('/([\w\W])\1{2,}/', $new_password)) {
            $result['message'] = Yii::t('register','Password cannot contain 3 or more repeating characters!');
            $result['success'] = false;
            return $result;
        }
        // Check for 3 or more sequential numbers
        if ($checkSequentialNumbers && preg_match('/(012|123|234|345|456|567|678|789|987|876|765|654|543|432|321|210)+/', strtolower($new_password))) {
            $result['message'] = Yii::t('register','Password cannot contain 3 or more sequential numbers!');
            $result['success'] = false;
            return $result;
        }
        // Check for 4 or more alphabetically sequential characters
        if ($checkSequentialLetters && preg_match('/((abcd|bcde|cdef|defg|efgh|fghi|ghij|hijk|ijkl|jklm|klmn|lmno|mnop|nopq|opqr|pqrs|qrst|rstu|stuv|tuvw|uvwx|vwxy|wxyz)|(zyxw|yxwv|xwvu|wvut|vuts|utsr|tsrq|srqp|rqpo|qpon|ponm|onml|nmlk|mlkj|lkji|kjih|jihg|ihgf|hgfe|gfed|fedc|edcb|dcba))+/', strtolower($new_password))) {
            $result['message'] = Yii::t('register','Password cannot contain 4 or more alphabetically sequential characters!');
            $result['success'] = false;
            return $result;
        }
        // Check for 4 or more US-keyboard-sequential characters
        if ($checkSequentialKeyboard && preg_match('/((qwer|wert|erty|rtyu|tyui|yuio|uiop|asdf|sdfg|dfgh|fghj|ghjk|hjkl|zxcv|xcvb|cvbn|vbnm)|(mnbv|nbvc|bvcx|vcxz|lkjh|kjhg|jhgf|hgfd|gfds|fdsa|poiu|oiuy|iuyt|uytr|ytre|trew|rewq))+/', strtolower($new_password))) {
            $result['message'] = Yii::t('register','Password cannot contain 4 or more US-keyboard-sequential characters!');
            $result['success'] = false;
            return $result;
        }


        // The password must not be a dictionary word
        if ($checkDictionary && Settings::get('pass_dictionary_check') == 'on' ) {
            $base = Docebo::getRootBasePath();

            $arrayOfWords = file($base . DIRECTORY_SEPARATOR . 'lms' . DIRECTORY_SEPARATOR . 'protected' . DIRECTORY_SEPARATOR . 'words.txt');
            $arrayOfWords = array_map('trim', $arrayOfWords);
            $flippedArray = array_flip($arrayOfWords);

            if ( isset($flippedArray[$new_password]) ) {
                $result['message'] = Yii::t('register', 'Password cannot contain common dictionary words and most common passwords');
                $result['success'] = false;
                return $result;
            }
        }
        $result['success'] = true;
        $result['message'] = '';
        return $result;
    }
} 