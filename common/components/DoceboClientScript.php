<?php
class DoceboClientScript extends CClientScript
{
    
    const ORDER_FIRST   = 1;
    const ORDER_LAST    = 0;
    
	/**
	 * * Inserts the scripts at the beginning of the body section.
	 * @return string the output to be inserted with scripts.
	 */
	public function renderOnRequest()
	{
		$html='';
		foreach($this->scriptFiles as $scriptFiles)
		{
			foreach($scriptFiles as $scriptFile)
				$html.=CHtml::scriptFile($scriptFile)."\n";
		}
		foreach($this->scripts as $script)
			$html.=CHtml::script(implode("\n",$script))."\n";

		if($html!=='')
			return $html;
	}
	
	
	
	public function getScriptFiles() {
		return $this->scriptFiles;
	}
	

	public function getCssFiles() {
		return $this->cssFiles;
	}
	

	/**
	 * Return a two-elements array consisting Javascript and CSS file URLs (absolute)
	 * @return multitype:multitype:string
	 */
	public function getClientResourcesUrls() {
		$jsArray = array();
		foreach ($this->getScriptFiles() as $position => $scripts)  
			foreach ($scripts as $key => $val) 
				$jsArray[] = $val;
			
		$cssArray = array();
		foreach ($this->getcssFiles() as $key => $val)  
			$cssArray[] = $key;
		
		return array('js' => $jsArray, 'css' => $cssArray);
	}
	
	
	/**
	 * Override parent method to allow specificying ordering (i.e. t put the script at FIRST or LAST place)
	 * 
	 * {@inheritDoc}
	 * @see CClientScript::registerScriptFile()
	 */
	public function registerScriptFile($url,$position=null,array $htmlOptions=array(), $order=self::ORDER_LAST) {
	    // Default behavior is to put the script at the bottom (last)
	    if ($order === false || $order === self::ORDER_LAST) {
	       parent::registerScriptFile($url,$position,$htmlOptions);
	    }
	    // Make it on TOP of the list???
	    else {
	        parent::registerScriptFile($url,$position,$htmlOptions);
	        // Get the arreay of scripts
	        $scriptFiles = $this->scriptFiles[$position];
	        // Remove the last one (just added)
	        array_pop($scriptFiles);
	        // Inverse the array
	        $scriptFiles = array_reverse($scriptFiles, true);
	        // Add our script at the bottom
	        $scriptFiles[$url] = $url;
	        // And inverse again
	        $scriptFiles = array_reverse($scriptFiles, true);
	        // Finally, set the script files array
	        $this->scriptFiles[$position] = $scriptFiles;
	    }
	    return $this;
	}
	
	
}

