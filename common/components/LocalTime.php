<?php

/**
 * Class LocalTime
 *
 * todo: check sub_start_date and sub_end_date text fields in
        /lms/prot/modules/player/views/coursesettings/catalog.php and
        /lms/prot/modules/player/views/coursesettings/subscription.php
 *
 */
class LocalTime extends CApplicationComponent {
    /**
     * Default server time
     */
    const _utc = 'UTC';

    const SHORT 	= 'short';
    const MEDIUM	= 'medium';
    const LONG		= 'long';  // Use it for DATES only, LONG time is not supported by date time parsers

	// Default ones, used as default function parameters, etc
    const DEFAULT_DATE_WIDTH = self::SHORT;
    const DEFAULT_TIME_WIDTH = self::MEDIUM;

    /**
     * Generally, we have 2 formats: UTC and LOCAL one
     * @var string
     */
    const FORMAT_UTC_DATE 		= 'utc_date';
    const FORMAT_UTC_DATETIME 	= 'utc_datetime';
    const FORMAT_LOCAL_DATE		= 'local_date';
    const FORMAT_LOCAL_DATETIME	= 'local_datetime';


    /**
     * Constants used in date comparisions
     * @var string
     */
    const EQUAL = 'equal';
    const GT	= 'greater_than';
    const LT	= 'less_than';


    /**
     * Constants used in Add/Substract operations
     * @var string
     */
    const OPERATION_ADD = 'add';
    const OPERATION_SUB = 'sub';

    const HOUR_12 = "h12";
    const HOUR_24 = "h24";

    /**
     * @var string The locally applied timezone
     */
    private $_localTimezone = null;

    /**
     * @var string Datetime format to force (ignoring all platform settings)
     */
    private $_forcedDatetimeformat = null;

    /**
     * @var string Date format to force (ignoring all platform settings)
     */
    private $_forcedDateformat = null;


    /**
     * Resets the timezone cached
     */
    public function resetLocalTimezone() {
        $this->_localTimezone = null;
    }

    /**
     * Return the current timeZone
     * @return string
     */
    public function getTimeZone()
    {
        // Get the localTimezone if it's been set
        if(!$this->_localTimezone) {
            // Fetch it from the user settings
			$timeZone = false;
            if (!(Yii::app() instanceof CConsoleApplication) && (Yii::app()->hasComponent("user")) && (Yii::app()->user !== null)) {
            	$timeZone = Yii::app()->user->getTimeZone();
            }

            // No user timezone -> fallback to utc
            if (!$timeZone)
                $timeZone = self::_utc;

            $this->_localTimezone = $timeZone;
        }

        return $this->_localTimezone;
    }

    /**
     * Local now() function
     * Can use any of the php date() formats to return the local date/time value
     * http://php.net/manual/en/function.date.php
     * @param string $format
     * @return string
     */
    public function getLocalNow($format=DATE_ISO8601)
    {
        $localNow = new DateTime(null, $this->localDateTimeZone);
        return $localNow->format($format);
    }

    /**
     * UTC Now() function
     * Can use any of the php date() formats to return the UTC date/time value
     *
     * WARNING: This function returns (by default) a timestamp in the DATE_ISO8601 format,
     * not interpreted correctly by strtotime(). To properly get a timestamp use:
     * $timestamp = strtotime(Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'))
     *
     * http://php.net/manual/en/function.date.php
     * @param string $format
     * @return string
     */
    public function getUTCNow($format=DATE_ISO8601)
    {
        // Fix encoding issue when Arabic date format is selected
        // Values in yii/i18n/data/ar.php::dateFormats are badly encoded
        $format = preg_replace('/[^[:print:]]/', '', $format);
        $utcNow = new DateTime(null, $this->serverDateTimeZone);
        return $utcNow->format($format);
    }

    /**
     * Return a datetimezone object for the local time
     * @param null $timeZone
     * @return DateTimeZone
     */
    public function getLocalDateTimeZone($timeZone=null)
    {
        if ($timeZone===null)
            $timeZone = $this->getTimeZone();

        // Create a local datetimezone object
        return new DateTimeZone($timeZone);
    }

    /**
     * Return a datetimezone object for UTC
     * @return DateTimeZone
     */
    public function getServerDateTimeZone()
    {
        // Create a local datetimezone object
        $dateTimeZone = new DateTimeZone(self::_utc);
        return $dateTimeZone;
    }

	/**
	 * Converts a local timestamp to UTC
	 * Expects a date in Y-m-d H:i:s format and assumes it is the local time zone
	 * Returns an ISO date in the UTC zone
	 * @param $localTimestamp
	 * @oaram $format es. 'Y-m-d H:i:s'
	 * @return string
	 */
	public function toUTC($localTimestamp, $format = DATE_ISO8601)
	{
		try {
			// Create an object using the local time zone - this will account for daylight saving
			$serverTime = new DateTime($localTimestamp, $this->localDateTimeZone);

			// Then set the timeZone to UTC and it will be automagically updated
			// In theory this step isn't needed if using the ISO8601 format.
			$serverTime->setTimeZone($this->serverDateTimeZone);

			// Return the formatted date
			return $serverTime->format($format);
		} catch (Exception $e) {
			return $localTimestamp;
		}
	}

	/**
	 * Use in afterFind
	 * Returns the date based on the current locale
	 * Expects a date string in the yyyy-mm-dd type format
	 * @param string $serverTimestamp
	 * @param string $dateWidth
	 * @param string $localFormat
	 * @param boolean $applyTimezone
	 * @return string The date formatted in the local format
	 */
	public function toLocalDate($serverTimestamp = null, $dateWidth = self::DEFAULT_DATE_WIDTH)
	{
		try {
			// Create a server datetime object
			$localTime = new DateTime($serverTimestamp, $this->serverDateTimeZone);

			// Get the local yii date only format
			$localFormat = $this->getLocalDateFormat($dateWidth);

			// Return as a local datetime
			return (Yii::app()->dateFormatter->format($localFormat, $localTime->format('Y-m-d H:i:s')));

		} catch (Exception $e) {
			// Give up formatting -> show original date string
			return $serverTimestamp;
		}
	}

	/**
	 * Accepts a time in the HH:mm format and returns it in the local format
	 * @param null $serverTimestamp
	 * @param string $width
	 * @return mixed
	 */
	public function toLocalTime($serverTimestamp = null, $width = self::DEFAULT_TIME_WIDTH)
	{
		// Get the local yii time only format
		$locale = $this->getLocalDateTimeFormatLocale();
		$localFormat = $locale->getTimeFormat($width);

		return Yii::app()->dateFormatter->format($localFormat, $serverTimestamp);
	}

	/**
	 * Use in afterFind
	 * Returns a date/time combination based on the current locale
	 * Expects a date/time in the yyyy-mm-dd hh:mm:ss type format
	 *
	 * @param string $serverTimestamp
	 * @param string $dateWidth
	 * @param string $timeWidth
	 * @param string $localFormat
	 * @param string $timezone (the timezone the timestamp is in)
	 * @param bool $allowNullDatetime
	 * @param bool $keepUTC
	 *
	 * @return string The datetime formatted as the local format
	 */
	public function toLocalDateTime($serverTimestamp = null, $dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth = self::DEFAULT_TIME_WIDTH, $localFormat = null, $timezone = null, $allowNullDatetime = true, $keepUTC = false)
	{
		if ((!$allowNullDatetime && !$serverTimestamp) || ($serverTimestamp == '0000-00-00 00:00:00'))
			return $serverTimestamp;

		try {
			// Get the timezone the timestamp is in
			$sourceTimezone = $timezone ? $this->getLocalDateTimeZone($timezone) : $this->serverDateTimeZone;

			// Create a server datetime object
			$localTime = new DateTime($serverTimestamp, $sourceTimezone);

			if (!$keepUTC) {
				// Then set the timezone to local and it will be automagically updated, even allowing for daylight saving
				$localTime->setTimeZone($this->localDateTimeZone);
			} else {
				$localTime->setTimeZone($this->serverDateTimeZone);
			}

			if ($localFormat === null) {
				// Get the local yii date+time format
				$localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);
				// Convert to php date format
				$localFormat = $this->YiitoPHPDateFormat($localFormat);
			}

			// Return as a local datetime
			return $localTime->format($localFormat);

		} catch (Exception $e) {
			// Give up formatting -> show original datetime
			return $serverTimestamp;
		}
	}

    /**
     * @TODO This method is mistery to me [plamen]  
     * 
     * Returns a date/time combination based on the current system locale for ssconvert from csv to xls
     * Expects a date/time in the yyyy-mm-dd hh:mm:ss type format
     *
     * @param string $serverTimestamp
     * @param string $dateWidth
     * @param string $timeWidth
     *
     * @return string The datetime formatted as the system locale format
     */
    public function toDateTimeInServerFormatWithLocalTz($serverTimestamp = null, $dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth = self::DEFAULT_TIME_WIDTH)
    {
        if (!$serverTimestamp || ($serverTimestamp == '0000-00-00 00:00:00'))
            return '';

        try {
            // Get the timezone the timestamp is in
            $sourceTimezone = $this->serverDateTimeZone;

            // Create a server datetime object
            $localTime = new DateTime($serverTimestamp, $sourceTimezone);

             // Then set the timezone to local and it will be automagically updated, even allowing for daylight saving
            $localTime->setTimeZone($this->localDateTimeZone);


            // Get the local yii date+time format
            $localFormat = Yii::app()->locale->getDateFormat($dateWidth) . " " . Yii::app()->locale->getTimeFormat($timeWidth);
            // Convert to php date format
            $localFormat = $this->YiitoPHPDateFormat($localFormat);


            // Return as a local datetime
            return $localTime->format($localFormat);

        } catch (Exception $e) {
            // Give up formatting -> show original datetime
            return $serverTimestamp;
        }
    }

	/**
	 * Use in beforeSave
	 * Converts a date/time string in the locale format to an ISO time for saving to the server
	 * eg 31/12/2011 will become 2011-12-31T00:00:00+0000
	 *
	 * @param        $localTime
	 * @param string $dateWidth
	 * @param string $timeWidth
	 * @param null   $localFormat Override the local format with a specific date/time format
	 * @param bool   $keepUTC
	 *
	 * @return string Returns the date in ISO format in UTC timezone
	 */
    public function fromLocalDateTime($localTime, $dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth = self::DEFAULT_TIME_WIDTH, $localFormat = null, $keepUTC = false)
    {
        if(!$localTime)
            return null;

        try {
            // If date is already in ISO format, return it as it is
            if(CDateTimeParser::parse($localTime,'yyyy-MM-dd hh:mm:ss') !== FALSE) {
				$localFormat = 'yyyy-MM-dd hh:mm:ss';
			} else {

				if ($localFormat === null) { // Local datetime format

					// Check if the time is just a date
					$localFormat = $this->getLocalDateFormat($dateWidth);
					if(CDateTimeParser::parse($localTime, $localFormat) !== FALSE) {
						return $this->fromLocalDate($localTime, $dateWidth);
					}

					// Try with incoming timeWidth and if no success, try the oppsite (short->medium, medium -> short)
					$localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);
					if(CDateTimeParser::parse($localTime, $localFormat) === FALSE) {
						$timeWidth = ($timeWidth == self::SHORT ? self::MEDIUM : self::SHORT);
						$localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);
					}
				}
			}
            // Fix encoding issue when Arabic date format is selected
            // Values in yii/i18n/data/ar.php::dateFormats are badly encoded
            $localFormat = preg_replace('/[^[:print:]]/', '', $localFormat);

            // Uses a modified CDateTimeParser that defaults the time values rather than return false
            // Also returns a time string rather than a timestamp just in case the timestamp is the wrong timezone
            $defaults = array('year'=>$this->getLocalNow('Y'), 'month'=>$this->getLocalNow('m'), 'day'=>$this->getLocalNow('d'), 'hour'=>0, 'minute'=>0, 'second'=>0 );
            $timeValues = DefaultDateTimeParser::parse($localTime, $localFormat, $defaults);

            if(!$keepUTC) {
                // Create a new date time in the local timezone
                $serverTime = new DateTime($timeValues, $this->localDateTimeZone);

                // Set the timezone to UTC
                $serverTime = $serverTime->setTimeZone($this->serverDateTimeZone);
            } else {
                // Create a new date time in UTC
                $serverTime = new DateTime($timeValues, $this->serverDateTimeZone);
            }

            // Return it as an iso date ready for saving
            return $serverTime->format(DATE_ISO8601);

            // MAYBE This ????
            //return $serverTime->format('Y-m-d H:i:s');

        } catch(Exception $e) {
            return $localTime;
        }
    }

    /**
     * Parses a date from local format to 'Y-m-d'
     * @param $localTime
     * @param string $dateWidth
     * @return string The Y-m-d date string
     */
    public function fromLocalDate($localTime, $dateWidth = self::DEFAULT_DATE_WIDTH)
    {
        if(!$localTime)
            return null;

        try {
            // If date is already in ISO format, return it as it is
            if(CDateTimeParser::parse($localTime,'yyyy-MM-dd') !== FALSE)
                return $localTime;

            // Local datetime format
            $localFormat = $this->getLocalDateFormat($dateWidth);

            // Fix encoding issue when Arabic date format is selected
            // Values in yii/i18n/data/ar.php::dateFormats are badly encoded
            $localFormat = preg_replace('/[^[:print:]]/', '', $localFormat);

            // Uses a modified CDateTimeParser that defaults the time values rather than return false
            // Also returns a time string rather than a timestamp just in case the timestamp is the wrong timezone
            $defaults = array('year'=>$this->getLocalNow('Y'), 'month'=>$this->getLocalNow('m'), 'day'=>$this->getLocalNow('d'), 'hour'=>0, 'minute'=>0, 'second'=>0 );

            $timeValues = DefaultDateTimeParser::parse($localTime, $localFormat, $defaults);

            $localTime = new DateTime($timeValues, $this->localDateTimeZone);

            // Return it as an iso date ready for saving
            return $localTime->format('Y-m-d');
        } catch(Exception $e) {
            return $localTime;
        }
    }

	/**
	 * Removes the time part from a locally formatted datetime (used to show datetimes in bdatepicker)
	 *
	 * @param string $localDatetime Locally formatted datetime string
	 * @param string $dateWidth
	 *
	 * @return locally formatted date
	 */
    public function stripTimeFromLocalDateTime($localDatetime, $dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth=self::DEFAULT_TIME_WIDTH) {

        if(!$localDatetime)
            return null;

        try {
            // If date is already in ISO format, return the first part of the mysql date
            if(CDateTimeParser::parse($localDatetime,'yyyy-MM-dd hh:mm:ss') !== FALSE)
                return substr($localDatetime, 0, 10);


            $localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);
            if(CDateTimeParser::parse($localDatetime, $localFormat) === FALSE) {
            	$timeWidth = ($timeWidth == self::SHORT ? self::MEDIUM : self::SHORT);
            	$localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);
            }
            $defaults = array('year'=>$this->getLocalNow('Y'), 'month'=>$this->getLocalNow('m'), 'day'=>$this->getLocalNow('d'), 'hour'=>0, 'minute'=>0, 'second'=>0 );
            $timeValues = DefaultDateTimeParser::parse($localDatetime, $localFormat, $defaults);

            // Create a new date time in the local timezone
            $localTime = new DateTime($timeValues, $this->localDateTimeZone);

            // Get the local date format and convert it to php date format
            $localFormat = $this->getLocalDateFormat($dateWidth);
            $localFormat = $this->YiitoPHPDateFormat($localFormat);

            // Return as a local datetime
            return $localTime->format($localFormat);

        } catch(Exception $e) {
            return $localDatetime;
        }
    }

    public function getPHPLocalDateFormat($dateWidth = self::DEFAULT_DATE_WIDTH)
    {
        // Get the local yii date only format
        $localFormat = $this->getLocalDateFormat($dateWidth);

        // Convert to php date format
        return $this->YiitoPHPDateFormat($localFormat);
    }

    public function getPHPLocalDateTimeFormat($dateWidth = self::DEFAULT_DATE_WIDTH,$timeWidth=self::DEFAULT_TIME_WIDTH)
    {
        // Get the local yii date only format
        $localFormat = $this->getLocalDateTimeFormat($dateWidth, $timeWidth);

        // Convert to php date format
        return $this->YiitoPHPDateFormat($localFormat);
    }

    /**
     * Method called by the api controller to force date format yyyy-MM-dd HH:mm:ss
     */
    public function forceMysqlDatetimeFormats() {
        $this->_forcedDatetimeformat = 'yyyy-MM-dd HH:mm:ss';
        $this->_forcedDateformat = 'yyyy-MM-dd';
    }



	/**
	 * Returns the local date time format from yii
	 */
	public function getLocalDateTimeFormat($dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth = self::DEFAULT_TIME_WIDTH, $ignoreForcedLongYears = false)
	{
		if ($this->_forcedDatetimeformat)
			$localFormat = $this->_forcedDatetimeformat;
		else {
			if (null === $dateWidth) $dateWidth = self::DEFAULT_DATE_WIDTH;
			if (null === $timeWidth) $timeWidth = self::DEFAULT_TIME_WIDTH;

			$locale = $this->getLocalDateTimeFormatLocale();

			// This assumes the locale has already been set
			// This returns the order of the date time combination, eg {1} {0}
			$localFormat = $locale->getDateTimeFormat();

			// Get the local date format - eg dd/MM/yyyy
			$dateFormat = $locale->getDateFormat($dateWidth);

			// Fix encoding issue when Arabic date format is selected
			// Values in yii/i18n/data/ar.php::dateFormats are badly encoded
			$dateFormat = preg_replace( '/[^[:print:]]/', '',$dateFormat);

			if ($this->forceLongYears() && !$ignoreForcedLongYears) { $dateFormat = $this->convertAllYearsIntoLongYears($dateFormat); }
			$localFormat = str_replace('{1}', /*$locale->getDateFormat($dateWidth)*/$dateFormat, $localFormat);

			// Get the local time format with seconds - eg hh:mm:ss
			$localFormat = str_replace('{0}', $locale->getTimeFormat($timeWidth), $localFormat);
		}

		return $localFormat;
	}


	/**
	 * Just a wrapper of the yii getDateFormat function
	 */
	public function getLocalDateFormat($width = self::DEFAULT_DATE_WIDTH, $ignoreForcedLongYears = false)
	{
		if ($this->_forcedDateformat)
			$localFormat = $this->_forcedDateformat;
		else {
			if (null === $width) $width = self::DEFAULT_DATE_WIDTH;

			$locale = $this->getLocalDateTimeFormatLocale();

			// Get the local date format - eg dd/MM/yyyy
			$localFormat = $locale->getDateFormat($width);

			// Fix encoding issue when Arabic date format is selected
			// Values in yii/i18n/data/ar.php::dateFormats are badly encoded
			$localFormat = preg_replace( '/[^[:print:]]/', '',$localFormat);

			if ($this->forceLongYears() && !$ignoreForcedLongYears) { $localFormat = $this->convertAllYearsIntoLongYears($localFormat); }
		}

		return $localFormat;
	}

	/**
	 * Convert local date format to new given format
	 * @param $originalValue
	 * @param $newFormat
	 * @return formatted date
	 */
	public function convertLocalDateToFormat($originalValue , $newFormat)
	{
		try{
			$format = DateTime::createFromFormat($this->getPHPLocalDateFormat(), $originalValue);
		}catch (Exception $e){
			$format = null;
		}

		if($format)
			return $format->format($newFormat);
		else
			return $originalValue;
	}

    /**
     * Find the locale from which to apply the date/time formatting
     * @return CLocale
     */
    public function getLocalDateTimeFormatLocale()
    {
        $localeId = Yii::app()->getLanguage();

        $dateFormatType = Settings::get('date_format');

        if ($dateFormatType == 'browser_code') {
            // the date/times will be formatted according to the locale set
        }
        else if ($dateFormatType == 'user_selected') {
            if (!(Yii::app() instanceof CConsoleApplication)) {
                $userSelectedLocaleId = Yii::app()->user->selectedDateTimeFormat;
                if (!empty($userSelectedLocaleId)) {
                    $localeId = $userSelectedLocaleId;
                }
            }
        }
        else {
            // $dateFormatType == 'custom'
            // the superadmin has forced a date format
            $localeId = Settings::get('date_format_custom');
        }

        $locale = Yii::app()->getLocale($localeId);

        return $locale;
    }

    /**
     * Converts a yii date format to a php date format
     * Eg dd/MM/yyyy HH:mm:ss will be converted to d/m/Y H:i:s
     *
     * @param $dateFormat
     * @return mixed
     */
    public function YiitoPHPDateFormat($dateFormat)
    {
        $patterns = array(
            // 'ampm'=>array('a'=>'a','A'=>'A'),
            // don't want to show microseconds
            'microseconds'=>array('zzzz'=>'','z'=>''),
            'seconds'=>array('ss'=>'s','s'=>'s'),
            'minutes'=>array('mm'=>'i','m'=>'i'),
            '24hours'=>array('HH'=>'H','H'=>'G'),
            '12hours'=>array('hh'=>'h','h'=>'g'),
            'years'=>array('yyyy'=>'Y','yy'=>'y','y'=>'Y'),
            'months'=>array('MMMM'=>'F','MMM'=>'M','MM'=>'m','M'=>'n'),
            'days'=>array('dd'=>'d','d'=>'j'),
        );

        // special case (long format)
        $dateFormat = str_replace('EEEE', 'l', $dateFormat);

        // Run through each time pattern
        foreach($patterns as $pattern)
        {
            foreach($pattern as $search=>$replace)
            {
                if (strpos($dateFormat, $search)!==false)
                {
                    // We have a winner!
                    // Replace the first pattern found and then get out of here
                    $dateFormat = str_replace($search, $replace, $dateFormat);
                    break;
                }
            }
        }

        return $dateFormat;
    }

    /**
     * Converts a PHP date format to a format to be used by datepicker jQuery UI widget
     * The $dateString should come in 'short' format only!
     * @param $dateString
     * @return string
     */
    public function dateStringToJQueryDatepickerFormat($dateString)
    {
        $pattern = array(

            //day
            'd',		//day of the month
            'j',		//3 letter name of the day
            'l',		//full name of the day
            'z',		//day of the year

            //month
            'F',		//Month name full
            'M',		//Month name short
            'm',		//numeric month leading zeros
            'n',		//numeric month no leading zeros

            //year
            'Y', 		//full numeric year
            'y'		//numeric year: 2 digit
        );
        $replace = array(
            'dd','d','DD','o',
            'MM','M','mm','m',
            'yy','y'
        );

        foreach($pattern as &$p)
            $p = '/'.$p.'/';

        return preg_replace($pattern, $replace, $dateString);
    }

    /**
     * Converts a PHP date format to a format to be used by bootstrap's datepicker plugin
     * The $dateString should come in 'short' format only!
     * @param $dateString
     * @param $dateWidth
     * @return string
     */
    public function dateStringToBDatepickerFormat($dateString = null, $dateWidth = null)
    {
        if ($dateString === null)
            $dateString = $this->getPHPLocalDateFormat($dateWidth);

    	$map = array(
    		// day
    		'd' => 	'dd',   //day of the month
    		'j'	=>	'd',	//3 letter name of the day
    		'l'	=>	'DD',	//full name of the day
    		'z'	=>	'o',	//day of the year
    		// month
    		'F' =>	'MM',	//Month name full
    		'M'	=>	'M',	//Month name short
    		'm'	=> 	'mm',	//numeric month leading zeros
    		'n'	=>	'm',	//numeric month no leading zeros
    		// year
    		'Y'	=>	'yyyy',	//full numeric year
    		'y'	=> 	'yy'	//numeric year: 2 digit
    	);
    	$result = strtr($dateString, $map);
		return $result;
    }


    /**
     * Returns an array of locale codes for all regions of a locale which defaults to the yii language
     * @param null $localeCode
     * @return array
     */
    public function getLocaleRegions($localeCode = null) {

        if ($localeCode === null) {
            $localeCode = Yii::app()->getLanguage();
        }

        // find related locales
        $regions = array($localeCode);
        $localeIds = CLocale::getLocaleIDs();

        foreach ($localeIds as $localeId) {
            // find locales that begin with the specified code,
            // immediately followed by '_' !
            if (0 === strpos($localeId, $localeCode.'_')) {
                $regions[] = $localeId;
            }
        }

        return $regions;
    }

    /**
     * todo: optimize
     *
     * Return time (seconds from epoch) of the nearest hour ahead
     *
     * @param int $hours
     * @param bool|number $time seconds
     * @return number
     */
    public function nextRoundHourTime($hours=1, $time=false) {
        if (!$time) {
            $time = time();
        }
        return (($time - ($time % 3600)) / 3600 + $hours) * 3600;
    }

	/**
	 * Returns an array of date/time formats
	 * @param null $locale
	 * @param string $width
	 * @return array
	 */
	public function getDateTimeFormatsArray($locale = null, $dateWidth = self::DEFAULT_DATE_WIDTH, $timeWidth = self::DEFAULT_TIME_WIDTH)
	{
		$dateTimeFormats = array();

		// Create a server datetime object
		$localTime = new DateTime('2005-03-16 09:30:00', $this->getLocalDateTimeZone("Europe/Rome"));
		$localTime = $localTime->setTimeZone($this->getLocalDateTimeZone());

		$localeRegions = $this->getLocaleRegions($locale);

		foreach ($localeRegions as $localeId) {

			$locale = Yii::app()->getLocale($localeId);

			// This assumes the locale has already been set
			// This returns the order of the date time combination, eg {1} {0}
			$localFormat = $locale->getDateTimeFormat();

			// Get the local date format - eg dd/MM/yyyy
			$dateFormat = $locale->getDateFormat($dateWidth);
			if ($this->forceLongYears()) { $dateFormat = $this->convertAllYearsIntoLongYears($dateFormat); }
			$localFormat = str_replace('{1}', /*$locale->getDateFormat($dateWidth)*/$dateFormat, $localFormat);

			// Get the local time format with seconds - eg hh:mm:ss
			$localFormat = str_replace('{0}', $locale->getTimeFormat($timeWidth), $localFormat);

			// Convert to php date format
			$localFormat = $this->YiitoPHPDateFormat($localFormat);

			$dateTimeFormats[$localeId] = $localTime->format($localFormat);
		}

		// select the distinct date formats
		$dropdown = array_unique($dateTimeFormats);

		return $dropdown;
	}

    /**
     * Returns the array of timezones
     */
    public function getTimezonesArray() {
        $timezoneIdentifiers = DateTimeZone::listIdentifiers();
        $utcTime = new DateTime('now', new DateTimeZone('GMT'));

        $tempTimezones = array();
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = array(
                'offset' => (int) $currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            );
        }

        // Sort the array by offset, identifier ascending
        usort($tempTimezones, function($a, $b) {
            return ($a['offset'] == $b['offset']) ? strcmp($a['identifier'], $b['identifier']) : $a['offset'] - $b['offset'];
        });

        $timezoneList = array();
        foreach ($tempTimezones as $tz) {
            $sign = ($tz['offset'] > 0) ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$tz['identifier']] = '(GMT ' . $sign . ' ' . $offset . ') ' . $tz['identifier'];
        }

        return $timezoneList;
    }

    /**
     * Returns the passed timezone (GMT+xxx) label
     * @param string $timezoneName The name of the timezone
     * @param bool $clean - if '(GMT ...)' should be omitted
     * @return string
     */
    public function getTimezoneOffset($timezoneName, $clean = false) {
        $utcTime = new DateTime('now', new DateTimeZone('GMT'));
        $timeName = ($timezoneName) ? $timezoneName : 'UTC' ;
        $currentTimezone = new DateTimeZone($timeName);
        $offset = (int) $currentTimezone->getOffset($utcTime);
        $sign = ($offset > 0) ? '+' : '-';
        $offset = gmdate('H:i', abs($offset));
        if($clean){
            return $sign . $offset;
        }else {
            return '(GMT ' . $sign . ' ' . $offset . ')';
        }
    }


	/**
	 * Returns an array of the available time zones and their corresponding
	 * offset from UTC in seconds
	 * @return array
	 */
	public function getTimezoneOffsetsSecondsArray(){
		$timezoneIdentifiers = DateTimeZone::listIdentifiers();
		$utcTime = new DateTime('now', new DateTimeZone('GMT'));

		$tempTimezones = array();
		foreach ($timezoneIdentifiers as $timezoneIdentifier) {
			$currentTimezone = new DateTimeZone($timezoneIdentifier);

			$tempTimezones[$timezoneIdentifier] = (int) $currentTimezone->getOffset($utcTime);
		}
		ksort($tempTimezones);
		return $tempTimezones;
	}
	/**
	 * Returns the offset of the passed timezone from UTC in seconds.
	 * Can be a positive or negative number depending on the time zone
	 * @param $timezoneName
	 * @return int
	 */
	public function getTimezoneOffsetSecondsFromUTC($timezoneName){
		$utcTime = new DateTime('now', new DateTimeZone('GMT'));
		$timeName = ($timezoneName) ? $timezoneName : 'UTC' ;
		$currentTimezone = new DateTimeZone($timeName);
		$offset = (int) $currentTimezone->getOffset($utcTime);
		return $offset;
	}

    /**
     * todo: refactor & optimize
     *
     * Converts date strings. User must specify the formatFrom correctly, matching the dateString to convert.
     * There is NO magic here, no automatic format recognition whatsoever! You MUST know the incoming format!
     *
     * @param string $dateString   "31-12-2013"
     * @param string $formatFrom  "d-m-Y"
     * @param string $formatTo	   "Y-m-d 00:00:00"   ==>  "2013-12-31 00:00:00"
     */
    public function convertDateString($dateString, $formatFrom, $formatTo) {
        $date = date_create_from_format($formatFrom, $dateString);
        if ($date == false) {
            // if the previous fail return false, if this is passed directly we receive an error
            Yii::log("error with convertDateString($dateString, $formatFrom, $formatTo)", 'error');
            $date = new DateTime(date("Y-m-d"));
        }
        return date_format($date, $formatTo);
    }


	/**
	 * todo: refactor & optimize
	 *
	 * @param      $seconds
	 * @param bool $shortVersion
	 *
	 * @return string
	 */
    public function hoursAndMinutes($seconds, $shortVersion = false)
    {
        if ($seconds < 30)
            return "{$seconds}s";

        $totalHours = floor($seconds / 3600);
        $totalMinutes = round(($seconds - (3600*$totalHours)) / 60);
	    if($shortVersion) {
		    if($totalMinutes>0){
			    return "{$totalHours}h{$totalMinutes}m";
		    }else{
			    return "{$totalHours}h";
		    }
	    }else {
		    return "{$totalHours}h {$totalMinutes}m";
	    }
    }

    /**
     * Returns the date difference in days between the 2 dates passed
     * Expects 2 dates in UTC format
     * @param $datetimeA
     * @param $datetimeB
     * @return float
     */
    public function countDayDiff($datetimeA, $datetimeB)
    {
        $timeA = strtotime($datetimeA);
        $timeB = strtotime($datetimeB);

        // Subtract these two numbers and divide by the number of seconds in a
        //  day. Round the result since crossing over a daylight savings time
        //  barrier will cause this time to be off by an hour or two.
        return round( abs( $timeA - $timeB ) / 86400 );
    }









    /**
     * Internal method to Add or Subsctract an interval to data/datetime
     *
     * @param string $dateTime UTC or Local formatted date/datetime e.g. 2014-01-21, 21/01/14, ...
     * @param string $interval See http://php.net/manual/en/class.dateinterval.php, Example: P10DT5H50S -> 10 days, 5 hours, 50 seconds
     * @param string $operation
     * @throws CException
     * @return string
     */
    protected function addOrSubInterval($dateTime, $interval, $operation) {

   		$formatId = $this->detectFormat($dateTime);

    	switch ($formatId) {
    		case self::FORMAT_UTC_DATE:
    		case self::FORMAT_UTC_DATETIME:
    			$dt = new DateTime($dateTime);
    			$dt->{$operation}(new DateInterval($interval));
    			$format = ($formatId == self::FORMAT_UTC_DATE) ? 'Y-m-d' : 'Y-m-d H:i:s';
    			$result = $dt->format($format);
    			return $result;
    			break;
    		case self::FORMAT_LOCAL_DATE:
    		case self::FORMAT_LOCAL_DATETIME:
    			$format = ($formatId == self::FORMAT_LOCAL_DATE) ? $this->getPHPLocalDateFormat() : $this->getPHPLocalDateTimeFormat();
    			$formatCreate = $format;
    			if ($formatId == self::FORMAT_LOCAL_DATE) {
    				$dateTime 		.= ' 00:00:00';
    				$formatCreate  	.= ' H:i:s';
    			}
    			$dt = DateTime::createFromFormat($formatCreate, $dateTime);
				if(!$dt){
				    $dateTime = Yii::app()->localtime->toLocalDateTime(Yii::app()->localtime->fromLocalDateTime($dateTime, 'short', 'short', 'Y-m-d H:i:s'), self::DEFAULT_DATE_WIDTH, self::DEFAULT_TIME_WIDTH, 'Y-m-d H:i:s' );
					$dt = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
				}

    			$dt->{$operation}(new DateInterval($interval));
    			$result = $dt->format($format);
    			return $result;
    			break;

    		default:
    			// Using unknown format to add/substract intervals is dangerous and may lead to unpredictable results. Throw an exception.
    			throw new CException('Error while adding an interval to a badly formatted date/datetime');
    			break;

    	}



    }



	/**
     * Add  interval to date or datetime strings.
     *
	 * @param string $dateTime date or datetime in UTC or LOCAL format, e.g. 2014-01-21, 21/01/14 10:00:00, ...
	 * @param string $interval See http://php.net/manual/en/class.dateinterval.php, Example: P10DT5H50S -> 10 days, 5 hours, 50 seconds
	 * @return string
	 */
    public function addInterval($dateTime, $interval) {
    	return $this->addOrSubInterval($dateTime, $interval, self::OPERATION_ADD);
    }



    /**
     * Substract interval from date or datetime
     *
	 * @param string $dateTime date or datetime in UTC or LOCAL format, e.g. 2014-01-21, 21/01/14 10:00:00, ...
	 * @param string $interval See http://php.net/manual/en/class.dateinterval.php, Example: P10DT5H50S -> 10 days, 5 hours, 50 seconds
	 * @return string
     */
    public function subInterval($dateTime, $interval) {
    	return $this->addOrSubInterval($dateTime, $interval, self::OPERATION_SUB);
    }


    /**
     * Return number of seconds or DateInterval object between 2 dates or datetimes.
     * To detect the relation between dates (which is first, which second), get the object and inspect the "invert" property.
     * If "invert"==0, the $dateTime1 is BEFORE $dateTime2 and if it is 1, it is the opposite case.
     *
     * @param string $dateTime1  Date or Datetime string, UTC or Local
     * @param string $dateTime2  Date or Datetime string, UTC or Local
     * @param boolean $returnObject  Whether to return number of seconds or DateInterval object
     *
     * @return string|DateInterval|boolean
     */
    public function diff($dateTime1, $dateTime2, $returnObject = false) {

    	// Detect both formats
    	$format1 = $this->detectFormat($dateTime1);
    	$format2 = $this->detectFormat($dateTime2);

    	// Convert them both to UTC
    	if (in_array($format1, array(self::FORMAT_LOCAL_DATE, self::FORMAT_LOCAL_DATETIME))) {
    		$dateTime1 = ($format1 == self::FORMAT_LOCAL_DATE) ? $this->fromLocalDate($dateTime1) : $this->fromLocalDateTime($dateTime1);
    	}

    	if (in_array($format2, array(self::FORMAT_LOCAL_DATE, self::FORMAT_LOCAL_DATETIME))) {
    		$dateTime2 = ($format2 == self::FORMAT_LOCAL_DATE) ? $this->fromLocalDate($dateTime2) : $this->fromLocalDateTime($dateTime2);
    	}

    	// We do not try to handle timezones here; let the DateTime detect it if it is there (+0000)
    	// If TZ is not in the string, we just compare the two datetimes, even if they came here in different format
    	// Like: 2014-01-31 00:00:00 === 30/01/14 00:00:00 (more precisely, the Local format)
    	$dt1 = new DateTime($dateTime1);
    	$dt2 = new DateTime($dateTime2);

    	$diff = $dt1->diff($dt2);

    	if ($diff instanceof DateInterval ) {
    		return ($returnObject) ? $diff : (($diff->invert ? -1 : 1) * ($diff->days*86400 + $diff->h*3600 + $diff->i*60 + $diff->s));
    	}

    	return false;
    }


  	/**
  	 * Compare two dates or datetimes. Analyzing DateInterval object
  	 *
  	 * @param string $dateTime1
  	 * @param string $dateTime2
  	 * @return string|boolean
  	 */
    protected function compare($dateTime1, $dateTime2) {

    	$diff = $this->diff($dateTime1, $dateTime2, true);
    	if ($diff === false) {
    		return $diff;
    	}

    	$seconds = $diff->days*86400 + $diff->h*3600 + $diff->i*60 + $diff->s;
    	if ($seconds == 0) {
    		return self::EQUAL;
    	}
    	else if ($diff->invert == 0) {
    		return self::LT;
    	}
		else if ($diff->invert == 1) {
    		return self::GT;
    	}

    	return false;
    }



    /**
     * Set of functions to compare date/datetimes
     *
     * @param string $dateTime1 UTC or Local formatted
     * @param string $dateTime2 UTC or Local formatted
     * @return boolean
     */
    public function isEqual($dateTime1, $dateTime2) {
    	return $this->compare($dateTime1, $dateTime2) == self::EQUAL;
    }

    public function isGt($dateTime1, $dateTime2) {
    	return $this->compare($dateTime1, $dateTime2) == self::GT;
    }

    public function isGte($dateTime1, $dateTime2) {
    	return in_array($this->compare($dateTime1, $dateTime2), array(self::EQUAL, self::GT));
    }

    public function isLt($dateTime1, $dateTime2) {
    	return $this->compare($dateTime1, $dateTime2) == self::LT;
    }

    public function isLte($dateTime1, $dateTime2) {
    	return in_array($this->compare($dateTime1, $dateTime2), array(self::EQUAL, self::LT));
    }


    /**
     * Try to detect the format of the passed date/datetime [string] : is it in UTC or LOCAL format.
     * Detects Yii format, not PHP core one.
     *
     * Note: Sometimes, LOCAL format could be Y-m-d H:i:s, i.e. much like UTC. I have no idea how to distinguish these 2.
     *
     * Expects date or datetime string, UTC or Local
     *
     * @param string $dateTime UTC or Local formatted
     * @return string|boolean
     */
    public function detectFormat($dateTime) {

    	if ($dateTime === '0000-00-00 00:00:00') {
    		return self::FORMAT_UTC_DATETIME;
    	}

    	if ($dateTime === '0000-00-00') {
    		return self::FORMAT_UTC_DATE;
    	}

    	// Just try to parse in different formats until succeed or until no more formats (failure)
    	// Pure UTC-like (well, UTC is not a format, but..)
    	$format = 'yyyy-MM-dd hh:mm:ss';
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_UTC_DATETIME;

    	// ISO + UTC
    	$format = 'yyyy-MM-dd?hh:mm:ss?0000';
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_UTC_DATETIME;

    	// ISO + <other time zone>
    	$format = 'yyyy-MM-dd?hh:mm:ss?????';
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_UTC_DATETIME;

    	// ~ISO, NO time zone
    	$format = 'yyyy-MM-dd?hh:mm:ss';
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_UTC_DATETIME;

    	// ISO Date only
    	$format = 'yyyy-MM-dd';
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_UTC_DATE;

    	// Local date
    	$format = $this->getLocalDateFormat();
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_LOCAL_DATE;

    	// Local datetime
    	$format = $this->getLocalDateTimeFormat();
    	if(CDateTimeParser::parse($dateTime,$format) !== FALSE)
    		return self::FORMAT_LOCAL_DATETIME;

    	// Warn in the log we are dealing with some unknown to us format
    	// Yii::log('Unable to detect formatId (UTC|LOCAL) of the passed date time: ' . $dateTime, CLogger::LEVEL_INFO);
    	return false;


    }

    /**
     * Inspect Yii locale time format and detect if hour is in 12 or 24 format
     *
     * @return string
     */
    public function hourFormat() {
    	$timeFormat = Yii::app()->localtime->getLocalDateTimeFormatLocale()->getTimeFormat();
    	if (preg_match('/a/', $timeFormat)) {
    		return self::HOUR_12;
    	}
    	else {
    		return self::HOUR_24;
    	}
    }

    /**
     * Return list of rounded hours, following locale, if not forced to specific hour format (00:00 .. 23:00 / 12:00 am .. 11:00 pm)
     *
     * @param string $hourFormat
     *
     * @return array array(0..23 => <hh::00 [am|pm]>)
     */
    public function hoursList($hourFormat = false) {

    	if (!$hourFormat) {
    		$hourFormat = $this->hourFormat();
    	}

    	$hours = array();
    	if ($hourFormat == self::HOUR_24) {
    		for ($i=0; $i<=23; $i++) {
    			$h = $i;
    			if (strlen($h) < 2) {
					$h = '0' . $h;
    			}
    			$h = $h . ":00";
    			$hours[$i] = $h;
    		}
    	}
    	else {
    		$hours[0] = '12:00 am';
    		for ($i=1; $i<=11; $i++) {
    			$h = $i;
    			if (strlen($h) < 2) {
    				$h = '0' . $h;
    			}
    			$h = $h . ':00 am';
    			$hours[$i] = $h;
    		}

    		$hours[12] = '12:00 pm';
    		for ($i=13; $i<=23; $i++) {
    			$h = $i-12;
    			if (strlen($h) < 2) {
    				$h = '0' . $h;
    			}
    			$h = $h . ':00 pm';
    			$hours[$i] = $h;
    		}
    	}

    	return $hours;

    }





	/**
	 * Given a date in local format, detect width
	 * @param $dateTime string a date in some local format
	 * @return bool|string
	 */
	public function detectLocalDateWidth($date) {

		$widths = array(self::SHORT, self::MEDIUM, self::LONG);

		foreach ($widths as $width) {
			$format = $this->getLocalDateFormat($width);
			//if(CDateTimeParser::parse($date, $format) !== FALSE) {
			if(DefaultDateTimeParser::parse($date, $format) !== FALSE) {
				return $width;
			}
		}

		// Warn in the log we are dealing with some incorrect date format
		Yii::log('Unable to detect width of the passed date: ' . $date, '; warning: probably the date format is incorrect');
		return false;
	}

	/**
	 * Used in notifications
	 * Get timezone and datetime format from the settings for specific user
	 * Returns a date/time combination based on the user locale for specific user
	 * Expects a date/time in the yyyy-mm-dd hh:mm:ss type format
	 * @param $userId
	 * @param $serverTimestamp
	 * @return string
	 */
	public function toUserLocalDatetime($userId, $serverTimestamp) {
		//if the date is already formatted, return it
		if(!CDateTimeParser::parse($serverTimestamp,'yyyy-MM-dd hh:mm:ss'))
			return $serverTimestamp;
		//find the time zone for this user
		$timezone = $this->getTimezoneFromSettings($userId);
		$localTime = new DateTime($serverTimestamp, new DateTimeZone(self::_utc));
		//set user timezone
		$localTime->setTimezone(new DateTimeZone($timezone));
		//find the datetime format for this user
		$localFormat = $this->getDateFormatFromSettings($userId);

		// Return as a local datetime
		return $localTime->format($localFormat);
	}

    /**
     * Used in Background jobs
     * Get the user timezone and convert it to UTC format/timezone
     *
     * @param $userId integer User idst
     * @param $serverTimestamp string Datetime string
     * @return string UTC datetime format
     */
    public function fromUserLocalDatetime($userId, $serverTimestamp){
        //if the date is already formatted, return it
        if(CDateTimeParser::parse($serverTimestamp,'yyyy-MM-dd hh:mm:ss'))
            return $serverTimestamp;
        //find the time zone for this user
        $timezone = $this->getTimezoneFromSettings($userId);
        $localTime = new DateTime($serverTimestamp, new DateTimeZone($timezone));
        //set user timezone
        $localTime->setTimezone(new DateTimeZone(self::_utc));

        // Return as a local datetime
        return $localTime->format('Y-m-d H:i:s');
    }

	/**
	 * Returns the timezone for the specific user
	 * according to the datetime settings
	 * @param $userId
	 * @return string
	 */
	public function getTimezoneFromSettings($userId=false) {
		$timezone = Settings::get('timezone_default', 'UTC');

		if (!$userId) {
		    return $timezone;
		}

		if ('on' === Settings::get('timezone_allow_user_override', 'off')) {
			$userTimezoneSetting = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => $userId,
				'path_name' => 'timezone'
			));
			if($userTimezoneSetting) {
				$timezone = $userTimezoneSetting->value;
			}
		}

		return $timezone;
	}

	/**
	 * Returns the locale for the specific user
	 * according to the datetime settings
	 * @param $userId
	 * @return mixed
	 */
	public function findUserLocale($userId) {
		//default locale id
		$localeId = 'en';
		$dateFormatType = Settings::get('date_format');

		if ($dateFormatType == 'browser_code') {
			// use default locale id, because there is no logged user
		}
		else if ($dateFormatType == 'user_selected') {
			$userDateFormatSetting = CoreSettingUser::model()->findByAttributes(array(
				'id_user' => $userId,
				'path_name' => 'date_format'
			));
			if(!$userDateFormatSetting || empty($userDateFormatSetting->value)) {
				$userDateFormatSetting = CoreSettingUser::model()->findByAttributes(array(
					'id_user' => $userId,
					'path_name' => 'date_format_locale'
				));
			}
			if($userDateFormatSetting && !empty($userDateFormatSetting->value)) {
				$localeId = $userDateFormatSetting->value;
			}
		}
		else {
			// the superadmin has forced a date format
			$localeId = Settings::get('date_format_custom');
		}

		$locale = Yii::app()->getLocale($localeId);

		return $locale;
	}

	/**
	 * Returns the datetime format for the specific user
	 * according to the datetime settings
	 * @param $userId
	 * @return mixed
	 */
	public function getDateFormatFromSettings($userId) {
		//find the date format for this user
		$locale = $this->findUserLocale($userId);
		// This returns the order of the date time combination, eg {1} {0}
		$localFormat = $locale->getDateTimeFormat();
		// Get the local date format - eg dd/MM/yyyy
		$dateFormat = $locale->getDateFormat(LocalTime::DEFAULT_DATE_WIDTH);
		if ($this->forceLongYears()) { $dateFormat = $this->convertAllYearsIntoLongYears($dateFormat); }
		$localFormat = str_replace('{1}', /*$locale->getDateFormat(LocalTime::DEFAULT_DATE_WIDTH)*/$dateFormat, $localFormat);
		// Get the local time format with seconds - eg hh:mm:ss
		$localFormat = str_replace('{0}', $locale->getTimeFormat(LocalTime::DEFAULT_TIME_WIDTH), $localFormat);
		//converts the yii date format to php date format
		$localFormat = Yii::app()->localtime->YiitoPHPDateFormat($localFormat);

		return $localFormat;
	}



	/**
	 * Returns if the date is in daylight savings for the logged user's timezone.
	 * @param $dateTime
	 * @return string
	 */
	public function inDaylightSavings($dateTime){

		$userId = false;
		if(!(Yii::app() instanceof CConsoleApplication)) {
			$userId = isset( Yii::app()->user ) ? Yii::app()->user->getIdst() : FALSE;
		}

		$timeZone = Yii::app()->localtime->getTimezoneFromSettings($userId);
		$date = new DateTime($dateTime.' '. $timeZone);

		return $date->format('I');
	}



	/**
	 * This will tell if we have to force long year format in dates or not
	 * @return bool
	 */
	public function forceLongYears() {
		return true; //TODO: adopt a setting or whatever to make this optional
	}



	/**
	 * Change "short" years (yy) into "long" years (yyyy) in a date format string
	 * @param $dateFormat a date format (in Yii format)
	 * @return bool|string formatted date (false if error)
	 */
	public function convertAllYearsIntoLongYears($dateFormat) {

		//validate input
		if (!is_string($dateFormat)) { return false; }

		//prepare internal vars
		$length = strlen($dateFormat);
		if ($length < 2) { return $dateFormat; } //empty string or too short string (to contain a "yy" at least 2 chars are needed)

		//input analysis
		$output = '';
		for ($i=0; $i < $length; $i++) {
			if ($dateFormat{$i} == 'y') {
				//possibly a year
				if ((($i+1) < $length) && $dateFormat{$i+1} == 'y') {
					//we have found a year. Check if it is a "long" year (yyyy) ar a "short" one (yy)
					if (((($i+2) >= $length) || ((($i+2) < $length) && $dateFormat{$i+2} != 'y')) && (($i-1 < 0) || ($i-1 >= 0 && $dateFormat{$i-1} != 'y'))) {
						//we have found a "short" year (yy)
						$output .= 'yyyy'; //force the "long" year
					} else {
						$output .= 'yy';
					}
					$i++; //no need to check next index in the loop
				} else {
					$output .= 'y';
				}
			} else {
				$output .= $dateFormat{$i}; //just copy the character in the output string
			}
		}

		return $output;
	}

    /**
     * @param $localTime
     * @return bool|string formatted date (false if error)
     */
    public function getPHPTimestamp($localTime) {

        $utcStartDate = str_replace(array("T", "+0000"), array(" ", ""), $this->fromLocalDateTime($localTime));
        return strtotime($utcStartDate." UTC");
    }

    /**
     * Convert the formatted time on the coupon page to a Y-m-d format, regardless of the applied date formatting
     * @param $origDate string The formatted datestring
     * @param $origFormat string applied date format (eg. d-m-Y, Y-n-j, etc)
     * @return string The re-formatted string
     */
    public function convertCouponDate($origDate, $origFormat){
        $i = 0;
        $copyDate = $origDate;
        $copyFormat = $origFormat;
        $arDateParts = array();
        $arFormatParts = array();
        $arDateFunctionLetters = array('d', 'j', 'm', 'n', 'o', 'y', 'Y');
        $y = "";
        $m = "";
        $d = "";
        while(strlen($copyDate) >= 1){
            $char = substr($copyDate, 0, 1);
            $copyDate = substr($copyDate, 1);
            if(is_numeric($char)){
                if(isset($arDateParts[$i])){
                    $arDateParts[$i] .= $char;
                } else {
                    $arDateParts[$i] = $char;
                }
            } else {
                $i++;
            }
        }
        while(strlen($copyFormat) >= 1){
            $char = substr($copyFormat, 0, 1);
            $copyFormat = substr($copyFormat, 1);
            if(in_array($char, $arDateFunctionLetters)){
                $arFormatParts[] = $char;
            }
        }

        if(count($arFormatParts) != 3) return $origDate;

        foreach($arFormatParts as $x => $f){
            switch($f){
                case 'd':
                case 'j':
                    $d = str_pad($arDateParts[$x], 2, '0', STR_PAD_LEFT);
                    break;
                case 'm':
                case 'n':
                    $m = str_pad($arDateParts[$x], 2, '0', STR_PAD_LEFT);
                    break;
                case 'o':
                case 'y':
                case 'Y':
                    $y = $arDateParts[$x];
                    break;
            }
        }

        return $y."-".$m."-".$d;
    }

    /**
     * Round the give date time string to the closest hour.
     *
     * Example: 2016-01-07 13:05:10 equals to 2016-01-07 13:00:00
     *          2016-01-07 13:45:10 equals to 2016-01-07 14:00:00
     *
     *
     * @param null $dateTime date time string
     * @return string Rounded dateTime string
     */
    public function roundToClosestHour($dateTime = null){
        if(!$dateTime){
            $dateTime = $this->getUTCNow('Y-m-d H:i:s');
        }

        $date = new DateTime($dateTime);

        $minutes = $date->format('i');

        if($minutes > 30){
            $date->modify('+ 1 hour');
        }

        return $date->format('Y-m-d H:00:00');
    }
}
