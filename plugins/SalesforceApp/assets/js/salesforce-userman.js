(function($){
	$(document).on('click', '#fancytree-orgchart-wrapper .fancytree-title', function(){
		if($(this).css('color') == "rgb(4, 147, 215)"){ // light blue = salesforce
			$("#userman-users-massive-action option[value='activate']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='deactivate']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='assign_node_append']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='assign_node_move']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='assign_node_remove']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='edit']").attr("disabled","disabled");
			$("#userman-users-massive-action option[value='delete']").attr("disabled","disabled");
		} else {
			$("#userman-users-massive-action option[value='activate']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='deactivate']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='assign_node_append']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='assign_node_move']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='assign_node_remove']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='edit']").removeAttr("disabled");
			$("#userman-users-massive-action option[value='delete']").removeAttr("disabled");
		}
	});

	$(document).on('ready', function(){
		if($('#fancytree-orgchart-wrapper').length > 0) { // only exist if wrapper exists
			var countPreviousFancyTreeTitles = 0;
			var loops = 0;
			var extraLoops = 0;
			var keepGoing = setInterval(function () {
				loops++;
				var countCurrentFancyTreeTitles = $('#fancytree-orgchart-wrapper .fancytree-title').length;
				var activeBranch = $('#fancytree-orgchart-wrapper .fancytree-active .fancytree-title');
				if ((countCurrentFancyTreeTitles > 0) && (countCurrentFancyTreeTitles == countPreviousFancyTreeTitles)) {
					extraLoops++;
					if ($(activeBranch).css('color') == "rgb(4, 147, 215)") {
						$("#userman-users-massive-action option[value='activate']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='deactivate']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='assign_node_append']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='assign_node_move']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='assign_node_remove']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='edit']").attr("disabled", "disabled");
						$("#userman-users-massive-action option[value='delete']").attr("disabled", "disabled");
					} else {
						$("#userman-users-massive-action option[value='activate']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='deactivate']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='assign_node_append']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='assign_node_move']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='assign_node_remove']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='edit']").removeAttr("disabled");
						$("#userman-users-massive-action option[value='delete']").removeAttr("disabled");
					}
					if (extraLoops > 8) { // allowing 8 more loops to be performed, because active branch doesn't always seem to be set yet
						clearInterval(keepGoing);
					}
				}
				if (loops > 20 && countCurrentFancyTreeTitles == 0) {  // extra safety --> if after 20 loops, not a single title has been found, abort process
					clearInterval(keepGoing);
				}
				countPreviousFancyTreeTitles = countCurrentFancyTreeTitles;
			}, 500);
		}
	})
}) (jQuery);