var SalesforceApp = false;
var jwizard = false;

(function($){
    
    /*
     * Constructor
     */
    SalesforceApp = function(options){
        this.defaultOptions = {
            debug: true,
        };
        
        if(typeof options === "undefined"){
            var options = {};
        }
        
        this.init(options);
    };
    
    /**
     * Prototype
     */
    SalesforceApp.prototype = {
        init: function(options){
          this.options = $.extend({}, this.defaultOptions, options);
          this.debug('Initilize SalesforceApp');
          this.addListeners();
        },
        
        debug: function(message){
            if(this.options.debug === true){
                console.log('SalesforceApp: ', message);
            }
        },
        
        initWizard: function(){
            this.debug('Initializing Wizard');
          // This is just an example how to set jWizard Dispatcher URL (IF you want! It is optional), depending on which <a> is clicked
            $(".wizard-starter").on("click", function(){
                if ( typeof $(this).data("wizard-dispatcher-url") == "string" ) {
                    jwizard.options.dispatcherUrl = $(this).data("wizard-dispatcher-url");
                }
            });



            // Initialize jWizard class and create a global variable "jwizard". Give the wizard the ID of the opening dialog (the dialog)
            // NOTE: As of this writing the global variable MUST be named "jwizard"
            var options = {
                dialogId: "wizard-demo",    // Must be equal to the  a.open-dialog's  'rel' attribute! See above
                // dispatcherUrl = "/lms/?r=test2/axWizardFinish"  // <--- you can set the dispatching URL directly here, upon jWizard initialization
            };
            jwizard = new jwizardClass(options);  
        },
        
        addListeners: function(){
            this.debug('Add Listeners');
            this.initWizard();
          
            $(document).on('change', 'input[id=user_type_contact]', function(){                
                if($(this).is(':checked')){                    
                    $('#user_type_lead-styler').removeClass('disabled');
                } else{                    
                    $('#user_type_lead-styler').addClass('disabled');
                }
            });
            
            $(document).on('change', 'input[type=radio]', function(){                
                if($(this).val() == 1){
                    $('#sales_order').removeClass('hidden');
                } else{
                    $('#sales_order').addClass('hidden');
                }
            });

            $(document).on('click', '#step1-submit_btn', function(){
                    var errorFlag       = false;
                    var apiAgent        = $('#api-agent');
                    var inputPassword   = $('#inputPassword');
                    var inputToken      = $('#inputToken');

               if ( apiAgent.val().length < 1 && apiAgent.prev('span').html() == '') {
                   apiAgent.prev('span.error').html('Empty Api Agent!'); errorFlag = true;
               }
                if ( inputPassword.val().length < 1 && inputPassword.prev('span').html() == '') {
                    inputPassword.prev('span.error').html('Empty Password!'); errorFlag = true;
               }
                if ( inputToken.val().length < 1 && inputToken.prev('span').html() == '') {
                    inputToken.prev('span.error').html('Empty Token!'); errorFlag = true;
               }

                if (errorFlag) { return false; } // If one of the above fields ( agent, pass, token ) is empty return false;
            });
            
            $(document).on('keyup', '.step1-input', function() {
               if ( $(this).prev('span.error').html() != '' ) {
                   $(this).prev('span.error').html('');
               }
            });
        },
    };
}) (jQuery);