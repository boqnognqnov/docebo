<?php
/**
 * 
 * 
 *
 */
class SalesforceAppModule extends CWebModule
{

	/**
	 * URL to this module's assets (web accessible)
	 *
	 * @var string
	 */
	protected $_assetsUrl = null;



	public function init()
	{

		// import the module-level models and components
		$this->setImport(array(
			'SalesforceApp.models.*',
			'SalesforceApp.components.*',
			//'SalesforceApp.soapclient.*', //see below
		));

		//in some environemnts, Yii seems unable to load certain libraries, so we try to do it manually
		spl_autoload_unregister(array('YiiBase','autoload')); // temporary disable Yii autoloader
		$list = glob(dirname(__FILE__).'/soapclient/*.php');
		foreach ($list as $file) {
			require_once($file);
		}
		spl_autoload_register(array('YiiBase','autoload')); // re-enable Yii autoloader

		if(!(Yii::app() instanceof CConsoleApplication)) {
			Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/../plugins/SalesforceApp/assets/css/salesforce.css');
			Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/../plugins/SalesforceApp/assets/js/salesforce-userman.js');

			//if (!Yii::app()->request->isAjaxRequest) {
			//	Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/../plugins/SalesforceApp/assets/css/salesforce.css');
			//	Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/../plugins/SalesforceApp/assets/js/salesforce-userman.js');
			//}
		}

		// Listen for various events
		Yii::app()->event->on(EventManager::EVENT_USER_SUBSCRIBED_COURSE,                     array($this, 'onUserSubscribedCourse'));
		Yii::app()->event->on(EventManager::EVENT_USER_UNSUBSCRIBED,                          array($this, 'onUserUnsubscribed'));

		// These 2 events have the same meaning: final user enrollment into a session
		Yii::app()->event->on(EventManager::EVENT_USER_ENROLLED_IN_CLASSROOM_SESSION,         array($this, 'onILTSessionUserEnrolled'));
		Yii::app()->event->on(EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED,    array($this, 'onILTSessionUserEnrolled'));

		// These 2 events have the same meaning: final user enrollment into a session
		Yii::app()->event->on(EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION,           array($this, 'onUserEnrolledInWebinarSession'));
		Yii::app()->event->on(EventManager::EVENT_USER_WEBINAR_SESSION_ENROLL_ACTIVATED,      array($this, 'onUserEnrolledInWebinarSession'));

		Yii::app()->event->on(EventManager::EVENT_USER_ENROLLED_IN_LEARNING_PLAN,             array($this, 'onUserEnrolledInLearningPlan'));
		Yii::app()->event->on(EventManager::EVENT_LEARNING_PLAN_UPDATED,                      array($this, 'onLearningPlanUpdated'));
		Yii::app()->event->on('CourseAddedToCoursepath',                                      array($this, 'onCourseAddedToLearningPlan'));
		Yii::app()->event->on('CourseRemovedFromCoursepath',                                  array($this, 'onCourseRemovedFromLearningPlan'));
		Yii::app()->event->on('AfterSaveLo',                                                  array($this, 'onLearningObjectAddedToCourse'));
		Yii::app()->event->on('AfterDeleteLo',                                                array($this, 'onLearningObjectDeletedFromCourse'));
		Yii::app()->event->on(EventManager::EVENT_USER_UNENROLLED_FROM_LEARNING_PLAN,         array($this, 'onUserUnEnrolledFromLearningPlan'));

		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_LO,                       array($this, 'onStudentCompletedLO'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_WEBINAR_SESSION,          array($this, 'onStudentCompletedWebinarSession'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_ILT_SESSION,              array($this, 'onStudentCompletedILTSession'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_COURSE,                   array($this, 'onStudentCompletedCourse'));
		Yii::app()->event->on(EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN,            array($this, 'onStudentCompletedLearningPlan'));

		Yii::app()->event->on(EventManager::EVENT_BEFORE_CREATE_ORGCHART_TREE,                array($this, 'onBeforeCreateOrgchartTree'));

		if (SalesforceLmsAccount::getFirstAccount() != null) {
			Yii::app()->event->on(EventManager::EVENT_NEW_COURSE, array($this, 'onNewCourseCreated'));
			Yii::app()->event->on(EventManager::EVENT_COURSE_PROP_CHANGED, array($this, 'onCoursePropertiesChanged'));
			Yii::app()->event->on('BeforeCourseDeleted', array($this, 'beforeCourseDeleted'));
			Yii::app()->event->on('BeforeCourseDeleted', array($this, 'onCourseDeleted'));

			Yii::app()->event->on(EventManager::EVENT_ADDED_LEARNING_COURSEPATH, array($this, 'onLearningPathCreated'));
			Yii::app()->event->on(EventManager::EVENT_UPDATED_LEARNING_COURSEPATH, array($this, 'onLearningPathUpdated'));
			//Yii::app()->event->on('NewContentInLearningPlan',									  array($this, 'onCourseOrPlanCreatedOrUpdated'));
			//Yii::app()->event->on('CourseRemovedFromCoursepath',								  array($this, 'onCourseOrPlanCreatedOrUpdated'));
			Yii::app()->event->on('BeforeCoursepathDeleted', array($this, 'beforeLearningPathDeleted'));
			Yii::app()->event->on('BeforeCoursepathDeleted', array($this, 'onLearningPathDeleted'));
		}
	}

	public function onNewCourseCreated(DEvent $event)
	{
		$this->onCourseOrPlanCreatedOrUpdated($event);
	}

	public function onCoursePropertiesChanged(DEvent $event)
	{
		$this->onCourseOrPlanCreatedOrUpdated($event);
	}

	public function onCourseDeleted(DEvent $event)
	{
		$this->onCourseOrPlanDeleted($event);
	}

	public function onLearningPathCreated(DEvent $event)
	{
		$this->onCourseOrPlanCreatedOrUpdated($event);
	}

	public function onLearningPathUpdated(DEvent $event)
	{
		$this->onCourseOrPlanCreatedOrUpdated($event);
	}

	public function onLearningPathDeleted(DEvent $event)
	{
		$this->onCourseOrPlanDeleted($event);
	}

	public function beforeCourseDeleted($event)
	{
		$sfAgent = new SalesforceSyncAgent();
		$sfAgent->deleteRelatedEnrollments($event->sender, SalesforceApiClient::ITEM_TYPE_COURSE);
	}

	public function beforeLearningPathDeleted($event){
		$sfAgent = new SalesforceSyncAgent();
		$sfAgent->deleteRelatedEnrollments($event->sender, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
	}

	public function onCourseOrPlanCreatedOrUpdated(DEvent $event)
	{
		$sfAgent = new SalesforceSyncAgent();
		if(empty($sfAgent->getApiError())) {
			if ($event->sender == null) {
				if (!($event->sender instanceof LearningCoursepath))
					$event->sender = $event->getParamValue('learningPlan', null);
				if (!($event->sender instanceof LearningCoursepath))
					$event->sender = $event->getParamValue('coursepath', null);
			}
			$sfAgent->saveCourseOrPlan($event->sender);
		} else {
			Yii::log('Salesforce error: '.$sfAgent->getApiError());
		}
	}

	public function onCourseOrPlanDeleted(DEvent $event)
	{
		$sfAgent = new SalesforceSyncAgent();
		$sfAgent->deleteCourseOrPlan($event->sender);
	}

	public function onStudentCompletedWebinarSession(DEvent $event){
		$this->onStudentCompletedWebinarOrILTSession($event);
	}

	public function onStudentCompletedILTSession(DEvent $event){
		$this->onStudentCompletedWebinarOrILTSession($event);
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() 
	{
		Yii::app()->mainmenu->addAppMenu(
            'salesforce',
            array(
                'icon' => 'admin-ico salesforce',
                'link' => Docebo::createAdminUrl('//SalesforceApp/SalesforceApp/index'),
                'label' => 'Salesforce',
			    //'settings' => Docebo::createAdminUrl('//SalesforceApp/SalesforceApp/settings'),
            ), ''
        );
    }

    /**
     * 
     */
    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('SalesforceApp.assets'));
        }
        return $this->_assetsUrl;
    }

    /**
     * @see CWebModule::beforeControllerAction()
     */
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	
	/**
	 * Check if LMS user is a salesforce one, also filtering by type (User, Lead, Contact, ...) 
	 * 
	 * @param integer $idUser
	 * @param string $type
	 * 
	 * @return boolean|SalesforceUser
	 */
	protected function isSalesforceUser($idUser, $type=false) {
		
		if ($type === false)
			$findArray = array('id_user_lms' => $idUser);
		else 
			$findArray = array('id_user_lms' => $idUser, 'sf_type' => $type);
		
		$sfUser = SalesforceUser::model()->findByAttributes($findArray);
		
		if (!$sfUser) return false;
		
		return $sfUser;
	}
	

	/**
	 * Convert LMS-based Salesforce user from LEAD to CONTACT (including calling SF API)
	 * 
	 * @param SalesforceApiClient $api
	 * @param SalesforceUser $sfUser
	 * @param SalesforceLmsAccount $sfLmsAccount
	 */
	protected function convertLeadToContact(SalesforceApiClient $api, SalesforceUser $sfUser, SalesforceLmsAccount $sfLmsAccount) {
		
		Yii::log('Converting a Lead to contact: LMS User ID: ' .$sfUser->id_user_lms, CLogger::LEVEL_INFO, __CLASS__);
		Yii::log('Salesforce Lead ID: ' . $sfUser->id_user_sf, CLogger::LEVEL_INFO, __CLASS__);
		
		$newContactId = $api->convertLeadToContact($sfUser->id_user_sf);
		Yii::log('New Salesforce Contact ID: ' . $newContactId, CLogger::LEVEL_INFO, __CLASS__);
		
		if (!$newContactId) {
		    Yii::log('Unable to convert Lead to a Contact', CLogger::LEVEL_ERROR, __CLASS__);
		    Yii::log(var_export($api->getConnection()->getLastResponse(), true), CLogger::LEVEL_ERROR, __CLASS__);
		    return false;
		}
		
		$sfUser->id_user_sf = $newContactId;
		$sfUser->sf_type = SalesforceUser::USER_TYPE_CONTACT;
		$sfUser->save(false);

		// Get the idOrg of the CONTACT branch for the Salesforce
		$salesforceOrgchart = SalesforceOrgchart::model()->findByAttributes(array(
			'sf_lms_id'     => $sfLmsAccount->id_account,
			'sf_type'       => 'contact'
		));
		
		$criteria = new CDbCriteria();
		$criteria->condition = 'translation = :dummyAcc';
		$criteria->order = 'id_dir DESC';
		$criteria->params['dummyAcc'] = SalesforceApiClient::DUMMY_ACCOUNT;
		$criteria->limit = 1;
		
		$accoutDummyOrgId = CoreOrgChart::model()->find($criteria);			
		
		// Get the idOrg of the LEAD branch
		$salesforceOrgchartLead = SalesforceOrgchart::model()->findByAttributes(array(
			'sf_lms_id'     => $sfLmsAccount->id_account,
			'sf_type'       => 'lead'
		));

		// Error check
		if (!$accoutDummyOrgId || !$salesforceOrgchartLead) return false;
		
		$contactIdOrg 	= $accoutDummyOrgId->id_dir;
		$leadIdOrg 		= $salesforceOrgchartLead->id_org;
		 
		CoreOrgChartTree::moveUserToNode($sfUser->id_user_lms, $leadIdOrg, $contactIdOrg );

		return true;
		
	}
	

	/**
	 * Call SF API to issue a sales order, if course/plan is for sale and other checks
	 * 
	 * @param SalesforceApiClient $api
	 * @param LearningCourse $course
	 * @param LearningCoursepath $plan
	 * @param string $itemType  (course or plan)
	 * @param CoreUser $user
	 * @param SalesforceUser $sfUser
	 * @param SalesforceLmsAccount $sfLmsAccount
	 */
	protected function issueSalesOrder($api, $course, $plan, $itemType, $user, $sfUser, $sfLmsAccount) {

		//Yii::log('Salesforce selling - 2: '. $course->selling );
		//Yii::log('Salesforce replicate_courses_as_products - 2: '. $sfLmsAccount->replicate_courses_as_products );

		$orderInformation = array();

		//check if contract exists, if not --> create it
		$contract = $api->getValidAccountContract($sfUser->account_id);
		$contractId = null;

		if(!$contract){
			//Create new Contract
			$response = $api->createAccountContract($sfUser->account_id);
			$contractId = $response[0]->id;
		} else {
			$contractId = $contract;
		}

		//Save the contract ID in SalesforceOrgchart
		Yii::app()->db->createCommand()
			->update(SalesforceAccounts::model()->tableName(), array(
				'contract_id' => $contractId
			), 'lmsAccount = :lmsId AND accountId = :accountId', array(
				':lmsId' => $sfLmsAccount->id_account,
				':accountId' => $sfUser->account_id
			));
		
		if ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) {
			if ( ($course->selling) && $sfLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) {
				//Yii::log('Salesforce selling: '. $course->selling );
				//Yii::log('Salesforce replicate_courses_as_products: '. $sfLmsAccount->replicate_courses_as_products );

				$tmpInfo = $api->issueSalesOrder($course, SalesforceApiClient::ITEM_TYPE_COURSE, $user, $sfUser);
			}
		}
		else if ($itemType == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN) {
			if ( ($plan->is_selling) && $sfLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) {
				$tmpInfo = $api->issueSalesOrder($plan, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, $user, $sfUser);
			}
		}

		if (!empty($tmpInfo)) {
			$orderInformation['productId'] = $tmpInfo['productId'];
			$orderInformation['orderId']	 = $tmpInfo['orderId'];
		}
		
		return $orderInformation;
	}


	/**
	 * Multifunctional handler: for Course & Learning Plan Enrollment events
	 *
	 * @param DEvent $event
	 * @param string $itemType (course, plan, etc.)
	 */
	protected function onUserEnrolledToCourseOrPlan(DEvent $event, $itemType)
	{

		/* @var $sfUser SalesforceUser */
		/* @var $sfLmsAccount SalesforceLmsAccount */
		/* @var $enrollment LearningCourseuser */
		/* @var $course LearningCourse */
		/* @var $plan LearningCoursepath */
		$eventParams = $event->params;

		if ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) {
			if (!$eventParams['course'] || !$eventParams['user']) return;
			$course = $eventParams['course'];
			$user = $eventParams['user'];
			$selling = $course->selling;

			// Do not act on webinars and classrooms COURSES
			if (in_array($course->course_type, array(LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR))) return;

			// Skip if this course enrolling is ran as part of LearningPlan courses enrollment (cycle)
			if (isset($eventParams['learningPlanContext']) && $eventParams['learningPlanContext']) return;
		} else if ($itemType == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN) {
			if (!$eventParams['learning_plan'] || !$eventParams['user_id']) return;
			$plan = $eventParams['learning_plan'];
			$user = CoreUser::model()->findByPk($eventParams['user_id']);
			$selling = $plan->is_selling;
		} else {
			return;
		}

		$idUser = $user->idst;

		// Check: user is SF and SF account is set up
		$sfUser = $this->isSalesforceUser($idUser);
		if (empty($sfUser)) { return; }
		$sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id);
		if (empty($sfLmsAccount)) { return; }

		try {
				$api = new SalesforceApiClient($sfLmsAccount);

				if ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) {
					/* @var $enrollment LearningCourseuser */
					$enrollment = LearningCourseuser::model()->findByAttributes(array(
						'idCourse' => $course->idCourse,
						'idUser' => $user->idst,
					));
				} else {
					/* @var $enrollment LearningCoursepathUser */
					$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
						'id_path' => $plan->id_path,
						'idUser' => $user->idst,
					));
				}

				$enrollments = array($enrollment);

				$additionalInfo = array();

				if ($sfUser->sf_type == SalesforceUser::USER_TYPE_CONTACT) {
					//Yii::log('Salesforce selling: '. $course->selling );
					//Yii::log('Salesforce replicate_courses_as_products: '. $sfLmsAccount->replicate_courses_as_products );

					if ( ($selling) && $sfLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) {
						$orderInformation = $this->issueSalesOrder($api, $course, $plan, $itemType, $user, $sfUser, $sfLmsAccount);
						$additionalInfo = CMap::mergeArray($additionalInfo, $orderInformation);
					}
				}


				// Create TASKS for USER type
				if ($sfUser->sf_type == SalesforceUser::USER_TYPE_USER) {
					if ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) {
						$taskId = $api->createTaskFromCourse($enrollment, $sfUser);
					} else {
						$taskId = $api->createTaskFromLearningPlan($enrollment, $sfUser);
					}

					if (is_string($taskId)) {
						$additionalInfo['taskId'] = $taskId;
					}

					$sfTaskId = $taskId;
					Yii::app()->db->createCommand(
						'insert into salesforce_enrollment_sf_task
							(path_id, course_id, user_id, salesforce_task_id)
						 values
						 	(:path_id, :course_id, :user_id, :salesforce_task_id)'
					)->query(
						array(
							":path_id" => ($itemType == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN) ? $plan->id_path : 0,
							":course_id" => ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) ? $course->idCourse : 0,
							":user_id" => $user->idst,
							":salesforce_task_id" => $sfTaskId
						)
					);

				}

				if($itemType == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN){
					$completion = $this->fetchLearningPathCompletion($plan->id_path, $user->idst);
					if($completion){
						$additionalInfo['path_completion'] = $completion;
					}
					$additionalInfo['skip_fa_la_date_creation'] = true;
				}

				// Always:  Update Enrollment custom object
				$response = $api->createOrUpdateEnrollments($enrollments, $itemType, $additionalInfo);

				Yii::log(var_export($response, true), CLogger::LEVEL_INFO);

				// Create CO2 items for each course related to a LP:
				if($itemType == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN){
					$courseIds = Yii::app()->db->createCommand()
						->select('c.idCourse')
						->from(LearningCoursepathCourses::model()->tableName().' p')
						->join(LearningCourse::model()->tableName().' c', 'p.id_item = c.idCourse')
						->where('p.id_path = :idPath AND c.course_type = :courseType', array(
							':idPath' => $plan->id_path,
							':courseType' => LearningCourse::TYPE_ELEARNING
						))
						->queryAll();

					foreach($courseIds as $courseId){
						$enrollment = LearningCourseuser::model()->findByAttributes(array(
							'idCourse' => $courseId['idCourse'],
							'idUser' => $user->idst,
						));
						$enrollments = array($enrollment);
						$additionalInfo = array();

						if ($sfUser->sf_type == SalesforceUser::USER_TYPE_USER) {
							$taskId = $api->createTaskFromCourse($enrollment, $sfUser);

							if (is_string($taskId)) {
								$additionalInfo['taskId'] = $taskId;
							}

							$sfTaskId = $taskId;
							Yii::app()->db->createCommand(
								'insert into salesforce_enrollment_sf_task
									(path_id, course_id, user_id, salesforce_task_id)
								 values
									(:path_id, :course_id, :user_id, :salesforce_task_id)'
									)->query(
								array(
									":path_id" => $plan->id_path,
									":course_id" => $courseId['idCourse'],
									":user_id" => $user->idst,
									":salesforce_task_id" => $sfTaskId
								)
							);

						}
						$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, $additionalInfo);

						Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
					}
				}
			} catch (Exception $e) {
				Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				if ($itemType == SalesforceApiClient::ITEM_TYPE_COURSE) {
					$details['Course ID'] = $course->idCourse;
				} else {
					$details['Plan ID'] = $plan->id_path;
				}
				$details['User ID'] = $user->idst;
				Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
			}
	     
	}

	/**
	 * Retrieves the completion of a learning path for Salesforce users
	 *
	 * @param int $learningPlanId
	 * @param int $lmsUserId
	 * @return string eg. 5/7
	 */
	public function fetchLearningPathCompletion($learningPlanId, $lmsUserId){
		$completion = Yii::app()->db->createCommand("
            SELECT (
                SELECT count(lpc.id_path)
                FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
                WHERE lpc.id_path = lpu.id_path
            ) AS path_items_total, (
                SELECT count(lcu.idCourse)
                FROM ".LearningCourseuser::model()->tableName()." AS lcu
                WHERE lcu.idUser = lpu.idUser AND lcu.status = ".LearningCourseuser::$COURSE_USER_END." AND lcu.idCourse IN(
                    SELECT lpc2.id_item
                    FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc2
                    WHERE lpc2.id_path = lpu.id_path
                )
            ) AS path_items_completed
            FROM ".LearningCoursepathUser::model()->tableName()." AS lpu
            INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lpu.idUser = sfu.id_user_lms
            WHERE lpu.idUser = ".$lmsUserId." AND lpu.id_path = ".$learningPlanId)->queryRow();
		if(empty($completion)){
			return false;
		} else {
			return $completion['path_items_completed']."/".$completion['path_items_total'];
		}
	}



	/**
	 * Update Salesforce CO2 (enrollment) record for the given course & user
	 * 
	 * @param SalesforceApiClient $api
	 * @param LearningCourse $course
	 * @param CoreUser $user
	 * @param bool $courseCompletedDatetime
	 * @param int $percCompleted
	 * @param datetime $firstAccess
	 */
	protected function updateCourseAttendance(SalesforceApiClient $api, LearningCourse $course, CoreUser $user, $courseCompletedDatetime=false, $percCompleted = false, $firstAccess = false) {
	    $idCourse  = $course->idCourse;
	    $idUser    = $user->idst;

	    $lastScore = false;
	    $maxScore = false;
	    $scoreInfo = LearningCourseuser::getLastScoreByUserAndCourse($idCourse, $idUser, true);
	    	
	    if (isset($scoreInfo['hasScore']) && ($scoreInfo['hasScore'] !== false) ) {
	        $lastScore   = $scoreInfo['score'];
	        $maxScore    = $scoreInfo['maxScore'];
	    }
	    	
	    $enrollment = LearningCourseuser::model()->findByAttributes(array(
	        'idUser'      => $idUser,
	        'idCourse'    => $idCourse,
	    ));
	    
	    $secondsInCourse = LearningTracksession::model()->getUserTotalTime($idUser, $idCourse, '', false, true);
	    $response = $api->updateAttendance($enrollment, $secondsInCourse, $courseCompletedDatetime, $lastScore, $maxScore, $percCompleted, $firstAccess);

	    return $response;
	     
	}
	

	
	/**
	 * Update Salesforce CO2 (enrollment) record for the given course & user
	 *
	 * @param SalesforceApiClient $api
	 * @param LearningCoursepath $plan
	 * @param CoreUser $user
	 */
	protected function updatePlanAttendance(SalesforceApiClient $api, LearningCoursepath $plan, CoreUser $user, $courseCompletedDatetime=false) {

	    $idUser    = $user->idst;
	     
	    $enrollment = LearningCoursepathUser::model()->findByAttributes(array(
	        'idUser'      => $user->idst,
	        'id_path'     => $plan->id_path,
	    ));

	    $response = $api->updateAttendance($enrollment, false, $courseCompletedDatetime);
	    return $response;
	
	}
	
	
	
	/**
	 * Executed on user enrollment in a course
	 * 
	 * @param DEvent $event
	 * 
	 */
	public function onUserSubscribedCourse(DEvent $event) {
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
	    $this->onUserEnrolledToCourseOrPlan($event, SalesforceApiClient::ITEM_TYPE_COURSE);
	}
	
	
	/**
	 * Executed on user un-enrollment
	 *
	 * @param DEvent $event
	 *
	 */
	public function onUserUnsubscribed(DEvent $event) {

		/* @var $sfUser SalesforceUser */
		/* @var $sfLmsAccount SalesforceLmsAccount */
		/* @var $enrollment LearningCourseuser */
		/* @var $course LearningCourse */
	    
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
				
		$eventParams = $event->params;
		
		// No course or user ? Get out
		if (!$eventParams['course'] || !$eventParams['user']) return;
		
		$course 		= $eventParams['course'];
		$user 			= $eventParams['user'];
		$idUser 		= $user->idst;
		
		
		// First, check if user is part of Salesforce
		if (!($sfUser = $this->isSalesforceUser($idUser))) return;
		
		// Get LMS based Salesforce account
		$sfLmsIdAccount = $sfUser->sf_lms_id;
		$sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfLmsIdAccount);
		
		// If no such.. get out
		if (!$sfLmsAccount) return;

		try {
			$api = new SalesforceApiClient($sfLmsAccount);

			$enrollment = new LearningCourseuser();
			$enrollment->idCourse    = $course->idCourse;
			$enrollment->idUser      = $user->idst;
			$enrollments = array($enrollment);
			
			$delete = true;
			$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, false, false, $delete);
			
			Yii::log(var_export($response, true), CLogger::LEVEL_INFO);

			// remove task from Sf
			// remove item from salesforce_enrollment_sf_task

			$sfTaskIds =
				Yii::app()->db->createCommand(
					'SELECT salesforce_task_id FROM salesforce_enrollment_sf_task WHERE course_id = :course_id and user_id = :user_id'
				)->queryAll(true,
						array(
							":course_id" => $course->idCourse,
							":user_id" => $user->idst
						)
					);


			foreach ($sfTaskIds as $sfTaskId)
			{
				$sfTaskId = $sfTaskId['salesforce_task_id'];

				$sfResponce = $api->deleteTask($sfTaskId);

				$result =
					Yii::app()->db->createCommand(
						'delete from salesforce_enrollment_sf_task where salesforce_task_id = :old_task_id')
						->query(
							array(
								":old_task_id" => $sfTaskId
							)
						);
			}
		}
		catch (Exception $e) {
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details = array(
				'Course ID' => $course->idCourse,
				'User ID' 	=> $user->idst,
			);
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
		
		
		
	}
	
	/**
	 * 
	 * @param DEvent $event
	 */
	public function onILTSessionUserEnrolled(DEvent $event) {

	    // Skip handling if the plugin is disabled
		if (!PluginManager::isPluginActive('ClassroomApp')) return;
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
		
		$session_id 	= $event->params['session_id'];
		$idUser 		= $event->params['user'];
		
		// Check: user is SF and SF account is setted up
		if (!($sfUser = $this->isSalesforceUser($idUser))) return;
		if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;
		
		
		/* @var $session LtCourseuserSession */
		$session = LtCourseuserSession::model()->findByAttributes(array(
			'id_session'		=> $session_id,
			'id_user'			=> $idUser,
		));
		if (!$session) return;
		
		$user 		= CoreUser::model()->findByPk($idUser);
		$idCourse 	= $session->ltCourseSession->course_id;
		$course 	= LearningCourse::model()->findByPk($idCourse);
		$sessions	= LtCourseSession::model()->findByPk($session_id);
		$dates		= $sessions->getDates();

		try {
			
			$api = new SalesforceApiClient($sfLmsAccount);
			foreach($dates as $date) {
				$additionalInfo = array();

				$locationId = intval($date['id_location']);
				$classroomId = intval($date['id_classroom']);
				$Mlocation = LtLocation::model()->findByPk($locationId);
				$Mclassroom = LtClassroom::model()->findByPk($classroomId);
				$locationOffice = trim($Mlocation['name']);
				$locationRoom = trim($Mclassroom['name']);
				$locationAddress = trim($Mlocation['address']);
				$location = $locationOffice.", ".$locationRoom. " - ".$locationAddress;

				$startTime = strtotime($date['day']." ".$date['time_begin']);
				$endTime = strtotime($date['day']." ".$date['time_end']);
				$durationInSeconds = abs($endTime - $startTime);

				$eventDate = $date['day']." ".$date['time_begin'];

				// USER enrolling into a session?  Create an event
				if ($sfUser->sf_type == SalesforceUser::USER_TYPE_USER) {
					$eventId = $api->createEventFromClassroom($session, $sfUser, $location, $eventDate, $durationInSeconds);
					if (is_string($eventId))
						$additionalInfo['eventId'] = $eventId;
				}

				// Leads must be converted to Contacts before proceeding
				if ($sfUser->sf_type == SalesforceUser::USER_TYPE_LEAD) {
					if (!$this->convertLeadToContact($api, $sfUser, $sfLmsAccount)) {
						throw new Exception(Yii::t('salesforce', 'Unable to convert a Lead into a Contact'));
					}
				}

				// Contact ?  Create order (If it was LEAD, the conversion above would convert it to Contact)
				if ($sfUser->sf_type == SalesforceUser::USER_TYPE_CONTACT) {
					//Yii::log('Salesforce selling: '. $course->selling );
					//Yii::log('Salesforce replicate_courses_as_products: '. $sfLmsAccount->replicate_courses_as_products );

					if ( ($course->selling) && $sfLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) {
						$orderInformation = $this->issueSalesOrder($api, $course, false, SalesforceApiClient::ITEM_TYPE_COURSE, $user, $sfUser, $sfLmsAccount);
						$additionalInfo = CMap::mergeArray($additionalInfo, $orderInformation);
					}
				}

				$enrollment = new LearningCourseuser();
				$enrollment->idCourse = $idCourse;
				$enrollment->idUser = $idUser;
				$enrollment->date_inscr = Yii::app()->localtime->getUTCNow();
				$enrollments = array($enrollment);

				$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, $additionalInfo);
			}
			
			Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
			
		}
		catch (Exception $e) {
			$details = array();
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details['User ID'] = $sfUser->id_user_lms;
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
		
		
		
	}
	
	
	/**
	 * Executed when user enrolls to a Webinar session
	 * 
	 * @param DEvent $event
	 */
	public function onUserEnrolledInWebinarSession(DEvent $event) {

		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
				
		/* @var $enrollment WebinarSessionUser */
		if(isset($event->params['enrollment'])){
			$enrollment = $event->params['enrollment'];
		} else{
			if(isset($event->params['user']) && isset($event->params['session_id'])){
				$enrollment = WebinarSessionUser::model()->findByAttributes(array(
					'id_user' => $event->params['user'],
					'id_session' => $event->params['session_id']
				));
			}
		}
		if (!$enrollment instanceof WebinarSessionUser) return;
		$idUser = $enrollment->id_user;
		
		
		// Check: user is SF and SF account is set up
		if (!($sfUser = $this->isSalesforceUser($idUser))) return;
		if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;


		$session = WebinarSession::model()->findByPk($enrollment->id_session);
		$user = CoreUser::model()->findByPk($idUser);
		/**
		 * @var $session WebinarSession
		 */
		$idCourse 	= $session->course_id;
		$course = LearningCourse::model()->findByPk($idCourse);
		
		try {
				
			$api = new SalesforceApiClient($sfLmsAccount);
			$additionalInfo = array();
				
			// USER enrolling into a session?  Create an event
			if ($sfUser->sf_type == SalesforceUser::USER_TYPE_USER) {
				$eventId = $api->createEventFromWebinar($enrollment, $sfUser);
				if (is_string($eventId))
					$additionalInfo['eventId'] = $eventId;
			}
		
			// Leads must be converted to COntacts before proceeding
			if ($sfUser->sf_type == SalesforceUser::USER_TYPE_LEAD) {
			    if (!$this->convertLeadToContact($api, $sfUser, $sfLmsAccount)) {
			        throw new Exception(Yii::t('salesforce', 'Unable to convert a Lead into a Contact'));
			    }
			}
				
			// Contact ?  Create order (If it was LEAD, the convertion above would convert it to Contact)
			if ($sfUser->sf_type == SalesforceUser::USER_TYPE_CONTACT) {
				//Yii::log('Salesforce selling: '. $course->selling );
				//Yii::log('Salesforce replicate_courses_as_products: '. $sfLmsAccount->replicate_courses_as_products );

				if ( ($course->selling) && $sfLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) {
					$orderInformation = $this->issueSalesOrder($api, $course, false, SalesforceApiClient::ITEM_TYPE_COURSE, $user, $sfUser, $sfLmsAccount);
					$additionalInfo = CMap::mergeArray($additionalInfo, $orderInformation);
				}
			}
				
			$enrollment = new LearningCourseuser();
			$enrollment->idCourse = $idCourse;
			$enrollment->idUser	= $idUser;
			$enrollment->date_inscr = Yii::app()->localtime->getUTCNow();
			$enrollments = array($enrollment);
				
			$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, $additionalInfo);
				
			Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
				
		}
		catch (Exception $e) {
			$details = array();
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details['User ID'] = $sfUser->id_user_lms;
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
		
	}
	
	
	/**
	 * Executed when student is assigned to a learning plan
	 *
	 * @param DEvent $event
	 *
	 */
	public function onUserEnrolledInLearningPlan(DEvent $event) {

	    // Skip handling if the plugin is disabled
	    if (!PluginManager::isPluginActive('CurriculaApp')) return;
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
	    	    
	    $this->onUserEnrolledToCourseOrPlan($event, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
	}

	public function onLearningPlanUpdated(DEvent $event){
		// Skip handling if the plugin is disabled
		if (!PluginManager::isPluginActive('CurriculaApp')) return;
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');

		$user = $event->params['user_id'];
		$plan = $event->params['learning_plan']->id_path;

		// Check: user is SF and SF account is set up
		$sfUser = $this->isSalesforceUser($user);
		if (empty($sfUser) || $sfUser->sf_type != SalesforceUser::USER_TYPE_USER) {
			return;
		} else {
			$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
			$api = new SalesforceApiClient($sfLmsAccount);

			// Delete old tasks
			$oldTasks = Yii::app()->db->createCommand()
				->select('salesforce_task_id')
				->from('salesforce_enrollment_sf_task')
				->where('path_id = :pathId AND user_id = :userId', array(
					':pathId' => $plan,
					':userId' => $user
				))
				->queryAll();
			if(!empty($oldTasks)){
				foreach($oldTasks as $oldTask){
					$api->deleteTask($oldTask['salesforce_task_id']);
				}
				Yii::app()->db->createCommand()
					->delete('salesforce_enrollment_sf_task', 'path_id = :pathId AND user_id = :userId', array(
						':pathId' => $plan,
						':userId' => $user
					));
			}

			// Create new tasks
			$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
				'id_path' => $plan,
				'idUser' => $user,
			));
			$taskId = $api->createTaskFromLearningPlan($enrollment, $sfUser);
			Yii::app()->db->createCommand(
				'insert into salesforce_enrollment_sf_task
							(path_id, course_id, user_id, salesforce_task_id)
						 values
						 	(:path_id, :course_id, :user_id, :salesforce_task_id)'
			)->query(
				array(
					":path_id" => $plan,
					":course_id" => 0,
					":user_id" => $user,
					":salesforce_task_id" => $taskId
				)
			);
			// Create CO2 items for each course related to a LP:
			$courseIds = Yii::app()->db->createCommand()
				->select('c.idCourse')
				->from(LearningCoursepathCourses::model()->tableName().' p')
				->join(LearningCourse::model()->tableName().' c', 'p.id_item = c.idCourse')
				->where('p.id_path = :idPath AND c.course_type = :courseType', array(
					':idPath' => $plan,
					':courseType' => LearningCourse::TYPE_ELEARNING
				))
				->queryAll();

			foreach($courseIds as $courseId){
				$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idCourse' => $courseId['idCourse'],
					'idUser' => $user,
				));
				$taskId = $api->createTaskFromCourse($enrollment, $sfUser);
				Yii::app()->db->createCommand(
					'insert into salesforce_enrollment_sf_task
							(path_id, course_id, user_id, salesforce_task_id)
						 values
							(:path_id, :course_id, :user_id, :salesforce_task_id)'
				)->query(
					array(
						":path_id" => $plan,
						":course_id" => $courseId['idCourse'],
						":user_id" => $user,
						":salesforce_task_id" => $taskId
					)
				);
			}
		}

	}

	public function onCourseAddedToLearningPlan(DEvent $event){
		// Skip handling if the plugin is disabled
		if (!PluginManager::isPluginActive('CurriculaApp')) return;
		$this->onCourseOrPlanCreatedOrUpdated($event);
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
		$eventParams = $event->params;

		// No course or learning path? Get out
		if (!$eventParams['coursepath'] || !$eventParams['targetCourse']) return;

		$plan	= $eventParams['coursepath'];
		$course = $eventParams['targetCourse'];

		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		$api = new SalesforceApiClient($sfLmsAccount);

		$usersInLearningPlan = $this->getUsersEnrolledInLearningPlan($plan->id_path);

		foreach($usersInLearningPlan as $userInLearningPlan){
			// Add the enrollment in Salesforce Custom Object 2
			$event = new DEvent();
			$event->event_name = 'onCourseAddedToLearningPlan';
			$event->params['user'] = CoreUser::model()->findByAttributes(array('idst' => $userInLearningPlan['idUser']));
			$event->params['course'] = $course;
			$this->onUserEnrolledToCourseOrPlan($event, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);

			// Update the enrollment in Salesforce
			$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
				'idUser'      => $userInLearningPlan['idUser'],
				'id_path'     => $plan->id_path,
			));
			$pctCompleted = $this->calculatePathCompletion($plan->id_path, $userInLearningPlan['idUser']);
			$firstAccess = $this->getPlanStartDate($plan->id_path, $userInLearningPlan['idUser']);
			$pathCompleted = $this->fetchLearningPathCompletion($plan->id_path, $userInLearningPlan['idUser']);
			$api->updateAttendance($enrollment, false, false, false, false, $pctCompleted, $firstAccess, true, $pathCompleted, true);
		}
	}

	public function onCourseRemovedFromLearningPlan(DEvent $event){
		// Skip handling if the plugin is disabled
		if (!PluginManager::isPluginActive('CurriculaApp')) return;
		$this->onCourseOrPlanCreatedOrUpdated($event);
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
		$eventParams = $event->params;

		// No course or learning path? Get out
		if (!$eventParams['coursepath'] || !$eventParams['targetCourse']) return;

		$plan	= $eventParams['coursepath'];
		$course = $eventParams['targetCourse'];

		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		$api = new SalesforceApiClient($sfLmsAccount);

		// We'll skip setting the course unenrollment when this course is also featured on another LP which the SF user is enrolled in
		$skipUnenrollments = $this->getUsersInOtherPlansForDeletedCourse($course->idCourse);

		$lpUsers = $this->getUsersEnrolledInLearningPlan($plan->id_path);
		try {
			foreach ($lpUsers as $lpUser) {
				// Set course unenrollment if the course wasn't part of another Learning Plan the user is enrolled in
				if (!in_array($lpUser['idUser'], $skipUnenrollments)) {
					$enrollment = LearningCourseuser::model()->findByAttributes(array(
						'idUser' => $lpUser['idUser'],
						'idCourse' => $course->idCourse
					));
					$enrollments = array($enrollment);
					$delete = true;
					$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, false, false, $delete);
					Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
				}

				// Update LP enrollments
				$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
					'idUser'      => $lpUser['idUser'],
					'id_path'     => $plan->id_path,
				));
				$pctCompleted = $this->calculatePathCompletion($plan->id_path, $lpUser['idUser']);
				$firstAccess = $this->getPlanStartDate($plan->id_path, $lpUser['idUser']);
				$pathCompleted = $this->fetchLearningPathCompletion($plan->id_path, $lpUser['idUser']);
				if($pctCompleted == 100){
					$courseCompleted = Yii::app()->localtime->getUTCNow();
				} else {
					$courseCompleted = false;
				}

				$api->updateAttendance($enrollment, false, $courseCompleted, false, false, $pctCompleted, $firstAccess, true, $pathCompleted, true);
			}
		} catch (Exception $e) {
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details = array(
				'Course ID' => $course->idCourse,
				'User ID' 	=> $lpUser['idUser'],
			);
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
	}

	public function onLearningObjectAddedToCourse(DEvent $event){
		// Skip handling if the plugin is disabled
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
		$eventParams = $event->params;

		// No learning object? Get out
		if (!$eventParams['loModel']) return;

		$lo	= $eventParams['loModel'];

		if($lo instanceof LearningOrganization){
			$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
			$api = new SalesforceApiClient($sfLmsAccount);

			$users = $this->getAllUsersAssignedToCourse($lo->idCourse);

			try {
				foreach ($users as $user) {
					// Updating course enrollments
					$enrollment = LearningCourseuser::model()->findByAttributes(array(
						'idCourse' => $lo->idCourse,
						'idUser' => $user,
					));
					$pctCompleted = ($enrollment->status == LearningCourseuser::$COURSE_USER_END)?false:$this->calculateCourseCompletion($lo->idCourse, $user);
					$api->updateAttendance($enrollment, false, false, false, false, $pctCompleted, null, true, false, true, true);

					// No need to update LP enrollments at this time
				}
			} catch (Exception $e) {
				Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				$details = array(
					'Course ID' => $lo->idCourse
				);
				Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
			}
		}

	}

	public function onLearningObjectDeletedFromCourse(DEvent $event){
		set_time_limit(500000);
		// Skip handling if the plugin is disabled
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
		$eventParams = $event->params;
		// No course model? Get out
		if (!$eventParams['courseModel']) return;

		$course	= $eventParams['courseModel'];

		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		$api = new SalesforceApiClient($sfLmsAccount);

		$users = $this->getAllUsersAssignedToCourse($course->idCourse);

		$usersCompletedCourse = array();
		try {
			// Updating course enrollments
			foreach ($users as $user) {
				$enrollment = LearningCourseuser::model()->findByAttributes(array(
					'idCourse' => $course->idCourse,
					'idUser' => $user,
				));
				$pctCompleted = $this->calculateCourseCompletion($course->idCourse, $user);
				$dontUpdateStatus = ($enrollment->status == LearningCourseuser::$COURSE_USER_END)?true:false;
				if($pctCompleted == 100){
					$courseCompleted = Yii::app()->localtime->getUTCNow();
					$usersCompletedCourse[] = $user;
				} else {
					$courseCompleted = false;
				}
				$api->updateAttendance($enrollment, false, $courseCompleted, false, false, $pctCompleted, null, true, false, true, $dontUpdateStatus);
			}

			// Updating learning plan enrollments
			$lpEnrollments = $this->getLearningPlanEnrollmentsForUsers($usersCompletedCourse, $course->idCourse);

			foreach($lpEnrollments as $lpEnrollment){
				$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
					'idUser'      => $lpEnrollment['idUser'],
					'id_path'     => $lpEnrollment['idPath']
				));
				$pctCompleted = $this->calculatePathCompletion($lpEnrollment['idPath'], $lpEnrollment['idUser']);
				if($pctCompleted == 100){
					$courseCompleted = Yii::app()->localtime->getUTCNow();
				} else {
					$courseCompleted = false;
				}
				$pathCompleted = $this->fetchLearningPathCompletion($lpEnrollment['idPath'], $lpEnrollment['idUser']);
				$api->updateAttendance($enrollment, false, $courseCompleted, false, false, $pctCompleted, false, true, $pathCompleted, true);
			}
		} catch (Exception $e) {
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details = array(
				'Course ID' => $course->idCourse
			);
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
	}
	

	
	/**
	 *When user unenrolls from Learning Plan
	 * 
	 * @param DEvent $event
	 */
	public function onUserUnEnrolledFromLearningPlan(DEvent $event) {
	    
	    /* @var $sfUser SalesforceUser */
	    /* @var $sfLmsAccount SalesforceLmsAccount */
	    /* @var $enrollment LearningCoursepathUser */
	    /* @var $course LearningCourse */
	    /* @var $plan LearningCoursepath */

		// Skip handling if the plugin is disabled
		if (!PluginManager::isPluginActive('CurriculaApp')) return;
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
				
	    $eventParams = $event->params;
	    
	    // No course or user ? Get out
	    if (!$eventParams['learningPlan'] || !$eventParams['user']) return;
	    
	    $plan           = $eventParams['learningPlan'];
	    $user           = $eventParams['user'];
	    $idUser 		= $user->idst;
	    
	    // First, check if user is part of Salesforce
	    if (!($sfUser = $this->isSalesforceUser($idUser))) return;
	    
	    // Get LMS based Salesforce account
	    $sfLmsIdAccount = $sfUser->sf_lms_id;
	    $sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfLmsIdAccount);
	    
	    // If no such.. get out
	    if (!$sfLmsAccount) return;
	    
	    try {

	        $api = new SalesforceApiClient($sfLmsAccount);
	    
	        $enrollment = LearningCoursepathUser::model()->findByAttributes(array(
	            'id_path'		=> $plan->id_path,
	            'idUser'		=> $idUser,
	        ));
	        	
	        $enrollment = new LearningCoursepathUser();
	        $enrollment->id_path       = $plan->id_path;
	        $enrollment->idUser        = $idUser;
	        $enrollments = array($enrollment);

	        $delete = true;
	        $response = $api->createOrUpdateEnrollments($enrollments, false, false, false, $delete);
	        Yii::log(var_export($response, true), CLogger::LEVEL_INFO);

			// Also delete all enrollments for applicable courses that aren't part of another Learning plan to which the user is subscribed to
			// Not necessary atm (keeping the code, just in case
			/*$courses = $this->fetchLearningPlanLinkedCoursesForUnenrollment($idUser, $plan->id_path);
			if(!empty($courses)){
				foreach($courses as $c){
					$enrollment = LearningCourseuser::model()->findByAttributes(array(
						'idUser'	=> $idUser,
						'idCourse'	=> $c['courseId']
					));
					$enrollments = array($enrollment);

					$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, false, false, $delete);
					Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
				}
			}*/
	        	
	    }
	    catch (Exception $e) {
	        Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
	        $details = array(
	            'Course ID' => $course->idCourse,
	            'User ID' 	=> $user->idst,
	        );
	        Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
	    }
	    
	     
	    
	}

	
	/**
	 * Executed when user completes a LO.
	 * 
	 * Updates Salesforce CO2 object for the course+user for this LO, as well as 
	 * all CO2 objects for Learning Plans + the user completing the LO, which (the plans) contain this course (where LO is)
	 *
	 * @param DEvent $event
	 *
	 */
	public function onStudentCompletedLO(DEvent $event) {

		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
				
		//'user'
		//'organization'
		//'track'
		$eventParams = $event->params;

		/* @var $user CoreUser */
		/* @var $lo LearningOrganization */
		/* @var $track LearningCommonTrack */
		
		$user     = $eventParams['user'];
		$lo       = $eventParams['organization'];
		$track    = $eventParams['track'];
		$course   = $lo->course;
		$idUser   = $user->idst;
		$idCourse = $course->idCourse;

		// Check: user is SF and SF account is set up
		if (!($sfUser = $this->isSalesforceUser($idUser))) return;
		if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;

		
		try {

			$api = new SalesforceApiClient($sfLmsAccount);

			
			// CASE1: Learning plans containing the course whose LO just has been completed
			
			// Is this course is part of a Learning Plan?
			// If yes, create list of CO2-wize (Enrollments) names (Lxxxx_<user>) and
			// check if there are such objects (records) in SF CO2
			// If yes, we have to update their attendance status accordingly
			$plans = LearningCoursepathCourses::model()->findAllByAttributes(array('id_item' => $idCourse));
			if (!empty($plans)) {
			    $sfNames = array();
			    foreach ($plans as $plan) {
			        $sfNames[] = "'L" . $plan->id_path . "_" . $idUser . "'";

					if (!empty($sfNames)) {
						$sfNamesList = implode(',', $sfNames);
						$sfPlanEnrollments = $api->getSfEnrollments("Name IN ($sfNamesList)");
						if (!empty($sfPlanEnrollments)) {
							foreach ($sfPlanEnrollments as $name => $sfPlanEnrollment) {
								// LX_Y ====>   LP Id = X, idUser = Y
								$tmp1 = preg_replace("/(^L)/", "", $name);
								$tmp2 = preg_split("/_/", $tmp1);
								$idPath = (int)$tmp2[0];

								// @TODO Calculate scores for the learning plan and se
								$lastScore = false;
								$maxScore = false;

								// @TODO Time in LP calculation (minutes)
								$minutesInCourse = false;

								$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
									'id_path' => $idPath,
									'idUser' => $idUser,
								));
								$pathCompleted = $this->calculatePathCompletion($idPath, $idUser);
								$firstAccess = $this->getPlanStartDate($idPath, $idUser);
								$api->updateAttendance($enrollment, $minutesInCourse, false, $lastScore, $maxScore, $pathCompleted, $firstAccess);
							}
						}
					}
			    }
			}
			

			// CASE2: The COURSE itself
			$percentageCompleted = $this->calculateCourseCompletion($idCourse, $idUser);
			$firstAccess = $this->getCourseStartDate($idCourse, $idUser);
			$this->updateCourseAttendance($api, $course, $user, false, $percentageCompleted, $firstAccess);

		}
		catch (Exception $e) {
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}
		
	}

	/**
	 * Executed when a SF user joins a Webinar
	 *
	 * @param DEvent $event
	 */
	public function onStudentCompletedWebinarOrILTSession(DEvent $event){
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');

		$eventParams = $event->params;

		$courseId = $eventParams['courseId'];
		$userId = $eventParams['userId'];

		// Check: user is SF and SF account is set up
		if (!($sfUser = $this->isSalesforceUser($userId))) return;
		if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;

		try{
			$api = new SalesforceApiClient($sfLmsAccount);

			// Only need to handle Learning Plans, the course itself is being handled in
			$plans = LearningCoursepathCourses::model()->findAllByAttributes(array('id_item' => $courseId));
			if(!empty($plans)){
				$sfNames = array();
				foreach ($plans as $plan) {
					$sfNames[] = "'L" . $plan->id_path . "_" . $userId . "'";

					if (!empty($sfNames)) {
						$sfNamesList = implode(',', $sfNames);
						$sfPlanEnrollments = $api->getSfEnrollments("Name IN ($sfNamesList)");
						if (!empty($sfPlanEnrollments)) {
							foreach ($sfPlanEnrollments as $name => $sfPlanEnrollment) {
								// LX_Y ====>   LP Id = X, idUser = Y
								$tmp1 = preg_replace("/(^L)/", "", $name);
								$tmp2 = preg_split("/_/", $tmp1);
								$idPath = (int)$tmp2[0];

								// @TODO Calculate scores for the learning plan and se
								$lastScore = false;
								$maxScore = false;

								// @TODO Time in LP calculation (minutes)
								$minutesInCourse = false;

								$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
									'id_path' => $idPath,
									'idUser' => $userId,
								));
								$pathCompleted = $this->calculatePathCompletion($idPath, $userId);
								$firstAccess = $this->getPlanStartDate($idPath, $userId);

								$api->updateAttendance($enrollment, $minutesInCourse, false, $lastScore, $maxScore, $pathCompleted, $firstAccess);
							}
						}
					}
				}
			}
		} catch(Exception $e){
			Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
			$details = array(
				'Course ID' => $courseId,
				'User ID' 	=> $userId,
			);
			Yii::log('Details: ' . var_export($details, true), CLogger::LEVEL_ERROR);
		}
	}

	/**
	 * Calculates the percentage of learning objects in a course that have been completed
	 *
	 * @param $idCourse
	 * @param $idUser
	 * @return int
	 */
	public function calculateCourseCompletion($idCourse, $idUser){
		$test = Yii::app()->db->createCommand("
            SELECT (
                SELECT COUNT(lo2.idOrg)
                FROM ".LearningOrganization::model()->tableName()." AS lo2
                WHERE lo2.idCourse = ".$idCourse." AND lo2.idParent > 0
            ) AS courseObjectsTotal, (
                SELECT COUNT(lct.idReference)
                FROM ".LearningCommontrack::model()->tableName()." AS lct
                WHERE lct.idUser = ".$idUser." AND lct.status = '".LearningCommontrack::STATUS_COMPLETED."' AND lct.idReference IN (
                    SELECT lo3.idOrg
                    FROM ".LearningOrganization::model()->tableName()." AS lo3
                    WHERE lo3.idParent = lo1.idOrg
                )
            ) AS courseObjectsCompleted
            FROM ".LearningCourseuser::model()->tableName()." AS lcu
            INNER JOIN ".LearningOrganization::model()->tableName()." AS lo1 ON lcu.idCourse = lo1.idCourse
            WHERE lcu.idUser = ".$idUser." AND lcu.idCourse = ".$idCourse." AND lo1.idParent = 0
        ")->queryRow();

		if(!empty($test)){
			if($test['courseObjectsTotal'] == 0){
				return 0;
			} else {
				return round(($test['courseObjectsCompleted'] / $test['courseObjectsTotal']) * 100);
			}
		} else {
			return false;
		}
	}

	public function calculatePathCompletion($idPath, $idUser){
		// Keeping this as a reference --> this gives the total completion percentage of LO's in a
		/*$test = Yii::app()->db->createCommand("
            SELECT lpu.id_path, lpu.idUser, lpc.id_item, lo1.idOrg, (
                SELECT COUNT(lo2.idOrg)
                FROM ".LearningOrganization::model()->tableName()." AS lo2
                WHERE lo2.idCourse = lpc.id_item AND lo2.idParent > 0
            ) AS courseObjectsTotal, (
                SELECT COUNT(lct.idReference)
                FROM ".LearningCommontrack::model()->tableName()." AS lct
                WHERE lct.idUser = ".$idUser." AND lct.status = '".LearningCommontrack::STATUS_COMPLETED."' AND lct.idReference IN (
                    SELECT lo3.idOrg
                    FROM ".LearningOrganization::model()->tableName()." AS lo3
                    WHERE lo3.idParent = lo1.idOrg
                )
            ) AS courseObjectsCompleted
            FROM ".LearningCoursepathUser::model()->tableName()." AS lpu
            INNER JOIN ".LearningCoursepathCourses::model()->tableName()." AS lpc ON lpu.id_path = lpc.id_path
            INNER JOIN ".LearningOrganization::model()->tableName()." AS lo1 ON lpc.id_item = lo1.idCourse
            WHERE lpu.idUser = ".$idUser." AND lpu.id_path = ".$idPath." AND lo1.idParent = 0
        ")->queryAll();

		if(!empty($test)){
			$countCompleted = 0;
			$countAll = 0;
			foreach($test as $t){
				$countCompleted += $t['courseObjectsCompleted'];
				$countAll += $t['courseObjectsTotal'];
			}
			if($countAll > 0){
				return round(($countCompleted / $countAll) * 100);
			} else {
				return false;
			}
		} else {
			return false;
		}*/

		$pathCompletion = $this->fetchLearningPathCompletion($idPath, $idUser);
		if(!empty($pathCompletion)){
			$parts = explode('/', $pathCompletion);
			return round(($parts[0]/$parts[1])*100);
		} else {
			return false;
		}
	}

	/**
	 * Get the time a user entered a course for the first time
	 * @param $idCourse
	 * @param $idUser
	 * @return bool|string
	 */
	public function getCourseStartDate($idCourse, $idUser){
		$startTime = Yii::app()->db->createCommand("
			SELECT lts.enterTime
			FROM ".LearningTracksession::model()->tableName()." AS lts
			WHERE idUser = ".$idUser." AND idCourse = ".$idCourse."
			ORDER BY enterTime ASC
			LIMIT 1
		")->queryScalar();

		if(!empty($startTime)) {
			return str_replace(" ", "T", trim($startTime))."+0000";
		} else {
			return false;
		}
	}

	/**
	 * Get the time a user entered a learning plan for the first time
	 * @param int $idPath
	 * @param int $idUser
	 * @return bool|string
	 */
	public function getPlanStartDate($idPath, $idUser){
		$startTime = Yii::app()->db->createCommand("
			SELECT lts.enterTime
			FROM ".LearningTracksession::model()->tableName()." AS lts
			WHERE idUser = ".$idUser." AND idCourse IN (
				SELECT id_item
				FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
				WHERE lpc.id_path = ".$idPath."
			)
			ORDER BY enterTime ASC
			LIMIT 1
		")->queryScalar();

		if(!empty($startTime)) {
			return str_replace(" ", "T", trim($startTime))."+0000";
		} else {
			return false;
		}
	}

	/**
	 * fetch the Salesforce users which are currently enrolled in a specific Learning Plan
	 * @param int $idPath
	 * @return array
	 */
	public function getUsersEnrolledInLearningPlan($idPath){
		$users = Yii::app()->db->createCommand("
			SELECT sfu.id_user_lms AS idUser
			FROM ".SalesforceUser::model()->tableName()." AS sfu
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lcu ON sfu.id_user_lms = lcu.idUser
			WHERE lcu.id_path = ".$idPath."
		")->queryAll();

		return $users;
	}

	/**
	 * Fetches the Salesforce users which are enrolled into a specific course
	 * @param int $courseId
	 * @return array
	 */
	public function getAllUsersAssignedToCourse($courseId){
		$result = array();
		$users = Yii::app()->db->createCommand("
			SELECT lcu.idUser
			FROM ".LearningCourseuser::model()->tableName()." AS lcu
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lcu.idUser = sfu.id_user_lms
			WHERE lcu.idCourse = ".$courseId."
		")->queryAll();
		foreach($users as $user){
			$result[] = $user['idUser'];
		}
		return $result;
	}

	/**
	 * Fetches the Salesforce users which are enrolled into a specific course but have not yet completed the course
	 * @param int $courseId
	 * @return array
	 */
	public function getAllUsersAssignedToUncompletedCourse($courseId){
		$result = array();
		$users = Yii::app()->db->createCommand("
			SELECT lcu.idUser
			FROM ".LearningCourseuser::model()->tableName()." AS lcu
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lcu.idUser = sfu.id_user_lms
			WHERE lcu.idCourse = ".$courseId." AND lcu.status <> ".LearningCourseuser::$COURSE_USER_END."
		")->queryAll();
		foreach($users as $user){
			$result[] = $user['idUser'];
		}
		return $result;
	}

	/**
	 * Returns an array with all Salesforce users assigned to a plan other than the plan the deleted course belonged to
	 * @param int $courseId
	 * @return array
	 */
	public function getUsersInOtherPlansForDeletedCourse($courseId){
		$result = array();
		$users = Yii::app()->db->createCommand("
			SELECT lpu.idUser
			FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lpu ON lpc.id_path = lpu.id_path
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lpu.idUser = sfu.id_user_lms
			WHERE lpc.id_item = ".$courseId."
		")->queryAll();

		foreach($users as $user){
			$result[] = $user['idUser'];
		}

		return $result;
	}

	/**
	 * Returns an array with Learning Plan enrollments for preselected Salesforce users
	 *
	 * @param array $users
	 * @param int $idCourse
	 * @return array
	 */
	public function getLearningPlanEnrollmentsForUsers($users, $idCourse){
		$enrollments = array();
		if(!empty($users)) {
			$enrollments = Yii::app()->db->createCommand("
				SELECT lpc.id_path as idPath, lpu.idUser
				FROM " . LearningCoursepathCourses::model()->tableName() . " AS lpc
				INNER JOIN " . LearningCoursepathUser::model()->tableName() . " AS lpu ON lpc.id_path = lpu.id_path
				WHERE lpc.id_item = " . $idCourse . " AND lpu.idUser IN (" . implode(",", $users) . ")
			")->queryAll();
		}

		return $enrollments;
	}
	
	
	/**
	 * Executed when user completes a course
	 *
	 * @param DEvent $event
	 *
	 */
	public function onStudentCompletedCourse(DEvent $event) {
	     
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
			    
	    //'course'
	    //'user'
	    $eventParams = $event->params;

	    /* @var $user          CoreUser */
	    /* @var $course        LearningCourse */
	    /* @var $enrollment    LearningCourseuser */
	    
	    $user          = $eventParams['user'];
	    $course        = $eventParams['course'];
	    $idUser        = $user->idst;
	    $idCourse      = $course->idCourse;
	    
	    // Check: user is SF and SF account is set up
	    if (!($sfUser = $this->isSalesforceUser($idUser))) return;
	    if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;

		// Check if the course is part of a LP the user is subscribed to
		$paths = $this->fetchPathIdsForEnrollment($idUser, $idCourse);

	    try {
	        $api = new SalesforceApiClient($sfLmsAccount);
	        $completedDatetime = Yii::app()->localtime->getUTCNow();
			$firstAccess = $this->getCourseStartDate($idCourse, $idUser);
	        $this->updateCourseAttendance($api, $course, $user, $completedDatetime, 100, $firstAccess);

			if(!empty($paths)){
				foreach($paths as $path){
					$enrollment = LearningCoursepathUser::model()->findByAttributes(array(
						'id_path' => $path['id_path'],
						'idUser' => $idUser,
					));
					$enrollments = array($enrollment);
					$additionalInfo = array();
					$completion = $this->fetchLearningPathCompletion($path['id_path'], $idUser);
					if($completion){
						$additionalInfo['path_completion'] = $completion;
					}

					$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, $additionalInfo);

					Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
				}
			}
	    }
	    catch (Exception $e) {
	        Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
	    }
	     
	    
	}

	/**
	 * Creates an array with Learning Plan Id's which the course is part of
	 *
	 * @param $idUser
	 * @param $idCourse
	 * @return array Matching LP IDs
	 */
	public function fetchPathIdsForEnrollment($idUser, $idCourse){
		$paths = Yii::app()->db->createCommand("
            SELECT lpc.id_path
            FROM ".LearningCourseuser::model()->tableName()." AS lcu
            INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lcu.idUser = sfu.id_user_lms
            INNER JOIN ".LearningCoursepathCourses::model()->tableName()." AS lpc ON lcu.idCourse = lpc.id_item
            WHERE lcu.idUser = ".$idUser." AND lcu.idCourse = ".$idCourse)->queryAll();
		return $paths;
	}

	/**
	 * Creates an array with course id's belonging to the current Learning Plan, but not to other active LP's the user might be assigned to
	 *
	 * @param $idUser
	 * @param $idPath
	 * @return array
	 */
	public function fetchLearningPlanLinkedCoursesForUnenrollment($idUser, $idPath){
		$courses = Yii::app()->db->createCommand("
            SELECT lpc.id_item AS courseId
            FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
            WHERE lpc.id_path = ".$idPath." AND lpc.id_item NOT IN (
                SELECT lpc2.id_item
                FROM ".LearningCoursepathUser::model()->tableName()." AS lpu2
                INNER JOIN ".LearningCoursepathCourses::model()->tableName()." AS lpc2 ON lpu2.id_path = lpc2.id_path
                WHERE lpu2.id_path <> ".$idPath." AND lpu2.idUser = ".$idUser."
            )
        ")->queryAll();
		return $courses;
	}
	
	
	/**
	 * Executed when user completes a Learning Plan
	 *
	 * @param DEvent $event
	 *
	 */
	public function onStudentCompletedLearningPlan(DEvent $event) {
	
	    // Skip handling if the plugin is disabled
	    if (!PluginManager::isPluginActive('CurriculaApp')) return;
		Yii::log('Event ' . $event->event_name . ' intercepted in Salesforce');
	    	     
	    
	    // learningPlan LearningCoursepath 
	    // user CoreUser
	    // certificate LearningCertificateAssignCp
	    $eventParams = $event->params;
	     
	    /* @var $user          CoreUser */
	    /* @var $learningPlan LearningCoursepath */
	    /* @var $certificate LearningCertificateAssignCp */
	     
	    $user          = $eventParams['user'];
	    $plan          = $eventParams['learningPlan'];
	    $idUser        = $user->idst;
	    
	    // Check: user is SF and SF account is setted up
	    if (!($sfUser = $this->isSalesforceUser($idUser))) return;
	    if (!($sfLmsAccount = SalesforceLmsAccount::model()->findByPk($sfUser->sf_lms_id))) return;
	     
	     
	    try {
	        $api = new SalesforceApiClient($sfLmsAccount);
	        $completedDatetime = Yii::app()->localtime->getUTCNow();
	        $this->updatePlanAttendance($api, $plan, $user, $completedDatetime);
	    }
	    catch (Exception $e) {
	        Yii::log('Salesforce API Exception: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
	    }
	
	     
	}
	
	/**
	 * When Orgchart tree is being created
	 * 
	 * @param DEvent $event
	 */
	public function onBeforeCreateOrgchartTree( DEvent $event ) {
		$sourceData = & $event->params['sourceData'];
		$firstItem = reset( $sourceData );
		if(isset($firstItem['selectable']))
			return;
		
		$allIdOrgs = array_keys( $sourceData );
		
		if( !empty($allIdOrgs) ) {
			// @TODO What if accounts are more than one ?
			$lmsAccount = SalesforceLmsAccount::getFirstAccount();

			if ($lmsAccount) {
				$roots = Yii::app()->db->createCommand()
					->selectDistinct('id_org')
					->from(SalesforceOrgchart::model()->tableName())
					->where('sf_lms_id = :lmsAccount AND active = 1 AND is_root = 1', array(
						':lmsAccount' => $lmsAccount->id_account
					))
					->queryAll();
				foreach($roots as $root){
					$sourceData[$root['id_org']]['customCssClasses'] .= 'salesforce-root'; // root Salesforce branch ( cloud icon )
				}

				/*
				$clients 	= SalesforceOrgchart::model()->findAllBySql("SELECT * from salesforce_orgchart WHERE sf_lms_id = " . $lmsAccount->id_account ." AND id_org IN (" . implode(",", $allIdOrgs) . ") AND active = 1 AND is_root = 0");
				foreach ($clients as $client) {
					$sourceData[$client->id_org]['customCssClasses'] = 'salesforce';
				}
				*/
				$tableName = SalesforceOrgchart::model()->tableName();
				$query = "SELECT DISTINCT(id_org) from ".$tableName." WHERE sf_lms_id = :lms_account AND active = 1 AND is_root = 0"; //AND id_org IN (" . implode(",", $allIdOrgs) . ")
				/* @var $cmd CDbCommand */
				$cmd = Yii::app()->db->createCommand($query);
				$reader = $cmd->query(array(
					':lms_account' => $lmsAccount->id_account
				));
				if ($reader) {
					while ($client = $reader->read()) {
						$id_org = $client['id_org'];
						if (array_key_exists($id_org, $sourceData)) { //this is to avoid the inefficient long IN statement in the query
							$sourceData[$id_org]['customCssClasses'] = 'salesforce';
						}
					}
				}
			}
		}
	}

	
	
	
	
}
