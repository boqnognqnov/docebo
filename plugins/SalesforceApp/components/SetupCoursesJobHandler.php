<?php

class SetupCoursesJobHandler extends JobHandler implements IJobHandler
{


	/**
	 * Job name
	 * @var string
	 */
	const JOB_NAME = 'Salesforce.setupCourses';


	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'SalesforceApp.components.SetupCoursesJobHandler';


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}



	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level

		/* @var $job CoreJob */
		$job = $this->job;
		if (!$job) {
			Yii::log('Invalid job');
			return;
		}
		//---


		try {

			$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();

			// If no account has been set up before, redirect to main page
			if (empty($sfLmsAccount)) {
				throw new CException('Invalid Salesforce account');
			}

			//validate job params
			$jobParams = CJSON::decode($job->params);
			if (empty($jobParams) || !isset($jobParams['process_id'])) {
				throw new CException('Invalid job parameters');
			}
			$runningOperationKey = (isset($jobParams['running_operation']) ? $jobParams['running_operation'] : '');
			$runningIndex = -1; //init value

			//retrieve process info
			$process = SalesforceRunningProcess::model()->findByPk($jobParams['process_id']);
			if (empty($process)) {
				throw new CException('Invalid specified process');
			}
			if ($process->status == SalesforceRunningProcess::STATUS_NOT_STARTED) {
				$process->status = SalesforceRunningProcess::STATUS_RUNNING;
				$process->save();
			}
			$processData = CJSON::decode($process->process_data);
			if (empty($processData) || !isset($processData['subprocesses']) || empty($processData['subprocesses']) || !is_array($processData['subprocesses'])) {
				throw new CException('Invalid process data');
			}
			$subProcesses = $processData['subprocesses'];

			//retrieve array index of {running key}
			$found = false;
			$counter = 0;
			while (!$found && $counter < count($subProcesses)) {
				if (isset($subProcesses[$counter])) {
					if ($subProcesses[$counter]['key'] == $runningOperationKey) {
						$found = true;
						$runningIndex = $counter;
					}
				}
				$counter++;
			}

			//check if we had effectively retrieved the subprocess
			if ($runningIndex < 0) {
				throw new CException('Invalid specified process');
			}

			if ($subProcesses[$runningIndex]['status'] != 'done') {

				//update process info
				$subProcesses[$runningIndex]['status'] = 'running';
				$processData['subprocesses'] = $subProcesses;
				$process->process_data = CJSON::encode($processData);
				$process->save();

				//=======================
				//=== executing stuff ===
				//=======================

				$api = new SalesforceApiClient($sfLmsAccount);
				$syncAgent = new SalesforceSyncAgent();

				$processKey = $subProcesses[$runningIndex]['key'];
				$checkForMetadataAsynchronousJobs = false;
				try {

					if (isset($jobParams['check_metadata_jobs']) && $jobParams['check_metadata_jobs']) {

						Yii::log('checking for running metadata jobs');

						//first check if some metadata jobs are still running
						$metadataJobsInProgress = false;
						$allSalesforceSyncManualJobs = CoreJob::model()->findAllByAttributes(array(
							'name' => 'SalesforceSyncManual',
							'type' => 'onetime',
							'active' => 1
						));
						if (is_array($allSalesforceSyncManualJobs) && !empty($allSalesforceSyncManualJobs)) {
							foreach ($allSalesforceSyncManualJobs as $salesforceSyncManualJob) {
								$tmp = CJSON::decode($salesforceSyncManualJob->params);
								if (!empty($tmp) && isset($tmp['type']) && $tmp['type'] == 'update_metadata') {
									$metadataJobsInProgress = true;
									break;
								}
							}
						}

						//if yes, act accordingly
						if ($metadataJobsInProgress) {
							$checkForMetadataAsynchronousJobs = true;
							Yii::log('metadata jobs still in progress ...');
						} else {
							Yii::log('no metadata jobs currently in progress');
						}

					} else {

						Yii::log('Running creation job: '.$processKey);

						switch ($processKey) {

							case 'CO1':
								// Creating Custom Objects
								// COURSE/PLAN
								if (!$api->customObjectExists(SalesforceApiClient::CO_COURSE)) {

									$addProductField = $syncAgent->addProductField($sfLmsAccount->replicate_courses_as_products);
									Yii::log('Creating Courses Custom Object');
									$result = $api->createCourseCustomObject($addProductField);
									/**
									 * replicate_courses_highest_history represents the highest setting the LMS account has ever had for replicate_courses_as_products
									 * if value = 0 --> Configuration never had product replication active
									 * if value = 1 --> Currently or previously, product replication has been active, but not Sales Orders
									 * if value = 2 --> Currently or previously, product replication and Sales Orders have been active
									 * Based on this value, we're able to determine whether or not to add extra fields to the Custom Objects in Salesforce
									 */
									Yii::app()->db->createCommand()
										->update(SalesforceLmsAccount::model()->tableName(), array(
											'replicate_courses_highest_history' => $sfLmsAccount->replicate_courses_as_products
										), 'id_account = :idAccount', array(
											':idAccount' => $sfLmsAccount->id_account
										));

									Yii::log(var_export($result, true));
									if ($result->result->success == false) {
										//return false;
										throw new CException('Error while creating Custom Object 1');
									} else {
										$dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'created');
									}
								} elseif (($sfLmsAccount->replicate_courses_as_products > $sfLmsAccount->replicate_courses_highest_history)
									&&
									$sfLmsAccount->replicate_courses_highest_history == SalesforceLmsAccount::TYPE_REPLICATE_NONE
								) {

									$result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE, SalesforceApiClient::$COURSE_CUSTOM_OBJECT_FIELDS, 'Product');

									Yii::log(var_export($result, true));
									if ($result->result->success == true) {
										$dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'updated');
									}
								}
								$checkForMetadataAsynchronousJobs = true;
								break;

							case 'CO2':
								// ENROLLMENTS
								if (!$api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)) {
									$addOrderField = false;
									if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
										$addOrderField = true;
									}

									$addProductField = $syncAgent->addProductField($sfLmsAccount->replicate_courses_as_products);

									Yii::log('Creating Enrollments Custom Object');
									$result = $api->createEnrollmentCustomObject($addProductField, $addOrderField);
									Yii::log(var_export($result, true));
									if ($result->result->success == false) {
										//return false;
										throw new CException('Error while creating Custom Object 2');
									} else {
										$dataInfo['CO2'] = 'Course Custom Object' . ' ' . Yii::t('salesforce', 'created');
									}
								} elseif ($sfLmsAccount->replicate_courses_as_products > $sfLmsAccount->replicate_courses_highest_history) {
									if ($sfLmsAccount->replicate_courses_as_products >= SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
										$result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Product');
									}
									if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
										$result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Order');
									}
									if ($result->result->success == true) {
										$dataInfo['CO2'] = 'Enrollment Custom Object' . ' ' . Yii::t('salesforce', 'updated');
									}

									Yii::app()->db->createCommand()
										->update(SalesforceLmsAccount::model()->tableName(), array(
											'replicate_courses_highest_history' => $sfLmsAccount->replicate_courses_as_products
										), 'id_account = :idAccount', array(
											':idAccount' => $sfLmsAccount->id_account
										));
								}
								$checkForMetadataAsynchronousJobs = true;
								break;

							case 'ProductFamily':
								// Creating or Updating product Family
								if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
									Yii::log('Creating Product Family');
									$result = $api->updateProduct2FamilyPicklist();
									Yii::log(var_export($result, true));

									if ($result->result->success == false) {
										//// return false;
										throw new CException('Error while creatingProduct Family');
									} else {
										$dataInfo['ProductFamily'] = 'Product Family' . ' ' . Yii::t('salesforce', 'created');
									}
								}
								break;

							case 'PriceBook':
								if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
									// Create Pricebook
									Yii::log('Creating Pricebook');
									$result = $api->createPriceBook();
									Yii::log(var_export($result, true));
									if ($result === false) {
										//return false;
										throw new CException('Error while creating Pricebook');
									}
									$dataInfo['Pricebook'] = 'Pricebook' . ' ' . Yii::t('salesforce', 'created');
								}
								break;

							case 'OrderType':
								// Creating or Updating Order Type
								if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
									Yii::log('Creating Order type');
									$result = $api->updateOrderTypePicklist();
									Yii::log(var_export($result, true));
									if ($result->result->success == false) {
										//return false;
										throw new CException('Error while creating Order Type');
									} else {
										$dataInfo['OrderType'] = 'Order Type' . ' ' . Yii::t('salesforce', 'created');
									}
								}
								break;

						} //end switch

					} //end if (check_metadata_jobs) ...

				} catch (Exception $e) {
					//update process info
					$subProcesses[$runningIndex]['status'] = 'error';
					$subProcesses[$runningIndex]['error_message'] = $e->getMessage();
					$processData['subprocesses'] = $subProcesses;
					$process->process_data = CJSON::encode($processData);
					$process->save();
					throw $e;
				}



				//==========================
				//=== end execution part ===
				//==========================

				//update process info
				if ($checkForMetadataAsynchronousJobs) {
					$subProcesses[$runningIndex]['more_sub_jobs'] = true;
				} else {
					$subProcesses[$runningIndex]['status'] = 'done';
				}
				$processData['subprocesses'] = $subProcesses;
				$process->process_data = CJSON::encode($processData);
				$process->save();
			}

			//prepare next job (if exists)
			$nextRunningIndex = $runningIndex + 1;
			if ($checkForMetadataAsynchronousJobs) {
				/* @var $scheduler SchedulerComponent */
				$scheduler = Yii::app()->scheduler;
				$jobParams = array(
					'process_id' => $process->getPrimaryKey(),
					'running_operation' => $subProcesses[$runningIndex]['key'], //remain in the same operation, until metadata jobs are finished
					'check_metadata_jobs' => true
				);
				$job = $scheduler->createImmediateJob(SetupCoursesJobHandler::JOB_NAME, SetupCoursesJobHandler::id(), $jobParams);
				if (!$job) {
					throw new CException('Error(s) while creating external job');
				}
			} elseif (isset($subProcesses[$nextRunningIndex])) {
				/* @var $scheduler SchedulerComponent */
				$scheduler = Yii::app()->scheduler;
				$jobParams = array(
					'process_id' => $process->getPrimaryKey(),
					'running_operation' => $subProcesses[$nextRunningIndex]['key']
				);
				$job = $scheduler->createImmediateJob(SetupCoursesJobHandler::JOB_NAME, SetupCoursesJobHandler::id(), $jobParams);
				if (!$job) {
					throw new CException('Error(s) while creating external job');
				}
			} else {
				$process->status = SalesforceRunningProcess::STATUS_DONE;
				$process->end_date = Yii::app()->localtime->getLocalNow();
				$process->save();
			}

		} catch(Exception $e) {
			Yii::log('Salesforce Setup Courses process, an error occurred: '.$e->getMessage());
			if (isset($process) && is_object($process)) {
				$process->status = SalesforceRunningProcess::STATUS_ERROR;
				if (!isset($processData) || !is_array($processData)) {
					$processData['error_message'] = $e->getMessage();//Yii::t('standard', '_OPERATION_FAILURE');
					$process->process_data = CJSON::encode($processData);
				}
			}
		}

	}

}