<?php
/**
 *
 *
 *
 */
class SalesforceApiClient extends CComponent
{

	const MAX_OBJECTS_API_CALL 		= 100;
	
	const ITEM_TYPE_COURSE 			= 'course';
	const ITEM_TYPE_LEARNING_PLAN	= 'learning_plan';
	
    /**
     * Product families
     * @var string
     */
	const FAMILY_ELEARNING 			= 'DOCEBO_E_Learning';
	const FAMILY_CLASSROOM 			= 'DOCEBO_ILT';
	const FAMILY_WEBINAR 			= 'DOCEBO_Webinar';
	const FAMILY_LEARNING_PLAN 		= 'DOCEBO_Learning_plan';


	/**
	 * User enrollment status in a course
	 * @var string
	 */
	const ENROLLSTATUS_SUBSCRIBED   = 'SUBSCRIBED';
	const ENROLLSTATUS_IN_PROGRESS  = 'IN_PROGRESS';
	const ENROLLSTATUS_COMPLETED    = 'COMPLETED';
	/**
	 * Product families definition (replicating Course types and includes Learning plan .. as sort of Course type)
	 * @var unknown
	 */
    static $PRODUCT_FAMILIES =  array(
        
        LearningCourse::TYPE_ELEARNING => array(
            'sf_name'   => self::FAMILY_ELEARNING,
            'attributes'    => array('default'	=> true),
        ), 
        LearningCourse::TYPE_CLASSROOM => array(
            'sf_name'   => self::FAMILY_CLASSROOM,
            'attributes'    => array(),
        ),
        LearningCourse::TYPE_WEBINAR => array(
            'sf_name'   => self::FAMILY_WEBINAR,
            'attributes'    => array(),
        ),
        self::ITEM_TYPE_LEARNING_PLAN => array(
            'sf_name'   => self::FAMILY_LEARNING_PLAN,
            'attributes'    => array(),
        ),
        
    );
    
    
	/**
	 * @var string Dummy account we create for catch all sales orders (if any)
	 */
    const DUMMY_ACCOUNT = 'AccountDmy';
    
	/**
	 * @var string Default Pricebook name in SF
	 */
	const DOCEBO_PRICEBOOK_NAME = 'DOCEBO_PB';
	
	/**
	 * 
	 * @var string
	 */
	const DOCEBO_ORDER_TYPE	= 'DOCEBO_OT';
		
	
	/**
	 * @var string Course custom object name
	 */
    const CO_COURSE = 'DOCEBO1_CO__c';

    /**
     * @var string Enrollment custom object name in SF
     */
    const CO_COURSE_ENROLLMENT = 'DOCEBO2_CO__c';

    /**
     * @var integer Number of records per API call (chunk)
     */
    const CHUNK_SIZE = 200;

    /**
     * @var string Partner WSDL schema file name
     */
    const WSDL_SCHEMA = "partner.wsdl.xml";
    const WSDL_SCHEMA_SANDBOX   = "partner.wsdl.sandbox.xml";

    /**
     * @var string Metadata WSDL schema file name
     */
    const METADATA_WSDL_SCHEMA = "metadata.wsdl.xml";

    /**
     * @var string A rpofile [name] which must exists in Salefroce and which the current SF account must be assigned to
     */
    const REQUIRED_PROFILE_NAME = "Docebo API";
    

    /**
     * Represents a NONE date (sometimes we HAVE to put some date in a SF datetime field, but to consider it NONE/NULL/FALSE)
     * @var unknown
     */
    const NONE_DATE = '1900-01-01T00:00:00+0000';

	/**
	 * Represents value in the custom object fileds array
	 * @var unknown
	 */
	const ORDER_CUSTOM_OBJECT_FIELD = 'Order__c';

    /**
     * @var array List of logged in user's permissions to check during T1 checks (account consistency check)
     */
    static $ACCOUNT_CONSISTENCY_PERMISSIONS = array(
        'ModifyAllData',
        'PasswordNeverExpires',
        'ApiEnabled'
    );

    /**
     * Contact Fields (for retrieval)
     * @var array 
     */
    static $CONTACT_FIELDS = array(
        'Id',
        'Email',
        'FirstName',
        'LastName',
        'CreatedDate',
        'LastModifiedDate',
        'AccountId'
    );

    /**
     * User fields (for retrieval)
     * @var array 
     */
    static $USER_FIELDS = array(
        'Id',
        'Username',
        'Email',
        'FirstName',
        'LastName',
        'CreatedDate',
        'LastModifiedDate',
        'TimeZoneSidKey'
    );
    
    /**
     * Contract fields (for retrieval)
     * 
     * @var array 
     */
    static $CONTRACT_FIELDS = array(
   		'Id',
   		'AccountId',
   		'Pricebook2Id',
   		'StartDate',
   		'EndDate',
   		'OwnerId',
   		'Status',
   		'ActivatedById',
   		'ActivatedDate',
   		'StatusCode',
   		'IsDeleted',
   		'ContractNumber',
   		'CreatedDate'
    );
    

    /**
     * Pricebook fields (for retrieval)
     * 
     * @var array 
     */
    static $PRICEBOOK_FIELDS = array(
   		'Id',
   		'Name',
   		'IsActive',
   		'IsStandard',
    );
    
    
    /**
     * Product fields (for retrieval)
     * 
     * @var array 
     */
    static $PRODUCT_FIELDS = array(
    	'Id',
    	'Name',	
		'Description', 
    	'Family',
    	'IsActive',
    	'IsDeleted',
		'ProductCode',
    );
    
    /**
     * Account fields (for retrieval)
     * 
     * @var array 
     */
    static $ACCOUNT_FIELDS = array(
        'Id',
        'Name',
        'LastModifiedDate',
        'ParentId'
    );
    
    /**
     * ALL COURSE fields (for retrieval)
     * 
     * @var array 
     */
    static $COURSE_FIELDS_ALL = array(
        'Name',
        'C_DC_COURSE_CODE__c',
        'D_CREATE__c',
        'D_DELETE__c',
        'C_DC_COURSE_DESC__c',
        'C_DC_COURSE_LANG__c',
        'C_DC_COURSE_NAME__c',
        'N_REVISION__c',
        'C_DC_COURSE_TYPE__c',
        'D_UPDATE__c',
    );
    
    /**
     * List of COURSE fields (for retrieval)
     * 
     * @var array 
     */
    static $COURSE_FIELDS = array(
            'Id',
    		'Name',
    );
    
    /**
     * Course Custom Object fields definition
     * 
     * @var array
     */
    static $COURSE_CUSTOM_OBJECT_FIELDS = array(

        'C_DC_COURSE_ID__c'         => array(
            'isNameField'       => true,
            'type'              => 'Text',
            'label'             => 'Course ID',
            'length'            => 16,
            'caseSensitive'     => true,
            'unique'            => true,
            'externalId'        => true,
            'required'          => true,
        ),
        
        'C_DC_COURSE_CODE__c'       => array(
            'type'              => 'Text',
            'label'             => 'Course Code',
            'length'            => 50,
            'required'          => false,
       		'trackHistory'      => true,        		
        ),        
        
        'C_DC_COURSE_NAME__c'       => array(
            'type'              => 'Text',
            'label'             => 'Course Name',
            'length'            => 255,
            'required'          => true,
       		'trackHistory'      => true,        		
        ),
        
        'C_DC_COURSE_DESC__c'       => array(
            'type'              => 'TextArea',
            'label'             => 'Description',
            'required'          => false,
       		'trackHistory'      => true,        		
        ),
        
        'C_DC_COURSE_LANG__c'       => array(
            'type'              => 'Text',
            'label'             => 'Language',
            'length'            => 100,
            'required'          => true,
       		'trackHistory'      => true,        		
        ),
        
        'C_DC_COURSE_TYPE__c'       => array(
            'type'              => 'Text',
            'label'             => 'Course type',
            'length'            => 255,
            'required'          => true,
       		'trackHistory'      => true,        		
        ),
        
        'D_CREATE__c'           => array(
            'type'              => 'DateTime',
            'label'             => 'Created',
            'required'          => true,
       		'trackHistory'      => true,        		
        ),

        'D_UPDATE__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Updated',
            'required'          => true,
       		'trackHistory'      => true,        		
        ),
        
        'D_DELETE__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Deleted Datetime',
            'required'          => false,
       		'trackHistory'      => true,        		
        ),
        
        'N_REVISION__c'           => array(
            'type'              => 'Number',
            'label'             => 'Revision',
            'precision'         => 18,
            'scale'             => 0,
            'required'          => true,
       		'trackHistory'      => true,        		
        ),
        
        'Product__c'     => array(
            'type'              => 'Lookup',
            'label'             => 'Product',
            'referenceTo'       => 'Product2',
            'relationshipName'  => 'Course_Product',
            'required'          => false,
            'trackHistory'      => false,
        ),
        
        
        
    );
    
    /**
     * Enrollment Custom object fields definition
     * 
     * @var array
     */
    static $ENROLLMENT_CUSTOM_OBJECT_FIELDS = array(

        'C_DC_ENROLL_ID__c'         => array(
            'isNameField'       => true,
            'type'              => 'Text',
            'label'             => 'Enroll ID',
            'length'            => 64,
            'caseSensitive'     => true,
            'unique'            => true,
            'externalId'        => true,
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'C_DC_COURSE_ID__c'     => array(
            'type'              => 'MasterDetail',         		
            'label'             => 'Course ID',
            'referenceTo'       => self::CO_COURSE,
            'relationshipName'  => 'Enrollment_Course',
            'required'          => false,
            'trackHistory'      => true,
        ),
        
        'C_SF_PRODUCT_ID__c'       => array(
            'type'              => 'Text',
            'label'             => 'Product ID',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'C_SF_USER_ID__c'       => array(
            'type'              => 'Text',
            'label'             => 'User ID',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'C_SF_USER_TYPE__c'       => array(
            'type'              => 'Text',
            'label'             => 'User Type',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'C_DC_COURSE_STATUS__c'       => array(
            'type'              => 'Text',
            'label'             => 'Status',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'C_SF_SO_ID__c'       => array(
            'type'              => 'Text',
            'label'             => 'SO ID',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'N_DC_COURSE_SCORE__c'       => array(
            'type'              => 'Number',
            'label'             => 'Score',
            'precision'         => 18,
            'scale'             => 4,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'N_DC_MAX_COURSE_SCORE__c'       => array(
            'type'              => 'Number',
            'label'             => 'Max Score',
            'precision'         => 18,
            'scale'             => 4,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'C_DC_TIMEZONESID__c'       => array(
            'type'              => 'Text',
            'label'             => 'Timezone',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'C_SF_EVENT_ID__c'       => array(
            'type'              => 'Text',
            'label'             => 'Event ID',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'C_SF_TASK_ID__c'       => array(
            'type'              => 'Text',
            'label'             => 'Task ID',
            'length'            => 64,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'D_DC_ENROLL__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Enroll Datetime',
            'trackHistory'      => true,
            'required'          => true,
        ),
        
        'D_DC_COURSE_COMP__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Completed datetime',
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'D_DC_UNENROLL__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Unenrolled datetime',
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'D_DC_COURSE_FA__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'First Access',
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'D_DC_COURSE_LA__c'       => array(
            'type'              => 'DateTime',
            'label'             => 'Last Access',
            'trackHistory'      => true,
            'required'          => false,
        ),
        
        'N_DC_COMPLETION_PCT__c'       => array(
            'type'              => 'Number',
            'label'             => 'Completion Percentage',
            'precision'         => 18,
            'scale'             => 4,
            'trackHistory'      => true,
            'required'          => false,
        ),

        'PATH_COMPLETION__c'  => array(
            'type'              => 'Text',
            'label'             => 'Path completion',
            'length'            => 10,
            'trackHistory'      => false,
            'required'          => false,
        ),
        
        'N_DC_TIME_IN_COURSE__c'       => array(
            'type'              => 'Number',
            'label'             => 'Time in course',
            'precision'         => 18,
            'scale'             => 0,
            'trackHistory'      => true,
            'required'          => false,
        ),
        
    	'Contact__c'     => array(
    			'type'              => 'Lookup',
    			'label'             => 'Contact',
    			'referenceTo'       => 'Contact',
    			'relationshipName'  => 'Enrollment_Contact',
    			'required'          => false,
    			'trackHistory'      => false,
    	),
    		
    	'User__c'     => array(
    			'type'              => 'Lookup',
    			'label'             => 'User',
    			'referenceTo'       => 'User',
    			'relationshipName'  => 'Enrollment_User',
    			'required'          => false,
    			'trackHistory'      => false,
    	),
    		
    	self::ORDER_CUSTOM_OBJECT_FIELD  => array(
    			'type'              => 'Lookup',
    			'label'             => 'Order',
    			'referenceTo'       => 'Order',
    			'relationshipName'  => 'Enrollment_Order',
    			'required'          => false,
    			'trackHistory'      => false,
    	),
    		
    	'Product__c'     => array(
    			'type'              => 'Lookup',
    			'label'             => 'Product',
    			'referenceTo'       => 'Product2',
    			'relationshipName'  => 'Enrollment_Product',
    			'required'          => false,
    			'trackHistory'      => false,
    	),
    );

    /**
     * Connection
     *
     * @var SforcePartnerClient
     */
    private $connection;

    /**
     * Metadata Connection
     *
     * @var SforceMetadataClient
     */
    private $metadataConnection;

    /**
     * @var stdClass Salesforce login data
     */
    private $loginData;

    /**
     * @var string WSDL schema file name
     */
    private $wsdl;

    /**
     * @var string Path to SalesforceApp base folder
     */
    private $basePath;

    /**
     * @var string Path to Salesforce Soapclient base folder
     */
    private $soapClientPath;

    /**
     * @var string Holds the username
     */
    private $username;

    /**
     * @var string Holds the password
     */
    private $password;

    /**
     * @var string Holds the token
     */
    private $token;

    /**
     * @var string Keep last error in this attribute
     */
    private $error;

    
    /**
     * @var array Price Book data
     */
    private $priceBookInfo;
    
    /**
     * 
     * @var SalesforceLmsAccount
     */
    private $sfLmsAccount;
    
    /**
     * @var stdClass Various data, collected during client initialization
     */
    public $data;
    
    // private

    /**
     * Map between Core LMS enrollment status and Salesforce textual representation
     * @return array
     */
    public function enrollmentStatusMap() {
    	$map = array(
	   		LearningCourseuser::$COURSE_USER_SUBSCRIBED 	=> self::ENROLLSTATUS_SUBSCRIBED,	
    		LearningCourseuser::$COURSE_USER_BEGIN 			=> self::ENROLLSTATUS_IN_PROGRESS,
   			LearningCourseuser::$COURSE_USER_END 			=> self::ENROLLSTATUS_COMPLETED,
    	);
    	return $map;
    }
    
	/**
	 * 
	 * @param number $lmsStatus
	 */    
    public function convertCourseEnrollmentStatus($lmsStatus) {
    	$map = $this->enrollmentStatusMap();
    	if (isset($map[$lmsStatus]))
    		return $map[$lmsStatus];
    	return false;
    }


	/**
	 * Constructor
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $token
	 */
	public function __construct($sfLmsAccountModel)
	{
		try {
			$this->sfLmsAccount = $sfLmsAccountModel;
			if ($this->sfLmsAccount instanceof SalesforceLmsAccount) {
				/**
				 * Checks if we need to decrypt the records. Recods, are no need to be decrypted, only when checking the authentication
				 */
				$securityManager = new CSecurityManager();
				$securityManager->setEncryptionKey(SalesforceLmsAccount::getEncryptionKey());
				$securityManager->cryptAlgorithm = SalesforceLmsAccount::getCryptAlgorithm();

				//Check if some Salesforce accounts are not encrypted
				if ($this->sfLmsAccount->getScenario() === 'update') {
					if (!$this->sfLmsAccount->isBase64($this->sfLmsAccount->agent) && !$this->sfLmsAccount->isBase64($this->sfLmsAccount->password) && !$this->sfLmsAccount->isBase64($this->sfLmsAccount->token)) {
						//Just call save method and beforeSave will encrypt the fields!
						$this->sfLmsAccount->setScenario('encrypt');
						$this->sfLmsAccount->save();
					}
				}

				$this->username = (($this->sfLmsAccount->isBase64($this->sfLmsAccount->agent)) ? $securityManager->decrypt(base64_decode($this->sfLmsAccount->agent)) : $this->sfLmsAccount->agent);
				$this->password = (($this->sfLmsAccount->isBase64($this->sfLmsAccount->password)) ? $securityManager->decrypt(base64_decode($this->sfLmsAccount->password)) : $this->sfLmsAccount->password);
				$this->token = (($this->sfLmsAccount->isBase64($this->sfLmsAccount->token)) ? $securityManager->decrypt(base64_decode($this->sfLmsAccount->token)) : $this->sfLmsAccount->token);
				$this->init();
			} else {
				throw new Exception('Invalid Salesforce account');
			}
		} catch (Exception $e) {
			$this->error = $e->getMessage();
		}


	}



	/**
	 * Initialize
	 */
	public function init()
	{

		// $proxy = new ProxySettings();
		// $proxy->host = '127.0.0.1';
		// $proxy->port = '8888';

		// Non-meta connection
		$this->basePath = Yii::getPathOfAlias('SalesforceApp');
		$this->soapClientPath = Yii::getPathOfAlias('SalesforceApp.soapclient');
		$this->connection = new SforcePartnerClient();
		
		$wsdlSchema = $this->sfLmsAccount->use_sandbox ? self::WSDL_SCHEMA_SANDBOX : self::WSDL_SCHEMA;
		
		$this->connection->createConnection($this->soapClientPath . DIRECTORY_SEPARATOR . $wsdlSchema, $proxy);
		$this->loginData = $this->connection->login($this->username, $this->password . $this->token);
		if ($this->loginData) {
			$this->collectData();

			// Now create Metadata connection (used to customize Salesforce organization meta-data)
			$metadataWsdl = $this->soapClientPath . DIRECTORY_SEPARATOR . self::METADATA_WSDL_SCHEMA;
			$this->metadataConnection = new SforceMetadataClient($metadataWsdl, $this->loginData, $this->connection);
		}
	}



    /**
     * Check API response consistency
     */
    protected function checkApiResponse($response, $contextDescription)
    {
        if (! $response instanceof stdClass) {
            Yii::log("Invalid API response: $contextDescription");
            throw new Exception(Yii::t('salesforce', 'Invalid API response'));
        }
        return true;
    }
    

    /**
     * Check validity of getItems() method response
     * @param unknown $response
     */
    protected function checkGetItemsResponse($response) {
    	if (!is_array($response) || !isset($response['pageCount']) || !isset($response['data']) ) return false;
		return true;
    }

    
    /**
     * General method to generate Picklist field
     * 
     * @param unknown $objectFullName
     * @param unknown $fieldFullName
     * @param unknown $picklistValues
     */
    protected function generatePicklistField($objectFullName, $fieldFullName, $picklistValues, $attributes=array()) {
    	
    	$customField = new SforceCustomField();
    	$customField->setFullName("$objectFullName.$fieldFullName");
    	$customField->setType("Picklist"); // This is important
    	
    	$pickList = new SforcePicklist();
    	$plValues = array();

    	foreach ($picklistValues as $pickValueName => $pickValueAttributes) {
    		$plValue = new SforcePicklistValue();
    		$plValue->setFullName($pickValueName);
    		foreach ($pickValueAttributes as $attrName => $attrValue) {
    			$plValue->$attrName = $attrValue;
    		}
    		array_push($plValues, $plValue);
    	}

    	$pickList->setPicklistValues($plValues);
    	$customField->setPicklist($pickList);
    	
    	foreach ($attributes as $attrName => $attrValue) {
    		$customField->$attrName = $attrValue;
    	}
    	
    	return $customField;
    	 
    	
    }
    
    

    /**
     * Generate Custom Fields from a fields definition array
     * @param array $fieldsDefinition (see self::$COURSE_CUSTOM_OBJECT_FIELDS)
     */
    protected function generateCustomObjectFields($fieldsDefinition, $addOrderField = false) {
        $fields = array();
        foreach ($fieldsDefinition as $fullName => $fieldAttributes) {
	        if($fullName == self::ORDER_CUSTOM_OBJECT_FIELD && !$addOrderField){
		        continue;
	        }

	        $customField = new SforceCustomField();
	        $customField->fullName = $fullName;
	        foreach ($fieldAttributes as $attrName => $attrValue) {
		        $customField->$attrName = $attrValue;
	        }
	        if ($customField->isNameField) {
		        $nameField = clone $customField;
	        }
	        else {
		        array_push($fields, $customField);
	        }
        }
        
        $data = array(
            'fields'        => $fields,
            'nameField'     => $nameField,
        );
        
        return $data;
    }
    
    
    
    /**
     * Generate Course object used for Salesforce Custom Object creation later
     */
    protected function generateCourseCustomObject($addProductField = false)
    {
        
        // Custom object
        $customObject = new SforceCustomObject();
        $customObject->fullName = self::CO_COURSE;
        $customObject->deploymentStatus = DEPLOYMENT_STATUS_DEPLOYED;
        $customObject->sharingModel = SHARING_MODEL_READWRITE;
        $customObject->setEnableSharing(true);
        $customObject->setEnableReports(true);
        $customObject->setEnableActivities(true);
        $customObject->setEnableBulkApi(true);
        $customObject->setDescription("LMS Courses");
        $customObject->setEnableDivisions(false);
        $customObject->setEnableHistory(true);
        $customObject->setHousehold(false);
        $customObject->setLabel("LMS Course");
        $customObject->pluralLabel = 'LMS Courses';

        // Generate fields
        $fieldsData = $this->generateCustomObjectFields(self::$COURSE_CUSTOM_OBJECT_FIELDS);
        if(!$addProductField){
            foreach($fieldsData['fields'] as $key => $fields){
                if($fields->label == 'Product'){
                    unset($fieldsData['fields'][$key]);
                }
            }
        }

        // Set custon object's fields
        $customObject->setFields($fieldsData['fields']);
        
        // Set NAME field
        $customObject->nameField = $fieldsData['nameField'];
        
        return $customObject;
    }

    /**
     * Generate Course object used for Salesforce Custom Object creation later
     */
    protected function generateFieldForAddition($masterCustomObject, $fieldsList, $fieldLabel)
    {
        $fieldsData = $this->generateCustomObjectFields($fieldsList, true);
        $field = null;
        foreach($fieldsData['fields'] as $key => $fields){
            if($fields->label != $fieldLabel){
                unset($fieldsData['fields'][$key]);
            }
            else
                $field = $fieldsData['fields'][$key];
        }
        $field->fullName = $masterCustomObject . '.' . $field->fullName;

        return $field;
    }
    
    /**
     * Set given custom object's fields visibe
     * 
     * @param string $customObjectName
     * @param array $fieldsDefinition
     */
    protected function setCustomObjectFieldsVisible($customObjectName, $fieldsDefinition) {
        $fields = array();
        foreach ($fieldsDefinition as $fullName => $fieldAttributes) {
        	
        	//Exculde "required" filds and fiels with relationship "MasterDetail"
            if (!$fieldAttributes['required'] && $fieldAttributes['type'] !== 'MasterDetail') {
                if ( !isset($fieldAttributes['isNameField']) || !$fieldAttributes['isNameField']) {
                    $fields[] = $customObjectName . "." . $fullName;
                }
            }
        }
        $response = $this->setFieldsVisible($fields);
        return $response;
    }
    
    

    /**
     * 
     */
    protected function setCourseFieldsVisible($addProductField) {
        $fields = self::$COURSE_CUSTOM_OBJECT_FIELDS;
        if(!$addProductField){
            unset($fields['Product__c']);
        }
        return $this->setCustomObjectFieldsVisible(self::CO_COURSE, $fields);
    }
    
    /**
     *
     */
    protected function setEnrollmentFieldsVisible($addProductField = false, $addOrderField = true) {
	    $fields = self::$ENROLLMENT_CUSTOM_OBJECT_FIELDS;
	    if(isset($fields[SalesforceApiClient::ORDER_CUSTOM_OBJECT_FIELD]) && !$addOrderField ){
		    unset($fields[SalesforceApiClient::ORDER_CUSTOM_OBJECT_FIELD]);
	    }
        if(!$addProductField){
            unset($fields['Product__c']);
        }

        return $this->setCustomObjectFieldsVisible(self::CO_COURSE_ENROLLMENT, $fields);
    }
    
    
    
    /**
     * Generate Course object used for Salesforce Custom Object creation later
     */
    protected function generateEnrollmentCustomObject($addProductField = false, $addOrderField = false)
    {
    
        // Custom object
        $customObject = new SforceCustomObject();
        $customObject->fullName = self::CO_COURSE_ENROLLMENT;
        $customObject->deploymentStatus = DEPLOYMENT_STATUS_DEPLOYED;         
        $customObject->sharingModel = SHARING_MODEL_CONTROLLEDBYPARENT;
        $customObject->setEnableSharing(true);
        $customObject->setEnableReports(true);
        $customObject->setEnableActivities(true);
        $customObject->setEnableBulkApi(true);
        $customObject->setDescription("LMS Course Enrollment");
        $customObject->setEnableDivisions(false);
        $customObject->setEnableHistory(true);
        $customObject->setHousehold(false);
        $customObject->setLabel("LMS Course Enrollment");
        $customObject->setEnableStreamingApi(true);
        $customObject->pluralLabel = 'LMS Course Enrollments';
        
        // Generate fields
        $fieldsData =$this->generateCustomObjectFields(self::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, $addOrderField);
        if(!$addProductField){
            foreach($fieldsData['fields'] as $key => $fields){
                if($fields->label == 'Product'){
                    unset($fieldsData['fields'][$key]);
                }
            }
        }

        // Set custon object's fields
        $customObject->setFields($fieldsData['fields']);
    
        // Set NAME field
        $customObject->nameField = $fieldsData['nameField'];
    
        return $customObject;
    }

    
    protected function generateObjectsToDelete($sfItems) {
    }
    
    
    
    
    /**
     * Generate objects (courses, learning plans) suitable for API calls (update, create, upsert, etc.)
     * Can be used also to set *SOME* items as deleted (BUT they must still exist in LMS!!!!!!!)
     * 
     * @param array $items Array of LMS IDs
     * @param booleat $forDeletion Should we prepare objects for DELETION instead ?
     * @param array $itemsInfoForDeletion  Array of <lms-id> => <sf-id> 
     */
    protected function generateObjectsFromItems($items, $itemType, $forDeletion=false, $itemsInfoForDeletion=array()) {
    	
        // Collect products info first (if Products replication is enabled)
        $sfProductNameToId = array();
        if ($this->sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
            $sfProducts = $this->getProducts(true);
            $sfProductNames = array();
            foreach ($sfProducts as $sfProductInfo) {
                $sfProductNameToId[$sfProductInfo['Name']] = $sfProductInfo['Id'];
            }
        }
        
    	$crit = new CDbCriteria();
    	
    	if ($itemType == self::ITEM_TYPE_COURSE) {
    		$crit->addInCondition('idCourse', $items);
    		$models = LearningCourse::model()->findAll($crit);
    	}
    	else if ($itemType == self::ITEM_TYPE_LEARNING_PLAN) {
    		$crit->addInCondition('id_path', $items);
    		$models = LearningCoursepath::model()->findAll($crit);
    	}
    	else 
    		return array();
    	
    	$objects = array();
    	    
    	foreach ($models as $model) {
    		
    	    $isSelling     = false;
    	    $fieldsToNull = array();

            $unsetDeletionDate = false;

    		if ($itemType == self::ITEM_TYPE_COURSE) {
    		    
    		    $objectName    = 'C' . $model->idCourse;
    		    $isSelling = $model->selling > 0; 
    		    
	    	    $createDate = Yii::app()->localtime->fromLocalDateTime($model->create_date);
	    	    $fields = array();
                $utcNow = Yii::app()->localtime->getUTCNow();
	    	    if ($forDeletion && !empty($itemsInfoForDeletion[$model->idCourse])) {

	    	        $fields = array (
	    	            'Id'                   => $itemsInfoForDeletion[$model->idCourse],
	    	            'D_DELETE__c'          => $utcNow,
	    	        );
	    	    }
	    	    else {
	        		$fields = array (
	        			'Name'                       => $objectName,
	        			'C_DC_COURSE_CODE__c'        => "<![CDATA[" . $model->code . "]]>",
	        			'C_DC_COURSE_NAME__c'        => "<![CDATA[" . $model->name . "]]>",
	        			'C_DC_COURSE_DESC__c'        => "<![CDATA[" . Docebo::ellipsis(strip_tags($model->description), 240, '...') . "]]>",
	        			'C_DC_COURSE_LANG__c'        => $model->lang_code,
	        			'C_DC_COURSE_TYPE__c'        => $model->course_type,
	        			'D_CREATE__c'                => $createDate,
	        		    'D_UPDATE__c'                => $utcNow,
	        			'N_REVISION__c'              => 1,
                    );
                    $unsetDeletionDate = true;
	    	    }
    		}
    		else if ($itemType == self::ITEM_TYPE_LEARNING_PLAN) {
    		    
    		    $objectName   = 'L' . $model->id_path;
    		    $isSelling    = $model->is_selling > 0;
    		    
    			// Learning plan dates are NOT auto-converted to Local
    			$createDate = Yii::app()->localtime->toUTC($model->create_date);
                $utcNow = Yii::app()->localtime->getUTCNow();

    			$fields = array();

    			if ($forDeletion && !empty($itemsInfoForDeletion[$model->id_path])) {

    				$fields = array (
   						'Id'                   => $itemsInfoForDeletion[$model->id_path],
   						'D_DELETE__c'          => $utcNow,
    				);
    			}
    			else {
    				$fields = array (
   						'Name'                       => $objectName,
   						'C_DC_COURSE_CODE__c'        => "<![CDATA[" . $model->path_code . "]]>",
   						'C_DC_COURSE_NAME__c'        => "<![CDATA[" . $model->path_name . "]]>",
   						'C_DC_COURSE_DESC__c'        => "<![CDATA[" . Docebo::ellipsis(strip_tags($model->path_descr), 240, '...') . "]]>",
   						'C_DC_COURSE_LANG__c'        => 'english',
   						'C_DC_COURSE_TYPE__c'        => self::ITEM_TYPE_LEARNING_PLAN,
   						'D_CREATE__c'                => $createDate,
   						'D_UPDATE__c'                => $utcNow,
   						'N_REVISION__c'              => 1,
    				);
                    $unsetDeletionDate = true;
    			}
    			 
    		}


            if($this->sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
                // If the item (course or plan) is among existing products
                if (isset($sfProductNameToId[$fields['Name']]) && $isSelling) {
                    $fields['Product__c'] = $sfProductNameToId[$fields['Name']];
                } else {
                    $fieldsToNull[] = 'Product__c';
                }
            }

            if ($unsetDeletionDate)
            {
                $fieldsToNull[] = 'D_DELETE__c';
            }
    		
    		if (!empty($fields)) {
	    		$sObject = new SObject();
    			$sObject->fields = $fields;
    			$sObject->type = self::CO_COURSE;
    			$sObject->fieldsToNull = $fieldsToNull;
    			$objects[] = $sObject;
    		}
    	}

    	return $objects;
    	
    }
    
    
    /**
     * Generate Product objects from courses
     * 
     * @param array $courses Array of course IDs
     */
    protected function generateProductObjectsFromItems($items, $itemType, $sellingOnly=true, $forDeletion=false) {

        /* @var $model LearningCourse */
         
        $crit = new CDbCriteria();

        if ($itemType == self::ITEM_TYPE_COURSE) {
        	// Only Courses for e-commerce ???
        	if ($sellingOnly) $crit->addCondition('selling > 0');
        	$crit->addInCondition('idCourse', $items);
        	$models = LearningCourse::model()->findAll($crit);
        }
        else if ($itemType == self::ITEM_TYPE_LEARNING_PLAN) {
        	// Only Courses for e-commerce ???
        	if ($sellingOnly) $crit->addCondition('is_selling > 0');
        	$crit->addInCondition('id_path', $items);
        	$models = LearningCoursepath::model()->findAll($crit);
        }
        else 
        	return array();
        
        $objects = array();
         
        foreach ($models as $model) {
        	
        	$fields = array();
        	
        	if ($itemType == self::ITEM_TYPE_COURSE) {
        		$createDate = Yii::app()->localtime->fromLocalDateTime($model->create_date);
        		$fields = array (
					'Name'                           => 'C' . $model->idCourse,
        			'ProductCode'			         => $model->code ? $model->code : '',
        			'Description'					 => $model->name,
       				'Family'			             => self::$PRODUCT_FAMILIES[$model->course_type]['sf_name'],
        			'IsActive'					     => 1,
        		);
        	}
        	else if ($itemType == self::ITEM_TYPE_LEARNING_PLAN) {
        		$createDate = Yii::app()->localtime->toUTC($model->create_date);
        		$fields = array (
        			'Name'							 => 'L' . $model->id_path,
        			'ProductCode'					 => $model->path_code ? $model->path_code : '',
       				'Description'			         => $model->path_name,
       				'Family'			             => self::FAMILY_LEARNING_PLAN,
       				'IsActive'					     => 1,
        		);
        	}

        	if (!empty($fields)) {
        		if ($forDeletion) {
        			$fields['IsActive'] = 0;
        		}
        		$sObject = new SObject();
        		$sObject->fields = $fields;
        		$sObject->type = 'Product2';
        		$objects[] = $sObject;
        	}
            
        }
        
        return $objects;
         
    }
    
    
    /**
     * Generate Pricebook entries from products, i.e. associates Product Object with a Pricebook
     *
     * @param boolean $standardPricebook Generate these objects for Standard or for Custom pricebook
     */
    protected function generatePricebookEntriesForProducts($standardPricebook=false) {

    	// Get our products, those having Family equal to one of our product families
    	$products = $this->getProducts(true);

    	// If STANDARD pricebook is requested, get it from SF and throw exception if it does not exists
    	// Because every product must be put in some existing (and Active!) Standard Pricebook before it can go in Custom PB,
    	// hence, non existing Standard Pricebook is bad! Log the error. 
    	if ($standardPricebook) {
    		$pbInfo = $this->getStandardPriceBook();
    		if (!$pbInfo) {
    			Yii::log('Salesforce (Agent: ' . $this->username . '): could not retrieve a Standard & Active pricebook', 'error');
				throw new Exception(Yii::t('salesforce', 'It seems a Standard (and active) pricebook is missing at Salesforce for account {username}', array(
					'{username}' => $this->username,	
				)));
    		}
    	}
    	// Otherwise, get the Custom PB...
    	else {
    		$pbInfo = $this->getPriceBook();
    		if (!$pbInfo) {
    			$this->createPriceBook();
    			$pbInfo = $this->getPriceBook();
    		}
    	}
        // If the SF account has multi currency option enabled, a check must be made if this setting is active
        // When active, at least one of the currencies in Salesforce must match the currency set up in the LMS
        // If not, an error must be thrown
        // If the currency matches, the currency ISO-code must be submitted to the PBE
        $multiCurrency = $this->isMultiCurrencyEnabled($this->sfLmsAccount);
        $currencyCheck = $this->currencyConsistencyCheck($this->sfLmsAccount);
        $appendFields = false;
        if ($multiCurrency) {
            if($currencyCheck){
                $appendFields = true;
                $arAppendFields = array('CurrencyIsoCode' => Settings::get('currency_code', null, false, false));
            } else {
                $consistencyError = $this->getErrorConsistencyCheck($this->sfLmsAccount);
                Yii::log($consistencyError, 'error');
                throw new Exception($consistencyError);
            }
        }

    	$objects = array();
    	foreach ($products as $product) {
    		
    		// We need the price of this Product. Get the price from LMS database (Name = C<courseId>, Familiy=<course type>
    		$price = floatval($this->getLmsEntityPrice($product['Name'], $product['Family']));

    		// Now, we play with "create or update"
    		// First, we assume that "product ID + pricebook ID" is unique
    		// Second, we search for the pricebook entry for the given Product & Pricebook
    		// If it exists, we are in UPDATE mode. Otherwise: INSERT mode
   			$pricebookEntry = $this->getPricebookEntry($product['Id'], $pbInfo['Id']);
    			
   			// PBE exists?  Update mode (product and pb IDs are removed from fields as they can not be changed)
			if ($pricebookEntry) {
   				$fields = array (
   			 		'Id'							=> $pricebookEntry,	
		 			'IsActive'						=> 1,
					'UnitPrice'						=> $price,
   			 		'UseStandardPrice'				=> 'FALSE',
    			);
                if($appendFields) $fields = array_merge($fields, $arAppendFields);
    		}
    		// PB does not exists? Insert mode. Remove PBE ID and put Product ID and Pricebook ID
    		else {
    			$fields = array (
    				'Product2Id'					=> $product['Id'],
    				'Pricebook2Id'					=> $pbInfo['Id'],
    				'IsActive'						=> 1,
    				'UnitPrice'						=> $price,
    				'UseStandardPrice'				=> 'FALSE',
    			);
                if($appendFields) $fields = array_merge($fields, $arAppendFields);
    		}

    		// Doh! The above update/insert took me .. like ... 6 hours!!!
    		
    		$sObject = new SObject();
    		$sObject->fields = $fields;
    		$sObject->type = 'PricebookEntry';
    		$objects[] = $sObject;
    			
    	}
    	
    	return $objects;

    }

    /**
     * Checks if the currency (or currencies if MultiCurrency is enabled) matches the currency in the LMS
     * @param $sfLmsAccount
     * @return bool
     */
    public function currencyConsistencyCheck($sfLmsAccount){
        $api = new SalesforceApiClient($sfLmsAccount);
        $userInfo = $api->connection->getUserInfo();
        $organizationMultiCurrency = $userInfo->organizationMultiCurrency;
        $orgDefaultCurrencyIsoCode = $userInfo->orgDefaultCurrencyIsoCode;
        $lmsCurrency = Settings::get('currency_code', null, false, false);

        $arCurrencies = array();
        if($organizationMultiCurrency){
            $query = "SELECT IsoCode FROM CurrencyType WHERE IsActive = true";
            $currencies = $api->connection->query($query);
            foreach($currencies->records as $record){
                $arCurrencies[] = strip_tags($record->any);
            }
        } else {
            $arCurrencies[] = $orgDefaultCurrencyIsoCode;
        }
        return in_array($lmsCurrency, $arCurrencies);
    }

    public function getErrorConsistencyCheck($sfLmsAccount){
        $api = new SalesforceApiClient($sfLmsAccount);
        $userInfo = $api->connection->getUserInfo();
        $organizationMultiCurrency = $userInfo->organizationMultiCurrency;
        $orgDefaultCurrencyIsoCode = $userInfo->orgDefaultCurrencyIsoCode;
        $lmsCurrency = Settings::get('currency_code', null, false, false);

        $arCurrencies = array();
        if($organizationMultiCurrency){
            $query = "SELECT IsoCode FROM CurrencyType WHERE IsActive = true";
            $currencies = $api->connection->query($query);
            foreach($currencies->records as $record){
                $arCurrencies[] = strip_tags($record->any);
            }
        } else {
            $arCurrencies[] = $orgDefaultCurrencyIsoCode;
        }
        return "The currencies on LMS (".$lmsCurrency.") and your Salesforce account (".implode(', ', $arCurrencies).") do not match!";
    }

    /**
     * Returns true/false if the SF account has MultiCurrency enabled
     * @param $sfLmsAccount
     * @return bool
     */
    public function isMultiCurrencyEnabled($sfLmsAccount){
        $api = new SalesforceApiClient($sfLmsAccount);
        $userInfo = $api->connection->getUserInfo();
        return $userInfo->organizationMultiCurrency;
    }
    

    /**
     *
     * @param array $courses Array of course IDs
     */
    protected function generateObjectsFromEnrollments($enrollments, $itemType, $additionalInfo=false, $status=self::ENROLLSTATUS_SUBSCRIBED, $forDeletion=false) {
        /* @var $model LearningCourseUser */
        
        // Get SF IDs of courses/plans
        $items = array();
        $sfItems = array();
        $typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
        
        foreach ($enrollments as $enrollment) {
            
        	$id = $itemType == self::ITEM_TYPE_COURSE ? $enrollment->idCourse : $enrollment->id_path;
            $items[] = "'$typeLetter" . $id . "'";
        }
        
        $response = $this->getItems(self::CO_COURSE, array('Id', 'Name'), 'Name IN (' . implode(',', $items) . ')' );
        
        if (!$this->checkGetItemsResponse($response)) {
            Yii::log('Could not get list of Salesforce courses for registering enrollments', 'error');
            throw new Exception('Could not get list of Salesforce courses for registering enrollments');
        }
        
        foreach ($response['data'] as $info) {
            $lmsId = (int) preg_replace("/^$typeLetter/i", "", $info['Name']); 
            $sfItems[$lmsId] = $info['Id'];
        }
        
        // No course/plan found to be part of Salesforce Courses/Plans (in custom object CO1) ?  Return NO objects
        if (empty($sfItems)) {
        	return array();
        }
        
        // Generate the array of objects
        $objects = array();
        
        /* @var $sfUser SalesforceUser */
        
        foreach ($enrollments as $enrollment) {
            
        	$idLmsItem 		= ($itemType == self::ITEM_TYPE_COURSE ? $enrollment->idCourse : $enrollment->id_path);
            $idUser     	= $enrollment->idUser;
            
            $userTimezone = Yii::app()->localtime->getTimezoneFromSettings($idUser);
            
            // Get SF user from LMS
            $sfUser = SalesforceUser::model()->findByAttributes(array('id_user_lms' => $idUser));

            if ($itemType == self::ITEM_TYPE_COURSE) {
            	$model = LearningCourseuser::model()->findByAttributes(array(
            		'idCourse'  => $idLmsItem,
            		'idUser'    => $idUser,
            	));
            	$status = $this->convertCourseEnrollmentStatus($model->status); 
            }
            else {
            	$model = LearningCoursepathUser::model()->findByAttributes(array(
           			'id_path'  	=> $idLmsItem,
           			'idUser'    => $idUser,
            	));
            	if(!$forDeletion) $status = self::ENROLLSTATUS_SUBSCRIBED;
            }
            
            $enrollmentName = $typeLetter . $idLmsItem . "_" . $idUser;

            $fieldsToNull = array();
            
            // Delete ?
            if ($forDeletion) {
            	
            	$tmp = $this->getItems(self::CO_COURSE_ENROLLMENT, array(
            		'C_SF_SO_ID__c', 
            		'C_SF_EVENT_ID__c', 
            		'C_SF_TASK_ID__c'
            	), "Name='$enrollmentName'");
            	
            	$sfEnrollment = $tmp['data'][0];
            	
            	if (isset($sfEnrollment['C_SF_SO_ID__c'])) 		$this->deleteOrder($sfEnrollment['C_SF_SO_ID__c']);
            	if (isset($sfEnrollment['C_SF_EVENT_ID__c'])) 	$this->deleteEvent($sfEnrollment['C_SF_EVENT_ID__c']);
            	if (isset($sfEnrollment['C_SF_TASK_ID__c'])) 	$this->deleteTask($sfEnrollment['C_SF_TASK_ID__c']);
            	
            	$fields = array (
            		'Name'                     	=> $enrollmentName,
            		'D_DC_UNENROLL__c'          => Yii::app()->localtime->getUTCNow()
            	);
            	if($status !== false){
                    $fields['C_DC_COURSE_STATUS__c'] = $status;
                }
            }
            else {
                
	            $enrollDate = $itemType == self::ITEM_TYPE_COURSE ? $model->date_inscr : $model->date_assign;
	            $enrollDate = Yii::app()->localtime->fromLocalDateTime($enrollDate);
	            
	            $dateFirstAccess = $itemType == self::ITEM_TYPE_COURSE ? $model->date_first_access : $enrollDate;
	            $dateLastAccess	 = $itemType == self::ITEM_TYPE_COURSE ? $model->date_last_access : $enrollDate;	
	
	            $fields = array (
	                'Name'                          => $enrollmentName,
	                'C_DC_COURSE_ID__c'             => $sfItems[$idLmsItem],
	                'C_SF_USER_ID__c'               => $sfUser->id_user_sf,
	                'C_SF_USER_TYPE__c'             => $sfUser->sf_type,          
	                'C_DC_COURSE_STATUS__c'         => $status,
	                'C_DC_TIMEZONESID__c'           => $userTimezone,   
	                'D_DC_ENROLL__c'                => $enrollDate
	            );

                $fieldsToNull[] = 'D_DC_UNENROLL__c';
	            
	            // Fill the Lookup fields (User or Contact)
	            if ($sfUser->sf_type == SalesforceUser::USER_TYPE_USER) {
	                $fields['User__c'] = $sfUser->id_user_sf;
	            }
	            else if ($sfUser->sf_type == SalesforceUser::USER_TYPE_CONTACT) {
	                $fields['Contact__c'] = $sfUser->id_user_sf;
	            }

                if(!isset($additionalInfo['skip_fa_la_date_creation']) || $additionalInfo['skip_fa_la_date_creation'] == false) {
                    if ($dateFirstAccess) $fields['D_DC_COURSE_FA__c'] = $dateFirstAccess;
                    if ($dateLastAccess) $fields['D_DC_COURSE_LA__c'] = $dateLastAccess;
                } else {
                    $fieldsToNull[] = 'D_DC_COURSE_FA__c';
                    $fieldsToNull[] = 'D_DC_COURSE_LA__c';
                }
	            
	            // Try to find if the course/plan is registered as a product; register the product ID here if YES
	            // But preserve the incoming product ID, if set
	            if (!isset($additionalInfo['productId']) && ($this->sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE)) {
	            	$condition = "Name='${typeLetter}${idLmsItem}' AND IsActive=TRUE";
	            	$products = $this->getItems("Product2", array('Id'), $condition);
	            	if ($this->checkGetItemsResponse($products) && !empty($products['data'])) {
	            		$additionalInfo['productId'] = $products['data'][0]['Id'];
	            	}
	            } 
	            
	            if (isset($additionalInfo['productId'])) 	    $fields['C_SF_PRODUCT_ID__c'] 	= $additionalInfo['productId'];
	            if (isset($additionalInfo['productId'])) 	    $fields['Product__c'] 			= $additionalInfo['productId'];
	            if (isset($additionalInfo['orderId'])) 		    $fields['C_SF_SO_ID__c'] 		= $additionalInfo['orderId'];
	            if (isset($additionalInfo['orderId'])) 		    $fields['Order__c'] 		    = $additionalInfo['orderId'];
	            if (isset($additionalInfo['eventId'])) 		    $fields['C_SF_EVENT_ID__c'] 	= $additionalInfo['eventId'];
	            if (isset($additionalInfo['taskId'])) 		    $fields['C_SF_TASK_ID__c'] 		= $additionalInfo['taskId'];
                if (isset($additionalInfo['path_completion']))  $fields['PATH_COMPLETION__c']   = $additionalInfo['path_completion'];
	             
            
            }
            
            $sObject = new SObject();
            $sObject->fields = $fields;
            $sObject->type = self::CO_COURSE_ENROLLMENT;
            if(!empty($fieldsToNull)){
                $sObject->fieldsToNull = $fieldsToNull;
            }
            $objects[] = $sObject;
        }
    
        return $objects;
         
    }
    
    /**
     */
    protected function collectData()
    {
        
        // Store all data in standard class
        $this->data = new stdClass();
        $this->data->permissions = new stdClass();
        
        // By default, false
        $this->data->permissions->ModifyAllData = false;
        $this->data->permissions->PasswordNeverExpires = false;
        $this->data->permissions->PasswordNeverExpires = false;
        
        // Get profile ID of the *required profile*, e.g. "Docebo API" (if any)
        $query = "SELECT Id,Name FROM Profile WHERE Name = '" . self::REQUIRED_PROFILE_NAME . "'";
        $response = $this->connection->query($query);
        $queryResult = new QueryResult($response);
        
        // If profile is found, get more information (data)
        if ($queryResult->size > 0) {
            $queryResult->rewind();
            
            // Profile ID
            $record = $queryResult->current();
            $this->data->requiredProfileId = $record->Id;
            
            // Collect permissions info
            $query = "
				SELECT Id, ProfileId, PermissionsModifyAllData, PermissionsPasswordNeverExpires,PermissionsApiEnabled 
				FROM PermissionSet WHERE ProfileId = '" . $record->Id . "'
			";
            $response = $this->connection->query($query);
            $queryResult = new QueryResult($response);
            if ($queryResult->size > 0) {
                $record = $queryResult->current();
                $this->data->permissions->ModifyAllData = $record->PermissionsModifyAllData;
                $this->data->permissions->PasswordNeverExpires = $record->PermissionsPasswordNeverExpires;
                $this->data->permissions->ApiEnabled = $record->PermissionsApiEnabled;
            }
        }
        
        return $this;
    }

    
    /**
     * Transform Salesforce Response to an array of common sense data
     * 
     * @param array $response
     */
    protected function transformUpsertResponse($response) {
    	
    	$errors = array();
    	$created = 0;
    	$failed = 0;
    	$succeeded = 0;
    	

    	if (is_array($response) && !empty($response)) {
    		foreach ($response as $r) {
    			if ($r->created) $created++;
    			if (!$r->success) $failed++;
    			else $succeeded++;    			    		
    			
    			if (!empty($r->errors)) $errors[] = $r->errors[0]->message;
    		}
    	}
    	 
    	$result = array(
    		'itemsCount'	=> $succeeded,
    		'errors'		=> $errors,
    		'created'		=> $created,
    		'failed'		=> $failed,
    	);
    	
    	return $result;
    	 
    }
    
    
    /**
     * Change fields permissions
     *
     * @param array $profiles
     * @param array $fields
     * @param boolean $readable
     * @param boolean $editable
     * @param boolean|int $currentChunk
     * @return mixed
     */
    public function setFieldsPermissions($profiles, $fields, $readable=true, $editable=true, $currentChunk = false) {
    	if (!is_array($profiles))
    		$profiles = array($profiles);
    	
    	if (!is_array($fields))
    		$fields = array($fields);
    
    	$objects = array();
    	foreach ($profiles as $id => $profileName) {
    		$profileObj = new SforceProfile();
    		$profileObj->fullName = $profileName; // xxx
    		
    		$fieldPermissions = array();
    		foreach ($fields as $field) {
    		    $tmp = new SForceProfileFieldLevelSecurity();
    		    $tmp->readable = $readable;
    		    $tmp->editable = $editable;
    		    $tmp->field = $field;
    		    $fieldPermissions[] = $tmp;
    		}
    		
    		$profileObj->fieldPermissions = $fieldPermissions;
    		$objects[] = $profileObj;
    	}
        $params = array(
            'profiles' => $profiles,
            'fields' => $fields
        );
    	$response = $this->metadataConnection->updateMetadata($objects, $currentChunk, $params);
    	return $response;
    
    }
    
    /**
     * Create/Update Salesforce Pricebook Entries (in the STANDARD pricebook!) for Our Products in Salesforce
     * Products can not be put in a Custom Pricebook without being first in Standard one.
     *
     * This is NOT public, to avoid calling it separately. It is always called by another public method (search!)
     *
     */
    protected function createOrUpdateStandardPricebookEntries() {
    	$objects = $this->generatePricebookEntriesForProducts(true);  // true is for STANARD pricebook
    	$response = $this->upsertChunked('Id', $objects);
    	return $response;
    }
    
    

    /**
     * Check if Course or Learning plan object is marked as DELETED in
     *
     * @param number $lmsId
     * @param string $itemType
     * @return boolean
     */
    protected function isCourseOrPlanDeleted($lmsId, $itemType) {
    	 
    	$typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
    	$name = $typeLetter.$lmsId;
    	 
    	$condition = " (D_DELETE__c <> NULL) AND (Name='$name') ";
    	$response = $this->getItems(self::CO_COURSE, array('D_DELETE__c'), $condition);
    	 
    	if (!$this->checkGetItemsResponse($response)) {
    		return false;
    	}
    
    	$deleted = count($response['data']) > 0;
    	$deletedDatetime = $deleted ?  $response['data'][0]['D_DELETE__c'] : '';
    	 
    	if ($deleted)
    		return $deletedDatetime;
    	else
    		return false;
    }
    
    /**
     * Check if COURSE is deleted
     *
     * @param number $lmsId
     * @return boolean
     */
    protected function isCourseDeleted($lmsId) {
    	return $this->isCourseOrPlanDeleted($lmsId, self::ITEM_TYPE_COURSE);
    }
    
    /**
     * Check if Learnin PLAN is deleted
     *
     * @param number $lmsId
     * @return boolean
     */
    protected function isLearningPlanDeleted($lmsId) {
    	return $this->isCourseOrPlanDeleted($lmsId, self::ITEM_TYPE_LEARNING_PLAN);
    }


	/**
	 * Update/Insert objects in chunks
	 *
	 * @param string $ext_Id
	 * @param array $sObjects
	 * @return array
	 */
	protected function upsertChunked($ext_Id, $sObjects)
	{
		$chunks = array_chunk($sObjects, self::MAX_OBJECTS_API_CALL);
		$response = array();
		foreach ($chunks as $index => $chunk) {
			$tmpResponse = $this->connection->upsert($ext_Id, $chunk);
			$response = array_merge($response, $tmpResponse);
		}
		return $response;
	}



    /**
     * Update objects in chunks 
     * 
     * @param array $sObjects
     * @return array
     */
	protected function updateChunked($sObjects) {
    	$chunks = array_chunk($sObjects, self::MAX_OBJECTS_API_CALL);
    	$response = array();
    	foreach ($chunks as $index => $chunk) {
    		$tmpResponse = $this->connection->update($chunk);
    		$response = array_merge($response, $tmpResponse);
    	}
    	return $response;
    }
    
    /**
     * Create objects in chunks 
     * 
     * @param array $sObjects
     * @return array
     */
    protected function createChunked($sObjects) {
    	$chunks = array_chunk($sObjects, self::MAX_OBJECTS_API_CALL);
    	$response = array();
    	foreach ($chunks as $index => $chunk) {
    		$tmpResponse = $this->connection->create($chunk);
    		$response = array_merge($response, $tmpResponse);
    	}
    	return $response;
    }
    

    /**
     * Delete objects in chunks 
     * 
     * @param array $ids
     * @return array
     */
    protected function deleteChunked($ids) {
    	$chunks = array_chunk($ids, self::MAX_OBJECTS_API_CALL);
    	$response = array();
    	foreach ($chunks as $index => $chunk) {
    		$tmpResponse = $this->connection->delete($chunk);
    		$response = array_merge($response, $tmpResponse);
    	}
    	return $response;
    }
    
    
    
    protected function generateSsoUrl(CoreUser $user, $idCourse=false, $toLP=false) {
    	
    	$userId = $user->getClearUserId();
    	$time = time();
    	$secret = Settings::get('sso_secret', '8ca0f69afeacc7022d1e589221072d6bcf87e39c');
    	$token = md5($userId.','.$time.','.$secret);
    	
    	$params = array(
    		'login_user'		=> $userId,
    		'auth_regen'		=> 1,
    		'time'				=> $time,
    		'token'				=> $token,				
    	);
    	
    	if ($idCourse) {
    		$params['id_course'] = (int) $idCourse;
    	}
    	
    	if ($toLP) {
    		$params['destination'] = 'learningplan';
    	}
    	
    	$url = Docebo::createAbsoluteLmsUrl('site/sso', $params);

    	return $url;
    }
    
    
    
    /**
     * Return Salesforce LMS account model
     */
    public function getSalesforceLmsAccount() {
        return $this->sfLmsAccount;
    }
    
    
    /**
     * Get named pricebook info
     * 
     * @param string $name
     */
    public function getPriceBook($name = false) {
    	if ($this->priceBookInfo) return $this->priceBookInfo;
    	if (!$name) $name = self::DOCEBO_PRICEBOOK_NAME;
    	$condition = " Name='$name'";
    	$data = $this->getItems('Pricebook2', self::$PRICEBOOK_FIELDS, $condition);
    	
    	// Invalid data? Return false
    	if (!isset($data['data'][0]) || !is_array($data['data'][0]) || empty($data['data'][0])) {
    		return false;
    	} 
    	
    	// Otherwise, return data (the first record)
    	$this->priceBookInfo = $data['data'][0];
    	return $this->priceBookInfo;
    }

    /**
     * Search for Standard and Active Pricebooks in SF and return the first one
     *
     * @param string $name
     */
    public function getStandardPriceBook() {
    	$condition = " IsStandard=TRUE AND IsActive=TRUE";
    	$response = $this->getItems('Pricebook2', self::$PRICEBOOK_FIELDS, $condition);

    	if (!$this->checkGetItemsResponse($response) || empty($response['data'])) {
    		return false;
    	}
    	$priceBooks = $response['data'];
    	return $priceBooks[0];
    }
    
    /**
     * Return current connection object
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Return current Metadata connection object
     */
    public function getMetdataConnection()
    {
        return $this->metadataConnection;
    }

    /**
     * General (and simple) method to get Salesforce Items (contacts, users, ...)
     *
     * <strong>Some important notes:</strong>
     *
     * Salesforce API does not allow returning ALL possible items (records). Instead it does it by chunks, which size is
     * from 200 to several thousands. For this reason we have to implement "paging" mechanic. This method returns data, as follows:
     *
     * If $page is FALSE, it will keep grabbing data from API, chunk by chunk, until all recods are collected and then return FULL LIST of records (memory)
     * If $page is set to >= 1, this method will retrieve only the requested page (i.e. chunk) of data and will return it.
     *
     * Also, it always return the pageCount, based on currently set chunk size (@see self::CHUNK_SIZE).
     *
     * An example scenario is (apart from the obvious one to get ALL DATA at once):
     * 1. Request page 1.
     * 2. Examine the pageCount and if it is greater than 1, start a cycle to call for next pages
     *
     * The pagination is good for the possible progressive synchronization we are going to run at some point.
     *
     *
     * @param string $type
     *            User, Contact
     * @param array $fieldNames
     *            Array of field names (see constants in this class)
     * @param string $condition
     *            WHERE condition, without "WHERE"!!! Note: this is NOT SQL, although it looks like :-)
     * @param integer $page
     *            Page number to extract, optional (if FALSE : get all data)
	 * @param string $order
	 * 			  <field-name> <ASC|DESC>
     *            
     * @return array
     */
    public function getItems($type, $fieldNames, $condition = '', $page = false, $order = false)
    {
	    if($type == SalesforceApiClient::CO_COURSE_ENROLLMENT
		    && $this->sfLmsAccount
		    && ($this->sfLmsAccount->replicate_courses_as_products < SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS)
	    ){
		    $tmpArray = array();
		    foreach($fieldNames as $field){
			    if($field != SalesforceApiClient::ORDER_CUSTOM_OBJECT_FIELD){
				    $tmpArray[] = $field;
			    }
		    }
		    $fieldNames = $tmpArray;
	    }
        // Set CHUNK size: it has minimum (200) and maximum value
        $options = new QueryOptions(self::CHUNK_SIZE);
        $this->connection->setQueryOptions($options);
        
        // Default result
        $result = array(
            'pageCount' => 0,
            'data' => array()
        );
        
        // Adjust PAGE: can not be negative
        if ($page !== false && (int) $page < 1) {
            $page = 1;
        }
        
        // COUNT ALL records (chunk doesn't apply here)
        $query = "SELECT COUNT() FROM $type";
        if ($condition) {
            $query .= " WHERE $condition";
        }
        $response = $this->connection->query($query);
        
        $queryResult = new QueryResult($response);
        $numRecords = $queryResult->size;
        
        if ($numRecords <= 0)
            return $result;
            
            // Calculate page count
        $pageCount = (int) (($queryResult->size + self::CHUNK_SIZE - 1) / self::CHUNK_SIZE);
        
        // DATA collection
        $query = "SELECT " . implode(',', $fieldNames) . " FROM $type";
        if ($condition) {
            $query .= " WHERE $condition";
        }

        if ($order) {
        	$query .= " ORDER BY $order";
        }

        if ((int) $page > 0) {
            if ($page > $pageCount)
                $page = $pageCount;
            $query .= " LIMIT " . self::CHUNK_SIZE . " OFFSET " . ($page - 1) * self::CHUNK_SIZE;
        }

        // Keep querying until we get DONE (getting data chunk by chunk from Salesforce)
        // If we are requesting some specific page, there should (must) be a single chunk only, because we set the page size exactly to chunk size.
        $done = false;
        $data = array();
        // Make the first query
        $response = $this->connection->query($query);
        while (! $done) {
            $queryResult = new QueryResult($response);
            for ($queryResult->rewind(); $queryResult->pointer < count($queryResult->records); $queryResult->next()) {
                $dataItem = array();
                foreach ($fieldNames as $field) {
                    $record = $queryResult->current();
                    $dataItem[$field] = $queryResult->current()->$field;
                }
                $data[] = $dataItem;
            }
            // Check if API returned DONE status (i.e. last chunk jast has been downloaded)
            // If NOT, we RE-run the query, by executing the "query more" method, which will extract the next "chunk"
            if ($queryResult->done)
                $done = true;
            else
                $response = $this->connection->queryMore($response->queryLocator);
        }

        // Compose the result, always return the page count!
        $result = array(
            'pageCount' => $pageCount,
            'data' => $data
        );
        return $result;
    }

    /**
     * @param $type
     * @param $fieldNames
     * @param string $condition
     * @param bool|false $page
     * @param string|false $lastSalesforceId
     * @return array
     */
    public function getItems2($type, $fieldNames, $condition = '', $page = false, $lastSalesforceId = false){
        if($type == SalesforceApiClient::CO_COURSE_ENROLLMENT
            && $this->sfLmsAccount
            && ($this->sfLmsAccount->replicate_courses_as_products < SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS)
        ){
            $tmpArray = array();
            foreach($fieldNames as $field){
                if($field != SalesforceApiClient::ORDER_CUSTOM_OBJECT_FIELD){
                    $tmpArray[] = $field;
                }
            }
            $fieldNames = $tmpArray;
        }
        // Set CHUNK size: it has minimum (100) and maximum value
        $options = new QueryOptions(self::CHUNK_SIZE);
        $this->connection->setQueryOptions($options);

        // Default result
        $result = array(
            'pageCount' => 0,
            'data' => array(),
            'lastSalesforceId' => null
        );

        // Adjust PAGE: can not be negative
        if ($page !== false && (int) $page < 1) {
            $page = 1;
        }

        // COUNT ALL records (chunk doesn't apply here)
        $query = "SELECT COUNT() FROM $type";
        if ($condition) {
            $query .= " WHERE $condition";
        }
        $response = $this->connection->query($query);

        $queryResult = new QueryResult($response);
        $numRecords = $queryResult->size;

        if ($numRecords <= 0)
            return $result;

        // Calculate page count
        $pageCount = (int) (($queryResult->size + self::CHUNK_SIZE - 1) / self::CHUNK_SIZE);

        // DATA collection
        $query = "SELECT " . implode(',', $fieldNames) . " FROM $type";
        $addWhere = "";
        if ($condition) {
            $addWhere = " WHERE $condition";
        }

        if(!empty($lastSalesforceId)){
            if(!empty($addWhere)){
                $addWhere .= " AND Id > '".$lastSalesforceId."'";
            } else {
                $addWhere = " WHERE Id > '".$lastSalesforceId."'";
            }
        }

        $query .= $addWhere . " ORDER BY Id ASC LIMIT " . self::CHUNK_SIZE;

        Yii::log('Salesforce '.$type.' query executed: '.$query);

        // Keep querying until we get DONE (getting data chunk by chunk from Salesforce)
        // If we are requesting some specific page, there should (must) be a single chunk only, because we set the page size exactly to chunk size.
        $done = false;
        $data = array();
        // Make the first query
        $response = $this->connection->query($query);
        while (! $done) {
            $queryResult = new QueryResult($response);
            for ($queryResult->rewind(); $queryResult->pointer < count($queryResult->records); $queryResult->next()) {
                $dataItem = array();
                foreach ($fieldNames as $field) {
                    $record = $queryResult->current();
                    $dataItem[$field] = $queryResult->current()->$field;
                }
                $data[] = $dataItem;
                $lastId = $record->Id;
            }
            // Check if API returned DONE status (i.e. last chunk jast has been downloaded)
            // If NOT, we RE-run the query, by executing the "query more" method, which will extract the next "chunk"
            if ($queryResult->done)
                $done = true;
            else
                $response = $this->connection->queryMore($response->queryLocator);
        }

        // Compose the result, always return the page count!
        $result = array(
            'pageCount' => $pageCount,
            'data' => $data,
            'lastSalesforceId' => $lastId
        );
        return $result;
    }

    /**
     * Get Salesforce Accounts (wrapper)
     *
     * @param string $condition
     * @param integer $page
     * @param string $lastSalesforceId
     * @param array $additionalLmsFields
     * @param array $additionalSfFields
     * @return object
     */
    public function getAccounts($condition = false, $page = false, $lastSalesforceId = false, $additionalLmsFields = array(), $additionalSfFields = array())
    {
        $fieldsToFetch = array_unique(array_merge(self::$ACCOUNT_FIELDS, $additionalLmsFields, $additionalSfFields));
        return $this->getItems2('Account', $fieldsToFetch, $condition, $page, $lastSalesforceId);
    }

    /**
     * Get Contacts (wrapper)
     *
     * @param string $condition            
     * @param integer $page
     * @param string $lastSalesforceId
     * @param array $additionalLmsFields
     * @param array $additionalSfFields
     * @return object
     */
    public function getContacts($condition = false, $page = false, $lastSalesforceId = false, $additionalLmsFields = array(), $additionalSfFields = array())
    {
        // DO NOT get Deleted Contacts!!
        $tmp = ' (isDeleted=FALSE) ';
        if ($condition) {
            $tmp .= " AND " . $condition;
        }
        $fieldsToFetch = array_unique(array_merge(self::$CONTACT_FIELDS, $additionalLmsFields, $additionalSfFields));
        return $this->getItems2('Contact', $fieldsToFetch, $tmp, $page, $lastSalesforceId);
    }

    /**
     * Get Contacts NOT associated to ANY Salesforce account (wrapper)
     *
     * @param string $condition            
     * @param integer $page            
     */
    public function getOrphanContacts($condition = false, $page = false)
    {
        if ($condition) {
            $condition .= " AND (AccountId = '')";
        } else {
            $condition = " (AccountId = '')";
        }
        return $this->getItems('Contact', self::$CONTACT_FIELDS, $condition, $page);
    }

    /**
     * Get Contacts associated to a given account (wrapper)
     *
     * @param string $condition            
     * @param integer $page            
     */
    public function getAccountContacts($AccountId, $condition = false, $page = false)
    {
        if ($condition) {
            $condition .= " AND (AccountId = '$AccountId')";
        } else {
            $condition = " (AccountId = '$AccountId')";
        }
        return $this->getItems('Contact', self::$CONTACT_FIELDS, $condition, $page);
    }

    
    /**
     * Get Contracts associated to a given account (wrapper)
     *
     * @param string $condition
     * @param integer $page
     */
    public function getAccountContracts($AccountId, $condition = false, $page = false)
    {
    	if ($condition) {
    		$condition .= " AND (AccountId = '$AccountId')";
    	} else {
    		$condition = " (AccountId = '$AccountId')";
    	}
    	return $this->getItems('Contract', self::$CONTRACT_FIELDS, $condition, $page);
    }
    
    /**
     * Search and return a Valid Contract for a given Account
     * Valid is defined as:
     * 		1. Contract is Activated
     * 		2. Contract is assigned the DOCEBO_PB pricebook
     * 
     * Most recent (by activation date) contract is returned (ID)
     * 
     * 
     * @param string $AccountId (SALESFORCE!!!)
     * @return string Contract ID
     */
    public function getValidAccountContract($AccountId)
    {
    	$query = " 
    	SELECT Id, ActivatedDate, Status, Pricebook2Id
    	FROM Contract
    	WHERE (Status='Activated') AND (
    			Pricebook2Id In
    			(SELECT Id from Pricebook2 WHERE Name='".self::DOCEBO_PRICEBOOK_NAME."')
    	) AND
    	(AccountId='".$AccountId."')
    	ORDER BY ActivatedDate DESC
    	LIMIT 1";
    	
   		$response = $this->connection->query($query);
    	
    	$queryResult = new QueryResult($response);
    	
    	$numRecords = $queryResult->size;
    	
    	if ($numRecords <= 0) {
    		return false;
    	}

    	$queryResult = new QueryResult($response);
    	$records = $queryResult->records;
    	
    	if (!is_array($records) || empty($records))
    		return false; 

    	$queryResult->rewind();
    	
    	return $queryResult->current()->Id;
    }

    /**
     * Get Users (wrapper)
     *
     * @param string $condition
     * @param bool|integer $page
     * @param string $lastSalesforceId
     * @param array $additionalLmsFields
     * @param array $additionalSfFields
     * @return object
     */
    public function getUsers($condition = false, $page = false, $lastSalesforceId = false, $additionalLmsFields = array(), $additionalSfFields = array())
    {
        $fieldsToFetch = array_unique(array_merge(self::$USER_FIELDS, $additionalLmsFields, $additionalSfFields));
        return $this->getItems2('User', $fieldsToFetch, $condition, $page, $lastSalesforceId);
    }

    
    /**
     * Reeturn an array of IDs (integers) of courses currently exisitng in Salesforce Course Object
     * 
     * @return array:
     */
    public function getExistingItems($itemType, $notDeletedOnly=false) {
    	
    	static $cache;
    	$cacheKey = sha1(var_export(func_get_args(), true));

    	if (isset($cache[$cacheKey])) return $cache[$cacheKey]; 
    	
    	$typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
    	
		$type =  self::CO_COURSE;
		$condition = "(Name LIKE '$typeLetter%') ";
		
		if ($notDeletedOnly) {
		    $condition = $condition . " AND (D_DELETE__c = NULL)"; 
		}
		
		$response = $this->getItems($type, self::$COURSE_FIELDS, $condition);

	    $items = array();

	    if (!isset($response['data']) || !is_array($response['data'])) {
	    	$this->error = "Invalid items list data structure from API";
	    	return false;
	    }
	    
	    $data = $response['data'];
	    
		foreach ($data as $info) {
			if (isset($info['Name'])) {
		    	$lmsId = (int) preg_replace("/^$typeLetter/i", "", $info['Name']);
			    $items[$lmsId] = $info['Id'];
			}
		} 
		
		$cache[$cacheKey] = $items;
	    return $items;
    }
    
    
    /**
     * 
     */
    public function getProducts($allFields=false) {
        
    	$familiesList = $this->getFamiliesList();
    	if (empty($familiesList))
    		return array();
    	
    	if ($allFields)
    		$fields = self::$PRODUCT_FIELDS;
    	else
    		$fields = array("Id", "Name", "Family");
    	
    	$type =  "Product2";
    	$response = $this->getItems($type, $fields, "Family IN (" . implode(',', $this->getFamiliesList(true)) . ")");
    	
    	if ($this->checkGetItemsResponse($response)) return $response['data'];
    	else return array();
    }
    
    
    /**
     * List names of Product families as they are used in Salesforce
     * 
     * @return array
     */
    public function getFamiliesList($quoted = false) {
    	$result = array();
    	foreach (self::$PRODUCT_FAMILIES as $courseType => $familyInfo) {
    		if ($quoted)
    			$result[] = "'" . $familyInfo['sf_name'] . "'";
    		else 
    			$result[] = $familyInfo['sf_name'];
    	}	
    	return $result; 
    }
    
    
    /**
     * Discover the price of a course (or other product family type) based on SF Product ID
     * 
     * @param string $product SF-wize product "name", e.g.  "C201", which is  "C" +  LMS entity ID
     * @param string $family SF-wize product family name, see constants
     */
    public function getLmsEntityPrice($product, $family) {
    	
    	switch ($family) {
    		case self::FAMILY_ELEARNING:
    		case self::FAMILY_CLASSROOM:
    		case self::FAMILY_WEBINAR:
    			$id = (int) preg_replace("/^C/i", "", $product);
    			$model = LearningCourse::model()->findByPk($id);
				if (!model) {
					return false;
				}    			 
				return $model->prize;    			
    			break;
    			
    		case self::FAMILY_LEARNING_PLAN:
    			$id = (int) preg_replace("/^L/i", "", $product);
    			$model = LearningCoursepath::model()->findByPk($id);
    			if (!$model) {
    				return false;
    			}
    			return $model->price;
    			break;	
    		
    	}
    	return false;
    }
    
    /**
     * Get Pricebook entries. 
     * Warning: can return a LOT of information!
     * 
     */
    public function getPricebookEntries() {
    	$fields = array("Id", "Pricebook2Id", "Product2Id");
    	$type =  "PricebookEntry";
    	$response = $this->getItems($type, $fields);
    	return $response; 
    }
    

    /*
     * Get array of <id> => <Name> of all profiles
     * 
     */
    public function getAllProfileNames() {
        
        static $cache = null;
        
        if (isset($cache)) return $cache;
        
        $profiles = array();
        $response = $this->getMetdataConnection()->listMetadata('Profile');
        
        if ($response instanceof stdClass && !empty($response->result)) {
            foreach ($response->result as $profileInfo) {
                $profiles[$profileInfo->id] = $profileInfo->fullName; 
            }
        }
        
        $cache = $profiles;
        
        return $profiles;
    }
    
    /**
     * Get a PricebookEntry ID (!) by product & pricebook ID
     *  
     * @param string $productId
     * @param string $pricebookId
     */
    public function getPricebookEntry($productId, $pricebookId) {
    	$fields = array("Id", "UnitPrice");
    	$type =  "PricebookEntry";
    	$condition = "Product2Id='$productId' AND Pricebook2Id='$pricebookId'";
    	$response = $this->getItems($type, $fields, $condition);
    	
    	if (!$this->checkGetItemsResponse($response)) return false;
    	if (empty($response['data']['0']['Id'])) return false;
    	
    	return $response['data']['0']['Id'];
    	
    }
        
    
    /**
     * Return Salesforce account data
     * 
     * @return stdClass
     */
    public function getLoginData()
    {
        return $this->loginData;
    }

    
    /**
     * Return
     * @param unknown $id
     * @param unknown $itemType
     * @return multitype:
     */
    public function getCourseOrPlanInfo($id, $itemType) {
        $typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
        $id = $typeLetter . $id;
        $fields = array_merge(array('Id'), self::$COURSE_FIELDS_ALL); 
        $type =  self::CO_COURSE;
        $condition = "Name='$id'";
        $response = $this->getItems($type, $fields, $condition);
        
        if (!$this->checkGetItemsResponse($response)) return false;
        
        return $response['data'][0];
        
    }
    
    
    /**
     * Retrieve list of Salesforce enrollments (CO2 records)
     * 
     * @param string $condition
     */
    public function getSfEnrollments($condition='', $useCache = false) {
        
        static $cache;
        $cacheKey = sha1($condition);
        
        if (isset($cache[$cacheKey]) && $useCache) return $cache[$cacheKey];
        
        $fieldNames = self::$ENROLLMENT_CUSTOM_OBJECT_FIELDS;
        unset($fieldNames['C_DC_ENROLL_ID__c']);

        if($this->sfLmsAccount->replicate_courses_as_products == 0){
            unset($fieldNames['Product__c']);
        }


        $fieldNames = array_merge(array('Id', 'Name'), array_keys($fieldNames));
        $response = $this->getItems(self::CO_COURSE_ENROLLMENT, $fieldNames, $condition);
        
        if (!$this->checkGetItemsResponse($response) || empty($response['data'])) {
            $cache[$cacheKey] = array();
            return array();
        }
        
        $sfEnrollments = $response['data'];
        
        $result = array();
        foreach ($sfEnrollments as $sfEnrollment) {
            $result[$sfEnrollment['Name']] = $sfEnrollment;
        }
        
        $cache[$cacheKey] = $result;
        
        return $result;
    }
    
    
    /**
     * Check if there is any error set during previous operations
     *
     * @return bool
     */
    public function hasError()
    {
        return ! empty($this->error);
    }

    /**
     * Return the recently set error string.
     * Clear the error if requested
     *
     * @return string
     */
    public function error($clearError = false)
    {
        $result = $this->error;
        
        if ($clearError)
            $this->error = null;
        
        return $result;
    }

    /**
     * T1 (account consistency) Checks : Preliminary checks (SF profile validation, permissions check, etc.)
     */
    public function accountConsistencyCheck()
    {
        
        // Profile & Profile assignment check
        if ($this->data->requiredProfileId != $this->loginData->userInfo->profileId) {
            $this->error = Yii::t('salesforce', 'Required profile does not exists ({profile_name}) or current user is not assigned to it', array(
                '{profile_name}' => self::REQUIRED_PROFILE_NAME
            ));
            // Return pure FALSE. The caller must analyze the return value and the hasError()
            return false;
        }
        
        // Check list of permissions and make a resulting array having statuses of every permission (<permission> => <true|false>)
        $result = array();
        foreach (self::$ACCOUNT_CONSISTENCY_PERMISSIONS as $permission) {
            $result[$permission] = ! empty($this->data->permissions->$permission);
        }
        
        // Return an array of permission => status; caller must decide what to do next (show error, etc)
        return $result;
    }

    /**
     * Create the COURSE Custom object in SF
     */
    public function createCourseCustomObject($addProductField)
    {
        if ($this->customObjectExists(self::CO_COURSE)) {
            throw new Exception(Yii::t('salesforce', 'Course custom object already exists in Salesforce'));
        }
        
        $customObject = $this->generateCourseCustomObject($addProductField);
        
        // Create the CO
        $response = $this->metadataConnection->createMetadata($customObject);
        
        // Make all Course fields visible
        if ($response) $this->setCourseFieldsVisible($addProductField);
        
        $this->checkApiResponse($response, "creating course custom object in Salesforce");
        
        return $response;
    }

    public function updateCustomObjectToAddField($masterCustomObject, $productsList, $productLabel)
    {
        $customField = $this->generateFieldForAddition($masterCustomObject, $productsList, $productLabel);
        $response = $this->metadataConnection->updateMetadataToAddField($customField);
        switch($masterCustomObject){
            case SalesforceApiClient::CO_COURSE:
                $this->setCourseFieldsVisible(true);
                break;
            case SalesforceApiClient::CO_COURSE_ENROLLMENT:
                $this->setEnrollmentFieldsVisible(true);
                break;
        }
        $this->checkApiResponse($response, "updating $masterCustomObject custom object in Salesforce");
        return $response;
    }

    /**
     * Update the COURSE Custom object in SF
     */
    public function updateCourseCustomObject()
    {
        if (! $this->customObjectExists(self::CO_COURSE)) {
            throw new Exception(Yii::t('salesforce', 'Course custom object does not exists in Salesforce'));
        }

        $customObject = $this->generateCourseCustomObject();
        $response = $this->metadataConnection->updateMetadata($customObject);

        // Make all Course fields visible
        if ($response) $this->setCourseFieldsVisible();

        $this->checkApiResponse($response, "updating course custom object in Salesforce");

        return $response;
    }

    /**
     * Update the Product2 Standard Object in SF, adding (replacing) picklist values for "Family" field
     */
    public function updateProduct2FamilyPicklist()
    {
    	$values = array();
    	
    	// Get current ones
    	$picklistValues = $this->getPicklistValues('Product2', 'Family');
    	
    	if (is_array($picklistValues) && !empty($picklistValues)) { 
			foreach ($picklistValues as $obj) {
    			$values[$obj->value] = array();
    		}
    	}
    	
    	// We add (or replace) these product families
    	$myValues = array();
    	foreach (self::$PRODUCT_FAMILIES as $courseType => $familyInfo) {
    	    $myValues[$familyInfo['sf_name']] = $familyInfo['attributes'];
    	}
    	
    	$values = CMap::mergeArray($values, $myValues);
    	
    	$customField = $this->generatePicklistField('Product2', 'Family', $values);
    	$response = $this->metadataConnection->updateMetadata($customField);
    
    	$this->checkApiResponse($response, "updating Product2 Family standard field in Salesforce");
    
    	return $response;
    }
    
    
    /**
     * Return Picklist values for a given Object.Field
     * @return array  Array of stdClass (attrs:  active, defaultValue, label, value)
     */
    public function getPicklistValues($objectFullName, $fieldName) {
    	$result = array();
    	$response = $this->getConnection()->describeSObject($objectFullName);
     	if (isset($response->fields)) {
     		foreach ($response->fields as $field) {
     			if (strtolower($field->name) === strtolower($fieldName) && strtolower($field->type) === strtolower('picklist')) {
     				if (!empty($field->picklistValues)) {
     					foreach ($field->picklistValues as $picklistValue) {
     						$result[] = $picklistValue;
     					}
     				}
     			}
     		}
     	}
    	return $result;
    }
    
    /**
     * Update the Order Standard Object in SF, adding (replacing) picklist values for "Type" field
     */
    public function updateOrderTypePicklist()
    {
    	
    	$values = array();
    	
    	// Get current ones
    	$picklistValues = $this->getPicklistValues('Order', 'Type');
    	
    	if (is_array($picklistValues) && !empty($picklistValues)) {
    		foreach ($picklistValues as $obj) {
    			$values[$obj->value] = array();   
    		}
    	}

    	// We add (or replace) these order types
    	$myValues = array(
    		self::DOCEBO_ORDER_TYPE	=> array('default' => true),
    	);
    	$values = CMap::mergeArray($values, $myValues);
    	
    	$customField = $this->generatePicklistField('Order', 'Type', $values);
    	$response = $this->metadataConnection->updateMetadata($customField);
    
    	$this->checkApiResponse($response, "updating Order Type standard field in Salesforce");
    
    	return $response;
    	
    }
    

    /**
     * Create the COURSE Enrollment Custom object in SF
     */
    public function createEnrollmentCustomObject($addProductField = false, $addOrderField = false)
    {
        if ($this->customObjectExists(self::CO_COURSE_ENROLLMENT)) {
            throw new Exception(Yii::t('salesforce', 'Course Enrollment custom object already exists in Salesforce'));
        }
        
        $customObject = $this->generateEnrollmentCustomObject($addProductField, $addOrderField);
        
        // Create the CO
        $response = $this->metadataConnection->createMetadata($customObject);
        
        // Make all fields visible
        if ($response) $this->setEnrollmentFieldsVisible($addProductField, $addOrderField);
        
        $this->checkApiResponse($response, "creating course enrollment custom object in Salesforce");
        
        return $response;
    }

    /**
     * Update COURSE ENROLLMENT Custom object in SF
     */
    public function updateEnrollmentCustomObject()
    {
        if (! $this->customObjectExists(self::CO_COURSE_ENROLLMENT)) {
            throw new Exception(Yii::t('salesforce', 'Course Enrollment custom object does not exists in Salesforce'));
        }
        
        $customObject = $this->generateEnrollmentCustomObject();
        $response = $this->metadataConnection->updateMetadata($customObject);
        
        // Make all fields visible
        if ($response) $this->setEnrollmentFieldsVisible();
        
        $this->checkApiResponse($response, "updating course Enrollment custom object in Salesforce");
        
        return $response;
    }

    
    /**
     * Creates the Pricebook if it doesn't exist yet. Returns TRUE if it exists. Otherwise, standard API response 
     * 
     * @return boolean|SaveResult
     */
    public function createPriceBook() {
    	
    	$pbInfo = $this->getPriceBook(self::DOCEBO_PRICEBOOK_NAME);
    	
    	// Maybe it already exists? Return TRUE
    	if (is_array($pbInfo) && !empty($pbInfo)) {
    		return true;
    	}
    	
    	// Good, lets create the object and return API response
   		$fields = array (
    		'Name' => self::DOCEBO_PRICEBOOK_NAME,
    		'IsActive' => 1,
    	);
    	$sObject = new SObject();
    	$sObject->fields = $fields;
    	$sObject->type = 'Pricebook2';
		$response = $this->createChunked(array($sObject));
		
		if (is_array($response) && isset($response[0]) && $response[0] instanceof stdClass) {
			return $response[0]->success;
		}
		return false;
		
    }
    
    
    /**
     * Creates a new contract assigned to a given SF account, assigning a Pricebook created earlier (or just existing)
     *  
     * @param string $accountId
     * 
     * @return boolean|SaveResult|UpdateResult
     */
    public function createAccountContract($accountId) {
    	
        // Get DOCEBO Pricebook
        $pbInfo = $this->getPriceBook(self::DOCEBO_PRICEBOOK_NAME);
        if (!$pbInfo) {
            $this->error = 'Failed to get Pricebook';
            return false;
        }
     
        // 1. Create contract
    	$fields = array (
    		'Pricebook2Id' 	=> $pbInfo['Id'],
    		'AccountId'		=> $accountId,
            'ContractTerm' => 333,
    	    'StartDate'    => Yii::app()->localtime->getUTCNow(),
    	);
    	$sObject = new SObject();
    	$sObject->fields = $fields;
    	$sObject->type = 'Contract';
    	$response = $this->createChunked(array($sObject));
    	
    	// 2. Activate contract
    	if (isset($response[0]) && $response[0]->success) {
   	        $sObject->fields = array (
   	            'Id'   => $response[0]->id,
   	            'Status'    => 'Activated',
   	        );
   	        $response = $this->updateChunked(array($sObject));
    	}
    	else {
    	    $this->error = 'Failed to Update contract (trying to activate)';
    	    return false;
    	}
    	
    	return $response;
    	
    }
    

    /**
     * Check if Account exists, by Name
     * 
     * @param string $name
     */
    public function accountExistsByName($name) {
        $query = "SELECT Id FROM Account WHERE Name = '" . $name . "'";
        $response = $this->connection->query($query);
        $queryResult = new QueryResult($response);
        
        if ($queryResult->size > 0) {
            $queryResult->rewind();
            $record = $queryResult->current();
            return $record->Id;
        }
        else {
            return false;
        }
        
    }

    /**
     * @param string $type account|contact|user
     * @param bool $noFakeCounters if false, all counters are set to a fake value '1'
     *                             if true, the (very slow) API calls are made to fetch the number of records for each listview
     * @return array
     */
    public function generateListviewDropdownList($type, $noFakeCounters = false){
        $results[] = array(
            'listviewId' => '',
            'label' => Yii::t('salesforce', 'Please select a filter...'),
            'items' => ''
        );
        $syncAgent = new SalesforceSyncAgent();
        $query = "SELECT Id,Name FROM ListView WHERE SobjectType = '".$type."' ORDER BY Name ASC";
        $response = $this->connection->query($query);
        $customFields = $syncAgent->getAllFields($type);
        foreach($response->records as $record){
            if($noFakeCounters){
                $listViewName = $this->fetchListViewDevName($type, $record->Id[0]);
                $listViewFilters = $this->fetchListViewFilters($type, $listViewName);
                $filterString = $syncAgent->generateFilterString($type, $listViewFilters, $customFields);
                $items = $syncAgent->countItemsInListview($type, $filterString);
            }
            $results[] = array(
                'listviewId' => $record->Id[0],
                'label' => strip_tags($record->any),
                'items' => ($noFakeCounters)?$items['records']:1
            );
        }
        return $results;
    }

    /**
     * Returns an associative array for a specified object type with {$type|$fieldname} => label
     * @param string $sobjectType
     * @return array
     */
    public function getSalesforceFieldsList($sobjectType){
        $results[] = Yii::t('salesforce', 'Please select an additional field...');
        $response = $this->connection->describeSObject($sobjectType);
        foreach($response->fields as $field){
            $results[$field->type."|".$field->name] = $field->label;
        }
        return $results;
    }

    public function getFilterName($listviewId){
        $query = "SELECT Name FROM ListView WHERE Id = '".$listviewId."'";
        $response = $this->connection->query($query);
        return strip_tags($response->records[0]->any);
    }

    /**
     * Returns a multidimensional array with picklist dependencies
     * @param string $sobjectType
     * @return array
     */
    public function getPicklistStructure($sobjectType){
        $picklists = array();
        $dependencies = array();
        $associations = array();
        $result = $this->connection->describeSObject($sobjectType);
        foreach($result->fields as $field){
            if($field->type == 'picklist'){
                if(!isset($field->controllerName)){
                    $picklists[$field->name] = array(
                        'fieldname'    => $field->name,
                        'label'        => $field->label,
                        'dependencies' => array()
                    );
                    $associations[$field->name] = "root";
                } else {
                    $dependencies[] = array(
                        'fieldname'    => $field->name,
                        'label'        => $field->label,
                        'dependentOn'  => $field->controllerName
                    );
                    $associations[$field->name] = $field->controllerName;
                }
            }
        }
        while(!empty($dependencies)){
            foreach($dependencies as $key => $dependency){
                if($associations[$dependency['dependentOn']] == 'root'){ // 2nd level field
                    $picklists[$dependency['dependentOn']]['dependencies'][$dependency['fieldname']] = array(
                        'fieldname'    => $dependency['fieldname'],
                        'label'        => $dependency['label'],
                        'dependencies' => array()
                    );
                    unset($dependencies[$key]);
                } else { // 3rd level field
                    $picklists[$associations[$dependency['dependentOn']]]['dependencies'][$dependency['dependentOn']]['dependencies'][$dependency['fieldname']] = array(
                        'fieldname'    => $dependency['fieldname'],
                        'label'        => $dependency['label'],
                        'dependencies' => array()
                    );
                    unset($dependencies[$key]);
                }
            }
        }
        return $picklists;
    }

    /**
     * Retrieves a list view from Salesforce
     * @param $sObjectType string
     * @param $listViewId string 18-char long string with object id
     * @param $offset int starting record for the operation
     */
    public function fetchListViewContents($sObjectType, $listViewId, $offset){
        return $this->connection->executeListView($sObjectType, $listViewId, $offset);
    }
    /**
     * Retrieves the used filters on a listview
     * @param $type string Account|User|Contact
     * @param $listviewName string DeveloperName for the listview
     * @return array
     */
    public function fetchListViewFilters($type, $listviewName){
        $metadata = $this->metadataConnection->readMetadata("ListView", array($type.".".$listviewName));
        $result = array(
            'filters' => $metadata->result->records->filters,
            'booleanFilter' => false
        );
        if(isset($metadata->result->records->booleanFilter)) $result['booleanFilter'] = $metadata->result->records->booleanFilter;

        return $result;
    }

    /**
     * Checks if the listview has been set to "Visible to all users", if not the listview filters can't be read!
     * @param $type
     * @param $listviewName
     * @return bool
     */
    public function checkListViewVisibility($type, $listviewName){
        $results = $this->metadataConnection->readMetadata("ListView", array($type.".".$listviewName));
        return isset($results->result->records->fullName);
    }

    public function fetchListViewDevName($type, $listviewId){
        $result = $this->connection->query("SELECT DeveloperName FROM ListView WHERE Id = '".$listviewId."' AND SobjectType = '".$type."'");
        return strip_tags($result->records[0]->any);
    }
    
    
    
    /**
     * Delete List of Custom Objects from SF
     */
    public function deleteCustomObjects($fullNames)
    {
        if (! is_array($fullNames))
            $fullNames = array(
                $fullNames
            );
        
        foreach ($fullNames as $index => $fullName) {
            // Make sure full name has __c at the end
            if (! preg_match("/__c$/i", $fullName)) {
                $fullName .= "__c";
            }
            $fullNames[$index] = $fullName;
        }
        
        $response = $this->metadataConnection->deleteMetadata("CustomObject", $fullNames);
        
        return $response;
    }

    /**
     * Delete the COURSE Custom object from SF
     */
    public function deleteCourseCustomObject()
    {
        return $this->deleteCustomObjects(array(
            self::CO_COURSE
        ));
    }

    /**
     * Delete the COURSE Custom object from SF
     */
    public function deleteCourseEnrollmentCustomObject()
    {
        return $this->deleteCustomObjects(array(
            self::CO_COURSE_ENROLLMENT
        ));
    }

    /**
     * Get list of CustoObjects currently exisitng in SF
     *
     * @return array
     */
    public function getCustomObjectsMetadataList()
    {
        
        // Do some caching to prevent multiple API calls
        static $cache;
        if (isset($cache)) {
            return $cache;
        }
        
        // Get the list through API
        $response = $this->getMetdataConnection()->listMetadata('CustomObject');
        
        $cache = array();
        
        if (isset($response->result) && is_array($response->result)) {
            $objsList = $response->result;
        }

        if (is_array($objsList)) {
	        foreach ($objsList as $obj) {
	            $item = array();
	            foreach (get_object_vars($obj) as $varName => $varValue) {
	                $item[$varName] = $varValue;
	            }
	            $cache[] = $item;
	        }
        }
        
        return $cache;
    }

    /**
     * Check if a custom object exists, by full name (please include __c!)
     *
     * @param string $coFullName            
     */
    public function customObjectExists($coFullName)
    {
        
        // Make sure Custom object name ends with "__c"
        if (! preg_match("/__c$/i", $coFullName)) {
            $coFullName .= "__c";
        }
        
        // Do some caching
        static $cache = array();
        $cacheKey = sha1(json_encode(func_get_args()));
        
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }
        
        $list = $this->getCustomObjectsMetadataList();
        
        $cache[$cacheKey] = false;
        if (is_array($list)) {
            foreach ($list as $object) {
                if ($object['fullName'] == $coFullName) {
                    $cache[$cacheKey] = true;
                    return true;
                }
            }
        }
        
        return false;
    }
    
    
    /**
     * Create/Update Salesforce Courses from LMS courses
     * 
     * @param array $courses
     */
    public function createOrUpdateItems($items, $itemType) {
    	$objects = $this->generateObjectsFromItems($items, $itemType);
    	$response = $this->upsertChunked('Name', $objects);
    	return $this->transformUpsertResponse($response);
    } 
    
    
    /**
     * Create/Update Salesforce Products from LMS courses
     * 
     * @param array $courses
     */
    public function createOrUpdateProducts($items, $itemType) {
        $objects = $this->generateProductObjectsFromItems($items, $itemType);
        $response = $this->upsertChunked('Name', $objects);
        return $this->transformUpsertResponse($response);
    }

    
    /**
     * Create/Update Salesforce Pricebook Entries (in the custom pricebook!) for Our Products in Salesforce
     *
     * @param array $courses
     */
    public function createOrUpdateCustomPricebookEntries() {
    	// First: Products MUST be part of a Standard and Active Pricebook.
    	$this->createOrUpdateStandardPricebookEntries();
    	
    	$objects = $this->generatePricebookEntriesForProducts();
    	$response = $this->upsertChunked('Id', $objects);
    	
    	return $this->transformUpsertResponse($response);
    	
    }



	/**
	 *
	 */
	public function createOrUpdateEnrollments($enrollments, $itemType, $additionalInfo = false, $status = false, $delete = false)
	{
		if ($status === false && $delete === false) $status = self::ENROLLSTATUS_SUBSCRIBED;
		$objects = $this->generateObjectsFromEnrollments($enrollments, $itemType, $additionalInfo, $status, $delete);
		$response = $this->upsertChunked('Name', $objects);
		return $response;
	}



    /**
     * Set list of items as "DELETED" in Courses custom object in Salesforce
     * Here: 
     *      Item means Course or learning plan (Cxxx or Lxxx)
     *      Delete means set the DELETED field to the date of deletion (now)
     *      
     * @param array $items Array of LMS IDs of courses/plans 
     */
    public function deleteItems($items, $itemType) {
        
        // We need to get SF-Ids of courses/plans we need to delete
        $existingItems = $this->getExistingItems($itemType);
        
        // Make <lms-id> => <sf-id> array of only existing (SF) courses/plans, ignoring the rest  
        $objects = array();
        $utcNow = Yii::app()->localtime->getUTCNow();
        
        if (is_array($existingItems)) {
        	foreach ($existingItems as $lmsId => $sfId) {
        	    if (in_array($lmsId, $items)) {
            	    $fields = array (
                	    'Id'                   => $sfId,
	    	        	'D_DELETE__c'          => $utcNow,
	    	    	);
                	$sObject = new SObject();
                	$sObject->fields = $fields;
                	$sObject->type = self::CO_COURSE;
                	$objects[] = $sObject;
            	}
        	}
        	$response = $this->updateChunked($objects);
        }
        
    	return true;
    	
    }
    
    
    /**
     * Deactivates all products generated for specified courses (by their LMS ids)
     * 
     * @param array $courses Array of LMS IDs
     * @return UpsertResult
     */
    public function deactivateProducts($items, $itemType) {
        
        $existingProducts = $this->getProducts(true);

        $typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
        
        // Filter out courses which does not exist in Salesforce Products
        $finalItems = array();
        foreach ($existingProducts as $productInfo) {
            $lmsId = (int) preg_replace("/^$typeLetter/i", "", $productInfo['Name']);
            if (in_array($lmsId, $items)) {
                $finalItems[] = $lmsId; 
            }
        }

        $objects = $this->generateProductObjectsFromItems($finalItems, $itemType, true, true);
        $response = $this->upsertChunked('Name', $objects);
        return $response;
    }
    
    

    
    
    /**
     * Make fields visible
     * 
     * @param array $fields
     */
    public function setFieldsVisible($fields) {
        $profiles = $this->getAllProfileNames();
        $sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
        $scheduler = Yii::app()->scheduler;
        $params = array(
            'type' => SalesforceLog::TYPE_LOG_UPDATE_METADATA,
            'id_account' => $sfLmsAccount->id_account,
            'profiles' => $profiles,
            'fields' => $fields,
            'index' => 0
        );
        return $scheduler->createImmediateJob(SalesforceSyncTask::JOB_NAME, SalesforceSyncTask::id(), $params);
        //return $this->setFieldsPermissions($profiles, $fields, true, true);
    }
    
    /**
     * Make fields invisible
     * 
     * @param array $fields
     */
    public function setFieldsInVisible($fields) {
        $profiles = $this->getAllProfileNames();
        return $this->setFieldsPermissions($profiles, $fields, false, false);
    }
    
    
    /**
     * Assign a give Contact to an account (effectively MOVING it from account to account)) 
     * 
     * @param string $idContact
     * @param string $idAccount
     */
    public function assignContactToAccount($idContact, $idAccount) {
    	
    	$object = new SObject();
    	$object->type = "Contact";
    	$object->fields = array(
    		'Id'			=> $idContact,	
    		'AccountId'		=> $idAccount,
    	);
    	$sObjects = array($object);
    	$response = $this->updateChunked($sObjects);
    	return $response;
    	
    }
    
    
    /**
     * General method to create SF events 
     *  
     * @param unknown $subject
     * @param unknown $whatId
     * @param unknown $startDatetime
     * @param unknown $description
     * @param unknown $durationInMinutes
     * @param string $isAllDayEvent
     */
    public function createEvent($subject, $description, $whatId, $ownerId, $startDatetime, $durationInMinutes, $location) {

        $object = new SObject();
        $object->type = "Event";
        $object->fields = array(
            'Subject'           => $subject,
            'WhatId'            => $whatId,
            //'WhoId'             => $whoId,
        	'OwnerId'			=> $ownerId,
            'StartDateTime'     => $startDatetime,
            'Description'       => $description, 
            'DurationInMinutes' => $durationInMinutes, 
            'IsAllDayEvent'     => 'FALSE',
            'Location'          => $location
        );
        $sObjects = array($object);
        $response = $this->createChunked($sObjects);
        return $response;
        
    }
    
    
    /**
     * Delete an Event
     * @param string $sfEventId
     */
    public function deleteEvent($sfEventId) {
    	$response = $this->deleteChunked(array($sfEventId));
    	return $response;
    }


	/**
	 * General method to create SF events
	 */
	public function createTask($subject, $description, $whatId, $ownerId, $startDate)
	{

		$object = new SObject();
		$object->type = "Task";
		$object->fields = array(
			'Subject' => $subject,
			'Description' => $description,
			'WhatId' => $whatId,
			'OwnerId' => $ownerId,
			'ActivityDate' => $startDate,
		);
		$sObjects = array($object);

		$response = $this->createChunked($sObjects);

		return $response;
	}
    

    /**
     * Delete a Task
     * @param string $sfEventId
     */
    public function deleteTask($sfTaskId) {
    	$response = $this->deleteChunked(array($sfTaskId));
    	return $response;
    }
    

    /**
     * Create a New event in SF Calendar for a given course webinar session 
     * 
     * @param LearningCourseuser $enrollment
     * @param LearningCourse $lmsCourse
     * @param CoreUser $lmsUser
     */
    public function createEventFromWebinar(WebinarSessionUser $enrollment, SalesforceUser $sfUser) {

    	// Get Course model and course id
        $sessionModel = WebinarSession::model()->findByPk($enrollment->id_session);

        /**
         * @var $sessionModel WebinarSession
         */
        $idCourse 	= $sessionModel->course_id;
        $course = LearningCourse::model()->findByPk($idCourse);
        $userModel = CoreUser::model()->findByPk($enrollment->id_user);

        // Course must be available in SF
    	$sfItems = $this->getExistingItems(self::ITEM_TYPE_COURSE);
    	if (!in_array($idCourse, array_keys($sfItems))) return false;
    	 
    	$courseInfo = $this->getCourseOrPlanInfo($idCourse, self::ITEM_TYPE_COURSE);
    	 
    	if ($courseInfo !== false) {
    		
    		$url = $this->generateSsoUrl($userModel, $idCourse);
    		$description = "<![CDATA[" . $url . "]]>";
    		
    		$whatId = $courseInfo['Id'];
    		$ownerId = $sfUser->id_user_sf;

    		$durationInMinutes = $sessionModel->getTotalMinutes();
    		$startDatetime = Yii::app()->localtime->fromLocalDateTime($sessionModel->date_begin);

            //TODO: add location
            $location = "";

    		$response = $this->createEvent($course->name, $description, $whatId, $ownerId , $startDatetime, $durationInMinutes, $location);
    		$eventId = $response[0]->id;
    		return $eventId;
    	}
    	 
    	return false;
    	 
    	
    }

    
    /**
     * Create a New event in SF Calendar for a given course classroom session 
     * 
     * @param LtCourseuserSession $enrollment
     * @param SalesforceUser $sfUser
     * @param string $location
     * @param string $startDateTime
     * @param int $durationInMinutes
     * @return SaveResult|boolean
     */
    public function createEventFromClassroom(LtCourseuserSession $enrollment, SalesforceUser $sfUser, $location, $startDatetime, $durationInMinutes) {
    
    	// Get Course model and course id
    	$course = $enrollment->ltCourseSession->course;
    	$idCourse = $course->idCourse;

    	// Course must be available in SF
    	$sfItems = $this->getExistingItems(self::ITEM_TYPE_COURSE);
    	if (!in_array($idCourse, array_keys($sfItems))) return false;
    	
    	$courseInfo = $this->getCourseOrPlanInfo($idCourse, self::ITEM_TYPE_COURSE);
    	if ($courseInfo !== false) {
    		
    		$url = $this->generateSsoUrl($enrollment->learningUser, $idCourse);
    		$description = "<![CDATA[" . $url . "]]>";

    		$whatId = $courseInfo['Id'];
    		$ownerId = $sfUser->id_user_sf;
    		$startDatetime = Yii::app()->localtime->fromLocalDateTime($startDatetime);

    		$response = $this->createEvent($course->name, $description, $whatId, $ownerId, $startDatetime, $durationInMinutes, $location);
    		$eventId = $response[0]->id; 
    		return $eventId;
    	}
    	
    	return false;
    	 
    
    }

    
    
    
    
	/**
	 * Create SF Task, based on a given course
	 * 
	 * @param LearningCourseuser $enrollment
	 * @param SalesforceUser $sfUser
     * @return string Id of the Task
	 */
    public function createTaskFromCourse(LearningCourseuser $enrollment, SalesforceUser $sfUser) {
    	if ($sfUser->sf_type != SalesforceUser::USER_TYPE_USER) return;
    	 
    	$id = $enrollment->idCourse;
    	$name = 'LMS course: ' . $enrollment->course->name;
    	
    	$sfInfo = $this->getCourseOrPlanInfo($id, self::ITEM_TYPE_COURSE);

        if(isset($enrollment->date_expire_validity)){
            $startDate = date('Y-m-d', strtotime(str_replace("/","-",$enrollment->date_expire_validity))) . 'T00:00:00.000Z';
        } else {
            $startDate = Yii::app()->localtime->getUTCNow('Y-m-d') . 'T00:00:00.000Z';
        }

    	$url = $this->generateSsoUrl($enrollment->user, $enrollment->idCourse);
    	$description = "<![CDATA[" . $url . "]]>";
    	
    	$response = $this->createTask($name, $description, $sfInfo['Id'], $sfUser->id_user_sf, $startDate);

    	$entityId = $response[0]->id;
    	
    	return $entityId;
    	
    }
    
    /**
     * Create SF task based on a Learning plan
     * 
     * @param LearningCoursepathUser $enrollment
     * @param SalesforceUser $sfUser
     * @return string Id of the Task
     */
    public function createTaskFromLearningPlan(LearningCoursepathUser $enrollment, SalesforceUser $sfUser) {
    	if ($sfUser->sf_type != SalesforceUser::USER_TYPE_USER) return;
    	
    	$id = $enrollment->id_path;
    	$name = $enrollment->path->path_name;
    	 
    	$sfInfo = $this->getCourseOrPlanInfo($id, self::ITEM_TYPE_LEARNING_PLAN);
    	 
        if(isset($enrollment->date_end_validity) && $enrollment->date_end_validity != ""){
            $startDate = date('Y-m-d', strtotime(str_replace("/", "-", $enrollment->date_end_validity))) . 'T00:00:00.000Z';
        } else {
            $startDate = Yii::app()->localtime->getUTCNow('Y-m-d') . 'T00:00:00.000Z';
        }

    	$url = $this->generateSsoUrl($enrollment->user, false, true);
    	$description = "<![CDATA[" . $url . "]]>";
    	 
    	$response = $this->createTask($name, $description, $sfInfo['Id'], $sfUser->id_user_sf, $startDate);
    	 
    	$entityId = $response[0]->id;
    	 
    	return $entityId;
    
    }
    
    
    
    
    /**
     * ISSUE Sales Order
     * 
     * @param LearningCourse $lmsCourse
     * @param CoreUser $lmsUser
     * @param SalesforceUser $sfUser
     */
    public function issueSalesOrder($lmsItemModel, $itemType, $lmsUser, $sfUser) {
    	
    	$typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
    	
    	//-- 1. Get Account and Contract
    	
    	$contactId = $sfUser->id_user_sf;
    	$contacts = $this->getContacts("Id='" . $contactId . "'");
    	
    	if (!$this->checkGetItemsResponse($contacts) || empty($contacts['data'])) {
    		Yii::log('Could not create sales order. Requested Contact does not exist in Salesforce: ' . $sfUser->id_user_sf, CLogger::LEVEL_ERROR);
    		return false;
    	}
    	
    	// Account id is ...
    	$accountId = isset($contacts['data'][0]['AccountId']) ? $contacts['data'][0]['AccountId'] : false;

    	// Get a valid contract for this Account (Valid = having our Pricebook assigned)
  		$contractId = $this->getValidAccountContract($accountId);

    	
    	//-- 2. Get product (must be active)
    	
    	$lmsId = $itemType == self::ITEM_TYPE_COURSE ? $lmsItemModel->idCourse : $lmsItemModel->id_path;
    	$condition = "Name='${typeLetter}${lmsId}' AND IsActive=TRUE";
    	$products = $this->getItems("Product2", array('Id'), $condition);
    	
    	if (!$this->checkGetItemsResponse($products) || empty($products['data'])) {
    		throw new Exception('Failed to find a product item for sales order');
    	}

    	$productId = $products['data'][0]['Id'];

    	//-- 3 Create the Order
    	$price = $itemType == self::ITEM_TYPE_COURSE ? $lmsItemModel->prize : $lmsItemModel->price;
    	$price = floatval($price);
    	$orderId = $this->createOrder($accountId, $contactId, $contractId, $productId, $price, 1);
    	
    	$result = array(
    		'productId'	=> $productId,
    		'orderId'	=> $orderId,
    	);
    	
    	return $result;
  		
    }
    
    /**
     * Create Order
     * 
     * @param unknown $accountId
     * @param unknown $contractId
     * @param unknown $productId
     * @param unknown $price
     * @param number $quantity
     * @param string $isDummyAccount
     * @throws Exception
     * @return Ambigous <SaveResult, UpdateResult>
     */
    public function createOrder($accountId, $contactId, $contractId, $productId, $price, $quantity=1, $isDummyAccount=false) {

    	//-- 1. Create the order in Draft status
    	$pbInfo = $this->getPriceBook();
    	
    	$fields = array(
    		'AccountId'		=> $accountId,
    		'Pricebook2Id'	=> $pbInfo['Id'],	
    		'ContractId'	=> $contractId,
    		'Type'			=> self::DOCEBO_ORDER_TYPE,
    		'EffectiveDate'	=> Yii::app()->localtime->getUTCNow(),
    		'Status'		=> 'Draft',
   			'Description'	=> 'Automatically generated by LMS',
    		'CustomerAuthorizedById'	=> 	$contactId,
    	);
    	
    	
    	$sObject = new SObject();
    	$sObject->fields = $fields;
    	$sObject->type = 'Order';
    	$response = $this->createChunked(array($sObject));
    	
    	// General error
    	if (empty($response[0]) || !($response[0] instanceof stdClass)) {
    		throw new Exception('Failed to create Sales order');
    	}
    	
    	// Soap Fault or SF error(s)
    	if (!$response[0]->success) {
			$message = var_export($response[0]->errors, true);  
			throw new Exception($message);
    	}
    	
    	$orderId = $response[0]->id;
    	
    	//-- 2. Add order item
    	
    	// Get pricebook entry
    	$pricebookEntryId = $this->getPricebookEntry($productId, $pbInfo['Id']);

    	// Create Object  (Order Item)
     	$sObject = new SObject();
     	$sObject->fields = array(
     		'OrderId'			=> $orderId,
     		'PricebookEntryId'	=> $pricebookEntryId,
     		'Quantity'			=> $quantity,
     		'UnitPrice'			=> $price,			
     	);
     	$sObject->type = 'OrderItem';
     	$response = $this->createChunked(array($sObject));

     	
     	//-- 3. Activate the order
     	if (!$isDummyAccount) {
     		$sObject = new SObject();
     		$sObject->fields = array(
                'Id'			=> $orderId,
     			'Status'		=> 'Activated',
     		);
     		$sObject->type = 'Order';
     		$response = $this->updateChunked(array($sObject));
     		 
     	}
    	
     	return $orderId;
    	
    }
    
    

    /**
     * 
     * @param unknown $orderId
     * @return DeleteResult
     */
    public function deleteOrder($orderId) {
    	 
    	$sObject = new SObject();
    	$sObject->fields = array(
    			'Id'			=> $orderId,
    			'Status'		=> 'Draft',
    	);
    	$sObject->type = 'Order';
    	$response = $this->updateChunked(array($sObject));
    	$response = $this->deleteChunked(array($orderId));
    	return $response;
    }
    
    
    /**
     * 
     * @param unknown $lmsItemId
     * @param unknown $itemType
     * @param SalesforceUser $sfUser
     * @return boolean|DeleteResult
     */
    public function deleteOrderOld($lmsItemId, $itemType, SalesforceUser $sfUser) {
    	
    	$typeLetter = $itemType == self::ITEM_TYPE_COURSE ? "C" : "L";
    	$name = $typeLetter . $lmsItemId;

    	$response = $this->getItems("Product2", array("Id"), "Name='$name'");
    	if (!$this->checkGetItemsResponse($response) || empty($response['data'])) return false;
    	$productId = $response['data'][0]['Id'];
    	
    	$pbInfo = $this->getPriceBook();
    	$pbEntryId = $this->getPricebookEntry($productId, $pbInfo['Id']);

    	$response = $this->getItems("OrderItem", array("Id", "OrderId"), "PricebookEntryId='$pbEntryId'");
    	if (!$this->checkGetItemsResponse($response) || empty($response['data'])) return false;
    	
    	$orderIds = array();
    	foreach ($response['data'] as $tmp) {
    		$orderIds[] = "'" . $tmp['OrderId'] . "'";
    	}
    	$ordersList = implode(',', $orderIds);
    	
    	$contactId = $sfUser->id_user_sf;
    	$response = $this->getItems("Order", array("Id"), "(CustomerAuthorizedById='$contactId') AND (Id IN ($ordersList))");
    	if (!$this->checkGetItemsResponse($response) || empty($response['data'])) return false;
    	$orderId = $response['data'][0]['Id']; 
    	 
    	$sObject = new SObject();
    	$sObject->fields = array(
    		'Id'			=> $orderId,
    		'Status'		=> 'Draft',
    	);
    	$sObject->type = 'Order';
    	
    	$response = $this->updateChunked(array($sObject));
    	$response = $this->deleteChunked(array($orderId));
    	
    	return $response;
    	 
    	
    	
    	//$pbEntries = $this->getItems('PricebookEntry', array('Id'), 'Product2Id')
    	//$orderItems = $this->getItems('OrderItem', "Id", $condition);
    	
    }
    

    /**
     * 
     * @param obj $enrollment
     * @param bool|int $minutesInCourse
     * @param bool|string $completedDatetime
     * @param bool|string $courseScore
     * @param bool|string $courseMaxScore
     * @param bool|int $completionPct
     * @param bool|datetime $firstAccess
     * @param bool $resetCompletion
     * @param bool|string $pathCompleted
     * @param bool $ignoreLastAccess
     * @param bool $dontUpdateStatus
     */
    public function updateAttendance($enrollment, $minutesInCourse = false, $completedDatetime = false, $courseScore = false, $courseMaxScore = false, $completionPct = false, $firstAccess = null, $resetCompletion = false, $pathCompleted = false, $ignoreLastAccess = false, $dontUpdateStatus = false) {
    	switch (get_class($enrollment)) {
    		case 'LearningCourseuser':
    			$enrollmentName = 'C' . $enrollment->idCourse . "_" . $enrollment->idUser;
    			break;

    		case 'LearningCoursepathUser':
    			$enrollmentName = 'L' . $enrollment->id_path . "_" . $enrollment->idUser;
    			break;

    		default:
    			return;
    			break;

    	}

    	// Set last access to NOW (UTC)
    	$dateNowUtc = Yii::app()->localtime->getUTCNow();
    	$sfEnrollments = $this->getSfEnrollments("Name='$enrollmentName'");

    	if (empty($sfEnrollments)) return;

    	$sfEnrollment = $sfEnrollments[$enrollmentName];

    	$fields = array();
        $fieldsToNull = array();

    	$fields['Id']                              = $sfEnrollment['Id'];

    	if (!$ignoreLastAccess)                                    $fields['D_DC_COURSE_LA__c']            = $dateNowUtc;
    	if ($sfEnrollment['D_DC_COURSE_FA__c'] === false)          $fields['D_DC_COURSE_FA__c']            = $dateNowUtc;
    	if ($minutesInCourse)                                      $fields['N_DC_TIME_IN_COURSE__c']       = $minutesInCourse;
    	if ($courseScore)                                          $fields['N_DC_COURSE_SCORE__c']         = $courseScore;
    	if ($courseMaxScore)                                       $fields['N_DC_MAX_COURSE_SCORE__c']     = $courseMaxScore;
    	if ($completionPct || $completionPct === 0)                $fields['N_DC_COMPLETION_PCT__c']       = $completionPct;

    	// If not yet set as completed.... (because if it is already completed.. we do NOT change its status, except when LP has changed)
        if($resetCompletion){
            if(!$dontUpdateStatus) {
                switch ($completionPct) {
                    case 0:
                        $fields['C_DC_COURSE_STATUS__c'] = self::ENROLLSTATUS_SUBSCRIBED;
                        $fieldsToNull[] = 'D_DC_COURSE_COMP__c';
                        $fieldsToNull[] = 'N_DC_COMPLETION_PCT__c';
                        break;
                    case 100:
                        $fields['C_DC_COURSE_STATUS__c'] = self::ENROLLSTATUS_COMPLETED;
                        break;
                    default:
                        $fields['C_DC_COURSE_STATUS__c'] = self::ENROLLSTATUS_IN_PROGRESS;
                        $fieldsToNull[] = 'D_DC_COURSE_COMP__c';
                }
                $fields['PATH_COMPLETION__c'] = $pathCompleted;
                if ($completedDatetime) {
                    $fields['D_DC_COURSE_COMP__c'] = $completedDatetime;
                }
            }
        } elseif ($sfEnrollment['C_DC_COURSE_STATUS__c'] != self::ENROLLSTATUS_COMPLETED) {
    	    if ($completedDatetime) {
    	        $fields['D_DC_COURSE_COMP__c']          = $completedDatetime;
    	        $fields['C_DC_COURSE_STATUS__c']        = self::ENROLLSTATUS_COMPLETED;
    	    }
    	    else {
    	        $fields['C_DC_COURSE_STATUS__c']        = self::ENROLLSTATUS_IN_PROGRESS;    	        
    	    }
    	}

        // Update status to "completed" upon learning path completion
        if(get_class($enrollment) == 'LearningCoursepathUser' && $completionPct == 100){
            $fields['C_DC_COURSE_STATUS__c'] = self::ENROLLSTATUS_COMPLETED;
        }

        // Always set the LP first access time to datetime for first attempted LO
        if($firstAccess != false){
            $fields['D_DC_COURSE_FA__c'] = $firstAccess;
        }

    	$sObject = new SObject();
    	$sObject->fields = $fields;
    	$sObject->type = self::CO_COURSE_ENROLLMENT;
        $sObject->fieldsToNull = $fieldsToNull;
    	$objects= array($sObject);

    	$response = $this->updateChunked($objects);

    	return $response;
    	
    }
}