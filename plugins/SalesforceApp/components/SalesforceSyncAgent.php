<?php

class SalesforceSyncAgent extends CComponent {
	
	/** 
	 * @var SalesforceLmsAccount 
	 */
	private $account;
	
	/** 
	 * @var SalesforceApiClient 
	 */
	private $api;
	
	/**
	 * Constructor 
	 *  
	 * @param integer $idAccount  Salesforce account ID (in the LMS!)
	 */
	public function __construct($idAccount = null) {
		
		
		if (!$idAccount) {
			$this->account = SalesforceLmsAccount::getFirstAccount();
		} else{
            $this->account 		= SalesforceLmsAccount::model()->findByPk($idAccount);
        }
		$this->api 	= new SalesforceApiClient($this->account);  
		
	}
    
    /**
     * Generating random password for the user
     * 
     * @return string random password
     */
    private function generatePassword(){
        $randomPassword = chr(mt_rand(65, 90))
					. chr(mt_rand(97, 122))
					. mt_rand(0, 9)
					. chr(mt_rand(97, 122))
					. chr(mt_rand(97, 122))
					. chr(mt_rand(97, 122))
					. mt_rand(0, 9)
					. chr(mt_rand(97, 122));
        
        return $randomPassword;
    }

    private function insertSfOrgChart($idOrg, $idParent, $sfLmsAccount, $type,  $accountId = null){
        $insert = Yii::app()->db->createCommand()
            ->insert(SalesforceOrgchart::model()->tableName(), array(
                'id_org'        => $idOrg,
                'id_parent'     => $idParent,
                'sf_lms_id'     => $sfLmsAccount,
                'sf_type'       => $type,
                'sf_account_id' => $accountId,
                'active'        => 1,
                'is_root'       => 0
            ));
        if($insert){
            return Yii::app()->db->getLastInsertID();
        } else {
            return false;
        }
    }       

	
	/**
     * Replicate Branches from Salesforce Users | Contacts | Accounts to LMS.
     * It depends of what is choosen to be replicated
     * 
     * @return boolean Success
     */
	public function structureReplication() {
        //todo: remove this function (deprecated)
        set_time_limit(1500);
        $userReplications = $this->account->getUserReplications();

        $dataInfo = array();
        
        if($userReplications === null){
            return true;
        }

        $activeLanguages = $this->getActiveLanguages();
        
        if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $userReplications) && SalesforceOrgchart::isStructured($this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_USERS) == false){            
            $orgId = SalesforceLmsAccount::createSalesforceOrgChart("Users", $activeLanguages, $this->account->id_org);
            if($this->insertSfOrgChart($orgId, $this->account->id_org, $this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_USERS)){
                $dataInfo['users'] = 'Users Branch' . ' ' . Yii::t('salesforce', 'created');
            }
        }
        
        if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $userReplications) && SalesforceOrgchart::isStructured($this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) == false){            
            $orgId = SalesforceLmsAccount::createSalesforceOrgChart("Contacts", $activeLanguages, $this->account->id_org);
            if($this->insertSfOrgChart($orgId, $this->account->id_org, $this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS)){
                $dataInfo['contacts'] = 'Contacts Branch' . ' ' . Yii::t('salesforce', 'created');
            }
        }

        //Creating or Updating product Family
        if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
        	
        	Yii::log('Creating Product Family');
        	$result = $this->api->updateProduct2FamilyPicklist();
        	Yii::log(var_export($result, true));
        	
        	if($result->result->success == false){
        		// return false;
        	} else{
        		$dataInfo['ProductFamily'] = 'Product Family' . ' ' . Yii::t('salesforce', 'created');
        	}
        }

		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
	        //Creating or Updating Order Type
	        Yii::log('Creating Order type');
	        $result = $this->api->updateOrderTypePicklist();
	        Yii::log(var_export($result, true));
	        if($result->result->success == false){
	            return false;
	        } else{
	            $dataInfo['OrderType'] = 'Order Type' . ' ' . Yii::t('salesforce', 'created');
	        }
		}
        // Creating Custom Objects
        // COURSE/PLAN
        if(!$this->api->customObjectExists(SalesforceApiClient::CO_COURSE)){

            $addProductField = $this->addProductField($this->account->replicate_courses_as_products);
        	Yii::log('Creating Courses Custom Object');
            $result = $this->api->createCourseCustomObject($addProductField);
            /**
             * replicate_courses_highest_history represents the highest setting the LMS account has ever had for replicate_courses_as_products
             * if value = 0 --> Configuration never had product replication active
             * if value = 1 --> Currently or previously, product replication has been active, but not Sales Orders
             * if value = 2 --> Currently or previously, product replication and Sales Orders have been active
             * Based on this value, we're able to determine whether or not to add extra fields to the Custom Objects in Salesforce
             */
            Yii::app()->db->createCommand()
                ->update(SalesforceLmsAccount::model()->tableName(), array(
                    'replicate_courses_highest_history' => $this->account->replicate_courses_as_products
                ), 'id_account = :idAccount', array(
                    ':idAccount' => $this->account->id_account
                ));

            Yii::log(var_export($result, true));
            if($result->result->success == false){
                return false;
            } else{
                $dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'created');
            }
        } elseif(($this->account->replicate_courses_as_products > $this->account->replicate_courses_highest_history)
                &&
                $this->account->replicate_courses_highest_history == SalesforceLmsAccount::TYPE_REPLICATE_NONE) {

            $result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE, SalesforceApiClient::$COURSE_CUSTOM_OBJECT_FIELDS, 'Product');

            Yii::log(var_export($result, true));
            if($result->result->success == true){
                $dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'updated');
            }
        }

        // ENROLLMENTS
        if(!$this->api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)){
			$addOrderField = false;
	        if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
		        $addOrderField = true;
	        }

            $addProductField = $this->addProductField($this->account->replicate_courses_as_products);

        	Yii::log('Creating Enrollments Custom Object');
            $result = $this->api->createEnrollmentCustomObject($addProductField, $addOrderField);
            Yii::log(var_export($result, true));
            if($result->result->success == false){
                return false;
            } else{
                $dataInfo['CO2'] = 'Course Custom Object' . ' ' . Yii::t('salesforce', 'created');
            }
        } elseif($this->account->replicate_courses_as_products > $this->account->replicate_courses_highest_history) {
            if($this->account->replicate_courses_as_products >= SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
                $result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Product');
            }
            if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
                // todo: create Order field in Custom Object 2
                $result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Order');
            }
            if($result->result->success == true){
                $dataInfo['CO2'] = 'Enrollment Custom Object' . ' ' . Yii::t('salesforce', 'updated');
            }

            Yii::app()->db->createCommand()
                ->update(SalesforceLmsAccount::model()->tableName(), array(
                    'replicate_courses_highest_history' => $this->account->replicate_courses_as_products
                ), 'id_account = :idAccount', array(
                    ':idAccount' => $this->account->id_account
                ));
        }




		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
			// Create Pricebook
			Yii::log('Creating Pricebook');
			$result = $this->api->createPriceBook();
			Yii::log(var_export($result, true));
			if ($result === false) return false;
			$dataInfo['Pricebook'] = 'Pricebook' . ' ' . Yii::t('salesforce', 'created');
		}


		$addDmyContact = false;
		$dummyStuff = 'Dummy account' . ' ' . Yii::t('salesforce', 'created');
		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
			$addDmyContact = true;
			$dummyStuff =  'Dummy account & contract' . ' ' . Yii::t('salesforce', 'created');
		}

		$addPriceBook = false;
		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
			$addPriceBook = true;
		}

        return $dataInfo;
		
	}

    /**
     * Creates the condition to fetch records from Salesforce
     *
     * @param $listViewType string Account|User|Contact|Lead
     * @param $listViewFilter object
     * @param $fieldDefinitions array
     * @param array $additionalParams array
     * @return null|string
     */
    public function generateFilterString($listViewType, $listViewFilter, $fieldDefinitions, $additionalParams = array()){
        $filters = array();
        if(!is_array($additionalParams)) $additionalParams = array($additionalParams);
        if($listViewFilter['filters']){
            if(is_array($listViewFilter['filters'])){
                foreach($listViewFilter['filters'] as $filter){
                    $newFilter = $this->markupSoqlCondition($filter, $listViewType, $fieldDefinitions);
                    if($newFilter) $filters[] = $newFilter;
                }
            } else {
                $newFilter = $this->markupSoqlCondition($listViewFilter['filters'], $listViewType, $fieldDefinitions);
                if($newFilter) $filters[] = $newFilter;
            }
        }
        if(!empty($filters)) {
            if($listViewFilter['booleanFilter'] !== false){ // in case custom filter logic is used in listview filters
                $filterContainer = $this->prepareFilterLogic($listViewFilter['booleanFilter']);
                foreach($filters as $i => $filter){
                    $filterContainer = strtoupper(str_replace("::".($i+1)."::", $filter, $filterContainer));
                }
                $filters = array("(".$filterContainer.")");
            }
            if(!empty($additionalParams)) $filters = array_merge($additionalParams, $filters);
            return implode(" AND ", $filters);
        } else {
            return null;
        }
    }

    /**
     * Fetches the custom fields amongst the regular fields in an object
     * @param $sobjectType string
     * @return array
     */
    public function getAllFields($sobjectType){
        $fieldDefinitions = array();
        $describeObj = $this->api->connection->describeSObject($sobjectType);
        if($describeObj){
            foreach($describeObj->fields as $field){
                $fieldDefinitions[$field->name] = $field->type;
            }
        }
        return $fieldDefinitions;
    }

    /**
     * Checks if there's an error in the api (eg, when SF plugin is active, but no account exists or no valid account exists)
     * @return string
     */
    public function getApiError(){
        return $this->api->error();
    }

    /**
     * Bloody awful method to translate readMetadata stuff to create a query
     * Please don't shoot me for this, shoot the people that created Salesforce... and their families... and their dog... the goldfish may live
     *
     * @param $obj object Contains field, operator & value
     * @param $listViewType string Account|User|Contact|Lead
     * @param $fieldDefinitions array
     * @return string
     */
    public function markupSoqlCondition($obj, $listViewType, $fieldDefinitions){
        $isDateField = false;
        $isBoolField = false;
        $isUserTypeField = false;
        $isCustomField = false;

        // Handling field
        if(strpos($obj->field, "__c") !== false){ // Custom fields
            $field = $obj->field;
            $isCustomField = true;
            $compareField = $obj->field;
        } else { // Default fields
            // First: convert some frequently used strings to their query counterparts
            switch($listViewType){
                case "Account":
                    $extendFind = array('ADDRESS1_', 'ADDRESS2_', 'CORE.USERS.ALIAS', 'CORE.USERS.FIRST_NAME', 'CORE.USERS.LAST_NAME', 'CORE.USERS');
                    $extendReplace = array('BILLING_', 'SHIPPING_', 'OWNER.ALIAS', 'OWNER.FIRST_NAME', 'OWNER.LAST_NAME', 'USER');
                    break;
                case "User":
                    $extendFind = array('CORE.USERS', 'USER.ADDRESS_', 'CORE.USER_ROLE', 'CORE.PROFILE', 'SALES.CONTACT.NAME');
                    $extendReplace = array('USER', 'USER.', 'USER_ROLE', 'PROFILE', 'CONTACT.NAME');
                    break;
                case "Contact":
                    $extendFind = array('ADDRESS1_', 'ADDRESS2_', 'CORE.USERS.');
                    $extendReplace = array('OTHER_', 'MAILING_', 'OWNER.');
                    break;
            }
            $defaultFind = array('_ZIP', '.ZIP', '.PHONE1', '.PHONE2', '.PHONE3', '.PHONE4', '.PHONE5', '.PHONE6', '.CELL', '.URL', '.EMPLOYEES', 'DANDB_COMPANY', 'JIGSAW_KEY', 'DBA_NAME', '.SALES', '.TICKER', 'CREATEDBY_USER.', 'UPDATEDBY_USER.');
            $defaultReplace = array('_POSTAL_CODE', '.POSTAL_CODE', '.PHONE', '.FAX', '.MOBILE_PHONE', '.HOME_PHONE', '.OTHER_PHONE', '.ASSISTANT_PHONE', '.MOBILE_PHONE', '.WEBSITE', '.NUMBER_OF_EMPLOYEES', 'DANDB_COMPANY_ID', 'JIGSAW', 'TRADESTYLE', '.ANNUAL_REVENUE', '.TICKER_SYMBOL', 'CREATED_BY.', 'LAST_MODIFIED_BY.');

            $find = array_merge($extendFind, $defaultFind);
            $replace = array_merge($extendReplace, $defaultReplace);
            $objField = str_ireplace($find, $replace, $obj->field);
            switch($objField){
                case "FULL_NAME":
                    $field = "Name";
                    break;
                case "ACCOUNT.ACCOUNT_ID":
                    $field = "Account.Id";
                    break;
                case "PARENT.NAME":
                case "PARENT_NAME":
                    $field = "Account.Parent.Name";
                    break;
                case "CREATED_BY_NAME":
                    $field = "CreatedBy.Name";
                    break;
                case "CREATED_BY_ALIAS":
                case "CREATEDBY_USER.ALIAS":
                    $field = "CreatedBy.Alias";
                    break;
                case "LAST_UPDATE_BY_ALIAS":
                case "UPDATEDBY_USER.ALIAS":
                    $field = "LastModifiedBy.Alias";
                    break;
                case "ACCOUNT.LAST_ACTIVITY":
                    $field = "Account.LastActivityDate";
                    $isDateField = 'long';
                    break;
                case "CONTACT.LAST_ACTIVITY":
                    $field = "LastActivityDate";
                    $isDateField = 'short';
                    break;
                case "ACCOUNT.CREATED_DATE":
                case "CONTACT.CREATED_DATE":
                case "CREATED_DATE":
                    $field = "CreatedDate";
                    $isDateField = 'long';
                    break;
                case "ACCOUNT.LAST_UPDATE":
                case "CONTACT.LAST_UPDATE":
                case "LAST_UPDATE":
                    $field = "LastModifiedDate";
                    $isDateField = 'long';
                    break;
                case "USER.LAST_LOGIN":
                    $field = "User.LastLoginDate";
                    $isDateField = 'long';
                    break;
                case "CONTACT.BIRTHDATE":
                    $field = "Contact.Birthdate";
                    $isDateField = 'short';
                    break;
                case "CONTACT.LAST_CU_REQUEST_DATE":
                    $field = "LastCURequestDate";
                    $isDateField = 'long';
                    break;
                case "CONTACT.LAST_CU_UPDATE_DATE":
                    $field = "LastCUUpdateDate";
                    $isDateField = 'long';
                    break;
                case "CONTACT.EMAIL_BOUNCED_DATE":
                    $field = "EmailBouncedDate";
                    $isDateField = 'long';
                    break;
                case "USER.ACTIVE":
                    $field = "User.IsActive";
                    $isBoolField = true;
                    break;
                case "USER.INFO_EMAILS":
                    $field = "User.ReceivesInfoEmails";
                    $isBoolField = true;
                    break;
                case "USER.INFO_EMAILS_ADMIN":
                    $field = "User.ReceivesAdminInfoEmails";
                    $isBoolField = true;
                    break;
                case "MARKETING_USER":
                case "OFFLINE_USER":
                case "MOBILE_USER":
                case "JIGSAW_PROSPECTING_USER":
                case "SITEFORCE_CONTRIBUTOR_USER":
                case "SITEFORCE_PUBLISHER_USER":
                case "KNOWLEDGE_USER":
                    $field = "UserPermissions".$this->convertUppercaseToCamelcase($objField);
                    $isBoolField = true;
                    break;
                case "SFCONTENT_USER":
                    $field = "UserPermissionsSFContentUser";
                    $isBoolField = true;
                    break;
                case "USER.FORECAST_ENABLED":
                    $field = "User.ForecastEnabled";
                    $isBoolField = true;
                    break;
                case "IS_EMAIL_ADDRESS_BOUNCED":
                    $field = "IsEmailBounced";
                    $isBoolField = true;
                    break;
                case "PROFILE.USERTYPE":
                    $field = "Profile.UserType";
                    $isUserTypeField = true;
                    break;
                case "MANAGER":
                    $field = "Manager.Name";
                    break;
                case "a.BillingAddress":
                case "a.ShippingAddress":
                case "c.MailingAddress":
                case "u.Address":
                case "c.OtherAddress":
                case "l.Address":
                    $field = substr($objField, strrpos($objField, ".")+1);
                    break;
                case "LANGUAGE": //Todo: Need to check out Language for User
                case "LOCALE": //Todo: Need to check out Locale for User
                case "TIMEZONE": //Todo: Need to check out Timezone for User
                case "EMAIL_ENCODING":
                case "SU_ACCESS_EXPIRATION":
                case "SU_ORG_ADMIN_EXPIRATION":
                case "IS_FROZEN":
                case "STORAGE_USED":
                case "DELEGATED_APPROVER":
                case "START_OF_DAY":
                case "END_OF_DAY":
                case "PASSWORD_EXPIRES":
                case "CLEAN_STATUS":
                case "CAMPAIGN_MEMBER.STATUS":
                case "Topics":
                    return false;
                    break;
                default: // just convert to ucwords() eg. ACCOUNT.TYPE --> Account.Type
                    $field = $this->convertUppercaseToCamelcase($objField);
                    break;
            }

            if(strpos($field, ".") !== false){
                $compareField = substr($field, strrpos($field, '.') + 1);
            } else {
                $compareField = $field;
            }
        }

        // Handling operator and value init
        $valuePrefix = "'";
        $valueSuffix = "'";
        $filterPrefix = "";
        switch($obj->operation){
            case "equals":
                $operator = "=";
                break;
            case "notEqual":
                $operator = "<>";
                break;
            case "contains":
                $operator = "LIKE";
                $valuePrefix = "'%";
                $valueSuffix = "%'";
                break;
            case "notContain":
                $operator = "LIKE";
                $valuePrefix = "'%";
                $valueSuffix = "%')";
                $filterPrefix = "(NOT ";
                break;
            case "startsWith":
                $operator = "LIKE";
                $valueSuffix = "%'";
                break;
            case "lessThan":
                $operator = "<";
                break;
            case "lessOrEqual":
                $operator = "<=";
                break;
            case "greaterThan":
                $operator = ">";
                break;
            case "greaterOrEqual":
                $operator = ">=";
                break;
            case "within":
                $distanceParts = explode(":", $obj->value);
                return "DISTANCE(".$field.", GEOLOCATION(".$distanceParts[0].",".$distanceParts[1]."), '".$distanceParts[3]."') < ".$distanceParts[2]."";
                break;
        }

        // Handling numeric search values
        $arNumericOperators = array('equals', 'notEqual', 'lessThan', 'lessOrEqual', 'greaterThan', 'greaterOrEqual');
        $arListOperators = array('equals', 'notEqual', 'includes', 'excludes');
        $arStringFields = array('string', 'phone', 'address', 'textarea', 'url');
        if(in_array($obj->operation, $arNumericOperators) && $isDateField) { // Handling dates
            if(empty($obj->value)){ // date = null breaks the function --> so just skip it
                return $this->_handleDate("NULL", $isDateField, $field, $obj->operation);
            } else {
                return $this->_handleDate($obj->value, $isDateField, $field, $obj->operation);
            }
        } elseif($isCustomField && in_array($fieldDefinitions[$obj->field], array('date', 'datetime'))){ // Handling dates for custom fields
            switch($fieldDefinitions[$obj->field]){
                case "date":
                    $isDateField = "short";
                    break;
                case "datetime":
                    $isDateField = "long";
                    break;
            }
            if(!empty($obj->value)){
                return $this->_handleDate($obj->value, $isDateField, $field, $obj->operation);
            } else {
                return $this->_handleDate("NULL", $isDateField, $field, $obj->operation);
            }
        } elseif(in_array($obj->operation, $arNumericOperators) && $isBoolField) { // Handling booleans
            $value = $this->_handleBoolean($obj->value);
        } elseif($isCustomField && $fieldDefinitions[$obj->field] == 'boolean') { // Handling booleans for custom fields
            $value = $this->_handleBoolean($obj->value);
        } elseif(in_array($obj->operation, $arNumericOperators) && $isUserTypeField) { // Handling UserTypes
            $value = $valuePrefix.$this->_handleUserType($obj->value).$valueSuffix;
        } elseif(in_array($obj->operation, $arNumericOperators) && (is_numeric($obj->value) || is_real($obj->value))) { // Handling numeric search values
            if (!empty($obj->value)) {
                $value = $obj->value;
            } else {
                $value = "NULL";
            }
        } elseif($isCustomField && !isset($obj->value)){ // Handling custom datatypes set to NULL
            $value = 'NULL';
        } elseif(in_array($obj->operation,$arListOperators ) && (strpos($obj->value, ",") !== false)) { // Handling Lists()
            $values = explode(",", $obj->value);
            $value = "";
            foreach($values as $v){
                if(is_numeric($v) || is_real($v)){ // Numeric values
                    $value .= ",".$v;
                } else { // String values
                    $value .= ",'".$v."'";
                }
            }
            $value = "(".substr($value, 1).")";
            switch($obj->operation){
                case "equals":
                    $operator = "IN";
                    break;
                case "notEqual":
                    $operator = "IN";
                    $filterPrefix = "(NOT ";
                    $value .= ")";
                    break;
                case "includes":
                    $operator = "INCLUDES";
                    break;
                case "excludes":
                    $operator = "EXCLUDES";
            }
        } else {  // Extra string handling (may fuck up default handling in case of int values)
            $value = $valuePrefix.$obj->value.$valueSuffix;
        }
        return $filterPrefix.$field." ".$operator." ".$value;
    }

    /**
     * Handling boolean values for SF filters
     * @param $value mixed
     * @return string
     */
    protected function _handleBoolean($value){
        switch ($value) {
            case "0":
            case 0:
            case "false":
            case false:
                return "false";
                break;
            default:
                return "true";
        }
    }

    /**
     * Handling user_type for SF filters
     * @param $value mixed
     * @return string
     */
    protected function _handleUserType($value){
        $findReplace = array(
            'S' => 'Standard',
            'P' => 'Partner',
            'C' => 'CustomerPortalManager',
            'c' => 'CustomerPortalUser',
            'G' => 'Guest',
            'b' => 'HighVolumePortal',
            'n' => 'CsnOnly',
            'F' => 'Self Service'
        );
        if (array_key_exists($value, $findReplace)) {
            return $findReplace[$value];
        } else {
            return $value;
        }
    }

    /**
     * Handling date types for SF filters
     * @param $value mixed
     * @param $type string long|short
     * @param $field string
     * @param $operator string
     * @return string
     */
    protected function _handleDate($value, $type, $field, $operator){
        if($value != "NULL") {
            $dateUnformatted = $value;
            if (strpos($dateUnformatted, ' ') !== false) $dateUnformatted = trim(substr($dateUnformatted, 0, strpos($dateUnformatted, ' ')));
            $d = explode("/", $dateUnformatted);
            $minDateTime = $d[2] . '-' . str_pad($d[0], 2, '0', STR_PAD_LEFT) . '-' . str_pad($d[1], 2, '0', STR_PAD_LEFT);
            $maxDateTime = $d[2] . '-' . str_pad($d[0], 2, '0', STR_PAD_LEFT) . '-' . str_pad($d[1], 2, '0', STR_PAD_LEFT);
            if ($type == "long") {
                $minDateTime .= "T00:00:00.000Z";
                $maxDateTime .= "T23:59:59.000Z";
            }
            switch ($operator) {
                case "equals":
                    return $field . " > " . $minDateTime . " AND " . $field . " < " . $maxDateTime;
                    break;
                case "notEqual":
                    return $field . " < " . $minDateTime . " AND " . $field . " > " . $maxDateTime;
                    break;
                case "lessThan":
                    return $field . " < " . $minDateTime;
                    break;
                case "lessOrEqual":
                    return $field . " < " . $maxDateTime;
                    break;
                case "greaterThan":
                    return $field . " > " . $maxDateTime;
                    break;
                case "greaterOrEqual":
                    return $field . " > " . $minDateTime;
                    break;
            }
        } else {
            switch ($operator) {
                case "equals":
                    return $field . " = NULL";
                    break;
                case "notEqual":
                    return $field . " <> NULL";
                    break;
                case "lessThan":
                    return $field . " < NULL";
                    break;
                case "lessOrEqual":
                    return $field . " < NULL";
                    break;
                case "greaterThan":
                    return $field . " > NULL";
                    break;
                case "greaterOrEqual":
                    return $field . " > NULL";
                    break;
            }
        }
    }

    public function validateFilterString($objType, $filter){
        $errors = array();
        $data = array();
        $success = false;
        try{
            if($filter){
                $filter = " WHERE ".$filter;
            }
            $query = "SELECT Name FROM ".$objType.$filter." LIMIT 1";
            $this->api->connection->query($query);
            $success = true;
        } catch(Exception $e){
            $errors = array_merge($errors, array($e->getMessage()));
        }
        $data['errors'] = $errors;
        $data['success'] = $success;
        return $data;
    }

    /**
     * Returns a count of the number of items in a selected listview
     * @param string $objType
     * @param string $filter
     * @return array
     */
    public function countItemsInListview($objType, $filter = ""){
        $errors = array();
        $data = array();
        $numRecords = false;
        try{
            if($filter){
                $filter = " WHERE ".$filter;
            }
            $query = "SELECT COUNT() FROM ".$objType.$filter;
            $response = $this->api->connection->query($query);
            $queryResult = new QueryResult($response);
            $numRecords = $queryResult->size;
        } catch(Exception $e){
            $errors = array_merge($errors, array($e->getMessage()));
        }
        $data['errors'] = $errors;
        $data['records'] = $numRecords;
        return $data;
    }

    /**
     * Prepares a given filter replacement string to be easily overridden.
     * eg: 3 AND (1 OR 2) --> ::3:: AND (::1:: OR ::2::)
     * @param string $filter
     * @return string
     */
    public function prepareFilterLogic($filter){
        $pattern = '/(\d+)/';
        $replace = '::${1}::';
        return preg_replace($pattern, $replace, $filter);
    }

    /**
     * Converts the fieldname FROM readMetadata calls to what's needed in a query
     * eg: ACCOUNT.FIRST_NAME --> Account.FirstName
     *
     * @param $fieldname string
     * @return string
     */
    public function convertUppercaseToCamelcase($fieldname){
        $arSymbolsFrom = array(".", "_");
        $arSymbolsTo = array(" . ", " _ ");
        return str_replace("_", "", str_replace($arSymbolsTo, $arSymbolsFrom, ucwords(strtolower(str_replace($arSymbolsFrom, $arSymbolsTo, $fieldname)))));
    }

    public function addProductField($structure_replication){
        if($structure_replication == SalesforceLmsAccount::TYPE_REPLICATE_NONE){
            return false;
        } else {
            return true;
        }
    }
    
    
    /**
     * Create the Branch stuchture of the Salesforce Accounts   
     * 
     * @param array $accounts Salesforce Accounts
     * @param string $idParent Parent element( if "null" parent is Root Node)
     * @param int $contactOrgId Contact Orgchart id (for orphan child accounts)
     * @return array Number of created elements and errors (if any)
     */
    private function createAccountStructure($accounts, $idParent = null, $contactOrgId){
        //todo: remove this function (deprecated)
        set_time_limit(3*3600); // 3 hours
        $errors = array();
        $items = 0;
        $alreadyInLMS = array();

        $activeLanguages = $this->getActiveLanguages();
        
        foreach ($accounts as $account){
            $sfOrgChart = Yii::app()->db->createCommand()
                ->select('id, id_parent, id_org, sf_account_id, sf_contract_id')
                ->from(SalesforceOrgchart::model()->tableName())
                ->where('sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
                    ':lmsId' => $this->account->id_account,
                    ':accountId' => $account['Id'],
                    ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                ))
                ->queryRow();

            if(!$sfOrgChart){
                $parentId = $idParent;

                if(!$parentId){
                    $parentId = Yii::app()->db->createCommand()
                        ->select('id_org')
                        ->from(SalesforceOrgchart::model()->tableName())
                        ->where('sf_lms_id = :lmsId AND sf_account_id = :parentId AND sf_type = :type', array(
                            ':lmsId' => $this->account->id_account,
                            ':parentId' => $account['ParentId'],
                            ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                        ))
                        ->queryScalar();
                }

                // If there's still no parentId set, it's because this account is an orphaned child account
                // We'll move this to the main contact branch
                if(!$parentId){
                    $parentId = $contactOrgId;
                }
                //Create the root in CoreOrgChartTree and CoreOrgChart
                $orgId = SalesforceLmsAccount::createSalesforceOrgChart($account['Name'], $activeLanguages, $parentId);

                //Insert in SalesforceOrgchart
                $sfAccountId = $this->insertSfOrgChart($orgId, $parentId, $this->account->id_account, SalesforceLmsAccount::SF_TYPE_ACCOUNT, $account['Id']);
                if(!$sfAccountId){
                    $errors[] = "Cannot create Branch";
                } else{
                    $items++;
                    $alreadyInLMS[] = $sfAccountId;
                }
            }
            else{
                $parentId = Yii::app()->db->createCommand()
                    ->select('id_org')
                    ->from(SalesforceOrgchart::model()->tableName())
                    ->where('sf_lms_id = :lmsId AND sf_account_id = :parentId AND sf_type = :type', array(
                        ':lmsId' => $this->account->id_account,
                        ':parentId' => $account['ParentId'],
                        ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                    ))
                    ->queryScalar();


                if($parentId && ($parentId != SalesforceOrgchart::getSfBranchIdOrg(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) && $parentId != $sfOrgChart['id_parent'])){

                    $org = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $sfOrgChart['id_org']));
                    $newParent = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $parentId));
                    if($org->moveAsFirst($newParent)){
                        Yii::app()->db->createCommand()
                            ->update(SalesforceOrgchart::model()->tableName(), array(
                                'id_parent' => $newParent->idOrg
                            ), 'sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
                                ':lmsId' => $this->account->id_account,
                                ':accountId' => $account['Id'],
                                ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                            ));

                        $items++;
                        $alreadyInLMS[] = $sfOrgChart['sf_account_id'];
                    }
                }

            	//Checking if the Name is the same as our branch
                $existingTranslation = Yii::app()->db->createCommand()
                    ->select('translation')
                    ->from(CoreOrgChart::model()->tableName())
                    ->where('id_dir = :idDir AND lang_code = :langCode', array(
                        ':idDir' => $sfOrgChart['id_org'],
                        ':langCode' => 'english'
                    ))
                    ->queryScalar();

                if($existingTranslation != $account['Name']){
                    Yii::app()->db->createCommand()
                        ->update(CoreOrgChart::model()->tableName(), array(
                            'translation' => $account['Name']
                        ), 'id_dir = :idDir', array(
                            ':idDir' => $sfOrgChart['id_org']
                        ));
                    $items++;
                }

            	$alreadyInLMS[] = $sfOrgChart['id'];
            }
            
            //If we create a brand new account, now we need to get it from the DB
            if(!$sfOrgChart){
                $sfOrgChart = Yii::app()->db->createCommand()
                    ->select('sf_contract_id')
                    ->from(SalesforceOrgchart::model()->tableName())
                    ->where('sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
                        ':lmsId' => $this->account->id_account,
                        ':accountId' => $account['Id'],
                        ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                    ))
                    ->queryRow();
            }
            
            //Only if SO is enabled!!
            if($this->account->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS){
                if($sfOrgChart && empty($sfOrgChart['sf_contract_id'])){
                    
                    //Get the ONLY ONE contract with the specific conditions
                    //@see SalesforceApiClient::getValidAccountContract
                    $contract = $this->api->getValidAccountContract($account['Id']);
                    $contractId = null;
                    
                    if(!$contract){
                        //Create new Contract
                        $response = $this->api->createAccountContract($account['Id']);
                        $contractId = $response[0]->id;
                    }
                    
                    //Save the contract ID in SalesforceOrgchart
                    $result = Yii::app()->db->createCommand()
                        ->update(SalesforceOrgchart::model()->tableName(), array(
                            'sf_contract_id' => $contractId
                        ), 'sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
                            ':lmsId' => $this->account->id_account,
                            ':accountId' => $account['Id'],
                            ':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
                        ));
                }
            }
            
        }
        
        $data = array(
            'errors' => $errors,
            'items' => $items,
            'alreadyInLMS' => $alreadyInLMS
        );
        
        return $data;

    }

    /**
     * Add a branch in the org chart structure
     * @param string $type user|contact
     * @param string $fieldname
     * @param id $idParent
     * @param array $activeLanguages
     * @param string $sfAccountId
     * @return int
     */
    private function createBranch($type, $fieldname, $idParent, $activeLanguages, $sfAccountId = null){
        //Create the root in CoreOrgChartTree and CoreOrgChart
        $orgId = SalesforceLmsAccount::createSalesforceOrgChart($fieldname, $activeLanguages, $idParent);

        //Insert in SalesforceOrgchart
        $this->insertSfOrgChart($orgId, $idParent, $this->account->id_account, $type, $sfAccountId);

        return $orgId;
    }

    public function getActiveLanguages(){ // The non-AR way to fetch the languages
        $langs = Yii::app()->db->createCommand()
            ->select('lang_code')
            ->from(CoreLangLanguage::model()->tableName())
            ->where('lang_active = :langActive', array(
                ':langActive' => 1
            ))
            ->order('lang_code ASC')
            ->queryAll();
        $return = array();
        foreach($langs as $lang){
            $return[] = $lang['lang_code'];
        }
        return $return;
    }

    /**
     * @param bool $emptyTempTable Upon a first visit, the temporary table should be cleared out
     * @return array
     */
    public function syncUsers($emptyTempTable){
        
        set_time_limit(3*3600); // 3 hours
        $types = explode(',', $this->account->sync_user_types);
        $itemsCount = 0;
        $errors = array();
        $newUsers = array();
        $startTime = microtime(true);

        if($emptyTempTable) Yii::app()->db->createCommand()->delete(SalesforceSyncValues::model()->tableName());

        // Users
        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $types)){
            $usersCompleted = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'completed');
            Yii::log("user completed: ".$usersCompleted);
            if(!$usersCompleted) {
                Yii::log("Running usersync");
                $this->synchroniseUsers();
                $this->logMemoryUsage($startTime, microtime(true));
                return array('completed' => false);
            }
        } else {
            $this->disableRootAndChildBranches(SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
        }

        // Accounts (accounts only need to be synched when contacts are active)
        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $types)){
            $accountsCompleted = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'completed');
            Yii::log("accounts completed: ".$accountsCompleted);
            if(!$accountsCompleted){
                Yii::log("Running accountsync");
                $this->synchroniseAccounts();
                $this->logMemoryUsage($startTime, microtime(true));
                return array('completed' => false);
            }
        }

        // Contacts
        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $types)){
            $contactsCompleted = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'completed');
            Yii::log("contacts completed: ".$contactsCompleted);
            if(!$contactsCompleted){
                Yii::log("Running contactsync");
                $this->synchroniseContacts();
                $this->logMemoryUsage($startTime, microtime(true));
                return array('completed' => false);
            }
        } else {
            $this->disableRootAndChildBranches(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
        }

        // If we reach this point, all sync jobs have been done and we can wrap things up: start events, write log and clear temp table

        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $types)){ // Users
            $addUsers = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'newUsers');
            $newUsers = array_merge($newUsers, $addUsers);

            $countUsers = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'itemsCount');
            $itemsCount += $countUsers;

            $errorUsers = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'errors');
            $errors = array_merge($errors, $errorUsers);
        }

        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $types)){ // Accounts & contacts
            $addContacts = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'newContacts');
            $newUsers = array_merge($newUsers, $addContacts);

            $countAccounts = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'itemsCount');
            $countContacts = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'itemsCount');
            //$itemsCount += $countAccounts;
            $itemsCount += $countContacts;

            $errorAccounts = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'errors');
            $errorContacts = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'errors');
            $errors = array_merge($errors, $errorAccounts, $errorContacts);
        }

        // Update new accounts or existing accounts without any previous login attempts to update force_change setting in core_user
        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $types)) { // Updating users if needed
            $this->resetForceChangePasswords(SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
        }

        if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $types)) { // Updating contacts if needed
            $this->resetForceChangePasswords(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
        }

        $params = array(
            'users' => $newUsers,
            'branches' => $this->getLMSBranchIds()
        );
        Yii::app()->event->raise(EventManager::EVENT_MASS_IMPORT_BRANCH_USERS, new DEvent($this, $params));

        $data = array(
            'itemsCount' => $itemsCount,
            'errors' => $errors,
            'completed' => true
        );
        $this->logMemoryUsage($startTime, microtime(true));
        return $data;
    }

    public function synchroniseUsers(){
        set_time_limit(3*3600); // 3 hours
        $continue = true;

        try {
            $listViewModel = SalesforceSyncListviews::model()->findByAttributes(array(
                'idAccount' => $this->account->id_account,
                'type' => 'User'
            ));

            // check if there is a record available in salesforce_sync_values
            // if so, this isn't the first job handling this data type and we can safely fetch other arrays from the database
            // if not, this is the first job and we have to run some initial actions
            $lastRecordId = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'lastRecordId');

            if($lastRecordId == false){ // first run
                Yii::log('first run');
                $firstRun = true;

                $listViewName = $this->api->fetchListViewDevName('User', $listViewModel->listviewId);
                $listViewFilters = $this->api->fetchListViewFilters('User', $listViewName);
                $fieldDefinitions = $this->getAllFields('User');
                $filterString = $this->generateFilterString('User', $listViewFilters, $fieldDefinitions);
                $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'filterString', $filterString);

                $validate = $this->validateFilterString('User', $filterString);
                $listViewVisible = $this->api->checkListViewVisibility('User', $listViewName);

                $errors = array();

                if($validate['success'] == false){
                    $errors = array_merge($errors, $validate['errors']);
                    $continue = false;
                } elseif(!$listViewVisible){
                    $errors[] = 'The selected users listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
                    $continue = false;
                }
            } else { // subsequent runs
                Yii::log('subsequent run');
                $firstRun = false;
                $filterString = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'filterString');
            }


            if($continue) {
                if($firstRun) { // first run
                    // First: check the main branch
                    $this->updateOrgChartRoot($listViewModel);
                    $activeLanguages = $this->getActiveLanguages();
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'activeLanguages', $activeLanguages);

                    $additionalLmsFields = $this->getAdditionalLmsFields('User');
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalLmsFields', $additionalLmsFields);

                    $additionalSfFields = $this->getAdditionalSfFields($listViewModel);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalSfFields', $additionalSfFields);

                    $additionalFieldBranches = $this->getAdditionalFieldBranches($listViewModel);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalFieldBranches', $additionalFieldBranches);

                    $currentUserBranches = $this->getCurrentUserBranches(SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'currentUserBranches', $currentUserBranches);

                    // Second: deactivate all branches (additional fields hierarchy branches will be activated on the fly later)
                    $this->deactivateAllSalesforceBranches(SalesforceLmsAccount::SYNC_USER_TYPE_USERS);

                    $newUsers = array();
                    $itemsCount = 0;
                } else { // subsequent runs
                    $activeLanguages = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'activeLanguages');
                    $additionalLmsFields = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalLmsFields');
                    $additionalSfFields = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalSfFields');
                    $additionalFieldBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalFieldBranches');
                    $currentUserBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'currentUserBranches');
                    $newUsers = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'newUsers');
                    $itemsCount = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'itemsCount');
                    $errors = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'errors');

                    $filterString = $this->addLastRecordIdToFilterString($filterString, $lastRecordId);
                }

                $users = $this->api->getUsers($filterString, 1, false, $additionalLmsFields, $additionalSfFields);
                if(!empty($users['data'])) {
                    $dataParams = array(
                        'data' => $users['data'],
                        'type' => SalesforceLmsAccount::SYNC_USER_TYPE_USERS,
                        'syncType' => $listViewModel->orgchartType,
                        'activeLanguages' => $activeLanguages,
                        'additionalLmsFields' => $additionalLmsFields,
                        'additionalSfFields' => $additionalSfFields,
                        'additionalFieldBranches' => $additionalFieldBranches,
                        'accountBasedBranches' => array(),
                        'rootBranch' => $listViewModel->orgchartId,
                        'currentUserBranches' => $currentUserBranches,
                        'salesforceBranches' => array(),
                        'accountInfo' => array(),
                        'additionalAccountLmsFields' => array(),
                        'forceChangePassword' => $listViewModel->force_change_password
                    );
                    $data = $this->syncUsersType($dataParams);

                    $newUsers = array_merge($newUsers, $data['newUsers']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'newUsers', $newUsers);

                    $itemsCount += $data['itemsCount'];
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'itemsCount', $itemsCount);

                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'lastRecordId', $users['lastSalesforceId']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'additionalFieldBranches', $data['additionalFieldBranches']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'completed', ($users['pageCount'] == 1) ? 1 : 0);
                    Yii::log('pageCount: ' . $users['pageCount']);

                    $errors = array_merge($errors, $data['errors']);
                } else {
                    Yii::log('No matching users found. Skipping user sync.');
                    $errors[] = 'No matching users found. Skipping user sync.';
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'newUsers', array());
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'itemsCount', 0);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'completed', 1);
                }
            }
        }
        catch (Exception $e) {
            // Let's do a bit more logging here as SF API is really hard to debug
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
            Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
            $errors = array_merge($errors, array($e->getMessage()));
        }

        $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, 'errors', $errors);
    }

    public function synchroniseContacts(){
        set_time_limit(3*3600); // 3 hours
        $continue = true;

        try {
            $accountListviewModel = SalesforceSyncListviews::model()->findByAttributes(array(
                'idAccount' => $this->account->id_account,
                'type' => 'Account'
            ));
            $contactListviewModel = SalesforceSyncListviews::model()->findByAttributes(array(
                'idAccount' => $this->account->id_account,
                'type' => 'Contact'
            ));

            // check if there is a record available in salesforce_sync_values
            // if so, this isn't the first job handling this data type and we can safely fetch other arrays from the database
            // if not, this is the first job and we have to run some initial actions
            $lastRecordId = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'lastRecordId');

            if ($lastRecordId == false) { // first run
                $firstRun = true;

                $listViewName = $this->api->fetchListViewDevName('Contact', $contactListviewModel->listviewId);
                $listViewFilters = $this->api->fetchListViewFilters('Contact', $listViewName);
                $fieldDefinitions = $this->getAllFields('Contact');
                $filterString = $this->generateFilterString('Contact', $listViewFilters, $fieldDefinitions);
                $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'filterString', $filterString);

                $validate = $this->validateFilterString('Contact', $filterString);
                $listViewVisible = $this->api->checkListViewVisibility('Contact', $listViewName);

                $errors = array();

                if ($validate['success'] == false) {
                    $errors = array_merge($errors, $validate['errors']);
                    $continue = false;
                } elseif (!$listViewVisible) {
                    $errors[] = 'The selected users listview "' . $listViewName . '" could not be read. Please make it visible to all users.';
                    $continue = false;
                }
            } else { // subsequent runs
                $firstRun = false;
                $filterString = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'filterString');
            }


            if ($continue) {
                if ($firstRun) { // first run
                    // First: check the main branch
                    $this->updateOrgChartRoot($contactListviewModel);
                    $activeLanguages = $this->getActiveLanguages();
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'activeLanguages', $activeLanguages);

                    $additionalAccountLmsFields = $this->getAdditionalLmsFields('Account');
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalAccountLmsFields', $additionalAccountLmsFields);

                    $additionalContactLmsFields = $this->getAdditionalLmsFields('Contact');
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalContactLmsFields', $additionalContactLmsFields);

                    $additionalSfFields = $this->getAdditionalSfFields($accountListviewModel);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalSfFields', $additionalSfFields);

                    $additionalFieldBranches = $this->getAdditionalFieldBranches($accountListviewModel);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalFieldBranches', $additionalFieldBranches);

                    $accountInfo = $this->getAccountAdditionalFields();
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'accountInfo', $accountInfo);

                    $currentUserBranches = $this->getCurrentUserBranches(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'currentUserBranches', $currentUserBranches);

                    $accountBasedBranches = $this->getAccountBasedBranches($accountListviewModel);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'accountBasedBranches', $accountBasedBranches);

                    $salesforceBranches = $this->getSalesforceBranchIds();
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'salesforceBranches', $salesforceBranches);

                    // Second: deactivate all branches (additional fields hierarchy branches will be activated on the fly later)
                    $this->deactivateAllSalesforceBranches(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);

                    // Re-activate all active SF folders (account_based only)
                    if ($accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED) {
                        $this->activateAccountBasedBranches($accountListviewModel->orgchartId, $activeLanguages);
                    }

                    $newContacts = array();
                    $itemsCount = 0;
                } else { // subsequent runs
                    $activeLanguages = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'activeLanguages');
                    $additionalAccountLmsFields = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalAccountLmsFields');
                    $additionalContactLmsFields = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalContactLmsFields');
                    $additionalSfFields = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalSfFields');
                    $additionalFieldBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalFieldBranches');
                    $accountInfo = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'accountInfo');
                    $currentUserBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'currentUserBranches');
                    $accountBasedBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'accountBasedBranches');
                    $salesforceBranches = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'salesforceBranches');

                    $newContacts = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'newContacts');
                    $itemsCount = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'itemsCount');
                    $errors = $this->getSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'errors');

                    $filterString = $this->addLastRecordIdToFilterString($filterString, $lastRecordId);
                }

                $contacts = $this->api->getContacts($filterString, 1, false, $additionalContactLmsFields, array());
                if(!empty($contacts['data'])) {
                    $dataParams = array(
                        'data' => $contacts['data'],
                        'type' => SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS,
                        'syncType' => $accountListviewModel->orgchartType,
                        'activeLanguages' => $activeLanguages,
                        'additionalLmsFields' => $additionalContactLmsFields,
                        'additionalSfFields' => $additionalSfFields,
                        'additionalFieldBranches' => $additionalFieldBranches,
                        'accountBasedBranches' => $accountBasedBranches,
                        'rootBranch' => $accountListviewModel->orgchartId,
                        'currentUserBranches' => $currentUserBranches,
                        'salesforceBranches' => $salesforceBranches,
                        'accountInfo' => $accountInfo,
                        'additionalAccountLmsFields' => $additionalAccountLmsFields,
                        'forceChangePassword' => $contactListviewModel->force_change_password
                    );
                    $data = $this->syncUsersType($dataParams);

                    $newContacts = array_merge($newContacts, $data['newUsers']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'newContacts', $newContacts);

                    $itemsCount += $data['itemsCount'];
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'itemsCount', $itemsCount);

                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'lastRecordId', $contacts['lastSalesforceId']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'additionalFieldBranches', $data['additionalFieldBranches']);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'completed', ($contacts['pageCount'] == 1) ? 1 : 0);

                    $errors = array_merge($errors, $data['errors']);
                } else {
                    Yii::log('No matching contacts found. Skipping contact sync.');
                    $errors[] = 'No matching contacts found. Skipping contact sync.';
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'newContacts', array());
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'itemsCount', 0);
                    $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'completed', 1);
                }
            }
        }
        catch (Exception $e) {
            // Let's do a bit more logging here as SF API is really hard to debug
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
            Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
            $errors = array_merge($errors, array($e->getMessage()));
        }

        $this->saveSetting(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, 'errors', $errors);
    }

    public function synchroniseAccounts(){
        set_time_limit(3*3600); // 3 hours
        $continue = true;

        try{
            $accountModel = SalesforceSyncListviews::model()->findByAttributes(array(
                'idAccount' => $this->account->id_account,
                'type' => 'Account'
            ));

            // check if there is a record available in salesforce_sync_values
            // if so, this isn't the first job handling this data type and we can safely fetch other arrays from the database
            // if not, this is the first job and we have to run some initial actions

            $lastRecordId = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'lastRecordId');

            if($lastRecordId == false){
                $firstRun = true;

                $listViewName = $this->api->fetchListViewDevName('Account', $accountModel->listviewId);
                $listViewFilters = $this->api->fetchListViewFilters('Account', $listViewName);
                $fieldDefinitions = $this->getAllFields('Account');
                $filterString = $this->generateFilterString('Account', $listViewFilters, $fieldDefinitions);
                $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'filterString', $filterString);

                $validate = $this->validateFilterString('Account', $filterString);
                $listViewVisible = $this->api->checkListViewVisibility('Account', $listViewName);

                $errors = array();

                if($validate['success'] == false) {
                    $errors = array_merge($errors, $validate['errors']);
                    $continue = false;
                } elseif(!$listViewVisible){
                    $errors[] = 'The selected accounts listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
                    $continue = false;
                }
            } else {
                $firstRun = false;
                $filterString = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'filterString');
            }

            if($continue){
                if($firstRun){ // first run
                    $this->deactivateAllSalesforceAccounts();

                    $additionalLmsFields = $this->getAdditionalLmsFields('Account');
                    $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'additionalLmsFields', $additionalLmsFields);

                    $additionalSfFields = $this->getAdditionalSfFields($accountModel);
                    $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'additionalSfFields', $additionalSfFields);

                    $existingAccounts = $this->getExistingAccounts();
                    $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'existingAccounts', $existingAccounts);

                    $itemsCount = 0;
                } else { // subsequent runs
                    $additionalLmsFields = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'additionalLmsFields');
                    $additionalSfFields = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'additionalSfFields');
                    $existingAccounts = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'existingAccounts');

                    $itemsCount = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'itemsCount');
                    $errors = $this->getSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'errors');

                    $filterString = $this->addLastRecordIdToFilterString($filterString, $lastRecordId);
                }
                if(!$existingAccounts || ($existingAccounts == null)) $existingAccounts = array();

                $sfAccounts = $this->api->getAccounts($filterString, 1, null, $additionalLmsFields, $additionalSfFields);
                $data = $this->registerAccounts($sfAccounts['data'], $existingAccounts, $additionalLmsFields, $additionalSfFields);

                $itemsCount += $data['itemsCount'];
                $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'itemsCount', $itemsCount);

                $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'lastRecordId', $sfAccounts['lastSalesforceId']);
                $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'existingAccounts', $data['existingAccounts']);
                $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'completed', ($sfAccounts['pageCount'] == 1)?1:0);

                $errors = array_merge($errors, $data['errors']);
            }
        } catch (CException $e){
            $errors = array_merge($errors, array($e->getMessage()));
        }

        $this->saveSetting(SalesforceLmsAccount::SF_TYPE_ACCOUNT, 'errors', $errors);
    }

    /**
     * @param string $type: user|contact|account
     * @param string $key
     * @param string|array $value
     */
    public function saveSetting($type, $key, $value){
        $value = CJSON::encode($value);
        $setting = SalesforceSyncValues::model()->findByAttributes(array(
            'type' => $type,
            'key' => $key
        ));
        if(empty($setting)){
            $setting = new SalesforceSyncValues();
            $setting->type = $type;
            $setting->key = $key;
        }
        $setting->value = $value;
        $setting->save();
    }

    /**
     * @param string $type: user|contact|account
     * @param string $key
     * @return string|array|bool: returns a string or array if the value exists, returns false if no value could be found
     */
    public function getSetting($type, $key){
        $setting = SalesforceSyncValues::model()->findByAttributes(array(
            'type' => $type,
            'key' => $key
        ));
        if(!empty($setting)){
            return CJSON::decode($setting->value);
        } else {
            return false;
        }
    }

    /**
     * Will reset all force_change values in core_user with the settings for contacts or users
     * @param string $type: user|contact
     * @return bool
     */
    public function resetForceChangePasswords($type){
        $forceChangePassword = Yii::app()->db->createCommand()
            ->select('force_change_password')
            ->from(SalesforceSyncListviews::model()->tableName())
            ->where('idAccount = :lmsAccount AND type = :type', array(
                ':lmsAccount' => $this->account->id_account,
                ':type' => ucfirst($type)
            ))
            ->queryScalar();
        $forceChangePassword = intval($forceChangePassword);

        $updated = Yii::app()->db->createCommand("
            UPDATE ".CoreUser::model()->tableName()." AS cu
            INNER JOIN ".SalesforceUser::model()->tableName()." AS su ON cu.idst = su.id_user_lms
            SET cu.force_change = '".$forceChangePassword."'
            WHERE cu.lastenter IS NULL AND su.sf_type = '".$type."'
        ")->execute();
        Yii::log($updated.' Salesforce '.$type.'s got their force_change_password reset to '.$forceChangePassword.'.');
    }

    public function addLastRecordIdToFilterString($filterString, $lastRecord){
        if(!empty($filterString)){
            return $filterString." AND Id > '".$lastRecord."'";
        } else {
            return "Id > '".$lastRecord."'";
        }
    }

    /**
     * @param string $type user|contact
     */
    public function deactivateAllSalesforceBranches($type){
        Yii::app()->db->createCommand()
            ->update(SalesforceOrgchart::model()->tableName(), array(
                'active' => 0
            ),'sf_type = :sfType AND sf_lms_id = :lmsAccount AND is_root = :isRoot', array(
                ':sfType' => $type,
                ':lmsAccount' => $this->account->id_account,
                ':isRoot' => 0
            ));
    }

    public function disableRootAndChildBranches($type){
        Yii::app()->db->createCommand()
            ->update(SalesforceOrgchart::model()->tableName(), array(
                'active' => 0
            ),'(sf_type = :sfType1 OR sf_type = :sfType2 ) AND sf_lms_id = :lmsAccount', array(
                ':sfType1' => $type,
                ':sfType2' => ucfirst($type),
                ':lmsAccount' => $this->account->id_account
            ));
    }

    public function deactivateAllSalesforceAccounts(){
        Yii::app()->db->createCommand()
            ->update(SalesforceAccounts::model()->tableName(), array(
                'active' => 0
            ), 'lmsAccount = :lmsAccount', array(
                ':lmsAccount' => $this->account->id_account
            ));
    }

    /**
     * Retrieves all currently existing Salesforce accounts
     * @return array
     */
    public function getExistingAccounts(){
        $result = array();
        $accounts = Yii::app()->db->createCommand()
            ->select('accountId, lastModified, lmsAdditionalFields, sfAdditionalFields')
            ->from(SalesforceAccounts::model()->tableName())
            ->where('lmsAccount = :lmsAccount', array(
                ':lmsAccount' => $this->account->id_account
            ))
            ->queryAll();
        foreach($accounts as $account){
            $result[$account['accountId']] = array(
                'lastModified' => $account['lastModified'],
                'lmsAdditionalFields' => $account['lmsAdditionalFields'],
                'sfAdditionalFields' => $account['sfAdditionalFields']
            );
        }
        return $result;
    }

    /**
     * Creates or updates the salesforce accounts in the database
     * @param array $sfAccounts: contains all the user information
     * @param array $existingAccounts: contains the necessary information needed to determine if a record should be updated or not
     * @param array $additionalLmsFields: contains the field definitions for the additional profile fields
     * @param array $additionalSfFields: contains the field definitions for the hierarchy orgchart branching
     * @return array
     */
    public function registerAccounts($sfAccounts, $existingAccounts, $additionalLmsFields, $additionalSfFields){
        set_time_limit(3*3600); // 3 hours
        $items = 0;
        $errors = array();
        $activeAccounts = array();

        $transaction = Yii::app()->db->getCurrentTransaction();
        if(!$transaction) $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach($sfAccounts as $account){
                $columns = array(
                    'accountName' => $account['Name'],
                    'parentId' => ($account['ParentId'] != false)?$account['ParentId']:null,
                    'lastModified' => $account['LastModifiedDate'],
                    'active' => 1
                );

                $lmsFields = array();
                foreach ($additionalLmsFields as $additionalLmsField) {
                    $lmsFields[$additionalLmsField] = $account[$additionalLmsField];
                }
                $columns['lmsAdditionalFields'] = json_encode($lmsFields);

                $sfFields = array();
                foreach ($additionalSfFields as $additionalSfField) {
                    $sfFields[$additionalSfField] = $account[$additionalSfField];
                }
                $columns['sfAdditionalFields'] = json_encode($sfFields);

                if(array_key_exists($account['Id'], $existingAccounts)){ // Update record
                    if($existingAccounts[$account['Id']]['lastModified'] != $columns['lastModified']
                        || $existingAccounts[$account['Id']]['lmsAdditionalFields'] != $columns['lmsAdditionalFields']
                        || $existingAccounts[$account['Id']]['sfAdditionalFields'] != $columns['sfAdditionalFields']
                    ){
                        Yii::app()->db->createCommand()
                            ->update(SalesforceAccounts::model()->tableName(),
                                $columns,
                                'lmsAccount = :lmsAccount AND accountId = :accountId',
                                array(
                                    ':lmsAccount' => $this->account->id_account,
                                    ':accountId' => $account['Id']
                                ));
                        Yii::log('Salesforce account '. $columns['accountName']. ' has been updated.');
                    }
                } else { // Insert record
                    $columns['lmsAccount'] = $this->account->id_account;
                    $columns['accountId'] = $account['Id'];
                    Yii::app()->db->createCommand()
                        ->insert(SalesforceAccounts::model()->tableName(), $columns);
                }
                $activeAccounts[] = $account['Id'];
                $items++;
            }
            $transaction->commit();
        } catch (CException $e){
            $data['errors'][] = $e->getMessage();
            $transaction->rollback();
        }

        // reactivate the active accounts
        if(!empty($activeAccounts)){
            Yii::app()->db->createCommand()
                ->update(SalesforceAccounts::model()->tableName(), array('active' => 1), array('in', 'accountId', $activeAccounts));
        }

        $data = array(
            'itemsCount' => $items,
            'errors' => $errors,
            'existingAccounts' => $existingAccounts
        );
        return $data;
    }

    /**
     * @param int $rootBranch
     * @param array $activeLanguages
     */
    public function activateAccountBasedBranches($rootBranch, $activeLanguages){
        // Fetching the existing account based branches
        $currentBranches = array();
        $branches = Yii::app()->db->createCommand("
            SELECT so.id_org, so.id_parent, so.sf_account_id, coc.translation
            FROM ".SalesforceOrgchart::model()->tableName()." AS so
            INNER JOIN ".CoreOrgChart::model()->tableName()." AS coc ON so.id_org = coc.id_dir
            WHERE so.sf_lms_id = ".$this->account->id_account." AND so.sf_type = 'contact' AND so.sf_account_id IS NOT NULL AND so.is_root = 0 AND coc.translation <> ''
            GROUP BY coc.id_dir
        ")->queryAll();
        foreach($branches as $branch){
            $currentBranches[$branch['sf_account_id']] = array(
                'id_org' => $branch['id_org'],
                'id_parent' => $branch['id_parent'],
                'account_name' => $branch['translation']
            );
        }

        // Sorting out the parent branches
        $parentBranches = Yii::app()->db->createCommand("
            SELECT accountId, accountName, NULL AS parentId
            FROM ".SalesforceAccounts::model()->tableName()."
            WHERE lmsAccount = ".$this->account->id_account." AND active = 1 AND parentId IS NULL
        ")->queryAll();
        $newBranches = $this->handleAccountBranches($parentBranches, $currentBranches, $rootBranch, $activeLanguages);
        $currentBranches = array_merge($currentBranches, $newBranches);

        // Sorting out the child branches
        $childBranches = Yii::app()->db->createCommand("
            SELECT accountId, accountName, parentId
            FROM ".SalesforceAccounts::model()->tableName()."
            WHERE lmsAccount = ".$this->account->id_account." AND active = 1 AND parentId IN(
                SELECT accountId
                FROM ".SalesforceAccounts::model()->tableName()."
                WHERE lmsAccount = ".$this->account->id_account." AND active = 1
            )
        ")->queryAll();
        $this->handleAccountBranches($childBranches, $currentBranches, $rootBranch, $activeLanguages);

        // Sorting out the orphan branches
        $orphanBranches = Yii::app()->db->createCommand("
            SELECT accountId, accountName, parentId
            FROM ".SalesforceAccounts::model()->tableName()."
            WHERE lmsAccount = ".$this->account->id_account." AND active = 1 AND parentId NOT IN(
                SELECT accountId
                FROM ".SalesforceAccounts::model()->tableName()."
                WHERE lmsAccount = ".$this->account->id_account." AND active = 1
            )
        ")->queryAll();
        $this->handleAccountBranches($orphanBranches, $currentBranches, $rootBranch, $activeLanguages);

        // Re-activating all active Salesforce branches
        Yii::app()->db->createCommand("
            UPDATE ".SalesforceOrgchart::model()->tableName()."
            SET active = 1
            WHERE sf_lms_id = '".$this->account->id_account."' AND sf_type = 'contact' AND sf_account_id IS NOT NULL AND sf_account_id IN (
                SELECT accountId
                FROM ".SalesforceAccounts::model()->tableName()."
                WHERE active = 1
            )
        ")->execute();
    }

    /**
     * Updates the Salesforce org chart root
     * @param object $listviewModel
     * @return bool
     */
    public function updateOrgChartRoot($listviewModel){
        $sfOrgchartModel = SalesforceOrgchart::model()->findByAttributes(array(
            'sf_lms_id' => $this->account->id_account,
            'sf_type' => $listviewModel->type,
            'is_root' => 1
        ));
        if(empty($sfOrgchartModel)){
            $sfOrgchartModel = new SalesforceOrgchart();
            $sfOrgchartModel->sf_lms_id   = $this->account->id_account;
            $sfOrgchartModel->sf_type     = $listviewModel->type;
            $sfOrgchartModel->is_root     = 1;
        }
        switch($listviewModel->type){
            case 'User':
                $sfOrgchartModel->id_org = $listviewModel->orgchartId;
                break;
            case 'Contact':
                $sfOrgchartModel->id_org = Yii::app()->db->createCommand()
                    ->select('orgchartId')
                    ->from(SalesforceSyncListviews::model()->tableName())
                    ->where('idAccount = :lmsAccount AND type = :type', array(
                        ':lmsAccount' => $this->account->id_account,
                        ':type' => 'Account'
                    ))
                    ->queryScalar();
                break;
        }
        $sfOrgchartModel->id_parent = Yii::app()->db->createCommand()
            ->select('idParent')
            ->from(CoreOrgChartTree::model()->tableName())
            ->where('idOrg = :idOrg', array(
                ':idOrg' => $listviewModel->orgchartId
            ))
            ->queryScalar();
        $sfOrgchartModel->active      = 1;
        return $sfOrgchartModel->save();
    }

    /**
     * Handler function for parent branches, child branches and orphan branches
     * @param array $branches
     * @param array $existingBranches
     * @param int $rootBranch
     * @param array $activeLanguages
     * @return array Updated version of $existingBranches
     */
    public function handleAccountBranches($branches, $existingBranches, $rootBranch, $activeLanguages){
        foreach($branches as $branch){
            if(!array_key_exists($branch['accountId'], $existingBranches)){ // Create branch
                if($branch['parentId'] == null || !array_key_exists($branch['parentId'], $existingBranches)){
                    $parentId = $rootBranch;
                } else {
                    $parentId = $existingBranches[$branch['parentId']]['id_org'];
                }
                $orgId = $this->createBranch(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $branch['accountName'], $parentId, $activeLanguages, $branch['accountId']);
                $existingBranches[$branch['accountId']] = array(
                    'id_org' => $orgId,
                    'id_parent' => $parentId,
                    'account_name' => $branch['accountName']
                );
            } else { // Update branch
                // Name has changed?
                if($branch['accountName'] != $existingBranches[$branch['accountId']]['account_name']){
                    Yii::app()->db->createCommand()
                        ->update(CoreOrgChart::model()->tableName(), array(
                            'translation' => $branch['accountName']
                        ), 'id_dir = :idDir', array(
                            ':idDir' => $existingBranches[$branch['accountId']]['id_org']
                        ));
                }

                // ParentId has changed?
                $oldParentId = $existingBranches[$branch['accountId']]['id_parent'];
                $newParentId = ($branch['parentId'] == null || !array_key_exists($branch['parentId'], $existingBranches))?$rootBranch:$existingBranches[$branch['parentId']]['id_org'];
                if($oldParentId != $newParentId){
                    $org = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $existingBranches[$branch['accountId']]['id_org']));
                    $newParent = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $newParentId));
                    if($org->moveAsFirst($newParent)){
                        Yii::app()->db->createCommand()
                            ->update(SalesforceOrgchart::model()->tableName(), array(
                                'id_parent' => $newParent->idOrg
                            ), 'sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
                                ':lmsId' => $this->account->id_account,
                                ':accountId' => $branch['accountId'],
                                ':type' => SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS
                            ));
                        $existingBranches[$branch['accountId']]['id_parent'] = $newParentId;
                    }
                }
            }
        }
        return $existingBranches;
    }

    /**
     * Fetches the list of requested additional fields
     * @param string $objectType User|Contact
     * @return array
     */
    public function getAdditionalLmsFields($objectType){
        $result = array();
        $fields = Yii::app()->db->createCommand("
            SELECT CONCAT(cuf.type, '|', saf.lmsFieldId) AS lmsFieldname, saf.sfObjectType, saf.sfFieldName
            FROM ".SalesforceAdditionalFields::model()->tableName()." AS saf
            INNER JOIN ".CoreUserField::model()->tableName()." AS cuf ON saf.lmsFieldId = cuf.id_field
            INNER JOIN ".CoreUserFieldTranslations::model()->tableName()." AS cuft ON cuf.id_field = cuft.id_field
            WHERE saf.lmsAccountId = ".$this->account->id_account." AND saf.sfObjectType = '".$objectType."' AND cuft.translation <> ''
            GROUP BY cuf.id_field
        ")->queryAll();
        foreach($fields as $field){
            $result[$field['lmsFieldname']] = $field['sfFieldName'];
        }
        return $result;
    }

    /**
     * Fetches the list of additional fields required for additional fields hierarchy branches (or empty array in case of single_branch of account_based)
     * @param object $listviewModel
     * @return array
     */
    public function getAdditionalSfFields($listviewModel){
        $result = array();
        if($listviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS){
            if(!empty($listviewModel->hierarchy1)){
                $result[] = $listviewModel->hierarchy1;
            }
            if(!empty($listviewModel->hierarchy2)){
                $parts = explode('|', $listviewModel->hierarchy2);
                $result[] = $parts[1];
            }
            if(!empty($listviewModel->hierarchy3)){
                $parts = explode('|', $listviewModel->hierarchy3);
                $result[] = $parts[2];
            }
        }
        return $result;
    }

    /**
     * Fetches the current branch structure, but only for additional_fields orgchart type
     * @param object $listViewModel
     * @return array
     */
    public function getAdditionalFieldBranches($listViewModel){
        if($listViewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS){
            $result = array();
            $rootSettings = Yii::app()->db->createCommand("
                SELECT lev, iLeft, iRight
                FROM ".CoreOrgChartTree::model()->tableName()."
                WHERE idOrg = ".$listViewModel->orgchartId
            )->queryRow();

            $branches = Yii::app()->db->createCommand("
                SELECT cot.idOrg, cot.lev, coc.translation
                FROM ".CoreOrgChartTree::model()->tableName()." AS cot
                INNER JOIN ".CoreOrgChart::model()->tableName()." AS coc ON cot.idOrg = coc.id_dir
                LEFT JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
                WHERE coc.translation <> '' AND cot.iLeft > ".$rootSettings['iLeft']." AND cot.iRight < ".$rootSettings['iRight']." AND so.sf_account_id IS NULL
                GROUP BY cot.idOrg
                ORDER BY cot.iLeft
            ")->queryAll();

            $startLevel = $branches[0]['lev'];
            $levels = array();
            foreach($branches as $branch){
                $levels[$branch['lev']] = $branch['translation'];
                $arLevels = array();
                for($i = $startLevel; $i <= $branch['lev']; $i++){
                    $arLevels[] = $levels[$i];
                }
                $key = implode("|", $arLevels);
                $result[$key] = $branch['idOrg'];
            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Fetches the current branch structure, but only for account_based orgchart type
     * @param object $listViewModel
     * @return array
     */
    public function getAccountBasedBranches($listViewModel){
        if($listViewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED){
            $result = array();
            $rootSettings = Yii::app()->db->createCommand("
                SELECT lev, iLeft, iRight
                FROM ".CoreOrgChartTree::model()->tableName()."
                WHERE idOrg = ".$listViewModel->orgchartId
            )->queryRow();

            $branches = Yii::app()->db->createCommand("
                SELECT cot.idOrg, so.sf_account_id
                FROM ".CoreOrgChartTree::model()->tableName()." AS cot
                INNER JOIN ".CoreOrgChart::model()->tableName()." AS coc ON cot.idOrg = coc.id_dir
                LEFT JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
                WHERE coc.translation <> '' AND cot.iLeft > ".$rootSettings['iLeft']." AND cot.iRight < ".$rootSettings['iRight']." AND so.sf_account_id IS NOT NULL
                GROUP BY cot.idOrg
                ORDER BY cot.iLeft
            ")->queryAll();

            foreach($branches as $branch){
                $result[$branch['sf_account_id']] = $branch['idOrg'];
            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     *
     * @return array
     */
    public function getAccountAdditionalFields(){
        $results = array();
        $accounts = Yii::app()->db->createCommand()
            ->select('accountId, lmsAdditionalFields, sfAdditionalFields')
            ->from(SalesforceAccounts::model()->tableName())
            ->queryAll();
        foreach($accounts as $account){
            $results[$account['accountId']] = array(
                'lmsAdditionalFields' => json_decode($account['lmsAdditionalFields'], true),
                'sfAdditionalFields' => json_decode($account['sfAdditionalFields'], true)
            );
        }
        return $results;
    }

    /**
     * Returns an associative array: 'sf_account_id' => 'orgchart_id'
     * @param string $type contact|user
     * @return array
     */
    public function getCurrentUserBranches($type){
        $result = array();
        $userBranches = Yii::app()->db->createCommand("
            SELECT distinct su.id_user_sf, so.id_org
            FROM " . SalesforceUser::model()->tableName() . " AS su
            INNER JOIN " . CoreGroupMembers::model()->tableName() . " as cgm ON su.id_user_lms = cgm.idstMember
            INNER JOIN " . CoreOrgChartTree::model()->tableName() . " AS cot ON cgm.idst = cot.idst_oc
            INNER JOIN " . SalesforceOrgchart::model()->tableName() . " AS so ON cot.idOrg = so.id_org
            WHERE su.sf_type = '" . $type . "'
        ")->queryAll();

        foreach ($userBranches as $userBranch) {
            $result[$userBranch['id_user_sf']] = $userBranch['id_org'];
        }
        return $result;
    }

	/**
     *
     * Synchronizing single type of Salesforce users(users | contacts) to LMS
     *
     * @param array $params
     * @return array items that are synchronized and errors(if any)
     */
	public function syncUsersType($params) {
        // Initialising the params:
        $data = $params['data']; //array of Salesforce users
        $type = $params['type']; //Type of users(users | contacts)
        $syncType = $params['syncType']; //single_branch|additional_fields|account_based
        $activeLanguages = $params['activeLanguages']; //array with active languages in the LMS
        $additionalLmsFields = $params['additionalLmsFields']; //array of additional fields available for the user profiles
        $additionalSfFields = $params['additionalSfFields']; //additional fields array used in establishing the hierachy (if $syncType == 'additional_fields')
        $additionalFieldBranches = $params['additionalFieldBranches']; //array of the currently available additional field branches
        $accountBasedBranches = $params['accountBasedBranches']; //array of the currently available account based branches
        $rootBranch = $params['rootBranch'];
        $currentUserBranches = $params['currentUserBranches']; //associative array containing 'sfaccount_id' => 'current_branch'
        $salesforceBranches = $params['salesforceBranches']; //array of salesforceAccounts used to check if a contact(!) should be imported or not
        $accountInfo = $params['accountInfo']; //contains subarrays with the LMS & SF additional fields
        $additionalAccountLmsFields = $params['additionalAccountLmsFields']; //array of additional account fields (contacts only)
        $forceChangePassword = $params['forceChangePassword']; // 0 or 1 (used when creating new users)

        $logData = array();
        $items = 0;
        $newUsers = array();
        // Get the existing users from DB
        foreach($data as $user) {
            $userNames[] = $user['Id'];
        }

        $usersList = Yii::app()->db->createCommand()
            ->select(array("id_user_sf", "id_user_lms"))
            ->from(SalesforceUser::model()->tableName())
            ->where(array('in', 'id_user_sf', $userNames))
            ->queryAll(true);
        $existingUsers = array();
        foreach($usersList as $tmp) {
            $existingUsers[$tmp['id_user_sf']] = $tmp['id_user_lms'];
        }

        $transaction = Yii::app()->db->getCurrentTransaction();
        if(!$transaction) $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $sfUser) {
                if (empty($sfUser['Email'])) {
                    $errors = array('Missing e-mail address for user ' . $sfUser['FirstName'] . ' ' . $sfUser['LastName'] . '. Please, fix the issue and sync again.');
                    $logData = array_merge($logData, $errors);
                } elseif ($type != SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS || in_array($sfUser['AccountId'], $salesforceBranches)) { // All users as well as contacts belonging to an active
                    // Determine the branch to assign the user to
                    switch ($syncType) {
                        case 'single_branch':
                            $orgchartId = $rootBranch;
                            break;
                        case 'account_based':
                            $orgchartId = $accountBasedBranches[$sfUser['AccountId']];
                            break;
                        case 'additional_fields':
                            $orgchartId = $rootBranch;
                            $keys = array();
                            if ($type == SalesforceLmsAccount::SYNC_USER_TYPE_USERS) {
                                $keys[0] = ((count($additionalSfFields) >= 1) && (strlen($sfUser[$additionalSfFields[0]]) >= 1)) ? $sfUser[$additionalSfFields[0]] : false;
                                $keys[1] = ((count($additionalSfFields) >= 2) && (strlen($sfUser[$additionalSfFields[1]]) >= 1)) ? $sfUser[$additionalSfFields[0]] . "|" . $sfUser[$additionalSfFields[1]] : false;
                                $keys[2] = ((count($additionalSfFields) >= 3) && (strlen($sfUser[$additionalSfFields[2]]) >= 1)) ? $sfUser[$additionalSfFields[0]] . "|" . $sfUser[$additionalSfFields[1]] . "|" . $sfUser[$additionalSfFields[2]] : false;
                            } elseif ($type == SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) {
                                $keyset = $accountInfo[$sfUser['AccountId']]['sfAdditionalFields'];
                                if ((count($keyset) >= 1) && (strlen($keyset[$additionalSfFields[0]]) >= 1)) {
                                    $keys[0] = $keyset[$additionalSfFields[0]];
                                    $sfUser[$additionalSfFields[0]] = $keyset[$additionalSfFields[0]];
                                } else {
                                    $keys[0] = false;
                                }
                                if ((count($keyset) >= 2) && (strlen($keyset[$additionalSfFields[1]]) >= 1)) {
                                    $keys[1] = $keyset[$additionalSfFields[0]] . "|" . $keyset[$additionalSfFields[1]];
                                    $sfUser[$additionalSfFields[1]] = $keyset[$additionalSfFields[1]];
                                } else {
                                    $keys[1] = false;
                                }
                                if ((count($keyset) >= 3) && (strlen($keyset[$additionalSfFields[2]]) >= 1)) {
                                    $keys[2] = $keyset[$additionalSfFields[0]] . "|" . $keyset[$additionalSfFields[1]] . "|" . $keyset[$additionalSfFields[2]];
                                    $sfUser[$additionalSfFields[2]] = $keyset[$additionalSfFields[2]];
                                } else {
                                    $keys[2] = false;
                                }
                            }

                            $lastOrgChartId = $rootBranch;
                            foreach ($keys as $i => $key) {
                                if ($key != false) {
                                    if (array_key_exists($key, $additionalFieldBranches)) {
                                        $orgchartId = $additionalFieldBranches[$key];
                                        // Re-activate the branch
                                        $result = Yii::app()->db->createCommand()
                                            ->update(SalesforceOrgchart::model()->tableName(), array(
                                                'active' => 1
                                            ), 'id_org = :idOrg', array(
                                                ':idOrg' => $orgchartId
                                            ));
                                        if (!$result) { // Occurs after clearing out the config --> salesforce_org_chart is emptied, but the branches still exist
                                            $this->insertSfOrgChart($orgchartId, $lastOrgChartId, $this->account->id_account, $type);
                                        }
                                    } else {
                                        $parentId = $orgchartId;
                                        $orgchartId = $this->createBranch($type, $sfUser[$additionalSfFields[$i]], $parentId, $activeLanguages);
                                        $additionalFieldBranches[$key] = $orgchartId;
                                    }
                                }
                                $lastOrgChartId = $orgchartId;
                            }
                            break;
                    }

                    // setup array for additional profile fields
                    $additionalProfileFields = array();
                    foreach ($additionalLmsFields as $key => $field) {
                        $keyParts = explode('|', $key);
                        switch ($keyParts[0]) {
                            case "textfield":
                            case "freetext":
                                $additionalProfileFields[$keyParts[1]] = ($sfUser[$field]) ? $sfUser[$field] : '';
                                break;
                            case "yesno":
                                $additionalProfileFields[$keyParts[1]] = ($sfUser[$field] == 'true') ? 1 : 2;
                                break;
                            case "date":
                                $additionalProfileFields[$keyParts[1]] = ($sfUser[$field] != false) ? substr($sfUser[$field], 0, 10) : '';
                                break;
                        }
                    }
                    if (($type == SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) && !empty($additionalAccountLmsFields)) {
                        foreach ($additionalAccountLmsFields as $key => $field) {
                            $keyParts = explode('|', $key);
                            $value = $accountInfo[$sfUser['AccountId']]['lmsAdditionalFields'][$field];
                            switch ($keyParts[0]) {
                                case "textfield":
                                case "freetext":
                                    $additionalProfileFields[$keyParts[1]] = ($value) ? $value : '';
                                    break;
                                case "yesno":
                                    $additionalProfileFields[$keyParts[1]] = ($value == 'true') ? 1 : 2;
                                    break;
                                case "date":
                                    $additionalProfileFields[$keyParts[1]] = ($value != false) ? substr($value, 0, 10) : '';
                                    break;
                            }
                        }
                    }

                    if (isset($existingUsers[$sfUser['Id']])) {
                        $oldOrgchartId = $currentUserBranches[$sfUser['Id']];
                        $result = $this->updateUser($existingUsers[$sfUser['Id']], $sfUser, $type, $orgchartId, $oldOrgchartId);
                        $this->createOrUpdateLmsAdditionalFields($existingUsers[$sfUser['Id']], $additionalProfileFields);
                        $items += $result['itemsCount'];
                        continue;
                    }
                    // Create new CoreUser
                    if (!empty($sfUser['Username'])) { // Only Salesforce users have Username
                        $userId = $sfUser['Username'];
                    } else {
                        $userId = $sfUser['Email'];
                    }
                    $sfUsernameAlreadyExists = Yii::app()->db->createCommand()
                        ->select('COUNT(idst)')
                        ->from(CoreUser::model()->tableName())
                        ->where('userid = :userId OR email = :email', array(
                            ':userId' => Yii::app()->user->getAbsoluteUsername($userId),
                            ':email' => $sfUser['Email']
                        ))
                        ->queryScalar();

                    if ($sfUsernameAlreadyExists == 0) { // Clean addition, just insert the user
                        Yii::app()->db->createCommand("INSERT INTO `core_st` (`idst`) VALUES (NULL)")->execute();
                        $idst = Yii::app()->db->getLastInsertID();

                        Yii::app()->db->createCommand()
                            ->insert(CoreUser::model()->tableName(), array(
                                'idst' => $idst,
                                'userid' => Yii::app()->user->getAbsoluteUsername($userId),
                                'firstname' => ($sfUser['FirstName'] == null) ? '' : $sfUser['FirstName'],
                                'lastname' => ($sfUser['LastName'] == null) ? '' : $sfUser['LastName'],
                                'pass' => Yii::app()->security->hashPassword($this->generatePassword()),
                                'email' => ($sfUser['Email'] == null) ? 'no-reply@docebo.com' : $sfUser['Email'],
                                'force_change' => $forceChangePassword,
                                'register_date' => date('Y-m-d H:i:s')
                            ));

                        // Create new SalesforceUser
                        Yii::app()->db->createCommand()
                            ->insert(SalesforceUser::model()->tableName(), array(
                                'id_user_lms' => $idst,
                                'id_user_sf' => $sfUser['Id'],
                                'sf_type' => $type,
                                'sf_lms_id' => $this->account->id_account,
                                'account_id' => ($sfUser['AccountId'] == null) ? '' : $sfUser['AccountId']
                            ));

                        // Add timezone settings
                        if (!empty($sfUser['TimezoneSidKey'])) {
                            Yii::app()->db->createCommand()
                                ->insert(CoreSettingUser::model()->tableName(), array(
                                    'path_name' => 'timezone',
                                    'id_user' => $idst,
                                    'value' => $sfUser['TimezoneSidKey']
                                ));
                        }
                        $newUsers[] = $idst;
                        $result = $this->putUserInBranch($idst, $orgchartId);
                        $this->createOrUpdateLmsAdditionalFields($idst, $additionalProfileFields);
                        $items += $result['itemsCount'];
                    } else { // A user already exists with this username, now the fun begins
                        $existingInfo = $this->getExistingUserInfo(Yii::app()->user->getAbsoluteUsername($userId));
                        if (empty($existingInfo)) { // No relations to a salesforce table, so we can just allocate him to a Salesforce branch.
                            $idst = $this->getUserId($sfUser['Email']);
                            if ($idst) {
                                $result = $this->putUserInBranch($idst, $orgchartId);
                                // Add as SalesforceUser
                                Yii::app()->db->createCommand()
                                    ->insert(SalesforceUser::model()->tableName(), array(
                                        'id_user_lms' => $idst,
                                        'id_user_sf' => $sfUser['Id'],
                                        'sf_type' => $type,
                                        'sf_lms_id' => $this->account->id_account,
                                        'account_id' => ($sfUser['AccountId'] == null) ? '' : $sfUser['AccountId']
                                    ));
                                $newUsers[] = $idst;
                                $this->createOrUpdateLmsAdditionalFields($idst, $additionalProfileFields);
                                $items += $result['itemsCount'];
                            } else {
                                $errors = array('no details found for ' . $userId);
                                $logData = array_merge($logData, $errors);
                            }
                        } else {
                            $errors = array('Duplicate e-mail found. ' . $existingInfo['firstname'] . ' ' . $existingInfo['lastname'] . ' and ' . $sfUser['FirstName'] . ' ' . $sfUser['LastName'] . ' share ' . $sfUser['Email']);
                            $logData = array_merge($logData, $errors);
                        }
                    }
                }
            }
            $transaction->commit();
        } catch(CException $e){
            $data['errors'][] = $e->getMessage();
            $transaction->rollback();
        }

      //--- NOTE: since we are creating users with direct SQL queries instead of active records (due to performance reasons)
      // then no event related to users creating/updating are raised. We have to manually take care of them.
      if (!empty($newUsers)) {
          GroupAutoAssignTask::setUpdateJob($newUsers);
      }
      //---

        $result = array(
            'itemsCount' => $items,
            'errors' => $logData,
            'newUsers' => $newUsers,
            'additionalFieldBranches' => $additionalFieldBranches
        );

        return $result;
	}

    /**
     * Returns the salesforce related info for a given username
     * @param string $username (the actual username), eg: /john.doe@docebo.com
     * @return array
     */
    public function getExistingUserInfo($username){
        $result = Yii::app()->db->createCommand("
			SELECT cu.firstname, cu.lastname, cu.idst AS userId, cgm.idst AS idstGroup, cot.idOrg, so.sf_account_id, so.sf_type, su.id_user_sf
			FROM ".CoreUser::model()->tableName()." AS cu
			INNER JOIN ".CoreGroupMembers::model()->tableName()." AS cgm ON cu.idst = cgm.idstMember
			INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot ON cgm.idst = cot.idst_oc
			INNER JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
			INNER JOIN ".SalesforceUser::model()->tableName()." AS su ON cu.idst = su.id_user_lms
			WHERE cu.userid = :username AND so.sf_type IS NOT NULL
		")->queryRow(true, array(':username' => $username));
        return $result;
    }

    /**
     * @param int $userId
     * @param array $fields associative array containing the additional fields: 'id_common' => 'user_entry'
     */
    public function createOrUpdateLmsAdditionalFields($userId, $fields)
    {
        // Creating a user entry row if there isn't one
        $record = CoreUserFieldValue::model()->findByAttributes(['id_user' => $userId]);

        if ($record === null) {
            $record = new CoreUserFieldValue;
            $record->id_user = $userId;
        }

        // Updating the user values
        foreach($fields as $idCommon => $userEntry){
            $columnName = 'field_' . $idCommon;
            $record->{$columnName} = $userEntry;
        }

        $record->save();
    }

    /**
     * Fetch the userId for a given e-mail address
     * @param string $email
     * @return int
     */
    public function getUserId($email){
        $userId = Yii::app()->db->createCommand()
            ->select('idst')
            ->from(CoreUser::model()->tableName())
            ->where('email = :email', array(
                ':email' => $email
            ))
            ->order('idst ASC')
            ->limit(1)
            ->queryScalar();
        return $userId;
    }

    public function getLMSBranchIds(){
        $tmpBranches = Yii::app()->db->createCommand("SELECT id_org FROM salesforce_orgchart")->queryAll();
        $branches = array();
        foreach($tmpBranches as $branch){
            $branches[] = $branch['id_org'];
        }
        return $branches;
    }

    public function getSalesforceBranchIds(){
        $return = array();
        $salesforceBranches = Yii::app()->db->createCommand()
            ->select('accountId')
            ->from(SalesforceAccounts::model()->tableName())
            ->where('lmsAccount = :lmsAccount AND active = :active', array(
                ':lmsAccount' => $this->account->id_account,
                ':active' => 1
            ))
            ->queryAll();
        foreach($salesforceBranches as $sfBranch){
            $return[] = $sfBranch['accountId'];
        }
        return $return;
    }

    /**
     * Removing a user from a branch
     * @param int $lmsIdUser
     * @return bool
     */
    public function removeUserFromBranch($lmsIdUser){
        return Yii::app()->db->createCommand("
            DELETE ".CoreGroupMembers::model()->tableName()."
            FROM ".CoreGroupMembers::model()->tableName()."
            INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot
                ON ".CoreGroupMembers::model()->tableName().".idst = cot.idst_oc
                OR ".CoreGroupMembers::model()->tableName().".idst = cot.idst_ocd
            INNER JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
            WHERE ".CoreGroupMembers::model()->tableName().".idstMember = ".$lmsIdUser."
        ")->execute();
    }

    /**
     *
     * @param int $idstUser idst of LMS user
     * @param string $sfUser Id of Salesforce user
     * @param string $type user|contact
     * @param int $newOrgchartId orgchart to assign the user to
     * @param int $oldOrgchartId old orgchart id as a reference
     * @return array items that are synchronized and errors(if any)
     */
    private function updateUser($lmsIdUser, $sfUser, $type, $newOrgchartId, $oldOrgchartId){
        $errors = array();
        $items = 0;

        $arUpdate = array(
            'firstname' => $sfUser['FirstName'],
            'lastname' => $sfUser['LastName'],
            'email' => $sfUser['Email']
        );

        $result = Yii::app()->db->createCommand()->update(
            CoreUser::model()->tableName(),
            $arUpdate,
            'idst = :user',
            array(':user' => $lmsIdUser)
        );

        Yii::app()->db->createCommand()->update(
            SalesforceUser::model()->tableName(),
            array(
                'account_id' => ($sfUser['AccountId'] == null)?'':$sfUser['AccountId']
            ),
            'id_user_sf = :sfId',
            array(':sfId' => $sfUser['Id'])
        );

        if($result != 0 && $result < 1){
            $errors = array_merge($errors, array($result));
        }

        if($newOrgchartId != $oldOrgchartId){
            // only remove user from old branch if it exists...
            if($oldOrgchartId) $this->removeUserFromBranch($lmsIdUser);

            $result = $this->putUserInBranch($lmsIdUser, $newOrgchartId);
            $items += $result['itemsCount'];
            if(!empty($result['errors'])){
                $errors = array_merge($errors, $result['errors']);
            }
        }

        $data = array(
            'itemsCount' => $items,
            'errors' => $errors
        );

        return $data;
    }

    /**
     *
     * Assign user to Salesforce branch
     *
     * @param int $idUser idst of LMS user
     * @param int $sfOrgChartId idOrg where the user or contact will be assigned to
     * @return array items that are synchronized and errors(if any)
     */
    private function putUserInBranch($idUser, $sfOrgChartId){
        $item = 0;
        $errors = array();

        $result = $this->_insertInGroup($sfOrgChartId, $idUser);
        $errors = array_merge($errors, $result['errors']);
        $item += $result['itemsCount'];

        $data = array(
            'itemsCount' => $item,
            'errors' => $errors
        );

        return $data;
    }

    /**
     * Inserting user in CoreGroupMembers
     *
     * @param string $idOrg Id Org of Branch
     * @param string $idUser Id of User
     * @return array Number of created items and errors (if any)
     */
    private function _insertInGroup($idOrg, $idUser){

        $errors = array();

        $chart = CoreOrgChartTree::model()->findByPk($idOrg);

        $exists = CoreGroupMembers::model()->find('idst = :idst_oc AND idstMember = :userId', array(
            ':idst_oc' => $chart->idst_oc,
            ':userId' => $idUser
        ));

        if(!$exists){
            //OC
            $newMember = new CoreGroupMembers();
            $newMember->idst = $chart->idst_oc;
            $newMember->idstMember = $idUser;
            if(!$newMember->save()){
                $errors = array_merge($errors, $newMember->getErrors());
            }
        }

        $exists = CoreGroupMembers::model()->find('idst = :idst_ocd AND idstMember = :userId', array(
            ':idst_ocd' => $chart->idst_ocd,
            ':userId' => $idUser
        ));

        if(!$exists){
            //OCD
            $newMember = new CoreGroupMembers();
            $newMember->idst = $chart->idst_ocd;
            $newMember->idstMember = $idUser;
            if(!$newMember->save()){
                $errors = array_merge($errors, $newMember->getErrors());
            }

        }
        //Inserting in normal users group
        $groupUsers = CoreGroup::model()->find('groupid = :group', array(
            ':group' => '/framework/level/user'
        ));

        $exists = CoreGroupMembers::model()->find('idst = :usersGroup AND idstMember = :userId', array(
            ':usersGroup' => $groupUsers->idst,
            ':userId' => $idUser
        ));

        if(!$exists){
            $newMember = new CoreGroupMembers();
            $newMember->idst = $groupUsers->idst;
            $newMember->idstMember = $idUser;
            $newMember->save();
        }

        $data = array(
            'itemsCount' => (empty($errors)) ? 1 : 0,
            'errors' => $errors
        );

        return $data;
    }


    /**
     * Creates an array of imported Salesforce accounts
     *
     * @param $accountId int Salesforce LMS Account ID
     * @return array
     */
    public function getAccountIds($accountId){
        $accounts = Yii::app()->db->createCommand()
            ->select('sf_account_id')
            ->from(SalesforceOrgchart::model()->tableName())
            ->where('sf_lms_id = :lmsId AND sf_type = :sfType', array(
                ':lmsId' => $accountId,
                ':sfType' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
            ))
            ->queryAll();
        $return = array();
        foreach($accounts as $account){
            $return[] = $account['sf_account_id'];
        }
        return $return;
    }

    public function saveCourseOrPlan($courseOrPlanModel)
    {
        // 1. Delete courses or learning plans
        if ($this->account->getSelectionType() != null) {
            // Get items selection (courses and plans)
            $selectedItems = $this->account->getSelectedItems();

            if ($courseOrPlanModel instanceof LearningCourse) {
                if (array_search($courseOrPlanModel->idCourse, $selectedItems['courses']) == false) {
                    $this->deleteCourseOrPlan($courseOrPlanModel, true);
                    return;
                }
            }
            if ($courseOrPlanModel instanceof LearningCoursepath) {
                if (array_search($courseOrPlanModel->id_path, $selectedItems['plans']) == false) {
                    $this->deleteCourseOrPlan($courseOrPlanModel, true);
                    return;
                }
            }
        }

        // 2. Create courses or learning plans
        if ($courseOrPlanModel instanceof LearningCourse) { // Courses
            // Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
            if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
                $this->api->createOrUpdateProducts(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
            }

            // Replicate (create/update)
            $this->api->createOrUpdateItems(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
        } elseif ($courseOrPlanModel instanceof LearningCoursepath) {
            // Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
            if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
                $this->api->createOrUpdateProducts(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
            }

            // Replicate (create/update)
            $this->api->createOrUpdateItems(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
        }

        $pbInfo = $this->api->getPriceBook(SalesforceApiClient::DOCEBO_PRICEBOOK_NAME);
        if ($pbInfo && $this->api->getStandardPriceBook()) {
            // Pricebook
            $this->api->createOrUpdateCustomPricebookEntries();
        }
    }

    public function deleteCourseOrPlan($courseOrPlanModel, $omitCourseFilterInclusionCheck = false)
    {
        if (!$omitCourseFilterInclusionCheck)
        {
            if ($this->account->getSelectionType() != null)
            {
                // Get items selection (courses and plans)
                $selectedItems = $this->account->getSelectedItems();

                if ($courseOrPlanModel instanceof LearningCourse)
                    if (array_search($courseOrPlanModel->idCourse, $selectedItems['courses']) == false) {
                        return;
                    }
                if ($courseOrPlanModel instanceof LearningCoursepath)
                    if (array_search($courseOrPlanModel->id_path, $selectedItems['plans']) == false) {
                        return;
                    }
            }
        }

        if ($courseOrPlanModel instanceof LearningCourse) {
            $deletedCourses =
                $this->api->deleteItems(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
        }
        else {
            if ($courseOrPlanModel instanceof LearningCoursepath)
                $deletedCourses =
                    $this->api->deleteItems(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
        }
    }

    public function deleteRelatedEnrollments($courseOrPlan, $type)
    {
        $courseOrPlanId =
            ($type == SalesforceApiClient::ITEM_TYPE_COURSE)
                ? $courseOrPlan->idCourse
                : $courseOrPlan->id_path;

        $sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
        $api = new SalesforceApiClient($sfLmsAccount);

        $delete = true;

        if ($type == SalesforceApiClient::ITEM_TYPE_COURSE)
        {
            $enrollments = LearningCourseuser::model()->findAllByAttributes(array(
                'idCourse' => $courseOrPlanId
            ));

            if (count($enrollments) > 0) {
                $response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, false, false, $delete);
                Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
            }
        }
        else
            if ($type == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN)
            {
                $enrollments = LearningCoursepathUser::model()->findAllByAttributes(array(
                    'id_path' => $courseOrPlanId
                ));
                if (count($enrollments) > 0) {
                    $response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, false, false, $delete);
                    Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
                }
            }
    }

    /**
     * Makes an api call to Salesforce for the first chunk and creates a new job if there's more chunks ahead
     * @param string $serializedChunks
     */
    public function updateMetadata($profiles, $fields, $index){
        $salesforceModel = SalesforceLmsAccount::getFirstAccount();
        $api = new SalesforceApiClient($salesforceModel);
        return $api->setFieldsPermissions($profiles, $fields, true, true, $index);
    }

	/**
	 * Push LMS courses into Salesforce, i.e.   LMS ==> Salesforce sync (courses and plans, if any)
	 */
	public function syncCourses() {

		// Get items selection (courses and plans)
		$selectedItems = $this->account->getSelectedItems();
		
		$itemsCount = 0;
		$errors = array();
		$itemsCount = 0;
		$created = 0;
		$failed = 0;

		// Error Check
		if (!isset($selectedItems['plans']) || !isset($selectedItems['courses'])) {
			$errors = array('Invalid selection: no plans or no courses');
		} elseif(PluginManager::isPluginActive('EcommerceApp') && ($this->account['replicate_courses_as_products'] > SalesforceLmsAccount::TYPE_REPLICATE_NONE) && !$this->api->currencyConsistencyCheck($this->account)){
            $errors = array($this->api->getErrorConsistencyCheck($this->account));
        }
		else {
		    
		    try {
		    
    		    //-- 1: COURSES
    		    // Delete what must be deleted
                $exCourses  = $this->api->getExistingItems(SalesforceApiClient::ITEM_TYPE_COURSE, true);
                $coursesForDeletion = array_diff(array_keys($exCourses), $selectedItems['courses']);
                $deletedCourses = $this->api->deleteItems($coursesForDeletion, SalesforceApiClient::ITEM_TYPE_COURSE);
    
                // Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
                if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
                    // Create Products
                    $responseProductsCourses    = $this->api->createOrUpdateProducts($selectedItems['courses'], SalesforceApiClient::ITEM_TYPE_COURSE);
                    $errors                     = array_merge($errors, $responseProductsCourses['errors']);
                }
                
                // Replicate (create/update)
                $responseCourses = $this->api->createOrUpdateItems($selectedItems['courses'], SalesforceApiClient::ITEM_TYPE_COURSE);
                $errors 	= array_merge($errors, $responseCourses['errors']);
                $itemsCount = $itemsCount + $responseCourses['itemsCount'];
                $created 	= $created + $responseCourses['created'];
                $failed 	= $failed + $responseCourses['failed'];
                
                
                //-- 2: LEARNING PLANS
                if (PluginManager::isPluginActive('CurriculaApp')) {
                    // Delete what must be deleted
                    $exPlans            = $this->api->getExistingItems(SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, true);
                    $plansForDeletion   = array_diff(array_keys($exPlans), $selectedItems['plans']);
                    $deletedPlans       = $this->api->deleteItems($plansForDeletion, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
    
                    // Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
                    if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
                        // Create Products
                        $responseProductsPlans = $this->api->createOrUpdateProducts($selectedItems['plans'], SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
                        $errors = array_merge($errors, $responseProductsPlans['errors']);
                    }
                    
                    // Replicate (create/update)
                    $responsePlans   = $this->api->createOrUpdateItems($selectedItems['plans'], SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
    			    $errors 	= array_merge($errors, $responsePlans['errors']);
                    $itemsCount = $itemsCount + $responsePlans['itemsCount'];
    			    $created 	= $created + $responsePlans['created'];
    			    $failed 	= $failed + $responsePlans['failed'];
    			    
    			}
    
			    $pbInfo = $this->api->getPriceBook(SalesforceApiClient::DOCEBO_PRICEBOOK_NAME);
			    if($pbInfo  && $this->api->getStandardPriceBook()){
    			// Pricebook
    			$responsePricebook = $this->api->createOrUpdateCustomPricebookEntries();
    			$errors = array_merge($errors, $responsePricebook['errors']);
			    }
			
                Yii::log("Courses synchronised to Salesforce.");
			
			
		    }
		    catch (Exception $e) {
		        // Lets do a bit more logging here as SF API is really hard to debug
		        Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
		        Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
		        $errors = array_merge($errors, array($e->getMessage()));
		    }
				
		}
		
		
		
		$updated = $itemsCount - $created;
		
		$data = array(
			'itemsCount' 	=> $itemsCount,
			'errors' 		=> $errors,
			'created'		=> $created,
			'updated'		=> $updated,
			'failed'		=> $failed, 
		);
		
		return $data;
		
	}

    public function createCourseCompletionField(){
        set_time_limit(500);
        $salesforceModel = SalesforceLmsAccount::getFirstAccount();
        $api = new SalesforceApiClient($salesforceModel);
        if($api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)){
            $description = $api->connection->describeSObject(SalesforceApiClient::CO_COURSE_ENROLLMENT);
            $courseCompletionExists = false;
            foreach($description->fields as $field){
                if($field->name == 'PATH_COMPLETION__c'){
                    $courseCompletionExists = true;
                }
            }
            if(!$courseCompletionExists){
                $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Path completion');
            }
        }
    }

    /**
     * Returns an associative array with available additional fields in the LMS: $idField => $translation
     * @return array
     */
    public function getLmsAdditionalFields(){
        $lmsFields = array();
        $lmsFields[0] = Yii::t('salesforce', 'Select LMS field...');
        $fields = Yii::app()->db->createCommand("
            SELECT `cuf`.`id_field`, `cuf`.`type`, `cuft`.`translation`
            FROM `" . CoreUserField::model()->tableName() . "` `cuf`
            INNER JOIN `" . CoreUserFieldTranslations::model()->tableName() . "` `cuft` ON `cuf`.`id_field` = `cuft`.`id_field`
            WHERE `cuft`.`translation` <> '' AND `cuft`.`translation` IS NOT NULL AND `cuf`.`type` IN ('textfield', 'yesno', 'date', 'freetext')
            GROUP BY id_field
        ")->queryAll();

        foreach($fields as $field){
            $lmsFields[$field['type']."|".$field['id_field']] = $field['translation'];
        }

        return $lmsFields;
    }

    /**
     * Returns an array with the existing applied additional fields for the active SF installation
     * @param string $sobjectType
     * @return array
     */
    public function getSfLmsAdditionalFields($sobjectType){
        $fields = Yii::app()->db->createCommand("
            SELECT sfFieldType, sfFieldName, sfFieldLabel, lmsFieldId
            FROM ".SalesforceAdditionalFields::model()->tableName()."
            WHERE lmsAccountId = ".$this->account->id_account." AND sfObjectType = '".$sobjectType."'
            ORDER BY id ASC
        ")->queryAll();
        return $fields;
    }

    /**
     * Checks if the configuration is complete for users
     * @param string $sobjectType
     * @return bool
     */
	public function checkUserConfigurationSettings(){
        $listModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $this->account->id_account,
            'type' => 'User'
        ));

        // Check 1: does the record exist?
        if(empty($listModel)) return false;

        // Check 2: are listviewId, orgchartType & orgchartId set?
        if(empty($listModel->listviewId) || empty($listModel->orgchartType) || empty($listModel->orgchartId)) return false;

        // Check 3: in case of using additional fields hierarchy, is at least hierarchy1 set?
        if(($listModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS) && empty($listModel->hierarchy1)) return false;

        // If nothing of the previous fails, return true
        return true;
    }

    public function checkContactConfigurationSettings(){
        $accountModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $this->account->id_account,
            'type' => 'Account'
        ));
        $contactModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $this->account->id_account,
            'type' => 'Contact'
        ));

        // Check 1: do the models exist?
        if(empty($accountModel) || empty($contactModel)) return false;

        // Check 2: are listviewId's, orgchartType & orgchartId set?
        if(empty($accountModel->listviewId) || empty($contactModel->listviewId) || empty($accountModel->orgchartType) || empty($accountModel->orgchartId)) return false;

        // Check 3: in case of using additional fields hierarchy, is at least hierarchy1 set?
        if(($accountModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS) && empty($accountModel->hierarchy1)) return false;

        // If nothing of the previous fails, return true
        return true;
    }

    public function checkCourseConfigurationSettings(){
        // Check 1: Is selected_items not null?
        if(!$this->account->selected_items) return false;

        // Check 2: Do we have at least 1 course ready for sync?
        $selectedItems = $this->account->getSelectedItems();
        $selectedCounter = count($selectedItems['courses']) + count($selectedItems['plans']);
        if($selectedCounter == 0) return false;

        // If nothing of the previous fails, return true
        return true;
    }

    /**
     * Monitoring function to check memory usage in log
     */
    public function logMemoryUsage($start, $end){
        $maxMemory = ini_get('memory_limit') * 1024 * 1024;
        $usedMemory = memory_get_usage();
        $timeDiff = $end - $start;
        Yii::log("Memory used: ".$usedMemory." / ".$maxMemory." bytes (".round(($usedMemory/$maxMemory)*100, 2)."%). Script executed in ".round($timeDiff, 2)." seconds.");
    }
}