<?php
//
//class SalesforceSyncAgent extends CComponent {
//
//	/**
//	 * @var SalesforceLmsAccount
//	 */
//	private $account;
//
//	/**
//	 * @var SalesforceApiClient
//	 */
//	private $api;
//
//	/**
//	 * Constructor
//	 *
//	 * @param integer $idAccount  Salesforce account ID (in the LMS!)
//	 */
//	public function __construct($idAccount = null) {
//
//
//		if (!$idAccount) {
//			$this->account = SalesforceLmsAccount::getFirstAccount();
//		} else{
//			$this->account 		= SalesforceLmsAccount::model()->findByPk($idAccount);
//		}
//		$this->api 	= new SalesforceApiClient($this->account);
//
//	}
//
//	/**
//	 * Generating random password for the user
//	 *
//	 * @return string random password
//	 */
//	private function generatePassword(){
//		$randomPassword = chr(mt_rand(65, 90))
//			. chr(mt_rand(97, 122))
//			. mt_rand(0, 9)
//			. chr(mt_rand(97, 122))
//			. chr(mt_rand(97, 122))
//			. chr(mt_rand(97, 122))
//			. mt_rand(0, 9)
//			. chr(mt_rand(97, 122));
//
//		return $randomPassword;
//	}
//
//	/**
//	 * Generating Username, by incrementing the last username came from Salesforce.
//	 *
//	 * @return string Username
//	 */
//	private function generateUsername($blacklistedNames, $lastUserId){
//		while(in_array($lastUserId, $blacklistedNames)){
//			$lastUserId++;
//		}
//		return "d".str_pad($lastUserId, 6, '0', STR_PAD_LEFT);
//	}
//
//	/**
//	 *
//	 * Creating SalesforceOrgChart object
//	 *
//	 * @param type $idOrg ID of OrgChartTree
//	 * @param type $idParent ID of Parent element
//	 * @param type $sfLmsAccount Salesforce account ID in LMS
//	 * @param type $type users | contact | leads | account
//	 * @param type $accountId Id of Salesforce Account
//	 * @return boolean Successfull saved
//	 */
//	private function insertSfOrgChart($idOrg, $idParent, $sfLmsAccount, $type,  $accountId = null){
//		$insert = Yii::app()->db->createCommand()
//			->insert(SalesforceOrgchart::model()->tableName(), array(
//				'id_org'        => $idOrg,
//				'id_parent'     => $idParent,
//				'sf_lms_id'     => $sfLmsAccount,
//				'sf_type'       => $type,
//				'sf_account_id' => $accountId
//			));
//		if($insert){
//			return Yii::app()->db->getLastInsertID();
//		} else {
//			return false;
//		}
//	}
//
//
//	/**
//	 * Replicate Branches from Salesforce Users | Contacts | Leads | Accounts to LMS.
//	 * It depends of what is choosen to be replicated
//	 *
//	 * @return boolean Success
//	 */
//	public function structureReplication() {
//		set_time_limit(1500);
//		$userReplications = $this->account->getUserReplications();
//
//		$dataInfo = array();
//
//		if($userReplications === null){
//			return true;
//		}
//
//		$activeLanguages = $this->getActiveLanguages();
//
//		if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $userReplications) && SalesforceOrgchart::isStructured($this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_USERS) == false){
//			$orgId = SalesforceLmsAccount::createSalesforceOrgChart("Users", $activeLanguages, $this->account->id_org);
//			if($this->insertSfOrgChart($orgId, $this->account->id_org, $this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_USERS)){
//				$dataInfo['users'] = 'Users Branch' . ' ' . Yii::t('salesforce', 'created');
//			}
//		}
//
//		if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $userReplications) && SalesforceOrgchart::isStructured($this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) == false){
//			$orgId = SalesforceLmsAccount::createSalesforceOrgChart("Contacts", $activeLanguages, $this->account->id_org);
//			if($this->insertSfOrgChart($orgId, $this->account->id_org, $this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS)){
//				$dataInfo['contacts'] = 'Contacts Branch' . ' ' . Yii::t('salesforce', 'created');
//			}
//		}
//
//		if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, $userReplications) && SalesforceOrgchart::isStructured($this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_LEADS) == false){
//			$orgId = SalesforceLmsAccount::createSalesforceOrgChart("Leads", $activeLanguages, $this->account->id_org);
//			if($this->insertSfOrgChart($orgId, $this->account->id_org, $this->account->id_account, SalesforceLmsAccount::SYNC_USER_TYPE_LEADS)){
//				$dataInfo['leads'] = 'Leads Branch' . ' ' . Yii::t('salesforce', 'created');
//			}
//		}
//
//		//Creating or Updating product Family
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//
//			Yii::log('Creating Product Family');
//			$result = $this->api->updateProduct2FamilyPicklist();
//			Yii::log(var_export($result, true));
//
//			if($result->result->success == false){
//				// return false;
//			} else{
//				$dataInfo['ProductFamily'] = 'Product Family' . ' ' . Yii::t('salesforce', 'created');
//			}
//		}
//
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
//			//Creating or Updating Order Type
//			Yii::log('Creating Order type');
//			$result = $this->api->updateOrderTypePicklist();
//			Yii::log(var_export($result, true));
//			if($result->result->success == false){
//				return false;
//			} else{
//				$dataInfo['OrderType'] = 'Order Type' . ' ' . Yii::t('salesforce', 'created');
//			}
//		}
//		// Creating Custom Objects
//		// COURSE/PLAN
//		if(!$this->api->customObjectExists(SalesforceApiClient::CO_COURSE)){
//
//			$addProductField = $this->addProductField($this->account->replicate_courses_as_products);
//			Yii::log('Creating Courses Custom Object');
//			$result = $this->api->createCourseCustomObject($addProductField);
//			/**
//			 * replicate_courses_highest_history represents the highest setting the LMS account has ever had for replicate_courses_as_products
//			 * if value = 0 --> Configuration never had product replication active
//			 * if value = 1 --> Currently or previously, product replication has been active, but not Sales Orders
//			 * if value = 2 --> Currently or previously, product replication and Sales Orders have been active
//			 * Based on this value, we're able to determine whether or not to add extra fields to the Custom Objects in Salesforce
//			 */
//			Yii::app()->db->createCommand()
//				->update(SalesforceLmsAccount::model()->tableName(), array(
//					'replicate_courses_highest_history' => $this->account->replicate_courses_as_products
//				), 'id_account = :idAccount', array(
//					':idAccount' => $this->account->id_account
//				));
//
//			Yii::log(var_export($result, true));
//			if($result->result->success == false){
//				return false;
//			} else{
//				$dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'created');
//			}
//		} elseif(($this->account->replicate_courses_as_products > $this->account->replicate_courses_highest_history)
//			&&
//			$this->account->replicate_courses_highest_history == SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
//
//			$result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE, SalesforceApiClient::$COURSE_CUSTOM_OBJECT_FIELDS, 'Product');
//
//			Yii::log(var_export($result, true));
//			if($result->result->success == true){
//				$dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'updated');
//			}
//		}
//
//		// ENROLLMENTS
//		if(!$this->api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)){
//			$addOrderField = false;
//			if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
//				$addOrderField = true;
//			}
//
//			$addProductField = $this->addProductField($this->account->replicate_courses_as_products);
//
//			Yii::log('Creating Enrollments Custom Object');
//			$result = $this->api->createEnrollmentCustomObject($addProductField, $addOrderField);
//			Yii::log(var_export($result, true));
//			if($result->result->success == false){
//				return false;
//			} else{
//				$dataInfo['CO2'] = 'Course Custom Object' . ' ' . Yii::t('salesforce', 'created');
//			}
//		} elseif($this->account->replicate_courses_as_products > $this->account->replicate_courses_highest_history) {
//			if($this->account->replicate_courses_as_products >= SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
//				$result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Product');
//			}
//			if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
//				// todo: create Order field in Custom Object 2
//				$result = $this->api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Order');
//			}
//			if($result->result->success == true){
//				$dataInfo['CO2'] = 'Enrollment Custom Object' . ' ' . Yii::t('salesforce', 'updated');
//			}
//
//			Yii::app()->db->createCommand()
//				->update(SalesforceLmsAccount::model()->tableName(), array(
//					'replicate_courses_highest_history' => $this->account->replicate_courses_as_products
//				), 'id_account = :idAccount', array(
//					':idAccount' => $this->account->id_account
//				));
//		}
//
//
//
//
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//			// Create Pricebook
//			Yii::log('Creating Pricebook');
//			$result = $this->api->createPriceBook();
//			Yii::log(var_export($result, true));
//			if ($result === false) return false;
//			$dataInfo['Pricebook'] = 'Pricebook' . ' ' . Yii::t('salesforce', 'created');
//		}
//
//
//		$addDmyContact = false;
//		$dummyStuff = 'Dummy account' . ' ' . Yii::t('salesforce', 'created');
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES){
//			$addDmyContact = true;
//			$dummyStuff =  'Dummy account & contract' . ' ' . Yii::t('salesforce', 'created');
//		}
//
//		$addPriceBook = false;
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//			$addPriceBook = true;
//		}
//
//		// Create Dummy Account & Contract
//		if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, $userReplications)) {
//			Yii::log('Creating Dummy Account & Contract');
//			$result = $this->api->createDummyAccountAndContract($addDmyContact, $addPriceBook);
//			Yii::log(var_export($result, true));
//			if ($result === false) return false;
//			$dataInfo['DummyStuff'] = $dummyStuff;
//		}
//
//		return $dataInfo;
//
//	}
//
//
//	/**
//	 * Saynchronizing Account from Salesforce to LMS.
//	 *
//	 * TODO: unlinking
//	 */
//	public function syncAccountsStructure() {
//		set_time_limit(3*3600); // 3 hours
//		$data = array(
//			'errors' => array(),
//			'accounts' => array()
//		);
//		$itemsCount = 0;
//
//		try{
//			$listViewId = Yii::app()->db->createCommand()
//				->select('listviewId')
//				->from(SalesforceSyncListviews::model()->tableName())
//				->where('idAccount = :idAccount AND type = :type', array(
//					':idAccount' => $this->account->id_account,
//					':type' => 'Account'
//				))
//				->queryScalar();
//
//			//Yii::log("**** start memory usage: ".round((memory_get_usage()/1048576), 2)."MB");
//			$listViewName = $this->api->fetchListViewDevName('Account', $listViewId);
//			$listViewFilters = $this->api->fetchListViewFilters('Account', $listViewName);
//			$fieldDefinitions = $this->getAllFields('Account');
//			$parentFilterString = $this->generateFilterString('Account', $listViewFilters, $fieldDefinitions, "ParentId = ''");
//			$childFilterString = $this->generateFilterString('Account', $listViewFilters, $fieldDefinitions, "ParentId <> ''");
//
//			$userReplications = $this->account->getUserReplications();
//
//			$validate = $this->validateFilterString('Account', $parentFilterString);
//			$listViewVisible = $this->api->checkListViewVisibility('Account', $listViewName);
//			if($validate['success'] == false) {
//				$data['errors'] = array_merge($data['errors'], $validate['errors']);
//			} elseif(!$listViewVisible){
//				$data['errors'][] = 'The selected accounts listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
//			} else {
//				// First: set all records in salesforce_orgchart to active (we'll deactivate the trash in final step
//				Yii::app()->db->createCommand('UPDATE '.SalesforceOrgchart::model()->tableName().' SET active = 1')->execute();
//
//				// If leads are being synced, add a check if the dummy account has been synced already (required for lead conversion)
//				if(in_array(SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, $userReplications)) {
//					$dummyAccountSynced = false;
//				} else {
//					$dummyAccountSynced = true;
//				}
//
//				//Get the Contacts Org Id
//				$contactOrgId = Yii::app()->db->createCommand()
//					->select('id_org')
//					->from(SalesforceOrgchart::model()->tableName())
//					->where('sf_type = :type', array(
//						':type' => SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS
//					))
//					->queryScalar();
//
//				$sfAccountsRoots = $this->api->getAccounts($parentFilterString, 1, null);
//				$dataRoots = $this->syncAccounts($sfAccountsRoots['data'], null, $contactOrgId);
//				//Yii::log("**** memory usage after parents 1: ".round((memory_get_usage()/1048576), 2)."MB");
//				if (!$dummyAccountSynced) $dummyAccountSynced = $this->hasDummyAccountBeenLooped($sfAccountsRoots['data']);
//				$data = CMap::mergeArray($data, $dataRoots);
//				$itemsCount += $dataRoots['itemsCount'];
//				$countRoots = $sfAccountsRoots['pageCount'];
//				for ($i = 2; $i <= $countRoots; $i++) {
//					$lastSalesforceId = $sfAccountsRoots['lastSalesforceId'];
//					$sfAccountsRoots = $this->api->getAccounts($parentFilterString, $i, $lastSalesforceId);
//					$dataRoots = $this->syncAccounts($sfAccountsRoots['data'], null, $contactOrgId);
//					//Yii::log("**** memory usage after parents ".$i.": ".round((memory_get_usage()/1048576), 2)."MB");
//					if (!$dummyAccountSynced) $dummyAccountSynced = $this->hasDummyAccountBeenLooped($sfAccountsRoots['data']);
//					$itemsCount += $dataRoots['itemsCount'];
//					$data['errors'] = array_merge($data['errors'], $dataRoots['errors']);
//					$data['accounts'] = array_merge($data['accounts'], $dataRoots['accounts']);
//				}
//
//				$sfAccountsChild = $this->api->getAccounts($childFilterString, 1, null);
//				$dataChilds = $this->syncAccounts(null, $sfAccountsChild['data'], $contactOrgId);
//				//Yii::log("**** memory usage after children 1: ".round((memory_get_usage()/1048576), 2)."MB");
//				if (!$dummyAccountSynced) $dummyAccountSynced = $this->hasDummyAccountBeenLooped($sfAccountsChild['data']);
//				$data = CMap::mergeArray($data, $dataChilds);
//				$itemsCount += $dataChilds['itemsCount'];
//				$countChilds = $sfAccountsChild['pageCount'];
//				for ($i = 2; $i <= $countChilds; $i++) {
//					$lastSalesforceId = $sfAccountsChild['lastSalesforceId'];
//					$sfAccountsChild = $this->api->getAccounts($childFilterString, $i, $lastSalesforceId);
//					$dataChilds = $this->syncAccounts(null, $sfAccountsChild['data'], $contactOrgId);
//					//Yii::log("**** memory usage after children ".$i.": ".round((memory_get_usage()/1048576), 2)."MB");
//					if (!$dummyAccountSynced) $dummyAccountSynced = $this->hasDummyAccountBeenLooped($sfAccountsChild['data']);
//					$itemsCount += $dataChilds['itemsCount'];
//					$data['errors'] = array_merge($data['errors'], $dataChilds['errors']);
//					$data['accounts'] = array_merge($data['accounts'], $dataChilds['accounts']);
//				}
//
//				// Adding dummy account
//				if (!$dummyAccountSynced) {
//					$sfAccountsDummy = $this->api->getAccounts("Name = '" . SalesforceApiClient::DUMMY_ACCOUNT . "'", 1, null);
//					$dataDummy = $this->syncAccounts($sfAccountsDummy['data'], null, $contactOrgId);
//					//Yii::log("**** memory usage after dummy account: ".memory_get_usage()." / ".memory_get_usage(true));
//					$data = CMap::mergeArray($data, $dataDummy);
//					$itemsCount += $dataDummy['itemsCount'];
//					$data['errors'] = array_merge($data['errors'], $dataDummy['errors']);
//					$data['accounts'] = array_merge($data['accounts'], $dataDummy['accounts']);
//				}
//			}
//
//		} catch (CException $e){
//			$data['errors'][] = $e->getMessage();
//		}
//		$this->unlinkAccounts($data['accounts']);
//
//		$data = array(
//			'itemsCount' => $itemsCount,
//			'errors' => $data['errors']
//		);
//		return $data;
//	}
//
//	/**
//	 * Creates the condition to fetch records from Salesforce
//	 *
//	 * @param $listViewType string Account|User|Contact|Lead
//	 * @param $listViewFilter object
//	 * @param $fieldDefinitions array
//	 * @param array $additionalParams array
//	 * @return null|string
//	 */
//	public function generateFilterString($listViewType, $listViewFilter, $fieldDefinitions, $additionalParams = array()){
//		$filters = array();
//		if(!is_array($additionalParams)) $additionalParams = array($additionalParams);
//		if($listViewFilter['filters']){
//			if(is_array($listViewFilter['filters'])){
//				foreach($listViewFilter['filters'] as $filter){
//					$newFilter = $this->markupSoqlCondition($filter, $listViewType, $fieldDefinitions);
//					if($newFilter) $filters[] = $newFilter;
//				}
//			} else {
//				$newFilter = $this->markupSoqlCondition($listViewFilter['filters'], $listViewType, $fieldDefinitions);
//				if($newFilter) $filters[] = $newFilter;
//			}
//		}
//		if(!empty($filters)) {
//			if($listViewFilter['booleanFilter'] !== false){ // in case custom filter logic is used in listview filters
//				$filterContainer = $this->prepareFilterLogic($listViewFilter['booleanFilter']);
//				foreach($filters as $i => $filter){
//					$filterContainer = strtoupper(str_replace("::".($i+1)."::", $filter, $filterContainer));
//				}
//				$filters = array("(".$filterContainer.")");
//			}
//			if(!empty($additionalParams)) $filters = array_merge($additionalParams, $filters);
//			return implode(" AND ", $filters);
//		} else {
//			return null;
//		}
//	}
//
//	/**
//	 * Fetches the custom fields amongst the regular fields in an object
//	 * @param $sobjectType string
//	 * @return array
//	 */
//	public function getAllFields($sobjectType){
//		$fieldDefinitions = array();
//		$describeObj = $this->api->connection->describeSObject($sobjectType);
//		if($describeObj){
//			foreach($describeObj->fields as $field){
//				$fieldDefinitions[$field->name] = $field->type;
//			}
//		}
//		return $fieldDefinitions;
//	}
//
//	/**
//	 * Bloody awful method to translate readMetadata stuff to create a query
//	 * Please don't shoot me for this, shoot the people that created Salesforce... and their families... and their dog... the goldfish may live
//	 *
//	 * @param $obj object Contains field, operator & value
//	 * @param $listViewType string Account|User|Contact|Lead
//	 * @param $fieldDefinitions array
//	 * @return string
//	 */
//	public function markupSoqlCondition($obj, $listViewType, $fieldDefinitions){
//		$isDateField = false;
//		$isBoolField = false;
//		$isUserTypeField = false;
//		$isCustomField = false;
//
//		// Handling field
//		if(strpos($obj->field, "__c") !== false){ // Custom fields
//			$field = $obj->field;
//			$isCustomField = true;
//			$compareField = $obj->field;
//		} else { // Default fields
//			// First: convert some frequently used strings to their query counterparts
//			switch($listViewType){
//				case "Account":
//					$extendFind = array('ADDRESS1_', 'ADDRESS2_', 'CORE.USERS.ALIAS', 'CORE.USERS.FIRST_NAME', 'CORE.USERS.LAST_NAME', 'CORE.USERS');
//					$extendReplace = array('BILLING_', 'SHIPPING_', 'OWNER.ALIAS', 'OWNER.FIRST_NAME', 'OWNER.LAST_NAME', 'USER');
//					break;
//				case "User":
//					$extendFind = array('CORE.USERS', 'USER.ADDRESS_', 'CORE.USER_ROLE', 'CORE.PROFILE', 'SALES.CONTACT.NAME');
//					$extendReplace = array('USER', 'USER.', 'USER_ROLE', 'PROFILE', 'CONTACT.NAME');
//					break;
//				case "Contact":
//					$extendFind = array('ADDRESS1_', 'ADDRESS2_', 'CORE.USERS.');
//					$extendReplace = array('OTHER_', 'MAILING_', 'OWNER.');
//					break;
//				case "Lead":
//					$extendFind = array('CORE.USERS.');
//					$extendReplace = array('OWNER.');
//					break;
//			}
//			$defaultFind = array('_ZIP', '.ZIP', '.PHONE1', '.PHONE2', '.PHONE3', '.PHONE4', '.PHONE5', '.PHONE6', '.CELL', '.URL', '.EMPLOYEES', 'DANDB_COMPANY', 'JIGSAW_KEY', 'DBA_NAME', '.SALES', '.TICKER', 'CREATEDBY_USER.', 'UPDATEDBY_USER.');
//			$defaultReplace = array('_POSTAL_CODE', '.POSTAL_CODE', '.PHONE', '.FAX', '.MOBILE_PHONE', '.HOME_PHONE', '.OTHER_PHONE', '.ASSISTANT_PHONE', '.MOBILE_PHONE', '.WEBSITE', '.NUMBER_OF_EMPLOYEES', 'DANDB_COMPANY_ID', 'JIGSAW', 'TRADESTYLE', '.ANNUAL_REVENUE', '.TICKER_SYMBOL', 'CREATED_BY.', 'LAST_MODIFIED_BY.');
//
//			$find = array_merge($extendFind, $defaultFind);
//			$replace = array_merge($extendReplace, $defaultReplace);
//			$objField = str_ireplace($find, $replace, $obj->field);
//			switch($objField){
//				case "FULL_NAME":
//					$field = "Name";
//					break;
//				case "ACCOUNT.ACCOUNT_ID":
//					$field = "Account.Id";
//					break;
//				case "PARENT.NAME":
//				case "PARENT_NAME":
//					$field = "Account.Parent.Name";
//					break;
//				case "CREATED_BY_NAME":
//					$field = "CreatedBy.Name";
//					break;
//				case "CREATED_BY_ALIAS":
//				case "CREATEDBY_USER.ALIAS":
//					$field = "CreatedBy.Alias";
//					break;
//				case "LAST_UPDATE_BY_ALIAS":
//				case "UPDATEDBY_USER.ALIAS":
//					$field = "LastModifiedBy.Alias";
//					break;
//				case "ACCOUNT.LAST_ACTIVITY":
//					$field = "Account.LastActivityDate";
//					$isDateField = 'long';
//					break;
//				case "CONTACT.LAST_ACTIVITY":
//				case "LEAD.LAST_ACTIVITY":
//					$field = "LastActivityDate";
//					$isDateField = 'short';
//					break;
//				case "ACCOUNT.CREATED_DATE":
//				case "CONTACT.CREATED_DATE":
//				case "LEAD.CREATED_DATE":
//				case "CREATED_DATE":
//					$field = "CreatedDate";
//					$isDateField = 'long';
//					break;
//				case "ACCOUNT.LAST_UPDATE":
//				case "CONTACT.LAST_UPDATE":
//				case "LEAD.LAST_UPDATE":
//				case "LAST_UPDATE":
//					$field = "LastModifiedDate";
//					$isDateField = 'long';
//					break;
//				case "USER.LAST_LOGIN":
//					$field = "User.LastLoginDate";
//					$isDateField = 'long';
//					break;
//				case "CONTACT.BIRTHDATE":
//					$field = "Contact.Birthdate";
//					$isDateField = 'short';
//					break;
//				case "CONTACT.LAST_CU_REQUEST_DATE":
//					$field = "LastCURequestDate";
//					$isDateField = 'long';
//					break;
//				case "CONTACT.LAST_CU_UPDATE_DATE":
//					$field = "LastCUUpdateDate";
//					$isDateField = 'long';
//					break;
//				case "CONTACT.EMAIL_BOUNCED_DATE":
//				case "LEAD.EMAIL_BOUNCED_DATE":
//					$field = "EmailBouncedDate";
//					$isDateField = 'long';
//					break;
//				case "USER.ACTIVE":
//					$field = "User.IsActive";
//					$isBoolField = true;
//					break;
//				case "USER.INFO_EMAILS":
//					$field = "User.ReceivesInfoEmails";
//					$isBoolField = true;
//					break;
//				case "USER.INFO_EMAILS_ADMIN":
//					$field = "User.ReceivesAdminInfoEmails";
//					$isBoolField = true;
//					break;
//				case "MARKETING_USER":
//				case "OFFLINE_USER":
//				case "MOBILE_USER":
//				case "JIGSAW_PROSPECTING_USER":
//				case "SITEFORCE_CONTRIBUTOR_USER":
//				case "SITEFORCE_PUBLISHER_USER":
//				case "KNOWLEDGE_USER":
//					$field = "UserPermissions".$this->convertUppercaseToCamelcase($objField);
//					$isBoolField = true;
//					break;
//				case "SFCONTENT_USER":
//					$field = "UserPermissionsSFContentUser";
//					$isBoolField = true;
//					break;
//				case "USER.FORECAST_ENABLED":
//					$field = "User.ForecastEnabled";
//					$isBoolField = true;
//					break;
//				case "IS_EMAIL_ADDRESS_BOUNCED":
//					if($listViewType == "Lead") {
//						return false;
//					} else {
//						$field = "IsEmailBounced";
//						$isBoolField = true;
//					}
//					break;
//				case "PROFILE.USERTYPE":
//					$field = "Profile.UserType";
//					$isUserTypeField = true;
//					break;
//				case "MANAGER":
//					$field = "Manager.Name";
//					break;
//				case "LEAD.UNREAD":
//					$field = "Lead.IsUnreadByOwner";
//					$isBoolField = true;
//					break;
//				case "LEAD.CONVERTED":
//					$field = "Lead.IsConverted";
//					$isBoolField = true;
//					break;
//				case "a.BillingAddress":
//				case "a.ShippingAddress":
//				case "c.MailingAddress":
//				case "u.Address":
//				case "c.OtherAddress":
//				case "l.Address":
//					$field = substr($objField, strrpos($objField, ".")+1);
//					break;
//				case "LANGUAGE": //Todo: Need to check out Language for User
//				case "LOCALE": //Todo: Need to check out Locale for User
//				case "TIMEZONE": //Todo: Need to check out Timezone for User
//				case "EMAIL_ENCODING":
//				case "SU_ACCESS_EXPIRATION":
//				case "SU_ORG_ADMIN_EXPIRATION":
//				case "IS_FROZEN":
//				case "STORAGE_USED":
//				case "DELEGATED_APPROVER":
//				case "START_OF_DAY":
//				case "END_OF_DAY":
//				case "PASSWORD_EXPIRES":
//				case "CLEAN_STATUS":
//				case "CAMPAIGN_MEMBER.STATUS":
//				case "Topics":
//					return false;
//					break;
//				default: // just convert to ucwords() eg. ACCOUNT.TYPE --> Account.Type
//					$field = $this->convertUppercaseToCamelcase($objField);
//					break;
//			}
//
//			if(strpos($field, ".") !== false){
//				$compareField = substr($field, strrpos($field, '.') + 1);
//			} else {
//				$compareField = $field;
//			}
//		}
//
//		// Handling operator and value init
//		$valuePrefix = "'";
//		$valueSuffix = "'";
//		$filterPrefix = "";
//		switch($obj->operation){
//			case "equals":
//				$operator = "=";
//				break;
//			case "notEqual":
//				$operator = "<>";
//				break;
//			case "contains":
//				$operator = "LIKE";
//				$valuePrefix = "'%";
//				$valueSuffix = "%'";
//				break;
//			case "notContain":
//				$operator = "LIKE";
//				$valuePrefix = "'%";
//				$valueSuffix = "%')";
//				$filterPrefix = "(NOT ";
//				break;
//			case "startsWith":
//				$operator = "LIKE";
//				$valueSuffix = "%'";
//				break;
//			case "lessThan":
//				$operator = "<";
//				break;
//			case "lessOrEqual":
//				$operator = "<=";
//				break;
//			case "greaterThan":
//				$operator = ">";
//				break;
//			case "greaterOrEqual":
//				$operator = ">=";
//				break;
//			case "within":
//				$distanceParts = explode(":", $obj->value);
//				return "DISTANCE(".$field.", GEOLOCATION(".$distanceParts[0].",".$distanceParts[1]."), '".$distanceParts[3]."') < ".$distanceParts[2]."";
//				break;
//		}
//
//		// Handling numeric search values
//		$arNumericOperators = array('equals', 'notEqual', 'lessThan', 'lessOrEqual', 'greaterThan', 'greaterOrEqual');
//		$arListOperators = array('equals', 'notEqual', 'includes', 'excludes');
//		$arStringFields = array('string', 'phone', 'address', 'textarea', 'url');
//		if(in_array($obj->operation, $arNumericOperators) && $isDateField) { // Handling dates
//			if(empty($obj->value)){ // date = null breaks the function --> so just skip it
//				return $this->_handleDate("NULL", $isDateField, $field, $obj->operation);
//			} else {
//				return $this->_handleDate($obj->value, $isDateField, $field, $obj->operation);
//			}
//		} elseif($isCustomField && in_array($fieldDefinitions[$obj->field], array('date', 'datetime'))){ // Handling dates for custom fields
//			switch($fieldDefinitions[$obj->field]){
//				case "date":
//					$isDateField = "short";
//					break;
//				case "datetime":
//					$isDateField = "long";
//					break;
//			}
//			if(!empty($obj->value)){
//				return $this->_handleDate($obj->value, $isDateField, $field, $obj->operation);
//			} else {
//				return $this->_handleDate("NULL", $isDateField, $field, $obj->operation);
//			}
//		} elseif(in_array($obj->operation, $arNumericOperators) && $isBoolField) { // Handling booleans
//			$value = $this->_handleBoolean($obj->value);
//		} elseif($isCustomField && $fieldDefinitions[$obj->field] == 'boolean') { // Handling booleans for custom fields
//			$value = $this->_handleBoolean($obj->value);
//		} elseif(isset($fieldDefinitions[$compareField]) && in_array($fieldDefinitions[$compareField], $arStringFields)){  // Extra string handling (may fuck up default handling in case of int values)
//			$value = $valuePrefix.$obj->value.$valueSuffix;
//		} elseif(in_array($obj->operation, $arNumericOperators) && $isUserTypeField) { // Handling UserTypes
//			$value = $valuePrefix.$this->_handleUserType($obj->value).$valueSuffix;
//		} elseif(in_array($obj->operation, $arNumericOperators) && (is_numeric($obj->value) || is_real($obj->value))) { // Handling numeric search values
//			if (!empty($obj->value)) {
//				$value = $obj->value;
//			} else {
//				$value = "NULL";
//			}
//		} elseif($isCustomField && !isset($obj->value)){ // Handling custom datatypes set to NULL
//			$value = 'NULL';
//		} elseif(in_array($obj->operation,$arListOperators ) && (strpos($obj->value, ",") !== false)) { // Handling Lists()
//			$values = explode(",", $obj->value);
//			$value = "";
//			foreach($values as $v){
//				if(is_numeric($v) || is_real($v)){ // Numeric values
//					$value .= ",".$v;
//				} else { // String values
//					$value .= ",'".$v."'";
//				}
//			}
//			$value = "(".substr($value, 1).")";
//			switch($obj->operation){
//				case "equals":
//					$operator = "IN";
//					break;
//				case "notEqual":
//					$operator = "IN";
//					$filterPrefix = "(NOT ";
//					$value .= ")";
//					break;
//				case "includes":
//					$operator = "INCLUDES";
//					break;
//				case "excludes":
//					$operator = "EXCLUDES";
//			}
//		} else { // Handling non-numeric values
//			$value = $valuePrefix.$obj->value.$valueSuffix;
//		}
//		return $filterPrefix.$field." ".$operator." ".$value;
//	}
//
//	/**
//	 * Handling boolean values for SF filters
//	 * @param $value mixed
//	 * @return string
//	 */
//	protected function _handleBoolean($value){
//		switch ($value) {
//			case "0":
//			case 0:
//			case "false":
//			case false:
//				return "false";
//				break;
//			default:
//				return "true";
//		}
//	}
//
//	/**
//	 * Handling user_type for SF filters
//	 * @param $value mixed
//	 * @return string
//	 */
//	protected function _handleUserType($value){
//		$findReplace = array(
//			'S' => 'Standard',
//			'P' => 'Partner',
//			'C' => 'CustomerPortalManager',
//			'c' => 'CustomerPortalUser',
//			'G' => 'Guest',
//			'b' => 'HighVolumePortal',
//			'n' => 'CsnOnly',
//			'F' => 'Self Service'
//		);
//		if (array_key_exists($value, $findReplace)) {
//			return $findReplace[$value];
//		} else {
//			return $value;
//		}
//	}
//
//	/**
//	 * Handling date types for SF filters
//	 * @param $value mixed
//	 * @param $type string long|short
//	 * @param $field string
//	 * @param $operator string
//	 * @return string
//	 */
//	protected function _handleDate($value, $type, $field, $operator){
//		if($value != "NULL") {
//			$dateUnformatted = $value;
//			if (strpos($dateUnformatted, ' ') !== false) $dateUnformatted = trim(substr($dateUnformatted, 0, strpos($dateUnformatted, ' ')));
//			$d = explode("/", $dateUnformatted);
//			$minDateTime = $d[2] . '-' . str_pad($d[0], 2, '0', STR_PAD_LEFT) . '-' . str_pad($d[1], 2, '0', STR_PAD_LEFT);
//			$maxDateTime = $d[2] . '-' . str_pad($d[0], 2, '0', STR_PAD_LEFT) . '-' . str_pad($d[1], 2, '0', STR_PAD_LEFT);
//			if ($type == "long") {
//				$minDateTime .= "T00:00:00.000Z";
//				$maxDateTime .= "T23:59:59.000Z";
//			}
//			switch ($operator) {
//				case "equals":
//					return $field . " > " . $minDateTime . " AND " . $field . " < " . $maxDateTime;
//					break;
//				case "notEqual":
//					return $field . " < " . $minDateTime . " AND " . $field . " > " . $maxDateTime;
//					break;
//				case "lessThan":
//					return $field . " < " . $minDateTime;
//					break;
//				case "lessOrEqual":
//					return $field . " < " . $maxDateTime;
//					break;
//				case "greaterThan":
//					return $field . " > " . $maxDateTime;
//					break;
//				case "greaterOrEqual":
//					return $field . " > " . $minDateTime;
//					break;
//			}
//		} else {
//			switch ($operator) {
//				case "equals":
//					return $field . " = NULL";
//					break;
//				case "notEqual":
//					return $field . " <> NULL";
//					break;
//				case "lessThan":
//					return $field . " < NULL";
//					break;
//				case "lessOrEqual":
//					return $field . " < NULL";
//					break;
//				case "greaterThan":
//					return $field . " > NULL";
//					break;
//				case "greaterOrEqual":
//					return $field . " > NULL";
//					break;
//			}
//		}
//	}
//
//	public function validateFilterString($objType, $filter){
//		$errors = array();
//		$data = array();
//		$success = false;
//		try{
//			if($filter){
//				$filter = " WHERE ".$filter;
//			}
//			$query = "SELECT Name FROM ".$objType.$filter." LIMIT 1";
//			$this->api->connection->query($query);
//			$success = true;
//		} catch(Exception $e){
//			$errors = array_merge($errors, array($e->getMessage()));
//		}
//		$data['errors'] = $errors;
//		$data['success'] = $success;
//		return $data;
//	}
//
//	/**
//	 * Returns a count of the number of items in a selected listview
//	 * @param string $objType
//	 * @param string $filter
//	 * @return array
//	 */
//	public function countItemsInListview($objType, $filter = ""){
//		$errors = array();
//		$data = array();
//		$numRecords = false;
//		try{
//			if($filter){
//				$filter = " WHERE ".$filter;
//			}
//			$query = "SELECT COUNT() FROM ".$objType.$filter;
//			$response = $this->api->connection->query($query);
//			$queryResult = new QueryResult($response);
//			$numRecords = $queryResult->size;
//		} catch(Exception $e){
//			$errors = array_merge($errors, array($e->getMessage()));
//		}
//		$data['errors'] = $errors;
//		$data['records'] = $numRecords;
//		return $data;
//	}
//
//	/**
//	 * Prepares a given filter replacement string to be easily overridden.
//	 * eg: 3 AND (1 OR 2) --> ::3:: AND (::1:: OR ::2::)
//	 * @param string $filter
//	 * @return string
//	 */
//	public function prepareFilterLogic($filter){
//		$pattern = '/(\d+)/';
//		$replace = '::${1}::';
//		return preg_replace($pattern, $replace, $filter);
//	}
//
//	/**
//	 * Converts the fieldname FROM readMetadata calls to what's needed in a query
//	 * eg: ACCOUNT.FIRST_NAME --> Account.FirstName
//	 *
//	 * @param $fieldname string
//	 * @return string
//	 */
//	public function convertUppercaseToCamelcase($fieldname){
//		$arSymbolsFrom = array(".", "_");
//		$arSymbolsTo = array(" . ", " _ ");
//		return str_replace("_", "", str_replace($arSymbolsTo, $arSymbolsFrom, ucwords(strtolower(str_replace($arSymbolsFrom, $arSymbolsTo, $fieldname)))));
//	}
//
//	/**
//	 * Checks if the Dummy account was listed among the fetched accounts (useful to make sure it's not synced twice)
//	 *
//	 * @param $sfAccounts array
//	 * @return bool
//	 */
//	public function hasDummyAccountBeenLooped($sfAccounts){
//		foreach($sfAccounts as $account){
//			if($account['Name'] == SalesforceApiClient::DUMMY_ACCOUNT) return true;
//		}
//		return false;
//	}
//
//	public function addProductField($structure_replication){
//		if($structure_replication == SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//			return false;
//		} else {
//			return true;
//		}
//	}
//
//
//	/**
//	 * Create the Branch stuchture of the Salesforce Accounts
//	 *
//	 * @param array $accounts Salesforce Accounts
//	 * @param string $idParent Parent element( if "null" parent is Root Node)
//	 * @param int $contactOrgId Contact Orgchart id (for orphan child accounts)
//	 * @return array Number of created elements and errors (if any)
//	 */
//	private function createAccountStructure($accounts, $idParent = null, $contactOrgId){
//		set_time_limit(3*3600); // 3 hours
//		$errors = array();
//		$items = 0;
//		$alreadyInLMS = array();
//
//		$activeLanguages = $this->getActiveLanguages();
//
//		foreach ($accounts as $account){
//			$sfOrgChart = Yii::app()->db->createCommand()
//				->select('id, id_parent, id_org, sf_account_id, sf_contract_id')
//				->from(SalesforceOrgchart::model()->tableName())
//				->where('sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
//					':lmsId' => $this->account->id_account,
//					':accountId' => $account['Id'],
//					':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//				))
//				->queryRow();
//
//			if(!$sfOrgChart){
//				$parentId = $idParent;
//
//				if(!$parentId){
//					$parentId = Yii::app()->db->createCommand()
//						->select('id_org')
//						->from(SalesforceOrgchart::model()->tableName())
//						->where('sf_lms_id = :lmsId AND sf_account_id = :parentId AND sf_type = :type', array(
//							':lmsId' => $this->account->id_account,
//							':parentId' => $account['ParentId'],
//							':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//						))
//						->queryScalar();
//				}
//
//				// If there's still no parentId set, it's because this account is an orphaned child account
//				// We'll move this to the main contact branch
//				if(!$parentId){
//					$parentId = $contactOrgId;
//				}
//				//Create the root in CoreOrgChartTree and CoreOrgChart
//				$orgId = SalesforceLmsAccount::createSalesforceOrgChart($account['Name'], $activeLanguages, $parentId);
//
//				//Insert in SalesforceOrgchart
//				$sfAccountId = $this->insertSfOrgChart($orgId, $parentId, $this->account->id_account, SalesforceLmsAccount::SF_TYPE_ACCOUNT, $account['Id']);
//				if(!$sfAccountId){
//					$errors[] = "Cannot create Branch";
//				} else{
//					$items++;
//					$alreadyInLMS[] = $sfAccountId;
//				}
//			}
//			else{
//				$parentId = Yii::app()->db->createCommand()
//					->select('id_org')
//					->from(SalesforceOrgchart::model()->tableName())
//					->where('sf_lms_id = :lmsId AND sf_account_id = :parentId AND sf_type = :type', array(
//						':lmsId' => $this->account->id_account,
//						':parentId' => $account['ParentId'],
//						':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//					))
//					->queryScalar();
//
//
//				if($parentId && ($parentId != SalesforceOrgchart::getSfBranchIdOrg(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) && $parentId != $sfOrgChart['id_parent'])){
//
//					$org = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $sfOrgChart['id_org']));
//					$newParent = CoreOrgChartTree::model()->findByAttributes(array('idOrg' => $parentId));
//					if($org->moveAsFirst($newParent)){
//						Yii::app()->db->createCommand()
//							->update(SalesforceOrgchart::model()->tableName(), array(
//								'id_parent' => $newParent->idOrg
//							), 'sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
//								':lmsId' => $this->account->id_account,
//								':accountId' => $account['Id'],
//								':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//							));
//
//						$items++;
//						$alreadyInLMS[] = $sfOrgChart['sf_account_id'];
//					}
//				}
//
//				//Checking if the Name is the same as our branch
//				$existingTranslation = Yii::app()->db->createCommand()
//					->select('translation')
//					->from(CoreOrgChart::model()->tableName())
//					->where('id_dir = :idDir AND lang_code = :langCode', array(
//						':idDir' => $sfOrgChart['id_org'],
//						':langCode' => 'english'
//					))
//					->queryScalar();
//
//				if($existingTranslation != $account['Name']){
//					Yii::app()->db->createCommand()
//						->update(CoreOrgChart::model()->tableName(), array(
//							'translation' => $account['Name']
//						), 'id_dir = :idDir', array(
//							':idDir' => $sfOrgChart['id_org']
//						));
//					$items++;
//				}
//
//				$alreadyInLMS[] = $sfOrgChart['id'];
//			}
//
//			//If we create a brand new account, now we need to get it from the DB
//			if(!$sfOrgChart){
//				$sfOrgChart = Yii::app()->db->createCommand()
//					->select('sf_contract_id')
//					->from(SalesforceOrgchart::model()->tableName())
//					->where('sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
//						':lmsId' => $this->account->id_account,
//						':accountId' => $account['Id'],
//						':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//					))
//					->queryRow();
//			}
//
//			//Only if SO is enabled!!
//			if($this->account->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS){
//				if($sfOrgChart && empty($sfOrgChart['sf_contract_id'])){
//
//					//Get the ONLY ONE contract with the specific conditions
//					//@see SalesforceApiClient::getValidAccountContract
//					$contract = $this->api->getValidAccountContract($account['Id']);
//					$contractId = null;
//
//					if(!$contract){
//						//Create new Contract
//						$response = $this->api->createAccountContract($account['Id']);
//						$contractId = $response[0]->id;
//					}
//
//					//Save the contract ID in SalesforceOrgchart
//					$result = Yii::app()->db->createCommand()
//						->update(SalesforceOrgchart::model()->tableName(), array(
//							'sf_contract_id' => $contractId
//						), 'sf_lms_id = :lmsId AND sf_account_id = :accountId AND sf_type = :type', array(
//							':lmsId' => $this->account->id_account,
//							':accountId' => $account['Id'],
//							':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//						));
//				}
//			}
//
//		}
//
//		$data = array(
//			'errors' => $errors,
//			'items' => $items,
//			'alreadyInLMS' => $alreadyInLMS
//		);
//
//		return $data;
//
//	}
//
//	public function getActiveLanguages(){ // The non-AR way to fetch the languages
//		$langs = Yii::app()->db->createCommand()
//			->select('lang_code')
//			->from(CoreLangLanguage::model()->tableName())
//			->where('lang_active = :langActive', array(
//				':langActive' => 1
//			))
//			->order('lang_code ASC')
//			->queryAll();
//		$return = array();
//		foreach($langs as $lang){
//			$return[] = $lang['lang_code'];
//		}
//		return $return;
//	}
//
//	/**
//	 * Checks if the the Account is already in LMS. If not - Create it.
//	 *
//	 * @param array $sfAccountsRoots Salesorce Accounts - only root
//	 * @param array $sfAccountsChild Salesforce Accounts - only childs
//	 * @param int $contactOrgId Contact Orgchart id (for orphan child accounts)
//	 * @return array Number of created items and errors (if any)
//	 */
//	private function syncAccounts($sfAccountsRoots = null, $sfAccountsChild = null, $contactOrgId = null){
//		set_time_limit(3*3600); // 3 hours
//		$items = 0;
//		$errors = array();
//
//		//Helper array for checking if we have an account that is not in the Salesforce any more!
//		$alreadyInLMS = array();
//
//		if($sfAccountsRoots !== null){
//			$result = $this->createAccountStructure($sfAccountsRoots, $contactOrgId, null);
//			$items += $result['items'];
//			$errors = array_merge($errors, $result['errors']);
//			$alreadyInLMS = array_merge($alreadyInLMS, $result['alreadyInLMS']);
//		}
//
//		if($sfAccountsChild !== null){
//			$result = $this->createAccountStructure($sfAccountsChild, null, $contactOrgId);
//			$items += $result['items'];
//			$errors = array_merge($errors, $result['errors']);
//			$alreadyInLMS = array_merge($alreadyInLMS, $result['alreadyInLMS']);
//		}
//
//		$data = array(
//			'itemsCount' => $items,
//			'errors' => $errors,
//			'accounts' => $alreadyInLMS
//		);
//		return $data;
//	}
//
//
//	/**
//	 * Unlinking Accounts from OrgChartTree
//	 *
//	 * Just deleting from SalesforceOrgchart
//	 *
//	 * @param array $accounts Accounts that are not any more in Salesforce
//	 * @return boolean If there is no accounts to remove - false
//	 */
//	private function unlinkAccounts($accounts){
//		if(!empty($accounts)){
//			sort($accounts);
//			$accountsChunked = array_chunk($accounts, 100);
//			$lastMaxId = 0;
//			foreach($accountsChunked as $i => $dracula){
//				if(!isset($minId)){
//					$minId = 0;
//				} else {
//					$minId = $lastMaxId + 1;
//				}
//				$numberOfItemsInChunk = count($dracula);
//				if($i < (count($accountsChunked) - 1)){
//					$maxId = $dracula[$numberOfItemsInChunk-1];
//				} else {
//					$maxId = Yii::app()->db->createCommand()
//						->select('MAX(id)')
//						->from(SalesforceOrgchart::model()->tableName())
//						->queryScalar();
//				}
//				$lastMaxId = $maxId;
//				Yii::app()->db->createCommand('UPDATE '.SalesforceOrgchart::model()->tableName().' SET active = 0 WHERE id NOT IN('.implode(',', $dracula).') AND (id BETWEEN '.$minId.' AND '.$maxId.') AND sf_type=\''.SalesforceLmsAccount::SF_TYPE_ACCOUNT.'\'')->execute();
//			}
//		}
//	}
//
//	/**
//	 *
//	 * Synchronizing Salesforce Users | Contacts | Leads to LMS
//	 *
//	 *
//	 * @return array items that are synchronized and errors (if any)
//	 */
//	public function syncUCL() {
//		set_time_limit(3*3600); // 3 hours
//		// Types to sync (depends on account setting)
//		$types = explode(',', $this->account->sync_user_types);
//		$itemsCount = 0;
//		$errors = array();
//		$newUsers = array();
//
//		// Users
//		if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $types)) {
//			$users = $this->syncSfUsers();
//			$newUsers = array_merge($newUsers, $users['newUsers']);
//			$itemsCount += $users['itemsCount'];
//			$errors = array_merge($errors, $users['errors']);
//		}
//
//		// Contacts
//		if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $types)) {
//			$contacts = $this->syncSfContacts();
//			$newUsers = array_merge($newUsers, $contacts['newUsers']);
//			$itemsCount += $contacts['itemsCount'];
//			$errors = array_merge($errors, $contacts['errors']);
//		}
//
//		// Leads
//		if (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, $types)) {
//			$leads = $this->syncSfLeads();
//			$newUsers = array_merge($newUsers, $leads['newUsers']);
//			$itemsCount += $leads['itemsCount'];
//			$errors = array_merge($errors, $leads['errors']);
//		}
//
//		$params = array(
//			'users' => $newUsers,
//			'branches' => $this->getLMSBranchIds()
//		);
//		Yii::app()->event->raise(EventManager::EVENT_MASS_IMPORT_BRANCH_USERS, new DEvent($this, $params));
//
//		$data = array(
//			'itemsCount' => $itemsCount,
//			'errors' => $errors
//		);
//		return $data;
//	}
//
//
//	/**
//	 *
//	 * Synchronizing single type of Salesforce users(users | contacts | leads) to LMS
//	 *
//	 * @param array $data array of Salesforce users
//	 * @param string $type Type of users(users | contacts | leads)
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	public function syncUsersType($data, $type) {
//
//		$logData = array();
//		$items = 0;
//		$newUsers = array();
//
//		// Get the existing users from DB
//		foreach($data as $user) {
//			$userNames[] = $user['Id'];
//		}
//
//		$usersList = Yii::app()->db->createCommand()
//			->select(array("id_user_sf", "id_user_lms"))
//			->from(SalesforceUser::model()->tableName())
//			->where(array('in', 'id_user_sf', $userNames))
//			->queryAll(true);
//		$existingUsers = array();
//		foreach($usersList as $tmp) {
//			$existingUsers[$tmp['id_user_sf']] = $tmp['id_user_lms'];
//		}
//
//		$salesforceBranches = $this->getSalesforceBranchIds();
//		foreach($data as $sfUser) {
//			if(empty($sfUser['Email'])){
//				$errors = array('Missing e-mail address for user '.$sfUser['FirstName'].' '.$sfUser['LastName'].'. Please, fix the issue and sync again.');
//				$logData = array_merge($logData, $errors);
//			} elseif($type != SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS || in_array($sfUser['AccountId'], $salesforceBranches)) { // Second condition is to ensure only users belonging to an existing branch are being synced into the LMS
//				if (isset($existingUsers[$sfUser['Id']])) {
//					$result = $this->updateUser($existingUsers[$sfUser['Id']], $sfUser, $type);
//					$items += $result['itemsCount'];
//					$logData = array_merge($logData, $result['errors']);
//					continue;
//				}
//				// Create new CoreUser
//				if (!empty($sfUser['Username'])) { // Only Salesforce users have Username
//					$userId = $sfUser['Username'];
//				} else {
//					$userId = $sfUser['Email'];
//				}
//				$sfUsernameAlreadyExists = Yii::app()->db->createCommand()
//					->select('COUNT(idst)')
//					->from(CoreUser::model()->tableName())
//					->where('userid = :userId OR email = :email', array(
//						':userId' => Yii::app()->user->getAbsoluteUsername($userId),
//						':email' => $sfUser['Email']
//					))
//					->queryScalar();
//
//				if ($sfUsernameAlreadyExists == 0) { // Clean addition, just insert the user
//					Yii::app()->db->createCommand("INSERT INTO `core_st` (`idst`) VALUES (NULL)")->execute();
//					$idst = Yii::app()->db->getLastInsertID();
//
//					Yii::app()->db->createCommand()
//						->insert(CoreUser::model()->tableName(), array(
//							'idst' => $idst,
//							'userid' => Yii::app()->user->getAbsoluteUsername($userId),
//							'firstname' => ($sfUser['FirstName'] == null) ? '' : $sfUser['FirstName'],
//							'lastname' => ($sfUser['LastName'] == null) ? '' : $sfUser['LastName'],
//							'pass' => Yii::app()->security->hashPassword($this->generatePassword()),
//							'email' => ($sfUser['Email'] == null) ? 'no-reply@docebo.com' : $sfUser['Email'],
//							'force_change' => 1
//						));
//
//					// Create new SalesforceUser
//					Yii::app()->db->createCommand()
//						->insert(SalesforceUser::model()->tableName(), array(
//							'id_user_lms' => $idst,
//							'id_user_sf' => $sfUser['Id'],
//							'sf_type' => $type,
//							'sf_lms_id' => $this->account->id_account
//						));
//
//					// Add timezone settings
//					if (!empty($sfUser['TimezoneSidKey'])) {
//						Yii::app()->db->createCommand()
//							->insert(CoreSettingUser::model()->tableName(), array(
//								'path_name' => 'timezone',
//								'id_user' => $idst,
//								'value' => $sfUser['TimezoneSidKey']
//							));
//					}
//					$newUsers[] = $idst;
//					$result = $this->putUserInBranch($type, $idst, (isset($sfUser['AccountId']) ? $sfUser['AccountId'] : null));
//					$items += $result['itemsCount'];
//				} else { // A user already exists with this username, now the fun begins
//					$existingInfo = $this->getExistingUserInfo(Yii::app()->user->getAbsoluteUsername($userId));
//					if(empty($existingInfo)){ // No relations to a salesforce table, so we can just allocate him to a Salesforce branch.
//						$idst = $this->getUserId($sfUser['Email']);
//						$result = $this->putUserInBranch($type, $idst, (isset($sfUser['AccountId']) ? $sfUser['AccountId'] : null));
//						// Add as SalesforceUser
//						Yii::app()->db->createCommand()
//							->insert(SalesforceUser::model()->tableName(), array(
//								'id_user_lms' => $idst,
//								'id_user_sf' => $sfUser['Id'],
//								'sf_type' => $type,
//								'sf_lms_id' => $this->account->id_account
//							));
//						$newUsers[] = $idst;
//						$items += $result['itemsCount'];
//					} else {
//						$errors = array('Duplicate e-mail found. '.$existingInfo['firstname'].' '.$existingInfo['lastname'].' and '.$sfUser['FirstName'].' '.$sfUser['LastName'].' share '.$sfUser['Email']);
//						$logData = array_merge($logData, $errors);
//					}
//				}
//			}
//		}
//		$result = array(
//			'itemsCount' => $items,
//			'errors' => $logData,
//			'newUsers' => $newUsers
//		);
//
//		return $result;
//	}
//
//	/**
//	 * Returns the salesforce related info for a given username
//	 * @param string $username (the actual username), eg: /john.doe@docebo.com
//	 * @return array
//	 */
//	public function getExistingUserInfo($username){
//		$result = Yii::app()->db->createCommand("
//			SELECT cu.firstname, cu.lastname, cu.idst AS userId, cgm.idst AS idstGroup, cot.idOrg, so.sf_account_id, so.sf_type, su.id_user_sf
//			FROM ".CoreUser::model()->tableName()." AS cu
//			INNER JOIN ".CoreGroupMembers::model()->tableName()." AS cgm ON cu.idst = cgm.idstMember
//			INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot ON cgm.idst = cot.idst_oc
//			INNER JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
//			INNER JOIN ".SalesforceUser::model()->tableName()." AS su ON cu.idst = su.id_user_lms
//			WHERE cu.userid = '".$username."' AND so.sf_type IS NOT NULL
//		")->queryRow();
//		return $result;
//	}
//
//	/**
//	 * Fetch the userId for a given e-mail address
//	 * @param string $email
//	 * @return int
//	 */
//	public function getUserId($email){
//		$userId = Yii::app()->db->createCommand()
//			->select('idst')
//			->from(CoreUser::model()->tableName())
//			->where('email = :email', array(
//				':email' => $email
//			))
//			->order('idst ASC')
//			->limit(1)
//			->queryScalar();
//		return $userId;
//	}
//
//	public function getLMSBranchIds(){
//		$tmpBranches = Yii::app()->db->createCommand("SELECT id_org FROM salesforce_orgchart")->queryAll();
//		$branches = array();
//		foreach($tmpBranches as $branch){
//			$branches[] = $branch['id_org'];
//		}
//		return $branches;
//	}
//
//	public function getSalesforceBranchIds(){
//		$return = array();
//		$salesforceBranches = Yii::app()->db->createCommand()
//			->select('sf_account_id')
//			->from(SalesforceOrgchart::model()->tableName())
//			->where('sf_lms_id = :lmsAccount AND sf_type = :sfType', array(
//				':lmsAccount' => $this->account->id_account,
//				':sfType' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//			))
//			->queryAll();
//		foreach($salesforceBranches as $sfBranch){
//			$return[] = $sfBranch['sf_account_id'];
//		}
//		return $return;
//	}
//
//	/**
//	 * Removing user form a branch. Later he will be assigned to the same or another branch
//	 *
//	 * @param $idstUser LMS User ID
//	 * @param $sfAccountId Salesforce Account ID
//	 * @param $type Salesforce User type (user | lead | contact)
//	 *
//	 * @return bool Result of operation (true - successful, false - fail)
//	 */
//	private function _removeUserFromBranch($idstUser, $sfAccountId, $type){
//
//		if($type === SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS)
//			$type = SalesforceLmsAccount::SF_TYPE_ACCOUNT;
//
//		// Try to fetch the current account idOrg
//		$sfOrgChart = Yii::app()->db->createCommand()
//			->select(array('sf_lms_id'))
//			->from(SalesforceOrgchart::model()->tableName())
//			->where(array("AND", 'sf_account_id = :accountId', 'sf_type = :type'), array(
//				':accountId' => $sfAccountId,
//				':type' => $type
//			))
//			->queryScalar();
//
//		// If unsuccessful to fetch by AccountId, fetch by type
//		if(!$sfOrgChart){
//			$sfOrgChart = Yii::app()->db->createCommand()
//				->select(array('sf_lms_id'))
//				->from(SalesforceOrgchart::model()->tableName())
//				->where('sf_type = :type', array(':type' => $type))
//				->queryScalar();
//		}
//
//		// Fetching idOrgList for current type
//		$allAccount = SalesforceOrgchart::getAllOrgChats($sfOrgChart);
//		// Get all the org_chart's ids
//		$idstsQuery = Yii::app()->db->createCommand()
//			->select(array('idst_oc', 'idst_ocd'))
//			->from(CoreOrgChartTree::model()->tableName())
//			->where(array("in", 'idOrg', $allAccount))
//			->queryAll(true);
//
//		// Convert to return array in array of non repeating org_charts ids
//		$idsts = array();
//		foreach($idstsQuery as $orgChart){
//			$idsts[$orgChart['idst_oc']] = $orgChart['idst_oc'];
//			$idsts[$orgChart['idst_ocd']] = $orgChart['idst_ocd'];
//		}
//
//		// If array is empty --> there are no ORG charts available yet and we can't assign users to non-existing branches
//		if($idsts === array()) return false;
//
//		return Yii::app()->db->createCommand()->delete(
//			CoreGroupMembers::model()->tableName(),
//			array("and", "idstMember = :user" , array("in", "idst" , $idsts)), array(":user" => $idstUser)
//		);
//	}
//
//	/**
//	 *
//	 * @param int $idstUser idst of LMS user
//	 * @param string $sfUser Id of Salesforce user
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	private function updateUser($lmsIdUser, $sfUser, $type){
//		$errors = array();
//		$items = 0;
//
//		$arUpdate = array(
//			'firstname' => $sfUser['FirstName'],
//			'lastname' => $sfUser['LastName'],
//			'email' => $sfUser['Email']
//		);
//
//		$result = Yii::app()->db->createCommand()->update(
//			CoreUser::model()->tableName(),
//			$arUpdate,
//			'idst = :user',
//			array(':user' => $lmsIdUser)
//		);
//
//		if($result != 0 && $result < 1){
//			$errors = array_merge($errors, array($result));
//		}
//
//		if($this->_removeUserFromBranch($lmsIdUser, (isset($sfUser['AccountId']) ? $sfUser['AccountId'] : null), $type)  ){
//			$result = $this->putUserInBranch($type, $lmsIdUser, (isset($sfUser['AccountId']) ? $sfUser['AccountId'] : null));
//			$items += $result['itemsCount'];
//			if(!empty($result['errors'])){
//				$errors = array_merge($errors, $result['errors']);
//			}
//		}
//
//		$data = array(
//			'itemsCount' => $items,
//			'errors' => $errors
//		);
//
//		return $data;
//	}
//
//	/**
//	 *
//	 * Assign user to Salesforce branch
//	 *
//	 * @param string $sfType users | contacts | leads
//	 * @param int $idUser idst of LMS user
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	private function putUserInBranch($sfType, $idUser, $accountId = null){
//
//		$item = 0;
//		$errors = array();
//
//		switch($sfType){
//			case SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS:
//				$sfOrgChartId = Yii::app()->db->createCommand()
//					->select('id_org')
//					->from(SalesforceOrgchart::model()->tableName())
//					->where('sf_account_id = :accountId AND sf_type = :type', array(
//						':accountId' => $accountId,
//						':type' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//					))
//					->queryScalar();
//
//				if(!$sfOrgChartId){
//					$sfOrgChartId = Yii::app()->db->createCommand()
//						->select('id_org')
//						->from(SalesforceOrgchart::model()->tableName())
//						->where('sf_type = :type', array(':type' => $sfType))
//						->queryScalar();
//				}
//
//				$result = $this->_insertInGroup($sfOrgChartId, $idUser);
//
//				$errors = array_merge($errors, $result['errors']);
//				$item += $result['itemsCount'];
//
//				break;
//			case SalesforceLmsAccount::SYNC_USER_TYPE_USERS:
//			case SalesforceLmsAccount::SYNC_USER_TYPE_LEADS:
//			default:
//				$sfOrgChartId = Yii::app()->db->createCommand()
//					->select('id_org')
//					->from(SalesforceOrgchart::model()->tableName())
//					->where('sf_type = :type', array(':type' => $sfType))
//					->queryScalar();
//				$result = $this->_insertInGroup($sfOrgChartId, $idUser);
//
//				$errors = array_merge($errors, $result['errors']);
//				$item += $result['itemsCount'];
//				break;
//		}
//
//		$data = array(
//			'itemsCount' => $item,
//			'errors' => $errors
//		);
//
//		return $data;
//	}
//
//	/**
//	 * Inserting user in GoreGroupMemeber
//	 *
//	 * @param string $idOrg Id Org of Branch
//	 * @param string $idUser Id of User
//	 * @return array Number of created items and errors (if any)
//	 */
//	private function _insertInGroup($idOrg, $idUser){
//
//		$errors = array();
//		$item = 0;
//		$chart = Yii::app()->db->createCommand()
//			->select(array('idst_oc', 'idst_ocd'))
//			->from(CoreOrgChartTree::model()->tableName())
//			->where('idOrg = :idOrg', array(':idOrg' => $idOrg))
//			->queryRow();
//
//		$exists = Yii::app()->db->createCommand()
//			->select('COUNT(idst)')
//			->from(CoreGroupMembers::model()->tableName())
//			->where('idst = :idst_oc AND idstMember = :userId', array(
//				':idst_oc' => $chart['idst_oc'],
//				':userId' => $idUser
//			))
//			->queryScalar();
//
//		if(!$exists){
//			//OC
//			Yii::app()->db->createCommand()->insert(
//				CoreGroupMembers::model()->tableName(),
//				array(
//					'idst'       => $chart['idst_oc'],
//					'idstMember' => $idUser,
//					'filter'     => ''
//				)
//			);
//		}
//
//		$exists = Yii::app()->db->createCommand()
//			->select('COUNT(idst)')
//			->from(CoreGroupMembers::model()->tableName())
//			->where('idst = :idst_ocd AND idstMember = :userId', array(
//				':idst_ocd' => $chart['idst_ocd'],
//				':userId' => $idUser
//			))
//			->queryScalar();
//
//		if(!$exists){
//			//OCD
//			Yii::app()->db->createCommand()->insert(
//				CoreGroupMembers::model()->tableName(),
//				array(
//					'idst'       => $chart['idst_ocd'],
//					'idstMember' => $idUser,
//					'filter'     => ''
//				)
//			);
//
//		}
//		//Inserting in normal users group
//		$groupUsersIdst = Yii::app()->db->createCommand()
//			->select('idst')
//			->from(CoreGroup::model()->tableName())
//			->where('groupid = :group', array(':group' => '/framework/level/user'))
//			->queryScalar();
//
//		$exists = Yii::app()->db->createCommand()
//			->select('COUNT(idst)')
//			->from(CoreGroupMembers::model()->tableName())
//			->where('idst = :usersGroup AND idstMember = :userId', array(
//				':usersGroup' => $groupUsersIdst,
//				':userId' => $idUser
//			))
//			->queryScalar();
//
//		if(!$exists){
//			Yii::app()->db->createCommand()->insert(
//				CoreGroupMembers::model()->tableName(),
//				array(
//					'idst'       => $groupUsersIdst,
//					'idstMember' => $idUser,
//					'filter'     => ''
//				)
//			);
//			Yii::app()->db->createCommand()->insert(
//				CoreGroupMembers::model()->tableName(),
//				array(
//					'idst'       => 1,
//					'idstMember' => $idUser,
//					'filter'     => ''
//				)
//			);
//			Yii::app()->db->createCommand()->insert(
//				CoreGroupMembers::model()->tableName(),
//				array(
//					'idst'       => 2,
//					'idstMember' => $idUser,
//					'filter'     => ''
//				)
//			);
//		}
//
//		$data = array(
//			'itemsCount' => (empty($errors)) ? 1 : 0,
//			'errors' => $errors
//		);
//
//		return $data;
//	}
//
//	/**
//	 * Synchronizing Salesforce contacts to LMS users
//	 *
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	public function syncSfContacts() {
//		set_time_limit(3*3600); // 3 hours
//		$errors = array();
//		$itemsCount = 0;
//		$newUsers = array();
//
//		try {
//			$listViewId = Yii::app()->db->createCommand()
//				->select('listviewId')
//				->from(SalesforceSyncListviews::model()->tableName())
//				->where('idAccount = :idAccount AND type = :type', array(
//					':idAccount' => $this->account->id_account,
//					':type' => 'Contact'
//				))
//				->queryScalar();
//
//			$listViewName = $this->api->fetchListViewDevName('Contact', $listViewId);
//			$listViewFilters = $this->api->fetchListViewFilters('Contact', $listViewName);
//			$fieldDefinitions = $this->getAllFields('Contact');
//			$filterString = $this->generateFilterString('Contact', $listViewFilters, $fieldDefinitions);
//			$validate = $this->validateFilterString('Contact', $filterString);
//			$listViewVisible = $this->api->checkListViewVisibility('Contact', $listViewName);
//
//			if($validate['success'] == false){
//				$errors = array_merge($errors, $validate['errors']);
//			} elseif(!$listViewVisible){
//				$errors[] = 'The selected contacts listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
//			} else {
//
//				$contacts = $this->api->getContacts($filterString, 1, null);
//
//				$data = $this->syncUsersType($contacts['data'], SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
//				$newUsers = array_merge($newUsers, $data['newUsers']);
//				$itemsCount += $data['itemsCount'];
//				$errors = array_merge($errors, $data['errors']);
//
//				$pageCount = $contacts['pageCount'];
//
//				for ($i = 2; $i <= $pageCount; $i++) {
//					$lastSalesforceId = $contacts['lastSalesforceId'];
//					$contacts = $this->api->getContacts($filterString, $i, $lastSalesforceId);
//					$data = $this->syncUsersType($contacts['data'], SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
//					$newUsers = array_merge($newUsers, $data['newUsers']);
//					$itemsCount += $data['itemsCount'];
//					$errors = array_merge($errors, $data['errors']);
//				}
//
//				$this->syncAccountsStructure();
//			}
//		}
//		catch (Exception $e) {
//			// Lets do a bit more logging here as SF API is really hard to debug
//			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
//			Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
//			$errors = array_merge($errors, array($e->getMessage()));
//		}
//
//
//		$data = array(
//			'itemsCount' => $itemsCount,
//			'errors' => $errors,
//			'newUsers' => $newUsers
//		);
//		return $data;
//	}
//
//	/**
//	 * Synchronizing Salesforce Leads to LMS users
//	 *
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	public function syncSfLeads() {
//		set_time_limit(3*3600); // 3 hours
//		$errors = array();
//		$itemsCount = 0;
//		$newUsers = array();
//
//		try {
//			$listViewId = Yii::app()->db->createCommand()
//				->select('listviewId')
//				->from(SalesforceSyncListviews::model()->tableName())
//				->where('idAccount = :idAccount AND type = :type', array(
//					':idAccount' => $this->account->id_account,
//					':type' => 'Lead'
//				))
//				->queryScalar();
//
//			$listViewName = $this->api->fetchListViewDevName('Lead', $listViewId);
//			$listViewFilters = $this->api->fetchListViewFilters('Lead', $listViewName);
//			$fieldDefinitions = $this->getAllFields('Lead');
//			$filterString = $this->generateFilterString('Lead', $listViewFilters, $fieldDefinitions);
//
//			$validate = $this->validateFilterString('Lead', $filterString);
//			$listViewVisible = $this->api->checkListViewVisibility('Lead', $listViewName);
//
//			if($validate['success'] == false){
//				$errors = array_merge($errors, $validate['errors']);
//			} elseif(!$listViewVisible){
//				$errors[] = 'The selected leads listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
//			} else {
//				$leads = $this->api->getLeads($filterString, 1, null);
//				$data = $this->syncUsersType($leads['data'], SalesforceLmsAccount::SYNC_USER_TYPE_LEADS);
//				$newUsers = array_merge($newUsers, $data['newUsers']);
//				$itemsCount += $data['itemsCount'];
//				$errors = array_merge($errors, $data['errors']);
//
//				$pageCount = $leads['pageCount'];
//
//				for ($i = 2; $i <= $pageCount; $i++) {
//					$lastSalesforceId = $leads['lastSalesforceId'];
//					$leads = $this->api->getLeads($filterString, $i, $lastSalesforceId);
//					$data = $this->syncUsersType($leads['data'], SalesforceLmsAccount::SYNC_USER_TYPE_LEADS);
//					$newUsers = array_merge($newUsers, $data['newUsers']);
//					$itemsCount += $data['itemsCount'];
//					$errors = array_merge($errors, $data['errors']);
//				}
//			}
//		}
//		catch (Exception $e) {
//			// Lets do a bit more logging here as SF API is really hard to debug
//			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
//			Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
//			$errors = array_merge($errors, array($e->getMessage()));
//		}
//
//		$data = array(
//			'itemsCount' => $itemsCount,
//			'errors' => $errors,
//			'newUsers' => $newUsers
//		);
//		return $data;
//	}
//	/**
//	 * Synchronizing Salesforce Users to LMS users
//	 *
//	 * @return array items that are synchronized and errors(if any)
//	 */
//	public function syncSfUsers(){
//		set_time_limit(3*3600); // 3 hours
//		$errors = array();
//		$itemsCount = 0;
//		$newUsers = array();
//
//		try {
//			$listViewId = Yii::app()->db->createCommand()
//				->select('listviewId')
//				->from(SalesforceSyncListviews::model()->tableName())
//				->where('idAccount = :idAccount AND type = :type', array(
//					':idAccount' => $this->account->id_account,
//					':type' => 'User'
//				))
//				->queryScalar();
//
//			$listViewName = $this->api->fetchListViewDevName('User', $listViewId);
//			$listViewFilters = $this->api->fetchListViewFilters('User', $listViewName);
//			$fieldDefinitions = $this->getAllFields('User');
//			$filterString = $this->generateFilterString('User', $listViewFilters, $fieldDefinitions);
//
//			$validate = $this->validateFilterString('User', $filterString);
//			$listViewVisible = $this->api->checkListViewVisibility('User', $listViewName);
//
//			if($validate['success'] == false){
//				$errors = array_merge($errors, $validate['errors']);
//			} elseif(!$listViewVisible){
//				$errors[] = 'The selected users listview "'.$listViewName.'" could not be read. Please make it visible to all users.';
//			} else {
//				$users = $this->api->getUsers($filterString, 1, null);
//				$data = $this->syncUsersType($users['data'], SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
//				$newUsers = array_merge($newUsers, $data['newUsers']);
//				$itemsCount += $data['itemsCount'];
//				$errors = array_merge($errors, $data['errors']);
//
//				$pageCount = $users['pageCount'];
//
//				for ($i = 2; $i <= $pageCount; $i++) {
//					$lastSalesforceId = $users['lastSalesforceId'];
//					$users = $this->api->getUsers($filterString, $i, $lastSalesforceId);
//					$data = $this->syncUsersType($users['data'], SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
//					$newUsers = array_merge($newUsers, $data['newUsers']);
//					$itemsCount += $data['itemsCount'];
//					$errors = array_merge($errors, $data['errors']);
//				}
//			}
//		}
//		catch (Exception $e) {
//			// Lets do a bit more logging here as SF API is really hard to debug
//			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
//			Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
//			$errors = array_merge($errors, array($e->getMessage()));
//		}
//
//		$data = array(
//			'itemsCount' => $itemsCount,
//			'errors' => $errors,
//			'newUsers' => $newUsers
//		);
//		return $data;
//	}
//
//	/**
//	 * Creates an array of imported Salesforce accounts
//	 *
//	 * @param $accountId int Salesforce LMS Account ID
//	 * @return array
//	 */
//	public function getAccountIds($accountId){
//		$accounts = Yii::app()->db->createCommand()
//			->select('sf_account_id')
//			->from(SalesforceOrgchart::model()->tableName())
//			->where('sf_lms_id = :lmsId AND sf_type = :sfType', array(
//				':lmsId' => $accountId,
//				':sfType' => SalesforceLmsAccount::SF_TYPE_ACCOUNT
//			))
//			->queryAll();
//		$return = array();
//		foreach($accounts as $account){
//			$return[] = $account['sf_account_id'];
//		}
//		return $return;
//	}
//
//	public function saveCourseOrPlan($courseOrPlanModel)
//	{
//		if ($this->account->getSelectionType() != null)
//		{
//			// Get items selection (courses and plans)
//			$selectedItems = $this->account->getSelectedItems();
//
//			if ($courseOrPlanModel instanceof LearningCourse)
//				if (array_search($courseOrPlanModel->idCourse, $selectedItems['courses']) == false) {
//					$this->deleteCourseOrPlan($courseOrPlanModel, true);
//					return;
//				}
//			if ($courseOrPlanModel instanceof LearningCoursepath)
//				if (array_search($courseOrPlanModel->id_path, $selectedItems['plans']) == false) {
//					$this->deleteCourseOrPlan($courseOrPlanModel, true);
//					return;
//				}
//		}
//
//		if ($courseOrPlanModel instanceof LearningCourse)
//			$responseCourse =
//				$this->api->createOrUpdateItems(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
//		else
//			if ($courseOrPlanModel instanceof LearningCoursepath)
//				$responseCourse =
//					$this->api->createOrUpdateItems(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//
//		if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
//			if ($courseOrPlanModel instanceof LearningCourse)
//				$responseProductCourse =
//					$this->api->createOrUpdateProducts(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
//			else
//				if ($courseOrPlanModel instanceof LearningCoursepath)
//					$responseProductCourse =
//						$this->api->createOrUpdateProducts(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//		}
//	}
//
//	public function deleteCourseOrPlan($courseOrPlanModel, $omitCourseFilterInclusionCheck = false)
//	{
//		if (!$omitCourseFilterInclusionCheck)
//		{
//			if ($this->account->getSelectionType() != null)
//			{
//				// Get items selection (courses and plans)
//				$selectedItems = $this->account->getSelectedItems();
//
//				if ($courseOrPlanModel instanceof LearningCourse)
//					if (array_search($courseOrPlanModel->idCourse, $selectedItems['courses']) == false) {
//						return;
//					}
//				if ($courseOrPlanModel instanceof LearningCoursepath)
//					if (array_search($courseOrPlanModel->id_path, $selectedItems['plans']) == false) {
//						return;
//					}
//			}
//		}
//
//		if ($courseOrPlanModel instanceof LearningCourse) {
//			$deletedCourses =
//				$this->api->deleteItems(array($courseOrPlanModel->idCourse), SalesforceApiClient::ITEM_TYPE_COURSE);
//		}
//		else {
//			if ($courseOrPlanModel instanceof LearningCoursepath)
//				$deletedCourses =
//					$this->api->deleteItems(array($courseOrPlanModel->id_path), SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//		}
//	}
//
//	public function deleteRelatedEnrollments($courseOrPlan, $type)
//	{
//		$courseOrPlanId =
//			($type == SalesforceApiClient::ITEM_TYPE_COURSE)
//				? $courseOrPlan->idCourse
//				: $courseOrPlan->id_path;
//
//		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
//		$api = new SalesforceApiClient($sfLmsAccount);
//
//		$delete = true;
//
//		if ($type == SalesforceApiClient::ITEM_TYPE_COURSE)
//		{
//			$enrollments = LearningCourseuser::model()->findAllByAttributes(array(
//				'idCourse' => $courseOrPlanId
//			));
//
//			if (count($enrollments) > 0) {
//				$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_COURSE, false, false, $delete);
//				Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
//			}
//		}
//		else
//			if ($type == SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN)
//			{
//				$enrollments = LearningCoursepathUser::model()->findAllByAttributes(array(
//					'id_path' => $courseOrPlanId
//				));
//				if (count($enrollments) > 0) {
//					$response = $api->createOrUpdateEnrollments($enrollments, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, false, false, $delete);
//					Yii::log(var_export($response, true), CLogger::LEVEL_INFO);
//				}
//			}
//	}
//
//	/**
//	 * Push LMS courses into Salesforce, i.e.   LMS ==> Salesforce sync (courses and plans, if any)
//	 */
//	public function syncCourses() {
//
//		// Get items selection (courses and plans)
//		$selectedItems = $this->account->getSelectedItems();
//
//		$itemsCount = 0;
//		$errors = array();
//		$itemsCount = 0;
//		$created = 0;
//		$failed = 0;
//
//		// Error Check
//		if (!isset($selectedItems['plans']) || !isset($selectedItems['courses'])) {
//			$errors = array('Invalid selection: no plans or no courses');
//		} elseif(PluginManager::isPluginActive('EcommerceApp') && ($this->account['replicate_courses_as_products'] > SalesforceLmsAccount::TYPE_REPLICATE_NONE) && !$this->api->currencyConsistencyCheck($this->account)){
//			$errors = array($this->api->getErrorConsistencyCheck($this->account));
//		}
//		else {
//
//			try {
//
//				//-- 1: COURSES
//				// Delete what must be deleted
//				$exCourses  = $this->api->getExistingItems(SalesforceApiClient::ITEM_TYPE_COURSE, true);
//				$coursesForDeletion = array_diff(array_keys($exCourses), $selectedItems['courses']);
//				$deletedCourses = $this->api->deleteItems($coursesForDeletion, SalesforceApiClient::ITEM_TYPE_COURSE);
//
//				// Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
//				if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//					// Create Products
//					$responseProductsCourses    = $this->api->createOrUpdateProducts($selectedItems['courses'], SalesforceApiClient::ITEM_TYPE_COURSE);
//					$errors                     = array_merge($errors, $responseProductsCourses['errors']);
//				}
//
//				// Replicate (create/update)
//				$responseCourses = $this->api->createOrUpdateItems($selectedItems['courses'], SalesforceApiClient::ITEM_TYPE_COURSE);
//				$errors 	= array_merge($errors, $responseCourses['errors']);
//				$itemsCount = $itemsCount + $responseCourses['itemsCount'];
//				$created 	= $created + $responseCourses['created'];
//				$failed 	= $failed + $responseCourses['failed'];
//
//
//				//-- 2: LEARNING PLANS
//				if (PluginManager::isPluginActive('CurriculaApp')) {
//					// Delete what must be deleted
//					$exPlans            = $this->api->getExistingItems(SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN, true);
//					$plansForDeletion   = array_diff(array_keys($exPlans), $selectedItems['plans']);
//					$deletedPlans       = $this->api->deleteItems($plansForDeletion, SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//
//					// Create products (FIRST!) if we enabled the "replicate Docebo courses as Salesforce products"
//					if($this->account->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE){
//						// Create Products
//						$responseProductsPlans = $this->api->createOrUpdateProducts($selectedItems['plans'], SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//						$errors = array_merge($errors, $responseProductsPlans['errors']);
//					}
//
//					// Replicate (create/update)
//					$responsePlans   = $this->api->createOrUpdateItems($selectedItems['plans'], SalesforceApiClient::ITEM_TYPE_LEARNING_PLAN);
//					$errors 	= array_merge($errors, $responsePlans['errors']);
//					$itemsCount = $itemsCount + $responsePlans['itemsCount'];
//					$created 	= $created + $responsePlans['created'];
//					$failed 	= $failed + $responsePlans['failed'];
//
//				}
//
//				$pbInfo = $this->api->getPriceBook(SalesforceApiClient::DOCEBO_PRICEBOOK_NAME);
//				if($pbInfo  && $this->api->getStandardPriceBook()){
//					// Pricebook
//					$responsePricebook = $this->api->createOrUpdateCustomPricebookEntries();
//					$errors = array_merge($errors, $responsePricebook['errors']);
//				}
//
//
//
//
//			}
//			catch (Exception $e) {
//				// Lets do a bit more logging here as SF API is really hard to debug
//				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, __CLASS__);
//				Yii::log("Trace: " . $e->getTraceAsString(), CLogger::LEVEL_ERROR, __CLASS__);
//				$errors = array_merge($errors, array($e->getMessage()));
//			}
//
//		}
//
//
//
//		$updated = $itemsCount - $created;
//
//		$data = array(
//			'itemsCount' 	=> $itemsCount,
//			'errors' 		=> $errors,
//			'created'		=> $created,
//			'updated'		=> $updated,
//			'failed'		=> $failed,
//		);
//
//		return $data;
//
//	}
//
//	public function createCourseCompletionField(){
//		set_time_limit(500);
//		$salesforceModel = SalesforceLmsAccount::getFirstAccount();
//		$api = new SalesforceApiClient($salesforceModel);
//		if($api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)){
//			$description = $api->connection->describeSObject(SalesforceApiClient::CO_COURSE_ENROLLMENT);
//			$courseCompletionExists = false;
//			foreach($description->fields as $field){
//				if($field->name == 'PATH_COMPLETION__c'){
//					$courseCompletionExists = true;
//				}
//			}
//			if(!$courseCompletionExists){
//				$api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Path completion');
//			}
//		}
//	}
//
//	/**
//	 * Returns an associative array with available additional fields in the LMS: $idField => $translation
//	 * @return array
//	 */
//	public function getLmsAdditionalFields(){
//		$lmsFields = array();
//		$lmsFields[0] = Yii::t('salesforce', 'Select LMS field...');
//		$fields = Yii::app()->db->createCommand("
//            SELECT id_common, translation
//            FROM ".CoreField::model()->tableName()."
//            WHERE translation <> '' AND translation IS NOT NULL AND type_field = 'textfield'
//            GROUP BY id_common
//        ")->queryAll();
//
//		foreach($fields as $field){
//			$lmsFields[$field['id_common']] = $field['translation'];
//		}
//
//		return $lmsFields;
//	}
//
//	/**
//	 * Returns an array with the existing applied additional fields for the active SF installation
//	 * @param string $sobjectType
//	 * @return array
//	 */
//	public function getSfLmsAdditionalFields($sobjectType){
//		$fields = Yii::app()->db->createCommand("
//            SELECT sfFieldType, sfFieldName, sfFieldLabel, lmsFieldId
//            FROM ".SalesforceAdditionalFields::model()->tableName()."
//            WHERE lmsAccountId = ".$this->account->id_account." AND sfObjectType = '".$sobjectType."'
//            ORDER BY id ASC
//        ")->queryAll();
//		return $fields;
//	}
//
//	/**
//	 * Checks if the configuration is complete for users
//	 * @param string $sobjectType
//	 * @return bool
//	 */
//	public function checkUserConfigurationSettings(){
//		$listModel = SalesforceSyncListviews::model()->findByAttributes(array(
//			'idAccount' => $this->account->id_account,
//			'type' => 'User'
//		));
//
//		// Check 1: does the record exist?
//		if(empty($listModel)) return false;
//
//		// Check 2: are listviewId, orgchartType & orgchartId set?
//		if(empty($listModel->listviewId) || empty($listModel->orgchartType) || empty($listModel->orgchartId)) return false;
//
//		// Check 3: in case of using additional fields hierarchy, is at least hierarchy1 set?
//		if(($listModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS) && empty($listModel->hierarchy1)) return false;
//
//		// If nothing of the previous fails, return true
//		return true;
//	}
//
//	public function checkContactConfigurationSettings(){
//		$accountModel = SalesforceSyncListviews::model()->findByAttributes(array(
//			'idAccount' => $this->account->id_account,
//			'type' => 'Account'
//		));
//		$contactModel = SalesforceSyncListviews::model()->findByAttributes(array(
//			'idAccount' => $this->account->id_account,
//			'type' => 'Contact'
//		));
//
//		// Check 1: do the models exist?
//		if(empty($accountModel) || empty($contactModel)) return false;
//
//		// Check 2: are listviewId's, orgchartType & orgchartId set?
//		if(empty($accountModel->listviewId) || empty($contactModel->listviewId) || empty($accountModel->orgchartType) || empty($accountModel->orgchartId)) return false;
//
//		// Check 3: in case of using additional fields hierarchy, is at least hierarchy1 set?
//		if(($accountModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS) && empty($accountModel->hierarchy1)) return false;
//
//		// If nothing of the previous fails, return true
//		return true;
//	}
//}