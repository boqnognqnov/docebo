<?php

class SalesforceAppController extends Controller {

	public function beforeAction($action){
		JsTrans::addCategories('salesforce');
		return parent::beforeAction($action);
	}


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$allowedActions = array(
			'index',
			'step1',
			'AxOpen',
			'AxStep1',
			'AxStep2',
			'AxStep3',
			'AxStep4',
		);
		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/adminmanager/view'),
			'actions' => $allowedActions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {
		$settings = new SalesforceAppSettingsForm();
		$this->render('settings', array('settings' => $settings));
	}




	/**
	 * Register client resources (CSS, JS,..)
	 */
	public function registerResources()
	{
		$cs = Yii::app()->getClientScript();

		$assetsUrl = $this->module->getAssetsUrl();
		$url = Yii::app()->theme->baseUrl . '/css/DoceboPager.css';
		Yii::app()->getClientScript()->registerCssFile($url);
		$cs->registerCssFile($assetsUrl . '/css/salesforce.css');
		$cs->registerScriptFile($assetsUrl . '/js/salesforce.js');
		// $cs->registerCssFile($assetsUrl . '/js/bootstro/bootstro.css');

		$themeUrl = Yii::app()->theme->baseUrl;
		$cs->registerScriptFile($themeUrl . '/js/jwizard.js');
		$cs->registerCssFile($themeUrl . '/css/admin.css');
		$cs->registerCssFile($themeUrl . '/css/usersselector.css');
		$cs->registerScriptFile($themeUrl . '/js/usersselector.js');
	}




	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->registerResources(); // Load the client resources /* CSS, JS, IMG */

		$salesforceModel = null;
		$salesforceModelCount = SalesforceLmsAccount::model()->count();
		if($salesforceModelCount > 0){
			$salesforceModel = SalesforceLmsAccount::getFirstAccount();
		} else{
			$salesforceModel = new SalesforceLmsAccount();
		}

		//Enter here if the Synchronization frequency is changed
		if (isset($_POST['SalesforceLmsAccount'])) {
			$salesforceModel->attributes = $_POST['SalesforceLmsAccount'];

			if (!empty($salesforceModel->sync_job_hash)) {
				$job = CoreJob::model()->find('hash_id = :hash', array(':hash' => $salesforceModel->sync_job_hash));
				Yii::app()->scheduler->deleteJob($job, true);
			}

			if ($salesforceModel->validate()) {
				$params = array('sfAccountId' => $salesforceModel->id_account);
				$job = Yii::app()->scheduler->createJob(
					SalesforceAutoSync::JOB_NAME,
					SalesforceAutoSync::id(),
					CoreJob::TYPE_RANDOM,
					'hour',
					$salesforceModel->sync_frequency,
					0,  // minute is always 0
					0,
					'UTC',
					null,
					null,
					null,
					$params
				);
				$salesforceModel->sync_job_hash = $job->hash_id;
				$salesforceModel->save();
			}
		}


		$configParams = $this->_getConfigParams();
		if ($configParams['salesforceModelCount'] <= 0) {

			$showHints = array();
			$showHints[] = 'bootstroConfigureSalesforce';
			$showHints[] = 'bootstroAddBlockLauncher';
			$this->render('index', array(
				'showHints' => implode(',', $showHints)
			));

		} else {

			$this->render('config', $configParams);
		}
	}




	/**
	 * @throws CException
	 */
    public function actionAxWizardDispatcher() {

        $id = Yii::app()->request->getParam('id', null);

        $salesforceModel = null;
        if($id !== null){
            $salesforceModel = SalesforceLmsAccount::model()->findByPk($id);
        } else{
            $salesforceModel = new SalesforceLmsAccount();
        }


        if(isset($_POST['SalesforceLmsAccount'])){
            $salesforceModel->attributes = $_POST['SalesforceLmsAccount'];
            /*if(isset($_POST['SalesforceLmsAccount']['sync_user_types']) && is_array($_POST['SalesforceLmsAccount']['sync_user_types'])){

                $types = array();
                foreach ( $_POST['SalesforceLmsAccount']['sync_user_types'] as  $type ) {
                    $types[] = $type;
                }

                $salesforceModel->sync_user_types = implode(',', $types);
            }
            if(isset($_POST['also_orders']) && $salesforceModel->replicate_courses_as_products != 0){
                $salesforceModel->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS;
            }*/
        }


        if ($_POST['from_step'] == 'step1') {

            if($salesforceModel->validate()){
                $this->renderPartial('wizard/step2', array(
                    'salesforceLmsAccount' => $salesforceModel
                ));
                Yii::app()->end();
            } else{
                $this->renderPartial('wizard/step1', array(
                    'salesforceLmsAccount' => $salesforceModel,
                    'error' => $salesforceModel->getErrors()
                ));
                Yii::app()->end();
            }
        } /************************* END STEP1 ************************/

        if(($_POST['from_step'] == 'step2')){
            $sfAgent = new SalesforceApiClient($salesforceModel);



            if($sfAgent->hasError()){
                $this->renderPartial('wizard/step1', array(
                    'salesforceLmsAccount' => $salesforceModel,
                    'error' => $sfAgent->error()
                ));
                Yii::app()->end();
            } else{
                $consistency = $sfAgent->accountConsistencyCheck();

							if ($sfAgent->hasError()) {
								$this->renderPartial('wizard/step1', array(
									'salesforceLmsAccount' => $salesforceModel,
									'error' => $sfAgent->error()
								));
								Yii::app()->end();
							}
							if (!is_array($consistency)) {
								$this->renderPartial('wizard/step1', array(
									'salesforceLmsAccount' => $salesforceModel,
									'error' => Yii::t('standard', '_OPERATION_FAILURE')
								));
								Yii::app()->end();
							}


                foreach ($consistency as $perm){
                    if($perm == false){
                        $error = "You don't have enough permissions!";
                        $this->renderPartial('wizard/step1', array(
                            'salesforceLmsAccount' => $salesforceModel,
                            'error' => $error
                        ));
                        Yii::app()->end();
                    }
                    break;
                }

                if($salesforceModel->getScenario() == 'insert'){
                    $salesforceModel->setScenario('encrypt');
                }

                if($salesforceModel->save()){
                    $this->renderPartial('wizard/step5');
                } else{
                    $this->renderPartial('wizard/step1');
                }
                Yii::app()->end();
            }
        }

        // Steps 3 & 4 have been removed from the setup wizard in SalesforceApp 2.0, in stead, skip right ahead to step 5
        /*if (($_POST['from_step'] == 'step3') && $_POST['next_button']) {
            if(!isset($_POST['SalesforceLmsAccount']['sync_user_types'])){
                $salesforceModel->sync_user_types = null;
                    $error = 'error';
                    $this->renderPartial('wizard/step3', array(
                        'salesforceLmsAccount' => $salesforceModel,
                        'error'                => $error
                    ));
                    Yii::app()->end();

            }
            if($salesforceModel->validate()){
                $this->renderPartial('wizard/step4', array(
                    'salesforceLmsAccount' => $salesforceModel
                ));
            } else{
                $this->renderPartial('wizard/step3', array(
                    'salesforceLmsAccount' => $salesforceModel
                ));
            }
            Yii::app()->end();
        }
        if (($_POST['from_step'] == 'step3') && $_POST['prev_button']) {
            $this->renderPartial('wizard/step1', array(
                'salesforceLmsAccount' => $salesforceModel
            ));
            Yii::app()->end();
        }

        if (($_POST['from_step'] == 'step4') && $_POST['next_button']) {
            $SfLmsAccount = Yii::app()->request->getParam("SalesforceLmsAccount", false);
            $sfAgent = new SalesforceApiClient($salesforceModel);
            // If Docebo courses have to be replicated as Salesforce products, there a currency consistency check being made
            if(PluginManager::isPluginActive('EcommerceApp') && ($SfLmsAccount['replicate_courses_as_products'] > SalesforceLmsAccount::TYPE_REPLICATE_NONE) && !$sfAgent->currencyConsistencyCheck($salesforceModel)){
                $error = $sfAgent->getErrorConsistencyCheck($salesforceModel);
                $this->renderPartial('wizard/step1', array(
                    'salesforceLmsAccount' => $salesforceModel,
                    'error' => $error
                ));

                Yii::app()->end();
            }

            //Structure replication

            $result = array();


            if(empty($salesforceModel->id_org)){
                $sfSyncAgent = new SalesforceSyncAgent();
                $activeLanguages = $sfSyncAgent->getActiveLanguages();
                $salesforceModel->id_org = SalesforceLmsAccount::createSalesforceOrgChart("Salesforce", $activeLanguages);
            }

            if($salesforceModel->getScenario() == 'insert'){
            	$salesforceModel->setScenario('encrypt');
            }

            if($salesforceModel->save()){

            	$structureTypes = SalesforceLmsAccount::getUserTypes();
            	$structureTypes[] = SalesforceLmsAccount::SF_TYPE_ACCOUNT;
                if(SalesforceOrgchart::isStructured($salesforceModel->id_account, implode(',', $structureTypes)) == false){
                    $result = $this->initSalesforce();
                    $result['success'] = 'Salesforce ' . Yii::t('salesforce', 'configured successfully');
                }
                $this->renderPartial('wizard/step5', array(
                    'info' => $result
                ));
            } else{
                $this->renderPartial('wizard/step4');
            }
            Yii::app()->end();
        }

        if (($_POST['from_step'] == 'step4') && $_POST['prev_button']) {
            $this->renderPartial('wizard/step3', array(
                'salesforceLmsAccount' => $salesforceModel
            ));
            Yii::app()->end();
        }*/

        if(($_POST['from_step'] == 'step5') && $_POST['finnish_button']){

            echo '<a class="auto-close"></a>';

            $redirectUrl = Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index');
            // Use commonly used Dialog2/JS content to redirect
            $this->renderPartial('lms.protected.views.site._js_redirect', array(
                'url'=>$redirectUrl,
                'showMessage' => false
            ));
            Yii::app()->end();
        }
    }

    public function actionSetupUsers(){
        if(isset($_POST['cancelChanges'])){ // Cancel button clicked --> redirect to main page
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $sfLmsAccount = SalesforceLmsAccount::getFirstAccount();

        $listviewModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $sfLmsAccount->id_account,
            'type' => 'User'
        ));
        if(empty($listviewModel)){
            $listviewModel = new SalesforceSyncListviews();
            $listviewModel->idAccount = $sfLmsAccount->id_account;
            $listviewModel->type = 'User';
        }

        // If no account has been set up before, redirect to main page
        if(empty($sfLmsAccount)) {
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $api = new SalesforceApiClient($sfLmsAccount);
        $syncAgent = new SalesforceSyncAgent();

        $additionalUserFields = $syncAgent->getSfLmsAdditionalFields('User');

        $errors = array();
        // Submitting the form, checking the settings and saving to database.
        if(isset($_POST['saveChanges'])){
            // Check user list view
            $usersListviewId = Yii::app()->request->getParam('users', '');
            if(strlen($usersListviewId) == 18){
                $listviewModel->listviewId = $usersListviewId;
            } else {
                $errors[] = Yii::t('salesforce', 'Please choose a valid list view filter.');
            }

            // Set force change password setting
            $listviewModel->force_change_password = Yii::app()->request->getParam('check_force_change_password', 0);

            // Check org chart selection
            $orgchartType = Yii::app()->request->getParam('organization_chart', '');
            if(in_array($orgchartType, array(SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH, SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS))){
                $listviewModel->orgchartType = $orgchartType;
            } else {
                $errors[] = Yii::t('salesforce', 'Please select a valid organization chart type.');
            }

            // Check org chart id
            $orgchartId = intval(Yii::app()->request->getParam('idOrgChart', 0));
            if($orgchartId > 0){
                $listviewModel->orgchartId = $orgchartId;
            } else {
                $errors[] = Yii::t('salesforce', 'Please select a root branch for your organization chart.');
            }

            // Check hierarchy fields
            $hierarchy1 = Yii::app()->request->getParam('hierarchy1', '');
            $hierarchy2 = Yii::app()->request->getParam('hierarchy2', '');
            $hierarchy3 = Yii::app()->request->getParam('hierarchy3', '');
            if($orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS){
                if(strlen($hierarchy1) > 1){
                    $listviewModel->hierarchy1 = $hierarchy1;
                    $listviewModel->hierarchy2 = (strlen($hierarchy2) > 1)?$hierarchy2:'';
                    $listviewModel->hierarchy3 = (strlen($hierarchy3) > 1)?$hierarchy3:'';
                } else {
                    $errors[] = Yii::t('salesforce', 'Please select field level 1 for your organization chart.');
                }
            } else {
                $listviewModel->hierarchy1 = '';
                $listviewModel->hierarchy2 = '';
                $listviewModel->hierarchy3 = '';
            }

            // Checking additional fields
					$newAdditionalFields = array();
					if (is_array($_POST)) {
						foreach ($_POST as $key => $val) {
							if (strpos($key, 'sfUserFieldName_') !== false) { $newAdditionalFields[$key] = $val; }
						}
					}
					$additionalFieldsData = $this->processAdditionalFields($newAdditionalFields, 'user');
					$errors = array_merge($errors, $additionalFieldsData['errors']);

            $oldFields = array();
            $createFields = array();
            $updateFields = array();
            $deleteFields = array();
            if(empty($errors)){ // Only do the next step of the additional field validation in case we don't have any errors....
                if(!empty($additionalUserFields)){
                    foreach($additionalUserFields as $oldField){
                        $oldFields[$oldField['lmsFieldId']] = array(
                            'fieldname' => $oldField['sfFieldType']."|".$oldField['sfFieldName'],
                            'label' => $oldField['sfFieldLabel']
                        );
                    }
                }

                foreach($additionalFieldsData['newFields'] as $key => $newField){
                    if(array_key_exists($key, $oldFields)){
                        $updateFields[$key] = $newField;
                    } else {
                        $createFields[$key] = $newField;
                    }
                }
                foreach($oldFields as $key => $oldField){
                    if(!array_key_exists($key, $additionalFieldsData['newFields'])){
                        $deleteFields[$key] = $oldField;
                    }
                }

                // SAVING ALL CHANGES
                $listviewModel->save();
                $this->deleteFields($deleteFields, 'User', $sfLmsAccount->id_account);
                $this->updateFields($updateFields, 'User', $sfLmsAccount->id_account);
                $this->createFields($createFields, 'User', $sfLmsAccount->id_account);

                header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
                exit;
            }
            $additionalUserFields = $additionalFieldsData['updatedAdditionalFields'];
        }

        $userOptions = $api->generateListviewDropdownList('User');
        $listUsers = array();
        $listUsersJs = array();
        foreach($userOptions as $option){
            $listUsers[$option['listviewId']] = $option['label'];
            $listUsersJs[$option['listviewId']] = $option['items'];
        }

        $picklistStructure = $api->getPicklistStructure('User');
        $hierarchySelects = $this->getHierarchySelects($picklistStructure, $listviewModel);

        if(!empty($listviewModel)){
            $selectedUserListview = $listviewModel->listviewId;
        } else {
            $selectedUserListview = false;
        }

        $filteredSalesforceUserFields = $api->getSalesforceFieldsList('User');

        $alreadyAssignedFields = array();
        $alreadyAssignedLmsFields = array('User' => array(), 'Account' => array(), 'Contact' => array());
        $alreadyModels = SalesforceAdditionalFields::model()->findAll();
        if (is_array($alreadyModels)) {
            foreach ($alreadyModels as $alreadyModel) {
                /* @var $alreadyModel SalesforceAdditionalFields */
                if ($alreadyModel->sfObjectType == 'User') {
                    $_sfIndex = $alreadyModel->sfFieldName;
                    $_lmsIndex = $alreadyModel->lmsFieldId;
                    $alreadyAssignedFields[$_sfIndex] = $_lmsIndex;
                }
                //collect LMS field IDs
                $_lmsFieldId = 0;
                if (is_numeric($alreadyModel->lmsFieldId)) {
                    $_lmsFieldId = $alreadyModel->lmsFieldId;
                } elseif (is_string($alreadyModel->lmsFieldId) && strpos($alreadyModel->lmsFieldId, '|') !== false) {
                    $_arr = explode('|', $alreadyModel->lmsFieldId);
                    $_lmsFieldId = $_arr[1];
                }
                if ($_lmsFieldId > 0) {
                    $alreadyAssignedLmsFields[$alreadyModel->sfObjectType][] = $_lmsFieldId;
                }
            }
        }

        $this->render('setupUsers', array(
            'errors'                   => $errors,
            'listUsers'                => $listUsers,
            'listUsersJs'              => $listUsersJs,
            'listviewModel'            => $listviewModel,
            'selectedUserListview'     => $selectedUserListview,
            'hierarchySelects'         => $hierarchySelects,
            'salesforceUserFields'     => $filteredSalesforceUserFields,
            'lmsAdditionalFields'      => $syncAgent->getLmsAdditionalFields(),
            'additionalUserFields'     => $additionalUserFields,
			'alreadyAssignedFields'    => $alreadyAssignedFields,
			'alreadyAssignedLmsFields' => $alreadyAssignedLmsFields
        ));
    }

    public function actionSetupContacts(){
        if(isset($_POST['cancelChanges'])){ // Cancel button clicked --> redirect to main page
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $sfLmsAccount = SalesforceLmsAccount::getFirstAccount();

        $accountListviewModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $sfLmsAccount->id_account,
            'type' => 'Account'
        ));
        if(empty($accountListviewModel)){
            $accountListviewModel = new SalesforceSyncListviews();
            $accountListviewModel->idAccount = $sfLmsAccount->id_account;
            $accountListviewModel->type = 'Account';
            $accountListviewModel->force_change_password = 0;
        }
        $contactListviewModel = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $sfLmsAccount->id_account,
            'type' => 'Contact'
        ));
        if(empty($contactListviewModel)){
            $contactListviewModel = new SalesforceSyncListviews();
            $contactListviewModel->idAccount = $sfLmsAccount->id_account;
            $contactListviewModel->type = 'Contact';
            $contactListviewModel->orgchartType = 'none';
            $contactListviewModel->orgchartId = 0;
        }

        // If no account has been set up before, redirect to main page
        if(empty($sfLmsAccount)) {
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $api = new SalesforceApiClient($sfLmsAccount);
        $syncAgent = new SalesforceSyncAgent();

        $additionalAccountFields = $syncAgent->getSfLmsAdditionalFields('Account');
        $additionalContactFields = $syncAgent->getSfLmsAdditionalFields('Contact');

        $errors = array();
        // Submitting the form, checking the settings and saving to database.
        if(isset($_POST['saveChanges'])){
            // Check list views
            $accountsListviewId = Yii::app()->request->getParam('accounts', '');
            $contactsListviewId = Yii::app()->request->getParam('contacts', '');
            $listviewErrorShown = false;
            if(strlen($accountsListviewId) == 18){
                $accountListviewModel->listviewId = $accountsListviewId;
            } else {
                $listviewErrorShown = true;
                $errors[] = Yii::t('salesforce', 'Please choose valid list view filters.');
            }

            if(strlen($contactsListviewId) == 18){
                $contactListviewModel->listviewId = $contactsListviewId;
            } else {
                if(!$listviewErrorShown) $errors[] = Yii::t('salesforce', 'Please choose valid list view filters.');
            }

            // Set force change password setting
            $contactListviewModel->force_change_password = Yii::app()->request->getParam('check_force_change_password', 0);

            // Check org chart selection
            $orgchartType = Yii::app()->request->getParam('organization_chart', '');
					if (empty($orgchartType) && $accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED) {
						//if we come here then it means that the options was already set previously and could not be selected now because blocked by exceeded limit of accounts
						$orgchartType = SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED;
					}
					$_possibleValues = array(
						SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH,
						SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED,
						SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS
					);
            if (in_array($orgchartType, $_possibleValues)) {
                $accountListviewModel->orgchartType = $orgchartType;
            } else {
                $errors[] = Yii::t('salesforce', 'Please select a valid organization chart type.');
            }

            // Check org chart id
            $orgchartId = intval(Yii::app()->request->getParam('idOrgChart', 0));
            if($orgchartId > 0){
                $accountListviewModel->orgchartId = $orgchartId;
            } else {
                $errors[] = Yii::t('salesforce', 'Please select a root branch for your organization chart.');
            }

            // Check hierarchy fields
            $hierarchy1 = Yii::app()->request->getParam('hierarchy1', '');
            $hierarchy2 = Yii::app()->request->getParam('hierarchy2', '');
            $hierarchy3 = Yii::app()->request->getParam('hierarchy3', '');
            if($orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS){
                if(strlen($hierarchy1) > 1){
                    $accountListviewModel->hierarchy1 = $hierarchy1;
                    $accountListviewModel->hierarchy2 = (strlen($hierarchy2) > 1)?$hierarchy2:'';
                    $accountListviewModel->hierarchy3 = (strlen($hierarchy3) > 1)?$hierarchy3:'';
                } else {
                    $errors[] = Yii::t('salesforce', 'Please select field level 1 for your organization chart.');
                }
            } else {
                $accountListviewModel->hierarchy1 = '';
                $accountListviewModel->hierarchy2 = '';
                $accountListviewModel->hierarchy3 = '';
            }

            // Checking additional fields
					$newAdditionalAccountFields = array();
					$newAdditionalContactFields = array();
					if (is_array($_POST)) {
						foreach ($_POST as $key => $val) {
							if (strpos($key, 'sfAccountFieldName_') !== false) { $newAdditionalAccountFields[$key] = $val; }
							if (strpos($key, 'sfContactFieldName_') !== false) { $newAdditionalContactFields[$key] = $val; }
						}
					}

            $additionalAccountFieldsData = $this->processAdditionalFields($newAdditionalAccountFields, 'account');
            $additionalContactFieldsData = $this->processAdditionalFields($newAdditionalContactFields, 'contact', $additionalAccountFieldsData['arNewLmsFields'], $additionalAccountFieldsData['errorsShown']);
            $errors = array_merge($errors, $additionalAccountFieldsData['errors'], $additionalContactFieldsData['errors']);

            $oldAccountFields = array();
            $oldContactFields = array();
            $createAccountFields = array();
            $createContactFields = array();
            $updateAccountFields = array();
            $updateContactFields = array();
            $deleteAccountFields = array();
            $deleteContactFields = array();
            if(empty($errors)){ // Only do the next step of the additional field validation in case we don't have any errors....
                if(!empty($additionalAccountFields)){
                    foreach($additionalAccountFields as $oldField){
                        $oldAccountFields[$oldField['lmsFieldId']] = array(
                            'fieldname' => $oldField['sfFieldType']."|".$oldField['sfFieldName'],
                            'label' => $oldField['sfFieldLabel']
                        );
                    }
                }
                if(!empty($additionalContactFields)){
                    foreach($additionalContactFields as $oldField){
                        $oldContactFields[$oldField['lmsFieldId']] = array(
                            'fieldname' => $oldField['sfFieldType']."|".$oldField['sfFieldName'],
                            'label' => $oldField['sfFieldLabel']
                        );
                    }
                }
                foreach($additionalAccountFieldsData['newFields'] as $key => $newField){
                    if(array_key_exists($key, $oldAccountFields)){
                        $updateAccountFields[$key] = $newField;
                    } else {
                        $createAccountFields[$key] = $newField;
                    }
                }
                foreach($additionalContactFieldsData['newFields'] as $key => $newField){
                    if(array_key_exists($key, $oldContactFields)){
                        $updateContactFields[$key] = $newField;
                    } else {
                        $createContactFields[$key] = $newField;
                    }
                }
                foreach($oldAccountFields as $key => $oldField){
                    if(!array_key_exists($key, $additionalAccountFieldsData['newFields'])){
                        $deleteAccountFields[$key] = $oldField;
                    }
                }
                foreach($oldContactFields as $key => $oldField){
                    if(!array_key_exists($key, $additionalContactFieldsData['newFields'])){
                        $deleteContactFields[$key] = $oldField;
                    }
                }

                // SAVING ALL CHANGES
                $accountListviewModel->save();
                $contactListviewModel->save();
                $this->deleteFields($deleteAccountFields, 'Account', $sfLmsAccount->id_account);
                $this->deleteFields($deleteContactFields, 'Contact', $sfLmsAccount->id_account);
                $this->updateFields($updateAccountFields, 'Account', $sfLmsAccount->id_account);
                $this->updateFields($updateContactFields, 'Contact', $sfLmsAccount->id_account);
                $this->createFields($createAccountFields, 'Account', $sfLmsAccount->id_account);
                $this->createFields($createContactFields, 'Contact', $sfLmsAccount->id_account);

                header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
                exit;
            }
            $additionalAccountFields = $additionalAccountFieldsData['updatedAdditionalFields'];
            $additionalContactFields = $additionalContactFieldsData['updatedAdditionalFields'];
        }

        $accountOptions = $api->generateListviewDropdownList('Account', true);
        $contactOptions = $api->generateListviewDropdownList('Contact');
        $listAccounts = array();
        $listContacts = array();
        $listAccountsJs = array();
        $listContactsJs = array();
        foreach($accountOptions as $option){
            $listAccounts[$option['listviewId']] = $option['label'];
            $listAccountsJs[$option['listviewId']] = $option['items'];
        }
        foreach($contactOptions as $option){
            $listContacts[$option['listviewId']] = $option['label'];
            $listContactsJs[$option['listviewId']] = $option['items'];
        }

        $picklistStructure = $api->getPicklistStructure('Account');
        $hierarchySelects = $this->getHierarchySelects($picklistStructure, $accountListviewModel);

        if(!empty($accountListviewModel)){
            $selectedAccountListview = $accountListviewModel->listviewId;
        } else {
            $selectedAccountListview = false;
        }
        if(!empty($contactListviewModel)){
            $selectedContactListview = $contactListviewModel->listviewId;
        } else {
            $selectedContactListview = false;
        }

			/*/--- Removed  filter for standar fields
			$salesforceAccountFields = $api->getSalesforceFieldsList('Account');
			$salesforceContactFields = $api->getSalesforceFieldsList('Contact');
			$filteredSalesforceAccountFields = array();
			$filteredSalesforceContactFields = array();
			$defaultAccountFields = SalesforceApiClient::$ACCOUNT_FIELDS;
			$defaultContactFields = SalesforceApiClient::$CONTACT_FIELDS;
			if (is_array($salesforceAccountFields)) {
				foreach ($salesforceAccountFields as $key => $value) {
					list($type, $name) = explode('|', $key);
					if (!in_array($name, $defaultAccountFields)) { $filteredSalesforceAccountFields[$key] = $value; }
				}
			}
			if (is_array($salesforceContactFields)) {
				foreach ($salesforceContactFields as $key => $value) {
					list($type, $name) = explode('|', $key);
					if (!in_array($name, $defaultContactFields)) { $filteredSalesforceContactFields[$key] = $value; }
				}
			}
			/---*/

            $filteredSalesforceAccountFields = $api->getSalesforceFieldsList('Account');
            $filteredSalesforceContactFields = $api->getSalesforceFieldsList('Contact');

			$alreadyAssignedFields = array(
				'Account' => array(),
				'Contact' => array()
			);
			$alreadyModels = SalesforceAdditionalFields::model()->findAll();
			$alreadyAssignedLmsFields = array();
			if (is_array($alreadyModels)) {
				foreach ($alreadyModels as $alreadyModel) {
					/* @var $alreadyModel SalesforceAdditionalFields */
					$_sfIndex = $alreadyModel->sfFieldName;
					$_lmsIndex = $alreadyModel->lmsFieldId;
					switch ($alreadyModel->sfObjectType) {
						case 'Account':
							$alreadyAssignedFields['Account'][$_sfIndex] = $_lmsIndex;
							break;
						case 'Contact':
							$alreadyAssignedFields['Contact'][$_sfIndex] = $_lmsIndex;
							break;
					}
					//collect LMS field IDs
					$_lmsFieldId = 0;
					if (is_numeric($alreadyModel->lmsFieldId)) {
						$_lmsFieldId = $alreadyModel->lmsFieldId;
					} elseif (is_string($alreadyModel->lmsFieldId) && strpos($alreadyModel->lmsFieldId, '|') !== false) {
						$_arr = explode('|', $alreadyModel->lmsFieldId);
						$_lmsFieldId = $_arr[1];
					}
					if ($_lmsFieldId > 0) {
						$alreadyAssignedLmsFields[$alreadyModel->sfObjectType][] = $_lmsFieldId;
					}
				}
			}

        $this->render('setupContacts', array(
            'errors'                   => $errors,
            'listAccounts'             => $listAccounts,
            'listContacts'             => $listContacts,
            'listAccountsJs'           => $listAccountsJs,
            'listContactsJs'           => $listContactsJs,
            'accountListviewModel'     => $accountListviewModel,
            'contactListviewModel'     => $contactListviewModel,
            'selectedAccountListview'  => $selectedAccountListview,
            'selectedContactListview'  => $selectedContactListview,
            'hierarchySelects'         => $hierarchySelects,
            'salesforceAccountFields'  => $filteredSalesforceAccountFields,
            'salesforceContactFields'  => $filteredSalesforceContactFields,
            'lmsAdditionalFields'      => $syncAgent->getLmsAdditionalFields(),
            'additionalAccountFields'  => $additionalAccountFields,
            'additionalContactFields'  => $additionalContactFields,
					'alreadyAssignedFields' => $alreadyAssignedFields,
					'alreadyAssignedLmsFields' => $alreadyAssignedLmsFields
        ));
    }

    /**
     * Processes the additional fields for validation and (when necessary) prefilling the updated form after erroneous submit
     * @param array $newAdditionalFields
     * @param string $type: user|contact|account
     * @param array $arNewLmsFields : array to check if LMS fields are unique
     * @param array $errorsShown: keeps track of earlier displayed errors in order not to display the same error twice
     * @return array
     */
    public function processAdditionalFields($newAdditionalFields, $type, $arNewLmsFields = array(), $errorsShown = array()){
        $newFields = array(); // Create an array with key=>val structure
        $arNewSfFields = array(); // array to check if Salesforce fields are unique
        $updatedAdditionalFields = array();
        if(empty($errorsShown)){
            $errorsShown = array(
                'lmsFieldErrorShown' => false,
                'lmsNoneSelectedShown' => false,
                'sfFieldErrorShown' => false
            );
        }
        $errors = array();

        switch($type){
            case 'user':
                $f_sfFieldname = 'sfUserFieldName_';
                $f_sfFieldLabel = 'sfUserFieldLabel_';
                $f_lmsFieldId = 'UserLmsFieldId_';
                break;
            case 'account':
                $f_sfFieldname = 'sfAccountFieldName_';
                $f_sfFieldLabel = 'sfAccountFieldLabel_';
                $f_lmsFieldId = 'AccountLmsFieldId_';
                break;
            case 'contact':
                $f_sfFieldname = 'sfContactFieldName_';
                $f_sfFieldLabel = 'sfContactFieldLabel_';
                $f_lmsFieldId = 'ContactLmsFieldId_';
                break;
        }

        if (!empty($newAdditionalFields)){
            foreach($newAdditionalFields as $key => $val){
                $index = str_replace($f_sfFieldname, '', $key);
                $lmsField = Yii::app()->request->getParam($f_lmsFieldId.$index, '');
                $sfField = Yii::app()->request->getParam($f_sfFieldname.$index, '');
                $sfLabel = Yii::app()->request->getParam($f_sfFieldLabel.$index, '');
                $noErrors = true;
                if(in_array($lmsField, $arNewLmsFields)) { // LMS field isn't unique
                    $noErrors = false;
                    if (!$errorsShown['lmsFieldErrorShown']) {
                        $errors[] = Yii::t('salesforce', 'LMS fields may not be assigned to multiple Salesforce fields.');
                        $errorsShown['lmsFieldErrorShown'] = true;
                    }
                } elseif(!$lmsField){ // LMS field hasn't been chosen
                    $noErrors = false;
                    if(!$errorsShown['lmsNoneSelectedShown']){
                        $errors[] = Yii::t('salesforce', 'Please assign all added salesforce fields to an LMS field.');
                        $errorsShown['lmsNoneSelectedShown'] = true;
                    }
                } else {
                    $arNewLmsFields[] = $lmsField;
                }
                if(in_array($sfField, $arNewSfFields)){ // Salesforce field isn't unique
                    $noErrors = false;
                    if(!$errorsShown['sfFieldErrorShown']){
                        $errors[] = Yii::t('salesforce', 'Salesforce fields may not be assigned to multiple LMS fields.');
                        $errorsShown['sfFieldErrorShown'] = true;
                    }
                } else {
                    $arNewSfFields[] = $sfField;
                }
                if($noErrors){
                    $newFields[$lmsField] = array(
                        'fieldname' => $sfField,
                        'label' => $sfLabel
                    );
                }
                $fieldnameParts = explode("|", $sfField);
                $updatedAdditionalFields[] = array(
                    'sfFieldType' => $fieldnameParts[0],
                    'sfFieldName' => $fieldnameParts[1],
                    'sfFieldLabel' => $sfLabel,
                    'lmsFieldId' => $lmsField
                );
            }
        }

        $response = array(
            'errors'                  => $errors,
            'arNewLmsFields'          => $arNewLmsFields,
            'newFields'               => $newFields,
            'updatedAdditionalFields' => $updatedAdditionalFields
        );
        return $response;
    }



	/**
	 * Deletes the list of old additional fields
	 * @param array $deleteFields
	 * @param string $type account|contact|user
	 * @param int $accountId
	 */
	public function deleteFields($deleteFields, $type, $accountId)
	{
		if (!empty($deleteFields)) {
			foreach ($deleteFields as $lmsId => $deleteField) {
				if (is_string($lmsId) && strpos($lmsId, '|') !== false) {
					list(, $lmsId) = explode('|', $lmsId);
				}
				Yii::app()->db->createCommand(
					"DELETE FROM " . SalesforceAdditionalFields::model()->tableName() . " ".
					"WHERE sfObjectType = '" . $type . "' AND lmsAccountId = '" . $accountId . "' AND lmsFieldId = '" . $lmsId . "'"
				)->execute();
			}
		}
	}



	/**
	 * Updates the list of existing additional fields
	 * @param array $updateFields
	 * @param string $type
	 * @param int $accountId
	 */
	public function updateFields($updateFields, $type, $accountId)
	{
		if (!empty($updateFields)) {
			foreach ($updateFields as $lmsId => $updateField) {
				$fieldnameParts = explode("|", $updateField['fieldname']);
				if (is_string($lmsId) && strpos($lmsId, '|') !== false) {
					list(, $lmsId) = explode('|', $lmsId);
				}
				Yii::app()->db->createCommand(
					"UPDATE " . SalesforceAdditionalFields::model()->tableName() . " ".
					"SET sfFieldType = '" . $fieldnameParts[0] . "', sfFieldName = '" . $fieldnameParts[1] . "', sfFieldLabel = '" . $updateField['label'] . "' ".
					"WHERE sfObjectType = '" . $type . "' AND lmsAccountId = '" . $accountId . "' AND lmsFieldId = '" . $lmsId . "'"
				)->execute();
			}
		}
	}



	/**
	 * Creates the list of new additional fields
	 * @param array $createFields
	 * @param string $type
	 * @param int $accountId
	 */
	public function createFields($createFields, $type, $accountId)
	{
		if (!empty($createFields)) {
			foreach ($createFields as $lmsId => $createField) {
				$fieldnameParts = explode("|", $createField['fieldname']);
				if (is_string($lmsId) && strpos($lmsId, '|') !== false) {
					list(, $lmsId) = explode('|', $lmsId);
				}
				Yii::app()->db->createCommand()
					->insert(SalesforceAdditionalFields::model()->tableName(), array(
						'sfObjectType' => $type,
						'sfFieldType' => $fieldnameParts[0],
						'sfFieldName' => $fieldnameParts[1],
						'sfFieldLabel' => $createField['label'],
						'lmsFieldId' => $lmsId,
						'lmsAccountId' => $accountId
					));
			}
		}
	}

    public function actionSetupCourses(){
        set_time_limit(3*3600); // 3 hours
        if(isset($_POST['cancelChanges'])){ // Cancel button clicked --> redirect to main page
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $sfLmsAccount = SalesforceLmsAccount::getFirstAccount();

        // If no account has been set up before, redirect to main page
        if(empty($sfLmsAccount)) {
            header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
            exit;
        }

        $api = new SalesforceApiClient($sfLmsAccount);
        $syncAgent = new SalesforceSyncAgent();

        $errors = array();
        // Submitting the form, checking the settings and saving to database.
        if(isset($_POST['saveChanges'])){
            $selectionType = Yii::app()->request->getParam('course_selection');
            if($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL){
                $sfLmsAccount->selected_items = $selectionType;
            }
            $selectedItems = $sfLmsAccount->getSelectedItems();

            $selectedCounter = count($selectedItems['courses']) + count($selectedItems['plans']);

            $replicate_products = Yii::app()->request->getParam('replicate_products');
            $issue_so = Yii::app()->request->getParam('issue_so');
            if($replicate_products == SalesforceLmsAccount::TYPE_REPLICATE_NONE){
                $sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_NONE;
            } else {
                if(!$issue_so){
                    $sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_COURSES;
                } else {
                    $sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS;
                }
            }

            if(in_array($selectionType, array(SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES, SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS)) && ($sfLmsAccount->selected_items == null)){
                $errors[] = Yii::t('salesforce', 'Select at least 1 category or catalog.');
            } elseif($selectedCounter == 0){
                $errors[] = Yii::t('salesforce', 'You have no matching courses for your selection. Please broaden your selection or add courses to your existing categories or catalogs.');
            }

            if(empty($errors)) {
                $sfLmsAccount->save();

                $dataInfo = array();

                // Creating or Updating product Family
                if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
                    Yii::log('Creating Product Family');
                    $result = $api->updateProduct2FamilyPicklist();
                    Yii::log(var_export($result, true));

                    if ($result->result->success == false) {
                        // return false;
                    } else {
                        $dataInfo['ProductFamily'] = 'Product Family' . ' ' . Yii::t('salesforce', 'created');
                    }
                }

                // Creating or Updating Order Type
                if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
                    Yii::log('Creating Order type');
                    $result = $api->updateOrderTypePicklist();
                    Yii::log(var_export($result, true));
                    if ($result->result->success == false) {
                        return false;
                    } else {
                        $dataInfo['OrderType'] = 'Order Type' . ' ' . Yii::t('salesforce', 'created');
                    }
                }

                // Creating Custom Objects
                // COURSE/PLAN
                if (!$api->customObjectExists(SalesforceApiClient::CO_COURSE)) {

                    $addProductField = $syncAgent->addProductField($sfLmsAccount->replicate_courses_as_products);
                    Yii::log('Creating Courses Custom Object');
                    $result = $api->createCourseCustomObject($addProductField);
                    /**
                     * replicate_courses_highest_history represents the highest setting the LMS account has ever had for replicate_courses_as_products
                     * if value = 0 --> Configuration never had product replication active
                     * if value = 1 --> Currently or previously, product replication has been active, but not Sales Orders
                     * if value = 2 --> Currently or previously, product replication and Sales Orders have been active
                     * Based on this value, we're able to determine whether or not to add extra fields to the Custom Objects in Salesforce
                     */
                    Yii::app()->db->createCommand()
                        ->update(SalesforceLmsAccount::model()->tableName(), array(
                            'replicate_courses_highest_history' => $sfLmsAccount->replicate_courses_as_products
                        ), 'id_account = :idAccount', array(
                            ':idAccount' => $sfLmsAccount->id_account
                        ));

                    Yii::log(var_export($result, true));
                    if ($result->result->success == false) {
                        return false;
                    } else {
                        $dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'created');
                    }
                } elseif (($sfLmsAccount->replicate_courses_as_products > $sfLmsAccount->replicate_courses_highest_history)
                    &&
                    $sfLmsAccount->replicate_courses_highest_history == SalesforceLmsAccount::TYPE_REPLICATE_NONE
                ) {

                    $result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE, SalesforceApiClient::$COURSE_CUSTOM_OBJECT_FIELDS, 'Product');

                    Yii::log(var_export($result, true));
                    if ($result->result->success == true) {
                        $dataInfo['CO1'] = 'Custom Object' . ' ' . Yii::t('salesforce', 'updated');
                    }
                }

                // ENROLLMENTS
                if (!$api->customObjectExists(SalesforceApiClient::CO_COURSE_ENROLLMENT)) {
                    $addOrderField = false;
                    if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
                        $addOrderField = true;
                    }

                    $addProductField = $syncAgent->addProductField($sfLmsAccount->replicate_courses_as_products);

                    Yii::log('Creating Enrollments Custom Object');
                    $result = $api->createEnrollmentCustomObject($addProductField, $addOrderField);
                    Yii::log(var_export($result, true));
                    if ($result->result->success == false) {
                        return false;
                    } else {
                        $dataInfo['CO2'] = 'Course Custom Object' . ' ' . Yii::t('salesforce', 'created');
                    }
                } elseif ($sfLmsAccount->replicate_courses_as_products > $sfLmsAccount->replicate_courses_highest_history) {
                    if ($sfLmsAccount->replicate_courses_as_products >= SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
                        $result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Product');
                    }
                    if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_COURSES) {
                        $result = $api->updateCustomObjectToAddField(SalesforceApiClient::CO_COURSE_ENROLLMENT, SalesforceApiClient::$ENROLLMENT_CUSTOM_OBJECT_FIELDS, 'Order');
                    }
                    if ($result->result->success == true) {
                        $dataInfo['CO2'] = 'Enrollment Custom Object' . ' ' . Yii::t('salesforce', 'updated');
                    }

                    Yii::app()->db->createCommand()
                        ->update(SalesforceLmsAccount::model()->tableName(), array(
                            'replicate_courses_highest_history' => $sfLmsAccount->replicate_courses_as_products
                        ), 'id_account = :idAccount', array(
                            ':idAccount' => $sfLmsAccount->id_account
                        ));
                }

                if ($sfLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) {
                    // Create Pricebook
                    Yii::log('Creating Pricebook');
                    $result = $api->createPriceBook();
                    Yii::log(var_export($result, true));
                    if ($result === false) return false;
                    $dataInfo['Pricebook'] = 'Pricebook' . ' ' . Yii::t('salesforce', 'created');
                }

                header('location: '.Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index'));
                exit;
            }
        }

        $selectionType = $sfLmsAccount->getSelectionType();
        if($selectionType == '') $selectionType = SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL;
        $selectedItems = $sfLmsAccount->getSelectedItems();
        $selectedAll = $sfLmsAccount->getSelectedItems(true);

        $selectedCounters = array(
            SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL => count($selectedAll['courses']) + count($selectedAll['plans']),
            SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES => 0,
            SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS => 0
        );
        $selectedCounters[$selectionType] = count($selectedItems['courses']) + count($selectedItems['plans']);

        $this->render('setupCourses', array(
            'errors'                   => $errors,
            'lmsAccount'               => $sfLmsAccount,
            'selectionType'            => $selectionType,
            'selectedCounters'         => $selectedCounters
        ));
    }


	public function actionAxSelectOrganizationChartNode()
	{
		$type = Yii::app()->request->getParam('type', false);
		$selectedNode = Yii::app()->request->getParam('node', false);

		$stateIndex = 'SalesForce-'.$type.'-selectedNode';
		$state = Yii::app()->user->getState($stateIndex);
		if ($state) {
			$selectedNode = $state;
		}
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');

		if (isset($_POST['orgcharts_selected-json'])) {
			Yii::app()->user->setState($stateIndex, $_POST['orgcharts_selected-json']);
			echo '<a class="auto-close" data-node="' . $_POST['orgcharts_selected-json'] . '"></a>';
		}

		$this->renderPartial('axSelectOrganizationChartNode', array('node' => $selectedNode, 'type' => $type), false, false);
	}


    public function actionAxCreateNewFieldPopup()
    {
        $this->renderPartial('axCreateNewFieldPopup', array(), false, false);
    }

    /**
     * Initialize the sturcture replication of Salesforce
     *
     * @return array Array of operations that are succeded with the creating of the structure
     */
    private function initSalesforce(){
        $sfSyncAgent = new SalesforceSyncAgent();

        return $sfSyncAgent->structureReplication();
    }

    public function actionAxOpen() {

        $id = Yii::app()->request->getParam('account', null);

        $salesforceModel = null;

        if($id === null){
            $salesforceModel = new SalesforceLmsAccount();
        } else{
            $salesforceModel = SalesforceLmsAccount::model()->findByPk($id);
        }

        $this->renderPartial('wizard/step1', array(
            'salesforceLmsAccount' => $salesforceModel
        ));
        //$this->renderPartial('step1', array(),true,true);
        //echo $html;
    }

    public function actionAxStep1() {
        $html = $this->renderPartial('wizard/step1', array(), true, true);
        echo $html;
    }


    public function actionAxStep2() {
        $this->renderPartial('wizard/step2');
    }


    public function actionAxStep3() {
        $html = $this->renderPartial('wizard/step3', array(),true,true);
        echo $html;
    }


    public function actionAxStep4() {
        $html = $this->renderPartial('wizard/step4', array(),true,true);
        echo $html;
    }

    public function actionAxStep5() {
        $html = $this->renderPartial('wizard/step5', array(),true,true);
        echo $html;
    }

    public function actionSelectCourseCategories(){
        $confirmBtn         = Yii::app()->request->getParam('confirm_write', null);
        $id_account         = Yii::app()->request->getParam('id_account', null);
        $search_input       = Yii::app()->request->getParam('search_input', null);

        if ($id_account) {
            $salesforceLmsAccount = SalesforceLmsAccount::model()->findByPk( $id_account );
            $selectedIds = $salesforceLmsAccount->getSelectedIds();
        }

        if ($confirmBtn != null) {
            $categories         = Yii::app()->request->getParam('fancytree-selected-nodes-json', null);

            $categoriesJson = array(
                'type'        => SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES,
                'selected'    => CJSON::decode($categories),
            );
            $salesforceLmsAccount->selected_items = CJSON::encode($categoriesJson);
            $salesforceLmsAccount->save();

            echo '<a class="auto-close"></a>';
            echo '<script>window.location.reload()</script>';
            Yii::app()->end();
        }

        $treeData = new CoursesGrid();
        if ($treeData) {
            $categoryTree2 = $treeData->callBuildTreeData(false);
        }

        $filterObject = new stdClass();
        $filterObject->search_input = $search_input;

        $this->renderPartial('selectCourseCategories', array(
            'categoryTree2'         => $categoryTree2,
            'filterObject'			=> $filterObject,
            'selectedIds'        	=> $selectedIds,
            'id'                    => 'orgChartFancyTree1'
        ), false, true);

        Yii::app()->end();
    }

    public function actionSelectCourseCatalog(){
        $confirmBtn         = Yii::app()->request->getParam('confirm_write', null);
        $id_account         = Yii::app()->request->getParam('id_account', null);
        $search_input       = Yii::app()->request->getParam('search_input', null);
        $comboGridViewSelectAll       = Yii::app()->request->getParam('comboGridViewSelectAll', null);

        if ($id_account) {
            $salesforceLmsAccount = SalesforceLmsAccount::model()->findByPk( $id_account );
            $selectionType = $salesforceLmsAccount->getSelectionType();
            $selectedIds = $salesforceLmsAccount->getSelectedIds();
        }

        if ($confirmBtn != null) {
            $catalogs = Yii::app()->request->getParam('items-grid-checkboxes', null);

            $catalogJson = array(
                'type'        => SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS,
                'selected'    => $catalogs
            );
            $salesforceLmsAccount->selected_items = CJSON::encode($catalogJson);
            $salesforceLmsAccount->save();

            echo "<a class='auto-close catalogs' data-catalogs='". CJSON::encode($catalogs)."'></a>";
            echo '<script>window.location.reload()</script>';
            Yii::app()->end();
        }
        $catalogModel = LearningCatalogue::model();



        if ( $comboGridViewSelectAll ) {
            $dataProvider = $catalogModel->sqlDataProvider();
            $dataProvider->setPagination(false);

            $keys = $dataProvider->keys;

            foreach ($keys as $index => $key) {
                $keys[$index] = (int) $key;
            }
            echo CJSON::encode($keys);
            Yii::app()->end();
        }

        $filterObject = new stdClass();
        $filterObject->search_input = $search_input;

        $this->renderPartial('selectCourseCatalog', array(
            'catalogModel'          => $catalogModel,
            'filterObject'			=> $filterObject,
            'selectionType'          => $selectionType,
            'selectedIds'        	=> $selectedIds,
            'id'                    => 'orgChartFancyTree1'
        ), false, true);
        Yii::app()->end();
    }

    public function actionCoursesSynch( ) {
        //Todo: function has become deprecated...
        $confirmBtn         = Yii::app()->request->getParam('confirm_write', null);
        $cfilter_option     = Yii::app()->request->getParam('cfilter_option', null);
        $id_account         = Yii::app()->request->getParam('id_account', null);
        $search_input       = Yii::app()->request->getParam('search_input', null);
        $comboGridViewSelectAll       = Yii::app()->request->getParam('comboGridViewSelectAll', null);

        if ($id_account) {
            $salesforceLmsAccount = SalesforceLmsAccount::model()->findByPk( $id_account );
            $selectionType = $salesforceLmsAccount->getSelectionType();
            $selectedIds = $salesforceLmsAccount->getSelectedIds();
        }

        if ($confirmBtn === 'Confirm' ) {

            $salesforceLmsAccount = SalesforceLmsAccount::model()->findByPk( $id_account );

            $catalogs           = Yii::app()->request->getParam('selected_catalogs', null);
            $categories         = Yii::app()->request->getParam('fancytree-selected-nodes-json', null);
            $learningCourse 	= Yii::app()->request->getParam('LearningCourse', null);
           // $salesforceLmsAccount = SalesforceLmsAccount::model()->findByPk( $id_account );



            if ($cfilter_option) {
                switch ($cfilter_option) {
                    case 1:
                        $salesforceLmsAccount->selected_items = SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL;
                        $salesforceLmsAccount->save();
                        break;

                    case 2:
                        $categoriesJson = array(
                            'type'        => SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES,
                            'selected'    => CJSON::decode($categories),
                        );
                        $salesforceLmsAccount->selected_items = CJSON::encode($categoriesJson);
                        $salesforceLmsAccount->save();
                        break;

                    case 3:
						$catalogJson = array(
                        	'type'        => SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS,
                            'selected'    => explode(',', $catalogs),
						);
                        $salesforceLmsAccount->selected_items = CJSON::encode($catalogJson);
                        $salesforceLmsAccount->save();
                        break;

                   	default:
                    	break;
                }
            }

            echo '<a class="auto-close"></a>';

            echo '<script>$("#sync_courses").trigger("click")</script>';
            echo '<script>window.location.reload()</script>';
            Yii::app()->end();
        }
        $catalogModel = LearningCatalogue::model();



        if ( $comboGridViewSelectAll ) {
        	$dataProvider = $catalogModel->sqlDataProvider();
        	$dataProvider->setPagination(false);

        	$keys = $dataProvider->keys;

        	foreach ($keys as $index => $key) {
        		$keys[$index] = (int) $key;
        	}
        	echo CJSON::encode($keys);
        	Yii::app()->end();
        }

        $categoryTree = LearningCourseCategoryTree::model();

        $treeData = new CoursesGrid();
        if ($treeData) {
            $categoryTree2 = $treeData->callBuildTreeData(false);
        }

    	$filterObject = new stdClass();
    	$filterObject->search_input = $search_input;

        $this->renderPartial('coursesSynch', array(
            'catalogModel'          => $catalogModel,
            'categoryTree'          => $categoryTree,
            'categoryTree2'         => $categoryTree2,
        	'filterObject'			=> $filterObject,
            'selectionType'          => $selectionType,
            'selectedIds'        	=> $selectedIds,
            'id'                    => 'orgChartFancyTree1'
        ), false, true);
        Yii::app()->end();
    }

    public function actionSelectListViewAccounts(){
        $confirmBtn = Yii::app()->request->getParam('confirm_write', null);
        $error = false;

        $accountId = Yii::app()->request->getParam('id_account', null);
        $sfLmsAccount = SalesforceLmsAccount::model()->findByPk($accountId);
        $api = new SalesforceApiClient($sfLmsAccount);

        $listViewAccount = $this->retrieveListview($api, $accountId, SalesforceLmsAccount::SF_TYPE_ACCOUNT);

        if($confirmBtn){
            $check = Yii::app()->request->getParam('salesforce-account-filter');
            if(!$check){
                $error = Yii::t('salesforce', 'Please select a filter...');
            }
            echo '<a class="auto-close"></a>';
            echo '<script>window.location.reload()</script>';
        }

        $this->renderPartial('selectListViewAccounts', array(
            'listAccounts'      => $api->generateListviewDropdownList('Account'),
            'selectedAccount'   => $listViewAccount->listviewId,
            'error'             => $error
        ), false, true);

        Yii::app()->end();
    }

    public function actionSelectListViewUsers(){
        $confirmBtn = Yii::app()->request->getParam('confirm_write', null);
        $error = false;

        $accountId = Yii::app()->request->getParam('id_account', null);
        $sfLmsAccount = SalesforceLmsAccount::model()->findByPk($accountId);
        $api = new SalesforceApiClient($sfLmsAccount);

        $listViewUser    = $this->retrieveListview($api, $accountId, SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
        $listViewContact = $this->retrieveListview($api, $accountId, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
        $listViewLead    = $this->retrieveListview($api, $accountId, SalesforceLmsAccount::SYNC_USER_TYPE_LEADS);

        if($confirmBtn){
            $checkUser    = Yii::app()->request->getParam('salesforce-user-filter');
            $checkContact = Yii::app()->request->getParam('salesforce-contact-filter');
            $checkLead    = Yii::app()->request->getParam('salesforce-lead-filter');
            if(
                !$checkUser && strpos($sfLmsAccount->sync_user_types, 'user') !== false
                ||
                !$checkContact && strpos($sfLmsAccount->sync_user_types, 'contact') !== false
                ||
                !$checkLead && strpos($sfLmsAccount->sync_user_types, 'lead') !== false
            ){
                $error = Yii::t('salesforce', 'Please select all filters...');
            }
            else {
                echo '<a class="auto-close"></a>';
                echo '<script>window.location.reload()</script>';
            }
        }

        $this->renderPartial('selectListViewUser', array(
            'sync_user_types' => $sfLmsAccount->sync_user_types,
            'listUsers'       => $api->generateListviewDropdownList('User'),
            'listContacts'    => $api->generateListviewDropdownList('Contact'),
            'listLeads'       => $api->generateListviewDropdownList('Lead'),
            'selectedUser'    => $listViewUser->listviewId,
            'selectedContact' => $listViewContact->listviewId,
            'selectedLead'    => $listViewLead->listviewId,
            'error'           => $error
        ), false, true);
        Yii::app()->end();
    }

    /**
     * Finds the AR for the select type or creates a new one
     *
     * @param $apiClient
     * @param $accountId int The LmsAccountId for the Salesforce account
     * @param $type string user|account|lead|contact
     * @return SalesforceSyncListviews|static
     */
    public function retrieveListview($apiClient, $accountId, $type){
        $listView = SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $accountId,
            'type' => ucfirst($type)
        ));
        $fieldName = 'salesforce-'.$type.'-filter';
        if(!$listView){
            $listView = new SalesforceSyncListviews();
            $listView->idAccount = $accountId;
            $listView->type = ucfirst($type);
            $listView->listviewId = null;
            $listView->listviewName = null;
        }
        if(!empty($_POST[$fieldName])){
            $listView->listviewId = Yii::app()->request->getParam($fieldName);
            $listView->listviewName = $apiClient->getFilterName($listView->listviewId);
            $listView->save();
        }
        return $listView;
    }

    /**
     * Try to retrieve a listview record
     * @param $accountId
     * @param $type
     * @return static
     */
    public function fetchListView($accountId, $type){
        return SalesforceSyncListviews::model()->findByAttributes(array(
            'idAccount' => $accountId,
            'type' => ucfirst($type)
        ));
    }

    /**
     * Starting an imidiateJob for syncing acounts | users | courses.
     */
    public function actionSync(){

        $result = array();

        $id = Yii::app()->request->getParam('id_account', null);

        $sfLmsAccount = SalesforceLmsAccount::model()->findByPk($id);

        if(!$sfLmsAccount){
        	$data['success'] = false;
        	echo json_encode($data);
        	Yii::app()->end();
        }

        //Check for Overlapping
        $jobStarted = CoreJob::model()->find('name = :name', array(
            ':name' => SalesforceSyncTask::JOB_NAME
        ));

        if($jobStarted){
//            $data['success'] = false;
            $params = CJSON::decode($jobStarted->params);
            $data['success'] = true;
            $data['jobId'] = (isset($params['firstJobId']))?$params['firstJobId']:$jobStarted->hash_id;
        } else {
            $type = Yii::app()->request->getParam('type', null);
            $params = array(
                'type' => $type,
                'id_account' => $id
            );
            $scheduler = Yii::app()->scheduler;
            $idJob = $scheduler->createImmediateJob(SalesforceSyncTask::JOB_NAME, SalesforceSyncTask::id(), $params);
            $data['success'] = true;
            $data['jobId'] = $idJob->hash_id;
        }

        echo json_encode($data);
    }



	/**
	 * Waiting for Job to finish the proccess of synchronization.
	 */
	public function actionWaitForSync()
	{
		$job_id = Yii::app()->request->getParam('job_id', null);

		$salesforceLog = SalesforceLog::model()->find('job_id = :job', array(':job' => $job_id));

		$data = null;

		if (!empty($salesforceLog)) {

			//update box 3 content (it will be re-drawn in the browser page)
			$params = $this->_getConfigParams();
			$boxContent = $this->renderPartial('config/_box_3', array(
				'blockReady' => $params['block3Ready'],
				'blockActive' => $params['block3Active'],
				'salesforceLmsAccount' => $params['salesforceLmsAccount'],
				'coursesCompleted' => $params['coursesCompleted'],
				'coursesButtonText' => $params['coursesButtonText'],
				'showSyncCoursesButton' => $params['showSyncCoursesButton'],
				'coursesLog' => $params['coursesLog']
			), true);

			$data = array(
				'lastDate' => Yii::app()->localtime->toLocalDateTime($salesforceLog->datetime),
				'itemsCount' => $salesforceLog->getItemsCount(),
				'box_3' => $boxContent,
				'success' => true
			);
		} else {
			$data = array(
				'success' => false
			);
		}

		$this->sendJSON($data);
		Yii::app()->end();
	}



    public function actionViewLog(){
        $type = Yii::app()->request->getParam('type', null);
        $accountId = Yii::app()->request->getParam('account', null);

        $logModel = null;
        switch ($type){
            case SalesforceLog::TYPE_LOG_USERS:
                $logModel = SalesforceLog::getLastUsersLog($accountId);
                break;
            case SalesforceLog::TYPE_LOG_COURSES:
                $logModel = SalesforceLog::getLastCoursesLog($accountId);
                break;
        }

        $dataProvider = new CArrayDataProvider($logModel->getLogErrors(), array(
            'keyField' => false,
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            )
        ));

        $this->renderPartial('logs', array(
            'logModel' => $logModel,
            'dataProvider' => $dataProvider
        ), false, true);
    }

    public function actionAxProgressiveExportLogs() {

        $response = new AjaxResult();

        $defaultLimit = 10;

        $html = "";

        $logId 	    = Yii::app()->request->getParam('logId', false);
        $limit 		= Yii::app()->request->getParam('limit', $defaultLimit);
        $offset 	= Yii::app()->request->getParam('offset', 0);
        $start 		= Yii::app()->request->getParam('start', false);
        $fileName	= basename(Yii::app()->request->getParam('fileName', false));
        $totalNumber= Yii::app()->request->getParam('totalNumber', false);
        $download	= Yii::app()->request->getParam('download', false);
        $downloadGo	= Yii::app()->request->getParam('downloadGo', false);
        $exportType	= Yii::app()->request->getParam('exportType', LearningReportFilter::EXPORT_TYPE_CSV);
        $phase		= Yii::app()->request->getParam('phase', 'start');

        $customFilter = Yii::app()->request->getParam('custom_filter', array());

        if ($download) {
            echo '<a class="auto-close"></a>';
            Yii::app()->end();
        }

        if ($downloadGo) {
            $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_UPLOADS);
            $storage->sendFile($fileName);
            Yii::app()->end();
        }

        try {
            if (!$logId) {
                throw new CException('Invalid or missing Salesforce account ID');
            }

            $loggerModel = SalesforceLog::model()->findByPk($logId);
            $loggedErrors = $loggerModel->getLogErrors();

            $reportTitle = 'Salesforce_Log_Export_'.rand(100, 99999);

            $csvFileName = Sanitize::fileName($reportTitle . ".csv", true);
            $csvFileName = $fileName ? $fileName : $csvFileName;
            $csvFilePath = Docebo::getUploadTmpPath().DIRECTORY_SEPARATOR . $csvFileName;

            $nextOffset = $offset;

            if ( $phase == 'start' || $start ) {
                $fh = fopen( $csvFilePath, 'w');
                $headers = array(Yii::t('standard', '_DESCRIPTION'));
                /***- (_Write HEADERS_) -***/
                CSVParser::filePutCsv($fh, $headers);
            }
            else if ( $phase == 'extract' ) {

                switch ($_REQUEST['export']) {
                    case 1: $exportType = LearningReportFilter::EXPORT_TYPE_XLS;
                        break;
                    case 2: $exportType = LearningReportFilter::EXPORT_TYPE_CSV;
                }

                if($exportType != LearningReportFilter::EXPORT_TYPE_CSV) {
                    if(($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
                        $formatCellsAsText = false;
                    } else {
                        $gnumeric = new Gnumeric();
                        $formatCellsAsText = $gnumeric->checkGnumeric();
                    }
                }

                $arr = array();
                foreach ($loggedErrors as $e){
                	$arr[] = array($e);
                }

                $fh = fopen($csvFilePath, 'a');
                $nextOffset = $offset + $limit;
                foreach ( $arr as $k => $v) {
                    CSVParser::filePutCsv($fh, $v);
                }

            }
            else if ($phase == 'convert') {
                $gnumeric = new Gnumeric();
                $in = $csvFilePath;
                $pathInfo = pathinfo($in);
                $extension = $exportType;
                $fileName = $pathInfo['filename'] . "." . $extension;

                $out =  $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $fileName;

                $format = false;

                switch ($exportType) {
                    case LearningReportFilter::EXPORT_TYPE_CSV:
                        $format = Gnumeric::FORMAT_CSV;
                        break;
                    case LearningReportFilter::EXPORT_TYPE_XLS;
                        $format = Gnumeric::FORMAT_XLS;
                        break;
                }

                $result = $gnumeric->convertTo($in, $out, $format);

                if (!$result) {
                    throw new CException($gnumeric->getError());
                }

                FileHelper::removeFile($in);

            }

            if ($fh) {
                fclose($fh);
            }

            if ( !$totalNumber ) {
                $totalNumber = count ( implode( ',', $loggedErrors) );
            }
            $exportingNumber = $nextOffset + $limit;
            $exportingNumber = min($exportingNumber, $totalNumber);

            $exportingPercent = 0;
            if ($totalNumber > 0) {
                $exportingPercent =  $exportingNumber / $totalNumber * 100;
            }


            switch ($phase) {
                case 'start':
                    $nextPhase = 'extract';
                    break;
                case 'extract':
                    $nextPhase = $phase;
                    if ($nextOffset > $totalNumber) {
                        if (($exportType == LearningReportFilter::EXPORT_TYPE_XLS) && ($totalNumber > Gnumeric::MAX_ROWS_XLS)) {
                            $exportType = LearningReportFilter::EXPORT_TYPE_CSV;
                            $nextPhase = 'download';
                            $forceCsvMaxRows = true;
                        }
                        else {

                            $gnumeric = new Gnumeric();
                            $check = $gnumeric->checkGnumeric();
                            if (!$check) {
                                $forceCsvMissingGnumeric = true;
                                $exportType = LearningReportFilter::EXPORT_TYPE_CSV;
                                $nextPhase = 'download';
                            }
                            else {
                                $nextPhase = $exportType == LearningReportFilter::EXPORT_TYPE_CSV ?  'download' : 'convert';
                            }
                        }
                    }
                    break;
                case 'convert':
                    $nextPhase = 'download';
                    break;
            }

            $stop = ($nextPhase == 'download') || ($totalNumber <= 0);

            $html .= $this->renderPartial('_progressive_export_logs', array(
                'reportTitle' 		=> $reportTitle,
                'totalNumber'		=> $totalNumber,
                'exportingNumber'	=> $exportingNumber,
                'exportingPercent'	=> $exportingPercent,
                'fileName'			=> $fileName,
                'nextPhase'			=> $nextPhase,
                'exportType'		=> $exportType,
                'forceCsvMaxRows'	=> $forceCsvMaxRows,
                'forceCsvMissingGnumeric' => $forceCsvMissingGnumeric,
            ),true,true);

            $data = array(
                'offset'		=> $nextOffset,
                'stop'			=> $stop,
                'fileName'		=> $csvFileName,
                'totalNumber'	=> $totalNumber,
                'exportType'	=> $exportType,
                'phase'			=> $nextPhase,
                'custom_filter' => $customFilter,
                'logId'         => $logId,
            );

            $response = new AjaxResult();
            $response->setStatus(true);
            $response->setHtml($html)->setData($data)->toJSON();
            Yii::app()->end();


        } catch (CException $e) {
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $response->setStatus(false)->setMessage($e->getMessage());
            $response->toJSON();
            Yii::app()->end();
        }

    }

    /**
     * Based on the passed $type log, get the log from salesforce_log (_data_) column
     *
     * @param string     $type_log (_users, accounts, contacts_)
     *
     * @return array
     * @throws CException
     */
    public function getSalesforceLog( $idLog )	{
        return SalesforceLog::getLastUsersLog( $idLog );
    }

    /**
     * @throws CException
     */
    public function actionClearConfig() {
    	$confirmYes = Yii::app()->request->getParam('confirm_yes', null);
    	$accountId 	= Yii::app()->request->getParam('account', null);

    	if ( $confirmYes === 'Yes' && $accountId ) {
    		/* Delete all Salesforce related data */
    		SalesforceLmsAccount::clearConfig( $accountId );

    		echo '<a class="auto-close"></a>';

            $redirectUrl = Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index');
            $this->renderPartial('lms.protected.views.site._js_redirect', array(
                'url'           => $redirectUrl,
                'showMessage'   => false
            ));

            Yii::app()->end();
    	}

    	 $this->renderPartial('clearConfig', array(),false, true);
    	 Yii::app()->end();
    }

    /**
     * Render CATALOGS grid views' "Avail items" column (courses and plans)
     *
     * @param array $data
     * @param integer $index
     */
    protected function renderAvailableCoursesColumn($data, $index) {
        $courses = LearningCatalogue::getCatalogCoursesListV2($data['idCatalogue'], array(LearningCourse::TYPE_ELEARNING, LearningCourse::TYPE_CLASSROOM, LearningCourse::TYPE_WEBINAR));
        $plans   = LearningCatalogue::getCatalogPlansList($data['idCatalogue']);
        $count = count($courses) + count($plans);
        echo $count;
    }

	/**
	 * Activates or deactivates the users
	 */
	public function actionAxToggleUsers()
	{
		$this->_toggleUsers(SalesforceLmsAccount::SYNC_USER_TYPE_USERS);
	}

	/**
	 * Activates or deactivates the contacts
	 */
	public function actionAxToggleContacts()
	{
		$this->_toggleUsers(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS);
	}

	protected function _toggleUsers($type)
	{
		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		$active = Yii::app()->db->createCommand("
            SELECT sync_user_types
            FROM " . SalesforceLmsAccount::model()->tableName() . "
            WHERE id_account = " . $sfLmsAccount->id_account
		)->queryScalar();
		if (empty($active)) {
			Yii::app()->db->createCommand("
                UPDATE " . SalesforceLmsAccount::model()->tableName() . "
                SET sync_user_types = '" . $type . "'
                WHERE id_account = " . $sfLmsAccount->id_account
			)->execute();
		} else {
			$items = explode(",", $active);
			if (in_array($type, $items)) {
				for ($i = 0; $i < count($items); $i++) {
					if ($items[$i] == $type) {
						unset($items[$i]);
					}
				}
			} else {
				$items[] = $type;
			}
			Yii::app()->db->createCommand("
                UPDATE " . SalesforceLmsAccount::model()->tableName() . "
                SET sync_user_types = '" . implode(',', $items) . "'
                WHERE id_account = " . $sfLmsAccount->id_account
			)->execute();
		}

		//send updated blocks content to the client
		$output = array(
			'success' => true,
		);

		$params = $this->_getConfigParams();
		if ($params['salesforceModelCount'] > 0 && isset($params['salesforceLmsAccount'])) {

			$output['box_1'] = $this->renderPartial('config/_box_1', array(
				'blockReady' => $params['block1Ready'],
				'enabledUserFilter' => $params['enabledUserFilter'],
				'enabledContactFilter' => $params['enabledContactFilter']
			), true);

			$output['box_2'] = $this->renderPartial('config/_box_2', array(
				'blockReady' => $params['block2Ready'],
				'blockActive' => $params['block2Active'],
				'salesforceLmsAccount' => $params['salesforceLmsAccount'],
				'enabledUserFilter' => $params['enabledUserFilter'],
				'enabledContactFilter' => $params['enabledContactFilter'],
				'usersCompleted' => $params['usersCompleted'],
				'contactsCompleted' => $params['contactsCompleted'],
				'usersButtonText' => $params['usersButtonText'],
				'contactsButtonText' => $params['contactsButtonText'],
				'showSyncUsersButton' => $params['showSyncUsersButton'],
				'syncUsersButtonText' => $params['syncUsersButtonText'],
				'usersLog' => $params['usersLog'],
                'salesforceModel' => $sfLmsAccount
			), true);

			$output['box_3'] = $this->renderPartial('config/_box_3', array(
				'blockReady' => $params['block3Ready'],
				'blockActive' => $params['block3Active'],
				'salesforceLmsAccount' => $params['salesforceLmsAccount'],
				'coursesCompleted' => $params['coursesCompleted'],
				'coursesButtonText' => $params['coursesButtonText'],
				'showSyncCoursesButton' => $params['showSyncCoursesButton'],
				'coursesLog' => $params['coursesLog']
			), true);

		}

		echo CJSON::encode($output);
		Yii::app()->end();

		/*
		$redirectUrl = Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index');
		$this->renderPartial('lms.protected.views.site._js_redirect', array(
			'url' => $redirectUrl,
			'showMessage' => false
		));
		Yii::app()->end();
		*/
	}

    /**
     * @param bool $usersActivated
     * @param bool $usersCompleted
     * @param bool $contactsActivated
     * @param bool $contactsCompleted
     * @return bool
     */
    public function checkGlobalUserSettings($usersActivated, $usersCompleted, $contactsActivated, $contactsCompleted){
        // Check 1: either $usersActivated or $contactsActivated must be true
        if(!$usersActivated && !$contactsActivated) return false;

        // Check 2: if $usersActivated is true, then $usersCompleted must be true as well
        if($usersActivated){
            if(!$usersCompleted) return false;
        }

        // Check 3: if $contactsActivated is true, then $contactsCompleted must be true as well
        if($contactsActivated){
            if(!$contactsCompleted) return false;
        }

        // If none of the conditions above fail, return true
        return true;
    }

    /**
     * Returns the correct value for the user synchronisation button, depending on the settings
     * @param bool $usersActivated
     * @param bool $contactsActivated
     * @return string
     */
    public function getSyncUsersButtonText($usersActivated, $contactsActivated){
        if($usersActivated && $contactsActivated){
            return Yii::t('salesforce', 'Sync users and contacts');
        } elseif($usersActivated && !$contactsActivated){
            return Yii::t('salesforce', 'Sync users');
        } elseif(!$usersActivated && $contactsActivated){
            return Yii::t('salesforce', 'Sync contacts');
        } else {
            return false;
        }
    }

    /**
     * Splits the supplied array into 3 subarrays to use as options in a select
     * @param array $picklistStructure
     * @param object $listviewModel
     * @return array
     */
    public function getHierarchySelects($picklistStructure, $listviewModel){
        $selects = array(
            'level1' => array(
                0 => Yii::t('salesforce', 'Field level 1')
            ),
            'level2' => array(
                0 => Yii::t('salesforce', 'Field level 2')
            ),
            'level3' => array(
                0 => Yii::t('salesforce', 'Field level 3')
            ),
            'selected1' => false,
            'selected2' => false,
            'selected3' => false
        );
        foreach($picklistStructure as $lvl1){
            $selects['level1'][$lvl1['fieldname']] = $lvl1['label'];
            if($lvl1['fieldname'] == $listviewModel->hierarchy1){
                $selects['selected1'] = $lvl1['fieldname'];
            }
            if(!empty($lvl1['dependencies'])){
                foreach($lvl1['dependencies'] as $lvl2){
                    $selects['level2'][$lvl1['fieldname']."|".$lvl2['fieldname']] = $lvl2['label'];
                    if($lvl1['fieldname']."|".$lvl2['fieldname'] == $listviewModel->hierarchy2){
                        $selects['selected2'] = $lvl1['fieldname']."|".$lvl2['fieldname'];
                    }
                    if(!empty($lvl2['dependencies'])){
                        foreach($lvl2['dependencies'] as $lvl3){
                            $selects['level3'][$lvl1['fieldname']."|".$lvl2['fieldname']."|".$lvl3['fieldname']] = $lvl3['label'];
                            if($lvl1['fieldname']."|".$lvl2['fieldname']."|".$lvl3['fieldname'] == $listviewModel->hierarchy3){
                                $selects['selected3'] = $lvl1['fieldname']."|".$lvl2['fieldname']."|".$lvl3['fieldname'];
                            }
                        }
                    }
                }
            }
        }
        return $selects;
    }

	public function actionAxFieldRemoveConfirm()
	{
		if (Yii::app()->request->isAjaxRequest) {
			if (Yii::app()->request->getParam('remove-field-confirm', 0) > 0) {
				$this->sendJSON(array());
			}
			$this->sendJSON(array(
				'html' => $this->renderPartial('delete', null, true)
			));
		}
	}

//    public function actionAxToggleForceChangePasswords(){
//        $accountId = Yii::app()->request->getParam('accountId', null);
//        if(!$accountId){
//            Yii::log('Could not find active Salesforce account while trying to update force change passwords (No account ID submitted)');
//        } else {
//            $accountModel = SalesforceLmsAccount::model()->findByPk($accountId);
//            if(empty($accountModel)){
//                Yii::log('Could not find active Salesforce account while trying to update force change passwords (No account model found)');
//            } else {
//                $accountModel->force_change_password = ($accountModel->force_change_password == 1)?0:1;
//                $accountModel->force_change_password_updated = 1;
//                $accountModel->save();
//            }
//        }
//    }


	public function actionSetupCoursesProgress() {

		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		if ($rq->isAjaxRequest) {

			try {

				$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();

				// If no account has been set up before, redirect to main page
				if(empty($sfLmsAccount)) {
					$this->sendJSON(array(
						'success' => false,
						'redirect' => Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index')
					));
					exit;
				}


				$errors = array();

				$selectionType = Yii::app()->request->getParam('course_selection');
				if($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL){
					$sfLmsAccount->selected_items = $selectionType;
				}
				$selectedItems = $sfLmsAccount->getSelectedItems();

				$selectedCounter = count($selectedItems['courses']) + count($selectedItems['plans']);

				$replicate_products = Yii::app()->request->getParam('replicate_products');
				$issue_so = Yii::app()->request->getParam('issue_so');
				if($replicate_products == SalesforceLmsAccount::TYPE_REPLICATE_NONE){
					$sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_NONE;
				} else {
					if(!$issue_so){
						$sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_COURSES;
					} else {
						$sfLmsAccount->replicate_courses_as_products = SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS;
					}
				}

				if(in_array($selectionType, array(SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES, SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS)) && ($sfLmsAccount->selected_items == null)){
					$errors[] = Yii::t('salesforce', 'Select at least 1 category or catalog.');
				} elseif($selectedCounter == 0){
					$errors[] = Yii::t('salesforce', 'You have no matching courses for your selection. Please broaden your selection or add courses to your existing categories or catalogs.');
				}

				if(empty($errors)) {

					//only a single process per time is allowed so we will check if a job is already running
					$alreadyExisting = SalesforceRunningProcess::model()->findByAttributes(array(
						'process_name' => 'setup_courses',
						'status' => array(SalesforceRunningProcess::STATUS_NOT_STARTED, SalesforceRunningProcess::STATUS_RUNNING)
					));
					if (!empty($alreadyExisting)) {
						//throw new CException('Multiple setup processes cannot be executed simultaneously');
						//TODO: find a better way to manage this situation
					}

					//check all the sub-steps of the process
					$subProcesses = array();
					switch ($sfLmsAccount->replicate_courses_as_products) {
						case SalesforceLmsAccount::TYPE_REPLICATE_NONE:
							$subProcesses[] = array('key' => 'CO1', 'status' => 'not started', 'order' => 0);
							$subProcesses[] = array('key' => 'CO2', 'status' => 'not started', 'order' => 1);
							break;
						case SalesforceLmsAccount::TYPE_REPLICATE_COURSES:
							$subProcesses[] = array('key' => 'CO1', 'status' => 'not started', 'order' => 0);
							$subProcesses[] = array('key' => 'CO2', 'status' => 'not started', 'order' => 1);
							$subProcesses[] = array('key' => 'ProductFamily', 'status' => 'not started', 'order' => 2);
							$subProcesses[] = array('key' => 'PriceBook', 'status' => 'not started', 'order' => 3);
							break;
						case SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS:
							$subProcesses[] = array('key' => 'CO1', 'status' => 'not started', 'order' => 0);
							$subProcesses[] = array('key' => 'CO2', 'status' => 'not started', 'order' => 1);
							$subProcesses[] = array('key' => 'ProductFamily', 'status' => 'not started', 'order' => 2);
							$subProcesses[] = array('key' => 'PriceBook', 'status' => 'not started', 'order' => 3);
							$subProcesses[] = array('key' => 'OrderType', 'status' => 'not started', 'order' => 4);
							break;
					}

					//if no process is running yet,
					$process = new SalesforceRunningProcess();
					$process->process_name = 'setup_courses';
					$process->start_date = Yii::app()->localtime->getLocalNow();
					$process->process_data = CJSON::encode(array('subprocesses' => $subProcesses));
					$process->user_id = Yii::app()->user->id;
					$process->save();

					//save settings and start process
					$sfLmsAccount->save();

					/* @var $scheduler SchedulerComponent */
					$scheduler = Yii::app()->scheduler;
					$jobParams = array(
						'process_id' => $process->getPrimaryKey(),
						'running_operation' => $subProcesses[0]['key']
					);
					$job = $scheduler->createImmediateJob(SetupCoursesJobHandler::JOB_NAME, SetupCoursesJobHandler::id(), $jobParams);
					if (!$job) {
						throw new CException('Error(s) while creating external job');
					}

				} else {

					throw new CException('Error while setting Salesforce account options');
				}

			} catch(Exception $e) {

				$this->sendJSON(array(
					'success' => false,
					'message' => $e->getMessage()
				));
			}

			//if we arrive here, than all is ok until now
			$uniqueDialogId = $rq->getParam('unique-dialog-id', 0);
			$this->sendJSON(array(
				'success' => true,
				'html' => $this->renderPartial('setupCoursesProgress', array(
					'uniqueDialogId' => $uniqueDialogId,
					'process' => $process,
					'steps' => $subProcesses
				), true)
			));

		}
	}



	public function actionSetupCoursesProgressCheckStatus() {

		$rq = Yii::app()->request;

		if (!$rq->isAjaxRequest) {
			//only ajax-polling is allowed
			return;
		}

		//prepare output
		$output = array(
			'success' => false,
			'message' => ''
		);

		//start effective checking
		try {

			$idProcess = $rq->getParam('process_id', 0);
			if ($idProcess <= 0) {
				throw new CException('Invalid specified process');
			}

			//retrieve process info from database
			$process = SalesforceRunningProcess::model()->findByPk($idProcess);
			if (empty($process)) {
				throw new CException('Invalid specified process');
			}
			$processData = CJSON::decode($process->process_data);
			if (empty($processData) || !isset($processData['subprocesses']) || empty($processData['subprocesses']) || !is_array($processData['subprocesses'])) {
				throw new CException('Invalid process data');
			}
			$subProcesses = $processData['subprocesses'];
			$output['success'] = true;
			$output['subprocessesStatuses'] = array();
			foreach ($subProcesses as $subProcess) {
				$output['subprocessesStatuses'][$subProcess['key']] = $subProcess['status'];
			}

		} catch(Exception $e) {
			$output['success'] = false;
			$output['message'] = $e->getMessage();
		}
		$this->sendJSON($output);
	}


    /*******  TEMPORARY FUNCTION - CAN BE DELETED AS SOON AS DEVELOPMENT IS FULLY TESTED  ******/
    public static function getDummyPicklistStructure(){
        return array(
            'field1' => array(
                'fieldname' => 'field1',
                'label' => 'Field 1',
                'dependencies' => array(
                    'field11' => array(
                        'fieldname' => 'field11',
                        'label' => 'Field 1.1',
                        'dependencies' => array()
                    )
                )
            ),
            'field2' => array(
                'fieldname' => 'field2',
                'label' => 'Field 2',
                'dependencies' => array(
                    'field21' => array(
                        'fieldname' => 'field21',
                        'label' => 'Field 2.1',
                        'dependencies' => array(
                            'field211' => array(
                                'fieldname' => 'field211',
                                'label' => 'Field 2.1.1',
                                'dependencies' => array()
                            )
                        )
                    )
                )
            ),
            'field3' => array(
                'fieldname' => 'field3',
                'label' => 'Field 3',
                'dependencies' => array(
                    'field31' => array(
                        'fieldname' => 'field31',
                        'label' => 'Field 3.1',
                        'dependencies' => array(
                            'field311' => array(
                                'fieldname' => 'field311',
                                'label' => 'Field 3.1.1',
                                'dependencies' => array()
                            ),
                            'field312' => array(
                                'fieldname' => 'field312',
                                'label' => 'Field 3.1.2',
                                'dependencies' => array()
                            ),
                            'field313' => array(
                                'fieldname' => 'field313',
                                'label' => 'Field 3.1.3',
                                'dependencies' => array()
                            )
                        )
                    ),
                    'field32' => array(
                        'fieldname' => 'field32',
                        'label' => 'Field 3.2',
                        'dependencies' => array(
                            'field321' => array(
                                'fieldname' => 'field321',
                                'label' => 'Field 3.2.1',
                                'dependencies' => array()
                            )
                        )
                    ),
                    'field33' => array(
                        'fieldname' => 'field33',
                        'label' => 'Field 3.3',
                        'dependencies' => array(
                            'field331' => array(
                                'fieldname' => 'field331',
                                'label' => 'Field 3.3.1',
                                'dependencies' => array()
                            ),
                            'field332' => array(
                                'fieldname' => 'field332',
                                'label' => 'Field 3.3.2',
                                'dependencies' => array()
                            ),
                            'field333' => array(
                                'fieldname' => 'field333',
                                'label' => 'Field 3.3.3',
                                'dependencies' => array()
                            ),
                            'field334' => array(
                                'fieldname' => 'field334',
                                'label' => 'Field 3.3.4',
                                'dependencies' => array()
                            )
                        )
                    ),
                    'field34' => array(
                        'fieldname' => 'field34',
                        'label' => 'Field 3.4',
                        'dependencies' => array()
                    )
                )
            ),
            'field4' => array(
                'fieldname' => 'field4',
                'label' => 'Field 4',
                'dependencies' => array(
                    'field41' => array(
                        'fieldname' => 'field41',
                        'label' => 'Field 4.1',
                        'dependencies' => array()
                    ),
                    'field42' => array(
                        'fieldname' => 'field42',
                        'label' => 'Field 4.2',
                        'dependencies' => array()
                    ),
                    'field43' => array(
                        'fieldname' => 'field43',
                        'label' => 'Field 4.3',
                        'dependencies' => array()
                    ),
                    'field44' => array(
                        'fieldname' => 'field44',
                        'label' => 'Field 4.4',
                        'dependencies' => array()
                    )
                )
            ),
            'field5' => array(
                'fieldname' => 'field5',
                'label' => 'Field 5',
                'dependencies' => array()
            ),
            'field6' => array(
                'fieldname' => 'field6',
                'label' => 'Field 6',
                'dependencies' => array()
            ),
            'field7' => array(
                'fieldname' => 'field7',
                'label' => 'Field 7',
                'dependencies' => array()
            ),
            'field8' => array(
                'fieldname' => 'field8',
                'label' => 'Field 8',
                'dependencies' => array()
            ),

        );
    }



	protected function _getConfigParams() {
		$output = array();

		$salesforceModelCount = SalesforceLmsAccount::model()->count();
		$output['salesforceModelCount'] = $salesforceModelCount;

		if ($salesforceModelCount <= 0){
			return $output;
		}

		$salesforceModel = SalesforceLmsAccount::getFirstAccount();
		$modelId = $salesforceModel->id_account;

		$output['salesforceModel'] = $salesforceModel;
		$output['salesforceLmsAccount'] = $salesforceModel;

		$usersLog = SalesforceLog::getLastUsersLog($modelId);
		$coursesLog = SalesforceLog::getLastCoursesLog($modelId);
		$output['usersLog'] = $usersLog;
		$output['coursesLog'] = $coursesLog;

		$allCoursesCount =  Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".LearningCourse::model()->tableName())->queryScalar();
		$selectedItems = $salesforceModel->getSelectedItems();
		$selectedItemsCount = count($selectedItems);
		$listviewAccount = $this->fetchListView($modelId, SalesforceLmsAccount::SF_TYPE_ACCOUNT);
		$output['allCoursesCount'] = $allCoursesCount;
		$output['selectedItems'] = $selectedItems;
		$output['selectedItemsCount'] = $selectedItemsCount;
		$output['listviewAccount'] = $listviewAccount;

		// Check if users or contacts have been enabled
		$enabledUserFilter = (strpos($salesforceModel->sync_user_types, SalesforceLmsAccount::SYNC_USER_TYPE_USERS) !== false);
		$enabledContactFilter = (strpos($salesforceModel->sync_user_types, SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS) !== false);
		$output['enabledUserFilter'] = $enabledUserFilter;
		$output['enabledContactFilter'] = $enabledContactFilter;

		// Check if users, contacts or courses have been properly set up
		$syncAgent = new SalesforceSyncAgent($modelId);
		$usersCompleted = $syncAgent->checkUserConfigurationSettings();
		$contactsCompleted = $syncAgent->checkContactConfigurationSettings();
		$coursesCompleted = $syncAgent->checkCourseConfigurationSettings();
		$output['usersCompleted'] = $usersCompleted;
		$output['contactsCompleted'] = $contactsCompleted;
		$output['coursesCompleted'] = $coursesCompleted;

		// Providing the button text in case user or contact setup is completed
		$output['usersButtonText'] = ($usersCompleted ? Yii::t('salesforce', 'Change settings') : Yii::t('salesforce', 'Set up now'));
		$output['contactsButtonText'] = ($contactsCompleted ? Yii::t('salesforce', 'Change settings') : Yii::t('salesforce', 'Set up now'));
		$output['coursesButtonText'] = ($coursesCompleted ? Yii::t('salesforce', 'Change settings') : Yii::t('salesforce', 'Set up now'));

		// Determine if the global user and/or contact setup is completed
		$usersOrContactsCompleted = $this->checkGlobalUserSettings($enabledUserFilter, $usersCompleted, $enabledContactFilter, $contactsCompleted);
		$output['usersOrContactsCompleted'] = $usersOrContactsCompleted;

		$output['syncUsersButtonText'] = $this->getSyncUsersButtonText($enabledUserFilter, $enabledContactFilter);
		$output['showSyncUsersButton'] = $usersOrContactsCompleted;
		$output['showSyncCoursesButton'] = $coursesCompleted;

		//Check for already started synchronisations
		$job = CoreJob::model()->find('name = :sfSync', array(':sfSync' => SalesforceSyncTask::JOB_NAME));
		$startedJobs = array();
        $userJobStarted = "";
        $courseJobStarted = "";
		if($job){
			//Delete the job if is more than 6 hours old
            $dateJob = strtotime(Yii::app()->localtime->fromLocalDateTime($job->created)) + 21600; // 6 hours
            $dateNow = strtotime(Yii::app()->localtime->getLocalNow());
			if($dateNow > $dateJob){
				$job->delete();
			} else{
				$params = json_decode($job->params);
				$startedJobs[$params->type] = $job->hash_id;
                switch($params->type){
                    case SalesforceLog::TYPE_LOG_USERS:
                        $userJobStarted = $job->hash_id;
                        break;
                    case SalesforceLog::TYPE_LOG_COURSES:
                        $courseJobStarted = $job->hash_id;
                        break;
                }
			}
		}
		$output['startedJobs'] = $startedJobs;
        $output['userJobStarted'] = $userJobStarted;
        $output['courseJobStarted'] = $courseJobStarted;

		// Fetch the number of existing SF accounts/branches, excluding the dummy account:
		$existingAccounts = Yii::app()->db->createCommand()
			->select('COUNT(o.id)')
			->from(SalesforceOrgchart::model()->tableName().' AS o')
			->join(CoreOrgChart::model()->tableName().' AS c', 'o.id_org = c.id_dir')
			->where('o.sf_lms_id = :sfLmsId AND o.sf_type = :sfType AND c.lang_code = :langCode AND c.translation <> :dummyAccount', array(
				':sfLmsId'      => $modelId,
				':sfType'       => SalesforceLmsAccount::SF_TYPE_ACCOUNT,
				':langCode'     => 'english',
				':dummyAccount' => SalesforceApiClient::DUMMY_ACCOUNT
			))
			->queryScalar();
		$output['existingAccounts'] = $existingAccounts;

		$usersAndContactsCount = Yii::app()->db->createCommand()
			->select('COUNT(id)')
			->from(SalesforceUser::model()->tableName())
			->where('sf_lms_id = :lmsAccount', array(
				':lmsAccount' => $modelId
			))
			->queryScalar();
		$output['usersAndContactsCount'] = $usersAndContactsCount;

		// Determine if block 1 is ready AND if block 2 can become active
		$block2Active = $block1Ready = (($enabledUserFilter || $enabledContactFilter) ? true : false);
		$block3Active = $block2Ready = (($block2Active && $usersOrContactsCompleted && !empty($usersLog) && ($usersAndContactsCount >= 1)) ? true : false);
		$block4Active = $block3Ready = (($block3Active && $coursesCompleted && !empty($coursesLog)) ? true : false);
		$output['block1Ready'] = $block1Ready;
		$output['block2Ready'] = $block2Ready;
		$output['block3Ready'] = $block3Ready;
		$output['block2Active'] = $block2Active;
		$output['block3Active'] = $block3Active;
		$output['block4Active'] = $block4Active;

		return $output;
	}


}