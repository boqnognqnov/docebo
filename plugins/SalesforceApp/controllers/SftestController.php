<?php

class SftestController extends Controller {
	
	
	
	public function showAccountContacts(SalesforceApiClient $client) {
		
		$accounts = $client->getAccounts();
		
		$contacts = array();
		foreach ($accounts['data'] as $account) {
			$contacts[$account['Id']] = $client->getAccountContacts($account['Id']);
		}
		
		CVarDumper::dump($contacts, 80, true);
		
	}
	
	
	public function actionTest() {

		$m1 = SalesforceLmsAccount::model()->findByPk(1);
		$m2 = SalesforceOrgchart::model()->findByPk(8);
		
		$m1->addError('agent', 'Error m1');
		$m2->addError('sf_account_id', 'Error m2');
		
		$this->render('test', array(
			'm1' => $m1,
			'm2' => $m2,
		));
		
	}
	
	public function actionIndex() {
		

		// Example: testing if credentials are good 
		// "plamendp@gmail.com", "!mandocebo13792", "Qi5nBGspdR6FOZak5gFNqqLOS"
		// "kyuchukovv@gbg.com", "!mandocebo1379", "ScZwemkL8iN8cmsZsY5Rmyx1"

	    $salesforceModel = SalesforceLmsAccount::model()->find();
	    
		$client = new SalesforceApiClient($salesforceModel);
		
		//$client = new SalesforceApiClient("someone@scavaline.com", "scava2015", "FjQKGpfl6WHvhA1DgXvFJ4Rdo");
		
		//$client = new SalesforceApiClient("plamen@petkoff.eu", "!mandocebo1379", "wrsnORKHkaZwhrCfPY6KUx8C");
		
		if ($client->hasError()) {
			echo "ERROR: " . $client->error();
		}
		else {
			//echo "SUCCESS (account login)<br>";
			
			try {
			    
			    
				//CVarDumper::dump($client->createPriceBook(), 80, true);die;
				
				CVarDumper::dump($client->test(), 80, true);die;
				
			    CVarDumper::dump(SalesforceUser::getUserType(243, 16344), 80, true);die;
			    
			    
			    //CVarDumper::dump($client->getAllProfileNames(), 80, true);die;
			    
				CVarDumper::dump($client->getConnection()->describeSObject('Profile'), 80, true);die;
				
				//CVarDumper::dump($client->getConnection()->describeSObject('PricebookEntry'), 80, true);die;
				
                //CVarDumper::dump($client->getConnection()->describeSObject('Contract'), 80, true);die;
			
				//CVarDumper::dump($client->getPicklistValues('Order', 'Type'), 80, true);die;
				//CVarDumper::dump($client->getConnection()->describeSObject('Order'), 80, true);die;
			    
			    //CVarDumper::dump($client->deleteCourseEnrollmentCustomObject(), 80, true);
			    //CVarDumper::dump($client->deleteCourseCustomObject(), 80, true);
			    			 
			    //CVarDumper::dump($client->createCourseCustomObject(), 80, true); 
			    //CVarDumper::dump($client->createEnrollmentCustomObject(), 80, true);die;
			    
			    //CVarDumper::dump($client->updateOrderTypePicklist(), 80, true);  
			    //CVarDumper::dump($client->updateProduct2FamilyPicklist(), 80, true); die;
			    
			    //CVarDumper::dump($client->createDummyAccountAndContract(), 80, true);die;
			    
			    CVarDumper::dump($client->updateCourseCustomObject(), 80, true); die;
			    //CVarDumper::dump($client->updateEnrollmentCustomObject(), 80, true); die;
			    
			
			    //CVarDumper::dump($client->getMetdataConnection()->listMetadata('CustomField'), 80, true);die;
			    
			    
			    //CVarDumper::dump($client->getCustomObjectsMetadataList(), 80, true);die;
			    //CVarDumper::dump($client->customObjectExists('DOCEBO1_CO__c'), 80, true);die;
			    
			    
			    //CVarDumper::dump($client->deleteCustomObjects(array('DOCEBO2_CO', 'DOCEBO1_CO')));die;
			    
			     
    			//CVarDumper::dump($client->getMetdataConnection()->deleteMetadata('CustomObject', array('DOCEBO1_CO__c')), 80, true);die;
	   		
                //CVarDumper::dump($client->getAccountData(), 80, true);
                //CVarDumper::dump($client->getConnection()->describeGlobal(), 80, true);
			
			
    			//CVarDumper::dump($client->getConnection()->describeSObject('TestCO__c'), 80, true);die;
	       		//CVarDumper::dump($client->getConnection()->describeSObject('User'), 80, true);
                //CVarDumper::dump($client->getConnection()->describeSObject('DatacloudDandBCompany'), 80, true); die;
                //CVarDumper::dump($client->t1Checks(), 80, true);
			
    			//CVarDumper::dump($client->getAccountData()->collectedInfo->requiredProfileId, 80, true);
			
	       		$status = $client->accountConsistencyCheck();
                if ($client->hasError()) {
				    echo "but... FAIL (has some errors after that)<br>";
				    CVarDumper::dump($client->error(), 80, true);
                }
                else {
                    echo "<hr>";
                    echo "Accounts<br>";
                    CVarDumper::dump($client->getAccounts(), 80, true);
                    
                    echo "<hr>";
                    echo "Users<br>";
                    CVarDumper::dump($client->getUsers(), 80, true);
                    
                    echo "<hr>";
                    echo "Leads<br>";
                    CVarDumper::dump($client->getLeads(), 600, true);
                    
                    echo "<hr>";
                    echo "Contacts<br>";
                    CVarDumper::dump($client->getContacts(), 80, true);
                    
                    echo "<hr>";
                    echo "Orphaned Contacts (not associated to an account)<br>";
                    CVarDumper::dump($client->getOrphanContacts(), 80, true);
                    
                    echo "<hr>";
                    echo "Account Contacts for Account ID=0012400000BqG3FAAV <br>";
                    CVarDumper::dump($client->getAccountContacts('0012400000BqG3FAAV'), 80, true);
                    
                    echo "<hr>";
                    CVarDumper::dump($client->getLoginData(), 80, true);
                }
                
			}
			catch (Exception $e) {
				throw $e;
			    CVarDumper::dump($e->getMessage(), 80, true);
			}
			
		}

		die;

		
		//CVarDumper::dump($client->getConnection()->getSessionId(), 80, true);
		
		
		
	}
	
	public function actionCalculate(){
		$idUser = 61073;
		$idPath = 84;

		$test = Yii::app()->db->createCommand("
            SELECT

        ")->queryAll();

		if(!empty($test)){
			$countCompleted = 0;
			$countAll = 0;
			foreach($test as $t){
				$countCompleted += $t['courseObjectsCompleted'];
				$countAll += $t['courseObjectsTotal'];
			}

			var_dump($countCompleted,$countAll);
		}
	}

	public function actionGetFirstLoCompletionTime(){
		$idUser = 61079;
		$idPath = 83;

		$firstAttempt = Yii::app()->db->createCommand("
			SELECT lct.firstAttempt
			FROM ".LearningCommontrack::model()->tableName()." AS lct
			INNER JOIN ".LearningOrganization::model()->tableName()." AS lo ON lct.idReference = lo.idOrg
			WHERE lct.idUser = ".$idUser." AND lo.idCourse IN (
				SELECT lpc.id_item
				FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
				WHERE lpc.id_path = ".$idPath."
			)
			ORDER BY lct.firstAttempt
			LIMIT 1
		")->queryScalar();

		if(!empty($firstAttempt)) {
			$test[] = $firstAttempt;
			$test[] = Yii::app()->localtime->toLocalDateTime($firstAttempt, 'SHORT', 'MEDIUM', DATE_ISO8601);
			$test[] = Yii::app()->localtime->toUTC($firstAttempt, DATE_ISO8601);
			$test[] = substr(Yii::app()->localtime->toLocalDateTime($firstAttempt, 'SHORT', 'MEDIUM', DATE_ISO8601), 0, 19)."+0000";
			$test[] = substr(Yii::app()->localtime->toUTC($firstAttempt, DATE_ISO8601), 0, 19)."+0000";
			$test[] = str_replace(" ", "T", trim($firstAttempt))."+0000";
		} else {
			$test = Yii::app()->localtime->getUTCNow();
		}

		var_dump($firstAttempt, $test);
	}

	public function actionFetchUsersEnrolledInLearningPlan(){
		$idPath = 84;

		$users = Yii::app()->db->createCommand("
			SELECT sfu.id_user_lms AS idUser
			FROM ".SalesforceUser::model()->tableName()." AS sfu
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lcu ON sfu.id_user_lms = lcu.idUser
			WHERE lcu.id_path = ".$idPath."
		")->queryAll();

		var_dump($users);

		$event = new DEvent();
		$event->params['user'] = CoreUser::model()->findByAttributes(array('idst' => 61073));
		$event->params['course'] = LearningCourse::model()->findByAttributes(array('idCourse' => 574));

		var_dump($event);
	}

	public function actionGetAllUsersAssignedToCourse(){
		$courseId = 595;

		$result = array();
		$enrollments = Yii::app()->db->createCommand("
			SELECT lpc.id_path as idPath, lpu.idUser
			FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lpu ON lpc.id_path = lpu.id_path
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lpu.idUser = sfu.id_user_lms
			WHERE lpc.id_item = ".$courseId."
		")->queryAll();

		foreach($enrollments as $enrollment){
			$result[$enrollment['idUser']][] = $enrollment['idPath'];
		}
		//var_dump($enrollments);
		var_dump($result);
	}

	public function actionGetAllUsersAssignedToUncompletedCourse(){
		$courseId = 602;

		$users = Yii::app()->db->createCommand("
			SELECT lcu.idUser, lcu.status
			FROM ".LearningCourseuser::model()->tableName()." AS lcu
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lcu.idUser = sfu.id_user_lms
			WHERE lcu.idCourse = ".$courseId." AND lcu.status <> ".LearningCourseuser::$COURSE_USER_END."
		")->queryAll();

		var_dump($users);
	}

	public function actionGetLearningPlanEnrollmentsForCourse(){
		$courseId = 603;

		$enrollments = Yii::app()->db->createCommand("
			SELECT lpc.id_path AS idPath, lpu.idUser
			FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lpu ON lpc.id_path = lpu.id_path
			INNER JOIN ".SalesforceUser::model()->tableName()." AS sfu ON lpu.idUser = sfu.id_user_lms
			WHERE lpc.id_item = ".$courseId."
		")->queryAll();

		var_dump($enrollments);
	}

	public function actionGetLearningPlanEnrollmentsForUsers(){
		$users = array(61069, 61070, 61071, 61072, 61073, 61078, 61082);
		$idCourse = 602;

		$sql = "
			SELECT lpc.id_path as idPath, lpu.idUser
			FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
			INNER JOIN ".LearningCoursepathUser::model()->tableName()." AS lpu ON lpc.id_path = lpu.id_path
			WHERE lpc.id_item = ".$idCourse." AND lpu.idUser IN (".implode(",", $users).")
		";

		$enrollments = Yii::app()->db->createCommand($sql)->queryAll();

		var_dump($sql, $enrollments);
	}

	public function actionGetStartTime(){
		$idUser = 98232;
		$idCourse = 595;
		$idPlan = 82;
		$startTime = Yii::app()->db->createCommand("
			SELECT lts.enterTime
			FROM ".LearningTracksession::model()->tableName()." AS lts
			WHERE idUser = ".$idUser." AND idCourse IN (
				SELECT id_item
				FROM ".LearningCoursepathCourses::model()->tableName()." AS lpc
				WHERE lpc.id_path = ".$idPlan."
			)
			ORDER BY enterTime ASC
			LIMIT 1
		")->queryScalar();

		var_dump($startTime);
	}

	public function actionGetUserBranchInfo(){
		//$userId = "/kurt@celtis.net";
		$userId = "/efrank@genepoint.com";

		$result = Yii::app()->db->createCommand("
			SELECT cu.firstname, cu.lastname, cu.idst AS userId, cgm.idst AS idstGroup, cot.idOrg, so.sf_account_id, so.sf_type, su.id_user_sf
			FROM ".CoreUser::model()->tableName()." AS cu
			INNER JOIN ".CoreGroupMembers::model()->tableName()." AS cgm ON cu.idst = cgm.idstMember
			INNER JOIN ".CoreOrgChartTree::model()->tableName()." AS cot ON cgm.idst = cot.idst_oc
			INNER JOIN ".SalesforceOrgchart::model()->tableName()." AS so ON cot.idOrg = so.id_org
			INNER JOIN ".SalesforceUser::model()->tableName()." AS su ON cu.idst = su.id_user_lms
			WHERE cu.userid = '".$userId."' AND so.sf_type IS NOT NULL
		")->queryRow();

		var_dump($result);
	}

	public function actionGetUserId(){
		$username = "/efrank@genepoint.com";
		$userId = Yii::app()->db->createCommand()
			->select('idst')
			->from(CoreUser::model()->tableName())
			->where('userid = :userId', array(
				':userId' => $username
			))
			->queryScalar();
		var_dump($userId);
	}

	public function actionTestPicklistDependencies(){
		$sfLmsAccount = SalesforceLmsAccount::getFirstAccount();
		$api = new SalesforceApiClient($sfLmsAccount);
		$response = $api->getConnection()->describeSObject('Account');
		$countries = $response->fields[112]->picklistValues;
		foreach($countries as $i => $country) {
			$orig = $country->validFor;
			$b64 = base64_encode($orig);
			$hex = $this->strToHex($b64);
			var_dump($i.". ".$country->label." - ".$orig." - ".$b64." - ".$hex);
		}

		var_dump($countries);
	}

	public function strToHex($string)
	{
		$hex='';
		for ($i=0; $i < strlen($string); $i++)
		{
			$hex .= dechex(ord($string[$i]));
		}
		return $hex;
	}

	public function actionFetchData(){
		$results = Yii::app()->db->createCommand("
			SELECT
			CONCAT('C',lcu.idCourse,'_',lcu.idUser) AS enroll_id,
			CONCAT('C',lcu.idCourse) AS course_id,
			NULL AS product_id,
			sfu.id_user_sf AS user_id,
			sfu.sf_type AS user_type,
			lcu.status,
			NULL AS so_id,
			( SELECT lct2.score
				FROM learning_commontrack AS lct2
				INNER JOIN learning_organization AS lo4 ON lct2.idReference = lo4.idOrg
				WHERE lct2.idUser = lcu.idUser AND lo4.idCourse = lcu.idCourse AND lct2.score IS NOT NULL AND lct2.score_max IS NOT NULL
				ORDER BY lct2.last_complete ASC
				LIMIT 1
            ) AS score,
            ( SELECT lct3.score_max
				FROM learning_commontrack AS lct3
				INNER JOIN learning_organization AS lo5 ON lct3.idReference = lo5.idOrg
				WHERE lct3.idUser = lcu.idUser AND lo5.idCourse = lcu.idCourse AND lct3.score IS NOT NULL AND lct3.score_max IS NOT NULL
				ORDER BY lct3.last_complete ASC
				LIMIT 1
            ) AS score_max,
            csu.value AS timezone,
            NULL AS event_id,
            NULL AS task_id,
			lcu.date_inscr AS enroll_datetime,
			lcu.date_complete AS completed_datetime,
			NULL AS unenrolled_datetime,
			lcu.date_first_access AS first_access,
			lcu.date_last_access AS last_access,
			ROUND((
                SELECT COUNT(lct.idReference)
                FROM learning_commontrack AS lct
                WHERE lct.idUser = lcu.idUser AND lct.status = 'completed' AND lct.idReference IN (
                    SELECT lo3.idOrg
                    FROM learning_organization AS lo3
                    WHERE lo3.idParent = lo1.idOrg
                )
            ) / (
                SELECT COUNT(lo2.idOrg)
                FROM learning_organization AS lo2
                WHERE lo2.idCourse = lcu.idCourse AND lo2.idParent > 0
            ) * 100) AS completion_pct,
            NULL AS path_completion,
            ( SELECT SUM(UNIX_TIMESTAMP(lts.lastTime) - UNIX_TIMESTAMP(lts.enterTime)) AS TimeInCourse
				FROM learning_tracksession AS lts
				WHERE lts.idCourse = lcu.idCourse AND lts.idUser = lcu.idUser
				AND UNIX_TIMESTAMP(lts.lastTime) > UNIX_TIMESTAMP(lts.enterTime)
				AND lts.lastTime != '0000-00-00 00:00:00' AND lts.lastTime IS NOT NULL
				AND lts.enterTime != '0000-00-00 00:00:00' AND lts.enterTime IS NOT NULL
            ) AS time_in_course,
            sfu.id_user_sf AS enrollment_contact,
            NULL AS enrollment_order,
            NULL AS enrollment_product
			FROM learning_courseuser AS lcu
			INNER JOIN salesforce_user AS sfu ON lcu.idUser = sfu.id_user_lms
			INNER JOIN learning_organization AS lo1 ON lcu.idCourse = lo1.idCourse
			LEFT JOIN core_setting_user AS csu ON sfu.id_user_lms = csu.id_user AND csu.path_name = 'timezone'
			GROUP BY lcu.idUser, lcu.idCourse
		")->queryAll();
		var_dump($results);
	}

	public function actionTestImportedUsers(){
		$users = Yii::app()->db->createCommand("SELECT id_user_lms FROM salesforce_user")->queryAll();
		$newUsers = array();
		foreach($users as $user) {
			$newUsers[] = $user['id_user_lms'];
		}
		$tmpBranches = Yii::app()->db->createCommand("SELECT id_org FROM salesforce_orgchart")->queryAll();
		$branches = array();
		foreach($tmpBranches as $branch){
			$branches[] = $branch['id_org'];
		}
		$params = array(
			'users' => $newUsers,
			'branches' => $branches
		);
		Yii::app()->event->raise(EventManager::EVENT_MASS_IMPORT_BRANCH_USERS, new DEvent($this, $params));
	}

	public function actionTestSettings(){
		var_dump('memory 1: '.ini_get('memory_limit'));
		ini_set('memory_limit', '1024M');
		var_dump('memory 2: '.ini_get('memory_limit'));
	}

	public function actionQuery(){

		$qry1 = "
                SELECT su.id_user_sf/*, so.id_org*/
                FROM " . SalesforceUser::model()->tableName() . " AS su
                /*INNER JOIN " . CoreGroupMembers::model()->tableName() . " as cgm ON su.id_user_lms = cgm.idstMember
                INNER JOIN " . CoreOrgChartTree::model()->tableName() . " AS cot ON cgm.idst = cot.idst_oc
                INNER JOIN " . SalesforceOrgchart::model()->tableName() . " AS so ON cot.idOrg = so.id_org*/
                WHERE su.sf_type = 'contact'
            ";


		$start = microtime(true);
		$userBranches = Yii::app()->db->createCommand($qry1)->queryAll();
		$end = microtime(true);
		print $this->getDiff($start, $end);
	}

	public function getDiff($start, $end){
		return round(($end-$start)*1000)."ms";
	}

	public function actionCreateCsv(){
		$accounts = 500;
		$contactsPerAccount = 7;
		$this->logMemoryUsage();
		for($i = 1; $i <= $accounts; $i++){
			for($j = 1; $j <= $contactsPerAccount; $j++){
				print ".";
			}
		}
		$this->logMemoryUsage();
	}


	public function logMemoryUsage(){
		$start = microtime(true);
		sleep(2);
		$end = microtime(true);
		$diff = $end - $start;
		$maxMemory = ini_get('memory_limit') * 1024 * 1024;
		$usedMemory = memory_get_usage();
		var_dump("Memory used: ".$usedMemory." / ".$maxMemory." bytes (".round(($usedMemory/$maxMemory)*100, 2)."%). Script executed in ".$diff);
	}

	public function actionGetJob(){
		$jobStarted = CoreJob::model()->find('name = :name', array(
			':name' => SalesforceSyncTask::JOB_NAME
		));
		$params = CJSON::decode($jobStarted->params);
		$x1 = (isset($params['firstJobId']))?$params['firstJobId']:$jobStarted->hash_id;
		$x2 = $params['firstJobId'];
		$x3 = $jobStarted->hash_id;
		var_dump($x1, $x2, $x3);
		var_dump($params['id_account']);
		var_dump($jobStarted);
	}

	public function actionLastScore(){
		$idCourse = 693;
		$idUser = 51417;
		$scoreInfo = LearningCourseuser::getLastScoreByUserAndCourse($idCourse, $idUser, true, true);
		var_dump($scoreInfo);
	}

	public function actionTestForceChangePassword(){
//		$updates = Yii::app()->db->createCommand("
//			UPDATE ".CoreUser::model()->tableName()." AS cu
//			INNER JOIN ".SalesforceUser::model()->tableName()." AS su ON cu.idst = su.id_user_lms
//			SET force_change = '2'
//			WHERE cu.lastenter IS NULL AND su.sf_type = 'contact'
//		")->execute();
//
//		var_dump('updates done: ', $updates);

		$test = Yii::app()->db->createCommand("
			SELECT id_user_lms, id_user_sf, userid, sf_type, force_change, lastenter
			FROM ".SalesforceUser::model()->tableName()." AS su
			INNER JOIN ".CoreUser::model()->tableName()." AS cu ON su.id_user_lms = cu.idst
			WHERE cu.lastenter IS NULL
		")->queryAll();

		var_dump($test);
	}
}