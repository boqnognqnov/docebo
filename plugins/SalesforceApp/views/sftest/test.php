<?php
/* @var $form  CActiveForm */


$form = $this->beginWidget('CActiveForm',
	array(
		'id' => 'webinar-account-settings-form',
		'htmlOptions' => array(
			'class' => 'webinar-account-settings-form'
		)
	)
);

	// errors
	echo $form->errorSummary(array($m1, $m2));

	// M1
	echo $form->textField($m1, 'agent');
	echo $form->textField($m1, 'token');

	echo "<hr>";
	
	// M2
	echo $form->textField($m2, 'sf_account_id');
	echo $form->textField($m2, 'sf_type');

	echo "<hr>";
	
	echo Chtml::submitButton('Submit');

$this->endWidget();