<h1><?= Yii::t('salesforce', 'Select courses to synchronize'); ?></h1>
<?php
echo CHtml::beginForm('', 'POST', array(
	'class'	=> 'ajax',
	'id'	=> 'selectCourseCategories'
));

echo CHtml::hiddenField('selected_catalogs');

echo CHtml::hiddenField('catalogs_from_db', implode(',', $selectedIds), array('class' => 'selected-ids'));
echo Chtml::hiddenField('selected-items', $selectionType, array('class' => 'selected-items'));

$encodedIds = CJSON::encode($selectedIds);

?>

<div class="category-grid">
	<div class="row">
		<div class="span3 legend" style="width: 624px; padding: 0px">
			<div style="border-top-color: white; border-top-style: solid; border-top-width: 1px; border-bottom-color: white; border-bottom-style: solid; border-bottom-width: 1px; padding: 17px">
				<div class="left-selections pull-left clearfix" style="margin-top: -10px; margin-right: 18px;">
					<p style="margin-left: -9px; margin-bottom: 1px">You have selected <strong><span id="combo-grid-selected-count2">0</span> items</strong>.</p>
					<a style="margin-left: 1px" href="javascript:void(0);" class="combo-grid-select-all"> Select all </a>
					<a href="javascript:void(0);" class="combo-grid-deselect-all"> Unselect all </a>
				</div>
                <span>
                    <span class="icon-select-node-yes"></span> <?php echo Yii::t('standard', '_YES');?>
					<span class="icon-select-node-no"></span> <?php echo Yii::t('standard', '_NO');?>
					<span class="icon-select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT');?>
                </span>
				<div class="search-input-wrapper pull-right">
					<input
						class="search-query ui-autocomplete-input" placeholder="Search"
						id="search_input2" type="text" value=""
						name="search_input2" autocomplete="off"
						style="width: 120px; margin-top: -7px; margin-right: -9px;"
						>
					<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
					<button id="fancytree-filter-clear-button2" type="button" class="close clear-search" style="margin-top: -7px; margin-right: -9px;">�</button>
					<span class="perform-search search-icon" style="margin-top: -7px; margin-right: -9px;"></span>
					</input>
				</div>
			</div>
		</div>
	</div>
	<?php
	$this->widget('common.extensions.fancytree.FancyTree', array(
		'id'            => $id,
		'treeData'      => $categoryTree2,
		'preSelected'   => $selectedIds,
		'filterInputFieldName' => 'search_input2',
		'filterClearElemId' => 'fancytree-filter-clear-button2',
		'eventHandlers' => array(
			'select' => 'function(event, data) {selectCallback(event, data);}',
		),
	));
	?>
</div>

<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm_write"
		   value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" /> <input
		class="btn-docebo black big close-dialog" type="reset"
		value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
</div>

<?php
CHTml::endForm();
?>


<script type="text/javascript">
	function selectCallback(event, data){
		var element = $('input#selectedCategories');
		var selectedNodes = $(event.target).fancytree("getTree").ext.docebo._getSelectedNodes();
		if(selectedNodes != []){
			var newData =[];
			for(var i=0; i < selectedNodes.length ; i++){
				var currentNode = selectedNodes[i];
				var currentData = { id: currentNode.key, state: currentNode.selectState };
				newData.push(currentData);
			}
			element.val(JSON.stringify(newData));
		}else{
			element.val('');
		}

	}

	$(function() {
		$("<div><h2 style='color: #3e7dbf; font-weight: bold'><?= Yii::t('standard', 'Select Courses Catalogs')?></h2></div><hr style='margin: 0px !important;'>").insertBefore( $("#combo-grid") );

		var comboGrid = $("#courses-synch-grid");
		var encodedIds = <?= $encodedIds ?>;
		if ( $(".selected-items").val() == 'catalogs') {
			comboGrid.comboGridView('setSelection', <?= $encodedIds ?>);
		}

		$('.r_courses').styler();

		// Handle Select All clicks in Org Chart tree
		$(".combo-grid-select-all").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(2);
		});

		// Handle De-Select All clicks in Org Chart tree
		$(".combo-grid-deselect-all").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(0);
		});
	});
</script>