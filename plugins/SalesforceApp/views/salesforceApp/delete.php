<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<?php $model = new CoreUser(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	)); ?>

	<?= CHtml::hiddenField('remove-field-confirm', 1) ?>
	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'LearningCourseuser_confirm_' . $checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php
		echo chtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'),
			'LearningCourseuser_confirm_' . $checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>