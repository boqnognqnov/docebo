<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'Salesforce'
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$idAccount = $salesforceLmsAccount->id_account;

$this->breadcrumbs = $_breadcrumbs;
?>
<div class="salesforce-config">
    <div class="row-fluid">
        <div class="span6">
            <h2>Salesforce</h2>
        </div>
        <div class="span6">
            <a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/knowledge-base/docebo-per-salesforce/' : 'http://www.docebo.com/knowledge-base/docebo-for-salesforce/' ?>" target="_blank" class="app-link-read-manual pull-right">
                <?= Yii::t('apps', 'Read Manual'); ?>
            </a>        
        </div>
    </div>        
    <hr />
    <div class="row-fluid">
        <div class="span6"> 
            <span class="tick"></span><p class="lead"><?=Yii::t('salesforce', '<strong>Salesforce APP</strong> has been configured correctly');?></p>
        </div>
        <div class="span6">

        <a href="index.php?r=SalesforceApp/SalesforceApp/clearConfig&account=<?=$idAccount?>"
        	style="margin-left: 15px; background-color: #e74c3c;"
            class="open-dialog btn btn-docebo red big open-dialog pull-right"
            data-dialog-class="salesforce-dialog-clear-config"
            title="<?= Yii::t("salesforce", "Clear configuration") ?>"
           >
            <?=Yii::t('salesforce', 'Clear configuration');?>
        </a> 
        <a href="index.php?r=SalesforceApp/SalesforceApp/axOpen&account=<?=$idAccount?>"
            class="open-dialog wizard-starter btn btn-docebo blue big open-dialog pull-right"
            data-dialog-class="salesforce-dialog-step1"
            data-wizard-dispatcher-url="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/axWizardDispatcher', array(
                'id' => $idAccount
            ))?>"
            
            data-bootstro-id="bootstroConfigureSalesforce"
            rel="wizard-demo"
            title="<?=Yii::t('salesforce', 'Set up Wizard');?>"
           >
            <?=Yii::t('salesforce', 'Edit main configuration');?>
        </a>            
        </div>
    </div>

    <br/>

    <div class="salesforce-dashboard">

			<div class="salesforce-box" id="salesforce-box-1">
				<?php
				$this->renderPartial('config/_box_1', array(
					'blockReady' => $block1Ready,
					'enabledUserFilter' => $enabledUserFilter,
					'enabledContactFilter' => $enabledContactFilter
				));
				?>
			</div>

			<div id="salesforceBox2Container" class="salesforce-box-container">
				<?php
				$this->renderPartial('config/_box_2', array(
					'blockReady' => $block2Ready,
					'blockActive' => $block2Active,
					'salesforceLmsAccount' => $salesforceLmsAccount,
					'enabledUserFilter' => $enabledUserFilter,
					'enabledContactFilter' => $enabledContactFilter,
					'usersCompleted' => $usersCompleted,
					'contactsCompleted' => $contactsCompleted,
					'usersButtonText' => $usersButtonText,
					'contactsButtonText' => $contactsButtonText,
					'showSyncUsersButton' => $showSyncUsersButton,
					'syncUsersButtonText' => $syncUsersButtonText,
					'usersLog' => $usersLog,
					'salesforceModel' => $salesforceModel
				));
				?>
			</div>

			<div class="salesforce-box last<?= ($block3Active ? "" : " inactive") ?>" id="salesforce-box-3">
				<?php
				$this->renderPartial('config/_box_3', array(
					'blockReady' => $block3Ready,
					'blockActive' => $block3Active,
					'salesforceLmsAccount' => $salesforceLmsAccount,
					'coursesCompleted' => $coursesCompleted,
					'coursesButtonText' => $coursesButtonText,
					'showSyncCoursesButton' => $showSyncCoursesButton,
					'coursesLog' => $coursesLog
				));
				?>
			</div>

    </div>

    <br />

    <div class="row-fluid row-item-sf odd">
        <div class="span2">
            <p class="lead">
                <?=Yii::t('salesforce', 'Synchronization frequency');?>
            </p>
        </div>
        <div class="span10">
            <div class="syncfrequency-form<?=($block4Active?"":" inactive")?>">
                <?=CHtml::beginForm(); ?>
                <div class="control-label">
                    <?=CHtml::label(Yii::t('salesforce', 'Launch the automatic Docebo <i class="icon-retweet"></i> Salesforce synchronization') ,'sync_freq', array('style'=>'margin-bottom: 10px'));?>
                    <?=CHtml::dropDownList('SalesforceLmsAccount[sync_frequency]', $salesforceLmsAccount->sync_frequency, array(
                        1 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 1)),
                        4 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 4)),
                        8 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 8)),
                        16 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 16)),
                        24 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 24)),
                        26280 => Yii::t('salesforce', 'Manually'),
                    ), array('style' => 'width: 526px; margin-right: 10px;', 'disabled' => ($block4Active?"":"disabled")))?>
                    <?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
											'id' => 'sync_frequency_save_button',
                        'class' => 'btn btn-docebo green big',
                        'disabled' => ($block4Active?"":"disabled")
                    ));?>
                </div>
                <?=CHtml::endForm(); ?>
            </div>
        </div>
    </div>



</div>

<script type="text/javascript">
	var salesforceOptions = {};
	SalesforceAppManager = new SalesforceApp(salesforceOptions);
	var actionsAllowed = true, debugging = <?= (Settings::getCfg('do_debug', false) ? 'true' : 'false') ?>;

	<?php
	if(!empty($startedJobs)):

	$type = null;
	$jobHash = null;

	if(isset($startedJobs[SalesforceLog::TYPE_LOG_USERS])){
			$type = 'users';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_USERS];
	} elseif(isset($startedJobs[SalesforceLog::TYPE_LOG_COURSES])){
			$type = 'courses';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_COURSES];
	} elseif(isset($startedJobs[SalesforceLog::TYPE_LOG_UPDATE_METADATA])){
			$type = 'update_metadata';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_UPDATE_METADATA];
	}
	?>
	$(function () {
		var waitForSuccess = setInterval(function () {
			$.ajax({
				url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/waitForSync',
				data: {'job_id': "<?=$jobHash?>"},
				success: function (res) {
					console.log(res);
					if (res.success == true) {
						clearInterval(waitForSuccess);
						var html = "<div class='span3 text-uppercase'><span class='success-tick'><span></div><div class='span9'><p class='lead text-uppercase'><strong>" + Yii.t('salesforce', 'Syncing') + "<br/><p class='green-text text-uppercase'>" + Yii.t('standard', '_COMPLETED') + "</p></strong></p></div>";
						$('#sync-<?=$type?>-holder').html(html);
					}
				}
			});
		}, 3000);
	});
	<?php
	endif;
	?>


	function reloadWithUrl(url) {
		if (!actionsAllowed) return;
		$.ajax({
			url: url,
			dataType: 'json',
			beforeSend: function() {
				blockActions();
			}
		}).done(function (data) {
			if (data.success) {
				if (typeof data.box_1 !== 'undefined') { $('#salesforce-box-1').html(data.box_1); }
				if (typeof data.box_2 !== 'undefined') { $('#salesforceBox2Container').html(data.box_2); }
				if (typeof data.box_3 !== 'undefined') { $('#salesforce-box-3').html(data.box_3); }
				assignOverlayEventToButtons(); //since buttons are re-created here, their previously attached events are lost, so re-assign them
			}
		}).always(function() {//fail(function() {
			allowActions();
		});
	}


	//while synchronizing do not allow playing with other buttons
	function blockActions() {
		actionsAllowed = false;
		showSpinner();
	}

	//re-enable action buttons
	function allowActions() {
		actionsAllowed = true;
		hideSpinner();
	}

	function showSpinner() { $('.overlay-spinner').removeClass('hidden'); }
	function hideSpinner() { $('.overlay-spinner').addClass('hidden'); }

	function assignOverlayEventToButtons() {
		//reset eventual previously assigned events and re-assign the click ovent responsible to make overlay spinner appearing
		$('a.sf-button.load-overlay-spinner').off("click");
		$('a.sf-button.load-overlay-spinner').on("click", function(e) { blockActions(); });
	}

	$(document).ready(function() {
		$(document.body).append('<div class="modal-backdrop overlay-spinner hidden"><i class="fa fa-spinner fa-spin spin-big"></i></div>');
		assignOverlayEventToButtons()
	});

		function pollUsers(){
			var orig = $('#sync_users_holder').html();
			var html = "<div id='sync_users' class='btn btn-docebo btn-docebo-xl blue big indented-vertically'><span class='spinner'>" + Yii.t('salesforce', 'Syncing') + "...</span></div>";
			$('#sync_users_holder').html(html);
			$.ajax({
				url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/sync',
				data: {
					type: '<?=SalesforceLog::TYPE_LOG_USERS?>',
					id_account: <?=$idAccount?>
				},
				dataType: 'json',
				success: function (obj) {
					if (obj.success === false) {
						return;
					}
					var waitForSuccess = setInterval(function () {
						$.ajax({
							url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/waitForSync',
							data: {
								job_id: obj.jobId,
								sync_type: 'users'
							},
							dataType: 'json',
							success: function (res) {
								if (res.success == true) {
									clearInterval(waitForSuccess);
									$('#sync_users_holder').html(orig);
									$('#usersSyncMsgHasLog').find('strong').html(res.lastDate);
									$('#usersSyncMsgHasLog').css('display', 'inline-block');
									$('#usersSyncMsgNoLog').css('display', 'none');
									// temporary reload function upon first sync
									if ($('#salesforce-box-3').hasClass('inactive')) {
										//window.location.reload(); //NOTE: no need to reload the entire page
										$('#salesforceBox2Container .salesforce-box-icon').addClass('completed'); //second box is considered completed after sync
										$('#salesforce-box-3').removeClass('inactive');
										if (typeof res.box_3 !== 'undefined') { $('#salesforce-box-3').html(res.box_3); }
									}
								}
							}
						});
					}, 3000);
				}
			});
		}

		$(document).on('click', '#sync_users', function () {
			pollUsers();
		});

		<?php if($userJobStarted){ ?>
			pollUsers();
		<?php } ?>

		function pollCourses(){
			var orig = $('#sync_courses_holder').html();
			var html = "<div id='sync_courses' class='btn btn-docebo btn-docebo-xl blue big indented-vertically' style='margin-top: 54px;'><span class='spinner'>" + Yii.t('salesforce', 'Syncing') + "...</span></div>";
			$('#sync_courses_holder').html(html);
			$.ajax({
				url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/sync',
				data: {
					type: '<?=SalesforceLog::TYPE_LOG_COURSES?>',
					id_account: <?=$idAccount?>
				},
				dataType: 'json',
				success: function (obj) {
					if (obj.success == false) {
						//TODO: how to handle the error here ?
						return;
					}
					var waitForSuccess = setInterval(function () {
						$.ajax({
							url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/waitForSync',
							data: {
								job_id: obj.jobId,
								sync_type: 'courses'
							},
							dataType: 'json',
							success: function (res) {
								if (res.success == true) {
									clearInterval(waitForSuccess);
									$('#sync_courses_holder').html(orig);
									$('#courseSyncMsgHasLog').find('strong').html(res.lastDate);
									$('#courseSyncMsgHasLog').css('display', 'inline-block');
									$('#courseSyncMsgNoLog').css('display', 'none');
									// temporary reload function upon first sync
									if ($('.syncfrequency-form').hasClass('inactive')) {
										//window.location.reload(); //NOTE: no need to reload the entire page
										$('#salesforce-box-3 .salesforce-box-icon').addClass('completed');
										$('.syncfrequency-form').removeClass('inactive');
										$('#SalesforceLmsAccount_sync_frequency').prop('disabled', false);
										$('#sync_frequency_save_button').prop('disabled', false);
										//$('#sync_frequency_save_button').removeClass('disabled');
									}
								}
							}
						});
					}, 3000);
				}
			});
		}

		$(document).on('click', '#sync_courses', function () {
			pollCourses();
		});

		<?php if($courseJobStarted){ ?>
			pollCourses();
		<?php } ?>

</script>