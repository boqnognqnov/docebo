<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', 'Salesforce') => Docebo::createAppUrl('admin:SalesforceApp/SalesforceApp/index'),
	'Course Settings'
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;
?>

<?= CHtml::beginForm(Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupCourses'), 'POST', array('id' => 'setup-courses-form')); ?>
<h3 class="title-bold back-button">
    <a href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index')?>"><?=Yii::t('standard', '_BACK')?></a>
    <span><?=Yii::t('salesforce', 'Course Settings')?></span>
</h3>

<?php if(!empty($errors)){
    print "<br>";
    foreach ($errors as $error) { ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="errorSummary"><?=$error?></div>
            </div>
        </div>
    <?php } } ?>

<div class="advanced-main sf-user-settings-blocks-list">
    <div class="section">
        <div class="row odd">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Select courses') ?>
                </div>
                <div class="values">
                    <div><?= Yii::t('salesforce', 'Please select which courses you want to sync from Docebo to Salesforce') ?></div>
                    <br/>

                    <div style="min-height: 40px;"><?= CHtml::radioButton('course_selection', ($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL)?true:false, array('id' => 'all', 'value' => 'all', 'class' => 'courseSelection')) ?><label for="all"><?=Yii::t('salesforce', 'All courses')?> (<?=$selectedCounters[SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL]?>)</label></div>
                    <div style="clear: left; min-height: 40px;">
                        <div style="width: 200px; float: left"><?= CHtml::radioButton('course_selection', ($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES)?true:false, array('id' => 'categories', 'value' => 'categories', 'class' => 'courseSelection')) ?><label for="categories"><?=Yii::t('salesforce', 'Course categories')?> (<span id="catalogCount"><?=$selectedCounters[SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES]?></span>)</label></div>
                        <?= CHtml::hiddenField('selectedCategories', '', array('id' => 'selectedCategories')); ?>
                        <div style="margin-top: -5px; float: left">
                            <a href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/selectCourseCategories', array('id_account' => $lmsAccount->id_account))?>"
                                data-dialog-class="salesforce-dialog-select-courses"
                                class="open-dialog sf-button" id="select_categories"
                                <?=($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES)?'':' style="display:none;"'?> >
                                <?=Yii::t('standard', '_SELECT');?>
                            </a>
                        </div>
                    </div>
                    <div style="clear: left; min-height: 40px;">
                        <div style="width: 200px; float: left"><?= CHtml::radioButton('course_selection', ($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS)?true:false, array('id' => 'catalog', 'value' => 'catalog', 'class' => 'courseSelection')) ?><label for="catalog"><?=Yii::t('salesforce', 'Course catalog')?> (<span id="catalogCount"><?=$selectedCounters[SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS]?></span>)</label></div>
                        <?= CHtml::hiddenField('selectedCatalogs', '', array('id' => 'selectedCatalogs')); ?>
                        <div style="margin-top: -5px; float: left">
                            <a href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/selectCourseCatalog', array('id_account' => $lmsAccount->id_account))?>"
                                data-dialog-class="salesforce-dialog-select-courses"
                                class="open-dialog sf-button" id="select_catalog"
                                <?=($selectionType == SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS)?'':' style="display:none;"'?>>
                                <?=Yii::t('standard', '_SELECT');?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row even">
            <div class="row">

                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Advanced settings') ?>
                </div>
                <div class="values">
                    <div><?= Yii::t('salesforce', 'Do you want to replicate Docebo courses as SF products? (Only if the Docebo course is flagged to be synchronized with SF)') ?></div>
                    <br/>

                    <div>
                        <?= CHtml::radioButton('replicate_products', ($lmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_NONE)?true:false, array('id'=> 'not_replicate', 'value'=>'0')) ?><label for="not_replicate"><?=Yii::t('salesforce', 'No, do not replicate anything')?></label>
                    </div>
                    <br />
                    <div>
                        <?= CHtml::radioButton('replicate_products', ($lmsAccount->replicate_courses_as_products >= SalesforceLmsAccount::TYPE_REPLICATE_COURSES)?true:false, array('id'=> 'replicate', 'value'=>'1')) ?><label for="replicate"><?=Yii::t('salesforce', 'Yes, replicate docebo courses as SF products')?></label>
                    </div>
                    <br />
                    <div style="margin-left: 28px;<?=($lmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_NONE)?' display:none;':''?>" id="issue_so_container">
                        <?=CHtml::checkBox('issue_so', ($lmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS), array('id' => 'issue_so', 'value' => '1'))?><label for="issue_so"><?=Yii::t('salesforce', 'Also issue sales orders')?></label>
                    </div>
                </div>

            </div>
        </div>
        <div class="row even">
            <div class="settingsButtons">
                <input class="btn-docebo green big" id="saveChanges" name="saveChanges" type="button" value="<?=Yii::t('salesforce', 'Save changes')?>" />
                <input class="btn-docebo black big" id="cancelChanges" name="cancelChanges" type="submit" value="<?=Yii::t('standard', '_CANCEL')?>"/>
            </div>
        </div>

    </div>
</div>
<?=CHtml::endForm(); ?>

<script>
    $('#all').styler();
    $('#categories').styler();
    $('#catalog').styler();

    $('#not_replicate').styler();
    $('#replicate').styler();
    $('#issue_so').styler();
		//$('.divider')[3].outerHTML = '';
		var _dividers = $('.divider');
		if (_dividers.length >= 3) {
			var _tmp_divider = _dividers[3];
			if (typeof _tmp_divider !== 'undefined' && typeof _tmp_divider.outerHTML !== 'undefined') {
				_tmp_divider.outerHTML = '';
			}
		}

    $('#not_replicate, #replicate').on('change', function(){
        var id = $(this).attr('id');
        var issue_so = $('#issue_so').prop('checked');
        if(id == 'not_replicate'){
            $('#issue_so-styler').removeClass('checked');
            $('#issue_so').prop('checked', false);
            $('#issue_so_container').css('display', 'none');
        } else {
            $('#issue_so_container').css('display', 'block')
        }
    });

    $('.courseSelection').on('change', function(){
        var type = $(this).attr('id');
        if(type == "all"){
            $('#select_categories').css('display', 'none');
            $('#select_catalog').css('display', 'none');
        } else if(type == 'categories'){
            $('#select_categories').css('display', 'block');
            $('#select_catalog').css('display', 'none');
        } else if(type == 'catalog'){
            $('#select_categories').css('display', 'none');
            $('#select_catalog').css('display', 'block');
        }
    });

    // Handle a.auto-close
    $(document).on("dialog2.content-update", ".modal", function () {
        if ($(this).find("a.auto-close").length > 0) {
            // Remove editor
            if ($(this).find("a.auto-close.catalogs").length > 0) {
                var data = $(this).find("a.auto-close.catalogs").data('catalogs');
                $('input#selectedCatalogs').val(data);
            }
        }
    });


	$(document).ready(function() {

		$('#saveChanges').on('click', function(e) {

			var unique = new Date().getTime();
			var dialogId = 'courses-config-dialog-'+unique;
			$('<div/>').dialog2({
				autoOpen: true,
				id: dialogId,
				title: '<?= Yii::t('salesforce', 'Configure Salesforce App') ?>',
				modalClass: 'modal-salesforce-courses-setup',
				showCloseHandle: false, //this dialog cannot be closed, since it is monitoring a job status and the user is not supposed to do anything else in the page
				removeOnClose: false,
				closeOnEscape: false,
				closeOnOverlayClick: false
			});
			//put a loader icon while performing ajax request
			$('#'+dialogId).html('<div class="overlay-spinner"><i class="fa fa-spinner fa-spin spin-big"></i></div>');

			//send form data, start process and progress bar
			$.ajax({
				url: '<?= Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupCoursesProgress') ?>',
				type: 'POST',
				dataType: 'JSON',
				data: $('#setup-courses-form').serialize() + '&unique-dialog-id=' + unique
			}).done(function(o) {
				if (o.success) {
					$('#'+dialogId).html(o.html || '');
				} else {
					//show error message
				}
			}).fail(function() {
				//show error message
			}).always(function() {
				//do stuff, whatever the call ends up
			});
		});

	});

</script>