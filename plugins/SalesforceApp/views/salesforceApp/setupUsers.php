<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', 'Salesforce') => Docebo::createAppUrl('admin:SalesforceApp/SalesforceApp/index'),
	Yii::t('salesforce', 'User Settings')
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;
?>

<h3 class="title-bold back-button">
    <a href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index')?>"><?=Yii::t('standard', '_BACK')?></a>
    <span><?=Yii::t('salesforce', 'User Settings')?></span>
</h3>

<?php if(!empty($errors)){
    print "<br>";
    foreach ($errors as $error) { ?>
<div class="row-fluid">
    <div class="span12">
        <div class="errorSummary"><?=$error?></div>
    </div>
</div>
<?php } } ?>

<?php
    $ft = new FancyTree();
    echo "<script src='" . $ft->getAssetsUrl() . '/jquery.fancytree-all.js' . "'></script>";
    echo "<script src='" . $ft->getAssetsUrl() . '/jquery.fancytree.docebo.js' . "'></script>";
    echo "<link rel='stylesheet' href='" . $ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css' . "' />";
    //Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
?>

<?=CHtml::beginForm(); ?>
<div class="advanced-main sf-user-settings-blocks-list">
    <div class="section">
        <div class="row odd">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'List View') ?>
                </div>
                <div class="values">
                    <div><?= Yii::t('salesforce', 'Before syncing the users from Salesforce, you must select a user list view in order to import only the desired users.') ?></div>
                    <div style="margin-top: 10px"><?= Yii::t('salesforce', 'Users') ?></div>

                    <?=CHtml::dropDownList('users', $selectedUserListview, $listUsers)?>
                    <span id="total_users"<?=($selectedUserListview)?"":" style='opacity: 0;'"?>><?=Yii::t('salesforce', 'Total users')?>: <b id="total_users_count"><?=($selectedUserListview)?$listUsersJs[$selectedUserListview]:"0"?></b></span>
                </div>
            </div>
        </div>

        <div class="row even">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('profile', '_CHANGEPASSWORD') ?>
                </div>
                <div class="values">
                    <?=CHtml::checkBox('check_force_change_password', ($listviewModel->force_change_password == 1)?true:false, array(
                        'id' => 'check_force_change_password',
                        'value' => 1
                    ));?>
                    <?=CHtml::label(Yii::t('salesforce', 'Force users to change their password at the first sign in'), 'check_force_change_password');?>
                </div>
            </div>
        </div>

        <div class="row odd">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Organization chart') ?>
                </div>
                <div class="values" id="orgchartBlock">
                    <div><?= Yii::t('salesforce', 'Before syncing from Salesforce, you have to select destination for synchronized users') ?></div>
                    <br/>

                    <?php
                        $singleBranchSelected = ($listviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH)?true:false;
                        $orgChartAdditionalFields = ($listviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS)?true:false;
                        if (!$singleBranchSelected && !$orgChartAdditionalFields)
                            $singleBranchSelected = true;
                    ?>

                    <div>
                        <?= CHtml::radioButton(
                                'organization_chart',
                                $singleBranchSelected,
                                array('id'=> 'single_branch', 'value'=>SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH))
                        ?>
                        <label for="single_branch"><?=Yii::t('salesforce', 'Use single branch')?><span class="labelSmallInfo"><?=Yii::t('salesforce', 'Transfer all your users into one branch')?></span></label>
                    </div>
                    <br/>
                    <div>
                        <?= CHtml::radioButton(
                                'organization_chart',
                                $orgChartAdditionalFields,
                                array('id'=> 'additional_fields', 'value'=>SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS))
                        ?>
                        <label for="additional_fields"><?=Yii::t('salesforce', 'Use additional fields hierarchy')?><span class="labelSmallInfo"><?=Yii::t('salesforce', 'Define your LMS org Chart based on Salesforce fields (user level)')?></span></label>
                    </div>
                    <br/>
                    <div class="hierarchy-container">
                        <div class="hierarchy-block">
                            <?= CHtml::hiddenField('idOrgChart', ($listviewModel->orgchartId) ? $listviewModel->orgchartId:'', array('id' => 'selectedIdOrg')); ?>
                            <a
                                data-dialog-class="org-chart-popup-content"
                                class="sf-button open-dialog"
                                id="select-org-chart"
                                href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/axSelectOrganizationChartNode', array('node' => $listviewModel->orgchartId, 'type' => 'users'))?>"
                                >
                                <?=Yii::t('standard', '_SELECT');?>
                            </a>
                            <div class="sf-button" id="select-org-chart-dummy"><?=Yii::t('standard', '_SELECT');?></div>
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy1', $hierarchySelects['selected1'], $hierarchySelects['level1'])?>
                            <span class="smallInfo" id="hierarchy1Info"><?=Yii::t('salesforce', 'You can add max 3 levels');?></span>
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy2', $hierarchySelects['selected2'], $hierarchySelects['level2'])?>
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy3', $hierarchySelects['selected3'], $hierarchySelects['level3'])?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row even" style="border-bottom: 0px">
            <div class="row">

                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Fields') ?>
                </div>
                <div class="values" id="fieldsBlock">
                    <div><?= Yii::t('salesforce', 'Import Salesforce user fields as Docebo additional user fields') ?></div>
                    <div class="additionalFieldSelect">
                        <?=CHtml::dropDownList('sf_fields_type', false, $salesforceUserFields, array('id' => 'sf_fields_type', 'class'=>'fieldSelectorDropdown')) ?>
                    </div>
                    <div class="additionalFieldsBoxTop"<?=(empty($additionalUserFields))?" style='display: none;'":""?>>
                        <div class="additionalFieldsFieldname additionalFieldsLabel"><?= Yii::t('salesforce', 'Salesforce field') ?></div>
												<div class="additionalFieldsRightPart additionalFieldsLabel">
													<?= Yii::t('salesforce', 'Assign to existing field') ?>
													&nbsp;&nbsp;
													<span class="hint">(<?= Yii::t('salesforce', 'Only compatible fields will be showed') ?>)</span>
												</div>
                    </div>
                    <br/>
                    <hr class="additionalFieldsHrTop"<?=(empty($additionalUserFields))?" style='display: none;'":""?> />
                    <div id="additionalFieldsUsers">
                        <?php /* NOTE: this part is initialized and filled by javascript on document.ready event */ ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row even">
            <div class="settingsButtons" style="padding-top: 10px; padding-bottom: 10px">
                <input class="btn-docebo green big" id="saveChanges" name="saveChanges" type="submit" value="<?=Yii::t('salesforce', 'Save changes')?>" />
                <input class="btn-docebo black big" id="cancelChanges" name="cancelChanges" type="submit" value="<?=Yii::t('standard', '_CANCEL')?>"/>
            </div>
        </div>
    </div>
</div>
<?=CHtml::endForm(); ?>

<script>

    $('#additional_fields').styler();
    $('#single_branch').styler();
    $('#check_force_change_password').styler();

    // Show/hide users counter
    $('#users').on('change', function(){
        var listUsers = <?=json_encode($listUsersJs)?>;
        var selectedItem = $('#users').val();
        var amount = listUsers[selectedItem];
        if(amount == false) amount = 0;
        $('#total_users_count').text(amount);
        if($('#users').val() != ""){
            $('#total_users').css('opacity', 1);
        } else {
            $('#total_users').css('opacity', 0);
        }
    });


		var alreadyAssignedLmsFields = {user: [], account: [], contact: []};
		<?php
		if (is_array($alreadyAssignedLmsFields) && !empty($alreadyAssignedLmsFields)) {
			foreach ($alreadyAssignedLmsFields as $sfType => $fieldsList) {
				switch ($sfType) {
					case 'User':
						echo "\n".'alreadyAssignedLmsFields.user = ['.implode(',', $fieldsList).'];';
						break;
					case 'Account':
						echo "\n".'alreadyAssignedLmsFields.account = ['.implode(',', $fieldsList).'];';
						break;
					case 'Contact':
						echo "\n".'alreadyAssignedLmsFields.contact = ['.implode(',', $fieldsList).'];';
						break;
				}
			}
		}
		?>


		var alreadyAssignedSalesforceFields = [<?php
		if (is_array($alreadyAssignedFields) && !empty($alreadyAssignedFields)) {
			$_keys = array_keys($alreadyAssignedFields);
			echo '"'.implode('","', $_keys).'"';
		}
		?>];

		function updateFieldDropdown() {
			var options = $('#sf_fields_type option');
			if (options.length > 0) {
				$.map(options, function (option) {
					var value = $(option).attr('value');
					if (value != 0) {
						var name = value.split('|')[1];
						if (name && alreadyAssignedSalesforceFields.indexOf(name) >= 0) {
							$(option).prop('disabled', true);
							$(option).removeClass('not-yet-selected');
							$(option).addClass('already-selected');
						} else {
							$(option).prop('disabled', false);
							$(option).removeClass('already-selected');
							$(option).addClass('not-yet-selected');
						}
					}
				});
			}
		}

		var setupTranslations = {
			_CREATE_NEW_FIELD: <?= CJSON::encode(Yii::t('salesforce', 'Create new field')) ?>,
			_ADDITIONAL_FIELDS: <?= CJSON::encode(Yii::t('salesforce', 'Additional Fields')) ?>,
			_OR: <?= CJSON::encode(Yii::t('salesforce', 'OR')) ?>,
			_DELETE: <?= CJSON::encode(Yii::t('standard', '_DEL')) ?>,
			_CONFIRM: <?= CJSON::encode(Yii::t('standard', '_CONFIRM')) ?>,
			_CANCEL: <?= CJSON::encode(Yii::t('standard', '_CANCEL')) ?>
		};

		var lmsAdditionalFields = [
			<?php
			$first = true;
			foreach ($lmsAdditionalFields as $key => $label) {
				if ($first) { $first = false; } else { echo ','."\n"; }
				list($type, $name) = explode('|', $key);
				echo '{type: "'.$type.'", name: "'.$name.'", label: '.CJSON::encode($label).'}';
			}
			?>
		];

		var SalesforceFieldsMap = {
			<?php
			$first = true;
			foreach (SalesforceAdditionalFields::$SALESFORCE_FIELDS_TYPE_MAP as $sfType => $lmsType) {
				if ($first) { $first = false; } else { echo ','."\n"; }
				echo $sfType.': "'.$lmsType.'"';
			}
			?>
		};



		function getNewFieldRow(context, newFieldName, newFieldLabel, selectedField, filter) {
			var newBlock = '';
			if (newFieldName != 0) {
				var additionalFieldsExisting = $('.additionalFields'+context+'-item').length;
				if (additionalFieldsExisting == 0){
					var newKey = 0;
				} else {
					var newKey = parseInt($('.additionalFields'+context+'-item').last().attr('id').split('_')[1]) + 1;
				}
				var i, key, label, selected;
				newBlock += "<div class='additionalFields"+context+"-item' id='additionalFields"+context+"_"+newKey+"' style='border-bottom-width: 0px'>";
				newBlock += "   <input type='hidden' value='"+newFieldName+"' name='sf"+context+"FieldName_"+newKey+"' id='sf"+context+"FieldName_"+newKey+"'>";
				newBlock += "   <input type='hidden' value='"+newFieldLabel+"' name='sf"+context+"FieldLabel_"+newKey+"' id='sf"+context+"FieldLabel_"+newKey+"'>";
				newBlock += "   <div class='additionalFieldsBox'>";
				newBlock += "       <div class='additionalFieldsFieldname'>";
				newBlock += "           "+newFieldLabel;
				newBlock += "       </div>";
				newBlock += "       <div class='additionalFieldsRightPart'>";
				newBlock += "           <div class='additionalFieldsSelect'>";
				newBlock += "               <select name='"+context+"LmsFieldId_"+newKey+"' id='"+context+"LmsFieldId_"+newKey+"' class='lms-fields-selector'>";
				for (i=0; i<lmsAdditionalFields.length; i++) {
					if (i<=0 || !filter || (filter && filter == lmsAdditionalFields[i].type)) {
						if (alreadyAssignedLmsFields.account.indexOf(parseInt(lmsAdditionalFields[i].name)) < 0 && alreadyAssignedLmsFields.contact.indexOf(parseInt(lmsAdditionalFields[i].name)) < 0) {
							key = (lmsAdditionalFields[i].type == '0' ? '0' : lmsAdditionalFields[i].type + '|' + lmsAdditionalFields[i].name);
							label = lmsAdditionalFields[i].label;
							selected = (selectedField && selectedField == lmsAdditionalFields[i].name);
							newBlock += "                   <option value='" + key + "'" + (selected ? " selected='selected'" : "") + ">" + label + "</option>";
						}
					}
				}
				newBlock += "               </select>";
				newBlock += "           </div>";
				/*newBlock += "           <div class='additionalFieldsOr'>";
				newBlock += "               " + setupTranslations._OR;
				newBlock += "           </div>";
				newBlock += "           <div class='additionalFieldsCreate'>";
				newBlock += "               <a class='sf-button popup-handler'";
				newBlock += "                   href='<?=Docebo::createAdminUrl('additionalFields/createFieldModal&type=textfield')?>'";
				newBlock += "                   data-modal-title='" + setupTranslations._ADDITIONAL_FIELDS + "'";
				newBlock += "                   data-modal-class='' >";
				newBlock += "                       " + setupTranslations._CREATE_NEW_FIELD;
				newBlock += "               </a>";
				newBlock += "               <div class='sf-button' style='display:none;'>" + setupTranslations._CREATE_NEW_FIELD + "</div>";
				newBlock += "           </div>";*/
				newBlock += "           <div class='additionalFieldsDelete'>";
				newBlock += "               <a href='#' class='sf-button deleteAdditional"+context+"Field ajaxModal' id='deleteAdditional"+context+"Field_"+newKey+"' "
					+"data-toggle='modal' data-modal-class='delete-node' data-modal-title='" + setupTranslations._DELETE + "' "
					+"data-buttons='[{&quot;type&quot;: &quot;submit&quot;, &quot;title&quot;: &quot;" + setupTranslations._CONFIRM + "&quot;},{&quot;type&quot;: &quot;cancel&quot;, &quot;title&quot;: &quot;" + setupTranslations._CANCEL + "&quot;}]' "
					+"data-url='SalesforceApp/SalesforceApp/axFieldRemoveConfirm' "
					+"data-after-loading-content='function() { hideConfirmButton(); selectedAdditionalField = $(&quot;#additionalFields"+context+"_"+newKey+"&quot;) }' "
					+"data-after-submit='afterRemoveFieldConfirm' "
					+"modal-request-type='GET' modal-request-data='{&quot;remove-key&quot;:" + newKey + "}' ></a>";
				newBlock += "               <div class='sf-button deleteAdditionalField' style='display:none;'></div>";
				newBlock += "           </div>";
				newBlock += "       </div>";
				newBlock += "   </div>";
				newBlock += "</div><hr/>";
			}
			return newBlock;
		}


		// Adding an additional user field
		$('#sf_fields_type').on('change', function () {
			var newFieldName = $('#sf_fields_type option:selected').attr('value');
			var newFieldLabel = $('#sf_fields_type option:selected').text();
			if (newFieldName != 0) {
				$('.additionalFieldsBoxTop').css('display', 'block');
				$('.additionalFieldsHrTop').css('display', 'block');
				var _split = newFieldName.split('|');
				var sfType = _split[0];
				var sfName = _split[1];
				var lmsType = (SalesforceFieldsMap[sfType] || false);
				var newBlock = getNewFieldRow('User', newFieldName, newFieldLabel, false, lmsType);
				$("#additionalFieldsUsers").append(newBlock);
				$("#sf_fields_type").val('0');
				alreadyAssignedSalesforceFields.push(sfName);
				updateFieldDropdown();
				updateLmsFieldSelectors();
			}
			return false;
		});



    function removeModal() {
        $('.modal.in').modal('hide');
        $('.modal.in').remove();
        $('#bootstrapContainer').remove();
        $('.modal-backdrop').remove();
    }

    // Passing orgchartId to hidden field
    $(document).off("dialog2.content-update", '.org-chart-popup-content').on("dialog2.content-update", '.org-chart-popup-content', function () {
        if ($(this).find("a.auto-close").length > 0) {
            var node = $(this).find("a.auto-close").data('node');
            if(node){
                $('input#selectedIdOrg').val(node);
            }
        }
    });


		var selectedAdditionalField = null;

		function afterRemoveFieldConfirm() {
			if (selectedAdditionalField) {

				var _split = $(selectedAdditionalField).attr('id').split('_');
				var context = _split[0].replace('additionalFields', ''); //element ID is in the form "additionalFields{context}_{key}"
				var input = $("#sf"+context+"FieldName_" + _split[1]);
				if (input) {
					var sfName = input.val().split('|')[1];
					var index = alreadyAssignedSalesforceFields.indexOf(sfName);
					if (index >= 0) {
						alreadyAssignedSalesforceFields.splice(index, 1);
					}
					updateFieldDropdown();
				}

				$(selectedAdditionalField).next().remove();
				$(selectedAdditionalField).remove();
				selectedAdditionalField = null;
			}
			// If there is no additional field left, remove the title bar...
			var additionalFieldsRemaining = $('.additionalFieldsUser-item').length;
			if (additionalFieldsRemaining < 1) {
				$('.additionalFieldsBoxTop').css('display', 'none');
				$('.additionalFieldsHrTop').css('display', 'none');
			}

			removeModal();

			return false;
		}


    // Show/hide appropriate hierarchy selects
    $(document).on('ready', function(){
        $('.hierarchy-block select').bind('change', function () {
            var selectedItem = $(this).attr('id');
            var valueSelect1 = $('#hierarchy1 option:selected').attr('value');
            var valueSelect2 = $('#hierarchy2 option:selected').attr('value');
            var valueSelect3 = $('#hierarchy3 option:selected').attr('value');

            // If the first value is at default option, reset selects 2 and 3 as well in order not to pollute the results
            if(valueSelect1 == '0'){
                $('#hierarchy2').val('0');
                $('#hierarchy3').val('0');
                $('#hierarchy2 option:not(:first)').css('display', 'none');
                $('#hierarchy3 option:not(:first)').css('display', 'none');
                $('#hierarchy2').css('display', 'none');
                $('#hierarchy3').css('display', 'none');
            } else { // The first value isn't equal to default option
                var matchingLevel2Items = $("#hierarchy2 option[value^='"+valueSelect1+"|']").length;
                if(matchingLevel2Items == 0){
                    $('#hierarchy2').val('0');
                    $('#hierarchy3').val('0');
                    $('#hierarchy2 option:not(:first)').css('display', 'none');
                    $('#hierarchy3 option:not(:first)').css('display', 'none');
                    $('#hierarchy2').css('display', 'none');
                    $('#hierarchy3').css('display', 'none');
                } else {
                    if(selectedItem == 'hierarchy1'){ // resetting select 2 & 3 on change
                        valueSelect2 = '0';
                        valueSelect3 = '0';
                        $('#hierarchy2').val('0');
                        $('#hierarchy3').val('0');
                        $('#hierarchy3 option:not(:first)').css('display', 'none');
                        $('#hierarchy3').css('display', 'none');
                    }
                    $('#hierarchy2').css('display', 'block');
                    $('#hierarchy2 option:not(:first)').css('display', 'none');
                    $("#hierarchy2 option[value^='"+valueSelect1+"|']").css('display', 'block');

                    // If the second value is at default option, reset select 3 as well in order not to pollute the results
                    if(valueSelect2 == '0'){
                        $('#hierarchy3').val('0');
                        $('#hierarchy3 option:not(:first)').css('display', 'none');
                        $('#hierarchy3').css('display', 'none');
                    } else { // The second value isn't equal to default option
                        var matchingLevel3Items = $("#hierarchy3 option[value^='"+valueSelect2+"|']").length;
                        if(matchingLevel3Items == 0){
                            $('#hierarchy3').val('0');
                            $('#hierarchy3 option:not(:first)').css('display', 'none');
                            $('#hierarchy3').css('display', 'none');
                        } else {
                            if(selectedItem == 'hierarchy2'){ // resetting select 3 on change
                                $('#hierarchy3').val('0');
                            }
                            $('#hierarchy3').css('display', 'block');
                            $('#hierarchy3 option:not(:first)').css('display', 'none');
                            $("#hierarchy3 option[value^='"+valueSelect2+"|']").css('display', 'block');
                        }
                    }
                }
            }
        });
        $('#hierarchy3').trigger('change');
    });

    function enableFieldsUI()
    {
        // Variables to check
        var listviewFilter = $('#users').val();
        var orgchartType = '';
        if($("#single_branch").is(':checked')) orgchartType = 'single_branch';
        if($("#additional_fields").is(':checked')) orgchartType = 'additional_fields';
        var orgchartId = $('#selectedIdOrg').val();
        var hierarchy1 = $('#hierarchy1').val();

        // Properties to set
        var orgChartBlockOpacity = 0.5;
        var radioButtonsDisabled = true;
        var selectOrgChartDisplay = 'none';
        var selectOrgChartDummyDisplay = 'block';
        var hierarchyVisibility = 'hidden';
        var hierarchyDisabled = true;
        var fieldsBlockOpacity = 0.5;
        var sfFieldsTypeDisabled = true;
        var additionalFieldsSelectDisabled = true;
        var additionalFieldsCreateADisplay = 'none';
        var additionalFieldsCreateDivDisplay = 'block';
        var deleteAdditionalFieldADisplay = 'none';
        var deleteAdditionalFieldDivDisplay = 'block';
        var submitDisabled = true;
        var submitOpacity = 0.5;

        if(orgchartType == 'additional_fields' && $('#selectedIdOrg')[0].value != ''){
            hierarchyVisibility = 'visible';
        }

        if(listviewFilter != ''){ // A listviewfilter has been set
            orgChartBlockOpacity = 1;
            radioButtonsDisabled = false;
            if(orgchartType != ''){ // The orgchart type has been chosen
                selectOrgChartDisplay = 'block';
                selectOrgChartDummyDisplay = 'none';
                if((orgchartId != '0') && orgchartId != '' && orgchartId != null){ // An orgchart node has been chosen
                    hierarchyDisabled = false;
                    if((orgchartType != 'additional_fields') || (hierarchy1 != '0')){
                        // Orgchart type isn't 'additional fields' or if selected, at least level 1 hierarchy has been selected
                        fieldsBlockOpacity = 1;
                        sfFieldsTypeDisabled = false;
                        additionalFieldsSelectDisabled = false;
                        additionalFieldsCreateADisplay = 'block';
                        additionalFieldsCreateDivDisplay = 'none';
                        deleteAdditionalFieldADisplay = 'block';
                        deleteAdditionalFieldDivDisplay = 'none';
                        submitDisabled = false;
                        submitOpacity = 1;
                    }
                }
            }
        }

        $('#orgchartBlock').css('opacity', orgChartBlockOpacity);
        $("input[type=radio]").prop('disabled', radioButtonsDisabled);
        $('#select-org-chart').css('display', selectOrgChartDisplay);
        $('#select-org-chart-dummy').css('display', selectOrgChartDummyDisplay);
        $('#hierarchy1, #hierarchy2, #hierarchy3').prop('disabled', hierarchyDisabled);
        $('#hierarchy1, #hierarchy2, #hierarchy3, #hierarchy1Info').css('visibility', hierarchyVisibility);
        $('#fieldsBlock').css('opacity', fieldsBlockOpacity);
        $('#sf_fields_type').prop('disabled', sfFieldsTypeDisabled);
        $('.additionalFieldsSelect select').prop('disabled', additionalFieldsSelectDisabled);
        $('.additionalFieldsCreate a.sf-button').css('display', additionalFieldsCreateADisplay);
        $('.additionalFieldsCreate div.sf-button').css('display', additionalFieldsCreateDivDisplay);
        $('a.deleteAdditionalField').css('display', deleteAdditionalFieldADisplay);
        $('div.deleteAdditionalField').css('display', deleteAdditionalFieldDivDisplay);
        $('#saveChanges').prop('disabled', submitDisabled).css('opacity', submitOpacity);

			//handle radio buttons stylers
			var radios = $("span.jq-radio");
			if (radioButtonsDisabled) {
				radios.addClass('disabled');
			} else {
				radios.removeClass('disabled');
			}
    }

    // Incrementally enable/disable blocks on the page
    function incrementallyEnableDisableBlocksOnThePage(){
        $('#users, #single_branch, #additional_fields, #selectedIdOrg, #hierarchy1').bind('change', enableFieldsUI);
        $('#users').trigger('change');
    }




		function updateLmsFieldSelectors() {

			//retrieve all field selectors in the page
			var selects = $('.lms-fields-selector');

			//check and store fields selector values
			var idField, selectedNow = [];
			selects.each(function(index, el) {
				var value = $(el).val();
				if (value != 0) {
					idField = parseInt(value.split('|')[1]);
					selectedNow.push(idField);
				}
			});

			//in each field selector, hide already selected fields
			var currentSelectedId;
			selects.each(function(index, el) {
				var value = $(el).val();
				currentSelectedId = (value != 0 ? parseInt(value.split('|')[1]) : 0);
				$(el).find('option').each(function(index, option) {
					var optionValue = $(option).attr('value');
					if (optionValue != 0) {
						var optionIdField = parseInt(optionValue.split('|')[1]);
						if (optionIdField) {
							var isAlreadyAssigned = (alreadyAssignedLmsFields.account.indexOf(optionIdField) >= 0 || alreadyAssignedLmsFields.contact.indexOf(optionIdField) >= 0);
							var isSelectedNow = (selectedNow.indexOf(optionIdField) >= 0);
							if ((isAlreadyAssigned || isSelectedNow) && optionIdField != currentSelectedId) {
								$(option).prop('disabled', true);
								$(option).removeClass('not-yet-selected');
								$(option).addClass('already-selected');
							} else {
								$(option).prop('disabled', false);
								$(option).removeClass('already-selected');
								$(option).addClass('not-yet-selected');
							}
						}
					}
				}) ;
			});

		}



		$(document).on('ready', function() {
			incrementallyEnableDisableBlocksOnThePage();

			//disable already assigned salesforce fields
			updateFieldDropdown();

			//print assigned salesforce fields rows
			<?php
			if (is_array($additionalUserFields) && !empty($additionalUserFields)) {
				foreach ($additionalUserFields as $key => $additionalField) {
					$afKey = $additionalField['sfFieldType']."|".$additionalField['sfFieldName'];
					$afLabel = CJSON::encode($additionalField['sfFieldLabel']);
					$lmsType = SalesforceAdditionalFields::$SALESFORCE_FIELDS_TYPE_MAP[$additionalField['sfFieldType']];
					$_lmsFieldId = 0;
					if (is_numeric($additionalField['lmsFieldId'])) {
						$_lmsFieldId = $additionalField['lmsFieldId'];
					} elseif (is_string($additionalField['lmsFieldId']) && strpos($additionalField['lmsFieldId'], '|') !== false) {
						$_arr = explode('|', $additionalField['lmsFieldId']);
						$_lmsFieldId = $_arr[1];
					}
					echo '$("#additionalFieldsUsers").append( getNewFieldRow("User", "'.$afKey.'", '.$afLabel.', '.((int)$_lmsFieldId > 0 ? (int)$_lmsFieldId : 'false').', '.CJSON::encode($lmsType).') );'."\n";
				}
			}
			?>

			$('#additionalFieldsUsers').on('change', '.lms-fields-selector', function(e) {
				updateLmsFieldSelectors();
			});
			updateLmsFieldSelectors();

		});


		$(document).on("dialog2.closed", ".modal", function () {
			if ($(this).find("a.auto-close").length > 0) {
				if ($('#selectedIdOrg')[0].value != '')
					enableFieldsUI();
			}
		});
		//$('.divider')[3].outerHTML = '';
		var _dividers = $('.divider');
		if (_dividers.length >= 3) {
			var _tmp_divider = _dividers[3];
			if (typeof _tmp_divider !== 'undefined' && typeof _tmp_divider.outerHTML !== 'undefined') {
				_tmp_divider.outerHTML = '';
			}
		}

</script>