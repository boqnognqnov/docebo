<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'Salesforce'
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;
?>

        <a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/knowledge-base/docebo-per-salesforce/' : 'http://www.docebo.com/knowledge-base/docebo-for-salesforce/' ?>" target="_blank" class="app-link-read-manual">
            <?= Yii::t('apps', 'Read Manual'); ?>
        </a>
        <h2>Salesforce</h2>
<hr/>
        <div class="sfIntroContainer">
            <div class="row">
                <div class="first-config-message <?php echo Yii::app()->getLanguage(); ?>">
                    <?= Yii::t('salesforce', 'Start here to configure your SF app for your first usage') ?>
                </div>
                <i class="bootstro-salesforce-arrow">

                </i>
            </div>

            <a href="index.php?r=SalesforceApp/SalesforceApp/axOpen"
                class="open-dialog wizard-starter btn-docebo green big"
                data-dialog-class="salesforce-dialog-step1"
                id="btn-confirm-configure-salesforce"
                data-bootstro-id="bootstroConfigureSalesforce"
                rel="wizard-demo"
                title="Set up Wizard"
                data-wizard-dispatcher-url="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/axWizardDispatcher')?>">
                <?= Yii::t('standard', 'Configure App') ?>
            </a>
        </div>

<hr>
<script type="text/javascript">

    $(document).controls();   
    
    var salesforceOptions = {};
    SalesforceAppManager = new SalesforceApp(salesforceOptions);


</script>
