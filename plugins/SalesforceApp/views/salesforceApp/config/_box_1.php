<?php
$_hrefUsers = "javascript:reloadWithUrl('".Docebo::createAdminUrl('SalesforceApp/SalesforceApp/axToggleUsers')."')";
$_hrefContacts = "javascript:reloadWithUrl('".Docebo::createAdminUrl('SalesforceApp/SalesforceApp/axToggleContacts')."')";
?>
<!--<div class="salesforce-box" id="salesforce-box-1">-->
	<div class="salesforce-box-icon<?=($blockReady?" completed":"")?>">&#xf058;</div>
	<div class="salesforce-box-title">
		<div>1. <?=Yii::t('salesforce', '<strong>Activate</strong> user/contacts (or both)');?></div>
		<div class="salesforce-details"><?=Yii::t('salesforce', 'Activate and sync all Salesforce users/contacts and make them Docebo users');?></div>
	</div>
	<div>
		<div class="sf-caption"><?=Yii::t('salesforce', 'Users');?></div>
		<div class="sf-field">
			<a href="<?= $_hrefUsers ?>" class="sf-switch <?=($enabledUserFilter)?"active":"inactive"?>" id="activateUsers"></a>
		</div>
	</div>
	<div>
		<div class="sf-caption"><?=Yii::t('salesforce', 'Contacts');?></div>
		<div class="sf-field">
			<a href="<?= $_hrefContacts ?>" class="sf-switch <?=($enabledContactFilter)?"active":"inactive"?>" id="activateContacts"></a>
		</div>
	</div>
<!--</div>-->