<?php
$_hrefViewLog = Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/viewLog', array(
	'type'      => SalesforceLog::TYPE_LOG_USERS,
	'account'   => $salesforceLmsAccount->id_account
));
?>
<div class="salesforce-box<?=($enabledUserFilter || $enabledContactFilter)?"":" inactive"?>" id="salesforce-box-2">
	<div class="salesforce-box-icon<?=($blockReady?" completed":"")?>">&#xf058;</div>
	<div class="salesforce-box-title">
		<div>2. <?=Yii::t('salesforce', '<strong>Set up</strong> and <strong>sync</strong> users and contacts');?></div>
		<div class="salesforce-details"><?=Yii::t('salesforce', 'Define settings for users/contacts and sync');?></div>
	</div>
	<div>

		<div class="sf-caption"><?=Yii::t('salesforce', 'Users');?>
			<?php if($usersCompleted){ ?>
				<div style="color: #5EBE5D; font-size: 10px"><?=Yii::t('salesforce', 'Setup completed!');?></div>
			<?php } ?>
		</div>
		<div class="sf-field">
			<?php if(!$blockActive || !$enabledUserFilter){ ?>
				<div class="sf-button"><?=$usersButtonText?></div>
			<?php } else { ?>
				<a class="sf-button load-overlay-spinner" href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupUsers')?>"><?=$usersButtonText?></a>
			<?php } ?>
		</div>
	</div>
	<div>
		<div class="sf-caption"><?=Yii::t('salesforce', 'Contacts');?>
			<?php if($contactsCompleted){ ?>
				<div style="color: #5EBE5D; font-size: 10px"><?=Yii::t('salesforce', 'Setup completed!');?></div>
			<?php } ?>
		</div>
		<div class="sf-field">
			<?php if(!$blockActive || !$enabledContactFilter){ ?>
				<div class="sf-button"><?=$contactsButtonText?></div>
			<?php } else { ?>
				<a class="sf-button load-overlay-spinner" href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupContacts')?>"><?=$contactsButtonText?></a>
			<?php } ?>
		</div>
	</div>
	<?php if($showSyncUsersButton){ ?>
		<div id="sync_users_holder">
			<a id="sync_users" data-type="<?=SalesforceLog::TYPE_LOG_USERS?>" data-account="<?= $salesforceLmsAccount->id_account ?>" class="btn btn-docebo btn-docebo-xl blue big indented-vertically">
				<?=$syncUsersButtonText?>
			</a>
		</div>
		<p class="sync_info">
			<?=Yii::t('salesforce', 'Last sync:')?>
			<span id='users_last_date'>
				<?php
				if(!empty($usersLog)){
					$hasLog = 'inline-block';
					$noLog = 'none';
				} else {
					$hasLog = 'none';
					$noLog = 'inline-block';
				}
				?>

				<span id="usersSyncMsgHasLog" style="display:<?=$hasLog?>;">
					<strong><?=Yii::app()->localtime->toLocalDateTime($usersLog->datetime)?></strong>
					&nbsp;|&nbsp;
					<a href="<?= $_hrefViewLog ?>" class='underline blue-text open-dialog'>
						<?=Yii::t('salesforce', 'View logs')?>
					</a>
				</span>
				<span id="usersSyncMsgNoLog" style="display:<?=$noLog?>;">
					<strong><?=Yii::t('standard', '_NEVER')?></strong>
				</span>
			</span>
		</p>
	<?php } ?>
</div>