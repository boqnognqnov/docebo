<?php
$_hrefViewLog = Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/viewLog', array(
	'type'      => SalesforceLog::TYPE_LOG_COURSES,
	'account'   => $salesforceLmsAccount->id_account
));
?>
<div class="salesforce-box-icon<?=($blockReady?" completed":"")?>">&#xf058;</div>
<div class="salesforce-box-title">
	<div>3. <?=Yii::t('salesforce', '<strong>Set up</strong> and <strong>sync</strong> courses');?></div>
	<div class="salesforce-details"><?=Yii::t('salesforce', 'Push selected Docebo courses or courses catalogs to Salesforce');?></div>
</div>
<div>
	<div class="sf-caption"><?=Yii::t('salesforce', 'Courses');?>
		<?php if($coursesCompleted){ ?>
			<div style="color: #5EBE5D; font-size: 10px"><?=Yii::t('salesforce', 'Setup completed!');?></div>
		<?php } ?>
	</div>
	<div class="sf-field">
		<?php if (!$blockActive) { ?>
			<div class="sf-button"><?=$coursesButtonText?></div>
		<?php } else { ?>
			<a class="sf-button load-overlay-spinner" href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupCourses')?>">
				<?=$coursesButtonText?>
			</a>
		<?php } ?>
	</div>
</div>
<?php if($showSyncCoursesButton){ ?>
	<div id="sync_courses_holder">
		<a id="sync_courses" data-type="<?=SalesforceLog::TYPE_LOG_COURSES?>" data-account="<?=$salesforceLmsAccount->id_account?>" class="btn btn-docebo btn-docebo-xl blue big indented-vertically" style="width: 298px; margin-top: 54px;"><?=Yii::t('salesforce', 'Sync courses')?></a>
	</div>
	<p class="sync_info">
		<?=Yii::t('salesforce', 'Last sync:')?>
		<span id='courses_last_date'>
			<?php
			if(!empty($coursesLog)){
				$hasLog = 'inline-block';
				$noLog = 'none';
			} else {
				$hasLog = 'none';
				$noLog = 'inline-block';
			}
			?>

			<span id="courseSyncMsgHasLog" style="display:<?=$hasLog?>;">
				<strong><?=Yii::app()->localtime->toLocalDateTime($coursesLog->datetime)?></strong>
				&nbsp;|&nbsp;
				<a href="<?= $_hrefViewLog ?>" class='underline blue-text open-dialog'>
					<?=Yii::t('salesforce', 'View logs')?>
				</a>
			</span>
			<span id="courseSyncMsgNoLog" style="display:<?=$noLog?>;">
				<strong><?=Yii::t('standard', '_NEVER')?></strong>
			</span>
		</span>
	</p>
<?php } ?>