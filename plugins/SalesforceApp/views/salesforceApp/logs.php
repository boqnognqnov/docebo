<h1><?=Yii::t('salesforce', 'Synchronization logs')?></h1>

<form class="form-horizontal">
<div class="salesforce-logs">
    <div class="filters-wrapper">
    <div class="row-fluid filters">
        <div class="span6">
            <div class="row-fluid">
                <p><?=Yii::t('salesforce', 'Last synchronization');?>: <strong><?=Yii::app()->localtime->toLocalDateTime($logModel->datetime)?></strong></p>
            </div>
            <div class="row-fluid">
                <p>
                    <?php
                        switch ($logModel->type_log){
                            case SalesforceLog::TYPE_LOG_USERS:
                                echo Yii::t('salesforce', 'Synchronized users');
                                break;
                            case SalesforceLog::TYPE_LOG_COURSES:
                                echo Yii::t('salesforce', 'Synchronized courses');
                                break;
                        }
                    ?>
                    <strong><?=$logModel->getItemsCount()?></strong>
                </p>
            </div>
        </div>
        <div class="span6">                        
            <?=CHtml::label(Yii::t('standard', '_EXPORT'), 'export', array(
                    'style' => 'display: inline-block;'
                ));
                    ?>                    
                <?= CHtml::dropDownList('export', 0, array(
                    0 => 'Choose an action',
                    LearningReportFilter::EXPORT_TYPE_XLS => Yii::t('standard', '_EXPORT_XLS'),
                    LearningReportFilter::EXPORT_TYPE_CSV => Yii::t('standard', '_EXPORT_CSV'),                    
                ), array(
                    'style' => 'display: inline-block'
                ));
            ?>            
        </div>
    </div>
        </div>
    <div class="form-wrapper">
        <?php
            $this->beginWidget('DoceboCGridView', array(
                'id' => 'salesforce-logs-grid',
                'htmlOptions' => array(
                    'class' => 'grid-view clearfix'
                ),                
                'dataProvider' => $dataProvider,
                'summaryText' => Yii::t('standard', '_TOTAL'),
                'template' => '{items}{pager}',
                'pager' => array(
                    'class' => 'DoceboCLinkPager'
                ),                
                'ajaxUpdate' => true,
                'columns' => array(
                    array(
                        'header' => Yii::t('standard', '_DESCRIPTION'),
                    		'value' => '$data'
                    ),
                )
            ));
            $this->endWidget();
        ?>
    </div>
    <div class="form-actions">
        <?=CHtml::button(Yii::t('standard', '_CLOSE'), array(
            'class' => 'btn btn-docebo black big close-dialog pull-right'
        ))?>
    </div>
</div>
</form>

<?php
    $progressUrl = Docebo::createAbsoluteLmsUrl('progress/run', array(
        'type'		=> Progress::JOBTYPE_EXPORT_SALESFORCE_LOG,
    ));
?>

<script type="text/javascript">

    $(function(){

        $('#export').on('change', function(){
            if ($(this).val() == '') {
                return true;
            }
            var url = '<?= $progressUrl ?>' + '&exportType=' + $(this).val();
            $('<div/>').dialog2({
                title: '',
                content: url,
                ajaxType: 'post',
                data: {
                    'logId' : '<?= $logModel->id ?>'
                },
                id: "progressive-salesforce-export-dialog"
            });
            return true;

        });


    });



</script>