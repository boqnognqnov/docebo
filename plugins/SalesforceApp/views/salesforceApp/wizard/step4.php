<div class="form">
<?php
$nextDialogUrl = Docebo::createLmsUrl('salesforceApp/axStep4', array('type' => 'wizarddemo'));
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'step4-form-id',
    'htmlOptions' => array(
        'name' => 'step4-form-name',
        'class' => 'ajax jwizard-form form-horizontal',
        )
));
echo CHtml::hiddenField('from_step', 'step4');
echo $form->hiddenField($salesforceLmsAccount, 'agent');
echo $form->hiddenField($salesforceLmsAccount, 'password');
echo $form->hiddenField($salesforceLmsAccount, 'token');
echo $form->hiddenField($salesforceLmsAccount, 'use_sandbox');


$allTypes = explode(',', $salesforceLmsAccount->sync_user_types);
if ( $salesforceLmsAccount->sync_user_types ) {
    if ( is_array($allTypes) ) {
        foreach ( $allTypes as $type) {
            echo CHtml::hiddenField('SalesforceLmsAccount[sync_user_types]['.$type.']', $type );
        }
    } else {
        echo CHtml::hiddenField('SalesforceLmsAccount[sync_user_types][]', $salesforceLmsAccount->sync_user_types );
    }
}
?>
    <div class="salesforce-sync">
        <p><?=Yii::t('salesforce', 'Docebo courses are synchronized with Salesforce as custom objects. However, you can replicate them also as Salesforce Products.').'<br/><br/>'.Yii::t('salesforce','Do you want to replicate Docebo courses as Salesforce products? (Only if the Docebo course is flagged to be synchronized with Salesforce)');?></p>
        <br/>
        <div class="row-fluid">            
                <?=CHtml::radioButtonList('SalesforceLmsAccount[replicate_courses_as_products]',(($salesforceLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_COURSES || $salesforceLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) ? SalesforceLmsAccount::TYPE_REPLICATE_COURSES : SalesforceLmsAccount::TYPE_REPLICATE_NONE) , array(
                    SalesforceLmsAccount::TYPE_REPLICATE_NONE => Yii::t('salesforce', 'No, replicate courses as custom objects only'),
                    SalesforceLmsAccount::TYPE_REPLICATE_COURSES => Yii::t('salesforce', 'Yes, replicate Docebo courses as Salesforce products')
                ), array(
                    'class'         => 'replicate-radio-btn',
                    'labelOptions'  => array('style' => 'display: inline;', 'class' => 'replicate-radio-btn')
                ))?>            
        </div>
        <?php        
        ?>
        <div id="sales_order" class="row-fluid <?=($salesforceLmsAccount->replicate_courses_as_products > SalesforceLmsAccount::TYPE_REPLICATE_NONE) ? "" : 'hidden'?>">
            <?=CHtml::checkBox('also_orders', ($salesforceLmsAccount->replicate_courses_as_products == SalesforceLmsAccount::TYPE_REPLICATE_SALES_ORDERS) ? true : false)?>
            <?= CHtml::label(Yii::t('salesforce', 'Also issue sales orders'),'also_orders', array(
                'style' => 'display : inline ; margin-left : 5px' )) ?>
        </div>
    </div>


<!-- Always use the Dialog4 feature "form-actions" - all buttons go in the modal footer -->
<div class="form-actions jwizard">
    <?php
    // Render Navigation button.
    // Variant 1: Not providing URL for next wizard step. In this case, form.action will be used, as usual
    echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
        'class' => 'btn btn-docebo green big jwizard-nav',
        'name' => 'prev_button',
        'data-jwizard-url' => '/lms/?r=salesforceApp/salesforceApp/axStep1'));

    echo CHtml::submitButton(Yii::t('standard', '_CONFIRM'), array('class' => 'btn btn-docebo green big jwizard-nav', 'name' => 'next_button'));

    // Variant 4: Adding attribute 'data-jwizard-url'='<next-step-url>'; on clicking this button, form.action will be replaced by this attribute
    // echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array('class' => 'btn btn-docebo black big jwizard-nav', 'data-jwizard-url' => '/lms/?r=usersSelector/axOpen&type=wizarddemo'));




    // Normal 'cancel' button, no special relation to jWizard
    echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-docebo black big close-dialog', 'name' => 'cancel_step'));

    ?>


</div>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">

    // You can set THIS STEP modal dialog class by anouncing it to jWizard.
    // After dialog update is finished, this class will be applied by jWizard event, internally
    jwizard.setDialogClass('jwizard-step4');
    
    $('.salesforce-sync input').styler();   

</script>
<style type="text/css">
    .jq-radio.replicate-radio-btn {
        margin-top: 5px;
    }
</style>