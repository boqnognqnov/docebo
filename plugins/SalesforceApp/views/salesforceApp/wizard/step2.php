<?php
$nextDialogUrl = Docebo::createLmsUrl('salesforceApp/axStep2', array('type' => 'wizarddemo'));
?>
<div class="step2-wrapper">
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'step2-form-id',
    'htmlOptions' => array(
        'name' => 'step2-form-name',
        'class' => 'ajax jwizard-form form-horizontal',
        )
));

echo CHtml::hiddenField('from_step', 'step2');
echo $form->hiddenField($salesforceLmsAccount, 'agent');
echo $form->hiddenField($salesforceLmsAccount, 'password');
echo $form->hiddenField($salesforceLmsAccount, 'token');
echo $form->hiddenField($salesforceLmsAccount, 'use_sandbox');

?>
<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

    // You can set THIS STEP modal dialog class by anouncing it to jWizard.
    // After dialog update is finished, this class will be applied by jWizard event, internally
    jwizard.setDialogClass('jwizard-step2');

    $(function(){
        
        //Delegate for patching dialog2 ajax-start event - adding extra html the the loader
        $(document).delegate('.modal', 'dialog2.ajax-start', function(){
            $('.modal-body').html("<span class=" + "loader" + "></span><br><div class=" + "loader-extra" + "><p class=" + "salesforceLead lead" + "><?=Yii::t('salesforce', 'Checking your data');?>...</p></div>");
        });         
        
        //Dialog2 should first load all the needed information about ajax loading and the executiong the submit.
        setTimeout(function(){            
            $('form#step2-form-id').submit();
            $(".step2-wrapper").html('');
        }, 1);
    });
</script>