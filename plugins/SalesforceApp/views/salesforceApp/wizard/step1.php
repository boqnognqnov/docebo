<!--  H1 tag is eaten by Dialog2 and set for Header -->
<h1><?= Yii::t('salesforce', 'Configure Salesforce App') ?></h1>

<?php

$nextDialogUrl = Docebo::createLmsUrl('salesforceApp/axStep1', array('type' => 'wizarddemo'));

/* @var $form CActiveForm */

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'step1-form-id',
    'htmlOptions' => array('class' => 'ajax jwizard-form form-horizontal', 'data-grid' => '#fields-grid', 'name' => 'step1-form-name',)
));
echo CHtml::hiddenField('from_step', 'step1');
?>

<div class="form-group row-fluid">
    <div class="span12">
        <b><?= Yii::t('standard', 'Welcome')?></b>
        <br />
        <?= Yii::t('salesforce', 'In order to start, please insert your Docebo API agent, password and token.') ?>
        <br />
        <?= Yii::t('salesforce', 'If you need any help to set up your Salesforce API agent or token please ')?>
        <a class="classic-link " href="https://www.docebo.com/knowledge-base/docebo-for-salesforce/" target="_blank"><?= Yii::t('salesforce', 'visit this page')?></a>
    </div>
</div>
<hr />
<?php if ( $error ):

	if(is_array($error)){
		foreach ($error as $err){
			?>
			<div class="row-fluid">
			    <div class="span12">
			        <div class="errorSummary"> <?= $err[0] ?> </div>
			    </div>
			</div>
			<?php
		}
	} else{
?>
<div class="row-fluid">
    <div class="span12">
        <div class="errorSummary"> <?= $error ?> </div>
    </div>
</div>
<?php } endif; ?>
<div class="form-group row-fluid">
    <div class="span12">
        <label class="control-label" for="api-agent">
            <?= Yii::t('salesforce', 'Salesforce API Agent') ?>
        </label>
        <span class="error"></span>
        <?php
        $optionsApiAgent = array(
            'class' => 'step1-input form-control jwizard span12',
            'id' => 'api-agent',
            'placeholder' => Yii::t('salesforce', 'API Agent'),
            'value' => $salesforceLmsAccount->getDecryptedField($salesforceLmsAccount->agent)
        );
        if(isset($salesforceLmsAccount->id_account)) $optionsApiAgent['disabled'] = 'disabled';
        ?>
        <?=$form->emailField($salesforceLmsAccount, 'agent', $optionsApiAgent)?>
    </div>
</div>

<div class="form-group row-fluid">
    <div class="span12">
        <label class="control-label" for="inputPassword">
            <?= Yii::t('standard', '_PASSWORD') ?>
        </label>
        <span class="error"></span>
        <?=$form->passwordField($salesforceLmsAccount, 'password', array(
            'class' => 'step1-input form-control jwizard span12',
            'placeholder' => Yii::t('standard', '_PASSWORD'),
            'id' => 'inputPassword',
        		'value' => $salesforceLmsAccount->getDecryptedField($salesforceLmsAccount->password)
        ))?>
    </div>
</div>

<div class="form-group row-fluid">
    <div class="span12">
            <label class="control-label" for="inputToken">
            <?= Yii::t('salesforce', 'Token') ?>
        </label>
        <span class="error"></span>
        <?=$form->passwordField($salesforceLmsAccount, 'token', array(
            'class' => 'step1-input form-control jwizard span12',
            'placeholder' => Yii::t('salesforce', 'Token'),
            'id' => 'inputToken',
        		'value' => $salesforceLmsAccount->getDecryptedField($salesforceLmsAccount->token)
        ))?>
    </div>
</div>

<?php if(!isset($salesforceLmsAccount->id_account)){ ?>
<br>
<div class="form-group row-fluid">
    <p><?php print Yii::t('salesforce', 'Once set the "Salesforce Installation Type" parameter, you will be able to change it only through the "Clear configuration" procedure.'); ?><br><br></p>
    <p><?php print Yii::t('salesforce', 'Salesforce Installation Type'); ?>:<br><br></p>
    <?php
         $data = array(
             '0'   => Yii::t('salesforce','Production environment'),
             '1'   => Yii::t('salesforce','Sandbox'),
         );
        echo $form->radioButtonList($salesforceLmsAccount, 'use_sandbox', $data, array(
            'class' => 'step1-input form-control jwizard',
            'uncheckValue'  => null,
            'template'  => '{input}{label}',
            'separator' => '&nbsp;&nbsp;',
            'labelOptions'  => array('style' => 'display: inline;'),
        ));
    ?>
</div>
<?php } ?>


<div class="form-actions jwizard">
    <?php
        echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
            'class' => 'btn btn-docebo green big jwizard-nav',
            'name' => 'next_button',
            'id' => 'step1-submit_btn'
        ));
        echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
            'class' => 'btn btn-docebo black big close-dialog',
            'name' => 'cancel_step'
        ));
    ?>
</div>
<?php $this->endWidget();/*echo CHtml::endForm();*/ ?>

<script type="text/javascript">
    jwizard.setDialogClass('jwizard-step1');
    //Remove delegate created in step2 if we click Previous button
    $(document).undelegate('.modal', 'dialog2.ajax-start');

    $(function(){
    	$('input[type="radio"]').styler();
    });


</script>