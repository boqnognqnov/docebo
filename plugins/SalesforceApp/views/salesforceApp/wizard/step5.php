<?php
/**
 * jWizard Demo - Step
 *
 *
 * For demonstration purposes and make live easier I put CSS in the view code, but it should be moved to external CSS file
 *
 */
?>

<style>
    <!--

    -->
</style>


<h1><?= Yii::t('salesforce', 'Configure Salesforce App') ?></h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'step5-form-id',
    'htmlOptions' => array(
        'name' => 'step5-form-name',
        'class' => 'ajax jwizard-form form-horizontal',
        )
));
    echo CHtml::hiddenField('from_step', 'step5');
?>

<div>
    <p class="setup-completed">&#xf00c;</p>
    <p style="text-align: center;"><?php print Yii::t('salesforce', 'Successfully connected'); ?></p>
</div>

<!-- Always use the Dialog5 feature "form-actions" - all buttons go in the modal footer -->
<div class="form-actions ">
    <?php
        echo CHtml::submitButton(Yii::t('iotask', '_FINISH'), array('class' => 'btn btn-docebo green big center jwizard-nav', 'name' => 'finnish_button'));
    ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">

    // You can set THIS STEP modal dialog class by anouncing it to jWizard.
    // After dialog update is finished, this class will be applied by jWizard event, internally
    jwizard.setDialogClass('jwizard-step5');


</script>