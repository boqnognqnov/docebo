<div class="form">
<?php

$nextDialogUrl = Docebo::createLmsUrl('salesforceApp/axStep3', array('type' => 'wizarddemo'));
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'step3-form-id',
    'htmlOptions' => array(
        'name' => 'step3-form-name',
        'class' => 'ajax jwizard-form',
        )
));

echo CHtml::hiddenField('from_step', 'step3');
echo $form->hiddenField($salesforceLmsAccount, 'agent');
echo $form->hiddenField($salesforceLmsAccount, 'password');
echo $form->hiddenField($salesforceLmsAccount, 'token');
echo $form->hiddenField($salesforceLmsAccount, 'use_sandbox');
echo CHtml::hiddenField('error', 'error');


$error = Yii::app()->request->getParam('error', null);


//Replace CHtml with ActiveFrom, when the model is ready!!!!
?>
    <?php if ($error == 'error') : ?>
        <div >
            <?php $this->widget('common.widgets.warningStrip.WarningStrip', array(
                'message'	=> Yii::t('salesforce', 'You must select at least one option!'),
                'type'		=>'warning',
            )); ?>
        </div><br/>
    <?php endif; ?>
<div class="salesforce-sync">
    <p><?=Yii::t('salesforce', 'Select what you would like to synchronize from Salesforce to Docebo')?></p>
    <br/>
    <?php
        $sync_user_types = explode(',', $salesforceLmsAccount->sync_user_types);
    ?>
    <div class="control-group">
        <div class="checkbox">
            <?=CHtml::checkBox('SalesforceLmsAccount[sync_user_types]['.SalesforceLmsAccount::SYNC_USER_TYPE_USERS.']', in_array(SalesforceLmsAccount::SYNC_USER_TYPE_USERS, $sync_user_types) ? true : false, array(
                'id' => 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_USERS,
                'value' => SalesforceLmsAccount::SYNC_USER_TYPE_USERS
            ));?>
            <?=CHtml::label(Yii::t('standard', '_USERS'), 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_USERS, array(
                'style' => 'display: inline-block;'
            ));?>
        </div>
    </div>
    <div class="control-group">
        <div class="checkbox">
            <?=CHtml::checkBox('SalesforceLmsAccount[sync_user_types]['.SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS.']', (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, $sync_user_types)) ? true : false, array(
                'id' => 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS,
                'value' => SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS
            ));?>
            <?=CHtml::label(Yii::t('profile', '_CONTACTS'), 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_CONTACTS, array(
                'style' => 'display: inline-block;'
            )); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="checkbox">
            <?=CHtml::checkBox('SalesforceLmsAccount[sync_user_types]['.SalesforceLmsAccount::SYNC_USER_TYPE_LEADS.']', (in_array(SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, $sync_user_types)) ? true : false, array(
                'id' => 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_LEADS,
                'class' => ($salesforceLmsAccount->sync_user_types == SalesforceLmsAccount::SYNC_USER_TYPE_USERS || $salesforceLmsAccount->sync_user_types == null) ? 'disabled' : '',
                'value' => SalesforceLmsAccount::SYNC_USER_TYPE_LEADS
            )); ?>
            <?=CHtml::label(Yii::t('salesforce', 'Leads'), 'user_type_' . SalesforceLmsAccount::SYNC_USER_TYPE_LEADS, array(
                'style' => 'display: inline-block;'
            )); ?>
        </div>
    </div>
</div>

    <!-- Always use the Dialog2 feature "form-actions" - all buttons go in the modal footer -->
<div class="form-actions jwizard">
    <?php
    // Render Navigation button.
    // Variant 1: Not providing URL for next wizard step. In this case, form.action will be used, as usual
    echo CHtml::submitButton(Yii::t('standard', '_PREV'), array(
        'class' => 'btn btn-docebo green big jwizard-nav',
        'name' => 'prev_button',
        /*'data-jwizard-url' => '/lms/?r=salesforceApp/salesforceApp/axStep2'*/));

    echo CHtml::submitButton(Yii::t('standard', '_NEXT'), array(
        'class' => 'btn btn-docebo green big jwizard-nav',
        'name' => 'next_button',
        'id' => 'step3-submit_btn'
    ));
    echo CHtml::button(Yii::t('standard', '_CANCEL'), array(
        'class' => 'btn btn-docebo black big close-dialog',
        'name' => 'cancel_step'
    ));
    ?>


</div>
</div>


<?php $this->endWidget(); ?>

<script type="text/javascript">
    // You can set THIS STEP modal dialog class by anouncing it to jWizard.
    // After dialog update is finished, this class will be applied by jWizard event, internally
    jwizard.setDialogClass('jwizard-step3');
    
    //Remove delegate from step2 if we click Next button
    $(document).undelegate('.modal', 'dialog2.ajax-start');

    $('input[type=checkbox]').styler();


</script>

<script>

    (
        function disableLeadOptionAtNoContactsRequired()
        {
            var contactsCheckbox = $('#user_type_contact');
            contactsCheckbox.change(
                function () {
                    if (!contactsCheckbox[0].checked) {
                        $('#user_type_lead')[0].checked = false;
                        $('#user_type_lead-styler').removeClass('checked');
                    }
                }
            );
        }
    )();
</script>