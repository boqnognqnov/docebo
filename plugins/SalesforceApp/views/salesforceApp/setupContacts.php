<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('menu', 'Salesforce') => Docebo::createAppUrl('admin:SalesforceApp/SalesforceApp/index'),
	Yii::t('salesforce', 'Contact Settings')
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;
?>
<h3 class="title-bold back-button">
    <a href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index')?>"><?=Yii::t('standard', '_BACK')?></a>
    <span><?=Yii::t('salesforce', 'Contact Settings')?></span>
</h3>

<?php if(!empty($errors)){
    print "<br>";
    foreach ($errors as $error) { ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="errorSummary"><?=$error?></div>
            </div>
        </div>
    <?php } } ?>

<?php
$ft = new FancyTree();
echo "<script src='" . $ft->getAssetsUrl() . '/jquery.fancytree-all.js' . "'></script>";
echo "<script src='" . $ft->getAssetsUrl() . '/jquery.fancytree.docebo.js' . "'></script>";
echo "<link rel='stylesheet' href='" . $ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css' . "' />";
//Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
?>

<?=CHtml::beginForm(); ?>
<div class="advanced-main sf-user-settings-blocks-list">
    <div class="section">
        <div class="row odd">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'List View') ?>
                </div>
                <div class="values">
                    <div><?= Yii::t('salesforce', 'Before syncing contacts from Salesforce, you must select accounts and contacts list views in order to import only the desired items.') ?></div>
                    <div class="listviewContainer">
                        <div class="listviewBlock">
                            <div style="margin-top: 10px"><?= Yii::t('salesforce', 'Accounts') ?></div>
                            <?=CHtml::dropDownList('accounts', $selectedAccountListview, $listAccounts)?>
                            <span id="total_accounts"<?=($selectedAccountListview)?"":" style='opacity: 0;'"?>><?=Yii::t('salesforce', 'Total accounts')?>: <b id="total_accounts_count"><?=($selectedAccountListview)?$listAccountsJs[$selectedAccountListview]:"0"?></b></span>
                        </div>
                        <div class="listviewBlock">
                            <div style="margin-top: 10px"><?= Yii::t('salesforce', 'Contacts') ?></div>
                            <?=CHtml::dropDownList('contacts', $selectedContactListview, $listContacts)?>
                            <span id="total_contacts"<?=($selectedContactListview)?"":" style='opacity: 0;'"?>><?=Yii::t('salesforce', 'Total contacts')?>: <b id="total_contacts_count"><?=($selectedContactListview)?$listContactsJs[$selectedContactListview]:"0"?></b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="row even">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('profile', '_CHANGEPASSWORD') ?>
				</div>
				<div class="values">
					<?=CHtml::checkBox('check_force_change_password', ($contactListviewModel->force_change_password == 1)?true:false, array(
						'id' => 'check_force_change_password',
						'value' => 1
					));?>
					<?=CHtml::label(Yii::t('salesforce', 'Force users to change their password at the first sign in'), 'check_force_change_password');?>
				</div>
			</div>
		</div>

        <div class="row odd" id="organizationChartGroup">
            <div class="row">

                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Organization chart') ?>
                </div>
                <div class="values" id="orgchartBlock">
                    <div><?= Yii::t('salesforce', 'Before syncing from Salesforce, you have to select a destination for the synchronized contacts.') ?></div>
                    <br/>

                    <?php
                    $singleBranchEnabled = ($accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH)?true:false;
                    $accountBasedEnabled = ($accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED)?true:false;
                    $additionalFieldsEnabled = ($accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS)?true:false;

                    if (!$singleBranchEnabled && !$accountBasedEnabled && !$additionalFieldsEnabled)
                        $singleBranchEnabled = true;
                    ?>

                    <div>
                        <?= CHtml::radioButton(
                            'organization_chart',
                            $singleBranchEnabled,
                            array('id'=> 'single_branch', 'value'=>SalesforceSyncListviews::ORGCHART_TYPE_SINGLE_BRANCH))
                        ?>
                        <label for="single_branch"><?=Yii::t('salesforce', 'Use single branch')?><span class="labelSmallInfo"><?=Yii::t('salesforce', 'Transfer all your contacts into one branch')?></span></label>
                    </div>
                    <br/>
                    <div>
                        <?= CHtml::radioButton(
                            'organization_chart',
                            $accountBasedEnabled,
                            array('id'=> 'account_based', 'value'=>SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED))
                        ?>
                        <label for="account_based"><?=Yii::t('salesforce', 'SF account based branches')?> <span class="limitListviewAccounts">(<?=Yii::t('salesforce', 'A maximum of {XX} accounts can be synced with this option', array('{XX}' => SalesforceSyncListviews::LISTVIEW_MAX_ACCOUNTS))?>)</span><span class="labelSmallInfo"><?=Yii::t('salesforce', 'Define your LMS Org Chart based on Salesforce accounts hierarchy')?></span></label>
                    </div>
                    <br/>
                    <div>
                        <?= CHtml::radioButton(
                            'organization_chart',
                            $additionalFieldsEnabled,
                            array('id'=> 'additional_fields', 'value'=>SalesforceSyncListviews::ORGCHART_TYPE_ADDITIONAL_FIELDS))
                        ?>
                        <label for="additional_fields"><?=Yii::t('salesforce', 'Use additional fields hierarchy')?><span class="labelSmallInfo"><?=Yii::t('salesforce', 'Define your LMS org Chart based on Salesforce fields (account level)')?></span></label>
                    </div>
                    <br/>
                    <div class="hierarchy-container">
                        <div class="hierarchy-block">
                            <?= CHtml::hiddenField('idOrgChart', ($accountListviewModel->orgchartId) ? $accountListviewModel->orgchartId:'', array('id' => 'selectedIdOrg')); ?>
                            <a
                                data-dialog-class="org-chart-popup-content"
                                class="sf-button open-dialog gray"
                                id="select-org-chart"
                                href="<?=Docebo::createAdminUrl('SalesforceApp/SalesforceApp/axSelectOrganizationChartNode', array('node' => $accountListviewModel->orgchartId, 'type' => 'contacts'))?>"
                                disabled="true"
                                >
                                <?=Yii::t('standard', '_SELECT');?>
                            </a>
                            <!-- <div class="sf-button" id="select-org-chart-dummy"><?=Yii::t('standard', '_SELECT');?></div> -->
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy1', $hierarchySelects['selected1'], $hierarchySelects['level1'], array('disabled' => 'disabled'))?>
                            <span class="smallInfo" id="hierarchy1Info"><?=Yii::t('salesforce', 'You can add max 3 levels');?></span>
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy2', $hierarchySelects['selected2'], $hierarchySelects['level2'], array('disabled' => 'disabled'))?>
                        </div>
                        <div class="hierarchy-block">
                            <?=CHtml::dropDownList('hierarchy3', $hierarchySelects['selected3'], $hierarchySelects['level3'], array('disabled' => 'disabled'))?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="row even" id="accountFields">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Account fields') ?>
                </div>
                <div class="values" id="accountFieldsBlock">
                    <div><?= Yii::t('salesforce', 'Import Salesforce account fields as Docebo additional user fields') ?></div>
                    <div class="additionalFieldSelect">
                        <?=CHtml::dropDownList('sf_fields_type_account', false, $salesforceAccountFields, array('id' => 'sf_fields_type_account', 'class'=>'fieldSelectorDropdown', 'disabled' => 'disabled')) ?>
                    </div>
                    <div class="additionalFieldsBoxTop boxTopAccount"<?=(empty($additionalAccountFields))?" style='display: none;'":""?>>
                        <div class="additionalFieldsFieldname additionalFieldsLabel"><?= Yii::t('salesforce', 'Salesforce field') ?></div>
												<div class="additionalFieldsRightPart additionalFieldsLabel">
													<?= Yii::t('salesforce', 'Assign to existing field') ?>
													&nbsp;&nbsp;
													<span class="hint">(<?= Yii::t('salesforce', 'Only compatible fields will be showed') ?>)</span>
												</div>
                    </div>
                    <br /><hr />
                    <hr class="additionalFieldsHrTop hrTopAccount"<?=(empty($additionalAccountFields)) ? " style='display: none;'" : ""?> />
                    <div id="additionalFieldsAccounts">
                        <?php /* NOTE: this part is initialized and filled by javascript on document.ready event */ ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row odd" id="contactFields">
            <div class="row">
                <div class="setting-name public-key-setting-name">
                    <?= Yii::t('salesforce', 'Contact fields') ?>
                </div>
                <div class="values" id="contactFieldsBlock">
                    <div><?= Yii::t('salesforce', 'Import Salesforce contact fields as Docebo additional user fields') ?></div>
                    <div class="additionalFieldSelect">
                        <?=CHtml::dropDownList('sf_fields_type_contact', false, $salesforceContactFields, array('id' => 'sf_fields_type_contact', 'class'=>'fieldSelectorDropdown', 'disabled' => 'disabled')) ?>
                    </div>
                    <div class="additionalFieldsBoxTop boxTopContact"<?=(empty($additionalContactFields))?" style='display: none;'":""?>>
                        <div class="additionalFieldsFieldname additionalFieldsLabel"><?= Yii::t('salesforce', 'Salesforce field') ?></div>
												<div class="additionalFieldsRightPart additionalFieldsLabel">
													<?= Yii::t('salesforce', 'Assign to existing field') ?>
													&nbsp;&nbsp;
													<span class="hint">(<?= Yii::t('salesforce', 'Only compatible fields will be showed') ?>)</span>
												</div>
                    </div>
                    <br /><hr />
                    <hr class="additionalFieldsHrTop hrTopContact"<?=(empty($additionalContactFields)) ? " style='display: none;'" : ""?> />
                    <div id="additionalFieldsContacts">
                        <?php /* NOTE: this part is initialized and filled by javascript on document.ready event */ ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row even">
            <div class="settingsButtons">
                <input class="btn-docebo green big" id="saveChanges" name="saveChanges" type="submit" value="<?=Yii::t('salesforce', 'Save changes')?>" />
                <input class="btn-docebo black big" id="cancelChanges" name="cancelChanges" type="submit" value="<?=Yii::t('standard', '_CANCEL')?>"/>
            </div>
        </div>
    </div>
</div>
<?=CHtml::endForm(); ?>


<script>
    $('#single_branch').styler();
    $('#account_based').styler();
    $('#additional_fields').styler();
	$('#check_force_change_password').styler();

    // Show/hide accounts counter
    $('#accounts').on('change', function(){
        var listAccounts = <?=json_encode($listAccountsJs)?>;
        var selectedItem = $('#accounts').val();
        var amount = listAccounts[selectedItem];
        if(amount == false) amount = 0;
        $('#total_accounts_count').text(amount);
        if($('#accounts').val() != ""){
            $('#total_accounts').css('opacity', 1);
        } else {
            $('#total_accounts').css('opacity', 0);
        }
    });

    // Show/hide contacts counter
    $('#contacts').on('change', function(){
        var listContacts = <?=json_encode($listContactsJs)?>;
        var selectedItem = $('#contacts').val();
        var amount = listContacts[selectedItem];
        if(amount == false) amount = 0;
        $('#total_contacts_count').text(amount);
        if($('#contacts').val() != ""){
            $('#total_contacts').css('opacity', 1);
        } else {
            $('#total_contacts').css('opacity', 0);
        }
    });



		var alreadyAssignedLmsFields = {user: [], account: [], contact: []};
		<?php
		if (is_array($alreadyAssignedLmsFields) && !empty($alreadyAssignedLmsFields)) {
			foreach ($alreadyAssignedLmsFields as $sfType => $fieldsList) {
				switch ($sfType) {
					case 'User':
						echo "\n".'alreadyAssignedLmsFields.user = ['.implode(',', $fieldsList).'];';
						break;
					case 'Account':
						echo "\n".'alreadyAssignedLmsFields.account = ['.implode(',', $fieldsList).'];';
						break;
					case 'Contact':
						echo "\n".'alreadyAssignedLmsFields.contact = ['.implode(',', $fieldsList).'];';
						break;
				}
			}
		}
		?>


		var alreadyAssignedSalesforceFields = {
			account: [<?php
				if (is_array($alreadyAssignedFields['Account']) && !empty($alreadyAssignedFields['Account'])) {
					$_keys = array_keys($alreadyAssignedFields['Account']);
					echo '"'.implode('","', $_keys).'"';
				}
				?>],
			'contact': [<?php
				if (is_array($alreadyAssignedFields['Contact']) && !empty($alreadyAssignedFields['Contact'])) {
					$_keys = array_keys($alreadyAssignedFields['Contact']);
					echo '"'.implode('","', $_keys).'"';
				}
				?>]
		};

		function updateFieldDropdown(context) {
			var _context = '';
			switch (context) {
				case 'Account':
				case 'account': _context = 'account'; break;
				case 'Contact':
				case 'contact': _context = 'contact'; break;
			}
			if (_context == '') return;
			var options = $('#sf_fields_type_'+_context+' option');
			if (options.length > 0) {
				$.map(options, function (option) {
					var value = $(option).attr('value');
					if (value != 0) {
						var name = value.split('|')[1];
						if (name && alreadyAssignedSalesforceFields[_context].indexOf(name) >= 0) {
							$(option).prop('disabled', true);
							$(option).removeClass('not-yet-selected');
							$(option).addClass('already-selected');
						} else {
							$(option).prop('disabled', false);
							$(option).removeClass('already-selected');
							$(option).addClass('not-yet-selected');
						}
					}
				});
			}
		}

		function updateFieldDropdowns() {
			updateFieldDropdown('account');
			updateFieldDropdown('contact');
		}


		var setupTranslations = {
			_CREATE_NEW_FIELD: <?= CJSON::encode(Yii::t('salesforce', 'Create new field')) ?>,
			_ADDITIONAL_FIELDS: <?= CJSON::encode(Yii::t('salesforce', 'Additional Fields')) ?>,
			_OR: <?= CJSON::encode(Yii::t('salesforce', 'OR')) ?>,
			_DELETE: <?= CJSON::encode(Yii::t('standard', '_DEL')) ?>,
			_CONFIRM: <?= CJSON::encode(Yii::t('standard', '_CONFIRM')) ?>,
			_CANCEL: <?= CJSON::encode(Yii::t('standard', '_CANCEL')) ?>
		};

		var lmsAdditionalFields = [
		<?php
		$first = true;
		foreach ($lmsAdditionalFields as $key => $label) {
			if ($first) { $first = false; } else { echo ','."\n"; }
			list($type, $name) = explode('|', $key);
			echo '{type: "'.$type.'", name: "'.$name.'", label: '.CJSON::encode($label).'}';
		}
		?>
		];

		var SalesforceFieldsMap = {
			<?php
			$first = true;
			foreach (SalesforceAdditionalFields::$SALESFORCE_FIELDS_TYPE_MAP as $sfType => $lmsType) {
				if ($first) { $first = false; } else { echo ','."\n"; }
				echo $sfType.': "'.$lmsType.'"';
			}
			?>
		};

		function getNewFieldRow(context, newFieldName, newFieldLabel, selectedField, filter) {
			var newBlock = '';
			if (newFieldName != 0) {
				var additionalFieldsExisting = $('.additionalFields'+context+'-item').length;
				if (additionalFieldsExisting == 0){
					var newKey = 0;
				} else {
					var newKey = parseInt($('.additionalFields'+context+'-item').last().attr('id').split('_')[1]) + 1;
				}
				var i, key, label, selected;
				newBlock += "<div class='additionalFields"+context+"-item' id='additionalFields"+context+"_"+newKey+"' style='border-bottom-width: 0px'>";
				newBlock += "   <input type='hidden' value='"+newFieldName+"' name='sf"+context+"FieldName_"+newKey+"' id='sf"+context+"FieldName_"+newKey+"'>";
				newBlock += "   <input type='hidden' value='"+newFieldLabel+"' name='sf"+context+"FieldLabel_"+newKey+"' id='sf"+context+"FieldLabel_"+newKey+"'>";
				newBlock += "   <div class='additionalFieldsBox'>";
				newBlock += "       <div class='additionalFieldsFieldname'>";
				newBlock += "           "+newFieldLabel;
				newBlock += "       </div>";
				newBlock += "       <div class='additionalFieldsRightPart'>";
				newBlock += "           <div class='additionalFieldsSelect'>";
				newBlock += "               <select name='"+context+"LmsFieldId_"+newKey+"' id='"+context+"LmsFieldId_"+newKey+"' class='lms-fields-selector'>";
				for (i=0; i<lmsAdditionalFields.length; i++) {
					if (i<=0 || !filter || (filter && filter == lmsAdditionalFields[i].type)) {
						var contextualCheck = false;
						switch (context.toLowerCase()) {
							case 'account': contextualCheck = alreadyAssignedLmsFields.contact.indexOf(parseInt(lmsAdditionalFields[i].name)) < 0; break;
							case 'contact': contextualCheck = alreadyAssignedLmsFields.account.indexOf(parseInt(lmsAdditionalFields[i].name)) < 0; break;
						}
						if (alreadyAssignedLmsFields.user.indexOf(parseInt(lmsAdditionalFields[i].name)) < 0 || contextualCheck) {
							key = (lmsAdditionalFields[i].type == '0' ? '0' : lmsAdditionalFields[i].type + '|' + lmsAdditionalFields[i].name);
							label = lmsAdditionalFields[i].label;
							selected = (selectedField && selectedField == lmsAdditionalFields[i].name);
							newBlock += "                   <option value='" + key + "'" + (selected ? " selected='selected'" : "") + ">" + label + "</option>";
						}
					}
				}
				newBlock += "               </select>";
				newBlock += "           </div>";
				/*newBlock += "           <div class='additionalFieldsOr'>";
				newBlock += "               " + setupTranslations._OR;
				newBlock += "           </div>";
				newBlock += "           <div class='additionalFieldsCreate'>";
				newBlock += "               <a class='sf-button popup-handler'";
				newBlock += "                   href='<?=Docebo::createAdminUrl('additionalFields/createFieldModal&type=textfield')?>'";
				newBlock += "                   data-modal-title='" + setupTranslations._ADDITIONAL_FIELDS + "'";
				newBlock += "                   data-modal-class='' >";
				newBlock += "                       " + setupTranslations._CREATE_NEW_FIELD;
				newBlock += "               </a>";
				newBlock += "               <div class='sf-button' style='display:none;'>" + setupTranslations._CREATE_NEW_FIELD+ "</div>";
				newBlock += "           </div>";*/
				newBlock += "           <div class='additionalFieldsDelete'>";
				newBlock += "               <a href='#' class='sf-button deleteAdditional"+context+"Field ajaxModal' id='deleteAdditional"+context+"Field_"+newKey+"' "
					+"data-toggle='modal' data-modal-class='delete-node' data-modal-title='" + setupTranslations._DELETE + "' "
					+"data-buttons='[{&quot;type&quot;: &quot;submit&quot;, &quot;title&quot;: &quot;" + setupTranslations._CONFIRM + "&quot;},{&quot;type&quot;: &quot;cancel&quot;, &quot;title&quot;: &quot;" + setupTranslations._CANCEL + "&quot;}]' "
					+"data-url='SalesforceApp/SalesforceApp/axFieldRemoveConfirm' "
					+"data-after-loading-content='function() { hideConfirmButton(); selectedFieldToRemove = $(&quot;#additionalFields"+context+"_"+newKey+"&quot;) }' "
					+"data-after-submit='afterRemoveFieldConfirm' "
					+"modal-request-type='GET' modal-request-data='{&quot;remove-key&quot;:" + newKey + "}' ></a>";
				newBlock += "               <div class='sf-button deleteAdditionalField' style='display:none;'></div>";
				newBlock += "           </div>";
				newBlock += "       </div>";
				newBlock += "   </div>";
				newBlock += "</div><hr/>";
			}
			return newBlock;
		}


		// Adding an additional account field
		$('#sf_fields_type_account').on('change', function(){
			var newFieldName = $('#sf_fields_type_account option:selected').attr('value');
			var newFieldLabel = $('#sf_fields_type_account option:selected').text();
			if(newFieldName != 0){
				$('.boxTopAccount').css('display', 'block');
				$('.htTopAccount').css('display', 'block');
				var _split = newFieldName.split('|');
				var sfType = _split[0];
				var sfName = _split[1];
				var lmsType = (SalesforceFieldsMap[sfType] || false);
				var newBlock = getNewFieldRow('Account', newFieldName, newFieldLabel, false, lmsType);
				$("#additionalFieldsAccounts").append(newBlock);
				$("#sf_fields_type_account").val('0');
				alreadyAssignedSalesforceFields.account.push(sfName);
				updateFieldDropdown('account');
				updateLmsFieldSelectors();
			}
			return false;
		});

		// Adding an additional contact field
		$('#sf_fields_type_contact').on('change', function(){
			var newFieldName = $('#sf_fields_type_contact option:selected').attr('value');
			var newFieldLabel = $('#sf_fields_type_contact option:selected').text();
			if (newFieldName != 0){
				$('.boxTopContact').css('display', 'block');
				$('.htTopContact').css('display', 'block');
				var _split = newFieldName.split('|');
				var sfType = _split[0];
				var sfName = _split[1];
				var lmsType = (SalesforceFieldsMap[sfType] || false);
				var newBlock = getNewFieldRow('Contact', newFieldName, newFieldLabel, false, lmsType);
				$("#additionalFieldsContacts").append(newBlock);
				$("#sf_fields_type_contact").val('0');
				alreadyAssignedSalesforceFields.contact.push(sfName);
				updateFieldDropdown('contact');
				updateLmsFieldSelectors();
			}
			return false;
		});

    function removeModal() {
        $('.modal.in').modal('hide');
        $('.modal.in').remove();
        $('#bootstrapContainer').remove();
        $('.modal-backdrop').remove();
    }

    // Passing orgchartId to hidden field
    $(document).off("dialog2.content-update", '.org-chart-popup-content').on("dialog2.content-update", '.org-chart-popup-content', function () {
        if ($(this).find("a.auto-close").length > 0) {
            var node = $(this).find("a.auto-close").data('node');
            if(node){
                $('input#selectedIdOrg').val(node);
            }
        }
    });


		var selectedFieldToRemove = null;

		function afterRemoveFieldConfirm()
		{
			if (selectedFieldToRemove) {

				var _split = $(selectedFieldToRemove).attr('id').split('_');
				var context = _split[0].replace('additionalFields', ''); //element ID is in the form "additionalFields{context}_{key}"
				var input = $("#sf"+context+"FieldName_" + _split[1]);
				context = context.toLowerCase();
				if (input && (context == 'account' || context == 'contact')) {
					var sfName = input.val().split('|')[1];
					var index = alreadyAssignedSalesforceFields[context].indexOf(sfName);
					if (index >= 0) {
						alreadyAssignedSalesforceFields[context].splice(index, 1);
					}
					updateFieldDropdown(context);
				}

				selectedFieldToRemove.next().remove();
				selectedFieldToRemove.remove();
				selectedFieldToRemove = null;
			}

			// If there is no additional field left, remove the title bar...
			var additionalFieldsRemaining = $('.additionalFieldsAccount-item').length;
			if (additionalFieldsRemaining < 1){
				$('.boxTopAccount').css('display', 'none');
				$('.hrTopAccount').css('display', 'none');
			}
			var additionalFieldsRemaining = $('.additionalFieldsContact-item').length;
			if (additionalFieldsRemaining < 1){
				$('.boxTopContact').css('display', 'none');
				$('.hrTopContact').css('display', 'none');
			}

			removeModal();

			return false;
		}


    function enableFieldsUI()
    {
        // Variables to check
        var accountListviewFilter = $('#accounts').val();
        var contactListviewFilter = $('#contacts').val();
        var totalAccountsCount = parseInt($('#total_accounts_count').html());
        if($("#single_branch").is(':checked')) orgchartType = 'single_branch';
        if($("#account_based").is(':checked')) orgchartType = 'account_based';
        if($("#additional_fields").is(':checked')) orgchartType = 'additional_fields';
        var orgchartId = $('#selectedIdOrg').val();
        var hierarchy1 = $('#hierarchy1').val();
			//check if the option "account based" was already checked when loading the page, we need to know if fields boxes have to be disabled or not
			var accountBasedWasAlreadyEnabled = <?= $accountListviewModel->orgchartType == SalesforceSyncListviews::ORGCHART_TYPE_ACCOUNT_BASED ? 'true' : 'false' ?>;

        // Properties to set
        var orgChartBlockOpacity = 0.5;
        var radioButtonsDisabled = true;

		var radioSingleBranchDisabled = true;
		var radioAccountBasedDisabled = true;
		var radioAdditionalFieldsDisabled = true;
		var continueAfterOrgChartType = true;
        var selectOrgChartDisplay = 'none';
        var selectOrgChartDummyDisplay = 'block';
        var hierarchyVisibility = 'hidden';
        var hierarchyDisabled = true;
        var fieldsBlockOpacity = 0.5;
        var sfFieldsTypeDisabled = true;
        var additionalFieldsSelectDisabled = true;
        var additionalFieldsCreateADisplay = 'none';
        var additionalFieldsCreateDivDisplay = 'block';
        var deleteAdditionalFieldADisplay = 'none';
        var deleteAdditionalFieldDivDisplay = 'block';
        var submitDisabled = true;
        var submitOpacity = 0.5;
		var maxListviewAccountsMessageDisplay = 'none';

		var listviewMaxAccounts = <?=SalesforceSyncListviews::LISTVIEW_MAX_ACCOUNTS?>;

        if(orgchartType == 'additional_fields'){
            hierarchyVisibility = 'visible';
        }

		console.log("orgcharttype: "+orgchartType);
		console.log("Accounts: "+totalAccountsCount+"/"+listviewMaxAccounts);

        if((accountListviewFilter != '') && (contactListviewFilter != '')){ // A listviewfilter has been set
            orgChartBlockOpacity = 1;
			radioSingleBranchDisabled = false;
			radioAdditionalFieldsDisabled = false;

			if(totalAccountsCount <= listviewMaxAccounts){ // only when the selected listview contains less than the max allowed accounts
				radioAccountBasedDisabled = false;
				maxListviewAccountsMessageDisplay = 'none';
			} else {
				maxListviewAccountsMessageDisplay = 'inline-block';
			}

            if(orgchartType != ''){ // The orgchart type has been chosen
                selectOrgChartDisplay = 'block';
                selectOrgChartDummyDisplay = 'none';

				// Additional check if number of accounts in the selected listview doesn't surpass the maximum allowed
				if((orgchartType == 'account_based') && (totalAccountsCount > listviewMaxAccounts)){
					if (!accountBasedWasAlreadyEnabled ) {
						continueAfterOrgChartType = false;
					}
				}

                if((orgchartId != '0') && orgchartId != '' && orgchartId != null && continueAfterOrgChartType == true){ // An orgchart node has been chosen
                    hierarchyDisabled = false;
                    if((orgchartType != 'additional_fields') || (hierarchy1 != '0')){
                        // Orgchart type isn't 'additional fields' or if selected, at least level 1 hierarchy has been selected
                        fieldsBlockOpacity = 1;
                        sfFieldsTypeDisabled = false;
                        additionalFieldsSelectDisabled = false;
                        additionalFieldsCreateADisplay = 'block';
                        additionalFieldsCreateDivDisplay = 'none';
                        deleteAdditionalFieldADisplay = 'block';
                        deleteAdditionalFieldDivDisplay = 'none';
                        submitDisabled = false;
                        submitOpacity = 1;
                    }
                }
            }
        }

        $('#orgchartBlock').css('opacity', orgChartBlockOpacity);
		$('#single_branch').prop('disabled', radioSingleBranchDisabled);
		$('#account_based').prop('disabled', radioAccountBasedDisabled);
		$('#additional_fields').prop('disabled', radioAdditionalFieldsDisabled);
		$('.limitListviewAccounts').css('display', maxListviewAccountsMessageDisplay);
        $('#select-org-chart').css('display', selectOrgChartDisplay);
        $('#select-org-chart-dummy').css('display', selectOrgChartDummyDisplay);
        $('#hierarchy1, #hierarchy2, #hierarchy3').prop('disabled', hierarchyDisabled);
        $('#hierarchy1, #hierarchy2, #hierarchy3, #hierarchy1Info').css('visibility', hierarchyVisibility);
        $('#accountFieldsBlock').css('opacity', fieldsBlockOpacity);
        $('#contactFieldsBlock').css('opacity', fieldsBlockOpacity);
        $('#sf_fields_type_account').prop('disabled', sfFieldsTypeDisabled);
        $('#sf_fields_type_contact').prop('disabled', sfFieldsTypeDisabled);
        $('.additionalFieldsSelect select').prop('disabled', additionalFieldsSelectDisabled);
        $('.additionalFieldsCreate a.sf-button').css('display', additionalFieldsCreateADisplay);
        $('.additionalFieldsCreate div.sf-button').css('display', additionalFieldsCreateDivDisplay);
        $('a.deleteAdditionalField').css('display', deleteAdditionalFieldADisplay);
        $('div.deleteAdditionalField').css('display', deleteAdditionalFieldDivDisplay);
        $('#saveChanges').prop('disabled', submitDisabled).css('opacity', submitOpacity);

		//handle radio buttons stylers
		var stylerSingleBranch = $("span#single_branch-styler");
		var stylerAccountBased = $("span#account_based-styler");
		var stylerAdditionalFields = $("span#additional_fields-styler");

		if(radioSingleBranchDisabled){
			stylerSingleBranch.addClass('disabled');
		} else {
			stylerSingleBranch.removeClass('disabled');
		}

		if(radioAccountBasedDisabled){
			stylerAccountBased.addClass('disabled');
		} else {
			stylerAccountBased.removeClass('disabled');
		}

		if(radioAdditionalFieldsDisabled){
			stylerAdditionalFields.addClass('disabled');
		} else {
			stylerAdditionalFields.removeClass('disabled');
		}
    }




		function updateLmsFieldSelectors() {

			//retrieve all field selectors in the page
			var selects = $('.lms-fields-selector');

			//check and store fields selector values
			var idField, selectedNow = [];
			selects.each(function(index, el) {
				var value = $(el).val();
				if (value != 0) {
					idField = parseInt(value.split('|')[1]);
					selectedNow.push(idField);
				}
			});

			//in each field selector, hide already selected fields
			var currentSelectedId;
			selects.each(function(index, el) {
				var value = $(el).val();
				currentSelectedId = (value != 0 ? parseInt(value.split('|')[1]) : 0);
				$(el).find('option').each(function(index, option) {
					var optionValue = $(option).attr('value');
					if (optionValue != 0) {
						var optionIdField = parseInt(optionValue.split('|')[1]);
						if (optionIdField) {
							var isAlreadyAssigned = (alreadyAssignedLmsFields.user.indexOf(optionIdField) >= 0);
							var isSelectedNow = (selectedNow.indexOf(optionIdField) >= 0);
							if ((isAlreadyAssigned || isSelectedNow) && optionIdField != currentSelectedId) {
								$(option).prop('disabled', true);
								$(option).removeClass('not-yet-selected');
								$(option).addClass('already-selected');
							} else {
								$(option).prop('disabled', false);
								$(option).removeClass('already-selected');
								$(option).addClass('not-yet-selected');
							}
						}
					}
				}) ;
			});

		}




		$(document).on('ready', function () {

			// Show/hide appropriate hierarchy selects
			$('.hierarchy-block select').bind('change', function () {
				var selectedItem = $(this).attr('id');
				var valueSelect1 = $('#hierarchy1 option:selected').attr('value');
				var valueSelect2 = $('#hierarchy2 option:selected').attr('value');
				var valueSelect3 = $('#hierarchy3 option:selected').attr('value');

				// If the first value is at default option, reset selects 2 and 3 as well in order not to pollute the results
				if(valueSelect1 == '0'){
					$('#hierarchy2').val('0');
					$('#hierarchy3').val('0');
					$('#hierarchy2 option:not(:first)').css('display', 'none');
					$('#hierarchy3 option:not(:first)').css('display', 'none');
					$('#hierarchy2').css('display', 'none');
					$('#hierarchy3').css('display', 'none');
				} else { // The first value isn't equal to default option
					var matchingLevel2Items = $("#hierarchy2 option[value^='"+valueSelect1+"|']").length;
					if(matchingLevel2Items == 0){
						$('#hierarchy2').val('0');
						$('#hierarchy3').val('0');
						$('#hierarchy2 option:not(:first)').css('display', 'none');
						$('#hierarchy3 option:not(:first)').css('display', 'none');
						$('#hierarchy2').css('display', 'none');
						$('#hierarchy3').css('display', 'none');
					} else {
						if(selectedItem == 'hierarchy1'){ // resetting select 2 & 3 on change
							valueSelect2 = '0';
							valueSelect3 = '0';
							$('#hierarchy2').val('0');
							$('#hierarchy3').val('0');
							$('#hierarchy3 option:not(:first)').css('display', 'none');
							$('#hierarchy3').css('display', 'none');
						}
						$('#hierarchy2').css('display', 'block');
						$('#hierarchy2 option:not(:first)').css('display', 'none');
						$("#hierarchy2 option[value^='"+valueSelect1+"|']").css('display', 'block');

						// If the second value is at default option, reset select 3 as well in order not to pollute the results
						if(valueSelect2 == '0'){
							$('#hierarchy3').val('0');
							$('#hierarchy3 option:not(:first)').css('display', 'none');
							$('#hierarchy3').css('display', 'none');
						} else { // The second value isn't equal to default option
							var matchingLevel3Items = $("#hierarchy3 option[value^='"+valueSelect2+"|']").length;
							if(matchingLevel3Items == 0){
								$('#hierarchy3').val('0');
								$('#hierarchy3 option:not(:first)').css('display', 'none');
								$('#hierarchy3').css('display', 'none');
							} else {
								if(selectedItem == 'hierarchy2'){ // resetting select 3 on change
									$('#hierarchy3').val('0');
								}
								$('#hierarchy3').css('display', 'block');
								$('#hierarchy3 option:not(:first)').css('display', 'none');
								$("#hierarchy3 option[value^='"+valueSelect2+"|']").css('display', 'block');
							}
						}
					}
				}
			});
			$('#hierarchy3').trigger('change');

			// Incrementally enable/disable blocks on the page
			// fields blocks initailization
			$('#accounts, #contacts, #single_branch, #account_based, #additional_fields, #selectedIdOrg, #hierarchy1').bind('change', enableFieldsUI);
			$('#accounts').trigger('change');

			//disable already assigned salesforce fields
			updateFieldDropdowns();

			//print assigned salesforce fields rows
			<?php
			if (is_array($additionalAccountFields) && !empty($additionalAccountFields)) {
				foreach ($additionalAccountFields as $key => $additionalField) {
					$afKey = $additionalField['sfFieldType']."|".$additionalField['sfFieldName'];
					$afLabel = CJSON::encode($additionalField['sfFieldLabel']);
					$lmsType = SalesforceAdditionalFields::$SALESFORCE_FIELDS_TYPE_MAP[$additionalField['sfFieldType']];
					$_lmsFieldId = 0;
					if (is_numeric($additionalField['lmsFieldId'])) {
						$_lmsFieldId = $additionalField['lmsFieldId'];
					} elseif (is_string($additionalField['lmsFieldId']) && strpos($additionalField['lmsFieldId'], '|') !== false) {
						$_arr = explode('|', $additionalField['lmsFieldId']);
						$_lmsFieldId = $_arr[1];
					}
					echo '$("#additionalFieldsAccounts").append( getNewFieldRow("Account", "'.$afKey.'", '.$afLabel.', '.((int)$_lmsFieldId > 0 ? (int)$_lmsFieldId : 'false').', '.CJSON::encode($lmsType).') );'."\n";
				}
			}
			if (is_array($additionalContactFields) && !empty($additionalContactFields)) {
				foreach ($additionalContactFields as $key => $additionalField) {
					$afKey = $additionalField['sfFieldType']."|".$additionalField['sfFieldName'];
					$afLabel = CJSON::encode($additionalField['sfFieldLabel']);
					$lmsType = SalesforceAdditionalFields::$SALESFORCE_FIELDS_TYPE_MAP[$additionalField['sfFieldType']];
					$_lmsFieldId = 0;
					if (is_numeric($additionalField['lmsFieldId'])) {
						$_lmsFieldId = $additionalField['lmsFieldId'];
					} elseif (is_string($additionalField['lmsFieldId']) && strpos($additionalField['lmsFieldId'], '|') !== false) {
						$_arr = explode('|', $additionalField['lmsFieldId']);
						$_lmsFieldId = $_arr[1];
					}
					echo '$("#additionalFieldsContacts").append( getNewFieldRow("Contact", "'.$afKey.'", '.$afLabel.', '.((int)$_lmsFieldId > 0 ? (int)$_lmsFieldId : 'false').', '.CJSON::encode($lmsType).') );'."\n";
				}
			}
			?>

			$('#additionalFieldsAccounts').on('change', '.lms-fields-selector', function(e) {
				updateLmsFieldSelectors();
			});
			$('#additionalFieldsContacts').on('change', '.lms-fields-selector', function(e) {
				updateLmsFieldSelectors();
			});
			updateLmsFieldSelectors();

		});


    $(document).on("dialog2.closed", ".modal", function () {
        if ($(this).find("a.auto-close").length > 0) {
            if ($('#selectedIdOrg')[0].value != '')
            {
                enableFieldsUI();
                $('#saveChanges')[0].style.opacity = '1';
                $('#cancelChanges')[0].style.opacity = '1';
                $('#saveChanges')[0].style.cursor = '';
                $('#cancelChanges')[0].style.cursor = '';
            }
        }
    });

    function enableOrgChartSection()
    {
        $('#organizationChartGroup').attr('style', '');
        $('#select-org-chart').attr('disabled', '');
        var nodeHtml =
            $('#select-org-chart')[0].outerHTML;

        $('#select-org-chart')[0].classList.remove('gray');
        $('#select-org-chart')[0].classList.add('green');

        $('#hierarchy1')[0].disabled = false;
        $('#hierarchy2')[0].disabled = false;
        $('#hierarchy3')[0].disabled = false;
    }

    $('#accounts').change(
        function ()
        {
            if ($('#accounts option:selected').attr('value') != '' && $('#contacts option:selected').attr('value') != '')
            {
                enableOrgChartSection();
            }
        }
    );
    $('#contacts').change(
        function ()
        {
            if ($('#accounts option:selected').attr('value') != '' && $('#contacts option:selected').attr('value') != '')
            {
                enableOrgChartSection();
            }
        }
    );

    $(document).on("dialog2.closed", ".modal", function () {
        if ($(this).find("a.auto-close").length > 0) {
            if ($('#selectedIdOrg')[0].value != '') {
                $('#sf_fields_type_account').parent().parent().parent().parent().attr('style', 'opacity: 1');
                $('#sf_fields_type_contact').parent().parent().parent().parent().attr('style', 'opacity: 1');
                $('#saveChanges').parent().parent().attr('style', 'opacity: 1');
                $('#saveChanges')[0].style = '';
                $('#cancelChanges')[0].style = '';
                $('#saveChanges').parent().parent().attr('style', '');
            }

        }
    });
</script>