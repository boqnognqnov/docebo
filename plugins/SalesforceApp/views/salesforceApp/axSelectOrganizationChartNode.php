<?php

$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray(false, true, true);
$orgchartsFancyTreeId = 'orgcharts-fancytree';
$preselectedBranches = array(array('key' => (int)$node, 'selectState' => 1));

?>
<h1><?= Yii::t('salesforce', 'Select Organization Chart Node') ?></h1>
<h2><?= Yii::t('salesforce', 'Organization Chart Node') ?></h2>
<div><?= Yii::t('salesforce', 'Select the origin of custom organization chart') ?></div>
<div id='orgcharts-fancytree'></div>

<script>

    function selectCallback(event, data) {
        $('#orgcharts_selected-json').val(data.node.data.idOrg);
    }
</script>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'select-node',
    'htmlOptions' => array(
        'class' => 'ajax',
        'name' => 'delete-client'
    ),
));

$this->widget('common.extensions.fancytree.FancyTree', array(
    'id' => $orgchartsFancyTreeId,
    'treeData' => $fancyTreeData,
    'selectMode' => 1,
    'preSelected' => $preselectedBranches,
    'formFieldNames' => array(
        'selectedInputName' => 'orgcharts_selected',
        'activeInputName' => 'orgcharts_active_node',
        'selectModeInputName' => 'orgcharts_select_mode',
    ),
    // http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeOptions
    'fancyOptions' => array(
        'checkbox' => true,
        'selectMode' => 1,
        'icons' => false,
        'filter' => array(),
    ),
    'htmlOptions' => array('class' => ''),
    'filterInputFieldName' => 'fancytree-filter-input',
    'filterClearElemId' => 'fancytree-filter-clear-button',

    // http://www.wwwendt.de/tech/fancytree/doc/jsdoc/global.html#FancytreeEvents
    'eventHandlers' => array(
        'click' => 'function(event, data){}',
        'blur' => 'function(event, data){}',
        'select' => 'function(event, data) {selectCallback(event, data);}',
        'beforePreselection' => 'function(event,data) {}',
        'afterPreselection' => 'function(event,data) {}',
    ),
));

?>


<div class="form-actions">
    <input id="confirm_btn" class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>"

        />
    <input id="cancel_btn" class="btn-docebo black big close-dialog" type="reset"
           value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script>

    $('.fancytree-wrapper').hide();
    /*
     $('.green').on('click', function (){
     $('.modal-backdrop').hide();
     $('#popup').hide();

     });
     $('.black').on('click', function (){
     $('#popup').hide();
     $('.modal-backdrop').hide();
     });
     */
    $('.modal-body')[0].style.padding = '10px';
    $('#orgcharts-fancytree')[0].style.height = '538px';

    function onConfirm() {
        alert('onConfirm')
        enableFieldsUI();
    }

</script>
