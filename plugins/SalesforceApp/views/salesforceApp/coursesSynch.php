<h1><?= Yii::t('salesforce', 'Select courses to synchronize'); ?></h1>
<?php 
	echo CHtml::beginForm('', 'POST', array(
		'class'	=> 'ajax',
		'id'	=> 'salesforce-course-submit-form'
	));

	echo CHtml::hiddenField('selected_catalogs');
	
    echo CHtml::hiddenField('catalogs_from_db', implode(',', $selectedIds), array('class' => 'selected-ids'));
    echo Chtml::hiddenField('selected-items', $selectionType, array('class' => 'selected-items'));

    $encodedIds = CJSON::encode($selectedIds);

?>

<div class="notification-selector-top-panel">
	<br>

	<div class="row-fluid">
		<div class="grey-wrapper" style="padding: 0px">
            <div style="border-top-color: white; border-top-style: solid; border-top-width: 1px; border-bottom-color: white; border-bottom-style: solid; border-bottom-width: 1px; ">
                <div class="row-fluid" style="margin: 17px">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span4 radio_all_courses" style="margin-right: -30px;" >
                                <label class="radio pull-left">
                                    <?php

                                    echo CHtml::radioButton('cfilter_option', 1, array(
                                        'value' => 1,
                                        'class' => 'r_courses'
                                    ));
                                    echo Yii::t('standard', '_ALL_COURSES');
                                    ?>
                                </label>
                            </div>
                            <div class="span4 radio_course_categories" style="margin-right: 25px;">
                                <label class="radio pull-left">
                                    <?php
                                    echo CHtml::radioButton('cfilter_option', 0, array(
                                        'value' => 2,
                                        'class' => 'r_courses'
                                    ));
                                    echo Yii::t('standard', 'Courses categories');
                                    ?>
                                </label>
                            </div>
                            <?php if ( PluginManager::isPluginActive('CoursecatalogApp') ): ?>
                            <div class="span4 radio_course_catalogs">
                                <label class="radio pull-left">
                                    <?php
                                    echo CHtml::radioButton('cfilter_option', 0, array(
                                        'value' => 3,
                                        'class' => 'r_courses'
                                    ));
                                    echo Yii::t('standard', 'Courses catalogs');
                                    ?>
                                </label>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>



<!--  $categoryTree->getCatalogTreeData()  -->

<div class="category-grid">
    <div class="row">
        <div class="span3 legend" style="width: 624px; padding: 0px">
            <div style="border-top-color: white; border-top-style: solid; border-top-width: 1px; border-bottom-color: white; border-bottom-style: solid; border-bottom-width: 1px; padding: 17px">
                <div class="left-selections pull-left clearfix" style="margin-top: -10px; margin-right: 18px;">
                    <p style="margin-left: -9px; margin-bottom: 1px">You have selected <strong><span id="combo-grid-selected-count2">0</span> items</strong>.</p>
                    <a style="margin-left: 1px" href="javascript:void(0);" class="combo-grid-select-all"> Select all </a>
                    <a href="javascript:void(0);" class="combo-grid-deselect-all"> Unselect all </a>
                </div>
                <span>
                    <span class="icon-select-node-yes"></span> <?php echo Yii::t('standard', '_YES');?>
                    <span class="icon-select-node-no"></span> <?php echo Yii::t('standard', '_NO');?>
                    <span class="icon-select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT');?>
                </span>
                <div class="search-input-wrapper pull-right">
                    <input
                        class="search-query ui-autocomplete-input" placeholder="Search"
                        id="search_input2" type="text" value=""
                        name="search_input2" autocomplete="off"
                        style="width: 120px; margin-top: -7px; margin-right: -9px;"
                        >
                        <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        <button id="fancytree-filter-clear-button2" type="button" class="close clear-search" style="margin-top: -7px; margin-right: -9px;">×</button>
                        <span class="perform-search search-icon" style="margin-top: -7px; margin-right: -9px;"></span>
                    </input>
                </div>
            </div>
        </div>
    </div>
    <?php
        $this->widget('common.extensions.fancytree.FancyTree', array(
            'id'            => $id,
            'treeData'      => $categoryTree2,
            'preSelected'   => $selectedIds,
            'filterInputFieldName' => 'search_input2',
            'filterClearElemId' => 'fancytree-filter-clear-button2',
            'eventHandlers' => array(
                'select' =>
                    'function(event, data) {
					    $("#combo-grid-selected-count2").html(data.tree.countSelected());
					}',
            ),


        ));
    ?>
</div>




<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm_write"
		value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" /> <input
		class="btn-docebo black big close-dialog" type="reset"
		value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
</div>


<?php
    CHTml::endForm();
?>

<div class="catalog-grid">
	
    <?php $this->widget('common.widgets.ComboGridView', array(
        'gridId' 					=> 'courses-synch-grid',
        'dataProvider' 				=> $catalogModel->sqlDataProvider( $filterObject ),
    	'disableMassActions' 		=> true,
    	'massSelectorsInFilterForm'	=> true,
    	'massSelectorUrl'			=> Docebo::createAbsoluteAdminUrl( 'SalesforceApp/SalesforceApp/coursesSynch', array() ),
    	'doNotCreateForm'			=> true,	
        'columns' => array(
            array(
            	'class' => 'CCheckBoxColumn',
				'id' 	=> 'items-grid-checkboxes',
				'value' => '$data["idCatalogue"]',
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top; padding: 6px 15px;'),
            ),
            array(
                'header'    => Yii::t('salesforce', 'Catalog Name'),
                'name'      => 'name',
            ),
            array(
                'header'    => Yii::t('course', '_COURSE_CONTENT'),
                'type'      => 'raw',
                'value'     => array($this, 'renderAvailableCoursesColumn'),
            ),
        )
    ));

    ?>
    
</div>


<script type="text/javascript">
    $(function() {
        $("<div><h2 style='color: #3e7dbf; font-weight: bold'><?= Yii::t('standard', 'Select Courses Catalogs')?></h2></div><hr style='margin: 0px !important;'>").insertBefore( $("#combo-grid") );

        var category = $("div.category-grid");
            category.hide();
        if ( $("div.catalog-grid").length ) {
            var catalog = $("div.catalog-grid");
            catalog.hide();
        }

        var selectedItems = $(".selected-items").val();
        if ( selectedItems == 'categories') {
            $('input[name="cfilter_option"][value=2]').prop('checked', true);
            catalog.hide();
            category.show();
        } else if ( selectedItems == 'catalogs') {
            $('input[name="cfilter_option"][value=3]').prop('checked', true);
            category.hide();
            catalog.show();
        } else {
            $('input[name="cfilter_option"][value=1]').prop('checked', true);
            category.hide();
            catalog.hide();
        }

        $('.radio_all_courses').on("click", function(){
            category.hide();
            catalog.hide();
        });
        $('.radio_course_categories').on("click", function(){
            category.show();
            catalog.hide();
        });
        $('.radio_course_catalogs').on("click", function(){
            category.hide();
            catalog.show();

            $('.filters-wrapper').next().hide();
        });

        $("input#cfilter_option").on("click", function(){
            var that = $(this);
            if ( that.is(":checked")) {
                switch ( that.val() ) {
                   case '1':
                       category.hide();
                       catalog.hide();
                       break;
                   case '2':
                       category.show();
                       catalog.hide();
                       break;
                   case '3':
                       category.hide();
                       catalog.show();
                       break;
                    default:
                        break;
               }
            }
        });

        var comboGrid = $("#courses-synch-grid");
        var encodedIds = <?= $encodedIds ?>;
        if ( $(".selected-items").val() == 'catalogs') {
            comboGrid.comboGridView('setSelection', <?= $encodedIds ?>);
        }


		$('#salesforce-course-submit-form').on('submit', function(){
			var selectedCatalogs = comboGrid.yiiGridView('getSelection');
			$('input[name="selected_catalogs"]').val(selectedCatalogs.join());
		});

        $('.r_courses').styler();

        $($('.jq-radio')[0]).on("click", function(){
            category.hide();
            catalog.hide();
        });
        $($('.jq-radio')[1]).on("click", function(){
            category.show();
            catalog.hide();
        });
        $($('.jq-radio')[2]).on("click", function(){
            category.hide();
            catalog.show();

            $('.filters-wrapper').next().hide();
        });

        // Handle Select All clicks in Org Chart tree
        $(".combo-grid-select-all").on("click", function(){
            var tree = $('#<?= $id ?>').fancytree('getTree');
            tree.rootNode.children[0].setSelectState(2);
        });

        // Handle De-Select All clicks in Org Chart tree
        $(".combo-grid-deselect-all").on("click", function(){
            var tree = $('#<?= $id ?>').fancytree('getTree');
            tree.rootNode.children[0].setSelectState(0);
        });


    });
</script>