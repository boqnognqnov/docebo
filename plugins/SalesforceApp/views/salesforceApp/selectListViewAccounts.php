<h1><?=Yii::t('salesforce', 'Setup accounts')?></h1>
<p><?=Yii::t('salesforce', 'Before synchronizing Accounts, you must select a Salesforce filter in order to import the desired accounts.')?></p>
<br />

<?php
print CHtml::beginForm('', 'POST', array(
	'id'	=> 'salesforce-setup-accounts-form',
	'class' => 'ajax'
));
?>
<?php if($error){ ?>
<div class="row-fluid">
	<div class="span12">
		<div class="errorSummary"><?=$error?></div>
	</div>
</div>
<?php } ?>
<div class="row-fluid">
	<div class="span12">
		<label for="salesforce-account-filter"><?=Yii::t('salesforce', 'Accounts'); ?></label>
		<?=CHtml::dropDownList('salesforce-account-filter', $selectedAccount, $listAccounts)?>
	</div>
</div>

<div class="form-actions">
	<input class="btn-docebo green big confirm-btn" type="submit" name="confirm_write" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>" />
</div>
<?php CHtml::endForm(); ?>
