<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'Salesforce'
);

if (is_array($breadcrumbs)) {
	$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
} else {
	$_breadcrumbs[] = $breadcrumbs;
}

$idAccount = $salesforceLmsAccount->id_account;

$this->breadcrumbs = $_breadcrumbs;
?>
<div class="salesforce-config">
	<div class="row-fluid">
		<div class="span6">
			<h2>Salesforce</h2>
		</div>
		<div class="span6">
			<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual pull-right">
				<?= Yii::t('apps', 'Read Manual'); ?>
			</a>
		</div>
	</div>
	<hr />
	<div class="row-fluid">
		<div class="span6">
			<span class="tick"></span><p class="lead"><?=Yii::t('salesforce', '<strong>Salesforce APP</strong> has been configured correctly');?></p>
		</div>
		<div class="span6">

			<a href="index.php?r=SalesforceApp/SalesforceApp/clearConfig&account=<?=$idAccount?>"
			   style="margin-left: 15px;"
			   class="open-dialog btn btn-docebo red big open-dialog pull-right"
			   data-dialog-class="salesforce-dialog-clear-config"
			   title="<?= Yii::t("salesforce", "Clear configuration") ?>"
				>
				<?=Yii::t('salesforce', 'Clear configuration');?>
			</a>

			<a href="index.php?r=SalesforceApp/SalesforceApp/axOpen&account=<?=$idAccount?>"
			   class="open-dialog wizard-starter btn btn-docebo blue big open-dialog pull-right"
			   data-dialog-class="salesforce-dialog-step1"
			   data-wizard-dispatcher-url="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/axWizardDispatcher', array(
				   'id' => $idAccount
			   ))?>"

			   data-bootstro-id="bootstroConfigureSalesforce"
			   rel="wizard-demo"
			   title="Set up Wizard"
				>
				<?=Yii::t('salesforce', 'Edit main configuration');?>
			</a>
		</div>
	</div>
	<br/>
	<div class="row-fluid odd row-item-sf">
		<div class="span2">
			<p class="lead">
				<?=Yii::t('salesforce', 'Synchronization frequency');?>
			</p>
		</div>
		<div class="span10">
			<div class="form">
				<?=CHtml::beginForm(); ?>
				<div class="control-label">
					<?=CHtml::label(Yii::t('salesforce', 'Launch the automatic Docebo <i class="icon-retweet"></i> Salesforce synchronization') ,'sync_freq', array('style'=>'margin-bottom: 10px'));?>
					<?=CHtml::dropDownList('SalesforceLmsAccount[sync_frequency]', $salesforceLmsAccount->sync_frequency, array(
						1 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 1)),
						4 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 4)),
						8 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 8)),
						16 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 16)),
						24 => Yii::t('salesforce', 'Every {hour} hours', array('{hour}' => 24)),
						26280 => Yii::t('salesforce', 'Manually'),
					), array('style' => 'width: 400px'))?>
					<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
						'class' => 'btn btn-docebo green big'
					));?>
				</div>
				<?=CHtml::endForm(); ?>
			</div>
		</div>
	</div>
	<?php

	$swapOddEven = false;

	if($enableAccountBlock){
		$swapOddEven = true;
		?>
		<div class="row-fluid even row-item-sf">
			<!-- ************  Accounts - title block  ********************* -->
			<div class="span2">
				<p class="lead">
					<?=Yii::t('salesforce', 'Accounts');?>
				</p>
				<p class="muted">
					<?=Yii::t('salesforce', 'Sync all Salesforce accounts and make them Docebo organization chart branches');?>
				</p>
			</div>
			<div class="span10">
				<div class="row-fluid">
					<!-- ************  Accounts - Settings button  ********************* -->
					<div class="span2">
						<a href="<?= Yii::app()->controller->createUrl("selectListviewAccounts", array("id_account" => $idAccount)) ?>"
						   data-dialog-class="salesforce-dialog-select-listview-accounts"
						   class="open-dialog btn btn-docebo green big">
							<?=Yii::t('standard', 'Settings');?>
						</a>
					</div>
					<!-- ************  Accounts - Settings message  ********************* -->
					<?php
					if($listviewAccount){ // Accounts have been set
						?>
						<div class="span10">
							<div class="row"><?=Yii::t('salesforce', 'Account filter')?>: <strong><?=$listviewAccount->listviewName?></strong></div>
						</div>
						<?php
					} else { // Accounts have not yet been set
						?>
						<div class="span10">
							<div class="settings-message-box">
								<div class="settings-arrow"></div>
								<div class="settings-message-content"><?=Yii::t('salesforce', '<span>Start here</span> and <span>sync your Accounts</span> for your first usage')?></div>
							</div>
						</div>
					<?php } ?>
				</div>
				<br />
				<div class="row-fluid">
					<!-- ************  Accounts - Sync now button  ********************* -->
					<div id="sync-accounts-holder" class="span2">
						<?php
						if(!$listviewAccount) { // Accounts have not yet been set
							?>
							<a class="btn btn-docebo blue big" disabled="disabled"><?=Yii::t('salesforce', 'Sync now');?></a>
							<?php
						} elseif($startedJobs[SalesforceLog::TYPE_LOG_ACCOUNTS]){ // During sync process
							?>
							<div class='span3 text-uppercase'><img src='../../../../themes/spt/images/ajax-loader10.gif'>
							</div>
							<div class='span9'><p class='lead text-uppercase'><strong><?= Yii::t('salesforce', 'Syncing') ?>
										<br/><?= Yii::t('salesforce', 'Accounts') ?>...</strong></p></div>
							<?php
						} else { // Else: show button
							?>
							<a id="sync_accounts" data-type="<?=  SalesforceLog::TYPE_LOG_ACCOUNTS;?>" data-account="<?=$idAccount;?>" class="btn btn-docebo blue big indented-vertically"><?=Yii::t('salesforce', 'Sync now');?></a>
						<?php } ?>
					</div>
					<!-- ************  Accounts - Sync now message  ********************* -->
					<div class="span10">
						<?php
						if($listviewAccount && (empty($accountsLog))) {
							// Account settings have been set, but have not yet been synced
							?>
							<div class="settings-message-box">
								<div class="settings-arrow"></div>
								<div class="settings-message-content green"><?= Yii::t('salesforce', 'Sync now!') ?></div>
							</div>
							<?php
						} elseif($startedJobs[SalesforceLog::TYPE_LOG_ACCOUNTS]){ // During sync process
							// nothing to show here during syncing.....
						} elseif($listviewAccount && (!empty($accountsLog) /*&& $accountsLog->getItemsCount() > 0*/)){
							// Account settings have been set and have already been synced
							?>
							<div class="row">
								<p><?=Yii::t('salesforce', 'Last synchronization: ')?> <strong><span id='accounts_last_date'><?=(!empty($accountsLog)) ? Yii::app()->localtime->toLocalDateTime($accountsLog->datetime) : Yii::t('standard', '_NEVER');?> </span></strong>
									<?php $errorLog = json_decode(SalesforceLog::getLastAccountsLog($idAccount)->data, true);
									if ( !empty( $errorLog )){ ?> |
										<span class="error-log-visibility">
                                <a href="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/viewLog', array(
									'type'      => SalesforceLog::TYPE_LOG_ACCOUNTS,
									'account'   => $idAccount
								))?>"
								   class='underline blue-text open-dialog'><?=Yii::t('salesforce', 'View logs')?>
								</a>
                            </span>
									<?php } ?>
								</p>
							</div>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Synchronized accounts');?>:
									<span id="accounts_items_count"><?=(!empty($accountsLog)) ? $accountsLog->getItemsCount() : 0;?></span>
								</p>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<div class="row-fluid row-item-sf <?=($swapOddEven) ? 'odd' : 'even' ?>">
		<!-- ************  Users - title block  ********************* -->
		<div class="span2">
			<p class="lead">
				<?=Yii::t('standard', '_USERS');?>
			</p>
			<p class="muted">
				<?=Yii::t('salesforce', 'Sync all Salesforce users and make them Docebo users');?>
			</p>
		</div>
		<div class="span10">
			<div class="row-fluid">
				<!-- ************  Users - Settings button  ********************* -->
				<div class="span2">
					<?php
					if($enableAccountBlock && ($existingAccounts == 0)){
						// Account block is enabled AND accounts have not been set or accounts have not yet been synced
						?>
						<a class="btn btn-docebo green big" disabled="disabled"><?=Yii::t('standard', 'Settings');?></a>
					<?php } else { ?>
						<a href="<?= Yii::app()->controller->createUrl("selectListviewUsers", array("id_account" => $idAccount)) ?>"
						   data-dialog-class="salesforce-dialog-select-listview-users"
						   class="open-dialog btn btn-docebo green big">
							<?=Yii::t('standard', 'Settings');?>
						</a>
					<?php } ?>
				</div>
				<!-- ************  Users - Settings message  ********************* -->
				<div class="span10">
					<?php
					if($enableAccountBlock && ($existingAccounts == 0)){
						// Account block is enabled AND accounts have not been set or accounts have not yet been synced
						?>
						<div class="row"><?=Yii::t('salesforce', "You <strong>must sync your Accounts first</strong>, then you'll be able to sync Users.")?></div>
					<?php } elseif ($hasSelectedUserSettingRequiredFields) { ?>
						<?php if ($enabledUserFilter === true) { ?>
							<div class="row"><?=Yii::t('salesforce', 'User filter')?>: <strong><?= $selectedUserFilter->listviewName ?></strong></div>
						<?php } ?>
						<?php if ($enabledContactFilter === true) { ?>
							<div class="row"><?=Yii::t('salesforce', 'Contact filter')?>: <strong><?= $selectedContactFilter->listviewName ?></strong></div>
						<?php } ?>
						<?php if ($enabledLeadFilter === true) { ?>
							<div class="row"><?=Yii::t('salesforce', 'Lead filter')?>: <strong><?= $selectedLeadFilter->listviewName ?></strong></div>
						<?php } ?>
					<?php } else { ?>
						<div class="settings-message-box">
							<div class="settings-arrow"></div>
							<div class="settings-message-content"><?=Yii::t('salesforce', 'Now <span>select</span> <span>your Users</span>')?></div>
						</div>
					<?php } ?>
				</div>
			</div>
			<br />
			<div class="row-fluid">
				<!-- ************  Users - Sync now button  ********************* -->
				<div id="sync-users-holder" class="span2">
					<?php if(($enableAccountBlock && ($existingAccounts == 0)) || !$hasSelectedUserSettingRequiredFields){
						// Account block is enabled AND accounts  have not been set or accounts have not yet been synced, or users settings have not been specified
						?>
						<a class="btn btn-docebo blue big" disabled="disabled"><?=Yii::t('salesforce', 'Sync now');?></a>
					<?php } elseif($startedJobs[SalesforceLog::TYPE_LOG_USERS]){ ?>
						<div class='span3 text-uppercase'><img src='../../../../themes/spt/images/ajax-loader10.gif'></div><div class='span9'><p class='lead text-uppercase'><strong><?=Yii::t('salesforce', 'Syncing') ?><br/><?=Yii::t('standard', '_USERS')?>...</strong></p></div>
					<?php } else { ?>
						<a id="sync_users" data-type="<?=  SalesforceLog::TYPE_LOG_USERS?>" data-account="<?=$idAccount?>" class="btn btn-docebo blue big indented-vertically"><?=Yii::t('salesforce', 'Sync now');?></a>
					<?php } ?>
				</div>
				<!-- ************  Users - Sync now message  ********************* -->
				<div class="span10">
					<?php if($hasSelectedUserSettingRequiredFields && empty($usersLog)){
						// User settings finished AND users have not yet been synced
						?>
						<div class="settings-message-box">
							<div class="settings-arrow"></div>
							<div class="settings-message-content green"><?= Yii::t('salesforce', 'Sync now!') ?></div>
						</div>
					<?php } elseif($hasSelectedUserSettingRequiredFields && !empty($usersLog)){
						// User settings finished AND users have been synced
						?>
						<div class="row">
							<p><?=Yii::t('salesforce', 'Last synchronization: ')?><strong><span id='users_last_date'> <?=(!empty($usersLog)) ? Yii::app()->localtime->toLocalDateTime($usersLog->datetime) : Yii::t('standard', '_NEVER');?></span></strong>
								<?php $errorLog = json_decode(SalesforceLog::getLastUsersLog($idAccount)->data, true);
								if ( !empty( $errorLog )): ?> |
									<span class="error-log-visibility">
                                <a href="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/viewLog', array(
									'type' => SalesforceLog::TYPE_LOG_USERS,
									'account' => $idAccount
								))?>" class='underline blue-text open-dialog'><?=Yii::t('salesforce', 'View logs');?>
								</a>
                            </span>
								<?php endif; ?>
							</p>
						</div>
						<div class="row">
							<p>
								<?=Yii::t('salesforce', 'Synchronized users');?>:
								<strong><span id="users_items_count"><?=(!empty($usersLog)) ? $usersLog->getItemsCount() : 0;?></span></strong>
							</p>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row-fluid row-item-sf <?=(!$swapOddEven) ? 'odd' : 'even' ?>">
		<div class="span2">
			<p class="lead">
				<?=Yii::t('standard', '_COURSES');?>
			</p>
			<p class="muted">
				<?=Yii::t('salesforce', 'Push selected Docebo courses or courses catalogs to Salesforce');?>
			</p>
		</div>
		<div class="span10">
			<div class="row-fluid">
				<div class="span2">
					<a href="index.php?r=SalesforceApp/SalesforceApp/coursesSynch&id_account=<?= $idAccount ?>"
					   data-dialog-class="salesforce-dialog-select-courses"
					   class="open-dialog btn btn-docebo green big indented-vertically">
						<?=Yii::t('standard', '_SELECT');?>
					</a>
				</div>
				<div class="span10">
					<?php  ?>
					<?php switch ( $salesforceLmsAccount->getSelectionType() ):
						case SalesforceLmsAccount::SYNC_COURSE_TYPE_ALL: ?>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected: <strong>all</strong>');?>
								</p>
							</div>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected courses')?>: <strong><?=count($selectedItems['courses'])?></strong>,
									<?=Yii::t('myactivities', 'Learning plans')?>: <strong><?=count($selectedItems['plans'])?></strong>
								</p>
							</div>
							<?php break;?>
						<?php case SalesforceLmsAccount::SYNC_COURSE_TYPE_CATEGORIES: ;?>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected categories')?>: <strong><?=count($selectedItems['categories'])?></strong>
								</p>
							</div>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected courses')?>: <strong><?=count($selectedItems['courses'])?></strong>
								</p>
							</div>
							<?php break;?>
						<?php case SalesforceLmsAccount::SYNC_COURSE_TYPE_CATALOGS: ;?>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected catalogs')?>: <strong><?=$selectedItemsCount?></strong>
								</p>
							</div>
							<div class="row">
								<p>
									<?=Yii::t('salesforce', 'Selected courses')?>: <strong><?=count($selectedItems['courses'])?></strong>,
									<?= Yii::t('myactivities','Learning plans')?>: <strong><?=count($selectedItems['plans'])?></strong>
								</p>
							</div>
							<?php break;?>
						<?php case 'default': break;?>
						<?php endswitch; ?>
				</div>
			</div>
			<br/>
			<div class="row-fluid">
				<div id="sync-courses-holder" class="span2">
					<?php
					if($startedJobs[SalesforceLog::TYPE_LOG_COURSES]):
						?>
						<div class='span3 text-uppercase'><img src='../../../../themes/spt/images/ajax-loader10.gif'></div><div class='span9'><p class='lead text-uppercase'><strong><?=Yii::t('salesforce', 'Syncing') ?><br/><?=Yii::t('standard', '_COURSES')?>...</strong></p></div>
						<?php
					else:
						?>
						<a id="sync_courses" data-type="<?=  SalesforceLog::TYPE_LOG_COURSES?>" data-account="<?=$idAccount?>" class="btn btn-docebo blue big indented-vertically"><?=Yii::t('salesforce', 'Sync now');?></a>
					<?php endif;?>
				</div>
				<div class="span10">
					<div class="row">
						<p><?=Yii::t('salesforce', 'Last synchronization: ')?><strong><span id='courses_last_date'> <?=(!empty($coursesLog)) ? Yii::app()->localtime->toLocalDateTime($coursesLog->datetime) : Yii::t('standard', '_NEVER');?></span></strong>
							<?php $errorLog = json_decode(SalesforceLog::getLastCoursesLog($idAccount)->data, true);
							if ( !empty( $errorLog )): ?> |
								<span class="error-log-visibility">
                                <a href="<?=Docebo::createAbsoluteUrl('SalesforceApp/SalesforceApp/viewLog', array(
									'type'      => SalesforceLog::TYPE_LOG_COURSES,
									'account'   => $idAccount
								))?>" class='underline blue-text open-dialog'><?=Yii::t('salesforce', 'View logs');?>
								</a>
                           </span>
							<?php endif; ?>
						</p>
					</div>
					<div class="row">
						<p>
							<?=Yii::t('salesforce', 'Synchronized items'); ?>:
							<strong><span id="courses_items_count"><?=(!empty($coursesLog)) ? $coursesLog->getItemsCount() : 0;?></span></strong>
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>

<script type="text/javascript">
	var salesforceOptions = {};
	SalesforceAppManager = new SalesforceApp(salesforceOptions);

	<?php
		if(!empty($startedJobs)):

		$type = null;
		$jobHash = null;

		if(isset($startedJobs[SalesforceLog::TYPE_LOG_USERS])){
			$type = 'users';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_USERS];
		} elseif(isset($startedJobs[SalesforceLog::TYPE_LOG_ACCOUNTS])){
			$type = 'accounts';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_ACCOUNTS];
		} elseif(isset($startedJobs[SalesforceLog::TYPE_LOG_COURSES])){
			$type = 'courses';
			$jobHash = $startedJobs[SalesforceLog::TYPE_LOG_COURSES];
		}
		?>
	$(function(){
		var waitForSuccess = setInterval(function() {
			$.ajax({
				url: HTTP_HOST + '?r=SalesforceApp/SalesforceApp/waitForSync',
				data: {'job_id': "<?=$jobHash?>"},
				success: function (res) {
					console.log(res);
					if (res.success == true) {
						clearInterval(waitForSuccess);
						var html = "<div class='span3 text-uppercase'><span class='success-tick'><span></div><div class='span9'><p class='lead text-uppercase'><strong>" + Yii.t('salesforce', 'Syncing') + "<br/><p class='green-text text-uppercase'>" + Yii.t('standard', '_COMPLETED') + "</p></strong></p></div>";
						$('#sync-<?=$type?>-holder').html(html);
						$('#<?=$type?>_items_count').html(res.itemsCount);
						$('#<?=$type?>_last_date').html(res.lastDate);
						window.location.reload();

					}
				}
			});
		}, 3000);
	});
	<?php
		endif;
	?>

	$(document).on('click', "a[id^='sync_']", function(){
		var waitForSuccess = setInterval(function(){
			if($('span.success-tick').length > 0){
				clearInterval(waitForSuccess);
				console.log("sync finished successfully!!!");
				var waitToReload = setTimeout(function(){
					window.location.reload();
				}, 2000);
			}
			console.log("sync button was clicked!!!");
		}, 3000);
	});
</script>
