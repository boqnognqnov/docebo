<?php
/* @var $form CActiveForm */
/* @var $settings SalesforceAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/knowledge-base/docebo-per-salesforce/' : 'http://www.docebo.com/knowledge-base/docebo-for-salesforce/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2>Salesforce</h2>
<br/>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'span2'));?>
    <div class="controls">
        <a class="" href="http://www.salesforce.com/" target="_blank">http://www.salesforce.com</a>
    </div>
</div>

<hr/>

<div class="control-group">
    <label class="span2"><?=Yii::t('configuration', '_URL');?> :</label>
    <div class="controls">
        <?= Settings::get('url'); ?>
    </div>
</div>

<hr/>

<div class="control-group">
	<label class="span2">API Key :</label>
	<div class="controls">
		<?=$GLOBALS['cfg']['api_key'];?>
	</div>
</div>

<div class="control-group">
	<label class="span2">API Secret :</label>
	<div class="controls">
		<?=$GLOBALS['cfg']['api_secret'];?>
	</div>
</div>

<hr/>

<div class="control-group">
	<label class="span2"><?=Yii::t('configuration', '_SSO_TOKEN');?> :</label>
	<div class="controls">
		<? if (Settings::get('sso_token') == 'on') {
			echo Settings::get('sso_secret');
		} else {
			echo '<strong>' . Yii::t('standard', '_OFF') . '</strong>'
				.' '.Yii::t('app', '(If you need to enable the SSO functionality do it from the API & SSO app)');
		} ?>
	</div>
</div>

<div class="form-actions">
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_BACK');?></a>
</div>

<?php $this->endWidget(); ?>