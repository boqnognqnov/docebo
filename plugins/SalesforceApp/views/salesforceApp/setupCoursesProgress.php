<div class="jwizard-step5">
<!--<?= Yii::t('salesforce', 'Configuring your Salesforce App') ?>
<div class="setup-courses-progress-bar">
	<span>0</span>&nbsp;%
</div>-->



<?php

$_allLabels = array(
	'CO1' => Yii::t('salesforce', 'Custom object 1 (LMS Course) created'),
	'CO2' => Yii::t('salesforce', 'Custom object 2 (LMS Course Enrollment) created'),
	'ProductFamily' => Yii::t('salesforce', 'Product Family created'),
	'PriceBook' => Yii::t('salesforce', 'Pricebook created'),
	'OrderType' => Yii::t('salesforce', 'Order Type created')
);

if (is_array($steps) && !empty($steps)) {
	$_index = 0;
	foreach ($steps as $step):
		?>
		<div class="row-fluid" id="subprocess_row_<?= $step['key'] ?>" class="subprocess-row" data-subprocess-key="<?= $step['key'] ?>">
			<p>
				<i class="<?= $_index == 0 ? 'fa fa-spinner fa-spin black pre-started' : 'fa fa-check grey' ?>"></i>
				&nbsp;&nbsp;
				<?= $_allLabels[$step['key']] ?>
			</p>
			<br/>
		</div>
		<?php
		$_index++;
	endforeach;
}
?>

</div>


<div class="finish-button">
	<a href="#" id="finish-button-<?= $uniqueDialogId ?>" class="btn btn-docebo green big center jwizard-nav finish-button disabled">
		<?= Yii::t('iotask', '_FINISH') ?>
	</a>
</div>


<script>

	$('#finish-button-<?= $uniqueDialogId ?>').on('click', function() {
		if ($(this).hasClass('disabled')) { return false; }
		window.location.href = '<?= Docebo::createAdminUrl('SalesforceApp/SalesforceApp/index') ?>';
	});

	(function doPoll() {

		//status polling
		$.ajax({
			url: '<?= Docebo::createAdminUrl('SalesforceApp/SalesforceApp/setupCoursesProgressCheckStatus') ?>',
			type: 'POST',
			dataType: 'JSON',
			data: {
				process_id: '<?= $process->getPrimaryKey() ?>'
			}
		}).done(function (o) {

			if (o.success) {
				var x, sp_key, sp_status, _text = '', continuePolling = false;

				var icon, row, nextRow, nextIcon;

				if (o.subprocessesStatuses) {
					for (x in o.subprocessesStatuses) {
						sp_key = x;
						sp_status = o.subprocessesStatuses[x];
						if (sp_status != 'done' && sp_status != 'error') {
							continuePolling = true; //only when all substatuses are 'done' we stop polling
						}

						row = $('#subprocess_row_' + sp_key);
						if (row.length > 0) {
							icon = row.find('.fa');
							if (icon.length > 0) {
								switch (sp_status) {
									case 'not started':
										if (!icon.hasClass('pre-started')) {
											icon.removeClass('fa.check fa-spinner fa-spin fa-close green grey black red');
											icon.addClass('fa fa-check grey');
										}
										break;
									case 'running':
										icon.removeClass('fa.check fa-spinner fa-spin fa-close green grey black red');
										icon.addClass('fa fa-spinner fa-spin black');
										break;
									case 'done':
										icon.removeClass('fa.check fa-spinner fa-spin fa-close green grey black red');
										icon.addClass('fa fa-check green');
										break;
									case 'error':
										icon.removeClass('fa.check fa-spinner fa-spin fa-close green grey black red');
										icon.addClass('fa fa-close red');
										break;
								}
							}
							//if task is 'done' then pre-start next row (only visual for the moment)
							if (sp_status == 'done') {
								nextRow = row.next();
								if (nextRow.length > 0) {
									nextIcon = row.find('.fa');
									if (nextIcon.length > 0) {
										if (nextIcon.hasClass('fa-check') && nextIcon.hasClass('grey')) {
											icon.removeClass('fa.check fa-spinner fa-spin fa-close green grey black red');
											icon.addClass('fa fa-spinner fa-spin black');
										}
									}
								}
							}
						}
					}
				}


				if (continuePolling) {
					setTimeout(doPoll, 500);
				} else {
					//$('#courses-config-dialog-<?= $uniqueDialogId ?>').dialog2('close');
					var b = $('#finish-button-<?= $uniqueDialogId ?>');
					b.prop('disabled', false);
					b.removeClass('disabled');
				}
			} else {
				//show error message
			}
		}).fail(function () {
			//show error message
		}).always(function () {
			//do stuff, whatever the call ends up
		});

	})();

</script>