{
    "html":
        "<?php
            ob_start();

            ?>
                <h3>Assign content for each language</h3>
                <hr/>
                <a style='display: inline; float: right' class='sf-button black' data-dismiss='modal'>
                    <?= Yii::t('salesforce', 'CANCEL') ?>
                </a>
                <a class='sf-button green close-dialog' data-dismiss='modal' style='display: inline; float: right; margin-right: 10px'>
                    <?= Yii::t('salesforce', 'CONFIRM') ?>
                </a>
            <?
            $fancyTreeHtml = ob_get_clean();

            $fancyTreeHtml = str_replace('"', "\\\"", $fancyTreeHtml);
            $fancyTreeHtml = preg_replace('/[\n\r\t]/', " ", $fancyTreeHtml);
            echo $fancyTreeHtml;
        ?>"
}