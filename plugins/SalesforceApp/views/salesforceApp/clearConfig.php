<h1>
	<?= Yii::t('salesforce', 'Clear main salesforce configuration') ?>
</h1>
<?php
	$this->widget('common.widgets.warningStrip.WarningStrip', array(
         'message'	=> Yii::t('salesforce', 'You are about to delete your current Salesforce account inside Docebo. By doing so, you will reset all your synchronization data (e.g. Users, Contacts, Leads, etc). Are you sure you want to proceed?'),
         'type'		=>'warning',
	));
	?>
	<?php echo CHtml::beginForm('', 'POST', array(
		'class'	=> 'ajax',
		'id'	=> 'salesforce-clear-config-form'
	)); ?>
	<div class="row-fluid">
        <div class="span1">
            <?php
            echo CHtml::checkBox('confirm', false, array(
                'class' => 'confirm_checkbox',
                'value'	=> 'on'
            ));
            ?>
        </div>
        <div class="span11">
            <label id="confirm_label" for="confirm"><?= Yii::t('salesforce', 'Are you sure you want to delete your Salesforce account?')?></label>
        </div>
    </div>


	
	<div class="form-actions">
	<input class="btn-docebo hidden green big yes" type="submit" name="confirm_yes"
		value="<?php echo Yii::t('standard', '_YES'); ?>" /> <input
		class="btn-docebo red big close-dialog" type="reset"
		value="<?php echo Yii::t('standard', '_NO'); ?>" />
</div>
	<?php CHtml::endForm(); ?>

<script type="text/javascript">
    $(function() {
		$(".confirm_checkbox").on('change', function() {
			if ( $(".confirm_checkbox").hasClass("checked") ) {
				$(".btn-docebo.green.yes").removeClass("hidden");
			} else {
				$(".btn-docebo.green.yes").addClass("hidden");
				}
		});
        
        $(".confirm_checkbox").styler();
        $("#confirm_label").styler();
	});
</script>
<style type="text/css">
	#salesforce-clear-config-form {
		margin-top: 30px;
	}
	.warning-text {
		float: none !important;
		display: block !important;
	}
    .warning-strip {
        min-height: 70px;
    }
</style>