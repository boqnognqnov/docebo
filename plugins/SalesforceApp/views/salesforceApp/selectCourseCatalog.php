<h1><?= Yii::t('salesforce', 'Select courses to synchronize'); ?></h1>
<?php
echo CHtml::beginForm('', 'POST', array(
	'class'	=> 'ajax',
	'id'	=> 'selectCourseCatalogs'
));

echo CHtml::hiddenField('selected_catalogs');

//echo CHtml::hiddenField('catalogs_from_db', implode(',', $selectedIds), array('class' => 'selected-ids'));
echo CHtml::hiddenField('catalogs_from_db', implode(',', $selectedIds), array('class' => 'selected-ids'));
echo Chtml::hiddenField('selected-items', $selectionType, array('class' => 'selected-items'));

$encodedIds = CJSON::encode($selectedIds);

?>

<div class="catalog-grid">

	<?php $this->widget('common.widgets.ComboGridView', array(
		'gridId' 					=> 'courses-synch-grid',
		'dataProvider' 				=> $catalogModel->sqlDataProvider( $filterObject ),
		'disableMassActions' 		=> true,
		'massSelectorsInFilterForm'	=> true,
		'massSelectorUrl'			=> Docebo::createAbsoluteAdminUrl( 'SalesforceApp/SalesforceApp/coursesSynch', array() ),
		'doNotCreateForm'			=> true,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'id' 	=> 'items-grid-checkboxes',
				'value' => '$data["idCatalogue"]',
				'htmlOptions'=>array('style'=>'max-width: 300px; word-wrap: break-word; vertical-align:top; padding: 6px 15px;'),
			),
			array(
				'header'    => Yii::t('salesforce', 'Catalog Name'),
				'name'      => 'name',
			),
			array(
				'header'    => Yii::t('course', '_COURSE_CONTENT'),
				'type'      => 'raw',
				'value'     => array($this, 'renderAvailableCoursesColumn'),
			),
		)
	));

	?>

</div>




<div class="form-actions">
	<input class="btn-docebo green big" type="submit" name="confirm_write"
		   value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" /> <input
		class="btn-docebo black big close-dialog" type="reset"
		value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
</div>


<?php
CHTml::endForm();
?>




<script type="text/javascript">
	$(function() {
		$("<div><h2 style='color: #3e7dbf; font-weight: bold'><?= Yii::t('standard', 'Select Courses Catalogs')?></h2></div><hr style='margin: 0px !important;'>").insertBefore( $("#combo-grid") );

		var comboGrid = $("#courses-synch-grid");
		var encodedIds = <?= $encodedIds ?>;
		if ( $(".selected-items").val() == 'catalogs') {
			comboGrid.comboGridView('setSelection', <?= $encodedIds ?>);
		}

		$('#salesforce-course-submit-form').on('submit', function(){
			var selectedCatalogs = comboGrid.yiiGridView('getSelection');
			$('input[name="selected_catalogs"]').val(selectedCatalogs.join());
		});

		$('.r_courses').styler();

		// Handle Select All clicks in Org Chart tree
		$(".combo-grid-select-all").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(2);
		});

		// Handle De-Select All clicks in Org Chart tree
		$(".combo-grid-deselect-all").on("click", function(){
			var tree = $('#<?= $id ?>').fancytree('getTree');
			tree.rootNode.children[0].setSelectState(0);
		});
	});
</script>