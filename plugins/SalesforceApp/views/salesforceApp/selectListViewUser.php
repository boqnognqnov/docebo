<h1><?=Yii::t('salesforce', 'Setup users'); ?></h1>
<p><?=Yii::t('salesforce', 'Before synchronizing Users, you must select a Salesforce filter in order to import the desired users, contacts and leads.')?></p>
<br />

<?php
print CHtml::beginForm('', 'POST', array(
	'id'	=> 'salesforce-setup-accounts-form',
	'class' => 'ajax'
));
?>
<?php if($error){ ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="errorSummary"><?=$error?></div>
		</div>
	</div>
<?php } ?>

<?php if (strpos($sync_user_types, 'user') !== false) { ?>
	<div class="row-fluid">
		<div class="span12">
			<label for="salesforce-user-filter"><?=Yii::t('salesforce', 'Users'); ?></label>
			<?=CHtml::dropDownList('salesforce-user-filter', $selectedUser, $listUsers)?>
		</div>
	</div>
	<br />
<?php } ?>

<?php if (strpos($sync_user_types, 'contact') !== false) { ?>
	<div class="row-fluid">
		<div class="span12">
			<label for="salesforce-contact-filter"><?=Yii::t('salesforce', 'Contacts'); ?></label>
			<?=CHtml::dropDownList('salesforce-contact-filter', $selectedContact, $listContacts)?>
		</div>
	</div>
	<br />
<?php } ?>

<?php if (strpos($sync_user_types, 'lead') !== false) { ?>
	<div class="row-fluid">
		<div class="span12">
			<label for="salesforce-lead-filter"><?=Yii::t('salesforce', 'Leads'); ?></label>
			<?=CHtml::dropDownList('salesforce-lead-filter', $selectedLead, $listLeads)?>
		</div>
	</div>
<?php } ?>

<div class="form-actions">
	<!--<input class="btn-docebo green big confirm-btn" type="submit" name="confirm_sync" value="<?php /*echo Yii::t('standard', '_CONFIRM_AND_SYNC'); */?>" />-->
	<input class="btn-docebo green big confirm-btn" type="submit" name="confirm_write" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>" />
</div>
<?php CHTml::endForm(); ?>
