<?php

/**
 * This is the model class for table "salesforce_additional_fields".
 *
 * The followings are the available columns in table 'salesforce_additional_fields':
 * @property integer $id
 * @property string $sfObjectType
 * @property string $sfFieldType
 * @property string $sfFieldName
 * @property string $sfFieldLabel
 * @property integer $lmsFieldId
 * @property integer $lmsAccountId
 *
 * The followings are the available model relations:
 * @property SalesforceLmsAccount $lmsAccount
 */
class SalesforceAdditionalFields extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_additional_fields';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sfObjectType, sfFieldType, sfFieldName, sfFieldLabel, lmsFieldId, lmsAccountId', 'required'),
			array('lmsAccountId', 'numerical', 'integerOnly'=>true),
			array('sfObjectType', 'length', 'max'=>10),
			array('sfFieldType', 'length', 'max'=>20),
			array('sfFieldName, sfFieldLabel, lmsFieldId', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sfObjectType, sfFieldType, sfFieldName, sfFieldLabel, lmsFieldId, lmsAccountId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lmsAccount' => array(self::BELONGS_TO, 'SalesforceLmsAccount', 'lmsAccountId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sfObjectType' => 'Salesforce Object Type',
			'sfFieldType' => 'Salesforce Field Type',
			'sfFieldName' => 'Salesforce Field Name',
			'sfFieldLabel' => 'Salesforce Field Label',
			'lmsFieldId' => 'LMS Field',
			'lmsAccountId' => 'LMS Account ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sfObjectType',$this->sfObjectType,true);
		$criteria->compare('sfFieldType',$this->sfFieldType,true);
		$criteria->compare('sfFieldName',$this->sfFieldName,true);
		$criteria->compare('sfFieldLabel',$this->sfFieldLabel,true);
		$criteria->compare('lmsFieldId',$this->lmsFieldId);
		$criteria->compare('lmsAccountId',$this->lmsAccountId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceAdditionalFields the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Map salesforce types as Docebo additinoal field types.
	 * @var array [ {Salesforce type} => {LMS type} ]
	 */
	public static $SALESFORCE_FIELDS_TYPE_MAP = array(
        'id' => CoreUserField::TYPE_TEXT,
        'reference' => CoreUserField::TYPE_TEXT,
        'string' => CoreUserField::TYPE_TEXT,
        'double' => CoreUserField::TYPE_TEXT,
        'address' => CoreUserField::TYPE_TEXT,
        'phone' => CoreUserField::TYPE_TEXT,
        'url' => CoreUserField::TYPE_TEXT,
        'currency' => CoreUserField::TYPE_TEXT,
        'int' => CoreUserField::TYPE_TEXT,
        'email' => CoreUserField::TYPE_TEXT,
        'location' => CoreUserField::TYPE_TEXT,
        'percent' => CoreUserField::TYPE_TEXT,
        'encryptedstring' => CoreUserField::TYPE_TEXT,
        'picklist' => CoreUserField::TYPE_TEXT,
        'multipicklist' => CoreUserField::TYPE_TEXT,
        'country' => CoreUserField::TYPE_TEXT,
        'textarea' => CoreUserField::TYPE_TEXTAREA,
        'date' => CoreUserField::TYPE_DATE,
        'datetime' => CoreUserField::TYPE_DATE,
        'boolean' => CoreUserField::TYPE_YESNO
	);

}
