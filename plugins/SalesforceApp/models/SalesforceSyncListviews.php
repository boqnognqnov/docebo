<?php

/**
 * This is the model class for table "salesforce_sync_listviews".
 *
 * The followings are the available columns in table 'salesforce_sync_listviews':
 * @property integer $id
 * @property integer $idAccount
 * @property string $type
 * @property string $listviewId
 * @property string $listviewName
 * @property integer $force_change_password
 * @property string $orgchartType
 * @property integer $orgchartId
 * @property string $hierarchy1
 * @property string $hierarchy2
 * @property string $hierarchy3
 * @property string $last_updated
 *
 * The followings are the available model relations:
 * @property SalesforceLmsAccount $idAccount0
 */
class SalesforceSyncListviews extends CActiveRecord
{
	const EXECUTE_LIST_VIEW_LIMIT = 100;
	const LISTVIEW_MAX_ACCOUNTS = 250; // max number of accounts allowed in a listview to be able to use SF branches as orgchart type

	const ORGCHART_TYPE_SINGLE_BRANCH     = 'single_branch';
	const ORGCHART_TYPE_ACCOUNT_BASED     = 'account_based';
	const ORGCHART_TYPE_ADDITIONAL_FIELDS = 'additional_fields';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_sync_listviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAccount, type, listviewId, orgchartType, orgchartId', 'required'),
			array('idAccount, force_change_password, orgchartId', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>10),
			array('orgchartType', 'length', 'max'=>30),
			array('listviewId', 'length', 'max'=>18),
			array('listviewName, hierarchy1, hierarchy2, hierarchy3', 'length', 'max'=>255),
			array('last_updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idAccount, type, listviewId, force_change_password, orgchartType, orgchartId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAccount0' => array(self::BELONGS_TO, 'SalesforceLmsAccount', 'idAccount'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idAccount' => 'Id Account',
			'type' => 'Type',
			'listviewId' => 'Listview',
			'listviewName' => 'Listview name',
			'force_change_password' => 'Force change password',
			'orgchartType' => 'Organization chart Type',
			'orgchartId' => 'Organization chart ID',
			'hierarchy1' => 'Addition field hierarchy 1',
			'hierarchy2' => 'Addition field hierarchy 2',
			'hierarchy3' => 'Addition field hierarchy 3',
			'last_updated' => 'Last Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idAccount',$this->idAccount);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('listviewId',$this->listviewId,true);
		$criteria->compare('listviewName',$this->listviewName,true);
		$criteria->compare('force_change_password',$this->force_change_password,true);
		$criteria->compare('orgchartType',$this->orgchartType,true);
		$criteria->compare('orgchartId',$this->orgchartId,true);
		$criteria->compare('hierarchy1',$this->hierarchy1,true);
		$criteria->compare('hierarchy2',$this->hierarchy2,true);
		$criteria->compare('hierarchy3',$this->hierarchy3,true);
		$criteria->compare('last_updated',$this->last_updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceSyncListviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
