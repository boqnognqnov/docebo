<?php

/**
 * This is the model class for table "salesforce_accounts".
 *
 * The followings are the available columns in table 'salesforce_accounts':
 * @property integer $id
 * @property integer $lmsAccount
 * @property string $accountId
 * @property string $accountName
 * @property string $parentId
 * @property datetime $lastModified
 * @property integer $active
 * @property string $lmsAdditionalFields
 * @property string $sfAdditionalFields
 * @property string $contract_id
 */
class SalesforceAccounts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'salesforce_accounts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lmsAccount, accountId, accountName, lastModified', 'required'),
			array('lmsAccount, active', 'numerical', 'integerOnly'=>true),
			array('accountId, parentId, contract_id', 'length', 'max'=>18),
			array('lastModified', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lmsAccount, accountId, accountName, parentId, lastModified, active, lmsAdditionalFields, sfAdditionalFields, contract_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lmsAccount' => 'Lms Account',
			'accountId' => 'Account ID',
			'accountName' => 'Account Name',
			'parentId' => 'Parent ID',
			'lastModified' => 'Last modified',
			'active' => 'Active',
			'lmsAdditionalFields' => 'LMS Additional fields (user profile)',
			'sfAdditionalFields' => 'SF Additional fields (in additional fields orgchart scenario)',
			'contract_id' => 'Contract ID'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lmsAccount',$this->lmsAccount);
		$criteria->compare('accountId',$this->accountId,true);
		$criteria->compare('accountName',$this->accountName,true);
		$criteria->compare('parentId',$this->parentId,true);
		$criteria->compare('lastModified',$this->lastModified,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('contract_id',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SalesforceAccounts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
