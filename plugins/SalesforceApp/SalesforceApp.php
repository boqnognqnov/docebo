<?php

/**
 * Plugin for CoursecatalogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class SalesforceApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'SalesforceApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

    public static function settingsUrl()
    {
        return Docebo::createAdminUrl('//SalesforceApp/SalesforceApp/index');
    }

	public function activate() {

        // refresh session  permission for the logged in users
        Yii::app()->authManager->refreshCache( Yii::app()->user->id );

        // activate options
        $sql = <<< SQL
        
SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Table structure for table `salesforce_lms_account`
--

CREATE TABLE IF NOT EXISTS `salesforce_lms_account` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `agent` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `sync_user_types` varchar(32) DEFAULT NULL COMMENT 'user || account || lead',
  `replicate_courses_as_products` int(11) NOT NULL DEFAULT '0',
  `replicate_courses_highest_history` INT NOT NULL DEFAULT '0',
  `sync_frequency` int(11) NOT NULL DEFAULT '26280',
  `selected_items` text,
  `id_org` int(11) DEFAULT NULL COMMENT 'Branch ID of the root node for this SF entity',
  `sync_job_hash` varchar(64) DEFAULT NULL,
  `use_sandbox` int(11) DEFAULT '1',
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `salesforce_log`
--

CREATE TABLE IF NOT EXISTS `salesforce_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `type_log` varchar(45) NOT NULL,
  `datetime` datetime NOT NULL,
  `data` longtext,
  `job_id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `salesforce_orgchart`
--

CREATE TABLE IF NOT EXISTS `salesforce_orgchart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_org` int(11) NOT NULL,
  `id_parent` int(11) NOT NULL,
  `sf_lms_id` int(11) NOT NULL COMMENT 'The Salesforce LMS account ID',
  `sf_account_id` varchar(64) DEFAULT NULL COMMENT 'The Salesforce ACCOUNT ID ',
  `sf_contract_id` varchar(64) DEFAULT NULL,
  `sf_type` varchar(16) NOT NULL COMMENT 'Like user, contact, lead, account',
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_org` (`id_org`),
  CONSTRAINT `salesforce_orgchart_ibfk_1` FOREIGN KEY (`id_org`) REFERENCES `core_org_chart_tree` (`idOrg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8;

--
-- Table structure for table `salesforce_user`
--

CREATE TABLE IF NOT EXISTS `salesforce_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_lms` int(11) NOT NULL,
  `id_user_sf` varchar(64) NOT NULL,
  `sf_type` varchar(16) NOT NULL,
  `sf_lms_id` int(11) NOT NULL,
  `account_id` varchar(18) NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_lms` (`id_user_lms`),
  CONSTRAINT `salesforce_user_ibfk_1` FOREIGN KEY (`id_user_lms`) REFERENCES `core_user` (`idst`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10709 DEFAULT CHARSET=utf8;

--
-- Table structure for table `salesforce_sync_listviews`
--

CREATE TABLE IF NOT EXISTS `salesforce_sync_listviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAccount` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `listviewId` varchar(18) NOT NULL,
  `listviewName` varchar(255) NOT NULL,
  `force_change_password` INT(1) NULL DEFAULT '1',
  `orgchartType` VARCHAR(30) NOT NULL,
  `orgchartId` INT NOT NULL,
  `hierarchy1` VARCHAR(255) NULL,
  `hierarchy2` VARCHAR(255) NULL,
  `hierarchy3` VARCHAR(255) NULL,
  `last_updated` DATETIME NULL,
  PRIMARY KEY (`id`),
  KEY `idAccount` (`idAccount`),
  CONSTRAINT `idAccount` FOREIGN KEY (`idAccount`) REFERENCES `salesforce_lms_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `salesforce_enrollment_sf_task`
--

CREATE TABLE IF NOT EXISTS `salesforce_enrollment_sf_task` (
  `path_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `salesforce_task_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `salesforce_additional_fields`
--

CREATE TABLE IF NOT EXISTS `salesforce_additional_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sfObjectType` varchar(10) NOT NULL,
  `sfFieldType` varchar(20) NOT NULL,
  `sfFieldName` varchar(255) NOT NULL,
  `sfFieldLabel` varchar(255) NOT NULL,
  `lmsFieldId` varchar(255) NOT NULL,
  `lmsAccountId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `lmsAccountId` FOREIGN KEY (`lmsAccountId`) REFERENCES `salesforce_lms_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `salesforce_accounts`
--

CREATE TABLE IF NOT EXISTS `salesforce_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lmsAccount` int(11) NOT NULL,
  `accountId` varchar(18) NOT NULL,
  `accountName` varchar(255) NOT NULL,
  `parentId` varchar(18) NULL,
  `lastModified` varchar(30) NOT NULL,
  `active` int(1),
  `lmsAdditionalFields` TEXT NULL,
  `sfAdditionalFields` TEXT NULL,
  `contract_id` varchar(18) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `lmsAccount` FOREIGN KEY (`lmsAccount`) REFERENCES `salesforce_lms_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `salesforce_sync_values`
--

CREATE TABLE IF NOT EXISTS `salesforce_sync_values` (
  `type` varchar(15) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` LONGTEXT NOT NULL,
  PRIMARY KEY (`type`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=1;
        		        		
SQL;
        
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
        // When doing command->execute(), always issue an unset(command)
        // before any other SQL related code, to close the MySQL cursor
        // Otherwise you will get:
        //     Cannot execute queries while other unbuffered queries are active.
        unset($command);
        parent::activate();

	}

	public function deactivate() {
		parent::deactivate();
	}

}
