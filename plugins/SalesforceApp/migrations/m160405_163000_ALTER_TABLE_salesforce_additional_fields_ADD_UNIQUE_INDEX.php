<?php

class m160405_163000_ALTER_TABLE_salesforce_additional_fields_ADD_UNIQUE_INDEX extends DoceboDbMigration {

	public function safeUp()
	{
		$sql = "ALTER TABLE `salesforce_additional_fields` ADD UNIQUE INDEX `lmsFieldId_UNIQUE` (`lmsFieldId` ASC);";
		$this->execute($sql);
		return true;
	}

	public function safeDown()
	{
		$sql = "ALTER TABLE `salesforce_additional_fields` DROP INDEX `lmsFieldId_UNIQUE`;";
		$this->execute($sql);
		return true;
	}

}
