<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m151209_093819_salesforce_task_id_storage extends DoceboDbMigration {

	const TABLE_NAME = 'salesforce_enrollment_sf_task';

	public function safeUp()
	{
		if (Yii::app()->db->schema->getTable(self::TABLE_NAME,true)===null) {
			$sql = "
				CREATE TABLE IF NOT EXISTS " . self::TABLE_NAME . "
				(
					path_id INT,
					course_id INT,
					user_id INT,
					salesforce_task_id VARCHAR(50)
				)ENGINE=INNODB DEFAULT CHARSET=utf8;
			";
			$this->execute($sql);
		} else {
			// check if the old column exists
			$table = Yii::app()->db->schema->getTable(self::TABLE_NAME);

			if (isset($table->columns['enrollment_id'])) {
				$this->dropColumn(self::TABLE_NAME, 'enrollment_id');
			}
			if (!isset($table->columns['path_id'])) {
				$this->addColumn(self::TABLE_NAME, 'path_id', 'INT');
			}
			if (!isset($table->columns['course_id'])) {
				$this->addColumn(self::TABLE_NAME, 'course_id', 'INT');
			}
			if (!isset($table->columns['user_id'])) {
				$this->addColumn(self::TABLE_NAME, 'user_id', 'INT');
			}
		}
	
		return true;
	}

	public function safeDown()
	{
		$sql = "
			SET FOREIGN_KEY_CHECKS = 0;
			DROP TABLE IF EXISTS " . self::TABLE_NAME . ";
			SET FOREIGN_KEY_CHECKS = 1
		";
		$this->execute($sql);
		return true;
	}
	
	
}
