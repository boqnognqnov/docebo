<?php
class m160720_122929_ALTER_salesforce_sync_values_CONVERT_value_TO_longtext extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `salesforce_sync_values` CHANGE `value` `value` LONGTEXT NOT NULL;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_sync_values` CHANGE `value` `value` TEXT NOT NULL;";
		$this->execute($order66);
		return true;
	}
	
	
}
