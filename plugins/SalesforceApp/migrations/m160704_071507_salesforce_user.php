<?php
class m160704_071507_salesforce_user extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `salesforce_user` ADD INDEX(`id_user_sf`);";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_user` DROP INDEX(`id_user_sf`);";
		$this->execute($order66);
		return true;
	}
}
