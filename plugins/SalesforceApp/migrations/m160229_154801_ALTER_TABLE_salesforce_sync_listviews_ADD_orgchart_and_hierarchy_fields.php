<?php
class m160229_154801_ALTER_TABLE_salesforce_sync_listviews_ADD_orgchart_and_hierarchy_fields extends DoceboDbMigration {

	public function safeUp()
	{
		//$columns = SalesforceSyncListviews::model()->getTableSchema()->getColumnNames();
		// --- NOTE: we can't use the above because we cannot be sure if the model has already properly initialized, so go with direct query instead.
		// --- The table should have already been created in "m151125_074009_CREATE_salesforce_sync_listviews.php"
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_sync_listviews";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---

		$order66 = "";
		if(!in_array('orgchartType', $columns)){
			$order66 .= "ALTER TABLE `salesforce_sync_listviews` ADD `orgchartType` VARCHAR(30) NOT NULL;";
		}
		if(!in_array('orgchartId', $columns)){
			$order66 .= "ALTER TABLE `salesforce_sync_listviews` ADD `orgchartId` INT NOT NULL;";
		}
		if(!in_array('hierarchy1', $columns)){
			$order66 .= "ALTER TABLE `salesforce_sync_listviews` ADD `hierarchy1` VARCHAR(255) NOT NULL;";
		}
		if(!in_array('hierarchy2', $columns)){
			$order66 .= "ALTER TABLE `salesforce_sync_listviews` ADD `hierarchy2` VARCHAR(255) NOT NULL;";
		}
		if(!in_array('hierarchy3', $columns)){
			$order66 .= "ALTER TABLE `salesforce_sync_listviews` ADD `hierarchy3` VARCHAR(255) NOT NULL;";
		}
		if(strlen($order66) > 0) $this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_sync_listviews`
			DROP `orgchartType`,
			DROP `orgchartId`,
			DROP `hierarchy1`,
			DROP `hierarchy2`,
			DROP `hierarchy3`;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
