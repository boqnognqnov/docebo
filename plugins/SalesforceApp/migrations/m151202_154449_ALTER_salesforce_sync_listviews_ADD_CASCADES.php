<?php
/**
 * Yii DB Migration template.
 *
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 */
class m151202_154449_ALTER_salesforce_sync_listviews_ADD_CASCADES extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "ALTER TABLE `salesforce_sync_listviews` DROP FOREIGN KEY `idAccount`;
			ALTER TABLE `salesforce_sync_listviews` ADD CONSTRAINT `idAccount` FOREIGN KEY (`idAccount`) REFERENCES `salesforce_lms_account`(`id_account`) ON DELETE CASCADE ON UPDATE CASCADE;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_sync_listviews` DROP FOREIGN KEY `idAccount`;
			ALTER TABLE `salesforce_sync_listviews` ADD CONSTRAINT `idAccount` FOREIGN KEY (`idAccount`) REFERENCES `salesforce_lms_account` (`id_account`)";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
