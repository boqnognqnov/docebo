<?php
class m160216_112051_ALTER_salesforce_orgchart_ADD_COLUMN_active extends DoceboDbMigration {

	public function safeUp()
	{
		//$columns = SalesforceOrgchart::model()->getTableSchema()->getColumnNames();
		// --- NOTE: we can't use the above because we cannot be sure if the model has already properly initialized, so go with direct query instead.
		// --- The table should have already been created in "m151125_074009_CREATE_salesforce_sync_listviews.php"
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_orgchart";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---
		if(!in_array('active', $columns)){
			$order66 = "ALTER TABLE `salesforce_orgchart` ADD `active` INT NOT NULL DEFAULT '1';";
			$this->execute($order66);
		}
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_orgchart` DROP `active`;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
