<?php
class m151203_120534_UPDATE_salesforce_lms_account_SOME_SYNC_STUFF extends DoceboDbMigration {

	/**
	 * STEP 1: Changing default sync frequency from 24 (1 day) to 26280 (3 years)
	 * STEP 2: Changing existing accounts to sync frequency 26280 to ensure no erroneous sync jobs take place after SF updates go live
	 * STEP 3: Changing existing Salesforce corejobs to have 3 year interval set up
	 */
	public function safeUp()
	{
		$order66 = "ALTER TABLE `salesforce_lms_account` CHANGE `sync_frequency` `sync_frequency` INT(11) NOT NULL DEFAULT '26280';
		UPDATE `salesforce_lms_account` SET `sync_frequency` = 26280;
		UPDATE core_job SET `rec_length` = 26280 WHERE `name` = 'SalesforceAutoSync';";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_lms_account` CHANGE `sync_frequency` `sync_frequency` INT(11) NOT NULL DEFAULT '24';";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
