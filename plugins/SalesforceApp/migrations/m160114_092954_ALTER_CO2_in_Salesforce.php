<?php
class m160114_092954_ALTER_CO2_in_Salesforce extends DoceboDbMigration {

	// This is a migration intended to create a one-time upgrade in a user's Salesforce account.
	// No changes are being made to the LMS database
	public function safeUp()
	{
		/*$salesforceModel = SalesforceLmsAccount::getFirstAccount();
		$params = array(
			'type' => SalesforceLog::TYPE_LOG_UPGRADE,
			'id_account' => $salesforceModel->id_account
		);
		$userId = Yii::app()->db->createCommand()
			->select('MIN(idst)')
			->from(CoreUser::model()->tableName())
			->queryScalar();
		Yii::app()->scheduler->createImmediateJob("SalesforceSyncManual", "common.components.job.handlers.SalesforceSyncTask", $params, $userId, null, false);*/
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
