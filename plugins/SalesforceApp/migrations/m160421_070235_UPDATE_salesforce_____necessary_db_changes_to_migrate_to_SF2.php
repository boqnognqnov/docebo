<?php
class m160421_070235_UPDATE_salesforce_____necessary_db_changes_to_migrate_to_SF2 extends DoceboDbMigration {

	public function safeUp()
	{
		// Step 1: Get the LMS id
		$sfAccountId = Yii::app()->db->createCommand("
			SELECT id_account FROM salesforce_lms_account ORDER BY id_account DESC LIMIT 1
		")->queryScalar();

		// If there's no existing SF account, there's nothing further to do and we can stop now
		if(!$sfAccountId) return true;

		// Step 1: Creating the accounts
		$accounts = Yii::app()->db->createCommand("
			SELECT so.sf_account_id, coc.translation
			FROM salesforce_orgchart AS so
			INNER JOIN core_org_chart AS coc ON so.id_org = coc.id_dir
			WHERE so.sf_account_id IS NOT NULL AND so.active = 1 AND coc.translation <> ''
			GROUP BY coc.id_dir
		")->queryAll();
		$existingAccountsTmp = Yii::app()->db->createCommand("
			SELECT accountId
			FROM salesforce_accounts
		")->queryAll();
		$existingAccounts = array();
		foreach($existingAccountsTmp as $tmp){
			$existingAccounts[] = $tmp['accountId'];
		}
		foreach($accounts as $account){
			if(!in_array($account['sf_account_id'], $existingAccounts)){
				Yii::app()->db->createCommand("
					INSERT INTO salesforce_accounts
					(lmsAccount, accountId, accountName, lastModified, active)
					VALUES (".$sfAccountId.", '".$account['sf_account_id']."', '".addslashes($account['translation'])."', '2000-01-01T08:00:00.000Z', 1)
				")->execute();
			}
		}

		// Step 2: Updating the org chart
		$userRoot = 0;
		$contactRoot = 0;
		$leadRoot = 0;

		// Count number of users / contacts --> if >1, the table was already converted
		$countUsers = Yii::app()->db->createCommand("
			SELECT COUNT(id)
			FROM salesforce_orgchart
			WHERE sf_type = 'user'
		")->queryScalar();
		$countContacts = Yii::app()->db->createCommand("
			SELECT COUNT(id)
			FROM salesforce_orgchart
			WHERE sf_type = 'contact'
		")->queryScalar();

		if(($countUsers <= 1) && ($countContacts <= 1)){
			$orgs = Yii::app()->db->createCommand("
				SELECT id, id_org, sf_type
				FROM salesforce_orgchart
				ORDER BY id ASC
			")->queryAll();
			foreach($orgs as $org){
				switch($org['sf_type']){
					case "user":
						$userRoot = $org['id_org'];
						$newType = "User";
						$isRoot = 1;
						break;
					case "contact":
						$contactRoot = $org['id_org'];
						$newType = "Contact";
						$isRoot = 1;
						break;
					case "lead":
						$leadRoot = $org['id_org'];
						$newType = "Lead";
						$isRoot = 1;
						break;
					case "account":
						$newType = "contact";
						$isRoot = 0;
						break;
				}
				Yii::app()->db->createCommand("
					UPDATE salesforce_orgchart
					SET sf_type = '".$newType."', is_root = ".$isRoot."
					WHERE id = ".$org['id']."
				")->execute();
			}
		}

		// Step 3: Updating the listview table
		if($userRoot > 0){
			Yii::app()->db->createCommand("
				UPDATE salesforce_sync_listviews
				SET orgchartType = 'single_branch', orgchartId = ".$userRoot."
				WHERE type = 'User'
			")->execute();
		}
		if($contactRoot > 0){
			Yii::app()->db->createCommand("
				UPDATE salesforce_sync_listviews
				SET orgchartType = 'account_based', orgchartId = ".$contactRoot."
				WHERE type = 'Account'
			")->execute();
			Yii::app()->db->createCommand("
				UPDATE salesforce_sync_listviews
				SET orgchartType = 'none', orgchartId = 0
				WHERE type = 'Contact'
			")->execute();
		}
		if($leadRoot > 0){
			Yii::app()->db->createCommand("
				UPDATE salesforce_sync_listviews
				SET orgchartType = 'single_branch', orgchartId = ".$leadRoot."
				WHERE type = 'Lead'
			")->execute();
		}
		return true;
	}

	public function safeDown()
	{
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
