<?php
class m160322_134656_ALTER_TABLE_salesforce_accounts_ADD_name_parentid_lastmodified extends DoceboDbMigration {

	public function safeUp()
	{
		//$columns = SalesforceAccounts::model()->getTableSchema()->getColumnNames();
		// --- NOTE: we can't use the above because we cannot be sure if the model has already properly initialized, so go with direct query instead.
		// --- The table should have already been created before.
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_accounts";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---
		$add = array();
		if(!in_array('accountName', $columns)){
			$add[] = "ADD `accountName` VARCHAR(255) NOT NULL AFTER `accountId`";
		}
		if(!in_array('parentId', $columns)){
			$add[] = "ADD `parentId` VARCHAR(18) NULL AFTER `accountName`";
		}
		if(!in_array('lastModified', $columns)){
			$add[] = "ADD `lastModified` VARCHAR(30) NOT NULL AFTER `parentId`";
		}
		if(!in_array('lmsAdditionalFields', $columns)){
			$add[] = "ADD `lmsAdditionalFields` TEXT NULL";
		}
		if(!in_array('sfAdditionalFields', $columns)){
			$add[] = "ADD `sfAdditionalFields` TEXT NULL";
		}
		if(!empty($add)){
			$order66 = "ALTER TABLE `salesforce_accounts` ".implode(", ", $add).";";
			$this->execute($order66);
		}
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_accounts` DROP `parentId`, DROP `lastModified`, DROP `accountName`, DROP `lmsAdditionalFields`, DROP `sfAdditionalFields`;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
