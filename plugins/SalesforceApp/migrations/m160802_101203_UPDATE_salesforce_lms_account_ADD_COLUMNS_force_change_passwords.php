<?php
class m160802_101203_UPDATE_salesforce_lms_account_ADD_COLUMNS_force_change_passwords extends DoceboDbMigration {

	public function safeUp()
	{
		// ---
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_sync_listviews";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---
		if (!in_array('force_change_password', $columns)) {
			$order66 = "ALTER TABLE `salesforce_sync_listviews` ADD `force_change_password` INT(1) NULL DEFAULT '1' AFTER `listviewName`;";
			$this->execute($order66);
		}
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_sync_listviews` DROP `force_change_password`;";
		$this->execute($order66);
		return true;
	}
	
	
}
