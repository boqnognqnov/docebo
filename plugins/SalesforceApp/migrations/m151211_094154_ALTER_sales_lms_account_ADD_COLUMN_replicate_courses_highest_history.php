<?php
class m151211_094154_ALTER_sales_lms_account_ADD_COLUMN_replicate_courses_highest_history extends DoceboDbMigration {

	public function safeUp()
	{
		//$columns = SalesforceLmsAccount::model()->getTableSchema()->getColumnNames();
		// --- NOTE: we can't use the above because we cannot be sure if the model has already properly initialized, so go with direct query instead.
		// --- The table should have already been created before.
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_lms_account";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---
		if(!in_array('replicate_courses_highest_history', $columns)){
			$order66 = "ALTER TABLE `salesforce_lms_account` ADD `replicate_courses_highest_history` INT NOT NULL DEFAULT '0' AFTER `replicate_courses_as_products`;";
			$this->execute($order66);
		}
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_lms_account` DROP `replicate_courses_highest_history`;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
