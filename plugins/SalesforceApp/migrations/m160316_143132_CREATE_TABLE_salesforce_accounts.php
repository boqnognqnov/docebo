<?php
class m160316_143132_CREATE_TABLE_salesforce_accounts extends DoceboDbMigration {
	public function safeUp()
	{
		$order66 = "
		CREATE TABLE IF NOT EXISTS `salesforce_accounts` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `lmsAccount` int(11) NOT NULL,
		  `accountId` varchar(18) NOT NULL,
		  `active` int(1),
		  PRIMARY KEY (`id`),
		  CONSTRAINT `lmsAccount` FOREIGN KEY (`lmsAccount`) REFERENCES `salesforce_lms_account` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "
			SET foreign_key_checks = 0;
			DROP TABLE IF EXISTS salesforce_accounts;
			SET foreign_key_checks = 1;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
