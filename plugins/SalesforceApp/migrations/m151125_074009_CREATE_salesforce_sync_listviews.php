<?php
class m151125_074009_CREATE_salesforce_sync_listviews extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "
			CREATE TABLE IF NOT EXISTS salesforce_sync_listviews (
				`id` int(11) NOT NULL AUTO_INCREMENT,
  				`idAccount` int(11) NOT NULL,
				`type` varchar(10) NOT NULL,
				`listviewId` varchar(18) NOT NULL,
				`listviewName` varchar(255) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `idAccount` (`idAccount`),
				CONSTRAINT `idAccount` FOREIGN KEY (`idAccount`) REFERENCES `salesforce_lms_account` (`id_account`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			SET FOREIGN_KEY_CHECKS=1;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "
			SET foreign_key_checks = 0;
			DROP TABLE IF EXISTS salesforce_sync_listviews;
			SET foreign_key_checks = 1;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
