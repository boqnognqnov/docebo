<?php
class m160406_115631_ALTER_TABLE_salesforce_users_ADD_account_id extends DoceboDbMigration {

	public function safeUp()
	{
		//$columns = SalesforceUser::model()->getTableSchema()->getColumnNames();
		// --- NOTE: we can't use the above because we cannot be sure if the model has already properly initialized, so go with direct query instead.
		// --- The table should have already been created before.
		$columns = array();
		$query = "SHOW COLUMNS FROM salesforce_user";
		$res = Yii::app()->db->createCommand($query)->queryAll();
		if (!empty($res) && is_array($res)) {
			foreach ($res as $record) {
				$columns[] = $record['Field'];
			}
		}
		//---
		if(!in_array('account_id', $columns)){
			$order66 = "ALTER TABLE `salesforce_user` ADD `account_id` VARCHAR(18) NULL;";
			$this->execute($order66);
		}
		return true;
	}

	public function safeDown()
	{
		$order66 = "ALTER TABLE `salesforce_user` DROP `account_id`;";
		$this->execute($order66);
		return true;
	}

	/**************************************
	 *                                    *
	 *  ~May the Salesforce be with you~  *
	 *                                    *
	 *             _,.-"T                 *
	 *       _.--{~    :l                 *
	 *     c"     `.    :I                *
	 *     |  .-"~-.\    l                *
	 *     | Y_r--. Y) ___I               *
	 *     |[__L__/ j"~=__]__             *
	 *  ___|  \.__.r--<~__.| T            *
	 * '--rl___/\ ( () ).,_L_].--.        *
	 *    `--'   `-^--^\ /___"(~\ Y       *
	 *                  "~   \ " `/       *
	 *                        ]--[        *
	 *                        |: L        *
	 *                        `| o\       *
	 *                         I  [       *
	 *                         l: |       *
	 *                         `|_I       *
	 *                          L :]      *
	 *                          [n]l      *
	 *                          I //      *
	 *                         /]"/       *
	 *                        //./        *
	 *                    _  // /         *
	 *            _  ,-="_"^K_/           *
	 *            [ ][.-~" ~"-.]          *
	 *     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
	 **************************************/
}
