<?php

class m160408_153622_CREATE_TABLE_salesforce_running_process extends DoceboDbMigration {

	public function safeUp()
	{
		$sql = "CREATE TABLE `salesforce_running_process` (
  `process_id` INT NOT NULL AUTO_INCREMENT,
  `status` INT NOT NULL DEFAULT 0,
  `start_date` TIMESTAMP NULL,
  `end_date` TIMESTAMP NULL,
  `process_data` TEXT CHARACTER SET 'utf8' NULL,
  `process_name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `user_id` INT NULL,
  PRIMARY KEY (`process_id`))
COMMENT = 'this table is used internally to keep track of some time-intensive tasks that need complex handling and monitoring';";
		$this->execute($sql);
		return true;
	}

	public function safeDown()
	{
		$sql = "DROP TABLE `salesforce_running_process`;";
		$this->execute($sql);
		return true;
	}

}
