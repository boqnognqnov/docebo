<?php
class m160706_135645_CREATE_TABLE_salesforce_sync_values extends DoceboDbMigration {

	public function safeUp()
	{
		$order66 = "
			CREATE TABLE IF NOT EXISTS `salesforce_sync_values` (
			  `type` varchar(15) NOT NULL,
			  `key` varchar(50) NOT NULL,
			  `value` text NOT NULL,
			  PRIMARY KEY (`type`,`key`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$this->execute($order66);
		return true;
	}

	public function safeDown()
	{
		$order66 = "
			SET foreign_key_checks = 0;
			DROP TABLE IF EXISTS salesforce_sync_values;
			SET foreign_key_checks = 1;";
		$this->execute($order66);
		return true;
	}
	
	
}
