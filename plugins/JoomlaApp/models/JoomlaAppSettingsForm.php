<?php

/**
 * JoomlaAppSettingsForm class.
 *
 * @property string $catalog_type
 */

/**
 * Nothing to do here, really
 */
class JoomlaAppSettingsForm extends CFormModel {

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(

		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array();
	}

	public function beforeValidate() {
		return parent::beforeValidate();
	}

}
