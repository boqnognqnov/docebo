<?php
/* @var $form CActiveForm */
/* @var $settings DoceboWebconferenceAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: Docebo Webconference</h2>
<br>

<!--<div class="error-summary">--><?php //echo $form->errorSummary($settings); ?><!--</div>-->

<div class="control-group">
	<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
	<div class="controls">
        <a class="app-settings-url" href="http://www.adobe.com/it/products/adobeconnect.html" target="_blank">http://www.adobe.com/it/products/adobeconnect.html</a>
	</div>
</div>

<div class="control-group">
    <?=$form->labelEx($settings, 'server_url', array('class'=>'control-label'));?>
    <div class="controls">
        <?=$form->textField($settings, 'server_url', array('class'=>'input-xlarge'));?>
        <?=$form->error($settings, 'server_url'); ?>
    </div>
</div>

<div class="control-group">
    <?=$form->labelEx($settings, 'admin_username', array('class'=>'control-label'));?>
    <div class="controls">
        <?=$form->textField($settings, 'admin_username', array('class'=>'input-xlarge'));?>
        <?=$form->error($settings, 'admin_username'); ?>
    </div>
</div>

<div class="control-group">
    <?=$form->labelEx($settings, 'admin_password', array('class'=>'control-label'));?>
    <div class="controls">
        <?=$form->passwordField($settings, 'admin_password', array('class'=>'input-xlarge'));?>
        <?=$form->error($settings, 'admin_password'); ?>
    </div>
</div>

<!--<div class="control-group">
    <?/*=$form->labelEx($settings, 'org_name', array('class'=>'control-label'));*/?>
    <div class="controls">
        <?/*=$form->textField($settings, 'org_name', array('class'=>'input-xlarge'));*/?>
        <?/*=$form->error($settings, 'org_name'); */?>
    </div>
</div>-->

<div class="control-group">
    <?=$form->labelEx($settings, 'max_rooms_per_course', array('class'=>'control-label'));?>
    <div class="controls">
        <?=$form->textField($settings, 'max_rooms_per_course', array('class'=>'input-xlarge'));?>
        <?=$form->error($settings, 'max_rooms_per_course'); ?>
    </div>
</div>

<div class="control-group">
    <?=$form->labelEx($settings, 'max_rooms', array('class'=>'control-label'));?>
    <div class="controls">
        <?=$form->textField($settings, 'max_rooms', array('class'=>'input-xlarge'));?>
        <?=$form->error($settings, 'max_rooms'); ?>
    </div>
</div>


<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
    $(document).ready(function(){
    });
</script>