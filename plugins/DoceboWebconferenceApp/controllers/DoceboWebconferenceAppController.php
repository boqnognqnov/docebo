<?php

class DoceboWebconferenceAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {

		$settings = new DoceboWebconferenceAppSettingsForm();

		if (isset($_POST['DoceboWebconferenceAppSettingsForm'])) {
			$settings->setAttributes($_POST['DoceboWebconferenceAppSettingsForm']);

			if ($settings->validate()) {
                $urlRequiredScheme = 'http://';
                if (0 !== strncmp($settings->server_url, $urlRequiredScheme, strlen($urlRequiredScheme))) {
                    $settings->server_url = $urlRequiredScheme . $settings->server_url;
                }
				Settings::save('adobeconnect_server_url', $settings->server_url, 'string', 255);
				Settings::save('adobeconnect_admin_username', $settings->admin_username, 'string', 255);
				Settings::save('adobeconnect_admin_password', $settings->admin_password, 'string', 255);
				//Settings::save('adobeconnect_org_name', $settings->org_name, 'string', 255);
				Settings::save('adobeconnect_max_rooms', $settings->max_rooms, 'string', 255);
				Settings::save('adobeconnect_max_rooms_per_course', $settings->max_rooms_per_course, 'string', 255);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->server_url = Settings::get('adobeconnect_server_url', '');
		$settings->admin_username = Settings::get('adobeconnect_admin_username', '');
		$settings->admin_password = Settings::get('adobeconnect_admin_password', '');
		//$settings->org_name = Settings::get('adobeconnect_org_name', '');

		$settings->max_rooms = Settings::get('adobeconnect_max_rooms', '');
        if (empty($settings->max_rooms)) {
            $settings->max_rooms = intval(Yii::app()->params['videoconf_max_rooms_default']);
        }
		$settings->max_rooms_per_course = Settings::get('adobeconnect_max_rooms_per_course', '');
        if (empty($settings->max_rooms_per_course)) {
            $settings->max_rooms_per_course = intval(Yii::app()->params['videoconf_max_rooms_per_course_default']);
        }


		$this->render('settings', array('settings' => $settings));
	}

}
