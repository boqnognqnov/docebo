<?php
/**
 * DoceboWebconference API Client
 *
 * @author Andrei Amariutei
 *
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 */
class DoceboWebconferenceApiClient extends CComponent {

	protected $_apiBaseUrl = 'http://vm2662.cloud.seeweb.it:8181/Docebo/Docebo/';

	protected $_apiUser = 'doceboAdmin';
	protected $_apiPass = 'secret';
	protected $_apiToken = null;


    /**
     * Constructor
     */
    public function __construct() {
        $this->init();
    }

    /**
     * Init component
     */
    public function init() {

        Yii::import('common.extensions.httpclient.*');

        if ( ! $this->authenticate() )
            throw new Exception('Authentication failed');
    }

    protected function authenticate() {
        $authenticateUrl = $this->_apiBaseUrl . '/authenticate/';

        $result = $this->makeRequest($authenticateUrl, array(
            'admin'=> $this->_apiUser,
            'pass'=> $this->_apiPass,
        ));

        if ($result) {
            $this->_apiToken = (string) $result->session->attributes()->id;

            return true;
        }

        return false;
    }


    /**
     * Creates a new conference room/invitation
     *
     * @param string $conferenceMode
     * @param string $roomName
     * @param string $startDateTime
     * @param integer $durationMinutes
     * @param array $media
     * @return bool|string
     */
    public function createMeeting($conferenceMode, $roomName, $startDateTime, $durationMinutes, $media = array()) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $createRoom = $xml->addChild('createroom');
        $createRoom->addAttribute('subject', $roomName);
        $createRoom->addAttribute('when', date('c', strtotime($startDateTime)));
        $createRoom->addAttribute('duration', $durationMinutes);

        $medialist = $createRoom->addChild('medialist');

        foreach ($media as $item) {
            $child = $medialist->addChild('media');
            $child->addAttribute('name', $item);
        }

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        if ($result) {

            // get room code
	        if($result->room->count() > 0 && $result->room->attributes() && $result->room->attributes()->code){
		        return ((string) $result->room->attributes()->code);
	        }else{
		        return FALSE;
	        }
        }


        return false;
    }


    /**
     * Modifies an existing room/invitation
     *
     * @param string $roomCode
     * @param string $conferenceMode
     * @param string $roomName
     * @param string $startDateTime
     * @param integer $duration
     * @param array $media
     * @return bool|string
     */
    public function updateMeeting($roomCode, $conferenceMode, $roomName, $startDateTime, $duration, $media = array()) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);

        $editRoom = $xml->addChild('editroom');
        $editRoom->addAttribute('code', $roomCode);
        $editRoom->addAttribute('subject', $roomName);
        $editRoom->addAttribute('when', date('c', strtotime($startDateTime)));
        $editRoom->addAttribute('duration', $duration);

        $medialist = $editRoom->addChild('medialist');

        foreach ($media as $item) {
            $child = $medialist->addChild('media');
            $child->addAttribute('name', $item);
        }

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        if ($result) {

            // get room code
            $roomCode = (string) $result->room->attributes()->code;

            return $roomCode;
        }


        return false;
    }


    /**
     * Deletes a conference
     *
     * @param $roomCode
     * @return bool
     */
    public function deleteMeeting($roomCode) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $destroyroom = $xml->addChild('destroyroom');
        $destroyroom->addAttribute('code', $roomCode);

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        return ($result) ? true : false;
    }

    /**
     * Adds participants to a room.
     *
     * Each participant data is an array containing these fields:
     *  - name: the full name of the participant (e.g., John Doe)
     *  - mail: a valid e-mail address (i.e., unique identifier within the system)
     *  - avatar: link to a valid image (this will be the participant display picture)
     *  - role: role of the participant within the conference (teacher/student/other?)
     *
     * The only mandatory attribute for each participant instance is mail, while the other attributes are optional.
     *
     * @param $roomCode
     * @param array $participants
     * @return bool
     */
    public function addParticipants($roomCode, $participants = array()) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $addparticipants = $xml->addChild('addparticipants');
        $addparticipants->addAttribute('code', $roomCode);

        $participantAttributes = array('name', 'mail', 'role', 'avatar');

        foreach ($participants as $participant) {
            $child = $addparticipants->addChild('participant');
            foreach ($participant as $k => $v) {
                if (in_array($k, $participantAttributes)) {
                    $child->addAttribute($k, $v);
                }
            }
        }

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        return ($result)? true : false;
    }

    /**
     * Modifies participants
     * @param $roomCode
     * @param array $participants
     * @return bool
     */
    public function modifyParticipants($roomCode, $participants = array()) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $modifyparticipants = $xml->addChild('modifyparticipants');
        $modifyparticipants->addAttribute('code', $roomCode);

        $participantAttributes = array('name', 'mail', 'role', 'avatar');

        foreach ($participants as $participant) {
            $child = $modifyparticipants->addChild('participant');
            foreach ($participant as $k => $v) {
                if (in_array($k, $participantAttributes)) {
                    $child->addAttribute($k, $v);
                }
            }
        }

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        return ($result)? true : false;
    }

    /**
     * Removes participants.
     * It is required to specify the 'mail' field for each participant. Reminder:
     *  - mail: a valid e-mail address (i.e., unique identifier within the system)
     *
     * @param $roomCode
     * @param array $participants
     * @return bool
     */
    public function removeParticipants($roomCode, $participants = array()) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $removeparticipants = $xml->addChild('removeparticipants');
        $removeparticipants->addAttribute('code', $roomCode);

        $participantAttributes = array('name', 'mail', 'role', 'avatar');

        foreach ($participants as $participant) {
            $child = $removeparticipants->addChild('participant');
            foreach ($participant as $k => $v) {
                if (in_array($k, $participantAttributes)) {
                    $child->addAttribute($k, $v);
                }
            }
        }

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        return ($result)? true : false;
    }

    /**
     * Returns information about a specific room
     *
     * @param string $roomCode
     * @return array|bool
     */
    public function getRoomInfo($roomCode) {

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);


        $getinfo = $xml->addChild('getinfo');
        $getinfo->addAttribute('code', $roomCode);

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        if ($result) {

            $room = $result->room;
            $roomAttr = $room->attributes();
            $media = array();
            $participants = array();

            foreach ($room->medialist->media as $item) {
                $value = (string) $item->attributes()->name;
                $media[] = $value;
            }

            foreach ($room->participants->participant as $participant) {
                $participants[] = array(
                    'name' => (string) $participant->attributes()->name,
                    'mail' => (string) $participant->attributes()->mail,
                    'role' => (string) $participant->attributes()->role,
                    'avatar' => (string) $participant->attributes()->avatar
                );
            }

            $output = array(
                'room_id' => (string) $roomAttr->code,
                'subject' => (string) $roomAttr->subject,
                'when' => (string) $roomAttr->when,
                'duration' => (string) $roomAttr->duration,
                'media' => $media,
                'participants' => $participants
            );

            return $output;

        }

        return false;
    }

    /**
     * Adds a callback url for these event types: 'start', 'end' or 'recording'
     *
     * @param $roomCode
     * @param $eventType
     * @param $callbackUrl
     * @return bool
     * @throws Exception
     */
    public function setCallback($roomCode, $eventType, $callbackUrl) {

        if ( ! in_array($eventType, array('start', 'end', 'recording'))) {
            throw new Exception('Invalid event type');
        }

        $xml = "<?xml version='1.0' encoding='utf-8'?><meetechorequest id='". uniqid() ."'></meetechorequest>";
        $xml = new SimpleXMLElement($xml);

        $setcallback = $xml->addChild('setcallback');
        $setcallback->addAttribute('code', $roomCode);

        $event = $setcallback->addChild('event');
        $event->addAttribute('name', $eventType);
        $event->addAttribute('uri', $callbackUrl);

        $result = $this->makeRequest($this->getRequestUrl(), $xml->asXML());

        return ($result) ? true : false;
    }

    /**
     * @return string
     */
    protected function getRequestUrl() {

        $url = $this->_apiBaseUrl . '/' . $this->_apiToken . '/';

        return $url;
    }


    /**
     * Makes a POST request to the Meetecho API
     *
     * @param $url
     * @param array|string $data
     * @return bool|null|SimpleXMLElement
     * @throws Exception
     */
    protected function makeRequest($url, $data) {

        $client = new EHttpClient($url, array(
            'adapter'      => 'EHttpClientAdapterCurl'
        ));

        if (is_array($data)) {

            $data = http_build_query($data, '', '&');

        } else {
            // $data is string ==> meaning that the request should be in xml format

            $data = urlencode($data);
        }

        $client->setRawData($data);

        $response = $client->request(EHttpClient::POST);

        // Check if we've got an error HTTP status; throw exception with error message
        if ($client->getLastResponse()->isError()) {
            throw new Exception($client->getLastResponse()->getMessage(), $client->getLastResponse()->getStatus());
        }

        if (!$response) {
			Yii::log("meetecho : {$url} \n data: {$data}", 'error');
			Yii::log("    response: {$response->getBody()}", 'error');
            throw new Exception('Invalid response from server');
        }

        // Decode response to an array
        $body = $response->getBody();
        $xmlAnswer = null;
        if ($body) {
            // STOP XML runtime errors
            libxml_use_internal_errors(true);

            // Load XML object from request body
            $xmlAnswer = new SimpleXMLElement($body);

            //CVarDumper::dump($xmlAnswer,20,true); die;

            // Collect errors (if result is not valid) and throw exception
            if (!$xmlAnswer){
                $errors = '';
                foreach(libxml_get_errors() as $error) {
                    $errors .= ",\t" . $error->message;
                }
                $error = "XML parsing error(s): $errors";
                throw new Exception($error);
            }
        }

        $response = $client->getLastResponse();
        if ($response && $response->getStatus() == 200) {
            return $xmlAnswer;
        }

        return false;
    }

}