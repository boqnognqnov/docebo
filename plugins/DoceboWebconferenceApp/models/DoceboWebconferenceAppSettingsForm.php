<?php

/**
 * DoceboWebconferenceAppSettingsForm class.
 *
 * @property string $server_url
 * @property string $admin_username
 * @property string $admin_password
 * @property string $org_name
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 */
class DoceboWebconferenceAppSettingsForm extends CFormModel {
	public $server_url;
	public $admin_username;
	public $admin_password;
	//public $org_name;
	public $max_rooms;
	public $max_rooms_per_course;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('server_url, admin_username, admin_password, max_rooms, max_rooms_per_course', 'safe'),
			array('server_url, admin_username, admin_password, max_rooms, max_rooms_per_course', 'required'),
            array('max_rooms, max_rooms_per_course', 'numerical', 'min'=>1),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
            'server_url' => Yii::t('custom_domain', '_DOMAIN_NAME_'),
            'admin_username' => Yii::t('standard', '_USERNAME'),
            'admin_password' => Yii::t('standard', '_PASSWORD'),
			'org_name' => Yii::t('standard','Company name'),
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
		);
	}


	public function beforeValidate() {
		return parent::beforeValidate();
	}


}
