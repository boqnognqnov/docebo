<?php

/**
 * Plugin for CoursecatalogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class HDGoldApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'HDGoldApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		Settings::save("helpdesk_level", "gold");
		parent::activate();
		// sso_token, don't touch it
		// sso_secret, don't touch it on activation
	}

	public function deactivate() {
		Settings::save("helpdesk_level", "silver");
		parent::deactivate();
	}

}
