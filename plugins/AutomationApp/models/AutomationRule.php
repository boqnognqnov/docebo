<?php

/**
 * This is the model class for table "automation_rule".
 *
 * The followings are the available columns in table 'automation_rule':
 * @property integer                   $id_rule
 * @property string                    $name
 * @property string                    $description
 * @property integer                   $active
 * @property string                    $conditions_operator
 * @property string                    $trigger_type
 * @property integer                   $id_job       Only valid if $trigger_type=scheduled
 * @property string                    $trigger_events
 * @property string                    $schedule_unit
 * @property integer                   $schedule_day_of_month
 * @property integer                   $schedule_day_of_week
 * @property integer                   $schedule_hour
 * @property integer                   $schedule_minute
 *
 * The followings are the available model relations:
 * @property AutomationRuleAction[]    $automationRuleActions
 * @property AutomationRuleCondition[] $automationRuleConditions
 * @property AutomationRuleLog[]       $automationRuleLogs
 * @property CoreJob                   $scheduledJob The scheduled job, if the rule is of the type "scheduled" (not realtime)
 */
class AutomationRule extends CActiveRecord {
	const SCHEDULE_UNIT_DAY = 'day';
	const SCHEDULE_UNIT_WEEK = 'week';
	const SCHEDULE_UNIT_MONTH = 'month';

	const CONDITION_AND = 'AND';
	const CONDITION_OR = 'OR';

	const SCHEDULE_TYPE_RECURRING = AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_SCHEDULED;
	const SCHEDULE_TYPE_REALTIME = AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_REALTIME;

	static public $validScheduleUnits = array(
		self::SCHEDULE_UNIT_DAY,
		self::SCHEDULE_UNIT_WEEK,
		self::SCHEDULE_UNIT_MONTH
	);
	static public $validTriggerTypes = array(
		AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_REALTIME,
		AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_SCHEDULED,
	);

	static public $coreRules = array(
		'UserInactivityCondition',
		'UserAdditionalFieldValuesCondition',

		'UserLoggedInCondition',
		'UserExpirationDateCondition',

		'AdministratorUploadedFileInFtp',
		'UserBelongsToBranchOrGroupCondition',
	);

	static public $coreActions = array(
		'SetUserExpirationDateAction',
		'ImportUsersFromCSV',
		'ImportBranchesFromCSV',
	);

	/**
	 * @return string the associated database table name
	 */
	public function tableName () {
		return 'automation_rule';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules () {
		return array(
			array( 'name', 'required' ),
			array( 'active, id_job', 'numerical', 'integerOnly' => TRUE ),
			array( 'schedule_day_of_month', 'numerical', 'max' => 31 ),
			array( 'schedule_day_of_week', 'numerical', 'max' => 7 ),
			array( 'schedule_hour', 'numerical', 'max' => 23 ),
			array( 'schedule_minute', 'numerical', 'max' => 59 ),
			array( 'name', 'length', 'max' => 255 ),
			array( 'description, trigger_events', 'length', 'max' => 512 ),
			array( 'conditions_operator', 'length', 'max' => 3 ),
			array( 'trigger_type', 'length', 'max' => 9 ),
			array( 'schedule_unit', 'length', 'max' => 5 ),
			array(
				'name, description, schedule_day_of_month, schedule_day_of_week, schedule_hour, schedule_minute',
				'safe'
			),
			array(
				'id_rule, name, description, active, conditions_operator, trigger_type, trigger_events, schedule_unit, schedule_value',
				'safe',
				'on' => 'search'
			),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations () {
		return array(
			'automationRuleActions'    => array( self::HAS_MANY, 'AutomationRuleAction', 'id_rule' ),
			'automationRuleConditions' => array( self::HAS_MANY, 'AutomationRuleCondition', 'id_rule' ),
			'automationRuleLogs'       => array( self::HAS_MANY, 'AutomationRuleLog', 'id_rule' ),
			'scheduledJob'             => array( self::BELONGS_TO, 'CoreJob', 'id_job' ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels () {
		return array(
			'id_rule'             => 'Id Rule',
			'name'                => 'Name',
			'description'         => 'Description',
			'active'              => 'Active',
			'conditions_operator' => 'Conditions Operator',
			'trigger_type'        => 'Trigger Type',
			'trigger_events'      => 'Comma separated list of hook names which will trigger this rule',
			'schedule_unit'       => 'Schedule Unit',
			'schedule_value'      => 'Schedule Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search () {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare( 'id_rule', $this->id_rule );
		$criteria->compare( 'name', $this->name, TRUE );
		$criteria->compare( 'description', $this->description, TRUE );
		$criteria->compare( 'active', $this->active );
		$criteria->compare( 'conditions_operator', $this->conditions_operator, TRUE );
		$criteria->compare( 'trigger_type', $this->trigger_type, TRUE );
		$criteria->compare( 'trigger_events', $this->trigger_events, TRUE );
		$criteria->compare( 'schedule_unit', $this->schedule_unit, TRUE );
		$criteria->compare( 'schedule_value', $this->schedule_value );

		return new CActiveDataProvider( $this, array(
			'criteria' => $criteria,
		) );
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className active record class name.
	 *
	 * @return AutomationRule the static model class
	 */
	public static function model ( $className = __CLASS__ ) {
		return parent::model( $className );
	}

	public function beforeSave(){
		return parent::beforeSave();
	}

	public function afterSave () {
		parent::afterSave();
	}

	public function afterDelete () {
		AutomationRuleCondition::model()->deleteAllByAttributes( array( 'id_rule' => $this->id_rule ) );
		AutomationRuleAction::model()->deleteAllByAttributes( array( 'id_rule' => $this->id_rule ) );

		if( $this->trigger_type == self::SCHEDULE_TYPE_RECURRING && $this->scheduledJob ) {
			Yii::app()->scheduler->deleteJob( $this->scheduledJob, TRUE );
		}
	}

	public function dataProvider () {
		$params = array();

		// Create base query
		$query = Yii::app()->getDb()->createCommand()
					->select( '*' )
					->from( self::model()->tableName() . ' t' );

		// Create separate counter query for performance
		$queryCounter = clone $query;
		$queryCounter->select( 'COUNT(t.id_rule)' );
		$count = $queryCounter->queryScalar();

		$sortAttributes = array();
		foreach ( $this->attributeNames() as $attributeName ) {
			$sortAttributes[ $attributeName ] = 't.' . $attributeName;
		}

		$config = array(
			'params'         => $query,
			'totalItemCount' => $count,
			'sort'           => array(
				'attributes'   => $sortAttributes,
				'defaultOrder' => 'id_rule ASC'
			),
			'keyField'       => 'id_rule',
			'pagination'     => array(
				'pageSize' => Settings::get( 'elements_per_page', 10 )
			),
		);

		return new CSqlDataProvider( $query->getText(), $config );
	}

	/**
	 * @param array $supportingTriggerTypes
	 *
	 * @return array
	 * @throws \CException
	 */
	public static function getConditionHandlers ( $supportingTriggerTypes = array() ) {

		if( empty( $supportingTriggerTypes ) ) {
			// If no trigger types filter is passed, get ALL handlers
			// (supporting both realtime and scheduled trigger types)
			$supportingTriggerTypes = array(
				self::SCHEDULE_TYPE_RECURRING,
				self::SCHEDULE_TYPE_REALTIME,
			);
		}

		$res = array();

		foreach ( self::$coreRules as $rule ) {
			$ruleFullPath = 'AutomationApp.components.rules.' . $rule;

			try {
				$handler = Yii::createComponent( $ruleFullPath );
			}catch(Exception $e){
				Yii::log($e->getTraceAsString(), CLogger::LEVEL_ERROR);
				$handler = false;
			}

			if( $handler){

				// Check if this handler is allowed for any of the passed trigger types
				if(method_exists( $handler, 'allowedTriggerTypes' ) ) {
					$allowedTriggerTypes              = $handler->allowedTriggerTypes();
					$handlerAllowedForThisTriggerType = FALSE;
					foreach ( $allowedTriggerTypes as $type ) {
						if( in_array( $type, $supportingTriggerTypes ) ) {
							$handlerAllowedForThisTriggerType = TRUE;
							break;
						}
					}

					if( ! $handlerAllowedForThisTriggerType ) {
						continue;
					}
				}

				if(method_exists($handler, 'getShortDescription')){
					$res[ $ruleFullPath ] = $handler->getShortDescription();
				}else{
					$res[ $ruleFullPath ] = $rule;
				}

			}
		}

		$event = new DEvent( Yii::app()->getController(), array(
			'rules'                 => &$res,
			'supportingTriggerType' => $supportingTriggerTypes,
		) );
		Yii::app()->event->raise( 'collectCustomAutomationRules', $event );

		return $res;
	}
	/**
	 * @param array $excludeConditionActions
	 */
	private static function removeConditionActions($excludeConditionActions = array()){

		$activeConditions = Yii::app()->request->getParam('active-conditions');

		if($excludeConditionActions){
			foreach($activeConditions as $activeConditionDetail){
				$activeConditionDetail = CJSON::decode($activeConditionDetail);
				$handler = explode('.', $activeConditionDetail['handler']);
				$actionName = end($handler);
				$actionConditions = $excludeConditionActions[$actionName];
				if(isset($actionConditions)){
					foreach($actionConditions as $conditionName){
						if(($key = array_search($conditionName, self::$coreActions)) !== false) {
							unset(self::$coreActions[$key]);
						}
					}
				}
			}
		}
	}

	/**
	 * @return array
	 * @throws \CException
	 */
	public static function getActionsHandlers () {
		$res = array();

		// Here we describe in which condition which action to be removed (hidden)
		// Right now if (Administrator Uploaded File In Ftp) condition is active
		// We remove the (Set User Expiration Date) Action

		$removeConditionActions = array(
			'AdministratorUploadedFileInFtp' => array(
				'SetUserExpirationDateAction'
			)
		);

		self::removeConditionActions($removeConditionActions);

		foreach ( self::$coreActions as $action ) {
			$actionFullPath = 'AutomationApp.components.actions.' . $action;
			try{
				$handler = Yii::createComponent($actionFullPath);
			}catch (Exception $e){
				Yii::log($e->getTraceAsString(), CLogger::LEVEL_ERROR);
				$handler = false;
			}

			if($handler){
				if(method_exists($handler, 'getShortDescription')){
					$res[$actionFullPath] = $handler->getShortDescription();
				}else{
					$res[$actionFullPath] = $action;
				}
			}

		}

		$event = new DEvent( Yii::app()->getController(), array(
			'rules' => &$res,
		) );
		Yii::app()->event->raise( 'collectCustomAutomationActions', $event );

		return $res;
	}

	/**
	 * TODO: IMPLEMENT THIS
	 * Method called when this automation rule is to be triggered.
	 * It will check that conditions are met and, if yes, run the one or more actions.
	 * Every "real" execution of this method should write a log row.
	 *
	 * @params array Array of params passed to the rule (optional)
	 * @param array $eventParams Params from the raised Yii event (only for "realtime" rules)
	 *
	 * @return bool
	 * @throws \CException
	 */
	public function trigger ( $eventParams = array() ) {
		/* @var $dataAdapter AutomationAppParams */

		if(!$this->active)
			return FALSE;

		try {
			$log = array();

			// Instantiate a blank adapter here, that will be gradually
			// populated by each "condition handler", as it's being evaluated "if met"
			$dataAdapter = new AutomationAppParams();
            if ($this->id_job) {
                $dataAdapter->rule_name = $this->name;
                $dataAdapter->id_author = Yii::app()->db->createCommand()
                    ->select("id_author")
                    ->from(CoreJob::model()->tableName() . " cj")
                    ->where("cj.id_job = :id_job", array(':id_job' => $this->id_job))
                    ->queryScalar();
            }

			$conditionsMet = $this->_evaluateConditions( $eventParams, $dataAdapter );
			if( $conditionsMet ) {
				// Finally, execute all actions of this rule over the items from the adapter
				$this->_executeActions($dataAdapter);
			} else {
				Yii::log( 'At least one condition for rule ' . $this->id_rule . ' is not met', CLogger::LEVEL_ERROR );
				return FALSE;
			}

			$log['adapter'] = (array) $dataAdapter;

			// Log what we just did, to allow rollback (work in progress)
			$logModel          = new AutomationRuleLog();
			$logModel->id_rule = $this->id_rule;
			$logModel->result  = CJSON::encode( $log );
			$logModel->on_date = new CDbExpression( 'NOW()' );
			if( ! $logModel->save() )
				Yii::log(var_export($logModel->getErrors(),true), CLogger::LEVEL_ERROR);
		} catch ( Exception $e ) {
			Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR );
		}

		return TRUE;
	}

	/**
	 * Go through each "condition" of this rule, passing it a global
	 * params adapter object to be populated with items that satisfy
	 * the given "condition". The adapter is passed by reference
	 *
	 * @param bool                 $eventName   - The raised Yii event that lead to this rule's triggering,
	 *                                          if any (because rules can be triggered by schedule also,
	 *                                          this means without any event data)
	 * @param array                $eventParams The params from the raised Yii event
	 * @param \AutomationAppParams $globalAdapter
	 *
	 * @return bool
	 */
	private function _evaluateConditions ( $eventParams = array(), AutomationAppParams &$globalAdapter ) {
		$conditions = Yii::app()->getDb()->createCommand()
						 ->from( AutomationRuleCondition::model()->tableName() )
						 ->where( 'id_rule=:idRule', array( ':idRule' => $this->id_rule ) )
						 ->queryAll();

		Yii::log( 'Evaluating ' . count( $conditions ) . ' conditions using ' . $this->conditions_operator . ' operator', CLogger::LEVEL_INFO );

		$ruleConditionsAreMet = TRUE;

		if( $this->conditions_operator == self::CONDITION_AND ) {

			/**
			 * When the operator is AND, we evaluate each condition
			 * and collect all local "adapters" returned by those
			 * conditions. Finally we "intersect" entries in these adapters
			 * and build one master adapter that only contains entries
			 * that exist among ALL of those local adapters
			 */

			$localAdapterInstances = array();
			/* @var $localAdapterInstances AutomationAppParams[] */

			foreach ( $conditions as $condition ) {

				// Pass a "local" data adapter to each condition handler
				// so they can populate it. This adapter will later be
				// intersected with all other adapters by the other looped conditions
				$localConditionAdapter = new AutomationAppParams();
				$conditionModel = new AutomationRuleCondition();
				$conditionModel->setAttributes( $condition, FALSE );
				$conditionModel->setAdapter( $localConditionAdapter );
				$conditionModel->getHandlerObject();
				if( ! $conditionModel->isMet( $eventParams ) ) {
					Yii::log( "[RULE {$this->id_rule}] Condition " . $condition['id_condition'] . ' for rule ' . $this->id_rule . ' IS NOT met', CLogger::LEVEL_WARNING );
					$ruleConditionsAreMet = FALSE;
					break;
				} else {
					// Merge (intersect) the global and local adapters
					// so that ONLY users, courses, whatever items are
					// only present in the adapter if they are returned
					// by all conditions (since we are evaluating the AND operator here)
					Yii::log( "[RULE {$this->id_rule}] Condition " . $condition['id_condition'] . ' (handler: ' . $condition['handler'] . ') for rule ' . $this->id_rule . ' IS met', CLogger::LEVEL_WARNING );
					$localAdapterInstances[] = $localConditionAdapter;
				}
			}


			if( $ruleConditionsAreMet ) {
				// ALL conditions in the rule are met

				// Merge (intersect) the data between all adapters
				// and put them in the main adapter that will be passed
				// to the Rule Actions

				// Get possible data arrays in the adapter (public properties of class)
				$dataArraysInMasterAdapter = call_user_func( 'get_object_vars', $globalAdapter );
				foreach ( array_keys( $dataArraysInMasterAdapter ) as $adapterPart ) {
					$arrayOfData = FALSE;
					foreach ( $localAdapterInstances as $localAdapter ) {
						if( property_exists( $localAdapter, $adapterPart ) ) {
							if( FALSE == $arrayOfData ) {
								// First iteration
								$arrayOfData = array_values( $localAdapter->$adapterPart );
							}
							$arrayOfData = array_intersect( $arrayOfData, array_values( $localAdapter->$adapterPart ) );
						}
					}

					if( $arrayOfData ) {
						// Populate the master adapter's data array with the
						// intersected item values from all local adapters
						$globalAdapter->$adapterPart = array_merge( $arrayOfData, $globalAdapter->$adapterPart );
					}
				}
			}

		} elseif( $this->conditions_operator == self::CONDITION_OR ) {

			// Collect all adapters between all conditions and finally
			// merged them in the master adapter, that will be passed
			// to the actions
			$localAdapterInstances = array();
			foreach ( $conditions as $condition ) {
				$localAdapter = new AutomationAppParams();
				$conditionModel = new AutomationRuleCondition();
				$conditionModel->setAttributes( $condition, FALSE );
				$conditionModel->setAdapter( $localAdapter );
				$conditionModel->getHandlerObject();
				if( $conditionModel->isMet( $eventParams ) ) {
					$ruleConditionsAreMet = TRUE;
					$localAdapterInstances[] = $localAdapter;
				}else{
				}
			}

			// Get possible data arrays in the adapter (public properties of class)
			$dataArraysInMasterAdapter = call_user_func( 'get_object_vars', $globalAdapter );
			foreach ( array_keys( $dataArraysInMasterAdapter ) as $adapterPart ) {
				$arrayOfData = array();
				foreach ( $localAdapterInstances as $localAdapter ) {
					if( property_exists( $localAdapter, $adapterPart ) ) {
						$arrayOfData = array_merge( $arrayOfData, array_values( $localAdapter->$adapterPart ) );
					}
				}

				if( ! empty( $arrayOfData ) ) {
					// Populate the master adapter's data array with the
					// collected values from all local adapters from "met" conditions
					$globalAdapter->$adapterPart = array_merge( $arrayOfData, $globalAdapter->$adapterPart );
				}
			}
		}

		return $ruleConditionsAreMet;
	}

	private function _executeActions ( AutomationAppParams $paramsAdapter ) {
		$actions = Yii::app()->getDb()->createCommand()
					  ->from( AutomationRuleAction::model()->tableName() )
					  ->where( 'id_rule=:idRule', array( ':idRule' => $this->id_rule ) )
					  ->queryAll();

		Yii::log( 'Executing ' . count( $actions ) . ' action for rule ' . $this->id_rule, CLogger::LEVEL_INFO );

		foreach ( $actions as $action ) {
			$actionModel = new AutomationRuleAction();
			$actionModel->setAttributes( $action, FALSE );
			$actionModel->setAdapter( $paramsAdapter );
			$actionModel->getHandlerObject();
			$actionModel->run( $paramsAdapter );
			$actionModel = NULL;
		}
	}
}
