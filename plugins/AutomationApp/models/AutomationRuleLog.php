<?php

/**
 * This is the model class for table "automation_rule_log".
 *
 * The followings are the available columns in table 'automation_rule_log':
 * @property integer $id_log
 * @property integer $id_rule
 * @property string $on_date
 * @property string $result
 * @property integer $rolled_back
 *
 * The followings are the available model relations:
 * @property AutomationRule $idRule
 */
class AutomationRuleLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'automation_rule_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rule, on_date', 'required'),
			array('id_rule, rolled_back', 'numerical', 'integerOnly'=>true),
			array('result', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_log, id_rule, on_date, result, rolled_back', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRule' => array(self::BELONGS_TO, 'AutomationRule', 'id_rule'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_log' => 'Id Log',
			'id_rule' => 'Id Rule',
			'on_date' => 'On Date',
			'result' => 'Result',
			'rolled_back' => 'Rolled Back',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_log',$this->id_log);
		$criteria->compare('id_rule',$this->id_rule);
		$criteria->compare('on_date',$this->on_date,true);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('rolled_back',$this->rolled_back);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutomationRuleLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
