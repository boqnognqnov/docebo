<?php

/**
 * This is the model class for table "automation_rule_condition".
 *
 * The followings are the available columns in table 'automation_rule_condition':
 * @property integer $id_condition
 * @property integer $id_rule
 * @property string $handler
 * @property string $condition_params
 *
 * The followings are the available model relations:
 * @property AutomationRule $idRule
 */
class AutomationRuleCondition extends CActiveRecord
{
	/**
	 * The internal handler object used to forward some calls
	 * @var AutomationConditionHandler
	 */
	private $handlerObject;

	/**
	 * An adapter, passed around to all conditions for them
	 * to populate gradually with "items" (e.g. users, courses
	 * or other entities) that satisfy the condition
	 * @var AutomationAppParams
	 */
	private $adapter = null;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'automation_rule_condition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_rule, handler', 'required'),
			array('id_rule', 'numerical', 'integerOnly'=>true),
			array('handler', 'length', 'max'=>255),
			array('condition_params', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_condition, id_rule, handler, condition_params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRule' => array(self::BELONGS_TO, 'AutomationRule', 'id_rule'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_condition' => 'Id Condition',
			'id_rule' => 'Id Rule',
			'handler' => 'Handler',
			'condition_params' => 'Condition Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_condition',$this->id_condition);
		$criteria->compare('id_rule',$this->id_rule);
		$criteria->compare('handler',$this->handler,true);
		$criteria->compare('condition_params',$this->condition_params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutomationRuleCondition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Loads and initializes the handler
	 */
	public function afterFind()
	{
		parent::afterFind();

		// Initialize the handler object with the condition params
		$this->getHandlerObject();
	}

	/**
	 * @return \AutomationConditionHandler|mixed
	 */
	public function getHandlerObject(){
		if(!$this->handlerObject) {
			try {
				if($this->handler) {
					$this->handlerObject = Yii::createComponent($this->handler);
					if(is_array($this->condition_params)) {
						$params = $this->condition_params;
					}elseif(!empty($this->condition_params)){
						$params = CJSON::decode($this->condition_params);
					}else{
						$params = array();
					}

					if($this->adapter) {
						$this->handlerObject->setAdapter( $this->adapter );
					}

					$this->handlerObject->init($params);
				} else
					throw new CException("Missing handler in condition instance");
			} catch(Exception $e) {
				Yii::log("Failed to initialize automation condition. Error:".$e->getMessage(), CLogger::LEVEL_ERROR);
			}
		}

		return $this->handlerObject;
	}

	/**
	 * Method called when this condition must be verified
	 *
	 * @param array $eventParams Passed if the underlaying rule was triggered by a raised event
	 *                           (in contrast to a rule triggered by schedule -> no event raised)
	 *
	 * @return bool
	 */
	public function isMet($eventParams = array()) {
		$eventName = isset( $eventParams['event_name'] ) ? $eventParams['event_name'] : FALSE;

		if($this->handlerObject) {
			return $this->handlerObject->verify($eventName, $eventParams);
		}else {
			return FALSE;
		}
	}

	public function setAdapter ( AutomationAppParams &$adapter ) {
		$this->adapter = &$adapter;
	}

	public function beforeSave()
	{
		if(is_array($this->condition_params)){
			$this->condition_params = CJSON::encode($this->condition_params);
		}elseif(is_string($this->condition_params)){
			// Already a JSON
		}
		return parent::beforeSave();
	}
}
