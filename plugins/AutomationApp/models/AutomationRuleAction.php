<?php

/**
 * This is the model class for table "automation_rule_action".
 *
 * The followings are the available columns in table 'automation_rule_action':
 * @property integer $id_action
 * @property integer $id_rule
 * @property string $handler
 * @property string $action_params
 *
 * The followings are the available model relations:
 * @property AutomationRule $idRule
 */
class AutomationRuleAction extends CActiveRecord
{
	/**
	 * The internal handler object used to forward some calls
	 * @var AutomationActionHandler
	 */
	private $handlerObject;

	/**
	 * The params adapter, populated with values by the conditions
	 * as they are satisfied
	 *
	 * @var AutomationAppParams
	 */
	private $paramsAdapter;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'automation_rule_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_rule, handler', 'required'),
			array('id_rule', 'numerical', 'integerOnly'=>true),
			array('handler', 'length', 'max'=>255),
			array('action_params', 'safe'),
			array('id_action, id_rule, handler, action_params', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'idRule' => array(self::BELONGS_TO, 'AutomationRule', 'id_rule'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_action' => 'Id Action',
			'id_rule' => 'Id Rule',
			'handler' => 'Handler',
			'action_params' => 'Action Params',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_action',$this->id_action);
		$criteria->compare('id_rule',$this->id_rule);
		$criteria->compare('handler',$this->handler,true);
		$criteria->compare('action_params',$this->action_params,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AutomationRuleAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Loads and initializes the handler
	 */
	public function afterFind()
	{
		parent::afterFind();

		// Initialize the handler object with the condition params
		$this->getHandlerObject();
	}

	/**
	 * Method called when this action must run
	 *
	 * @param \AutomationAppParams $paramsAdapter
	 *
	 * @return bool
	 */
	public function run(AutomationAppParams $paramsAdapter) {
		if($this->handlerObject) {
			if(!$paramsAdapter){
				return $this->handlerObject->run( $this->paramsAdapter );
			}else{
				return $this->handlerObject->run( $paramsAdapter );
			}
		}else {
			return FALSE;
		}
	}

	/**
	 * @return \AutomationConditionHandler|mixed
	 */
	public function getHandlerObject(){
		if(!$this->handlerObject) {
			try {
				if($this->handler) {
					$this->handlerObject = Yii::createComponent($this->handler);
					if(is_array($this->action_params)) {
						$params = $this->action_params;
					}elseif($this->action_params){
						$params = CJSON::decode($this->action_params);
					}else{
						$params = array();
					}
					$this->handlerObject->init($params);
				} else
					throw new CException("Missing handler in action instance");
			} catch(Exception $e) {
				Yii::log("Failed to initialize automation action. Error:".$e->getMessage(), CLogger::LEVEL_ERROR);
			}
		}

		return $this->handlerObject;
	}

	public function setAdapter ( AutomationAppParams $paramsAdapter ) {
		$this->paramsAdapter = $paramsAdapter;
	}

	public function beforeSave()
	{
		if(is_array($this->action_params)){
			$this->action_params = CJSON::encode($this->action_params);
		}elseif(is_string($this->action_params)){
			// Already a JSON
		}
		return parent::beforeSave();
	}
}
