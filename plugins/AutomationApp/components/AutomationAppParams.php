<?php
/**
 * A common adapter, passed around between conditions and actions
 * for easier transfer of data.
 *
 * For example, while multiple "scheduled" conditions are evaluated,
 * they might populate the $users array with users that match the given
 * condition, and later this list may be used by the "actions" as items
 * to execute the actions on
 *
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 17.11.2015 г.
 * Time: 12:54 ч.
 */

class AutomationAppParams {
    public $rule_name = '';
    public $id_author = null;

	public $users = array();
	public $courses = array();

	/**
	 * Automation "condition handlers" should populate this field
	 * of the adapter with anything else that is not course or user IDs.
	 *
	 * PLEASE, don't just directly populate the top level array,
	 * but instead use a meaningful array key and store the data there.
	 * This is useful, because the $others property may be populated
	 * with multiple "types" of other data
	 *
	 * E.g. to preserve a list of file names (that will later be used by
	 * the automation "action handlers" as items to execute actions on),
	 * populate using:
	 * $adapter->others['filenames'] = $arrayOfFiles;
	 *
	 * @var array
	 */
	public $others = array();
}