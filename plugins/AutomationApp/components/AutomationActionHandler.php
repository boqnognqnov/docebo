<?php

/**
 * Class AutomationActionHandler
 *
 * Abstract class to be implemented by all automation action types
 */
abstract class AutomationActionHandler extends CComponent
{
	/**
	 * Action params
	 * @var array
	 */
	protected $params;

	/**
	 * Called to pass initialization params to this action handler
	 * @param array $initParams
	 */
	public function init($initParams = array()) {
		$this->params = $initParams;
	}

	/**
	 * Returns HTML code to display as custom form settings
	 */
	public function renderActionParamsForm() {
		return '';
	}

	/**
	 * Abstract method called when this action must be run
	 *
	 * @param \AutomationAppParams $paramsAdapter
	 *
	 * @return bool
	 */
	public abstract function run(AutomationAppParams $paramsAdapter);

	/**
	 * Return a localized description that describes
	 * what this action does (in a few words, usually displayed
	 * in dropdowns or other UI areas where actions are listed)
	 * @return string
	 */
	public abstract function getShortDescription();

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly this action does, when runned.
	 * For example if the implementation of the condition handler represents
	 * "Set user to {status}" and the $params
	 * array contains: array('status'=>'inactive'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "Set user status to Inactive",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public abstract function describeActionByParams($params = array());
}