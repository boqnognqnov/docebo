<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 11.11.2015 г.
 * Time: 18:47 ч.
 */
class SetUserExpirationDateAction extends AutomationActionHandler {

	/**
	 * Returns HTML code to display as custom form settings
	 */
	public function renderConditionParamsForm($params = array()) {
		Yii::app()->clientScript->registerCss('SetUserExpirationDate', '
		.actions-settings .user-inactivity-rule-settings input,
		.actions-settings .user-inactivity-rule-settings select{
		width: 100%;
		}
		');
		Yii::app()->getController()->renderPartial('AutomationApp.components.actions.views.set_user_expiration_date', array(), false, true);
	}

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly this action does, when runned.
	 * For example if the implementation of the condition handler represents
	 * "Set user to {status}" and the $params
	 * array contains: array('status'=>'inactive'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "Set user status to Inactive",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function describeActionByParams ( $params = array() ) {
		if((!$params || empty($params)) && !empty($this->params)){
			// Take params from the current instance, if set
			$params = $this->params;
		}

		$days = $params['days_list'];

		$dateUnit = null;
		switch($days){
			case 'empty':
				$dateUnit = Yii::t('automation', 'Empty value');
				break;
			case 'today':
				$dateUnit = Yii::t( 'calendar', '_TODAY' );
				break;
			case 'in7days':
				$dateUnit = Yii::t( 'automation', 'Today + 7 days' );
				break;
			case 'in30days':
				$dateUnit = Yii::t( 'automation', 'Today + 30 days' );
				break;
		}

		return Yii::t('automation', 'Set user expiration date to {date}', array(
			'{date}'=>$dateUnit,
		));
	}

	/**
	 * Abstract method called when this action must be run
	 *
	 * @param \AutomationAppParams $paramsAdapter
	 *
	 * @return bool
	 */
	public function run ( AutomationAppParams $paramsAdapter ) {
		$dayOfExpiration = $this->params['days_list'];

		foreach($paramsAdapter->users as $key=>$userId) {
			if(CoreUser::isUserGodadmin($userId)){
				unset($paramsAdapter->users[$key]);
			}
		}
		Yii::app()->event->raise('OnSettingUserExpirationDateAction', new DEvent($this, array(
				'paramsAdapter' => &$paramsAdapter
		)));

		$userIds = $paramsAdapter->users;

		// Prepare the general criteria object
		$criteria = new CDbCriteria();
		if($dayOfExpiration != 'empty')
			$criteria->addCondition('expiration IS NULL OR expiration="0000-00-00"');
		$criteria->addInCondition('idst', $userIds);

		switch($dayOfExpiration){
			case 'empty':
				CoreUser::model()->updateAll(array('expiration'=>null), $criteria);
				break;
			case 'today':
				CoreUser::model()->updateAll(array('expiration'=>new CDbExpression('NOW()')), $criteria);
				break;
			case 'in7days':
				CoreUser::model()->updateAll(array('expiration'=>new CDbExpression('NOW() + INTERVAL 7 DAY')), $criteria);
				break;
			case 'in30days':
				CoreUser::model()->updateAll(array('expiration'=>new CDbExpression('NOW() + INTERVAL 30 DAY')), $criteria);
				break;
		}
	}

	/**
	 * Return a localized description that describes
	 * what this action does (in a few words, usually displayed
	 * in dropdowns or other UI areas where actions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t( 'automation', 'Set user expiration date' );
	}
}