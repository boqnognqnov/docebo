<?php

class ImportBranchesFromCSV extends AutomationActionHandler {

    const MAX_BRANCHES_PER_RUN = 300;
    /**
     * Returns HTML code to display as custom form settings
     */
    public function renderConditionParamsForm() {
        Yii::app()->clientScript->registerCss('import-branches-from-csv', '
            div.import-branch-rule-settings.import p.params-title{
                margin-left: 3px;
            }
            div.import-branch-rule-settings.import label{
                margin-bottom: 2px;
                margin-left: 2px;
            }
            div.import-branch-rule-settings.import .org-chart > label{
                margin-left: 0px;
            }
            div.import-branch-rule-settings.import label.select-separator{
                margin-left: 3px;
            }
            div.import-branch-rule-settings.import div.csv-row-separators > span span{
                margin-left: 15px
            }
            div.import-branch-rule-settings.import div.row-fluid{
                margin-top: 15px;
            }
            .import .row-fluid label, .import .row-fluid span, .import .row-border > div, .import .row-fluid input {
                display: inline-block;
                vertical-align: middle;
            }
            div.import-branch-rule-settings.import a.import-schema-input{
                width: 110px;
            }
            div.import-branch-rule-settings.import div.csv-row-separators{
                margin-top: 10px;
            }
            div.import-branch-rule-settings.import div.csv-row-separators input#manual_separator{
                width: 100px;
            }
            div.import-branch-rule-settings.import div.csv-row-separators > span label{
                margin-right: 10px;
            }
            div.import-branch-rule-settings.import .span6.file-charset select, div.import-branch-rule-settings.import .span6.org-chart select{
                width: 100% !important;
            }
            div.import-branch-rule-settings.import .send-log-to-mail input{
                width: 95%;
            }
            div.import-branch-rule-settings.import div.csv-row-separators > span > span:first-of-type,
            div.import-branch-rule-settings.import span#require-password-change-input > span:first-of-type{
                margin-left: 0px;
                padding-left: 0px;
            }
            div.import-branch-rule-settings.import .require-password-change label{
                margin-bottom: 10px;
            }
            div.import-branch-rule-settings.import #require-password-change-input label{
                padding-top:8px;
            }
            .download-file .download-icon {
                height: 20px;
                width: 16px;
                vertical-align: middle;
                background: url(../themes/spt/images/icons_elements.png) -253px -178px no-repeat;
                margin-right: 5px;
            }
            div.importSchemaButton{
                display: inline-table !important;
            }
            .download-file{
                margin-left: 30px;
                margin-top: 3px;
                display: inline-block;
            }
            div.download-file a{
                text-decoration: underline;
            }
		');

        $model = new OrgChartImportForm();

        Yii::app()->getController()->renderPartial('AutomationApp.components.actions.views.import_branches_from_csv', array(
            'model' => $model
        ), false, true);

    }

    /**
     * Your implementation should take the array of params, unique to
     * the handler and return a localized message, describing to the user
     * when exactly this action does, when runned.
     * For example if the implementation of the condition handler represents
     * "Set user to {status}" and the $params
     * array contains: array('status'=>'inactive'),
     * the implementation of the method below may for example return a localized
     * message along the lines of "Set user status to Inactive",
     * replacing the dynamic parts of the message depending
     * on the values in $params.
     *
     * @param array $params
     *
     * @return mixed
     */
    public function describeActionByParams ( $params = array() ) {
        return $this->getShortDescription();
    }

    /**
     * Abstract method called when this action must be run
     *
     * @param \AutomationAppParams $paramsAdapter
     *
     * @return bool
     */
    public function run ( AutomationAppParams $paramsAdapter ) {

        // Let's get the ftp params....
        // Since we are here this means that they are validated and working and everything is ok
        $ftp = $paramsAdapter->others[0];
        $file = $this->params;

        //Now we need to create a recurring background job that will do all the work
        $params = array('isFirst' =>true, 'ftp' => $ftp, 'form' => $file );
        $name = 'AutomationAppBranchesImport';
        $handler = AutomationAppBranchImport::id();
        // Let's create a immidiate job that will
        Yii::app()->scheduler->createImmediateJob($name, $handler, $params, false, null, true);
    }

    /**
     * Return a localized description that describes
     * what this action does (in a few words, usually displayed
     * in dropdowns or other UI areas where actions are listed)
     * @return string
     */
    public function getShortDescription () {
        return Yii::t( 'automation', 'Import branches from CSV' );
    }
}