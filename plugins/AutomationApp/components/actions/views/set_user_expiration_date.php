<div class="user-inactivity-rule-settings">
	<p><?= Yii::t( 'standard', 'When' ) ?></p>

	<div>
		<?= CHtml::dropDownList( 'days_list', NULL, array(
			'empty'    => Yii::t( 'automation', 'Empty value' ),
			'today'    => Yii::t( 'calendar', '_TODAY' ),
			'in7days'  => Yii::t( 'automation', 'Current date + 7 days' ),
			'in30days' => Yii::t( 'automation', 'Current date + 30 days' ),
		) ) ?>
	</div>
</div>