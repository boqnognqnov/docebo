<?php $isHydraThemeActive = Yii::app()->legacyWrapper->hydraFrontendEnabled();?>
<div class="import-users-rule-settings import">
    <p class="params-title"><?= Yii::t( 'standard', 'Params' ) ?>:</p>

    <div class="row-fluid">
        <div class="span2">
            <?php
                echo CHtml::link('<span></span>'. strtoupper(Yii::t('organization_chart', '_IMPORT_MAP')), Yii::app()->createUrl('AutomationApp/additionalActions/importSchema'), array(
                    'class' => 'btn btn-docebo blue big import-schema-input open-dialog',
                    'alt' => Yii::t('organization_chart', '_IMPORT_MAP'),
                    'data-dialog-class' => 'importSchema',
                ));
            ?>
        </div>

        <div class="importSchema" style="display: none">
            <?php echo CHtml::textField('UserImportForm[importMap]', null, array('id' => 'importMap')); ?>
        </div>
    </div>

    <div class="row-fluid">
        <?= CHtml::activeLabel($model, 'separator', array('class' => 'select-separator')); ?>
        <div class="csv-row-separators">
            <?= CHtml::activeRadioButtonList($model, 'separator', array(
                'auto' => Yii::t('standard', '_AUTODETECT'),
                'comma' => ',',
                'semicolon' => ';',
                'manual' => Yii::t('standard', '_MANUAL'),
            ), array(
                'separator' => '',
            )); ?>
            <?= CHtml::activeTextField($model, 'manualSeparator', array('id' => 'manual_separator')); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <?= CHtml::activeCheckBox($model, 'firstRowAsHeader'); ?>
            <?= CHtml::activeLabel($model, 'firstRowAsHeader'); ?>
        </div>
        <div class="span6">
            <?= CHtml::activeCheckBox($model, 'insertUpdate'); ?>
            <?= CHtml::activeLabel($model, 'insertUpdate'); ?>
        </div>
    </div>

	<?php if($isHydraThemeActive): ?>
		<div class="row-fluid">
			<div class="span6 file-charset">
				<?= CHtml::activeLabel($model, 'charset'); ?>
				<?= CHtml::activeDropDownList($model, 'charset', UserImportForm::getDropDownCharsets()); ?>
			</div>
			<div class="span6 send-log-to-mail">
				<?= CHtml::label(Yii::t('automation', 'Email address for error log'), 'sendLogToMail'); ?>
				<?= CHtml::textField('UserImportForm[sendLogToMail]'); ?>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6 branch-options">
				<?= CHtml::activeLabel($model, 'branchOption'); ?>
				<div>
					<?= CHtml::activeRadioButtonList($model, 'branchOption', array(
						'existing' => Yii::t('user_management', '_EXISTING_BRANCH'),
						'autocreate' => Yii::t('user_management', '_CREATE_BRANCH'),
					), array(
						'separator' => '',
						'baseID' => 'branch-options-input',
						'onChange' => 'onBranchOptionChange(this);'
					)); ?>
				</div>
			</div>
			<div class="span6 org-chart">
				<?= CHtml::activeLabel($model, 'node'); ?>
				<?php
				$orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree(true, true, true);
				$fake_node = new CoreOrgChartTree();
				$fake_node->idOrg = '-1';
				$fake_node->coreOrgChart = new CoreOrgChart();
				$fake_node->coreOrgChart->translation = Yii::t('preassessment', '_DO_NOTHING');
				array_unshift($orgChartsInfo['list'], $fake_node);
				echo CHtml::activeDropDownList($model, 'node', CHtml::listData($orgChartsInfo['list'], 'idOrg', 'coreOrgChart.translation'), array('encode' => false));
				?>
			</div>
		</div>
	<?php else: ?>
		<div class="row-fluid">
			<div class="span6 file-charset">
                <?= CHtml::activeLabel($model, 'charset'); ?>
                <?= CHtml::activeDropDownList($model, 'charset', UserImportForm::getDropDownCharsets()); ?>
			</div>
			<div class="span6 org-chart">
                <?= CHtml::activeLabel($model, 'node'); ?>
                <?php
                $orgChartsInfo = CoreOrgChartTree::getFullOrgChartTree(true, true, true);
                $fake_node = new CoreOrgChartTree();
                $fake_node->idOrg = '-1';
                $fake_node->coreOrgChart = new CoreOrgChart();
                $fake_node->coreOrgChart->translation = Yii::t('preassessment', '_DO_NOTHING');
                array_unshift($orgChartsInfo['list'], $fake_node);
                echo CHtml::activeDropDownList($model, 'node', CHtml::listData($orgChartsInfo['list'], 'idOrg', 'coreOrgChart.translation'), array('encode' => false));
                ?>
			</div>
		</div>

		<div class="row-fluid">
			<div class="span6 send-log-to-mail">
                <?= CHtml::label(Yii::t('automation', 'Email address for error log'), 'sendLogToMail'); ?>
                <?= CHtml::textField('UserImportForm[sendLogToMail]'); ?>
			</div>
		</div>
	<?php endif; ?>

    <div class="row-fluid require-password-change">
        <?= CHtml::activeLabel($model, 'passwordChanging'); ?>
        <div>
            <?= CHtml::activeRadioButtonList($model, 'passwordChanging', array(
                'no' => Yii::t('standard', '_NO'),
                'yes' => Yii::t('standard', '_YES'),
            ), array(
                'separator' => '',
                'baseID' => 'require-password-change-input'
            )); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="clearfix ignorePasswordChange">
            <?= CHtml::activeCheckBox($model, 'ignorePasswordChangeForExistingUsers'); ?>
            <div class="user-import-options-label">
                <?= CHtml::activeLabelEx($model, 'ignorePasswordChangeForExistingUsers'); ?>
            </div>
        </div>
    </div>

	<div class="row-fluid">
		<?= CHtml::activeCheckBox($model, 'autoAssignBranches'); ?>
		<?= CHtml::activeLabel($model, 'autoAssignBranches'); ?>
		
	</div>

</div>

<script type="text/javascript">
    (function ($) {
        $(document).controls();
        $(function () {
            $('input,select').styler();

            // Handle IMPORT SCHEMA INPUT
            $(".import-schema-input").on("click", function(){
                $(".rule-wizard-dialog").hide();
            });
            $(document).on("dialog2.closed", '.importSchema', function(){
                $(".rule-wizard-dialog").show();
                $('.modal-backdrop:first-of-type').remove();
            })

        });
    })(jQuery);

    <?php if($isHydraThemeActive): ?>
		setTimeout(function() {
			var branchOption = $('input[name="UserImportForm[branchOption]"]:checked').val();
			if(branchOption == 'autocreate') {
				$(".org-chart").hide();
			}
		}, 100);

		function onBranchOptionChange(ev) {
			var value = $(ev).val();
			//branch dropdown is shown only on "existing branch" option
			if(value == "autocreate") {
				$(".org-chart").hide();
			} else if(value == "existing") {
				$(".org-chart").show();
			}
		}
    <?php endif;?>
</script>