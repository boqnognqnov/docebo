<div class="import-branch-rule-settings import">
    <p class="params-title"><?= Yii::t( 'standard', 'Params' ) ?>:</p>

    <div class="row-fluid">
        <div class="span2 importSchemaButton">
            <?php
            echo CHtml::link('<span></span>'. strtoupper(Yii::t('organization_chart', '_IMPORT_MAP')), Yii::app()->createUrl('AutomationApp/additionalActions/importBranchSchema'), array(
                'class' => 'btn btn-docebo blue big import-schema-input open-dialog',
                'alt' => Yii::t('organization_chart', '_IMPORT_MAP'),
                'data-dialog-class' => 'importSchema',
            ));
            ?>
        </div>

        <div class="importSchema" style="display: none">
            <?php echo CHtml::textField('OrgChartImportForm[importMap]', null, array('id' => 'importMap')); ?>
        </div>

        <div class="download-file">
            <span>
                <span class="download-icon"></span>
                <?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), Yii::app()->createUrl('AutomationApp/additionalActions/DownloadBranchImportSample', array('download' => 'sample'))); ?>
            </span>
            <?php echo Yii::t('standard', '_A_SAMPLE_CSV_FILE'); ?>
        </div>
    </div>

    <div class="row-fluid">
        <?= CHtml::activeLabel($model, 'separator', array('class' => 'select-separator')); ?>
        <div class="csv-row-separators">
            <?= CHtml::activeRadioButtonList($model, 'separator', array(
                'auto' => Yii::t('standard', '_AUTODETECT'),
                'comma' => ',',
                'semicolon' => ';',
                'manual' => Yii::t('standard', '_MANUAL'),
            ), array(
                'separator' => '',
            )); ?>
            <?= CHtml::activeTextField($model, 'manualSeparator', array('id' => 'manual_separator')); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6">
            <?= CHtml::activeCheckBox($model, 'firstRowAsHeader'); ?>
            <?= CHtml::activeLabel($model, 'firstRowAsHeader'); ?>
        </div>
        <div class="span6">
            <?= CHtml::activeCheckBox($model, 'insertUpdate'); ?>
            <?= CHtml::activeLabel($model, 'insertUpdate'); ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6 file-charset">
            <?= CHtml::activeLabel($model, 'charset'); ?>
            <?= CHtml::activeDropDownList($model, 'charset', UserImportForm::getDropDownCharsets()); ?>
        </div>
        <div class="span6 send-log-to-mail">
            <?= CHtml::label(Yii::t('automation', 'Email address for error log'), 'sendLogToMail'); ?>
            <?= CHtml::textField('OrgChartImportForm[sendLogToMail]'); ?>
        </div>
    </div>

</div>

<script type="text/javascript">
    (function ($) {
        $(document).controls();
        $(function () {
            $('input,select').styler();

            // Handle IMPORT SCHEMA INPUT
            $(".import-schema-input").on("click", function(){
                $(".rule-wizard-dialog").hide();
            });
            $(document).on("dialog2.closed", '.importSchema', function(){
                $(".rule-wizard-dialog").show();
                $('.modal-backdrop:first-of-type').remove();
            })

        });
    })(jQuery);
</script>