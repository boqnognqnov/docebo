<?php

class ImportUsersFromCSV extends AutomationActionHandler {

    const MAX_USERS_PER_RUN = 300;
    /**
     * Returns HTML code to display as custom form settings
     */
    public function renderConditionParamsForm($params = array()) {
        Yii::app()->clientScript->registerCss('import-users-from-csv', '
            div.import-users-rule-settings.import p.params-title{
                margin-left: 3px;
            }
            div.import-users-rule-settings.import label{
                margin-bottom: 2px;
                margin-left: 2px;
            }
            div.import-users-rule-settings.import .org-chart > label{
                margin-left: 0px;
            }
            div.import-users-rule-settings.import label.select-separator{
                margin-left: 3px;
            }
            div.import-users-rule-settings.import div.csv-row-separators > span span{
                margin-left: 15px
            }
            div.import-users-rule-settings.import div.row-fluid{
                margin-top: 15px;
            }
            .import .row-fluid label, .import .row-fluid span, .import .row-border > div, .import .row-fluid input {
                display: inline-block;
                vertical-align: middle;
            }
            div.import-users-rule-settings.import a.import-schema-input{
                width: 110px;
            }
            div.import-users-rule-settings.import div.csv-row-separators{
                margin-top: 10px;
            }
            div.import-users-rule-settings.import div.csv-row-separators input#manual_separator{
                width: 100px;
            }
            div.import-users-rule-settings.import div.csv-row-separators > span label{
                margin-right: 10px;
            }
            div.import-users-rule-settings.import .span6.file-charset select, div.import-users-rule-settings.import .span6.org-chart select{
                width: 100% !important;
            }
            div.import-users-rule-settings.import .send-log-to-mail input{
                width: 95%;
            }
            div.import-users-rule-settings.import div.csv-row-separators > span > span:first-of-type,
            div.import-users-rule-settings.import span#require-password-change-input > span:first-of-type{
                margin-left: 0px;
                padding-left: 0px;
            }
            div.import-users-rule-settings.import .require-password-change label{
                margin-bottom: 10px;
            }
            div.import-users-rule-settings.import .branch-options label{
                margin-bottom: 10px;
            }
            div.import-users-rule-settings.import #require-password-change-input label{
                padding-top:8px;
            }
            div.import-users-rule-settings.import span[id*="ignorePasswordChange"] {
                float: left;
            }
            
		');

        $model = new UserImportForm('step_one');
        $rootNode = CoreOrgChartTree::getOrgRootNode();
        $model->node = $rootNode->idOrg;

        Yii::app()->getController()->renderPartial('AutomationApp.components.actions.views.import_users_from_csv', array(
            'model' => $model
        ), false, true);

    }

    /**
     * Your implementation should take the array of params, unique to
     * the handler and return a localized message, describing to the user
     * when exactly this action does, when runned.
     * For example if the implementation of the condition handler represents
     * "Set user to {status}" and the $params
     * array contains: array('status'=>'inactive'),
     * the implementation of the method below may for example return a localized
     * message along the lines of "Set user status to Inactive",
     * replacing the dynamic parts of the message depending
     * on the values in $params.
     *
     * @param array $params
     *
     * @return mixed
     */
    public function describeActionByParams ( $params = array() ) {
        return $this->getShortDescription();
    }

    /**
     * Abstract method called when this action must be run
     *
     * @param \AutomationAppParams $paramsAdapter
     * @return bool
     */
    public function run ( AutomationAppParams $paramsAdapter ) {
        $ftpConfig = $paramsAdapter->others[0];
        $form = $this->params;

        // if we have a hydra theme active, send the config to the hydra bgjob endpoint via CURL
        if (Yii::app()->legacyWrapper->hydraFrontendEnabled()) {
            $data = $form['UserImportForm'];
            $data['name'] = $paramsAdapter->rule_name;
            $data['id_author'] = $paramsAdapter->id_author;
            $data['filename'] = $this->uploadFileToS3($ftpConfig);
            BackgroundJobBridge::importUsersFromCSV($data);
        } else {
            $params = ['isFirst' => true, 'ftp' => $ftpConfig, 'form' => $form];
            $name = 'AutomationAppUserImport';
            $handler = AutomationAppUserImport::id();
            // Let's create a immidiate job that will
            $job = Yii::app()->scheduler->createImmediateJob($name, $handler, $params, false, null, true);
        }
    }

    /**
     * Get the file from FTP and save it to amazon.
     * @param array $ftpData
     * @throws Exception
     * @return string
     */
    private function uploadFileToS3($ftpData)
    {
        // Define where the file should be saved locally
        $tempFilename = Docebo::randomHash() . '.csv';
        $localFile = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $tempFilename;
        $remoteFile = isset($ftpData['file']) ? $ftpData['file'] : false;

        // Init the ftp manager and add params
        $ftp = Yii::app()->ftp;
        $ftp->host = isset($ftpData['host']) ? $ftpData['host'] : null;
        $ftp->port = isset($ftpData['port']) ? $ftpData['port'] : null;
        $ftp->username = isset($ftpData['username']) ? $ftpData['username'] : null;
        $ftp->password = isset($ftpData['password']) ? $ftpData['password'] : null;
        $ftp->ssh = isset($ftpData['protocol']) ? ($ftpData['protocol'] == 'sftp') : null;

        if (!$ftp->connect()) {
            throw new Exception("Can't connect to the server:" . $ftp->host);
        }
        // move to the configured folder
        if (isset($ftpData['folder'])) {
            $ftp->chdir($ftpData['folder']);
        }
        // get the file and store it locally so we can upload it to s3
        $ftp->getFile($remoteFile, $localFile);

        if (!file_exists($localFile)) {
            throw new Exception("Can't download the file from the server");
        }
        // upload file to s3
        $storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_ASSETS);
        $storage->store($localFile);
        // remove local file
        unlink($localFile);
        // return the public file url, so the background job can download it
        return $storage->fileUrl($tempFilename);
    }

    /**
     * Return a localized description that describes
     * what this action does (in a few words, usually displayed
     * in dropdowns or other UI areas where actions are listed)
     * @return string
     */
    public function getShortDescription () {
        return Yii::t( 'automation', 'Import Users From CSV' );
    }
}