<?php

class AutomationRuleJobHandler extends JobHandler implements IJobHandler {

	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'plugin.AutomationApp.components.AutomationRuleJobHandler';

	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		$job = $this->job;
		$jobParams = json_decode($job->params, true);

		// Try to load the automation rule
		if (!isset($jobParams['id_rule']) || !($rule = AutomationRule::model()->findByPk($jobParams['id_rule'])))
			throw new Exception('Automation rules job handler called with invalid rule ID');

		// Call trigger method and let the rule decide whether or not it should be run
		/* @var $rule AutomationRule */
		$rule->trigger($jobParams);
	}

	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}
}



