<?php

/**
 * Class AutomationConditionHandler
 *
 * Abstract class to be implemented by all condition types
 */
abstract class AutomationConditionHandler extends CComponent
{
	/* === Allowed trigger types === */
	const AUTOMATION_CONDITION_TRIGGER_SCHEDULED = 'scheduled';
	const AUTOMATION_CONDITION_TRIGGER_REALTIME = 'realtime';

	/**
	 * Condition params
	 * @var array
	 */
	protected $params;

	/**
	 * Allows this condition handler to populate the adapter
	 * with data (users, courses, etc), that satisfy the condition
	 * @var AutomationAppParams
	 */
	protected $adapter = null;

	/**
	 * Called to pass initialization params to this condition handler
	 * @param array $initParams
	 */
	public function init($initParams = array()) {
		$this->params = $initParams;
	}

	/**
	 * Returns an array of allowed trigger types.
	 * Default implementation allows both
	 *
	 * @return array
	 */
	public function allowedTriggerTypes() {
		return array(self::AUTOMATION_CONDITION_TRIGGER_REALTIME, self::AUTOMATION_CONDITION_TRIGGER_SCHEDULED);
	}

	/**
	 * Only used if this condition handler supports "realtime" trigger type
	 * A list of Yii events which will trigger the automation rule, to which this
	 * rule condition is "attached"
	 *
	 * @return array
	 */
	public function getTriggeringEvents(){
		return array();
	}

	/**
	 * Override this method and render a partial view that has
	 * inputs, checkboxes, or whatever you want that should serve
	 * as controllable settings for your condition/rule. Don't wrap them
	 * in a <form>. These inputs will then be serialized as JSON
	 * and stored as condition's params, so the condition handler
	 * can use them to make a reasonable decision if "it was met".
	 */
	abstract public function renderConditionParamsForm($params = array());

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public abstract function verify($realtimeEventName = false, $realtimeEventParams = array());

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public abstract function getShortDescription();

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly is this condition satisfied.
	 * For example if the implementation of the condition handler represents
	 * "User has been inactive for X [days/months]" and the $params
	 * array contains: array('days'=>'60', 'operator'=>'>'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "User has been inactive for more than 60 days",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public abstract function describeConditionByParams($params = array());

	public function setAdapter(AutomationAppParams &$adapter){
		$this->adapter = $adapter;
	}
}