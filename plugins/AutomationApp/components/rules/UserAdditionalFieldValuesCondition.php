<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 19.11.2015 г.
 * Time: 13:38 ч.
 */
class UserAdditionalFieldValuesCondition extends AutomationConditionHandler {

	/**
	 * Returns HTML code to display as custom form settings
	 */
	public function renderConditionParamsForm ($params = array()) {
		Yii::app()->clientScript->registerCss( 'user-additional-fields-rule', '
		.user-additional-fields-rule .operator{
			margin-top: 10px;
		}
		.step-recipes .user-additional-fields-rule select{
			width: 95%;
		}
		.step-recipes .user-additional-fields-rule input{
			width: 97%;
			padding-left: 1.5%;
			padding-right: 1.5%;
		}
		' );
		
		Yii::app()->clientScript->registerScriptFile(Yii::app()->assetManager->publish(Yii::getPathOfAlias( 'AutomationApp.components.rules.assets')) . '/user_additional_field_value_rule.js');

		$criteria = new CDbCriteria();
		$criteria->addInCondition( 't.type', array(
			// For now only Text Field and Dropdown additional fields are supported
			// For the rest we need to implement their own "tiny settings forms"
			CoreUserField::TYPE_DROPDOWN,
			CoreUserField::TYPE_TEXT,
		) );
		$criteria->index  = 'id_field';
		$additionalFields = CoreUserField::model()->language()->findAll($criteria);

		$fields = array();
        foreach ($additionalFields as $fieldId => $field) {
            /** @var CoreUserField $field */
            $fields[$fieldId] = $field->getTranslation();
		}

		Yii::app()->getController()->renderPartial( 'AutomationApp.components.rules.views.user_additional_field_value_rule', array(
			'fields'      => $fields,
			'fieldModels' => $additionalFields,
		), FALSE, TRUE );
	}

	public function allowedTriggerTypes () {
		return array( self::AUTOMATION_CONDITION_TRIGGER_SCHEDULED );
	}

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ( $realtimeEventName = FALSE, $realtimeEventParams = array() ) {
		$params       = $this->params;
		$fieldEnabled = $params['additional-fields-enabled'];

		$fieldIdsToFilterBy = array();
		foreach ( $fieldEnabled as $fieldId => $enabled ) {
			if( $enabled ) {
				$fieldIdsToFilterBy[] = $fieldId;
			}
		}

		$criteria = new CDbCriteria();
		$criteria->addInCondition( 'id_field', $fieldIdsToFilterBy );
		$criteria->index = 'id_field';
		$fields = CoreUserField::model()->language()->findAll( $criteria );

		$query = Yii::app()->getDb()->createCommand()
					->selectDistinct( 'u.idst' )
					->from( 'core_user u' );

		$tmpFieldConditions = NULL;
		$queryParams        = array();

		foreach ( $fields as $fieldId => $field ) {
			/* @var $field CoreUserField */

			$fieldValueRaw = isset( $params['additional-field-filter'][ $fieldId ]['value'] ) ? $params['additional-field-filter'][ $fieldId ]['value'] : FALSE;

			if( $fieldValueRaw === FALSE ) {
				continue;
			}

			switch ( $field->getFieldType() ) {
				case CoreUserField::TYPE_DROPDOWN:
				case CoreUserField::TYPE_TEXT:
					$tmpFieldConditions[]             = "(`field_{$fieldId}` = :value_{$fieldId})";
					$queryParams[":value_{$fieldId}"] = $fieldValueRaw;
					break;
				default:
					$query->andWhere( '1=0' ); // prevent any results
					break;
			}
		}

		if( ! empty( $tmpFieldConditions ) ) {
			// A small trick (using HAVING) to get only user IDs
			// that match ALL the selected additional fields and not just ANY)
			$query->join('(SELECT id_user, COUNT(id_user) cntMatchingAdditionalFields FROM `' . CoreUserFieldValue::model()->tableName() . '`
				WHERE '.implode( ' OR ', $tmpFieldConditions ).'
				GROUP BY `id_user`
				HAVING cntMatchingAdditionalFields=:cntFields
				) ue', 'ue.id_user=u.idst', array(
					':cntFields'=>count($tmpFieldConditions)
				));
//			$query->join('(SELECT id_user, COUNT(id_user) cntMatchingAdditionalFields FROM `core_field_userentry`
//				WHERE '.implode( ' OR ', $tmpFieldConditions ).'
//				GROUP BY id_user
//				HAVING cntMatchingAdditionalFields=:cntFields
//				) ue', 'ue.id_user=u.idst', array(
//					':cntFields'=>count($tmpFieldConditions)
//				));
		}

		$userIdsMatchingCondition = $query->queryColumn( $queryParams );

		if( ! empty( $userIdsMatchingCondition ) ) {
			$this->adapter->users = array_merge( $this->adapter->users, $userIdsMatchingCondition );

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly is this condition satisfied.
	 * For example if the implementation of the condition handler represents
	 * "User has been inactive for X [days/months]" and the $params
	 * array contains: array('days'=>'60', 'operator'=>'>'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "User has been inactive for more than 60 days",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		$params = empty( $params ) ? $this->params : $params;

		$fieldIdsToFilterBy = array();
		if( isset( $params['additional-fields-enabled'] ) ) {
			foreach ( $params['additional-fields-enabled'] as $idCommon => $enabled ) {
				if( $enabled ) {
					$fieldIdsToFilterBy[] = $idCommon;
				}
			}
		}

		$criteria = new CDbCriteria();
        $criteria->index = 'id_field';
		$criteria->addInCondition( CoreUserField::model()->getTableAlias() . '.id_field', $fieldIdsToFilterBy );
		$fields = CoreUserField::model()->language()->findAll( $criteria );

		// @TODO iterate through all the enabled fields (where flag is 1)
		// check the configured possible value for that individual field
		// and retrieve all users having that value for the additional field
		// Multiple additional field filters will behave with an AND operator
		// meaning that a given user must match ALL defined additional field
		// filters for the whole condition to be considered as met

		if( count( $fields ) > 1 ) {
			// Multiple additional field values for filtering (they behave as AND operator)
			$fieldTranslations = array();
			foreach ( $fields as $fieldId => $field ) {
                /** @var CoreUserField $field */
				$fieldValue = $params['additional-field-filter'][ $fieldId ]['value'];

				switch ( $field->getFieldType() ) {
					case CoreUserField::TYPE_DROPDOWN:
                        $fieldValue = CoreUserFieldDropdownTranslations::getLabelForLanguage(null, null, $fieldId, $fieldValue)->queryRow();
                        $fieldValue = $fieldValue['translation'];
                        break;
                }

                $translation = $field->getTranslation();
                $fieldTranslations[] = '"' . htmlspecialchars( $translation ) . '"' . ' is "' . htmlspecialchars($fieldValue) . '"';
			}

			return Yii::t( 'automation', 'Additional fields: {fields}', array(
				'{fields}' => implode( '; ', $fieldTranslations )
			) );
		} elseif( count( $fields ) == 1 ) {
            /** @var CoreUserField $field */
			$field = reset( $fields );

			$idCommon = $field->getFieldId();

			$fieldValue = Yii::app()->htmlpurifier->purify( $params['additional-field-filter'][ $idCommon ]['value'] );

			switch ( $field->getFieldType() ) {
				case CoreUserField::TYPE_DROPDOWN:
					$fieldValue = CoreUserFieldDropdownTranslations::getLabelForLanguage(null, null, $idCommon, $fieldValue)->queryRow();
                    $fieldValue = $fieldValue['translation'];
					break;
			}

			$translation = $field->getTranslation();
			return Yii::t( 'automation', 'Additional field "{field_name}" has value "{field_value}"', array( '{field_name}'  => htmlspecialchars($translation),
																											 '{field_value}' => htmlspecialchars($fieldValue)
			) );
		} else {
			return 'Invalid field...';
		}
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t( 'automation', 'User additional field value is' );
	}
}