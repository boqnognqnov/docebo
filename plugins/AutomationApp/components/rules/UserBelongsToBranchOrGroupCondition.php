<?php
/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 27.11.2015 г.
 * Time: 13:39 ч.
 */

class UserBelongsToBranchOrGroupCondition extends AutomationConditionHandler{

	public function allowedTriggerTypes () {
		return array(
//					self::AUTOMATION_CONDITION_TRIGGER_REALTIME,
					self::AUTOMATION_CONDITION_TRIGGER_SCHEDULED
				);
	}

	public function getTriggeringEvents () {
		return array(
//				'onAfterLogin' /* @see DoceboWebUser::login() */
		);
	}


	/**
	 * Override this method and render a partial view that has
	 * inputs, checkboxes, or whatever you want that should serve
	 * as controllable settings for your condition/rule. Don't wrap them
	 * in a <form>. These inputs will then be serialized as JSON
	 * and stored as condition's params, so the condition handler
	 * can use them to make a reasonable decision if "it was met".
	 */
	public function renderConditionParamsForm ($params = array()) {
		// TODO: Implement renderConditionParamsForm() method.
		Yii::app()->controller->renderPartial(
				'AutomationApp.components.rules.views._selectBranchOrGroup',
				array(),
				false,
				true
		);
	}

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ( $realtimeEventName = FALSE, $realtimeEventParams = array() ) {
		$rule_groups = array();
		$rule_branches = array();

		if(isset($this->params['rule_groups'])) {
			$rule_groups = explode(',', $this->params['rule_groups']);
		}

		if(isset($this->params['rule_branches'])) {
			$rule_branches = CJSON::decode(trim($this->params['rule_branches']));
		}

		$userIds = array();
		$coreGroupMembersModel = new CoreGroupMembers();

		// Get branches users
		if(is_array($rule_branches) && count($rule_branches) > 0) {
			foreach($rule_branches as $rule_branch => $state) {
				// Select all branch members depending on the branches states !!!
				$rule_branch_users = $coreGroupMembersModel->getBranchUserIds($rule_branch, (($state == 2) ? true : false));

				if(is_array($rule_branch_users) && count($rule_branch_users) > 0) {
					foreach($rule_branch_users as $userRecord) {
						if(!empty($userRecord['idst'])) {
							$userIds[$userRecord['idst']] = $userRecord['idst'];
						}
					}
				}
			}
		}

		// Get groups users
		if(is_array($rule_groups) && count($rule_groups) > 0) {
			$command = Yii::app()->db->createCommand();
			$command->selectDistinct('user.idst idUser');
			$command->from('core_group_members gm');
			$command->join('core_user user','gm.idstMember=user.idst');
			$command->where(array('IN', 'gm.idst', $rule_groups));

			$rows = $command->queryAll();

			foreach ($rows as $row) {
				$userIds[$row['idUser']] = (int) $row['idUser'];
			}
		}

		// Apply the users selection here
		if(is_array($userIds) && count($userIds) > 0) {
			$this->adapter->users = array_merge($this->adapter->users, $userIds);
			return true;
		}

		return false;
	}

	/**
	 * Describes when is this condition satisfied
	 * @param array $params
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		$count = !empty($params['groupsBranchesCounterInput']) ? $params['groupsBranchesCounterInput'] : 0;
		return Yii::t('automation', 'User belongs to {count} groups and branches', array('{count}' => $count));
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t('automation', 'User belongs to group or branch');
	}
}