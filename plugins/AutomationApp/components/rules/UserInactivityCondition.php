<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 11.11.2015 г.
 * Time: 18:47 ч.
 */
class UserInactivityCondition extends AutomationConditionHandler {

	/**
	 * Returns HTML code to display as custom form settings
	 */
	public function renderConditionParamsForm ($params = array()) {
		Yii::app()->getController()->renderPartial( 'AutomationApp.components.rules.views.user_inactivity_rule', array(), FALSE, TRUE );
	}

	public function allowedTriggerTypes () {
		return array(self::AUTOMATION_CONDITION_TRIGGER_SCHEDULED);
	}

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly is this condition satisfied.
	 * For example if the implementation of the condition handler represents
	 * "User has been inactive for X [days/months]" and the $params
	 * array contains: array('days'=>'60', 'operator'=>'>'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "User has been inactive for more than 60 days",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		if( ( ! $params || empty( $params ) ) && ! empty( $this->params ) ) {
			// Take params from the current instance, if set
			$params = $this->params;
		}
		$operator = $params['inactivity_operator'];
		$number   = $params['inactivity_number'];
		$units    = $params['inactivity_unit'];

		$comparisonLocalized = NULL;
		switch ( $operator ) {
			case '>':
				$comparisonLocalized = Yii::t( 'automation', 'more than' );
				break;
			case '<':
				$comparisonLocalized = Yii::t( 'automation', 'less than' );
		}
		$unit = Yii::t( 'standard', 'days' );
		switch ( $units ) {
			case 'day':
				break;
			case 'week':
				$unit = Yii::t( 'standard', 'weeks' );
				break;
			case 'month':
				$unit = Yii::t( 'standard', 'months' );
				break;
		}

		return Yii::t( 'automation', 'User has been inactive for {comparison} {value} {unit}', array(
			'{comparison}' => $comparisonLocalized,
			'{value}'      => $number,
			'{unit}'       => $unit
		) );
	}

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ($realtimeEventName = false, $realtimeEventParams = array()) {
		$operator = isset( $this->params['inactivity_operator'] ) ? $this->params['inactivity_operator'] : FALSE;
		$number   = isset( $this->params['inactivity_number'] ) ? intval( $this->params['inactivity_number'] ) : FALSE;
		$units    = isset( $this->params['inactivity_unit'] ) ? $this->params['inactivity_unit'] : FALSE;

		$adapter = &$this->adapter;
		/* @var $adapter AutomationAppParams */

		$query = Yii::app()->getDb()->createCommand()
					->select( 'idst' )
					->from( 'core_user' );

		switch ( $units ) {
			case 'day':
				$unit = 'DAY';
				break;
			case 'week':
				$unit = 'WEEK';
				break;
			case 'month':
				$unit = 'MONTH';
				break;
		}

		// Reversing the actual query operators because
		// NOT LOGGED IN SINCE X DAYS means get users with `lastenter`
		// SMALLER than (today minus X days), not GREATER THAN
		if( $operator == '>' ) {
			$query->andWhere( '(lastenter <> \'0000-00-00 00:00:00\' AND lastenter < DATE_SUB(NOW(), INTERVAL ' . $number . ' ' . $unit . ')) OR (lastenter IS NULL AND register_date < DATE_SUB(NOW(), INTERVAL ' . $number . ' ' . $unit . '))' );
		} elseif( $operator == '<' ) {
			$query->andWhere( '(lastenter <> \'0000-00-00 00:00:00\' AND lastenter > DATE_SUB(NOW(), INTERVAL ' . $number . ' ' . $unit . ')) OR (lastenter IS NULL AND register_date < DATE_SUB(NOW(), INTERVAL ' . $number . ' ' . $unit . '))' );
		}

		$userIds = $query->queryColumn();

		if( ! empty( $userIds ) ) {
			$adapter->users = array_merge( $adapter->users, $userIds );

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t( 'automation', 'User has been inactive for' );
	}
}