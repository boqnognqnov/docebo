<?php
/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 27.11.2015 г.
 * Time: 13:39 ч.
 */

class UserLoggedInCondition extends AutomationConditionHandler{

	public function allowedTriggerTypes () {
		return array(self::AUTOMATION_CONDITION_TRIGGER_REALTIME);
	}

	public function getTriggeringEvents () {
		return array(
			'onAfterLogin' /* @see DoceboWebUser::login() */
		);
	}


	/**
	 * Override this method and render a partial view that has
	 * inputs, checkboxes, or whatever you want that should serve
	 * as controllable settings for your condition/rule. Don't wrap them
	 * in a <form>. These inputs will then be serialized as JSON
	 * and stored as condition's params, so the condition handler
	 * can use them to make a reasonable decision if "it was met".
	 */
	public function renderConditionParamsForm ($params = array()) {
		// TODO: Implement renderConditionParamsForm() method.
	}

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ( $realtimeEventName = FALSE, $realtimeEventParams = array() ) {
		switch($realtimeEventName){
			case 'onAfterLogin':
				if(isset($realtimeEventParams['user']['idst'])){
					$idst = $realtimeEventParams['user']['idst'];
					$this->adapter->users = array($idst);
					return true;
				}

				break;
		}
		return false;
	}

	/**
	 * Describes when is this condition satisfied
	 * @param array $params
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		return Yii::t('automation', 'User logged in');
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t('automation', 'User logged in');
	}
}