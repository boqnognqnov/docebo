$(document).on("click", ".remove-additional-field-filter", function(){
	$(this).closest(".additional-field-filter").hide().find("input[type=hidden]").val(0);
});

$(document).on("change", "select#user_additional_field_id_common", function(){
	var idCommonSelected = $(this).val();

	// Show the container with the field-specific settings
	// and set the flag for this field to "1" so the filtering/checking
	// logic knows to take into account this field\'s filters as well
	$(".additional-field-filter.field-"+idCommonSelected).show().find('[name="additional-fields-enabled['+idCommonSelected+']"]').val(1);

	// Reset the dropdown to non-selected state
	$(this).find("option").removeAttr("selected");
	$(this).find("option").first().attr("selected", "selected");
});

$(document).on("afterRecipeSettingsRestore", ".recipes-settings", function(event, params){
	// After clicking the edit button and the condition and its settings for loaded (after ajax)
	$(".additional-fields-enabled").each(function(){
		var id = $(this).attr('id').replace('additional-fields-enabled_', '');
		if(typeof params['additional-fields-enabled'] !== 'undefined' && typeof params['additional-fields-enabled'][id] !== 'undefined' && params['additional-fields-enabled'][id] > 0 && $(this).val() <= 0) {
			$(this).val(1);
		}
		if($(this).val() > 0){
			$(this).closest('.additional-field-filter').show();
		}
	});
});