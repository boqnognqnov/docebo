<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 27.11.2015 г.
 * Time: 18:30 ч.
 */
class UserExpirationDateCondition extends AutomationConditionHandler {

	const DATE_HAS_VALUE = 'has_value';
	const DATE_FUTURE = 'future';
	const DATE_PAST = 'past';

	/**
	 * Override this method and render a partial view that has
	 * inputs, checkboxes, or whatever you want that should serve
	 * as controllable settings for your condition/rule. Don't wrap them
	 * in a <form>. These inputs will then be serialized as JSON
	 * and stored as condition's params, so the condition handler
	 * can use them to make a reasonable decision if "it was met".
	 */
	public function renderConditionParamsForm ($params = array()) {
		Yii::app()->controller->renderPartial( 'AutomationApp.components.rules.views.user_expiration_date_condition', array() );
	}

	public function getTriggeringEvents () {
		// This condition supports both scheduled or realtime triggering
		// (when used in combination with another conditions, because this one
		// doesn't listen for any events on its own. A good companion is the
		// "UserLoggedInCondition" condition, which causes the automation
		// rule to be triggered when a user logs in

		// This is why an empty array is returned
		return parent::getTriggeringEvents();
	}


	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ( $realtimeEventName = FALSE, $realtimeEventParams = array() ) {
		$filter = $this->params['user_expiration_date_condition'];

		$query = Yii::app()->getDb()->createCommand()
					->select( 'idst' )
					->from( 'core_user' );

		switch ( $filter ) {
			case self::DATE_HAS_VALUE:
				$query->andWhere('expiration IS NOT NULL AND expiration <> "0000-00-00"');
				break;
			case self::DATE_FUTURE:
				// Expiration date means the user will expire at the END OF THE DAY
				// of that date, so include also users that expire at the end of TODAY
				// since this is also FUTURE (relative to [now])
				$query->andWhere('expiration IS NOT NULL AND expiration >= DATE(NOW())');
				break;
			case self::DATE_PAST:
				// Expiration date means the user will expire at the END OF THE DAY
				// of that date, so include only users who have expired up until yesterday midnight
				$query->andWhere('expiration IS NOT NULL AND expiration < DATE(NOW())');
				break;
			default:
				// WHAT?
				$query->andWhere('1=0');
				break;
		}

		$userIds = $query->queryColumn();

		if( ! empty( $userIds ) ) {
			$this->adapter->users = array_merge( $this->adapter->users, $userIds );

			return TRUE;
		}

		return FALSE;
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t( 'automation', 'User expiration date' );
	}

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly is this condition satisfied.
	 * For example if the implementation of the condition handler represents
	 * "User has been inactive for X [days/months]" and the $params
	 * array contains: array('days'=>'60', 'operator'=>'>'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "User has been inactive for more than 60 days",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		if(empty($params)){
			$params = $this->params;
		}

		$filter = $params['user_expiration_date_condition'];

		switch ( $filter ) {
			case self::DATE_HAS_VALUE:
				return Yii::t('automation', 'User expiration date is not empty');
				break;
			case self::DATE_FUTURE:
				return Yii::t('automation', 'User expiration date is in the future');
				break;
			case self::DATE_PAST:
				return Yii::t('automation', 'User expiration date is in the past');
				break;
		}
	}
}