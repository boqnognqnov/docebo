<?php

class AdministratorUploadedFileInFtp extends AutomationConditionHandler {

	CONST FTP = "ftp";
	CONST SFTP = "sftp";

	/**
	 * Returns HTML code to display as custom form settings
	 */
	public function renderConditionParamsForm ($params = array()) {
		Yii::app()->clientScript->registerCss( 'user-additional-fields-rule', '
		.admin-uploaded-file-rule-settings .params-title{
			margin-left: 2px;
		}
		.admin-uploaded-file-rule-settings label{
			margin-bottom: 2px;
			margin-left: 2px;
		}

		.admin-uploaded-file-rule-settings label.ftp-port-title, .admin-uploaded-file-rule-settings label.ftp-password-title{
			margin-left: 0px;
		}
		.admin-uploaded-file-rule-settings .row-fluid{
			margin-top: 10px;
		}
		.admin-uploaded-file-rule-settings .ftp-host{
			width: 69%;
		}
		.admin-uploaded-file-rule-settings .ftp-port{
			margin-left: 35px;
		}
		.admin-uploaded-file-rule-settings .ftp-username,.admin-uploaded-file-rule-settings .ftp-password{
			width: 48%;
		}
		.admin-uploaded-file-rule-settings .ftp-username input,.admin-uploaded-file-rule-settings .ftp-password input{
			width: 94%;
		}
		.admin-uploaded-file-rule-settings .ftp-folder input,.admin-uploaded-file-rule-settings .ftp-file input{
			width: 97%;
		}
		span#protocol > span{
			margin-left: 0px;
			float: left;
		}
		label[for="protocol"]{
			margin-bottom: 10px;
		}
		span#protocol > label{
			float: left;
			margin-right:30px;
			margin-left:5px;
		}
		' );

		Yii::app()->getController()->renderPartial( 'AutomationApp.components.rules.views.administrator_uploaded_file_in_ftp', array(), FALSE, TRUE );
	}

	public function allowedTriggerTypes () {
		return array(self::AUTOMATION_CONDITION_TRIGGER_SCHEDULED);
	}

	/**
	 * Your implementation should take the array of params, unique to
	 * the handler and return a localized message, describing to the user
	 * when exactly is this condition satisfied.
	 * For example if the implementation of the condition handler represents
	 * "User has been inactive for X [days/months]" and the $params
	 * array contains: array('days'=>'60', 'operator'=>'>'),
	 * the implementation of the method below may for example return a localized
	 * message along the lines of "User has been inactive for more than 60 days",
	 * replacing the dynamic parts of the message depending
	 * on the values in $params.
	 *
	 * @param array $params
	 *
	 * @return mixed
	 */
	public function describeConditionByParams ( $params = array() ) {
		return $this->getShortDescription();
	}

	/**
	 * Abstract method called when this condition must be verified
	 * Implementations of this method should populate the adapter
	 * with items that satisfy its condition
	 * @see $payloadAdapter
	 *
	 * @param bool  $realtimeEventName
	 * @param array $realtimeEventParams
	 *
	 * @return bool
	 */
	public function verify ($realtimeEventName = false, $realtimeEventParams = array()) {

		// Retrieve all the credentials that we need to
		$host 		= isset( $this->params['host'] ) 		? $this->params['host'] 		: FALSE;
		$port   	= isset( $this->params['port'] ) 		? $this->params['port'] 		: FALSE;
		$username   = isset( $this->params['username'] ) 	? $this->params['username'] 	: FALSE;
		$password   = isset( $this->params['password'] ) 	? $this->params['password'] 	: FALSE;
		$folder    	= isset( $this->params['folder'] ) 		? $this->params['folder'] 		: FALSE;
		$file    	= isset( $this->params['file'] ) 		? $this->params['file'] 		: FALSE;
		$protocol	= isset( $this->params['protocol'] ) 	? $this->params['protocol'] 	: FALSE;

		$this->adapter->others['ftp'] = $this->params;


		return true;
//		return FALSE;
	}

	/**
	 * Return a localized description that describes
	 * what this condition does (in a few words, usually displayed
	 * in dropdowns or other UI areas where conditions are listed)
	 * @return string
	 */
	public function getShortDescription () {
		return Yii::t( 'automation', 'Administrator uploaded file in a FTP folder' );
	}
}