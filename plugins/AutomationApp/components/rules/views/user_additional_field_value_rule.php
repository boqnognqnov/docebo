<?php
/* @var $fields array */
/* @var CoreUserField $field */
/* @var $fieldModels array<CoreUserField> */
?>

<div class="user-additional-fields-rule">

	<p>
		<?=Yii::t('automation', 'Select an additional field to filter by:')?>
	</p>
	<div>
		<?= CHtml::dropDownList( 'user_additional_field_id_common', NULL, array( 0 => 'Select...' ) + $fields, array(
			'style' => 'width:100%'
		) ) ?>
	</div>

	<br/>

	<? foreach ( $fieldModels as $fieldId => $field ):
		// Render a tiny per-additional-field settings form,
		// inside the tiny rule-condition-level settings form
		// and hide it until we explicitly select it from the dropdown
		// of possible Additional fields (which means the user wants to
		// configure/filter this condition by the possible value of this additional field)
		?>
		<div style="display:none" class="additional-field-filter field-<?= $fieldId ?>">

			<?
			// A hidden "flag" is rendered with each additional field's tyny
			// settings form. This flag is put to 1 or 0 depending on if this specific
			// additional field is configured for filtering by its value by the user.
			// The filter checker logic should only consider filters for additional
			// fields that have this flag set to "1".
			?>
			<?=CHtml::hiddenField('additional-fields-enabled[' . $fieldId . ']', 0, array('class'=>'additional-fields-enabled'))?>

			<div class="row-fluid">
				<div class="span3">
					<label><?= ($field->translation) ? htmlspecialchars($field->translation) : htmlspecialchars($field->default_translation) ?> <?=Yii::t('group_management', 'is')?>:</label>
				</div>
				<div class="span8">
					<? switch ( $field->getFieldType() ) {
						case CoreUserField::TYPE_TEXT:
							echo CHtml::textField( 'additional-field-filter[' . $fieldId . '][value]', NULL );
							break;
						case CoreUserField::TYPE_DROPDOWN:
							$languageCode 			  = Lang::getCodeByBrowserCode( Yii::app()->getLanguage() );
							$dropdown 	  			  = new FieldDropdown;
							$dropdown->id_field 	  = $fieldId;
							$possibleDropdownElements = array_map(function($label) { return htmlspecialchars($label); }, $dropdown->getOptions());

							echo CHtml::dropDownList( 'additional-field-filter[' . $fieldId . '][value]', NULL, $possibleDropdownElements, array(
								'style' => 'width:100%'
							) );
							break;
						// @TODO add support for more types of fields
					} ?>
				</div>
				<div class="span1">
					<a class="remove-additional-field-filter" href="#">
						<i class="i-sprite is-remove red"></i>
					</a>
				</div>
			</div>
			<br/>
		</div>
	<? endforeach; ?>
</div>