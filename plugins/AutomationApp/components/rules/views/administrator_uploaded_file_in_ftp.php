<div class="admin-uploaded-file-rule-settings">
    <p class="params-title"><?= Yii::t( 'standard', 'Params' ) ?>:</p>


    <div class="row-fluid">
        <div class="span8 ftp-host">
            <?php
                echo CHtml::label(Yii::t('standard', 'Host'), 'host');
                echo CHtml::textField("host");
            ?>
        </div>
        <div class="span3 ftp-port">
            <?php
                echo CHtml::label(Yii::t('standard', 'Port'), 'port', array('class' => 'ftp-port-title'));
                echo CHtml::textField("port");
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span5 ftp-username">
            <?php
                echo CHtml::label(Yii::t('standard', '_USERNAME'), 'username');
                echo CHtml::textField("username");
            ?>
        </div>
        <div class="span5 ftp-password pull-right">
            <?php
                echo CHtml::label(Yii::t('standard', '_PASSWORD'), 'password', array('class' => 'ftp-password-title'));
                echo CHtml::passwordField("password");
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 ftp-folder">
            <?php
                echo CHtml::label(Yii::t('standard', 'Folder'), 'folder');
                echo CHtml::textField("folder");
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 ftp-file">
            <?php
                echo CHtml::label(Yii::t('standard', '_FILENAME'), 'file');
                echo CHtml::textField("file");
            ?>
        </div>
    </div>

    <div class="row-fluid">
        <?php
            $ftpProtocols = array(
                AdministratorUploadedFileInFtp::FTP => Yii::t('standard', 'FTP'),
                AdministratorUploadedFileInFtp::SFTP => Yii::t('standard', 'SFTP'),
            );
            echo CHtml::label(Yii::t('standard', 'Protocol'), 'protocol');
            echo CHtml::radioButtonList('protocol', false, $ftpProtocols, array(
                'separator' => '',
            ));
        ?>
    </div>

</div>

<script type="text/javascript">
    (function ($) {
        $(function () {
            $('input,select').styler();
        });
    })(jQuery);
</script>
