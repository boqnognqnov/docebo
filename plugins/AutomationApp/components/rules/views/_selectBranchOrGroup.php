<br />
<div class="admin-uploaded-file-rule-settings">
	<p class="params-title">
		<?= Yii::t( 'standard', 'Selected branches/groups' ) ?>: <strong id="groupsBranchesCounter">0</strong>
		&nbsp;&nbsp;&nbsp;
		<?= CHtml::link(Yii::t('standard', '_SELECT'), '#', array(
				'class' => 'btn-docebo green big bigpadding group-select-users users-selector',
				'style' => 'display: inline-block;',
				'title' => 'Assign users',
				'id'    => 'select-branch-or-group-btn'

		)); ?>
		<?= CHtml::hiddenField('rule_groups', ''); ?>
		<?= CHtml::hiddenField('rule_branches', '{}'); ?>
		<?= CHtml::hiddenField('groupsBranchesCounterInput', '0'); ?>
	</p>
</div>

<?php

$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
		'type' 	=> UsersSelector::TYPE_AUTOMATION_RULE,
//		'useSessionData' => true,
//		'idUser'	=> $idUser,
));

?>

<script type="text/javascript">
	$(function(){

		$('#select-branch-or-group-btn').on('click', function(e) {
			e.preventDefault();

			var rule_groups = $('#rule_groups').val();
			var rule_branches = $('#rule_branches').val();

			openDialog(
					false,
					Yii.t('manmenu', 'Select groups and branches'),
					'/lms/index.php?r=usersSelector/axOpen&type=automation_rule_wizard',
					'automation-select-branch-group',
					'users-selector automation-select-branch-group',
					'POST',
					{
						//rule_groups: rule_groups,
						//rule_branches: rule_branches
					}
			);
		});

		$(document).off('dialog2.content-update', '.automation-select-branch-group');
		$(document).on('dialog2.content-update', '.automation-select-branch-group', function () {
			var that = $(this);
			var currGroupsGridId = $('div#groups > div.grid-view').attr('id');
			var rule_groups = $('#rule_groups').val();
			var rule_groups_arr = '['+rule_groups+']';

			// Do actual group preselection here !!!
			$('input[name="selected_groups"]').val(rule_groups_arr).change();
		});



		$(document).off('users-selector-form-submission');
		$(document).on('users-selector-form-submission', function(e, selectorData) {

			// Is there is data returned as object proceed
			if(typeof selectorData == 'object') {
				// If there are selected branches
				if(('selectedOrgcharts' in selectorData) && (typeof selectorData.selectedOrgcharts == 'object')) {
					Docebo.log(selectorData.selectedOrgcharts);
					var selectedBranches = {};
					var groupsBranchesCounter = 0;
					$.each(selectorData.selectedOrgcharts, function(index, elem) {
						if(typeof elem == 'object' && ('key' in elem) && ('selectState' in elem)) {
							selectedBranches[elem.key] = elem.selectState;
							groupsBranchesCounter++;
						}
					});

					$('#rule_branches').val(JSON.stringify(selectedBranches));
				}

				// If there are selected groups
				if(('selectedGroups' in selectorData) && ($.isArray(selectorData.selectedGroups))) {
					groupsBranchesCounter += selectorData.selectedGroups.length;
					var selectedGroups = selectorData.selectedGroups.join(',');
					$('#rule_groups').val(selectedGroups);
				}

				$('#groupsBranchesCounter').html(groupsBranchesCounter);
				$('#groupsBranchesCounterInput').val(groupsBranchesCounter);
			}

		});

		$('#groupsBranchesCounter').html($('#groupsBranchesCounterInput').val());

		$(document).off('change', '#groupsBranchesCounterInput');
		$(document).on('change', '#groupsBranchesCounterInput', function() {
			$('#groupsBranchesCounter').html($('#groupsBranchesCounterInput').val());
		});

		$(document).off("afterRecipeSettingsRestore", ".recipes-settings");
		$(document).on("afterRecipeSettingsRestore", ".recipes-settings", function(){
			$('#groupsBranchesCounter').html($('#groupsBranchesCounterInput').val());
		});

	});
</script>