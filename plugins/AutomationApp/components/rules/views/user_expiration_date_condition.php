<?=CHtml::dropDownList('user_expiration_date_condition', null, array(
	UserExpirationDateCondition::DATE_HAS_VALUE=>Yii::t('automation', 'Is not empty'),
	UserExpirationDateCondition::DATE_FUTURE=>Yii::t('automation', 'Is in the future'),
	UserExpirationDateCondition::DATE_PAST=>Yii::t('automation', 'Is in the past'),
));