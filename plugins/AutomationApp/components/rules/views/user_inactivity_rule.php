<div class="user-inactivity-rule-settings">
	<p><?= Yii::t( 'standard', 'Value' ) ?></p>

	<div class="row-fluid">
		<div class="span4">
			<?= CHtml::dropDownList( 'inactivity_operator', NULL, array(
				'>' => Yii::t( 'automation', 'more than' ),
				'<' => Yii::t( 'automation', 'less than' ),
			) ) ?>
		</div>
		<div class="span4">
			<input type="text" name="inactivity_number"/>
		</div>
		<div class="span4">
			<?= CHtml::dropDownList( 'inactivity_unit', NULL, array(
				'day'   => Yii::t( 'standard', 'Days' ),
				'week'  => Yii::t( 'standard', 'Weeks' ),
				'month' => Yii::t( 'standard', 'Months' ),
			) ) ?>
		</div>
	</div>
</div>