<?php

class m151119_105030_ALTER_TABLE_automation_rule_ADD_COLUMN_id_job extends CDbMigration
{
	public function up()
	{
		$this->addColumn('automation_rule', 'id_job', 'integer');
	}

	public function down()
	{
		$this->dropColumn('automation_rule', 'id_job');
	}
}