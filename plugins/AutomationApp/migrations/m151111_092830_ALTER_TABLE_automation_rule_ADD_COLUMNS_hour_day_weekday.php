<?php

class m151111_092830_ALTER_TABLE_automation_rule_ADD_COLUMNS_hour_day_weekday extends CDbMigration {
	public function up () {
		try {
			$this->addColumn( 'automation_rule', 'schedule_day_of_month', 'TINYINT(2) NULL' );
			$this->addColumn( 'automation_rule', 'schedule_day_of_week', 'TINYINT(1) NULL' );
			$this->addColumn( 'automation_rule', 'schedule_hour', 'TINYINT(2) NULL' );
			$this->addColumn( 'automation_rule', 'schedule_minute', 'TINYINT(2) NULL' );
			$this->dropColumn( 'automation_rule', 'schedule_value' );
		} catch ( Exception $e ) {
			echo 'ERROR: ' . $e->getMessage();
			Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR );
		}
	}

	public function down () {
		try {
			$this->dropColumn( 'automation_rule', 'schedule_day_of_month' );
			$this->dropColumn( 'automation_rule', 'schedule_day_of_week' );
			$this->dropColumn( 'automation_rule', 'schedule_hour' );
			$this->dropColumn( 'automation_rule', 'schedule_minute' );
			$this->addColumn( 'automation_rule', 'schedule_minute', 'TINYINT(4) NULL DEFAULT 0' );
		} catch ( Exception $e ) {
			echo 'ERROR: ' . $e->getMessage();
			Yii::log( $e->getMessage(), CLogger::LEVEL_ERROR );
		}
	}
}