<?php

class m151123_145208_ALTER_TABLE_automation_rule_condition__automation_rule_action_MODIFY_COLUMNS_params extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('automation_rule_condition', 'condition_params', 'text');
		$this->alterColumn('automation_rule_action', 'action_params', 'text');
	}

	public function down()
	{
		$this->alterColumn('automation_rule_condition', 'condition_params', 'tinytext');
		$this->alterColumn('automation_rule_action', 'action_params', 'tinytext');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}