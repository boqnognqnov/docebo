<?php

class m151103_154440_CREATE_TABLE_automation_rules extends CDbMigration
{
	public function safeUp()
	{
		try {
			$sql = <<<SQL

SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE IF NOT EXISTS `automation_rule` (
  	`id_rule` INT(11) NOT NULL AUTO_INCREMENT,
  	`name` VARCHAR(255) NOT NULL,
	`description` VARCHAR(512) NULL,
	`active` TINYINT(1) NOT NULL default 0,
	`conditions_operator` ENUM('AND','OR') default 'AND',
	`trigger_type` ENUM('scheduled','realtime') NOT NULL default 'realtime',
	`trigger_events` VARCHAR(512) NULL COMMENT 'Comma separated list of hook names which will trigger this rule',
	`schedule_unit` ENUM('day','week','month') NULL,
	`schedule_value` TINYINT(4) default 0,
	PRIMARY KEY (`id_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `automation_rule_condition` (
	`id_condition` INT(11) NOT NULL AUTO_INCREMENT,
	`id_rule` INT(11) NOT NULL,
	`handler` VARCHAR(255) NOT NULL,
	`condition_params` TINYTEXT NULL,
  	PRIMARY KEY (`id_condition`),
  	CONSTRAINT `fk_automation_rule_condition` FOREIGN KEY (`id_rule`) REFERENCES `automation_rule` (`id_rule`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `automation_rule_action` (
  	`id_action` INT(11) NOT NULL AUTO_INCREMENT,
	`id_rule` INT(11) NOT NULL,
	`handler` VARCHAR(255) NOT NULL,
	`action_params` TINYTEXT NULL,
  	PRIMARY KEY (`id_action`),
  	CONSTRAINT `fk_automation_rule_action` FOREIGN KEY (`id_rule`) REFERENCES `automation_rule` (`id_rule`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `automation_rule_log` (
	`id_log` INT(11) NOT NULL AUTO_INCREMENT,
   	`id_rule` INT(11) NOT NULL,
	`on_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`result` LONGTEXT NULL,
	`rolled_back` TINYINT(1) NOT NULL default 0,
	PRIMARY KEY (`id_log`),
  	CONSTRAINT `fk_automation_rule_log` FOREIGN KEY (`id_rule`) REFERENCES `automation_rule` (`id_rule`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS=1;

SQL;
			$this->execute($sql);
		} catch (Exception $e) {
			echo "\nMIGRATION EXCEPTION:\n";
			echo $e->getMessage();
			echo "\n";
			return false;
		}
	}

	public function safeDown()
	{
		try {

			$sql = <<<SQL
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE `automation_rule_log`;
DROP TABLE `automation_rule_action`;
DROP TABLE `automation_rule_condition`;
DROP TABLE `automation_rule`;

SET FOREIGN_KEY_CHECKS=1;
SQL;

			$this->execute($sql);
		} catch (Exception $e) {
			Yii::log('Failed migration DOWN: Domain: ' . $GLOBALS['domain_code'] . ', ' . __CLASS__, 'error');
			return false;
		}
	}
}