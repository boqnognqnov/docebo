<?php
/**
 * @var $handler AutomationConditionHandler
 * @var $this    RuleWizardController
 * @var $params  array
 */
?>

<div id="<?=$uniqueId?>" class="single-condition <?=$uniqueId?>" data-handler="<?= $handlerName ?>" data-params='<?= CJSON::encode( $params ) ?>'>
	<div class="pull-right">
		<a href="#" class="edit-condition"><i class="fa fa-pencil-square-o"></i></a>
		<a href="#" class="delete-condition"><i class="fa fa-times"></i></a>
	</div>
	<div class="rule-descriptor">
		<p>
			<?= $handler->describeConditionByParams($params) ?>
		</p>
		<input type="hidden" name="active-conditions[<?=intval(microtime(1)).rand(100,200)// we need some value here, even a fake one works?>]" value='<?= CJSON::encode( array('handler'=>$handlerName, 'params'=>$params )) ?>'/>
	</div>
</div>