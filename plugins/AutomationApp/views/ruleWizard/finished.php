<?
/* @var $model AutomationRule */
?>
<div class="step-finished clearfix row-fluid">

	<div class="new-rule-success span3">
		<div class="copy-success-icon"></div>
		<?php echo Yii::t('standard', '_OPERATION_SUCCESSFUL'); ?>
	</div>

	<div class="new-rule-success-content span9">
		<h3><?php echo Yii::t('standard', 'What do you want to do next?'); ?></h3>

		<? if($model->trigger_type==AutomationRule::SCHEDULE_TYPE_RECURRING): ?>
		<div class="span12 new-rule-info-row save-and-run">
			<div class="pull-right">
				<?php echo CHtml::link(Yii::t('automation', 'Save, Activate and Execute'), Docebo::createAppUrl('lms:player/training/session', array('course_id'=>$model->id_rule)), array('class' => 'btn btn-submit')); ?>
			</div>
			<div class="lbl">
				<span><?php echo Yii::t('automation', '<span>Save, Activate and Execute</span> the rule'); ?></span>
			</div>
		</div>
		<? endif; ?>

		<div class="span12 new-rule-info-row save-and-back">
			<div class="pull-right">
				<?php echo CHtml::link(Yii::t('automation', 'Save and Back to list'), Docebo::createAppUrl('lms:player/training/session', array('course_id'=>$model->id_rule)), array('class' => 'btn btn-submit')); ?>
			</div>
			<div class="lbl">
				<span><?php echo Yii::t('automation', '<span>Just save</span> the rule and go back'); ?></span>
			</div>
		</div>


	</div>

</div>

<script>
	$('a.btn-submit').on('click', function(e){
		var button = $(e.target);

		if(button.hasClass('disabled')) {
			e.preventDefault();
			e.stopPropagation();
			return false;
		} else {
			button.addClass('disabled');
		}
	});

</script>