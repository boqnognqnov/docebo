<?
/* @var $model AutomationRule */
/* @var $allowedTriggerTypes array */
?>
<div class="step-schedule">
	<h1><?= Yii::t( 'automation', 'New rule' ) ?></h1>

	<div class="row-fluid">
		<? if(in_array(AutomationRule::SCHEDULE_TYPE_RECURRING, $allowedTriggerTypes)): ?>
		<div class="span6">
			<?= CHtml::activeRadioButton( $model, 'trigger_type', array(
				'value' => AutomationRule::SCHEDULE_TYPE_RECURRING,
				'uncheckValue'=>null
			) ) ?> <?= Yii::t( 'automation', 'Schedule this rule' ) ?>
			<p class="light-description">
				<?= Yii::t( 'automation', 'Select date/time to execute this rule' ) ?>
			</p>
		</div>
		<? endif; ?>

		<? if(in_array(AutomationRule::SCHEDULE_TYPE_REALTIME, $allowedTriggerTypes)): ?>
		<div class="span6">
			<?= CHtml::activeRadioButton( $model, 'trigger_type', array(
				'value' => AutomationRule::SCHEDULE_TYPE_REALTIME,
				'uncheckValue'=>null
			) ) ?> <?= Yii::t( 'automation', 'Do not schedule this rule' ) ?>
			<p class="light-description">
				<?= Yii::t( 'automation', 'The rule is triggered in realtime when its conditions are met' ) ?>
			</p>
		</div>
		<? endif; ?>
	</div>

	<div class="schedule-interval-settings"
		 style="<?= $model->schedule_unit != AutomationRule::SCHEDULE_TYPE_RECURRING ? 'display:none' : NULL ?>">
		<br/>

		<?= Yii::t( 'standard', 'When' ) ?>?

		<br/>

		<?= CHtml::activeDropDownList( $model, 'schedule_unit', array(
			'day'   => Yii::t( 'standard', 'Daily' ),
			'week'  => Yii::t( 'standard', 'Weekly' ),
			'month' => Yii::t( 'standard', 'Montly' ),
		) ) ?>

		<span class="day-of-week" style="display: none;">
		&nbsp;&nbsp;<?= Yii::t( 'standard', 'on' ) ?>&nbsp;&nbsp;
			<?= CHtml::activeDropDownList( $model, 'schedule_day_of_week', Docebo::getWeekDays(), array(
				'class' => 'input-small'
			) ) ?>
		</span>
		<span class="day-of-month" style="display: none;">
			&nbsp;&nbsp;<?= Yii::t( 'standard', 'on' ) ?>&nbsp;&nbsp;
			<? $days = array();
			for ( $i = 1; $i <= 31; $i ++ ) {
				$days[$i] = $i;
			}
			?>
			<?= CHtml::activeDropDownList( $model, 'schedule_day_of_month', $days, array(
				'class' => 'input-small'
			) ) ?>
		</span>
		<span class="hour-of-day">
			&nbsp;&nbsp;<?= Yii::t( 'standard', 'at' ) ?>&nbsp;&nbsp;
			<?= CHtml::dropDownList( 'tmp_schedule_hour', ( $model->schedule_hour * 60 + $model->schedule_minute ), TimeHelpers::arrayForDurations( true, 60 ), array( 'class' => 'input-small' ) ) ?>

			<?
			// The hidden fields below will be autopopulated by JS
			// when the dropdown named 'tmp_schedule_hour' is changed.
			// See plugins/AutomationApp/assets/rule-wizard.js
			echo CHtml::activeHiddenField( $model, 'schedule_hour' );
			echo CHtml::activeHiddenField( $model, 'schedule_minute' );
			?>
		</span>
	</div>
	<!-- .schedule-interval-settings -->

</div>