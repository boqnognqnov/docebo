<div class="step-actions">

	<p><b><?= Yii::t( 'automation', 'Your rule recipe' ) ?></b></p>

	<div class="recipe-descriptor">
		<span class="if-holder">
			<?= Yii::t( 'standard', 'if' ) ?>
		</span>
		<span class="recipes-placeholder">
			<?
			$conditionDescriptorsArr = array();
			if( ! empty( $inData ) && isset( $inData['active-conditions'] ) ): ?>
				<? foreach ($inData['active-conditions'] as $activeConditionDetail ): ?>
					<?
					$activeConditionDetail = CJSON::decode($activeConditionDetail);
					$handler = $activeConditionDetail['handler'];
					$params = $activeConditionDetail['params'];
					?>
					<?
					// Create a temporary model to help resolve the actual handler object
					$handlerWrapperModel                   = new AutomationRuleCondition();
					$handlerWrapperModel->handler          = $handler;
					$handlerWrapperModel->condition_params = $params;
					$handler                               = $handlerWrapperModel->getHandlerObject();
					if( method_exists( $handler, 'describeConditionByParams' ) ): ?>
						<? $conditionDescriptorsArr[] = $handler->describeConditionByParams(); ?>
					<? endif; ?>
				<? endforeach ?>


				<?= implode( " " . $model->conditions_operator . " ", $conditionDescriptorsArr ) ?>

			<? endif; ?>

		</span>
        <span class="then-holder">
			<?= Yii::t( 'standard', 'then' ) ?>
		</span>
		<span class="actions-placeholder empty">
			<?= Yii::t( 'automation', 'Select one or more action to execute' ) ?>
		</span>
	</div>

	<br/>

	<p class="define-action-title">
		<?= Yii::t( 'automation', 'Define action' ) ?>:
	</p>

	<?= CHtml::dropDownList( 'actions_list', NULL, array( Yii::t( 'standard', 'Select...' ) ) + AutomationRule::getActionsHandlers(), array(
		'class'            => 'span12',
		'data-bootstro-id' => 'bootstroNoActions'
	) ) ?>

	<div class="actions-settings"></div>
	<!-- .recipe-settings -->

	<div class="clearfix"></div>

	<div class="pull-right">
		<a class="btn-docebo green big bigpadding add-action" href="#"><?= Yii::t( 'standard', 'Add' ) ?></a>
	</div>

	<div class="clearfix"></div>

	<div class="cookbook-wrapper">
		<h3><?= Yii::t( 'automation', 'Active actions' ) ?>:</h3>

		<div class="inner">
			<?
			if( ! empty( $inData ) && isset( $inData['active-actions'] ) ): ?>

				<? foreach ($inData['active-actions'] as $activeConditionDetails ):
						// Incoming data should be always string(JSON), if not then the controller is passing wrong data
						$data = CJSON::decode($activeConditionDetails);
						$handlerName = $data['handler'];
						$params = $data['params'];

						// Create a temporary model to help resolve the actual handler object
						$handlerWrapperModel                = new AutomationRuleAction();
						$handlerWrapperModel->handler       = $handlerName;
						$handlerWrapperModel->action_params = $params;
						$handler                            = $handlerWrapperModel->getHandlerObject();
						if( method_exists( $handler, 'describeActionByParams' ) ): ?>
							<? $this->renderPartial( 'AutomationApp.views.ruleWizard.actionDescriptor', array(
								'handler'     => $handler,
								'handlerName' => $handlerName,
								'params'      => $params,
								'uniqueId'    => 'action-' . Docebo::randomHash(),
							) ); ?>
						<? endif; ?>
					<? endforeach; ?>
			<? endif; ?>
		</div>

	</div>

</div>