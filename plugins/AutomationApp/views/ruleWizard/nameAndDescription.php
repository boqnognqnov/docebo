<h1><?= Yii::t( 'standard', 'New Rule' ) ?></h1>

<div class="row-fluid">
	<div class="span12">
		<?= Yii::t( 'automation', 'Name' ) ?>
		<br/>
		<?= CHtml::activeTextField( $model, 'name', array(
			'class' => 'span12'
		) ) ?>

		<br/><br/>

		<?= Yii::t( 'automation', 'Description' ) ?>
		<br/>
		<?= CHtml::activeTextArea( $model, 'description', array(
			'class' => 'span12'
		) ) ?>
	</div>
</div>