<?
/* @var $model AutomationRule */
?>
<div class="step-recipes">

	<p><b><?= Yii::t( 'automation', 'Your rule recipe' ) ?></b></p>

	<div class="recipe-descriptor">
		<span class="if-holder">
			<?= Yii::t( 'standard', 'if' ) ?>
		</span>

		&nbsp;&nbsp;
		<span class="recipes-placeholder empty" data-placeholder="<?= Yii::t( 'automation', 'Select a condition to start building your rule...' ) ?>">
			<?= Yii::t( 'automation', 'Select a condition to start building your rule...' ) ?>
		</span>
	</div>

	<br/>

	<p class="define-condition-title">
		<?= Yii::t( 'automation', 'Define condition' ) ?>:
	</p>

	<?= CHtml::dropDownList( 'conditions_list', NULL, array( Yii::t('standard', 'Select...') ) + AutomationRule::getConditionHandlers($model->trigger_type ? array($model->trigger_type) : array()), array(
		'class' => 'span12',
		'data-bootstro-id'=>'bootstroNoRecipes'
	) ) ?>

	<div class="recipes-settings"></div><!-- .recipe-settings -->

	<div class="clearfix"></div>

	<div class="pull-right">
		<a class="btn-docebo green big bigpadding add-recipe" href="#"><?=Yii::t('standard', 'Add')?></a>
	</div>

	<div class="clearfix"></div>

	<div class="cookbook-wrapper">
		<h3><?=Yii::t('automation', 'Active conditions')?>:</h3>

		<div class="inner">
			<?=implode('', $restoredElements)?>
		</div>

		<div class="condition-operators">
			<p><?=CHtml::activeRadioButton($model, 'conditions_operator', array('value'=>AutomationRule::CONDITION_AND, 'uncheckValue'=>null))?> <?=Yii::t('standard', 'All the above conditions must be satisfied')?></p>
			<p><?=CHtml::activeRadioButton($model, 'conditions_operator', array('value'=>AutomationRule::CONDITION_OR, 'uncheckValue'=>null))?> <?=Yii::t('standard', 'At least one of the above conditions must be satisfied')?></p>
		</div>

	</div>



</div>