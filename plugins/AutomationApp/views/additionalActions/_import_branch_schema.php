<h1><?= Yii::t('automation', 'Define Import Schema') ?></h1>

<?php
$form = $this->beginWidget( 'CActiveForm', array(
    'id'          => 'import-schema-form',
    'htmlOptions' => array('class' => 'ajax'),
) );
?>

<p><?= Yii::t( 'automation', 'Select the columns in your CSV file and map them to the corresponding branch field' ) ?>.</p>

<div style="display: none" class="columnsList">
    <?= CHtml::dropDownList('import_map[]', false, OrgChartImportForm::getImportMapList(), array('class'=>"column-types")) ?>
</div>

<?php
    $this->widget('common.widgets.ComboGridView', array(
        'gridId' => 'import-schema-grid',
        'disableMassSelection' => true,
        'disableMassActions' => true,
        'disableSearch' => true,
        'sortableRows' => true,
        'sortableUpdateUrl' => false,
        'gridTemplate' => '{items}',
        'dataProvider' => new CArrayDataProvider(array(), array()),
        'columns' => array(
            array(
                'header' => strtoupper(Yii::t('automation', 'Column')),
                'name' => 'column',
                'value' => 'Yii::t("automacion", "Column") . " " . $data["sequence"]',
                'type' => 'raw',
                'htmlOptions' => array('class' => 'span2 row-number'),
            ),
            array(
                'header' => strtoupper(Yii::t('automation', 'Attribute')),
                'value' => function($data){
                    echo CHtml::dropDownList('import_map['.$data['id'].']', false, OrgChartImportForm::getImportMapList(), array('class'=>"column-types"));
                },
                'type' => 'raw',
                'htmlOptions' => array('class' => 'span3'),
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{order}{delete}',
                'htmlOptions' => array('class' => 'button-column import-schema-actions', 'style' => 'width: 180px'),
                'header' => CHtml::link(strtoupper(Yii::t('automation', 'Add New Column')), '#', array('class' => 'btn btn-docebo green big new-column')),
                'headerHtmlOptions' => array('class' => 'import-schema-new-column'),
                'buttons' => array(
                    'order' => array(
                        'url' => '"javascript:;"',
                        'label' => Yii::t('standard', '_MOVE'),
                        'options' => array('class' => 'order-field move'),
                    ),
                    'delete' => array(
                        'url' => '"javascript:;"',
                        'click' => 'function(){}'
                    ),
                ),
            ),
        ),
    ));
?>

<!-- For Dialog2 - they go in footer -->
<div class="form-actions">
    <?= CHtml::button( Yii::t( 'standard', '_SAVE' ), array('class' => 'btn btn-docebo green big keep-schema' )); ?>
    <?= CHtml::button( Yii::t( 'standard', '_CANCEL' ), array( 'class' => 'btn btn-cancel close-dialog' ) ); ?>
</div>

<?php $this->endWidget(); ?>

<style>
    .grid-view table.items th.import-schema-new-column, .grid-view table.items td.button-column.import-schema-actions{
        text-align: right;
    }
    .grid-view table.items th a.new-column{
        color: white !important;
    }

    .grid-view table.items td > .column-types{
        width: 250px;
    }

    .button-column.import-schema-actions > a.delete{
        margin-left: 2px;
    }

</style>

<script>
    var count = 0;

    $(function(){
        // Well the document is ready so we need to fill the grid with the proper
        var currentSchema = $('#importMap').val().split (",");

        var fillRow = function(rowNumber, selected){
            var columnsList = $(".columnsList select").clone(true).off();
            // Set selected value is any
            columnsList.find('option[value="'+selected+'"]').attr('selected', true);

            var row = "<tr class='sorted[]_" + rowNumber + "'>";
            row += "<td class='span2 row-number'>" + "<?= Yii::t("automation", "Column") ?>" + " " + rowNumber + "</td>";
            row += "<td class='span3'>" + columnsList[0].outerHTML + "</td>";
            row += "<td class='button-column import-schema-actions' style='width: 180px'><a class='order-field move' title='Move' href='javascript:;'>Move</a><a class='delete' title='Delete' href='javascript:;'></a></td>";
            row += "</tr>";

            return row;
        };

        var fillGrid = function(html, rowNumber){
            // If there is not content yet let's set this row to be the entire content of the grid for now and in the next iteration it will be appended
            if(count == 0)
                $('#import-schema-grid table tbody').html(html);
            else
                $('#import-schema-grid table tbody').append(html);

            // Append the keys
            $('#import-schema-grid div.keys').append("<span>" + rowNumber + '</span>');
        }

        // First we need to fill the grid if there is any previous selections
        if($('#importMap').val()){
            for (var i = 0; i < currentSchema.length; i++){
                var rowNumber = count + 1;
                // Let's first fill the grid
                var html = fillRow(rowNumber, currentSchema[i]);
                // Let's ge set the correct counter
                fillGrid($(html), rowNumber);
                count++;
            }
        }

        $(document).off('click', '.import-schema-new-column > .new-column').on("click", '.import-schema-new-column > .new-column',function(){
            var rowNumber = parseInt(count) + 1;
            var html = fillRow(rowNumber);
            // Let's fill the grid
            fillGrid(html, rowNumber);

            count++;
        });

        $(document).off('click', '#import-schema-grid .import-schema-actions a.delete').on('click', '#import-schema-grid .import-schema-actions a.delete', function(){
            var number = $(this).closest("tr").attr("class").replace(/[^\d.-]/g, "");
            // Clear the keys
            $("#import-schema-grid .keys > span:contains(\'" + number + "\')").remove();
            // Not let's remvoe the entire row from the table
            $(this).closest("tr").remove();
            count--;

            // Recalculate column number
            recalculateColumns();
        });
        $(document).off('click', '.modal.importSchema a.keep-schema').on("click", '.modal.importSchema a.keep-schema', function(){
            var schema = [];
            // Collect what ever the user has entered
            $('#import-schema-grid tbody').find('select').each(function(){
                schema.push($(this).val());
            });
            // Now when we collected schema let's set keep it
            $('#importMap').val(schema);

            $(this).closest('.modal').find('.modal-body').dialog2("close");
        });

        $(document).off("sortupdate", "#import-schema-grid table.items tbody").on( "sortupdate", "#import-schema-grid table.items tbody" ,function( event, ui ) {
            // Recalculate column number
            recalculateColumns();
        } );

        var recalculateColumns = function (){
            var title = '<?= Yii::t("automacion", "Column") ?>';
            $('tbody.ui-sortable tr .row-number').each(function(i,e){
                $(e).html(title + " " + (i + 1));
            })
        }
    });
</script>