<?php
/**
 * @var $dataProvider CSqlDataProvider
 * @var $this AutomationAppController
 */

/* @var CClientScript $cs */
$cs = Yii::app()->getClientScript();

// Needed by the Rule creation wizard
$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jwizard.js');

$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('automation', 'Automation')
);

$this->breadcrumbs = $_breadcrumbs;

?>
<script>
	if(!bootstroLang) {
		var bootstroLang = {
			bootstroNoRecipes: '<?=Yii::t('automation', '{arrow}Start here!<br/>Select a condition and start<br/>building your rule!', array(
				'{arrow}'=>'<div class="bootstro-arrow arrow-nw"></div>'
			))?>',
			bootstroNoActions: '<?=Yii::t('automation', '{arrow}Start here!<br/>Select one or more actions to execute!', array(
				'{arrow}'=>'<div class="bootstro-arrow arrow-nw"></div>'
			))?>'
		};
	}
</script>
<?
$this->widget('OverlayHints', array('hints'=>'bootstroNoRecipes'));

$wizardId = 'rule-wizard-dialog';
?>

<div class="main-actions clearfix">
	<h3 class="title-bold"><?=Yii::t('automation', 'Automation')?></h3>

	<ul>
		<li>
			<div>
				<?
				// Those will be needed by the wizard. Registering them there caused them to be
				// included multiple times, which caused problems, that's why they are moved here and loaded just once
 				$assetsUrl = Yii::app()->assetManager->publish( Yii::getPathOfAlias( 'AutomationApp.assets' ));
 				Yii::app()->clientScript->registerCssFile   ($assetsUrl . '/rule-wizard.css');
 				Yii::app()->clientScript->registerScriptFile($assetsUrl . '/rule-wizard.js');
				?>
				<a rel="<?=$wizardId?>" alt="<?=Yii::t('automation', 'New rule')?>" class="open-dialog new-node" data-dialog-class="rule-wizard-dialog" href="<?=DOcebo::createAdminUrl('AutomationApp/RuleWizard/wizard')?>"><span></span> <?=Yii::t('automation', 'New rule')?></a>
			</div>
		</li>
	</ul>

	<div class="info"></div>
</div>
<?
$this->widget('common.widgets.ComboGridView', array(
	'gridId' => 'automation-rules-grid',
	'disableMassSelection' => true,
	'disableMassActions' => true,
	'disableSearch' => true,
	'dataProvider' => $dataProvider,
	'columns' => array(
		array(
			'header' => Yii::t('automation', 'Rule name'),
			'name' => 'name',
		),
		array(
			'header'=>Yii::t('automation', 'Scheduled'),
			'value'=>function($data){
				switch($data['trigger_type']){
					case AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_REALTIME:
						return Yii::t('automation', 'Real time');
						break;
					case AutomationConditionHandler::AUTOMATION_CONDITION_TRIGGER_SCHEDULED:
						$scheduleUnit = $data['schedule_unit'];
						$scheduleDayOfMonth = $data['schedule_day_of_month'];
						$scheduleDayOfWeek = $data['schedule_day_of_week'];
						$scheduleHour = $data['schedule_hour'];
						$scheduleMinute = $data['schedule_minute'];

						$weekDayTranslations = Docebo::getWeekDays();

						$params = array(
							'{hour}'=>sprintf("%02d", $scheduleHour),
							'{minute}'=>sprintf("%02d", $scheduleMinute),
							'{day}'=>$scheduleDayOfMonth,
							'{weekDay}'=>$weekDayTranslations[$scheduleDayOfWeek],
						);
						switch($scheduleUnit){
							case AutomationRule::SCHEDULE_UNIT_DAY:
									return Yii::t('automation', 'Every day at {hour}:{minute}', $params);
								break;
							case AutomationRule::SCHEDULE_UNIT_WEEK:
									return Yii::t('automation', 'Every {weekDay} at {hour}:{minute}', $params);
								break;
							case AutomationRule::SCHEDULE_UNIT_MONTH:
									return Yii::t('automation', 'Every month on the {day}st at {hour}:{minute}', $params);
								break;
						}

						break;
				}
			}
		),
		array(
			'type' => 'raw',
			'htmlOptions' => array(
				'class' => 'single-column',
			),
			'value' => function($data){
				if($data['active'] && $data['trigger_type']==AutomationRule::SCHEDULE_TYPE_RECURRING) {
					return CHtml::link( "<i class=\"fa fa-play fa-lg text-success\"></i>", Docebo::createAdminUrl( "AutomationApp/AutomationApp/triggerRule", array( "id" => $data["id_rule"] ) ), array( "class" => "activate-rule" ) );
				}
			},
		),
		array(
			'type' => 'raw',
			'htmlOptions' => array(
				'class' => 'single-column',
			),
			'value' => 'CHtml::link("", Yii::app()->createUrl("AutomationApp/AutomationApp/changeRuleStatus", array("id" => $data["id_rule"])), array("class" => "rule-status ".( (bool) $data["active"] ? "suspend-action" : "activate-action" ))) ',
		),
		array(
			'type' => 'raw',
			'htmlOptions' => array(
				'class' => 'single-column',
			),
			'value' => function($data) use ($wizardId){
				return CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('ruleWizard/wizard', array('id' => $data['id_rule'])), array(
					'class' => 'open-dialog edit-action',
					'data-dialog-class' => 'rule-wizard-dialog',
					'title' => Yii::t('standard', '_MOD'),
					'closeOnOverlayClick' => 'false',
					'closeOnEscape' => 'false',
					'removeOnClose' => 'true',
					'rel' => $wizardId,
				));
			}
		),
		array(
			'type' => 'raw',
			'htmlOptions' => array(
				'class' => 'single-column',
			),
			'value' => function($data){
				return CHtml::link('', $this->createUrl('automationApp/axDelete', array('id' => $data['id_rule'])), array(
					'class' => 'open-dialog delete-action',
					'data-dialog-class' => 'modal-delete-rule',
					'title' => Yii::t('standard', '_DEL'),
					'removeOnClose' => 'true',
					'rel' => 'tooltip',
				));
			}
		),
	),
));
?>

<script>
	$(function(){
		// Create Wizard object (themes/spt/js/jwizard.js)
		var jwizard = new jwizardClass({
			dialogId		: '<?= $wizardId ?>',
			alwaysPostGlobal: true,
			debug			: true
		});

		$(document).on('click', 'a.rule-status' ,function(){
			var node = $(this);
			$.ajax({
				url: $(this).attr('href'),
				success: function(){
					if (node.hasClass('suspend-action')) {
						node.attr('class', 'rule-status activate-action');
					} else {
						node.attr('class', 'rule-status suspend-action');
					}
					$('#automation-rules-grid').comboGridView('updateGrid');
				}
			});
			return false;
		});

		$(document).on('click', '.activate-rule' ,function(e){
			var node = $(this);
			$.ajax({
				url: $(this).attr('href'),
				success: function(){
					// Rule triggered now
					Docebo.Feedback.show('success', Yii.t('standard', 'Rule triggered'));
				}
			});
			return false;
		});

		$(document).on("dialog2.content-update", ".modal-delete-rule", function () {
			if ($(this).find('a.update-grid').length > 0) {
				$('#automation-rules-grid').comboGridView('updateGrid');
			}
		});
	})
</script>