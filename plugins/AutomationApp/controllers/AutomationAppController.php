<?php
/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 9.11.2015 г.
 * Time: 10:59 ч.
 */

class AutomationAppController extends Controller {

	public function beforeAction($action) {
		return parent::beforeAction($action);
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	public function actionSettings(){
		$model = new AutomationRule();

		UsersSelector::registerClientScripts();
		$this->render('settings', array(
			'dataProvider'=>$model->dataProvider(),
		));

	}

	public function actionChangeRuleStatus(){
		Yii::app()->db->createCommand('UPDATE ' . AutomationRule::model()->tableName(). ' SET active = NOT active WHERE id_rule = :id')->execute(array( ':id' => Yii::app()->request->getParam('id', null)));
	}

	public function actionAxDelete()
	{
		try {
			$id = Yii::app()->request->getParam('id', false);
			$model = AutomationRule::model()->findByPk($id);

			if ($model == null) {
				throw new Exception('Invalid layout, can not delete');
			}

			if (isset($_REQUEST['confirm'])) {
				if (isset($_REQUEST['agree_delete']) && $_REQUEST['agree_delete'] == 'agree') {
					$model->delete();
				}
				echo '<a class="auto-close success update-grid"></a>';
				Yii::app()->end();
			}

			$this->renderPartial('_confirm_delete', array(
				'model'	=> $model,
			));
			Yii::app()->end();
		}
		catch (Exception $e) {
			$this->renderPartial('//common/_dialog2_error', array('type' => 'error',  'message' => $e->getMessage()));
			Yii::app()->end();
		}
	}

	public function actionTriggerRule(){

		$id = Yii::app()->request->getParam('id', null);

		$rule = AutomationRule::model()->findByPk($id);
		if($rule !== null && $rule->active) {
			$params            = array();
			$params['id_rule'] = $rule->id_rule;
			Yii::app()->scheduler->createImmediateJob( 'AutomationRule', AutomationRuleJobHandler::id(), $params );
		}else{
			throw new CException('Inactive job can not be triggered');
		}
	}
}