<?php
class AdditionalActionsController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        $res = array();

        // keep it in the following order:

        // http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
        // Allow following actions to admin only:
        $admin_only_actions = array('importSchema', 'importBranchSchema', 'downloadBranchImportSample');
        $res[] = array(
            'allow',
            'roles' => array(Yii::app()->user->level_godadmin),
            'actions' => $admin_only_actions,
        );

        // deny admin only actions to all other users:
        $res[] = array(
            'deny',
            'actions' => $admin_only_actions,
        );

        // Allow access to other actions only to logged-in users:
        $res[] = array(
            'allow',
            'users' => array('@'),
        );

        $res[] = array(
            'deny', // deny all
            'users' => array('*'),
        );

        return $res;
    }

    public function actionImportSchema()
    {
        $this->renderPartial('_import_schema', array(), false, true);
    }

    public function actionImportBranchSchema()
    {
        $this->renderPartial('_import_branch_schema', array(), false, true);
    }

    public function actionDownloadBranchImportSample(){
        // download sample csv file
        if (isset($_GET['download']) && $_GET['download'] == 'sample') {
            $sep = DIRECTORY_SEPARATOR;
            $fileName = 'sample-automated-branch.csv';
            $filePath = Yii::getPathOfAlias('common').$sep.'extensions'.$sep.$fileName;
            if (file_exists($filePath)) {
                $fileContent = file_get_contents($filePath);
                Yii::app()->request->sendFile('sample.csv', $fileContent, NULL, false);
            }else{
                echo $_GET['download'].' . csv not found';
            }
            Yii::app()->end();
        }
    }
}