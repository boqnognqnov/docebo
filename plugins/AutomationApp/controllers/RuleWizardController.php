<?php

/**
 * @author Dzhuneyt <Dzhuneyt@dzhuneyt.com>
 * Date: 9.11.2015 г.
 * Time: 15:24 ч.
 */
class RuleWizardController extends DoceboWizardController {

	public function actionWizard () {
		// Get client model, if any
		$idRule    = Yii::app()->request->getParam( "id", FALSE );
		$ruleModel = AutomationRule::model()->findByPk( $idRule );
		if( ! $ruleModel ) {
			$ruleModel = new AutomationRule();
		}

		// Collect data from all steps of the wizard and set them to the model
		$inData = $this->collectStepData();
		if( isset( $inData['AutomationRule'] ) ) {
			$ruleModel->setAttributes( $inData['AutomationRule'] );
		}

		$params = array(
			'model' => $ruleModel
		);

		$this->navigate( $params );
	}

	/* Recipes/rules: */
	public function actionGetConditionSettingsForm(){
		$handlerName = isset($_POST['handlerName']) ? $_POST['handlerName'] : null;
		if(!$handlerName) return;

		$classPath = Yii::getPathOfAlias( $handlerName ) . '.php';
		if( ! is_file( $classPath ) ) {
			throw new CException( 'Invalid class path for handler ' . $handlerName . '. File not found at ' . $classPath );
		} else {
			Yii::import( $handlerName );
			$tmpParts            = explode( '.', $handlerName );
			$className           = end( $tmpParts );
			$ruleHandlerInstance = new $className;
			if( $ruleHandlerInstance instanceof AutomationConditionHandler ) {
				$ruleHandlerInstance->renderConditionParamsForm($tmpParts);
			}
		}
	}
	public function actionConvertConditionSettings(){
		$handlerName = isset($_POST['handlerName']) ? $_POST['handlerName'] : null;

		$instanceId = isset($_POST['editedId']) && $_POST['editedId'] ? $_POST['editedId'] : null;

		$recipeUniqueId = $instanceId ? $instanceId : 'condition-'.Docebo::randomHash();

		// The code below avoids issues with POST-ed fields with "nested names",
		// e.g. "active-conditions[123][something]" and properly parses them
		// to a multidimensional array
		$serializedParams = isset($_POST['params']) ? $_POST['params'] : array();
		$handlerParams = array();
		parse_str($serializedParams, $handlerParams);

		$res = array(
			'html'=>null,
			'descriptor'=>null,
			'uniqueId'=>$recipeUniqueId, // A unique ID that identifies this condition "instance"
		);

		$classPath = Yii::getPathOfAlias( $handlerName ) . '.php';
		if( ! is_file( $classPath ) ) {
			Yii::log('Invalid class path for handler ' . $handlerName . '. File not found at ' . $classPath, CLogger::LEVEL_ERROR);
			$res['html'] = 'Invalid class path for handler ' . $handlerName . '. File not found at ' . $classPath;
		} else {
			$model = new AutomationRuleCondition();
			$model->handler = $handlerName;
			$model->condition_params = $handlerParams;
			$ruleHandlerInstance = $model->getHandlerObject();

			if( $ruleHandlerInstance instanceof AutomationConditionHandler && method_exists($ruleHandlerInstance, 'describeConditionByParams') ) {
				$res['html'] = $this->renderPartial('ruleDescriptor', array(
					'handler'=>$ruleHandlerInstance,
					'handlerName'=>$handlerName,
					'params'=>$handlerParams,
					'uniqueId'=>$recipeUniqueId,
				), true);
				$res['descriptor'] = $ruleHandlerInstance->describeConditionByParams();
			}
		}

		echo CJSON::encode($res);
	}
	/* Recipes/rules end */


	/* ACTIONS */
	public function actionGetActionsSettingsForm(){
		$handlerName = isset($_POST['actionName']) ? $_POST['actionName'] : null;
		if(!$handlerName) return;

		$classPath = Yii::getPathOfAlias( $handlerName ) . '.php';
		if( ! is_file( $classPath ) ) {
			throw new CException( 'Invalid class path for handler ' . $handlerName . '. File not found at ' . $classPath );
		} else {
			Yii::import( $handlerName );
			$tmpParts            = explode( '.', $handlerName );
			$className           = end( $tmpParts );
			$ruleHandlerInstance = new $className;

			if( $ruleHandlerInstance instanceof AutomationActionHandler ) {
				$ruleHandlerInstance->renderConditionParamsForm($tmpParts);
			}
		}
	}

	public function actionConvertActionsSettings () {
		$handlerName   = isset( $_POST['handlerName'] ) ? $_POST['handlerName'] : NULL;

		$instanceId = isset( $_POST['editedId'] ) && $_POST['editedId'] ? $_POST['editedId'] : NULL;

		$serializedParams = isset( $_POST['params'] ) ? $_POST['params'] : array();
		$handlerParams    = array();
		parse_str( $serializedParams, $handlerParams );

		if(is_string($handlerParams)){
			$handlerParams = CJSON::decode($handlerParams);
		}

		$actionUniqueId = $instanceId ? $instanceId : 'action-' . Docebo::randomHash();

		$res = array(
			'html'=>null,
			'uniqueId'=>$actionUniqueId, // A unique ID that identifies this condition "instance"
		);

		$classPath = Yii::getPathOfAlias( $handlerName ) . '.php';
		if( ! is_file( $classPath ) ) {
			throw new CException( 'Invalid class path for handler ' . $handlerName . '. File not found at ' . $classPath );
		} else {
			$model = new AutomationRuleAction();
			$model->handler = $handlerName;
			$model->action_params = $handlerParams;
			$ruleHandlerInstance = $model->getHandlerObject();
			if( $ruleHandlerInstance instanceof AutomationActionHandler && method_exists($ruleHandlerInstance, 'describeActionByParams') ) {
				$res['html'] = $this->renderPartial('actionDescriptor', array(
					'handler'=>$ruleHandlerInstance,
					'handlerName'=>$handlerName,
					'params'=>$handlerParams,
					'uniqueId'=>$actionUniqueId,
				), true);
			}
		}

		echo CJSON::encode($res);
	}

	/* ACTIONS */

	/**
	 * Returns an array of step methods that this wizard has (ordered)
	 * @throws \CException
	 * @return array
	 */
	public function steps () {
		return array(
			array(
				'alias'   => 'nameAndDescription',
				'handler' => array( $this, 'nameAndDescription' ),
				'validator' => function ( $fromStep, $dataFromAllSteps, $params = array() ) {
					/* @var $model AutomationRule */
					if( ! ( $model = $params['model'] ) ) {
						throw new CException( 'Can not find AutomationRule model in validator params. Make sure it is passed to DoceboWizardController::_processStep()' );
					}
					if( ! $model->name || ! $model->description ) {
						Yii::app()->user->setFlash( 'error', Yii::t( 'automation', 'Name and description can not be blank' ) );

						return FALSE;
					}

					return TRUE;
				}
			),
			array(
				'alias'     => 'schedule',
				'handler'   => array( $this, 'schedule' ),
				'validator' => function ( $fromStep, $dataFromAllSteps, $params = array() ) {
					/* @var $model AutomationRule */
					if( ! ( $model = $params['model'] ) ) {
						throw new CException( 'Can not find AutomationRule model in validator params. Make sure it is passed to DoceboWizardController::_processStep()' );
					}

					if( ! $model->trigger_type ) {
						Yii::app()->user->setFlash( 'error', 'Invalid schedule type' );

						return FALSE;
					}

					if( ! in_array( $model->trigger_type, AutomationRule::$validTriggerTypes ) ) {
						Yii::app()->user->setFlash( 'error', 'Invalid schedule type' );

						return FALSE;
					}

					return TRUE;
				}
			),
			array(
				'alias'     => 'recipes',
				'handler'   => array( $this, 'recipes' ),
				'validator' => function ( $fromStep, $dataFromAllSteps, $params = array() ) {
					/* @var $model AutomationRule */
					if( ! ( $model = $params['model'] ) ) {
						throw new CException( 'Can not find AutomationRule model in validator params. Make sure it is passed to DoceboWizardController::_processStep()' );
					}

					$prevButton = isset($_POST['prev_button']) ? true : false;

					if(!isset($dataFromAllSteps['active-conditions']) && !$prevButton){
						Yii::app()->user->setFlash('error', Yii::t('automation', 'You must define conditions for the rule'));
						return FALSE;
					}

					return TRUE;
				}
			),
			array(
				'alias'     => 'actions',
				'handler'   => array( $this, 'actions' ),
				'validator' => function ( $fromStep, $dataFromAllSteps, $params = array() ) {
					/* @var $model AutomationRule */
					if( ! ( $model = $params['model'] ) ) {
						throw new CException( 'Can not find AutomationRule model in validator params. Make sure it is passed to DoceboWizardController::_processStep()' );
					}

					$prevButton = isset($_POST['prev_button']) ? true : false;

					if(!isset($dataFromAllSteps['active-actions']) && !$prevButton){
						Yii::app()->user->setFlash('error', Yii::t('automation', 'You must define actions for the rule'));
						return FALSE;
					}

					return TRUE;
				}
			),
			array(
				'alias'     => 'finished',
				'handler'   => array( $this, 'finished' ),
				'validator' => function ( $fromStep, $dataFromAllSteps, $params = array(), $goingBack = false) {
					/* @var $ruleModel AutomationRule */
					if( ! ( $ruleModel = $params['model'] ) ) {
						throw new CException( 'Can not find AutomationRule model in validator params. Make sure it is passed to DoceboWizardController::_processStep()' );
					}

					if( ! $goingBack ) {
						// Ok, finally SAVE the rule!

						$activeConditions = $dataFromAllSteps['active-conditions'];
						$activeActions    = $dataFromAllSteps['active-actions'];

						$isEdit = FALSE;

						if( $ruleModel->getIsNewRecord() ) {
							$ruleModel->active = 1;
						} else {
							$isEdit = TRUE;
						}

						if( $ruleModel->save() ) {

							// Populate with Yii events that this rule's conditions
							// will listen to. We keep them in a DB column on rule-level
							// to prevent instantiating of condition handler classes on each
							// request and retrieving its supported Yii events every time
							$_realtimeEventsList = array();

							if( $isEdit ) {
								// Delete old conditions and actions on rule edit
								// because we will now create new models based on the POSTed data
								try {
									AutomationRuleCondition::model()->deleteAllByAttributes( array( 'id_rule' => $ruleModel->id_rule ) );
									AutomationRuleAction::model()->deleteAllByAttributes( array( 'id_rule' => $ruleModel->id_rule ) );
								} catch ( Exception $e ) {
									Yii::log( 'Can not delete old conditions and actions for rule ' . $ruleModel->id_rule . ' due to ' . $e->getTraceAsString(), CLogger::LEVEL_WARNING );
								}
							}

							if( is_array( $activeConditions ) ) {
								foreach ( $activeConditions as  $instance ) {
									$instance = CJSON::decode($instance);
									$handler = $instance['handler'];
									$jsonInstanceOfHandler = $instance['params'];

									$conditionModel                   = new AutomationRuleCondition();
									$conditionModel->id_rule          = $ruleModel->id_rule;
									$conditionModel->handler          = $handler;
									$conditionModel->condition_params = $jsonInstanceOfHandler;

									try {
										$handlerObject = $conditionModel->getHandlerObject();
										if( $handlerObject && in_array( $ruleModel->trigger_type, $handlerObject->allowedTriggerTypes() ) ) {

											if( ! $conditionModel->save() ) {
												Yii::log(var_export($conditionModel->getErrors(),true), CLogger::LEVEL_ERROR);
											} else {
												// Populate this condition handler's events in the global
												// array of listened events for this rule

												try {
													if( $handlerObject && method_exists( $handlerObject, 'getTriggeringEvents' ) ) {
														$_realtimeEventsList = array_unique( array_merge( $_realtimeEventsList, $handlerObject->getTriggeringEvents() ) );
													}
												} catch ( Exception $e ) {

												}
											}
										} else {
											Yii::log( 'Invalid handler object for automation rule condition, or the rule\'s trigger type is not supported. Rule ' . $ruleModel->id_rule . ', condition handler ' . $handler, CLogger::LEVEL_ERROR );
										}
									} catch ( Exception $e ) {
										Yii::log( 'Can not get handler object for rule ' . $ruleModel->id_rule . ': ' . $e->getMessage() . ' ' . $e->getTraceAsString(), CLogger::LEVEL_ERROR );
									}

								}
							}
							if( is_array( $activeActions ) ) {
								foreach ( $activeActions as $instance) {
									$instance = CJSON::decode($instance);
									$handler = $instance['handler'];;
									$jsonInstanceOfHandler = $instance['params'];
									$actionModel                = new AutomationRuleAction();
									$actionModel->id_rule       = $ruleModel->id_rule;
									$actionModel->handler       = $handler;
									$actionModel->action_params = $jsonInstanceOfHandler;
									if( ! $actionModel->save() ) {
										Yii::log(var_export($actionModel->getErrors(),true), CLogger::LEVEL_ERROR);
									}
								}
							}

							if( $ruleModel->trigger_type == AutomationRule::SCHEDULE_TYPE_REALTIME ) {
								// Job created dynamically when a event is raised

								if( ! empty( $_realtimeEventsList ) ) {
									// Save the rule's list of listened events in comma
									// separated form for easier retrieval (and attaching listeners)
									$ruleModel->trigger_events = implode( ',', $_realtimeEventsList );
									if( ! $ruleModel->save( TRUE, array( 'trigger_events' ) ) ) {
										Yii::log( 'Can not save list of events listened for rule ' . $ruleModel->id_rule . ' : ' . print_r( $ruleModel->getErrors(), TRUE ), CLogger::LEVEL_ERROR );
									}
								}

								return TRUE;
							} elseif( $ruleModel->trigger_type == AutomationRule::SCHEDULE_TYPE_RECURRING ) {

								if( $ruleModel->scheduledJob ) {
									Yii::app()->scheduler->deleteJob( $ruleModel->scheduledJob, TRUE );
								}

								$jobParams            = array();
								$jobParams['id_rule'] = $ruleModel->id_rule;

								$job = Yii::app()->scheduler->createJob( 'AutomationRule', AutomationRuleJobHandler::id(), CoreJob::TYPE_RECURRING,
									$ruleModel->schedule_unit,
									1, // don't skip any "periods"
									$ruleModel->schedule_minute,  // minute is always 0
									$ruleModel->schedule_hour,
									Yii::app()->localtime->getLocalDateTimeZone()->getName(), // time zone
									$ruleModel->schedule_day_of_week,
									$ruleModel->schedule_day_of_month,
									NULL, // start date
									$jobParams
								);

								if( $job ) {

									// Successfully scheduled a recurring Sidekiq job
									// Save its ID with the Rule model so we can quickly access the job
									$ruleModel->id_job = $job->id_job;

									$ruleReSaved = $ruleModel->save();

									if( ! $ruleReSaved ) {
										Yii::log(var_export($ruleModel->getErrors(),true), CLogger::LEVEL_ERROR);
										throw new CException( 'Failed to save the scheduled job ID to the rule model' );
									}

									if( isset( $dataFromAllSteps['save-type'] ) ) {
										echo '<a class="auto-close" data-save-type="' . Yii::app()->htmlpurifier->purify( $dataFromAllSteps['save-type'] ) . '" data-rule-id="' . $ruleModel->id_rule . '"></a>';
										Yii::app()->end();
									}

									return $ruleReSaved;
								}
							}
						} else {
							Yii::log(var_export($ruleModel->getErrors(),true), CLogger::LEVEL_ERROR);
							Yii::app()->user->setFlash( 'error', 'Can not save rule' );

							return FALSE;
						}

					}

					return true;
				}

			),
		);
	}

	public function nameAndDescription ( $params = array() ) {
		$model     = ( isset( $params['model'] ) ? $params['model'] : new AutomationRule() );
		$stepAlias = 'nameAndDescription';

		$stepHtml = $this->renderPartial( 'nameAndDescription', array(
			'model' => $model,
		), TRUE, TRUE );

		$this->renderStep( $stepAlias, $stepHtml );
	}

	public function schedule ($params = array()) {
		$model     = ( isset( $params['model'] ) ? $params['model'] : new AutomationRule() );
		$stepAlias = 'schedule';

		// Only show schedule modes for which we have at least some conditional handlers:
		$allowedTriggerTypes = array();
		$conditionHandlers = AutomationRule::getConditionHandlers();
		foreach($conditionHandlers as $conditionHandler=>$handlerDescriptionString){
			$handlerInstance = Yii::createComponent( $conditionHandler );
			if($handlerInstance && method_exists($handlerInstance, 'allowedTriggerTypes')){
				$allowedTriggerTypes              = array_merge($allowedTriggerTypes, $handlerInstance->allowedTriggerTypes());
			}
		}

		if(empty($conditionHandlers)){
			// A very rare case, probably misconfiguration
			Yii::log('No condition handlers found. Misconfigured?', CLogger::LEVEL_ERROR);
			$this->renderStep( $stepAlias, 'Error: No condition handlers found' );
			return;
		}

		$stepHtml = $this->renderPartial( 'schedule', array(
			'model' => $model,
			'allowedTriggerTypes' => $allowedTriggerTypes,
		), TRUE, TRUE );

		$this->renderStep( $stepAlias, $stepHtml );
	}

	public function recipes ($params = array()) {
		$model     = ( isset( $params['model'] ) ? $params['model'] : new AutomationRule() );
		$stepAlias = 'recipes';

		$inData = $this->collectStepData();

		// On rule model edit, restore old data to UI compatible JSON format
		if( !$model->getIsNewRecord() && (!isset($inData['active-conditions']) && !$inData['active-conditions'])) {
			$restoreConditions = array();
			foreach ( $model->automationRuleConditions as $condition ) {
				/* @var $condition AutomationRuleCondition */
				$uuid = Docebo::generateUuid();
				$restoreConditions[$uuid] = CJSON::encode(array('handler'=>$condition->handler, 'params'=> CJSON::decode($condition->condition_params)));
			}

			$inData['active-conditions'] = $restoreConditions;
		}

		// Parse the data being POSTed across steps into actual UI elements
		// The wizard will preserve all active conditions and their settings,
		// submitted by POST from the recipes step in a param called 'active-conditions'
		// Each element from this array corresponds to a condition handler and its possible
		// insances (each handler may be used multiple times with different settings)
		// A sample element from the array:
		// [handler class]=> array( // <--e.g. AutomationApp.components.rules.UserInactivityRule
		//        [JSON encoded instance 1 settings of the above class]
		//        [JSON encoded instance 2 settings of the above class]
		// )
		$restoredConditionsUIelements = array();
		if( ! empty( $inData ) && isset( $inData['active-conditions'] ) ):
			foreach ( $inData['active-conditions'] as $handlerActiveConditions ):
				$data = CJSON::decode($handlerActiveConditions);
				$handlerName = $data['handler'];
				$handlerConditionRestored = $data['params'];

				// Create a temporary model to help resolve the actual handler object
				$handlerWrapperModel                   = new AutomationRuleCondition();
				$handlerWrapperModel->handler          = $handlerName;
				$handlerWrapperModel->condition_params = $handlerConditionRestored;
				$handler                               = $handlerWrapperModel->getHandlerObject();
				if($handler instanceof AutomationConditionHandler && method_exists( $handler, 'describeConditionByParams' ) ):
					if(in_array($model->trigger_type, $handler->allowedTriggerTypes())) {
						$restoredConditionsUIelements[] = $this->renderPartial( 'AutomationApp.views.ruleWizard.ruleDescriptor', array(
							'handler'     => $handler,
							'handlerName' => $handlerName,
							'params'      => $handlerConditionRestored,
							'uniqueId'    => 'condition-' . Docebo::randomHash(),
						), TRUE );
					}
				endif;
			endforeach;
		endif;

		$stepHtml = $this->renderPartial( 'recipes', array(
			'model'  => $model,
			'inData' => $inData,
			'restoredElements' => $restoredConditionsUIelements,
		), TRUE, TRUE );

		$this->renderStep( $stepAlias, $stepHtml );
	}

	public function actions ($params = array()) {
		$model     = ( isset( $params['model'] ) ? $params['model'] : new AutomationRule() );
		$stepAlias = 'actions';
		$inData = $this->collectStepData();

		// On edit, restore old data to UI
		if(!$model->getIsNewRecord() && (!isset($inData['active-actions']) && !$inData['active-actions'])){
			$restoredActions = array();
			foreach($model->automationRuleActions as $action) {
				/* @var $action AutomationRuleAction */
				if (!isset($restoredActions[$action->handler])) {
					$uuid = Docebo::generateUuid();
					$restoredActions[$uuid] = CJSON::encode(array('handler'=>$action->handler, 'params'=> CJSON::decode($action->action_params)));
				}
				$inData['active-actions'] = $restoredActions;
			}
		}

		$stepHtml = $this->renderPartial( 'actions', array(
			'model' => $model,
			'inData'=>$inData,
		), TRUE, TRUE );

		$this->renderStep( $stepAlias, $stepHtml );
	}

	public function finished ($params = array()) {
		$model     = ( isset( $params['model'] ) ? $params['model'] : new AutomationRule() );
		$stepAlias = 'finished';

		$stepHtml = $this->renderPartial( 'finished', array(
			'model' => $model,
		), TRUE, TRUE );

		$this->renderStep( $stepAlias, $stepHtml );
	}

	public function actionFinishGroupBranchSelection() {
		echo "<a class='auto-close success'></a>";
		Yii::app()->end();
	}
}