<?php
/**
 * AutomationApp:  Event driven plugin.
 *
 */
class AutomationAppModule extends CWebModule
{
	const RULE_JOB_HANDLER_ID = 'plugin.AutomationApp.components.AutomationRuleJobHandler';

	/**
	 * @see CModule::init()
	 */
	public function init()
	{
		// 1. Collect all event names from all active rule conditions
		// 2. If list non empty, install the shared listener for each of them
		$availableEvents = $this->getAvailableEvents();
		foreach($availableEvents as $eventName) {
			Yii::app()->event->on( $eventName, array( $this, 'triggerImmediateJob' ) );
		}
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		if(!Yii::app()->user->getIsPu()) {
			Yii::app()->mainmenu->addAppMenu('automation', array(
					'icon' => 'admin-ico settings',
					'app_icon' => 'fa-magic', // icon used in the new menu
					'link' => Docebo::createAdminUrl('//AutomationApp/AutomationApp/settings'),
					'label' => Yii::t('automation', 'Automation'),
					'settings' => ''
				),
				array()
			);
		}
	}

	/**
	 * Shared listener which will create immediate jobs for non synchronous rules
	 * @param DEvent $event
	 */
	public function triggerImmediateJob(DEvent $event) {
		// 1. Loop through all "active" automation rules triggered by this event
		// 2. Foreach rule
		// 		create a core_job with "params" containing the id_rule and the event params
		// 		schedule an immediate sidekiq job that will evaluate the conditions and, maybe, trigger the actions
		// 		use self::RULE_JOB_HANDLER_ID as the job handler

		$criteria = new CDbCriteria();
		$criteria->condition = "active AND trigger_type = :type AND trigger_events LIKE :event";
		$criteria->params = array(':type' => AutomationRule::SCHEDULE_TYPE_REALTIME, ':event' => "%" . $event->event_name . "%");
		$activeRules = AutomationRule::model()->findAll($criteria);

		foreach($activeRules as $rule) {
			$params = $event->params;

			Yii::log('Realtime rule '.$rule->id_rule.' triggered by event '.$event->event_name, CLogger::LEVEL_INFO);

			// We are reusing the existing "event params" array
			// to pass some internal fields that will later be needed
			// to simplify the process of automation rule triggering
			$params['event_name'] = $event->event_name;
			$params['id_rule'] = $rule->id_rule;

			Yii::app()->scheduler->createImmediateJob( 'AutomationRule', AutomationRuleJobHandler::id(), $params );
		}
	}

	private function getAvailableEvents() {
		$activeRules = Yii::app()->db->createCommand()
			->select('trigger_events')
			->from(AutomationRule::model()->tableName())
			->where('active')
			->andWhere('trigger_type = :realtime')
			->andWhere('trigger_events IS NOT NULL')
			->queryColumn(array(':realtime' => AutomationRule::SCHEDULE_TYPE_REALTIME));

		$availableEvents = array();
		foreach($activeRules as $events){
			$ruleEvents = strpos($events, ",") === false ? array($events) : explode(',', $events);
			foreach($ruleEvents as $event)
				$availableEvents[$event] = $event;
		}

		return $availableEvents;
	}

}
