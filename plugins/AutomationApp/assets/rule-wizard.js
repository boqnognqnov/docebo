/**
 * See the "$(function () {" block at the end of the file
 */

    var _restoreParamsToInput = function(input, params){
        var inputName = $(input).attr('name');

        if (inputName.indexOf("[") >= 0) {
            // This input name is "complex" meaning that its in array form like:
            // <input name="my-input[something][bla]">
            // To propertly parse it, we need to break down the name
            // into its parts, then check if each part corresponds to
            // a valid object property in the JSON params array
            // and finally, if found, extract the value of that
            // nested element in the JSON array and put it as value
            // for the input
            var parts = inputName.split("[");
            var topLevelInputName = parts[0];

            // Now try to extract the deeper array keys,
            // that is "something", "else" if the input name is
            // <input name="myname[something][else]">
            var regex = new RegExp(/\[([^\]]*)\]/g);
            var tmpMatches;

            var haystack = params[topLevelInputName];
            var needle = null;

            while ((tmpMatches = regex.exec(inputName)) != null) {
                var matchedArraySubkey = tmpMatches[1];

                if ($.type(haystack) === "object" && haystack.hasOwnProperty(matchedArraySubkey)) {
                    haystack = haystack[matchedArraySubkey];
                }

                if ($.type(haystack) !== "object") {
                    // Haystack is no longer a haystack. We found our needle
                    needle = haystack;
                }
            }

            if (needle !== null) {
				// We will handle the different input "types" with different approaches.
				// So let's check what's the current input type
                switch ($(input).attr('type')){
					case "hidden": break;
					case "checkbox":
					case "radio":		fillRadioNCheckboxButtons(input, inputName, needle);break;
					// We assume that the default one should be TEXT FIELD
					default : 			$(input).val(needle);

				}
            }
        } else {
			// A simple input, e.g. <input name="something">
			// Restore setting from JSON to input value
			// BUT if the input is not set ..... GET OUT
			if (!params.hasOwnProperty(inputName)) return;

			// We will handle the different input "types" with different approaches.
			// So let's check what's the current input type
			switch ($(input).attr('type')){
				case "checkbox":
				case "radio":		fillRadioNCheckboxButtons(input, inputName, params[inputName]);break;
				// We assume that the default one should be TEXT FIELD
				default : 			$(input).val(params[inputName]);

			}
        }
    };

	var fillRadioNCheckboxButtons = function(input, inputName, value){
		// IN CASE OF RADIO AND CHECKBOX BUTTONS
		// EXAMPLE: we may have 5 radioButtons/checkboxes with the same name and different value
		// so let's check if the current button's value is equals to last kept input value
		// if so let's restore its previous value
		if($(input).val() == value){
			$(input).attr('checked', 'checked');
			$("#" + $(input).attr("id") + "-styler").addClass("checked");
		}else{
			$(input).attr('checked', false);
			$("#" + $(input).attr("id") + "-styler").removeClass("checked");
		}
	};

var RuleWizardHelpers = {

	activeRecipes: [],

	stepScheduleHelpers: function () {
		var that = this;
		$(document).on('change', '#AutomationRule_schedule_unit', function () {
			var selectedScheduleUnit = $(this).val();

			switch (selectedScheduleUnit) {
				case 'day':
					$('.day-of-month').hide();
					$('.day-of-week').hide();
					$('.hour-of-day').show();
					break;
				case 'week':
					$('.day-of-month').hide();
					$('.day-of-week').show();
					$('.hour-of-day').show();
					break;
				case 'month':
					$('.day-of-month').show();
					$('.day-of-week').hide();
					$('.hour-of-day').show();
					break;
			}
		});

		$(document).on('change', '#tmp_schedule_hour', function () {
			var minutesInt = $(this).val();

			var hours = Math.floor(minutesInt / 60);
			var minutes = minutesInt % 60;

			$('#AutomationRule_schedule_hour').val(hours);
			$('#AutomationRule_schedule_minute').val(minutes);
		});


		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('#AutomationRule_trigger_type').length > 0) {

				var container = this;

				if ($('#AutomationRule_trigger_type:checked', container).val() === 'scheduled') {
					$('.schedule-interval-settings', container).show();
				} else {
					$('.schedule-interval-settings', container).hide();
				}

				$(container).on('change', '#AutomationRule_trigger_type', function () {
					if ($(this).val() === 'scheduled') {
						$('.schedule-interval-settings', container).show();
					} else {
						$('.schedule-interval-settings', container).hide();
					}
				});

				$('#AutomationRule_schedule_unit', container).trigger('change');
			}
		});

		/**
		 * Listen for Usersselector's FancyTree to coplete loading and take care of preselected branches
		 */
		$(document).on("users-selector-orgchart-loaded", "", function (e) {
			// Get saved selected branches (json string:   {"123":1, "345":2, ...}
			var rule_branches = $('#rule_branches').val();
			// Convert it to FancyTree style: [{key:123, selectState:1}, {key:345, selectState: 2}]
			var converted = that.convertRuleBranches(rule_branches);
			// Update fancy tree specific field name and trigger change 
			$('input[name="orgcharts_selected-json"]').val(converted).change();
		});
		


	},

	/**
	 * Convert branches selection from simple one to FancyTree style
	 * 
	 * {id1:state1, id2:state2,...}   ====>  [ {key:id1, selectState: state1}, {key:id2, selectState: state2}, ... ] 
	 */
	convertRuleBranches: function(branchesJson) {
		var converted = [];
		$.each(JSON.parse(branchesJson), function(key, value) {
			converted.push({
				key: parseInt(key),  // INTEGER!!!
				selectState: value
			});
		});
		return JSON.stringify(converted);
	},
	
	stepRecipesHelpers:  function () {

		var updateRecipeLine = function () {
			// Concatenate all active recipes in a single line
			var tmpHtml = "";
			var i = 0;
			var conditionsOperator = Yii.t('standard', 'AND');
			var currentOperatorRaw = $('#AutomationRule_conditions_operator:checked').val();
			switch (currentOperatorRaw) {
				case 'AND':
					conditionsOperator = Yii.t('standard', 'AND');
					break;
				case 'OR':
					conditionsOperator = Yii.t('standard', 'OR');
					break;
			}

			var activeConditionDescriptors = $('.rule-descriptor p');

			if(activeConditionDescriptors.length===0){
				var placeholderText = $('.recipes-placeholder').data('placeholder');
				$('.recipes-placeholder').html(placeholderText).addClass("empty");
			}else{
				activeConditionDescriptors.each(function(){
					var descriptor = $(this).html();

					tmpHtml += " " + descriptor;
					i++;
					if (i < activeConditionDescriptors.length) {
						tmpHtml += " " + "<b>" +conditionsOperator+"</b>";
					}
				});
				$('.recipes-placeholder').html(tmpHtml).removeClass("empty");
			}

		};

		var renderConditionSettings = function (handlerName) {
			$('.recipes-settings').addClass('loading').html('<div class="text-center"><img src="/themes/spt/images/ajax-loader10.gif" /></div>');
			$.post('?r=AutomationApp/ruleWizard/getConditionSettingsForm', {
				'handlerName': handlerName
			}, function (data) {
				$('.recipes-settings').html(data).removeClass('loading');

				// Trigger an event on the dropdown to let listeners know
				// that the settings form for this handler has loaded
				// (and it's ready to be dynamically populated)
				$('#conditions_list').trigger('condition-form-rendered');
			});
		};

		// Trigger the rule above when the step is just loaded also, to apply the effect immediately
		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('#conditions_list').length > 0) {
				$(this).find('#conditions_list').on('change', function () {
					var conditionHandlerClass = $(this).val();

					if (conditionHandlerClass == 0) {
						$('.step-recipes .add-recipe').hide();
						$('.step-recipes .recipes-settings').hide();
					} else {
						$('.step-recipes .add-recipe').show();
						$('.step-recipes .recipes-settings').show();

						renderConditionSettings(conditionHandlerClass);
					}
				});
				$(this).find('#conditions_list').trigger('change');

				updateRecipeLine();
			}
		});

		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('.cookbook-wrapper').length > 0) {
				// Hide the cookbook if there are no "ingredients" in this recipe
				if ($('.single-condition, .single-action', '.cookbook-wrapper').length === 0) {
					$('.cookbook-wrapper').hide();
				}
			}
		});

		$(document).on('click', '.step-recipes .add-recipe', function () {
			var conditionHandler = $('#conditions_list').val();

			var editedId = $('#conditions_list').data('editing');

			/** @deprecated **/
			var handlerParams = {};

			var virtualForm = $('<form></form>');
			$('input,select', '.recipes-settings').each(function () {
				var clonedInput = $(this).clone();
				clonedInput.val($(this).val());
				virtualForm.append(clonedInput);

				handlerParams[$(this).attr('name')] = $(this).val();
			});

			$.post('?r=AutomationApp/ruleWizard/convertConditionSettings', {
				'handlerName': conditionHandler,
				'params':      virtualForm.serialize(),
				'editing': editedId
			}, function (raw) {

				// Reset the top settings form so it is not in "edit mode" anymore
				$('#condition_settings').data('editing', '');
				// Delete the old container for the item being edited, since we
				// just added a fresh DOM element for it
				$('#'+editedId).remove();

				var data = $.parseJSON(raw);

				var uniqueId = data.uniqueId;
				var descriptor = data.descriptor;
				var html = data.html;

				$('.cookbook-wrapper').show();
				$('.cookbook-wrapper .inner').append(html);
				RuleWizardHelpers.activeRecipes.push({selector: uniqueId, descriptor: descriptor});

				updateRecipeLine();

				$('select#conditions_list option:first-child').attr("selected", "selected").trigger('change');
			});
		});

		// Editing one of the active conditions
		$(document).on('click', '.step-recipes .edit-condition', function () {

			// First show all containers
			$('.single-condition').show();

			// A container holds the UI elements (the description of the condition,
			// edit, delete buttons) and a data-params attribute with JSONed
			// settings of that condition. The code below will restore the
			// input values for that condition from this JSON, after the
			// condition's settings for loads (after the 'condition-form-rendered' event)
			var container = $(this).closest('.single-condition');

			// Temporarily hide this condition's container
			// while it is being edited
			container.hide();

			var handlerName = container.data('handler');
			var params = container.data('params');

			var uniqueId = container.attr('id');

			var conditionHandlersDropdown = $('#conditions_list');

			// Preserve the unique ID of the condition we are editing
			// in the conditions dropdown
			conditionHandlersDropdown.data('editing', uniqueId);

			// After the tiny settings form for this condition loads (ajax)
			// the 'condition-form-rendered' event will be triggered.
			// Run a single (one time) operation of restoring this
			// action's settings (the action we are editing) to the
			// inputs and dropdowns that serve as a settings form for this condition
			conditionHandlersDropdown.one('condition-form-rendered', function () {
				// .one() will make sure the event is only triggered once

				// For each available setting field of this condition type,
				// check if such setting is saved in the JSON settings field
				// for this condition and restore its value
				$('select,input', '.recipes-settings').each(function () {
					_restoreParamsToInput(this, params);
				});

				// Trigger an event to let potential handlers know when the
				// condition handler's settings have loaded
				$('.recipes-settings').trigger('afterRecipeSettingsRestore', params);

				if ($('.cookbook-wrapper .single-condition:visible').length === 0) {
					$('.cookbook-wrapper').hide();
				}

				// Updated the top line with descriptions of actions
				updateRecipeLine();
			});

			// Set the currently edited condition in the conditions list
			// to be the same "condition type" like the one we are currently editing
			conditionHandlersDropdown.val(handlerName).trigger('change')
		});

		$(document).on('click', '.step-recipes .delete-condition', function () {
			$(this).closest('.single-condition').remove();

			// No more "ingredients" in the cookbook, hide the cookbook container
			if ($('.cookbook-wrapper .single-condition:visible').length === 0) {
				$('.cookbook-wrapper').hide();
			}

			// Updated the top line with descriptions of actions
			updateRecipeLine();
		});
	},

	stepActionsHelpers: function () {

		var renderActionSettings = function (actionName) {
			$('.actions-settings').addClass('loading').html('<div class="text-center"><img src="/themes/spt/images/ajax-loader10.gif" /></div>');
			$.post('?r=AutomationApp/ruleWizard/getActionsSettingsForm', {
				'actionName': actionName
			}, function (data) {
				$('.actions-settings').html(data).removeClass('loading');

				$('#actions_list').trigger('action-form-rendered');
			});
		};

		// Trigger the rule above when the step is just loaded also, to apply the effect immediately
		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('#actions_list').length > 0) {
				$(this).find('#actions_list').on('change', function () {
					var actionHandlerClass = $(this).val();

					if (actionHandlerClass == 0) {
						$('.step-actions .add-action').hide();
					} else {
						$('.step-actions .add-action').show();

						renderActionSettings(actionHandlerClass);
						$('.actions-settings').show();
					}
				});
				$(this).find('#actions_list').trigger('change');
			}
		});

		$(document).on('click', '.step-actions .add-action', function () {
			var actionHandler = $('#actions_list').val();

			// If the action settings form was in "edit mode"
			var editedId = $('#actions_list').data('editing');

			var actionParams = {};
			var virtualForm = $('<form></form>');
			$('input,select', '.actions-settings').each(function () {
				var clonedInput = $(this).clone();
				clonedInput.val($(this).val());
				virtualForm.append(clonedInput);

				actionParams[$(this).attr('name')] = $(this).val();
			});

			$.post('?r=AutomationApp/ruleWizard/convertActionsSettings', {
				'handlerName': actionHandler,
				'params':      virtualForm.serialize()
			}, function (raw) {
				// Reset the top settings form so it is not in "edit mode" anymore
				$('#actions_list').data('editing', '');

				// Delete the old container for the item being edited, since we
				// will now add a fresh DOM element for it
				// (with the submitted edits to its settings)
				$('#'+editedId).remove();

				var data = $.parseJSON(raw);

				var uniqueId = data.uniqueId;
				var descriptor = data.descriptor;
				var html = data.html;

				$('.cookbook-wrapper').show();
				$('.cookbook-wrapper .inner').append(html);

				$('select#actions_list option:first-child').attr("selected", "selected").trigger('change');
				$('.actions-settings').hide();
			});
		});

		$(document).on('click', '.step-actions .edit-action', function () {
			$('.actions-settings').hide();

			// First show all containers
			$('.single-action').show();

			// A container holds both the text label description of what
			// this action does and a hidden JSON field with the settings
			// specific for this action
			var container = $(this).closest('.single-action');

			// Temporarily hide the container while it's being edited
			container.hide();

			var handlerName = container.data('handler');
			var params = container.data('params');

			var uniqueId = container.attr('id');

			var actionTypesDropdown = $('#actions_list');

			actionTypesDropdown.data('editing', uniqueId);
			actionTypesDropdown.val(handlerName);

			actionTypesDropdown.trigger('change').one('action-form-rendered', function () {

				// After the settings form for this handler has loaded,
				// restore the input and dropdown values from the stored JSON
				// settings field
				$('select,input', '.actions-settings').each(function () {
                    _restoreParamsToInput(this, params);
				});

				if ($('.cookbook-wrapper .single-action:visible').length === 0) {
					$('.cookbook-wrapper').hide();
				}
			});
		});

		$(document).on('click', '.step-actions .delete-action', function () {
			$(this).closest('.single-action').remove();

			// No more "ingredients" in the cookbook, hide the cookbook container
			if ($('.cookbook-wrapper .single-action').length === 0) {
				$('.cookbook-wrapper').hide();
			}
		});
	},

	stepFinishRuleWizard: function () {
		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('.step-finished').length > 0) {
				// Hide the next button in the last step
				$('.next_button').hide();
			}

			// If the last step is SUBMITTED (wizard finished)
			// get the save type
			var autoClose = $(this).find('.auto-close');
			if(autoClose.length>0){
				// see finished.php in the ruleWizard folder
				var saveType = autoClose.data('save-type');
				if(saveType && saveType==="save-and-run"){
					var ruleId = autoClose.data('rule-id');
					$.get('?r=AutomationApp/AutomationApp/triggerRule', {id: ruleId}, function(){
						Docebo.Feedback.show('success', Yii.t('standard', 'Rule triggered'));
					});
				}
			}
		});

		$(document).on('click', '.step-finished .save-and-run', function (e) {
			e.preventDefault();
			$(this).closest('form').append('<input type="hidden" name="save-type" value="save-and-run"/>').find('.next_button').click();
		});
		$(document).on('click', '.step-finished .save-and-back', function (e) {
			e.preventDefault();
			$(this).closest('form').append('<input type="hidden" name="save-type" value="save-and-back"/>').find('.next_button').click();
		});

		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('.auto-close').length > 0) {
				// Wizard ended, reload grid
				$('#automation-rules-grid').comboGridView('updateGrid');
			}
		});
	},

	/**
	 * Bootstro related (when there are no recipes or no actions yet)
	 * Show a small overlaying message with an arrow to "teach" the user
	 * what is next to do
	 */
	prepareBootstroTutorials: function(){
		$(document).on("dialog2.content-update", ".rule-wizard-dialog", function () {
			if ($(this).find('.step-recipes').length > 0) {
				if($(this).find('.cookbook-wrapper .single-condition:visible').length==0 && $('#conditions_list').val()==0){
					$('#conditions_list').css('margin-bottom', '120px');
					bootstro.start('bootstroNoRecipes', {
						onStopAll: function(){
							$('#conditions_list').css('margin-bottom', 'auto');
						}
					}, {
						placement: 'bottom',
						width: '600px'
					});
				}
			}

			if ($(this).find('.step-actions').length > 0) {
				if($(this).find('.cookbook-wrapper .single-action').length==0 && $('#actions_list').val()==0){
					$('#actions_list').css('margin-bottom', '120px');
					bootstro.start('bootstroNoActions', {
						onStopAll: function(){
							$('#actions_list').css('margin-bottom', 'auto');
						}
					}, {
						placement: 'bottom',
						width: '600px'
					});
				}
			}
		});
	}
};

$(function () {
	// Helpers for step 1:

	// Helpers for step 2:
	RuleWizardHelpers.stepScheduleHelpers();

	// Helpers for step 3:
	RuleWizardHelpers.stepRecipesHelpers();

	// Helpers for step 4:
	RuleWizardHelpers.stepActionsHelpers();

	// Helpers for step 5 (final):
	RuleWizardHelpers.stepFinishRuleWizard();

	// Common for step 3 and 4:
	RuleWizardHelpers.prepareBootstroTutorials();
});