<div id="custom-filters-audit-trail">

	<div class="select-wrapper">
		&nbsp;&nbsp;
		<label><?php echo Yii::t( 'audit_trail', 'Filter by event' ); ?></label>
		<?= CHtml::dropDownList( "custom_filter[audit_trail_event]", $_REQUEST['custom_filter']['audit_trail_event'], $availableEvents, array(
			'class' => 'trigger-form'
		) ) ?>
	</div>

	<div class="select-wrapper" style="margin-left: 30px;">
		<label><?php echo Yii::t( 'audit_trail', 'Date range' ); ?>: <?= Yii::t( 'standard', '_FROM' ) ?></label>
		<?= CHtml::textField( 'custom_filter[audit_trail_date_from]',
			Yii::app()->htmlpurifier->purify( $_REQUEST['custom_filter']['audit_trail_date_from'] ),
			array(
				'class' => 'datepicker trigger-form',
				'id'=>'audit_trail_date_from'
			) ); ?>
		<i class="i-sprite is-calendar-date trigger-date" data-target="#audit_trail_date_from"></i>
		&nbsp;&nbsp;&nbsp;<?= Yii::t( 'standard', '_TO' ) ?>
		<?= CHtml::textField( 'custom_filter[audit_trail_date_to]',
			Yii::app()->htmlpurifier->purify( $_REQUEST['custom_filter']['audit_trail_date_to'] ),
			array(
				'class' => 'datepicker trigger-form',
				'id'=>'audit_trail_date_to'
			) ); ?>
		<i class="i-sprite is-calendar-date trigger-date" data-target="#audit_trail_date_to"></i>
	</div>

</div>

<script>
	$(function () {

		var customFiltersForm = $('form.report-custom-filters');

		var container = customFiltersForm.find('#custom-filters-audit-trail');

		$('.datepicker', container).datepicker({dateFormat: 'yy-mm-dd'}).click(function () {
			$(this).datepicker('show');
		});

		$('.trigger-form', container).change(function () {
			customFiltersForm.submit();
		})

		$('.trigger-date', container).click(function(){
			var targetSelector = $(this).data('target');
			$(targetSelector).datepicker('show');
		});
	});
</script>