<?php
/**
 * AuditTrailApp:  Event driven logging plugin: create a listener here, raise the event somewhere and log whatever you need 
 * in the listener. 
 * 
 * AuditTrail App is totally event driven and as such, ideally, it should observe ALL possible events
 * raised by the LMS. Thus, event names listed in AuditTrail categories are not NEW or created here, they already have been
 * defined and/or used somewhere else. For example 'CourseDeleted', 'CourseCreated' events, they are already
 * used to trigger Notifications. AudtiTrail is just another event observer dedicated specifically to logging.
 *
 */
class AuditTrailAppModule extends CWebModule
{

	/**
	 * @see CModule::init()
	 */
	public function init()
	{

		// Set all categories's event(s) listeners
		// They are in a separate component (AuditTrailListeners)
		$allAvailCats = AuditTrailLog::getAvailableCategoriesAndEvents($getLabels = false);
		$listenersComponent = new AuditTrailListeners();
		foreach ($allAvailCats as $categoryName => $category)
			foreach ($category['events'] as $indexEvent => $event)
				Yii::app()->event->on($event["event_name"], array($listenersComponent, $event["event_name"]));
		
		
		
		// ------ Non-audit-trail-actions listeners ---------
		
		// A listener for AuditTrailLog CustomReport generation
		// The event is raised upon Custom Report Grid generation and during Custom Report export operation
		// Please check the code where 'OnGetReportDataProviderInfo' is raised for parameters passed and what is expected back
		$reportHelper = new AuditTrailLogCustomReportHelper();
		Yii::app()->event->on('OnGetReportDataProviderInfo', array($reportHelper, 'OnGetReportDataProviderInfo'));
		
		// Other, module-level listeners
		Yii::app()->event->on('getAuditTrailCategories', array($this, 'getAuditTrailCategories'));
		Yii::app()->event->on('getAdditionalCustomReportFilters', array($this, 'renderAuditTrailFilters'));
		
	}


	/**
	 * 
	 * @param DEvent $event
	 */
	public function getAuditTrailCategories(DEvent $event){
		if(!$event->return_value){
			$event->return_value = array();
		}

		$categories = AuditTrailLog::getAvailableCategoriesAndEvents();

		foreach($categories as $catIdUnused=>$catData){
			$event->return_value[$catIdUnused] = $catData;
		}
	}

	/**
	 * 
	 * @param DEvent $event
	 */
	public function renderAuditTrailFilters(DEvent $event){
		$learningReportFilter = $event->getParamValue('model'); /* @var $learningReportFilter LearningReportFilter */

		if($learningReportFilter->report_type_id == LearningReportType::AUDIT_TRAIL){

			$cs = Yii::app()->getClientScript();
			$cs->registerCoreScript('jquery.ui');
			$cs->registerCssFile	($cs->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
			$cs->registerCss('bla', '
			#custom-filters-audit-trail .datepicker{
				width: 110px;
				padding-left: 10px;
			}
			');

			Yii::app()->getController()->renderPartial('AuditTrailApp.views._custom_report_filters', array(
				'availableEvents'=>(array(''=>'All') + AuditTrailLog::getAvailableEventsSimpleArray()),
			));

			$event->preventDefault();
		}
	}
	
}
