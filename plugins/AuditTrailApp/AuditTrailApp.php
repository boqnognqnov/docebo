<?php

/**
 * Plugin for CoursecatalogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class AuditTrailApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'AuditTrailApp';

	// Override parent
	public static $HAS_SETTINGS = false;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function activate() {
		$model = LearningReportType::model()->findByAttributes(array('id'=>LearningReportType::AUDIT_TRAIL));
		if(!$model){
			$model = new LearningReportType();
			$model->id = LearningReportType::AUDIT_TRAIL;
			$model->title = 'Audit Trail';
			$model->save();
		}
		parent::activate();
	}

	public function deactivate() {
		LearningReportType::model()->deleteAllByAttributes(array('id'=>LearningReportType::AUDIT_TRAIL));
		parent::deactivate();
	}

}
