<?php
/**
 *
 * Action/Event listeners for AuditTrailLog
 *
 *
 */
class AuditTrailListeners {


    /**
     * Internally used
     *
     * @param DEvent $event
     * @return AuditTrailLog
     */
    protected function getAuditTrailModel(DEvent $event) {

        $model = new AuditTrailLog();

        // Any arbitrary data to save ?
        $data = $event->getParamValue('data'); // optional
        if ($data) {
            $model->json_data = CJSON::encode($data);
        }

        // User IP
        $model->ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

        // Action, usually the event name
        $model->action = $event->event_name;

        $idUser = $event->getParamValue('idUser');
        if (empty($idUser) || (intval($idUser) == 0)) {
            $idUser = Yii::app()->user->id;
        }

        $model->idUser = $idUser;

        return $model;

    }

    /**
     * Internally used
     *
     * @param AuditTrailLog $model
     */
    protected function saveModel($model)
    {
        if (!$model->save()) {
            Yii::log('Error while saving audit trail.', CLogger::LEVEL_ERROR);
            Yii::log(var_export($model->errors, true).' - '.var_export($model->action, true), CLogger::LEVEL_ERROR);
        }
    }


    /**
     *
     * @param DEvent $event
     */
    public function onPuBuySeats(DEvent $event){

    	$model = $this->getAuditTrailModel($event);

    	$idPowerUser = $event->getParamValue('idPowerUser');
    	$idCourse = $event->getParamValue('idCourse');

    	$model->idUser = $idPowerUser;
    	if($idCourse)
    		$model->target_course = $idCourse;

    	// Save
    	$this->saveModel($model);

    }

    /**
     *
     * @param DEvent $event
     */
    public function onPuSeatConsume(DEvent $event){

    	$model = $this->getAuditTrailModel($event);

    	$idPowerUser = $event->getParamValue('idPowerUser');
    	$idCourse = $event->getParamValue('idCourse');
    	$idUser = $event->getParamValue('idUser');
    	$model->idUser = $idPowerUser;
    	if($idCourse)
    		$model->target_course = $idCourse;

    	if($idUser)
    		$model->target_user = $idUser;

    	// Save
    	$this->saveModel($model);

    }

    /**
     *
     * @param DEvent $event
     */
    public function onPuSeatRestore(DEvent $event){

    	$model = $this->getAuditTrailModel($event);

    	$idPowerUser = $event->getParamValue('idPowerUser');
    	$idCourse = $event->getParamValue('idCourse'); // optional
    	$idUser = $event->getParamValue('idUser'); // optional

    	if(!$idPowerUser){
    		Yii::log('Can not log audit trail because the power user ID is not set', CLogger::LEVEL_ERROR, __CLASS__);
    		return;
    	}

    	$model->idUser = $idPowerUser;

    	if($idCourse)
    		$model->target_course = $idCourse;

    	if($idUser)
    		$model->target_user = $idUser;

    	// Save
    	$this->saveModel($model);

    }


	/**
	 *
	 * @param DEvent $event
	 */
	public function AdminAddedExtraSeatsToPu(DEvent $event){

		$model = $this->getAuditTrailModel($event);

		$superAdminId = $event->getParamValue('adminId');
		$adminUserName = $event->getParamValue('adminUserName');
		$addedExtraSeats = $event->getParamValue('addedExtraSeats');
		$puModel = $event->getParamValue('user');
		$puUsername = $event->getParamValue('puUsername');
		$courseModel = $event->getParamValue('course');

		$idPowerUser = ($puModel) ? $puModel->puser_id : '';
		$idCourse = ($courseModel) ? $courseModel->idCourse : '';

		$model->idUser = $superAdminId;

		if($idCourse)
			$model->target_course = $idCourse;

		if($idPowerUser)
			$model->target_user = $idPowerUser;

		// Save additional info in json as course/PU can be deleted later
		$model->json_data = json_encode(array(
			'course'        => $courseModel->attributes,
			'powerUser'     => $puModel->attributes,
			'extra_seats'   => $addedExtraSeats,
			'adminUserName' => $adminUserName,
			'puUsername' => $puUsername
		));

		// Save
		$this->saveModel($model);
	}

	/**
	 *
	 * @param DEvent $event
	 */
	public function AdminRemovedExtraSeatsToPu(DEvent $event){

		$model = $this->getAuditTrailModel($event);

		$superAdminId = $event->getParamValue('adminId');
		$adminUserName = $event->getParamValue('adminUserName');
		$removedExtraSeats = $event->getParamValue('remSeatsCount');
		$puModel = $event->getParamValue('user');
		$puUsername = $event->getParamValue('puUsername');
		$courseModel = $event->getParamValue('course');

		$idPowerUser = ($puModel) ? $puModel->puser_id : '';
		$idCourse = ($courseModel) ? $courseModel->idCourse : '';

		$model->idUser = $superAdminId;

		if($idCourse)
			$model->target_course = $idCourse;

		if($idPowerUser)
			$model->target_user = $idPowerUser;

		// Save additional info in json as course/PU can be deleted later
		$model->json_data = json_encode(array(
			'course'        => $courseModel->attributes,
			'powerUser'     => $puModel->attributes,
			'extra_seats'   => $removedExtraSeats,
			'adminUserName' => $adminUserName,
			'puUsername' => $puUsername
		));

		// Save
		$this->saveModel($model);
	}

    /**
     *
     * @param DEvent $event
     */
    public function CourseDeleted(DEvent $event)
    {

        $model = $this->getAuditTrailModel($event);

        $courseModel = $event->getParamValue('course');
        $model->target_course = $courseModel->idCourse;

        // Save additional info in json as course won't be available anymore during reports (it is deleted isn't it?)
        $model->json_data = json_encode(array(
            'course' => $courseModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
	public function ILTSessionDeleted(DEvent $event)
	{

		$model = $this->getAuditTrailModel($event);
		$session_data = $event->getParamValue('session_data');
		if(isset($session_data['date_begin']))
			$session_data['date_begin'] = Yii::app()->localtime->fromLocalDateTime($session_data['date_begin']);
		if(isset($session_data['date_end']))
			$session_data['date_end'] = Yii::app()->localtime->fromLocalDateTime($session_data['date_end']);

		$model->target_course = $event->getParamValue('course_id');
		$model->id_target = $session_data['id_session'];

		// Save additional info in json
		$model->json_data = json_encode(array(
			'sessionData'	=> 	$session_data,
		));

		// Save
		$this->saveModel($model);
	}

	/**
	 *
	 * @param DEvent $event
	 */
	public function WebinarSessionDeleted(DEvent $event)
	{

		$model = $this->getAuditTrailModel($event);
		$session_data = $event->getParamValue('session_data');

		$model->target_course = $event->getParamValue('course_id');
		$model->id_target = $session_data['id_session'];

		// Save additional info in json
		$model->json_data = json_encode(array(
			'sessionData'	=> 	$session_data,
		));

		// Save
		$this->saveModel($model);
	}

	/**
	 *
	 * @param DEvent $event
	 */
    public function NewCourse(DEvent $event)
    {

        $model = $this->getAuditTrailModel($event);

        $courseModel = $event->getParamValue('course');
        $model->target_course = $courseModel->idCourse;

        // Save additional info in json as course can be deleted later
        $model->json_data = json_encode(array(
            'course' => $courseModel->attributes
        ));


        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function CoursePropChanged(DEvent $event)
    {

        $model = $this->getAuditTrailModel($event);

        $courseModel = $event->getParamValue('course');
        $model->target_course = $courseModel->idCourse;

        // Save additional info in json
        $model->json_data = json_encode(array(
            'course' => $courseModel->attributes
        ));


        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function NewCategory(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the category model
        $categoryModel = $event->getParamValue('category');
        //set the category id as id_target
        $model->id_target = $categoryModel->idCategory;

        // Save additional info in json
         $model->json_data = json_encode(array(
            'category'  => $categoryModel->coursesCategoryTree->coursesCategoryTranslated->attributes
         ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function RemoveCategory(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the category model
        $categoryAttributes = $event->getParamValue('category');
        //set the category id as id_target
        $model->id_target = !empty($categoryAttributes['idCategory']) ? $categoryAttributes['idCategory'] : null;

        // Save additional info in json
        $model->json_data = json_encode(array(
            'category'  => $categoryAttributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UpdateCategory(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the category model
        $categoryModel = $event->getParamValue('category');
        //set the category id as id_target
        $model->id_target = $categoryModel->idCategory;

        // Save additional info in json
        $model->json_data = json_encode(array(
            'category'  => $categoryModel->coursesCategoryTree->coursesCategoryTranslated->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    public function ResetUserLoStatus(DEvent $event){

        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        // Get the LearningOrganization model
        /* @var $learningOrganization LearningOrganization */
        $learningOrganization = $event->getParamValue('model');

        // Set the course id as target_course
        $model->target_course = $event->getParamValue('id_course');

        // Set the lo id as id_target
        $model->id_target = $learningOrganization->idOrg;

        // Set the user id as target_user
        $model->target_user = $event->getParamValue('id_user');

        $user = $event->getParamValue('user');

        // Add more information in json
        $model->json_data = json_encode(array(
            'learningObject'	=> 	$learningOrganization->attributes,
            'user' => $user->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function AddedCatalogue(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        // Get the catalogue model
        /* @var $catalogueModel LearningCatalogue */
        $catalogueModel = $event->getParamValue('model');

        // Set the catalogue id as id_target
        $model->id_target = $catalogueModel->idCatalogue;

        // Add more information in json
        $model->json_data = json_encode(array(
        	'catalogue'		=> 	$catalogueModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function EditedCatalogue(DEvent $event)
    {
        //as the code is identical to AddedCatalogue(), so use it
        self::AddedCatalogue($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function DeletedCatalogue(DEvent $event)
    {
        //as the code is identical to AddedCatalogue(), so use it
        self::AddedCatalogue($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function NewLoAddedCourse(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the course
        $courseModel            = $event->getParamValue('course');
        $model->target_course   = $courseModel->idCourse;

        //set the LO id as id_target
        $learningObjectModel    = $event->getParamValue('learningobject');
        $model->id_target       = $learningObjectModel->idOrg;

        // Add more information in json
        $model->json_data = json_encode(array(
            'learningObject'    => 	$learningObjectModel->attributes,
            'course'            => $courseModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function EditedCourseLo(DEvent $event)
    {
        //as the code is identical to NewLoAddedCourse(), so use it
        self::NewLoAddedCourse($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function DeletedCourseLo(DEvent $event)
    {
        //as the code is identical to NewLoAddedCourse(), so use it
        self::NewLoAddedCourse($event);
    }


    /**
     *
     * @param DEvent $event
     */
    public function CourseFileAdded(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the learninCourseFile model
        $courseFileModel = $event->getParamValue('courseFile');

        //set the course id
        $model->target_course = $courseFileModel->id_course;
        $courseModel = LearningCourse::model()->findByPk($courseFileModel->id_course);

        //set the file id
        $model->id_target = $courseFileModel->id_file;

        //get the idUser
        $model->target_user = $courseFileModel->idUser;

        // Add more information in json
        $model->json_data = json_encode(array(
            'courseFile'    => 	$courseFileModel->attributes,
            'course'        =>  $courseModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }


    /**
     *
     * @param DEvent $event
     */
    public function CourseFileDownloaded(DEvent $event)
    {
        //as the code is identical to CourseFileAdded(), so use it
        self::CourseFileAdded($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function AddedLearningCoursepath(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get learningCoursepath model
        $learningCoursepathModel = $event->getParamValue('model');

        //get the learning coursepath id
        $model->id_target = $learningCoursepathModel->id_path;

        // Add more information in json
        $model->json_data = json_encode(array(
            'learningCoursepath'        =>  $learningCoursepathModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UpdatedLearningCoursepath(DEvent $event)
    {
        //as the code is identical to AddedLearningCoursepath(), so use it
        self::AddedLearningCoursepath($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function DeletedLearningCoursepath(DEvent $event)
    {
        //as the code is identical to AddedLearningCoursepath(), so use it
        self::AddedLearningCoursepath($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function CourseAddedToCoursepath(DEvent $event)
    {
        $model = $this->getAuditTrailModel($event);

        //get course
        $course = $event->getParamValue('targetCourse');
        $model->target_course = $course->idCourse;

        //get coursepath
        $coursepath = $event->getParamValue('coursepath');
        $model->id_target = $coursepath->id_path;
        $model->json_data =  json_encode(array(
            'course'      =>  $course->name,
            'coursepath'    =>  $coursepath->path_name,
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function CourseRemovedFromCoursepath(DEvent $event)
    {
        $model = $this->getAuditTrailModel($event);

        //get course
        $course = $event->getParamValue('targetCourse');
        $model->target_course = $course->idCourse;

        //get coursepath
        $coursepath = $event->getParamValue('coursepath');
        $model->id_target = $coursepath->id_path;
        $model->json_data =  json_encode(array(
            'course'      =>  $course->name,
            'coursepath'    =>  $coursepath->path_name,
        ));

        // Save
        $this->saveModel($model);
    }



    /**
     *
     * @param DEvent $event
     */
    public function CustomReportAdded(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the custom report id_filter
        $customReportModel = $event->getParamValue('model');
        $model->id_target = $customReportModel->id_filter;

        // Add more information in json
        $model->json_data = json_encode(array(
            'customReport' =>  $customReportModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function CustomReportDeleted(DEvent $event)
    {
        //as the code is identical to CustomReportAdded(), so use it
        self::CustomReportAdded($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function ToggledPublicCustomReport(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get the customReport model
        $customReportModel = $event->getParamValue('model');
        $model->json_data =  json_encode(array(
            'customReport' =>  $customReportModel->attributes
        ));

        //set the custom report id_filter
        $model->id_target = $customReportModel->id_filter;

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function NewUserCreated(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('user');
        $model->target_user = $target_user->idst;

        //get the user model
        $userModel = $event->getParamValue('user');
        $model->json_data =  json_encode(array(
            'user' =>  $userModel->attributes
        ));

        // Save
        $this->saveModel($model);
    }

	public function UserSuspended(DEvent $event){

		//set regular auditTrail content
		$model = $this->getAuditTrailModel($event);

		$userModel = $event->getParamValue('user');

		$model->target_user = $userModel->idst;

		$model->json_data =  json_encode(array(
			'user' =>  $userModel->attributes
		));

		// Save
		$this->saveModel($model);
	}
	public function UserUnSuspended(DEvent $event){
		// Same logic
		$this->UserSuspended($event);
	}

    /**
     *
     * @param DEvent $event
     */
    public function UserModified(DEvent $event)
    {
        //as the code is identical to NewUserCreated(), so use it
        self::NewUserCreated($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserDeleted(DEvent $event)
    {
        //as the code is identical to NewUserCreated(), so use it
        self::NewUserCreated($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function NewUserCreatedSelfreg(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('target_user');
        $user = $event->getParamValue('user');

        $model->idUser = $target_user->idst;

        //get the user model
        $model->json_data =  json_encode(array(
            'user' =>  $target_user->attributes,
            'approved_by' => $user->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserSubscribedCourse(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('user');
        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        //get course
        $target_course = $event->getParamValue('course');
        $model->target_course = $target_course->idCourse;

		$courseuser = LearningCourseuser::model()->findByAttributes(array('idCourse' => $model->target_course, 'idUser' => $model->idUser));

        $model->json_data =  json_encode(array(
            'user'			=> $target_user->attributes,
            'course'		=> $target_course->attributes,
			'courseuser'	=> $courseuser->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserUnsubscribed(DEvent $event)
    {
        //as the code is identical to UserSubscribedCourse(), so use it
        self::UserSubscribedCourse($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserCourselevelChanged(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('user');
        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        //get course
        $target_course = $event->getParamValue('course');
        $model->target_course = $target_course->idCourse;

        $courseUser = $event->getParamValue('courseuser');

        $model->json_data =  json_encode(array(
            'user'      =>  $target_user->attributes,
            'course'    => $target_course->attributes,
            'courseUser'    => $courseUser->attributes
        ));

        // Save
        $this->saveModel($model);
    }

	/**
	 *
	 * @param DEvent $event
	 */
	public function UserEnrollmentStatusChanged(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('user');
        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        //get course
        $target_course = $event->getParamValue('course');
        $model->target_course = $target_course->idCourse;

        $courseUser = $event->getParamValue('courseuser');

        $model->json_data =  json_encode(array(
            'user'      =>  $target_user->attributes,
            'course'    => $target_course->attributes,
            'courseUser'    => $courseUser->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserSubscribedGroup(DEvent $event)
    {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('user');
        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        //get group
        $target_group = $event->getParamValue('group');
        $model->id_target = $target_group->idst;

        $model->json_data =  json_encode(array(
            'user'      =>  $target_user->attributes,
            'group'    => $target_group->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserRemovedFromGroup (DEvent $event)
    {
        //as the code is identical to UserSubscribedGroup(), so use it
        self::UserSubscribedGroup($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserAssignedToBranch(DEvent $event)
    {
        $model = $this->getAuditTrailModel($event);

        //get user
        $model->target_user = $event->getParamValue('targetUser');
        $model->idUser = !empty($model->idUser) ? $model->idUser : $model->target_user;

        //get group
        $model->id_target = $event->getParamValue('BranchIdOrg');
        $model->json_data =  json_encode(array(
            'user'      =>  CoreUser::getFullNameById($model->target_user),
            'branch'    =>  CoreOrgChartTree::getTranslatedName($model->id_target),
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserRemovedFromBranch(DEvent $event)
    {
        $model = $this->getAuditTrailModel($event);

        //get user
        $model->target_user = $event->getParamValue('targetUser');
        $model->idUser = !empty($model->idUser) ? $model->idUser : $model->target_user;

        //get group
        $model->id_target = $event->getParamValue('BranchIdOrg');
        $model->json_data =  json_encode(array(
            'user'      =>  CoreUser::getFullNameById($model->target_user),
            'branch'    =>  CoreOrgChartTree::getTranslatedName($model->id_target),
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function ILTSessionUserEnrolled(DEvent $event) {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user_id = $event->getParamValue('user');
        $target_user = CoreUser::model()->findByPk($target_user_id);
        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        //get session
        $target_session_id = $event->getParamValue('session_id') ?: $event->getParamValue('session');
        $target_session = LtCourseSession::model()->with('learningCourseSessionDates')->findByPk($target_session_id);

        $session_dates = array();

        foreach($target_session->learningCourseSessionDates as $sessionDate) {
            $session_dates["{$sessionDate->id_session}{$sessionDate->day}"] = $sessionDate->attributes;
            $sessionDateLocation = LtLocation::model()->with('country')->findByPk($sessionDate->id_location);
            $session_dates["{$sessionDate->id_session}{$sessionDate->day}"]['location'] = $sessionDateLocation->attributes;
//            $session_dates["{$sessionDate->id_session}{$sessionDate->day}"]['location']['country_name'] = $sessionDateLocation->country->name_country; // if needed just uncomment
//            $session_dates["{$sessionDate->id_session}{$sessionDate->day}"]['location']['iso_code_country'] = $sessionDateLocation->country->iso_code_country; // if needed just uncomment
        }
        $session_dates = array_values($session_dates);

        $model->id_target = $target_session_id;

        //get course
        $target_course = LearningCourse::model()->findByPk($target_session->course_id);
        $model->target_course = $target_course->idCourse;

        $model->json_data =  json_encode(array(
            'user'              => $target_user->attributes,
            'course'            => $target_course->attributes,
            'session'           => $target_session->attributes,
            'session_dates'     => $session_dates,
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function ILTSessionUserUnenrolled(DEvent $event) {
        self::ILTSessionUserEnrolled($event);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserEnrolledInWebinarSession(DEvent $event) {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $enrollment = $event->getParamValue('enrollment');

        $target_user = CoreUser::model()->findByPk($enrollment->id_user);
        $target_session = WebinarSession::model()->findByPk($enrollment->id_session);
        $target_course = LearningCourse::model()->findByPk($target_session->course_id);

        $model->target_user = $target_user->idst;
        $model->idUser = !empty($model->idUser) ? $model->idUser : $target_user->idst;

        $model->id_target = $target_session->id_session;

        $model->target_course = $target_course->idCourse;

        $model->json_data =  json_encode(array(
            'user'              => $target_user->attributes,
            'course'            => $target_course->attributes,
            'session'           => $target_session->attributes
        ));

        // Save
        $this->saveModel($model);
    }

    /**
     *
     * @param DEvent $event
     */
    public function UserUnenrolledFromWebinarSession(DEvent $event) {
        self::UserEnrolledInWebinarSession($event);
    }

    public function UserWaitingApprovalSubscription(DEvent $event) {
        //set regular auditTrail content
        $model = $this->getAuditTrailModel($event);

        //get user
        $target_user = $event->getParamValue('userwaiting');

        // if the CoreUserTemp model already exists then update it don't create another row in the table...
        $oldModel = $model->model()->findByAttributes(array('idUser' => $target_user->idst));
        if($oldModel) $model = $oldModel;

        $model->idUser = $target_user->idst;

        //get the user model
        $model->json_data =  json_encode(array(
            'user' =>  $target_user->attributes
        ));

        // Save
        $this->saveModel($model);
    }

}
