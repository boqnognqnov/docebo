<?php
/**
 * Audit Trail Custom report generation helper
 *
 */
class AuditTrailLogCustomReportHelper {

	// Audit trail
	const F_AUDIT_TRAIL_ID_USER 			= "audit_trail.idUser";
	const F_AUDIT_TRAIL_ID_TARGET 			= "audit_trail.id_target";
	const F_AUDIT_TRAIL_ACTION 				= "audit_trail.action";
	const F_AUDIT_TRAIL_TARGET_COURSE 		= "audit_trail.target_course";
	const F_AUDIT_TRAIL_TARGET_USER 		= "audit_trail.target_user";
	const F_AUDIT_TRAIL_JSON 				= "audit_trail.json_data";
	const F_AUDIT_TRAIL_IP 					= "audit_trail.ip";
	const F_AUDIT_TRAIL_TIMESTAMP 			= "audit_trail.timestamp";


	/**
	 *
	 * @var LearningReportFilter
	 */
	protected $filterModel;

	/**
	 *
	 * @var array
	 */
	protected $customFilter;


	protected $pagination;


	/**
	 * Event handler for Audit Trail Custom report.
	 *
	 * @param DEvent $event
	 */
	public function OnGetReportDataProviderInfo(DEvent $event) {

		$reportType = $event->getParamValue('reportType'); /* @var $reportType The type of the custom report */
		if ($reportType == LearningReportType::AUDIT_TRAIL) {

			$this->filterModel = $event->params['reportFilterModel'];
			$this->customFilter = $event->params['customFilter'];
			$this->pagination = $event->params['pagination'];

			$dataProvider = $this->sqlDataProvider();
			$gridColumns = $this->getGridColumns();

			$event->params['dataProvider'] = $dataProvider;
			$event->params['gridColumns'] = $gridColumns;
		}

	}


	/**
	 * Array of translated specific field/column names used in custom report grids and exports.
	 * Non-specific ones are taken from core report generation code.
	 *
	 * @return array
	 */
	protected function fieldNames() {
		$result = array(
			self::F_AUDIT_TRAIL_ID_USER			=> Yii::t('audit_trail', 'User ID'),
			self::F_AUDIT_TRAIL_ID_TARGET		=> Yii::t('audit_trail', 'Object ID'),
			self::F_AUDIT_TRAIL_ACTION 			=> Yii::t('audit_trail', 'Action'),
			self::F_AUDIT_TRAIL_TARGET_COURSE	=> Yii::t('audit_trail', 'Course ID'),
			self::F_AUDIT_TRAIL_TARGET_USER		=> Yii::t('audit_trail', 'User ID'),
			self::F_AUDIT_TRAIL_JSON 			=> Yii::t('audit_trail', 'Event details'),
			self::F_AUDIT_TRAIL_IP				=> Yii::t('audit_trail', 'IP'),
			self::F_AUDIT_TRAIL_TIMESTAMP		=> Yii::t('audit_trail', 'Date'),
		);
		return $result;
	}


	/**
	 * Get a single field name, translated
	 *
	 * @param string $field
	 * @return string
	 */
	protected function fieldName($field) {
		$names = $this->fieldNames();
		return isset($names[$field]) ? $names[$field] : '';
	}

	/**
	 * SQL Data provider for custom report
	 *
	 * @param LearningReportFilter $filterModel
	 * @param CPagination $pagination
	 * @param array $customFilter
	 *
	 * @return CSqlDataProvider
	 */
	protected function sqlDataProvider() {

		$filterModel 	= $this->filterModel;
		$customFilter	= $this->customFilter;
		$pagination		= $this->pagination;

		/* @var $commandBase CDbCommand */
		/* @var $commandCounter CDbCommand */

		$languageCode = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

		$auditTrailEventsIncluded = $this->getActionsFilterSelection();

		// Get selected users from filter
		$users  = $filterModel->getUsersFromFilter();

        $allUsers = Yii::app()->getDb()->createCommand("
            SELECT idst FROM " . CoreUser::model()->tableName() . "
            UNION
            SELECT idst FROM " . CoreUserTemp::model()->tableName() . "
        ")->queryColumn();

    	// Get groups from filter (directly selected and from Orgcharts)
		$groups = $filterModel->getGroupsFromFilter();

		$courses = $filterModel->getCoursesFromFilter();

		$params = array();


		$commandBase = Yii::app()->getDb()->createCommand()
		->from(AuditTrailLog::model()->tableName().' audit_trail')

		// Get the course to which the action was done in this audit trail
		->leftJoin(LearningCourse::model()->tableName().' course', 'audit_trail.target_course=course.idCourse')

		// Get the user to whom the action was done in this audit trail
		->leftJoin(CoreUser::model()->tableName().' targetUser', 'audit_trail.target_user=targetUser.idst')

		// And join the user that triggered the audit trail
		->leftJoin(CoreUser::model()->tableName().' user', 'user.idst=audit_trail.idUser')
		->leftJoin(CoreUserTemp::model()->tableName().' userTemp', 'userTemp.idst=audit_trail.idUser AND userTemp.confirmed=1');

		if(!empty($auditTrailEventsIncluded))
			$commandBase->andWhere(array('IN', 'audit_trail.action', $auditTrailEventsIncluded));

		if(count($groups) > 0 || count($users) > 0)
		{
			$tmpUsersTableName = 'tmp_selected_users';
			Yii::app()->db->createCommand("
				DROP TABLE IF EXISTS $tmpUsersTableName;
				CREATE TEMPORARY TABLE $tmpUsersTableName (idst int(11) NOT NULL, PRIMARY KEY(idst), INDEX(idst));
			")->execute();

			if(count($users) > 0)
				Yii::app()->db->createCommand("
					INSERT INTO $tmpUsersTableName VALUES (" . implode('),(', $users) . ")
				")->execute();
			if(count($groups) > 0)
				Yii::app()->db->createCommand("
					INSERT IGNORE INTO $tmpUsersTableName (SELECT DISTINCT idstMember FROM core_group_members WHERE idst IN (" . implode(',', $groups) . "))
				")->execute();

			// If the "User confirmed email and waiting for approval" event is selected in the Audit Trail report
			// creation wizard, users that triggered this event are not yet in `core_user` but in `core_user_temp`
			// Make sure we exclude the user selection filter for this report and always include all audit trail
			// logs for this event in the report
			if(in_array(CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION, $auditTrailEventsIncluded)) {
				$commandBase->leftJoin($tmpUsersTableName, $tmpUsersTableName.'.idst = audit_trail.idUser');
				$commandBase->andWhere('
                    (audit_trail.action=:waitingApprovalEvent AND audit_trail.idUser IN(' . implode(',', $allUsers) . '))
                     OR
                    (audit_trail.action != :waitingApprovalEvent AND '.$tmpUsersTableName.'.idst IS NOT NULL)
                ');
				$params[':waitingApprovalEvent'] = CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION;
			}else{
				$commandBase->join($tmpUsersTableName, $tmpUsersTableName.'.idst = audit_trail.idUser');
			}
		}elseif(count($users) == 0) {
            // PREVENTS showint the results if there are no users selected in the user filter..
            $commandBase->andWhere('0');
		}

		$commandBase->group(array('audit_trail.id'));

		if($customFilter){
			if(isset($customFilter['audit_trail_event']) && $customFilter['audit_trail_event']){
				$commandBase->andWhere('audit_trail.action=:customFilterAction');
				$params[':customFilterAction'] = $customFilter['audit_trail_event'];
			}
			if(isset($customFilter['audit_trail_date_from']) && $customFilter['audit_trail_date_from']){
				// The user enters the date in his local timezone (UTC may be -/+1 day from the selected date in some cases)
				// So we take the "midnight, start of day" of the user's selected date and convert that moment to UTC
				$localtimeBeginTimestamp = date(Yii::app()->localtime->getPHPLocalDateTimeFormat(), strtotime($customFilter['audit_trail_date_from'].' 00:00:00'));
				$utcBeginTimestamp = Yii::app()->localtime->fromLocalDateTime($localtimeBeginTimestamp);
				$commandBase->andWhere('audit_trail.timestamp >= :from');
				$params[':from'] = $utcBeginTimestamp;
			}
			if(isset($customFilter['audit_trail_date_to']) && $customFilter['audit_trail_date_to']){
					// The user enters the date in his local timezone (UTC may be -/+1 day from the selected date in some cases)
						// So we take the "midnight, end of day" of the user's selected date and convert that moment to UTC
						$localtimeEndTimestamp = date(Yii::app()->localtime->getPHPLocalDateTimeFormat(), strtotime($customFilter['audit_trail_date_to'].' 23:59:59'));
						$utcEndTimestamp = Yii::app()->localtime->fromLocalDateTime($localtimeEndTimestamp);
						$commandBase->andWhere('audit_trail.timestamp <= :to');
						$params[':to'] = $utcEndTimestamp;
					}
		}

		if ( Yii::app()->user->getIsPu() ) {
			// Power users can see only their own audit trails
			$commandBase->andWhere('audit_trail.idUser=:idPuUser');
			$params[':idPuUser'] = Yii::app()->user->getIdst();
		}

		if ($courses && !empty($courses)) {
			// Filtering by course ID for NEW COURSE event is non-sense
			// Because course filter is created BEFORE new course is created.. and thus has NO chance to be part of the filtering
			$params[':NewCourse'] = CoreNotification::NTYPE_NEW_COURSE;
			$commandBase->andWhere('audit_trail.target_course IN ('.implode(',', $courses).') OR (audit_trail.target_course=0 AND audit_trail.json_data IS NOT NULL) OR (audit_trail.action=:NewCourse)');
		}

		// Additional fields: JOIN selected fields and build the SELECT for them
		// NOTE: $commandBase is modified internally to add JOINS!!!
		//$additionalFieldsSelect = $this->joinAndBuildSelectForSelectedAdditionalFields($commandData, $this->getAdditionalFilteringFieldsFromFilter());
		$additionalFieldsSelect = $filterModel->joinAndBuildSelectForSelectedAdditionalFields($commandBase, $filterModel->getAdditionalFilteringFieldsFromFilter());


		$commandBase->select(
			# at least one parentheses in the select will prevent the automatic quoteColumnName() of the alias by Yii (which breaks the query's SELECT if active)
			$additionalFieldsSelect .
			"
			(audit_trail.id) id,
			COALESCE(user.idst, userTemp.idst)	as `user.idUser`,
			COALESCE(user.idst, userTemp.idst)	as `user.idst`,
			COALESCE(user.userid, userTemp.userid)									as `".LearningReportFilter::F_USER_USERID."`,
			COALESCE(user.firstname, userTemp.firstname)								as `".LearningReportFilter::F_USER_FIRSTNAME."`,
			COALESCE(user.lastname, userTemp.lastname)								as `".LearningReportFilter::F_USER_LASTNAME."`,
			COALESCE(user.email, userTemp.email)									as `".LearningReportFilter::F_USER_EMAIL."`,
			COALESCE(user.register_date, userTemp.request_on)							as `".LearningReportFilter::F_USER_REGISTER_DATE."`,
			(CASE WHEN user.expiration = '0000-00-00' THEN NULL ELSE user.expiration END)								as `".LearningReportFilter::F_USER_EXPIRATION."`,
			COALESCE(user.email_status, userTemp.confirmed)							as `".LearningReportFilter::F_USER_EMAIL_STATUS."`,
			(CASE WHEN user.valid=1 THEN 0 ELSE 1 END)	as `".LearningReportFilter::F_USER_SUSPENDED."`,
			user.suspend_date							as `".LearningReportFilter::F_USER_SUSPEND_DATE."`,

			(CASE WHEN course.name <> '' THEN course.name ELSE 'N/A' END)  as `" .LearningReportFilter::F_COURSE_NAME.  "`,
			course.idCourse								as `" .LearningReportFilter::F_STAT_INTERNAL_COURSE_ID."`,
			course.idCourse								as `" .LearningReportFilter::F_COURSE_ID."`,
			-- course.name									as `" .LearningReportFilter::F_COURSE_NAME.  "`,
			course.code			 						as `" .LearningReportFilter::F_COURSE_CODE.  "`,
			course.course_type			 				as `" .LearningReportFilter::F_COURSE_TYPE.  "`,
			course.status			 					as `" .LearningReportFilter::F_COURSE_STATUS.  "`,
			course.credits			 					as `" .LearningReportFilter::F_COURSE_CREDITS.  "`,
			course.date_begin							as `" .LearningReportFilter::F_COURSE_DATE_BEGIN.  "`,
			course.date_end								as `" .LearningReportFilter::F_COURSE_DATE_END.  "`,

			-- course category
			coursecat.translation 						as `" .LearningReportFilter::F_COURSE_CATEGORY.  "`,

			# Internal fields. Join the user to which this audit trail relates to
			# (not the one who triggered/logged the audit trail but to WHOM)
			targetUser.idst `targetUser.idst`,
			targetUser.userid `targetUser.userid`,
			targetUser.firstname `targetUser.firstname`,
			targetUser.lastname `targetUser.lastname`,
			targetUser.email `targetUser.email`,

			audit_trail.idUser 							as `".self::F_AUDIT_TRAIL_ID_USER."`,
			audit_trail.action 							as `".self::F_AUDIT_TRAIL_ACTION."`,
			audit_trail.target_course 					as `".self::F_AUDIT_TRAIL_TARGET_COURSE."`,
			audit_trail.target_user 					as `".self::F_AUDIT_TRAIL_TARGET_USER."`,
			audit_trail.json_data 						as `".self::F_AUDIT_TRAIL_JSON."`,
			audit_trail.ip 								as `".self::F_AUDIT_TRAIL_IP."`,
			audit_trail.timestamp 						as `".self::F_AUDIT_TRAIL_TIMESTAMP."`
		");

		// Join Course category table to get categor names
		$commandBase->leftJoin('learning_category coursecat', '(course.idCategory=coursecat.idCategory) AND (coursecat.lang_code=\'' . $languageCode .  '\')');


		//var_dump($commandBase->text);die;

		$commandCounter = Yii::app()->db->createCommand();
		$commandCounter->select('COUNT(*)')->from("(" . $commandBase->getText() . ") AS baseSQL");
		$numRecords = $commandCounter->queryScalar($params);
		$moreFilters = $filterModel->getFilteringOptionsFromFilter();
		$sort = new CSort();
		$sort->attributes = array(
			self::F_AUDIT_TRAIL_TIMESTAMP,
			self::F_AUDIT_TRAIL_ACTION,
		);

		$sort->defaultOrder = array(
			self::F_AUDIT_TRAIL_TIMESTAMP => CSort::SORT_DESC,
		);

		$addSorting = true;
		if(isset($_GET['sort']) && $_GET['sort']){
			$addSorting = false;
		}

		if (!empty($moreFilters['order']) && is_array($moreFilters['order']) && $addSorting) {
			$commandBase->order($moreFilters['order']['orderBy'] . ' ' . $moreFilters['order']['type'] . ', ' . self::F_AUDIT_TRAIL_TIMESTAMP . ' DESC');
			$sort->defaultOrder = null;
		}

		// Data provider config
		$config = array(
			'totalItemCount'	=> $numRecords,
			'pagination' 		=> $pagination,
			'params'			=> $params,
			'sort'				=> $sort,
		);

		// Create data provider and return to caller
		$dataProvider = new CSqlDataProvider($commandBase->getText(), $config);
		return $dataProvider;
	}


	/**
	 *
	 * @param LearningReportFilter $filterModel
	 * @return array:
	 */
	protected function getGridColumns(){
		$filterModel = $this->filterModel;
		$columns = array();
		$columns = array_merge($columns, $filterModel->getFieldsBundleColumns('user'));
		$columns = array_merge($columns, $this->getAuditTrailColumns());

		// Get the additional fields columns too, remove old additional fields columns and add new one
		$additionalFields = $filterModel->getAdditionalFieldsFromFilter();
		foreach ($additionalFields as $fieldId) {
			// Remove the old additional field from the list
			unset($columns['user.'.$fieldId]);

			// Add additional field details here
			$columns = array_merge($columns, $filterModel->getSingleColumnForAdditionalField($fieldId));
		}

        /******************* --- IMPORTANT --- ********************/

		// Overwrite some of the columns value data in order to return desired info for the report
		$columns = $this->getColumnsFromJsonData($columns);

        /******************* --- IMPORTANT --- ********************/

		// Get and add the course fields here
		$columns = array_merge($columns, $filterModel->getFieldsBundleColumns('course'));

        $additionalCourseFields = $filterModel->getAdditionalCourseFieldsFromFilter();
        foreach ($additionalCourseFields as $fieldId)
        {
            unset($columns['course.'.$fieldId]);
            $columns = array_merge($columns, $filterModel->getSingleColumnForAdditionalCourseField($fieldId));
        }

		$columns = $filterModel->reorderColumns(LearningReportType::AUDIT_TRAIL, $columns);

		return $columns;
	}


	/**
	 *
	 * @param unknown $filterModel
	 * @return multitype:
	 */
	protected function getAuditTrailColumns() {

		$filterModel = $this->filterModel;

		$filter = CJSON::decode($filterModel->filter_data);
		$columns= array();

		$fields = $filterModel->getSelectionFieldsFromFilter('audit_trail');

		foreach ($fields as $field => $flag) {
			if ( (int) $flag > 0 ) {
				$columns = array_merge($columns, $this->getSingleColumn($field));
			}
		}
		return $columns;
	}


	/**
	 *
	 * @param string $field
	 *
	 * @return array
	 */
	protected function getSingleColumn($field) {

		$filterModel = $this->filterModel;

		switch ($field) {

            // Format datetime columns
			case self::F_AUDIT_TRAIL_TIMESTAMP:
                $valueString = 'Yii::app()->localtime->toLocalDateTime($data["'.$field.'"],"short","short",null,null,false)';
                break;

			case self::F_AUDIT_TRAIL_ACTION:
				$valueString = function($data) {
					$actionConst = $data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_ACTION];
					return Yii::t('audit_trail', AuditTrailLog::actionLabel($actionConst));
				};
				break;

			case self::F_AUDIT_TRAIL_JSON: // Event details column
				$columnType = 'html';
				$valueString = function($data){
					$auditTrailActionType = $data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_ACTION];

					$targetCourse 	= $data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_TARGET_COURSE];
					$targetUser 	= $data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_TARGET_USER];
					$dataArr 		= CJSON::decode($data[AuditTrailLogCustomReportHelper::F_AUDIT_TRAIL_JSON]);

					$users   		= isset( $dataArr['users'] ) 	? (array) $dataArr['users'] : array();
					$courses 		= isset( $dataArr['courses'] ) 	? (array) $dataArr['courses'] : array();
					$seats 			= isset( $dataArr['seats'] ) 	? (int) $dataArr['seats'] : 1;

                    // can return at maximum 1st level not more e.g.: arr[0][1] will work  arr[0][1][2] wont work
                    $getSingleItemFromDataArr = function($field)use($dataArr){
                        $fields = explode('.', $field);
                        if(count($fields) == 2) {
                            $field_1 = $fields[0];
                            $field_2 = $fields[1];
                            if(!empty($dataArr[$field_1]) && !empty($dataArr[$field_1][$field_2])) {
                                return $dataArr[$field_1][$field_2];
                            }
                        } elseif(count($fields) == 1) {
                            return $dataArr[$fields[0]];
                        }
                        return null;
                    };

					$enrollmentStatusesList = LearningCourseuser::getStatusesArray();

                    // can return at maximum 1st level not more e.g.: arr[0][1] will work  arr[0][1][2] wont work
                    $getSingleItemFromDataArr = function($field)use($dataArr){
                        $fields = explode('.', $field);
                        if(count($fields) == 2) {
                            $field_1 = $fields[0];
                            $field_2 = $fields[1];
                            if(!empty($dataArr[$field_1]) && !empty($dataArr[$field_1][$field_2])) {
                                return $dataArr[$field_1][$field_2];
                            }
                        } elseif(count($fields) == 1) {
                            return $dataArr[$fields[0]];
                        }
                        return null;
                    };


					switch($auditTrailActionType){

						case AuditTrailLog::ACTION_SEAT_CONSUMED:

							if ($targetUser) {
								// Single user
								$userNames = array(Yii::app()->user->getRelativeUsername($data['targetUser.userid']));
							}
							else{
								// Multiple users
								$userNames   = Yii::app()->getDb()->createCommand()
								                  ->select( 'CONCAT_WS(" ", firstname, lastname)' )
								                  ->from( CoreUser::model()->tableName() )
								                  ->where( array( 'IN', 'idst', $users ) );
							}

							if ($targetCourse) {
								// Single course
								$courseNames = array($data[LearningReportFilter::F_COURSE_NAME]);
							}
							else {
								// Multiple courses
								$courseNames = Yii::app()->getDb()->createCommand()
								                  ->select( 'name' )
								                  ->from( LearningCourse::model()->tableName() )
								                  ->where( array( 'IN', 'idCourse', $courses ) );
							}

							return Yii::t('audit_trail', '1 seat to enroll :users to :courses|:num seat(s) to enroll :users to :courses', array(
								$seats,
								':num'=> $seats,
								':users'=>implode(', ', $userNames),
								':courses'=>implode(', ', $courseNames),
							));
							break;
						case AuditTraillog::ACTION_ADMIN_RESET_USER_LO_STATUS:

							$loTitle = (!empty($dataArr["learningObject"]) && !empty($dataArr["learningObject"]["title"])) ? $dataArr["learningObject"]["title"] : null;
							$userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : null;
							$userName = preg_replace('#\/|\\\#', '', $userName);

							if(!empty($loTitle) && !empty($userName)){
								return Yii::t('standard', '_USER') . ': <b>' . $userName . '</b> <br/>' .  Yii::t('standard', 'Learning Object') . ': <b>' . $loTitle . '</b>';
							}

							break;

						case AuditTrailLog::ACTION_SEAT_RESTORED:
							if ($targetUser) {
								// Single user
								$userNames = array(Yii::app()->user->getRelativeUsername($data['targetUser.userid']));
							}
							else {
								// Multiple users
								$userNames   = Yii::app()->getDb()->createCommand()
								                  ->select( 'CONCAT_WS(" ", firstname, lastname)' )
								                  ->from( CoreUser::model()->tableName() )
								                  ->where( array( 'IN', 'idst', $users ) );
							}

							if ($targetCourse) {
								// SIngle Course
								$courseNames = array($data[LearningReportFilter::F_COURSE_NAME]);
							}
							else {
								// Multiple courses
								$courseNames = Yii::app()->getDb()->createCommand()
								                  ->select( 'name' )
								                  ->from( LearningCourse::model()->tableName() )
								                  ->where( array( 'IN', 'idCourse', $courses ) );
							}

							return Yii::t('audit_trail', '1 seat by unenrolling :users from :courses|:seats seats by unenrolling :users from :courses', array(
								$seats,
								':seats'=> $seats,
								':users'=>implode(', ', $userNames),
								':courses'=>implode(', ', $courseNames),
							));

							break;

						case AuditTrailLog::ACTION_SEATS_BOUGHT:

							if (!$targetCourse) {
								// Error
								Yii::log('Target course must be specified for audit log of type '.AuditTrailLog::ACTION_SEATS_BOUGHT, CLogger::LEVEL_ERROR, __CLASS__);
								return Yii::t('standard', '_OPERATION_FAILURE');
							}
							else {
								$courseName = isset($data[LearningReportFilter::F_COURSE_NAME]) ? $data[LearningReportFilter::F_COURSE_NAME] : null;
								return Yii::t('audit_trail', "1 seat for :course course|:seats seats for :course course", array(
									$seats,
									':seats'=>$seats,
									':course'=>$courseName,
								));
							}
							break;

						case AuditTrailLog::ACTION_ADMIN_ADDED_EXTRA_SEATS:
							// GodAdmin username
							$godAdminUserName = $dataArr['adminUserName'];

							// PowerUser username
							$puUsername = $dataArr['puUsername'];

							// Course name
							$courseName = $dataArr['course']['name'];

							// Extra seats number
							$extraSeats = $dataArr['extra_seats'];

							return Yii::t('audit_trail', ':admin granted :num extra seats to :user for course: :course', array(
								':admin'=> $godAdminUserName,
								':num'=> $extraSeats,
								':user'=>$puUsername,
								':course'=>$courseName
							));
							break;

						case AuditTrailLog::ACTION_ADMIN_REMOVED_EXTRA_SEATS:
							// GodAdmin username
							$godAdminUserName = $dataArr['adminUserName'];

							// PowerUser username
							$puUsername = $dataArr['puUsername'];

							// Course name
							$courseName = $dataArr['course']['name'];

							// Extra seats number
							$extraSeats = $dataArr['extra_seats'];

							return Yii::t('audit_trail', ':admin removed :num extra seats from :user for course: :course', array(
								':admin'=> $godAdminUserName,
								':num'=> $extraSeats,
								':user'=>$puUsername,
								':course'=>$courseName
							));
							break;

						case AuditTrailLog::ACTION_ADMIN_ADD_CATALOGUE:
                        case AuditTrailLog::ACTION_ADMIN_UPDATE_CATALOGUE:
                        case AuditTrailLog::ACTION_ADMIN_DELETED_CATALOGUE:
                            $catalogueName = (!empty($dataArr["catalogue"]) && !empty($dataArr["catalogue"]["name"])) ? $dataArr["catalogue"]["name"] : null;
                            return $catalogueName;
                            break;

                        case CoreNotification::NTYPE_NEW_COURSE:
                        case CoreNotification::NTYPE_COURSE_DELETED:
                        case CoreNotification::NTYPE_COURSE_PROP_CHANGED:
                            $courseName = (!empty($dataArr["course"]) && !empty($dataArr["course"]["name"])) ? $dataArr["course"]["name"] : null;
                            return $courseName;
                            break;

						case CoreNotification::NTYPE_ILT_SESSION_DELETED:
						case CoreNotification::NTYPE_WEBINAR_SESSION_DELETED:
							if(!empty($dataArr["sessionData"]) && !empty($dataArr["sessionData"]["name"])) {
								$sessionName = $dataArr["sessionData"]["name"];
								if(!empty($dataArr["sessionData"]["date_begin"]) && !empty($dataArr["sessionData"]["date_end"])) {
									$sessionName .= ' : '.
									Yii::app()->localtime->toLocalDateTime($dataArr["sessionData"]["date_begin"]).
									' - '.
									Yii::app()->localtime->toLocalDateTime($dataArr["sessionData"]["date_end"]);
								}
								return $sessionName;
							}
							return null;
							break;

                        case CoreNotification::NTYPE_NEW_CATEGORY:
                        case AuditTrailLog::ACTION_ADMIN_UPDATE_CATEGORY:
                        case AuditTrailLog::ACTION_ADMIN_REMOVE_CATEGORY:
                            $categoryName = (!empty($dataArr["category"]) && !empty($dataArr["category"]["translation"])) ? $dataArr["category"]["translation"] : null;
                            return $categoryName;
                            break;

                        case CoreNotification::NTYPE_NEW_LO_ADDED_TO_COURSE:
                        case AuditTrailLog::ACTION_ADMIN_EDITED_COURSE_LO:
                        case AuditTrailLog::ACTION_ADMIN_DELETED_COURSE_LO:
                        $loTitle = (!empty($dataArr["learningObject"]) && !empty($dataArr["learningObject"]["title"])) ? $dataArr["learningObject"]["title"] : null;
                        $courseName = (!empty($dataArr["course"]) && !empty($dataArr["course"]["name"])) ? $dataArr["course"]["name"] : null;
                        return (!empty($courseName) && !empty($loTitle)) ? ($courseName . ':' . $loTitle) : "N/A";
                        break;

                        case AuditTrailLog::ACTION_COURSE_FILE_DOWNLOAD:
                        case AuditTrailLog::ACTION_ADDED_DOWNLOAD:
                            $courseFileTitle = (!empty($dataArr["courseFile"]) && !empty($dataArr["courseFile"]["title"])) ? $dataArr["courseFile"]["title"] : null;
                            $courseName = (!empty($dataArr["course"]) && !empty($dataArr["course"]["name"])) ? $dataArr["course"]["name"] : null;
                            return (!empty($courseName) && !empty($courseFileTitle)) ? ($courseName . ':' . $courseFileTitle) : "N/A";
                            break;

                        case AuditTrailLog::ACTION_LEARNING_COURSEPATH_ADDED:
                        case AuditTrailLog::ACTION_LEARNING_COURSEPATH_UPDATED:
                        case AuditTrailLog::ACTION_LEARNING_COURSEPATH_DELETED:
                            return (!empty($dataArr["learningCoursepath"]) && !empty($dataArr["learningCoursepath"]["path_name"])) ? $dataArr["learningCoursepath"]["path_name"] : null;
                            break;
                        case AuditTrailLog::ACTION_COURSE_ADDED_TO_LEARNING_COURSEPATH:
                            return Yii::t('audit_trail','Assigned course = <strong>{course}</strong> : Learning Plan = <strong>{coursepath}</strong>', array('{course}'=>$dataArr['course'],'{coursepath}'=>$dataArr['coursepath']));
                            break;
                        case AuditTrailLog::ACTION_COURSE_REMOVED_FROM_LEARNING_COURSEPATH:
                            return Yii::t('audit_trail','Removed course = <strong>{course}</strong> : Learning Plan = <strong>{coursepath}</strong>', array('{course}'=>$dataArr['course'],'{coursepath}'=>$dataArr['coursepath']));
                            break;


                        case AuditTrailLog::ACTION_CUSTOM_REPORT_ADDED:
                        case AuditTrailLog::ACTION_CUSTOM_REPORT_PUBLISHED_UNPUBLISHED:
                        case AuditTrailLog::ACTION_CUSTOM_REPORT_DELETED:
                            return (!empty($dataArr["customReport"]) && !empty($dataArr["customReport"]["filter_name"])) ? $dataArr["customReport"]["filter_name"] : null;
                            break;

                        case CoreNotification::NTYPE_NEW_USER_CREATED:
                        case CoreNotification::NTYPE_USER_MODIFIED:
                        case CoreNotification::NTYPE_USER_DELETED:
                        case CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG:
						case CoreNotification::NTYPE_USER_SUSPENDED:
						case CoreNotification::NTYPE_USER_UNSUSPENDED:
                            $userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : null;
                            $userName = preg_replace('#\/|\\\#', '', $userName);
                            return $userName;
                            break;

                        case CoreNotification::NTYPE_USER_SUBSCRIBED_COURSE:
                        case CoreNotification::NTYPE_USER_UNSUBSCRIBED:
                            $userName_ = $getSingleItemFromDataArr('user.userid');
                            $userName = preg_replace('/\/|\\\/','',$userName_);

                            $courseName = $getSingleItemFromDataArr('course.name');

                            $userFirstName = $getSingleItemFromDataArr('user.firstname');

                            $userLastName = $getSingleItemFromDataArr('user.lastname');

                            $courseInternalId = $getSingleItemFromDataArr('course.idCourse');

                            $result = '';

                            $result .= !empty($courseName) ? "{$courseName}" : '';
                            $result .= !empty($userName) ? ":{$userName}" : '';
                            $result .= !empty($userFirstName) ? ":{$userFirstName}" : '';
                            $result .= !empty($userLastName) ? ":{$userLastName}" : '';
                            $result .= !empty($courseInternalId) ? ":{$courseInternalId}" : '';

                            return $result;
                            break;

                        case CoreNotification::NTYPE_USER_COURSELEVEL_CHANGED:
                            $userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : null;
                            $userName = preg_replace('#\/|\\\#', '', $userName);
                            $courseName = (!empty($dataArr["course"]) && !empty($dataArr["course"]["name"])) ? $dataArr["course"]["name"] : null;
                            $courseLevel = (!empty($dataArr["courseUser"]) && !empty($dataArr["courseUser"]["level"])) ? $dataArr["courseUser"]["level"] : '';
                            if(!empty($courseLevel)) {
                                $courseLevel = Yii::t("levels", "_LEVEL_".$courseLevel);
                            }

                            return (!empty($userName) && !empty($courseName) && !empty($courseLevel)) ? ($courseName . ':' . $userName . '  ' . $courseLevel) : '';
                            break;

                        case CoreNotification::NTYPE_USER_SUBSCRIBED_GROUP:
                        case CoreNotification::NTYPE_USER_REMOVED_FROM_GROUP:
                            $userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : null;
                            $userName = preg_replace('#\/|\\\#', '', $userName);
                            $groupName = (!empty($dataArr["group"]) && !empty($dataArr["group"]["groupid"])) ? $dataArr["group"]["groupid"] : null;
                            $groupName = preg_replace('#\/|\\\#', '', $groupName);

                            return (!empty($userName) && !empty($groupName)) ? ($userName . ' : ' . $groupName) : '';
                            break;
                        case CoreNotification::NTYPE_USER_ASSIGNED_TO_BRANCH:
                            return Yii::t('audit_trail','Assigned user = <strong>{user}</strong> : Branch = <strong>{branch}</strong>', array('{user}'=>$dataArr['user'],'{branch}'=>$dataArr['branch']));
                            break;
                        case CoreNotification::NTYPE_USER_REMOVED_FROM_BRANCH:
                            return Yii::t('audit_trail','Removed user = <strong>{user}</strong> : Branch = <strong>{branch}</strong>', array('{user}'=>$dataArr['user'],'{branch}'=>$dataArr['branch']));
                            break;
                        case EventManager::EVENT_USER_ENROLLED_IN_WEBINAR_SESSION:
                        case EventManager::EVENT_USER_UNENROLLED_FROM_WEBINAR_SESSION:
                            $userName_ = $getSingleItemFromDataArr('user.userid');
                            $userName = preg_replace('/\/|\\\/','',$userName_);

                            $userFirstName = $getSingleItemFromDataArr('user.firstname');

                            $userLastName = $getSingleItemFromDataArr('user.lastname');

                            $courseInternalId = $getSingleItemFromDataArr('course.idCourse');

                            $sessionInternalId = $getSingleItemFromDataArr('session.id_session');

                            $sessionName = $getSingleItemFromDataArr('session.name');

                            $session = $dataArr['session'];
                            $toolList = WebinarSession::toolList();
                            $sessionTools = $toolList[$session['tool']];

                            $result = '';

                            $result .= !empty($userName) ? "{$userName}" : '';
                            $result .= !empty($userFirstName) ? ":{$userFirstName}" : '';
                            $result .= !empty($userLastName) ? ":{$userLastName}" : '';
                            $result .= !empty($courseInternalId) ? ":{$courseInternalId}" : '';
                            $result .= !empty($sessionInternalId) ? ":{$sessionInternalId}" : '';
                            $result .= !empty($sessionName) ? ":{$sessionName}" : '';
                            $result .= !empty($sessionTools) ? ":{$sessionTools}" : '';

                            return $result;
                            break;
                        case CoreNotification::NTYPE_ILT_SESSION_USER_ENROLLED:
                        case CoreNotification::NTYPE_ILT_SESSION_USER_UNENROLLED:
                            $userName_ = $getSingleItemFromDataArr('user.userid');
                            $userName = preg_replace('/\/|\\\/','',$userName_);

                            $userFirstName = $getSingleItemFromDataArr('user.firstname');

                            $userLastName = $getSingleItemFromDataArr('user.lastname');

                            $courseInternalId = $getSingleItemFromDataArr('course.idCourse');

                            $sessionInternalId = $getSingleItemFromDataArr('session.id_session');

                            $sessionName = $getSingleItemFromDataArr('session.name');

                            $location_dates = !empty($dataArr['session_dates']) ? $dataArr['session_dates'] : array();

                            $session_locations = array();

                            foreach($location_dates as $locationDate) {
                                if(!empty($locationDate['location']))
                                    $session_locations[$locationDate['id_location']] = $locationDate['location'];
                            }

                            $session_locations = array_values($session_locations);

                            $sessionLocations = '';

                            foreach($session_locations as $i => $loc) {
                                if($i > 0) $sessionLocations .= ',';
                                $sessionLocations .= !empty($loc['name']) ? $loc['name'] : '';
                            }

                            $result = '';

                            $result .= !empty($userName) ? "{$userName}" : '';
                            $result .= !empty($userFirstName) ? ":{$userFirstName}" : '';
                            $result .= !empty($userLastName) ? ":{$userLastName}" : '';
                            $result .= !empty($courseInternalId) ? ":{$courseInternalId}" : '';
                            $result .= !empty($sessionInternalId) ? ":{$sessionInternalId}" : '';
                            $result .= !empty($sessionName) ? ":{$sessionName}" : '';
                            $result .= !empty($sessionLocations) ? ":{$sessionLocations}" : '';

                            return $result;
                            break;
						case AuditTrailLog::ACTION_ADMIN_CHANGED_USER_COURSE_STATUS:
							$userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : null;
							$userName = preg_replace('#\/|\\\#', '', $userName);
							$courseName = (!empty($dataArr["course"]) && !empty($dataArr["course"]["name"])) ? $dataArr["course"]["name"] : null;
							$courseStatus = (!empty($dataArr["courseUser"]) && (!empty($dataArr["courseUser"]["status"]) ||  (isset($dataArr["courseUser"]["status"]) && ($dataArr["courseUser"]["status"] == "0" ||  $dataArr["courseUser"]["status"] == 0 )))) ? (int) $dataArr["courseUser"]["status"] : '';
							if(!empty($courseStatus) || isset($enrollmentStatusesList[$courseStatus])) {
								$courseStatus = Yii::t('standard', 'Enrollment status').' : '.(isset($enrollmentStatusesList[$courseStatus]) ? strtolower($enrollmentStatusesList[$courseStatus]) : '');
							}

							return (!empty($userName) && !empty($courseName) && !empty($courseStatus)) ? ($courseName . ' : ' . $userName . '  ' . $courseStatus) : '';
							break;
                        case CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION:
                            $userName = $getSingleItemFromDataArr('user.userid');
                            break;
						case CoreNotification::NTYPE_SUB_SEAT_ASSIGNED:
							return $this->getLogInformationByAction('sub_seat', $dataArr);

						case CoreNotification::NTYPE_SUB_SEAT_UNASSIGNED:
							return $this->getLogInformationByAction('sub_seat', $dataArr);

						case CoreNotification::NTYPE_SUB_SEAT_EDITED:
							return $this->getLogInformationByAction('sub_seat', $dataArr);

						case CoreNotification::NTYPE_SUB_PLAN_CREATED:

							return $this->getLogInformationByAction('sub_plan', $dataArr);

						case CoreNotification::NTYPE_SUB_PLAN_EDITED:
							return $this->getLogInformationByAction('sub_plan', $dataArr);

						case CoreNotification::NTYPE_SUB_PLAN_DELETED:
							return $this->getLogInformationByAction('sub_plan', $dataArr);


						case CoreNotification::NTYPE_SUB_BUNDLE_CREATED:
							return $this->getLogInformationByAction('sub_bundle', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_DELETED:
							return $this->getLogInformationByAction('sub_bundle', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_EDITED:
							return $this->getLogInformationByAction('sub_bundle', $dataArr);

						case CoreNotification::NTYPE_SUB_RECORD_CREATED:
							return $this->getLogInformationByAction('sub_record', $dataArr);
						case CoreNotification::NTYPE_SUB_RECORD_EDITED:
							return $this->getLogInformationByAction('sub_record', $dataArr);
						case CoreNotification::NTYPE_SUB_RECORD_DELETED:
							return $this->getLogInformationByAction('sub_record', $dataArr);
						case CoreNotification::NTYPE_SUB_RECORD_ADD_ITEM:
							return $this->getLogInformationByAction('sub_record_item', $dataArr);
						case CoreNotification::NTYPE_SUB_RECORD_DELETE_ITEM:
							return $this->getLogInformationByAction('sub_record_item', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_ITEM_DELETED:
							return $this->getLogInformationByAction('sub_bindle_item', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_ADD_ITEM:
							return $this->getLogInformationByAction('sub_bindle_item', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_ADD_VISIBILITY:
							return $this->getLogInformationByAction('sub_bindle_visibility', $dataArr);
						case CoreNotification::NTYPE_SUB_BUNDLE_DELETE_VISIBILITY:
							return $this->getLogInformationByAction('sub_bindle_visibility', $dataArr);
						default:
//                            return $dataArr["name"];
                            break;

					}
				};
                break;

			default:
				$valueString = '$data["'.$field.'"]';
				break;
		}

		// Resolve field/column header
		// First, try to get the header from THIS report helper
		$header = $this->fieldName($field);
		// If not set, try to find it from CORE custom report model
		if (!$header) {
			$header = $filterModel->fieldName($field);
		}
		// If no value, set to native field name
		if (!$header)
			$header = $field;

		// -----
		$column = array();
		$column[$field] = array(
			'header' 	=> $this->fieldName($field),
			'value' 	=> $valueString,
			'cssClassExpression'	=> '"nowrap"',
			'htmlOptions'	=> array(
			),
			'name'	=> $field,
		);

		if ($columnType)
			$column[$field]['type'] = $columnType;

		return $column;

	}

	private function getLogInformationByAction($action, $data)
	{
		$rowActionDetails='';

		switch ($action) {
//			TYPE learning_course
			case 'sub_bindle_item':

				return $this->getSubBundleItemInformation($data);
			case 'sub_record':
//				Log::_($data);
				$subscriptionData = $data['subscription_record'];

				$rowActionDetails = 'id = ' . $subscriptionData['id'];
				if (!empty($subscriptionData['name'])) {
					$rowActionDetails .= ' name= ' . $subscriptionData['name'];
				}
				if (!empty($subscriptionData['code'])) {
					$rowActionDetails .= ' code= ' . $subscriptionData['code'];
				}

				$sql = 'SELECT s.code FROM sub_subscriptions AS s WHERE s.id = :idSubscription';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(':idSubscription', $subscriptionData['bundle_id']);
				$subBundleItem = $command->queryColumn();

				if (!empty($subBundleItem[0])) {
					$rowActionDetails .= ' bundle_code= ' . $subBundleItem[0];
				}

				$sql = 'SELECT sp.code FROM sub_subscription_plans AS sp WHERE sp.id = :idPlan';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(':idPlan', $subscriptionData['plan_id']);
				$subPlan = $command->queryColumn();

				if (!empty($subPlan[0])) {
					$rowActionDetails .= ' plan_code= ' . $subPlan[0];
				}

				return $rowActionDetails;

			case 'sub_bundle':
				$rowActionDetails = 'id = ' . $data['id'] . ' name= ' . $data['name'] . ' code = ' . $data['code'];
				return $rowActionDetails;
			case 'sub_plan':


				if (!empty($data['plan_name'])) {
					$rowActionDetails .= ' name= ' . $data['plan_name'];
				}
				if (!empty($data['code'])) {
					$rowActionDetails .= ' code= ' . $data['code'];
				}

				return $rowActionDetails;
			case 'sub_seat':
				$rowActionDetails .= 'id= ' . $data['id'];

				$sql = 'SELECT sr.code FROM sub_subscription_records AS sr WHERE sr.id = :idRecord';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(':idRecord', $data['id']);
				$subRecord = $command->queryColumn();

				if (!empty($subRecord[0])) {
					$rowActionDetails .= ' record_code= ' . $subRecord[0];
				}
				return $rowActionDetails;

//				$user = CoreUser::model()->findByAttributes(
//					array('idst' => $data['user_id'])
//				);
//				$userName = '';
//				if ($user) {
//					$userName = $user->userid;
//				}
//				return $userName;
			case 'sub_bindle_visibility':
				//Log::_($data);
				$sql = 'SELECT s.name FROM sub_subscriptions AS s WHERE s.id = :idSubscription';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(':idSubscription', $data['idSubscription']);
				$subBundleItem = $command->queryColumn();

				$result = '';

				if ($subBundleItem) {
					$result .= $subBundleItem[0];


					$lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

					$defaultLang = CoreUser::getDefaultLangCode();

					$sql = 'SELECT 
									DISTINCT (CASE
									WHEN :objType = 1
										THEN IF(b.translation NOT LIKE "", b.translation, b2.translation )	
									WHEN :objType = 2
										THEN TRIM(LEADING \'/\' FROM g.groupid)
									  END
									) AS translation
							FROM (SELECT "" AS fColumn) AS fectiveTbl
							LEFT JOIN core_org_chart AS b  ON b.id_dir = :idObject AND b.lang_code LIKE :lang
							LEFT JOIN core_org_chart AS b2  ON b2.id_dir = :idObject AND b2.lang_code LIKE :defaultLang
							LEFT JOIN core_group AS g ON g.idst = :idObject
							';

					$command = Yii::app()->db->createCommand($sql);
					$command->bindParam(':objType', $data['type']);
					$command->bindParam(':idObject', $data['idObject']);
					$command->bindParam(':defaultLang', $defaultLang);
					$command->bindParam(':lang', $lang);
					$groupTranslation = $command->queryColumn();


					if ($groupTranslation) {
						$result .= ' -> ' . $groupTranslation[0];
					}

				}
				return $result;

			case 'sub_record_item':
				Log::_($data);
//				:itemType = 1 -> COURSE
//				:itemType = 2 -> CATALOG
//				:itemType = 3 -> LEARNING_PLAN

				$sql = 'SELECT sr.name,
								(CASE
								WHEN :itemType = 1
									THEN CONCAT(lc.name," ( course )")
								WHEN :itemType = 2
									THEN CONCAT(lcat.name," ( catalog )")
								WHEN :itemType = 3
									THEN CONCAT(lcp.path_name," ( learning plan )")
								END) AS item_name
							FROM sub_subscription_records AS sr 
							LEFT JOIN learning_course AS lc ON lc.idCourse = :idItem
							LEFT JOIN learning_catalogue AS lcat ON lcat.idCatalogue = :idItem
							LEFT JOIN learning_coursepath AS lcp ON lcp.id_path = :idItem
							WHERE sr.id = :idRecord';
				$command = Yii::app()->db->createCommand($sql);
				$command->bindParam(':idRecord', $data['idRecord']);
				$command->bindParam(':itemType', $data['itemType']);
				$command->bindParam(':idItem', $data['idItem']);

				$subBundleItem = $command->queryAll();


				$result = '';

				if ($subBundleItem[0]) {
					$result .= $subBundleItem[0]['name'].' -> '.$subBundleItem[0]['item_name'];
				}
				return $result;


		}
	}

	private function getSubBundleItemInformation($itemData)
	{
		$information = '';
//		Log::_($itemData);
		$sql = 'SELECT s.name FROM sub_subscriptions AS s WHERE s.id = :idSubscription';

		$command = Yii::app()->db->createCommand($sql);
		$command->bindParam(':idSubscription', $itemData['idSubscription']);
		$subBundleItem = $command->queryColumn();


		if (!empty($subBundleItem)) {

			switch ($itemData['itemType']) {
//			TYPE learning_course
				case 1:

					$q = 'SELECT lc.name FROM learning_course AS lc WHERE lc.idCourse = :idItem';
					$command2 = Yii::app()->db->createCommand($q);
					$command2->bindParam(':idItem', $itemData['idItem']);
					$learningCourse = $command2->queryColumn();

					if (!empty($learningCourse)) {
						$information = $subBundleItem[0] . ' -> ' . $learningCourse[0] . ' ( course )';
					}


					break;
				//			TYPE learning_catalog
				case 2:

					$q = 'SELECT lcc.name FROM learning_catalogue AS lcc WHERE lcc.idCatalogue = :idItem';
					$command2 = Yii::app()->db->createCommand($q);
					$command2->bindParam(':idItem', $itemData['idItem']);
					$learningCourse = $command2->queryColumn();

					if (!empty($learningCourse)) {
						$information = $subBundleItem[0] . ' -> ' . $learningCourse[0] . ' ( catalogue )';
					}


					break;

			}
		}

		return $information;
	}

	/**
	 * Return list of actions to filter by
	 *
	 * @param LearningReportFilter $filterModel
	 * @return array
	 */
	protected function getActionsFilterSelection(){
		$filterModel = $this->filterModel;
		$filter = CJSON::decode($filterModel->filter_data);
		$res = array();
		if(isset($filter['filters']['audit_trail']) && is_array($filter['filters']['audit_trail'])){
			foreach($filter['filters']['audit_trail'] as $selectedTrail=>$flag){
				if($flag==1){
					$res[] = $selectedTrail;
				}
			}
		}
		return $res;
	}

	/**
	 * Get the report columns data from json_data if needed(if the corresponding records are deleted from the db)
	 *
	 * @param array $columns
	 * @return array $columns
	 */
	public function getColumnsFromJsonData($columns = array()) {
		/**
		 * Retrieve the user's information from core_deleted_user
		 * if the user was deleted from the system
		 */
		$_helperGetDeletedUserValue = function($dataRow, $field) {
			if(isset($dataRow[self::F_AUDIT_TRAIL_ID_USER])) {
				$deletedUser = CoreDeletedUser::model()->findByAttributes(array('idst' => $dataRow[self::F_AUDIT_TRAIL_ID_USER]));
				$property = str_replace('user.', '', $field);
				if($deletedUser && isset($deletedUser->$property) && !empty($deletedUser->$property)) {
					return $deletedUser->$property;
				}
			}
			return 'N/A';
		};
		/* This columns overwrite the existing group_list with group_member.idst and
		 * insert name of branches user is assigned to */
		$columns[LearningReportFilter::F_USER_GROUPS_LIST] = array(
			'header' 	=> Yii::t('standard', '_ORGCHART'),
			'value' 	=> function($data) {
				return CoreOrgChartTree::getOrgPathByUserId( $data['user.idUser'] );
			},
			'name'	=> LearningReportFilter::F_USER_GROUPS_LIST ,
		);

		// Verify, that username is not missing if the user record is removed
		$columns[LearningReportFilter::F_USER_USERID] = array(
			'header' 	=> Yii::t('admin_directory', '_DIRECTORY_FILTER_userid'),
			'value' 	=> function($data) use ($_helperGetDeletedUserValue) {

				$userName = ltrim($data[LearningReportFilter::F_USER_USERID],"/");

				// If there is no user id to return try to get it from the json_data field
				if (empty($userName)) {
					// Try to get the event details from the json_data
					$dataArr = CJSON::decode($data[self::F_AUDIT_TRAIL_JSON]);

					$userName = (!empty($dataArr["user"]) && !empty($dataArr["user"]["userid"])) ? $dataArr["user"]["userid"] : $_helperGetDeletedUserValue($data, LearningReportFilter::F_USER_USERID);
					$userName = ltrim($userName,"/");
				}

				return $userName;
			},
			'name'	=> LearningReportFilter::F_USER_USERID,
		);

		// Verify, that user e-mail is not missing if the user record is removed
		$columns[LearningReportFilter::F_USER_EMAIL] = array(
			'header' 	=> Yii::t('standard', '_EMAIL'),
			'value' 	=> function($data) use ($_helperGetDeletedUserValue) {

				$userEmail = $data[LearningReportFilter::F_USER_EMAIL];

				// If there is no user id to return try to get it from the json_data field
				if (empty($data[LearningReportFilter::F_USER_USERID])) {
					// Try to get the event details from the json_data
					$dataArr = CJSON::decode($data[self::F_AUDIT_TRAIL_JSON]);

					// Set user email
					$userEmail = (!empty($dataArr["user"]) && !empty($dataArr["user"]["email"])) ? $dataArr["user"]["email"] : $_helperGetDeletedUserValue($data, LearningReportFilter::F_USER_EMAIL);
				}

				return $userEmail;
			},
			'name'	=> LearningReportFilter::F_USER_EMAIL,
		);

		// Verify, that user firstName is not missing if the user record is removed
		$columns[LearningReportFilter::F_USER_FIRSTNAME] = array(
			'header' 	=> Yii::t('standard', '_FIRSTNAME'),
			'value' 	=> function($data) use ($_helperGetDeletedUserValue) {

				$userFirstname = $data[LearningReportFilter::F_USER_FIRSTNAME];

				// If there is no user id to return try to get it from the json_data field
				if (empty($data[LearningReportFilter::F_USER_USERID])) {
					// Try to get the event details from the json_data
					$dataArr = CJSON::decode($data[self::F_AUDIT_TRAIL_JSON]);

					// Set user email
					$userFirstname = (!empty($dataArr["user"]) && !empty($dataArr["user"]["firstname"])) ? $dataArr["user"]["firstname"] : $_helperGetDeletedUserValue($data, LearningReportFilter::F_USER_FIRSTNAME);
				}

				return $userFirstname;
			},
			'name'	=> LearningReportFilter::F_USER_FIRSTNAME,
		);

		// Verify, that user lastName is not missing if the user record is removed
		$columns[LearningReportFilter::F_USER_LASTNAME] = array(
			'header' 	=> Yii::t('standard', '_LASTNAME'),
			'value' 	=> function($data) use ($_helperGetDeletedUserValue) {

				$userLarstname = $data[LearningReportFilter::F_USER_LASTNAME];

				// If there is no user id to return try to get it from the json_data field
				if (empty($data[LearningReportFilter::F_USER_USERID])) {
					// Try to get the event details from the json_data
					$dataArr = CJSON::decode($data[self::F_AUDIT_TRAIL_JSON]);

					// Set user email
					$userLarstname = (!empty($dataArr["user"]) && !empty($dataArr["user"]["lastname"])) ? $dataArr["user"]["lastname"] : $_helperGetDeletedUserValue($data, LearningReportFilter::F_USER_LASTNAME);
				}

				return $userLarstname;
			},
			'name'	=> LearningReportFilter::F_USER_LASTNAME,
		);

		return $columns;
	}

}
