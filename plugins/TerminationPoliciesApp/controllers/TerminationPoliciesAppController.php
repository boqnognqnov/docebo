<?php

/**
 * Class TerminationPoliciesAppController
 *
 * @property TerminationPolicies $model
 * @property Exporter $exporter
 */
class TerminationPoliciesAppController extends AdminController
{
	protected $model;
	protected $exporter;

	public function init()
	{
		ini_set('max_execution_time', '10000');
		ini_set('memory_limit', '256M');

		$this->model = new TerminationPolicies();

		$type = Yii::app()->request->getParam('type', 'csv');
		$this->exporter = new Exporter($type);

		parent::init();
	}


	public function accessRules()
	{
		$res = array();


		$res[] = array(
			'allow',
			'users' => array('*')
		);

		return $res;
	}

	/**
	 * public function actionExportObjectsFilesAssociation
	 *
	 * Call from url: platformUrl/lms/index.php?r=TerminationPoliciesApp/TerminationPoliciesApp/exportObjectsFilesAssociation
	 *
	 * Url addition parameters
	 * type - optional - csv|xls - Choose the downloaded file extension between csv and xls
	 */
	public function actionExportObjectsFilesAssociation()
	{
		//Recover the objects
		$objects = $this->model->getObjects();

		//Recover the files/folders info
		$scorms_folders = $this->model->getScormsFolders();
		$videos_files = $this->model->getVideosFiles();
		$items_files = $this->model->getItemsFiles();
		$tincans_files = $this->model->getTincansFiles();

		//CSV/XLS heading
		$this->exporter->addRow(array('Course code', 'Course name', 'LO title', 'LO type', 'File or Folder'));

		foreach($objects as $object)
		{
			$this->exporter->addCell($object['code']);
			$this->exporter->addCell($object['name']);
			$this->exporter->addCell($object['title']);
			$this->exporter->addCell($object['objectType']);

			switch($object['objectType'])
			{
				case 'authoring':
					$this->exporter->addCell('authoring/'.$object['idResource'].'/');
				break;
				case 'file':
					$this->exporter->addCell($items_files[$object['idResource']]);
				break;
				case 'scormorg':
					$this->exporter->addCell($scorms_folders[$object['idResource']]);
				break;
				case 'tincan':
					$this->exporter->addCell($tincans_files[$object['idResource']]);
				break;
				case 'video':
					$this->exporter->addCell($videos_files[$object['idResource']]);
				break;
				default:
					$this->exporter->addCell('n/a');
				break;
			}

			$this->exporter->closeRow();
		}

		$this->exporter->getFile();
	}
}
