<?php

/**
 * Plugin for Label
 */

class TerminationPoliciesApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = false;

	public $plugin_name = 'TerminationPoliciesApp';


	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
	}
}
