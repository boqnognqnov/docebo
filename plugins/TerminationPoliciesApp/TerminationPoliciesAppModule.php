<?php
/**
 * Class GamificationAppModule
 * Main Gamification plugin module
 */
class TerminationPoliciesAppModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'TerminationPoliciesApp.models.*',
			'TerminationPoliciesApp.controllers.*'
		));
	}
}
