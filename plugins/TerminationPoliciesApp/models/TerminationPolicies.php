<?php

class TerminationPolicies
{
	protected $command;

	public function __construct()
	{
		$this->command = Yii::app()->db->createCommand();
	}


	/**
	 * @return array An array with all the courses LO
	 */
	public function getObjects()
	{
		$query = "
			select c.code, c.name, o.idOrg, o.title, o.objectType, o.idResource
			from learning_organization as o
			join learning_course as c on c.idCourse = o.idCourse
			where o.objectType <> ''
			order by c.code, c.name, o.iLeft";

		return $this->command->setText($query)->queryAll();
	}


	/**
	 * @return array An array with the correspondences between SCORM learning_organization idResource and the SCORM folder name
	 */
	public function getScormsFolders()
	{
		$query = "
			select idscorm_package, path
			from learning_scorm_package";

		$results = $this->command->setText($query)->queryAll();
		$res = array();

		foreach($results as $data)
		{
			$path = 'scorm/'.$data['path'].'/';
			if(stripos($data['path'], 'partner/') == 0)
				$path = 'Docebo Marketplace';
			$res[$data['idscorm_package']] = $path;
		}

		return $res;
	}


	/**
	 * @return array An array with the correspondences between Video learning_organization idResource and the Video MP4 file name
	 */
	public function getVideosFiles()
	{
		$query = "
			select id_video, video_formats
			from learning_video";

		$results = $this->command->setText($query)->queryAll();
		$res = array();

		foreach($results as $data)
		{
			$formats = CJSON::decode($data['video_formats'], true);
			$res[$data['id_video']] = 'video/'.$formats['mp4'];
		}

		return $res;
	}


	/**
	 * @return array An array with the correspondences between Item learning_organization idResource and the Item file name
	 */
	public function getItemsFiles()
	{
		$query = "
			select idLesson, path
			from learning_materials_lesson";

		$results = $this->command->setText($query)->queryAll();
		$res = array();

		foreach($results as $data)
		{
			$path = 'item/'.$data['path'];
			$res[$data['idLesson']] = $path;
		}

		return $res;
	}


	/**
	 * @return array An array with the correspondences between TinCan learning_organization idResource and the TinCan folder name
	 */
	public function getTincansFiles()
	{
		$query = "
			select a.id_tc_activity, p.path
			from learning_tc_activity as a
			join learning_tc_ap as p on a.id_tc_ap = p.id_tc_ap";

		$results = $this->command->setText($query)->queryAll();
		$res = array();

		foreach($results as $data)
		{
			$path = 'tincan/'.$data['path'].'/';
			$res[$data['id_tc_activity']] = $path;
		}

		return $res;
	}
}
