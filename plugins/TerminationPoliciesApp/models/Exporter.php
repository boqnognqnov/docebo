<?php defined("IN_DOCEBO") or die('Direct access is forbidden.');

/* ======================================================================== \
| 	DOCEBO - The E-Learning Suite											|
| 																			|
| 	Copyright (c) 2008 (Docebo)												|
| 	http://www.docebo.com													|
|	License 	http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt		|
\ ======================================================================== */

class Exporter
{
	protected $type;
	protected $filename;
	protected $delimiter;
	protected $enclousure;
	protected $data;
	protected $tmp_row;

	protected $xls_class;
	protected $counter;
	protected $current_chr;
	protected $sub_chr;
	protected $first;

	public function __construct($type)
	{
		$this->type = strtolower($type);
		$this->filename = date('YmdHis');
		$this->tmp_row = array();

		switch($this->type)
		{
			case 'csv':
				$this->delimiter = ',';
				$this->enclousure = '"';
				$this->data = '';
			break;
			case 'xls':
				spl_autoload_unregister(array('YiiBase','autoload'));

				Yii::import('common.extensions.phpexcel.Classes.PHPExcel', true);

				$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
				$this->xls_class = new PHPExcel();
				$this->counter = 0;
				$this->resetXlsParams();

				spl_autoload_register(array('YiiBase','autoload'));
			break;
		}
	}

	protected function resetXlsParams()
	{
		$this->current_chr = 'A';
		$this->sub_chr = '';
		$this->first = true;
		$this->counter++;
	}

	public function setFilename($filename)
	{
		$this->filename = $filename;
	}

	public function setDelimiter($delimiter)
	{
		$this->delimiter = $delimiter;
	}

	public function setEnclousure($enclousure)
	{
		$this->enclousure = $enclousure;
	}

	public function addRow($row)
	{
		switch($this->type)
		{
			case 'csv':
				$first = true;
				foreach($row as $value)
				{
					if($first)
						$first = false;
					else
						$this->data .= $this->delimiter;
					$this->data .= $this->enclousure.str_replace($this->enclousure, $this->enclousure.$this->enclousure, html_entity_decode(stripslashes($value), ENT_COMPAT | ENT_HTML401, 'UTF-8')).$this->enclousure;
				}
				$this->data .= "\n";
			break;
			case 'xls':
				foreach($row as $value)
				{
					$this->xls_class->getActiveSheet()->setCellValue($this->sub_chr.$this->current_chr.$this->counter, html_entity_decode(stripslashes($value), ENT_COMPAT | ENT_HTML401, 'UTF-8'));
					if($this->current_chr === 'Z')
					{
						$this->current_chr = 'A';
						if($this->first)
						{
							$this->sub_chr = 'A';
							$this->first = false;
						}
						else
							$this->sub_chr++;
					}
					else
						$this->current_chr++;
				}
				$this->resetXlsParams();
			break;
		}
	}

	public function addRows($rows)
	{
		switch($this->type)
		{
			case 'csv':
				foreach($rows as $row)
				{
					$first = true;
					foreach($row as $value)
					{
						if($first)
							$first = false;
						else
							$this->data .= $this->delimiter;
						$this->data .= $this->enclousure.str_replace($this->enclousure, $this->enclousure.$this->enclousure, html_entity_decode(stripslashes($value), ENT_COMPAT | ENT_HTML401, 'UTF-8')).$this->enclousure;
					}
					$this->data .= "\n";
				}
			break;
			case 'xls':
				foreach($rows as $row)
				{
					foreach($row as $value)
					{
						$this->xls_class->getActiveSheet()->setCellValue($this->sub_chr.$this->current_chr.$this->counter, html_entity_decode(stripslashes($value), ENT_COMPAT | ENT_HTML401, 'UTF-8'));
						if($this->current_chr === 'Z')
						{
							$this->current_chr = 'A';
							if($this->first)
							{
								$this->sub_chr = 'A';
								$this->first = false;
							}
							else
								$this->sub_chr++;
						}
						else
							$this->current_chr++;
					}
					$this->resetXlsParams();
				}
			break;
		}
	}

	public function addStringRow($row)
	{
		switch($this->type)
		{
			case 'csv':
				$this->data .= html_entity_decode(stripslashes($row), ENT_COMPAT | ENT_HTML401, 'UTF-8')."\n";
			break;
			case 'xls':
			break;
		}
	}

	public function addCell($text)
	{
		switch($this->type)
		{
			case 'csv':
				$this->tmp_row[] = html_entity_decode(stripslashes($text), ENT_COMPAT | ENT_HTML401, 'UTF-8');
			break;
			case 'xls':
				$this->tmp_row[] = html_entity_decode(stripslashes($text), ENT_COMPAT | ENT_HTML401, 'UTF-8');
			break;
		}
	}

	public function closeRow()
	{
		switch($this->type)
		{
			case 'csv':
				$this->addRow($this->tmp_row);
				$this->tmp_row = array();
			break;
			case 'xls':
				$this->addRow($this->tmp_row);
				$this->tmp_row = array();
			break;
		}
	}

	public function addEmptyRow()
	{
		switch($this->type)
		{
			case 'csv':
				$this->data .= "\n";
			break;
			case 'xls':
				$this->resetXlsParams();
			break;
		}
	}

	public function getFile()
	{
		switch($this->type)
		{
			case 'csv';
				ob_end_clean();
				session_write_close();
				header('Content-Length:'. strlen($this->data));
				header("Content-type: application/download; charset=utf-8");
				header("Cache-control: private");
				header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
				if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
					header('Pragma: private');
				header('Content-Disposition: attachment; filename="'.$this->filename.'.'.$this->type.'"');

				echo $this->data;

				Yii::app()->end();
			break;
			case 'xls':
				@ob_end_clean();
				header('Content-Type: application/vnd.ms-excel');
				header("Content-type: application/download; charset=utf-8");
				header('Content-Disposition: attachment;filename="'.$this->filename.'.'.$this->type.'"');
				header('Cache-Control: max-age=0');

				$ow = PHPExcel_IOFactory::createWriter($this->xls_class, 'Excel5');
				$ow->save('php://output');

				Yii::app()->end();
			break;
		}
	}
}