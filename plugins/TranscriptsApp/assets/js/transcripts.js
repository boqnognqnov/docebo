/**
 * Created by vladimir on 13.10.2015 �..
 */
$( document ).ready(function() {
    $(document).on('dialog2.before-ajaxify', '.modal-edit-activity', function(){
        /*=- Validation for only numbers, dot and some key combination(alt+a) And for IE 9 and lesser support -=*/
        $('input[name="TranscriptsRecord[score]"]').keydown(function( e ) {
            if ( $.inArray (e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });
});
