<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160607_141906_ALTER_TRANSCRIPTS_RECORD_CHANGE_FOREIGN_KEY_TO_COURSE extends DoceboDbMigration {

	public function safeUp()
	{
		$fks = $this->getColumnForeignKeys('transcripts_record', 'course_id');

		if(!empty($fks)){
			$keyName = $fks[0]['CONSTRAINT_NAME'];
			$sql = "ALTER TABLE transcripts_record DROP FOREIGN KEY $keyName;
				ALTER TABLE transcripts_record ADD CONSTRAINT $keyName FOREIGN KEY (course_id) REFERENCES transcripts_course(id) ON UPDATE CASCADE ON DELETE SET NULL;";
			Yii::app()->db->createCommand($sql)->execute();
		}

		return true;
	}

	public function safeDown()
	{
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		$keyName = $this->getColumnForeignKeys('transcripts_record', 'course_id')[0]['CONSTRAINT_NAME'];
		$sql = "ALTER TABLE transcripts_record DROP FOREIGN KEY $keyName;
				ALTER TABLE transcripts_record ADD CONSTRAINT $keyName FOREIGN KEY (course_id) REFERENCES transcripts_course(id) ON UPDATE CASCADE ON DELETE CASCADE;";
		Yii::app()->db->createCommand($sql)->execute();
		return true;
	}
	
	
}
