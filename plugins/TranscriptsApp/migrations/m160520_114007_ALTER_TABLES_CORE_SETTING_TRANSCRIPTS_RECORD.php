<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160520_114007_ALTER_TABLES_CORE_SETTING_TRANSCRIPTS_RECORD extends DoceboDbMigration {

	public function safeUp()
	{
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		$this->alterColumn('transcripts_record', 'course_type', 'VARCHAR(20)');
		$this->update(CoreSetting::model()->tableName(), array(
				'value_type' => 'string'
		), 'param_name = "transcripts_require_defined_list"');
		return true;
	}

	public function safeDown()
	{
		// PUT YOUR MIGRATION-DOWN CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}
	
	
}
