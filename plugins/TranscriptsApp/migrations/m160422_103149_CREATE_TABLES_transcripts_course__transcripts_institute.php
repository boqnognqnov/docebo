<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */
class m160422_103149_CREATE_TABLES_transcripts_course__transcripts_institute extends DoceboDbMigration {

	public function safeUp()
	{
		$this->createTable('transcripts_course', array(
			'id'=>'pk',
			'course_name'=>'string NOT NULL',
			'type'=>'string NOT NULL',
			'institute_id'=>'integer NULL',
		));
		$this->createTable('transcripts_institute', array(
			'id'=>'pk',
			'institute_name'=>'string NOT NULL',
		));
		$this->addForeignKey('transcripts_course_to_institute', 'transcripts_course', 'institute_id', 'transcripts_institute', 'id', 'SET NULL', 'CASCADE');

		$this->execute('ALTER TABLE `transcripts_course` ENGINE=InnoDB;');
		$this->execute("ALTER TABLE `transcripts_record`
						ADD COLUMN `course_id`  int(11) NULL
						COMMENT 'If populated, course_name and course_type should be ignored' AFTER `id_user`;");
		$this->addForeignKey('transcripts_record_to_course', 'transcripts_record', 'course_id', 'transcripts_course', 'id', 'CASCADE', 'CASCADE');
		return true;
	}

	public function safeDown()
	{
		try {
			$this->dropForeignKey('transcripts_course_to_institute', 'transcripts_course');
		}catch(Exception $e){}
		try {
			$this->dropForeignKey('transcripts_record_to_course', 'transcripts_record');
		}catch(Exception $e){}

		$this->dropColumn('transcripts_record', 'course_id');

		$this->dropTable('transcripts_course');
		$this->dropTable('transcripts_institute');
		return true;
	}
	
	
}
