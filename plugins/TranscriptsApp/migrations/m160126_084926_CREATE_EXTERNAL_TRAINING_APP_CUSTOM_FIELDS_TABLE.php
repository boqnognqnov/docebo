<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160126_084926_CREATE_EXTERNAL_TRAINING_APP_CUSTOM_FIELDS_TABLE extends DoceboDbMigration {

	public function safeUp()
	{
		$sql = "
			CREATE TABLE IF NOT EXISTS `transcripts_field` (
			  `id_field` int(11) NOT NULL AUTO_INCREMENT,
			  `type` varchar(60) NOT NULL,
			  `sequence` int(5) NOT NULL DEFAULT '0',
			  `settings` TEXT NULL,
			  PRIMARY KEY (`id_field`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			CREATE TABLE IF NOT EXISTS `transcripts_field_translation` (
			  `id_field` int(11) NOT NULL,
			  `lang_code` varchar(255) NOT NULL DEFAULT '',
			  `translation` varchar(255) NOT NULL DEFAULT '',
			  PRIMARY KEY (`id_field`,`lang_code`),
			  CONSTRAINT FK_transcripts_field_translation FOREIGN KEY (`id_field`) REFERENCES `transcripts_field` (`id_field`) ON UPDATE CASCADE ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			CREATE TABLE IF NOT EXISTS `transcripts_field_dropdown` (
			  `id_option` int(11) NOT NULL AUTO_INCREMENT,
			  `id_field` int(11) NOT NULL,
			  `sequence` int(5) NOT NULL DEFAULT '0',
			  PRIMARY KEY (`id_option`),
			  CONSTRAINT FK_transcripts_field_dropdown FOREIGN KEY (`id_field`) REFERENCES `transcripts_field` (`id_field`) ON UPDATE CASCADE ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			CREATE TABLE IF NOT EXISTS `transcripts_field_dropdown_translations` (
			  `id_option` int(11) NOT NULL AUTO_INCREMENT,
			  `lang_code` varchar(255) NOT NULL DEFAULT '',
			  `translation` varchar(255) NOT NULL DEFAULT '',
			  PRIMARY KEY (`id_option`, `lang_code`),
			  CONSTRAINT FK_transcripts_field_dropdown_translations FOREIGN KEY (`id_option`) REFERENCES `transcripts_field_dropdown` (`id_option`) ON UPDATE CASCADE ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;

			CREATE TABLE IF NOT EXISTS `transcripts_field_value` (
			  `id_record` int(11) NOT NULL,
			  PRIMARY KEY (`id_record`),
			  CONSTRAINT FK_transcripts_field_value FOREIGN KEY (`id_record`) REFERENCES `transcripts_record` (`id_record`) ON UPDATE CASCADE ON DELETE CASCADE
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		$this->execute($sql);
	}

	public function safeDown()
	{
		$sql = "
			SET foreign_key_checks = 0;

			DROP TABLE `transcripts_field`;
			DROP TABLE `transcripts_field_translation`;
			DROP TABLE `transcripts_field_dropdown`;
			DROP TABLE `transcripts_field_dropdown_translations`;
			DROP TABLE `transcripts_field_value`;

			SET foreign_key_checks = 1;
		";

		$this->execute($sql);
	}
	
	
}
