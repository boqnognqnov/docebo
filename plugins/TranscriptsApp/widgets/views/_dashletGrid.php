<?php
$_columns = array();

if ((Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu()) && $dashletWidth > 4) {
    $_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('standard', '_USER'),
        'value' => array($this, 'gridRenderUserid'),
    );
}
    $_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('standard', '_COURSE_NAME'),
        'value' => '$data->renderCourseName();',
    );
if ($dashletWidth > 4) {
    $_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('standard', '_TYPE'),
        'value' => function($data)use($that) {
            if((int)$data->course_id > 0) {
                $transcriptsCourse = TranscriptsCourse::model()->findByPk($data->course_id);
                echo LearningCourse::model()->getCourseTypeTranslation($transcriptsCourse->type);
                return;
            }
            echo LearningCourse::model()->getCourseTypeTranslation($data->course_type);
        }
    );

    $_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('standard', '_DATE'),
        'cssClassExpression' => '"column-dates"',
        'value' => array($this, 'gridRenderDate'),
    );
    if (!((Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu()) && $dashletWidth == 6)) {
        $_columns[] = array(
            'type' => 'raw',
            'header' => Yii::t('standard', '_SCORE'),
            'value' => array($this, 'gridRenderScore'),
            'htmlOptions' => array(
                'class' => 'text-center',
            ),
            'headerHtmlOptions' => array(
                'class' => 'text-center',
            )
        );
    }
}
if ($dashletWidth > 8) {
    $_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('transcripts', 'Training institute'),
        'value' => '$data->renderTrainingInstituteName();',
    );
}
$_columns[] = array(
        'type' => 'raw',
        'header' => Yii::t('transcripts', 'Status'),
        'value' => '$data->renderStatus();',
        'htmlOptions' => array(
            'class' => 'text-center',
        ),
        'headerHtmlOptions' => array(
            'class' => 'text-center',
        )
    );
if ($enableEditRecords) {
    $_columns[] = array(
        'type' => 'raw',
        'header' => '',
        'htmlOptions' => array(
            'class' => 'controls',
        ),
        'value' => function ($data) {
            if (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu() || (Settings::get('transcripts_allow_users', 'off') == 'on' && $data->status != TranscriptsRecord::STATUS_APPROVED)) {
                if (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu()){
                    $linkEdit = Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/editActivity', array('id_record' => $data->id_record));
                    $linkDelete = Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/deleteActivity', array('id_record' => $data->id_record));
                } else {
                    $linkEdit = Docebo::createAbsoluteLmsUrl('TranscriptsApp/transcriptsUser/editActivity', array('id_record' =>$data->id_record));
                    $linkDelete = Docebo::createAbsoluteLmsUrl('TranscriptsApp/transcriptsUser/deleteActivity', array('id_record' =>$data->id_record));
                }
                $controlParams = array(
                    'arrayData' => array(
                        array('url' => $linkEdit, 'text' => 'Edit', 'method' => 'editActivity', 'dialog-title' => ''),
                        array('url' => $linkDelete, 'text' => 'Delete', 'method' => 'deleteActivity', 'dialog-title' => '')
                    ),
                    'containerClass' => 'customDropdown '
                );
                $this->widget('common.widgets.DropdownControls', $controlParams);
            }
        }
    );
}
    $gridTemplate = ($paginate) ? '{items}{pager}' : '{items}';

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'transcripts-management-grid-' . $dashletId,
        'htmlOptions' => array('class' => 'grid-view clearfix transcripts-management-grid'),
        'cssFile' => false,
        'ajaxType' => 'POST',
        'ajaxUrl' => Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/gridData', array('dashletId' => $dashletId)),
        'dataProvider' => $dataProvider,
        'columns' => $_columns,
        'template' => $gridTemplate,
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
        'pager' => array(
            'class' => 'DoceboCLinkPager',
            'maxButtonCount' => 8,
        ),
        'ajaxUpdate' => 'all-items',
        'beforeAjaxUpdate' => 'function(id, options) {
                    // Reset placeholder text if default one
                    options.type = "POST";
                    options.data = $("#transcripts-management-form-' . $dashletId . '").serialize();
                }',
        'afterAjaxUpdate' => 'function(id, data) {
                    $(\'a[rel="tooltip"]\').tooltip();
                    $(\'span[rel="tooltip"]\').tooltip();
                    $(document).controls();
                }',
    ));
?>

<script>
    $(function() {
        $(document).off("click", '[data-method="editActivity"]');
        $(document).off("click", '[data-method="deleteActivity"]');

        $(document).on("click", '[data-method="editActivity"]', function(){
            var url = $(this).attr("data-url");
            openDialog(false, '<?=Yii::t('standard', '_MOD')?>', url, 'modal-edit-activity', 'modal-edit-activity', 'GET');
        })

        $(document).on("click", '[data-method="deleteActivity"]', function(){
            var url = $(this).attr("data-url");
            openDialog(false, '<?=Yii::t('standard', '_DEL')?>', url, 'modal-delete-activity', 'modal-delete-activity', 'GET');
        })
    })
</script>
