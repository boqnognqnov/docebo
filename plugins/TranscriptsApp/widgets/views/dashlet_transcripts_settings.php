<?php
/**
 * Created by PhpStorm.
 * User: NikVas
 * Date: 17-Dec-15
 * Time: 16:11
 */
?>
<div>
    <hr>
</div>
<br>
<div class="tabs-settings-container transcripts-settings">
    <div class="row-fluid">
        <div class="span12">
            <label class="checkbox">
                <?php
                $checked = $this->getParam('transcripts_enable_edit_records', true);
                echo CHtml::checkBox('transcripts_enable_edit_records', $checked, array('uncheckValue' => '0'));
                echo Yii::t('transcripts', 'Enable creation and editing of external training records');
                ?>
            </label>
            <div class="setting-description styler indented"><?= Yii::t('transcripts', 'Allows to submit external trainings or change existing ones inside this widget') ?></div>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span12">
            <label class="checkbox">
                <?php
                $checked = $this->getParam('transcripts_show_view_all', true);
                echo CHtml::checkBox('transcripts_show_view_all', $checked, array('uncheckValue' => '0'));
                echo Yii::t('transcripts', 'Show "view all" link');
                ?>
            </label>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span12">
            <label class="checkbox">
                <?php
                $checked = $this->getParam('transcripts_paginate_widget', true);
                echo CHtml::checkBox('transcripts_paginate_widget', $checked, array('uncheckValue' => '0'));
                echo Yii::t('transcripts', 'Paginate widget');
                ?>
            </label>
        </div>
    </div>
    <br>
    <hr>
<br>
    <div class="row-fluid">
        <div class="span12">
            <?php
            $maxItems = $this->getParam('transcripts_max_items', 10);
            echo Yii::t('transcripts','Display max &nbsp; &nbsp; {input} &nbsp; &nbsp; items per page',array('{input}'=>CHtml::textField('transcripts_max_items',(isset($maxItems) && $maxItems > -1)?$maxItems:10,array('class'=>'span2'))));
            ?>
            <br>
            <span class="setting-description compact">&nbsp;<?= Yii::t('dashboard', 'Leave blank to display all items') ?></span>
        </div>
    </div>

    <br>

    <hr>
</div>

