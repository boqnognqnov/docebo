<?php
if (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu())
    $creatUrl = Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/createActivity');
else
    $creatUrl = Docebo::createAbsoluteLmsUrl('TranscriptsApp/transcriptsUser/createActivity', array('idUser' => Yii::app()->user->id));

$dashletCell = DashboardCell::model()->findByPk($this->dashletModel->cell);
?>

<div class="transcripts-dashlet-filter" id="transcripts-dashlet-<?=$this->dashletModel->id?>">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'transcripts-management-form-' . $this->dashletModel->id,
        'method' => 'POST',
        'action' => '',
        'htmlOptions' => array(
            'class' => 'ajax-grid-form',
            'data-grid' => '#transcripts-management-grid-'  . $this->dashletModel->id
        )
    ));
    ?>

    <div class="filters-wrapper">
        <div class="filters clearfix">
            <div class="pull-left show-approved-rejected-requests" style="margin-top: 7px;    margin-bottom: 7px;">
                <?php
                if ($dashletCell->width < 5){
                    echo Yii::t('standard', 'Show also') ?>: &nbsp;&nbsp;&nbsp; <?= CHtml::checkBox('filter-by-status', false, array('id' => 'filter-by-status-' . $this->dashletModel->id, 'value' => 'all')) . ' &nbsp;&nbsp;&nbsp;' . CHtml::label(Yii::t('standard', 'Activities waiting for approval'), 'filter-by-status',
                        array('style' => 'display:inline-block;'));
                } else {
                    echo Yii::t('standard', 'Show also') ?>: &nbsp;&nbsp;&nbsp; <?= CHtml::checkBox('filter-by-status', false, array('id' => 'filter-by-status-' . $this->dashletModel->id, 'value' => 'all')) . ' &nbsp;&nbsp;&nbsp;' . CHtml::label(Yii::t('standard', 'Activities waiting for approval (<span id="dashlet-waiting-for-approval">{n}</span>)', $countWaitingForApproval), 'filter-by-status',
                        array('style' => 'display:inline-block;'));
                }
                ?>
            </div>
            <?php if ($enableEditRecords && ((Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu()) || Settings::get('transcripts_allow_users', 'off') == 'on')) {?>
            <div class="input-wrapper pull-right">
                <?php
                    echo CHtml::link(Yii::t('standard', 'New activity'), $creatUrl,
                        array(
                        'class' => 'btn btn-docebo green big open-dialog',
                        'name'	=> 'new_activity_button',
                        'id'    => 'new_activity_button',
                        'data-dialog-class' => "modal-edit-activity",
                        'data-dialog-title' => Yii::t('helper', 'New external activity - transcripts')
                    ));
                ?>
            </div>
            <?php } ?>
        </div>
    </div>

    <?php $this->endWidget();?>
</div>

<div class="dl-footer">
    <div class="span6 dl-footer-left text-left"></div>
    <div class="span6 dl-footer-right text-right">
        <?php if($this->idLayout !== DashboardLayout::VIRTUAL_TYPE_SINGLE_CELL && $showViewAll): ?>
            <?php
            $url = (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu()) ?
                Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList', array()) :
                Docebo::createLmsUrl('/myActivities/index', array('tab' => 'external-activities'));
            ?>
            <a href="<?=$url;?>"><?=Yii::t('standard', 'View all')?></a>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    $(function () {
        var url = '<?=Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/gridData', array('dashletId' => $this->dashletModel->id));?>';
        $.get(url, function(res){
            $(res ).appendTo('#transcripts-dashlet-<?=$this->dashletModel->id?>');
        });

        $('#transcripts-dashlet-<?=$this->dashletModel->id?> .show-approved-rejected-requests input').styler();

        $('#filter-by-status-<?=$this->dashletModel->id?>').on('change', function() {
            $("#transcripts-management-grid-<?=$this->dashletModel->id?>").yiiGridView('update',  {
                data: $('#transcripts-management-form-<?=$this->dashletModel->id?>').serialize()
            });
        });
    });

    //TinyMCE FIX
    $(document).on("dialog2.closed", ".modal", function () {
        // Remove editor
        try {
            if (tinymce.editors.length >0)
                tinymce.editors[Notification.textareaid].remove();
        }
        catch(e) {
            tinyMCE.editors = [];
        }
    });
</script>