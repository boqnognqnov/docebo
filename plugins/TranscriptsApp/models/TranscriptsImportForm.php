<?php

class TranscriptsImportForm extends CFormModel
{

	public $file;
	public $separator = 'auto';
	public $manualSeparator;
	public $firstRowAsHeader = true;
	public $charset = 'UTF-8'; // charset of csv file
	public $insertUpdate = false;

	protected $_data;
	protected $_importMap = array(
		'id_record',
		'course_name',
		'course_type',
		'from_date',
		'to_date',
		'score',
		'credits',
		'training_institute',
		'certificate',
		'status'
	);
	protected $_maxFileSize = 3145728; // 314572800 = 300 Mb

	public function rules()
	{
		return array(
			array('file', 'file', 'on' => 'step_import', 'maxSize' => $this->_maxFileSize,
				'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed ' . $this->getMaxFileSize('MB') . ' megabytes.',
				'types' => array('csv'),
				'wrongType' => 'File type should be in "csv" format',
			),
			array('importMap', 'checkImportMap', 'on' => 'step_process'),
			array('separator, manualSeparator, firstRowAsHeader, charset, insertUpdate', 'safe')
		);
	}

	public function attributeLabels()
	{
		return array(
			'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
			'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
			'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
			'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET'),
			'insertUpdate' => Yii::t('transcripts', 'Update transcripts info'),
		);
	}

	public function dataProvider()
	{
		$items = $this->getData();

		if ($this->firstRowAsHeader) {
			unset($items[0]);
		}

		return new CArrayDataProvider($items, array(
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			),
		));
	}

	public function setImportMap($importMap)
	{
		if (is_array($importMap)) {
			$this->_importMap = $importMap;
			return true;
		}
		return false;
	}

	public function getImportMap()
	{
		return $this->_importMap;
	}

	public function getMaxFileSize($type = 'B')
	{
		switch ($type) {
			case 'B':
				return $this->_maxFileSize;
				break;
			case 'KB':
				return (int)($this->_maxFileSize / 1024);
				break;
			case 'MB':
				return (int)($this->_maxFileSize / (1024 * 1024));
				break;
		}
	}

	/**
	 * Inline validator
	 */
	public function checkImportMap($attribute, $params)
	{
		$selectedItems = array();
		foreach ($this->$attribute as $item) {
			if ($item != 'ignoredfield') {
				if (!in_array($item, $selectedItems)) {
					$selectedItems[] = $item;
				} else {
					$this->addError('importMap', 'Duplicate of fields');
					break;
				}
			}
		}
	}



	public function setData($data = array())
	{
		$this->_data = $data;
	}



	public function getData()
	{
		if (!is_array($this->_data)) {
			$delimiter = '';
			switch ($this->separator) {
				case 'comma':
					$delimiter = ',';
					break;
				case 'semicolon':
					$delimiter = ';';
					break;
				case 'manual':
					$delimiter = $this->manualSeparator;
					break;
				case 'auto':
					$delimiter = 'auto';
					break;
			}
			$csvFile = new QsCsvFile(array(
				'path' => $this->file->tempName,
				'charset' => $this->charset,
				'delimiter' => $delimiter,
				'firstRowAsHeader' => $this->firstRowAsHeader,
			));
			$this->_data = $csvFile->getArray();
		}

		return $this->_data;
	}



	public static function getImportFields() {
		return array(
			//'id_record',
			'username',
			'course_name',
			'course_type',
			'training_institute',
			'from_date',
			'to_date',
			'score',
			'credits',
			//'certificate',
			//'status'
		);
	}



	public static function getImportMapList()
	{
		$transcriptsFields = self::getImportFields();

		$list = array(
			'ignoredfield' => Yii::t('organization_chart', '_IMPORT_IGNORE'),
		);

		foreach ($transcriptsFields as $value) {
			$list[$value] = TranscriptsRecord::model()->getAttributeLabel($value);
		}

		//$list['name_country'] = Yii::t('standard', '_COUNTRY');

		return $list;
	}



	public function save()
	{
		if (!$this->validate()) {
			return false;
		}

		$data = $this->_data;
		if ($this->firstRowAsHeader) {
			unset($data[0]);
		}

		$transcriptIndex = null;
		$countryName = null;

		foreach ($this->_importMap as $k => $fieldName) {
			if ($fieldName == 'ignoredfield') {
				unset($this->_importMap[$k]);
			} else if ($fieldName == 'id_record') {
				$transcriptIndex = $k;
				unset($this->_importMap[$k]);
			}
		}

		$insertCount = 0;
		$count = 1;

		$date = new DateTime();

		// pre-fetch users list
		$cacheUsers = array();
		$users = Yii::app()->db->createCommand()
			->select("idst, userid")
			->from(CoreUser::model()->tableName())
			->where("userid <> '/Anonymous'")
			->andWhere("valid = 1")
			->andWhere("(expiration IS NULL OR expiration >= ':today')", array(':today' => $date->format('Y-m-d')))
			->queryAll();
		foreach ($users as $user) {
			$cacheUsers[strtolower(Yii::app()->user->getRelativeUsername($user['userid']))] = $user['idst'];
		}
		$users = null; //free some memory


		$transaction = Yii::app()->db->beginTransaction();
		foreach ($data as $item) {

			//this variable will contain the transcript record
			$object = null;

			//ignore empty lines (sometimes the importing CSVs contain empty lines -usually at the end of the file- and those may result into confusing error messages)
			if (empty($item) || (count($item) == 1 && empty($item[0]))) {
				$count++;
				continue;
			}

			if ($this->insertUpdate && isset($transcriptIndex)) {
				$idLocation = intval($item[$transcriptIndex]);
				$object = TranscriptsRecord::model()->findByPk($idLocation);
			}

			if ($object === null) {
				$object = new TranscriptsRecord();

				// By default, imported from CSV transcripts are Approved, as long as this action is run by GodAdmin
				$object->status = TranscriptsRecord::STATUS_APPROVED;
			}


			$invalidUser = false;
			$invalidDate = false;
			foreach ($this->_importMap as $i => $name) {
				if ($name == 'from_date' || $name == 'to_date') {
					$processedDate = self::processDateInput($item[$i]);
					//Check if provided date was valid. If not then raise an error.
					if (!empty($item[$i]) && empty($processedDate)) {
						Yii::log('Could not import the following external activity:', CLogger::LEVEL_ERROR);
						Yii::log(CVarDumper::dumpAsString($object->attributes), CLogger::LEVEL_ERROR);
						Yii::log('Field "'.$name.'" is invalid date', CLogger::LEVEL_ERROR);
						$key = $name;
						$error = Yii::t('standard', '_DATE_FORMAT');
						$this->addError('transcript_' . $insertCount . '_field_' . $key, 'Line #' . $insertCount . '. ' . $error);
						Yii::app()->session['importTranscriptsErrors'] = array_merge(Yii::app()->session['importTranscriptsErrors'], array(array(
							'row' => ($this->firstRowAsHeader ? 1 : 0) + $count,
							'field' => $key,
							'error' => $error
						)));
						$invalidDate = true;
					}
					$object->$name = $processedDate;
				} elseif (property_exists(get_class($object), $name) || $object->hasAttribute($name)) {
					$object->$name = $item[$i];
				} else if ($name == 'username') {
					// find user by username (lowercase)
					$userName = strtolower(trim($item[$i]));
					if (isset($cacheUsers[$userName])) {
						$object->id_user = $cacheUsers[$userName];
					} else {
						$invalidUser = true;
					}
				}
			}

			if ($invalidUser) { //the username in this row is non-existent in the LMS, or blank
				Yii::log('Could not import the following external activity:', CLogger::LEVEL_ERROR);
				Yii::log(CVarDumper::dumpAsString($object->attributes), CLogger::LEVEL_ERROR);
				Yii::log(CVarDumper::dumpAsString($object->getErrors()), CLogger::LEVEL_ERROR);

				$key = 'id_user'; //we know it for sure
				$error = Yii::t('register', '_ERR_INVALID_USER');
				$this->addError('transcript_' . $insertCount . '_field_' . $key, 'Line #' . $insertCount . '. ' . $error);
				Yii::app()->session['importTranscriptsErrors'] = array_merge(Yii::app()->session['importTranscriptsErrors'], array(array(
					'row' => ($this->firstRowAsHeader ? 1 : 0) + $count,
					'field' => $key,
					'error' => $error
				)));

				$count++;
				continue;
			}

			if ($invalidDate) {
				//errors for dates have already been reported
				$count++;
				continue;
			}

			$isValid = $object->validate();
			if (!$isValid)
			{
				Yii::log('Could not import the following external activity:', CLogger::LEVEL_ERROR);
				Yii::log(CVarDumper::dumpAsString($object->attributes), CLogger::LEVEL_ERROR);
				Yii::log(CVarDumper::dumpAsString($object->getErrors()), CLogger::LEVEL_ERROR);

				//this should avoid to report empty line in the csv (we know the an empty line generates at least 4 error messages when validating)
				if(count($object->errors) >= 4)
				{
					$count++;
					continue;
				}

				foreach ($object->errors as $key => $errors)
				{
					if ($key !== 'id_record')
					{
						foreach ($errors as $error)
						{
							$this->addError('transcript_' . $insertCount . '_field_' . $key, 'Line #' . $insertCount . '. ' . $error);

							Yii::app()->session['importTranscriptsErrors'] = array_merge(Yii::app()->session['importTranscriptsErrors'], array(array(
								'row' => ($this->firstRowAsHeader ? 1 : 0) + $count,
								'field' => $key,
								'error' => $error
							)));
						}
					}
				}

				$count++;

				continue;
			}

			if(Settings::get('transcripts_require_defined_list', false) == 'on'){
				$transcriptsRecords = TranscriptsCourse::model()->findByAttributes(array('course_name' => $object->course_name));
				$object->course_id = $transcriptsRecords->id;
				$object->course_name = '';
				$object->course_type = null;
				$object->training_institute = null;
			}

			if ($object->save()) {
				$insertCount++;
			}

			$count++;
		}

		$transaction->commit();

		return ($insertCount > 0);
	}


	/**
	 * If we have forced long years in dates then recognize short dates in input and transform them
	 * @param $date the date to be analyzed
	 * @return string the eventually transformed date
	 */
	public static function processDateInput($date) {

		$output = $date;

		if (!empty($date)) {

			/* @var $lt LocalTime */
			$lt = Yii::app()->localtime;
			if ($lt->forceLongYears()) {

				// Local date
				$format = $lt->getLocalDateFormat(LocalTime::DEFAULT_DATE_WIDTH, true); //read local format, avoid forcing long years
				$timestamp = CDateTimeParser::parse($date, $format);
				if ($timestamp !== FALSE) {
					//return the same date, but adopting long years
					$output = $lt->toLocalDate(date('Y-m-d', $timestamp));
				} else {
					//maybe long year has been provided, check it
					$formatWithLongYears = $lt->getLocalDateFormat(LocalTime::DEFAULT_DATE_WIDTH, false); //local format with forced long years
					if ($formatWithLongYears != $format) {
						$timestamp = CDateTimeParser::parse($date, $formatWithLongYears);
						if ($timestamp !== FALSE) {
							$output = $lt->toLocalDate(date('Y-m-d', $timestamp));
						} else {
							//the date is definitely of invalid format
							$output = false;
						}
					} else {
						//the two formats are already the same, this means that the check has already been performed with negative result
						$output = false;
					}
				}

			} else {

				//just check date validity
				if (!CDateTimeParser::parse($date, $lt->getLocalDateFormat(LocalTime::DEFAULT_DATE_WIDTH))) {
					$output = false;
				}
			}

		}

		return $output;
	}
}