<?php

/**
 * TranscriptsAppSettingsForm class.
 *
 * @property string $transcripts_allow_users
 * @property string $transcripts_admin_approval
 */
class TranscriptsAppSettingsForm extends CFormModel {

	public $transcripts_allow_users;
	public $transcripts_admin_approval;
	public $transcripts_require_defined_list;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('transcripts_allow_users,transcripts_admin_approval,transcripts_require_defined_list', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'transcripts_allow_users' => Yii::t('transcripts','Allow users to add their external activities'),
			'transcripts_admin_approval' => Yii::t('transcripts', 'Put the external activities added by users in a waiting list for the admin\'s moderation'),
			'transcripts_require_defined_list' => Yii::t('transcripts', 'Require users to select training institute and courses from defined list.'),
		);
	}



	public function beforeValidate() {
		$this->transcripts_allow_users = ($this->transcripts_allow_users ? 'on' : 'off');
		$this->transcripts_admin_approval = ($this->transcripts_admin_approval ? 'on' : 'off');
		$this->transcripts_require_defined_list = ($this->transcripts_require_defined_list ? 'on' : 'off');
		return parent::beforeValidate();
	}

}
