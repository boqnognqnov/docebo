<?php

/**
 * This is the model class for table "transcripts_course".
 *
 * The followings are the available columns in table 'transcripts_course':
 * @property integer $id
 * @property string $course_name
 * @property string $type
 * @property integer $institute_id
 *
 * The followings are the available model relations:
 * @property TranscriptsRecord[] $transcriptsRecords
 */
class TranscriptsCourse extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transcripts_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course_name, type', 'required'),
			array('institute_id', 'numerical', 'integerOnly'=>true),
			array('course_name, type', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, course_name, type, institute_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transcriptsRecords' => array(self::HAS_MANY, 'TranscriptsRecord', 'course_id'),
            'institute' => array(self::HAS_ONE, 'TranscriptsInstitute', array('id' => 'institute_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'course_name' => Yii::t('transcripts','Course Name'),
			'type' => 'Type',
			'course_type' => Yii::t('transcripts', 'Course Type'),
			'institute_id' => 'Institute',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('course_name',$this->course_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('institute_id',$this->institute_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranscriptsCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';
        $request = Yii::app()->request;

		if ($request->getParam('filter-by-text', false))
			$search = $request->getParam('filter-by-text');

		$criteria = new CDbCriteria();

		if ($search != '') {

			$criteria->addCondition('t.course_name like :search_text');
			$criteria->params[':search_text'] = '%'.$search.'%';
		}

		$config = array();
		$sortAttributes = array('course_name'=>'t.course_name');
		$config['criteria'] = $criteria;
		$config['countCriteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id desc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

    public function renderType() {
        return LearningCourse::model()->getCourseTypeTranslation($this->type);
    }

	public function renderInstitute() {
        static $institutes = null;

		if($institutes === null) {
			$criteria   = new CDbCriteria;
			$institutes = TranscriptsInstitute::model()->findAll( $criteria );
		}

        $instituteList  = CHtml::listData($institutes, 'id', function($data){
            return $data->institute_name;
        });

		if (!empty($this->institute_id)) {
			$content    = CHtml::link(TranscriptsInstitute::getName($this->institute_id), 'javascript:void(0);', array('class' => 'institutesLink'));
		} else {
			$content    = CHtml::link('<span class="assignInstitute" style="color:#ff9326;">'. Yii::t('transcripts', 'Assign a training institute') .'</span>', 'javascript:void(0);', array('class' => 'institutesLink'));
		}

		$content       .= CHtml::dropDownList('TranscriptsInstitutes[id]', (!empty($this->institute_id) ? $this->institute_id : ''), !empty($instituteList) ? $instituteList : array(), array('empty' => Yii::t('transcripts', 'Select Institute'), 'class' => 'institutesLinkSelect', 'data-course-id' => $this->id));

		return $content;
	}

	public function renderActions(){
		$html = CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('TranscriptsApp/TranscriptsManagement/editCourse', array(
				'id' => $this->id
		)), array(
				'class' => 'open-dialog course-action-edit',
                'data-dialog-title' => Yii::t('transcripts', 'Edit Course'),
                'data-dialog-class' => 'modal-edit-course',
                'data-helper-text' => Yii::t('helper', 'Edit Course')
        ));
		$html .= CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>', Docebo::createAdminUrl('TranscriptsApp/TranscriptsManagement/deleteCourse', array(
				'id' => $this->id
		)), array(
				'class' => 'open-dialog course-action-delete',
                'data-dialog-title' => Yii::t('transcripts', 'Delete Course'),
                'data-dialog-class' => 'delete-course'
		));

		return $html;
	}
}
