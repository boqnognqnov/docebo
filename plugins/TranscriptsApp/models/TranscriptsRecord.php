<?php

/**
 * This is the model class for table "transcripts_record".
 *
 * The followings are the available columns in table 'transcripts_record':
 * @property integer $id_record
 * @property integer $id_user
 * @property integer $course_id
 * @property string $course_name
 * @property string $course_type
 * @property string $from_date
 * @property string $to_date
 * @property integer $score
 * @property double $credits
 * @property string $training_institute
 * @property string $certificate
 * @property string $original_filename
 * @property string $status
 * @property integer $id_admin
 * @property string $notes
  *
 * The followings are the available model relations:
 * @property CoreUser $idUser
 * @property CertificationItem $certification
 */
class TranscriptsRecord extends CActiveRecord
{

	const STATUS_WAITING_APPROVAL = 'waiting_approval';
	const STATUS_APPROVED = 'approved';
	const STATUS_REJECTED = 'rejected';

	const STORAGE_COLLECTION = CFileStorage::COLLECTION_USERFILES;
	const SCENARIO_ADMIN_APPROVAL = 'admin-approve';
	const SCENARIO_ADMIN_DELETE = 'admin-delete';
	const SCENARIO_IMPORT = 'insert';

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transcripts_record';
	}

	public function customRequiredCheck($attribute, $params){
		$label = $this->getAttributeLabel($attribute);
		$isRequireToChooseInstitute = (Settings::get('transcripts_require_defined_list') == 'on') ? true : false;
		if($isRequireToChooseInstitute){
			$requiredList = array('id_user', 'to_date', 'course_id');
		}else{
			$requiredList = array('id_user', 'to_date', 'course_name', 'course_type', 'training_institute');
		}
		if(in_array($attribute, $requiredList)) {
			if (empty($this->$attribute)) {
				if ($attribute == 'course_id') {
					$this->addError('course_name', Yii::t('error', 'Invalid input data'));
				} else
					$this->addError($attribute, Yii::t('error', 'Invalid input data'));
			}
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(
                'id_user, course_name, course_id, course_type, to_date, training_institute',
                'customRequiredCheck',
                'except' => array(
                    self::SCENARIO_ADMIN_APPROVAL,
                    self::SCENARIO_ADMIN_DELETE,
					self::SCENARIO_IMPORT
                )
            ),
			array('id_user, score, course_id', 'numerical', 'integerOnly'=>true),
			array('credits', 'numerical'),
			array('course_name, training_institute', 'length', 'max'=>255),
			array('course_type', 'length', 'max'=>9),
			array('certificate', 'length', 'max'=>512),
			array('original_filename', 'length', 'max'=>512),
			array('status', 'length', 'max'=>16),
			array('course_id', 'length', 'max'=>11),
			array('from_date, to_date', 'required', 'on' => 'classroom'),
			array('from_date, to_date', 'safe'),
            array('to_date', 'dateValidation'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_record, id_user, course_name, course_type, from_date, to_date, score, credits, training_institute, certificate, original_filename, status, id_admin, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CoreUser', 'id_user'),
			'admin' => array(self::BELONGS_TO, 'CoreUser', 'id_admin'),
			'certification' => array(self::HAS_ONE, 'CertificationItem', 'id_item', 'on'=>'certification.item_type=:itemTypeTranscript', 'params'=>array(':itemTypeTranscript'=>CertificationItem::TYPE_TRANSCRIPT), 'together'=>true),
			'course'=>array(self::HAS_ONE, 'TranscriptsCourse', array('id'=>'course_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_record' => 'Id Record',
			'id_user' => 'Id User',
			'course_name' => Yii::t('standard', '_COURSE_NAME'),
			'course_type' => Yii::t('course', '_COURSE_TYPE'),
			'from_date' => Yii::t('standard', '_DATE_BEGIN'),
			'to_date' => Yii::t('standard', '_DATE_END'),
			'score' => Yii::t('standard', '_SCORE'),
			'credits' => Yii::t('standard', '_CREDITS'),
			'training_institute' => Yii::t('transcripts', 'Training institute'),
			'certificate' => Yii::t('player', 'Certificate'),
			'original_filename' => Yii::t('player', 'Original filename'),
			'status' => Yii::t('standard', '_STATUS'),
			'id_admin' => 'Id Admin',
			'notes' => Yii::t('standard', '_NOTES')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_record',$this->id_record);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('course_name',$this->course_name,true);
		$criteria->compare('course_type',$this->course_type,true);
		$criteria->compare('from_date',$this->from_date,true);
		$criteria->compare('to_date',$this->to_date,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('credits',$this->credits);
		$criteria->compare('training_institute',$this->training_institute,true);
		$criteria->compare('certificate',$this->certificate,true);
		$criteria->compare('original_filename',$this->original_filename,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranscriptsRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function behaviors() {
		return array(
			'LocalTimeConversionBehavior' => array(
				'class' => 'common.components.LocalTimeConversionBehavior',
				'timestampAttributes' => array(),
				'dateAttributes' => array('to_date', 'from_date')
			)
		);
	}




	//embedded certificate file upload/management
	protected $_certificateFile = null;
	protected $_certificateToBeRemoved = null;

	public function attachCertificateFile($fileName, $newFileName = false) {
		if ($fileName) {
			$this->_certificateFile = (!empty($newFileName) ? array($fileName, $newFileName) : $fileName);
		}
	}

	protected function createUniqueFileName($fileName) {
		$ext = pathinfo($fileName, PATHINFO_EXTENSION);
		return Docebo::generateUuid() . "." . $ext;
	}

	public function getOriginalCertificateFileName() {
		return $this->original_filename;
	}

	public function getCertificateDownloadUrl() {
		if (empty($this->certificate)) return false; //no certificate assigned to this activity

		$url = Docebo::createAbsoluteLmsUrl('TranscriptsApp/TranscriptsUser/downloadCertificate', array(
			'id_record' => $this->id_record,
		));

		return $url;
	}

	public function beforeSave() {

		if ($this->course_type == LearningCourse::TYPE_ELEARNING) {
			$this->from_date = null; //elearning courses do not have an attendance period, but only an obtaining date
		}

		//check if a new certificate file has been specified
		if (!empty($this->_certificateFile)) {

			if (is_array($this->_certificateFile)) {
				$certificateFileNameReal = $this->_certificateFile[0];
				$certificateFileNameAlias = $this->_certificateFile[1];
			} else {
				$certificateFileNameReal = $this->_certificateFile;
				$certificateFileNameAlias = $this->_certificateFile;
			}

			//if yes, store it and save the file name in DB
			$saveFileName = $this->createUniqueFileName($certificateFileNameAlias);

			$storageManager = CFileStorage::getDomainStorage(self::STORAGE_COLLECTION);
			if (!$storageManager->storeAs($certificateFileNameReal, $saveFileName)) {
				throw new CHttpException(500, 'Error while storing certificate name');
			}

			//old certificate file will be removed later
			$this->_certificateToBeRemoved = $this->certificate;

			//if file storage has been successful then update "certificate" field
			$this->certificate = $saveFileName;
			$this->original_filename = $certificateFileNameAlias;


			//clear certificate file name for this AR, no more needed
			$this->_certificateFile = null;
		}

		//Updated the issued certification date
		if (!$this->isNewRecord && PluginManager::isPluginActive('CertificationApp')) {
			$certItem = CertificationItem::model()->findByAttributes(array(
				'id_item' => $this->id_record,
				'item_type' => CertificationItem::TYPE_TRANSCRIPT
			));

			if($certItem){
				$certUser = CertificationUser::model()->findByAttributes(array(
					'id_user' => $this->id_user,
					'id_cert_item' => $certItem->id
				));

				if($certUser){
					$certUser->on_datetime = $this->to_date;
					$certUser->setExpireAt($certItem->certification);
					$certUser->save();
				}
			}
		}

		return parent::beforeSave();
	}




	public function afterSave() {

		//send user notifications, if set
		if (!empty($this->userMessage)) {
			switch ($this->status) {
				case self::STATUS_REJECTED: {
					//send user message
					$this->sendEmailToUser(Yii::t('transcripts', 'Rejected activities'), $this->userMessage);
				} break;
				case self::STATUS_APPROVED: {
					//TODO: send email here too?
				} break;
			}
			$this->userMessage = false;
		}

		//try to remove old certificate file, if set
		if (!empty($this->_certificateToBeRemoved)) {
			$storageManager = CFileStorage::getDomainStorage(self::STORAGE_COLLECTION);
			$storageManager->remove($this->_certificateToBeRemoved);
			$this->_certificateToBeRemoved = null;
		}

		return parent::afterSave();
	}




	public function afterDelete() {
		parent::afterDelete();

		//notify to the user the deletion of its activity
		if ($this->scenario == self::SCENARIO_ADMIN_DELETE && !empty($this->userMessage)) {
			//send user message
			$this->sendEmailToUser(Yii::t('standard', '_USER_STATUS_CANCELLED'), $this->userMessage);
		}

		//try to remove certificate file, if set
		if (!empty($this->certificate)) {
			$storageManager = CFileStorage::getDomainStorage(self::STORAGE_COLLECTION);
			$storageManager->remove($this->certificate);
		}


		if (PluginManager::isPluginActive('CertificationApp')) {
			//Deleting the linked certification with this activity
			$ci = CertificationItem::model()->findByAttributes(array(
				'id_item' => $this->id_record,
				'item_type' => CertificationItem::TYPE_TRANSCRIPT
			));

			if ($ci) {
				$cu = CertificationUser::model()->findByAttributes(array(
					'id_cert_item' => $ci->id,
					'id_user' => $this->id_user
				));

				if ($cu) {
					$cu->delete();
				}
			}
		}
	}





	public static function checkStatusValidity($status) {
		$valid = false;
		switch ($status) {
			case self::STATUS_APPROVED:
			case self::STATUS_WAITING_APPROVAL:
			case self::STATUS_REJECTED: { $valid = true; } break;
		}
		return $valid;
	}


	public function dataProvider($params = array()) {
		$criteria = new CDbCriteria;
		$config = array();

		$criteria->with['user'] = array();

		//filter on text, this is common to all scenarios
		$rq = Yii::app()->request;
		$textFilter = $rq->getParam('filter-by-text', '');
		if (!empty($textFilter)) {
			$query = addcslashes($textFilter, '%_');
			$criteria->addCondition("
				(CONCAT(course.course_name, IF(institute.institute_name IS NOT NULL, CONCAT(' (',institute.institute_name,')'), '')) LIKE :text_match)
				OR
				(CONCAT(t.course_name, IF(t.training_institute IS NOT NULL, CONCAT(' (',t.training_institute,')'), '')) LIKE :text_match)
			");
			$criteria->params[':text_match'] = "%$query%";
			$criteria->with = array('course', 'course.institute');
		}

		//detect if we have to apply power user filter
		$filterPU = false;

		//depending on scenario, some search conditions vary
		switch ($this->scenario) {
			case 'admin-activities-list': {
				$filterPU = true;
				$criteria->addCondition("t.status = :status");
				$criteria->params[':status'] = self::STATUS_APPROVED;
				//user filter
				$userFilter = $rq->getParam('filter-by-user', '');
				if (!empty($userFilter)) {
					$query = addcslashes($userFilter, '%_');
					$criteria->addCondition("user.userid LIKE :user_match");
					$criteria->params[':user_match'] = "%$query%";
				}
			} break;
			case 'admin-waiting-approval': {
				$filterPU = true;
				//user filter
				$userFilter = $rq->getParam('filter-by-user', '');
				if (!empty($userFilter)) {
					$query = addcslashes($userFilter, '%_');
					$criteria->addCondition("user.userid LIKE :user_match");
					$criteria->params[':user_match'] = "%$query%";
				}
				//status filter
				$statusFilter = $rq->getParam('filter-by-status', 'all');

				switch ($statusFilter) {
					case 'all': {
						$criteria->addCondition("t.status = :status1 OR t.status = :status2");
						$criteria->params[':status1'] = self::STATUS_REJECTED;
						$criteria->params[':status2'] = self::STATUS_WAITING_APPROVAL;
					} break;
					case TranscriptsRecord::STATUS_REJECTED: {
						$criteria->addCondition("t.status = :status");
						$criteria->params[':status'] = self::STATUS_REJECTED;
					} break;
					case TranscriptsRecord::STATUS_WAITING_APPROVAL: {
						$criteria->addCondition("t.status = :status");
						$criteria->params[':status'] = self::STATUS_WAITING_APPROVAL;
					} break;
				}
			} break;
			case 'user-activities-list': {
				$criteria->addCondition("t.id_user = :id_user");
				$criteria->params[':id_user'] = (!empty($this->id_user) ? $this->id_user : Yii::app()->user->id);
				//check status filters
				$statusFilter = $rq->getParam('filter-by-status', false); //by default, all status are pre-selected
				if (!is_array($statusFilter)) { $statusFilter = array('approved', 'waiting', 'rejected'); }
				if (!empty($statusFilter)) {
					foreach ($statusFilter as $statusFilterItem) {
						switch ($statusFilterItem) {
							case 'approved': {
								$statusConditions = array("t.status = :status1");
								$criteria->params[':status1'] = self::STATUS_APPROVED;
							} break;
							case 'waiting': {
								$statusConditions[] = "t.status = :status2";
								$criteria->params[':status2'] = self::STATUS_WAITING_APPROVAL;
							} break;
							case 'rejected': {
								$statusConditions[] = "t.status = :status3";
								$criteria->params[':status3'] = self::STATUS_REJECTED;
							} break;
						}
					}
					$criteria->addCondition("(".implode(" OR ", $statusConditions).")");
				}
			} break;
			case 'widgit-approval': {
				$filterPU = true;
				//status filter
				$statusFilter = $rq->getParam('filter-by-status', '');

				if (!Yii::app()->user->isGodAdmin && !Yii::app()->user->getIsPu()){
					$criteria->addCondition("t.id_user = :id_user");
					$criteria->params[':id_user'] = Yii::app()->user->id;
				}

				if ($statusFilter != 'all'){
					$criteria->addCondition("t.status = :status");
					$criteria->params[':status'] = self::STATUS_APPROVED;
				}
			} break;
		}

		if ($filterPU && Yii::app()->user->isPu) {
			$criteria->join = "JOIN core_user_pu pu ON (t.id_user = pu.user_id AND pu.puser_id = :id_pu)";
			$criteria->params[':id_pu'] = Yii::app()->user->id;
			$criteria->together = true;
		}

		$sortAttributes = array();
		foreach ($this->attributeNames() as $attributeName) {
			$sortAttributes[$attributeName] = 't.' . $attributeName;
		}
		//$sortAttributes['userid'] = 'CoreUser.userid';

		$elements_per_page = (isset($params['pageSize']) && !empty($params['pageSize'])) ? $params['pageSize'] : Settings::get('elements_per_page', 10);
		$config['criteria'] = $criteria;

		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.to_date DESC';
		$config['pagination'] = array(
			'pageSize' => $elements_per_page
		);

		return new CActiveDataProvider(get_class($this), $config);
	}



	/**
	 * Retrieve activity user userid (already relativized)
	 *
	 * @return string|boolean the relative userid of the user, false if no user or invalid user is assigned
	 */
	public function getUserid() {
		if (empty($this->id_user)) return false;
		$user = CoreUser::model()->findByPk($this->id_user);
		if (empty($user)) return false;
		return Yii::app()->user->getRelativeUsername($user->userid);
	}



	/**
	 * Count how many transcripts are waiting for admin approval. The count is filtered by admin level.
	 *
	 * @return integer|boolean the number of waiting transcripts (false if error)
	 */
	public function countWaitingForApproval($ignorePermissionCheck = false) {

		//preliminary checkings
		if (Settings::get('transcripts_admin_approval', 'off') == 'off' && !$ignorePermissionCheck) return false; //if no approval is required, this function is meaningless
		if (Yii::app()->user->isUser && !$ignorePermissionCheck) return false; //normal users are not allowed to see this info

		//compose the count query
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(self::model()->tableName()." tr")
			->where(
				"tr.status = :status1 OR tr.status = :status2",
				array(':status1' => self::STATUS_WAITING_APPROVAL, ':status2' => self::STATUS_REJECTED)
			);
		//set power user filter, if needed
		if (Yii::app()->user->isPu) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = tr.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}

		if (Yii::app()->user->getIsUser()) {
			$command->andWhere("tr.id_user = :id_user", array(':id_user' => Yii::app()->user->id));
		}

		//do the count and return the integer result
		$count = $command->queryScalar();
		return (int)$count;
	}



	/**
	 * Count an user activities filtering by status
	 *
	 * @param integer $idUser the idst of the specified user
	 * @param string $status an activity status to search for. Must be valid
	 * @return bool|int the number of activities with the specified status, "false" if error
	 */
	public function countUserActivitiesByStatus($idUser, $status) {

		//preliminary checkings
		if (Settings::get('transcripts_admin_approval', 'off') == 'off') return false; //if no approval is required, this function is meaningless
		if (!self::checkStatusValidity($status)) return false; //specified status must be a valid value

		//compose the count query
		$command = Yii::app()->db->createCommand()
			->select("COUNT(*)")
			->from(self::model()->tableName()." tr")
			->where("tr.status = :status", array(':status' => $status))
			->andWhere("tr.id_user = :id_user", array(':id_user' => $idUser));

		//do the count and return the integer result
		$count = $command->queryScalar();
		return (int)$count;
	}



	public function getMaxScore() {
		return 100;
	}


	protected $_userMessage = false;

	public function getUserMessage() {
		return (!empty($this->_userMessage) ? "".$this->_userMessage : false);
	}


	public function setUserMessage($userMessage) {
		$this->_userMessage = "".$userMessage; //force casting to string
	}


	public function reject($idAdmin = false, $userMessage = false) {
		if ($this->getIsNewRecord()) return false; //no physical record exists yet
		if ($this->status == self::STATUS_REJECTED) return false; //record is already in "rejected" status
		$this->status = self::STATUS_REJECTED;
		if ((int)$idAdmin > 0) {
			$this->id_admin = (int)$idAdmin;
		} elseif ($this->scenario == self::SCENARIO_ADMIN_DELETE) {
			$this->id_admin = Yii::app()->user->id;
		}
		if (!empty($userMessage)) { $this->userMessage = $userMessage; }
		if (!empty($this->userMessage)) { $this->notes = $this->userMessage; }

		return $this->save();
	}


	public function approve($idAdmin = false, $userMessage = false) {
		if ($this->getIsNewRecord()) return false; //no physical record exists yet
		if ($this->status == self::STATUS_APPROVED) return false; //record is already in "rapproved" status
		$this->status = self::STATUS_APPROVED;
		if ((int)$idAdmin > 0) {
			$this->id_admin = (int)$idAdmin;
		} elseif ($this->scenario == 'admin-approve') {
			$this->id_admin = Yii::app()->user->id;
		}
		//if (!empty($userMessage)) { $this->userMessage = $userMessage; }
		//if (!empty($this->userMessage)) { $this->notes = $this->userMessage; }

		// Raise an event upon approval
		$eventParams = array(
			'transcript' 	=> $this,
			'user'			=> $this->user
		);
		Yii::app()->event->raise('ExternalTrainingApproved', new DEvent($this, $eventParams));


		return $this->save();
	}



	protected function sendEmailToUser($subject, $body) {
		//retrieve user info and email
		$user = CoreUser::model()->findByPk($this->id_user);
		if (!$user || !empty($user->email)) return false; //unable to send email

		//retrieve admin email
		$admin = CoreUser::model()->findByPk($this->id_admin);
		$fromAdminEmail = ($admin && !empty($admin->email) ? $admin->email : Settings::get('mail_sender'));

		if (empty($fromAdminEmail)) {
			//no sender email can be specified so stop here, otherwise a system exception will be raised
			Yii::log(get_class($this).'::'.__METHOD__.' : unable to find a mail sender address', CLogger::LEVEL_ERROR);
			return false;
		}

		//send email
		try {
			$mm = new MailManager();
			$mm->setContentType('text/html');
			$result = $mm->mail($fromAdminEmail, $user->email, $subject, $body);
		} catch (Exception $e) {
			Yii::log(get_class($this).'::'.__METHOD__.' : '.$e->getMessage(), CLogger::LEVEL_ERROR);
			$result = false;
		}
		return $result;
	}

    public function dateValidation($to_date) {
        if ($this->$to_date == '') {
            return true;
        }
		if(!empty($this->course_id)){
			// we get the course type by relation from predefined courses and institutes
			$courseType = Yii::app()->db->createCommand()->select('type')->from(TranscriptsCourse::model()->tableName())->where('id=:id',
					array(':id' => $this->course_id))->queryScalar();
			if ($courseType != LearningCourse::TYPE_ELEARNING && strtotime(Yii::app()->localtime->fromLocalDateTime($this->$to_date)) * 1000 < strtotime(Yii::app()->localtime->fromLocalDateTime($this->from_date)) * 1000 ) {
				$this->addError('to_date', Yii::t('transcripts', 'Date end cannot be lesser than from date'));
				return false;
			}
		}elseif ($this->course_type != LearningCourse::TYPE_ELEARNING && strtotime(Yii::app()->localtime->fromLocalDateTime($this->$to_date)) * 1000 < strtotime(Yii::app()->localtime->fromLocalDateTime($this->from_date)) * 1000 ) {
            $this->addError('to_date', Yii::t('transcripts', 'Date end cannot be lesser than from date'));
            return false;
        }
        return true;
    }

	/*
	 * @param array $fields Transcripts Additional Fields
	 */
	public function updateAdditionalFiled($fields){
		if(empty($fields)) return;
		$fieldModel = TranscriptsFieldValue::model()->findByPk($this->id_record);
		if(!$fieldModel){
			$fieldModel = new TranscriptsFieldValue();
			$fieldModel->id_record = $this->id_record;
			$fieldModel->save();
		}
		$columns = array();
		foreach($fields as $id => $field){
			$fieldValue = $field['value'];
			if($field['type'] == TranscriptsField::TYPE_FIELD_DATE)
				$fieldValue = ($fieldValue) ? Yii::app()->localtime->fromLocalDate($fieldValue) : null;

			$columns["field_" . $id] = $fieldValue;
		}

		if($columns !== array()){
			Yii::app()->db->createCommand()->update(
				TranscriptsFieldValue::model()->tableName(),
				$columns,
				'id_record = :record',
				array(':record' => $this->id_record)
			);
		}
	}

	/**
	 * Returns the value of the field for this course (NO MODELS)
	 * @param $fieldId - Id of the course field
	 * @param $recordId - Optional id of the course to take it's value
	 * @param $dropdownKeyInsteadOfValue - Determines whether we want the key instead of the value of a dropdown field
	 */
	public function getAdditionalFieldValue($fieldId, $id_record=false, $dropdownKeyInsteadOfValue = false){
		$fieldType = Yii::app()->db->createCommand()
			->select('type')
			->from(TranscriptsField::model()->tableName())
			->where('id_field = '.((int)$fieldId))
			->queryScalar();

		$rid = ($id_record)? $id_record : $this->id_record;
		$result = Yii::app()->db->createCommand()
			->select('field_'.$fieldId.' v')
			->from(TranscriptsFieldValue::model()->tableName().' fv')
			->where('id_record = '.$rid)
			->queryScalar();

		if($result && ($fieldType==TranscriptsField::TYPE_FIELD_DATE || $fieldType==TranscriptsField::TYPE_FIELD_DROPDOWN )) {
			if( $fieldType==TranscriptsField::TYPE_FIELD_DATE){
				return Yii::app()->localtime->toLocalDate($result);
			}elseif($fieldType==TranscriptsField::TYPE_FIELD_DROPDOWN) {
				if($dropdownKeyInsteadOfValue){
					return $result;
				}
				else {
					$translation = Yii::app()->db->createCommand()
						->select('translation')
						->from(TranscriptsFieldDropdownTranslations::model()->tableName())
						->where('id_option = ' . $result . ' AND lang_code = "' . Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) . '"')
						->queryScalar();
					if (!$translation)
						$translation = Yii::app()->db->createCommand()
							->select('translation')
							->from(TranscriptsFieldDropdownTranslations::model()->tableName())
							->where('id_option = ' . $result . ' AND lang_code = "' . Settings::get('default_language') . '"')
							->queryScalar();

					$result = $translation;
				}
			}
		}
		return $result;
	}

	public function renderStatus()
	{
		switch ($this->status) {
			case self::STATUS_REJECTED: {
				return CHtml::link('&nbsp;', '', array(
						'class' => 'transcripts-sprite reject-small',
						'title' => Yii::t('transcripts', 'Rejected'),
						'rel' => 'tooltip',
				));
			} break;
			case self::STATUS_APPROVED: {
				return CHtml::link('&nbsp;', '', array(
						'class' => 'transcripts-sprite approve-small',
						'title' => Yii::t('transcripts', 'Approved'),
						'rel' => 'tooltip',
				));
			} break;
			case self::STATUS_WAITING_APPROVAL: {
				return CHtml::link('&nbsp;', '', array(
						'class' => 'transcripts-sprite waiting-small',
						'title' =>  Yii::t('catalogue', '_WAITING_APPROVAL'),
						'rel' => 'tooltip',
				));
			} break;
		}

	}

	public function renderTrainingInstituteName()
	{
		if((int)$this->course_id > 0) {
			$transcriptsCourse = TranscriptsCourse::model()->with('institute')->findByPk($this->course_id);
			if(Settings::get('transcripts_require_defined_list') == 'off')
				return '<span style="color: #aaa">'.$transcriptsCourse->institute->institute_name.'</span>';
			return $transcriptsCourse->institute->institute_name;
		}
		if(Settings::get('transcripts_require_defined_list') == 'on')
			return '<span style="color: #aaa">'.$this->training_institute.'</span>';
		return $this->training_institute;
	}

	public function renderCourseName()
	{
		if((int)$this->course_id > 0) {
			$transcriptsCourse = TranscriptsCourse::model()->findByPk($this->course_id);
			if(Settings::get('transcripts_require_defined_list') == 'off')
				return '<span style="color: #aaa">'.$transcriptsCourse->course_name.'</span>';
			return $transcriptsCourse->course_name;
		}
		if(Settings::get('transcripts_require_defined_list') == 'on')
			return '<span style="color: #aaa">'.$this->course_name.'</span>';
		return $this->course_name;
	}

	public static function getCourseInstituteFields(){

		$listOfInstitutes = array();
		$coursesPerInstitutes = array();
		$listCoursesTypes = array();
		$isRequireToChooseInstitute = (Settings::get('transcripts_require_defined_list') == 'on') ? true : false;
		if($isRequireToChooseInstitute){
			$listOfInstitutesFromDB=Yii::app()->db->createCommand()
					->select('ti.*')->from(TranscriptsInstitute::model()->tableName().' ti')
					->join(TranscriptsCourse::model()->tableName().' tc', 'ti.id=tc.institute_id')->queryAll();
			if(!empty($listOfInstitutesFromDB))
				foreach ($listOfInstitutesFromDB as $institute) {
					$listOfInstitutes[$institute['id']] = $institute['institute_name'];
				}
			$coursesPerInstitutesDB = Yii::app()->db->createCommand()
					->select('tc.*')->from(TranscriptsCourse::model()->tableName() . ' tc')
					->join(TranscriptsInstitute::model()->tableName() . ' ti', 'tc.institute_id=ti.id')
					->where('tc.institute_id IS NOT NULL')->queryAll();

			if(!empty($coursesPerInstitutesDB))
				foreach ($coursesPerInstitutesDB as $course) {
					$listCoursesTypes[$course['id']] = $course['type'];
					$courseName = Yii::app()->htmlpurifier->purify(preg_replace('/\t+/', '', $course['course_name']));
					$courseName = html_entity_decode($courseName);
					if (isset($coursesPerInstitutes[$course['institute_id']])) {
						$coursesPerInstitutes[$course['institute_id']][$course['id']] = $courseName;
					} else {
						$coursesPerInstitutes[$course['institute_id']] = array(
								$course['id'] => $courseName
						);
					}
				}
		}

		return array($listOfInstitutes, $coursesPerInstitutes, $isRequireToChooseInstitute, $listCoursesTypes);
	}
}
