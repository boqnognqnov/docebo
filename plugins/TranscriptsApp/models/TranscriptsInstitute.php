<?php

/**
 * This is the model class for table "transcripts_institute".
 *
 * The followings are the available columns in table 'transcripts_institute':
 * @property integer $id
 * @property string $institute_name
 */
class TranscriptsInstitute extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transcripts_institute';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('institute_name', 'required'),
			array('institute_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, institute_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'institute_name' => 'Institute Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('institute_name',$this->institute_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TranscriptsInstitute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function dataProvider(){
		$search = '';
        $request = Yii::app()->request;

		if ($request->getParam('filter-by-text', false))
			$search = $request->getParam('filter-by-text');

		$criteria = new CDbCriteria();

		if ($search != '') {

			$criteria->addCondition('t.institute_name like :search_text');
			$criteria->params[':search_text'] = '%'.$search.'%';
		}

		$config = array();
		$sortAttributes = array('institute_name'=>'t.institute_name');
		$config['criteria'] = $criteria;
		$config['countCriteria'] = $criteria;
		$config['sort']['attributes'] = $sortAttributes;
		$config['sort']['defaultOrder'] = 't.id desc';
		$config['pagination'] = array (
				'pageSize' => Settings::get('elements_per_page', 10)
		);

		return new CActiveDataProvider(get_class($this), $config);
	}

    public static function getName($id) {
        $model = self::model()->findByPk($id);

        if($model) return $model->institute_name;
        return null;
    }

	public function renderActions(){
		$html = CHtml::link('<span class="i-sprite is-edit">&nbsp;</span>', Docebo::createAdminUrl('TranscriptsApp/TranscriptsManagement/editInstitute', array(
				'id' => $this->id
		)), array(
				'class' => 'open-dialog institute-action-edit',
                'data-dialog-title' => Yii::t('transcripts', 'Edit Institute'),
                'data-dialog-class' => 'modal-edit-institute',
                'data-helper-text' => Yii::t('helper', 'Edit Institute')
        ));
		$html .= '&nbsp;&nbsp;&nbsp;'.CHtml::link('<span class="i-sprite is-remove red">&nbsp;</span>&nbsp;&nbsp;&nbsp;', Docebo::createAdminUrl('TranscriptsApp/TranscriptsManagement/deleteInstitute', array(
				'id' => $this->id
		)), array(
				'class' => 'open-dialog institute-action-delete',
                'data-dialog-title' => Yii::t('transcripts', 'Delete Institute'),
                'data-dialog-class' => 'delete-Institute'
		));

		return $html;
	}
}
