<?php
/**
 * This is the model class for table "transcripts_field".
 *
 * The followings are the available columns in table 'transcripts_field':
 * @property integer $id_field
 * @property string $type
 * @property integer $sequence
 * @property string $settings
 *
 * The followings are the available model relations:
 * @property TranscriptsFieldDropdown[] $transcriptsFieldDropdowns
 * @property TranscriptsFieldTranslation[] $transcriptsFieldTranslations
 *
 * @property string $transcriptsEntry
 * @property string $lang_code
 * @property TranscriptsRecord $record
 */
class TranscriptsField extends CActiveRecord
{
    /**
     * Array of translations for this field in all available languages
     */
    private $fieldTranslations = array();

    public $maxSequence;
    public $transcriptsEntry = '';
    public $lang_code;
    public $record = null;

    /* FIELD TYPES */
    const TYPE_FIELD_DATE = 'date';
    const TYPE_FIELD_DROPDOWN = 'dropdown';
    const TYPE_FIELD_TEXTFIELD = 'textfield';
    const TYPE_FIELD_TEXTAREA = 'textarea';

    /**
     * Mapping between field types and value column type
     * @var array
     */
    private $_valueColumnType = array(
        self::TYPE_FIELD_DATE => 'DATE',
        self::TYPE_FIELD_DROPDOWN => 'SMALLINT',
        self::TYPE_FIELD_TEXTAREA => 'TEXT',
        self::TYPE_FIELD_TEXTFIELD => 'VARCHAR(255)'
    );

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'transcripts_field';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('sequence', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max'=>60),
            array('id_field, type, sequence', 'safe', 'on'=>'search'),
            array('id_field, type, sequence, settings, mandatory', 'safe', 'on'=>'clone')
        );
    }

    public function renderFilterField(){
        return '';
    }

    public function getSubclassedInstance() {
        $subclassType = $this->type ? ('TranscriptsFieldType' . ucfirst($this->type)) : 'TranscriptsField';
        $instance = new $subclassType('clone');
        $instance->attributes = $this->attributes;
        return $instance;
    }

    public function renderFieldSettings() {
        return '';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'transcriptsFieldDropdown' => array(self::HAS_MANY, 'TranscriptsFieldDropdown', 'id_field'),
            'transcriptsFieldTranslation' => array(self::HAS_MANY, 'TranscriptsFieldTranslation', 'id_field'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_field'          => 'Id Field',
            'type'              => Yii::t('standard', '_TYPE'),
            'sequence'          => Yii::t('manmenu', '_ORDER'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id_field',$this->id_field);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('sequence',$this->sequence);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TranscriptsField the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @see CActiveRecord::afterSave()
     */
    public function afterSave () {
        // Load all language codes
        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();

        // 1. Translations handling
        if($this->getIsNewRecord()) {

            // New record -> create all translations
            $sql = "INSERT INTO ".TranscriptsFieldTranslation::model()->tableName()." (id_field, lang_code, translation) VALUES ";
            $rows= array();
            foreach($activeLanguagesList as $key => $language)
                $rows[] = "(".$this->id_field.",'".$key."', ".Yii::app()->db->quoteValue(isset($this->fieldTranslations[$key]) ? $this->fieldTranslations[$key] : '').")";
            $sql .= implode(", ", $rows);
            Yii::app()->db->createCommand($sql)->execute();

        } else if(!empty($this->fieldTranslations)) {

            // Existing record -> update translations only if set in fieldTranslations
            $sql = '';
            foreach($this->fieldTranslations as $key => $translation)
                $sql .= "UPDATE " . TranscriptsFieldTranslation::model()->tableName() . " SET translation = " . Yii::app()->db->quoteValue($translation) .
                    " WHERE id_field = " . $this->id_field . " AND lang_code = '" . $key . "'";
            if($sql)
                Yii::app()->db->createCommand($sql)->execute();
        }

        // 2. Value table handling
        if($this->getIsNewRecord()) {
            // Add new column to the value table
            $sql = "ALTER TABLE ".TranscriptsFieldValue::model()->tableName()."
  					ADD COLUMN field_".$this->id_field." ".$this->_valueColumnType[$this->type]." DEFAULT NULL";

            try {
                // Syntax only available for MYSQL >= 5.6
                Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
            } catch(CDbException $e) {
                // Well, looks like MYSQL version doesn't like this syntax
                Yii::app()->db->createCommand($sql)->execute();
            }
        }

        parent::afterSave();
    }

    public function afterDelete() {

        // After deleting the field, remove the column from transcripts_field_value
        try {
            $sql = "ALTER TABLE ".TranscriptsFieldValue::model()->tableName()." DROP COLUMN field_".$this->id_field;

            try {
                // Syntax only available for MYSQL >= 5.6
                Yii::app()->db->createCommand($sql.", ALGORITHM = INPLACE, LOCK = NONE")->execute();
            } catch(CDbException $e) {
                // Well, looks like MYSQL version doesn't like this syntax
                Yii::app()->db->createCommand($sql)->execute();
            }

            parent::afterDelete();
        } catch(Exception $e) {
        	Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Returns the next sequence number
     * @return int
     */
    public function getNextSequence()
    {
        $criteria=new CDbCriteria;
        $criteria->select = 'max(sequence) as maxSequence';
        $res = $this->model()->find($criteria);
        if(is_null($res->maxSequence))
            $max_seq = 0;
        else
            $max_seq = $res->maxSequence + 1;

        return $max_seq;
    }

    /**
     * Sets the fields translations to be saved in afterSave
     * @param $translations
     */
    public function setFieldTranslations($translations) {
        if(!empty($translations))
            $this->fieldTranslations = $translations;
    }

    /**
     * @return CArrayDataProvider
     */
    public function getAdditionalTranscriptsFieldsDataProvider()
    {
        $params = Yii::app()->request->getParam('TranscriptsFieldTranslation');
        $translation = $params['translation'];
        $criteria = new CDbCriteria(array('order' => 'sequence asc'));
        if ( $translation ) {
            $criteria->join = 'LEFT JOIN transcripts_field_translation st ON st.id_field = t.id_field';
            $criteria->join .= ' LEFT JOIN transcripts_field_translation sst ON sst.id_field = st.id_field AND st.translation = "" AND sst.lang_code = "'.Settings::get('default_language').'"';
            $criteria->addCondition('st.lang_code = :code');
            $criteria->addCondition('st.translation LIKE "%'.$translation.'%" OR sst.translation LIKE "%'.$translation.'%"');
            $criteria->params[':code'] = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        }

        $rawData = $this->findAll($criteria);
        $defaultElements = 10;
        $coreSetting = CoreSetting::model()->findByAttributes(array('param_name' => 'elements_per_page'));
        $elementsPerPage = $coreSetting->param_value ? $coreSetting->param_value : $defaultElements;
        /*=- If there is not set elements per page in Advance Setting default is 10 -=*/
        return new CArrayDataProvider($rawData, array(
            'keyField'      => 'id_field',
            'pagination' => array(
                'pageSize' => $elementsPerPage,
            ),
        ));
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return self::getCategoryNameForType($this->type);
    }

    /**
     * Generates the transcripts field label (localized)
     * @param $type
     * @return string
     */
    public static function getCategoryNameForType($type) {
        if(!in_array($type, TranscriptsField::getTranscriptsFieldsTypes())) {
            $event = new DEvent("self", array('type' => $type));
            Yii::app()->event->raise('GetTranscriptsFieldLabel', $event);
            if($event->return_value['label'])
                return $event->return_value['label'];
            else
                return 'Unknown field type';
        } else {
            // Here we try to reuse the "freetext" translation
            return Yii::t('field', '_' . strtoupper(($type == self::TYPE_FIELD_TEXTAREA) ? 'freetext' : $type));
        }
    }

    /**
     * @return array
     */
    public static function getTranscriptsFieldsTypes()
    {
        return array(self::TYPE_FIELD_DATE, self::TYPE_FIELD_DROPDOWN, self::TYPE_FIELD_TEXTAREA, self::TYPE_FIELD_TEXTFIELD);
    }

    /**
     * @param null $id_field
     * @return mixed
     */
    public function getAdditionalFieldsTranslationName($id_field=null)
    {
        $lang = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        static $translations = array();

        if(!isset($translations[$id_field])) {
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(TranscriptsFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => $lang,
                ))
                ->queryScalar();
        }

        if ( empty($translations[$id_field]) || $translations[$id_field] === '') {
            /* If there is no translation for this $id_field apply the default(english) */
            $translations[$id_field] = Yii::app()->getDb()->createCommand()
                ->select('translation')
                ->from(TranscriptsFieldTranslation::model()->tableName())
                ->where('id_field=:id_field', array(':id_field' => $id_field))
                ->andWhere('lang_code=:current', array(
                    ':current' => Settings::get('default_language', 'english'),
                ))
                ->queryScalar();
        }

        return $translations[$id_field];
    }


    public function getTranslation($idField=false, $lang_code = false, $returnDefault = true){
        $lang_code = !$lang_code ? Lang::getCodeByBrowserCode(Yii::app()->getLanguage()) : $lang_code;
        $fid = ($idField)? $idField : $this->id_field;
        $translation = Yii::app()->db->createCommand()
            ->select('translation')
            ->from(TranscriptsFieldTranslation::model()->tableName().' ft')
            ->where('id_field = '.$fid.' AND lang_code = "'.$lang_code.'"')
            ->queryScalar();
        if(!$translation && $returnDefault){
            $translation = Yii::app()->db->createCommand()
                ->select('translation')
                ->from(TranscriptsFieldTranslation::model()->tableName().' ft')
                ->where('id_field = '.$fid.' AND lang_code = "'.Settings::get('default_language').'"')
                ->queryScalar();
        }
        return $translation;
    }

    public function getConditionsOptions() {
        return array(
            'contains' => Yii::t('standard','_CONTAINS'),
            'equal' => Yii::t('standard','_EQUAL'),
            'not-equal' => Yii::t('standard','_NOT_EQUAL'),
        );
    }

    public function getTextareaConditionsOptions() {
        return array(
            'contains' => Yii::t('standard','_CONTAINS'),
            'not-contains' => Yii::t('standard','_NOT_CONTAINS')
        );
    }

    public function validateFieldEntry() {
        if ($this->mandatory) {
            if (empty($this->transcriptsEntry)) {
                $this->addError('transcriptsEntry', Yii::t('register', '_SOME_MANDATORY_EMPTY'));
            }
        }
    }

    public function renderValue($transcriptsEntry, $transcriptsEntryModel=null) {
        $className = 'TranscriptsFieldType' . ucfirst($this->type);
        $field = $className::model()->findByAttributes(array('id_field' => $this->id_field));
        return $field->renderFieldValue($transcriptsEntry, $transcriptsEntryModel);
    }

    public static function renderFieldValue($transcriptsEntry, $transcriptsEntryModel=null, $renderedWhere) {
        return '';
    }

    private function populateField() {
        $className = 'TranscriptsField' . ucfirst($this->type);
        return $className::model()->findByAttributes(array('id_field' => $this->id_field));
    }

    public function renderFilter() {
        $field = $this->populateField();
        return $field->renderFilterField();
    }

    public static function hasAnyField(){
        $fieldCount = self::model()->count();

        return (($fieldCount > 0) ? true : false);
    }

    public function prepareDropdownData( ) {
        $data = array();
        $data[$this->id_field] = array();
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
        $dropdownModels = TranscriptsFieldDropdown::model()->findAllByAttributes(array('id_field'  => $this->id_field));
        $dropdownOptions = array();
        foreach ( $dropdownModels as $model ) {
            $dropdownOptions[$model->id_option] = $model->id_option;
        }
        foreach ( $activeLanguagesList as $lang_code => $language) {
            $result = Yii::app()->db->createCommand()
                ->select('*')
                ->from(TranscriptsFieldDropdownTranslations::model()->tableName())
                ->where('lang_code = :lang_code',array(':lang_code'    => $lang_code))
                ->andWhere(array('in', 'id_option', $dropdownOptions))
                ->queryAll();

            $data[$this->id_field][$lang_code] = array(
                'label'     => $this->getTranslation(false, $lang_code, false),
                'options'   => array()
            );
            foreach ( $result as $res ) {
                $data[$this->id_field][$lang_code]['options'][$res['id_option']] = $res['translation'];
            }
        }

        return $data;
    }

    /**
     * Returns a deserialized settings array for this field model
     * @return array|mixed
     */
    public function getSettingsArray() {
        return $this->settings ? CJSON::decode($this->settings) : array();
    }

    public function getFieldEntry($idTranscript){
        $fieldClass = 'TranscriptsFieldType' . ucfirst($this->type);
        $transcriptsField = $fieldClass::model()->findByPk($this->id_field);
        $tempValue = Yii::app()->getDb()->createCommand()
            ->select('field_'.$this->id_field)
            ->from(TranscriptsFieldValue::model()->tableName().' v')
            ->where('v.id_record=:record', array(':record' => $idTranscript))
            ->queryScalar();
        $transcriptsField->transcriptsEntry = $tempValue;
        $resultValue = $fieldClass::renderFieldValue($transcriptsField->transcriptsEntry);
        return $resultValue;
    }
}