<?php

/**
 * This is the model class for table "transcripts_field_value".
 *
 * The followings are the available columns in table 'transcripts_course_field_value':
 * @property integer $id_record
 *
 * The followings are the available model relations:
 * @property TranscriptsRecord #id_record
 */
class TranscriptsFieldValue extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'transcripts_field_value';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id_record', 'required'),
            array('id_record', 'numerical', 'integerOnly'=>true),
            array('id_record', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'id_record' => array(self::BELONGS_TO, 'TranscriptsRecord', 'id_record'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_record' => 'Id Record'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id_record',$this->id_record);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TranscriptsFieldValue the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
