<?php
/* @var $form CActiveForm */
/* @var $settings TranscriptsAppSettingsForm */
$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('transcripts', 'External activities') => Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList'),
	Yii::t('transcripts', 'Settings'),
);
$all_settings = $settings->attributeLabels();
?>

<?php DoceboUI::printFlashMessages(); ?>
<?= CHtml::beginForm(); ?>
<div class="advanced-main">
    <div class="row-fluid row" style="margin-left:0!important;border-top: 1px solid #e4e6e5; border-bottom: 1px solid #e4e6e5;">
        <div class="row" style="background: #f1f3f2; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff; padding: 22px 18px;margin-left:0!important;">
            <div class="span2">
                <h4 style="padding-left: 18px;color:#333333;"><?= Yii::t('standard', 'Settings') ?></h4>
            </div>
            <div id="external-training-settings" class="span10">
                <?php foreach($all_settings as $name => $descr): ?>
                <div class="control-group row-fluid">
                    <?php
                    echo CHtml::checkBox("TranscriptsAppSettingsForm[{$name}]", $settings[$name], array('uncheckValue' => 0));
                    echo CHtml::label($descr, "TranscriptsAppSettingsForm_{$name}", array(
                        'style' => 'display: inline-block; margin-left: 10px;'
                    ));
                    if($name == 'transcripts_require_defined_list') {
                        echo '<a href="'.Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes').'" class="redirect-url">' . Yii::t('transcripts', 'Configure here courses and training institutes.') . '</a>';
                    }
                    ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row-fluid text-right row even" style="margin-left:0!important;">
        <?= CHtml::submitButton(Yii::t('standard', '_SAVE'), array(
            'class' => 'btn btn-docebo green big',
            'name' => 'submit'
        )); ?>
        <?= CHtml::link('Cancel', Yii::app()->createUrl('site/index'), array(
            'class' => 'btn btn-docebo black big',
            'name' => 'cancel'
        )); ?>
    </div>
</div>
    <?= CHtml::endForm(); ?>

<script type="text/javascript">
    $(function(){
        $('input[type=checkbox]').styler();

        if ( $(".alert-success").length) {
            setInterval(function(){
                $(".alert-success").fadeOut(1500);
            }, 3000);
        }
    });

</script>

<style>

    div.advanced-main a.redirect-url {
        color: #08c;
        text-decoration: underline;
    }

</style>