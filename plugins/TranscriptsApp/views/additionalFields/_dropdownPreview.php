<table class="preview-wrapper">
    <tr>
        <td class="preview-border">
            <table>
                <tr>
                    <td class="language-select">
                        <span class="preview"><?php echo Yii::t('standard', '_PREVIEW'); ?></span>
                        <p><?php echo Yii::t('standard', '_LANGUAGE'); ?>: <strong class="lang-code"></strong></p>
                    </td>
                    <td class="preview-viewport-wrapper">
                        <table>
                            <tr class="preview-viewport">

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $(function() {

        Docebo.log(DD);
        //refreshDropdownPreview();


        $('#TranscriptsFieldDropdownTranslation_lang_code').live('change', function () {
            DD.refreshDropdownPreview();
        })


        $('.orgChart_translation').blur(function() {
            DD.refreshDropdownPreview();
        })

        $('.field-options').on('blur', '.dropdownOption', function() {
            DD.refreshDropdownPreview();
        })
    });
</script>