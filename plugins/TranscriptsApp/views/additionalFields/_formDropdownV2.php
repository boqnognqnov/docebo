<div class="form dropdown">
    <?php $defaultLanguage = CoreLangLanguage::getDefaultLanguage(); ?>
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'add-field-form',
        'action' => $model->isNewRecord ? array('additionalFields/createDropdown') : array('additionalFields/editDropdown', 'id' => $model->id_field),
    )); ?>


    <div class="dropdown_form_title"><?php echo Yii::t('branding', '_PAGE_LANGUAGE_LABEL'); ?></div>
    <div class="clearfix">
        <div class="languagesList">

            <?php echo $form->dropDownList($modelTranslation, 'lang_code', $activeLanguagesList, array('class' => 'additionalFieldTypeDropdown')); ?>
            <?php echo $form->error($modelTranslation, 'lang_code'); ?>
            <div class="orgChart_languages">
                <div><?php echo Yii::t('admin_lang', '_LANG_ALL') . ': <span>' . count($activeLanguagesList).'</span>'; ?></div>
                <div id="orgChart_assignedCount"><?php echo Yii::t('standard', 'Filled languages') . ' <span>'.((!empty($translationsList))?count(array_filter($translationsList)):0).'</span>'; ?></div>
            </div>
        </div>

        <div class="orgChart_translationsList">
            <?php

            $currentLanguage = false;

            if (!empty($activeLanguagesList)) {

                $currentLanguage = (!empty($modelTranslation->lang_code) ? $modelTranslation->lang_code : $first_key);

                if ( !empty($model->id_field))
                    $preparedData = $model->prepareDropdownData();

                foreach ($activeLanguagesList as $key => $lang) {


                    if (($key == $first_key && empty($modelTranslation->lang_code)) || ($modelTranslation->lang_code == $key)) {
                        $class = 'show';
                    } else {
                        $class = 'hide';
                    }
                    ?>
                    <div id="CoreOrgChart_<?= $key ?>" class="languagesName <?php echo $key . ' ' . $class; ?>">
                        <div class="field-name" style="position: relative;">
                            <div class="orgChart_form_title"><?php echo Yii::t('standard', '_FIELD_NAME', array($lang)); ?></div>
                            <?php echo $form->textField($modelTranslation, 'translation[' . $key . '][field]', array('class' => 'orgChart_translation'.($defaultLanguage == $key ? ' default_lang' : ''), 'maxlength' => '255', 'value' => (!empty($translationsList[$key]['field']) ? $translationsList[$key]['field'] : !empty($preparedData[$model->id_field][$key]['options']) ? $preparedData[$model->id_field][$key]['label'] : ''))); ?>
                        </div>
                        <div id="field-options-<?= $key ?>" class="field-options" data-lang="<?php echo $key; ?>" style="position: relative;">
                            <div class="orgChart_form_title"><?php echo Yii::t('test', '_TEST_QUEST_ELEM') , " ($lang)"; ?></div>
                            <div id="field-options-inputs-<?= $key ?>">
                                <!-- here the fields list to be printed by JS -->
                                <?php
                                if ( !empty($model->id_field)) {
                                    $i=0;
                                    foreach ( $preparedData[$model->id_field][$key]['options'] as $optionKey => $optionItem) {
                                        echo '<input
                                       type="text"
                                       placeholder=""
                                       class="dropdownOption"
                                       value="'.$optionItem.'"
                                       data-id-option="'.$optionKey.'"
                                       id="translation_'.$key.'_options_translation_'.$i.'" name="TranscriptsFieldTranslation[translation]['.$key.'][options][translation][]"
                                            >';
                                        $i++;
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
            <?php } ?>
        </div>
    </div>

    <?php echo $model->getSubclassedInstance()->renderFieldSettings(); ?>

    <?php echo $this->renderPartial('_dropdownPreview'); ?>
    
    <?php echo $form->hiddenField($model, 'type', array('value' => $model->type)); ?>
    <?php $this->endWidget(); ?>
</div>
<div class="saving-dropdown-field" style="display:none">
    <img src="<?= Yii::app()->theme->baseUrl.'/images/ajax-loader10.gif' ?>" />
</div>
<script type="text/javascript">
    //var DD;
    (function ($) {

        <?php
      $jsObject = array();
        foreach ($activeLanguagesList as $key => $lang) {
            $jsObject[$key] = array(
                'lang_name' => $lang,
                'translations' => array(),
                'idSon' => array(),
            );

$translation = array();
            $translation[$model->id_field];

        }
        echo 'var translations = '.CJavaScript::encode($jsObject).';';
        echo "\n";

     ?>

        DD = new dropdownFieldManager({
            <?php if ( !empty($model->id_field)) {

            ?>
            idField: <? echo $model->id_field ?>,
            <?php } else { ?>
            idField: '',
            <?php } ?>
            currentActiveLanguage: <?= CJavaScript::encode($currentLanguage) ?>,
            translations: translations,
            filledLanguageTranslation: <?= CJavaScript::encode(Yii::t('standard', 'Filled languages')) ?>,
            newOptionPlaceholder: <?= CJSON::encode(Yii::t('field', 'Type here to add an option...')) ?>
        });
        /*
         On modal hide, remove the modal content in case there are js events or something else
         that may interrupt the work of the other parts of the code
         */
        $('#field-modal').on('hidden', function () {
            $('#field-modal').remove();
            /*
             When closing the dialog destroy the DD object
             because the object is needed only when this view is shown.
             */
            DD = null;
        });
    })(jQuery);
</script>