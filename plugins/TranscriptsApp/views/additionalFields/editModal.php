<div class="modal hide fade new-field" id="field-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo Yii::t('menu', '_FIELD_MANAGER');?> - <?php echo $modelField->getCategoryName(); ?></h3>
    </div>
    <div class="modal-body">
        <?php if ($modelField->type == 'dropdown'): ?>
            <?php echo $this->renderPartial('_formDropdownV2', array(
                'model' => $modelField,
                'activeLanguagesList'   => $activeLanguagesList,
                'translationsList'      => $translationsList,
                'modelTranslation'      => $modelTranslation
            )); ?>
        <?php else: ?>
            <?php echo $this->renderPartial('_form', array(
                'modelField' => $modelField,
                'activeLanguagesList'   => $activeLanguagesList,
                'translationsList'      => $translationsList,
                'modelTranslation'      => $modelTranslation
            )); ?>
        <?php endif; ?>
    </div>
    <div class="modal-footer buttons">
        <a href="#" class="btn confirm-btn"><?php echo Yii::t('standard', '_SAVE');?></a>
        <a href="#" class="btn cancel" data-dismiss="modal"><?php echo Yii::t('standard', '_UNDO');?></a>
    </div>
    <script type="text/javascript">
        $('select').styler();
    </script>
</div>