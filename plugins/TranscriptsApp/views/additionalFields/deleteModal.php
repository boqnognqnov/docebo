<h1>
    <?= Yii::t('custom_fields', 'Delete Additional Field') ?>
</h1>

<div class="form">
    <?php echo CHtml::beginForm('', 'POST', array(
        'class'	=> 'ajax',
        'id'	=> 'delete-custom-field'
    )); ?>
    <div class="row-fluid">
        <p class="field-name additional-field-name">
            <?php echo Yii::t('standard', '_FIELD_NAME') . ': "' .$model->getTranslation() . '"'; ?>
        </p>
    </div>

    <div class="delete-message">
        <div class="row-fluid">
            <div class="span1">
                <?php
                echo CHtml::checkBox('confirm', false, array(
                    'class' => 'confirm_checkbox',
                    'value'	=> 'on'
                ));
                ?>
            </div>
            <div class="span11">
                <label id="confirm_label" for="confirm"><?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?></label>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <input class="btn-docebo hidden green big yes" type="submit" name="confirm_yes"
               value="<?php echo Yii::t('standard', '_YES'); ?>" /> <input
            class="btn-docebo red big close-dialog" type="reset"
            value="<?php echo Yii::t('standard', '_NO'); ?>" />
    </div>
    <?php CHtml::endForm(); ?>
</div>

<script type="text/javascript">
    $(function() {

        $(".confirm_checkbox").on('change', function() {
            if ( $(".confirm_checkbox").hasClass("checked") ) {
                $(".btn-docebo.green.yes").removeClass("hidden");
            } else {
                $(".btn-docebo.green.yes").addClass("hidden");
            }
        });

        $(".confirm_checkbox").styler();
        $("#confirm_label").styler();
    });
</script>
<style type="text/css">
    #delete_field_form {
        margin-top: 30px;
    }
    .warning-text {
        float: none !important;
        display: block !important;
    }
    .warning-strip {
        min-height: 70px;
    }
    .modal.delete-field {
        width: 415px !important;
        margin-left: -211px;
    }
    .delete-message {
        margin-bottom: -30px;
    }
    .field-name {

    }
</style>