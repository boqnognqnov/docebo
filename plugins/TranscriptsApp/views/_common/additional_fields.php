<?php
/* @var $model TranscriptsRecord */ ?>
<div id="additional_fields" class="settings-tab hidden-tab player-settings-form">
    <?php
    $transcFields = $this->getAdditionalFieldsToDisplay($model);
    foreach($transcFields as $index => $transcField):?>
        <div class="control-group">
            <?= CHtml::label($transcField->translation, '', array('class' => 'control-label')) ?>
            <div class="controls">
                <div class="row-fluid">
                    <?= $transcField->render(); ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>