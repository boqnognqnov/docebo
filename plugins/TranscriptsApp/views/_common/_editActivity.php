<style>
	<!--
	.modal-edit-activity label{
		word-break: normal !important;
	}

	-->
</style>
<?
/* @var $model TranscriptsRecord */
?>
<h1>
	<?= $model->isNewRecord ? Yii::t('helper', 'New external activity - transcripts') : Yii::t('transcripts', 'Edit external activity') ?>
</h1>

<div class="form transcripts-edit-activity">

	<?php
	$isConfirmButtonVisible = true;
	
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-activity-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal',
			'enctype' => 'multipart/form-data'
		)
	));
	
/* @var $form CActiveForm */

	$errorClass = ($userError)? 'error' : '';
	switch ($formType) {

		case 'admin': {
			echo '<div class="control-group">';
			if ($model->isNewRecord) {
				echo CHtml::label(Yii::t('transcripts', 'Assign to').' <span class="required">*</span>', 'username-input', array('class' => 'control-label required '.$errorClass));
				echo '<div class="controls"><div class="row-fluid">';
				if($userError) {
					echo '<div class="errorMessage">'.$userError.'</div>';
				}
				echo '<input data-url="'.Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/assignUserAutocomplete').'"
					id="username-input" name="userid" autocomplete="off" type="text" class="typeahead input-block-level long-input '.$errorClass.'"
					data-source-desc="true" data-type="core" placeholder="'.Yii::t('transcripts', 'Type here the username').'"/>';
				//echo $form->textField($model, 'id_user', array('class'=>'input-block-level'));
				echo '<p class="help-block">';
				echo Yii::t('transcripts', 'Type here the user name that performed this external activity');
				echo '</p>';
				echo '</div></div>';
			} else {
				echo CHtml::label(Yii::t('transcripts', 'Assign to'), false, array('class' => 'control-label '.$errorClass));
				echo '<div class="controls"><div class="row-fluid">';
				echo '<span class="edit-activity-userid">'.$model->getUserid().'</span>';
				$_fullName = $model->user->getFullName();
				if (!empty($_fullName)) {
					echo '&nbsp;<span class="edit-activity-username">('.$_fullName.')</span>';
				}
				echo '</div></div>';
				echo $form->hiddenField($model, 'id_user', array('id' => 'input-id-user')); //is this redoundant?
				echo CHtml::hiddenField('id_record', $model->getPrimaryKey(), array('id' => 'input-id-record'));
			}
			echo '</div>';
		} break;

		case 'user': {
			echo CHtml::hiddenField('id_user', $idUser, array('id' => 'input-id-user'));
			if (!$model->isNewRecord) {
				echo CHtml::hiddenField('id_record', $model->getPrimaryKey(), array('id' => 'input-id-record'));
			}
		} break;

	}
	?>
	<?php
	if($isRequireToChooseInstitute === true) {
		if (!empty($listOfInstitutes) && !empty($coursesPerInstitutes)){
		?>
		<div class="control-group">
			<?= $form->labelEx($model, 'training_institute', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row-fluid">
					<?= $form->error($model, 'training_institute') ?>
					<?= $form->dropDownList($model, 'training_institute', $listOfInstitutes, array('options' => array($selectedInstituteId => array('selected' => true)), 'prompt' => Yii::t('standard', 'Select...'))) ?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<?= $form->labelEx($model, 'course_name', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row-fluid">
					<?= $form->error($model, 'course_name') ?>
					<?php
					if(!empty($coursesPerInstitutes[key($listOfInstitutes)])){ ?>
					<?= $form->dropDownList($model, 'course_name', $coursesPerInstitutes[key($listOfInstitutes)], array('prompt' => Yii::t('standard', 'Select...'))); ?>
					<?php }?>
				</div>
			</div>
		</div>
	<?php }else { $isConfirmButtonVisible = false; ?>
			<div class="control-group">
				<div class="controls">
					<div class="row-fluid">
						<div id="warning-strip" class="warning-strip warning warning-strip" style="height: 53px;">
							<div class="exclamation-mark"><i class="fa fa-exclamation-circle"></i></div>
							<div class="warning-text" style="padding-top: 1px">
								<?php
								if (Yii::app()->user->getIsGodadmin()) { ?>
									<?= Yii::t('transcripts', 'There are no training institutes or courses present.').'<br/>' ?>
									<?= CHtml::link(Yii::t('transcripts', 'Configure here courses and training institutes.'),
											Docebo::createAppUrl('TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes'),
											array('style'=>'text-decoration:underline')) ?>
								<?php } else { ?>
									<?= Yii::t('transcripts', 'There are no training institutes or courses present.') . '<br/>' . Yii::t('course', 'Please contact your administrator') ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php }
	} else { ?>
		<div class="control-group">
			<?= $form->labelEx($model, 'training_institute', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row-fluid">
					<?= $form->error($model, 'training_institute') ?>
					<?= $form->textField($model, 'training_institute', array('class' => 'span8 input-block-level long-input')) ?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<?= $form->labelEx($model, 'course_name', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row-fluid">
					<?= $form->error($model, 'course_name') ?>
					<?= $form->textField($model, 'course_name', array('class' => 'span8 input-block-level long-input')) ?>
				</div>
			</div>
		</div>

		<div class="control-group">
			<?= $form->labelEx($model, 'course_type', array('class' => 'control-label')) ?>
			<div class="controls">
				<div class="row-fluid">
					<?= CHtml::label(
							$form->radioButton($model, 'course_type', array(
									'uncheckValue' => null,
									'value' => LearningCourse::TYPE_ELEARNING,
									'checked'=>($model->course_type == LearningCourse::TYPE_ELEARNING),
									'id'=> 'course-type-elearning'
							)) . ' '
							. LearningCourse::model()->getCourseTypeTranslation(LearningCourse::TYPE_ELEARNING),
							'course-type-elearning',
							array('class'=>'radio-list-element')
					) ?>
					<?= CHtml::label(
							$form->radioButton($model, 'course_type', array(
									'uncheckValue' => null,
									'value' => LearningCourse::TYPE_CLASSROOM,
									'checked'=>($model->course_type == LearningCourse::TYPE_CLASSROOM),
									'id'=> 'course-type-classroom'
							)) . ' '
							. LearningCourse::model()->getCourseTypeTranslation(LearningCourse::TYPE_CLASSROOM),
							'course-type-classroom',
							array('class'=>'radio-list-element')
					) ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="control-group">
		<?php
		echo CHtml::label(Yii::t('standard', '_DATE').' <span class="required">*</span>', false, array('class' => 'control-label required'));
		?>
		<div class="controls">
			<div class="validity-period-error">
				<?php if ($form->error($model, 'from_date') || $form->error($model, 'to_date')) :?>
					<div class="row-fluid">
						<div class="valid-from-error" <?=$model->course_type == LearningCourse::TYPE_ELEARNING ? 'style="display:none"':''?> >
							<?= $form->error($model, 'from_date') ?>
						</div>
						<div class="valid-to-error"><?= $form->error($model, 'to_date') ?></div>
					</div>
				<?endif?>
			</div>
			<div class="validity-period">
				<div class="valid-from only-classroom-ib" style="vertical-align:top<?=$model->course_type == LearningCourse::TYPE_ELEARNING ? ';display:none':''?>">
					<?= CHtml::label(Yii::t('standard', '_FROM'), 'from-date-input') ?>
					<?= $form->textField($model, 'from_date', array('class'=>'datepicker edit-day-input', 'id' => 'from-date-input')) ?>
					<i class="p-sprite calendar-black-small date-icon"></i>
				</div>
				<div class="valid-to">
					<?= $form->error($model, 'to_date') ?>
					<?= CHtml::label(Yii::t('standard', '_TO'), 'to-date-input', array('class' => 'only-classroom-i', 'style'=>$model->course_type == LearningCourse::TYPE_ELEARNING ? 'display:none':'')) ?>
					<?= $form->textField($model, 'to_date', array('class'=>'datepicker edit-day-input', 'id' => 'to-date-input')) ?>
					<i class="p-sprite calendar-black-small date-icon"></i>
					<p class="help-block only-elearning-b" style="<?=$model->course_type != LearningCourse::TYPE_ELEARNING ? 'display:none':''?>">
						<?= Yii::t('transcripts', 'The date refers to the completion of the course'); ?>
					</p>
				</div>
			</div>

		</div>
	</div>

	<div class="control-group">
		<?= $form->labelEx($model, 'score', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= $form->error($model, 'score') ?>
				<?= $form->numberField($model, 'score', array(
                        'class' => 'span8 input-block-level small-input',
                        'max'   => '100',
                        'min'   => '0'
                    )) ?>
				&nbsp;/&nbsp;<?= $model->getMaxScore() ?>
			</div>
		</div>
	</div>
	
	<div class="control-group">
		<?= $form->labelEx($model, 'credits', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= $form->error($model, 'credits') ?>
				<?= $form->textField($model, 'credits', array('class' => 'span8 input-block-level small-input')) ?>
			</div>
		</div>
	</div>



	<!--<div class="control-group">
		<?php
		echo CHtml::label(Yii::t('player', 'Certificate'), 'certificate', array('class' => 'control-label'));
		echo '<div class="controls"><div class="row-fluid">';
		echo CHtml::fileField('certificate', '', array('id' => 'certificate-input'));
		echo '<p class="help-block">';
		echo Yii::t('transcripts', 'It must be an image or a PDF document');
		echo '</p>';
		echo '</div></div>';
		?>
	</div>-->

	<?php
	echo CHtml::label(Yii::t('player', 'Certificate'), 'certificate', array('class' => 'control-label'));
	echo '<div class="controls"><div class="row-fluid">';
	if (!empty($model->certificate)) {
		echo '<div id="certificate-file-dl-link" style="display:block;">';
		echo '<span id="remove-uploaded-file" class="i-sprite is-remove red"></span>';
		echo '<a href="'.$model->getCertificateDownloadUrl().'" class="evaluation-file-download">'.$model->getOriginalCertificateFileName().'</a>';
		echo CHtml::hiddenField('certificate-file-removed', 0, array('id' => 'input-certificate-file-removed'));
		echo '</div>';
		echo '<div id="certificate-file-new-upload" style="display:none;">';
		echo CHtml::fileField('certificate', '', array('id' => 'certificate-input'));
		echo '<p class="help-block">';
		echo Yii::t('transcripts', 'It must be an image or a PDF document');
		echo '</p>';
		echo '</div>';
	} else {
		//echo CHtml::hiddenField('certificate-file-removed', 1, array('id' => 'input-certificate-file-removed')); //simulate a new file upload
		echo CHtml::fileField('certificate', '', array('id' => 'certificate-input'));
		echo '<p class="help-block">';
		echo Yii::t('transcripts', 'It must be an image or a PDF document');
		echo '</p>';
	}
	echo '</div></div>';
	?>
	<br/>

	<? if(PluginManager::isPluginActive('CertificationApp')): ?>
	<div class="control-group">
		<?php echo CHtml::label(Yii::t('certification', 'Certifications & Retraining'), 'certification', array('class'=>'control-label')); ?>
		<div class="controls">
			<div class="row-fluid">
				<?php echo CHtml::dropDownList('certification', ($model->certification ? $model->certification->id_cert : null), array(Yii::t('standard', '_NONE')) + CHtml::listData(Certification::model()->findAll('deleted = 0'), 'id_cert', 'title')); ?>
				<p class="help-block"><?=Yii::t('certification', 'Select a certification to associate with this external training')?></p>
			</div>
		</div>
	</div>
	<? endif; ?>

	<?php (TranscriptsField::hasAnyField()) ? $this->renderPartial('TranscriptsCommonViews.additional_fields', array('model' =>  $model)): null ?>

	<div class="form-actions">
		<?php
		if ($isConfirmButtonVisible === true) { ?>
			<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>"/>
		<?php } ?>
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">

$(function() {
	var isRequireToChooseInstitute = '<?=$isRequireToChooseInstitute?>';
	if(isRequireToChooseInstitute == 1) {
//		var listOfInstitutes = JSON.parse('<?//=json_encode($listOfInstitutes)?>//');
		var coursesPerInstitutes = JSON.parse('<?=addslashes(json_encode($coursesPerInstitutes))?>');
		var listCoursesTypes = JSON.parse('<?=addslashes(json_encode($listCoursesTypes))?>');
	}

	var container = $('.modal-edit-activity');
	container.controls();
	container.find('input').styler({browseText: "<?= Yii::t('organization', 'Upload File') ?>"});
	<?php if ($formType == 'admin'): ?>
	applyTypeahead($('#username-input'));
	<?php endif; ?>

	// Install date picker
	var fromPicker = $('#from-date-input').bdatepicker({format: '<?= $this->getLocalDateFormat() ?>', language: '<?= Yii::app()->getLanguage() ?>'}).data('datepicker');
	var toPicker = $('#to-date-input').bdatepicker({format: '<?= $this->getLocalDateFormat() ?>', language: '<?= Yii::app()->getLanguage() ?>'}).data('datepicker');
	$('#from-date-input').bdatepicker().on('show', function(ev){ toPicker.hide(); });
	$('#to-date-input').bdatepicker().on('show', function(ev){ fromPicker.hide(); });

	// Install click on calendar icon
	$(".date-icon").on('click', function (ev) {
		if (!$(this).prev('input').attr('disabled'))
			$(this).prev('input').bdatepicker().data('datepicker').show();
	});

	var isInit = true;
	// Install course type radio button check
	function updateCourseType() {
		if ($('#course-type-elearning').is(":checked")) {
			$('.only-elearning-b').css('display', 'block');
			$('.only-classroom-i').css('display', 'none');
			$('.only-classroom-ib').css('display', 'none');
			$('.valid-from-error').css('display', 'none');
			if (!isInit) {
				$('.validity-period-error .row-fluid').remove();
			}
		}
		if ($('#course-type-classroom').is(":checked")) {
			$('.only-elearning-b').css('display', 'none');
			$('.only-classroom-i').css('display', 'inline');
			$('.only-classroom-ib').css('display', 'inline-block');
			$('.valid-from-error').css('display', 'inline-block');
			if (!isInit) {
				$('.validity-period-error .row-fluid').remove();
			}
		}
		isInit = false;
	}
	$('#course-type-elearning').on('change', updateCourseType);
	$('#course-type-classroom').on('change', updateCourseType);
	updateCourseType();

	//certificate file uploader management
	$('#remove-uploaded-file').bind('click', function(e) {
		$('#certificate-file-dl-link').css('display', 'none');
		$('#certificate-file-new-upload').css('display', 'block');
		$('#input-certificate-file-removed').val('1');
	});

	//refresh gridview after operation
	$(document).undelegate(".modal-edit-activity", "dialog2.content-update"); //this is to avoid repeated event assignments when opening the dialog multiple times
	$(document).delegate(".modal-edit-activity", "dialog2.content-update", function() {
		var e = $(this);
		var autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");
			// reload transcripts grid
			//$("#transcripts-management-grid").yiiGridView('update');
			$(".transcripts-management-grid").each(function(){
				$(this).yiiGridView('update');
			});
			$("#transcripts-waiting-approval-grid").yiiGridView('update');

			var href = autoclose.attr('href');
			if (href) { window.location.href = href; }
			//check for other info
			var data = autoclose.data(), actionButton = $('#waiting-for-approval-action-button');
			$("#dashlet-waiting-for-approval").html(data.countWaiting);
			if (actionButton.length > 0 && data.countWaiting !== undefined) {
				$('#count-waiting-for-approval').html(data.countWaiting);
				if (data.countWaiting > 0) {
					actionButton.css('display', 'list-item');
				} else {
					actionButton.css('display', 'none');
				}
			}
		}
	});
	if(isRequireToChooseInstitute == 1) {
		$(document).off('change', '#TranscriptsRecord_training_institute').on('change', '#TranscriptsRecord_training_institute', function () {
			var idInstitute = $(this).val();
			var secondDropdown = $('#TranscriptsRecord_course_name');
			secondDropdown.empty();
			secondDropdown.append('<option value>' + Yii.t('standard', 'Select...') + '</option>');
			var newOptions = coursesPerInstitutes[idInstitute];
			if(newOptions!=undefined) {
				$.each(newOptions, function (index, el) {
					var option = $('<option>');
					option.val(index);
					option.text(el);
					option.appendTo(secondDropdown);
				})
				$('#TranscriptsRecord_course_name').trigger('change');
			}
		});

		$(document).off('change', '#TranscriptsRecord_course_name').on('change', '#TranscriptsRecord_course_name', function () {
			var courseId = $(this).val();
			if (listCoursesTypes[courseId] == 'elearning') {
				$('div.valid-from.only-classroom-ib').hide();
				$('p.help-block.only-elearning-b').show();
				$('label.only-classroom-i').hide();
				$('.valid-from-error').hide();
			} else if (listCoursesTypes[courseId] == 'classroom') {
				$('div.valid-from.only-classroom-ib').show();
				$('p.help-block.only-elearning-b').hide();
				$('label.only-classroom-i').show();
				$('.valid-from-error').show();
			}
		});

		$('#TranscriptsRecord_training_institute').trigger('change');
		$('#TranscriptsRecord_course_name').trigger('change');
		var selectedCourseId = '<?=$selectedCourseId?>';
		if(selectedCourseId){
			$('#TranscriptsRecord_course_name option[value="' + selectedCourseId + '"]').prop('selected', true);
			$('#TranscriptsRecord_course_name').trigger('change');
		}
	}
});
</script>

<style>
	.valid-to .errorMessage {
		margin-left: 23px;
	}
</style>
