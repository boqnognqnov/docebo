<?php
/* @var $this LocationController */
/* @var $model LtLocation */
/* @var $form CActiveForm */
?>
<h1>
	<i class="i-sprite is-remove white large"></i>
	<?= Yii::t('standard', '_AREYOUSURE') ?>
</h1>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'delete-activity-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'ajax'
	),
));

echo CHtml::hiddenField('id_record', $model->getPrimaryKey(), array('id' => 'input-id-record'));
echo CHtml::hiddenField('confirm', 1, array('id' => 'input-confirm'));

if ($formType == 'admin') {
	echo CHtml::hiddenField('rem-action', 'remove', array('id' => 'input-rem-action'));
}

?>
<?php
	$courseName = ($model->course_id) ? $model->course->course_name : $model->course_name ;
?>
<?= Yii::t('player', "Are you sure you want to delete:") . " <strong>".  $courseName ."</strong> ?" ?>


<div class="form-actions">
	<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
	<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	$(document).undelegate(".modal-delete-activity", "dialog2.content-update"); //this is to avoid repeated event assignments when opening the dialog multiple times
	$(document).delegate(".modal-delete-activity", "dialog2.content-update", function() {
		var e = $(this);
		var autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");
			// reload transcripts grid
			//$("#transcripts-management-grid").yiiGridView('update');
			$(".transcripts-management-grid").each(function(){
				$(this).yiiGridView('update');
			})
			$("#transcripts-waiting-approval-grid").yiiGridView('update');
			//check for other info
			var data = autoclose.data(), actionButton = $('#waiting-for-approval-action-button');
			$("#dashlet-waiting-for-approval").html(data.countWaiting);
			if (actionButton.length > 0 && data.countWaiting !== undefined) {
				$('#count-waiting-for-approval').html(data.countWaiting);
				if (data.countWaiting > 0) {
					actionButton.css('display', 'list-item');
				} else {
					actionButton.css('display', 'none');
				}
			}
			//check for redirect
			var href = autoclose.attr('href');
			if (href) { window.location.href = href; }
		}
	});

</script>

