<?php if(Yii::app()->user->checkAccess(Yii::app()->user->level_godadmin)) : ?>
<div class="activities-main-actions-container main-actions clearfix">

	<ul class="clearfix">

		<li>
            <div>
				<?php echo CHtml::link('<span></span>'.Yii::t('transcripts','Create New Course'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/editCourse'),
					array(
						'id' => 'edit-course',
						'alt' => Yii::t('helper','Create New Course'),
						'class' => 'new-activity open-dialog newCourse',
						'data-dialog-class' => "modal-edit-course",
						'data-helper-title' => Yii::t('helper', 'Create New Course'),
						'data-helper-text' => Yii::t('helper', 'Create New Course')
					)); ?>
			</div>
		</li>
		<li>
			<div>
				<?php echo CHtml::link('<i class="fa fa-university fa-3x" style="margin-bottom: 11.5px; display: block;"></i>'.Yii::t('transcripts','Manage Institutes'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/manageInstitutes'),
					array(
                        'id' => 'manage-institues-button',
						'alt' => Yii::t('helper','Manage Institutes'),
						'class' => 'institutes',
					)); ?>
			</div>
		</li>

	</ul>

</div>
<style>

    a.newCourse > span {
        height: 35px !important;
        /*width: 60px !important;*/
        background-position: -876px -97px;
    }

    a.newCourse:hover > span {
        background-position: -944px -98px;
    }

</style>
<?php endif; ?>