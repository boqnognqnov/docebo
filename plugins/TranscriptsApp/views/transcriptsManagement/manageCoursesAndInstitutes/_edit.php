<?php $request = Yii::app()->request; ?>
<h1>
	<?= $request->getParam('id', false) ? Yii::t('transcripts', 'Edit Course') : Yii::t('transcripts', 'Create New Course') ?>
</h1>

<div class="form transcripts-edit-course">

	<?php

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'new-course-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal'
		)
	));

	?>

	<div class="control-group">
		<?= $form->labelEx($model, 'course_name', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= $form->error($model, 'course_name') ?>
				<?= $form->textField($model, 'course_name', array('class' => 'span8 input-block-level long-input')) ?>
			</div>
		</div>
	</div>

	<div class="control-group">
        <br />
		<?= $form->labelEx($model, 'course_type', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= CHtml::label(
					$form->radioButton($model, 'course_type', array(
						'uncheckValue' => null,
						'value' => LearningCourse::TYPE_ELEARNING,
						'checked'=>($model->type == LearningCourse::TYPE_ELEARNING || $model->type != LearningCourse::TYPE_CLASSROOM),
						'id'=> 'course-type-elearning'
					)) . ' '
					. LearningCourse::model()->getCourseTypeTranslation(LearningCourse::TYPE_ELEARNING),
					'course-type-elearning',
					array('class'=>'radio-list-element span6')
				) ?>
				<?= CHtml::label(
					$form->radioButton($model, 'course_type', array(
						'uncheckValue' => null,
						'value' => LearningCourse::TYPE_CLASSROOM,
						'checked'=>($model->type == LearningCourse::TYPE_CLASSROOM),
						'id'=> 'course-type-classroom'
					)) . ' '
					. LearningCourse::model()->getCourseTypeTranslation(LearningCourse::TYPE_CLASSROOM),
					'course-type-classroom',
					array('class'=>'radio-list-element span6')
				) ?>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

    <script>
        $('input[type=radio]').styler();
    </script>

	<?php $this->endWidget(); ?>
</div>