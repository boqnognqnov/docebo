<?php
/* @var $model TranscriptsRecord */

$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('transcripts', 'External activities') => Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList'),
	Yii::t('transcripts', 'Courses and Institutes'),
);
end($this->breadcrumbs);
?>

<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?= CHtml::link(Yii::t('standard', '_BACK'), prev($this->breadcrumbs)); ?>
		<?= end($this->breadcrumbs) ?>
	</h3>
</div>

<br />

<?php $this->renderPartial('manageCoursesAndInstitutes/_mainAdminActions', array('showWaiting' => true)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'transcripts-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-grid-form',
		'data-grid' => '#transcripts-management-grid'
	),
)); ?>

<div class="main-section">

	<div class="filters-wrapper">
		<table class="filters filters_transcripts_management">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>

							<td class="group filter-by-text clearfix" align="right">
								<table>
									<tr>
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/filterCoursesByTextAutocomplete') ?>"
													class="typeahead"
													id="advanced-search-transcripts-by-text"
													autocomplete="off"
													type="text"
													name="filter-by-text"
													placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<button id ="transcripts-grid-text-search-clear-button" type="button" class="close grid-search-clear" style="display: none;">&times;</button>
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>

						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>


<div class="bottom-section clearfix">

	<div id="grid-wrapper" class="TranscriptsManagementPage">
		<?php $_columns = array();
		$_columns[] = array(
			'type' => 'raw',
            'header' => Yii::t('transcripts', 'Course Name'),
			'value' => '$data->course_name',
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('transcripts', 'Type'),
			'value' => '$data->renderType();',
            'htmlOptions' => array(
                'style' => 'min-width:5em!important;'
            )
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('transcripts', 'Training institute'),
            'headerHtmlOptions' => array(
                'class' => 'text-center'
            ),
			'value' => '$data->renderInstitute()',
            'htmlOptions' => array(
                'class' => 'text-center',
                'style' => 'min-width:12em!important;'
            )
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => '', // Empty header for row actions
			'cssClassExpression' => '"text-right"',
			'value' => '$data->renderActions()',
            'htmlOptions' => array(
                'style' => 'min-width:40px!important;'
            )
		);

		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'transcripts-course-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProvider(),
			'columns' => $_columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				// Reset placeholder text if default one
				resetSearchPlaceholder();
				options.type = "POST";
				options.data = $("#transcripts-management-form").serialize();
			}',
			'afterAjaxUpdate' => 'function(id, data) {
				$(\'a[rel="tooltip"]\').tooltip();
				$(\'span[rel="tooltip"]\').tooltip();
				$(document).controls();
                $(\'select\').styler();
                $(\'.institutesLinkSelect\').hide();
			}',
		)); ?>
	</div>
</div>
<style>
    td.filter-by-text > table {
        float:right;
    }

    button#transcripts-grid-text-search-clear-button {
        top: 8px;
        right: 35px !important;
        position: absolute;
    }

    .search-icon {
        right: 8px !important;
        top: 8px !important;
    }
</style>

<script type="text/javascript">

	function resetSearchPlaceholder() {
		var $searchText = $('#advanced-search-transcripts-by-text');
		var $searchClear = $('#transcripts-grid-text-search-clear-button');
		if ($searchText.val() == "<?= Yii::t('standard', '_SEARCH'); ?>") { $searchText.val(''); }
	}

	$(function () {

		var $searchText = $('#advanced-search-transcripts-by-text');
        var $searchClear = $('#transcripts-grid-text-search-clear-button');
		var previousFilterText = $.trim($searchText.val());

		function filterTranscriptsGrid() {
            previousFilterText = $.trim($searchText.val());
            $("#transcripts-course-management-grid").yiiGridView('update');
		}

		$searchText.bind('keydown', function (e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				// enter was pressed, then update grid
				filterTranscriptsGrid();
				e.preventDefault();
				return false;
			}
		});

        $searchText.bind('keyup', function(e){
            if($searchText.val() !== '') {
                $searchClear.show();
            } else {
                $searchClear.hide();
            }
        })

		//text search filter reset button action
		$('#transcripts-grid-text-search-clear-button').on('click', function() {
			$searchText.val("");
            filterTranscriptsGrid();
            $searchClear.hide();
		});

		//others
		$('input, select').styler();
		resetSearchPlaceholder();


        $('.institutesLinkSelect').hide();
        $('.institutesLink').live('click', function () {
            $(this).parent().parent().removeClass('selected');
            $('.institutesLinkSelect').hide();
            $('.institutesLink').show().parent().parent().removeClass('withSelect selected');
            $(this).hide();
            $(this).parent().children('.institutesLinkSelect').show().parent().parent().addClass('withSelect selected');
          //$(this).next().next().show();
        });

        $('.institutesLinkSelect').live('change', function () {
          var course_id = $(this).data('course-id');
          var institute_id = $(this).val();
          $.post(HTTP_HOST + '?r=TranscriptsApp/TranscriptsManagement/assignInstitute', {'course_id': course_id, 'institute_id': institute_id}, function (data) {
            if (data.status == 'success') {
              $('#transcripts-course-management-grid').yiiGridView('update');
            }
          }, 'json');
        });
	});

</script>
