<?php $request = Yii::app()->request; ?>
<h1>
    <?= $request->getParam('id', false) ? Yii::t('transcripts', 'Edit Institute') : Yii::t('transcripts', 'Create New Institute') ?>
</h1>

<div class="form transcripts-edit-institute">

	<?php

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'new-institute-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal',
		)
	));
    
	?>

	<div class="control-group">
		<?= $form->labelEx($model, 'institute-name', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= $form->error($model, 'institute_name') ?>
				<?= $form->textField($model, 'institute_name', array('class' => 'span12 input-block-level long-input')) ?>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big institute_confirm_button" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

    <style>
        div.form.transcripts-edit-institute td.name-label > h5 {
            font-weight: 600;
        }
    </style>

	<?php $this->endWidget(); ?>
</div>