<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 8.2.2016 г.
 * Time: 14:54
 *
 * @var $rewardSetModel GamificationRewardsSet
 * @var $this RewardsSetController
 */


echo CHtml::form('', 'post', array(
    'class' => 'ajax form-horizontal',
    'id' => 'form-delete-reward-set'
));
?>
<?php DoceboUI::printFlashMessages();?>
<?php echo Yii::t('gamification', 'Are you sure you want to delete <strong>"{name}"</strong>', array(
    '{name}' => $model->institute_name
)); ?>
<br>
<br>

<div class="row-fluid">
    <div style="display: inline-block">
        <?= CHtml::checkBox('deleteConfirmed', false)?>
        <?= CHtml::label(Yii::t('app7020', 'Yes, I want to proceed!'), 'deleteConfirmed', array(
            'style' => 'display: inline; margin-left: 5px'
        )) ?>
    </div>
</div>

<?=CHtml::hiddenField('id', $rewardSetModel->id) ?>


<div class="form-actions">
    <input class="btn btn-docebo green big confirm-button hidden" type="submit" name="save" id="handle-room-button" value="<?= Yii::t('standard', '_CONFIRM') ?>">
    <input class="btn btn-docebo black big close-dialog" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>



<?php echo CHtml::endForm(); ?>


<script type="text/javascript">
    $( document ).ready(function() {

        $('form#form-delete-reward-set input').styler();

        $('input[name=deleteConfirmed]').on('change', function(){
            if($(this).is(':checked')){
                $('a.confirm-button').removeClass('hidden');
            } else{
                $('a.confirm-button').addClass('hidden');
            }
        })
    });
</script>