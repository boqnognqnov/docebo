<?php if(Yii::app()->user->checkAccess(Yii::app()->user->level_godadmin)) : ?>
<div class="activities-main-actions-container main-actions clearfix">

	<ul class="clearfix">

		<li>
            <div>
				<?php echo CHtml::link('<i class="fa fa-plus-circle fa-3x" style="margin-bottom: 11.5px; padding:0 22px; display: block;"></i>'.Yii::t('transcripts','New Institute'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/editInstitute'),
					array(
						'id' => 'create-new-institute',
						'alt' => Yii::t('helper','New Institute'),
						'class' => 'new-activity open-dialog newInstitute',
						'data-dialog-class' => "modal-edit-activity",
						'data-helper-title' => Yii::t('helper', 'New Institute'),
						'data-helper-text' => Yii::t('helper', 'New Institute')
					)); ?>
			</div>
		</li>

	</ul>

</div>
<style>
    a.newInstitute {
        padding-top: 13px;
        height: 62px;
        padding-right: 10px !important;
        padding-left: 10px !important;
    }

    a.newInstitute > span {
        height: 35px !important;
        /*width: 60px !important;*/
        background-position: 3px -35px;
    }

    a.newInstitute:hover > span {
        background-position: -59px -35px;
    }

</style>
<?php endif; ?>