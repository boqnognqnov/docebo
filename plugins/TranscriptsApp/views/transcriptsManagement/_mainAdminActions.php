<?php if(Yii::app()->user->checkAccess('/framework/admin/transcripts/mod')) : ?>
<div class="activities-main-actions-container main-actions clearfix">

	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('helper', 'New external activity - transcripts'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/createActivity'),
					array(
						'id' => 'create-new-activity',
						'alt' => Yii::t('helper', 'New external activity - transcripts'),
						'class' => 'new-activity open-dialog',
						'data-dialog-class' => "modal-edit-activity",
						'data-helper-title' => Yii::t('helper', 'New external activity - transcripts'),
						'data-helper-text' => Yii::t('helper', 'New external activity - transcripts')
					)); ?>
			</div>
		</li>

		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('transcripts','Import from CSV'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/importFromCsv'),
					array(
						'alt' => Yii::t('helper','Import from CSV - transcripts'),
						'class' => 'import'
					)); ?>
			</div>
		</li>

		<?php if ($showWaiting && Settings::get('transcripts_admin_approval', 'off') == 'on'): ?>
		<?php
		$_countWaitingForApproval = TranscriptsRecord::model()->countWaitingForApproval();
		//if ($_countWaitingForApproval > 0):
			$_notification = '<span class="notification" id="count-waiting-for-approval">'.$_countWaitingForApproval.'</span>';
		?>
		<li id="waiting-for-approval-action-button">
			<div>
				<?php echo $_notification.CHtml::link('<span></span>'.Yii::t('helper','Waiting for approval - transcripts'),
					Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/waitingForApproval'),
					array(
						'alt' => Yii::t('helper','Waiting for approval - transcripts'),
						'class' => 'transcripts-waiting-approval'
					)); ?>
			</div>
		</li>
		<?php //endif; ?>
		<?php endif; ?>

		<?php if (Yii::app()->user->getIsGodadmin()): ?>
			<li>
				<div>
					<?php
						echo CHtml::link('<span></span>' . Yii::t('menu', '_FIELD_MANAGER'),
							$this->createUrl('additionalFields/index'),
							array('class' => 'additional-fields', 'alt' => Yii::t('helper', 'Extend your default training record by creating and assigning brand new custom fields')));
					?>
				</div>
			</li>
		<?php endif; ?>

        <?php if ((Settings::get('transcripts_require_defined_list') == 'on' && Yii::app()->user->checkAccess(Yii::app()->user->level_godadmin)) || Yii::app()->user->checkAccess(Yii::app()->user->level_godadmin)): ?>

            <li>
                <div>
                    <?php echo CHtml::link('<i class="fa fa-cog fa-3x" style="margin-bottom: 11.5px; padding:0 2px; display: block;"></i>'.Yii::t('transcripts','Manage courses and Institutes'),
                        Yii::app()->createUrl('TranscriptsApp/TranscriptsManagement/manageCoursesAndInstitutes'),
                        array(
                            'alt' => Yii::t('helper','Manage the courses and training institutes that issue them'),
                            'id' => 'manage-courses-and-institues-button',
                        )); ?>
                </div>
            </li>

        <?php endif; ?>

	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>

			<p></p>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		if ($('#count-waiting-for-approval').html() === '0') {
			mainActionsResponsify(); //hiding the action button may interfere with graphical adjustments: ensure doing it before setting display attribute
			$('#waiting-for-approval-action-button').css('display', 'none'); //totally hide the action button
		}
	});
</script>
<?php endif; ?>