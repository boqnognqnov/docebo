<?php
/* @var $this LocationController */
/* @var $model LtLocation */
?>
<h1><?= Yii::t('classroom', 'View location map') ?></h1>



<div class="location-address"><?= $model->renderAddress() ?></div>

<div class="map-preview-container">
    <span class="classroom-sprite map-placeholder"></span>
    <div class="location-map-canvas"></div>
</div>



<div class="form-actions">
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CLOSE'); ?>"/>
</div>


<script type="text/javascript">

    (function() {

        var geocoder;
        var map;

        function updateMapPreview() {
            var $mapCanvas = $('.modal .location-map-canvas');
            var address = $.trim( $('.modal .location-address').html() );

            if (address == '') {
                // clear preview
                $mapCanvas.html('').attr('style','');
                return;
            }

            if (!geocoder)
                geocoder = new google.maps.Geocoder();

            geocoder.geocode( {'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var mapOptions = {
                        zoom: 14
                    }
                    map = new google.maps.Map($mapCanvas[0], mapOptions);

                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    Docebo.log('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        updateMapPreview();

    })();
</script>

