<?php
/* @var $this TranscriptsManagementController */
/* @var $model TranscriptsImportForm */
/* @var $columns array */
/* @var $form CActiveForm */

$backUrl = Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList');

$this->breadcrumbs[Yii::t('transcripts', 'External activities')] = $backUrl;
$this->breadcrumbs[] = Yii::t('transcripts', 'Import from CSV');

?>

<h3 class="title-bold">
	<a class="docebo-back" href="<?= $backUrl ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= Yii::t('classroom', 'Import external activities from CSV') ?></h3>
<br>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'transcripts-import-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'docebo-form form-horizontal form-import',
		'enctype' => 'multipart/form-data',
	),
)); ?>

<!--<div class="control-container odd">
	<div class="control-group">
		<?= $form->labelEx($model, 'insertUpdate', array('class' => 'control-label')) ?>
		<div class="controls">
			<?= $form->checkBox($model, 'insertUpdate', array()) ?>
			<?= $form->labelEx($model, 'insertUpdate') ?>
		</div>
	</div>
</div>-->

<br>
<h3 class="title-bold"><?= Yii::t('organization_chart', '_IMPORT_MAP') ?></h3>

<?= $form->errorSummary($model) ?>

<?php $this->widget('DoceboCGridView', array(
	'id' => 'transcripts-import-grid',
	'htmlOptions' => array('class' => 'grid-view'),
	'dataProvider' => $model->dataProvider(),
	'columns' => $columns,
	'template' => '<div class="table-scroll">{items}</div>{summary}{pager}',
	'summaryText' => Yii::t('standard', '_TOTAL'),
	'pager' => array(
		'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
	),
	'beforeAjaxUpdate' => 'function(id, options) {
    options.type = "POST";
    if (options.data == undefined) { options.data = {}; }
    options.data.contentType = "html";
	}',
	'afterAjaxUpdate' => 'function() {
    //$("select").hide();
    $("select").styler();
    autoSelectWidth();
    $(".table-scroll").jScrollPane({autoReinitialise: true});
  }'
)); ?>

<br>
<div class="text-right">
	<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_NEXT'); ?>"/>
	<a href="<?= $backUrl ?>" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
</div>
<?php $this->endWidget(); ?>


<script type="text/javascript">
	//<!--

	function autoSelectWidth() {
		var selectWidth = $('select').outerWidth(),
			selectStylerContainer = $('.form-import .jqselect'),
			selectStyler = $('.form-import .jq-selectbox__select');
		selectStylerContainer.css('width', selectWidth);
		selectStyler.css('width', 'auto');
	}
	;

	(function ($) {
		$(function () {
			$('input').styler();
			autoSelectWidth();
			$('.table-scroll').jScrollPane({
				autoReinitialise: true
			});
		});
	})(jQuery);

	//-->
</script>