<div class="grid-view">
	<table class="items" style="width:100%;text-align:left;">
		<thead>
			<tr>
				<th><?=Yii::t('standard', '_ROW');?></th>
				<th><?=Yii::t('standard', '_FIELD_NAME');?></th>
				<th><?=Yii::t('standard', '_ERRORS');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($errors as $num => $details)
				{
					$field = (isset($translations[$details['field']]) ? $translations[$details['field']] : $details['field']);
			?>
				<tr class="odd">
					<td><?=$details['row'];?></td>
					<td><?=$field;?></td>
					<td><?=$details['error'];?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
