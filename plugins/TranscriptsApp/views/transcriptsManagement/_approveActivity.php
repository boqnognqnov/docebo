<h1>
	<?= Yii::t('transcripts', 'Approve activity') ?>
</h1>

<div class="form transcripts-approve-activity">

	<?php

	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'approve-activity-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal',
			'enctype' => 'multipart/form-data'
		)
	));

	/* @var $form CActiveForm */

	if ($isMultiple) {
		echo CHtml::hiddenField('selected-transcripts', '', array('id' => 'input-selected-transcripts'));
	} else {
		echo CHtml::hiddenField('id_record', $model->getPrimaryKey(), array('id' => 'input-id-record'));
	}
	echo CHtml::hiddenField('confirm', 1, array('id' => 'input-confirm'));

	?>

	<p class="approve-text">
		<?= Yii::t('player', "Are you sure you want to approve selected activity/ies") . ' ?' ?>
	</p>
	<?php if ($isMultiple): ?>
	<p>
		<?= Yii::t('transcripts', 'Selected external activities') ?>:&nbsp;<strong id="count-selected-activities"></strong>
	</p>
	<?php else: ?>
	<div>
		<table>
			<tbody>
				<tr>
					<td class=""approve-label"><?= Yii::t('standard', '_USER') ?>:</td><td class="approve-content"><?= Yii::app()->user->getRelativeUsername($model->user->userid) ?></td>
				</tr>
				<tr>
					<td class=""approve-label"><?= Yii::t('standard', '_COURSE_NAME') ?>:</td><td class="approve-content">
						<?= $courseName ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php endif; ?>

	<!--<div class="control-group">
		<?= CHtml::label(Yii::t('transcripts', 'Add message for the user').':<br />'
			.'<p class="label-help-block">('.Yii::t('transcripts', 'optional').')</p>', 'approve-message', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= CHtml::textArea('approve-message', '', array('id' => 'approve-message', 'class' => 'long-input')) ?>
			</div>
		</div>
	</div>-->

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	$(function() {
		var container = $('.modal-delete-activity');
		container.controls();
		container.find('input').styler();

		<?php if ($isMultiple): ?>
		var selectedItems = $('#transcripts-waiting-approval-grid-selected-items-list').val();
		$('#input-selected-transcripts').val(selectedItems);
		if (selectedItems.length) {
			var selectedTranscripts = selectedItems.split(',');
			var countTranscripts = selectedTranscripts.length;
			$('#count-selected-activities').html(countTranscripts);
		}
		<?php endif; ?>

		$(document).undelegate(".modal-approve-activity", "dialog2.content-update"); //this is to avoid repeated event assignments when opening the dialog multiple times
		$(document).delegate(".modal-approve-activity", "dialog2.content-update", function() {
			var e = $(this);
			var autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				//check for redirect
				var href = autoclose.attr('href');
				if (href) { window.location.href = href; }
				// reload transcripts grid
				$('#transcripts-management-grid').yiiGridView('update');
				$("#transcripts-waiting-approval-grid").yiiGridView('update');
			}
		});

	});
</script>