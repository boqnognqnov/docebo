<h1>
	<i class="i-sprite is-remove white large"></i>
	<?= Yii::t('transcripts', 'Reject/delete activity') ?>
</h1>

<div class="form transcripts-edit-activity">

	<?php


	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'delete-activity-form',
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => 'ajax docebo-form form-horizontal',
			'enctype' => 'multipart/form-data'
		)
	));

/* @var $form CActiveForm */

	if ($isMultiple) {
		echo CHtml::hiddenField('selected-transcripts', '', array('id' => 'input-selected-transcripts'));
	} else {
		echo CHtml::hiddenField('id_record', $model->getPrimaryKey(), array('id' => 'input-id-record'));
	}
	echo CHtml::hiddenField('confirm', 1, array('id' => 'input-confirm'));

	?>

	<?php if ($isMultiple): ?>
	<div class="control-group">
		<p>
			<?= Yii::t('transcripts', 'Selected external activities') ?>:&nbsp;<strong id="count-selected-activities"></strong>
		</p>
	</div>
	<?php endif; ?>

	<div class="control-group">
		<?= CHtml::label(Yii::t('transcripts', 'Action').':', false, array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= CHtml::label(
					CHtml::radioButton('rem-action', true, array(
						'uncheckValue' => null,
						'value' => 'reject',
						'id'=> 'rem-action-reject'
					)) . ' '
					. Yii::t('transcripts', 'Reject activity'),
					'rem-action-reject',
					array('class'=>'radio-list-element')
				) ?>
				<?= CHtml::label(
					CHtml::radioButton('rem-action', false, array(
						'uncheckValue' => null,
						'value' => 'remove',
						'id'=> 'rem-action-remove'
					)) . ' '
					. Yii::t('transcripts', 'Delete activity permanently'),
					'rem-action-remove',
					array('class'=>'radio-list-element')
				) ?>
			</div>
		</div>
	</div>

	<div class="control-group">
		<?= CHtml::label(Yii::t('transcripts', 'Add message for the user').':<br />'
			.'<p class="label-help-block">('.Yii::t('transcripts', 'optional').')</p>', 'rem-message', array('class' => 'control-label')) ?>
		<div class="controls">
			<div class="row-fluid">
				<?= CHtml::textArea('rem-message', '', array('id' => 'rem-message', 'class' => 'long-input')) ?>
			</div>
		</div>
	</div>

	<div class="form-actions">
			<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
			<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
$(function() {
	var container = $('.modal-delete-activity');
	container.controls();
	container.find('input').styler();

	<?php if ($isMultiple): ?>
	var selectedItems = $('#transcripts-waiting-approval-grid-selected-items-list').val();
	$('#input-selected-transcripts').val(selectedItems);
	if (selectedItems.length) {
		var selectedTranscripts = selectedItems.split(',');
		var countTranscripts = selectedTranscripts.length;
		$('#count-selected-activities').html(countTranscripts);
	}
	<?php endif; ?>

	$(document).undelegate(".modal-delete-activity", "dialog2.content-update"); //this is to avoid repeated event assignments when opening the dialog multiple times
	$(document).delegate(".modal-delete-activity", "dialog2.content-update", function() {
		var e = $(this);
		var autoclose = e.find("a.auto-close");
		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");
			// reload transcripts grid
			//$('#transcripts-management-grid').yiiGridView('update');
			$(".transcripts-management-grid").each(function(){
				$(this).yiiGridView('update');
			});
			$("#transcripts-waiting-approval-grid").yiiGridView('update');

			//check for other info
			var data = autoclose.data(), actionButton = $('#waiting-for-approval-action-button');
			$("#dashlet-waiting-for-approval").html(data.countWaiting);
			if (actionButton.length > 0 && data.countWaiting !== undefined) {
				$('#count-waiting-for-approval').html(data.countWaiting);
				if (data.countWaiting > 0) {
					actionButton.css('display', 'list-item');
				} else {
					actionButton.css('display', 'none');
				}
			}
		}
	});

});
//refresh gridview after operation
$(document).undelegate(".modal-edit-activity", "dialog2.content-update"); //this is to avoid repeated event assignments when opening the dialog multiple times
$(document).delegate(".modal-edit-activity", "dialog2.content-update", function() {
	var e = $(this);
	var autoclose = e.find("a.auto-close");
	if (autoclose.length > 0) {
		e.find('.modal-body').dialog2("close");
		// reload transcripts grid
		$("#transcripts-management-grid").yiiGridView('update');
		$("#transcripts-waiting-approval-grid").yiiGridView('update');
		var href = autoclose.attr('href');
		if (href) { window.location.href = href; }
		//check for other info
		var data = autoclose.data(), actionButton = $('#waiting-for-approval-action-button');
		if (actionButton.length > 0 && data.countWaiting !== undefined) {
			$('#count-waiting-for-approval').html(data.countWaiting);
			if (data.countWaiting > 0) {
				actionButton.css('display', 'list-item');
			} else {
				actionButton.css('display', 'none');
			}
		}
	}
});
</script>
