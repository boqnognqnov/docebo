<?php

/* @var $this TranscriptsManagementController */
/* @var $model TranscriptsRecord */
/* @var $form CActiveForm */

$backUrl = Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList');

$this->breadcrumbs[Yii::t('transcripts', 'External activities')] = $backUrl;
$this->breadcrumbs[] = Yii::t('catalogue', '_WAITING_APPROVAL');

?>

<h3 class="title-bold">
	<a class="docebo-back" href="<?= $backUrl ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= Yii::t('transcripts', 'Activities waiting for approval') ?>
</h3>
<br>
<br>

<?php $this->renderPartial('_mainAdminActions', array('showWaiting' => false)); ?>


<!--search box-->
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'transcripts-grid-search-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'docebo-form grid-search-form'
    ),
)); ?>



<div class="main-section">

	<div class="filters-wrapper">
		<table class="filters filters_transcripts_management">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>

							<td class="group filter-by-user">
								<table>
									<tr>
										<td class="input-label">
											<?= Yii::t('transcripts', 'Filter by user') ?>
										</td>
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/filterByUserAutocomplete', array('from' => 'waiting')) ?>" class="typeahead"
															 id="advanced-search-transcripts-by-user"
															 autocomplete="off"
															 type="text"
															 name="filter-by-user"
															 placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<button id ="transcripts-grid-user-search-clear-button" type="button" class="close grid-search-clear single">&times;</button>
											</div>
										</td>
									</tr>
								</table>
							</td>

							<td class="group filter-by-status">
								<table>
									<tr>
										<td class="input-label">
											<?= Yii::t('standard', '_SHOW') ?>
										</td>
										<td>
											<div class="input-wrapper">
												<?php
												$_statusList = array(
													'all' => Yii::t('standard', '_ALL'),
													TranscriptsRecord::STATUS_REJECTED => Yii::t('transcripts', 'Rejected'),
													TranscriptsRecord::STATUS_WAITING_APPROVAL => Yii::t('catalogue', '_WAITING_APPROVAL')
												);
												echo CHtml::dropDownList('filter-by-status', 'all', $_statusList, array('id' => 'input-filter-by-status'));
												?>
											</div>
										</td>
									</tr>
								</table>
							</td>

							<td class="group filter-by-text clearfix" align="right">
								<table>
									<tr>
										<td class="input-label">
											<?= Yii::t('transcripts', 'Filter by course name or training institute') ?>
										</td>
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/filterByTextAutocomplete', array('from' => 'waiting')) ?>" class="typeahead"
															 id="advanced-search-transcripts-by-text"
															 autocomplete="off"
															 type="text"
															 name="filter-by-text"
															 placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>"/>
												<button id ="transcripts-grid-text-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>

						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>





<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
		'gridId' => 'transcripts-waiting-approval-grid',
		'class' => 'left-selections clearfix',
		'dataProvider' => $model->dataProvider(),
		'itemValue' => 'id_record',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="transcripts-waiting-approval-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="approve"><?php echo Yii::t('transcripts', 'Approve'); ?></option>
			<option value="delete"><?php echo Yii::t('transcripts', 'Reject'); ?></option>
		</select>
		<label for="transcripts-waiting-approval-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>

		<div style="display:none;">
			<a class="open-dialog hide" data-dialog-class="modal-approve-activity" id="transcripts-approve-massive-link" href="<?= $this->createUrl('approveActivityMultiple') ?>"></a>
			<a class="open-dialog hide" data-dialog-class="modal-delete-activity" id="transcripts-delete-massive-link" href="<?= $this->createUrl('deleteActivityMultiple') ?>"></a>
		</div>
	</div>

</div>


<div class="bottom-section clearfix">

	<div id="grid-wrapper" class="TranscriptsManagementPage">
		<?php $_columns = array();
        $that = $this;
		$_columns[] = array(
			'class' => 'CCheckBoxColumn',
			'selectableRows' => 2,
			'id' => 'transcripts-waiting-approval-grid-checkboxes',
			'checked' => 'in_array($data->id_record, Yii::app()->session[\'selectedItems\'])',
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_USER'),
			'value' => array($this, 'gridRenderUserid'),
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_COURSE_NAME'),
			'value' => '$data->renderCourseName()',
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_TYPE'),
            'value' => function($data)use($that) {
                if((int)$data->course_id > 0) {
                    $transcriptsCourse = TranscriptsCourse::model()->findByPk($data->course_id);
					if (Settings::get('transcripts_require_defined_list') == 'off')
						echo '<span style="color: #aaa">' . LearningCourse::model()->getCourseTypeTranslation($transcriptsCourse->type) . '</span>';
					else echo LearningCourse::model()->getCourseTypeTranslation($transcriptsCourse->type);
                    return;
                }
				if (Settings::get('transcripts_require_defined_list') == 'on')
					echo '<span style="color: #aaa">' . $that->gridRenderCourseType($data, 0) . '</span>';
				else echo $that->gridRenderCourseType($data, 0);
            }
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_DATE'),
			'value' => array($this, 'gridRenderDate'),
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_SCORE'),
			'value' => array($this, 'gridRenderScore'),
			'htmlOptions' => array(
				'class' => 'text-center',
			),
			'headerHtmlOptions' => array(
				'class' => 'text-center',
			)
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_CREDITS'),
			'value' => array($this, 'gridRenderCredits'),
			'htmlOptions' => array(
				'class' => 'text-center',
			),
			'headerHtmlOptions' => array(
				'class' => 'text-center',
			)
		);
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('transcripts', 'Training institute'),
			'value' => '$data->renderTrainingInstituteName()',
		);
		if(PluginManager::isPluginActive('CertificationApp')){
			$_columns[] = array(
				'type'=>'raw',
				'header'=>Yii::t('certification', 'Certification'),
				'value'=>function($data, $index){
					$certificationName = CertificationItem::getCertificationNameByItemAndType($data->id_record, CertificationItem::TYPE_TRANSCRIPT);
					if($certificationName) {
						echo '<i class="admin-ico certification" rel="tooltip" title="' . CertificationItem::getCertificationNameByItemAndType( $data->id_record, CertificationItem::TYPE_TRANSCRIPT ) . '"></i>';
					}
				},
				'cssClassExpression' => function($index, $data) {
					$class = "text-center";
					return $class;
				},
				
			);
		}
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('player', 'Certificate'),
			'value' => array($this, 'gridRenderCertificate'),
			'htmlOptions' => array(
				'class' => 'text-center',
			),
			'headerHtmlOptions' => array(
				'class' => 'text-center',
			)
		);
		$_columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderLinkApproveActivity'),
			'htmlOptions' => array(
				'class' => 'column-icon text-center',
			)
		);
		$_columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderLinkRejectActivity'),
			'htmlOptions' => array(
				'class' => 'column-icon text-center',
			)
		);

		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'transcripts-waiting-approval-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProvider(),
			'columns' => $_columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				// Reset placeholder text if default one
				resetSearchPlaceholder();
				options.type = "POST";
				options.data = $("#transcripts-grid-search-form").serialize();
			}',
			'afterAjaxUpdate' => 'function(id, data) {
				$("#transcripts-waiting-approval-grid input").styler();
				$(\'a[rel="tooltip"]\').tooltip();
				$(\'span[rel="tooltip"]\').tooltip();
				$(document).controls();
			}'
		)); ?>
	</div>
</div>

<script type="text/javascript">

	function resetSearchPlaceholder() {
		var $searchText = $('#advanced-search-transcripts-by-text');
		if ($searchText.val() == "<?= Yii::t('standard', '_SEARCH'); ?>") { $searchText.val(''); }
		var $searchUser = $('#advanced-search-transcripts-by-user');
		if ($searchUser.val() == "<?= Yii::t('standard', '_SEARCH'); ?>") { $searchUser.val(''); }
	}

	$(function () {

		var $searchText = $('#advanced-search-transcripts-by-text');
		var $searchUser = $('#advanced-search-transcripts-by-user');
		var previousFilterText = $.trim($searchText.val());
		var previousFilterUser = $.trim($searchUser.val());

		var filterTranscriptsGrid = function (type) {
			switch (type) {
				case 'text': {
					previousFilterText = $.trim($searchText.val());
					$("#transcripts-waiting-approval-grid").yiiGridView('update', {});
				} break;
				case 'user': {
					previousFilterUser = $.trim($searchUser.val());
					$("#transcripts-waiting-approval-grid").yiiGridView('update', {});
				} break;
			}
		};

		$searchText.bind('keypress', function (e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				// enter was pressed, then update grid
				filterTranscriptsGrid('text');
				e.preventDefault();
				return false;
			}
		});
		$searchUser.bind('keypress', function (e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				// enter was pressed, then update grid
				filterTranscriptsGrid('user');
				e.preventDefault();
				return false;
			}
		});

		//text search filter reset button action
		$('#transcripts-grid-text-search-clear-button').on('click', function() {
			$searchText.val("");
			//if (previousFilterText != "") {
			filterTranscriptsGrid('text');
			//}
		});

		//text search filter reset button action
		$('#transcripts-grid-user-search-clear-button').on('click', function() {
			$searchUser.val("");
			//if (previousFilterUser != "") {
			filterTranscriptsGrid('user');
			//}
		});

		//others
		$('input, select').styler();
		replacePlaceholder();

		//status filter dropdown
		$('#input-filter-by-status').on('change', function() {
			$("#transcripts-waiting-approval-grid").yiiGridView('update');
		});

		//massive operations
		$('#transcripts-waiting-approval-massive-action').on('change', function() {
			var selectedTranscripts = [];
			var selectedItems = $('#transcripts-waiting-approval-grid-selected-items-list').val();
			if (selectedItems.length) { selectedTranscripts = selectedItems.split(','); }
			if (selectedTranscripts.length) { $('#transcripts-'+$(this).val()+'-massive-link').click(); }
			// clear selection
			$(this).find('option:first').attr('selected', true);
		});

	});

</script>