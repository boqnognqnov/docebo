<?php
/* @var $this LocationController */
/* @var $locationModel LtLocation */
/* @var $form CActiveForm */

$this->breadcrumbs[] = Yii::t('transcripts', 'External activities');

$canSeeGrid = true;
$canSeeCertificates = true;
//if power user is viewing some user profile via the reports
//don't check for permission when viewing own profile
if(Yii::app()->user->getIsPu() && ($model->id_user != Yii::app()->user->getIdst())) {
	if(!Yii::app()->user->checkAccess('/framework/admin/transcripts/view')  && !Yii::app()->user->checkAccess('/lms/admin/report/view')){
		$canSeeGrid = false;
	}
	if(!Yii::app()->user->checkAccess('/lms/admin/certificate/view')){
		$canSeeCertificates = false;
	}
}

?>

<h2><?= Yii::t('transcripts', 'External activities') ?></h2>
<?php if($canSeeGrid): ?>
<br>

<?php if((Settings::get('transcripts_allow_users', 'off') == 'on') || (Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPu())): ?>
		<ul id="docebo-bootcamp-transcripts" class="docebo-bootcamp docebo-bootcamp-inline clearfix">
			<li>
				<a id="create-new-activity"
					 href="<?= $this->createUrl('createActivity', array('idUser' => $model->id_user)) ?>"
					 class="open-dialog"
					 data-dialog-class="modal-edit-activity"
					 data-helper-title="<?= Yii::t('coursereport', '_ADD_ACTIVITY') ?>"
					 data-helper-text="<?= Yii::t('helper', 'New external activity - transcripts') ?>">
					<span class="transcripts-sprite add-big-black"></span>
					<span class="transcripts-sprite add-big-white white"></span>
					<h4><?= Yii::t('coursereport', '_ADD_ACTIVITY') ?></h4>
				</a>
			</li>

			<li class="helper">
				<a href="#">
					<span class="i-sprite is-circle-quest large"></span>
					<h4 class="helper-title"></h4>
					<p class="helper-text"></p>
				</a>
			</li>

		</ul>
<?php endif;?>

<!--search box-->
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'transcripts-grid-search-form',
	'method' => 'POST',
	'htmlOptions' => array(
		'class' => 'docebo-form grid-search-form'
	),
)); ?>

<div class="main-section" id="search-filter-container">

	<div class="filters-wrapper">
		<table class="filters filters_transcripts_management">
			<tbody>
			<tr>
				<td class="table-padding">
					<table class="table-border">
						<tr>

							<?php if (Settings::get('transcripts_admin_approval', 'off') == 'on'): ?>
							<td class="group filter-by-status">
								<table>
									<tr>
										<td class="input-label">
											<?= Yii::t('transcripts', 'Show also').':' ?>
										</td>
										<td>
											<?php
											echo CHtml::hiddenField('filter-by-status[]', 'approved', array('id' => 'input-filter-by-status-approved'));
											$_countWaiting = TranscriptsRecord::model()->countUserActivitiesByStatus($model->id_user, TranscriptsRecord::STATUS_WAITING_APPROVAL);
											$_countRejected = TranscriptsRecord::model()->countUserActivitiesByStatus($model->id_user, TranscriptsRecord::STATUS_REJECTED);

											echo Chtml::checkBox('filter-by-status[]', true, array('id' => 'input-filter-by-status-waiting', 'value' => 'waiting'));
											echo Chtml::label('&nbsp;'.Yii::t('transcripts', 'Activities waiting for approval').' (<span id="count-waiting">'.$_countWaiting.'</span>)',
											'input-filter-by-status-waiting', array(
													'class'=>'checkbox-list-element'
												));
                                            ?>
                                        </td>
                                        <td>
                                            <?php
											echo Chtml::checkBox('filter-by-status[]', true, array('id' => 'input-filter-by-status-rejected', 'value' => 'rejected'));
											echo Chtml::label('&nbsp;'.Yii::t('transcripts', 'Rejected activities').' (<span id="count-rejected">'.$_countRejected.'</span>)',
												'input-filter-by-status-rejected', array(
													'class'=>'checkbox-list-element'
												));
											?>
										</td>
									</tr>
								</table>
							</td>
							<?php endif; ?>

							<td class="group filter-by-text clearfix" align="right">
								<table>
									<tr>
										<!--<td class="input-label">
											<?= Yii::t('transcripts', 'Filter by course name or training institute') ?>
										</td>-->
										<td>
											<div class="input-wrapper">
												<input data-url="<?= Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsUser/filterByTextAutocomplete') ?>"
															 class="typeahead"
															 id="transcripts-grid-search"
															 autocomplete="off"
															 type="text"
															 name="filter-by-text"
															 placeholder="<?= Yii::t('standard', '_SEARCH') ?>"/>
												<button id ="transcripts-grid-text-search-clear-button" type="button" class="close grid-search-clear">&times;</button>
												<span class="search-icon"></span>
											</div>
										</td>
									</tr>
								</table>
							</td>

						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>

<?php $this->endWidget(); ?>

<!--locations grid-->
<div id="grid-wrapper" class="">
	<?php

	$_columns = array();
    $that = $this;
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_COURSE_NAME'),
		'value' => '$data->renderCourseName()',
	);
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_TYPE'),
        'value' => function($data)use($that) {
            if((int)$data->course_id > 0) {
				$transcriptsCourse = TranscriptsCourse::model()->findByPk($data->course_id);
				if (Settings::get('transcripts_require_defined_list') == 'off')
					echo '<span style="color: #aaa">' . LearningCourse::model()->getCourseTypeTranslation($transcriptsCourse->type) . '</span>';
				else echo LearningCourse::model()->getCourseTypeTranslation($transcriptsCourse->type);
				return;
			}
			if (Settings::get('transcripts_require_defined_list') == 'on')
				echo '<span style="color: #aaa">'.$that->gridRenderCourseType($data, 0).'</span>';
			else echo $that->gridRenderCourseType($data, 0);
        }
	);
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_DATE'),
		'value' => array($this, 'gridRenderDate'),
	);
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_SCORE'),
		'value' => array($this, 'gridRenderScore'),
		'htmlOptions' => array(
			'class' => 'text-center',
		),
		'headerHtmlOptions' => array(
			'class' => 'text-center',
		)
	);
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('standard', '_CREDITS'),
		'value' => array($this, 'gridRenderCredits'),
		'htmlOptions' => array(
			'class' => 'text-center',
		),
		'headerHtmlOptions' => array(
			'class' => 'text-center',
		)
	);
	$_columns[] = array(
		'type' => 'raw',
		'header' => Yii::t('transcripts', 'Training institute'),
		'value' => '$data->renderTrainingInstituteName()',
	);
	if(PluginManager::isPluginActive('CertificationApp')){
		$_columns[] = array(
			'type'=>'raw',
			'header'=>Yii::t('certification', 'Certification'),
			'value'=>function($data, $index){
				$certificationName = CertificationItem::getCertificationNameByItemAndType($data->id_record, CertificationItem::TYPE_TRANSCRIPT);
				if($certificationName) {
					echo '<i class="admin-ico certification" rel="tooltip" title="' . CertificationItem::getCertificationNameByItemAndType( $data->id_record, CertificationItem::TYPE_TRANSCRIPT ) . '"></i>';
				}
			},
			'cssClassExpression' => function($index, $data) {
				$class = "text-center";
				return $class;
			},

		);
	}
	if($canSeeCertificates)
	{
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('player', 'Certificate'),
			'value' => array($this, 'gridRenderCertificate'),
			'htmlOptions' => array(
				'class' => 'text-center',
			),
			'headerHtmlOptions' => array(
				'class' => 'text-center',
			)
		);
	}
	if (Settings::get('transcripts_admin_approval', 'off') == 'on') {
		//activities status is showed only if admin approvations is required, otherwise they are always considered as "approved"
		$_columns[] = array(
			'type' => 'raw',
			'header' => Yii::t('standard', '_STATUS'),
			'value' => array($this, 'gridRenderStatus'),
			'htmlOptions' => array(
				'class' => 'text-center',
			),
			'headerHtmlOptions' => array(
				'class' => 'text-center',
			)
		);
	}
	if((Yii::app()->user->getIsGodadmin() || Yii::app()->user->getIsPu())) {
		$_columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderLinkEditActivity'),
			'htmlOptions' => array(
				'class' => 'edit-column text-center',
			)
		);
		$_columns[] = array(
			'type' => 'raw',
			'value' => array($this, 'gridRenderLinkDeleteActivity'),
			'htmlOptions' => array(
				'class' => 'button-column-single text-center',
			)
		);
	} elseif(Settings::get('transcripts_allow_users', 'off') == 'on') {
		$_columns[] = array(
			'type' => 'raw',
			'header' => '',
			'htmlOptions' => array(
				'class' => 'controls'
			),
			'value' => function ($data) {
				if ($data->status != TranscriptsRecord::STATUS_APPROVED) {
					$linkEdit = Docebo::createAbsoluteLmsUrl('TranscriptsApp/transcriptsUser/editActivity', array('id_record' => $data->id_record));
					$linkDelete = Docebo::createAbsoluteLmsUrl('TranscriptsApp/transcriptsUser/deleteActivity', array('id_record' => $data->id_record));

					$controlParams = array(
						'arrayData' => array(
							array('url' => $linkEdit, 'text' => 'Edit', 'method' => 'editActivity', 'dialog-title' => ''),
							array('url' => $linkDelete, 'text' => 'Delete', 'method' => 'deleteActivity', 'dialog-title' => '')
						),
						'containerClass' => 'customDropdown '
					);
					$this->widget('common.widgets.DropdownControls', $controlParams);
				}
			}
		);
	}

	$this->widget('DoceboCGridView', array(
		'id' => 'transcripts-management-grid',
		'htmlOptions' => array('class' => 'printable grid-view transcripts-management-grid'),
		'dataProvider' => $model->dataProvider(),
		'columns' => $_columns,
		'rowCssClassExpression' => '($row%2 ? $this->rowCssClass[1] : $this->rowCssClass[0])'
			.'.(Settings::get("transcripts_admin_approval", "off") == "on" && $data->status == TranscriptsRecord::STATUS_REJECTED ? " row-rejected" : "")',
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			// Reset placeholder text if default one
			resetSearchPlaceholder();
			options.type = "POST";
			options.data = $("#transcripts-grid-search-form").serialize()
			$(\'[rel="tooltip"]\').tooltip();
		}',
		'afterAjaxUpdate' => 'function(id, data){
			$(\'[rel="tooltip"]\').tooltip();
			$(document).controls();
			//update status filter count values
			var countWaiting = parseInt($(data).find("#count-waiting").html());
			var countRejected = parseInt($(data).find("#count-rejected").html());
			$("#count-waiting").html(countWaiting);
			$("#count-rejected").html(countRejected);
			updateCountCheckboxes();
			$("#transcripts-management-grid input").styler();
		}'
	));
	?>
</div>

<?php else:?>

	<?php echo Yii::t('standard', 'Do not have the needed permissions');?>

<?php endif;?>


<script type="text/javascript">

	$(function() {
		$(document).off("click", '[data-method="editActivity"]');
		$(document).off("click", '[data-method="deleteActivity"]');

		$(document).on("click", '[data-method="editActivity"]', function(){
			var url = $(this).attr("data-url");
			openDialog(false, '<?=Yii::t('standard', '_MOD')?>', url, 'modal-edit-activity', 'modal-edit-activity', 'GET');
		})

		$(document).on("click", '[data-method="deleteActivity"]', function(){
			var url = $(this).attr("data-url");
			openDialog(false, '<?=Yii::t('standard', '_DEL')?>', url, 'modal-delete-activity', 'modal-delete-activity', 'GET');
		})
	});

	function resetSearchPlaceholder() {
		var $search = $('#transcripts-grid-search');
		if ($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
			$search.val('');
	}

	function updateCountCheckboxes() {
		var countWaiting = parseInt($("#count-waiting").html());
		var countRejected = parseInt($("#count-rejected").html());
		var $inputWaiting = $('#input-filter-by-status-waiting');
		var $inputRejected = $('#input-filter-by-status-rejected');
		if (countWaiting <= 0) {
			$inputWaiting.attr('disabled', 'disabled');
			$inputWaiting.styler();
		} else {
			$inputWaiting.removeAttr('disabled');
			$inputWaiting.styler();
		}
		if (countRejected <= 0) {
			$inputRejected.attr('disabled', 'disabled');
			$inputRejected.styler();
		} else {
			$inputRejected.removeAttr('disabled');
			$inputRejected.styler();
		}
	}

	$(function () {
		$('[rel="tooltip"]').tooltip();

		var $search = $('#transcripts-grid-search');
		var previousFilter = $.trim($search.val());

		var filterTranscriptsGrid = function () {
			previousFilter = $.trim($search.val());
			$("#transcripts-management-grid").yiiGridView('update');
		};

		$('#docebo-bootcamp-transcripts').doceboBootcamp();

		// on enter key press
		$search.bind('keypress', function (e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				// enter was pressed, then update rid
				filterTranscriptsGrid();
				e.preventDefault();
				return false;
			}
		});

		//text search filter reset button action
		$('#transcripts-grid-text-search-clear-button').on('click', function() {
			$search.val("");
			if (previousFilter != "") {
				filterTranscriptsGrid();
			}
		});

		//status search filters actions
		$('#input-filter-by-status-waiting').on('change', function() { $("#transcripts-management-grid").yiiGridView('update'); });
		$('#input-filter-by-status-rejected').on('change', function() { $("#transcripts-management-grid").yiiGridView('update'); });

		//input styler
		$('#search-filter-container').find('input').styler();
		//updateCountCheckboxes(); //Breaks the gridview auto refresh

		//TinyMCE FIX
		$(document).on("dialog2.closed", ".modal", function () {
			$("#transcripts-management-grid").yiiGridView('update');

			// Remove editor
			try {
				if (tinymce.editors.length >0)
					tinymce.editors[Notification.textareaid].remove();
			}
			catch(e) {
				tinyMCE.editors = [];
			}
		});
	});
</script>

<style>
	.gridItemsContainer {
		clear: none !important;
	}
</style>