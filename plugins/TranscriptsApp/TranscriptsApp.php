<?php

/**
 * Plugin for Label
 */

class TranscriptsApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = true;

	/**
	 * @var string Name of the plugin; needed to identify plugin folder and entry script
	 */
	public $plugin_name = 'TranscriptsApp';

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name=__CLASS__) {
        return parent::plugin($class_name);
    }

    /**
     * Activation method. Creates the model
     * @return bool|void
     */
    public function activate() {


        $sql = "
-- -----------------------------------------------------
-- Table `transcripts_record`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `transcripts_record` (
  `id_record` INT(11) NOT NULL AUTO_INCREMENT,
  `id_user` INT(11) NOT NULL,
  `course_name` VARCHAR(255) NOT NULL,
  `course_type` ENUM('classroom', 'elearning') NOT NULL DEFAULT 'elearning',
  `from_date` DATE NULL,
  `to_date` DATE NULL,
  `score` INT(11) NOT NULL,
  `credits` FLOAT NULL DEFAULT NULL,
  `training_institute` VARCHAR(255) NULL,
  `certificate` VARCHAR(512) NULL,
  `original_filename` varchar(512) DEFAULT NULL,        		
  `status` ENUM('waiting_approval', 'approved', 'rejected') NOT NULL default 'waiting_approval',
  `id_admin` INT(11) NULL,
  `notes` MEDIUMTEXT NULL,
  PRIMARY KEY (`id_record`),
  INDEX `fk_transcripts_record_core_user` (`id_user`),
  CONSTRAINT `fk_transcripts_record_core_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
			$command = Yii::app()->db->createCommand($sql);
			$command->execute();

			$core_role = CoreRole::model()->findByAttributes(array('roleid' => '/framework/admin/transcripts/view'));

			if (empty($core_role)) {
				$sql = "INSERT INTO `core_st` VALUES (NULL);";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();

				$sql = "INSERT INTO `core_role` (`idst`, `roleid`) VALUES (LAST_INSERT_ID(), '/framework/admin/transcripts/view');";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();

				$sql = "INSERT INTO `core_role_members` (`idst`, `idstMember`) VALUES (LAST_INSERT_ID(), 3);";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}

			$core_role = CoreRole::model()->findByAttributes(array('roleid' => '/framework/admin/transcripts/mod'));

			if (empty($core_role)) {
				$sql = "INSERT INTO `core_st` VALUES (NULL);";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();

				$sql = "INSERT INTO `core_role` (`idst`, `roleid`) VALUES (LAST_INSERT_ID(), '/framework/admin/transcripts/mod');";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();

				$sql = "INSERT INTO `core_role_members` (`idst`, `idstMember`) VALUES (LAST_INSERT_ID(), 3);";
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
			}

			parent::activate();
		}

	/**
	 * Plugin deactivation method
	 * @return bool|void
	 */
	public function deactivate()
	{
		parent::deactivate();
	}
}
