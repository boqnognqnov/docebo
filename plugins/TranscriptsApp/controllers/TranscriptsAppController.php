<?php

class TranscriptsAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	public function actionSettings() {

        /**
         * Use the LMS layout since the ADMIN layout bring filthy CSS and JS
         * that are making the UI looks ugly
         */
        $this->layout = "//layouts/lms";

		$settings = new TranscriptsAppSettingsForm();

		if (isset($_POST['TranscriptsAppSettingsForm'])) {
			$settings->setAttributes($_POST['TranscriptsAppSettingsForm']);

			if ($settings->validate()) {
//				if(Settings::get('transcripts_require_defined_list') != $settings->transcripts_require_defined_list && $settings->transcripts_require_defined_list == 'off') {
					// if the setting is changed, we delete all currently entered courses and institutes if we were in "required mode"
//					Yii::app()->db->createCommand()
//							->update(TranscriptsRecord::model()->tableName(), array(
//									'course_name' => '',
//									'training_institute' => '',
//									'course_type' => '',
//							));
//				}

				Settings::save('transcripts_allow_users', $settings->transcripts_allow_users, 'enum', 3);
				Settings::save('transcripts_admin_approval', $settings->transcripts_admin_approval, 'enum', 3);
				Settings::save('transcripts_require_defined_list', $settings->transcripts_require_defined_list, 'enum', 3);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->transcripts_allow_users = (Settings::get('transcripts_allow_users') === 'on' ? true : false);
		$settings->transcripts_admin_approval = (Settings::get('transcripts_admin_approval') === 'on' ? true : false);
		$settings->transcripts_require_defined_list = (Settings::get('transcripts_require_defined_list') === 'on' ? true : false);

		$this->render('settings', array('settings' => $settings));
	}



	public function actionIndex() {
		//...
	}

}
