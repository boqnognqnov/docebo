<?php


class TranscriptsManagementController extends TranscriptsBaseController/*AdminController*/ {


	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/transcripts/view'),
			'actions' => array(
				'index',
				'activitiesList',
				'filterByUserAutocomplete',
				'filterByTextAutocomplete',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/transcripts/mod'),
			'actions' => array(
				'waitingForApproval',
				'createActivity',
				'editActivity',
				'deleteActivity',
				'approveActivity',
				'deleteActivityMultiple',
				'approveActivityMultiple',
				'importFromCsv',
				'csvSample',
				'assignUserAutocomplete',
				'axImportErrorsDetails'
			),
		);

        if(Settings::get('transcripts_require_defined_list', false) == 'on' || Yii::app()->user->checkAccess(Yii::app()->user->level_godadmin)) {
            $res[] = array(
                'allow',
                'roles' => array(Yii::app()->user->level_godadmin),
                'actions' => array(
                    'manageCoursesAndInstitutes',
                    'manageInstitutes',
                    'editInstitute',
                    'editCourse',
                    'deleteCourse',
                    'deleteInstitute',
                    'assignInstitute',
                    'filterCoursesByTextAutocomplete',
                    'filterInstitutesByTextAutocomplete',
                ),
            );
        }

		$res[] = array(
			'allow',
			'users' => array('@'), //all logged users can access this section
			'actions' => array(
				'gridData',
			),
		);

		$res[] = array(
			'deny',
			'users' => array('*'), // deny all
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}




	public function beforeAction($action)
	{
		$toLoad = array();

		//collect items to be loaded by action
		switch (lcfirst($action->id)) {
			case 'index':
			case 'activitiesList':
			case 'waitingForApproval': {
				$toLoad[] = 'datepicker';
				$toLoad[] = 'transcripts.css';
			} break;
			case 'importFromCsv': {
				$toLoad[] = 'transcripts.css';
			} break;
		}

		//physically load colected items, if any
		if (!empty($toLoad)) {
			foreach ($toLoad as $loadItem) {
				switch($loadItem) {
					case 'datepicker': {
						//Load the datepicker (used in the new/edit actiovity modal)
						Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
						Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');
					} break;
					case 'transcripts.css': {
						$cs = Yii::app()->getClientScript();
						$cs->registerCssFile($this->getModule()->getAssetsUrl().'/css/transcripts.css');
					} break;
				}
			}
		}

		return parent::beforeAction($action);
	}



	//internal renderers for grids

	protected function gridRenderLinkApproveActivity($row, $index) {
		$html = CHtml::link('&nbsp;', $this->createUrl('approveActivity', array('id_record' => $row->id_record)), array(
			'class' => 'transcripts-sprite approve-small open-dialog',
			'data-dialog-class' => 'modal-approve-activity',
			'title' => Yii::t('transcripts', 'Approve activity'),
			'removeOnClose' => 'true',
			'closeOnOverlayClick' => 'true',
			'closeOnEscape' => 'true',
			'rel' => 'tooltip',
		));
		return $html;
	}

	protected function gridRenderLinkRejectActivity($row, $index) {
		$iconStyle = ($row->status == TranscriptsRecord::STATUS_REJECTED
			? 'delete-action'
			: 'transcripts-sprite reject-small');
		$iconTitle = ($row->status == TranscriptsRecord::STATUS_REJECTED
			? Yii::t('transcripts', 'Permanently delete activity')
			: Yii::t('transcripts', 'Reject/delete activity'));
		$html = CHtml::link('&nbsp;', $this->createUrl('deleteActivity', array('id_record' => $row->id_record)), array(
			'class' => $iconStyle.' open-dialog',
			'data-dialog-class' => 'modal-delete-activity',
			'title' => $iconTitle,
			'removeOnClose' => 'true',
			'closeOnOverlayClick' => 'true',
			'closeOnEscape' => 'true',
			'rel' => 'tooltip',
		));
		return $html;
	}



//actions

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$this->actionActivitiesList(); //"index" action is just an alias for "activitiesList" action
	}



	public function actionActivitiesList() {
		Yii::app()->tinymce;
		if(!isset(Yii::app()->session['importTranscriptsErrors']))
		{
			Yii::app()->session['importTranscriptsErrors'] = array();
			Yii::app()->session['importTranscriptsErrorsDetails'] = array();
		}

		if(is_array(Yii::app()->session['importTranscriptsErrors']) && !empty(Yii::app()->session['importTranscriptsErrors']))
		{
			//var_dump(Yii::app()->session['importTranscriptsErrors']);
			Yii::app()->user->setFlash('error',
				Yii::t('standard', '_OPERATION_FAILURE').'. '
				.CHtml::link(Yii::t('apps', 'More details&hellip;'), 'index.php?r=TranscriptsApp/transcriptsManagement/axImportErrorsDetails', array(
					'class' => 'open-dialog group-select-users',
					'rel' 	=> 'users-selector',
					'alt' 	=> Yii::t('apps', 'More details&hellip;'),
					'data-dialog-title' => Yii::t('standard', '_DETAILS'),
					'title'	=> Yii::t('standard', '_DETAILS'),
				)));
			Yii::app()->session['importTranscriptsErrorsDetails'] = Yii::app()->session['importTranscriptsErrors'];
			Yii::app()->session['importTranscriptsErrors'] = array();
		}
		elseif(is_array(Yii::app()->session['importTranscriptsErrors']) && empty(Yii::app()->session['importTranscriptsErrors']))
			Yii::app()->session['importTranscriptsErrorsDetails'] = array();

		unset(Yii::app()->session['importTranscriptsStep']);

		$model = TranscriptsRecord::model();
		$model->scenario = 'admin-activities-list';

		$this->render('activitiesList', array(
			'model' => $model
		));
	}



	public function actionCreateActivity() {
		$model = new TranscriptsRecord();
		$userError = false;
		list($listOfInstitutes, $coursesPerInstitutes, $isRequireToChooseInstitute, $listCoursesTypes) = TranscriptsRecord::getCourseInstituteFields();

		if (isset($_POST['TranscriptsRecord'])) {

			$inputData = $_POST['TranscriptsRecord'];
			$user = null;

			//we need to specify an user by its numeric ID. If not present, try to retrieve it by its username.
			if (!isset($inputData['id_user']) || empty($inputData['id_user'])) {
				$user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($_POST['userid'])));
				if (!$user) {
					// no user has been specified: return an error
					$userError = Yii::t('transcripts', 'No user has been specified');
				} else {
					if($user->isUserExpired() || $user->valid == CoreUser::STATUS_NOTVALID)
						$userError = Yii::t('transcripts', 'No user has been specified');
					else
					$model->id_user = $user->getPrimaryKey();
				}
			}

			$model->attributes = $inputData;
			$model->status = TranscriptsRecord::STATUS_APPROVED;
			$model->id_admin = Yii::app()->user->id;

			//check if a certificate file has been uploaded
			if (isset($_FILES['certificate']) && ($_FILES['certificate']['name'] != '')) {
				$model->attachCertificateFile($_FILES['certificate']['tmp_name'], $_FILES['certificate']['name']);
			}
			if (intval($model->training_institute) == $model->training_institute
					&& intval($model->course_name) == $model->course_name
					&& Settings::get('transcripts_require_defined_list') == 'on') {
				$model->training_institute = Yii::app()->db->createCommand()->select('institute_name')
						->from(TranscriptsInstitute::model()->tableName())
						->where('id=:id', array(':id' => $model->training_institute))->queryScalar();

				$course = Yii::app()->db->createCommand()->select()
						->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_name))->queryAll()[0];
				$model->course_name = $course['course_name'];
				$model->course_type = $course['type'];
				$model->course_id = $course['id'];
			}

			$model->scenario = 'elearning';
			if ($model->course_type == LearningCourse::TYPE_CLASSROOM)
				$model->scenario = 'classroom';
			if ($isRequireToChooseInstitute) {
				$model->course_type = null;
				$model->course_name = null;
				$model->training_institute = null;
			}
			if($model->validate()) {
				if (!$model->save()) { throw new CHttpException(500, 'Error while saving activity data'); }

				if (isset($_POST['certification']) && PluginManager::isPluginActive('CertificationApp')) {
					// Save Certification association (retraining)
					CertificationItem::assignItemToCertification(CertificationItem::TYPE_TRANSCRIPT, $model->getPrimaryKey(), intval($_POST['certification']));

					// Raise an event upon approval
					$eventParams = array(
						'transcript' 	=> $model,
						'user'			=> $user
					);
					Yii::app()->event->raise('ExternalTrainingApproved', new DEvent($this, $eventParams));
				}

				// Update additional fields
				if(isset($_POST['Transcripts']['additional']))
					$model->updateAdditionalFiled($_POST['Transcripts']['additional']);

				// close dialog and reload activities
				echo '<a href="" class="auto-close" data-count-waiting="'.TranscriptsRecord::model()->countWaitingForApproval().'"></a>';
				return;
			}
		}


		$selectedCourseId = null;
		$selectedInstituteId = null;
		if ($isRequireToChooseInstitute) {
			if ($model->course_id) {
				$selectedCourseId = $model->course_id;
				$selectedInstituteId = Yii::app()->db->createCommand()
						->select('institute_id')->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_id))->queryScalar();
			}
		}

		if(!$model->course_type)
			$model->course_type = LearningCourse::TYPE_ELEARNING;
		$this->renderPartial('TranscriptsCommonViews._editActivity', array(
			'model' => $model,
			'formType' => 'admin',
			'userError' => $userError,
			'isRequireToChooseInstitute' => $isRequireToChooseInstitute,
			'listOfInstitutes' => $listOfInstitutes,
			'coursesPerInstitutes' => $coursesPerInstitutes,
			'listCoursesTypes' => $listCoursesTypes,
			'selectedCourseId' => $selectedCourseId,
			'selectedInstituteId' => $selectedInstituteId,
		));
	}

	public function actionEditActivity() {
		$idRecord = intval(Yii::app()->request->getParam('id_record'));
		$model = TranscriptsRecord::model()->findByPk($idRecord);
		if (!$model) { throw new CHttpException(500, 'Invalid activity'); }
		list($listOfInstitutes, $coursesPerInstitutes, $isRequireToChooseInstitute, $listCoursesTypes) = TranscriptsRecord::getCourseInstituteFields();

		if (isset($_POST['TranscriptsRecord'])) {

			$inputData = $_POST['TranscriptsRecord'];

			$model->attributes = $inputData;
			if (intval($model->training_institute) == $model->training_institute
					&& intval($model->course_name) == $model->course_name
					&& Settings::get('transcripts_require_defined_list') == 'on') {
				$model->training_institute = Yii::app()->db->createCommand()->select('institute_name')
						->from(TranscriptsInstitute::model()->tableName())
						->where('id=:id', array(':id' => $model->training_institute))->queryScalar();

				$course = Yii::app()->db->createCommand()->select()
						->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_name))->queryAll()[0];
				$model->course_name = $course['course_name'];
				$model->course_type = $course['type'];
				$model->course_id = $course['id'];
			}

			if(Settings::get('transcripts_require_defined_list') == 'off'){
				// delete course_id value - not needed, we are in free text input state
				$model->course_id = null;
			}

			//check if a certificate file has been uploaded
			if (isset($_FILES['certificate']) && ($_FILES['certificate']['name'] != '')) {
				$model->attachCertificateFile($_FILES['certificate']['tmp_name'], $_FILES['certificate']['name']);
			}

			$model->scenario = 'elearning';
			if ($model->course_type == LearningCourse::TYPE_CLASSROOM)
				$model->scenario = 'classroom';
			if ($isRequireToChooseInstitute) {
				$model->course_type = null;
				$model->course_name = null;
				$model->training_institute = null;
			}

			if($model->validate()) {
				if (!$model->save()) { throw new CHttpException(500, 'Error while saving activity data'); }

				if (isset($_POST['certification']) && PluginManager::isPluginActive('CertificationApp')) {
					// Save Certification association (retraining)
					CertificationItem::assignItemToCertification(CertificationItem::TYPE_TRANSCRIPT, $model->getPrimaryKey(), intval($_POST['certification']));

					//we need to specify an user by its numeric ID. If not present, try to retrieve it by its username.
					if (!isset($inputData['id_user']) || empty($inputData['id_user'])) {
						$user = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($_POST['userid'])));
						if (!$user) {
							// no user has been specified: return an error
							$userError = Yii::t('transcripts', 'No user has been specified');
						} else {
							$model->id_user = $user->getPrimaryKey();
						}
					}else{
						$user = CoreUser::model()->findByPk($inputData['id_user']);
					}
					// Raise an event upon approval
					$eventParams = array(
							'transcript' 	=> $model,
							'user'			=> $user
					);
					Yii::app()->event->raise('ExternalTrainingApproved', new DEvent($this, $eventParams));
				}

				// Update additional fields
				if(isset($_POST['Transcripts']['additional']))
					$model->updateAdditionalFiled($_POST['Transcripts']['additional']);

				// close dialog and reload activities
				echo '<a href="" class="auto-close" data-count-waiting="'.TranscriptsRecord::model()->countWaitingForApproval().'"></a>';
				return;
			}
		}

		$selectedCourseId = null;
		$selectedInstituteId = null;
		if ($isRequireToChooseInstitute) {
			if ($model->course_id) {
				$selectedCourseId = $model->course_id;
				$selectedInstituteId = Yii::app()->db->createCommand()
						->select('institute_id')->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_id))->queryScalar();
			}
		}
		if (empty($model->course_type)) {
			$model->course_type = Yii::app()->db->createCommand()
					->select('type')->from(TranscriptsCourse::model()->tableName())
					->where('id=:id', array(':id'=>$model->course_id))->queryScalar();
		}

		$this->renderPartial('TranscriptsCommonViews._editActivity', array(
			'model' => $model,
			'formType' => 'admin',
			'isRequireToChooseInstitute' => $isRequireToChooseInstitute,
			'listOfInstitutes' => $listOfInstitutes,
			'coursesPerInstitutes' => $coursesPerInstitutes,
			'listCoursesTypes' => $listCoursesTypes,
			'selectedCourseId' => $selectedCourseId,
			'selectedInstituteId' => $selectedInstituteId,
		));
	}

	public function actionDeleteActivity() {

		$rq = Yii::app()->request;

		//retrieve record instance
		$idRecord = intval($rq->getParam('id_record'));
		$model = TranscriptsRecord::model()->findByPk($idRecord);
		if (!$model) { throw new CHttpException(500, 'Invalid activity'); }
		$model->scenario = TranscriptsRecord::SCENARIO_ADMIN_DELETE;

		if ($rq->getParam('confirm', false) !== false) {
			//set optional user reject/delete message
			$model->userMessage = $rq->getParam('rem-message');

			//detect which action must be executed
			switch ($rq->getParam('rem-action')) {
				case 'reject': {
					//set "rejected" status to the record
					if (!$model->reject()) { throw new CHttpException(500, 'Error while rejecting external activity'); }
				} break;
				case 'remove': {
					//physically delete record from DB
					if (!$model->delete()) { throw new CHttpException(500, 'Error while deleting external activity'); }
				} break;
				default: {
					throw new CHttpException(500, 'Invalid specified action');
				} break;
			}

			// close dialog and reload activities
			echo '<a href="" class="auto-close" data-count-waiting="'.TranscriptsRecord::model()->countWaitingForApproval(true).'"></a>';
			return;
		}

		if ($model->status == TranscriptsRecord::STATUS_REJECTED) {
			//already rejected, allow to just delete it
			$this->renderPartial('TranscriptsCommonViews._deleteActivity', array(
				'model' => $model,
				'formType' => 'admin'
			));
		} else {
			//multiple option
			$this->renderPartial('_deleteActivity', array(
				'model' => $model
			));
		}
	}


	public function actionApproveActivity() {

		$rq = Yii::app()->request;

		//retrieve record instance
		$idRecord = intval($rq->getParam('id_record'));
		$model = TranscriptsRecord::model()->findByPk($idRecord);
		if (!$model) { throw new CHttpException(500, 'Invalid activity'); }
		$model->scenario = TranscriptsRecord::SCENARIO_ADMIN_APPROVAL;

		if ($rq->getParam('confirm', false) !== false) {

			//set "approved" status to the record
			if (!$model->approve()) { throw new CHttpException(500, 'Error while approving external activity'); }

			// close dialog and reload activities
			$href = '';/*(TranscriptsRecord::model()->countWaitingForApproval() <= 0
				? Docebo::createAppUrl('admin:TranscriptsApp/TranscriptsManagement/activitiesList')
				: '');*/
			echo '<a href="'.$href.'" class="auto-close"></a>';
			return;
		}
		$courseName ='';
		if (empty($model->course_name)) {
			// get course name from the relation
			$courseName = Yii::app()->db->createCommand()->select('course_name')
					->from(TranscriptsCourse::model()->tableName())
					->where('id=:id', array(':id' => $model->course_id))->queryScalar();
		} else {
			$courseName = $model->course_name;
		}
		$this->renderPartial('_approveActivity', array(
			'model' => $model,
			'courseName' => $courseName
		));
	}



	public function actionDeleteActivityMultiple() {

		$rq = Yii::app()->request;

		if ($rq->getParam('confirm', false) !== false) {

			$userMessage = $rq->getParam('rem-message');
			$selectedTranscripts = Yii::app()->request->getParam('selected-transcripts');
			$transcripts = (!empty($selectedTranscripts) ? explode(',', $selectedTranscripts) : array());

			foreach ($transcripts as $transcript) {
				//set "approved" status to the record
				$model = TranscriptsRecord::model()->findByPk($transcript);
				if (!$model) { throw new CHttpException(500, 'Invalid specified external activity'); }
				$model->scenario = TranscriptsRecord::SCENARIO_ADMIN_DELETE;
				if ($userMessage) { $model->userMessage = $userMessage; }

				//detect which action must be executed
				switch ($rq->getParam('rem-action')) {
					case 'reject': {
						//set "rejected" status to the record
						if ($model->status != TranscriptsRecord::STATUS_REJECTED) {
							if (!$model->reject()) { throw new CHttpException(500, 'Error while rejecting external activity'); }
						} else {
							//activity has been already rejected, no further operations requested
							//$model->id_admin = Yii::app()->user->id;
							//$model->save();
						}
					} break;
					case 'remove': {
						//physically delete record from DB
						if (!$model->delete()) { throw new CHttpException(500, 'Error while deleting external activity'); }
					} break;
					default: {
					throw new CHttpException(500, 'Invalid specified action');
					} break;
				}
			}

			// close dialog and reload grid
			echo '<a href="#" class="auto-close"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('_deleteActivity', array(
			'isMultiple' => true
		));
	}



	public function actionApproveActivityMultiple() {

		$rq = Yii::app()->request;

		if ($rq->getParam('confirm', false) !== false) {

			$selectedTranscripts = Yii::app()->request->getParam('selected-transcripts');
			$transcripts = (!empty($selectedTranscripts) ? explode(',', $selectedTranscripts) : array());

			foreach ($transcripts as $transcript) {
				//set "approved" status to the record
				$model = TranscriptsRecord::model()->findByPk($transcript);
				if (!$model) { throw new CHttpException(500, 'Invalid specified external activity'); }
				$model->scenario = 'admin-approve';

				if ($model->status != TranscriptsRecord::STATUS_APPROVED) {
					if (!$model->approve()) { throw new CHttpException(500, 'Error while approving external activity'); }
				} else {
					//activity has been already approved, no further operations requested
					//$model->id_admin = Yii::app()->user->id;
					//$model->save();
				}

			}

			// close dialog and reload grid
			echo '<a href="#" class="auto-close"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('_approveActivity', array(
			'isMultiple' => true
		));
	}



	public function actionWaitingForApproval() {
		//Call tinymce in case when we have Free text field from Additional fields
		Yii::app()->tinymce;
		unset(Yii::app()->session['importTranscriptsStep']);

		$model = TranscriptsRecord::model();
		$model->scenario = 'admin-waiting-approval';

		$this->render('waitingApproval', array(
			'model' => $model
		));
	}




	public function actionImportFromCsv() {


		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		// jscrollpane
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.css');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.js');

		$model = null;
		$step = 'step_import';

		if (isset(Yii::app()->session['importTranscriptsStep'])) {
			$tmpStep = Yii::app()->session['importTranscriptsStep'];
			if (in_array($tmpStep, array('step_import', 'step_process')))
				$step = $tmpStep;
		} else {
			unset(Yii::app()->session['importTranscriptsStep']);
		}

		if ($step == 'step_import') {
			unset(Yii::app()->session['importTranscriptsModel']);
			$model = new TranscriptsImportForm($step);

			if (isset($_POST['TranscriptsImportForm'])) {
				$postData = $_POST['TranscriptsImportForm'];
				$model->attributes = $postData;
				$model->file = CUploadedFile::getInstance($model, 'file');
				if ($model->validate()) {
					$step = 'step_process';
					Yii::app()->session['importTranscriptsStep'] = $step;
					Yii::app()->session['importTranscriptsModel'] = $model->attributes;
				}
			}

		}

		switch ($step) {

			case 'step_process':
				if ($model == null) {
					$model = new TranscriptsImportForm();
					$model->attributes = Yii::app()->session['importTranscriptsModel'];
					$model->setData(Yii::app()->session['importTranscriptsData']);
				}

				if (isset($_POST['import_map'])/* && isset($_POST['TranscriptsImportForm'])*/ && !Yii::app()->request->isAjaxRequest) {
					$model->scenario = 'step_process';
					//$model->attributes = $_POST['TranscriptsImportForm'];
					$model->setImportMap($_POST['import_map']);

					if ($model->save()) {
						unset(Yii::app()->session['importTranscriptsModel']);
						unset(Yii::app()->session['importTranscriptsStep']);
						$this->redirect($this->createUrl('index'));
					} else {
						Yii::app()->session['importTranscriptsModel'] = $model;
						$this->redirect($this->createUrl('index'));
					}
				}

				$items = $model->getData();

				if (empty($items)) {
					$this->redirect($this->createUrl('index'));
				}

				Yii::app()->session['importTranscriptsData'] = $items;

				$columns = array();
				//$i = 1;
				$csvHeaderLine = $items[0];
				$importFields = TranscriptsImportForm::getImportFields();
				foreach ($csvHeaderLine as $key => $value) {
					$column = array(
						'name' => $key,
						'htmlOptions' => array('class' => 'transcripts-column-' . $key),
					);
					//NOTE: at this time we do not know which columns contain dates because the fields import map has not been set yet, so dates cannot be handled/reformatted here
					$columnHeader = CHtml::dropDownList('import_map[' . $key . ']', /*$model->importMap[$i]*/$importFields[$key], TranscriptsImportForm::getImportMapList());
					if ($model->firstRowAsHeader) {
						$columnHeader .=  '<p>' . $value . '</p>';
					}
					$column['header'] = $columnHeader;
					$columns[] = $column;
					//$i++;
				}

				$this->render('csvProcess', array(
					'model' => $model,
					'columns' => $columns
				));
				break;

			case 'step_import':
			default:
				Yii::app()->session['importTranscriptsErrors'] = array();
				Yii::app()->session['importTranscriptsErrorsDetails'] = array();
				$this->render('csvImport', array(
					'model' => $model
				));
				break;
		}
	}

	public function actionAxImportErrorsDetails()
	{
		$this->renderPartial('importErrorsDetails', array(
			'translations' => TranscriptsRecord::model()->attributeLabels(),
			'errors' => Yii::app()->session['importTranscriptsErrorsDetails']
		));
	}



	public function actionCsvSample() {
		$filePath = Yii::getPathOfAlias('TranscriptsFiles').DIRECTORY_SEPARATOR.'sample-transcripts.csv';
		if (file_exists($filePath)) {
			$fileContent = file_get_contents($filePath);
			//convert dates contained in sample file in local date format
			$nowTime = time();
			$date_from = date("Y-m-d", $nowTime - 8*24*3600); //a week and one day ago
			$date_to = date("Y-m-d", $nowTime - 24*3600); //yesterday
			$fileContent = str_replace(
				array('{date_from}', '{date_to}'),
				array(Yii::app()->localtime->toLocalDate($date_from), Yii::app()->localtime->toLocalDate($date_to)),
				$fileContent
			);
			Yii::app()->request->sendFile('sample-transcripts.csv', $fileContent, NULL, false);
		}
		Yii::app()->end();
	}



	public function actionFilterByUserAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$command = Yii::app()->db->createCommand()
			->select("tr.id_user, u.userid")
			->from(TranscriptsRecord::model()->tableName()." tr")
			->join(CoreUser::model()->tableName()." u", "u.idst = tr.id_user")
			->where("u.userid LIKE :match", array(':match' => "%$query%"))
			->order("u.userid ASC");
		if (Yii::app()->user->getIsAdmin()) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = tr.id_user AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}
		switch (Yii::app()->request->getParam('from')) {
			case 'list': {
				$command->andWhere("tr.status = :status", array(
					':status' => TranscriptsRecord::STATUS_APPROVED
				));
			} break;
			case 'waiting': {
				$command->andWhere("tr.status = :status1 OR tr.status = :status2", array(
					':status1' => TranscriptsRecord::STATUS_WAITING_APPROVAL,
					':status2' => TranscriptsRecord::STATUS_REJECTED
				));
			} break;
		}

		$transcripts = $command->queryAll();
		$transcriptsList = array_values(CHtml::listData($transcripts, 'id_user', function($item) {
			return Yii::app()->user->getRelativeUsername($item['userid']);
		}));
		$this->sendJSON(array('options' => $transcriptsList));
	}



	public function actionFilterByTextAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria();
		$criteria->addCondition("
			(course.course_name LIKE :match OR institute.institute_name LIKE :match)
			OR
			(t.course_name LIKE :match OR t.training_institute LIKE :match)
		");
		$criteria->params[':match'] = "%$query%";
		$criteria->with = array('course', 'course.institute');

/*
		if (Yii::app()->user->isPu) {
			$criteria->with['powerUserManagers'] = array(
				'condition' => 'powerUserManagers.idst = :puser_id',
				'params' => array(':puser_id' => Yii::app()->user->id),
				'together' => true,
			);
		}
*/
		switch (Yii::app()->request->getParam('from')) {
			case 'list': {
				$criteria->addCondition("status = :status");
				$criteria->params[':status'] = TranscriptsRecord::STATUS_APPROVED;
			} break;
			case 'waiting': {
				$criteria->addCondition("status = :status1 OR status = :status2");
				$criteria->params[':status1'] = TranscriptsRecord::STATUS_WAITING_APPROVAL;
				$criteria->params[':status2'] = TranscriptsRecord::STATUS_REJECTED;
			} break;
		}

		$transcripts = TranscriptsRecord::model()->findAll($criteria);
		$transcriptsList = array();
		foreach($transcripts as $transcript) {
			$name = '';
			if($transcript->course) {
				$name .= $transcript->course->course_name;

				if($transcript->course->institute && $transcript->course->institute->institute_name != null) {
					$name .= ' ('.$transcript->course->institute->institute_name.')';
				}
			} else if($transcript->course_name) {
				$name .= $transcript->course_name;
				if($transcript->training_institute) {
					$name .= ' ('.$transcript->training_institute.')';
				}
			}
			if($name != '' && !in_array($name, $transcriptsList)) {
				$transcriptsList[] = $name;
			}
		}
		$this->sendJSON(array('options' => $transcriptsList));
	}



	public function actionAssignUserAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$date = new DateTime();

		$command = Yii::app()->db->createCommand()
			->select("u.idst, u.userid, u.firstname, u.lastname")
			->from(CoreUser::model()->tableName()." u")
			->where("u.userid LIKE :match OR CONCAT(u.firstname, ' ', u.lastname) LIKE :match", array(':match' => "%$query%"))
			->andWhere("u.userid <> '/Anonymous'")
			->andWhere("u.valid = 1")
			->andWhere("(u.expiration IS NULL OR u.expiration >= ':today')", array(':today' => $date->format('Y-m-d')))
			->order("u.userid ASC");
		if (Yii::app()->user->getIsAdmin()) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"pu.user_id = u.idst AND pu.puser_id = :puser_id",
				array(':puser_id' => Yii::app()->user->id)
			);
		}

		$reader = $command->query();
		$usersList = array();
		while ($item = $reader->read()) {
			$userName = Yii::app()->user->getRelativeUsername($item['userid']);
			$fullName = $item['firstname'].' '.$item['lastname'];
			$usersList[$userName] = $userName.' ('.$fullName.')';
		}

		$this->sendJSON(array('options' => $usersList));
	}

	/**
	 * Determines the set of field name/values to display in the settings page
	 * @param $transcModel TranscriptsField
	 * @return array
	 */
	public function getAdditionalFieldsToDisplay($transcModel)
	{
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$additionalFields = TranscriptsField::model()->findAll($criteria);
		$transcFields = array();
		foreach ($additionalFields as $field) {
			$fieldClass = 'TranscriptsFieldType' . ucfirst($field->type);
			$transcField = $fieldClass::model()->findByPk($field->id_field);
			$tempValue = Yii::app()->getDb()->createCommand()
					->select('field_' . $field->id_field)
					->from(TranscriptsFieldValue::model()->tableName() . ' v')
					->where('v.id_record=:record', array(':record' => $transcModel->id_record))
					->queryScalar();
			$transcField->transcriptsEntry = $tempValue;
			$transcField->record = $transcModel;
			$transcFields[] = $transcField;
		}
		
		return $transcFields;
	}

	public function actionGridData() {
		$dashletId = Yii::app()->request->getParam('dashletId');

		$dashletModel = DashboardDashlet::model()->findByPk($dashletId);
		$dashletCell = DashboardCell::model()->findByPk($dashletModel->cell);

		$params = json_decode($dashletModel->params);

		$model = TranscriptsRecord::model();
		$model->scenario = 'widgit-approval';

		$dataProvider = $model->dataProvider(array('pageSize' => $params->transcripts_max_items));

		$cs = Yii::app()->clientScript;
		$cs->scriptMap['jquery.yiigridview.js'] = false;

		$this->renderPartial('plugin.TranscriptsApp.widgets.views._dashletGrid', array(
			'dataProvider' => $dataProvider,
			'dashletWidth' => $dashletCell->width,
			'dashletId'    => $dashletId,
			'enableEditRecords' => $params->transcripts_enable_edit_records,
			'paginate' 	   => $params->transcripts_paginate_widget
		), false, true);

		Yii::app()->end();
	}

    public function actionManageCoursesAndInstitutes() {
		$model = TranscriptsCourse::model();
		$model->scenario = 'admin-activities-list';

		$this->render('manageCoursesAndInstitutes/index', array(
            'model' => $model
        ));
    }

    public function actionManageInstitutes() {
		$model = TranscriptsInstitute::model();
		$model->scenario = 'admin-activities-list';

		$this->render('manageCoursesAndInstitutes/manageInstitutes/index', array(
            'model' => $model
        ));
    }

    public function actionEditInstitute() {
		$model = new TranscriptsInstitute();
        $request = Yii::app()->request;

        if($request->getParam('id', FALSE)) {
            $institute_id = (int) $request->getParam('id', FALSE);
            $model_n = TranscriptsInstitute::model()->findByPk($institute_id);
            if(!empty($model_n)) {
                $model = $model_n;
            }
        }
        
        if($request->getParam('TranscriptsInstitute', FALSE)) {
            $post_data = $request->getParam('TranscriptsInstitute');

            $model->institute_name = !empty($post_data['institute_name']) ? $post_data['institute_name'] : $model->institute_name;
            if($model->save()) {
                echo '<a href="" class="auto-close" data-count-waiting="'.TranscriptsRecord::model()->countWaitingForApproval().'"></a><script>$("#transcripts-institute-management-grid").yiiGridView("update");</script>';
                return;
            }
        }

		$this->renderPartial('manageCoursesAndInstitutes/manageInstitutes/_edit', array(
			'model' => $model
		));
    }

    public function actionEditCourse() {
        $model = new TranscriptsCourse();
        $request = Yii::app()->request;

        if($request->getParam('id', FALSE)) {
            $course_id = (int) $request->getParam('id');
            $model_n = TranscriptsCourse::model()->findByPk($course_id);
            if(!empty($model_n)) {
                $model = $model_n;
            }
        }

        if($request->getParam('TranscriptsCourse', FALSE)) {
            $post_data = $request->getParam('TranscriptsCourse');

            $model->course_name = !empty($post_data['course_name']) ? $post_data['course_name'] : $model->course_name;
            $model->type = !empty($post_data['course_type']) ? $post_data['course_type'] : $model->type;
            $model->institute_id = $request->getParam('institute_id', FALSE) ? $request->getParam('institute_id') : $model->institute_id;

            if($model->save()) {
                echo '<a href="" class="auto-close" data-count-waiting="'.TranscriptsRecord::model()->countWaitingForApproval().'"></a><script>$("#transcripts-course-management-grid").yiiGridView("update");</script>';
                return;
            }
        }
        $this->renderPartial('manageCoursesAndInstitutes/_edit', array(
            'model' => $model
        ));
    }

    public function actionDeleteCourse() {
        $request = Yii::app()->request;

        $id = $request->getParam('id', false);

        if ($id) {
            $model = TranscriptsCourse::model()->findByPk($id);

            if ($model) {

                if ($request->getParam('deleteConfirmed', FALSE)) {
					// check if the course have some activities, attached to it. If so, we don't allow deletion!
					$existingActivitiesForCourse = Yii::app()->db->createCommand()
							->select('id_record')->from(TranscriptsRecord::model()->tableName())
							->where('course_id=:id', array(':id' => $model->id))->queryColumn();
					if (empty($existingActivitiesForCourse)) {
						if ($model->delete()) {
							echo '<a class="auto-close"></a><script>$("#transcripts-course-management-grid").yiiGridView("update");</script>';
							return;
						}
					} else {
						Yii::app()->user->setFlash('error', Yii::t('transcripts', 'You cannot delete the course, because it is related with at least one external activity'));
					}
                }

                $this->renderPartial('manageCoursesAndInstitutes/_delete', array(
                    'model' => $model,
                    'lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
                ));
            }
        }
    }

    public function actionDeleteInstitute() {
        $request = Yii::app()->request;

        $id = $request->getParam('id', false);

        if ($id) {
            $model = TranscriptsInstitute::model()->findByPk($id);

            if ($model) {
				if ($request->getParam('deleteConfirmed', FALSE)) {
					// check if the course have some activities, attached to it. If so, we don't allow deletion!
					$existingActivitiesForInstitute = Yii::app()->db->createCommand()
							->select('id_record')->from(TranscriptsRecord::model()->tableName(). ' tr')
							->join(TranscriptsCourse::model()->tableName().' tc', 'tr.course_id=tc.id')
							->join(TranscriptsInstitute::model()->tableName(). ' ti', 'ti.id=tc.institute_id')
							->where('ti.id=:id', array(':id' => $model->id))->queryColumn();
					if (empty($existingActivitiesForInstitute)) {
						if ($model->delete()) {
							echo '<a class="auto-close"></a><script>$("#transcripts-institute-management-grid").yiiGridView("update");</script>';
							return;
						}
					} else {
						Yii::app()->user->setFlash('error', Yii::t('transcripts', 'You cannot delete the institute, because it is related with at least one external activity'));
					}
				}

                $this->renderPartial('manageCoursesAndInstitutes/manageInstitutes/_delete', array(
                    'model' => $model,
                    'lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())
                ));
            }
        }
    }

    public function actionAssignInstitute() {
        $request = Yii::app()->request;

        $course_id = $request->getParam('course_id', FALSE);
        $institute_id = $request->getParam('institute_id', FALSE);

        $model = TranscriptsCourse::model()->findByPk($course_id);

        if($model) {
            $model->institute_id = $institute_id;
            if($model->save()) {
                $result = array('status' => 'success');
            } else {
                $result = array('status' => 'error');
            }
            $this->sendJSON($result);
        }
    }

    public function actionFilterCoursesByTextAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria();
		$criteria->addCondition("(course_name LIKE :match)");
		$criteria->params[':match'] = "%$query%";

		$courses = TranscriptsCourse::model()->findAll($criteria);
		$coursesList = array_values(CHtml::listData($courses, 'id', function($item) {
			return $item->course_name;
		}));
		$this->sendJSON(array('options' => $coursesList));
	}

    public function actionFilterInstitutesByTextAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria();
		$criteria->addCondition("(institute_name LIKE :match)");
		$criteria->params[':match'] = "%$query%";

		$institutes = TranscriptsInstitute::model()->findAll($criteria);
		$institutesList = array_values(CHtml::listData($institutes, 'id', function($item) {
			return $item->institute_name;
		}));
		$this->sendJSON(array('options' => $institutesList));
	}

}
