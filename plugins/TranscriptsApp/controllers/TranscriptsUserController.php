<?php

class TranscriptsUserController extends TranscriptsBaseController {


	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'users' => array('@'), //all logged users can access this section
			'actions' => array(
				'downloadCertificate',
			),
		);

		if (Settings::get('transcripts_allow_users', 'off') == 'on' || (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu())) { //this cannot be accessed if not enabled for users only
			$res[] = array(
				'allow',
				'users' => array('@'), //all logged users can access this section
				'actions' => array(
					'index',
					'createActivity',
					'editActivity',
					'deleteActivity',
					'filterByTextAutocomplete'
				),
			);
		}

		$res[] = array(
			'allow',
			'users' => array('@'),
			'actions' => array(
				'indexPartial'
			),
		);

		$res[] = array(
			'deny',
			'users' => array('*'), // deny all
			'deniedCallback' => array($this, 'adminHomepageRedirect'),
		);

		return $res;
	}




	public function beforeAction($action)
	{
		Yii::app()->tinymce->init();

		switch (lcfirst($action->id)) {
			case 'indexPartial':
			case 'index': {
				$cs = Yii::app()->getClientScript();
				//admin styles
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
				// Bootcamp menu
				$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');
				//Load the datepicker (used in the new/edit actiovity modal)
				Yii::app()->bootstrap->registerAssetCss('bootstrap-datepicker.css');
				Yii::app()->bootstrap->registerAssetJs('bootstrap.datepicker.js');

				//style for custom dropdown (edit/delete)
				$cs->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');

				//transcripts specific styles
				$cs->registerCssFile($this->getModule()->getAssetsUrl().'/css/transcripts.css');
				$cs->registerScriptFile($this->getModule()->getAssetsUrl().'/js/transcripts.js');
			} break;
		}
		return parent::beforeAction($action);
	}



	protected function gridRenderStatus($row, $index) {
		$html = "";
		switch ($row->status) {
			case TranscriptsRecord::STATUS_APPROVED: {
				$admin = (!empty($row->id_admin) ? CoreUser::model()->findByPk($row->id_admin) : false);
				$title = ($admin
					? Yii::t('transcripts', 'Approved by').': <b>'.Yii::app()->user->getRelativeUsername($admin->userid).'</b>'
					: Yii::t('transcripts', 'Approved')
				);
				$html .= CHtml::tag('span', array(
					'class' => 'transcripts-sprite approve-small',
					'title' => $title,
					'rel' => 'tooltip',
					'data-html' => true
				));
			} break;
			case TranscriptsRecord::STATUS_REJECTED: {
				$admin = (!empty($row->id_admin) ? CoreUser::model()->findByPk($row->id_admin) : false);
				$title = ($admin
					? Yii::t('transcripts', 'Rejected by').': <b>'.Yii::app()->user->getRelativeUsername($admin->userid).'</b>'
					: Yii::t('transcripts', 'Rejected')
				);
				/*if (!empty($row->notes)) {
					$title .= '<br />'.$row->notes;
				}*/
				$html .= CHtml::tag('span', array(
					'class' => 'transcripts-sprite reject-small',
					'title' => $title,
					'rel' => 'tooltip',
					'data-html' => true
				));
			} break;
			case TranscriptsRecord::STATUS_WAITING_APPROVAL: {
				$html .= CHtml::tag('span', array(
					'class' => 'transcripts-sprite waiting-small',
					'title' => Yii::t('catalogue', '_WAITING_APPROVAL'),
					'rel' => 'tooltip',
					'data-html' => true
				));
			} break;
		}
		return $html;
	}

	protected function gridRenderLinkEditActivity($row, $index) {
		$adminApproval = (Settings::get('transcripts_admin_approval', 'off') == 'on');
		if ($adminApproval && $row->status == TranscriptsRecord::STATUS_APPROVED) return '&nbsp;'; //user can't modifiy already approved transcripts
		$iconStyle = ($adminApproval && $row->status == TranscriptsRecord::STATUS_REJECTED ? 'transcripts-sprite reload-small' : 'i-sprite is-edit');
		$html = CHtml::link('<span class="'.$iconStyle.'"></span>', $this->createUrl('editActivity', array('id_record' => $row['id_record'])), array(
			'class' => 'open-dialog',
			'data-dialog-class' => 'modal-edit-activity',
			'title' => Yii::t('standard', '_MOD'),
			'closeOnOverlayClick' => 'true',
			'closeOnEscape' => 'true',
			'removeOnClose' => 'true',
			'rel' => 'tooltip',
		));

		return $html;
	}


	protected function gridRenderLinkDeleteActivity($row, $index) {
		if (Settings::get('transcripts_admin_approval', 'off') == 'on' && $row->status == TranscriptsRecord::STATUS_APPROVED) return '&nbsp;'; //user can't delete already approved transcripts
		return parent::gridRenderLinkDeleteActivity($row, $index);
	}



	// actions

	public function actionIndex() {
		$model = TranscriptsRecord::model();
		$model->scenario = 'user-activities-list';
		$model->id_user = Yii::app()->user->getIdSt();

		$this->render('index', array(
			'model' => $model
		));
	}


	/**
	 * AJAX called by  My Activities->External Training to load the tab content
	 *
	 */
	public function actionIndexPartial() {
		$model = TranscriptsRecord::model();
		$model->scenario = 'user-activities-list';
		$idUser = Yii::app()->request->getParam('id_user', false);
		if (!empty($idUser)/* && (Yii::app()->user->isGodAdmin || Yii::app()->user->getIsPu())*/) {
			$model->id_user = $idUser;
		}
		$html = $this->renderPartial('index', array(
			'model' => $model
		), true, true);

		echo $html;
	}



	public function actionCreateActivity($idUser) {

		$model = new TranscriptsRecord();
		list($listOfInstitutes, $coursesPerInstitutes, $isRequireToChooseInstitute, $listCoursesTypes) = TranscriptsRecord::getCourseInstituteFields();

		if (isset($_POST['TranscriptsRecord'])) {
			// Sanitize the input params
			foreach($_POST['TranscriptsRecord'] as $attrKey => $attrValue) {
				$model->$attrKey = Yii::app()->htmlpurifier->purify(trim($attrValue));
			}

			$model->id_user = Yii::app()->request->getParam('id_user', Yii::app()->user->id);
			$model->status = (Settings::get('transcripts_admin_approval', 'off') == 'on'
				? TranscriptsRecord::STATUS_WAITING_APPROVAL
				: TranscriptsRecord::STATUS_APPROVED);

			//check if a certificate file has been uploaded
			if (isset($_FILES['certificate']) && ($_FILES['certificate']['name'] != '')) {
				$model->attachCertificateFile($_FILES['certificate']['tmp_name'], $_FILES['certificate']['name']);
			}
			if (intval($model->training_institute) == $model->training_institute
					&& intval($model->course_name) == $model->course_name
					&& Settings::get('transcripts_require_defined_list') == 'on') {
				$model->training_institute = Yii::app()->db->createCommand()->select('institute_name')
						->from(TranscriptsInstitute::model()->tableName())
						->where('id=:id', array(':id' => $model->training_institute))->queryScalar();

				$course = Yii::app()->db->createCommand()->select()
						->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_name))->queryAll()[0];
				$model->course_name = $course['course_name'];
				$model->course_type = $course['type'];
				$model->course_id = $course['id'];
			}

			$model->scenario = 'elearning';
			if ($model->course_type == LearningCourse::TYPE_CLASSROOM)
				$model->scenario = 'classroom';
			if ($isRequireToChooseInstitute) {
				$model->course_type = null;
				$model->course_name = null;
				$model->training_institute = null;
			}

			if ($model->validate()) {
				if (!$model->save()) { throw new CHttpException(500, 'Error while saving activity data'); }

				if (isset($_POST['certification'])) {

					if (PluginManager::isPluginActive('CertificationApp')) {
						// Save Certification association (retraining)
						CertificationItem::assignItemToCertification(CertificationItem::TYPE_TRANSCRIPT, $model->getPrimaryKey(), intval($_POST['certification']));
					}

					if(Settings::get('transcripts_admin_approval', 'off') == 'off'){
						$userModel = CoreUser::model()->findByPk($idUser);

						// Raise an event upon approval
						$eventParams = array(
								'transcript' 	=> $model,
								'user'			=> $userModel
						);
						Yii::app()->event->raise('ExternalTrainingApproved', new DEvent($this, $eventParams));
					}
				}

				// Update additional fields
				if(isset($_POST['Transcripts']['additional']))
					$model->updateAdditionalFiled($_POST['Transcripts']['additional']);

				// close dialog and reload activities
				echo '<a href="" class="auto-close" data-count-waiting="' . $model->countWaitingForApproval(true) . '"></a>';
				return;
			}
		}

		$selectedCourseId = null;
		$selectedInstituteId = null;
		if ($isRequireToChooseInstitute) {
			if ($model->course_id) {
				$selectedCourseId = $model->course_id;
				$selectedInstituteId = Yii::app()->db->createCommand()
						->select('institute_id')->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_id))->queryScalar();
			}
		}
		if(!$model->course_type)
			$model->course_type = LearningCourse::TYPE_ELEARNING;
		$this->renderPartial('TranscriptsCommonViews._editActivity', array(
			'model' => $model,
			'idUser' => $idUser,
			'formType' => 'user',
			'isRequireToChooseInstitute' => $isRequireToChooseInstitute,
			'listOfInstitutes' => $listOfInstitutes,
			'coursesPerInstitutes' => $coursesPerInstitutes,
			'listCoursesTypes' => $listCoursesTypes,
			'selectedCourseId' => $selectedCourseId,
			'selectedInstituteId' => $selectedInstituteId,
		));
	}


	public function actionEditActivity() {
		$idRecord = intval(Yii::app()->request->getParam('id_record'));
		$model = TranscriptsRecord::model()->findByPk($idRecord);
		if (!$model) { throw new CHttpException(500, 'Invalid activity'); }
		list($listOfInstitutes, $coursesPerInstitutes, $isRequireToChooseInstitute, $listCoursesTypes) = TranscriptsRecord::getCourseInstituteFields();

		if (isset($_POST['TranscriptsRecord'])) {
			// Sanitize the input params
			foreach($_POST['TranscriptsRecord'] as $attrKey => $attrValue) {
				$model->$attrKey = Yii::app()->htmlpurifier->purify(trim($attrValue));
			}

			if (Settings::get('transcripts_admin_approval', 'off') == 'on' && $model->status == TranscriptsRecord::STATUS_REJECTED) {
				//after editing an activity, it has to be re-approved by an admin (if previously rejected)
				$model->status = TranscriptsRecord::STATUS_WAITING_APPROVAL;
				$model->id_admin = null;
			}

			if (intval($model->training_institute) == $model->training_institute
					&& intval($model->course_name) == $model->course_name
					&& Settings::get('transcripts_require_defined_list') == 'on') {
				$model->training_institute = Yii::app()->db->createCommand()->select('institute_name')
						->from(TranscriptsInstitute::model()->tableName())
						->where('id=:id', array(':id' => $model->training_institute))->queryScalar();

				$course = Yii::app()->db->createCommand()->select()
						->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_name))->queryAll()[0];
				$model->course_name = $course['course_name'];
				$model->course_type = $course['type'];
				$model->course_id = $course['id'];
			}

			if(Settings::get('transcripts_require_defined_list') == 'off'){
				// delete course_id value - not needed, we are in free text input state
				$model->course_id = null;
			}

			//check if a certificate file has been uploaded
			if (isset($_FILES['certificate']) && ($_FILES['certificate']['name'] != '')) {
				$model->attachCertificateFile($_FILES['certificate']['tmp_name'], $_FILES['certificate']['name']);
			}

			$model->scenario = 'elearning';
			if ($model->course_type == LearningCourse::TYPE_CLASSROOM)
				$model->scenario = 'classroom';
			if ($isRequireToChooseInstitute) {
				$model->course_type = null;
				$model->course_name = null;
				$model->training_institute = null;
			}

			if($model->validate()) {
				if (!$model->save()) { throw new CHttpException(500, 'Error while saving activity data'); }

				if (isset($_POST['certification']) && PluginManager::isPluginActive('CertificationApp')) {
					// Save Certification association (retraining)
					CertificationItem::assignItemToCertification(CertificationItem::TYPE_TRANSCRIPT, $model->getPrimaryKey(), intval($_POST['certification']));
				}

				// Update additional fields
				if(isset($_POST['Transcripts']['additional']))
					$model->updateAdditionalFiled($_POST['Transcripts']['additional']);

				// close dialog and reload activities
				echo '<a href="" class="auto-close"></a>';
				return;
			}
		}


		$selectedCourseId = null;
		$selectedInstituteId = null;
		if ($isRequireToChooseInstitute) {
			if ($model->course_id) {
				$selectedCourseId = $model->course_id;
				$selectedInstituteId = Yii::app()->db->createCommand()
						->select('institute_id')->from(TranscriptsCourse::model()->tableName())
						->where('id=:id', array(':id' => $model->course_id))->queryScalar();
			}
		}
		if (empty($model->course_type)) {
			$model->course_type = Yii::app()->db->createCommand()
					->select('type')->from(TranscriptsCourse::model()->tableName())
					->where('id=:id', array(':id'=>$model->course_id))->queryScalar();
		}

		$this->renderPartial('TranscriptsCommonViews._editActivity', array(
			'model' => $model,
			'formType' => 'user',
			'listOfInstitutes' => $listOfInstitutes,
			'coursesPerInstitutes' => $coursesPerInstitutes,
			'isRequireToChooseInstitute' => $isRequireToChooseInstitute,
			'listCoursesTypes' => $listCoursesTypes,
			'selectedCourseId' => $selectedCourseId,
			'selectedInstituteId' => $selectedInstituteId,
		));
	}


	public function actionDeleteActivity() {
		$rq = Yii::app()->request;

		//retrieve record instance
		$idRecord = intval($rq->getParam('id_record'));
		$model = TranscriptsRecord::model()->with("course")->findByPk($idRecord);
		if (!$model) { throw new CHttpException(500, 'Invalid activity'); }
		$model->scenario = 'user-delete';

		if (Settings::get('transcripts_admin_approval', 'off') == 'on' && $model->status == TranscriptsRecord::STATUS_APPROVED) {
			throw new CHttpException(500, 'Users cannot edit/delete already modified external activities');
		}

		if ($rq->getParam('confirm', false) !== false) {

			//physically delete record from DB
			if (!$model->delete()) { throw new CHttpException(500, 'Error while deleting external activity'); }

			// close dialog and reload activities
			echo '<a href="" class="auto-close" data-count-waiting="' . $model->countWaitingForApproval(true) . '"></a>';
			return;
		}

		$this->renderPartial('TranscriptsCommonViews._deleteActivity', array(
			'model' => $model,
			'formType' => 'user'
		));
	}


	public function actionFilterByTextAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria();
		$criteria->addCondition("(course_name LIKE :match OR training_institute LIKE :match)");
		$criteria->params[':match'] = "%$query%";
		$criteria->addCondition("id_user = :id_user");
		$criteria->params[':id_user'] = Yii::app()->user->id;

		/*
		switch (Yii::app()->request->getParam('from')) {
			case 'list': {
				$criteria->addCondition("status = :status");
				$criteria->params[':status'] = TranscriptsRecord::STATUS_APPROVED;
			} break;
			case 'waiting': {
				$criteria->addCondition("status = :status1 OR status = :status2");
				$criteria->params[':status1'] = TranscriptsRecord::STATUS_WAITING_APPROVAL;
				$criteria->params[':status2'] = TranscriptsRecord::STATUS_REJECTED;
			} break;
		}
		*/

		$transcripts = TranscriptsRecord::model()->findAll($criteria);
		$transcriptsList = array_values(CHtml::listData($transcripts, 'id_record', function($item) {
			return $item->course_name.(!empty($item->training_institute) ? ' ('.$item->training_institute.')' : '');
		}));
		$this->sendJSON(array('options' => $transcriptsList));
	}



	/**
	 * Takes a file from file storage and sends it to the client [browser], i.e. "Download a file"
	 *
	 * @throws CException
	 */
	public function actionDownloadCertificate() {

		$idRecord = Yii::app()->request->getParam('id_record', false);
		$model = TranscriptsRecord::model()->findByAttributes(array(
				'id_record' => $idRecord,
		));

		// Check model and also if this record belongs to current user OR user is god admin; Just security
		if (!$model || !(($model->id_user != Yii::app()->user->id) || !Yii::app()->user->isGodAdmin || !Yii::app()->user->getIsPu())) {
			Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
			$this->redirect(Docebo::createLmsUrl("site/index"), true);

		}

		// Send file back to user
		$storageManager = CFileStorage::getDomainStorage(TranscriptsRecord::STORAGE_COLLECTION);
		$model->original_filename = Sanitize::fileName($model->original_filename,true);
		$storageManager->sendFile($model->certificate, $model->original_filename);

		Yii::app()->end();

	}

	/**
	 * Determines the set of field name/values to display in the settings page
	 * @param $transcModel TranscriptsField
	 * @return array
	 */
	public function getAdditionalFieldsToDisplay($transcModel) {
		$criteria = new CDbCriteria();
		$criteria->order = 'sequence ASC';
		$additionalFields = TranscriptsField::model()->findAll($criteria);
		$transcFields = array();
		foreach ($additionalFields as $field) {
			$fieldClass = 'TranscriptsFieldType' . ucfirst($field->type);
			$transcField = $fieldClass::model()->findByPk($field->id_field);
			$tempValue = Yii::app()->getDb()->createCommand()
				->select('field_'.$field->id_field)
				->from(TranscriptsFieldValue::model()->tableName().' v')
				->where('v.id_record=:record', array(':record' => $transcModel->id_record))
				->queryScalar();
			$transcField->transcriptsEntry = $tempValue;
			$transcField->record = $transcModel;
			$transcFields[] = $transcField;
		}
		return $transcFields;
	}


}
