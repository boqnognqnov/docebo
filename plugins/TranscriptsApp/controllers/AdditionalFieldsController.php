<?php

class AdditionalFieldsController extends AdminController {

    public function beforeAction($action) {
        parent::beforeAction($action);

        if (!Yii::app()->request->isAjaxRequest) {
            // Initialize JS Translations: list all used categories!!
            JsTrans::addCategories("user_management");
        }

        return true;
    }

    public function filters() {
        return array(
            'accessControl' => 'accessControl',
        );
    }

    /***
     * @return array
     */
    public function accessRules() {
        $res = array();

        $res[] = array(
            'allow',
            'roles' => array(Yii::app()->user->level_godadmin),
        );

        $res[] = array(
            'deny',
            // deny all
            'users' => array('*'),
            'deniedCallback' => array($this, 'adminHomepageRedirect'),
        );

        return $res;
    }


    /**
     *
     */
    public function actionIndex() {
        $cs = Yii::app()->clientScript;

        DatePickerHelper::registerAssets();
        $cs->registerCoreScript('jquery.ui');
        $cs->registerScriptFile($this->getModule()->getAssetsUrl() .'/js/dropdown_custom_field.js', CClientScript::POS_END);

        $coreField = TranscriptsField::model();
        $fieldTypes = $coreField::getTranscriptsFieldsTypes(); // for courses

        $this->render('index', array(
            'coreField' => $coreField,
            'fieldTypes' => $fieldTypes,
        ));
    }

    protected function gridRenderTranslation($data, $index)
    {
        return (empty($data->translation) && !empty($data->enTranslation)) ? $data->enTranslation[0]->translation : $data->translation;
    }

    public function actionFieldsAutocomplete() {

        $result = array();
        $query = strtolower(addcslashes($_REQUEST['data']['query'], '%_'));
        $response = Yii::app()->db->createCommand()
            ->select("IF(t.translation = '', st.translation, t.translation)")
            ->from(TranscriptsFieldTranslation::model()->tableName() . ' t')
            ->leftJoin(TranscriptsFieldTranslation::model()->tableName() . ' st', 'st.id_field = t.id_field AND t.translation = "" AND st.lang_code = "'.Settings::get('default_language').'"')
            ->where(
                array('and' ,
                    't.lang_code = :lang',
                    array('or',
                       array('like', 't.translation', '%' . $query . '%'),
                       array('like', 'st.translation', '%' . $query . '%'))
                ),
                array(':lang' => Lang::getCodeByBrowserCode(Yii::app()->getLanguage())))
            ->queryColumn();

        $this->sendJSON(array('options' => $response));
    }

    public function actionDeleteField( $id ) {

        $confirmYes = Yii::app()->request->getParam('confirm_yes', null);
        $field = TranscriptsField::model()->findByPk( $id );
        if ( empty( $field)) {
            $this->sendJSON( array());
        }
        $field->getTranslation();
        if ( $confirmYes ) {
            $field->delete();

            echo '<a class="auto-close"></a>';

        }
        if ( Yii::app()->request->isAjaxRequest && $confirmYes != 'yes') {
            $this->renderPartial('deleteModal', array(
                'model' => $field,
            ),false);
            Yii::app()->end();
        }
        Yii::app()->end();
    }

    public function actionCreateFieldModal($type = 'default') {
        $fieldTranslation   = new TranscriptsFieldTranslation();
        $fieldTranslation->lang_code = Settings::get('default_language', 'english');
        $field = new TranscriptsField();

        if (!empty($_REQUEST['type']))
            $type = $_REQUEST['type'];

        if ( !empty($_POST['TranscriptsField']) && !empty($_POST['TranscriptsFieldTranslation']['translation'][$fieldTranslation->lang_code]) ) {
            $enteredLanguages = $_POST['TranscriptsFieldTranslation']['translation'];

            $baseField = new TranscriptsField();
            $baseField->setFieldTranslations(array_filter($enteredLanguages));
            $baseField->type                = $_POST['TranscriptsField']['type'];
            $baseField->sequence            = TranscriptsField::model()->getNextSequence();
            $baseField->settings            = CJSON::encode(is_array($_POST['TranscriptsField']['settings']) ? $_POST['TranscriptsField']['settings'] : array());

            $success = false;
            if ( $baseField->validate() ) {

                $transaction = Yii::app()->db->beginTransaction();

                if (empty($_POST['TranscriptsFieldTranslation']['translation'])) {
                    $success = true;
                } else {

                    $filteredTranslations = array_filter($enteredLanguages);
                    $newField = new TranscriptsField();
                    $newField->setFieldTranslations($filteredTranslations);
                    $newField->type = $baseField->type;
                    $newField->sequence = $baseField->sequence;
                    $newField->settings = $baseField->settings;
                    $success = $newField->save();

                    if ($success) {
                        $transaction->commit();
                    } else {
                        $transaction->rollback();
                    }
                }
            }
            $this->sendJSON(array(
                'success' => $success,
            ));
        }

        $field->type = $type;
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
		$lang_code = Settings::get('default_language', 'english');
		$activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelDropdown = new TranscriptsFieldDropdown();
        $modelDropdownTranslation = new TranscriptsFieldDropdownTranslations();

        $html = $this->renderPartial('createModal',
            array(
                'modelField'                => $field,
                'modelTranslation'          => $fieldTranslation,
                'modelDropdown'             => $modelDropdown,
                'modelDropdownTranslation'  => $modelDropdownTranslation,
                'activeLanguagesList'       => $activeLanguagesList
            ), true);

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionEditField( $id )
    {
        $lang_code = Settings::get('default_language', 'english');
        $activeLanguagesList = CoreLangLanguage::getActiveLanguages();
        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelTranslation   = new TranscriptsFieldTranslation();
        $modelTranslation->lang_code = Settings::get('default_language', 'english');

        if ( $id ) {
            $fieldTranslations = TranscriptsFieldTranslation::getFieldTranslations( $id );
            $baseField = TranscriptsField::model()->findByPk( $id );
            if ( isset($_REQUEST['TranscriptsField']['settings']) )
            {
                $baseField->settings            = CJSON::encode(is_array($_POST['TranscriptsField']['settings']) ? $_POST['TranscriptsField']['settings'] : array());
                $baseField->save();
            }
        }

        if ( !empty($_POST['TranscriptsFieldTranslation']) && !empty($_POST['TranscriptsField'])) {
            $platformDefaultLang = Settings::get('default_language');
            if (empty($_POST['TranscriptsFieldTranslation']['translation'][$platformDefaultLang])) {

                $fieldTranslations->addError('translation', Yii::t('user_management', '_ERROR_ADDITIONAL_FIELDS_ENGLISH_REQUIRED'));

            } else {
                $success = false;

                $transaction = Yii::app()->db->beginTransaction();

                foreach ( $activeLanguagesList as $k => $l) {
                    $translationModel = TranscriptsFieldTranslation::model()->findByAttributes(array(
                        'id_field'  => $id,
                        'lang_code' => $k
                    ));

                    if ( empty($translationModel) ) {
                        $translationModel = TranscriptsFieldTranslation::model();
                        $translationModel->lang_code = $k;
                    }

                    $translationModel->translation = $_POST['TranscriptsFieldTranslation']['translation'][$k];
                    $success = $translationModel->save();
                    if (!$success) {
                        Yii::log($translationModel->getErrors(), CLogger::LEVEL_ERROR);
                        $result['success'] = false;
                        break;
                    }
                }

                if ($success) {
                    $transaction->commit();
                } else {
                    $transaction->rollback();
                }

                $this->sendJSON(array(
                    'success' => $success,
                ));
            }
        }

        $html = $this->renderPartial('editModal',
            array(
                'modelField'            => $baseField,
                'activeLanguagesList'   => $activeLanguagesList,
                'translationsList'      => $fieldTranslations,
                'modelTranslation'      => $modelTranslation
            ), true );

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionCreateDropdown() {
        $transaction = Yii::app()->db->beginTransaction();
        $success = false;
        $optionsJsonData = Yii::app()->request->getParam('dropdownElemsJson', false);
        $filteredTranslations = array();

        if ( !empty($_POST['TranscriptsFieldTranslation']) && !empty($_POST['TranscriptsFieldTranslation']['translation'][CoreUser::getDefaultLangCode()])) {
            $translationList = $_POST['TranscriptsFieldTranslation']['translation'];
            $fieldTranslations = array();

            foreach ($translationList as $langCode => $translation) {
                if ( $translation['field'])
                    $fieldTranslations[$langCode] = $translation['field'];
            }
            $filteredTranslations = array_filter($fieldTranslations);
        }

        if (!$optionsJsonData) {
            $success = false;
        }

        $optionsData = CJSON::decode($optionsJsonData);

        $optionsTranslations = $optionsData['CoreField']['translation'];

        $baseField = new TranscriptsField();
        $baseField->type = TranscriptsField::TYPE_FIELD_DROPDOWN;
        $baseField->settings            = CJSON::encode(is_array($_POST['TranscriptsField']['settings']) ? $_POST['TranscriptsField']['settings'] : array());
        $baseField->sequence = TranscriptsField::model()->getNextSequence();

        if ( $filteredTranslations ) $baseField->setFieldTranslations($filteredTranslations);

        if ($baseField->save()) {

            $idBaseField = $baseField->id_field;
            /*
             * If the user when creating fill translations for more then one language and the the option's count is different
             * For instance The user enter for English 5 options and for Italian 3 options
             * We have the following input:
             * Translations:
             *    english => [ 'en1', 'en2', 'en3', 'en4', 'en5']
             *    italian => [ 'it1', 'it2', 'it3' ]
             *  Now we need to find the max options in order to get the count of the fields that we must create
             */
            $success = true;
            $activeLanguagesList = CoreLangLanguage::getActiveLanguages();
            $maxOptions = 0;
            foreach($optionsTranslations as $inputLanguage) {
                // Get the count of the options entered from the user for this language
                $curLangOptions = count(array_filter($inputLanguage['options']['translation']));
                // If this language options has more options than the previous one, then the current will become the MAX
                if($maxOptions <= $curLangOptions) {
                    $maxOptions = $curLangOptions;
                }
            }
            // Now in the $maxOptions we have the count of the fields that we must create
            for ($i = 0; $i < $maxOptions; $i++) {
                $optionIndex = $i + 1;
                $tmpOptionModel = new TranscriptsFieldDropdown();
                $tmpOptionModel->id_field = $idBaseField;
                $tmpOptionModel->sequence = $optionIndex;
                if ($tmpOptionModel->save() ) {
                    // Now for this field for each active language we must create translation
                    foreach  ( $activeLanguagesList as $lang_code => $lang ) {
                        // If the field is successfully saved now we must save the options translation
                        $translationModel = new TranscriptsFieldDropdownTranslations();
                        $translationModel->id_option = $tmpOptionModel->id_option;
                        $translationModel->lang_code = $lang_code;
                        $text = isset($optionsTranslations[$lang_code]['options']['translation'][$i]) ? $optionsTranslations[$lang_code]['options']['translation'][$i] : '';
                        $translationModel->translation = $text;
                        if (!$translationModel->save()) {
                            $success = false;
                            break;
                        }
                    }
                } else {
                    break;
                    $success = false;
                }
            }

        } else {
            $success = false;
        }

        // Send the response back
        if ($success) {
            $transaction->commit();
        } else {
            $transaction->rollback();
        }

        $this->sendJSON(array(
            'success' => $success,
        ));

    }


    public function actionEditDropdown($id) {

        $success = false;
        $optionsJsonData        = Yii::app()->request->getParam('dropdownElemsJson', false);
        $lang_code              = Settings::get('default_language', 'english');
        $activeLanguagesList    = CoreLangLanguage::getActiveLanguages();
        $activeLanguagesList[$lang_code] = $activeLanguagesList[$lang_code] . " (" . Yii::t('standard', '_DEFAULT_LANGUAGE') . ")";
        $modelTranslation       = new TranscriptsFieldTranslation();
        $modelTranslation->lang_code = Settings::get('default_language', 'english');
        $fieldTranslations      = TranscriptsFieldTranslation::getFieldTranslations( $id );

        if ($id)
            $baseField = TranscriptsField::model()->findByPk($id);
        if ( isset($_POST['TranscriptsField']) ) {
            $baseField->settings            = CJSON::encode(is_array($_POST['TranscriptsField']['settings']) ? $_POST['TranscriptsField']['settings'] : array());
            $baseField->save();
        }

        if (!$optionsJsonData) {
            $success = false;
        } else {
            $optionsData = CJSON::decode($optionsJsonData);
            $optionsTranslations = $optionsData['CoreField']['translation'];
            $transaction = Yii::app()->db->beginTransaction();
            $options = array();
            try {
                foreach  ( $activeLanguagesList as $lang_code => $lang ) {
                    $translationArray = $optionsTranslations[$lang_code]['options']['translation'];
                    // Select options translation
                    foreach ($translationArray as $key => $translation) {

                        $idOption = $optionsTranslations[$lang_code]['options']['idSon'][$key];

                        // If the options is null and the translation is empty then this is empty field and we can skipp it
                        if($idOption === null && $translation === "") continue;

                        $object = TranscriptsFieldDropdownTranslations::model()->findByAttributes(array('id_option' => $idOption, 'lang_code' => $lang_code));
                        if ( $object instanceof TranscriptsFieldDropdownTranslations ) {
                            $object->translation = $translation;
                            $object->save();
                        } else {
                            // If there is no translations in DB for this option and the translation is empty then we go to the next one
                            if ($translation === "") continue;

                            // We must create new OPTION
                            $sequence = Yii::app()->db->createCommand()
                                ->select("sequence")
                                ->from(TranscriptsFieldDropdown::model()->tableName())
                                ->where('id_field = :field')
                                ->order('sequence DESC')
                                ->limit("1")
                                ->queryScalar(array(":field" => $id));

                            $newOption = new TranscriptsFieldDropdown();
                            $newOption->id_field = $id;
                            $newOption->sequence = $sequence !== false ? ($sequence + 1) : 0;
                            $newOption->save();
                            // We just created a option, let's add all the fields for it
                            foreach ($activeLanguagesList as $nested_lang_code => $nested_lang){
                                $newTranslation = $nested_lang_code == $lang_code ? $translation : "";
                                $object = new TranscriptsFieldDropdownTranslations();
                                $object->translation = $newTranslation;
                                $object->lang_code = $nested_lang_code;
                                $object->id_option = $newOption->id_option;
                                $object->save();
                            }
                        }
                        // Collect all the changes we made
                        $options[$idOption][$lang_code] = $translation;
                    }

                    // Handle the translation of the current field
                    $translationModel = TranscriptsFieldTranslation::model()->findByAttributes(array('id_field' => $id, 'lang_code' => $lang_code));
                    if ( empty($translationModel) ) {
                        $translationModel = TranscriptsFieldTranslation::model();
                        $translationModel->lang_code = $lang_code;
                    }
                    // If the current translation is the same as the last one no need of update
                    if($translationModel->translation == $_POST['TranscriptsFieldTranslation']['translation'][$lang_code]['field']) continue;
                    // I they are different then let's update the current filed translation title
                    $translationModel->translation = $_POST['TranscriptsFieldTranslation']['translation'][$lang_code]['field'];
                    $success = $translationModel->save();
                    if (!$success) {
                        Yii::log($translationModel->getErrors(), CLogger::LEVEL_ERROR);
                        $result['success'] = false;
                        break;
                    }
                }

                // DELETE OPTIONS WITH EMPTY TRANSLATIONS
                // Extract all the options that have empty translations for all the languages
                $emptyTranslations = Yii::app()->db->createCommand()
                    ->select("id_option")
                    ->from(TranscriptsFieldDropdownTranslations::model()->tableName())
                    ->group("id_option")
                    ->having("COUNT( IF( translation = '', NULL, translation) ) = 0")
                    ->queryColumn();
                // If there are such fields let's delete the options then
                if(!empty($emptyTranslations)) {
                    // If we are going to delete something let's do it
                    Yii::app()->db->createCommand()->delete(
                        TranscriptsFieldDropdown::model()->tableName(),
                        array('in', 'id_option', $emptyTranslations)
                    );
                    // Well since we deleted a record let's the sequence column
                    $command = "SET @i = 0;
                        UPDATE `transcripts_field_dropdown`
                           SET `sequence` = @i:=@i+1
                           WHERE id_field = $id
                         ORDER BY `sequence` ASC;";
                    // Execute the command
                    Yii::app()->db->createCommand($command)->query();
                }

                $transaction->commit();
            }
            catch(Exception $e)
            {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
        }

        $html = $this->renderPartial('editModal', array(
            'modelField'            => $baseField,
            'activeLanguagesList'   => $activeLanguagesList,
            'translationsList'      => $fieldTranslations,
            'modelTranslation'      => $modelTranslation
        ), true);

        $this->sendJSON(array(
            'html' => $html,
        ));
    }

    public function actionOrderFields() {
        $ids = $_POST['ids'];
        $command = Yii::app()->db->createCommand();

        if (!empty($ids)) {
            foreach ($ids as $weight => $id)
                $command->update(TranscriptsField::model()->tableName(), array('sequence' => $weight), 'id_field = :id', array(':id' => $id));

            $this->sendJSON(array('success' => true));
        } else {
            $this->sendJSON(array('success' => false));
        }
    }

}