<?php

class TranscriptsFieldTypeDate extends TranscriptsField
{

    //date width to be used
    protected $_dateWidth = LocalTime::DEFAULT_DATE_WIDTH;//LocalTime::MEDIUM;

    public function render(){
        if($this->translation == ''){
            $tmp = TranscriptsField::model()->findByPk($this->id_field);
            $this->translation = $tmp->getTranslation();
            unset($tmp);
        }

        $fieldId = 'date_' . $this->id_field;
        $date = $this->transcriptsEntry ? Yii::app()->localtime->toLocalDate($this->transcriptsEntry) : null;
        $html = '';
        $html .= CHtml::hiddenField('Transcripts[additional]['.$this->id_field.'][type]', $this->type);
        $html .= CHtml::textField('Transcripts[additional]['.$this->id_field.'][value]', $date, array("class"=>"datepicker", "id" => "$fieldId"));
        $html .= '<i class="p-sprite calendar-black-small date-icon"></i>';
        $html .= '<script>
            var d = $("#'.$fieldId.'").bdatepicker({format: "'.  Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')) .'", language: "'. Yii::app()->getLanguage() .'"}).data("datepicker");

        </script>';


        return $html;
    }

    public function getDateWidth()
    {
        //check date width for input fields
        if (!Yii::app()->localtime->forceLongYears()) {
            //trying to prevent short year format to be used in input fields
            switch ($this->_dateWidth) {
                case LocalTime::LONG:
                    $dateWidth = LocalTime::LONG;
                    break;
                default:
                    $dateWidth = LocalTime::MEDIUM;
                    break;
            }
        } else {
            //long years are forced, all date widths are safe
            $dateWidth = $this->_dateWidth;
        }
        return $dateWidth;
    }

    public function setDateWidth($dateWidth)
    {
        switch ($dateWidth) {
            case LocalTime::SHORT:
            case LocalTime::MEDIUM:
            case LocalTime::LONG:
                $this->_dateWidth = $dateWidth;
                break;
            default:
                $this->_dateWidth = LocalTime::DEFAULT_DATE_WIDTH;
                break;
        }
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function validateFieldEntry() {
        parent::validateFieldEntry();
    }

    public static function renderFieldValue($transcEntry, $transcEntryModel = null, $wherefrom = null) {
        if(!empty($transcEntry))
            $transcEntry = Yii::app()->localtime->toLocalDate($transcEntry);

        return CHtml::encode($transcEntry);
    }

    public function renderFilterField() {
        Yii::app()->controller->renderPartial('TranscriptsFieldsViews._date',
            array(
                'label'                 => $this->getTranslation(),
                'id_field'              => $this->id_field,
                'conditionsOptions'     => $this->getConditionsOptions(),
                'type'                  => $this->type
            ),
            false,
            false
        );
    }
}