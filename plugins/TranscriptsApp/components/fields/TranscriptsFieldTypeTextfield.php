<?php

class TranscriptsFieldTypeTextfield extends TranscriptsField {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function render() {
        $htmlOptions = array( 'class' => 'span8 input-block-level long-input' );
        $transcEntry = $this->transcriptsEntry;
        if($this->translation == '')
        {
            $tmp = TranscriptsField::model()->findByPk($this->id_field);
            $this->translation = $tmp->getTranslation();
            unset($tmp);
        }

        $html = CHtml::textfield('Transcripts[additional]['.$this->id_field.'][value]', $transcEntry, $htmlOptions);
        $html .= CHtml::hiddenField('Transcripts[additional]['.$this->id_field.'][type]', $this->type);
        return $html;
    }

    public function renderFilterField() {
        Yii::app()->controller->renderPartial('TranscriptsFieldsViews._textfield',
            array(
                'label'                 => $this->getTranslation(),
                'id_field'              => $this->id_field,
                'conditionsOptions'     => $this->getConditionsOptions(),
                'type'                  => $this->type
            ),
            false,
            false
        );
    }

    public function validateFieldEntry() {
        parent::validateFieldEntry();
    }

    public static function renderFieldValue($transcEntry, $transcEntryModel=null, $whereFrom = null) {
        return CHtml::encode($transcEntry);
    }

}