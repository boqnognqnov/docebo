<?php

class TranscriptsFieldTypeDropdown extends TranscriptsField
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function render(){
        $htmlOptions = array();
        $transcEntry = $this->transcriptsEntry;


        $html =  CHtml::dropDownList('Transcripts[additional]['.$this->id_field.'][value]', $transcEntry, $this->getOptions(), array('prompt' => Yii::t('standard', '_SELECT')));
        $html .= CHtml::hiddenField('Transcripts[additional]['.$this->id_field.'][type]', $this->type);

        return $html;
    }

    public function getOptions()
    {
        $fieldSons = TranscriptsFieldDropdown::model()->findAllByAttributes(array('id_field' => $this->id_field,),array('order' => 'sequence ASC'));
        if (empty($this->lang_code)) {
            $this->lang_code = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());
        }
        $localizedOptions = array();
        foreach ($fieldSons as $fieldSon) {
            $optionModel = TranscriptsFieldDropdownTranslations::model()->findByAttributes(array('id_option' => $fieldSon->id_option,'lang_code' => $this->lang_code));
            if ($optionModel->translation)
            	$localizedOptions[$fieldSon->id_option] = $optionModel->translation;
        }

        $defaultOptions = array();
        foreach ($fieldSons as $fieldSon) {
			$langCode = Settings::get('default_language', 'english');
			$optionModel = TranscriptsFieldDropdownTranslations::model()->findByAttributes(array('id_option' => $fieldSon->id_option,'lang_code' => $langCode));
			$defaultOptions[$fieldSon->id_option] = $optionModel->translation;
		}
		$displayOptions = count($localizedOptions) > 0 ? $localizedOptions : $defaultOptions;

		asort($displayOptions);
        return $displayOptions;
    }

    public function validateFieldEntry() {
        parent::validateFieldEntry();
    }

    public static function renderFieldValue($transcEntry, $transcEntryModel=null, $whereFrom = null) {

        $currentLanguage = Lang::getCodeByBrowserCode(Yii::app()->getLanguage());

        $fieldSon = TranscriptsFieldDropdownTranslations::model()->findByAttributes(array(
            'id_option' 	=> $transcEntry,
            'lang_code' 	=> $currentLanguage
        ));

        $lmsDefaultLanguage = Settings::get('default_language', 'english');

        if (!empty($fieldSon->translation)) {
            return CHtml::encode($fieldSon->translation);
        } else {
            $fieldSon = TranscriptsFieldDropdownTranslations::model()->findByAttributes(array(
                'id_option' 	=> $transcEntry,
                'lang_code' 	=> $lmsDefaultLanguage
            ));

            if (!empty($fieldSon->translation)) {
                return CHtml::encode($fieldSon->translation);
            }
        }

        return "";
    }

    public function renderFilterField() {
        Yii::app()->controller->renderPartial('TranscriptsFieldsViews._dropdown',
            array(
                'label'     => $this->getTranslation(),
                'id_field'  => $this->id_field,
                'options'   => $this->getOptions(),
                'type'      => $this->type
            ),
            false,
            false
        );
    }

}