<?php
class TranscriptsFieldTypeTextarea extends TranscriptsField {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function render() {
        $htmlOptions = array();
        $transcEntry = $this->transcriptsEntry;
        if($this->translation == '')
        {
            $tmp = TranscriptsField::model()->findByPk($this->id_field);
            $this->translation = $tmp->getTranslation();
            unset($tmp);
        }

        Yii::app()->controller->renderPartial('TranscriptsFieldsViews._textarea_fill', array(
            'model' => $this,
            'transcEntry' => $transcEntry
        ),
            false,
            false
        );
    }

    public function renderFilterField() {
        Yii::app()->controller->renderPartial('TranscriptsFieldsViews._textarea',
            array(
                'label'                 => $this->getTranslation(),
                'id_field'              => $this->id_field,
                'conditionsOptions'     => $this->getTextareaConditionsOptions(),
                'type'                  => $this->type
            ),
            false,
            false
        );
    }

    public function validateFieldEntry() {
        parent::validateFieldEntry();
    }

    public static function renderFieldValue($transcEntry, $transcEntryModel=null, $whereFrom = null) {
        return $transcEntry;
    }

}