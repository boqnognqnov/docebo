<div class="row-fluid row-course-field row-field-<?=$id_field ?>" id="row-field-<?=$id_field ?>" data-filter-value="<?=$id_field ?>">
    <div class="span3 span3-label">
        <?=CHtml::label($label, 'advancedSearch[additional]['.$id_field.']');  ?>
    </div>
    <div class="span9 dropdown-span9">
        <div class="row-fluid  course-field-dropdown-value">
            <div class="span11 dropdown-span11">
                <div class="row-fluid">
                    <div class="span4 course-field-select-condition-container">
                        <?=CHtml::dropDownlist(
                            'advancedSearch[additional]['.$id_field.'][condition]',
                            (!empty($_REQUEST['advancedSearch']['additional'][$id_field]['condition'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['condition']) : '',
                            $conditionsOptions,
                            array('style' => 'width: 100%', 'class' => 'course-field-select-condition')
                        );  ?>
                    </div>

                    <div class="span8 course-field-textfield-span8">
                        <?=CHtml::textfield(
                            'advancedSearch[additional]['.$id_field.'][value]',
                            (!empty($_REQUEST['advancedSearch']['additional'][$id_field]['value'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['value']) : '',
                            array('class' => 'course-field-textfield-input')
                        );  ?>

                        <?=CHtml::hiddenField('advancedSearch[additional][' . $id_field . '][type]', $type) ?>
                    </div>
                </div>
            </div>
            <div class="span1 course-field-textfield-input-remove">
                <a class="remove-advanced-filter-link" data-filter-value="<?=$id_field ?>" data-filter-text="<?=$label ?>">X</a>
            </div>
        </div>
    </div>
</div>
