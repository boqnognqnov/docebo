<div class="row-fluid row-course-field row-field-<?=$id_field ?>" id="row-field-<?=$id_field ?>" data-filter-value="<?=$id_field ?>">
    <div class="span3 span3-label">
        <?=CHtml::label($label, 'advancedSearch[additional][' . $id_field . ']'); ?>
    </div>

    <div class="span9 dropdown-span9">
        <div class="row-fluid  course-field-dropdown-value">
            <div class="span11 dropdown-span11">
                <div class="row-fluid">
                    <div class="span4 course-field-select-condition-container">
                        <?=CHtml::dropDownlist(
                            'advancedSearch[additional][' . $id_field . '][condition]',
                            (!empty($_REQUEST['advancedSearch']['additional'][$id_field]['condition'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['condition']) : '',
                            array('>' => '>', '<' => '<', '=' => '=', '>=' => '>=', '<=' => '<=', '<>' => '!='),
                            array('style' => 'width: 100%')
                        ); ?>
                    </div>
                    <div class="span8 course-date-field-filter-container">
                        <?php
                        $this->widget('common.widgets.DatePicker', array(
                            'id' => 'advancedSearch[additional]['.$id_field.'][value]',
                            //'label' => '',
                            'fieldName' => 'advancedSearch[additional][' . $id_field . '][value]',//CHtml::activeName(LearningCourseFieldValue::model(), 'field_' . $id_field),
                            'value' => (!empty($_REQUEST['advancedSearch']['additional'][$id_field]['value'])) ? ($_REQUEST['advancedSearch']['additional'][$id_field]['value']) : '',
                            'scriptPosition' => CClientScript::POS_HEAD,
                            'htmlOptions' => array(
                                'id' => CHtml::activeId(TranscriptsFieldValue::model(), 'field_' . $id_field . '_' . uniqid()), // fixed issue with multi-instances
                                'class' => 'datepicker course-field-datepicker',
                                'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
                                'data-date-language' => Yii::app()->getLanguage(),
                                //'style' => 'padding-left: 0; padding-right: 0;'
                            )
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="span1 course-date-field-filter-remove-container">
                <a class="remove-advanced-filter-link" data-filter-value="<?=$id_field ?>" data-filter-text="<?=$label ?>">X</a>
            </div>
        </div>
    </div>

    <?=CHtml::hiddenField('advancedSearch[additional][' . $id_field . '][type]', $type) ?>
</div>
