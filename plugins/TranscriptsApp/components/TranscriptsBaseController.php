<?php

/**
 * Base controller with common functions for controllers used by transcripts
 */
class TranscriptsBaseController extends AdminController {


	public function __construct($id, $module=null) {
		parent::__construct($id, $module);
		Yii::setPathOfAlias('TranscriptsRoot', dirname(__FILE__).'/../');
		Yii::setPathOfAlias('TranscriptsFiles', dirname(__FILE__).'/../files/');
		Yii::setPathOfAlias('TranscriptsCommonViews', dirname(__FILE__).'/../views/_common/');
		Yii::setPathOfAlias('TranscriptsFieldsViews', dirname(__FILE__).'/../components/fields/views/');
	}

	//some common functions for user interface and admin management interface


	//common gridviews renderers

	protected function gridRenderUserid($row, $index) {
		return Yii::app()->user->getRelativeUsername($row->user->userid);
	}

	protected function gridRenderCourseType($row, $index) {
		return LearningCourse::model()->getCourseTypeTranslation($row->course_type);
	}

	protected function gridRenderDate($row, $index) {
		if ($row->course_type == LearningCourse::TYPE_CLASSROOM || ($row->course && $row->course->type ==  LearningCourse::TYPE_CLASSROOM )){
			return $row->from_date.' / '.$row->to_date;
		}
		return $row->to_date;
	}

	protected function gridRenderScore($row, $index) {
		return (int)$row->score.'/'.$row->getMaxScore();
	}

	protected function gridRenderCredits($row, $index) {
		return Yii::app()->format->formatNumber($row->credits);
	}

	public function gridRenderCertificate($row, $index) {
		if (empty($row->certificate)) { return '-'; }
		$fileUrl = $row->getCertificateDownloadUrl();
		$isGrey = (Settings::get('transcripts_admin_approval', 'off') == 'on' && $this->id == 'transcriptsUser' && $row->status == TranscriptsRecord::STATUS_REJECTED);
		return CHtml::link('<span class="i-sprite is-cert'.($isGrey ? ' grey' : '').'"></span>', $fileUrl, array(
			'title' => Yii::t('player', 'Certificate'),
			'rel' => 'tooltip',
			'target' => '_blank'
		));
	}

	protected function gridRenderLinkEditActivity($row, $index) {
		$html = CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('editActivity', array('id_record' => $row->id_record)), array(
			'class' => 'edit-action open-dialog',
			'data-dialog-class' => 'modal-edit-activity',
			'title' => Yii::t('standard', '_MOD'),
			'removeOnClose' => 'true',
			'closeOnOverlayClick' => 'true',
			'closeOnEscape' => 'true',
			'rel' => 'tooltip',
		));
		return $html;
	}

	protected function gridRenderLinkDeleteActivity($row, $index) {
		$html = CHtml::link('<span class="i-sprite is-remove red"></span>', $this->createUrl('deleteActivity', array('id_record' => $row->id_record)), array(
			'class' => 'delete-action open-dialog',
			'data-dialog-class' => 'modal-delete-activity',
			'title' => Yii::t('standard', '_DEL'),
			'removeOnClose' => 'true',
			'closeOnOverlayClick' => 'true',
			'closeOnEscape' => 'true',
			'rel' => 'tooltip',
		));
		return $html;
	}


	//other common methods

	public function getLocalDateFormat() {
		return Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short'));
	}
}
