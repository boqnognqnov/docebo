<?php

class TranscriptsAppModule extends CWebModule {

	/**
	 * @var string Assets url for this module
	 */
	private $_assetsUrl;


	public function init() {
		// import the module-level models and components
		$this->setImport(array(
			'TranscriptsApp.models.*',
			'TranscriptsApp.components.*',
			'TranscriptsApp.components.fields.*',
			'TranscriptsApp.widgets.*',
		));

		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'installTranscriptsDashlet'));
	}


    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            /**
             * Check if the user is trying to access this module's endpoint trough the LMS route
             * and if so redirect him to use the ADMIN route, because we need JS and CSS that are already loaded there
             */
            if (strpos(Yii::app()->request->requestUri,'/lms/index.php') === 0) {
                $path = array($controller->module->id, $controller->id, $action->id);
                Yii::app()->controller->redirect(Docebo::createAdminUrl(implode('/', $path)));
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('TranscriptsApp.assets'));
		}
		return $this->_assetsUrl;
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {

		Yii::app()->mainmenu->addAppMenu('transcripts', array(
			'icon' => 'home-ico transcripts',
			'app_icon' => 'fa-files-o', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//TranscriptsApp/TranscriptsManagement/activitiesList'),
			'label' => Yii::t('transcripts', 'External activities'),
			'settings' => Docebo::createLmsUrl('//TranscriptsApp/TranscriptsApp/settings'),
			'permission' => '/framework/admin/transcripts/view'
		), array());

	}


	public function userMenu() {
		$output = array();
		/*
		 * User Menu->Extrenal training is moved inside "My Activities"
		if (Settings::get('transcripts_allow_users', 'off') == 'on') {
			//load css needed for menu voice icon rendering
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile($this->getAssetsUrl() . '/css/usermenu.css');
			//set menu voice properties
			$output[] = array(
				'href' => Docebo::createAppUrl('lms:TranscriptsApp/TranscriptsUser/index', array()),
				'icon' => 'transcripts',
				'label' => Yii::t('transcripts', 'External activities'),
			);
		}
		*/
		return $output;
	}

	/**
	 * @param DEvent $event
	 */
	public function installTranscriptsDashlet(DEvent $event){
		if (!isset($event->params['descriptors']))
			return;

		$dashletTranscriptsDescriptor = DashletTranscripts::descriptor();

		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor)
				$collectedSoFarHandlers[] = $descriptorObj->handler;
		}

		if(!in_array($dashletTranscriptsDescriptor->handler, $collectedSoFarHandlers)){
			$event->params['descriptors'][] = $dashletTranscriptsDescriptor;
		}

	}
}