<?php

/**
 * LinkedinAppSettingsForm class.
 *
 * @property string $linkedin_access
 * @property string $linkedin_secret
 */
class LinkedinAppSettingsForm extends CFormModel {
	public $linkedin_access;
	public $linkedin_secret;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('linkedin_access, linkedin_secret', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'linkedin_access' => Yii::t('configuration', '_SOCIAL_LINKEDIN_ACCESS'),
			'linkedin_secret' => Yii::t('configuration', '_SOCIAL_LINKEDIN_SECRET'),
		);
	}
}
