<?php
/* @var $form CActiveForm */
/* @var $settings LinkedinAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-docebo-per-linkedin/' : 'https://www.docebo.com/knowledge-base/docebo-for-linkedin/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('standard','Settings') ?>: <?= Yii::t('app', 'linkedin') ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="http://developer.linkedin.com/" target="_blank">http://developer.linkedin.com/</a>
    </div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'linkedin_access', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'linkedin_access');?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'linkedin_secret', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'linkedin_secret');?>
	</div>
</div>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>