<?php
/**
 * RESTfull API client for OnSync (http://www.digitalsamba.com)
 * 
 * All methods are throwable. Use them in try/catch blocks.
 * Almost all methods return an array as a REST data.
 * 
 * @author Plamen Petkov
 * 
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 */
class OnsyncApiClient extends CComponent {
    
    const ROLE_MODERATOR = 1;
    const ROLE_PARTICIPANT = 2;
    const ROLE_OBSERVER = 3;
    
    // Used to debug
    public $proxyHost = false;
    public $proxyPort = false;
    
    // Requested Format of the returned data
    // Please do not change. Keep it JSON!!!
    public $format = 'json';  // REST Response format: xml | json | serialize | php | html | csv
    
    
    // Will hold the EHttpClient
    private $_client = null;
    
    // Will come from App Settings
    private $_baseDomain = "";
    private $_version = "";
    private $_username = "";
    private $_password = "";
    
    
    // Holds some predefined URLs
    private $_apiUrl = "";
    private $_baseJoinUrl = "";
            
    // A single AuthVerify is called at init() stage; success/failure is kept here
    private $_authVerified = false;
    

    /**
     * Constructor
     * 
     */
    public function __construct($admin_username = null, $admin_password = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$admin_username && !$admin_password) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'onsync'));
			if($account) {
				$admin_username = $account->settings->admin_username;
				$admin_password = $account->settings->admin_password;
			}
		}

        $this->init($admin_username, $admin_password);
    }

    
    /**
     * Component Init
     * 
     */
    public function init($admin_username = null, $admin_password = null) {
        
        Yii::import('common.extensions.httpclient.*');

        // Hardcoded
        $this->_baseDomain = 'http://onsync.digitalsamba.com';
        $this->_version = '2';

        // From settings
        $this->_username = $admin_username;
        $this->_password = $admin_password;

        // Build the Api-base url  (http://onsync.digitalsamba.com/api/[ver]/[username]/)
        $this->_apiUrl = $this->_baseDomain . '/api/' . (int) $this->_version.  '/' . $this->_username . "/";

        // Base for GO ENTER URL
        $this->_baseJoinUrl = $this->_baseDomain . "/go/" . $this->_username . "/";
        
        // Check if we authenticate successfully

        // Skip this on component creation. Not needed. 
        /*
        if (!$this->authVerify()) {
            throw new Exception('Authentication failed');
        }
        */
    }
    

    /**
     * Makes the API call
     * 
     * @param string $actionPath  REST method
     * @param string $method  GET/POST/PUT/etc.
     * @param string $params
     * @throws Exception
     * @return array
     */
    private function call($actionPath, $method, $params=false) {

        // Add format to all URLs
        $url = $this->_apiUrl . $actionPath . '/format/' . $this->format; 
        
        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
        ));
        // Always set Auth header
        $this->_client->setAuth($this->_username, $this->_password, EHttpClient::AUTH_BASIC);
        
        $body = "";
        if (is_array($params) && (count($params) > 0)) {
            $body = "input_type=json&rest_data=" . CJSON::encode($params);
        }
        
        if (!empty($body)) {
            $this->_client->setRawData($body);
        }
        
        // Do the request
        $response = $this->_client->request($method);
        
        // Check if we've got an error; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            $errorMessage = "";
            $body = $response->getBody();
            if ($body) {
                $body = CJSON::decode($body);
                if (isset($body["error"])) {
                    $errorMessage = $body["error"];
                }
            }
            throw new Exception($this->_client->getLastResponse()->getMessage() . " ($errorMessage)", $this->_client->getLastResponse()->getStatus());
        }
        
        if (!$response) {
            throw new Exception('Invalid response from server');
        }
        
        // Decode response to an array
        //@TODO Check format and use appropriate 'decoding'; currently it uses JSON
        $result = CJSON::decode($response->getBody());
        
        if ( ($result == null) || !is_array($result) ) {
            throw new Exception('Invalid data format received from server');
        }
        
        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        
        
        // Return an array
        return $result;
        
    }
    
    /**
     * Check we can authenticate successfully
     * 
     * @return boolean
     */
    private function authVerify() {
        $result = $this->call('authverify', EHttpClient::GET);
        $this->_authVerified = $result['authenticated'] == 1;
        return $this->_authVerified;
    }

    
    /**
     * Get all cookies
     * 
     */
    private function getAllCookies() {
        return $this->_client->getCookieJar()->getAllCookies();
    }
    
    
    /**
     * Check authentication
     * 
     * @return boolean
     */
    public function checkAuth() {
        return $this->authVerify();
    }
    
    /**
     * Returns an array of all sessions on server (meetings)
     * 
     * @return array
     */
    public function getMeetings() {
        $actionPath = "sessions";
        $method = EHttpClient::GET;
        $result = $this->call($actionPath, $method);
        return $result;
    }
    

    /**
     * Returns a given meeting information
     * 
     * @param number $id
     * @return array
     */
    public function getMeetingById($id) {
        $actionPath = "session/id/" . (int) $id;
        $method = EHttpClient::GET;
        $result = $this->call($actionPath, $method);
        return $result;
    }
    
    
    
    /**
     * Creates a meeting (session)
     * 
     * @param string $topic
     * @param string $startDateTime
     * @param int $duration Minutes
     * @param string $friendlyId
     * @param string $password
     * @param array $invitedParticipants
     * @throws Exception
     * @return array
     */
    public function createMeeting($topic, $startDateTime, $duration, $friendlyId='', $password='', $invitedParticipants=array()) {
        $actionPath = "session";
        $method = EHttpClient::PUT;
        $params = array("topic"=>$topic, "start_time"=>$startDateTime, "duration" => $duration);

        // Optional parameters
        if ($friendlyId) $params['friendly_url'] = $friendlyId;
        if ($password) $params['password'] = $password;
        if (is_array($invitedParticipants) && (count($invitedParticipants) > 0)) $params['invited_participants'] = $invitedParticipants;
        
        $result = $this->call($actionPath, $method, $params);

        if (!is_array($result)) {
            throw new Exception('Invalid response from server. Must be array');
        }
        
        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        
        // ID must be returned, otherwise - error
        if (isset($result['id'])) {
            $result = $result['id'];
        }
        else {
            throw new Exception('Unable to create meeting, no Id is returned');
        }
        
        return $result;
    }
    
    
    /**
     * Updates a meeting
     * 
     * @param int $id
     * @param string $topic
     * @param string $startDateTime
     * @param string $duration
     * @param string $friendlyId
     * @param string $password
     * @param array $invitedParticipants
     * @throws Exception
     * @return array
     */
    public function updateMeeting($id, $topic='', $startDateTime='', $duration=false, $friendlyId='', $password='', $invitedParticipants=array()) {
        
        $actionPath = "session";
        $method = EHttpClient::POST;
        $params = array();
        
        $params['id'] = (int) $id;
        
        // Optional parameters
        if ($topic) $params['topic'] = $topic; 
        if ($startDateTime) $params['start_time'] = $startDateTime;
        if ($duration) $params['duration'] = (int) $duration;
        if ($friendlyId) $params['friendly_url'] = $friendlyId;
        if ($password) $params['password'] = $password;
        if (is_array($invitedParticipants) && (count($invitedParticipants) > 0)) $params['invited_participants'] = $invitedParticipants;
        
        $result = $this->call($actionPath, $method, $params);
        
        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        return $result;
    }
    
    
    
    /**
     * Delete a meeting
     * 
     * @param int $id
     * @throws Exception
     * @return array
     */
    public function deleteMeeting($id) {
        $actionPath = "session/id/" . (int) $id;
        $method = EHttpClient::DELETE;
		try {
        	$result = $this->call($actionPath, $method);
		} catch(Exception $e) {
			if($e->getCode() == 404) { //already deleted
				Yii::log('Onsync API error (id = '.$id.'): '.$e->getMessage());
				return true;
			} else {
				throw new Exception($e->getMessage(), $e->getCode());
			}
		}

        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        
        return $result;
    }
    
    
    /**
     * Get statistics 
     * 
     * @return array
     */
    public function getStats() {
        $actionPath = "statistics/days/2/return/id;sid;jt;lt";
        $method = EHttpClient::GET;
        $result = $this->call($actionPath, $method);
        return $result;
    }
    


    /**
     * Returns a base URL for JOIN URLs
     * 
     * @return string
     */
    public function getJoinBaseUrl() {
        return $this->_baseJoinUrl;
    }
    
    
    
    /**
     * Add attendee to a meeting (session)
     * 
     * @param int $id
     * @param string $email
     * @param string $nameFirst
     * @param string $nameLast
     * @param int $role
     * @throws Exception
     * @return array
     */
    public function addInvitee($id, $email, $nameFirst = "John", $nameLast = "Doe", $role = self::ROLE_PARTICIPANT) {
        $actionPath = "invitee";
        $method = EHttpClient::PUT;
        
        if ( (int) $id <= 0) {
            throw new Exception('Invalid meeting/session id');
        }
        
        if ( !$email ) {
            throw new Exception('Invalid invitee email');
        }
        
        $params['session_id'] = (int) $id;
        $params['email'] = $email;
        $params['first_name'] = $nameFirst;
        $params['last_name'] = $nameLast;
        $params['send_email_invitation'] = 'false';
        $params['role'] = $role;
        
        $result = $this->call($actionPath, $method, $params);
        
        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        return $result;
    }
    
    
    public function getUser() {
        $actionPath = "user";
        $method = EHttpClient::GET;
        $result = $this->call($actionPath, $method);
        if (isset($result["error"])) {
            throw new Exception($result["error"]);
        }
        return $result;
    }
    
    
    
    
}

?>