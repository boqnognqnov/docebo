<?php

/**
 * OnsyncAppSettingsForm class.
 *
 * @property string $admin_username
 * @property string $admin_password
 * @property string $account_name
 * @property string $additional_info
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $max_concurrent_rooms
 */
class OnsyncAppSettingsForm extends CFormModel {

	public $admin_username;
	public $admin_password;
	public $account_name;
	public $additional_info;
	public $max_rooms;
	public $max_rooms_per_course;
	public $max_concurrent_rooms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name, admin_username, admin_password, additional_info, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'safe'),
			array('account_name, admin_username, admin_password, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'required'),
			array('account_name, admin_username, admin_password, max_rooms, max_rooms_per_course', 'required'),
            array('max_concurrent_rooms, max_rooms, max_rooms_per_course', 'numerical'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'admin_username' => Yii::t('standard','_USERNAME'),
			'admin_password' => Yii::t('standard', '_PASSWORD'),
			'account_name' => Yii::t('webinar', 'Account Name'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
		);
	}
}
