<?php

/**
 * Class OnsyncWebinarTool
 * Implemenents webinar tool fo onsync
 */
class OnsyncWebinarTool extends WebinarTool {

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'onsync';
		$this->name = 'Onsync';
	}

	/**
	 * Creates a new room using the Onsyc api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		$friendlyId = $this->randomString(12);
		$durationSeconds = (int) (strtotime($endDateTime) - strtotime($startDateTime));
		if ( $durationSeconds > 0)
			$durationMinutes = (int) floor($durationSeconds/60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new OnsyncApiClient($accountSettings->admin_username, $accountSettings->admin_password);
		$sessionId = $apiClient->createMeeting($roomName, $startDateTime, $durationMinutes, $friendlyId);

		$result = array(
			'session_id' => $sessionId,
			'friendly_id' => $friendlyId,
		);

		return $result;
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$durationSeconds = (int) (strtotime($endDateTime) - strtotime($startDateTime));
		if ( $durationSeconds > 0)
			$durationMinutes = (int) floor($durationSeconds/60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new OnsyncApiClient($accountSettings->admin_username, $accountSettings->admin_password);
		$apiClient->updateMeeting($api_params['session_id'], $roomName, $startDateTime, $durationMinutes);
		$api_params['name'] = $roomName;
		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$session_id = $api_params['session_id'];
		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new OnsyncApiClient($accountSettings->admin_username, $accountSettings->admin_password);
			$apiClient->deleteMeeting($session_id);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new OnsyncApiClient($accountSettings->admin_username, $accountSettings->admin_password);
		return $apiClient->getJoinBaseUrl() . $api_params['friendly_id'];
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new OnsyncApiClient($accountSettings->admin_username, $accountSettings->admin_password);
		return $apiClient->getJoinBaseUrl() . $api_params['friendly_id'];
	}

}