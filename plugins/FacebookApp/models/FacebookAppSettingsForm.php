<?php

/**
 * FacebookAppSettingsForm class.
 *
 * @property string $fb_api
 * @property string $fb_secret
 */
class FacebookAppSettingsForm extends CFormModel {
	public $fb_api;
	public $fb_secret;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('fb_api, fb_secret', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'fb_api' => Yii::t('configuration', '_SOCIAL_FB_API'),
			'fb_secret' => Yii::t('configuration', '_SOCIAL_FB_SECRET'),
		);
	}
}
