<?php
/* @var $form CActiveForm */
/* @var $settings FacebookAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-come-inegrare-docebo-lms-con-facebook/' : 'https://www.docebo.com/knowledge-base/docebo-for-facebook/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('standard','Settings') ?>: <?= Yii::t('app', 'facebook') ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="http://developers.facebook.com/" target="_blank">http://developers.facebook.com/</a>
    </div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'fb_api', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'fb_api');?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'fb_secret', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'fb_secret');?>
	</div>
</div>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>