<?php

class FacebookAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'FacebookApp.models.*',
			'FacebookApp.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('facebook', array(
			'app_icon' => 'fa-facebook',
			'settings' => Docebo::createAdminUrl('FacebookApp/FacebookApp/settings'),
			'label' => 'Facebook',
		),
			array()
		);
	}
}
