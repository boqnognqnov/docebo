<?php

/**
 * Plugin for OpenSesameApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class OpenSesameApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = true;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'OpenSesameApp';


	public static function settingsUrl()
	{
		return Docebo::createAdminUrl('OpenSesameApp/test/index'); //TODO: this wil be removed in the future, it's just for developing/testing purpose
	}


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

		/* @var $db CDbConnection */
		$db = Yii::app()->db;

		$sql = "CREATE TABLE `opensesame_tenants` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `os_id` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `type` VARCHAR(255) NOT NULL DEFAULT 'Prod',
  `api_key` VARCHAR(255) NULL,
  `api_secret` VARCHAR(255) NULL,
  `api_url` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();

		$sql = "CREATE TABLE `opensesame_users` (
  `tenant_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `sso_url` VARCHAR(255) NULL,
  `firstname` VARCHAR(255) NULL,
  `lastname` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  `userid` VARCHAR(255) NULL,
  PRIMARY KEY (`tenant_id`, `user_id`),
  INDEX `fk_opensesame_user_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_opensesame_tenant_id`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `opensesame_tenants` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_opensesame_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;";
		$cmd = $db->createCommand($sql);
		$cmd->execute();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
