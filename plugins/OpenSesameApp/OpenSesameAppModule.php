<?php

class OpenSesameAppModule extends CWebModule {


	private $_assetsUrl;


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}



	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(
				Yii::getPathOfAlias('OpenSesameApp.assets'),
				false,
				-1,
				Yii::app()->params['forceCopyAssets']
			);
		}
		return $this->_assetsUrl;
	}



	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		// Add menu voices !
		/*
		Yii::app()->mainmenu->addAppMenu('opensesame', array(
			'app_initials' => 'OS',
			'link' => Docebo::createAdminUrl('/OpenSesameApp/main/index'),
			'label' => Yii::t('opensesame', 'OpenSesame'),
		), '');
		*/
	}



	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
			'OpenSesameApp.assets.*',
			'OpenSesameApp.assets.images.*',
			'OpenSesameApp.models.*',
			'OpenSesameApp.components.*',
			'OpenSesameApp.components.api.*',
			'OpenSesameApp.controllers.*',
		));

		//whitelist action(s)
		if (Yii::app() instanceof CWebApplication) {
			Yii::app()->request->noCsrfValidationRoutes[] = 'OpenSesameApp/main/contentSynchronization';
		}

		/*
		// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
		if (!Yii::app()->request->isAjaxRequest) {
			if (method_exists(Yii::app(), 'getClientScript')) { // NOTE: make sure that, when executed in a console, this code won't break.
				$cs = Yii::app()->getClientScript();
				if (is_object($cs) && method_exists($cs, 'registerCssFile')) {
					$cs->registerCssFile($this->getAssetsUrl() . "/css/opensesame.css");
				}
			}
		}
		*/

		//assign events
		//Yii::app()->event->on('...', array($this, '...'));
	}

}