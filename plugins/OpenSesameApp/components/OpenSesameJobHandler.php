<?php

class OpenSesameJobHandler extends JobHandler implements IJobHandler {


	/**
	 * Job name
	 * @var string
	 */
	const JOB_NAME = 'OpenSesameJob';


	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'OpenSesameApp.components.OpenSesameJobHandler';


	//some internal parameters for OpenSesame operations
	const OPENSESAME_BATCH_SIZE = 5;
	const OPENSESAME_PREPARING_TIMEOUT = 1500; //in seconds
	const OPENSESAME_RETRY_TIME = 30; //in seconds


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}



	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Already loaded at upper level

		/* @var $job CoreJob */
		$job = $this->job;
		if (!$job) {
			Yii::log('Invalid job: ' . __METHOD__, CLogger::LEVEL_ERROR);
			return;
		}
		//---


		try {

			$params = CJSON::decode($job->params);
			if (empty($params) || !is_array($params)) {
				throw new CException('Invalid job parameters');
			}

			//=======================
			//=== executing stuff ===
			//=======================

			//do all preparing and loading operations here

			$tenantId = $params['tenant_id'];
			$tenant = OpenSesameTenants::model()->findByPk($tenantId);
			if (empty($tenant)) {
				throw new CException('OpenSesame Tenant cannot be found');
			}

			$apiClient = OpenSesameApiClient::getInstance();

			//possible sync type values: ‘current’, ‘history’, ‘historyMetadataOnly’
			$syncType = 'current';
			$pageSize = self::OPENSESAME_BATCH_SIZE;
			$apiParams = array(
				'SyncType' => $syncType,
				'PageSize' => $pageSize
			);

			//do preparation
			$ready = false;
			$timeBegin = microtime(true); //this is to avoid infinite loop in the "while" cycle

			//NOTE: sometimes the job may start too early so that the preparing API would return "error" status with
			// a message "Sync does not contain any courses.", so we wait few second just in case.
			sleep(15);
			$maxErrorWaitingTimeout = 15; //in seconds

			//--- start preparation process
			while (!$ready) {

				$timeNow = microtime(true);
				if ($timeNow - $timeBegin > self::OPENSESAME_PREPARING_TIMEOUT) {
					throw new CException('Preparing operation timeout (prepare_pull_content_sync)');
				}

				//check preparation status through an API call
				$apiResponse = $apiClient->call($tenant->api_key, $tenant->api_secret, $tenant->os_id . '/prepare_pull_content_sync', $apiParams);

				/* @var $apiResponse OpenSesameApiResponse */
				switch ($apiResponse->getResponseStatus()) {

					case 'preparing':
						//preparation of the process is still occurring on OpenSesame servers
						//check if the system has indicated a waiting time (NOTE: the 'RetryAfter' parameter is in the form of unix timestamp)

						$retryAfter = $apiResponse->getResponseParameter('RetryAfter');

						if (!empty($retryAfter) && $retryAfter > 0) {
							//NOTE: we ALWAYS wait at least 3 seconds before launching another API call, just to avoid too much calls

							$difference = $retryAfter - time();
							//if ($difference > 30) { $difference = 30; } //do not let the system to wait too long

							if ($difference > 1) {
								sleep($difference); //wait {n} seconds specified by retry-after parameter
							} else {
								sleep(OPENSESAME_RETRY_TIME); //fallback to default seconds when difference in current time and retry-after time is less than 1 second
							}

						} else {
							sleep(OPENSESAME_RETRY_TIME); //wait anyway for default seconds, just to be sure not to overflow the OpenSesame service (which has some limits about API calls number)
						}

						break;

					case 'success':
						$syncId = $apiResponse->getResponseParameter('SyncId');
						$totalItems = $apiResponse->getResponseParameter('TotalItems');
						$ready = true;
						break;

					case 'error':
						//NOTE: see note above about sync error

						if (strtolower(trim($apiResponse->getErrorMessage())) == strtolower("Sync does not contain any courses.")) {
							if ($maxErrorWaitingTimeout > 0) {
								$maxErrorWaitingTimeout -= 2;
								Yii::log('Error "Sync does not contain any courses." detected. Waiting 2 seconds (remaining: '.$maxErrorWaitingTimeout.')', CLogger::LEVEL_INFO);
								sleep(2);
								break;
							}
						}

					default:
						//something went wrong and we have "failed" status or something else unexpected
						$errorMessage = $apiResponse->getErrorMessage();
						throw new CException('Error while preparing sync'.(!empty($errorMessage) ? ' (error message: "'.$errorMessage.'")' : ''));
						break;
				}

			}
			//--- end preparation process


			//check Sync ID validity
			if (empty($syncId)) {
				throw new CException('Invalid Sync ID');
			}

			//check if we effectivley have something to sync
			if ($totalItems <= 0) {
				//if no objects have to be synched, then just finish here the process
				Yii::log(__METHOD__.' , "TotalItems" value was 0 or not present', CLogger::LEVEL_ERROR);
				return;
			}

			//now we have to retrieve objects batches and import them in the LMS
			$pageNumber = 0;
			$itemsCounter = 0;
			$allPages = false;
			$timeBegin = microtime(true); //this is to avoid infinite loop in the "while" cycle

			//--- start import process
			while (!$allPages) {

				Yii::log(__METHOD__.': "get_content" check: itemsCounter='.$itemsCounter.', pageNumber='.$pageNumber, CLogger::LEVEL_INFO);

				//check time passed from beginning of import process
				$timeNow = microtime(true);
				if (($timeNow - $timeBegin) > self::OPENSESAME_PREPARING_TIMEOUT) { //give maximum time to finish the operation, in order to avoid potential infinite loops in the while cycle
					throw new CException('Preparing operation timeout (get_content_page)');
				}

				//retrieve physical batch of packages
				$apiParams = array(
					'SyncId' => $syncId,
					'PageNumber' => $pageNumber
				);
				$apiResponse = $apiClient->call($tenant->api_key, $tenant->api_secret, $tenant->os_id . '/get_content_page', $apiParams);

				//check if packets are ready for download

				if ($apiResponse->getResponseStatus() == 'preparing') {

					//preparation of the packages is still occurring on OpenSesame servers
					//check if the system has indicated a waiting time (NOTE: the 'RetryAfter' parameter is in the form of unix timestamp)

					$retryAfter = $apiResponse->getResponseParameter('RetryAfter');

					//NOTE: we ALWAYS wait at least 3 seconds before launching another API call, just to avoid too much calls

					if (!empty($retryAfter) && $retryAfter > 0) {
						$difference = $retryAfter - time();

						//if ($difference > 30) { $difference = 30; } //do not let the system to wait too long

						if ($difference > 1) {
							sleep($difference); //wait {n} seconds specified by retry-after parameter
						} else {
							sleep(OPENSESAME_RETRY_TIME); //fallback to 3 second when difference in current time and retry-after time is less than 1 second
						}
					} else {
						sleep(OPENSESAME_RETRY_TIME); //wait anyway for 3 second, just to be sure not to overflow the OpenSesame service (which has some limits about API calls number)
					}
					continue; //go back to loop beginning, nothing else to do here
				}

				//content is ready, check if we are receiving files or something else went wrong
				if (!$apiResponse->isSuccessful()) {
					Yii::log(__METHOD__ . ' ERROR: OpenSesame API call failed ' . trim('(HTTP status : ' . $apiResponse->getHttpStatus() . ') ' . $apiResponse->getErrorMessage()), CLogger::LEVEL_ERROR);
					throw new CException('OpenSesame API call failed');
				}

				//check format and handle stuff
				switch ($apiResponse->getOutputType()) {
					case 'file':
						$path = $apiResponse->getResponseData(); //in case of file, the data in the response is a string containing the file path
						$result = $this->handleScormPackages($path, $params['user_id']);
						//TODO: handle result
						break;
					case 'json':
						//no file here, this happens when a json (usually in the form of simple "{success:true}") is returned instead of a file content
						break;
					default:
						Yii::log(__METHOD__ . ' ERROR: API response of incorrect or unknown type.'."\n".print_r($apiResponse, true), CLogger::LEVEL_ERROR);
						throw new CException('OpenSesame API response of incorrect or unknown type');
						break;
				}

				//update items counter and page number for the next iteration of the loop
				$itemsCounter += $pageSize;
				$pageNumber++;
				if ($itemsCounter >= $totalItems) {
					$allPages = true;
				}
			}
			//-- end import process

			Yii::log(__METHOD__.': content sync operation is finished.', CLogger::LEVEL_INFO);

			//==========================
			//=== end execution part ===
			//==========================

		} catch(Exception $e) {

			Yii::log("ERROR:\n".$e->getMessage()."\n\nSTACK TRACE:\n".$e->getTraceAsString(), 'error');
			throw $e;
		}

	}




	/**
	 * After receiving a batch of packages from OpenSesame, we need to extract packages and import them in the LMS
	 *
	 * @param $path The path of the zip file to be processed
	 * @throws CException
	 */
	protected function handleScormPackages($path, $userId) {

		$path_parts = pathinfo($path);
		$destFolderName = str_replace('.zip', '', $path_parts['basename']);
		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $destFolderName;
		// check if a previous upload (path) exists; create folder if not
		if (!file_exists($unpackPath)) {
			if ( ! mkdir($unpackPath, 0777) ) {
				throw new CException("Directory creation failed.");
			}
		}
		$unpackPath = realpath($unpackPath);


		// ZIP Management
		$zip = new ZipArchive();
		// Open the zip file
		$zip_open = $zip->open($path);

		//Yii::log("DEBUG OPENSESAME:\nfile path = $path\nfilesize = ".filesize($path)."\nunpack path = $unpackPath", CLogger::LEVEL_INFO);

		if ($zip_open !== true) {
			// unable to open the zip file
			throw new CException('Unable to open zip file (Error: '.$zip_open.')');
		}

		// Checking if it is a valid scorm package
		if ($zip->locateName("metadata.json") === false) {
			// ok no imsmanifest, throw this away
			throw new CException('File \'metadata.json\' not found in the archive');
		}

		// Extract files in the zip archive
		if ($zip->extractTo($unpackPath) !== true) {
			throw new CException('Unable to extract zip archive');
		}
		$zip->close();


		//read metadata: there there is info about the packages contained in the file "courses.zip"
		$metadata_json = file_get_contents($unpackPath.DIRECTORY_SEPARATOR.'metadata.json');
		//Log::_( "TESTING: ".$metadata_json);

		$metadata = CJSON::decode($metadata_json);
		if (empty($metadata)) {
			throw new CException('Invalid metadata');
		}

		//start processing "courses.zip"
		$zip = new ZipArchive();
		// Open the zip file
		$zip_open = $zip->open($unpackPath.DIRECTORY_SEPARATOR.'courses.zip');
		if ($zip_open !== true) {
			// unable to open the zip file
			throw new CException('Unable to open zip file (Error: '.$zip_open.')');
		}

		// Extract files in the zip archive
		// NOTE: scorm package model automatically is looking for package files into "upload_tmp" folder
		if ($zip->extractTo(Docebo::getUploadTmpPath()) !== true) {
			throw new CException('Unable to extract zip archive');
		}
		$zip->close();

		/*
		Sample Metadata (Both for pull and push sync; can be customized per integration):
		[{
		"zipFilename" : "323dbb88904c4461add57e91cdb8c5a6.zip",
		"title" : "Taking Accountability",
		"expirationDate" : "20141129",
		"bundleName" : "Office Etiquette",
		"mobileCompatibility" : "allDevices",
		"externalId" : "323dbb88904c4461add57e91cdb8c5a6"
		},{
		"zipFilename" : "323dbb88904c4461add57e91cdb8c5a6.zip",
		"title" : "Mentoring 101",
		"expirationDate" : "20141129",
		"bundleName" : "Leadership",
		"mobileCompatibility" : "allDevices",
		"externalId" : "323dbb88904c4461add57e91cdb8c5a6"
		}]

		List of Available Metadata Fields:
		● Course Title
		● Seller (course provider)
		● Seat Time (duration, in minutes)
		● Description
		● Learning Objectives
		● Intended Audience
		● Subjects (categories)
		● Course features (audio, video, games, quizzes, simulation, etc.)
		● Course image (thumbnail)
		● System Requirements
		● Interface Language
		● Accredited By
		*/

		Yii::log('Scorm packages extracted, starting saving into system ...', CLogger::LEVEL_INFO);

		//this will physically load Scorm packages in the LMS
		$result = OpenSesameHelper::saveScormPackages($metadata, $userId);

		Yii::log('Scorm packages import done.'."\nResult:\n".print_r($result, true), CLogger::LEVEL_INFO);

		return $result;
	}


}