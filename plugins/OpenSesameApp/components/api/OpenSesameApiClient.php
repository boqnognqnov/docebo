<?php

class OpenSesameApiClient {


	private static $_instance = NULL;

	private static $_errorMessage = false;

	private static $_token = false;

	protected static $_apiBaseUrl = 'https://www.opensesame.com/api/';

	/* Since Slack has a limit on possible API calls during time, we try to manage it by code */
	protected $max_api_calls_per_second = 0;

	private function __construct() {
		//NOTE: this is private in order to make a "factory" class
	}


	public static function getInstance() {
		if (self::$_instance === NULL) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}


	public static function getToken() {
		if (self::$_token === false) {
			$tenantModel = OpenSesameHelper::getCurrentTenant();
			if (!empty($tenantModel) && !empty($tenantModel->token)) {
				self::$_token = $tenantModel->token;
			}
		}
		return self::$_token;
	}


	private static function setError($message) {
		$message = trim($message);
		if (!empty($message)) {
			Yii::log(__CLASS__.' ERROR: '.$message, CLogger::LEVEL_ERROR);
			self::$_errorMessage = $message;
		}
	}

	public static function getError() {
		return self::$_errorMessage;
	}

	private static function resetError() {
		self::$_errorMessage = false;
	}


	/**
	 * This function should ensure that no more than {$this->max_api_calls_per_second} API calls are performed in a second
	 */
	protected function startCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//start doing checks
		if ($this->max_api_calls_per_second <= 0) return; //no limit of API calls per second
		$time_slice = ceil(1000000 / $this->max_api_calls_per_second); //microseconds, time to wait between each API call
		if ($time_slice <= 0) return; //extreme case
		$last_api_call_time_J = Settings::get('opensesame_last_api_call_time', null);
		$current_time_MT = microtime();
		if (empty($last_api_call_time_J)) {
			//this means that there have been no API calls yet on this server, so just set the setting and go on
			Settings::save('opensesame_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
		} else {
			//some API calls have already been made, check if enough time has passed
			$go_on = false;
			while (!$go_on) {
				//first check if someone else is already performing API calls in this moment
				$previous_value = CJSON::decode($last_api_call_time_J);
				if (empty($previous_value)) { throw new CException('Error while checking API call time'); }
				if ($previous_value['user_calling'] != 0 && empty($previous_value['end_time'])) {
					usleep(100000); //wait an arbitrary 0.1 seconds and then re-check
					$last_api_call_time_J = Settings::get('opensesame_last_api_call_time', null);
					$current_time_MT = microtime();
				} else {
					$go_on = true;
				}
			}
			//reserve the right to API-call for the current user
			//(other user accessing the API calling function will wait until we are finished here, see above 'while' cycle)
			Settings::save('opensesame_last_api_call_time', CJSON::encode(array(
				'start_time' => $current_time_MT,
				'end_time' => "",
				'user_calling' => $uid
			)));
			//check last API call ending time
			$last_api_call_time_MT = $previous_value['end_time'];
			list($current_time_uS, $current_time_S) = explode(' ', $current_time_MT);
			list($last_api_call_time_uS, $last_api_call_time_S) = explode(' ', $last_api_call_time_MT);
			$diff_S = (doubleval($current_time_S) - doubleval($last_api_call_time_S));
			$diff_uS = (doubleval($current_time_uS) - doubleval($last_api_call_time_uS));
			//NOTE: since max time slice is 1 second, $diff_S (difference in seconds) has just to be less than 1
			if (($diff_S < 1) && (floor($diff_uS * 1000000) < $time_slice)) {
				$waiting_time = (int)($time_slice - floor($diff_uS * 1000000));
				Yii::log('OpenSesame API call delay: '.number_format($waiting_time, 0, '', '').' microseconds', CLogger::LEVEL_INFO);
				usleep($waiting_time);
			}
		}
	}


	/**
	 * Finalize the API call checks
	 */
	protected function stopCheckingLastAPICallTime() {
		//check current user
		$uid = Yii::app()->user->id;
		if (empty($uid) || is_null($uid)) {
			//maybe we are in a background async. job, with no current user itself?
			$uid = -1;
		}
		//do checks
		$previous_value = Settings::get('opensesame_last_api_call_time', null);
		if (empty($previous_value)) { return; } //this should not happen, anyway nothing else to do here
		$value = CJSON::decode($previous_value);
		if (empty($value) || $value['user_calling'] != $uid) { throw new CException('Error while checking API call time'); }
		//API call is finished, so set the end time and remove the user ID from calling parameter
		$value['end_time'] = microtime();
		$value['user_calling'] = 0;
		Settings::save('opensesame_last_api_call_time', CJSON::encode($value));
	}


	/**
	 * @param $key
	 * @param $secret
	 * @param $apiCall
	 * @param $callParams
	 * @return OpenSesameApiResponse
	 * @throws CException
	 */
	public function call($key, $secret, $apiCall, $callParams) {
		// URL format: www.opensesame.com/api/{integrationName}/{methodName}
		$url = self::$_apiBaseUrl . Settings::get('opensesame_integration_name', '') . '/' . $apiCall;

		$this->startCheckingLastAPICallTime();

		$method = 'POST';
		OpenSesameOAuthUtil::Instance()->SignRequest($key, $secret, $url, $callParams, $method);

//die('<pre>'.print_r($callParams, true).'</pre>');
		$fields_string = self::build_base_string($callParams, '&');
		$options = array(
			//CURLOPT_HTTPHEADER => $header,
			CURLOPT_HEADER => false,
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_POST => count($callParams),
			CURLOPT_POSTFIELDS => $fields_string
		);

		$outputType = $this->outputType($apiCall);

		//check output type for special curl options
		if ($outputType == 'file') {
			$name = 'tmp_opensesame_fetch_'.mt_rand(100000, 999999).'_'.time().'.zip'; //generate an unique name, time-based plus some random addition
			$path = Docebo::getUploadTmpPath()   . DIRECTORY_SEPARATOR . $name;
			$file = fopen($path, 'w+');
			$options[CURLOPT_FILE] = $file;
			$options[CURLOPT_FOLLOWLOCATION] = true;
			$options[CURLOPT_TIMEOUT] = 1000; //high timeout time, in case the file is large
		} else {
			//$options[CURLOPT_RETURNTRANSFER] = true;
		}

		//Init the request and set its params
		$request = curl_init();
		curl_setopt_array($request, $options);

		//Make the request
		$response = curl_exec($request);

		$this->stopCheckingLastAPICallTime();

		if ($outputType == 'file') {
			fclose($file);
			/*
			//just for logging purpose
			$cnt = file_get_contents($path, null, null, 0, 110);
			if (strlen($cnt) > 100) {
				$cnt = substr($cnt, 0, 100).' ...';
			}
			Yii::log("DEBUG OpenSesame ".__METHOD__." :\nfilesize = ".filesize($path)."\ntest content = ".$cnt);
			*/
			//check real content type
			$cnt = file_get_contents($path, null, null, 0, 1);
			if ($cnt == '{') { //check by the first character if the content is a possible JSON format
				Yii::log(__METHOD__.' , detected a possible JSON response for a "file" API call ('.$apiCall.')', CLogger::LEVEL_INFO);
				//this may be a json response, so retrieve entire content and try to decode it
				$cnt = file_get_contents($path);
				$decoded_cnt = CJSON::decode($cnt);
				if (!empty($decoded_cnt)) {
					//we found a json message instead of a file content
					$outputType = 'json'; //revert back the previous checked type into json
					$response = $cnt; //the file content is treated as a normal json response
					unlink($path); //we don't need the file anymore, remove it
				}
			}
		}

		$http_status = curl_getinfo($request, CURLINFO_HTTP_CODE);

		Yii::log("DEBUG OpenSesame API call '$apiCall' response:\nurl = ".$url."\nkey = ".$key."\nsecret = ".$secret."\nstatus = ".$http_status."\nresponse = ".print_r($response, true), CLogger::LEVEL_INFO);

		curl_close($request);

		if ($outputType != 'file') {
			if (empty($response)) {
				//self::setError(curl_error($request));
				//return false;
				return new OpenSesameApiResponse($http_status, false, false, false, curl_error($request));
			}
		}

		if ($outputType == 'json') {
			$decoded = CJSON::decode($response);
			if (empty($decoded)) {
				//self::setError("Invalid API result");
				//return false;
				return new OpenSesameApiResponse($http_status, false, false, false, "Invalid API result");
			}
			if ($decoded['status'] != 'success') {
				Log::_("Debugging " . __METHOD__ . ' ('.$apiCall.'):' . "\n" . print_r($decoded, true));
				//self::setError('API call was not successfull');
				//return false;
				return new OpenSesameApiResponse($http_status, $decoded['status'], false, false, $decoded['message']);
			}
		}

		if ($outputType == 'json') {
			return new OpenSesameApiResponse($http_status, $decoded['status'], $decoded['data'], 'json', false);
		}
		if ($outputType == 'file') {
			return new OpenSesameApiResponse($http_status, 'success', $path, 'file', false);
		}

		return new OpenSesameApiResponse($http_status, false, false, false, "Unknown output format ($outputType)"); //this should never happen
	}



	protected function outputType($call) {
		if (strpos($call, 'fetch_content') !== false) {
			return 'file';
		}
		if (strpos($call, 'get_content_page') !== false) {
			return 'file';
		}
		return 'json';
	}



	//----------------------------------

	protected $_consumerKey = 'ff8309db-bac8-4096-ba04-dcd0252da08c';//Settings::get('opensesame_oauth_key');
	protected $_consumerSecret = 'b9b3d329-e25c-4c27-a948-e2c3e2601f51';//Settings::get('opensesame_oauth_secret');


	/**
	 * Computes and returns a signature for the request.
	 * @param $secret
	 * @param $fields
	 * @param $request_type
	 * @param $url
	 * @return string
	 */
	static public function build_signature($secret, $fields, $request_type, $url){
		ksort($fields);
		//Build base string to be used as a signature
		$base_info = $request_type.'&'.$url.'&'.self::build_base_string($fields, '&'); //return complete base string
		//Create the signature from the secret and base string
		$composite_key = rawurlencode($secret);
		return base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));

	}

	/**
	 * Builds a segment from an array of fields.  Its used to create string representations of headers and URIs
	 * @param $fields
	 * @param $delim
	 * @return string
	 */
	static public function build_base_string($fields, $delim){
		$r = array();
		foreach($fields as $key=>$value){
			$r[] = rawurlencode($key) . "=" . rawurlencode($value);
		}
		return implode($delim, $r); //return complete base string

	}

	/**
	 * Returns typical headers needed for a request
	 * @param $consumer_key
	 * @param $nonce
	 */
	static public function auth_headers($consumer_key, $nonce = ''){
		return array(
			'oauth_consumer_key' => $consumer_key,
			'oauth_nonce' => $nonce,
			'oauth_signature_method' => 'HMAC-SHA1',
			'oauth_timestamp' => time(),
			'oauth_version' => '1.0'
		);
	}




}