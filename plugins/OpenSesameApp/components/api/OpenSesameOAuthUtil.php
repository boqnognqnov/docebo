<?php

//usage example: OAuthUtil::Instance()->SignRequest($tenant->OAuthKey(), $tenant->OAuthSecret(), $url, $params, 'GET');

class OpenSesameOAuthUtil {

	const OAUTH_CONSUMER_KEY_KEY = 'oauth_consumer_key';
	const OAUTH_SIGNATURE_KEY = 'oauth_signature';
	const OAUTH_SIGNATURE_METHOD_KEY = 'oauth_signature_method';
	const OAUTH_NONCE_KEY = 'oauth_nonce';
	const OAUTH_TIMESTAMP_KEY = 'oauth_timestamp';
	const OAUTH_VERSION_KEY = 'oauth_version';

	private static $singletonInstance;

	public static function Instance() {
		if (!self::$singletonInstance) {
			self::$singletonInstance = new OpenSesameOAuthUtil();
		}
		return self::$singletonInstance;
	}

	public static function Clear() {
		self::$singletonInstance = NULL;
	}

	public function SignRequest($consumer_key, $consumer_secret, $url, &$params, $method = 'POST') {
		$params[self::OAUTH_VERSION_KEY] = '1.0';
		$params[self::OAUTH_SIGNATURE_METHOD_KEY] = 'HMAC-SHA1';
		$nonce = Guid::NewGuid();
		$timestamp = time();

		$oauth = new OAuth($consumer_key, $consumer_secret);
		$oauth->setNonce($nonce);
		$oauth->setTimestamp($timestamp);
		$params[self::OAUTH_SIGNATURE_KEY] = $oauth->generateSignature($method, $url, $params);

		$params[self::OAUTH_NONCE_KEY] = $nonce;
		$params[self::OAUTH_TIMESTAMP_KEY] = $timestamp;
		$params[self::OAUTH_CONSUMER_KEY_KEY] = $consumer_key;
	}


	public function getSignatureParams($consumer_key, $consumer_secret, $url, $method = 'POST') {
		$params = array();
		$params[self::OAUTH_VERSION_KEY] = '1.0';
		$params[self::OAUTH_SIGNATURE_METHOD_KEY] = 'HMAC-SHA1';
		$nonce = Guid::NewGuid();
		$timestamp = time();

		/*
		$oauth = new OAuth($consumer_key, $consumer_secret);
		$oauth->setNonce($nonce);
		$oauth->setTimestamp($timestamp);
		$params[self::OAUTH_SIGNATURE_KEY] = $oauth->generateSignature($method, $url, $params);
		*/
		$params[self::OAUTH_SIGNATURE_KEY] = $this->generateSignature($consumer_key, $consumer_secret, $method, $url, $params);

		$params[self::OAUTH_NONCE_KEY] = $nonce;
		$params[self::OAUTH_TIMESTAMP_KEY] = $timestamp;
		$params[self::OAUTH_CONSUMER_KEY_KEY] = $consumer_key;

		return $params;
	}


	protected function generateSignature($consumer_key, $consumer_secret, $method, $url, $fields) {

		$fieldsString = '';
		if (!empty($fields)) {
			$r = array();
			foreach ($fields as $key => $value) {
				$r[] = rawurlencode($key) . "=" . rawurlencode($value);
			}
			$fieldsString = implode('&', $r);
		}

		$base_info = $method.'&'.$url.'&'.$fieldsString;
		$composite_key = rawurlencode($consumer_secret);
		$output = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));

		return $output;
	}

}

class Guid {
	public static function NewGuid() {
		//return substr(str_replace('.', '', uniqid(uniqid(), TRUE)), 0, 32);

		// The field names refer to RFC 4122 section 4.1.2.
		return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
			// 32 bits for "time_low".
			mt_rand(0, 65535), mt_rand(0, 65535),
			// 16 bits for "time_mid".
			mt_rand(0, 65535),
			// 12 bits before the 0100 of (version) 4 for "time_hi_and_version".
			mt_rand(0, 4095),
			bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
			// 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
			// (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
			// 8 bits for "clk_seq_low" 48 bits for "node".
			mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
		);
	}

	public static function EnsureHasDashes($guid) {
		if (mb_strlen($guid) == 32) {
			$segments = array(
				substr($guid, 0, 8),
				substr($guid, 8, 4),
				substr($guid, 12, 4),
				substr($guid, 16, 4),
				substr($guid, 20, 12),
			);
			return implode('-', $segments);
		}

		return $guid;
	}

	public static function EnsureDoesNotHaveDashes($guid) {
		return str_replace('-', '', $guid);
	}

	// Return TRUE if $guid is a valid GUID (with dashes)
	public static function EnsureGuid($guid) {
		$guid = strtoupper($guid);
		if (preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/', $guid)) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}