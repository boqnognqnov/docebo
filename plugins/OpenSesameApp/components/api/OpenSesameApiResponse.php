<?php

class OpenSesameApiResponse {

	protected $http_status;
	protected $response_status;
	protected $response_data;
	protected $output_type;
	protected $error_message;


	public function __construct($http_status, $response_status, $response_data, $output_type = 'json', $error_message = false) {
		$this->http_status = $http_status;
		$this->response_status = $response_status;
		$this->response_data = $response_data;
		$this->output_type = $this->validateOutputType($output_type);
		$this->error_message = $error_message;
	}


	protected function validateOutputType($type) {
		switch ($type) {
			case 'json':
			case 'file':
				return $type;
				break;
		}
		return false;
	}


	public function getHttpStatus() {
		return $this->http_status;
	}


	public function getResponseStatus() {
		return $this->response_status;
	}


	public function getOutputType() {
		return $this->output_type;
	}


	public function isSuccessful() {
		return ($this->getHttpStatus() == 200 && ($this->response_status == 'success' || ($this->output_type == 'file' && !empty($this->response_data))));
	}


	public function getErrorMessage() {
		return $this->error_message;
	}


	public function getResponseData() {
		return $this->response_data;
	}


	public function getResponseParameter($index) {
		if (empty($index) || !is_array($this->response_data) || !isset($this->response_data[$index])) {
			return null;
		}
		return $this->response_data[$index];
	}

}
