<?php


class OpenSesameHelper extends CComponent {


	const PROVIDER_NAME = 'OpenSesame';



	public static function getCurrentTenant() {
		$ct = Settings::get('opensesame_current_tenant', false);
		if ($ct === false) {
			return OpenSesameTenants::model()->find();
		}
		return OpenSesameTenants::model()->findByPk($ct);
	}





	public static function saveScormPackage($params, $userId = false) {
		$fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $params['zipFilename'];
		$success = false;
		$message = Yii::t('standard', '_OPERATION_FAILURE');

		$shortDescription = $params['description'];
		$thumb = $params['thumb'];


		// In case we are doing SCORM Package update ... we will get idOrg != false ... (from edit form)
		$idObject = 0;//(int) Yii::app()->request->getParam("id_object", false);
		if ($idObject > 0) {
			//updating not managed for the moment ...
		}


		// Otherwise, we are obviosly going to create NEW One... lets go...

		Yii::app()->db->beginTransaction();

		try {

			// Validate file input
			if (!file_exists($fullFilePath)) { throw new CException("Invalid file."); }

			//$fullFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $file['name'];
			/*********************************************************/
			if (isset(Yii::app()->params['virusscan_enable']) && (Yii::app()->params['virusscan_enable'] == true)) {
				$vs = new VirusScanner();
				$clean = $vs->scanFile($fullFilePath);
				if (!$clean) {
					FileHelper::removeFile($fullFilePath);
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}

			/*********************************************************/

			/*** File extension type validate ************************/
			$fileValidator = new FileTypeValidator(FileTypeValidator::TYPE_ARCHIVE);
			if (!$fileValidator->validateLocalFile($fullFilePath))
			{
				FileHelper::removeFile($fullFilePath);
				throw new CException($fileValidator->errorMsg);
			}
			/*********************************************************/

			$providerId = self::getProviderId();
			if (empty($providerId)) {
				return array(
					'success' => false,
					'message' => 'Invalid provider ID'
				);
			}

			//handle description length (by LMS specifications it should be max 200 chars)
			if (strlen($shortDescription) > 200) {
				$shortDescription = substr($shortDescription, 0, 197) . '...';
			}

			//create effective repository object in database
			$repoObject = new LearningRepositoryObject();
			$repoObject->short_description = $shortDescription;
			$repoObject->resource = intval($thumb);
			$repoObject->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
			$repoObject->params_json = CJSON::encode(self::prepareLoOptions());
			$repoObject->from_csp = $providerId;
			$categoryNodeId = LearningCourseCategoryTree::getRootNodeId();
			if (isset(Yii::app()->session['currentCentralRepoNodeId'])) {
				$categoryNodeId = Yii::app()->session['currentCentralRepoNodeId'];
			}
			$repoObject->id_category = $categoryNodeId;

			if ($repoObject->save()) {
				$result = self::createNewVersion($repoObject->id_object, $params['zipFilename'], 0, $userId);
				if ($result) {
					$success = true;
					$message = Yii::t('standard', '_OPERATION_SUCCESSFUL');
				} else {
					throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
				}
			}

			$transaction = Yii::app()->getDb()->getCurrentTransaction();
			if ($transaction) {
				$transaction->commit();
			}
			$output = array(
				'success' => $success,
				'message' => $message
			);

		} catch (CException $e) {

			$transaction = Yii::app()->getDb()->getCurrentTransaction();
			if($transaction) {
				$transaction->rollback();
			}
			$output = array(
				'success' => false,
				'message' => $e->getMessage()
			);
		}

		return $output;
	}


	protected static function createNewVersion($idObject, $fileName, $prevVersion = 0, $userId = false){

		$versionName = 'V1';//Yii::app()->request->getParam('version_name', 'V1');
		$versionDesc = false;//Yii::app()->request->getParam('version_description', false);

		$uid = (!empty($userId) ? $userId : Yii::app()->user->id);
		$userModel = (!empty($userId) ? CoreUser::model()->findByPk($userId) : Yii::app()->user->loadUserModel());
		if (empty($userModel)) {
			Yii::log(__METHOD__.' ERROR: invalid user specified #'.$uid, CLogger::LEVEL_ERROR);
			throw new CException('invalid user specified #'.$uid);
		}

		$package = new LearningScormPackage();
		$package->idUser = $uid;
		$package->setFile($fileName);
		$package->setScenario('centralRepo');

		if ($package->save()) {

			$repoObjectVersion = new LearningRepositoryObjectVersion();
			/**
			 * @var $userModel CoreUser
			 */
			$authorData = array(
				'idUser' => $userModel->idst,
				'username' => $userModel->getUserNameFormatted()
			);
			$repoObjectVersion->author = CJSON::encode($authorData);
			$repoObjectVersion->create_date = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
			$repoObjectVersion->id_object = $idObject;
			$repoObjectVersion->object_type = LearningOrganization::OBJECT_TYPE_SCORMORG;
			$repoObjectVersion->id_resource = $package->scormOrganization->idscorm_organization;
			$repoObjectVersion->version = $prevVersion + 1;
			$repoObjectVersion->version_name = $versionName;
			if ($versionDesc !== false) {
				$repoObjectVersion->version_description = $versionDesc;
			}

			if ($repoObjectVersion->save()) {

				$repoObject = LearningRepositoryObject::model()->findByPk($idObject);

				if($repoObject){
					$repoObject->title = $package->scormOrganization->title;

					return $repoObject->save();
				}
			}
		}

		return false;
	}



	/**
	 * A work towards saving LO specific options/settings based on the current REQUEST to axSaveLo() action
	 *
	 * @param object|bool $loModel
	 */
	protected static function prepareLoOptions() {

		$data = array();

		// These are pretty common and can be sent by all LO types being saved/uploaded
		$data['desktop_openmode']    = LearningOrganization::OPENMODE_LIGHTBOX;//Yii::app()->request->getParam("desktop_openmode", LearningOrganization::OPENMODE_LIGHTBOX);
		$data['tablet_openmode']     = LearningOrganization::OPENMODE_FULLSCREEN;//Yii::app()->request->getParam("tablet_openmode", LearningOrganization::OPENMODE_FULLSCREEN);
		$data['smartphone_openmode'] = LearningOrganization::OPENMODE_FULLSCREEN;//Yii::app()->request->getParam("smartphone_openmode", LearningOrganization::OPENMODE_FULLSCREEN);

		return $data;
	}



	public static function saveScormPackages($metadata, $userId = false) {

		$output = array();

		if (!is_array($metadata)) {
			$output['message'] = 'Invalid metadata';
			$output['success'] = false;
			return $output;
		}

		try {

			foreach ($metadata as $metadata_item) {
				Yii::log('DEBUGGING '.__METHOD__.', importing scorm package '.$metadata_item['zipFilename'].' into Central LO', CLogger::LEVEL_INFO);
				$output['imported_packages'][] = self::saveScormPackage($metadata_item, $userId);
			}

		} catch (Exception $e) {
			$output['message'] = $e->getMessage();
			$output['success'] = false;
			return $output;
		}

		$output['message'] = '';
		$output['success'] = true;
		return $output;
	}



	public static function getProviderId() {
		$query = "SELECT * FROM lti_providers_info WHERE name = :name";
		$cmd = Yii::app()->db->createCommand($query);
		/* @var $cmd CDbCommand */
		$cmd->params[':name'] = self::PROVIDER_NAME;
		$record = $cmd->queryRow();
		if (empty($record)) {
			Yii::log(__METHOD__.' DEBUG: could not find "OpenSesame" provider record in table "lti_providers_info".', CLogger::LEVEL_WARNING);
			return false;
		}
		return $record['id'];
	}

}