<?php


class TestController extends Controller {



	public function actionCreateTenant() {
		$key = 'ff8309db-bac8-4096-ba04-dcd0252da08c';//$tenant->OAuthKey()
		$secret = 'b9b3d329-e25c-4c27-a948-e2c3e2601f51';//$tenant->OAuthSecret()
		$url = 'https://www.opensesame.com/api/docebo/provision_tenant';
		$params = array(
			'TenantId' => 'docebo_test_prod',
			'TenantName' => 'Docebo Test Prod',
			'TenantType' => OpenSesameTenants::TYPE_PROD
		);
		$method = 'POST';
		OpenSesameOAuthUtil::Instance()->SignRequest($key, $secret, $url, $params, $method);
		//die('<pre>'.print_r($params, true).'</pre>');

		$fields_string = OpenSesameApiClient::build_base_string($params, '&');;
		if (strcasecmp($method, "GET") == 0) {
			$url .= '?'.$fields_string;
			$options = array(
				//CURLOPT_HTTPHEADER => $header,
				CURLOPT_HEADER => false,
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false
			);
		} else {
			$options = array(
				//CURLOPT_HTTPHEADER => $header,
				CURLOPT_HEADER => false,
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_POST => count($params),
				CURLOPT_POSTFIELDS => $fields_string
			);
		}

		//Init the request and set its params
		$request = curl_init();
		curl_setopt_array($request, $options);

		//Make the request
		$response = curl_exec($request);
		$status = curl_getinfo($request, CURLINFO_HTTP_CODE);
		curl_close($request);

		die('<pre>STATUS: '.$status."\n".'RESPONSE:'."\n".print_r($response, true).'</pre>');
	}



	public function actionRemoveTenant() {
		//opensesame tenant name has been passed
		$tenantOsId = Yii::app()->request->getParam('tenant_os_id', false);
		if (!empty($tenantOsId)) {
			$key = Settings::get('opensesame_oauth_key', '');
			$secret = Settings::get('opensesame_oauth_secret', '');

			$apiClient = OpenSesameApiClient::getInstance();
			$response = $apiClient->call($key, $secret, 'remove_tenant', array(
				'TenantId' => $tenantOsId
			));

			die('<pre>'.print_r($response, true).'</pre>');
		}

		//local docebo id has been passed instead
		$tenantId = Yii::app()->request->getParam('tenant_id', false);
		if (!empty($tenantId)) {

			$tenant = OpenSesameTenants::model()->findByPk($tenantId);
			if (empty($tenant)) die();

			$key = Settings::get('opensesame_oauth_key', '');
			$secret = Settings::get('opensesame_oauth_secret', '');

			$apiClient = OpenSesameApiClient::getInstance();
			$response = $apiClient->call($key, $secret, 'remove_tenant', array(
				'TenantId' => $tenant->os_id
			));

			if ($response->isSuccessful()) {
				$tenant->delete();
			}

			die('<pre>'.print_r($response, true).'</pre>');
		}
	}



	public function actionUserProvisioning() {

		$tenant = OpenSesameHelper::getCurrentTenant();
		if (empty($tenant)) {
			throw new CException('No tenant available');
		}

		/* @var $tenant OpenSesameTenant */
		/* @var $user CoreUser */

		$user = CoreUser::model()->findByPk(Yii::app()->user->id);
		if (empty($user) || $user->userid == '/Anonymous') {
			throw new CException('Invalid user');
		}

		/*
		Request Parameters:
		FirstName: (string) user’s first name
		LastName: (string) user’s last name
		Email: (string) user’s email address
		UserId: (string) unique identifier for the user
		*/
		$params = array(
			'FirstName' => $user->firstname,
			'LastName' => $user->lastname,
			'Email' => $user->email,
			'UserId' => ltrim($user->userid, '/')
		);

		$apiClient = OpenSesameApiClient::getInstance();
		$key = $tenant->api_key;
		$secret = $tenant->api_secret;
		$response = $apiClient->call($key, $secret, $tenant->os_id.'/provision_user', $params);

		die('USER PROVISIONING TEST:<pre>'.print_r($response, true).'</pre>');
	}



	//---------------------------------------------------------------------




	public function actionContentSync() {
		//PUSH method, not intended to be used (will be removed later)
	}





	public function actionPullContentSync() {

		$tenant = OpenSesameHelper::getCurrentTenant();
		if (empty($tenant)) {
			throw new CException('No tenant available');
		}

		$params = array('SyncType' => 'new');

		$apiClient = OpenSesameApiClient::getInstance();
		$response = $apiClient->call($tenant->api_key, $tenant->api_secret, $tenant->os_id.'/fetch_content', $params);

		//echo '"PullContentSync" result of fetching:<pre>'.print_r($response, true).'</pre>';

		if (empty($response)) {
			throw new CException('Error while fetching OpenSesame courses');
		}

		$path = $response->getResponseData();
		if (empty($path)) {
			throw new CException('Invalid path');
		}

		//if no new packets are available on OpenSesame, a json string is returned instead of a zip file.
		//the json string is just '{"status":"success"}'
		if (filesize($path) <= 100) { //to make sure we have a json string, just check if the file is of a reasonable size
			$test_json = file_get_contents($path);
			$decoded = CJSON::decode($test_json);
			if (!empty($decoded)) {
				$this->renderPartial('import_packages', array(
					'already_sunchronized' => true
				));
				return;
			} else {
				//TODO: handle error
			}
		}

		$path_parts = pathinfo($path);
		$destFolderName = str_replace('.zip', '', $path_parts['basename']);
		$unpackPath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $destFolderName;
		// check if a previous upload (path) exists; create folder if not
		if (!file_exists($unpackPath)) {
			if ( ! mkdir($unpackPath, 0777) ) {
				throw new CException("Directory creation failed.");
			}
		}
		$unpackPath = realpath($unpackPath);


		// ZIP Management
		$zip = new ZipArchive();
		// Open the zip file
		$zip_open = $zip->open($path);

Yii::log("DEBUG OPENSESAME:\nfile path = $path\nfilesize = ".filesize($path)."\nunpack path = $unpackPath");

		if ($zip_open !== true) {
			// unable to open the zip file
			throw new CException('Unable to open zip file (Error: '.$zip_open.')');
		}

		// Checking if it is a valid scorm package
		if ($zip->locateName("metadata.json") === false) {
			// ok no imsmanifest, throw this away
			throw new CException('File \'metadata.json\' not found in the archive');
		}

		// Extract files in the zip archive
		if ($zip->extractTo($unpackPath) !== true) {
			throw new CException('Unable to extract zip archive');
		}
		$zip->close();


		//read metadata: there there is info about the packages contained in the file "courses.zip"
		$metadata_json = file_get_contents($unpackPath.DIRECTORY_SEPARATOR.'metadata.json');
		//Log::_( "TESTING: ".$metadata_json);

		$metadata = CJSON::decode($metadata_json);
		if (empty($metadata)) {
			throw new CException('Invalid metadata');
		}

		//start processing "courses.zip"
		$zip = new ZipArchive();
		// Open the zip file
		$zip_open = $zip->open($unpackPath.DIRECTORY_SEPARATOR.'courses.zip');
		if ($zip_open !== true) {
			// unable to open the zip file
			throw new CException('Unable to open zip file (Error: '.$zip_open.')');
		}

		// Extract files in the zip archive
		// NOTE: scorm package model automatically is looking for package files into "upload_tmp" folder
		if ($zip->extractTo(Docebo::getUploadTmpPath()) !== true) {
			throw new CException('Unable to extract zip archive');
		}
		$zip->close();

		/*
		Sample Metadata (Both for pull and push sync; can be customized per integration):
		[{
		"zipFilename" : "323dbb88904c4461add57e91cdb8c5a6.zip",
		"title" : "Taking Accountability",
		"expirationDate" : "20141129",
		"bundleName" : "Office Etiquette",
		"mobileCompatibility" : "allDevices",
		"externalId" : "323dbb88904c4461add57e91cdb8c5a6"
		},{
		"zipFilename" : "323dbb88904c4461add57e91cdb8c5a6.zip",
		"title" : "Mentoring 101",
		"expirationDate" : "20141129",
		"bundleName" : "Leadership",
		"mobileCompatibility" : "allDevices",
		"externalId" : "323dbb88904c4461add57e91cdb8c5a6"
		}]

		List of Available Metadata Fields:
		● Course Title
		● Seller (course provider)
		● Seat Time (duration, in minutes)
		● Description
		● Learning Objectives
		● Intended Audience
		● Subjects (categories)
		● Course features (audio, video, games, quizzes, simulation, etc.)
		● Course image (thumbnail)
		● System Requirements
		● Interface Language
		● Accredited By
		*/

		Yii::log('Scorm packages extracted ...', CLogger::LEVEL_INFO);

		//$result = OpenSesameHelper::saveScormPackages($metadata);

		//echo '<pre>'.print_r($result, true).'</pre>';

		$this->renderPartial('import_packages', array(
			'already_sunchronized' => false,
			'metadata' => $metadata,
			'metadata_json' => $metadata_json
		));
	}


	public function actionSaveContent() {
		try {
			$metadata_json = Yii::app()->request->getParam('metadata', false);
			if (empty($metadata_json)) {
				throw new CException('Invalid metadata');
			}
			$metadata = CJSON::decode($metadata_json);
			if (empty($metadata)) {
				throw new CException('Invalid metadata');
			}
			$result = OpenSesameHelper::saveScormPackages($metadata);
			echo CJSON::encode($result);
		} catch (Exception $e) {
			$output = array('success' => false, 'message' => $e->getMessage());
			echo CJSON::encode($output);
		}
	}


	/**
	 * This is just a testing function and will be removed
	 */
	public function actionTestingScormImport() {

		$metadata = array(
			array(
				'title' => 'maritime-test',
				'description' => 'just a test',
				'zipFilename' => 'maritime.zip'
			)
		);
		$result = OpenSesameHelper::saveScormPackages($metadata);

		echo '<pre>'.print_r($result, true).'</pre>';
	}


	public function actionDebugging() {
		if (Yii::app() instanceof CWebApplication) {
			// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
			if (!Yii::app()->request->isAjaxRequest) {
				//Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.min.js', CClientScript::POS_HEAD);
			}
		}
		$metadata = array();
		$metadata_json = '[]';
		$this->renderPartial('import_packages', array(
			'already_sunchronized' => false,
			'metadata' => $metadata,
			'metadata_json' => $metadata_json
		));
	}

}