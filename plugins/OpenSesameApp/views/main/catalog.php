<?php


$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	'OpenSesame',
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;


//----------

if (empty($tenant)) {
	echo '<p>'.'Invalid tenant'.'</p>';
} else if (empty($sso_url)) {
	echo '<p>' . 'Cannot login in OpenSesame marketplace' . '</p>';
} else {

?>

	<div class="row-fluid">
		<div class="span12">
			<h3 class="advanced-opensesame-settings-form-title">
				<?= Yii::t('opensesame', 'OpenSesame') ?>
			</h3>
		</div>
	</div>
	<br>

	<?php DoceboUI::printFlashMessages(); ?>

	<div class="opensesame-content">

		<div class="section">

			<iframe src="<?= $sso_url ?>" style="width: 100%; min-height: 700px; overflow: scroll;">
			</iframe>

		</div>

	</div>

	<?php

}
