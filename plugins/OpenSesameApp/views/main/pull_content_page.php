<!doctype html>
<html>
<head>
	<!--<meta http-equiv="X-UA-Compatible" content="IE=edge" />-->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= Yii::t('standard', '_LOADING') ?></title>
	<style>
		body{
			height:100%;
			width:100%;
			margin:0;
			padding: 30px 0 0 0;
			background-color: #f1f1f1; /*(241,241,241)*/
			text-align: center;
			font-family: 'Open Sans', Sans-Serif;
		}

		.pull-sync-page-container .big-text-container p {
			color: #333333;
			font-size: 22px;
		}

		.pull-sync-page-container .small-text-container p {
			color: #aaaaaa;
			font-size: 13px;
			font-weight: normal;
			line-height: 18px;
		}

		.pull-sync-page-container .buttons-container {
			padding-top: 30px;
		}

		.pull-sync-page-container .buttons-container a {
			display: inline-block;
			vertical-align: middle;
			text-decoration:none;
			cursor: pointer;
			font-size: 14px;
			text-transform: uppercase;
			padding: 10px 10px;
			min-width: 260px;
			text-align: center;
			box-shadow: none;
			font-weight: 600;
			/*line-height: 16px;*/
		}

		.pull-sync-page-container .buttons-container a.opensesame-continue-browsing {
			border-radius: 0px !important;
			webkit-border-radius: 0px !important;
			-moz-border-radius: 0px !important;

			border: 2px solid;
			border-color: #5FBF5F;
			background-color: #5FBF5F;
			color: #ffffff;
			/*text-shadow: 0 1px 0 #4EAE4F;*/
		}

		.pull-sync-page-container .buttons-container a.opensesame-redirect-after-sync {
			border-radius: 0px !important;
			webkit-border-radius: 0px !important;
			-moz-border-radius: 0px !important;

			border: 2px solid;
			border-color: #5FBF5F;
			background: transparent;
			color: #5FBF5F;
			/*text-shadow: 0 1px 0 #ccddcc;*/
		}
	</style>
</head>

<body>

<div class="pull-sync-page-container">

	<div class="image-container">
		<img src="<?= $this->module->getAssetsUrl().'/images/processing.gif' ?>" title="<?= Yii::t('standard', '_LOADING').' ...' ?>" />
	</div>

	<div class="big-text-container">
		<p>
			<?= Yii::t('opensesame', 'We are importing your courses into LMS') ?>
		</p>
	</div>

	<div class="small-text-container">
		<p>
			<?= Yii::t('opensesame', 'The synchronization process has been started successfully') ?>
		</p>
		<p>
			<?= Yii::t('opensesame', 'While we are transferring your courses in background, what would you like to do?') ?>
		</p>
	</div>

	<div class="buttons-container">
		<?= CHtml::link(
			Yii::t('opensesame', 'Continue browsing'),
			'#',//Docebo::createHydraUrl('marketplace', 'providers/details/opensesame'),
			array(
				'id' => 'continue-browsing-btn',
				'class' => 'opensesame-continue-browsing',
				'onclick' => 'window.top.location.href = "'.Docebo::createAbsoluteAdminUrl("marketplace/index").'";',
			)
		) ?>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<?= CHtml::link(
			Yii::t('opensesame', 'Go to Central LO repository'),
			'#',
			array(
				'id' => 'central-repo-btn',
				'class' => 'opensesame-redirect-after-sync',
				'onclick' => 'window.top.location.href = "'.Docebo::createAbsoluteLmsUrl("centralrepo/centralRepo/index").'";',
			)
		) ?>
	</div>

</div>

</body>
