<!doctype html>
<html>
<head>
	<!--<meta http-equiv="X-UA-Compatible" content="IE=edge" />-->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= Yii::t('standard', '_LOADING') ?></title>
	<style>
		body{
			height:100%;
			width:100%;
			margin:0;
			padding: 30px 0 0 0;
			background-color: #f1f1f1; /*(241,241,241)*/
			text-align: center;
			font-family: 'Open Sans', Sans-Serif;
		}

		.no-access-page-container .big-text-container p {
			color: #333333;
			font-size: 22px;
		}

		.no-access-page-container .small-text-container p {
			color: #aaaaaa;
			font-size: 13px;
			font-weight: normal;
			line-height: 18px;
		}

		.no-access-page-container .buttons-container {
			padding-top: 30px;
		}

		.no-access-page-container .buttons-container a {
			display: inline-block;
			vertical-align: middle;
			text-decoration:none;
			cursor: pointer;
			font-size: 14px;
			text-transform: uppercase;
			padding: 10px 10px;
			min-width: 260px;
			text-align: center;
			box-shadow: none;
			font-weight: 600;
			/*line-height: 16px;*/
		}

		.no-access-page-container .buttons-container a.opensesame-continue-browsing {
			border-radius: 0px !important;
			webkit-border-radius: 0px !important;
			-moz-border-radius: 0px !important;

			border: 2px solid;
			border-color: #5FBF5F;
			background-color: #5FBF5F;
			color: #ffffff;
			/*text-shadow: 0 1px 0 #4EAE4F;*/
		}

		.no-access-page-container .buttons-container a.opensesame-redirect-after-sync {
			border-radius: 0px !important;
			webkit-border-radius: 0px !important;
			-moz-border-radius: 0px !important;

			border: 2px solid;
			border-color: #5FBF5F;
			background: transparent;
			color: #5FBF5F;
			/*text-shadow: 0 1px 0 #ccddcc;*/
		}
	</style>
</head>

<body>

<div class="no-access-page-container">

	<div class="image-container">
		<img src="<?= $this->module->getAssetsUrl().'/images/no_access.png' ?>" title="<?= Yii::t('standard', '_OPERATION_FAILURE') ?>" />
	</div>

	<div class="big-text-container">
		<p>
			<?= Yii::t('standard', '_OPERATION_FAILURE') ?>
		</p>
	</div>

	<div class="small-text-container">
		<?php if (!empty($message)): ?>
		<p>
			<?= $message ?>
		</p>
		<?php endif; ?>
		<p>
			<?= Yii::t('opensesame', 'Please contact OpenSesame support in order to solve the issue'); ?>
		</p>
	</div>

	<div class="buttons-container">
		<?= CHtml::link(
			Yii::t('opensesame', 'Back to Marketplace'),
			'#',//Docebo::createHydraUrl('marketplace', 'providers/details/opensesame'),
			array(
				'id' => 'continue-browsing-btn',
				'class' => 'opensesame-continue-browsing',
				'onclick' => 'window.top.location.href = "'.Docebo::createAbsoluteAdminUrl("marketplace/index").'";',
			)
		) ?>
		<!--&nbsp;&nbsp;&nbsp;&nbsp;
		<?= CHtml::link(
			Yii::t('opensesame', 'Go to Central LO repository'),
			'#',
			array(
				'id' => 'central-repo-btn',
				'class' => 'opensesame-redirect-after-sync',
				'onclick' => 'window.top.location.href = "'.Docebo::createAbsoluteLmsUrl("centralrepo/centralRepo/index").'";',
			)
		) ?>-->
	</div>

</div>

</body>
