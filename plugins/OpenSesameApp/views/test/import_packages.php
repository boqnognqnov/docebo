<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, target-densitydpi=160dpi, minimal-ui">
	<!-- ,user-scalable=no - for safari bug -->
	<link rel="shortcut icon" href="<?php echo $this->faviconUrl; ?>" type="image/png" />

	<!-- base js -->
	<script type="text/javascript">var HTTP_HOST = '<?php echo Yii::app()->request->getBaseUrl(true).'/'; ?>';</script>

	<?php
	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);

	if(count($matches)<2)
	{
		preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
	}

	$jquery = 'jquery.js';
	$jquery_min = 'jquery.min.js';

	if (count($matches)>1)
	{
		$version = $matches[1];

		if($version == 8)
		{
			$jquery = 'jquery.old.js';
			$jquery_min = 'jquery.old.min.js';
		}
	}
	?>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? $jquery : $jquery_min?>"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/<?=YII_DEBUG ? 'jquery-migrate.js' : 'jquery-migrate.min.js'?>"></script>

	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/app.js?69"></script>
	<!--<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/menu.js"></script>-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php
	$cssToMerge = array();
	if (Lang::isRTL(Yii::app()->getLanguage())) {
		$cssToMerge[] = '/css/bootstrap-rtl.min.css';
		$cssToMerge[] = '/css/bootstrap-responsive-rtl.min.css';
//			$cssToMerge[] = '/css/rtl.css';  Commented out because breaks final merged file for some reason.
	}
	//$cssToMerge[] = '/css/admin.css';
	$cssToMerge[] = '/css/i-sprite.css';
	$cssToMerge[] = '/css/player-sprite.css';

	DoceboAssetMerger::publishAndRender($cssToMerge, true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);

	$cssToMerge = array();

	$cssToMerge[] = '/css/common.'. (YII_DEBUG ? '' : 'min.') . 'css';

	$enable_legacy_menu = Settings::get('enable_legacy_menu', 'on');
	if ($enable_legacy_menu == 'off') {
		$cssToMerge[] = '/css/new-menu.css';
	}


	DoceboAssetMerger::publishAndRender($cssToMerge, true, DoceboAssetMerger::$TARGET_THEME_ASSETS_FOLDER);
	?>
	<?php if(Lang::isRTL(Yii::app()->getLanguage())) { ?>
		<link href="<?= Yii::app()->theme->baseUrl ?>/css/rtl.css" rel="stylesheet">
	<?php } ?>
	<!-- template color scheme -->
	<link href="<?php echo Docebo::createLmsUrl('site/colorschemeCss', array('time' => time())) ?>" rel="stylesheet" id="colorSchemeCssFile">

	<link href="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.docebo.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/formstyler/jquery.formstyler.min.js"></script>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie.css" rel="stylesheet">
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/html5.js"></script>
	<![endif]-->

	<!--[if lt IE 8]>
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/css/ie6.css" rel="stylesheet">
	<![endif]-->

	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/respond.min.js"></script>
	<![endif]-->

	<!-- Open sans font -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<?= DoceboUI::custom_css(); ?>

	<!-- EVENT beforeHtmlHeadTagClose -->
	<?php Yii::app()->event->raise('beforeHtmlHeadTagClose', new DEvent($this, array())); ?>

	<!--[if IE 8]>
	<style>
		#player-quicknav-carousel .player-quicknav-object .object-data > span {margin-top:-8px\9\0;}
	</style>
	<![endif]-->
</head>
<body class="docebo">


<div class="container">
	<div id="importing-message-container" class="">
		<p id="importing-message">
			<?php
			if ($already_sunchronized) {
				echo Yii::t('opensesame', 'Packages already synchronized');
			} else {
				echo Yii::t('opensesame', 'Preparing package import').' ...';
			}
			?>
		</p>
	</div>

	<?php if (!$already_sunchronized): ?>
		<script type="text/javascript">

			(function ( $ ) {

				$.ajax({
					url: '<?= Docebo::createAbsoluteUrl('admin:OpenSesameApp/test/saveContent')?>',
					type: 'post',
					dataType: 'json',
					data: {
						metadata: <?= CJSON::encode($metadata_json) ?> //json info sent directly as a string
					},
					beforeSend: function () {
						$('#importing-message').html('<?= Yii::t('opensesame', 'Importing packages') ?> ...');
					}
				}).done(function(o) {
					if (o.success) {
						$('#importing-message').html('<?= Yii::t('standard', '_OPERATION_SUCCESSFUL') ?>');
					} else {
						$('#importing-message').html(o.message);
					}
				}).fail(function(o) {
					$('#importing-message').html('<?= Yii::t('standard', '_OPERATION_FAILURE') ?>');
				}).always(function(o) {

				});

			})($);

		</script>
	<?php endif; ?>
</div>

</body>
</html>
