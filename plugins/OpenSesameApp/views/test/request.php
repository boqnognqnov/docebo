<?php

$tenantId = '';
$tenantName = '';
$tenantType = 'Demo';

?>

<?= CHtml::beginForm(); ?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="opensesame-test-settings-form-title">
			<?= Yii::t('opensesame', 'Testing OpenSesame Teanat Provisioning') ?>
		</h3>
	</div>
</div>

<br>

<div class="advanced-main shopify-settings">

	<pre>
		<?php
		echo 'Integration level OAuth credentials:'."\n";
		echo '- key:'."\t\t".Settings::get('opensesame_oauth_key', '')."\n";
		echo '- secret:'."\t\t".Settings::get('opensesame_oauth_secret', '');
		?>
	</pre>

	<div class="section">

		<div class="row odd">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('opensesame', 'Tenant ID') ?>:
					<p class="setting-description"><?= Yii::t('opensesame', 'Unique identifier that requestor uses for this tenant') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('tenantId', $tenantId, array()) ?>
				</div>
			</div>
		</div>

		<div class="row even">
			<div class="row">
				<div class="setting-name public-key-setting-name">
					<?= Yii::t('opensesame', 'Tenant name') ?>:
					<p class="setting-description"><?= Yii::t('opensesame', 'the human readable identifier for this tenant') ?></p>
				</div>
				<div class="values">
					<?= CHtml::textField('tenantName', $tenantName, array()) ?>
				</div>
			</div>
		</div>

		<?= CHtml::hiddenField('tenantType', $tenantType); ?>

		<div class="row odd">
			<div class="row">
				<div class="row-fluid right" style="text-align: right">
					<?= CHtml::submitButton(Yii::t('opensesame', 'Start request'), array(
						'class' => 'btn btn-docebo green big',
						'name' => 'submit'
					)); ?>
					<?= CHtml::submitButton(Yii::t('standard', '_CANCEL'), array(
						'class' => 'btn btn-docebo black big',
						'name' => 'cancel'
					)); ?>
				</div>
			</div>
		</div>

	</div>
</div>
