<?php

class DashletPoweruserKPIs extends DashletWidget implements IDashletWidget{
	
	protected static $kpiList;
	

	/**
	 * Dashlet descriptor
	 *
	 * @return DashletDescriptor Object, describing the dashlet to the outside world
	 */
	public static function descriptor( $params = FALSE ) {
		$sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/pukpi_sample.jpg";

		$descriptor = new DashletDescriptor();

		$descriptor->name 					= 'core_dashlet_pu_kpis';
		$descriptor->handler				= 'plugin.PowerUserApp.widgets.DashletPoweruserKPIs';
		$descriptor->title					= 'Power User KPIs';
		$descriptor->description			= Yii::t('dashlets', 'Shows a list of Power Users statistics related to their users and courses');
		$descriptor->sampleImageUrl 		= $sampleImageUrl;
		$descriptor->settingsFieldNames		= array_merge(array('field_order'), array_keys(self::getSettings()));

		return $descriptor;
	}

	private static function getSettings() {
		return self::getKPIList();
	}

	/**
	 * Render dashlet settings, used in Admin Dashlets UI
	 */
	public function renderSettings() {
		$tmpAvailSettings = self::getSettings();

		$orderedSettings = array();

		// If editing a dashlet, restore custom ordering of fields, if set
		$fieldsOrdered = explode(',', $this->getParam('field_order'));
		if(!empty($fieldsOrdered)){
			foreach($fieldsOrdered as $field){
				if(in_array($field, array_keys($tmpAvailSettings))){
					$orderedSettings[$field] = $tmpAvailSettings[$field];
				}
			}
		}
		// Finally, add all settings not yet added by the ordered adding above.
		// On initial dashlet creation this array will just be populated with
		// the original settings in their original order
		foreach($tmpAvailSettings as $setting=>$label){
			if(!in_array($setting, array_keys($orderedSettings)))
				$orderedSettings[$setting] = $label;
		}

		$this->render('dashlet_pukpis_settings', array(
			'settings'=>$orderedSettings,
		));
	}

	/**
	 * Render fronend content.
	 *
	 * NOTE: Don't use Yii's CClientScript to register js/css resources
	 * here since it doesn't work. Use echo '<script src=...</script>' instead
	 */
	public function renderFrontEnd() {
		if(!Yii::app()->user->getIsPu()){
			echo Yii::t('dashlets', 'You are not a power user');
			return;
		}

		$fieldsOrdered = explode(',', $this->getParam('field_order'));

		if(empty($fieldsOrdered)){
			// Get default order
			$fieldsOrdered = array_keys(self::getSettings());
		}

		// Holds the HTML for each block to be rendered in the dashlet
		// Since blocks may be rendered side by side or in a single column
		// vertically depending on the dashlet cell layout (one half, one third, etc)
		// This is why we are building a reusable array, which can be easily split in
		// chunks before rendering when needed
		$blocks = array();

		foreach($fieldsOrdered as $settingName){

			if(!in_array($settingName, array_keys(self::getSettings())))
				continue; // This setting is not available in the platform

			if(!$this->getParam($settingName))
				continue; // Setting not enabled in dashlet configuration screen

			$block = array(
				'dataUrl' => Docebo::createLmsUrl('PowerUserApp/PowerUserDashlet/axGetDashletBlockHTML', array('settingName'=>$settingName)),
				'html' => null,
			);

			$blocks[] = $block;
		}

		//$twoElementBlocks = array_chunk($blocks, 2);

		$this->render('dashlet_pukpis', array(
			'blocks'=>$blocks,
		));
	}

	
	/**
	 * Returns list of KPIs
	 *
	 * @return array
	 */
	public static function getKPIList() {
	
		if (!empty(self::$kpiList)) {
			return self::$kpiList;
		}
	
		$list = array(
			'show_users'=>Yii::t('dashlets', 'Managed users'),
			'show_courses'=>Yii::t('dashlets', 'Managed courses'),
			'worst_score'=>Yii::t('dashlets', 'Course with the worst score among the managed courses'),
			'best_score'=>Yii::t('dashlets', 'Course with the best score among the managed courses'),
			'logged_in_users'=>Yii::t('dashlets', 'Managed users currently logged in the LMS'),
		);
		
		$kips = self::collectCustomKPIs();
		if (is_array($kips)) {
			foreach ($kips as $kpi => $label) {
				$list[$kpi] = $label;
			}
		}
	
		self::$kpiList = $list;
	
		return $list;
	}
	
	
	/**
	 * Run arbitrary code just before the main page (dashboard) is going to be loaded (rendered) IF the dashlet takes part of it
	 *
	 * <strong>Note 1</strong>: this method is called BEFORE rendering of the main page, as an attribute of an instance created BEFORE rendering!
	 * Which means, DURING the rendering of the dashlet, another object instance is created/used, having no relation to the first instance
	 * The first intance is purely created (and dismissed) for the porpose of bootstraping to allow, mainly, preloading client (browser) resources
	 *
	 * <strong>Note 2</strong>: Be aware, if you have 2 dashlets of the same type in the dashboard, bootstrap() will be called 2 times!!!
	 *
	 * <strong>Hint</strong>: Use this to execute Yii::app()->getClientScript()->registerXXXXX(<url>) to load dashlet specific own javascript/css
	 *
	 * @param bool|string $params
	 */
	public function bootstrap( $params = FALSE ) {

		$cs = Yii::app()->clientScript;
		$am = Yii::app()->assetManager;

		$cs->registerScriptFile(Yii::app()->getModule('player')->getAssetsUrl() . "/js/jquery.knob.js");
		$cs->registerScriptFile($am->publish(Yii::getPathOfAlias('mydashboard.assets')).'/js/kpi_dashlets_helper.js');
        $cs->registerCssFile   ($am->publish(Yii::getPathOfAlias('mydashboard.assets')).'/css/pukpi.css');
	}
	

	/**
	 * Collect Custom KPIs from whoever listens for the given event (e.g. a plugin)
	 * The event listener must return and array of id => label
	 *
	 * @return array
	 */
	public static function collectCustomKPIs() {
		$kpis = array();
		Yii::app()->event->raise('CollectCustomPowerUserKPIs', new DEvent(self, array(
			'kpis' => &$kpis
		)));
		return $kpis;
	}
	
	
	
}