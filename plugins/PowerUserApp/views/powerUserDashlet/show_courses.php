<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-objects large"></span>
</div>

<div class="left-padded">
	<p class="green large-number"><b><?= $count ?></b></p>
	<p><?= Yii::t( 'dashlets', 'Managed <b>courses</b>' ) ?></p>
</div>