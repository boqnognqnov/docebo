<? if($percent >= 0): // zero is a valid percent ?>
	<div class="pull-left knob-holder">
		<input type="text" class="auto-knob" value="<?= number_format($percent, 0) ?>" />
	</div>
<? endif; ?>
<p style="margin-top: 20px"><?= Yii::t( 'dashlets', 'Managed users <br/><b>currently logged in the LMS</b>' ) ?></p>