<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-file large"></span>
</div>

<div class="left-padded">
    <a href="<?=Docebo::createLmsUrl('/player/report', array('course_id'=>$idCourse))?>">
	    <p class="block-head tight"><b><?= $name ?></b></p>
    </a>
	<div><span class="red"><b><?=$score?></b></span>/100</div>
	<p><?= Yii::t( 'dashlets', 'Course with <b>worst score</b> among the managed courses' ) ?></p>
</div>