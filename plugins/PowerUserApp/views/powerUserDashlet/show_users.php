<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-couple large"></span>
</div>

<div class="left-padded">
	<p class="green large-number"><b><?= $count ?></b></p>
	<p class="pukpi-text"><?= Yii::t( 'dashlets', 'Managed <b>users</b>' ) ?></p>
</div>