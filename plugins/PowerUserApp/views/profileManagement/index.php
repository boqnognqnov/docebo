<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::t('power_users','Profiles management')
); ?>

<?php $this->renderPartial('_mainProfileActions'); ?>

<div class="bottom-section border-bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'profile-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $profileModel->dataProviderProfile(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'columns' => array(
				array(
	    			'name' => 'profile',
	    			'value' => '$data->relativeProfileId()',
                    'header' => Yii::t('profile', '_PROFILE')
				),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal edit-action",
						"data-toggle" => "modal",
						"data-modal-class" => "profile-edit-node",
						"data-modal-title" => "'.Yii::t('power_users', 'Power User Profile').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "PowerUserApp/profileManagement/edit&idst=$data->idst",
						"data-after-submit" => "updateProfileContent",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
				array(
					'name' => '',
					'type' => 'raw',
					'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "'.Yii::t('power_users', 'Delete Profile').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "PowerUserApp/profileManagement/delete&idst=$data->idst",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateProfileContent",
					));',
					'htmlOptions' => array('class' => 'button-column-single')
				),
			),
		)); ?>
	</div>
</div>
<script>

	function updateSelectionCountersPuFilter(index) {
		var countCheckedEvents = $('.permission-categories-accordion .accordion-content-' + index + ' .accordion-checkbox').filter(':checked').length;
		$('.permission-categories-accordion .ui-accordion-header').filter('.header-'+index).find('.selected-events').html(countCheckedEvents);

	};

	function 	updateArrowIconsInAccordionHeadersPuFilter(){
		var holders = $('.permission-categories-accordion .ui-accordion-header');
		holders.find('.toggler i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
		holders.filter('.ui-accordion-header-active').find('.toggler i').removeClass('fa-chevron-down').addClass('fa-chevron-up');

		$('.ui-accordion-header .pu-select-all-events').hide();
		$('.ui-accordion-header-active .pu-select-all-events').show();
	};

	function addCheckboxEvents(){
		$('.permission-categories-accordion .accordion-checkbox').change(function(){
			var container = $(this).closest('.ui-accordion-content');
			var index = container.data('index');
			updateSelectionCountersPuFilter(index);
		}).trigger('change');

		$('.pu-select-all-events a.select-all').on('click', function(){
			var index = $(this).data('index');
			$('.accordion-content-' + index + ' input[type="checkbox"]').prop('checked', true).trigger('refresh');
			updateSelectionCountersPuFilter(index);
		});


		$('.pu-select-all-events a.unselect-all').on('click', function(){
			var index = $(this).data('index');
			$('.accordion-content-' + index + ' input[type="checkbox"]').prop('checked', false).trigger('refresh');
			updateSelectionCountersPuFilter(index);
		});
	}


	function removeField() {
		var divToRemove = $(this).prev();
		var fieldId = divToRemove.data('value');
		console.log(fieldId);
		if(fieldId != undefined) {
			var hr = $(this).next();
			hr.remove();
			$(this).remove();
			divToRemove.remove();
			$('.modal.in #idField option[value="' + fieldId + '"]').show();
			$('input[type="hidden"][value="' + fieldId + '"]').remove();
		}
	}

	function addField(){
		var value = $('.modal.in #idField').val();
		var field = $('.modal.in #idField option[value="' + value + '"]');
		console.log(value);
		if (value != 0) {
			field.hide();
			$('.modal.in #idField').val(0);
			var div = $('<div>');
			div.text(field.text());
			div.attr('data-value', value);
			div.css({'width': '90.7%', 'display': 'inline-block'});
			var hidden = $('.hideField').first().clone();
			hidden.attr('value', value);
			var removeLink = $('<a href="#">');
			removeLink.attr('data-value', value);
			removeLink.addClass('i-sprite is-remove red removeNode');
			$('.modal.in #selectedFields').append(div).append(removeLink).append('<hr/>').append(hidden);
		}
	}

	$(document).on('click', '.modal.in .removeNode', removeField);

	$(document).on('click', '.modal.in #addField', addField);

</script>