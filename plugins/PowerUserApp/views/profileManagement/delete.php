<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'profile_form',
	)); ?>

	<p><?php echo Yii::t('standard', '_DEL') . ': "'. $model->relativeProfileId() .'" '.Yii::t('profile', '_PROFILE'); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'CoreGroup_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo $form->labelEx($model, Yii::t('standard', 'Yes, I confirm I want to proceed'), array('for' => 'CoreGroup_confirm_'.$checkboxIdKey)); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>