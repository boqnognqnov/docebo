

<style>
	.permissions-config .accordion-header{
		font-weight: bold;
		margin-bottom: 10px;
		font-size: 16px;
	}
	.permissions-config .ui-widget{
		font-family: 'Open Sans', sans-serif;
		font-size: 13px;
		border-bottom: 1px solid #ccc;
	}
	.permissions-config .ui-accordion .ui-accordion-header{
		border-radius: 0;
		border: none;
		border-top: 1px solid #ccc;
		padding: 0;
		margin: 0;
		background: #fff;
	}
	.permissions-config .ui-accordion .ui-accordion-header > div{
		padding: .5em .5em .5em .7em;
	}
	.permissions-config .ui-accordion .ui-accordion-content{
		border: none;
		border-bottom: 2px solid #fff;
		border-radius: 0;
	}
	.permissions-config .ui-accordion .ui-accordion-header.ui-accordion-header-active{
		font-weight: bold;
		border-bottom: none;
	}
	.permissions-config .ui-accordion .ui-accordion-header.ui-accordion-header-active > div{
		border-top: 1px solid #fff;
	}
	.permissions-config .ui-accordion .ui-accordion-header.ui-accordion-header-active,
	.permissions-config .ui-accordion .ui-accordion-content{
		background: #F1F3F2;
	}

	.permissions-config a.select-all,
	.permissions-config a.unselect-all{
		color: #0465AC !important;
		font-weight: 600 !important;
		text-decoration: underline !important;
	}

	.permissions-config .jq-checkbox.accordion-checkbox{
		margin-left: -27px !important;
	}

	.permissions-config .setting-description{
		margin-left: 21px !important;
	}


</style>
<div class="permissions-config">
	<div class="permission-categories-accordion">
	<?php
		$accordionContentIndex = 1;
	    $permissionsDesc = CoreRole::getPuPermissionDescription();
	?>

		<div class="header header-<?= $accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
			<div>
				<div class="pull-right">
					<?=Yii::t('audit_trail', '<span class="selected-events">{n}</span> selected')?>

					&nbsp;&nbsp;&nbsp;

												<span class="toggler">
													<i class="fa fa-chevron-down"></i>
												</span>
				</div>
				<?= Yii::t('standard', '_DETAILS'); ?>

				<span class="pu-select-all-events" data-index="<?= $accordionContentIndex ?>">
												&nbsp;
					<?php
					echo "<u>" . CHtml::link(Yii::t('standard' , "_SELECT_ALL"),"javascript:void();", array(
							'data-index' => $accordionContentIndex,
							'class'	=> 'select-all',
						)) ."</u>";

					echo " &nbsp; ";

					echo "<u>" . CHtml::link(Yii::t('standard' , "_UNSELECT_ALL"),"javascript:void();", array(
							'data-index' => $accordionContentIndex,
							'class'	=> 'unselect-all',
						)) ."</u>";
					?>
					&nbsp;
											</span>
			</div>
		</div>
		<div class="accordion-content-<?=$accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
			<div class="profile-name">
				<label for="CoreGroup_groupid"><?php echo Yii::t('menu', '_PUBLIC_ADMIN_RULES'); ?></label>
				<?php echo $form->textfield($model, 'groupid', array('value' => $model->relativeProfileId())); ?>
				<?php echo $form->error($model, 'groupid'); ?>
			</div>
			<hr/>
				<div class="row-fluid">
					<? $i=0; foreach($groupSettings as $key => $setting): ?>
					<?php $key = strtoupper(str_replace('.', '_', $setting->path_name)); ?>

						<div class="span6">
							<label class="checkbox">
								<?= CHtml::checkbox('CoreSettingGroup[' . $setting->path_name . '][value]', $setting->value == 'on' ? true : false, array(
										'class'=>'accordion-checkbox',
										'uncheckValue' => 0,
										'id' => 'prefix_'.$key
									));
								?>
								<?php switch($key) {
									case "ADMIN_RULES_DIRECT_COURSE_SUBSCRIBE" : echo Yii::t('adminrules', '_DIRECT_COURSE_SUBSCRIBE');break;
									case "ADMIN_RULES_DIRECT_USER_INSERT" : echo Yii::t('adminrules', '_DIRECT_USER_INSERT');break;
									case "ADMIN_RULES_ALLOW_ONLY_STUDENT_ENROLLMENTS" : echo Yii::t('adminrules', '_ALLOW_ONLY_STUDENT_ENROLLMENTS');break;
									case "ADMIN_RULES_ALLOW_BUY_SEATS" : echo Yii::t('adminrules', 'Allow power user to buy and assign seats for courses and learning plans'); break;
									case "ADMIN_RULES_MANAGE_SEAT_FOR_SUBSCRIPTION" : echo Yii::t('adminrules', 'Allow power user to buy and assign seats for subscriptions'); break;
									case "ADMIN_RULES_VIEW_ASSIGNED_USER_CERTIFICATES_ONLY" : echo Yii::t('adminrules', 'Allow power user to see certificates, issued to his assigned users only'); break;
									case "ADMIN_RULES_ALLOW_POWER_USERS_TO_SKIP_MANDATORY_FIELDS" : echo Yii::t('adminrules', 'Allow_power_users_to_skip_mandatory_fields_in_user_creation'); break;
								} ?>
							</label>

							<p class="setting-description styler indented">
								<?php switch($key) {
									case "ADMIN_RULES_DIRECT_COURSE_SUBSCRIBE" : echo Yii::t('adminrules', 'If checked, any learner enrolled to a course by this power user becomes active without superadmin approval.');break;
									case "ADMIN_RULES_DIRECT_USER_INSERT" : echo Yii::t('adminrules', 'If checked, any user this power user creates becomes active without superadmin approval.');break;
									case "ADMIN_RULES_ALLOW_ONLY_STUDENT_ENROLLMENTS" : echo Yii::t('adminrules', 'If checked, this power user can only enroll learners to courses.Otherwise, they can enroll both learners and instructors.');break;
									case "ADMIN_RULES_ALLOW_BUY_SEATS" : echo Yii::t('adminrules', 'If checked, this power user is able to purchase and assign seats for courses or learning plans that they manage.'); break;
									case "ADMIN_RULES_MANAGE_SEAT_FOR_SUBSCRIPTION" : echo Yii::t('adminrules', 'If checked, this power user is able to purchase and assign seats for subscriptions that they manage.'); break;
									case "ADMIN_RULES_VIEW_ASSIGNED_USER_CERTIFICATES_ONLY" : echo Yii::t('adminrules','If checked, this power user can see certificates that have been issued to their users from the certificates menu.'); break;
									case "ADMIN_RULES_ALLOW_POWER_USERS_TO_SKIP_MANDATORY_FIELDS" : echo Yii::t('adminrules', 'If checked, this power user can only manually create users and ignore additional fields marked as mandatory.'); break;
								} ?>
							</p>

						</div>
						<? $i++; ?>

				<? if($i==2): $i=0; // break rows ?>
				</div><!--.row-fluid end-->
				<br/>
				<div class="row-fluid">
					<? endif; ?>

					<? endforeach; ?>
				</div>



		</div>


	<? foreach($permGroups as $groupKey=>$group):  ?>
	<?php
		$accordionContentIndex++;
	?>

			<div class="header header-<?= $accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
				<div>
					<div class="pull-right">
						<?=Yii::t('audit_trail', '<span class="selected-events">{n}</span> selected')?>

						&nbsp;&nbsp;&nbsp;

												<span class="toggler">
													<i class="fa fa-chevron-down"></i>
												</span>
					</div>
					<?=$group['title']?>

					<span class="pu-select-all-events" data-index="<?= $accordionContentIndex ?>">
												&nbsp;
						<?php
						echo "<u>" . CHtml::link(Yii::t('standard' , "_SELECT_ALL"),"javascript:void();", array(
								'data-index' => $accordionContentIndex,
								'class'	=> 'select-all',
							)) ."</u>";

						echo " &nbsp; ";

						echo "<u>" . CHtml::link(Yii::t('standard' , "_UNSELECT_ALL"),"javascript:void();", array(
								'data-index' => $accordionContentIndex,
								'class'	=> 'unselect-all',
							)) ."</u>";
						?>
						&nbsp;
											</span>
				</div>
			</div>
			<div class="accordion-content-<?=$accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
				<div class="row-fluid">
					<? $i=0; foreach($roles[$groupKey] as $role): ?>

					<? if(!empty($role)) : ?>
						<div class="span6">
							<label class="checkbox">
								<?=  CHtml::checkBox('CoreRoleMembers['. $role['idst'] .']', $role['selected'], array(
										'class'=>'accordion-checkbox',
										'uncheckValue' => 0
									))?>
								<?= (isset($permissionsDesc[$role['role']]) && isset($permissionsDesc[$role['role']]['label']) && $permissionsDesc[$role['role']]['label']) ? $permissionsDesc[$role['role']]['label'] : "" ;?>
							</label>

								<p class="setting-description styler indented">
									<?= (isset($permissionsDesc[$role['role']]) && isset($permissionsDesc[$role['role']]['description']) && $permissionsDesc[$role['role']]['description'] )  ? $permissionsDesc[$role['role']]['description'] : ""?>
								</p>

						</div>
						<? $i++; ?>
					<? endif;?>

					<? if($i==2): $i=0; // break rows ?>
						</div><!--.row-fluid end-->
						<br/>
						<div class="row-fluid">
					<? endif; ?>

					<? endforeach; ?>
				</div>
			</div>

			<? endforeach; ?>

<!--		THIS FUNCTION IS ONLY FOR NEW LOGIC IN HYDRA PROJECT  !!!-->

		<? foreach($additionalPermissionData as $additionalPermission):  ?>
			<?php
			$accordionContentIndex++;
			?>

			<div class="header header-<?= $accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
				<div>
					<div class="pull-right">
						<?=Yii::t('audit_trail', '<span class="selected-events">{n}</span> selected')?>

						&nbsp;&nbsp;&nbsp;

												<span class="toggler">
													<i class="fa fa-chevron-down"></i>
												</span>
					</div>
					<?=$additionalPermission['title']?>

					<span class="pu-select-all-events" data-index="<?= $accordionContentIndex ?>">
												&nbsp;
						<?php
						echo "<u>" . CHtml::link(Yii::t('standard' , "_SELECT_ALL"),"javascript:void();", array(
								'data-index' => $accordionContentIndex,
								'class'	=> 'select-all',
							)) ."</u>";

						echo " &nbsp; ";

						echo "<u>" . CHtml::link(Yii::t('standard' , "_UNSELECT_ALL"),"javascript:void();", array(
								'data-index' => $accordionContentIndex,
								'class'	=> 'unselect-all',
							)) ."</u>";
						?>
						&nbsp;
											</span>
				</div>
			</div>


			<div class="accordion-content-<?=$accordionContentIndex ?>" data-index="<?= $accordionContentIndex ?>">
				<div class="row-fluid">
					<? $i=0;
					foreach($additionalPermission['items'] as $itemData):
					?>

					<? if(!empty($itemData)) : ?>
						<div class="span6">
							<label class="checkbox">
								<?=

								CHtml::checkBox('permissions['.$itemData['profile_name'].']['.$itemData['item_unique_name'].']', $itemData['selected'], array(
									'class'=>'accordion-checkbox',
									'uncheckValue' => 0
								));
								?>
								<?=$itemData['action_title'] ?>
							</label>

							<p class="setting-description styler indented">
								<?=$itemData['action_description'] ?>
							</p>

						</div>
						<? $i++; ?>
					<? endif;?>

					<? if($i==2): $i=0; // break rows ?>
				</div><!--.row-fluid end-->
				<br/>
				<div class="row-fluid">
					<? endif; ?>

					<? endforeach; ?>
				</div>
			</div>



		<? endforeach; ?>
	</div>
</div>



<script>





	$(function(){
		$('input').styler();
		$('.permission-categories-accordion').accordion({
			header: ".header",
			icons: false,
			heightStyle: "content",
			create: function(event, ui){
				updateArrowIconsInAccordionHeadersPuFilter();
			},
			activate: function(event, ui){
				updateArrowIconsInAccordionHeadersPuFilter();
			}
		});
		addCheckboxEvents();


	});
</script>