<style>
	#selectedFields hr{
		margin: 10px 0;
	}
</style>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'power-user-profile-form',
)); ?>

<?php echo $form->hiddenField($model, 'idst', array('value' => $model->idst)); ?>

<div class="powerUserProfile-tabs">
	<ul class="nav nav-tabs">
<!--		<li class="active">-->
<!--			<a class="details" data-target=".powerUserProfile-page-details" href="javascript:void(0);">-->
<!--				<span></span>--><?php //echo Yii::t('standard', '_DETAILS'); ?>
<!--			</a>-->
<!--		</li>-->
		<li class="active">
			<a class="permissions" data-target=".powerUserProfile-page-permissions-categories" href="javascript:void(0);">
				<span></span><?php echo Yii::t('standard', 'Permissions'); ?>
			</a>
		</li>
<!--		<li>-->
<!--			<a class="permissions" data-target=".powerUserProfile-page-permissions" href="javascript:void(0);">-->
<!--				<span></span>--><?php //echo Yii::t('standard', 'Permissions'); ?>
<!--			</a>-->
<!--		</li>-->
		<?php
		if(!empty($additionalFields)) {
		?>
		<li>
			<a class="fields" data-target=".powerUserProfile-page-fields" href="javascript:void(0);">
				<span></span><?php echo Yii::t('admin', 'Options'); ?>
			</a>
		</li>
		<?php }?>
		<li>
			<a class="assignment" data-target=".powerUserProfile-page-assignment" href="javascript:void(0);">
				<span></span><?php echo Yii::t('admin', 'Assignment'); ?>
			</a>
		</li>
	</ul>
	<div class="power-user-profile-form-wrapper">
		<div class="tab-content">
<!--			<div class="powerUserProfile-page-details tab-pane active">-->
<!--				<div class="profile-name">-->
<!--					<label for="CoreGroup_groupid">--><?php //echo Yii::t('menu', '_PUBLIC_ADMIN_RULES'); ?><!--</label>-->
<!--					--><?php //echo $form->textfield($model, 'groupid', array('value' => $model->relativeProfileId())); ?>
<!--					--><?php //echo $form->error($model, 'groupid'); ?>
<!--				</div>-->
<!--				<hr/>-->
<!--				<div class="settings">-->
<!--					--><?php //foreach ($groupSettings as $key => $setting): ?>
<!--						<p>-->
<!--							--><?php //$key = strtoupper(str_replace('.', '_', $setting->path_name)); ?>
<!--							--><?php //echo CHtml::checkbox('CoreSettingGroup[' . $setting->path_name . '][value]', $setting->value == 'on' ? true : false, array('uncheckValue' => 0, 'id' => 'prefix_'.$key)); ?>
<!--							<label for="--><?//='prefix_'.$key?><!--">-->
<!--								--><?php //switch($key) {
//									case "ADMIN_RULES_DIRECT_COURSE_SUBSCRIBE" : echo Yii::t('adminrules', '_DIRECT_COURSE_SUBSCRIBE');break;
//									case "ADMIN_RULES_DIRECT_USER_INSERT" : echo Yii::t('adminrules', '_DIRECT_USER_INSERT');break;
//									case "ADMIN_RULES_ALLOW_ONLY_STUDENT_ENROLLMENTS" : echo Yii::t('adminrules', '_ALLOW_ONLY_STUDENT_ENROLLMENTS');break;
//									case "ADMIN_RULES_ALLOW_BUY_SEATS" : echo Yii::t('adminrules', 'Allow power user to buy and assign seats for courses and learning plans'); break;
//									case "ADMIN_RULES_VIEW_ASSIGNED_USER_CERTIFICATES_ONLY" : echo Yii::t('adminrules', 'Allow power user to see certificates, issued to his assigned users only'); break;
//									case "ADMIN_RULES_ALLOW_POWER_USERS_TO_SKIP_MANDATORY_FIELDS" : echo Yii::t('adminrules', 'Allow_power_users_to_skip_mandatory_fields_in_user_creation').
//										"<div id='warningMsg' style='display:none'>".Yii::t('adminrules', 'Warning: the user should be asked to fill in mandatory information at first login. Set the option in the “Advanced settings“')."</div>"; break;
//								} ?>
<!--							</label>-->
<!--						</p><br/>-->
<!--					--><?php //endforeach; ?>
<!---->
<!--				</div>-->
<!--			</div>-->
			<div class="powerUserProfile-page-permissions-categories tab-pane active">
				<?php echo $this->renderPartial('_permissionsForm', array('permGroups'=>$permGroups, 'roles' => $roles,'additionalPermissionData'=>$additionalPermissionData ,'form' => $form,  'groupSettings' => $groupSettings , 'model' => $model), true , true); ?>
			</div>
<!--			<div class="powerUserProfile-page-permissions tab-pane">-->
<!--				<table id="power-user-permission">-->
<!--					<thead>-->
<!--						<tr>-->
<!--							<td>&nbsp;</td>-->
<!--							--><?php //foreach ($thead as $key => $title): ?>
<!--								<td class="--><?php //echo $key ?><!--"><span data-toggle="tooltip" title="--><?php //echo $title; ?><!--"></span></td>-->
<!--							--><?php //endforeach; ?>
<!--						</tr>-->
<!--					</thead>-->
<!--					<tbody>-->
<!--						--><?php //foreach ($permGroups as $key => $group): ?>
<!--							<tr>-->
<!--								<td>--><?php //echo $group['title']; ?><!--</td>-->
<!--								--><?php //foreach ($roles[$key] as $role): ?>
<!--									<td>-->
<!--										--><?php //echo !empty($role) ? CHtml::checkbox('CoreRoleMembers['. $role['idst'] .']', $role['selected'], array('uncheckValue' => 0)) : '&nbsp;'; ?>
<!--									</td>-->
<!--								--><?php //endforeach; ?>
<!--							</tr>-->
<!--						--><?php //endforeach; ?>
<!--					</tbody>-->
<!--				</table>-->
<!--				<script type="text/javascript">-->
<!--					$(function () {-->
<!--						$('#power-user-permission').find("thead td span").tooltip({-->
<!--							position: 'bottom'-->
<!--						});-->
<!---->
<!--						var targetCheckBox = $('#prefix_ADMIN_RULES_ALLOW_POWER_USERS_TO_SKIP_MANDATORY_FIELDS');-->
<!--						targetCheckBox.change(function () {-->
<!--							var element = $(this);-->
<!--							if (element.is(":checked")) {-->
<!--								$('#warningMsg').show();-->
<!--							} else {-->
<!--								$('#warningMsg').hide();-->
<!--							}-->
<!--						});-->
<!--					});-->
<!--				</script>-->
<!--			</div>-->
			<?php if(!empty($additionalFields)) { ?>
			<div class="powerUserProfile-page-fields tab-pane">
				<div class="profile-name">
					<div><h5 style="margin-bottom: 5px"><?php echo Yii::t('power_user_management', 'Pre-populated user fields'); ?></h5></div>
					<div><?php echo Yii::t('power_user_management', 'By selecting the following fields, all newly created users will inherit user fields values from the Power User creating them'); ?></div>
				</div>


				<div class="settings" style="margin: 20px 0">
					<p>
						<?php echo CHtml::dropDownList('', '', $additionalFields, array('id' => 'idField', 'style' => 'width: 90% !important; margin-right: 10px')); ?>
						<a href="#" class="i-sprite is-plus-solid green" id="addField"></a>
					</p><br/>
					<div id="selectedFields">
						<h6 style="margin-top: 20px;text-transform: uppercase"><?=Yii::t('standard', '_NAME')?></h6>
						<hr/>
						<input type="hidden" name="CoreGroup[additional_fields][]" class="hideField"/>
						<?php

						if (!empty($selectedFields)) {
							$selectedFields = array_filter($selectedFields);
							foreach ($selectedFields as $key => $selectedField) { ?>
								<div style="width: 90%; display: inline-block"
									 data-value="<?= $key ?>"><?= $selectedField ?></div>
								<a href="#" data-value="<?= $key ?>"
								   class="i-sprite is-remove red removeNode"></a>
								<hr/>
								<input type="hidden" name="CoreGroup[additional_fields][]" class="hideField"
									   value="<?= $key ?>"/>
							<?php }
						}
						?>
					</div>
				</div>

			</div>
			<?php } ?>

		<div class="powerUserProfile-page-assignment tab-pane">
			<div class="profile-name">
				<div><h5 style="margin-bottom: 5px"><?php echo Yii::t('power_user_management', 'Default Assignment'); ?></h5></div>
				<div><?php echo Yii::t('power_user_management', ''); ?></div>
			</div>
			<div class="settings" style="margin: 20px 0">
				<p>
					<?php echo CHtml::dropDownList('assignmentType', $model->assignment_type, $this->assignmentDropdownData(), array('id' => 'default_assignment', 'style' => 'width: 90% !important; margin-right: 10px')); ?>
				</p>
			</div>
		</div>

		</div>
	</div>
</div>

<?php $this->endWidget(); ?>
<script>
	var $selectedFields = JSON.parse('<?=addslashes(json_encode($selectedFields))?>');

	for (var i in $selectedFields) {
		$('#idField option[value="' + i + '"]').hide();
	}
</script>
