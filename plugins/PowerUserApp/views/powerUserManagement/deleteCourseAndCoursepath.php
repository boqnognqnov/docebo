<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p>
		<?php

			$_countCourses = count($courses);
			$_countCoursepaths = count($coursepaths);
			$_countTotal = $_countCourses + $_countCoursepaths + count($categories) + count($categoriesWithDescendants);

			$_firstName = '';
			if (!empty($courses)) {
				$_firstName = $courses[0]->name;
			} elseif (!empty($coursepaths)) {
				$_firstName = $coursepaths[0]->path_name;
			}

			$_tmp = array();
			if (!empty($courses)) { $_tmp[] = Yii::t('standard', '_COURSES'); }
			if (!empty($coursepaths)) { $_tmp[] = Yii::t('standard', '_COURSEPATH'); }
			if(!empty($categories) || !empty($categoriesWithDescendants)) $_tmp[] = Yii::t('standard', 'Categories');
			$_unassignText = implode(" / ", $_tmp);

			echo Yii::t('standard', '_UNASSIGN').': "'.$_countTotal.'" '.$_unassignText;
		?>
	</p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array($_countTotal, '{name}' => $_firstName)), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php
		echo CHtml::hiddenField('id', implode(',', $id));
	?>
	<?php $this->endWidget(); ?>
</div>