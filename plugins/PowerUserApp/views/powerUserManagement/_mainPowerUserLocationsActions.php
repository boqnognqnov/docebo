<div class="main-actions course-enrollments-actions clearfix">

	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('classroom', 'Assign locations').': '.Yii::app()->user->getRelativeUsername($user->userid); ?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('classroom', 'Assign locations'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignLocations', array('idst' => $user->idst)), array(
					'class' => 'assign-locations popup-handler',
					'alt' => Yii::t('helper', 'Power users assign locations - pu'),
					'data-modal-title' => Yii::t('classroom', 'Assign locations'),
					'data-modal-class' => 'assign-locations',
					'data-after-send' => 'powerUserAssignLocationAfterSubmit(data);',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>

</div>
