<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php
			$checked = in_array($data['idst'], Yii::app()->session['selectedItems']);
			echo '<input id="assign-power-user-management-list-checkboxes_'. /*$data->user_id */$data['idst']
				.'" type="checkbox" name="assign-power-user-management-list-checkboxes[]" value="'./*$data->user_id*/$data['idst']
				//.'" '.(($data->inSessionList('selectedItems', /*'user_id'*/'idst'))?'checked="checked"':'').'>';
				.'" '.($checked?'checked="checked"':'').'>';
		?>
	</div>

	<div class="group-member-username">
		<?php 
			
			switch ($data['type']) {
				case CoreAdminTree::TYPE_USER: {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('standard', '_USER');
					$_name = Yii::app()->user->getRelativeUsername($data['name']);
				} break;
				case CoreAdminTree::TYPE_GROUP: {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_GROUPTYPE_FREE');
					$_name = Yii::app()->user->getRelativeUsername($data['name']);
				} break;
				case CoreAdminTree::TYPE_ORGCHART: {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE');
					$_name = $data['name'];
				} break;
				case CoreAdminTree::TYPE_ORGCHART_DESC: {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE').' + '.Yii::t('organization_chart', '_ORG_CHART_INHERIT');
					$_name = $data['name'];
				} break;
				default:  {
					$_style = '';
					$_title = '';
					$_suffix = '';
					$_name = '';
				} break;
			}
			echo '<span class="'.$_style.'"'.($_title != '' ? ' title="'.$_title.'"' : '').'></span>';

			
			echo ($_name != '' ? $_name : '&nbsp;');
			echo ($_suffix != '' ? '&nbsp;('.strtolower($_suffix).')' : '');

		?>
	</div>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => 'Delete Assigned User',
				'linkTitle' => 'Delete',
				'url' => 'PowerUserApp/powerUserManagement/deleteAssignedUser&id='. /*$data->user_id*/$data['idst'],
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => 'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => 'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserAsignUsersContent',
			),
		)); ?>
	</div>
</div>