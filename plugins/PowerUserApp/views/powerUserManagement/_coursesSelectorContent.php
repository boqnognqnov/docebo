<div id="grid-wrapper" class="poweruser-course-table">
<h2><?=Yii::t('standard', 'Select courses')?></h2>
	<?php
		$columns = array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'poweruser-course-grid-checkboxes',
				'value' => '$data->idCourse',
				'checked' => 'in_array($data->idCourse, Yii::app()->session[\'poweruser_course_selector_filter\'])',
			),
			'code',
			array(
				'name' => 'name',
				'htmlOptions' => array('class' => 'wrapped'),
				'type' => 'raw'
			),
		);

		if (PluginManager::isPluginActive('ClassroomApp')) {
			$columns[] = 'course_type';
		}
	
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'poweruser-course-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProvider(),
			'cssFile' => false,
			'columns' => $columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
				'cssFile' => false,
			),
			'beforeAjaxUpdate' => 'function(id, options) {		
								options.type = "POST";
								if (options.data == undefined) {
									options.data = {
										contentType: "html",
										"LearningCourse[name]":$("#advanced-search-course").val(),
										selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
									}
								}
							}',
			'ajaxUpdate' => 'poweruser-course-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#poweruser-course-grid input").styler(); afterGridViewUpdate(id);}',
		));
	?>
</div>

