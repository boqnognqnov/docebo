<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($locations).'" '.Yii::t('classroom', 'Locations'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($locations), '{name}' => $locations[0]->name)), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php foreach ($locations as $location) {
		echo CHtml::hiddenField('id[]', $location->id_location, array('id' => false));
	} ?>
	<?php $this->endWidget(); ?>
</div>