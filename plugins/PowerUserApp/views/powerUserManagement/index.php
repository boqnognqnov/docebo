<style>
	.filters label{
		display: inline-block;
		margin-right: 10px;
	}
	div.filters span.jq-checkbox{
		margin: 0 5px !important;
	}
</style>
<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users'),
); ?>

<?php $this->renderPartial('_mainPowerUserActions'); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'power-user-management-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#power-user-management-grid'),
)); ?>

<div class="main-section additional-fields-wrapper">
	<div class="filters-wrapper">
        <div class="filters clearfix">
			<div style="padding: 1.7% 0; display: inline-block">
				<?= Yii::t('power_users', 'Show only Power Users with unassigned:') ?>
				<div style="display: inline-block; margin-left: 5px">
					<input type="checkbox" id="userProfile" name="filterEmpty[]" value="userProfile" <?=isset($_SESSION['filterEmpty']) && in_array('userProfile',$_SESSION['filterEmpty']) ? 'checked' : ''?>/>
					<label for="userProfile"><?= Yii::t('profile', '_PROFILE') ?></label>

					<input type="checkbox" id="users" name="filterEmpty[]" value="users" <?=isset($_SESSION['filterEmpty']) && in_array('users',$_SESSION['filterEmpty']) ? 'checked' : ''?>/>
					<label for="users"><?= Yii::t('standard', '_USERS') ?></label>

					<input type="checkbox" id="courses" name="filterEmpty[]" value="courses" <?=isset($_SESSION['filterEmpty']) && in_array('courses',$_SESSION['filterEmpty']) ? 'checked' : ''?>/>
					<label for="courses"><?= Yii::t('standard', '_COURSES') ?></label>

					<input type="checkbox" id="catalogs" name="filterEmpty[]" value="catalogs" <?=isset($_SESSION['filterEmpty']) && in_array('catalogs',$_SESSION['filterEmpty']) ? 'checked' : ''?>/>
					<label for="catalogs"><?= Yii::t('standard', 'Courses catalogs') ?></label>

					<input type="checkbox" id="locations" name="filterEmpty[]" value="locations" <?=isset($_SESSION['filterEmpty']) && in_array('locations',$_SESSION['filterEmpty']) ? 'checked' : ''?>/>
					<label for="locations"><?= Yii::t('classroom', 'Locations') ?></label>
				</div>
			</div>
			<div class="input-wrapper">
				<input autocomplete="off"
					   data-url="<?= Docebo::createAppUrl('admin:PowerUserApp/powerUserManagement/PowerUserAutocompleate') ?>"
					   id="advanced-search-power-user-management-grid" class="typeahead" type="text" name="filter"
					   placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" value="<?=$filter?>"/>
				<span class="search-icon"></span>
			</div>
        </div>
    </div>
</div>

<?php echo CHtml::submitButton('', array('id' => 'submitSearchCriteria', 'style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="bottom-section border-bottom-section clearfix">
	<div id="grid-wrapper">
		<?php
		$_columns = array();
		$_columns[] = array(
			'name' => 'userid',
			'header' => Yii::t('standard', '_USERNAME'),
			'value' => 'substr($data["userid"], 1)'
		);
		$_columns[] = array(
			'name' => 'fullname',
			'header' => Yii::t('standard', '_FULLNAME'),
			'value' => '$data["fullname"]'
		);
		$_columns[] = array(
			'type' => 'raw',
			'name' => 'groupid',
			'header' => Yii::t('profile', '_PROFILE'),
			'value' => function ($data) {
				$user = CoreUser::model()->findByPk($data['idst']);
				echo $user->renderPowerUserProfile();
			},
			'htmlOptions' => array('class' => 'power-users-profile'),
		);
		$_columns[] = array(
			'type' => 'raw',
			'name' => '',
			'value' => function ($data) {
				$user = CoreUser::model()->findByPk($data['idst']);
				echo $user->renderPowerUserAssignedEntities();
			},
			'htmlOptions' => array('class' => 'power-users-members'),
			'headerHtmlOptions' => array('class' => 'center-aligned'),
		);
		$_columns[] = array(
			'type' => 'raw',
			'name' => '',
			'value' => function ($data) {
				$user = CoreUser::model()->findByPk($data['idst']);
				echo $user->renderPowerUserCourses();
			},
			'htmlOptions' => array('class' => 'power-users-members'),
			'headerHtmlOptions' => array('class' => 'center-aligned'),
		);
		$_columns[] = array(
			'type' => 'raw',
			'name' => '',
			'value' => function ($data) {
				$user = CoreUser::model()->findByPk($data['idst']);
				echo $user->renderPowerUserCatalogs();
			},
			'htmlOptions' => array('class' => 'power-users-members'),
			'headerHtmlOptions' => array('class' => 'center-aligned'),
		);
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$_columns[] = array(
				'type' => 'raw',
				'name' => '',
				'value' => function ($data) {
					$user = CoreUser::model()->findByPk($data['idst']);
					echo $user->renderPowerUserLocations();
				},
				'htmlOptions' => array('class' => 'power-users-members'),
				'headerHtmlOptions' => array('class' => 'center-aligned'),
			);
		}

		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'power-user-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $powerUserModel->dataProviderPowerUser(),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'afterAjaxUpdate' => 'function(id, data){$(\'select\').styler();$(\'.powerUserProfileSelect\').hide();}',
			'columns' => $_columns
		));

		?>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {

		$('.search-icon').css("cursor", "pointer");
		$('.search-icon').css("cursor", "hand");
		$('.search-icon').on('click', function () {
			$('#submitSearchCriteria').trigger('click');
		});

		$('.powerUserProfileSelect').hide();
		autoSelectWidth();
		$('input, select').styler();
	});

	$(document).on('change', '#userProfile, #users, #courses, #catalogs, #locations', function () {
		$('#submitSearchCriteria').trigger('click');
	});
</script>
