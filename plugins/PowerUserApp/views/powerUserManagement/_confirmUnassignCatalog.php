<h1><?= Yii::t('standard', '_UNASSIGN') . ' ' . Yii::t('standard', '_CATALOGUE') ?></h1>
<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 18-Sep-15
 * Time: 4:40 PM
 * @var $model LearningCatalogue
 */
?>
<?= CHtml::beginForm('', 'POST', array('class' => 'ajax', 'id' => 'submitForm')) ?>
<div>
	<?php
	if (isset($count)) {
		echo Yii::t('standard', '_UNASSIGN') . ' <strong>' . $count . '</strong> ' . Yii::t('standard', '_CATALOGUE');?>
		<input type="hidden" value="<?= implode(',',$selectedCatalogs) ?>" name="selectedCatalogs">
	<?php } else {
		echo Yii::t('standard', '_UNASSIGN') . ' <strong>' . $model->name . '</strong>?';?>
		<input type="hidden" value="<?= $model->idCatalogue ?>" name="idCatalogue">
	<?php }
	?>
</div>

<input type="hidden" value="<?= $userId ?>" name="userId">
<div class="form-actions">
	<input class="btn confirm-btn" type="submit" value="<?= Yii::t('standard', '_CONFIRM'); ?>" name="submit"
		   data-submit-form="select-form"/>
	<input class="btn close-btn close-dialog" type="button" value="<?= Yii::t('standard', '_CLOSE'); ?>"/>
</div>
<?= CHtml::endForm() ?>