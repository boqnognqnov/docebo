<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'poweruser-location-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#poweruser-location-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="poweruser-location-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('lms:ClassroomApp/location/locationAutocomplete') ?>" class="typeahead" id="advanced-search-location" autocomplete="off" type="text" name="LtLocation[name]" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" data-poweruser-not="<?php echo $model->powerUserManager->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'poweruser-location-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(),
				'itemValue' => 'id_location',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'select-location-form',
	'htmlOptions' => array(
		'data-grid-items' => 'true',
	),
)); ?>


	<div id="grid-wrapper" class="poweruser-location-table">
	<h2><?php echo Yii::t('standard', '_SELECT'); ?></h2>
    <?php
		
		$columns = array(
				array(
					'class' => 'CCheckBoxColumn',
					'selectableRows' => 2,
					'id' => 'poweruser-location-grid-checkboxes',
					'value' => '$data->id_location',
					'checked' => 'in_array($data->id_location, Yii::app()->session[\'selectedItems\'])',
				),
				'name',
				'address',
				array(
					'name' => 'country',
					'header' => Yii::t('standard', '_COUNTRY'),
					'value' => '$data->country ? $data->country->name_country : ""',
				)
			);
			
	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'poweruser-location-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'columns' => $columns,
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
			'cssFile' => false,
		),
		'beforeAjaxUpdate' => 'function(id, options) {
							options.type = "POST";
							if (options.data == undefined) {
								options.data = {
									contentType: "html",
									selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
								}
							}
						}',
		'ajaxUpdate' => 'poweruser-location-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#poweruser-location-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
<?php $this->endWidget(); ?>

<div class="modal-footer">
	<input class="btn confirm-btn" type="button" value="<?= Yii::t('standard', '_CONFIRM');?>" name="submit" data-submit-form="select-location-form"/>
	<input class="btn close-btn" type="button" value="<?= Yii::t('standard', '_CLOSE');?>"/>
</div>
