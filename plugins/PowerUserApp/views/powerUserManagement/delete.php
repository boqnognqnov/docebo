<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'power_user_form',
	)); ?>

	<p><?php echo Yii::t('standard', '_DEL') . ': "'. Yii::app()->user->getRelativeUsername($model->userid) .'" '.Yii::t('standard', '_PUBLIC_ADMIN_USER'); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'CoreUser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo $form->labelEx($model, Yii::t('standard', 'Yes, I confirm I want to proceed'), array('for' => 'CoreUser_confirm_'.$checkboxIdKey)); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>