<style>
	.rightTd, .rightTh{
		text-align: right !important;
		width: 10% !important;
		padding-right:3% !important;
	}
	.rightTh{
		padding-right: 2% !important;
	}
</style>
<h1><?=Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'); ?></h1>
<div id="grid-wrapper">
	<?php $this->widget('common.widgets.ComboGridView', array(
		'gridId' => 'catalogue-management-grid',
		'dataProvider' => $catalogModel->dataProvider(null, $search, $assignedCatalogsIds),
		'massSelectorsInFilterForm' => true,
		'disableMassActions' => true,
		'gridTitleLabel' => Yii::t('standard', 'Select Courses Catalogs'),
		'massSelectorUrl' => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/catalogsSelectAll', array('catalogId' => $catalogModel->idCatalogue)),
		'columns' => array(
			array(
				'header' => '<input type="checkbox" class="select-on-check-all" />',
				'type' => 'raw',
				'value' => 'CHtml::checkBox("", false, array("value"=>"$data->idCatalogue", "class"=>"select-on-check"))',

				'htmlOptions' => array(
					'style' => 'width: 7%'
				),
			),
			array(
				'header'=> Yii::t('salesforce', 'Catalog Name'),
				'value'=>'$data->name',
			),
			array(
				'header'=> '<span class="i-sprite is-forum"></span>',
				'value'=>'$data->renderCatalogCourses(true, true);',
				'htmlOptions' => array(
					'class' => 'rightTd'
				),
				'headerHtmlOptions' => array(
					'class' => 'rightTh'
				),
			),
			array(
				'header'=> '<span class="i-sprite is-couple"></span>',
				'value'=>'$data->renderCatalogUsers(true, true);',
				'htmlOptions' => array(
					'class' => 'rightTd'
				),
				'headerHtmlOptions' => array(
					'class' => 'rightTh'
				),
			),
		),
	)); ?>
	<?= CHtml::beginForm(Docebo::createAdminUrl('PowerUserApp/powerUserManagement/editCatalogs'),'POST', array('class'=>'ajax', 'id'=>'submitForm')) ?>
	<div class="form-actions">
		<input class="btn confirm-btn" type="submit" value="<?= Yii::t('standard', '_CONFIRM'); ?>" name="submit"
			   data-submit-form="select-form"/>
		<input class="btn close-btn close-dialog" type="button" value="<?= Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
	<input type="hidden" value="<?=$powerUserModel->idst?>" id="idst"/>
	<?= CHtml::endForm() ?>
</div>

<script>
	$('#submitForm').submit(function(e){
		e.preventDefault();
		var selectedCatalogs = $('#catalogue-management-grid').comboGridView('getSelection');
		var data = {};
		data['selectedCatalogs'] = selectedCatalogs;
		data['idst'] = $('#idst').val();
		$.ajax({
			url:$('#submitForm').prop('action'),
			type:'post',
			data: {data:data}
		}).done(function(){
			$(".close-dialog").trigger('click');
			$('#catalogItems-grid').comboListView('updateListView');
		})
	});

//	$(document).on('combogridview.grid-ajax-updated', function (element, options) {
//		if (options.gridId == 'catalogue-management-grid') {
//			triggerCheckOnSelectedCatalogs();
//		}
//	})
	$('.combo-grid-deselect-all').trigger('click');


</script>