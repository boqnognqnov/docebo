<?php if (!empty($categoriesFancyTreeData)) : ?>
	<?php
	// Show Org Charts using Fancytree
	$id = 'categories-fancytree';
	$this->widget('common.extensions.fancytree.FancyTree', array(
		'id' => $id,
		'treeData' => $categoriesFancyTreeData,
		'preSelected' => $categoriesPreselected,
		'selectMode' => FancyTree::SELECT_MODE_DOCEBO,
		'formFieldNames' => array(
			'selectedInputName'	 	=> 'select-categories',
			'activeInputName' 		=> 'categories_active_node',
			'selectModeInputName'	=> 'categories_select_mode'
		),
	));
	?>

<?php endif; ?>