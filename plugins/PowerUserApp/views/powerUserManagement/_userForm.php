<div class="new-user-tabs">
	<ul class="nav nav-tabs" id="userFormTab">
		<li class="active">
			<a data-target="#userForm-details" href="#"><span class="user-form-details"></span>Details</a>
		</li>
		<li>
			<a data-target="#userForm-orgchat" href="#"><span class="user-form-orgchat"></span>Organization chart</a>
		</li>
	</ul>

	<div class="form new-user-form clearfix">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'user_form',
			'htmlOptions' => array(
				'enctype' => 'multipart/form-data',
			),
		)); ?>

		<div class="tab-content" id="userFormTabContent">
			<div class="tab-pane active" id="userForm-details">
				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'userid'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'userid'); ?>
						<?php echo $form->labelEx($user, 'userid'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'firstname'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'firstname'); ?>
						<?php echo $form->labelEx($user, 'firstname'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'lastname'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'lastname'); ?>
						<?php echo $form->labelEx($user, 'lastname'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'email'); ?>
					<div class="clearfix">
						<?php echo $form->textField($user, 'email'); ?>
						<?php echo $form->labelEx($user, 'email'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'new_password'); ?>
					<div class="clearfix">
						<?php echo $form->passwordField($user, 'new_password'); ?>
						<?php echo $form->labelEx($user, 'new_password'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($user, 'new_password_repeat'); ?>
					<div class="clearfix">
						<?php echo $form->passwordField($user, 'new_password_repeat'); ?>
						<?php echo $form->labelEx($user, 'new_password_repeat'); ?>
					</div>
				</div>

				<div class="clearfix force_change line-margin">
					<?php echo $form->error($user, 'force_change'); ?>
					<div class="clearfix">
						<?php echo $form->checkBox($user, 'force_change'); ?>
						<?php echo $form->labelEx($user, 'force_change'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($groupMember, 'idst'); ?>
					<div class="clearfix">
						<?php 
							$criteria = new CDbCriteria();
							$criteria->compare('groupid', '/framework/level/admin', true);
							$coreGroup = CoreGroup::model()->find($criteria);
						?>
						<?php echo $form->dropDownList($groupMember, 'idst', array($coreGroup->idst => Yii::t('admin_directory', '_DIRECTORY_' . $coreGroup->groupid))); ?>
						<?php echo $form->labelEx($groupMember, 'idst'); ?>
					</div>
				</div>

				<div class="clearfix line-margin">
					<?php echo $form->error($settingUser, 'value'); ?>
					<div class="clearfix">
						<?php echo $form->dropDownList($settingUser, 'value', CoreLangLanguage::getActiveLanguages()); ?>
						<?php echo $form->labelEx($user, 'language.value'); ?>
					</div>
				</div>

				<?php if (!empty($user->additionalFields)) { ?>
					<div class="additional-fields">
						<?php foreach ($user->additionalFields as $field) { ?>
							<div class="clearfix line-margin">
								<?php echo $field->renderField(); ?>
							</div>
						<?php } ?>
					</div>
				<?php } ?>

			</div>
			<!-- #userForm-details -->

			<div class="tab-pane" id="userForm-orgchat">
				<?php if (!empty($fullOrgChartTree)) { ?>
					<ul class='node-tree'>
						<?php $level = 1; ?>
						<?php foreach ($fullOrgChartTree as $node) {
							/*if ($node->idOrg == $model->idOrg) {
								continue;
							}*/
							$left = 10 * $node->lev;
							$rootNode = '';
							
							
							$hasChildren = 'hasChildren';
							// If node is a REAL leaf, set hasChildren to '' (NO children)
							if ($node->isLeaf()) {
								$hasChildren = '';
							}
							// Maybe this node is a "fake" leaf due to Power User assignments? (see CoreAdminTree::getPowerUserOrgChartsOnly())
							else if (in_array($node->idOrg, $puLeafs)) {
								$hasChildren = '';
							}
														
							
							if ($node->isRoot()) {
								$rootNode = 'rootNode';
							}
							?>
							<?php if ($node->lev < $level) { ?>
								<?php //for ($i = $node->lev; $i < $level; $i++) { echo '</ul></li>'; } ?>
								<?php for ($i = $node->lev; $i < $level; $i++) {
									echo '</ul>';
								} ?>
							<?php } ?>

						<li class="node nodeTree-node <?php echo $hasChildren.' '.$rootNode; ?>">
							<div>
								<?php if ($node->isRoot()) { ?>
									<span class="node-icon"></span>
								<?php } elseif (!$node->isLeaf()) { ?>
									<span class="open-close-link"></span>
								<?php } ?>
								<?php
								$htmlOptions = array(
									'value' => $node->idOrg,
									'id' => 'CoreUser_chartGroups_'.$node->idOrg,
								);
								if (in_array($node->idOrg, $user->getOrgChartGroupsList())) {
									$htmlOptions['checked'] = 'checked';
								}
								if ($node->lev > 1) {
									echo $form->checkBox($user, 'chartGroups[]', $htmlOptions);
								}
								echo $form->labelEx($user, 'chartGroups[]', array(
									'label' => $node->coreOrgChart->translation,
									'for' => 'CoreUser_chartGroups_'.$node->idOrg,
									'class' => 'label',
								));
								?>
							</div>
							<?php if (!empty($hasChildren)) { ?>
							<ul class="nodeUl">
						<?php } ?>

							<?php if (empty($hasChildren)) { ?>
								</li>
							<?php } ?>

							<?php $level = $node->lev; ?>
						<?php } ?>
					</ul>

					<!-- .node-tree -->
				<?php } ?>
			</div>
			<!-- #userForm-orgchat -->
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>