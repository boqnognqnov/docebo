<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS') . ': ' . Yii::app()->user->getRelativeUsername($userid);?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php
					$title 	= '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'); 
					$url 	= Docebo::createLmsUrl('usersSelector/axOpen', array(
								'type' 	=> UsersSelector::TYPE_POWERUSER_MANAGEMENT,
								'idUser' 	=> $id,	
							));
					
					echo CHtml::link($title, $url, array(
						'class' => 'open-dialog group-select-users',
						'rel' 	=> 'users-selector',	
						'alt' 	=> Yii::t('helper', 'Power users assign user'),
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_USERS'),
						'title'	=> Yii::t('standard', '_ASSIGN_USERS')
					)); 
				?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>


<script>
	/**
	 * The name of this function is hardcoded in UsersSelector JS as a callback upon successfully closing the dialog
	 *  Use in on your discretion to update current UI
	 */
	function usersSelectorSuccessCallback(data) {
		$.fn.yiiListView.update('assign-power-user-management-list');
	}
			
</script>
