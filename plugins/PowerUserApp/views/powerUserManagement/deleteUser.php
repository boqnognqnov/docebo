<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.$count.'" '.implode(', ', $what_del); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courses), '{name}' => $courses[0]->name)), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php
	if(is_array($models)) {
		$idstList = array();
		foreach ($models as $model) {
			if (is_object($model)) {
				$idstList[] = $model->idst;
			} else {
				$idstList[] = (int)$model; //we assume that it is a simple numeric ID
			}
		}
		echo CHtml::hiddenField('idst_list', implode(",", $idstList));
	} elseif (is_string($models)) {
		echo CHtml::hiddenField('idst_list', $models);
	}
	?>

	<?php $this->endWidget(); ?>
</div>