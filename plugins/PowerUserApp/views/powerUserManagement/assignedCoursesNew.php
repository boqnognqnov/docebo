<?php

$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::app()->user->getRelativeUsername($powerUserModel->userid),
	Yii::t('standard', '_ASSIGN_COURSES'),
);

?>

<?php $this->renderPartial('_mainPowerUserCoursesActions', array('user' => $powerUserModel)); ?>

<div class="selections clearfix">
	<?php $this->widget('common.widgets.ComboListView', array(
		'listId'=>'poweruser-courses-management-list',
		'dataProvider' => $courseModel->dataProviderPUManagement(),

		// Autocomplete related:
		'autocompleteRoute' => '/courseManagement/courseAutocomplete',
		'hiddenFields'=>array(
			'data[powerusermanager]'=>$powerUserModel->idst,
			'responseType'=>'cjuiautocomplete',
		),

		'massiveActions'	=> array('delete' => Yii::t('standard', '_UNASSIGN')),
		'listItemView'=>'plugin.PowerUserApp.views.powerUserManagement._viewCourseAndCoursepathNew',
		'afterAjaxUpdate'=>'courseAssignModeChanged();',
	)); ?>
</div>

<div class="grey-wrapper custom-course-assign-mode" style="display: none;">

</div>

<script type="text/javascript">
	var courseAssignModeChanged = function(){
		$('#poweruser-courses-management-list').hide();
		$('.custom-course-assign-mode').hide();
		$.getJSON('<?=Docebo::createAdminUrl('PowerUserApp/powerUserManagement/getAssignedCoursesMode')?>', {id: <?=$powerUserModel->idst?>}, function(data){
			if(data.mode==='<?=CoreAdminCourse::COURSE_SELECTION_MODE_ALL?>' || data.mode==='<?=CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS?>' || data.mode==='<?=CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS?>'  || data.mode==='<?=CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS?>' || data.mode==='<?=CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS?>'){
				$('#poweruser-courses-management-list').hide();
				$('.custom-course-assign-mode').show().html(data.label);
			}else{
				$('#poweruser-courses-management-list').show();
				$('.custom-course-assign-mode').hide().html('');
			}
		});
	};

	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();

			$(document).on('click', '.modal.assign-courses', function(e){
				$(this).closest('.modal').find('#categories-fancytree').fancytree("getTree").ext.docebo._generateFormElements(true);
			});
		});

		courseAssignModeChanged();
	})(jQuery);
</script>
