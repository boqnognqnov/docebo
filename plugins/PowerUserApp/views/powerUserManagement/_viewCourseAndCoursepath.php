<?php

if ($data['item_type'] == CoreAdminCourse::TYPE_COURSE):

?>

<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php 
			$_value = $data['id_item'].'_'.$data['item_type'];
			echo '<input id="poweruser-courses-management-list-checkboxes_' 
				. $_value . '" type="checkbox" name="poweruser-courses-management-list-checkboxes[]" value="' 
				. $_value . '" ' . (($this->inSessionListCoursePath('selectedItems', $_value)) ? 'checked="checked"' : '') . '>'; 
		?>
	</div>
	<div class="poweruser-courses-name"><?php echo $data['name']; ?></div>
    <?php if(PluginManager::isPluginActive('ClassroomApp')): ?>
        <div class="course-type">
			<?
			if ($data['course_type'] == LearningCourse::TYPE_CLASSROOM) {
				echo Yii::t('standard', '_CLASSROOM');
				echo '&nbsp;<i class="docebo-sprite ico-classroom" style="background-position: -252px -100px;"></i>';
			}
			else if ($data['course_type'] == LearningCourse::TYPE_WEBINAR) {
				echo Yii::t('webinar', 'Webinar');
				echo '&nbsp;<i class="icon webinar-icon-small-inline" style="vertical-align: middle; margin-left: 3px;"></i>';
			}
			else {
				echo Yii::t('course', '_COURSE_TYPE_ELEARNING');
				echo '&nbsp;<i class="docebo-sprite ico-elearning"></i>';
			}
			?>
		</div>
    <?php endif; ?>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => 'PowerUserApp/powerUserManagement/unassignCourses&id='. $data['id_item'],
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM')//'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL')//'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserCoursesList',
			),
		)); ?>
	</div>
</div>

<?php

elseif ($data['item_type'] == CoreAdminCourse::TYPE_COURSEPATH):

?>

<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php 
			$_value = $data['id_item'].'_'.$data['item_type'];
			echo '<input id="poweruser-courses-management-list-checkboxes_' 
				. $_value . '" type="checkbox" name="poweruser-courses-management-list-checkboxes[]" value="' 
				. $_value . '" ' . (($this->inSessionListCoursePath('selectedItems', $_value)) ? 'checked="checked"' : '') . '>'; 
		?>
	</div>
	<div class="poweruser-courses-name"><?php echo $data['name']; ?></div>
	<div class="course-type"><?= Yii::t('standard', '_COURSEPATH') ?><i class="<?= CoreAdminCourse::TYPE_COURSEPATH ?>"></i></div>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => 'PowerUserApp/powerUserManagement/unassignCoursepaths&id='. $data['id_item'],
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM')//'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL')//'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserCoursesList',
			),
		)); ?>
	</div>
</div>

<?php

elseif($data['item_type'] == CoreAdminCourse::TYPE_CATEGORY_DESC || $data['item_type'] == CoreAdminCourse::TYPE_CATEGORY):

	$label = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE');
	if($data['item_type'] == CoreAdminCourse::TYPE_CATEGORY_DESC){
		$label = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE') . ' + ' . Yii::t('organization_chart', '_ORG_CHART_INHERIT');
	}
?>

<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php
		$_value = $data['id_item'].'_'.$data['item_type'];
		echo '<input id="poweruser-courses-management-list-checkboxes_'
		     . $_value . '" type="checkbox" name="poweruser-courses-management-list-checkboxes[]" value="'
		     . $_value . '" ' . (($this->inSessionListCoursePath('selectedItems', $_value)) ? 'checked="checked"' : '') . '>';
		?>
	</div>
	<div class="poweruser-courses-name"><?php echo $data['name']; ?></div>
	<div class="course-type"><?= $label ?><i class="<?= $data['item_type'] ?>"></i></div>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => Docebo::createAbsoluteLmsUrl('PowerUserApp/powerUserManagement/unassignCategory', array('id'=>$data['id_item'], 'item_type'=>$data['item_type'])),
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM')//'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL')//'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserCoursesList',
			),
		)); ?>
	</div>
</div>

<?

else:

	//...

endif;

?>