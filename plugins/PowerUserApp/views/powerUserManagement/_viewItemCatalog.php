<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 17-Sep-15
 * Time: 2:27 PM
 */
$unassignUrl = Docebo::createAbsoluteLmsUrl('PowerUserApp/powerUserManagement/unassignCatalog', array('id' => $data['idCatalogue'], 'userId'=>$_GET['id']));

?>
<style>
	.items span {
		margin: 5px;
		display: inline-block;
	}

	.delete-button {
		position: static !important;
		display: inline-block;
	}
</style>
<div>
	<?= $data->name ?>
	<div class="pull-right items">
		<?= $data->renderCatalogCourses(true); ?>
		<?= $data->renderCatalogUsers(true); ?>

		<div class="delete-button">
			<?php echo CHtml::link('', $unassignUrl, array(
				'class' => 'open-dialog',
				'alt' => Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'),
				'data-modal-title' => Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'),
				'data-modal-class' => 'assign-courses',
			)); ?>
			<?php
//			$this->renderPartial('//common/_modal', array(
//				'config' => array(
//					'class' => 'delete-node',
//					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
//					'linkTitle' => 'Delete',
//					'url' => $unassignUrl,
//					'buttons' => array(
//						array(
//							'type' => 'submit',
//							'title' => Yii::t('standard', '_CONFIRM')
//						),
//						array(
//							'type' => 'cancel',
//							'title' => Yii::t('standard', '_CANCEL')
//						),
//					),
//					'afterSubmit' => 'updatePowerUserCoursesList',
//				),
//			));
			?>
		</div>
	</div>
</div>