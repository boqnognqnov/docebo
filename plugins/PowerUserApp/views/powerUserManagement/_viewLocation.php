<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo '<input id="poweruser-locations-management-list-checkboxes_' . $data->id_location 
			. '" type="checkbox" name="poweruser-locations-management-list-checkboxes[]" value="' . $data->id_location . '" ' 
			. (($data->inSessionList('selectedItems', 'id_location')) ? 'checked="checked"' : '') . '>'; ?>
	</div>
	<div class="poweruser-locations-name"><?php echo $data->name; $country = $data->country; if ($country) { echo '&nbsp;('.$country->name_country.')'; } ?></div>
	<!-- ... other location info ? ... -->
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => 'PowerUserApp/powerUserManagement/unassignLocations&id='. $data->id_location,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),//'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL')//'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserLocationsList',
			),
		)); ?>
	</div>
</div>