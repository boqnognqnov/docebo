<style>
	ul.clearfix li div a span {
		height: 37px !important;
		background-position: -318px -36px;
	}

	ul.clearfix li:hover div a span {
		background-position: -376px -35px;
	}

	div.info {
		min-height: 102px !important;
	}

</style>
<script>
	function setDefaultDropdown(){
		$("#catalogItems-grid_massive_action option[value='']").prop("selected",true);
	}
</script>
<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 17-Sep-15
 * Time: 10:47 AM
 */
$unassignUrl = Docebo::createAbsoluteLmsUrl('PowerUserApp/powerUserManagement/unassignCatalog', array('userId'=>$_GET['id']));
$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::app()->user->getRelativeUsername($powerUserModel->userid),
	Yii::t('standard', '_ASSIGN') . ' ' . Yii::t('standard', 'Courses catalogs'),

);

$this->renderPartial('_mainPowerUserCatalogActions', array('user' => $powerUserModel)); ?>

<div id="grid-wrapper">
	<?php
	$this->widget('common.widgets.ComboListView', array(
		'listId' => 'catalogItems-grid',
		'dataProvider' => $catalogModel->dataProvider(intval(Yii::app()->request->getParam('id')), $params['search']),

		// Autocomplete related:
//		'autocompleteRoute' => '/courseManagement/courseAutocomplete',
		'hiddenFields' => array(
			'data[powerusermanager]' => $powerUserModel->idst,
			'responseType' => 'cjuiautocomplete',
		),
		'afterAjaxUpdate' => 'setDefaultDropdown()',
		'massiveActions' => array('delete' => Yii::t('standard', '_UNASSIGN')),
		'listItemView' => 'plugin.PowerUserApp.views.powerUserManagement._viewItemCatalog',
	)); ?>

</div>

<script>
	$(function () {
		$('input').styler();
		$(document).controls();
		$('#catalogItems-grid_massive_action').change(function () {
			if ($(this).val() == 'delete') {
				var selectedCatalogs = $('#catalogItems-grid').comboListView('getSelection');
				var url = '<?=$unassignUrl?>';
				var divOpenDialog = $('<a class="open-dialog" id="test" href="'+url+'&selectedCatalogs='+selectedCatalogs.join(',')+'">');
				$(this).append(divOpenDialog);
				$(document).controls();
				divOpenDialog.trigger('click');
			}
		});
	})
</script>

