<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('power_users','Profiles management');?></span>
	</h3>
	<ul class="clearfix">
		<li>
        	<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-power-profile',
						'modalTitle' => 'Power user profile',
						'linkTitle' => '<span></span>New profile',
						'linkOptions' => array(
							'alt' => 'Lorem Ipsum bla bla bla'
						),
						'url' => 'PowerUserApp/profileManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
                                'title' => Yii::t('standard', '_CONFIRM'),
							),
							array(
								'type' => 'cancel',
                                'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						// 'afterLoadingContent' => 'courseFormInit(111);',
						// 'beforeSubmit' => 'beforeCourseSubmit',
						'afterSubmit' => 'updateProfileContent',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
	    <div>
			<h4 class="clearfix"></h4>
			<p></p>
	    </div>
	</div>
</div>