<div class="main-actions course-enrollments-actions clearfix">

	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_COURSES').': '.Yii::app()->user->getRelativeUsername($user->userid); ?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', '_ASSIGN_COURSES'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignCourses', array('idst' => $user->idst)), array(
					'class' => 'assign-courses popup-handler',
					'alt' => Yii::t('helper', 'Power users assign courses'),
					'data-modal-title' => Yii::t('standard', '_ASSIGN_COURSES'),
					'data-modal-class' => 'assign-courses',
					//'data-after-close' => 'newUserAfterClose();',
					'data-after-send' => 'powerUserAssignCourseAfterSubmit(data);',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>

</div>
