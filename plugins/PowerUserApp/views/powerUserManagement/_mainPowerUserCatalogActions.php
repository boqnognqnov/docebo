<div class="main-actions course-enrollments-actions clearfix">

	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs').': '.Yii::app()->user->getRelativeUsername($user->userid); ?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/assignCatalogs', array('idst' => $user->idst)), array(
					'class' => 'open-dialog',
					'alt' => Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'),
					'data-modal-title' => Yii::t('standard', '_ASSIGN').' '.Yii::t('standard', 'Courses catalogs'),
					'data-modal-class' => 'assign-courses',
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>

</div>
