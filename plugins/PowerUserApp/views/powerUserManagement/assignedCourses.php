<?php

$_gridView = '_viewCourseAndCoursepath';
$_dataProvider = $courseModel->dataProviderPUManagement();


$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::app()->user->getRelativeUsername($powerUserModel->userid),
	Yii::t('standard', '_ASSIGN_COURSES'),
);

?>

<?php $this->renderPartial('_mainPowerUserCoursesActions', array('user' => $powerUserModel)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'poweruser-courses-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#poweruser-courses-management-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>" class="typeahead" id="advanced-search-course" autocomplete="off" type="text" name="LearningCourse[name]" placeholder="<?= Yii::t('standard', '_SEARCH');?>" data-powerusermanager="<?php echo $powerUserModel->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'poweruser-courses-management-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $_dataProvider,
		'itemValue' => '$data[\'id_item\'] . "_" . $data[\'item_type\']',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="poweruser-courses-management-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
		</select>
	</div>
</div>

<div class="bottom-section">
	<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned courses').': '.Yii::app()->user->getRelativeUsername($powerUserModel->userid); ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'poweruser-courses-management-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $_dataProvider,
			'itemView' => $_gridView,
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'poweruser-courses-management-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				//console.log($(\'#\'+id+\'-selected-items-list\').val());
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
			'afterAjaxUpdate' => 'function(id, data) { $("#poweruser-courses-management-list input").styler();afterListViewUpdate(id);}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();

			$(document).on('click', '.modal.assign-courses', function(e){
				$(this).closest('.modal').find('#categories-fancytree').fancytree("getTree").ext.docebo._generateFormElements(true);
			});
			var fancyTreeSelector = '#orgcharts-fancytree';
			if ($(fancyTreeSelector).length > 0) {
				// Generate <input> form fields for selected branch nodes
				// This is a standard method of the Docebo FancyTree widget!
				// Form ID is set in enroll_user.php
				$(fancyTreeSelector).fancytree("getTree").ext.docebo._generateFormElements(false);
			}
		});
	})(jQuery);
</script>
