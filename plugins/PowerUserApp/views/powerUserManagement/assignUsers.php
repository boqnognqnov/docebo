<?php

$this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::app()->user->getRelativeUsername($powerUserModel->userid),
	Yii::t('standard', '_ASSIGN_USERS'),
);


//$this->renderPartial('_mainPowerUserAssignUsersActions', array('id' => $powerUserModel->idst, 'userid' => $powerUserModel->userid));
$this->renderPartial('_mainAssignUsersActions', array('id' => $powerUserModel->idst, 'userid' => $powerUserModel->userid));





$form = $this->beginWidget('CActiveForm', array(
	'id' => 'assign-power-user-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#assign-power-user-management-list'
	),
));

//$_dataProvider = $assignedUsers->dataProvider($powerUserModel->idst);

?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($assignedUsers, 'search_input', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
					'data-onlyUsers' => true,
					'data-onlyAssignedUsers' => true,
					'placeholder' => Yii::t('standard', '_SEARCH'),
					'data-source-desc' => 'true',
				)); ?>
				<button type="button" class="close clear-search">&times;</button>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'assign-power-user-management-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $assignedUsers->dataProvider($powerUserModel->idst),//$_dataProvider,
		'itemValue' => 'idst',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="assign-power-user-management-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
		</select>
	</div>
</div>

<div class="bottom-section">
<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned users').': '.Yii::app()->user->getRelativeUsername($powerUserModel->userid); ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'assign-power-user-management-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $assignedUsers->dataProviderPowerUserMembers($powerUserModel->idst),//$_dataProvider,
			'itemView' => '_view',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			/*'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),*/
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'assign-power-user-management-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				options.data = $("#assign-power-user-management-form").serialize();
				options.data += "&selectedItems=" + $(\'#\'+id+\'-selected-items-list\').val();
			}',
			'afterAjaxUpdate' => 'function(id, data) {
				$("#assign-power-user-management-list input").styler();

				if(!$(\'input[name="assign-power-user-management-list-selected-items-list"]\').val()){
					$("#assign-power-user-management-list-selected-items-list").val("");
					$("a.deselect-all").removeClass("active");
					$("a.select-all").addClass("active");
					$("a.deselect-this").removeClass("active");
					$("a.select-this").addClass("active");
				}

				updateSelectedCheckboxesCounter("assign-power-user-management-list");
				$.fn.updateListViewSelectPage();
			}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});

		//On Clear-Search icon click (x in the search form field)
		$('.input-wrapper .clear-search').on('click', function() {
			$('#advanced-search-group').val('');
			if ($.fn.yiiListView) {
				$.fn.yiiListView.update('assign-power-user-management-list');
			}
		});
	})(jQuery);
</script>
