<?
/* @var $idst integer The power user ID */
/* @var $courseSelectionMode string The result of CoreAdminCourse::getCourseAssignModeForPU($idst) */
$thisFormId = 'assign-courses-'.md5(microtime(1)).rand(1,10000);
$selectOptions = [
	CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD => Yii::t('standard', 'Select courses'),
	CoreAdminCourse::COURSE_SELECTION_MODE_ALL 		=> Yii::t('standard', 'Associate all courses'),
	CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS 		=> Yii::t('standard', 'Associate all Learning Plans'),
	CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS 	=> Yii::t('standard', 'Associate all courses and Learning Plans'),
	CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS 	=> Yii::t('standard', 'All courses / learning plans in visible catalogs'),
];
if(Settings::get('pu_course_assign_mode_visible_catalogs')) {
	$selectOptions[CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS] = Yii::t('standard', 'Associate all the courses from visible catalogs');
}

?>
<div id="<?=$thisFormId?>">
	<div class="grey-wrapper">
		<div class="pull-left">
			<?=CHtml::label(Yii::t('standard', 'Selection mode') . ': &nbsp;&nbsp;&nbsp; ' . CHtml::dropDownList(CoreAdminCourse::TYPE_COURSE_ASSIGN_MODE, $courseSelectionMode, $selectOptions), 'course_assign_mode', array('class' => 'clearfix')); ?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="users-selector">

		<div class="users-selector-tabs">

			<ul class="nav nav-tabs">
				<li class="active">
					<a data-target=".courses-tab" href="#">
						<span class="user-form-details"></span><?= Yii::t('standard', '_COURSES') ?>
					</a>
				</li>
				<?php if($includeCurriculas): ?>
				<li>
					<a data-target=".coursepaths-tab" href="#">
						<span class="user-form-details"></span><?= Yii::t('standard', '_COURSEPATH') ?>
					</a>
				</li>
				<?php endif; ?>
				<li>
					<a data-target=".categories-tab" href="#">
						<span class="user-form-details"></span><?= Yii::t('reservation', '_CATEGORY_CAPTION') ?>
					</a>
				</li>
			</ul>


			<div class="select-user-form-wrapper">

				<!-- FILTERS -->
				<div class="tab-content">

					<div class="courses-tab tab-pane active">
						<?php
							$this->renderPartial('_coursesSelectorFilter', array(
								'model' => $courseModel,
							));
						?>
					</div>

					<?php if($includeCurriculas): ?>
					<div class="coursepaths-tab tab-pane">
						<?php
							$this->renderPartial('_coursepathsSelectorFilter', array(
								'model' => $coursepathModel,
							));
						?>
					</div>
					<?php endif; ?>

					<div class="categories-tab tab-pane">
						<!-- No filter for Categoires available
						 so show an empty element here -->
					</div>

				</div>



				<!-- CONTENT -->
				<?php $form = $this->beginWidget('CActiveForm', array(
					//'id' => 'users-selector-form',
					'id' => 'select-form',
					'htmlOptions' => array(
						'data-grid-items' => 'true',
					),
				)); ?>

				<div class="tab-content">

					<div class="courses-tab tab-pane active">
						<?php
							$this->renderPartial('_coursesSelectorContent', array(
								'model' => $courseModel,
								'idst'=>$idst,
							));
						?>
					</div>

					<?php if($includeCurriculas): ?>
					<div class="coursepaths-tab tab-pane">
						<?php
							$this->renderPartial('_coursepathsSelectorContent', array(
								'model' => $coursepathModel,
							));
						?>
					</div>
					<?php endif; ?>

					<div class="categories-tab tab-pane">
						<?php
						$this->renderPartial('_categoriesSelectorTreeContent', array(
							'categoriesFancyTreeData' => $categoriesFancyTreeData,
							'categoriesPreselected' => $categoriesPreselected,
						));
						?>
					</div>

				</div>

				<?php $this->endWidget(); ?>

			</div>

		</div>

	</div>

	<div class="modal-footer">
		<input class="btn confirm-btn" type="button" value="<?= Yii::t('standard', '_CONFIRM');?>" name="submit" data-submit-form="select-form"/>
		<input class="btn close-btn" type="button" value="<?= Yii::t('standard', '_CLOSE');?>"/>
	</div>

</div>

<script>
	$(function(){
		var filterForm = $('.users-selector', '#<?=$thisFormId?>');

		filterForm.css('opacity', '0');

		var showOrHideCoursesGrid = function(){
			var selectedRadioVal = $('#course_assign_mode').val();
			if(selectedRadioVal !== '<?=CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD?>'){
				filterForm.hide();
			}else{
				filterForm.show().css('opacity', 1);
			}

			// Add a hidden input field with current course selection mode
			// so confirming the dialog actually submits the value
			var currentSelectGridForm = $('#select-form', '#<?=$thisFormId?>');
			var selectionModeHiddenInput = currentSelectGridForm.find('input[type=hidden].course-selection-type');
			if(selectionModeHiddenInput.length==0){
				selectionModeHiddenInput = $('<input type="hidden" name="course-selection-type"/>').addClass('course-selection-type');
				currentSelectGridForm.append(selectionModeHiddenInput);
			}
			selectionModeHiddenInput.val(selectedRadioVal);
		};


		$(document).on('change', '#<?=$thisFormId?> #course_assign_mode', function(){
			// every time the course picker mode changes,
			// show or hide the courses grid,
			// depending on which mode was selected
			showOrHideCoursesGrid();
		});
		setTimeout(function(){
			showOrHideCoursesGrid(); // call once on dialog open initially
		}, 500);
	});
</script>