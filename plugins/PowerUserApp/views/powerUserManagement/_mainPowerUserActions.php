<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', 'Power users'); ?></h3>
	<ul class="clearfix">
		<!--<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-user',
						'modalTitle' => 'Create a new power user',
						'linkTitle' => '<span></span>New power user',
						'linkOptions' => array(
							'alt' => 'Lorem Ipsum bla bla bla'
						),
						'url' => 'PowerUserApp/powerUserManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function() {OrgChartTree(); $(\'.modal.in\').find(\'#userForm-details, #userForm-orgchat\').jScrollPane({autoReinitialise: true});}',
						'afterSubmit' => 'updatePowerUserContent',
					),
				)); ?>
			</div>
		</li>-->
		<li>
			<div>
				<?php echo CHtml::link('<span></span>'.Yii::t('power_users','Profiles management'),
					Yii::app()->createUrl('PowerUserApp/profileManagement/index'),
					array(
						'alt' => Yii::t('helper','Profiles management'),
						'class' => 'new-power-profile'
					)); ?>
			</div>
		</li>

	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>

			<p></p>
		</div>
	</div>
</div>