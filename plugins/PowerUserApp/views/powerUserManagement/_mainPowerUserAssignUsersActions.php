<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('PowerUserApp/powerUserManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS') . ': ' . Yii::app()->user->getRelativeUsername($userid);?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'group-select-users',
						'modalTitle' => Yii::t('standard', '_ASSIGN_USERS'),
						'linkTitle' => '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'Power users assign user')
						),
						'url' => 'PowerUserApp/powerUserManagement/assignSelectUsers&id=' . $id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_ADD'),
								'formId' => 'select-form',
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function() {replacePlaceholder(); applyTypeahead($(\'.typeahead\')); visualizeOrgchartDescendants();}',
						'beforeSubmit' => 'initGroupMemberData',
						'afterSubmit' => 'updatePowerUserAsignUsersContent',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>