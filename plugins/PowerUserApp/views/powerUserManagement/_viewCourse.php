<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo '<input id="poweruser-courses-management-list-checkboxes_' . $data->idCourse . '" type="checkbox" name="poweruser-courses-management-list-checkboxes[]" value="' . $data->idCourse . '" ' . (($data->inSessionList('selectedItems', 'idCourse')) ? 'checked="checked"' : '') . '>'; ?>
	</div>
	<div class="poweruser-courses-name"><?php echo $data->name; ?></div>
    <?php if(PluginManager::isPluginActive('ClassroomApp')): ?>
        <div class="course-type"><?= $data->getCourseTypeTranslation() ?><i class="<?= $data->course_type ?>"></i></div>
    <?php endif; ?>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => 'PowerUserApp/powerUserManagement/unassignCourses&id='. $data->idCourse,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => 'Confirm',
					),
					array(
						'type' => 'cancel',
						'title' => 'Cancel',
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserCoursesList',
			),
		)); ?>
	</div>
</div>