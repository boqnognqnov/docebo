<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Power users') => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/index'),
	Yii::app()->user->getRelativeUsername($powerUserModel->userid),
	Yii::t('classroom', 'Assign locations'),
); ?>

<?php $this->renderPartial('_mainPowerUserLocationsActions', array('user' => $powerUserModel)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'poweruser-locations-management-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#poweruser-locations-management-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('lms:ClassroomApp/location/locationAutocomplete') ?>" class="typeahead" id="advanced-search-location" autocomplete="off" type="text" name="LtLocation[name]" placeholder="<?= Yii::t('standard', '_SEARCH');?>" data-powerusermanager="<?php echo $powerUserModel->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'poweruser-locations-management-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $locationModel->dataProvider(),
		'itemValue' => 'id_location',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="poweruser-locations-management-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
		</select>
	</div>
</div>

<div class="bottom-section">
	<h3 class="title-bold title-with-border"><?php echo Yii::t('classroom', 'Locations').': '.Yii::app()->user->getRelativeUsername($powerUserModel->userid); ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'poweruser-locations-management-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $locationModel->dataProvider(),
			'itemView' => '_viewLocation',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'poweruser-locations-management-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				//console.log($(\'#\'+id+\'-selected-items-list\').val());
				if (options.data == undefined) {
					options.data = {
						selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
					}
				}
			}',
			'afterAjaxUpdate' => 'function(id, data) { $("#poweruser-locations-management-list input").styler();afterListViewUpdate(id);$.fn.updateListViewSelectPage();}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);
</script>
