<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($courses).'" '.Yii::t('standard', '_COURSES'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($courses), '{name}' => $courses[0]->name)), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php foreach ($courses as $course) {
		echo CHtml::hiddenField('id[]', $course->idCourse, array('id' => false));
	} ?>
	<?php $this->endWidget(); ?>
</div>