<div id="grid-wrapper" class="poweruser-coursepath-table">
<h2>Select coursepaths</h2>
	<?php
		$columns = array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'poweruser-coursepath-grid-checkboxes',
				'value' => '$data->id_path',
				'checked' => 'in_array($data->id_path, Yii::app()->session[\'poweruser_coursepath_selector_filter\'])',
			),
			array(
				'name' => 'code',
				'header' => Yii::t('standard', '_CODE'),
				'value' => '$data->path_code',
			),
			array(
				'name' => 'name',
				'header' => Yii::t('standard', '_NAME'),
				'value' => '$data->path_name',
				'htmlOptions' => array('class' => 'wrapped'),
			)
		);
	
		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'poweruser-coursepath-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProvider(array(), false),
			'cssFile' => false,
			'columns' => $columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
				'cssFile' => false,
			),
			'beforeAjaxUpdate' => 'function(id, options) {
								options.type = "POST";
								if (options.data == undefined) {
									options.data = {
										contentType: "html",
										"LearningCoursepath[path_name]":$("#advanced-search-coursepath").val(),
										selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
									}
								}
							}',
			'ajaxUpdate' => 'poweruser-coursepath-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#poweruser-coursepath-grid input").styler(); afterGridViewUpdate(id);}',
		));
	?>
</div>