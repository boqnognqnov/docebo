<?php
	$itemTypeLabel = '';
	$unassignUrl = '';
	switch($data['item_type']) {
		case CoreAdminCourse::TYPE_COURSE:
			if (PluginManager::isPluginActive('ClassroomApp') && $data['course_type'] == LearningCourse::TYPE_CLASSROOM)
				$itemTypeLabel = Yii::t('standard', '_CLASSROOM')."&nbsp;&nbsp;".'<i class="docebo-sprite ico-classroom" style="background-position: -252px -100px;"></i>';
			else if ($data['course_type'] == LearningCourse::TYPE_WEBINAR)
				$itemTypeLabel = Yii::t('webinar', 'Webinar')."&nbsp;".'<i class="icon webinar-icon-small-inline" style="vertical-align: middle; margin-left: 3px;"></i>';
			else
				$itemTypeLabel = Yii::t('course', '_COURSE_TYPE_ELEARNING')."&nbsp;&nbsp;".'<i class="docebo-sprite ico-elearning"></i>';
			$unassignUrl = 'PowerUserApp/powerUserManagement/unassignCourses&id='. $data['id_item'];
			break;
		case CoreAdminCourse::TYPE_COURSEPATH:
			$itemTypeLabel = Yii::t('standard', '_COURSEPATH') ."&nbsp&nbsp;".'<i class="docebo-sprite ico-elearning"></i>';
			$unassignUrl = 'PowerUserApp/powerUserManagement/unassignCoursepaths&id='. $data['id_item'];
			break;
		case CoreAdminCourse::TYPE_CATEGORY_DESC:
		case CoreAdminCourse::TYPE_CATEGORY:
			$itemTypeLabel = Yii::t('standard', '_CATEGORY'). (($data['item_type'] == CoreAdminCourse::TYPE_CATEGORY_DESC) ? (' + ' . Yii::t('organization_chart', '_ORG_CHART_INHERIT')) : '')
				."&nbsp;&nbsp;".'<i class="fa fa-folder-open fa-lg"></i>';
			$unassignUrl = Docebo::createAbsoluteLmsUrl('PowerUserApp/powerUserManagement/unassignCategory', array('id'=>$data['id_item'], 'item_type'=>$data['item_type']));
			break;
	}
?>
<div style="position: relative; padding-right: 25px;">
	<div class="pull-right">
		<div class="course-type"><?= $itemTypeLabel ?></div>
	</div>
	<div class="poweruser-courses-name break-word"><?php echo $data['name']; ?></div>
	<div class="delete-button">
		<?php
		$this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => 'Delete',
				'url' => $unassignUrl,
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM')
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL')
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updatePowerUserCoursesList',
			),
		)); ?>
	</div>
</div>