<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': 1 '.Yii::t('standard', '_CATEGORY'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?=CHtml::hiddenField('id', $category['idCategory'])?>
	<?=CHtml::hiddenField('item_type', $category['type_of_entry'])?>
	<?php $this->endWidget(); ?>
</div>