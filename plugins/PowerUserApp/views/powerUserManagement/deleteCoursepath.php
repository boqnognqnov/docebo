<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.count($coursepaths).'" '.Yii::t('standard', '_COURSEPATH'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('confirm', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($coursepaths), '{name}' => $coursepaths[0]->path_name)), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?php foreach ($coursepaths as $coursepath) {
		echo CHtml::hiddenField('id[]', $coursepath->id_path, array('id' => false));
	} ?>
	<?php $this->endWidget(); ?>
</div>