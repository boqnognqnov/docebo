<?php
/**
 *
 *  @package common.components.job.handlers
 *
 */
class PowerUserJobHandler extends JobHandler implements IJobHandler {

	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'plugin.PowerUserApp.components.PowerUserJobHandler';


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Must be already assigned at upper level
		$job = $this->job;
		$jobParams = json_decode($job->params, true);

		if (!isset($jobParams['users']) && !isset($jobParams['courses']) && !isset($jobParams['learning_plans']) && !isset($jobParams['catalogs'])) {
			throw new Exception('Power user handler called with invalid parameters');
		}


		//do efffective job

		if (isset($jobParams['users'])) {
			$users = $jobParams['users'];

			$rs = CoreUserPU::updatePowerUsersOfUsers($users);
			if (!$rs) {
				throw new Exception('Power user handler error');
			}
		}

		if (isset($jobParams['courses'])) {
			$courses = $jobParams['courses'];

			$rs = CoreUserPuCourse::updatePowerUsersOfCourses($courses);
			if (!$rs) {
				throw new Exception('Power user handler error');
			}
		}

		if (isset($jobParams['learning_plans'])) {
			$learningPlans = $jobParams['learning_plans'];

			if (is_array($learningPlans)) {
				foreach ($learningPlans as $learningPlan) {
					$objs = CoreAdminCourse::model()->findAllByAttributes(array(
						'type_of_entry' => 'coursepath',
						'id_entry' => $learningPlan
					));
					if (!empty($objs)) {
						foreach ($objs as $obj) {
							CoreUserPuCourse::model()->updatePowerUserSelection($obj->idst_user);
						}
					}
				}
			}
		}

		if (isset($jobParams['catalogs'])) {
			$catalogs = $jobParams['catalogs'];

			if (is_array($catalogs)) {
				foreach ($catalogs as $catalog) {
					$objs = CoreAdminCourse::model()->findAllByAttributes(array(
						'type_of_entry' => 'catalogue',
						'id_entry' => $catalog
					));
					if (!empty($objs)) {
						foreach ($objs as $obj) {
							CoreUserPuCourse::model()->updatePowerUserSelection($obj->idst_user);
						}
					}
				}
			}
		}

	}


	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


}
