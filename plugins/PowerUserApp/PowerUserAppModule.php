<?php

class PowerUserAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'PowerUserApp.models.*',
			'PowerUserApp.components.*',
			'PowerUserApp.controllers.*',
			'PowerUserApp.widgets.*'
		));

		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'providePuDashlet'));
		Yii::app()->event->on('EventForSkipMandatoryFields', array($this, 'eventForSkipMandatoryFields'));
		Yii::app()->event->on('NewUserCreated', array($this, 'eventNewUserCreated'));
		Yii::app()->event->on('NewUserCreatedSelfreg', array($this, 'eventNewUserCreatedSelfreg'));

		Yii::app()->event->on(EventManager::EVENT_NEW_COURSE_ADDED, array($this, 'eventNewCourse'));
		Yii::app()->event->on(EventManager::EVENT_NEW_COURSE, array($this, 'eventNewCourse'));
		Yii::app()->event->on(EventManager::EVENT_COURSE_PROP_CHANGED, array($this, 'eventNewCourse')); //course category may have been changed
		Yii::app()->event->on(EventManager::EVENT_COURSE_DELETED, array($this, 'eventCourseDeleted'));
		if (PluginManager::isPluginActive('CurriculaApp')) {
			Yii::app()->event->on('NewContentInLearningPlan', array($this, 'eventNewContentInLearningPlan'));
		}
	}

	/**
	* This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	*/
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('powerUser', array(
			'icon' => 'admin-ico poweruser',
			'app_icon' => 'fa-user-secret', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//PowerUserApp/powerUserManagement/index'),
			'label' => Yii::t('standard', '_PUBLIC_ADMIN_USER'),
		), array());
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else
			return false;
	}

	public function providePuDashlet(DEvent $event){
		if(!isset($event->params['descriptors'])){
			return;
		}

		$dashletDescriptorPuKPIs = DashletPoweruserKPIs::descriptor();

		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor) {
				$collectedSoFarHandlers[] = $descriptorObj->handler;
			}
		}

		if(!in_array($dashletDescriptorPuKPIs->handler, $collectedSoFarHandlers)){
			// Add the MyBlogApp dashlet to the event params
			// so the Blog dashlet is available everywhere
			$event->params['descriptors'][] = $dashletDescriptorPuKPIs;
		}

	}

	public function eventForSkipMandatoryFields(DEvent $event)
	{
		$idUser = Yii::app()->user->getId();
		$coreSettingId = Yii::app()->db->createCommand()->
		select('cgm.idst')->
		from('core_group_members as cgm')->
		join('core_setting_group as csg', 'cgm.idst=csg.idst')->
		where('cgm.idstMember=:idUser', array(':idUser' => $idUser))->queryScalar();
		if ($coreSettingId) {
			$stateOfOption = Yii::app()->db->createCommand()->
			select('value')->
			from('core_setting_group')->
			where('path_name = "admin_rules.allow_power_users_to_skip_mandatory_fields"')->
			andWhere('idst = :coreSettingId', array(':coreSettingId' => $coreSettingId))->
			queryScalar();
			if ($stateOfOption === 'on') {
				$event->preventDefault();
			}
		}
	}

	/**
	 * Create a power user updating job on the server
	 * @param $users a list of users IDs : the power users owning these users will be updated
	 * @return bool|CoreJob the created job (false if no job has been created)
	 */
	public static function setUpdateJob($users) {

		if (is_numeric($users) && intval($users) > 0) {
			$users = array(intval($users));
		}
		if (!is_array($users) || empty($users)) {
			return false;
		}

		//make sure that array values are valid and strict int values (the latter is useful when storing data as json string in DB)
		array_walk($users, function(&$value) { $value = (int)$value; });

		Yii::app()->scheduler->createImmediateJob('update_power_user_selection', PowerUserJobHandler::id(), array('users' => $users));
	}




	public function eventNewUserCreated($event) {

		$user = $event->params['user'];

		//prevent potential errors: $user should be a CoreUser object
		if (get_class($user) != 'CoreUser') {
			return false;
		}

		switch ($user->scenario) {
			case 'powerUserCreateTempUser':
				//no real user created yet, but only a temporary user by power user with no permission to directly create users
				break;
			case 'import':
				// Progressive operation executing ... we are not going to handle this case here. All users will be collected by
				// the importer and then a single job will be created consequently.
				// Since Imports may involve big number of users. avoid creating a lot of jobs for every single user (otherwise
				// the server may collapse).
				break;
			case 'create': //creation of user by administration
				//no need to set up a job for a single user (usually creation by admin or registration)
				//we need immediate update for notification too
				CoreUserPU::updatePowerUsersOfUsers($user->idst);
				break;
			default:
				//standard user creation: set up a job to update power users assignments
				self::setUpdateJob($user->idst);
				break;
		}

	}


	public function eventNewUserCreatedSelfreg($event) {

		$user = $event->params['user'];

		//prevent potential errors: $user should be a CoreUser object
		if (get_class($user) != 'CoreUser') {
			return false;
		}

		//no need to set up a job for a single user (usually creation by admin or registration)
		//we need immediate update for notification too
		CoreUserPU::updatePowerUsersOfUsers($user->idst);
	}





	public function eventNewCourse($event) {

		$course = $event->params['course'];

		if (get_class($course) != 'LearningCourse') {
			return false;
		}

		//no need to set up a job for a single course (usually creation by admin)
		//we need immediate update for notifications too
		CoreUserPuCourse::updatePowerUsersOfCourses($course->idCourse);
	}


	public function eventCourseDeleted($event) {

		$course = $event->params['course'];

		if (get_class($course) != 'LearningCourse') {
			return false;
		}

		//no need to set up a job for a single course (usually creation by admin)
		//we need immediate update for notifications too
		CoreUserPuCourse::model()->deleteAllByAttributes(array(
			'course_id' => $course->idCourse
		));
	}


	public function eventNewContentInLearningPlan($event) {

		$learningPlan = $event->params['learningPlan'];

		if (get_class($learningPlan) != 'LearningCoursepath') {
			return false;
		}

		Yii::app()->scheduler->createImmediateJob('update_power_user_selection', PowerUserJobHandler::id(), array('learning_plans' => array($learningPlan->id_path)));
	}

}
