
<?php
class ProfileManagementController extends AdminController {
	
	
	const ALLOW_PU_TO_SKIP_MANADATORY_FIELDS = 'admin_rules.allow_power_users_to_skip_mandatory_fields';
	const GENERAL_ALLOW_BUY_SEATS = 'general/allow_buy_seats';
	
	
	/**
	 * Get data for dropdown 
	 * @return array
	 */
	public function assignmentDropdownData() {
		$selectOptions = [
			CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD => Yii::t('standard', 'Select courses'),
			CoreAdminCourse::COURSE_SELECTION_MODE_ALL 		=> Yii::t('standard', 'Associate all courses'),
			CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS 		=> Yii::t('standard', 'Associate all Learning Plans'),
			CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS 	=> Yii::t('standard', 'Associate all courses and Learning Plans'),
			CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS 	=> Yii::t('standard', 'All courses / learning plans in visible catalogs'),
		];
		if(Settings::get('pu_course_assign_mode_visible_catalogs')) {
			$selectOptions[CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS] = Yii::t('standard', 'Associate all the courses from visible catalogs');
		}
		
		return $selectOptions;
	}







	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}

	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/adminmanager/view'),
			'actions' => array(
				'index',
				'edit',
				'create',
				'delete',
			),
		);

		// Deny by default
		$res[] = array('deny', // if some permission is wrong -> the script goes here
			'users' => array('*'),
			'deniedCallback' => array($this, 'accessDeniedCallback'),
			'message' => Yii::t('course', '_NOENTER'),
		);

		return $res;
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$profileModel = new CoreGroup;
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->clientScript->getCoreScriptUrl().
			'/jui/css/base/jquery-ui.css'
		);
		$profileModel = new CoreGroup();
		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreGroup'])) {
			$profileModel->attributes = $_REQUEST['CoreGroup'];
		}
		$this->render('index', array(
			'profileModel' => $profileModel,
		));
	}

	public function actionCreate() {
		$profileModel = new CoreGroup;

		$result = array();
		$selectedRoles = array();
		$selectedSettings = array();
		if (Yii::app()->request->isAjaxRequest) {
			
			if ( !empty($_POST['assignmentType']) ) {
				$profileModel->assignment_type = $_POST['assignmentType'];			
			}
			
			if (!empty($_POST['CoreGroup'])) {

				if(!empty($_POST['CoreGroup']['additional_fields'])){
					$_POST['CoreGroup']['additional_fields'] = implode(',', array_filter($_POST['CoreGroup']['additional_fields']));
				}
				$profileModel->attributes = $_POST['CoreGroup'];
				$profileModel->hidden = true;

				foreach ($_POST['CoreRoleMembers'] as $roleId => $value) {
					if($value == 1){
						$selectedRoles[$roleId] = $roleId;
					}
				}

				foreach($_POST['CoreSettingGroup'] as $pathName => $pathValue){
					if($pathValue['value'] == 1){
						$selectedSettings[$pathName] = $pathName;
					}
				}

				if ($profileModel->validate()) {
					$profileModel->groupid = $profileModel->absoluteProfileId();

					if ($profileModel->save()) {
						foreach ($_POST['CoreSettingGroup'] as $key => $setting) {
							$s = new CoreSettingGroup;
							$s->idst = $profileModel->idst;
							$s->path_name = $key;
							$s->value = $setting['value'] == 0 ? 'off' : 'on';
							$s->save();
						}

						foreach ($_POST['CoreRoleMembers'] as $roleId => $toggle) {
							if ($toggle == 1) {
								$row = new CoreRoleMembers;
								$row->idst = $roleId;
								$row->idstMember = $profileModel->idst;
								$row->save();
							}
						}

						/*
						 * Right Before Send the result to the User
						 * Sync the Data with the Yii2 RBAC
						 */
						HydraRBAC::syncPermissionsTable($_POST);
						
						
						// Add perimission to hydra
						if ( !empty($_POST['CoreSettingGroup']['admin_rules.allow_power_users_to_skip_mandatory_fields']['value']) ) {
							$this->_createRbac(self::ALLOW_PU_TO_SKIP_MANADATORY_FIELDS, $profileModel->groupid);
						}
						
						if ( !empty($_POST['CoreSettingGroup']['admin_rules.allow_buy_seats']['value']) ) {
							$this->_createRbac(self::GENERAL_ALLOW_BUY_SEATS, $profileModel->groupid);
						}
						

						$this->sendJSON($result);
					}
				}
			}

			$additionalPermissionData = $this->getAdditionalPermissionData();
			$additionalFields = $this->getAdditionalFieldsToPopulate();
			$result['html'] = $this->renderPartial('_form',
				array(
					'model' => $profileModel,
					'additionalPermissionData' => $additionalPermissionData,
					'thead' => CoreRole::getRolesInfo(),
					'roles' => CoreRole::initRoles($selectedRoles),
					'permGroups' => CoreRole::getRoleIds(),
					'groupSettings' => CoreSettingGroup::initSettings(null, array(), $selectedSettings),
					'additionalFields' => $additionalFields,
				), true);
			$this->sendJSON($result);
		}
	}



	public function actionEdit($idst) {
		$profileModel = CoreGroup::model()->findByPk($idst);
		$coreRoles = CoreRoleMembers::model()->findAllByAttributes(array('idstMember' => $idst));
		$coreRolesIds = CHtml::listData($coreRoles, 'idst', 'idstMember');

		$result = array();
		if ($profileModel && Yii::app()->request->isAjaxRequest) {
			
			if ( !empty($_POST['assignmentType']) ) {
				$profileModel->assignment_type = $_POST['assignmentType'];			
			}
			
			
			
		//LOGIC FOR UPDATE PERMISSIONS
			$oldGroupName = $profileModel->groupid;

			if (!empty($_POST['CoreGroup'])) {
				$profileModel->idst = $_POST['CoreGroup']['idst'];
				$profileModel->groupid = $_POST['CoreGroup']['groupid'];

				if(!empty(array_filter($_POST['CoreGroup']['additional_fields'])))
					$profileModel->additional_fields = implode(',', array_filter($_POST['CoreGroup']['additional_fields']));
				else $profileModel->additional_fields = null;
				$criteria = new CDbCriteria;
				$criteria->compare('idst', $idst);
				$criteria->addInCondition('path_name', array_keys($_POST['CoreSettingGroup']));
				$criteria->index = 'path_name';

				// Preload the models and index them in key-based array for easier retrieval
				$models = CoreSettingGroup::model()->findAll($criteria);

				foreach ($_POST['CoreSettingGroup'] as $key => $setting) {
					$s = isset($models[$key]) ? $models[$key] : null;

					if (empty($s)) {
						$s = new CoreSettingGroup;
						$s->idst = $idst;
						$s->path_name = $key;
					}

					$s->value = $setting['value'] == 0 ? 'off' : 'on';
					if(!$s->save()){
						Yii::log('Can not save PU profile checkboxes: '.print_r($s->getErrors(), 1) );
					}
				}

				$coreRolesIds = (!empty($coreRolesIds) ? array_keys($coreRolesIds) : array());
				$selected = array();
				// PERMISSION FIELD USED IN HYDRA. BACKWARDS COMPATIBILITY
				$permissions = array_values($_POST['permissions']);
				$permissionKeys = array_keys($permissions[0]);
				
				$permissionIds = CoreRole::model()->findAllByAttributes(array('roleid' => $permissionKeys));
				$final = array();
				foreach($permissionIds as $key=>$value) {
					$final[$value['idst']] = $permissions[0][$value['roleid']];
				}
				$postedFields = $_POST['CoreRoleMembers'] + $final; // MERGE OLD AND NEW PERMISSIONS
				foreach ($postedFields as $roleId => $toggle) {
					if ($toggle == 0) {
						if (in_array($roleId, $coreRolesIds)) {
							$toDelete[] = $roleId;
						}
					} else {
						$selected[] = $roleId;
					}
				}

				$toCreate = (!empty($selected) ? array_diff($selected, $coreRolesIds) : array());

				if (!empty($toDelete)) {
					CoreRoleMembers::model()->deleteAllByAttributes(array('idst' => $toDelete, 'idstMember' => $idst));
				}

				foreach ($toCreate as $roleId) {
					$row = new CoreRoleMembers;
					$row->idst = $roleId;
					$row->idstMember = $idst;
					$row->save();
				}

				if ($profileModel->validate()) {
					$profileModel->groupid = $profileModel->absoluteProfileId();
					if ($profileModel->save()) {

						/*
						 * Right Before Send the result to the User
						 * Sync the Data with the Yii2 RBAC
						 */
						HydraRBAC::syncPermissionsTable($_POST,$oldGroupName);
						
						// Add perimission to hydra
						if ( !empty($_POST['CoreSettingGroup']['admin_rules.allow_power_users_to_skip_mandatory_fields']['value']) ) {
							$this->_createRbac(self::ALLOW_PU_TO_SKIP_MANADATORY_FIELDS, $profileModel->groupid);
						} else {
							$this->_removeRbac(self::ALLOW_PU_TO_SKIP_MANADATORY_FIELDS, $profileModel->groupid);
						}
						
						if ( !empty($_POST['CoreSettingGroup']['admin_rules.allow_buy_seats']['value']) ) {
							$this->_createRbac(self::GENERAL_ALLOW_BUY_SEATS, $profileModel->groupid);
						} else {
							$this->_removeRbac(self::GENERAL_ALLOW_BUY_SEATS, $profileModel->groupid);
						}
			
						
						$this->sendJSON(array());
					}
				}
			}
			//LOGIC FOR SHOW EDIT WINDOW
			$additionalFields = $this->getAdditionalFieldsToPopulate();
			$selectedFields = explode(',', $profileModel->additional_fields);
			foreach($selectedFields as $key=>$selectedField) {
				if (array_key_exists($selectedField, $additionalFields)) {
					unset($selectedFields[$key]);
					$selectedFields[$selectedField] = $additionalFields[$selectedField];
				}
			}

			$additionalPermissionData=$this->getAdditionalPermissionData($profileModel->groupid);

			$result['html'] = $this->renderPartial('_form',
					array(
							'model' => $profileModel,
							'thead' => CoreRole::getRolesInfo(),
							'roles' => CoreRole::initRoles($coreRolesIds),
							'permGroups' => CoreRole::getRoleIds($idst),
						    'additionalPermissionData'=>$additionalPermissionData,
							'additionalFields' => $additionalFields,
							'selectedFields' => $selectedFields,
							'groupSettings' => CoreSettingGroup::initSettings($idst, array(
									'admin_rules.direct_course_subscribe',
									'admin_rules.direct_user_insert',
									'admin_rules.allow_only_student_enrollments',
									'admin_rules.allow_buy_seats',
									'admin_rules.manage_seat_for_subscription',
									'admin_rules.allow_power_users_to_skip_mandatory_fields',
									'admin_rules.view_assigned_user_certificates_only',
							)),
					), true);
			$this->sendJSON($result);
		}
	}

	//THIS FUNCTION IS ONLY FOR NEW LOGIC IN HYDRA PROJECT  !!!
	private function getAdditionalPermissionData($profileName = '')
	{
		$additionalPermData = CoreRole::$rbac_permissions;
		$dataToReturn = [];

//GET ALLOWED ACTIONS FOR THIS USER LIKE "course/create"
		$allowedActions = [];

		if (!empty($profileName)) {
			$sql = 'SELECT rbac_item_child.child 
						FROM rbac_item_child 
						WHERE rbac_item_child.parent LIKE "' . $profileName . '"';

			$connection = Yii::app()->db;
			$allowedActions = $connection->createCommand($sql)->queryColumn();
		}

		$counter = 0;
		foreach ($additionalPermData as $targetName => $targetData) {
			$dataToReturn[$counter]['target'] = $targetName;
			$dataToReturn[$counter]['title'] = $targetData['title'];

			$counter2 = 0;
			foreach ($targetData['items'] as $actionRealName => $actionData) {

				//$dataToReturn[$counter]['items'][$counter2]['action_real_name'] = $actionRealName;
				$dataToReturn[$counter]['items'][$counter2]['action_title'] = Yii::t('adminrules', $actionData['title']);

				$rbacItemName = $targetName . '/' . $actionRealName;


				$sql = 'SELECT rbac_item.description 
						FROM rbac_item 
						WHERE rbac_item.name LIKE "' . $rbacItemName . '"';

				$connection = Yii::app()->db;
				$actionDescription = $connection->createCommand($sql)->queryRow();
				$dataToReturn[$counter]['items'][$counter2]['action_description'] = Yii::t('adminrules', $actionDescription['description']);

				//////////////////

				$dataToReturn[$counter]['items'][$counter2]['selected'] = in_array($rbacItemName, $allowedActions);

				$items = [];

				$dataToReturn[$counter]['items'][$counter2]['profile_name'] = $profileName;
				$dataToReturn[$counter]['items'][$counter2]['item_unique_name'] = $rbacItemName;

				$items[$profileName][$rbacItemName];
				$dataToReturn[$counter]['items'][$counter2]['id'] = $items;


				$counter2++;
			}

			$counter++;
		}

		return $dataToReturn;
	}
	public function actionDelete($idst) {
		$coreGroup = CoreGroup::model()->findByPk($idst);
		if (empty($coreGroup)) {
			$this->sendJSON(array());
		}

		if (!empty($_POST['CoreGroup'])) {
			if ($_POST['CoreGroup']['confirm']) {

				/*
				 * Before sending the response to the user
				 * let's sync the with the Hydra RBAC tables
				 */
				HydraRBAC::deleteRole($idst);

				CoreRoleMembers::model()->deleteAllByAttributes(array('idstMember' => $idst));
				CoreSettingGroup::model()->deleteAllByAttributes(array('idst' => $idst));
				$coreGroup->delete();
				$this->sendJSON(array());
			}
		}
		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('delete', array(
				'model' => $coreGroup,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	private function getAdditionalFieldsToPopulate()
    {
        /** @var CoreUserField $field */
		$field = new CoreUserField;
		$fields = $field->getAllAdditionalFields();
		$additionalFields = array(0 => Yii::t('standard', '_SELECT').' '.Yii::t('standard', '_FIELD_NAME'));

		foreach ($fields as $field) {
            /**
             * @todo Is fetching the model instances again one at a time necessary? Don't think so, but leaving it here
             *       just in case...
             */
            /** @var CoreUserField $field */
			$fieldModel = CoreUserField::model()->findByAttributes(array(
                'id_field' => $field->getFieldId()
			));

			if ($fieldModel->getFieldType() != CoreUserField::TYPE_UPLOAD) {
				$additionalFields[$field->getFieldId()] = $field->getTranslation();
			}
		}

		$additionalFields['language'] = Yii::t('standard', '_LANGUAGE');

		return $additionalFields;
	}

	/**
	 * Add RBAC perimission to hydra
	 * @param str $itemName
	 * @param str $profileName
	 */
	private function _createRbac($itemName, $profileName) {
		$hasRbac = Yii::app()->getDb()->createCommand()
			->select('child')
			->from('rbac_item_child')
			->where('child = :itemName AND parent = :profileName', array(':itemName' => $itemName, ':profileName' => $profileName))	
			->queryAll();
				
		if ( !$hasRbac ) {
			$cmd = Yii::app()->db->createCommand();
			$cmd->insert(
				'rbac_item_child',
				array(
					'child' => $itemName,
					'parent' => $profileName,
				)
			);
		}
	}
	
	/**
	 * remove RBAC permission
	 * @param str $itemName
	 * @param str $profileName
	 */
	private function _removeRbac($itemName, $profileName) {
		$cmd = Yii::app()->db->createCommand();
		$cmd->delete(
				'rbac_item_child', 
				'child = :itemName AND parent = :profileId',
				array(
					':itemName' => $itemName,
					':profileId' => $profileName
				)
			);
	}
	
	
}
