<?php

class PowerUserDashletController extends Controller {

	public function actionAxGetDashletBlockHTML($settingName = null){

		$params = array();
		$kpi = $settingName;

		switch($kpi){
			case 'show_users':
				$params['count'] = Yii::app()->getDb()->createCommand()
				                                   ->select('COUNT(u.idst)')
				                                   ->from(CoreUser::model()->tableName().' u')
				                                   ->join(CoreUserPU::model()->tableName().' pu', 'u.idst=pu.user_id AND pu.puser_id=:idUser')
				                                   ->bindValues(array(
					                                   ':idUser'=>Yii::app()->user->getIdst()
				                                   ))->queryScalar();
				break;
			case 'show_courses':
				$params['count'] = Yii::app()->getDb()->createCommand()
				                      ->select('COUNT(c.idCourse)')
				                      ->from(LearningCourse::model()->tableName().' c')
				                      ->join(CoreUserPuCourse::model()->tableName().' pu', 'c.idCourse=pu.course_id AND pu.puser_id=:idUser')
				                      ->bindValues(array(
					                      ':idUser'=>Yii::app()->user->getIdst()
				                      ))->queryScalar();
				break;
			case 'worst_score':
				$params['name'] = null;
				$params['score'] = 0;
                $params['idCourse'] = null;

				$query = Yii::app()->getDb()->createCommand()
				            ->select('c.name, COALESCE(cu.score_given,0) AS score_given, c.idCourse')
				            ->from(LearningCourse::model()->tableName().' c')
				            ->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=c.idCourse')
				            ->join(CoreUserPuCourse::model()->tableName().' pu', 'c.idCourse=pu.course_id AND pu.puser_id=:idUser')
							->join(CoreUserPU::model()->tableName().' pmu', 'cu.idUser=pmu.user_id AND pmu.puser_id=:idUser')
				            ->order('COALESCE(cu.score_given,0) ASC')
				            ->bindValues(array(
					            ':idUser'=>Yii::app()->user->getIdst()
				            ))->queryRow();

				if(!empty($query)){
					$params['name'] = $query['name'];
					$params['score'] = (int)$query['score_given'];
					$params['idCourse'] = (int)$query['idCourse'];
				}
				break;
			case 'best_score':
				$params['name'] = null;
				$params['score'] = 0;
                $params['idCourse'] = null;

                $query = Yii::app()->getDb()->createCommand()
					->select('c.name, COALESCE(cu.score_given,0) AS score_given, c.idCourse')
					->from(LearningCourse::model()->tableName().' c')
					->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=c.idCourse')
					->join(CoreUserPuCourse::model()->tableName().' pu', 'c.idCourse=pu.course_id AND pu.puser_id=:idUser')
					->join(CoreUserPU::model()->tableName().' pmu', 'cu.idUser=pmu.user_id AND pmu.puser_id=:idUser')
					->order('COALESCE(cu.score_given,0) DESC')
					->bindValues(array(
						':idUser'=>Yii::app()->user->getIdst()
					))->queryRow();

				if(!empty($query)){
					$params['name'] = $query['name'];
					$params['score'] = (int)$query['score_given'];
                    $params['idCourse'] = (int)$query['idCourse'];
				}
				break;
			case 'logged_in_users':
				$params['percent'] = 0;

				$allManagedUsers = Yii::app()->getDb()->createCommand()
					->selectDistinct('pu.user_id')
					->from(CoreUserPU::model()->tableName().' pu')
					->join(CoreUser::model()->tableName().' u', 'u.idst=pu.user_id')
					->where('pu.puser_id=:idUser', array( ':idUser' => Yii::app()->user->getIdst() ) )
					->queryColumn();

				if(!empty($allManagedUsers)){


					$activeUsers = Yii::app()->getDb()->createCommand()
					                  ->selectDistinct('idUser')
					                  ->from(LearningTracksession::model()->tableName())
					                  ->where(array('IN', 'idUser', $allManagedUsers))
					                  ->andWhere('active=1')
					                  ->queryColumn();

					$activeCount = count($activeUsers);
					$totalCount = count($allManagedUsers);

					$params['percent'] = $totalCount<=0 ? 0 : ($activeCount/$totalCount*100);
					
				}
				break;
				
			default:
				// Looks like we didin't find a CORE KPI with this id, lets raise an event and see if someone got some data & view
				$viewPath 	= false;
				$viewParams	= false;
				$viewHtml 	= false;
				$kpi = $kpi;
				Yii::app()->event->raise('CollectCustomKPIData', new DEvent($this, array(
					'kpi'			=> $kpi,
					'viewPath' 		=> &$viewPath,  	// !! by reference
					'viewParams'	=> &$viewParams,	// !! by reference
					'viewHtml'		=> &$viewHtml,		// !! by reference
				)));
				
				// If we get HTML, show it, it has high priority
				if ($viewHtml) {
					$this->sendJSON(array(
						'html'=>$viewHtml,
						'success'=>true,
					));
				}
				else if ($viewPath && $viewParams) {
					$html = $this->renderPartial($viewPath, $viewParams, 1);
					$this->sendJSON(array(
						'html'=>$html,
						'success'=>true,
					));
				}
				
				Yii::app()->end();
				
				break;	
				
		}

		$html = $this->renderPartial($kpi, $params, 1);
		$this->sendJSON(array(
			'html'=>$html,
			'success'=>true,
		));
	}
}