<?php

class PowerUserManagementController extends AdminController {

	/**
	 * @return array
	 */
	public function filters() {
		return array(
			'accessControl' => 'accessControl',
		);
	}


	/**
	 * Returns list of allowed actions.
	 * @return array actions configuration.
	 */
	public function accessRules() {
		$res = array();

		$allowedActions = array(
			'index',
			'create',
			'delete',
			'assignUsers',
			'assignedCourses',
			'getAssignedCoursesMode',
			'assignCourses',
			'unassignCourses',
			'unassignCategory',
			'assignUsersToPowerUser',
			'assignSelectUsers',
			'assignedCatalogs',
			'assignCatalogs',
			'catalogAutocomplete',
			'unassignCatalog',
			'catalogsSelectAll',
			'assignSelectCourses',
			'assignProfile',
			'editCatalogs',
			'deleteAssignedUser',
            'PowerUserAutocompleate'
			// 'groupAutocomplete',
		);
		if (PluginManager::isPluginActive('ClassroomApp')) {
			$allowedActions[] = 'assignLocations';
			$allowedActions[] = 'assignedLocations';
			$allowedActions[] = 'unassignLocations';
		}
		if (PluginManager::isPluginActive('CurriculaApp')) {
			$allowedActions[] = 'unassignCoursepaths';
			$allowedActions[] = 'unassignCoursesAndCoursepaths';
		}

		$res[] = array(
			'allow',
			'roles' => array('/framework/admin/adminmanager/view'),
			'actions' => $allowedActions,
		);

		$res[] = array(
			'deny',
			// deny all
			'users' => array('*'),
			'deniedCallback' => array(
				$this,
				'adminHomepageRedirect'
			),
		);

		return $res;
	}



	/**
	 * This is just a patch for coursepath management, since they do not use AR models as records
	 * and consequently they do not have "ModelSelect" behavior
	 *
	 * @param string $list the index of session variable
	 * @param mixed $value the value to find in session stored items
	 * @return boolean whether the value has been found in session or not
	 */
	protected function inSessionListCoursePath($list, $value) {
		if (is_string($list)) {
			if (isset(Yii::app()->session[$list]) && is_array(Yii::app()->session[$list])) {
				return in_array($value, Yii::app()->session[$list]);
			}
		}
		return false;
	}

    public function actionPowerUserAutocompleate(){
        $requestData = Yii::app()->request->getParam('data', array());

        // What will the format of the final JSON returned array be
        $responseType = Yii::app()->request->getParam('responseType', 'quartz');

        $query = '';
        if(!empty($requestData['query'])) {
            $query = addcslashes($requestData['query'], '%_');
        }

        if(!$query){
            $term = Yii::app()->request->getParam('term', '');
            $query = addcslashes($term, '%_');
        }
        $criteria = new CDbCriteria;

        $criteria->with = array(
			'adminLevel' => array(
				'on' => 'adminLevel.groupid = :groupid',
				'params' => array(
					':groupid' => '/framework/level/admin',
				),
			),
			// 'language',
			'adminProfile',
			//'allAssignedUsers',
			//'manageCourses'
		);
        $userModel = CoreUser::model();
        $criteria->addCondition('('.$userModel->getTableAlias().'.userid like :filter OR '.$userModel->getTableAlias().'.firstname like :filter OR '.$userModel->getTableAlias().'.lastname like :filter)');
		$criteria->params[':filter'] = '%'.$query.'%';

        $criteria->order = 't.userid ASC';

        $result = $userModel->findAll($criteria);
        $usersList = CHtml::listData($result, 'idst', 'userNameFormatted');

        switch($responseType){
            case 'cjuiautocomplete':
                $this->sendJSON($usersList);
                break;
            case 'quartz':default:
                $this->sendJSON(array('options' => $usersList));
        }
    }



	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {

		if (PluginManager::isPluginActive("ClassroomApp")) {
			/* @var $cs CClientScript */
			$cs = Yii::app()->getClientScript();
			$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');
		}
		$filter = Yii::app()->request->getParam('filter', null);
		if($filter === null)
			$filter = !empty($_SESSION['filterSearch']) ? $_SESSION['filterSearch'] : '';
		$_SESSION['filterSearch'] = $filter;

		$powerUserModel = new CoreUser();
		$this->render('index', array(
			'powerUserModel' => $powerUserModel,
			'filter' => $filter,
		));
	}


	/**
	 *
	 */
	public function actionCreate() {
		if (Yii::app()->request->isAjaxRequest) {
			$user = new CoreUser('create');
			$groupMember = new CoreGroupMembers('create');
			$settingUser = new CoreSettingUser('create');
			if (isset($_POST['CoreUser']) && isset($_POST['CoreGroupMembers']) && isset($_POST['CoreSettingUser'])) {
				$user->attributes = $_POST['CoreUser'];
				$user->setAdditionalFieldValues($_POST['CoreUser']['additional']);
				$groupMember->attributes = $_POST['CoreGroupMembers'];
				$settingUser->attributes = $_POST['CoreSettingUser'];
				// todo i/ probably need to create constant with 'ui.language' etc.
				$settingUser->path_name = 'ui.language';

				if ($user->validate() && $groupMember->validate() && $settingUser->validate()) {
					if ($user->save()) {
						$groupMember->idstMember = $user->idst;
						$settingUser->id_user = $user->idst;
						$groupMember->save();
						$settingUser->save();
						$this->sendJSON(array('status' => 'success'));
					}
				}
			}
			$isActiveNotificationOfType = CoreNotification::isActiveNotificationOfType(CoreNotification::NTYPE_USER_EMAIL_MUST_VERIFY);
			$content = $this->renderPartial('_userForm', array(
				'user' => $user,
				'groupMember' => $groupMember,
				'settingUser' => $settingUser,
				'isActiveNotificationOfType' => $isActiveNotificationOfType,
			), true);

			$this->sendJSON(array('html' => $content));
		}
	}

	/**
	 *
	 */
	public function actionAssignProfile() {
		
		if ( !(int)$_POST['assignedCoursesCount'] ) {
			$coreGroupModel = CoreGroup::model()->findByPk($_POST['profileId']);
			if ( !empty($coreGroupModel->assignment_type) ) {
				CoreAdminCourse::setCourseAssignModeForPU( $_POST['puserId'], $coreGroupModel->assignment_type );
				$coreUserPuCourse = new CoreUserPuCourse();
				$coreUserPuCourse->updatePowerUserSelection($_POST['puserId'], array(), $coreGroupModel->assignment_type);
			}
		}
		
		
		if (!empty($_POST['puserId'])) {
			$profileMemberModel = CoreGroupMembers::model()->getPowerUserProfile($_POST['puserId']);

			/*
			 * Assign OR Unassign user to role
			 */
			$data = array(
				"user" => $_POST['puserId'],
				"group" => isset($_POST['profileId']) ? $_POST['profileId'] : null
			);
			HydraRBAC::manageUserToRole($data, true);

			if (!empty($_POST['profileId'])) {
				$profileMemberModel->idst = $_POST['profileId'];
				$profileMemberModel->save();
			} elseif (!$profileMemberModel->isNewRecord) {
				$profileMemberModel->delete();
			}
			$this->sendJSON(array('status' => 'success'));
		}
		Yii::app()->end();
	}


	/**
	 * @param null $id
	 */
	public function actionAssignUsers($id = null) {
        // FancyTree initialization
        Yii::app()->fancytree->init();

		$powerUserModel = CoreUser::model()->findByPk($id);
		Yii::app()->session['currentPowerUserId'] = $id;

		//$assignedUsers = new CoreUserPU();
		$assignedUsers = new CoreAdminTree();
		//$assignedUsers->scenario = 'assign_users';
		//$assignedUsers->powerUserManager = $powerUserModel;

		/*if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['CoreUserPU'])) {

			CoreOrgChartTree::model()->flushCache();

			$findUser = CoreUser::model()->findByAttributes(array('userid' => Yii::app()->user->getAbsoluteUsername($_REQUEST['CoreUserPU']['search_input'])));
			if (!empty($findUser)) {
				$assignedUsers->user_id = $findUser->idst;
			}
		}*/

		$searchFilter = false;
		$inputFilter = Yii::app()->request->getPost('CoreAdminTree', '');
		if (is_array($inputFilter) && isset($inputFilter['search_input'])) {
			$searchFilter = addcslashes(''.$inputFilter['search_input'], '%_');
		}
		if (!empty($searchFilter)) { $assignedUsers->search_input = $searchFilter; }



		// Register required script for users selector, as we are going to use it in this page
		UsersSelector::registerClientScripts();


		$this->render('assignUsers', array(
			'powerUserModel' => $powerUserModel,
			'assignedUsers' => $assignedUsers
		));

	}




	/**
	 * Collect data submited by users selector
	 */
	public function actionAssignUsersToPowerUser() {

		// Check group ID and its model (the group we are editing)
		$idUser = Yii::app()->request->getParam('idUser', false);

		$usersList = UsersSelector::getUsersList($_REQUEST);
		$selectedOrgcchartsInfo = UsersSelector::getSelectedOrgchartsInfo($_REQUEST);

		if (!empty($usersList) || (!empty($selectedOrgcchartsInfo))) {

			try {

				$with = array(
					'adminLevel' => array(
						'condition' => 'adminLevel.groupid = :groupid',
						'params' => array(
							':groupid' => '/framework/level/admin',
						),
					),
				);

				$powerUserModel = CoreUser::model()->with($with)->findByPk($idUser);
				if (empty($powerUserModel)) {
					throw new CException(Yii::t('standard', _OPERATION_FAILURE) . " (invalid power user)");
				}


				// Get and save selections
				$usersList = array();
				$groupsList = array();
				$orgChartList = array();
				$orgChartDescList = array();

				// Users
				if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_USERS])) {
					$usersList = CJSON::decode($_REQUEST[UsersSelector::FIELD_SELECTED_USERS]);
				}

				// Groups
				if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_GROUPS])) {
					$groupsList = CJSON::decode($_REQUEST[UsersSelector::FIELD_SELECTED_GROUPS]);
				}

				// Orgcharts
				list($orgChartList, $orgChartDescList) = UsersSelector::getSelectedOrgchartsInfo($_REQUEST);

				$listToAdd = array_merge($usersList, $groupsList, $orgChartList, $orgChartDescList);
				$result = CoreAdminTree::updatePowerUserSelection($idUser, $listToAdd, true); //tell the function to remove unselected stuff previously present in PU selection

				if (!$result ) {
					throw new CException(Yii::t('standard', _OPERATION_FAILURE) . " (assignment failure)");
				}

				CoreOrgChartTree::model()->flushCache();

				echo "<a class='auto-close success'></a>";
				Yii::app()->end();

			}
			catch (CException $e) {
				Yii::log($e->getMessage(), 'error');
				Yii::app()->user->setFlash('error', $e->getMessage());
				$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
						'type' 	=> UsersSelector::TYPE_POWERUSER_MANAGEMENT,
						'idUser'	=> $idUser,
				));
				$this->redirect($selectorDialogUrl, true);
			}

		}

		echo "<a class='auto-close success'></a>";
		Yii::app()->end();

	}













	/**
	 * @param null $id
	 */
	public function actionAssignSelectUsers($id = null) {
		$with = array('adminLevel' => array(
				'condition' => 'adminLevel.groupid = :groupid',
				'params' => array(
					':groupid' => '/framework/level/admin',
				),
			),
		);

		$powerUserModel = CoreUser::model()->with($with)->findByPk($id);
		if (empty($powerUserModel)) {
			$this->sendJSON(array());
		}

		$withUser = array('adminLevel' => array(
			'condition' => 'adminLevel.groupid = :groupid',
			'params' => array(
				':groupid' => '/framework/level/user',
			),
		),
		);
		$user = CoreUser::model()->with($withUser);

		$user->scenario = 'search';
		$user->unsetAttributes();

		if (isset($_REQUEST['CoreUser'])) {
			$user->attributes = $_REQUEST['CoreUser'];
		}

		$group = new CoreGroup();
		if (isset($_REQUEST['CoreGroup'])) {
			$group->attributes = $_REQUEST['CoreGroup'];
		}

		if (isset($_POST['select-user-grid-selected-items']) || isset($_POST['select-group-grid-selected-items']) || isset($_POST['select-orgchart'])) {
			$usersList = $_POST['select-user-grid-selected-items'] ? explode(',', $_POST['select-user-grid-selected-items']) : array();
			$groupsList = $_POST['select-group-grid-selected-items'] ? explode(',', $_POST['select-group-grid-selected-items']) : array();
			$orgChartList = array();
			$orgChartDescList = array();
			if (isset($_POST['select-orgchart']) && !empty($_POST['select-orgchart'])) {
				foreach ($_POST['select-orgchart'] as $idOrg => $selected) {
					switch((int)$selected) {
						case 0: //not selected
							break;
						case 1: //selected
							$ar = CoreOrgChartTree::model()->findByPk($idOrg);
							if ($ar) {
								$orgChartList[] = $ar->idst_oc;
							}
							break;
						case 2: //selected + descendants
							$ar = CoreOrgChartTree::model()->findByPk($idOrg);
							if ($ar) {
								$orgChartDescList[] = $ar->idst_ocd;
							}
							break;
					}
				}
			}

			$listToAdd = array_merge($usersList, $groupsList, $orgChartList, $orgChartDescList);
			$rs = CoreAdminTree::updatePowerUserSelection($id, $listToAdd);

			CoreOrgChartTree::model()->flushCache();

			if ($rs) {
				unset(Yii::app()->session['selectedOrgChartItems']);
				$this->sendJSON(array('status' => 'saved'));
			} else {
				$this->sendJSON(array('status' => 'error')); //TO DO: manage this case in client side
			}
		}

		if (Yii::app()->request->isAjaxRequest) {

			$powerUserOrgChartsMembers = CoreAdminTree::getPowerUserMembers($id, array(CoreAdminTree::TYPE_ORGCHART, CoreAdminTree::TYPE_ORGCHART_DESC));
			$selectedOrgCharts = array();
			foreach ($powerUserOrgChartsMembers as $info) {
				$selectedOrgCharts[$info['id']] = $info['type'] == CoreAdminTree::TYPE_ORGCHART ? 1 : 2;
			}
			Yii::app()->session['selectedOrgChartItems'] = $selectedOrgCharts;

			// Build fancytree specific PRE-selected array
			$preSelected = array();
			foreach ($selectedOrgCharts as $key => $selectState) {
				$preSelected[] = array("key" => $key, "selectState" => $selectState);
			}

			$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray();
			$content = $this->renderPartial('//common/_usersSelector', array(
				'userModel' => $user,
				'groupModel' => $group,
				'isNeedRegisterYiiGridJs' => true,
				'onlyUsers' => true,
				'fancyTreeData' => $fancyTreeData,
				'preSelected' => $preSelected,
			), true, true);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $content;
			} else {
				$this->sendJSON(array('html' => $content));
			}
		}
		Yii::app()->end();
	}




	public function actionDeleteAssignedUser() {
		$currentPowerUserId = Yii::app()->session['currentPowerUserId'];
		$idsArr = false;
		if (isset($_POST['idst_list'])) {
			// mass unassign
			$idstList = explode(",", $_POST['idst_list']);
			if (is_array($idstList)) {
				$idsArr = $idstList;
			} else {
				$idsArr = array($_POST['idst']);
			}
		} else {
			if (isset($_GET['id'])) { // non-mass/single unassign
				$idsArr = array($_GET['id']);
			}
		}

		$condition = new CDbCriteria();
		$condition->addInCondition('idst', $idsArr);
		$condition->addCondition('idstAdmin=:pu');
		$condition->params[':pu'] = (int)$currentPowerUserId;

		if (isset($_POST['confirm'])) {

			CoreOrgChartTree::model()->flushCache();

			// Actually unassing the users/groups from the power user
			CoreAdminTree::model()->deleteAll($condition);
            $this->updateUsersInReportFilters((int)$currentPowerUserId,$idsArr);
			// Recalculate cached tables for this power user
			CoreUserPU::model()->updatePowerUserSelection($currentPowerUserId);
			$this->sendJSON(array());
		}

		if (empty($idsArr)) {
			$this->sendJSON(array());
		}

		$count_users =	Yii::app()->db->createCommand()->select('count(*)')
						->from(CoreUser::model()->tableName())
						->where(array('in', 'idst', $idsArr))
						->queryScalar();
		$count_groups =	Yii::app()->db->createCommand()->select('count(*)')
						->from(CoreGroup::model()->tableName())
						->where(array('in', 'idst', $idsArr))
						->andWhere("groupid NOT LIKE '/oc\_%'")
						->andWhere("groupid NOT LIKE '/ocd\_%'")
						->queryScalar();
		$count_folder =	Yii::app()->db->createCommand()->select('count(*)')
						->from(CoreGroup::model()->tableName())
						->where(array('in', 'idst', $idsArr))
						->andWhere("groupid LIKE '/oc\_%'")
						->queryScalar();
		$count_folder_desc =	Yii::app()->db->createCommand()->select('count(*)')
								->from(CoreGroup::model()->tableName())
								->where(array('in', 'idst', $idsArr))
								->andWhere("groupid LIKE '/ocd\_%'")
								->queryScalar();

		$what_del = array();

		if($count_users > 0)
			$what_del[] = Yii::t('standard', '_USERS');
		if($count_groups > 0)
			$what_del[] = Yii::t('standard', '_GROUPS');
		if($count_folder > 0 || $count_folder_desc > 0)
			$what_del[] = Yii::t('standard', 'branches');

		if (Yii::app()->request->isAjaxRequest) {
			$content = $this->renderPartial('deleteUser', array(
				'models' => $idsArr,
				'count' => count($idsArr),
				'what_del' => $what_del
			), TRUE);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
		Yii::app()->end();
	}

	public function actionGetAssignedCoursesMode($id){
		$mode = CoreAdminCourse::getCourseAssignModeForPU($id);
		
		$label = null;
		switch($mode){
			case CoreAdminCourse::COURSE_SELECTION_MODE_ALL:
				$label = Yii::t('standard', 'You have selected all courses');
				break;
			case CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS:
				$label = Yii::t('standard', 'You have selected all courses from visible course catalogs');
				break;
            case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS:
                $label = Yii::t('standard', 'You have selected all courses and Learning Plans');
                break;
			case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS:
				$label = Yii::t('standard', 'You have selected all Learning Plans');
				break;
			case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS:
				$label = Yii::t('standard', 'You have selected all courses and Learning Plans in visible catalogs');
				break;
		}

		echo CJSON::encode(array(
			'mode'=>$mode,
			'label'=>$label,
		));
	}

	/**
	 * @param null $id
	 */
	public function actionAssignedCourses($id = null) {
        // FancyTree initialization
        Yii::app()->fancytree->init();

        // get the power user if it set
        $currentPuId = Yii::app()->request->getParam('id', false);
        $selectAll 		= Yii::app()->request->getParam('comboListViewSelectAll', false);

        $powerUserModel = CoreUser::model()->findByPk($id);
        Yii::app()->session['currentPowerUserId'] = $id;

        // update Power User assignments as some of the related data are missing in the corresponding db tables
        if ($currentPuId) {
            CoreUserPuCourse::model()->updatePowerUserSelection($currentPuId);
        }

		$courseModel = new LearningCourse('power_user_assigned');
		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['LearningCourse'])) {
			$courseModel->attributes = $_REQUEST['LearningCourse'];
		}
		$courseModel->powerUserManager = $powerUserModel;

		// Search filtering
		if(isset($_REQUEST['search_input'])){
			$courseModel->name = $_REQUEST['search_input'];
		}

		if($selectAll){
			$dp = $courseModel->dataProviderPUManagement();
			$dp->setPagination(false);
			$keys = $dp->keys;
			foreach ($keys as $index => $key) {
				$keys[$index] = $key;
			}
			echo json_encode($keys);
			Yii::app()->end();
		}


        //This is for the logn autocompleate result if search for course!!!
        Yii::app()->clientScript->registerCss('cssautocompleate', '
            ul.ui-autocomplete{
                max-height: 500px;
                overflow: auto;
            }
       ');


		$this->render('assignedCoursesNew', array(
			'powerUserModel' => $powerUserModel,
			'courseModel' => $courseModel,
		));
	}

	public function actionAssignedCatalogs($id = null){
		// get the power user if it set
		$currentPuId = Yii::app()->request->getParam('id', false);
		$selectAll = Yii::app()->request->getParam('comboListViewSelectAll', false);
		$search = Yii::app()->request->getParam('search_input', null);
		if($selectAll){
			$this->actionCatalogsSelectAll($search);
		}
		$powerUserModel = CoreUser::model()->findByPk($id);
		Yii::app()->session['currentPowerUserId'] = $id;
		$catalogModel = new LearningCatalogue();
		$params = array();
		if (!empty($search))
			$params['search'] = $search;

		$this->render('assignedCatalogs', array(
			'powerUserModel' => $powerUserModel,
			'catalogModel' => $catalogModel,
			'params' => $params,
		));
	}

	public function actionCatalogsSelectAll($search = ''){
		if ($search == '') {
			$search = Yii::app()->request->getParam('search_input', null);
		}
		$catalogModel = new LearningCatalogue();
		$dataProvider = $catalogModel->dataProvider(Yii::app()->request->getParam('id'), $search);
		$dataProvider->setPagination(false);

		$keys = $dataProvider->keys;

		foreach ($keys as $index => $key) {
			$keys[$index] = (int) $key;
		}
		echo CJSON::encode($keys);
		Yii::app()->end();
	}

	/**
	 * @param $idst
	 */
	public function actionAssignCourses($idst) {
		if( Yii::app()->request->isAjaxRequest ) {
			$powerUserModel = CoreUser::model()->findByPk( $idst );

			$result = array();

			$courseModel = new LearningCourse( 'power_user_assign' );
			$courseModel->unsetAttributes();
			if( isset( $_REQUEST['LearningCourse'] ) ) {
				$courseModel->attributes = $_REQUEST['LearningCourse'];
			}
			$courseModel->powerUserManager = $powerUserModel;

			if( PluginManager::isPluginActive( 'CurriculaApp' ) ) {
				$coursepathModel = new LearningCoursepath( 'power_user_assign' );
				$coursepathModel->unsetAttributes();
				if( isset( $_REQUEST['LearningCoursepath'] ) ) {
					$coursepathModel->attributes = $_REQUEST['LearningCoursepath'];
				}
				$coursepathModel->powerUserManager = $powerUserModel;
			} else {
				$coursepathModel = NULL;
			}

			$selectedItems = Yii::app()->request->getParam('selectedItems');
			$selectedCourses = Yii::app()->request->getParam('poweruser-course-grid-selected-items-list');
			$selectedCoursePaths = Yii::app()->request->getParam('poweruser-coursepath-grid-selected-items-list');
			$ajax = Yii::app()->request->getParam('ajax');

			if(($selectedItems || $selectedCourses) && $ajax == 'poweruser-course-grid') {
				$selectedItems = $selectedItems ? $selectedItems : $selectedCourses;
				Yii::app()->session['poweruser_course_selector_filter'] = explode(',', $selectedItems);
			} elseif(($selectedItems || $selectedCoursePaths) && $ajax == 'poweruser-coursepath-grid') {
				$selectedItems = $selectedItems ? $selectedItems : $selectedCoursePaths;
				Yii::app()->session['poweruser_coursepath_selector_filter'] = explode(',', $selectedItems);
			}else{
				$preselectedCourses =  Yii::app()->db->createCommand()
					->select( "id_entry" )
					->from( CoreAdminCourse::model()->tableName() )
					->where( "idst_user = :id_admin AND type_of_entry = :course", array( ':id_admin' => $idst, ':course' => CoreAdminCourse::TYPE_COURSE ) )
					->queryColumn();
				Yii::app()->session['poweruser_course_selector_filter'] = $preselectedCourses;

                $preselectedCoursePaths = Yii::app()->db->createCommand()
					->select( "id_entry" )
                    ->from( CoreAdminCourse::model()->tableName() )
                    ->where( "idst_user = :id_admin AND type_of_entry = :course", array( ':id_admin' => $idst, ':course' => CoreAdminCourse::TYPE_COURSEPATH ) )
                    ->queryColumn();
				Yii::app()->session['poweruser_coursepath_selector_filter'] = $preselectedCoursePaths;
			}

			$categoriesModel = new LearningCourseCategory();

			//check input data

			$sendAssignResult = FALSE;

			$courseAssignMode = isset( $_REQUEST['course-selection-type'] ) ? $_REQUEST['course-selection-type'] : false;

			if($courseAssignMode) {
				CoreAdminCourse::setCourseAssignModeForPU( $idst, $courseAssignMode );

				switch ( $courseAssignMode ) {
					case CoreAdminCourse::COURSE_SELECTION_MODE_ALL:
					case CoreAdminCourse::COURSE_SELECTION_MODE_ALL_IN_VISIBLE_CATALOGS:
                    case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_WITH_PATHS:
					case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_PATHS:
					case CoreAdminCourse::COURSE_SELECTION_MODEL_ALL_COURSES_PATHS_IN_CATALOGS:
						// When the mode is "All courses", delete all previous
						// associations for courses, categories, learning plans
						// since on next login the PU will be resolved/will receive
						// access to all courses anyway, regardless of what
						$deleteTypes = array(
							CoreAdminCourse::TYPE_COURSE,
							CoreAdminCourse::TYPE_COURSEPATH,
							CoreAdminCourse::TYPE_CATEGORY,
							CoreAdminCourse::TYPE_CATEGORY_DESC,
						);
						CoreAdminCourse::model()->deleteAllByAttributes( array(
							'idst_user'     => $idst,
							'type_of_entry' => $deleteTypes
						) );
						CoreUserPuCoursepath::model()->deleteAllByAttributes( array( 'puser_id' => $idst ) );

						$sendAssignResult = TRUE;
						break;
					case CoreAdminCourse::COURSE_SELECTION_MODE_STANDARD:
					default:
							// Save selected courses
							if (isset($_REQUEST['poweruser-course-grid-selected-items']) && ($_REQUEST['poweruser-course-grid-selected-items'] != '')) {
								$courselist = explode(',', $_REQUEST['poweruser-course-grid-selected-items']);
								if (is_array($courselist) && !empty($courselist)) {
									$deleteTypes = array(
										CoreAdminCourse::TYPE_COURSE,
									);
									CoreAdminCourse::model()->deleteAllByAttributes(array(
										'idst_user' => $idst,
										'type_of_entry' => $deleteTypes
									));
								}

								foreach ($courselist as $courseId) {
									$coreUserPuCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
										'puser_id' => $idst,
										'course_id' => $courseId
									));
									if (!$coreUserPuCourseModel) {
										$model = new CoreUserPuCourse();
										$model->puser_id = $idst;
										$model->course_id = (int)$courseId;
										$model->save();
									}

									$coreAdminCourseModel = CoreAdminCourse::model()->findByAttributes(array(
										'idst_user' => $idst,
										'type_of_entry' => CoreAdminCourse::TYPE_COURSE,
										'id_entry' => $courseId
									));

									if (!$coreAdminCourseModel) {
										// TODO: CoreAdminCourse handling will be upgraded in the future
										$adminCourseModel = new CoreAdminCourse();
										$adminCourseModel->idst_user = $idst;
										$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_COURSE;
										$adminCourseModel->id_entry = $courseId;
										$adminCourseModel->save();
									}

									$sendAssignResult = TRUE;
								}
							}

						// Save selected Learning Plans (coursepaths)
						if( PluginManager::isPluginActive( 'CurriculaApp' ) ) {
							if( isset( $_REQUEST['poweruser-coursepath-grid-selected-items'] ) && ( $_REQUEST['poweruser-coursepath-grid-selected-items'] != '' ) ) {
								$coursepathlist = explode( ',', $_REQUEST['poweruser-coursepath-grid-selected-items'] );

                                if (is_array($coursepathlist) && !empty($coursepathlist)) {
                                    $deleteTypes = array(
                                        CoreAdminCourse::TYPE_COURSEPATH,
                                    );
                                    CoreAdminCourse::model()->deleteAllByAttributes(array(
                                        'idst_user' => $idst,
                                        'type_of_entry' => $deleteTypes
                                    ));
                                }

								foreach ( $coursepathlist as $coursepathId ) {
								    $coursePuCoursePath = CoreUserPuCoursepath::model()->findByAttributes(array(
								        'puser_id' => $idst,
                                        'path_id' => (int) $coursepathId
                                    ));

                                    if(!$coursePuCoursePath){
                                        $model           = new CoreUserPuCoursepath();
                                        $model->puser_id = $idst;
                                        $model->path_id  = (int) $coursepathId;
                                        $model->save();
                                    }

									// TODO: CoreAdminCourse handling will be upgraded in the future
									$adminCourseModel                = new CoreAdminCourse();
									$adminCourseModel->idst_user     = $idst;
									$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_COURSEPATH;
									$adminCourseModel->id_entry      = $coursepathId;
									$adminCourseModel->save();

									$sendAssignResult = TRUE;
								}
							}
						}

						// Save selected categories
						if( isset( $_REQUEST['select-categories'] ) && $_REQUEST['select-categories'] ) {

							// First clear old category associations
							$deleteCriteria = new CDbCriteria();
							$deleteCriteria->compare( 'idst_user', $idst );
							$deleteCriteria->compare( 'type_of_entry', array(
								CoreAdminCourse::TYPE_CATEGORY,
								CoreAdminCourse::TYPE_CATEGORY_DESC
							) );
							CoreAdminCourse::model()->deleteAll( $deleteCriteria );

							$selectedCategories = json_decode( $_REQUEST['select-categories'], TRUE );
							if( $selectedCategories && is_array( $selectedCategories ) ) {
								foreach ( $selectedCategories as $catSelection ) {
									if( $catSelection['state'] == 2 ) {
										// Category + Descendants
										$adminCourseModel                = new CoreAdminCourse();
										$adminCourseModel->idst_user     = $idst;
										$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_CATEGORY_DESC;
										$adminCourseModel->id_entry      = $catSelection['key'];
										$adminCourseModel->save();

										$sendAssignResult = TRUE;
									} elseif( $catSelection['state'] == 1 ) {
										// Category only (no descendants)
										$adminCourseModel                = new CoreAdminCourse();
										$adminCourseModel->idst_user     = $idst;
										$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_CATEGORY;
										$adminCourseModel->id_entry      = $catSelection['key'];
										$adminCourseModel->save();

										$sendAssignResult = TRUE;
									}
								}
							}
						}

						break;
				}
			}

			if( $sendAssignResult ) {
				// Refresh courses to make counters ok
				CoreUserPuCourse::model()->updatePowerUserSelection( $idst );

				$result['status'] = 'saved';
				$this->sendJSON( $result );
			}

			$result['html'] = $this->renderPartial( 'assignCoursesWithCoursepaths', array(
				'courseAssignMode'       => $courseAssignMode,
				'idst'                    => $idst,
				'courseModel'             => $courseModel,
				'coursepathModel'         => $coursepathModel,
				'categoriesModel'         => $categoriesModel,
				'categoriesFancyTreeData' => LearningCourseCategory::buildFancyTreeCategoriesData(),
				'categoriesPreselected'   => CoreAdminCourse::getFancyTreePreSelectedArrayCategories( $idst ),
				'includeCurriculas'       => PluginManager::isPluginActive( 'CurriculaApp' ),
				'courseSelectionMode'     => CoreAdminCourse::getCourseAssignModeForPU( $idst ),
			), TRUE, TRUE );

			if( isset( $_REQUEST['contentType'] ) && ( $_REQUEST['contentType'] == 'html' ) ) {
				echo $result['html'];
			} else {
				$this->sendJSON( $result );
			}

		}
	}

	public function actionAssignCatalogs($idst){
		if (Yii::app()->request->isAjaxRequest) {
			$powerUserModel = CoreUser::model()->findByPk($idst);
			$search = null;
			$catalogModel = new LearningCatalogue();
			$assignedCatalogsIds = Yii::app()->db->createCommand()
				->select('id_entry')
				->from(CoreAdminCourse::model()->tableName())
				->where('idst_user = :userId AND type_of_entry = :type', array(
					':userId' => $powerUserModel->idst,
					':type' => CoreAdminCourse::TYPE_CATALOG
				))->queryColumn();

			if (!empty($_POST['search_input'])) {
				$search = Yii::app()->request->getParam('search_input');
			}

			$this->renderPartial('_catalog', array(
				'catalogModel' => $catalogModel,
				'powerUserModel' => $powerUserModel,
				'search' => $search != null ? $search : null,
				'assignedCatalogsIds' => !empty($assignedCatalogsIds) ? $assignedCatalogsIds : null
			), false, true);
		}
	}

	public function actionEditCatalogs(){
		$data = Yii::app()->request->getParam('data', null);
		if($data) {
			$selectedCatalogsIds = $data['selectedCatalogs'];
			if(!$selectedCatalogsIds) $selectedCatalogsIds = array();
			$idst = $data['idst'];
			if ($idst) {

				$existingCatalogsIds = Yii::app()->db->createCommand()
					->select('id_entry')
					->from(CoreAdminCourse::model()->tableName())
					->where('idst_user = :userId AND type_of_entry = :type', array(
						':userId' => $idst,
						':type' => CoreAdminCourse::TYPE_CATALOG,
					))->queryColumn();

				foreach ($selectedCatalogsIds as $catalogId) {
					if (!in_array($catalogId, $existingCatalogsIds)) {
						Yii::app()->db->createCommand()->insert(CoreAdminCourse::model()->tableName(), array(
							'idst_user' => $idst,
							'type_of_entry' => CoreAdminCourse::TYPE_CATALOG,
							'id_entry' => $catalogId
						));
					}
				}
				
				Yii::app()->event->raise(EventManager::EVENT_POWER_USER_OBJECTS_SELECTION_UPDATED, new DEvent($this, array(
				    'puser_id'       => $idst,
				)));
				
			}
		}
		Yii::app()->end();
	}

	public function actionUnassignCatalog(){
		if (Yii::app()->request->isAjaxRequest) {
			$id = Yii::app()->request->getParam('id',null);
			$selectedCatalogs = Yii::app()->request->getParam('selectedCatalogs', null);
			if (!empty($selectedCatalogs)) $selectedCatalogs = explode(',', $selectedCatalogs);
			$userId = Yii::app()->request->getParam('userId');
			$catalogModel = LearningCatalogue::model()->findByPk($id);
			if(!$catalogModel) $catalogs = LearningCatalogue::model()->findAllByAttributes(array(
				'idCatalogue'=>$selectedCatalogs
			));
			if(isset($_POST['submit']) && !empty($_POST['submit'])) {
				if (!empty($selectedCatalogs)) {
					$c = new CDbCriteria();
					$c->compare('id_entry', $selectedCatalogs);
					$c->compare('type_of_entry', CoreAdminCourse::TYPE_CATALOG);
					$c->compare('idst_user', $userId);
					CoreAdminCourse::model()->deleteAll($c);
				} else {
					Yii::app()->db->createCommand()->delete(CoreAdminCourse::model()->tableName(),
						'idst_user = :userId AND id_entry=:idEntry AND type_of_entry=:type', array(
							':userId' => $userId,
							':idEntry' => $catalogModel->idCatalogue,
							':type' => CoreAdminCourse::TYPE_CATALOG,
						));
				}
				echo '<a class="auto-close"></a>';
				echo '<script>$("a.combo-list-deselect-all").trigger("click");$("#catalogItems-grid").comboListView("updateListView");</script>';
				
				Yii::app()->event->raise(EventManager::EVENT_POWER_USER_OBJECTS_SELECTION_UPDATED, new DEvent($this, array(
				    'puser_id'       => $userId,
				)));
				
				
				Yii::app()->end();
			}
			if(!empty($selectedCatalogs)){
				$count = count($selectedCatalogs);
				$this->renderPartial('_confirmUnassignCatalog', array('count' => $count, 'userId' => $userId, 'selectedCatalogs' => $selectedCatalogs));
				Yii::app()->end();
			}
			$this->renderPartial('_confirmUnassignCatalog', array('model' => $catalogModel, 'userId' => $userId));
		}
	}
	/**
	 * @param null $id
	 */
	public function actionUnassignCourses($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['id'])) {
				$id = $_POST['id'];
			}

			$courses = LearningCourse::model()->findAllByAttributes(array('idCourse' => $id));
			if (empty($courses)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				$puserId = Yii::app()->session['currentPowerUserId'];
				CoreUserPuCourse::model()->deleteAllByAttributes(array('course_id' => $id, 'puser_id' => $puserId));
				//TO DO: CoreAdminCourse handling will be upgraded in the future
				CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $id, 'type_of_entry' => CoreAdminCourse::TYPE_COURSE, 'idst_user' => $puserId));
				$this->sendJSON(array());
			}

			$content = $this->renderPartial('deleteCourse', array(
				'courses' => $courses,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
	}



	/**
	 * @param null $id
	 */
	public function actionUnassignCoursepaths($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['id'])) {
				$id = $_POST['id'];
			}

			$coursepaths = LearningCoursepath::model()->findAllByAttributes(array('id_path' => $id));
			if (empty($coursepaths)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				$puserId = Yii::app()->session['currentPowerUserId'];
				CoreUserPuCoursepath::model()->deleteAllByAttributes(array('path_id' => $id, 'puser_id' => $puserId));
                $this->updateLearningPlansInReportFilters($puserId,$id);
				//TO DO: CoreAdminCourse handling will be upgraded in the future
				CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $id, 'type_of_entry' => CoreAdminCourse::TYPE_COURSEPATH, 'idst_user' => $puserId));
				$this->sendJSON(array());
			}

			$content = $this->renderPartial('deleteCoursepath', array(
				'coursepaths' => $coursepaths,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
	}

	public function actionUnassignCategory($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['id'])) {
				$id = $_POST['id'];
			}

			$itemType = CoreAdminCourse::TYPE_CATEGORY;
			if(isset($_REQUEST['item_type']) && $_REQUEST['item_type']==CoreAdminCourse::TYPE_CATEGORY_DESC){
				$itemType = CoreAdminCourse::TYPE_CATEGORY_DESC;
			}

			$puserId = Yii::app()->session['currentPowerUserId'];
			$language = Lang::getNameByBrowserCode(Yii::app()->getLanguage());

			$category = Yii::app()->getDb()->createCommand()
				->select('t.idCategory, c.translation, ca.type_of_entry')
				->from(LearningCourseCategoryTree::model()->tableName().' t')
				->join(CoreAdminCourse::model()->tableName().' ca', 'ca.id_entry=t.idCategory AND ca.type_of_entry=:type AND ca.idst_user=:idUser', array(':type'=>$itemType, ':idUser'=>$puserId))
				->join(LearningCourseCategory::model()->tableName().' c', 't.idCategory=c.idCategory AND c.lang_code=:lang', array(':lang'=>$language))
				->where('t.idCategory = :categoryId', array(':categoryId' => $id))
				->queryRow();

			if (empty($category)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				$criteria = new CDbCriteria();
				$criteria->compare('idst_user', $puserId);
				$criteria->compare('type_of_entry', $itemType);
				$criteria->compare('id_entry', $id);
				CoreAdminCourse::model()->deleteAll($criteria);

				// Refresh courses to make counters ok
				CoreUserPuCourse::model()->updatePowerUserSelection($puserId);

				$this->sendJSON(array());
			}

			$content = $this->renderPartial('deleteCategory', array(
				'category'=>$category,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
	}



	/**
	 * @param null $id
	 */
	public function actionUnassignCoursesAndCoursepaths($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['id'])) {
				$id = explode(',', $_POST['id']);
			}

			$idCourses = array();
			$idCoursepaths = array();
			$idCategories = array();
			$idCategoriesWithDescendants = array();

			foreach ($id as $item) {
				$parts = explode('_', $item);
				$idItem = $parts[0];
				array_shift($parts);
				$itemType = implode('_', $parts);
				switch ($itemType) {
					case CoreAdminCourse::TYPE_COURSE: { $idCourses[] = $idItem; } break;
					case CoreAdminCourse::TYPE_COURSEPATH: { $idCoursepaths[] = $idItem; } break;
					case CoreAdminCourse::TYPE_CATEGORY: { $idCategories[] = $idItem; } break;
					case CoreAdminCourse::TYPE_CATEGORY_DESC: { $idCategoriesWithDescendants[] = $idItem; } break;
				}
			}

			$courses = (!empty($idCourses) ? LearningCourse::model()->findAllByAttributes(array('idCourse' => $idCourses)) : array());
			$coursepaths = (!empty($idCoursepaths) ? LearningCoursepath::model()->findAllByAttributes(array('id_path' => $idCoursepaths)) : array());
			if (empty($courses) && empty($coursepaths) && empty($idCategories) && empty($idCategoriesWithDescendants)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				$puserId = Yii::app()->session['currentPowerUserId'];
				if (!empty($idCourses)) {
					CoreUserPuCourse::model()->deleteAllByAttributes(array('course_id' => $idCourses, 'puser_id' => $puserId));
					//TO DO: CoreAdminCourse handling will be upgraded in the future
					CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $idCourses, 'type_of_entry' => CoreAdminCourse::TYPE_COURSE, 'idst_user' => $puserId));
				}
				if (!empty($idCoursepaths)) {
					CoreUserPuCoursepath::model()->deleteAllByAttributes(array('path_id' => $idCoursepaths, 'puser_id' => $puserId));
					//TO DO: CoreAdminCourse handling will be upgraded in the future
					CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $idCoursepaths, 'type_of_entry' => CoreAdminCourse::TYPE_COURSEPATH, 'idst_user' => $puserId));
				}
				if(!empty($idCategories)){
					$criteria = new CDbCriteria();
					$criteria->compare('idst_user', $puserId);
					$criteria->compare('type_of_entry', CoreAdminCourse::TYPE_CATEGORY);
					$criteria->compare('id_entry', $idCategories);
					CoreAdminCourse::model()->deleteAll($criteria);
				}
				if(!empty($idCategoriesWithDescendants)){
					$criteria = new CDbCriteria();
					$criteria->compare('idst_user', $puserId);
					$criteria->compare('type_of_entry', CoreAdminCourse::TYPE_CATEGORY_DESC);
					$criteria->compare('id_entry', $idCategoriesWithDescendants);
					CoreAdminCourse::model()->deleteAll($criteria);
				}

				// Refresh PUs assignments to show proper grid items
				CoreUserPuCourse::model()->updatePowerUserSelection($puserId);

				$this->sendJSON(array());
			}

			$content = $this->renderPartial('deleteCourseAndCoursepath', array(
				'courses' => $courses,
				'coursepaths' => $coursepaths,
				'categories' => $idCategories,
				'categoriesWithDescendants' => $idCategoriesWithDescendants,
				'id' => $id
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
	}



	/**
	 * @param null $id
	 */
	public function actionAssignedLocations($id = null) {
		$powerUserModel = CoreUser::model()->findByPk($id);
		Yii::app()->session['currentPowerUserId'] = $id;

		$locationModel = new LtLocation('power_user_assigned');
		if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['LtLocation'])) {
			$locationModel->attributes = $_REQUEST['LtLocation'];
		}
		$locationModel->powerUserManager = $powerUserModel;

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');

		$this->render('assignedLocations', array(
			'powerUserModel' => $powerUserModel,
			'locationModel' => $locationModel,
		));
	}


	/**
	 * @param $idst
	 */
	public function actionAssignLocations($idst) {
		if (Yii::app()->request->isAjaxRequest) {
			$powerUserModel = CoreUser::model()->findByPk($idst);

			$result = array();
			$locationModel = new LtLocation('power_user_assign');
			$locationModel->unsetAttributes();
			if (isset($_REQUEST['LtLocation'])) {
				$locationModel->attributes = $_REQUEST['LtLocation'];
			}
			$locationModel->powerUserManager = $powerUserModel;

			if (isset($_REQUEST['poweruser-location-grid-selected-items']) && ($_REQUEST['poweruser-location-grid-selected-items'] != '')) {
				$locationlist = explode(',', $_REQUEST['poweruser-location-grid-selected-items']);
				foreach ($locationlist as $locationId) {
					$exists = CoreUserPuLocation::model()->exists('puser_id = :puser_id AND location_id = :location_id', array(
						':puser_id' => $idst,
						':location_id' =>$locationId
					));
					if(!$exists) {
						$model = new CoreUserPuLocation();
						$model->puser_id = $idst;
						$model->location_id = $locationId;
						$model->save();
					}
					/*
					//TO DO: CoreAdminCourse handling will be upgraded in the future
					$adminCourseModel = new CoreAdminCourse();
					$adminCourseModel->idst_user = $idst;
					$adminCourseModel->type_of_entry = CoreAdminCourse::TYPE_LOCATION;
					$adminCourseModel->id_entry = $locationId;
					$adminCourseModel->save();
					*/
				}
				$result['status'] = 'saved';
				$this->sendJSON($result);
			}

			$processOutput = false;
			if (isset($_REQUEST['popupFirstShow']) && $_REQUEST['popupFirstShow'] === '1') {
				$processOutput = true;
			}
			$result['html'] = $this->renderPartial('assignLocations', array(
				'model' => $locationModel,
			), true, $processOutput);

			if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
				echo $result['html'];
			} else {
				$this->sendJSON($result);
			}

		}
	}


	/**
	 * @param null $id
	 */
	public function actionUnassignLocations($id = null) {
		if (Yii::app()->request->isAjaxRequest) {
			if (!empty($_POST['id'])) {
				$id = $_POST['id'];
			}

			$locations = LtLocation::model()->findAllByAttributes(array('id_location' => $id));
			if (empty($locations)) {
				$this->sendJSON(array());
			}

			if (!empty($_POST['confirm'])) {
				$puserId = Yii::app()->session['currentPowerUserId'];
				CoreUserPuLocation::model()->deleteAllByAttributes(array('location_id' => $id, 'puser_id' => $puserId));
				//TO DO: CoreAdminCourse handling will be upgraded in the future
				/*
				CoreAdminCourse::model()->deleteAllByAttributes(array('id_entry' => $id, 'type_of_entry' => CoreAdminCourse::TYPE_COURSE, 'idst_user' => $puserId));
				*/
				$this->sendJSON(array());
			}

			$content = $this->renderPartial('deleteLocation', array(
				'locations' => $locations,
			), true);
			$this->sendJSON(array(
				'html' => $content,
			));
		}
	}

    private function updateUsersInReportFilters($puID,$users){
        $customReports = LearningReportFilter::model()->findAllByAttributes(array(
            'author'=>$puID
        ));
        if ($customReports){
            if (!is_array($customReports)) {
                $customReports = array($customReports);
            }
            foreach ($customReports as $report) {
                $report->cleanUpUsers($users);
            }
        }
    }
    private function updateLearningPlansInReportFilters($puID,$plans){
        // Clean up report filters: remove the user being deleted from all reports, that are created by this PU
        $customReports = LearningReportFilter::model()->findAllByAttributes(array(
            'author'=>$puID,
            'report_type_id'=>LearningReportType::USERS_COURSEPATH
        ));

        if ($customReports){
            if (!is_array($customReports)) {
                $customReports = array($customReports);
            }
            foreach ($customReports as $report) {
                $report->cleanUpCoursepaths($plans);
            }
        }
    }
}
