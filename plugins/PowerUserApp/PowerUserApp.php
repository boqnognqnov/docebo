<?php

/**
 * Plugin for Label
 */

class PowerUserApp extends DoceboPlugin {

	// Override parent
	public static $HAS_SETTINGS = false;


	/**
	 * @var string Name of the plugin; needed to identify plugin folder and entry script
	 */
	public $plugin_name = 'PowerUserApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		// activate options
	}


	public function deactivate() {
		parent::deactivate();

		//Removing widgets from dashboards
		DashboardDashlet::model()->deleteAllByAttributes(array('handler' => 'plugin.PowerUserApp.widgets.DashletPoweruserKPIs'));
	}


}
