<?php

class LectoraTrialInstaller extends CComponent
{
	const LECTORA_SERVER_URL_US =   'lectoraonline.com';
	const LECTORA_SERVER_URL_EU =   'eu.lectoraonline.com';
	const LECTORA_SERVER_URL_TEST = 'test.lectoraonline.com';

	const LECTORA_SERVER_KEY_US =   '484B9362F5ACFEBB6CB764E8BD6F9';
	const LECTORA_SERVER_KEY_EU =   'D57D988AE563E679347FB47D3FDAC';
	const LECTORA_SERVER_KEY_TEST = 'E4731E37D72CF27C88367E3D854A9';

	const LECTORA_URL_US =   'https://lectoraonline.com/app';
	const LECTORA_URL_EU =   'https://eu.lectoraonline.com/app';
	const LECTORA_URL_TEST = 'https://test.lectoraonline.com/app';

	const LECTORA_OPERATION = 'dt';

	const LECTORA_TRIAL_SOURCE = 'docebo';

	// American countries will be redirected to American server, all others to European server
	// These countries match the spelling from the ERP server and the country value retrieved from ErpApiClient::apiErpGetComboInfo()
	public static $americanCountries = array(
		'antigua_and_barbuda',
		'argentina',
		'bahamas',
		'barbados',
		'belize',
		'bolivia',
		'brazil',
		'canada',
		'chile',
		'costa_rica',
		'cuba',
		'dominica',
		'dominican_republic',
		'el_salvador',
		'grenada',
		'guatemala',
		'guyana',
		'haiti',
		'honduras',
		'jamaica',
		'mexico',
		'nicaragua',
		'panama',
		'paraguay',
		'peru',
		'saint_kitts_and_nevis',
		'saint_lucia',
		'saint_vincent',
		'suriname',
		'trinidad_and_tobago',
		'united_states',
		'uruguay',
		'venezuela'
	);

	public function installTrial(){
		try {
			$pensAppModel = PensApplication::model()->findByAttributes(array(
				'related_plugin' => 'Lectora'
			));
			if(empty($pensAppModel)){
				throw new CException('no Pens app model found');
			}

			$erpParams = array(
				'installation_id' => Settings::getCfg('erp_installation_id'),
				'current_installation_only' => true
			);
			$erpClient = new ErpApiClient2();
			$erpCrmInfo = $erpClient->getInstallationCrmData();
			if(!$erpCrmInfo){
				throw new CException('No reply from ERP server');
			}

			// American clients go to North American server, rest of the world go to European server
			if(in_array($erpCrmInfo['country'], self::$americanCountries)){
				$apiKey = self::LECTORA_SERVER_KEY_US;
				$server = self::LECTORA_SERVER_URL_US;
				$settingsURL = self::LECTORA_URL_US;
			} else {
				$apiKey = self::LECTORA_SERVER_KEY_EU;
				$server = self::LECTORA_SERVER_URL_EU;
				$settingsURL = self::LECTORA_URL_EU;
			}

			$company_name = $erpCrmInfo['company_name'];
            if (empty($erpCrmInfo['company_name'])) {
                list($derdomain, $domain) = explode('@', $erpCrmInfo['contact_email']);
                if (empty($domain)) {
                    throw new CException('Invalid email');
                }
                $arr = explode('.', $domain);
                if (count($arr) < 2) {
                    throw new CException('Invalid email domain');
                }
                $company_name = $arr[0];
            }

			// Override for testing purposes:
			// Todo: comment/delete the following 3 lines when going to production
			//$apiKey = self::LECTORA_SERVER_KEY_TEST;
			//$server = self::LECTORA_SERVER_URL_TEST;
			//$settingsURL = self::LECTORA_URL_TEST;

			Settings::save('lectora_url', $settingsURL);
			$pensUrl = str_replace('http://', 'https://', Docebo::getRootBaseUrl(true)."/pens");
			$getParams = array(
				'op' => self::LECTORA_OPERATION,
				'apikey' => $apiKey,
				'email' => $erpCrmInfo['contact_email'],
				'company' => $company_name,
				'fname' => 'N/A',
				'lname' => 'N/A',
				'phone' => 'N/A',
				'trialSrc' => self::LECTORA_TRIAL_SOURCE,
				'endpoint' => $pensUrl,
				'uname' => $pensAppModel->user_id,
				'pword' => $pensAppModel->password
			);
			$getString = "";
			foreach($getParams as $key => $value){
				$getString .= "&".$key."=".urlencode($value);
			}
			$getTrialUrl = "https://".$server."/app/jsp/subscriptionworker.jsp?".ltrim($getString, '&');

			$ch = curl_init($getTrialUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			$response = CJSON::decode($response);

            Yii::log('Lectora cURL command: ' . $getTrialUrl);
            Yii::log('Lectora cURL response: ' . print_r($response,true));

			if($response['errorMsg'] > 0){
				throw new CException('(cURL) '.$response['errorMsg']);
			} else {
				$lectoraUsername = $response['username'];
				$lectoraPassword = $response['password'];
				Yii::log('Lectora Online Trial successfully created. Username: '.$lectoraUsername.' / Password: '.$lectoraPassword, CLogger::LEVEL_INFO);
			}
		} catch(CException $e){
			Yii::log('Could not create Lectora Online Trial: '.$e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}
}