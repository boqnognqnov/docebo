<?php

class LectoraAppController extends Controller {

	protected $_assetsUrl;

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'actions' => array('webhook'),
			'users' => array('*'),
		);


		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionIndex() {
		Yii::app()->getClientScript()->registerCssFile($this->getAssetsUrl() . "/css/lectora.css");

		$lectora_url = '';
		$errors = array();

		$pensApplication = PensApplication::model()->findByAttributes(array('related_plugin' => 'Lectora'));

		// handle form submit
		if (isset($_POST['submit'])) {
			// validate and save URL
			$lectora_url = Yii::app()->request->getParam('lectora_url', false);
			$pattern = '#^(https://){1}([a-zA-Z]*\.)?(lectoraonline\.com/app){1}[/]*$#';
			if (preg_match($pattern, $lectora_url)) {
				Settings::save('lectora_url', $lectora_url);
				Yii::app()->user->setFlash('success', Yii::t('standard', '_OPERATION_SUCCESSFUL'));
			} else {
				$errors[] = Yii::t('lectora', 'You have entered an invalid Lectora On-Line URL. The URL must be similar to {XXX}', array('{XXX}' => '<strong>"https://eu.lectoraonline.com/app/"</strong>'));
			}
			// validate and save category
			$lectora_category = Yii::app()->request->getParam('lectora_category', NULL);
			if (empty($lectora_category)) { $lectora_category = LearningCourseCategoryTree::getRootNodeId(); } //make sure to have a valid value as category
			if (!empty($pensApplication)) {
				$pensApplication->category_id = $lectora_category;
				$result = $pensApplication->save();
				if (!$result) { $errors[] = Yii::t('lectora', 'An error occurred while trying to save destination category'); }
			}
		}

		if(empty($lectora_url)) $lectora_url = Settings::get('lectora_url'); // Try to fetch it from the settings first (if it's not changed after a form submit
		if(empty($lectora_url)) $lectora_url = 'https://'; // If the setting doesn't exist, show the https prefix

		if (!empty($pensApplication)) {
			$lectora_category = $pensApplication->category_id;
		} else {
			$lectora_category = LearningCourseCategoryTree::getRootNodeId();
		}

		$this->render('index', array(
			'errors' => $errors,
			'lectora_url' => $lectora_url,
			'lectora_category' => $lectora_category,
			'pensApplication' => $pensApplication
		));
	}



	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('LectoraApp.assets'));
		}
		return $this->_assetsUrl;
	}
}
