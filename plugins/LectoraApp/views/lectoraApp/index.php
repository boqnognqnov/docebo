<?php
$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('lectora', 'Lectora Online')
);

if (isset($breadcrumbs) && !empty($breadcrumbs)) {
	if (is_array($breadcrumbs)) {
		$_breadcrumbs = array_merge($_breadcrumbs, $breadcrumbs);
	} else {
		$_breadcrumbs[] = $breadcrumbs;
	}
}

$this->breadcrumbs = $_breadcrumbs;

$_password = '';
$_password .= '<div class="password-container hide-value">';
for ($i=0; $i<8; $i++) { $_password .= '<i class="fa fa-circle"></i>'; }
$_password .= '<span class="value">'.$pensApplication->password.'</span>';
$_password .= '</div>';
$_password .= '<div class="password-toggler">';
$_password .= '<i class="fa fa-eye"></i>';
$_password .= '</div>';


$_lmsUrl = Settings::get('url', '');
if (!empty($_lmsUrl)) {
	if (substr($_lmsUrl, -1) != '/') { $_lmsUrl .= '/'; }
	$_lmsUrl .= 'pens';
}

?>

<div class="row-fluid">
	<div class="span6">
		<h3 class="advanced-elucidat-settings-form-title"><?=Yii::t('lectora', 'Lectora On-Line Settings')?></h3>
	</div>
	<div class="span6">
		<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual">
			<?= Yii::t('apps', 'Read Manual'); ?>
		</a>
	</div>
</div>
<br>

<?php DoceboUI::printFlashMessages(); ?>

<?= CHtml::beginForm(); ?>

<?php if (is_array($errors) && !empty($errors)){ ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="errorSummary">
				<?= implode('<br/>', $errors) ?>
			</div>
		</div>
	</div>
<?php } ?>

<div class="lectora-settings">
	<div class="advanced-main">

		<div class="section">

			<div class="row odd">
				<div class="row">
					<div class="setting-name">
						<?= Yii::t('lectora', 'Lectora On-Line URL') ?>
						<p class="muted"><?= Yii::t('lectora', 'Your Lectora On-Line platform URL') ?></p>
					</div>
					<div class="values">
						<?= CHtml::textField('lectora_url', $lectora_url) ?>
					</div>
				</div>
			</div>

			<div class="row even">
				<div class="row">
					<div class="setting-name">
						<?= Yii::t('standard', '_CATEGORY') ?>
						<p class="muted"><?= Yii::t('lectora', 'Select destination category for Lectora online learning objects') ?></p>
					</div>
					<div class="values">
						<?= CHtml::dropDownList('lectora_category', $lectora_category, LearningCourseCategory::getCategoriesList(), array('encode' => false/*, 'prompt' => ''*/)) ?>
					</div>
				</div>
			</div>

			<div class="row odd">
				<div class="row">
					<div class="setting-name">
						<?= Yii::t('webinar', 'Account') ?>
					</div>
					<div class="values account-data">
						<p class="account-data-label"><?= Yii::t('lectora', 'Server URL') ?>:</p>
						<p class="account-data-value"><?= $_lmsUrl ?></p>
						<p class="account-data-label"><?= Yii::t('standard', '_USERNAME') ?>:</p>
						<p class="account-data-value"><?= $pensApplication->user_id ?></p>
						<p class="account-data-label"><?= Yii::t('standard', '_PASSWORD') ?>:</p>
						<div class="account-data-value"><?= $_password ?></div>
					</div>
				</div>
			</div>

			<br>

			<div class="row-fluid right">
				<?= CHtml::submitButton(Yii::t('app7020','Save Changes'), array(
					'class' => 'btn btn-docebo green big',
					'name' => 'submit'
				)); ?>
				<?= CHtml::submitButton(Yii::t('app7020','Cancel'), array(
					'class' => 'btn btn-docebo black big',
					'name' => 'cancel'
				)); ?>
			</div>

		</div>
	</div>
</div>

<?= CHtml::endForm() ?>

<script type="text/javascript">
	$(function(){
		$('#lectora_url').on('focus', function(){
			var val = $(this).val();
			if(val == "https://"){
				$(this).val('');
			}
		}).on('blur', function(){
			var val = $(this).val();
			if(val == ''){
				$(this).val('https://');
			}
		});

		$('#show-account-password').on('click', function() {
			$(this).addClass('display-none');
			$('#account-password').removeClass('display-none');
		});

		//password hide/show
		$('.account-data-value').on('click', '.fa.fa-eye, .fa.fa-eye-slash', function() {
			var t = $(this);
			var h = t.parent().parent().find('.password-container');
			if (t.hasClass('fa-eye')) {
				if (h.length > 0) {
					h.removeClass('hide-value');
					h.addClass('show-value');
					t.removeClass('fa-eye');
					t.addClass('fa-eye-slash');
				}
			} else if (t.hasClass('fa-eye-slash')) {
				if (h.length > 0) {
					h.removeClass('show-value');
					h.addClass('hide-value');
					t.removeClass('fa-eye-slash');
					t.addClass('fa-eye');
				}
			}
		});
	});
</script>