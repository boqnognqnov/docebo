<?php

class LectoraApp extends DoceboPlugin {

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'LectoraApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name = __CLASS__) {
		return parent::plugin($class_name);
	}

	public function init(){
		Yii::app()->event->on(EventManager::EVENT_INSTALL_PLUGIN_FROM_INSTALLER, array($this, 'initFromInstaller'));
	}

	public function initFromInstaller(){
		PluginManager::loadPlugin('LectoraApp');
		$installer = new LectoraTrialInstaller();
		$installer->installTrial();
	}

	public static function settingsUrl()
	{
		return Docebo::createAdminUrl('//LectoraApp/LectoraApp/index');
	}

	public function activate() {
		parent::activate();

		// Automatically add a new PENS record when Lectora App is activated
		$model = new PensApplication();
		$model->name = Yii::t('lectora', 'Lectora Online');
		$model->description = Yii::t('lectora', 'Auto-generated PENS App by Lectora Online plugin');
		$_generatedAccess = PensApplication::generateUniqueKey();
		$model->app_id = $_generatedAccess['app_id'];
		$model->user_id = $_generatedAccess['user_id'];
		$model->password = PensApplication::generatePassword();
		$model->enable = 1;
		$model->date_creation = Yii::app()->localtime->toLocalDateTime();
		$model->date_update = null;
		$model->not_editable = 1;
		$model->related_plugin = 'Lectora';
		$model->save();
	}

	public function deactivate() {
		// Remove the Lectora row from PENS app when deactivating
		PensApplication::model()->deleteAllByAttributes(array(
			'related_plugin' => 'Lectora'
		));

		Settings::remove('lectora_url');

		parent::deactivate();
	}

}
