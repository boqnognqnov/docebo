<?php

class LectoraAppModule extends CWebModule
{
	public function init(){
		// import the module-level models and components
		$this->setImport(array(
			'LectoraApp.assets.*',
			'LectoraApp.components.*'
		));

		$this->defaultController = 'lectoraApp';
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu()
	{
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('lectora', array(
			'icon' => 'admin-ico lectora',
			'app_icon' => 'fa-book', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//LectoraApp/LectoraApp/index'),
			'label' => 'Lectora Online',
		), array());
	}
}
