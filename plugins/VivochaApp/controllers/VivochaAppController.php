<?php

class VivochaAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {

		$settings = new VivochaAppSettingsForm();

		if (isset($_POST['VivochaAppSettingsForm'])) {
			$settings->setAttributes($_POST['VivochaAppSettingsForm']);

			if ($settings->validate()) {
				Settings::save('vivocha_code', $settings->vivocha_code, 'check', 1);
				Settings::save('vivocha_in_lms', $settings->vivocha_in_lms, 'textarea', 65535);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->vivocha_in_lms = Settings::get('vivocha_in_lms', '0');
		$settings->vivocha_code = Settings::get('vivocha_code', '');


		$this->render('settings', array('settings' => $settings));
	}

}
