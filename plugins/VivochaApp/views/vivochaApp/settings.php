<?php
/* @var $form CActiveForm */
/* @var $settings VivochaAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/elearning-docebo-per-vivochas-live-help/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-vivochas-live-help/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: Vivocha</h2>
<br>

<!--<div class="error-summary">--><?php //echo $form->errorSummary($settings); ?><!--</div>-->

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="http://www.vivocha.com/" target="_blank">http://www.vivocha.com/</a>
    </div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'vivocha_in_lms', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->checkBox($settings, 'vivocha_in_lms');?>
		<?=$form->error($settings, 'vivocha_in_lms'); ?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'vivocha_code', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textArea($settings, 'vivocha_code', array('class'=>'span6', 'rows'=>10));?>
		<?=$form->error($settings, 'vivocha_code'); ?>
	</div>
</div>



<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(document).ready(function(){
		var checkbox = $('#app-settings-form input[type="checkbox"]');
		var disableTextarea = function(){
			var textArea = $('#app-settings-form textarea');
			var isDisabled = (checkbox.is(':checked') == false);
			textArea.attr('readonly', isDisabled);
		};
		checkbox.click(disableTextarea);
		disableTextarea();
	});
</script>