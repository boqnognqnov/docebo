<?php

/**
 * Plugin for VivochaApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class VivochaApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'VivochaApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
	}


	public function deactivate() {
		parent::deactivate();
		Settings::save('vivocha_in_lms', '0', 'check', 1); // disable the feature
	}


}
