<?php

/**
 * VivochaAppSettingsForm class.
 *
 * @property string $vivocha_code
 * @property string $vivocha_in_lms
 */
class VivochaAppSettingsForm extends CFormModel {
	public $vivocha_code;
	public $vivocha_in_lms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('vivocha_code, vivocha_in_lms', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'vivocha_code' => Yii::t('standard', '_CODE'),
		    'vivocha_in_lms' => Yii::t('standard', '_ACTIVE'),
		);
	}


	public function beforeValidate() {
		$this->vivocha_in_lms = ($this->vivocha_in_lms ? '1' : '0');
		return parent::beforeValidate();
	}


}
