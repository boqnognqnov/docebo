<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class EnrollmentrulesAppModule extends CWebModule {


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
			'EnrollmentrulesApp.models.*',
			'EnrollmentrulesApp.components.*',
		));

		// Bind events to a single handler; event names taken from the _EVENTNAME_TYPE_MAP
		Yii::app()->event->on(EventManager::EVENT_NEW_GROUP_USER, array(new EnrollmentRulesAgent(), 'newGroupUserEvent'));
		Yii::app()->event->on(EventManager::EVENT_NEW_BRANCH_USER, array(new EnrollmentRulesAgent(), 'newBranchUserEvent'));
		Yii::app()->event->on(EventManager::EVENT_MASS_IMPORT_BRANCH_USERS, array(new EnrollmentRulesAgent(), 'newMassImportBranchUsers'));

	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('enrollmentrules', array(
			'icon' => 'admin-ico enrollment-rules',
			'link' => Docebo::createAdminUrl('enrollmentRules/index'),
			'label' => Yii::t('standard', '_ENROLLRULES'),
			'app_icon' => 'fa-sliders', // icon used in the new menu
		), array());
	}


}
