<?php

class EnrollmentRulesAgent extends CComponent
{
	const LOG_NAME = 'EnrollmentRules Agent';

	/**
	 * @param DEvent $event
	 */
	public function newGroupUserEvent(DEvent $event)  {
		try {
			$rules = CoreEnrollRule::getRulesByGroup($event->sender->idst);
			foreach ($rules as $rule)
			{
				$items = $rule->rule_type == CoreEnrollRule::RULE_GROUP_COURSE ? $rule->courseItems : $rule->curriculaItems;
				if (count($items))
				{
					$this->makeEnrollments($rule, $event->sender->idstMember, $items);
				}
			}
			Yii::log("Handled event ".EventManager::EVENT_NEW_GROUP_USER." for user ".$event->sender->idstMember, 'info', __CLASS__);
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), 'error', self::LOG_NAME);
		}
	}

	/**
	 * @param DEvent $event
	 */
	public function newBranchUserEvent(DEvent $event)  {
		try {
            $orgChartId = $event->sender->group->orgChartId;
            if (!$orgChartId)
                $orgChartId = $event->sender->group->getOrgChartId();
			$rules = CoreEnrollRule::getRulesByBranch($orgChartId);
			foreach ($rules as $rule)
			{
				$items = $rule->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE ? $rule->courseItems : $rule->curriculaItems;
				if (count($items))
				{
					$this->makeEnrollments($rule, $event->sender->idstMember, $items);
				}
			}
			Yii::log("Handled event ".EventManager::EVENT_NEW_BRANCH_USER." for user ".$event->sender->idstMember, 'info', __CLASS__);
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), 'error', self::LOG_NAME);
		}
	}

	public function newMassImportBranchUsers(DEvent $event){
		set_time_limit(3*3600);
		$users = $event->params['users'];
		$branches = $event->params['branches'];
		if(!$users || !$branches) return;
		try {
			$usersInBranches = $this->allocateUsersToBranch($branches, $users);
			foreach ($branches as $branch) {
				$rules = CoreEnrollRule::getRulesByBranch($branch);
				if (!empty($rules)) {
					foreach ($rules as $rule) {
						$items = $rule->rule_type == CoreEnrollRule::RULE_BRANCH_COURSE ? $rule->courseItems : $rule->curriculaItems;
						if(count($items) && isset($usersInBranches[$branch])){
							foreach ($usersInBranches[$branch] as $user) {
								$this->makeEnrollments($rule, $user, $items);
							}
						}
					}
				}
			}
		} catch(CException $e){
			Yii::log($e->getMessage(), 'error', self::LOG_NAME);
		}
	}

	public function makeEnrollments($rule, $userId, $items)
	{
		$enrolledArray = array();

		for ($i = 0; $i < count($items); $i++)
		{
			$enrolled = false;
			switch($rule->rule_type)
			{
				case CoreEnrollRule::RULE_GROUP_COURSE:
				case CoreEnrollRule::RULE_BRANCH_COURSE:
					$model = new LearningCourseuser();
					$enrolled = $model->subscribeUser($userId, Yii::app()->user->id, $items[$i]->item_id, 0, false, false, false, false, false, false, false, null, true);
					break;
				case CoreEnrollRule::RULE_GROUP_CURRICULA:
				case CoreEnrollRule::RULE_BRANCH_CURRICULA:
					$enrolled = LearningCoursepathUser::subscribeUser($userId, $items[$i]->item_id, true, true);
					break;
			}
			if ($enrolled)
				$enrolledArray[] = $items[$i];
		}
		if (count($enrolledArray)) //create enrollLog only in case we have at least one enrolled user
		{
			$log = new CoreEnrollLog();
			$log->rule_id = $rule->rule_id;
			$log->save(false);
			foreach ($enrolledArray as $item)
			{
				$logItem = new CoreEnrollLogItem();
				$logItem->enroll_log_id = $log->primaryKey;
				$logItem->user_id = $userId;
				$logItem->target_id = $item->item_id;
				$logItem->save(false);
			}
		}
	}

	/**
	 * Get a 2D array of branches with the preselected users
	 * layout: $array['branchId']['userId']
	 * @param array $branches
	 * @param array $users
	 * @return array
	 */
	public function allocateUsersToBranch($branches, $users){
		$result = array();
		if(empty($branches) || empty($users)){
			return $result;
		}
		foreach($branches as $branch){
			$usersInBranch = Yii::app()->db->createCommand("
				SELECT cgm.idstMember
				FROM ".CoreOrgChartTree::model()->tableName()." AS cot
				INNER JOIN ".CoreGroupMembers::model()->tableName()." AS cgm ON cot.idst_oc = cgm.idst
				WHERE cot.idOrg = ".$branch." AND cgm.idstMember > 0 AND cgm.idstMember IN (".implode(',',$users).")
			")->queryAll();
			foreach($usersInBranch as $user){
				$result[$branch][] = $user['idstMember'];
			}
		}
		return $result;
	}

}