<?php
/* @var $form CActiveForm */
/* @var $settings TwitterAppSettingsForm */
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'http://www.docebo.com/it/lms-elearning-moduli-e-integrazioni/' : 'http://www.docebo.com/lms-elearning-modules-integrations/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('standard','Settings') ?>: <?= Yii::t('standard', '_TWITTER') ?></h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="https://dev.twitter.com/" target="_blank">https://dev.twitter.com/</a>
    </div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'twitter_consumer', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'twitter_consumer');?>
	</div>
</div>

<div class="control-group">
	<?=$form->labelEx($settings, 'twitter_secret', array('class'=>'control-label'));?>
	<div class="controls">
		<?=$form->textField($settings, 'twitter_secret');?>
	</div>
</div>

<div class="form-actions">
	<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
	&nbsp;
	<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
</div>

<?php $this->endWidget(); ?>