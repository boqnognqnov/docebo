<?php

/**
 * TwitterAppSettingsForm class.
 *
 * @property string $twitter_consumer
 * @property string $twitter_secret
 */
class TwitterAppSettingsForm extends CFormModel {
	public $twitter_consumer;
	public $twitter_secret;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('twitter_consumer, twitter_secret', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'twitter_consumer' => Yii::t('configuration', '_SOCIAL_TWITTER_CONSUMER'),
			'twitter_secret' => Yii::t('configuration', '_SOCIAL_TWITTER_SECRET'),
		);
	}
}
