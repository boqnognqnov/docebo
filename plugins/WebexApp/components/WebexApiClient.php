<?php
/**
 * XML API Client for Cisco WebEx Meetings
 * http://www.webex.com/products/web-conferencing.html
 *
 * @author Plamen Petkov
 *
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 *
 */
class WebexApiClient extends CComponent {

	const MTG_TYPE_RAS = 10;
	const MTG_TYPE_TRS = 128;
	const MTG_TYPE_ONS = 129;
	const MTG_TYPE_SC3 = 552;

	const WEBEX_TIMEZONE_UTC = 20; //Greenwich timezone, GMT +0:00
	const WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING = 150; //Cape Verde timezone, GMT -1:00, no daylight

	const WEBEX_SITE_ADMIN_LISTED = 0;
	const WEBEX_SITE_ADMIN_UNLISTED = 1;

    // Sometimes used to test with fiddler
    public $proxyHost = false;
    public $proxyPort = false;

    // Will be loaded From settings
    private $_siteName = "";
    private $_siteId = "";
    private $_partnerId = "";
    private $_webexId = "";
    private $_password = "";


    // Internally used
    private $_client = null;
    private $_baseUrl = "";

    // XML Template used for all API calls
    private $_xmlTmpl = '<?xml version="1.0" encoding="UTF-8"?>
                        <message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <header>
                                <securityContext>
                                    <webExID></webExID>
                                    <password></password>
                                    <siteName></siteName>
                                </securityContext>
                            </header>
                            <body>
                                <bodyContent>
                                </bodyContent>
                            </body>
                        </message>
            ';


	private $_redoErrors = array(
		'All meetings must be unlisted'=>'All meetings must be unlisted'
	);


    /**
     * Constructor
     */
    public function __construct($sitename = null, $siteid = null, $partnerid = null, $webexid = null, $password = null) {
        $this->init($sitename, $siteid, $partnerid, $webexid, $password);
    }


    /**
     * Initialize component
     *
     */
	public function init($sitename = null, $siteid = null, $partnerid = null, $webexid = null, $password = null)
	{

        Yii::import('common.extensions.httpclient.*');

        // Get these from settings
        // Read WebEx Meetings documentation
        /*$this->_siteName = "apidemoeu";
        $this->_siteId = "690319";
        $this->_partnerId = "g0webx!";
        $this->_webexId = "plamenwebex";
        $this->_password = "";*/

		if(!$sitename || !$siteid || !$partnerid || !$webexid || !$password) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'webex'));
			if($account) {
				$sitename = $account->settings->sitename;
				$siteid = $account->settings->siteid;
				$partnerid = $account->settings->partnerid;
				$webexid = $account->settings->webexid;
				$password = $account->settings->password;
			}
		}

        $this->_siteName = $sitename;
        $this->_siteId = $siteid;
        $this->_partnerId = $partnerid;
        $this->_webexId = $webexid;
        $this->_password = $password;

        // Build base URL
        $this->_baseUrl = "https://" . $this->_siteName . ".webex.com/WBXService/XMLService";


        // TESTING with fiddler
        //$this->proxyHost = '127.0.0.1';
        //$this->proxyPort = '8888';


    }


    /**
     * Add login information to XML. Called before ALL API calls
     *
     * @param object $xml
     * @return object
     */
    private function securityHeader($xml) {

        $xml->header->securityContext->webExID = $this->_webexId;
        $xml->header->securityContext->password = $this->_password;
        $xml->header->securityContext->siteName = $this->_siteName;

        return $xml;
    }



    /**
     * Execute API Call and return uniformed result to be analyzed by caller
     *
     * @param object $xml
	 * @param bool $redoIfError
	 * @param bool $noHardErrors: doesn't throw an error, but returns an array with error message instead.
     * @throws Exception
     * @throws ErrorException
     * @return mixed
     */
    private function call($xml, $redoIfError = false, $noHardErrors = false) {
		$action = '';
		foreach($xml->body->bodyContent->attributes() as $key => $value)
		{
			if($key == 'xsi:type')
				$action = $value->__toString();
		}

        // Set secutiry header values
        $xml = $this->securityHeader($xml);

        $url = $this->_baseUrl;

        $this->_client = new EHttpClient($url, array(
                'adapter'      => 'EHttpClientAdapterCurl',
                'proxy_host'   => $this->proxyHost,
                'proxy_port'   => $this->proxyPort,
				//'disable_ssl_verifier' => true, //comment on production .. I guess
        ));

        // Put XML string into payload
        $this->_client->setRawData($xml->asXml());

        // Execute request
        $response = $this->_client->request(EHttpClient::POST);

        // Check if we've got an error; throw exception with error message
        if ($this->_client->getLastResponse()->isError()) {
            throw new Exception($this->_client->getLastResponse()->getMessage(), $this->_client->getLastResponse()->getStatus());
        }

        if (!$response) {
            throw new Exception('Invalid response from server');
        }


        // Decode response to an array
        $body = $response->getBody();

        $xmlAnswer = null;

        if ($body) {

            // Remove unused attributes! MUST BE DONE!
            $body = preg_replace('/<[a-z]{1,}:/','<',$body);
            $body = preg_replace('/<\/[a-z]{1,}:/','</',$body);

            // STOP XML runtime errors
            libxml_use_internal_errors(true);

            // Load XML object from request body
            $xmlAnswer = new SimpleXMLElement($body);

            if($xmlAnswer){
				$process_error = true;
				if($xmlAnswer->header->response->result != 'SUCCESS' && ($action == 'java:com.webex.service.binding.meeting.DelMeeting' || $action === 'java:com.webex.service.binding.event.DelEvent' || $action === 'java:com.webex.service.binding.training.DelTrainingSession'))
				{
					$process_error = false;
					Yii::log('Error during WebEx conference deletion with id: "'.$xml->body->bodyContent->meetingKey->__toString().'" and returned error: "'.$xmlAnswer->header->response->reason->__toString().'"', CLogger::LEVEL_ERROR);
				}
                if($xmlAnswer->header->response->result != 'SUCCESS' && $process_error){
                	$error = "An error was returned from the API: '" . $xmlAnswer->header->response->reason->__toString() . "'";
	                if($redoIfError && in_array($xmlAnswer->header->response->reason->__toString(), $this->_redoErrors)
		                && isset($xml->body->bodyContent->accessControl->meetingPassword)
	                    && $xml->body->bodyContent->accessControl->meetingPassword
	                ){
		                unset($xml->body->bodyContent->accessControl->isPublic);
		                $xml->body->bodyContent->accessControl->addChild('isPublic','false');
		                return  $this->call($xml);

	                }else{
		                //$error = "An error was returned from the API: ".
		                //        $xmlAnswer->header->response->reason->__toString() . ' ('.
		                //        $xmlAnswer->header->response->exceptionID->__toString() .')';
						if($noHardErrors) {
							return array('error' => $error);
						} else {
							throw new Exception($error);
						}
	                }

                }
            } else {
                $errors = '';
                foreach(libxml_get_errors() as $error) {
                    $errors .= ",\t" . $error->message;
                }
                $error = "Unable to parse XML response, errors occured: $errors";
                throw new Exception($error);
            }
        }

        // Convert to array
        $json = json_encode($xmlAnswer);
        $data = json_decode($json, TRUE);

        // Always add response status/message
        $result["response_status"] = $this->_client->getLastResponse()->getStatus();
        $result["response_message"] = $this->_client->getLastResponse()->getMessage();
        $result["data"] = $data;

        // Return array
        return $result;

    }

	/**
	 * Create meeting remotely
	 *
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param int $siteAdminOption
	 * @param string $password
	 * @return boolean|array
	 */
	public function createMeeting($roomName, $startDateTime, $durationMinutes, $siteAdminOption = 0, $password = "")
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.CreateMeeting";

		$metaData = $xml->body->bodyContent->addChild('metaData');
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		if ($password) {
			$accessControl->addChild('meetingPassword', $password);
		}

		$metaData->addChild('confName', $roomName);

		if ($this->_siteName == "apidemoeu") {
			$metaData->addChild('meetingType', self::MTG_TYPE_RAS);
		}

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// Passing timezone to Webex as UTC
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if (Yii::app()->localtime->inDaylightSavings($startDateTime)) {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		} else {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}

		$schedule->addChild('duration', $durationMinutes);

		// Only add this param in case of listed meetings, would throw an error if Webex admin is setup to unlisted only.
		if($siteAdminOption == self::WEBEX_SITE_ADMIN_LISTED) $accessControl->addChild('isPublic', 'true');

		$result = $this->call($xml, true);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}

	/**
	 * Update a meeting
	 *
	 * @param string $meetingKey
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param string $password
	 * @return boolean|array
	 */
	public function updateMeeting($meetingKey, $roomName, $startDateTime, $durationMinutes, $password = "")
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.SetMeeting";

		$xml->body->bodyContent->addChild('meetingkey', $meetingKey);
		$metaData = $xml->body->bodyContent->addChild('metaData');
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		$accessControl->addChild('isPublic', 'false');
		if ($password)
			$accessControl->addChild('meetingPassword', $password);

		$metaData->addChild('confName', $roomName);

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// Passing UTC timezone to webex
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if (Yii::app()->localtime->inDaylightSavings($startDateTime)) {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		} else {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}

		$schedule->addChild('duration', $durationMinutes);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];


		return $result;
	}



	/**
	 * Delete a meeting remotely
	 *
	 * @param string $meetingKey
	 * @return boolean|array
	 */
	public function deleteMeeting($meetingKey)
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.DelMeeting";
		$xml->body->bodyContent->addChild('meetingKey', $meetingKey);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}



	/**
	 * Create event remotely
	 *
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param int $siteAdminOption
	 * @param string $password
	 * @return boolean|array
	 */
	public function createEvent($roomName, $startDateTime, $durationMinutes, $siteAdminOption = 0, $password = "")
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.event.CreateEvent";

		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		if($siteAdminOption == self::WEBEX_SITE_ADMIN_LISTED) $accessControl->addChild('listing', 'PUBLIC');
		if($password)
			$accessControl->addChild('sessionPassword', $password);

		$metaData = $xml->body->bodyContent->addChild('metaData');
		$metaData->addChild('sessionName', $roomName);

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// UTC timezone to webex
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if(Yii::app()->localtime->inDaylightSavings($startDateTime)){
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		}else{
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}
		$schedule->addChild('duration', $durationMinutes);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}


	/**
	 * Update an event
	 *
	 * @param string $sessionKey
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param string $password
	 * @return boolean|array
	 */
	public function updateEvent($sessionKey, $roomName, $startDateTime, $durationMinutes, $password = "")
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.event.SetEvent";

		$xml->body->bodyContent->addChild('sessionKey', $sessionKey);

		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		$accessControl->addChild('listing', 'PUBLIC');
		if ($password)
			$accessControl->addChild('sessionPassword', $password);

		$metaData = $xml->body->bodyContent->addChild('metaData');
		$metaData->addChild('sessionName', $roomName);

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// UTC timezone to Webex
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if (Yii::app()->localtime->inDaylightSavings($startDateTime)) {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		} else {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}
		$schedule->addChild('duration', $durationMinutes);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}



	/**
	 * Delete an event remotely
	 *
	 * @param string $sessionKey
	 * @return boolean|array
	 */
	public function deleteEvent($sessionKey)
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.event.DelEvent";
		$xml->body->bodyContent->addChild('sessionKey', $sessionKey);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}



	/**
	 * Create session remotely
	 *
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param int $siteAdminOption
	 * @param string $password
	 * @return boolean|array
	 */
	public function createTrainingSession($roomName, $startDateTime, $durationMinutes, $siteAdminOption = 0, $password = "") {
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.training.CreateTrainingSession";

		$metaData = $xml->body->bodyContent->addChild('metaData');
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		if($siteAdminOption == self::WEBEX_SITE_ADMIN_LISTED) $accessControl->addChild('listing', 'PUBLIC');
		if($password)
			$accessControl->addChild('sessionPassword', $password);

		$metaData->addChild('confName', $roomName);

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// Passing timezone to Webex as UTC
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if (Yii::app()->localtime->inDaylightSavings($startDateTime)) {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		} else {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}

		$schedule->addChild('duration', $durationMinutes);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}



	/**
	 * Update a training session
	 *
	 * @param string $sessionKey
	 * @param string $roomName
	 * @param string $startDateTime
	 * @param int $durationMinutes
	 * @param string $password
	 * @return boolean|array
	 */
	public function updateTrainingSession($sessionKey, $roomName, $startDateTime, $durationMinutes, $password = "") {
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.training.SetTrainingSession";

		$xml->body->bodyContent->addChild('sessionKey', $sessionKey);
		$metaData = $xml->body->bodyContent->addChild('metaData');
		$schedule = $xml->body->bodyContent->addChild('schedule');
		$accessControl = $xml->body->bodyContent->addChild('accessControl');
		$accessControl->addChild('listing', 'PUBLIC');
		if($password)
			$accessControl->addChild('sessionPassword', $password);

		$metaData->addChild('confName', $roomName);

		// NOTE: we expect $startDateTime is ISO8601 format with timezone specified (es. 2015-09-24T03:00:00+0000), but
		// in some cases a mysql ISO format (es. 2015-09-24 03:00:00) may be passed.
		// Detect format and convert it if needed.
		$pattern = '/^([0-9]{4})-(1[0-2]{1}|0[1-9]{1})-([0-2]{1}[0-9]{1}|3[01]{1}) ([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1}):([0-5]{1}[0-9]{1})$/';
		if (preg_match($pattern, $startDateTime)) {
			//this will output datetime with timezone
			$tmp = Yii::app()->localtime->toLocalDateTime($startDateTime);
			$startDateTime = Yii::app()->localtime->fromLocalDateTime($tmp);
		}

		// Passing UTC timezone to webex
		$dt = new DateTime($startDateTime);
		$dt->setTimezone(new DateTimeZone('UTC'));
		$schedule->addChild('startDate', $dt->format('m/d/Y H:i:00'));
		if (Yii::app()->localtime->inDaylightSavings($startDateTime)) {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC_DAYLIGHTSAVING);
		} else {
			$schedule->addChild('timeZoneID', self::WEBEX_TIMEZONE_UTC);
		}

		$schedule->addChild('duration', $durationMinutes);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}

	/**
	 * Delete a training session remotely
	 *
	 * @param string $sessionKey
	 * @return boolean|array
	 */
	public function deleteTrainingSession($sessionKey)
	{
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.training.DelTrainingSession";
		$xml->body->bodyContent->addChild('sessionKey', $sessionKey);

		$result = $this->call($xml);

		if ($result['data']['header']['response']['result'] != 'SUCCESS') {
			return false;
		}

		$result = $result['data']['body']['bodyContent'];

		return $result;
	}



    /**
     * Get list of active/running meetings
     *
     * @throws ErrorException
     * @return array
     */
	public function getActiveMeetings()
	{
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.ep.LstOpenSession";

        $result = array();
        try {
            $result = $this->call($xml);
            if (isset($result['data']['body']['bodyContent'])) {
                $returned = (int) $result['data']['body']['bodyContent']['matchingRecords']['returned'];
                if ($returned == 1) {
                    $sessions = array(0 => $result['data']['body']['bodyContent']['services']['sessions']);
                    return $sessions;
				} else if ($returned > 1) {
                    return $result['data']['body']['bodyContent']['services']['sessions'];
                }
            }
        } catch (ErrorException $e) {
            if(!preg_match('/000015/', $e->getMessage())){
                throw $e;
            }
        }

        return $result;
    }


    /**
     * Get URL to start a meeting as a Host
     * @param string $meetingKey
     * @return string
     */
	public function getHostStartMeetingUrl($meetingKey)
	{
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.GethosturlMeeting";
        $xml->body->bodyContent->addChild('sessionKey', $meetingKey);

        $result = $this->call($xml);

        if ($result['data']['header']['response']['result'] != 'SUCCESS') {
            return false;
        }

        return $result['data']['body']['bodyContent']['hostMeetingURL'];
    }


	/**
	 * Returns the URL to start an event (using Webex URL API)
	 * @param $meetingKey
	 */
	public function getHostStartEventUrl($meetingKey) {
		return $this->_baseUrl = "https://" . $this->_siteName . ".webex.com/".$this->_siteName."/m.php?AT=TE&MK=".$meetingKey;
	}


    /**
     * Return URL for attendee to join
     *
     * @param string $meetingKey
     * @param string $attendeeName
     * @param string $attendeeEmail
     * @return string
     */
	public function getAttendeeJoinMeetingUrl($meetingKey, $attendeeName, $attendeeEmail)
	{
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.GetjoinurlMeeting";
        $xml->body->bodyContent->addChild('sessionKey', $meetingKey);
        $xml->body->bodyContent->addChild('attendeeName', $attendeeName);
        $xml->body->bodyContent->addChild('attendeeEmail', $attendeeEmail);

        $result = $this->call($xml);

        if ($result['data']['header']['response']['result'] != 'SUCCESS') {
            return false;
        }

        return $result['data']['body']['bodyContent']['joinMeetingURL'];
    }



	/**
	 * Return URL for attendee to register to an event
	 *
	 * @param string $meetingKey
	 * @param string $firstname
	 * @param string $lastname
	 * @param string $attendeeEmail
	 * @return string
	 */
	public function getAttendeeRegisterEventUrl($meetingKey, $firstname, $lastname, $attendeeEmail)  {
		return "https://" . $this->_siteName . ".webex.com/".$this->_siteName."/m.php?AT=EN&MK=".$meetingKey
			."&AE=".urlencode($attendeeEmail)."&FN=".urlencode($firstname)."&LN=".urlencode($lastname) . '&servicename=EC';
	}



	/**
	 * Return URL for attendee to join an event
	 *
	 * @param string $meetingKey
	 * @param string $firstname
	 * @param string $lastname
	 * @param string $attendeeEmail
	 * @return string
	 */
	public function getAttendeeJoinEventUrl($meetingKey, $firstname, $lastname, $attendeeEmail)  {
		return "https://" . $this->_siteName . ".webex.com/".$this->_siteName."/m.php?AT=JE&MK=".$meetingKey
			."&AE=".urlencode($attendeeEmail)."&FN=".urlencode($firstname)."&LN=".urlencode($lastname) . '&servicename=EC';
	}


    /**
     * Return "invite" URL
     *
     * @param string $meetingKey
     * @param string $attendeeName
     * @param string $attendeeEmail
     * @return string
     */
    public function getAttendeeInviteMeetingUrl($meetingKey, $attendeeName, $attendeeEmail)  {
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meeting.GetjoinurlMeeting";
        $xml->body->bodyContent->addChild('sessionKey', $meetingKey);
        $xml->body->bodyContent->addChild('attendeeName', $attendeeName);
        $xml->body->bodyContent->addChild('attendeeEmail', $attendeeEmail);

        $result = $this->call($xml);

        if ($result['data']['header']['response']['result'] != 'SUCCESS') {
            return false;
        }

        return $result['data']['body']['bodyContent']['inviteMeetingURL'];
    }




    /**
     * Get WebEx API version
     *
     * @return string
     */
    public function getApiVersion() {
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.ep.GetAPIVersion";
        $result = $this->call($xml);

        if ($result['data']['header']['response']['result'] != 'SUCCESS') {
            return false;
        }

        $version = $result['data']['body']['bodyContent']['apiVersion'];

        return $version;

    }


    /**
     * Get a URL to direct login of the user and redirect to Webex
     *
     * @return string
     */
    public function getLoginUserUrl() {
        $xml = new SimpleXMLElement($this->_xmlTmpl);
        $xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.user.GetloginurlUser";
        $xml->body->bodyContent->addChild('webExID', 'docebo');

        $result = $this->call($xml);

        if ($result['data']['header']['response']['result'] != 'SUCCESS') {
            return false;
        }

        $url = $result['data']['body']['bodyContent']['userLoginURL'];

        return $url;
    }





    /**
     * Get WebEx list of available meeting types at the WebEx site.
     *
     * @param string $typeId
     * @return string
     */
    public function getMeetingTypes($typeId=false) {
    	$xml = new SimpleXMLElement($this->_xmlTmpl);
    	$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.meetingtype.LstMeetingType";

    	if ($typeId !== false) {
    		$xml->body->bodyContent->addChild('meetingTypeID', $typeId);
    	}
    	$result = $this->call($xml);
    	if ($result['data']['header']['response']['result'] != 'SUCCESS') {
    		return false;
    	}
    	$list = $result['data']['body']['bodyContent']['meetingType'];
    	return $list;

    }

	/**
	 * get the recording for a specified meeting
	 *
	 * @param int $sessionId
	 * @return mixed
	 */
	public function retrieveRecordingLink($sessionId){
		$xml = new SimpleXMLElement($this->_xmlTmpl);
		$xml->body->bodyContent['xsi:type'] = "java:com.webex.service.binding.ep.LstRecording";
		$xml->body->bodyContent->addChild('sessionKey', $sessionId);
		return $this->call($xml, false, true);
	}

}


/*




// Meeting types reported from "apidemoeu" (the DEMO WebEx site):

array
(
    0 => array
    (
        'productCodePrefix' => 'RAS'
        'active' => 'ACTIVATED'
        'name' => 'Access Anywhere session'
        'displayName' => 'Access Anywhere session'
        'limits' => array
        (
            'maxAppShareDuration' => '1440'
            'maxAppShareUser' => '999999'
            'maxDesktopShareDuration' => '1440'
            'maxDesktopShareUser' => '999999'
            'maxFileTransferUser' => '999999'
            'maxMeetingDuration' => '1440'
            'maxMeetingUser' => '999999'
            'maxRecordUser' => '0'
            'maxVideoDuration' => '0'
            'maxVideoUser' => '0'
            'maxWebTourDuration' => '0'
            'maxWebTourUser' => '0'
        )
        'options' => array
        (
            'supportAppShare' => 'true'
            'supportAppShareRemote' => 'true'
            'supportAttendeeRegistration' => 'false'
            'supportRemoteWebTour' => 'false'
            'supportWebTour' => 'false'
            'supportFileShare' => 'false'
            'supportChat' => 'false'
            'supportCobrowseSite' => 'false'
            'supportCorporateOfficesSite' => 'false'
            'supportDesktopShare' => 'true'
            'supportDesktopShareRemote' => 'true'
            'supportFileTransfer' => 'true'
            'supportInternationalCallOut' => 'false'
            'supportJavaClient' => 'false'
            'supportMacClient' => 'false'
            'supportMeetingCenterSite' => 'true'
            'supportMeetingRecord' => 'false'
            'supportMultipleMeeting' => 'true'
            'supportOnCallSite' => 'true'
            'supportOnStageSite' => 'false'
            'supportPartnerOfficesSite' => 'false'
            'supportPoll' => 'false'
            'supportPresentation' => 'false'
            'supportSolarisClient' => 'false'
            'supportSSL' => 'true'
            'supportE2E' => 'false'
            'supportPKI' => 'false'
            'supportTeleconfCallIn' => 'false'
            'supportTeleconfCallOut' => 'false'
            'supportTollFreeCallIn' => 'false'
            'supportVideo' => 'false'
            'supportVoIP' => 'true'
            'supportWebExComSite' => 'false'
            'supportWindowsClient' => 'true'
            'supportQuickStartAttendees' => 'false'
            'supportQuickStartHost' => 'false'
            'hideInScheduler' => 'false'
        )
        'phoneNumbers' => array
        (
            'primaryTollCallInNumber' => '+1-415-655-0001'
            'primaryTollFreeCallInNumber' => '+1-855-749-4750'
        )
        'meetingTypeID' => '10'
        'serviceTypes' => array
        (
            'serviceType' => array
            (
                0 => 'MeetingCenter'
                1 => 'SalesCenter'
                2 => 'SupportCenter'
                3 => 'TrainingCenter'
            )
        )
    )
    1 => array
    (
        'productCodePrefix' => 'TRS'
        'active' => 'ACTIVATED'
        'name' => 'TC Eval 4x20'
        'displayName' => 'TC Eval 4x20'
        'limits' => array
        (
            'maxAppShareDuration' => '20'
            'maxAppShareUser' => '4'
            'maxDesktopShareDuration' => '20'
            'maxDesktopShareUser' => '4'
            'maxFileTransferUser' => '4'
            'maxMeetingDuration' => '20'
            'maxMeetingUser' => '4'
            'maxRecordUser' => '4'
            'maxVideoDuration' => '20'
            'maxVideoUser' => '4'
            'maxWebTourDuration' => '20'
            'maxWebTourUser' => '4'
        )
        'options' => array
        (
            'supportAppShare' => 'true'
            'supportAppShareRemote' => 'true'
            'supportAttendeeRegistration' => 'true'
            'supportRemoteWebTour' => 'true'
            'supportWebTour' => 'true'
            'supportFileShare' => 'false'
            'supportChat' => 'true'
            'supportCobrowseSite' => 'false'
            'supportCorporateOfficesSite' => 'false'
            'supportDesktopShare' => 'true'
            'supportDesktopShareRemote' => 'true'
            'supportFileTransfer' => 'true'
            'supportInternationalCallOut' => 'true'
            'supportJavaClient' => 'true'
            'supportMacClient' => 'false'
            'supportMeetingCenterSite' => 'true'
            'supportMeetingRecord' => 'true'
            'supportMultipleMeeting' => 'true'
            'supportOnCallSite' => 'false'
            'supportOnStageSite' => 'false'
            'supportPartnerOfficesSite' => 'false'
            'supportPoll' => 'true'
            'supportPresentation' => 'true'
            'supportSolarisClient' => 'false'
            'supportSSL' => 'true'
            'supportE2E' => 'false'
            'supportPKI' => 'false'
            'supportTeleconfCallIn' => 'true'
            'supportTeleconfCallOut' => 'true'
            'supportTollFreeCallIn' => 'true'
            'supportVideo' => 'true'
            'supportVoIP' => 'true'
            'supportWebExComSite' => 'false'
            'supportWindowsClient' => 'true'
            'supportQuickStartAttendees' => 'false'
            'supportQuickStartHost' => 'false'
            'hideInScheduler' => 'false'
        )
        'phoneNumbers' => array
        (
            'primaryTollCallInNumber' => '+1-415-655-0001'
            'primaryTollFreeCallInNumber' => '+1-855-749-4750'
        )
        'meetingTypeID' => '128'
        'serviceTypes' => array
        (
            'serviceType' => array
            (
                0 => 'MeetingCenter'
                1 => 'TrainingCenter'
            )
        )
    )
    2 => array
    (
        'productCodePrefix' => 'ONS'
        'active' => 'ACTIVATED'
        'name' => 'OS Eval 4x20'
        'displayName' => 'OS Eval 4x20'
        'limits' => array
        (
            'maxAppShareDuration' => '20'
            'maxAppShareUser' => '4'
            'maxDesktopShareDuration' => '20'
            'maxDesktopShareUser' => '4'
            'maxFileTransferUser' => '0'
            'maxMeetingDuration' => '20'
            'maxMeetingUser' => '4'
            'maxRecordUser' => '4'
            'maxVideoDuration' => '0'
            'maxVideoUser' => '0'
            'maxWebTourDuration' => '20'
            'maxWebTourUser' => '4'
        )
        'options' => array
        (
            'supportAppShare' => 'true'
            'supportAppShareRemote' => 'false'
            'supportAttendeeRegistration' => 'true'
            'supportRemoteWebTour' => 'false'
            'supportWebTour' => 'true'
            'supportFileShare' => 'false'
            'supportChat' => 'true'
            'supportCobrowseSite' => 'false'
            'supportCorporateOfficesSite' => 'false'
            'supportDesktopShare' => 'true'
            'supportDesktopShareRemote' => 'false'
            'supportFileTransfer' => 'false'
            'supportInternationalCallOut' => 'false'
            'supportJavaClient' => 'true'
            'supportMacClient' => 'true'
            'supportMeetingCenterSite' => 'false'
            'supportMeetingRecord' => 'true'
            'supportMultipleMeeting' => 'true'
            'supportOnCallSite' => 'false'
            'supportOnStageSite' => 'true'
            'supportPartnerOfficesSite' => 'false'
            'supportPoll' => 'true'
            'supportPresentation' => 'true'
            'supportSolarisClient' => 'true'
            'supportSSL' => 'true'
            'supportE2E' => 'false'
            'supportPKI' => 'false'
            'supportTeleconfCallIn' => 'false'
            'supportTeleconfCallOut' => 'false'
            'supportTollFreeCallIn' => 'false'
            'supportVideo' => 'false'
            'supportVoIP' => 'true'
            'supportWebExComSite' => 'false'
            'supportWindowsClient' => 'true'
            'supportQuickStartAttendees' => 'false'
            'supportQuickStartHost' => 'false'
            'hideInScheduler' => 'false'
        )
        'phoneNumbers' => array
        (
            'primaryTollCallInNumber' => '+1-415-655-0001'
            'primaryTollFreeCallInNumber' => '+1-855-749-4750'
        )
        'meetingTypeID' => '129'
        'serviceTypes' => array
        (
            'serviceType' => 'EventCenter'
        )
    )
    3 => array
    (
        'productCodePrefix' => 'SC3'
        'active' => 'ACTIVATED'
        'name' => 'SC-Eval 120'
        'displayName' => 'SC-Eval 120'
        'limits' => array
        (
            'maxAppShareDuration' => '120'
            'maxAppShareUser' => '999999'
            'maxDesktopShareDuration' => '120'
            'maxDesktopShareUser' => '999999'
            'maxFileTransferUser' => '999999'
            'maxMeetingDuration' => '120'
            'maxMeetingUser' => '999999'
            'maxRecordUser' => '999999'
            'maxVideoDuration' => '120'
            'maxVideoUser' => '999999'
            'maxWebTourDuration' => '120'
            'maxWebTourUser' => '0'
        )
        'options' => array
        (
            'supportAppShare' => 'true'
            'supportAppShareRemote' => 'true'
            'supportAttendeeRegistration' => 'false'
            'supportRemoteWebTour' => 'false'
            'supportWebTour' => 'false'
            'supportFileShare' => 'false'
            'supportChat' => 'true'
            'supportCobrowseSite' => 'false'
            'supportCorporateOfficesSite' => 'false'
            'supportDesktopShare' => 'true'
            'supportDesktopShareRemote' => 'true'
            'supportFileTransfer' => 'true'
            'supportInternationalCallOut' => 'true'
            'supportJavaClient' => 'true'
            'supportMacClient' => 'false'
            'supportMeetingCenterSite' => 'false'
            'supportMeetingRecord' => 'true'
            'supportMultipleMeeting' => 'true'
            'supportOnCallSite' => 'false'
            'supportOnStageSite' => 'false'
            'supportPartnerOfficesSite' => 'false'
            'supportPoll' => 'false'
            'supportPresentation' => 'true'
            'supportSolarisClient' => 'true'
            'supportSSL' => 'true'
            'supportE2E' => 'false'
            'supportPKI' => 'false'
            'supportTeleconfCallIn' => 'true'
            'supportTeleconfCallOut' => 'true'
            'supportTollFreeCallIn' => 'true'
            'supportVideo' => 'true'
            'supportVoIP' => 'true'
            'supportWebExComSite' => 'false'
            'supportWindowsClient' => 'true'
            'supportQuickStartAttendees' => 'false'
            'supportQuickStartHost' => 'false'
            'hideInScheduler' => 'false'
        )
        'phoneNumbers' => array
        (
            'primaryTollCallInNumber' => '+1-415-655-0001'
            'primaryTollFreeCallInNumber' => '+1-855-749-4750'
        )
        'meetingTypeID' => '552'
        'serviceTypes' => array
        (
            'serviceType' => 'SupportCenter'
        )
    )
)



// PRO Meeting type


array
(
    'productCodePrefix' => 'PRO'
    'active' => 'ACTIVATED'
    'name' => 'Pro 25'
    'displayName' => 'Pro 25'
    'limits' => array
    (
        'maxAppShareDuration' => '1440'
        'maxAppShareUser' => '25'
        'maxDesktopShareDuration' => '1440'
        'maxDesktopShareUser' => '25'
        'maxFileTransferUser' => '25'
        'maxMeetingDuration' => '1440'
        'maxMeetingUser' => '25'
        'maxRecordUser' => '25'
        'maxVideoDuration' => '1440'
        'maxVideoUser' => '25'
        'maxWebTourDuration' => '1440'
        'maxWebTourUser' => '25'
    )
    'options' => array
    (
        'supportAppShare' => 'true'
        'supportAppShareRemote' => 'true'
        'supportAttendeeRegistration' => 'true'
        'supportRemoteWebTour' => 'true'
        'supportWebTour' => 'true'
        'supportFileShare' => 'false'
        'supportChat' => 'true'
        'supportCobrowseSite' => 'false'
        'supportCorporateOfficesSite' => 'true'
        'supportDesktopShare' => 'true'
        'supportDesktopShareRemote' => 'true'
        'supportFileTransfer' => 'true'
        'supportInternationalCallOut' => 'true'
        'supportJavaClient' => 'true'
        'supportMacClient' => 'true'
        'supportMeetingCenterSite' => 'true'
        'supportMeetingRecord' => 'true'
        'supportMultipleMeeting' => 'true'
        'supportOnCallSite' => 'true'
        'supportOnStageSite' => 'false'
        'supportPartnerOfficesSite' => 'false'
        'supportPoll' => 'true'
        'supportPresentation' => 'true'
        'supportSolarisClient' => 'true'
        'supportSSL' => 'true'
        'supportE2E' => 'false'
        'supportPKI' => 'false'
        'supportTeleconfCallIn' => 'true'
        'supportTeleconfCallOut' => 'true'
        'supportTollFreeCallIn' => 'true'
        'supportVideo' => 'true'
        'supportVoIP' => 'true'
        'supportWebExComSite' => 'false'
        'supportWindowsClient' => 'true'
        'supportQuickStartAttendees' => 'false'
        'supportQuickStartHost' => 'true'
        'hideInScheduler' => 'false'
    )
    'phoneNumbers' => array
    (
        'primaryTollCallInNumber' => '+1-415-655-0001'
        'primaryTollFreeCallInNumber' => array()
    )
    'meetingTypeID' => '123'
    'serviceTypes' => array
    (
        'serviceType' => 'MeetingCenter'
    )
)






 */



?>