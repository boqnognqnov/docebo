<?php
/* @var $form CActiveForm */
/* @var $settings WebexAppSettingsForm */
?>

<?php DoceboUI::printFlashMessages(); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal  docebo-form'),
));
?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/docebo-per-webex/' : 'https://www.docebo.com/knowledge-base/elearning-docebo-for-webex/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>

<h2><?=Yii::t('standard','Settings') ?>: Webex</h2>
<br>


<div class="control-container odd">
	<div class="control-group">
		<?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<a class="app-settings-url" href="http://www.webex.com/" target="_blank">http://www.webex.com/</a>
		</div>
	</div>
</div>

<div class="control-container even">
	<div class="control-group">
		<?=CHtml::label(Yii::t('webinar', 'Accounts'), false, array('class'=>'control-label'));?>
		<div class="controls">
			<?php
			$this->widget('common.widgets.WebinarAccountsEditor', array(
				'dataProvider' => WebinarToolAccount::model()->dataProvider('webex'),
				'editAccountUrl' => $this->createUrl('editAccount'),
				'enableMultiAccount' => WebinarTool::getById('webex_meetings')->hasMultiAccountSupport(),
				'modalWidth' => 650
			));
			?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>