<?php
/* @var $form CActiveForm */
/* @var $settings WebexAppSettingsForm */
?>

<h1><?= $modalTitle ?></h1>
<div class="form edit-webinar-account-dialog">

	<?php $form = $this->beginWidget('CActiveForm',
		array(
			'id' => 'webinar-account-settings-form',
			'htmlOptions' => array(
				'class' => 'ajax webinar-account-settings-form'
			)
		)
	); ?>
	<div class="columnsContainer">
		<div class="columnLeft">
			<div class="control-group">
				<?=$form->labelEx($settings, 'account_name', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'account_name', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'account_name'); ?>
				</div>
			</div>


			<div class="control-group">
				<?=$form->labelEx($settings, 'webexid', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'webexid', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'webexid'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'password', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->passwordField($settings, 'password', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'password'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'sitename', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'sitename', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'sitename'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'siteid', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'siteid', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'siteid'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'partnerid', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'partnerid', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'partnerid'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'additional_info', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textArea($settings, 'additional_info', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'additional_info'); ?>
				</div>
			</div>
		</div>

		<div class="columnRight">
			<div class="control-group radio-button-list">
				<?=$form->labelEx($settings, 'site_admin_option', array('class'=>'control-label'));?>
				<?=$form->radioButtonList($settings, 'site_admin_option', $paramsTypeSelection, $optionsTypeSelection)?>
				<?=$form->error($settings, 'site_admin_option'); ?>
			</div>

			<hr>
			<div class="control-group webinar-info-text">
				<?=Yii::t('webinar', 'To allow an unlimited amount of sessions or meetings, enter "0" in the following fields')?>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'max_rooms_per_course', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'max_rooms_per_course', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'max_rooms_per_course'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'max_rooms', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'max_rooms', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'max_rooms'); ?>
				</div>
			</div>

			<div class="control-group">
				<?=$form->labelEx($settings, 'max_concurrent_rooms', array('class'=>'control-label'));?>
				<div class="controls">
					<?=$form->textField($settings, 'max_concurrent_rooms', array('class'=>'input-xlarge'));?>
					<?=$form->error($settings, 'max_concurrent_rooms'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-actions">
		<input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
		<input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
	</div>

	<?php $this->endWidget(); ?>

</div>

<script type="text/javascript">
	$('input[type=radio]').styler();

	$(function() {
		//clean possible delegated events from previous dialogs
		$(document).undelegate('.modal-edit-webinar-account', "dialog2.content-update");

		//set dialog behaviors on server answer
		$(document).delegate(".modal-edit-webinar-account", "dialog2.content-update", function() {
			var e = $(this), autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				$.fn.yiiListView.update("webinar-accounts-management-list");
			} else {
				var err = e.find("a.error");
				if (err.length > 0) {
					var msg = $(err[0]).data('message');
					Docebo.Feedback.show('error', msg);
					e.find('.modal-body').dialog2("close");
				}
			}
		});
	});
</script>