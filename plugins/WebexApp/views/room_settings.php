<?php
/* @var $account stdClass */
/* @var $params array */
?>

<?php
echo CHtml::label(Yii::t('standard', '_PASSWORD'), 'webex_password', array('style' => 'margin-top: 20px;'));
echo CHtml::textField('webex_password', isset($params['webex_password']) ? $params['webex_password'] : '', array('class' => 'tool_params'));
?>