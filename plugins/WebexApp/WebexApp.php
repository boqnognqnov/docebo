<?php

/**
 * Plugin for WebexApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class WebexApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'WebexApp';

	/**
	 * Returns the settings page (admin app)
	 */
	public static function settingsUrl() {
		return Docebo::createAdminUrl('WebexApp/webexApp/settings');
	}

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		$sql = "CREATE TABLE IF NOT EXISTS `conference_webex_meeting` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `id_room` int(11) NOT NULL COMMENT 'Id of the LMS room (local)',
		  `type` VARCHAR(10)  NOT NULL  DEFAULT 'meeting'  COMMENT 'meeting | event',
		  `session_id` bigint(20) unsigned NOT NULL COMMENT 'WebEx session id',
		  `host_calendar_url` varchar(512) DEFAULT NULL,
		  `attendee_calendar_url` varchar(512) DEFAULT NULL,
		  `guest_token` varchar(255) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  KEY `id_room` (`id_room`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}

	public function deactivate() {
		parent::deactivate();
	}
}