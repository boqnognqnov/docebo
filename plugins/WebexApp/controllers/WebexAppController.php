<?php

class WebexAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings', 'editAccount');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * Renders the edit webinar account form for webex
	 */
	public function actionEditAccount($id_account = null) {
		// Load the webinar account
		if(!$id_account) {
			$account = new WebinarToolAccount();
			$account->tool = 'webex';
		} else
			$account = WebinarToolAccount::model()->findByPk($id_account);

		// Load the settings form
		$settings = new WebexAppSettingsForm();
		if (isset($_POST['WebexAppSettingsForm'])) {
			$settings->setAttributes($_POST['WebexAppSettingsForm']);
			if ($settings->validate()) {

				// Save the settings in the webinar account
				$accountSettings = array();
				$accountSettings['webexid'] = $settings->webexid;
				$accountSettings['password'] = $settings->password;
				$accountSettings['sitename'] = $settings->sitename;
				$accountSettings['siteid'] = $settings->siteid;
				$accountSettings['partnerid'] = $settings->partnerid;
				$accountSettings['site_admin_option'] = $settings->site_admin_option;
				$accountSettings['max_rooms'] = $settings->max_rooms;
				$accountSettings['max_rooms_per_course'] = $settings->max_rooms_per_course;
				$accountSettings['max_concurrent_rooms'] = $settings->max_concurrent_rooms;
				$account->name = $settings->account_name;
				$account->additional_info = $settings->additional_info;
				$account->settings = $accountSettings;
				$account->save();

				// Close the modal
				echo '<a class="auto-close"></a>';
				Yii::app()->end();
			}
		}

		// Load account settings in form
		$accountSettings = $account->settings;
		$settings->account_name = $account->name;
		$settings->webexid = $accountSettings->webexid;
		$settings->password = $accountSettings->password;
		$settings->sitename = $accountSettings->sitename;
		$settings->siteid = $accountSettings->siteid;
		$settings->partnerid = $accountSettings->partnerid;
		$settings->site_admin_option = !empty($accountSettings->site_admin_option)?$accountSettings->site_admin_option:0;
		$settings->max_rooms = $accountSettings->max_rooms;
		if (empty($settings->max_rooms) && !$id_account)
			$settings->max_rooms = intval(Yii::app()->params['videoconf_max_rooms_default']);
		$settings->max_rooms_per_course = $accountSettings->max_rooms_per_course;
		if (empty($settings->max_rooms_per_course) && !$id_account)
			$settings->max_rooms_per_course = intval(Yii::app()->params['videoconf_max_rooms_per_course_default']);
        $settings->max_concurrent_rooms = $accountSettings->max_concurrent_rooms;
        if (empty($settings->max_concurrent_rooms) && !$id_account)
            $settings->max_concurrent_rooms = intval(Yii::app()->params['videoconf_max_concurrent_rooms_default']);
		$settings->additional_info = $account->additional_info;

		// Options to set up radio button list
		$paramsTypeSelection = array(
			WebexApiClient::WEBEX_SITE_ADMIN_LISTED   => Yii::t('webinar', 'Listed'),
			WebexApiClient::WEBEX_SITE_ADMIN_UNLISTED => Yii::t('webinar', 'Unlisted')
		);
		$optionsTypeSelection = array(
			'separator' => ''
		);

		// Render the edit account view in the modal
		$this->renderPartial('_account_settings', array(
			'modalTitle' => $account->id_account ? Yii::t('webinar', 'Edit webinar account') : Yii::t('webinar', 'New webinar account'),
			'settings' => $settings,
			'paramsTypeSelection' => $paramsTypeSelection,
			'optionsTypeSelection' => $optionsTypeSelection
		));
	}

	/**
	 * Renders the main settings page
	 */
	public function actionSettings() {
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->theme->baseUrl . '/css/webinar-setup.css');

		$settings = new WebexAppSettingsForm();
		$this->render('settings', array('settings' => $settings));
	}

	/**
	 * Called by Webex in certain events (error joining, after attendee joins, etc.).
	 * Set in Conference manager as a Callback Url
	 *
	 */
	public function actionCallback() {
		// Standard Webex URL parameters
		$action     = Yii::app()->request->getParam('AT', '');
		$status     = Yii::app()->request->getParam('ST', '');
		$reason     = Yii::app()->request->getParam('RS', '-');

		// Added by our callback
		$sessionId   = (int) Yii::app()->request->getParam('session_id', '');
		$sessionType = Yii::app()->request->getParam('session_type', 'webinar');
		$sessionModel = null;
		$courseUrl = Docebo::createLmsUrl("site/index");

		if($sessionType == 'webinar') {
			$day   = Yii::app()->request->getParam('day', '');
			$sessionModel = WebinarSessionDate::model()->with('webinarSession')->findByAttributes(array('id_session'=>$sessionId, 'day' => $day));
			$courseUrl = Yii::app()->createAbsoluteUrl("player/training/webinarSession", array("course_id" => $sessionModel->webinarSession->course_id, 'session_id' => $sessionModel->id_session));
			$tool_params = $sessionModel->webinar_tool_params;
		} else if($sessionType == 'coaching') {
			$sessionModel = LearningCourseCoachingWebinarSession::model()->findByPk($sessionId); /* @var $sessionModel LearningCourseCoachingWebinarSession*/
			$courseUrl = Yii::app()->createAbsoluteUrl("player/training", array("course_id" => $sessionModel->idCoachingSession->idCourse));
			$tool_params = $sessionModel->tool_params;
		} else{
			//widget!!
			$day   =  Yii::app()->request->getParam('day', '');
			$sessionModel = WebinarSessionDate::model()->with('webinarSession')->findByAttributes(array('id_session'=>$sessionId, 'day' => $day));
//			$sessionModel = WebinarSessionDate::model()->with('webinarSession')->findByAttributes(array('id_session'=>$sessionId));
			$courseUrl = Yii::app()->createAbsoluteUrl("player/training", array("course_id" => $sessionModel->webinarSession->course_id));
			$tool_params = $sessionModel->webinar_tool_params;
		}

		if($sessionModel) {
			switch ($action) {
				case 'EN': // coming from enroll event
					if(($status == 'SUCCESS') || ($reason == 'AlreadyEnrolled')) {
						$accountSettings = WebinarTool::getById('webex_events')->getAccountById($sessionModel->id_tool_account)->settings;
						$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
						$api_params = CJSON::decode($tool_params);
						$user = Yii::app()->user->loadUserModel();
						$firstname = trim($user->firstname) ? trim($user->firstname) : ("User " . $user->idst);
						$lastname = trim($user->lastname) ? trim($user->lastname) : "Lastname";
						$join_url = $apiClient->getAttendeeJoinEventUrl($api_params['session_id'], $firstname, $lastname, $user->email);
						$callBack = urlencode(Yii::app()->createAbsoluteUrl("WebexApp/WebexApp/callback", array('session_id' => $sessionModel->id_session, 'session_type' => $sessionType, 'day' => $sessionModel->day)));
						$join_url .= "&BU=$callBack";
						$this->redirect($join_url, true);
						Yii::app()->end();
					}
				case 'JE':  // coming from join event
				case 'JM':  // coming from join meeting
				case 'JS':  // coming from join meeting
					if ($status != 'SUCCESS') {
						Yii::app()->user->setFlash('error', 'Webex Server Error: ' . Yii::t('conference', $reason));
					}
					break;
			}

			$this->redirect($courseUrl, true);
			Yii::app()->end();
		} else {
			Yii::log("Missing or invalid session ID (id=".$sessionId.")", CLogger::LEVEL_ERROR);
		}

		$this->redirect(Docebo::createLmsUrl("site/index"), true);
		Yii::app()->end();
	}

}
