<?php

/**
 * WebexAppSettingsForm class.
 *
 * @property string $webexid
 * @property string $password
 * @property string $sitename
 * @property string $siteid
 * @property string $partnerid
 * @property string $account_name
 * @property string $additional_info
 * @property string $site_admin_option
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $max_concurrent_rooms
 */
class WebexAppSettingsForm extends CFormModel {
	public $webexid;
	public $password;
	public $sitename;
	public $siteid;
	public $partnerid;
	public $account_name;
	public $additional_info;
	public $site_admin_option;
	public $max_rooms;
	public $max_rooms_per_course;
	public $max_concurrent_rooms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name, webexid, password, sitename, siteid, partnerid, additional_info, site_admin_option, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'safe'),
			array('account_name, webexid, password, sitename, siteid, partnerid, site_admin_option, max_rooms, max_rooms_per_course, max_concurrent_rooms', 'required'),
            array('max_concurrent_rooms, max_rooms, max_rooms_per_course', 'numerical'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
            'webexid' => 'Webex id',
            'password' => 'Webex password',
            'sitename' => 'Site name',
            'siteid' => 'Site id',
            'partnerid' => 'Partner id',
			'account_name' => Yii::t('webinar', 'Account Name'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'site_admin_option' => Yii::t('webinar', 'Site admin option'),
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
		);
	}
}
