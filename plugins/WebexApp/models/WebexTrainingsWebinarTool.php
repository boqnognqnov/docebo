<?php

/**
 * Class WebexTrainingsWebinarTool
 * Implemenents webinar tool fo webex meeting
 */
class WebexTrainingsWebinarTool extends WebinarTool {
	/**
	 * @var bool does the current webinar tool supports grabbing a recording via API call or automatically identify a viewing URL?
	 */
	protected $apiFindRecording = true;

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'webex_trainings';
		$this->name = 'Webex Trainings';

		$this->playRecordingType = self::PLAY_RECORDING_TYPE_EXTERNAL;
	}

	/**
	 * Returns the array of accounts configured for the current tool indexed by id
	 * @return WebinarToolAccount[]
	 */
	public function getAccounts() {
		if(!$this->accounts) {
			$records = WebinarToolAccount::model()->findAllByAttributes(array('tool' => 'webex'));
			$this->accounts = CHtml::listData($records, 'id_account', function($row){
				return $row;
			});
		}

		return $this->accounts;
	}

	/**
	 * Returns true if the current webinar tools has support for multi account
	 * @return bool
	 */
	public function hasMultiAccountSupport() {
		return true;
	}

	/**
	 * Renders the HTML for the specific webex settings (e.g. meeting password)
	 *
	 * @param $api_params
	 *
	 * @return The html to embed in the webinar form
	 */
	public function getCustomSettingsHTML($api_params) {
		return Yii::app()->controller->renderPartial('WebexApp.views.room_settings', array(
			'account' => $this,
			'params' => $api_params
		), true);
	}

	/**
	 * Returns an array of "localized" room info to be displayed somewhere (e.g. in the New webinar session notification)
	 * @param integer $idAccount
	 * @param array $api_params
	 *
	 * @return array
	 */
	public function getAttendeeInfoForRoom($idAccount, $api_params = array()) {
		$roomInfo = array();

		if($api_params['webex_password']) {
			$roomInfo[] = array(
				'label' => Yii::t('standard', '_PASSWORD'),
				'value' => $api_params['webex_password']
			);
		}

		return $roomInfo;
	}

	/**
	 * Creates a new room using the Webex api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 * @throws CHttpException
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		if(!is_array($api_params)){
			$api_params = CJSON::decode($api_params);
		}
		$durationSeconds = (int) (strtotime($endDateTime) - strtotime($startDateTime));
		if ( $durationSeconds > 0)
			$durationMinutes = (int) floor($durationSeconds/60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
		$training = $apiClient->createTrainingSession($roomName, $startDateTime, $durationMinutes, $accountSettings->site_admin_option, $api_params['webex_password']);
		if (!is_array($training) || !isset($training["sessionkey"]))
			throw new CHttpException(500, ConferenceManager::$MSG_INVALID_DATA_RETURNED);

		// Follow DB Table names
		$result = array(
			"type" => 'training',
			"session_id" => $training["sessionkey"],
			"guest_token" => $training["additionalInfo"]["guestToken"],
			"webex_password" => $api_params['webex_password']
		);

		return $result;

	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$durationSeconds = (int)(strtotime($endDateTime) - strtotime($startDateTime));
		if ($durationSeconds > 0)
			$durationMinutes = (int)floor($durationSeconds / 60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
		$apiClient->updateTrainingSession($api_params['session_id'], $roomName, $startDateTime, $durationMinutes, $api_params['webex_password']);

		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$trainingId = $api_params['session_id'];
		$account = $this->getAccountById($idAccount, true);
		if($account !== false)
		{
			$accountSettings = $account->settings;
			$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
			$apiClient->deleteTrainingSession($trainingId);
		}
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
		$url = $apiClient->getAttendeeJoinMeetingUrl($api_params['session_id'], $this->getUserDisplayName($userModel), $userModel->email);

		if( $sessionDateModel instanceof LearningCourseCoachingWebinarSession){
			$session_type = 'coahing';
		} else if( $sessionDateModel instanceof WebinarSessionDate){
			if($widgetContext){
				$session_type = 'widget';
			} else{
				$session_type = 'webinar';
			}
		}

		// Add Callback URL to handle various situations
		$callBack = urlencode(Yii::app()->createAbsoluteUrl("WebexApp/WebexApp/callback", array(
			'session_id' => $sessionDateModel->id_session,
			'session_type' => $session_type
		)));
		$url .= "&BU=$callBack";
		return $url;
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSessionDate $sessionDateModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel) {
		//first check if the room has already been started by someone else
		$toolParams = CJSON::decode($sessionDateModel->webinar_tool_params);
		$alreadyStartedBy = (is_array($toolParams) && isset($toolParams['webex_already_started_by']) && !empty($toolParams['webex_already_started_by']) ? $toolParams['webex_already_started_by'] : false);
		if (!empty($alreadyStartedBy) && $alreadyStartedBy != $userModel->idst) {
			//since the conference is already hosted by another user, then just output a join url, in order to avoid conflicts with other hosts
			return $this->getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel);
		}
		//updating "started by" information in session date parameters.
		//NOTE: using direct query in order to avoid dates validation when saving session record.
		$toolParams['webex_already_started_by'] = $userModel->idst;
		$query = "UPDATE ".WebinarSessionDate::model()->tableName()." SET webinar_tool_params = :webinar_tool_params WHERE id_session = :id_session AND day = :day";
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand($query);
		$cmd->execute(array(':webinar_tool_params' => CJSON::encode($toolParams), ':id_session' => $sessionDateModel->id_session, ':day' => $sessionDateModel->day));
		$sessionDateModel->refresh(); //in case the same object will be used elsewhere
		//after updating information, now effectively retrieve hosting url
		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
		return $apiClient->getHostStartMeetingUrl($api_params['session_id']);
	}

	/**
	 * @param int $idAccount $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @return array
	 */
	public function retrieveRecordingLink($idAccount, $api_params){
		$return = array(
			'error' => '',
			'recordingUrl' => ''
		);

		$accountSettings = $this->getAccountById($idAccount)->settings;
		$apiClient = new WebexApiClient($accountSettings->sitename, $accountSettings->siteid, $accountSettings->partnerid, $accountSettings->webexid, $accountSettings->password);
		$result = $apiClient->retrieveRecordingLink($api_params['session_id']);

		if($result['response_status'] !== 200){
			$return['error'] = $result['response_message'];
		} elseif(!empty($result['error'])){
			$return['error'] = $result['error'];
		} else {
			if(isset($result['data']['body']['bodyContent']['recording']['streamURL'])){ // single recording exists
				$return['recordingUrl'] = $result['data']['body']['bodyContent']['recording']['streamURL'];
			} elseif(isset($result['data']['body']['bodyContent']['recording'][0]['streamURL'])){ // multiple recordings exist --> pick first
				$return['recordingUrl'] = $result['data']['body']['bodyContent']['recording'][0]['streamURL'];
			} else {
				$return['error'] = Yii::t('webinar', 'No recording URL could be retrieved for this session.');
			}
		}
		return $return;
	}

}