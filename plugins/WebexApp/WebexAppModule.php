<?php

class WebexAppModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'WebexApp.models.*',
			'WebexApp.components.*',
		));

		Yii::app()->event->on('WebinarCollectTools', array($this, 'installWebinarTool'));
	}


	/**
	 * Installs the webinar tool for adobe connect
	 * @param $event DEvent
	 */
	public function installWebinarTool($event) {
		$event->return_value['tools']['webex_meetings'] = 'WebexMeetingsWebinarTool';
		$event->return_value['tools']['webex_events'] = 'WebexEventsWebinarTool';
		$event->return_value['tools']['webex_trainings'] = 'WebexTrainingsWebinarTool';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('webex', array(
			'app_initials' => 'WX',
			'settings' => Docebo::createAdminUrl('WebexApp/WebexApp/settings'),
			'label' => 'Webex',
		),
			array()
		);
	}
}
