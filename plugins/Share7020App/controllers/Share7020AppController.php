<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Share7020AppController
 *
 * @author App7020 team
 */
class Share7020AppController extends Controller {

	private $show = 'show';

	public function init() {

		JsTrans::addCategories('coach7020Settings');

		$themeUrl = Yii::app()->theme->baseUrl;
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/user_management.js', CClientScript::POS_HEAD);
		//Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/scriptModal.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/confirm/jquery.confirm.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/require_js/require.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($themeUrl . '/css/user_management.css');
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-docebo/ui.fancytree.css');
		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
		UsersSelector::registerClientScripts();
		parent::init();
	}

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:
		// Allow following actions to admin only:
		$admin_only_actions = array();
		$res[] = array(
			'allow',
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}

	public function actionSettings() {
		App7020Helpers::registerApp7020Options();
		Yii::app()->getClientScript()->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020Activity/main.js', CClientScript::POS_END);
		if (Yii::app()->request->isPostRequest) {
			$model = ShareApp7020Settings::model();
			$model->attributes = Yii::app()->request->getParam('ShareApp7020Settings');
			if ($model->validate()) {
				if ($model->save()) {
					$script = 'Docebo.Feedback.show(\'success\', Yii.t("app7020", "Settings were saved successfully"));';
				}
			} else {
				$script = "";
				$flat = App7020Helpers::convertErrorsToFlatArray($model->getErrors());
				$script .= "Docebo.Feedback.show('error', " . json_encode($flat) . ", true, false, true);\n";
			}
			Yii::app()->getClientScript()->registerScript('notifyShareSettings', $script, CClientScript::POS_END);
		} else {
			$model = ShareApp7020Settings::model(true);
		}
		$settingsModel = $model;
		$radio_list_args = array(
			'data-act' => 'additional',
			'labelOptions' => array(
				'class' => 'control-label',
			),
			'template' => '<div class="control-group">{input}{label}</div>',
		);


		$shareSettings = array(
			'model' => $settingsModel,
			'showAdditionalContainer' => array(
				'video_contribute' => ($settingsModel->video_contribute == ShareApp7020Settings::VIDEO_DURATION ? $this->show : ''),
				'other_contribution' => ($settingsModel->other_contribution == ShareApp7020Settings::ON ? $this->show : '')
			),
			'radio_list_args' => $radio_list_args,
			'checkbox_video_not_shorter' => Settings::get(ShareApp7020Settings::VIDEO_NOT_SHORTER_TIME),
			'checkbox_video_not_longer' => Settings::get(ShareApp7020Settings::VIDEO_NOT_LONGER_TIME),
		);


		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Coach & Share Settings'),
		);


		$this->render('shareSettings', $shareSettings);
	}


	/**
	 * Main action for coach settings
	 */
	/*
	public function actionCoachSettings() {
		JsTrans::addCategories('coach7020Settings');

		App7020Helpers::registerApp7020Options();
		Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
		Yii::app()->getClientScript()->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020Activity/main.js', CClientScript::POS_END);
		if (Yii::app()->request->isPostRequest) {
			$model = CoachApp7020Settings::model();
			$model->attributes = Yii::app()->request->getParam('CoachApp7020Settings');
			if ($model->validate()) {
				if ($model->save()) {
					$script = 'Docebo.Feedback.show(\'success\', Yii.t("app7020", "Settings were saved successfully"));';
				}
			} else {
				$script = "";
				foreach ($model->getErrors() as $k => $errors) {
					foreach ($errors as $error) {
						$script .= "Docebo.Feedback.show('error', '" . $error . "');\n";
					}
				}
			}
			Yii::app()->getClientScript()->registerScript('notifyCoachSettings', $script, CClientScript::POS_END);
		} else {
			$model = CoachApp7020Settings::model(true);
		}
		$this->breadcrumbs = array(
			Yii::t('app7020', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
			Yii::t('app7020', 'Coach Settings'),
		);
		$this->render('settings', array('settings' => $model));
	}
	*/

	/**
	 * Renders edit lo tab item
	 * @param $courseModel LearningCourse
	 * @param $loModel LearningOrganization
	 */
	public function renderEditLoTab($courseModel, $loModel) {
		/*if (Settings::get('app7020_ask_button_lo') === on) {
			if (Yii::app()->user->getIsGodadmin() || LearningCourseuser::isUserLevel(Yii::app()->user->getId(), $courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
				$this->renderPartial('plugin.Share7020App.views.share7020App._edit_lo_tab', array(
					'courseModel' => $courseModel,
					'loModel' => $loModel
				));
			}
		}
		*/
	}

	/**
	 * Renders edit lo tab content
	 * @param $courseModel LearningCourse
	 * @param $loModel LearningOrganization
	 */
	public function renderEditLoTabContent($courseModel, $loModel) {
		/*if ((Yii::app()->user->getIsGodadmin() || LearningCourseuser::isUserLevel(Yii::app()->user->getId(), $courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR))
			&& ($loModel instanceof LearningOrganization)) {
			$themeUrl = Yii::app()->theme->baseUrl;
			if (Yii::app()->request->isAjaxRequest) {
				App7020Helpers::registerApp7020Options();
				Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
				Yii::app()->getClientScript()->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
			}
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.cookie.js');
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/tags/tagmanager.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/handlebars.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/' . (YII_DEBUG ? 'typeahead.jquery.min.js' : 'typeahead.jquery.js'), CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/typehead/typeahead.bundle.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/tags/app7020-tags.css');
			Yii::app()->getClientScript()->registerScriptFile($themeUrl . '/js/jquery.cookie.js');
			$ft = new FancyTree();
			Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_HEAD);
			Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');


			$fcbkAutocompleteUrl = Docebo::createApp7020Url('axAssets/axGetAutoCompleteTags');
			$js = 'var $fcbkAutocompleteUrl = ' . json_encode($fcbkAutocompleteUrl) . PHP_EOL;
			Yii::app()->getClientScript()->registerScript("App7020UploadSettings" . null, $js, CClientScript::POS_HEAD);
			$topics = array();
			$tags = array();
			if ($loModel) {
				//$topics = App7020TopicsLo::model()->findAllByAttributes(array('idLo' => $loModel->idOrg));
				$tags = App7020TagsLo::model()->findAllByAttributes(array('idLo' => $loModel->idOrg));
			}
			$this->renderPartial('plugin.Share7020App.views.share7020App._edit_lo_tab_content', array(
				'courseModel' => $courseModel,
				'loModel' => $loModel,
				'tags' => $tags
				//'topics' => $topics
			), false, true
			);
		}*/
	}

	/**
	 * Save lo tab content
	 * @param $loModel LearningOrganization
	 */
	public function saveLoTabContent($loModel) {
		/*if (Yii::app()->user->getIsGodadmin() || LearningCourseuser::isUserLevel(Yii::app()->user->getId(), $loModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR)) {
			if (!empty($loModel->idOrg)) {

				$questions = App7020Question::model()->findAllByAttributes(array('idLearningObject' => $loModel->idOrg));

				if (!empty($_POST['enableApp7020'])) {
					App7020TagsLo::model()->deleteAllByAttributes(array('idLo' => $loModel->idOrg));

					if (!empty($_POST['hidden-tags'])) {
						$tags = explode(",", $_POST['hidden-tags']);
						foreach ($tags as $value) {
							$tagModel = App7020Tag::model()->findByAttributes(array('tagText' => $value));
							if ($tagModel->id) {
								$app7020TagsLO = new App7020TagsLo();
								$app7020TagsLO->idLo = $loModel->idOrg;
								$app7020TagsLO->idTag = $tagModel->id;
								$app7020TagsLO->save();
							}
						}
					}
					//remove this in case the topics tree should be uneditable
					App7020TopicsLo::model()->deleteAllByAttributes(array('idLo' => $loModel->idOrg));

					foreach ($questions as $value) {
						App7020QuestionTopic::model()->deleteAllByAttributes(array('idQuestion' => $value->id));
					}
					//use this in case the topics tree should be uneditable
					//if (empty(App7020TopicsLo::model()->findAllByAttributes(array('idLo' => $loModel->idOrg)))) {
					if (!empty($_POST['fancytree-selected-nodes'])) {
						$topics = array_keys($_POST['fancytree-selected-nodes']);
						foreach ($topics as $value) {
							$app7020TopicsLO = new App7020TopicsLo();
							$app7020TopicsLO->idLo = $loModel->idOrg;
							$app7020TopicsLO->idTopic = $value;
							$app7020TopicsLO->save();
							foreach ($questions as $question) {
								$app7020QuestionTopic = new App7020QuestionTopic();
								$app7020QuestionTopic->idTopic = $value;
								$app7020QuestionTopic->idQuestion = $question->id;
								$app7020QuestionTopic->save();
							}
						}
					}
					//}
				} else {
					App7020TagsLo::model()->deleteAllByAttributes(array('idLo' => $loModel->idOrg));
					App7020TopicsLo::model()->deleteAllByAttributes(array('idLo' => $loModel->idOrg));
					foreach ($questions as $value) {
						App7020QuestionTopic::model()->deleteAllByAttributes(array('idQuestion' => $value->id));
					}
				}
			}
		}*/
	}

}
