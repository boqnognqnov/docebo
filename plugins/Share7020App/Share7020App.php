<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 12.10.2015 �.
 * Time: 18:09
 */

class Share7020App extends DoceboPlugin{
    // Name of the plugin; needed to identify plugin folder and entry script
    public $plugin_name = 'Share7020App';
    public static $HAS_SETTINGS = false;

    /**
     * This will allow us to access "statically" (singleton) to plugin methods
     * @param string $class_name
     * @return Plugin Object
     */
    public static function plugin($class_name = __CLASS__) {
        return parent::plugin($class_name);
    }

    public function activate() {
        parent::activate();
        
        // Also, INDEX all documents of Knowledge Assets type to Elasticsearch
        $agent = new EsAgentKnowledgeAsset();
        $agent->index();

        // Also, INDEX all documents of Q&A type to Elasticsearch
        $agent = new EsAgentQandA();
        $agent->index();
    }

    public function deactivate() {
        parent::deactivate();
        
        // Also, delete all documents of QKnowledge Assets type from Elasticsearch
        Yii::app()->es->deleteTypes(array(EsAgentKnowledgeAsset::TYPE));

        // Also, delete all documents of Q&A type from Elasticsearch
        Yii::app()->es->deleteTypes(array(EsAgentQandA::TYPE));
    }
}