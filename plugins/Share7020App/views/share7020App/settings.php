<section id="app7020-coach-settings">
	<?php $this->renderPartial('_settings_form', array('settings' => $settings)); ?>
</section>
<?php if($this->uniqueid != 'channelsManagement' && Yii::app()->user->isGodadmin && Docebo::isTrialPeriod() && !CoreSettingUser::getSettingByUser(Yii::app()->user->idst,'cs_splash_screen')) {?>
	<script>
		openDialog(null, '', '<?= Docebo::createApp7020Url('assets/showSplashScreen') ?>', 'app7020-splash-screen', 'modal-splash-screen');
	</script>
<?php  }?>
