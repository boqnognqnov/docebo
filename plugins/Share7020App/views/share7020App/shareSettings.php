<section id="app7020-general-settings-form-container">
	<h1><?php echo Yii::t('app7020', 'Coach & Share setting') ?></h1>
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'app7020-general-settings-form',
		'method' => 'POST',
		'action' => '',
		'htmlOptions' => array(
			'class' => "ajax form-horizontal",
		),
	));
	?>

	<div class="border-wraper">
		<div class="row-fluid  app7020-container grey">
			<div class="span4">
				<h5><?php echo Yii::t('app7020', 'Private assets'); ?></h5>
			</div>
			<div class="span8">
				<div class="control-group">
					<div class="controls">
						<?php
						echo $form->checkBox($model, 'allow_private_assets', array('uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON));
						echo $form->labelEx($model, 'allow_private_assets', array('class' => 'control-label'));
						?>
					</div>
				</div>
				<div class="details padd"><?php echo Yii::t('app7020', "Assets will be directly published into user's personal channel and visible to user and people invited by the user ONLY") ?></div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<h5><?php echo Yii::t('app7020', 'Video asstes'); ?></h5>
			<div class="details">
				<?php echo Yii::t("app7020", "Define duration limits for the videos that contributors want to upload") ?>
			</div>
		</div>
		<div class="span8 logical-group">
			<?php
			echo $form->radioButtonList($model, 'video_contribute', array(
				ShareApp7020Settings::ALLOW_ANY_LENGHT => '<span></span>' . Yii::t('app7020', 'Allow upload of videos of any length'),
				ShareApp7020Settings::VIDEO_DURATION => '<span></span>' . Yii::t('app7020', 'Set Duration limits (not shorten/longer than)')
					), $radio_list_args);
			?>
			<span class="additional_container <?= $showAdditionalContainer['video_contribute'] ?>">
				<div class="control-group">
					<div class="controls related-items">
						<?php
						echo CHtml::checkBox('video_not_shorter', $checkbox_video_not_shorter, array('data-act' => 'related'));
						echo CHtml::label('<span></span>' . Yii::t('app7020', 'Video Not Shorter'), 'video_not_shorter', array('class' => 'control-label'));
						echo $form->textField($model, 'video_not_shorter_time', array('placeholder' => '00:00:00', 'id' => 'video_not_shorter_time'));
						echo Yii::t('app7020', 'hh:mm:ss');
						?>
					</div>
				</div>
				<div class="control-group">
					<div class="controls related-items">
						<?php
						echo CHtml::checkBox('video_not_longer', $checkbox_video_not_longer, array('data-act' => 'related'));
						echo CHtml::label('<span></span>' . Yii::t('app7020', 'Video Not Longer'), 'video_not_longer', array('class' => 'control-label'));
						echo $form->textField($model, 'video_not_longer_time', array('placeholder' => '00:00:00', 'id' => 'video_not_longer_time'));
						echo Yii::t('app7020', 'hh:mm:ss');
						?>
					</div>
				</div>
			</span>
		</div>
	</div>
<!--	<div class="border-wraper">
		<div class="row-fluid app7020-container grey">
			<div class="span4">
				<h5><?php //echo Yii::t('app7020', 'Other assets'); ?></h5>
				<div class="details">
					<?php //echo Yii::t("app7020", "Define formats of knowledge assets that can be uploaded") ?>
				</div>
			</div>
			<div class="span8 logical-group">
				<div class="control-group">
					<div class="controls">
						<?php
//echo $form->checkBox($model, 'documents_contribution', array('uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON));
//echo $form->labelEx($model, 'documents_contribution', array('class' => 'control-label'));
						?>
					</div>
				</div>
				<div class="details padd">(doc xls pdf ppt pps odt ods odp docx xlsx pptx)</div>
				<div class="control-group">
					<div class="controls">
						<?php
//echo $form->checkBox($model, 'other_contribution', array('data-act' => 'additional', 'uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON));
//echo $form->labelEx($model, 'other_contribution', array('class' => 'control-label'));
						?>
					</div>
				</div>
				<span class="additional_container <?php //$showAdditionalContainer['other_contribution'] ?>">
					<div class="control-group">
						<div class="controls">
							<?php
//echo $form->textField($model, 'file_extensions', array('id' => 'file_extensions'));
							?>
						</div>
					</div>
					<div class="details padd"><?php echo Yii::t('app7020', 'Please specify files extensions, comma separated (e.g. jpg, txt, rtf)') ?></div>
				</span>
			</div>
		</div>
	</div>-->
	<div class="border-wraper">
		<div class="row-fluid app7020-container grey">
			<div class="span4">
				<h5><?php echo Yii::t('app7020', 'Assets Download'); ?></h5>
			</div>
			<div class="span8 logical-group">
				<div class="control-group">
					<div class="controls">
						<?php
						echo $form->checkBox($model, 'allow_assets_dowload', array('uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON));
						echo $form->labelEx($model, 'allow_assets_dowload', array('class' => 'control-label'));
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<h5><h5><?php echo Yii::t('app7020', 'Answer Settings'); ?></h5></h5>
		</div>
		<div class="span8 logical-group">
			<div class="control-group">
				<div class="controls">
					<?php
                    echo $form->checkBox($model, 'enable_answer_questions', array('uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON, 'checked' => ShareApp7020Settings::isEnableAnswerQuestionsGlobal()));
					echo $form->labelEx($model, 'enable_answer_questions', array('class' => 'control-label'));
					?>
				</div>
			</div>
			<div class="details padd"><?php echo Yii::t("app7020", "Allow contributor to participate by answering to learners' question along with the assigned experts") ?></div>
		</div>
	</div>
<!--	<div class="border-wraper">
		<div class="row-fluid app7020-container grey">
			<div class="span4">
				<h5><?php // echo Yii::t('app7020', 'Sharing'); ?></h5>
			</div>
			<div class="span8">
				<div class="control-group">
					<div class="controls">
						<?php
//echo $form->checkBox($model, 'invite_watch', array('uncheckValue' => ShareApp7020Settings::OFF, 'value' => ShareApp7020Settings::ON));
//echo $form->labelEx($model, 'invite_watch', array('class' => 'control-label'));
						?>
					</div>
				</div>
				<div class="details padd"><?php //echo Yii::t('app7020', 'Allow users to invite other people to watch an asset') ?></div>
			</div>
		</div>
	</div>-->
    <div class="row-fluid">
        <div class="span4">
        </div>
        <div class="span8">
            <div class="control-group">
                <div class="controls">
					<?php
					echo CHtml::link(Yii::t("app7020", "Cancel"), array('Share7020App/settings', 'activeTab' => 0), array('class' => 'btn cancel app7020-button'));
					echo CHtml::SubmitButton(Yii::t("app7020", "Save Changes"), array('class' => 'btn save app7020-button'));
					?>
                </div>
            </div>
        </div>
    </div>
	<?php $this->endWidget(); ?>
</section>

<?php if($this->uniqueid != 'channelsManagement' && Yii::app()->user->isGodadmin && Docebo::isTrialPeriod() && !CoreSettingUser::getSettingByUser(Yii::app()->user->idst,'cs_splash_screen')) {?>
	<script>
		openDialog(null, '', '<?= Docebo::createApp7020Url('assets/showSplashScreen') ?>', 'app7020-splash-screen', 'modal-splash-screen');
	</script>

<?php  }?>
