<h1><?= Yii::t('app7020', 'Settings') ?></h1>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'Coach7020AppSettings',
    'action' => Docebo::createAppUrl("admin:Share7020App/Share7020App/coachSettings"),
    'method' => 'POST',
    'htmlOptions' => array('class' => "ajax form-horizontal")
)); ?>



<div class="row-fluid app7020-container">

    <div class="span4">
        <h5><?php echo Yii::t('app7020', 'General Settings'); ?></h5>
    </div>

    <div class="span8">
        <div class="control-group">
            <div class="controls">
                <?php
                echo $form->checkBox($settings, 'enable_answer_questions', array('uncheckValue' => $settings::OFF, 'value' => $settings::ON));
                echo $form->labelEx($settings, 'enable_answer_questions', array('class' => 'control-label'));
                ?>
            </div>
        </div>
        <div class="details padd"><?php echo Yii::t("app7020", "Allow contributor to participate by answering to learners' question along with the assigned experts") ?></div>



<!--        <div class="control-group">
            <div class="controls">
                <?php
                echo $form->checkBox($settings, 'app7020_ask_button_lo', array('uncheckValue' => $settings::OFF, 'value' => $settings::ON));
                echo $form->labelEx($settings, 'app7020_ask_button_lo', array('class' => 'control-label'));
                ?>
            </div>

        </div>
        <div class="details padd"><?php echo Yii::t('app7020', 'Allow users to engage and ask the experts while playing courses training material') ?></div>-->

    </div>

</div>

<div class="row-fluid">
    <div class="span4">

    </div>
    <div class="span8">
        <div class="control-group coach-align-button">
            <div class="controls">
                <?php
                echo CHtml::submitButton('Save Changes', array('class' => ' save app7020-button', 'id' => 'saveCoachSettings'));
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>