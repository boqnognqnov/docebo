<?php
	$isHaveModel = (!empty($loModel) && $loModel::isAskGuruEnabled($loModel->idOrg)) ? true : false;
	//$preSelectedTopics = array();
	//foreach ($topics as $value){
	//	$preSelectedTopics[] = $value->idTopic;
	//}
	//use this in case the topics tree should be uneditable
	//$lockTopics = (!empty($preSelectedTopics)) ? true : false;
	$lockTopics = false;
	
	$tagsIncluded = array();
	foreach ($tags as $value){
		$tagObject = App7020Tag::model()->findByPk($value->idTag);
		if($tagObject->tagText){
			$tagsIncluded[] = $tagObject->tagText;
		}
	}
?>
<div class="tab-pane" id="lo-7020-tags-topics" style="min-height: 200px;">
	<div class="row-fluid">
		<div class="span12">
			<div class="control-group">
				<div class="controls">
					<label class="checkbox">
						<input type="checkbox" id="enableApp7020" name="enableApp7020" <?php echo ($isHaveModel) ? 'checked' : ''; ?>> 
						<label for="enableApp7020"><?php echo Yii::t('app7020', 'Enable "Ask the Expert" feature for this learning object'); ?></label>
					</label>
				</div>
			</div>
			
			<div class="row-fluid" id="lo-7020-tags-topics-container" style="display: <?php echo ($isHaveModel) ? 'block' : 'none'; ?>;">
				<div class="span6" id="lo-7020-tags-container">
					<p class="title"><?php echo Yii::t('app7020', 'Tags'); ?></p>
					<p class="subTitle"><?php echo Yii::t('app7020', 'Assign different tags to your LO'); ?></p>
					<div class="tagsSection">
					<?php 
						echo CHtml::textField('tags', '', 
						array(
							'class' => 'tagsField tags-input tag-input',
							'placeholder' => Yii::t('app7020', 'Add tags here (spearated by a comma)'), 
							'data-included' => implode(",", $tagsIncluded)
						)); 
					?>
					</div>
					<div class="app7020-tags-container" id="app7020-tags-container"></div>
					<?php /*
					<div class="app7020-hint app7020-lo-topics-hint">
						<h4><?php echo Yii::t('app7020', '<span>By selecting one or more topics,</span> you\'ll enable'); ?></h4>
						<h4><?php echo Yii::t('app7020', 'Experts <span>(assigned to that topics)</span> to'); ?></h4>
						<h4><?php echo Yii::t('app7020', 'answer learners\' questions!'); ?></h4>
					</div>
                    */ ?>
				</div>
				<?php /*
				<div class="span6" id="lo-7020-topics-container">
					<div id="rightSideBar">
						<?php $this->renderPartial('app7020.protected.views.includes._treeWidget', array('preSelected' => implode(",", $preSelectedTopics), 'isDisabledTree' => $lockTopics, 'subTitle' => Yii::t('app7020', 'Categorize LO by topic'))); ?>
                    </div>
				</div>
				*/
				?>
			</div>
			<div class="row-fluid" id="checkBoxHint"  style="display: <?php echo ($isHaveModel) ? 'none' : 'block'; ?>;">
				<div class="span12">
					<div class="hint-wrapper">
						<div class="app7020-hint app7020-hint-lo-tags-topics">
						<h4><?php echo Yii::t('app7020', 'Allow your learners to ask questions'); ?></h4>
						<h4><?php echo Yii::t('app7020', 'to the Experts,<span>that will help</span>'); ?></h4>
						<h4><?php echo Yii::t('app7020', '<span>them learn</span> faster <span>and</span> better!'); ?></h4>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>