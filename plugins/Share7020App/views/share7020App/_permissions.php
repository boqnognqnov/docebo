<section id="app7020-share-permissions-form-container">
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'app7020-share-permissions-form',
		'enableAjaxValidation' => false,
		'method' => 'POST',
		'htmlOptions' => array(
			'class' => "ajax form-horizontal",
		),
	));
	?>

	<div class="border-wraper">
		<div class="row-fluid  app7020-container grey">

			<div class="span4">
				<h5><?php echo Yii::t('app7020', 'Contributions'); ?></h5>
				<div class="details">
					<?php echo Yii::t("app7020", "Define who will be able to contribute with assets") ?>
				</div>
			</div>

			<div id="video-contributions" class="span8">
				<?php
				echo $form->radioButtonList(
						$sendDataToView['model'], 'contribute_permissions', array(
					'contribute_all' => '<span></span>' . Yii::t('app7020', 'All users'),
					'contribute_experts' => '<span></span>' . Yii::t('app7020', 'Experts only'),
					'contribute_admins' => '<span></span>' . Yii::t('app7020', 'Admins only'),
					'contribute_custom' => '<span></span>' . Yii::t('app7020', 'User groups or branches'),
						), $sendDataToView['permissions_radio_list_args']);
				?>



				<span id="share-permissions-counter"><?php echo $sendDataToView['groupsAndBranchesCount']; ?></span>
<?php
echo CHtml::hiddenField("permissions-branches", '');
echo CHtml::hiddenField("permissions-groups", '');
echo CHtml::button(Yii::t("app7020", "Select"), array('class' => 'btn select app7020-button', 'id' => 'app7020-select-groups-branches'));
?>

			</div>

		</div>
	</div>

    <div class="row-fluid  app7020-container">

        <div class="span4">
            <h5><?php echo Yii::t('app7020', 'Peer review'); ?></h5>
            <div class="details">
				<?php echo Yii::t("app7020", "Define policies for the Experts peer review") ?>
            </div>
        </div>

        <div class="span8">
			<?php
			echo $form->radioButtonList($sendDataToView['model'], 'publish_permissions', array(
				'contribute_auto_publish' => '<span></span>' . Yii::t('app7020', 'Automatically approve and publish any asset'),
				'contribute_manual_publish' => '<span></span>' . Yii::t('app7020', 'Manual asset publishing by the experts or admins'),
					), $sendDataToView['permissions_radio_list_args']);
			?>

        </div>

    </div>



    <div class="row-fluid">
        <div class="span4">

        </div>
        <div class="span8">
            <div class="control-group">
                <div class="controls">
					<?php
 					echo CHtml::link(Yii::t("app7020", "Cancel"),array('Share7020App/settings', 'activeTab'=>1),array('class'=>'btn cancel app7020-button'));
					echo CHtml::button(Yii::t("app7020", "Save Changes"), array('class' => 'btn save app7020-button', 'id' => 'share7020-save-permissions'));
					?>
                </div>
            </div>
        </div>
    </div>

	<?php $this->endWidget(); ?>
</section> 
