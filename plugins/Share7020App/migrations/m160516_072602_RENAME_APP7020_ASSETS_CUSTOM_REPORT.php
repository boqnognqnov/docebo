<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160516_072602_RENAME_APP7020_ASSETS_CUSTOM_REPORT extends DoceboDbMigration {

	public function safeUp()
	{
		$reportModel = LearningReportType::model()->findByAttributes(array('id'=>LearningReportType::APP7020_ASSETS));
		if($reportModel){
			$reportModel->title = 'Assets - Statistics';
			$reportModel->save();
		}
		return true;
	}

	public function safeDown()
	{
		
		return true;
	}
	
	
}
