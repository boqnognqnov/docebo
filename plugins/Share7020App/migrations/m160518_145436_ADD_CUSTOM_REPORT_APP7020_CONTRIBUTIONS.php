<?php
/**
 * Yii DB Migration template.
 * 
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 * 
 */
class m160518_145436_ADD_CUSTOM_REPORT_APP7020_CONTRIBUTIONS extends DoceboDbMigration {

	public function safeUp()
	{

		$this->insert('learning_report_type', array(
				'id'=>LearningReportType::APP7020_USER_CONTRIBUTIONS,
				'title'=>'User - Contributions',
		));
		// PUT YOUR MIGRATION-UP CODE HERE
		// DO NOT USE try/catch or DB transactions!
		
		// Internally, this method call is wrapped in a try/catch block and DB transaction.
		// You can throw exceptions here to be catched internally.
		// If you wish to cancel the migration, return false
		return true;
	}

	public function safeDown()
	{
		$this->delete('learning_report_type', 'id = :id', array(
			':id' => LearningReportType::APP7020_USER_CONTRIBUTIONS
		));

		return true;
	}
	
}
