<section id="questions-and-answers-settings">
    <div class="row-fluid">
        <div class="control-group">
            <span><?= Yii::t('app7020', 'Sort by') ?></span>
            <div class="controls">
                <div class="row-fluid" id="selected-sorting-fields">
                    <select name="selected_sorting_fields" id="selected_sorting_fields" multiple="multiple">
                        <?php if(count($FCBKfieldsToRestore)): ?>
                        <?php foreach($FCBKfieldsToRestore as $key=>$value): ?>
                        <option selected="selected" class="selected" value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <p>
                        <?= Yii::t('app7020', '<b>Tip:</b> Ordering questions by last edit date is useful for keeping users updated on the latest answered questions') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="row-fluid">
                    <?php
                    echo CHtml::checkBox('show_ask_guru_button', $this->params['show_ask_guru_button']);
                    echo CHtml::label(Yii::t('app7020', 'Shows "Ask the Expert" button'), 'show_ask_guru_button', array('class' => 'control-label'));
                    ?>
                    <p class="padding">
                        <?= Yii::t('app7020', 'Allow users to ask questions to the Expert from the dashboard') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="row-fluid">
                    <?php
                    echo CHtml::checkBox('show_search_bar', $this->params['show_search_bar']);
                    echo CHtml::label(Yii::t('app7020', 'Shows search bar'), 'show_search_bar', array('class' => 'control-label'));
                    ?>
                    <p class="padding">
                        <?= Yii::t('app7020', 'Allow users to search among Questions and Answers') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="row-fluid">
                    <span>
                        <?= Yii::t('app7020', 'Display') ?>
                    </span>
                    <?= CHtml::textField('number_items', $this->params['number_items'], array('class' => '')) ?>
                    <span>
                        <?= Yii::t('app7020', 'items in widget') ?>
                    </span>
                    <p>
                        <?= Yii::t('app7020', 'Leave blank to display all items') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="row-fluid">
                    <?php
                    echo CHtml::checkBox('paginate_widget', $this->params['paginate_widget']);
                    echo CHtml::label(Yii::t('app7020', 'Paginate Widget'), 'paginate_widget', array('class' => 'control-label'));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <label class="control-label"></label>
            <div class="controls">
                <div class="row-fluid">
                    <?php
                    echo CHtml::checkBox('show_quetions_and_answers_link', $this->params['show_quetions_and_answers_link']);
                    echo CHtml::label(Yii::t('app7020', 'Show "view full Questions & Answers" link'), 'show_quetions_and_answers_link', array('class' => 'control-label'));
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>