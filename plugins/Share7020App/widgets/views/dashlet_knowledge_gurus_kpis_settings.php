<section id="dashlet-knowledge-gurus-settings">
    <div class="row-fluid">
        <div class="span12">
            <p><strong><?= Yii::t('standard', '_SHOW') ?></strong></p>
            <span class="setting-description compact"><?= Yii::t('app7020', '<b>You must select at least one item</b>. Drag & drop to reorder items') ?></span>
        </div>
    </div>
    <div class="auto-sortable">
        <?php foreach ($settings['checkboxes'] as $key => $label): ?>
            <div class="row-fluid setting-row">
                <div class="span12 setting">
                    <label class="checkbox">
                        <?= CHtml::checkBox($key, $this->getParam($key, false)) ?>
                        <?= $label ?>
                        <span class="p-sprite move setting-handle pull-right"></span>
                    </label>
                </div>
            </div>
        <?php endforeach; ?>
        <?= CHtml::hiddenField('field_order', implode(',', array_keys($settings['checkboxes']))) ?>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <p><strong><?= Yii::t('app7020', 'Timeframe') ?></strong></p>
            <span class="setting-description compact"><?= Yii::t('app7020', 'Selecting "From the beginning" will disable the previous timeframe % comparison and the line chart') ?></span>
        </div>
    </div>
    <?php
    $timeframe = $settings['radioButtons']['timeframe'];
    echo CHtml::radioButtonList($timeframe['name'], $this->getParam($timeframe['name'], 'weekly'), $timeframe['data'], $timeframe['htmlOptions']);
    ?>
</section>    