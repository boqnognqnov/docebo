<section id="knowledge-libraty-settings">
    <div class="row-fluid">
        <div class="span12">
            <p class="start-p"><?= Yii::t('app7020', '<b>Start with following</b> view') ?></p>
            <span class="display-mode"><?= Yii::t('app7020', 'Display mode') ?></span>
            <?php echo CHtml::radioButtonList($radioButtons[0]['name'], $this->getParam($radioButtons[0]['name'], 'list'), $radioButtons[0]['data'], $radioButtons[0]['htmlOptions']); ?>
        </div>
    </div>
    <div class="row-fluid" id="group1-container">
        <?php foreach ($checkBoxes as $checkBox): ?>
            <?php if ($checkBox['group'] == 1) : ?>
                <div class="span12">
                    <label class="checkbox">
                        <?= CHtml::checkBox($checkBox['name'], $this->getParam($checkBox['name'], false)) ?>
                        <?= $checkBox['label']['text'] ?>
                    </label>
                    <p class="description padding"><?= $checkBox['infoText'] ?></p>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="row-fluid">
        <div class="control-group">
            <span><?= Yii::t('app7020', 'Show only the assets that are associated to the following tags') ?></span>
            <div class="controls">
                <div class="row-fluid" id="selected-tags">
                    <select name="showAssetsTags" multiple="multiple" id="showAssetsTags">
                        <?php if (count($FCBKtagsToRestore)): ?>
                            <?php foreach ($FCBKtagsToRestore as $tagKey => $tagVal): ?>
                                <option selected="selected" class="selected" value="<?= $tagKey ?>"><?= Chtml::encode($tagVal) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid" id="sortBy-container">
        <div class="control-group">
            <span><?= Yii::t('app7020', 'Sort by') ?></span>
            <div class="controls">
                <div class="row-fluid" id="selected-sorting-fields">
                    <select name="sortBy" multiple="multiple" id="sortBy">
                        <?php if (count($FCBKsortByfieldsToRestore)): ?>
                            <?php foreach ($FCBKsortByfieldsToRestore as $key => $value): ?>
                                <option selected="selected" class="selected" value="<?= $key ?>"><?= $value ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <span>
                <?= Yii::t('app7020', 'Display') ?>
            </span>
            <?= CHtml::textField($textFields[0]['name'], $this->params[$textFields[0]['name']]) ?>
            <span>
                <?= Yii::t('app7020', 'Items in widget') ?>
            </span>
            <p class="description">
                <?= Yii::t('app7020', 'Leave blank to display all items') ?>
            </p>
        </div>
    </div>
    <div id="group2-container" class="row-fluid">
        <?php foreach ($checkBoxes as $checkBox): ?>
            <?php if ($checkBox['group'] == 2) : ?>
                <div class="span12">
                    <label class="checkbox">
                        <?= CHtml::checkBox($checkBox['name'], $this->getParam($checkBox['name'], false)) ?>
                        <?= $checkBox['label']['text'] ?>
                    </label>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</section>    