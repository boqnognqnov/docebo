<?php

class DashletKnowledgeGurusActivitySummary extends DashletWidget implements IDashletWidget {

    /**
     * 
     */
    public static function descriptor($params = false) {
        $sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/kg_widget_sample.jpg";

        $descriptor = new DashletDescriptor();

        $descriptor->name = 'core_dashlet_knowledgegurusactivitysummary';
        $descriptor->handler = 'plugin.Share7020App.widgets.DashletKnowledgeGurusActivitySummary';
        $descriptor->title = 'Knowledge Guru Activities Summary';
        $descriptor->description = Yii::t('app7020', 'Shows the Expert Activities summary: Knowledge to create, assets...');
        $descriptor->sampleImageUrl = $sampleImageUrl;
        $descriptor->settingsFieldNames = array('');
        $descriptor->disallowMultiInstance = true; // do not allow more than one dashlet of this type in the same layout

        return $descriptor;
    }

    /**
     * @see IDashletWidget::renderSettings()
     */
    public function renderSettings() {
        $this->render('dashlet_knowledge_gurus_activity_summary_settings');
    }

    /**
     * @see IDashletWidget::renderFrontEnd()
     */
    public function renderFrontEnd() {
        if ($this->dashletModel) {
            $this->render('dashlet_knowledge_gurus_activity_summary');
        } else {
            // do something
        }
    }

    /**
     * @see IDashletWidget::bootstrap() 
     */
    public function bootstrap($params = false) {
        Yii::app()->getClientScript()->registerCssFile(App7020Helpers::getAssetsUrl() . '/css/app7020.css');
    }


}
