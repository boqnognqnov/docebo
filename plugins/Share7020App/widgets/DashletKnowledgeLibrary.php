<?php

class DashletKnowledgeLibrary extends DashletWidget implements IDashletWidget {

    /**
     * 
     */
    public static function descriptor($params = false) {
        $sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/knowledge_library_widget_sample.jpg";

        $descriptor = new DashletDescriptor();


        $descriptor->name = 'core_dashlet_knowledgelibrary';
        $descriptor->handler = 'plugin.Share7020App.widgets.DashletKnowledgeLibrary';
        $descriptor->title = 'Knowledge Library';
        $descriptor->description = Yii::t('app7020', 'Shows the assets in the Knowledge Library');
        $descriptor->sampleImageUrl = $sampleImageUrl;
        $descriptor->settingsFieldNames = self::_inputs(true);
        $descriptor->disallowMultiInstance = true; // do not allow more than one dashlet of this type in the same layout

        return $descriptor;
    }

    /**
     * @see IDashletWidget::renderSettings()
     */
    public function renderSettings() {
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;

        // FCBK
        $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
        $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');


        $tmpUrlJS = Yii::app()->mydashboard->getModule()->getAssetsUrl();
        Yii::app()->getClientScript()->registerScriptFile($tmpUrlJS . '/js/knowledge_library_settings.js');




        $inputs = self::_inputs();

        // FCBK - Tags
        $FCBKtagsToRestore = array();
        $selectedTags = $this->getParam($inputs['multipleSelects'][0]['name'], false);
        $tags = App7020Tag::getAllTags();
        foreach ($tags as $tag) {
            $availTags[$tag['id']] = Chtml::encode($tag['tagText']);
        }
        if (is_array($selectedTags)) {
            foreach ($selectedTags as $selectedTag) {
                $FCBKtagsToRestore[$selectedTag] = $availTags[$selectedTag];
            }
        }

        // FCBK - sortBy
        $FCBKsortByfieldsToRestore = array();
        $selectedSortingFields = $this->getParam($inputs['multipleSelects'][1]['name'], false);
        $availSortingFields = self::getAvailableSortingFields();
        if (is_array($selectedSortingFields)) {
            foreach ($selectedSortingFields as $field) {
                $FCBKsortByfieldsToRestore[$field] = $availSortingFields[$field];
            }
        }



        $this->render('dashlet_knowledge_library_settings', array(
            'radioButtons' => $inputs['radioButtons'],
            'checkBoxes' => $inputs['checkBoxes'],
            'multipleSelects' => $inputs['multipleSelects'],
            'textFields' => $inputs['textFields'],
            'FCBKsortByfieldsToRestore' => $FCBKsortByfieldsToRestore,
            'FCBKtagsToRestore' => $FCBKtagsToRestore,
                )
        );
    }

    /**
     * @see IDashletWidget::renderFrontEnd()
     */
    public function renderFrontEnd() {
        if ($this->dashletModel) {
            
            $this->render('dashlet_knowledge_library', array(
                
                'widgetParams' => $this->params
            ));
        }
    }

    /**
     * @see IDashletWidget::bootstrap() 
     */
    public function bootstrap($params = false) {
        $cs = Yii::app()->getClientScript();
		$am = Yii::app()->assetManager;
		$ft = new FancyTree();
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree-all.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile($ft->getAssetsUrl() . '/jquery.fancytree.docebo.js', CClientScript::POS_END);
		Yii::app()->getClientScript()->registerCssFile($ft->getAssetsUrl() . '/skin-tree-widget/ui.fancytree.css');
		App7020Helpers::registerApp7020Options();
		$cs->registerScriptFile(App7020Helpers::getAssetsUrl() . '/js/app7020.js', CClientScript::POS_END);
    }

    /**
     * Returns all inputs configurations
     * @param boolean $allowedInputs
     * @return array
     */
    private static function _inputs($allowedInputs = false) {
        $labelClass = 'control-label';

        $radioButtonsTemplate = '<div class="control-group">{input}{label}</div>';

        $radioButtons = array(
            array(
                'name' => 'displayMode',
                'data' => array(
                    'tumbnails' => Yii::t('app7020', 'Thumbnails'),
                    'list' => Yii::t('app7020', 'List'),
                ),
                'htmlOptions' => array(
                    'labelOptions' => array(
                        'class' => $labelClass,
                    ),
                    'container' => '',
                    'separator' => '',
                    'template' => $radioButtonsTemplate,
                ),
            ),
        );

        $checkBoxes = array(
            array(
                'group' => 1,
                'name' => 'showContribute',
                'label' => array(
                    'text' => Yii::t('app7020', 'Show "Contribute" button'),
                ),
                'infoText' => Yii::t('app7020', 'Allow users to upload assets from the dashboard'),
            ),
            /*array(
                'group' => 1,
                'name' => 'showMyContribute',
                'label' => array(
                    'text' => Yii::t('app7020', 'Show "My Contribute" button'),
                ),
                'infoText' => Yii::t('app7020', 'Allow users to access the assets they uploaded from the dashboard'),
            ),
            */
//            array(
//                'group' => 1,
//                'name' => 'showTopics',
//                'label' => array(
//                    'text' => Yii::t('app7020', 'Show <b>topics</b> selector'),
//                ),
//                'infoText' => Yii::t('app7020', 'Allow users to browse and select Topics'),
//            ),
//            array(
//                'group' => 1,
//                'name' => 'showFilters',
//                'label' => array(
//                    'text' => Yii::t('app7020', 'Show <b>filters</b> selector'),
//                ),
//                'infoText' => Yii::t('app7020', 'Allow users to filter assets'),
//            ),
            array(
                'group' => 1,
                'name' => 'showTumbnailsList',
                'label' => array(
                    'text' => Yii::t('app7020', 'Show <b>thumbnails/list</b> switch'),
                ),
                'infoText' => Yii::t('app7020', 'Allow users to switch display mode'),
            ),
            /*array(
                'group' => 1,
                'name' => 'showSearch',
                'label' => array(
                    'text' => Yii::t('app7020', 'Show <b>search</b> bar'),
                ),
                'infoText' => Yii::t('app7020', 'Allow users to search courses'),
            ),*/
            array(
                'group' => 2,
                'name' => 'paginateWidget',
                'label' => array(
                    'text' => Yii::t('app7020', 'Paginate widget'),
                ),
            ),
            array(
                'group' => 2,
                'name' => 'showViewFullKnowledgeLibraryLink',
                'label' => array(
                    'text' => Yii::t('app7020', 'Show "view full Knowledge Library" link'),
                ),
            ),
        );

        $multipleSelects = array(
            array(
                'name' => 'showAssetsTags'
            ),
            array(
                'name' => 'sortBy'
            ),
        );

        $textFields = array(
            array(
                'name' => 'displayNumberItems'
            ),
        );



        if ($allowedInputs) {
            // Allowed fields for descriptor()
            $inputs = array_merge(array_column($radioButtons, 'name'), array_column($checkBoxes, 'name'), array_column($multipleSelects, 'name'), array_column($textFields, 'name'));
        } else {
            // Arrays for Setting view
            $inputs = array(
                'radioButtons' => $radioButtons,
                'checkBoxes' => $checkBoxes,
                'multipleSelects' => $multipleSelects,
                'textFields' => $textFields,
            );
        }
        return $inputs;
    }

    /**
     * Return available sorting fields and their labels for Settings UI
     * 
     * @return array
     */
    public static function getAvailableSortingFields() {
        return array(
            'creation_date_asc' => Yii::t('app7020', 'Creation date (ascending order)'),
        );
    }

}
