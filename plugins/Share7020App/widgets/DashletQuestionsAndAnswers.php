<?php

class DashletQuestionsAndAnswers extends DashletWidget implements IDashletWidget {

    /**
     * 
     */
    public static function descriptor($params = false) {
        $sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/questions_answers_widget_sample.jpg";

        $descriptor = new DashletDescriptor();

        $descriptor->name = 'core_dashlet_quetionsandanswers';
        $descriptor->handler = 'plugin.Share7020App.widgets.DashletQuestionsAndAnswers';
        $descriptor->title = 'Questions & Answers';
        $descriptor->description = Yii::t('app7020', 'Shows a customizable feed of questions and answers by learners and Experts ');
        $descriptor->sampleImageUrl = $sampleImageUrl;
        $descriptor->settingsFieldNames = array('selected_sorting_fields', 'show_ask_guru_button', 'show_search_bar', 'number_items', 'paginate_widget', 'show_quetions_and_answers_link');
        $descriptor->disallowMultiInstance = true; // do not allow more than one dashlet of this type in the same layout

        return $descriptor;
    }

    /**
     * @see IDashletWidget::renderSettings()
     */
    public function renderSettings() {
        $cs = Yii::app()->getClientScript();
        $themeUrl = Yii::app()->theme->baseUrl;

        // FCBK
        $cs->registerCssFile($themeUrl . '/css/fcbkcomplete.css');
        $cs->registerScriptFile($themeUrl . '/js/jquery.fcbkcomplete.js');

        $tmpUrlJS = Yii::app()->mydashboard->getModule()->getAssetsUrl();
         Yii::app()->getClientScript()->registerScriptFile($tmpUrlJS . '/js/questions_and_answers_settings.js');

        
        // FCBK - selected_sorting_fields
        $FCBKfieldsToRestore = array();
        $selectedSortingFields = $this->getParam('selected_sorting_fields', array());
        $availSortingFields = self::getAvailableSortingFields();
        if (is_array($selectedSortingFields)) {
            foreach ($selectedSortingFields as $field) {
                $FCBKfieldsToRestore[$field] = $availSortingFields[$field];
            }
        }




        $this->render('dashlet_questions_and_answers_settings', array('FCBKfieldsToRestore' => $FCBKfieldsToRestore));
    }

    /**
     * @see IDashletWidget::renderFrontEnd()
     */
    public function renderFrontEnd() {

        if ($this->dashletModel) {
            
            $this->render('dashlet_questions_and_answers', array(
                
                'widgetOptions' => $this->params
            ));
        }
    }

    /**
     * @see IDashletWidget::bootstrap() 
     */
    public function bootstrap($params = false) {
        Yii::app()->tinymce;
    }

    /**
     * Return available sorting fields and their labels for Settings UI
     * 
     * @return array
     */
    public static function getAvailableSortingFields() {
        return array(
            'last_edit_date_asc' => Yii::t('app7020', 'Last edit date (ascending order)'),
        );
    }
    
    public function renderPartial(){
        
    }

}
