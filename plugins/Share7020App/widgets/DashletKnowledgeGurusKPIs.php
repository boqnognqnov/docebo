<?php

class DashletKnowledgeGurusKPIs extends DashletWidget implements IDashletWidget {

    protected static $kpiList;

    /**
     * Dashlet descriptor
     *
     * @return DashletDescriptor Object, describing the dashlet to the outside world
     */
    public static function descriptor($params = FALSE) {
        $sampleImageUrl = Yii::app()->mydashboard->getModule()->getAssetsUrl() . "/images/kg_kpis_widget_sample.jpg";

        $descriptor = new DashletDescriptor();
        $descriptor->name = 'core_dashlet_mykpis';
        $descriptor->handler = 'plugin.Share7020App.widgets.DashletKnowledgeGurusKPIs';
        $descriptor->title = Yii::t('app7020', 'Expert KPIs');
        $descriptor->description = Yii::t('app7020', 'Shows a list of statistics and KPIs for the Expert to be used for engagement');
        $descriptor->sampleImageUrl = $sampleImageUrl;
        $descriptor->settingsFieldNames = self::_allowedInputs();
        $descriptor->disallowMultiInstance = true; // do not allow more than one dashlet of this type in the same layout

        return $descriptor;
    }

    /**
     * Register any assets here (e.g. scripts or styles)
     */
    public function bootstrap($params = false) {
        
    }

    /**
     * Render dashlet settings, used in Admin app7020 UI
     */
    public function renderSettings() {
        $tmpAvailKPIs = self::getKPIList();

        $orderedSettings = array();

        // If editing a dashlet, restore custom ordering of fields, if set
        $fieldsOrdered = explode(',', $this->getParam('field_order'));
        if (!empty($fieldsOrdered)) {
            foreach ($fieldsOrdered as $kpi) {
                if (in_array($kpi, array_keys($tmpAvailKPIs))) {
                    $orderedSettings[$kpi] = $tmpAvailKPIs[$kpi];
                }
            }
        }
        // Finally, add all settings not yet added by the ordered adding above.
        // On initial dashlet creation this array will just be populated with
        // the original settings in their original order
        foreach ($tmpAvailKPIs as $setting => $label) {
            if (!in_array($setting, array_keys($orderedSettings)))
                $orderedSettings[$setting] = $label;
        }

        $this->render('dashlet_knowledge_gurus_kpis_settings', array(
            'settings' => array(
                'checkboxes' => $orderedSettings,
                'radioButtons' => self::_radioButtons(),
            ),
        ));
    }

    /**
     * Render fronend content
     */
    public function renderFrontEnd() {
        
        $this->render('dashlet_knowledge_gurus_kpis', array(
            'widgetOptions' => $this->params
        ));
    }

    /**
     * Return alowed fields for settings view
     * @return array
     */
    private static function _allowedInputs() {
        $checkBoxes = array_keys(self::getKPIList());
        $radioButtons = array_column(self::_radioButtons(), 'name');
        $arr = array_merge(array('field_order'), $checkBoxes, $radioButtons);
        return $arr;
    }

    /**
     * Returns list of KPIs 
     *
     * @return array
     */
    public static function getKPIList() {

        if (!empty(self::$kpiList)) {
            return self::$kpiList;
        }

        $list = array(
            'answered_questions' => Yii::t('app7020', 'Answered Questions'),
            'reviewed_assets' => Yii::t('app7020', 'Reviewed assets'),
            'satisfied_knowledge_requests' => Yii::t('app7020', 'Satisfied Knowledge Requests'),
            'what_learners_think' => Yii::t('app7020', 'What do learners think about my answers'),
            'answers_marked_as_best' => Yii::t('app7020', 'My answers marked as "Best Answer"'),
        );

        return $list;
    }

    /**
     * It returns content for CHtml::radioButtonList in dashlet_knowledge_gurus_kpis_settings.php view
     * @return array
     */
    private static function _radioButtons() {
        return array(
            'timeframe' => array(
                'name' => 'timeframe',
                'data' => array(
                    'weekly' => Yii::t('app7020', 'Weekly'),
                    'monthly' => Yii::t('app7020', 'Monthly'),
                    'from_beginning' => Yii::t('app7020', 'From the beginning'),
                ),
                'htmlOptions' => array(
                    'labelOptions' => array(
                        'class' => 'control-label',
                    ),
                    'container' => '',
                    'separator' => '',
                    'template' => '<div class="control-group">{input}{label}</div>',
                ),
            ),
        );
    }
}


// ======================================================================
//            Implementation of array_column, if PHP <= 5.5
// ======================================================================
if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }

}

