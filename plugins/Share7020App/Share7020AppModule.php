<?php
/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 12.10.2015 �.
 * Time: 18:10
 */

class Share7020AppModule extends CWebModule{

    public function init(){
		//if (Yii::app()->user->isGodAdmin) {
			$this->setImport(array(
                'Share7020App.controllers.*',
				'Share7020App.widgets.*'
			));
            if (Yii::app()->user->isGodAdmin) {
                // import the module-level models and components
                // Register hooks to add "edit lo" tab for 7020
                Yii::app()->event->on('RenderEditLoTabs', array($this, 'renderEditLoTabs'));
                Yii::app()->event->on('RenderEditLoTabsContent', array($this, 'renderEditLoTabsContent'));
                Yii::app()->event->on('AfterSaveLo', array($this, 'saveLoTabContent'));


                Yii::app()->event->on(EventManager::EVENT_ON_PLUGIN_DEACTIVATE, array($this, 'onPluginDeactivate'));
            }

			//Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'installKnowledgeLibraryDashlet'));
            //Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'install7020Dashlets'));
		//}
    }

    public function menu() {

    }

    public function installKnowledgeLibraryDashlet(DEvent $event){
        if(!isset($event->params['descriptors']))
            return;

        $knowledgeLibraryDescriptor = DashletKnowledgeLibrary::descriptor();

        $collectedSoFarHandlers = array();
        foreach($event->params['descriptors'] as $descriptorObj){
            if ($descriptorObj instanceof DashletDescriptor)
                $collectedSoFarHandlers[] = $descriptorObj->handler;
        }

        if(!in_array($knowledgeLibraryDescriptor->handler, $collectedSoFarHandlers)){
            // Add the SubscriptionCodesDashlet dashlet to the event params
            $event->params['descriptors'][] = $knowledgeLibraryDescriptor;
        }
    }


    /**
     * Adds the tab to the edit LO page
     * @param $event DEvent
     */
    public function renderEditLoTabs($event) {
        $courseModel = $event->params['courseModel']; /* @var $courseModel LearningCourse */
        $loModel = $event->params['loModel']; /* @var $loModel LearningOrganization */

        $controller = new Share7020AppController('Share7020App', $this);
        $controller->renderEditLoTab($courseModel, $loModel);
    }

    /**
     * Renders the tab content in edit lo page
     * @param $event DEvent
     */
    public function renderEditLoTabsContent($event) {
        $courseModel = $event->params['courseModel']; /* @var $courseModel LearningCourse */
        $loModel = $event->params['loModel']; /* @var $loModel LearningOrganization */

        $controller = new Share7020AppController('Share7020App', $this);
        $controller->renderEditLoTabContent($courseModel, $loModel);
    }

    /**
     * Saves edit log tab content
     * @param $event
     */
    public function saveLoTabContent($event) {
        $controller = new Share7020AppController('Share7020App', $this);
//		$controller->saveLoTabContent($event->params['loModel']);
    }



    public function install7020Dashlets(DEvent $event) {
        if (!isset($event->params['descriptors']))
            return;

        $knowledgeGuruActivitySummaryDescriptor = DashletKnowledgeGurusActivitySummary::descriptor();
        $knowledgeGuruKPIsDescriptor = DashletKnowledgeGurusKPIs::descriptor();
        $questionsAndAnswersDescriptor = DashletQuestionsAndAnswers::descriptor();

        $collectedSoFarHandlers = array();
        foreach ($event->params['descriptors'] as $descriptorObj) {
            if ($descriptorObj instanceof DashletDescriptor)
                $collectedSoFarHandlers[] = $descriptorObj->handler;
        }

        if (!in_array($knowledgeGuruActivitySummaryDescriptor->handler, $collectedSoFarHandlers)) {
            // Add the SubscriptionCodesDashlet dashlet to the event params
            $event->params['descriptors'][] = $knowledgeGuruActivitySummaryDescriptor;
        }

        if (!in_array($knowledgeGuruKPIsDescriptor->handler, $collectedSoFarHandlers)) {
            // Add the SubscriptionCodesDashlet dashlet to the event params
            $event->params['descriptors'][] = $knowledgeGuruKPIsDescriptor;
        }

        if (!in_array($questionsAndAnswersDescriptor->handler, $collectedSoFarHandlers)) {
            // Add the SubscriptionCodesDashlet dashlet to the event params
            $event->params['descriptors'][] = $questionsAndAnswersDescriptor;
        }
    }

    public function onPluginDeactivate(DEvent $event) {
        if (!isset($event->params['pluginName']) || $event->params['pluginName'] !== $this->name) {
            $event->return_value['canProceed'] = true;
            return;
        }

        if (PluginManager::deactivateAppByCodename('Share7020')) {
            $CorePluginModel = CorePlugin::model()->find(
                array(
                    'condition' => 'plugin_name=:plugin_name',
                    'params' => array(':plugin_name' => 'Share7020App')
                ));
            // call the api to remove this app for the current installation
            $apiParams = array(
                'installation_id' => Docebo::getErpInstallationId(),
                'app_id' => $CorePluginModel->plugin_id
            );
            AppsMpApiClient::apiAppCancel($apiParams);
            $event->return_value['canProceed'] = true;
            return;
        }
    }
}