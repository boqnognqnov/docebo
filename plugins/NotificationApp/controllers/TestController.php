<?php
class TestController extends Controller {
	public function actionTest(){
		$test = $this->getSendTypes(82);

		foreach($test as $type => $handlerClass){
			if(!@class_exists($handlerClass)){
				throw new CException('The notification handler class '.$handlerClass.' does not exist.');
			}

			$job = CoreJob::model()->findByAttributes(array(
				'name' => 'immediate_notification'
			));
			$jobParams = CJSON::decode($job->params);
			$notHandler = new NewCourse($job);
			$notification = CoreNotification::model()->findByPk($jobParams['notification_id']);

			$response = $notHandler->getRecipientsData();
//			var_dump($response);die;

			// Notification handlers MUST return a response of specific class: NotificationHandlerResponse
			if (!is_object($response)  || get_class($response) != 'NotificationHandlerResponse') {
				Yii::log('Returned response was not of type NotificationHandlerResponse', CLogger::LEVEL_ERROR);
				return;
			}

			// NO data? Get out!
			if (empty($response->languages) || empty($response->recipients)) {
				Yii::log('Language or recipients are empty', CLogger::LEVEL_ERROR);
				return;
			}

			// Inspect returned list of *recipeints* languages and see if Notification IS actually translated in all of them.
			// If some is not, change the recipient's language to some valid one ("good" one).
			// The result of this code is a final array of "good languages", meaning notification is translated into those languages and
			// recipient's languages have been adjusted to be one of them (i.e. to be a "good" language).
			// This is to ensure that recipient WILL receive the notification, in some language, no matter what!
			$goodLanguages = array();
			foreach ($response->languages as $languageCode) {
				$goodLanguage = $languageCode;
				if (!$notification->isTranslatedInLanguage(Lang::getBrowserCodeByCode($languageCode))) {
					// Resolve the usable language
					$newLanguageCode = $notification->resolveUsableLanguage($languageCode);
					// Now enumerate all recipients's data and change the language to the new one (good one)
					foreach ($response->recipients as $idUser => $userData) {
						if ($userData['language'] == $languageCode)
							$response->recipients[$idUser]['language'] = $newLanguageCode;
					}
					$goodLanguage = $newLanguageCode;
				}
				$goodLanguages[] = $goodLanguage;
			}

			// Modify response languages to "good" languages
			$response->languages = array_unique($goodLanguages);

			// Collect Subjects[] and Messages[] in all languages
			foreach ($response->languages as $languageCode) {
				$subject = $notification->subject(Lang::getBrowserCodeByCode($languageCode));
				$message = $notification->message(Lang::getBrowserCodeByCode($languageCode));

				$subjects[$languageCode]	= $subject;
				$messages[$languageCode]	= $message;
			}
			$finalDataHash = array(
				'recipients'	=> $response->recipients,
				'languages' 	=> $response->languages,
				'subjects'		=> $subjects,
				'messages'		=> $messages,
			);

//			var_dump($finalDataHash);die;

			$handler = new $handlerClass;
			$handler->deliver($finalDataHash, $notHandler, $notification);
		}
	}

	private function getSendTypes($notificationId){
		$result = array();
		$tmp = Yii::app()->db->createCommand("
			SELECT t1.transport_id, t1.subject, t2.name, t2.info
			FROM ".CoreNotificationTranslation::model()->tableName()." AS t1
			INNER JOIN ".CoreNotificationTransport::model()->tableName()." AS t2 ON t1.transport_id = t2.id
			WHERE t1.notification_id = ".$notificationId."
		")->queryAll();
		foreach($tmp as $t){
			$info = CJSON::decode($t['info']);
			$result[$t['name']] = $info['manager_class'];
		}
		return $result;
	}
}