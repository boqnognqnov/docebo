<? if (!isset($isPlaying)) { $isPlaying = false; } ?>
<? if ($testTrack) : ?>

    <?php if (!$isPlaying) {
        echo '<div class="notification-test-review l-testpoll review-dialog review-answers" style="width:850px;font-family: \'Open Sans\', sans-serif;line-height: 18px; font-size: 14px; color: rgb(68, 68, 68);">';
    } ?>

    <div class="row-fluid">
        <div class="span2">
            <a href="javascript:;" class="test-details-print" onclick="window.print()" style="display: none;">
                <?=CHtml::image('plugin/NotificationApp/assets/images/print.png')?>
            </a>&nbsp;
        </div>
        <div class="span10 text-right test-review-title">
            <div class="pull-right">
            </div>
            <p class="review-header" style="font-size:24px; margin: 8px; display: block; text-align: right; overflow-wrap: break-word;"><?php

                echo '<span class="review-icon">'
                    .( $testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED
                        ? '<span class="p-sprite remove-small-red" style="background-image: url("'.Docebo::getRootBaseUrl(true) . "/themes/spt/images/player-sprite.png".'") !important; width:20px; height:20px;vertical-align:middle;display:inline-block;*display:inline;zoom:1; margin-right: 5px;background-position: -655px -96px; width: 20px; height: 20px;margin-top: -5px;"></span>'
                        : '<span class="p-sprite check-solid-small-green" style="background-image: url("'.Docebo::getRootBaseUrl(true) . "/themes/spt/images/player-sprite.png".'") !important; width:20px; height:20px;vertical-align:middle;display:inline-block;*display:inline;zoom:1; margin-right: 5px;background-position: -623px -96px; margin-top: -5px;"></span>' )
                    .'</span>&nbsp;';

                echo Yii::t('standard', '_FINAL_SCORE') . ':&nbsp;';

                //check if we are showing points or percentage
                switch ($test->point_type) {
                    case LearningTest::POINT_TYPE_NORMAL: {
                        $userScore = number_format($testTrack->getCurrentScore(),2);
                        echo '<b class="' . ($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '" style="color:'.(($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED)?'#FF0103':'rgb(111, 207, 111)').';">'
                            // . number_format($testTrack->getCurrentScore(),2) . ($test->order_type < 2 ? '/'. number_format($test->getMaxScore(),2) : '' ).'</b>';
                            . $userScore . '</b>';
                    } break;
                    case LearningTest::POINT_TYPE_PERCENT: {
                        $_percent = $testTrack->getCurrentScore();
                        $_percent = ($_percent > 0)? $_percent : 0;
                        $userScore = round($_percent, 1, PHP_ROUND_HALF_EVEN);
                        echo '<b class="' . ($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED ? 'score-failed' : 'score-passed' ) . '" style="color:'.(($testTrack->score_status == LearningTesttrack::STATUS_NOT_PASSED)?'#FF0103':'rgb(111, 207, 111)').';">'
                            . $userScore . ($commonTrack->score_max == 100 ? '</b> <span class="review-percentage">%</span>' : '');
                    } break;
                }
                ?>
            </p>
            <?php
            $manualScore = $testTrack->getMaxManualScore();
            if ($manualScore > 0 && $testTrack->score_status != LearningTesttrack::STATUS_VALID) {
                echo '<p class="review-manual-score">'
                    . Yii::t('test', '_TEST_MANUAL_SCORE')
                    . ' <b>' . $manualScore . '</b> ' . (($test->point_type == LearningTest::POINT_TYPE_NORMAL || $commonTrack->score_max != 100) ? Yii::t('test', '_TEST_SCORES') : ' <span class="review-percentage">%</span>')
                    . '</p>';
            } ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="block-hr" style="background: #E6E6E6; height: 1px; margin: 1px 0; width: 100%;"></div>
    <!-- answers -->
    <div class="row-fluid"><?php

        //TODO: this is a fix, write it in a better way
        $answeredQuestions = $testTrack->getAnsweredQuestions();
        $allQuestions = array();
        foreach ($testTrack->questsWithTitles as $i => $question) {
            $allQuestions[$question->getPrimaryKey()] = $question;
        }

        //TODO: test partially done should be managed in a different way
        foreach ($answeredQuestions as $i => $answeredQuestion) :

            $question = $allQuestions[$answeredQuestion['idQuest']];
            if(!$question)
                continue;

            $questionPlayer = CQuestionComponent::getQuestionManager($question->type_quest);

            $userAnswer = LearningTesttrackAnswer::model()->findAllByAttributes(array(
                'idTrack' => $testTrack->idTrack,
                'idQuest' => $question->idQuest,
            ));


            if ($question->type_quest == LearningTestquest::QUESTION_TYPE_TITLE) : ?>
                <div class="span12 test-details-type-title"><span><?=$question->title_quest?></span></div>
            <?php else : ?>
                <div class="block <?= (!($i%2)) ? 'odd' : '' ?>" style="padding: 15px 25px;<?= (!($i%2)) ? 'background: #F1F3F2;' : '' ?>">
                    <div class="row-fluid">
                        <div class="span2 question-type" style="display: inline-block;vertical-align: top;margin-top: 14px;margin-right: 50px;width: 12%;"><?=isset($questionsLang[$question->type_quest]) ? $questionsLang[$question->type_quest] : '-'?></div>
                        <div class="span10 question-info" style="display: inline-block;vertical-align: top;width: 81%;">
                            <div class="test-details-question-title" style="font-weight: bold;">
                                <span class="pull-left" style="vertical-align: top; margin-top: 13px; display: inline-block;"><?=$i+1?>)&nbsp;</span> <span class="question-title-name" style="display:inline-block;"><?= Docebo::nbspToSpace($questionPlayer->applyTransformation('review', $question->title_quest, array(
                                    'idQuestion' => $question->idQuest,
                                    'idTrack' => $testTrack->idTrack,
                                    'questionManager' => $questionPlayer,
                                    'correctAnswerStyle' => 'color:#5EBE5D;',
                                    'incorrectAnswerStyle' => 'color:#FF0103;',
                                )));?> </span>
                            </div>
                            <?php
                            if ($isPlaying) {
                                $_showCorrectAnswers = $test->correctAnswersVisibleOnReview($userId);
                                $_showAnswers = $test->answersVisibleOnReview($userId);
                            } else {
                                $_showCorrectAnswers = true;
                                $_showAnswers = true;
                            }
                            $this->widget('player.components.widgets.TestReportsAnswerFormatter', array(
                                'isForNotifications' => true,
                                'questionType' => $question->type_quest,
                                'questId' => $question->idQuest,
                                'trackId' => $testTrack->idTrack,
                                'testId' => $testTrack->idTest,
                                'userId' => $userId,
                                'courseId' => $organization->idCourse,
                                'isTeacher' => (isset($isTeacher) ? (bool)$isTeacher : false),
                                'showCorrectAnswers' => $_showCorrectAnswers,
                                'showAnswers' => $_showAnswers,
                                'skipNewLine' => true,
                                'skipLinkInEvaluationBtn' => true,
                            ));
                            ?>
                        </div>
                    </div>
                    <div style="clear:both"><!-- this is needed by IE8 --></div>
                </div>
                <div class="block-hr" style="background: #E6E6E6; height: 1px; margin: 1px 0; width: 100%;"></div>
            <? endif; ?>
        <? endforeach ?>
    </div>
<? else : ?>
    <div style="margin: 50px;"><?=Yii::t('standard', '_NO_CONTENT')?></div>
<? endif; ?>

<?php if (!$isPlaying) {
    echo '</div>';
} ?>

<script>
    $(document).ready(function() {
        try {
            $('.score-editable').editable({
                validate: function(value) {
                    //NOTE: at the moment decimal separator is not managed by international standards.
                    //We are assuming the dot "." as decimal separator, but commas "," are accepted and managed too!
                    //TODO: This must be changed in the future and i18n standards applied for number formats (same thing applies to actionEditTestScore() in ReportController.php)
                    if (isNaN(parseFloat(value.replace(",", ".")))) {
                        return <?= CJSON::encode(Yii::t('player', 'The value must be a number')) ?>;
                    }
                },
                success: function(res, value)
                {
                    res = JSON.parse(res);
                    if (res.success == false)
                        return res.message;
                }
            });
        } catch(e) {}
    });
</script>
<style>

    /*Notification Inbox Specific*/
    /*===========================*/
    div.inbox-dialog, div.inbox-dialog-full-page{
        width: 900px !important;
        margin-left: -475px !important;
    }
    div.inbox-dialog div.notification-test-review, div.inbox-dialog-full-page div.notification-test-review{
        margin-bottom: 64px !important;
    }
    div.inbox-dialog div.notification-test-review div.question-type, div.inbox-dialog-full-page div.notification-test-review div.question-type{
        margin-right: 10px !important;
    }
    div.inbox-dialog div.notification-test-review div.question-info, div.inbox-dialog-full-page div.notification-test-review div.question-info{
        width: 79% !important;
    }
    div.inbox-dialog div.notification-test-review .p-sprite, div.inbox-dialog-full-page div.notification-test-review .p-sprite{
        margin-top: 0px !important;
    }
    div.inbox-dialog div.notification-test-review div.test-details-question-title span.pull-left, div.inbox-dialog-full-page div.notification-test-review div.test-details-question-title span.pull-left{
        margin-top: 0px !important;
    }
    div.inbox-dialog div.notification-test-review div.test-details-question-title span.question-title-name, div.inbox-dialog-full-page div.notification-test-review div.test-details-question-title span.question-title-name{
        display: inline !important;
    }
    /*===========================*/

    /* LIKERT SCALE */
    @media print {
        .container, .menu {
            display:none;
        }
        .docebo {
            width: 950px;
        }
        .fancybox-inner {
            display:block;
        }
    }
</style>