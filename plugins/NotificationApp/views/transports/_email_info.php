<div class="row-fluid">

	<div class="span6">
		<div class="control-group">
			<label><?= Yii::t('standard', 'From (name)') ?></label>
			<?= CHtml::textField('email_transport_additional_info[from_name]', $from_name, array('class' => 'span12')) ?>
		</div>
	</div>

	<div class="span6">
		<div class="control-group">
			<label><?= Yii::t('standard', 'From (email)') ?></label>
			<?= CHtml::textField('email_transport_additional_info[from_email]', $from_email, array('class' => 'span12')) ?>
		</div>
	</div>

</div>