<?php //echo Yii::t('app7020','<p>Hi,</p><p>[app7020_owner_answer] answered to your question. You can see the new answer &nbsp;[app7020_question_url]</p>', array(), null, $shortLang)       ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="format-detection" content="address=no" />
		<!--/ GLOBAL STYLES -->
		<style type="text/css">
			@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700);
			body {margin: 0; padding: 0; min-width: 100%; font-family:'Open Sans', Arial; border-collapse:collapse; font-weight:400; border:none; font-size:14px; color:#444444; line-height:22px;}
			table tbody tr td {border-collapse:collapse !important; border-spacing:0px;}
			td ul {margin-top:0px !important; margin-bottom:0px !important;}
			/* FOR ARABIC TURN ON STYLES BELOW 
			td table {direction:rtl;}
			.ltr {direction:ltr !important;}
			.rtl {direction:rtl;}
			.rtl-list {padding:0px 30px 0px 0px !important;}
			td {font-family:Tahoma, Arial !important;}
			.align-left {text-align:left !important;}
			*/
			.assetimage  {width: 180px; margin-right: 20px; border: 1px solid #efefef; float: left;}
			
			/*/ MOBILE STYLES */
			@media only screen and (max-width: 570px) {
				/* FOR ARABIC TURN ON STYLES BELOW 
				td table {direction:rtl;}
				.ltr {direction:ltr !important;}
				.rtl {direction:rtl;}
				.rtl-list.list-items {padding:0px 19px 0px 0px !important;}
				*/
				td {-webkit-text-size-adjust: none;}
				.header {height:50px !important;}
				.logo {width:95px !important; padding-top:0px !important;}
				.btn123 {display:block !important; line-height:15px; padding: 12px 20px 12px 20px !important;}
				.btn-table123 {width:100%;}
				.body-padding {padding:26px 20px !important;}
				.w100 {width: 100% !important; border-left:none; text-align:center;}
				.Spacer {height:12px;}
				.list-items {padding:0px 0px 0px 19px !important;}
				.mob-title {padding-bottom:18px !important;}
				.border-bottom {border-bottom: 1px solid #e6e6e6;}
				.social {border-left: none !important; border-right: none !important; border-bottom:12px solid #f5f5f5; width: 100%; display: block;}
				.step-padding {padding:26px 0px 26px 20px !important;}
				.wrapper {width: 368px; padding: 0px 12px;}
				.inner-img {width: 304px;}
				.markets {font-size:9px !important; letter-spacing:2px !important;}
				.global-footer {padding: 0px 0px 12px 0px !important;}
				.look-ahead {padding-top: 18px;}
				.cities {letter-spacing:0px !important;}
				.align-center {text-align:center !important;}
				.col-stack {width:100% !important; border:none !important;}
				.top-col {padding:33px 0px 15px 0px !important;}
				.bot-col {padding:15px 0px 33px 0px !important;}
				.sg50 {width:100% !important;}
				.sg50 img {width:40px; padding-top: 18px;}
				.assetimage  {width: 100%; margin-bottom: 20px; border: 1px solid #efefef; }
			}
			@media only screen and (max-width: 330px) { 
				.inner-img {width: 256px;}
				.wrapper {width: 320px;}
			}
		</style>
		<!-- / IE STYLES -->
		<!--[if IE]>
		<style type="text/css">
		  .logo {padding-top:0px;}
		@media only screen and (max-width: 570px) {
		.btn-block {width:100%;}
		}
		</style>
		<![endif]-->
		<!-- / OUTLOOK STYLES -->
		<!--[if gte mso 9]>
		<style type="text/css">
		  body {margin: 0; padding: 0; font-family:arial; border-collapse:collapse; border:none; font-size:14px; line-height:22px;}
		  .w550 {width: 550px !important;}
		  .col-stack {width:49%}
		  .social {width:32% !important;padding: 14px 0px;}
		  .social img {padding-top:5px;}
		  .mso-social {background-color:#335782;}
		  .contact {width:24% !important;}
		  td ul {margin-top:0px !important; margin-bottom:0px !important;}
		</style>
		<![endif]-->
	</head>
	<!-- / BODY -->
	<body bgcolor="#f5f5f5;" style="background-color:#f5f5f5;" class="rtl">
		<div style="background-color:#f5f5f5; width:100%;">
			<!--[if (gte mso 9)|(IE)]>
			  <table align="center" class="wrapper" width="100%" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					  <td>
						<table align="center" class="wrapper" width="550" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
								<![endif]-->
			<!-- / WRAPPER -->
			<table align="center" class="wrapper" width="574" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" style="font-size:14px; line-height:22px; padding: 0px 12px;">
				<tr>
					<td>
						<table width="100%" class="w550" align="center" cellpadding="0" cellspacing="0" border="0" style="max-width:550px;">
							<!-- / HEADER START -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#f5f5f5" height="24" class="Spacer"></td>
										</tr>
										<tr>
											<td bgcolor="#f5f5f5" height="12"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- /  HEADER END -->
							<!-- / FULL WIDTH IMAGE BUTTON LAYOUT START -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<!--  100% IMAGE MUST BE 1100PX WIDE NO LESS OR MORE -->
											<td bgcolor="#ffffff" align="center" style="padding-top:30px;">
												<img src="<?= Docebo::getRootBaseUrl(true) . '/themes/spt/images/company_logo.png'; ?>" border="0" alt="Notification" />
											</td>
										</tr>
									</table>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#ffffff" class="body-padding" style="padding:30px 30px; border-bottom: 1px solid #e6e6e6;">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td class="mob-title" style="font-size: 14px; font-weight: 500; line-height: 29px; padding-bottom: 9px; letter-spacing: -0.1px;">Hello [app7020_owner_question],</td>
													</tr>
													<tr>
														<td style="padding-bottom: 20px;">
															<b>[app7020_owner_answer] <?= Yii::t('standard', 'answered', array(), null, $shortLang); ?> </b> <?= Yii::t('standard', 'your question', array(), null, $shortLang); ?>:
													</tr>
													<tr>
														<td >
															<table  border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td style="width: 50px; padding-right: 10px;" valign="top">
																			[app7020_answer_owner_avatar]</td>
																	<td style="padding: 10px; font-size: 13px; background-color: #f5f5f5; border: 1px solid #e4e6e5">
																		[app7020_answer_title]
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<!-- / BUTTON START -->
													<tr>
														<td height="30"></td>
													</tr>
													<tr>
														<td>
															<table class="btn-table123" border="0" cellspacing="0" cellpadding="0">

																<tr>
																	<td class="btn123" bgcolor="#55AB54" style="padding: 8px 20px; -webkit-border-radius:2px; border-radius:2px;  display: inline-block; background-color:#55AB54;" align="center">
																		<a href="[app7020_question_url]" target="_blank" style="color: #ffffff; text-decoration: none;"><?= Yii::t('standard', 'READ THE FULL QUESTION & ANSWERS!', array(), null, $shortLang); ?></a>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<!-- / BUTTON END -->
												</table>
											</td>
										</tr>
										<tr>
											<td height="24" bgcolor="#f5f5f5" class="Spacer"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- / FULL WIDTH IMAGE BUTTON LAYOUT END -->
							<!-- / UNSUBSCRIBE FOOTER START  -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#f5f5f5">
												<table class="align-center" width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="padding-bottom: 9px; font-size:11px; color:#a5a5a5; line-height:14px"><?= Yii::t('standard', 'This message was automatically sent to', array(), null, $shortLang); ?> <a href="#"> [app7020_question_owner_email] </a>. <?= Yii::t('standard', 'If you don\'t want to receive these emails,', array(), null, $shortLang); ?>  <a href="#"><?= Yii::t('standard', 'contact your Administrator', array(), null, $shortLang); ?></a></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="33" bgcolor="#f5f5f5"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- / UK UNSUBSCRIBE FOOTER END  -->
						</table>
					</td>
				</tr>
			</table>
			<!-- / WRAPPER END  -->
			<!--[if (gte mso 9)|(IE)]>
								</td>
							</tr>
						</table>
					  </td>
				  </tr>
			  </table>
			<![endif]-->
		</div>
	</body>
	<!-- / BODY END  -->
</html>
