<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="format-detection" content="address=no" />
		<!-- GLOBAL STYLES -->

		<!--  IE STYLES -->
		<!--[if IE]>
		<style type="text/css">
		  .logo {padding-top:0px;}
		@media only screen and (max-width: 570px) {
		.btn-block {width:100%;}
		}
		</style>
		<![endif]-->
		<!--  OUTLOOK STYLES -->
		<!--[if gte mso 9]>
		<style type="text/css">
		  body {margin: 0; padding: 0; font-family:arial; border-collapse:collapse; border:none; font-size:14px; line-height:22px;}
		  .w550 {width: 550px !important;}
		  .col-stack {width:49%}
		  .social {width:32% !important;padding: 14px 0px;}
		  .social img {padding-top:5px;}
		  .mso-social {background-color:#335782;}
		  .contact {width:24% !important;}
		  td ul {margin-top:0px !important; margin-bottom:0px !important;}
		</style>
		<![endif]-->

	</head>
	<!--  BODY -->
	<body bgcolor="#f5f5f5;" style="background-color:#f5f5f5;" class="rtl">
		<style type="text/css">
			body {margin: 0; padding: 0; min-width: 100%; font-family:'Open Sans', Arial; border-collapse:collapse; font-weight:400; border:none; font-size:14px; color:#444444; line-height:22px;}
			table tbody tr td {border-collapse:collapse !important; border-spacing:0px;}
			td ul {margin-top:0px !important; margin-bottom:0px !important;}
			.assetimage  {width: 250px; height: 127px; border-bottom: 1px solid #efefef;}
			

			/* MOBILE STYLES */
			@media only screen and (max-width: 570px) {
				td {-webkit-text-size-adjust: none;}
				.header {height:50px !important;}
				.logo {width:95px !important; padding-top:0px !important;}
				.btn123 {display:block !important; line-height:15px; padding: 12px 20px 12px 20px !important;}
				.btn-table123 {width:100%;}
				.body-padding {padding:26px 20px !important;}
				.w100 {width: 100% !important; border-left:none; text-align:center;}
				.Spacer {height:12px;}
				.list-items {padding:0px 0px 0px 19px !important;}
				.mob-title {padding-bottom:18px !important;}
				.border-bottom {border-bottom: 1px solid #e6e6e6;}
				.social {border-left: none !important; border-right: none !important; border-bottom:12px solid #f5f5f5; width: 100%; display: block;}
				.step-padding {padding:26px 0px 26px 20px !important;}
				.wrapper {width: 368px; padding: 0px 12px;}
				.inner-img {width: 304px;}
				.markets {font-size:9px !important; letter-spacing:2px !important;}
				.global-footer {padding: 0px 0px 12px 0px !important;}
				.look-ahead {padding-top: 18px;}
				.cities {letter-spacing:0px !important;}
				.align-center {text-align:center !important;}
				.col-stack {width:100% !important; border:none !important;}
				.top-col {padding:33px 0px 15px 0px !important;}
				.bot-col {padding:15px 0px 33px 0px !important;}
				.sg50 {width:100% !important;}
				.sg50 img {width:40px; padding-top: 18px;}
				.assetimage  {width: 100%; border-bottom: 1px solid #efefef; float: left;}
				.watchitnowbtn{text-decoration: none; color: #fff!important;}
				.watchitnowbtn:hover{color:#fff;}
			}
			@media only screen and (max-width: 330px) { 
				.inner-img {width: 256px;}
				.wrapper {width: 320px;}
			}
		</style>
		<div style="background-color:#f5f5f5; width:100%;">
			<!--[if (gte mso 9)|(IE)]>
			  <table align="center" class="wrapper" width="100%" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
				  <tr>
					  <td>
						<table align="center" class="wrapper" width="550" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
								<![endif]-->
			<!--  WRAPPER -->
			<table align="center" class="wrapper" width="574" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0" style="font-size:14px; line-height:22px; padding: 0px 12px;">
				<tr>
					<td>
						<table width="100%" class="w550" align="center" cellpadding="0" cellspacing="0" border="0" style="max-width:550px;">
							<!--  HEADER START -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#f5f5f5" height="24" class="Spacer"></td>
										</tr>

										<tr>
											<td bgcolor="#f5f5f5" height="12"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!--   HEADER END -->
							<!--  FULL WIDTH IMAGE BUTTON LAYOUT START -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<!--  100% IMAGE MUST BE 1100PX WIDE NO LESS OR MORE //-->
											<td bgcolor="#ffffff" align="center" style="padding-top:30px;">
												<img src="<?= Docebo::getRootBaseUrl(true) . '/themes/spt/images/company_logo.png' ?>" border="0" alt="Notification" />
											</td>
										</tr>
									</table>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#ffffff" class="body-padding" style="padding:30px 30px; border-bottom: 1px solid #e6e6e6;">
												<table width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td class="mob-title" style="font-size: 14px; font-weight: 500; line-height: 29px; padding-bottom: 10px; letter-spacing: -0.1px; "><?= Yii::t('standard', 'Hello', array(), null, $shortLang) ?> [first_name],</td>
													</tr>
													<tr>
														<td>

															<table  border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 20px;">
																<tr>
																	<td style="width: 50px; padding-right: 10px;" valign="top">
																			[app7020_inviter_avatar]</td>
																	<td style="font-size: 14px; line-height: 19px;" valign="center">
																		<!--   Inviter      -->
																		<b>[app7020_inviter_name] <?= Yii::t('standard', 'invited you', array(), null, $shortLang); ?> </b><?= Yii::t('standard', 'to watch an asset', array(), null, $shortLang); ?>: <br />
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td style="padding: 20px; background-color: #f5f5f5; border: 1px solid #e4e6e5;" align="center">

															<table width="250px" border="0" cellpadding="0" cellspacing="0" style="background-color:rgb(255,255,255); -moz-box-shadow:0px 1px 2px rgba(0,0,0,0.2); box-shadow:0px 1px 2px rgba(0,0,0,0.2);">
																<tr>
																	<td>[app7020_content_thumb]</td>
																</tr>
																<tr>
																	<td style="padding: 10px">
																		<a style="color: #0465ac; font-weight: bold; text-decoration: none; line-height: 18px;" href="[app7020_content_url]">[app7020_content_title]</a></td>
																</tr>
															</table>
														</td>
													</tr>
													<!--  BUTTON START -->
													<tr>
														<td height="25"></td>
													</tr>
													<tr>
														<td align="center">
															<table class="btn-table123" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td class="btn123" bgcolor="#55AB54" style="padding: 8px 20px; -webkit-border-radius:2px; border-radius:2px;  display: inline-block; background-color:#55AB54;" align="center">
																		<a style="color:#fff!important; text-decoration: none;" href="[app7020_content_url]" target="_blank" class="watchitnowbtn"><?= Yii::t('standard', 'WATCH IT NOW!', array(), null, $shortLang); ?></a>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<!--  BUTTON END -->

												</table>
											</td>
										</tr>
										<tr>
											<td height="24" bgcolor="#f5f5f5" class="Spacer"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!--  FULL WIDTH IMAGE BUTTON LAYOUT END -->
							<!--  UNSUBSCRIBE FOOTER START  -->
							<tr>
								<td>
									<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td bgcolor="#f5f5f5">
												<table class="align-center" width="100%" border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="padding-bottom: 9px; font-size:11px; color:#a5a5a5; line-height:14px"><?= Yii::t('standard', 'This message was automatically sent to', array(), null, $shortLang); ?> <a href="#"> [app7020_recipient_email] </a>. <?= Yii::t('standard', 'If you don\'t want to receive these emails,', array(), null, $shortLang); ?>  <a href="#"><?= Yii::t('standard', 'contact your Administrator', array(), null, $shortLang); ?></a></td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td height="33" bgcolor="#f5f5f5"></td>
										</tr>
									</table>
								</td>
							</tr>
							<!--  UK UNSUBSCRIBE FOOTER END  -->
						</table>
					</td>
				</tr>
			</table>
			<!--  WRAPPER END  -->
			<!--[if (gte mso 9)|(IE)]>
								</td>
							</tr>
						</table>
					  </td>
				  </tr>
			  </table>
			<![endif]-->
		</div>
	</body>
	<!--  BODY END  -->
</html>
<?php //echo Yii::t('app7020', '<p>Hello</p><p>You have been invited to watch a new content named [app7020_content_title]</p><p>Click to link to watch [app7020_content_url]</p>', array(), null, $shortLang) ?>
