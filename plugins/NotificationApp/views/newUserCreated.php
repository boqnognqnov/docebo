<?php
/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 02-Jun-16
 * Time: 2:24 PM
 */
?>
<style type="text/css">
	@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300,500,600,700);

	.innerContent {
		margin: 0;
		padding: 0;
		min-width: 100%;
		font-family: 'Open Sans', Arial;
		border-collapse: collapse;
		font-weight: 400;
		border: none;
		font-size: 14px;
		color: #444444;
		line-height: 22px;
	}

	table tbody tr td {
		border-collapse: collapse !important;
		border-spacing: 0px;
	}

	td ul {
		margin-top: 0px !important;
		margin-bottom: 0px !important;
	}

	.assetimage {
		width: 180px;
		margin-right: 20px;
		border: 1px solid #efefef;
		float: left;
	}
	.wordBreak{
		word-break: keep-all;
	}

	/*/ MOBILE STYLES */

	@media only screen and (max-width: 570px) {

		td {
			-webkit-text-size-adjust: none;
		}

		.header {
			height: 50px !important;
		}

		.logo {
			width: 95px !important;
			padding-top: 0px !important;
		}

		.btnCustom {
			display: block !important;
			line-height: 15px;
			/*padding: 12px 20px 12px 20px !important;*/
		}

		.btn-tableCustom {
			width: 100%;
		}

		.body-padding {
			padding: 26px 20px !important;
		}

		.w100 {
			width: 100% !important;
			border-left: none;
			text-align: center;
		}

		.Spacer {
			height: 12px;
		}

		.list-items {
			padding: 0px 0px 0px 19px !important;
		}

		.mob-title {
			padding-bottom: 18px !important;
		}

		.border-bottom {
			border-bottom: 1px solid #e6e6e6;
		}

		.social {
			border-left: none !important;
			border-right: none !important;
			border-bottom: 12px solid #f5f5f5;
			width: 100%;
			display: block;
		}

		.step-padding {
			padding: 26px 0px 26px 20px !important;
		}

		.wrapper {
			width: 368px;
			padding: 0px 12px;
		}

		.inner-img {
			width: 304px;
		}

		.markets {
			font-size: 9px !important;
			letter-spacing: 2px !important;
		}

		.global-footer {
			padding: 0px 0px 12px 0px !important;
		}

		.look-ahead {
			padding-top: 18px;
		}

		.cities {
			letter-spacing: 0px !important;
		}

		.align-center {
			text-align: center !important;
		}

		.col-stack {
			width: 100% !important;
			border: none !important;
		}

		.top-col {
			padding: 33px 0px 15px 0px !important;
		}

		.bot-col {
			padding: 15px 0px 33px 0px !important;
		}

		.sg50 {
			width: 100% !important;
		}

		.sg50 img {
			width: 40px;
			padding-top: 18px;
		}

		.assetimage {
			width: 100%;
			margin-bottom: 20px;
			border: 1px solid #efefef;
		}
	}

	@media only screen and (max-width: 330px) {
		.inner-img {
			width: 256px;
		}

		.wrapper {
			width: 320px;
		}
	}


</style>

<!-- / IE STYLES -->
<!--[if IE]>
<style type="text/css">
	.logo {
		padding-top: 0px;
	}

	@media only screen and (max-width: 570px) {
		.btn-block {
			width: 100%;
		}
	}
</style>
<![endif]-->
<!-- / OUTLOOK STYLES -->
<!--[if gte mso 9]>
<style type="text/css">
	.innerContent {
		margin: 0;
		padding: 0;
		font-family: arial;
		border-collapse: collapse;
		border: none;
		font-size: 14px;
		line-height: 22px;
	}

	.w550 {
		width: 550px !important;
	}

	.col-stack {
		width: 49%
	}

	.social {
		width: 32% !important;
		padding: 14px 0px;
	}

	.social img {
		padding-top: 5px;
	}

	.mso-social {
		background-color: #335782;
	}

	.contact {
		width: 24% !important;
	}

	td ul {
		margin-top: 0px !important;
		margin-bottom: 0px !important;
	}
</style>
<![endif]-->

<!-- / BODY -->
<div class="innerContent" style="background-color:#f5f5f5; width:100%;">
	<!--[if (gte mso 9)|(IE)]>
	<table align="center" class="wrapper" width="100%" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<table align="center" class="wrapper" width="550" bgcolor="#f5f5f5" border="0" cellpadding="0"
					   cellspacing="0">
					<tr>
						<td>
	<![endif]-->
	<!-- / WRAPPER -->

	<table align="center" class="wrapper" width="574" bgcolor="#f5f5f5" border="0" cellpadding="0" cellspacing="0"
		   style="font-size:14px; line-height:22px; padding: 0px 12px;margin: 0 auto">
		<tr>
			<td>
				<table width="100%" class="w550" align="center" cellpadding="0" cellspacing="0" border="0"
					   style="max-width:550px;margin: 0 auto">
					<!-- / HEADER START -->
					<tr>
						<td>
							<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td bgcolor="#f5f5f5" height="24" class="Spacer"></td>
								</tr>
								<tr>
									<td bgcolor="#f5f5f5" height="12"></td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- /  HEADER END -->

					<!-- / FULL WIDTH IMAGE BUTTON LAYOUT START -->
					<tr>
						<td>
							<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<!--  100% IMAGE MUST BE 1100PX WIDE NO LESS OR MORE //-->
									<td bgcolor="#ffffff" align="center" style="padding-top:30px;">
										<img
												src="<?=Docebo::getRootBaseUrl(true).'/themes/spt/images/company_logo.png'?>"
												border="0" alt="Notification" style="width: 200px"/>
									</td>
								</tr>
							</table>

							<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td bgcolor="#ffffff" class="body-padding"
										style="padding:30px 30px; border-bottom: 1px solid #e6e6e6;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td class="mob-title wordBreak"
													style="font-size: 14px; font-weight: 500; line-height: 29px; padding-bottom: 18px; letter-spacing: -0.1px;">
													<?=Yii::t('standard', 'Hello', array(), null, $shortLang)?> [first_name],
												</td>
											</tr>
											<tr>
												<td class="wordBreak" style="padding-bottom: 9px;">
													<?=Yii::t('notification', '<p>Dear [firstname] [lastname],</p><p>you have been registered to [lms_link] with the following credentials:</p><ul><li>Username: [username]</li><li>Password: [password]</li></ul><p>Welcome!</p>', array(), null, $shortLang)?>
												</td>
											</tr>
											<!-- / BUTTON START -->
											<tr>
												<td height="12"></td>
											</tr>
											<tr>
												<td>
													<table class="btn-tableCustom" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td class="btnCustom" bgcolor="#55AB54"
																style="-webkit-border-radius:2px; border-radius:2px;  display: inline-block; background-color:#55AB54;"
																align="center">
																<a href="[lms_link]" target="_blank"
																   style="display: inline-block;padding: 8px 20px;color: #ffffff; text-decoration: none;">
																	<?=Yii::t('standard', 'Log in now', array(), null, $shortLang)?></a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<!-- / BUTTON END -->
										</table>
									</td>
								</tr>
								<tr>
									<td height="24" bgcolor="#f5f5f5" class="Spacer"></td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- / FULL WIDTH IMAGE BUTTON LAYOUT END -->
				</table>
			</td>
		</tr>
	</table>
	<!-- / WRAPPER END  -->
	<!--[if (gte mso 9)|(IE)]>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<![endif]-->
</div>
