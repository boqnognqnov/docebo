<?php

class App7020NewQuestion extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$users = $this->eventParams['users'];
			$recipCollection = $this->getUsersDataFromArray($users);
			$questionId = $this->eventParams['questionId'];
			$questionObject = App7020Question::model()->findByPk($questionId);
			$userObject = CoreUser::model()->findByPk($questionObject->idUser);
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if ($useravatar['avatar']) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div>'
					. '<span style="
						display: inline-block;
						position: relative;
						vertical-align: middle;
						width: 100px;
						height: 100px!important;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						margin-right: 10px;">'
					. '<img style="
						width:100px;
						min-width:100px;
						height:100px;
						min-height:100px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avatarImg = '';
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<div>
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span></div>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<div class="widget-app7020Avatar-container">
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span></div>';
				}
			}

			$response->metadata['questionObject'] = $questionObject;
			$response->metadata['questionUrl'] = Docebo::createAbsoluteApp7020Url("askTheExpert/index") . '#/question/' . $questionId;
			$response->metadata['questionAuthor'] = CoreUser::getForamattedNames($questionObject->idUser, " ");
			$response->metadata['questionOwnerAvatar'] = $avatar;
			$response->metadata['questionOwnerAvatarImageUrl'] = $avatarImg;

			foreach ($recipCollection['users'] as $index => $recipient) {
				$recipCollection['users'][$index]['app7020_first_name'] = $recipCollection['users'][$index]['firstname'];
				$recipCollection['users'][$index]['app7020_email_users'] = $recipCollection['users'][$index]['email'];
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$questionObject = (is_array($metadata) && isset($metadata['questionObject']) ? $metadata['questionObject'] : false);
		if (empty($questionObject)) {
			$title = '';
		} else {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$title = $questionObject->title;
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$title = strip_tags($questionObject->title);
					break;
			}
		}

		$avatar = (is_array($metadata) && isset($metadata['questionOwnerAvatar']) ? $metadata['questionOwnerAvatar'] : '');
		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				//avatar is already HTML format, nothing else to do here
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				//these format cannot display images or HTML stuff, so just make it empty ...
				$avatar = '';
				break;
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_QUESTION_AUTHOR] = (is_array($metadata) && isset($metadata['questionAuthor']) ? $metadata['questionAuthor'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_URL] = (is_array($metadata) && isset($metadata['questionUrl']) ? $metadata['questionUrl'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_TITLE] = $title;
		$result[CoreNotification::SC_APP7020_QUESTION_OWNER_AVATAR] = $avatar;
		$result[CoreNotification::SC_APP7020_QUESTION_EMAIL_USERS] = $recipient['app7020_email_users'];
		$result[CoreNotification::SC_APP7020_QUESTION_RECIPIENT_FIRSTNAME] = $recipient['app7020_first_name'];

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {
			$language = $this->getUserLanguageCode($userModel['idst']);
			$users[$userModel['idst']] = array(
				'idst' => $userModel['idst'],
				'user' => $userModel['idst'],
				'username' => trim($userModel['userid'], '/'),
				'email' => $userModel['email'],
				'language' => $language,
				'firstname' => $userModel['firstname'],
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}



	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['questionAuthor']) ? $metadata['questionAuthor'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['questionOwnerAvatarImageUrl']) ? $metadata['questionOwnerAvatarImageUrl'] : '')
		);
	}



}
