<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 15-Jun-15
 * Time: 11:45 AM
 */
class CourseDeliverableSubmitted extends NotificationHandler implements INotificationHandler
{

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 * @param bool|string $params Optional parameters
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseId = $this->eventParams['courseId'];
			$courseModel = LearningCourse::model()->findByAttributes(array('idCourse' => $courseId));
			$deliverable = $this->eventParams['deliverableObject']; /* @var $deliverable LearningDeliverableObject */
			if(!$courseId || !$courseModel || !$deliverable)
				return $recipCollection;

			$targetUsers = $this->maskItems(self::MASK_USERS, array($deliverable->id_user));
			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			if (!empty($targetCourses) && !empty($targetUsers))
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					$recipCollection['users'][$index]['user'] = $deliverable->id_user;
					$recipCollection['users'][$index]['deliverableObject'] = $deliverable;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}
}