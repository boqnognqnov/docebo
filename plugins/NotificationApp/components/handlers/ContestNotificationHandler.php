<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 8.12.2015 г.
 * Time: 11:38
 */
class ContestNotificationHandler extends NotificationHandler {

    private function getAllUsers(){
        $users = array();


        $command = Yii::app()->db->createCommand();
        $command->select('idst')
            ->from(CoreUser::model()->tableName())
            ->where('userid <> "/Anonymous" AND valid = 1');

        $rows = $command->queryAll();

        foreach($rows as $user){
            $users[] = $user['idst'];
        }

        return $users;
    }

    protected function getUsersFromFilter(GamificationContest $contest){
        $targetUsers = $this->maskItems(self::MASK_USERS, false);
        $contestUsers = $contest->getUsersFromFilter();

        if(!empty($contestUsers)){
            if($this->notification->ufilter_option == CoreNotification::USER_NO_USER_FILTER){
                $target = $contestUsers;
            } else{
                $target = array_intersect($targetUsers, $contestUsers);
            }
            return $target;
        } else{
            if($this->notification->ufilter_option == CoreNotification::USER_NO_USER_FILTER && $contest->filter === 'all'){
                return $this->getAllUsers();
            } else if(!empty($targetUsers) && $contest->filter === 'all'){
                return $targetUsers;
            } else{
                return array();
            }
        }
    }

    protected function getRecipients($targetUsers = array()){
        $data = array('users' => array(), 'languages' => array());
        $countUsers = count($targetUsers);

        if($countUsers > 1000){
            $repeatNum = ceil($countUsers / 1000);

            for($i = 0; $i < $repeatNum; $i++){
                $offset = $i * 1000;
                $users = array_slice($targetUsers, $offset, 1000);
                switch($this->notification->recipient){
                    case CoreNotification::NOTIFY_RECIPIENTS_USER:
                        $tmpData = $this->helperGetUsersData($users);
                        $data['users'] = array_merge($data['users'], $tmpData['users']);
                        $data['languages'] = array_merge($data['languages'], $tmpData['languages']);
                        break;
                    case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
                        $tmpData = $this->helperGetPowerUsersOfUsersData($users);
                        $data['users'] = array_merge($data['users'], $tmpData['users']);
                        $data['languages'] = array_merge($data['languages'], $tmpData['languages']);
                        break;
                    case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
                        return $this->helperGetAllGodAdmins();
                        break;
                }
            }

            return $data;
        } else{
            switch($this->notification->recipient){
                case CoreNotification::NOTIFY_RECIPIENTS_USER:
                    return $this->helperGetUsersData($targetUsers);
                    break;
                case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
                    return $this->helperGetPowerUsersOfUsersData($targetUsers);
                    break;
                case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
                    return $this->helperGetAllGodAdmins();
                    break;
            }
        }
    }

    protected function buildShortCodes(&$usersList, GamificationContest $contest){
        if(!empty($usersList)){
            foreach ($usersList as &$user) {
                $user['contest_name'] = $contest->getName();
                $user['contest_description'] = $contest->getDescription();
                $contestGoal = $contest->goal;
                $goals = GamificationContest::getGoals();
                $user['contest_goal'] = $goals[$contestGoal];
                $user['contest_start_date'] = Yii::app()->localtime->toLocalDate($contest->from_date);
                $user['contest_end_date'] = Yii::app()->localtime->toLocalDate($contest->to_date);
                $user['contest_link'] = Docebo::createAbsoluteLmsUrl('//GamificationApp/GamificationApp/MyBadges', array(
                    'tab' => 'contests'
                ));
            }
        }
    }

    protected function getContestsBeforeAfter(){
        $intervalBeginEnd = $this->getTargetPeriodForShifted($this->notification->schedule_type, $this->notification->schedule_shift_number, $this->notification->schedule_shift_period);

        $beginPeriod = Yii::app()->localtime->roundToClosestHour($intervalBeginEnd[0]);
        $endPeriod = Yii::app()->localtime->roundToClosestHour($intervalBeginEnd[1]);

        $contests = array();

        $command = Yii::app()->db->createCommand();
        /**
         * @var $command CDbCommand
         */

        $command->select("id, from_date, to_date, timezone")
            ->from(GamificationContest::model()->tableName());
        $rows = $command->queryAll();
        
        if(!empty($rows)) {
            foreach($rows as &$row1) {              
                $row1['from_date'] = GamificationContest::convertFromContestTimezoneToUTC($row1['from_date'], $row1['timezone']);
                $row1['to_date'] = GamificationContest::convertFromContestTimezoneToUTC($row1['to_date'], $row1['timezone']);
            }
        } else {
            return $contests;
        }                
        
        if($this->notification->type == CoreNotification::NTYPE_GAMIFICATION_CONTEST_START){
            foreach($rows as $row){
                if(Yii::app()->localtime->isGte($row['from_date'], $beginPeriod) && Yii::app()->localtime->isLt($row['from_date'], $endPeriod)){
                    $contests[] = $row['id'];
                }
            }
        } elseif($this->notification->type == CoreNotification::NTYPE_GAMIFICATION_CONTEST_END){           
            foreach($rows as $row){
                if(Yii::app()->localtime->isGte($row['to_date'], $beginPeriod) && Yii::app()->localtime->isLt($row['to_date'], $endPeriod)){
                    $contests[] = $row['id'];
                }
            }
        }
                
        return $contests;
    }

    public function getShortcodesData($recipient, $metadata = array(), $textFormat = false)
    {
        $result =  parent::getShortcodesData($recipient, $metadata, $textFormat);
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_START_DATE] = $recipient['contest_start_date'];
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_NAME] = $recipient['contest_name'];
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_LINK] = $recipient['contest_link'];
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_END_DATE] = $recipient['contest_end_date'];
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_GOAL] = $recipient['contest_goal'];
        $result[CoreNotification::SC_GAMIFICATION_CONTEST_CONTEST_DESCRIPTION] = $recipient['contest_description'];

        return $result;
    }

}