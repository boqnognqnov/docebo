<?php

class App7020DeleteAsset extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$contentObject = $this->eventParams['content'];
			$dbCommand2 = Yii::app()->db->createCommand();
			$dbCommand2->select("cu.idst as adminId");
			$dbCommand2->from('core_user cu');
			$dbCommand2->join('core_group_members cgm', 'cu.idst=cgm.idstMember');
			$dbCommand2->join('core_group cg', 'cgm.idst=cg.idst');
			$dbCommand2->where("cg.groupid = '/framework/level/godadmin'");
			$users = $dbCommand2->queryColumn();
			$recipCollection = $this->getUsersDataFromArray($users);
			$userObject = CoreUser::model()->findByPk($contentObject->userId);

			$fullName = trim($userObject->firstname) . ' ' . trim($userObject->lastname);
			$userid = trim(ltrim($userObject->userid, '/'));

			$response->metadata['contentObject'] = $contentObject;
			$response->metadata['userObject'] = $userObject;
			$response->metadata['ownerContentName'] = (!empty($fullName) ? $fullName .' ('.$userid.')' : $userid);
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$contentObject = (is_array($metadata) && isset($metadata['contentObject']) ? $metadata['contentObject'] : false);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_CONTENT_AUTHOR] = (is_array($metadata) && isset($metadata['ownerContentName']) ? $metadata['ownerContentName'] : '');
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = (!empty($contentObject) ? $contentObject->title : '');

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userId) {
			$userModel = CoreUser::model()->findByPk($userId);
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] = array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}

}
