<?php
class App7020NewAssetToReview extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			// Get event params and validate them
			$users = App7020Assets::getAssetExperts($this->eventParams['contentId']);
			$recipCollection = $this->getUsersDataFromArray($users);
			$assetId = $this->eventParams['contentId'];
			$userAuthor = $this->eventParams['author'];
			$assetObject = App7020Content::model()->findByPk($assetId);
			$userObject = CoreUser::model()->findByPk($assetObject->userId);
			$assetThumb = App7020Assets::getAssetThumbnailUri($assetId);

			$useravatar = App7020Helpers::getUserAvatar($assetObject->userId);
			if ($useravatar['avatar']) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div class="widget-app7020Avatar-container">'
					. '<span style="
						width:42px;
						min-width:42px;
						height:42px;
						min-height:42px;
						display: inline-block;
						position: relative;
						vertical-align: top;" '
					. 'class="small avatar-container">'
					. '<img style="
						height: 42px;
						width: 42px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				$avatarImg = '';
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				}
			}

			$response->metadata['assetAuthor'] = CoreUser::getForamattedNames($userObject, ' ');
			$response->metadata['assetTitle'] = $assetObject->title;
			$response->metadata['assetThumb'] = $assetThumb;
			$response->metadata['assetUrl'] = Docebo::createApp7020AssetsPeerReviewNgrUrl($assetObject->id, true);;
			$response->metadata['userAvatar'] = $avatar;
			$response->metadata['userAvatarUrl'] = $avatarImg;

			//$experts = App7020ChannelExperts::getAllChannelExperts($assetId);
			foreach ($recipCollection['users'] as $index => $recipient) {
				$recipCollection['users'][$index]['app7020_asset_url'] = Docebo::createApp7020AssetsPeerReviewNgrUrl($assetObject->id, true);
				$recipCollection['users'][$index]['app7020_asset_author'] = $userObject->firstname." ".$userObject->lastname;
				$recipCollection['users'][$index]['first_name'] = $recipient['firstname'];
				$recipCollection['users'][$index]['app7020_user_email'] = $recipient['email'];
			}
		}
		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				//avatar is already in HTML format, so just use it as it comes
				$avatar = (is_array($metadata) && isset($metadata['userAvatar']) ? $metadata['userAvatar'] : '');
				$thumb = (is_array($metadata) && isset($metadata['assetThumb']) ? $metadata['assetThumb'] : '');
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				//these formats cannot display images
				$avatar = '';
				$thumb = '';
				break;
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_FIRSTNAME] = $recipient['first_name'];
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_EMAIL] = $recipient['app7020_user_email'];
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_AUTHOR] = (is_array($metadata) && isset($metadata['assetAuthor']) ? $metadata['assetAuthor'] : '');
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_TITLE] = (is_array($metadata) && isset($metadata['assetTitle']) ? $metadata['assetTitle'] : '');
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_THUMB] = $thumb;
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_URL] = (is_array($metadata) && isset($metadata['assetUrl']) ? $metadata['assetUrl'] : '');
		$result[CoreNotification::SC_APP7020_ASSET_TO_REVIEW_AVATAR] = $avatar;

		return $result;
	}


	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['assetAuthor']) ? $metadata['assetAuthor'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['userAvatarUrl']) ? $metadata['userAvatarUrl'] : ''),
			'thumb' => (is_array($metadata) && isset($metadata['assetThumb']) ? $metadata['assetThumb'] : '')
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel['idst']);
			$users[$userModel['idst']] =array(
				'idst' => $userModel['idst'],
				'user' => $userModel['idst'],
				'username' => trim($userModel['userid'], '/'),
				'email' => $userModel['email'],
				'language' => $language,
				'firstname' => $userModel['firstname'],
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
