<?php
/**
 * Created by PhpStorm.
 * User: asen
 * Date: 14-Sep-15
 * Time: 11:25 AM
 */

class AssignedCourseNotYetCompleted extends NotificationHandler implements INotificationHandler    {

	protected function getRelatedFields(){
		return array();
	}


	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 * @param array|bool $params Optional parameters
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 * @param bool|string $params Optional parameters
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// This handler is only for AFTER
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {
			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;
			$shiftType 		= $this->notification->schedule_type;

			$enrollments = $this->getUsersNotYetCompletedCourse($shiftType, $shiftNumber, $shiftPeriod);

			if(!empty($enrollments)){
			    
				$relatedFields = array('user', 'course', 'subscribed_at','expire_at');
				$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);

			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}
	
	
	/**
	 * Return list of enrollments where user is enrolled at a certain period of time and still haven't completed the course,  
	 * 
	 * @param string $shiftType
	 * @param integer $shiftNumber
	 * @param string $shiftPeriod
	 */
	public function getUsersNotYetCompletedCourse($shiftType, $shiftNumber, $shiftPeriod, $pivotUtc=false) {
	    
	    $enrollments = array();
	    $languages = array();
	    $select = array(
	        'cu.firstname',
	        'cu.lastname',
	        'cu.userid username',
	        'cu.idst',
	        'cu.idst user',
	        'cu.email',
	        'lc.name course_name',
	        'lc.idCourse course',
	        'csu.value language',
	        'lcu.date_inscr	subscribed_at',
	        'IF(lcu.date_expire_validity IS NOT NULL AND lcu.date_expire_validity <> :zeroTime, lcu.date_expire_validity,
	        	IF(lc.date_end IS NOT NULL AND lc.date_end <> :zeroTime, lc.date_end,"")
			) expire_at'
	        //			'CONVERT_TZ(CONCAT_WS(" ", icsDate.day, icsDate.time_begin), icsDate.timezone, "UTC") AS utcStartTimestamp',
	    );
	
	    //$intervalBeginAndEnd = $this->getTargetPeriodForShiftedWithoutTime($shiftType, $shiftNumber, $shiftPeriod);
	    
	    // Ask the "target period calculator" to select "WHOLE DAY" for DAY/WEEK periods, rather than exact 24-hour period (last true parameter)
	    $intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod, $pivotUtc, true);
	
	    $command = Yii::app()->getDb()->createCommand()
    	   ->select($select)
	       ->from(LearningCourseuser::model()->tableName() . ' lcu')
	       ->join(CoreUser::model()->tableName() . ' cu', 'lcu.idUser=cu.idst')
	       ->join(LearningCourse::model()->tableName() . ' lc', 'lcu.idCourse=lc.idCourse')
	       ->join(CoreSettingUser::model()->tableName() . ' csu', 'csu.id_user=cu.idst')
	       ->where('lcu.`date_inscr`>=:start
				AND lcu.`date_inscr`<=:end
				AND lcu.level =:learnerLevel
				AND lc.course_type = :type', array(
					    ':start' => $intervalBeginAndEnd[0],
					    ':end' => $intervalBeginAndEnd[1],
					    ':type' => LearningCourse::TYPE_ELEARNING,
					    ':learnerLevel' => LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT,
			))
			->andWhere(array('in', 'lcu.status', array(LearningCourseuser::$COURSE_USER_BEGIN, LearningCourseuser::$COURSE_USER_SUBSCRIBED)));
	    
	    $assocCourses = Yii::app()->db->createCommand()->select('idItem')
	       ->from(CoreNotificationAssoc::model()->tableName())
	       ->where('id = :id', array(':id'=>$this->notification->id))->queryColumn();
	    
	    if(!empty($assocCourses)){
	        $command->andWhere('lc.idCourse IN ('.implode(',',$assocCourses).')');
	    }
	    
	    $groups = Yii::app()->db->createCommand()->select('iditem')
	       ->from(CoreNotificationUserFilter::model()->tableName())
	       ->where('id=:id AND type=:type', array(
	           'id' => $this->notification->id,
	           'type' => CoreNotificationUserFilter::NOTIF_FILTER_GROUP))->queryColumn();
	       
	    $usersInGroups = array();
	    
	    if(!empty($groups)) {
	        $usersInGroups = Yii::app()->db->createCommand()
	        ->select('idstMember')
	        ->from(CoreGroupMembers::model()->tableName())
	        ->where('idst IN (' . implode(',', $groups) . ')')->queryColumn();
	    }
	
	    $branches = Yii::app()->db->createCommand()->select('idItem, selectionState')
	       ->from(CoreNotificationUserFilter::model()->tableName())
	       ->where('id=:id AND type=:type', array(
	           'id' => $this->notification->id,
	           'type' => CoreNotificationUserFilter::NOTIF_FILTER_BRANCH))->queryAll();

	    $usersInBranches = array();

	    if(!empty($branches)) {
	        foreach ($branches as $branch) {
	            $model = CoreOrgChartTree::model()->findByAttributes(
	                array('idOrg' => $branch['idItem'])
	            );

				if ($branch['selectionState'] == UsersSelector::ORGCHART_THIS_ONLY) { // oc_
					$usersInBranches = array_merge($usersInBranches, $model->getNodeUsers());
				} else if ($branch['selectionState'] == UsersSelector::ORGCHART_THIS_AND_DESC) { // ocd_
					$usersInBranches = array_merge($usersInBranches, $model->getBranchUsers());
				}
	        }
	    }
	    
	    $users = array_unique(array_merge($usersInBranches, $usersInGroups));
	    
	    if(!empty($users)){
	        $command->andWhere('cu.idst IN (' . implode(',', $users) . ')');
	    }
	    
	    $command->group('lcu.idUser, lcu.idCourse');
	    $enrollments = $command->queryAll(true, array(
			':zeroTime' => '0000-00-00 00:00:00'
		));
	
	    foreach ($enrollments as $enrollment) {
	        $languages[$enrollment['language']] = $enrollment['language'];
	    }
	
	    return array(
	        'enrollments' => $enrollments,
	        'languages' => array_keys($languages),
	    );
	}
	
	
}