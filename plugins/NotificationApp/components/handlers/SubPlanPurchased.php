<?php



/**
 * Subscription plan purchase notification
 * !IMPORTANT! This notification is raised in Hydra
 */
class SubPlanPurchased extends NotificationHandler implements INotificationHandler {
	
	public function getRecipientsData($params = false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users' => array(), 'languages' => array());
		
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			// Shortcodes
			$response->metadata['purchase_date'] = $this->eventParams['purchase_date'];
			$response->metadata['transaction_code'] = $this->eventParams['transaction_code'];
			$response->metadata['expiration_date'] = $this->eventParams['expiration_date'];
			$response->metadata['bundle_name'] = $this->eventParams['bundle_name'];
			$response->metadata['plan_name'] = $this->eventParams['plan_name'];
			$response->metadata['type'] = $this->eventParams['type'];
			$response->metadata['capacity'] = $this->eventParams['capacity'];
			$response->metadata['price'] = $this->eventParams['price'];
			$response->metadata['note'] = $this->eventParams['note'];
			$response->metadata['sold_to'] = $this->eventParams['sold_to'];
			
			$users = $this->eventParams['user_ids'];
			$userIds = [];
			
			switch ($this->notification["recipient"]) {
				case "user": 
					$userIds = array_unique(array_merge_recursive($users['user_ids'], $users['admin_ids']));
					break;
				
				case "poweruser":
					$userIds = array_unique(array_merge_recursive($users['pu_ids'], $users['admin_ids']));
					break;
				
				case "godadmin":
					$userIds = $users['admin_ids'];
					break;
			}
			
			$userModel = new CoreUser();
			$usersToSend = $userModel->findAllByAttributes(['idst' => $userIds]);
			
			$recipCollection = $this->getUsersDataFromArray($usersToSend);
			
			$response->recipients = $recipCollection['users'];
			$response->languages = $recipCollection['languages'];

			return $response;
		}
	}
	
	
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE] = $metadata['purchase_date'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE] = $metadata['transaction_code'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE] = $metadata['expiration_date'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME] = $metadata['bundle_name'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_TYPE] = $metadata['type'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_CAPACITY] = $metadata['capacity'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PRICE] = $metadata['price'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_NOTE] = $metadata['note'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PLAN_NAME] = $metadata['plan_name'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_SOLD_TO] = $metadata['sold_to'];

		return $result;
	}
	
	
	
	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
	
	
	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userId) {
			$language = $this->getUserLanguageCode($userId['idst']);
			$users[$userId['idst']] = array(
				'idst' => $userId['idst'],
				'user' => $userId['idst'],
				'username' => trim($userId['userid'], '/'),
				'email' => $userId['email'],
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}
	
	
	
	
}
