<?php
/**
 * 
 * 
 *
 */
class NewUserCreated extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			if (!$userModel)
				return $recipCollection;
			
			$targetUsers 	= array($userModel->idst);
			$targetCourses 	= false;
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY, false, false, false, array(), false, false);
			
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['user'] = $userModel->idst;

					// Send the plain password to the user only!!
					if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) {
						$recipCollection['users'][$index]['passwordPlainText'] = $userModel->passwordPlainText;
					}else{
                        $recipCollection['users'][$index]['passwordPlainText'] = '*****';
                    }
				}
			}
		}
		
		// "AFTER" ?
		else  if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {

			// ALL users/courses are candidates (flase, false)
			$targetUsers 	= false;	// all users are legit
			
			// AFTER INFO
			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType	 = $this->notification->schedule_type;
			
			// Get list of users (and their personal data) having the "subscribed" event in the given period (some time ago)
			$usersHavingEvent = $this->getUsersWithEventInPeriod($targetUsers, self::USER_EVENT_REGISTERED_TO_LMS, $shiftType, $shiftNumber, $shiftPeriod);
			
			// Specific related data
			$relatedFields = array('user', 'register_date');
			$recipCollection = $this->helperGetRecipientsForListOfUsers($usersHavingEvent, $this->notification->recipient, $relatedFields);

			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					if(trim($recipient['email'])) {
						if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) {
							$code = md5(mt_rand() . mt_rand());
							// Checking if the code generated already exists in core_pwd_recover
							$corePwdRecoverModel = CorePwdRecover::model()->findByPk($recipient['idst']);

							if ($corePwdRecoverModel === NULL) {
								$corePwdRecoverModel = new CorePwdRecover();
								$corePwdRecoverModel->idst_user = $recipient['idst'];
								$corePwdRecoverModel->random_code = $code;
								$corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
								$corePwdRecoverModel->save();
							} else {
								$corePwdRecoverModel->random_code = $code;
								$corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
								$corePwdRecoverModel->update();
							}
							$link = Yii::app()->createAbsoluteUrl('user/recoverPwd', array('code' => $code));
							$recipCollection['users'][$index]['passwordPlainText'] = $link;
						} else {
							$recipCollection['users'][$index]['passwordPlainText'] = '*****';
						}
					}
				}
			}
		}
		
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
