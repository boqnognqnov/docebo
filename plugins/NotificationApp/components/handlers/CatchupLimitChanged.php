<?php

class CatchupLimitChanged extends NotificationHandler implements INotificationHandler    {

    public function getRecipientsData($params=false) {

        $response = new NotificationHandlerResponse();
        $response->recipients       = array();
        $response->languages        = array();
        $recipCollection            = array('users'=> array(), 'languages'=> array());

        $learningPlanModel 	= isset($this->eventParams['lpModel']) ? $this->eventParams['lpModel'] : null;
        if (!$learningPlanModel)
            return $recipCollection;

        $userModel 	= isset($this->eventParams['userModel']) ? $this->eventParams['userModel'] : null;
        if (!$userModel)
            return $recipCollection;

        $catchupLimit = $this->eventParams['catchupLimit'];

        $targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
        $targetPlans 	= $this->maskItems(self::MASK_PLANS, array($learningPlanModel->id_path));

        // No plan or plans ! :(
        if (empty($targetPlans)) {
            return $recipCollection; // is empty
        }

        $recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers);

        // ADD RELATED INFO
        if (!empty($recipCollection['users'])) {
            foreach ($recipCollection['users'] as $index => $recipient) {
                $recipCollection['users'][$index]['catchup_plan_name'] 			= $learningPlanModel->path_name;
                $recipCollection['users'][$index]['catchup_limit'] 			    = $catchupLimit;
                $recipCollection['users'][$index]['user'] 			            = $userModel->idst;
            }
        }

        $response->recipients 	= $recipCollection['users'];
        $response->languages 	= $recipCollection['languages'];

        return $response;
    }

    public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
       $result =  parent::getShortcodesData($recipient, $metadata, $textFormat);
                            /********- Set custom ['shortcodes'] -********/
            $result[CoreNotification::SC_LEARNINGPLAN_NAME]	= $recipient['catchup_plan_name'];
			$replace[CoreNotification::SC_LEARNINGPLAN_URL]	= Docebo::createAbsoluteLmsUrl('curricula/show');
            $result[CoreNotification::SC_CATCHUP_LIMIT]	= $recipient['catchup_limit'];

        return $result;
    }

	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}