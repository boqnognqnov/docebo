<?php



class UserUnsubscribed extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$userModel 		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			if (!$courseModel || !$userModel) {
				return $recipCollection;
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			if (!empty($targetCourses) && !empty($targetUsers)) {

				// Change targetCourses to false (i.e. no filter by enrollment), because at this point, the enrollment is already deleted (probably)
				// and if we filter by the course, the result is always empty, which makes no sense to us: we need USER info.
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, false, self::PU_OF_USERSONLY, false, false, false, array(), false, false);

				switch($this->notification->recipient){
					case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
						$instructorLevels = array(
							LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
							LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
							LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
							LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
						);
						$recipCollection = $this->helperGetUsersData(false, $targetCourses, $instructorLevels);
						break;

					case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:

				//power users need to be filtered by owned courses too
				if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
					//enlist the IDs of extracted PUs
					$puserIds = (is_array($recipCollection) && isset($recipCollection['users'])
						? array_keys($recipCollection['users'])
						: array());
					if (!empty($puserIds)) {
						//retrieve PUs IDs owning target courses
						$coursesPuserIds = Yii::app()->getDb()->createCommand()
							->select('puser_id')
							->from(CoreUserPuCourse::model()->tableName())
							->where(array('IN', 'course_id', $targetCourses))
							->queryColumn();
						if (!is_array($coursesPuserIds)) {
							$coursesPuserIds = array();
						}
						//detect which of them own target users AND target courses
						$filteredPuserIds = array_intersect($puserIds, $coursesPuserIds);
						//detect which PUs owns only an user or only a course (but noth both) and remove them from the recipients
						$toBeRemoved = array_diff($puserIds, $filteredPuserIds);
						if (!empty($toBeRemoved)) {
							foreach ($toBeRemoved as $id) {
								if (isset($recipCollection['users'][$id])) {
									$recipCollection['users'][$id] = null;
									unset($recipCollection['users'][$id]);
								}
							}
						}
					}
					break;
				}
				}

				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] 		= $courseModel->idCourse;
						$recipCollection['users'][$index]['user'] 			= $userModel->idst;
					}
				}
			}

		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
	}



	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
