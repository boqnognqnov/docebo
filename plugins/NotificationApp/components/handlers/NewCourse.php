<?php
/**
 * 
 * 
 *
 */
class NewCourse extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			
			if (!$courseModel){
				if(isset($this->eventParams['id_course'])) {
					$courseModel = LearningCourse::model()->findByPk($this->eventParams['id_course']);
				}
				else
					return $recipCollection;
			}

			// Possible users: any
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			// Target courses: none, doesn't matter
			$targetCourses 	= false;
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);
				
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
				}
			}
			
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
