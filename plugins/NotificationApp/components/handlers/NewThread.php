<?php


class NewThread extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$userModel		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$threadModel	= isset($this->eventParams['thread']) ? $this->eventParams['thread'] : null;
			
			if (!$courseModel || !$userModel || !$threadModel) {
				return array();
			}
		
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true);
			
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['thread'] 	= $threadModel->idThread;
					$recipCollection['users'][$index]['course'] 	= $courseModel->idCourse;
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}



	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


}
