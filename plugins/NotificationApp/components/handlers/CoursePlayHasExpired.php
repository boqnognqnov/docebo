<?php



class CoursePlayHasExpired extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		
		// Restrict running for allowed only (in this case it is after and before the event)
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {
			
			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;
			$shiftType 		= $this->notification->schedule_type;
			
			// ALL users/courses are candidates (false, false), because we are going to observe all enrollments,
			// Of course masked/filtered by assigned users & courses.
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);
			
			// Get all enrollments related to target users/courses, feltered according to the after/before settings. 
			$enrollments = $this->getEnrollmentsWithEventAfterBeforePeriod($targetUsers, $targetCourses, self::ENROLL_EVENT_COURSE_EXPIRED, $shiftType, $shiftNumber, $shiftPeriod);
			// This notification has something to add to the basic recipients information (taken out from enrollment data)
			$relatedFields = array('user', 'course', 'expire_at');
			
			// Collect an array of recipients; basically an array of items which will then be transformed into an email.
			// With that said, the number of items in this array is equal to the number of emails going to be sent by the parent method.
			$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
