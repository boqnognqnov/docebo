<?php


class DigestCourseHasExpired extends NotificationHandler implements INotificationHandler
{


    public function getRecipientsData($params = false)
    {
        $response = new NotificationHandlerResponse();
        $response->recipients = array();
        $response->languages = array();
        $recipCollection = array('users' => array(), 'languages' => array());

        // Restrict running for allowed only (in this case it is after and before the event)
        if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_ONLY_EVERY)) {

            $shiftNumber = $this->notification->schedule_shift_number;
            $shiftPeriod = $this->notification->schedule_shift_period;
            $shiftType = $this->notification->schedule_type;
            // ALL users/courses are candidates (false, false), because we are going to observe all enrollments,
            // Of course masked/filtered by assigned users & courses.
            $targetUsers = $this->maskItems(self::MASK_USERS, false);
            $targetCourses = $this->maskItems(self::MASK_COURSES, false);

            // Get all enrollments related to target users/courses, feltered according to the after/before settings.
            $enrollments = $this->getEnrollmentsWithEventAfterBeforePeriod($targetUsers, $targetCourses, self::ENROLL_EVENT_COURSE_EXPIRED, $shiftType, $shiftNumber, $shiftPeriod, true);
            // This notification has something to add to the basic recipients information (taken out from enrollment data)
            $relatedFields = array('user', 'course', 'expire_at');

            // Collect an array of recipients; basically an array of items which will then be transformed into an email.
            // With that said, the number of items in this array is equal to the number of emails going to be sent by the parent method.


            //god admin get metadata
            if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_GODADMIN) {
                $recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments,
                    CoreNotification::NOTIFY_RECIPIENTS_USER, self::PU_OF_USERSONLY, $relatedFields);
                $recipientList = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient,
                    self::PU_OF_USERSONLY, $relatedFields);
                //flag for which notification template to send
                $multiple = true;

                //power user metadata
            }else if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
                $recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, CoreNotification::NOTIFY_RECIPIENTS_USER, self::PU_OF_USERSONLY, $relatedFields);
                $recipientList = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
                //flag for which notification template to send
                $multiple = true;
            }
            //instructor metadata
            else if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER) {
                $recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, CoreNotification::NOTIFY_RECIPIENTS_USER, self::PU_OF_USERSONLY, $relatedFields);
                $recipientList = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
                //flag for which notification template to send
                $multiple = true;
            }
            //else is for users metadata
            else {
                $recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments,
                    $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
                //flag for which notification template to send
                $multiple = false;
            }

        }
        $items = array();
        $itemsTemp = array();
        $adminsRec = array();
        //check the flag which template and structure to build
        if ($multiple) {
            foreach ($recipientList['users'] as $recipient) {
                // Get the User and Course model
                $userModel = CoreUser::model()->findByPk($recipient['user']);
                $courseModel = LearningCourse::model()->findByPk($recipient['course']);
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['course_name'] = $courseModel->name;
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['course_link'] = Docebo::createAbsoluteLmsUrl('player',
                    array('course_id' => $courseModel->idCourse));
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['subscribed_at'] = $recipient['subscribed_at'];
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['expire_at'] = $recipient['expire_at'];
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['idst'] = $userModel->idst;
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['first_name'] = $userModel->firstname;
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['last_name'] = $userModel->lastname;
                $itemsTemp[$recipient['idst']][$recipient['user']]['courses'][$courseModel->idCourse]['username'] = $userModel->userid;
                $itemsTemp[$recipient['idst']][$recipient['user']]['admin_idst'] = $recipient['idst'];
                $itemsTemp[$recipient['idst']][$recipient['user']]['admin_username'] = $recipient['username'];
                $itemsTemp[$recipient['idst']][$recipient['user']]['admin_email'] = $recipient['email'];
                $itemsTemp[$recipient['idst']][$recipient['user']]['admin_language'] = $recipient['language'];
            }
            foreach ($itemsTemp as $key => $value) {
                foreach ($value as $d=>$data) {
                    $items[$key][] = $data;
                    $userModel = CoreUser::model()->findByPk($data['admin_idst']);

                    $adminsRec[$key.$d]['idst'] = $data['admin_idst'];
                    $adminsRec[$key.$d]['username'] = $data['admin_username'];
                    $adminsRec[$key.$d]['email'] = $data['admin_email'];
                    $adminsRec[$key.$d]['language'] = $data['admin_language'];
                    $adminsRec[$key.$d]['first_name'] = $userModel->firstname;
                    $adminsRec[$key.$d]['last_name'] = $userModel->lastname;
                }
            }
        } else {
            foreach ($recipCollection['users'] as $recipient) {
                // Get the User and Course model
                $userModel = CoreUser::model()->findByPk($recipient['user']);
                $courseModel = LearningCourse::model()->findByPk($recipient['course']);
                $items[$recipient['user']]['courses'][$courseModel->idCourse]['course_name'] = $courseModel->name;
                $items[$recipient['user']]['courses'][$courseModel->idCourse]['course_link'] = Docebo::createAbsoluteLmsUrl('player',
                    array('course_id' => $courseModel->idCourse));
                $items[$recipient['user']]['courses'][$courseModel->idCourse]['subscribed_at'] = $recipient['subscribed_at'];
                $items[$recipient['user']]['courses'][$courseModel->idCourse]['expire_at'] = $recipient['expire_at'];
                $items[$recipient['user']]['idst'] = $userModel->idst;
                $items[$recipient['user']]['first_name'] = $userModel->firstname;
                $items[$recipient['user']]['last_name'] = $userModel->lastname;
                $items[$recipient['user']]['username'] = $userModel->userid;
                $items[$recipient['user']]['admin_idst'] = $recipient['idst'];
                $items[$recipient['user']]['admin_username'] = $recipient['username'];
                $items[$recipient['user']]['admin_email'] = $recipient['email'];
                $items[$recipient['user']]['admin_language'] = $recipient['language'];
            }
            foreach ($items as $key => $value) {
                    $userModel = CoreUser::model()->findByPk($key);

                    $adminsRec[$key]['idst'] = $value['admin_idst'];
                    $adminsRec[$key]['username'] = $value['admin_username'];
                    $adminsRec[$key]['email'] = $value['admin_email'];
                    $adminsRec[$key]['language'] = $value['admin_language'];
                    $adminsRec[$key]['first_name'] = $userModel->firstname;
                    $adminsRec[$key]['last_name'] = $userModel->lastname;
            }
        }
        $response->metadata['items'] = $items;
        $response->metadata['languages'] = $recipCollection['languages'];
        $response->recipients =$adminsRec;
        $response->languages = $recipCollection['languages'];
        return $response;

    }

    /**
     *
     * @param string $params
     * @return NotificationDescriptor
     */
    public static function descriptor($params = false)
    {
        $descriptor = new NotificationDescriptor();
        return $descriptor;
    }


}
