<?php
class UpdateCoachingSession extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - Users assigned to the session (if any)
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$idCourse = $this->eventParams['idCourse'];
			$idSession = $this->eventParams['idSession'];
			$courseModel = LearningCourse::model()->findByPk($idCourse);
			$sessionModel = LearningCourseCoachingSession::model()->findByPk($idSession);
			if(!$idCourse || !$courseModel || !$idSession || !$sessionModel)
				return $recipCollection;

			// Run filtering on users (we restrict users only to those assigned to the session)
			$usersFilter = false;
			if(in_array($this->notification->recipient, array(CoreNotification::NOTIFY_RECIPIENTS_USER, CoreNotification::NOTIFY_RECIPIENTS_TEACHER))) {
				foreach($sessionModel->sessionUsers as $sessionUser)
					$usersFilter[] = $sessionUser->idUser;
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, $usersFilter);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			// Get recipients data
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);

			// Add related info to each recipient
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					$recipCollection['users'][$index]['coaching_session'] = $sessionModel;
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
