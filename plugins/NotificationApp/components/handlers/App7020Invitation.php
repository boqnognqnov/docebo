<?php

class App7020Invitation extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			// Get event params and validate them
			$users = $this->eventParams['users'];
			$recipCollection = $this->getUsersDataFromArray($users);
			$contentId = $this->eventParams['contentId'];
			$inviter = $this->eventParams['idInviter'];
			$contentObject = App7020Assets::model()->findByPk($contentId);
			$useravatar = App7020Helpers::getUserAvatar($this->eventParams['idInviter']);
			if (is_array($useravatar) && isset($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div class="widget-app7020Avatar-container">'
					. '<span style="width: 42px;
						height: 42px;
						display: inline-block;
						position: relative;
						vertical-align: top;" '
					. 'class="small avatar-container">'
					. '<img style="
						width:42px;
						min-width:42px;
						height:42px;
						min-height:42px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				$avatarImg = '';
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				}
			}

			$response->metadata['inviterAvatar'] = $avatar;
			$response->metadata['inviterAvatarUrl'] = $avatarImg;
			$response->metadata['inviterName'] = CoreUser::getForamattedNames($inviter, ' ');

			$response->metadata['content'] = $contentObject;
			$response->metadata['contentUrl'] = Docebo::createApp7020AssetsViewNgrUrl($contentId, true);
			$response->metadata['contentTitle'] = $contentObject->title;
			$response->metadata['contentAuthor'] = CoreUser::getForamattedNames($contentObject->userId, ' ');
			$response->metadata['contentThumbUrl'] = App7020Assets::getAssetThumbnailUri($contentId);

			foreach ($recipCollection['users'] as $index => $recipient) {
				$recipCollection['users'][$index]['first_name'] = CoreUser::getFirstNameById($index);
				$recipCollection['users'][$index]['app7020_recipient_email'] = $recipient['email'];
			}
		}
		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];
		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				//set HTML image to be displayed in thumb
				$thumb = '<img src="' . $metadata['contentThumbUrl'] . '"/>';
				//avatar image is already provided in HTML format, no further operations to be done here
				$avatar = $metadata['inviterAvatar'];
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				// these formats are unable to display images
				$thumb = '';
				$avatar = '';
				break;
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_FIRSTNAME] = $recipient['first_name'];
		$result[CoreNotification::SC_APP7020_CONTENT_RECIPIENT_EMAIL] = $recipient['app7020_recipient_email'];
		$result[CoreNotification::SC_APP7020_CONTENT_AUTHOR] = $metadata['contentAuthor'];
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = $metadata['contentTitle'];
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = $metadata['contentUrl'];
		$result[CoreNotification::SC_APP7020_CONTENT_THUMB] = $thumb;
		$result[CoreNotification::SC_APP7020_CONTENT_INVITER] = $metadata['inviterName'];
		$result[CoreNotification::SC_APP7020_CONTENT_INVITER_AVATAR] = $avatar;

		return $result;
	}



	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['inviterName']) ? $metadata['inviterName'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['inviterAvatarUrl']) ? $metadata['inviterAvatarUrl'] : ''),
			'thumb' => (is_array($metadata) && isset($metadata['contentThumbUrl']) ? $metadata['contentThumbUrl'] : '')
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $value) {
			$userModel = CoreUser::model()->findByPk($value);

			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] = array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}

}
