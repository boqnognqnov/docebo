<?php

/* @var $learningPlanModel LearningCoursepath */
/* @var $certificateModel LearningCertificateAssignCp */

class StudentCompletedLearningPlan extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$learningPlanModel  = isset($this->eventParams['learningPlan']) ? $this->eventParams['learningPlan'] : null;
			$userModel 			= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$certificateModel	= isset($this->eventParams['certificate']) ? $this->eventParams['certificate'] : null;

			if (!$learningPlanModel || !$userModel) {
				if(!$learningPlanModel)
					Yii::log('Wrong learning plan specified', CLogger::LEVEL_ERROR);
				if(!$userModel)
					Yii::log('Wrong user specified', CLogger::LEVEL_ERROR);
				return $recipCollection; // none
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetPlans	= $this->maskItems(self::MASK_PLANS, array($learningPlanModel->id_path));

			// No target plan ? get out!
			if (empty($targetPlans)) {
				return $recipCollection; // empty
			}

			// Get courses from the plan
			$planCourses = $learningPlanModel->getCoursesIdList();
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $planCourses, self::PU_OF_USERSONLY);

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['plan'] = $learningPlanModel->id_path;
					$recipCollection['users'][$index]['user'] = $userModel->idst;
					$recipCollection['users'][$index]['completed_at'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');

					if ($certificateModel) {
						$recipCollection['users'][$index]['id_certificate'] = $certificateModel->id_certificate;
					}
				}
			}

		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;

	}


	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
