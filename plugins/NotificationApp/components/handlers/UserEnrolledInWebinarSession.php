<?php

class UserEnrolledInWebinarSession extends NotificationHandler implements INotificationHandler {


	public function getRecipientsData( $params = FALSE ) {

		$response             = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection      = array( 'users' => array(), 'languages' => array() );

		// ONLY "AT"
		if ( $this->notification->schedule_type == CoreNotification::SCHEDULE_AT ) {

			$session_id   = isset( $this->eventParams['session_id'] ) ? $this->eventParams['session_id'] : NULL;

			$sessionModel = WebinarSession::model()->findByPk($session_id);
			$response->metadata['sessionModel'] = $sessionModel;

			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : NULL;
			$user_id = $this->eventParams['user'];
			$user = CoreUser::model()->findByPk($user_id);

			if ( $user instanceof CoreUser ) {
				$user_id = $user->idst;
			} elseif ( is_numeric( $user ) && (int)$user > 0 ) {
				$user_id = (int)$user;
			} else {
				// Invalid user
				return $response;
			}

			if (!$sessionModel ) {
				return $response;
			}

			$targetUsers   = $this->maskItems( self::MASK_USERS, (array) $user_id );
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $sessionModel->course_id ) );

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$recipCollection = $this->helperGetAllGodAdmins();
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					// Now get INSTRUCTORS (data) of all these courses
					$studentsLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT
					);

					$studentsIdsts = array();
					$cmd = Yii::app()->db->createCommand()
						->select("cus.id_user")
						->from(WebinarSessionUser::model()->tableName()." cus")
						->join(WebinarSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
						->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
						->where(array( 'IN', 'cs.course_id', $targetCourses ))
						->andWhere(array( 'IN', 'cus.id_user', $targetUsers ))
						->andWhere(array( 'IN', 'cu.level', $studentsLevels ))
						->andWhere("cus.id_session = :id_session", array(':id_session' => $sessionModel->id_session));
					$reader = $cmd->query();

					if ($reader) {
						while ($row = $reader->read()) {
							$studentsIdsts[] = $row['id_user'];
						}
					}

					$recipCollection = $this->helperGetUsersData($studentsIdsts);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
					// Get the PUs/managers of the users for which we raised this event/notification
					$puserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPU::model()->tableName())
						->where(array('IN', 'user_id', $targetUsers))
						->queryColumn();
					// Make sure that the notification is being sent to PUs owning the course
					$coursesPuserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPuCourse::model()->tableName())
						->where(array('IN', 'course_id', $targetCourses))
						->queryColumn();
					if (!is_array($coursesPuserIds)) { $coursesPuserIds = array(); }
					if (!is_array($puserIds)) { $puserIds = array(); }
					$puserIds = array_intersect($puserIds, $coursesPuserIds);
					// Get recipients details
					$recipCollection = $this->helperGetUsersData($puserIds, false, false);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					// Now get INSTRUCTORS (data) of all these courses
					$instructorLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);

					$instructorsIdsts = array();
					$cmd = Yii::app()->db->createCommand()
						->select("cus.id_user")
						->from(WebinarSessionUser::model()->tableName()." cus")
						->join(WebinarSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
						->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
						->where(array( 'IN', 'cs.course_id', $targetCourses ))
						->andWhere(array( 'IN', 'cu.level', $instructorLevels ))
						->andWhere("cus.id_session = :id_session", array(':id_session' => $sessionModel->id_session));
					$reader = $cmd->query();

					if ($reader) {
						while ($row = $reader->read()) {
							$instructorsIdsts[] = $row['id_user'];
						}
					}

					$recipCollection = $this->helperGetUsersData($instructorsIdsts);
					break;
			}

			$sessionDates = $sessionModel->getDates();
			$response->metadata['sessionDates'] = $sessionDates;
			$ics = new ICSManager( ICSManager::TYPE_REQUEST, $sessionModel->date_begin, $sessionModel->date_end, $sessionModel->name, '', NULL, $this->notification->from_name, $this->notification->from_email, $sessionModel );

			// ADD RELATED INFO
			if ( ! empty( $recipCollection['users'] ) ) {
				foreach ( $recipCollection['users'] as $index => $recipient ) {
					$recipCollection['users'][$index]['course'] = $sessionModel->course_id;
					$recipCollection['users'][$index]['id_session'] = $sessionModel->id_session;
					$recipCollection['users'][$index]['icsManager'] = $ics;
					$recipCollection['users'][$index]['user'] = $user_id;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages  = $recipCollection['languages'];

		return $response;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$sessionModel = $metadata['sessionModel'];
		$sessionDates = $metadata['sessionDates'];
		list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_SESSION_DATES] = $dateInfo;
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = $sessionModel->getToolName(true);
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = $details;

		return $result;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();

		return $descriptor;
	}
}
