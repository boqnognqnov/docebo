<?php

class WebinarSessionDeleted extends NotificationHandler implements INotificationHandler
{

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param bool|string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();

		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type = CoreNotification::SCHEDULE_AT) {
			$sessionId = $this->eventParams['session_data']['session_id'];
			$name = $this->eventParams['session_data']['name'];
			$description = $this->eventParams['session_data']['other_info'];
			$datebegin = $this->eventParams['session_data']['date_begin'];
			$dateEnd = $this->eventParams['session_data']['date_end'];

			$usersList = $this->eventParams['session_data']['users_list'];

			$courseId = $this->eventParams['course_id'];
			$courseModel = LearningCourse::model()->findByPk($courseId);
			$sessionModel = $this->eventParams['sessionModel'];

			$response->metadata['courseModel'] = $courseModel;
			$response->metadata['sessionModel'] = $sessionModel;

			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseId));
			$targetUsers = $this->maskItems(self::MASK_USERS, false);

			// Prepare Session start and end dates
			$webinar_start_date = str_replace(array("T", "+0000"), array(" ", ""), $datebegin);
			$webinar_end_date = str_replace(array("T", "+0000"), array(" ", ""), $dateEnd);

			$sessionDates = $this->eventParams['dates'];
			$response->metadata['sessionDates'] = $sessionDates;

			if (($targetCourses !== false && in_array($courseId, $targetCourses)) || $targetCourses === false) {

				if ($targetUsers === false) {
					$targetUsers = $usersList;
				}

				$enrollments = $this->getWebinarEnrolledRecipientsData($this->notification->recipient, $targetUsers, $targetCourses);

				foreach ($enrollments['users'] as $index => $recipient) {
					$dateBegin = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_start_date);
					$dateEnd = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_end_date);

					$ics = new ICSManager(
						ICSManager::TYPE_REQUEST,
						$dateBegin,
						$dateEnd,
						$sessionModel->name,
						$sessionModel->description,
						null,
						$this->notification->from_name,
						$this->notification->from_email,
						$sessionModel);

					//list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates);

					$enrollments['users'][$index]['course'] = $courseModel->idCourse;
					//$enrollments['users'][$index]['course_name'] = $courseModel->name;//$this->eventParams['session_data'];
					//$enrollments['users'][$index]['session_name'] = $name;
					//$enrollments['users'][$index]['session_dates'] = $dateInfo;
					//$enrollments['users'][$index]['webinar_tool'] = $this->eventParams['session_data']['toolName'];
					$enrollments['users'][$index]['icsManager'] = $ics;
				}

				$response->recipients = $enrollments['users'];
				$response->languages = $enrollments['languages'];

				return $response;
			}
		}
	}

	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$courseModel = (is_array($metadata) && isset($metadata['courseModel']) ? $metadata['courseModel'] : null);
		$sessionModel = (is_array($metadata) && isset($metadata['sessionModel']) ? $metadata['sessionModel'] : null);
		$sessionDates = (is_array($metadata) && isset($metadata['sessionDates']) ? $metadata['sessionDates'] : array());

		list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$result[CoreNotification::SC_COURSENAME] = (!empty($courseModel) ? $courseModel->name : '');
		$result[CoreNotification::SC_SESSION_DATES] = $dateInfo;
		$result[CoreNotification::SC_SESSION_NAME] = (!empty($sessionModel) ? $sessionModel->name : '');
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = (!empty($sessionModel) ? $sessionModel->getToolName(true) : '');

		return $result;
	}

}