<?php



class UserWaitingSubscribe extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			$sessionId = isset($this->eventParams['sessionId']) ? $this->eventParams['sessionId'] : null;

			if (!empty($targetCourses) && !empty($targetUsers)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_BOTH);

				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] 		= $courseModel->idCourse;
						$recipCollection['users'][$index]['user'] 			= $userModel->idst;
						$recipCollection['users'][$index]['subscribed_at'] 	= Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
						$recipCollection['users'][$index]['id_session'] 	= $sessionId;
					}
				}
			}

		}

		// "AFTER" ?
		else  if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {

			// ALL users/courses are candidates (flase, false)
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);

			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType	 = $this->notification->schedule_type;

			$enrollments = $this->getEnrollmentsWithEventAfterBeforePeriod($targetUsers, $targetCourses, self::ENROLL_EVENT_SUBSCRIBED_WAITING, $shiftType, $shiftNumber, $shiftPeriod);
			$relatedFields = array('user', 'course', 'subscribed_at');
			$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_BOTH, $relatedFields);
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;



	}

	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
