<?php
class App7020BestAnswer extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			// Get event params and validate them
			$answerId = $this->eventParams['answerId'];
			$answerObject = App7020Answer::model()->findByPk($answerId);
			$questionId = $answerObject->idQuestion;
			$questionObject = App7020Question::model()->findByPk($questionId);
			$users = array(CoreUser::model()->findByPk($answerObject->idUser));
			$ownerQuestion = CoreUser::model()->findByPk($questionObject->idUser);
			$recipCollection = $this->getUsersDataFromArray($users);

			$ownerQuestionName = (!empty($ownerQuestion) ? trim(trim($ownerQuestion->firstname).' '.trim($ownerQuestion->lastname)) : '');
			if (empty($ownerQuestionName)) {
				//fallback to username
				$ownerQuestionName = (!empty($ownerQuestion) ? ltrim($ownerQuestion->userid, '/') : '');
			};

			$response->metadata['questionUrl'] = Docebo::createAbsoluteApp7020Url("askTheExpert/index") . '#/question/' . $questionId;
			$response->metadata['ownerQuestion'] = $ownerQuestion;
			$response->metadata['ownerQuestionName'] = $ownerQuestionName;
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_QUESTION_AUTHOR] = (is_array($metadata) && isset($metadata['ownerQuestionName']) ? $metadata['ownerQuestionName'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_URL] = (is_array($metadata) && isset($metadata['questionUrl']) ? $metadata['questionUrl'] : '');

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] =array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}



	public function getSpecialProperties($recipient, $metadata = array()) {

		$authorName = (is_array($metadata) && isset($metadata['ownerQuestionName']) ? $metadata['ownerQuestionName'] : false);
		$userObject = (is_array($metadata) && isset($metadata['ownerQuestion']) ? $metadata['ownerQuestion'] : false);
		if (!empty($userObject)) {
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if (is_array($useravatar) && isset($useravatar['avatar']) && !empty($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
			} else {
				$avatarImg = '';
			}
		} else {
			$avatarImg = '';
		}

		return array(
			'author' => $authorName,
			'avatar' => $avatarImg
		);
	}


}
