<?php
class App7020NewContentsQuestion extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them		
			$questionId = $this->eventParams['questionId'];
			$questionObject = App7020Question::model()->findByPk($questionId);
			if(!$questionObject->idContent){
				return $response;
			}
			$users = App7020Assets::getAssetExperts($questionObject->idContent);

			$recipCollection = $this->getUsersDataFromArray($users);
			$contentObject = App7020Assets::model()->findByPk($questionObject->idContent);
			$questionOwner = CoreUser::model()->findByPk($questionObject->idUser);

			$response->metadata['questionUrl'] = Docebo::createAbsoluteApp7020Url("askTheExpert/index",array('#'=>'/question/'.$questionId));
			$response->metadata['questionOwner'] = $questionOwner;
			$response->metadata['questionOwnerName'] = CoreUser::getForamattedNames($questionOwner, ' ');
			$response->metadata['questionObject'] = $questionObject;
			$response->metadata['contentUrl'] = Docebo::createApp7020AssetsViewNgrUrl($contentObject->id, true);
			$response->metadata['contentObject'] = $contentObject;
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		$contentObject = (is_array($metadata) && isset($metadata['contentObject']) ? $metadata['contentObject'] : false);
		$questionObject = (is_array($metadata) && isset($metadata['questionObject']) ? $metadata['questionObject'] : false);

		if (!empty($questionObject)) {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$questionText = $questionObject->title;
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$questionText = strip_tags($questionObject->title);
					break;
			}
		} else {
			$questionText = '';
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_QUESTION_AUTHOR] = (is_array($metadata) && isset($metadata['questionOwnerName']) ? $metadata['questionOwnerName'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_URL] = (is_array($metadata) && isset($metadata['questionUrl']) ? $metadata['questionUrl'] : '');
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = (!empty($contentObject) ? $contentObject->title : '');
		$result[CoreNotification::SC_APP7020_QUESTION_TITLE] = $questionText;
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = (is_array($metadata) && isset($metadata['contentUrl']) ? $metadata['contentUrl'] : '');

		return $result;
	}



	public function getSpecialProperties($recipient, $metadata = array()) {

		$userObject = (is_array($metadata) && isset($metadata['questionOwner']) ? $metadata['questionOwner'] : false);
		if (!empty($userObject)) {
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if (is_array($useravatar) && isset($useravatar['avatar']) && !empty($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
			} else {
				$avatarImg = '';
			}
		} else {
			$avatarImg = '';
		}

		return array(
			'author' => (is_array($metadata) && isset($metadata['questionOwnerName']) ? $metadata['questionOwnerName'] : ''),
			'avatar' => $avatarImg
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $user){
			$language = $this->getUserLanguageCode($user["idst"]);
			$users[$user["idst"]] =array(
				'idst' => $user["idst"],
				'user' => $user["idst"],
				'username' => trim($user["userid"], '/'),
				'email' => $user["email"],
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
