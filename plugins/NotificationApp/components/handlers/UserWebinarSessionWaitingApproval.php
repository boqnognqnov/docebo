<?php

class UserWebinarSessionWaitingApproval extends NotificationHandler implements INotificationHandler {


	public function getRecipientsData( $params = FALSE ) {

		$response             = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection      = array( 'users' => array(), 'languages' => array() );

		// ONLY "AT"
		if ( $this->notification->schedule_type == CoreNotification::SCHEDULE_AT ) {

			$session_id = isset( $this->eventParams['session_id'] ) ? $this->eventParams['session_id'] : NULL;
			$user_id       = isset( $this->eventParams['user'] ) ? $this->eventParams['user'] : FALSE;
			$sessionModel = WebinarSession::model()->findByPk( $session_id );
			if (!$sessionModel)
				return $response;

			$targetUsers   = $this->maskItems( self::MASK_USERS, array( $user_id ) );
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $sessionModel->course_id ) );

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$recipCollection = $this->helperGetAllGodAdmins();
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$studentsIdsts = Yii::app()->getDb()->createCommand()
						->select('idst')
						->from(CoreUser::model()->tableName())
						->where(array('IN', 'idst', $targetUsers))
						->queryColumn();
					$levels = array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
					$recipCollection = $this->helperGetUsersData($studentsIdsts, false, $levels);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
					// Get the PUs/managers of the users for which we raised this event/notification
					$puserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPU::model()->tableName())
						->where(array('IN', 'user_id', $targetUsers))
						->queryColumn();
					// Make sure that the notification is being sent to PUs owning the course
					$coursesPuserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPuCourse::model()->tableName())
						->where(array('IN', 'course_id', $targetCourses))
						->queryColumn();
					if (!is_array($coursesPuserIds)) { $coursesPuserIds = array(); }
					if (!is_array($puserIds)) { $puserIds = array(); }
					$puserIds = array_intersect($puserIds, $coursesPuserIds);

					if($this->notification->puProfileId)
					{
						$usersInGroup = Yii::app()->getDb()->createCommand()
							->select('idstMember')
							->from(CoreGroupMembers::model()->tableName())
							->where(array('IN', 'idstMember', $puserIds))
							->andWhere('idst = :groupId', array('groupId' => $this->notification->puProfileId))
							->queryColumn();
						if($usersInGroup) {
							$puserIds = $usersInGroup;
						} else {
							$puserIds = array();
						}
					}

					// Get recipients details
					$recipCollection = $this->helperGetUsersData($puserIds, false, false, false, false, false);
					break;

				// MANAGERS of the users
				case CoreNotification::NOTIFY_RECIPIENTS_MANAGER:
					// Get recipients details
					$recipCollection = $this->helperGetManagersOfUsersData($targetUsers, $targetCourses);
					break;

				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					// Now get INSTRUCTORS (data) of all these courses
					$instructorLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);

					$instructorsIdsts = array();
					$cmd = Yii::app()->db->createCommand()
						->select("cus.id_user")
						->from(WebinarSessionUser::model()->tableName()." cus")
						->join(WebinarSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
						->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
						->where(array( 'IN', 'cs.course_id', $targetCourses ))
						->andWhere(array( 'IN', 'cu.level', $instructorLevels ))
						->andWhere("cus.id_session = :id_session", array(':id_session' => $sessionModel->id_session));
					$reader = $cmd->query();

					if ($reader) {
						while ($row = $reader->read()) {
							$instructorsIdsts[] = $row['id_user'];
						}
					}

					$recipCollection = $this->helperGetUsersData($instructorsIdsts);
					break;
			}

			$sessionDates = $sessionModel->getDates();
			// Prepare Session start and end dates
			$webinar_start_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getStartDate($session_id)));
			$webinar_end_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getEndDate($session_id)));

			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$dateBegin = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_start_date);
					$dateEnd = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_end_date);

					$ics = new ICSManager(
						ICSManager::TYPE_REQUEST,
						$dateBegin,
						$dateEnd,
						$sessionModel->name,
						$sessionModel->description,
						null,
						$this->notification->from_name,
						$this->notification->from_email,
						$sessionModel);

					$recipCollection['users'][ $index ]['user'] = $user_id;
					$recipCollection['users'][ $index ]['course']     = $sessionModel->course_id;
					$recipCollection['users'][ $index ]['id_session'] = $sessionModel->id_session;
					$recipCollection['users'][ $index ]['icsManager'] = $ics;

					$recipCollection['users'][ $index ]['webinar_tool'] = $sessionModel->getToolName(true);

					list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates);

					$recipCollection['users'][$index]['webinar_session_details'] = $details;
					$recipCollection['users'][$index]['session_dates'] = $dateInfo;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages  = $recipCollection['languages'];

		return $response;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		$result = parent::getShortcodesData($recipient);
		$result[CoreNotification::SC_SESSION_DATES] = $recipient['session_dates'];
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = $recipient['webinar_tool'];
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = $recipient['webinar_session_details'];
		return $result;
	}

	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();

		return $descriptor;
	}
}