<?php
class EcommerceSettingsChanged extends NotificationHandler implements INotificationHandler    {
	/**
	 * Recipients can be "godadmins"
	 * @param bool $params
	 * @return NotificationHandlerResponse
	 */
	public function getRecipientsData($params=false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// Restrict running for allowed only (in this case it is after and before the event)
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_ONLY_AT)) {
			$recipCollection = $this->helperGetAllGodAdmins();
			$authorId = $this->eventParams['user'];
			foreach ($recipCollection['users'] as &$recipient) {
				$recipient['user'] = $authorId;
				$recipient['ecommerceShortCodes'] = true;
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
