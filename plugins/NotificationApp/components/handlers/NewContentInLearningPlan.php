<?php
class NewContentInLearningPlan extends NotificationHandler implements INotificationHandler {

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData( $params = FALSE ) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$learningPlanModel  = isset($this->eventParams['learningPlan']) ? $this->eventParams['learningPlan'] : null;

			if (!$learningPlanModel) {
				return $recipCollection; // none
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetPlans	= $this->maskItems(self::MASK_PLANS, array($learningPlanModel->id_path));

			// No target plan ? get out!
			if (empty($targetPlans)) {
				return $recipCollection; // empty
			}

			// Get courses from the plan
			$planCourses = $learningPlanModel->getCoursesIdList();
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $planCourses, self::PU_OF_USERSONLY);

			//build learning plan users filter: only users effectively enrolled to the LP must receive the notification
			$lpUsers = array();
			$utcNow = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
			$reader = Yii::app()->db->createCommand()
				->select("*")
				->from(LearningCoursepathUser::model()->tableName())
				->where("id_path = :id_path")
				->query(array(':id_path' => $learningPlanModel->getPrimaryKey()));
			while ($record = $reader->read()) {
				$valid = true;
				if (!empty($record['date_begin_validity']) && $record['date_begin_validity'] != '0000-00-00 00:00:00') {
					$valid = strcmp($utcNow, $record['date_begin_validity']) >= 0; // NOTE: ISO dates can be compared just like strings or numbers
				}
				if (!$valid) { continue; }
				if (!empty($record['date_end_validity']) && $record['date_end_validity'] != '0000-00-00 00:00:00') {
					$valid = strcmp($utcNow, $record['date_end_validity']) <= 0; // NOTE: ISO dates can be compared just like strings or numbers
				}
				if ($valid) {
					$lpUsers[] = $record['idUser'];
				}
			}

			// FILTER EFFECTIVE RECIPIENTS AND ADD RELATED INFO
			$finalRecipients = array();
			if (!empty($lpUsers) && !empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					if (in_array($recipCollection['users'][$index]['idst'], $lpUsers)) {
						$idstIndex = $recipCollection['users'][$index]['idst'];
						$finalRecipient = $recipCollection['users'][$index];
						$finalRecipient['plan'] = $learningPlanModel->id_path;
						$finalRecipient['completed_at'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
						$finalRecipients[$idstIndex] = $finalRecipient; // the $idstIndex will avoid duplicates in recipients list
					}
				}
			}

		} elseif ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER){
			Yii::log('"After" notification not yet implemented for New Content in Learning Plan notification', CLogger::LEVEL_ERROR);
		}

		$response->recipients 	= array_values($finalRecipients);
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}}