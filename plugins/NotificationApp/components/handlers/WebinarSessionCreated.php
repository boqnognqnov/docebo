<?php

class WebinarSessionCreated extends NotificationHandler implements INotificationHandler
{

	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();

		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type = CoreNotification::SCHEDULE_AT) {

			$sessionModel = $this->eventParams['session'];
			$sessionModel->refresh();
			$response->metadata['sessionModel'] = $sessionModel;

			/**
			 * @var $sessionModel WebinarSession
			 */
			$courseId = $sessionModel->course_id;
			$id_session = $sessionModel->id_session;

			$targetCourses = $this->maskItems(self::MASK_COURSES, array(
				$sessionModel->course_id
			));

			if (($targetCourses !== false && in_array($courseId, $targetCourses)) || $targetCourses === false) {
				$courseModel = LearningCourse::model()->findByPk($courseId);
				$response->metadata['courseModel'] = $courseModel;

				// Prepare Session start and end dates
				$webinar_start_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getStartDate($id_session)));
				$webinar_end_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getEndDate($id_session)));


				$sessionDates = $sessionModel->getDates();
				$response->metadata['sessionDates'] = $sessionDates;

				$recipCollection = $this->getPUusersAndGodAdmins($targetCourses, $this->notification->recipient);

				foreach ($recipCollection['users'] as $index => $recipient) {
					$dateBegin = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_start_date);
					$dateEnd = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_end_date);

					$ics = new ICSManager(
						ICSManager::TYPE_REQUEST,
						$dateBegin,
						$dateEnd,
						$sessionModel->name,
						$sessionModel->description,
						null,
						$this->notification->from_name,
						$this->notification->from_email,
						$sessionModel
					);

					//list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates);

					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					//$recipCollection['users'][$index]['course_name'] = $courseModel->name;
					//$recipCollection['users'][$index]['session_name'] = $sessionModel->name;
					//$recipCollection['users'][$index]['session_dates'] = $dateInfo;
					//$recipCollection['users'][$index]['webinar_tool'] = $sessionModel->getToolName(true);
					//$recipCollection['users'][$index]['webinar_session_details'] = $details;
					$recipCollection['users'][$index]['icsManager'] = $ics;
					$recipCollection['users'][$index]['id_session'] = $id_session;
				}

				$response->recipients = $recipCollection['users'];
				$response->languages = $recipCollection['languages'];

				return $response;
			}
		}

	}

	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false)
	{
		$courseModel = (is_array($metadata) && isset($metadata['courseModel']) ? $metadata['courseModel'] : null);
		$sessionModel = (is_array($metadata) && isset($metadata['sessionModel']) ? $metadata['sessionModel'] : null);
		$sessionDates = (is_array($metadata) && isset($metadata['sessionDates']) ? $metadata['sessionDates'] : array());

		list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$result[CoreNotification::SC_COURSENAME] = (!empty($courseModel) ? $courseModel->name : '');
		$result[CoreNotification::SC_SESSION_DATES] = $dateInfo;
		$result[CoreNotification::SC_SESSION_NAME] = (!empty($sessionModel) ? $sessionModel->name : '');
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = (!empty($sessionModel) ? $sessionModel->getToolName(true) : '');
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = $details;
		return $result;
	}

	public function allowedScheduleTypes()
	{
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}


	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}