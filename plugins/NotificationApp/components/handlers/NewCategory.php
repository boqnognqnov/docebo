<?php
/**
 * 
 * 
 *
 */
class NewCategory extends NotificationHandler implements INotificationHandler    {

	/* @var $categoryModel LearningCourseCategory */

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$categoryModel 	= isset($this->eventParams['category']) ? $this->eventParams['category'] : null;
			
			if (!$categoryModel)
				return $recipCollection;
			
			// Possible users: any; apply selected users filter mask
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
				
			// Target courses: none, doesn't matter
			$targetCourses 	= false;
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);
				
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['category'] = $categoryModel->idCategory;
				}
			}
					
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
