<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 8.12.2015 г.
 * Time: 10:24
 */
class GamificationContestEnd extends ContestNotificationHandler implements INotificationHandler
{

    /**
     * Return a descriptor calss, describing all properties of the Notificatio
     *
     * @param array|bool $params Optional parameters
     *
     * @return NotificationDescriptor
     */
    public static function descriptor($params = false)
    {
        $descriptor = new NotificationDescriptor();
        return $descriptor;
    }

    /**
     * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
     *
     * @param bool|string $params Optional parameters
     *
     * @return array
     */
    public function getRecipientsData($params = false)
    {
        $response = new NotificationHandlerResponse();
        $response->recipients = array();
        $response->languages = array();
        $recipCollection = array('users' => array(), 'languages' => array());

        if($this->notification->schedule_type == CoreNotification::SCHEDULE_AT){
            $idContest = $this->eventParams['id_contest'];
            $contest = GamificationContest::model()->findByPk($idContest);

            /**
             * @var $contest GamificationContest
             */
            $targetUsers = $this->getUsersFromFilter($contest);
            $recipCollection = $this->getRecipients($targetUsers);
            $this->buildShortCodes($recipCollection['users'], $contest);

            $response->recipients = $recipCollection['users'];
            $response->languages = $recipCollection['languages'];

        } else {
            $contestIds = $this->getContestsBeforeAfter();                      

            if(!empty($contestIds)){
                foreach($contestIds as $contestId){
                    $contest = GamificationContest::model()->findByPk($contestId);

                    /**
                     * @var $contest GamificationContest
                     */

                    $contestUsers = $this->getUsersFromFilter($contest);
                    $tmpRecipCollection = $this->getRecipients($contestUsers);
                    $this->buildShortCodes($tmpRecipCollection['users'], $contest);

                    $recipCollection['users'] = array_merge($recipCollection['users'], $tmpRecipCollection['users']);
                    $recipCollection['languages'] = array_merge($recipCollection['languages'], $tmpRecipCollection['languages']);
                }
            }
            $response->recipients = $recipCollection['users'];
            $response->languages = $recipCollection['languages'];
        }

        return $response;
    }
}