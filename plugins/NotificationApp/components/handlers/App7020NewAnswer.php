<?php

class App7020NewAnswer extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();
		$recipCollection = array('users' => array(), 'languages' => array());
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			// Get event params and validate them
			$questionId = $this->eventParams['questionId'];
			$questionObject = App7020Question::model()->findByPk($questionId);
			$lastAnswer = App7020Question::getLastAnswerByQuestionId($questionId);
			$users = array(CoreUser::model()->findByPk($questionObject->idUser));
			$ownerAnswer = $this->eventParams['ownerAnswer'];
			$recipCollection = $this->getUsersDataFromArray($users);
			$useravatar = App7020Helpers::getUserAvatar($ownerAnswer->idst);
			if ($useravatar['avatar']) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div class="widget-app7020Avatar-container">'
					. '<span style="
						display: inline-block;
						position: relative;
						vertical-align: top;" '
					. 'class="small avatar-container">'
					. '<img style="
						width:42px;
						min-width:42px;
						height:42px;
						min-height:42px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				$avatarImg = '';
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				}
			}

			$answerOwnerName = (!empty($ownerAnswer) ? trim(trim($ownerAnswer->firstname) . ' ' . trim($ownerAnswer->lastname)) : '');
			if (empty($answerOwnerName)) {
				$answerOwnerName = ltrim($ownerAnswer->userid, '/');
			}

			$response->metadata['lastAnswer'] = $lastAnswer;
			$response->metadata['questionOwnerName'] = CoreUser::getForamattedNames($questionObject->idUser, ' ');
			$response->metadata['questionUrl'] = Docebo::createAbsoluteApp7020Url("askTheExpert/index") . '#/question/' . $questionId;
			$response->metadata['answerOwner'] = $ownerAnswer;
			$response->metadata['answerOwnerName'] = $answerOwnerName;
			$response->metadata['answerOwnerAvatar'] = $avatar;
			$response->metadata['answerOwnerAvatarImageUrl'] = $avatarImg;

			foreach ($recipCollection['users'] as $index => $recipient) {
				$recipCollection['users'][$index]['app7020_question_owner_email'] = $recipCollection['users'][$index]['email'];
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		$lastAnswer = (is_array($metadata) && isset($metadata['lastAnswer']) ? $metadata['lastAnswer'] : '');
		if (empty($lastAnswer)) {
			$answerText = '';
		} else {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$answerText = $lastAnswer['content'];
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$answerText = strip_tags($lastAnswer['content']);
					break;
			}
		}

		$answerOwnerAvatar = (is_array($metadata) && isset($metadata['answerOwnerAvatar']) ? $metadata['answerOwnerAvatar'] : '');
		if (!empty($answerOwnerAvatar)) {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					// the avatar is already in HTML format and it is good for this format
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					// the avatar image simply cannot be displayed in these formats
					$answerOwnerAvatar = '';
					break;
			}
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_QUESTION_URL] = (is_array($metadata) && isset($metadata['questionUrl']) ? $metadata['questionUrl'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_AUTHOR] = (is_array($metadata) && isset($metadata['questionOwnerName']) ? $metadata['questionOwnerName'] : '');
		$result[CoreNotification::SC_APP7020_QUESTION_OWNER_EMAIL] = $recipient['app7020_question_owner_email'];
		$result[CoreNotification::SC_APP7020_ANSWER_TITLE] = $answerText;
		$result[CoreNotification::SC_APP7020_ANSWER_AUTHOR] = (is_array($metadata) && isset($metadata['answerOwnerName']) ? $metadata['answerOwnerName'] : '');
		$result[CoreNotification::SC_APP7020_ANSWER_OWNER_AVATAR] = $answerOwnerAvatar;

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] = array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}



	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['answerOwnerName']) ? $metadata['answerOwnerName'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['answerOwnerAvatarImageUrl']) ? $metadata['answerOwnerAvatarImageUrl'] : '')
		);
	}


}
