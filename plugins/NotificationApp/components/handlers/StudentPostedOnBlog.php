<?php

class StudentPostedOnBlog extends NotificationHandler implements INotificationHandler
{

	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$userModel = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$postModel = isset($this->eventParams['post']) ? $this->eventParams['post'] : null;

			if (!$courseModel || !$userModel || !$postModel) {
				return array();
			}

			$targetUsers = $this->maskItems(self::MASK_USERS, false);
			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			// Get related recipients
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true);
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					// Remove POST OWNER if is among the recipients
					if ($recipient['idst'] == $postModel->id_user) {
						unset($recipCollection['users'][$index]);
						continue;
					}
					$recipCollection['users'][$index]['post'] = $postModel->id_post;
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					// Remove the post owner
				}
			}
		}
		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
