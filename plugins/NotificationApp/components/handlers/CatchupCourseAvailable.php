<?php

class CatchupCourseAvailable extends NotificationHandler implements INotificationHandler    {

    public function getRecipientsData($params=false) {
        $response = new NotificationHandlerResponse();
        $response->recipients       = array();
        $response->languages        = array();
        $recipCollection            = array('users'=> array(), 'languages'=> array());

        $learningPlanModel 	= isset($this->eventParams['learningPlan']) ? $this->eventParams['learningPlan'] : null;
		$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
		$idCatchup = isset($this->eventParams['id_catchup']) ? $this->eventParams['id_catchup'] : null;
		$idMainCourse = isset($this->eventParams['id_main_course']) ? $this->eventParams['id_main_course'] : null;

        if (!$learningPlanModel || !$userModel || !$idCatchup || !$idMainCourse)
            return $recipCollection;

		$catchupCourse = LearningCourse::model()->findByPk($idCatchup);
		$mainCourse = LearningCourse::model()->findByPk($idMainCourse);
        if (!$catchupCourse || !$mainCourse)
            return $recipCollection;

        $targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
        $targetPlans 	= $this->maskItems(self::MASK_PLANS, array($learningPlanModel->id_path));

        // No plan or plans ! :(
        if (empty($targetPlans)) {
            return $recipCollection; // is empty
        }

        $recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers);

        // ADD RELATED INFO
        if (!empty($recipCollection['users'])) {
            foreach ($recipCollection['users'] as $index => $recipient) {
                $recipCollection['users'][$index]['plan'] 		= $learningPlanModel->id_path;
				$recipCollection['users'][$index]['course']		= $catchupCourse->idCourse;
                $recipCollection['users'][$index]['user'] 	    = $userModel->idst;
            }
        }

        $response->recipients 	= $recipCollection['users'];
        $response->languages 	= $recipCollection['languages'];
        return $response;
    }

	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}