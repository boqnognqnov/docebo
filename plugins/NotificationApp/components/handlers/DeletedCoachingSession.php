<?php
class DeletedCoachingSession extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - Users assigned to the session (if any)
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$sessionModel = $this->eventParams['sessionModel']; /* @var $sessionModel LearningCourseCoachingSession */
			if(!$sessionModel)
				return $recipCollection;

			// Run filtering on users (we restrict users only to those assigned to the session)
			$usersFilter = false;
			if(!empty($this->eventParams['sessionUsers']) && in_array($this->notification->recipient, array(CoreNotification::NOTIFY_RECIPIENTS_USER, CoreNotification::NOTIFY_RECIPIENTS_TEACHER)))
				$usersFilter = $this->eventParams['sessionUsers'];

			$targetUsers 	= $this->maskItems(self::MASK_USERS, $usersFilter);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($sessionModel->idCourse));

			// Get recipients data
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);

			// Add related info to each recipient
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $sessionModel->idCourse;
					$recipCollection['users'][$index]['coaching_session'] = $sessionModel;
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
