<?php

class App7020Publishing extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$contentId = $this->eventParams['contentId'];
			$idPublisher = $this->eventParams['idPublisher'];
			$contentObject = App7020Assets::model()->findByPk($contentId);
			$publisher = CoreUser::model()->findByPk($idPublisher);
			$users = App7020ContentReview::getAttendedExperts($contentId);

			$isContributorExpert = false;
			foreach ($users as $key => $value) {
				if($value->idst == $contentObject->userId){
					$isContributorExpert = true;
				}
				if($value->idst == $publisher->idst){
					unset($users[$key]);
				}
			}
			if(!$isContributorExpert){
				$users[] = CoreUser::model()->findByPk($contentObject->userId);
			}

			$recipCollection = $this->getUsersDataFromArray($users);

			$response->metadata['contentUrl'] = Docebo::createApp7020AssetsViewNgrUrl($contentObject->id, true);
			$response->metadata['contentTitle'] = $contentObject->title;
			$response->metadata['publisher'] = $publisher;
			$response->metadata['publisherName'] = CoreUser::getForamattedNames($publisher, ' ');
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_PUBLISHER] = $metadata['publisherName'];
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = $metadata['contentTitle'];
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = $metadata['contentUrl'];

		return $result;
	}



	public function getSpecialProperties($recipient, $metadata = array()) {

		$authorName = (is_array($metadata) && isset($metadata['publisherName']) ? $metadata['publisherName'] : false);
		$userObject = (is_array($metadata) && isset($metadata['publisher']) ? $metadata['publisher'] : false);
		if (!empty($userObject)) {
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if (is_array($useravatar) && isset($useravatar['avatar']) && !empty($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
			} else {
				$avatarImg = '';
			}
		} else {
			$avatarImg = '';
		}

		return array(
			'author' => $authorName,
			'avatar' => $avatarImg
		);
	}




	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] =array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
