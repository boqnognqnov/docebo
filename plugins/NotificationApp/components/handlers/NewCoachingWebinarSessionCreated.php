<?php
class NewCoachingWebinarSessionCreated extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {
		$response               = new NotificationHandlerResponse();
		$response->recipients   = array();
		$response->languages    = array();
		$recipCollection        = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$courseModel            = $this->eventParams['course'];
			$idCourse               = $this->eventParams['idCourse'];
			$idUser                 = $this->eventParams['idUser'];
			$idCoach                = $this->eventParams['idCoach'];
			$idSession              = $this->eventParams['idSession'];
			$webinarModel           = $this->eventParams['webinarModel'];

			// Get the User model
			$userModel              = CoreUser::model()->findByPk($idUser);

			// Get the Coach model
			$coachModel             = CoreUser::model()->findByPk($idCoach);

			// Get the Coaching Session model
			$coachingSessionModel   = LearningCourseCoachingSession::model()->findByPk($idSession);

			// Ensure, that there is a course model to be user in further notifiaction processing
			if(empty($courseModel->idCourse)) {
				$courseModel = LearningCourse::model()->findByPk($idCourse);
			}

			// If there is something missing
			if(!$courseModel || !$idUser || !$userModel || !$coachingSessionModel || !$coachModel)
				return $recipCollection;

			// Prepare Session start and end dates
			$start_date         = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($coachingSessionModel->datetime_start));
			$end_date           = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($coachingSessionModel->datetime_end));

			// Prepare Session start and end dates
			$webinar_start_date         = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($webinarModel->date_begin));
			$webinar_end_date           = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($webinarModel->date_end));

			// Run filtering on users
			$targetUsers 	    = $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	    = $this->maskItems(self::MASK_COURSES, array((int) $idCourse));

			// Get recipients data
			$recipCollection    = $this->helperGetRecipientsFromUsersData(
				$this->notification->recipient,
				$targetUsers,
				$targetCourses,
				self::PU_OF_USERSONLY,
				false,
				false,
				false,
				array()
			);

			$sessionUsers = array();
			if (!empty($idSession)) {
				// Get assigned to the coaching webinar session users
				$getSessionUsers = Yii::app()->db->createCommand('SELECT id_user FROM '.LearningCourseCoachingWebinarSessionUser::model()->tableName() . ' WHERE id_session='.$webinarModel->id_session)->queryAll();

				// Prepare array with currently assigned users to the coaching webinar sesssion !!!
				if (is_array($getSessionUsers) && count($getSessionUsers) > 0) {
					foreach ($getSessionUsers as $itemUserId) {
						$sessionUsers[] = $itemUserId['id_user'];
					}
				}
			}

            $webinar = WebinarTool::getById($webinarModel->tool);
            $data = $webinar->getAttendeeInfoForRoom($webinarModel->id_tool_account, CJSON::decode($webinarModel->tool_params));
			$details = $this->formatWebinarSessionDetails($data);

			// Add related info to each recipient
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					// Filter only recipients of type users
					if ((in_array($recipient['idst'], $sessionUsers) && $this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) || ($this->notification->recipient != CoreNotification::NOTIFY_RECIPIENTS_USER)) {
						$recipCollection['users'][$index]['course']                             = $courseModel->idCourse;
						$recipCollection['users'][$index]['course_name']                        = $courseModel->name;
						$recipCollection['users'][$index]['course_link']                        = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $courseModel->idCourse));
						$recipCollection['users'][$index]['coach_name']                         = $coachModel->getFullName();
						$recipCollection['users'][$index]['coach_email']                        = $coachModel->email;
						$recipCollection['users'][$index]['coaching_session_dates']             = $start_date . ' - '. $end_date;
						$recipCollection['users'][$index]['coaching_webinar_date_begin']        = $webinar_start_date;
						$recipCollection['users'][$index]['coaching_webinar_date_end']          = $webinar_end_date;
						$recipCollection['users'][$index]['coaching_webinar_name']              = $webinarModel->name;
						$recipCollection['users'][$index]['coaching_webinar_url']               = $webinarModel->getWebinarUrl($recipient['idst']);
                        $recipCollection['users'][$index]['coaching_webinar_meeting_details']   = $details;
					} else {
						unset($recipCollection['users'][$index]);
					}
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 * getShortcodesData() - sets the custom short codes
	 * @param array $recipient - array with recipients data
	 * @return array - array with shortcodes and values to be replaced
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result =  parent::getShortcodesData($recipient, $metadata, $textFormat);

		/********- Set custom ['shortcodes'] -********/
		$result[CoreNotification::SC_COURSENAME]	            = $recipient['course_name'];
		$result[CoreNotification::SC_COURSE_LINK]	            = $recipient['course_link'];
		$result[CoreNotification::SC_COACH_NAME]	            = $recipient['coach_name'];
		$result[CoreNotification::SC_COACH_EMAIL]	            = $recipient['coach_email'];
		$result[CoreNotification::SC_COACH_SESSION_DATES]	    = $recipient['coaching_session_dates'];
		$result[CoreNotification::SC_COACH_WEBINAR_DATE_BEGIN]	= $recipient['coaching_webinar_date_begin'];
		$result[CoreNotification::SC_COACH_WEBINAR_DATE_END]	= $recipient['coaching_webinar_date_end'];
		$result[CoreNotification::SC_COACH_WEBINAR_NAME]	    = $recipient['coaching_webinar_name'];
		$result[CoreNotification::SC_COACH_WEBINAR_URL]	        = $recipient['coaching_webinar_url'];
		$result[CoreNotification::SC_COACH_WEBINAR_DETAILS]		= $recipient['coaching_webinar_meeting_details'];
		return $result;
	}

	/**
	 * allowedScheduleTypes() gets list of allowed schedule types
	 * @return array - list of all allowed schedule types
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
