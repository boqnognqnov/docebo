<?php

class App7020DeleteAssetByAdmin extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// ONLY "AT"
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			// Get event params and validate them
			$contentObject = $this->eventParams['content'];
			$userObject = CoreUser::model()->findByPk($contentObject->userId);
			$userObjectAdmin = CoreUser::model()->findByPk($this->eventParams['users']);
			$users = array($userObject->idst);
			$recipCollection = $this->getUsersDataFromArray($users);

			$fullName = trim($userObjectAdmin->firstname) . ' ' . trim($userObjectAdmin->lastname);
			$userid = trim(ltrim($userObjectAdmin->userid, '/'));

			$response->metadata['adminName'] = (!empty($fullName) ? $fullName .' ('.$userid.')' : $userid);
			$response->metadata['contentTitle'] = $contentObject->title;
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$result[CoreNotification::SC_APP7020_SUPER_ADMIN] = (is_array($metadata) && isset($metadata['adminName']) ? $metadata['adminName'] : '');
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = (is_array($metadata) && isset($metadata['contentTitle']) ? $metadata['contentTitle'] : '');
		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userId) {
			$userModel = CoreUser::model()->findByPk($userId);
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] = array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}

}
