<?php

class UserUnenrolledFromWebinarSession extends NotificationHandler implements INotificationHandler {


	public function getRecipientsData( $params = FALSE ) {

		$response             = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection      = array( 'users' => array(), 'languages' => array() );

		// ONLY "AT"
		if ( $this->notification->schedule_type == CoreNotification::SCHEDULE_AT ) {

			$session_id   = isset( $this->eventParams['session'] ) ? $this->eventParams['session'] : NULL;

			$sessionModel = WebinarSession::model()->findByPk($session_id);
			$response->metadata['sessionModel'] = $sessionModel;

			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : NULL;
			$user_id = $this->eventParams['user_id'];

			if (!$sessionModel)
				return $response;

			$targetUsers   = $this->maskItems( self::MASK_USERS, array( $user_id ) );
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $courseModel->idCourse ) );

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$recipCollection = $this->helperGetAllGodAdmins();
					foreach($recipCollection['users'] as &$godadmin){
						$godadmin['user'] = $user_id;
					}
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$studentsIdsts = Yii::app()->getDb()->createCommand()
						->select('idst')
						->from(CoreUser::model()->tableName())
						->where(array('IN', 'idst', $targetUsers))
						->queryColumn();
					$levels = array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
					$recipCollection = $this->helperGetUsersData($studentsIdsts, false, $levels, false, false, false);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:

					// Get recipients details
					$recipCollection = $this->helperGetPowerUsers(array($user_id), $targetCourses, true, false);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					$recipCollection = $this->getTeachersDataFromUser($user_id, $session_id, $courseModel->idCourse);
					break;
			}

			$sessionDates = $sessionModel->getDates();
			$response->metadata['sessionDates'] = $sessionDates;

			$ics = new ICSManager(ICSManager::TYPE_CANCEL, $sessionModel->date_begin, $sessionModel->date_end, $sessionModel->name, '', null, $this->notification->from_name, $this->notification->from_email, $sessionModel);
			if (!empty($recipCollection['users'])) {
				
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					$recipCollection['users'][$index]['id_session'] = $sessionModel->id_session;
					$recipCollection['users'][$index]['icsManager'] = $ics;
					$recipCollection['users'][$index]['user'] = $user_id;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages  = $recipCollection['languages'];

		return $response;
	}

	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$sessionModel = $metadata['sessionModel'];
		$sessionDates = $metadata['sessionDates'];
		list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_SESSION_DATES] = $dateInfo;
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = $sessionModel->getToolName(true);
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = $details;

		return $result;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();

		return $descriptor;
	}
}
