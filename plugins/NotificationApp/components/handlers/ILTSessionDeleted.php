<?php

class ILTSessionDeleted extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$course_id = isset($this->eventParams['course_id']) ? $this->eventParams['course_id'] : null;
			$sessionData = isset($this->eventParams['session_data']) ? $this->eventParams['session_data'] : null;

			$targetUsers = false;
			if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER || $this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER) {
				$targetUsers = $sessionData['usersIds'];
			}
			$targetCourses = $this->maskItems( self::MASK_COURSES, array($course_id) );

			if(is_array($targetCourses) && empty($targetCourses))
				return $response;

			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true, false, false, $targetUsers);
			$sessionDatesData = null;
			if(isset($sessionData['sessionDatesData']) && $sessionData['sessionDatesData']){
				$sessionDatesData = $sessionData['sessionDatesData'];
			}


			$ics = new ICSManager(ICSManager::TYPE_CANCEL, $sessionData['date_begin'], $sessionData['date_end'], $sessionData['name'], $sessionData['other_info'], $sessionData['icsLocation'], $this->notification->from_name, $this->notification->from_email, null, $sessionDatesData);
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $course_id;
					$recipCollection['users'][$index]['session_data'] = $sessionData;
					$recipCollection['users'][$index]['icsManager'] = $ics;
				}
			}

		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
