<?php
/**
 * 
 * 
 *
 */
class NewUserCreatedSelfreg extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			
			if (!$userModel) {
				if(isset($this->eventParams['id_user']))
					$userModel = CoreUser::model()->findByPk($this->eventParams['id_user']);
				else
					return $recipCollection;
			}
			
			$targetUsers 	= array($userModel->idst);
			$targetCourses 	= false;
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY, false, false, false, array(), false, false);
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['user'] = $userModel->idst;
				}
			}
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
