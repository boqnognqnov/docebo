<?php
class App7020ChannelAssignment extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {


			// Get event params and validate them
			$channelModel = $this->eventParams['channelModel'];
			$experts = $this->eventParams['experts'];
			$superAdminId = $this->eventParams['superAdminId'];


			// Get "Experts" data and make them as notification recipients
			$criteria = new CDbCriteria();
			//if(count($experts) > 1 ) {
				$criteria->addInCondition('idst', array(implode(',', $experts)));
			//}
			//else{
			//	$criteria->addInCondition('idst', $experts);
			//}
			$recipients = CoreUser::model()->findAll($criteria);
			$recipCollection = $this->getUsersDataFromArray($recipients);

			// Get "Super Admin" data
			$superAdmin = CoreUser::model()->findByPk($superAdminId);

			$response->metadata['channelModel'] = $channelModel;
			$response->metadata['superAdmin'] = $superAdmin;
 		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}



	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$channelModel = $metadata['channelModel'];
		$superAdmin = $metadata['superAdmin'];

		/* @var $channelModel App7020Channels */
		$trans = (!empty($channelModel) ? $channelModel->translation(Yii::app()->getLanguage()) : false);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_CHANNEL_NAME] = (is_array($trans) && isset($trans['name']) ? $trans['name'] : '');
		$result[CoreNotification::SC_APP7020_SUPER_ADMIN] = (!empty($superAdmin) ? CoreUser::getForamattedNames($superAdmin, ' ') : '');

		return $result;
	}



	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] = array(
				'idst' => $userModel->idst,
				'user' => $userModel->idst,
				'username' => trim($userModel->userid, '/'),
				'email' => $userModel->email,
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
