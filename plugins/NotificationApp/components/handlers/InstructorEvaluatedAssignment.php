<?php

class InstructorEvaluatedAssignment extends NotificationHandler implements INotificationHandler
{

	const TYPE_FREE_TEXT_ANSWER_TRACK = 'free_text';
	const TYPE_ASSIGNMENT = 'assignment';

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 * @param bool|string $params Optional parameters
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseId = $this->eventParams['course'];
			$courseModel = LearningCourse::model()->findByAttributes(array(
				'idCourse' => $courseId
			));
			$idObject = $this->eventParams['idObject'];
			$idObjectType = isset($this->eventParams['idObjectType']) ? $this->eventParams['idObjectType'] : self::TYPE_ASSIGNMENT;

			$user = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			if(is_numeric($user)){
				$userModel = CoreUser::model()->findByPk($user);
			}elseif($user instanceof CoreUser){
				$userModel = $user;
			}

			$targetUsers = $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			if (!empty($targetCourses) && !empty($targetUsers)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);
			}

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
					$recipCollection['users'][$index]['user'] = $userModel->idst;
					$recipCollection['users'][$index]['idObject'] = $idObject;
					$recipCollection['users'][$index]['idObjectType'] = $idObjectType;
					$recipCollection['users'][$index]['completed_at'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}
}