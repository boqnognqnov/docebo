<?php

/**
 * Created by PhpStorm.
 * User: Asen Nikolov
 * Date: 25-Jun-15
 * Time: 10:07 AM
 * @TODO Rename this class so it doesn't include "ILT" in its name to avoid confusion. ILT sessions are only for classroom courses
 */
class ILTWebinarSessionStarting extends NotificationHandler implements INotificationHandler
{

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 * @param array|bool $params Optional parameters
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 * @param bool|string $params Optional parameters
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();

		$recipCollection = array('users' => array(), 'languages' => array());

		$targetCourses = $this->maskItems(self::MASK_COURSES);
		$targetUsers   = $this->maskItems( self::MASK_USERS);

		if(is_array($targetCourses) && empty($targetCourses))
			return $response;

		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {

			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType = $this->notification->schedule_type;

			$sessions = $this->getWebinarSessionInPeriod($shiftType, $shiftNumber, $shiftPeriod, $targetCourses);

			if(empty($sessions))
				return $response;

			$pu_sessions = array();

			switch($this->notification->recipient)
			{
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					$recipCollectionTemp = $this->helperGetAllGodAdmins();
				break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
					$command = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPuCourse::model()->tableName());

					if($targetCourses !== false)
						$command->where(array('IN', 'course_id', $targetCourses));

					if(!empty($targetUsers) && ($targetCourses !== false)){
						$command->andWhere(array('IN', 'puser_id', $targetUsers));
					}elseif(!empty($targetUsers)){
						$command->where(array('IN', 'puser_id', $targetUsers));
					}


					$puserIds = $command->queryColumn();
					$recipCollectionTemp = $this->helperGetUsersData($puserIds, false, false, false, false, false);

					$command = Yii::app()->db->createCommand()->
						select("puc.puser_id, ws.id_session")->
						from(WebinarSession::model()->tableName().' ws')->
						join(CoreUserPuCourse::model()->tableName().' puc', 'puc.course_id = ws.course_id')
						->where(array('IN', 'ws.id_session', $sessions))
						->andWhere(array('IN', 'puc.puser_id', $puserIds));

					$tmp = $command->queryAll();
					foreach($tmp as $data)
						$pu_sessions[$data['puser_id']][$data['id_session']] = $data['id_session'];
				break;
				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					$levelFilter = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH);
					$enrollments = $this->getWebinarSessionsBeforeAfterDataSimple($shiftType, $shiftNumber, $shiftPeriod, $targetCourses, $targetUsers, $levelFilter);
					$recipCollectionTemp = $this->helperGetUsersData(array_keys($enrollments));
				break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					$levelFilter = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
					$enrollments = $this->getWebinarSessionsBeforeAfterDataSimple($shiftType, $shiftNumber, $shiftPeriod, $targetCourses, $targetUsers, $levelFilter);
					$recipCollectionTemp = $this->helperGetUsersData(array_keys($enrollments));
				break;
			}

			if(empty($recipCollectionTemp))
				return $response;

			$recipCollection = array('users' => array(), 'languages' => $recipCollectionTemp['languages']);
			$index = 0;
			$session_info = array();

			$c = new CDbCriteria();
			$c->addInCondition('id_session', $sessions);
			$c->index = 'id_session';
			$sessionModels = WebinarSession::model()->with('course')->findAll($c);

			foreach($sessions as $id_session)
			{
				$sessionModel = $sessionModels[$id_session];

				$courseModel = $sessionModel->course;

				// Prepare Session start and end dates
				$webinar_start_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getStartDate($id_session)));
				$webinar_end_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getEndDate($id_session)));

				$details = array();
				foreach (NotificationTransportManager::getAllTextFormatTypes() as $textFormat) {
					// NOTE: since we cannot predict at this point which text format will be used by the notification handler, we
					// simply use them all here and the right one will be resolved later in getShortcodesData() method.
					// All this method should be refactored in order to have this kind of data in getShortcodesData() method
					// instead of here.
					$details[$textFormat] = $this->getWebinarDetailsTagContentsFromSessionModel($sessionModel, $textFormat);
				}

				$session_info[$id_session] = array(
					'course'					=> $courseModel->idCourse,
					'course_name'				=> $courseModel->name,
					'session_name'				=> $sessionModel->name,
					'webinar_tool'				=> $sessionModel->getToolName(),

					'webinar_session_details'	=> $details,
					'webinar_start_date'		=> $webinar_start_date, // UTC
					'webinar_end_date'			=> $webinar_end_date, // UTC
				);
			}

			foreach ($recipCollectionTemp['users'] as $key => $user)
			{
				switch($this->notification->recipient)
				{
					case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
						foreach($sessions as $id_session)
						{
							$sessionModel = $sessionModels[$id_session];

							// Prepare Session start and end dates
							$webinar_start_date = $session_info[$id_session]['webinar_start_date'];
							$webinar_end_date = $session_info[$id_session]['webinar_end_date'];

							$details = $session_info[$id_session]['webinar_session_details'];

							$toolName = $session_info[$id_session]['webinar_tool'];

							$dateBegin = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_start_date);
							$dateEnd = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_end_date);

							$ics = new ICSManager(ICSManager::TYPE_REQUEST, $dateBegin, $dateEnd, $sessionModel->name, $sessionModel->description, null, $this->notification->from_name, $this->notification->from_email, $sessionModel);

							$recipCollection['users'][$index] = $user;

							$recipCollection['users'][$index]['course']						= $session_info[$id_session]['course'];
							$recipCollection['users'][$index]['course_name']				= $session_info[$id_session]['course_name'];
							$recipCollection['users'][$index]['session_name']				= $session_info[$id_session]['session_name'];
							$recipCollection['users'][$index]['session_dates']				= $dateBegin . ' - '. $dateEnd;
							$recipCollection['users'][$index]['webinar_tool']				= $toolName;
							$recipCollection['users'][$index]['webinar_session_details']	= $details;
							$recipCollection['users'][$index]['icsManager']					= $ics;
							$recipCollection['users'][$index]['id_session'] 				= $id_session;

							$index++;
						}
					break;
					case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
						if(isset($pu_sessions[$user['idst']]) && is_array($pu_sessions[$user['idst']])){
							foreach($pu_sessions[$user['idst']] as $id_session)
							{
								$sessionModel = $sessionModels[$id_session];

								// Prepare Session start and end dates
								$webinar_start_date = $session_info[$id_session]['webinar_start_date'];
								$webinar_end_date = $session_info[$id_session]['webinar_end_date'];

								$details = $session_info[$id_session]['webinar_session_details'];

								$toolName = $session_info[$id_session]['webinar_tool'];

								$dateBegin = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_start_date);
								$dateEnd = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_end_date);

								$ics = new ICSManager(ICSManager::TYPE_REQUEST, $dateBegin, $dateEnd, $sessionModel->name, $sessionModel->description, null, $this->notification->from_name, $this->notification->from_email, $sessionModel);
								$recipCollection['users'][$index] = $user;

								$recipCollection['users'][$index]['course']						= $session_info[$id_session]['course'];
								$recipCollection['users'][$index]['course_name']				= $session_info[$id_session]['course_name'];
								$recipCollection['users'][$index]['session_name']				= $session_info[$id_session]['session_name'];
								$recipCollection['users'][$index]['session_dates']				= $dateBegin . ' - '. $dateEnd;
								$recipCollection['users'][$index]['webinar_tool']				= $toolName;
								$recipCollection['users'][$index]['webinar_session_details']	= $details;
								$recipCollection['users'][$index]['icsManager']					= $ics;
								$recipCollection['users'][$index]['id_session'] 				= $id_session;

								$index++;
							}
						}
					break;
					case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					case CoreNotification::NOTIFY_RECIPIENTS_USER:
						if(isset($enrollments[$user['idst']])){
							foreach($enrollments[$user['idst']] as $id_session)
							{
								$sessionModel = $sessionModels[$id_session];

								// Prepare Session start and end dates
								$webinar_start_date = $session_info[$id_session]['webinar_start_date'];
								$webinar_end_date = $session_info[$id_session]['webinar_end_date'];

								$details = $session_info[$id_session]['webinar_session_details'];

								$toolName = $session_info[$id_session]['webinar_tool'];

								$dateBegin = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_start_date);
								$dateEnd = Yii::app()->localtime->toUserLocalDatetime($user['idst'], $webinar_end_date);

								$ics = new ICSManager(ICSManager::TYPE_REQUEST, $dateBegin, $dateEnd, $sessionModel->name, $sessionModel->description, null, $this->notification->from_name, $this->notification->from_email, $sessionModel);
								$recipCollection['users'][$index] = $user;

								$recipCollection['users'][$index]['course']						= $session_info[$id_session]['course'];
								$recipCollection['users'][$index]['course_name']				= $session_info[$id_session]['course_name'];
								$recipCollection['users'][$index]['session_name']				= $session_info[$id_session]['session_name'];
								$recipCollection['users'][$index]['session_dates']				= $dateBegin . ' - '. $dateEnd;
								$recipCollection['users'][$index]['webinar_tool']				= $toolName;
								$recipCollection['users'][$index]['webinar_session_details']	= $details;
								$recipCollection['users'][$index]['icsManager']					= $ics;
								$recipCollection['users'][$index]['id_session'] 				= $id_session;

								$index++;
							}
						}
					break;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		$details = $recipient['webinar_session_details']; //just for readability

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$result[CoreNotification::SC_COURSENAME] = $recipient['course_name'];
		$result[CoreNotification::SC_SESSION_DATES] = $recipient['session_dates'];
		$result[CoreNotification::SC_SESSION_NAME] = $recipient['session_name'];
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = $recipient['webinar_tool'];
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = (isset($details[$textFormat]) ? $details[$textFormat] : '');

		return $result;
	}




	public function getWebinarSessionInPeriod($shiftType = false, $shiftNumber = false, $shiftPeriod = false, $targetCourses = false)
	{
		$intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
		$command = Yii::app()->db->createCommand()
			->selectDistinct("w.id_session")
			->from(WebinarSession::model()->tableName().' w')
			->join(
				WebinarSessionDate::model()->tableName().' dates',
				"w.id_session=dates.id_session
				AND CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC') >= :start
				AND CONVERT_TZ(CONCAT(dates.day,' ',dates.time_begin), dates.timezone_begin, 'UTC') <= :end",
				array(
					':start'=>$intervalBeginAndEnd[0],
					':end'=>$intervalBeginAndEnd[1],
				)
			)
			->group('w.id_session');


		if($targetCourses !== false)
			$command->andWhere(array('in', 'w.course_id', $targetCourses));

		return $command->queryColumn();
	}



	public function getWebinarSessionsBeforeAfterDataSimple($shiftType = false, $shiftNumber = false, $shiftPeriod = false, $targetCourses = false, $targetUsers = false, $levelFilter = false)
	{
		$intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
		$command = Yii::app()->db->createCommand()->
		select("wsu.id_user, ws.id_session")->
		from(WebinarSession::model()->tableName() . ' ws')->
		join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session=wsu.id_session')->
		join(WebinarSessionDate::model()->tableName() . ' wsd', 'wsd.id_session=wsu.id_session')->
		join(LearningCourseuser::model()->tableName() . ' lcu', 'wsu.id_user=lcu.idUser AND ws.course_id=lcu.idCourse')->
		join(CoreUser::model()->tableName() . ' cu', 'wsu.id_user=cu.idst')->
		where("CONVERT_TZ(CONCAT(wsd.day,' ',wsd.time_begin), wsd.timezone_begin, 'UTC') >= '" . $intervalBeginAndEnd[0] . "' AND CONVERT_TZ(CONCAT(wsd.day,' ',wsd.time_begin), wsd.timezone_begin, 'UTC') <= '" . $intervalBeginAndEnd[1] . "'");

		if($targetCourses !== false)
			$command->andWhere(array('in', 'ws.course_id', $targetCourses));

		if($targetUsers !== false)
			$command->andWhere(array('in', 'wsu.id_user', $targetUsers));

		if($levelFilter !== false)
			$command->andWhere(array('in', 'lcu.level', $levelFilter));

		$enrollments = $command->queryAll();
		$result = array();

		foreach($enrollments as $data)
			$result[$data['id_user']][$data['id_session']] = $data['id_session'];

		return $result;
	}
}