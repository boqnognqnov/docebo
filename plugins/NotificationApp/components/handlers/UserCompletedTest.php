<?php
/**
 * Created by PhpStorm.
 * User: NikVasilev
 * Date: 21.10.2016 г.
 * Time: 10:19
 */

class UserCompletedTest extends NotificationHandler implements INotificationHandler    {

    /**
     * Collect recipients data and return data structure/class
     *
     * @return NotificationHandlerResponse
     */
    public function getRecipientsData($params=false) {
        /* @var $courseModel LearningCourse */
        /* @var $loModel	LearningOrganization */
        /* @var $userModel 	CoreUser */


        $response = new NotificationHandlerResponse();
        $response->recipients = array();
        $response->languages  = array();
        $recipCollection = array('users'=> array(), 'languages'=> array());

        // This notification is definitely for "AT THE TIME OF THE EVENT" mode, at least for now
        if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

            $courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
            $loModel		= isset($this->eventParams['learningobject']) ? $this->eventParams['learningobject'] : null;
            $userModel 		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

            if (!$courseModel || !$loModel || !$userModel) {
                if(isset($this->eventParams['id_user']) && isset($this->eventParams['id_course']) && isset($this->eventParams['id_learningobject'])){
                    $courseModel    = LearningCourse::model()->findByPk($this->eventParams['id_course']);
                    $loModel		= LearningOrganization::model()->findByPk($this->eventParams['id_learningobject']);
                    $userModel 		= CoreUser::model()->findByPk($this->eventParams['id_user']);
                }
                else {
                    return $recipCollection;
                }
            }
            $targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
            $targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

            if (!empty($targetCourses)) {
                $recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_BOTH, false, false, false,array(),false,true,false);

                // ADD RELATED INFO
                if (!empty($recipCollection['users'])) {
                    foreach ($recipCollection['users'] as $index => $recipient) {
                        $recipCollection['users'][$index]['course'] 		= $courseModel->idCourse;
                        $recipCollection['users'][$index]['learningobject'] = $loModel->idOrg;
                        $recipCollection['users'][$index]['user'] = $userModel->idst;
                    }
                }
            }

        }

        $response->recipients 	= $recipCollection['users'];
        $response->languages 	= $recipCollection['languages'];

        return $response;
    }

    /**
     *
     * @param string $params
     * @return NotificationDescriptor
     */
    public static function descriptor($params=false) {
        $descriptor = new NotificationDescriptor();
        return $descriptor;
    }


}