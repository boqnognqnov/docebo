<?php
/**
 * 
 * 
 *
 */
class CourseDeleted extends NotificationHandler implements INotificationHandler    {
	

	/**
	 * LOGIC: 
	 * Send notification to: All Subscribed users OR GodAdmins if they pass the Users filter
	 * 
	 * Notify all course subscribers if they are 
	 * 
	 * 
	 * @return array
	 */
	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			
			if (!$courseModel) {
				if(isset($this->eventParams['id_course']))
					$courseModel = LearningCourse::model()->findByPk($this->eventParams['id_course']);
				else
					return $recipCollection;
			}
			
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_NONE);
			
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
				}
			}
				
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}

	
	/**
	 * In this case we override the parent method because when it is called (the parrent method) the course will be already deleted and course model is gone
	 *  
	 * @see NotificationHandler::getShortcodesData()
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			if ($courseModel) {
				$result[CoreNotification::SC_COURSENAME] 		= $courseModel->name;
			}
		}
		
		return $result;
		
	}

	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
