<?php




class UserSubscribedGroup extends NotificationHandler implements INotificationHandler    {
	
	/* @var $groupModel CoreGroup */
	/* @var $userModel CoreUser */

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// "AT" ?  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$groupModel = isset($this->eventParams['group']) ? $this->eventParams['group'] : null;
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			
			$targetUsers 	= array($userModel->idst);
			$targetGroups 	= $this->maskItems(self::MASK_GROUPS, array($groupModel->idst));
			
			if (!empty($targetGroups)) {
				
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, false, self::PU_OF_USERSONLY, false, false, false, array(), false, false);
				
				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['group'] 			= $groupModel->idst;
						$recipCollection['users'][$index]['user'] 			= $userModel->idst;
					}
				}
			}
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
