<?php
/**
 * 
 * 
 *
 */
class UserApproval extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$userModel 		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			
			if (!$userModel || !$courseModel) {
				return $recipCollection;
			}
			
			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);
				
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['user'] 	= $userModel->idst;
					$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
				}
			}
					
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}

