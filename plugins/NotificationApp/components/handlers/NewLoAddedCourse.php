<?php
/**
 * 
 * 
 *
 */
class NewLoAddedCourse extends NotificationHandler implements INotificationHandler    {
	
	/**
	 * Collect recipients data and return data structure/class
	 * 
	 * @return NotificationHandlerResponse
	 */
	public function getRecipientsData($params=false) {
		/* @var $courseModel LearningCourse */
		/* @var $loModel	LearningOrganization */
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		
		// This notification is definitely for "AT THE TIME OF THE EVENT" mode, at least for now
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$loModel		= isset($this->eventParams['learningobject']) ? $this->eventParams['learningobject'] : null;
			// The user who added the LO (not used yet)
			$userModel 		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
				
			if (!$courseModel || !$loModel || !$userModel) 
				return $recipCollection;
			
			$targetUsers 	= false;
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			
			if (!empty($targetCourses)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY);
				
				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] 		= $courseModel->idCourse;
						$recipCollection['users'][$index]['learningobject'] = $loModel->idOrg;
					}
				}
			}
			
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
	
}