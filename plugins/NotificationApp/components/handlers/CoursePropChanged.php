<?php
/**
 * 
 * 
 *
 */
class CoursePropChanged extends NotificationHandler implements INotificationHandler    {
	
	/**
	 * Collect recipients data and return data structure/class
	 * 
	 * @return NotificationHandlerResponse
	 */
	public function getRecipientsData($params=false) {
		/* @var $courseModel LearningCourse */
		/* @var $userModel 	CoreUser */
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$userModel 		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			
			if (!$courseModel || !$userModel) 
				return $recipCollection;
			
			// All users could be involved (e.g. subscribers), hence the "false"
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			
			if (!empty($targetCourses)) {
				$statuses = array(
					LearningCourseuser::$COURSE_USER_SUBSCRIBED,
					LearningCourseuser::$COURSE_USER_BEGIN,
				);
				
				// Get STUDENTS only, having above statuses
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true, $statuses);
				
				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] = $courseModel->idCourse;
						$recipCollection['users'][$index]['user'] 	= $userModel ? $userModel->idst : null;
					}
				}
			}
			
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}