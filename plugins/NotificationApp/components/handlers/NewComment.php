<?php


class NewComment extends NotificationHandler implements INotificationHandler    {

	/* @var $threadModel LearningForumthread */

	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$courseModel 	= isset($this->eventParams['course']) ? $this->eventParams['course'] : null;
			$userModel		= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$threadModel	= isset($this->eventParams['thread']) ? $this->eventParams['thread'] : null;
			$message			= isset($this->eventParams['message']) ? $this->eventParams['message'] : null;

			if (!$courseModel || !$userModel || !$threadModel || !$message) {
				return array();
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			// Return nothing if the course this thread is is not among associated courses filter output
			if (!in_array($threadModel->forum->idCourse, $targetCourses))
				return $recipCollection;

			// Get out if the POSTER is not among filtered users
			if ($targetUsers !== false && !in_array($userModel->idst, $targetUsers))
				return $recipCollection;

			// Get related recipients
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true);

			//Clear the user which inserted the message, this specific user is comming from the event params
			foreach($recipCollection['users'] as $key => $user){
				if ($user['idst'] == $userModel->idst) {
					unset($recipCollection['users'][$key]);
				}
			}

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['thread']		= $threadModel->idThread;
					$recipCollection['users'][$index]['course']		= $courseModel->idCourse;
					$recipCollection['users'][$index]['message']	= $message;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}



	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
