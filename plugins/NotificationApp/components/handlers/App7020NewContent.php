<?php

class App7020NewContent extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$contentId = $this->eventParams['contentId'];
			$contentObject = App7020Assets::model()->findByPk($contentId);
			$users = App7020Assets::getAssetExperts($contentId);
			$recipCollection = $this->getUsersDataFromArray($users);
			$userObject = CoreUser::model()->findByPk($contentObject->userId);

			$response->metadata['contentUrl'] = Docebo::createApp7020AssetsViewNgrUrl($contentObject->id, true);
			$response->metadata['contentTitle'] = $contentObject->title;
			$response->metadata['contentOwnerName'] = CoreUser::getForamattedNames($userObject, ' ');
			$response->metadata['contentOwner'] = $userObject;
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}



	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_CONTENT_AUTHOR] = $metadata['contentOwnerName'];
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = $metadata['contentTitle'];
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = $metadata['contentUrl'];

		return $result;
	}



	public function getSpecialProperties($recipient, $metadata = array()) {

		$authorName = (is_array($metadata) && isset($metadata['contentOwnerName']) ? $metadata['contentOwnerName'] : false);
		$userObject = (is_array($metadata) && isset($metadata['contentOwner']) ? $metadata['contentOwner'] : false);
		if (!empty($userObject)) {
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if (is_array($useravatar) && isset($useravatar['avatar']) && !empty($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
			} else {
				$avatarImg = '';
			}
		} else {
			$avatarImg = '';
		}

		return array(
			'author' => $authorName,
			'avatar' => $avatarImg
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel['idst']);
			$users[$userModel['idst']] = array(
				'idst' => $userModel['idst'],
				'user' => $userModel['idst'],
				'username' => trim($userModel['userid'], '/'),
				'email' => $userModel['email'],
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
