<?php
class SentMessageToCoachByLearner extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 * NOTE: for this notification only - just send to the session coach !!!
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {
		$response               = new NotificationHandlerResponse();
		$response->recipients   = array();
		$response->languages    = array();
		$recipCollection        = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$courseModel            = $this->eventParams['course'];
			$idCourse               = $this->eventParams['idCourse'];
			$idUser                 = $this->eventParams['idUser'];
			$idCoach                = $this->eventParams['idCoach'];
			$idSession              = $this->eventParams['idSession'];
			$coachingChatMessage    = $this->eventParams['message'];

			// Get the User model
			$userModel              = CoreUser::model()->findByPk($idUser);

			// Get the Coach model
			$coachModel             = CoreUser::model()->findByPk($idCoach);

			// Get the Coaching Session model
			$coachingSessionModel   = LearningCourseCoachingSession::model()->findByPk($idSession);

			// If there is something missing
			if(!$courseModel || !$idUser || !$userModel || !$coachingSessionModel || !$coachModel)
				return $recipCollection;

			// Prepare Session start and end dates
			$start_date         = Yii::app()->localtime->stripTimeFromLocalDateTime(
									Yii::app()->localtime->toLocalDateTime($coachingSessionModel->datetime_start));
			$end_date           = Yii::app()->localtime->stripTimeFromLocalDateTime(
									Yii::app()->localtime->toLocalDateTime($coachingSessionModel->datetime_end));

			// Run filtering on users
			$targetUsers 	    = $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	    = $this->maskItems(self::MASK_COURSES, array((int) $idCourse));

			// Get recipients data
			$recipCollection    = $this->helperGetRecipientsFromUsersData(
									$this->notification->recipient,
									$targetUsers,
									$targetCourses,
									self::PU_OF_USERSONLY,
									false,
									false,
									false,
									array((int) $idCoach) // filter users to receive the notification by id of the coach
			);


			// Add related info to each recipient
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['course']                 = $courseModel->idCourse;
					$recipCollection['users'][$index]['course_name']            = $courseModel->name;
					$recipCollection['users'][$index]['course_link']            = Docebo::createAbsoluteLmsUrl('player', array('course_id' => $courseModel->idCourse));
					$recipCollection['users'][$index]['user']                   = $idUser;
					$recipCollection['users'][$index]['username']               = Yii::app()->user->getRelativeUsername($userModel->userid);
					$recipCollection['users'][$index]['coach_name']             = $coachModel->getFullName();
					$recipCollection['users'][$index]['coach_email']            = $coachModel->email;
					$recipCollection['users'][$index]['coaching_session_dates'] = $start_date . ' - '. $end_date;
					$recipCollection['users'][$index]['coaching_chat_message']  = $coachingChatMessage;
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 * getShortcodesData() - sets the custom short codes
	 * @param array $recipient - array with recipients data
	 * @return array - array with shortcodes and values to be replaced
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result =  parent::getShortcodesData($recipient, $metadata, $textFormat);
		/********- Set custom ['shortcodes'] -********/
		$result[CoreNotification::SC_COURSENAME]	        = $recipient['course_name'];
		$replace[CoreNotification::SC_COURSE_LINK]	        = $recipient['course_link'];
		$result[CoreNotification::SC_COACH_NAME]	        = $recipient['coach_name'];
		$result[CoreNotification::SC_COACH_EMAIL]	        = $recipient['coach_email'];
		$result[CoreNotification::SC_COACH_SESSION_DATES]	= $recipient['coaching_session_dates'];
		$result[CoreNotification::SC_COACH_CHAT_MESSAGE]	= $recipient['coaching_chat_message'];

		return $result;
	}

	/**
	 * allowedScheduleTypes() gets list of allowed schedule types
	 * @return array - list of all allowed schedule types
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
