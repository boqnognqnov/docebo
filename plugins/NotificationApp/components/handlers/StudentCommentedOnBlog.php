<?php


class StudentCommentedOnBlog extends NotificationHandler implements INotificationHandler {

	public function getRecipientsData($params = false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$userModel = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$postModel = isset($this->eventParams['post']) ? $this->eventParams['post'] : null;
			$commentModel = isset($this->eventParams['comment']) ? $this->eventParams['comment'] : null;

			if (!$userModel || !$postModel || !$commentModel) {
				return array();
			}

			// Well we wouldn't send notification if somebody commented his own post
			// OR if the post has id_course, this notification is only for blog that are not in course widget
			if (($postModel->id_user == $commentModel->id_user) || $postModel->id_course)
				return $recipCollection;

			// Add the Post Owner in the list of recepients
			$targetUsers = array($postModel->id_user);

			// Get related recipients
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, false, self::PU_OF_USERSONLY, false);

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['post'] = $postModel->id_post;
					$recipCollection['users'][$index]['comment'] = $commentModel->id_comment;
				}
			}
		}
		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

}
