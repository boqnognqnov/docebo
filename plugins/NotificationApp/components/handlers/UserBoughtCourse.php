<?php
/**
 * 
 * 
 *
 */
class UserBoughtCourse extends NotificationHandler implements INotificationHandler    {
	
	/**
	 * Collect recipients data and return data structure/class
	 * 
	 * @return NotificationHandlerResponse
	 */
	public function getRecipientsData($params=false) {
		/* @var $courseModel LearningCourse */
		/* @var $userModel 	CoreUser */
		/* @var $transactionModel EcommerceTransaction */
		/* @var $item EcommerceTransactionInfo */

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$transactionModel = isset($this->eventParams['transaction']) ? $this->eventParams['transaction'] : null;
			$userModel = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			// No user, transaction or items ? get out
			if (!$userModel) return $recipCollection;
			if (!$transactionModel) return $recipCollection;
			$items = $transactionModel->transaction_items;
			if (empty($items)) {
				return $recipCollection; 
			}

			$candidateCourses = array();
			foreach ($items as $item) {
				$candidateCourses[] = $item->id_course;
			}

			// All users could be involved (e.g. subscribers), hence the "false"
			$targetUsers = $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses = $this->maskItems(self::MASK_COURSES, $candidateCourses);

			if(!empty($targetCourses)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_BOTH);
			}

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['transaction'] = $transactionModel->id_trans;
					$recipCollection['users'][$index]['user'] = $userModel->idst;
				}
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
	}



	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}