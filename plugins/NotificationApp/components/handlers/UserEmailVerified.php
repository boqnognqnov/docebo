<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 23-Nov-15
 * Time: 11:02 AM
 */
class UserEmailVerified extends NotificationHandler implements INotificationHandler
{
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		$user = (isset($this->eventParams['user']))? $this->eventParams['user'] : null;
		if(!$user){
			$user = CoreUser::model()->findByPk($this->eventParams['id_user']);
		}
		// ONLY "AT"
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Filter by groups
			$groups = $this->groupsMask;
			$usersInGroups = Yii::app()->db->createCommand()
					->select('idstMember')
					->from(CoreGroupMembers::model()->tableName())
					->where(array('IN','idst', $groups))->queryColumn();

			// Filter by branches
			$usersInBranches = array();
			$branches = $this->branchesMask;
			foreach($branches as $branchId){
				$model = CoreOrgChartTree::model()->findByAttributes(
						array('idOrg'=>$branchId)
				);
				$usersInBranches = array_merge($usersInBranches, $model->getNodeUsers());
			}
			$users = array_unique(array_merge($usersInBranches, $usersInGroups));

			// has filter by groups and/or branches
			if(!empty($this->groupsMask) || !empty($this->branchesMask)){
				if (in_array($user->idst, $users)) {
					$recipCollection = $this->processRecipient($user);
				}
			}else {
				$recipCollection = $this->processRecipient($user);
			}
		}
		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	private function processRecipient($user){
		$language = Yii::app()->db->createCommand()
				->select('value')
				->from(CoreSettingUser::model()->tableName())
				->where('path_name = "ui.language" AND id_user = :userId', array(':userId' => $user->idst))->queryScalar();
		$recipCollection['languages'] = array($language => $language);
		$recipCollection['users'][0] = array(
				'idst' => $user->idst,
				'email' => $user->email,
				'username' => trim($user->userid, '/'),
				'language' => $language,
		);

		//set flag for flash message to show
		$validHours = intval(Settings::get('hour_request_limit'));
		$validTime = time() + $validHours * 3600;
		$paramsForMsg = array(
				'type' => 'flash',
				'timestamp' => $validTime,
				'text' => 'emailSendToUser',
		);
		$types = $this->notification->notify_type;
		if (is_array($types) && in_array('email', $types)) { //OLD CONDITION: $type == CoreNotification::NOTIFY_TYPE_EMAIL_ONLY || $type == CoreNotification::NOTIFY_TYPE_INBOX_AND_EMAIL
			// delete one time message /if exists/
			Yii::app()->db->createCommand()
					->delete(CoreSettingUser::model()->tableName(),
							'path_name="one_time_flash_msg" AND id_user = :id', array(':id' => $user->idst));

			Yii::app()->db->createCommand()
					->insert(CoreSettingUser::model()->tableName(), array(
							'path_name' => 'one_time_flash_msg',
							'id_user' => $user->idst,
							'value' => json_encode($paramsForMsg)
					));
		}

		return $recipCollection;
	}
}