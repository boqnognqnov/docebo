<?php



class UserSubscribedCourse extends NotificationHandler implements INotificationHandler    {
	

	public function getRecipientsData($params=false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseModel = isset($this->eventParams['course']) ? $this->eventParams['course'] : null; /* @var $courseModel LearningCourse */
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$expire_at = null;
			if($this->eventParams['date_expire_validity'] && (strpos($this->eventParams['date_expire_validity'], '0000-00-00') === FALSE))
				$expire_at = str_replace(array("T", "+0000"), array(" ", ""), $this->eventParams['date_expire_validity']);
			else if($this->eventParams['date_end'] && ($this->eventParams['date_end'] != '0000-00-00'))
				$expire_at = $this->eventParams['date_end'].' 23:59:59';

			$updatePowerUserSelection = isset($this->eventParams['updatePowerUserSelection']) ? $this->eventParams['updatePowerUserSelection'] : false;
			if($updatePowerUserSelection && $this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
				CoreUserPU::updatePowerUsersOfUsers($userModel->idst);
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));

			if (!empty($targetCourses) && !empty($targetUsers)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_USERSONLY);

				//power users need to be filtered by owned courses too
				if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
					//enlist the IDs of extracted PUs
					$puserIds = (is_array($recipCollection) && isset($recipCollection['users'])
						? array_keys($recipCollection['users'])
						: array());
					if (!empty($puserIds)) {
						//retrieve PUs IDs owning target courses
						$coursesPuserIds = Yii::app()->getDb()->createCommand()
							->select('puser_id')
							->from(CoreUserPuCourse::model()->tableName())
							->where(array('IN', 'course_id', $targetCourses))
							->queryColumn();
						if (!is_array($coursesPuserIds)) {
							$coursesPuserIds = array();
						}
						//detect which of them own target users AND target courses
						$filteredPuserIds = array_intersect($puserIds, $coursesPuserIds);
						//detect which PUs owns only an user or only a course (but noth both) and remove them from the recipients
						$toBeRemoved = array_diff($puserIds, $filteredPuserIds);
						if (!empty($toBeRemoved)) {
							foreach ($toBeRemoved as $id) {
								if (isset($recipCollection['users'][$id])) {
									$recipCollection['users'][$id] = null;
									unset($recipCollection['users'][$id]);
								}
							}
						}
					}
				}

				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] 		= $courseModel->idCourse;
						$recipCollection['users'][$index]['user'] 			= $userModel->idst;
						$recipCollection['users'][$index]['subscribed_at'] 	= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], Yii::app()->localtime->getUTCNow('Y-m-d H:i:s'));
						$recipCollection['users'][$index]['expire_at'] 	= $expire_at ? Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $expire_at) : '';
					}
				}
			}
		}

		// "AFTER" ?  
		else  if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {

			// ALL users/courses are candidates (flase, false)
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);

			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType	 = $this->notification->schedule_type;

			$enrollments = $this->getEnrollmentsWithEventAfterBeforePeriod($targetUsers, $targetCourses, self::ENROLL_EVENT_SUBSCRIBED, $shiftType, $shiftNumber, $shiftPeriod);
			$relatedFields = array('user', 'course', 'subscribed_at','expire_at');
			$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_BOTH, $relatedFields);
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}



	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
