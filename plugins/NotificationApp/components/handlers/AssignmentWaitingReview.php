<?php
class AssignmentWaitingReview extends NotificationHandler implements INotificationHandler    {
	/**
	 * Recipients can be "all"
	 * @param bool $params
	 * @return NotificationHandlerResponse
	 */
	public function getRecipientsData($params=false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// Restrict running for allowed only (in this case it is after and before the event)
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {
			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;

			// ALL users/courses are candidates (false, false), because we are going to observe all coaching session assignments,
			// Of course masked/filtered by assigned users & courses.
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);

			// Get all assigned users related to target users/courses, filtered according to the after/before settings.
			$deliverables = $this->getDeliverablesToReviewWithAfterPeriod($targetUsers, $targetCourses, $shiftNumber, $shiftPeriod);

			// This notification has something to add to the basic recipients information (taken out from assignment data)
			$relatedFields = array('user', 'course', 'idObject');

			// Collect an array of recipients; basically an array of items which will then be transformed into an email.
			// With that said, the number of items in this array is equal to the number of emails going to be sent by the parent method.
			$recipCollection = $this->getRecipientsFromDeliverableAssignments($deliverables, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}

	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Get list of still to be reviewed deliverables/assignments filtered by list of users & courses.
	 *
	 * @param array|bool  $usersList FALSE means "don't care" or "all"
	 * @param array|bool  $coursesList FALSE means "don't care" or "all"
	 * @param bool|int    $shiftNumber Number of periods after/before
	 * @param bool|string $shiftPeriod The period type, e.g. day. hour, week
	 */
	protected function getDeliverablesToReviewWithAfterPeriod($usersList=false, $coursesList=false, $shiftNumber=false, $shiftPeriod=false) {
		$params = array(
			':pending_evaluation_status' => LearningDeliverableObject::STATUS_PENDING,
			':lo_deliverable' => LearningOrganization::OBJECT_TYPE_DELIVERABLE
		);
		$select = array(
			'user.idst							AS idst',
			'user.userid 						AS username',
			'user.email							AS email',
			'csu_lang.value						AS language',
			'csu_timezone.value					AS timezone',
			'csu_format_locale.value			AS date_format_locale',
			'lo.idCourse						AS course',
			'ldo.idObject						AS object'
		);

		$command = Yii::app()->db->createCommand()->from('core_user user');
		$command->select(implode(',', $select));

		// Join user settings table to get the user's "language"
		$command->leftJoin('core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')");

		// Join user settings table to get the user's "timezone"
		$command->leftJoin('core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')");

		// Join user settings table to get the user's "date_format_locale"
		$command->leftJoin('core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')");

		// Join learning_deliverable_object table to only get users who submitted deliverables not yet reviewd
		$command->join('learning_deliverable_object ldo', "(user.idst=ldo.id_user) AND (ldo.evaluation_status = :pending_evaluation_status)");
		$command->join('learning_deliverable ld', "ldo.id_deliverable=ld.id_deliverable");
		$command->join('learning_organization lo', "(lo.idResource=ld.id_deliverable) AND (lo.objectType = :lo_deliverable)");

		// Filter by list of courses
		if ($coursesList !== false && is_array($coursesList))
			$command->andWhere('lo.idCourse IN (' . implode(',',$coursesList) . ')');

		// Filter by list of users
		if ($usersList !== false && is_array($usersList))
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');

		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");

		// If "after event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
		if ($shiftNumber && $shiftPeriod) {
			list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted(CoreNotification::SCHEDULE_AFTER, $shiftNumber, $shiftPeriod);
			if ($targetTimeFrom) {
				$command->andWhere('ldo.created >= :timeFrom');
				$params[':timeFrom'] = $targetTimeFrom;
			}

			if ($targetTimeTo) {
				$command->andWhere('ldo.created <= :timeTo');
				$params[':timeTo'] = $targetTimeTo;
			}
		}

		// Run the query
		$reader = $command->query($params);

		$deliverables = array();
		$languages = array();
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$deliverable = array(
				'idst' 					=> $row['idst'],
				'username'				=> trim($row['username'],'/'),
				'email'					=> $row['email'],
				'language'				=> $language,
				'timezone'				=> $row['timezone'],
				'date_format_locale'	=> $row['date_format_locale'],
				'course'				=> $row['course'],
				'user'					=> $row['idst'],
				'idObject'				=> $row['object']
			);

			$languages[$language] = 1;
			$deliverables[] = $deliverable;
		}

		return array(
			'deliverables' => $deliverables,
			'languages'	=> array_keys($languages),
		);
	}

	/**
	 * Multifunctional helper to build list of recipients data structure (suitable for directly sending emails)
	 * out of list of deliverables, having the same data structure pattern.
	 * This method builds recipient data deliverable by deliverable, where single deliverable MAY produce MORE than one
	 * recipient of a given type, e.g. one deliverable -> MANY instructors (recipients). Or MANY Power Users managing the same user/course.
	 *
	 *
	 * @param array $deliverableData Usually, this is the result of @see getCoachingAssignmentsWithEventAfterBeforePeriod method
	 * @param string $recipientType
	 * @param string $powerUsersOf If applicable, get power users managing related USERS only, or COURSES only, or BOTH, or none.
	 * @param array $relatedFields Array of  fieldName => fieldValue to add, on top of other data, per recipient
	 *
	 * @return array
	 */
	protected function getRecipientsFromDeliverableAssignments($deliverableData, $recipientType, $powerUsersOf = self::PU_OF_USERSONLY, $relatedFields=array()) {
		$data = array();
		$data['languages'] 	= array();
		$data['users']		= array();

		switch ($recipientType) {

			// INSTRUCTORS + COACHES
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$instructorLevels = array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				);

				foreach ($deliverableData['deliverables'] as $deliverable) {
					$targets = $this->helperGetUsersData(false, array($deliverable['course']), $instructorLevels);
					foreach ($targets['users'] as $tmpData) {
						$tmp = array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);

						foreach ($relatedFields as $f)
							$tmp[$f] = isset($deliverable[$f]) ? $deliverable[$f] : '??';

						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// GODADMINS
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				// YES, before the cycles!!
				$targets = $this->helperGetAllGodAdmins();
				foreach ($deliverableData['deliverables'] as $deliverable) {
					foreach ($targets['users'] as $tmpData) {
						$tmp = array(
							'idst' => $tmpData['idst'],        // godadmin
							'username' => $tmpData['username'],
							'email' => $tmpData['email'],
							'language' => $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($deliverable[$f]) ? $deliverable[$f] : '??';

						$data['users'][] = $tmp;
					}
				}
				$data['languages'] = array_merge($data['languages'], $targets['languages']);
				break;

			// POWER USERS of target users
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				foreach ($deliverableData['deliverables'] as $deliverable) {
					switch ($powerUsersOf) {
						case self::PU_OF_BOTH:
							$targets = $this->helperGetPowerUsers(array($deliverable['idst']), array($deliverable['course']));
							break;
						case self::PU_OF_USERSONLY:
							$targets = $this->helperGetPowerUsersOfUsersData(array($deliverable['idst']));
							break;
						case self::PU_OF_COURSESONLY:
							$targets = $this->helperGetPowerUsersOfCoursesData(array($deliverable['course']));
							break;
						default:
							$targets = array();
							break;
					}

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// Power user
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($deliverable[$f]) ? $deliverable[$f] : '??';

						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// MANAGERS of the users
			case CoreNotification::NOTIFY_RECIPIENTS_MANAGER: 
				foreach ($deliverableData['deliverables'] as $deliverable) {
					$targets = $this->helperGetManagersOfUsersData(array($deliverable['idst']), array($deliverable['course']));

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// Manager user
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($deliverable[$f]) ? $deliverable[$f] : '??';

						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// SUBSCRIBERS
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$data['users'] 		= $deliverableData['deliverables'];
				$data['languages']	= $deliverableData['languages'];
				break;
		}

		$data['languages'] = array_unique($data['languages']);
		return $data;
	}

}
