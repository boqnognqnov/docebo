<?php

/**
 * Created by PhpStorm.
 * User: asen
 * Date: 14-Jul-15
 * Time: 10:22 AM
 */
class NewCourseUnlockedInLearningPlan extends NotificationHandler implements INotificationHandler
{

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 * @param bool|string $params Optional parameters
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$courseModel = $this->eventParams['course'];
			$courseId = $courseModel->idCourse;
			$userModel = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			$targetUsers = $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseModel->idCourse));
			$targetCoursesForGetuserData = $targetCourses;
			$unlockedCourses = array();
			foreach ($targetCourses as $key => $targetCourse) {
				$coursesToBeUnlockedCommand = Yii::app()->db->createCommand()->select('lcpc.id_item, lcpc.id_path')->
				from(LearningCoursepathCourses::model()->tableName() . ' lcpc')->
				join(LearningCoursepathUser::model()->tableName() . ' lcpu', 'lcpu.id_path = lcpc.id_path and lcpu.idUser = :idUser', array(':idUser' => $userModel->idst))->
				where('lcpc.prerequisites LIKE :id', array(':id' => '%' . $targetCourse . '%'));

				$notificationPlans = $this->plansMask;
				if(is_array($notificationPlans) && $notificationPlans){
					$coursesToBeUnlockedCommand->andWhere('lcpc.id_path in ('.implode(',', $notificationPlans).') AND  lcpu.id_path in ('.implode(',', $notificationPlans).')');
				}

				$coursesToBeUnlocked = $coursesToBeUnlockedCommand->queryAll();
				if (!empty($coursesToBeUnlocked)) {
					foreach ($coursesToBeUnlocked as $data) {
						$singleUnlockedCourse = LearningCoursepathCourses::model()->findByAttributes(array(
							'id_item' => $data['id_item'],
							'id_path' => $data['id_path']
						));
						if(in_array($targetCourse, explode(',', $singleUnlockedCourse->prerequisites)))
						{
							foreach ($targetUsers as $user)
							{
								$isLocked = $singleUnlockedCourse->isLocked($user);
								if(is_array($isLocked))
									$isLocked = $isLocked[0];
								if (!$isLocked)
									$unlockedCourses[] = $singleUnlockedCourse->id_item;
							}
						}
					}
				}
			}
			sort($targetCourses);
			if (!empty($unlockedCourses) && !empty($targetUsers)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $unlockedCourses, self::PU_OF_USERSONLY);
			}
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					if (count($unlockedCourses) > 1) {
						foreach ($unlockedCourses as $targetCourse) {
							$userToClone = $recipCollection['users'][$index];
							$userToClone['course'] = $targetCourse;
							$recipCollection['users'][] = $userToClone;
						}
						unset($recipCollection['users'][$index]);
					} else {
						$recipCollection['users'][$index]['course'] = $unlockedCourses[0];
					}
					$recipCollection['users'][$index]['users'] = $targetUsers;
				}
			}
		}

		// "AFTER" ?
		elseif ($this->notification->schedule_type == CoreNotification::SCHEDULE_AFTER) {

			// ALL users/courses are candidates (flase, false)
			$targetUsers = false;
			$targetCourses = false;
			$usersCompletedCourse = array();

			$shiftNumber = $this->notification->schedule_shift_number;
			$shiftPeriod = $this->notification->schedule_shift_period;
			$shiftType = $this->notification->schedule_type;
			$periodsForSearching = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
			$completedCourses = Yii::app()->db->createCommand('SELECT
						 `lcu`.`idCourse`
						 FROM `learning_courseuser` `lcu`
						 JOIN `learning_coursepath_courses` lcpc
						 ON lcu.`idCourse`=lcpc.`id_item`
						 WHERE lcu.`date_complete` IS NOT NULL
						GROUP BY lcu.`idCourse` ')->queryColumn();
			foreach ($completedCourses as $singleCompletedCourse) {
				$delayedCoursesToBeUnlockedCommand = Yii::app()->db->createCommand()->
				select('lcpc.id_item,lcpc.delay_time,lcpc.delay_time_unit,lcpc.id_path  ')->
				from(LearningCoursepathCourses::model()->tableName() . ' lcpc')->
				where('lcpc.prerequisites LIKE :id', array(':id' => '%' . $singleCompletedCourse . '%'));

				$notificationPlans = $this->plansMask;
				if(is_array($notificationPlans) && $notificationPlans){
					$delayedCoursesToBeUnlockedCommand->andWhere('lcpc.id_path in ('.implode(',', $notificationPlans).')');
				}

				$coursesToBeUnlocked = $delayedCoursesToBeUnlockedCommand->queryAll();

				if (!empty($coursesToBeUnlocked)) {
					$ids = array();
					foreach ($coursesToBeUnlocked as $single) {
						$ids[] = $single['id_item'];
					}
					$completedCoursesWithDates = Yii::app()->db->createCommand('SELECT lcu.`idCourse`,lcu.`idUser`,lcu.`date_complete`
									FROM `learning_courseuser` lcu
									WHERE lcu.`idCourse` = '.$singleCompletedCourse.' AND lcu.`date_complete` IS NOT NULL')->queryAll();
					foreach ($coursesToBeUnlocked as $courseToUnlock) {
						$delay = strtotime('+' . $courseToUnlock['delay_time'] . ' ' . $courseToUnlock['delay_time_unit'], 0);
						foreach ($completedCoursesWithDates as $singleCourseCompleted) {
							$delaySingleUnlockedCourse = LearningCoursepathCourses::model()->findByAttributes(array(
								'id_item' => $courseToUnlock['id_item'],
								'id_path' => $courseToUnlock['id_path']
							));
							if($delaySingleUnlockedCourse && (in_array($singleCourseCompleted['idCourse'], explode(',', $delaySingleUnlockedCourse->prerequisites)) )){
								$unlockingTime = date('Y-m-d H:i:s', strtotime($singleCourseCompleted['date_complete']) + $delay);
								if ($unlockingTime >= $periodsForSearching[0] && $unlockingTime <= $periodsForSearching[1]) {
									$assignedUser = LearningCoursepathUser::model()->findByAttributes(
										array(
											'id_path'=>$courseToUnlock['id_path'],
											'idUser' => $singleCourseCompleted['idUser'],
										));
									if($assignedUser){
										// the course to be notificated is in required interval
										$targetCourses[] = $courseToUnlock['id_item'];
										$targetUsers[] = $singleCourseCompleted['idUser'];
										$usersCompletedCourse[] = array($singleCourseCompleted['idUser'], $courseToUnlock['id_item']);
									}
								}
							}
						}
					}
				}
			}

			if ($targetCourses && $targetUsers) {
				$targetCourses = array_unique($targetCourses);
				if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER){
					$recipCollection = $this->helperGetPowerUsers($targetUsers, $targetCourses, true);
				}elseif($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER) {
					$recipCollection = $this->helperGetUsersData(false,$targetCourses, array(LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR));
				}else{
					$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_BOTH);
				}
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						if (count($usersCompletedCourse) > 1) {
							if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER){
								$tempUser = $recipCollection['users'][$index];
								unset($recipCollection['users'][$index]);
								foreach ($usersCompletedCourse as $user) {
									if($recipient['idst'] == $user[0]){
										$tempUser['users'][] = $user[0];
										$recipCollection['users'][] = $tempUser;
										unset($tempUser['users']);
									}
								}
							}else{
								if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER) {
									$recipCollection['users'][$index]['users'][] = $recipCollection['users'][$index]['user'];
								} elseif($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_GODADMIN) {
									// add users and courses
									$tempUser = $recipCollection['users'][$index];
									unset($recipCollection['users'][$index]);
									foreach ($usersCompletedCourse as $user) {
										$tempUser['users'][] = $user[0];
										$tempUser['courses'][] = $user[1];
										$recipCollection['users'][] = $tempUser;
										unset($tempUser['users']);
									}
								}else{
									$tempUser = $recipCollection['users'][$index];
									unset($recipCollection['users'][$index]);
									foreach ($usersCompletedCourse as $user) {
										if(in_array($user[1],$tempUser['courses']) || $user[1] == $tempUser['courses']) {
											$tempUser['users'][] = $user[0];
											$recipCollection['users'][] = $tempUser;
											unset($tempUser['users']);
										}
									}
								}
							}
						} else {
							$recipCollection['users'][$index]['users'][] = $usersCompletedCourse[0][0];
						}
					}
					foreach ($recipCollection['users'] as $index => $recipient) {
						if(is_string($recipient['courses'])){
							$recipCollection['users'][$index]['course'] = $recipient['courses'];
						}
						if(is_array($recipient['courses'])){
							$recipCollection['users'][$index]['course'] = $recipient['courses'][0];
						}
						if(is_string($recipient['users'])){
							$recipCollection['users'][$index]['user'] = $recipient['users'];
						}
						if(is_array($recipient['users'])){
							$recipCollection['users'][$index]['user'] = $recipient['users'][0];
						}
					}
				}
			}
		}

		// check for duplicate entries and delete them
		$str = '';
		$oldStr = '';
		foreach ($recipCollection['users'] as $key => $user) {
			foreach ($user as $value) {
				if (is_array($value)) {
					$str .= $value[0];
				} else {
					$str .= $value;
				}
			}
			if ($oldStr == $str) {
				unset($recipCollection['users'][$key]);
			}
			$oldStr = $str;
			$str = '';
		}
		$recipCollection['users'] = array_values($recipCollection['users']);
		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}

	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false)
	{
		// change recipient data to be data from the learner, who completed the current course
		$recipientId = $recipient['users'][0];
		$user = CoreUser::model()->findByPk($recipientId);
		$recipient['idst'] = $recipientId;
		$recipient['username'] = substr($user->userid,1);
		$recipient['email'] = $user->email;
		$recipient['user'] = $recipientId;
		return parent::getShortcodesData($recipient, $metadata, $textFormat);
	}
}