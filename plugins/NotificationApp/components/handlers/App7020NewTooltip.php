<?php
class App7020NewTooltip extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$tooltipId = $this->eventParams['tooltipId'];
			$tooltipObject = App7020Tooltips::model()->findByPk($tooltipId);
			// Get event params and validate them
			$contentId = $tooltipObject->idContent;
			$contentObject = App7020Assets::model()->findByPk($contentId);
			$contentAuthorId = $contentObject->userId;
			$contentAuthor = CoreUser::model()->findByPk($contentAuthorId);
			$tooltipAuthor = CoreUser::model()->findByPk($tooltipObject->idExpert);

			$response->metadata['tooltipObject'] = $tooltipObject;
			$response->metadata['contentObject'] = $contentObject;
			$response->metadata['tooltipAuthor'] = $tooltipAuthor;

			$response->metadata['tooltipAuthorName'] = CoreUser::getForamattedNames($tooltipAuthor, " ");

			$users = App7020Assets::getAssetExperts($contentId);
			$users[] = $contentAuthor->attributes;

			$recipCollection = $this->getUsersDataFromArray($users);
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}

	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$tooltipObject = $metadata['tooltipObject'];
		$contentObject = $metadata['contentObject'];
		$tooltipAuthor = $metadata['tooltipAuthor'];

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				$tooltipBody = $tooltipObject->text;
				break;
			default:
				$tooltipBody = strip_tags($tooltipObject->text);
				break;
		}

		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = $contentObject->title;
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = Docebo::createApp7020AssetsViewNgrUrl($contentObject->id, true);
		$result[CoreNotification::SC_APP7020_TOOLTIP_OWNER] = $metadata['tooltipAuthorName'];
		$result[CoreNotification::SC_APP7020_TOOLTIP_BODY] = $tooltipBody;
		$result[CoreNotification::SC_APP7020_TOOLTIP_POSITION] = gmdate("i:s", $tooltipObject->durationFrom);

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel["idst"]);
			$users[$userModel["idst"]] = array(
				'idst' => $userModel["idst"],
				'user' => $userModel["idst"],
				'username' => trim($userModel["userid"], '/'),
				'email' => $userModel["email"],
				'language' => $language,
			);
			$languages[$language] = 1;
		}
		
		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}



	public function getSpecialProperties($recipient, $metadata = array()) {

		$authorName = (is_array($metadata) && isset($metadata['tooltipAuthorName']) ? $metadata['tooltipAuthorName'] : false);
		$userObject = (is_array($metadata) && isset($metadata['tooltipAuthor']) ? $metadata['tooltipAuthor'] : false);
		if (!empty($userObject)) {
			$useravatar = App7020Helpers::getUserAvatar($userObject->idst);
			if (is_array($useravatar) && isset($useravatar['avatar']) && !empty($useravatar['avatar'])) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
			} else {
				$avatarImg = '';
			}
		} else {
			$avatarImg = '';
		}

		return array(
			'author' => $authorName,
			'avatar' => $avatarImg
		);
	}


}
