<?php




class UserRemovedFromGroup extends NotificationHandler implements INotificationHandler    {
	
	/* @var $groupModel CoreGroup */
	/* @var $userModel CoreUser */

	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		// "AT" ?  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			
			$groupModel = isset($this->eventParams['group']) ? $this->eventParams['group'] : null;
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			
			if (!$userModel || !$groupModel) {
				return $recipCollection;
			}
			
			$targetUsers 	= array($userModel->idst);
			$targetGroups 	= $this->maskItems(self::MASK_GROUPS, array($groupModel->idst));
			
			if (!empty($targetGroups)) {
				
				// Manually add the USER recipient, because it is already deleted and won't be found by the other metod below
				if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) {
					$recipCollection['users'][] = array(
						'idst' 					=> $userModel->idst,
						'username'				=> trim($userModel->userid,'/'),
						'email'					=> $userModel->email,
						'language'				=> 'english',
						'timezone'				=> 'UTC',
						'date_format_locale'	=> 'en',
						'courses'				=> array(),
					);
					$recipCollection['languages'][] = 'english';
				}
				else {
					$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, false, self::PU_OF_USERSONLY);
				}
				
				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['group'] = $groupModel->idst;
						$recipCollection['users'][$index]['user']  = $userModel->idst;
					}
				}
				
				
			}
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];
		
		return $response;
		
		
		
	}
	
	
	/**
	 * We must use the original EVENT params to get the model in this case, because group is delete already
	 * @see NotificationHandler::getShortcodesData($recipient)
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
	
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
	
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$groupModel = isset($this->eventParams['group']) ? $this->eventParams['group'] : null;
			if ($groupModel) {
				$result[CoreNotification::SC_GROUPNAME] = $groupModel->relativeId($groupModel->relativeId());
			}
		}
	
		return $result;
	
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
