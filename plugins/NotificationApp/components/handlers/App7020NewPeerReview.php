<?php

class App7020NewPeerReview extends NotificationHandler implements INotificationHandler {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();
		$recipCollection = array('users' => array(), 'languages' => array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$reviewId = $this->eventParams['prId'];
			$reviewObject = App7020ContentReview::model()->findByPk($reviewId);
			$useravatar = App7020Helpers::getUserAvatar($reviewObject->idUser);
			if ($useravatar['avatar']) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div class="widget-app7020Avatar-container">'
					. '<span style="
						width: 42px;
						height: 42px;
						display: inline-block;
						position: relative;
						vertical-align: top;" '
					. 'class="small avatar-container">'
					. '<img style="
						width:42px;
						min-width:42px;
						height:42px;
						min-height:42px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				$avatarImg = '';
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				}
			}
			// Get event params and validate them
			$contentId = $reviewObject->idContent;
			$contentObject = App7020Content::model()->findByPk($contentId);
			$reviews = App7020ContentReview::model()->findAllByAttributes(array('idContent' => $reviewObject->idContent));
			$countReviews = count($reviews);
			$prevReview = $reviews[$countReviews - 2];

			if ($prevReview) {
				$datetime1 = strtotime($reviewObject->created);
				$datetime2 = strtotime($prevReview["created"]);
				$interval = abs($datetime2 - $datetime1);
				// "TIME DIFFERENCE IS LESS THAN 5 MINUTES NOTIFICATION WILL NOT BE EXECUTED!"
				$minutes = round($interval / 60);
				if (false) {
					exit;
				}
			}
			$prAuthor = CoreUser::model()->findByPk($contentObject->userId);
			$users = App7020Assets::getAssetExperts($contentId);
			$users[] = $prAuthor->attributes;
			$prOwnerAvatar = $avatar;
			$recipCollection = $this->getUsersDataFromArray($users);

			$response->metadata['prOwnerAvatar'] = $prOwnerAvatar;
			$response->metadata['prOwnerAvatarUrl'] = $avatarImg;
			$response->metadata['prOwnerName'] = CoreUser::getForamattedNames($reviewObject->idUser, " ");
			$response->metadata['peerReviewObject'] = $reviewObject;
			$response->metadata['contentObject'] = $contentObject;
			$response->metadata['contentUrl'] = Docebo::createApp7020AssetsPeerReviewNgrUrl($contentObject->id, true);;

			foreach ($recipCollection['users'] as $index => $recipient) {

				$recipCollection['users'][$index]['app7020_content_url'] = Docebo::createApp7020AssetsPeerReviewNgrUrl($contentObject->id, true);
				$recipCollection['users'][$index]['app7020_content_title'] = $contentObject->title;
				$recipCollection['users'][$index]['app7020_peer_review_body'] = $reviewObject->message;
				$recipCollection['users'][$index]['app7020_owner_peer_review'] = CoreUser::getForamattedNames($reviewObject->idUser, " ");
				$recipCollection['users'][$index]['app7020_recipient_email'] = $recipient["email"];
				$recipCollection['users'][$index]['first_name'] = $recipient["firstname"];
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}



	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		$contentObject = (is_array($metadata) && isset($metadata['contentObject']) ? $metadata['contentObject'] : false);
		$reviewObject = (is_array($metadata) && isset($metadata['peerReviewObject']) ? $metadata['peerReviewObject'] : false);

		if (empty($reviewObject)) {
			$reviewBody = '';
		} else {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$reviewBody = $reviewObject->message;
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$reviewBody = strip_tags($reviewObject->message);
					break;
			}
		}

		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				// retrieve HTML avatar
				$avatar = (is_array($metadata) && isset($metadata['prOwnerAvatar']) ? $metadata['prOwnerAvatar'] : '');
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				// images are not supported by these formats
				$avatar = '';
				break;
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_PEER_REVIEW_TEXT] = $reviewBody;
		$result[CoreNotification::SC_APP7020_PEER_REVIEW_AUTHOR] = (is_array($metadata) && isset($metadata['prOwnerName']) ? $metadata['prOwnerName'] : '');
		$result[CoreNotification::SC_APP7020_CONTENT_TITLE] = (!empty($contentObject) ? $contentObject->title : '');
		$result[CoreNotification::SC_APP7020_CONTENT_URL] = (is_array($metadata) && isset($metadata['contentUrl']) ? $metadata['contentUrl'] : '');
		$result[CoreNotification::SC_APP7020_REVIEW_OWNER_AVATAR] = $avatar;
		$result[CoreNotification::SC_APP7020_RECIPIENT_EMAIL] = $recipient['app7020_recipient_email'];
		$result[CoreNotification::SC_APP7020_RECIPIENT_FIRST_NAME] = $recipient['first_name'];

		return $result;
	}




	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['prOwnerName']) ? $metadata['prOwnerName'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['prOwnerAvatarUrl']) ? $metadata['prOwnerAvatarUrl'] : '')
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel) {
			$language = $this->getUserLanguageCode($userModel["idst"]);
			$users[$userModel["idst"]] = array(
				'idst' => $userModel["idst"],
				'user' => $userModel["idst"],
				'username' => trim($userModel["userid"], '/'),
				'email' => $userModel["email"],
				'language' => $language,
				'firstname' => $userModel['firstname'],
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}

}
