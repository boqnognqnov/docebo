<?php

class ILTSessionStarting extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {

			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;
			$shiftType 		= $this->notification->schedule_type;
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);
			// Get all enrollments related to target users/courses, filtered according to the after/before settings.
			$enrollments = $this->getILTSessionsBeforeAfterData($targetUsers, $targetCourses, self::ILT_SESSION_STARTING, $shiftType, $shiftNumber, $shiftPeriod);

			if (is_array($enrollments) && empty($enrollments['enrollments'])) return $response;

			$targetSessionsId = array();
			foreach ($enrollments['enrollments'] as $enrollment) {
				$targetSessionsId[] = $enrollment['id_session'];
			}
			if(!$targetSessionsId) return $recipCollection;

			// This notification has something to add to the basic recipients information (taken out from enrollment data)
			$relatedFields = array('course', 'id_session');

			// Collect an array of recipients; basically an array of items which will then be transformed into an email.
			// With that said, the number of items in this array is equal to the number of emails going to be sent by the parent method.
			$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_COURSESONLY, $relatedFields);

			if ($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER || $this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER) {
				if(is_array($targetUsers) && empty($targetUsers))
					return $response;
				if(is_array($targetCourses) && empty($targetCourses))
					return $response;

				if($this->notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_USER)
					$levels = array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				else
					$levels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);

				$recipCollection = array('users'=> array(), 'languages'=> array());
				foreach($targetSessionsId as $sessionId) {
					$sessionModel = LtCourseSession::model()->findByPk($sessionId);
					$targets = $this->helperGetUsersData(false, array($sessionModel->course_id), $levels, false, array($sessionId), true, true);
					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
							'course' 		=> $sessionModel->course_id,
							'id_session' 	=> $sessionId,
						);
						$recipCollection['users'][] = $tmp;
					}
					$recipCollection['languages'] = array_merge($recipCollection['languages'], $targets['languages']);
				}
			}
		}

		foreach ($targetSessionsId as $key => $sessionId) {
			$sessionModel = LtCourseSession::model()->findByPk($sessionId);
			$ics = new ICSManager(ICSManager::TYPE_REQUEST, $sessionModel->date_begin, $sessionModel->date_end, $sessionModel->name, $sessionModel->other_info, $sessionModel->icsLocation(), $this->notification->from_name, $this->notification->from_email, $sessionModel);
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					if ($recipient['id_session'] == $sessionModel->id_session) {
						$recipCollection['users'][$index]['icsManager'] = $ics;
					}
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 *
	 * @param boolean $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
