<?php



class LiveCoachSessionStarts extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// Restrict running for allowed only (in this case it is after and before the event)
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_ONLY_BEFORE)) {

			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;
			$shiftType 		= $this->notification->schedule_type;

			// ALL users/courses are candidates (false, false), because we are going to observe all enrollments,
			// Of course masked/filtered by assigned users & courses.
			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetCourses 	= $this->maskItems(self::MASK_COURSES, false);

			// Get all enrollments related to target users/courses, feltered according to the after/before settings.
			$enrollments = $this->getCoachWebinarSessionsInivationsBeforeDate($targetUsers, $targetCourses, $shiftType, $shiftNumber, $shiftPeriod, $this->notification->recipient);

			// This notification has something to add to the basic recipients information (taken out from enrollment data)
			$relatedFields = array('course', 'coach_name','coach_email','coaching_session_dates','coaching_webinar_date_begin','coaching_webinar_date_end','coaching_webinar_name','coaching_webinar_url');

			if (!empty($enrollments['enrollments'])) {
				foreach ($enrollments['enrollments'] as $index => $recipient) {
					$webinarSessionModel = LearningCourseCoachingWebinarSession::model()->findByPk($enrollments['enrollments'][$index]['id_session']);

					$webinar = WebinarTool::getById($webinarSessionModel->tool);
					$data = $webinar->getAttendeeInfoForRoom($webinarSessionModel->id_tool_account, CJSON::decode($webinarSessionModel->tool_params));
					$details = $this->formatWebinarSessionDetails($data);

					// Filter only recipients of type users
					$enrollments['enrollments'][$index]['coaching_webinar_meeting_details']   = $details;
				}
			}

			// Collect an array of recipients; basically an array of items which will then be transformed into an email.
			// With that said, the number of items in this array is equal to the number of emails going to be sent by the parent method.
			$recipCollection = $this->helperGetRecipientsFromEnrollments($enrollments, $this->notification->recipient, self::PU_OF_USERSONLY, $relatedFields);
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;

	}

	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * getShortcodesData() - sets the custom short codes
	 * @param array $recipient - array with recipients data
	 * @return array - array with shortcodes and values to be replaced
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result =  parent::getShortcodesData($recipient, $metadata, $textFormat);
		/********- Set custom ['shortcodes'] -********/
		$result[CoreNotification::SC_COACH_NAME]	            = $recipient['coach_name'];
		$result[CoreNotification::SC_COACH_EMAIL]	            = $recipient['coach_email'];
		$result[CoreNotification::SC_COACH_SESSION_DATES]	    = $recipient['coaching_session_dates'];
		$result[CoreNotification::SC_COACH_WEBINAR_DATE_BEGIN]	= $recipient['coaching_webinar_date_begin'];
		$result[CoreNotification::SC_COACH_WEBINAR_DATE_END]	= $recipient['coaching_webinar_date_end'];
		$result[CoreNotification::SC_COACH_WEBINAR_NAME]	    = $recipient['coaching_webinar_name'];
		$result[CoreNotification::SC_COACH_WEBINAR_URL]	        = $recipient['coaching_webinar_url'];
		$result[CoreNotification::SC_COACH_WEBINAR_DETAILS]		= $recipient['coaching_webinar_meeting_details'];

		return $result;
	}

}
