<?php
class SubPlanExpired extends NotificationHandler implements INotificationHandler {
	
	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
		// Restrict running for allowed only (in this case it is after and before the event)
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {
			$recipients = $this->notification["recipient"];
			$shiftNumber 	= $this->notification->schedule_shift_number;
			$shiftPeriod 	= $this->notification->schedule_shift_period;
			$shiftType 		= $this->notification->schedule_type;
			$expiringPlan = $this->getSubPlansWithEventAfterBeforePeriod($shiftType, $shiftNumber, $shiftPeriod);
			if($expiringPlan) {
				$planId = $expiringPlan["id"];
				// Get all expiring record
				$response->metadata['purchase_date'] = $expiringPlan['startDate'];
				$response->metadata['expiration_date'] = $expiringPlan['endDate'];
				$response->metadata['transaction_code'] = $expiringPlan['code'];
				$response->metadata['bundle_name'] = $expiringPlan["name"];
				$response->metadata['plan_name'] = $expiringPlan["planName"];
				$response->metadata['type'] = ($expiringPlan['type'] == '1') ? "Seats" : "License";
				$response->metadata['capacity'] = $expiringPlan["capacity"];
				$response->metadata['price'] = $expiringPlan["price"];
				$response->metadata['note'] = $expiringPlan["note"];
				$response->metadata['sold_to'] = $expiringPlan["sold_to"];
				$relatedFields = array();
				$recordModel = Yii::app()->db->createCommand("select * from sub_subscription_records WHERE id='$planId'")->queryAll();
				$userIds = $this->getRecipientUsers($recordModel,$recipients);
				$userModel = new CoreUser();
				$users = $userModel->findAllByAttributes(['idst' => $userIds]);
				
				$recipCollection = $this->getUsersDataFromArray($users);

				$response->recipients = $recipCollection['users'];
				$response->languages = $recipCollection['languages'];

				return $response;
			}
		}
	}
	/**
	 */
	protected function getSubPlansWithEventAfterBeforePeriod($shiftType=false, $shiftNumber=false, $shiftPeriod=false) {
		$params = array();
		$select = "*";
		list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
		$command = Yii::app()->getDb()->createCommand();
		$command->select('sub_records.* ,sub_plans.idSubscription,sub_subscriptions.name as planName');
		$command->from('sub_subscription_records sub_records');
		$command->join('sub_subscription_plans sub_plans','sub_records.idPlan = sub_plans.id');
		$command->join('sub_subscriptions','sub_subscriptions.id = sub_plans.idSubscription');
		if ($targetTimeFrom) {
			$command->andWhere('sub_records.endDate>=:timeFrom', array(':timeFrom' => $targetTimeFrom));
		}
		if ($targetTimeTo) {
			$command->andWhere('sub_records.endDate<=:timeTo', array(':timeTo' => $targetTimeTo));
		}
		
		// If "after/before event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
		return $command->queryRow();
	}	
	
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$purchaseDate=date_create($metadata['purchase_date']);
		$expirationDate=date_create($metadata['expiration_date']);
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PURCHASE_DATE] = date_format($purchaseDate,"Y/m/d H:i:s a");
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_TRANSACTION_CODE] = $metadata['transaction_code'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_EXPIRATION_DATE] = date_format($expirationDate,"Y/m/d H:i:s a");
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_BUNDLE_NAME] = $metadata['bundle_name'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_TYPE] = $metadata['type'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_CAPACITY] = $metadata['capacity'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PRICE] = $metadata['price'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_NOTE] = $metadata['note'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_PLAN_NAME] = $metadata['plan_name'];
		$result[CoreNotification::SC_SUBSCRIPTION_RECORD_SOLD_TO] = $metadata['sold_to'];

		return $result;
	}
	protected function getRecipientUsers($idRecord,$recipients) {
		/* get all god admins */
		$admins = array();
		$admins = Yii::app()->getDb()->createCommand();
		$admins->select(['cgm.idstMember']);
		$admins->from(CoreGroup::model()->tableName() . ' cg');
		$admins->join(CoreGroupMembers::model()->tableName() . ' cgm', 'cg.idst = cgm.idst');
		$admins->andWhere('groupid = :groupId ',array(':groupId'=>CoreGroup::GROUP_GODADMINS));
		$admins = $admins->queryColumn();
		/* get all power users */
			$PU = array();
			$PU = Yii::app()->getDb()->createCommand();
			$PU->select("cat.idstAdmin");
			$PU->from(CoreAdminTree::tableName() . ' cat ');
			$PU->join(CoreOrgChartTree::tableName() . ' cot', 'cat.idst = cot.idst_oc');
			$PU->where(['cot.idOrg' => $objectId]);
			$PU = $PU->queryColumn();
		$record = array();
		$record = Yii::app()->getDb()->createCommand();
		$record->select(['idObject']);
		$record->from("sub_subscription_records");
		$record->andWhere("id = :idRecord",array(":idRecord"=>$recordModel["id"]));
		
		switch ($recipients) {
			case "user": // BRANCH
				$record->andWhere("objectType = 3");
				$record = $record->queryColumn();
				return array_merge_recursive($admins,$record);
				break;
			case "poweruser": // GROUP
				return array_merge_recursive($admins,$PU);
			case "godadmin":
				return $admins;
		}

		if($idBranch) {

			$recipients = array_merge($admins,$PU);
			return $recipients;
		}
		return $admins;
		
	}
	
	
	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
	
	
	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userId) {
			$language = $this->getUserLanguageCode($userId['idst']);
			$users[$userId['idst']] = array(
				'idst' => $userId['idst'],
				'user' => $userId['idst'],
				'username' => trim($userId['userid'], '/'),
				'email' => $userId['email'],
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}
	
	
	
	
}
