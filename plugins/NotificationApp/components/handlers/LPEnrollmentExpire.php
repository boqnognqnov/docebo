<?php
class LPEnrollmentExpire extends NotificationHandler implements INotificationHandler {

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData( $params = FALSE ) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" not supported for this Notification type
		if (in_array($this->notification->schedule_type, CoreNotification::$SCHEDULE_PRESET_NO_AT)) {

			$targetUsers 	= $this->maskItems(self::MASK_USERS, false);
			$targetPlans	= $this->maskItems(self::MASK_PLANS, false);

			if($targetPlans === false)
				$targetPlans = Yii::app()->db->createCommand("SELECT id_path FROM ".LearningCoursepath::model()->tableName())->queryColumn();

			// No target plan ? get out!
			if (empty($targetPlans)) {
				return $recipCollection; // empty
			}

			// Get all user's expiring enrollments inside LPs in the interval
			// for which this single notification is valid
			$shiftPeriod = $this->getTargetPeriodForShifted($this->notification->schedule_type, $this->notification->schedule_shift_number, $this->notification->schedule_shift_period);

			$startUTC = $shiftPeriod[0];
			$endUTC = $shiftPeriod[1];

			// Get recipients:
			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					$recipCollection = $this->helperGetAllGodAdmins();

					$command = Yii::app()->getDb()->createCommand()
							->select(array(
									'user.idst AS user',
									't.id_path AS plan',
									't.date_begin_validity',
									't.date_end_validity',
							))
							->from(LearningCoursepathUser::model()->tableName().' t')
							->join( CoreUser::model()->tableName() . ' user', 't.idUser=user.idst' )
							->where('t.date_end_validity > :from AND t.date_end_validity < :to', array(
									':from'=>$startUTC,
									':to'=>$endUTC,
							));

					if($targetUsers===false){
						// All users are candidates
					}elseif(is_array($targetUsers)){
						$command->andWhere(array('IN', 'user.idst', $targetUsers));
					}else{
						$command->andWhere('1=0'); // invalid users array, prevent any results
					}

					if($targetPlans===false){
						// All LPs are candidates
					}elseif(is_array($targetPlans)){
						$command->andWhere(array('IN', 't.id_path', $targetPlans));
					}else{
						$command->andWhere('1=0'); // invalid LPs array, prevent any results
					}

					$toBeCopied = true;
					$backupOfRecipients = array();
					$query = $command->queryAll();

					foreach ($query as $recipientRowFromDb) {
						if(!empty($backupOfRecipients)){
							foreach ($backupOfRecipients as $backedUpRecipient) {
								$backedUpRecipient['user'] = $recipientRowFromDb['user'];
								$backedUpRecipient['plan'] = $recipientRowFromDb['plan'];
								$backedUpRecipient['date_begin_validity'] = $recipientRowFromDb['date_begin_validity'];
								$backedUpRecipient['date_end_validity'] = $recipientRowFromDb['date_end_validity'];
								$recipCollection['users'][] = $backedUpRecipient;
							}
						}else {
							foreach ($recipCollection['users'] as &$recipient) {
								$recipient['user'] = $recipientRowFromDb['user'];
								$recipient['plan'] = $recipientRowFromDb['plan'];
								$recipient['date_begin_validity'] = $recipientRowFromDb['date_begin_validity'];
								$recipient['date_end_validity'] = $recipientRowFromDb['date_end_validity'];
							}
							if ($toBeCopied) {
								$backupOfRecipients = $recipCollection['users'];
								$toBeCopied = false;
							}
						}
					}
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:

					$command = Yii::app()->getDb()->createCommand()
						->select(array(
							'user.idst AS user',
							'user.userid AS username',
							'user.email AS email',
							't.id_path AS plan',
							't.date_begin_validity',
							't.date_end_validity',
							'csu_lang.value AS language',
							'csu_timezone.value AS timezone',
							'csu_format_locale.value AS date_format_locale',
						))
						->from(LearningCoursepathUser::model()->tableName().' t')
						->join( CoreUser::model()->tableName() . ' user', 't.idUser=user.idst' )
						->where('t.date_end_validity > :from AND t.date_end_validity < :to', array(
							':from'=>$startUTC,
							':to'=>$endUTC,
						));
					// Join user settings table to get the user's "language"
					$command->leftJoin( 'core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')" );

					// Join user settings table to get the user's "timezone"
					$command->leftJoin( 'core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')" );

					// Join user settings table to get the user's "date_format_locale"
					$command->leftJoin( 'core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')" );
					if($targetUsers===false){
						// All users are candidates
					}elseif(is_array($targetUsers)){
						$command->andWhere(array('IN', 'user.idst', $targetUsers));
					}else{
						$command->andWhere('1=0'); // invalid users array, prevent any results
					}

					if($targetPlans===false){
						// All LPs are candidates
					}elseif(is_array($targetPlans)){
						$command->andWhere(array('IN', 't.id_path', $targetPlans));
					}else{
						$command->andWhere('1=0'); // invalid LPs array, prevent any results
					}

					$query = $command->queryAll();

					foreach($query as $recipientRowFromDb){
						$completed = LearningCoursepath::model()->getCoursepathPercentage($recipientRowFromDb['user'],  $recipientRowFromDb['plan'] );
						if((isset($completed['percentage']) && $completed['percentage'] != 100) || !isset($completed['percentage'])){
							$recipCollection['users'][$recipientRowFromDb['user']] = array(
								'idst' => $recipientRowFromDb['user'],
								'user' => $recipientRowFromDb['user'],
								'username' => trim($recipientRowFromDb['username'],'/'),
								'email' => $recipientRowFromDb['email'],
								'language' => $recipientRowFromDb['language'],
								'plan' => $recipientRowFromDb['plan'],
								'date_begin_validity' => $recipientRowFromDb['date_begin_validity'],
								'date_end_validity' => $recipientRowFromDb['date_end_validity'],
								'timezone' => $recipientRowFromDb['timezone'],
								'date_format_locale' => $recipientRowFromDb['date_format_locale'],
							);
							$recipCollection['languages'][] = $recipientRowFromDb['language'];
						}

					}
					$recipCollection['languages'] = array_unique($recipCollection['languages']);

					break;
				default: return $recipCollection;
			}

		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}}