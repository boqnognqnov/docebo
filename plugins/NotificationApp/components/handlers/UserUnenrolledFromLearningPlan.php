<?php
class UserUnenrolledFromLearningPlan extends NotificationHandler implements INotificationHandler {

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData( $params = FALSE ) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// "AT" ?
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$learningPlanModel  = isset($this->eventParams['learningPlan']) ? $this->eventParams['learningPlan'] : null;
			$userModel 			= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			if (!$learningPlanModel || !$userModel) {
				return $recipCollection; // none
			}

			$targetUsers 	= $this->maskItems(self::MASK_USERS, array($userModel->idst));
			$targetPlans	= $this->maskItems(self::MASK_PLANS, array($learningPlanModel->id_path));

			// No target plan ? get out!
			if (empty($targetPlans)) {
				return $recipCollection; // empty
			}

			// Get courses from the plan
			$planCourses = $learningPlanModel->getCoursesIdList();
			$stopFilteringUsers=true;
			$studentsOnly=false;
			$statuses=false;
			$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $planCourses, self::PU_OF_USERSONLY, $studentsOnly, $statuses, $stopFilteringUsers);

			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['plan'] = $learningPlanModel->id_path;
					$recipCollection['users'][$index]['user'] = $userModel->idst;
					$recipCollection['users'][$index]['completed_at'] = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
				}
			}

		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}}