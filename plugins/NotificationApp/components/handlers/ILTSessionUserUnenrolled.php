<?php

class ILTSessionUserUnenrolled extends NotificationHandler implements INotificationHandler {


	public function getRecipientsData( $params = FALSE ) {

		$response             = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection      = array( 'users' => array(), 'languages' => array() );

		// ONLY "AT"
		if ( $this->notification->schedule_type == CoreNotification::SCHEDULE_AT ) {

			$session_id = isset( $this->eventParams['session'] ) ? $this->eventParams['session'] : NULL;
			$user_id       = isset( $this->eventParams['user'] ) ? $this->eventParams['user'] : FALSE;
			$level     = isset( $this->eventParams['level'] ) ? $this->eventParams['level'] : NULL;
			$sessionModel = LtCourseSession::model()->findByPk( $session_id );
			if (!$sessionModel)
				return $response;

			$targetUsers   = $this->maskItems( self::MASK_USERS, array( $user_id ) );
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $sessionModel->course_id ) );

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$recipCollection = $this->helperGetAllGodAdmins();
					foreach($recipCollection['users'] as &$godadmin){
						$godadmin['user'] = $user_id;
					}
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$studentsIdsts = Yii::app()->getDb()->createCommand()
						->select('idst')
						->from(CoreUser::model()->tableName())
						->where(array('IN', 'idst', $targetUsers))
						->queryColumn();
					$levels = array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
					if(!$level || ($level && in_array($level, $levels))){
						$recipCollection = $this->helperGetUsersData($studentsIdsts, false, $levels, false, false, false);
					}

					break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:

					// Get recipients details
					$recipCollection = $this->helperGetPowerUsers(array($user_id), $targetCourses, true, false);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					$recipCollection = $this->getTeachersDataFromUser($user_id, $session_id);
					$levels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
					);
					if($level && in_array($level, $levels) && $user_id){
						$instructorCollection = $this->helperGetUsersData(array($user_id => $user_id), false, $levels, false, false, false);
						if(isset($instructorCollection['users'][0]) && !empty($instructorCollection['users'][0])){
							$recipCollection['users'][] = $instructorCollection['users'][0];
						}
						if(isset($instructorCollection['languages'][0]) && !empty($instructorCollection['languages'][0])
							&& is_array($recipCollection['languages']) && !in_array($instructorCollection['languages'][0], $recipCollection['languages'])){
							$recipCollection['languages'][] = $instructorCollection['languages'][0];
						}
					}

					break;
			}

			$ics = new ICSManager(ICSManager::TYPE_CANCEL, $sessionModel->date_begin, $sessionModel->date_end,$sessionModel->name, $sessionModel->other_info, $sessionModel->icsLocation(), $this->notification->from_name, $this->notification->from_email, $sessionModel);
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][ $index ]['course']     = $sessionModel->course_id;
					$recipCollection['users'][ $index ]['id_session'] = $sessionModel->id_session;
					$recipCollection['users'][ $index ]['icsManager'] = $ics;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages  = $recipCollection['languages'];

		return $response;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();

		return $descriptor;
	}
}
