<?php

class NewILTSessionCreated extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$session_id = isset($this->eventParams['session_id']) ? $this->eventParams['session_id'] : null;
			$sessionModel = LtCourseSession::model()->findByPk($session_id);

			if (!$sessionModel)
				return $recipCollection;

			$targetUsers = false;
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $sessionModel->course_id ) );

			if (!empty($targetCourses)) {
				// If we are entering this branch then the input course is included in the notification courses.
				// Otherwise it is not and we just leave the output empty.

				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient, $targetUsers, $targetCourses, self::PU_OF_COURSESONLY, true);

				$ics = new ICSManager(ICSManager::TYPE_REQUEST, $sessionModel->date_begin, $sessionModel->date_end, $sessionModel->name, $sessionModel->other_info, $sessionModel->icsLocation(), $this->notification->from_name, $this->notification->from_email, $sessionModel);
				// ADD RELATED INFO
				if (!empty($recipCollection['users'])) {
					foreach ($recipCollection['users'] as $index => $recipient) {
						$recipCollection['users'][$index]['course'] = $sessionModel->course_id;
						$recipCollection['users'][$index]['id_session'] = $sessionModel->id_session;
						$recipCollection['users'][$index]['icsManager'] = $ics;
					}
				}
			}

		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}


	/**
	 *
	 * @param boolean $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
