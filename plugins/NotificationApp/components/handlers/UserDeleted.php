<?php
/**
 * 
 * 
 *
 */
class UserDeleted extends NotificationHandler implements INotificationHandler    {
	
	/* @var $userModel CoreUser */
	
	public function getRecipientsData($params=false) {
		
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());
				
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Recipient for "User deleted" is possible for God Admin only. Everything else is non-sense because USER is already deleted
			// and we cannot extract info about Instructors, Power Users && users itself
			if ($this->notification->recipient != CoreNotification::NOTIFY_RECIPIENTS_GODADMIN) {
				return $recipCollection;
			}
			
			$userModel 	= isset($this->eventParams['user']) ? $this->eventParams['user'] : null;

			if (!$userModel) {
				return $recipCollection;
			}

			$userMask = ($this->notification->ufilter_option == CoreNotification::USER_NO_USER_FILTER)?$userModel->idst:false;
			$targetUsers 	= $this->maskItems(self::MASK_USERS, $userMask);
			$targetCourses 	= false;
			
			if (!empty($targetUsers)) {
				$recipCollection = $this->helperGetRecipientsFromUsersData($this->notification->recipient);
			}
			
			// ADD RELATED INFO
			if (!empty($recipCollection['users'])) {
				foreach ($recipCollection['users'] as $index => $recipient) {
					$recipCollection['users'][$index]['user'] = $userModel->idst;
				}
			}
				
		}
		
		$response->recipients 	= $recipCollection['users'];  
		$response->languages 	= $recipCollection['languages'];

		return $response;
		
		
		
	}

	
	/**
	 * In this case we override the parent method because when it is called (the parrent method) the user will be already deleted and user model is gone
	 *  
	 * @see NotificationHandler::getShortcodesData()
	 */
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$userModel = isset($this->eventParams['user']) ? $this->eventParams['user'] : null;
			if ($userModel) {
				$result[CoreNotification::SC_USERNAME]		= ltrim($userModel->firstname, '/');
				$result[CoreNotification::SC_FIRSTNAME]		= $userModel->firstname;
				$result[CoreNotification::SC_LASTNAME] 		= $userModel->lastname;
				$result[CoreNotification::SC_USER_EMAIL] 	= $userModel->email;
			}
		}
		
		return $result;
		
	}
	
	/**
	 *
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
}
