<?php

class StudentFailedILTSession extends NotificationHandler implements INotificationHandler {

	public function getRecipientsData( $params = FALSE ) {
		$response             = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection      = array( 'users' => array(), 'languages' => array() );

		// ONLY "AT"  
		if ( $this->notification->schedule_type == CoreNotification::SCHEDULE_AT ) {
			/* @var $userSessionModel LtCourseuserSession */
			$userSessionModel = isset( $this->eventParams['userSessionModel'] ) ? $this->eventParams['userSessionModel'] : NULL;
			if(!$userSessionModel)
				return $response;

			$user = $userSessionModel->idUser;
			$sessionModel = $userSessionModel->ltCourseSession;
			$targetUsers   = $this->maskItems( self::MASK_USERS, (array) $user->idst );
			$targetCourses = $this->maskItems( self::MASK_COURSES, array( $sessionModel->course_id ) );

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					if(is_array($targetCourses) && empty($targetCourses))
						return $response;
					$recipCollection = $this->helperGetAllGodAdmins();
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					if((is_array($targetUsers) && empty($targetUsers)) || (is_array($targetCourses) && empty($targetCourses)))
						return $response;

					$command = Yii::app()->getDb()->createCommand()
						->select( array(
							'user.idst AS user',
							'user.idst AS idst',
							'user.email AS email',
							'sess.id_session AS id_session',
							'course.course_id AS course',
							'csu_lang.value AS language',
							'csu_timezone.value AS timezone',
							'csu_format_locale.value AS date_format_locale',
						) )
						->from( LtCourseuserSession::model()->tableName() . ' sess' )
						->join( CoreUser::model()->tableName() . ' user', 'sess.id_user=user.idst' )
						->join( LtCourseSession::model()->tableName() . ' course', 'sess.id_session=course.id_session' )
						->andWhere( array( 'IN', 'user.idst', $targetUsers ) )
						->andWhere( array( 'IN', 'course.course_id', $targetCourses ) )
						->group( 'user.idst' );

					// Join user settings table to get the user's "language"
					$command->leftJoin( 'core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')" );

					// Join user settings table to get the user's "timezone"
					$command->leftJoin( 'core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')" );

					// Join user settings table to get the user's "date_format_locale"
					$command->leftJoin( 'core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')" );

					$query = $command->queryAll();

					foreach ( $query as $row ) {
						$recipCollection['users'][] = $row;
						if(!empty($row['language']))
							$recipCollection['languages'][] = $row['language'];
					}
					$recipCollection['languages'] = array_unique($recipCollection['languages']);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
					// Get the PUs/managers of the users for which we raised this event/notification
					$puserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPU::model()->tableName())
						->where(array('IN', 'user_id', $targetUsers))
						->queryColumn();
					// Make sure that the notification is being sent to PUs owning the course
					$coursesPuserIds = Yii::app()->getDb()->createCommand()
						->select('puser_id')
						->from(CoreUserPuCourse::model()->tableName())
						->where(array('IN', 'course_id', $targetCourses))
						->queryColumn();
					if (!is_array($coursesPuserIds)) { $coursesPuserIds = array(); }
					if (!is_array($puserIds)) { $puserIds = array(); }
					$puserIds = array_intersect($puserIds, $coursesPuserIds);
					// Get recipients details
					$recipCollection = $this->helperGetUsersData($puserIds, false, false);
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
					// Now get INSTRUCTORS (data) of all these courses
					$instructorLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR
					);
					$instructorsIdsts = array();
					$cmd = Yii::app()->db->createCommand()
						->select("cus.id_user")
						->from(LtCourseuserSession::model()->tableName()." cus")
						->join(LtCourseSession::model()->tableName()." cs", "cs.id_session = cus.id_session")
						->join(LearningCourseuser::model()->tableName()." cu", "cu.idCourse = cs.course_id AND cu.idUser = cus.id_user")
						->where(array( 'IN', 'cs.course_id', $targetCourses ))
						->andWhere(array( 'IN', 'cu.level', $instructorLevels ))
						->andWhere("cus.id_session = :id_session", array(':id_session' => $sessionModel->id_session));
					$reader = $cmd->query();

					if ($reader) {
						while ($row = $reader->read()) {
							$instructorsIdsts[] = $row['id_user'];
						}
					}
					$recipCollection = $this->helperGetUsersData($instructorsIdsts);
					break;
			}

			if ( ! empty( $recipCollection['users'] ) ) {
				foreach ( $recipCollection['users'] as $index => $recipient ) {
					$recipCollection['users'][ $index ]['course']     = $sessionModel->course_id;
					$recipCollection['users'][ $index ]['id_session'] = $sessionModel->id_session;
					$recipCollection['users'][ $index ]['user'] = $user->idst;
				}
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages  = $recipCollection['languages'];
		return $response;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 *
	 * @param boolean $params
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
}
