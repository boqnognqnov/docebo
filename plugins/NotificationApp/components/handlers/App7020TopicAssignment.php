<?php
class App7020TopicAssignment extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			// Get event params and validate them
			$assignedExpertId = $this->eventParams['assignedExpertId'];
			$assignedExpert = CoreUser::model()->findByPk($assignedExpertId);
			$superAdminId = $this->eventParams['superAdminId'];
			$superAdmin = CoreUser::model()->findByPk($superAdminId);
			$topicId = $this->eventParams['topicId'];
			$topicObject = App7020TopicTranslation::model()->findByAttributes(array('idTopic' => $topicId, 'language' => Yii::app()->getLanguage()));
			$users = array($assignedExpert);

			$recipCollection = $this->getUsersDataFromArray($users);

			$response->metadata['topicObject'] = $topicObject;
			$response->metadata['adminName'] = trim(trim($superAdmin->firstname)." ".trim($superAdmin->lastname));
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$topicObject = (is_array($metadata) && isset($metadata['topicObject']) ? $metadata['topicObject'] : false);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_TOPIC_NAME] = (!empty($topicObject) ? $topicObject->text : '');
		$result[CoreNotification::SC_APP7020_SUPER_ADMIN] = (is_array($metadata) && isset($metadata['adminName']) ? $metadata['adminName'] : '');

		return $result;
	}


	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] =array(
				'idst' 			=> $userModel->idst,
				'user'          => $userModel->idst,
				'username'		=> trim($userModel->userid,'/'),
				'email'			=> $userModel->email,
				'language'		=> $language,
			);
			$languages[$language] = 1;
		}
		
		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}
}
