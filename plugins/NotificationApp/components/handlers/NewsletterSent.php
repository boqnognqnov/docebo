<?php

class NewsletterSent extends NotificationHandler implements INotificationHandler    {


	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$corenewsletterId = isset($this->eventParams['newsletter']) ? $this->eventParams['newsletter'] : null;
			$newsletterModel = CoreNewsletter::model()->findByPk($corenewsletterId);

			if (!$newsletterModel)
				return $recipCollection;

			$usersRows = Yii::app()->getDb()->createCommand()
				->select(array(
					'user.idst AS idst',
					'user.idst AS user',
					'user.userid AS username',
					'user.email AS email',
					'u1.value AS language',
					'u2.value AS timezone'
				))
				->from(CoreNewsletterSendto::model()->tableName().' nRecip')
				->join(CoreUser::model()->tableName().' user', 'user.idst=nRecip.idst AND nRecip.id_send=:idNewsletter', array(':idNewsletter'=>$corenewsletterId))
				->leftJoin(CoreSettingUser::model()->tableName().' u1', 'user.idst=u1.id_user AND u1.path_name="ui.language"')
				->leftJoin(CoreSettingUser::model()->tableName().' u2', 'user.idst=u2.id_user AND u2.path_name="timezone"')
				->group('user.idst');

			switch($this->notification->recipient){
				case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
					$recipCollection = $this->helperGetAllGodAdmins();
					break;
				case CoreNotification::NOTIFY_RECIPIENTS_USER:
					// All users who are selected as recipients to the Notification
					// without regards to their level in the LMS
					$usersRows = $usersRows->queryAll();

					$recipCollection = array('users'=>array(), 'languages'=>array());
					foreach($usersRows as $tmpRow){
						$recipCollection['users'][] = $tmpRow;
						$recipCollection['languages'][] = $tmpRow['language'];
					}

					$recipCollection['languages'] = array_unique($recipCollection['languages']);
					break;
			}
		}

		$response->recipients = $recipCollection['users'];
		$response->languages = $recipCollection['languages'];

		return $response;
	}


	/**
	 * @see INotificationHandler::allowedScheduleTypes()
	 */
	public function allowedScheduleTypes() {
		return CoreNotification::$SCHEDULE_PRESET_ONLY_AT;
	}

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor( $params = FALSE ) {
		return new NotificationDescriptor();
	}
}
