<?php
class App7020MentionedAnswer extends NotificationHandler implements INotificationHandler    {

	/**
	 * LOGIC:
	 * Send notification to
	 * - GodAdmins
	 * - PU
	 * - Course Instructors
	 * if they pass the Users filter
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false) {

		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users'=> array(), 'languages'=> array());

		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {

			$mentionedIds = $this->eventParams['mentionedIds'];
			$questionId = $this->eventParams['questionId'];
			$mentionedByUserId = $this->eventParams['mentionedByUserId'];

			$mentionedBy = CoreUser::model()->findByPk($mentionedByUserId);
			$mentionedByName = CoreUser::getForamattedNames($mentionedBy, ' ');
			$questiontitle = App7020Question::getQuestionById($this->eventParams['questionId']);
			$criteria = new CDbCriteria();
			$criteria->addInCondition('idst', $mentionedIds);
			$users = CoreUser::model()->findAll($criteria);
			$recipCollection = $this->getUsersDataFromArray($users);
			$questionObject = App7020Question::model()->findByPk($questionId);
			$ownerQuestion = CoreUser::model()->findByPk($questionObject->idUser);
			$useravatar = App7020Helpers::getUserAvatar($this->eventParams['mentionedByUserId']);
			if ($useravatar['avatar']) {
				$storage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_AVATARS);
				$avatarImg = $storage->fileUrl($useravatar['avatar']);
				$avatar = '<div class="widget-app7020Avatar-container">'
					. '<span style="width: 42px;
						height: 42px;
						display: inline-block;
						position: relative;
						vertical-align: top;" '
					. 'class="small avatar-container">'
					. '<img style="
						width:42px;
						min-width:42px;
						height:42px;
						min-height:42px;
						border-radius: 50%;" 
						src="' . $avatarImg . '">'
					. '</span>'
					. '</div>';
			} else {
				$avatarImg = '';
				if ($useravatar['firstname'] && $useravatar['lastname']) {
					$avataratt = substr($useravatar['firstname'], 0, 1) . substr($useravatar['lastname'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				} else {
					$avataratt = substr($useravatar['username'], 0, 1);
					$avatar = '
						<span style="  
						background: #0465ac;
						text-align: center;
						display: inline-block;
						position: relative;
						-webkit-border-radius: 50%;
						-moz-border-radius: 50%;
						border-radius: 50%;
						margin-right: 10px;
						font-size: 19px;
						width: 40px;
						color: #fff;
						line-height: 40px;
						font-weight: 600;
						text-transform: uppercase;" class="circle">'
						. $avataratt
						. '</span>';
				}
			}

			$menthioned = CoreUser::model()->findByPk($this->eventParams['mentionedIds']);

			$response->metadata['questionTitle'] = $questiontitle['title'];
			$response->metadata['questionUrl'] = Docebo::createAbsoluteApp7020Url("askTheExpert/index") . '#/question/' . $questionId;
			$response->metadata['mentionAvatar'] = $avatar;
			$response->metadata['mentionAvatarUrl'] = $avatarImg;
			$response->metadata['mentionedByName'] = $mentionedByName;

			foreach ($recipCollection['users'] as $index => $recipient) {
				$recipCollection['users'][$index]['app7020_first_name'] = $recipCollection['users'][$index]['firstname'];
				$recipCollection['users'][$index]['app7020_user_email'] = (!empty($menthioned) ? $menthioned['email'] : '');
			}
		}

		$response->recipients 	= $recipCollection['users'];
		$response->languages 	= $recipCollection['languages'];

		return $response;
	}


	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}


	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {

		if (is_array($metadata) && isset($metadata['questionTitle'])) {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$questionText = $metadata['questionTitle'];
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$questionText = strip_tags($metadata['questionTitle']);
					break;
			}
		} else {
			$questionText = '';
		}

		switch ($textFormat) {
			case NotificationTransportManager::TEXT_FORMAT_HTML:
				// retrieve directly the previously generated HTML for avatar image
				$avatar = (is_array($metadata) && isset($metadata['mentionAvatar']) ? $metadata['mentionAvatar'] : '');
				break;
			case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
			default:
				// these formats cannot display images
				$avatar = '';
				break;
		}

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_APP7020_MENTIONED_ANSWER_FIRST_NAME] = $recipient['app7020_first_name'];
		$result[CoreNotification::SC_APP7020_MENTIONED_ANSWER_OWNER] = (is_array($metadata) && isset($metadata['mentionedByName']) ? $metadata['mentionedByName'] : '');
		$result[CoreNotification::SC_APP7020_MENTIONED_ANSWER_QUESTION_URL_PAGE] = (is_array($metadata) && isset($metadata['questionUrl']) ? $metadata['questionUrl'] : '');
		$result[CoreNotification::SC_APP7020_MENTIONED_ANSWER_QUESTION_TITLE] = $questionText;
		$result[CoreNotification::SC_APP7020_MENTIONED_ANSWER_OWNER_AVATAR] = $avatar;
		$result[CoreNotification::SC_APP7020_MENTIONED_USER_EMAIL] = $recipient['app7020_user_email'];

		return $result;
	}


	public function getSpecialProperties($recipient, $metadata = array()) {
		return array(
			'author' => (is_array($metadata) && isset($metadata['mentionedByName']) ? $metadata['mentionedByName'] : ''),
			'avatar' => (is_array($metadata) && isset($metadata['mentionAvatarUrl']) ? $metadata['mentionAvatarUrl'] : '')
		);
	}



	public function getUsersDataFromArray($arrayUsers = array()){
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userModel){
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] =array(
				'idst' 			=> $userModel->idst,
				'user'          => $userModel->idst,
				'username'		=> trim($userModel->userid,'/'),
				'email'			=> $userModel->email,
				'firstname'			=> $userModel->firstname,
				'language'		=> $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}

}
