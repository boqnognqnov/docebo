<?php




class SubBundleEdited extends NotificationHandler implements INotificationHandler {
	
	public function getRecipientsData($params = false) {
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages  = array();
		$response->metadata   = array();
		$recipCollection = array('users' => array(), 'languages' => array());
		
		// ONLY "AT"  
		if ($this->notification->schedule_type == CoreNotification::SCHEDULE_AT) {
			$catalogs = implode('</li><li class="catalog-item">', $this->eventParams['catalogs']);
			// Shortcodes
			$response->metadata['name'] = $this->eventParams["data"]['name'];
			$response->metadata['code'] = $this->eventParams["data"]['code'];
			$response->metadata['description'] = $this->eventParams["data"]['description'];
			$response->metadata['type'] = ($this->eventParams["data"]['type'] == '1') ? "Seats" : "License";
			$response->metadata['catalog_list'] = $catalogs;
			if($this->notification["recipient"] == "poweruser") {
				$pus = (!empty($this->eventParams["users"]["pu_ids"])) ? $this->eventParams["users"]["pu_ids"] : [];
				$userIds = array_values(array_unique(array_merge_recursive($pus, $this->eventParams['users']["admin_ids"])));
			} else {
				$userIds = $this->eventParams['users']["admin_ids"];
			}
			$userModel = new CoreUser();
			$users = $userModel->findAllByAttributes(['idst' => $userIds]);
			
			$recipCollection = $this->getUsersDataFromArray($users);
			
			$response->recipients = $recipCollection['users'];
			$response->languages = $recipCollection['languages'];

			return $response;
		}
	}
	
	
	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);

		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_NAME] = $metadata['name'];
		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_CODE] = $metadata['code'];
		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_DESCRIPTION] = $metadata['description'];
		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_TYPE] = $metadata['type'];
		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_CATALOG_LIST] = $metadata['catalog_list'];
		$result[CoreNotification::SC_SUBSCRIPTION_BUNDLE_PLAN_LIST] = $metadata['plan_list'];
		return $result;
		
	}
	
	
	
	/**
	 * Returns standard descriptor
	 * @param string $params
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false) {
		$descriptor = new NotificationDescriptor();
		return $descriptor;
	}
	
	
	
	public function getUsersDataFromArray($arrayUsers = array()) {
		$users = array();
		$languages = array();
		foreach ($arrayUsers as $userId) {
			$language = $this->getUserLanguageCode($userId['idst']);
			$users[$userId['idst']] = array(
				'idst' => $userId['idst'],
				'user' => $userId['idst'],
				'username' => trim($userId['userid'], '/'),
				'email' => $userId['email'],
				'language' => $language,
			);
			$languages[$language] = 1;
		}

		return array(
			'users' => $users,
			'languages' => array_keys($languages),
		);
	}
	
	
	
	
}
