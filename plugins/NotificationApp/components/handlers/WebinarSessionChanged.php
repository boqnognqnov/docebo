<?php

class WebinarSessionChanged extends NotificationHandler implements INotificationHandler
{

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params = false)
	{
		// TODO: Implement descriptor() method.
	}

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param bool|string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData($params = false)
	{
		$response = new NotificationHandlerResponse();
		$response->recipients = array();
		$response->languages = array();
		$response->metadata = array();

		$recipCollection = array('users' => array(), 'languages' => array());

		if ($this->notification->schedule_type = CoreNotification::SCHEDULE_AT) {
			$sessionModel = $this->eventParams['session'];
			$sessionModel->refresh();

			$response->metadata['sessionModel'] = $sessionModel;

			/**
			 * @var $sessionModel WebinarSession
			 */
			$courseId = $sessionModel->course_id;
			$id_session = $sessionModel->id_session;

			$targetCourses = $this->maskItems(self::MASK_COURSES, array($courseId));
			$targetUsers = $this->maskItems(self::MASK_USERS, false);

			if (($targetCourses !== false && in_array($courseId, $targetCourses)) || $targetCourses === false) {
				$courseModel = LearningCourse::model()->findByPk($courseId);
				$response->metadata['courseModel'] = $courseModel;

				if ($targetUsers === false) {
					$targetUsers = $sessionModel->getEnrolledUsers();
				}

				// Prepare Session start and end dates
				$webinar_start_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getStartDate($id_session)));
				$webinar_end_date = str_replace(array("T", "+0000"), array(" ", ""), Yii::app()->localtime->fromLocalDateTime(WebinarSession::model()->getEndDate($id_session)));

				$sessionDates = $sessionModel->getDates();
				$response->metadata['sessionDates'] = $sessionDates;

				$enrollments = $this->getWebinarEnrolledRecipientsData($this->notification->recipient, $targetUsers, $targetCourses);

				foreach ($enrollments['users'] as $index => $recipient) {
					$dateBegin = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_start_date);
					$dateEnd = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $webinar_end_date);

					$ics = new ICSManager(
						ICSManager::TYPE_REQUEST,
						$dateBegin,
						$dateEnd,
						$sessionModel->name,
						$sessionModel->description,
						null,
						$this->notification->from_name,
						$this->notification->from_email,
						$sessionModel);

					//list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates);

					$enrollments['users'][$index]['course'] = $courseModel->idCourse;
					//$enrollments['users'][$index]['course_name'] = $courseModel->name;
					//$enrollments['users'][$index]['session_name'] = $sessionModel->name;
					//$enrollments['users'][$index]['session_dates'] = $dateInfo;
					//$enrollments['users'][$index]['webinar_tool'] = $sessionModel->getToolName(true);
					//$enrollments['users'][$index]['webinar_session_details'] = $details;
					$enrollments['users'][$index]['icsManager'] = $ics;
					$enrollments['users'][$index]['id_session'] = $id_session;
				}

				$response->recipients = $enrollments['users'];
				$response->languages = $enrollments['languages'];
				return $response;
			}
		}
	}



	public function getShortcodesData($recipient, $metadata = array(), $textFormat = false) {
		$courseModel = (is_array($metadata) && isset($metadata['courseModel']) ? $metadata['courseModel'] : null);
		$sessionModel = (is_array($metadata) && isset($metadata['sessionModel']) ? $metadata['sessionModel'] : null);
		$sessionDates = (is_array($metadata) && isset($metadata['sessionDates']) ? $metadata['sessionDates'] : array());

		list($details, $dateInfo) = $this->getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat);

		$result = parent::getShortcodesData($recipient, $metadata, $textFormat);
		$result[CoreNotification::SC_COURSENAME] = (!empty($courseModel) ? $courseModel->name : '');
		$result[CoreNotification::SC_SESSION_DATES] = $dateInfo;
		$result[CoreNotification::SC_SESSION_NAME] = (!empty($sessionModel) ? $sessionModel->name : '');
		$result[CoreNotification::SC_WEBINAR_WEBINAR_TOOL] = (!empty($sessionModel) ? $sessionModel->getToolName(true) : '');
		$result[CoreNotification::SC_WEBINAR_SESSION_DETAILS] = $details;

		return $result;
	}

}