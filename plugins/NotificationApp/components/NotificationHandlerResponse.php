<?php

/**
 * Created by PhpStorm.
 * User: Georgi
 * Date: 28.3.2016 г.
 * Time: 14:35
 */
/**
 * Use this class as a response container from notification handlers.
 *
 */
class NotificationHandlerResponse extends stdClass {
    /**
     *  Array of recipients, where the key is core_user::idst and each element is having **very** specific structure:
     *
     * 	Key					Value
     * 	===					=====
     * 	// Mandatory (recipient's data)
     * 	idst				core_user::idst
     * 	username			core_user::userid
     * 	email				core_user::email
     * 	language			core_setting_user::value for ui.language path name
     *
     * 	// Most of the time you also want to add these (recipient's related)
     *	'timezone'				=> user's TZ
     *	'date_format_locale'	=> user's format
     *
     * 	// And of course you can anything else that your handlers require, some related information, like
     * 	'user'				=> <ID of a single user related to the "idst" in some way>
     *  'course'			=> <ID of a single course related to the "idst" in some way>
     *	'category'			=> ...
     *  'thread'			=> ...
     *  ...
     *
     * Example:
     * 		[] => array(
     * 			idst		=> 12303,
     * 			username 	=> 'johndoe',
     * 			email		=> 'johndoe@someemail.com'
     * 			language	=> 'german',
     * 			user		=> 12999   	// user related to the recipient & notification, like the user that has been subscribed to a course
     * 			course		=> 999		// course related to the recipient & notification,... see above
     * 			....
     * 		)
     *
     *	At the end of the day, this array of data is returned to Notification Job Handler wich "consume" it on its discretion, using whatever part of data
     *	it needs. The CORE notification job handler (NotificationJobHandler class) requires the above structure. If you create your own notification
     *	job handler, it may require completely different one. With that said, THIS class is *REQUIRED* for CORE Job handler only.
     *
     *
     * @var array
     */
    public $recipients 	= array();

    /**
     * Resulting array of distinct languages of all $recipients
     *
     * @var array  Array of language codes, e.g. ["english","german", ...]
     */
    public $languages 	= array();

}