<?php

/**
 * A base class to handle SSO links placed inside notification emails
 */
class NotificationSSOHandler extends CComponent implements ISsoHandler {

	/**
	 * This method should performs the authentication checks and return (if the user is logged in)
	 * the userid that Docebo must set as "logged in" in the LMS. In some cases, it can even not return
	 * and perform a redirect to the external authentication portal.
	 * @return string Userid of the logged in user
	 * @throws Exception in case something is wrong and SSO cannot be performed
	 */
	public function login() {
		// Get request params
		$login_user = urldecode( Yii::app()->request->getParam( 'login_user', '' ) );

		// Check if user is already logged in
		if ( ! Yii::app()->user->getIsGuest() ) {
			$loggedInUser = Yii::app()->user->getUsername();
			if ( $loggedInUser == $login_user ) {
				// Determine which URL to jump to
				$redirect_url = $this->getLandingURL();

				// Finally, redirect!!
				Yii::app()->controller->redirect( $redirect_url );
			} else {
				LearningTracksession::model()->updateAll( array( 'active' => 0 ), 'active = 1 AND idUser = ' . Yii::app()->user->id );
				Yii::app()->user->logout();
				Yii::app()->controller->refresh();
			}
		}

		$timestampWhenGenerated = Yii::app()->request->getParam( 'time', '' );
		$privateKey             = Settings::get( 'sso_secret', '8ca0f69afeacc7022d1e589221072d6bcf87e39c' );
		$publicKey              = strtoupper( Yii::app()->request->getParam( 'token', '' ) );
		$lifetimeDays           = Yii::app()->request->getParam( 'validity', 1 );

		// Get settings
		$is_sso_enabled = Settings::get( 'sso_token', 'off' ) == 'on';

		// Should we accept sso requests?
		if ( ( $login_user == '' ) || ! $is_sso_enabled ) {
			throw new Exception( "Empty login_user param or SSO not available", 1 );
		}

		// Recalculate the token internally
		$recalculatedPublicKey = strtoupper( md5( $login_user . ',' . $timestampWhenGenerated . ',' . $lifetimeDays . ',' . $privateKey ) );
		
		$currentTime = time();		
		
		//First check if the expiration day is expired, because of recalculation public key
		$lifeTimeInSeconds = strtotime( "{$lifetimeDays} day", 0 );
		if(( $timestampWhenGenerated + $lifeTimeInSeconds ) < $currentTime){
			throw new Exception( "Invalid or expired token", 1 );
		}

		if ( $recalculatedPublicKey == $publicKey ) {
			if ( $controller = Yii::app()->getController() ) {
				// Log the user in

				$identity = new DoceboUserIdentity( $login_user, NULL );
				if ( $identity->authenticateWithNoPassword() && Yii::app()->user->login( $identity ) ) {
					$controller->redirect( $this->getLandingURL() );
				} else {
					Yii::app()->user->logout();
					$controller->redirect( Docebo::createLmsUrl( 'site/index', array( 'error' => 1 ) ) );
				}

			} else {
				throw new CException( __CLASS__ . ' must be called from a Controller context for redirect to work' );
			}
		} else {
			// Validate the token here and check for time expiration
			$lifeTimeInSeconds = strtotime( "{$lifetimeDays} day", 0 );

			$tokenMissmatch = $recalculatedPublicKey != $publicKey;
			$tokenExpired   = ( $timestampWhenGenerated + $lifeTimeInSeconds ) < time();

			if ( $tokenMissmatch || $tokenExpired ) {
				throw new Exception( "Invalid or expired token", 1 );
			}

			return $login_user;
		}
	}

	/**
	 * This method returns the URL to redirect to, after a successful LMS login
	 * @return string URL to redirect to
	 */
	public function getLandingURL() {
		$id_course   = Yii::app()->request->getParam( 'id_course', 0 );
		$destination = Yii::app()->request->getParam( 'destination', '' );

		if ( $destination ) {
			return $destination;
		} else {
			return Docebo::createLmsUrl( 'site/index' );
		}
	}

	static public function getEncryptedSSOLinkForUser($userid, $expirationDays = 1, $landingUrl = false){
		$privateKey             = Settings::get( 'sso_secret', '8ca0f69afeacc7022d1e589221072d6bcf87e39c' );
		$timestampWhenGenerated = time();

		$publicKeyEncrypted = strtoupper( md5( $userid . ',' . $timestampWhenGenerated . ',' . $expirationDays . ',' . $privateKey ) );

		$queryParams = array(
			'token'=>$publicKeyEncrypted,
			'time'=>$timestampWhenGenerated,
			'login_user'=>$userid,
			'validity'=>$expirationDays,
			'sso_type'=>'notification', // causes NotificationSSOHandler to handle the SSO login
		);

		if($landingUrl!=false){
			$queryParams['destination'] = $landingUrl;
		}

		$ssoLoginLink = Docebo::createAbsoluteLmsUrl('site/sso', $queryParams);
		return $ssoLoginLink;
	}
}