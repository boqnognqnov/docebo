<?php

class NotificationHandler extends CComponent {

	static $CUSTOM_HANDLERS = array();


	const MASK_USERS 			= 'maskusers';
	const MASK_COURSES 			= 'maskcourses';
	const MASK_PLANS 			= 'maskplans';
	const MASK_GROUPS 			= 'maskgroups';
	const MASK_BRANCHES			= 'maskbranches';


	/**
	 * Which PU allocations to observer: users only? courses only? or both ? or NONE?
	 *
	 * @var string
	 */
	const PU_OF_BOTH 		= 'userscourses';
	const PU_OF_USERSONLY	= 'users';
	const PU_OF_COURSESONLY	= 'courses';
	const PU_OF_NONE 		= 'none';

	/**
	 * Various enrollment related events
	 *
	 * @var string
	 */
	const ENROLL_EVENT_SUBSCRIBED 				= 'subscribed';
	const ENROLL_EVENT_SUBSCRIBED_WAITING		= 'subscribed_waiting_approval';
	const ENROLL_EVENT_COURSE_EXPIRED 			= 'course_expired';
	const ENROLL_EVENT_STUDENT_COMPLETED 		= 'student_completed';

	/**
	 * Varios ILT session related events
	 */
	const ILT_SESSION_STARTING                  = 'ilt_session_starting';

	/**
	 * Various coaching session related events
	 *
	 * @var string
	 */
	const COACHING_EVENT_EXPIRED 				= 'expired';
	const COACHING_EVENT_STARTING 				= 'starting';

	/**
	 * User related events (registered, etc.)
	 *
	 * @var string
	 */
	const USER_EVENT_REGISTERED_TO_LMS			= 'registered';

	/**
	 * Users, Courses, Plans filters
	 * @var array
	 */
	protected $usersMask 	= array();
	protected $coursesMask 	= array();
	protected $plansMask 	= array();
	protected $groupsMask	= array();
	protected $branchesMask	= array();

	/**
	 *
	 * @var CoreNotification
	 */
	protected $notification	= null;

	/**
	 *
	 * @var CoreJob
	 */
	protected $job			= null;

	/**
	 *
	 * @var array
	 */
	protected $jobParams	= null;

	/**
	 * Event parameters, if any
	 * @var array
	 */
	protected $eventParams	= null;

	/**
	 * Default language to use if user has no language set (language code, e.g. "english")
	 * @var string
	 */
	protected $defaultLanguage;


	/**
	 *
	 * @param CoreJob $job
	 * @param $params array Array of paramas passed if we create a background job
	 */
	public function __construct(CoreJob $job, $params = array()) {
		/* @var $notification CoreNotification */

		if(empty($params)){
			$this->job = $job;
			$this->jobParams = json_decode($job->params, true);
		} else{
			$this->jobParams = $params;
			$this->eventParams = $this->jobParams;
		}

		if (isset($this->jobParams['eventParams'])) {
			$this->eventParams = unserialize($this->jobParams['eventParams']);
		}

		$notification = CoreNotification::model()->findByPk($this->jobParams['notification_id']);
		$this->notification			= $notification;

		// List of users/courses/groups/branches/plans to mask/filter the notification attributes (recipients, etc.)
		// USERS
		$this->usersMask	= $notification->getEffectiveUsersFromUserFilter();

		// GROUPS
		$info = $notification->getUserFilterInfo();
		$this->groupsMask = $info['groups'];

		// BRANCHES
		$this->branchesMask = $info['branchesFlatList'];

		// LEARNING PLANS
		$info = $notification->getAssocInfo();
		$this->plansMask 	= $info['plans'];

		// COURSES
		$this->coursesMask	= array_unique(array_merge($info['courses'], $info['plansCourses']));


		// We MUST be sure that user/recipient will receive the notification, no matter if it has a personal language set
		$this->defaultLanguage		= Settings::get('default_language', false) ? Settings::get('default_language') : "english";

	}



	/*
	 * Followig methods are helper methods, used by descendant classes to get/collect common information.
	 *
	 * The return structure of ALL helpers is the same:
	 *
	 * 		array(
	 * 			'languages'	=> array(<unique-codes-of-all-languages-of-all-users-found>),
	 * 			'users' => array(
	 * 				// Mandatory
	 * 				'idst' 				=> <user idst>,
	 * 				'username'			=> <user's userid>
	 * 				'email'				=> <user email>
	 * 				'language'			=> <user language from CoreSettingUser>
	 *
	 * 				// Most of the time (but check if it is available)
	 *				'timezone'				=> user's TZ
	 *				'date_format_locale'	=> user's format
	 *
	 * 				// Some helper may add more, like
	 *  			'user'				=> <ID of a single user related to the "idst" in some way>
	 * 				'course'			=> <ID of a single course related to the "idst" in some way>
	 *				'category'			=> ...
	 *  			'thread'			=> ...
	 *  			...
	 * 			)
	 * 		)
	 *
	 *
	 */


	/**
	 * Helper method to return information about list of users (and possibly subscribed courses, if any).
	 * Note please: Main purpose of this method is to extract data about USERS not to get purely enrollment data!
	 * See the bottom of the method for the returned data structure.
	 *
	 * @param array $usersList
	 * @param array $coursesList  Check if user is subscribed to these courses. If set to false: does not matter (default)
	 * @param array $levels Subscription levelS to check (but only if we also check for courses!)
	 * @param bool $statuses
	 * @param bool $iltSessionsList
	 * @param bool $filterByEnrollmentData join to learning_courseuser table and filter by course, level or status
	 *
	 * @return array of data describing the users found (ID, language, subscribed courses, etc.)
	 */
	protected function helperGetUsersData($usersList=false, $coursesList=false, $levels=false, $statuses=false, $iltSessionsList=false, $filterByEnrollmentData=true, $excludeILTwaitingUsers = false) {

		$data = array('users' => array(), 'languages' => array());

		// Empty arrays of these parameters means NO result (because it is a filtering mechanism)
		// FALSE is different! It means "user/course/level does NOT matter" !!!
		// Also, these checks prevent SQL IN() syntax error later
		if (is_array($coursesList) && empty($coursesList)) {
			return $data;
		}
		if (is_array($usersList) && empty($usersList)) {
			return $data;
		}
		if (is_array($levels) && empty($levels)) {
			return $data;
		}

		$params = array();
		$select = array(
				'user.idst							AS idst',
				'user.userid 						AS username',
				'user.email							AS email',
				'csu_lang.value						AS language',
				'csu_timezone.value					AS timezone',
				'csu_format_locale.value			AS date_format_locale',
		);

		$command = Yii::app()->db->createCommand()->from('core_user user');

		// Join user settings table to get the user's "language"
		$command->leftJoin('core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')");

		// Join user settings table to get the user's "timezone"
		$command->leftJoin('core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')");

		// Join user settings table to get the user's "date_format_locale"
		$command->leftJoin('core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')");


		if($filterByEnrollmentData) {
			$select[] = 'GROUP_CONCAT(enrollment.idCourse) AS courses';
			$enrollmentJoinCond = "user.idst=enrollment.idUser";

			// Filter by list of courses ONLY if it does matter (NOT false) and it is really an array
			if ($coursesList !== false && is_array($coursesList)) {
				$enrollmentJoinCond .= " AND enrollment.idCourse IN (".implode(',',$coursesList).")";
			}

			// Filter by user subscription level ONLY if we are also filtering by course
			if ($levels !== false && is_array($levels) && !empty($levels)) {
				$enrollmentJoinCond .= " AND enrollment.level IN (".implode(',',$levels).")";
			}

			// Filter by user subscription status ONLY if we are also filtering by course
			if ($statuses!== false && is_array($statuses) && !empty($statuses)) {
				$enrollmentJoinCond .= " AND enrollment.status IN (".implode(',',$statuses).")";
			}

			$command->join('learning_courseuser enrollment', $enrollmentJoinCond);
		}

		if($iltSessionsList !== false && is_array($iltSessionsList)){
			if (empty($iltSessionsList)) { $iltSessionsList = array("0"); }
			$command->join('lt_courseuser_session ics', 'user.idst=ics.id_user AND ics.id_session IN (' . implode(',',$iltSessionsList) . ')');
			if ($excludeILTwaitingUsers) {
				$command->andWhere('ics.status NOT IN (:s_status_1, :s_status_2, :s_status_3)', array(
					':s_status_1' => LtCourseuserSession::$SESSION_USER_CONFIRMED,
					':s_status_2' => LtCourseuserSession::$SESSION_USER_WAITING_LIST,
					':s_status_3' => LtCourseuserSession::$SESSION_USER_SUSPEND
				));
			}
			$select[] = 'GROUP_CONCAT(ics.id_session) AS sessions';
		}

		// Filter by list of users
		if ($usersList !== false && is_array($usersList)) {
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');
		}

		// Filter by groups
		$groups = $this->groupsMask;
		$usersInGroups = Yii::app()->db->createCommand()
				->select('idstMember')
				->from(CoreGroupMembers::model()->tableName())
				->where(array('IN','idst', $groups))->queryColumn();

		// Filter by branches
		$usersInBranches = array();
		$branches = $this->branchesMask;
		foreach($branches as $branchId){
			$model = CoreOrgChartTree::model()->findByAttributes(
					array('idOrg'=>$branchId)
			);
			$usersInBranches = array_merge($usersInBranches, $model->getNodeUsers());
		}
		$users = array_unique(array_merge($usersInBranches, $usersInGroups));
		if(!empty($users)){
			$command->andWhere('user.idst IN (' . implode(',', $users) . ')');
		}

		// GROUP by USER
		// We are collecting USERS data after all, not enrollments or other type!!!
		$command->group('user.idst');

		$command->select(implode(',', $select));

		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");

		$reader = $command->query($params);

		$users = array();
		$languages = array();
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$user = array(
				'idst' 					=> $row['idst'],
				'username'				=> trim($row['username'],'/'),
				'email'					=> $row['email'],
				'language'				=> $language,
				'timezone'				=> $row['timezone'],
				'date_format_locale'	=> $row['date_format_locale'],
				'courses'				=> !empty($row['courses']) ? explode(',',$row['courses']) : array(),
				'sessions'              => (isset($row['sessions']) && !empty($row['sessions']))  ? explode(',',$row['sessions']) : array(),
			);

			$languages[$language] = 1;
			$users[] = $user;
		}

		$data['users'] 		= $users;
		$data['languages'] 	= array_keys($languages);

		return $data;

	}



	/**
	 * Return array of godadmins data
	 *
	 * @return array
	 */
	protected function helperGetAllGodAdmins() {
		$godAdmins = Yii::app()->user->getAllGodAdmins();
		$users = array();
		$languages = array();
		foreach ($godAdmins as $userModel) {
			$language = $this->getUserLanguageCode($userModel->idst);
			$users[$userModel->idst] =array(
				'idst' 			=> $userModel->idst,
				'user'          => $userModel->idst,
				'username'		=> trim($userModel->userid,'/'),
				'email'			=> $userModel->email,
				'language'		=> $language,
			);
			$languages[$language] = 1;

		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}


	/**
	 * Return a combined data about power users, optionally related to passed users and courses
	 *
	 * @param string $userList FALSE means "doesn't matter"
	 * @param string $coursesList	FALSE means "doesn't matter"
	 *
	 * @return array
	 */
	protected function helperGetPowerUsers($usersList = false, $coursesList = false, $onlyBoth = false, $isSubscibedUser = true, $skipCourseUserStatus = false) {

		$powerUsersOfUsers 		= CoreAdminTree::getPowerUsersOfUsers($usersList);
		$powerUsersOfCourses 	= CoreAdminCourse::getPowerUsersOfCourses($coursesList);

		$users = array();
		$languages = array();

		if ($onlyBoth === true) {
			$powerusersCombined = array_intersect_key($powerUsersOfUsers, $powerUsersOfCourses);
			foreach ($powerUsersOfCourses as $pUser => $course) {
				foreach ($powerusersCombined as $key => $pu) {
					if ($pUser == $key) {
						$powerusersCombined[$key]['courses'] = $course;
					}
				}
			}

			foreach ($powerusersCombined as $idPowerUser => &$userId) {
				$userId['courses'] = array_unique($userId['courses']);
				$userModel = CoreUser::model()->findByPk($idPowerUser);
				$language = $this->getUserLanguageCode($idPowerUser);
				foreach ($userId as $key => $learnerId) {
					foreach ($userId['courses'] as $course) {

						if (is_string($learnerId) && (LearningCourseuser::isSubscribed($learnerId, $course) || !$isSubscibedUser)) {
							$user = LearningCourseuser::model()->findByAttributes(array(
								'idUser' => $learnerId,
								'idCourse' => $course
							));
							if(!$isSubscibedUser || (isset($user) && ($user->status != LearningCourseuser::$COURSE_USER_END || $skipCourseUserStatus == true) && $user->level == LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT)) {
								$users[] = array(
									'idst' => $idPowerUser,
									'username' => trim($userModel->userid, '/'),
									'email' => $userModel->email,
									'language' => $language,
									'user' => $learnerId,
									'courses' => $course,
								);
							}
						}
					}
				}

				$languages[$language] = 1;
			}
		} else {
			foreach ($powerUsersOfUsers as $idPowerUser => $puUsers) {
				$userModel = CoreUser::model()->findByPk($idPowerUser);
				$language = $this->getUserLanguageCode($idPowerUser);
				$users[$idPowerUser] = array(
					'idst' => $idPowerUser,
					'username' => trim($userModel->userid, '/'),
					'email' => $userModel->email,
					'language' => $language,
					'users' => $puUsers,
				);
				$languages[$language] = 1;
			}


			foreach ($powerUsersOfCourses as $idPowerUser => $puCourses) {
				if (!isset($users[$idPowerUser])) {
					$userModel = CoreUser::model()->findByPk($idPowerUser);
					$language = $this->getUserLanguageCode($idPowerUser);
					$users[$idPowerUser] = array(
						'idst' => $idPowerUser,
						'username' => trim($userModel->userid, '/'),
						'email' => $userModel->email,
						'language' => $language,
						'courses' => $puCourses,
					);
					$languages[$language] = 1;
				} else {
					$users[$idPowerUser]['courses'] = $puCourses;
				}
			}
		}

		if ($this->notification->puProfileId != 0)
			$users = $this->filterPowerUsersByProfile($users, $this->notification->puProfileId);

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);

	}

	protected function filterPowerUsersByProfile($pusers, $puProfileId){
		$allowedPuIds = Yii::app()->db->createCommand()
			->select('idstMember')
			->from(CoreGroupMembers::model()->tableName())
			->where('idst = :idGroup', array(':idGroup' => $puProfileId))->queryColumn();
		foreach ($pusers as $key => $puser) {
			if (!in_array($puser['idst'], $allowedPuIds))
				unset($pusers[$key]);
		}
		return $pusers;
	}

	/**
	 * Get list of power users related to LIST of users and optionally filtered by courses they are subscribed to
	 *
	 * @param array $usersList FALSE = doesn't matter
	 * @param array	$coursesList FALSE = doesn't matter
	 *
	 * @return array
	 */
	protected function helperGetPowerUsersOfUsersData($usersList = false, $coursesList = false, $stopFilteringUsers=false) {

		// Filter OUT users NOT related to list of courses, if provided as an array
		if ($coursesList !== false && is_array($coursesList)&&(!$stopFilteringUsers)) {
			$filteredUsers = $this->helperGetUsersData($usersList, $coursesList);
			$usersList = array();
			foreach ($filteredUsers['users'] as $tmpUserData) {
				$usersList[] = $tmpUserData['idst'];
			}
		}

		$powerUsers	= CoreAdminTree::getPowerUsersOfUsers($usersList);

		$users = array();
		$languages = array();

		foreach ($powerUsers as $idPowerUser => $assignedList) {
			$userModel = CoreUser::model()->findByPk($idPowerUser);
			$language = $this->getUserLanguageCode($idPowerUser);
			$users[$idPowerUser] =array(
				'idst' 				=> $idPowerUser,
				'username'			=> trim($userModel->userid,'/'),
				'email'				=> $userModel->email,
				'language'			=> $language,
				'users'		=> $assignedList,
			);
			$languages[$language] = 1;
		}

		if ($this->notification->puProfileId != 0)
			$users = $this->filterPowerUsersByProfile($users, $this->notification->puProfileId);

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);

	}

	/**
	 * Get list of managers related to LIST of users and optionally filtered by courses they are subscribed to
	 *
	 * @param array|bool $usersList FALSE = doesn't matter
	 * @param array|bool $coursesList FALSE = doesn't matter
	 * @param bool $stopFilteringUsers
	 *
	 * @return array
	 */
	protected function helperGetManagersOfUsersData($usersList = false, $coursesList = false, $stopFilteringUsers=false) 
	{
		$users = array();
		$languages = array();

		// Filter OUT users NOT related to list of courses, if provided as an array
		if ($coursesList !== false && is_array($coursesList)&&(!$stopFilteringUsers)) {
			$filteredUsers = $this->helperGetUsersData($usersList, $coursesList);
			$usersList = array();
			foreach ($filteredUsers['users'] as $tmpUserData) {
				$usersList[] = $tmpUserData['idst'];
			}
		}

		// Getting user managers
		$command = Yii::app()->db->createCommand();
		$command->select('managers.idManager, GROUP_CONCAT(u2.idst) AS users_list' );
		$command->from('skill_managers managers');
		$command->join("core_user u1", "managers.idManager = u1.idst"); // Manager exists
		$command->join("core_user u2", "managers.idEmployee	= u2.idst");  // USER exists
		$command->group('managers.idManager');

		// If there is some array, filter the result
		if ($usersList !== false) {
			$command->andWhere('managers.idEmployee IN (' . implode(',', $usersList) . ')');
		}

		$managers = $command->query();
		foreach ($managers as $row) {
			$userModel = CoreUser::model()->findByPk($row['idManager']);
			$language = $this->getUserLanguageCode($row['idManager']);
			$users[$row['idManager']] =array(
				'idst' 				=> $row['idManager'],
				'username'			=> trim($userModel->userid,'/'),
				'email'				=> $userModel->email,
				'language'			=> $language,
				'users'				=> isset($row['users_list']) ? explode(',',$row['users_list']) : array(),
			);
			$languages[$language] = 1;
		}

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);
	}


	/**
	 * Get list of power users related to LIST of courses
	 *
	 * @param array  $list FALSE = doesn't matter
	 *
	 * @return array
	 */
	protected function helperGetPowerUsersOfCoursesData($coursesList = false) {

		$groupsList = array();
		if(!empty($this->groupsMask))
			$groupsList = $this->groupsMask;
		if(!empty($this->branchesMask)) {
			$branches = Yii::app()->db->createCommand()
				->select('idst_oc')
				->from(CoreOrgChartTree::model()->tableName())
				->where(array('IN','idOrg', $this->branchesMask))->queryColumn();
			$groupsList = array_unique(array_merge($groupsList, $branches));
		}

		$powerUsers	= CoreAdminCourse::getPowerUsersOfCourses($coursesList, $groupsList);

		$users = array();
		$languages = array();

		foreach ($powerUsers as $idPowerUser => $assignedList) {
			$userModel = CoreUser::model()->findByPk($idPowerUser);
			$language = $this->getUserLanguageCode($idPowerUser);
			$users[$idPowerUser] =array(
				'idst' 				=> $idPowerUser,
				'username'			=> trim($userModel->userid,'/'),
				'email'				=> $userModel->email,
				'language'			=> $language,
				'courses'			=> $assignedList,
			);
			$languages[$language] = 1;
		}

		if ($this->notification->puProfileId != 0)
			$users = $this->filterPowerUsersByProfile($users, $this->notification->puProfileId);

		return array(
			'users' 		=> $users,
			'languages'		=> array_keys($languages),
		);

	}


	/**
	 * Multifunctional helper to build list of recipients data structure (suitable for directly sending emails)
	 * out of list of enrollments Data, having the same data structure pattern.
	 * This method builds recipient data enrollment by enrollment, where single enrollment MAY produce MORE than one
	 * recipient of a given type, e.g. one enrollment -> MANY instructors (recipients). Or MANY Power Users managing the same user/course.
	 *
	 *
	 * @param array $enrollmentsData Usually, this is the result of @see getEnrollmentsWithEventAfterBeforePeriod method
	 * @param string $recipientType
	 * @param string $powerUsersOf If applicable, get power users managing related USERS only, or COURSES only, or BOTH, or none.
	 * @param array $relatedFields Array of  fieldName => fieldValue to add, on top of other data, per recipient
	 *
	 * @return array
	 */
	protected function helperGetRecipientsFromEnrollments($enrollmentsData, $recipientType, $powerUsersOf = self::PU_OF_USERSONLY, $relatedFields=array()) {

		$data = array();
		$data['languages'] 	= array();
		$data['users']		= array();

		switch ($recipientType) {

			// INSTRUCTORS
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$instructorLevels = array(
						LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
						LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				);

				foreach ($enrollmentsData['enrollments'] as $enrollment) {
					$targets = $this->helperGetUsersData(false, array($enrollment['course']), $instructorLevels);
					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($enrollment[$f]) ? $enrollment[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// GODADMINS
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				// YES, before the cycles!!
				$targets = $this->helperGetAllGodAdmins();
				foreach ($enrollmentsData['enrollments'] as $enrollment) {
					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($enrollment[$f]) ? $enrollment[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}
				}
				$data['languages'] = $targets['languages'];
				break;

			// POWER USERS of target users
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				foreach ($enrollmentsData['enrollments'] as $enrollment) {

					switch ($powerUsersOf) {
						case self::PU_OF_BOTH:
							$targets = $this->helperGetPowerUsers(array($enrollment['idst']), array($enrollment['course']), true);
							break;
						case self::PU_OF_USERSONLY:
							$targets = $this->helperGetPowerUsersOfUsersData(array($enrollment['idst']));
							break;
						case self::PU_OF_COURSESONLY:
							$targets = $this->helperGetPowerUsersOfCoursesData(array($enrollment['course']));
							break;
						default:
							$targets = array();
							break;
					}

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($enrollment[$f]) ? $enrollment[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// MANAGERS of the users
			case CoreNotification::NOTIFY_RECIPIENTS_MANAGER:
				foreach ($enrollmentsData['enrollments'] as $enrollment) {
					$targets = $this->helperGetManagersOfUsersData(array($enrollment['idst']), array($enrollment['course']), true);

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($enrollment[$f]) ? $enrollment[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
 
				break;

			// SUBSCRIBERS
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$data['users'] 		= $enrollmentsData['enrollments'];
				$data['languages']	= $enrollmentsData['languages'];
				break;


		}

		$data['languages'] = array_unique($data['languages']);

		return $data;


	}

    /**
     * Get the enrolled users from Webinar session that applies to the notification users filter.
     *
     * @param $recipient Type of the notification receivers (users | instructors | PU | God Admins)
     * @param $usersList List of users
     * @param $coursesList List of courses
     * @return array|null Array of recipients
     */
	private function getWebinarSessionUsers($recipient, $usersList, $coursesList){

		if(empty($usersList)) return null;

		$level = 0;

		switch($recipient){
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$level = LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT;
				break;
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$level = LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR;
				break;
		}

		$data = array(
			'users' => array(),
            'languages' => array()
		);

		foreach($usersList as $key => $user){
			if(isset($user['level'])){
				if($user['level'] == $level){
                    $userModel = CoreUser::model()->findByPk($user['id_user']);
					$lang = CoreSettingUser::model()->findByAttributes(array(
						'id_user' => $userModel->idst,
						'path_name' => 'ui.language'
					));
					$data['users'][$userModel->idst] = array(
                        'idst' => $userModel->idst,
                        'username' => Yii::app()->user->getRelativeUsername($userModel->userid),
                        'email' => $userModel->email,
						'language' => $lang->value
                    );


                    $data['languages'][] = $lang->value;
				}
			} else{
                $userModel = CoreUser::model()->findByPk($user);

                foreach($coursesList as $course){
                    $courseUser = LearningCourseuser::model()->findByAttributes(array(
                        'idUser' => $userModel->idst,
                        'idCourse' => $course
                    ));

                    if($courseUser instanceof LearningCourseuser){
                        if($courseUser->level = $level){
							$lang = CoreSettingUser::model()->findByAttributes(array(
								'id_user' => $userModel->idst,
								'path_name' => 'ui.language'
							));

                            $data['users'][$userModel->idst] = array(
                                'idst' => $userModel->idst,
                                'username' => Yii::app()->user->getRelativeUsername($userModel->userid),
                                'email' => $userModel->email,
								'language' => $lang->value
                            );

                            $data['languages'][] = $lang->value;
                        }
                    }
                }
            }
		}

		return $data;
	}

    /**
     * Get the Webinar session recipients data
     *
     * @param $recipient Type of the notification receivers (users | instructors | PU | God Admins)
     * @param $usersList List of users
     * @param $coursesList List of courses
     * @return array|null Array of recipients
     */
	public function getWebinarEnrolledRecipientsData($recipType, $targetUsers, $targetCourses){
		switch($recipType){
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				return $this->getPUusersAndGodAdmins($targetCourses, $recipType);
				break;
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				return $this->getWebinarSessionUsers($recipType, $targetUsers, $targetCourses);
				break;
		}

	}

    /**
     * Get the God admins or PU managed filtered courses
     *
     * @param array|false List of courses
     * @param $recipient Type of the notification receivers (users | instructors | PU | God Admins)
     * @return array Array of recipients
     */
	public function getPUusersAndGodAdmins($targetCourses = false, $recipient){

		switch($recipient){
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				$puUsers = $this->helperGetPowerUsersOfCoursesData($targetCourses);
				return $puUsers;
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				$godAdmins = $this->helperGetAllGodAdmins();
				return $godAdmins;
			default:
				return array();
		}
	}

	/**
	 * Multifunctional helper to build list of recipients data structure (suitable for directly sending emails)
	 * out of list of coaching assignment Data, having the same data structure pattern.
	 * This method builds recipient data assignment by assignment, where single assignment MAY produce MORE than one
	 * recipient of a given type, e.g. one enrollment -> MANY instructors (recipients). Or MANY Power Users managing the same user/course.
	 *
	 *
	 * @param array $assignmentsData Usually, this is the result of @see getCoachingAssignmentsWithEventAfterBeforePeriod method
	 * @param string $recipientType
	 * @param string $powerUsersOf If applicable, get power users managing related USERS only, or COURSES only, or BOTH, or none.
	 * @param array $relatedFields Array of  fieldName => fieldValue to add, on top of other data, per recipient
	 *
	 * @return array
	 */
	protected function helperGetRecipientsFromCoachingAssignments($assignmentsData, $recipientType, $powerUsersOf = self::PU_OF_USERSONLY, $relatedFields=array()) {
		$data = array();
		$data['languages'] 	= array();
		$data['users']		= array();

		switch ($recipientType) {

			// INSTRUCTORS
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$instructorLevels = array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				);

				foreach ($assignmentsData['assignments'] as $assignment) {
					$targets = $this->helperGetUsersData(false, array($assignment['course']), $instructorLevels);
					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($assignment[$f]) ? $assignment[$f] : '??';

						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// GODADMINS
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				// YES, before the cycles!!
				$targets = $this->helperGetAllGodAdmins();
				foreach ($assignmentsData['assignments'] as $assignment) {
					foreach ($targets['users'] as $tmpData) {
						$tmp = array(
							'idst' => $tmpData['idst'],        // godadmin
							'username' => $tmpData['username'],
							'email' => $tmpData['email'],
							'language' => $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($assignment[$f]) ? $assignment[$f] : '??';

						$data['users'][] = $tmp;
					}
				}
				$data['languages'] = array_merge($data['languages'], $targets['languages']);
				break;

			// POWER USERS of target users
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				foreach ($assignmentsData['assignments'] as $assignment) {
					switch ($powerUsersOf) {
						case self::PU_OF_BOTH:
							$targets = $this->helperGetPowerUsers(array($assignment['idst']), array($assignment['course']));
							break;
						case self::PU_OF_USERSONLY:
							$targets = $this->helperGetPowerUsersOfUsersData(array($assignment['idst']));
							break;
						case self::PU_OF_COURSESONLY:
							$targets = $this->helperGetPowerUsersOfCoursesData(array($assignment['course']));
							break;
						default:
							$targets = array();
							break;
					}

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f)
							$tmp[$f] = isset($assignment[$f]) ? $assignment[$f] : '??';

						$data['users'][] 	= $tmp;
					}
					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// SUBSCRIBERS
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$data['users'] 		= $assignmentsData['assignments'];
				$data['languages']	= $assignmentsData['languages'];
				break;
		}

		$data['languages'] = array_unique($data['languages']);
		return $data;
	}

	/**
	 * Get list of users DATA (potential recipients) RELATED to a WHOLE LIST of users and/or to a WHOLE LIST of courses.
	 * Note: not PER user, but to ALL users in the list. If you need to collect recipients PER user, use helperGetRecipientsForListOfUsers
	 *
	 * @param string $recipient
	 * @param array $usersList
	 * @param array $coursesList
	 * @param string $powerUsersOf If applicable, get power users managing related USERS only, or COURSES only, or BOTH
	 * @param boolean $studentsOnly If USER recipients are explored, get only LEARNERS, ignoting tutors/instructors/etc
	 *
	 * @return array of Data See the bottom of the helperGetUsersData method for the returned data structure.
	 */
	protected function helperGetRecipientsFromUsersData($recipient, $usersList = false, $coursesList = false, $powerUsersOf = self::PU_OF_USERSONLY, $studentsOnly=false, $statuses=false, $stopFilteringUsers=false, $filterUsers = array(), $sessionId = false, $filterUsersByEnrollmentsData = true, $skipEnrollmentCheckForPu = false) {

		$data = array();

		switch ($recipient) {

			// INSTRUCTORS of students
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:

				$instructorLevels = array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				);

				// Collect ALL involved courses from collected users.
				// Note, users are collected && filtered by incoming courses (checking if they are enrolled)
				// If courses list is FALSE, data for all users are collected.
				$usersData = $this->helperGetUsersData($usersList, $coursesList);

				$involvedCoursesList = array();
				foreach ($usersData['users'] as $oneUserData) {
					if (isset($oneUserData['courses'])) {
						$involvedCoursesList= array_merge($involvedCoursesList, $oneUserData['courses']);
					}
				}


				$involvedCoursesList = array_unique($involvedCoursesList);

				if(count($filterUsers) == 0) {
					// Now get INSTRUCTORS (data) of all these courses
					$data = $this->helperGetUsersData(false, $involvedCoursesList, $instructorLevels, false, $sessionId);
				} else {
					// Now get INSTRUCTORS (data) of all these courses and get only users from the list to filter !!!
					$data = $this->helperGetUsersData($filterUsers, $involvedCoursesList, $instructorLevels, false, $sessionId);
				}

				break;

			// GOD ADMINS .. just get list of god admins
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				$data = $this->helperGetAllGodAdmins();
				break;

			// POWER USERS of .... combined though
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:

				switch ($powerUsersOf) {
					case self::PU_OF_BOTH:
						$data = $this->helperGetPowerUsers($usersList, $coursesList, true, $skipEnrollmentCheckForPu);
						break;
					case self::PU_OF_USERSONLY:
						$data = $this->helperGetPowerUsersOfUsersData($usersList, $coursesList, $stopFilteringUsers);
						break;
					case self::PU_OF_COURSESONLY:
						$data = $this->helperGetPowerUsersOfCoursesData($coursesList);
						break;
						case self::PU_OF_NONE:
					default:
						$data = array();
						break;
				}
				break;

			// MANAGERS of the users
			case CoreNotification::NOTIFY_RECIPIENTS_MANAGER:
				$data = $this->helperGetManagersOfUsersData($usersList, $coursesList, $stopFilteringUsers);
				break;

			// SUBSCRIBED *STUDENTS*
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				if ($studentsOnly) {
					$levels = array(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				}
				else {
					$levels = false;
				}
				$data = $this->helperGetUsersData($usersList, $coursesList, $levels, $statuses, false, $filterUsersByEnrollmentsData);
				break;
		}

		return $data;

	}


	/**
	 * Get user language setting, language code, e.g. "english"
	 *
	 * @param int $idst
	 * @return string
	 */
	protected function getUserLanguageCode($idst) {

		static $cached = array();

		$key = (int) $idst;

		if(isset($cached[$key])) {
			return $cached[$key];
		}

		// Find user in core USER settings table
		$model = CoreSettingUser::model()->findByAttributes(array(
				'path_name' => 'ui.language',
				'id_user' => $key,
		));

		// No user found in User settings table?  Check Waiting users table please
		if (!$model) {
			$tempModel = CoreUserTemp::model()->findByAttributes(array('idst' => $key));
			if ($tempModel && !empty($tempModel->language)) {
				$cached[$key] = $tempModel->language;
				return $cached[$key];
			}
		}
		else {
			if (!empty($model->value)) {
				$cached[$key] = $model->value;
				return $cached[$key];
			}
		}

		// Well, we tried everything, no language found for this user. Fallback to ENGLISH
		$cached[$key] = 'english'; // fallback to english
		return $cached[$key];

	}


	/**
	 * Universal masking/filtering method.
	 *
	 * Return masked (filtered) array of target ITEMS (or false, to indicate ALL or "doesn't matter").
	 * "Candidates list" means possible items, involved in some way into the currently handled notification.
	 *
	 * @param string $maskType @see self MASK_* constants, basically: users, courses, plans, groups, branches
	 * @param array $candidatesList
	 * @return array
	 */
	protected function maskItems($maskType, $candidatesList = false) {

		switch ($maskType) {

			case self::MASK_USERS:
				$doFilter = $this->notification->ufilter_option == CoreNotification::USER_DO_USER_FILTER;
				$maskArray = $this->usersMask;
				break;

			case self::MASK_GROUPS:
				$doFilter = $this->notification->ufilter_option == CoreNotification::USER_DO_USER_FILTER;
				$maskArray = $this->groupsMask;
				break;

			case self::MASK_BRANCHES:
				$doFilter = $this->notification->ufilter_option == CoreNotification::USER_DO_USER_FILTER;
				$maskArray = $this->branchesMask;
				break;


			case self::MASK_COURSES:
				$doFilter = $this->notification->cfilter_option == CoreNotification::COURSES_DO_ASSOC;
				$maskArray = $this->coursesMask;
				break;

			case self::MASK_PLANS:
				$doFilter = $this->notification->cfilter_option == CoreNotification::COURSES_DO_ASSOC;
				$maskArray = $this->plansMask;
				break;


			default:
				return array();

		}


		// Do we have to filter items?
		if ($doFilter) {
			// If candidates are FALSE, that means, all ITEMS, in this case, all selected (the mask), if any
			if ($candidatesList === false) {
				return $maskArray;
			}
			// If it is an array of ITEMS, make an intersection (i.e. filter)
			if (is_array($candidatesList)) {
				return array_intersect($maskArray, $candidatesList);
			}
			// Since we don't have list of candidates and NO explicite FALSE is passed, return empty result
			return array();
		}
		// NO filtering option means ALL CANDIDATES pass the filter
		else {
			return $candidatesList;
		}



	}


	/**
	 * Calculates left and right edge of a period (from & to), based on requested shift direction (after/before), period type, period number and pvotal point in time.
	 *
	 *  For example: 3 days ago (provided $pivotUtc is 23 Mar 2015 10:35:00):
	 *    From      = Pivot - (3+1) * 24 hours     (e.g.  2015-03-19 10:35:00)
	 *    To        = Pivot - 3 * 24 hours         (e.g.  2015-03-20 10:35:00)
	 *
	 *  However, If $wholeDay is TRUE, the target period for DAY and WEEK shift periods
	 *  is set from 00:00:00 to 23:59:59 of the resulting date (e.g. 23 Mar - 3 days = 20 Mar =>  2015-03-20 00:00:00 - 2015-03-20 23:59:59.
	 *  This is to respect the human perception of the "3 days ago", which does not perfectly match the 24h hours periods.
	 *
	 * @param string $shiftType
	 * @param integer $shiftNumber
	 * @param string $shiftPeriod
	 * @param string $pivotUtc
	 * @param boolean $wholeDay DEFAULT is FALSE, to keep backward compatibility
	 */
	protected function getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod, $pivotUtc = false, $wholeDay=false) {
        if ($pivotUtc === false) {
			$pivotUtc = Yii::app()->localtime->getUTCNow('Y-m-d H:i:s');
		}

		$tzUtc 	= new DateTimeZone('UTC');
		$dt 	= new DateTime($pivotUtc, $tzUtc);

		switch ($shiftPeriod) {
			case CoreNotification::SCHED_SHIFT_PERIOD_HOUR:
			    $hours = ' H:i:00';
				$dtInterval 		= new DateInterval('PT' . ((int) $shiftNumber) . 'H');
				$dtIntervalSingle 	= new DateInterval('PT1H');
				break;
			case CoreNotification::SCHED_SHIFT_PERIOD_WEEK:
			    $hours = $wholeDay ? false : ' H:i:00';
				$dtInterval 		= new DateInterval('P' . ((int) $shiftNumber * 7) . 'D');
				$dtIntervalSingle 	= new DateInterval('P1D');
				break;
            case CoreNotification::SCHED_SHIFT_PERIOD_MONTH:
                $hours = $wholeDay ? false : ' H:i:00';
                $dtInterval = new DateInterval('P' . ((int)$shiftNumber * 28) . 'D');
                $dtIntervalSingle = new DateInterval('P1M');
                break;
			case CoreNotification::SCHED_SHIFT_PERIOD_DAY:
			default:
			    $hours = $wholeDay ? false : ' H:i:00';
				$dtInterval 		= new DateInterval('P' . ((int) $shiftNumber) . 'D');
				$dtIntervalSingle 	= new DateInterval('P1D');
				break;
		}


		switch ($shiftType) {
			case CoreNotification::SCHEDULE_AFTER:
				// Sub N periods
				$dt->sub($dtInterval);
				$targetTimeTo = $dt->format('Y-m-d' . ($hours ? $hours : ' 23:59:59'));

				// Sub one more period move back in time
				if ($shiftPeriod == CoreNotification::SCHED_SHIFT_PERIOD_HOUR || !$wholeDay) $dt->sub($dtIntervalSingle);

				$targetTimeFrom = $dt->format('Y-m-d' . ($hours ? $hours : ' 00:00:00'));
				break;

			case CoreNotification::SCHEDULE_BEFORE:
				// Add N periods
				$dt->add($dtInterval);
				$targetTimeFrom = $dt->format('Y-m-d' . ($hours ? $hours : ' 00:00:00'));

				// Add one more period move forward in time
				if ($shiftPeriod == CoreNotification::SCHED_SHIFT_PERIOD_HOUR || !$wholeDay) $dt->add($dtIntervalSingle);

				$targetTimeTo = $dt->format('Y-m-d' . ($hours ? $hours : ' 23:59:59'));
				break;
            case CoreNotification::SCHEDULE_EVERY:
                // Add N periods
                $dt->add($dtInterval);
                $targetTimeFrom = $dt->format('Y-m-d' . ($hours ? $hours : ' 00:00:00'));
                // Add one more period move forward in time
                if ($shiftPeriod == CoreNotification::SCHED_SHIFT_PERIOD_HOUR || !$wholeDay) $dt->add($dtIntervalSingle);
                $targetTimeTo = $dt->format('Y-m-d' . ($hours ? $hours : ' 23:59:59'));
                break;
			default:
			    $targetTimeFrom = Yii::app()->localtime->getUTCNow('Y-m-d' . ($hours ? $hours : ' 00:00:00'));
				$targetTimeTo = $targetTimeFrom;
				break;

		}
		$result = array($targetTimeFrom,$targetTimeTo);

		return $result;


	}

	/**
	 * Get list of coaching session assignments filtered by list of users && courses.
	 * Optionally, filtered by various "session event in the past or future",
	 * like "when session starts" or "when session ends", etc.
	 *
	 * See bottom of the method for returned data structure.
	 *
	 * @param array|bool  $usersList FALSE means "don't care" or "all"
	 * @param array|bool  $coursesList FALSE means "don't care" or "all"
	 * @param bool        $sessionEvent
	 * @param bool|string $shiftType After, before
	 * @param bool|int    $shiftNumber Number of periods after/before
	 * @param bool|string $shiftPeriod The period type, e.g. day. hour, week
	 *
	 * @return array Very specific but known structure, hardcoded
	 */
	protected function getCoachingAssignmentsWithEventAfterBeforePeriod($usersList=false, $coursesList=false, $sessionEvent=false, $shiftType=false, $shiftNumber=false, $shiftPeriod=false) {
		$params = array();
		$select = array(
			'user.idst							AS idst',
			'user.userid 						AS username',
			'user.email							AS email',
			'csu_lang.value						AS language',
			'csu_timezone.value					AS timezone',
			'csu_format_locale.value			AS date_format_locale',
			'session.idSession					AS coaching_session',
			'session.idCourse					AS course'
		);

		$command = Yii::app()->db->createCommand()->from('core_user user');
		$command->select(implode(',', $select));

		// Join user settings table to get the user's "language"
		$command->leftJoin('core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')");

		// Join user settings table to get the user's "timezone"
		$command->leftJoin('core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')");

		// Join user settings table to get the user's "date_format_locale"
		$command->leftJoin('core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')");

		// Join coach session user and enrollment table
		$command->join('learning_course_coaching_session_user assignment', 'user.idst=assignment.idUser');
		$command->join('learning_course_coaching_session session', 'session.idSession=assignment.idSession');
		$command->join('learning_courseuser enrollment', 'user.idst=enrollment.idUser AND enrollment.idCourse=session.idCourse');

		// Filter by list of courses
		if ($coursesList !== false && is_array($coursesList))
			$command->andWhere('session.idCourse IN (' . implode(',',$coursesList) . ')');

		// Filter by list of users
		if ($usersList !== false && is_array($usersList))
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');

		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");

		// Event specific FILTERING/QUERIES
		switch ($sessionEvent) {
			case self::COACHING_EVENT_EXPIRED:
			case self::COACHING_EVENT_STARTING:
				// Filter BY enrollment statuses: must not be completed
				$command->andWhere('enrollment.status <> :completedStatus');
				$params[':completedStatus'] = LearningCourseuser::$COURSE_USER_END;
				break;
		}

		// If "after/before event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
		if ($shiftNumber && $shiftPeriod && $shiftType) {
			list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
			if ($targetTimeFrom) {
				switch ($sessionEvent) {
					case self::COACHING_EVENT_EXPIRED:
						$command->andWhere('session.datetime_end >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;
					case self::COACHING_EVENT_STARTING:
						$command->andWhere('session.datetime_start >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
				}
			}

			if ($targetTimeTo) {
				switch ($sessionEvent) {
					case self::COACHING_EVENT_EXPIRED:
						$command->andWhere('session.datetime_end <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;
					case self::COACHING_EVENT_STARTING:
						$command->andWhere('session.datetime_start <= :timeTo');
						$params[':timeFrom'] = $targetTimeFrom;
				}
			}
		}

		// Run the query
		$reader = $command->query($params);

		$assignments = array();
		$languages = array();
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$assignment = array(
				'idst' 					=> $row['idst'],
				'username'				=> trim($row['username'],'/'),
				'email'					=> $row['email'],
				'language'				=> $language,
				'timezone'				=> $row['timezone'],
				'date_format_locale'	=> $row['date_format_locale'],
				'course'				=> $row['course'],
				'user'					=> $row['idst'],
				'coaching_session'		=> $row['coaching_session']
			);

			$languages[$language] = 1;
			$assignments[] = $assignment;
		}

		return array(
			'assignments' => $assignments,
			'languages'	=> array_keys($languages),
		);
	}

	/**
	 * Get list of enrollments data structure filtered by list of users && courses.
	 * Optionally, filtered by various "enrollment event in the past or future",
	 * like "when user is subscribed" or "when course is completed", "when course is going to expire or is expired", etc.
	 *
	 * See bottom of the method for returned data structure.
	 *
	 * @param array|bool  $usersList FALSE means "don't care" or "all"
	 * @param array|bool  $coursesList FALSE means "don't care" or "all"
	 * @param bool        $enrollEvent
	 * @param bool|string $shiftType After, before
	 * @param bool|int    $shiftNumber Number of periods after/before
	 * @param bool|string $shiftPeriod The period type, e.g. day. hour, week
	 *
	 * @return array Very specific but known structure, hardcoded
	 */
    protected function getEnrollmentsWithEventAfterBeforePeriod(
        $usersList = false,
        $coursesList = false,
        $enrollEvent = false,
        $shiftType = false,
        $shiftNumber = false,
        $shiftPeriod = false,
        $forEvery = false
    )
    {

		// ----------

		$params = array();
		$select = array(
			'user.idst							AS idst',
			'user.userid 						AS username',
			'user.email							AS email',
			'csu_lang.value						AS language',
			'csu_timezone.value					AS timezone',
			'csu_format_locale.value			AS date_format_locale',
			'course.idCourse					AS course',
			'IF(enrollment.date_expire_validity IS NOT NULL, enrollment.date_expire_validity, IF(course.date_end != "0000-00-00", CONCAT(course.date_end, " 23:59:59"), course.date_end)) AS expire_at',
			'enrollment.date_inscr				AS subscribed_at',
			'enrollment.date_complete			AS completed_at',
		);

		// ----------


		$command = Yii::app()->db->createCommand()->from('core_user user');

		// Join user settings table to get the user's "language"
		$command->leftJoin('core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')");

		// Join user settings table to get the user's "timezone"
		$command->leftJoin('core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')");

		// Join user settings table to get the user's "date_format_locale"
		$command->leftJoin('core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')");

		// Filter by list of courses
		$command->join('learning_courseuser enrollment', 'user.idst=enrollment.idUser');
		if ($coursesList !== false && is_array($coursesList)) {
			$command->andWhere('enrollment.idCourse IN (' . implode(',',$coursesList) . ')');
		}

		// Get course data
		$command->join('learning_course course', 'course.idCourse=enrollment.idCourse');  // exisiting courses only

		// Filter by list of users
		if ($usersList !== false && is_array($usersList) && !empty($usersList)) {
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');
		}


		// ----------


		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");


		// -----------

		// Event specific FILTERING/QUERIES
		switch ($enrollEvent) {
			case self::ENROLL_EVENT_SUBSCRIBED:
				break;

			case self::ENROLL_EVENT_STUDENT_COMPLETED:
				// Filter status = END (completed)
				$command->andWhere('enrollment.status=:status');
				$params[':status'] = LearningCourseuser::$COURSE_USER_END;

				break;

			case self::ENROLL_EVENT_COURSE_EXPIRED:
				// Filter by course type: exclude Classrooms courses, since they do not maintain COURSE level expiration
				$command->andWhere("course.course_type NOT IN ('".LearningCourse::TYPE_CLASSROOM."','".LearningCourse::TYPE_WEBINAR."')");

				// Filter OUT courses by course->status (some cancelled, concluded, ... )
				$excludeStatuses = array(
					LearningCourse::$COURSE_STATUS_CANCELLED,
					LearningCourse::$COURSE_STATUS_CONCLUDED
				);
				$command->andWhere('course.status NOT IN ('  . implode(',', $excludeStatuses) . ')');

				// Filter BY enrollment statuses: must be subscribed/started but not completed
				$statuses = array(
					LearningCourseuser::$COURSE_USER_WAITING_LIST,
					LearningCourseuser::$COURSE_USER_CONFIRMED,
					LearningCourseuser::$COURSE_USER_SUBSCRIBED,
					LearningCourseuser::$COURSE_USER_BEGIN
				);
				$command->andWhere('enrollment.status IN (' . implode(',',$statuses) . ')');

				break;

			case self::ENROLL_EVENT_SUBSCRIBED_WAITING:
				$statuses = array(LearningCourseuser::$COURSE_USER_WAITING_LIST);
				$command->andWhere('enrollment.status IN (' . implode(',',$statuses) . ')');
				break;

		}

		// If "after/before event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
        //Old case before the digest notification
        if ($shiftNumber && $shiftPeriod && $shiftType && !$forEvery) {
			list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);

            if ($targetTimeFrom) {
				switch ($enrollEvent) {
					case self::ENROLL_EVENT_SUBSCRIBED:
					case self::ENROLL_EVENT_SUBSCRIBED_WAITING:
						$command->andWhere('enrollment.date_inscr >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;

					case self::ENROLL_EVENT_STUDENT_COMPLETED:
						$command->andWhere('enrollment.date_complete >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;

					case self::ENROLL_EVENT_COURSE_EXPIRED:
						$command->andWhere('IF(enrollment.date_expire_validity IS NOT NULL, enrollment.date_expire_validity >= :timeFrom, course.date_end <> \'0000-00-00\' AND concat(course.date_end, \' 23:59:59\') >= :timeFrom)');
						$params[':timeFrom'] = $targetTimeFrom;
						break;

				}
			}
			if ($targetTimeTo) {
				switch ($enrollEvent) {
					case self::ENROLL_EVENT_SUBSCRIBED:
					case self::ENROLL_EVENT_SUBSCRIBED_WAITING:
						$command->andWhere('enrollment.date_inscr <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;

					case self::ENROLL_EVENT_STUDENT_COMPLETED:
						$command->andWhere('enrollment.date_complete <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;

					case self::ENROLL_EVENT_COURSE_EXPIRED:
						$command->andWhere('IF(enrollment.date_expire_validity IS NOT NULL, enrollment.date_expire_validity <= :timeTo, course.date_end <> \'0000-00-00\' AND concat(course.date_end, \' 23:59:59\') <= :timeTo)');
						$params[':timeTo'] = $targetTimeTo;
						break;

				}
			}
            //digest notification events
        } elseif ($forEvery) {
            switch ($enrollEvent) {
                case self::ENROLL_EVENT_SUBSCRIBED:
                case self::ENROLL_EVENT_SUBSCRIBED_WAITING:
                    $command->andWhere('enrollment.date_inscr BETWEEN DATE_SUB(NOW(), INTERVAL 1 ' . $shiftPeriod . ') AND NOW()');
                    break;
                case self::ENROLL_EVENT_STUDENT_COMPLETED:
                    $command->andWhere('enrollment.date_complete IS NULL');
                    break;
                case self::ENROLL_EVENT_COURSE_EXPIRED:
                    $command->andWhere("
                        IF(
                            enrollment.date_expire_validity IS NOT NULL,
                            enrollment.date_expire_validity BETWEEN  NOW() and ADDDATE(NOW(),INTERVAL 1 " . $shiftPeriod . "),
                            course.date_end <> '0000-00-00'
                            AND
                            course.date_end
                            BETWEEN  NOW() and ADDDATE(NOW(),INTERVAL 1 " . $shiftPeriod . "
                            )
                        )
                        ");
                    break;
            }
        }
        //build sql
		$command->select(implode(',', $select));
        Log::_($command->getText());
		$reader = $command->query($params);

		$enrollments = array();
		$languages = array();
        //row formater
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$enrollment = array(
				'idst' 					=> $row['idst'],
				'username'				=> trim($row['username'],'/'),
				'email'					=> $row['email'],
				'language'				=> $language,
				'timezone'				=> $row['timezone'],
				'date_format_locale'	=> $row['date_format_locale'],
				'course'				=> $row['course'],
				'user'					=> $row['idst'],
				'expire_at'				=> $row['expire_at'],
				'subscribed_at'			=> $row['subscribed_at'],
				'completed_at'			=> $row['completed_at'],
			);
			$languages[$language] = 1;
			$enrollments[] = $enrollment;
		}

		return array(
				'enrollments' 		=> $enrollments,
				'languages'		=> array_keys($languages),
		);


	}

	/**
	 * Get ILT Sessions info filtered by users and courses.
	 *
	 * See bottom of the method for returned data structure.
	 *
	 * @param array|bool  $usersList FALSE means "don't care" or "all"
	 * @param array|bool  $coursesList FALSE means "don't care" or "all"
	 * @param bool        $retrievalScenario
	 * @param bool|string $shiftType After, before
	 * @param bool|int    $shiftNumber Number of periods after/before
	 * @param bool|string $shiftPeriod The period type, e.g. day. hour, week
	 *
	 * @return array Very specific but known structure, hardcoded
	 */
	public function getILTSessionsBeforeAfterData($usersList=false, $coursesList=false, $retrievalScenario=false, $shiftType=false, $shiftNumber=false, $shiftPeriod=false){
		$enrollments = array();
		$languages = array();

		$params = array();
		$select = array(
			'user.idst idst',
			'user.userid username',
			'user.email email',
			'course.idCourse course',
			'CONVERT_TZ(CONCAT_WS(" ", icsDate.day, icsDate.time_begin), icsDate.timezone, "UTC") AS utcStartTimestamp', // session start time in UTC
			'icsDate.name icsDateName', // session date name
			'ics.name icsName', // session name
			'ics.id_session id_session',
			'csu_lang.value AS language',
		);

		$command = Yii::app()->getDb()->createCommand()
			->select($select)
			->from(LtCourseSessionDate::model()->tableName().' icsDate')
			->join(LtCourseSession::model()->tableName().' ics', 'icsDate.id_session=ics.id_session')
			->join(LearningCourse::model()->tableName().' course', 'course.idCourse=ics.course_id')
			->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=course.idCourse')
			->join(CoreUser::model()->tableName().' user', 'user.idst=cu.idUser')
			->join("(SELECT `day` , id_session  FROM lt_course_session_date GROUP BY id_session)  icsDateFirst", "icsDate.`day` = icsDateFirst.`day` AND icsDate.id_session = icsDateFirst.id_session"  );

		// Join user settings table to get the user's "language"
		$command->leftJoin( 'core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')" );

		if($usersList !== false)
			$command->andWhere(array('in', 'user.idst', $usersList));

		if($coursesList !== false)
			$command->andWhere(array('in', 'course.idCourse', $coursesList));

		$command->group('icsDate.id_session'); // a session may have multiple dates, so get just a single date row per session that matches the criteria

		$intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);

		$command->andWhere('CONVERT_TZ(CONCAT_WS(" ", icsDate.day, icsDate.time_begin), icsDate.timezone, "UTC") >= :start AND CONVERT_TZ(CONCAT_WS(" ", icsDate.day, icsDate.time_begin), icsDate.timezone, "UTC") < :end', array(
			':start' => $intervalBeginAndEnd[0],
			':end' => $intervalBeginAndEnd[1]
		));

		$enrollments = $command->queryAll();

		foreach ($enrollments as $enrollment) {
			$languages[$enrollment['language']] = $enrollment['language'];
		}

		return array(
			'enrollments' => $enrollments,
			'languages' => array_keys($languages),
		);
	}


	public function getWebinarSessionsBeforeAfterData($shiftType = false, $shiftNumber = false, $shiftPeriod = false, $targetCourses = false)
	{
		$intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);
		$command = Yii::app()->db->createCommand()->
		select("ws.date_begin, wsu.id_user AS idst, cu.userid AS username, cu.email,ws.course_id AS course , ws.date_begin AS utcStartTimestamp,ws.name AS icsDateName,ws.name AS icsName,csu.value AS language, ws.id_session")->
		from(WebinarSession::model()->tableName() . ' ws')->
		join(WebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session=wsu.id_session')->
		join(CoreUser::model()->tableName() . ' cu', 'wsu.id_user=cu.idst')->
		leftJoin(CoreSettingUser::model()->tableName() . ' csu', 'cu.idst=csu.id_user AND csu.path_name="ui.language"')->
		where('ws.date_begin>="' . $intervalBeginAndEnd[0] . '" AND ws.date_begin<="' . $intervalBeginAndEnd[1] . '"');

		if($targetCourses !== false)
			$command->andWhere(array('in', 'ws.course_id', $targetCourses));

		$enrollments = $command->queryAll();

		$languages = array();
		if(empty($enrollments)){
			return array(
				'enrollments' 		=> $enrollments,
				'languages'		=> array_keys($languages),
			);
		}
		foreach ($enrollments as $enrollment) {
			$languages[$enrollment['language']] = $enrollment['language'];
		}
		return array(
			'enrollments' 		=> $enrollments,
			'languages'		=> array_keys($languages),
		);
	}


	public function getCoachWebinarSessionsInivationsBeforeDate($usersList=false, $coursesList=false, $shiftType = false, $shiftNumber = false, $shiftPeriod = false, $recipient = false)
	{
		$intervalBeginAndEnd = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);

		$enrollmentsCommand = Yii::app()->db->createCommand()->
		select("
			wsu.id_user AS idst,
		    cu.userid AS username,
		    cu.firstname,
		    cu.lastname,
		    cu.email,
		    lccs.idCourse AS course,
		    ws.date_begin AS utcStartTimestamp,
		    ws.name AS icsDateName,
		    ws.name AS icsName,
		    ws.name AS coaching_webinar_name,
		    csu.value AS language,
		    ws.id_session AS id_session,
		    ws.date_begin AS coaching_webinar_date_begin,
		    ws.date_end AS coaching_webinar_date_end,
		    lccs.idSession AS idCoachingSession,
		    lccs.datetime_start AS coaching_session_date_start,
		    lccs.datetime_end AS coaching_session_date_end,
		    CONCAT(coach.firstname, ' ', coach.lastname) AS coach_name,
		    coach.email AS coach_email
		")->
		from(LearningCourseCoachingWebinarSession::model()->tableName() . ' ws')->
		join(LearningCourseCoachingWebinarSessionUser::model()->tableName() . ' wsu', 'ws.id_session=wsu.id_session')->
		join(CoreUser::model()->tableName() . ' cu', 'wsu.id_user=cu.idst')->
		join(CoreSettingUser::model()->tableName() . ' csu', 'cu.idst=csu.id_user')->
		join(LearningCourseCoachingSession::model()->tableName() . ' lccs', 'ws.id_coaching_session=lccs.idSession')->
		join(CoreUser::model()->tableName() . ' coach', 'coach.idst=lccs.idCoach')->
		where('ws.date_begin>="' . $intervalBeginAndEnd[0] . '" AND ws.date_begin<="' . $intervalBeginAndEnd[1] . '" AND csu.path_name="ui.language"', array());

		if (!empty($recipient) && $recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) {
			$enrollmentsCommand->group('idst');
		} else  {
			$enrollmentsCommand->group('id_session');
		}

		// Filter by list of users
		if ($usersList !== false && is_array($usersList) && count($usersList) > 0) {
			$enrollmentsCommand->andWhere('cu.idst IN (' . implode(',',$usersList) . ')');
		}

		// Filter by list of courses
		if ($coursesList !== false && is_array($coursesList) && count($coursesList) > 0) {
			$enrollmentsCommand->andWhere('lccs.idCourse IN (' . implode(',',$coursesList) . ')');
		}

		$enrollments = $enrollmentsCommand->queryAll();

		$languages = array();
		if(empty($enrollments)){
			return array(
				'enrollments' 		=> $enrollments,
				'languages'		=> array_keys($languages),
			);
		}
		foreach ($enrollments as $key => $enrollment) {
			$languages[$enrollment['language']] = $enrollment['language'];
			$enrollments[$key]['username'] = trim($enrollment['username'],'/');
			$enrollments[$key]['user'] = $enrollment['idst'];

			$enrollments[$key]['coaching_webinar_date_begin'] = Yii::app()->localtime->toLocalDateTime($enrollment['coaching_webinar_date_begin']);
			$enrollments[$key]['coaching_webinar_date_end'] = Yii::app()->localtime->toLocalDateTime($enrollment['coaching_webinar_date_end']);


			$enrollments[$key]['coaching_session_dates'] = Yii::app()->localtime->stripTimeFromLocalDateTime(
				Yii::app()->localtime->toLocalDateTime($enrollment['coaching_session_date_start'])) . ' - '. Yii::app()->localtime->stripTimeFromLocalDateTime(
					Yii::app()->localtime->toLocalDateTime($enrollment['coaching_session_date_end'])) ;

			$webinarSessionModel = LearningCourseCoachingWebinarSession::model()->findByPk($enrollment['id_session']);
			$enrollments[$key]['coaching_webinar_url'] = $webinarSessionModel->getWebinarUrl($enrollment['idst']);
		}

		return array(
			'enrollments' 		=> $enrollments,
			'languages'		=> array_keys($languages),
		);
	}


	/**
	 * Gte list of users (and their data, not all but some), hwich has experienced some event in a given period or at all
	 *
	 * @param array $usersList
	 * @param string $userEvent
	 * @param string $shiftType
	 * @param integer$shiftNumber
	 * @param string $shiftPeriod
	 *
	 * @return array of DATA
	 */
	protected function getUsersWithEventInPeriod($usersList=false, $userEvent=false, $shiftType=false, $shiftNumber=false, $shiftPeriod=false) {

		// ----------
		$params = array();
		$select = array(
				'user.idst							AS idst',
				'user.userid 						AS username',
				'user.email							AS email',
				'user.register_date					AS register_date',
				'csu_lang.value						AS language',
				'csu_timezone.value					AS timezone',
				'csu_format_locale.value			AS date_format_locale',
		);

		// ----------

		$command = Yii::app()->db->createCommand()->from('core_user user');

		// Join user settings table to get the user's "language"
		$command->leftJoin('core_setting_user csu_lang', "(user.idst=csu_lang.id_user) AND (csu_lang.path_name='ui.language')");

		// Join user settings table to get the user's "timezone"
		$command->leftJoin('core_setting_user csu_timezone', "(user.idst=csu_timezone.id_user) AND (csu_timezone.path_name='timezone')");

		// Join user settings table to get the user's "date_format_locale"
		$command->leftJoin('core_setting_user csu_format_locale', "(user.idst=csu_format_locale.id_user) AND (csu_format_locale.path_name='date_format_locale')");

		// Filter by list of users
		if ($usersList !== false && is_array($usersList)) {
			$command->andWhere('user.idst IN (' . implode(',',$usersList) . ')');
		}


		// Valid users only
		$command->andWhere('user.valid=1');


		// ----------


		// Ignore anonymous user
		$command->andWhere("user.userid <> '/Anonymous'");

		// -----------

		// Event specific FILTERING/QUERIES
		switch ($userEvent) {
			case self::USER_EVENT_REGISTERED_TO_LMS:
				break;

		}


		// --------------

		// If "after/before event" filtering is requested,
		// Get target period of time to observe and apply the filtering
		// Allow open periods:  [FROM,infinity] and [infinity, TO]
		if ($shiftNumber && $shiftPeriod && $shiftType) {

			list($targetTimeFrom, $targetTimeTo) = $this->getTargetPeriodForShifted($shiftType, $shiftNumber, $shiftPeriod);

			if ($targetTimeFrom) {
				switch ($userEvent) {
					case self::USER_EVENT_REGISTERED_TO_LMS:
						$command->andWhere('user.register_date >= :timeFrom');
						$params[':timeFrom'] = $targetTimeFrom;
						break;
				}
			}
			if ($targetTimeTo) {
				switch ($userEvent) {
					case self::USER_EVENT_REGISTERED_TO_LMS:
						$command->andWhere('user.register_date <= :timeTo');
						$params[':timeTo'] = $targetTimeTo;
						break;
				}
			}
		}

		// -----------


		$command->select(implode(',', $select));

		// ----------


		$reader = $command->query($params);

		// ----------

		$list = array();
		$languages = array();
		foreach ($reader as $row) {
			$language = isset($row['language']) ? $row['language'] : $this->defaultLanguage;
			$user = array(
					'idst' 					=> $row['idst'],
					'username'				=> trim($row['username'],'/'),
					'email'					=> $row['email'],
					'language'				=> $language,
					'timezone'				=> $row['timezone'],
					'date_format_locale'	=> $row['date_format_locale'],
					// --- YES!! (below)
					'user'					=> $row['idst'],
					'register_date'			=> $row['register_date'],
			);
			$languages[$language] = 1;
			$list[] = $user;
		}


		// ----------


		return array(
				'users' 			=> $list,
				'languages'			=> array_keys($languages),
		);


	}



	/**
	 * Return recipients collection data (resulting in the same amount of emails) for every
	 * single user in a list (i.e. there could be MORE than one recipient per user).
	 * There is no course relation here!
	 *
	 * @param array $usersData
	 * @param integer $recipientType
	 * @param array $relatedFields
	 *
	 * @return array
	 */
	protected function helperGetRecipientsForListOfUsers($usersData, $recipientType, $relatedFields=array()) {

		$data = array();
		$data['languages'] 	= array();
		$data['users']		= array();  // recipients!!

		switch ($recipientType) {

			// GODADMINS
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				// YES, before the cycles!!
				$targets = $this->helperGetAllGodAdmins(); // recipients
				foreach ($usersData['users'] as $user) {
					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($user[$f]) ? $user[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}
				}
				$data['languages'] = $targets['languages'];
				break;

			// POWER USERS of target users
			case CoreNotification::NOTIFY_RECIPIENTS_POWER_USER:
				foreach ($usersData['users'] as $user) {

					$targets = $this->helperGetPowerUsersOfUsersData(array($user['idst']));

					foreach ($targets['users'] as $tmpData) {
						$tmp =  array(
							'idst' 			=> $tmpData['idst'],		// instructor
							'username' 		=> $tmpData['username'],
							'email' 		=> $tmpData['email'],
							'language' 		=> $tmpData['language'],
						);
						foreach ($relatedFields as $f) {
							$tmp[$f] = isset($user[$f]) ? $user[$f] : '??';
						}
						$data['users'][] 	= $tmp;
					}

					if($this->notification->puProfileId != 0)
						$data['users'] = $this->filterPowerUsersByProfile($data['users'], $this->notification->puProfileId);

					$data['languages'] = array_merge($data['languages'], $targets['languages']);
				}
				break;

			// USERS THEMSELVES
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$data['users'] 		= $usersData['users'];
				$data['languages']	= $usersData['languages'];
				break;


		}

		$data['languages'] = array_unique($data['languages']);

		return $data;
	}


	/**
	 * Some delivery system may accept some special properties (such as avatar images, author names, attachments etc.).
	 * This method provide a list of such special properties availble for the current notification handler.
	 * To be overloaded in subclasses if needed.
	 *
	 * @param $recipient Array of data related to the particular recipient that is going to be notified by email
	 * @param array $metadata Generic data from the specific notification handler (to be used in overloaded methods of subclasses)
	 * @return array
	 */
	public function getSpecialProperties($recipient, $metadata = array()) {
		return array();
	}


	/**
	 * Return allowed shortcodes in certains text format.
	 * If no filter is applied to a shortcode, then it is considered valid for every text format.
	 * Format of filtering array:
	 *   {shortcode name} => array( {list of allowed text formats} )
	 *
	 * @return array
	 */
	public function getShortcodesFilter() {
		return array();
	}


	/**
	 * Universal, one-for-all shortcodes data extractor.
	 * If you need to do something specififc, override in your specific handler.
	 * Keep in mind, you may need to call the parent::method first, to cover common shortcodes and then continue with your logic & data.
	 *
	 * Ideally, the incoming parameter consists of recipient's specific data:
	 *    idst
	 *    username
	 *    email
	 *    language
	 *    timezone
	 *    date_format_locale
	 *
	 * Plus other related data, like 'user', 'course', 'category' and so on, related to specific handler.
	 * Note: NO MODELS are passed, always some ID or primary key. So, above, 'category' is the categpory ID, not model!
	 *
	 * @param array $recipient Array of data related to the particular recipient that is going to be notified by email
	 * @param array $metadata Generic data from the specific notification handler (to be used in overloaded methods of subclasses)
	 * @param string $textFormat specifies the format of the message text requested by the notification. Some shortcodes may be filled differently depending on this parameter.
	 */
    public function getShortcodesData($recipient, $metadata = array(), $textFormat = false)
    {

        /* @var $groupModel CoreGroup */
        /* @var $userModel CoreUser */
        /* @var $courseModel LearningCourse */
        /* @var $loModel LearningOrganization */

        /* @var $ltSession WebinarSession|LtCourseSession */
        $result = array();
        $result[CoreNotification::SC_LMS_LINK] = Docebo::createAbsoluteLmsUrl('site/index');
        // USER
        $userModel = CoreUser::model()->findByPk((int)$recipient['user']);

        if ($userModel) {
            $result[CoreNotification::SC_FIRSTNAME] = $userModel->firstname;
            $result[CoreNotification::SC_LASTNAME] = $userModel->lastname;
            $result[CoreNotification::SC_USER_EMAIL] = $userModel->email;
            $result[CoreNotification::SC_USERNAME] = $userModel->getClearUserId();
            $result[CoreNotification::SC_USER_REGISTER_DATE] = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'],
                $userModel->register_date);

            $verificationLink = $userModel->getVerificationLink();
            if (!empty($verificationLink)) {
                switch ($textFormat) {
                    case NotificationTransportManager::TEXT_FORMAT_HTML:
                        //nothing to do here, the verification link has already been generated as HTML <a> link
                        break;
                    case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
                        // The verification link is already generated as HTML <a> link in user model class, hence we have to somehow
                        // convert it for markdown syntax output. The easiest way is to extract key elements ('href' and text contained
                        // in the link tag) from original content and rebuild the link from scratch in the new syntax.
                        $href = '';
                        $text = strip_tags($verificationLink);
                        if (preg_match('/<a\s+href=["\']([^"\']+)["\']/i', $verificationLink, $match)) {
                            if (!empty($match)) {
                                $href = $match[1];
                            }
                        }
                        $new_sc_content = '';
                        if (!empty($href)) {
                            $new_sc_content = (!empty($text) ? '<' . $href . '|' . $text . '>' : $href); //slack style link
                        }
                        $verificationLink = $new_sc_content;
                        break;
                    default:
                        //plain text, no link syntax available : just retrieve the URL, no other options possible
                        $href = '';
                        if (preg_match('/<a\s+href=["\']([^"\']+)["\']/i', $verificationLink, $match)) {
                            if (!empty($match)) {
                                $href = $match[1];
                            }
                        }
                        $verificationLink = $href;
                        break;
                }
            }
            $result[CoreNotification::SC_VERIFICATION_LINK] = $verificationLink;

            if (isset($recipient['passwordPlainText'])) {
                $result[CoreNotification::SC_USER_PASSWORD] = $recipient['passwordPlainText'];
            }
        } elseif ($this->notification->type == CoreNotification::NTYPE_NEW_USER_CREATED) {
            $userModel = CoreUserTemp::model()->findByPk((int)$recipient['user']);
            if ($userModel) {
                $result[CoreNotification::SC_FIRSTNAME] = $userModel->firstname;
                $result[CoreNotification::SC_LASTNAME] = $userModel->lastname;
                $result[CoreNotification::SC_USER_EMAIL] = $userModel->email;
                $result[CoreNotification::SC_USERNAME] = $userModel->getClearUserId();
                $result[CoreNotification::SC_USER_REGISTER_DATE] = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'],
                    $userModel->request_on);

                if (isset($recipient['passwordPlainText'])) {
                    $result[CoreNotification::SC_USER_PASSWORD] = $recipient['passwordPlainText'];
                }
            }
        }

        // GROUP
        $groupModel = CoreGroup::model()->findByPk((int)$recipient['group']);
        if ($groupModel) {
            $result[CoreNotification::SC_GROUPNAME] = $groupModel->relativeId($groupModel->relativeId());
            $result[CoreNotification::SC_GROUPID] = $groupModel->relativeId();
        }
        // COURSE
        $courseModel = LearningCourse::model()->findByPk((int)$recipient['course']);
        if ($courseModel) {
            $result[CoreNotification::SC_COURSENAME] = $courseModel->name;
            $result[CoreNotification::SC_COURSE_LINK] = Docebo::createAbsoluteLmsUrl('//player',
                array('course_id' => $courseModel->idCourse));
        }
        if ($userModel && $courseModel) {
            $enrollment = LearningCourseuser::model()->findByAttributes(array(
                'idUser' => $recipient['user'],
                'idCourse' => $recipient['course'],
            ));
            if ($enrollment) {
                $result[CoreNotification::SC_USER_COURSE_LEVEL] = $enrollment->getLevelLabel();
                $final_score = !is_null($enrollment->score_given) ? $enrollment->score_given : '';
                $result[CoreNotification::SC_FINAL_SCORE] = $final_score;
            }
        }


        // COACHING SESSION
        if ($recipient['coaching_session'] instanceof LearningCourseCoachingSession) {
            $coachingSession = $recipient['coaching_session'];
        } else {
            $coachingSession = LearningCourseCoachingSession::model()->findByPk((int)$recipient['coaching_session']);
        }
		if($coachingSession) {
			$coach = $coachingSession->coach;
			$result[CoreNotification::SC_COACH_NAME] = $coach ? $coach->getFullName() : '';
			$result[CoreNotification::SC_COACH_EMAIL] = $coach ? $coach->email : '';
			$result[CoreNotification::SC_COACH_SESSION_DATES] = Yii::t("standard", "_FROM") . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSession->datetime_start))
				. " " . Yii::t("standard", "_TO") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($coachingSession->datetime_end));

			$targetSession = $recipient['coaching_session_target'];
			if($targetSession instanceof LearningCourseCoachingSession)
				$result[CoreNotification::SC_COACH_SESSION_TARGET] = Yii::t("standard", "_FROM") . " " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($targetSession->datetime_start))
					. " " . Yii::t("standard", "_TO") . "  " . Yii::app()->localtime->stripTimeFromLocalDateTime(Yii::app()->localtime->toLocalDateTime($targetSession->datetime_end));
		}

		// FORUM THREAD
		if (isset($recipient['thread']) && $recipient['thread'] > 0) {
			$threadModel = LearningForumthread::model()->findByPk((int)$recipient['thread']);
			if ($threadModel) {
				$forumModel = LearningForum::model()->findByPk($threadModel->idForum);
				$forumLink = Docebo::createAbsoluteLmsUrl('forum/thread/index', array('forum_id' => $forumModel->idForum));
				$threadLink = Docebo::createAbsoluteLmsUrl('forum/messages', array('thread_id' => $threadModel->idThread));

				$result[CoreNotification::SC_FORUM_THREAD] = $threadModel->title;
				$result[CoreNotification::SC_FORUM] = $forumModel->title;
				$result[CoreNotification::SC_FORUM_LINK] = $forumLink;
				$result[CoreNotification::SC_FORUM_THREAD_LINK] = $threadLink;
				switch ($textFormat) {
					case NotificationTransportManager::TEXT_FORMAT_HTML:
						$result[CoreNotification::SC_FORUM_COMMENT] = $recipient['message'];
						break;
					case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					default:
						$result[CoreNotification::SC_FORUM_COMMENT] = strip_tags($recipient['message']);
						break;
				}
			}
		}


		// LEARNINGPLAN
		$learningPlanModel 	= LearningCoursepath::model()->findByPk((int) $recipient['plan']);
		if ($learningPlanModel) {
			if (isset($recipient['id_certificate']) &&  $userModel) {
				$certificateModel 	= LearningCertificateAssignCp::model()->findByAttributes(array(
					'id_user' 			=> $userModel->idst,
					'id_path' 			=> $learningPlanModel->id_path,
					'id_certificate'	=> $recipient['id_certificate'],
				));
				if ($certificateModel) {
					$downloadUrl = Docebo::createAbsoluteLmsUrl('curricula/show/downloadCertificate', array('id_path' => $learningPlanModel->id_path, 'id_user' => $userModel->idst));
					$result[CoreNotification::SC_CERTIFICATE_URL]= $downloadUrl;
				}
			}
			$result[CoreNotification::SC_LEARNINGPLAN_NAME]	= $learningPlanModel->path_name;
			$result[CoreNotification::SC_LEARNINGPLAN_URL]	= Docebo::createAbsoluteLmsUrl('curricula/show');
			$result[CoreNotification::SC_LEARNINGPLAN_USER_VALIDITY_DATE_START]	= $recipient['date_begin_validity'] == '0000-00-00 00:00:00' ? '' : Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $recipient['date_begin_validity']);
			$result[CoreNotification::SC_LEARNINGPLAN_USER_VALIDITY_DATE_END]	= $recipient['date_end_validity'] == '0000-00-00 00:00:00' ? '' : Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $recipient['date_end_validity']);
		}


		// TRANSACTION MODEL (from some purchase probably..)
		if (isset($recipient['transaction'])) {
			$transactionModel = EcommerceTransaction::model()->findByPk($recipient['transaction']);
			if ($transactionModel) {
				$receiptContent = $transactionModel->getOrderReceiptContent();
				switch ($textFormat) {
					case NotificationTransportManager::TEXT_FORMAT_HTML:
						$result[CoreNotification::SC_ORDER_RECEIPT_CONTENT] = nl2br($receiptContent);
						break;
					case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					default:
						$result[CoreNotification::SC_ORDER_RECEIPT_CONTENT] = $receiptContent;
						break;
				}

				// Also, we may have separate receipt data fields/placeholders
				$receiptInfo = $transactionModel->getOrderReceiptInfo();
				foreach ($receiptInfo as $shortCode => $receiptValue) {
					$result[$shortCode] = $receiptValue;
				}

				//order items text
				$orderItemsContent = $transactionModel->getOrderItemsText();
				switch ($textFormat) {
					case NotificationTransportManager::TEXT_FORMAT_HTML:
						$result[CoreNotification::SC_ORDER_ITEMS] = nl2br($orderItemsContent);
						break;
					case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					default:
						$result[CoreNotification::SC_ORDER_ITEMS] = $orderItemsContent;
						break;
				}
			}
		}


		// LEARNING OBJECT
		$loModel = LearningOrganization::model()->findByPk($recipient['learningobject']);
		if ($loModel) {
			$result[CoreNotification::SC_LO_TITLE] 	= $loModel->title;
			$result[CoreNotification::SC_LO_TYPE]	= LearningOrganization::objectTypeValue($loModel->objectType);
			$result[CoreNotification::SC_TEST_NAME] = $loModel->title;
		}

		if($userModel && $loModel){
			$trackModel = LearningCommontrack::model()->findByAttributes(array(
					'idReference'	=>	$loModel->idOrg,
					'objectType' 	=> 	LearningOrganization::OBJECT_TYPE_TEST,
					'idUser'		=>	$userModel->idst,
			));

			$commonTrackReference = $loModel->idOrg;
			if($courseModel && $courseModel->idCourse != $loModel->idCourse){
				$slaveTracks = LearningCommontrack::model()->findAllByAttributes(array(
							'idMasterOrg' 	=> $trackModel->idReference,
							'idUser'		=> $userModel->idst,
				));

				foreach($slaveTracks as $slaveTrack){
					$originOrganization = LearningOrganization::model()->findByPk($slaveTrack->idReference);
					if($originOrganization && $originOrganization->idCourse == $courseModel->idCourse){
						$commonTrackReference = $originOrganization->idOrg;
					}
				}

			}

			$utcVariant = Yii::app()->db->createCommand()->select('dateAttempt')->from(LearningCommontrack::model()->tableName())->where('idReference = '.$commonTrackReference.' AND objectType = "'.LearningOrganization::OBJECT_TYPE_TEST.'" AND idUser = '.$userModel->idst)->queryScalar();

			$result[CoreNotification::SC_TEST_COMPLETION_DATE] 	= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $utcVariant);
			$result[CoreNotification::SC_TEST_SCORE] 			= $trackModel->score;


			$testTrack = LearningTesttrack::model()->with(array('questsWithTitles'))->findByAttributes(array(
					'idReference' => $loModel->idOrg,
					'idUser' => $userModel->idst
			));
			$test = LearningTest::model()->findByPk($loModel->idResource);
			$params = array(
					'organization' => $loModel,
					'userId' => $userModel->idst,
					'testTrack' => $testTrack,//$trackModel,
					'commonTrack' => $trackModel,
					'questionsLang' => LearningTestquest::getQuestionTypesTranslationsList(),
					'test' => $test,
					'isTeacher' => true//$isTeacher
			);
			if(isset($metadata['sendType']) && ($metadata['sendType'] == CoreNotification::NOTIFY_TYPE_EMAIL || $metadata['sendType'] == CoreNotification::NOTIFY_TYPE_INBOX)){
				if (Yii::app() instanceof CWebApplication) {
					$result[CoreNotification::SC_TEST_RESULTS] = Yii::app()->getController()->renderPartial('plugin.NotificationApp.views._sc_test_results', $params, true);//$trackModel->score;
				} else if (Yii::app() instanceof CConsoleApplication) {
					$result[CoreNotification::SC_TEST_RESULTS] = Yii::app()->getCommand()->renderFile('plugin/NotificationApp/views/_sc_test_results.php', $params, true);//Yii::getPathOfAlias('NotificationApp') . '/views/_sc_test_results.php', $params, true);//$trackModel->score;
				}
			}
			else{
				$result[CoreNotification::SC_TEST_RESULTS] = '';
			}


		}

		if(isset($recipient['answertrack']['idTrack']) && isset($recipient['answertrack']['idQuest'])){
			$result[CoreNotification::SC_QUESTION_SCORE] = LearningTesttrackAnswer::getScoreByTrackAndQuestion($recipient['answertrack']['idTrack'],$recipient['answertrack']['idQuest']);//(string)($answertrack->score_assigned);
			$answerModel = LearningTesttrackAnswer::model()->findByAttributes(array(
					'idTrack' => $recipient['answertrack']['idTrack'],
					'idQuest' => $recipient['answertrack']['idQuest'],
			));
			$result[CoreNotification::SC_QUESTION_ANSWER] 	= $answerModel->more_info;
			$questModel = LearningTestquest::model()->findByPk($answerModel->idQuest);
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$questionText = $questModel->title_quest;
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
				default:
					$questionText = strip_tags($questModel->title_quest);
					break;
			}
			$result[CoreNotification::SC_QUESTION_TITLE] 	= $questionText;
		}



		// USER WAITING LMS SUBSCRIPTION APPROVAL
		$userTempModel = CoreUserTemp::model()->findByPk($recipient['userwaiting']);
		if ($userTempModel) {
			$result[CoreNotification::SC_FIRSTNAME] 		= $userTempModel->firstname;
			$result[CoreNotification::SC_LASTNAME] 			= $userTempModel->lastname;
			$result[CoreNotification::SC_USER_EMAIL] 		= $userTempModel->email;
			$result[CoreNotification::SC_USERNAME] 			= trim($userTempModel->userid, '/');
		}


		// COURSE CATEGORY
		if (isset($recipient['category'])) {
			$courseCategory = LearningCourseCategory::model()->findByAttributes(array(
				'idCategory' 	=> $recipient['category'],
				'lang_code'		=> $recipient['language'],
			));
			if ($courseCategory) {
				$result[CoreNotification::SC_CATEGORYNAME] = $courseCategory->translation;
			}
		}

		//LT Session
		$ltSession = null;
		if (isset($recipient['id_session']))
		{
			if($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
				$ltSession = WebinarSession::model()->findByPk($recipient['id_session']);
			} else {
				$ltSession = LtCourseSession::model()->findByPk($recipient['id_session']);
			}

			if ($ltSession)
			{
				$result[CoreNotification::SC_SESSION_NAME] = $ltSession->name;
				$sessionDates = $ltSession->compileShortcodeDates($textFormat);

				$result[CoreNotification::SC_SESSION_DATES] = $sessionDates;
				if ($courseModel->course_type == LearningCourse::TYPE_WEBINAR) {
					// webinars don't have locations
					$locations = array();
				} else {
					$locations = $ltSession->getLocations();
				}
				if(count($locations)){
					if(count($locations)>1){
						$result[CoreNotification::SC_SESSION_LOCATION] = Yii::t('classroom', 'Multiple locations');
					}else{
						$firstLocation = reset($locations);
						$result[CoreNotification::SC_SESSION_LOCATION] = $firstLocation['name'];
					}
				}
			}
		}

		//LT Session - in case of delete, we pass the data directly, as the model is already deleted
		if (isset($recipient['session_data']))
		{
			$session = $recipient['session_data'];
			$result[CoreNotification::SC_SESSION_NAME] = $session['name'];
			$result[CoreNotification::SC_SESSION_DATES] = $session['sessionDates'];
		}


		// "COMPLETED AT..." related information; like "student completed a course at..."
		if (isset($recipient['completed_at'])) {
			$result[CoreNotification::SC_COMPLETED_AT] 	= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $recipient['completed_at']);
		}

		// "EXPIRE(D) AT..." related information; like "course expire(s) at..."
		if (isset($recipient['expire_at'])) {
			$result[CoreNotification::SC_EXPIRE_AT] = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $recipient['expire_at']);
		}

		// "SUBSCRIBED AT..." related information; like "user subscribed to a course..."
		if (isset($recipient['subscribed_at'])) {
			$result[CoreNotification::SC_SUBSCRIBED_AT] = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $recipient['subscribed_at']);
		}

		// 1. Deliverable object
		// 2. Test answer for test question of the type "Free Text"
		if (isset($recipient['idObject'])) {

			$idObjectType = isset($recipient['idObjectType']) && $recipient['idObjectType'] ? $recipient['idObjectType'] : false;

			switch($idObjectType){
				case InstructorEvaluatedAssignment::TYPE_FREE_TEXT_ANSWER_TRACK:
					$track = Yii::app()->getDb()->createCommand()
						->select('score')
						->from(LearningTesttrack::model()->tableName())
						->where('idTrack=:idTrack', array(':idTrack'=>$recipient['idObject']))
						->queryRow();

					if(!empty($track)){
						$result[CoreNotification::SC_EVALUATION] = $track['score'];
						$result[CoreNotification::SC_EVALUATION_MESSAGE] = null;
						$result[CoreNotification::SC_ASSIGNMENT_STATUS] = null;
					}
					break;
				case InstructorEvaluatedAssignment::TYPE_ASSIGNMENT:
				default:
					$deliverableObject = Yii::app()->db->createCommand()
						->select('name, created, evaluation_score, evaluation_comments, evaluation_status')
						->from('learning_deliverable_object')
						->where('idObject=:id', array(':id' => $recipient['idObject']))
						->queryRow();

					$result[CoreNotification::SC_EVALUATION_MESSAGE] = $deliverableObject['evaluation_comments'];
					$result[CoreNotification::SC_EVALUATION] = $deliverableObject['evaluation_score'];
					$result[CoreNotification::SC_ASSIGNMENT_STATUS] = $deliverableObject['evaluation_status']==1 ? Yii::t('test','Assignment passed') : Yii::t('test','Assignment failed');
					$result[CoreNotification::SC_ASSIGNMENT_TITLE] = $deliverableObject['name'];
					$result[CoreNotification::SC_ASSIGNMENT_SUBMISSION_DATE] = Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $deliverableObject['created']);
			}
		}

		if(isset($recipient['deliverableObject']) && ($recipient['deliverableObject'] instanceof LearningDeliverableObject)) {
			$deliverable = $recipient['deliverableObject']; /* @var $deliverable LearningDeliverableObject */
			$result[CoreNotification::SC_ASSIGNMENT_TITLE] = $deliverable->name;
			$result[CoreNotification::SC_ASSIGNMENT_SUBMISSION_DATE] = $deliverable->created;
		}

		//check for blog post
		if (isset($recipient['post'])) {
			$post = MyblogPost::model()->findByPk($recipient['post']);
			if (!empty($post)) {
				$result[CoreNotification::SC_BLOG_DATE_PUBLISHED] = $post->posted_on;
				// Blog attributes
				$result[CoreNotification::SC_BLOG_TITLE] = $post->title;
				$result[CoreNotification::SC_BLOG_WRITER] = $post->user->getUserNameFormatted();
				switch ($textFormat) {
					case NotificationTransportManager::TEXT_FORMAT_HTML:
						$result[CoreNotification::SC_BLOG_TEXT] = $post->content;
						break;
					case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					default:
						$result[CoreNotification::SC_BLOG_TEXT] = strip_tags($post->content);
						break;
				}
			}
			// Check if the comment is engaged in the notification
			if (isset($recipient['comment'])) {
				$comment = MyblogComment::model()->findByPk($recipient['comment']);
				if (!empty($comment)) {
					$result[CoreNotification::SC_BLOG_COMMENT_USER] = $comment->user->getUserNameFormatted();
					switch ($textFormat) {
						case NotificationTransportManager::TEXT_FORMAT_HTML:
							$result[CoreNotification::SC_BLOG_COMMENT] = $comment->content;
							break;
						case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
						default:
							$result[CoreNotification::SC_BLOG_COMMENT] = strip_tags($comment->content);
							break;
					}
				}
			}
		}


		// Let plugins resolve their custom tags and return the $result array filled in properly
		Yii::app()->event->raise('ResolveNotificationTags', new DEvent($this, array(
			'notification' => $this->notification,
			'result' => &$result,
			'recipient' => $recipient,
			'models' => array(
				'group' => $groupModel,
				'userModel' => $userModel,
				'courseModel' => $courseModel,
				'loModel' => $loModel,
				'sessionModel'=>$ltSession,
			)
		)));

		return $result;
	}



	protected function getTeachersDataFromUser($userId = null, $targetSessionId = null, $courseId = null){
		$recipCollection['users'] = array();
		$recipCollection['languages'] = array();

		if ($userId === null || $targetSessionId === null){
			return $recipCollection;
		}
		$levels = array(
				LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
				LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
				LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
				LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
		);

		$recipCollection = array('users' => array(), 'languages' => array());
		if($courseId){
			$courseModel = LearningCourse::model()->findByPk($courseId);
			if(!$courseModel) return $recipCollection;
			switch($courseModel->course_type) {
				case LearningCourse::TYPE_CLASSROOM:
				case LearningCourse::TYPE_ELEARNING:
					$sessionModel = LtCourseSession::model()->findByAttributes(array(
							'id_session' => $targetSessionId,
							'course_id' => $courseId
					));
					$targets = $this->helperGetUsersData(false, array($sessionModel->course_id), $levels, false, array($targetSessionId));
					break;
				case LearningCourse::TYPE_WEBINAR:
					$sessionModel = WebinarSession::model()->findByAttributes(array(
							'id_session' => $targetSessionId,
							'course_id' => $courseId
					));
					$targets = $this->helperGetUsersData(false, array($sessionModel->course_id), $levels);
					break;
			}

		}else {
			$sessionModel = LtCourseSession::model()->findByPk($targetSessionId);
			$targets = $this->helperGetUsersData(false, array($sessionModel->course_id), $levels, false, array($targetSessionId));
		}
		foreach ($targets['users'] as $tmpData) {
			$tmp = array(
					'idst' => $tmpData['idst'],
					'username' => $tmpData['username'],
					'email' => $tmpData['email'],
					'language' => $tmpData['language'],
					'course' => $sessionModel->course_id,
					'id_session' => $targetSessionId,
					'user' => $userId,
			);
			$recipCollection['users'][] = $tmp;
		}
		$recipCollection['languages'] = array_merge($recipCollection['languages'], $targets['languages']);

		return $recipCollection;
	}



	/**
	 * Formats the webinar session details according to the provided data
	 * @param $data
	 */
	protected function formatWebinarSessionDetails($data, $textFormat = false) {

		if (empty($textFormat)) $textFormat = NotificationTransportManager::TEXT_FORMAT_HTML; //TODO: this is just for backward compatibility, but should be removed later

		$details = "";
		if (!empty($data)) {
			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					if (count($data) == 1) {
						$details = "<strong>" . $data[0]['label'] . "</strong>: " . $data[0]['value'];
					} else {
						$details .= "<ul>";
						foreach ($data as $detail) {
							$details .= "<li><strong>" . $detail['label'] . "</strong>: " . $detail['value'] . "</li>";
						}
						$details .= "</ul>";
					}
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					if (count($data) == 1) {
						$details = "*" . $data[0]['label'] . "* : " . $data[0]['value'];
					} else {
						foreach ($data as $detail) {
							$details .= "\t *" . $detail['label'] . "* : " . $detail['value'] . "\n";
						}
					}
					break;
				default:
					if (count($data) == 1) {
						$details = $data[0]['label'] . ": " . $data[0]['value'];
					} else {
						foreach ($data as $detail) {
							$details .= "\t" . $detail['label'] . ": " . $detail['value'] . "\n";
						}
					}
					break;
			}
		}

		return $details;
	}



	protected function getWebinarDetailsTagContentsFromSessionModel(WebinarSession $sessionModel, $textFormat = false){

		if (empty($textFormat)) $textFormat = NotificationTransportManager::TEXT_FORMAT_HTML; //TODO: this is just for backward compatibility, but should be removed later

		$sessionDates = $sessionModel->getDates();

		$details = '';
		foreach ($sessionDates as $date) {
			$webinarToolId = $date->webinar_tool;

			$webinar = WebinarTool::getById($webinarToolId);

			$dataForThisDate = $webinar->getAttendeeInfoForRoom($date->id_tool_account, CJSON::decode($date->webinar_tool_params));
			$detailsForThisDate = $this->formatWebinarSessionDetails($dataForThisDate, $textFormat);

			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$details .= $dataForThisDate ? '<p><strong>' . $date->day . '</strong></p>' . $detailsForThisDate : '';
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					$details .= $dataForThisDate ? '*' . $date->day . '*'."\n" . $detailsForThisDate : '';
					break;
				default:
					$details .= $dataForThisDate ? $date->day . "\n" . $detailsForThisDate : '';
					break;
			}
		}
		return $details;
	}



	protected function getWebinarSessionDateSessionInfo($recipient, $sessionDates, $textFormat = false) {

		if (empty($textFormat)) $textFormat = NotificationTransportManager::TEXT_FORMAT_HTML; //TODO: this is just for backward compatibility, but should be removed later

		$details = '';
		$dateInfo = '';
		foreach ($sessionDates as $date) {
			$detailsForThisDate = '';
			try {
				$webinar = WebinarTool::getById($date->webinar_tool);
				$dataForThisDate = $webinar->getAttendeeInfoForRoom($date->id_tool_account, CJSON::decode($date->webinar_tool_params));
				$detailsForThisDate = $this->formatWebinarSessionDetails($dataForThisDate, $textFormat);
			} catch (CHttpException $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			}

			switch ($textFormat) {
				case NotificationTransportManager::TEXT_FORMAT_HTML:
					$details .= '<p><strong>' . Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->day) . '</strong><br>'
						.'<strong>Tool: </strong>' . $date->getToolName() . '<br>' . $detailsForThisDate . '</p>';
					$dateInfo .= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcBeginTimestamp)
						. ' - '. Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcEndTimestamp)
						. ' - ' . $date->getToolName() . '<br>';
					break;
				case NotificationTransportManager::TEXT_FORMAT_MARKDOWN:
					$details .= '*' . Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->day) . '*'."\n"
						.'*Tool* : ' . $date->getToolName() . "\n" . $detailsForThisDate . "\n";
					$dateInfo .= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcBeginTimestamp)
						. ' - '. Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcEndTimestamp)
						. ' - ' . $date->getToolName() . "\n";
					break;
				default:
					$details .= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->day) . "\n"
						.'Tool: ' . $date->getToolName() . "\n" . $detailsForThisDate . "\n";
					$dateInfo .= Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcBeginTimestamp)
						. ' - '. Yii::app()->localtime->toUserLocalDatetime($recipient['idst'], $date->utcEndTimestamp)
						. ' - ' . $date->getToolName() . "\n";
					break;
			}

		}

		return array($details, $dateInfo);
	}
}






/**
 * Use this class as a response container from notification handlers.
 *
 */
class NotificationHandlerResponse extends stdClass {
	/**
	 *  Array of recipients, where the key is core_user::idst and each element is having **very** specific structure:
	 *
	 * 	Key					Value
	 * 	===					=====
	 * 	// Mandatory (recipient's data)
	 * 	idst				core_user::idst
	 * 	username			core_user::userid
	 * 	email				core_user::email
	 * 	language			core_setting_user::value for ui.language path name
	 *
	 * 	// Most of the time you also want to add these (recipient's related)
	 *	'timezone'				=> user's TZ
	 *	'date_format_locale'	=> user's format
	 *
	 * 	// And of course you can anything else that your handlers require, some related information, like
	 * 	'user'				=> <ID of a single user related to the "idst" in some way>
	 *  'course'			=> <ID of a single course related to the "idst" in some way>
	 *	'category'			=> ...
	 *  'thread'			=> ...
	 *  ...
	 *
	 * Example:
	 * 		[] => array(
	 * 			idst		=> 12303,
	 * 			username 	=> 'johndoe',
	 * 			email		=> 'johndoe@someemail.com'
	 * 			language	=> 'german',
	 * 			user		=> 12999   	// user related to the recipient & notification, like the user that has been subscribed to a course
	 * 			course		=> 999		// course related to the recipient & notification,... see above
	 * 			....
	 * 		)
	 *
	 *	At the end of the day, this array of data is returned to Notification Job Handler wich "consume" it on its discretion, using whatever part of data
	 *	it needs. The CORE notification job handler (NotificationJobHandler class) requires the above structure. If you create your own notification
	 *	job handler, it may require completely different one. With that said, THIS class is *REQUIRED* for CORE Job handler only.
	 *
	 *
	 * @var array
	 */
	public $recipients 	= array();

	/**
	 * Resulting array of distinct languages of all $recipients
	 *
	 * @var array  Array of language codes, e.g. ["english","german", ...]
	*/
	public $languages 	= array();


	/**
	 * Just collecting some generic info for the current notification. This will be used for shortcodes conversion.
	 *
	 * @var array Generic info (different for every notification handler) valid for all recipients
	 */
	public $metadata = array();

}