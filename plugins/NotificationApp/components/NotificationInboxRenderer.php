<?php
/**
 * @author Dzhuneyt <dzhuneyt@dzhuneyt.com>
 */

class NotificationInboxRenderer extends BaseNotificationRenderer{

	public $iconType = self::ICON_TYPE_HTML;

	public function getIcon() {
		return '';
	}

	public function getMessageText($shortVersion = false) {
		if($this->data){
			$subject = isset($this->data['subject']) ? Yii::app()->htmlpurifier->purify($this->data['subject']) : null;
			$body = isset($this->data['body']) ? Yii::app()->htmlpurifier->purify($this->data['body']) : null;
			$readMore = '...';
			$limit = 50;
			$body = strip_tags($body);
			$body = $this->cleanStringFromEncodingSpacesV2($body);
			if ($shortVersion) {
				if (strlen($body) > $limit)
					$body = substr(trim($body), 0, $limit) . $readMore;
				else $body = trim($body);
				if (strlen($subject) > $limit)
					$subject = substr($subject, 0, $limit).'...';
				return '<div class="dotLibContainer"><a href="javascript:void(0);">' . $subject . '</a><br/>' . $body . '</div>';
			} else {
				$readMore = ' ' . CHtml::link('<i class="fa fa-search-plus"></i> '.Yii::t('standard', '_VIEW') . ' ' . Yii::t('notification', 'notification'), Docebo::createLmsUrl('inbox/inbox/renderFullHTMLMessage', array('id' => $this->id)), array('class' => 'open-dialog readFullHTML', 'data-dialog-id' => 'inbox-dialog', 'data-dialog-class' => 'inbox-dialog-full-page'));
				$subject = CHtml::link($subject, 'javascript:void(0);');
				return '<div class="dotLibContainer"><div class="bodyContainer"><b>' . $subject . '</b></div><br/>'.$readMore.'<div class="bodyContainer">' . $body . '</div></div>';
			}
		}
	}

	public static function cleanStringFromEncodingSpaces($str){
		$str = preg_replace("/\\r+\\n+/", "", $str);
		$char = substr($str,0,1);
		if(empty($str) || !$char) return '...';
		$res ='';
		preg_match('/\\w+/',$char,$res);
		$str = str_replace(array("&nbsp;", "0xA0", "&#160;"), '', $str);
		$securityCheck = 0; //to avoid infinite loop anyway
		while (empty($res) && $securityCheck < 50000) {
			$str = ltrim($str, chr(0xC2) . chr(0xA0));
			$str = ltrim($str);
			if (empty($str)) break; //otherwise the while loop will become infinite
			$char = substr($str,0,1);
			preg_match('/\\w+/',$char,$res);
			$securityCheck++;
		}
		return $str;
	}

	//The function above throw timeout error at non latin words like arabic, cyrillic and etc.
	public static function cleanStringFromEncodingSpacesV2($str) {
		$str = preg_replace("/\\r+\\n+/", "", $str);
		$char = substr($str,0,1);
		if(empty($str) || !$char) return '...';
		$str = str_replace(array("&nbsp;", "0xA0", "&#160;"), '', $str);
		$str = str_replace("&nbsp;", "", htmlentities($str));

		return $str;
	}
}