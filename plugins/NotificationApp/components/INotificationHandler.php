<?php
/**
 * 
 * 
 *
 */
interface INotificationHandler {

	/**
	 * Return a descriptor calss, describing all properties of the Notificatio
	 *
	 * @param array|bool $params Optional parameters
	 *
	 * @return NotificationDescriptor
	 */
	public static function descriptor($params=false);

	/**
	 * Return specially crafted array of recipients to whom emails are going to be sent, along with notification sepcific related data
	 *
	 * @param bool|string $params Optional parameters
	 *
	 * @return array
	 */
	public function getRecipientsData($params=false);

}