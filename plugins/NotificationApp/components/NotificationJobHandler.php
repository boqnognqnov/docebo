<?php
/**
 *
 *  @package common.components.job.handlers
 *
 */
class NotificationJobHandler extends JobHandler implements IJobHandler {

	/**
	 * THIS handler identifier
	 * @var string
	 */
	const HANDLER_ID = 'plugin.NotificationApp.components.NotificationJobHandler';


	/**
	 * @see IJobHandler::init()
	 */
	public function init($params = false) {
		// do some initialization stuff here, specific for this job handler
	}

	/**
	 * @see IJobHandler::run()
	 */
	public function run($params = false) {
		// Must be already assigned at upper level
		$job = $this->job;
		$jobParams = json_decode($job->params, true);

		if (!isset($jobParams['notification_id']) || !($notification = CoreNotification::model()->findByPk($jobParams['notification_id']))) {
			throw new Exception('Notification handler called with invalid notification');
		}

		// Just return silently if notification is not active
		if (!$notification->active) {
			Yii::log('Notification ' . $notification->type . ' INACTIVE. Skipped.', CLogger::LEVEL_INFO);
			return;
		}

		$this->handleNotification();

	}


	/**
	 * @see IJobHandler::id()
	 */
	public static function id($params = false) {
		return self::HANDLER_ID;
	}


	/**
	 * General noifitcation handler which calls the "specific notification handler object", created on the fly, based on notification.
	 * Notification type is a string, which must be equal to a file/class available to Yii auto-loading mechanism.
	 *
	 *
	 */
	protected function handleNotification() {

		/* @var $notHandler INotificationHandler */
		/* @var $notification CoreNotification */
		/* @var $response NotificationHandlerResponse */

		// Get job parameters and create a notification object out of it
		$jobParams = json_decode($this->job->params, true);
		$notification = CoreNotification::model()->findByPk($jobParams['notification_id']);

		// Get notification handler class/type
		$class = $notification->type;

		// At this moment, the Class File must be already registered for auto-loading
		if (!stream_resolve_include_path ($class . '.php')) {
			throw new CException('[?] Notification class does not exists' . ': ' . $class . ". " . 'Did you forget to develop it or to turn a plugin ON?');
		}

		// Create an instance of the class
		$notHandler = new $class($this->job);

		// Call specific notification type handler to extract recipients data collection, one element per recipient
		// with all required information to send an email, including some handler-specific data, sent later back to him
		// when short codes must be replaced by actual user/courses/whatever data.

		$response = $notHandler->getRecipientsData();

		// Notification handlers MUST return a response of specific class: NotificationHandlerResponse
		if (!is_object($response)  || get_class($response) != 'NotificationHandlerResponse') {
			return;
		}

		// NO data? Get out!
		if (empty($response->languages) || empty($response->recipients)) {
			return;
		}

		// Inspect returned list of *recipeints* languages and see if Notification IS actually translated in all of them.
		// If some is not, change the recipient's language to some valid one ("good" one).
		// The result of this code is a final array of "good languages", meaning notification is translated into those languages and
		// recipient's languages have been adjusted to be one of them (i.e. to be a "good" language).
		// This is to ensure that recipient WILL receive the notification, in some language, no matter what!
		$goodLanguages = array();
		foreach ($response->languages as $languageCode) {
			$goodLanguage = $languageCode;
			if (!$notification->isTranslatedInLanguage(Lang::getBrowserCodeByCode($languageCode))) {
				// Resolve the usable language
				$newLanguageCode = $notification->resolveUsableLanguage($languageCode);
				// Now enumerate all recipients's data and change the language to the new one (good one)
				foreach ($response->recipients as $idUser => $userData) {
					if ($userData['language'] == $languageCode)
						$response->recipients[$idUser]['language'] = $newLanguageCode;
				}
				$goodLanguage = $newLanguageCode;
			}
			$goodLanguages[] = $goodLanguage;
		}

		//retrieve delivery systems active for this notification
		$sendTypes = $this->getSendTypes($jobParams['notification_id']);

		// Modify response languages to "good" languages
		$response->languages = array_unique($goodLanguages);

		// Collect Subjects[] and Messages[] in all languages
		foreach ($sendTypes as $sendType) {
			/* @var $sendType CoreNotificationTransport */
			foreach ($response->languages as $languageCode) {
				$subject = $notification->subject($sendType->id, Lang::getBrowserCodeByCode($languageCode));
				$message = $notification->message($sendType->id, Lang::getBrowserCodeByCode($languageCode));

				$subjects[$sendType->id][$languageCode] = $subject;
				$messages[$sendType->id][$languageCode] = $message;
			}
		}
		$finalDataHash = array(
			'recipients'	=> $response->recipients,
			'languages' 	=> $response->languages,
			'metadata'		=> $response->metadata,
			'subjects'		=> $subjects,
			'messages'		=> $messages,
		);

		//--- do some logging ---
		$sendTypeKeys = array();
		foreach ($sendTypes as $sendType) { $sendTypeKeys[] = $sendType->name; }
		Yii::log('Processing notification of type '.$notification->type.'. Sending via: '.implode(", ", $sendTypeKeys), CLogger::LEVEL_INFO);
		//--- ---

		//do effective messages delivery
		foreach ($sendTypes as $sendType){
			/* @var $sendType CoreNotificationTransport */
			$handler = $sendType->getManagerObject();
			if (empty($handler)) {
				Yii::log('Unable to retrieve manager class for transport "'.$sendType->name.'" in notification #'.$jobParams['notification_id'], CLogger::LEVEL_WARNING);
				continue;
			}
			//physical delivery
			$handler->deliver($finalDataHash, $notHandler, $notification);
		}

		//--- more logging (won't be showed in production environment) ---
		$logMessage = 'Notification sent to:';
		foreach ($response->recipients as $id_user => $info) {
			$logMessage .= "\n" . $id_user . ' - ' . $info['username'] . ' - ' . $info['email'];
		}
		//--- ---

		Yii::log($logMessage, CLogger::LEVEL_INFO);
		return;
	}




	public static function manageDateTagsFormat($message, $dateTags, $allTags)
	{
		foreach ($dateTags as $key => $tag) {
			DoceboShortcodeParser::parse($message, array(
				$tag => function ($tagName, $attributes, $fullTextTagMatch, &$html) use ($allTags) {
					preg_match_all('#((format)[=,:][",\']([\W\w]*)[",\'])+#Ui', $fullTextTagMatch, $matches, PREG_SET_ORDER);
					$format = false;
					if (isset($matches[0][3]))
						$format = $matches[0][3];

					if ($format) {
						// Get current format
						$currentFormat = Yii::app()->localtime->getLocalDateTimeFormat(LocalTime::SHORT, LocalTime::DEFAULT_TIME_WIDTH);
						$currentFormat = Yii::app()->localtime->YiitoPHPDateFormat($currentFormat);
						// Get current date
						$currentDateTime = $allTags[('[' . $tagName . ']')];
						// Build Object
						$dateAndTime = DateTime::createFromFormat($currentFormat, $currentDateTime);
						$newDate = $dateAndTime->format($format);
						$html = str_replace($fullTextTagMatch, $newDate, $html);
					}
				},
			));
		}
		$message = str_replace('\u200f', '', $message);
		return $message;
	}




	/**
	 * Fetch the active Transport methods and retrieve all associated transport ARs
	 *
	 * @param int $notificationId
	 * @return array
	 */
	protected function getSendTypes($notificationId){
		$result = array();
		$query = "SELECT *
			FROM ".CoreNotificationTransportActivation::model()->tableName()."
			WHERE notification_id = :notification_id AND active = 1";
		$cmd = Yii::app()->db->createCommand($query);
		$list = $cmd->queryAll(true, array(':notification_id' => $notificationId));
		if (is_array($list) && !empty($list)) {
			foreach ($list as $item) {
				$transport = CoreNotificationTransport::model()->findByPk($item['transport_id']);
				if (!empty($transport)) {
					$result[] = $transport;
				}
			}
		}
		return $result;
	}

}
