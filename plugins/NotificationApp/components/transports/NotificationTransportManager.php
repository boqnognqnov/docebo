<?php


abstract class NotificationTransportManager extends CComponent {


	/*
	 * These constants define all supported text formats for notifications messages.
	 * Each notification delivery syustem supports its own text format and one of these values must be returned
	 * by the method "getTextFormatType()".
	 * Other formats may be added to this list in the future, if support will be required;
	 */
	const TEXT_FORMAT_PLAIN = 'plain'; //plain text, no formatting, tags etc.
	const TEXT_FORMAT_MARKDOWN = 'markdown'; //markdown syntax, see e.g. https://sourceforge.net/p/slack/wiki/markdown_syntax/
	const TEXT_FORMAT_HTML = 'html'; //html text


	/**
	 * Returns the name of the transport, corresponding to the column "name" in "core_notification_transport" table
	 * @return string
	 */
	abstract public function getName();


	/**
	 * Returns the translated name for a specific notification transport
	 * @return string
	 */
	abstract public function getLabel();


	/**
	 * In the notification editor allow to insert more configuration parameters (e.g. "from_name" e "from_email" for email
	 * transport type)
	 * @params CoreNotification $notification the AR of the notification requesting the params
	 * @return string/bool the additional content, or false if no additional stuff is needed by the transport
	 */
	public function getAdditionalFormParameters(CoreNotification $notification) {
		return false;
	}


	/**
	 * This method is called when reading input parameters in notification editing form
	 * @param array $input stuff read from $_REQUEST
	 * @return array
	 */
	public function readInputParameters($input) {
		return array();
	}


	/**
	 * This method is called when validating input parameters in notification editing form
	 * @return boolean
	 */
	public function validateParameters($transportData, $data, $fromStep) {
		return true;
	}


	/**
	 * Some delivery methods may require special text editors, which usage require some specific textual hints
	 * @return string
	 */
	public function getEditorHintText() {
		return '';
	}


	/**
	 * This method will be responsible for the actual delivery of the notification using the desired media
	 * @param array $finalDataHash
	 * @param object $notHandler // instance of specific notification type
	 * @param CoreNotification $notification
	 * @return bool
	 */
	abstract public function deliver($finalDataHash, $notHandler, $notification);


	/**
	 * Retrieves the numeric ID of the currently used notification transport.
	 * If no ID can be retrieved, than boolean 'false' is returned.
	 * @return bool|integer
	 */
	protected final function getTransportId() {
		/* @var $cmd CDbCommand */
		$cmd = Yii::app()->db->createCommand();
		$cmd->select("id");
		$cmd->from(CoreNotificationTransport::model()->tableName());
		$cmd->where("name = :name");
		$record = $cmd->queryRow(true, array(':name' => $this->getName()));
		if (empty($record)) { return false; }
		return $record['id'];
	}


	/**
	 * This is called in notification management page for each active transport. It allows to load custom css/scripts for
	 * notification editor transport tabs. It has to be overloaded in subclasses if needed.
	 */
	public function loadEditorAssets() { }


	/**
	 * Specifies which kind of formatting the notification text are expected to be (e.g. email and inbox notification both uses HTML)
	 * @return string
	 */
	public function getTextFormatType() {
		return self::TEXT_FORMAT_PLAIN; //default value
	}


	/**
	 * Retrieves all available text formats and packs them in an array
	 * @return array
	 */
	public static function getAllTextFormatTypes() {
		return array(
			self::TEXT_FORMAT_PLAIN,
			self::TEXT_FORMAT_MARKDOWN,
			self::TEXT_FORMAT_HTML
		);
	}

}