<?php


class InboxTransportManager extends NotificationTransportManager {


	public function getName() {
		return 'inbox';
	}


	public function getLabel() {
		return Yii::t('notification', 'Notifications');
	}


	public function getTextFormatType() {
		return self::TEXT_FORMAT_HTML;
	}


	public function deliver($finalDataHash, $notHandler, $notification){
		$transportId = $this->getTransportId();
		$inboxesSavedCount = 0;
        $a = 0;

        foreach ($finalDataHash['recipients'] as $recipient) {
			$user = CoreUser::model()->findByPk(trim($recipient['idst']));
			if($user && (!$user->valid || $user->isUserExpired()))
				continue;

			$language = $recipient['language'];
			if (!array_key_exists('user', $recipient)) {
				$recipient['user'] = $recipient['idst'];
			}
			// Get shortcodes data from the particular handler.
			// We pass the handler the recipient data structure that was returned to us from it earlier.
			// That way, the handler KNOWS what to expect from this call and use it on its own discretion.
			// This forth-and-back "dance" is to provide an abstraction and centralized cycle for sending emails.
			// Otherwise we must code email sending into every notification handler class.
			$shortcodesData = $notHandler->getShortcodesData($recipient, array_merge(array('sendType'=>CoreNotification::NOTIFY_TYPE_INBOX),$finalDataHash['metadata']), $this->getTextFormatType());
            //list of user ids to send notification
			$to = trim($recipient['idst']);
			if ($to) {
                $metadata = $finalDataHash['metadata'];
				$subject = $finalDataHash['subjects'][$transportId][$language];
				$message = $finalDataHash['messages'][$transportId][$language];
				// Fix some legacy shortcodes
                //Start digest case
                if (
                    $notification->type == CoreNotification::NTYPE_DIGEST_USER_HAS_YET_COMPLETE_COURSE ||
                    $notification->type == CoreNotification::NTYPE_DIGEST_COURSE_HAS_EXPIRED ||
                    $notification->type == CoreNotification::NTYPE_DIGEST_USER_ENROLL_TO_COURSE
                ) {
                    //match from template
                    preg_match_all('/\[items\](.+)\[\/items\]/Uis', $message, $match, PREG_PATTERN_ORDER);
                    $itemsArray = array();
                    //foreach the result and replace the custom shortcodes for each result
                    if (
                        $notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_GODADMIN ||
                        $notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_POWER_USER ||
                        $notification->recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER
                    ) {
                        //check count for reseting digest counter
                        $checkCount = count($metadata['items'][$to]);
                        foreach ($metadata['items'][$to] as $key => $value) {
                            if ($key == $a) {
                                foreach ($metadata['items'][$to][$a]['courses'] as $course) {
                                    $shortcodesData['[course_name]'] = $course['course_name'];
                                    $shortcodesData['[course_link]'] = $course['course_link'];
                                    $shortcodesData['[expire_at]'] = $course['expire_at'];
                                    $shortcodesData['[subscribed_at]'] = $course['subscribed_at'];
                                    $shortcodesData['[first_name]'] = $course['first_name'];
                                    $shortcodesData['[last_name]'] = $course['last_name'];
                                    $shortcodesData['[username]'] = $course['username'];
                                    $itemsArray [] = str_replace(array_keys($shortcodesData), $shortcodesData,
                                        $match[1][0]);

                                }
                            }


                        }
                        //else case for send notification to users because it is different structure
                    } else {
                        foreach ($metadata['items'][$to]['courses'] as $course) {
                            $shortcodesData['[course_name]'] = $course['course_name'];
                            $shortcodesData['[course_link]'] = $course['course_link'];
                            $shortcodesData['[expire_at]'] = $course['expire_at'];
                            $shortcodesData['[subscribed_at]'] = $course['subscribed_at'];
                            $itemsArray [] = str_replace(array_keys($shortcodesData), $shortcodesData, $match[1][0]);
                        }
                    }
                    //this are the meta info for admin/PU/instructor
                    $shortcodesData['[first_name]'] = $recipient['first_name'];
                    $shortcodesData['[last_name]'] = $recipient['last_name'];
                    $shortcodesData['[username]'] = $recipient['username'];

                    $items = '';
                    //make items content to string
                    foreach ($itemsArray as $item) {
                        $items .= $item;
                    }
                    //normal shortcodes replace
                    $subject = str_replace(array("[firstname]", "[lastname]", "[password]"),
                        array("[first_name]", "[last_name]", "[user_password]"), $subject);
                    $subject = str_replace(array_keys($shortcodesData), $shortcodesData, $subject);
                    $message = str_replace($match[0][0], $items, $message);
                    $message = str_replace(array("[firstname]", "[lastname]", "[password]"),
                        array("[first_name]", "[last_name]", "[user_password]"), $message);
                    $message = str_replace(array_keys($shortcodesData), $shortcodesData, $message);

                    //End digest case
                } else {
                    $subject = str_replace(array("[firstname]", "[lastname]", "[password]"),
                        array("[first_name]", "[last_name]", "[user_password]"), $subject);
                    $message = str_replace(array("[firstname]", "[lastname]", "[password]"),
                        array("[first_name]", "[last_name]", "[user_password]"), $message);
                    $subject = str_replace(array_keys($shortcodesData), $shortcodesData, $subject);
                    $message = str_replace(array_keys($shortcodesData), $shortcodesData, $message);
                }

				// If you want your date tag to use the format functionality, just put it in the array: $FORMATTABLE_DATE_TAGS
				$subject = NotificationJobHandler::manageDateTagsFormat($subject, CoreNotification::$FORMATTABLE_DATE_TAGS, $shortcodesData);
				$message = NotificationJobHandler::manageDateTagsFormat($message, CoreNotification::$FORMATTABLE_DATE_TAGS, $shortcodesData);

				$attachments = array();
                //check if icsManager is provided and if the attachment is requested by a shortcode
				if (isset($recipient['icsManager']) && ($recipient['icsManager'] instanceof ICSManager))
				{
                    $attachment_subject = array(false, false);
					$attachment_message = array(false, false);

					DoceboShortcodeParser::parse($subject, array(
						CoreNotification::SC_CALENDAR_ATTACHMENT => function( $tagName, $attributes, $fullTextTagMatch, &$html ) use ( &$attachment_subject ) {
							$attachment_subject[0] = true;

							if(!empty($attributes) && isset($attributes['notify_organizer']) && $attributes['notify_organizer'] == 'true') {
								$attachment_subject[1] = true;
							}

							$html = str_replace($fullTextTagMatch, '', $html);
						}
					));

					DoceboShortcodeParser::parse($message, array(
						CoreNotification::SC_CALENDAR_ATTACHMENT => function( $tagName, $attributes, $fullTextTagMatch, &$html ) use ( &$attachment_message ) {
							$attachment_message[0] = true;

							if(!empty($attributes) && isset($attributes['notify_organizer']) && $attributes['notify_organizer'] == 'true') {
								$attachment_message[1] = true;
							}

							$html = str_replace($fullTextTagMatch, '', $html);
						}
					));

					$SC_CALENDAR_ATTACHMENT = $attachment_subject[0] || $attachment_message[0];
					$SC_CALENDAR_ATTACHMENT_REPLY = $attachment_subject[1] || $attachment_message[1];

					if ($SC_CALENDAR_ATTACHMENT) $attachments[$recipient['icsManager']->fileName] = $recipient['icsManager']->filePath();

					IF($SC_CALENDAR_ATTACHMENT_REPLY) {
						/* and add the RSVP to the attachment */
						// get ics file contents
						$ICS_Data = file_get_contents($recipient['icsManager']->filePath());

						if(!preg_match("/(ATTENDEE\;RSVP\=TRUE:MAILTO\:)/", $ICS_Data)) {

							// find the begin of the event regex pattern for preg_replace
							$regex_pattern = "/(BEGIN\:VEVENT)/";

							// set the needed string
							$RSVP_Data = "$1\r\nATTENDEE;RSVP=TRUE:MAILTO:{$recipient['icsManager']->orgEmail}";

							// replace all occurrences of the event
							$new_ICS_Data = preg_replace($regex_pattern, $RSVP_Data, $ICS_Data);

							// save the file
							file_put_contents($recipient['icsManager']->filePath(), $new_ICS_Data);

							// and add the attachment
							$attachments[$recipient['icsManager']->fileName] = $recipient['icsManager']->filePath();
						}
					}
				}

				$inboxMessageModel = new InboxAppNotification();
				$inboxMessageModel->type = 'notificationapp';
				$inboxMessageModel->json_data = json_encode(array(
					'subject'=>$subject,
					'body'=>$message,
				));
				if($inboxMessageModel->save()){
					Yii::app()->getDb()->createCommand()
						->insert(InboxAppNotificationDelivered::model()->tableName(), array(
							'user_id'=>$to,
							'notification_id'=>$inboxMessageModel->id,
						));
					$inboxesSavedCount++;
                    //digest counter incrementing with check when to reset it
                    $a++;
                    if ($a >= $checkCount) {
                        $a = 0;
                    }
				}else{
					Yii::log('Unable to save inbox message model for user '.$to, CLogger::LEVEL_ERROR);
				}
			}
			else {
				Yii::log("Notification '{$notification->type}', target user has no email. Skipped.", CLogger::LEVEL_WARNING);
			}
		}
		Yii::log('DEBUG: Total sent messages to inboxes: '.$inboxesSavedCount);
	}


}