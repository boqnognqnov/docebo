<?php

class TransportAgent2 extends CComponent {


	const LOGNAME = 'Notification Agent';

	/**
	 * A map of Parameter name => Expected type or object class.
	 * This is used to sanitize the process and prevent class/type mess and apply a strict control.
	 *
	 * @var array
	 */
	private static $_PARAMETER_TYPES = array (
			'course' 			=> 'LearningCourse',
			'category' 			=> 'LearningCourseCategory',
			'thread' 			=> 'LearningForumthread',
			'user' 				=> 'CoreUser',
			'userwaiting'		=> 'CoreUserTemp',
			'courseuser' 		=> 'LearningCourseuser',
			'group' 			=> 'CoreGroup',
			'learningobject' 	=> 'LearningOrganization',
            'learningPlan' 	    => 'LearningCoursepath',
            'certificate'       => 'LearningCertificateAssignCp',
			'transaction'		=> 'EcommerceTransaction',
	);


	/**
	 * List of REQUIRED paramaters that must be sent to the agent in order to do its job properly.
	 *
	 * @see TransportAgent::$_PARAMETER_TYPES - on the corresponding class names for each parameter
	 *
	 * @var array
	 */
	public static $_REQUIRED_PARAMETERS = array(
			CoreNotification::NTYPE_USER_COMPLETED_TEST 			=> array('course', 'user', 'learningobject'),
			CoreNotification::NTYPE_USER_TEST_EVALUATION_NEEDED		=> array('course', 'user', 'learningobject'),
			CoreNotification::NTYPE_INSTRUCTOR_EVALUATED_QUESTION	=> array('course', 'user', 'learningobject','answertrack'),


			CoreNotification::NTYPE_COURSE_PROP_CHANGED 			=> array('course', 'user'),
			CoreNotification::NTYPE_NEW_CATEGORY 					=> array('category'),
			CoreNotification::NTYPE_NEW_REPLY 						=> array('thread', 'course', 'user'),
			CoreNotification::NTYPE_NEW_THREAD 						=> array('thread', 'course', 'user'),
			CoreNotification::NTYPE_NEW_MESSAGE_RECEIVED 			=> array('thread'),
			CoreNotification::NTYPE_USER_APPROVAL 					=> array('course', 'user'),
			CoreNotification::NTYPE_USER_BOUGHT_COURSE 				=> array('user', 'transaction'),
			CoreNotification::NTYPE_STUDENT_COMPLETED_COURSE 		=> array('course', 'user'),
			CoreNotification::NTYPE_USER_SUBSCRIBED_COURSE 			=> array('course', 'user'),
			CoreNotification::NTYPE_USER_WAITING_SUBSCRIBE 			=> array('course', 'user'),
			CoreNotification::NTYPE_USER_COURSELEVEL_CHANGED 		=> array('course', 'user', 'courseuser'),
			CoreNotification::NTYPE_USER_UNSUBSCRIBED 				=> array('course', 'user'),
			CoreNotification::NTYPE_USER_DELETED 					=> array('user'),
			CoreNotification::NTYPE_USER_SUBSCRIBED_GROUP 			=> array('user', 'group'),
			CoreNotification::NTYPE_USER_REMOVED_FROM_GROUP 		=> array('user', 'group'),
			CoreNotification::NTYPE_USER_MODIFIED					=> array('user'),
			CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION	=> array('userwaiting'),
			CoreNotification::NTYPE_NEW_COURSE 						=> array('course'),
			CoreNotification::NTYPE_NEW_LO_ADDED_TO_COURSE 			=> array('course', 'learningobject'),
			CoreNotification::NTYPE_COURSE_DELETED 					=> array('course'),
			CoreNotification::NTYPE_NEW_USER_CREATED				=> array('user'),  // Note, use passwordPlainText class attribute to pass the password
			CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG		=> array('user'),
            CoreNotification::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN  => array('learningPlan', 'user')
	);


	/**
	 * Mapping Event names to Notification type for easy binind and calling the handler
	 *
	 * @var array
	 */
	public static $_EVENTNAME_TYPE_MAP = array(
			'CoursePropChanged' 		=> CoreNotification::NTYPE_COURSE_PROP_CHANGED ,
			'NewCategory' 				=> CoreNotification::NTYPE_NEW_CATEGORY ,
			'NewReply' 					=> CoreNotification::NTYPE_NEW_REPLY ,
			'NewThread' 				=> CoreNotification::NTYPE_NEW_THREAD ,
			'NewMessage' 				=> CoreNotification::NTYPE_NEW_MESSAGE_RECEIVED ,
			'UserApproval' 				=> CoreNotification::NTYPE_USER_APPROVAL ,
			'UserBoughtCourse' 			=> CoreNotification::NTYPE_USER_BOUGHT_COURSE ,
			EventManager::EVENT_STUDENT_COMPLETED_COURSE 	=> CoreNotification::NTYPE_STUDENT_COMPLETED_COURSE ,
			'UserSubscribedCourse' 		=> CoreNotification::NTYPE_USER_SUBSCRIBED_COURSE ,
			'UserWaitingSubscribe' 		=> CoreNotification::NTYPE_USER_WAITING_SUBSCRIBE ,
			'UserCourselevelChanged' 	=> CoreNotification::NTYPE_USER_COURSELEVEL_CHANGED ,
			'UserUnsubscribed' 			=> CoreNotification::NTYPE_USER_UNSUBSCRIBED ,
			'UserDeleted' 				=> CoreNotification::NTYPE_USER_DELETED ,
			'UserSubscribedGroup' 		=> CoreNotification::NTYPE_USER_SUBSCRIBED_GROUP ,
			'UserRemovedFromGroup' 		=> CoreNotification::NTYPE_USER_REMOVED_FROM_GROUP ,
			'UserModified' 				=> CoreNotification::NTYPE_USER_MODIFIED ,
			'UserWaitingApprovalSubscription' 	=> CoreNotification::NTYPE_USER_WAITING_APPROVAL_SUBSCRIPTION ,
			EventManager::EVENT_NEW_COURSE 				=> CoreNotification::NTYPE_NEW_COURSE ,
			'NewLoAddedCourse' 			=> CoreNotification::NTYPE_NEW_LO_ADDED_TO_COURSE ,
			'CourseDeleted' 			=> CoreNotification::NTYPE_COURSE_DELETED ,
			'NewUserCreated'			=> CoreNotification::NTYPE_NEW_USER_CREATED,
			'NewUserCreatedSelfreg'     => CoreNotification::NTYPE_NEW_USER_CREATED_SELFREG,
            EventManager::EVENT_STUDENT_COMPLETED_LEARNING_PLAN => CoreNotification::NTYPE_STUDENT_COMPLETED_LEARNINGPLAN
	);


	/**
	 * Holds the Object that raised the event or send the notification (controller, model, ...)
	 *
	 * @var objects
	 */
	protected $_senderObject = NULL;

	/**
	 * This array is sent by the event raising code, and depending on the
	 * event MUST contain certain keys in order for the event handler to work properly.
	 * See description of handlers.
	 *
	 * @var array
	 */
	private $_params = array();





	/**
	 * One single event handler for all events listed in the self::$_EVENTNAME_TYPE_MAP
	 *
	 * @param DEvent $event
	 */
	public function notifyEvent(DEvent $event)  {
		try {
			$notificationType = self::$_EVENTNAME_TYPE_MAP[$event->event_name];
			$this->_senderObject 	= $event->sender;
			$this->_sendNotifications($notificationType, $event->params);
		}
		catch (CException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR, self::LOGNAME);
		}
	}




	/**
	 * Using hardcoded list of required parameters per notification type, checks if required parameter(s) are available at all.
	 * @see TransportAgent::$_REQUIRED_PARAMETERS for the list
	 *
	 * @param number $type
	 * @throws CException
	 * @return boolean
	 */
	protected function _checkRequiredParams($type) {

		$required = self::$_REQUIRED_PARAMETERS;

		// Do we have info for this notification type?
		if (!isset($required[$type]) || count($required[$type]) <= 0) {
			return TRUE;
		}

		// Enumerate required parameters and check ifthey are passed to us (in $this->_params)
		$requiredParams = $required[$type];
		foreach ($requiredParams as $paramName) {
			if ( !isset($this->_params[$paramName]) ) {
				throw new CException(__METHOD__ . ": Parameter $paramName is required for this event.");
			}
		}
		return TRUE;
	}


	/**
	 * Check incoming parameters type. We need to control/check what we are receiving in order to do our job properly.
	 * @see TransportAgent::$_PARAMETER_TYPES
	 *
	 * @throws CException
	 */
	protected function _chekParamsType() {
		if (!is_array($this->_params)) {
			return;
		}

		// Enumerate all passed variables
		foreach ($this->_params as $key => $value) {
			$typeName = is_object($value) ? get_class($value) : gettype($value);
			if ($value && ($typeName != self::$_PARAMETER_TYPES[$key])) {
				throw new CException(__METHOD__ . ": Parameter '$key' must be of type/class '" . self::$_PARAMETER_TYPES[$key] . "'");
			}
		}
	}



	/**
	 * Send all notifications of the given type
	 *
	 * @param number $type
	 * @param array $params
	 * @return number
	 */
	protected function _sendNotifications($type, $params) {

		$this->_params = $params;

		// Get list of active notifications
		$notifications = CoreNotification::model()->findAllByAttributes(array(
				'type' 		=> $type,
				'active' 	=> 1,
		));

		// Return if no found
		if (!$notifications || (count($notifications) <= 0)) {
			Yii::log("No notification found of type $type (" . CoreNotification::typeName($type). ")", 'debug', __CLASS__);
			return 0;
		}

		// Enumerate all found notifications and handle them one by one
		foreach ($notifications as $notification) {
			try{
				$this->_sendOneNotification($notification);
			}catch(Exception $e){
				Yii::log("Can not send notification: ".$e->getMessage(), CLogger::LEVEL_ERROR);
			}
		}


	}

	/**
	 * Send a single notification
	 *
	 * @param CoreNotification $n
	 */
	protected function _sendOneNotification($n) {

		// This will throw an exception if some parameter is missing or is of a wrong type
		$this->_checkRequiredParams($n->type);
		$this->_chekParamsType();




		// ************************
		// TEST ONLY TEST ONLY TEST ONLY TEST ONLY TEST ONLY TEST ONLY TEST ONLY TEST ONLY
		// This is to test calling Notification Job handler using events
		$this->_pipeNotificationEventJob($n);
		return;
		// ************************


		$fromEmail 			= $this->_getFromEmail($n);

		$recipient = $n->recipient;

		// Some notification types (e.g. forum related) require a special
		// retrieval of notification recipients. Hence, the switch().
		switch($n->type){
			case CoreNotification::NTYPE_NEW_REPLY:
				// Get all users involved in the discussion and notify them

				$toArray = $this->_getToArrayForumReply($n);
				break;
			case CoreNotification::NTYPE_NEW_THREAD:
				// Get all users subscribed to the course to which the discussion belongs
				// !!! break; is missing on purpose here, so the below case is used !!!
			case CoreNotification::NTYPE_NEW_LO_ADDED_TO_COURSE:
			case CoreNotification::NTYPE_COURSE_PROP_CHANGED:
				// Get all users from the course to which the LO was added
				$toArray = $this->_getToArrayFromCurrentCourse($n);
				break;
			default:
				$toArray = $this->_getToArray($n->recipient);
		}

		// Get all active languages (they are all possible languages that might appear in Notifications table)
		$activeLanguages = CoreLangLanguage::getActiveLanguagesByBrowsercode();

		// Enumerate all emails, as they are grouped per language,
		// replace shortcodes and send the notification
		foreach ($toArray as $language => $to) {
			$subject = $this->_replaceShortCodes($n->subject($language) , $n, $language);
			$message = $this->_replaceShortCodes($n->message($language), $n, $language);

			Yii::app()->event->raise('BeforeNotificationSend', new DEvent($this, array(
				'type'=>$n->type,
				'subject'=>&$subject,
				'message'=>&$message,
				'params'=>$this->_params,
				'recipient'=>$to,
			)));

			$this->_sendEmail($fromEmail, $to, $subject, $message, $language, $n);
		}


	}



	/**
	 * Creates a One-Time job in Sidekiq queue, passing all event info. Notification job handler will handle the whole work then
	 *
	 * @param CoreNotification $n
	 */
	protected function _pipeNotificationEventJob($n) {
		$params = array(
			'notification_id' 	=> $n->id,
			'event'				=> true,
			'eventParams' 		=> serialize($this->_params),
			'eventSender'		=> serialize($this->_senderObject),
		);
		$job = Yii::app()->scheduler->createImmediateJob('Notification event job', NotificationJobHandler::id(), $params);
		return $job;
	}


	/**
	 * Return the FROM: according to notification type/recipient
	 * @param CoreNotification $notification
	 *
	 * @return string
	 */
	protected function _getFromEmail($notification) {

		switch ($notification->recipient) {
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
			case CoreNotification::NOTIFY_RECIPIENTS_COURSEADMIN:
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:

			default:
				$fromEmail = trim(Settings::get('sender_event', FALSE));
				if (empty($fromEmail)) {
					$fromEmail = isset(Yii::app()->params['support_sender_email']) ? Yii::app()->params['support_sender_email'] : '';
				}
				break;

		}

		// Fallback to info@docebo.com
		if (empty($fromEmail)) {
			$fromEmail = "info@docebo.com";
		}

		return $fromEmail;
	}


	/**
	 * Based on the "recipients type" of the notification model, return the list of recipients per language
	 * Like:
	 * 		'en' => array(some@one.it, another@gmail.com),
	 * 		'it' => array('john@mail.com')
	 *
	 * NOTE: We are using browser codes!!!! (en,de,it,...)
	 *
	 * @param number $recipient Recpipient TYPE! (see CoreNotification model)
	 */
	protected function _getToArray($recipient) {

		$toArray = array();

		if ($recipient == CoreNotification::NOTIFY_RECIPIENTS_USER) {
			if (!isset($this->_params['user'])) {
				throw new CException(__METHOD__ . ": User information is required for recipient of type '" . CoreNotification::recipientName($recipient));
			}

			$firstName 	= $this->_params['user']->firstname;
			$lastName 	= $this->_params['user']->lastname;

			$language = $this->_getUserLanguage($this->_params['user']->idst);
			if (!empty($this->_params['user']->email)) {
				$toArray = array($language => array($this->_params['user']->email));
			}
		}

		else if ($recipient == CoreNotification::NOTIFY_RECIPIENTS_GODADMIN) {
			$godAdmins = Yii::app()->user->getAllGodAdmins();
			$toArray = array();
			foreach ($godAdmins as $user) {
				if (!empty($user->email)) {
					$userLanguage = $this->_getUserLanguage($user->idst);
					$toArray[$userLanguage][] = $user->email;
				}
			}
		}

		else if ($recipient == CoreNotification::NOTIFY_RECIPIENTS_COURSEADMIN) {
			if (!isset($this->_params['course'])) {
				throw new CException(__METHOD__ . ": Course is required for recipient of type '" . CoreNotification::recipientName($recipient));
			}

			$course_id = $this->_params['course']->idCourse;

			$criteria = new CDbCriteria();
			$criteria->addCondition('idCourse=:idCourse AND level=:level');
			$criteria->params = array(':idCourse' => $course_id, ':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_ADMIN);

			// Get all subscriptions to this course where user is an ADMIN
			$subscriptions = LearningCourseuser::model()->findAll($criteria);

			$toArray = array();
			foreach ($subscriptions as $subscription) {
				$user = $subscription->user;
				$userLanguage = $this->_getUserLanguage($user->idst);
				if (!empty($user->email)) {
					$toArray[$userLanguage][] = $user->email;
				}
			}
		}

		else if ($recipient == CoreNotification::NOTIFY_RECIPIENTS_TEACHER) {
			if (!isset($this->_params['course'])) {
				throw new CException(__METHOD__ . ": Course information is required for recipient of type '" . CoreNotification::recipientName($recipient));
			}

			$course_id = $this->_params['course']->idCourse;
			// Get all subscriptions to this course where user is an ADMIN

			$criteria = new CDbCriteria();
			$criteria->addInCondition('level', array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
			));

			$criteria->addCondition('idCourse=:idCourse');
			$criteria->params[':idCourse'] = $course_id;

			$subscriptions = LearningCourseuser::model()->findAll($criteria);

			$toArray = array();
			foreach ($subscriptions as $subscription) {
				$user = $subscription->user;
				$userLanguage = $this->_getUserLanguage($user->idst);
				if (!empty($user->email)) {
					$toArray[$userLanguage][] = $user->email;
				}
			}
		}

		else {
			throw new CException(__METHOD__ . ": Unknown recipient type: " . $recipient );
		}



		return $toArray;
	}

	/**
	 * A new reply is posted to a forum discussion. Notify users based on the notification's recipient setting
	 *
	 * Currently there are 3 types of recipients: godadmins, users and teachers/instructors.
	 *
	 * - Users -
	 * Notifies all users, subscribed to the course to which the discussion belongs,
	 * with level "student" AND have posted at least once in the discussion. The user that
	 * have posted the current reply is excluded from notification.
	 *
	 * - Teachers/instructors -
	 * All instructors of the course receive the notification, BUT they
	 * need to have posted at least once in the discussion (same as "users").
	 *
	 * - Godadmins -
	 * All godadmins in the platform receive the notification.
	 * It does NOT matter if they have posted to the discussion or not.
	 *
	 * @param CoreNotification $n - the event.
	 *
	 * Required params: 'thread', 'course' (to which the thread belongs)
	 * and 'user' (who initiated the event)
	 *
	 * @return array Of the type:
	 * 'en' => email1, email2, email3...
	 * 'it' => email4, email5, email6...
	 * ...
	 */
	protected function _getToArrayForumReply(CoreNotification $n){

		$toArray = array();

		$thread = $this->_params['thread']; // LearningForumthread
		$course = $this->_params['course']; // LearningCourse
		$user = $this->_params['user']; // CoreUser (the event initiator/discussion replier)

		$c = new CDbCriteria();

		switch($n->recipient){
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				$godAdmins = Yii::app()->user->getAllGodAdmins();
				foreach ($godAdmins as $user) {
					if (!empty($user->email)) {
						$userLanguage = $this->_getUserLanguage($user->idst);

						if(!isset($toArray[$userLanguage])) $toArray[$userLanguage] = array();

						$toArray[$userLanguage][] = $user->email;
					}
				}
				return $toArray;

				break;
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$c->addCondition('t.level='.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				break;
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$c->addInCondition('t.level', array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				));
				break;
			default:
				Yii::log("Invalid recipient type: ".$n->recipient, CLogger::LEVEL_ERROR);
				return array(); // Invalid recipient type
		}

		$c->addCondition('t.idCourse=:idCourse');
		$c->params[':idCourse'] = $course->idCourse;

		$c->select = 't.idUser, t.idCourse, t.level, user.*';
		$c->with = 'user';

		$allSubscribedUsers = LearningCourseuser::model()->findAll($c);

		// Get all users who have posted
		// at least once in the discussion
		// (except the current reply poster)
		$c = new CDbCriteria();
		$c->addCondition('idThread=:currentThread');
		$c->params[':currentThread'] = $thread->idThread;

		// Exclude the person who just replied to the thread from notification
		$c->addCondition('author <> :eventInitiator');
		$c->params[':eventInitiator'] = $user->idst;
		$c->select = 'author';

		$discussionUsers = LearningForummessage::model()->findAll($c);

		$discussionUsersIdstArr = array();
		foreach($discussionUsers as $discussionUser){
			// Collect the IDSTs of users active in the discussion in array
			$discussionUsersIdstArr[] = $discussionUser->author;
		}

		$toArray = array();

		// Cross reference active users in the discussion
		// with users subscribed to the course and extract matches
		if($discussionUsersIdstArr && $allSubscribedUsers){
			foreach($allSubscribedUsers as $subscribedUser){
				if(in_array($subscribedUser->idUser, $discussionUsersIdstArr)){
					$language = $this->_getUserLanguage($subscribedUser->idUser);
					if($subscribedUser->user && $subscribedUser->user->email){

						// The end result array will be:
						// 'en' => email1, email2, email3
						// 'it' => email4, email5, email6
						if(!isset($toArray[$language])) $toArray[$language] = array();

						$toArray[$language][] = $subscribedUser->user->email;
					}else{
						Yii::log("Courseuser doesn't have a corresponding CoreUser or user has an empty email", CLogger::LEVEL_ERROR);
					}
				}
			}
		}

		return $toArray;
	}

	/**
	 * Get the array of recipients for the notification based
	 * on the notification's recipient. This is used only for
	 * cases when the recipients will be limited to only
	 * users inside the course (also passed to the notification
	 * as a 'course' param). Depending on the recipient, the following
	 * people will be notified:
	 * --Godadmins      All godadmins in the platform
	 * --Instructors    All teachers/instructors subscribed to the course
	 * --Users          All users subscribed to the course with level 'student'
	 *
	 * @param CoreNotification $n
	 *
	 * @return array - Array where the key is a language code and the value
	 * is an array of emails to be notified (in that language). Or an empty array.
	 */
	protected function _getToArrayFromCurrentCourse(CoreNotification $n){
		$toArray = array();

		$course = $this->_params['course']; // LearningCourse
		$user = $this->_params['user']; // CoreUser (the event initiator/discussion replier)

		if(!$course)
			throw new CException('TransportAgent::_getToArrayFromCurrentCourse() failed because the course is not passed to the event');

		$c = new CDbCriteria();

		switch($n->recipient){
			case CoreNotification::NOTIFY_RECIPIENTS_GODADMIN:
				$godAdmins = Yii::app()->user->getAllGodAdmins();
				foreach ($godAdmins as $user) {
					if (!empty($user->email)) {
						$userLanguage = $this->_getUserLanguage($user->idst);

						if(!isset($toArray[$userLanguage])) $toArray[$userLanguage] = array();

						$toArray[$userLanguage][] = $user->email;
					}
				}

				// The others just add a new condition to CDbCriteria
				return $toArray;

				break;
			case CoreNotification::NOTIFY_RECIPIENTS_USER:
				$c->addCondition('t.level='.LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT);
				break;
			case CoreNotification::NOTIFY_RECIPIENTS_TEACHER:
				$c->addInCondition('t.level', array(
					LearningCourseuser::$USER_SUBSCR_LEVEL_TUTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_MENTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
					LearningCourseuser::$USER_SUBSCR_LEVEL_COACH,
				));
				break;
			default:
				Yii::log("Invalid recipient type: ".$n->recipient, CLogger::LEVEL_ERROR);
				return array(); // Invalid recipient type
		}

		$c->addCondition('t.idCourse=:idCourse');
		$c->params[':idCourse'] = $course->idCourse;

		// Exclude the person who created the discussion from receiving the notification
		$c->addCondition('t.idUser <> :eventInitiator');
		$c->params[':eventInitiator'] = $user->idst;

		$c->select = 't.idUser, t.idCourse, t.level, user.*';
		$c->with = 'user';

		$allSubscribedUsers = LearningCourseuser::model()->findAll($c);

		$toArray = array();

		// Collect course subscribed users' emails in a well-formatter, lang-based array
		if($allSubscribedUsers){
			foreach($allSubscribedUsers as $subscribedUser){
				$language = $this->_getUserLanguage($subscribedUser->idUser);
				if($subscribedUser->user && $subscribedUser->user->email){

					// The end result array will be:
					// 'en' => email1, email2, email3
					// 'it' => email4, email5, email6
					if(!isset($toArray[$language])) $toArray[$language] = array();

					$toArray[$language][] = $subscribedUser->user->email;
				}else{
					Yii::log("Courseuser doesn't have a corresponding CoreUser or user has an empty email", CLogger::LEVEL_ERROR);
				}
			}
		}

		return $toArray;
	}

	/**
	 * Replace all shortcodes in the text
	 *
	 * @param string $message
	 */
	protected function _replaceShortCodes($text, $n, $language) {

		$replace = array();


		// GLOBAL
		$replace[CoreNotification::SC_LMS_LINK] = Docebo::createAbsoluteLmsUrl('site/index');


		// Course model related: LearningCourse model
		if (isset($this->_params['course'])) {
			$replace[CoreNotification::SC_COURSENAME] = $this->_params['course']->name;
			$replace[CoreNotification::SC_COURSE_LINK] = Docebo::createAbsoluteLmsUrl('//player', array('course_id' => $this->_params['course']->idCourse));
		}

        // Learningplan model related: LearningCoursepath model
        if (isset($this->_params['learningPlan'])) {
            $replace[CoreNotification::SC_LEARNINGPLAN_NAME] = $this->_params['learningPlan']->path_name;
			$replace[CoreNotification::SC_LEARNINGPLAN_URL]	= Docebo::createAbsoluteLmsUrl('curricula/show');
        }

		// User model related: CoreUser model
		if (isset($this->_params['user'])) {
			$replace[CoreNotification::SC_FIRSTNAME] 	= $this->_params['user']->firstname;
			$replace[CoreNotification::SC_LASTNAME] 	= $this->_params['user']->lastname;
			$replace[CoreNotification::SC_USER_EMAIL] 	= $this->_params['user']->email;
			$replace[CoreNotification::SC_USERNAME] 	= $this->_params['user']->getClearUserId();

			if (!empty($this->_params['user']->passwordPlainText)) {
				$replace[CoreNotification::SC_USER_PASSWORD] 	= $this->_params['user']->passwordPlainText;
			}

		}

		// User model related: CoreUserTemp model
		if (isset($this->_params['userwaiting'])) {
			$replace[CoreNotification::SC_FIRSTNAME] 	= $this->_params['userwaiting']->firstname;
			$replace[CoreNotification::SC_LASTNAME] 	= $this->_params['userwaiting']->lastname;
			$replace[CoreNotification::SC_USERNAME] 	= $this->_params['userwaiting']->userid;
			$replace[CoreNotification::SC_USER_EMAIL] 	= $this->_params['userwaiting']->email;
		}

		// Group model related: CoreGoup model
		if (isset($this->_params['group'])) {
			$replace[CoreNotification::SC_GROUPNAME] 	= $this->_params['group']->description;
			$replace[CoreNotification::SC_GROUPID] 		= $this->_params['group']->groupid;
		}


		// Learning objects related: LearningOrganization model
		if (isset($this->_params['learningobject'])) {
			$replace[CoreNotification::SC_LO_TITLE] 	= $this->_params['learningobject']->title;
		}


		// Forum related: LearningForumthread  model
		if (isset($this->_params['thread'])) {
			$forumModel 		= LearningForum::model()->findByPk($this->_params['thread']->idForum);
			$threadModel = $this->_params['thread'];

			$forumLink  = Docebo::useCustomDomain(Docebo::createAbsoluteLmsUrl('forum/thread/index', array('forum_id' => $forumModel->idForum)));
			$threadLink  = Docebo::useCustomDomain(Docebo::createAbsoluteLmsUrl('forum/messages', array('thread_id' => $threadModel->idThread)));

			$replace[CoreNotification::SC_FORUM_THREAD] 		= $this->_params['thread']->title;
			$replace[CoreNotification::SC_FORUM] 				= $forumModel->title;
			$replace[CoreNotification::SC_FORUM_LINK] 			= $forumLink;
			$replace[CoreNotification::SC_FORUM_THREAD_LINK] 	= $threadLink;
		}

		// Course category related: LearningCourseCategory model
		if (isset($this->_params['category'])) {
			$replace[CoreNotification::SC_CATEGORYNAME] 	= $this->_params['category']->translation;
		}

		// Course-User level related related: LearningCourseuser model
		if (isset($this->_params['courseuser'])) {
			$replace[CoreNotification::SC_USER_COURSE_LEVEL] 	= $this->_params['courseuser']->level;
		}


		// E-commerce related: EcommerceTransaction model
		if (isset($this->_params['transaction'])) {
			$transaction = $this->_params['transaction'];
			$receiptContent = $transaction->getOrderReceiptContent();

			// Full receipt, text format
			$replace[CoreNotification::SC_ORDER_RECEIPT_CONTENT] 	= nl2br($receiptContent);

			// Also, we may have separate receipt data fields/placeholders
			$receiptInfo = $transaction->getOrderReceiptInfo();
			foreach ($receiptInfo as $shortCode => $receiptValue) {
				$replace[$shortCode] = $receiptValue;
			}

			//order items text
			$orderItemsContent = $transaction->getOrderItemsText();
			$replace[CoreNotification::SC_ORDER_ITEMS] = nl2br($orderItemsContent);

		}

		$newText = str_replace(array_keys($replace), array_values($replace), $text);

		return $newText;

	}


	/**
	 * Get user language setting, converted to BROWSER CODE !!!!!
	 *
	 * We use caching here, because if many users are to be
	 * notified, we don't want to query the same table over and over again for the same user
	 *
	 * @param int $idst
	 * @return string - 'en', 'it', etc   (browser's language code)
	 */
	protected function _getUserLanguage($idst) {

		static $cached = array();

		$key = (int) $idst;

		if(isset($cached[$key])) {
			return $cached[$key];
		}

		// Find user in core USER settings table
		$model = CoreSettingUser::model()->findByAttributes(array(
			'path_name' => 'ui.language',
			'id_user' => $key,
		));

		// No user found in User settings table?  Check Waiting users table please
		if (!$model) {
			$tempModel = CoreUserTemp::model()->findByAttributes(array('idst' => $key));
			if ($tempModel && !empty($tempModel->language)) {
				$cached[$key] = Lang::getBrowserCodeByCode($tempModel->language);
				return $cached[$key];
			}
		}
		else {
			if (!empty($model->value)) {
				$cached[$key] = Lang::getBrowserCodeByCode($model->value);
				return $cached[$key];
			}
		}

		// Well, we tried everything, no language found for this user. Fallback to ENGLISH
		$cached[$key] = 'en'; // fallback to english
		return $cached[$key];

	}


	/**
	 * Finally send the email
	 *
	 * @param string $from
	 * @param array $to
	 * @param string $subject
	 * @param string $body
	 */
	protected function _sendEmail($from, $to, $subject, $body, $language, $notification) {
		$mm = new MailManager();
		$mm->mail($from, $to, $subject, $body);
		$this->_log($from, $to, $subject, $body, $language, $notification);
	}



	/**
	 * Write a log if enabled
	 *
	 * @param string $from
	 * @param array $to
	 * @param string $subject
	 * @param string $body
	 * @param string $language
	 * @param @see CoreNotification $notification
	 */
	protected function _log($from, $to, $subject, $body, $language, $notification) {
		if (isset(Yii::app()->params['log_notifications']) && Yii::app()->params['log_notifications'])  {
			Yii::log('Notification sent: ' .  CoreNotification::typeName($notification->type), CLogger::LEVEL_INFO);
			Yii::log('Recipient: ' 	.  CoreNotification::recipientName($notification->recipient), CLogger::LEVEL_INFO);
			Yii::log('Sender Object: ' . get_class($this->_senderObject),CLogger::LEVEL_INFO);
			Yii::log('From: ' . $from . "; To: ". implode(', ', $to), CLogger::LEVEL_INFO);
            Yii::log('Body: ' . $body, CLogger::LEVEL_INFO);
		}
	}

	/**
	 *
	 */
	public static function getTypeName($id) {

		foreach (self::$_EVENTNAME_TYPE_MAP as $name => $mapId) {
			if ($id == $mapId) {
				return $name;
			}
		}

		return "";

	}


}