<?php
/**
 * A class used to represent a certain Notification Type with all its attributes. 
 *  
 *
 */
class NotificationDescriptor extends stdClass {
	
	
	/**
	 * A string to identify the "author/owner/requester" of this notification descriptor, e.g. the custom plugin name: "CurriculeAppModule".
	 * 
	 * @var string
	 */
	public $requester = "unknown";
	
	/**
	 * Most important: the type identifier of the notification. It should match a class name, extending NotificationHandler,
	 * used by notofication job handler. This is valid for Core notifications and for custom notifications relying on core NotificationJobHandler.
	 * 
	 * Custom notifications may decide to use different approach, using their own notification job handler, in which case it is up to the
	 * developer to decide how to match the "type" to the handling mechanic.
	 * 
	 * "type" must be unique across ALL notifications in the LMS, including core and custom ones. Developers must be sure the type is not
	 * already taken by Core code or by another custom notification. Overriding exisiting types is prohibited and "first found - first used" rule is applied.
	 * A proper log messages are issued to warn about an overriding/rejected attempt.
	 * 
	 * Example:  CoursePropHasChanged : this notification will be handled by CoursePropHasChanged class, defined in core NotitficationApp.  
	 * 
	 * @var string
	 */
	public $type;
	public $title;
	public $description;
	public $template;
	public $group;
	
	/**
	 * An array of allowed schedule types: at, after, before
	 * @see CoreNotification::SCHEDULE_*
	 * 
	 * @var array
	 */
	public $allowedScheduleTypes;
	
	/**
	 * An array of allowed recipient types
	 * @see CoreNotification::NOTIFY_*
	 *
	 * @var array
	 */
	public $allowedRecipientTypes;
	
	/**
	 * An array of allowed notification properties/parts: user filter, course filter, etc.
	 * @see CoreNotification::PROP_*
	 *
	 * @var array
	 */
	public $allowedProperties;
	
	/**
	 * An array of shortcode placeholder names, available for email subject and body,
	 * e.g.  array('[first_name]', '[last_name]', ...).
	 * 
	 * Custom notifications can define their own shortcodes, apart from those already defined in CoreNotification model. 
	 *
	 * @see CoreNotification::SC_*
	 * 
	 * @var array
	 */
	public $shortcodes;
	
	/**
	 * A Yii class-path of the Job Handler used to execute the notification jobs, both "at the time" and "before/after"
	 * Defaulting to 'plugin.NotificationApp.components.NotificationJobHandler'  (NotificationJobHandler::HANDLER_ID)
	 * Custom notifications can define their own handler.
	 * 
	 * @var string
	 */
	public $jobHandler;
		
	
	/**
	 * An arbitrary array of custom notification attributes. Allows custom development to feed the notification handler 
	 * with more information for general use. No specific structure, anything goes here.
	 * 
	 * @var array
	 */
	public $customAttributes;
	
	
	/**
	 * Collected errors during validation
	 * 
	 * @var array
	 */
	private $errors = array();
	
	
	/**
	 * Do some basic validation
	 */
	public function validate() {
		
		if (empty($this->type)) {
			$this->errors[] = 'Invalid notitifcation type (empty)! Requested by: ' . $nd->requester;
		}
		
		if (empty($this->title)) {
			$this->errors[] = 'Invalid notitifcation title (empty)! Requested by: ' . $nd->requester;
		}
		
		if (empty($this->jobHandler)) {
			$this->errors[] = 'Invalid notification job handler ID (empty)! Requested by: ' . $nd->requester;
		}
		
		return empty($this->errors);
		
	}

	
	
	public function hasErrors() {
		return !empty($this->errors);	
	}
	
	
	public function errors() {
		return $this->errors;
	}
	
	
	
}