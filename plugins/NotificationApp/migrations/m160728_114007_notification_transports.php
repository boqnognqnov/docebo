<?php

/**
 * Yii DB Migration template.
 *
 * Extending our own DoceboDbMigration (which extends the base CdbMigration)
 *
 */
class m160728_114007_notification_transports extends DoceboDbMigration
{

    public function safeUp()
    {
        try {

            /* @var $db CDbConnection */
            $db = Yii::app()->db;

            //first retrieve the notifications app plugin
            $notificationApp = CorePlugin::model()->findByAttributes(array(
                'plugin_name' => 'NotificationApp'
            ));
            if (empty($notificationApp)) {
                //plugin has never been activated on this system, so notifications tables do not exists yet. Nothing to do here.
                return true;
            }

            //do some updates to core_notification table
            $result = $db->createCommand("ALTER TABLE `core_notification` ADD COLUMN `code` VARCHAR(255) NOT NULL DEFAULT '' AFTER `type`;")->execute();
            if ($result === false) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }

            //create new "core_notification_transport" table
            $result = $db->createCommand("CREATE TABLE `core_notification_transport` ("
                . "`id` INT(11) NOT NULL AUTO_INCREMENT,"
                . "`name` VARCHAR(255) NOT NULL,"
                . "`plugin_id` INT(11) UNSIGNED NOT NULL,"
                . "`info` TEXT NULL,"
                . "PRIMARY KEY (`id`),"
                . "INDEX `fk_ntf_transport_plugin_id_idx` (`plugin_id` ASC),"
                . "CONSTRAINT `fk_ntf_transport_plugin_id` "
                . "FOREIGN KEY (`plugin_id`) REFERENCES `core_plugin` (`plugin_id`) "
                . "ON DELETE CASCADE ON UPDATE CASCADE) "
                . "ENGINE = InnoDB "
                . "DEFAULT CHARACTER SET = utf8;")->execute();
            if ($result === false) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }

            //create "core_notification_transport_activation"
            $result = $db->createCommand("CREATE TABLE `core_notification_transport_activation` ("
                . "`notification_id` BIGINT(20) NOT NULL, "
                . "`transport_id` INT(11) NOT NULL, "
                . "`active` TINYINT(1) NOT NULL DEFAULT 1, "
                . "`additional_info` TEXT NULL, "
                . "PRIMARY KEY (`notification_id`, `transport_id`), "
                . "INDEX `fk_ntf_activation_transport_id_idx` (`transport_id` ASC), "
                . "CONSTRAINT `fk_ntf_activation_notification_id`"
                . " FOREIGN KEY (`notification_id`) "
                . " REFERENCES `core_notification` (`id`) "
                . " ON DELETE CASCADE ON UPDATE CASCADE, "
                . "CONSTRAINT `fk_ntf_activation_transport_id` "
                . " FOREIGN KEY (`transport_id`) "
                . " REFERENCES `core_notification_transport` (`id`) "
                . " ON DELETE CASCADE ON UPDATE CASCADE) "
                . "ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;")->execute();
            if ($result === false) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }

            //update pre-existing transports
            $cmd = $db->createCommand();
            $result = $cmd->insert(CoreNotificationTransport::model()->tableName(), array(
                'name' => "email",
                'plugin_id' => $notificationApp->plugin_id,
                'info' => CJSON::encode(array(
                    'manager_class' => 'EmailTransportManager',
                    'editor_tool' => 'tinymce',
                    'template' => true
                ))
            ));
            if ($result === false) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }
            $result = $cmd->insert(CoreNotificationTransport::model()->tableName(), array(
                'name' => "inbox",
                'plugin_id' => $notificationApp->plugin_id,
                'info' => CJSON::encode(array(
                    'manager_class' => 'InboxTransportManager',
                    'editor_tool' => 'tinymce',
                    'template' => true
                ))
            ));
            if ($result === false) {
                throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
            }
            //now re-read newly inserted transports in order to get their id
            $cmd = $db->createCommand();
            $cmd->select("*");
            $cmd->from(CoreNotificationTransport::model()->tableName());
            $reader = $cmd->query();
            $transports = array();
            if ($reader) {
                while ($record = $reader->read()) {
                    $transports[$record['name']] = $record['id'];
                }
            }

            //update core_notification_translation table
            $result = $db->createCommand("ALTER TABLE `core_notification_translation` DROP FOREIGN KEY `core_notification_translation_ibfk_1`;")->execute();
            $result = $db->createCommand("ALTER TABLE `core_notification_translation` DROP INDEX `notification_id_language`;")->execute();
            $result = $db->createCommand("ALTER TABLE `core_notification_translation` DROP COLUMN `id`,"
                . " ADD COLUMN `transport_id` INT(11) NOT NULL AFTER `notification_id`,"
                . " DROP PRIMARY KEY, ADD PRIMARY KEY (`notification_id`, `language`, `transport_id`),"
                . " ADD INDEX `fk_ntf_translation_transport_id_idx` (`transport_id` ASC);")->execute();
            $result = $db->createCommand("ALTER TABLE `core_notification_translation` "
                . " ADD CONSTRAINT `fk_ntf_translation_notification_id` "
                . " FOREIGN KEY (`notification_id`) REFERENCES `core_notification` (`id`) "
                . " ON DELETE CASCADE ON UPDATE CASCADE;")->execute();
            //update all rows of "transport_id" column with 'email' value in order to initialize the new column with valid content;
            //this is required to apply the next contraint (otherwise an error may be returned, due to inconsistency with foreign key values)
            $result = $db->createCommand()->update(CoreNotificationTranslation::model()->tableName(),
                array('transport_id' => $transports['email']));
            $result = $db->createCommand("ALTER TABLE `core_notification_translation` "
                . " ADD CONSTRAINT `fk_ntf_translation_transport_id` "
                . " FOREIGN KEY (`transport_id`) REFERENCES `core_notification_transport` (`id`) "
                . " ON DELETE CASCADE ON UPDATE CASCADE;")->execute();

            // retrieve the old "notify_type" value for every notification. We will use the information to update
            // the translations associated to the notification
            $cmd = $db->createCommand();
            $cmd->select("id, notify_type");
            $cmd->from(CoreNotification::model()->tableName());
            $reader = $cmd->query();
            $list = array();
            if ($reader) {
                while ($record = $reader->read()) {
                    $list[$record['id']] = $record['notify_type'];
                }
            }
            //process every notification and its translations
            $activationTable = CoreNotificationTransportActivation::model()->tableName();
            $activationCmd = $db->createCommand();
            foreach ($list as $id => $type) {
                //handle translations
                $cmd = $db->createCommand();
                $cmd->select("*");
                $cmd->from(CoreNotificationTranslation::model()->tableName());
                $cmd->where("notification_id = :notification_id");
                $reader = $cmd->query(array(':notification_id' => $id));
                if ($reader) {
                    while ($record = $reader->read()) {
                        switch ($type) {
                            case 'email':
                                //all translations have already been initialized with 'email' transport, no further operation required
                                break;
                            case 'inbox':
                                //existing translations are being changed to the "transport_id" of 'inbox' transport
                                $updateCmd = $db->createCommand();
                                $result = $updateCmd->update(
                                    CoreNotificationTranslation::model()->tableName(),
                                    array('transport_id' => $transports[$type]),
                                    "notification_id = :notification_id",
                                    array(':notification_id' => $id)
                                );
                                if ($result === false) {
                                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                                }

                                break;
                            case 'email_and_inbox':
                                //existing translations must be duplicated, the two records will have "transport_id"  of both 'email' and 'inbox'
                                $insertCmd = $db->createCommand();
                                $result = $insertCmd->insert(CoreNotificationTranslation::model()->tableName(), array(
                                    'notification_id' => $id,
                                    'transport_id' => $transports['inbox'],
                                    'language' => $record['language'],
                                    'subject' => $record['subject'],
                                    'message' => $record['message']
                                ));
                                if ($result === false) {
                                    throw new CException(Yii::t('standard', '_OPERATION_FAILURE'));
                                }
                                break;
                        }
                    }
                }
                //assign the transport activations
                switch ($type) {
                    case 'email':
                        $activationCmd->insert($activationTable,
                            array('notification_id' => $id, 'transport_id' => $transports[$type]));
                        break;
                    case 'inbox':
                        $activationCmd->insert($activationTable,
                            array('notification_id' => $id, 'transport_id' => $transports[$type]));
                        break;
                    case 'email_and_inbox':
                        $activationCmd->insert($activationTable,
                            array('notification_id' => $id, 'transport_id' => $transports['email']));
                        $activationCmd->insert($activationTable,
                            array('notification_id' => $id, 'transport_id' => $transports['inbox']));
                        break;
                }
            }


            //move away "from_name" and "from_email" informations
            $info = array();
            $query = "SELECT id, from_name, from_email FROM " . CoreNotification::model()->tableName();
            $cmd = Yii::app()->db->createCommand($query);
            $reader = $cmd->query();
            if ($reader) {
                while ($record = $reader->read()) {
                    $transportActivation = CoreNotificationTransportActivation::model()->findByPk(array(
                        'notification_id' => $record['id'],
                        'transport_id' => $transports['email']
                    ));
                    if (!empty($transportActivation)) {
                        $transportActivation->additional_info = CJSON::encode(array(
                            'from_name' => $record['from_name'],
                            'from_email' => $record['from_email']
                        ));
                        $transportActivation->save();
                    }
                }
            }


            //clean core_notitification table from no more used columns ?
            $this->dropColumn(CoreNotification::model()->tableName(), 'notify_type');
            $this->dropColumn(CoreNotification::model()->tableName(), 'from_name');
            $this->dropColumn(CoreNotification::model()->tableName(), 'from_email');
        } catch (Exception $e) {

        }
    }

    public function safeDown()
    {
        //this migration cannot be reverted
        return false;
    }


}
