<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class NotificationAppModule extends CWebModule {


	private $_assetsUrl;

	private $cachedNotifications = array();


	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}



	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('NotificationApp.assets'));
		}
		return $this->_assetsUrl;
	}



	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
			'NotificationApp.assets.css.*',
			'NotificationApp.models.*',
			'NotificationApp.components.*',
			'NotificationApp.components.handlers.*',
			'NotificationApp.components.transports.*',
		));

		// Register CSS ONLY if this is NOT an ajax call. Plugins' init() is always called during their loading
		if (!Yii::app()->request->isAjaxRequest) {
			if (method_exists(Yii::app(), 'getClientScript')) { // NOTE: make sure that, when executed in a console, this code won't break.
				$cs = Yii::app()->getClientScript();
				if (is_object($cs) && method_exists($cs, 'registerCssFile')) {
					$cs->registerCssFile($this->getAssetsUrl() . "/css/notifications.css");
				}
			}
		}

		// Get list of Notifications of type "AT THE TIME OF EVENT" and being active
		$criteria = new CDbCriteria();
		$criteria->distinct = true;
		$criteria->select = 'type';
		$criteria->condition = '(active = 1) AND (schedule_type=:scheduleType)';
		$criteria->params[':scheduleType'] = CoreNotification::SCHEDULE_AT;
		$notifications = CoreNotification::model()->findAll($criteria);
		
		// Set single observer for ALL of them
		foreach ($notifications as $n) {
			Yii::app()->event->on($n->type, array($this, 'notificationsListener'));
		}

		// Provide the Notification renderer for inbox message (see InboxApp)
		Yii::app()->event->on("onInboxNotificationRenderer", array($this, 'provideNotificationRendererToInboxApp'));

		// Register the token based SSO handler
		Yii::app()->event->on('RegisterSSOHandlers', array($this, 'registerTokenSsoHandler'));
		Yii::app()->event->on('ResolveNotificationTags', array($this, 'replaceSSOShortcodes'));

		//Yii::app()->onEndRequest = array($this, 'onApplicationFinish');
		//Yii::app()->event->on('onEndRequest', array($this, 'onApplicationFinish'));
		Yii::app()->attachEventHandler('onEndRequest', array($this, 'onApplicationFinish'));
	}



	public function onApplicationFinish($event){
		$this->pipeBackgroundJobsNotifications();
	}




	/**
	 * Listener for all notifications
	 * 
	 * @param DEvent $event
	 */
	public function notificationsListener(DEvent $event) {

		try {

			Yii::log("NOTIFICATION EVENT RAISED: " . $event->event_name, CLogger::LEVEL_INFO);

			$type = $event->event_name;
			$notInfo = CoreNotification::getNotificationInfo($type);

			// Get list of active notifications of the requested type 
			$notifications = CoreNotification::model()->findAllByAttributes(array(
				'type'   => $type,
				'active' => 1,
			));

			// Return if none found
			if (!$notifications || (count($notifications) <= 0)) {
				Yii::log('NO ACTIVE NOTIFCATIONS found of type ' . $event->event_name, CLogger::LEVEL_INFO);
				return 0;
			}
			else {
				Yii::log(count($notifications) . " active notification(s) found of type $type (" . $notInfo['title'] . ")", CLogger::LEVEL_INFO);
			}

			// Enumerate all active notifications and PIPE an "immediate job" to Sidekiq if notification is scheduled "at the time of event"
			// Event of other types (after/before) will be executed by Cronjob + Respective JobHandlers
			foreach ($notifications as $notification) {
				$id = $notification->id;
				if ($notification->schedule_type == CoreNotification::SCHEDULE_AT) {
					Yii::log("[EXECUTE] notification: #$id", 'debug');
					try {
						if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_NOTIFICATIONS)) {
							$this->cachedNotifications[] = array(
								'notification' => $notification,
								'event' => $event
							);
							if(count($this->cachedNotifications) >= 350){
								Yii::log('>350 cached notifications - running background job', CLogger::LEVEL_INFO);
								$this->pipeBackgroundJobsNotifications();
							} else {
								Yii::log('<350 cached notifications - do nothing.', CLogger::LEVEL_INFO);
							}
						} else {
							$this->pipeImmediateNotificationJob($notification, $event);
						}
					} catch (Exception $e) {
						Yii::log('Background jobs disabled - running immediate job', CLogger::LEVEL_INFO);
						Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
					}
				} else {
					Yii::log("[SKIP] notification #$id: scheduled to run after/before the event", 'debug');
				}
			}

		} catch (Exception $e) {
			Yii::log($e->getMessage(), 'error');
		}

	}



	public function registerTokenSsoHandler(DEvent $event){
		if(isset($event->params['map']['notification'])){
			throw new CException('Conflicting SSO handlers. Can not register SSO handler "NotificationSSOHandler" for SSO links with parameter "sso_type=notification". Handler already registered');
		}else{
			$event->params['map']['notification'] = new NotificationSSOHandler();
		}
	}




	/**
	 * Add support for the following tags:
	 * [sso_link], [reset_pwd_link]
	 *
	 * Usage:
	 * [sso_link]
	 * [sso_link expires_in=X], where X is days
	 * [reset_pwd_link]	 *
	 *
	 * @param \DEvent $event
	 *
	 * @see CoreNotification::SC_SSO_EXPIRING_LINK
	 * @see CoreNotification::SC_RESET_PASSWORD_LINK
	 */
	public function replaceSSOShortcodes(DEvent $event){
		/*
			$event variable structure:
			'notification' => CoreNotification,
			'result' => &$result, // array of [shortcode]=>[shortcode value] elements
			'recipient' => $recipient, // the currently iterated recipient element (array)
			'models' => array(
				'group' => $groupModel,
				'userModel' => $userModel,
				'courseModel' => $courseModel,
				'loModel' => $loModel
			)
		 */
		$notif = $event->params['notification']; /* @var $notif CoreNotification */
		$notifType = $notif->type;
		$recipient = $event->params['recipient'];
		$userModel = $event->params['models']['userModel']; /* @var $userModel CoreUser */

		if(!in_array($notif->type, array(
			CoreNotification::NTYPE_USER_SUBSCRIBED_COURSE,
			CoreNotification::NTYPE_COURSE_PLAY_HAS_EXPIRED,
			CoreNotification::NTYPE_COURSE_NOT_YET_COMPLETE
		))){
			return;
		}

		foreach ($notif->translations as $notifTranslation) {
			if ($notifTranslation->language == Lang::getBrowserCodeByCode($recipient['language'])) {
				$emailBody = $notifTranslation->message;

				if (Settings::get( 'sso_token', 'off' ) == 'on') {
					// SSO is enabled in the platform

					DoceboShortcodeParser::parse( $emailBody, array(
						CoreNotification::SC_SSO_EXPIRING_LINK => function ( $tagName, $attributes, $fullTextTagMatch, &$html ) use ( $userModel, $recipient, $event ) {
							$lifetimeDays = isset( $attributes['expires_in'] ) && $attributes['expires_in'] > 0 ? (int) $attributes['expires_in'] : 1;

							$ssoLoginLink = NotificationSSOHandler::getEncryptedSSOLinkForUser(Yii::app()->user->getRelativeUsername($userModel->userid), $lifetimeDays);

							$event->params['result'][ $fullTextTagMatch ] = $ssoLoginLink;
						}
					) );

				}
			}
		}

		// Replace the "password reset" shortcode
		$code = md5(mt_rand() . mt_rand());
		$corePwdRecoverModel = new CorePwdRecover();
		$corePwdRecoverModel->idst_user = $userModel->idst;
		$corePwdRecoverModel->random_code = $code;
		$corePwdRecoverModel->request_date = Yii::app()->localtime->toLocalDateTime();
		if(!$corePwdRecoverModel->save()){
			Yii::log(print_r($corePwdRecoverModel->getErrors(), 1), CLogger::LEVEL_ERROR);
		}else{
			// Get the link, but prevent Url manager to strip "release", if any
			$oldStripRelease = Yii::app()->urlManager->getStripRelease();
			Yii::app()->urlManager->setStripRelease(false);

			$link = Yii::app()->createAbsoluteUrl('user/recoverPwd', array('code' => $code));

			Yii::app()->urlManager->setStripRelease($oldStripRelease);

			$event->params['result'][CoreNotification::SC_RESET_PASSWORD_LINK] = $link;
		}
	}

	public function provideNotificationRendererToInboxApp(DEvent $event){
		if($event->getParamValue('type')==strtolower($this->getId())){ // 'notificationapp'
			$event->return_value = 'plugin.NotificationApp.components.NotificationInboxRenderer';
			$event->preventDefault();
		}
	}

	/**
	 * Creates a One-Time job in Sidekiq queue, passing all event info. Notification job handler will handle the whole work then
	 *
	 * @param CoreNotification $notification
	 */
	protected function pipeBackgroundJobsNotifications() {
		$job = null;
		if(!empty($this->cachedNotifications)) {
			$job = Yii::app()->backgroundJob->createJob('Immidiate notification', 'lms.protected.modules.backgroundjobs.components.handlers.BackgroundNotificationHandler', $this->cachedNotifications);
			$this->cachedNotifications = array();
		}

		return $job;
	}



	/**
	 * Creates a One-Time job in Sidekiq queue, passing all event info. Notification job handler will handle the whole work then
	 *
	 * @param CoreNotification $notification
	 */
	protected function pipeImmediateNotificationJob(CoreNotification $notification, DEvent $event) {
		$params = array(
			'notification_id' => $notification->id,
			'eventParams' => serialize($event->params),
		);
		$idUser = Yii::app()->user->id;

		$job = Yii::app()->scheduler->createImmediateJob('immediate_notification', $notification->job_handler_id, $params, $idUser);
		Yii::log('Created new notification immediate task: '.$job->hash_id, CLogger::LEVEL_INFO);

		return $job;
	}



	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {
		// Add menu voices !
		Yii::app()->mainmenu->addAppMenu('notification', array(
			'icon' => 'admin-ico system-notif',
			'app_icon' => 'fa-bell-o',
			'link' => Docebo::createAdminUrl('/notification/index'),
			'label' => Yii::t('notification', 'Notifications'),
		), array());
	}


}
