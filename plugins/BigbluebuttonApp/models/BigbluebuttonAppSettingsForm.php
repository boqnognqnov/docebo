<?php

/**
 * BigbluebuttonAppSettingsForm class.
 *
 * @property string $account_name
 * @property string $max_rooms
 * @property string $max_rooms_per_course
 * @property string $server_url
 * @property string $secret_salt
 * @property string $port
 * @property string $additional_info
 * @property string $max_concurrent_rooms
 */
class BigbluebuttonAppSettingsForm extends CFormModel {

	public $account_name;
	public $max_rooms;
	public $max_rooms_per_course;
	public $server_url;
	public $secret_salt;
	public $port;
	public $additional_info;
    public $max_concurrent_rooms;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('account_name,max_rooms, max_rooms_per_course, additional_info, server_url, secret_salt, port, max_concurrent_rooms', 'safe'),
			array('account_name, max_rooms, max_rooms_per_course, server_url, secret_salt, port, max_concurrent_rooms', 'required'),
            array('port, max_concurrent_rooms, max_rooms, max_rooms_per_course', 'numerical'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'max_rooms' => Yii::t('configuration','Max total rooms'),
			'max_rooms_per_course' => Yii::t('configuration','Max rooms per course'),
			'max_concurrent_rooms' => Yii::t('configuration','Max concurrent rooms'),
			'server_url' => Yii::t('configuration','_SERVER_ADDR'),
			'secret_salt' => 'Secret salt',
			'port' => Yii::t('configuration','_SMS_GATEWAY_PORT'),
			'additional_info' => Yii::t('webinar', 'Additional information'),
			'account_name' => Yii::t('webinar', 'Account Name')
		);
	}
}
