<?php

/**
 * Class BigbluebuttonWebinarTool
 * Implemenents webinar tool for bigbluebutton
 */
class BigbluebuttonWebinarTool extends WebinarTool {

	/**
	 * Main constructor
	 */
	protected function __construct() {
		$this->id = 'bigbluebutton';
		$this->name = 'Big Blue Button';
	}

	/**
	 * Creates a new room using the Big Blue Button api
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to create the room
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function createRoom($roomName, $roomDescription, $idAccount, $startDateTime, $endDateTime, $maxParticipants, $api_params = array()) {
		// Load api client
		$bbb = new BbbApiClient();

		// Id, passwords
		$meetingId = $this->randomString();
		$attendeePw = $this->randomString();
		$moderatorPw = $this->randomString();

		// Duration
		$durationSeconds = (int) (strtotime($endDateTime) - strtotime($startDateTime));
		if ($durationSeconds > 0)
			$durationMinutes = (int) floor($durationSeconds/60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		// Prepare parameters
		$params = array(
			'meetingId' => $meetingId, 		            // REQUIRED
			'meetingName' => $roomName, 	            // REQUIRED
			'attendeePw' => $attendeePw, 	            // Match this value in getJoinMeetingURL() to join as attendee.
			'moderatorPw' => $moderatorPw, 	            // Match this value in getJoinMeetingURL() to join as moderator.
			'welcomeMsg' => '', 					    // ''= use default. Change to customize.
			'dialNumber' => '', 					    // The main number to call into. Optional.
			'voiceBridge' => '12345', 				    // 5 digit PIN to join voice conference.  Required.
			'webVoice' => '', 						    // Alphanumeric to join voice. Optional.
			'logoutUrl' => false, 					// Default in bigbluebutton.properties. Optional.
			'record' => true, 					    // New. 'true' will tell BBB to record the meeting.
			'duration' => $durationMinutes, 			// Default = 0 which means no set duration in minutes. [number]
			//'meta_category' => '', 				    // Use to pass additional info to BBB server. See API docs.
		);

		$result = $bbb->createMeetingWithXmlResponseArray($params);
		if ($result == NULL)
			throw new CHttpException(500, ConferenceManager::$MSG_FAILED_TO_CONTACT_SERVER);
		else {
			if ($result['returncode'] == 'SUCCESS')
				return array(
					"meeting_id" => $meetingId,
					"attendee_pw" => $attendeePw,
					"moderator_pw" => $moderatorPw,
					"logout_url" => false,
					"duration" => $durationMinutes,
				);
			else {
				if ($result['message'])
					throw new CHttpException(500, $result['message']);
				else
					throw new CHttpException(500, ConferenceManager::$MSG_CONFERENCE_CREATION_FAILED);
			}
		}
	}

	/**
	 * Updates an existing room with the provided params
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param string $roomDescription The description of the room
	 * @param integer $idAccount The id of the account to use to update the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param $startDateTime string The start date&time in UTC + MYSQL/ISO format
	 * @param $endDateTime string The end date&time in UTC + MYSQL/ISO format
	 * @param $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 *
	 * @return array "tool specific" params returned by the api
	 */
	public function updateRoom($roomName, $roomDescription, $idAccount, $api_params = array(), $startDateTime, $endDateTime, $maxParticipants) {
		$bbb = new BbbApiClient();

		// Duration
		$durationSeconds = (int) (strtotime($endDateTime) - strtotime($startDateTime));
		if ( $durationSeconds > 0)
			$durationMinutes = (int) floor($durationSeconds/60);
		else
			$durationMinutes = 0; // infinite; until last person leaves

		// Prepare parameters
		$params = array(
			'meetingId' => $api_params['meeting_id'],
			'meetingName' => $roomName,
			'attendeePw' => $api_params['attendee_pw'],
			'moderatorPw' => $api_params['moderator_pw'],
			'record' => true, 					    	// New. 'true' will tell BBB to record the meeting.
			'duration' => $durationMinutes, 			// Default = 0 which means no set duration in minutes. [number]
		);

		$bbb->createMeetingWithXmlResponseArray($params);
		$api_params['name'] = $roomName;
		$api_params['duration'] = $durationMinutes;

		return $api_params;
	}

	/**
	 * Deletes an existing room
	 *
	 * @param integer $idAccount The id of the account to use to delete the room
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 */
	public function deleteRoom($idAccount, $api_params = array()) {
		$endParams = array (
			'meetingId' => $api_params['meeting_id'],
			'password' => $api_params['moderator_pw']
		);
		$bbb = new BbbApiClient();
		$bbb->endMeetingWithXmlResponseArray($endParams);
	}

	/**
	 * Returns the URL to join a room
	 *
	 * @param string         $roomName         The name of the room to create in the videconference platform
	 * @param integer        $maxParticipants  integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer        $idAccount        The id of the account to use
	 * @param array          $api_params       An array of "tool specific" params to pass to the api
	 * @param CoreUser       $userModel        The id of the user for whom the current URL is returned
	 * @param WebinarSession $sessionDateModel The current session object
	 */
	public function getJoinRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionDateModel, $widgetContext = false) {
		$bbb = new BbbApiClient();
		$creationParams = array(
			'meetingName' => $roomName,
			'meetingId' => $api_params['meeting_id'],
			'attendeePw' => $api_params['attendee_pw'],
			'moderatorPw' => $api_params['moderator_pw'],
			'welcomeMsg' => '',
			'dialNumber' => '',
			'voiceBridge' => '',
			'webVoice' => '',
			'logoutUrl' => '',
			'maxParticipants' => $maxParticipants, // Optional. -1 = unlimitted. Not supported in BBB. [number]
			'record' => 'true',
			'duration' => $api_params['duration'],
			'meta_category' => '',
		);


		// Re-create the meeting using save ID, passowrds etc. (when we created the LMS room locally)
		$bbb->createMeetingWithXmlResponseArray($creationParams);

		// Common JOIN parameters
		$joinParams = array(
			'meetingId' => $api_params['meeting_id'],
			'username' => $this->getUserDisplayName($userModel),
			'password' => $api_params['attendee_pw']
		);

		// Get the JOIN URL
		return $bbb->getJoinMeetingURL($joinParams);
	}

	/**
	 * Returns the URL to start a room
	 *
	 * @param string $roomName The name of the room to create in the videconference platform
	 * @param integer $maxParticipants integer The maximum number of enrollments in the webinar session (null -> unlimited)
	 * @param integer $idAccount The id of the account to use
	 * @param array $api_params An array of "tool specific" params to pass to the api
	 * @param CoreUser $userModel The id of the user for whom the current URL is returned
	 * @param WebinarSessionDate $sessionModel The current session object
	 */
	public function getStartRoomUrl($roomName, $maxParticipants, $idAccount, $api_params, $userModel, $sessionModel) {
		$bbb = new BbbApiClient();
		$creationParams = array(
			'meetingName' => $roomName,
			'meetingId' => $api_params['meeting_id'],
			'attendeePw' => $api_params['attendee_pw'],
			'moderatorPw' => $api_params['moderator_pw'],
			'welcomeMsg' => '',
			'dialNumber' => '',
			'voiceBridge' => '',
			'webVoice' => '',
			'logoutUrl' => '',
			'maxParticipants' => $maxParticipants, // Optional. -1 = unlimitted. Not supported in BBB. [number]
			'record' => 'true',
			'duration' => $api_params['duration'],
			'meta_category' => '',
		);


		// Re-create the meeting using save ID, passowrds etc. (when we created the LMS room locally)
		$bbb->createMeetingWithXmlResponseArray($creationParams);

		// Common JOIN parameters
		$joinParams = array(
			'meetingId' => $api_params['meeting_id'],
			'username' => $this->getUserDisplayName($userModel),
			'password' => $api_params['moderator_pw']
		);

		// Get the JOIN URL
		return $bbb->getJoinMeetingURL($joinParams);
	}

} 