<?php
/**
 * BigBlueButton open source conferencing system - http://www.bigbluebutton.org/
 * API Client
 * 
 * @author Plamen Petkov
 * 
 * Copyright (c) 2013 Docebo
 * http://www.docebo.com
 *
 */
class BbbApiClient extends CComponent {

    // Must match the server salt (configurable). Taken from LMS settings
    private $_securitySalt;
    
    // Base URL, like http://bbb.somedomain.eu. Taken from LMS settings
    private $_bbbServerBaseUrl;
    
    // Port number
    private $_port;

    // Local/Class usage parameters
    private $_meetingId = NULL;
    private $_meetingName = "";
    private $_username = "";
    private $_password = "";
    
    
    /**
     * Constructor
     * 
     */
    public function __construct($server_url = null, $secret_salt = null) {
		// If the new parameters are not passed, try to load the default webinar tools account
		if(!$server_url && !$secret_salt) {
			$account = WebinarToolAccount::model()->findByAttributes(array('tool' => 'bigbluebutton'));
			if($account) {
				$server_url = $account->settings->server_url;
				$secret_salt = $account->settings->secret_salt;
			}
		}
       	$this->init($server_url, $secret_salt);
    }
    
    /**
     * Init. Called by constructor
     * 
     */
    public function init($server_url = null, $secret_salt = null) {
        $this->_securitySalt 	 = $secret_salt;
        $this->_bbbServerBaseUrl = $server_url;
        $this->_bbbServerBaseUrl = rtrim($this->_bbbServerBaseUrl, '/') . "/bigbluebutton/";
        $this->_port = "80";
    }


	/**
	 *
	 * @param string $url
	 *
	 * @throws Exception
	 * @return SimpleXMLElement
	 */
    private function _processXmlResponse($url){
        if (extension_loaded('curl')) {
            $ch = curl_init() or die ( curl_error($ch) );
            $timeout = 10;
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec( $ch );
	        $info = curl_getinfo($ch);
            curl_close( $ch );

            if ($data) {
	            if(isset($info['http_code']) && $info['http_code'] == 404){
		            throw new Exception('Invalid HTTP code returned - '.$info['http_code'].'. Check the BBB server URL.');
	            }else{
		            try{
                        $result = new SimpleXMLElement($data);
		            }catch(Exception $e){
			            throw new Exception('Invalid XML response by BBB server: '.$e->getMessage());
		            }
                    return $result;
	            }
            } else {
	            Yii::log("No response received from BigBlueButton server. Called URL: ".$url, CLogger::LEVEL_ERROR);
	            throw new Exception('No data returned from BBB server');
            }
        }

	    // If CURL not present, we reach here
        return simplexml_load_file($url);
    }
    
    
    /**
     * Process required params and throw errors if we don't get values
     * 
     * @param string $param
     * @throws Exception
     * @return string
     */
    private function _requiredParam($param) {
        if ((isset($param)) && ($param != '')) {
            return $param;
        }
        elseif (!isset($param)) {
            throw new Exception('Missing parameter.');
        }
        else {
            throw new Exception(''.$param.' is required.');
        }
    }    
    
    /**
     * Pass most optional params through as set value, or set to '' 
     * Don't know if we'll use this one, but let's build it in case.
     * 
     * @param string $param
     * @return unknown|string
     */
    private function _optionalParam($param) {
        if ((isset($param)) && ($param != '')) {
            return $param;
        }
        else {
            $param = '';
            return $param;
        }
    }    
    
    
    /* __________________ BBB ADMINISTRATION METHODS _________________ */
    /* The methods in the following section support the following categories of the BBB API:
     -- create
    -- join
    -- end
    */
    
    
    /**
     * Returns a "create meeting" URL 
     * 
     * @param array $creationParams
     * @return string
     */
    public function getCreateMeetingUrl($creationParams) {
        $this->_meetingId = $this->_requiredParam($creationParams['meetingId']);
        $this->_meetingName = $this->_requiredParam($creationParams['meetingName']);
        
        $creationUrl = $this->_bbbServerBaseUrl."api/create?";
        
        // Add params:
        $params =
            'name='.urlencode($this->_meetingName).
            '&meetingID='.urlencode($this->_meetingId).
            '&attendeePW='.urlencode($creationParams['attendeePw']).
            '&moderatorPW='.urlencode($creationParams['moderatorPw']).
            '&dialNumber='.urlencode($creationParams['dialNumber']).
            '&voiceBridge='.urlencode($creationParams['voiceBridge']).
            '&webVoice='.urlencode($creationParams['webVoice']).
            '&logoutURL='.urlencode($creationParams['logoutUrl']).
            '&maxParticipants='.urlencode($creationParams['maxParticipants']).
            '&record='.urlencode($creationParams['record']).
            '&duration='.urlencode($creationParams['duration']);
            //'&meta_category='.urlencode($creationParams['meta_category']);
            
        $welcomeMessage = $creationParams['welcomeMsg'];
        if(trim($welcomeMessage)) {
            $params .= '&welcome='.urlencode($welcomeMessage);
        }
        
        $finalString = "create".$params.$this->_securitySalt;
        
        $url = $creationUrl.$params.'&checksum='.sha1($finalString);
        
        return ( $url );
    }
    
    
    /**
     * Create a meeting
     * 
     * @param array $creationParams
     * @throws Exception
     * @return multitype:NULL |NULL
     */
    public function createMeetingWithXmlResponseArray($creationParams) {
        /*
         USAGE:
        $creationParams = array(
                'name' => 'Meeting Name',	-- A name for the meeting (or username)
                'meetingId' => '1234',		-- A unique id for the meeting
                'attendeePw' => 'ap',  		-- Set to 'ap' and use 'ap' to join = no user pass required.
                'moderatorPw' => 'mp', 		-- Set to 'mp' and use 'mp' to join = no user pass required.
                'welcomeMsg' => '', 		-- ''= use default. Change to customize.
                'dialNumber' => '', 		-- The main number to call into. Optional.
                'voiceBridge' => '12345', 	-- 5 digit PIN to join voice conference.  Required.
                'webVoice' => '', 			-- Alphanumeric to join voice. Optional.
                'logoutUrl' => '', 			-- Default in bigbluebutton.properties. Optional.
                'maxParticipants' => '-1', 	-- Optional. -1 = unlimitted. Not supported in BBB. [number]
                'record' => 'false', 		-- New. 'true' will tell BBB to record the meeting.
                'duration' => '0', 			-- Default = 0 which means no set duration in minutes. [number]
                'meta_category' => '', 		-- Use to pass additional info to BBB server. See API docs to enable.
        );
        */
        
        $xml = $this->_processXmlResponse($this->getCreateMeetingURL($creationParams));
    
        if($xml) {
            if($xml->meetingID)
                return array(
                        'returncode' => $xml->returncode,
                        'message' => $xml->message,
                        'messageKey' => $xml->messageKey,
                        'meetingId' => $xml->meetingID,
                        'attendeePw' => $xml->attendeePW,
                        'moderatorPw' => $xml->moderatorPW,
                        'hasBeenForciblyEnded' => $xml->hasBeenForciblyEnded,
                        'createTime' => $xml->createTime
                );
            else
                return array(
                        'returncode' => $xml->returncode,
                        'message' => $xml->message,
                        'messageKey' => $xml->messageKey
                );
        }
        else {
            return NULL;
        }
    }
    
    
    /**
     * Get URL to join a meeting
     * 
     * @param array $joinParams
     * @return string
     */
    public function getJoinMeetingURL($joinParams) {
        /*
         NOTE: At this point, we don't use a corresponding joinMeetingWithXmlResponse here because the API
        doesn't respond on success, but you can still code that method if you need it. Or, you can take the URL
        that's returned from this method and simply send your users off to that URL in your code.
        USAGE:
        $joinParams = array(
                'meetingId' => '1234',		-- REQUIRED - A unique id for the meeting
                'username' => 'Jane Doe',	-- REQUIRED - The name that will display for the user in the meeting
                'password' => 'ap',			-- REQUIRED - The attendee or moderator password, depending on what's passed here
                'createTime' => '',			-- OPTIONAL - string. Leave blank ('') unless you set this correctly.
                'userID' => '',				-- OPTIONAL - string
                'webVoiceConf' => ''		-- OPTIONAL - string
        );
        */
        
        $this->_meetingId = $this->_requiredParam($joinParams['meetingId']);
        $this->_username = $this->_requiredParam($joinParams['username']);
        $this->_password = $this->_requiredParam($joinParams['password']);
        
        // Establish the basic join URL:
        $joinUrl = $this->_bbbServerBaseUrl."api/join?";
        
        // Add parameters to the URL:
        $params =
            'meetingID='.urlencode($this->_meetingId).
            '&fullName='.urlencode($this->_username).
            '&password='.urlencode($this->_password).
            '&userID='.urlencode($joinParams['userId']).
            '&webVoiceConf='.urlencode($joinParams['webVoiceConf']);
        
        // Only use createTime if we really want to use it. If it's '', then don't pass it:
        if (((isset($joinParams['createTime'])) && ($joinParams['createTime'] != ''))) {
            $params .= '&createTime='.urlencode($joinParams['createTime']);
        }
        
        // Return the URL:
        return ($joinUrl.$params.'&checksum='.sha1("join".$params.$this->_securitySalt));
    }    
    

    /**
     * Get URL that will end a meeting
     * 
     * @param array $endParams
     * @return string
     */
    public function getEndMeetingURL($endParams) {
        /* USAGE:
         $endParams = array (
                 'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
                 'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
         );
         
        */
        $this->_meetingId = $this->_requiredParam($endParams['meetingId']);
        $this->_password = $this->_requiredParam($endParams['password']);
        $endUrl = $this->_bbbServerBaseUrl."api/end?";
        $params =
            'meetingID='.urlencode($this->_meetingId).
            '&password='.urlencode($this->_password);
        
        return ($endUrl.$params.'&checksum='.sha1("end".$params.$this->_securitySalt));
    }
    
    /**
     * End a meeting
     * 
     * @param array $endParams
     * @return multitype:NULL |NULL
     */
    public function endMeetingWithXmlResponseArray($endParams) {
        /* USAGE:
         $endParams = array (
                 'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
                 'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
         );
        */
        
        $xml = $this->_processXmlResponse($this->getEndMeetingURL($endParams));
        if($xml) {
            return array(
                    'returncode' => $xml->returncode,
                    'message' => $xml->message,
                    'messageKey' => $xml->messageKey
            );
        }
        else {
            return NULL;
        }
    
    }
    
    
    /* __________________ BBB MONITORING METHODS _________________ */
    /* The methods in the following section support the following categories of the BBB API:
     -- isMeetingRunning
    -- getMeetings
    -- getMeetingInfo
    */
    
    /**
     * Get an URL to check if a meeting is running
     * 
     * @param string $meetingId
     * @return string
     */
    public function getIsMeetingRunningUrl($meetingId) {
        /* USAGE:
         $meetingId = '1234'		-- REQUIRED - The unique id for the meeting
        */
        
        $this->_meetingId = $this->_requiredParam($meetingId);
        $runningUrl = $this->_bbbServerBaseUrl."api/isMeetingRunning?";
        $params =
            'meetingID='.urlencode($this->_meetingId);
        
        return ($runningUrl.$params.'&checksum='.sha1("isMeetingRunning".$params.$this->_securitySalt));
    }
    
    /**
     * Make an API call to check if meeting is running
     * 
     * @param string $meetingId
     * @return multitype:NULL |NULL
     */
    public function isMeetingRunningWithXmlResponseArray($meetingId) {
        /* USAGE:
         $meetingId = '1234'		-- REQUIRED - The unique id for the meeting
        */
        
        $xml = $this->_processXmlResponse($this->getIsMeetingRunningUrl($meetingId));
        if($xml) {
            return array(
                    'returncode' => $xml->returncode,
                    'running' => $xml->running 	// -- Returns true/false.
            );
        }
        else {
            return NULL;
        }
    
    }
    
    /**
     * Returns an URL to use for getting list of meetings
     * 
     * @return string
     */
    public function getGetMeetingsUrl() {
        /* Simply formulate the getMeetings URL
         We do this in a separate function so we have the option to just get this
        URL and print it if we want for some reason.
        */
        
        $getMeetingsUrl = $this->_bbbServerBaseUrl."api/getMeetings?checksum=".sha1("getMeetings".$this->_securitySalt);
        return $getMeetingsUrl;
    }
    
    /**
     * API call to get list of running meetings
     * 
     * @return mixed
     */
    public function getMeetingsWithXmlResponseArray() {
        /* USAGE:
         We don't need to pass any parameters with this one, so we just send the query URL off to BBB
        and then handle the results that we get in the XML response.
        */
        
        $xml = $this->_processXmlResponse($this->getGetMeetingsUrl());
        if($xml) {
            // If we don't get a success code, stop processing and return just the returncode:
            if ($xml->returncode != 'SUCCESS') {
                $result = array(
                        'returncode' => $xml->returncode
                );
                return $result;
            }
            elseif ($xml->messageKey == 'noMeetings') {
                /* No meetings on server, so return just this info: */
                $result = array(
                        'returncode' => $xml->returncode,
                        'messageKey' => $xml->messageKey,
                        'message' => $xml->message
                );
                return $result;
            }
            else {
                // In this case, we have success and meetings. First return general response:
                $result = array(
                        'returncode' => $xml->returncode,
                        'messageKey' => $xml->messageKey,
                        'message' => $xml->message
                );
                // Then interate through meeting results and return them as part of the array:
                foreach ($xml->meetings->meeting as $m) {
                    $result[] = array(
                            'meetingId' => $m->meetingID,
                            'meetingName' => $m->meetingName,
                            'createTime' => $m->createTime,
                            'attendeePw' => $m->attendeePW,
                            'moderatorPw' => $m->moderatorPW,
                            'hasBeenForciblyEnded' => $m->hasBeenForciblyEnded,
                            'running' => $m->running
                    );
                }
                return $result;
            }
        }
        else {
            return NULL;
        }
    
    }
    
    /**
     * Returns an URL to get meeting information
     * 
     * @param array $infoParams
     * @return string
     */
    public function getMeetingInfoUrl($infoParams) {
        /* USAGE:
         $infoParams = array(
                 'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
                 'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
         );
        */
        
        $this->_meetingId = $this->_requiredParam($infoParams['meetingId']);
        $this->_password = $this->_requiredParam($infoParams['password']);
        $infoUrl = $this->_bbbServerBaseUrl."api/getMeetingInfo?";
        $params =
            'meetingID='.urlencode($this->_meetingId).
            '&password='.urlencode($this->_password);
        
        return ($infoUrl.$params.'&checksum='.sha1("getMeetingInfo".$params.$this->_securitySalt));
    }
    
    /**
     * API call to get meeting information
     * 
     * @param array $infoParams
     * @return mixed
     */
    public function getMeetingInfoWithXmlResponseArray($infoParams) {
        /* USAGE:
         $infoParams = array(
                 'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
                 'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
         );
        */
        
        $xml = $this->_processXmlResponse($this->getMeetingInfoUrl($infoParams));
        if($xml) {
            // If we don't get a success code or messageKey, find out why:
            if (($xml->returncode != 'SUCCESS') || ($xml->messageKey == NULL)) {
                $result = array(
                        'returncode' => $xml->returncode,
                        'messageKey' => $xml->messageKey,
                        'message' => $xml->message
                );
                return $result;
            }
            else {
                // In this case, we have success and meeting info:
                $result = array(
                        'returncode' => $xml->returncode,
                        'meetingName' => $xml->meetingName,
                        'meetingId' => $xml->meetingID,
                        'createTime' => $xml->createTime,
                        'voiceBridge' => $xml->voiceBridge,
                        'attendeePw' => $xml->attendeePW,
                        'moderatorPw' => $xml->moderatorPW,
                        'running' => $xml->running,
                        'recording' => $xml->recording,
                        'hasBeenForciblyEnded' => $xml->hasBeenForciblyEnded,
                        'startTime' => $xml->startTime,
                        'endTime' => $xml->endTime,
                        'participantCount' => $xml->participantCount,
                        'maxUsers' => $xml->maxUsers,
                        'moderatorCount' => $xml->moderatorCount,
                );
                // Then interate through attendee results and return them as part of the array:
                foreach ($xml->attendees->attendee as $a) {
                    $result[] = array(
                            'userId' => $a->userID,
                            'fullName' => $a->fullName,
                            'role' => $a->role
                    );
                }
                return $result;
            }
        }
        else {
            return NULL;
        }
    
    }
    
    /* __________________ BBB RECORDING METHODS _________________ */
    /* The methods in the following section support the following categories of the BBB API:
     -- getRecordings
    -- publishRecordings
    -- deleteRecordings
    */
    
    /**
     * Returns URL to use to get Recordings
     * 
     * @param array $recordingParams
     * @return string
     */
    public function getRecordingsUrl($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'meetingId' => '1234',		-- OPTIONAL - comma separate if multiple ids
         );
        */
        
        $recordingsUrl = $this->_bbbServerBaseUrl."api/getRecordings?";
        $params =
            'meetingID='.urlencode($recordingParams['meetingId']);
        
        return ($recordingsUrl.$params.'&checksum='.sha1("getRecordings".$params.$this->_securitySalt));
    
    }
    
    /**
     * API Call to get recordings
     * 
     * @param unknown $recordingParams
     * @return multitype:NULL |Ambigous <multitype:NULL , multitype:NULL multitype:NULL  >|NULL
     */
    public function getRecordingsWithXmlResponseArray($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'meetingId' => '1234',		-- OPTIONAL - comma separate if multiple ids
         );
        NOTE: 'duration' DOES work when creating a meeting, so if you set duration
        when creating a meeting, it will kick users out after the duration. Should
        probably be required in user code when 'recording' is set to true.
        */
        
        $xml = $this->_processXmlResponse($this->getRecordingsUrl($recordingParams));
        if($xml) {
            // If we don't get a success code or messageKey, find out why:
            if (($xml->returncode != 'SUCCESS') || ($xml->messageKey == NULL)) {
                $result = array(
                        'returncode' => $xml->returncode,
                        'messageKey' => $xml->messageKey,
                        'message' => $xml->message
                );
                return $result;
            }
            else {
                // In this case, we have success and recording info:
                $result = array(
                        'returncode' => $xml->returncode,
                        'messageKey' => $xml->messageKey,
                        'message' => $xml->message
                );
    
                foreach ($xml->recordings->recording as $r) {
                    $result[] = array(
                            'recordId' => $r->recordID,
                            'meetingId' => $r->meetingID,
                            'name' => $r->name,
                            'published' => $r->published,
                            'startTime' => $r->startTime,
                            'endTime' => $r->endTime,
                            'playbackFormatType' => $r->playback->format->type,
                            'playbackFormatUrl' => $r->playback->format->url,
                            'playbackFormatLength' => $r->playback->format->length,
                            'metadataTitle' => $r->metadata->title,
                            'metadataSubject' => $r->metadata->subject,
                            'metadataDescription' => $r->metadata->description,
                            'metadataCreator' => $r->metadata->creator,
                            'metadataContributor' => $r->metadata->contributor,
                            'metadataLanguage' => $r->metadata->language,
                            // Add more here as needed for your app depending on your
                            // use of metadata when creating recordings.
                    );
                }
                return $result;
            }
        }
        else {
            return NULL;
        }
    }
    
    /**
     * 
     * @param array $recordingParams
     * @return string
     */
    public function getPublishRecordingsUrl($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
                 'publish' => 'true',		-- REQUIRED - boolean: true/false
         );
        */
        $recordingsUrl = $this->_bbbServerBaseUrl."api/publishRecordings?";
        $params =
        'recordID='.urlencode($recordingParams['recordId']).
        '&publish='.urlencode($recordingParams['publish']);
        return ($recordingsUrl.$params.'&checksum='.sha1("publishRecordings".$params.$this->_securitySalt));
    
    }
    
    /**
     * 
     * @param array $recordingParams
     * @return mixed
     */
    public function publishRecordingsWithXmlResponseArray($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
                 'publish' => 'true',		-- REQUIRED - boolean: true/false
         );
        */
        $xml = $this->_processXmlResponse($this->getPublishRecordingsUrl($recordingParams));
        if($xml) {
            return array(
                    'returncode' => $xml->returncode,
                    'published' => $xml->published 	// -- Returns true/false.
            );
        }
        else {
            return NULL;
        }
    
    
    }
    
    /**
     * 
     * @param array $recordingParams
     * @return string
     */
    public function getDeleteRecordingsUrl($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
         );
        */
        $recordingsUrl = $this->_bbbServerBaseUrl."api/deleteRecordings?";
        $params =
        'recordID='.urlencode($recordingParams['recordId']);
        return ($recordingsUrl.$params.'&checksum='.sha1("deleteRecordings".$params.$this->_securitySalt));
    }
    
    /**
     * 
     * @param array $recordingParams
     * @return multitype:NULL |NULL
     */
    public function deleteRecordingsWithXmlResponseArray($recordingParams) {
        /* USAGE:
         $recordingParams = array(
                 'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
         );
        */
    
        $xml = $this->_processXmlResponse($this->getDeleteRecordingsUrl($recordingParams));
        if($xml) {
            return array(
                    'returncode' => $xml->returncode,
                    'deleted' => $xml->deleted 	// -- Returns true/false.
            );
        }
        else {
            return NULL;
        }
    
    }
    
    
    
    
}

?>