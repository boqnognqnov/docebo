<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class CoursecatalogAppModule extends CWebModule {


	public function init() {

		//events handlers
		Yii::app()->event->on('CollectProgressiveJobRun', array(new CoursecatalogLib(), 'progressiveJobRun'));
		
		Yii::app()->event->on('CollectMyDashboardDashletWidgets', array($this, 'provideDashletDescriptor'));

		/* Courses Catalog App Tile */
		Yii::app()->event->on('RenderCoursesLabelTile', array(new CoursecatalogLib(), 'onRenderCoursesLabelTile'));
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {

		Yii::app()->mainmenu->addAppMenu('coursecatalog', array(
			'icon' => 'admin-ico catalog',
			'app_icon' => 'fa-book', // icon used in the new menu
			'link' => Docebo::createAdminUrl('//CoursecatalogApp/CourseCatalogManagement/index'),
			'label' => Yii::t('standard', 'Catalog'),
			'settings' => Docebo::createAdminUrl('//CoursecatalogApp/CoursecatalogApp/settings'),
			'permission' => '/framework/admin/catalogManagement/del|/framework/admin/catalogManagement/view|/framework/admin/catalogManagement/add|/framework/admin/catalogManagement/mod',
		), array());
	}


	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Respond to the dashboard dashlet collector 
	 * @param DEvent $event
	 */
	public function provideDashletDescriptor(DEvent $event){
		
		if(!isset($event->params['descriptors'])){
			return;
		}
	
		$descriptor = DashletCatalog::descriptor();
		
		$collectedSoFarHandlers = array();
		foreach($event->params['descriptors'] as $descriptorObj){
			if ($descriptorObj instanceof DashletDescriptor) {
				$collectedSoFarHandlers[] = $descriptorObj->handler;
			}
		}
	
		if(!in_array($descriptor->handler, $collectedSoFarHandlers)){
			$event->params['descriptors'][] = $descriptor;
		}
	
	}
	

}
