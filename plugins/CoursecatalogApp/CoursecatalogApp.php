<?php

/**
 * Plugin for CoursecatalogApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class CoursecatalogApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'CoursecatalogApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		// enable the feature
		Settings::save('catalog_enabled', '0', 'int', 1);
		Settings::save('catalog_type', 'catalog', 'string', 255);
		Settings::save('catalog_external', 'off', 'enum', 3);
	}


	public function deactivate() {
		parent::deactivate();
		// disable the feature
		Settings::save('catalog_enabled', '0', 'int', 1);
		Settings::save('first_catalogue', 'off', 'enum', 3);
		Settings::save('catalog_external', 'off', 'enum', 3);

		// Disable catalog from appearing after the app is disabled
		Settings::save('catalog_type', 'mycourses');

		//Removing widgets from dashboards
		DashboardDashlet::model()->deleteAllByAttributes(array('handler' => 'mydashboard.widgets.DashletCatalog'));
	}


}
