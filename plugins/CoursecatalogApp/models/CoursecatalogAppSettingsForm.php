<?php

/**
 * CoursecatalogAppSettingsForm class.
 *
 * @property string $catalog_type
 * @property string $first_catalogue
 * @property string $on_catalogue_empty
 */
class CoursecatalogAppSettingsForm extends CFormModel {
	public $catalog_external;
	public $catalog_type;
	public $first_catalogue;
	public $on_catalogue_empty;
	public $catalog_use_categories_tree;
	public $show_course_details_dedicated_page;
	public $catalog_external_selected_catalogs;

	public static function getValuesDropdown()
	{
		$data = array(
			'mycourses'	=> Yii::t('CoursecatalogApp', 'Don\'t show the course catalog'),
			'catalog'	=> Yii::t('CoursecatalogApp', 'Show a link to the course catalog')
		);
		return $data;
	}

	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('catalog_type,catalog_external,first_catalogue,on_catalogue_empty,catalog_use_categories_tree,show_course_details_dedicated_page, catalog_external_selected_catalogs', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'catalog_external' => Yii::t('EcommerceApp','Enable the public catalog'),
			'catalog_type' => Yii::t('CoursecatalogApp', 'Catalog type'),
			'first_catalogue' => Yii::t('configuration','_FIRST_CATALOGUE'),
			'on_catalogue_empty' => Yii::t('configuration','_ON_CATALOGUE_EMPTY'),
			'catalog_use_categories_tree' => Yii::t('CoursecatalogApp', 'Use categories tree'),
			'show_course_details_dedicated_page' => Yii::t('CoursecatalogApp', 'Show the course details as a dedicated page'),
			'catalog_external_selected_catalogs' => Yii::t('CoursecatalogApp', 'Selected custom catalogs'),
		);
	}

	public function beforeValidate() {
	
		$this->catalog_external = ($this->catalog_external ? 'on' : 'off');
		$this->first_catalogue = ($this->first_catalogue ? 'on' : 'off');
		$this->on_catalogue_empty = ($this->on_catalogue_empty ? 'on' : 'off');
		$this->catalog_use_categories_tree = ($this->catalog_use_categories_tree ? 'on' : 'off');
		$this->show_course_details_dedicated_page = ($this->show_course_details_dedicated_page? 'on' : 'off');
		$this->catalog_external_selected_catalogs = (!$this->catalog_external_selected_catalogs ? ''  : $this->catalog_external_selected_catalogs);
		return parent::beforeValidate();
	}


}
