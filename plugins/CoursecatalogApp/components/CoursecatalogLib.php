<?php

/**
 * Course catalogs events handler and more
 * Class CoursecatalogLib
 */
class CoursecatalogLib extends CComponent {


	const JOB_TYPE_UNASSIGN_USERS  = 'catalog_unassign_users';
	const JOB_TYPE_ASSIGN_USERS    = 'catalog_assign_users';
	const JOB_TYPE_ASSIGN_COURSES  = 'catalog_assign_courses';


	/**
	 * Handle progressive job, but with our own custom job type(s)
	 * @param DEvent $event
	 */
	public function progressiveJobRun(DEvent $event) {

		$data = $event->params['data'];

		switch ($data['type']) {
			case self::JOB_TYPE_UNASSIGN_USERS:
				$params = array(
					'type' => $data['type'],
					'id_catalog' => $data['id_catalog']
				);
				$url = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/axProgressiveUnassignUsers', $params);
				$event->preventDefault();
				$event->return_value['url'] = $url;
			break;
			case self::JOB_TYPE_ASSIGN_USERS:
				$params = array(
					'id_catalog' => $data['id_catalog']
				);
				$url = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/axProgressiveAssignUsers', $params);
				$event->preventDefault();
				$event->return_value['url'] = $url;
			break;
			
			case self::JOB_TYPE_ASSIGN_COURSES:
			    $params = array(
                    'id_catalog' => $data['id_catalog']
			    );
			    $url = Docebo::createAbsoluteAdminUrl('CoursecatalogApp/CourseCatalogManagement/axProgressiveAssignCourses', $params);
			    $event->preventDefault();
			    $event->return_value['url'] = $url;
			    break;
			    	
		}

	}

	/**
	 * Course catalogs events handler and more
	 * Class CoursecatalogLib
	 */
	public function onRenderCoursesLabelTile(DEvent $event) {
		$event->params['title'] = Yii::t('standard', 'Courses catalogs');
		$event->params['description'] = '';
	}

}