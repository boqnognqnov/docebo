<?php

class CourseCatalogManagementController extends AdminController
{

	public function filters()
	{
			return array(
				'accessControl' => 'accessControl',
			);
		}

		/**
		 * Returns list of allowed actions.
		 * @return array actions configuration.
		 */
	public function accessRules()
	{
			$res = array();

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/adminmanager/view'),
				'actions' => array(
					'index',
					'create',
					'update',
					'delete',
					'assignUsers',
					'assignSelectUsers',
					'deleteAssignedUser',
					'assignCourses',
					'assignSelectCourses',
					'deleteAssignedCourse',
					'assignUsersToCatalog',
				    'assignCoursesToCatalog',
					'axProgressiveAssignUsers',
					'axProgressiveUnassignUsers',
				    'axProgressiveAssignCourses',
				),
			);
			$res[] = array(
				'allow',
				'roles' => array('/framework/level/admin'),
				'actions' => array(
					'index',
					'create',
					'update',
					'delete',
					'assignUsers',
					'assignSelectUsers',
					'deleteAssignedUser',
					'assignSelectCourses',
					'deleteAssignedCourse',
					'assignUsersToCatalog',
					'assignCoursesToCatalog',
					'axProgressiveAssignUsers',
					'axProgressiveUnassignUsers',
				    'axProgressiveAssignCourses'
				),
			);

			$res[] = array(
				'allow',
				'roles' => array('/framework/admin/catalogManagement/mod'),
				'actions' => array(
					'assignCourses'
				),
			);

			$res[] = array(
				'deny',
				// deny all
				'users' => array('*'),
				'deniedCallback' => array(
					$this,
					'adminHomepageRedirect'
				),
			);

			return $res;
		}



		/**
		 * This is the default 'index' action that is invoked
		 * when an action is not explicitly requested by users.
		 */
	public function actionIndex()
	{
			Yii::app()->tinymce;
			$catalogModel = new LearningCatalogue();
			if (Yii::app()->user->getIsPu()) {
				$actions = CoreAdminCourse::canPUManageCatalogs();
				$canCreate = $actions['add'];
				$columns = array(
					'name',
					array(
						'type' => 'raw',
						'name' => 'description',
						'value' => 'Docebo::ellipsis($data->description, 200)',
					),
					array(
						'header' => '',
						'type' => 'raw',
						'value' => '((empty($data->catalog_members) && empty($data->catalog_entries))?\'<span class="exclamation"></span>\':\'\');',
						'htmlOptions' => array('class' => 'single-action'),
					)
				);

				if($actions['mod'] === true){

					$columns[] = array(
						'type' => 'raw',
						'value' => '$data->renderCatalogCourses();',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					);
					$columns[] = array(
						'type' => 'raw',
						'value' => '$data->renderCatalogUsers();',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					);
					$columns[] = array(
						'header' => '',
						'type' => 'raw',
						'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal edit-action",
						"data-toggle" => "modal",
						"data-modal-class" => "update-catalog",
						"data-modal-title" => "' . Yii::t('standard', '_MOD') . '",
						"data-buttons" => \'[
							{"type": "submit", "title": "' . Yii::t('standard', '_CONFIRM') . '"},
							{"type": "cancel", "title": "' . Yii::t('standard', '_CANCEL') . '"}
						]\',
						"data-url" => "CoursecatalogApp/CourseCatalogManagement/update&id=$data->idCatalogue",
						"data-after-submit" => "updateCourseCatalogContent",
						"data-before-submit" => "courseCatalogBeforeSubmit",
						"data-after-loading-content" => "courseCatalogLoadCallback();",
					));',
						'htmlOptions' => array('class' => 'button-column-single')
					);
				}else{
					$columns[] = array(
						'type' => 'raw',
						'value' => '$data->renderCatalogCourses(true);',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					);
					$columns[] = array(
						'type' => 'raw',
						'value' => '$data->renderCatalogUsers(true);',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					);
				}

				if($actions['del'] === true){
					$columns[] = array(
						'name' => '',
						'type' => 'raw',
						'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "CoursecatalogApp/CourseCatalogManagement/delete&id=$data->idCatalogue",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateCourseCatalogContent",
					));',
						'htmlOptions' => array('class' => 'button-column-single')
					);
				}

				$this->render('index', array(
					'catalogModel' => $catalogModel,
					'columns' => $columns,
					'canCreate' => $canCreate,
				));
				Yii::app()->end();
			}

				//current user is admin
				$columns = array(
					'name',
					array(
						'type' => 'raw',
						'name' => 'description',
						'value' =>  'Docebo::ellipsis($data->description, 200)',
					),
					array(
						'header' => '',
						'type' => 'raw',
						'value' => '((empty($data->catalog_members) && empty($data->catalog_entries))?\'<span class="exclamation"></span>\':\'\');',
						'htmlOptions' => array('class' => 'single-action'),
					),
					array(
						'type' => 'raw',
						'value' => '$data->renderCatalogCourses();',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					),
					array(
						'type' => 'raw',
						'value' => '$data->renderCatalogUsers();',
						'htmlOptions' => array('class' => 'power-users-members'),
						'headerHtmlOptions' => array('class' => 'center-aligned'),
					),
					array(
						'header' => '',
						'type' => 'raw',
						'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal edit-action",
						"data-toggle" => "modal",
						"data-modal-class" => "update-catalog",
						"data-modal-title" => "'.Yii::t('standard', '_MOD').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "CoursecatalogApp/CourseCatalogManagement/update&id=$data->idCatalogue",
						"data-after-submit" => "updateCourseCatalogContent",
						"data-before-submit" => "courseCatalogBeforeSubmit",
						"data-after-loading-content" => "courseCatalogLoadCallback();",
					));',
						'htmlOptions' => array('class' => 'button-column-single')
					),
					array(
						'name' => '',
						'type' => 'raw',
						'value' => 'CHtml::link("", "", array(
						"class" => "ajaxModal delete-action",
						"data-toggle" => "modal",
						"data-modal-class" => "delete-node",
						"data-modal-title" => "'.Yii::t('standard', '_DEL').'",
						"data-buttons" => \'[
							{"type": "submit", "title": "'.Yii::t('standard', '_CONFIRM').'"},
							{"type": "cancel", "title": "'.Yii::t('standard', '_CANCEL').'"}
						]\',
						"data-url" => "CoursecatalogApp/CourseCatalogManagement/delete&id=$data->idCatalogue",
						"data-after-loading-content" => "hideConfirmButton();",
						"data-after-submit" => "updateCourseCatalogContent",
					));',
						'htmlOptions' => array('class' => 'button-column-single')
					),
				);
				$this->render('index', array(
					'catalogModel' => $catalogModel,
					'columns' => $columns,
				));

		}



	public function actionCreate()
	{
			if (Yii::app()->request->isAjaxRequest) {
				$catalogModel = new LearningCatalogue();
				if (isset($_POST['LearningCatalogue'])) {
					$catalogModel->attributes = $_POST['LearningCatalogue'];
					if ($catalogModel->validate() && $catalogModel->save()) {
						$coreAdminCourse = new CoreAdminCourse();
						$coreAdminCourse->id_entry = $catalogModel->idCatalogue;
						$coreAdminCourse->type_of_entry = CoreAdminCourse::TYPE_CATALOG;
						$coreAdminCourse->idst_user = Yii::app()->user->id;
						$coreAdminCourse->save();
						$this->sendJSON(array());
					}
				}
				$content = $this->renderPartial('_form', array(
					'model' => $catalogModel,
				), true);
				$this->sendJSON(array('html' => $content));
			}
			Yii::app()->end();
		}



	public function actionDelete($id)
	{
			$catalogModel = LearningCatalogue::model()->findByAttributes(array('idCatalogue' => $id));
			if (empty($catalogModel)) {
				$this->sendJSON(array());
			}
			if (!empty($_POST['LearningCatalogue'])) {
				if ($_POST['LearningCatalogue']['confirm']) {
					$catalogModel->delete();
					Yii::app()->db->createCommand()
						->delete(CoreAdminCourse::model()->tableName(), 'id_entry = :id AND type_of_entry = :type', array(
							':id' => $id,
							':type' => CoreAdminCourse::TYPE_CATALOG,
						));
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('delete', array(
					'model' => $catalogModel,
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}



	public function actionUpdate($id)
	{
			if (Yii::app()->request->isAjaxRequest) {
				$catalogModel = LearningCatalogue::model()->findByAttributes(array('idCatalogue' => $id));
				if (empty($catalogModel)) {
					$this->sendJSON(array());
				}
				if (isset($_POST['LearningCatalogue'])) {
					$catalogModel->attributes = $_POST['LearningCatalogue'];
					if ($catalogModel->validate() && $catalogModel->save()) {
						$this->sendJSON(array());
					}
				}
				$content = $this->renderPartial('_form', array(
					'model' => $catalogModel,
				), true);
				$this->sendJSON(array('html' => $content));
			}
			Yii::app()->end();
		}



		/**
		 * Get the REQUEST from UsersSelector dialog and assign selected enitites (users, groups, org charts) to a given Catalog
		 *
		 */
	public function actionAssignUsersToCatalog()
	{

			$idCatalog = Yii::app()->request->getParam('idCatalog', false);
			$usersList = array();
			$usersList = UsersSelector::getUsersList($_REQUEST);

//			if (!empty($usersList)) {

				try {
					// Get and save selections
					$groupsList = array();
					$orgChartList = array();
					$orgChartDescList = array();

					// Users
					if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_USERS])) {
						$usersList = CJSON::decode($_REQUEST[UsersSelector::FIELD_SELECTED_USERS]);
					}

					// Groups
					if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_GROUPS])) {
						$groupsList = CJSON::decode($_REQUEST[UsersSelector::FIELD_SELECTED_GROUPS]);
					}

					// Orgcharts
					list($orgChartList, $orgChartDescList) = UsersSelector::getSelectedOrgchartsInfo($_REQUEST, true);


					// Final selection to save
					$selection = array_merge($usersList, $groupsList, $orgChartList, $orgChartDescList);

					if (!empty($selection)) {
						$this->renderPartial('progressive_assign_users', array('id_catalog' => $idCatalog, 'ids' => implode(',', $selection)));
					}else{
						echo '<a href="#" class="auto-close"></a>';
						Yii::app()->end();
					}


			} catch (CException $e) {
					Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
					Yii::app()->user->setFlash('error', $e->getMessage());
					$selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
							'type' 	=> UsersSelector::TYPE_CATALOG_MANAGEMENT,
							'idCatalog'	=> $idCatalog,
					));
					$this->redirect($selectorDialogUrl, true);
				}

//			}


			Yii::app()->end();

		}


	public function actionAssignCoursesToCatalog()
	{

	    $idCatalog = Yii::app()->request->getParam('idCatalog', false);
	    $coursesList   = array();
	    $plansList     = array();

	    $plansList     = UsersSelector::getPlansListOnly($_REQUEST);

	    try {

	        // Courses
	        if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_COURSES])) {
	           $coursesList   = UsersSelector::getCoursesListOnly($_REQUEST);
	        }

	        // Learning Plans
	        if (isset($_REQUEST[UsersSelector::FIELD_SELECTED_PLANS])) {
	            $plansList     = UsersSelector::getPlansListOnly($_REQUEST);
	        }

	        if (empty($coursesList) && empty($plansList)) {
	            throw new CException(Yii::t("standard", "_EMPTY_SELECTION"));
	        }

	        // Final selection to save
	        $selection = array(
	            'courses'  => $coursesList,
	            'plans'    => $plansList,
	        );
	        if (!empty($coursesList) || !empty($plansList)) {
	            $this->renderPartial('progressive_assign_courses', array('id_catalog' => $idCatalog, 'selection' => $selection));
	            Yii::app()->end();
	        }



	    } catch (CException $e) {
	        Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
	        Yii::app()->user->setFlash('error', $e->getMessage());
	        $selectorDialogUrl = Docebo::createLmsUrl('usersSelector/axOpen', array(
	            'type' 	=> UsersSelector::TYPE_CATALOG_MANAGEMENT_COURSES,
	            'idCatalog'	=> $idCatalog,
	        ));
	        $this->redirect($selectorDialogUrl, true);
	    }

	    Yii::app()->end();

	}



	public function actionAssignUsers($id)
	{
        // FancyTree initialization
        Yii::app()->fancytree->init();

			$catalogModel = LearningCatalogue::model()->findByAttributes(array('idCatalogue' => $id));
			Yii::app()->session['currentCatalogId'] = $id;
		$assignedUsers = new LearningCatalogueMember('search');

			$assignedUsers->idCatalogue = $id;

			if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['LearningCatalogueMember'])) {
				$userid = Yii::app()->user->getAbsoluteUsername($_REQUEST['LearningCatalogueMember']['search_input']);
			$assignedUsers->search_input = $_REQUEST['LearningCatalogueMember']['search_input'];
				$findUser = CoreUser::model()->findByAttributes(array('userid'=>$userid));
				if (!empty($findUser)) {
					$assignedUsers->idst_member = $findUser->idst;
				}
			}


			UsersSelector::registerClientScripts();

			$this->render('assignUsers', array(
				'catalogModel' => $catalogModel,
				'assignedUsers' => $assignedUsers,
			));
		}



	public function actionAssignSelectUsers($id)
	{
			$catalogModel = LearningCatalogue::model()->findByAttributes(array('idCatalogue' => $id));
			if (empty($catalogModel)) {
				$this->sendJSON(array());
			}

			$user           = CoreUser::model();
			$user->scenario = 'search';
			$user->unsetAttributes();
			if (isset($_REQUEST['CoreUser'])) {
				$user->attributes = $_REQUEST['CoreUser'];
			}

			$group = new CoreGroup();
			if (isset($_REQUEST['CoreGroup'])) {
				$group->attributes = $_REQUEST['CoreGroup'];
			}

			// We should assign the raw selection, not the resolution at users level
			$usersList = $_POST['select-user-grid-selected-items'] ? explode(',', $_POST['select-user-grid-selected-items']) : array();
			$groupsList = $_POST['select-group-grid-selected-items'] ? explode(',', $_POST['select-group-grid-selected-items']) : array();
			$orgChartList = array();
			$orgChartDescList = array();
			if (isset($_POST['select-orgchart']) && !empty($_POST['select-orgchart'])) {
				foreach ($_POST['select-orgchart'] as $idOrg => $selected) {
					switch ((int)$selected) {
						case 0: //not selected
							break;
						case 1: //selected
							$ar = CoreOrgChartTree::model()->findByPk($idOrg);
						if ($ar) {
							$orgChartList[] = $ar->idst_oc;
						}
							break;
						case 2: //selected + descendants
							$ar = CoreOrgChartTree::model()->findByPk($idOrg);
						if ($ar) {
							$orgChartDescList[] = $ar->idst_ocd;
						}
							break;
					}
				}
			}

			$selection = array_merge($usersList, $groupsList, $orgChartList, $orgChartDescList);

			if (!empty($selection)) {
				$existsCatalogueMembers = LearningCatalogueMember::model()->findAllByAttributes(array('idCatalogue' => $id, 'idst_member' => $selection));
				if (!empty($existsCatalogueMembers)) {
					$existsCatalogueMembersList = CHtml::listData($existsCatalogueMembers, 'idst_member', 'idst_member');
					$new_selected = array_diff($selection, $existsCatalogueMembersList);
				} else {
					$new_selected = $selection;
				}
				if (!empty($new_selected)) {
					foreach ($new_selected as $newCatalogueMemberId) {
						$newCatalogueMember = new LearningCatalogueMember();
						$newCatalogueMember->idCatalogue = $id;
						$newCatalogueMember->idst_member = (int)$newCatalogueMemberId;
						$newCatalogueMember->save();
					}
				}
				$this->sendJSON(array('status' => 'saved'));
			}

			if (Yii::app()->request->isAjaxRequest) {

				$user_group_selected = array();
				$raw_user_group_selected = LearningCatalogueMember::model()->getCatalogMembers($id, array('user', 'group'));
				foreach($raw_user_group_selected as $user_group) {
					$user_group_selected[] = $user_group['idst'];
				}
				Yii::app()->session['selectedItems'] = $user_group_selected;

				$catalogOrgChartsMembers = LearningCatalogueMember::model()->getCatalogMembers($id, array('orgchart', 'orgchart_desc'));
				$selectedOrgCharts = array();
				foreach ($catalogOrgChartsMembers as $info) {

					$selectedOrgCharts[$info['id']] = ( $info['type'] =='orgchart' ? 1 : 2 );
				}
				Yii::app()->session['selectedOrgChartItems'] = $selectedOrgCharts;

				// Build fancytree specific PRE-selected array
				$preSelected = array();
				foreach ($selectedOrgCharts as $key => $selectState) {
					$preSelected[] = array("key" => $key, "selectState" => $selectState);
				}

				$fancyTreeData = CoreOrgChartTree::buildFancytreeDataArray();
				$content = $this->renderPartial('//common/_usersSelector', array(
					'userModel' => $user,
					'groupModel' => $group,
					'isNeedRegisterYiiGridJs' => true,
					'fancyTreeData' => $fancyTreeData,
					'preSelected' => $preSelected,
				), true, true);

				if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
					echo $content;
				} else {
					$this->sendJSON(array('html' => $content));
				}
			}
			Yii::app()->end();
		}



	public function actionDeleteAssignedUser($id = null)
	{

			//elaborate and validate input data
			//NOTE: both single user and multiple users cases are handled by this function
			$ids = array();
			if (!empty($id) && is_numeric($id) && (int)$id > 0) {
				//NOTE: $id is normally used for single user case
				$ids[] = (int)$id;
			}
			if (!empty($_POST['ids'])) {
				//NOTE: $ids is normally used for multiple users case
				$tmpArr = explode(',', $_POST['ids']);
				if (is_array($tmpArr)) {
					foreach ($tmpArr as $tmpId) {
						if (is_numeric($tmpId) && (int)$tmpId > 0) {
							$ids[] = (int)$tmpId;
						}
					}
				}
			}

			//check if we have some users to process
			$ids = array_unique($ids);
			if (empty($ids)) {
				//nothing else to do here
				$this->sendJSON(array());
			}

			$idCatalog = Yii::app()->request->getParam('id_catalog', false);
			$currentCatalogId = (!empty($idCatalog) ? $idCatalog : Yii::app()->session['currentCatalogId']);
			$catalogueMembers = LearningCatalogueMember::model()->findAllByAttributes(array('idCatalogue' => $currentCatalogId, 'idst_member' => $ids));

			if (empty($catalogueMembers)) {
				$this->sendJSON(array());
			}
			if (!empty($_POST['LearningCatalogueMember'])) {
				if ($_POST['LearningCatalogueMember']['confirm']) {

				    // Raise event for evey catalog member being unassigned
				    // We can't do this in afterDelete, because deletion below is a non-AR operatipon (deleteAllByAttributes)
				    foreach ($catalogueMembers as $memberModel) {
				        Yii::app()->event->raise(EventManager::EVENT_MEMBER_UNASSIGNED_FROM_CATALOG, new DEvent($this, array(
				            'model' => $memberModel,
				        )));
				    }

					LearningCatalogueMember::model()->deleteAllByAttributes(array('idCatalogue' => $currentCatalogId, 'idst_member' => $ids));
					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('deleteUser', array(
					'model' => new LearningCatalogueMember(),
					'users' => $catalogueMembers,
					'idCatalog' => $currentCatalogId
				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}

	public function actionAssignCourses($id)
	{
			$catalogModel = LearningCatalogue::model()->findByAttributes(array('idCatalogue' => $id));
			Yii::app()->session['currentCatalogId'] = $id;
			$assignedCourses = new LearningCatalogueEntry();
			$assignedCourses->idCatalogue = $id;
			if (Yii::app()->request->isAjaxRequest && isset($_REQUEST['LearningCourse']['name'])) {
				$assignedCourses->search_input = $_REQUEST['LearningCourse']['name'];
			}

			UsersSelector::registerClientScripts();


			$this->render('assignCourses', array(
				'catalogModel' => $catalogModel,
				'assignedCourses' => $assignedCourses,
			));
	}



	public function actionAssignSelectCourses($id)
	{
			if (Yii::app()->request->isAjaxRequest) {

				$selectedItems = Yii::app()->request->getParam('catalog-course-grid-selected-items-list', array());
				$selectedPathItems = Yii::app()->request->getParam('coursepath-grid-selected-items-list', array());
				$selectedItems = (is_string($selectedItems) && !empty($selectedItems))? explode(',', $selectedItems) : array();
				$selectedPathItems = (is_string($selectedPathItems) && !empty($selectedPathItems))? explode(',', $selectedPathItems) : array();

				$isSubmit = Yii::app()->request->getParam('isSubmit') ? true : false;
				$result = array();

				$courseModel = new LearningCourse();
				$courseModel->unsetAttributes();

				if (isset($_REQUEST['LearningCourse'])) {
					$courseModel->attributes = $_REQUEST['LearningCourse'];
				}
				$saved = false;

				// Learning Plans
				if (PluginManager::isPluginActive('CurriculaApp')) {
					$coursepathModel = new LearningCoursepath();
					$coursepathModel->unsetAttributes();
					if (isset($_REQUEST['LearningCoursepath'])) {
						$coursepathModel->attributes = $_REQUEST['LearningCoursepath'];
					}

					if ($isSubmit && !empty($selectedPathItems)) {
						$learningPlansTransaction = Yii::app()->db->beginTransaction();
						try {
							foreach ($selectedPathItems as $coursepathId) {
								$exists = Yii::app()->db->createCommand()
									->select('COUNT(*)')
									->from(LearningCatalogueEntry::model()->tableName())
									->where('idCatalogue=:idCatalogue AND idEntry=:idEntry AND type_of_entry=:type_of_entry', array(
										':idCatalogue' => $id,
										':idEntry' => $coursepathId,
										':type_of_entry' => 'coursepath'))
									->queryScalar();
								if(!$exists) {
									Yii::app()->db->createCommand()
										->insert(
											LearningCatalogueEntry::model()->tableName(),
											array(
												'idCatalogue' => $id,
												'idEntry' => $coursepathId,
												'type_of_entry' => 'coursepath'
											)
										);
								}
							}
							$saved = true;
							$learningPlansTransaction->commit();
						} catch (CDbException $e) {
							$learningPlansTransaction->rollback();
						}
					}
				}else{
					$coursepathModel = NULL;
				}

				// Courses
				if ($isSubmit && !empty($selectedItems)) {
					$coursesTransaction = Yii::app()->db->beginTransaction();
					try {
						foreach ($selectedItems as $courseId) {
							$exists = Yii::app()->db->createCommand()
								->select('COUNT(*)')
								->from(LearningCatalogueEntry::model()->tableName())
								->where('idCatalogue=:idCatalogue AND idEntry=:idEntry AND type_of_entry=:type_of_entry', array(
									':idCatalogue' => $id,
									':idEntry' => $courseId,
									':type_of_entry' => 'course'))
								->queryScalar();
							if(!$exists) {
								Yii::app()->db->createCommand()
									->insert(
										LearningCatalogueEntry::model()->tableName(),
										array(
											'idCatalogue' => $id,
											'idEntry' => $courseId,
											'type_of_entry' => 'course'
										)
									);
							}
						}
						$saved = true;
						$coursesTransaction->commit();
					} catch (CDbException $e) {
						$coursesTransaction->rollback();
					}
				}

				if($saved) {
					$result['status'] = 'saved';
				}
				if($isSubmit){
					$result['html'] = '';
					if ($saved) {
					   Yii::app()->event->raise(EventManager::EVENT_ENTRIES_ASSIGNED_TO_CATALOG, new DEvent($this, array(
					       'idCatalogue'   => $id,
					       'courses'       => $selectedItems,
					       'plans'         => $selectedPathItems,
					   )));
					}
					$this->sendJSON($result);
				}

				// Exclude courses that are already assigned to this PU
				$excludedCourses = Yii::app()->getDb()->createCommand()
					->select('t.idEntry')
					->from(LearningCatalogueEntry::model()->tableName().' t')
					->where('t.type_of_entry="course"')
					->andWhere('t.idCatalogue=:idCatalogue', array(':idCatalogue'=>$id))
					->queryColumn();
				$excludedPaths = Yii::app()->getDb()->createCommand()
					->select('t.idEntry')
					->from(LearningCatalogueEntry::model()->tableName().' t')
					->where('t.type_of_entry="coursepath"')
					->andWhere('t.idCatalogue=:idCatalogue', array(':idCatalogue'=>$id))
					->queryColumn();

				if (PluginManager::isPluginActive('CurriculaApp')) {
					if (Yii::app()->user->getIsPu()) {
						$assignedCourses = CoreUserPuCourse::model()->getList(Yii::app()->user->id);
						$selectedCourses = Yii::app()->db->createCommand()
							->select('idEntry')
							->from(LearningCatalogueEntry::model()->tableName())
							->where('idCatalogue = :id AND type_of_entry = :type', array(':id' => $id, ':type' => CoreAdminCourse::TYPE_COURSE))->queryColumn();
						$excludedCourses = Yii::app()->getDb()->createCommand()
							->select('t.idCourse')
							->from(LearningCourse::model()->tableName().' t')->queryColumn();
						$assignedCourses = array_diff($assignedCourses, $selectedCourses);
						$excludedCourses = array_diff($excludedCourses, $assignedCourses);
						$excludedCourses = array_values($excludedCourses);

						$assignedPaths = CoreUserPuCoursepath::model()->getList(Yii::app()->user->id);
						$selectedPaths = Yii::app()->db->createCommand()
							->select('idEntry')
							->from(LearningCatalogueEntry::model()->tableName())
							->where('idCatalogue = :id AND type_of_entry = :type', array(':id' => $id, ':type'=>CoreAdminCourse::TYPE_COURSEPATH))->queryColumn();
						$excludedPaths = Yii::app()->db->createCommand()
							->select('id_path')
							->from(LearningCoursepath::model()->tableName())->queryColumn();
						$assignedPaths = array_diff($assignedPaths, $selectedPaths);
						$excludedPaths = array_diff($excludedPaths, $assignedPaths);
						$excludedPaths = array_values($excludedPaths);
					}
					$content = $this->renderPartial('assignCoursesWithCoursepaths', array(
						'courseModel' => $courseModel,
						'coursepathModel' => $coursepathModel,
						'excludedCourses' => $excludedCourses,
						'excludedPaths' => $excludedPaths,
					), true, true);
				} else {
					$content = $this->renderPartial('assignSelectCourses', array(
						'model' => $courseModel,
						'courseModel' => $courseModel, //patch
						'excludedCourses' => $excludedCourses,
					), true, true);
				}

				if (isset($_REQUEST['contentType']) && ($_REQUEST['contentType'] == 'html')) {
					echo $content;
				} else {
					$this->sendJSON(array('html' => $content));
				}
			}
		}

	public function actionDeleteAssignedCourse($id = null)
	{
			if (!empty($_POST['ids'])) {
				$id = $_POST['ids'];
			} else {
				$id = array($id);
			}

			$currentCatalogId = Yii::app()->session['currentCatalogId'];
			$idCourses = array();
			$idCoursepaths = array();

			if (PluginManager::isPluginActive('CurriculaApp')) {

				foreach ($id as $item) {
					$parts = explode('_', $item);
					$idItem = $parts[0];
					array_shift($parts);
					$itemType = implode('_', $parts);
					switch ($itemType) {
						case CoreAdminCourse::TYPE_COURSE: { $idCourses[] = $idItem; } break;
						case CoreAdminCourse::TYPE_COURSEPATH: { $idCoursepaths[] = $idItem; } break;
					}
				}

				$courses = (!empty($idCourses) ? LearningCourse::model()->findAllByAttributes(array('idCourse' => $idCourses)) : array());
				$coursepaths = (!empty($idCoursepaths) ? LearningCoursepath::model()->findAllByAttributes(array('id_path' => $idCoursepaths)) : array());
				if (empty($courses) && empty($coursepaths)) {
					$this->sendJSON(array());
				}

			} else {

				foreach ($id as $item) {
					$idCourses[] = (int)$item;
				}

				$courses = (!empty($idCourses) ? LearningCourse::model()->findAllByAttributes(array('idCourse' => $idCourses)) : array());
				if (empty($courses)) {
					$this->sendJSON(array());
				}

			}

			if (!empty($_POST['LearningCatalogueEntry'])) {
				if ($_POST['LearningCatalogueEntry']['confirm']) {
					if (!empty($idCourses)) {
						LearningCatalogueEntry::model()->deleteAllByAttributes(array('idCatalogue' => $currentCatalogId, 'idEntry' => $idCourses, 'type_of_entry' => CoreAdminCourse::TYPE_COURSE));
					}
					if (!empty($idCoursepaths)) {
						LearningCatalogueEntry::model()->deleteAllByAttributes(array('idCatalogue' => $currentCatalogId, 'idEntry' => $idCoursepaths, 'type_of_entry' => CoreAdminCourse::TYPE_COURSEPATH));
					}

					Yii::app()->event->raise(EventManager::EVENT_ENTRIES_UNASSIGNED_FROM_CATALOG, new DEvent($this, array(
					    'idCatalogue'   => $currentCatalogId,
					    'courses'       => $idCourses,
					    'plans'         => $idCoursepaths,
					)));

					$this->sendJSON(array());
				}
			}
			if (Yii::app()->request->isAjaxRequest) {
				$content = $this->renderPartial('deleteCourse', array(
					'model' => new LearningCatalogueEntry(),
					'totalCount' => (count($idCourses) + count($idCoursepaths)),
					'courses' => $courses,
					'coursepaths' => $coursepaths,

				), true);
				$this->sendJSON(array(
					'html' => $content,
				));
			}
			Yii::app()->end();
		}




	protected function _generateCacheKey($prefix = '') {
		return uniqid($prefix).'_'.time();
}

	protected function _setCachedUsersToBeUnassigned(array $users) {

		$content = implode(',', $users);

		//generate a key (it will be used as file name for cached data)
		$cacheKey = $this->_generateCacheKey('_catalog_users_to_be_unassigned_');

		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		$file = fopen($pathName . DIRECTORY_SEPARATOR . $fileName, 'w');
		if (!$file) {
			//TODO: handle error
		}
		fwrite($file, $content);
		fclose($file);

		return $cacheKey;
	}


	protected function _unsetCachedUsersToBeUnassigned($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		unlink($pathName . DIRECTORY_SEPARATOR . $fileName);
	}

	protected function _getCachedUsersToBeUnassigned($cacheKey) {
		$pathName = Docebo::getUploadTmpPath();
		$fileName = $cacheKey.'.tmp';
		if (!file_exists($pathName . DIRECTORY_SEPARATOR . $fileName)) { throw new CException('Cache file not found [key: '.$cacheKey.']'); }
		$content = file_get_contents($pathName . DIRECTORY_SEPARATOR . $fileName);
		return explode(',', $content);
	}

	public function actionAxProgressiveAssignUsers()
	{
		//catalog ID must always be specified
		$idCatalog = Yii::app()->request->getParam('id_catalog', 0);
		if ($idCatalog <= 0) {
			$result['success'] = false;
			$result['message'] = 'Invalid specified catalog';
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		//read input and prepare cached data
		if (isset($_POST['ids']) && !isset($_POST['_key_'])) {

			//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'ids' input parameter is passed the first time only.

			//Form submission from assigning dialog: read input data and create local file cache
			$usersStr = trim(Yii::app()->request->getParam('ids', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);

			$cacheKey = $this->_setCachedUsersToBeUnassigned($users);

		} else {

			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}

		//read cached data
		try {
			$res = $this->_getCachedUsersToBeUnassigned($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}
		if (empty($res) || !is_array($res)) { $cacheError = true; } //make sure that the var type is right
		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res);

		//define chunk size, depending un users count (smaller chunks will make big operations too slow, and viceversa)
		if ($totalCount < 100) {
			$chunkSize = 10;
		} elseif ($totalCount < 500) {
			$chunkSize = 20;
		} elseif ($totalCount < 1000) {
			$chunkSize = 30;
		} elseif ($totalCount < 3000) {
			$chunkSize = 40;
		} else {
			$chunkSize = 50;
		}

		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
			'usersAssigned' => 0,
			'usersFailed' => 0,
		);

		//main loop
		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res[$index]) ? $res[$index] : -1);
			if ($user < 0) continue;

			//main operations
			try {
				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				if (!LearningCatalogueMember::model()->exists('idCatalogue=:idCat AND idst_member=:idMember', array(':idCat' => $idCatalog, ':idMember' => $user))) {
				    $newCatalogueMember = new LearningCatalogueMember();
				    $newCatalogueMember->idCatalogue = $idCatalog;
				    $newCatalogueMember->idst_member = (int)$user;
				    $newCatalogueMember->save();
				}

				$result['usersAssigned']++;

			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$result['usersFailed']++;
			}

		}

		//render dialog
		if ($index > $totalCount) { $index = $totalCount; } //normalize value
		$completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));
		$html = $this->renderPartial('assign_progressive_dialog', array(
			'offset' => $offset,
			'assigned' => $result['usersAssigned'],
			'failed' => $result['usersFailed'],
			'processedUsers' => $index,
			'totalUsers' => $totalCount,
			'totalBranches' => count($res['groups']),
			'completedPercent' => $completedPercent,
			'start' => ($offset == 0),
			'stop' => $stop,
			'cacheKey' => $cacheKey
		), true, true);

		//we are finished here, we do not need cache file anymore
		if ($stop) {
			$this->_unsetCachedUsersToBeUnassigned($cacheKey);
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			//specific parameters for this function
			'_key_'		=> $cacheKey
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();
	}

	public function actionAxProgressiveUnassignUsers()
	{

		//catalog ID must always be specified
		$idCatalog = Yii::app()->request->getParam('id_catalog', 0);
		if ($idCatalog <= 0) {
			$result['success'] = false;
			$result['message'] = 'Invalid specified catalog';
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		//read input and prepare cached data
		if (isset($_POST['ids']) && !isset($_POST['_key_'])) {

			//NOTE: if we enter this branch of the "if", then we are starting the progressive operation, since 'ids' input parameter is passed the first time only.

			//Form submission from assigning dialog: read input data and create local file cache
			$usersStr = trim(Yii::app()->request->getParam('ids', ''));
			if (empty($usersStr)) {
				$result['success'] = false;
				$result['message'] = 'No selected users';
				$result['status'] = 'error';
				$result['stop'] = true;
				$this->sendJSON($result);
				Yii::app()->end();
			}
			$users = explode(",", $usersStr);

			$cacheKey = $this->_setCachedUsersToBeUnassigned($users);

		} else {

			// Get the users array from the cache
			$cacheKey = Yii::app()->request->getParam('_key_', 0);
		}

		//read cached data
		try {
			$res = $this->_getCachedUsersToBeUnassigned($cacheKey);
		} catch (Exception $e) {
			$cacheError = true;
		}
		if (empty($res) || !is_array($res)) { $cacheError = true; } //make sure that the var type is right
		if ($cacheError) {
			//something went wrong while reading cached data ... handle the error
			$result['success'] = false;
			$result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
			$result['status'] = 'error';
			$result['stop'] = true;
			$this->sendJSON($result);
			Yii::app()->end();
		}

		// Get current offset and calculate stop flag
		$offset = Yii::app()->request->getParam('offset', 0);
		$totalCount = count($res);

		//define chunk size, depending un users count (smaller chunks will make big operations too slow, and viceversa)
		if ($totalCount < 100) {
			$chunkSize = 10;
		} elseif ($totalCount < 500) {
			$chunkSize = 20;
		} elseif ($totalCount < 1000) {
			$chunkSize = 30;
		} elseif ($totalCount < 3000) {
			$chunkSize = 40;
		} else {
			$chunkSize = 50;
		}

		$newOffset = $offset + $chunkSize;
		$stop = ($newOffset < $totalCount) ? false : true;

		// Preparing result for importer
		$result = array (
			'usersUnassigned' => 0,
			'usersFailed' => 0,
		);

		//main loop
		$toBeUnselected = array();
		$counter = 0;
		for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) { //NOTE: $newOffset may be greater than $totalCount, but we wouldn't count over $totalCount

			//security check
			$counter++;
			if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

			//read user ID, if we are over user list limit, then invalidate the value and stop this iteration
			$user = (isset($res[$index]) ? $res[$index] : -1);
			if ($user < 0) continue;

			//main operations
			try {

				//validate input user
				if (!$user) { throw new CException('Invalid user (index: '.$index.')'); }

				$attributes = array('idCatalogue' => $idCatalog, 'idst_member' => $user);
				LearningCatalogueMember::model()->deleteAllByAttributes($attributes);

				// Raise an event for the member being deleted from the catalog
				if ($tmpModel = LearningCatalogueMember::model()->findByAttributes($attributes)) {
				    Yii::app()->event->raise(EventManager::EVENT_MEMBER_UNASSIGNED_FROM_CATALOG, new DEvent($this, array(
				        'model' => $tmpModel,
				    )));
				}

				$result['usersUnassigned']++;
				$toBeUnselected[] = (int)$user;

			} catch(Exception $e) {
				Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
				$result['usersFailed']++;
			}

		}

		//render dialog
		if ($index > $totalCount) { $index = $totalCount; } //normalize value
		$completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));
		$html = $this->renderPartial('unassign_progressive_dialog', array(
			'offset' => $offset,
			'assigned' => $result['usersUnassigned'],
			'failed' => $result['usersFailed'],
			'processedUsers' => $index,
			'totalUsers' => $totalCount,
			'totalBranches' => count($res['groups']),
			'completedPercent' => $completedPercent,
			'start' => ($offset == 0),
			'stop' => $stop,
			'toBeUnselected' => $toBeUnselected,
			'cacheKey' => $cacheKey,
            'afterUpdateCallback'=>($stop ? '$.fn.updateCheckboxesAfterChangeStatus();' : null)
		), true, true);

		//we are finished here, we do not need cache file anymore
		if ($stop) {
			$this->_unsetCachedUsersToBeUnassigned($cacheKey);
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			//specific parameters for this function
			'_key_'		=> $cacheKey
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();

	}


	/**
	 * @TODO
	 */
	public function actionAxProgressiveAssignCourses()
	{
	    $idCatalog         = Yii::app()->request->getParam('id_catalog', 0);
	    $start             = Yii::app()->request->getParam('start', false);
	    $cacheKey          = Yii::app()->request->getParam('cache_key', false);
	    $coursesList       = Yii::app()->request->getParam('course_ids', false);
	    $plansList         = Yii::app()->request->getParam('plan_ids', false);
	    $offset            = Yii::app()->request->getParam('offset', 0);

	    if ($idCatalog <= 0) {
	        $result['success'] = false;
	        $result['message'] = Yii::t('standard', 'Invalid data');
	        $result['status'] = 'error';
	        $result['stop'] = true;
	        $this->sendJSON($result);
	        Yii::app()->end();
	    }

	    // Read input and prepare cached data
	    if ($start || !$cacheKey) {

	        if (empty($coursesList) && empty($plansList)) {
	            $result['success'] = false;
	            $result['message'] = Yii::t("standard", "_EMPTY_SELECTION");
	            $result['status'] = 'error';
	            $result['stop'] = true;
	            $this->sendJSON($result);
	            Yii::app()->end();
	        }


	        // Cache courses & plans selection, so that we do not pass it over every
	        $cacheKey = Docebo::randomHash();
	        Yii::app()->cache->set($cacheKey, array("courses" => $coursesList, "plans" => $plansList));
	    }

	    // Try to get selection from cache
	    // Stop the process with error if something goes wrong with the cache
	    $selectionLists =  Yii::app()->cache->get($cacheKey);
        if (!$selectionLists) {
            $result['success'] = false;
            $result['message'] = Yii::t('standard', '_OPERATION_FAILURE');
            $result['status'] = 'error';
            $result['stop'] = true;
            $this->sendJSON($result);
            Yii::app()->end();
        }

        // We are dealing with 2 types of data, in 2 arrays
        // lets make ONE single associative, structured array out of them: selection
        $courses = !empty($selectionLists['courses']) ? explode(",", $selectionLists['courses']) : array();
        $plans   = !empty($selectionLists['plans']) ? explode(",", $selectionLists['plans']) : array();
        $selection = array();
        foreach ($courses as $id) {
            $selection[] = array("t" => "c", "v" => $id);
        }
        foreach ($plans as $id) {
            $selection[] = array("t" => "p", "v" => $id);
        }

        // Get current offset and calculate stop flag
        $totalCount = count($selection);

        // Define chunk size, depending on items count
        if ($totalCount < 100) {
            $chunkSize = 10;
        } elseif ($totalCount < 500) {
            $chunkSize = 20;
        } elseif ($totalCount < 1000) {
            $chunkSize = 30;
        } elseif ($totalCount < 3000) {
            $chunkSize = 40;
        } else {
            $chunkSize = 50;
        }


        $newOffset = $offset + $chunkSize;
        $stop = ($newOffset < $totalCount) ? false : true;

        // Preparing result for importer
        $progressStats = array (
            'coursesAssigned'   => 0,
            'coursesFailed'     => 0,
            'plansAssigned'     => 0,
            'plansFailed'       => 0,
        );


        // Main loop
        $counter = 0;
        $learningPlanPluginActive = PluginManager::isPluginActive("CurriculaApp");
        for ($index = $offset; ($index < $newOffset && $index < $totalCount); $index++) {
            $counter++;

            // Precaution check
            if ($counter > $chunkSize) break; //NOTE: this shouldn't happen

            $item = (isset($selection[$index]) ? $selection[$index] : -1);
            if ($item < 0) continue;

            // Now recognize the type of this item and handle it accordingly (assign to catalog, that is)
            $command = Yii::app()->db->createCommand();

            // Assign course
            if ($item["t"] == "c") {
                $idCourse = $item["v"];
                $exists = LearningCatalogueEntry::model()->exists("idCatalogue=:idCatalog AND idEntry=:id AND type_of_entry=:toe", array(
                    ":idCatalog"    => $idCatalog,
                    ":id"           => $idCourse,
                    ":toe"          => LearningCatalogueEntry::ENTRY_COURSE,
                ));
                if(!$exists) {
                    $command->insert(LearningCatalogueEntry::model()->tableName(), array(
                        'idCatalogue'   => $idCatalog,
                        'idEntry'       => $idCourse,
                        'type_of_entry' => LearningCatalogueEntry::ENTRY_COURSE
                    ));
                }
            }
            // Assign learning plan
            else if ($learningPlanPluginActive && ($item["t"] == "p")) {
                $idPlan = $item["v"];
                $exists = LearningCatalogueEntry::model()->exists("idCatalogue=:idCatalog AND idEntry=:id AND type_of_entry=:toe", array(
                    ":idCatalog"    => $idCatalog,
                    ":id"           => $idPlan,
                    ":toe"          => LearningCatalogueEntry::ENTRY_COURSEPATH,
                ));
                if(!$exists) {
                    $command->insert(LearningCatalogueEntry::model()->tableName(), array(
                        'idCatalogue'   => $idCatalog,
                        'idEntry'       => $idPlan,
                        'type_of_entry' => LearningCatalogueEntry::ENTRY_COURSEPATH
                    ));
                }
            }

        }

        if ($index > $totalCount) { $index = $totalCount; } //normalize value

		if ($stop) {
			//raise catalog related event for courses assignemnt
			$_selectedCourses = array();
			$_selectedPlans = array();
			foreach ($selection as $_selectedItem) {
				switch ($_selectedItem['t']) {
					case 'c': $_selectedCourses[] = (int)$_selectedItem['v']; break;
					case 'p': $_selectedPlans[] = (int)$_selectedItem['v']; break;
				}
			}
			if (!empty($_selectedCourses) || !empty($_selectedPlans)) {
				Yii::app()->event->raise(EventManager::EVENT_ENTRIES_ASSIGNED_TO_CATALOG, new DEvent($this, array(
					'idCatalogue' => $idCatalog,
					'courses' => $_selectedCourses,
					'plans' => $_selectedPlans,
				)));
			}
		}

        // Render progress-dialog
        $completedPercent = ($totalCount == 0 ? 0 : (number_format($index/$totalCount * 100, 0)));
        $progressStats['processedItems']    = $index;
        $progressStats['totalItems']        = $totalCount;
        $progressStats['completedPercent']  = $completedPercent;

        $html = $this->renderPartial('assign_progressive_dialog_courses', array(
            'offset' => $offset,
            'progressStats' => $progressStats,
            'start' => ($offset == 0),
            'stop' => $stop,
            'cacheKey' => $cacheKey
        ), true, true);


		//we are finished here, we do not need cache file anymore
		if ($stop) {
		    Yii::app()->cache->delete($cacheKey);
		}

		// Send back JSON, handled by ProgressController's JS (see lms/protected/views/progress/index.php)
		// These will be used by ProgressController JS and sent back to us on next AJAX-call, IF ANY (stopping when $stop == true)
		$data = array(
			'offset'	=> $newOffset,
			'stop'		=> $stop,
			'cache_key'	=> $cacheKey
		);
		$response = new AjaxResult();
		$response->setStatus(true);
		$response->setHtml($html)->setData($data)->toJSON();
		Yii::app()->end();

	}


}
