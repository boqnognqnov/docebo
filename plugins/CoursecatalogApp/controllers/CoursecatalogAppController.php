<?php

class CoursecatalogAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {
		// Add the admin.css in order to be displayed correctly
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl.'/css/admin.css');

		$settings = new CoursecatalogAppSettingsForm();

		if (isset($_POST['CoursecatalogAppSettingsForm'])) {
			$settings->setAttributes($_POST['CoursecatalogAppSettingsForm']);

			if (!empty($_REQUEST['selected_catalogs'])) {
				$settings->catalog_external_selected_catalogs = Yii::app()->htmlpurifier->purify(Yii::app()->request->getParam('selected_catalogs', ''));
			}

			if ($settings->validate()) {

				Settings::save('catalog_external', $settings->catalog_external, 'enum', 3, '0', 5, 4);
				Settings::save('first_catalogue', $settings->first_catalogue, 'enum', 3, '0', 4, 2);
				Settings::save('catalog_type', $settings->catalog_type);
				Settings::save('on_catalogue_empty', $settings->on_catalogue_empty, 'enum', 3);
				Settings::save('catalog_use_categories_tree', $settings->catalog_use_categories_tree, 'enum', 3);
				Settings::save('show_course_details_dedicated_page', $settings->show_course_details_dedicated_page, 'enum', 3);
				Settings::save('catalog_external_selected_catalogs', $settings->catalog_external_selected_catalogs, 'string', 255);

				// Trigger Curricula App "is home page" to OFF if Catalog just has been set to ON
				if ($settings->first_catalogue === 'on') {
					Settings::save('curricula_is_home_page', 'off');
				}

				// Update the Course Catalog widget configuration
				$catalogDashlet = DashletCatalog::getFullCatalogDashlet();
				if($catalogDashlet) {
					$params = json_decode($catalogDashlet->params);
					$params->show_categories_selector = $settings->catalog_use_categories_tree == 'on' ? "1" : "0";
					$catalogDashlet->params = json_encode($params);
					$catalogDashlet->save();
				}

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));

				// Trigger hydra invalidation
				Yii::app()->legacyWrapper->invalidate();
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$settings->catalog_external = (Settings::getStandard('catalog_external') == 'on' ? true : false);
		$settings->catalog_type = Settings::getStandard('catalog_type');
		$settings->first_catalogue = (Settings::getStandard('first_catalogue') === 'on' ? true : false);
		$settings->on_catalogue_empty = (Settings::getStandard('on_catalogue_empty') === 'on' ? true : false);
		$settings->catalog_use_categories_tree = (Settings::getStandard('catalog_use_categories_tree') === 'on' ? true : false);
		$settings->show_course_details_dedicated_page = (Settings::getStandard('show_course_details_dedicated_page') === 'on' ? true : false);
		$settings->catalog_external_selected_catalogs = Settings::getStandard('catalog_external_selected_catalogs', '');

		$countSelectedCatalogs = 0;
		if ($settings->catalog_external_selected_catalogs <> '') {
			$countSelectedCatalogs = count(explode(',', $settings->catalog_external_selected_catalogs));
		}

		$this->render('settings',
			array(
				'settings' => $settings,
				'countSelectedCatalogs' => $countSelectedCatalogs
			)
		);
	}



	public function actionIndex() {

		$idUser = Yii::app()->user->id;

		$useCategoriesTree = (Settings::get('catalog_use_categories_tree') === 'on' ? true : false);

		$this->render('index', array(
			'idUser' => $idUser,
			'useCategoriesTree' => $useCategoriesTree,
			/*'useLoadMoreButton' => true,
			'useScrollAndLoad' => true*/
		));

	}

	public function actionAssignCatalogs(){
		if (Yii::app()->request->isAjaxRequest) {
			$powerUserModel = new CoreUser();

			$search = null;
			$catalogModel = new LearningCatalogue();
			if (!empty($_POST['search_input'])) {
				$search = Yii::app()->request->getParam('search_input');
			}

			$this->renderPartial('_catalog', array(
				'catalogModel' => $catalogModel,
				'powerUserModel' => $powerUserModel,
				'search' => $search != null ? $search : null
			), false, true);
		}
	}

}
