<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/index')); ?>
		<span><?php echo Yii::t('standard', 'Assigned courses') . ': ' . $catalogName;?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php
					$title 	= '<span></span>'.Yii::t('standard', '_ASSIGN_COURSES'); 
					$url = Docebo::createLmsUrl('usersSelector/axOpen', array(
                        'type' 		=> UsersSelector::TYPE_CATALOG_MANAGEMENT_COURSES,
						'idCatalog' => $id,
                    ));
					
					echo CHtml::link($title, $url, array(
						'class' => 'open-dialog group-select-users',
						'rel' 	=> 'users-selector',	
						'alt' 	=> Yii::t('helper', 'Catalog assign courses'),
						'data-dialog-title' => Yii::t('standard', '_ASSIGN_COURSES'),
						'title'	=> Yii::t('standard', '_ASSIGN_COURSES')
					)); 
					
				?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>