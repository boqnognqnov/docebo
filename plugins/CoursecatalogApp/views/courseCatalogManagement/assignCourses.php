<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Catalog') => Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/index'),
	$catalogModel->name,
	Yii::t('standard', '_ASSIGN_COURSES'),
); ?>

<?php $this->renderPartial('_mainCourseCatalogAssignCoursesActions2', array('id' => $catalogModel->idCatalogue, 'catalogName' => $catalogModel->name)); ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'assign-course-catalog-management-course-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#assign-course-catalog-management-course-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($assignedCourses, 'idEntry', array(
					'id' => 'advanced-search-group',
					'name' => 'LearningCourse[name]',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:courseManagement/courseAutocomplete'),
					'data-catalogue' => $catalogModel->idCatalogue,
					'data-onlyUsers' => true,
					'data-onlyAssignedCourses' => true,
					'placeholder' => Yii::t('standard', '_SEARCH'),
				)); ?>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'assign-course-catalog-management-course-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $assignedCourses->dataProvider(),
		'itemValue' => 'idEntry',
		'displayFullSelection' => 'true',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="assign-course-catalog-management-course-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_DEL'); ?></option>
		</select>
		<label for="assign-course-catalog-management-course-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
	</div>
</div>

<div class="bottom-section">
<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned courses', array($catalogModel->name)); ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'assign-course-catalog-management-course-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $assignedCourses->dataProvider(),
			'itemView' => '_viewCourseSqlDataprovider',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'DoceboCLinkPager',
				'maxButtonCount' => 8,
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'assign-course-catalog-management-course-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {}
				}
				options.data.selectedItems =  $(\'#\'+id+\'-selected-items-list\').val();
			}',
			'ajaxUpdate' => 'assign-course-catalog-management-course-list-all-items',
			'afterAjaxUpdate' => 'function(id, data) { $("#assign-course-catalog-management-course-list input").styler();updateSelectedCheckboxesCounter(id); $.fn.updateListViewSelectPage();}',
		)); ?>
	</div>
</div>

<script type="text/javascript">
	(function ($) {
		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});
	})(jQuery);
</script>
