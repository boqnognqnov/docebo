<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo '<input id="assign-course-catalog-management-list-checkboxes_' . $data['idst']
			.'" type="checkbox" name="assign-course-catalog-management-list-checkboxes[]" value="' . $data['idst']
			.'" ' . ( LearningCatalogueMember::model()->inSessionList('selectedItems', 'idst_member') ? 'checked="checked"' : '' ) . '>'; ?>
	</div>
	<div class="group-member-username">
		<?php
			switch ($data['type']) {
				case 'user': {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('standard', '_USER');
					$_name = Yii::app()->user->getRelativeUsername($data['name']);
				} break;
				case 'group': {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_GROUPTYPE_FREE');
					$_name = Yii::app()->user->getRelativeUsername($data['name']);
				} break;
				case 'orgchart': {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE');
					$_name = $data['name'];
				} break;
				case 'orgchart_desc': {
					$_style = '';
					$_title = '';
					$_suffix = Yii::t('admin_directory', '_DIRECTORY_MEMBERTYPETREE').' + '.Yii::t('organization_chart', '_ORG_CHART_INHERIT');
					$_name = $data['name'];
				} break;
				default:  {
					$_style = '';
					$_title = '';
					$_suffix = '';
					$_name = '';
				} break;
			}
			echo '<span class="'.$_style.'"'.($_title != '' ? ' title="'.$_title.'"' : '').'></span>';

			echo ($_name != '' ? $_name : '&nbsp;');
			echo ($_suffix != '' ? '&nbsp;('.strtolower($_suffix).')' : '');
		?>
	</div>
	<div class="delete-button">
		<?php $this->renderPartial('//common/_modal', array(
			'config' => array(
				'class' => 'delete-node',
				'modalTitle' => Yii::t('standard', '_UNASSIGN'),
				'linkTitle' => Yii::t('standard', '_DEL'),
				'url' => 'CoursecatalogApp/CourseCatalogManagement/deleteAssignedUser&id='. $data['idst'].'&id_catalog='.$data['id_catalog'],
				'buttons' => array(
					array(
						'type' => 'submit',
						'title' => Yii::t('standard', '_CONFIRM'),
					),
					array(
						'type' => 'cancel',
						'title' => Yii::t('standard', '_CANCEL'),
					),
				),
				'afterLoadingContent' => 'hideConfirmButton();',
				'afterSubmit' => 'updateCourseCatalogAssignUsersContent_new',
			),
		)); ?>
	</div>
</div>