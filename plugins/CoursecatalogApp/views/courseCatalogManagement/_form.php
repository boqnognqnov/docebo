<?php $uniqueId = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'courseCatalog_form',
	)); ?>

		<?php echo $form->labelEx($model, 'name'); ?>
		<?php echo $form->textField($model, 'name'); ?>
		<?php echo $form->error($model, 'name'); ?>

		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->textArea($model, 'description', array('id' => 'tiny_'. $uniqueId)); ?>
		<?php echo $form->error($model, 'description'); ?>
		
	<?php $this->endWidget(); ?>
</div>