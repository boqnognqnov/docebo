<?php
echo CHtml::beginForm('', 'POST', array(
	'class'=>'ajax',
));

echo CHtml::hiddenField('_key_', $cacheKey, array('id' => '_key_-input'));

?>
<div id="assign-user-progressive-container" class="assign-user-progressive-container">


	<div class="row-fluid">
		<div class="span12">

			<div class="pull-left sub-title" >
				<b></b><?= Yii::t('standard', 'Importing {x} out of {y}', array('{x}' => $processedUsers, '{y}' => $totalUsers)) ?></b>
			</div>
			<div class="pull-right percentage-gauge"><?=$completedPercent?>%</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span12">
			<div class="docebo-progress progress progress-success">
				<div style="width: <?=$completedPercent?>%;" class="bar full-height" id="uploader-progress-bar"></div>
			</div>
		</div>
	</div>

</div>

<?= CHtml::endForm() ?>


<?php

if($stop && $afterUpdateCallback): ?>
    
<script type="text/javascript">
    <?= $afterUpdateCallback ?>
</script>
<?php endif; ?>    
    
<script type="text/javascript">

	$(function(){

		//the ID of the listview
		var listId = 'assign-course-catalog-management-user-list';

		//while doing progressive stuff, no confirm button has to be displayed
		var b = $('.modal.fade.in').find('.btn-submit');
		if (b.length > 0) { b.remove(); }

		var updateGrid = function() {
			$.fn.yiiListView.update(listId);
		};

		var stopProgress = function() {
			if ($.activeProgressOperation) {
				$.activeProgressOperation.status = 'stopped';
			}
		}

		<?php if ($stop): ?>
		updateGrid();
		<?php endif; ?>

		<?php if ($start && !$stop): ?>
		var c = $('.modal.fade.in').find('.btn-cancel');
		if (c.length > 0) { c.on('click', function(e) { updateGrid(); stopProgress(); }); }
		var c = $('.modal.fade.in').find('.btn-close');
		if (c.length > 0) { c.on('click', function(e) { updateGrid(); stopProgress(); }); }
		<?php endif; ?>

		<?php if (!empty($toBeUnselected)): ?>

		var searchIndex = function(arr, needle) {
			for (var i=0; i<arr.length; i++) {
				if (arr[i] == needle) { //NOTE: we don't know if the needle comes as integer or numeric string ... no strict check here
					return i;
				}
			}
			return -1;
		};


		//update selection
		var toRemove = <?= CJSON::encode($toBeUnselected) ?>;
		var allItemsList = $('#'+listId+'-all-items-list').val().split(',');
		var selectedItemsList = $('#'+listId+'-selected-items-list').val().split(',');

		if (allItemsList.length > 0) {
			var newAllItemsList = [];
			for (var i=0; i<allItemsList.length; i++) {
				if (searchIndex(toRemove, allItemsList[i]) < 0) {
					newAllItemsList.push(allItemsList[i]);
				}
			}
			$('#'+listId+'-all-items-list').val(newAllItemsList.join(','));
			$('#'+listId+'-all-items-count').val(newAllItemsList.length);
		}

		if (selectedItemsList.length > 0) {
			var newSelectedItemsList = [];
			if (newSelectedItemsList.length > 0) {
				for (var i = 0; i < selectedItemsList.length; i++) {
					if (searchIndex(toRemove, selectedItemsList[i]) < 0) {
						newSelectedItemsList.push(selectedItemsList[i]);
					}
				}
			}
			$('#' + listId + '-selected-items-list').val(newSelectedItemsList.join(','));
			$('#' + listId + '-selected-items-count').html('' + newSelectedItemsList.length);
		}

		<?php endif; ?>
	});

</script>