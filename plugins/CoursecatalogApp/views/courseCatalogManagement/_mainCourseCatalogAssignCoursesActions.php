<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/index')); ?>
		<span><?php echo Yii::t('standard', 'Assigned courses') . ': ' . $catalogName;?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'group-select-users',
						'modalTitle' => Yii::t('standard', '_ASSIGN_COURSES'),
						'linkTitle' => '<span></span>'. Yii::t('standard', '_ASSIGN_COURSES'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'Catalog assign courses'),
						),
						'url' => 'CoursecatalogApp/CourseCatalogManagement/assignSelectCourses&id=' . $id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
								'formId' => 'select-course-form',
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CLOSE'),
							),
						),

						// !!!! THIS DOESN'T SUPPORT MULTILINE functions:
						// Gotta love Quartz (see scriptModal.js::processCallback())
						'afterLoadingContent' => 'function() {replacePlaceholder(); applyTypeahead($(\'.typeahead\')); $(".modal.group-select-users").addClass("force-absolute");}',

						'beforeSubmit' => 'initCourseCatalogAssignCoursesData',
						'afterSubmit' => 'updateCourseCatalogAssignCoursesContent',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>