<div id="grid-wrapper" class="poweruser-course-table">
<h2>Select courses</h2>
	<?php
		$columns = array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'catalog-course-grid-checkboxes',
				'value' => '$data->idCourse',
				'checked' => 'in_array($data->idCourse, Yii::app()->session[\'selectedItems\'])',
			),
			'code',
			'name'
		);

		if (PluginManager::isPluginActive('ClassroomApp')) {
			$columns[] = 'course_type';
		}

		$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'catalog-course-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix'),
			'dataProvider' => $model->dataProvider(false, $excludedCourses),
			'cssFile' => false,
			'columns' => $columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
				'cssFile' => false,
			),
			'beforeAjaxUpdate' => 'function(id, options) {
					options.type = "POST";
					if (options.data == undefined) {
						options.data = {
							contentType: "html",
						}
					}
					options.data.selectedItems =  $(\'#\'+id+\'-selected-items-list\').val();
				}',
			'ajaxUpdate' => 'catalog-course-grid-all-items',
			'afterAjaxUpdate' => 'function(id, data){$("#catalog-course-grid input").styler(); afterGridViewUpdate(id);}',
		));
	?>
</div>

