<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'courseCatalog_form',
	)); ?>

	<p><?php echo Yii::t('standard', 'Catalog') . ': "' . $model->name . '"'; ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'CourseCatalog_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo $form->labelEx($model, Yii::t('standard', 'Yes, I confirm I want to proceed'), array('for' => 'CourseCatalog_confirm_'.$checkboxIdKey)); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>