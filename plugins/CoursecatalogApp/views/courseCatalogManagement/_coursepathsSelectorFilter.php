<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'coursepath-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#coursepath-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="coursepath-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('lms:CurriculaApp/curriculaManagement/curriculaAutocomplete') ?>" class="typeahead" id="advanced-search-coursepath" autocomplete="off" type="text" name="LearningCoursepath[path_name]" placeholder="Search coursepath" data-poweruser-not="<?php echo $model->powerUserManager->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'coursepath-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider($excludedPaths),
				'itemValue' => 'id_path',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>
<?php echo CHtml::hiddenField('isSubmit', false, array('id' => 'coursePathSubmitFlag') )?>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>