<div class="users-selector">

	<div class="users-selector-tabs">
		
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-target=".courses-tab" href="#">
					<span class="user-form-details"></span><?= Yii::t('standard', '_COURSES') ?>
				</a>
			</li>
			<li>
				<a data-target=".coursepaths-tab" href="#">
					<span class="user-form-details"></span><?= Yii::t('standard', '_COURSEPATH') ?>
				</a>
			</li>
		</ul>


		<div class="select-user-form-wrapper">
		
			<!-- FILTERS -->
			<div class="tab-content">

				<div class="courses-tab tab-pane active">
					<?php
						$this->renderPartial('_coursesSelectorFilter', array(
							'model' => $courseModel,
							'excludedCourses'=>$excludedCourses,
						));
					?>
				</div>

				<div class="coursepaths-tab tab-pane">
					<?php
						$this->renderPartial('_coursepathsSelectorFilter', array(
							'model' => $coursepathModel,
							'excludedPaths'=>$excludedPaths,
						));
					?>
				</div>

			</div>



			<!-- CONTENT -->
			<?php $form = $this->beginWidget('CActiveForm', array(
				//'id' => 'users-selector-form',
				'id' => 'select-form',
				'htmlOptions' => array(
					'data-grid-items' => 'true',
				),
			)); ?>
			<div class="tab-content">

				<div class="courses-tab tab-pane active">
					<?php
						$this->renderPartial('_coursesSelectorContent', array(
							'model' => $courseModel,
							'excludedCourses'=>$excludedCourses,
						));
					?>
				</div>

				<div class="coursepaths-tab tab-pane">
					<?php
						$this->renderPartial('_coursepathsSelectorContent', array(
							'model' => $coursepathModel,
							'excludedPaths'=>$excludedPaths,
						));
					?>
				</div>

			</div>
			<?php echo CHtml::hiddenField('isSubmit', false, array('id' => 'submitFlag') )?>
			<?php $this->endWidget(); ?>

		</div>

	</div>

</div>