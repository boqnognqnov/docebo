<? $type = $data['type_of_entry'];  //set prefix to the ID in order to recognize course from course path when deleting from LearningCatalogueEntry?>
<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo CHtml::checkBox(
				'assign-course-catalog-management-list-checkboxes[]',
				in_array($data['idEntry'].'_'.$type, Yii::app()->session['selectedItems']),
				array(
					'id' => 'assign-course-catalog-management-list-checkboxes_'. $data['idEntry'],
					'value' => $data['idEntry'].'_'.$type
				)
		)?>
	</div>

	<div class="poweruser-courses-name" style="margin-left: 20px;"><?=$data['type_of_entry']=='course' ? $data['courseName'] : $data['cpName']?></div>
	<div class="course-type">
	<?php if($data['type_of_entry']=='course'): ?>
		<?
		$position = '-48px';
		if ($data['course_type'] == LearningCourse::TYPE_CLASSROOM)
			echo LearningCourse::model()->getCourseTypeTranslation($data['course_type']).'<i class="'.$data['course_type'].'" style="background-position: -252px -100px"></i>';
		else if ($data['course_type'] == LearningCourse::TYPE_WEBINAR)
			echo LearningCourse::model()->getCourseTypeTranslation($data['course_type']).'<i class="icon webinar-icon-small-inline" style="vertical-align: middle; margin-left: 8px; margin-right: 3px;"></i>';
		else
			echo LearningCourse::model()->getCourseTypeTranslation($data['course_type']).'<i class="'.$data['course_type'].'"></i>';
		?>
	<?php else: ?>
		<?= Yii::t('standard', '_COURSEPATH') ?>
	<?php endif; ?>
	</div>
	<div class="delete-button">
			<?php $this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'delete-node',
					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
					'linkTitle' => Yii::t('standard', '_UNASSIGN'),
					'url' => 'CoursecatalogApp/CourseCatalogManagement/deleteAssignedCourse&id='.$data['idEntry'].'_'.$type.'&type_of_entry='.$data['type_of_entry'],
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'hideConfirmButton();',
					'afterSubmit' => 'updateCourseCatalogAssignCoursesContent',
				),
			)); ?>
	</div>
</div>