<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': '<?=CoursecatalogLib::JOB_TYPE_ASSIGN_USERS?>',
			'ids': '<?= $ids ?>',
            'id_catalog': <?=$id_catalog; ?>
		}, function(data){
			var dialogId = 'assign-element-to-catalog';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=  addslashes(Yii::t('standard', 'Enroll users'))?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>
<a class='auto-close'></a>