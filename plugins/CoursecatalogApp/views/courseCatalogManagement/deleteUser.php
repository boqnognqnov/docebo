<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php
	$formParams = array('id' => 'user_form');
	if (is_array($users) && count($users) > 10) { //smaller operations don't need to be progressive
		$formParams['action'] = Docebo::createAbsoluteLmsUrl('progress/run', array('type' => CoursecatalogLib::JOB_TYPE_UNASSIGN_USERS, 'format' => 'json'));
	}
	$form = $this->beginWidget('CActiveForm', $formParams);
	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN'). ' ' . count($users) . ' ' . Yii::t('standard', 'items'); ?></p>

	<div class="clearfix">
		<?php echo $form->checkbox($model, 'confirm', array('id' => 'catalogUser_confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed', array(count($users))), 'catalogUser_confirm_'.$checkboxIdKey); ?>
		<?php echo $form->error($model, 'confirm'); ?>
	</div>

	<?php

	$ids = '';
	if (is_array($users) && !empty($users)) {
		$first = true;
		foreach ($users as $user) {
			if ($first) { $first = false; } else { $ids .= ','; }
			$ids .= $user->idst_member;
		}
	}
	echo CHtml::hiddenField('ids', $ids);

	echo CHtml::hiddenField('id_catalog', $idCatalog);

	$this->endWidget();

	?>
</div>