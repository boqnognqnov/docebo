<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', '_COURSES'); ?></h3>
	<ul class="clearfix">
		<li>
      <div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-label',
						'modalTitle' => 'New Label',
						'linkTitle' => '<span></span>New Label',
						'linkOptions' => array(
							'alt' => 'Lorem Ipsum bla bla bla'
						),
						'url' => 'LabelApp/labelManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => 'Confirm',
							),
							array(
								'type' => 'cancel',
								'title' => 'Cancel',
							),
						),
						'afterLoadingContent' => 'function() { schemeFormInit(); $(\'.modal.in\').find(\'#userForm-details, #userForm-orgchat\').jScrollPane({autoReinitialise: true});
//$(\'#user_form, #orgChart_form, #add-field-form\').jClever({applyTo: {select: true,checkbox: false,radio: false,button: false,file: false,input: false,textarea: false}});
}',
						'afterSubmit' => 'updateLabelContent',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
	    <div>
				<h4 class="clearfix"></h4>
				<p></p>
	    </div>
	</div>
</div>