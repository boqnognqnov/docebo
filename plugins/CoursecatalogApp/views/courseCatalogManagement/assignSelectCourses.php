<div class="users-selector">

	<div class="users-selector-tabs">


		<div class="select-user-form-wrapper">

			<!-- FILTERS -->
			<div class="">

				<div class="courses-tab">
					<?php
					$this->renderPartial('_coursesSelectorFilter', array(
						'model' => $courseModel,
					));
					?>
				</div>

			</div>



			<!-- CONTENT -->
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'select-form',
				'htmlOptions' => array(
					'data-grid-items' => 'true',
				),
			)); ?>
			<div class="">

				<div class="courses-tab">
					<?php
					$this->renderPartial('_coursesSelectorContent', array(
						'model' => $courseModel,
						'excludedCourses'=>$excludedCourses,
					));
					?>
				</div>


			</div>
			<?php echo CHtml::hiddenField('isSubmit', false, array('id' => 'submitFlag') )?>
			<?php $this->endWidget(); ?>

		</div>

	</div>

</div>