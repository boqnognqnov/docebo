<div class="main-actions course-enrollments-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/index')); ?>
		<span><?php echo Yii::t('standard', '_ASSIGN_USERS') . ': ' . $catalogName;?></span>
	</h3>
	<ul class="clearfix">
		<li>
			<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'group-select-users',
						'modalTitle' => Yii::t('standard', '_ASSIGN_USERS'),
						'linkTitle' => '<span></span>'.Yii::t('standard', '_ASSIGN_USERS'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'Catalog assign users'),
						),
						'url' => 'CoursecatalogApp/CourseCatalogManagement/assignSelectUsers&id=' . $id,
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_ADD'),
								'formId' => 'select-form',
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'function() { replacePlaceholder(); applyTypeahead($(\'.typeahead\')); }',
						'beforeSubmit' => 'initGroupMemberData',
						'afterSubmit' => 'updateCourseCatalogAssignUsersContent',
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
		<div>
			<h4 class="clearfix"></h4>
			<p></p>
		</div>
	</div>
</div>