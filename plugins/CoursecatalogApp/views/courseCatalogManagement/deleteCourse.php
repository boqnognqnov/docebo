<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<?php $checkboxIdKey = time(); ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'user_form',
	));	?>

	<p><?php echo Yii::t('standard', '_UNASSIGN').': "'.$totalCount.'" '.Yii::t('standard', '_COURSES'); ?></p>

	<div class="clearfix">
		<?php echo CHtml::checkBox('LearningCatalogueEntry[confirm]', false, array('id' => 'confirm_'.$checkboxIdKey, 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'confirm_'.$checkboxIdKey); ?>
	</div>

	<?

	if (isset($courses) && is_array($courses)) {
		foreach ($courses as $course) {
			echo CHtml::hiddenField('ids[]', $course->idCourse . '_' . CoreAdminCourse::TYPE_COURSE, array('id' => false));
		}
	}

	if (isset($coursepaths) && is_array($coursepaths)) {
		foreach ($coursepaths as $coursepath) {
			echo CHtml::hiddenField('ids[]', $coursepath->id_path . '_' . CoreAdminCourse::TYPE_COURSEPATH, array('id' => false));
		}
	}

	?>

	<?php $this->endWidget(); ?>
</div>