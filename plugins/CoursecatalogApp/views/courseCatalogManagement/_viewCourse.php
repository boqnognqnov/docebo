<div class="item  <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<div class="checkbox-column">
		<?php echo '<input id="assign-course-catalog-management-list-checkboxes_'. $data->idEntry .'" type="checkbox" name="assign-course-catalog-management-list-checkboxes[]" value="'.$data->idEntry.'" '.(($data->inSessionList('selectedItems', 'idEntry'))?'checked="checked"':'').'>'; ?>
	</div>

	<div class="group-member-username"><?php echo $data->course->name;?></div>
		<div class="delete-button">
			<?php $this->renderPartial('//common/_modal', array(
				'config' => array(
					'class' => 'delete-node',
					'modalTitle' => Yii::t('standard', '_UNASSIGN'),
					'linkTitle' => Yii::t('standard', '_UNASSIGN'),
					'url' => 'CoursecatalogApp/CourseCatalogManagement/deleteAssignedCourse&id='. $data->idEntry,
					'buttons' => array(
						array(
							'type' => 'submit',
							'title' => Yii::t('standard', '_CONFIRM'),
						),
						array(
							'type' => 'cancel',
							'title' => Yii::t('standard', '_CANCEL'),
						),
					),
					'afterLoadingContent' => 'hideConfirmButton();',
					'afterSubmit' => 'updateCourseCatalogAssignCoursesContent',
				),
			)); ?>
		</div>
</div>