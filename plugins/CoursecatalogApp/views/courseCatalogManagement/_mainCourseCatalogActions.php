<div class="main-actions clearfix">
	<h3 class="title-bold"><?php echo Yii::t('standard', 'Catalog'); ?></h3>
	<ul class="clearfix">
		<li>
      		<div>
				<?php $this->renderPartial('//common/_modal', array(
					'config' => array(
						'class' => 'new-catalog',
						'modalTitle' => Yii::t('catalogue', '_NEW_CATALOGUE'),
						'linkTitle' => '<span></span>'.Yii::t('catalogue', '_NEW_CATALOGUE'),
						'linkOptions' => array(
							'alt' => Yii::t('helper', 'New catalog')
						),
						'url' => 'CoursecatalogApp/CourseCatalogManagement/create',
						'buttons' => array(
							array(
								'type' => 'submit',
								'title' => Yii::t('standard', '_CONFIRM'),
							),
							array(
								'type' => 'cancel',
								'title' => Yii::t('standard', '_CANCEL'),
							),
						),
						'afterLoadingContent' => 'courseCatalogLoadCallback();',
						'beforeSubmit' => 'beforeCourseCreateSubmit'
					),
				)); ?>
			</div>
		</li>
	</ul>
	<div class="info">
	    <div>
			<h4 class="clearfix"></h4>
			<p></p>
	    </div>
	</div>
</div>


<script>
	$('.modal.new-catalog a.btn.btn-submit').live('click', function(ev) {
		var btn = $(this);
		btn.addClass('disabled');
		btn.prop('disabled', true);

		var modal = $('.modal.new-catalog');
		var data = modal.find("form").serializeArray();
		data.push({
			name: 'LearningCatalogue[description]',
			value: tinymce.activeEditor.getContent()
		});

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('form').attr('action'),
			'cache': false,
			'data': data,
			'success': function (data) {
				$.fn.updateCourseCatalogContent(data);
				hideLoading('contentLoading');
				setTimeout(function() {
					btn.removeClass('disabled');
					btn.prop('disabled', false);
				}, 1000);
			}
		});
	})

</script>
