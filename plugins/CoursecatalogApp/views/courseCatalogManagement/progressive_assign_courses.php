<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			type		: <?= json_encode(CoursecatalogLib::JOB_TYPE_ASSIGN_COURSES) ?>,
			course_ids	: <?= json_encode(implode(",", $selection['courses'])) ?>,
			plan_ids	: <?= json_encode(implode(",", $selection['plans'])) ?>,
            id_catalog	: <?= json_encode($id_catalog) ?>
		}, function(data){
			var dialogId = 'assign-element-to-catalog';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=addslashes(Yii::t('standard', 'Assign courses and learning plans'))?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>
<a class='auto-close'></a>