<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'catalog-course-form',
	'htmlOptions' => array('class' => 'ajax-grid-form', 'data-grid' => '#catalog-course-grid'),
)); ?>
<div class="main-section">

	<div class="filters-wrapper">
		<div class="selections" data-grid="catalog-course-grid">
			<div class="input-wrapper">
				<input data-url="<?= Docebo::createAppUrl('admin:courseManagement/courseAutocomplete') ?>" class="typeahead" id="advanced-search-course" autocomplete="off" type="text" name="LearningCourse[name]" placeholder="Search course" data-poweruser-not="<?php echo $model->powerUserManager->idst; ?>"/>
				<span class="search-icon"></span>
			</div>
			<?php $this->widget('ext.local.widgets.GridViewSelectAll', array(
				'gridId' => 'catalog-course-grid',
				'class' => 'left-selections',
				'dataProvider' => $model->dataProvider(false, $excludedCourses),
				'itemValue' => 'idCourse',
			)); ?>
		</div>
	</div>
</div>

<input type="hidden" name="contentType" value="html"/>
<?php echo CHtml::hiddenField('isSubmit', false, array('id' => 'submitFlag') )?>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>