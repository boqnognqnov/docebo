<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Catalog'),
); ?>

<?php
if (!isset($canCreate) || $canCreate === true)
	$this->renderPartial('_mainCourseCatalogActions');

$isPu = Yii::app()->user->getIsPu();
$powerUserId = null;
if($isPu) $powerUserId = Yii::app()->user->id;
?>

<div class="bottom-section border-bottom-section clearfix">
	<div id="grid-wrapper">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'catalogue-management-grid',
			'htmlOptions' => array('class' => 'grid-view clearfix catalogue-management-table'),
			'dataProvider' => $catalogModel->dataProvider($powerUserId),
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
			'pager' => array(
				'class' => 'ext.local.pagers.DoceboLinkPager',
			),
			'ajaxUpdate' => 'all-items',
			'afterAjaxUpdate' => 'function(id, data){ }',
			'columns' => $columns
		)); ?>
	</div>
</div>