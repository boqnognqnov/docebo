<?php $this->breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard', 'Catalog') => Yii::app()->createUrl('CoursecatalogApp/CourseCatalogManagement/index'),
	$catalogModel->name,
	Yii::t('standard', '_ASSIGN_USERS'),
); ?>

<?php //$this->renderPartial('_mainCourseCatalogAssignUsersActions', array('id' => $catalogModel->idCatalogue, 'catalogName' => $catalogModel->name)); ?>
<?php $this->renderPartial('_mainAssignUsersActions', array('id' => $catalogModel->idCatalogue, 'catalogName' => $catalogModel->name)); ?>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'assign-course-catalog-management-user-form',
	'htmlOptions' => array(
		'class' => 'ajax-list-form',
		'data-grid' => '#assign-course-catalog-management-user-list'
	),
)); ?>

<div class="main-section group-management">
	<div class="filters-wrapper">
		<div class="filters clearfix">
			<div class="input-wrapper">
				<?php echo $form->textField($assignedUsers, 'search_input', array(
					'id' => 'advanced-search-group',
					'class' => 'typeahead',
					'autocomplete' => 'off',
					'data-url' => Docebo::createAppUrl('admin:userManagement/usersAutocomplete'),
					'data-catalogue' => $catalogModel->idCatalogue,
					'data-onlyUsers' => true,
					'data-onlyAssignedUsers' => true,
					'placeholder' => Yii::t('standard', '_SEARCH'),
					'data-source-desc' => 'true',
				)); ?>
				<span class="search-icon"></span>
			</div>
		</div>
	</div>
</div>

<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
<?php $this->endWidget(); ?>

<div class="selections clearfix">
	<?php $this->widget('ext.local.widgets.ListViewSelectAll', array(
		'gridId' => 'assign-course-catalog-management-user-list',
		'class' => 'left-selections clearfix',
		'dataProvider' => $assignedUsers->dataProvider(),
		'itemValue' => 'idst_member',
	)); ?>

	<div class="right-selections clearfix">
		<select name="massive_action" id="assign-course-catalog-management-user-massive-action">
			<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
			<option value="delete"><?php echo Yii::t('standard', '_UNASSIGN'); ?></option>
		</select>
		<label for="assign-course-catalog-management-user-massive-action"><?php echo Yii::t('standard', '_ON_SELECTED'); ?></label>
	</div>
</div>

<div class="bottom-section">
	<h3 class="title-bold title-with-border"><?php echo Yii::t('standard', 'Assigned users', array($catalogModel->name)); ?></h3>
	<div id="grid-wrapper">
		<?php $this->widget('ext.local.widgets.CXListView', array(
			'id' => 'assign-course-catalog-management-user-list',
			'htmlOptions' => array('class' => 'list-view clearfix'),
			'dataProvider' => $assignedUsers->assignedDataProvider($assignedUsers->idCatalogue),
			'itemView' => '_view',
			'itemsCssClass' => 'items clearfix',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				//'class' => 'ext.local.pagers.DoceboLinkPager',
				'class' => 'common.components.DoceboCLinkPager',
			),
			'template' => '{items}{pager}{summary}',
			'ajaxUpdate' => 'assign-course-catalog-management-user-list-all-items',
			'beforeAjaxUpdate' => 'function(id, options) {
				options.type = "POST";
				if (options.data == undefined) {
					options.data = {} 
                }
                options.data.selectedItems =  $(\'#\'+id+\'-selected-items-list\').val();

			}',
			'afterAjaxUpdate' => 'function(id, data) { gridAfterAjaxUpdate(id, data); $.fn.updateListViewSelectPage();}',
		)); ?>
	</div>
</div>
<script type="text/javascript">

	var gridAfterAjaxUpdate = function(id, data) {

		var selected = $('#assign-course-catalog-management-user-list-selected-items-list').val().split(',');
		if (selected.length > 0) {
			var checkboxes = $('input[id^="assign-course-catalog-management-list-checkboxes_"]');
			if (checkboxes.length > 0) {
				checkboxes.each(function(index) {
					var value = $(this).val(), found = false;
					for (var i=0; i<selected.length && !found; i++) {
						if (value == selected[i]) {
							found = true;
						}
					}
					if (found) {
						$(this).prop('checked', true);
					}
				});
			}
		}

		$("#assign-course-catalog-management-user-list input").styler();
	};



	(function ($) {

		$(function () {
			$('input, select').styler();
			replacePlaceholder();
		});

		//NOTE: original "updateCourseCatalogAssignUsersContent" function is found in file 'scriptModal.js'
		$.fn.updateCourseCatalogAssignUsersContent_new = function(data) {
			if (data.html) {
				$('.modal.in .modal-body').html(data.html);
			} else {
				//hide dialog and manually update selector
				$('#assign-course-catalog-management-user-list-selected-items-list').val('');
				$.fn.yiiListView.update('assign-course-catalog-management-user-list');
				$('.modal.in').modal('hide');
				$('#assign-course-catalog-management-user-massive-action').val('');
				$('#assign-course-catalog-management-user-list-selected-items-count').text('0');

				//Restore the page selector
				$("a.deselect-all").removeClass('active');
				$("a.deselect-this").removeClass('active');
				$("a.select-all").addClass('active');
				$("a.select-this").addClass('active');
			}
		}

		//NOTE: actually functions "getSelectedCheckboxes()" and "getRandKey()" are found in file 'script.js'
		$('#assign-course-catalog-management-user-massive-action').live('change', function() {
			var selectedUsers = getSelectedCheckboxes('assign-course-catalog-management-user-list', 'assign-course-catalog-management-user-list-checkboxes');
			if (selectedUsers.length > 0) {
				switch ($(this).val()) {
					case 'delete':
						config = {
							'id'                 : 'modal-' + getRandKey(),
							'modalClass'         : 'delete-node',
							'modalTitle'         : <?= CJSON::encode(Yii::t('standard', '_DEL_SELECTED')) ?>,
							'modalUrl'           : <?= CJSON::encode(Docebo::createAdminUrl('CoursecatalogApp/CourseCatalogManagement/deleteAssignedUser', array('id_catalog' => $catalogModel->getPrimaryKey()))) ?>,
							'modalRequestType'   : 'POST',
							'modalRequestData'   : {'ids' : selectedUsers.join(",")},
							'buttons'            : [
								{"type": "submit", "title": Yii.t("standard", "_CONFIRM")},
								{"type": "cancel", "title": Yii.t("standard", "_CLOSE")}
							],
							'afterLoadingContent': 'hideConfirmButton();',
							'afterSubmit'        : 'updateCourseCatalogAssignUsersContent_new'
						}
						$('body').showModal(config);
						break;
				}
			}
			$('#'+$(this).attr('id')+'-styler ul li:first').click();
		});

	})(jQuery);

</script>