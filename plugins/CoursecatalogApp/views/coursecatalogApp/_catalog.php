<h1><?=Yii::t('standard', '_SELECT').' '.Yii::t('standard', 'Courses catalogs'); ?></h1>
<div id="grid-wrapper">
	<?php $this->widget('common.widgets.ComboGridView', array(
		'gridTemplate'	=> '{items}{summary}{pager}',
		'gridId' => 'catalogue-management-grid',
		'dataProvider' => $catalogModel->dataProvider(null, $search),
		'massSelectorsInFilterForm' => true,
		'disableMassActions' => true,
		'gridTitleLabel' => Yii::t('standard', 'Select Courses Catalogs'),
		'massSelectorUrl' => Docebo::createAdminUrl('PowerUserApp/powerUserManagement/catalogsSelectAll', array('catalogId' => $catalogModel->idCatalogue)),
		'columns' => array(
			array(
				'header' => '<input type="checkbox" class="select-on-check-all" />',
				'type' => 'raw',
				'value' => 'CHtml::checkBox("idCatalogue",false,array("value"=>"$data->idCatalogue", "class"=>"select-on-check"))'
			),
			array(
				'header'=> Yii::t('salesforce', 'Catalog Name'),
				'value'=>'$data->name',
			),
			array(
				'header'=> '<span class="i-sprite is-forum"></span>',
				'value'=>'$data->renderCatalogCourses(true, true);',
			),
			array(
				'header'=> '<span class="i-sprite is-couple"></span>',
				'value'=>'$data->renderCatalogUsers(true, true);',
			),
		),
	)); ?>
	<?= CHtml::beginForm('','POST', array('class'=>'ajax', 'id'=>'submitForm')) ?>
	<div class="form-actions">
		<input class="btn confirm-btn" type="submit" value="<?= Yii::t('standard', '_CONFIRM'); ?>" name="submit"
		       data-submit-form="select-form"/>
		<input class="btn close-btn close-dialog" type="button" value="<?= Yii::t('standard', '_CLOSE'); ?>"/>
	</div>
	<?= CHtml::endForm() ?>
</div>

<script>
	$(function() {
		var preselectedItems = $('input[name=selected_catalogs]').val();

			if (preselectedItems != '') {
				var preselectedArr1 = preselectedItems.split(",");
				var preselectedArr2 = [];
				$.each( preselectedArr1, function( key, value ) {
					preselectedArr2.push(parseInt(value));
				});

				$('#catalogue-management-grid').comboGridView('setSelection', preselectedArr2);
			}
	});

	$('#submitForm').submit(function(e){
		e.preventDefault();
		var selectedCatalogs = $('#catalogue-management-grid').comboGridView('getSelection');
		var data = {};
		data['selectedCatalogs'] = selectedCatalogs;

		$('input[name=selected_catalogs]').val(selectedCatalogs);
		$('span#selected_catalogs_count').html(selectedCatalogs.length);

		$(this).closest('.modal-body').dialog2('close');
	});
//	$('input').styler();
</script>