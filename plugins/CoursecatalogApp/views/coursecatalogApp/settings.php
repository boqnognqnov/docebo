<?php
/* @var $form CActiveForm */
/* @var $settings CoursecatalogAppSettingsForm */

$_breadcrumbs = array(
	Yii::t('menu', 'Admin') => Docebo::createAppUrl('admin:dashboard/index'),
	Yii::t('standard' ,'Catalog') => Docebo::createAppUrl('admin:CoursecatalogApp/CoursecatalogApp/settings'),
	Yii::t('standard','Settings')
);

$this->breadcrumbs = $_breadcrumbs;
?>

<?php DoceboUI::printFlashMessages(); ?>

<a href="<?= ('it' == Yii::app()->getLanguage()) ?	'https://www.docebo.com/it/knowledge-base/elearning-come-attivare-e-gestire-un-catalogo/'
		: 'https://www.docebo.com/knowledge-base/elearning-how-to-activate-and-manage-a-catalog/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>


<div class="advanced-main" id="advanced-catalog-settings-form">


	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'app-settings-form',
		'htmlOptions'=>array('class' => 'form-horizontal'),
	));
	?>

	<div class="section">
		<h3 class="advanced-catalog-settings-form-title"><?= Yii::t('standard' ,'Catalog') ?> <?=Yii::t('standard','Settings') ?></h3>
		<br />
		<div class="row odd">
			<div class="row">
				<div class="setting-name"><?php echo CHtml::label(Yii::t('catalogue', 'Internal Catalog'), 'CoursecatalogAppSettingsForm_catalog_type'); ?></div>
				<div class="values">
					<?=$form->dropDownList($settings, 'catalog_type', call_user_func(array($settings, "getValuesDropdown")), array('class'=>'input-xxlarge'));?>
					<?=$form->error($settings, 'catalog_type'); ?>

					<div class="checkbox-item">
						<br/>
						<?=$form->checkBox($settings, 'on_catalogue_empty');?>
						<?=CHtml::label(Yii::t('catalogue', 'Show the internal generic catalog if user is not assigned to any specific catalog'), 'CoursecatalogAppSettingsForm_on_catalogue_empty');?>
						<?=$form->error($settings, 'on_catalogue_empty'); ?>
					</div>
				</div>
			</div>
		</div>


		<div class="row even">
			<div class="row">
			<div class="setting-name"><?php echo CHtml::label(Yii::t('catalogue', 'Public Catalog'), 'CoursecatalogAppSettingsForm'); ?></div>
				<div class="values">
					<div class="checkbox-item">
						<?=$form->checkBox($settings, 'catalog_external', array('uncheckValue' => 0, 'value' => 1));?>
						<?=CHtml::label(Yii::t('catalogue', 'Show the catalog also to non authenticated users'), 'CoursecatalogAppSettingsForm_catalog_external', array('class'=>''));?>
						<?=$form->error($settings, 'catalog_external'); ?>
					</div>
					<div class="row extra-catalog_external" style="margin-left: 30px;display:inline-block;float:left;">
						<br />
						<div class="checkbox-item">
							<?=CHtml::radioButton('catalog_external_selected_catalogs', ($settings->catalog_external_selected_catalogs == '' || !$settings->catalog_external_selected_catalogs) ? true : false, array('id'=>'catalog_external_no_selected_catalogs', 'value'=> 0));?>
							<?=CHtml::label(Yii::t('catalogue', 'Display all available public courses and learning plans'), 'catalog_external_no_selected_catalogs', array('class'=>'', 'style'=>'display: inline-block;margin-left: 0px;'));?>
							<?php /* echo $form->error($settings, 'catalog_external_selected_catalogs');  */ ?>
						</div>
						<div class="checkbox-item">
							<?=CHtml::radioButton('catalog_external_selected_catalogs', ($settings->catalog_external_selected_catalogs != '' && $settings->catalog_external_selected_catalogs) ? true : false, array('id'=>'catalog_external_selected_catalogs', 'value'=> 1));?>
							<?=CHtml::label(Yii::t('catalogue', 'Select catalogs to display') .'(<span id="selected_catalogs_count">'.$countSelectedCatalogs.'</span>)', 'catalog_external_selected_catalogs', array('class'=>''));?>
							<a href="index.php?r=CoursecatalogApp/CoursecatalogApp/assignCatalogs&idst=<?= $idAccount ?>"
							   data-dialog-class="catalogapp-dialog-select-catalogs"
							   class="open-dialog btn btn-docebo green big select-custom-catalogs" style="margin-left: 200px; display: inline-block; margin-top: -35px;">
								<?=Yii::t('standard', '_SELECT');?>
							</a>
							<?=CHtml::hiddenField('selected_catalogs_holder', $settings->catalog_external_selected_catalogs); ?>
							<?=CHtml::hiddenField('selected_catalogs', $settings->catalog_external_selected_catalogs) ; ?>
							<?=$form->error($settings, 'catalog_external_selected_catalogs'); ?>
						</div>
					</div>
                    <div class="layout-thumbnail" style="display:inline-block; margin-right:10px;margin-top: 15px;float: right;position:relative;">
                        <span class="preview" style="max-width:265px;left:50px;top:-20px;font-size:13px !important;"><strong><?php echo Yii::t('catalogue', 'Public Catalog preview in Homepage form'); ?></strong></span>
                        <span class="background" style="max-width:136px;left:-20px;top:190px;"><strong><?php echo Yii::t('certificate', '_BACK_IMAGE'); ?></strong></span>
                        <span class="title" style="left:170px;top:295px;"><strong><?php echo Yii::t('catalogue', 'Catalog courses'); ?></strong></span>
                        <span class="text" style="left:240px;top:10px;"><strong><?php echo Yii::t('branding', '_TITLE_TEXT_SECTION'); ?></strong></span>
                        <span class="external" style="left:10px;top:110px;"><strong><?php echo Yii::t('admin_webpages', '_WEBPAGES_CAPTION'); ?></strong></span>
                        <?php echo CHtml::image(Yii::app()->theme->baseUrl . '/images/loginlayouts/preview-catalog.jpg','',array('style'=>'width: 380px;')); ?>

                    </div>
				</div>
			</div>
		</div>

		<div class="row odd">
			<div class="row">
				<div class="setting-name">
					<?php echo CHtml::label(Yii::t('player', 'Catalog options'), 'CoursecatalogAppSettingsForm'); ?>
					<p class="setting-description"><?= Yii::t('catalogue', 'It applies to both the Internal and the Public Catalog.')?></p>
				</div>
				<div class="values">
					<div class="checkbox-item">
						<?=$form->checkBox($settings, 'catalog_use_categories_tree');?>
						<?=$form->labelEx($settings, 'catalog_use_categories_tree', array('class'=>''));?>
						<?=$form->error($settings, 'catalog_use_categories_tree'); ?>
					</div>
					<?php if (!Yii::app()->legacyWrapper->hydraFrontendEnabled()) { ?>
					<div class="checkbox-item">
						<?=$form->checkBox($settings, 'show_course_details_dedicated_page');?>
						<?=$form->labelEx($settings, 'show_course_details_dedicated_page', array('class'=>''));?>
						<?=$form->error($settings, 'show_course_details_dedicated_page'); ?>
					</div>
					<?php }?>
				</div>
			</div>
		</div>


		<div class="form-actions">
			<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
			&nbsp;
			<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>
<style type="text/css">
    div.layout-thumbnail span{
        color:#0465AC;
        font-family:'Handlee',cursive;
        font-size:19px;
        line-height:22px;
        text-align:right;
        max-width:150px;
        position:absolute;
    }
</style>
<script type="text/javascript">
	$('input,select').styler();

	var selectCatalogLink = $('.select-custom-catalogs').attr('href');
	var noLink = 'javascript:return false;';

	// Hide select custom catalogs radio group if needed
	if (!($('input[id=CoursecatalogAppSettingsForm_catalog_external]').is(':checked'))) {
		$('.extra-catalog_external').addClass('hidden');
	}

	// Show/Hide select custom catalogs
	$(document).on('change', 'input[id=CoursecatalogAppSettingsForm_catalog_external]', function(){
		if($(this).is(':checked')){
            $('.row.extra-catalog_external.hidden').show();
			$('.extra-catalog_external').removeClass('hidden');
            $('.layout-thumbnail').show();
		} else{
			$('.extra-catalog_external').addClass('hidden');
            $('.layout-thumbnail').hide();
            $('.row.extra-catalog_external.hidden').hide();
		}
	});

	// Enable/Disable select catalogs button
	if ($('input[name=catalog_external_selected_catalogs]:checked').val() == 0) {
		$('.select-custom-catalogs').addClass('hidden');
		$('.select-custom-catalogs').addClass('disabled');
	} else {
		$('.select-custom-catalogs').removeClass('hidden');
		$('.select-custom-catalogs').removeClass('disabled');
	}

	$(document).on('change', 'input[id=catalog_external_no_selected_catalogs]', function(){
		if($(this).is(':checked')){
			$('.select-custom-catalogs').addClass('hidden');
			$('.select-custom-catalogs').addClass('disabled');

			$('input[name=selected_catalogs]').val('');
			$('span#selected_catalogs_count').html(0);
		} else{
			$('.select-custom-catalogs').removeClass('hidden');
			$('.select-custom-catalogs').removeClass('disabled');
		}
	});

	$(document).on('change', 'input[id=catalog_external_selected_catalogs]', function(){
		if($(this).is(':checked')){
			$('.select-custom-catalogs').removeClass('hidden');
			$('.select-custom-catalogs').removeClass('disabled');
		} else{
			$('.select-custom-catalogs').addClass('hidden');
			$('.select-custom-catalogs').addClass('disabled');
		}
	});
    if(!$('input[id=CoursecatalogAppSettingsForm_catalog_external]').is(':checked')){
        $('.layout-thumbnail').hide();
        $('.row.extra-catalog_external.hidden').hide();
    }

	$('#app-settings-form').submit(function(e) {
		if ($('input[name=catalog_external_selected_catalogs]:checked').val() == 1 && $('input[name=selected_catalogs]').val() == '') {
			e.preventDefault();
			Docebo.Feedback.show("error", "<?= Yii::t('catalogue', 'Please, select at least one custom catalog.'); ?>");
		}

	});

</script>