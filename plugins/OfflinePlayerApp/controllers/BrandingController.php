<?php

/**
 * Controllers that returns the branding assets
 * Class BrandingController
 */
class BrandingController extends Controller {
	
	const OFFLINE_PLAYER_BASE_FILENAME = 'OLPSetup_HS';
	const STORAGE_SUBFOLDER = 'offline_player';

	private $setupOfflinePlayerFilename = 'OLPSetup_HS.exe';

	/**
	 * Array of branding assets returned
	 * @var array
	 */
	private $brandingAssets = array (
		'splash_en_US' => array('type' => 'img', 'file' => 'splash_en_US.bmp'),
		'logo'  => array('type' => 'img', 'file' => null),
		'launcher' => array('type' => 'img', 'file' => 'launcher.gif'),
		'delete' => array('type' => 'img', 'file' => 'delete.gif'),
		'progress' => array('type' => 'img', 'file' => 'progress.gif'),
		'image_css' => array('type' => 'css', 'file' => 'image.css'),
		'branding_css' => array('type' => 'css', 'file' => 'branding.css'),
		'notification' => array('type' => 'xml', 'file' => 'LMSUpdates.xml')
	);

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		$loggedInActions = array('downloadOfflinePlayerSetup', 'downloadOfflinePlayer');

		$res[] = array (
			'allow',
			'users' => array('@'),
			'actions' => $loggedInActions
		);

		$res[] = array(
			'deny',
			'actions' => $loggedInActions
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('*'),
		);

		return $res;
	}

	/**
	 * Returns the requested branding asset via HTTP
	 */
	public function actionDownloadAsset() {
		try {
			// Check asset name requested
			$asset_name = Yii::app()->request->getParam('name');
			if(!$asset_name || !isset($this->brandingAssets[$asset_name]))
				throw new CHttpException(404, "Invalid branding asset requested");

			// Retrieve physical file to stream
			$asset = $this->brandingAssets[$asset_name];
			$logoFile = $logoPath = null;
			if(($asset_name == 'logo') || ($asset_name == 'image_css')) {
				// TODO: DOWNLOAD THE LOGO IN UPLOAD_TMP, RESIZE IT AND RETURN IT RESIZED (THEN DELETE IT)
				// We must get the logo assigned to the current LMS url
				$logoPath = Yii::app()->theme->getLogoUrl();
				if($logoPath) {
					$file_headers = @get_headers($logoPath);
					if($file_headers[0] == 'HTTP/1.1 404 Not Found')
						throw new CHttpException(404, "Cannot read logo file");
					else
						$logoFile = basename($logoPath);
				} else
					throw new CHttpException(404, "No logo available");
			}

			if($asset_name == 'logo') {
				$asset['file'] = $logoFile;
				$asset['file_content'] = file_get_contents($logoPath);
			} else {
				// Return the set of default files (independent from the LMS url)
				$asset['file_path'] = Yii::getPathOfAlias('OLP.assets_path').'/'.$asset['type'].'/'.$asset['file'];

				if(!file_exists($asset['file_path']))
					throw new CHttpException(404, "Cannot find physical file");

				$asset['file_content'] = file_get_contents($asset['file_path']);
				if($asset_name == 'image_css')
					$asset['file_content'] = str_replace("logo.gif", $logoFile, $asset['file_content']);
			}

			// Send the file content
			Yii::app()->request->sendFile($asset['file'], $asset['file_content']);

		} catch(Exception $e) {
			if(!headers_sent()) {
				$code = $e->statusCode;
				header("HTTP/1.0 ".$code." ". ($code == 404 ? "Not Found" : "Internal Server Error"));
			}
			echo $e->getMessage();
		}

		Yii::app()->end();
	}

	/**
	 * !NOT used anymore, see the similar action below. This is kept here just for awhile.
	 * Then, it should be removed
	 * 
	 * Returns the setup file for the Offline Player
	 */
	public function actionDownloadOfflinePlayerSetup() {
		try {
			$filePath = Yii::getPathOfAlias('OLP.assets_path').'/setup/'.$this->setupOfflinePlayerFilename;
			if(!file_exists($filePath))
				throw new CHttpException(404, "Cannot find setup file");

			// Send the file content
			Yii::app()->request->sendFileDirect($filePath);

		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			throw $e;
		}
	}
	
	
	/**
	 * Send Offline Player setup executable file to the browser
	 */
	public function actionDownloadOfflinePlayer() {

	    // Get the System (Shared) Assets storage, which is above all LMS storages, e.g. /files/assets, NOT /files/m/y/my_docebo_com/assets
		$storage = CFileStorage::getSystemFilesStorage(CFileStorage::COLLECTION_ASSETS);
		$language = Yii::app()->getLanguage();
		$filename = self::OFFLINE_PLAYER_BASE_FILENAME . "_" . $language . ".exe";
		
		// Try first current language, then fallback to English, then go back to NO-language EXE (current one, as of 10 Nov 2015).
		if (!$storage->fileExists($filename, self::STORAGE_SUBFOLDER)) {
			$filename = self::OFFLINE_PLAYER_BASE_FILENAME . "_en" . ".exe";
			if (!$storage->fileExists($filename, self::STORAGE_SUBFOLDER)) {
			    $filename = self::OFFLINE_PLAYER_BASE_FILENAME . ".exe";
			    if (!$storage->fileExists($filename, self::STORAGE_SUBFOLDER)) {
				    Yii::app()->end();
			    }
			}
		}
		
		// Send the file to browser
		$storage->sendFile($filename, $filename, self::STORAGE_SUBFOLDER);
		Yii::app()->end();
	}
	

}