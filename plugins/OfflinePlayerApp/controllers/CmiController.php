<?php

/**
 * Controllers that handles the CMI-based communication with the Offline player
 * Class CmiController
 */
class CmiController extends Controller {

	private static $HTTP_POST_VAR = '<?xml_version';
	private static $XML_PREFIX = 'xmlslashprefix"';

	private static $supportedMessages = array(
		'ocpInitialize', // Initialize course download
		'ocpGetPackage', // Get course content
		'ocpGetProgress', // Get user progress request
		'ocpFinish', // Finish course donwload request
		'ocpPutProgress' // Send progress data to the LMS
	);

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// Allow access to every user
		$res[] = array(
			'allow',
			'users' => array('*'),
		);

		return $res;
	}

	public function actionDownload() {

		$course_id = Yii::app()->request->getParam('course_id');
		$treeParams = array('userInfo' => false, 'scormChapters' => false, 'showHiddenObjects' => true, 'aiccChapters' => false);
		$learningObjects = LearningObjectsManager::getCourseLearningObjectsTree($course_id, $treeParams);
		$learningObjects = LearningObjectsManager::flattenLearningObjectsTree($learningObjects);

		$countScorms = 0;
		if (!empty($learningObjects)) {
			foreach ($learningObjects as $lo) {
				if ($lo['type'] === LearningOrganization::OBJECT_TYPE_SCORMORG)
					$countScorms++;
			}
		}

		$this->renderPartial('download', array(
			'learningObjects' => ($countScorms > 0) ? $learningObjects : array(),
			'countScorms' => $countScorms
		));
	}

	/**
	 * Downloads a SCORM package zipped.
	 * If the scorm is not zipped, it will compress it and leave it on S3.
	 */
	public function actionDownloadScorm() {
		try {
			$organizationId = Yii::app()->request->getParam('item_id');
			$userId = Yii::app()->request->getParam('user_id');
			$courseId = Yii::app()->request->getParam('course_id');

			if(!$organizationId || !$userId || !$courseId)
				throw new Exception("Missing required params");

			// Check user is valid
			$user = CoreUser::model()->findByPk($userId);
			if(!$user || ($user->valid == 0))
				throw new CHttpException(510, "Invalid or inactive user");

			// Check scorm is valid
			$scorm = LearningOrganization::model()->findByPk($organizationId);
			if(!$scorm || ($scorm->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG))
				throw new CHttpException(500, "Invalid or missing course id");

			// Check that user is enrolled to the course where this item belongs
			$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $courseId, 'idUser' => $userId));
			if(!$enrollment || !$enrollment->canEnterCourse())
				throw new CHttpException(500, "User is not authorized to access this course");

			// Check if there's an active olp session
			$session = OlpDownloadSessions::model()->findByAttributes(array('id_user' => $userId, 'id_org' => $organizationId));
			if(!$session || !$session->isActive())
				throw new CHttpException(500, "Olp session not valid or expired");

			// Find the scorm package
			$scormPackage = $scorm->scormOrganization->scormPackage; /* @var $scormPackage LearningScormPackage */
			$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_SCORM);
			$scormFile = $scormPackage->path.".zip";
			if(!$storageManager->fileExists($scormFile)) {
				// The zip file is not there. Let's download the scorm in upload_tmp, zip it and send it to the user
				if($storageManager->fileExists($scormPackage->path)) {
					$localFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $scormPackage->path;
					$storageManager->downloadCollectionAs($localFilePath, $scormPackage->path);
					$zipFilePath = $localFilePath.".zip";

					// Zip the folder
					$zip = new ZipArchive();
					$zip->open($zipFilePath, ZipArchive::CREATE);
					$this->_addFolderToZip($localFilePath, $zip);
					if(!$zip->close())
						throw new CHttpException(500, "Error occurred while preparing scorm package");

					if(!file_exists($zipFilePath) || !$storageManager->store($zipFilePath))
						throw new CException("Error while saving file to final storage.");

					// Remove temporary
					FileHelper::removeDirectory($localFilePath);
					FileHelper::removeFile($zipFilePath);
				} else
					throw new CHttpException(500, "Can't find scorm package");
			}

			$storageManager->sendFile($scormFile);
		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			echo $e->getMessage();
		}

		Yii::app()->end();
	}

	/** Adds an entire directory structure to the zip
	 * @param $zipFile ZipArchive
	 * @param $folder
	 */
	private function _addFolderToZip($folder, $zipFile, $subfolder = null) {
		if ($zipFile == null)
			return false; // no resource given, exit

		// we check if $folder has a slash at its end, if not, we append one
		$folderParts = str_split($folder);
		$folder .= end($folderParts) == "/" ? "" : "/";
		$subFolderParts = str_split($subfolder);
		$subfolder .= end($subFolderParts) == "/" ? "" : "/";

		// we start by going through all files in $folder
		$handle = opendir($folder);
		while ($f = readdir($handle)) {
			if ($f != "." && $f != "..") {
				if (is_file($folder . $f)) {
					// if we find a file, store it
					// if we have a subfolder, store it there
					if ($subfolder != null)
						$zipFile->addFile($folder . $f, $subfolder . $f);
					else
						$zipFile->addFile($folder . $f);
				} elseif (is_dir($folder . $f)) {
					// if we find a folder, create a folder in the zip
					$zipFile->addEmptyDir($f);
					// and call the function again
					$this->_addFolderToZip($folder . $f, $zipFile, $f);
				}
			}
		}
	}

	/**
	 * Handles all CMI requests sent to the LMS by the OLP
	 */
	public function actionIndex() {
		$errorCode = 0;
		$errorDescription = 'success';
		$messageName = null;
		$response = array();

		// Force timezone and date formats (UTC and mysql format)
		date_default_timezone_set("UTC");
		Yii::app()->user->forceTimezone("UTC");
		Yii::app()->localtime->forceMysqlDatetimeFormats();

		try {
			// Read and parse XML message
			$cmiMessage = $this->_getCmiMessage();

			// Message parsed. Get the session ID and the operation name
			$messageName = $cmiMessage->nodeName;
			$messageName = substr($messageName, 0, strrpos($messageName, "Request"));
			if(!in_array($messageName, self::$supportedMessages))
				throw new Exception("Unsupported cmi command '".$messageName."'");

			if($cmiMessage->getAttribute('sessionID'))
				list($organizationId, $userId) = explode("_", $cmiMessage->getAttribute('sessionID'));

			if(!$organizationId || !$userId)
				throw new Exception("Missing or malformed session ID");

			// Check that platform is still active
			if(Docebo::isPlatformExpired())
				throw new CHttpException(510, "LMS platform has expired");

			// Check user is valid
			$user = CoreUser::model()->findByPk($userId);
			if(!$user || ($user->valid == 0))
				throw new CHttpException(510, "Invalid or inactive user");

			// Check scorm item is valid
			$scorm = LearningOrganization::model()->findByPk($organizationId);
			if(!$scorm || ($scorm->objectType != LearningOrganization::OBJECT_TYPE_SCORMORG))
				throw new CHttpException(500, "Invalid or missing course id");

			// Check that user is enrolled to the course where this item belongs
			$courseId = $scorm->idCourse;
			$enrollment =  LearningCourseuser::model()->findByAttributes(array('idCourse' => $courseId, 'idUser' => $userId));
			if(!$enrollment || !$enrollment->canEnterCourse())
				throw new CHttpException(500, "User is not authorized to access this course");

			// Handle specific command
			switch($messageName) {
				case 'ocpInitialize':
					$response = $this->_handleCourseDownloadInitialize($user, $scorm, $courseId);
					break;
				case 'ocpGetPackage':
					$response = $this->_handleCourseDownload($user, $scorm, $courseId);
					break;
				case 'ocpGetProgress':
					$response = $this->_handleGetProgress($user, $scorm, $courseId, $enrollment);
					break;
				case 'ocpFinish':
					$response = $this->_handleFinish($user, $scorm, $courseId);
					break;
				case 'ocpPutProgress':
					$response = $this->_handlePutProgress($user, $scorm, $courseId, $cmiMessage);
					break;
			}

		} catch(CHttpException $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$errorCode = $e->statusCode;
			if($errorCode == 510)
				$errorDescription = 'inactive';
			else
				$errorDescription = 'error';

		} catch(Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			$errorCode = 500;
			$errorDescription = 'error';
		}

		// Prepare response and send it back
		if($messageName) {
			header('Content-type: text/xml');
			$responseBody = $this->_composeCmiResponse($errorCode, $errorDescription, $messageName, $response);
			echo $responseBody;
		}

		Yii::app()->end();
	}

	/**
	 * Preprocessing of the HTTP request.
	 * @return DOMElement The full XML/CMI message to be parsed
	 */
	private function _getCmiMessage() {
		$post_variable = Yii::app()->request->getPost(self::$HTTP_POST_VAR);
		if(!$post_variable)
			throw new CHttpException(500, "Invalid CMI request");

		$post_variable = "<?xml version=".$post_variable;
		$post_variable = str_replace(self::$XML_PREFIX, '"', $post_variable);

		// Try parsing the message
		$xml = new DOMDocument();
		$xml->loadXML($post_variable);
		return $xml->documentElement;
	}

	/**
	 * Adds the passed attribute to the node using DOM
	 * @param $dom DOMDocument
	 * @param $node
	 * @param $name
	 * @param $value
	 */
	private function _setNodeAttributes($dom, $node, $attributes) {
		foreach($attributes as $name => $value) {
			$attr = $dom->createAttribute($name);
			$attr->appendChild($dom->createTextNode($value));
			$node->appendChild($attr);
		}
	}

	/**
	 * Sets the node value for the passed node
	 * @param $dom DOMDocument
	 * @param $node
	 * @param $value
	 */
	private function _setNodeValue($dom, $node, $value) {
		$cdataPos = strpos($value, "<![CDATA[");
		if($cdataPos === 0) {
			$value = substr($value, 9, strlen($value) - 12);
			$valueNode = $dom->createCDATASection($value);
		}
		else
			$valueNode = $dom->createTextNode($value);

		$node->appendChild($valueNode);
	}

	/**
	 * Prepares the response CMI message
	 * @param $errorCode
	 * @param $errorDescription
	 * @param $messageName
	 * @param $message
	 */
	private function _composeCmiResponse($errorCode, $errorDescription, $messageName, $response) {
		$dom = new DOMDocument('1.0', 'utf-8');
		$dom->formatOutput = true;

		$message = $dom->createElement($messageName."Response");
		$dom->appendChild($message);

		$responseAttributes = array(
			'version' => '1.0',
			'xmlns:ocp' => 'ocp',
			'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance'
		);
		switch($messageName) {
			case '':
			default:
				$noNamespaceSchemaLocation = "responseXML.xsd";
		}
		$responseAttributes['xsi:noNamespaceSchemaLocation']  = $noNamespaceSchemaLocation;
		$this->_setNodeAttributes($dom, $message, $responseAttributes);


		// Add error message page
		$error = $dom->createElement('error');
		$message->appendChild($error);
		$error_id = $dom->createElement('ocp:id');
		$this->_setNodeValue($dom, $error_id, $errorCode);
		$error->appendChild($error_id);
		$error_description = $dom->createElement('ocp:description');
		$this->_setNodeValue($dom, $error_description, $errorDescription);
		$error->appendChild($error_description);

		if($errorCode == 0) {
			// Add the response to the body
			$this->_buildResponseBody($dom, $message, $response);
		}

		$xml = $dom->saveXML();
		return $xml;
	}

	/**
	 * @param $dom DOMDocument
	 * @param $parentNode DOMElement
	 * @param $currentElement
	 */
	private function _buildResponseBody($dom, $parentNode, $currentElement) {
		foreach($currentElement as $key => $value) {
			$arrayPos = strpos($key, "[");
			if($arrayPos !== FALSE) {
				// key is a special tag name to be used for an XML array (e.g. progress[0], progress[1])
				$key = substr($key, 0, $arrayPos);
			}

			$node = $dom->createElement($key);
			if(is_array($value))
				$this->_buildResponseBody($dom, $node, $value);
			else
				$this->_setNodeValue($dom, $node, $value);

			$parentNode->appendChild($node);
		}
	}

	/**
	 * Handles the initialize course download message
	 * @param $user CoreUser
	 * @param $scorm LearningOrganization
	 * @param $courseId
	 */
	private function _handleCourseDownloadInitialize($user, $scorm, $courseId) {

		// Reset any past record in the sessions table
		OlpDownloadSessions::model()->deleteAllByAttributes(array('id_user' => $user->idst, 'id_org' => $scorm->idOrg));

		// Create a new download session
		$session = new OlpDownloadSessions();
		$session->id_org = $scorm->idOrg;
		$session->id_user = $user->idst;
		$session->timestamp = round(microtime(true) * 1000);
		$session->ip_address = $_SERVER['REMOTE_ADDR'];
		if(!$session->save()) {
			Yii::log(var_export($session->getErrors(),true), CLogger::LEVEL_ERROR);
			throw new CHttpException(500, "Unable to save session");
		}

		return array();
	}

	/**
	 * Handles the course download message
	 * @param $user CoreUser
	 * @param $scorm LearningOrganization
	 * @param $courseId
	 */
	private function _handleCourseDownload($user, $scorm, $courseId) {
		$response = array(
			'package' => array(
				'ocp:url' => '<![CDATA['.
					Docebo::createAbsoluteUrl('lms:OfflinePlayerApp/cmi/downloadScorm', array('item_id' => $scorm->getPrimaryKey(), 'user_id' => $user->idst, 'course_id' => $courseId))
					.']]>',
				//'ocp:contentPackageSize' => 0
			)
		);

		return $response;
	}

	/**
	 * Handles the finish course download
	 * @param $user CoreUser
	 * @param $scorm LearningOrganization
	 * @param $courseId
	 */
	private function _handleFinish($user, $scorm, $courseId) {
		// Create a new download session
		$session = OlpDownloadSessions::model()->findByAttributes(array('id_user' => $user->idst, 'id_org' => $scorm->idOrg));
		$session->completed = 1;
		if($session && !$session->save()) {
			Yii::log(var_export($session->getErrors(),true), CLogger::LEVEL_ERROR);
			throw new CHttpException(500, "Unable to save session");
		}

		return array();
	}

	/**
	 * Handles the course download message
	 * @param $user CoreUser
	 * @param $scorm LearningOrganization
	 * @param $courseId
	 * @param $enrollment LearningCourseuser
	 */
	private function _handleGetProgress($user, $scorm, $courseId, $enrollment) {
		$response = array();

		// Fill in student data
		$response['student'] = array(
			'ocp:studentId'	=> $user->idst,
			'ocp:username' => Yii::app()->user->getRelativeUsername($user->userid),
			'ocp:fullname' => $user->getFullName()
		);

		// Collect all tracks for this scorm
		$criteria = new CDbCriteria();
		$criteria->addCondition('idUser = :id_user')
			->addCondition('idReference = :id_org')
			->order = 'last_access DESC';
		$criteria->params[':id_user'] = $user->idst;
		$criteria->params[':id_org'] = $scorm->idOrg;
		$scormTracks = LearningScormTracking::model()->findAll($criteria);
		$lastAccessedSco = null;
		$scormTracksByItem = array();
		foreach($scormTracks as $track) {
			/* @var $track LearningScormTracking */
			if($track->idscorm_item) {
				$scormTracksByItem[$track->idscorm_item] = $track;
				if(!$lastAccessedSco)
					$lastAccessedSco = $track->scorm_item;
			}
		}
		unset($scormTracks);

		// Fill in course data
		$response['course'] = array (
			'ocp:courseId' => $scorm->idOrg,
			'ocp:specificationType' => 'SCORM'
		);

		// LMS Handler data
		$response['course']['ocp:lmsVendorData'] = '<![CDATA[';
		$response['course']['ocp:lmsVendorData'] .= 'offlinecmi_sync_URL=/lms/index.php?r=OfflinePlayerApp/cmi/index'."\r\n";
		$response['course']['ocp:lmsVendorData'] .= 'offlinecmi_sID='.$scorm->idOrg."_".$user->idst.'_download'."\r\n";
		$response['course']['ocp:lmsVendorData'] .= 'clientTimeZone=UTC'."\r\n";
		$response['course']['ocp:lmsVendorData'] .= 'isRegistered=true'."\r\n";
		$response['course']['ocp:lmsVendorData'] .= ']]>';

		// Fill in course expiration fields (if there is a valid period for subscription)
		if ($enrollment->date_expire_validity && (strpos($enrollment->date_expire_validity, '0000-00-00') === FALSE)) {
			$utcDateExpireValidity = Yii::app()->localtime->fromLocalDateTime($enrollment->date_expire_validity);
			$utcDateExpireValidityTimestamp = strtotime($utcDateExpireValidity);
			$session = OlpDownloadSessions::model()->findByAttributes(array('id_user' => $user->idst, 'id_org' => $scorm->idOrg));
			$response['course']['ocp:crsDownloadDate'] = $session->timestamp;
			$response['course']['ocp:expirationDate'] = $utcDateExpireValidityTimestamp * 1000;
		}

		// Add SCORM 2004 specific fields
		if($scorm->scormOrganization->scormPackage->scormVersion !== '1.2') {
			$commontTrack = LearningCommontrack::model()->findByAttributes(array('idUser' => (int)$user->idst, 'idReference' => (int) $scorm->idOrg));
			$response['course']['ocp:contentscore'] = $commontTrack ? $commontTrack->score : '';
			$response['course']['ocp:contentstatus'] = $commontTrack ? $commontTrack->status : '';
			$response['course']['ocp:latestScoId'] = ($lastAccessedSco) ? $lastAccessedSco->item_identifier : '';

		}

		// Iterate over all sco items and collect cmi trackings
		$response['progressCollection'] = array();
		$scormItems = $scorm->scormOrganization->getItems();
		foreach($scormItems as $index => $scormItem) {
			/* @var $scormItem LearningScormItems */
			if(isset($scormTracksByItem[$scormItem->idscorm_item])) {
				$tracking = $scormTracksByItem[$scormItem->idscorm_item];
				if(!$tracking->xmldata)
					continue;

				$cmiData = '<![CDATA[';
				$cmiRecordsDataProvider = $tracking->getTrackingArray();
				$rows = $cmiRecordsDataProvider->getData();
				foreach ($rows as $row)
					$cmiData .= $row['cmi']."=".$row['title']."\r\n";
				$cmiData .= ']]>';

				$response['progressCollection']['progress['.$index.']'] = array(
					'ocp:courseElementId' => $scormItem->item_identifier,
					'ocp:data' => $cmiData
				);
			}
		}

		return $response;
	}

	/**
	 * Handles the put sco progress message
	 * @param $user CoreUser
	 * @param $scorm LearningOrganization
	 * @param $courseId
	 * @param $cmiMessage DOMElement
	 */
	private function _handlePutProgress($user, $scorm, $courseId, $cmiMessage) {
		$response = array();
		$scorm_package = $scorm->scormOrganization->scormPackage; /* @var $scorm_package LearningScormPackage */

		$progressCollectionElement = $cmiMessage->getElementsByTagName('progressCollection');
		if($progressCollectionElement && $progressCollectionElement->length > 0) {
			$progressElements = $progressCollectionElement->item(0)->childNodes;
			for($i=0; $i<$progressElements->length; $i++) {
				$progressElement = $progressElements->item($i); /* @var $progressElement DOMElement */
				if($progressElement instanceof DOMElement) {
					$scoIdentifier = $progressElement->getElementsByTagNameNS('ocp', 'courseElementId')->item(0)->nodeValue;
					$olpScoTimestamp = $progressElement->getElementsByTagNameNS('ocp', 'dateTime')->item(0)->nodeValue;

					// Check scorm item
					$scormItem = LearningScormItems::model()->findByAttributes(array('idscorm_organization' => $scorm->scormOrganization->idscorm_organization, 'item_identifier' => $scoIdentifier));
					if(!$scormItem)
						throw new CHttpException(500, "Missing scorm item");

					// Initialize scorm api
					if ($scorm_package->scormVersion == '1.2')
						$scormapi = new ScormApi12($scorm->idOrg);
					else
						$scormapi = new ScormApi2004($scorm->idOrg);

					// Check if we already have a track for this sco
					$scoTrack = LearningScormTracking::model()->findByAttributes(array('idUser' => $user->idst, 'idscorm_item' => $scormItem->idscorm_item));
					/* @var $scoTrack LearningScormTracking */
					if(!$scoTrack) {
						// create a track and initialize it
						$scormapi->Initialize($user->idst, $scormItem);
						$scoTrack = LearningScormTracking::model()->find('idUser=:idUser AND idscorm_item=:idscorm_item', array(
							':idUser' => $user->idst,
							':idscorm_item' => $scormItem->idscorm_item
						));
					}

					// Get timestamp (convert last_access date to MYSQL UTC -> to Unix timestamp)
					$scoTimestamp = strtotime(Yii::app()->localtime->fromLocalDateTime($scoTrack->last_access));
					if(!$scoTimestamp || $scoTimestamp < $olpScoTimestamp) {
						$cmiData = explode("progressdelim", $progressElement->getElementsByTagNameNS('ocp', 'data')->item(0)->nodeValue);
						$cmi = array();
						foreach($cmiData as $data) {
							// Parse the cmi line
							$commandBegin = strpos($data, "cmi.");
							if($commandBegin !== FALSE) {
								list($key, $value) = explode("=", substr($data, $commandBegin));
								if(strtolower($key) == "cmi.suspend_data")
									$value = urldecode($value);
								$cmi[trim($key)] = trim($value);
							}
						}

						$scormapi->Finish($user->idst, $scormItem, $cmi, true);
					}
				}
			}
		}

		return $response;
	}
}