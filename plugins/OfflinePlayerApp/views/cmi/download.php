<?php
/** @var $this CmiController */
/** @var $learningObjects [] */
/** @var $countScorms int */
?>

<style type="text/css">
	.modal.play-offline-modal {
		width: 820px;
		margin-left: -410px;
	}
	.play-offline-modal .warning-msg {
		background: #FDE79A;
		text-align: center;
		padding: 8px 0;
		font-size: 1.2em;
		margin-bottom: 30px;
	}
	.play-offline-modal .olp-option-dld,
	.play-offline-modal .olp-option-help {
		text-align: center;
	}
	.play-offline-modal .olp-option-dld > div,
	.play-offline-modal .olp-option-help > div {
		text-align: center;
		width: 58%;
		margin: 20px auto;
	}
	.play-offline-modal .olp-option-dld.locked a {
		cursor: default;
		opacity: 0.3;
	}
	.play-offline-modal .unavailable-msg {
		background: #F9F9F9;
		border: 1px solid #EBEBEB;
		padding: 5px 10px;
		margin: 20px 0;
	}
	.play-offline-modal .lo-list {
		border-top: 1px solid #ECECEC;
	}
	.play-offline-modal .lo-list > li {
		border-bottom: 1px solid #ECECEC;
		padding: 6px 0;
	}
	.play-offline-modal .lo-list > li > i,
	.play-offline-modal .lo-list > li > a {
		float: right;
		margin-left: 10px;
	}
	.play-offline-modal .lo-list > li > span {
		float: right;
	}

	.play-offline-modal .help-list {}
	.play-offline-modal .help-list > li {
		position: relative;
		padding: 0 15px 0 50px;
		border: 1px solid #ECECEC;
		margin-bottom: 15px;
		min-height: 34px;
	}
	.play-offline-modal .help-list .help-index {
		display: block;
		position: absolute;
		top: 0; left: 0;
		font-size: 20px;
		font-weight: bold;
		height: 34px;
		width: 34px;
		text-align: center;
		line-height: 34px;
		padding: 0;
	}
	.play-offline-modal .help-list .help-text {
		display: block;
		padding: 7px 0;
	}
	.play-offline-modal .help-list .help-text a {
		color: #005EAB;
	}
	.play-offline-modal .help-list .help-text a:hover {
		color: #005EAB;
		border-bottom: 1px solid #005EAB;
		text-decoration: none;
	}
	.play-offline-modal .help-list > li > i {}
</style>

<h1><?= Yii::t('offlineplayer', 'Download for offline play') ?></h1>

<div class="warning-msg">
	<i class="i-sprite is-solid-exclam large"></i>
	<?= Yii::t('offlineplayer', 'You need to install the <strong>Offline Player</strong> to play your course offline') ?>
</div>

<div class="row-fluid">
	<div class="span6">
		<div class="olp-option-dld">
			<i class="offline-player-sprite olp-dld-large-icon"></i>
			<div class="text-colored-green"><?= Yii::t('offlineplayer', 'I already have installed the offline player on my computer') ?></div>

			<?php
			$downloadBtnHtml = CHtml::tag('a', array('href'=>'#', 'class'=>'btn-docebo green big olp-lo-toggle'), Yii::t('offlineplayer', 'Download the course'));
			if ($countScorms == 1) {
				foreach ($learningObjects as $lo) {
					if ($lo['type']===LearningOrganization::OBJECT_TYPE_SCORMORG) {
						$downloadUrl = Yii::app()->getModule('OfflinePlayerApp')->getScormDownloadUrl($lo['idOrganization']);
						$downloadBtnHtml = CHtml::tag('a', array('href'=>$downloadUrl, 'target'=>'_blank', 'class'=>'btn-docebo green big'), Yii::t('offlineplayer', 'Download the course'));
					}
				}
			}
			echo $downloadBtnHtml;
			?>
		</div>
	</div>
	<div class="span6">
		<div class="olp-option-help">
			<i class="offline-player-sprite olp-qmark-large-icon"></i>
			<div><?= Yii::t('offlineplayer', 'I haven\'t installed the offline player on my computer') ?></div>
			<a href="#" class="btn-docebo black big" id="olp-help-toggle"><?= Yii::t('offlineplayer', 'Show me how to do') ?></a>
		</div>
	</div>
</div>

<br/>
<br/>

<div class="help-container hide">
	<ul class="help-list">
		<li>
			<span class="help-index btn-docebo black">1</span>
			<span class="help-text">
				<?= Yii::t('offlineplayer', '{click_here} to download the offline player installer (Windows only) and install it on your computer', array(
					'{click_here}' => CHtml::link(Yii::t('offlineplayer', 'Click here'), Yii::app()->getModule('OfflinePlayerApp')->getDownloadUrl(), array('target'=>'_blank'))
				)) ?>
			</span>
		</li>
		<li>
			<span class="help-index btn-docebo black">2</span>
			<span class="help-text">
				<?= Yii::t('offlineplayer', 'Once installed, right click on the player\'s icon in the Windows taskbar and choose <strong>Offline Player Settings</strong>') ?>
			</span>
			<div class="text-right">
				<i class="offline-player-sprite olp-hint-1"></i>
			</div>
		</li>
		<li>
			<span class="help-index btn-docebo black">3</span>
			<span class="help-text">
				<?= Yii::t('offlineplayer', 'Copy and paste this url: {url} (that\'s your lms) in the LMS URL field and click <strong>OK</strong>.', array(
					'{url}' => CHtml::tag('strong',array(),Yii::app()->request->getHostInfo())
				)) ?>
			</span>
			<div class="text-right">
				<i class="offline-player-sprite olp-hint-2"></i>
			</div>
		</li>
		<li>
			<span class="help-index btn-docebo green">4</span>
			<span class="help-text text-colored-green">
				<strong>
					<?= Yii::t('offlineplayer', 'You are now ready to download the course to your computer!') ?>
				</strong>
			</span>
		</li>
	</ul>
	<div class="text-right">
		<?= $downloadBtnHtml ?>
	</div>
</div>

<div class="lo-list-container hide">
	<h4><?= Yii::t('standard', 'Training materials') ?>:</h4>

	<div class="unavailable-msg">
		<?= Yii::t('offlineplayer', 'Objects marked with <i class="offline-player-sprite olp-unavailable-icon"></i> are not available for offline play. You will be able to play them when an internet connection is available.') ?>
	</div>

	<?php if ($countScorms > 0) : ?>
	<ul class="lo-list">
		<?php foreach ($learningObjects as $lo) : ?>
		<li>
			<?= trim($lo['title']) ? $lo['title'] : Yii::t('standard', '_NOTITLE') ?>
			<?php if ($lo['type'] === LearningOrganization::OBJECT_TYPE_SCORMORG) : ?>
				<a href="<?= Yii::app()->getModule('OfflinePlayerApp')->getScormDownloadUrl($lo['idOrganization']) ?>" target="_blank"><i class="offline-player-sprite olp-dld-icon"></i></a>
			<?php else: ?>
				<i class="offline-player-sprite olp-unavailable-icon"></i>
			<?php endif; ?>
		</li>
		<?php endforeach; ?>
	</ul>
	<?php endif; ?>

	<br/>
	<div class="text-right">
		<button class="btn-docebo black big close-dialog"><?= Yii::t('standard', '_CLOSE') ?></button>
	</div>
</div>

<script type="text/javascript">
	$(document).delegate(".play-offline-modal", "dialog2.content-update", function() {
		var e = $(this);
		var $modal = e.find('.modal-body');

		var autoclose = e.find("a.auto-close");

		if (autoclose.length > 0) {
			e.find('.modal-body').dialog2("close");

			var href = autoclose.attr('href');
			if (href) {
				window.location.href = href;
			}
		}
		else {

			$modal.find('.olp-lo-toggle').unbind('click').click(function() {
				$modal.find('.lo-list-container').show();
				$modal.find('.olp-option-dld').addClass('locked');
				$modal.find('.help-container').hide();
				$modal.find('#olp-help-toggle').removeClass('active');
				$modal.css({maxHeight: (400 + $modal.find('.lo-list-container').height())+'px'});
				return false;
			});

			$modal.find('#olp-help-toggle').unbind('click').click(function() {
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$modal.find('.olp-option-dld').removeClass('locked');
					$modal.find('.help-container').hide();
					$modal.css({maxHeight: (400 + $modal.find('.lo-list-container').height())+'px'});
				} else {
					$(this).addClass('active');
					$modal.find('.olp-option-dld').addClass('locked');
					$modal.find('.lo-list-container').hide();
					$modal.find('.help-container').show();
					$modal.css({maxHeight: (400 + $modal.find('.help-container').height())+'px'});
				}
				return false;
			}).removeClass('active');

			$modal.find('.close-dialog').unbind('click').click(function() {
				e.find('.modal-body').dialog2("close");
				return false;
			});
		}
	});
</script>