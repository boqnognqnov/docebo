<?php

	/* @var $module OfflinePlayerAppModule */
	$module = Yii::app()->getModule('OfflinePlayerApp');
?>
<div class="text-right" id="course-download-button" <?php if(!$module->canShowDownloadLink()): ?>style="display: none;" <?php endif; ?>>
	<?php
	$url = Docebo::createAppUrl('lms:OfflinePlayerApp/cmi/download', array('course_id'=>$course_id));
	echo CHtml::link(Yii::t('offlineplayer', 'Play Offline'), $url, array(
		'data-dialog-class' => 'play-offline-modal',
		'class' => 'open-dialog btn-docebo green big',
		'rel'  => 'download-offline',
		Yii::t('offlineplayer', 'Play Offline'),
		'title' => '',
	));
	?>
</div>

<script type="text/javascript">
	function updateDownloadOlpButton() {
		var $scormCount = 0;
		$.each(Arena.learningObjects.management.tree[0].children, function(index,value) {
			if(value.type == 'scormorg')
				$scormCount++;
		});

		if($scormCount > 0)
			$('#course-download-button').show();
		else
			$('#course-download-button').hide();
	}

	$(document).on('lo_list_refresh', function () {
		updateDownloadOlpButton();
	});
</script>
