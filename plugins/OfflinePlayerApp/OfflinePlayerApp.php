<?php

/**
 * Plugin for OfflinePlayerApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class OfflinePlayerApp extends DoceboPlugin {
	
	// Override parent 
	public static $HAS_SETTINGS = false;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'OfflinePlayerApp';

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();

		$sql = "
-- -----------------------------------------------------
-- Table `olp_download_sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `olp_download_sessions` (
  `id_org` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `timestamp` VARCHAR(255) NOT NULL,
  `ip_address` VARCHAR(20) NOT NULL,
  `completed` tinyint(1) NOT NULL default 0,
  PRIMARY KEY (`id_org`, `id_user`),
  CONSTRAINT `fk_olp_core_user`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_olp_learning_organization`
    FOREIGN KEY (`id_org`)
    REFERENCES `learning_organization` (`idOrg`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
";

		$command = Yii::app()->db->createCommand($sql);
		$command->execute();
	}


	public function deactivate() {
		parent::deactivate();
	}


}
