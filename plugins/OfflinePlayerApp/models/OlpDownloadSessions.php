<?php

/**
 * This is the model class for table "olp_download_sessions".
 *
 * The followings are the available columns in table 'olp_download_sessions':
 * @property integer $id_org
 * @property integer $id_user
 * @property string $timestamp
 * @property string $ip_address
 * @property boolean $completed
 */
class OlpDownloadSessions extends CActiveRecord
{
	public static $SESSION_DURATION = 60000; // 60 seconds

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'olp_download_sessions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_org, id_user, timestamp, ip_address', 'required'),
			array('id_org, id_user, completed', 'numerical', 'integerOnly'=>true),
			array('timestamp', 'length', 'max'=>255),
			array('ip_address', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_org, id_user, timestamp, ip_address, completed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_org' => 'Id Org',
			'id_user' => 'Id User',
			'timestamp' => 'Timestamp',
			'ip_address' => 'Ip Address',
			'completed' => 'Download session completed'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_org',$this->id_org);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('ip_address',$this->ip_address,true);
		$criteria->compare('completed',$this->completed,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OlpDownloadSessions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks if the current session is still valid:
	 * - not expired
	 * - the IP address is the same starting the session
	 */
	public function isActive() {
		$currentTimestamp = round(microtime(true) * 1000);
		return ($_SERVER['REMOTE_ADDR'] == $this->ip_address) && ($currentTimestamp - $this->timestamp <= self::$SESSION_DURATION);
	}

}