<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */

class OfflinePlayerAppModule extends CWebModule {

	/**
	 * @var string Assets url for this module
	 */
	private $_assetsUrl;

	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('OfflinePlayerApp.assets'));
		}
		return $this->_assetsUrl;
	}

	/**
	 * Initialization method
	 */
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->setImport(array(
				'OfflinePlayerApp.models.*',
				'OfflinePlayerApp.components.*',
		));

		Yii::setPathOfAlias('OLP.assets_path', dirname(__FILE__)."/assets/");

        if(Yii::app() instanceof CWebApplication){
            $action = Yii::app()->request->getParam('r', '');
            if($action == 'OfflinePlayerApp/cmi/index')
                Yii::app()->request->noCsrfValidationRoutes[] = 'OfflinePlayerApp/cmi/index';

            if (!Yii::app()->request->isAjaxRequest) {
                if(strpos($action, 'player') !== FALSE) {
                    $cs = Yii::app()->getClientScript();
                    $cs->registerCssFile($this->assetsUrl.'/css/olp.css');
                }
            }
        }
	}

	/**
	 * Returns the Offline Player Download URL
	 */
	public function getDownloadUrl() {
		return Docebo::createAppUrl('lms:OfflinePlayerApp/branding/downloadOfflinePlayer');
	}

	/**
	 * Checks if the current course in the player has at least one SCORM object
	 */
	public function canShowDownloadLink() {
		$course = Yii::app()->player->course;
		if($course) {
			$scormAmount = LearningOrganization::model()->countByAttributes(array('idCourse' => $course->idCourse, 'objectType' => LearningOrganization::OBJECT_TYPE_SCORMORG));
			return $scormAmount > 0;
		}

		return false;
	}

	/**
	 * Returns the download URL for a certain SCORM
	 * @param $idOrg
	 */
	public function getScormDownloadUrl($idOrg) {
		if(Yii::app()->user->getIsGuest())
			return null;

		return 'http://localhost:4444/Download?offlinecmi_SID='.$idOrg.'_'.Yii::app()->user->id.'&offlinecmi_URL='.
			CHtml::encode(Docebo::createAbsoluteUrl('lms:OfflinePlayerApp/cmi/index'));
	}

}
