<?php


class CmiApiTest extends \Codeception\TestCase\Test
{
	private $HTTP_POST_VAR = '<?xml_version';

   /**
    * @var \UnitTester
    */
    protected $tester;

    protected function _before() {

    }

    protected function _after() {

    }

    // tests
    public function testInitializeCourseDownload() {
        $this->tester->sendPOST('lms/index.php?r=OfflinePlayerApp/cmi/index', array(
			$this->HTTP_POST_VAR => '"1.0" encoding="UTF-8"?>
<ocpInitializeRequest sessionID="12_12303" version="1.0"  xmlns:ocp="ocp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ocpInitializeRequest.xsd"/>',
        ));
    }

	public function testCourseDownload() {
		$this->tester->sendPOST('lms/index.php?r=OfflinePlayerApp/cmi/index', array(
			$this->HTTP_POST_VAR => '"1.0" encoding="UTF-8"?>
<ocpGetPackageRequest sessionID="12_12303"  version="1.0" xmlns:ocp="ocp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ocpGetPackageRequest.xsd"/>',
		));
	}

	public function testGetProgress() {
		$this->tester->sendPOST('lms/index.php?r=OfflinePlayerApp/cmi/index', array(
			$this->HTTP_POST_VAR => '"1.0" encoding="UTF-8"?>
<ocpGetProgressRequest sessionID="12_12303" version="1.0" xmlns:ocp="ocp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ocpGetProgressRequest.xsd"/>',
		));
	}

	public function testFinish() {
		$this->tester->sendPOST('lms/index.php?r=OfflinePlayerApp/cmi/index', array(
			$this->HTTP_POST_VAR => '"1.0" encoding="UTF-8"?>
<ocpFinishRequest sessionID="12_12303" version="1.0"  xmlns:ocp="ocp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="ocpFinishRequest.xsd"/>',
		));
	}

	public function testPutProgress() {
		$this->tester->sendPOST('lms/index.php?r=OfflinePlayerApp/cmi/index', array(
			$this->HTTP_POST_VAR => '"1.0xmlslashprefix" encoding=xmlslashprefix"UTF-8xmlslashprefix"?><ocpPutProgressRequest sessionID=xmlslashprefix"12_12303xmlslashprefix" version=xmlslashprefix"1.0xmlslashprefix" xmlns:ocp=xmlslashprefix"ocpxmlslashprefix" xmlns:xsi=xmlslashprefix"http://www.w3.org/2001/XMLSchema-instancexmlslashprefix" xsi:noNamespaceSchemaLocation=xmlslashprefix"ocpPutProgressRequest.xsdxmlslashprefix"><student><ocp:studentId>12313</ocp:studentId><ocp:username>rashmi</ocp:username><ocp:password> </ocp:password></student><course> <ocp:courseId>18</ocp:courseId><ocp:specificationType> SCORM</ocp:specificationType><ocp:expirationDate>1414799999000</ocp:expirationDate><ocp:lmsVendorData></ocp:lmsVendorData></course><progressCollection><progress><ocp:courseElementId>S110001</ocp:courseElementId><ocp:dateTime>1414487694511</ocp:dateTime><ocp:lmsVendorData></ocp:lmsVendorData><ocp:data><![CDATA[[CORE]cmi.core.student_id=rashmiprogressdelimcmi.core.student_name=Kulkarni,Rashmiprogressdelimcmi.core.lesson_location=4-1progressdelimcmi.core.credit=creditprogressdelimcmi.core.lesson_status=incompleteprogressdelimcmi.core.session_time=00:00:24progressdelimcmi.core.total_time=00:45:52.00progressdelim[SCORE]cmi.core.score.raw=40progressdelim[STUDENT_DATA][OBJECTIVES]_count=8progressdelim[OBJECTIVE]cmi.objectives.0.id=cap_1progressdelimcmi.objectives.0.status=completedprogressdelim[OBJECTIVE]cmi.objectives.1.id=cap_2progressdelimcmi.objectives.1.status=completedprogressdelim[OBJECTIVE]cmi.objectives.2.id=cap_3progressdelimcmi.objectives.2.status=completedprogressdelim[OBJECTIVE]cmi.objectives.3.id=cap_4progressdelimcmi.objectives.3.status=incompleteprogressdelim[OBJECTIVE]cmi.objectives.4.id=cap_5progressdelim[OBJECTIVE]cmi.objectives.5.id=ej_1progressdelimcmi.objectives.5.score.raw=100progressdelim[OBJECTIVE]cmi.objectives.6.id=pr_1_1progressdelimcmi.objectives.6.score.raw=0progressdelim[OBJECTIVE]cmi.objectives.7.id=ej_2progressdelimcmi.objectives.7.score.raw=0progressdelim[STUDENT_PREFERENCE]cmi.student_preference.audio=0progressdelimcmi.student_preference.language=enprogressdelimcmi.student_preference.speed=0progressdelimcmi.student_preference.text=0progressdelim[INTERACTIONS][OTHERS]cmi.suspend_data=capsStats%3D0%25252D0%25253D0%25253B4%25252D0%25253D1%25253B3%25252D0%25253D1%25253B1%25252D1%25253D2%25253B2%25252D1%25253D2%25253B2%25252D2%25253D2%25253B3%25252D1%25253D2%25253B3%25252D2%25253D2%25253B4%25252D1%25253D1%25253B0%25252D0%25253D0%26ejs%3D0%25253D0%25253B1%25253D100progressdelim]]></ocp:data></progress></progressCollection></ocpPutProgressRequest>',
		));
	}
}