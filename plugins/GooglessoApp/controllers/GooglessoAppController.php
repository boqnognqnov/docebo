<?php

class GooglessoAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:


		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		// Allow actions to anonymous users
		$res[] = array(
			'allow',
			'users' => array('*'),
			'actions' => array('login'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionSettings() {
		$settings = new GooglessoAppSettingsForm();
		$settings->googlesso_consumer_key = Settings::get('googlesso_consumer_key');
		$settings->googlesso_consumer_secret = Settings::get('googlesso_consumer_secret');

		if (isset($_POST['GooglessoAppSettingsForm'])) {
			$settings->setAttributes($_POST['GooglessoAppSettingsForm']);

			if ($settings->validate()) {

				Settings::save('googlesso_consumer_key', $settings->googlesso_consumer_key);
				Settings::save('googlesso_consumer_secret', $settings->googlesso_consumer_secret);

				Yii::app()->user->setFlash('success', Yii::t('standard', "_OPERATION_SUCCESSFUL"));
			}
			else {
				Yii::app()->user->setFlash('error', Yii::t('standard', "_OPERATION_FAILURE"));
			}
		}

		$lmsDomain = str_ireplace(array('http://', 'https://'), '', rtrim(Docebo::getCurrentDomain(), '/'));
		$redirectUrl = 'http'.(Yii::app()->request->isSecureConnection ? 's':'').'://'.$lmsDomain.'/hybridauth/?hauth.done=Google';

		$this->render('settings', array('settings' => $settings, 'redirectUrl'=>$redirectUrl));
	}

	/**
	 * Initiate a Google+ sign in request using HybridAuth.
	 * If the Google SSO app is not configured (consumer key and secret not set),
	 * an error is logged and the user is redirected back to the login screen.
	 *
	 * Under the hood: On first visit HybridAuth below checks if authorized, and because not -
	 * redirects to our generic endpoint (lmsUrl.com/hybridauth/) for authorization.
	 * The endpoint redirects to the vendor website for user consent. If user approves
	 * the vendor website redirects back to the endpoint URL with callback parameters,
	 * which are checked by the endpoint and if everything is OK, the endpoint redirects
	 * one more time, but this time to the URL that initially started it all (that is -
	 * the action below). And since now the authorization is ok (saved in session),
	 * the HybridAuth authentication below will succeed and log the user in to the platform
	 */
	public function actionLogin(){

		// Read auth mode. Possible auth modes:
		// - "session" (default): LMS login session
		// - "oauth2": generates and returns an OAuth2 session (used only by webapp)
		$authentication_mode = Yii::app()->session['sso_auth_mode'];
		if(!$authentication_mode)
			$authentication_mode = Yii::app()->request->getParam('auth_mode', 'session');
		Yii::app()->session['sso_auth_mode'] = $authentication_mode;

        // Check if the use is Coming from Hydra Frontend
        $isComingFromHydra = isset($_GET['sso_auth_mode']) && $_GET['sso_auth_mode']  == 'oauth2' && isset($_GET['sso_target']) && $_GET['sso_target'] == 'hydra';
        $isComingFromHydra = $isComingFromHydra || Yii::app()->legacyWrapper->hydraFrontendEnabled();

        $mobile = false;
        if ( isset($_SERVER) && is_array($_SERVER) ) {
            if ( preg_match("/(?:mobile)/i", $_SERVER['HTTP_REFERER']) )
                $mobile = true;
        }
		Yii::import('common.vendors.hybridauth.Hybrid.Auth',1);
		Yii::import('common.vendors.hybridauth.Hybrid.Endpoint',1);

		// Check if the app is not correctly configured yet
		if(!Settings::get('googlesso_consumer_key') || !Settings::get('googlesso_consumer_secret')){
			Yii::log('Google SSO app consumer key or secret not defined. Unable to login user.', CLogger::LEVEL_ERROR);

            // Check from where is coming the User and redirect him accordingly
            if ( $isComingFromHydra ) {
                $url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                $this->redirect($url);
            } else if($authentication_mode == 'oauth2') {
				$response = array('success' => false, 'message' => "Google SSO app consumer key or secret not defined. Unable to login user");
				header("Location: " . Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
			} else {
                Yii::app()->user->setFlash('error', Yii::t("login", '_NOACCESS'));
                $url = Docebo::createLmsUrl('site/index');
				if ($mobile) {
                    $url = Docebo::createMobileUrl('site/index');
				}
                $this->redirect($url);
			}
		}

		$lmsDomain = str_ireplace(array('http://', 'https://'), '', rtrim(Docebo::getCurrentDomain(), '/'));

		// "?hauth.done=Google" will be appended to this URL during redirect
		// See: http://hybridauth.sourceforge.net/userguide/IDProvider_info_Google.html
		// Param: "Callback URL"
		$baseUrl = 'http'.(Yii::app()->request->isSecureConnection ? 's':'').'://'.$lmsDomain.'/hybridauth/';

		$config = array(
			// "base_url" the url that point to HybridAuth Endpoint (where the index.php and config.php are found)
			 "base_url" => $baseUrl,
			 "providers" => array (
				 "Google" => array (
					 'keys' => array ( "id" => Settings::get('googlesso_consumer_key'), "secret" => Settings::get('googlesso_consumer_secret') ),
					 'scope'=>'email',
					 'access_type'=> 'online'
				 )
			 )
		);

		if(isset(Yii::app()->params['hybridauthConfig']))
			$config = CMap::mergeArray($config, Yii::app()->params['hybridauthConfig']);

		try{

			// hybridauth EP
			$hybridauth = new Hybrid_Auth( $config );

			// automatically try to login with Google
			$google = $hybridauth->authenticate( "Google" );

			// boolean
			$is_user_logged_in = $google->isUserConnected();

			// get the user profile
			$user_profile = $google->getUserProfile(); /* @var $user_profile Hybrid_User_Profile */

			$email = $user_profile->emailVerified;

			$userModel = CoreUser::model()->findByAttributes(array( 'email' => $email ));

			if($userModel) {
				// User with that email found in database
				// Search for the user in DB and authenticate him without password
				$ui = new DoceboUserIdentity($userModel->userid, null);

				if ($ui->authenticateWithNoPassword()) {
					Yii::app()->user->login($ui);
                    if ( $isComingFromHydra ) {
                        /** Since he is already Authenticated, Create Token for him and Return him back to Hydra Frontend */
                        $server = DoceboOauth2Server::getInstance();
                        $server->init();
                        $token = $server->createAccessToken("hydra_frontend", $ui->getId(), "api");
                        $url = Docebo::createHydraUrl('learn', 'signin', array_merge(array("type" => "oauth2_response"),$token) );
                        $this->redirect($url);
                    } else if($authentication_mode == 'oauth2') {
						unset(Yii::app()->session['sso_auth_mode']);
						$server = DoceboOauth2Server::getInstance();
						if($server) {
							$server->init();
							$token = $server->createAccessToken("mobile_app", $ui->getId(), "webapp");
							if($token)
								$response = array('success' => true, 'token' => $token);
							else
								$response = array('success' => false, 'message' => "Could not create Oauth2 token");
						} else
							$response = array('success' => false, 'message' => "Could not initialize Oauth2 server component");

						header("Location: " . Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
					} else {
						Yii::app()->user->login($ui);

						// if the authentication goes well we need to check for a suspended installation process
						Docebo::resumeSaasInstallation();

						// Determine which URL to jump to
						if (!$mobile) $redirect_url = Docebo::getUserAfetrLoginRedirectUrl();
						else $redirect_url = Yii::app()->createMobileUrl('site/index', array('login' => 1));
						// *** HOOK FOR PLUGINS ***
						$loginEvent = new DEvent($this, array('redirect_url' => &$redirect_url));
						Yii::app()->event->raise('ActionSsoLogin', $loginEvent);

						// Immediately invalidate the Google SSO session to avoid security issues
						// We already know the user's email address and that's all we need
						$google->logout();

						// Finally, redirect!!
						$this->redirect($redirect_url);
					}
				} else {
					$google->logout();
					$ui->logFailedLoginAttempt();
                    // Check if the user is coming from Hydra and Redirect him accordingly
                    if( $isComingFromHydra ) {
                        $url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                        $this->redirect($url);
                    }

					throw new Exception("Invalid username or user suspended", 2);
				}
			}else{
				$google->logout();
                // Check if the user is coming from Hydra and Redirect him accordingly
                if( $isComingFromHydra ) {
                    $url = Docebo::createHydraUrl('learn', 'signin', array( "type" => 'oauth2_response', 'error' => Yii::t('login', '_NOACCESS')) );
                    $this->redirect($url);
                }

				throw new Exception("Invalid username or user suspended", 2);
			}
		}
		catch( Exception $e ){
			// In case we have errors 6 or 7, then we have to use Hybrid_Provider_Adapter::logout() to
			// let hybridauth forget all about the user so we can try to authenticate again.

			// Display the recived error,
			// to know more please refer to Exceptions handling section on the userguide
			$errMessage = null;
			switch( $e->getCode() ){
				case 0 : $errMessage = "Unspecified error."; break;
				case 1 : $errMessage = "Hybridauth configuration error."; break;
				case 2 : $errMessage = "Provider not properly configured."; break;
				case 3 : $errMessage = "Unknown or disabled provider."; break;
				case 4 : $errMessage = "Missing provider application credentials."; break;
				case 5 : $errMessage = "Authentication failed. "
					. "The user has canceled the authentication or the provider refused the connection.";
					break;
				case 6 : {
                    $errMessage = "User profile request failed. Most likely the user is not connected "
                        . "to the provider and he should to authenticate again.";
                    if( method_exists($google, 'logout') ) {
                        $google->logout();
                    }
                } break;
				case 7 : {
                    $errMessage = "User not connected to the provider.";
                    if( method_exists($google, 'logout') ) {
                        $google->logout();
                    }
                } break;
				case 8 : $errMessage = "Provider does not support this feature."; break;
			}

			if($errMessage){
				Yii::log('Google HybridAuth SSO error: ' . $errMessage, CLogger::LEVEL_ERROR);
			}
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);

            // Check if the user is coming from Hydra and Redirect him accordingly
            if( $isComingFromHydra ) {
                $url = Docebo::createHydraUrl('learn', 'signin', array( "type" =>'oauth2_response', 'error' => ($errMessage ? $errMessage : Yii::t('login', '_NOACCESS')) ));
                $this->redirect($url);
            } else if($authentication_mode == 'oauth2') {
				$response = array('success' => false, 'message' => $errMessage);
				header("Location: " . Docebo::getWebappUrl(true) . '?oauth2_response=' . urlencode(CJSON::encode($response)));
			} else {
                Yii::app()->user->setFlash('error', Yii::t("login", '_NOACCESS'));
                $url = Docebo::createLmsUrl('site/index');
                if ($mobile) {
                    $url = Docebo::createMobileUrl('site/index');
                }
                $this->redirect($url);
			}
		}
	}

}
