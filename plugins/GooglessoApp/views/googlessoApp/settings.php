<?php
/* @var $form CActiveForm */
/* @var $settings GooglessoAppSettingsForm */
?>
<style type="text/css">
	.redirect-url{
		line-height: 28px;
	}
</style>

<a href="<?= ('it' == Yii::app()->getLanguage()) ? 'https://www.docebo.com/it/knowledge-base/come-attivare-e-configuarare-lapp-gmail/' : 'https://www.docebo.com/knowledge-base/activate-manage-gmail-app/' ?>" target="_blank" class="app-link-read-manual"><?= Yii::t('apps', 'Read Manual'); ?></a>
<h2><?=Yii::t('standard','Settings') ?>: Google SSO</h2>
<br>

<?php DoceboUI::printFlashMessages(); ?>


<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'app-settings-form',
	'htmlOptions'=>array('class' => 'form-horizontal'),
));
?>

	<div class="error-summary"><?php echo $form->errorSummary($settings); ?></div>

<div class="control-group">
    <?=CHtml::label(Yii::t('standard', 'Website'), false, array('class'=>'control-label'));?>
    <div class="controls">
        <a class="app-settings-url" href="https://code.google.com/apis/console" target="_blank">https://code.google.com/apis/console</a>
    </div>
</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'googlesso_consumer_key', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'googlesso_consumer_key');?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'googlesso_consumer_secret', array('class'=>'control-label'));?>
		<div class="controls">
			<?=$form->textField($settings, 'googlesso_consumer_secret');?>
		</div>
	</div>

	<div class="control-group">
		<?=$form->labelEx($settings, 'googlesso_redirect_uri', array('class'=>'control-label'));?>
		<div class="controls">
			<span class="redirect-url"><?=$redirectUrl;?></span>
		</div>
	</div>



	<div class="form-actions">
		<?=CHtml::submitButton(Yii::t('standard', '_SAVE'), array('class'=>'btn-docebo green big')); ?>
		&nbsp;
		<a href="<?=Docebo::createLmsUrl("app/index");?>" class="btn-docebo black big"><?=Yii::t('standard', '_CANCEL');?></a>
	</div>

<?php $this->endWidget(); ?>