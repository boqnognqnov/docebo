<?php

/**
 * Plugin for GooglessoApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class GooglessoApp extends DoceboPlugin {


	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'GooglessoApp';
	public static $HAS_SETTINGS = 1;
    public $is_mobile_capable = true;

	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


	public function activate() {
		parent::activate();
		Settings::save('social_google_active', 'on'); // enable the feature
	}


	public function deactivate() {
		parent::deactivate();
		Settings::save('social_google_active', 'off'); // disable the feature
	}


}
