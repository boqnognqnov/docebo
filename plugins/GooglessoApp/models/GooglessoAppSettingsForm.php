<?php

/**
 * GooglessoAppSettingsForm class.
 *
 * @property string $googlesso_consumer_key
 * @property string $googlesso_consumer_secret
 * @property string $googlesso_redirect_uri
 */
class GooglessoAppSettingsForm extends CFormModel {
	public $googlesso_consumer_key;
	public $googlesso_consumer_secret;
	public $googlesso_redirect_uri;


	/**
	 * Declares the validation rules.
	 */
	public function rules() {
		return array(
			array('googlesso_consumer_key, googlesso_consumer_secret', 'safe'),
		);
	}


	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels() {
		return array(
			'googlesso_consumer_key' => Yii::t('GoogleappsApp', 'Consumer key'),
			'googlesso_consumer_secret' => Yii::t('GoogleappsApp', 'Consumer secret'),
			'googlesso_redirect_uri' => Yii::t('GoogleappsApp', 'Redirect URI'),
		);
	}
}
