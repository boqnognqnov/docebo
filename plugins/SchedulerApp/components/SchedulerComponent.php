<?php
/**
 * Jobs Scheduler Application component, accessible through Yii::app()->scheduler
 *
 * Manage scheduled or one time jobs on demand.
 *
 *  @package docebo-plugins
 *  @subpackage scheduler
 *
 */
class SchedulerComponent extends CApplicationComponent {

	/**
	 * Public parameters
	 * @var mixed
	 */
	public $sidekiqJobsQueue 	= 'scheduler';
	public $sidekiqApiJobsQueue = 'asyncapi';
	public $sidekiqRetry		= true;
	public $sidekiqNamespace	= false;

	/**
	 * Reflects the SchedulerApp plugin status (active/non-active).
	 * Certain operations/methods might be disallowed if the SchedulerApp is disabled (not active).
	 *
	 * This also helps checking if SchedulerApp is active by checking Yii::app()->scheduler->enabled.
	 *
	 * @var boolean
	 */
	private $_enabled = false;

	/**
	 * Private attributes
	 * @var mixed
	 */
	private $_redisHost;
	private $_redisPort;
	private $_redisDb;
	private $_redis;

	/**
	 * Initialize
	 */
	public function init() {

		parent::init();

		// Check if SchedulerApp is active and set private attribue
		if (PluginManager::isPluginActive('SchedulerApp')) {
			$this->_enabled = true;
		}

		// Sidekiq/Scheduler/Redis related
		$this->_redisHost = Yii::app()->params['jobs_redis_host'];
		$this->_redisPort = Yii::app()->params['jobs_redis_port'];
		$this->_redisDb   = Yii::app()->params['jobs_redis_db'];

		require_once(Yii::getPathOfAlias('common.vendors.Predis') . "/Autoloader.php");
		require_once(Yii::getPathOfAlias('common.vendors.SidekiqJobPusher') . "/Autoloader.php");
		Predis\Autoloader::register();
		SidekiqJobPusher\Autoloader::register();

		$this->_redis = new Predis\Client(array(
			'host' 		=> $this->_redisHost,
			'port' 		=> $this->_redisPort,
			'database' 	=> $this->_redisDb,
		));
	}

	/**
	 * $this->_enabled getter
	 *
	 * @return boolean
	 */
	public function getEnabled() {
		return $this->_enabled;
	}


	/**
	 * Push a job to Sidekiq Scheduler Redis database.
	 * It will be picked up by SK and a cron job (or other type of job executor) will be created to run the job by LMS
	 *
	 * @param string $name
	 * @param string $handler
	 * @param string $type
	 * @param string $recurringPeriod
	 * @param string $recurringLength
	 * @param string $atMinute
	 * @param string $atHour
	 * @param string $atDayOfWeek
	 * @param string $atDayOfMonth
	 * @param string $startDate
	 * @param string $params
	 * @param string $allowMultiple
	 * @param string $idUser
	 * @param string $immediate
	 * @return CoreJob
	 */
	public function createJob($name, $handler, $type,
				$recurringPeriod= false,  // hourly, daily, weekly, monthly, yearly
				$recurringLength= false,  // every <N> <period>
				$atMinute		= false,  //
				$atHour			= false,
				$timeZone		= false,
				$atDayOfWeek	= false,
				$atDayOfMonth	= false,
				$startDate		= false,  // At which date the job must be run for the first time
				$params			= false,
				$allowMultiple 	= false,
				$idUser			= false) {

		$job = CoreJob::createJob($name, $handler, $type,
			$recurringPeriod,
			$recurringLength,
			$atMinute,
			$atHour,
			$timeZone,
			$atDayOfWeek,
			$atDayOfMonth,
			$startDate,
			$params,
			$allowMultiple,
			$idUser
		);

		$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);

		$retry 	= $this->sidekiqRetry;
		$queue 	= $this->sidekiqJobsQueue;

		$jobRunUrl = Docebo::createAbsoluteLmsUrl('job', array('hash_id' => $job->hash_id));

		$command = "/usr/bin/wget  --spider --no-check-certificate -q -t 1 '" . $jobRunUrl . "'";
		$args = array($job->hash_id, $job->cron_timing, $command, Docebo::getOriginalDomain());
		$sidekiq->perform('AddNewTask', $args, $retry, $queue);

		return $job;
	}


	/**
	 * Modify a Job (LMS DB and Sidekiq scheduler)
	 *
	 * @param CoreJob $job
	 * @param string $name
	 * @param string $handler
	 * @param string $type
	 * @param string $recurringPeriod
	 * @param string $recurringLength
	 * @param string $atMinute
	 * @param string $atHour
	 * @param string $atDayOfWeek
	 * @param string $atDayOfMonth
	 * @param string $startDate
	 * @param string $params
	 * @param string $allowMultiple
	 * @param string $idUser
	 * @param string $immediate Run the job immediately?
	 *
	 * @return CoreJob
	 */
	public function modifyJob(CoreJob $job, $name, $handler, $type,
				$recurringPeriod= false,  // hourly, daily, weekly, monthly, yearly
				$recurringLength= false,  // every <N> <period>
				$atMinute		= false,  //
				$atHour			= false,
				$timeZone		= false,
				$atDayOfWeek	= false,
				$atDayOfMonth	= false,
				$startDate		= false,  // At which date the job must be run for the first time
				$params			= false,
				$allowMultiple 	= false,
				$idUser			= false) {


		$oldJob = clone $job;


		$idUser = $idUser ? (int) $idUser : (isset(Yii::app()->user) ? Yii::app()->user->id : null);
        $job->type                  = $type;
        $job->handler_id            = $handler;
		$job->name                  = $name;
		$job->rec_period 			= $recurringPeriod;
		$job->rec_length 			= $recurringLength ? (int) $recurringLength : 1;
		$job->rec_minute			= (int) $atMinute;
		$job->rec_hour				= (int) $atHour;
		$job->rec_timezone			= $timeZone;
		$job->rec_day_of_week 		= $atDayOfWeek ? $atDayOfWeek : 1;
		$job->rec_day_of_month 		= $atDayOfMonth ? $atDayOfMonth : 1;
		$job->start_date			= $startDate;
		$job->params				= CJSON::encode($params);
		$job->allow_multiple		= $allowMultiple ? 1 : 0;
		$job->id_author				= $idUser;
		$job->cron_timing			= $job->buildCronTiming();

		$result = $job->save();

		$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);

		$retry 	= $this->sidekiqRetry;
		$queue 	= $this->sidekiqJobsQueue;


		$jobRunUrl = Docebo::createAbsoluteLmsUrl('job', array('hash_id' => $job->hash_id));

		$command = "/usr/bin/wget --spider --no-check-certificate -q -t 1 '" . $jobRunUrl . "'";
		$args = array($job->hash_id, $job->cron_timing, $command, Docebo::getOriginalDomain());
		$sidekiq->perform('AddNewTask', $args, $retry, $queue);

		return $job;
	}



	/**
	 * Remove a job from Sidekiq scheduler and from LMS database (optional)
	 *
	 * @param CoreJob $job
	 * @return boolean
	 */
	public function deleteJob(CoreJob $job, $deleteJobModel = false) {

		$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);

		$retry 	= $this->sidekiqRetry;
		$queue 	= $this->sidekiqJobsQueue;

		$args = array($job->hash_id, Docebo::getOriginalDomain());
		$sidekiq->perform('RemoveTask', $args, $retry, $queue);

		if ($deleteJobModel)
			$job->delete();

		return true;
	}


	/**
	 * Push a so called "immediate" job to Sidekiq queue.
	 * It will be executed at the moment Sidekiq picks it up, effectively executing it "immediately"
	 *
	 * @param CoreJob $job
	 * @param string $name
	 * @param string $handler
	 * @param string $params
	 * @param string $idUser
	 *
	 * @return CoreJob
	 */
	public function createImmediateJob($name, $handler, $params=false, $idUser=false, $existing_job = null, $commitTransaction = false) {

		if(!$existing_job) {
			$job = CoreJob::createJob($name, $handler, CoreJob::TYPE_ONETIME,
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					$params,
					false,
					$idUser
			);
		} else {
			$job = $existing_job;
		}

		$postpone = false;
		$currentTransaction = Yii::app()->db->getCurrentTransaction();
		if (!empty($currentTransaction)) {
			if ($commitTransaction) {
				//we have been explicitly allowed to finish current transaction, so do it
				$currentTransaction->commit();
			} else {
				//we have to postpone the job pusher action somehow, otherwise it may kick in before the job record is actually written
				$postpone = true;
			}
		}

		$retry = $this->sidekiqRetry;
		$queue = $this->sidekiqJobsQueue;

		$jobRunUrl = Docebo::createAbsoluteLmsUrl('job', array('hash_id' => $job->hash_id));

		$command = "/usr/bin/wget --spider --no-check-certificate -q -t 1 ";
		$commandArgs = "'" . $jobRunUrl . "'";
		$args = array($job->hash_id, $command, $commandArgs, Docebo::getOriginalDomain());

		if ($postpone) {
			// NOTE: the job external command will be executed at the end of current application running, since at this point
			// the physical record for the job has not been written yet and it won't until the DB transaction will be committed.
			$this->setPostponedJob($job->id_job, 'AddImmediateTask', $args, $retry, $queue);
		} else {
			// no transaction involved and the job record has already been physically written in the DB. At this point
			// we are safe to execute the external command.
			$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);
			$sidekiq->perform('AddImmediateTask', $args, $retry, $queue);
		}

		return $job;
	}


	/**
	 * Push a so called "immediate" API job to Sidekiq queue.
	 * It will be executed at the moment Sidekiq picks it up, effectively executing it "immediately"
	 * Uses the AsynchAPI special job type
	 *
	 * @param CoreJob $job
	 * @param string $name
	 * @param string $handler
	 * @param string $params
	 * @param string $idUser
	 *
	 * @return CoreJob
	 */
	public function createApiJob($name, $handler, $params=false, $idUser=false, $existing_job = null, $commitTransaction = false) {

		if(!$existing_job) {
			$job = CoreJob::createJob($name, $handler, CoreJob::TYPE_ONETIME,
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					false,
					$params,
					false,
					$idUser
			);
		} else {
			$job = $existing_job;
		}

		$postpone = false;
		$currentTransaction = Yii::app()->db->getCurrentTransaction();
		if (!empty($currentTransaction)) {
			if ($commitTransaction) {
				//we have been explicitly allowed to finish current transaction, so do it
				$currentTransaction->commit();
			} else {
				//we have to postpone the job pusher action somehow, otherwise it may kick in before the job record is actually written
				$postpone = true;
			}
		}

		$retry 	= $this->sidekiqRetry;
		$queue 	= $this->sidekiqApiJobsQueue;

		$jobRunUrl = Docebo::createAbsoluteLmsUrl('job', array('hash_id' => $job->hash_id));

		$command = "/usr/bin/wget --spider --no-check-certificate -q -t 1 ";
		$commandArgs = "'" . $jobRunUrl . "'";
		$args = array($job->hash_id, $command, $commandArgs, Docebo::getOriginalDomain());

		if ($postpone) {
			// NOTE: the job external command will be executed at the end of current application running, since at this point
			// the physical record for the job has not been written yet and it won't until the DB transaction will be committed.
			$this->setPostponedJob($job->id_job, 'AsynchAPI', $args, $retry, $queue);
		} else {
			// no transaction involved and the job record has already been physically written in the DB. At this point
			// we are safe to execute the external command.
			$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);
			$sidekiq->perform('AsynchAPI', $args, $retry, $queue);
		}

		return $job;
	}


	/**
	 * List of postponed jobs for the current request. It will contain arguments and info for every postponed job.
	 * @var array
	 */
	protected $_postponedJobs = array();

	/**
	 * Add a record in the postponed job list
	 * @param $jobId the numerical ID in the "core_job" table
	 * @param $type task type
	 * @param $args command arguments for the job to be called externally
	 * @param $retry parameter for the job
	 * @param $queue parameter for the job
	 */
	protected function setPostponedJob($jobId, $type, $args, $retry, $queue) {
		Yii::log('Setting postponed job #'.$jobId.' ('.$type.')', CLogger::LEVEL_INFO);
		$this->_postponedJobs[] = array(
			'jobId' => $jobId,
			'type' => $type,
			'args' => $args,
			'retry' => $retry,
			'queue' => $queue,
			'executed' => false
		);
	}

	/**
	 * This method will launch all jobs that have ben postponed for some reasons.
	 * It just iterates the internal postponed jobs list and executes them one by one.
	 *
	 * NOTE: this method has to be called externally. At the moment of writing this it is triggered in the file
	 * "plugins/SchedulerApp/SchedulerAppModule.php", linked to 'onEndRequest' event of Yii application object.
	 */
	public function executePostponedJobs() {
		if (!empty($this->_postponedJobs)) {
			$sidekiq = new SidekiqJobPusher\Client($this->_redis, $this->sidekiqNamespace);
			foreach ($this->_postponedJobs as $index => $postponedJob) {
				//make sure that the job cannot be executed a second time
				if ($postponedJob['executed']) {
					continue;
				}
				// if not executed yet, then continue regularly
				$jobId = $postponedJob['jobId'];
				$job = CoreJob::model()->findByPk($jobId); //make sure that postponed job effectively exists (it may not, due to failed transactions and so on)
				if (!empty($job)) {
					$type = $postponedJob['type'];
					$args = $postponedJob['args'];
					$retry = $postponedJob['retry'];
					$queue = $postponedJob['queue'];
					Yii::log('Executing postponed job #'.$jobId.' ('.$type.')', CLogger::LEVEL_INFO);
					$sidekiq->perform($type, $args, $retry, $queue);
				} else {
					Yii::log('Warning: a job execution request could not being satisfied, job #'.$jobId.' not found', CLogger::LEVEL_WARNING);
				}
				//mark the job as already executed
				$this->_postponedJobs[$index]['executed'] = true;
			}
		}
	}

}