<?php
/**
 * 
 *  @package docebo-plugins
 *  @subpackage scheduler
 *
 */
class SchedulerAppModule extends CWebModule {
	
	/**
	 * @see CModule::init()
	 */
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'SchedulerApp.models.*',
			'SchedulerApp.components.*',
		));

		Yii::app()->attachEventHandler('onEndRequest', array($this, 'onEndRequest'));
	}

	/**
	 * @see CWebModule::beforeControllerAction()
	 */
	public function beforeControllerAction($controller, $action) {
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}


	public function onEndRequest($event) {
		// in case some immediate jobs could not be executed immediatly, make sure that they are at the end of current request
		Yii::app()->scheduler->executePostponedJobs();
	}

}
