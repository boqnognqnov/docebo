<?php
/**
 * 
 *  @package docebo-plugins
 *  @subpackage scheduler
 *  
 */
class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
}