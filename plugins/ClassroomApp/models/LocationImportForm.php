<?php

class LocationImportForm extends CFormModel {

    public $file;
    public $separator = 'auto';
    public $manualSeparator;
    public $firstRowAsHeader = true;
    public $charset = 'UTF-8'; // charset of csv file
    public $insertUpdate = false;

    protected $_data;
    protected $_importMap = array(
        'id_location',
        'name',
        'address',
        'name_country',
        'telephone',
        'email',
        'reaching_info',
        'accomodations',
        'other_info',
    );
    protected $_maxFileSize = 3145728; // 314572800 = 300 Mb

    public function rules() {
        return array(
            array('file', 'file', 'on'=>'step_import', 'maxSize'=>$this->_maxFileSize,
                'tooLarge' => 'The file "{file}" is too large. Its size cannot exceed '.$this->getMaxFileSize('MB').' megabytes.',
                'types' => array('csv'),
                'wrongType' => 'File type should be in "csv" format',
            ),
            array('file', 'file', 'on' => 'step_import',
                'mimeTypes' => array(
                    'text/x-comma-separated-values',
                    'text/comma-separated-values',
                    'application/octet-stream',
                    'application/vnd.ms-excel',
                    'text/csv',
                    'text/plain',
                    'application/csv',
                    'application/excel',
                    'application/vnd.msexcel'
                ),
                'wrongMimeType' => 'File type should be in "csv" format',
                'skipOnError' => true,
            ),
            array('importMap', 'checkImportMap', 'on' => 'step_process'),
            array('separator, manualSeparator, firstRowAsHeader, charset, insertUpdate', 'safe')
        );
    }

    public function attributeLabels() {
        return array(
            'file' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_FILE'),
            'separator' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_SEPARATOR'),
            'firstRowAsHeader' => Yii::t('admin_directory', '_GROUP_USER_IMPORT_HEADER'),
            'charset' => Yii::t('organization_chart', '_ORG_CHART_IMPORT_CHARSET'),
            'insertUpdate' => Yii::t('classroom', 'Update locations info'),
        );
    }

    public function dataProvider() {
        $items = $this->getData();

        if ($this->firstRowAsHeader) {
            unset($items[0]);
        }

        return new CArrayDataProvider($items, array(
            'pagination' => array(
                'pageSize' => Settings::get('elements_per_page', 10)
            ),
        ));
    }

    public function setImportMap($importMap) {
        if (is_array($importMap)) {
            $this->_importMap = $importMap;
            return true;
        }
        return false;
    }

    public function getImportMap() {
        return $this->_importMap;
    }

    public function getMaxFileSize($type = 'B') {
        switch ($type) {
            case 'B':
                return $this->_maxFileSize;
                break;
            case 'KB':
                return (int) ($this->_maxFileSize / 1024);
                break;
            case 'MB':
                return (int) ($this->_maxFileSize / (1024 * 1024));
                break;
        }
    }

    /**
     * Inline validator
     */
    public function checkImportMap($attribute, $params) {
        $selectedItems = array();
        foreach ($this->$attribute as $item) {
            if ($item != 'ignoredfield') {
                if (!in_array($item, $selectedItems)) {
                    $selectedItems[] = $item;
                } else {
                    $this->addError('importMap', 'Duplicate of fields');
                    break;
                }
            }
        }
    }

    public function setData($data = array()) {
        $this->_data = $data;
    }

    public function getData() {
        if ( ! is_array($this->_data)) {
            $delimiter = '';
            switch ($this->separator) {
                case 'comma':
                    $delimiter = ',';
                    break;
                case 'semicolon':
                    $delimiter = ';';
                    break;
                case 'manual':
                    $delimiter = $this->manualSeparator;
                    break;
                case 'auto':
                    $delimiter = 'auto';
                    break;
            }
            $csvFile = new QsCsvFile(array(
                'path' => $this->file->tempName,
                'charset' => $this->charset,
                'delimiter' => $delimiter,
                'firstRowAsHeader' => $this->firstRowAsHeader,
            ));
            $this->_data = $csvFile->getArray();
        }

        return $this->_data;
    }

    public static function getImportMapList() {
        $locationFields = array(
            //'id_location',
            'name',
            'address',
            'name_country',
            'telephone',
            'email',
            'reaching_info',
            'accomodations',
            'other_info',
        );

        $list = array(
            'ignoredfield' => Yii::t('organization_chart', '_IMPORT_IGNORE'),
        );

        foreach ($locationFields as $value) {
            $list[$value] = LtLocation::model()->getAttributeLabel($value);
        }

        $list['name_country'] = Yii::t('standard', '_COUNTRY');

        return $list;
    }

    public function save() {
        if (!$this->validate()) {
            return false;
        }

        $data = $this->_data;
        if ($this->firstRowAsHeader) {
            unset($data[0]);
        }

        $locationIndex = null;
        $countryName = null;

        foreach ($this->_importMap as $k => $fieldName) {
            if ($fieldName == 'ignoredfield') {
                unset($this->_importMap[$k]);
            }
            else if ($fieldName == 'id_location') {
                $locationIndex = $k;
                unset($this->_importMap[$k]);
            }
        }

        $insertCount = 0;

        // pre-fetch country list
        $cacheCountry = array();
        $countries = CoreCountry::model()->findAll();
        foreach ($countries as $country) {
            $cacheCountry[strtolower($country['name_country'])] = $country;
        }

        $transaction = Yii::app()->db->beginTransaction();
        foreach ($data as $item) {
            $object = null;

            if ($this->insertUpdate && isset($locationIndex)) {
                $idLocation = intval( $item[$locationIndex] );
                $object = LtLocation::model()->findByPk($idLocation);
            }

            if ($object === null) {
                $object = new LtLocation();
            }

            foreach ($this->_importMap as $i => $name) {
                if (property_exists(get_class($object), $name) || $object->hasAttribute($name)) {
                    $object->$name = $item[$i];
                }
                else if ($name == 'name_country') {
                    // find country by name (lowercase)
                    $countryName = strtolower( trim($item[$i]) );
                    if (isset($cacheCountry[$countryName])) {
                        $country = $cacheCountry[$countryName];
                        $object->id_country = $country->id_country;
                    }
                }
            }

            $isValid = $object->validate();
            if (!$isValid) {
                Yii::log('Could not import the following location:', CLogger::LEVEL_ERROR);
                Yii::log(CVarDumper::dumpAsString($object->attributes), CLogger::LEVEL_ERROR);
                Yii::log(CVarDumper::dumpAsString($object->getErrors()), CLogger::LEVEL_ERROR);
                continue;
            }

            if ($object->hasErrors()) {
                foreach ($object->errors as $key => $errors) {
                    if (in_array($key, $this->_importMap)) {
                        if ($key !== 'id_location') {
                            foreach ($errors as $error) {
                                $this->addError('location_'.$insertCount.'_field_'.$key, 'Line #'.$insertCount.'. '.$error);
                            }
                        }
                    }
                }
            }


            if ($object->save())
                $insertCount++;
        }

        $transaction->commit();

        return ($insertCount > 0);
    }
}