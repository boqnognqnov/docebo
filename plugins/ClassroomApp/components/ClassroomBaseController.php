<?php



/**
 * Base controller with common functions for controllers used by classroom
 */
class ClassroomBaseController extends Controller {

	// ** Internal variables for course and session models (if specified in input parameters) **

	/**
	 * @var LearningCourse
	 */
	protected $_courseModel = NULL;

	/**
	 * @var LtCourseSession
	 */
	protected $_sessionModel = NULL;

	public $userCanAdminCourse = false;



	/**
	 * Preloads course model (and session model)
	 * @param $action
	 * @return bool
	 * @throws CException
   */
	public function beforeAction($action) {
		$rq = Yii::app()->request;
		$cs = Yii::app()->getClientScript();

		//first check if we are requesting an autocomplete action:
		//these actions have different parameters from standard requests
		$input = $rq->getParam('input', false);
		if (is_array($input) && !empty($input) && isset($input['query'])) {
			$idCourse = $input['course_id'];
			$idSession = $input['id_session'];
		} else {
			$idCourse = $rq->getParam('course_id', $rq->getParam('idCourse', false));
			$idSession = $rq->getParam('id_session', $rq->getParam('idSession', false));
		}

		// Preload course module
		if ($idCourse !== false) {
			$this->_courseModel = LearningCourse::model()->findByPk($idCourse);

			// Do some validation on course type
			if (!$this->_courseModel)
				throw new CHttpException('Invalid course ('.$idCourse.')');
			else if ($this->_courseModel->course_type != LearningCourse::TYPE_CLASSROOM)
				throw new CHttpException('Selected course ('.$idCourse.') is not a classroom course!');

			if (!LearningCourseuser::isSubscribed(Yii::app()->user->id, $idCourse)) {
				// Is this a power user and the course was assigned to him?
				$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
					'puser_id' => Yii::app()->user->id,
					'course_id' => $idCourse
				));

				if (Yii::app()->user->getIsAdmin() && !$pUserCourseAssigned)
					throw new CHttpException('Cannot access course "' . $idCourse . '"');
			}

			// Let "player" Yii component know about the course we are in
			// This will allow Main Menu to show "course related" tiles: "play" and "manage courses"
			Yii::app()->player->setCourse($this->_courseModel);
		}

		// Preload session model
		if ($idSession !== false) {
			$this->_sessionModel = LtCourseSession::model()->findByPk($idSession);

			// Do some validation on session info
			if (!$this->_sessionModel)
				throw new CHttpException('Invalid session ('.$idSession.')');
			else if ($idCourse !== false) {
				if ($this->_sessionModel->course_id != $idCourse)
					throw new CHttpException('Session course is different from passed course');
			}
		}

		// Load common scripts and css
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/classroom.css');

		//check provenience and update breadcrumbs if necessary
		$from = $rq->getParam('ref_src', false);
		switch ($from) {
			case 'admin': {
				//we came here from admin course management
				if (!Yii::app()->request->isAjaxRequest) {
					$this->breadcrumbs = array(); //initialization. WARNING: do not assign $this->breadcrumbs = ... directly in views !
					$this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
					$this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
				}
				Docebo::setDefaultUrlParam('ref_src', $from);
			}
		}

		if($idCourse === false && $idSession !== false)
			$idCourse = $this->_sessionModel->course_id;

		$pUserCourseAssigned = CoreUserPuCourse::model()->findByAttributes(array(
			'puser_id' => Yii::app()->user->id,
			'course_id' => $idCourse
		));

		$courseLevel = LearningCourseuser::userLevel(Yii::app()->user->id, $idCourse);

		// Set this controller attribute for later usage in forum admin UIs: Course Admins CAN admin course forums in any way
		if (Yii::app()->user->isGodAdmin){
			//if it's god admin, course user level doesn't matter
			$this->userCanAdminCourse = true;
		}elseif($courseLevel && $courseLevel > LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT){
			// User subscribed to course and his level is high enough
			$this->userCanAdminCourse = true;
		}elseif($pUserCourseAssigned && Yii::app()->user->checkAccess('/lms/admin/course/mod')){
			// User power user, course assigned to him, and he has permissions to modify
			$this->userCanAdminCourse = true;
		}

		//standard function
		return parent::beforeAction($action);
	}

	// Getter methods
	public function getIdCourse() { return (!empty($this->_courseModel) ? $this->_courseModel->getPrimaryKey() : NULL); }
	public function getCourseModel() { return (!empty($this->_courseModel) ? $this->_courseModel : NULL); }
	public function getIdSession() { return (!empty($this->_sessionModel) ? $this->_sessionModel->getPrimaryKey() : NULL); }
	public function getSessionModel() { return (!empty($this->_sessionModel) ? $this->_sessionModel : NULL); }
}