<?php


class TimeTableHandler extends CComponent {

	/**
	 * @var array the list of periods. Each period has a "begin" and an "end" value, and periods are sorted from most recent to latest
	 */
	protected $periods = array();


	/**
	 * Check time format validity, a time must be provided in the format hh:mm:ss
	 * @param $time string
	 * @return bool true if the format of provided time is valid, otherwise false
	 */
	protected function checkTimeFormat($time) {
		//TODO: this ....
		return true;
	}


	protected function convertTimeIntoSeconds($time,$timezone = false) {
		list($hours, $minutes, $seconds) = explode(':', $time);
		if($timezone){
			$offset = $this->get_timezone_offset($timezone,'Pacific/Midway');
			return (int)$hours*3600 + (int)$minutes*60 + (int)$seconds -(int)$offset;
		} else {
			return (int)$hours*3600 + (int)$minutes*60 + (int)$seconds;
		}
	}

	protected function convertSecondsIntoTime($seconds,$timezone = false) {
		$timeHours = str_pad(''.(floor($seconds/3600)), 2, '0', STR_PAD_LEFT);
		$timeMinutes = str_pad(''.(floor(($seconds - 3600*$timeHours)/60)), 2, '0', STR_PAD_LEFT);
		$timeSeconds = str_pad(''.($seconds % 60), 2, '0', STR_PAD_LEFT);
		return $timeHours.':'.$timeMinutes.':'.$timeSeconds;
	}


	protected function sortPeriods() {
		usort($this->periods, function($a, $b) {
			if ($a->begin == $b->begin) { return 0; }
			return ($a->begin < $b->begin) ? -1 : 1;
		});
	}

	protected function get_timezone_offset($remote_tz, $origin_tz = null) {
		if($origin_tz === null) {
			if(!is_string($origin_tz = date_default_timezone_get())) {
				return false; // A UTC timestamp was returned -- bail out!
			}
		}
		$origin_dtz = new DateTimeZone($origin_tz);
		$remote_dtz = new DateTimeZone($remote_tz);
		$origin_dt = new DateTime("now", $origin_dtz);
		$remote_dt = new DateTime("now", $remote_dtz);
		$offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
		return $offset;
	}


	/**
	 * Add a time period to the periods list, checking and adjusting time overlappings
	 * @param $timeBegin period beginning time, format hh:mm:ss
	 * @param $timeEnd period ending time, format hh:mm:ss
	 * @param $name session name
	 * @param $timeEnd classroom name
	 */
	public function addPeriod($timeBegin, $timeEnd, $session_name = '',$classroom_name = '',$session_id = '',$begin = '', $end = '', $course_name = '', $timezone = false) {

		$secondsBegin = $this->convertTimeIntoSeconds($timeBegin,$timezone);
		$secondsEnd = $this->convertTimeIntoSeconds($timeEnd,$timezone);

		if ($this->checkPeriod($timeBegin, $timeEnd)) {
			//specified period does not overlap with existing periods: add it directly to the periods array (sorted, of course)
			$newPeriod = new stdClass();
			$newPeriod->begin = $secondsBegin;
			$newPeriod->end = $secondsEnd;
			$newPeriod->message = Yii::t('classroom',"Warning!Classroom - {classroom_name} is already booked by {course_name} - {session_name} from {start} to {end}.To save this date you must change your session's time or classroom.",array(
				'{classroom_name}'=>$classroom_name,
				'{session_name}'=>$session_name,
				'{start}'=>$begin,
				'{end}'=>$end,
				'{course_name}'=>$course_name)
			);
			$newPeriod->session_id = $session_id;
			$this->periods[] = $newPeriod;
			$this->sortPeriods();
		} else {
			//we must detect which existing periods overlap with new period, and adjusting the periods array accordingly
			$checkedAll = false;
			$overlappingPeriods = array();
			$loopSecurityCheck = 0;
			while (!$checkedAll && $loopSecurityCheck < 50000) { //TODO: a better way to prevent infinite loops?
				if (empty($this->periods)) {
					$checkedAll = true;
				} else {
					$breaked = false;
					foreach ($this->periods as $index => $period) {
						if ($period->begin > $secondsEnd || $period->end < $secondsBegin) { continue; }
						//now we know that some periods overlap with input period
						list($sliced) = array_slice ($this->periods , $index, 1, false);
						if ($sliced->begin < $secondsBegin || $sliced->end > $secondsEnd) {
							$overlappingPeriods[] = $sliced;
						}
						$breaked = true;
						break;
					}
					if (!$breaked) {
						$checkedAll = true;
					}
				}
				$loopSecurityCheck++;
			}
			if ($loopSecurityCheck >= 50000) { throw new CException('Error in internal loop'); }
			if (!empty($overlappingPeriods)) {
				//NOTE: there can't be more then 2 overlapping periods
				$joinedPeriod = array($secondsBegin, $secondsEnd);
				foreach ($overlappingPeriods as $overlappingPeriod) {
					if ($overlappingPeriod->begin < $joinedPeriod[0]) { $joinedPeriod[0] = $overlappingPeriod->begin; }
					if ($overlappingPeriod->end > $joinedPeriod[1]) { $joinedPeriod[1] = $overlappingPeriod->end; }
				}
				$this->periods[] = $joinedPeriod;
				$this->sortPeriods();
			}
		}
	}



	/**
	 * Check if a time period overlaps with existing periods
	 * @param $timeBegin string period beginning time, format hh:mm:ss
	 * @param $timeEnd string period ending time, format hh:mm:ss
	 * @return bool if the period to be checked does not overlap then return TRUE, otherwise return false
	 */
	public function checkPeriod($timeBegin, $timeEnd,$timezone=false) {
		$output  = true;
		if (empty($this->periods)) { return true; } //no periods to check, so ...
		$secondsBegin = $this->convertTimeIntoSeconds($timeBegin,$timezone);
		$secondsEnd = $this->convertTimeIntoSeconds($timeEnd,$timezone);
		foreach ($this->periods as $index => $period) {
			if (($secondsBegin < $period->begin && $secondsEnd > $period->begin) || ($secondsBegin < $period->end && $secondsEnd > $period->end)) {
				$output = false;
			}
			if (!$output) break; //no more checks needed
		}
		return $output;
	}



	/**
	 * Find all overlapping time periods for a given period
	 * @param $timeBegin string period beginning time, format hh:mm:ss
	 * @param $timeEnd string period ending time, format hh:mm:ss
	 * @return array list of found periods
	 */
	public function getOverlappingPeriods($timeBegin, $timeEnd,$timezone=false) {
		$output  = array();
		if (empty($this->periods)) { return array(); } //no periods to check, so ...
		$secondsBegin = $this->convertTimeIntoSeconds($timeBegin,$timezone);
		$secondsEnd = $this->convertTimeIntoSeconds($timeEnd,$timezone);
		foreach ($this->periods as $index => $period) {
			if (($secondsBegin <= $period->begin && $secondsEnd >= $period->begin) || ($secondsBegin <= $period->end && $secondsEnd >= $period->end)) {
				$output[] = $period;
			}
		}
		return $output;
	}


	/**
	 * Return the list of stored periods
	 * @param bool $useArrays specify if the periods returned are in form of array or object
	 * @return array the list of periods
	 */
	public function getPeriods($useArrays = false) {
		$output = array();
		foreach ($this->periods as $period) {
			if ($useArrays) {
				$item = array(
					'timeBegin' => $this->convertSecondsIntoTime($period->begin),
					'timeEnd' => $this->convertSecondsIntoTime($period->end)
				);
			} else {
				$item = new stdClass();
				$item->timeBegin = $this->convertSecondsIntoTime($period->begin);
				$item->timeEnd = $this->convertSecondsIntoTime($period->end);
			}
			$output[] = $item;
		}
		return $output;
	}
}