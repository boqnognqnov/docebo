<?php

/**
 * DOCEBO, e-learning SAAS
 *
 * @link http://www.docebo.com/
 * @copyright Copyright &copy; 2004-2013 Docebo
 */
class ClassroomAppModule extends CWebModule {

	/**
	 * @var string Path to assets for this module
	 */	
	
	protected $_assetsUrl = null;
	protected $_playerAssetsUrl = null;

	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

        if(Yii::app() instanceof CWebApplication){
            $r = Yii::app()->request->getParam("r");
            if(strtolower($r) == 'player/training/session') {
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile($this->getAssetsUrl().'/js/sessionForm.js');
                $cs->registerScriptFile($this->getAssetsUrl().'/js/validateSessionForm.js'); // NOT THE RIGHT PLACE TO CALL THIS FILE /-- TrainingController.php --/
            }
        }
		
		// Listen for the event raised by My Dashboard->KPIs Dashlet, collecting custom KPIs
		Yii::app()->event->on('CollectCustomMyKPIs', array($this, 'provideMyKPIs'));
		
		// Also, listen for event to return specific KPI's data and view path to be rendered
		Yii::app()->event->on('CollectCustomKPIData', array($this, 'provideKPIData'));
		
		// Also, listen for event to register our own JS/CSS
		Yii::app()->event->on('RegisterClientResources', array($this, 'onRegisterClientResources'));

		
	}


	/**
	 * Get module assets url path
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ClassroomApp.assets'));
		}

		return $this->_assetsUrl;
	}


	/**
	 * This method will be called when the plugin is loaded in order to setup the menu of the app (it's optional)
	 */
	public function menu() {

		$subMenu = array();
		//check permissions for submenu
		if (Yii::app()->user->checkAccess('/lms/admin/course/view')) {
			$subMenu[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
		}
		if (Yii::app()->user->checkAccess('/lms/admin/location/view')) {
			$subMenu[Yii::t('classroom', 'Locations')] = Docebo::createAppUrl('lms:ClassroomApp/location/index');
		}
		if (Yii::app()->user->getIsGodadmin()) {
			$subMenu[Yii::t('standard', 'Settings')] = Docebo::createAppUrl('admin:courseManagement/attendance');
		}
		$event = new DEvent($this, array());
		Yii::app()->event->raise('CheckForSettingMenu', $event);
		$additionalMenu = null;
		if (!$event->shouldPerformAsDefault()) {
			$additionalMenu = $event->return_value;
		}

		//compose the entire menu
		Yii::app()->mainmenu->addAppMenu('classroom', array(
				'icon' => 'admin-ico classroom',
				'app_icon' => 'fa-users', // icon used in the new menu
				'link' => 'javascript:void(0);',
				'label' => Yii::t('standard', '_CLASSROOM'),
				'permission' 	=> '/lms/admin/course/view|/lms/admin/location/view', //double permission check
				'settings' => $additionalMenu
			), $subMenu);
	}



	public function beforeControllerAction($controller, $action) {
		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here

			// Trigger event to let plugins do their custom init operations
			$event = new DEvent($this, array (
				'controller' => $controller,
				'action' => $action
			));
			Yii::app()->event->raise('OnClassroomBeforeControllerAction', $event);
			// end event

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return the info needed to allow the management of this admin in the lms
	 * @return array
	 */
	public function getRoleIds() {
		$roles = array();
		$roles['locations'] = array(
			'title' => Yii::t('classroom', 'Locations'),
			'roleId' => '/lms/admin/location/',
		);
		return $roles;
	}

	/**
	 * Convert ISO dates and times into locally specified regional format
	 * 
	 * @param string $input date in ISO format
	 * @return string date in regional format
	 */

	public function formatTime($input) {
		if (!$input) {
			$input = '';
		}
		return Yii::app()->localtime->toLocalTime($input, 'short');  // up to minutes only, no seconds
	}

	

	/**
	 * This plugin's list of custom MY KPIs
	 * 
	 * @return array
	 */
	public static function myKPIList() {
		return array(
			'best_ilt_score'						=> Yii::t( 'classroom', 'Best score on ILT session' ),
			'upcoming_ilt_session_as_instructor' 	=> Yii::t( 'classroom', 'Upcoming ILT session (as instructor)'), 
		);
	}
	
	
	/**
	 * Get player module assets url path
	 * @return mixed
	 */
	public function getPlayerAssetsUrl() {
		if ($this->_playerAssetsUrl === null) {
			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}
	
	
	/**
	 * Return (actually, add) KPIs provided by this plugin to the event parameter
	 *
	 * @param DEvent $event
	 */
	public function provideMyKPIs(DEvent $event) {
		foreach (self::myKPIList() as $kpi => $label) {
			$event->params['kpis'][$kpi] = $label;
		}
	}
	
	
	/**
	 * Return specific KPI's data and view path to render by caller OR directly HTML to be shown
	 *
	 * @param DEvent $event
	 */
	public function provideKPIData(DEvent $event) {
		
		$kpi = isset($event->params['kpi']) ? $event->params['kpi'] : false;
		
		// No KPI specified or this KPI is NOT mine, get out
		$comboKPIList = array_merge(self::myKPIList());
		if (!$kpi || !in_array($kpi, array_keys($comboKPIList)))
			return true;
		
		// Ok, lets see how we can help this guy.. 
		switch ($kpi) {
			case 'best_ilt_score':
				$result = $this->KPI_best_ilt_score();
				break;
			case 'upcoming_ilt_session_as_instructor':
				$result = $this->KPI_upcoming_ilt_session_as_instructor();
				break;	
		}
		
		// Set the evernt parameters, it MUST be passed by reference
		$event->params['viewParams'] 	= $result['viewParams'];
		$event->params['viewPath'] 		= $result['viewPath'];
		$event->params['viewHtml'] 		= $result['viewHtml'];
		
		
	}
	
	
	/**
	 * Return calculated data && view path to render for this very KPI
	 * 
	 * @return array
	 */
	protected function KPI_best_ilt_score() {
		
		$params['sessionName'] = null;
		$params['score'] = 0;
		$params['idCourse'] = null;
		
		$params['instructors'] = array();
		
		$sessionWithBestScore = Yii::app()->getDb()->createCommand()
			->select('s.id_session, s.course_id, s.name, (cu.evaluation_score/s.score_base*100) AS scorePercent')
			->from(LtCourseSession::model()->tableName().' s')
			->join(LtCourseuserSession::model()->tableName().' cu', 'cu.id_session=s.id_session AND cu.id_user=:idUser AND cu.status=:ended')
			->bindValues(array(
				':idUser'=>Yii::app()->user->getIdst(),
				':ended'=>LtCourseuserSession::$SESSION_USER_END,
			))
			->order('scorePercent DESC')
			->queryRow();
		
		if ( ! empty( $sessionWithBestScore ) ) {
			$params['sessionName'] = $sessionWithBestScore['name'];
			$params['score']       = intval( $sessionWithBestScore['scorePercent'] );
			$params['idCourse'] = intval($sessionWithBestScore['course_id']);
		
			$instructors = Yii::app()->getDb()->createCommand()
				->select( 'u.firstname, u.lastname, u.userid' )
				->from( CoreUser::model()->tableName() . ' u' )
				->join( LearningCourseuser::model()->tableName() . ' cu', 'cu.idUser=u.idst AND cu.idCourse=:idCourse AND cu.level=:instructor' )
				->bindValues( array(
					':idCourse' => $sessionWithBestScore['course_id'],
					':instructor' => LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
				) )
				->queryAll();
		
			if(!empty($instructors)){
				foreach($instructors as $instructor){
					$params['instructors'][$instructor['userid']] = $instructor['firstname'].' '.$instructor['lastname'];
				}
			}
		}
		
		return array(
			'viewParams' 	=> $params,
			'viewPath'		=> 'plugin.ClassroomApp.views.kpi.mykpis_best_ilt_score',
		); 
		
	}
	

	
	/**
	 * Return calculated data && view path to render for this very KPI
	 *
	 * @return array
	 */
	protected function KPI_upcoming_ilt_session_as_instructor() {

		$params['name'] = null;
		$params['date'] = null;
		$params['location'] = null;
		
		$upcomingSessionAsInstructor = Yii::app()->getDb()->createCommand()
			->select('s.id_session, s.name, s.date_begin')
			->from(LtCourseSession::model()->tableName().' s')
			->join(LearningCourseuser::model()->tableName().' cu', 'cu.idCourse=s.course_id AND cu.idUser=:idUser AND cu.level=:instructor')
			->join(LtCourseuserSession::model()->tableName().' csu', 'csu.id_user=cu.idUser AND s.id_session=csu.id_session')
			->where('s.date_begin > NOW()')
			->order('s.date_begin ASC')
			->bindValues(array(
				':idUser'=>Yii::app()->user->getIdst(),
				':instructor'=>LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR,
			))
			->queryRow();

		if(!empty($upcomingSessionAsInstructor)){
			$params['name'] = $upcomingSessionAsInstructor['name'];
			$params['date'] = Yii::app()->localtime->toLocalDatetime($upcomingSessionAsInstructor['date_begin']);
		
			$locations = Yii::app()->getDb()->createCommand()
				->select('l.name')
				->from(LtLocation::model()->tableName().' l')
				->join(LtCourseSessionDate::model()->tableName().' date', 'date.id_location=l.id_location AND date.id_session=:idSession', array(':idSession'=>$upcomingSessionAsInstructor['id_session']))
				->group('date.id_location')
				->queryColumn();
		
			if(count($locations)==1){
				// Only show the location if it's one.
				// If multiple dates with multiple dates
				// are present for this session, don't show location
				$params['location'] = reset($locations);
			}
		}
		
		return array(
			'viewParams' 	=> $params,
			'viewPath'		=> 'plugin.ClassroomApp.views.kpi.mykpis_upcoming_ilt_session_as_instructor',
		);
	
	}

	
	/**
	 * Use this event handler to [Yii] Register Script/CSS or just echo JS/CSS.
	 */
	public function onRegisterClientResources(DEvent $event) {
	}
	
	
}
