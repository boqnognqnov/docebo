/**********************************************************************************************
 *  Arena
 *
 *  We need this because some of the base LO are using Arena.course_id
 *  to send tracking message to the system to track if the LO is completed or not
 *
 */
var Arena = {
    /**
     * Initialize the environment
     * @param options
     */
    init: function (options) {
        this.course_id = options.course_id;
        this.uploadFilesUrl = options.uploadFilesUrl;
        this.csrfToken = options.csrfToken;
    },
};

/**********************************************************************************************
 *  Launcher
 *
 *  All the classroom LO launching business is done here
 *
 */
var ClassroomLoLauncher = function (options) {
	this.init(options);
};

ClassroomLoLauncher.prototype = {

	courseInfo: {
		course_id: 0,
		session_id: 0,
		isMpCourse: false,
		mpSeatsUnlimited: true,
		availableSeats: 0,
		userHasSeat: true
	},


	init: function (options) {
		this.courseInfo.course_id = options.course_id;
		this.courseInfo.session_id = options.session_id;
		this.courseInfo.idResource = options.idResource;
		this.courseInfo.isMpCourse = options.isMpCourse;
		this.courseInfo.mpSeatsUnlimited = options.mpSeatsUnlimited;
		this.courseInfo.availableSeats = options.availableSeats;
		this.courseInfo.userHasSeat = options.userHasSeat;

		//reference variable
		var that = this;

		// close listener for launchpad
		$(document).on('click', '#player-inline-close, #inline-player-close', function (e) {
			e.preventDefault();
			that.setLearnerView();
			that.resetLaunchpad();
            /* Check the: plugins/ClassroomApp/widgets/views/sessionInfo.php to check the ListviewID */
            $.fn.yiiListView.update('classroom-objects-list');
		});

		//set events for launching objects
		//var listView = $('#classroom-objects-list');
		$('.classroom-lo-play').on('click', function(e) {
			e.preventDefault();
			that.launch($(this).data());
		});
	},


	isTablet: function() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			var w = $(window).width();
			var h = $(window).height();
			var device_width = Math.max(w,h);
			if (device_width > 767) { return true; }
		}
		return false;
	},


	isSmarthphone: function() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			var w = $(window).width();
			var h = $(window).height();
			var device_width = Math.max(w,h);
			if (device_width <= 767) { return true; }
		}
		return false;
	},


	setLearnerView: function() {
		$('#player-arena-launchpad').hide();
		$('#session-info-container').show();
	},

	setPlayerView: function() {
		$('#session-info-container').hide();
		$('#player-arena-launchpad').show();
	},


	resetLaunchpad: function() {
		$('#player-arena-launchpad').html('');
	},


	launch: function (loData) {

		// Check if the course we are running
		//   1. Is a Marketplace course
		//   2. Has ZERO available seats
		//   3. And user has NO seats taken before
		//   If all match: show feedback message and return
		if (this.courseInfo.isMpCourse && !this.courseInfo.mpSeatsUnlimited){
			if ((this.courseInfo.availableSeats == 0) && (!this.courseInfo.userHasSeat) ) {
				Docebo.Feedback.show('error', Yii.t('standard', 'No seats available'));
				return false;
			}
		}

		var that = this;

		// We are launching a LO! So, first, we have to track the "course access"
		$.ajax({
			url: Docebo.lmsAbsoluteBaseUrl+'/index.php?r=//player/training/axTrackCourseAccess',
			type: 'post',
			dataType: 'json',
			data: {
				course_id: that.courseInfo.course_id
			},
			beforeSend: function () {}
		}).done(function (res) {});

		// Change the Learner/Admin View switch
		this.setPlayerView();

		switch (loData.type) {
            case 'authoring':
                that.authoring(loData);
                break;

            case 'htmlpage':
                that.htmlpage(loData);
                break;

            case 'deliverable':
            case 'video':
            case 'file':
            case 'poll':
            case 'test':
                that.launchpad(loData);
                break;

			default:
				Docebo.Feedback.show('error', loData.type+" launcher not available ...");
				break;
		}

		return true;
	},

    launchpad: function(loData){

        // !!! Compose the Launchpad URL ( <module>/default/axLaunchpad ) !!!
        var url = Docebo.lmsAbsoluteBaseUrl+'/index.php?r='+loData.type+'/default/axLaunchpad';

        var ajaxParams = {
            course_id: this.courseInfo.course_id,  // We need course_id for ALL requests
            id_object: loData.id_object
        };
        if (this.courseInfo.session_id) {
            ajaxParams.session_id = this.courseInfo.session_id;
        }

        // Load the launchpad
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: ajaxParams,
            beforeSend: function () {
            }
        }).done(function (res) {
            if (res.success == true) {
                // We expect res.data.html OR res.html
                // res.html will be subject to global .ajaxFilter!!
                // res.data.html - not. You decide which one to use
                if (res.data != null && typeof (res.data.html) !== 'undefined') {
                    $('#player-arena-launchpad').html(res.data.html);
                } else {
                    $('#player-arena-launchpad').html(res.html);
                }
            } else {
                Docebo.Feedback.show('error', res.message);
            }
        }).always(function () {
            Docebo.activateAjaxFilter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
        });
    },

    authoring: function(loData) {
        //var launchLink = Docebo.rootAbsoluteBaseUrl+'/authoring/index.php?r=main/play&id=' + loData.idResource + '&id_course=' + Arena.course_id;
        var launchLink = Docebo.rootAbsoluteBaseUrl+'/authoring/index.php?r=main/play&id=' + loData.id_resource + '&id_object=' + loData.id_object + '&id_course=' + this.courseInfo.course_id;
        var launchLinkStripped 	= launchLink +'&stripped=1';
        window.location.href = launchLink;
    },

    /**
     * Show HTML Page learning object
     */
    htmlpage:		function(loData) {

        // Get the launch type (LO specific)
        // Possible open mode parameters are:
        // loData.params.openmode, .desktop_openmode, .tablet_openmode, .smartphone_openmode
        // Use them on your discretion

        var launchType = 'inline';

        // However, Force INLINE for now; Initially, HTML page is required to be inline only, but.. we may change this
        launchType = 'inline';

        var launchLink = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=htmlpage/default/content&id_object=' + loData.id_object + '&course_id=' + this.courseInfo.course_id;

        // Launch the htmlpage
        this.inline(launchLink, loData);

    },

    lightbox: function(url, loData) {

        $.fancybox.open([{
            type: 'iframe',
            href: url
        }], {
            padding: 0,
            margin: ( /iPad/.test(navigator.userAgent) ? [30, 0, 0, 0] : [33, 10, 5, 10] ),
            tpl: {
                closeBtn: '<a title="'+Yii.t('standard', '_CLOSE')+'" class="fancybox-docebo-close" href="javascript:;">'
                +Yii.t('standard', '_CLOSE')+'</a>'
            },
            autoSize : false,
            width: loData.width && loData.width > 100 ? loData.width+'px' : '100%',
            height : loData.height && loData.height > 100 ? loData.height+'px' : '100%',
            openEffect: 'elastic',
            closeEffect: 'fade',
            helpers: {
                overlay: { closeClick: false },
                doceboTitle: { title: (loData.type == 'sco' && loData.scormTitle !== 'undefined')? loData.scormTitle : loData.title }
            },
            beforeLoad: function() {
                if(loData.type == 'sco')
                    loadCheck();
            },
            afterLoad: function () {
                Arena.learningObjects.ajaxUpdate();
            },
            beforeClose: function () {
                $(this.inner).find("iframe").attr('src', '#');
                $.fancybox.showLoading();
            },
            afterClose: function () {
                $.fancybox.hideLoading();
                Launcher.showCommonLaunchpadFirstToPlay();
                Arena.showQuickNav();
            }
        });

    },


    inline: function(url, loData) {
        var title = loData.title;
        $('#player-arena-launchpad').show();
        $('#player-arena-launchpad').html(
            '<div class="row-fluid player-launchpad-header">'+
            '<h2>'+title+'</h2>'+
            '<a id="player-inline-close" class="player-close" href="#">'+Yii.t('standard', '_CLOSE')+'</a>'+
            '</div>'+
            '<div class="player-launchpad"></div>'
        );

        var iframe = $('<iframe />', {
            id: 'inline-'+loData.idOrganization,
            width: '100%',
            height: (loData.height ? loData.height+'px' : '680px'),
            'class': 'player-arena-inline ' + loData.type
        });
        iframe.appendTo('#player-arena-launchpad div.player-launchpad');
        iframe.attr('src', url);

    },

};