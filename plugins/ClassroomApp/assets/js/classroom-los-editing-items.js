/**********************************************************************************************
 * LoUploader
 *
 *
 * Serves the LO creation process when File must be uploaded: Scorm, Tincan, File, Video, ...
 *
 */
var LoUploader = function () {
    //this.globalListeners();
};

LoUploader.prototype = {

    uploadFilters: [],
    hide: function() {
        $('#player-arena-uploader-panel').hide();
    },
    show: function(oSettings) {
        Arena.hideAllArenaContent();
        var $panel = $('#player-arena-uploader-panel');
        $panel.show();
        if (oSettings && oSettings.title) {
            $panel.find('.header').html(oSettings.title);
        }
    },
    lo_type: null,
    parent_id: null,

    /**
     * Ask plUploader to re-render; used to fight IE super browser :-)
     */
    refresh: function() {
        this.pluploader.refresh();
    },

    // Started after _upload partial view is loaded
    run: function() {

        // Create uploader instance
        var pl_uploader = new plupload.Uploader({
            unique_names: true, // If true, you will find the PHYSICAL file name in  $_REQUEST['file']['target_name'], otherwise: $_REQUEST['file']['name']
            chunk_size: '1mb',
            multi_selection: false,
            runtimes: 'html5,flash',
            browse_button: 'pickfiles',
            container: 'player-pl-uploader',
            drop_element: 'player-pl-uploader-dropzone',
            max_file_size: '1024mb',
            url: Arena.uploadFilesUrl,
            multipart: true,
            multipart_params: {
                course_id: Arena.course_id,
                YII_CSRF_TOKEN: Arena.csrfToken
            },
            flash_swf_url: Arena.pluploadAssetsUrl + '/plupload.flash.swf',
            filters: Arena.uploader.uploadFilters
        });

        this.pluploader = pl_uploader;

        // Init event hander
        pl_uploader.bind('Init', function(up, params) {

        });

        // Run uploader
        pl_uploader.init();


        /**
         * Check the upload feedback (info)
         * Normally "info" is:  Object {response: "{"jsonrpc" : "2.0", "result" : null, "id" : "id"}", status: 200}
         * In case of some errors, it is changing to: Object {response: "", status: 0}
         * We analyze this to decide if upload is going well or not
         */
        var checkInfo = function(up, file, info) {
            if (info.response === "" || info.status === 0 || info.status >= 400) {
                up.trigger('Error', {
                    code : plupload.HTTP_ERROR,
                    message : Yii.t('standard', 'Error occured during file upload, please check your connection and try again.'),
                    file : file,
                    status : info.status
                })
                $('#player-uploaded-cancel-upload').click();
                return false;
            }
            return true;
        };


        pl_uploader.bind('ChunkUploaded', function(up, file, info){
            checkInfo(up, file, info);
        });


        pl_uploader.bind('FileUploaded', function(up, file, info){
            checkInfo(up, file, info);
        });


        // Just before uploading file
        pl_uploader.bind('BeforeUpload', function(up, file) {
            $('#player-save-uploaded-lo').addClass("disabled");

            $('.upload-button').hide();
            $('.upload-progress').show();
            $('.player-uploaded-percent').show();
            if(typeof(bootstro) != "undefined")
                bootstro.stop('bootstroAddBlockLauncher');
        });


        // Handle 'files added' event
        pl_uploader.bind('FilesAdded', function(up, files) {
            // in edit mode, mark the file as a new upload
            $('#player-arena-uploader').find('#file-new-upload').val(1);

            this.files = [files[0]];
            $("#player-uploaded-filename").html(files[0].name);
            $("#player-uploaded-filesize").html( plupload.formatSize(files[0].size) );
            pl_uploader.start();
        });

        // Upload progress
        pl_uploader.bind('UploadProgress', function(up, file) {
            $('#uploader-progress-bar').css('width', file.percent + '%');
            $('#player-uploaded-percent-gauge').html('&nbsp;' + file.percent + '%');
        });

        // On error
        pl_uploader.bind('Error', function(up, error) {
            $('#player-save-uploaded-lo').removeClass("disabled");
            Docebo.Feedback.show('error', error.message, true, false);
        });

        // On Upload complete
        pl_uploader.bind('UploadComplete', function(up, files){
            $('.docebo-progress')
                .addClass("progress-success")
                .removeClass("progress-striped");
            $('#player-save-uploaded-lo').removeClass("disabled");
            $('#player-uploaded-cancel-upload').hide();
        });

        // cancel upload cancel
        $('#player-uploaded-cancel-upload').on('click', function(e){
            e.preventDefault();

            // Inline Upload area may have Tinymce attached to 'file-description' which must be destroyed
            // TinyMce.removeEditorById('file-description');

            $('#player-save-uploaded-lo').removeClass("disabled");
            if (pl_uploader) {
                pl_uploader.stop();
                pl_uploader.splice();
            }
            $('.upload-button').show();
            $('.upload-progress').hide();
        });

        // ON CANCEL Upload Button
        $('#player-cancel-uploaded-lo').on('click', function(e) {
            e.preventDefault();

            // Inline Upload area may have Tinymce attached to 'file-description' which must be destroyed
            TinyMce.removeEditorById('file-description');

            // Destroy what we have done
            if (pl_uploader) {
                pl_uploader.stop();
                pl_uploader.destroy();
            }
            $("#player-arena-uploader").html("");
            Arena.setEditCourseMode();
            return false;
        });


        // On SAVE BUTTON click (actual saving of the LO), ajax-submit the form
        $('#upload-lo-form').submit(function() {

            // Button may still be disabled (upload not finished??)
            if ($(this).hasClass('disabled')) {
                return false;
            }

            var $playerArenaUploader = $('#player-arena-uploader');
            var idOrg = parseInt( $playerArenaUploader.find('#file-id-org').val() );
            var isNewUpload = parseInt( $playerArenaUploader.find('#file-new-upload').val() );
            var theFile = false;
            var videoSource = $playerArenaUploader.find('input[name=video-source]:checked').val();

            if (!isNaN(idOrg) && !isNewUpload || videoSource == 1) {
                // do not check if a new file was uploaded
            } else {
                if (pl_uploader.files.length <= 0) {
                    Docebo.Feedback.show('error', Yii.t('error', 'Select a file to upload!'));
                    return false;
                }
                theFile = pl_uploader.files[0];

                // Make some checks
                if (theFile.percent < 100) {
                    Docebo.Feedback.show('error', 'Failed to upload 100% of the file!');
                    return false;
                }
            }

            var title = "", $title = $playerArenaUploader.find('#file-title');
            if ($title.length > 0) {
                title = $title.val();
                if (title.length <= 0) {
                    Docebo.Feedback.show('error', 'Enter a title!');
                    return false;
                }
            }

            var description = "";
            var $description = $playerArenaUploader.find('textarea[id^="file-description"]');
            if ($description.length > 0) {
                description = $description.val();
            }

            var videoUrl = "";
            var $videoUrl = $playerArenaUploader.find('input[name=video_url]').val();
            if(videoSource == 1) {
                if ($videoUrl.length > 0) {
					if(!validateVideoUrl($videoUrl)){
						Docebo.Feedback.show('error', Yii.t('standard', 'The URL is not valid'));
						return false;
					}
                    videoUrl = $videoUrl;
                } else {
                    Docebo.Feedback.show('error', 'Enter a training matirial URL');
                    return false;
                }
            }

            // URI Encode text data (because of IE!)
            // Do NOT forget to URL Decose PHP side
            theFile.name 	= encodeURIComponent(theFile.name);
            title = encodeURIComponent(title);
            description = encodeURIComponent(description);

            var dataObject = {
                course_id: Arena.course_id,
                parent_id: Arena.uploader.parent_id,
                file: theFile,
                title: title,
                description: description,
                videoSource: videoSource,
                videoUrl: videoUrl
            };

            var formOptions = {
                dataType: 'json',
                data: dataObject,
                beforeSubmit: function(a, form, o) {
                    // No need to show 'Upload...' message for small files
                    if (theFile.size > 30000) {
                        $('#player-arena-uploader').slideUp();
                        $('#upload-stuff-message').slideDown();
                    }
                    return true;
                },

                beforeSend: function(x) {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType('application/x-www-form-urlencoded; charset=UTF-8');
                    }
                    $('#upload-lo-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                },

                success: function(res) {
                    // Destroy TinyMce editor and switch to Admin view
                    TinyMce.removeEditorById('file-description');
                    Arena.setEditCourseMode();
                    if (res.success) {
                        $('#upload-stuff-message').slideUp();
                        Docebo.Feedback.show('success', res.data.message);
                        Arena.learningObjectsManagement.ajaxUpdate('all', function(){
                            $(document).trigger('lo-upload-complete');
                        });
                    }
                    else {
                        $('#upload-stuff-message').slideUp();
                        $('#player-arena-uploader').slideDown();
                        Docebo.Feedback.show('error', res.message);
                    }
                    $('#upload-lo-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                }
            };

            $('#upload-lo-form').ajaxSubmit(formOptions);


            return false;

        });

    },

    globalListeners: function() {
        $('#player-arena-container').on('change', 'input[name=video-source]', function (){
            if($(this).val() == 1){
                $('#upload-file').addClass('hidden');
                $('#video-seekable').addClass('hidden');
                $('#video-subtitles').addClass('hidden');
                $('#video-url').removeClass('hidden');
            } else{
                $('#video-url').addClass('hidden');
                $('#upload-file').removeClass('hidden');
                $('#video-seekable').removeClass('hidden');
                $('#video-subtitles').removeClass('hidden');
            }
        });

        $('#player-arena-container').on('keyup change paste', 'input[name=video_url]', function(){
            setTimeout(function(){
                var element = $('#player-arena-container input[name=video_url]');
                var value = element.val();
                var infoBox = element.parent().find('.url-valudator-info');

                if(validateVideoUrl(value)){
                    infoBox.html('<i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>');
					element.val(value.trim());
                } else{
                    infoBox.html('<i rel="tooltip" data-original-title="' + Yii.t('standard', 'The URL is not valid') + '" class="fa fa-times fa-2x" aria-hidden="true"></i>');
                }

                infoBox.find('i').tooltip('show');
            }, 100);
        });
        // Bind LO Upload clicks
        $('#player-arena-container').on('click', '.lo-upload', function(e) {
            e.preventDefault();

            var $this = $(this);
            var lo_type = $this.data('lo-type');

            switch (lo_type) {
                case 'video':
                    Arena.uploader.uploadFilters = [
                        //{title: "Video files", extensions: "mp4,ogv,webm,flv,avi"}
                        {title: "Video files", extensions: Arena.videoExtensionsWhitelist}
                    ];
                    break;
                case 'deliverable':
                    Arena.uploader.uploadFilters = [
                        {title: "Video files", extensions: "mp4,ogv,webm,flv"}
                    ];
                    break;
                default:
                    Arena.uploader.uploadFilters = [];
                    break;
            }

            var folder_key = Arena.learningObjectsManagement.getActiveFolderKey();
            if (folder_key === false) {
                Docebo.Feedback.show('error', 'Please select a folder');
                return false;
            }

            Arena.hideAllArenaContent();
            var uploaderSettings = {title:$this.data('title')};
            Arena.uploader.show(uploaderSettings);

            var nodeElement = $this.parent().parent().parent().parent().parent();
            var versionContext = nodeElement.find('.version-info');

            var url = Arena.uploadDialogUrl;

            //Check here if we need to edit a version of a central Repo Object!!!
            // If so - change the URL
            var runUploader = true;
            if(versionContext.length > 0){
                url = $this.attr('href');
                runUploader = false;
            }

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    course_id: Arena.course_id,
                    lo_type: lo_type,
                    folder_key: folder_key,
                    id_org: $(this).data('id-object') // only for editing
                },
                beforeSend: function() {

                }
            }).done(function(res) {
                if (res.success) {
                    Arena.uploader.lo_type = lo_type;
                    Arena.uploader.parent_id = folder_key;
                    //$('#player-arena-uploader').html(res.html);
                    $('#player-lo-edit-inline-panel').html(res.html).show();

                    // PlUploader is good to be run AFTER HTML is rendered!!
                    if(runUploader){
                        Arena.uploader.run();
                    }
                    //Arena.uploader.pluploader.refresh();
                }
            }).always(function() {
                Docebo.scrollTo('#arena-board-top');
            }).fail(function(jqXHR, textStatus, errorThrown) {
                //...
            });

        });

    }
};

/*********************************************************************************************\
 * Central Repository
 *
 * Show Create/Edit Central Repository dialog.
 */

var LoCentralRepoEdit = function(options){
    if(typeof options !== "undefined"){
        this.init(options);
    }
};

LoCentralRepoEdit.prototype = {
    init: function(options){
        this.editUrl = options.editUrl;
    },

    formListeners: function(){
        var $this = this;

        $(document).off('change', 'form#edit-shared-lo-form #enable-custom-view').on('change', 'form#edit-shared-lo-form #enable-custom-view', function(e){
            if($(this).is(':checked')){
                $(this).parent().parent().parent().find('.overlay').css('display', 'none');
            } else{
                $(this).parent().parent().parent().find('.overlay').css('display', 'block');
            }
        });

        $(document).off('click', '.player-central-lo-shared-settings #player-cancel-uploaded-lo').on('click', '.player-central-lo-shared-settings #player-cancel-uploaded-lo', function(){
            $this.resetPanel();
        });

        $(document).off('submit', 'form#edit-shared-lo-form').on('submit', 'form#edit-shared-lo-form', function(e){
            e.preventDefault();
            var options = {
                dataType: 'json',
                success: function (res) {
                    if (res.success == true) {
                        if (res.data.showConfirmationDialog == true) {
                            var dialogId = 'version-change-lo-dialog';
                            $('<div/>').dialog2({
                                autoOpen: true, // We will open the dialog later
                                id: dialogId,
                                title: Yii.t('standard', 'Version change'),
                                modalClass: 'version-change-modal'
                            });
                            $('#' + dialogId).html(res.html);
                        } else {
                            Docebo.Feedback.show('success', res.message);
							Arena.learningObjectsManagement.ajaxUpdate('all', function () {
								$(document).trigger('lo-upload-complete');
							});
                            Arena.setEditCourseMode();
                        }
                    }
                    else
                        Docebo.Feedback.show('error', res.message);
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });

        $(document).off('click', '.version-change-modal form#save-shared-lo-form input[name="confirm-button"]').on('click', '.version-change-modal form#save-shared-lo-form input[name="confirm-button"]', function(e){
            e.preventDefault();
            var options = {
                dataType: 'json',
                success: function (res) {
                    $('#version-change-lo-dialog').dialog2('close');
                    Docebo.Feedback.show('success', Yii.t('standard', '_OPERATION_SUCCESSFUL'));
                    $this.resetPanel();
                    if (res.success == true) {
                        Docebo.Feedback.show('success', res.message);
						Arena.learningObjectsManagement.ajaxUpdate('all', function () {
							$(document).trigger('lo-upload-complete');
						});
                    } else
                        Docebo.Feedback.show('error', res.message);
                }
            };

            $('.version-change-modal form#save-shared-lo-form').ajaxSubmit(options);
            return false;
        });

        $(document).off('click', '.version-change-modal #player-cancel-save-version').on('click', '.version-change-modal #player-cancel-save-version', function(e){
            $('#version-change-lo-dialog').dialog2('close');
            $this.resetPanel();
        });
    },

    resetPanel: function(){
        $("#player-arena-uploader").html("");
        Arena.setEditCourseMode();
    },

    globalListeners: function(){
        var $this = this;
        var jwizard = new jwizardClass({
            dialogId		: 'push-to-course-dialog',
            dispatcherUrl	: Docebo.createAbsoluteAdminUrl('centralrepo/CentralRepoPushLOToCourse/wizard', []),
            alwaysPostGlobal: true,
            debug			: false
        });

        $(document).on('dialog2.closed', '#push-lo-to-single-course', function(){
            Arena.learningObjectsManagement.ajaxUpdate('all', function () {
                $(document).trigger('lo-upload-complete');
            });
        });
    }
};

/**********************************************************************************************
 * Organizer
 *
 *
 * Create NEW folder dialog.
 *
 */
var LoOrganizer = function (options) {
    this.init(options);
    //this.globalListeners();
};

LoOrganizer.prototype = {

    init: function(){

    },

    show: function(orgId){
        var folder_key = Arena.learningObjectsManagement.getActiveFolderKey();
        if (folder_key === false) {
            Docebo.Feedback.show('error', 'Please select a folder');
            return false;
        }

        var url = Arena.createFolderDialogUrl + '&parent_id=' + folder_key;
        if (orgId) {
            url += '&org_id=' + orgId;
        }
        var inlineEditPanel = $('#player-lo-edit-inline-panel');

        $.post(url, {}, function(response) {
            if (response.success == true) {
                Arena.hideAllArenaContent();
                inlineEditPanel.html(response.html).show();
                Arena.organizer.formListeners();
            } else {
                Docebo.Feedback.show('error', response.message);
            }
        }, 'json');
    },

    formListeners: function(){

        $('#cancel_save_newfolder').click(function(e) {
            e.preventDefault();
            Arena.setEditCourseMode();
        });

        $('#create-newfolder-form').ajaxForm({
            dataType: 'json',
            success: function(result) {
                if (result.success == true) {
                    Arena.learningObjectsManagement.ajaxUpdate();
                    Arena.setEditCourseMode();
                } else {
                    Docebo.Feedback.show('error', result.message);
                }
                $('#create-newfolder-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
            },
            beforeSubmit: function (arr, $form, options) {
                $('#create-newfolder-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                return true;
            }
        });
    },

    globalListeners: function() {
        // On CREATE FOLDER
        $('#create-folder').on('click', function(e){
            e.preventDefault();
            Arena.organizer.show();
        });

        $('#player-arena-container').on('click', '.lo-edit-folder', function(e){
            var incomingFolderIdObject = $(this).data('id-object');
            e.preventDefault();
            Arena.organizer.show(incomingFolderIdObject);
        });

    }
};

/**********************************************************************************************
 * Deliverable
 *
 *
 * Show Create/Edit Deliverable dialog: title and editable area. Handles the submition process.
 *
 */
var LoDeliverable = function (options) {
    this.init(options);
    //this.globalListeners();
};

LoDeliverable.prototype = {

    init: function (options) {
        this.editDeliverableUrl = options.editDeliverableUrl;
    },

    show: function (idResource) {

        Arena.hideAllArenaContent();
        //var panel = $('#player-arena-htmlpage-editor-panel');
        var inlineEditPanel = $('#player-lo-edit-inline-panel');
        $.post(this.editDeliverableUrl, {
            idResource: idResource,
            course_id: Arena.course_id
        }, function (response) {
            if (response.success == true) {
                //panel.show().find('.content').html(response.html);
                inlineEditPanel.html(response.html).show();
                TinyMce.attach('#player-arena-deliverable-editor-textarea', 'standard_embed', {height: 300});
                Arena.deliverable.formListeners();
                Docebo.scrollTo('#arena-board-top');
            }
        }, 'json');
    },

    formListeners: function () {
        // ON Cancel button click
        $("#cancel_save_deliverable").on("click", function (e) {
            e.preventDefault();
            TinyMce.removeEditorById('player-arena-deliverable-editor-textarea');
            Arena.setEditCourseMode();
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#deliverable-editor-form').submit(function () {
            var options = {

                data:     {
                    course_id: Arena.course_id,
                    parent_id: Arena.learningObjectsManagement.getActiveFolderKey()
                },
                dataType: 'json',
                success:  function (res) {
                    if (res.success == true) {
                        Docebo.Feedback.show('success', res.data.message);
                        Arena.learningObjectsManagement.ajaxUpdate('all', function () {
                            $(document).trigger('lo-upload-complete');
                        });
                        Arena.setEditCourseMode();
                        TinyMce.removeEditorById('player-arena-deliverable-editor-textarea');
                        //$(document).trigger('lo-upload-complete');
                    } else {
                        Docebo.Feedback.show('error', res.message);
                    }
                    $('#deliverable-editor-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                },
                beforeSubmit: function (arr, $form, options) {
                    $('#deliverable-editor-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });
    },

    globalListeners: function () {
        $('#player-manage-add-lo-button').on('click', '.lo-create-deliverable', function (e) {
            e.preventDefault();
            Arena.deliverable.show();
        });
    }
};

/**********************************************************************************************
 * HtmlPage
 *
 *
 * Show Create/Edit HTML Page dialog: title and editable area. Handles the submition process.
 *
 */
var LoHtmlEdit = function (options) {
    this.init(options);
    //this.globalListeners();
};

LoHtmlEdit.prototype = {

    init: function(options) {
        this.editHtmlPageUrl = options.editHtmlPageUrl;
    },

    show: function(idResource){

        Arena.hideAllArenaContent();
        //var panel = $('#player-arena-htmlpage-editor-panel');
        var inlineEditPanel = $('#player-lo-edit-inline-panel');
        //xxxx
        $.post(this.editHtmlPageUrl, {idResource: idResource, course_id: Arena.course_id}, function (response) {
            if (response.success == true) {
                //panel.show().find('.content').html(response.html);
                inlineEditPanel.html(response.html).show();
                TinyMce.attach('#player-arena-htmlpage-editor-textarea', 'standard_embed', {height: 300});
                Arena.htmledit.formListeners();
                Docebo.scrollTo('#arena-board-top');
            }
        }, 'json');
    },

    formListeners: function() {
        // ON Cancel button click
        $("#cancel_save_htmlpage").on("click", function(e){
            e.preventDefault();
            TinyMce.removeEditorById('player-arena-htmlpage-editor-textarea');
            Arena.setEditCourseMode();
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#htmlpage-editor-form').submit(function(){
            var options = {

                data: {
                    course_id: Arena.course_id,
                    parent_id: Arena.learningObjectsManagement.getActiveFolderKey()
                },
                dataType: 'json',
                success: function(res) {
                    if (res.success == true) {
                        Docebo.Feedback.show('success', res.data.message);
                        Arena.learningObjectsManagement.ajaxUpdate('all', function(){
                            $(document).trigger('lo-upload-complete');
                        });
                        Arena.setEditCourseMode();
                        TinyMce.removeEditorById('player-arena-htmlpage-editor-textarea');
                        //$(document).trigger('lo-upload-complete');
                    } else {
                        Docebo.Feedback.show('error', res.message);
                    }
                    $('#htmlpage-editor-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                },
                beforeSubmit: function (arr, $form, options) {
                    $('#htmlpage-editor-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                }
            };

            $(this).ajaxSubmit(options);
            return false;
        });
    },

    globalListeners: function() {
        $('#player-manage-add-lo-button').on('click', '.lo-create-htmlpage', function(e){
            e.preventDefault();
            Arena.htmledit.show();
        });
    }
};

/**********************************************************************************************
 * POLL
 *
 *
 * Create dialog and submisison. No EDIT!
 *
 */
var LoPoll = function (options) {
    this.init(options);
    //this.globalListeners();
};

LoPoll.prototype = {
    init: function(options) {
    },

    hide: function(){
        $('.player-arena-poll-create-panel').hide();
    },

    show:  function(){
		var thisShow = this;
        var inlineEditPanel = $('#player-lo-edit-inline-panel');
        $.post(Arena.createPollUrl, {}, function(response) {
            if (response.success == true) {
                inlineEditPanel.html(response.html).show();
                TinyMce.attach('#player-arena-poll-editor-textarea',{height: 300});
                thisShow.formListeners();
                Docebo.scrollTo('#arena-board-top');
            }
        }, 'json');
    },

    formListeners: function() {
        // ON Cancel button click
        $("#cancel_save_poll").on("click", function(e){
            e.preventDefault();
            Arena.setEditCourseMode();
            TinyMce.removeEditorById('player-arena-poll-editor-textarea');
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#poll-create-form').submit(function(){
            var options = {
                data: {
                    course_id: Arena.course_id,
                    parent_id: Arena.learningObjectsManagement.getActiveFolderKey()
                },
                dataType: 'json',
                success: function(res) {
                    if (res.success == true) {
                        Docebo.Feedback.show('success', res.data.message);
                        Arena.learningObjectsManagement.ajaxUpdate(function(){
                            $(document).trigger('lo-upload-complete');
                        });

                        redirectUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=poll/default/edit&id_object=' + res.data.organizationId + '&course_id=' + Arena.course_id;
                        window.location.href = redirectUrl;
                    } else {
                        $('#poll-create-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                    }
                },
                beforeSubmit: function (arr, $form, options) {
                    $('#poll-create-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                }
            };
            $(this).ajaxSubmit(options);
            return false;
        });
    },

    globalListeners: function() {
        // On CREATE POLL
        $('#player-manage-add-lo-button').on('click', '.lo-create-poll', function(e){
            e.preventDefault();
            Arena.hideAllArenaContent();
            Arena.poll.show();
        });

    }
};

/**********************************************************************************************
 * TEST
 *
 *
 *  Create TEST Dialog and submission. NO EDIT!
 *
 */
var LoTest = function (options) {
    this.init(options);
    //this.globalListeners();
};

LoTest.prototype = {

    init: function (options) {
    },

    hide: function() {
        $('.player-arena-test-create-panel').hide();
    },

    show: function(){
		var thisShow = this;	
        var inlineEditPanel = $('#player-lo-edit-inline-panel');
        $.post(Arena.createTestUrl, {}, function(response) {
            if (response.success == true) {
                inlineEditPanel.html(response.html).show();
                TinyMce.attach('#player-arena-test-editor-textarea', {height: 300});
                thisShow.formListeners();
                Docebo.scrollTo('#arena-board-top');
            }
        }, 'json');
    },

    formListeners: function() {
        // ON Cancel button click
        $("#cancel_save_test").on("click", function(e){
            e.preventDefault();
            Arena.setEditCourseMode();
            TinyMce.removeEditorById('player-arena-test-editor-textarea');
        });

        // AJAX-Submit the form (jQuery forms is already loaded)
        $('#test-create-form').submit(function(){
            var options = {
                data: {
                    course_id: Arena.course_id,
                    parent_id: Arena.learningObjectsManagement.getActiveFolderKey()
                },
                dataType: 'json',
                success: function(res) {
                    if (res.success == true) {
                        Docebo.Feedback.show('success', res.data.message);
                        Arena.learningObjectsManagement.ajaxUpdate();
                        TinyMce.removeEditorById('player-arena-test-editor-textarea');
                        redirectUrl = Docebo.lmsAbsoluteBaseUrl + '/index.php?r=test/default/edit&id_object=' + res.data.organizationId + '&course_id=' + Arena.course_id;
                        window.location.href = redirectUrl;
                    } else {
                        $('#test-create-form input[type=submit]').removeAttr('disabled').removeClass("disabled");
                    }
                },
                beforeSubmit: function (arr, $form, options) {
                    $('#test-create-form input[type=submit]').attr('disabled','disabled').addClass("disabled");
                    return true;
                }
            };
            $(this).ajaxSubmit(options);
            return false;
        });
    },

    globalListeners: function() {
        // On "CREATE TEST"
        $('#player-manage-add-lo-button').on('click', '.lo-create-test', function(e){
            e.preventDefault();
            Arena.hideAllArenaContent();
            Arena.test.show();
        });
    }
};