var isAmPmSupported = false; //if we want to enable "am/pm" hour type, just set this to true

function validateHhMm() {
	/*
	 //NOTE: that date format cannot be used in any browser (e.g. Firefox requires strictly standard input for new Date(...) )
	 var timeBegin = new Date('2015-01-01 ' + $('#time-begin').val());
	 var timeEnd = new Date('2015-01-01 ' + $('#time-end').val());
	 var breakBegin = new Date('2015-01-01 ' + $('#break-begin').val());
	 var breakEnd = new Date('2015-01-01 ' + $('#break-end').val());
	 */

	function decodeInput(value) {
		var arr, hours, minutes;
		arr = value.replace('.', ':').split(':');
		if (arr.length == 2) {
			hours = parseInt(arr[0]);
			minutes = parseInt(arr[1]);
			if (isAmPmSupported) {
				var isAM = (arr[1].indexOf('am') > 0), isPM = (arr[1].indexOf('pm') > 0);
				if (isAM) { if (hours >= 12) hours = 0; }
				if (isPM) { if (hours < 12) hours += 12; }
			}
		} else {
			hours = 0;
			minutes = 0;
		}
		return new Date(2015, 1, 1, hours, minutes);
	};


	var timeBegin = decodeInput( $('#time-begin').val() );
	var timeEnd = decodeInput( $('#time-end').val() );
	var breakBegin = decodeInput( $('#break-begin').val() );
	var breakEnd = decodeInput( $('#break-end').val() );


	clearInputErrors(); //clear all current input errors and validate again

	var re_1 = /^(([01]{0,1}\d{1})|(2[0-3]{1}))(:|\.)([0-5]{1}\d{1})$/;
	var re_2 = /^(([0]{0,1}[1-9]{1})|(1[0-2]{1}))(:|\.)([0-5]{1}\d{1})([ap]m)$/;

	//validate each input if it's empty or invalid format
	$("#time-begin,#time-end,#break-begin,#break-end").each(function() {
		var check, value = $(this).val().replace('.', ':');
		var isEmpty = (value == '');
		var is24h = value.match(re_1);
		var is12h = value.match(re_2);

		switch ($(this).attr('id')) {
			case 'time-begin':
			case 'time-end':
				if (isAmPmSupported) { check = (!is24h && !is12h); } else { check = (!is24h); }
				if (check) {
					setInputErrorById($(this).attr('id'));
				}
				break;
			case 'break-begin':
			case 'break-end':
				if (isAmPmSupported) { check = (!isEmpty && !is24h && !is12h); } else { check = (!isEmpty && !is24h); }
				if (check) {
					setInputErrorById($(this).attr('id'));
				}
				break;
		}

	});

	//check if we have start and end time correctly added and only then check the break interval as well
	if (timeBegin >= timeEnd) {
		setInputErrorById('time-begin');
		setInputErrorById('time-end');
	} else {
		if ($('#break-begin').val() == '' && $('#break-end').val() == '') {
			//both break times are empty, all is ok
			//clear only break times input errors
			$("#break-begin,#break-end").each(function() { $(this).css('background-color', ''); });
			$("#create-session-date").attr("disabled",false);
		} else {
			//at least one break time is filled
			if (breakBegin >= breakEnd) { //this is wrong for sure
				setInputErrorById('break-begin');
				setInputErrorById('break-end');
			} else {
				if (($('#break-begin').val() == '' && $('#break-end').val() != '') || ($('#break-begin').val() != '' && $('#break-end').val() == '')) {
					//only if both break begin and end are empty then we can validate the inputs
					setInputErrorById('break-begin');
					setInputErrorById('break-end');
				} else {
					if ($('#break-begin').val() != '' && (breakBegin <= timeBegin || breakBegin >= timeEnd)) {
						setInputErrorById('break-begin');
					}
					if ($('#break-end').val() != '' && (breakEnd >= timeEnd || breakEnd <= timeBegin)) {
						setInputErrorById('break-end');
					}
				}
			}
		}
	}

}

function setInputErrorById(fieldId) {
	$("#"+fieldId).css('background-color', '#fba'); //TODO: do not set style directly, use CSS classes instead !!!
	$("#create-session-date").attr("disabled",true);
}

function clearInputErrors() {
	$("#time-begin,#time-end,#break-begin,#break-end").each(function(){
		$(this).css('background-color', '');
	});
	$("#create-session-date").attr("disabled",false);
}

function ValidateHour(e) {

	//arrow keys have no effect on input, ignore them
	switch (e.keyCode) {
		case 37: //left
		case 38: //up
		case 39: //right
		case 40: //down
			return;
	}

	var $t = $(this);

	//validate input value, remove invalid characters
	if (isAmPmSupported) {
		$t.val($t.val().replace(/[^0-9:\.apm]/g, ''));
	} else {
		$t.val( $t.val().replace(/[^0-9:\.]/g, '') );
	}

	var value = $t.val();
	var checker = (isAmPmSupported ? /^\d{1}(:|\.)\d{2}(([ap]{1})|([ap]m))?$/ : /^\d{1}(:|\.)\d{2}$/);
	if (value.match(checker)) { value = '' + '0' + '' + value; }

	var length = value.length;
	if (length > 5) {

		if (isAmPmSupported) {
			if (length == 6) {
				var t = value.substr(5, 1);
				if (t != 'a' && t != 'p') {
					$t.val(value.substr(0, 5));
				}
			} else if (length == 7) {
				var t = value.substr(5, 2);
				if (t != 'am' && t != 'pm') {
					$t.val(value.substr(0, 5));
				}
			} else {
				$t.val(value.substr(0, 5));
			}
		} else {
			$t.val(value.substr(0, 5));
		}

	}
	validateHhMm();
}