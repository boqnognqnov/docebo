Object.size = function (obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

var SessionManager = {
	/* Array of session dates for the current session */
	sessionDates: null, /*<?= $this->getSessionDatesAsJson($model) ?>,*/

	/* Date picker object */
	datePicker: null,

	/* Currently selected session date */
	selectedDate: null,

	/* Previously selected session date */
	previousDate: null,

	/* Array of months */
	months: null, /*<?= $this->getLocalizedMonthNames() ?>,*/

	/* Array of classrooms to fill in the selection menu */
	classrooms: null, /*<?= $this->getClassroomsAsJson() ?>,*/

	/* Array of locations */
	locations: null, /*<?= $this->getLocationsAsJson() ?>,*/

	/* Array of timezone names with their GMT offsets */
	timezoneOffsets: null, /*<?= $this->getTimezoneOffsetsAsJson() ?>,*/



	currentLanguage: null,
	translations: {},

	/* Initialization function for the modal create/edit session manager */
	init: function (params) {

		this.sessionDates = params.sessionDates;
		this.datePicker = params.datePicker;
		this.months = params.months;
		this.classrooms = params.classrooms;
		this.locations = params.locations;
		this.timezoneOffsets = params.timezoneOffsets;
		this.currentLanguage = params.currentLanguage;
		this.currentTimezone = params.currentTimezone;
		this.checkClassroomAvailability = params.checkClassroomAvailability;
		this.checkClassroomAvailabilityUrl = params.checkClassroomAvailabilityUrl;
		this.isGodAdmin = params.isGodAdmin;
		this.translations = params.translations;


		try {

			// Apply style to inputs in the form, that is in a modal
			$('.modal input').styler();

			// Set current date to today
			this.selectedDate = new Date();

			// Fill in the calendar with days already set up for this session
			this.initCalendar();

			// Install click handlers on form buttons
			this.initDateForms();

            // Install click handlers on evaluation switch
            this.initEvaluationSwitcher();

			// Install tabs click handlers
			var manager = this;
			$('.edit-session-tabs > ul.nav.nav-tabs li').bind('click', function (e) {
				e.preventDefault();

                // hide all tabs
                $(this).closest('div').find('.tab-content .tab-pane').hide();
                $(this).parent().find('li').removeClass('active');
                $(e.currentTarget).addClass('active');

                // Show the right tab and hide the other tab
				switch ($(e.target).attr('data-target')) {
					case '#sessionForm-details':
					    // Show session details form and hide dates form
						$('#sessionForm-details').show();

						// Hide datepicker
						manager.datePicker.hide();

						// Hide the blank slate under calendar
						$('#popup_bootstroAddAnotherDate').hide();
						break;

					case '#sessionForm-dates':
					    // Show session dates form
						$('#sessionForm-dates').show();

						// Show datepicker
						manager.datePicker.show();

						// Refresh the date form
						manager.showDateForm();
						break;

                    case '#sessionForm-evaluation':
                        $('#sessionForm-evaluation').show();

                        // Hide datepicker
                        manager.datePicker.hide();

                        break;
				}
			}).eq(0).find('a').click();
		} catch (e) {
			console.log(e);
		}
	},

	/* Converts a JS date object into a mysql string yyyy-MM-dd */
	getMysqlDate: function (date) {
		var d = date.getUTCDate();
		var m = date.getUTCMonth() + 1;
		var y = date.getUTCFullYear();
		d = (d < 10 ? '0' : '') + d;
		m = (m < 10 ? '0' : '') + m;
		return y + '-' + m + '-' + d;
	},

	/* Returns the suffix of the day of month */
	getMonthDaySuffix: function (n) {
		if (n >= 11 && n <= 13)
			return "th";
		else {
			switch (n % 10) {
				case 1:
					return "st";
				case 2:
					return "nd";
				case 3:
					return "rd";
				default:
					return "th";
			}
		}
	},

	/* Returns date name in long format */
	getLongDateName: function (date) {
		var suffix = (this.currentLanguage == 'english' ? this.getMonthDaySuffix(date.getUTCDate()) : '');
		var formattedDay = this.months[date.getUTCMonth()] + " " + date.getUTCDate() + suffix + ", " + date.getUTCFullYear();
		return formattedDay;
	},

	/* Sets up the days in the calendar widget */
	initCalendar: function () {
		var manager = this;
		this.datePicker = $('#session-dates-calendar').bdatepicker({
			format: 'yyyy-mm-dd',
			inline: true,
			language: yii.language,
			onRender: function (date) {
				var mysql_date = manager.getMysqlDate(date);
				if (mysql_date in manager.sessionDates)
					return ' session-day';
				else
					return '';
			}
		}).data('datepicker');

		// fix calendar position
		var placeDatePicker = function(){manager.datePicker.place()};
		$(window).on('resize', placeDatePicker);
		// Customize datepicker with our class
		this.datePicker.picker.addClass('session-dates');

		// Intercept dialog2.closed and destroy the calendar
		$(document).delegate(".modal-create-session", "dialog2.closed", function () {
			manager.datePicker.picker.remove();
			$(window).off('resize', placeDatePicker);
		});
		//the above may not work for some IE versions < 11. This patch should solve the issue.
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			$('.modal-create-session').on('remove', function () {
				if (manager && manager.datePicker) {
					manager.datePicker.picker.remove();
				}
			});
		}

		// Install day selection handler
		$('#session-dates-calendar').on('changeDate', function (ev) {
			manager.selectedDate = ev.date;
			manager.showDateForm();
		});
	},

	/* Updates the status of the form confirm button */
	updateConfirmButton: function () {
		if (Object.size(this.sessionDates) > 0) {
			// Session can be saved -> unlock all submit buttons
			$('.session-error').html('').hide();
			$('.session-form-actions > .submit-session-button').removeClass('disabled');
			$('#sessionForm-details .session-form-actions > .submit-session-button').val(this.translations._SAVE);
		}
		else {
			// Can't save session -> disable all submit buttons ...
			$('.session-form-actions > .submit-session-button').addClass('disabled');
			// ... with the exception of Details form (should show "Set up dates" and lead to the Session Dates form when clicked)
			$('#sessionForm-details .session-form-actions > .submit-session-button').val(this.translations._SET_UP_DATES);
			$('#sessionForm-details .session-form-actions > .submit-session-button').removeClass('disabled');
			$('.session-error').html(this.translations._MISSING_SESSION_DATE).show();
		}
	},

	/* Updates the status of the form confirm button */
	updateCreateSessionDateButton: function () {
		var manager = this;

		// Prepare the object to submit
		var formData = $('#session_form').serializeArray();
		$.each(manager.sessionDates, function (day, value) {
			for (field in manager.sessionDates[day]) {
				if (field != 'id_session')
					formData.push({
						name: "LtCourseSession[dates][" + day + "][" + field + "]",
						value: manager.sessionDates[day][field]
					});
			}
		});

		// Submit form via ajax
		$.ajax({
			dataType: 'json',
			type: 'POST',
			url: manager.checkClassroomAvailabilityUrl,
			cache: false,
			data: $.param(formData),
			success: function (data) {
				if (data.success == true) {
					$('.session-warning .message').html('').parent().hide();

					if(data.overlapping.length > 0 ){
						// Can't save session -> disable all submit buttons ...
						$('.session-form-actions > #create-session-date').addClass('disabled');
						var messages = [];
						$.each(data.overlapping,function(index,value){
							messages[value.session_id] = value.message;
						});
						$.each(messages,function(index,value){
							if(typeof value !== 'undefined'){
								$('.session-warning .message').append('<p>'+value+'</p>').parent().show();
							}
						});
						if(manager.isGodAdmin){
							$('.session-form-actions > #create-session-date').removeClass('disabled');
						}

					} else {
						$('.session-warning .message').html('').parent().hide();
						$('.session-form-actions > #create-session-date').removeClass('disabled');
					}


				} else {
					// Can't save session -> disable all submit buttons ...
					$('.session-form-actions > #create-session-date').addClass('disabled');
					// Show error message
					$('.session-warning .message').html('<span class="red">' + data.message + '</span>').parent().show();

					// Restore edit form
					//manager.toggleFormLoader(false);
				}
			},
			beforeSend: function () {
				$('.session-warning .message').html('').parent().hide();
			},
			fail: function (jqXHR, status, errorThrown) {
				// Show error message
				$('.session-error').html('<span class="red">' + this.translations_OPERATION_FAILURE + '</span>').show();

				// Restore edit form
				//manager.toggleFormLoader(false);
			}
		});
	},

	/* Initializes form buttons and other stuff */
	initDateForms: function () {
		var manager = this;
		// Install click handler on Add date form button
		$('#session-date-add').on('click', function (ev) {
			manager.showEditForm();
		});

		// Install click handler on "Select timezone"
		$('#select-timezone-button').on('click', function (ev) {
			$('#timezone-line').show();
		});

		// Install on change on location select
		$("#day-location").on('change', function (ev) {
			var currentLocation = $(this).val();
			manager.redrawClassroomDropdown(currentLocation);
		});

		$('#day-classroom,#day-calendar,#day-timezone').on('change',function(ev){
			if(manager.checkClassroomAvailability){
				manager.updateCreateSessionDateButton();
			}
		});

		$('#time-begin,#time-end').on('input',function(ev){
			setTimeout(function() {
				if(manager.checkClassroomAvailability){
					manager.updateCreateSessionDateButton();
				}
			}, 1000);

		});

		// Init edit form calendar
		$('.edit-day-input').bdatepicker({
			format: 'yyyy-mm-dd',
            language: yii.language,
			onRender: function (date) {
				var mysql_date = manager.getMysqlDate(date);
				if (mysql_date in manager.sessionDates)
					return ' disabled';
				else
					return '';
			}
		});

		$('.edit-day-input').each(function (index, elem) {
			$(elem).bdatepicker().data('datepicker').picker.addClass('day-inputs');
		});

		// Install day selection handler
		$('#day-calendar').on('changeDate', function (ev) {
			manager.previousDate = manager.selectedDate;
			manager.selectedDate = ev.date;
		});

		// Install click on calendar icon
		$(".date-icon").on('click', function (ev) {
			$(this).prev('input').bdatepicker().data('datepicker').show();
		});

		// Intercept dialog2.closed and destroy the calendar
		$(document).delegate(".modal-create-session", "dialog2.closed", function () {
			$(".day-inputs").each(function (index, elem) {
				$(elem).remove();
			});
		});
		//the above may not work for some IE versions < 11. This patch should solve the issue.
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			$('.modal-create-session').on('remove', function () {
				$(".day-inputs").each(function (index, elem) {
					$(elem).remove();
				});
			});
		}

		// Install click handler on the create date button
		$('#create-session-date').on('click', function (ev) {
			if($(this).hasClass('disabled')){
				return false;
			}
			if (manager.updateOrCreateSessionDate()) {
				// Refresh days calendar
				$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

				// Check if the empty session message should be displayed on not
				manager.updateConfirmButton();

				// Show view form
				manager.showViewForm();
			}
		});

		// Install click handler on the edit date button
		$('#edit-session-date').on('click', function (ev) {
			// Show edit form
			manager.showEditForm();
		});

		// Install click handler on cancel "edit session date"
		$('#cancel-edit-session-date').on('click', function (ev) {
			// Loose current modifications and show the view/add form
			manager.showDateForm();
		});

		// Install click handler on delete session date
		$('#delete-session-date').on('click', function (ev) {
			// Delete current date and show the add form
			if (manager.deleteSessionDate()) {
				// Refresh days calendar
				$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

				// Check if the empty session message should be displayed on not
				manager.updateConfirmButton();

				manager.showNewForm();
			}
		});

		// Install the click handler for the copy date button
		$('#make-copy-date').on('click', function (ev) {
			$('#copy-date-box').slideToggle('fast');
			$('#destination-day').val('');
		});

		// Install the make a copy handler
		$('#confirm-copy-session-date').on('click', function (ev) {
			manager.copyCurrentSessionDateFrom($('#destination-day').bdatepicker().data('datepicker').getUTCDate());

			// Check if the empty session message should be displayed on not
			manager.updateConfirmButton();

			// Refresh days calendar
			$('#session-dates-calendar').bdatepicker().data('datepicker').fill();

			// Close the copy date box
			$('#copy-date-box').slideToggle('fast');
		});

		// Check if the empty session message should be displayed on not and install click handler
		this.updateConfirmButton();
		$('.session-form-actions > .submit-session-button').on('click', function (ev) {
			if ($(this).is('.disabled'))
				return;

			if (Object.size(manager.sessionDates) == 0) {
				$('#sessionForm-details').hide();
				$('#sessionForm-dates').show();
				var tabs = $('#sessionFormTab').children('li');
				$(tabs[1]).addClass('active');
				$(tabs[0]).removeClass('active');

				// Show datepicker
				manager.datePicker.show();

				// Refresh the date form
				manager.showDateForm();
				return;
			}

			if (manager.validateSessionData()) {

				// Prepare the object to submit
				var formData = $('#session_form').serializeArray();
				$.each(manager.sessionDates, function (day, value) {
					for (field in manager.sessionDates[day]) {
						if (field != 'id_session')
							formData.push({
								name: "LtCourseSession[dates][" + day + "][" + field + "]",
								value: manager.sessionDates[day][field]
							});
					}
				});

				// Submit form via ajax
				$.ajax({
					dataType: 'json',
					type: 'POST',
					url: $("#session_form").attr('action'),
					cache: false,
					data: $.param(formData),
					success: function (data) {
						if (data.success == true) {
							$.fn.yiiGridView.update('sessions-management-grid', {
								data: $("#session-grid-search-form").serialize()
							});
							try {
								$('.modal-create-session').find('.modal-body').dialog2("close");
							} catch (e) {
							}
						} else {
							// Show error message
							$('.session-error').html('<span class="red">' + data.message + '</span>').show();

							// Restore edit form
							manager.toggleFormLoader(false);
						}
					},
					beforeSend: function () {
						manager.toggleFormLoader(true);
					},
					fail: function (jqXHR, status, errorThrown) {
						// Show error message
						$('.session-error').html('<span class="red">' + this.translations_OPERATION_FAILURE + '</span>').show();

						// Restore edit form
						manager.toggleFormLoader(false);
					}
				});
			} else {
				$('#sessionForm-details').show();
				$('#sessionForm-dates').hide();
				var tabs = $('#sessionFormTab').children('li');
				$(tabs[0]).addClass('active');
				$(tabs[1]).removeClass('active');

				// Hide datepicker
				manager.datePicker.hide();

				// Hide the blank slate under calendar
				$('#popup_bootstroAddAnotherDate').hide();
			}
		});

		// Install click handler of cancel buttons
		$('.cancel-session-modal').on('click', function (ev) {
			$('.modal-create-session').find('.modal-body').dialog2("close");
		});
	},

    /* Change handler for evaluation type */
    initEvaluationSwitcher: function() {
        $('input[name="LtCourseSession[evaluation_type]"]').on('change', function (ev) {
            if($(this).val() == '0')
                $('#evaluation-score-base-option').show();
            else
                $('#evaluation-score-base-option').hide();
        });
    },

	/* Toggle the form loader view */
	toggleFormLoader: function (makeVisible) {
		if (makeVisible) {
			// Hide current multi-tab div inside modal and date picker
			$('.edit-session-tabs').hide();
			this.datePicker.hide();

			// Show loader content
			$('.modal-create-session').find('.loader').show();
			$('.modal-create-session').addClass('loading');
		} else {
			// Hide loader content
			$('.modal-create-session').find('.loader').hide();
			$('.modal-create-session').removeClass('loading');

			// Show current multi-tab div inside modal and date picker
			$('.edit-session-tabs').show();
			if ($('#sessionForm-dates').is(':visible'))
				this.datePicker.show();
		}
	},

	/* Validates session data, before submitting the form to the controller (via ajax) */
	validateSessionData: function () {
		var result = true;

		$("#LtCourseSession_name").removeClass('error');
		$("#LtCourseSession_max_enroll").removeClass('error');
		$('#session-name-error-message').html('');
		$('#max-enroll-error-message').html('');

		// Check if session name is not empty
		var sessionName = $("#LtCourseSession_name").val().trim();
		if (sessionName == '') {
			$("#LtCourseSession_name").addClass('error');
			$('#session-name-error-message').html(this.translations._SESSION_NAME_IS_REQUIRED + '.');
			result = false;
		}

		// Check if max enrollments is greater than zero
		var maxEnrollments = $('#LtCourseSession_max_enroll').val().trim();
		if ((maxEnrollments == '') || (maxEnrollments % 1 !== 0) || parseInt(maxEnrollments)<=0) {
			result = false;
			$("#LtCourseSession_max_enroll").addClass('error');
			$('#max-enroll-error-message').html(this.translations._ENTER_A_NUMBER_GREATER_THAN_0 + '.');
		}

		return result;
	},

	/* Copies the current selected session date to a destination date */
	copyCurrentSessionDateFrom: function (dstDate) {
		var srcDateString = this.getMysqlDate(this.selectedDate);
		var dstDateString = this.getMysqlDate(dstDate);

		// If target date Date already in session? -> we can't copy it
		if (!(dstDateString in this.sessionDates)) {
			var srcDayObject = this.sessionDates[srcDateString];
			var dstDayObject = {
				day: dstDateString,
				name: srcDayObject.name,
				time_begin: srcDayObject.time_begin,
				time_end: srcDayObject.time_end,
				break_begin: srcDayObject.break_begin,
				break_end: srcDayObject.break_end,
				timezone: srcDayObject.timezone,
				id_location: srcDayObject.id_location,
				id_classroom: srcDayObject.id_classroom
			};

			this.sessionDates[dstDateString] = dstDayObject;
		}
	},

	/* Resets day error messages */
	resetDayErrors: function () {
		$("#time-begin").removeClass('error');
		$("#time-end").removeClass('error');
		$('#time-error-message').html('');
		$("#day-location").removeClass('error');
		$('#day-location-error').html('');
	},

	/* Validates the information in the date form */
	validateDayInformation: function () {
		var result = true;
		var regex = new RegExp("^(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$"); // Regexp for time validation (24H)

		// Reset previous error messages
		this.resetDayErrors();

		// Check if time_begin and time_end are present and valid
		var timeBegin = $("#time-begin").val().trim();
		if ((timeBegin == '') || !regex.test(timeBegin)) {
			$("#time-begin").addClass('error');
			result = false;
		}

		var timeEnd = $("#time-end").val().trim();
		if ((timeEnd == '') || !regex.test(timeEnd)) {
			$("#time-end").addClass('error');
			result = false;
		}

		if (result == false)
			$('#time-error-message').html(this.translations._INVALID_OR_MISSING_DATE_HOURS + '.');


		// Check if a location has been selected
		var idLocation = $('#day-location').val();
		if (idLocation == 0) {
			result = false;
			$("#day-location").addClass('error');
			$('#day-location-error').html(this.translations._PLEASE_CHOOSE_A_LOCATION + '.');
		}

		return result;
	},

	/* Remove the session date from the sessionsDate array */
	deleteSessionDate: function () {
		// Get the JS object for the day
		var dateString = this.getMysqlDate(this.selectedDate);
		if (dateString in this.sessionDates) { // Date already in session -> get object from sessionDates array
			delete this.sessionDates[dateString];
			return true;
		} else
			return false;
	},

	/* Creates a new session date or edits an existing one */
	updateOrCreateSessionDate: function () {

		// Perform validation
		if (this.validateDayInformation() == true) {

			// If the current day was changed, delete the old record in sessionsDate
			if (this.previousDate) {
				var previousDateString = this.getMysqlDate(this.previousDate);
				if (previousDateString in this.sessionDates)
					delete this.sessionDates[previousDateString];
				this.previousDate = null;
			}

			// Prepare the JS object for the day
			var dateString = this.getMysqlDate(this.selectedDate);
			var dayObject = null;
			if (dateString in this.sessionDates) // Date already in session -> get object from sessionDates array
				dayObject = this.sessionDates[dateString];
			else {
				dayObject = {}; // New session date -> create new object and add it to the array
				this.sessionDates[dateString] = dayObject;
			}

			// Fill in JS object from form fields
			dayObject.day = $('#day-calendar').val();
			dayObject.name = $('#day-name').val();
			dayObject.time_begin = $('#time-begin').val();
			dayObject.time_end = $('#time-end').val();
			dayObject.break_begin = $('#break-begin').val();
			dayObject.break_end = $('#break-end').val();
			dayObject.timezone = $('#day-timezone').val();
			dayObject.id_location = $('#day-location').val();
			dayObject.id_classroom = $('#day-classroom').val();
			return true;

		} else
			return false;
	},

	/* Redraws the classroom dropdown menu */
	redrawClassroomDropdown: function (currentLocation) {
		// Update classrooms dropdown
		$('#day-classroom').html('<option value="">' + this.translations._NO_CLASSROOM_SELECTED + '</option>');

		$('.session-warning .message').html('').parent().hide();

		var i, ddlist = [];
		if (currentLocation in this.classrooms) {
			for (var idClassroom in this.classrooms[currentLocation]) {
				if (this.classrooms[currentLocation].hasOwnProperty(idClassroom))
				//$('#day-classroom').append('<option value="'+idClassroom+'">'+this.classrooms[currentLocation][idClassroom].name+'</option>');
					ddlist.push({key: idClassroom, value: this.classrooms[currentLocation][idClassroom].name});
			}
		}
		if (ddlist.length > 0) {
			ddlist.sort(function (a, b) {
				if (a.value < b.value) return -1;
				if (a.value > b.value) return 1;
				return 0;
			});
			for (i = 0; i < ddlist.length; i++) {
				$('#day-classroom').append('<option value="' + ddlist[i].key + '">' + ddlist[i].value + '</option>');
			}
		}
	},

	/* Shows the right date form in the session dates tab */
	showDateForm: function () {
		var dateString = this.getMysqlDate(this.selectedDate);
		if (dateString in this.sessionDates) // Date already in session -> Show edit form
			this.showViewForm();
		else  // Date not in session -> Show new date form
			this.showNewForm();
	},

	/* Shows the new date form */
	showNewForm: function () {
		// Update date label
		$('#no-date-form > h2').html(this.getLongDateName(this.selectedDate));

		// Hide the blank slate under calendar
		$('#popup_bootstroAddAnotherDate').hide();

		$('#edit-date-form').hide();
		$('#view-date-form').hide();
		$('#no-date-form').show();
	},

	/* Show the view date form */
	showViewForm: function () {
		var dateString = this.getMysqlDate(this.selectedDate);
		var dayObject = this.sessionDates[dateString];

		// Update static labels
		$('#view-date-form > h2').html(this.getLongDateName(this.selectedDate));

		$('#view-date-form > .day-title > div').html(dayObject.name);

        if(dayObject.break_begin && dayObject.break_end && dayObject.break_begin != dayObject.break_end){
            $('#view-date-form > .day-time-line > div').html(dayObject.time_begin + ' - ' + dayObject.time_end + ' | ' + dayObject.break_begin + ' - ' + dayObject.break_end
                + '<span style="margin-left: 40px;">' + this.timezoneOffsets[dayObject.timezone] + '</span>');
        }else{
            $('#view-date-form > .day-time-line > div').html(dayObject.time_begin + ' - ' + dayObject.time_end
                + '<span style="margin-left: 40px;">' + this.timezoneOffsets[dayObject.timezone] + '</span>');
        }


		var locationDetails = '';
		if ((dayObject.id_classroom !== '' && dayObject.id_classroom !== null) && (this.classrooms[dayObject.id_location] !== undefined && dayObject.id_classroom in this.classrooms[dayObject.id_location])) {
			var classroom = this.classrooms[dayObject.id_location][dayObject.id_classroom];
			locationDetails += classroom.name;
			if (classroom.seats > 0)
				locationDetails += ' (' + classroom.seats + ' ' + this.translations._SEATS + ')';
			locationDetails += '<br/>';
		}
		if (dayObject.id_location in this.locations) {
			var location = this.locations[dayObject.id_location];
			locationDetails += location.name + '<br/>';
			if (location.formatted_address.length > 0)
				locationDetails += location.formatted_address + '<br/>';
		}

		$('#view-date-form > .day-location > div').html(locationDetails);

		// Hide other tabs
		$('#edit-date-form').hide();
		$('#no-date-form').hide();

		// Close the clone day box
		$('#copy-date-box').hide();

		// Show the blank slate under calendar
		$('#popup_bootstroAddAnotherDate').show();

		// Show the view form
		$('#view-date-form').show();
	},

	/* Shows the edit date form */
	showEditForm: function () {
		$('#no-date-form').hide();
		$('#view-date-form').hide();

		// Reset previous error messages
		this.resetDayErrors();

		// Show edit form
		$('#edit-date-form').show();

		$('.session-warning .message').html('').parent().hide();
		$('.session-form-actions > #create-session-date').removeClass('disabled');

		// Get information about the currently selected date
		var dateString = this.getMysqlDate(this.selectedDate);
		var dayObject = null;
		if (dateString in this.sessionDates) { // Date already in session -> get information from sessionDates array
			dayObject = this.sessionDates[dateString];
			$('#create-session-date').attr('value', this.translations._UPDATE_SESSION_DATE);
		}
		else { // Date not in session -> Create new "empty" day object
			dayObject = {
				day: dateString,
				name: '',
				time_begin: '9:00',
				time_end: '18:00',
				break_begin: '13:00',
				break_end: '14:00',
				timezone: this.currentTimezone,
				id_location: this.idLocation,
				id_classroom: 0
			};
			$('#create-session-date').attr('value', this.translations._CREATE_SESSION_DATE);
		}

		var tmpBreakBegin = dayObject.break_begin;
		var tmpBreakEnd = dayObject.break_end;
		//break times cannot contain "00:00" value
		if (tmpBreakBegin == '00:00' || tmpBreakEnd == '00:00') {
			tmpBreakBegin = '';
			tmpBreakEnd = '';
		}

		// Fill in edit form fields
		$('#day-calendar').val(dayObject.day);
		$('#day-name').val(dayObject.name);
		$('#time-begin').val(dayObject.time_begin);
		$('#time-end').val(dayObject.time_end);
		$('#break-begin').val(/*dayObject.break_begin*/tmpBreakBegin);
		$('#break-end').val(/*dayObject.break_end*/tmpBreakEnd);
		$('#day-timezone').val(dayObject.timezone);
		$('#day-location').val(dayObject.id_location);

		// Redraw the classroom dropdown
		this.redrawClassroomDropdown(dayObject.id_location);
		$('#day-classroom').val(dayObject.id_classroom);

		// Update timezone to correct when edit session date
		var currentTimezone = $('#day-timezone').val();
		var timezoneOffset = this.timezoneOffsets[currentTimezone].match(/(\(.*\))/g) ;
		$('#day-timezone-label').html(timezoneOffset);
	}
};