<?php

class LocationController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		//allowed actions based on permissions
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/location/view'),
			'actions' => array(
				'index',
				'axViewMap',
				'locationAutocomplete',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/location/mod'),
			'actions' => array(
				'axDelete',
				'axDeletePhoto',
				'axEdit',
				'csvImport',
				'csvSample',
			),
		);


		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}


	public function actionIndex() {

		unset(Yii::app()->session['importLocationsStep']);
		Yii::app()->tinymce->init();

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		$cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?key='.Settings::get('googlemaps_api_key', 'AIzaSyAuUMueMRN0lXmNnxyj-RnWIOtwAxOP500').'&v=3.exp');

		// Bootcamp menu
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		// classroom.css
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');


		if (Yii::app()->user->isAdmin) {
			//apply power user filter
			$locationModel = new LtLocation('power_user_management');
			$locationModel->powerUserManager = CoreUser::model()->findByPk(Yii::app()->user->id);
		} else {
			$locationModel = new LtLocation(); //no specific scenario required for super admins
		}

		// filter locations by name
		if (isset($_POST['LtLocation'])) {
			$postData = $_POST['LtLocation'];
			$locationModel->name = (isset($postData['name'])) ? $postData['name'] : null;
		}

		$this->render('index', array(
			'locationModel' => $locationModel,
		));
	}



	public function actionAxViewMap() {
		$id = intval(Yii::app()->request->getParam('id'));
		$model = LtLocation::model()->findByPk($id);

		if ($model) {
			$this->renderPartial('_viewMap', array(
				'model' => $model
			));
		}
	}



	public function actionAxEdit() {

		$model = null;
		$id = intval(Yii::app()->request->getParam('id'));
		$isNew = false;

		if ($id > 0) {
			$model = LtLocation::model()->findByPk($id);
		}
		if (!$model) {
			$model = new LtLocation();
			$isNew = true;
		}

		if (isset($_POST['LtLocation'])) {
			$postData = $_POST['LtLocation'];

			$model->attributes = $postData;

			if ($model->save()) {

				// save location photos
				$photos = CUploadedFile::getInstancesByName('LearningLocationPhotos');
				if (!empty($photos) && count($photos) > 0) {
					foreach ($photos as $photo) {
						$destinationPath = '';

						// save photo to tmp dir (due to storage restrictions) while renaming it
						$filename = 'location_' . $model->id_location . '_photo_' . uniqid(time()) . '.' . CFileHelper::getExtension($photo->getName());
						$uploadTmpFilePath = Docebo::getUploadTmpPath() . DIRECTORY_SEPARATOR . $filename;
						if ($photo->saveAs($uploadTmpFilePath)) {

							// now save this tmp file to storage
							$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_LOCATION);
							if ($storageManager->store($uploadTmpFilePath)) {

								// save photo in db
								$locationPhoto = new LtLocationPhoto();
								$locationPhoto->id_location = $model->id_location;
								$locationPhoto->file_path = $filename;
								$locationPhoto->save();
							}
						}
					}
				}

				//if we are a power user, then self-assign the newly created location
				if ($isNew && Yii::app()->user->isAdmin) {
					$puModel = new CoreUserPuLocation();
					$puModel->puser_id = Yii::app()->user->id;
					$puModel->location_id = $model->id_location;
					$puModel->save();
				}

				// close dialog and reload locations
				echo '<a href="" class="auto-close"></a>';
				return;
			}
		}

		$this->renderPartial('_edit', array(
			'model' => $model
		));
	}

	public function actionAxDelete() {

        $id = intval( Yii::app()->request->getParam('id') );
        $model = LtLocation::model()->findByPk($id);

        if (isset($_POST['LtLocation']) && !LtLocation::isUsedInFutureSessionDate($id)) {
            if ($model->delete()) {
                // close dialog and reload locations
                echo '<a href="" class="auto-close"></a>';
                return;
            }
        }

        $this->renderPartial('_delete', array(
            'model' => $model
        ));
    }

    public function actionAxDeletePhoto() {
        $id = intval( Yii::app()->request->getParam('id') );
        $photo = LtLocationPhoto::model()->findByPk($id);
        if ($photo) {
            echo $photo->delete();
        }
    }

    protected function gridRenderClassrooms($row, $index) {
        $count = intval( count($row['classrooms']) );
        if (!$count) {
            $html = '<a href="' . Docebo::createAppUrl('lms:ClassroomApp/ClassroomApp/index', array('id_location'=>$row['id_location'])) . '" class="call-to-action">'.Yii::t('classroom', 'Add classroom(s)').'</a>';
        } else {
            $html = '<a href="' . Docebo::createAppUrl('lms:ClassroomApp/ClassroomApp/index', array('id_location'=>$row['id_location'])) . '">'.count($row['classrooms']).' <span class="classroom-sprite classroom"></span></a>';
        }
        return $html;
    }

    protected function gridRenderLinkViewMap($row, $index) {
        $html = CHtml::link('<span class="classroom-sprite map"></span>', $this->createUrl('axViewMap', array('id' => $row['id_location'])), array(
            'class' => 'open-dialog',
            'data-dialog-class' => 'modal-location-map',
            'title' => Yii::t('classroom', 'View location map'),
		  'removeOnClose' => 'true',
            'rel' => 'tooltip',
        ));
        return $html;
    }

    protected function gridRenderLinkEditLocation($row, $index) {
        $html = CHtml::link('<span class="i-sprite is-edit"></span>', $this->createUrl('axEdit', array('id' => $row['id_location'])), array(
            'class' => 'open-dialog',
            'data-dialog-class' => 'modal-edit-location',
            'title' => Yii::t('standard', '_MOD'),
            'closeOnOverlayClick' => 'false',
            'closeOnEscape' => 'false',
		  'removeOnClose' => 'true',
            'rel' => 'tooltip',
        ));
        return $html;
    }

    protected function gridRenderLinkDeleteLocation($row, $index) {
		//don't show the button if the location is used in at least one session date in the future
		if(LtLocation::isUsedInFutureSessionDate($row['id_location'])) return '';

        $html = CHtml::link('<span class="i-sprite is-remove red"></span>', $this->createUrl('axDelete', array('id' => $row['id_location'])), array(
            'class' => 'open-dialog',
            'data-dialog-class' => 'modal-delete-location',
            'title' => Yii::t('standard', '_DEL'),
		  'removeOnClose' => 'true',
            'rel' => 'tooltip',
        ));
        return $html;
    }

    public function actionCsvImport() {

        /* @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();

        // jscrollpane
        $cs->registerCssFile (Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.css');
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.js');

        // classroom.css
        $cs->registerCssFile (Yii::app()->theme->baseUrl . '/css/classroom.css');

        $model = null;
        $step = 'step_import';


        if (isset(Yii::app()->session['importLocationsStep'])) {
            $tmpStep = Yii::app()->session['importLocationsStep'];
            if (in_array($tmpStep, array('step_import', 'step_process')))
                $step = $tmpStep;
        } else {
            unset(Yii::app()->session['importLocationsStep']);
        }

        if ($step == 'step_import') {
            unset(Yii::app()->session['importLocationsModel']);
            $model = new LocationImportForm($step);

            if (isset($_POST['LocationImportForm'])) {
                $postData = $_POST['LocationImportForm'];
                $model->attributes = $postData;
                $model->file = CUploadedFile::getInstance($model, 'file');
                if ($model->validate()) {
                    $step = 'step_process';
                    Yii::app()->session['importLocationsStep'] = $step;
                    Yii::app()->session['importLocationsModel'] = $model->attributes;
                }
            }

        }

        switch($step) {

            case 'step_process':
                if ($model == null) {
                    $model = new LocationImportForm();
                    $model->attributes = Yii::app()->session['importLocationsModel'];
                    $model->setData( Yii::app()->session['importLocationsData'] );
                }

                if (isset($_POST['import_map']) && isset($_POST['LocationImportForm']) && !Yii::app()->request->isAjaxRequest) {
                    $model->scenario = 'step_process';
                    $model->attributes = $_POST['LocationImportForm'];
                    $model->setImportMap($_POST['import_map']);

                    if ($model->save()) {
                        unset(Yii::app()->session['importLocationsModel']);
                        unset(Yii::app()->session['importLocationsStep']);
                        $this->redirect($this->createUrl('index'));
                    } else {
                        Yii::app()->session['importLocationsModel'] = $model;
                        $this->redirect($this->createUrl('index'));
                    }
                }

                $items = $model->getData();

                if (empty($items)) {
                    $this->redirect($this->createUrl('index'));
                }

                Yii::app()->session['importLocationsData'] = $items;

                $columns = array();
                $i = 1;
                $csvHeaderLine = $items[0];
                foreach ($csvHeaderLine as $key => $value) {
                    $column = array(
                        'name' => $key,
                        'htmlOptions' => array('class' => 'column-'.$key),
                    );
                    if ($model->firstRowAsHeader) {
                        $columnHeader = CHtml::dropDownList('import_map['.$key.']', $model->importMap[$i], LocationImportForm::getImportMapList()).'<p>'.$value.'</p>';
                        $column['header'] = $columnHeader;
                    }
                    $columns[] = $column;
                    $i++;
                }

                $this->render('csvProcess', array(
                    'model' => $model,
                    'columns' => $columns
                ));
                break;

            case 'step_import':
            default:
                $this->render('csvImport', array(
                    'model' => $model
                ));
                break;
        }
    }

    public function actionCsvSample() {
    	$sep = DIRECTORY_SEPARATOR;
    	$filePath = Yii::getPathOfAlias('common').$sep.'extensions'.$sep.'sample-locations.csv';
        if (file_exists($filePath)) {
            $fileContent = file_get_contents($filePath);
            Yii::app()->request->sendFile('sample-locations.csv', $fileContent, NULL, false);
        }
        Yii::app()->end();
    }



	public function actionLocationAutocomplete() {
		$query = addcslashes($_REQUEST['data']['query'], '%_');

		$criteria = new CDbCriteria(array(
			'condition' => 'name LIKE :match OR name LIKE :match2',
			'params' => array(
				':match' => "/%$query%",
				':match2' => "%$query%"
			),
		));

		if (Yii::app()->user->getIsAdmin()) {
			$criteria->with['powerUserManagers'] = array(
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => Yii::app()->user->id),
				'together' => true,
			);
		} elseif (isset($_REQUEST['data']['powerusermanager'])) {
			$criteria->with['powerUserManagers'] = array(
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => $_REQUEST['data']['powerusermanager']),
				'together' => true,
			);
		}

		if (isset($_REQUEST['data']['poweruserNot'])) {
			$powerUserModel = CoreUser::model()->findByPk($_REQUEST['data']['poweruserNot']);
			$criteria->with['powerUserManagers'] = array(
				'joinType' => 'LEFT JOIN',
			);
			$puLocations = array();
			$tmpCriteria = new CDbCriteria();
			$tmpCriteria->addCondition("puser_id = :puser_id");
			$tmpCriteria->params[':puser_id'] = $powerUserModel->idst;
			$tmpList = CoreUserPuLocation::model()->findAll($tmpCriteria);
			if (!empty($tmpList)) { foreach ($tmpList as $tmpItem) { $puLocations[] = $tmpItem->location_id; } }
			if (!empty($puLocations)) {
				$criteria->with['powerUserManagers']['condition'] = 't.id_location NOT IN ('
					. implode(',', $puLocations)
					. ') OR powerUserManagers.idst IS NULL';
			}
			$criteria->together = true;
			$criteria->group = 't.id_location';
		}

		$locations = LtLocation::model()->findAll($criteria);
		$locationsList = array_values(CHtml::listData($locations, 'id_location', 'name'));
		$this->sendJSON(array('options' => $locationsList));
	}

}
