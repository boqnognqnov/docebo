<?php


/**
 * This controllers manages instructor administration modules inside the course
 */
class InstructorController extends ClassroomBaseController {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		/*$admin_only_actions = array();
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);*/

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	/**
	 * Prepare controller actions here
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action) {
		$rs = parent::beforeAction($action);
		if (!$rs)
			return false;

		$cs = Yii::app()->getClientScript();

		// This is an administration module, but it is played from lms frontend,
		// so import administration stylesheet from admin folder
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');
		// $cs->registerCssFile(Yii::app()->baseUrl . '/../admin/css/admin-temp.css');
		$cs->registerScriptFile(Yii::app()->baseUrl . '/../admin/js/script.js');

		if (Yii::app()->player) {
			// Let "player" Yii component know about the course we are in
			// This will allow Main Menu to show "course related" tiles: "play" and "manage courses"
			Yii::app()->player->setCourse($this->courseModel);
		}

		//some action-specific initializations
		switch ($action->id) {
			//case 'axEvaluateUser':
			//case 'axEditUserEvaluation':
			case 'evaluation': {
				Yii::app()->tinymce->init();
			} break;
		}

		return true;
	}



	/**
	 * Show evaluation and attendance management interface
	 */
	public function actionEvaluation() {
		if(!$this->_courseModel || !$this->_sessionModel)
            throw new CHttpException("Invalid params passed to this controller action");

		$isSubscribed = LtCourseuserSession::isSubscribed(Yii::app()->user->id, $this->_sessionModel->id_session);
		$isInstructor = LearningCourseuser::isUserLevel(Yii::app()->user->id,  $this->_courseModel->idCourse, LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
		if (!Yii::app()->user->getIsGodadmin() && (Yii::app()->user->isPu && !Yii::app()->user->checkAccess("/lms/admin/course/evaluation")) && !($isInstructor && $isSubscribed)){
			throw new CException(Yii::t("course", '_NOENTER'));
		}

		$this->render('evaluation', array(
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel
		));
	}


	/**
	 * Allow instructor to print an attendance sheet
	 */
	public function actionAxAttendanceSheet() {

		$day = Yii::app()->request->getParam('day', false);
		$allSessionsDates = false;
		if ($day === false) {
			//no day has been specified: then pick the first day of the session
			$dates = $this->sessionModel->getDates();
			if (empty($dates)) { throw new CException('Error: no dates in the session'); } //there are no dates in this session yet
			$selectedDate = $dates[0]->day;
		} elseif ($day == 0) {
			$allSessionsDates = true;
			$listOfSessionsDates = LtCourseSessionDate::model()->findAllByAttributes(array(
					'id_session' => $this->_sessionModel->id_session
			));
			$selectedDate = 0;
		} else {
			$selectedDate = $day;
		}

		$dateModel = LtCourseSessionDate::model()->findByPk(array('id_session' => $this->idSession, 'day' => $day));

		$attendanceFieldsModel = AttendanceSheetMainField::model()->findByAttributes(
			array(
				'idCourse' => Yii::app()->request->getParam('course_id'),
				'idSession' => Yii::app()->request->getParam('id_session')
			)
		);
		if(!$attendanceFieldsModel) {
			$attendanceFieldsModel = AttendanceSheetMainField::model()->findByPk(1);
		}
		$listOfFieldsTitles = array();
		$order = explode(',', $attendanceFieldsModel->order);
		foreach($order as $field){
			if (is_numeric($attendanceFieldsModel->$field)
					&& $attendanceFieldsModel->$field != 0) {
				// we have an id of the field, so now get his translation
				/** @var CoreUserField $model */
				$model = CoreUserField::model()->findByPk($attendanceFieldsModel->$field);
				if($model) {
					$listOfFieldsTitles[$attendanceFieldsModel->$field] = $model;
				}
			} else {
				$listOfFieldsTitles[] = $attendanceFieldsModel->$field;
			}
		}

		$listOfFieldsTitles = array_filter($listOfFieldsTitles);
		$this->renderPartial('attendanceSheet', array(
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'selectedDate' => $selectedDate,
			'dateModel' => $dateModel,
			'allSessions' => $allSessionsDates,
			'listOfFieldsTitles' => isset($listOfFieldsTitles) ? $listOfFieldsTitles : null,
			'listOfSessionsDates' => isset($listOfSessionsDates) ? $listOfSessionsDates : null
		));
	}

	/**
	 * Update a single user attendance status (from instructor attendance management grid)
	 *
	 * @throws Cexception
	 */
	public function actionAxSetAttendance() {

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);
		$idSession = $rq->getParam('id_session', 0);
		$sessionEnrollment = LtCourseuserSession::model()->findByPk(array('id_session' => $idSession, 'id_user' => $idUser));
		if(!$sessionEnrollment)
			return;

		$day = $rq->getParam('day', false);
		$oldStatus = ($rq->getParam('old_status', -1) > 0 ? 1 : 0);
		$newStatus = ($rq->getParam('new_status', -1) > 0 ? 1 : 0);

		//check if we are already in the desired status. If so, no operation has to be performed
		if ($oldStatus == $newStatus) {
			echo CJSON::encode(array(
				'success' => true,
				'old_status' => $oldStatus,
				'new_status' => $newStatus
			));
			return;
		}

		//prepare output variable
		$output = array(
			'success' => false,
			'message' => ''
		);

		$transaction = Yii::app()->db->beginTransaction();

		try {

			if ($oldStatus > 0) {

				//here we are going to delete an already existant attendance record

				//find session date attendance record for the user
				$attendanceModel = LtCourseSessionDateAttendance::model()->findByPk(array(
					'id_session' => $idSession,
					'day' => $day,
					'id_user' => $idUser
				));
				if (!$attendanceModel) { throw new CException('Invalid attendance info'); } //the record MUST exists

				//remove it
				$attendanceModel->delete();

			} else {

				//here we are going to create an attendance record for the user

				$attendanceModel = new LtCourseSessionDateAttendance();
				$attendanceModel->id_user = $idUser;
				$attendanceModel->id_session = $idSession;
				$attendanceModel->day = $day;

				//create it
				$rs = $attendanceModel->save();

				//set user In Progress
				$sessionEnrollment->status = LtCourseuserSession::$SESSION_USER_BEGIN;

				if (!$rs) { throw new CException('Error while creating user attendance record'); }

			}
            // Refresh enrollment
            $sessionEnrollment->save();

			$transaction->commit();

			$output['success'] = true;
			$output['old_status'] = $oldStatus;
			$output['new_status'] = $newStatus;

		} catch(CException $e) {

			//something went wrong: revert back database operations and return an error message
			$transaction->rollback();
			$output['success'] = false;
			$output['message'] = $e->getMessage();
		}

		//send output to the client in json format
		echo CJSON::encode($output);
	}



	/**
	 * Given a list uf users idsts, set attendance status to present for all in a specified classroom day
	 *
	 * @throws CException
	 */
	public function actionAxSetUsersAsPresent() {

		//read input
		$rq = Yii::app()->request;
		$usersStr = $rq->getParam('users', '');
		$idSession = $rq->getParam('id_session', 0);
		$day = $rq->getParam('day', false);

		//users are being passed as string, transform it into something usable in php
		$users = explode(',', $usersStr);
		if (empty($users)) { throw new CException('No users specified'); }

		//check if any users has already attendend the day
		$alreadyUsers = array();
		$alreadyAttendances = LtCourseSessionDateAttendance::model()->findAllByAttributes(array(
			'id_session' => $idSession,
			'day' => $day
		));
		if (!empty($alreadyAttendances)) {
			foreach ($alreadyAttendances as $alreadyAttendance) {
				$alreadyUsers[] = (int)$alreadyAttendance->id_user;
			}
		}
		//free some memory by no more useful variables
		$alreadyAttendances = NULL;
		unset($alreadyAttendance);

		$transaction = Yii::app()->db->beginTransaction();

		//prepare output variable
		$output = array(
			'success' => false,
			'message' => ''
		);

		try {

			//set physically the users attendance into database
			foreach ($users as $idUser) {

				if (!in_array((int)$idUser, $alreadyUsers)) { //some users may have already set as present, so don't touch them

					$attendanceModel = new LtCourseSessionDateAttendance();
					$attendanceModel->id_user = $idUser;
					$attendanceModel->id_session = $idSession;
					$attendanceModel->day = $day;

					//create it
					$rs = $attendanceModel->save();

					//set users In Progress
					$sessUserModel = LtCourseuserSession::model()->findByAttributes(array('id_session' => $idSession, 'id_user' => $idUser));
					$sessUserModel->status = LtCourseuserSession::$SESSION_USER_BEGIN;
					$su = $sessUserModel->save();

					if (!$rs || !$su) { throw new CException('Error while creating user attendance record'); }
				}
			}

			//commit and set output for the client
			$transaction->commit();
			$output['success'] = true;

		} catch (CException $e) {

			//something went wrong: revert back database operations and return an error message
			$transaction->rollback();
			$output['success'] = false;
			$output['message'] = $e->getMessage();
		}

		//send output to the client in json format
		echo CJSON::encode($output);
	}




	/**
	 * Display user evaluation dialog (no actions)
	 */
	public function actionAxShowUserEvaluation() {

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);

		//switch to edit mode if requested
		// TODO: is this bad practice?
		if ($rq->getParam('edit-redirect', false) !== false) {
			$this->actionAxEditUserEvaluation(); //the dialog2 popup will be filled with the editing markup
			return;
		}

		//retreive displaying data
		$userModel = CoreUser::model()->findByPk($idUser);
		$enrollModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $this->idCourse));
		$enrollSessionModel = LtCourseuserSession::model()->findByPk(array('id_user' => $idUser, 'id_session' => $this->idSession));

		//render dialog
		$this->renderPartial('_evaluate_dialog', array(
			'context' => 'showing',
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idUser' => $idUser,
			'userModel' => $userModel,
			'enrollModel' => $enrollModel,
			'enrollSessionModel' => $enrollSessionModel
		));

	}


	public function actionAxEditUserEvaluation() {

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);

		//retreive displaying data
		$userModel = CoreUser::model()->findByPk($idUser);
		$enrollModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $this->idCourse));
		$enrollSessionModel = LtCourseuserSession::model()->findByPk(array('id_user' => $idUser, 'id_session' => $this->idSession));

		//check if editing has been confirmed: if so then perform database stuff and close dialog
		if ($rq->getParam('confirm', false) !== false) {

			$score = $rq->getParam('evaluation_score', 0);
			$text = $rq->getParam('evaluation_text', '');
			$status = $rq->getParam('evaluation_status');

			$evaluationFile = $enrollSessionModel->evaluation_file; //by default: keep old file specification
			//check if we have uploaded a new file
			if ($rq->getParam('evaluation-file-removed', 0) > 0) {

				$evaluationFile = NULL;
				if (isset($_FILES['evaluation_file']) && ($_FILES['evaluation_file']['name'] != '')) {

					//if yes, store it and save the file name in DB
					$saveFile = $enrollSessionModel->createUniqueFileName($_FILES['evaluation_file']['name']);
					$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
					if (!$storageManager->storeAs($_FILES['evaluation_file']['tmp_name'], $saveFile)) {
						throw new CException('Error while storing uploaded file');
					}

					//the generated unique file name will be saved into DB
					$evaluationFile = $saveFile;
				}

			}

			$transaction = Yii::app()->db->beginTransaction();

			try {
				if($score > $this->sessionModel->score_base)
					throw new CException(Yii::t('deliverable', 'Invalid score'));

				$rs = $enrollSessionModel->setEvaluation($status, $score, Yii::app()->user->id, $text, $evaluationFile);
				if (!$rs) { throw new CException('Error while evaluating user'); }

				$transaction->commit();

				//all went ok, close dialog
				echo '<a class="auto-close"></a>';

			} catch (CException $e) {

				$transaction->rollback();
				echo '<a class="error" data-message="'.$e->getMessage().'"></a>';
			}


			return;
		}

		//render dialog
		$this->renderPartial('_evaluate_dialog', array(
			'context' => 'editing',
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idUser' => $idUser,
			'userModel' => $userModel,
			'enrollModel' => $enrollModel,
			'enrollSessionModel' => $enrollSessionModel,
			'isFromShowing' => ($rq->getParam('edit-redirect', false) !== false)
		));

	}


	/**
	 * Clear an user evaluation for a classroom session. First a confirm popup is displayed
	 */
	public function actionAxClearUserEvaluation() {

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);

		//clearing has been confirmed: proceed with database operations
		if ($rq->getParam('confirm', false) !== false) {

			$transaction = Yii::app()->db->beginTransaction();

			try {

				$enrollSessionModel = LtCourseuserSession::model()->findByPk(array('id_user' => $idUser, 'id_session' => $this->idSession));
				if (!$enrollSessionModel) { throw new CException('Invalid input data'); }

				$rs = $enrollSessionModel->clearEvaluation();
				if (!$rs) { throw new Cexception('Error while clearing evaluation'); }

				$transaction->commit();

				echo '<a class="auto-close"></a>';

			} catch (CException $e) {

				$transaction->rollback();
				echo '<a class="error" data-message="'.$e->getMessage().'"></a>';
			}

			//operation done: exit now
			return;
		}

		//if no confirmation has been provided, then dispay the confirm dialog


		$this->renderPartial('_clear_user_evaluation', array(
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idUser' => $idUser,
			'userModel' => CoreUser::model()->findByPk($idUser)
		));
	}


	/**
	 * Display user evaluation dialog (creating/editing action)
	 */
	public function actionAxEvaluateUser() {

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);

		//retreive displaying data
		$userModel = CoreUser::model()->findByPk($idUser);
		$enrollModel = LearningCourseuser::model()->findByAttributes(array('idUser' => $idUser, 'idCourse' => $this->idCourse));
		$enrollSessionModel = LtCourseuserSession::model()->findByPk(array('id_user' => $idUser, 'id_session' => $this->idSession));

		//check if confirm button has been pressed: if so, do DB stuff and go to next step
		if ($rq->getParam('confirm', false) !== false) {

			//read input data
			$score = $rq->getParam('evaluation_score', 0);
			$text = $rq->getParam('evaluation_text', '');
			$status = $rq->getParam('evaluation_status');

			//check if we have uploaded a file
			$evaluationFile = '';
			if (isset($_FILES['evaluation_file']) && ($_FILES['evaluation_file']['name'] != '')) {

				//if yes, store it and save the file name in DB
				$saveFile = $enrollSessionModel->createUniqueFileName($_FILES['evaluation_file']['name']);
				$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
				if (!$storageManager->storeAs($_FILES['evaluation_file']['tmp_name'], $saveFile)) {
					throw new CException('Error while storing uploaded file');
				}

				//the generated unique file name will be saved into DB
				$evaluationFile = $saveFile;
			}

			$transaction = Yii::app()->db->beginTransaction();

			try {

				//save evaluation data into DB
				$rs = $enrollSessionModel->setEvaluation($status, $score, Yii::app()->user->id, $text, $evaluationFile);
				if (!$rs) {
					$errors = $enrollSessionModel->getErrors();
					$messages = "";
					foreach ($errors as $attr => $error)
						$messages .= implode('<br/>', $error);

					throw new CException('Error while evaluating user. Errors: ' . $messages);
				} else {
					// Raising an event for other applications to work with
					Yii::app()->event->raise(EventManager::EVENT_STUDENT_COMPLETED_ILT_SESSION, new DEvent($this, array(
						'courseId' => $this->idCourse,
						'userId' => $idUser
					)));
				}

				//in the next step, we may choose from a list of users to evaluate: retrieve them
				$usersList = array();
				$command = Yii::app()->db->createCommand()
					->select("cus.id_user, u.userid, u.firstname, u.lastname")
					->from(LtCourseuserSession::model()->tableName()." cus")
					->join(LearningCourseuser::model()->tableName()." cu", "cus.id_user = cu.idUser")
					->join(CoreUser::model()->tableName()." u", "u.idst = cus.id_user");
				if (Yii::app()->user->isAdmin) {
					$command->join(
						CoreUserPU::model()->tableName()." pu",
						"cus.id_user = pu.user_id AND pu.puser_id = :puser_id",
						array(':puser_id' => Yii::app()->user->id)
					);
				}
				$command->where("cu.idCourse = :id_course", array(':id_course' => $this->idCourse))
					->andWhere("cus.id_session = :id_session", array(':id_session' => $this->idSession))
					->andWhere("cu.level = :level", array(':level' => LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT))
					->andWhere("cus.evaluation_status IS NULL"); //we need only non-evaluated users
				$command->order("u.firstname, u.lastname");
				$list = $command->queryAll();
				if (!empty($list)) {
					foreach ($list as $item) {
						$usersList[(int)$item['id_user']] = $item['firstname'].' '.$item['lastname'] . " (" . Yii::app()->user->getRelativeUsername($item['userid']) . ")";
					}
				}

				//prepare next action, if any
				if (!empty($usersList)) {

					//now render next step content
					$this->renderPartial('_evaluate_next_step', array(
						'idCourse' => $this->idCourse,
						'idSession' => $this->idSession,
						'courseModel' => $this->courseModel,
						'sessionModel' => $this->sessionModel,
						'usersList' => $usersList
					));

				} else {

					//if there aren't unevaluated users, then there isn't any next step so go back
					echo '<a class="auto-close"></a>';
				}

				$transaction->commit();

			} catch(CException $e) {

				//something went wrong: return an error status+message
				$transaction->rollback();
				echo '<a class="error" data-message="'.$e->getMessage().'"></a>';
			}

			return;
		}

		//render dialog
		$this->renderPartial('_evaluate_dialog', array(
			'context' => 'evaluation',
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idUser' => $idUser,
			'userModel' => $userModel,
			'enrollModel' => $enrollModel,
			'enrollSessionModel' => $enrollSessionModel
		));
	}



	/**
	 * For autocomplete inputs in enrolled users searching
	 */
	public function actionAxSearchAutoComplete() {

		//read input and prepare output
		$rq = Yii::app()->request();
		$input = Yii::app()->request->getParam('input', array());
		$output = array('options' => array());

		$idSession = (int)$input['id_session'];
		$searchText = trim(strtolower($input['query'])); // TODO: see if some formatting is needed for seatchText

		//search values
		$command = Yii::app()->db->createCommand()
			->select("u.idst, u.userid, u.firstname, u.lastname")
			->from(CoreUser::model()->tableName()." u")
			->join(LtCourseuserSession::model()->tableName()." cus", "cus.id_user = u.idst");

		//check if we have to apply power user visibility filter
		if (Yii::app()->user->isAdmin) {
			$command->join(
				CoreUserPU::model()->tableName()." pu",
				"u.idst = pu.user_id AND puser_id = :id_admin",
				array(':id_admin' => Yii::app()->user->id)
			);
		}

		//handle results
		$results = $command->queryAll();
		if (!empty($results)) {
			foreach ($results as $record) {
				$output['options'][] = $record['userid'];
			}
		}

		//send output to the client
		$this->sendJSON($output);
	}




	/**
	 * Given an user idst and a session id, retrieve evaluation file and send it
	 * to the client, with original file name.
	 * If evaluation fiel is not present throw an exception (this shouldn't happen, though)
	 *
	 * @throws CException
	 * @throws Exception
	 */
	public function actionDownloadEvaluationFile(){

		//read input
		$rq = Yii::app()->request;
		$idUser = $rq->getParam('id_user', 0);

		//validate input data
		$enrollSessionModel = LtCourseuserSession::model()->findByPk(array(
			'id_user' => $idUser,
			'id_session' => $this->idSession
		));
		if (!$enrollSessionModel) { throw new CException('Invalid enrollment info'); }
		if (empty($enrollSessionModel->evaluation_file)) { throw new CException('No evaluation file'); }

		//send file to the client
		$storageManager = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LT_EVALUATION);
		/*
		$filePath = $storageManager->resourcePath($enrollSessionModel->evaluation_file);
		if (file_exists($filePath)) {
			//content type forcing download
			header("Content-type: application/download\n");
			//cache control
			header("Cache-control: private");
			header('Content-Disposition: attachment; filename="'. $enrollSessionModel->getOriginalFileName() .'"');
			readfile($filePath);
			Yii::app()->end();
		} else{
			throw new CException('Error: file not present');
		}
		*/
		$storageManager->sendFile($enrollSessionModel->evaluation_file, $enrollSessionModel->getOriginalFileName());
	}



	/**
	 * Export attendance sheet as a PDF file
	 */
	public function actionAttendancePdf() {

		//read and validate input
		$day = Yii::app()->request->getParam('day', false);
		if ($day === false) { throw new CException('Error: invalid specified date'); }
		$allSessions = false;
		if($day == 0){
			$allSessions = true;
			$listOfSessionsDates = LtCourseSessionDate::model()->findAllByAttributes(array(
					'id_session' => $this->_sessionModel->id_session
			));
		}
		//create html content
		$dateModel = LtCourseSessionDate::model()->findByPk(array(
			'id_session' => $this->idSession,
			'day' => $day
		));
		$html = '';
		$customHeader = false;

		$attendanceFieldsModel = AttendanceSheetMainField::model()->findByAttributes(
				array(
						'idCourse' => Yii::app()->request->getParam('course_id'),
						'idSession' => Yii::app()->request->getParam('id_session')
				)
		);
		if(!$attendanceFieldsModel)
			$attendanceFieldsModel = AttendanceSheetMainField::model()->findByPk(1);
		$listOfFieldsTitles = array();
		$order = explode(',', $attendanceFieldsModel->order);
		foreach($order as $field){

			if(is_numeric($attendanceFieldsModel->$field)
					&& $attendanceFieldsModel->$field != 0){
				$model = CoreUserField::model()->findByPk($attendanceFieldsModel->$field);
				if($model)
					$listOfFieldsTitles[] = $model;
			}else {
				$listOfFieldsTitles[] = $attendanceFieldsModel->$field;
			}
		}

		$listOfFieldsTitles = array_filter($listOfFieldsTitles);

		$html = $this->renderPartial('attendanceSheetPdf', array(
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idDate' => $day,
			'allSessions' => $allSessions,
			'dateModel' => $dateModel,
			'listOfFieldsTitles' => isset($listOfFieldsTitles) ? $listOfFieldsTitles : null,
			'listOfSessionsDates' => isset($listOfSessionsDates) ? $listOfSessionsDates : null,
			'customHeader' => $customHeader
		), true);

		//start pdf creation
		if ($allSessions === true)
			$PDF = Yii::app()->ePdf->mpdf('', 'A4-L');
		else $PDF = Yii::app()->ePdf->mpdf('', 'A4');

		// For supporting asian and arabic characters
		$PDF->useAdobeCJK = true;
		$PDF->setAutoFont(AUTOFONT_ALL);

		// Add Custom Content (i.e: Header || Footer)
		$event = new DEvent($this, array(
			'PDF' => &$PDF,
			'idCourse' => $this->idCourse,
			'idSession' => $this->idSession,
			'courseModel' => $this->courseModel,
			'sessionModel' => $this->sessionModel,
			'idDate' => $day,
			'dateModel' => $dateModel,
		));
		Yii::app()->event->raise('beforeRenderAttendanceSheetPDF', $event);

        //Load a stylesheet
/*		$stylesheet = file_get_contents(Yii::getPathOfAlias('admin') . '/css/admin.css');
        $PDF->WriteHTML($stylesheet, 1);
        $stylesheet = file_get_contents(Yii::app()->theme->basePath . '/css/classroom-attendance-sheet.css');
        $PDF->WriteHTML($stylesheet, 1);*/

		//put html content into PDF
		$PDF->WriteHTML($html, 2);

		//generate physical file
		$pdfFileName = 'AttendanceSheet_'.time().'.pdf';
		$tmpPdfDir = Yii::app()->basePath . '/runtime/tmpPdf/';
		if (!file_exists($tmpPdfDir)) { mkdir($tmpPdfDir); }
        $pdfFullPath = $tmpPdfDir . $pdfFileName;
        $PDF->Output($pdfFullPath, 'F');

		//send created file to the client
		$pdfFullPath = Yii::app()->basePath.'/runtime/tmpPdf/'.$pdfFileName;
		if (!empty($pdfFileName) && file_exists($pdfFullPath)) {
			// We'll be outputting a PDF
			header('Content-type: application/pdf');
			// It will be called downloaded.pdf
			header('Content-Disposition: attachment; filename="'. $pdfFileName .'"');
			// The PDF source is in original.pdf
			readfile($pdfFullPath);
		}
		Yii::app()->end();
	}

	public function actionConfigureAttendanceSheet(){
		$msg = '';
		$settings = AttendanceSheetMainField::model()->findByAttributes(array(
			'idCourse'=>Yii::app()->request->getParam('course_id'),
			'idSession'=>Yii::app()->request->getParam('id_session')
		));
		if(isset($_POST['submitSettings']) && !empty($_POST['submitSettings'])){
			$idCourse = Yii::app()->request->getParam('course_id');
			$idSession = Yii::app()->request->getParam('id_session');
			if(isset($_POST['enabledCustomSettings']) && $_POST['enabledCustomSettings'] == 1){
				if(!$settings) $settings = new AttendanceSheetMainField();
				$settings->fieldName1 = $_POST['fieldName1'];
				$settings->fieldName2 = $_POST['fieldName2'];
				$settings->fieldName3 = $_POST['fieldName3'];
				$settings->fieldName4 = $_POST['fieldName4'];
				$settings->fieldName5 = $_POST['fieldName5'];

				if (!($settings->fieldName1 == $settings->fieldName2
						&& $settings->fieldName2 == $settings->fieldName3
						&& $settings->fieldName3 == $settings->fieldName4
						&& $settings->fieldName4 == $settings->fieldName5
						&& $settings->fieldName1 == 0)) {
					// there is at least one active field
					$settings->order = $_POST['order'];
					$settings->idCourse = $idCourse;
					$settings->idSession = $idSession;
					$settings->save();
				}
			} elseif($settings){
				$settings->delete();
				$settings = null;
			}
			echo '<a class="auto-close"></a>';
			?>
			<script>
				$(function(){
					var link = $('<a class="open-dialog" data-dialog-class="attendance-sheet-print-dialog" style="display: block" href="<?= Docebo::createLmsUrl('ClassroomApp/instructor/axAttendanceSheet', array('course_id' => $idCourse,'id_session' => $idSession)) ?>">1</a>');
					console.log(link);
					$('body').append(link);
					$(document).controls();
					link.trigger('click');
					link.remove();
				});
			</script>
			<?php Yii::app()->end();
		}
		if(!$settings) {
			$settings = AttendanceSheetMainField::model()->findByPk(1);
		}
		$this->widget('common.widgets.attendanceSheet.AttendanceSheetWidget', array('settings' => $settings, 'msg' => $msg, 'renderPartial' => true));
	}
}