<?php

class SessionController extends ClassroomBaseController {

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:
		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admins only:
		$admin_only_actions = array('index', 'axSearchAutoComplete', 'axCopySession', 'axDeleteSession', 'searchAutoCompleteImportFromSession',
			'create', 'edit', 'save'/*, 'assign'*/, 'axApprove'/*, 'axAssignUsers', 'axDeleteUnassigned'*/, 'axReject', 'axSetWaiting' /*, 'enroll'*/, 'waiting', 'checkClassroomAvailability');
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/course/mod'),
			'actions' => $admin_only_actions,
		);

		$res[] = array(
			'allow',
			'roles' => array('enrollment/create', 'enrollment/view', 'enrollment/delete', 'enrollment/update'),
			'actions' => array('assign', 'axAssignUsers', 'axDeleteUnassigned'),
		);

		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess("/lms/admin/course/mod"))',
			'actions' => array('manageLearningObjects')
		);

		$res[] = array(
			'allow',
			'expression' => 'Yii::app()->controller->userCanAdminCourse',
			'actions' => array('manageLearningObjects')
		);

		// Allow PUs with Session view/mod permission
		// to create sessions
		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add"))',
			'actions' => array('create')
		);
		$res[] = array(
			'allow',
			'expression' => function() {
				if (Yii::app()->user->getIsGodadmin()) {
					return true;
				}
				if (Yii::app()->user->getIsPu()) {
					if (Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod")) {
						return true;
					} else {
						if ((Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add"))) {
							$idSession = Yii::app()->request->getParam('id_session', 0);
							$sessionModel = LtCourseSession::model()->findByPk($idSession);
							if ($sessionModel && $sessionModel->created_by == Yii::app()->user->id) {
								return true;
							}
						}
					}
				}
				return false;
			},//'(Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod"))',
			'actions' => array('edit', 'axDeleteSession')
		);
		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess("/lms/admin/course/moderate") ||  Yii::app()->user->checkAccess("/lms/admin/classroomsessions/assign")))',
			'actions' => array('waiting', 'axApprove', 'axReject')
		);

		$res[] = array(
			'allow',
			'expression' => '(Yii::app()->user->getIsPu() && (Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add") || Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod")))',
			'actions' => array('save', 'axCopySession')
		);

		//$res[] = array('deny', 'actions' => 'manageLearningObjects');

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		// Allow access to other actions only to logged-in users:
		$res[] = array(
			'allow',
			'users' => array('@'),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}



	/**
	 * Before action controller
	 * @param CAction $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		$rq = Yii::app()->request;

		switch ($action->id) {
			case 'assign':
			case 'waiting':
				$selectedItems = $rq->getParam('selectedItems', '');
				if (Yii::app()->request->isAjaxRequest && !empty($selectedItems)) {
					Yii::app()->session['selectedItems'] = explode(',', $selectedItems);
				} else {
					Yii::app()->session['selectedItems'] = array();
				}
				break;
		}

		JsTrans::addCategories(array('player', 'classroom', 'error'));

		return parent::beforeAction($action);
	}



	/**
	 * Main action of the controller, it will display the sessions management screen
	 *
	 * @throws CException
	 */
	public function actionIndex() {
		// Check that course model was loaded, before calling this action
		if (!$this->_courseModel)
			throw new CHttpException("Unable to load course information");

		// Load used scripts and styles
		$cs = Yii::app()->getClientScript();

		// Bootcamp menu
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		// Load the datepicker (used in the new/edit session modal)
		DatePickerHelper::registerAssets();

		// Load bootstro styles (used in the new/edit session modal)
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/bootstro/bootstro.css');

		// Render page
		$this->render('index', array(
			'course' => $this->courseModel
		));
	}

	protected function checkSessionPermissions(){
		//To do permissions
	}

	private function managementError(){
		$userManagementError = array();
		$userManagementError['data']['error'] = Yii::t('user_management', 'Some of the users you are trying to manipulate are not active');
		echo CJSON::encode($userManagementError);
		Yii::app()->end();
	}

	/**
	 * Edit session action
	 */
	public function actionEdit() {
		try {
			// Check that session and course models have been saved
			if (!$this->_courseModel || !$this->_sessionModel)
				throw new Exception("Invalid course or session id provided.", 1);

			if(Yii::app()->user->getIsPu()){
				if(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') &&
					(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') || $this->_sessionModel->created_by!=Yii::app()->user->getIdst())) {
						throw new CException('Power users can edit only sessions they have created');
				}
			}

			// Show session edit form
			$this->renderPartial('create', array(
				'model' => $this->_sessionModel,
				'actionUrl' => $this->createUrl('session/save')
			));
		} catch (Exception $e) {
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
	}

	/**
	 * This action manages session create process.
	 * It displays session creation dialog or, when confirm parameter is set, it will
	 * save session data into database
	 *
	 * @throws CException
	 */
	public function actionCreate() {
		try {
			// Check that course model has been saved
			if (!$this->_courseModel)
				throw new Exception("Invalid course id provided.", 1);

			// Show session creation form
			$learningCourseSession = new LtCourseSession();
			$learningCourseSession->course_id = $this->_courseModel->idCourse;
			$this->renderPartial('create', array(
				'model' => $learningCourseSession,
				'actionUrl' => $this->createUrl('session/save')
			));
		} catch (Exception $e) {
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
			echo '<a class="auto-close"></a>';
			Yii::app()->end();
		}
	}

	/**
	 * Save session data (with dates) in DB
	 * (AJAX call)
	 * @throws CException
	 */
	public function actionSave() {
		if (Yii::app()->request->isAjaxRequest) {
			$result = array('success' => false, 'message' => '');

			try {
				// Check POST data are ok (e.g. at least one session date)
				if (!empty($_POST['LtCourseSession']) && !empty($_POST['LtCourseSession']['dates'])) {
					$idSession = $_POST['LtCourseSession']['id_session'];
					if ($idSession)
						$learningCourseSession = LtCourseSession::model()->findByPk($idSession);
					else
						$learningCourseSession = new LtCourseSession();

					if(Yii::app()->user->getIsPu()){
						if($idSession && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') &&
							(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') || $learningCourseSession->created_by!=Yii::app()->user->getIdst())) {
							// A PU is trying to save/edit a session not created by him or doesn't have Sessions edit permission
							throw new CException("Power users can only update their own sessions");
						}
					}
					$oldMaxEnroll = $learningCourseSession->max_enroll;
					$learningCourseSession->attributes = $_POST['LtCourseSession'];
					if($learningCourseSession->last_subscription_date){
						$date = Yii::app()->localtime->fromLocalDateTime($learningCourseSession->last_subscription_date).' 23:59:59';
						$learningCourseSession->last_subscription_date = Yii::app()->localtime->toLocalDateTime($date, LocalTime::DEFAULT_DATE_WIDTH, LocalTime::DEFAULT_TIME_WIDTH, null, Yii::app()->localtime->getTimeZone());
					}


					// Check if the times are valid. First let custom plugins provide their own checks
					$event = new DEvent($this, array('session' => $learningCourseSession));
					Yii::app()->event->raise('BeforeILTSessionValidation', $event);

					// Custom validation performed.. Check what we get
					if($event->return_value['error'])
						throw new Exception($event->return_value['error']);

					// Run default validation
					if ($this->validateDates($_POST['LtCourseSession']['dates'])) {
						$learningCourseSession->saveWithDates($_POST['LtCourseSession']['dates']);
						$result['success'] = true;
						$learningCourse = LearningCourse::model()->findByPk($learningCourseSession->course_id);

						//AUTOMATICALY ENROLL WAITING USERS EXECUTION
						if($learningCourse->allow_automatically_enroll == 1 && $learningCourseSession->max_enroll > $oldMaxEnroll){
							$learningCourse->automaticallyEnroll(LearningCourse::TYPE_CLASSROOM, $learningCourseSession, $learningCourseSession->max_enroll - $oldMaxEnroll);
						}
						//AUTOMATICALY ENROLL WAITING USERS EXECUTION

					} else
						throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));


					if ($idSession) {
						Yii::log("Raising event ILTSessionChanged", 'info', __CLASS__);
						Yii::app()->event->raise('ILTSessionChanged', new DEvent($this, array(
							'session_id' => $learningCourseSession->id_session,
							'session' => $learningCourseSession
						)));
					}
					else
					{
						Yii::log("Raising event NewILTSessionCreated", 'info', __CLASS__);
						Yii::app()->event->raise('NewILTSessionCreated', new DEvent($this, array(
							'session_id' => $learningCourseSession->id_session,
							'session' => $learningCourseSession
						)));
					}

				} else
					throw new Exception("Can't save session!<br/>Missing or bad form data.");
			} catch (Exception $e) {
				$result['message'] = Yii::t('classroom', $e->getMessage());
			}


//			CVarDumper::dump($_POST['LtCourseSession'],10,true);


			$this->sendJSON($result);
		}

		Yii::app()->end();
	}


	/**
	 * Check if provided session dates are valid
	 * @param array $dates
	 * @return bool
	 */
	private function validateDates(array $dates){

		$result = false;

		foreach ($dates as $day => $theDay){

			$startTime = $this->timeToMin($day, $theDay['time_begin']);
			$endTime = $this->timeToMin($day, $theDay['time_end']);

			$tmp_breakBegin = $theDay['break_begin'];
			$tmp_breakEnd = $theDay['break_end'];
			if ($tmp_breakBegin == '0:00') { $tmp_breakBegin = '00:00'; }
			if ($tmp_breakEnd == '0:00') { $tmp_breakEnd = '00:00'; }
			if (!empty($tmp_breakBegin) && !empty($tmp_breakEnd) && !($tmp_breakBegin == '00:00' && $tmp_breakEnd == '00:00')) {
				$breakStart = $this->timeToMin($day, $tmp_breakBegin);
				$breakEnd = $this->timeToMin($day, $tmp_breakEnd);
			} else {
				$breakStart = false;
				$breakEnd = false;
			}

			if (!empty($breakStart) && !empty($breakEnd)) {
				if($startTime >= $endTime){
					$result = false;
				} else if($breakStart >= $breakEnd){
					$result = false;
				} else if($breakStart <= $startTime){
					$result = false;
				} else if($breakEnd >= $endTime){
					$result = false;
				} else {
					$result = true;
				}
			} else {
				if($startTime >= $endTime){
					$result = false;
				} else {
					$result = true;
				}
			}
		}

		return $result;
	}


	/**
	 * Simply convert a datetime into seconds
	 * @param $day the day date
	 * @param $time the time
	 * @return int the number of seconds
	 */
	private function timeToMin($day, $time){
		if ($time) {
			$dateTime = new DateTime($day . " " . $time);
			return $dateTime->getTimestamp();
		}
		else
			return 0;
	}


	/**
	 * Send a notification email to all enrolled to the passed session
	 * @param $learningCourseSession LtCourseSession
	 * @deprecated
	 */
	private function sendSessionChangeEmail($learningCourseSession) {

		// Collect all users enrolled to this session
		$enrolledUsers = $learningCourseSession->getEnrolledUsersExtended();

		// Prepare the list of recipients
		$recipients = array();
		foreach ($enrolledUsers as $user) {
			if (trim($user->email))
				$recipients[] = $user->email;
		}

		// Prepare the template for the body (localized)
		$emailBody = Yii::t('classroom', '<p>Dear user,</p>
            This message is to inform you that session <b>{name}</b> for course <b>{course}</b> was recently updated. <br/><br/>', array('{name}' => $learningCourseSession->name, '{course}' => $learningCourseSession->course->name));

		// Email subject
		$mailSubject = Yii::t('classroom', 'Session "{name}" for course "{course}" updated', array(
				'{name}' => $learningCourseSession->name, '{course}' => $learningCourseSession->course->name));

		// Send the email object
		$mm = new MailManager();
		$mm->setContentType('text/html');

		// Get the sender email
		$fromAdminEmail = Settings::get('mail_sender', null);
		if (!$fromAdminEmail) {
			$godAdminEmails = Yii::app()->user->getGodadminsEmails();
			if (is_array($godAdminEmails) && (count($godAdminEmails) > 0 ))
				$fromAdminEmail = $godAdminEmails[0];
		}

		// Send the email
		$result = $mm->mail(array($fromAdminEmail), $recipients, $mailSubject, $emailBody);
		if (!$result)
			Yii::log('Failed to send session change email to enrolled users', 'error');
	}

	/**
	 * Displays a list of course sessions for a student to enroll to
	 */
	public function actionEnroll() {

		if (!$this->_courseModel)
			throw new CHttpException("Invalid course provided");

		if($this->_courseModel->selling && !LearningCourseuser::isSubscribed(Yii::app()->user->id, $this->_courseModel->idCourse))
			throw new Exception(Yii::t('standard', '_OPERATION_FAILURE'));

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');

		if (Yii::app()->request->isPostRequest) {
			$sessionId = intval($_POST['courseSessionId']);

			$sessionModel = LtCourseSession::model()->findByPk(array('id_session' => $sessionId));
			$status = LtCourseuserSession::$SESSION_USER_SUBSCRIBED;
			if($sessionModel->isMaxEnrolledReached())
				$status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;

			if(LtCourseuserSession::enrollUser(Yii::app()->user->id, $sessionId, $status))
			{
				if($status == LtCourseuserSession::$SESSION_USER_SUBSCRIBED)
					$redirectUrl = Docebo::createAppUrl('lms:player/training/session', array(
							'course_id' => $this->getIdCourse(),
							'session_id' => $sessionId
					));
				else
				{
					//Yii::app()->user->setFlash('info', Yii::t('email', '_NEW_USER_SUBS_WAITING_SUBJECT'));

					$redirectUrl = Docebo::createAppUrl('lms:site/index', array(
							'opt' => 'fullmycourses'
					));
				}
				$this->redirect($redirectUrl);
			} else
				Yii::app()->user->setFlash('error', Yii::t('standard', '_OPERATION_FAILURE'));
		}

		$dataProvider = LtCourseSession::model()->sessionEnrollmentDataProvider($this->_courseModel->getPrimaryKey(), $this->_courseModel->allow_overbooking);

		$this->render('enroll', array(
			'dataProvider' => $dataProvider,
			'courseName' => $this->_courseModel->name
		));
	}

	/**
	 * Displays the list of course sessions for a student to select from
	 *
	 * todo: automatically select the session if there is only one session available; need to ask
	 */
	public function actionSelect() {
		if (Yii::app()->request->isPostRequest) {
			$sessionId = intval($_POST['courseSessionId']);

			// redirect to classroom app learner view
			$redirectUrl = Docebo::createAppUrl('lms:player/training/session', array(
					'course_id' => $this->getIdCourse(),
					'session_id' => $sessionId
			));
			$this->redirect($redirectUrl);
		}

		$this->breadcrumbs[Yii::t('course', '_WELCOME')] = Docebo::createLmsUrl('site/index');
		$this->breadcrumbs[Yii::t('menu_over', '_MYCOURSES')] = Docebo::createLmsUrl('site/index', array('opt' => 'fullmycourses'));

		$courseId = $this->getIdCourse();

		$courseSessions = LtCourseuserSession::model()->sessionSelectDataProvider(Yii::app()->user->id, $courseId);
		$courseModel = LearningCourse::model()->findByPk($this->getIdCourse());

		if (0 == $courseSessions->getTotalItemCount()) {
			$this->render('no_sessions', array('courseName' => $courseModel->name));
		} else {
			$this->render('select', array(
				'dataProvider' => $courseSessions,
				'courseName' => $courseModel->name
			));
		}
	}

	/**
	 * Merging both providers Course and Session and filter them
	 */
	private function mergeCourseSessionProviders(CActiveDataProvider $courseProvider, CActiveDataProvider $sessionProvider, $providerParams = array() , $autocomplete = false){


		$courseProv = array();
		$sessionProv = array();

		$courseProvider->pagination = false;
		$sessionProvider->pagination = false;

		// If Course Waiting list (checkbox filter is checked)
		$courseList = Yii::app()->request->getParam('courseList', 1);
		if($courseList && !$autocomplete){
			Yii::app()->session['iltCourseWaitingFilter'] = 1;
		}elseif( !$autocomplete){
			Yii::app()->session['iltCourseWaitingFilter'] = 0;
		}

		// If Session Waiting list (checkbox filter is checked)
		$sessionList = Yii::app()->request->getParam('sessionList', 0);
		if($sessionList && !$autocomplete){
			Yii::app()->session['iltSessionWaitingFilter'] = 1;
		}elseif(!$autocomplete){
			Yii::app()->session['iltSessionWaitingFilter'] = 0;
		}

		// By default we're showing only the course waiting list
		$courseProv = $courseProvider->getData();

		// Here we filter which provider should be loaded

		if(($sessionList == 1 && !$autocomplete) || (isset(Yii::app()->session['iltSessionWaitingFilter']) && Yii::app()->session['iltSessionWaitingFilter']  && $autocomplete)){
			$sessionProv = $sessionProvider->getData();
		}

		if(Yii::app()->request->isAjaxRequest){
			if(($courseList == 0 && !$autocomplete) || ((!isset(Yii::app()->session['iltCourseWaitingFilter']) || !Yii::app()->session['iltCourseWaitingFilter']) && $autocomplete)){
				$courseProv = array();
			}
		}

		$mergedProviders = CMap::mergeArray(
			$courseProv,
			$sessionProv
		);

		return new CArrayDataProvider($mergedProviders, $providerParams);
	}

	/*
	 * Assign unassigned users to a session
	 */

	public function actionAssign() {

		/* @var $courseModel LearningCourse */
		$courseModel = LearningCourse::model()->findByPk($this->getIdCourse());

		// If the user is PU he can manage only his sessions
		$applyPowerUserFilter = true;

		$params = array();
		if (($data = Yii::app()->request->getParam('data')) != null) {

			if ($data['autocomplete']) {
				$params['user_query'] = $data['query'];
			}

			$courseModel = LearningCourse::model()->findByPk($data['course_id']);
			$courseProvider = $courseModel->unassignedUsersDataProvider($params);
			$sessionParams = array('courseId' => $courseModel->idCourse, 'applyPowerUserFilter' => $applyPowerUserFilter, 'user_query' => $params['user_query']);
			$sessionProvider = LtCourseSession::model()->dataProviderUserWaiting($sessionParams);
			$provider = $this->mergeCourseSessionProviders($courseProvider, $sessionProvider, array() , true);

			$result = array('options' => array());
			foreach ($provider->getData() as $courseuser) {
				$user = $courseuser->learningUser;
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$text = "{$username}";

				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$text .= " - $names";

				$result['options'][$username] = $text;
			}
			$this->sendJSON($result);
		}

		$searchInput = Yii::app()->request->getParam('searchText', '');

		$params['user_search'] = $searchInput;
		$courseProvider = $courseModel->unassignedUsersDataProvider($params);
		$sessionParams = array('courseId' => $courseModel->idCourse, 'applyPowerUserFilter' => $applyPowerUserFilter, 'user_search' => $searchInput);
		$sessionProvider = LtCourseSession::model()->dataProviderUserWaiting($sessionParams);

		$providerParams = array(
			'keyField' => false,
			'pagination' => array(
				'pageSize' => Settings::get('elements_per_page', 10)
			)
		);

		$provider = $this->mergeCourseSessionProviders($courseProvider, $sessionProvider, $providerParams);

		$this->render('assign', array(
			'dataProvider' => $provider,
			'course' => $courseModel,
			'permissions' => array(
				'assign' => (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/create')),
				'del' => (Yii::app()->user->getIsGodadmin() || Yii::app()->user->checkAccess('enrollment/delete'))
			)
		));
	}

	/**
	 * Renders a grid with the session users that have the status 'waiting'
	 */
	public function actionWaiting() {

		if(Yii::app()->user->getIsPu()){
			$isPuWithSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
			$isPuWithSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
			$isSessionOwnedByPu = ($this->_sessionModel->created_by == Yii::app()->user->getIdst());
			if (!$isPuWithSessionEditPerm && !($isSessionOwnedByPu && $isPuWithSessionAddPerm) && !Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign')) {
				throw new CException('Power users can edit only their own sessions');
			}
		}

		/* @var $session LtCourseSession */
		$session = LtCourseSession::model()->with('learningCourse')->findByPk(Yii::app()->request->getParam('id_session'));
		$params = array();
		$params['applyPowerUserFilter'] = true;


		if (($data = Yii::app()->request->getParam('data')) != null) {

			if ($data['autocomplete']) {
				$params['user_query'] = $data['query'];
			}

			$dataProvider = $session->dataProviderWaiting($params);
			$dataProvider->pagination = false;
			$result = array('options' => array());
			foreach ($dataProvider->data as $courseuser) {
				$user = $courseuser->learningUser;
				$username = Yii::app()->user->getRelativeUsername($user->userid);
				$text = "{$username}";

				$names = array();
				if (!empty($user->firstname))
					$names[] = $user->firstname;
				if (!empty($user->lastname))
					$names[] = $user->lastname;
				$names = join(' ', $names);
				if ($names)
					$text .= " - $names";

				$result['options'][$username] = $text;
			}
			$this->sendJSON($result);
		}

		$params['user_search'] = Yii::app()->request->getParam('searchText', '');
		$dataProvider = $session->dataProviderWaiting($params);

		//--- check permissions for admins and sub-admins
		$canAssign = $canDelete = self::checkWaitingPermissions();
		//---

		$this->render('waiting', array(
			'dataProvider' => $dataProvider,
			'session' => $session,
			'course' => $session->learningCourse,
			'permissions' => array(
				'assign' => $canAssign,
				'del' => $canDelete
			)
		));
	}

	public function actionAxAssignUsers() {
		$idCourse = Yii::app()->request->getParam('id_course');
		$course = LearningCourse::model()->findByPk($idCourse);

		$isBulkAction = Yii::app()->request->getParam('isBulkAction');

		$setWaiting = Yii::app()->request->getParam('set_waiting');

		if (($sessionId = intval(Yii::app()->request->getParam('id_session'))) > 0) {

			$userIds = array();
			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (empty($selectedUsers)) {
				$userIds[] = Yii::app()->request->getParam('id_user');
			} else {
				$userIds = explode(',', $selectedUsers);
			}

			// If a user is already in learning_courseuser, it's in waiting list and it should NOT take seat
			// This is important for decreasing Power User's seats in the course
			// by those number of users
			$usersNewToCourse = array();
			$usersInCourseAlready = LearningCourseuser::getEnrolledByCourse($idCourse);


			if(!$setWaiting) {
				//check the session max users
				if (!LtCourseSession::checkIfEnrollmentsAreAvailable($sessionId, count($userIds))) {
					echo Yii::t('classroom', "Limit of max enrollable users reached");
					Yii::app()->end();
				}
			}

			foreach($userIds as $userId){
				if(!in_array($userId, $usersInCourseAlready)){
					$usersNewToCourse[] = $userId;
				}
			}

			if(count($usersNewToCourse) && CoreUserPU::isPUAndSeatManager()) {
				// The current user is a Power user and he is
				// allowed to enroll via "seats" only.
				// Check if he has enough available for this course
				if (!CoreUserPuCourse::checkIfPuHasEnoughSeatsInCourse( count( $usersNewToCourse ), $idCourse ) ) {
					echo Yii::t('standard', "You don't have enough seats for this course");
					Yii::app()->end();
				}
			}

			$status = false;
			if($setWaiting){
				$status = LtCourseuserSession::$SESSION_USER_WAITING_LIST;
			}

			foreach ($userIds as $userId) {
				if (LtCourseuserSession::enrollUser($userId, $sessionId, $status)) {

					//We are no more in "waiting" status for the course
					$courseUserModel = LearningCourseUser::model()->findByAttributes(array(
						'idUser' => $userId,
						'idCourse' => $idCourse
					));
					if ($courseUserModel && $courseUserModel->waiting > 0) {
						$courseUserModel->waiting = 0;
						$courseUserModel->save(false);
					}

					$userModel = CoreUser::model()->findByPk($userId);
					if($userModel){
						Yii::log("Raised event UserApproval for course ${$idCourse} and user {$userId}", 'info', __CLASS__);
						Yii::app()->event->raise('UserApproval', new DEvent($this, array(
							'course' => $course,
							'user' => $userModel,
						)));
					}
				}

			}

			if(count($usersNewToCourse) && CoreUserPU::isPUAndSeatManager()){
				// The current user is a PU only allowed to enroll
				// users in courses using "seats", so decrease his available
				// seats in the course by the amount of users he enrolled
				$seatsToDecreaseCount = -count($usersNewToCourse);
				CoreUserPU::modifyAvailableSeats($idCourse, $seatsToDecreaseCount);
			}

			if($setWaiting){
				echo CHtml::link('', Docebo::createAdminUrl('ClassroomApp/session/waiting', array(
					'course_id'=>$idCourse,
					'id_session'=>$sessionId
				)), array('class'=>'auto-close redirect'));
				Yii::app()->end();
			}else{
				// close dialog and reload grid
				echo '<a href="#" class="auto-close" data-unselect="'.implode(',', $userIds).'"></a>';
				Yii::app()->end();
			}
		}

		echo $this->renderPartial('_modal_assign', array(
			'course' => $course,
			'isBulkAction' => ($isBulkAction)
			), true, true);
	}

	/**
	 * Bulk delete the selected users
	 */
	public function actionAxDeleteUnassigned() {
		/* @var $rq CHttpRequest */
		$rq = Yii::app()->request;

		$idCourse = Yii::app()->request->getParam('id_course');
		$selectedUsers = trim(Yii::app()->request->getParam('selected_users'));
		$userIds = (empty($selectedUsers) ? array() : explode(',', $selectedUsers));

		$lpUserIds = array();
		if (PluginManager::isPluginActive('CurriculaApp')) {
			if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($idCourse)) {
				$lpAssignments = LearningCoursepath::checkEnrollmentsMultipleUsers($userIds, $idCourse);
				foreach ($lpAssignments as $lpIdUser => $lpList) {
					if (!empty($lpList)) {
						$toBeExcluded[] = $lpIdUser;
					}
				}
				//update input list, removing non-unenrollable users
				if (!empty($toBeExcluded)) {
					$userIds = array_diff($userIds, $toBeExcluded);
					$lpUserIds = $toBeExcluded;
				}
				//check if any user is remaining to be unenrolled, otherwise send a message to the user
				if (empty($userIds)) {
					$this->renderPartial('_modal_delete_unassigned_none', array(
						'userIds' => $userIds,
						'lpUserIds' => $lpUserIds
					));
					Yii::app()->end();
				}
			}
		}

		if ($rq->isPostRequest && $rq->getParam('confirm', false) !== false) {

			//in case of PU seats available, count real user deletion number and restore the seats
			$seatsToRestore = 0;
			foreach ($userIds as $userId) {
				$res = LearningCourseuser::model()->deleteAllByAttributes(array(
					'idUser' => $userId,
					'idCourse' => $idCourse
				));
				if ($res)
					$seatsToRestore++;
			}

			if ($seatsToRestore && Yii::app()->user->getIsPu() && CoreUser::adminRuleEnabled(Yii::app()->user->id, 'allow_buy_seats'))
			{
				// The current user is a Power user and he is
				// allowed to enroll via "seats" only.
				// make the seat available again
				$puCourseModel = CoreUserPuCourse::model()->findByAttributes(array(
					'puser_id'=>Yii::app()->user->getIdst(),
					'course_id'=>$idCourse,
				));
				if($puCourseModel){
					$puCourseModel->available_seats += $seatsToRestore;
					$puCourseModel->save();
				}
			}

			// close dialog and reload grid
			if (is_array(Yii::app()->session['selectedItems'])) {
				Yii::app()->session['selectedItems'] = array_diff(Yii::app()->session['selectedItems'], $userIds);
			}
			echo '<a href="#" class="auto-close" data-unselect="'.implode(',', $userIds).'"></a>';
			Yii::app()->end();
		}

		$this->renderPartial('_modal_delete_unassigned', array(
			'userIds' => $userIds,
			'lpUserIds' => $lpUserIds
		));
	}



	/**
	 * Check specific permissions to access waiting management features
	 * @return bool
	 */
	public static function checkWaitingPermissions() {
		if (Yii::app()->user->getIsGodAdmin()) {
			return true;
		} elseif (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess("/lms/admin/course/moderate")) {
			$puCanActivateEnrollments = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
			if ($puCanActivateEnrollments) {
				$idSession = Yii::app()->request->getParam('id_session', 0);
				if ($idSession <= 0) return false;
				$sessionModel = LtCourseSession::model()->findByPk($idSession);
				if (!$sessionModel) return false;
				$sessionBelongsToPu = ($sessionModel->created_by == Yii::app()->user->id);
				$puHasSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
				$puHasSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
				if ($puHasSessionEditPerm || ($puHasSessionAddPerm && $sessionBelongsToPu)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Changes a session subscribed user's status from 'waiting' to 'subscribed'
	 */
	public function actionAxApprove() {



		if(Yii::app()->user->getIsPu()) {
			$canPuManageWaiting = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
			$isPuWithSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
			$isPuWithSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
			$isPuWithSessionAssingPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign');
			$isSessionOwnedByPu = ($this->_sessionModel->created_by == Yii::app()->user->getIdst());
			if (!$canPuManageWaiting || (!$isPuWithSessionEditPerm && !($isSessionOwnedByPu && $isPuWithSessionAddPerm) && !$isPuWithSessionAssingPerm) ) {
				throw new CException('Power users can only approve enrollments in their own sessions');
			}
		}

		$userIds = array();
		$sessionId = Yii::app()->request->getParam('id_session');
		$invalidUser = false;

		if (Yii::app()->request->isPostRequest) {


			$selectedUsers = Yii::app()->request->getParam('selected_users');



			if (!empty($selectedUsers)) {
				$userIds = explode(',', $selectedUsers);
			} else {
				$userIds[] = Yii::app()->request->getParam('id_user');
			}

			if (!LtCourseSession::checkIfEnrollmentsAreAvailable($sessionId, count($userIds))) {
				echo CJSON::encode(array('data' => array('error' => Yii::t('classroom', 'Limit of max enrollable users reached'))));
				Yii::app()->end();
			}

			foreach ($userIds as $userId) {
				/* @var $model LtCourseuserSession */
				$model = LtCourseuserSession::model()->with('ltCourseSession')->findByAttributes(array(
					'id_user' => $userId,
					'id_session' => $sessionId
				));

				if ($model) {
					if($model->idUser->valid <= 0){
						$invalidUser = true;
						continue;
					}

					$model->status = $model::$SESSION_USER_SUBSCRIBED;
					if ($model->hasAttribute('waiting')) { $model->waiting = 0; } // NOTE: "waiting" column is deprecated for "lt_courseuser_session" table. Use "status" instead.
					$result = $model->save();
					if($result)
					{
						Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_ACTIVATED, new DEvent($this, array(
							'session_id'    => $sessionId,
							'user'          => $userId,
						)));
					}

					// also change the status of the corresponding _courseuser row
					$cuModel = LearningCourseuser::model()->findByAttributes(array(
						'idUser' => $userId,
						'idCourse' => $model->ltCourseSession->course_id
					));
					if ($cuModel) {
						$cuModel->status = LearningCourseuser::$COURSE_USER_SUBSCRIBED;
						$cuModel->waiting = 0;  // In case the subscription is moderated
						$cuModel->save();
					}
				}
			}

			if($invalidUser){
				$this->managementError();
			}
			// close dialog and reload grid
			$this->sendJSON('<a href="#" class="auto-close"></a>');
			Yii::app()->end();
		}

		if (1 === intval(Yii::app()->request->getParam('isBulkAction'))) {
			$this->renderPartial('_modal_approve_waiting', array('sessionId' => $sessionId));
		}
	}

	/**
	 * Sends the selected users back to the global waiting list of the course
	 * (currently they are in the waiting list for a specific session)
	 */
	public function actionAxSetWaiting(){
		if(Yii::app()->user->getIsPu()) {
			$canPuManageWaiting = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
			$isPuWithSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
			$isPuWithSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
			$isPuWithSessionAssingPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign');
			$isSessionOwnedByPu = $this->_sessionModel->created_by != Yii::app()->user->getIdst();
			if (!$canPuManageWaiting || (!$isPuWithSessionEditPerm && !($isSessionOwnedByPu && $isPuWithSessionAddPerm) && !$isPuWithSessionAssingPerm)) {
				throw new CException('Power users can only approve enrollments in their own sessions');
			}
		}

		$userIds = array();
		$sessionId = Yii::app()->request->getParam('id_session');
		$invalidUser = false;

		if (Yii::app()->request->isPostRequest) {
			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (!empty($selectedUsers)) {
				$userIds = explode(',', $selectedUsers);
			} else {
				$userIds[] = Yii::app()->request->getParam('id_user');
			}

			foreach ($userIds as $userId) {
				/* @var $model LtCourseuserSession */
				$model = LtCourseuserSession::model()->with('ltCourseSession')->findByAttributes(array(
					'id_user' => $userId,
					'id_session' => $sessionId
				));

				if($model){
					if($model->idUser->valid <= 0){
						$invalidUser = true;
						if(is_array($userIds) && in_array($userId, $userIds)){
							$idKey = array_search($userId, $userIds);
							if($idKey !== false){
								unset($userIds[$idKey]);
							}
						}
						continue;
					}
				}
			}

			LtCourseuserSession::model()->deleteAllByAttributes(array(
				'id_user' => $userIds,
				'id_session' => $sessionId
			));

			if($invalidUser){
				$this->managementError();
			}
			// close dialog and reload grid
			$this->sendJSON('<a href="#" class="auto-close"></a>');
			Yii::app()->end();
		}

		if (1 === intval(Yii::app()->request->getParam('isBulkAction'))) {
			$this->renderPartial('_modal_reject_waiting', array('sessionId' => $sessionId));
		}
	}

	/**
	 * Deletes a user's session subscription and also the course enrollment of the user
	 * if this is the only session enrollment to this course
	 */
	public function actionAxReject() {

		if(Yii::app()->user->getIsPu()) {
			$canPuManageWaiting = CoreUser::adminRuleEnabled(Yii::app()->user->id, 'direct_course_subscribe');
			$isPuWithSessionAddPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add');
			$isPuWithSessionEditPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod');
			$isPuWithSessionAssignPerm = Yii::app()->user->checkAccess('/lms/admin/classroomsessions/assign');
			$isSessionOwnedByPu = $this->_sessionModel->created_by != Yii::app()->user->getIdst();
			if (!$canPuManageWaiting || (!$isPuWithSessionEditPerm && !($isSessionOwnedByPu && $isPuWithSessionAddPerm) && !$isPuWithSessionAssignPerm)) {
				throw new CException('Power users can only approve enrollments in their own sessions');
			}
		}

		$userIds = array();
		$sessionId = Yii::app()->request->getParam('id_session');

		if (Yii::app()->request->isPostRequest) {
			$selectedUsers = Yii::app()->request->getParam('selected_users');

			if (!empty($selectedUsers)) {
				$userIds = explode(',', $selectedUsers);
			} else {
				$userIds[] = Yii::app()->request->getParam('id_user');
			}

			foreach ($userIds as $userId) {
				/* @var $model LtCourseuserSession */
				$model = LtCourseuserSession::model()->findByAttributes(array(
					'id_user' => $userId,
					'id_session' => $sessionId
				));

				if ($model) {
					// delete session enrollment
					if($model->delete()) {
						Yii::app()->event->raise(EventManager::EVENT_USER_CLASSROOM_SESSION_ENROLL_DECLINED, new DEvent($this, array(
							'session_id'    => $sessionId,
							'user'          => $userId,
						)));
					}

					/* @var $session LtCourseSession */
					$session = LtCourseSession::model()->findByPk($sessionId);

					// find the other course sessions
					/* @var $sessions LtCourseSession[] */
					$sessions = LtCourseSession::model()->findAllByAttributes(array(
						'course_id' => $session->course_id
					));

					if ($sessions) {
						// check if user is subscribed to other sessions of this course
						$sessionIds = array_keys(CHtml::listData($sessions, 'id_session', ''));
						$criteria = new CDbCriteria();
						$criteria->addCondition('id_user = :id_user');
						$criteria->addInCondition('id_session', $sessionIds);
						$criteria->params[':id_user'] = Yii::app()->request->getParam('id_user');
						$courseuserSessions = LtCourseuserSession::model()->findAll($criteria);

						if (!$courseuserSessions) {
							// unsubscribe from course
							LearningCourseuser::model()->deleteAllByAttributes(array(
								'idUser' => $userId,
								'idCourse' => $session->course_id
							));
						}
					}
				}
			}

			// close dialog and reload grid
			$this->sendJSON('<a href="#" class="auto-close"></a>');
			Yii::app()->end();
		}

		if (1 === intval(Yii::app()->request->getParam('isBulkAction'))) {
			$lpAssignments = array();
			if (PluginManager::isPluginActive('CurriculaApp'))
			{
				$idCourse = Yii::app()->request->getParam('id_course');
				if (LearningCoursepathCourses::isCourseAssignedToLearningPaths($idCourse))
				{
					$waitingUsers =	Yii::app()->db->createCommand()
									->select('id_user')
									->from(LtCourseuserSession::model()->tableName())
									->where('id_session = :id_session AND status = :status', array(':id_session' => $sessionId, ':status' => LtCourseuserSession::$SESSION_USER_WAITING_LIST))
									->queryColumn();
					if(!empty($waitingUsers))
						$lpAssignments = LearningCoursepath::getEnrollments($waitingUsers, $idCourse);
				}
			}

			$this->renderPartial('_modal_reject_waiting', array('sessionId' => $sessionId, 'lpAssignments' => $lpAssignments));
		}
	}

	/**
	 * @param $data LtCourseuserSession
	 * @param $index
	 * @return string
	 */
	protected function sessionWaitingGridRenderApprove($data, $index = 0) {

		$errorClass = null;

		if($data->learningUser->valid <= 0){
			$errorClass = 'user-manipulation-error';
		}

		return CHtml::link('', $this->createUrl('axApprove', array(
					'id_user' => $data->id_user,
					'id_session' => $data->id_session
				)), array(
				'class' => 'suspend-action ' . $errorClass,
				'rel' => 'tooltip',
				'title' => Yii::t('standard', '_APPROVE')
		));
	}

	/**
	 * @param $data LtCourseuserSession
	 * @param $index
	 * @return string
	 */
	protected function sessionWaitingGridRenderReject($data, $index = 0, $waitingSessionView = false) {
		$lpEnrollments = array();
		if(PluginManager::isPluginActive('CurriculaApp') && !$waitingSessionView)
			$lpEnrollments = LearningCoursepath::checkEnrollment($data->id_user, $data['learningCourseuser']->idCourse, true);

		if(!empty($lpEnrollments))
		{
			$canUnenrollMessage = '<div class="cannot-unenroll-tooltip">';
			$canUnenrollMessage .= '<span>' . Yii::t('myactivities', 'Learning plans') . ':</span><br />';
			$canUnenrollMessage .= '<ul>';
			foreach ($lpEnrollments as $lpId => $lpInfo) {
				$canUnenrollMessage .= '<li>- ' . $lpInfo['path_name'] . '</li>';
			}
			$canUnenrollMessage .= '</ul>';
			$canUnenrollMessage .= '</div>';

			return CHtml::link('', 'javascript:return false;', array(
					'rel' => 'tooltip',
					'data-html' => 'true',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'title' => $canUnenrollMessage,
					'class' => 'delete-action',
					'style' => "background-position:-714px -19px;"
				));
		}
		else
			return CHtml::link('', $this->createUrl('axReject', array(
						'id_user' => $data->id_user,
						'id_session' => $data->id_session
					)), array(
					'class' => 'delete-action',
					'rel' => 'tooltip',
					'title' => Yii::t('standard', '_DEL')
			));
	}

	protected function sessionAssignGridRenderAssign($data, $index = 0) {
		if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->checkAccess("enrollment/create")) { return ''; }
		if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->checkAccess("/lms/admin/course/moderate")) { return ''; }
		$url = $this->createUrl('axAssignUsers', array(
			'id_user' => $data->idUser,
			'id_course' => $this->getIdCourse()
		));
		return CHtml::link(Yii::t('standard', '_ASSIGN'), $url, array(
				'class' => 'custom-grid-link open-dialog',
				'data-dialog-class' => 'modal-session-assign-users'
		));
	}

	protected function sessionsSelectGridRenderStatus($data, $index) {
		$css = '';
		switch ($data['status']) {
			case LtCourseuserSession::$SESSION_USER_END:
				$css = 'green';
				break;
			case LtCourseuserSession::$SESSION_USER_BEGIN:
				$css = 'orange';
				break;
			case LtCourseuserSession::$SESSION_USER_SUBSCRIBED:
			default:
				$css = 'grey';
				break;
		}
		$statuses = LtCourseuserSession::model()->getStatusesArray();
		$title = (isset($statuses[$data['status']]) ? $statuses[$data['status']] : '');
		$html = '<span class="i-sprite is-circle-check ' . $css . '"'.($title ? ' title='.CJSON::encode($title).' rel="tooltip"' : '').'></span>';
		return $html;
	}

	/**
	 * Returns the array of dates for the current session (as JSON array)
	 * @param LtCourseSession $sessionModel
	 * @return string
	 */
	protected function getSessionDatesAsJson(LtCourseSession $sessionModel) {

		// Get the array of session dates
		$dates = $sessionModel->getDates();
		$jsonArray = array();
		foreach ($dates as $date) {
			// Remove seconds from mysql date result
			$date->time_begin = substr($date->time_begin, 0, 5);
			$date->time_end = substr($date->time_end, 0, 5);
			$date->break_begin = substr($date->break_begin, 0, 5);
			$date->break_end = substr($date->break_end, 0, 5);

			$jsonArray[$date->day] = $date;
		}

		if (!empty($jsonArray))
			return CJSON::encode($jsonArray);
		else
			return "{}";
	}

	/**
	 * Returns the current timezone
	 */
	protected function getCurrentTimezone() {
		return Yii::app()->localtime->getTimeZone();
	}

	/**
	 * Returns the current timezone name with UTC offset
	 * @return string
	 */
	protected function getCurrentTimezoneWithOffset() {
        $currentTimezone = $this->getCurrentTimezone();
        return Yii::app()->localtime->getTimezoneOffset($currentTimezone);
	}

	/**
	 * Returns the array of timezones
	 */
	protected function getTimezonesArray() {
        return Yii::app()->localtime->getTimezonesArray();
	}

	/**
	 * Returns a JSON array of timezone offsets
	 */
	protected function getTimezoneOffsetsAsJson() {
		return CJson::encode($this->getTimezonesArray());
	}

	/**
	 * Return the list of locations records (filtered by power user)
	 */
	protected function getLocations() {
		$criteria = new CDbCriteria();
		$criteria->order = 't.name ASC';

		if (Yii::app()->user->getIsPu()) {
			$criteria->with['powerUserManagers'] = array(
				'joinType' => 'INNER JOIN',
				'condition' => 'powerUserManagers.idst=:puser_id',
				'params' => array(':puser_id' => Yii::app()->user->id),
			);
			$criteria->together = true;
			$criteria->group = 't.id_location';
		}

		return LtLocation::model()->findAll($criteria);
	}

	/**
	 * Returns the list of locations available for the dropdown
	 */
	protected function getLocationsArray() {
		$selectOptions = array(0 => Yii::t('classroom', 'No location selected'));
		$locations = $this->getLocations();
		foreach ($locations as $location)
			$selectOptions[$location->id_location] = $location->name;

		return $selectOptions;
	}

	/**
	 * Returns the array of locations as a JSON object
	 */
	protected function getLocationsAsJson() {
		$locations = array();
		$records = $this->getLocations();
		foreach ($records as $record) {
			$locations[$record->id_location] = array(
				'id_location' => $record->id_location,
				'name' => $record->name,
				'formatted_address' => $record->renderAddress()
			);
		}

		return CJson::encode($locations);
	}

	/**
	 * Returns the list of classrooms per each location
	 */
	protected function getClassroomsAsJson() {
		$classrooms = array();
		$models = LtClassroom::model()->findAll(array('order' => 'name'));
		foreach ($models as $model) {
			$classrooms[$model->id_location][$model->id_classroom] = $model;
		}
		return CJSON::encode($classrooms);
	}


	/**
	 * Returns the list of all allocated classrooms (to check availability)
	 */
	protected function getAllocatedClassroomsAsJson() {
		$allocatedClassrooms = array();
		$criteria = new CDbCriteria();
		$criteria->addCondition("day >= :today"); //we don't need past days, they are already gone ...
		$criteria->params[':today'] = date("Y-m-d");
		$models = LtCourseSessionDate::model()->findAll($criteria);
		foreach ($models as $model) {
			if (!empty($model->id_classroom)) {
				$allocatedClassrooms[$model->id_classroom][] = array(
					'day' => $model->day,
					'time_begin' => $model->time_begin,
					'time_end' => $model->time_end,
					'break_begin' => $model->break_begin,
					'break_end' => $model->break_end,
					'timezone' => $model->timezone,
					'id_location' => $model->id_location,
					'id_session' => $model->id_session
				);
			}
		}
		if (empty($allocatedClassrooms)) { $allocatedClassrooms = false; }
		return CJSON::encode($allocatedClassrooms);
	}


	/**
	 * Returns a JSON array with all month names (localized);
	 */
	protected function getLocalizedMonthNames() {
		$months = array(
			"_JANUARY", "_FEBRUARY", "_MARCH", "_APRIL", "_MAY", "_JUNE",
			"_JULY", "_AUGUST", "_SEPTEMBER", "_OCTOBER", "_NOVEMBER", "_DECEMBER"
		);

		foreach ($months as &$month)
			$month = Yii::t('calendar', $month);

		return CJSON::encode($months);
	}

	/**
	 * this action manages session deletion. It displays delete dialog and, if confirmed,
	 * it performs database operations
	 *
	 * @throws CException
	 */
	public function actionAxDeleteSession() {

		try {
			// Check session
			$session = $this->sessionModel;
			if (!$session)
				throw new CException("Wrong session provided");

			// Check if this is a form submit
			$rq = Yii::app()->request;
			if ($rq->getParam('confirm', false) !== false) {
				// This will call delete events, deleting also dates and subscriptions associated to this session
				$rs = $session->deleteSession();
				$output = '<a class="auto-close"></a>';
			} else {
				// If no confirm, then we are opening the confirmation dialog
				$output = $this->renderPartial('_deleteSessionConfirm', array(
					'model' => $session
					), true);
			}
		} catch (CException $e) {
			//$output = $e->getMessage();
			$output = '<a class="auto-close"></a>';
		}

		// Send output to the user dialog
		echo $output;
		Yii::app()->end();
	}

	/**
	 * Return info for autocomplete in session management table search input
	 */
	public function actionAxSearchAutoComplete() {

		$input = Yii::app()->request->getParam('data');
		$output = array('options' => array());

		$idCourse = (int) $input['course_id'];
		$searchText = trim(strtolower($input['query'])); // TODO: see if some formatting is needed for seatchText

		if ($searchText) {
			$criteria = new CDbCriteria();
			$criteria->addCondition("course_id = :id_course");
			$criteria->addCondition("name LIKE :search_text");
			$criteria->params[':id_course'] = $idCourse;
			$criteria->params[':search_text'] = '%' . $searchText . '%';
			$records = LtCourseSession::model()->findAll($criteria);

			if (!empty($records)) {
				foreach ($records as $record) {
					$output['options'][] = $record['name'];
				}
			}
		}

		$this->sendJSON($output);
	}

	/**
	 * This action manages session copy operations. It displays copy dialog and, if confirmed,
	 * it performs related database operations
	 *
	 * @throws CException
	 */
	public function actionAxCopySession() {

		if (!$this->_sessionModel)
			throw new CHttpException("Invalid session id provided");

		$idCourse = $this->_sessionModel->course_id;
		$idSession = $this->_sessionModel->getPrimaryKey();

		if(Yii::app()->user->getIsPu()) {
			if(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/mod') &&
				(!Yii::app()->user->checkAccess('/lms/admin/classroomsessions/add') || $this->_sessionModel->created_by!=Yii::app()->user->getIdst())) {
				throw new CException('Power users can only make copies of their own sessions');
			}
		}

		$rq = Yii::app()->request;
		if ($rq->getParam('confirm', false) !== false) {
			// Form submission part
			try {
				$newName = $rq->getParam('new-name', '');

				$duplicateDates = ($rq->getParam('duplicate-dates', 0) > 0);
				if ($duplicateDates) {
					$repeats = $rq->getParam('repeats', LtCourseSession::REPEAT_DAILY);
					$numRepeats = $rq->getParam('num-repeats', 1);
				}

				$numCopies = $rq->getParam('num-copies', 1);
				if ($numCopies <= 0)
					$numCopies = 1;

				for ($i = 1; $i <= $numCopies; $i++) {
					$daysShift = 0;
					if ($duplicateDates) {
						switch ($repeats) {
							case LtCourseSession::REPEAT_DAILY: {
									$daysShift = $i * $numRepeats;
								} break;
							case LtCourseSession::REPEAT_WEEKLY: {
									$daysShift = $i * $numRepeats * 7;
								} break;
							case LtCourseSession::REPEAT_MONTHLY: {
									$daysShift = $i * $numRepeats * 30; // TODO: is this right? we should consider 31 days and 28/29days months ...
								} break;
							case LtCourseSession::REPEAT_YEARLY: {
									$daysShift = $i * $numRepeats * 365;
								} break;
						}
					}
					$newSession = $this->_sessionModel->copy($newName, $duplicateDates, $daysShift);
					if (!$newSession) {
						throw new CHttpException('Error while copying session');
					}else{
						if(Yii::app()->user->getIsPu()) {
							// Make the new session "owned" by the current user
							// if that user is a PU, so he can later edit/delete
							// this new session. Otherwise, if created_by is blank
							// it will be editable only by Godadmins and PU users with
							// course modification permissions
							$newSession->created_by = Yii::app()->user->getIdst();
							$newSession->save();
						}
					}
				}

				$output = '<a class="auto-close"></a>';
			} catch (CException $e) {
				$output = $e->getMessage();
			}

			echo $output;
			Yii::app()->end();
		}

		//if no confirmation, then show copy session dialog
		$this->renderPartial('_copySession', array(
			'idCourse' => $idCourse,
			'idSession' => $idSession
		));
	}

	/**
	 * Autocomplete info for import enrollments dialog (it differs from previous autocomplete,
	 * since we have to exclude our own id_session from research)
	 */
	public function actionSearchAutoCompleteImportFromSession() {

		$inputData = Yii::app()->request->getParam('data', array());
		$output = array();

		if (!empty($inputData)) {
			$idCourse = $inputData['course_id'];
			$idSession = $inputData['id_session'];
			$searchQuery = $inputData['query'];

			$output = array();
			$criteria = new CDbCriteria();
			$criteria->addCondition("course_id = :id_course");
			$criteria->params[':id_course'] = $idCourse;
			$criteria->addCondition("id_session <> :id_session");
			$criteria->params[':id_session'] = $idSession;
			$criteria->addCondition("name LIKE :search");
			$criteria->params[':search'] = '%' . $searchQuery . '%';

			$sessions = LtCourseSession::model()->findAll($criteria);
			if (!empty($sessions)) {
				foreach ($sessions as $session) {
					//avoid sessions without enrollments
					if ($session->countEnrolledUsers() > 0)
						$output[] = $session->name;
				}
			}
		}

		$this->sendJSON(array('options' => $output));
	}

	/**
	 * If classrooms LOs are enabled, load a page to manage them
	 */
	public function actionManageLearningObjects() {

		if (!$this->_courseModel)
			throw new CHttpException("Invalid course provided");

		$cs = Yii::app()->getClientScript();
		/* @var $cs CClientScript */

		// Core Yii
		Yii::app()->getModule('centralrepo')->registerResources(true);
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('jquery.ui');
		$cs->registerCssFile($cs->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
		// Player assets we share
		$cs->registerCssFile ($this->module->getPlayerAssetsUrl().'/css/base.css');
		$cs->registerCssFile($this->module->getPlayerAssetsUrl().'/css/base-responsive.css');
		//->registerCssFile($this->getPlayerAssetsUrl().'/css/player-sprite.css')  // moved to themes
		//->registerCssFile($this->getPlayerAssetsUrl().'/css/i-sprite.css')  // moved to themes
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/yiiform.css');
		// Load Tinymce jQuery integrator
		Yii::app()->tinymce->init();
		//load datepicker plugin
		DatePickerHelper::registerAssets(Yii::app()->getLanguage(), CClientScript::POS_HEAD);

		//others
		$cs->registerScriptFile($this->module->getPlayerAssetsUrl().'/js/jquery.blockUI.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/classroom.css');
		// Dynatree ("borrowed" by lms/player assets)
		$cs->registerScriptFile($this->module->getPlayerAssetsUrl().'/js/jquery.dynatree.js');
		$cs->registerCssFile($this->module->getPlayerAssetsUrl().'/css/ui.dynatree-modified.css');
		//our editing objects tree and launchpad
		$cs->registerScriptFile($this->module->getAssetsUrl().'/js/classroom-los-editing.js');
		$cs->registerScriptFile($this->module->getAssetsUrl().'/js/classroom-los-editing-items.js');
		//$cs->registerScriptFile($this->getAssetsUrl().'/js/classroom-los-launcher.js');

		$adminJs = Yii::app()->assetManager->publish(Yii::getPathOfAlias('admin.js'));
		$cs->registerScriptFile($adminJs . '/script.js');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jwizard.js');

        $dummyStorage = CFileStorage::getDomainStorage(CFileStorage::COLLECTION_LO_VIDEO);
        $whitelistArray = $dummyStorage->getVideoWhitelistArray();

		$params = array(
			'course_id' => $this->_courseModel->idCourse,      // set in BaseController
			'courseModel' => $this->_courseModel,  // set in BaseController
		);

		Yii::app()->event->raise('BeforeCoursePlayerViewRender', new DEvent($this, $params));

		$this->render('manageLearningObjects', array(
			'courseModel' => $this->_courseModel,
            'whitelistArray' => $whitelistArray
		));
	}


	public function actionCheckClassroomAvailability() {

		$rq = Yii::app()->request;
		$output = array();

		//read input parameters
		$idClassroom = $rq->getParam('id_classroom');
		$day = $rq->getParam('day');
		$timeBegin = $rq->getParam('time_begin');
		$timeEnd = $rq->getParam('time_end');
		$breakBegin = $rq->getParam('break_begin');
		$breakEnd = $rq->getParam('break_end');
		$timezone = $rq->getParam('timezone');
		$idSession = $rq->getParam('id_session',false);

		//retrieve existing classrooms dates
		$criteria = new CDbCriteria();
		$criteria->addCondition("id_classroom = :id_classroom");
		$criteria->addCondition("day = :day");
		$criteria->params[':id_classroom'] = $idClassroom;
		$criteria->params[':day'] = $day;
		$criteria->order = "time_begin ASC";
		$list = LtCourseSessionDate::model()->findAll($criteria);
		$classroom_name = LtClassroom::model()->findByPk($idClassroom);


		//do checkings
		try {
			//collect all occupied time slots
			$busy = new TimeTableHandler();
			foreach ($list as $item) {
				if($idSession && $idSession == $item->id_session){
					continue;
				}
				$session = LtCourseSession::model()->findByAttributes(array('id_session'=>$item->id_session));
				if (!empty($item->break_begin) && !empty($item->break_end) && $item->break_begin != '00:00:00' && $item->break_end != '00:00:00') {
					$busy->addPeriod($item->time_begin, $item->break_begin,$session->name,$classroom_name->name,$item->id_session,$item->time_begin,$item->time_end,$session->course->name,$item->timezone);
					$busy->addPeriod($item->break_end, $item->time_end,$session->name,$classroom_name->name,$item->id_session,$item->time_begin,$item->time_end,$session->course->name,$item->timezone);
				} else {
					$busy->addPeriod($item->time_begin, $item->time_end,$session->name,$classroom_name->name,$item->id_session,$item->time_begin,$item->time_end,$session->course->name,$item->timezone);
				}
			}
			//check if the time table is compatible with user input
			if ((!empty($breakBegin)) && (!empty($breakEnd))) {
				$overlapping1 = $busy->getOverlappingPeriods($timeBegin, $breakBegin,$timezone);
				$overlapping2 = $busy->getOverlappingPeriods($breakEnd, $timeEnd,$timezone);
				$overlapping = array_merge($overlapping1, $overlapping2);
			} else {
				$overlapping = $busy->getOverlappingPeriods($timeBegin, $timeEnd,$timezone);
			}
			$output['success'] = true;
			$output['overlapping'] = $overlapping;

		} catch (CException $e) {

			$output['success'] = false;
			$output['message'] = $e->getMessage();
		}

		echo CJSON::encode($output);
	}

}
