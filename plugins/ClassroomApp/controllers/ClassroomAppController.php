<?php

class ClassroomAppController extends Controller {


	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		$res = array();

		// keep it in the following order:

		// http://www.yiiframework.com/doc/guide/1.1/en/topics.auth#access-control-filter
		// Allow following actions to admin only:
		$admin_only_actions = array('settings');
		$res[] = array(
			'allow',
			'roles' => array(Yii::app()->user->level_godadmin),
			'actions' => $admin_only_actions,
		);

		// deny admin only actions to all other users:
		$res[] = array(
			'deny',
			'actions' => $admin_only_actions,
		);

		//allowed actions based on permissions
		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/location/view'),
			'actions' => array(
				'index',
			),
		);

		$res[] = array(
			'allow',
			'roles' => array('/lms/admin/location/mod'),
			'actions' => array(
				'axDelete',
				'axEdit',
				'csvImport',
				'csvSample',
			),
		);

		$res[] = array(
			'deny', // deny all
			'users' => array('*'),
		);

		return $res;
	}





	public function actionIndex() {

		unset(Yii::app()->session['importClassroomsStep']);

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		// Bootcamp menu
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/docebo_bootcamp.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/docebo_bootcamp.css');

		// classroom.css
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');

		$idLocation = intval(Yii::app()->request->getParam('id_location'));
		$locationModel = LtLocation::model()->findByPk($idLocation);
		if (!$locationModel) {
			$this->redirect(Docebo::createAppUrl('lms:ClassroomApp/Location/index'));
		}

		$classroomModel = new LtClassroom();
		$classroomModel->id_location = $locationModel->id_location;

		// filter by name
		if (isset($_POST['LtClassroom'])) {
			$postData = $_POST['LtClassroom'];
			$classroomModel->name = (isset($postData['name'])) ? $postData['name'] : null;
		}


		$this->render('index', array(
			'classroomModel' => $classroomModel,
			'locationModel' => $locationModel
		));
	}



	public function actionAxEdit() {

		$model = null;
		$id = intval(Yii::app()->request->getParam('id'));

		if ($id > 0) {
			$model = LtClassroom::model()->findByPk($id);
		}
		if (!$model) {
			$model = new LtClassroom();

			$idLocation = intval(Yii::app()->request->getParam('id_location'));
			$model->id_location = $idLocation;
		}

		if (isset($_POST['LtClassroom'])) {
			$postData = $_POST['LtClassroom'];

			$model->attributes = $postData;

			if ($model->save()) {

				// close dialog and reload locations
				echo '<a href="" class="auto-close"></a>';
				return;
			}
		}

		$this->renderPartial('_edit', array(
			'model' => $model
		));
	}



	public function actionAxDelete() {

		$id = intval(Yii::app()->request->getParam('id'));
		$model = LtClassroom::model()->findByPk($id);

		if (isset($_POST['LtClassroom']) && !LtClassroom::isUsedInFutureSessionDate($id)) {
			if ($model->delete()) {
				// close dialog and reload locations
				echo '<a href="" class="auto-close"></a>';
				return;
			}
		}

		$this->renderPartial('_delete', array(
			'model' => $model
		));
	}




	protected function gridRenderLinkEditClassroom($row, $index) {
		$html = CHtml::link('<span class="i-sprite is-edit grey"></span>', $this->createUrl('axEdit', array('id' => $row['id_classroom'])), array(
				'class' => 'open-dialog',
				'data-dialog-class' => 'modal-edit-classroom',
				'title' => Yii::t('standard', '_MOD'),
				//'closeOnOverlayClick' => 'false',
				//'closeOnEscape' => 'false',
				'rel' => 'tooltip',
		));
		return $html;
	}




	protected function gridRenderLinkDeleteClassroom($row, $index) {
		//don't show the button if the classroom is used in at least one session date in the future
		if(LtClassroom::isUsedInFutureSessionDate($row['id_classroom'])) return '';

		$html = CHtml::link('<span class="i-sprite is-remove red"></span>', $this->createUrl('axDelete', array('id' => $row['id_classroom'])), array(
				'class' => 'open-dialog',
				'data-dialog-class' => 'modal-delete-classroom',
				'title' => Yii::t('standard', '_DEL'),
				'rel' => 'tooltip',
		));
		return $html;
	}


	public function actionCsvImport() {

		// a location id must be set!
		$idLocation = intval(Yii::app()->request->getParam('id_location'));
		$locationModel = LtLocation::model()->findByPk($idLocation);
		if (!$locationModel) {
			$this->redirect(Docebo::createAppUrl('lms:ClassroomApp/Location/index'));
		}

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();

		// jscrollpane
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.css');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jscrollpane/jquery.jscrollpane.js');

		// classroom.css
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');

		$model = null;
		$step = 'step_import';


		if (isset(Yii::app()->session['importClassroomsStep'])) {
			$tmpStep = Yii::app()->session['importClassroomsStep'];
			if (in_array($tmpStep, array('step_import', 'step_process')))
				$step = $tmpStep;
		} else {
			unset(Yii::app()->session['importClassroomsStep']);
		}

		if ($step == 'step_import') {
			unset(Yii::app()->session['importClassroomsModel']);
			$model = new ClassroomImportForm($step);

			if (isset($_POST['ClassroomImportForm'])) {
				$postData = $_POST['ClassroomImportForm'];
				$model->attributes = $postData;
				$model->file = CUploadedFile::getInstance($model, 'file');
				if ($model->validate()) {
					$step = 'step_process';
					Yii::app()->session['importClassroomsStep'] = $step;
					Yii::app()->session['importClassroomsModel'] = $model->attributes;
				}
			}
		}

		switch ($step) {

			case 'step_process':
				if ($model == null) {
					$model = new ClassroomImportForm();
					$model->attributes = Yii::app()->session['importClassroomsModel'];
					$model->setData(Yii::app()->session['importClassroomsData']);
				}

				if (isset($_POST['import_map']) && isset($_POST['ClassroomImportForm']) && !Yii::app()->request->isAjaxRequest) {
					$model->scenario = 'step_process';
					$model->attributes = $_POST['ClassroomImportForm'];
					$model->setImportMap($_POST['import_map']);
					$model->idLocation = $locationModel->id_location;

					if ($model->save()) {
						unset(Yii::app()->session['importClassroomsModel']);
						unset(Yii::app()->session['importClassroomsStep']);
						$this->redirect($this->createUrl('index', array('id_location' => $locationModel->id_location)));
					} else {
						Yii::app()->session['importClassroomsModel'] = $model;
						$this->redirect($this->createUrl('index'));
					}
				}

				$items = $model->getData();

				if (empty($items)) {
					$this->redirect($this->createUrl('index'));
				}

				Yii::app()->session['importClassroomsData'] = $items;

				$columns = array();
				$i = 1;
				$csvHeaderLine = $items[0];
				foreach ($csvHeaderLine as $key => $value) {
					$column = array(
						'name' => $key,
						'htmlOptions' => array('class' => 'column-' . $key),
					);
					if ($model->firstRowAsHeader) {
						$columnHeader = CHtml::dropDownList('import_map[' . $key . ']', $model->importMap[$i], ClassroomImportForm::getImportMapList()) . '<p>' . $value . '</p>';
						$column['header'] = $columnHeader;
					}
					$columns[] = $column;
					$i++;
				}

				$this->render('csvProcess', array(
					'model' => $model,
					'columns' => $columns,
					'locationModel' => $locationModel
				));
				break;

			case 'step_import':
			default:
				$this->render('csvImport', array(
					'model' => $model,
					'locationModel' => $locationModel
				));
				break;
		}
	}

	public function actionCsvSample() {
		$sep = DIRECTORY_SEPARATOR;
		$filePath = Yii::getPathOfAlias('common').$sep.'extensions'.$sep.'sample-classrooms.csv';
		if (file_exists($filePath)) {
			$fileContent = file_get_contents($filePath);
			Yii::app()->request->sendFile('sample-classrooms.csv', $fileContent, NULL, false);
		}
		Yii::app()->end();
	}

}
