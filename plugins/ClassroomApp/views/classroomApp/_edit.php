<?php
/* @var $this ClassroomAppController */
/* @var $model LtClassroom */
/* @var $form CActiveForm */
?>
<h1><?= (true===$model->getIsNewRecord()) ? Yii::t('classroom', 'New classroom') : Yii::t('classroom', 'Edit classroom') ?></h1>



<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-classroom-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'ajax docebo-form form-horizontal'
    ),
));
echo $form->hiddenField($model, 'id_classroom');
?>

    <div class="control-group">
        <?= $form->labelEx($model, 'name', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= $form->textField($model, 'name', array('class'=>'input-block-level')) ?>
        </div>
    </div>

    <div class="control-group">
        <?= CHtml::label(
            Yii::t('standard', '_DETAILS') . ' ' . CHtml::tag('span', array('class'=>'help-block'), Yii::t('classroom', 'Building, floor, or other useful information')),
            'details',
            array('class'=>'control-label')
        ) ?>
        <div class="controls">
            <?= $form->textArea($model, 'details', array('class'=>'input-block-level')) ?>
        </div>
    </div>

    <div class="control-group">
        <?= $form->labelEx($model, 'seats', array('class'=>'control-label')) ?>
        <div class="controls">
            <?= $form->textField($model, 'seats', array('class'=>'input-block-level')) ?>
        </div>
    </div>

    <div class="control-group">
        <?= CHtml::label(
            Yii::t('classroom', 'Equipment') . ' ' . CHtml::tag('span', array('class'=>'help-block'), Yii::t('classroom', 'Indications on available equipment')),
            'equipment',
            array('class'=>'control-label')
        ) ?>
        <div class="controls">
            <?= $form->textArea($model, 'equipment', array('class'=>'input-block-level')) ?>
        </div>
    </div>

<div class="form-actions">
    <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

    (function() {

        $(document).delegate(".modal-edit-classroom", "dialog2.content-update", function() {
            var e = $(this);

            var autoclose = e.find("a.auto-close");

            if (autoclose.length > 0) {
                e.find('.modal-body').dialog2("close");

                // reload classrooms grid
                if ($("#classroom-management-grid").length)
                    $("#classroom-management-grid").yiiGridView('update');
                // reload locations grid

                if ($("#location-management-grid").length)
                    $("#location-management-grid").yiiGridView('update');

                var href = autoclose.attr('href');
                if (href) {
                    window.location.href = href;
                }
            }
            else {

            }
        });
    })();

</script>

