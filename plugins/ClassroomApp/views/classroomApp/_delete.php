<?php
/* @var $this LocationController */
/* @var $model LtClassroom */
/* @var $form CActiveForm */
?>
<h1><?= Yii::t('standard', '_AREYOUSURE') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'delete-classroom-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'ajax'
    ),
));
echo $form->hiddenField($model, 'id_classroom');
?>

<?= Yii::t('player', "Are you sure you want to delete:") . " <strong>".$model->name."</strong> ?" ?>


<div class="form-actions">
    <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

    $(document).delegate(".modal-delete-classroom", "dialog2.content-update", function() {
        var e = $(this);

        var autoclose = e.find("a.auto-close");

        if (autoclose.length > 0) {
            e.find('.modal-body').dialog2("close");

            // reload locations grid
            $("#classroom-management-grid").yiiGridView('update');

            var href = autoclose.attr('href');
            if (href) {
                window.location.href = href;
            }
        }
    });

</script>

