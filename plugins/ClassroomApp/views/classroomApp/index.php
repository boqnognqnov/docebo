<?php
/* @var $this ClassroomAppController */
/* @var $locationModel LtLocation */
/* @var $classroomModel LtClassroom */
/* @var $gridColumns array */
/* @var $form CActiveForm */

$this->breadcrumbs[Yii::t('classroom', 'Locations Manager')] = Docebo::createAppUrl('lms:ClassroomApp/Location/index');
$this->breadcrumbs[] = Yii::t('classroom', 'Classrooms');

// TODO: add bootcamp menu helper block

?>

<h3 class="title-bold">
	<a class="docebo-back" href="<?= Docebo::createAppUrl('lms:ClassroomApp/Location/index') ?>">
		<span><?= Yii::t('standard', '_BACK') ?></span>
	</a>
	<?= Yii::t('classroom', 'Classrooms') ?>
</h3>
<br>

<?php if (Yii::app()->user->checkAccess('/lms/admin/location/mod')): ?>
<ul id="docebo-bootcamp-classrooms" class="docebo-bootcamp clearfix">
    <li>
        <a id="add-new-classroom"
           href="<?= $this->createUrl('axEdit', array('id_location'=>$locationModel->id_location)) ?>"
           class="open-dialog"
           data-dialog-class="modal-edit-classroom"
           data-helper-title="<?= Yii::t('classroom', 'New classroom') ?>"
           data-helper-text="<?= Yii::t('helper', 'New classroom - lt') ?>">
            <span class="classroom-sprite new-classroom"></span>
            <span class="classroom-sprite new-classroom white"></span>
            <h4><?= Yii::t('classroom', 'New classroom') ?></h4>
        </a>
    </li>
    <li>
        <a id="import-csv-locations" href="<?= $this->createUrl('csvImport', array('id_location'=>$locationModel->id_location)) ?>"
           data-helper-title="<?= Yii::t('classroom', 'Import from CSV') ?>"
           data-helper-text="<?= Yii::t('helper', 'Import classroom from CSV - lt') ?>">
            <span class="i-sprite is-putin large"></span>
            <span class="i-sprite is-putin large white"></span>
            <h4><?= Yii::t('classroom', 'Import from CSV') ?></h4>
        </a>
    </li>

    <li class="helper">
        <a href="#">
            <span class="i-sprite is-circle-quest large"></span>
            <h4 class="helper-title"></h4>
            <p class="helper-text"></p>
        </a>
    </li>
</ul>
<?php endif; ?>

<h4><?= Yii::t('classroom', 'Classrooms for {location} - {address}', array(
        '{location}' => $locationModel->name,
        '{address}' => $locationModel->renderAddress()
    )) ?></h4>
<br>

<!--search box-->
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'classroom-grid-search-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'docebo-form grid-search-form'
    ),
)); ?>
<div class="control-container odd">
    <div class="control-group">
        <div class="input-wrapper">
            <input id="classroom-grid-search" autocomplete="off" type="text" name="<?= CHtml::activeName($classroomModel, 'name') ?>" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" data-autocomplete="true"/>
            <span class="i-sprite is-search"></span>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>


<!--classrooms grid-->
<div id="grid-wrapper" class="">
	<?php
	
		$_columns = array(
			array(
				'type' => 'raw',
				'value' => '$data["name"]',
				'header' => Yii::t('classroom', 'Classroom name'),
				'name' => 'name',
				'sortable' => true
			),
			array(
				'type' => 'raw',
				'value' => '$data["seats"]',
				'header' => Yii::t('standard', 'Available seats'),
				'name' => 'seats',
				'sortable' => true
			),
			array(
				'type' => 'raw',
				'value' => 'DoceboHtml::readmore($data["details"], 250)',
				'header' => Yii::t('standard', '_DETAILS'),
				'name' => 'details',
				'sortable' => true
			)
		);
		if (Yii::app()->user->checkAccess('/lms/admin/location/mod')) {
			$_columns[] = array(
				'type' => 'raw',
				'value' => array($this, 'gridRenderLinkEditClassroom'),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				)
			);
			$_columns[] = array(
				'type' => 'raw',
				'value' => array($this, 'gridRenderLinkDeleteClassroom'),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				)
			);
		}
		
		$this->widget('DoceboCGridView', array(
			'id' => 'classroom-management-grid',
			'htmlOptions' => array('class' => 'grid-view'),
			'dataProvider' => $classroomModel->dataProvider(),
			'columns' => $_columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
			),
			'beforeAjaxUpdate' => 'function(id, options) {
					resetSearchPlaceholder();
							options.type = "POST";
							options.data = $("#classroom-grid-search-form").serialize()
					}',
			'afterAjaxUpdate' => 'function(id, data){
							$(\'a[rel="tooltip"]\').tooltip();
							$(document).controls();
					}'
		));
	?>
</div>


<script type="text/javascript">

    function resetSearchPlaceholder() {
        var $search = $('#classroom-grid-search');
        if($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
            $('#classroom-grid-search').val('');
    }

    $(function(){
        var $search = $('#classroom-grid-search');

        var filterClassroomsGrid = function() {
            var filter = $.trim( $search.val() );
            $("#classroom-management-grid").yiiGridView('update');
        };

        $('#docebo-bootcamp-classrooms').doceboBootcamp();

        // on enter key press
        $search.bind('keypress', function(e) {
            var code = e.keyCode || e.which;
            if(code == 13) {
                // enter was pressed, then update map
                filterClassroomsGrid();
                e.preventDefault();
                return false;
            }
        })
    });

</script>