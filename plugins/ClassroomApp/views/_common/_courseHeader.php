<?php
/* @var $this CourseHeader */
?>
<div id="player-courseheader-container" class="row-fluid">
	<div class="span12">
		<div class="course-header-logo"><?=CHtml::image($courseModel->courseLogoUrl, '');?></div>
		<div class="course-header-title">
			<h2><span class="lang-sprite lang_<?=$courseModel->lang_code?>  lang-sprite-title" ></span><?=$courseModel->name ?></h2>
		</div>
		<div class="course-header-description">
			<?= $courseModel->description ?>
		</div>
	</div>
</div>
<div class="clearfix"></div>