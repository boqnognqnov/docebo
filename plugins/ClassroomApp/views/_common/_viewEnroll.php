<?php
if (!isset($canEnroll)) {
	$course_associated = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $data['idCourse'])) ? true : false;
	$user_associated = CoreUserPU::isAssignedToPU($data['idst'], 'user');
	//check admin permissions
	/*$canEnroll = (Yii::app()->user->isGodAdmin ||
		(Yii::app()->user->isAdmin && Yii::app()->user->checkAccess('enrollment/create') && $course_associated && $user_associated)
	);*/
	$canEnroll = false;
	if (Yii::app()->user->isGodAdmin) {
		$canEnroll = true;
	} else if (Yii::app()->user->isPu && $course_associated && $user_associated) {
		if (!isset($sessionModel)) { $sessionModel = LtCourseSession::model()->findByPk($data['id_session']); }
		if (!isset($pUserRights)) { $pUserRights = Yii::app()->user->checkPURights($data['idCourse']); }
		$isPuWithCourseEnrollPerm = $pUserRights->all && Yii::app()->user->checkAccess('enrollment/view');
		$isPuWithSessionAddPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add");
		$isPuWithSessionEditPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod");
		$isPuWithSessionAssignPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/assign");
		$isSessionOwnedByPU = ($sessionModel->created_by == Yii::app()->user->getIdst());
		//check PU permissions
		if ($isPuWithCourseEnrollPerm) {
			if ($isSessionOwnedByPU) {
				$canEnroll = ($isPuWithSessionAddPerm || $isPuWithSessionEditPerm);
			} else {
				$canEnroll = ($isPuWithSessionEditPerm);
			}
		}
	}
}

$canUnenroll = true;
$canUnenrollMessage = '';

if (PluginManager::isPluginActive('CurriculaApp')) {
	$lpAssigned = LearningCoursepathCourses::isCourseAssignedToLearningPaths($data['idCourse']);
	if (!empty($lpAssigned)) {
		$lpEnrollments = LearningCoursepath::checkEnrollment($data['idst'], $data['idCourse'], true);
		if (!empty($lpEnrollments)) {
			$canUnenroll = false;
			$canUnenrollMessage = '<div class="cannot-unenroll-tooltip">';
			$canUnenrollMessage .= '<span>' . Yii::t('myactivities', 'Learning plans') . ':</span><br />';
			$canUnenrollMessage .= '<ul>';
			foreach ($lpEnrollments as $lpId => $lpInfo) {
				$canUnenrollMessage .= '<li>- ' . $lpInfo['path_name'] . '</li>';
			}
			$canUnenrollMessage .= '</ul>';
			$canUnenrollMessage .= '</div>';
		}
	}
}
?>
<div class="item <?php echo (($index % 2) ? 'odd' : 'even'); ?>">
	<?php if ($canEnroll): ?>
	<div class="checkbox-column">
		<?php
			echo '<input id="group-member-management-list-checkboxes_' . $data['idst'] . '" '
				.'type="checkbox" name="group-member-management-list-checkboxes[]" '
				.'value="' . $data['idst'] . '" ' . ($this->inSessionList('enroll-selection', $data['idst']) ? 'checked="checked"': '')
				.'>';
		?>
	</div>
	<?php endif; ?>
	<div class="course-enrollment-username"><?php
		// Get system setting for names ordering
		$show_first_name_first = Settings::get('show_first_name_first', 'off');
		$names = array();
		
		// Prepare names depending to system set names order
		if ($show_first_name_first == 'off') {
			if (!empty($data["lastname"]))
				$names[] = $data["lastname"];
			if (!empty($data["firstname"]))
				$names[] = $data["firstname"];
		} else {
			if (!empty($data["firstname"]))
				$names[] = $data["firstname"];
			if (!empty($data["lastname"]))
				$names[] = $data["lastname"];
		}

		// Show names
		echo join(' ', $names);

		// Show Username
		echo '&nbsp;('.Yii::app()->user->getRelativeUsername($data['userid']).')';
	?></div>
	<div class="course-level"><?php echo Yii::t('levels', '_LEVEL_' . $data['level']);?></div>
	<div class="course-status"><span class="courseEnrollmentStatus_<?= (int)$data['status']?>"></span></div>

	<?php if ($canEnroll): ?>
	<div class="edit-button">
		<a class="session-enroll-icon enroll-edit popup-handler"
			 data-modal-title="<?= Yii::t('course_management', '_ENROLLMENT_EDIT') ?>"
			 data-modal-class="edit-enrollment"
			 data-dialog-class="edit-enrollment session-edit-enrollment-dialog"
			 data-after-send="sessionEditEnrollmentAfterSubmit(data);"
			 title="<?= Yii::t('course_management', '_ENROLLMENT_EDIT') ?>"
			 href="<?=
					Docebo::createAdminUrl('ClassroomApp/enrollment/editUser', array(
						'id_user' => $data['idst'],
						'id_session' => $data['id_session']
					))
				?>"></a>
	</div>

	<div class="delete-button" id="delete-button-<?= $data['idst'] ?>">
		<?php if ($canUnenroll): ?>
		<a class="session-enroll-icon enroll-remove popup-handler"
			 data-modal-title="<?= Yii::t('standard', 'Uneroll') ?>"
			 data-modal-class="remove-enrollment delete-node"
			 data-dialog-class="session-remove-enrollment-dialog"
			 data-after-send="sessionEditEnrollmentAfterSubmit(data);"
			 title="<?= Yii::t('standard', 'Uneroll') ?>"
			 href="<?=
					Docebo::createAdminUrl('ClassroomApp/enrollment/removeUser', array(
						'id_user' => $data['idst'],
						'id_session' => $data['id_session'],
						'idCourse'=>$data['idCourse'],
					))
				?>"></a>
		<?php else: ?>
			<?php if (!empty($canUnenrollMessage)) {
				echo CHtml::link('', 'javascript:return false;', array(
					'rel' => 'tooltip',
					'data-html' => 'true',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'title' => $canUnenrollMessage,
					'class' => 'cannot-unenroll'
				));
			} ?>
		<?php endif; ?>
	</div>

	<?php elseif(!$canEnroll && (isset($isPuWithSessionAssignPerm) && $isPuWithSessionAssignPerm  && isset($data['status']) && ($data['status']==LearningCourseuser::$COURSE_USER_SUBSCRIBED))): ?>
	<div class="delete-button" id="delete-button-<?= $data['idst'] ?>">
		<?php if ($canUnenroll): ?>
			<a class="session-enroll-icon enroll-remove popup-handler"
			   data-modal-title="<?= Yii::t('standard', 'Uneroll') ?>"
			   data-modal-class="remove-enrollment delete-node"
			   data-dialog-class="session-remove-enrollment-dialog"
			   data-after-send="sessionEditEnrollmentAfterSubmit(data);"
			   title="<?= Yii::t('standard', 'Uneroll') ?>"
			   href="<?=
			   Docebo::createAdminUrl('ClassroomApp/enrollment/removeUser', array(
				   'id_user' => $data['idst'],
				   'id_session' => $data['id_session'],
				   'idCourse'=>$data['idCourse'],
			   ))
			   ?>"></a>
		<?php else: ?>
			<?php if (!empty($canUnenrollMessage)) {
				echo CHtml::link('', 'javascript:return false;', array(
					'rel' => 'tooltip',
					'data-html' => 'true',
					'data-toggle' => 'tooltip',
					'data-placement' => 'top',
					'title' => $canUnenrollMessage,
					'class' => 'cannot-unenroll'
				));
			} ?>
		<?php endif; ?>
	</div>
	<?php endif; ?>
</div>
