<div class="session-details-container clearfix">
	<table>
		<tbody>
		<tr>
			<td class="title">
				<!-- title display -->
				<h3 class="title-bold"><?= $title ?></h3>
			</td>

			<td class="dates">
				<!-- session details -->
				<div class="classroom-dates-container">
					<ul>
						<li class="session-name"><?= $sessionModel->name; ?><i>&nbsp;</i></li>
						<?php
						$dates = $sessionModel->getDates();
						if (!empty($dates)) {
							echo '<li class="session-date">';
							echo $dates[0]->location->name . ' - ' . $dates[0]->location->address . ' - ' . $dates[0]->location->country->name_country;
							echo '<i>&nbsp;</i></li>';
						}
						?>
						<li class="session-from_to">
							<?php
							/*
							$minDate = false;
							$maxDate = false;
							if (!empty($dates)) {
								foreach ($dates as $date) {
									if ($minDate === false || $date->getDateBegin() < $minDate) {
										$minDate = $date->getDateBegin();
									}
									if ($maxDate === false || $date->getDateEnd() > $maxDate) {
										$maxDate = $date->getDateEnd();
									}
								}
							}
							*/
							$minDate = (!empty($sessionModel->date_begin) ? $sessionModel->date_begin : false);
							$maxDate = (!empty($sessionModel->date_end) ? $sessionModel->date_end : false);
							if ($minDate != false && $maxDate != false) {
								echo Yii::t('standard', '_FROM') . ' ' . $minDate;//Yii::app()->localtime->toLocalDateTime($minDate);
								echo '&nbsp;';
								echo Yii::t('standard', '_TO') . ' ' . $maxDate;//Yii::app()->localtime->toLocalDateTime($maxDate);
							}
							?>
							<i>&nbsp;</i>
						</li>
					</ul>
				</div>
			</td>
		</tr>
		</tbody>
	</table>
</div>