<?php
/* @var $courseModel LearningCourse */
/* @var $sessionModel LtCourseSession */

$backUrl = Docebo::createLmsUrl('player/training/session', array('course_id' => $courseModel->getPrimaryKey()));

$this->breadcrumbs[] = Docebo::ellipsis($courseModel->name, 60, '...');
$this->breadcrumbs[Yii::t('classroom', 'Sessions and Enrollments')] = $backUrl;
$this->breadcrumbs[] = Yii::t('classroom', 'Attendance sheet and evaluation');

$this->renderPartial('/_common/_courseHeader', array('courseModel' => $courseModel));
?>

<h3 class="main-actions title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), $backUrl); ?>
	<span><?= Yii::t('classroom', 'Attendance sheet and evaluation for').' '.$sessionModel->name?></span>
</h3>


<!--search box-->
<form id="evaluations-grid-search-form">
<div class="filters-wrapper">
    <table class="filters">
        <tbody>
        <tr>
            <td class="table-padding">
                <table class="table-border">
                    <tr>
											<td class="group">
												<?php echo ucfirst(Yii::t('standard', '_SHOW')); ?>
											</td>
                    	<td class="group">
												<?php
													echo CHtml::radioButton('search-filter-status', true, array('id' => 'search-filter-all', 'value' => 'all'));
													echo CHtml::label(Yii::t('standard', '_ALL'), 'search-filter-all');
												?>
                  		</td>
											<td class="group">
												<?php
													echo CHtml::radioButton('search-filter-status', false, array('id' => 'search-filter-already', 'value' => 'already'));
													echo CHtml::label(Yii::t('classroom', 'Already evaluated'), 'search-filter-already');
												?>
											</td>
                   		<td class="group">
												<?php
													echo CHtml::radioButton('search-filter-status', false, array('id' => 'search-filter-waiting', 'value' => 'waiting'));
													echo CHtml::label(Yii::t('classroom', 'Waiting for evaluation'), 'search-filter-waiting');
												?>
                    	</td>
                        <td class="group" style="width: 50%;">
                            <table style="float: right;">
                                <tr>
                                    <td>
                                        <div class="input-wrapper">
                                            <input id="advanced-search-session" type="text" name="searchText" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" />
                                            <span class="search-icon"></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</form>

<?php
$this->renderPartial('/_common/_sessionInfo', array(
	'title' => Yii::t('classroom', 'Session') . ': '.$sessionModel->name,
	'sessionModel' => $sessionModel
));
?>

<!--evaluations grid-->
<div id="grid-wrapper" class="">
	<?php

		$_columnList = array();

		$_columnList[] = array(
			'name' => 'userid',
			'type' => 'raw',
			'value' => '$data["userid"]',
			'header' => Yii::t('standard', '_USERNAME')
		);
	
		$_columnList[] = array(
			'name' => 'name',
			'type' => 'raw',
			'value' =>  function ($data, $index)  {
				// Get system setting for names ordering
				$show_first_name_first = Settings::get('show_first_name_first', 'off');
				$names = array();

				// Prepare names depending to system set names order
				if ($show_first_name_first == 'off') {
					if (!empty($data["lastname"]))
						$names[] = $data["lastname"];
					if (!empty($data["firstname"]))
						$names[] = $data["firstname"];
				} else {
					if (!empty($data["firstname"]))
						$names[] = $data["firstname"];
					if (!empty($data["lastname"]))
						$names[] = $data["lastname"];
				}

				// Return names
				echo join(' ', $names);
			},
			'header' => Yii::t('standard', '_NAME')
		);

		// Lets trigger event here, there can be a plugin that want to add columns in the grid
		$event = new DEvent($this, array('columnList' => &$_columnList, 'session'=>$sessionModel));
		Yii::app()->event->raise('beforeRenderAttendaceSheetEvaluationGrid', $event);

		$_columnList[] = array(
			'name' => 'date_begin',
			'type' => 'raw',
			'value' => function ($data, $index) use (&$sessionModel) {
						/* @var $sessionModel LtCourseSession */
						$output = '';
						switch ($sessionModel->evaluation_type) {
							case LtCourseSession::EVALUATION_TYPE_SCORE: {
								$output .= LtCourseSession::model()->renderEvaluationScore(
									$data["id_user"],
									$sessionModel->course_id,
									$sessionModel->getPrimaryKey(),
									$data["evaluation_date"],
									$data["evaluation_status"],
									$data["evaluation_score"],
									$sessionModel->score_base);
							} break;
							case LtCourseSession::EVALUATION_TYPE_ONLINE: {
//								$allowedObjectTypes = array(
//									//LearningOrganization::OBJECT_TYPE_POLL,
//									LearningOrganization::OBJECT_TYPE_TEST
//								);
//								$endObject = $sessionModel->course->getEndMilestoneLearningObject();
//								if ($endObject && in_array($endObject->objectType, $allowedObjectTypes)) {
//									$track = LearningCommontrack::model()->findByAttributes(array(
//										'idUser' => $data['id_user'],
//										'idReference' => $endObject->idOrg
//									));
//									if ($track) {
//										//$output .= $track->score.'/'.$track->score_max;
//										$userSessionModel = LtCourseuserSession::model()->findByPk(array('id_user' => $data['id_user'], 'id_session' => $sessionModel->getPrimaryKey()));
//										if ($userSessionModel) {
//											$score = $track->score;
//											$scoreMax = $track->score_max;
//											$status = $userSessionModel->status;
//											//display user evaluation
//											$html = '';
//											$html .= '<div id="evaluation-container-'.$data['id_user'].'" class="evaluation-actions-envelope">';
//											$score = trim($score);
//											if ($score !== false && $score !== '') {
//												$html .= '<span class="evaluation-score '.(!$status || $status <= 0 ? 'bad' : 'good').'">'.$score.'</span>/'.$scoreMax;
//												$html .= '&nbsp;';
//											}
//											if ($status > 0) {
//												$html .= '<span class="classroom-sprite approved-small popover-trigger"></span>';
//											} else {
//												$html .= '<span class="classroom-sprite unapproved-small popover-trigger"></span>';
//											}
//											$html .= '</div>';
//											$output .= $html;
//										} else {
//											//TODO: what to do when needed data has not been found ?
//										}
//
//									} else {
//										$output .= '-';
//									}
//								} else {
									$output .= '-';
//								}
							} break;
						}
						return $output;
			},
			'header' => Yii::t('classroom', 'Evaluation')
		);

		$_columnList[] = array(
			'name' => 'date_end',
			'type' => 'raw',
			'value' => function($data, $index) use (&$sessionModel) {
						/* @var $sessionModel LtCourseSession */
						$output = '';
						switch ($sessionModel->evaluation_type) {
							case LtCourseSession::EVALUATION_TYPE_SCORE: {
								$output .= (empty($data["evaluation_date"]) ? "-" : $data["evaluation_date"]);
							} break;
							case LtCourseSession::EVALUATION_TYPE_ONLINE: {
								$allowedObjectTypes = array(
									//LearningOrganization::OBJECT_TYPE_POLL,
									LearningOrganization::OBJECT_TYPE_TEST
								);
								$endObject = $sessionModel->course->getEndMilestoneLearningObject();
								if ($endObject && in_array($endObject->objectType, $allowedObjectTypes)) {
									$track = LearningCommontrack::model()->findByAttributes(array(
										'idUser' => $data['id_user'],
										'idReference' => $endObject->idOrg
									));
									if ($track) {
										//TODO: render this in a more adequate way, this is just temporary
										$output .= $track->dateAttempt;
									} else {
										$output .= '-';
									}
								} else {
									$output .= '-';
								}
							} break;
						}
						return $output;
			},
			'header' => Yii::t('classroom', 'Evaluated on')
		);

		//now print a column for every session date
		$cmodule = Yii::app()->getModule('ClassroomApp');
		foreach ($sessionModel->getDates() as $date) {
			$_columnList[] = array(
				'type' => 'raw',
				'value' => '(in_array("'.$date['day'].'", (is_array($data["attendance"]) ? $data["attendance"] : array())) '
					.' ? "<span class=\"classroom-sprite attended attendance-trigger day_'.$date['day'].'\" data-id_user=\"".$data["id_user"]."\" data-attended=\"1\" data-day=\"'.$date['day'].'\"></span>" '
					.' : "<span class=\"classroom-sprite not-attended attendance-trigger day_'.$date['day'].'\" data-id_user=\"".$data["id_user"]."\"  data-attended=\"0\" data-day=\"'.$date['day'].'\"></span>");',
				'headerHtmlOptions' => array('class' => 'center-aligned'),
				'header' => '<span class="session-date-time-subtitle">('.Yii::app()->localtime->toLocalDateTime($date->day . ' ' . $date->time_begin, 'short', 'short', null, $date->timezone)
					.' - '.Yii::app()->localtime->toLocalDateTime($date->day . ' ' . $date->time_end, 'short', 'short', null, $date->timezone).')</span>'
					.'<br />'
					.CHtml::link(
						Yii::t('classroom', 'Mark all as present'),
						'#',
						array('class' => 'attendance-multitrigger', 'data-day' => $date->day)
					)
			);
		}
		Yii::app()->event->raise("DeductionDropdown", new DEvent($this, array('sessionModel' => $sessionModel, 'columnList' => &$_columnList)));

		$this->widget('DoceboCGridView', array(
			'id' => 'evaluations-grid',
			'htmlOptions' => array('class' => 'grid-view evaluations-grid'),
			'dataProvider' => LtCourseSession::model()->dataProviderEvaluation($sessionModel->getPrimaryKey()),
			'columns' => $_columnList,
            'template' => '{items}{summary}{pager}',
            'summaryText' => Yii::t('standard', '_TOTAL'),
            'pager' => array(
                'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
            ),
            'ajaxUpdate' => 'sessions-management-grid-all-items',
            'beforeAjaxUpdate' => 'function(id, options) { '
							.' options.type = "POST"; '
							.'}',
            'afterAjaxUpdate' => 'function(id, data){ '
							.' $("#evaluations-grid").controls(); /*this to refresh dialog2 openers*/ '
							.' setEvaluationActions(); /* popovers */ '
                            .' setAttendanceActions(); /* mark attendances */ '
							.'}'
    )); ?>
</div>
<br />

<script type="text/javascript">


//set popover behavior for some callbacks, necessary to show dialog windows
var original = $.fn.popover.Constructor.prototype.show;
$.fn.popover.Constructor.prototype.show = function () {
	original.call(this);
	if (this.options.callback) {
		this.options.callback();
	}
}


//evaluation actions initializations
var setEvaluationActions = function() {
	$('.evaluation-actions-envelope').each(function(index, value) {
		var el = $(value), idUser = el.data('id_user'), popoverId = 'evaluation-popover-'+idUser;
		//el = el.add(el.find('span'));
		el.each(function(index, element){
			$(element).popover({
				html: true,
				placement: 'top',
				trigger: 'click',
				content: '<div id="'+popoverId+'" class="evaluation-popover-icons">'
						+'<table><tbody><tr>'
						+'<td>'
						+'<a class="classroom-sprite details open-dialog" data-dialog-class="edit-evaluation-dialog" href="<?php
								echo Docebo::createLmsUrl('ClassroomApp/instructor/axShowUserEvaluation', array('course_id' => $idCourse, 'id_session' => $idSession));
							?>&id_user='+idUser+'"></a>'
						+'</td><td>'
						+'<a class="i-sprite is-edit open-dialog" data-dialog-class="edit-evaluation-dialog" href="<?php
								echo Docebo::createLmsUrl('ClassroomApp/instructor/axEditUserEvaluation', array('course_id' => $idCourse, 'id_session' => $idSession));
							?>&id_user='+idUser+'"></span>'
						+'</td><td>'
						+'<a class="i-sprite is-remove red open-dialog" data-dialog-class="clear-evaluation-dialog" href="<?php
								echo Docebo::createLmsUrl('ClassroomApp/instructor/axClearUserEvaluation', array('course_id' => $idCourse, 'id_session' => $idSession));
							?>&id_user='+idUser+'"></a>'
						+'</td>'
						+'</tbody></table>'
						+'</div>',
				callback: function() {
					$('#'+popoverId).controls();
				}
			});
		});
	});
};

var setAttendanceActions = function() {
    //attendance updates
    $('.attendance-trigger').bind("click", $.proxy(function(e) {

        var t = $(e.target);

        //ask the server to make the attendance status chaging
        $.ajax({
            url: <?= CJSON::encode(Docebo::createLmsUrl('ClassroomApp/instructor/axSetAttendance', array('course_id' => $idCourse, 'id_session' => $idSession))) ?>,
            type: 'post',
            dataType: 'JSON',
            data: {
                day: t.data('day'),
                id_user: t.data('id_user'),
                old_status: (t.data('attended') > 0 ? 1 : 0),
                new_status: (t.data('attended') > 0 ? 0 : 1)
            },
            beforeSend: function () {
                //...
            }
        }).done(function (res) {
                if (res.success) {
                    //after doing operation, update icon appearance according to new attendance status
                    t.removeClass('attended not-attended');
                    t.addClass(res.new_status > 0 ? 'attended' : 'not-attended');
                    t.data('attended', res.new_status);
                } else {
                    // TODO: show error message (res.message)
                }
            }).always(function () {

            }).fail(function (jqXHR, textStatus, errorThrown) {

            });

    }, {}));


    //multiple presence updates, in columns headers
    $('.attendance-multitrigger').bind("click", $.proxy(function(e) {

        e.preventDefault();

        //this will read users ids and statuses.
        // NOTE: only displayed rows will be updated
        var t = $(e.target), collector = {
            users: [],
            reader: function(index, value) {
                var id_user = $(value).data('id_user');
                this.users.push(id_user);
            }
        };

        //read all column users id
        $('.day_'+t.data('day')).each($.proxy(collector.reader, collector));

        //then ask the server to update users attendance status with a single ajax request
        if (collector.users.length > 0) {

            $.ajax({
                url: <?= CJSON::encode(Docebo::createLmsUrl('ClassroomApp/instructor/axSetUsersAsPresent', array('course_id' => $idCourse, 'id_session' => $idSession))) ?>,
                type: 'post',
                dataType: 'JSON',
                data: {
                    day: t.data('day'),
                    users: collector.users.join(',')
                },
                beforeSend: function () {
                    //...
                }
            }).done(function (res) {
                    if (res.success) {
                        //update the grid
                        updateGrid();
                    } else {
                        // TODO: show error message (res.message)
                    }
                }).always(function () {

                }).fail(function (jqXHR, textStatus, errorThrown) {

                });
        }
    }, {}));
}

//table filters and refresh
var updateGrid = function() {
	$.fn.yiiGridView.update('evaluations-grid', {
		data: $("#evaluations-grid-search-form").serialize()
	});
};


//general initialization
$(function(){

	//set some events
	$('#search-filter-all').bind("change", function(e) { updateGrid(); });
	$('#search-filter-already').bind("change", function(e) { updateGrid(); });
	$('#search-filter-waiting').bind("change", function(e) { updateGrid(); });
	$('#evaluations-grid-search-form').bind('submit', function(e) {
		e.preventDefault();
		updateGrid();
	});

	setEvaluationActions();
	setAttendanceActions();

	//set some stylers for inputs
	$('#search-filter-all').styler();
	$('#search-filter-already').styler();
	$('#search-filter-waiting').styler();
});
</script>