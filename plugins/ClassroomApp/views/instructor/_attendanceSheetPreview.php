<?php

//retrieve date times
$dateTimes = array();
if (!empty($dateModel->break_begin) && !empty($dateModel->break_end)) {
	$dateTimes[] = array($dateModel->time_begin, $dateModel->break_begin);
	$dateTimes[] = array($dateModel->break_end, $dateModel->time_end);
} else {
	$dateTimes[] = array($dateModel->time_begin, $dateModel->time_end);
}

//$columnStyle = (count($listOfFieldsTitles) > 3 ? 'double-signature' : 'single-signature');
Yii::app()->event->raise('SetStylesForTable', new DEvent($this, array('listOfFieldsTitles' => $listOfFieldsTitles)));
?>


<div class="attendance-sheet-header">
	<table>
		<tbody>
			<tr>
				<td class="logo">
					<img src="<?= Yii::app()->theme->getLogoUrl() ?>" />
				</td>
				<td class="title">
					<?= Yii::t('classroom', 'Attendance sheet'); ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="attendance-sheet-titles">
	<div class="course-title"><?= $courseModel->name ?></div>
	<div class="session-title"><?= $sessionModel->name ?></div>
</div>

<div class="attendance-sheet-infobox">
	<table class="misc-info">
		<tbody>
			<tr>
				<td class="title">
					<?= Yii::t('classroom', 'Location') ?>
				</td>

				<td class="title">
					<?= Yii::t('standard', 'Teacher(s)') ?>
				</td>

				<td rowspan="2" class="time-info">
					<div>
						<span class="classroom-sprite clock-large"></span>
					</div>
					<div class="time-text">
						<div>
							<?php
								$timesToShow = array();
								$time = '';
								$cmodule = Yii::app()->getModule('ClassroomApp');
								foreach ($dateTimes as $dateTime)
								{
									if($dateTime[1] == '00:00:00')
										$time = $dateTime[0];
									elseif($dateTime[0] == '00:00:00')
										$timesToShow[] = $cmodule->formatTime($time).' - '.$cmodule->formatTime($dateTime[1]);
									else
										$timesToShow[] = $cmodule->formatTime($dateTime[0]).' - '.$cmodule->formatTime($dateTime[1]);
								}
								echo implode('</div><div>', $timesToShow);
							?>
						</div>
					</div>
					<div>
						<span><?= Yii::t('course', '_PARTIAL_TIME') ?></span>
					</div>
				</td>

				<td rowspan="2" class="time-info">
					<div>
						<span class="classroom-sprite calendar-large"></span>
					</div>
					<div class="time-text">
						<?php
							echo Yii::app()->localtime->toLocalDate($dateModel->day);
						?>
					</div>
					<div>
						<span><?= Yii::t('classroom', 'Session date(s)') ?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="content">
					<!-- location details -->
					<?php
						echo '<ul>';
						echo '<li>'.$dateModel->location->name.'</li>';
						echo '<li>'.$dateModel->classroom->name.'</li>';
						echo '<li>'.$dateModel->location->address.'</li>';
						echo '<li>'.$dateModel->location->country->name_country.'</li>';
						echo '</ul>';
					?>
				</td>
				<td class="instructors-container">
					<!-- instructors list -->
					<?php
						/*
						$enrollSessionModel = LtCourseuserSession::model()->findByPk(array(
							'id_user' => Yii::app()->user->id,
							'id_session' => $idSession
						));
						*/
						$instructors = LtCourseuserSession::model()->findSessionInstructors($idSession);//$enrollSessionModel->findInstructors();
						if (!empty($instructors)) {

							echo '<table class="instructors-list">';
							echo '<tbody>';
							foreach ($instructors as $instructor) {
								echo '<tr>';
								echo '<td class="instructor-name">'.$instructor->getFullName().'</td>';
								echo '<td class="signature-space"><div class="underline"></div></td>';
								echo '</tr>';
							}
							echo '</tbody>';
							echo '</table>';

						} else {

							echo '<div class="no-instructors">';
							echo '('.Yii::t('classroom', 'No instructors').')';
							echo '</div>';

						}
					?>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="attendance-sheet-userlist">
	<table class="attendance-sheet-users-table">
		<thead>
		<tr>
			<?php
			$br = 0;
			foreach ($listOfFieldsTitles as $field) { ?>
				<th <?= ($br == 0 ? 'colspan="2"' : '') ?>>
					<?php
						if (is_scalar($field) === true) {
							switch ($field) {
								case 'fullname':
									echo Yii::t('classroom', 'Name and Surname');
									break;
								case 'username':
									echo Yii::t('standard', '_USERNAME');
									break;
								case 'email':
									echo Yii::t('standard', '_EMAIL');
									break;
								case 'language':
									echo Yii::t('standard', '_LANGUAGE');
									break;
								case 'signature1':
									echo Yii::t('standard', '_SIGNATURE') . ' (' . $timesToShow[0] . ')';
									break;
								case 'signature2':
									echo Yii::t('standard', '_SIGNATURE') . ' (' . $timesToShow[1] . ')';
									break;
								default:
									break;
							}
						} else if ($field instanceof CoreUserField) {
							/** @var CoreUserField $field */
							echo $field->getTranslation();
						}
					?>
				</th>
				<?php $br++;
			}
			?>
		</tr>
		</thead>
		<tbody>
			<?php

				/* @var $sessionModel LtCourseSession */
				$counter = 0;
				$orderBy = isset($listOfFieldsTitles[0]) ? $listOfFieldsTitles[0] : 'username';

				$users = $sessionModel->getEnrolledUsers(
					LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT,
					true,
					isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]),
					$orderBy
				);

				foreach ($users as $user) {
					$fieldValues = null;
					$counter++;
					echo '<tr>';

					/** @var CoreUser $userModel */
					$userModel = CoreUser::model()->findByPk($user['id_user']);

					//row index
					echo '<td class="no-left-border index">', $counter, '</td>';
					$br = 0;

					foreach ($listOfFieldsTitles as $field) {
						$noWrapStyle = '';

						if ($br == 0) {
							echo '<td class="no-left-border fullname" style="', $noWrapStyle, '">';
						} else {
							echo '<td style="padding: 10px; ', $noWrapStyle, '">';
						}

						if (is_scalar($field) === true) {
							switch($field) {
								case 'fullname':
									echo $userModel->getFullName();
									break;
								case 'username':
									echo $userModel->getUserNameFormatted();
									break;
								case 'email':
									echo $userModel->email;
									break;
								case 'language':
									echo $userModel->getCurrentUserLanguage();
									break;
								case 'signature1':
									echo ' ';
									break;
								case 'signature2':
									echo ' ';
									break;
								default:
									break;
							}
						} else if ($field instanceof CoreUserField) {
							$fieldId = $field->getFieldId();
							$fieldType = $field->getFieldType();
							$typeFieldInstance = $field->getTypeModel();

							if ($fieldType == CoreUserField::TYPE_DATE) {
								$noWrapStyle = 'white-space: nowrap;';
							}

							// Fetching the additional field values for the user if not loaded until now
							if ($fieldValues === null) {
								$fieldValues = CoreUserField::getValuesForUsers([$userModel->idst], true);
								$fieldValues = count($fieldValues) ? current($fieldValues) : [];
							}

							echo isset($fieldValues[$fieldId]) ? $field->renderValue($fieldValues[$fieldId]) : '-';
						}

						echo '</td>';
						$br++;
					}
					echo '</tr>';

					if ($counter >= 5) break; //this is just a preview, we don't need to show more users
				}

			?>
		<tbody>
	</table>
</div>