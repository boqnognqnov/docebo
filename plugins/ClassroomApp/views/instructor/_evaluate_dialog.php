<?php

if (!isset($context)) { $context = 'showing'; }

?>

<h1>
	<?php
		echo ($context == 'showing'
			? Yii::t('classroom', 'Learner\'s evaluation')
			: Yii::t('classroom', 'Evaluate'));
	?>
</h1>

<div class="form session-evaluation-dialog">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'evaluate-user-form',
		'htmlOptions' => array(
			'class' => 'ajax',
			'enctype' => 'multipart/form-data'
		)
	)); ?>


	<?php
		// begin form content
	?>


	<?php

		if ($context == 'showing') {
			echo CHtml::hiddenField('edit-redirect', 1, array('id' => 'input-edit-redirect'));
		}

		if ($context == 'editing' && $isFromShowing) {
			$newAction = Docebo::createLmsUrl('ClassroomApp/instructor/axEditUserEvaluation', array(
				'course_id' => $idCourse,
				'id_session' => $idSession,
				'id_user' => $idUser
			));
			echo '<a class="update-action" data-action="'.$newAction.'"></a>';

		}

	?>

	<div class="row-fluid">

		<div class="span5 evaluation-user-info">
			<h3><?= Yii::t('menu_over', 'Student Area') ?></h3>
            <table>
                <tr>
			        <td class="instructor-avatar"><?php echo $userModel->getAvatarImage();?></td>
				    <td><?php echo $userModel->getFullName();?></td>
                </tr>
            </table>
		</div>

		<div class="span7">
			<h3><?= Yii::t('classroom', 'Course and session information') ?></h3>
			<p class="course-title"><?= $courseModel->name ?></p>
			<p class="session-title"><?= $sessionModel->name ?></p>
		</div>

	</div>

	<br />
	<?php

		if ($context == 'showing') {
			echo CHtml::label(Yii::t('classroom', 'Learning performance evaluation'), 'input-evaluation-text', array('class' => 'bordered'));
			echo '<div class="evaluation-text">'.Yii::app()->htmlpurifier->purify($enrollSessionModel->evaluation_text).'</div>';
		} else {
			echo CHtml::label(Yii::t('classroom', 'Learning performance evaluation'), 'input-evaluation-text');
			echo CHtml::textArea(
				'evaluation_text',
				($context == 'editing' ? $enrollSessionModel->evaluation_text : ''),
				array('id' => 'input-evaluation-text', 'class' => 'tinymce')
			);
		}
	?>
    <br />

	<?php
	// Trigger event to let plugins show their custom evaluation options
	$event = new DEvent($this, array (
		'context' => $context,
		'courseModel' => $courseModel,
		'sessionModel' => $sessionModel,
		'userModel' => $userModel,
		'enrollModel' => $enrollModel,
		'enrollSessionModel' => $enrollSessionModel
	));
	Yii::app()->event->raise('OnClassroomUserEvaluationDialogRender', $event);
	if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
		if (isset($event->return_value['html'])) { echo $event->return_value['html']; }
	}
	// end event
	?>

	<div class="row-fluid bordered-row">

		<!-- upload file -->
		<div class="span6">
			<h3><?= Yii::t('standard', '_ATTACHMENT') ?>:</h3>
			<?php


				$_downloadUrl = Docebo::createLmsUrl('ClassroomApp/instructor/downloadEvaluationFile', array(
					'id_user' => $idUser,
					'id_session' => $idSession,
					'course_id' => $idCourse
				));

				switch ($context) {

					//we have to upload a file
					case 'evaluation': {
						echo '<table><tr><td style="vertical-align: middle; padding-right: 10px;">';
						echo Yii::t('authoring', 'Select files').':&nbsp;';
						echo '</td><td>';
						echo CHtml::fileField('evaluation_file', '', array('id' => 'input-evaluation-file'));
						echo '</td></tr></table>';
					} break;

					//can only see attached file download link
					case 'showing': {
						if (!empty($enrollSessionModel->evaluation_file)) {
							echo '<span class="classroom-sprite dl-file"></span>';
							echo '<a href="'.$_downloadUrl.'" class="evaluation-file-download">'.$enrollSessionModel->getOriginalFileName().'</a>';
						} else {
							echo '<span class="attachment-none">('.Yii::t('standard', '_NONE').')</span>';
						}
					} break;

					//editing can see the attached file and remove it and upload a new file
					case 'editing': {

						if (!empty($enrollSessionModel->evaluation_file)) {
							echo '<div id="editing-evaluation-file-dl-link" style="display:block;">';
							echo '<span class="classroom-sprite dl-file"></span>';
							echo '<a href="'.$_downloadUrl.'" class="evaluation-file-download">'.$enrollSessionModel->getOriginalFileName().'</a>';
							echo '<span id="remove-uploaded-file" class="i-sprite is-remove red"></span>';
							echo CHtml::hiddenField('evaluation-file-removed', 0, array('id' => 'input-evaluation-file-removed'));
							echo '</div>';
							echo '<div id="editing-evaluation-file-new-upload" style="display:none;">';
							echo CHtml::fileField('evaluation_file', '', array('id' => 'input-evaluation-file'));
							echo '</div>';
						} else {
							echo CHtml::hiddenField('evaluation-file-removed', 1, array('id' => 'input-evaluation-file-removed')); //simulate a new file upload
							echo CHtml::fileField('evaluation_file', '', array('id' => 'input-evaluation-file'));
						}

					} break;

				}
			?>
		</div>

		<!-- set score (optional) -->
		<div class="span6">
			<?php
				switch ($context) {
					case 'evaluation':
					case 'editing': {
			?>
			<div class="set-evaluation-score clearfix">
				<table>
					<tr>
						<td><?= Yii::t('standard', '_SCORE') ?>:</td>
						<td>
							<?php
								echo CHtml::textField(
									'evaluation_score', ($context == 'editing' ? $enrollSessionModel->evaluation_score : ''), array('id' => 'input-evaluation-score'));
							?>
							<div style="display:none;" id="invalid-score" class="errorMessage"><?= Yii::t('deliverable', 'Invalid score'); ?></div>
						</td>
						<td>&nbsp;/&nbsp;<?= $sessionModel->score_base ?></td>
					</tr>
				</table>
				</div>
				<p class="score-desc"><?= Yii::t('classroom', 'Optional score evaluation can be added') ?></p>
			<?php
					} break;
					case 'showing': {
			?>
			<div class="set-evaluation-score">
                <table>
                    <tr>
                        <td><?= Yii::t('standard', '_SCORE') ?>:</td>
				        <td><span class="evaluation-score <?= $enrollSessionModel->evaluation_status > 0 ? 'good' : ($enrollSessionModel->evaluation_status < 0 ? 'bad' : '') ?>"><?= $enrollSessionModel->evaluation_score ?></span></td>
                        <td>&nbsp;/&nbsp;<?= $sessionModel->score_base ?></td>
                    </tr>
               </table>
			</div>
			<?php
					} break;
				} //end switch
			?>
		</div>
	</div>

	<div class="row-fluid bordered-row">

		<div class="span6">
			<div class="status-label">
				<?= Yii::t('standard', '_STATUS') ?>:&nbsp;<span id="status-selection"></span>
			</div>
		</div>

		<div class="span6" style="text-align: right;">
			<?php if ($context == 'showing'): ?>
			<span class="evaluation-score <?php echo ($enrollSessionModel->evaluation_status > 0 ? 'good' : 'bad'); ?>">
				<?php echo ($enrollSessionModel->evaluation_status > 0 ? Yii::t('coursereport', '_PASSED') : Yii::t('coursereport', '_NOT_PASSED')); ?>
			</span>
			&nbsp;&nbsp;
			<span class="classroom-sprite <?php echo ($enrollSessionModel->evaluation_status > 0 ? 'approved-large' : 'unapproved-large'); ?>"></span>
			<?php else: ?>
			<span style="margin-right: 10px;"><?= Yii::t('classroom', 'Mark as') ?>:</span>
			<a href="#" class="classroom-sprite approved-medium grey" id="evaluation-status-trigger-good" data-status="<?= LtCourseuserSession::EVALUATION_STATUS_PASSED ?>"></a>
			<a href="#" class="classroom-sprite unapproved-medium grey" id="evaluation-status-trigger-bad" data-status="<?= LtCourseuserSession::EVALUATION_STATUS_FAILED ?>"></a>
			<?php endif; ?>
		</div>

		<?php echo CHtml::hiddenField('evaluation_status', '', array('id' => 'input-evaluation-status')); ?>
	</div>

	<?php
		if ($context != 'showing') {
			echo CHtml::hiddenField('confirm', '1', array('id' => 'input-confirm'));
		}
	?>


	<?php if ($context == 'showing'): //display evaluator info ?>
	<div class="row-fluid">
		<div class="span5 evaluation-date"><?= $enrollSessionModel->evaluation_date ?></div>
		<div class="span7">
            <?php if ($enrollSessionModel->evaluator):?>
            <table class="instructor-box clearfix">
                <tr>
                    <td class="instructor-avatar"><?= $enrollSessionModel->evaluator->getAvatarImage(); ?></td>
				    <td>
                        <div><?= Yii::t('menu_over', 'Teacher') ?></div>
                        <div class="instructor-name"><?= $enrollSessionModel->evaluator->getFullName(); ?></div>
                    </td>
                </tr>
            </table>
            <?php endif; ?>
		</div>
	</div>
	<?php endif; ?>


	<?php
		// end form content
	?>

	<div class="form-actions">
		<?php
			switch ($context) {
				case 'showing': {
					echo CHtml::submitButton(Yii::t('standard','_MOD'), array('class' => 'btn confirm-btn blue notification-submit'));
				} break;
				case 'editing':
				case 'evaluation': {
					echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn confirm-btn notification-submit', 'id' => 'evaluation-confirm-button'));
				} break;
			}

			echo CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog'));
		?>
	</div>

	<?php $this->endWidget(); ?>

</div>
<script type="text/javascript">

//initialize active JS components into dialog
$(function() {

	var statusManager = {
		input: $('#input-evaluation-status'),
		selectedStatus: null,
		handler: function(e) {
			e.preventDefault();
			this.setStatus($(e.target).data("status"));
		},
		setStatus: function(newStatus) {
			var s = $('#status-selection');
			switch (newStatus) {
				case <?= LtCourseuserSession::EVALUATION_STATUS_PASSED ?>:
					this.input.val('1');
					this.selectedStatus = 1;
					s.html('<span class="evaluation-score good">'+<?= CJSON::encode(Yii::t('coursereport', '_PASSED')) ?>+'</span>');
					$('#evaluation-status-trigger-good').removeClass('grey');
					$('#evaluation-status-trigger-bad').addClass('grey');
					break;
				case <?= LtCourseuserSession::EVALUATION_STATUS_FAILED ?>:
					this.input.val('-1');
					this.selectedStatus = -1;
					s.html('<span class="evaluation-score bad">'+<?= CJSON::encode(Yii::t('coursereport', '_NOT_PASSED')) ?>+'</span>');
					$('#evaluation-status-trigger-bad').removeClass('grey');
					$('#evaluation-status-trigger-good').addClass('grey');
					break;
			}
			//after setting status, allow confirmation for form
			var cb = $('.notification-submit');
			cb.removeAttr('disabled');
			cb.removeClass('disabled');
		}
	};

	var statusProxy = $.proxy(statusManager.handler, statusManager);
	$('#evaluation-status-trigger-good').bind("click", statusProxy);
	$('#evaluation-status-trigger-bad').bind("click", statusProxy)

<?php
	//if we are editing an existing evaluation, we have to pre-set the status
	if ($context == 'editing' && $enrollSessionModel->evaluation_status !== NULL) {
		?>
	statusManager.setStatus(<?php echo (is_numeric($enrollSessionModel->evaluation_status) ? $enrollSessionModel->evaluation_status : null); ?>);
		<?php
	}

	//if we are evaluating an user for the first time, status is not set and confirm button should be disabled at beginning
	if ($context == 'evaluation') {
		?>
	var cb = $('.notification-submit');
	cb.attr('disabled', "disabled");
	cb.addClass('disabled');
		<?php
	}
?>

<?php
//if we are just showing an user evaluation, then no text editor is needed
if ($context != 'showing'):
?>
	/*tinymce.init({
		height  : '170px',
		selector: '#input-evaluation-text',
		theme   : "modern",
		plugins : [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste moxiemanager"
		],
		toolbar : "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		menubar  : false,
		statusbar: false,
		language: yii.language  // this one is set in /common/controllers/Controller.php, beforeRender()
	});*/
	TinyMce.attach('#input-evaluation-text', 'standard_embed', {height: 170});
<?php
endif;
?>

<?php
//initialize file uploader, this is needed only in evaluation/editing actions
if ($context == 'evaluation' || $context == 'editing'):
?>

$('#input-evaluation-file').styler({browseText:'<?= Yii::t('organization', 'Upload File')?>' });

<?php if ($context == 'editing'): ?>

<?php endif; ?>
$('#remove-uploaded-file').bind('click', function(e) {
	$('#editing-evaluation-file-dl-link').css('display', 'none');
	$('#editing-evaluation-file-new-upload').css('display', 'block');
	$('#input-evaluation-file-removed').val('1');
});
<?php
endif;
?>

});

//set dialog behaviors on server answer
$(document).delegate(".edit-evaluation-dialog", "dialog2.content-update", function() {
	var e = $(this), autoclose = e.find("a.auto-close");
	if (autoclose.length > 0) {
		e.find('.modal-body').dialog2("close");
		updateGrid();
	} else {
		var err = e.find("a.error");
		if (err.length > 0) {
			var msg = $(err[0]).data('message');
			Docebo.Feedback.show('error', msg);
			e.find('.modal-body').dialog2("close");
		}
	}
	//check for form action updating
	var update = e.find("a.update-action");
	if (update.length > 0) {
		$('#evaluate-user-form').attr('action', $(update[0]).data('action'));
	}
});

// On closing the dialog we must destroy TinyMce editors;
$(document).delegate(".edit-evaluation-dialog", "dialog2.closed", function() {
    try{
        tinymce.remove();
    } catch(e){}
});

function checkScore()
{
	var max_score = <?= $sessionModel->score_base; ?>;
	var input = $('#input-evaluation-score');
	var submit = $('.notification-submit');
	var score_error = $('#invalid-score');

	if(input.val() > max_score)
	{
		submit.attr('disabled', "disabled");
		submit.addClass('disabled');
		input.addClass('error');
		score_error.css('display', 'block');
	}
	else
	{
		submit.removeAttr('disabled');
		submit.removeClass('disabled');
		input.removeClass('error');
		score_error.css('display', 'none');
	}
}

$('#input-evaluation-score').change(checkScore);
$(document).ready(checkScore);

</script>
