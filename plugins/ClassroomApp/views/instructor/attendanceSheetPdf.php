<?php

//some css classes are not correctly read by mPDF1 PHP class. We are going
//to write inline styles in order to improve compatibility while printing
//the PDF file
// TODO: find a better way to implement this part, avoid inlined hardcoded styles if possible !!!
// @todo Refactored to support CoreUserField and the related models as a source of additional fields, but there's plenty
// 		 of things that need to be refactored. Code is not clean, there is no performance taken in mind whatsoever... needs to be fixed

class tmpPdfStyler {

	protected $_styles = array(

		'main' => "font-family: 'Open Sans', sans-serif;",

		'valign-top' => 'vertical-align: top;',

		'block-space' => 'margin-top: 22px;',

		'course-title' => 'font-size: 21px; font-weight: bold;',
		'session-title' => 'font-size: 17px;',

		'location-list-item' => 'font-size: 11px; text-align: left;',

		'time-info-blue-text' => 'font-weight: bold; font-size: 19px; color: #0465AC; text-align: center;',


		'misc-info' => 'width: 100%;',
		'misc-location-column' => 'width: 20%',
		'misc-instructors-column' => 'width: 40%;',
		'misc-title' => 'font-size:14px;text-align: left; font-weight: bold; font-size: 12px; height: 17px; vertical-align: middle;',
		//'signature-underline' => 'bottom: 4px; border-bottom: 1px solid #CCCCCC; margin-left: 6px; margin-right: 6px;',
		'signature-underline' => 'border-bottom: 1px solid #CCCCCC;',
		'instructor-list' => 'width: 100%;',
		'instructor-name' => 'width: 33%; font-size: 11px; text-align: left;',
		'instructor-sign' => 'width: 67%',
		'instructor-list-row' => 'height: 22px;',
		'time-def' => 'font-size: 12px;',

		'users-table' => 'border-collapse: collapse; color: #000000; width: 100%;',
		'th' => 'border-bottom: 1px solid #CCCCCC; font-size: large; font-weight: bold; vertical-align: bottom;padding: 0 5px; padding-bottom: 8px;',
		'th-time' => 'font-weight: normal;',
		'th-single' => 'width: 33%;',
		'th-double' => 'width: 66%;',

		'td' => 'border-bottom: 1px solid #CCCCCC; vertical-align: middle; height: 40px;',
		'td-left' => 'border-left: 1px solid #CCCCCC;',
		'td-index' => 'width: 5%; text-align:center; font-weight: 600;',
		'td-fullname' => 'width: 28%;',
		'td-single' => 'width: 33%;',
		'td-double' => 'width: 66%;',

		'sessionCheckMark' => 'margin: 15px 25px; border: 1px solid #CCC;padding: 15px 50px',
		'sessionCheckMarkParent' => 'border-top: 1px solid #CCC;border-bottom: 1px solid #CCC;text-align: center;padding: 20px',
		'no-left-border' => 'padding: 5px 0',
	);

	function get($classes) {
		$arr = explode(';', $classes);
		static $event = null;
		if (!$event) {
			$event = new DEvent($this, array());
			Yii::app()->event->raise('SetStylesInPdf', $event);
			if (!$event->shouldPerformAsDefault()) {
				$this->_styles = array_merge($this->_styles, $event->return_value);
			}
		}
		$final = array();
		if (!empty($arr)) {
			foreach($arr as $single) {
				$index = trim($single);
				if (isset($this->_styles[$index])) {
					$tmp = explode(';', $this->_styles[$index]);
					if (!empty($tmp)) {
						foreach ($tmp as $single_style) {
							if (!empty($single_style)) {
								$final[] = trim($single_style);
							}
						}
					}
				}
			}
		}

		$output = '';
		if (!empty($final)) {
			$output = ' style="'.implode(';', $final).'"';
		}

		$output = str_replace(';;', ';', $output);

		return $output;
	}

}

$styler = new tmpPdfStyler();

//retrieve date times
$dateTimes = array();
if (!empty($dateModel->break_begin) && !empty($dateModel->break_end)) {
	$dateTimes[] = array($dateModel->time_begin, $dateModel->break_begin);
	$dateTimes[] = array($dateModel->break_end, $dateModel->time_end);
} else {
	$dateTimes[] = array($dateModel->time_begin, $dateModel->time_end);
}

?>

<body<?= $styler->get('main'); ?>>
	<div class="attendance-sheet-pdf">

		<?php if(!$customHeader) : ?>
			<div style="padding-bottom:6px; border-bottom: 1px solid #CCCCCC;">

				<table style="width: 100%">
					<tbody>
					<tr>
						<td style="text-align: left; vertical-align:bottom;">
							<img src="<?= Yii::app()->theme->getLogoUrl() ?>" />
						</td>
						<td style="text-align: right; padding-right: 8px; font-size: 18px; vertical-align:bottom;">
							<?= Yii::t('classroom', 'Attendance sheet'); ?>
						</td>
					</tr>
					</tbody>
				</table>

			</div>
		<?php endif; ?>

		<div<?= $styler->get('block-space') ?>>
			<div<?= $styler->get('course-title') ?>><?= $courseModel->name ?></div>
			<div<?= $styler->get('session-title') ?>><?= $sessionModel->name ?></div>
		</div>

		<div<?= $styler->get('block-space') ?>>
			<?php
			if($allSessions === false){ ?>
				<table<?= $styler->get('misc-info') ?>>
					<tbody>
						<tr>
							<td<?= $styler->get('misc-title;misc-location-column') ?>>
								<?= Yii::t('classroom', 'Location') ?>
							</td>
							<td<?= $styler->get('misc-title;misc-instructors-column') ?>>
								<?= Yii::t('standard', 'Teacher(s)') ?>
							</td>
							<td rowspan="2" class="time-info" style="width: 20%; text-align: center; vertical-align: top;">
								<div>
									<img src="<?= Yii::app()->theme->baseUrl . '/images/classroom_icon_clock.png' ?>" />
								</div>
								<div<?=$styler->get('time-info-blue-text')?>>
									<?php
									$timesToShow = array();
									$time = '';

									foreach ($dateTimes as $dateTime) {
										if ($dateTime[1] == '00:00:00') {
											$time = $dateTime[0];
										} else if ($dateTime[0] == '00:00:00') {
											$timesToShow[] = Yii::app()->getModule('ClassroomApp')->formatTime($time).' - '.Yii::app()->getModule('ClassroomApp')->formatTime($dateTime[1]);
										} else {
											$timesToShow[] = Yii::app()->getModule('ClassroomApp')->formatTime($dateTime[0]).' - '.Yii::app()->getModule('ClassroomApp')->formatTime($dateTime[1]);
										}
									}
									echo implode('<br />', $timesToShow);
									?>
								</div>
								<div<?= $styler->get('time-def') ?>>
									<span><?= Yii::t('course', '_PARTIAL_TIME') ?></span>
								</div>
							</td>
							<td rowspan="2" class="time-info" style="width: 20%; text-align: center; vertical-align: top;">
								<div>
									<img src="<?= Yii::app()->theme->baseUrl . '/images/classroom_icon_calendar.png' ?>" />
								</div>
								<div<?=$styler->get('time-info-blue-text')?>>
									<?php
									echo Yii::app()->localtime->toLocalDate($dateModel->day);
									?>
								</div>
								<div<?= $styler->get('time-def') ?>>
									<span><?= Yii::t('classroom', 'Session date(s)') ?></span>
								</div>
							</td>
						</tr>
						<tr>
							<td<?= $styler->get('valign-top') ?>>
								<!-- location details -->
								<?php
								echo '<div'.$styler->get('location-list-item').'>'.$dateModel->location->name.'</div>';
								echo '<div'.$styler->get('location-list-item').'>'.$dateModel->classroom->name.'</div>';
								echo '<div'.$styler->get('location-list-item').'>'.$dateModel->location->address.'</div>';
								echo '<div'.$styler->get('location-list-item').'>'.$dateModel->location->country->name_country.'</div>';
								?>
							</td>
							<td<?= $styler->get('valign-top') ?>>
								<!-- instructors list -->
								<?php
								$instructors = LtCourseuserSession::model()->findSessionInstructors($idSession);//$enrollSessionModel->findInstructors();
								if (!empty($instructors)) {
									echo '<table'.$styler->get('misc-info').'>';
									echo '<tbody>';
									foreach ($instructors as $instructor) {
										echo '<tr'.$styler->get('instructor-list-row').'>';
										echo '<td'.$styler->get('instructor-name').'>'.$instructor->getFullName().'</td>';
										echo '<td'.$styler->get('instructor-sign').'><table style="width: 190px;" ><tr><td '.$styler->get('signature-underline').'>';
										echo '&nbsp;</td></tr></table></td>';
										echo '</tr>';
									}
									echo '</tbody>';
									echo '</table>';
								} else {
									echo '<div class="no-instructors">';
									echo '('.Yii::t('classroom', 'No instructors').')';
									echo '</div>';
								}
								?>
							</td>
						</tr>
					</tbody>
				</table>
			<?php
			} else {
			// template only for instructors
			?>
				<table<?= $styler->get('misc-info') ?>>
					<tbody>
					<tr>
						<td<?= $styler->get('misc-title;misc-instructors-column') ?>>
							<?= Yii::t('standard', 'Teacher(s)') ?>
						</td>
					</tr>
					<tr>
						<td<?= $styler->get('valign-top') ?>>
							<!-- instructors list -->
							<?php
							$instructors = LtCourseuserSession::model()->findSessionInstructors($idSession);//$enrollSessionModel->findInstructors();
							if (!empty($instructors)) {

								echo '<table'.$styler->get('misc-info').'>';
								echo '<tbody>';
								foreach ($instructors as $instructor) {
									echo '<tr'.$styler->get('instructor-list-row').'>';
									echo '<td'.$styler->get('instructor-name').'>'.$instructor->getFullName().'</td>';
									echo '<td'.$styler->get('instructor-sign').'><table style="width: 190px;" ><tr><td '.$styler->get('signature-underline').'>';
									echo '&nbsp;</td></tr></table></td>';
									echo '</tr>';
								}
								echo '</tbody>';
								echo '</table>';

							} else {

								echo '<div class="no-instructors">';
								echo '('.Yii::t('classroom', 'No instructors').')';
								echo '</div>';

							}
							?>
						</td>
					</tr>
					</tbody>
				</table>
			<?php
			}
			?>
		</div>

		<div<?= $styler->get('block-space') ?>>
			<table<?= $styler->get('users-table') ?>>
				<thead>
					<tr>
						<?php
						$br = 0;
						$currentLanguage = Yii::app()->db->createCommand()->select('lang_code')->from(CoreLangLanguage::model()->tableName())
							->where('lang_browsercode=:lang', array(':lang' => Yii::app()->getLanguage()))->queryScalar();
						if ($allSessions === true) { ?>
							<th style="text-transform: uppercase;text-align: left;padding-left: 5px; width: 200px">
								<?= Yii::t('classroom', 'Learner info'); ?>
							</th>
						<?php
						} else {
							foreach ($listOfFieldsTitles as $field) {
								?>
								<th <?=$styler->get('th')?> <?= ($br == 0 ? 'colspan="2"' : '') ?>>
									<?php
									if (is_scalar($field) === true) {
										switch ($field) {
											case 'fullname':
												echo Yii::t('classroom', 'Name and Surname');
												break;
											case 'username':
												echo Yii::t('standard', '_USERNAME');
												break;
											case 'email':
												echo Yii::t('standard', '_EMAIL');
												break;
											case 'language':
												echo Yii::t('standard', '_LANGUAGE');
												break;
											case 'signature1':
												echo Yii::t('standard', '_SIGNATURE') . ' (' . $timesToShow[0] . ')';
												break;
											case 'signature2':
												echo Yii::t('standard', '_SIGNATURE') . ' (' . $timesToShow[1] . ')';
												break;
											default:
												break;
										}
									} else if ($field instanceof CoreUserField) {
										/** @var CoreUserField $field */
										echo $field->getTranslation();
									}
									?>
								</th>
								<?php
								$br++;
							}
						}

						if ($allSessions === true) {
							foreach ($listOfSessionsDates as $sessionDate) {
								$day = Yii::app()->localtime->toLocalDate($sessionDate->day);
								$timeBegin = Yii::app()->localtime->toLocalTime($sessionDate->time_begin, LocalTime::SHORT);
								$timeEnd = Yii::app()->localtime->toLocalTime($sessionDate->time_end, LocalTime::SHORT);
								echo '<th>';
								echo '<div>' . $day . ' ' . $timeBegin . '</div><br/><div>' . $day . ' ' . $timeEnd . '</div>';
								echo '</th><th style="width: 20px"></th>';
							}
						}
						?>
					</tr>
				</thead>

				<tbody>
				<?php
				$counter = 0;
				$orderBy = isset($listOfFieldsTitles[0]) ? $listOfFieldsTitles[0] : 'username';

				$users = $sessionModel->getEnrolledUsers(
					LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT,
					true,
					isset(Yii::app()->session['PUIsInstructorForCourse'][$idCourse][$idSession]),
					$orderBy
				);

				foreach ($users as $user) {
					$counter++;
					/** @var CoreUser $userModel */
					$userModel = CoreUser::model()->findByPk($user['id_user']);
					$fieldValues = null;
					echo '<tr>';

					if ($allSessions === true) {
						echo '<td style="padding: 10px;border-bottom:1px solid #CCCCCC;border-top:1px solid #CCCCCC;border-left: 0;border-right: 0;" >';

						$switchNamePlaces = false;
						//Raise event to handle the username switcher if its needed
						$usernameEvent = new DEvent($this, array('type' => 'username', 'userModel'=> $userModel));
						$lastname = Yii::app()->event->raise('onRenderAttendanceSheetList', $usernameEvent);
						if(isset($usernameEvent->return_value['switch_names']) && $usernameEvent->return_value['switch_names']) {
							$switchNamePlaces = true;
						}

						// Switch the names depends on the case
						if($switchNamePlaces) {
							echo $usernameEvent->return_value['switched_name'];
						} else {
							foreach ($listOfFieldsTitles as $field) {
								$skipped = false;

								if (is_scalar($field) === true) {
									switch ($field) {
										case 'fullname':
											echo '<span style="font-weight: bold">' . $userModel->getFullName() . '</span>';
											break;
										case 'username':
											echo $userModel->getUserNameFormatted();
											break;
										case 'email':
											echo $userModel->email;
											break;
										case 'language':
											echo $userModel->getCurrentUserLanguage();
											break;
										case 'signature1':
											$skipped = true;
											break;
										case 'signature2':
											$skipped = true;
											break;
										default:
											break;
									}
								} else if ($field instanceof CoreUserField) {
									$fieldId = $field->getFieldId();
									$fieldType = $field->getFieldType();
									$typeFieldInstance = $field->getTypeModel();

									if ($fieldType == CoreUserField::TYPE_DATE) {
										$noWrapStyle = 'white-space: nowrap;';
									}

									// Fetching the additional field values for the user if not loaded until now
									if ($fieldValues === null) {
										$fieldValues = CoreUserField::getValuesForUsers([$userModel->idst], true);
										$fieldValues = count($fieldValues) ? current($fieldValues) : [];
									}

									echo isset($fieldValues[$fieldId]) ? $field->renderValue($fieldValues[$fieldId]) : '-';
								}

								if (!$skipped) {
									echo '<br/>';
								}
							}
						}

						// Trigger event to let plugins showing their custom info
						$event = new DEvent($this, array(
							'rowIndex' => $counter,
							'isPreview' => true,
							'userModel' => $userModel,
							'courseModel' => $courseModel,
							'sessionModel' => $sessionModel
						));
						Yii::app()->event->raise('OnClassroomAttendanceSheetUserInfo', $event);
						// end event

						echo '</td>';

						foreach($listOfSessionsDates as $sessionDate) {
							echo '<td '.$styler->get('sessionCheckMarkParent').'><table><tr><td '.$styler->get('sessionCheckMark').'></td></tr></table></td>';
							echo '<td style="width: 20px"> </td>';
						}
					} else {

						//row index
						echo '<td style="padding: 10px;border-bottom:1px solid #CCCCCC;border-left: 0;border-right: 0;' . $noWrapStyle . '" >' . $counter . '</td>';
						$br = 0;
						foreach ($listOfFieldsTitles as $field) {
							$alignStyle = 'text-align: center;';
							$noWrapStyle = '';

							if (is_scalar($field) === true) {
								switch($field) {
									case 'fullname':
										$cellContent = $userModel->getFullName();
										break;
									case 'username':
										$cellContent = $userModel->getUserNameFormatted();
										break;
									case 'email':
										$cellContent = $userModel->email;
										break;
									case 'language':
										$cellContent = $userModel->getCurrentUserLanguage();
										break;
									case 'signature1':
										$cellContent = ' ';
										break;
									case 'signature2':
										$cellContent = ' ';
										break;
									default:
										break;
								}
							} else if ($field instanceof CoreUserField) {
								$fieldId = $field->getFieldId();
								$fieldType = $field->getFieldType();
								$typeFieldInstance = $field->getTypeModel();

								if ($fieldType == CoreUserField::TYPE_DATE) {
									$noWrapStyle = 'white-space: nowrap;';
								}

								// Fetching the additional field values for the user if not loaded until now
								if ($fieldValues === null) {
									$fieldValues = CoreUserField::getValuesForUsers([$userModel->idst], true);
									$fieldValues = count($fieldValues) ? current($fieldValues) : [];
								}

								$cellContent = isset($fieldValues[$fieldId]) ? $field->renderValue($fieldValues[$fieldId]) : '-';
							}

							$tdOpen = '';
							if ($br == 0) {
								$tdOpen = '<td class="no-left-border fullname" style="border-bottom:1px solid #CCCCCC;'.$noWrapStyle.'">';
							} else {
								$tdOpen = '<td style="padding: 10px;border-bottom:1px solid #CCCCCC; '.$alignStyle.$noWrapStyle.'">';
							}

							echo $tdOpen, $cellContent, '</td>';
							$br++;
						}

						echo '</tr>';
					}

					echo '</tr>';
				}
				?>
				<tbody>
			</table>
		</div>
	</div>
</body>