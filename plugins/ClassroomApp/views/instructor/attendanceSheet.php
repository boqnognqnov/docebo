<?php
if($allSessions === true){?>
	<style>
		.modal.attendance-sheet-print-dialog {
			width: 1200px;
			margin-left: -600px;
		}
	</style>
<?php } ?>
<h1><?= Yii::t('classroom', 'Attendance sheet') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'remove-enroll-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>

	<div class="filters-wrapper">
		<table class="filters">
			<tbody>
				<tr>
					<td class="table-padding">

						<table class="table-border">
							<tbody>
								<tr>
									<td class="group">
										<?= Yii::t('classroom', 'Select session date') ?>
									</td>
									<td class="group">
										<?php
											$dates = $sessionModel->getDates();
											$dropDownList = array();
											if (!empty($dates)) {
												$cmodule = Yii::app()->getModule('ClassroomApp');
												foreach ($dates as $date) {
													$dropDownList[$date->day] =
                                                        Yii::app()->localtime->toLocalDate($date->day)
														.' '.Yii::t('standard', '_FROM').' '
														.$cmodule->formatTime($date->time_begin)
														.' '.Yii::t('standard', '_TO').' '
														.$cmodule->formatTime($date->time_end);
												}
												$dropDownList[] = Yii::t('classroom', 'All dates');
											} else {
												//this shouldn't happen ...
												throw new CException('Error: no dates in the session');
											}
											echo CHtml::dropDownList('day', $selectedDate, $dropDownList, array('id' => 'input-day'));
										?>
									</td>
									<?php
										$order_by = 0;
										// Raise event to Handle the Sheet Filter Options
										$event = new DEvent($this, array('course_id' => $idCourse, 'id_sessions' => $idSession));
										Yii::app()->event->raise('onRenderAttendanceSheetFilters', $event);
										if(isset($event->return_value['showDropDown']) && $event->return_value['showDropDown'])
										{
											$order_by = isset($_POST['order_by']) ? $_POST['order_by'] : 0;
											echo $event->return_value['dropDown'];
										}
									?>
									<td class="group">
										<?php
											echo CHtml::submitButton(Yii::t('certificate','_GENERATE'), array('class' => 'btn confirm-btn notification-submit'));
										?>
									</td>
									<td class="group">

										<a href="<?php
												echo Docebo::createLmsUrl('ClassroomApp/instructor/attendancePdf', array(
													'course_id' =>$idCourse,
													'id_session' => $idSession,
													'day' => $selectedDate,
													'order_by' => $order_by,
													'listOfFieldsTitles' => isset($listOfFieldsTitles) ? $listOfFieldsTitles : null
												));
											?>" class="attendance-sheet-link">
											<?= Yii::t('report', 'Download as PDF') ?>
											&nbsp;
											<span class="i-sprite is-pdf"></span>
										</a>

									</td>
									<td class="group">
										<a href="<?php
										echo Docebo::createLmsUrl('ClassroomApp/instructor/configureAttendanceSheet', array(
												'course_id' =>$idCourse,
												'id_session' => $idSession,
										));
										?>" class="attendance-sheet-link hideModal open-dialog">
											<?= Yii::t('classroom', 'Configure sheet') ?>
											&nbsp;
											<span class="i-sprite is-gearpair"></span>
										</a>
									</td>
								</tr>
							</tbody>
						</table>

					</td>
        </tr>
			</tbody>
		</table>
	</div>

	<?php

	if($allSessions === true){
		// render additional layout with all sessions
		$this->renderPartial('_attendanceSheetPreviewAllSessions', array(
				'idCourse' => $this->idCourse,
				'courseModel' => $this->courseModel,
				'sessionModel' => $this->sessionModel,
				'listOfSessionsDates' => isset($listOfSessionsDates) ? $listOfSessionsDates : null,
				'listOfFieldsTitles' => isset($listOfFieldsTitles) ? $listOfFieldsTitles : null
		));
	}else {
		$dateModel = LtCourseSessionDate::model()->findByPk(array(
				'day' => $selectedDate,
				'id_session' => $idSession
		));
		if (!$dateModel) {
			//...
		}

		$this->renderPartial('_attendanceSheetPreview', array(
				'idCourse' => $this->idCourse,
				'idSession' => $this->idSession,
				'idDate' => $selectedDate,
				'courseModel' => $this->courseModel,
				'sessionModel' => $this->sessionModel,
				'dateModel' => $dateModel,
				'listOfFieldsTitles' => isset($listOfFieldsTitles) ? $listOfFieldsTitles : null
		));
	}
	$this->endWidget(); ?>
</div>
<script type="text/javascript">
$(document).delegate(".session-remove-enrollment-dialog", "dialog2.content-update", function() {
	var e = $(this), autoclose = e.find("a.auto-close");
	if (autoclose.length > 0) {
		e.find('.modal-body').dialog2("close");
		$.fn.yiiListView.update('session-enrollment-list');
	}
});
	$(function(){
		$(document).controls();
		$('.attendance-sheet-link.hideModal').click(function(){
			$('.modal.attendance-sheet-print-dialog').find('.modal-body').dialog2('close');
		})
	})
</script>