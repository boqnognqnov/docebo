<style>
	.sessionCheckMark{
		margin: 5% 10%;
		border: 1px solid #CCC;
		min-height: 30px;
	}
	table.attendance-sheet-users-table th{
		font-size: small;
	}
</style>

<div class="attendance-sheet-header">

	<table>
		<tbody>
			<tr>
				<td class="logo">
					<img src="<?= Yii::app()->theme->getLogoUrl() ?>" />
				</td>
				<td class="title">
					<?= Yii::t('classroom', 'Attendance sheet'); ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="attendance-sheet-titles">
	<div class="course-title"><?= $courseModel->name ?></div>
	<div class="session-title"><?= $sessionModel->name ?></div>
</div>
<div class="attendance-sheet-infobox">
	<table class="misc-info">
		<tbody>
		<tr>
			<td class="title">
				<?= Yii::t('standard', 'Teacher(s)') ?>
			</td>
		</tr>
		<tr>
			<td class="instructors-container">
				<!-- instructors list -->
				<?php
				$instructors = LtCourseuserSession::model()->findSessionInstructors($sessionModel->id_session);//$enrollSessionModel->findInstructors();
				if (!empty($instructors)) {

					echo '<table class="instructors-list">';
					echo '<tbody>';
					foreach ($instructors as $instructor) {
						echo '<tr>';
						echo '<td class="instructor-name" style="width: 50px">'.$instructor->getFullName().'</td>';
						echo '<td class="signature-space" style="width: 250px"><div class="underline" style="margin-right: 60%"></div></td>';
						echo '</tr>';
					}
					echo '</tbody>';
					echo '</table>';

				} else {

					echo '<div class="no-instructors">';
					echo '('.Yii::t('classroom', 'No instructors').')';
					echo '</div>';

				}
				?>
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="attendance-sheet-userlist">
	<table class="attendance-sheet-users-table">
		<thead>
			<tr>
				<?php
				$currentLanguage = Yii::app()->db->createCommand()->select('lang_code')->from(CoreLangLanguage::model()->tableName())
				->where('lang_browsercode=:lang', array(':lang' => Yii::app()->getLanguage()))->queryScalar();
				?>
				<th style="text-transform: uppercase;text-align: left;padding-left: 5px">
					<?= Yii::t('classroom', 'Learner info');?>
				</th>
				<?php
				foreach ($listOfSessionsDates as $sessionDate) {
					$day = Yii::app()->localtime->toLocalDate($sessionDate->day);
					$timeBegin = Yii::app()->localtime->toLocalTime($sessionDate->time_begin, LocalTime::SHORT);
					$timeEnd = Yii::app()->localtime->toLocalTime($sessionDate->time_end,LocalTime::SHORT);
					echo '<th>';
					echo '<div>' . $day . ' ' . $timeBegin . '</div><br/><div>' . $day . ' ' . $timeEnd . '</div>';
					echo '</th>';
				}
				?>
			</tr>
		</thead>

		<tbody>
			<?php
				$counter = 0;

				/* @var $sessionModel LtCourseSession */
				foreach ($sessionModel->getEnrolledUsers(LearningCourseuser::$USER_SUBSCR_LEVEL_STUDENT, true) as $user) {
					$counter++;
					$cells = [];

					/**
					 * @todo The code was refactored for using CoreUserField and related models, but needs a lot
					 * 		 of refactoring for performance improvements
					 */

					// A variable for output markup
					$noWrapStyle = $userName = $tdValue = '';
					$fieldValues = null;

					// Fetching user's instance
					$userModel = CoreUser::model()->findByPk($user['id_user']);

					//Raise event to handle the username switcher if its needed
					$switchNamePlaces = false;
					$usernameEvent = new DEvent($this, array('type' => 'username', 'userModel'=> $userModel));
					if (isset($usernameEvent->return_value['switch_names']) && $usernameEvent->return_value['switch_names']) {
						$switchNamePlaces = true;
					}

					if ($switchNamePlaces) {
						// Switch the names depends on the case
						$userName = $usernameEvent->return_value['switched_name'];
					} else {
						foreach ($listOfFieldsTitles as $field) {
							/** @var CoreUserField $field */
							$skipped = false;

							if (is_scalar($field) === true) {
								switch ($field) {
									case 'fullname':
                                        $tdValue = '<span style="font-weight: bold">'.$userModel->getFullName().'</span>';
										break;
									case 'username':
                                        $tdValue = $userModel->getUserNameFormatted();
										break;
									case 'email':
                                        $tdValue = $userModel->email;
										break;
									case 'language':
                                        $tdValue = $userModel->getCurrentUserLanguage();
										break;
									case 'signature1':
										$skipped = true;
										break;
									case 'signature2':
										$skipped = true;
										break;
									default:
										break;
								}
							} else if ($field instanceof CoreUserField) {
								$fieldId = $field->getFieldId();
								$fieldType = $field->getFieldType();
                                $typeFieldInstance = $field->getTypeModel();

                                if ($fieldType == CoreUserField::TYPE_DATE) {
						            $noWrapStyle = 'white-space: nowrap;';
                                }

                                // Fetching the additional field values for the user if not loaded until now
								if ($fieldValues === null) {
									$fieldValues = CoreUserField::getValuesForUsers([$userModel->idst], true);
									$fieldValues = count($fieldValues) ? current($fieldValues) : [];
								}

                                $tdValue = isset($fieldValues[$fieldId]) ? $field->renderValue($fieldValues[$fieldId]) : '-';
							}

							if(!$skipped) {
                                $tdValue .= '<br/>';
							}

							array_push($cells, '<td class="no-left-border fullname" style="' . $noWrapStyle . '">' . $tdValue . '</td>' . $dateCells);
						}

						foreach($listOfSessionsDates as $sessionDate) {
							array_push($cells, '<td class="no-left-border"><div class="sessionCheckMark"></div></td>');
						}
					}

                    // Trigger event to let plugins showing their custom info
					$event = new DEvent($this, array(
						'rowIndex' => count($cells),
						'isPreview' => true,
						'userModel' => $userModel,
						'courseModel' => $courseModel,
						'sessionModel' => $sessionModel
					));
					Yii::app()->event->raise('OnClassroomAttendanceSheetUserInfo', $event);
					// end event

					// Printing the row of data
					echo '<tr>' . implode("\n", $cells) . '</tr>';

					if ($counter >= 5) {
						//this is just a preview, we don't need to show more users
						break;
					}
				}
			?>
		</tbody>
	</table>
</div>