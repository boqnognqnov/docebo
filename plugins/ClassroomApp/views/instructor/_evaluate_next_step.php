<h1>
	<?= Yii::t('classroom', 'Evaluate') ?>
</h1>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'evaluate-user-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	
	
	
	<div class="row-fluid evaluate-next-user">
		
		<div class="span3">
			<div class="success-icon"></div>
			<p class="evaluation-successful">
				<?= Yii::t('classroom', 'Learner evaluated') ?>
			</p>
		</div>
		
		<div class="span9">
			<!-- title -->
			<h3><?php echo Yii::t('standard', 'What do you want to do next?'); ?></h3>
			<!--evaluate another user -->
			<div class="row-fluid">
				<div class="span8">
					<div style="margin-bottom: 5px;"><?php echo Yii::t('classroom', 'I want to evaluate another user'); ?></div>
					<?php echo CHtml::dropDownList('id_user', false, $usersList, array('id' => 'input-id_user')); ?>
				</div>
				<div class="span4" style="padding-top: 20px;">
					<?php echo CHtml::submitButton(Yii::t('classroom','Evaluate'), array('class' => 'btn confirm-btn notification-submit')); ?>
				</div>
			</div>

			<!-- back button -->
			<div class="row-fluid">
				<div class="span8">
					<span><?php echo Yii::t('standard', 'i want go back'); ?></span>
				</div>
				<div class="span4">
					<?php echo CHtml::link(Yii::t('standard', '_BACK'), 'javascript:void(0);', array(
						'id' => 'next-step-back-link',
						'class' => 'btn close-btn',
						'data-dismiss' => 'modal',
					)); ?>
				</div>
			</div>

		</div>

	</div>
	
	<?php $this->endWidget(); ?>
	
</div>
<script type="text/javascript">

var updateFormUrl = function(newIdUser) {
	var f = $('#evaluate-user-form');
	var oldAction = f.attr('action');
	f.attr('action', oldAction.replace(/id_user=[0-9]+/, 'id_user='+newIdUser));
}

$(function() {
    TinyMce.removeEditorById('input-evaluation-text');

	//assign close action to "go back" button
	$('#next-step-back-link').bind('click', function(e) {
		$(".edit-evaluation-dialog").find('.modal-body').dialog2("close");
	});

	//prepare form for next user to be evaluated
	var dd = $('#input-id_user');
	dd.bind('change', function(e) { updateFormUrl($(this).val()); });
	updateFormUrl(dd.val());

	//we are coming from a successful evaluation operation, so force grid refreshing
	updateGrid();
});

</script>
