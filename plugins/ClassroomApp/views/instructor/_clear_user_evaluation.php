<h1><?= Yii::t('classroom', 'Clear evaluation') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'clear-evaluation-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	<!--<input type="hidden" name="confirm" value="1" />-->
	<p><?=Yii::t('classroom', 'You are clearing evaluation for "{name}". Can you confirm that?', array('{name}' => $userModel->getFullName()))?></p>
	<div class="dialog-confirm-checkbox clearfix">
		<?php 
			echo CHtml::checkBox('confirm', false, array('id' => 'clear-user-evaluation-confirm-checkbox'));
			echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'clear-user-evaluation-confirm-checkbox', array('id' => 'clear-user-evaluation-confirm-checkbox-label'));
		?>
	</div>
	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array(
			'class' => 'btn confirm-btn notification-submit disabled clear-user-evaluation-confirm-button', 
			'id' => 'clear-user-evaluation-confirm-button', 
			'disabled' => 'disabled')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
//set dialog behaviors on server answer
$(document).delegate(".clear-evaluation-dialog", "dialog2.content-update", function() {
	var e = $(this), autoclose = e.find("a.auto-close");
	if (autoclose.length > 0) {
		e.find('.modal-body').dialog2("close");
		updateGrid();
	} else {
		var err = e.find("a.error");
		if (err.length > 0) {
			var msg = $(err[0]).data('message');
			Docebo.Feedback.show('error', msg);
			e.find('.modal-body').dialog2("close");
		}
	}
});
$(function() {
	var c = $('#clear-user-evaluation-confirm-checkbox');
	var b = {
		get: function() { return $('.clear-user-evaluation-confirm-button'); },
		enable: function() {
			var el = this.get();
			el.removeClass('disabled');
			$('#clear-user-evaluation-confirm-button').removeAttr('disabled');
		},
		disable: function() {
			var el = this.get();
			el.addClass('disabled');
			$('#clear-user-evaluation-confirm-button').attr('disabled', true);
		}
	};
	b.disable();
	c.styler();
	c.bind('change', $.proxy(function(e) {
		if ($(e.target).attr('checked')) {
			this.enable();
		} else {
			this.disable();
		}
	}, b));
});
</script>