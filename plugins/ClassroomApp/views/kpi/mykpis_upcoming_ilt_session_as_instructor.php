<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-flag2 large"></span>
</div>

<p class="block-head tight"><b><?= $name ?></b></p>
<div><?=$date?> <?=($location ? "- {$location}" : null)?></div>
<p><?= Yii::t( 'dashlets', '<b>Upcoming ILT session</b> (as instructor)' ) ?></p>