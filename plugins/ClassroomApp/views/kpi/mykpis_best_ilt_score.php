<style type="text/css">
	.mykpis-course-name-hover{
		background: url('<?=Yii::app()->theme->getAbsoluteBaseUrl()?>/images/icons_elements.png') no-repeat -716px -42px;
	}
</style>

<div class="pull-left mykpis-icon-wrapper">
	<span class="i-sprite is-cup orange large"></span>
</div>

<span class="score-holder pull-left"><span class="green"><?=$score?></span>/100</span>
<a href="<?=Docebo::createLmsUrl('/player', array('course_id'=>$idCourse))?>">
<span class="mykpis-course-name-hover" rel="tooltip" title="<?=$sessionName?>"></span>
</a>
<div><?=Yii::t('menu_course', 'Teacher')?> <b><?=implode(', ', $instructors)?></b></div>
<p><?=Yii::t('dashlets', '<b>Best score</b> on <b>ILT Session</b>')?></p>