<?php

$userIsSubscribed = LearningCourseuser::isSubscribed(Yii::app()->user->id, $courseModel->idCourse);
if(!$userIsSubscribed) {
    $this->breadcrumbs[Yii::t('menu', 'Admin')] = Docebo::createAppUrl('admin:dashboard/index');
    $this->breadcrumbs[Yii::t('standard', '_COURSES')] = Docebo::createAppUrl('admin:courseManagement/index');
} else {
    $this->breadcrumbs = array(
        Yii::t('menu_over', '_MYCOURSES') => Docebo::createLmsUrl('site/index'),
    );
}
$this->breadcrumbs[] = Docebo::ellipsis($courseModel->name, 60, '...');
$this->breadcrumbs[Yii::t('classroom', 'Sessions and Enrollments')] = Docebo::createAppUrl('lms:player/training/session', array('course_id' => $courseModel->getPrimaryKey()));
$this->breadcrumbs[] = Yii::t('standard', 'Training materials');

$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course' => $courseModel
));

?>
<h3 class="title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Docebo::createAppUrl('lms:player/training/session', array('course_id' => $courseModel->getPrimaryKey()))); ?>
	<span><?= Yii::t('standard', 'Training materials') ?></span>
</h3>
<br/>

<div id="classroom-los-management">


	<div id="classroom-los-editing-container">

		<div id="player-arena-container">

			<!-- ADD LO BUTTON -->
			<a id="arena-board-top" href="#arena-board-top"></a>

			<div class="row-fluid" data-bootstro-id="bootstroLaunchpad" id="player-arena-content">

				<!-- INLINE Edit pane -->
				<div id="player-lo-edit-inline-panel"></div>

				<!-- LAUNCHPAD  -->
				<div id="player-arena-launchpad"></div>

				<div class="clearfix"></div>

				<!-- LO MANAGER -->
				<div id="player-lo-manage-panel" class="player-arena-board">
					<div class="row-fluid player-lo-manage-header">
						<div class="span4">
							<span class="lom-title"><?php echo Yii::t('organization', 'Folders'); ?></span>
						</div>
						<div class="span8">
							<span class="lom-title"><?php echo Yii::t('standard', 'Training materials'); ?></span>

                            <?= $this->renderPartial("_add_lo_button", array(
                                'courseModel' => $courseModel,
                            )) ?>

						</div>
					</div>
					<div class="player-arena-overall-container">
						<div class="row-fluid">
							<div class="span4">
								<div id="player-arena-overall-folders" class="player-arena-overall-column">
									No folders
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="span8">
								<div id="player-arena-overall-objlist" class="player-arena-overall-column player-arena-overall-objlist"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- /LO MANAGER -->
			</div>

			<!--<div class="player-quicknav-wrapper" id="player-quicknavigation"></div>-->

			<!--<?php //$this->widget('OverlayHints'); ?>-->
		</div>


	</div>


	<script type="text/javascript">
		$(function() {


            Arena.organizer = new LoOrganizer({});
            Arena.uploader = new LoUploader();
            Arena.deliverable = new LoDeliverable({});
            Arena.deliverable.init({editDeliverableUrl: "<?= Yii::app()->createUrl('deliverable/default/axEditDeliverable') ?>"});
            Arena.htmledit = new LoHtmlEdit({});
            Arena.htmledit.init({editHtmlPageUrl: "<?=  Yii::app()->createUrl('htmlpage/default/axEditHtmlPage') ?>"});
            Arena.test = new LoTest({});
            Arena.poll = new LoPoll({});
			Arena.centralRepo = new LoCentralRepoEdit({});

			Arena.init({
				course_id: <?= $courseModel->getPrimaryKey() ?>,
				courseInfo: {
					idCourse: <?= $courseModel->getPrimaryKey() ?>,
					code: <?= CJSON::encode($courseModel->code) ?>,
					name: <?= CJSON::encode($courseModel->name) ?>,
					mpidCourse: <?= CJSON::encode($courseModel->mpidCourse) ?>,
					isMpCourse: <?= CJSON::encode($courseModel->mpidCourse > 0) ?>,
					//availableSeats: ,
					userHasSeat: <?= CJSON::encode(LearningLogMarketplaceCourseLogin::userHasSeat($courseModel->getPrimaryKey(), Yii::app()->user->id)) ?>,
					mpSeatsUnlimited: <?= CJSON::encode($courseModel->mpSeatsUnlimited()) ?>
				},
				getCourseLearningObjectsTreeUrl: "<?= Docebo::createLmsUrl('player/training/axGetCourseLearningObjectsTree') ?>",
				createFolderDialogUrl: "<?= Docebo::createLmsUrl('player/training/axCreateFolder', array('course_id' => $courseModel->getPrimaryKey())) ?>",
				uploadDialogUrl: "<?= Docebo::createLmsUrl('player/training/axLoUploadDialog') ?>",
				uploadFilesUrl: "<?= Docebo::createLmsUrl('player/training/axUpload') ?>",
				saveUploadedCoursedocUrl: "<?= Docebo::createLmsUrl('player/block/axSaveCoursedoc') ?>",
				openInCentralRepositoryUrl: "<?= (Yii::app()->user->getIsGodadmin() ? Yii::app()->createAbsoluteUrl("centralrepo/centralRepo/index") : "")?>",
				pluploadAssetsUrl: 	"<?= Yii::app()->plupload->getAssetsUrl() ?>",
				csrfToken: "<?= Yii::app()->request->csrfToken ?>",
				deleteLoUrl: "<?= Docebo::createLmsUrl('player/training/axDeleteLo', array('course_id' => $courseModel->getPrimaryKey())) ?>",
				sortLearningObjectUrl: "<?= Docebo::createLmsUrl('player/training/axReorderLo') ?>",
				moveInFolderLearningObjectUrl: "<?= Docebo::createLmsUrl('player/training/axMoveLoToFolder') ?>",
				userCanAdminCourse: true,
				axEditLoPrerequisitesUrl: "<?= Docebo::createLmsUrl('player/training/axEditLoPrerequisites') ?>",
				onLoadArenaMode: /*"<?= Yii::app()->request->getParam("mode", "view_course") ?>"*/"edit_course",
				createTestUrl: "<?= Docebo::createLmsUrl('test/default/axCreateTest', array('course_id' => $courseModel->getPrimaryKey())) ?>",
				createPollUrl: "<?= Docebo::createLmsUrl('poll/default/axCreatePoll', array('course_id' => $courseModel->getPrimaryKey())) ?>",
				videoExtensionsWhitelist: "<?php echo  implode(',', $whitelistArray) ?>",
				isMobileCourse: <?= ($isMobile ? 'true' : 'false') ?>,
				reloadSocialRatingUrl: "<?= Yii::app()->controller->createUrl('axReloadSocialRatingPanel', array('courseId' => $courseModel->getPrimaryKey()))?>"
			});

			Arena.centralRepo.globalListeners();

            $('#player-manage-add-lo-button').on('click', '.lo-authoring', function(e){
                e.preventDefault();
                var url = Docebo.rootAbsoluteBaseUrl + '/authoring/index.php?r=main/create'
                    +'&id_course=' + Arena.course_id
                    +'&id_parent=' + Arena.learningObjectsManagement.getActiveFolderKey(true);
                window.location.href = url;
            });
		});
	</script>


</div>