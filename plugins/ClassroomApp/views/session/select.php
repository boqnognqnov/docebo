<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $form CActiveForm */
/* @var $courseName string */

$this->breadcrumbs[] = Docebo::ellipsis($courseName, 60, '...');

?>

<?php
$this->widget('player.components.widgets.CourseHeader', array(
	'course_id' => $this->getIdCourse(),
));

$event = new DEvent($this, array('courseId' => $this->_courseModel->idCourse));
Yii::app()->event->raise('RenderAdditionalCourseInfo', $event);
?>

<div class="well classroom-sessions-grid-container">
	<div class="inner">
		<h3 class="title-bold"><?= Yii::t('classroom', 'Select session') ?></h3>

		<p><?= Yii::t('classroom', 'You are enrolled in {count} different sessions for this course.', array('{count}' => CHtml::tag('strong', array(), $dataProvider->getTotalItemCount()))) ?></p>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'classroom-sessions-grid-form',
			'method' => 'POST',
			'htmlOptions' => array(
				'class' => 'docebo-form'
			),
		)); ?>

		<div class="classroom-sessions-grid">
			<?php $this->widget('DoceboCGridView', array(
				'id' => 'classroom-sessions-grid',
				'htmlOptions' => array('class' => 'grid-view'),
				'dataProvider' => $dataProvider,
				'columns' => array(
					array(
						'type' => 'raw',
						'value' => function($data, $index) {
							$params = array("value" => $data["id_session"]);
							//if ($data['waiting'] > 0) { // NOTE: "waiting" column is deprecated for "lt_courseuser_session" table. Use "status" instead.
							//	$params['disabled'] = true;
							//} else {
								switch ($data['status']) {
									case LtCourseuserSession::$SESSION_USER_WAITING_LIST:
									case LtCourseuserSession::$SESSION_USER_CONFIRMED:
									case LtCourseuserSession::$SESSION_USER_SUSPEND:
										$params['disabled'] = true;
										break;
								}
							//}
							return CHtml::radioButton("courseSessionId", false, $params);
						},
						'name' => 'id',
						'header' => '',
						'htmlOptions' => array(
							'class' => 'text-center'
						)
					),
					array(
						'type' => 'raw',
						'name' => 'name',
						'value' => '$data->ltCourseSession->name',
						'header' => Yii::t('standard', '_NAME')
					),
					array(
						'type' => 'raw',
						'name' => 'start',
						'value' => '$data->ltCourseSession->date_begin',
						'header' => Yii::t('standard', '_START')
					),
					array(
						'type' => 'raw',
						'name' => 'end',
						'value' => '$data->ltCourseSession->date_end',
						'header' => Yii::t('standard', '_END')
					),
					array(
						'type' => 'raw',
						'name' => 'location',
						'value' => '$data->ltCourseSession->renderLocations();',
						'header' => Yii::t('classroom', 'Location name'),
					),
					array(
						'type' => 'raw',
						'name' => 'status',
						'value' => array($this, 'sessionsSelectGridRenderStatus'),
						'header' => Yii::t('standard', '_STATUS'),
						'headerHtmlOptions' => array(
							'class' => 'text-center'
						),
						'htmlOptions' => array(
							'class' => 'text-center'
						)
					),
				),
				'template' => '{items}{pager}',
				'pager' => array(
					'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
				),
				'afterAjaxUpdate' => 'function(id, data){
           $(\'.classroom-sessions-grid a[rel="tooltip"]\').tooltip();
           $(\'.classroom-sessions-grid span[rel="tooltip"]\').tooltip();
           $(document).controls();
         }'
			)); ?>
		</div>

		<br>

		<div class="text-right">
			<input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>"/>
			<a href="<?= Yii::app()->createUrl('site/index') ?>"
				 class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
		</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		var $form = $('#classroom-sessions-grid-form');
		$form.find(':submit').addClass('disabled');
		$form.find(':radio').change(function () {
			$form.find(':submit').removeClass('disabled')
		});
		$form.submit(function (e) {
			if (!$form.find(':radio:checked').length) {
				e.preventDefault();
			}
		});
		$form.find(':radio').styler();
	});
</script>