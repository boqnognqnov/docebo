<?php
/* @var $this SessionController */
/* @var $form CActiveForm */
/* @var $sessionId string */
?>

<h1><?= Yii::t('standard', '_CONFIRM') ?></h1>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'session-approve-users-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'ajax docebo-form'
    ),
));

echo CHtml::hiddenField('id_session', $sessionId);
echo CHtml::hiddenField('selected_users', '');

?>

<p><?= Yii::t('classroom', 'Approve {count} user(s)?', array('{count}'=>CHtml::tag('strong', array('id'=>'count-placeholder'), ''))) ?></p>

<div>
    <label for="" class="checkbox" style="margin-left: 0">
        <span style="margin-right: 10px;"><input type="checkbox"/></span> <?= Yii::t('standard', 'Yes, I confirm I want to proceed') ?>
    </label>
</div>


    <div class="form-actions">
        <input class="btn-docebo green big btn-submit disabled" type="submit" value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
        <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
    </div>

<?php $this->endWidget(); ?>