<?php
/* @var $model LtCourseSession */
/* @var $this SessionController */
$locations = $this->getLocationsArray();
$locationIds = array_keys($locations);
?>
<h1>
	<i class='i-sprite new-date-small white'></i>
	<?= (!$model->getPrimaryKey() ? Yii::t('classroom', 'New session') : Yii::t('classroom', 'Edit session')) ?>
</h1>
<div class="edit-session-tabs">
	<ul class="nav nav-tabs" id="sessionFormTab">
		<li class="active">
			<a data-target="#sessionForm-details" href="#">
				<span class="user-form-details"></span><?= Yii::t('standard', '_DETAILS') ?>
			</a>
		</li>

		<li>
			<a data-target="#sessionForm-evaluation" href="#">
				<span
					class="user-form-evaluation"></span><?= Yii::t('classroom', 'Evaluation') . '/' . Yii::t('classroom', 'Completion') ?>
			</a>
		</li>

		<li>
			<a data-target="#sessionForm-dates" href="#">
				<span class="session-calendar-icon"></span><?= Yii::t('classroom', 'Session date(s)') ?>
			</a>

			<div id="session-dates-calendar"></div>
			<div class="popover bootstro fade bottom in" id="popup_bootstroAddAnotherDate" style="display: none;">
				<div class="popover-inner">
					<div class="popover-content">
						<div class="text-message"><span
								class="text-colored green"><?= Yii::t('classroom', 'Select a day') ?></span> <?= Yii::t('classroom', 'to add new date') ?>
						</div>
						<div class="bootstro-arrow arrow-nw"></div>
					</div>
				</div>
			</div>
		</li>
	</ul>

	<div class="form new-session-form clearfix">
		<div class="tab-content" id="sessionFormTabContent">
			<!-- details page -->
			<?php
			/* @var $form CActiveForm */
			$form = $this->beginWidget('CActiveForm', array(
				'id' => 'session_form',
				'htmlOptions' => array('class' => 'ajax'),
				'action' => $actionUrl
			));

			echo $form->hiddenField($model, 'course_id');
			echo $form->hiddenField($model, 'id_session');
			?>
			<div class="tab-pane active session-form-details-tab-content" id="sessionForm-details">
				<div class="mandatory">* <?= Yii::t('standard', '_MANDATORY') ?></div>
				<div class="row-fluid">
					<div class="span12">
						<div class="clearfix">
							<?php
							echo CHtml::label(Yii::t('standard', '_NAME') . ' *', CHtml::activeId($model, 'name'));
							echo $form->textField($model, 'name', array('class' => 'session-name'));
							?>
							<div id="session-name-error-message" class="errorMessage"></div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<div class="clearfix">
							<?php
							echo CHtml::label(Yii::t('classroom', 'Maximum enrollments') . ' *', CHtml::activeId($model, 'max_enroll'), array('class' => 'session-enroll'));
							echo $form->textField($model, 'max_enroll', array('class' => 'session-enroll'));
							?>
							<div id="max-enroll-error-message" class="errorMessage"></div>
						</div>
					</div>
					<div class="span6" style="float: right; text-align: right;">
						<div class="clearfix">
							<?php
							echo CHtml::label(Yii::t('classroom', 'Minimum enrollments'), CHtml::activeId($model, 'min_enroll'), array('class' => 'session-enroll'));
							echo $form->textField($model, 'min_enroll', array('class' => 'session-enroll'));
							?>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<table>
						<tbody>
						<tr>
							<td><?php echo CHtml::activeLabel($model, 'last_subscription_date'); ?></td>
							<td>
								<?php echo CHtml::textField(
									CHtml::activeName($model, 'last_subscription_date'),
									(!empty($model->last_subscription_date) && $model->last_subscription_date != '0000-00-00 00:00:00'
										? Yii::app()->localtime->stripTimeFromLocalDateTime($model->last_subscription_date)
										: ''),
									array(
										'class' => 'datepicker',
										'id' => CHtml::activeId($model, 'last_subscription_date'),
										'data-date-format' => Yii::app()->localtime->dateStringToBDatepickerFormat(Yii::app()->localtime->getPHPLocalDateFormat('short')),
										'data-date-language' => Yii::app()->getLanguage(),
								)); ?>
								<i class="p-sprite calendar-black-small date-icon"></i>
							</td>
						</tr>
						</tbody>
					</table>
				</div>

				<div class="max-enrollement-message single-action">
					<span class="exclamation"></span>
					<?= Yii::t('classroom', 'Don\'t forget to double check the classrooms capacity when you set up maximum enrollments') ?>
				</div>

				<div class="row-fluid">
					<div class="span12">
						<div class="clearfix">
							<?php echo CHtml::label(Yii::t('classroom', 'Other info'), CHtml::activeId($model, 'other_info')); ?>
							<div class="textarea-wrapper">
								<?php echo $form->textArea($model, 'other_info', array('id' => 'tiny_' . $uniqueId, 'class' => 'session-other_info')); ?>
							</div>
						</div>
					</div>
				</div>

				<div class="session-form-actions">
					<input class="btn close-btn cancel-session-modal" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
					<input class="btn confirm-btn submit-session-button" type="button" value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
					<span class="session-error"></span>
				</div>
			</div>

				<!-- evaluation page -->
				<div class="tab-pane session-form-evaluation-tab-content" id="sessionForm-evaluation">

					<div class="row-fluid">
					 <div class="span12">

							<!-- evaluation by online test -->
							<div>
								<div class="radio-list-element session-form-evaluation-spacer">
									<?= $form->radioButton($model, 'evaluation_type', array('id' => 'evaluation-score-type-online', 'value' => LtCourseSession::EVALUATION_TYPE_ONLINE, 'uncheckValue' => null)) ?>
								</div>
								<div class="radio-list-element session-form-evaluation-label">
									<?php
									echo CHtml::label(Yii::t('classroom', 'Online test'), 'evaluation-score-type-online', array());
									?>
								</div>
							</div>

							<!-- evaluation by manual assignment -->
							<div>
								<div class="radio-list-element session-form-evaluation-spacer">
									<?= $form->radioButton($model, 'evaluation_type', array('id' => 'evaluation-score-type-score', 'value' => LtCourseSession::EVALUATION_TYPE_SCORE, 'uncheckValue' => null)) ?>
								</div>
								<div class="radio-list-element session-form-evaluation-label">
									<?php
									echo CHtml::label(Yii::t('classroom', 'Evaluation score base'), 'evaluation-score-type-score', array());
									?>
								</div>
							</div>
							<div id="evaluation-score-base-option" style="display:<?= $model->evaluation_type == LtCourseSession::EVALUATION_TYPE_SCORE ? 'block' : 'none' ?>">
								<div class="radio-list-element session-form-evaluation-spacer">
									&nbsp;
								</div>
								<div class="radio-list-element">
									<?php
									$_inputHtmlParams = array('class' => 'session-score-base');
									if ($model->isNewRecord && isset(Yii::app()->params['classroom-default-session-score_base'])) {
										$_inputHtmlParams['value'] = (int)Yii::app()->params['classroom-default-session-score_base'];
									}
									echo $form->textField($model, 'score_base', $_inputHtmlParams);
									?>
								</div>
								<div class="radio-list-element text-description">
									<?= Yii::t('classroom', 'If you assign 100, for example, you\'ll manage evaluation sheet scores in this format: xx/100') ?>
								</div>
							</div>

						</div>
					</div>

					<!-- action buttons -->
					<div class="session-form-actions">
						<input class="btn close-btn cancel-session-modal" type="button" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input class="btn confirm-btn submit-session-button" type="button" value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
						<span class="session-error"></span>
					</div>

				</div>
				<!-- end evaluation page -->

			<!-- session dates page -->
			<div class="tab-pane" id="sessionForm-dates">
				<!-- New date page -->
				<div id="no-date-form">
					<h2></h2>

					<div class="add-date-box">
						<p><?= Yii::t('classroom', 'No time available') ?></p>
						<input class="btn confirm-btn" type="button" value="<?php echo Yii::t('standard', '_ADD'); ?>"
									 id="session-date-add"/>
					</div>

					<div class="popover bootstro fade bottom in" id="popup_bootstroAddSessionDate"
							 style="width: 300px; max-width: 300px; top: 156px; right: 50px; display: block;">
						<div class="popover-inner">
							<div class="popover-content">
								<div class="text-message"><span
										class="text-colored green"><?= Yii::t('standard', '_ADD') ?></span> <?= Yii::t('course', '_PARTIAL_TIME') ?>
								</div>
								<div class="bootstro-arrow arrow-nw"></div>
							</div>
						</div>
					</div>

					<div class="session-form-actions">
						<input class="btn close-btn cancel-session-modal" type="button"
									 value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input class="btn confirm-btn submit-session-button" type="button"
									 value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
						<span class="session-error"></span>
					</div>
				</div>
				<!-- end new date page -->

				<!-- Edit date page -->
				<div id="edit-date-form" class="date-forms" style="display: none;">
					<div class="mandatory">* <?= Yii::t('standard', '_MANDATORY') ?></div>
					<div class="date-line">
						<table>
							<tbody>
							<tr>
								<td><?php echo CHtml::label(Yii::t('standard', '_DATE') . " *", 'day'); ?></td>
								<td><?php echo CHtml::textField('day', '', array('class' => 'datepicker edit-day-input', 'id' => 'day-calendar', 'readonly' => 'readonly')); ?>
									<i class="p-sprite calendar-black-small date-icon"></i>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('standard', '_NAME'), 'name');
								echo CHtml::textField('name', '', array('id' => 'day-name', 'style' => 'width: 97%'));
								?>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<table id="day-times">
									<tbody>
									<tr>
										<td colspan="4"><?= Yii::t('report', '_TOTAL_SESSION') ?> *</td>
										<td colspan="5"><?= Yii::t('classroom', 'Break') ?></td>
									</tr>
									<tr> <!-- Added 'onchange' => 'validateHhMm(this)' to call the function from validateSessionForm.js -->
										<td><?= CHtml::textField('time_begin', '', array('id' => 'time-begin', 'class' => 'time', 'onkeyup' => 'validateHhMm(this)', 'style' => 'width: 70px')) ?></td>
										<td class="separator"> -</td>
										<td><?= CHtml::textField('time_end', '', array('id' => 'time-end', 'class' => 'time', 'onkeyup' => 'validateHhMm(this)', 'style' => 'width: 70px')) ?></td>
										<td class="space">&nbsp;</td>
										<td><?= CHtml::textField('break_begin', '', array('id' => 'break-begin', 'class' => 'break', 'onkeyup' => 'validateHhMm(this)', 'style' => 'width: 70px')) ?></td>
										<td class="separator"> -</td>
										<td><?= CHtml::textField('break_end', '', array('id' => 'break-end', 'class' => 'break', 'onkeyup' => 'validateHhMm(this)', 'style' => 'width: 70px')) ?></td>
										<td class="space">&nbsp;</td>
										<td>
											<p id="day-timezone-label"></p>
											<a href="#" id="select-timezone-button">
												<?= Yii::t('classroom', 'Different timezone') ?>?
											</a>
										</td>
									</tr>
									<tr>
										<td colspan="9">
											<div id="time-error-message" class="errorMessage"></div>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row-fluid" id="timezone-line" style="display: none">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('classroom', 'Select Timezone'), 'timezone');
								echo CHtml::dropDownList('timezone', '', $this->getTimezonesArray(), array('id' => 'day-timezone'));
								?>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('classroom', 'Location') . " *", 'id_location');
								echo CHtml::dropDownList('id_location', '', $locations, array('id' => 'day-location'));
								?>
								<div id="day-location-error" class="errorMessage"></div>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<?php
								echo CHtml::label(Yii::t('standard', '_CLASSROOM'), 'id_classroom');
								echo CHtml::dropDownList('id_classroom', '', array(), array('id' => 'day-classroom'));
								?>
							</div>
						</div>
					</div>

					<div class="session-warning">
						<div class="icon"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></div>
						<div class="message"></div>
					</div>

					<div id="classroom-availability-info-container" style="display:none;" class="row-fluid">
						<div class="span12">
							<div class="clearfix">
								<div id="classroom-availability-info">
									<!-- this will be filled with info by javascript code -->
								</div>
								<div id="classroom-availability-loading" style="display:none;">
									<span class="menu-loading"></span>Loading ...
								</div>
							</div>
						</div>
					</div>

					<div class="session-form-actions">
						<input id="cancel-edit-session-date" class="btn close-btn cancel-session-modal" type="button"
									 value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
                        <input id="create-session-date" class="btn confirm-btn" type="button"
                               value="<?php echo Yii::t('classroom', 'Create session date'); ?>" name="submit"/>
					</div>
				</div>
				<!-- end End date page -->

				<!-- View date page -->
				<div id="view-date-form" class="date-forms" style="display: none;">
					<h2></h2>

					<div class="day-title clearfix"><i></i>

						<div></div>
					</div>
					<div class="day-time-line clearfix">
						<i></i>

						<h3><?= Yii::t('classroom', 'What time?'); ?></h3>

						<div></div>
					</div>
					<div class="day-location clearfix">
						<i></i>

						<h3><?= Yii::t('classroom', 'Where?'); ?></h3>

						<div></div>
					</div>

					<div class="view-date-form-actions">
						<input id="make-copy-date" class="btn confirm-btn" type="button"
									 value="<?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>"/>
						<input id="edit-session-date" class="btn edit-btn" type="button"
									 value="<?php echo Yii::t('standard', '_MOD'); ?>"/>
						<input id="delete-session-date" class="btn close-btn" type="button"
									 value="<?php echo Yii::t('standard', '_DEL'); ?>"/>

						<div id="copy-date-box" style="display: none">
							<div class="arrow"></div>
							<table>
								<tbody>
								<tr>
									<td><?= Yii::t('classroom', 'Copy this session date to') ?>&nbsp;</td>
									<td><?php echo CHtml::textField('destination-day', '', array('class' => 'datepicker edit-day-input', 'id' => 'destination-day', 'readonly' => 'readonly')); ?>
										<i class="p-sprite calendar-black-small date-icon"></i>
									</td>
									<td style="text-align: right; width: 35%;">
										<input id="confirm-copy-session-date" class="btn confirm-btn" type="button"
													 value="<?php echo Yii::t('standard', '_MAKE_A_COPY'); ?>" name="submit"/>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="session-form-actions">
						<input class="btn close-btn cancel-session-modal" type="button"
									 value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
						<input class="btn confirm-btn submit-session-button" type="button"
									 value="<?php echo Yii::t('standard', '_SAVE'); ?>"/>
						<span class="session-error"></span>
					</div>
				</div>
			</div>
			<!-- end dates page -->

			<?php $this->endWidget(); ?>
			<!-- end details page -->
		</div>
	</div>
</div>
<!-- Form loader -->
<span class="loader" style="display: none;"></span>

<script type="text/javascript">
	/* ===  Onload event handler  === */
	$(function () {
		$('#<?php echo CHtml::activeId($model, 'last_subscription_date');?>').bdatepicker();

		SessionManager.init({
			sessionDates: <?= $this->getSessionDatesAsJson($model) ?>,
			months: <?=CJSON::encode(TimeHelpers::getLocalizedMonthNames())?>,
			classrooms: <?= $this->getClassroomsAsJson() ?>,
			locations: <?= $this->getLocationsAsJson() ?>,
			timezoneOffsets: <?= $this->getTimezoneOffsetsAsJson() ?>,

			currentLanguage: '<?= Yii::app()->session['current_lang'] ?>',
			currentTimezone: '<?= $this->getCurrentTimezone() ?>',
			idLocation: <?= (!empty($locations)) ? $locationIds[0] : '0' ?>,
			checkClassroomAvailabilityUrl: '<?php
				$_urlParams = array('course_id' => $model->course_id);
				if (!$model->isNewRecord) { $_urlParams['id_session'] = $model->getPrimaryKey(); }
				echo Docebo::createLmsUrl('ClassroomApp/session/checkClassroomAvailability', $_urlParams);
			?>',
			checkClassroomAvailability:'<?php
				$model = AttendanceSheetMainField::model()->findByPk(1);
				echo $model->prevent_session_overlap == 1;
			?>',
			isGodAdmin:'<?php echo Yii::app()->user->isGodAdmin; ?>',
			translations: <?= CJSON::encode(array(
				'_SAVE' => Yii::t('standard', '_SAVE'),
				'_SET_UP_DATES' => Yii::t('classroom', 'Set up dates'),
				'_MISSING_SESSION_DATE' => Yii::t('classroom', 'You can\'t save unless you <br/><span class="bold">set up at least one session date</span>'),
				'_OPERATION_FAILURE' => Yii::t('standard', '_OPERATION_FAILURE'),
				'_SESSION_NAME_IS_REQUIRED' => Yii::t('classroom', 'Session name is required'),
				'_ENTER_A_NUMBER_GREATER_THAN_0' => Yii::t('classroom', 'Enter a number greater than 0'),
				'_INVALID_OR_MISSING_DATE_HOURS' => Yii::t('classroom', 'Invalid or missing date hours'),
				'_PLEASE_CHOOSE_A_LOCATION' => Yii::t('classroom', 'Please choose a location for this session date'),
				'_NO_CLASSROOM_SELECTED' => Yii::t('classroom', 'No classroom selected'),
				'_SEATS' => strtolower(Yii::t('standard', 'Available seats')),
				'_UPDATE_SESSION_DATE' => Yii::t('classroom', 'Update session date'),
				'_CREATE_SESSION_DATE' => Yii::t('classroom', 'Create session date')
			)) ?>
		});
	});

    $( "input.datepicker#LtCourseSession_last_subscription_date").bdatepicker({ // Target last_subscription_date DatePicker in sessionForm
        startDate: new Date()  // Disable pick of past days ( < Today Date )
    }).attr("readonly","readonly"); // Forche choosing the BDatePicker
    $("input#day-calendar").bdatepicker({
        startDate: new Date()
    });
    $(".time, .break").bind('keyup', ValidateHour); // Validate the hours and do not allow more than 5 symbols
</script>
