<?php
/* @var $course LearningCourse */

//$this->breadcrumbs[Yii::t('menu', 'Course')] = Docebo::createAdminUrl('courseManagement/index');
$this->breadcrumbs[] = Yii::t('classroom', 'Sessions Management');

$this->renderPartial('/_common/_courseHeader', array('courseModel' => $course));

?>

<h3 class="main-actions title-bold back-button">
	<?php echo CHtml::link(Yii::t('standard', '_BACK'), Yii::app()->createUrl('courseManagement/index')); ?>
	<span><?=$course->name?></span>
</h3>

<?php
$this->widget('plugin.ClassroomApp.widgets.SessionManagement', array(
    'id' => 'session-management',
    'courseModel' => $course
));

?>