<h1><?= Yii::t('standard', '_MAKE_A_COPY') ?></h1>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'copy-session-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>
	
	<input type="hidden" name="confirm" value="1" />
	<input type="hidden" name="course_id" value="<?=$idCourse?>" />
	<input type="hidden" name="id_session" value="<?=$idSession?>" />

    <div class="control-group">
        <div class="controls">
            <?= CHtml::label(Yii::t('classroom', 'Assign a new name to this session'), 'new-name-input'); ?>
            <?= CHtml::textField('new-name', '', array('id' => 'new-name-input')); ?>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <?php
            echo CHtml::checkBox('duplicate-dates', false, array('id' => 'duplicate-dates-input'));
            echo CHtml::label(Yii::t('classroom', 'Duplicate session dates'), 'duplicate-dates-input', array('style' => 'display: inline-block; padding-left: 15px;'));
            ?>
        </div>
    </div>

    <table>
        <tbody>
            <tr>
                <td><?= CHtml::label(Yii::t('classroom', 'How many copies?'), 'num-copies-input'); ?></td>
                <td><?php $_numCopiesList = array();
                    for ($i=1; $i<=10; $i++)
                        $_numCopiesList[$i] = $i;
                    echo CHtml::dropDownList('num-copies', LtCourseSession::REPEAT_DAILY, $_numCopiesList, array('id' => 'num-copies-input', 'style' => 'width: 60px;'));?>
                </td>
            </tr>
            <tr>
                <td><?= CHtml::label(Yii::t('classroom', 'Repeats'), 'repeats-input'); ?></td>
                <td><?php
                    $_typesList = array(
                        LtCourseSession::REPEAT_DAILY => Yii::t('classroom', 'Daily'),
                        LtCourseSession::REPEAT_WEEKLY => Yii::t('dashboard', '_THIS_WEEK'),//Yii::t('classroom', 'Weekly'),
                        LtCourseSession::REPEAT_MONTHLY => Yii::t('classroom', 'Monthly'),
                        LtCourseSession::REPEAT_YEARLY => Yii::t('classroom', 'Yearly'),
                    );
                    echo CHtml::dropDownList('repeats', 1, $_typesList, array('id' => 'repeats-input', 'style' => 'width: 150px;', 'disabled' => 'disabled'));
                    ?>
                </td>
            </tr>
            <tr>
                <td><?= CHtml::label(Yii::t('classroom', 'Repeat every'), 'num-repeats-input'); ?></td>
                <td><?php
                    $_numRepeatsList = array();
                    for ($i=1; $i<=10; $i++)
                        $_numRepeatsList[$i] = $i;
                    echo CHtml::dropDownList('num-repeats', 1, $_numRepeatsList, array('id' => 'num-repeats-input', 'style' => 'width: 60px;', 'disabled' => 'disabled'));
                    echo '<span id="repeat-type-def"></span>'; ?>
                </td>
            </tr>
        </tbody>
    </table>

	<div class="form-actions">
		<?php echo CHtml::submitButton(Yii::t('standard','_CONFIRM'), array('class' => 'btn confirm-btn')); ?>
		<?= CHtml::button(Yii::t('standard', '_CANCEL'), array('class' => 'btn btn-cancel close-dialog')); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">

$('#duplicate-dates-input').on('change', function(e) {
	if ($(this).attr('checked')) {
		$('#repeats-input').removeAttr('disabled');
		$('#num-repeats-input').removeAttr('disabled');
	} else {
		$('#repeats-input').attr('disabled', "disabled");
		$('#num-repeats-input').attr('disabled', "disabled");
	}
});

$('#duplicate-dates-input').styler();
$(document).delegate(".modal-copy-session", "dialog2.content-update", function() {
	var e = $(this), autoclose = e.find("a.auto-close");
	if (autoclose.length > 0) {
        try {
            e.find('.modal-body').dialog2("close");
        } catch(e) {}

		$.fn.yiiGridView.update('sessions-management-grid', {
			data: $("#session-grid-search-form").serialize()
		});
	}
});
</script>