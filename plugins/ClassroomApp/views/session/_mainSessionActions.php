<div class="session-main-actions-container">
	<ul id="docebo-bootcamp-sessions" class="docebo-bootcamp clearfix">
	    <li>
	        <a id="add-new-session"
	           href="<?=Docebo::createAdminUrl('ClassroomApp/session/create', array('course_id' => $course->getPrimaryKey()))?>"
	           class="open-dialog"
	           data-dialog-class="modal-create-session"
	           data-helper-title="<?= Yii::t('classroom', 'New session') ?>"
	           data-helper-text="<?= Yii::t('helper', 'Create Session - lt') ?>">
	            <span class="classroom-sprite new-date"></span>
	            <span class="classroom-sprite new-date white"></span>
	            <h4><?= Yii::t('classroom', 'New session') ?></h4>
	        </a>
	    </li>
		<?php if(Yii::app()->user->getIsGodadmin() || (Yii::app()->user->getIsPu() && Yii::app()->user->checkAccess("enrollment/update"))): ?>
	    <li>
	        <a id="users-waiting-to-be-approved" 
						 href="<?=Docebo::createAdminUrl('ClassroomApp/session/assign', array('course_id' => $course->getPrimaryKey()))?>"
	           data-helper-title="<?php echo Yii::t('classroom', 'Users waiting to be assigned'); ?>"
	           data-helper-text="<?php echo Yii::t('helper', 'Unassigned Users - lt'); ?>">
	            <span class="session-waiting-approval"></span>
	            <span class="session-waiting-approval white"></span>
	            <h4><?php echo Yii::t('classroom', 'Users waiting to be assigned'); ?></h4>
	        </a>
	    </li>
		<?php endif; ?>
	    <li class="helper">
	        <a href="#">
	            <span class="i-sprite is-circle-quest large"></span>
	            <h4 class="helper-title"></h4>
	            <p class="helper-text"></p>
	        </a>
	    </li>
	
	</ul>
</div>