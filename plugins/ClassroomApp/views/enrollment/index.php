<?php
/** @var $courseModel LearningCourse */
/** @var $sessionModel LtCourseSession */

$course_associated = CoreUserPuCourse::model()->countByAttributes(array('puser_id' => Yii::app()->user->id, 'course_id' => $courseModel->getPrimaryKey())) ? true : false;
$pUserRights = Yii::app()->user->checkPURights($courseModel->idCourse);

$backUrl = Docebo::createAppUrl('lms:player/training/session', array('course_id' => $courseModel->getPrimaryKey()));

//check enrollment permissions
$canEnroll = false;
if (Yii::app()->user->isGodAdmin) {
	$canEnroll = true;
} else if (Yii::app()->user->isPu) {
	$isPuWithCourseEnrollPerm = $pUserRights->all && Yii::app()->user->checkAccess('enrollment/view');
	$isPuWithSessionAddPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add");
	$isPuWithSessionEditPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/mod");
	$isPuWithSessionAssignPerm = $pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/assign");
	$isSessionOwnedByPU = ($sessionModel->created_by == Yii::app()->user->getIdst());
	//check PU permissions
	if ($course_associated) {
		if ($isPuWithCourseEnrollPerm) {
			if ($isSessionOwnedByPU) {
				$canEnroll = ($isPuWithSessionAddPerm || $isPuWithSessionEditPerm );
			} else {
				$canEnroll = ($isPuWithSessionEditPerm);
			}
		}
	}
}

// don't allow PU to enroll users to a course, that belongs to one or more learning plans
$disallowPUEnrollCoursesWithLPs = 0; //default allow PU to enroll users to courses, that belongs to one or more LPs

// Show breadcrumbs
$this->breadcrumbs[] = Docebo::ellipsis($courseModel->name, 60, '...');
$this->breadcrumbs[Yii::t('classroom', 'Sessions and Enrollments')] = $backUrl;
$this->breadcrumbs[] = Yii::t('standard', 'Enrollments');

// Show course header
$this->widget('lms.protected.modules.player.components.widgets.CourseHeader', array(
	'course' => $courseModel
));

?>
<div class="main-actions clearfix">
	<h3 class="title-bold back-button">
		<?php echo CHtml::link(Yii::t('standard', '_BACK'), $backUrl); ?>
		<span><?=$sessionModel->name?></span>
	</h3>
	<?php

	Yii::app()->event->raise('OnSelectedSessionEnrollmentRender',
		new DEvent($this, array(
			'disallowPUEnrollCoursesWithLPs' => &$disallowPUEnrollCoursesWithLPs,
			'model' => $courseModel
		)));

	if (Yii::app()->user->isGodAdmin || ($canEnroll && !$sessionModel->lastSubscriptionDatePassed() && !$disallowPUEnrollCoursesWithLPs) || (isset($isPuWithSessionAssignPerm) && $isPuWithSessionAssignPerm && !$sessionModel->lastSubscriptionDatePassed() && !$disallowPUEnrollCoursesWithLPs))
		$this->renderPartial('_mainEnrollmentActions', array(
			'courseModel' => $courseModel,
			'sessionModel' => $sessionModel
		));
	?>
</div>

<div class="bootstroEnrollUsers-hide-content">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'session-enrollment-form',
		'htmlOptions' => array('class' => 'ajax-list-form', 'data-grid' => '#session-enrollment-list'),
	)); ?>

	<div class="main-section course-enrollments">
		<div class="filters-wrapper">
			<table class="filters">
				<tbody>
				<tr>
					<td class="table-padding">
						<table class="table-border">
							<tr>
								<td class="group">
									<div class="session-enrollments-order-by">
										<label for="order-session-enrollments"><?= Yii::t('social', 'Sort by'); ?>:</label>
										<?php echo CHtml::dropDownList('order_by', $orderByInput, array( Yii::t('standard', '_SELECT') ) + LearningCourseuser::getSessionEnrollmentsOrderList(), array('id' => 'order-session-enrollments')); ?>
									</div>
								</td>
								<td class="group seats">
								</td>
								<td class="group">
									<table>
										<tr>
											<td>
												<div class="input-wrapper">
													<input id="search-enrolled-users" type="text" name="search_input"
                                                           placeholder="<?php echo Yii::t('standard', '_SEARCH') ?>" />
													<span>
														<button class="search-btn-icon"></button>
													</span>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php echo CHtml::submitButton('', array('style' => 'display: none;')); ?>
	<?php $this->endWidget(); ?>

	<?php if ($canEnroll): ?>
	<div class="selections filter-border clearfix" data-grid="session-enrollment-list">
		<?php
			$this->widget('ext.local.widgets.ListViewSelectAll', array(
				'gridId' => 'session-enrollment-list',
				'class' => 'left-selections clearfix',
				'dataProvider' => $sessionModel->dataProviderEnrollment(),
				'itemValue' => 'idst',
			));
		?>
		<div class="right-selections clearfix">
			<select name="massive_action" id="session-enrollments-massive-action" data-id-session="<?=$sessionModel->getPrimaryKey()?>" data-id-course="<?=$courseModel->idCourse?>">
				<option value=""><?php echo Yii::t('standard', 'Select action'); ?></option>
				<option value="delete"><?php echo Yii::t('standard', 'Uneroll'); ?></option>
				<option value="mass-email"><?=Yii::t('course', 'Send Email')?></option>
			</select>
		</div>
	</div>
	<?php endif; ?>

	<?php
	$this->renderPartial('/_common/_sessionInfo', array(
		'title' => Yii::t('course', 'Users enrolled to', array('{course_name}' => $sessionModel->name)),
		'sessionModel' => $sessionModel
	));
	?>

	<div class="session-list-view">
		<?php 
			$this->widget('ext.local.widgets.CXListView', array(
				'id' => 'session-enrollment-list',
				'htmlOptions' => array('class' => 'list-view clearfix'.(!$canEnroll ? ' readonly' : '')),
				'dataProvider' => $sessionModel->dataProviderEnrollment($searchInput, $orderByInput),
				'itemView' => '/_common/_viewEnroll',
				'itemsCssClass' => 'items clearfix',
				'summaryText' => Yii::t('standard', '_TOTAL'),
				'pager' => array(
					//'class' => 'ext.local.pagers.DoceboLinkPager',
					'class' => 'common.components.DoceboCLinkPager',
				),
				'template' => '{items}{pager}{summary}',
				'ajaxUpdate' => 'session-enrollment-list-all-items',
				'beforeAjaxUpdate' => 'function(id, options) {
						options.type = "POST";
						if (options.data == undefined) {
							options.data = {
								selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
							}
						}
					}',
				'afterAjaxUpdate' => 'function(id, data) { '
					.' $("#session-enrollment-list input").styler(); '
					.' $(\'#session-enrollment-list\').controls(); '
					.' $(\'a[rel="tooltip"]\').tooltip(); '
					.' $.fn.updateListViewSelectPage(); '
					.'}',
				)
			); 
		?>
	</div>

</div>
<script type="text/javascript">
(function ($) {
	$(function () {

		$('input, select').styler();
		replacePlaceholder();

		$('.ajax-grid-form').find('input, select').live('change', function() {
			if ($(this).closest('.ajax-grid-form').length == 0) {
				return false;
			}
			$(this).closest('form').submit();
		});

		$('a[rel="tooltip"]').tooltip();

		$(document).on("change", "#order-session-enrollments", function(){
			$(this).closest('form').submit();
		});

		var jobType = <?= json_encode(md5(EnrollUsersToManyCourses::JOB_HANDLER)) ?>;
		var courseName = <?= json_encode($courseModel->name)?>;

		var lastUpdate = Math.floor(Date.now() / 1000);
		$(document).on('backgroundjobUpdate', function(event, hash, percent, type, nameParams) {
			if(type === jobType && typeof nameParams.course_name !== "undefined" && nameParams.course_name === courseName) {
				var nowUpdate = Math.floor(Date.now() / 1000);

				if((nowUpdate - lastUpdate) > 10){
					$.fn.yiiListView.update('session-enrollment-list');
					lastUpdate = nowUpdate;
				}
			}
		});

		$(document).on('backgroundjobFinished', function(event, hash, percent, type, nameParams){
			if(type === jobType && typeof nameParams.course_name !== "undefined" && nameParams.course_name === courseName) {
				$.fn.yiiListView.update('session-enrollment-list');
			}
		});

	});
})(jQuery);
</script>


<?php Yii::app()->event->raise('AfterSessionEnrollmentsGridRender', new DEvent($this, array(
	'course' => $courseModel
))); ?>