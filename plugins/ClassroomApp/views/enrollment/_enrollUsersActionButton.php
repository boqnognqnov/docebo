<div>
<?php 

	$title 	= '<span></span>'.Yii::t('standard', 'Enroll users');
	$url = Docebo::createLmsUrl('usersSelector/axOpen', array(
		'type' => UsersSelector::TYPE_CLASSROOM_ENROLLMENT,
		'idCourse' => $courseModel->getPrimaryKey(),
		'idSession' => $sessionModel->getPrimaryKey()
	));
	
	echo CHtml::link($title, $url, array(
			'data-dialog-class' => 'users-selector',
			'class' => 'open-dialog session-assign-users',
			'rel' 	=> 'users-selector',
			'alt' => Yii::t('helper', 'Enroll users to session - lt'),
			'data-dialog-title' => Yii::t('standard', 'Enroll users'),
			'title'	=> Yii::t('standard', 'Enroll users')
	));
	


?>

</div>


<script type="text/javascript">
	/**
 	 * The name of this function is hardcoded in UsersSelector JS as a callback upon successfully closing the dialog
 	 *  Use in on your discretion to update current UI
 	 */
	function usersSelectorSuccessCallback(data) {
		$.fn.yiiListView.update('session-enrollment-list');
	}


</script>