<script type='text/javascript'>
	$(function(){
		$.post('<?=Docebo::createLmsUrl('progress/run')?>', {
			'type': <?=Progress::JOBTYPE_ENROLL_USERS_TO_SESSION?>,
			'users': '<?= $users ?>',
			'idCourse': <?=$idCourse; ?>,
			'idSession': '<?= $session ?>'
			<?php
			if (isset($level) && !empty($level)) {
				echo ",\n'level': $level";
			}
			?>
		}, function(data){
			var dialogId = 'enroll-users-to-session-progressive';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?=Yii::t('course', 'Enroll users to session')?>'
			});
			$('#'+dialogId).html(data);
		});
	});
</script>
