<h3><?= Yii::t('course_management', '_ENROLLMENT_EDIT') ?></h3>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'edit-enrollment-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>

	<div class="clearfix">
		<?php echo $form->dropDownList($courseUserModel, 'level',
			LearningCourseuser::getLevelsArray(
				array(),
				array(LearningCourseuser::$USER_SUBSCR_LEVEL_COACH)
			)); ?>
		<?php echo $form->label($courseUserModel, 'level'); ?>
	</div>

	<div class="clearfix">
		<?php echo $form->dropDownList($sessionUserModel, 'status', LtCourseuserSession::getStatusesArray()); ?>
		<?php echo $form->label($sessionUserModel, 'status'); ?>
	</div>

	<input type="hidden" id="edit-enrollment-form-confirm" name="confirm" value="1" />

	<?php $this->endWidget(); ?>
</div>
<div class="modal-footer">
    <input class="btn confirm-btn" type="button" value="<?= Yii::t('standard','_CONFIRM') ?>" name="submit" data-submit-form="edit-enrollment-form">
    <input class="btn close-btn" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>

<script type="text/javascript">
$('select').styler();
</script>
