<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'import-users-from-session-form',
		'htmlOptions' => array(
			'class' => 'ajax',
		)
	)); ?>

	<?php
		echo CHtml::hiddenField('id_session', $idSession, array('id' => 'id_session-input'));
		echo CHtml::hiddenField('confirm', 1, array('id' => 'confirm-input'));
	?>

    <div class="copy-course-line">
		<?php echo CHtml::label(Yii::t('classroom', 'Session'), 'session-name-input'); ?>
		<input data-url="<?= Docebo::createAppUrl('lms:ClassroomApp/session/searchAutoCompleteImportFromSession') ?>"
			data-type="session" 
			data-course_id="<?php echo $idCourse; ?>" 
			data-id_session="<?php echo $idSession; ?>"
			class="typeahead" 
			id="advanced-search-session" 
			autocomplete="off" 
			type="text" 
			name="import_from"
			placeholder="<?php echo Yii::t('classroom', 'Search session') ?>" />
	</div>
    <div class="copy-course-line">
		<?php
			echo CHtml::label(Yii::t('standard', '_LEVEL'), 'select-level-input');
			echo CHtml::dropDownList('level', '', LearningCourseuser::getLevelsArray(array(), array(LearningCourseuser::$USER_SUBSCR_LEVEL_COACH)), array('id' => 'select-level-input'));
		?>
	</div>

	<?php $this->endWidget(); ?>
</div>

<div class="modal-footer">
    <input class="btn confirm-btn" type="button" value="<?= Yii::t('standard','_CONFIRM') ?>" name="submit" data-submit-form="import-users-from-session-form">
    <input class="btn close-btn" type="button" value="<?= Yii::t('standard', '_CANCEL') ?>">
</div>

<script type="text/javascript">
$('select').styler();
</script>
