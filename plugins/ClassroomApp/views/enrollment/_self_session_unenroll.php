<h1>
    <i class="i-sprite is-solid-exclam white large"></i>
    <?=Yii::t('standard', 'Uneroll') ?>
</h1>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'self-unsubscribe-form',
        'method' => 'POST',
        'htmlOptions' => array(
            'class' => 'docebo-form ajax'
        ),
    )); ?>

    <p><?php echo Yii::t('course', '_SELF_SESSION_UNSUBSCRIBE'); ?></p>

    <div class="form-actions text-right">
        <input class="btn-docebo green big" type="submit" name='confirm' value="<?php echo Yii::t('standard', '_CONFIRM'); ?>" />
        <input type="reset" class="close-dialog btn-docebo black big" value="<?php echo Yii::t('standard', '_CANCEL'); ?>" />
    </div>
    <?php $this->endWidget(); ?>
</div>