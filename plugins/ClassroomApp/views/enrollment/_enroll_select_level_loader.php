<div class="align-center" style="width: 36px; margin: 0 auto;">
	<img src="<?= Yii::app()->theme->baseUrl.'/images/loading_medium.gif' ?>">
</div>
<script type='text/javascript'>
	$(function(){

		$.post('<?= Docebo::createAdminUrl('ClassroomApp/enrollment/enrollUsersSelectLevel') ?>', {
			'users': '<?= $users ?>',
			'idCourse': <?=$idCourse; ?>,
			'idSession': '<?= $idSession ?>'
		}, function(data) {

			//close old selector dialog
			$('.modal.users-selector').find('.modal-body').dialog2("close");

			//prepare new selector
			var dialogId = 'enroll-users-to-session-select-level';

			$('<div/>').dialog2({
				autoOpen: true, // We will open the dialog later
				id: dialogId,
				title: '<?= Yii::t('standard', 'Enroll users') ?>',
				modalClass: 'users-level-selector'
			});

			var content = data.data;
			$('.modal.users-level-selector').find('.modal-footer').html(content.footer);
			$('#'+dialogId).html(content.body); //NOTE: the same as searching for .modal-body element

		}, 'json');
	});
</script>