<div>
	<?php

	$courseEnrollWidget = $this->widget('ext.local.widgets.ModalWidget', array(
		'type' => 'link',
		'modalClass' => 'course-enrollment',
		'handler' => array(
			'title' => '<span></span>' . Yii::t('standard', 'Enroll users'),
			'htmlOptions' => array(
				'class' => 'course-enrollment session-assign-users',
				'alt' => Yii::t('helper', 'Enroll users to session - lt'),
				'modal-title' => Yii::t('standard', '_SELECT_USERS'),
			),
		),
		'headerTitle' => Yii::t('standard', '_SELECT_USERS'),
		'url' => 'enrollment/enroll',
		'urlParams' => array(
			'course_id' => $courseModel->getPrimaryKey(),
			'id_session' => $sessionModel->getPrimaryKey()
		),
		'updateEveryTime' => true,
		'remoteDataType' => 'json',
		'buttons' => array(
			array(
				'title' => Yii::t('standard', '_ADD'),
				'type' => 'confirm',
				'class' => 'confirm-btn',
			),
			array(
				'title' => Yii::t('standard', '_CANCEL'),
				'type' => 'close',
				'class' => 'close-btn',
			),
		),
		'beforeShowModalCallBack' => 'applyTypeahead($(\'.course-enrollment .typeahead\'));',
	));

	?>
</div>
<script type="text/javascript">
	$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?> .confirm-btn').live('click', function () {
		var modal = $('#modal-<?php echo $courseEnrollWidget->uniqueId; ?> .modal-body');
		var form = modal.find("#select-user-form");

		/*******************************************************************************************************/
		// Handle Orgcharts/Branch selection. See  /admin/protected/views/courseManagement/enroll_user.php
		// The 'fancyTreeSelector' variable here must match the FancyTree ID in enroll_user.php
		var fancyTreeSelector = '#orgcharts-fancytree';
		if ($(fancyTreeSelector).length > 0) {
			// Generate <input> form fields for selected branch nodes
			// This is a standard method of the Docebo FancyTree widget!
			// Form ID is set in enroll_user.php
			$(fancyTreeSelector).fancytree("getTree").ext.docebo._generateFormElements(false);
		}
		/*******************************************************************************************************/


		if (form.data('grid-items') == true) {
			form.find('div.grid-view').each(function () {
				var selectedItems = $('#' + $(this).prop('id') + '-selected-items');
				if (selectedItems.length == 0) {
					selectedItems = $('#' + $(this).prop('id') + '-selected-items-list').clone();
					selectedItems.prop('name', $(this).prop('id') + '-selected-items')
						.prop('id', $(this).prop('id') + '-selected-items');
					form.append(selectedItems);
				} else {
					selectedItems.val($('#' + $(this).prop('id') + '-selected-items-list').val());
				}
			});
		}

		var formData = form.serialize();

		$.ajax({
			'dataType': 'json',
			'type': 'POST',
			'url': modal.find('#select-user-form').attr('action'),
			'cache': false,
			'data': formData,
			'success': function (data) {
				modal.html(data.html);
				if (data.modalClass !== undefined) {
					$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?>').addClass(data.modalClass);
				}
				if (data.modalTitle !== undefined) {
					$('#label-<?php echo $courseEnrollWidget->uniqueId; ?>').html('<span></span>' + data.modalTitle);
				}
				applyTypeahead(modal.find('.typeahead'));
				modal.find('input').styler();
				modalPosition();

				if (data.status == 'saved') {
					$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?>').modal('hide');
					$.fn.yiiListView.update('session-enrollment-list');
					Docebo.Feedback.hide();
				} else if (data.message) {
					// An error occurred -> Show flash message
					Docebo.Feedback.show('error', data.message);
					$('#modal-<?php echo $courseEnrollWidget->uniqueId; ?>').modal('hide');
				}
			}
		});
	});
</script>
