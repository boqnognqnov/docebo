<script type="text/javascript">
    // !!!This callback must be defined BEFORE Fancytree widget is loaded in order to show the number of selection ON loading
    function selectCallback(event, data) {
        $('#selected-count-orgcharts').text(data.tree.countSelected());
    }
</script>

<div class="courseEnroll-tabs users-selector">
	<ul class="nav nav-tabs">
		<li class="active">
			<a data-target=".courseEnroll-page-users" href="#">
				<span class="courseEnroll-users"></span><?php echo Yii::t('standard', '_USERS'); ?>
			</a>
		</li>
		<li>
			<a data-target=".courseEnroll-page-groups" href="#">
				<span class="courseEnroll-groups"></span><?php echo Yii::t('standard', '_GROUPS'); ?>
			</a>
		</li>
        <!-- HIDE IF NO BRANCHES -->
        <?php if(!empty($fancyTreeData)) : ?>
            <li>
                <a data-target=".courseEnroll-page-orgchart" href="#">
                    <span class="courseEnroll-orgchart"></span><?php echo Yii::t('standard', '_ORGCHART'); ?>
                </a>
            </li>
        <?php endif; ?>
	</ul>

	<div class="select-user-form-wrapper">
		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane active">
				<?php $this->renderPartial('_assignUserFilters', array('model' => $userModel, 'idCourse' => $idCourse, 'idSession' => $idSession)); ?>
			</div>
			<div class="courseEnroll-page-groups tab-pane">
				<?php $this->renderPartial('_assignGroupFilters', array('model' => $groupModel)); ?>
			</div>
            <div class="courseEnroll-page-orgchart tab-pane">

                <div class="orgchart tab-pane">
                    <div class="filters-wrapper">
                        <div class="selections">
                            <div class="input-wrapper-orgchart">
                                <span class="icon-select-node-yes"></span> <?php echo Yii::t('standard', '_YES')?>
                                <span class="icon-select-node-no"></span> <?php echo Yii::t('standard', '_NO')?>
                                <span class="icon-select-node-descendants"></span> <?php echo Yii::t('standard', '_INHERIT')?>
                            </div>
                            <div class="left-selections">
                                <p><?php echo Yii::t('standard', 'You have selected'); ?> <strong><span id="selected-count-orgcharts">0</span> <?php echo Yii::t('standard', 'branches'); ?></strong>.</p>
                                <a href="javascript:void(0);" class="select-all-orgcharts" data-value="1"><?php echo Yii::t('standard', '_SELECT_ALL'); ?></a>
                                <a href="javascript:void(0);" class="deselect-all-orgcharts" data-value="0"><?php echo Yii::t('standard', '_UNSELECT_ALL'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php // $this->renderPartial('_assignOrgchartFilters'); ?>
            </div>
		</div>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'select-user-form',
			'htmlOptions' => array(
				'data-grid-items' => 'true',
			),
		)); ?>

		<div class="tab-content">
			<div class="courseEnroll-page-users tab-pane active">
				<?php $this->renderPartial('_assignUser', array('model' => $userModel, 'idCourse' => $idCourse, 'idSession' => $idSession)); ?>
			</div>
			<div class="courseEnroll-page-groups tab-pane">
				<?php $this->renderPartial('_assignGroup', array('model' => $groupModel)); ?>
			</div>
			<div class="courseEnroll-page-orgchart tab-pane">
                <div>
                <?php
                        // Display Org Chart tree, using the new FancyTree widget
                        $orgChartFancyTreeId = 'orgcharts-fancytree';
                        $this->widget('common.extensions.fancytree.FancyTree', array(
                            'id' => $orgChartFancyTreeId,
                            'treeData' => $fancyTreeData,
                            'formFieldNames' => array(
                                'selectedInputName'	 	=> 'courseEnroll-orgchart',   // this is expected by controller
                            ),

                            'eventHandlers' => array(
                                'select' => 'function(event, data) {
										selectCallback(event, data);
									}',
                            ),

                        ));
                ?>
                </div>
				
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div>


<script type="text/javascript">
    $(function(){
        // Handle Select All clicks in Org Chart tree
        $(".users-selector .select-all-orgcharts").on("click", function(){
            var tree = $('#<?= $orgChartFancyTreeId ?>').fancytree('getTree');
            tree.rootNode.children[0].setSelectState(2);
        });

        // Handle De-Select All clicks in Org Chart tree
        $(".users-selector .deselect-all-orgcharts").on("click", function(){
            var tree = $('#<?= $orgChartFancyTreeId ?>').fancytree('getTree');
            tree.rootNode.children[0].setSelectState(0);
        });
    });
</script>