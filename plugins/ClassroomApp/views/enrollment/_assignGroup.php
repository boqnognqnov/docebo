<div id="grid-wrapper" class="group-table">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'group-grid',
		'htmlOptions' => array('class' => 'grid-view clearfix'),
		'emptyText' => Yii::t('report', '_NULL_REPORT_RESULT'),
		'dataProvider' => $model->dataProvider(),
		'cssFile' => false,
		'columns' => array(
			array(
				'class' => 'CCheckBoxColumn',
				'selectableRows' => 2,
				'id' => 'group-grid-checkboxes',
				'value' => '$data->idst',
				'checked' => '$data->inSessionList(\'selectedItems\', \'idst\');',
			),
			array(
				'name' => 'groupid',
				'value' => '$data->relativeId($data->groupid)',
				'type' => 'raw',
			),
		),
		'template' => '{items}{summary}{pager}',
		'summaryText' => Yii::t('standard', '_TOTAL'),
		'pager' => array(
			'class' => 'ext.local.pagers.DoceboLinkPager',
		),
		'beforeAjaxUpdate' => 'function(id, options) {
			options.type = "POST";
			if (options.data == undefined) {
				options.data = {
					contentType: "html",
					selectedItems: $(\'#\'+id+\'-selected-items-list\').val()
				}
			}
		}',
		'ajaxUpdate' => 'group-grid-all-items',
		'afterAjaxUpdate' => 'function(id, data){$("#group-grid input").styler(); afterGridViewUpdate(id);}',
	)); ?>
</div>
<script type="text/javascript">
	$(function() {
		afterGridViewUpdate('group-grid');
	});
</script>