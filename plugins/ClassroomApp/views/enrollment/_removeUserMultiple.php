<?php $onChange = 'return (($(this).prop("checked")) ? showConfirmButton() : hideConfirmButton());'; ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'session-multi-delete-form',
        'htmlOptions' => array(
            'class' => 'ajax',
        )
	));?>

	<p><?php echo Yii::t('classroom', 'You are unenrolling {count} users from this session', array('{count}' => '<strong>'.count($users).'</strong>')); ?><?php if ($showAutoEnrollMessage) echo "*";?></p>

	<?php
	if (PluginManager::isPluginActive('CurriculaApp') && $excludedUsers > 0) {
		echo '<p class="note">';
		echo Yii::t('curricula', '{n} selected user(s) have been assigned to a learning plan and cannot be unenrolled from course', array('{n}' => '<strong>'.$excludedUsers.'</strong>'));
		echo '</p>';
	}
	?>

	<div class="clearfix">
		<?php echo $form->checkbox($sessionUserModel, 'confirm', array('id' => 'LearningCourseuserSession_confirm', 'onchange' => $onChange)); ?>
		<?php echo CHtml::label(Yii::t('standard', 'Yes, I confirm I want to proceed'), 'LearningCourseuserSession_confirm'); ?>
	</div>

	<?php
		echo CHtml::hiddenField('id_session', $idSession);
		echo CHtml::hiddenField('idCourse', $idCourse);
		echo CHtml::hiddenField('users', implode(',', $users));
	?>
	<?php $this->endWidget(); ?>
	<?php if ($showAutoEnrollMessage) echo  '<br><p><small>* ' . Yii::t('classroom', 'Remember, if the course has the "automatic enrollment of users from waiting list when a seat is freed" setting enabled, users from the waiting list automatically become enrolled, taking the seats of the unenrolled users.') . '</small></p>';?>
</div>