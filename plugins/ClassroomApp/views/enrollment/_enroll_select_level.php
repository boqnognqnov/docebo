<div class="select-role-wrapper">

	<?php
	echo CHtml::beginForm('', 'POST', array(
		'id' => 'session-enroll-role-step-form',
		'name' => 'session-enroll-role-step-form',
		'class' => 'ajax'
	));
	echo CHtml::hiddenField('from_step', 'enroll-role-step');
	echo CHtml::hiddenField('idCourse', $idCourse);
	echo CHtml::hiddenField('idSession', $idSession);
	echo CHtml::hiddenField('users', implode(',', $usersList))
	?>

	<p>
		<?= Yii::t('subscribe', '_CAPTION_SELECT_LEVELS') ?>
	</p>
	<br />
	<br />

	<div class="row-fluid">
		<div class="span3 text-right">
			<p>
				<?= Yii::t('standard', '_LEVEL') ?>:&nbsp;
			</p>
		</div>
		<div class="span9">
			<?php
			$prompt = array(0 => Yii::t('standard', '_SELECT').'...');
			$userLevels = $prompt + $userLevels;
			echo CHtml::dropDownList('level', 0, $userLevels, array(
				'id' => 'enrollment_role-input'
			));
			?>
		</div>
	</div>

	<?php echo CHtml::endForm(); ?>

</div>

<script type="text/javascript">
	$(function() {

		<?php if (Yii::app()->backgroundJob->bgJobEnabled(BackgroundJob::FAMILTY_MASSENROLL)) : ?>
			var jUrl = <?= json_encode(Docebo::createAdminUrl('ClassroomApp/enrollment/startBackgroundJob')) ?>;
			var bgJobs = true;  
		<?php else : ?>
			var jUrl = <?= json_encode(Docebo::createLmsUrl('progress/run')) ?>;
			var bgJobs = false;
		<?php endif; ?> 

		//detect if confirm button has to be disabled or not
		var updateButtons = function() {
			var value = $('#enrollment_role-input').val();
			var cf = $('#confirm-button');
			if (value <= 0) {
				cf.prop('disabled', true);
				cf.addClass('disabled');
			} else {
				cf.prop('disabled', false);
				cf.removeClass('disabled');
			}
		};

		//manage dropdown and confirm button availability
		$(document).undelegate('#enrollment_role-input', 'change'); //remove previous events on this id
		$(document).delegate('#enrollment_role-input', 'change', function(e) {
			updateButtons();
		});
		updateButtons(); //initialize buttons status

		//manage confirm button
		$(document).undelegate('#confirm-button', 'click'); //remove previous events on this id
		$(document).delegate('#confirm-button', 'click', function(e) {
			//make sure input is valid
			if ($('#enrollment_role-input').val() <= 0) return;
			//manually send an ajax request with dialog form parameters
			var form = $("#session-enroll-role-step-form");
			var formData = form.serialize() + '&type=<?=Progress::JOBTYPE_ENROLL_USERS_TO_SESSION?>';
			$.ajax({
				type: "POST",
				url: jUrl,
				data: formData,
				beforeSend: function(jqXHR, settings) {
					//clear buttons and content before effectively sending ajax request
					$('.modal.users-level-selector').find('.modal-body').html('<div class="align-center" style="width: 36px; margin: 0 auto;"><img src="<?= Yii::app()->theme->baseUrl.'/images/loading_medium.gif' ?>"></div>');
					$('.modal.users-level-selector').find('.modal-footer').html('');
				},
				success: function(data) {
					//close actual dialog
					$('.modal.users-level-selector').find('.modal-body').dialog2("close");
					//open new progress dialog
					if (!bgJobs) {
						var dialogId = 'enroll-users-to-session-progressive';
						$('<div/>').dialog2({
							autoOpen: true, // We will open the dialog later
							id: dialogId,
							title: '<?= Yii::t('course', 'Enroll users to session') ?>'
						});
						$('#'+dialogId).html(data);
					} else{
						$('body').append(data);
					}
				}
			});
		});

		//manage cancel button
		$(document).undelegate('#cancel-button', 'click'); //remove previous events on this id
		$(document).delegate('#cancel-button', 'click', function(e) {
			//just close the dialog
			$('.modal.users-level-selector').find('.modal-body').dialog2("close");
		});

		//updating
		$(document).delegate(".modal.users-level-selector", "dialog2.content-update", function() {
			var e = $(this);
			var autoclose = e.find("a.auto-close");
			if (autoclose.length > 0) {
				e.find('.modal-body').dialog2("close");
				var href = autoclose.attr('href');
				if (href) { window.location.href = href; }
				return;
			}
		});

		$('#session-enroll-role-step-form').controls();
	});
</script>