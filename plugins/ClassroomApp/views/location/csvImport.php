<?php
/* @var $this LocationController */
/* @var $model LocationImportForm */
/* @var $form CActiveForm */

$this->breadcrumbs[Yii::t('classroom', 'Locations Manager')] = Docebo::createAppUrl('lms:ClassroomApp/Location/index');
$this->breadcrumbs[] = Yii::t('classroom', 'Import from CSV');

?>

<h3 class="title-bold">
    <a class="docebo-back" href="<?= Docebo::createAppUrl('lms:ClassroomApp/Location/index') ?>">
        <span><?= Yii::t('standard', '_BACK') ?></span>
    </a>
    <?= Yii::t('classroom', 'Import Locations from CSV') ?></h3>
<br>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'locations-import-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'docebo-form form-horizontal form-import',
        'enctype' => 'multipart/form-data',
    ),
)); ?>
    <div class="control-container odd">
        <div class="control-group">
            <?= $form->labelEx($model, 'file', array('class'=>'control-label')) ?>
            <div class="controls">

                <div class="download-sample">
                    <span class="i-sprite is-download"></span> <?php echo CHtml::link(Yii::t('standard', '_DOWNLOAD'), $this->createUrl('csvSample', array()), array('target'=>'_blank')); ?></span> <?php echo Yii::t('standard', '_A_SAMPLE_CSV_FILE'); ?>
                </div>

                <?= $form->fileField($model, 'file', array()) ?>
            </div>
        </div>
    </div>

    <div class="control-container">
        <div class="control-group">
            <?= $form->labelEx($model, 'separator', array('class'=>'control-label')) ?>
            <div class="controls">

                <?php echo $form->radioButtonList($model, 'separator', array(
                    'auto' => Yii::t('standard', '_AUTODETECT'),
                    'comma' => ',',
                    'semicolon' => ';',
                    'manual' => Yii::t('standard', '_MANUAL').':',
                ), array(
                    'separator' => '',
                )); ?>
                <?php echo $form->textField($model, 'manualSeparator', array('class' => 'manual-separator-input')); ?>

                <br>
                <br>
                <?php echo $form->checkBox($model, 'firstRowAsHeader'); ?>
                <?php echo $form->labelEx($model, 'firstRowAsHeader'); ?>

            </div>
        </div>
    </div>

    <div class="control-container odd">
        <div class="control-group">
            <?= $form->labelEx($model, 'charset', array('class'=>'control-label')) ?>
            <div class="controls">
                <?= $form->textField($model, 'charset', array('class'=>'span3')) ?>
            </div>
        </div>
    </div>
    <br>
    <div class="text-right">
        <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_NEXT'); ?>" />
        <a href="<?= $this->createUrl('index') ?>" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
    </div>
<?php $this->endWidget(); ?>


<script type="text/javascript">
    <!--

    (function ($) {
        $(function () {
            $('input,select').styler({browseText:'<?=Yii::t('organization', 'Upload File')?>'});
        });
    })(jQuery);

    //-->
</script>