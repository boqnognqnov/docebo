<?php
/* @var $this LocationController */
/* @var $locationModel LtLocation */
/* @var $form CActiveForm */

$this->breadcrumbs[] = Yii::t('classroom', 'Locations Manager');

?>

<h2><?= Yii::t('classroom', 'Locations') ?></h2>
<br>

<?php if (Yii::app()->user->checkAccess('/lms/admin/location/mod')): ?>
<ul id="docebo-bootcamp-locations" class="docebo-bootcamp clearfix">
    <li>
        <a id="add-new-location"
           href="<?= $this->createUrl('axEdit') ?>"
           class="open-dialog"
           data-dialog-class="modal-edit-location"
           data-helper-title="<?= Yii::t('classroom', 'New location') ?>"
           data-helper-text="<?= Yii::t('helper', 'New location - lt') ?>">
            <span class="classroom-sprite marker-plus"></span>
            <span class="classroom-sprite marker-plus white"></span>
            <h4><?= Yii::t('classroom', 'New location') ?></h4>
        </a>
    </li>
    <li>
        <a id="import-csv-locations" href="<?= $this->createUrl('csvImport') ?>"
           data-helper-title="<?= Yii::t('classroom', 'Import from CSV') ?>"
           data-helper-text="<?= Yii::t('helper', 'Import location from CSV - lt') ?>">
            <span class="i-sprite is-putin large"></span>
            <span class="i-sprite is-putin large white"></span>
            <h4><?= Yii::t('classroom', 'Import from CSV') ?></h4>
        </a>
    </li>

    <li class="helper">
        <a href="#">
            <span class="i-sprite is-circle-quest large"></span>
            <h4 class="helper-title"></h4>
            <p class="helper-text"></p>
        </a>
    </li>

</ul>
<?php endif; ?>

<!--search box-->
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'location-grid-search-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'docebo-form grid-search-form'
    ),
)); ?>
<div class="control-container odd">
    <div class="control-group">
        <div class="input-wrapper">
            <input id="locations-grid-search" autocomplete="off" type="text" name="<?= CHtml::activeName($locationModel, 'name') ?>" placeholder="<?php echo Yii::t('standard', '_SEARCH'); ?>" data-autocomplete="true"/>
            <span class="i-sprite is-search"></span>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<!--locations grid-->
<div id="grid-wrapper" class="">
	<?php
	
		$_columns = array(
			array(
				'type' => 'raw',
				'value' => '$data["name"]',
				'name' => 'name',
				'header' => Yii::t('classroom', 'Location name'),
				'sortable' => true,
			),
			array(
				'type' => 'raw',
				'name' => 'address',
				'value' => '$data["address"]',
				'header' => Yii::t('billing', '_ADDRESS')
			),
			array(
				'type' => 'raw',
				'value' => '$data->country["name_country"]',
				'header' => Yii::t('standard', '_COUNTRY')
			),
			array(
				'type' => 'raw',
				'value' => array($this, 'gridRenderClassrooms'),
				'header' => Yii::t('classroom', 'Classrooms'),
				'htmlOptions' => array(
					'class' => 'text-center',
				),
				'headerHtmlOptions' => array(
					'class' => 'text-center',
					"title" => Yii::t("classroom", "View location map")
				),
			),
			array(
				'id' => 'location-map',
				'type' => 'raw',
				'value' => array($this, 'gridRenderLinkViewMap'),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				)
			)
		);
		if (Yii::app()->user->checkAccess('/lms/admin/location/mod')) {
			$_columns[] = array(
				'type' => 'raw',
				'value' => array($this, 'gridRenderLinkEditLocation'),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				)
			);
			$_columns[] = array(
				'type' => 'raw',
				'value' => array($this, 'gridRenderLinkDeleteLocation'),
				'htmlOptions' => array(
					'class' => 'button-column-single',
				)
			);
		}

		// Let the plugins add custom fields in this grid
		$event = new DEvent($this, array('columns' => &$_columns));
		Yii::app()->event->raise('beforeRenderClassroomAppLocationsGrid', $event);

		$this->widget('DoceboCGridView', array(
			'id' => 'location-management-grid',
			'htmlOptions' => array('class' => 'grid-view'),
			'dataProvider' => $locationModel->dataProvider(),
			'columns' => $_columns,
			'template' => '{items}{summary}{pager}',
			'summaryText' => Yii::t('standard', '_TOTAL'),
			'pager' => array(
				'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
			),
			'beforeAjaxUpdate' => 'function(id, options) {
            // Reset placeholder text if default one
            resetSearchPlaceholder();
            options.type = "POST";
            options.data = $("#location-grid-search-form").serialize()
        }',
			'afterAjaxUpdate' => 'function(id, data){
            $(\'a[rel="tooltip"]\').tooltip();
            $(document).controls();
        }'
		));
	?>
</div>

<script type="text/javascript">

function resetSearchPlaceholder() {
    var $search = $('#locations-grid-search');
    if($search.val() == "<?= Yii::t('standard', '_SEARCH'); ?>")
        $('#locations-grid-search').val('');
}

$(function(){
        var $search = $('#locations-grid-search');

        var filterLocationsGrid = function() {
            var filter = $.trim( $search.val() );
            $("#location-management-grid").yiiGridView('update');
        };

        $('#docebo-bootcamp-locations').doceboBootcamp();

        // on enter key press
        $search.bind('keypress', function(e) {
            var code = e.keyCode || e.which;
            if(code == 13) {
                // enter was pressed, then update map
                filterLocationsGrid();
                e.preventDefault();
                return false;
            }
        })
});
</script>