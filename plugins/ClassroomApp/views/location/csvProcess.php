<?php
/* @var $this LocationController */
/* @var $model LocationImportForm */
/* @var $columns array */
/* @var $form CActiveForm */

$this->breadcrumbs[Yii::t('classroom', 'Locations Manager')] = Docebo::createAppUrl('lms:ClassroomApp/Location/index');
$this->breadcrumbs[] = Yii::t('classroom', 'Import from CSV');

?>

<h3 class="title-bold">
    <a class="docebo-back" href="<?= Docebo::createAppUrl('lms:ClassroomApp/Location/index') ?>">
        <span><?= Yii::t('standard', '_BACK') ?></span>
    </a>
    <?= Yii::t('classroom', 'Import Locations from CSV') ?></h3>
<br>


<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'locations-import-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'class' => 'docebo-form form-horizontal form-import',
        'enctype' => 'multipart/form-data',
    ),
)); ?>
    <div class="control-container odd">
        <div class="control-group">
            <?= $form->labelEx($model, 'insertUpdate', array('class'=>'control-label')) ?>
            <div class="controls">
                <?= $form->checkBox($model, 'insertUpdate', array()) ?>
                <?php echo $form->labelEx($model, 'insertUpdate'); ?>
            </div>
        </div>
    </div>

    <br>
    <h3 class="title-bold"><?= Yii::t('organization_chart', '_IMPORT_MAP') ?></h3>

    <?= $form->errorSummary($model) ?>

    <?php $this->widget('DoceboCGridView', array(
        'id' => 'locations-import-grid',
        'htmlOptions' => array('class' => 'grid-view'),
        'dataProvider' => $model->dataProvider(),
        'columns' => $columns,
        'template' => '<div class="table-scroll">{items}</div>{summary}{pager}',
        'summaryText' => Yii::t('standard', '_TOTAL'),
        'pager' => array(
            'class' => 'admin.protected.extensions.local.pagers.DoceboLinkPager',
        ),
        'beforeAjaxUpdate' => 'function(id, options) {
            //options.type = "POST";
            //options.data = $("#user-import-form-step2").serialize();
        }',
        'afterAjaxUpdate' => 'function() {
            // $("#user-import-form-step2").jCleverAPI("refresh");
            $("select").hide();
            $("select").styler();
            autoSelectWidth();
            $(".table-scroll").jScrollPane({autoReinitialise: true});
        }'
    )); ?>

    <br>
    <div class="text-right">
        <input class="btn-docebo green big" type="submit" value="<?php echo Yii::t('standard', '_NEXT'); ?>" />
        <a href="<?= $this->createUrl('index') ?>" class="btn-docebo black big"><?php echo Yii::t('standard', '_CANCEL'); ?></a>
    </div>
<?php $this->endWidget(); ?>


<script type="text/javascript">
    <!--

    function autoSelectWidth() {
        var selectWidth = $('select').outerWidth(),
            selectStylerContainer = $('.form-import .jqselect'),
            selectStyler = $('.form-import .jq-selectbox__select');
        selectStylerContainer.css('width',selectWidth);
        selectStyler.css('width','auto');
    };

    (function ($) {
        $(function () {
            $('input').styler();
            autoSelectWidth();
            $('.table-scroll').jScrollPane({
                autoReinitialise : true
            });
        });
    })(jQuery);

    //-->
</script>