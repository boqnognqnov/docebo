<?php
/* @var $this LocationController */
/* @var $model LtLocation */
/* @var $form CActiveForm */
?>
<h1><?= (true===$model->getIsNewRecord()) ? Yii::t('classroom', 'New location') : Yii::t('classroom', 'Edit location') ?></h1>



<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-location-form',
    'method' => 'POST',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class' => 'ajax docebo-form'
    ),
));
echo $form->hiddenField($model, 'id_location');
?>

<div class="docebo-modal-tabs clearfix">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#" data-toggle="tab" data-target="#editLocation-address">
                <span class="classroom-sprite map"></span>
                <span class="classroom-sprite map white active"></span>
                <?= Yii::t('classroom', 'Address and Contacts') ?>
            </a>
        </li>
        <li>
            <a href="#" data-toggle="tab" data-target="#editLocation-details">
                <span class="classroom-sprite list"></span>
                <span class="classroom-sprite list white active"></span>
                <?= Yii::t('standard', '_DETAILS') ?>
            </a>
        </li>
        <li>
            <a href="#" data-toggle="tab" data-target="#editLocation-photos">
                <span class="classroom-sprite picture"></span>
                <span class="classroom-sprite picture white active"></span>
                <?= Yii::t('classroom', 'Location photos') ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="editLocation-address">
            <?= $form->labelEx($model, 'name') ?>
            <?= $form->textField($model, 'name', array('class'=>'input-block-level')) ?>

            <?= $form->labelEx($model, 'address') ?>
            <?= $form->textField($model, 'address', array('class'=>'input-block-level')) ?>
            <p class="help-block">
                <strong><?= Yii::t('classroom', 'Complete address') ?></strong>
                <?= Yii::t('classroom', 'E.g. - Street name, 10 - 12345 City') ?>
            </p>

            <?= $form->labelEx($model, 'id_country') ?>
            <?= $form->dropDownList($model, 'id_country', CHtml::listData(CoreCountry::model()->findAll(),'id_country',function($country) {
                return CHtml::encode($country->name_country);
            }), array('class'=>'input-block-level')) ?>

            <?= $form->labelEx($model, 'telephone') ?>
            <?= $form->textField($model, 'telephone', array('class'=>'input-block-level')) ?>

            <?= $form->labelEx($model, 'email') ?>
            <?= $form->textField($model, 'email', array('class'=>'input-block-level')) ?>

            <?= CHtml::label(Yii::t('classroom', 'Map preview'), '') ?>
            <div class="map-preview-container">
                <span class="classroom-sprite map-placeholder"></span>
                <div id="location-map-canvas" class="map-canvas"></div>
            </div>
        </div>

        <div class="tab-pane" id="editLocation-details">
            <?= $form->labelEx($model, 'reaching_info') ?>
            <?= $form->textArea($model, 'reaching_info', array('class'=>'')) ?>

            <?= $form->labelEx($model, 'accomodations') ?>
            <?= $form->textArea($model, 'accomodations', array('class'=>'')) ?>

            <?= $form->labelEx($model, 'other_info') ?>
            <?= $form->textArea($model, 'other_info', array('class'=>'input-block-level')) ?>
        </div>

        <div class="tab-pane" id="editLocation-photos">
            <?= CHtml::label(Yii::t('classroom', 'Location photos (maximum {max})', array('{max}'=>10)), '') ?>

            <div class="control-container odd">
                <div class="control-group">

                    <?= CHtml::label( Yii::t('classroom', 'Upload Location photos'), '', array('class' => 'upload-photos-label') ) ?>

                    <?php $this->beginWidget('CMultiFileUpload', array(
                        'name' => 'LearningLocationPhotos[]',
                        'attribute' => 'file_path',
                        'accept' => 'jpg|jpeg|gif|png',
                        'max' => 4,
                        'htmlOptions' => array(
                            'multiple' => 'multiple'
                        )
                    )); ?>
                    <?php $this->endWidget(); ?>
                </div>
            </div>

            <div class="available-photos">
                <h4><?= Yii::t('templatemanager', 'Ours images collection') ?></h4>

                <p class="no-photos <?= ( ! is_array($model->photos) || 0===count($model->photos)) ? '' : 'hide' ?>">
                    <?= Yii::t('classroom', 'No photos available') ?>
                </p>

                <?php if ( is_array($model->photos) && 0 < count($model->photos)) : ?>
                <div class="row-fluid">
                    <?php foreach ($model->photos as $_photo) : ?>
                    <div class="thumbnail">
                        <img src="<?= $_photo->getUrl() ?>" alt=""/>
                        <div class="controls">
                            <a href="<?= $this->createUrl('axDeletePhoto', array('id' => $_photo->id_photo)) ?>" title="<?= Yii::t('standard', '_DEL') ?>">
                                <span class="i-sprite is-remove red"></span>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>

    </div>

</div>

<div class="form-actions">
    <input class="btn-docebo green big" type="submit" value="<?= Yii::t('standard', '_CONFIRM') ?>" />
    <input class="btn-docebo black big close-dialog" type="reset" value="<?php echo Yii::t('standard', '_CANCEL'); ?>"/>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

	(function () {

		var geocoder;
		var map;
		var lastEnteredAddress;

		function updateMapPreview() {
			var $mapCanvas = $('#location-map-canvas');
			var address = $.trim($('#<?= CHtml::activeId($model, 'address') ?>').val());

			if (address == '') {
				// clear preview
				$mapCanvas.html('').attr('style', '');
				return;
			}

			if (address === lastEnteredAddress)
				return;

			if (!geocoder)
				geocoder = new google.maps.Geocoder();

			geocoder.geocode({'address': address}, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var mapOptions = {
						zoom: 14
					}
					map = new google.maps.Map($mapCanvas[0], mapOptions);

					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
					lastEnteredAddress = address;
				} else {
					Docebo.log('Geocode was not successful for the following reason: ' + status);
				}
			});
		}

		$(document).on("dialog2.closed", ".modal-edit-location", function (e) {
			try
			{
				TinyMce.removeEditorById('<?= CHtml::activeId($model, 'reaching_info') ?>');
				TinyMce.removeEditorById('<?= CHtml::activeId($model, 'accomodations') ?>');
				TinyMce.removeEditorById('<?= CHtml::activeId($model, 'other_info') ?>');
			}
			catch(e)
			{
				tinyMCE.editors = [];
			}
		});

		$(document).delegate(".modal-edit-location", "dialog2.content-update", function () {
			var e = $(this);
			var $modal = e.find('.modal-body');

			var autoclose = e.find("a.auto-close");

			if (autoclose.length > 0) {

				//close the dialog
				e.find('.modal-body').dialog2("close");

				// reload locations grid
				$("#location-management-grid").yiiGridView('update');

				//check for page jumping
				var href = autoclose.attr('href');
				if (href) { window.location.href = href; }
			}
			else {
				Docebo.Modal.CenterPosition();

				updateMapPreview();

				// show the map preview for the entered address
				$('#<?= CHtml::activeId($model, 'address') ?>')
					.bind('keypress', function (e) {
						var code = e.keyCode || e.which;
						if (code == 13) {
							// enter was pressed, then update map
							updateMapPreview();
							e.preventDefault();
							return false;
						}
					})
					.bind('focusout', function (e) {
						updateMapPreview();
					});

				// init tinymce
				var idTextArea1 = '<?= CHtml::activeId($model, 'reaching_info') ?>';
				var idTextArea2 = '<?= CHtml::activeId($model, 'accomodations') ?>';
				var idTextArea3 = '<?= CHtml::activeId($model, 'other_info') ?>';
				var inputDetails1 = $('#' + idTextArea1);
				var inputDetails2 = $('#' + idTextArea2);
				var inputDetails3 = $('#' + idTextArea3);
				if (inputDetails1.length > 0 && !TinyMce.checkEditorExistenceById(idTextArea1)) { TinyMce.attach(inputDetails1, 'standard_embed', {height: 100}); }
				if (inputDetails2.length > 0 && !TinyMce.checkEditorExistenceById(idTextArea2)) { TinyMce.attach(inputDetails2, 'standard_embed', {height: 100}); }
				if (inputDetails3.length > 0 && !TinyMce.checkEditorExistenceById(idTextArea3)) { TinyMce.attach(inputDetails3, 'standard_embed', {height: 100}); }

				$('#LearningLocationPhotos').styler({browseText: '<?=Yii::t('standard', '_UPLOAD')?>'});

				$('.available-photos .controls > a').click(function (e) {
					e.preventDefault();
					var $that = $(this)
					$.post($that.attr('href'), function (response) {
						$that.closest('.thumbnail').remove();
						if (0 == $('.available-photos .thumbnail').length) {
							$('.available-photos .no-photos').removeClass('hide');
						}
					})
				});
			}

			$('#edit-location-form').bind('form-pre-serialize', function () {
				tinyMCE.triggerSave();
			});
		});
	})();
</script>

