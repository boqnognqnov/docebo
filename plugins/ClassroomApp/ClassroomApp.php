<?php

/**
 * Plugin for ClassroomApp
 * Note: for Apps the class name should match this:
 * ucfirst($codename).'App';
 * where $codename is the App.codename defined in Erp
 */

class ClassroomApp extends DoceboPlugin {

	public static $HAS_SETTINGS = false;

	// Name of the plugin; needed to identify plugin folder and entry script
	public $plugin_name = 'ClassroomApp';


	/**
	 * This will allow us to access "statically" (singleton) to plugin methods
	 * @param string $class_name
	 * @return Plugin Object
	 */
	public static function plugin($class_name=__CLASS__) {
		return parent::plugin($class_name);
	}


    /**
     * Install SQL queries for the classroom module
     * @return bool|void
     */
    public function activate() {
		parent::activate();

        $sql = "
-- -----------------------------------------------------
-- Table `lt_location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_location` (
  `id_location` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NULL,
  `id_country` INT(11) NOT NULL,
  `telephone` VARCHAR(45) NULL,
  `email` VARCHAR(255) NULL,
  `reaching_info` TEXT NULL,
  `accomodations` TEXT NULL,
  `other_info` TEXT NULL,
  PRIMARY KEY (`id_location`),
  INDEX `fk_learning_location_core_country1_idx` (`id_country` ASC),
  CONSTRAINT `fk_learning_location_core_country1`
    FOREIGN KEY (`id_country`)
    REFERENCES `core_country` (`id_country`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_classroom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_classroom` (
  `id_classroom` INT(11) NOT NULL AUTO_INCREMENT,
  `id_location` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `details` TEXT NULL,
  `seats` INT(11) NOT NULL DEFAULT 0,
  `equipment` TEXT NULL,
  PRIMARY KEY (`id_classroom`),
  INDEX `fk_learning_classroom_learning_location1_idx` (`id_location` ASC),
  CONSTRAINT `fk_learning_classroom_learning_location1`
    FOREIGN KEY (`id_location`)
    REFERENCES `lt_location` (`id_location`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_location_photo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_location_photo` (
  `id_photo` INT(11) NOT NULL AUTO_INCREMENT,
  `file_path` VARCHAR(255) NOT NULL,
  `order` INT(11) NULL DEFAULT 0,
  `description` VARCHAR(255) NULL,
  `id_location` INT(11) NOT NULL,
  PRIMARY KEY (`id_photo`),
  INDEX `fk_learning_location_photo_learning_location1_idx` (`id_location` ASC),
  CONSTRAINT `fk_learning_location_photo_learning_location1`
    FOREIGN KEY (`id_location`)
    REFERENCES `lt_location` (`id_location`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_course_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_course_session` (
  `id_session` INT(11) NOT NULL AUTO_INCREMENT,
  `course_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL,
  `max_enroll` INT(11) NOT NULL,
  `min_enroll` INT(11) NULL DEFAULT 0,
  `score_base` INT(11) NULL DEFAULT 100,
  `other_info` TEXT NULL,
  `notify_enrolled` TINYINT(1) NULL DEFAULT 0,
  `date_begin` TIMESTAMP NOT NULL,
  `date_end` TIMESTAMP NOT NULL,
  `total_hours` FLOAT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_session`),
  INDEX `fk_learning_course_session_learning_course1_idx` (`course_id` ASC),
  CONSTRAINT `fk_learning_course_session_learning_course1`
    FOREIGN KEY (`course_id`)
    REFERENCES `learning_course` (`idCourse`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_course_session_date`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_course_session_date` (
  `id_session` INT(11) NOT NULL,
  `day` DATE NOT NULL,
  `name` VARCHAR(255) NULL,
  `time_begin` TIME NOT NULL,
  `time_end` TIME NOT NULL,
  `break_begin` TIME NULL,
  `break_end` TIME NULL,
  `timezone` VARCHAR(255) NOT NULL,
  `id_location` INT(11) NOT NULL,
  `id_classroom` INT(11) NULL,
  PRIMARY KEY (`id_session`, `day`),
  INDEX `fk_learning_course_session_date_learning_location1_idx` (`id_location` ASC),
  CONSTRAINT `fk_learning_course_session_date_learning_course_session1`
    FOREIGN KEY (`id_session`)
    REFERENCES `lt_course_session` (`id_session`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_course_session_date_learning_location1`
    FOREIGN KEY (`id_location`)
    REFERENCES `lt_location` (`id_location`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_course_session_date_attendance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_course_session_date_attendance` (
  `id_session` INT(11) NOT NULL,
  `day` DATE NOT NULL,
  `id_user` INT(11) NOT NULL,
  PRIMARY KEY (`id_session`, `day`, `id_user`),
  INDEX `fk_learning_course_session_date_attendance_core_user1_idx` (`id_user` ASC),
  INDEX `fk_learning_course_session_date_attendance_learning_course__idx` (`id_session` ASC, `day` ASC),
  CONSTRAINT `fk_learning_course_session_date_attendance_core_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_course_session_date_attendance_learning_course_se1`
    FOREIGN KEY (`id_session`, `day`)
    REFERENCES `lt_course_session_date` (`id_session`, `day`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `lt_courseuser_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lt_courseuser_session` (
  `id_session` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `evaluation_text` TEXT NULL,
  `evaluation_file` VARCHAR(255) NULL,
  `evaluation_score` INT(11) NULL,
  `evaluation_date` DATE NULL,
  `evaluation_status` TINYINT(1) NULL,
  `evaluator_id` INT(11) NULL,
  `attendance_hours` INT(11) NULL DEFAULT 0,
  `status` TINYINT(1) NOT NULL DEFAULT 0,
  `date_subscribed` TIMESTAMP NOT NULL,
  `date_completed` TIMESTAMP NULL,
  PRIMARY KEY (`id_session`, `id_user`),
  INDEX `fk_learning_courseuser_session_core_user1_idx` (`id_user` ASC),
  INDEX `fk_learning_courseuser_session_core_user2_idx` (`evaluator_id` ASC),
  CONSTRAINT `fk_learning_courseuser_session_core_user1`
    FOREIGN KEY (`id_user`)
    REFERENCES `core_user` (`idst`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_courseuser_session_learning_course_session1`
    FOREIGN KEY (`id_session`)
    REFERENCES `lt_course_session` (`id_session`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_learning_courseuser_session_core_user2`
    FOREIGN KEY (`evaluator_id`)
    REFERENCES `core_user` (`idst`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
";
        $command = Yii::app()->db->createCommand($sql);
        $command->execute();
	}


	public function deactivate() {
		parent::deactivate();
		//...
	}


}
