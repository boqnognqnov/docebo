<?php

/**
 * Displays the basic session info, along with a list of session dates (accordion list type)
 */
class SessionInfo extends CWidget {

	public $courseModel;
	public $sessionId;
	public $hideScoreBase;

	/**
	 * @var LtCourseSession
	 */
	protected $session;
	/**
	 * @var CActiveDataProvider
	 */
	protected $dataProvider;
	protected $evaluation;
	protected $userLevel;


	protected $_assetsUrl = null;
	protected $_playerAssetsUrl = null;

	/**
	 * Get widget assets url path
	 * @return mixed
	 */
	public function getAssetsUrl() {
		if ($this->_assetsUrl === null) {
			$this->_assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ClassroomApp.assets'));
		}
		return $this->_assetsUrl;
	}



	/**
	 * Get player module assets url path
	 * @return mixed
	 */
	public function getPlayerAssetsUrl()
	{
		if ($this->_playerAssetsUrl === null) {
			$this->_playerAssetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('player.assets'));
		}
		return $this->_playerAssetsUrl;
	}



	public function init()
	{
		parent::init();

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/js/fancybox/jquery.fancybox.css');
		$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/fancybox/jquery.fancybox.pack.js');
		$cs->registerScriptFile('https://maps.googleapis.com/maps/api/js?key='.Settings::get('googlemaps_api_key', 'AIzaSyAuUMueMRN0lXmNnxyj-RnWIOtwAxOP500').'&v=3.exp');

		// need for widget in modal when edit and upload item
		$adminPath = Yii::app()->getRequest()->getBaseUrl() . '/../admin';
		$cs->registerScriptFile($adminPath . '/js/admin.js');
		$cs->registerScriptFile($adminPath . '/js/script.js');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/admin.css');

		$this->session = LtCourseSession::model()->findByPk($this->sessionId);
		$this->dataProvider = LtCourseSessionDate::sessionInfoDataProvider($this->sessionId);

		// Check is user is enrolled in this session
		$courseSession = LtCourseuserSession::model()->findByAttributes(array(
			'id_session' => $this->sessionId,
			'id_user' => Yii::app()->user->id
		));
		/* @var $courseSession LtCourseuserSession */

		// Do we have to show session subscription info ? (only for subscribed users)
		$userLevel = 0;
		if ($courseSession) {
			$this->userLevel = $courseSession->level;
			$showEvaluation = in_array($courseSession->evaluation_status, array(
					LtCourseuserSession::EVALUATION_STATUS_FAILED,
					LtCourseuserSession::EVALUATION_STATUS_PASSED
				)) && ($this->userLevel < LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR);
			if ($showEvaluation) {
				$this->evaluation = array(
					'status' => ($courseSession->evaluation_status == LtCourseuserSession::EVALUATION_STATUS_PASSED)
							? 'passed' : 'failed',
					'score' => $this->getEvaluationScore($courseSession),
					'comments' => $courseSession->evaluation_text,
					'attachment' => array(
						'name' => $courseSession->getOriginalFileName(),
						'url' => ($courseSession->hasEvaluationFile() ? Docebo::createLmsUrl('ClassroomApp/instructor/downloadEvaluationFile', array(
								'id_user' => Yii::app()->user->id,
								'id_session' => $courseSession->getPrimaryKey(),
								'course_id' => $this->session->course_id
							)) : '#')
					),
					'evaluator' => CoreUser::model()->findByPk($courseSession->evaluator_id),
					'date' => $courseSession->evaluation_date
				);
			}
		} else {
			if (!Yii::app()->user->getIsGodadmin() && !Yii::app()->user->getIsAdmin())
				throw new CHttpException("You are not subscribed to this session!");
		}
	}



	public function run() {

		/* @var $cs CClientScript */
		$cs = Yii::app()->getClientScript();
		// Dynatree ("borrowed" by lms/player assets)
		$cs->registerScriptFile($this->getPlayerAssetsUrl() . '/js/jquery.dynatree.js');
		$cs->registerCssFile($this->getPlayerAssetsUrl() . '/css/ui.dynatree-modified.css');
		$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/classroom-attendance-sheet.css');
		//our editing objects tree and launchpad
		$cs->registerScriptFile($this->getAssetsUrl() . '/js/classroom-los-editing.js');
		$cs->registerScriptFile($this->getAssetsUrl() . '/js/classroom-los-launcher.js');

		Yii::app()->event->raise('BeforeSessionInfoWidgetRender', new DEvent($this, array('sessionInfo'=>&$this)));

        $_countObjects = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".LearningOrganization::model()->tableName()
            ." WHERE objectType NOT IN('". LearningOrganization::OBJECT_TYPE_AICC
            ."','". LearningOrganization::OBJECT_TYPE_SCORMORG
            ."','". LearningOrganization::OBJECT_TYPE_TINCAN."', '') "
            ." AND idCourse = :id_course")->queryScalar(array(
            ':id_course' => $this->courseModel->getPrimaryKey()
        ));

		//render the course page
		$this->render('sessionInfo', array(
			'canEditCourse' => ($this->userLevel >= LearningCourseuser::$USER_SUBSCR_LEVEL_INSTRUCTOR || Yii::app()->user->getIsGodadmin()),
            '_countObjects' => $_countObjects
		));
	}


	public function  getEvaluationScore($courseSession){
		$score = null;
		if($this->session->evaluation_type == LtCourseSession::EVALUATION_TYPE_SCORE){
			$score =  $courseSession->evaluation_score;
		}elseif($this->session->evaluation_type == LtCourseSession::EVALUATION_TYPE_ONLINE){
			$learningCourseUserModel = LtCourseSession::model()->findByAttributes(array('id_session'=>$courseSession->id_session));
			if($learningCourseUserModel){
				$score = LearningCourseuser::getLastScoreByUserAndCourse($learningCourseUserModel->course_id, $courseSession->id_user, true);
				if(isset($score['score']) && $score['score']){
					$score['score'] = round(floatval($score['score']), 2);
				}
			}
		}

		return $score;
	}


}