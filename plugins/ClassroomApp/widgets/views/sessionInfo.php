<?php
/* @var $this SessionInfo */
?>
<?php if ($canEditCourse): ?>
<div style="margin-bottom: 20px;">
    <a class="docebo-back" href="<?= Docebo::createAppUrl('lms:player/training/session', array('course_id'=>$this->courseModel->getPrimaryKey())) ?>">
        <span><?= Yii::t('standard', '_BACK') ?></span>
    </a>
    <h3 style="display: inline-block"><?= CHtml::encode($this->session->name) ?></h3>
</div>
<?php endif; ?>

<div id="player-arena-launchpad"></div>

<div id="session-info-container" class="session-info-container">

	<div class="row-fluid">
		<div class="span2">
			<?= Yii::t('classroom', 'Other info') ?>:
		</div>
		<div class="span10">
			<div class="content"><?= $this->session->other_info ?></div>
		</div>
	</div>

	<?php if ($this->dataProvider->getTotalItemCount() > 0): ?>
		<div class="classroom-sessions-grid" style="margin-top: 20px;">
			<?php
			$this->widget('DoceboCListView', array(
				'id' => 'session-days-list',
				'htmlOptions' => array('class' => 'docebo-list-view'),
				'template' => '{header}{items}',
				'dataProvider' => $this->dataProvider,
				'itemView' => '_sessionInfoListView',
				'columns' => array(
					array(
						'name' => 'status',
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME')
					),
					array(
						'name' => 'toggle_handle',
					),
					array(
						'name' => 'location',
						'header' => Yii::t('classroom', 'Location name'),
					),
					array(
						'name' => 'time',
						'header' => Yii::t('standard', '_END'),
					),
					array(
						'name' => 'date',
						'header' => Yii::t('standard', '_START')
					),
				)
			));
			?>
		</div>
	<?php else: ?>
		<div class="classroom-sessions-grid"
				 style="border: 1px solid #ddd; padding: 20px 15px 20px 15px; text-align: center; margin-top: 20px;">
			<?= Yii::t('classroom', 'No dates available for this session') ?>.
		</div>
	<?php endif; ?>

	<br/>

		<div class="row-fluid" style="margin-top: 25px;">
			<h3 class="span12 title-bold">
				<?= Yii::t('standard', '_LEARNING_OBJECTS') ?>:
			</h3>
		</div>

		<?php
		if ($_countObjects > 0):
		?>
		<div class="classroom-objects-grid">
			<?php
			$this->widget('DoceboCListView', array(
				'id' => 'classroom-objects-list',
				'htmlOptions' => array('class' => 'docebo-list-view'),
				'template' => '{header}{items}{pager}',
				'dataProvider' => LearningOrganization::model()->dataProviderClassroomLOs($this->courseModel->getPrimaryKey()),
				'itemView' => '_classroomLOsListView',
				'afterAjaxUpdate' => 'function(){attachEvents();}',
				'columns' => array(
					array(
						'name' => 'status',
						'header' => '&nbsp;'//Yii::t('standard', '_TYPE')
					),
					array(
						'name' => 'name',
						'header' => Yii::t('standard', '_NAME'),
					)
				)
			));
			?>
		</div>
		<br />
		<script type="text/javascript">
			$(function () {
				attachEvents();
				Arena.init({
					course_id: <?= $this->courseModel->getPrimaryKey() ?>,
					uploadFilesUrl: "<?= Docebo::createLmsUrl('player/training/axUpload', array('course_id' => $this->courseModel->getPrimaryKey() )) ?>",
					csrfToken: "<?=Yii::app()->request->csrfToken ?>"
				});
			});
			function attachEvents() {
				var launcher = new ClassroomLoLauncher({
					course_id: <?= $this->courseModel->getPrimaryKey() ?>,
					session_id: <?= $this->session->getPrimaryKey() ?>,
					mpidCourse: <?= CJSON::encode($this->courseModel->mpidCourse) ?>,
					isMpCourse: <?= CJSON::encode(($this->courseModel->mpidCourse > 0)) ?>,
					mpSeatsUnlimited: <?= CJSON::encode($this->courseModel->mpSeatsUnlimited()) ?>,
					availableSeats: 0, //it seems that this parameter isn't used
					userHasSeat: <?= CJSON::encode(LearningLogMarketplaceCourseLogin::userHasSeat($this->courseModel->getPrimaryKey(), Yii::app()->user->id)) ?>
				});
			}
		</script>
		<?php endif; ?>

	<?php if (!empty($this->evaluation)) : ?>
		<br />
		<h3 class="title-bold"><?= Yii::t('classroom', 'Personal Performance Evaluation') ?></h3>
		<div class="evaluation-container">
			<div class="row-fluid">
				<div class="evaluation-meta">
					<div class="evaluation-date">
						<em>
							<?php
							/*echo date('F jS, Y', strtotime($this->evaluation['date']))*/
							//NOTE: using php date('F') for retrieving month name may lead to problems with localization and languages. Rely on LMS translation files instead.
							$_evaluationTimestamp = strtotime(Yii::app()->localtime->fromLocalDate($this->evaluation['date']));
							echo Yii::t('standard', '_MONTH_'.date('m', $_evaluationTimestamp)).' '.date('j, Y', $_evaluationTimestamp);
							?>
						</em>
					</div>
					<br/>

					<div class="evaluation-instructor">
						<?php if (!empty($this->evaluation['evaluator'])): ?>
						<div class="thumbnail"><?= $this->evaluation['evaluator']->getAvatarImage() ?></div>
						<div class="instructor-title"><?= Yii::t('menu_over', 'Teacher') ?></div>
						<div class="instructor-name"><?= $this->evaluation['evaluator']->getFullName() ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="evaluation-status status-<?= $this->evaluation['status'] ?>">
					<div class="row-fluid">
						<div class="span6">
							<div class="status-label-container">
								<?php if ($this->evaluation['status'] == 'passed') : ?>
									<h4 class="status-label text-colored-green"><?= Yii::t('coursereport', '_PASSED') ?></h4>
								<?php else: ?>
									<h4 class="status-label text-colored-red"><?= Yii::t('coursereport', '_NOT_PASSED') ?></h4>
								<?php endif; ?>
							</div>
						</div>
						<div class="span6">
							<?php if ($this->evaluation['status'] == 'passed') : ?>
								<span class="classroom-sprite valid-big"></span>
							<?php else: ?>
								<span class="classroom-sprite invalid-big"></span>
							<?php endif; ?>
						</div>
					</div>
					<br/>

					<div class="row-fluid">
						<?php if (($this->evaluation['score'] > 0) || is_array($this->evaluation['score'])): ?>
							<div class="span6"><?= Yii::t('standard', '_SCORE') ?></div>
							<div class="span6">
									<?php
										$_evaluationMode = $this->session->evaluation_type; //it may be an online test session
										//TODO: this should be moved into widget controller "SessionInfo.php" (see line 92)
										switch ($_evaluationMode) {
											case LtCourseSession::EVALUATION_TYPE_SCORE: {
												//display score assigned to the session by instructor
												?>
												<div class="session-info-score-container">
													<span class="score-value <?= ($this->evaluation['status'] == 'passed') ? 'text-colored-green' : 'text-colored-red' ?>">
														<?= $this->evaluation['score'] ?>
													</span>
													<?php if ($this->hideScoreBase !== true) : ?>
													<span class="score-sep">/</span>
													<span class="score-base">
													<?php
														if($this->session->score_base > 0)
															echo $this->session->score_base;
														else
															echo (isset(Yii::app()->params['classroom-default-session-score_base']) && (int)Yii::app()->params['classroom-default-session-score_base'] > 0
																? (int)Yii::app()->params['classroom-default-session-score_base']
																: '100');
													?>
													</span>
													<?php endif; ?>
												</div>
												<?php
											} break;
											case LtCourseSession::EVALUATION_TYPE_ONLINE: {
												if(isset($this->evaluation['score']['score'])  && $this->evaluation['score']['score'] ){
													?>
													<span class="score <?= ($this->evaluation['status'] == 'passed') ? 'text-colored-green' : 'text-colored-red' ?>">
														<?= $this->evaluation['score']['score'] ?>
													</span>
													<?php
												}else {
													echo '-';
												}
											} break;
										}
									?>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div class="evaluation-content">
					<h4><?= Yii::t('classroom', 'Student Performance Evaluation') ?></h4>

					<div class="evaluation-comments">
						<?= $this->evaluation['comments'] ?>
					</div>

					<?php if ($this->evaluation['attachment']['name']) : ?>
						<div class="evaluation-attachment">
							<h4><?= Yii::t('standard', 'Attached files') ?></h4>

							<div class="attached-file">
								<span class="i-sprite is-file black"></span>
								<a class="text-colored" href="<?= $this->evaluation['attachment']['url'] ?>"
									 target="_blank"><?= $this->evaluation['attachment']['name'] ?></a>
							</div>
						</div>
					<?php endif; ?>
				</div>

			</div>

			<?php
			// Trigger event to let plugins showing their custom information
			$event = new DEvent($this, array(
				'courseModel' => $this->courseModel,
				'sessionModel' => $this->session
			));
			Yii::app()->event->raise('OnUserSessionInfoDisplay', $event);
			$continue = true;
			if ($event->shouldPerformAsDefault() && !empty($event->return_value)) {
				//some plugins may not need normal subscribe actions .. let them avoid those
				if (isset($event->return_value['continue']) && !$event->return_value['continue']) {
					$continue = false;
				}
			}
			// end event
			?>

		</div>
	<?php endif; ?>

	<?php
	// Trigger event to let plugins showing their custom information
	$event = new DEvent($this, array(
		'courseModel' => $this->courseModel,
		'sessionModel' => $this->session
	));
	Yii::app()->event->raise('OnCustomSessionInfo', $event);
	?>

</div>

<script type="text/javascript">

	var geocoder;
	var map;

	function updateMapPreview($map) {
		if ($map.hasClass('loaded')) return;

		var $mapCanvas = $map.find('.location-map-canvas');
		var address = $map.find('input').val();

		if (address == '') {
			// clear preview
			$mapCanvas.html('').attr('style', '');
			return;
		}

		if (!geocoder)
			geocoder = new google.maps.Geocoder();

		geocoder.geocode({'address': address}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var mapOptions = {
					zoom: 14
				}
				map = new google.maps.Map($mapCanvas[0], mapOptions);

				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});

				$map.addClass('loaded');
			} else {
				Docebo.log('Geocode was not successful for the following reason: ' + status);
			}
		});
	}

	$(document).ready(function () {
		var $sessionDays = $('#session-days-list');

		// css class name for a selected list row
		var cssItemSelected = 'selected';

		$sessionDays.find('.item')
			.bind('expand-row', function () {
				var $item = $(this);
				var $handle = $item.find('.toggle-handle'),
					$target = $item.find('.secondary');

				// retract all other rows
				$item.parent().find('.item').siblings().trigger('retract-row');

				$item.addClass(cssItemSelected);
				$handle
					.removeClass($handle.data('retract-class'))
					.addClass($handle.data('expand-class'));
				$target.slideDown().removeClass('hide');

				// show google maps
				$map = $item.find('.map-preview-container');
				updateMapPreview($map);
			})
			.bind('retract-row', function () {
				var $item = $(this);
				var $handle = $item.find('.toggle-handle'),
					$target = $item.find('.secondary');

				$item.removeClass(cssItemSelected);
				$handle
					.removeClass($handle.data('expand-class'))
					.addClass($handle.data('retract-class'));
				$target.hide();
			});

		$sessionDays.find('.primary').click(function () {
			var $item = $(this).closest('.item');

			var fnName = $item.hasClass(cssItemSelected)
				? 'retract-row'
				: 'expand-row';
			var arrow = $('.item-col.col-toggle_handle span.fa');
			if (fnName == 'expand-row') {
				arrow.removeClass('fa-chevron-down');
				arrow.addClass('fa-chevron-up');
			}else{
				arrow.removeClass('fa-chevron-up');
				arrow.addClass('fa-chevron-down');
			}
			$item.trigger(fnName);
		});

		$('.fancybox').fancybox();
	});
</script>