<?php
$pUserRights = Yii::app()->user->checkPURights($course->getPrimaryKey());

$canEditSessions = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/classroomsessions/add")));
$canEditWaitingList = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("enrollment/update")));
$canEditTrainingMaterials = (Yii::app()->user->getIsGodadmin() || ($pUserRights->all && Yii::app()->user->checkAccess("/lms/admin/course/mod")) || $this->userCanAdminCourse);
?>

<div class="session-main-actions-container main-actions clearfix">
	<ul id="docebo-bootcamp-sessions" class="docebo-bootcamp clearfix">
		<?php if ($canEditSessions): ?>
			<li>
				<div>
					<a id="add-new-session"
						 href="<?= Docebo::createAdminUrl('ClassroomApp/session/create', array('course_id' => $course->getPrimaryKey())) ?>"
						 class="open-dialog"
						 data-dialog-class="modal-create-session"
						 alt="<?= Yii::t('helper', 'Create Session - lt') ?>">
						<span class="classroom-sprite new-date"></span>
						<span class="classroom-sprite new-date white"></span>
						<h4><?= Yii::t('classroom', 'New session') ?></h4>
					</a>
				</div>
			</li>
		<?php endif; ?>
		<?php if ($canEditWaitingList): ?>
			<li>
				<div>
					<a id="users-waiting-to-be-approved"
						 href="<?= Docebo::createAdminUrl('ClassroomApp/session/assign', array('course_id' => $course->getPrimaryKey())) ?>"
						 alt="<?php echo Yii::t('helper', 'Unassigned Users - lt'); ?>">
						<span class="session-waiting-approval"></span>
						<span class="session-waiting-approval white"></span>
						<?php
							$waitingForSessions = $course->countWaitingForAllIltSessions();
							$_countUnassigned = $course->getUnassignedUsersCount() + ((is_int($waitingForSessions) && $waitingForSessions>0)  ? $waitingForSessions : 0); ?>
						<?php if ($_countUnassigned > 0) : ?>
							<span class="notification"><?= $_countUnassigned ?></span>
						<?php endif; ?>
						<h4><?php echo Yii::t('classroom', 'Users waiting to be assigned'); ?></h4>
					</a>
				</div>
			</li>
		<?php endif; ?>


			<?php if ($canEditTrainingMaterials): ?>
				<li>
					<div>
						<a id="classroom-los-management"
							 href="<?= Docebo::createLmsUrl('ClassroomApp/session/manageLearningObjects', array('course_id' => $course->getPrimaryKey())) ?>"
							 alt="<?php echo Yii::t('organization', 'ADD TRAINING RESOURCES');//TODO: we should make a dedicated translation, something like: Yii::t('helper', 'Manage learning objects - lt'); ?>">
							<span class="classroom-sprite manage-objects black"></span>
							<span class="classroom-sprite manage-objects white"></span>
							<h4><?php echo Yii::t('standard', 'Training materials'); ?></h4>
						</a>
					</div>
				</li>
			<?php endif; ?>

		<? Yii::app()->event->raise('onClassroomSessionManagementButtonsDisplay', new DEvent($this, array(
			'course'=>$course,
		))); ?>
	</ul>

	<div class="info">
		<div>
			<h4></h4>
			<p></p>
		</div>
	</div>
</div>
